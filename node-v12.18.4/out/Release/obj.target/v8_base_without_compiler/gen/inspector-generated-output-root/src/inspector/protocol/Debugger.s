	.file	"Debugger.cpp"
	.text
	.section	.text._ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv, @function
_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv:
.LFB4422:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movq	8(%rsi), %rcx
	leaq	24(%rsi), %rdx
	cmpq	%rdx, %rcx
	je	.L5
	movq	%rcx, (%rdi)
	movq	24(%rsi), %rcx
	movq	%rcx, 16(%rdi)
.L3:
	movq	16(%rsi), %rcx
	movq	%rdx, 8(%rsi)
	xorl	%edx, %edx
	movq	$0, 16(%rsi)
	movq	%rcx, 8(%rax)
	movw	%dx, 24(%rsi)
	movq	40(%rsi), %rdx
	movq	%rdx, 32(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movdqu	24(%rsi), %xmm0
	movups	%xmm0, 16(%rdi)
	jmp	.L3
	.cfi_endproc
.LFE4422:
	.size	_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv, .-_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv:
.LFB4423:
	.cfi_startproc
	endbr64
	movdqu	48(%rsi), %xmm1
	movq	64(%rsi), %rdx
	pxor	%xmm0, %xmm0
	movq	%rdi, %rax
	movq	$0, 64(%rsi)
	movq	%rdx, 16(%rdi)
	movups	%xmm1, (%rdi)
	movups	%xmm0, 48(%rsi)
	ret
	.cfi_endproc
.LFE4423:
	.size	_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv, .-_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Debugger14ScriptPositionD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger14ScriptPositionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger14ScriptPositionD2Ev
	.type	_ZN12v8_inspector8protocol8Debugger14ScriptPositionD2Ev, @function
_ZN12v8_inspector8protocol8Debugger14ScriptPositionD2Ev:
.LFB5543:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5543:
	.size	_ZN12v8_inspector8protocol8Debugger14ScriptPositionD2Ev, .-_ZN12v8_inspector8protocol8Debugger14ScriptPositionD2Ev
	.weak	_ZN12v8_inspector8protocol8Debugger14ScriptPositionD1Ev
	.set	_ZN12v8_inspector8protocol8Debugger14ScriptPositionD1Ev,_ZN12v8_inspector8protocol8Debugger14ScriptPositionD2Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger14ScriptPositionD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger14ScriptPositionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger14ScriptPositionD0Ev
	.type	_ZN12v8_inspector8protocol8Debugger14ScriptPositionD0Ev, @function
_ZN12v8_inspector8protocol8Debugger14ScriptPositionD0Ev:
.LFB5545:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5545:
	.size	_ZN12v8_inspector8protocol8Debugger14ScriptPositionD0Ev, .-_ZN12v8_inspector8protocol8Debugger14ScriptPositionD0Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger8LocationD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger8LocationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger8LocationD2Ev
	.type	_ZN12v8_inspector8protocol8Debugger8LocationD2Ev, @function
_ZN12v8_inspector8protocol8Debugger8LocationD2Ev:
.LFB5511:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	addq	$24, %rdi
	movq	%rax, -24(%rdi)
	cmpq	%rdi, %r8
	je	.L9
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	ret
	.cfi_endproc
.LFE5511:
	.size	_ZN12v8_inspector8protocol8Debugger8LocationD2Ev, .-_ZN12v8_inspector8protocol8Debugger8LocationD2Ev
	.weak	_ZN12v8_inspector8protocol8Debugger8LocationD1Ev
	.set	_ZN12v8_inspector8protocol8Debugger8LocationD1Ev,_ZN12v8_inspector8protocol8Debugger8LocationD2Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger8LocationD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger8LocationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev
	.type	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev, @function
_ZN12v8_inspector8protocol8Debugger8LocationD0Ev:
.LFB5513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L12
	call	_ZdlPv@PLT
.L12:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5513:
	.size	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev, .-_ZN12v8_inspector8protocol8Debugger8LocationD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12CallArgumentD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12CallArgumentD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12CallArgumentD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime12CallArgumentD2Ev, @function
_ZN12v8_inspector8protocol7Runtime12CallArgumentD2Ev:
.LFB4930:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12CallArgumentE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	leaq	88(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L15
	call	_ZdlPv@PLT
.L15:
	movq	24(%rbx), %rdi
	leaq	40(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L16
	call	_ZdlPv@PLT
.L16:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L14
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4930:
	.size	_ZN12v8_inspector8protocol7Runtime12CallArgumentD2Ev, .-_ZN12v8_inspector8protocol7Runtime12CallArgumentD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime12CallArgumentD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime12CallArgumentD1Ev,_ZN12v8_inspector8protocol7Runtime12CallArgumentD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev:
.LFB4606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L20
	call	_ZdlPv@PLT
.L20:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L19
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4606:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger11SearchMatchD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev
	.type	_ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev, @function
_ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev:
.LFB5698:
	.cfi_startproc
	endbr64
	leaq	80+_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE(%rip), %rax
	movq	24(%rdi), %r8
	addq	$40, %rdi
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -40(%rdi)
	cmpq	%rdi, %r8
	je	.L23
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L23:
	ret
	.cfi_endproc
.LFE5698:
	.size	_ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev, .-_ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev
	.weak	_ZN12v8_inspector8protocol8Debugger11SearchMatchD1Ev
	.set	_ZN12v8_inspector8protocol8Debugger11SearchMatchD1Ev,_ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger11SearchMatchD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev
	.type	_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev, @function
_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev:
.LFB5700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	40(%r12), %rax
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movq	24(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L26
	call	_ZdlPv@PLT
.L26:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5700:
	.size	_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev, .-_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger13BreakLocationD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger13BreakLocationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger13BreakLocationD2Ev
	.type	_ZN12v8_inspector8protocol8Debugger13BreakLocationD2Ev, @function
_ZN12v8_inspector8protocol8Debugger13BreakLocationD2Ev:
.LFB5729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger13BreakLocationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	leaq	88(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L29
	call	_ZdlPv@PLT
.L29:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L28
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5729:
	.size	_ZN12v8_inspector8protocol8Debugger13BreakLocationD2Ev, .-_ZN12v8_inspector8protocol8Debugger13BreakLocationD2Ev
	.weak	_ZN12v8_inspector8protocol8Debugger13BreakLocationD1Ev
	.set	_ZN12v8_inspector8protocol8Debugger13BreakLocationD1Ev,_ZN12v8_inspector8protocol8Debugger13BreakLocationD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev, @function
_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev:
.LFB5119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	leaq	104(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L33
	call	_ZdlPv@PLT
.L33:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L34
	call	_ZdlPv@PLT
.L34:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L32
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5119:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev, .-_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev,_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.section	.text._ZN12v8_inspector8protocol23InternalRawNotificationD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol23InternalRawNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev
	.type	_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev, @function
_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev:
.LFB4419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol23InternalRawNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L38
	call	_ZdlPv@PLT
.L38:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L37
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4419:
	.size	_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev, .-_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev
	.weak	_ZN12v8_inspector8protocol23InternalRawNotificationD1Ev
	.set	_ZN12v8_inspector8protocol23InternalRawNotificationD1Ev,_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv:
.LFB4615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13CustomPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L44
	movq	(%rdi), %rax
	call	*24(%rax)
.L44:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L51:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4615:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv:
.LFB4614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13CustomPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L52
	movq	(%rdi), %rax
	call	*24(%rax)
.L52:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L59
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L59:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4614:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv:
.LFB4661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13ObjectPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L60
	movq	(%rdi), %rax
	call	*24(%rax)
.L60:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L67
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L67:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4661:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv:
.LFB4660:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13ObjectPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L68
	movq	(%rdi), %rax
	call	*24(%rax)
.L68:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L75
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L75:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4660:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv:
.LFB4762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime12EntryPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L76
	movq	(%rdi), %rax
	call	*24(%rax)
.L76:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L83
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L83:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4762:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv:
.LFB4761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime12EntryPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L84
	movq	(%rdi), %rax
	call	*24(%rax)
.L84:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L91
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L91:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4761:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv:
.LFB4730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime15PropertyPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L92
	movq	(%rdi), %rax
	call	*24(%rax)
.L92:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L99
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L99:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4730:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv:
.LFB4729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime15PropertyPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L100
	movq	(%rdi), %rax
	call	*24(%rax)
.L100:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L107
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L107:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4729:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv:
.LFB5133:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime9CallFrame7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L108
	movq	(%rdi), %rax
	call	*24(%rax)
.L108:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L115
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L115:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5133:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv:
.LFB5132:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime9CallFrame7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L116
	movq	(%rdi), %rax
	call	*24(%rax)
.L116:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L123
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L123:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5132:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv:
.LFB5064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime16ExceptionDetails7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L124
	movq	(%rdi), %rax
	call	*24(%rax)
.L124:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L131
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L131:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5064:
	.size	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv:
.LFB5063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime16ExceptionDetails7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L132
	movq	(%rdi), %rax
	call	*24(%rax)
.L132:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L139
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L139:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5063:
	.size	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime12CallArgument17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12CallArgument17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12CallArgument17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime12CallArgument17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime12CallArgument17serializeToBinaryEv:
.LFB4943:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime12CallArgument7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L140
	movq	(%rdi), %rax
	call	*24(%rax)
.L140:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L147
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L147:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4943:
	.size	_ZN12v8_inspector8protocol7Runtime12CallArgument17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime12CallArgument17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime12CallArgument15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12CallArgument15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12CallArgument15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime12CallArgument15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime12CallArgument15serializeToJSONEv:
.LFB4942:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime12CallArgument7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L148
	movq	(%rdi), %rax
	call	*24(%rax)
.L148:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L155
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L155:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4942:
	.size	_ZN12v8_inspector8protocol7Runtime12CallArgument15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime12CallArgument15serializeToJSONEv
	.section	.text._ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0, @function
_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0:
.LFB12540:
	.cfi_startproc
	testq	%rdi, %rdi
	je	.L156
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$8, %esi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L156:
	ret
	.cfi_endproc
.LFE12540:
	.size	_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0, .-_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImplD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger14DispatcherImplD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger14DispatcherImplD2Ev
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImplD2Ev, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImplD2Ev:
.LFB6138:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger14DispatcherImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	144(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	jne	.L166
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L192:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L162
.L165:
	movq	%rbx, %r13
.L166:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L163
	call	_ZdlPv@PLT
.L163:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L192
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L165
.L162:
	movq	136(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%r12), %rdi
	leaq	176(%r12), %rax
	movq	$0, 152(%r12)
	movq	$0, 144(%r12)
	cmpq	%rax, %rdi
	je	.L167
	call	_ZdlPv@PLT
.L167:
	movq	88(%r12), %r13
	testq	%r13, %r13
	jne	.L171
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L193:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L168
.L170:
	movq	%rbx, %r13
.L171:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	jne	.L193
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L170
.L168:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L172
	call	_ZdlPv@PLT
.L172:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector8protocol14DispatcherBaseD2Ev@PLT
	.cfi_endproc
.LFE6138:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImplD2Ev, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImplD2Ev
	.weak	_ZN12v8_inspector8protocol8Debugger14DispatcherImplD1Ev
	.set	_ZN12v8_inspector8protocol8Debugger14DispatcherImplD1Ev,_ZN12v8_inspector8protocol8Debugger14DispatcherImplD2Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger11SearchMatchD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD1Ev
	.type	_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD1Ev, @function
_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD1Ev:
.LFB12775:
	.cfi_startproc
	endbr64
	leaq	80+_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE(%rip), %rax
	movq	16(%rdi), %r8
	addq	$32, %rdi
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -40(%rdi)
	cmpq	%rdi, %r8
	je	.L194
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L194:
	ret
	.cfi_endproc
.LFE12775:
	.size	_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD1Ev, .-_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD1Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger13BreakLocationD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger13BreakLocationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger13BreakLocationD0Ev
	.type	_ZN12v8_inspector8protocol8Debugger13BreakLocationD0Ev, @function
_ZN12v8_inspector8protocol8Debugger13BreakLocationD0Ev:
.LFB5731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger13BreakLocationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L197
	call	_ZdlPv@PLT
.L197:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L198
	call	_ZdlPv@PLT
.L198:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$112, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5731:
	.size	_ZN12v8_inspector8protocol8Debugger13BreakLocationD0Ev, .-_ZN12v8_inspector8protocol8Debugger13BreakLocationD0Ev
	.section	.text._ZN12v8_inspector8protocol23InternalRawNotificationD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol23InternalRawNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev
	.type	_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev, @function
_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev:
.LFB4421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol23InternalRawNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L201
	call	_ZdlPv@PLT
.L201:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L202
	call	_ZdlPv@PLT
.L202:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4421:
	.size	_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev, .-_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev:
.LFB4608:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L208
	call	_ZdlPv@PLT
.L208:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L209
	call	_ZdlPv@PLT
.L209:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4608:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev, @function
_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev:
.LFB5121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L212
	call	_ZdlPv@PLT
.L212:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L213
	call	_ZdlPv@PLT
.L213:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L214
	call	_ZdlPv@PLT
.L214:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$136, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5121:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev, .-_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.section	.text._ZN12v8_inspector8protocol16InternalResponseD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol16InternalResponseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol16InternalResponseD0Ev
	.type	_ZN12v8_inspector8protocol16InternalResponseD0Ev, @function
_ZN12v8_inspector8protocol16InternalResponseD0Ev:
.LFB4412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L217
	movq	(%rdi), %rax
	call	*24(%rax)
.L217:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L218
	call	_ZdlPv@PLT
.L218:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4412:
	.size	_ZN12v8_inspector8protocol16InternalResponseD0Ev, .-_ZN12v8_inspector8protocol16InternalResponseD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12StackTraceIdD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev, @function
_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev:
.LFB5239:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	80(%r12), %rax
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movq	64(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L224
	call	_ZdlPv@PLT
.L224:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L225
	call	_ZdlPv@PLT
.L225:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5239:
	.size	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev, .-_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger11SearchMatchD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD0Ev
	.type	_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD0Ev, @function
_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD0Ev:
.LFB12777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	addq	$32, %rdi
	subq	$8, %rsp
	movq	-16(%rdi), %r8
	movups	%xmm0, -40(%rdi)
	cmpq	%rdi, %r8
	je	.L228
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L228:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12777:
	.size	_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD0Ev, .-_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD0Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImplD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger14DispatcherImplD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger14DispatcherImplD0Ev
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImplD0Ev, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImplD0Ev:
.LFB6140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger14DispatcherImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	144(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	jne	.L235
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L261:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L231
.L234:
	movq	%rbx, %r13
.L235:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L232
	call	_ZdlPv@PLT
.L232:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L261
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L234
.L231:
	movq	136(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%r12), %rdi
	leaq	176(%r12), %rax
	movq	$0, 152(%r12)
	movq	$0, 144(%r12)
	cmpq	%rax, %rdi
	je	.L236
	call	_ZdlPv@PLT
.L236:
	movq	88(%r12), %r13
	testq	%r13, %r13
	jne	.L240
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L262:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L237
.L239:
	movq	%rbx, %r13
.L240:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	jne	.L262
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L239
.L237:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L241
	call	_ZdlPv@PLT
.L241:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$192, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6140:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImplD0Ev, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImplD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12CallArgumentD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12CallArgumentD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12CallArgumentD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime12CallArgumentD0Ev, @function
_ZN12v8_inspector8protocol7Runtime12CallArgumentD0Ev:
.LFB4932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12CallArgumentE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L264
	call	_ZdlPv@PLT
.L264:
	movq	24(%r12), %rdi
	leaq	40(%r12), %rax
	cmpq	%rax, %rdi
	je	.L265
	call	_ZdlPv@PLT
.L265:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L266
	movq	(%rdi), %rax
	call	*24(%rax)
.L266:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$112, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4932:
	.size	_ZN12v8_inspector8protocol7Runtime12CallArgumentD0Ev, .-_ZN12v8_inspector8protocol7Runtime12CallArgumentD0Ev
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImpl14setReturnValueEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"params"
.LC1:
	.string	"newValue"
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl14setReturnValueEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl14setReturnValueEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl14setReturnValueEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl14setReturnValueEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$136, %rsp
	movl	%esi, -148(%rbp)
	movq	(%r8), %r14
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L272
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	jne	.L273
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L321
	call	_ZdlPv@PLT
.L321:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	cmpq	%r15, %rdi
	je	.L275
	call	_ZdlPv@PLT
.L275:
	leaq	.LC1(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-136(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol7Runtime12CallArgument9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L322
	leaq	-128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r13), %rsi
	movq	-136(%rbp), %rdx
	movq	%r12, %rdi
	movq	(%rsi), %rax
	movq	%rdx, -120(%rbp)
	leaq	-120(%rbp), %rdx
	movq	$0, -136(%rbp)
	call	*192(%rax)
	movq	-120(%rbp), %r14
	testq	%r14, %r14
	je	.L279
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12CallArgumentD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L280
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12CallArgumentE(%rip), %rax
	movq	72(%r14), %rdi
	movq	%rax, (%r14)
	leaq	88(%r14), %rax
	cmpq	%rax, %rdi
	je	.L281
	call	_ZdlPv@PLT
.L281:
	movq	24(%r14), %rdi
	leaq	40(%r14), %rax
	cmpq	%rax, %rdi
	je	.L282
	call	_ZdlPv@PLT
.L282:
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.L283
	movq	(%rdi), %rax
	call	*24(%rax)
.L283:
	movl	$112, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L279:
	cmpl	$2, -112(%rbp)
	je	.L323
	movq	-128(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L285
	movl	-148(%rbp), %esi
	movq	%r12, %rdx
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L285:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L286
.L326:
	call	_ZdlPv@PLT
.L286:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L278
	call	_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0
.L278:
	movq	-136(%rbp), %r12
	testq	%r12, %r12
	je	.L271
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12CallArgumentD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L289
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12CallArgumentE(%rip), %rax
	movq	72(%r12), %rdi
	movq	%rax, (%r12)
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L290
	call	_ZdlPv@PLT
.L290:
	movq	24(%r12), %rdi
	leaq	40(%r12), %rax
	cmpq	%rax, %rdi
	je	.L291
	call	_ZdlPv@PLT
.L291:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L292
	movq	(%rdi), %rax
	call	*24(%rax)
.L292:
	movl	$112, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L271:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L324
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	.cfi_restore_state
	movq	-112(%rbp), %rdi
.L273:
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L325
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L322:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	-148(%rbp), %esi
	movl	$-32602, %edx
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L278
	call	_ZdlPv@PLT
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L325:
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L289:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L323:
	movq	8(%r13), %rdi
	movq	-168(%rbp), %rcx
	movq	-160(%rbp), %rdx
	movl	-148(%rbp), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L326
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L280:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L279
.L324:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6314:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl14setReturnValueEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl14setReturnValueEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationD0Ev
	.type	_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationD0Ev, @function
_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationD0Ev:
.LFB5767:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	48(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L328
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L329
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L330
	call	_ZdlPv@PLT
.L330:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L328:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L331
	call	_ZdlPv@PLT
.L331:
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L329:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L328
	.cfi_endproc
.LFE5767:
	.size	_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationD0Ev, .-_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationD0Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationD2Ev
	.type	_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationD2Ev, @function
_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationD2Ev:
.LFB5765:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	48(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L337
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L338
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L339
	call	_ZdlPv@PLT
.L339:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L337:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L336
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L336:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L337
	.cfi_endproc
.LFE5765:
	.size	_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationD2Ev, .-_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationD2Ev
	.weak	_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationD1Ev
	.set	_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationD1Ev,_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev:
.LFB4640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	160(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L346
	movq	8(%r13), %rax
	movq	0(%r13), %r12
	movq	%rax, -64(%rbp)
	cmpq	%r12, %rax
	je	.L347
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %r14
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L402:
	movq	16(%r15), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%rdi, %rdi
	je	.L350
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	jne	.L351
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L350:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L352
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	jne	.L353
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L352:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L348:
	addq	$8, %r12
	cmpq	%r12, -64(%rbp)
	je	.L401
.L354:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L348
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L402
	movq	%r15, %rdi
	addq	$8, %r12
	call	*%rax
	cmpq	%r12, -64(%rbp)
	jne	.L354
	.p2align 4,,10
	.p2align 3
.L401:
	movq	0(%r13), %r12
.L347:
	testq	%r12, %r12
	je	.L355
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L355:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L346:
	movq	152(%rbx), %r13
	testq	%r13, %r13
	je	.L356
	movq	8(%r13), %r14
	movq	0(%r13), %r12
	cmpq	%r12, %r14
	jne	.L366
	testq	%r12, %r12
	je	.L367
	.p2align 4,,10
	.p2align 3
.L405:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L367:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L356:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L368
	call	_ZdlPv@PLT
.L368:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L369
	call	_ZdlPv@PLT
.L369:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L345
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L404:
	.cfi_restore_state
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L360
	call	_ZdlPv@PLT
.L360:
	movq	136(%r15), %rdi
	testq	%rdi, %rdi
	je	.L361
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L362
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L361:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L363
	call	_ZdlPv@PLT
.L363:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L364
	call	_ZdlPv@PLT
.L364:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L365
	call	_ZdlPv@PLT
.L365:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L358:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L403
.L366:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L358
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L404
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rax
	cmpq	%r12, %r14
	jne	.L366
	.p2align 4,,10
	.p2align 3
.L403:
	movq	0(%r13), %r12
	testq	%r12, %r12
	jne	.L405
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L345:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L353:
	.cfi_restore_state
	call	*%rax
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L351:
	call	*%rax
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L362:
	call	*%rax
	jmp	.L361
	.cfi_endproc
.LFE4640:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev:
.LFB4642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$168, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4642:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev:
.LFB4753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	16(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L409
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L410
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L409:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L408
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L412
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	popq	%rbx
	movq	%r12, %rdi
	movl	$168, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L408:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L410:
	.cfi_restore_state
	call	*%rax
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L412:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE4753:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev:
.LFB4713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movq	152(%rdi), %rdi
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L418
	call	_ZdlPv@PLT
.L418:
	movq	136(%rbx), %r12
	testq	%r12, %r12
	je	.L419
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L420
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L419:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L421
	call	_ZdlPv@PLT
.L421:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L422
	call	_ZdlPv@PLT
.L422:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L417
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L417:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L420:
	.cfi_restore_state
	call	*%rax
	jmp	.L419
	.cfi_endproc
.LFE4713:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev:
.LFB4755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	16(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L429
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L430
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L429:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L431
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L432
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L431:
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L430:
	.cfi_restore_state
	call	*%rax
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L432:
	call	*%rax
	jmp	.L431
	.cfi_endproc
.LFE4755:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev:
.LFB4715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	movq	152(%rdi), %rdi
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L441
	call	_ZdlPv@PLT
.L441:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L442
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L443
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L442:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L444
	call	_ZdlPv@PLT
.L444:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L445
	call	_ZdlPv@PLT
.L445:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L446
	call	_ZdlPv@PLT
.L446:
	movq	%r12, %rdi
	movl	$192, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L443:
	.cfi_restore_state
	call	*%rax
	jmp	.L442
	.cfi_endproc
.LFE4715:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl7stepOutEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl7stepOutEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl7stepOutEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl7stepOutEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-120(%rbp), %rdi
	movq	%rbx, %rsi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	movq	%r15, %rdi
	movq	(%rsi), %rax
	call	*232(%rax)
	cmpl	$2, -112(%rbp)
	je	.L464
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L453
	movq	%r15, %rdx
	movl	%r12d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L453:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L454
	call	_ZdlPv@PLT
.L454:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L451
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L451:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L465
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L464:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L453
.L465:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6326:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl7stepOutEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl7stepOutEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl8stepOverEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl8stepOverEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl8stepOverEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl8stepOverEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-120(%rbp), %rdi
	movq	%rbx, %rsi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	movq	%r15, %rdi
	movq	(%rsi), %rax
	call	*240(%rax)
	cmpl	$2, -112(%rbp)
	je	.L479
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L468
	movq	%r15, %rdx
	movl	%r12d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L468:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L469
	call	_ZdlPv@PLT
.L469:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L466
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L466:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L480
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L479:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L468
.L480:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6327:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl8stepOverEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl8stepOverEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl5pauseEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl5pauseEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl5pauseEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl5pauseEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6217:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-120(%rbp), %rdi
	movq	%rbx, %rsi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	movq	%r15, %rdi
	movq	(%rsi), %rax
	call	*72(%rax)
	cmpl	$2, -112(%rbp)
	je	.L494
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L483
	movq	%r15, %rdx
	movl	%r12d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L483:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L484
	call	_ZdlPv@PLT
.L484:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L481
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L481:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L495
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L494:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L483
.L495:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6217:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl5pauseEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl5pauseEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-120(%rbp), %rdi
	movq	%rbx, %rsi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	movq	%r15, %rdi
	movq	(%rsi), %rax
	call	*24(%rax)
	cmpl	$2, -112(%rbp)
	je	.L509
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L498
	movq	%r15, %rdx
	movl	%r12d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L498:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L499
	call	_ZdlPv@PLT
.L499:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L496
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L496:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L510
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L509:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L498
.L510:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6151:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl6resumeEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl6resumeEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl6resumeEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl6resumeEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6246:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-120(%rbp), %rdi
	movq	%rbx, %rsi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	movq	%r15, %rdi
	movq	(%rsi), %rax
	call	*104(%rax)
	cmpl	$2, -112(%rbp)
	je	.L524
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L513
	movq	%r15, %rdx
	movl	%r12d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L513:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L514
	call	_ZdlPv@PLT
.L514:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L511
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L511:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L525
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L524:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L513
.L525:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6246:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl6resumeEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl6resumeEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImpl16pauseOnAsyncCallEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"parentStackTraceId"
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl16pauseOnAsyncCallEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl16pauseOnAsyncCallEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl16pauseOnAsyncCallEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl16pauseOnAsyncCallEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6218:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$136, %rsp
	movl	%esi, -148(%rbp)
	movq	(%r8), %r14
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L527
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	jne	.L528
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L568
	call	_ZdlPv@PLT
.L568:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	cmpq	%r15, %rdi
	je	.L530
	call	_ZdlPv@PLT
.L530:
	leaq	.LC2(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-136(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol7Runtime12StackTraceId9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L569
	leaq	-128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r13), %rsi
	movq	-136(%rbp), %rdx
	movq	%r12, %rdi
	movq	(%rsi), %rax
	movq	%rdx, -120(%rbp)
	leaq	-120(%rbp), %rdx
	movq	$0, -136(%rbp)
	call	*80(%rax)
	movq	-120(%rbp), %r14
	testq	%r14, %r14
	je	.L534
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L535
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r14), %rdi
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm2
	leaq	80(%r14), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r14)
	cmpq	%rax, %rdi
	je	.L536
	call	_ZdlPv@PLT
.L536:
	movq	16(%r14), %rdi
	leaq	32(%r14), %rax
	cmpq	%rax, %rdi
	je	.L537
	call	_ZdlPv@PLT
.L537:
	movl	$104, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L534:
	cmpl	$2, -112(%rbp)
	je	.L570
	movq	-128(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L539
	movl	-148(%rbp), %esi
	movq	%r12, %rdx
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L539:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L540
.L573:
	call	_ZdlPv@PLT
.L540:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L533
	call	_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0
.L533:
	movq	-136(%rbp), %r12
	testq	%r12, %r12
	je	.L526
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L543
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r12), %rdi
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm1
	leaq	80(%r12), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	cmpq	%rax, %rdi
	je	.L544
	call	_ZdlPv@PLT
.L544:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L545
	call	_ZdlPv@PLT
.L545:
	movl	$104, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L526:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L571
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L527:
	.cfi_restore_state
	movq	-112(%rbp), %rdi
.L528:
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L572
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L569:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	-148(%rbp), %esi
	movl	$-32602, %edx
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L533
	call	_ZdlPv@PLT
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L572:
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L543:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L570:
	movq	8(%r13), %rdi
	movq	-168(%rbp), %rcx
	movq	-160(%rbp), %rdx
	movl	-148(%rbp), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L573
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L535:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L534
.L571:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6218:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl16pauseOnAsyncCallEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl16pauseOnAsyncCallEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0, @function
_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0:
.LFB12655:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rdx
	movq	(%rdi), %rax
	movq	%rdi, -160(%rbp)
	movq	%rdx, -192(%rbp)
	movq	%rax, -96(%rbp)
	cmpq	%rax, %rdx
	je	.L575
	.p2align 4,,10
	.p2align 3
.L1071:
	movq	-96(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -72(%rbp)
	testq	%rcx, %rcx
	je	.L576
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L577
	movq	160(%rcx), %r12
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger9CallFrameE(%rip), %rax
	movq	%rax, (%rcx)
	testq	%r12, %r12
	je	.L578
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L579
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rax
	movq	312(%r12), %r13
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	testq	%r13, %r13
	je	.L580
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L581
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	56(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L582
	call	_ZdlPv@PLT
.L582:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L583
	call	_ZdlPv@PLT
.L583:
	movl	$96, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L580:
	movq	304(%r12), %r13
	testq	%r13, %r13
	je	.L584
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L585
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L586
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	%rcx, -64(%rbp)
	cmpq	%r14, %rcx
	je	.L587
	movq	%r12, -80(%rbp)
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L1633:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L590
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L591
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L590:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L592
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L593
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L592:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L588:
	addq	$8, %r14
	cmpq	%r14, -64(%rbp)
	je	.L1632
.L594:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L588
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1633
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -64(%rbp)
	jne	.L594
	.p2align 4,,10
	.p2align 3
.L1632:
	movq	-56(%rbp), %rax
	movq	-80(%rbp), %r12
	movq	(%rax), %r14
.L587:
	testq	%r14, %r14
	je	.L595
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L595:
	movq	-56(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L586:
	movq	152(%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L596
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	movq	%rdx, -64(%rbp)
	cmpq	%r14, %rdx
	je	.L597
	movq	%r12, -80(%rbp)
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L1635:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L600
	call	_ZdlPv@PLT
.L600:
	movq	136(%r15), %r12
	testq	%r12, %r12
	je	.L601
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L602
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L601:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L603
	call	_ZdlPv@PLT
.L603:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L604
	call	_ZdlPv@PLT
.L604:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L605
	call	_ZdlPv@PLT
.L605:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L598:
	addq	$8, %r14
	cmpq	%r14, -64(%rbp)
	je	.L1634
.L606:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L598
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1635
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -64(%rbp)
	jne	.L606
	.p2align 4,,10
	.p2align 3
.L1634:
	movq	-56(%rbp), %rax
	movq	-80(%rbp), %r12
	movq	(%rax), %r14
.L597:
	testq	%r14, %r14
	je	.L607
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L607:
	movq	-56(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L596:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L608
	call	_ZdlPv@PLT
.L608:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L609
	call	_ZdlPv@PLT
.L609:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L610
	call	_ZdlPv@PLT
.L610:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L584:
	movq	264(%r12), %rdi
	leaq	280(%r12), %rax
	cmpq	%rax, %rdi
	je	.L611
	call	_ZdlPv@PLT
.L611:
	movq	216(%r12), %rdi
	leaq	232(%r12), %rax
	cmpq	%rax, %rdi
	je	.L612
	call	_ZdlPv@PLT
.L612:
	movq	168(%r12), %rdi
	leaq	184(%r12), %rax
	cmpq	%rax, %rdi
	je	.L613
	call	_ZdlPv@PLT
.L613:
	movq	152(%r12), %rdi
	testq	%rdi, %rdi
	je	.L614
	movq	(%rdi), %rax
	call	*24(%rax)
.L614:
	movq	112(%r12), %rdi
	leaq	128(%r12), %rax
	cmpq	%rax, %rdi
	je	.L615
	call	_ZdlPv@PLT
.L615:
	movq	64(%r12), %rdi
	leaq	80(%r12), %rax
	cmpq	%rax, %rdi
	je	.L616
	call	_ZdlPv@PLT
.L616:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L617
	call	_ZdlPv@PLT
.L617:
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L578:
	movq	-72(%rbp), %rax
	movq	152(%rax), %r12
	testq	%r12, %r12
	je	.L618
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L619
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rax
	movq	312(%r12), %r13
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm2
	movq	%rcx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r12)
	testq	%r13, %r13
	je	.L620
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L621
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	56(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L622
	call	_ZdlPv@PLT
.L622:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L623
	call	_ZdlPv@PLT
.L623:
	movl	$96, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L620:
	movq	304(%r12), %r13
	testq	%r13, %r13
	je	.L624
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L625
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L626
	movq	8(%rax), %rsi
	movq	(%rax), %r14
	movq	%rsi, -64(%rbp)
	cmpq	%r14, %rsi
	je	.L627
	movq	%r12, -80(%rbp)
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L1637:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L630
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L631
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L630:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L632
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L633
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L632:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L628:
	addq	$8, %r14
	cmpq	%r14, -64(%rbp)
	je	.L1636
.L634:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L628
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1637
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -64(%rbp)
	jne	.L634
	.p2align 4,,10
	.p2align 3
.L1636:
	movq	-56(%rbp), %rax
	movq	-80(%rbp), %r12
	movq	(%rax), %r14
.L627:
	testq	%r14, %r14
	je	.L635
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L635:
	movq	-56(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L626:
	movq	152(%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L636
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	%rcx, -64(%rbp)
	cmpq	%r14, %rcx
	je	.L637
	movq	%r12, -80(%rbp)
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L1639:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L640
	call	_ZdlPv@PLT
.L640:
	movq	136(%r15), %r12
	testq	%r12, %r12
	je	.L641
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L642
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L641:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L643
	call	_ZdlPv@PLT
.L643:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L644
	call	_ZdlPv@PLT
.L644:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L645
	call	_ZdlPv@PLT
.L645:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L638:
	addq	$8, %r14
	cmpq	%r14, -64(%rbp)
	je	.L1638
.L646:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L638
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1639
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -64(%rbp)
	jne	.L646
	.p2align 4,,10
	.p2align 3
.L1638:
	movq	-56(%rbp), %rax
	movq	-80(%rbp), %r12
	movq	(%rax), %r14
.L637:
	testq	%r14, %r14
	je	.L647
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L647:
	movq	-56(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L636:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L648
	call	_ZdlPv@PLT
.L648:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L649
	call	_ZdlPv@PLT
.L649:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L650
	call	_ZdlPv@PLT
.L650:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L624:
	movq	264(%r12), %rdi
	leaq	280(%r12), %rax
	cmpq	%rax, %rdi
	je	.L651
	call	_ZdlPv@PLT
.L651:
	movq	216(%r12), %rdi
	leaq	232(%r12), %rax
	cmpq	%rax, %rdi
	je	.L652
	call	_ZdlPv@PLT
.L652:
	movq	168(%r12), %rdi
	leaq	184(%r12), %rax
	cmpq	%rax, %rdi
	je	.L653
	call	_ZdlPv@PLT
.L653:
	movq	152(%r12), %rdi
	testq	%rdi, %rdi
	je	.L654
	movq	(%rdi), %rax
	call	*24(%rax)
.L654:
	movq	112(%r12), %rdi
	leaq	128(%r12), %rax
	cmpq	%rax, %rdi
	je	.L655
	call	_ZdlPv@PLT
.L655:
	movq	64(%r12), %rdi
	leaq	80(%r12), %rax
	cmpq	%rax, %rdi
	je	.L656
	call	_ZdlPv@PLT
.L656:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L657
	call	_ZdlPv@PLT
.L657:
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L618:
	movq	-72(%rbp), %rax
	movq	144(%rax), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L658
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -200(%rbp)
	movq	%rax, -80(%rbp)
	cmpq	%rax, %rdx
	je	.L659
	.p2align 4,,10
	.p2align 3
.L1060:
	movq	-80(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -88(%rbp)
	testq	%rcx, %rcx
	je	.L660
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L661
	movq	112(%rcx), %r12
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger5ScopeE(%rip), %rax
	movq	%rax, (%rcx)
	testq	%r12, %r12
	je	.L662
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L663
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L664
	call	_ZdlPv@PLT
.L664:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L662:
	movq	-88(%rbp), %rax
	movq	104(%rax), %r12
	testq	%r12, %r12
	je	.L665
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L666
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L667
	call	_ZdlPv@PLT
.L667:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L665:
	movq	-88(%rbp), %rax
	movq	64(%rax), %rdi
	addq	$80, %rax
	cmpq	%rax, %rdi
	je	.L668
	call	_ZdlPv@PLT
.L668:
	movq	-88(%rbp), %rax
	movq	48(%rax), %rcx
	movq	%rcx, -64(%rbp)
	testq	%rcx, %rcx
	je	.L669
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L670
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rax
	movq	312(%rcx), %r12
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm3
	movq	%rdx, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%rcx)
	testq	%r12, %r12
	je	.L671
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L672
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	56(%r12), %rdi
	movq	%rax, (%r12)
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L673
	call	_ZdlPv@PLT
.L673:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L674
	call	_ZdlPv@PLT
.L674:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L671:
	movq	-64(%rbp), %rax
	movq	304(%rax), %rdx
	movq	%rdx, -104(%rbp)
	testq	%rdx, %rdx
	je	.L675
	movq	(%rdx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%rcx, -56(%rbp)
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L676
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rdx)
	movq	160(%rdx), %rax
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	je	.L677
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -208(%rbp)
	movq	%rax, -112(%rbp)
	cmpq	%rax, %rdx
	je	.L678
	.p2align 4,,10
	.p2align 3
.L935:
	movq	-112(%rbp), %rax
	movq	(%rax), %rdx
	movq	%rdx, -144(%rbp)
	testq	%rdx, %rdx
	je	.L679
	movq	(%rdx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L680
	movq	16(%rdx), %rbx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rdx)
	testq	%rbx, %rbx
	je	.L681
	movq	(%rbx), %rax
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L682
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L683
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -216(%rbp)
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rcx
	je	.L684
	movq	%rbx, -240(%rbp)
	.p2align 4,,10
	.p2align 3
.L766:
	movq	-120(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -136(%rbp)
	testq	%rcx, %rcx
	je	.L685
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L686
	movq	16(%rcx), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	testq	%r12, %r12
	je	.L687
	movq	(%r12), %rax
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L688
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L689
	movq	8(%r14), %rbx
	movq	(%r14), %r13
	cmpq	%r13, %rbx
	je	.L690
	movq	%r12, -128(%rbp)
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L1641:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L693
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L694
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L693:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L695
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L696
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L695:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L691:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L1640
.L697:
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L691
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1641
	addq	$8, %r13
	movq	%r15, %rdi
	call	*%rax
	cmpq	%r13, %rbx
	jne	.L697
	.p2align 4,,10
	.p2align 3
.L1640:
	movq	-128(%rbp), %r12
	movq	(%r14), %r13
.L690:
	testq	%r13, %r13
	je	.L698
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L698:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L689:
	movq	152(%r12), %r14
	testq	%r14, %r14
	je	.L699
	movq	8(%r14), %rbx
	movq	(%r14), %r13
	cmpq	%r13, %rbx
	je	.L700
	movq	%r12, -128(%rbp)
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L1643:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L703
	call	_ZdlPv@PLT
.L703:
	movq	136(%r15), %r12
	testq	%r12, %r12
	je	.L704
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L705
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L704:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L706
	call	_ZdlPv@PLT
.L706:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L707
	call	_ZdlPv@PLT
.L707:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L708
	call	_ZdlPv@PLT
.L708:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L701:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L1642
.L709:
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L701
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1643
	addq	$8, %r13
	movq	%r15, %rdi
	call	*%rax
	cmpq	%r13, %rbx
	jne	.L709
	.p2align 4,,10
	.p2align 3
.L1642:
	movq	-128(%rbp), %r12
	movq	(%r14), %r13
.L700:
	testq	%r13, %r13
	je	.L710
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L710:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L699:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L711
	call	_ZdlPv@PLT
.L711:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L712
	call	_ZdlPv@PLT
.L712:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L713
	call	_ZdlPv@PLT
.L713:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L687:
	movq	-136(%rbp), %rax
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	je	.L714
	movq	(%rbx), %rax
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L715
	movq	160(%rbx), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%r13, %r13
	je	.L716
	movq	8(%r13), %rax
	movq	0(%r13), %r12
	movq	%rax, -128(%rbp)
	cmpq	%r12, %rax
	jne	.L724
	testq	%r12, %r12
	je	.L725
	.p2align 4,,10
	.p2align 3
.L1678:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L725:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L716:
	movq	152(%rbx), %rax
	movq	%rax, -184(%rbp)
	testq	%rax, %rax
	je	.L726
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -224(%rbp)
	movq	%rax, -128(%rbp)
	cmpq	%rax, %rdx
	je	.L727
	movq	%rbx, -248(%rbp)
	.p2align 4,,10
	.p2align 3
.L761:
	movq	-128(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L728
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L729
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L730
	call	_ZdlPv@PLT
.L730:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L731
	movq	0(%r13), %rax
	movq	-56(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L732
	movq	160(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L733
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -232(%rbp)
	cmpq	%r14, %rax
	je	.L734
	movq	%r12, -256(%rbp)
	movq	%r13, -264(%rbp)
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L1645:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L737
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L738
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L737:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L739
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L740
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L739:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L735:
	addq	$8, %r14
	cmpq	%r14, -232(%rbp)
	je	.L1644
.L741:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L735
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1645
	movq	%r12, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -232(%rbp)
	jne	.L741
	.p2align 4,,10
	.p2align 3
.L1644:
	movq	-256(%rbp), %r12
	movq	-264(%rbp), %r13
	movq	(%r15), %r14
.L734:
	testq	%r14, %r14
	je	.L742
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L742:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L733:
	movq	152(%r13), %r15
	testq	%r15, %r15
	je	.L743
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L744
	movq	%r12, -232(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L1647:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L747
	call	_ZdlPv@PLT
.L747:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L748
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L749
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L748:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L750
	call	_ZdlPv@PLT
.L750:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L751
	call	_ZdlPv@PLT
.L751:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L752
	call	_ZdlPv@PLT
.L752:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L745:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L1646
.L753:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L745
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1647
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L753
	.p2align 4,,10
	.p2align 3
.L1646:
	movq	-232(%rbp), %r12
	movq	-256(%rbp), %r13
	movq	(%r15), %r14
.L744:
	testq	%r14, %r14
	je	.L754
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L754:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L743:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L755
	call	_ZdlPv@PLT
.L755:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L756
	call	_ZdlPv@PLT
.L756:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L757
	call	_ZdlPv@PLT
.L757:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L731:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L758
	call	_ZdlPv@PLT
.L758:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L759
	call	_ZdlPv@PLT
.L759:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L760
	call	_ZdlPv@PLT
.L760:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L728:
	addq	$8, -128(%rbp)
	movq	-128(%rbp), %rax
	cmpq	%rax, -224(%rbp)
	jne	.L761
	movq	-184(%rbp), %rax
	movq	-248(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -128(%rbp)
.L727:
	movq	-128(%rbp), %rax
	testq	%rax, %rax
	je	.L762
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L762:
	movq	-184(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L726:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L763
	call	_ZdlPv@PLT
.L763:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L764
	call	_ZdlPv@PLT
.L764:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L765
	call	_ZdlPv@PLT
.L765:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L714:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L685:
	addq	$8, -120(%rbp)
	movq	-120(%rbp), %rax
	cmpq	%rax, -216(%rbp)
	jne	.L766
	movq	-176(%rbp), %rax
	movq	-240(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -120(%rbp)
.L684:
	movq	-120(%rbp), %rax
	testq	%rax, %rax
	je	.L767
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L767:
	movq	-176(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L683:
	movq	152(%rbx), %rax
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L768
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -136(%rbp)
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rdx
	je	.L769
	movq	%rbx, -184(%rbp)
	.p2align 4,,10
	.p2align 3
.L803:
	movq	-120(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L770
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L771
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L772
	call	_ZdlPv@PLT
.L772:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L773
	movq	0(%r13), %rax
	movq	-56(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L774
	movq	160(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L775
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -176(%rbp)
	cmpq	%r14, %rax
	je	.L776
	movq	%r12, -216(%rbp)
	movq	%r13, -224(%rbp)
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L1649:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L779
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L780
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L779:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L781
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L782
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L781:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L777:
	addq	$8, %r14
	cmpq	%r14, -176(%rbp)
	je	.L1648
.L783:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L777
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1649
	movq	%r12, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -176(%rbp)
	jne	.L783
	.p2align 4,,10
	.p2align 3
.L1648:
	movq	-216(%rbp), %r12
	movq	-224(%rbp), %r13
	movq	(%r15), %r14
.L776:
	testq	%r14, %r14
	je	.L784
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L784:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L775:
	movq	152(%r13), %r15
	testq	%r15, %r15
	je	.L785
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -176(%rbp)
	cmpq	%r14, %rax
	je	.L786
	movq	%r12, -216(%rbp)
	jmp	.L795
	.p2align 4,,10
	.p2align 3
.L1651:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L789
	call	_ZdlPv@PLT
.L789:
	movq	136(%rbx), %r12
	testq	%r12, %r12
	je	.L790
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L791
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L790:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L792
	call	_ZdlPv@PLT
.L792:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L793
	call	_ZdlPv@PLT
.L793:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L794
	call	_ZdlPv@PLT
.L794:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L787:
	addq	$8, %r14
	cmpq	%r14, -176(%rbp)
	je	.L1650
.L795:
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.L787
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1651
	movq	%rbx, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -176(%rbp)
	jne	.L795
	.p2align 4,,10
	.p2align 3
.L1650:
	movq	-216(%rbp), %r12
	movq	(%r15), %r14
.L786:
	testq	%r14, %r14
	je	.L796
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L796:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L785:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L797
	call	_ZdlPv@PLT
.L797:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L798
	call	_ZdlPv@PLT
.L798:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L799
	call	_ZdlPv@PLT
.L799:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L773:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L800
	call	_ZdlPv@PLT
.L800:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L801
	call	_ZdlPv@PLT
.L801:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L802
	call	_ZdlPv@PLT
.L802:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L770:
	addq	$8, -120(%rbp)
	movq	-120(%rbp), %rax
	cmpq	%rax, -136(%rbp)
	jne	.L803
	movq	-128(%rbp), %rax
	movq	-184(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -120(%rbp)
.L769:
	movq	-120(%rbp), %rax
	testq	%rax, %rax
	je	.L804
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L804:
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L768:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L805
	call	_ZdlPv@PLT
.L805:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L806
	call	_ZdlPv@PLT
.L806:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L807
	call	_ZdlPv@PLT
.L807:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L681:
	movq	-144(%rbp), %rax
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	je	.L808
	movq	(%rbx), %rax
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L809
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L810
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -136(%rbp)
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rcx
	je	.L811
	movq	%rbx, -184(%rbp)
	.p2align 4,,10
	.p2align 3
.L868:
	movq	-120(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L812
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L813
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L814
	movq	(%r12), %rax
	movq	-56(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L815
	movq	160(%r12), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r15, %r15
	je	.L816
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -176(%rbp)
	cmpq	%r14, %rax
	je	.L817
	movq	%r13, -216(%rbp)
	movq	%r12, -224(%rbp)
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L1653:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L820
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L821
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L820:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L822
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L823
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L822:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L818:
	addq	$8, %r14
	cmpq	%r14, -176(%rbp)
	je	.L1652
.L824:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L818
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1653
	movq	%r12, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -176(%rbp)
	jne	.L824
	.p2align 4,,10
	.p2align 3
.L1652:
	movq	-216(%rbp), %r13
	movq	-224(%rbp), %r12
	movq	(%r15), %r14
.L817:
	testq	%r14, %r14
	je	.L825
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L825:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L816:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L826
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -176(%rbp)
	cmpq	%r14, %rax
	je	.L827
	movq	%r12, -216(%rbp)
	jmp	.L836
	.p2align 4,,10
	.p2align 3
.L1655:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L830
	call	_ZdlPv@PLT
.L830:
	movq	136(%rbx), %r12
	testq	%r12, %r12
	je	.L831
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L832
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L831:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L833
	call	_ZdlPv@PLT
.L833:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L834
	call	_ZdlPv@PLT
.L834:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L835
	call	_ZdlPv@PLT
.L835:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L828:
	addq	$8, %r14
	cmpq	%r14, -176(%rbp)
	je	.L1654
.L836:
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.L828
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1655
	movq	%rbx, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -176(%rbp)
	jne	.L836
	.p2align 4,,10
	.p2align 3
.L1654:
	movq	-216(%rbp), %r12
	movq	(%r15), %r14
.L827:
	testq	%r14, %r14
	je	.L837
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L837:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L826:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L838
	call	_ZdlPv@PLT
.L838:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L839
	call	_ZdlPv@PLT
.L839:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L840
	call	_ZdlPv@PLT
.L840:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L814:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L841
	movq	(%r12), %rax
	movq	-56(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L842
	movq	160(%r12), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r15, %r15
	je	.L843
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -176(%rbp)
	cmpq	%r14, %rax
	je	.L844
	movq	%r13, -216(%rbp)
	movq	%r12, -224(%rbp)
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L1657:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L847
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L848
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L847:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L849
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L850
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L849:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L845:
	addq	$8, %r14
	cmpq	%r14, -176(%rbp)
	je	.L1656
.L851:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L845
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1657
	movq	%r12, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -176(%rbp)
	jne	.L851
	.p2align 4,,10
	.p2align 3
.L1656:
	movq	-216(%rbp), %r13
	movq	-224(%rbp), %r12
	movq	(%r15), %r14
.L844:
	testq	%r14, %r14
	je	.L852
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L852:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L843:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L853
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -176(%rbp)
	cmpq	%r14, %rax
	je	.L854
	movq	%r12, -216(%rbp)
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L1659:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L857
	call	_ZdlPv@PLT
.L857:
	movq	136(%rbx), %r12
	testq	%r12, %r12
	je	.L858
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L859
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L858:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L860
	call	_ZdlPv@PLT
.L860:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L861
	call	_ZdlPv@PLT
.L861:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L862
	call	_ZdlPv@PLT
.L862:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L855:
	addq	$8, %r14
	cmpq	%r14, -176(%rbp)
	je	.L1658
.L863:
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.L855
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1659
	movq	%rbx, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -176(%rbp)
	jne	.L863
	.p2align 4,,10
	.p2align 3
.L1658:
	movq	-216(%rbp), %r12
	movq	(%r15), %r14
.L854:
	testq	%r14, %r14
	je	.L864
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L864:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L853:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L865
	call	_ZdlPv@PLT
.L865:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L866
	call	_ZdlPv@PLT
.L866:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L867
	call	_ZdlPv@PLT
.L867:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L841:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L812:
	addq	$8, -120(%rbp)
	movq	-120(%rbp), %rax
	cmpq	%rax, -136(%rbp)
	jne	.L868
	movq	-128(%rbp), %rax
	movq	-184(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -120(%rbp)
.L811:
	movq	-120(%rbp), %rax
	testq	%rax, %rax
	je	.L869
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L869:
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L810:
	movq	152(%rbx), %rax
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L870
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -216(%rbp)
	movq	%rax, -128(%rbp)
	cmpq	%rax, %rdx
	je	.L871
	movq	%rbx, -240(%rbp)
	.p2align 4,,10
	.p2align 3
.L930:
	movq	-128(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L872
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L873
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L874
	call	_ZdlPv@PLT
.L874:
	movq	136(%rbx), %rdx
	movq	%rdx, -120(%rbp)
	testq	%rdx, %rdx
	je	.L875
	movq	(%rdx), %rax
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L876
	movq	160(%rdx), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rdx)
	testq	%r13, %r13
	je	.L877
	movq	8(%r13), %rax
	movq	0(%r13), %r12
	movq	%rax, -136(%rbp)
	cmpq	%r12, %rax
	jne	.L885
	testq	%r12, %r12
	je	.L886
	.p2align 4,,10
	.p2align 3
.L1681:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L886:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L877:
	movq	-120(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -184(%rbp)
	testq	%rax, %rax
	je	.L887
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -224(%rbp)
	movq	%rax, -136(%rbp)
	cmpq	%rax, %rsi
	je	.L888
	movq	%rbx, -248(%rbp)
	.p2align 4,,10
	.p2align 3
.L922:
	movq	-136(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L889
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L890
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L891
	call	_ZdlPv@PLT
.L891:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L892
	movq	0(%r13), %rax
	movq	-56(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L893
	movq	160(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L894
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -232(%rbp)
	cmpq	%r14, %rax
	je	.L895
	movq	%r12, -256(%rbp)
	movq	%r13, -264(%rbp)
	jmp	.L902
	.p2align 4,,10
	.p2align 3
.L1661:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L898
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L899
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L898:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L900
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L901
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L900:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L896:
	addq	$8, %r14
	cmpq	%r14, -232(%rbp)
	je	.L1660
.L902:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L896
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1661
	movq	%r12, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -232(%rbp)
	jne	.L902
	.p2align 4,,10
	.p2align 3
.L1660:
	movq	-256(%rbp), %r12
	movq	-264(%rbp), %r13
	movq	(%r15), %r14
.L895:
	testq	%r14, %r14
	je	.L903
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L903:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L894:
	movq	152(%r13), %r15
	testq	%r15, %r15
	je	.L904
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L905
	movq	%r12, -232(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L914
	.p2align 4,,10
	.p2align 3
.L1663:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L908
	call	_ZdlPv@PLT
.L908:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L909
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L910
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L909:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L911
	call	_ZdlPv@PLT
.L911:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L912
	call	_ZdlPv@PLT
.L912:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L913
	call	_ZdlPv@PLT
.L913:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L906:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L1662
.L914:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L906
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1663
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L914
	.p2align 4,,10
	.p2align 3
.L1662:
	movq	-232(%rbp), %r12
	movq	-256(%rbp), %r13
	movq	(%r15), %r14
.L905:
	testq	%r14, %r14
	je	.L915
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L915:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L904:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L916
	call	_ZdlPv@PLT
.L916:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L917
	call	_ZdlPv@PLT
.L917:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L918
	call	_ZdlPv@PLT
.L918:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L892:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L919
	call	_ZdlPv@PLT
.L919:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L920
	call	_ZdlPv@PLT
.L920:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L921
	call	_ZdlPv@PLT
.L921:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L889:
	addq	$8, -136(%rbp)
	movq	-136(%rbp), %rax
	cmpq	%rax, -224(%rbp)
	jne	.L922
	movq	-184(%rbp), %rax
	movq	-248(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -136(%rbp)
.L888:
	movq	-136(%rbp), %rax
	testq	%rax, %rax
	je	.L923
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L923:
	movq	-184(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L887:
	movq	-120(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L924
	call	_ZdlPv@PLT
.L924:
	movq	-120(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L925
	call	_ZdlPv@PLT
.L925:
	movq	-120(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L926
	call	_ZdlPv@PLT
.L926:
	movq	-120(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L875:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L927
	call	_ZdlPv@PLT
.L927:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L928
	call	_ZdlPv@PLT
.L928:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L929
	call	_ZdlPv@PLT
.L929:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L872:
	addq	$8, -128(%rbp)
	movq	-128(%rbp), %rax
	cmpq	%rax, -216(%rbp)
	jne	.L930
	movq	-176(%rbp), %rax
	movq	-240(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -128(%rbp)
.L871:
	movq	-128(%rbp), %rax
	testq	%rax, %rax
	je	.L931
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L931:
	movq	-176(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L870:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L932
	call	_ZdlPv@PLT
.L932:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L933
	call	_ZdlPv@PLT
.L933:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L934
	call	_ZdlPv@PLT
.L934:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L808:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L679:
	addq	$8, -112(%rbp)
	movq	-112(%rbp), %rax
	cmpq	%rax, -208(%rbp)
	jne	.L935
	movq	-168(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -112(%rbp)
.L678:
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L936
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L936:
	movq	-168(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L677:
	movq	-104(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L937
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -168(%rbp)
	movq	%rax, -112(%rbp)
	cmpq	%rax, %rdx
	je	.L938
	.p2align 4,,10
	.p2align 3
.L1047:
	movq	-112(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L939
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L940
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L941
	call	_ZdlPv@PLT
.L941:
	movq	136(%rbx), %rcx
	movq	%rcx, -120(%rbp)
	testq	%rcx, %rcx
	je	.L942
	movq	(%rcx), %rax
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L943
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L944
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -176(%rbp)
	movq	%rax, -128(%rbp)
	cmpq	%rax, %rcx
	je	.L945
	movq	%rbx, -208(%rbp)
	.p2align 4,,10
	.p2align 3
.L1002:
	movq	-128(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L946
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L947
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L948
	movq	(%r12), %rax
	movq	-56(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L949
	movq	160(%r12), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r15, %r15
	je	.L950
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -184(%rbp)
	cmpq	%r14, %rax
	je	.L951
	movq	%r13, -216(%rbp)
	movq	%r12, -224(%rbp)
	jmp	.L958
	.p2align 4,,10
	.p2align 3
.L1665:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L954
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L955
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L954:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L956
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L957
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L956:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L952:
	addq	$8, %r14
	cmpq	%r14, -184(%rbp)
	je	.L1664
.L958:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L952
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1665
	movq	%r12, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -184(%rbp)
	jne	.L958
	.p2align 4,,10
	.p2align 3
.L1664:
	movq	-216(%rbp), %r13
	movq	-224(%rbp), %r12
	movq	(%r15), %r14
.L951:
	testq	%r14, %r14
	je	.L959
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L959:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L950:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L960
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -184(%rbp)
	cmpq	%r14, %rax
	je	.L961
	movq	%r12, -216(%rbp)
	jmp	.L970
	.p2align 4,,10
	.p2align 3
.L1667:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L964
	call	_ZdlPv@PLT
.L964:
	movq	136(%rbx), %r12
	testq	%r12, %r12
	je	.L965
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L966
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L965:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L967
	call	_ZdlPv@PLT
.L967:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L968
	call	_ZdlPv@PLT
.L968:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L969
	call	_ZdlPv@PLT
.L969:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L962:
	addq	$8, %r14
	cmpq	%r14, -184(%rbp)
	je	.L1666
.L970:
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.L962
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1667
	movq	%rbx, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -184(%rbp)
	jne	.L970
	.p2align 4,,10
	.p2align 3
.L1666:
	movq	-216(%rbp), %r12
	movq	(%r15), %r14
.L961:
	testq	%r14, %r14
	je	.L971
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L971:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L960:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L972
	call	_ZdlPv@PLT
.L972:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L973
	call	_ZdlPv@PLT
.L973:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L974
	call	_ZdlPv@PLT
.L974:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L948:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L975
	movq	(%r12), %rax
	movq	-56(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L976
	movq	160(%r12), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r15, %r15
	je	.L977
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -184(%rbp)
	cmpq	%r14, %rax
	je	.L978
	movq	%r13, -216(%rbp)
	movq	%r12, -224(%rbp)
	jmp	.L985
	.p2align 4,,10
	.p2align 3
.L1669:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L981
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L982
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L981:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L983
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L984
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L983:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L979:
	addq	$8, %r14
	cmpq	%r14, -184(%rbp)
	je	.L1668
.L985:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L979
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1669
	movq	%r12, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -184(%rbp)
	jne	.L985
	.p2align 4,,10
	.p2align 3
.L1668:
	movq	-216(%rbp), %r13
	movq	-224(%rbp), %r12
	movq	(%r15), %r14
.L978:
	testq	%r14, %r14
	je	.L986
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L986:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L977:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L987
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -184(%rbp)
	cmpq	%r14, %rax
	je	.L988
	movq	%r12, -216(%rbp)
	jmp	.L997
	.p2align 4,,10
	.p2align 3
.L1671:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L991
	call	_ZdlPv@PLT
.L991:
	movq	136(%rbx), %r12
	testq	%r12, %r12
	je	.L992
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L993
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L992:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L994
	call	_ZdlPv@PLT
.L994:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L995
	call	_ZdlPv@PLT
.L995:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L996
	call	_ZdlPv@PLT
.L996:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L989:
	addq	$8, %r14
	cmpq	%r14, -184(%rbp)
	je	.L1670
.L997:
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.L989
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1671
	movq	%rbx, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -184(%rbp)
	jne	.L997
	.p2align 4,,10
	.p2align 3
.L1670:
	movq	-216(%rbp), %r12
	movq	(%r15), %r14
.L988:
	testq	%r14, %r14
	je	.L998
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L998:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L987:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L999
	call	_ZdlPv@PLT
.L999:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1000
	call	_ZdlPv@PLT
.L1000:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1001
	call	_ZdlPv@PLT
.L1001:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L975:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L946:
	addq	$8, -128(%rbp)
	movq	-128(%rbp), %rax
	cmpq	%rax, -176(%rbp)
	jne	.L1002
	movq	-144(%rbp), %rax
	movq	-208(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -128(%rbp)
.L945:
	movq	-128(%rbp), %rax
	testq	%rax, %rax
	je	.L1003
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1003:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L944:
	movq	-120(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L1004
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -176(%rbp)
	movq	%rax, -128(%rbp)
	cmpq	%rax, %rdx
	je	.L1005
	movq	%rbx, -208(%rbp)
	.p2align 4,,10
	.p2align 3
.L1039:
	movq	-128(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L1006
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1007
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1008
	call	_ZdlPv@PLT
.L1008:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1009
	movq	0(%r13), %rax
	movq	-56(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1010
	movq	160(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L1011
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -184(%rbp)
	cmpq	%r14, %rax
	je	.L1012
	movq	%r12, -216(%rbp)
	movq	%r13, -224(%rbp)
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1673:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1015
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1016
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1015:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1017
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1018
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1017:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1013:
	addq	$8, %r14
	cmpq	%r14, -184(%rbp)
	je	.L1672
.L1019:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1013
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1673
	movq	%r12, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -184(%rbp)
	jne	.L1019
	.p2align 4,,10
	.p2align 3
.L1672:
	movq	-216(%rbp), %r12
	movq	-224(%rbp), %r13
	movq	(%r15), %r14
.L1012:
	testq	%r14, %r14
	je	.L1020
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1020:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1011:
	movq	152(%r13), %r15
	testq	%r15, %r15
	je	.L1021
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -184(%rbp)
	cmpq	%r14, %rax
	je	.L1022
	movq	%r12, -216(%rbp)
	jmp	.L1031
	.p2align 4,,10
	.p2align 3
.L1675:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1025
	call	_ZdlPv@PLT
.L1025:
	movq	136(%rbx), %r12
	testq	%r12, %r12
	je	.L1026
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L1027
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1026:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1028
	call	_ZdlPv@PLT
.L1028:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1029
	call	_ZdlPv@PLT
.L1029:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1030
	call	_ZdlPv@PLT
.L1030:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1023:
	addq	$8, %r14
	cmpq	%r14, -184(%rbp)
	je	.L1674
.L1031:
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.L1023
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1675
	movq	%rbx, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -184(%rbp)
	jne	.L1031
	.p2align 4,,10
	.p2align 3
.L1674:
	movq	-216(%rbp), %r12
	movq	(%r15), %r14
.L1022:
	testq	%r14, %r14
	je	.L1032
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1032:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1021:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1033
	call	_ZdlPv@PLT
.L1033:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1034
	call	_ZdlPv@PLT
.L1034:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1035
	call	_ZdlPv@PLT
.L1035:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1009:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1036
	call	_ZdlPv@PLT
.L1036:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1037
	call	_ZdlPv@PLT
.L1037:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1038
	call	_ZdlPv@PLT
.L1038:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1006:
	addq	$8, -128(%rbp)
	movq	-128(%rbp), %rax
	cmpq	%rax, -176(%rbp)
	jne	.L1039
	movq	-144(%rbp), %rax
	movq	-208(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -128(%rbp)
.L1005:
	movq	-128(%rbp), %rax
	testq	%rax, %rax
	je	.L1040
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1040:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1004:
	movq	-120(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L1041
	call	_ZdlPv@PLT
.L1041:
	movq	-120(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L1042
	call	_ZdlPv@PLT
.L1042:
	movq	-120(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1043
	call	_ZdlPv@PLT
.L1043:
	movq	-120(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L942:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1044
	call	_ZdlPv@PLT
.L1044:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1045
	call	_ZdlPv@PLT
.L1045:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1046
	call	_ZdlPv@PLT
.L1046:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L939:
	addq	$8, -112(%rbp)
	movq	-112(%rbp), %rax
	cmpq	%rax, -168(%rbp)
	jne	.L1047
	movq	-136(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -112(%rbp)
.L938:
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L1048
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1048:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L937:
	movq	-104(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L1049
	call	_ZdlPv@PLT
.L1049:
	movq	-104(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L1050
	call	_ZdlPv@PLT
.L1050:
	movq	-104(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1051
	call	_ZdlPv@PLT
.L1051:
	movq	-104(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L675:
	movq	-64(%rbp), %rax
	movq	264(%rax), %rdi
	addq	$280, %rax
	cmpq	%rax, %rdi
	je	.L1052
	call	_ZdlPv@PLT
.L1052:
	movq	-64(%rbp), %rax
	movq	216(%rax), %rdi
	addq	$232, %rax
	cmpq	%rax, %rdi
	je	.L1053
	call	_ZdlPv@PLT
.L1053:
	movq	-64(%rbp), %rax
	movq	168(%rax), %rdi
	addq	$184, %rax
	cmpq	%rax, %rdi
	je	.L1054
	call	_ZdlPv@PLT
.L1054:
	movq	-64(%rbp), %rax
	movq	152(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1055
	movq	(%rdi), %rax
	call	*24(%rax)
.L1055:
	movq	-64(%rbp), %rax
	movq	112(%rax), %rdi
	subq	$-128, %rax
	cmpq	%rax, %rdi
	je	.L1056
	call	_ZdlPv@PLT
.L1056:
	movq	-64(%rbp), %rax
	movq	64(%rax), %rdi
	addq	$80, %rax
	cmpq	%rax, %rdi
	je	.L1057
	call	_ZdlPv@PLT
.L1057:
	movq	-64(%rbp), %rax
	movq	16(%rax), %rdi
	addq	$32, %rax
	cmpq	%rax, %rdi
	je	.L1058
	call	_ZdlPv@PLT
.L1058:
	movq	-64(%rbp), %rdi
	movl	$320, %esi
	call	_ZdlPvm@PLT
.L669:
	movq	-88(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1059
	call	_ZdlPv@PLT
.L1059:
	movq	-88(%rbp), %rdi
	movl	$120, %esi
	call	_ZdlPvm@PLT
.L660:
	addq	$8, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	%rax, -200(%rbp)
	jne	.L1060
	movq	-152(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -80(%rbp)
.L659:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L1061
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1061:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L658:
	movq	-72(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L1062
	call	_ZdlPv@PLT
.L1062:
	movq	-72(%rbp), %rax
	movq	96(%rax), %r12
	testq	%r12, %r12
	je	.L1063
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1064
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1065
	call	_ZdlPv@PLT
.L1065:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1063:
	movq	-72(%rbp), %rax
	movq	88(%rax), %r12
	testq	%r12, %r12
	je	.L1066
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1067
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1068
	call	_ZdlPv@PLT
.L1068:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1066:
	movq	-72(%rbp), %rax
	movq	48(%rax), %rdi
	addq	$64, %rax
	cmpq	%rax, %rdi
	je	.L1069
	call	_ZdlPv@PLT
.L1069:
	movq	-72(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1070
	call	_ZdlPv@PLT
.L1070:
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L576:
	addq	$8, -96(%rbp)
	movq	-96(%rbp), %rax
	cmpq	%rax, -192(%rbp)
	jne	.L1071
	movq	-160(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -96(%rbp)
.L575:
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L1072
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1072:
	movq	-160(%rbp), %rdi
	addq	$232, %rsp
	movl	$24, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1677:
	.cfi_restore_state
	movq	16(%r15), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r14, %r14
	je	.L720
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L721
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L720:
	movq	8(%r15), %r14
	testq	%r14, %r14
	je	.L722
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L723
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L722:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L718:
	addq	$8, %r12
	cmpq	%r12, -128(%rbp)
	je	.L1676
.L724:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L718
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1677
	movq	%r15, %rdi
	addq	$8, %r12
	call	*%rax
	cmpq	%r12, -128(%rbp)
	jne	.L724
	.p2align 4,,10
	.p2align 3
.L1676:
	movq	0(%r13), %r12
	testq	%r12, %r12
	jne	.L1678
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L1680:
	movq	16(%r15), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r14, %r14
	je	.L881
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L882
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L881:
	movq	8(%r15), %r14
	testq	%r14, %r14
	je	.L883
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L884
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L883:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L879:
	addq	$8, %r12
	cmpq	%r12, -136(%rbp)
	je	.L1679
.L885:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L879
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1680
	movq	%r15, %rdi
	addq	$8, %r12
	call	*%rax
	cmpq	%r12, -136(%rbp)
	jne	.L885
	.p2align 4,,10
	.p2align 3
.L1679:
	movq	0(%r13), %r12
	testq	%r12, %r12
	jne	.L1681
	jmp	.L886
	.p2align 4,,10
	.p2align 3
.L670:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L940:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L939
	.p2align 4,,10
	.p2align 3
.L680:
	movq	%rdx, %rdi
	call	*%rax
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L577:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L666:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L663:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L661:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L672:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L676:
	movq	%rdx, %rdi
	call	*%rax
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L771:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L686:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L1007:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1006
	.p2align 4,,10
	.p2align 3
.L947:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L946
	.p2align 4,,10
	.p2align 3
.L873:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L813:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L812
	.p2align 4,,10
	.p2align 3
.L1067:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1066
	.p2align 4,,10
	.p2align 3
.L579:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L1064:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1063
	.p2align 4,,10
	.p2align 3
.L619:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L943:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L682:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L809:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L729:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L890:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L1010:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L688:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L976:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L715:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L876:
	movq	%rdx, %rdi
	call	*%rax
	jmp	.L875
	.p2align 4,,10
	.p2align 3
.L949:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L948
	.p2align 4,,10
	.p2align 3
.L815:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L842:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L841
	.p2align 4,,10
	.p2align 3
.L774:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L773
	.p2align 4,,10
	.p2align 3
.L585:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L581:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L625:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L621:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L882:
	call	*%rax
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L955:
	call	*%rax
	jmp	.L954
	.p2align 4,,10
	.p2align 3
.L780:
	call	*%rax
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L732:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L782:
	call	*%rax
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L957:
	call	*%rax
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L823:
	call	*%rax
	jmp	.L822
	.p2align 4,,10
	.p2align 3
.L723:
	call	*%rax
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L721:
	call	*%rax
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L694:
	call	*%rax
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L1018:
	call	*%rax
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L696:
	call	*%rax
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L884:
	call	*%rax
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L859:
	call	*%rax
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L993:
	call	*%rax
	jmp	.L992
	.p2align 4,,10
	.p2align 3
.L821:
	call	*%rax
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L893:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L982:
	call	*%rax
	jmp	.L981
	.p2align 4,,10
	.p2align 3
.L705:
	call	*%rax
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L832:
	call	*%rax
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L850:
	call	*%rax
	jmp	.L849
	.p2align 4,,10
	.p2align 3
.L966:
	call	*%rax
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L984:
	call	*%rax
	jmp	.L983
	.p2align 4,,10
	.p2align 3
.L791:
	call	*%rax
	jmp	.L790
	.p2align 4,,10
	.p2align 3
.L1027:
	call	*%rax
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L848:
	call	*%rax
	jmp	.L847
	.p2align 4,,10
	.p2align 3
.L1016:
	call	*%rax
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L593:
	call	*%rax
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L602:
	call	*%rax
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L591:
	call	*%rax
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L631:
	call	*%rax
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L633:
	call	*%rax
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L642:
	call	*%rax
	jmp	.L641
	.p2align 4,,10
	.p2align 3
.L910:
	call	*%rax
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L749:
	call	*%rax
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L738:
	call	*%rax
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L899:
	call	*%rax
	jmp	.L898
	.p2align 4,,10
	.p2align 3
.L901:
	call	*%rax
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L740:
	call	*%rax
	jmp	.L739
	.cfi_endproc
.LFE12655:
	.size	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0, .-_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0
	.section	.rodata._ZN12v8_inspector8protocol16ValueConversionsIbE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"boolean value expected"
	.section	.text._ZN12v8_inspector8protocol16ValueConversionsIbE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"axG",@progbits,_ZN12v8_inspector8protocol16ValueConversionsIbE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,comdat
	.p2align 4
	.weak	_ZN12v8_inspector8protocol16ValueConversionsIbE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol16ValueConversionsIbE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol16ValueConversionsIbE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB4371:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$0, -25(%rbp)
	testq	%rdi, %rdi
	je	.L1685
	movq	(%rdi), %rax
	leaq	-25(%rbp), %rsi
	call	*32(%rax)
	testb	%al, %al
	je	.L1685
.L1684:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	movzbl	-25(%rbp), %eax
	jne	.L1691
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1685:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	jmp	.L1684
.L1691:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4371:
	.size	_ZN12v8_inspector8protocol16ValueConversionsIbE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol16ValueConversionsIbE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImpl20setBreakpointsActiveEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"active"
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl20setBreakpointsActiveEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl20setBreakpointsActiveEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl20setBreakpointsActiveEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl20setBreakpointsActiveEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$120, %rsp
	movl	%esi, -132(%rbp)
	movq	(%r8), %r14
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -144(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L1693
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	jne	.L1694
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L1719
	call	_ZdlPv@PLT
.L1719:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	cmpq	%r15, %rdi
	je	.L1696
	call	_ZdlPv@PLT
.L1696:
	leaq	.LC4(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsIbE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	%rbx, %rdi
	movl	%eax, %r14d
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L1720
	leaq	-120(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r13), %rsi
	movzbl	%r14b, %edx
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*176(%rax)
	cmpl	$2, -112(%rbp)
	je	.L1721
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1701
	movl	-132(%rbp), %esi
	movq	%r12, %rdx
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L1701:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1702
.L1724:
	call	_ZdlPv@PLT
.L1702:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1692
	call	_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0
.L1692:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1722
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1693:
	.cfi_restore_state
	movq	-112(%rbp), %rdi
.L1694:
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L1723
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L1696
	.p2align 4,,10
	.p2align 3
.L1720:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	-132(%rbp), %esi
	movl	$-32602, %edx
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1692
	call	_ZdlPv@PLT
	jmp	.L1692
	.p2align 4,,10
	.p2align 3
.L1723:
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L1696
	.p2align 4,,10
	.p2align 3
.L1721:
	movq	8(%r13), %rdi
	movq	-152(%rbp), %rcx
	movq	-144(%rbp), %rdx
	movl	-132(%rbp), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L1724
	jmp	.L1702
.L1722:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6312:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl20setBreakpointsActiveEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl20setBreakpointsActiveEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImpl16setSkipAllPausesEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"skip"
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl16setSkipAllPausesEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl16setSkipAllPausesEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl16setSkipAllPausesEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl16setSkipAllPausesEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$120, %rsp
	movl	%esi, -132(%rbp)
	movq	(%r8), %r14
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -144(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L1726
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	jne	.L1727
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L1752
	call	_ZdlPv@PLT
.L1752:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	cmpq	%r15, %rdi
	je	.L1729
	call	_ZdlPv@PLT
.L1729:
	leaq	.LC5(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsIbE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	%rbx, %rdi
	movl	%eax, %r14d
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L1753
	leaq	-120(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r13), %rsi
	movzbl	%r14b, %edx
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*208(%rax)
	cmpl	$2, -112(%rbp)
	je	.L1754
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1734
	movl	-132(%rbp), %esi
	movq	%r12, %rdx
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L1734:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1735
.L1757:
	call	_ZdlPv@PLT
.L1735:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1725
	call	_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0
.L1725:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1755
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1726:
	.cfi_restore_state
	movq	-112(%rbp), %rdi
.L1727:
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L1756
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L1729
	.p2align 4,,10
	.p2align 3
.L1753:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	-132(%rbp), %esi
	movl	$-32602, %edx
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1725
	call	_ZdlPv@PLT
	jmp	.L1725
	.p2align 4,,10
	.p2align 3
.L1756:
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L1729
	.p2align 4,,10
	.p2align 3
.L1754:
	movq	8(%r13), %rdi
	movq	-152(%rbp), %rcx
	movq	-144(%rbp), %rdx
	movl	-132(%rbp), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L1757
	jmp	.L1735
.L1755:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6323:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl16setSkipAllPausesEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl16setSkipAllPausesEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImpl8stepIntoEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"breakOnAsyncCall"
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl8stepIntoEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl8stepIntoEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl8stepIntoEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl8stepIntoEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	leaq	.LC0(%rip), %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$120, %rsp
	movq	%rdx, -152(%rbp)
	movq	(%r8), %r15
	movq	%rcx, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	testq	%rax, %rax
	je	.L1760
	cmpl	$6, 8(%rax)
	jne	.L1760
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L1788
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movq	-136(%rbp), %rax
.L1763:
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%r15, %rdi
	je	.L1765
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %r8
.L1765:
	testq	%r8, %r8
	movq	%r8, -136(%rbp)
	je	.L1777
	leaq	.LC6(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-136(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsIbE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movb	$1, -137(%rbp)
	movb	%al, -136(%rbp)
.L1764:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L1789
	leaq	-120(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movzbl	-137(%rbp), %ecx
	leaq	-122(%rbp), %rdx
	movq	%r12, %rdi
	movq	184(%r13), %rsi
	movq	(%rsi), %rax
	movb	%cl, -122(%rbp)
	movzbl	-136(%rbp), %ecx
	movb	%cl, -121(%rbp)
	call	*224(%rax)
	cmpl	$2, -112(%rbp)
	je	.L1790
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1770
	movq	%r12, %rdx
	movl	%r14d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L1770:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1771
	call	_ZdlPv@PLT
.L1771:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1758
	call	_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0
.L1758:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1791
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1788:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movq	-136(%rbp), %rax
	jmp	.L1763
	.p2align 4,,10
	.p2align 3
.L1760:
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L1792
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L1777:
	movb	$0, -136(%rbp)
	movb	$0, -137(%rbp)
	jmp	.L1764
	.p2align 4,,10
	.p2align 3
.L1789:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	$-32602, %edx
	movl	%r14d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1758
	call	_ZdlPv@PLT
	jmp	.L1758
	.p2align 4,,10
	.p2align 3
.L1792:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movb	$0, -136(%rbp)
	movb	$0, -137(%rbp)
	jmp	.L1764
	.p2align 4,,10
	.p2align 3
.L1790:
	movq	8(%r13), %rdi
	movq	-160(%rbp), %rcx
	movl	%r14d, %esi
	movq	-152(%rbp), %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L1770
.L1791:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6325:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl8stepIntoEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl8stepIntoEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"integer value expected"
	.section	.text._ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"axG",@progbits,_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,comdat
	.p2align 4
	.weak	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB4373:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	testq	%rdi, %rdi
	je	.L1796
	movq	(%rdi), %rax
	leaq	-28(%rbp), %rsi
	call	*48(%rax)
	testb	%al, %al
	je	.L1796
.L1795:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	movl	-28(%rbp), %eax
	jne	.L1802
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1796:
	.cfi_restore_state
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	jmp	.L1795
.L1802:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4373:
	.size	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImpl22setAsyncCallStackDepthEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"maxDepth"
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl22setAsyncCallStackDepthEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl22setAsyncCallStackDepthEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl22setAsyncCallStackDepthEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl22setAsyncCallStackDepthEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$120, %rsp
	movl	%esi, -132(%rbp)
	movq	(%r8), %r14
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -144(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L1804
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	jne	.L1805
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L1830
	call	_ZdlPv@PLT
.L1830:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	cmpq	%r15, %rdi
	je	.L1807
	call	_ZdlPv@PLT
.L1807:
	leaq	.LC8(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	%rbx, %rdi
	movl	%eax, %r14d
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L1831
	leaq	-120(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r13), %rsi
	movl	%r14d, %edx
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*120(%rax)
	cmpl	$2, -112(%rbp)
	je	.L1832
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1812
	movl	-132(%rbp), %esi
	movq	%r12, %rdx
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L1812:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1813
.L1835:
	call	_ZdlPv@PLT
.L1813:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L1803
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1803:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1833
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1804:
	.cfi_restore_state
	movq	-112(%rbp), %rdi
.L1805:
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L1834
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L1807
	.p2align 4,,10
	.p2align 3
.L1831:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	-132(%rbp), %esi
	movl	$-32602, %edx
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1803
	call	_ZdlPv@PLT
	jmp	.L1803
	.p2align 4,,10
	.p2align 3
.L1834:
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L1807
	.p2align 4,,10
	.p2align 3
.L1832:
	movq	8(%r13), %rdi
	movq	-152(%rbp), %rcx
	movq	-144(%rbp), %rdx
	movl	-132(%rbp), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L1835
	jmp	.L1813
.L1833:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6275:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl22setAsyncCallStackDepthEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl22setAsyncCallStackDepthEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"string value expected"
	.section	.text._ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"axG",@progbits,_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,comdat
	.p2align 4
	.weak	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB4377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	movq	$0, 8(%rdi)
	movw	%ax, 16(%rdi)
	movq	$0, 32(%rdi)
	testq	%rsi, %rsi
	je	.L1839
	movq	(%rsi), %rax
	movq	%rsi, %rdi
	movq	%r12, %rsi
	call	*56(%rax)
	testb	%al, %al
	je	.L1839
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1839:
	.cfi_restore_state
	movq	%r13, %rdi
	leaq	.LC9(%rip), %rsi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4377:
	.size	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImpl16removeBreakpointEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"breakpointId"
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl16removeBreakpointEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl16removeBreakpointEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl16removeBreakpointEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl16removeBreakpointEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6219:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$168, %rsp
	movl	%esi, -188(%rbp)
	movq	(%r8), %r14
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -200(%rbp)
	movq	%rcx, -208(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L1845
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	jne	.L1846
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L1873
	call	_ZdlPv@PLT
.L1873:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, -184(%rbp)
	cmpq	%r15, %rdi
	je	.L1848
	call	_ZdlPv@PLT
.L1848:
	leaq	.LC10(%rip), %rsi
	movq	%rbx, %rdi
	leaq	-160(%rbp), %r14
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-184(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L1874
	leaq	-168(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r13), %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*88(%rax)
	cmpl	$2, -112(%rbp)
	je	.L1875
	movq	-168(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1853
	movl	-188(%rbp), %esi
	movq	%r12, %rdx
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L1853:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1854
.L1878:
	call	_ZdlPv@PLT
.L1854:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1851
	call	_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0
.L1851:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1844
	call	_ZdlPv@PLT
.L1844:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1876
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1845:
	.cfi_restore_state
	movq	-112(%rbp), %rdi
.L1846:
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L1877
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movq	$0, -184(%rbp)
	jmp	.L1848
	.p2align 4,,10
	.p2align 3
.L1874:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	-188(%rbp), %esi
	movl	$-32602, %edx
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1851
	call	_ZdlPv@PLT
	jmp	.L1851
	.p2align 4,,10
	.p2align 3
.L1877:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movq	$0, -184(%rbp)
	jmp	.L1848
	.p2align 4,,10
	.p2align 3
.L1875:
	movq	8(%r13), %rdi
	movq	-208(%rbp), %rcx
	movq	-200(%rbp), %rdx
	movl	-188(%rbp), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L1878
	jmp	.L1854
.L1876:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6219:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl16removeBreakpointEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl16removeBreakpointEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImpl20setPauseOnExceptionsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"state"
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl20setPauseOnExceptionsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl20setPauseOnExceptionsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl20setPauseOnExceptionsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl20setPauseOnExceptionsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$168, %rsp
	movl	%esi, -188(%rbp)
	movq	(%r8), %r14
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -200(%rbp)
	movq	%rcx, -208(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L1880
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	jne	.L1881
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L1908
	call	_ZdlPv@PLT
.L1908:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, -184(%rbp)
	cmpq	%r15, %rdi
	je	.L1883
	call	_ZdlPv@PLT
.L1883:
	leaq	.LC11(%rip), %rsi
	movq	%rbx, %rdi
	leaq	-160(%rbp), %r14
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-184(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L1909
	leaq	-168(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r13), %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*184(%rax)
	cmpl	$2, -112(%rbp)
	je	.L1910
	movq	-168(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1888
	movl	-188(%rbp), %esi
	movq	%r12, %rdx
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L1888:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1889
.L1913:
	call	_ZdlPv@PLT
.L1889:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1886
	call	_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0
.L1886:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1879
	call	_ZdlPv@PLT
.L1879:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1911
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1880:
	.cfi_restore_state
	movq	-112(%rbp), %rdi
.L1881:
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L1912
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movq	$0, -184(%rbp)
	jmp	.L1883
	.p2align 4,,10
	.p2align 3
.L1909:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	-188(%rbp), %esi
	movl	$-32602, %edx
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1886
	call	_ZdlPv@PLT
	jmp	.L1886
	.p2align 4,,10
	.p2align 3
.L1912:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movq	$0, -184(%rbp)
	jmp	.L1883
	.p2align 4,,10
	.p2align 3
.L1910:
	movq	8(%r13), %rdi
	movq	-208(%rbp), %rcx
	movq	-200(%rbp), %rdx
	movl	-188(%rbp), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L1913
	jmp	.L1889
.L1911:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6313:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl20setPauseOnExceptionsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl20setPauseOnExceptionsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImpl16setVariableValueEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"scopeNumber"
.LC13:
	.string	"variableName"
.LC14:
	.string	"callFrameId"
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl16setVariableValueEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl16setVariableValueEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl16setVariableValueEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl16setVariableValueEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$248, %rsp
	movl	%esi, -252(%rbp)
	movq	(%r8), %r13
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -272(%rbp)
	movq	%rcx, -280(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L1915
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r13
	jne	.L1916
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L1973
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L1919:
	leaq	.LC12(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, -248(%rbp)
	cmpq	%r15, %rdi
	je	.L1921
	call	_ZdlPv@PLT
.L1921:
	leaq	.LC12(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-248(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, -256(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, -248(%rbp)
	cmpq	%r15, %rdi
	je	.L1922
	call	_ZdlPv@PLT
.L1922:
	leaq	.LC13(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-208(%rbp), %rax
	movq	-248(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, -248(%rbp)
	cmpq	%r15, %rdi
	je	.L1923
	call	_ZdlPv@PLT
.L1923:
	leaq	.LC1(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-248(%rbp), %rsi
	movq	%rbx, %rdx
	leaq	-232(%rbp), %rdi
	call	_ZN12v8_inspector8protocol7Runtime12CallArgument9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE@PLT
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%r15, %rdi
	je	.L1920
	movq	%r8, -248(%rbp)
	call	_ZdlPv@PLT
	movq	-248(%rbp), %r8
.L1920:
	leaq	.LC14(%rip), %rsi
	movq	%rbx, %rdi
	movq	%r8, -248(%rbp)
	leaq	-160(%rbp), %r13
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-248(%rbp), %r8
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L1974
	leaq	-224(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r14), %rsi
	movq	%r13, %r9
	movq	-232(%rbp), %rdx
	movq	-264(%rbp), %rcx
	leaq	-216(%rbp), %r8
	movq	%r12, %rdi
	movq	(%rsi), %rax
	movq	%rdx, -216(%rbp)
	movq	$0, -232(%rbp)
	movl	-256(%rbp), %edx
	call	*216(%rax)
	movq	-216(%rbp), %r13
	testq	%r13, %r13
	je	.L1927
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12CallArgumentD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1928
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12CallArgumentE(%rip), %rax
	movq	72(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	88(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1929
	call	_ZdlPv@PLT
.L1929:
	movq	24(%r13), %rdi
	leaq	40(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1930
	call	_ZdlPv@PLT
.L1930:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1931
	movq	(%rdi), %rax
	call	*24(%rax)
.L1931:
	movl	$112, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1927:
	cmpl	$2, -112(%rbp)
	je	.L1975
	movq	-224(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1933
	movl	-252(%rbp), %esi
	movq	%r12, %rdx
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L1933:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1934
	call	_ZdlPv@PLT
.L1934:
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1926
	call	_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0
.L1926:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1936
	call	_ZdlPv@PLT
.L1936:
	movq	-232(%rbp), %r12
	testq	%r12, %r12
	je	.L1937
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12CallArgumentD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1938
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12CallArgumentE(%rip), %rax
	movq	72(%r12), %rdi
	movq	%rax, (%r12)
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1939
	call	_ZdlPv@PLT
.L1939:
	movq	24(%r12), %rdi
	leaq	40(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1940
	call	_ZdlPv@PLT
.L1940:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1941
	movq	(%rdi), %rax
	call	*24(%rax)
.L1941:
	movl	$112, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1937:
	movq	-208(%rbp), %rdi
	leaq	-192(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1914
	call	_ZdlPv@PLT
.L1914:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1976
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1973:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L1919
	.p2align 4,,10
	.p2align 3
.L1915:
	movq	-112(%rbp), %rdi
.L1916:
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L1977
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L1918:
	leaq	.LC12(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%rbx, %rsi
	xorl	%edi, %edi
	call	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC13(%rip), %rsi
	movq	%rbx, %rdi
	movl	%eax, -256(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-208(%rbp), %rax
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC1(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-232(%rbp), %rdi
	movq	%rbx, %rdx
	xorl	%esi, %esi
	call	_ZN12v8_inspector8protocol7Runtime12CallArgument9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE@PLT
	xorl	%r8d, %r8d
	jmp	.L1920
	.p2align 4,,10
	.p2align 3
.L1974:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	-252(%rbp), %esi
	movl	$-32602, %edx
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1926
	call	_ZdlPv@PLT
	jmp	.L1926
	.p2align 4,,10
	.p2align 3
.L1977:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L1918
	.p2align 4,,10
	.p2align 3
.L1938:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1937
	.p2align 4,,10
	.p2align 3
.L1975:
	movq	8(%r14), %rdi
	movq	-280(%rbp), %rcx
	movq	-272(%rbp), %rdx
	movl	-252(%rbp), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L1933
	.p2align 4,,10
	.p2align 3
.L1928:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1927
.L1976:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6324:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl16setVariableValueEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl16setVariableValueEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol16ValueConversionsINS0_15DictionaryValueEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"object expected"
	.section	.text._ZN12v8_inspector8protocol16ValueConversionsINS0_15DictionaryValueEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"axG",@progbits,_ZN12v8_inspector8protocol16ValueConversionsINS0_15DictionaryValueEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,comdat
	.p2align 4
	.weak	_ZN12v8_inspector8protocol16ValueConversionsINS0_15DictionaryValueEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol16ValueConversionsINS0_15DictionaryValueEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol16ValueConversionsINS0_15DictionaryValueEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB4388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1979
	cmpl	$6, 8(%rsi)
	je	.L1980
.L1979:
	leaq	.LC15(%rip), %rsi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L1980:
	movq	(%r12), %rax
	leaq	-32(%rbp), %rdi
	movq	%r12, %rsi
	call	*88(%rax)
	movq	-32(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1981
	cmpl	$6, 8(%rdx)
	movl	$0, %eax
	cmovne	%rax, %rdx
.L1981:
	movq	%rdx, 0(%r13)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1991
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1991:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4388:
	.size	_ZN12v8_inspector8protocol16ValueConversionsINS0_15DictionaryValueEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol16ValueConversionsINS0_15DictionaryValueEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol16InternalResponseD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol16InternalResponseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol16InternalResponseD2Ev
	.type	_ZN12v8_inspector8protocol16InternalResponseD2Ev, @function
_ZN12v8_inspector8protocol16InternalResponseD2Ev:
.LFB4410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1993
	movq	(%rdi), %rax
	call	*24(%rax)
.L1993:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L1992
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1992:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4410:
	.size	_ZN12v8_inspector8protocol16InternalResponseD2Ev, .-_ZN12v8_inspector8protocol16InternalResponseD2Ev
	.weak	_ZN12v8_inspector8protocol16InternalResponseD1Ev
	.set	_ZN12v8_inspector8protocol16InternalResponseD1Ev,_ZN12v8_inspector8protocol16InternalResponseD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev, @function
_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev:
.LFB4457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rax
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	312(%rdi), %r12
	movq	%rdi, %rbx
	movups	%xmm0, (%rdi)
	testq	%r12, %r12
	je	.L2000
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2001
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	56(%r12), %rdi
	movq	%rax, (%r12)
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2002
	call	_ZdlPv@PLT
.L2002:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2003
	call	_ZdlPv@PLT
.L2003:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2000:
	movq	304(%rbx), %r12
	testq	%r12, %r12
	je	.L2004
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2005
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2004:
	movq	264(%rbx), %rdi
	leaq	280(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2006
	call	_ZdlPv@PLT
.L2006:
	movq	216(%rbx), %rdi
	leaq	232(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2007
	call	_ZdlPv@PLT
.L2007:
	movq	168(%rbx), %rdi
	leaq	184(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2008
	call	_ZdlPv@PLT
.L2008:
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2009
	movq	(%rdi), %rax
	call	*24(%rax)
.L2009:
	movq	112(%rbx), %rdi
	leaq	128(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2010
	call	_ZdlPv@PLT
.L2010:
	movq	64(%rbx), %rdi
	leaq	80(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2011
	call	_ZdlPv@PLT
.L2011:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L1999
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1999:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2001:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2000
	.p2align 4,,10
	.p2align 3
.L2005:
	call	*%rax
	jmp	.L2004
	.cfi_endproc
.LFE4457:
	.size	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev, .-_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.set	.LTHUNK0,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev:
.LFB12813:
	.cfi_startproc
	endbr64
	subq	$8, %rdi
	jmp	.LTHUNK0
	.cfi_endproc
.LFE12813:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	.weak	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, @function
_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev:
.LFB4459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$320, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4459:
	.size	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, .-_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger5ScopeD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger5ScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger5ScopeD2Ev
	.type	_ZN12v8_inspector8protocol8Debugger5ScopeD2Ev, @function
_ZN12v8_inspector8protocol8Debugger5ScopeD2Ev:
.LFB5654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger5ScopeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	112(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L2026
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2027
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2028
	call	_ZdlPv@PLT
.L2028:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2026:
	movq	104(%rbx), %r12
	testq	%r12, %r12
	je	.L2029
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2030
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2031
	call	_ZdlPv@PLT
.L2031:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2029:
	movq	64(%rbx), %rdi
	leaq	80(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2032
	call	_ZdlPv@PLT
.L2032:
	movq	48(%rbx), %r12
	testq	%r12, %r12
	je	.L2033
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2034
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2033:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L2025
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L2025:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2034:
	.cfi_restore_state
	call	*%rax
	jmp	.L2033
	.p2align 4,,10
	.p2align 3
.L2027:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2026
	.p2align 4,,10
	.p2align 3
.L2030:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2029
	.cfi_endproc
.LFE5654:
	.size	_ZN12v8_inspector8protocol8Debugger5ScopeD2Ev, .-_ZN12v8_inspector8protocol8Debugger5ScopeD2Ev
	.weak	_ZN12v8_inspector8protocol8Debugger5ScopeD1Ev
	.set	_ZN12v8_inspector8protocol8Debugger5ScopeD1Ev,_ZN12v8_inspector8protocol8Debugger5ScopeD2Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger9CallFrameD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger9CallFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger9CallFrameD2Ev
	.type	_ZN12v8_inspector8protocol8Debugger9CallFrameD2Ev, @function
_ZN12v8_inspector8protocol8Debugger9CallFrameD2Ev:
.LFB5576:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger9CallFrameE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	160(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L2047
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2048
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2047:
	movq	152(%rbx), %r12
	testq	%r12, %r12
	je	.L2049
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2050
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2049:
	movq	144(%rbx), %r14
	testq	%r14, %r14
	je	.L2051
	movq	8(%r14), %rax
	movq	(%r14), %r13
	movq	%rax, -64(%rbp)
	cmpq	%r13, %rax
	je	.L2052
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %r15
	jmp	.L2065
	.p2align 4,,10
	.p2align 3
.L2109:
	movq	112(%r12), %r8
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger5ScopeE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r8, %r8
	je	.L2055
	movq	(%r8), %rax
	movq	24(%rax), %rax
	cmpq	%r15, %rax
	jne	.L2056
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r8), %rdi
	movq	%rax, (%r8)
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L2057
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L2057:
	movl	$64, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L2055:
	movq	104(%r12), %r8
	testq	%r8, %r8
	je	.L2058
	movq	(%r8), %rax
	movq	24(%rax), %rax
	cmpq	%r15, %rax
	jne	.L2059
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r8), %rdi
	movq	%rax, (%r8)
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L2060
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L2060:
	movl	$64, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L2058:
	movq	64(%r12), %rdi
	leaq	80(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2061
	call	_ZdlPv@PLT
.L2061:
	movq	48(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2062
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2063
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movq	-56(%rbp), %rdi
	movl	$320, %esi
	call	_ZdlPvm@PLT
.L2062:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2064
	call	_ZdlPv@PLT
.L2064:
	movl	$120, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2053:
	addq	$8, %r13
	cmpq	%r13, -64(%rbp)
	je	.L2108
.L2065:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L2053
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2109
	movq	%r12, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -64(%rbp)
	jne	.L2065
	.p2align 4,,10
	.p2align 3
.L2108:
	movq	(%r14), %r13
.L2052:
	testq	%r13, %r13
	je	.L2066
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2066:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2051:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2067
	call	_ZdlPv@PLT
.L2067:
	movq	96(%rbx), %r12
	testq	%r12, %r12
	je	.L2068
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2069
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2070
	call	_ZdlPv@PLT
.L2070:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2068:
	movq	88(%rbx), %r12
	testq	%r12, %r12
	je	.L2071
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2072
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2073
	call	_ZdlPv@PLT
.L2073:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2071:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2074
	call	_ZdlPv@PLT
.L2074:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L2046
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L2046:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2063:
	.cfi_restore_state
	call	*%rax
	jmp	.L2062
	.p2align 4,,10
	.p2align 3
.L2059:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L2058
	.p2align 4,,10
	.p2align 3
.L2056:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L2055
	.p2align 4,,10
	.p2align 3
.L2050:
	call	*%rax
	jmp	.L2049
	.p2align 4,,10
	.p2align 3
.L2069:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2068
	.p2align 4,,10
	.p2align 3
.L2072:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2071
	.p2align 4,,10
	.p2align 3
.L2048:
	call	*%rax
	jmp	.L2047
	.cfi_endproc
.LFE5576:
	.size	_ZN12v8_inspector8protocol8Debugger9CallFrameD2Ev, .-_ZN12v8_inspector8protocol8Debugger9CallFrameD2Ev
	.weak	_ZN12v8_inspector8protocol8Debugger9CallFrameD1Ev
	.set	_ZN12v8_inspector8protocol8Debugger9CallFrameD1Ev,_ZN12v8_inspector8protocol8Debugger9CallFrameD2Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger9CallFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev
	.type	_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev, @function
_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev:
.LFB5578:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN12v8_inspector8protocol8Debugger9CallFrameD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$168, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5578:
	.size	_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev, .-_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger5ScopeD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger5ScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev
	.type	_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev, @function
_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev:
.LFB5656:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger5ScopeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	112(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L2113
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2114
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2115
	call	_ZdlPv@PLT
.L2115:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2113:
	movq	104(%r12), %r13
	testq	%r13, %r13
	je	.L2116
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2117
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2118
	call	_ZdlPv@PLT
.L2118:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2116:
	movq	64(%r12), %rdi
	leaq	80(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2119
	call	_ZdlPv@PLT
.L2119:
	movq	48(%r12), %r13
	testq	%r13, %r13
	je	.L2120
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2121
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2120:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2122
	call	_ZdlPv@PLT
.L2122:
	movq	%r12, %rdi
	movl	$120, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L2117:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2116
	.p2align 4,,10
	.p2align 3
.L2121:
	call	*%rax
	jmp	.L2120
	.p2align 4,,10
	.p2align 3
.L2114:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2113
	.cfi_endproc
.LFE5656:
	.size	_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev, .-_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev:
.LFB12778:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$320, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12778:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime10StackTraceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev, @function
_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev:
.LFB5165:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime10StackTraceE(%rip), %rax
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	80(%rdi), %r12
	movups	%xmm0, (%rdi)
	testq	%r12, %r12
	je	.L2136
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2137
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r12), %rdi
	leaq	-64(%rax), %rsi
	movq	%rax, %xmm2
	leaq	80(%r12), %rax
	movq	%rsi, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r12)
	cmpq	%rax, %rdi
	je	.L2138
	call	_ZdlPv@PLT
.L2138:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2139
	call	_ZdlPv@PLT
.L2139:
	movl	$104, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2136:
	movq	72(%rbx), %r12
	testq	%r12, %r12
	je	.L2140
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2141
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2140:
	movq	64(%rbx), %r13
	testq	%r13, %r13
	je	.L2142
	movq	8(%r13), %r14
	movq	0(%r13), %r12
	cmpq	%r12, %r14
	jne	.L2149
	testq	%r12, %r12
	je	.L2150
	.p2align 4,,10
	.p2align 3
.L2171:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2150:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2142:
	movq	24(%rbx), %rdi
	addq	$40, %rbx
	cmpq	%rbx, %rdi
	je	.L2135
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L2170:
	.cfi_restore_state
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r15), %rdi
	movq	%rax, (%r15)
	leaq	104(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2146
	call	_ZdlPv@PLT
.L2146:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2147
	call	_ZdlPv@PLT
.L2147:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2148
	call	_ZdlPv@PLT
.L2148:
	movl	$136, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2144:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L2169
.L2149:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L2144
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2170
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rax
	cmpq	%r12, %r14
	jne	.L2149
	.p2align 4,,10
	.p2align 3
.L2169:
	movq	0(%r13), %r12
	testq	%r12, %r12
	jne	.L2171
	jmp	.L2150
	.p2align 4,,10
	.p2align 3
.L2135:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2141:
	.cfi_restore_state
	call	*%rax
	jmp	.L2140
	.p2align 4,,10
	.p2align 3
.L2137:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2136
	.cfi_endproc
.LFE5165:
	.size	_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev, .-_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev
	.set	.LTHUNK2,_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD1Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD1Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD1Ev:
.LFB12819:
	.cfi_startproc
	endbr64
	subq	$8, %rdi
	jmp	.LTHUNK2
	.cfi_endproc
.LFE12819:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD1Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD1Ev
	.weak	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev,_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime10StackTraceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev, @function
_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev:
.LFB5167:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$88, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5167:
	.size	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev, .-_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev, @function
_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev:
.LFB5038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	168(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L2175
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2176
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2175:
	movq	160(%r12), %r13
	testq	%r13, %r13
	je	.L2177
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2178
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2177:
	movq	120(%r12), %rdi
	leaq	136(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2179
	call	_ZdlPv@PLT
.L2179:
	movq	72(%r12), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2180
	call	_ZdlPv@PLT
.L2180:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2181
	call	_ZdlPv@PLT
.L2181:
	movq	%r12, %rdi
	movl	$184, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L2176:
	.cfi_restore_state
	call	*%rax
	jmp	.L2175
	.p2align 4,,10
	.p2align 3
.L2178:
	call	*%rax
	jmp	.L2177
	.cfi_endproc
.LFE5038:
	.size	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev, .-_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationD0Ev
	.type	_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationD0Ev, @function
_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationD0Ev:
.LFB5880:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	224(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L2190
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2191
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2190:
	movq	168(%r12), %rdi
	leaq	184(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2192
	call	_ZdlPv@PLT
.L2192:
	movq	152(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2193
	movq	(%rdi), %rax
	call	*24(%rax)
.L2193:
	movq	112(%r12), %rdi
	leaq	128(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2194
	call	_ZdlPv@PLT
.L2194:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2195
	call	_ZdlPv@PLT
.L2195:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2196
	call	_ZdlPv@PLT
.L2196:
	movq	%r12, %rdi
	movl	$232, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L2191:
	.cfi_restore_state
	call	*%rax
	jmp	.L2190
	.cfi_endproc
.LFE5880:
	.size	_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationD0Ev, .-_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationD0Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger24ScriptParsedNotificationD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotificationD0Ev
	.type	_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotificationD0Ev, @function
_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotificationD0Ev:
.LFB5950:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger24ScriptParsedNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	232(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L2205
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2206
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2205:
	movq	176(%r12), %rdi
	leaq	192(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2207
	call	_ZdlPv@PLT
.L2207:
	movq	152(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2208
	movq	(%rdi), %rax
	call	*24(%rax)
.L2208:
	movq	112(%r12), %rdi
	leaq	128(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2209
	call	_ZdlPv@PLT
.L2209:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2210
	call	_ZdlPv@PLT
.L2210:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2211
	call	_ZdlPv@PLT
.L2211:
	movq	%r12, %rdi
	movl	$240, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L2206:
	.cfi_restore_state
	call	*%rax
	jmp	.L2205
	.cfi_endproc
.LFE5950:
	.size	_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotificationD0Ev, .-_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotificationD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev, @function
_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev:
.LFB5036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	168(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L2220
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2221
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2220:
	movq	160(%rbx), %r12
	testq	%r12, %r12
	je	.L2222
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2223
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2222:
	movq	120(%rbx), %rdi
	leaq	136(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2224
	call	_ZdlPv@PLT
.L2224:
	movq	72(%rbx), %rdi
	leaq	88(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2225
	call	_ZdlPv@PLT
.L2225:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L2219
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L2219:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2223:
	.cfi_restore_state
	call	*%rax
	jmp	.L2222
	.p2align 4,,10
	.p2align 3
.L2221:
	call	*%rax
	jmp	.L2220
	.cfi_endproc
.LFE5036:
	.size	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev, .-_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD1Ev,_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger24ScriptParsedNotificationD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotificationD2Ev
	.type	_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotificationD2Ev, @function
_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotificationD2Ev:
.LFB5948:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger24ScriptParsedNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	232(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L2235
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2236
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2235:
	movq	176(%rbx), %rdi
	leaq	192(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2237
	call	_ZdlPv@PLT
.L2237:
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2238
	movq	(%rdi), %rax
	call	*24(%rax)
.L2238:
	movq	112(%rbx), %rdi
	leaq	128(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2239
	call	_ZdlPv@PLT
.L2239:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2240
	call	_ZdlPv@PLT
.L2240:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L2234
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L2234:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2236:
	.cfi_restore_state
	call	*%rax
	jmp	.L2235
	.cfi_endproc
.LFE5948:
	.size	_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotificationD2Ev, .-_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotificationD2Ev
	.weak	_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotificationD1Ev
	.set	_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotificationD1Ev,_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotificationD2Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationD2Ev
	.type	_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationD2Ev, @function
_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationD2Ev:
.LFB5878:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	224(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L2250
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2251
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2250:
	movq	168(%rbx), %rdi
	leaq	184(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2252
	call	_ZdlPv@PLT
.L2252:
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2253
	movq	(%rdi), %rax
	call	*24(%rax)
.L2253:
	movq	112(%rbx), %rdi
	leaq	128(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2254
	call	_ZdlPv@PLT
.L2254:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2255
	call	_ZdlPv@PLT
.L2255:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L2249
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L2249:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2251:
	.cfi_restore_state
	call	*%rax
	jmp	.L2250
	.cfi_endproc
.LFE5878:
	.size	_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationD2Ev, .-_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationD2Ev
	.weak	_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationD1Ev
	.set	_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationD1Ev,_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationD2Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger18PausedNotificationD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger18PausedNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger18PausedNotificationD2Ev
	.type	_ZN12v8_inspector8protocol8Debugger18PausedNotificationD2Ev, @function
_ZN12v8_inspector8protocol8Debugger18PausedNotificationD2Ev:
.LFB5800:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger18PausedNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	88(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L2265
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2266
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r12), %rdi
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm1
	leaq	80(%r12), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	cmpq	%rax, %rdi
	je	.L2267
	call	_ZdlPv@PLT
.L2267:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2268
	call	_ZdlPv@PLT
.L2268:
	movl	$104, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2265:
	movq	80(%rbx), %r12
	testq	%r12, %r12
	je	.L2269
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2270
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r12), %rdi
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm2
	leaq	80(%r12), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r12)
	cmpq	%rax, %rdi
	je	.L2271
	call	_ZdlPv@PLT
.L2271:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2272
	call	_ZdlPv@PLT
.L2272:
	movl	$104, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2269:
	movq	72(%rbx), %r12
	testq	%r12, %r12
	je	.L2273
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2274
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2273:
	movq	64(%rbx), %r13
	testq	%r13, %r13
	je	.L2275
	movq	8(%r13), %r14
	movq	0(%r13), %r12
	cmpq	%r12, %r14
	je	.L2276
	.p2align 4,,10
	.p2align 3
.L2280:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2277
	call	_ZdlPv@PLT
	addq	$40, %r12
	cmpq	%r14, %r12
	jne	.L2280
	movq	0(%r13), %r12
.L2276:
	testq	%r12, %r12
	je	.L2281
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2281:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2275:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2282
	movq	(%rdi), %rax
	call	*24(%rax)
.L2282:
	movq	16(%rbx), %rdi
	leaq	32(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2283
	call	_ZdlPv@PLT
.L2283:
	movq	8(%rbx), %r14
	testq	%r14, %r14
	je	.L2264
	movq	8(%r14), %rbx
	movq	(%r14), %r12
	cmpq	%r12, %rbx
	je	.L2285
	leaq	_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev(%rip), %r15
	jmp	.L2288
	.p2align 4,,10
	.p2align 3
.L2317:
	call	_ZN12v8_inspector8protocol8Debugger9CallFrameD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2286:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L2316
.L2288:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L2286
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%r15, %rax
	je	.L2317
	call	*%rax
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L2288
	.p2align 4,,10
	.p2align 3
.L2316:
	movq	(%r14), %r12
.L2285:
	testq	%r12, %r12
	je	.L2289
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2289:
	addq	$8, %rsp
	movq	%r14, %rdi
	movl	$24, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L2277:
	.cfi_restore_state
	addq	$40, %r12
	cmpq	%r12, %r14
	jne	.L2280
	movq	0(%r13), %r12
	jmp	.L2276
	.p2align 4,,10
	.p2align 3
.L2264:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2266:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2265
	.p2align 4,,10
	.p2align 3
.L2270:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2269
	.p2align 4,,10
	.p2align 3
.L2274:
	call	*%rax
	jmp	.L2273
	.cfi_endproc
.LFE5800:
	.size	_ZN12v8_inspector8protocol8Debugger18PausedNotificationD2Ev, .-_ZN12v8_inspector8protocol8Debugger18PausedNotificationD2Ev
	.weak	_ZN12v8_inspector8protocol8Debugger18PausedNotificationD1Ev
	.set	_ZN12v8_inspector8protocol8Debugger18PausedNotificationD1Ev,_ZN12v8_inspector8protocol8Debugger18PausedNotificationD2Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger18PausedNotificationD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger18PausedNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger18PausedNotificationD0Ev
	.type	_ZN12v8_inspector8protocol8Debugger18PausedNotificationD0Ev, @function
_ZN12v8_inspector8protocol8Debugger18PausedNotificationD0Ev:
.LFB5802:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN12v8_inspector8protocol8Debugger18PausedNotificationD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5802:
	.size	_ZN12v8_inspector8protocol8Debugger18PausedNotificationD0Ev, .-_ZN12v8_inspector8protocol8Debugger18PausedNotificationD0Ev
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImpl13getStackTraceEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"stackTraceId"
.LC17:
	.string	"stackTrace"
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl13getStackTraceEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl13getStackTraceEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl13getStackTraceEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl13getStackTraceEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6216:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movl	%esi, -212(%rbp)
	movq	(%r8), %r14
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -224(%rbp)
	movq	%rcx, -232(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L2321
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	jne	.L2322
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L2377
	call	_ZdlPv@PLT
.L2377:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	cmpq	%r15, %rdi
	je	.L2324
	call	_ZdlPv@PLT
.L2324:
	leaq	.LC16(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-200(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol7Runtime12StackTraceId9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L2378
	leaq	-184(%rbp), %rdi
	movq	%r13, %rsi
	movq	$0, -192(%rbp)
	leaq	-168(%rbp), %r14
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r13), %rsi
	movq	-200(%rbp), %rdx
	movq	%r12, %rdi
	leaq	-192(%rbp), %rcx
	movq	(%rsi), %rax
	movq	$0, -200(%rbp)
	movq	64(%rax), %rax
	movq	%rdx, -168(%rbp)
	movq	%r14, %rdx
	call	*%rax
	movq	-168(%rbp), %r15
	testq	%r15, %r15
	je	.L2328
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2329
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r15), %rdi
	leaq	-64(%rax), %rbx
	movq	%rax, %xmm2
	leaq	80(%r15), %rax
	movq	%rbx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r15)
	cmpq	%rax, %rdi
	je	.L2330
	call	_ZdlPv@PLT
.L2330:
	movq	16(%r15), %rdi
	leaq	32(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2331
	call	_ZdlPv@PLT
.L2331:
	movl	$104, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2328:
	cmpl	$2, -112(%rbp)
	je	.L2379
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L2380
.L2334:
	movq	-184(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2381
	movl	-212(%rbp), %esi
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r13, -168(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2333
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L2333
	.p2align 4,,10
	.p2align 3
.L2379:
	movq	8(%r13), %rdi
	movq	-232(%rbp), %rcx
	movq	-224(%rbp), %rdx
	movl	-212(%rbp), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
.L2333:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2338
	call	_ZdlPv@PLT
.L2338:
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2339
	call	_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0
.L2339:
	movq	-192(%rbp), %r12
	testq	%r12, %r12
	je	.L2327
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2341
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2327:
	movq	-200(%rbp), %r12
	testq	%r12, %r12
	je	.L2320
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2343
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r12), %rdi
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm1
	leaq	80(%r12), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	cmpq	%rax, %rdi
	je	.L2344
	call	_ZdlPv@PLT
.L2344:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2345
	call	_ZdlPv@PLT
.L2345:
	movl	$104, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2320:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2382
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2321:
	.cfi_restore_state
	movq	-112(%rbp), %rdi
.L2322:
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L2383
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L2324
	.p2align 4,,10
	.p2align 3
.L2378:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	-212(%rbp), %esi
	movl	$-32602, %edx
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L2327
	call	_ZdlPv@PLT
	jmp	.L2327
	.p2align 4,,10
	.p2align 3
.L2381:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L2333
	.p2align 4,,10
	.p2align 3
.L2383:
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L2324
	.p2align 4,,10
	.p2align 3
.L2380:
	movq	-192(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-160(%rbp), %r15
	call	_ZNK12v8_inspector8protocol7Runtime10StackTrace7toValueEv@PLT
	movq	-168(%rbp), %rax
	leaq	.LC17(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	leaq	-176(%rbp), %rdx
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2335
	call	_ZdlPv@PLT
.L2335:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2334
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L2334
	.p2align 4,,10
	.p2align 3
.L2343:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2320
	.p2align 4,,10
	.p2align 3
.L2341:
	call	*%rax
	jmp	.L2327
	.p2align 4,,10
	.p2align 3
.L2329:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L2328
.L2382:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6216:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl13getStackTraceEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl13getStackTraceEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime10StackTraceD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD0Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD0Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD0Ev:
.LFB12779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$88, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12779:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD0Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12StackTraceIdD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev, @function
_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev:
.LFB5237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	80(%rbx), %rax
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movq	64(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L2387
	call	_ZdlPv@PLT
.L2387:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L2386
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L2386:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5237:
	.size	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev, .-_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD1Ev,_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD1Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD1Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD1Ev:
.LFB12776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	72(%rbx), %rax
	subq	$8, %rsp
	movups	%xmm0, -8(%rdi)
	movq	56(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L2391
	call	_ZdlPv@PLT
.L2391:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L2390
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L2390:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12776:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD1Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD1Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12StackTraceIdD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD0Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD0Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD0Ev:
.LFB12780:
	.cfi_startproc
	endbr64
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movups	%xmm0, -8(%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2395
	call	_ZdlPv@PLT
.L2395:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L2396
	call	_ZdlPv@PLT
.L2396:
	popq	%rbx
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12780:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD0Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD0Ev
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14ScriptPosition9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC18:
	.string	"lineNumber"
.LC19:
	.string	"columnNumber"
	.section	.text._ZN12v8_inspector8protocol8Debugger14ScriptPosition9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14ScriptPosition9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14ScriptPosition9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14ScriptPosition9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB6036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L2399
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r12
	je	.L2400
.L2399:
	leaq	.LC15(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, (%r15)
.L2398:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2416
	addq	$72, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2400:
	.cfi_restore_state
	movl	$16, %edi
	leaq	-96(%rbp), %r14
	call	_Znwm@PLT
	cmpl	$6, 8(%r12)
	movq	%rbx, %rdi
	movq	%rax, %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger14ScriptPositionE(%rip), %rax
	movq	%rax, 0(%r13)
	movl	$0, %eax
	cmovne	%rax, %r12
	movq	$0, 8(%r13)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC18(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -104(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -112(%rbp)
	cmpq	%rax, %rdi
	je	.L2403
	call	_ZdlPv@PLT
.L2403:
	leaq	.LC18(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-104(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC19(%rip), %rsi
	movq	%r14, %rdi
	movl	%eax, 8(%r13)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r12
	cmpq	-112(%rbp), %rdi
	je	.L2404
	call	_ZdlPv@PLT
.L2404:
	leaq	.LC19(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	%rbx, %rdi
	movl	%eax, 12(%r13)
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L2417
	movq	%r13, (%r15)
	jmp	.L2398
	.p2align 4,,10
	.p2align 3
.L2417:
	movq	0(%r13), %rax
	movq	$0, (%r15)
	leaq	_ZN12v8_inspector8protocol8Debugger14ScriptPositionD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2418
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2398
	.p2align 4,,10
	.p2align 3
.L2418:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2398
.L2416:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6036:
	.size	_ZN12v8_inspector8protocol8Debugger14ScriptPosition9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14ScriptPosition9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.text._ZNK12v8_inspector8protocol8Debugger14ScriptPosition7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Debugger14ScriptPosition7toValueEv
	.type	_ZNK12v8_inspector8protocol8Debugger14ScriptPosition7toValueEv, @function
_ZNK12v8_inspector8protocol8Debugger14ScriptPosition7toValueEv:
.LFB6037:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$96, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r15, (%r12)
	movl	$24, %edi
	movl	8(%rbx), %r13d
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	leaq	.LC18(%rip), %rsi
	movl	%r13d, 16(%rax)
	leaq	-96(%rbp), %r13
	movl	$2, 8(%rax)
	movq	%r13, %rdi
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, %r15
	cmpq	%rax, %rdi
	je	.L2420
	call	_ZdlPv@PLT
.L2420:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2421
	movq	(%rdi), %rax
	call	*24(%rax)
.L2421:
	movq	(%r12), %r8
	movl	12(%rbx), %ebx
	movl	$24, %edi
	movq	%r8, -120(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	movq	%r13, %rdi
	movq	%rdx, (%rax)
	movl	$2, 8(%rax)
	movl	%ebx, 16(%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-120(%rbp), %r8
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L2422
	call	_ZdlPv@PLT
.L2422:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2419
	movq	(%rdi), %rax
	call	*24(%rax)
.L2419:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2432
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2432:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6037:
	.size	_ZNK12v8_inspector8protocol8Debugger14ScriptPosition7toValueEv, .-_ZNK12v8_inspector8protocol8Debugger14ScriptPosition7toValueEv
	.section	.text._ZN12v8_inspector8protocol8Debugger14ScriptPosition17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger14ScriptPosition17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger14ScriptPosition17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Debugger14ScriptPosition17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Debugger14ScriptPosition17serializeToBinaryEv:
.LFB5551:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger14ScriptPosition7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2433
	movq	(%rdi), %rax
	call	*24(%rax)
.L2433:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2440
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2440:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5551:
	.size	_ZN12v8_inspector8protocol8Debugger14ScriptPosition17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Debugger14ScriptPosition17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Debugger14ScriptPosition15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger14ScriptPosition15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger14ScriptPosition15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Debugger14ScriptPosition15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Debugger14ScriptPosition15serializeToJSONEv:
.LFB5550:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger14ScriptPosition7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2441
	movq	(%rdi), %rax
	call	*24(%rax)
.L2441:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2448
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2448:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5550:
	.size	_ZN12v8_inspector8protocol8Debugger14ScriptPosition15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Debugger14ScriptPosition15serializeToJSONEv
	.section	.text._ZNK12v8_inspector8protocol8Debugger14ScriptPosition5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Debugger14ScriptPosition5cloneEv
	.type	_ZNK12v8_inspector8protocol8Debugger14ScriptPosition5cloneEv, @function
_ZNK12v8_inspector8protocol8Debugger14ScriptPosition5cloneEv:
.LFB6038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol8Debugger14ScriptPosition7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol8Debugger14ScriptPosition9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2450
	movq	(%rdi), %rax
	call	*24(%rax)
.L2450:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2456
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2456:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6038:
	.size	_ZNK12v8_inspector8protocol8Debugger14ScriptPosition5cloneEv, .-_ZNK12v8_inspector8protocol8Debugger14ScriptPosition5cloneEv
	.section	.rodata._ZN12v8_inspector8protocol8Debugger8Frontend7resumedEv.str1.1,"aMS",@progbits,1
.LC20:
	.string	"Debugger.resumed"
	.section	.text._ZN12v8_inspector8protocol8Debugger8Frontend7resumedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger8Frontend7resumedEv
	.type	_ZN12v8_inspector8protocol8Debugger8Frontend7resumedEv, @function
_ZN12v8_inspector8protocol8Debugger8Frontend7resumedEv:
.LFB6081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L2457
	movq	(%r12), %rax
	leaq	-80(%rbp), %r13
	leaq	.LC20(%rip), %rsi
	movq	$0, -104(%rbp)
	movq	%r13, %rdi
	movq	24(%rax), %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	-96(%rbp), %rdi
	leaq	-104(%rbp), %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16InternalResponse18createNotificationERKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rax
	movq	%r12, %rdi
	leaq	-88(%rbp), %rsi
	movq	$0, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	*%rbx
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2459
	movq	(%rdi), %rax
	call	*24(%rax)
.L2459:
	movq	-96(%rbp), %r12
	testq	%r12, %r12
	je	.L2460
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol16InternalResponseD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2461
	movq	56(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L2462
	movq	(%rdi), %rax
	call	*24(%rax)
.L2462:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2463
	call	_ZdlPv@PLT
.L2463:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2460:
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2464
	call	_ZdlPv@PLT
.L2464:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2457
	movq	(%rdi), %rax
	call	*24(%rax)
.L2457:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2483
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2461:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2460
.L2483:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6081:
	.size	_ZN12v8_inspector8protocol8Debugger8Frontend7resumedEv, .-_ZN12v8_inspector8protocol8Debugger8Frontend7resumedEv
	.section	.text._ZN12v8_inspector8protocol8Debugger8Frontend5flushEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger8Frontend5flushEv
	.type	_ZN12v8_inspector8protocol8Debugger8Frontend5flushEv, @function
_ZN12v8_inspector8protocol8Debugger8Frontend5flushEv:
.LFB6089:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.cfi_endproc
.LFE6089:
	.size	_ZN12v8_inspector8protocol8Debugger8Frontend5flushEv, .-_ZN12v8_inspector8protocol8Debugger8Frontend5flushEv
	.section	.text._ZN12v8_inspector8protocol8Debugger8Frontend23sendRawJSONNotificationENS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger8Frontend23sendRawJSONNotificationENS_8String16E
	.type	_ZN12v8_inspector8protocol8Debugger8Frontend23sendRawJSONNotificationENS_8String16E, @function
_ZN12v8_inspector8protocol8Debugger8Frontend23sendRawJSONNotificationENS_8String16E:
.LFB6090:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rsi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	movq	24(%rax), %r14
	movq	(%rsi), %rax
	cmpq	%rdx, %rax
	je	.L2499
	movq	%rdx, (%rsi)
	xorl	%edx, %edx
	movq	16(%rsi), %rcx
	leaq	-64(%rbp), %r13
	movw	%dx, 16(%rsi)
	movq	32(%rsi), %rdx
	leaq	-112(%rbp), %rbx
	movq	8(%rsi), %rdi
	movq	%rcx, -112(%rbp)
	movq	$0, 8(%rsi)
	movq	%rdx, -96(%rbp)
	movq	%r13, -80(%rbp)
	cmpq	%rbx, %rax
	je	.L2487
	movq	%rax, -80(%rbp)
	movq	%rcx, -64(%rbp)
.L2489:
	movq	%rdi, -72(%rbp)
	xorl	%eax, %eax
	movl	$72, %edi
	movq	%rdx, -48(%rbp)
	movq	%rbx, -128(%rbp)
	movq	$0, -120(%rbp)
	movw	%ax, -112(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol23InternalRawNotificationE(%rip), %rcx
	leaq	24(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%rdx, 8(%rax)
	movq	-80(%rbp), %rdx
	cmpq	%r13, %rdx
	je	.L2500
	movq	%rdx, 8(%rax)
	movq	-64(%rbp), %rdx
	movq	%rdx, 24(%rax)
.L2491:
	movq	-72(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	$0, 64(%rax)
	movq	%rax, -136(%rbp)
	leaq	-136(%rbp), %rsi
	movq	%rdx, 16(%rax)
	movq	-48(%rbp), %rdx
	movups	%xmm0, 48(%rax)
	movq	%rdx, 40(%rax)
	call	*%r14
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2492
	movq	(%rdi), %rax
	call	*24(%rax)
.L2492:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2485
	call	_ZdlPv@PLT
.L2485:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2501
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2499:
	.cfi_restore_state
	movq	32(%rsi), %rdx
	xorl	%ecx, %ecx
	leaq	-64(%rbp), %r13
	movdqu	16(%rsi), %xmm1
	movq	8(%rsi), %rdi
	movw	%cx, 16(%rsi)
	leaq	-112(%rbp), %rbx
	movq	$0, 8(%rsi)
	movq	%rdx, -96(%rbp)
	movq	%r13, -80(%rbp)
	movaps	%xmm1, -112(%rbp)
.L2487:
	movdqa	-112(%rbp), %xmm2
	movaps	%xmm2, -64(%rbp)
	jmp	.L2489
	.p2align 4,,10
	.p2align 3
.L2500:
	movdqa	-64(%rbp), %xmm3
	movups	%xmm3, 24(%rax)
	jmp	.L2491
.L2501:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6090:
	.size	_ZN12v8_inspector8protocol8Debugger8Frontend23sendRawJSONNotificationENS_8String16E, .-_ZN12v8_inspector8protocol8Debugger8Frontend23sendRawJSONNotificationENS_8String16E
	.section	.text._ZN12v8_inspector8protocol8Debugger8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE
	.type	_ZN12v8_inspector8protocol8Debugger8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE, @function
_ZN12v8_inspector8protocol8Debugger8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE:
.LFB6091:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r13
	movq	(%rsi), %rbx
	movl	$72, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %r15
	movq	8(%rsi), %r12
	movq	0(%r13), %rax
	movq	24(%rax), %r14
	movq	$0, 16(%rsi)
	movups	%xmm0, (%rsi)
	call	_Znwm@PLT
	movq	%r12, %xmm1
	movq	%rbx, %xmm0
	movq	%r13, %rdi
	leaq	24(%rax), %rdx
	punpcklqdq	%xmm1, %xmm0
	movq	%r15, 64(%rax)
	leaq	-64(%rbp), %rsi
	movq	%rdx, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol23InternalRawNotificationE(%rip), %rcx
	xorl	%edx, %edx
	movq	%rcx, (%rax)
	movq	$0, 16(%rax)
	movw	%dx, 24(%rax)
	movq	$0, 40(%rax)
	movq	%rax, -64(%rbp)
	movups	%xmm0, 48(%rax)
	call	*%r14
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2502
	movq	(%rdi), %rax
	call	*24(%rax)
.L2502:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2509
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2509:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6091:
	.size	_ZN12v8_inspector8protocol8Debugger8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE, .-_ZN12v8_inspector8protocol8Debugger8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE
	.section	.rodata._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm.str1.1,"aMS",@progbits,1
.LC21:
	.string	"vector::reserve"
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm:
.LFB6872:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	%rax, %rsi
	ja	.L2526
	movq	(%rdi), %r12
	movq	16(%rdi), %rax
	movq	%rdi, %r15
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rax, %rsi
	ja	.L2527
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2527:
	.cfi_restore_state
	movq	8(%rdi), %rbx
	xorl	%r13d, %r13d
	movq	%rbx, %rax
	subq	%r12, %rax
	movq	%rax, -56(%rbp)
	leaq	0(,%rsi,8), %rax
	movq	%rax, -64(%rbp)
	testq	%rsi, %rsi
	je	.L2513
	movq	%rax, %rdi
	call	_Znwm@PLT
	movq	8(%r15), %rbx
	movq	(%r15), %r12
	movq	%rax, %r13
.L2513:
	cmpq	%r12, %rbx
	je	.L2514
	movq	%r13, %r14
	.p2align 4,,10
	.p2align 3
.L2518:
	movq	(%r12), %rcx
	movq	$0, (%r12)
	movq	%rcx, (%r14)
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2515
	movq	(%rdi), %rcx
	addq	$8, %r12
	addq	$8, %r14
	call	*24(%rcx)
	cmpq	%rbx, %r12
	jne	.L2518
.L2516:
	movq	(%r15), %r12
.L2514:
	testq	%r12, %r12
	je	.L2519
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2519:
	movq	-56(%rbp), %r14
	movq	%r13, (%r15)
	addq	%r13, %r14
	addq	-64(%rbp), %r13
	movq	%r14, 8(%r15)
	movq	%r13, 16(%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2515:
	.cfi_restore_state
	addq	$8, %r12
	addq	$8, %r14
	cmpq	%r12, %rbx
	jne	.L2518
	jmp	.L2516
.L2526:
	leaq	.LC21(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE6872:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_:
.LFB7548:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	(%rdi), %rdi
	cmpq	%rax, %rsi
	je	.L2544
	leaq	16(%r12), %rdx
	cmpq	%rdx, %rdi
	je	.L2545
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	movq	16(%r12), %rdx
	movq	%rax, 8(%r12)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r12)
	testq	%rdi, %rdi
	je	.L2534
	movq	%rdi, (%rbx)
	movq	%rdx, 16(%rbx)
.L2532:
	xorl	%eax, %eax
	movq	$0, 8(%rbx)
	movw	%ax, (%rdi)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2545:
	.cfi_restore_state
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	movq	%rax, 8(%r12)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r12)
.L2534:
	movq	%rsi, (%rbx)
	movq	%rsi, %rdi
	jmp	.L2532
	.p2align 4,,10
	.p2align 3
.L2544:
	movq	8(%rbx), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L2530
	cmpq	$1, %rax
	je	.L2546
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L2530
	call	memmove@PLT
	movq	8(%rbx), %rax
	movq	(%r12), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L2530:
	xorl	%ecx, %ecx
	movq	%rax, 8(%r12)
	movw	%cx, (%rdi,%rdx)
	movq	(%rbx), %rdi
	jmp	.L2532
	.p2align 4,,10
	.p2align 3
.L2546:
	movzwl	16(%rbx), %eax
	movw	%ax, (%rdi)
	movq	8(%rbx), %rax
	movq	(%r12), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L2530
	.cfi_endproc
.LFE7548:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	.section	.rodata._ZN12v8_inspector8protocol8Debugger8Location9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC22:
	.string	"scriptId"
	.section	.text._ZN12v8_inspector8protocol8Debugger8Location9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger8Location9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger8Location9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger8Location9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB6029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L2548
	cmpl	$6, 8(%rsi)
	movq	%rsi, %rbx
	je	.L2549
.L2548:
	leaq	.LC15(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, (%r12)
.L2547:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2568
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2549:
	.cfi_restore_state
	movl	$64, %edi
	leaq	-96(%rbp), %r14
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	%rax, 0(%r13)
	leaq	24(%r13), %rax
	movq	%rax, 8(%r13)
	xorl	%eax, %eax
	cmpl	$6, 8(%rbx)
	movw	%ax, 24(%r13)
	movl	$0, %eax
	cmovne	%rax, %rbx
	movq	$0, 16(%r13)
	movq	$0, 40(%r13)
	movb	$0, 52(%r13)
	movl	$0, 56(%r13)
	movl	$0, 48(%r13)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC22(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -112(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -104(%rbp)
	cmpq	%rax, %rdi
	je	.L2552
	call	_ZdlPv@PLT
.L2552:
	leaq	.LC22(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-112(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	8(%r13), %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 40(%r13)
	cmpq	-104(%rbp), %rdi
	je	.L2553
	call	_ZdlPv@PLT
.L2553:
	leaq	.LC18(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -112(%rbp)
	cmpq	-104(%rbp), %rdi
	je	.L2554
	call	_ZdlPv@PLT
.L2554:
	leaq	.LC18(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-112(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	%r14, %rdi
	leaq	.LC19(%rip), %rsi
	movl	%eax, 48(%r13)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-104(%rbp), %rdi
	je	.L2555
	call	_ZdlPv@PLT
.L2555:
	testq	%r14, %r14
	je	.L2556
	leaq	.LC19(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movb	$1, 52(%r13)
	movl	%eax, 56(%r13)
.L2556:
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L2569
	movq	%r13, (%r12)
	jmp	.L2547
	.p2align 4,,10
	.p2align 3
.L2569:
	movq	0(%r13), %rax
	movq	$0, (%r12)
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L2547
.L2568:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6029:
	.size	_ZN12v8_inspector8protocol8Debugger8Location9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger8Location9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger11SearchMatch9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC24:
	.string	"double value expected"
.LC25:
	.string	"lineContent"
	.section	.text._ZN12v8_inspector8protocol8Debugger11SearchMatch9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger11SearchMatch9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger11SearchMatch9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger11SearchMatch9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB6045:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L2571
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r15
	je	.L2572
.L2571:
	leaq	.LC15(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, (%r12)
.L2570:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2596
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2572:
	.cfi_restore_state
	leaq	80+_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE(%rip), %rax
	movl	$64, %edi
	leaq	-96(%rbp), %r14
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm2
	movq	%rdx, %xmm1
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm1, -144(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%rax, %r13
	leaq	40(%rax), %rax
	movups	%xmm1, -40(%rax)
	movq	%rax, -152(%rbp)
	movq	%rax, 24(%r13)
	xorl	%eax, %eax
	cmpl	$6, 8(%r15)
	movw	%ax, 40(%r13)
	movl	$0, %eax
	cmovne	%rax, %r15
	movq	$0, 32(%r13)
	movq	$0, 56(%r13)
	movq	$0x000000000, 16(%r13)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC18(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r9
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rdi
	je	.L2575
	movq	%r9, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %r9
.L2575:
	leaq	.LC18(%rip), %rsi
	movq	%rbx, %rdi
	movq	%r9, -128(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %r9
	movq	$0x000000000, -104(%rbp)
	testq	%r9, %r9
	je	.L2578
	movq	(%r9), %rax
	leaq	-104(%rbp), %rsi
	movq	%r9, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L2577
.L2578:
	leaq	.LC24(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L2577:
	movsd	-104(%rbp), %xmm0
	leaq	.LC25(%rip), %rsi
	movq	%r14, %rdi
	movsd	%xmm0, 16(%r13)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r15
	cmpq	-120(%rbp), %rdi
	je	.L2579
	call	_ZdlPv@PLT
.L2579:
	leaq	.LC25(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	24(%r13), %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 56(%r13)
	cmpq	-120(%rbp), %rdi
	je	.L2580
	call	_ZdlPv@PLT
.L2580:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L2597
	movq	%r13, (%r12)
	jmp	.L2570
	.p2align 4,,10
	.p2align 3
.L2597:
	movq	0(%r13), %rax
	movq	$0, (%r12)
	leaq	_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2598
	movdqa	-144(%rbp), %xmm3
	movq	24(%r13), %rdi
	movups	%xmm3, 0(%r13)
	cmpq	%rdi, -152(%rbp)
	je	.L2581
	call	_ZdlPv@PLT
.L2581:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2570
	.p2align 4,,10
	.p2align 3
.L2598:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2570
.L2596:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6045:
	.size	_ZN12v8_inspector8protocol8Debugger11SearchMatch9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger11SearchMatch9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol8Debugger3API11SearchMatch14fromJSONStringERKNS_10StringViewE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger3API11SearchMatch14fromJSONStringERKNS_10StringViewE
	.type	_ZN12v8_inspector8protocol8Debugger3API11SearchMatch14fromJSONStringERKNS_10StringViewE, @function
_ZN12v8_inspector8protocol8Debugger3API11SearchMatch14fromJSONStringERKNS_10StringViewE:
.LFB6054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r14
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	movq	%r13, %rsi
	leaq	-112(%rbp), %rdi
	call	_ZN12v8_inspector8protocol10StringUtil9parseJSONERKNS_10StringViewE@PLT
	movq	-112(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L2610
	leaq	-104(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZN12v8_inspector8protocol8Debugger11SearchMatch9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rax
	movq	-112(%rbp), %rdi
	leaq	8(%rax), %rdx
	testq	%rax, %rax
	cmovne	%rdx, %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L2601
	movq	(%rdi), %rax
	call	*24(%rax)
.L2601:
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2611
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2610:
	.cfi_restore_state
	movq	$0, (%r12)
	jmp	.L2601
.L2611:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6054:
	.size	_ZN12v8_inspector8protocol8Debugger3API11SearchMatch14fromJSONStringERKNS_10StringViewE, .-_ZN12v8_inspector8protocol8Debugger3API11SearchMatch14fromJSONStringERKNS_10StringViewE
	.section	.text._ZN12v8_inspector8protocol8Debugger3API11SearchMatch10fromBinaryEPKhm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger3API11SearchMatch10fromBinaryEPKhm
	.type	_ZN12v8_inspector8protocol8Debugger3API11SearchMatch10fromBinaryEPKhm, @function
_ZN12v8_inspector8protocol8Debugger3API11SearchMatch10fromBinaryEPKhm:
.LFB6055:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	movq	%r13, %rsi
	leaq	-112(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZN12v8_inspector8protocol5Value11parseBinaryEPKhm@PLT
	movq	-112(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L2623
	leaq	-104(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZN12v8_inspector8protocol8Debugger11SearchMatch9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rax
	movq	-112(%rbp), %rdi
	leaq	8(%rax), %rdx
	testq	%rax, %rax
	cmovne	%rdx, %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L2614
	movq	(%rdi), %rax
	call	*24(%rax)
.L2614:
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2624
	addq	$80, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2623:
	.cfi_restore_state
	movq	$0, (%r12)
	jmp	.L2614
.L2624:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6055:
	.size	_ZN12v8_inspector8protocol8Debugger3API11SearchMatch10fromBinaryEPKhm, .-_ZN12v8_inspector8protocol8Debugger3API11SearchMatch10fromBinaryEPKhm
	.section	.rodata._ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC26:
	.string	"location"
	.section	.text._ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB6059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L2626
	cmpl	$6, 8(%rsi)
	movq	%rsi, %rbx
	je	.L2627
.L2626:
	leaq	.LC15(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, (%r12)
.L2625:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2657
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2627:
	.cfi_restore_state
	movl	$56, %edi
	leaq	-96(%rbp), %r14
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationE(%rip), %rax
	movq	%rax, 0(%r13)
	leaq	24(%r13), %rax
	movq	%rax, -136(%rbp)
	movq	%rax, 8(%r13)
	xorl	%eax, %eax
	cmpl	$6, 8(%rbx)
	movw	%ax, 24(%r13)
	movl	$0, %eax
	cmovne	%rax, %rbx
	movq	$0, 16(%r13)
	movq	$0, 40(%r13)
	movq	$0, 48(%r13)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC10(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -128(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rdi
	je	.L2630
	call	_ZdlPv@PLT
.L2630:
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	8(%r13), %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 40(%r13)
	cmpq	-120(%rbp), %rdi
	je	.L2631
	call	_ZdlPv@PLT
.L2631:
	movq	%r14, %rdi
	leaq	.LC26(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-120(%rbp), %rdi
	je	.L2632
	call	_ZdlPv@PLT
.L2632:
	leaq	.LC26(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-104(%rbp), %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol8Debugger8Location9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rax
	movq	48(%r13), %rdi
	movq	$0, -104(%rbp)
	movq	%rax, 48(%r13)
	testq	%rdi, %rdi
	je	.L2634
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2634
	movq	(%rdi), %rax
	call	*24(%rax)
.L2634:
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L2658
	movq	%r13, (%r12)
	jmp	.L2625
	.p2align 4,,10
	.p2align 3
.L2658:
	movq	0(%r13), %rax
	movq	$0, (%r12)
	leaq	_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2659
	movq	48(%r13), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L2635
	movq	(%rdi), %rax
	call	*24(%rax)
.L2635:
	movq	8(%r13), %rdi
	cmpq	%rdi, -136(%rbp)
	je	.L2636
	call	_ZdlPv@PLT
.L2636:
	movl	$56, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2625
	.p2align 4,,10
	.p2align 3
.L2659:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2625
.L2657:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6059:
	.size	_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.rodata._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_.str1.1,"aMS",@progbits,1
.LC27:
	.string	"basic_string::_M_create"
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_:
.LFB9749:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L2679
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r13
	movq	8(%rsi), %r15
	cmpq	%r13, %rdx
	je	.L2670
	movq	16(%rdi), %rax
.L2662:
	cmpq	%r15, %rax
	jb	.L2682
	leaq	(%r15,%r15), %rdx
	testq	%r15, %r15
	je	.L2666
.L2685:
	movq	(%r12), %rsi
	cmpq	$1, %r15
	je	.L2683
	testq	%rdx, %rdx
	je	.L2666
	movq	%r13, %rdi
	call	memmove@PLT
	movq	(%rbx), %r13
.L2666:
	xorl	%eax, %eax
	movq	%r15, 8(%rbx)
	movw	%ax, 0(%r13,%r15,2)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2682:
	.cfi_restore_state
	movabsq	$2305843009213693951, %rcx
	cmpq	%rcx, %r15
	ja	.L2684
	addq	%rax, %rax
	movq	%r15, %r14
	cmpq	%rax, %r15
	jnb	.L2665
	cmpq	%rcx, %rax
	cmovbe	%rax, %rcx
	movq	%rcx, %r14
.L2665:
	leaq	2(%r14,%r14), %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	(%rbx), %rdi
	movq	-56(%rbp), %rdx
	movq	%rax, %r13
	cmpq	%rdi, %rdx
	je	.L2669
	call	_ZdlPv@PLT
.L2669:
	movq	%r13, (%rbx)
	leaq	(%r15,%r15), %rdx
	movq	%r14, 16(%rbx)
	testq	%r15, %r15
	je	.L2666
	jmp	.L2685
	.p2align 4,,10
	.p2align 3
.L2679:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L2670:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$7, %eax
	jmp	.L2662
	.p2align 4,,10
	.p2align 3
.L2683:
	movzwl	(%rsi), %eax
	movw	%ax, 0(%r13)
	movq	(%rbx), %r13
	jmp	.L2666
.L2684:
	leaq	.LC27(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9749:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.section	.rodata._ZN12v8_inspector8protocol8Debugger5Scope9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC28:
	.string	"type"
.LC29:
	.string	"object"
.LC30:
	.string	"name"
.LC31:
	.string	"startLocation"
.LC32:
	.string	"endLocation"
	.section	.text._ZN12v8_inspector8protocol8Debugger5Scope9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger5Scope9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger5Scope9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger5Scope9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB6042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L2687
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r12
	je	.L2688
.L2687:
	leaq	.LC15(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, (%r14)
.L2686:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2742
	addq	$104, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2688:
	.cfi_restore_state
	movl	$120, %edi
	leaq	-96(%rbp), %rbx
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	%rax, %r15
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger5ScopeE(%rip), %rax
	movq	%rax, (%r15)
	leaq	24(%r15), %rax
	movq	%rax, 8(%r15)
	xorl	%eax, %eax
	cmpl	$6, 8(%r12)
	movw	%ax, 24(%r15)
	leaq	80(%r15), %rax
	movq	%rax, 64(%r15)
	movl	$0, %eax
	cmovne	%rax, %r12
	movups	%xmm0, 80(%r15)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 104(%r15)
	movq	$0, 16(%r15)
	movq	$0, 40(%r15)
	movq	$0, 48(%r15)
	movb	$0, 56(%r15)
	movq	$0, 96(%r15)
	movq	$0, 72(%r15)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC28(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -128(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rdi
	je	.L2691
	call	_ZdlPv@PLT
.L2691:
	leaq	.LC28(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	8(%r15), %rdi
	movq	%rbx, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 40(%r15)
	cmpq	-120(%rbp), %rdi
	je	.L2692
	call	_ZdlPv@PLT
.L2692:
	leaq	.LC29(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -136(%rbp)
	cmpq	-120(%rbp), %rdi
	je	.L2693
	call	_ZdlPv@PLT
.L2693:
	leaq	.LC29(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-104(%rbp), %rax
	movq	-136(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rax, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObject9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE@PLT
	movq	-104(%rbp), %rax
	movq	48(%r15), %rdi
	movq	$0, -104(%rbp)
	movq	%rax, 48(%r15)
	testq	%rdi, %rdi
	je	.L2695
	movq	(%rdi), %rax
	movq	24(%rax), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rax
	cmpq	%rax, %rdx
	jne	.L2696
	movq	%rdi, -136(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movq	-136(%rbp), %rdi
	movl	$320, %esi
	call	_ZdlPvm@PLT
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rax
.L2697:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2695
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2699
	movq	%rdi, -136(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movq	-136(%rbp), %rdi
	movl	$320, %esi
	call	_ZdlPvm@PLT
.L2695:
	leaq	.LC30(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L2700
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %rax
.L2700:
	testq	%rax, %rax
	movq	%rax, -136(%rbp)
	je	.L2701
	leaq	.LC30(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-136(%rbp), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	64(%r15), %rdi
	movq	%rbx, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movb	$1, 56(%r15)
	movq	-96(%rbp), %rdi
	movq	%rax, 96(%r15)
	cmpq	-120(%rbp), %rdi
	je	.L2701
	call	_ZdlPv@PLT
.L2701:
	leaq	.LC31(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L2703
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %rax
.L2703:
	testq	%rax, %rax
	movq	%rax, -136(%rbp)
	je	.L2704
	leaq	.LC31(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdi
	movq	%r13, %rdx
	movq	%rax, %rsi
	call	_ZN12v8_inspector8protocol8Debugger8Location9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rax
	movq	104(%r15), %rdi
	movq	$0, -104(%rbp)
	movq	%rax, 104(%r15)
	testq	%rdi, %rdi
	je	.L2704
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2704
	movq	(%rdi), %rax
	call	*24(%rax)
.L2704:
	leaq	.LC32(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r12
	cmpq	-120(%rbp), %rdi
	je	.L2708
	call	_ZdlPv@PLT
.L2708:
	testq	%r12, %r12
	je	.L2709
	leaq	.LC32(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol8Debugger8Location9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rax
	movq	112(%r15), %rdi
	movq	$0, -104(%rbp)
	movq	%rax, 112(%r15)
	testq	%rdi, %rdi
	je	.L2709
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2709
	movq	(%rdi), %rax
	call	*24(%rax)
.L2709:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L2743
	movq	%r15, (%r14)
	jmp	.L2686
	.p2align 4,,10
	.p2align 3
.L2743:
	movq	(%r15), %rax
	movq	$0, (%r14)
	leaq	_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2744
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol8Debugger5ScopeD1Ev
	movl	$120, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2686
	.p2align 4,,10
	.p2align 3
.L2699:
	call	*%rdx
	jmp	.L2695
	.p2align 4,,10
	.p2align 3
.L2696:
	call	*%rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rax
	jmp	.L2697
	.p2align 4,,10
	.p2align 3
.L2744:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L2686
.L2742:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6042:
	.size	_ZN12v8_inspector8protocol8Debugger5Scope9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger5Scope9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol8Debugger13BreakLocation9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger13BreakLocation9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger13BreakLocation9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger13BreakLocation9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB6056:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L2746
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r14
	je	.L2747
.L2746:
	leaq	.LC15(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, (%r12)
.L2745:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2777
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2747:
	.cfi_restore_state
	movl	$112, %edi
	leaq	-96(%rbp), %r13
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%rax, %r15
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger13BreakLocationE(%rip), %rax
	movq	%rax, (%r15)
	leaq	24(%r15), %rax
	movq	%rax, -120(%rbp)
	movq	%rax, 8(%r15)
	xorl	%eax, %eax
	cmpl	$6, 8(%r14)
	movw	%ax, 24(%r15)
	leaq	88(%r15), %rax
	movq	%rax, -128(%rbp)
	movq	%rax, 72(%r15)
	movl	$0, %eax
	cmovne	%rax, %r14
	movups	%xmm0, 88(%r15)
	movq	$0, 16(%r15)
	movq	$0, 40(%r15)
	movb	$0, 52(%r15)
	movl	$0, 56(%r15)
	movb	$0, 64(%r15)
	movq	$0, 104(%r15)
	movq	$0, 80(%r15)
	movl	$0, 48(%r15)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC22(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -112(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -104(%rbp)
	cmpq	%rax, %rdi
	je	.L2750
	call	_ZdlPv@PLT
.L2750:
	leaq	.LC22(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-112(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	8(%r15), %rdi
	movq	%r13, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 40(%r15)
	cmpq	-104(%rbp), %rdi
	je	.L2751
	call	_ZdlPv@PLT
.L2751:
	leaq	.LC18(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -112(%rbp)
	cmpq	-104(%rbp), %rdi
	je	.L2752
	call	_ZdlPv@PLT
.L2752:
	leaq	.LC18(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-112(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC19(%rip), %rsi
	movq	%r13, %rdi
	movl	%eax, 48(%r15)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r8
	cmpq	-104(%rbp), %rdi
	je	.L2753
	movq	%rax, -112(%rbp)
	call	_ZdlPv@PLT
	movq	-112(%rbp), %r8
.L2753:
	testq	%r8, %r8
	movq	%r8, -112(%rbp)
	je	.L2754
	leaq	.LC19(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-112(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movb	$1, 52(%r15)
	movl	%eax, 56(%r15)
.L2754:
	leaq	.LC28(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-104(%rbp), %rdi
	je	.L2755
	call	_ZdlPv@PLT
.L2755:
	testq	%r14, %r14
	je	.L2756
	leaq	.LC28(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	72(%r15), %rdi
	movq	%r13, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movb	$1, 64(%r15)
	movq	-96(%rbp), %rdi
	movq	%rax, 104(%r15)
	cmpq	-104(%rbp), %rdi
	je	.L2756
	call	_ZdlPv@PLT
.L2756:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L2778
	movq	%r15, (%r12)
	jmp	.L2745
	.p2align 4,,10
	.p2align 3
.L2778:
	movq	(%r15), %rax
	movq	$0, (%r12)
	leaq	_ZN12v8_inspector8protocol8Debugger13BreakLocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2779
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger13BreakLocationE(%rip), %rax
	movq	72(%r15), %rdi
	movq	%rax, (%r15)
	cmpq	%rdi, -128(%rbp)
	je	.L2758
	call	_ZdlPv@PLT
.L2758:
	movq	8(%r15), %rdi
	cmpq	%rdi, -120(%rbp)
	je	.L2759
	call	_ZdlPv@PLT
.L2759:
	movl	$112, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2745
	.p2align 4,,10
	.p2align 3
.L2779:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L2745
.L2777:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6056:
	.size	_ZN12v8_inspector8protocol8Debugger13BreakLocation9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger13BreakLocation9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC33:
	.string	"url"
.LC34:
	.string	"startLine"
.LC35:
	.string	"startColumn"
.LC36:
	.string	"endLine"
.LC37:
	.string	"endColumn"
.LC38:
	.string	"executionContextId"
.LC39:
	.string	"hash"
.LC40:
	.string	"executionContextAuxData"
.LC41:
	.string	"sourceMapURL"
.LC42:
	.string	"hasSourceURL"
.LC43:
	.string	"isModule"
.LC44:
	.string	"length"
	.section	.text._ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB6065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L2781
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r14
	je	.L2782
.L2781:
	leaq	.LC15(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, (%rbx)
.L2780:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2850
	addq	$88, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2782:
	.cfi_restore_state
	movl	$232, %edi
	leaq	-96(%rbp), %r15
	call	_Znwm@PLT
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	pxor	%xmm0, %xmm0
	movq	%rax, %r12
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	movq	%rax, 8(%r12)
	xorl	%eax, %eax
	cmpl	$6, 8(%r14)
	movw	%ax, 24(%r12)
	leaq	64(%r12), %rax
	movq	%rax, 48(%r12)
	leaq	128(%r12), %rax
	movq	%rax, 112(%r12)
	leaq	184(%r12), %rax
	movq	%rax, 168(%r12)
	movl	$0, %eax
	cmovne	%rax, %r14
	movw	%dx, 64(%r12)
	movups	%xmm0, 184(%r12)
	pxor	%xmm0, %xmm0
	movw	%cx, 128(%r12)
	movups	%xmm0, 88(%r12)
	movq	$0, 16(%r12)
	movq	$0, 40(%r12)
	movq	$0, 56(%r12)
	movq	$0, 80(%r12)
	movq	$0, 120(%r12)
	movq	$0, 144(%r12)
	movq	$0, 152(%r12)
	movb	$0, 160(%r12)
	movq	$0, 200(%r12)
	movq	$0, 176(%r12)
	movl	$0, 208(%r12)
	movb	$0, 212(%r12)
	movl	$0, 216(%r12)
	movq	$0, 224(%r12)
	movl	$0, 104(%r12)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC22(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -128(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rdi
	je	.L2785
	call	_ZdlPv@PLT
.L2785:
	leaq	.LC22(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	8(%r12), %rdi
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 40(%r12)
	cmpq	-120(%rbp), %rdi
	je	.L2786
	call	_ZdlPv@PLT
.L2786:
	leaq	.LC33(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -128(%rbp)
	cmpq	-120(%rbp), %rdi
	je	.L2787
	call	_ZdlPv@PLT
.L2787:
	leaq	.LC33(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	48(%r12), %rdi
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 80(%r12)
	cmpq	-120(%rbp), %rdi
	je	.L2788
	call	_ZdlPv@PLT
.L2788:
	leaq	.LC34(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -128(%rbp)
	cmpq	-120(%rbp), %rdi
	je	.L2789
	call	_ZdlPv@PLT
.L2789:
	leaq	.LC34(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC35(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, 88(%r12)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -128(%rbp)
	cmpq	-120(%rbp), %rdi
	je	.L2790
	call	_ZdlPv@PLT
.L2790:
	leaq	.LC35(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC36(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, 92(%r12)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -128(%rbp)
	cmpq	-120(%rbp), %rdi
	je	.L2791
	call	_ZdlPv@PLT
.L2791:
	leaq	.LC36(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC37(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, 96(%r12)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -128(%rbp)
	cmpq	-120(%rbp), %rdi
	je	.L2792
	call	_ZdlPv@PLT
.L2792:
	leaq	.LC37(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC38(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, 100(%r12)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -128(%rbp)
	cmpq	-120(%rbp), %rdi
	je	.L2793
	call	_ZdlPv@PLT
.L2793:
	leaq	.LC38(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC39(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, 104(%r12)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -128(%rbp)
	cmpq	-120(%rbp), %rdi
	je	.L2794
	call	_ZdlPv@PLT
.L2794:
	leaq	.LC39(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	112(%r12), %rdi
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 144(%r12)
	cmpq	-120(%rbp), %rdi
	je	.L2795
	call	_ZdlPv@PLT
.L2795:
	leaq	.LC40(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L2796
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
.L2796:
	testq	%rax, %rax
	movq	%rax, -128(%rbp)
	je	.L2797
	leaq	.LC40(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rax
	leaq	-104(%rbp), %rdi
	movq	%r13, %rdx
	movq	%rax, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS0_15DictionaryValueEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rax
	movq	152(%r12), %rdi
	movq	$0, -104(%rbp)
	movq	%rax, 152(%r12)
	testq	%rdi, %rdi
	je	.L2797
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2797
	movq	(%rdi), %rax
	call	*24(%rax)
.L2797:
	leaq	.LC41(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L2801
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
.L2801:
	testq	%rax, %rax
	movq	%rax, -128(%rbp)
	je	.L2802
	leaq	.LC41(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rax
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	168(%r12), %rdi
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movb	$1, 160(%r12)
	movq	%rax, 200(%r12)
	cmpq	-120(%rbp), %rdi
	je	.L2802
	call	_ZdlPv@PLT
.L2802:
	leaq	.LC42(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L2804
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
.L2804:
	testq	%rax, %rax
	movq	%rax, -128(%rbp)
	je	.L2805
	leaq	.LC42(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsIbE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movb	$1, 208(%r12)
	movb	%al, 209(%r12)
.L2805:
	leaq	.LC43(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L2806
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
.L2806:
	testq	%rax, %rax
	movq	%rax, -128(%rbp)
	je	.L2807
	leaq	.LC43(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsIbE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movb	$1, 210(%r12)
	movb	%al, 211(%r12)
.L2807:
	leaq	.LC44(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L2808
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
.L2808:
	testq	%rax, %rax
	movq	%rax, -128(%rbp)
	je	.L2809
	leaq	.LC44(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movb	$1, 212(%r12)
	movl	%eax, 216(%r12)
.L2809:
	leaq	.LC17(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-120(%rbp), %rdi
	je	.L2810
	call	_ZdlPv@PLT
.L2810:
	testq	%r14, %r14
	je	.L2811
	leaq	.LC17(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-104(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol7Runtime10StackTrace9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE@PLT
	movq	-104(%rbp), %rax
	movq	224(%r12), %rdi
	movq	$0, -104(%rbp)
	movq	%rax, 224(%r12)
	testq	%rdi, %rdi
	je	.L2811
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2811
	movq	(%rdi), %rax
	call	*24(%rax)
.L2811:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L2851
	movq	%r12, (%rbx)
	jmp	.L2780
	.p2align 4,,10
	.p2align 3
.L2851:
	movq	(%r12), %rax
	movq	$0, (%rbx)
	leaq	_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2852
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationD1Ev
	movl	$232, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2780
	.p2align 4,,10
	.p2align 3
.L2852:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2780
.L2850:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6065:
	.size	_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger24ScriptParsedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC45:
	.string	"isLiveEdit"
	.section	.text._ZN12v8_inspector8protocol8Debugger24ScriptParsedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB6068:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L2854
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r14
	je	.L2855
.L2854:
	leaq	.LC15(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, (%rbx)
.L2853:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2928
	addq	$88, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2855:
	.cfi_restore_state
	movl	$240, %edi
	leaq	-96(%rbp), %r15
	call	_Znwm@PLT
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rax, %r12
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger24ScriptParsedNotificationE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	movq	%rax, 8(%r12)
	xorl	%eax, %eax
	cmpl	$6, 8(%r14)
	movw	%ax, 24(%r12)
	leaq	64(%r12), %rax
	movq	%rax, 48(%r12)
	leaq	128(%r12), %rax
	movq	%rax, 112(%r12)
	leaq	192(%r12), %rax
	movq	%rax, 176(%r12)
	movl	$0, %eax
	cmovne	%rax, %r14
	movw	%dx, 64(%r12)
	movups	%xmm0, 192(%r12)
	pxor	%xmm0, %xmm0
	movw	%cx, 128(%r12)
	movups	%xmm0, 88(%r12)
	movw	%si, 160(%r12)
	movq	$0, 16(%r12)
	movq	$0, 40(%r12)
	movq	$0, 56(%r12)
	movq	$0, 80(%r12)
	movq	$0, 120(%r12)
	movq	$0, 144(%r12)
	movq	$0, 152(%r12)
	movb	$0, 168(%r12)
	movq	$0, 208(%r12)
	movq	$0, 184(%r12)
	movl	$0, 216(%r12)
	movb	$0, 220(%r12)
	movl	$0, 224(%r12)
	movq	$0, 232(%r12)
	movl	$0, 104(%r12)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC22(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -128(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rdi
	je	.L2858
	call	_ZdlPv@PLT
.L2858:
	leaq	.LC22(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	8(%r12), %rdi
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 40(%r12)
	cmpq	-120(%rbp), %rdi
	je	.L2859
	call	_ZdlPv@PLT
.L2859:
	leaq	.LC33(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -128(%rbp)
	cmpq	-120(%rbp), %rdi
	je	.L2860
	call	_ZdlPv@PLT
.L2860:
	leaq	.LC33(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	48(%r12), %rdi
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 80(%r12)
	cmpq	-120(%rbp), %rdi
	je	.L2861
	call	_ZdlPv@PLT
.L2861:
	leaq	.LC34(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -128(%rbp)
	cmpq	-120(%rbp), %rdi
	je	.L2862
	call	_ZdlPv@PLT
.L2862:
	leaq	.LC34(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC35(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, 88(%r12)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -128(%rbp)
	cmpq	-120(%rbp), %rdi
	je	.L2863
	call	_ZdlPv@PLT
.L2863:
	leaq	.LC35(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC36(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, 92(%r12)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -128(%rbp)
	cmpq	-120(%rbp), %rdi
	je	.L2864
	call	_ZdlPv@PLT
.L2864:
	leaq	.LC36(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC37(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, 96(%r12)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -128(%rbp)
	cmpq	-120(%rbp), %rdi
	je	.L2865
	call	_ZdlPv@PLT
.L2865:
	leaq	.LC37(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC38(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, 100(%r12)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -128(%rbp)
	cmpq	-120(%rbp), %rdi
	je	.L2866
	call	_ZdlPv@PLT
.L2866:
	leaq	.LC38(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC39(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, 104(%r12)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -128(%rbp)
	cmpq	-120(%rbp), %rdi
	je	.L2867
	call	_ZdlPv@PLT
.L2867:
	leaq	.LC39(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	112(%r12), %rdi
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 144(%r12)
	cmpq	-120(%rbp), %rdi
	je	.L2868
	call	_ZdlPv@PLT
.L2868:
	leaq	.LC40(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L2869
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
.L2869:
	testq	%rax, %rax
	movq	%rax, -128(%rbp)
	je	.L2870
	leaq	.LC40(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rax
	leaq	-104(%rbp), %rdi
	movq	%r13, %rdx
	movq	%rax, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS0_15DictionaryValueEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rax
	movq	152(%r12), %rdi
	movq	$0, -104(%rbp)
	movq	%rax, 152(%r12)
	testq	%rdi, %rdi
	je	.L2870
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2870
	movq	(%rdi), %rax
	call	*24(%rax)
.L2870:
	leaq	.LC45(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L2874
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
.L2874:
	testq	%rax, %rax
	movq	%rax, -128(%rbp)
	je	.L2875
	leaq	.LC45(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsIbE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movb	$1, 160(%r12)
	movb	%al, 161(%r12)
.L2875:
	leaq	.LC41(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L2876
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
.L2876:
	testq	%rax, %rax
	movq	%rax, -128(%rbp)
	je	.L2877
	leaq	.LC41(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rax
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	176(%r12), %rdi
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movb	$1, 168(%r12)
	movq	%rax, 208(%r12)
	cmpq	-120(%rbp), %rdi
	je	.L2877
	call	_ZdlPv@PLT
.L2877:
	leaq	.LC42(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L2879
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
.L2879:
	testq	%rax, %rax
	movq	%rax, -128(%rbp)
	je	.L2880
	leaq	.LC42(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsIbE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movb	$1, 216(%r12)
	movb	%al, 217(%r12)
.L2880:
	leaq	.LC43(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L2881
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
.L2881:
	testq	%rax, %rax
	movq	%rax, -128(%rbp)
	je	.L2882
	leaq	.LC43(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsIbE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movb	$1, 218(%r12)
	movb	%al, 219(%r12)
.L2882:
	leaq	.LC44(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L2883
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
.L2883:
	testq	%rax, %rax
	movq	%rax, -128(%rbp)
	je	.L2884
	leaq	.LC44(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movb	$1, 220(%r12)
	movl	%eax, 224(%r12)
.L2884:
	leaq	.LC17(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-120(%rbp), %rdi
	je	.L2885
	call	_ZdlPv@PLT
.L2885:
	testq	%r14, %r14
	je	.L2886
	leaq	.LC17(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-104(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol7Runtime10StackTrace9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE@PLT
	movq	-104(%rbp), %rax
	movq	232(%r12), %rdi
	movq	$0, -104(%rbp)
	movq	%rax, 232(%r12)
	testq	%rdi, %rdi
	je	.L2886
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2886
	movq	(%rdi), %rax
	call	*24(%rax)
.L2886:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L2929
	movq	%r12, (%rbx)
	jmp	.L2853
	.p2align 4,,10
	.p2align 3
.L2929:
	movq	(%r12), %rax
	movq	$0, (%rbx)
	leaq	_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotificationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2930
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotificationD1Ev
	movl	$240, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2853
	.p2align 4,,10
	.p2align 3
.L2930:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2853
.L2928:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6068:
	.size	_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImpl18continueToLocationEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC46:
	.string	"targetCallFrames"
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl18continueToLocationEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl18continueToLocationEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl18continueToLocationEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl18continueToLocationEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6144:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$232, %rsp
	movl	%esi, -244(%rbp)
	movq	(%r8), %r13
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -256(%rbp)
	movq	%rcx, -264(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L2932
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r13
	jne	.L2933
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L2979
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L2936:
	leaq	.LC26(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, -272(%rbp)
	cmpq	%r15, %rdi
	je	.L2938
	call	_ZdlPv@PLT
.L2938:
	leaq	.LC26(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-272(%rbp), %rsi
	movq	%rbx, %rdx
	leaq	-232(%rbp), %rdi
	call	_ZN12v8_inspector8protocol8Debugger8Location9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC46(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%r15, %rdi
	je	.L2954
	movq	%r8, -272(%rbp)
	call	_ZdlPv@PLT
	movq	-272(%rbp), %r8
.L2954:
	xorl	%edx, %edx
	testq	%r8, %r8
	leaq	-184(%rbp), %r13
	movb	$0, -208(%rbp)
	movq	%r13, -200(%rbp)
	movq	$0, -192(%rbp)
	movw	%dx, -184(%rbp)
	movq	$0, -168(%rbp)
	movq	%r8, -272(%rbp)
	je	.L2937
	leaq	.LC46(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-272(%rbp), %r8
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	-200(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-80(%rbp), %rax
	movq	-112(%rbp), %rdi
	movb	$1, -208(%rbp)
	movq	%rax, -168(%rbp)
	cmpq	%r15, %rdi
	je	.L2937
	call	_ZdlPv@PLT
.L2937:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L2980
	movq	%r14, %rsi
	leaq	-224(%rbp), %rdi
	leaq	-136(%rbp), %rbx
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r14), %rsi
	movq	(%rsi), %rax
	movq	%rbx, -152(%rbp)
	movq	16(%rax), %r8
	movzbl	-208(%rbp), %eax
	movb	%al, -160(%rbp)
	movq	-200(%rbp), %rax
	cmpq	%r13, %rax
	je	.L2981
	movq	%rax, -152(%rbp)
	movq	-184(%rbp), %rax
	movq	%rax, -136(%rbp)
.L2944:
	movq	-192(%rbp), %rax
	movq	%r12, %rdi
	movq	%r13, -200(%rbp)
	leaq	-160(%rbp), %rcx
	movq	$0, -192(%rbp)
	leaq	-216(%rbp), %rdx
	movq	%rax, -144(%rbp)
	xorl	%eax, %eax
	movw	%ax, -184(%rbp)
	movq	-168(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-232(%rbp), %rax
	movq	$0, -232(%rbp)
	movq	%rax, -216(%rbp)
	call	*%r8
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2945
	movq	(%rdi), %rax
	call	*24(%rax)
.L2945:
	movq	-152(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2946
	call	_ZdlPv@PLT
.L2946:
	cmpl	$2, -112(%rbp)
	je	.L2982
	movq	-224(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2948
	movl	-244(%rbp), %esi
	movq	%r12, %rdx
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L2948:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2949
	call	_ZdlPv@PLT
.L2949:
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2942
	call	_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0
.L2942:
	movq	-200(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L2951
	call	_ZdlPv@PLT
.L2951:
	movq	-232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2931
	movq	(%rdi), %rax
	call	*24(%rax)
.L2931:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2983
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2979:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L2936
	.p2align 4,,10
	.p2align 3
.L2932:
	movq	-112(%rbp), %rdi
.L2933:
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L2984
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L2935:
	leaq	.LC26(%rip), %rsi
	movq	%rbx, %rdi
	leaq	-184(%rbp), %r13
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-232(%rbp), %rdi
	movq	%rbx, %rdx
	xorl	%esi, %esi
	call	_ZN12v8_inspector8protocol8Debugger8Location9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	xorl	%ecx, %ecx
	movb	$0, -208(%rbp)
	movq	%r13, -200(%rbp)
	movq	$0, -192(%rbp)
	movw	%cx, -184(%rbp)
	movq	$0, -168(%rbp)
	jmp	.L2937
	.p2align 4,,10
	.p2align 3
.L2980:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	-244(%rbp), %esi
	movl	$-32602, %edx
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L2942
	call	_ZdlPv@PLT
	jmp	.L2942
	.p2align 4,,10
	.p2align 3
.L2984:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L2935
	.p2align 4,,10
	.p2align 3
.L2981:
	movdqu	-184(%rbp), %xmm0
	movups	%xmm0, -136(%rbp)
	jmp	.L2944
	.p2align 4,,10
	.p2align 3
.L2982:
	movq	8(%r14), %rdi
	movq	-264(%rbp), %rcx
	movq	-256(%rbp), %rdx
	movl	-244(%rbp), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L2948
.L2983:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6144:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl18continueToLocationEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl18continueToLocationEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImpl19evaluateOnCallFrameEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC47:
	.string	"expression"
.LC48:
	.string	"objectGroup"
.LC49:
	.string	"includeCommandLineAPI"
.LC50:
	.string	"silent"
.LC51:
	.string	"returnByValue"
.LC52:
	.string	"generatePreview"
.LC53:
	.string	"throwOnSideEffect"
.LC54:
	.string	"timeout"
.LC55:
	.string	"result"
.LC56:
	.string	"exceptionDetails"
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl19evaluateOnCallFrameEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl19evaluateOnCallFrameEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl19evaluateOnCallFrameEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl19evaluateOnCallFrameEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$408, %rsp
	movl	%esi, -388(%rbp)
	movq	(%r8), %r13
	leaq	.LC0(%rip), %rsi
	movq	%rdi, -384(%rbp)
	movq	%r12, %rdi
	movq	%rdx, -432(%rbp)
	movq	%rcx, -440(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L2986
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r13
	jne	.L2987
	leaq	-96(%rbp), %r14
	cmpq	%r14, %rdi
	je	.L3083
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L2990:
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r15
	cmpq	%r14, %rdi
	je	.L2992
	call	_ZdlPv@PLT
.L2992:
	leaq	.LC14(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-304(%rbp), %rax
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -416(%rbp)
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC47(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r15
	cmpq	%r14, %rdi
	je	.L2993
	call	_ZdlPv@PLT
.L2993:
	leaq	.LC47(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-256(%rbp), %rax
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -424(%rbp)
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC48(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r15
	cmpq	%r14, %rdi
	je	.L3041
	call	_ZdlPv@PLT
.L3041:
	leaq	-184(%rbp), %rax
	xorl	%ecx, %ecx
	movb	$0, -208(%rbp)
	movq	%rax, -376(%rbp)
	movq	%rax, -200(%rbp)
	movq	$0, -192(%rbp)
	movw	%cx, -184(%rbp)
	movq	$0, -168(%rbp)
	testq	%r15, %r15
	je	.L2994
	leaq	.LC48(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	-200(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-80(%rbp), %rax
	movq	-112(%rbp), %rdi
	movb	$1, -208(%rbp)
	movq	%rax, -168(%rbp)
	cmpq	%r14, %rdi
	je	.L2994
	call	_ZdlPv@PLT
.L2994:
	leaq	.LC49(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r15
	cmpq	%r14, %rdi
	je	.L3039
	call	_ZdlPv@PLT
.L3039:
	testq	%r15, %r15
	je	.L3048
	leaq	.LC49(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsIbE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movb	$1, -391(%rbp)
	movb	%al, -392(%rbp)
.L2996:
	leaq	.LC50(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r15
	cmpq	%r14, %rdi
	je	.L3037
	call	_ZdlPv@PLT
.L3037:
	testq	%r15, %r15
	je	.L3049
	leaq	.LC50(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsIbE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movb	$1, -389(%rbp)
	movb	%al, -390(%rbp)
.L2997:
	leaq	.LC51(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r15
	cmpq	%r14, %rdi
	je	.L3035
	call	_ZdlPv@PLT
.L3035:
	testq	%r15, %r15
	je	.L3050
	leaq	.LC51(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsIbE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movb	$1, -404(%rbp)
	movb	%al, -402(%rbp)
.L2998:
	leaq	.LC52(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r15
	cmpq	%r14, %rdi
	je	.L3033
	call	_ZdlPv@PLT
.L3033:
	testq	%r15, %r15
	je	.L3051
	leaq	.LC52(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsIbE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movb	$1, -405(%rbp)
	movb	%al, -403(%rbp)
.L2999:
	leaq	.LC53(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r15
	cmpq	%r14, %rdi
	je	.L3031
	call	_ZdlPv@PLT
.L3031:
	testq	%r15, %r15
	je	.L3052
	leaq	.LC53(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movl	$1, %r15d
	call	_ZN12v8_inspector8protocol16ValueConversionsIbE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movb	%al, -401(%rbp)
.L3000:
	leaq	.LC54(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r13
	cmpq	%r14, %rdi
	je	.L3029
	call	_ZdlPv@PLT
.L3029:
	testq	%r13, %r13
	je	.L3053
	leaq	.LC54(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-320(%rbp), %rsi
	movq	%r13, %rdi
	movq	$0x000000000, -320(%rbp)
	movq	0(%r13), %rax
	call	*40(%rax)
	testb	%al, %al
	jne	.L3001
	leaq	.LC24(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L3001:
	movsd	-320(%rbp), %xmm1
	movl	$1, %r13d
	movsd	%xmm1, -400(%rbp)
.L2991:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L3084
	movq	-384(%rbp), %rbx
	leaq	-336(%rbp), %rdi
	movq	$0, -352(%rbp)
	movq	$0, -344(%rbp)
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	movzbl	-401(%rbp), %ecx
	leaq	-136(%rbp), %rbx
	movzbl	-208(%rbp), %edx
	movsd	-400(%rbp), %xmm0
	movq	(%rsi), %rax
	movq	40(%rax), %rax
	movb	%cl, -327(%rbp)
	movzbl	-405(%rbp), %ecx
	movb	%dl, -160(%rbp)
	movb	%r13b, -320(%rbp)
	movq	-200(%rbp), %rdx
	movb	%cl, -354(%rbp)
	movzbl	-403(%rbp), %ecx
	movb	%r15b, -328(%rbp)
	movb	%cl, -353(%rbp)
	movzbl	-404(%rbp), %ecx
	movq	%rbx, -152(%rbp)
	movb	%cl, -356(%rbp)
	movzbl	-402(%rbp), %ecx
	movsd	%xmm0, -312(%rbp)
	movb	%cl, -355(%rbp)
	movzbl	-389(%rbp), %ecx
	movb	%cl, -358(%rbp)
	movzbl	-390(%rbp), %ecx
	movb	%cl, -357(%rbp)
	movzbl	-391(%rbp), %ecx
	movb	%cl, -360(%rbp)
	movzbl	-392(%rbp), %ecx
	movb	%cl, -359(%rbp)
	cmpq	-376(%rbp), %rdx
	je	.L3085
	movq	%rdx, -152(%rbp)
	movq	-184(%rbp), %rdx
	movq	%rdx, -136(%rbp)
.L3006:
	movq	-192(%rbp), %rdx
	subq	$8, %rsp
	leaq	-320(%rbp), %r13
	leaq	-328(%rbp), %r15
	movq	-376(%rbp), %rcx
	movq	%r12, %rdi
	leaq	-160(%rbp), %r14
	movq	$0, -192(%rbp)
	movq	%rdx, -144(%rbp)
	xorl	%edx, %edx
	leaq	-360(%rbp), %r9
	movq	%r14, %r8
	movw	%dx, -184(%rbp)
	movq	-168(%rbp), %rdx
	movq	%rcx, -200(%rbp)
	movq	-424(%rbp), %rcx
	movq	%rdx, -120(%rbp)
	leaq	-344(%rbp), %rdx
	pushq	%rdx
	leaq	-352(%rbp), %rdx
	pushq	%rdx
	leaq	-354(%rbp), %rdx
	pushq	%r13
	pushq	%r15
	pushq	%rdx
	leaq	-356(%rbp), %rdx
	pushq	%rdx
	leaq	-358(%rbp), %rdx
	pushq	%rdx
	movq	-416(%rbp), %rdx
	call	*%rax
	movq	-152(%rbp), %rdi
	addq	$64, %rsp
	cmpq	%rbx, %rdi
	je	.L3007
	call	_ZdlPv@PLT
.L3007:
	cmpl	$2, -112(%rbp)
	je	.L3086
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L3087
.L3015:
	movq	-336(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3088
	movl	-388(%rbp), %esi
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%rbx, -320(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE@PLT
	movq	-320(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3009
	movq	(%rdi), %rax
	call	*24(%rax)
.L3009:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3017
	call	_ZdlPv@PLT
.L3017:
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3018
	call	_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0
.L3018:
	movq	-344(%rbp), %r12
	testq	%r12, %r12
	je	.L3019
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3020
	call	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD1Ev
	movl	$184, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3019:
	movq	-352(%rbp), %r12
	testq	%r12, %r12
	je	.L3004
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3022
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3004:
	movq	-200(%rbp), %rdi
	cmpq	-376(%rbp), %rdi
	je	.L3023
	call	_ZdlPv@PLT
.L3023:
	movq	-256(%rbp), %rdi
	leaq	-240(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3024
	call	_ZdlPv@PLT
.L3024:
	movq	-304(%rbp), %rdi
	leaq	-288(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2985
	call	_ZdlPv@PLT
.L2985:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3089
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3083:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L2990
	.p2align 4,,10
	.p2align 3
.L2986:
	movq	-112(%rbp), %rdi
.L2987:
	leaq	-96(%rbp), %r14
	cmpq	%r14, %rdi
	je	.L3090
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L2989:
	leaq	.LC14(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-304(%rbp), %rax
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, -416(%rbp)
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC47(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-256(%rbp), %rax
	xorl	%esi, %esi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	movq	%rax, -424(%rbp)
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	-184(%rbp), %rax
	xorl	%esi, %esi
	pxor	%xmm2, %xmm2
	movb	$0, -208(%rbp)
	movq	%rax, -376(%rbp)
	movq	%rax, -200(%rbp)
	movq	$0, -192(%rbp)
	movw	%si, -184(%rbp)
	movq	$0, -168(%rbp)
	movb	$0, -401(%rbp)
	movb	$0, -403(%rbp)
	movb	$0, -405(%rbp)
	movb	$0, -402(%rbp)
	movb	$0, -404(%rbp)
	movb	$0, -390(%rbp)
	movb	$0, -389(%rbp)
	movb	$0, -392(%rbp)
	movb	$0, -391(%rbp)
	movsd	%xmm2, -400(%rbp)
	jmp	.L2991
	.p2align 4,,10
	.p2align 3
.L3084:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	$-32602, %edx
	movq	-384(%rbp), %rdi
	movl	-388(%rbp), %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3004
	call	_ZdlPv@PLT
	jmp	.L3004
	.p2align 4,,10
	.p2align 3
.L3088:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*24(%rax)
	jmp	.L3009
	.p2align 4,,10
	.p2align 3
.L3086:
	movq	-384(%rbp), %rax
	movq	-440(%rbp), %rcx
	movq	-432(%rbp), %rdx
	movl	-388(%rbp), %esi
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L3009
	.p2align 4,,10
	.p2align 3
.L3090:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L2989
	.p2align 4,,10
	.p2align 3
.L3085:
	movdqu	-184(%rbp), %xmm3
	movups	%xmm3, -136(%rbp)
	jmp	.L3006
	.p2align 4,,10
	.p2align 3
.L3087:
	movq	-352(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNK12v8_inspector8protocol7Runtime12RemoteObject7toValueEv@PLT
	movq	-320(%rbp), %rax
	leaq	.LC55(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	movq	%rax, -384(%rbp)
	cmpq	%rax, %rdi
	je	.L3011
	call	_ZdlPv@PLT
.L3011:
	movq	-328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3012
	movq	(%rdi), %rax
	call	*24(%rax)
.L3012:
	movq	-344(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L3015
	movq	%r13, %rdi
	call	_ZNK12v8_inspector8protocol7Runtime16ExceptionDetails7toValueEv@PLT
	movq	-320(%rbp), %rax
	leaq	.LC56(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	cmpq	-384(%rbp), %rdi
	je	.L3013
	call	_ZdlPv@PLT
.L3013:
	movq	-328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3015
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L3015
	.p2align 4,,10
	.p2align 3
.L3053:
	pxor	%xmm4, %xmm4
	xorl	%r13d, %r13d
	movsd	%xmm4, -400(%rbp)
	jmp	.L2991
	.p2align 4,,10
	.p2align 3
.L3048:
	movb	$0, -392(%rbp)
	movb	$0, -391(%rbp)
	jmp	.L2996
	.p2align 4,,10
	.p2align 3
.L3052:
	movb	$0, -401(%rbp)
	xorl	%r15d, %r15d
	jmp	.L3000
	.p2align 4,,10
	.p2align 3
.L3051:
	movb	$0, -403(%rbp)
	movb	$0, -405(%rbp)
	jmp	.L2999
	.p2align 4,,10
	.p2align 3
.L3050:
	movb	$0, -402(%rbp)
	movb	$0, -404(%rbp)
	jmp	.L2998
	.p2align 4,,10
	.p2align 3
.L3049:
	movb	$0, -390(%rbp)
	movb	$0, -389(%rbp)
	jmp	.L2997
	.p2align 4,,10
	.p2align 3
.L3022:
	call	*%rax
	jmp	.L3004
	.p2align 4,,10
	.p2align 3
.L3020:
	call	*%rax
	jmp	.L3019
.L3089:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6154:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl19evaluateOnCallFrameEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl19evaluateOnCallFrameEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger8Frontend18breakpointResolvedERKNS_8String16ESt10unique_ptrINS1_8LocationESt14default_deleteIS7_EE.str1.1,"aMS",@progbits,1
.LC57:
	.string	"Debugger.breakpointResolved"
	.section	.text._ZN12v8_inspector8protocol8Debugger8Frontend18breakpointResolvedERKNS_8String16ESt10unique_ptrINS1_8LocationESt14default_deleteIS7_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger8Frontend18breakpointResolvedERKNS_8String16ESt10unique_ptrINS1_8LocationESt14default_deleteIS7_EE
	.type	_ZN12v8_inspector8protocol8Debugger8Frontend18breakpointResolvedERKNS_8String16ESt10unique_ptrINS1_8LocationESt14default_deleteIS7_EE, @function
_ZN12v8_inspector8protocol8Debugger8Frontend18breakpointResolvedERKNS_8String16ESt10unique_ptrINS1_8LocationESt14default_deleteIS7_EE:
.LFB6071:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, (%rdi)
	je	.L3091
	movq	%rdi, %r12
	movl	$56, %edi
	movq	%rdx, %r13
	movq	%rsi, %r14
	call	_Znwm@PLT
	movq	%r14, %rsi
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	24(%rbx), %rax
	leaq	8(%rbx), %rdi
	movq	%rax, 8(%rbx)
	xorl	%eax, %eax
	movw	%ax, 24(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 48(%rbx)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r14), %rax
	movq	48(%rbx), %rdi
	movq	%rax, 40(%rbx)
	movq	0(%r13), %rax
	movq	$0, 0(%r13)
	movq	%rax, 48(%rbx)
	testq	%rdi, %rdi
	je	.L3093
	movq	(%rdi), %rax
	call	*24(%rax)
.L3093:
	movq	(%r12), %r12
	leaq	-80(%rbp), %r13
	leaq	.LC57(%rip), %rsi
	movq	%r13, %rdi
	movq	(%r12), %rax
	movq	%rbx, -104(%rbp)
	movq	24(%rax), %r14
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	-96(%rbp), %rdi
	leaq	-104(%rbp), %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16InternalResponse18createNotificationERKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rax
	movq	%r12, %rdi
	leaq	-88(%rbp), %rsi
	movq	$0, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	*%r14
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3094
	movq	(%rdi), %rax
	call	*24(%rax)
.L3094:
	movq	-96(%rbp), %r12
	testq	%r12, %r12
	je	.L3095
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol16InternalResponseD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3096
	movq	56(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L3097
	movq	(%rdi), %rax
	call	*24(%rax)
.L3097:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3098
	call	_ZdlPv@PLT
.L3098:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3095:
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3099
	call	_ZdlPv@PLT
.L3099:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3091
	movq	(%rdi), %rax
	call	*24(%rax)
.L3091:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3118
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3096:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3095
.L3118:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6071:
	.size	_ZN12v8_inspector8protocol8Debugger8Frontend18breakpointResolvedERKNS_8String16ESt10unique_ptrINS1_8LocationESt14default_deleteIS7_EE, .-_ZN12v8_inspector8protocol8Debugger8Frontend18breakpointResolvedERKNS_8String16ESt10unique_ptrINS1_8LocationESt14default_deleteIS7_EE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger8Frontend6pausedESt10unique_ptrISt6vectorIS3_INS1_9CallFrameESt14default_deleteIS5_EESaIS8_EES6_ISA_EERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS0_15DictionaryValueEEENSJ_IS4_ISD_SaISD_EEEENSJ_INS0_7Runtime10StackTraceEEENSJ_INSP_12StackTraceIdEEEST_.str1.1,"aMS",@progbits,1
.LC58:
	.string	"Debugger.paused"
	.section	.text._ZN12v8_inspector8protocol8Debugger8Frontend6pausedESt10unique_ptrISt6vectorIS3_INS1_9CallFrameESt14default_deleteIS5_EESaIS8_EES6_ISA_EERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS0_15DictionaryValueEEENSJ_IS4_ISD_SaISD_EEEENSJ_INS0_7Runtime10StackTraceEEENSJ_INSP_12StackTraceIdEEEST_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger8Frontend6pausedESt10unique_ptrISt6vectorIS3_INS1_9CallFrameESt14default_deleteIS5_EESaIS8_EES6_ISA_EERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS0_15DictionaryValueEEENSJ_IS4_ISD_SaISD_EEEENSJ_INS0_7Runtime10StackTraceEEENSJ_INSP_12StackTraceIdEEEST_
	.type	_ZN12v8_inspector8protocol8Debugger8Frontend6pausedESt10unique_ptrISt6vectorIS3_INS1_9CallFrameESt14default_deleteIS5_EESaIS8_EES6_ISA_EERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS0_15DictionaryValueEEENSJ_IS4_ISD_SaISD_EEEENSJ_INS0_7Runtime10StackTraceEEENSJ_INSP_12StackTraceIdEEEST_, @function
_ZN12v8_inspector8protocol8Debugger8Frontend6pausedESt10unique_ptrISt6vectorIS3_INS1_9CallFrameESt14default_deleteIS5_EESaIS8_EES6_ISA_EERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS0_15DictionaryValueEEENSJ_IS4_ISD_SaISD_EEEENSJ_INS0_7Runtime10StackTraceEEENSJ_INSP_12StackTraceIdEEEST_:
.LFB6075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rbp), %rax
	movq	16(%rbp), %r14
	movq	%rsi, -160(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%r8, -136(%rbp)
	movq	%rax, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, (%rdi)
	je	.L3119
	movq	%rdi, %rbx
	movl	$96, %edi
	movq	%rdx, %r15
	movq	%r9, %r12
	call	_Znwm@PLT
	movq	-160(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movq	%rax, %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger18PausedNotificationE(%rip), %rax
	leaq	32(%r13), %rdx
	movq	%rax, 0(%r13)
	xorl	%eax, %eax
	leaq	16(%r13), %rdi
	movq	%rdx, 16(%r13)
	movq	(%rsi), %rdx
	movq	$0, 24(%r13)
	movq	$0, (%rsi)
	movq	%r15, %rsi
	movq	%rdx, 8(%r13)
	movw	%ax, 32(%r13)
	movq	$0, 48(%r13)
	movq	$0, 88(%r13)
	movups	%xmm0, 56(%r13)
	movups	%xmm0, 72(%r13)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r15), %rdx
	movq	-144(%rbp), %rcx
	movq	-136(%rbp), %r8
	movq	%rdx, 48(%r13)
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L3122
	movq	56(%r13), %rdi
	movq	$0, (%rcx)
	movq	%rdx, 56(%r13)
	testq	%rdi, %rdi
	je	.L3122
	movq	(%rdi), %rdx
	call	*24(%rdx)
	movq	-136(%rbp), %r8
.L3122:
	movq	(%r8), %rdx
	testq	%rdx, %rdx
	je	.L3125
	movq	64(%r13), %r15
	movq	$0, (%r8)
	movq	%rdx, 64(%r13)
	testq	%r15, %r15
	je	.L3125
	movq	8(%r15), %rdx
	movq	(%r15), %r8
	cmpq	%r8, %rdx
	je	.L3127
	.p2align 4,,10
	.p2align 3
.L3131:
	movq	(%r8), %rdi
	leaq	16(%r8), %rcx
	cmpq	%rcx, %rdi
	je	.L3128
	movq	%r8, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
	movq	-136(%rbp), %rdx
	addq	$40, %r8
	cmpq	%r8, %rdx
	jne	.L3131
.L3129:
	movq	(%r15), %r8
.L3127:
	testq	%r8, %r8
	je	.L3132
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L3132:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3125:
	movq	(%r12), %rdx
	testq	%rdx, %rdx
	je	.L3134
	movq	$0, (%r12)
	movq	72(%r13), %r12
	movq	%rdx, 72(%r13)
	testq	%r12, %r12
	je	.L3134
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rcx
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L3136
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3134:
	movq	(%r14), %rdx
	testq	%rdx, %rdx
	je	.L3138
	movq	80(%r13), %r12
	movq	$0, (%r14)
	movq	%rdx, 80(%r13)
	testq	%r12, %r12
	je	.L3138
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rcx
	movq	24(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L3140
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rdx
	movq	64(%r12), %rdi
	leaq	-64(%rdx), %rax
	movq	%rdx, %xmm1
	leaq	80(%r12), %rdx
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	cmpq	%rdx, %rdi
	je	.L3141
	call	_ZdlPv@PLT
.L3141:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rdx
	cmpq	%rdx, %rdi
	je	.L3142
	call	_ZdlPv@PLT
.L3142:
	movl	$104, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3138:
	movq	-152(%rbp), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L3144
	movq	88(%r13), %r12
	movq	$0, (%rax)
	movq	%rdx, 88(%r13)
	testq	%r12, %r12
	je	.L3144
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rcx
	movq	24(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L3146
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rdx
	movq	64(%r12), %rdi
	leaq	-64(%rdx), %rax
	movq	%rdx, %xmm2
	leaq	80(%r12), %rdx
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r12)
	cmpq	%rdx, %rdi
	je	.L3147
	call	_ZdlPv@PLT
.L3147:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rdx
	cmpq	%rdx, %rdi
	je	.L3148
	call	_ZdlPv@PLT
.L3148:
	movl	$104, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3144:
	movq	(%rbx), %r12
	leaq	.LC58(%rip), %rsi
	movq	(%r12), %rdx
	movq	%r13, -120(%rbp)
	leaq	-96(%rbp), %r13
	movq	%r13, %rdi
	movq	24(%rdx), %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	-112(%rbp), %rdi
	leaq	-120(%rbp), %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16InternalResponse18createNotificationERKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE@PLT
	movq	-112(%rbp), %rax
	movq	%r12, %rdi
	leaq	-104(%rbp), %rsi
	movq	$0, -112(%rbp)
	movq	%rax, -104(%rbp)
	call	*%rbx
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3149
	movq	(%rdi), %rax
	call	*24(%rax)
.L3149:
	movq	-112(%rbp), %r12
	testq	%r12, %r12
	je	.L3150
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol16InternalResponseD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3151
	movq	56(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L3152
	movq	(%rdi), %rax
	call	*24(%rax)
.L3152:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3153
	call	_ZdlPv@PLT
.L3153:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3150:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3154
	call	_ZdlPv@PLT
.L3154:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3119
	movq	(%rdi), %rax
	call	*24(%rax)
.L3119:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3188
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3128:
	.cfi_restore_state
	addq	$40, %r8
	cmpq	%r8, %rdx
	jne	.L3131
	jmp	.L3129
	.p2align 4,,10
	.p2align 3
.L3151:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3150
	.p2align 4,,10
	.p2align 3
.L3146:
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L3144
	.p2align 4,,10
	.p2align 3
.L3136:
	call	*%rdx
	jmp	.L3134
	.p2align 4,,10
	.p2align 3
.L3140:
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L3138
.L3188:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6075:
	.size	_ZN12v8_inspector8protocol8Debugger8Frontend6pausedESt10unique_ptrISt6vectorIS3_INS1_9CallFrameESt14default_deleteIS5_EESaIS8_EES6_ISA_EERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS0_15DictionaryValueEEENSJ_IS4_ISD_SaISD_EEEENSJ_INS0_7Runtime10StackTraceEEENSJ_INSP_12StackTraceIdEEEST_, .-_ZN12v8_inspector8protocol8Debugger8Frontend6pausedESt10unique_ptrISt6vectorIS3_INS1_9CallFrameESt14default_deleteIS5_EESaIS8_EES6_ISA_EERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS0_15DictionaryValueEEENSJ_IS4_ISD_SaISD_EEEENSJ_INS0_7Runtime10StackTraceEEENSJ_INSP_12StackTraceIdEEEST_
	.section	.rodata._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger5ScopeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_.str1.1,"aMS",@progbits,1
.LC59:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger5ScopeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger5ScopeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger5ScopeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger5ScopeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger5ScopeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB10651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdx
	movq	8(%rdi), %rax
	movq	%rdi, -88(%rbp)
	movq	%rax, -80(%rbp)
	subq	%rdx, %rax
	movq	%rdx, -56(%rbp)
	sarq	$3, %rax
	movabsq	$1152921504606846975, %rdx
	cmpq	%rdx, %rax
	je	.L3235
	movq	%rsi, %rcx
	movq	%rsi, %r12
	movq	%rsi, %r13
	subq	-56(%rbp), %rcx
	testq	%rax, %rax
	je	.L3213
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L3236
.L3191:
	movq	%rbx, %rdi
	movq	%rcx, -96(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%rax, -64(%rbp)
	addq	%rax, %rbx
	movq	%rbx, -72(%rbp)
	leaq	8(%rax), %rbx
.L3212:
	movq	(%r15), %rax
	movq	-64(%rbp), %rsi
	movq	$0, (%r15)
	movq	-56(%rbp), %r15
	movq	%rax, (%rsi,%rcx)
	cmpq	%r15, %r12
	je	.L3193
	movq	%r13, -96(%rbp)
	movq	%rsi, %rbx
	jmp	.L3206
	.p2align 4,,10
	.p2align 3
.L3238:
	movq	112(%r14), %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger5ScopeE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r13, %r13
	je	.L3196
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3197
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3198
	call	_ZdlPv@PLT
.L3198:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3196:
	movq	104(%r14), %r13
	testq	%r13, %r13
	je	.L3199
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3200
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3201
	call	_ZdlPv@PLT
.L3201:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3199:
	movq	64(%r14), %rdi
	leaq	80(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3202
	call	_ZdlPv@PLT
.L3202:
	movq	48(%r14), %r13
	testq	%r13, %r13
	je	.L3203
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3204
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3203:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3205
	call	_ZdlPv@PLT
.L3205:
	movl	$120, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3194:
	addq	$8, %r15
	addq	$8, %rbx
	cmpq	%r15, %r12
	je	.L3237
.L3206:
	movq	(%r15), %rax
	movq	$0, (%r15)
	movq	%rax, (%rbx)
	movq	(%r15), %r14
	testq	%r14, %r14
	je	.L3194
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3238
	addq	$8, %r15
	movq	%r14, %rdi
	addq	$8, %rbx
	call	*%rax
	cmpq	%r15, %r12
	jne	.L3206
	.p2align 4,,10
	.p2align 3
.L3237:
	movq	-64(%rbp), %rbx
	movq	%r12, %rax
	movq	-96(%rbp), %r13
	subq	-56(%rbp), %rax
	leaq	8(%rbx,%rax), %rbx
.L3193:
	movq	-80(%rbp), %rax
	cmpq	%rax, %r12
	je	.L3207
	subq	%r12, %rax
	leaq	-8(%rax), %rsi
	movq	%rsi, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rsi, %rsi
	je	.L3215
	movq	%rcx, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L3209:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L3209
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r13
	leaq	(%rbx,%r13), %rdx
	addq	%r12, %r13
	cmpq	%rax, %rcx
	je	.L3210
.L3208:
	movq	0(%r13), %rax
	movq	%rax, (%rdx)
.L3210:
	leaq	8(%rbx,%rsi), %rbx
.L3207:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L3211
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L3211:
	movq	-64(%rbp), %xmm0
	movq	-88(%rbp), %rax
	movq	%rbx, %xmm2
	movq	-72(%rbp), %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3197:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3196
	.p2align 4,,10
	.p2align 3
.L3200:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3199
	.p2align 4,,10
	.p2align 3
.L3204:
	call	*%rax
	jmp	.L3203
	.p2align 4,,10
	.p2align 3
.L3236:
	testq	%rsi, %rsi
	jne	.L3192
	movq	$0, -72(%rbp)
	movl	$8, %ebx
	movq	$0, -64(%rbp)
	jmp	.L3212
	.p2align 4,,10
	.p2align 3
.L3213:
	movl	$8, %ebx
	jmp	.L3191
.L3215:
	movq	%rbx, %rdx
	jmp	.L3208
.L3192:
	cmpq	%rdx, %rsi
	cmovbe	%rsi, %rdx
	leaq	0(,%rdx,8), %rbx
	jmp	.L3191
.L3235:
	leaq	.LC59(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10651:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger5ScopeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger5ScopeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.rodata._ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Debugger5ScopeESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC60:
	.string	"array expected"
	.section	.text._ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Debugger5ScopeESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"axG",@progbits,_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Debugger5ScopeESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,comdat
	.p2align 4
	.weak	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Debugger5ScopeESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Debugger5ScopeESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Debugger5ScopeESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB7600:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L3240
	cmpl	$7, 8(%rsi)
	movq	%rsi, %r12
	jne	.L3240
	movq	%rdx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	movq	24(%r12), %r14
	pxor	%xmm0, %xmm0
	subq	16(%r12), %r14
	movq	$0, 16(%rax)
	movq	%rax, %r15
	movups	%xmm0, (%rax)
	movq	%r14, %rax
	movq	%r14, -120(%rbp)
	sarq	$3, %rax
	testq	%r14, %r14
	js	.L3345
	testq	%rax, %rax
	jne	.L3244
.L3262:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	je	.L3346
	movq	-152(%rbp), %rax
	movq	8(%r15), %rbx
	movq	(%r15), %r12
	movq	$0, (%rax)
	cmpq	%r12, %rbx
	je	.L3279
	movq	%r15, -120(%rbp)
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %r13
	jmp	.L3280
	.p2align 4,,10
	.p2align 3
.L3348:
	movq	112(%r14), %r15
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger5ScopeE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r15, %r15
	je	.L3283
	movq	(%r15), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L3284
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r15), %rdi
	movq	%rax, (%r15)
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L3285
	call	_ZdlPv@PLT
.L3285:
	movl	$64, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3283:
	movq	104(%r14), %r15
	testq	%r15, %r15
	je	.L3286
	movq	(%r15), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L3287
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r15), %rdi
	movq	%rax, (%r15)
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L3288
	call	_ZdlPv@PLT
.L3288:
	movl	$64, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3286:
	movq	64(%r14), %rdi
	leaq	80(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3289
	call	_ZdlPv@PLT
.L3289:
	movq	48(%r14), %r15
	testq	%r15, %r15
	je	.L3290
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rcx
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3291
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3290:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3292
	call	_ZdlPv@PLT
.L3292:
	movl	$120, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3281:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L3347
.L3280:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L3281
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3348
	addq	$8, %r12
	movq	%r14, %rdi
	call	*%rax
	cmpq	%r12, %rbx
	jne	.L3280
	.p2align 4,,10
	.p2align 3
.L3347:
	movq	-120(%rbp), %r15
	movq	(%r15), %r12
.L3279:
	testq	%r12, %r12
	je	.L3293
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L3293:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3239:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3349
	movq	-152(%rbp), %rax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3244:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	8(%r15), %rcx
	movq	(%r15), %r14
	movq	%rax, -128(%rbp)
	cmpq	%r14, %rcx
	je	.L3247
	movq	%r12, -144(%rbp)
	movq	%rax, %rbx
	movq	%r14, %r12
	movq	%rcx, %r14
	movq	%r15, -136(%rbp)
	movq	%r13, -160(%rbp)
	jmp	.L3260
	.p2align 4,,10
	.p2align 3
.L3351:
	movq	112(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger5ScopeE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L3250
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3251
	movq	8(%r15), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	leaq	24(%r15), %rdx
	movq	%rax, (%r15)
	cmpq	%rdx, %rdi
	je	.L3252
	call	_ZdlPv@PLT
.L3252:
	movl	$64, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3250:
	movq	104(%r13), %r15
	testq	%r15, %r15
	je	.L3253
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3254
	movq	8(%r15), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	leaq	24(%r15), %rdx
	movq	%rax, (%r15)
	cmpq	%rdx, %rdi
	je	.L3255
	call	_ZdlPv@PLT
.L3255:
	movl	$64, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3253:
	movq	64(%r13), %rdi
	leaq	80(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L3256
	call	_ZdlPv@PLT
.L3256:
	movq	48(%r13), %r15
	testq	%r15, %r15
	je	.L3257
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rax
	movq	%r15, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3258
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3257:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L3259
	call	_ZdlPv@PLT
.L3259:
	movl	$120, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3248:
	addq	$8, %r12
	addq	$8, %rbx
	cmpq	%r12, %r14
	je	.L3350
.L3260:
	movq	(%r12), %rdx
	movq	$0, (%r12)
	movq	%rdx, (%rbx)
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L3248
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3351
	addq	$8, %r12
	movq	%r13, %rdi
	addq	$8, %rbx
	call	*%rdx
	cmpq	%r12, %r14
	jne	.L3260
	.p2align 4,,10
	.p2align 3
.L3350:
	movq	-136(%rbp), %r15
	movq	-144(%rbp), %r12
	movq	-160(%rbp), %r13
	movq	(%r15), %r14
.L3247:
	testq	%r14, %r14
	je	.L3261
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3261:
	movq	-128(%rbp), %r14
	movq	16(%r12), %rax
	movq	%r14, %xmm0
	addq	-120(%rbp), %r14
	punpcklqdq	%xmm0, %xmm0
	movq	%r14, 16(%r15)
	movups	%xmm0, (%r15)
	cmpq	%rax, 24(%r12)
	je	.L3262
	leaq	-96(%rbp), %rax
	xorl	%ebx, %ebx
	movq	%rax, -128(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -136(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rax, -120(%rbp)
	jmp	.L3278
	.p2align 4,,10
	.p2align 3
.L3352:
	movq	-104(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%r15)
.L3265:
	movq	-104(%rbp), %r14
	testq	%r14, %r14
	je	.L3266
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3267
	movq	112(%r14), %r9
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger5ScopeE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r9, %r9
	je	.L3268
	movq	(%r9), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3269
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r9), %rdi
	movq	%rax, (%r9)
	leaq	24(%r9), %rax
	cmpq	%rax, %rdi
	je	.L3270
	movq	%r9, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r9
.L3270:
	movl	$64, %esi
	movq	%r9, %rdi
	call	_ZdlPvm@PLT
.L3268:
	movq	104(%r14), %r9
	testq	%r9, %r9
	je	.L3271
	movq	(%r9), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3272
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r9), %rdi
	movq	%rax, (%r9)
	leaq	24(%r9), %rax
	cmpq	%rax, %rdi
	je	.L3273
	movq	%r9, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r9
.L3273:
	movl	$64, %esi
	movq	%r9, %rdi
	call	_ZdlPvm@PLT
.L3271:
	movq	64(%r14), %rdi
	leaq	80(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3274
	call	_ZdlPv@PLT
.L3274:
	movq	48(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3275
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3276
	movq	%rdi, -144(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movq	-144(%rbp), %rdi
	movl	$320, %esi
	call	_ZdlPvm@PLT
.L3275:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3277
	call	_ZdlPv@PLT
.L3277:
	movl	$120, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3266:
	movq	24(%r12), %rax
	subq	16(%r12), %rax
	addq	$1, %rbx
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jnb	.L3262
.L3278:
	movq	-128(%rbp), %r14
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String1611fromIntegerEm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L3263
	call	_ZdlPv@PLT
.L3263:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol9ListValue2atEm@PLT
	movq	-120(%rbp), %rdi
	movq	%r13, %rdx
	movq	%rax, %rsi
	call	_ZN12v8_inspector8protocol8Debugger5Scope9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	8(%r15), %rsi
	cmpq	16(%r15), %rsi
	jne	.L3352
	movq	-120(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger5ScopeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L3265
	.p2align 4,,10
	.p2align 3
.L3267:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L3266
	.p2align 4,,10
	.p2align 3
.L3276:
	call	*%rax
	jmp	.L3275
	.p2align 4,,10
	.p2align 3
.L3272:
	movq	%r9, %rdi
	call	*%rax
	jmp	.L3271
	.p2align 4,,10
	.p2align 3
.L3269:
	movq	%r9, %rdi
	call	*%rax
	jmp	.L3268
	.p2align 4,,10
	.p2align 3
.L3240:
	leaq	.LC60(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	-152(%rbp), %rax
	movq	$0, (%rax)
	jmp	.L3239
	.p2align 4,,10
	.p2align 3
.L3346:
	movq	-152(%rbp), %rax
	movq	%r15, (%rax)
	jmp	.L3239
	.p2align 4,,10
	.p2align 3
.L3284:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L3283
	.p2align 4,,10
	.p2align 3
.L3287:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L3286
	.p2align 4,,10
	.p2align 3
.L3291:
	call	*%rax
	jmp	.L3290
	.p2align 4,,10
	.p2align 3
.L3258:
	call	*%rdx
	jmp	.L3257
	.p2align 4,,10
	.p2align 3
.L3254:
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L3253
	.p2align 4,,10
	.p2align 3
.L3251:
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L3250
.L3349:
	call	__stack_chk_fail@PLT
.L3345:
	leaq	.LC21(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7600:
	.size	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Debugger5ScopeESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Debugger5ScopeESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger9CallFrame9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC61:
	.string	"functionName"
.LC62:
	.string	"functionLocation"
.LC63:
	.string	"scopeChain"
.LC64:
	.string	"this"
.LC65:
	.string	"returnValue"
	.section	.text._ZN12v8_inspector8protocol8Debugger9CallFrame9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger9CallFrame9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger9CallFrame9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger9CallFrame9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB6039:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L3354
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r13
	je	.L3355
.L3354:
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	-120(%rbp), %rax
	movq	$0, (%rax)
.L3353:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3486
	movq	-120(%rbp), %rax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3355:
	.cfi_restore_state
	movl	$168, %edi
	leaq	-96(%rbp), %r15
	call	_Znwm@PLT
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	pxor	%xmm0, %xmm0
	movq	%rax, %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger9CallFrameE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r14)
	leaq	24(%r14), %rax
	movq	%rax, 8(%r14)
	xorl	%eax, %eax
	cmpl	$6, 8(%r13)
	movw	%ax, 24(%r14)
	leaq	64(%r14), %rax
	movq	%rax, 48(%r14)
	leaq	120(%r14), %rax
	movw	%dx, 64(%r14)
	movw	%cx, 120(%r14)
	movq	%rax, 104(%r14)
	movl	$0, %eax
	movq	$0, 16(%r14)
	cmovne	%rax, %r13
	movq	$0, 40(%r14)
	movq	$0, 56(%r14)
	movq	$0, 80(%r14)
	movq	$0, 112(%r14)
	movq	$0, 136(%r14)
	movq	$0, 160(%r14)
	movups	%xmm0, 88(%r14)
	movups	%xmm0, 144(%r14)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC14(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %rbx
	leaq	-80(%rbp), %rax
	movq	%rax, -128(%rbp)
	cmpq	%rax, %rdi
	je	.L3358
	call	_ZdlPv@PLT
.L3358:
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	8(%r14), %rdi
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 40(%r14)
	cmpq	-128(%rbp), %rdi
	je	.L3359
	call	_ZdlPv@PLT
.L3359:
	leaq	.LC61(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %rbx
	cmpq	-128(%rbp), %rdi
	je	.L3360
	call	_ZdlPv@PLT
.L3360:
	leaq	.LC61(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	48(%r14), %rdi
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 80(%r14)
	cmpq	-128(%rbp), %rdi
	je	.L3361
	call	_ZdlPv@PLT
.L3361:
	leaq	.LC62(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %rbx
	cmpq	-128(%rbp), %rdi
	je	.L3362
	call	_ZdlPv@PLT
.L3362:
	leaq	-104(%rbp), %rax
	movq	%rax, -136(%rbp)
	testq	%rbx, %rbx
	je	.L3363
	leaq	.LC62(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-136(%rbp), %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol8Debugger8Location9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rax
	movq	88(%r14), %rdi
	movq	$0, -104(%rbp)
	movq	%rax, 88(%r14)
	testq	%rdi, %rdi
	je	.L3363
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3363
	movq	(%rdi), %rax
	call	*24(%rax)
.L3363:
	leaq	.LC26(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %rbx
	cmpq	-128(%rbp), %rdi
	je	.L3367
	call	_ZdlPv@PLT
.L3367:
	leaq	.LC26(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-136(%rbp), %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol8Debugger8Location9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rax
	movq	96(%r14), %rdi
	movq	$0, -104(%rbp)
	movq	%rax, 96(%r14)
	testq	%rdi, %rdi
	je	.L3369
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3369
	movq	(%rdi), %rax
	call	*24(%rax)
.L3369:
	leaq	.LC33(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %rbx
	cmpq	-128(%rbp), %rdi
	je	.L3371
	call	_ZdlPv@PLT
.L3371:
	leaq	.LC33(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	104(%r14), %rdi
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 136(%r14)
	cmpq	-128(%rbp), %rdi
	je	.L3372
	call	_ZdlPv@PLT
.L3372:
	leaq	.LC63(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %rbx
	cmpq	-128(%rbp), %rdi
	je	.L3373
	call	_ZdlPv@PLT
.L3373:
	leaq	.LC63(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-136(%rbp), %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Debugger5ScopeESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rax
	movq	144(%r14), %rcx
	movq	$0, -104(%rbp)
	movq	%rax, 144(%r14)
	movq	%rcx, %rax
	movq	%rcx, -144(%rbp)
	testq	%rcx, %rcx
	je	.L3375
	movq	8(%rcx), %rcx
	movq	(%rax), %rbx
	cmpq	%rbx, %rcx
	je	.L3376
	movq	%r14, -152(%rbp)
	movq	%rcx, %r14
	movq	%r13, -160(%rbp)
	movq	%r12, -168(%rbp)
	jmp	.L3389
	.p2align 4,,10
	.p2align 3
.L3488:
	movq	112(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger5ScopeE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L3379
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3380
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3381
	call	_ZdlPv@PLT
.L3381:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3379:
	movq	104(%r12), %r13
	testq	%r13, %r13
	je	.L3382
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3383
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3384
	call	_ZdlPv@PLT
.L3384:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3382:
	movq	64(%r12), %rdi
	leaq	80(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3385
	call	_ZdlPv@PLT
.L3385:
	movq	48(%r12), %r13
	testq	%r13, %r13
	je	.L3386
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3387
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3386:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3388
	call	_ZdlPv@PLT
.L3388:
	movl	$120, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3377:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L3487
.L3389:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3377
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3488
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3377
	.p2align 4,,10
	.p2align 3
.L3491:
	movq	-144(%rbp), %rax
	movq	-152(%rbp), %r14
	movq	-160(%rbp), %r13
	movq	-168(%rbp), %r12
	movq	(%rax), %rbx
.L3392:
	testq	%rbx, %rbx
	je	.L3406
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L3406:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3375:
	leaq	.LC64(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %rbx
	cmpq	-128(%rbp), %rdi
	je	.L3407
	call	_ZdlPv@PLT
.L3407:
	leaq	.LC64(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-136(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%r12, %rdx
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObject9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE@PLT
	movq	-104(%rbp), %rax
	movq	152(%r14), %rbx
	movq	$0, -104(%rbp)
	movq	%rax, 152(%r14)
	testq	%rbx, %rbx
	je	.L3409
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	24(%rax), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rax
	movq	%rax, -144(%rbp)
	cmpq	%rax, %rdx
	jne	.L3410
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
	movq	-144(%rbp), %rax
.L3411:
	movq	-104(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L3409
	movq	(%rbx), %rdx
	movq	%rbx, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3413
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L3409:
	leaq	.LC65(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r13
	cmpq	-128(%rbp), %rdi
	je	.L3414
	call	_ZdlPv@PLT
.L3414:
	testq	%r13, %r13
	je	.L3415
	leaq	.LC65(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-136(%rbp), %rdi
	movq	%r13, %rsi
	movq	%r12, %rdx
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObject9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE@PLT
	movq	-104(%rbp), %rax
	movq	160(%r14), %r13
	movq	$0, -104(%rbp)
	movq	%rax, 160(%r14)
	testq	%r13, %r13
	je	.L3415
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rax
	movq	%rax, -128(%rbp)
	cmpq	%rax, %rdx
	jne	.L3418
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	movq	-128(%rbp), %rax
.L3419:
	movq	-104(%rbp), %r13
	testq	%r13, %r13
	je	.L3415
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3421
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3415:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L3489
	movq	-120(%rbp), %rax
	movq	%r14, (%rax)
	jmp	.L3353
	.p2align 4,,10
	.p2align 3
.L3489:
	movq	-120(%rbp), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev(%rip), %rdx
	movq	$0, (%rax)
	movq	(%r14), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3490
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol8Debugger9CallFrameD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3353
	.p2align 4,,10
	.p2align 3
.L3487:
	movq	-144(%rbp), %rax
	movq	-152(%rbp), %r14
	movq	-160(%rbp), %r13
	movq	-168(%rbp), %r12
	movq	(%rax), %rbx
.L3376:
	testq	%rbx, %rbx
	je	.L3390
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L3390:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	movq	-104(%rbp), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L3375
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	cmpq	%rbx, %rcx
	je	.L3392
	movq	%r14, -152(%rbp)
	movq	%rcx, %r14
	movq	%r13, -160(%rbp)
	movq	%r12, -168(%rbp)
	jmp	.L3405
	.p2align 4,,10
	.p2align 3
.L3492:
	movq	112(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger5ScopeE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L3395
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3396
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3397
	call	_ZdlPv@PLT
.L3397:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3395:
	movq	104(%r12), %r13
	testq	%r13, %r13
	je	.L3398
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3399
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3400
	call	_ZdlPv@PLT
.L3400:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3398:
	movq	64(%r12), %rdi
	leaq	80(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3401
	call	_ZdlPv@PLT
.L3401:
	movq	48(%r12), %r13
	testq	%r13, %r13
	je	.L3402
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3403
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3402:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3404
	call	_ZdlPv@PLT
.L3404:
	movl	$120, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3393:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L3491
.L3405:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3393
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3492
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3393
	.p2align 4,,10
	.p2align 3
.L3380:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3379
	.p2align 4,,10
	.p2align 3
.L3383:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3382
	.p2align 4,,10
	.p2align 3
.L3387:
	call	*%rax
	jmp	.L3386
	.p2align 4,,10
	.p2align 3
.L3403:
	call	*%rax
	jmp	.L3402
	.p2align 4,,10
	.p2align 3
.L3399:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3398
	.p2align 4,,10
	.p2align 3
.L3396:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3395
	.p2align 4,,10
	.p2align 3
.L3490:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L3353
	.p2align 4,,10
	.p2align 3
.L3413:
	call	*%rdx
	jmp	.L3409
	.p2align 4,,10
	.p2align 3
.L3410:
	call	*%rdx
	movq	-144(%rbp), %rax
	jmp	.L3411
	.p2align 4,,10
	.p2align 3
.L3421:
	call	*%rdx
	jmp	.L3415
	.p2align 4,,10
	.p2align 3
.L3418:
	call	*%rdx
	movq	-128(%rbp), %rax
	jmp	.L3419
.L3486:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6039:
	.size	_ZN12v8_inspector8protocol8Debugger9CallFrame9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger9CallFrame9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB10756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdx
	movq	8(%rdi), %rax
	movq	%rdi, -224(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rax, -216(%rbp)
	subq	%rdx, %rax
	movq	%rdx, -168(%rbp)
	sarq	$3, %rax
	movabsq	$1152921504606846975, %rdx
	movq	%rsi, -144(%rbp)
	cmpq	%rdx, %rax
	je	.L4304
	movq	%rsi, %r13
	subq	-168(%rbp), %r13
	testq	%rax, %rax
	je	.L3874
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L4305
.L3495:
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	%rax, -192(%rbp)
	addq	%rax, %rbx
	movq	%rbx, -208(%rbp)
	leaq	8(%rax), %rbx
.L3873:
	movq	(%r12), %rax
	movq	-192(%rbp), %rdx
	movq	$0, (%r12)
	movq	%rax, (%rdx,%r13)
	movq	-168(%rbp), %rax
	cmpq	%rax, -112(%rbp)
	je	.L3497
	movq	%rdx, -104(%rbp)
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L3867:
	movq	-80(%rbp), %rsi
	movq	-104(%rbp), %rcx
	movq	(%rsi), %rax
	movq	$0, (%rsi)
	movq	%rax, (%rcx)
	movq	(%rsi), %r15
	testq	%r15, %r15
	je	.L3498
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3499
	movq	160(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger9CallFrameE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L3500
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3501
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3500:
	movq	152(%r15), %r12
	testq	%r12, %r12
	je	.L3502
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3503
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3502:
	movq	144(%r15), %rax
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L3504
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -120(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rcx
	je	.L3505
	movq	%r15, -184(%rbp)
	.p2align 4,,10
	.p2align 3
.L3856:
	movq	-64(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -56(%rbp)
	testq	%rcx, %rcx
	je	.L3506
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3507
	movq	112(%rcx), %r12
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger5ScopeE(%rip), %rax
	movq	%rax, (%rcx)
	testq	%r12, %r12
	je	.L3508
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3509
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3510
	call	_ZdlPv@PLT
.L3510:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3508:
	movq	-56(%rbp), %rax
	movq	104(%rax), %r12
	testq	%r12, %r12
	je	.L3511
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3512
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3513
	call	_ZdlPv@PLT
.L3513:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3511:
	movq	-56(%rbp), %rax
	movq	64(%rax), %rdi
	addq	$80, %rax
	cmpq	%rax, %rdi
	je	.L3514
	call	_ZdlPv@PLT
.L3514:
	movq	-56(%rbp), %rax
	movq	48(%rax), %rbx
	testq	%rbx, %rbx
	je	.L3515
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3516
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rax
	movq	312(%rbx), %r12
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	testq	%r12, %r12
	je	.L3517
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3518
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	56(%r12), %rdi
	movq	%rax, (%r12)
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3519
	call	_ZdlPv@PLT
.L3519:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3520
	call	_ZdlPv@PLT
.L3520:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3517:
	movq	304(%rbx), %rsi
	movq	%rsi, -88(%rbp)
	testq	%rsi, %rsi
	je	.L3521
	movq	(%rsi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3522
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rsi)
	movq	160(%rsi), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L3523
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -136(%rbp)
	movq	%rax, -72(%rbp)
	cmpq	%rax, %rcx
	je	.L3524
	movq	%rbx, -232(%rbp)
	.p2align 4,,10
	.p2align 3
.L3731:
	movq	-72(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -96(%rbp)
	testq	%rcx, %rcx
	je	.L3525
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3526
	movq	16(%rcx), %rbx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	testq	%rbx, %rbx
	je	.L3527
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3528
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L3529
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -152(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L3530
	movq	%rbx, -240(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L3587:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3531
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3532
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L3533
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3534
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -200(%rbp)
	testq	%rax, %rax
	je	.L3535
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	cmpq	%r14, %rcx
	je	.L3536
	movq	%rbx, -256(%rbp)
	movq	%r14, %rbx
	movq	%rcx, %r14
	movq	%r12, -248(%rbp)
	jmp	.L3543
	.p2align 4,,10
	.p2align 3
.L4307:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L3539
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3540
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3539:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L3541
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3542
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3541:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3537:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L4306
.L3543:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L3537
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4307
	addq	$8, %rbx
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%rbx, %r14
	jne	.L3543
	.p2align 4,,10
	.p2align 3
.L4306:
	movq	-200(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %rbx
	movq	(%rax), %r14
.L3536:
	testq	%r14, %r14
	je	.L3544
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3544:
	movq	-200(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3535:
	movq	152(%r13), %rax
	movq	%rax, -200(%rbp)
	testq	%rax, %rax
	je	.L3545
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L3546
	movq	%r12, -248(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L3555
	.p2align 4,,10
	.p2align 3
.L4309:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3549
	call	_ZdlPv@PLT
.L3549:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3550
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3551
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3550:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3552
	call	_ZdlPv@PLT
.L3552:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3553
	call	_ZdlPv@PLT
.L3553:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3554
	call	_ZdlPv@PLT
.L3554:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3547:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L4308
.L3555:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3547
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L4309
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L3555
	.p2align 4,,10
	.p2align 3
.L4308:
	movq	-200(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %r13
	movq	(%rax), %r14
.L3546:
	testq	%r14, %r14
	je	.L3556
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3556:
	movq	-200(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3545:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3557
	call	_ZdlPv@PLT
.L3557:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3558
	call	_ZdlPv@PLT
.L3558:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3559
	call	_ZdlPv@PLT
.L3559:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3533:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L3560
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3561
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -200(%rbp)
	testq	%rax, %rax
	je	.L3562
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	cmpq	%r14, %rdx
	je	.L3563
	movq	%rbx, -256(%rbp)
	movq	%r14, %rbx
	movq	%rdx, %r14
	movq	%r12, -248(%rbp)
	jmp	.L3570
	.p2align 4,,10
	.p2align 3
.L4311:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L3566
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3567
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3566:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L3568
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3569
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3568:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3564:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L4310
.L3570:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L3564
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4311
	addq	$8, %rbx
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%rbx, %r14
	jne	.L3570
	.p2align 4,,10
	.p2align 3
.L4310:
	movq	-200(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %rbx
	movq	(%rax), %r14
.L3563:
	testq	%r14, %r14
	je	.L3571
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3571:
	movq	-200(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3562:
	movq	152(%r13), %rax
	movq	%rax, -200(%rbp)
	testq	%rax, %rax
	je	.L3572
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L3573
	movq	%r12, -248(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L3582
	.p2align 4,,10
	.p2align 3
.L4313:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3576
	call	_ZdlPv@PLT
.L3576:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3577
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3578
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3577:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3579
	call	_ZdlPv@PLT
.L3579:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3580
	call	_ZdlPv@PLT
.L3580:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3581
	call	_ZdlPv@PLT
.L3581:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3574:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L4312
.L3582:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3574
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L4313
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L3582
	.p2align 4,,10
	.p2align 3
.L4312:
	movq	-200(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %r13
	movq	(%rax), %r14
.L3573:
	testq	%r14, %r14
	je	.L3583
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3583:
	movq	-200(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3572:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3584
	call	_ZdlPv@PLT
.L3584:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3585
	call	_ZdlPv@PLT
.L3585:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3586
	call	_ZdlPv@PLT
.L3586:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3560:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3531:
	addq	$8, %rbx
	cmpq	%rbx, -152(%rbp)
	jne	.L3587
	movq	-176(%rbp), %rax
	movq	-240(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L3530:
	testq	%rdi, %rdi
	je	.L3588
	call	_ZdlPv@PLT
.L3588:
	movq	-176(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3529:
	movq	152(%rbx), %rax
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L3589
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -152(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L3590
	movq	%rbx, -240(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L3624:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3591
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3592
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3593
	call	_ZdlPv@PLT
.L3593:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3594
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3595
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L3596
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L3597
	movq	%rbx, -248(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -200(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L3604
	.p2align 4,,10
	.p2align 3
.L4315:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L3600
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3601
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3600:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L3602
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3603
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3602:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3598:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L4314
.L3604:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3598
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4315
	addq	$8, %rbx
	movq	%r12, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L3604
	.p2align 4,,10
	.p2align 3
.L4314:
	movq	-200(%rbp), %r12
	movq	-248(%rbp), %rbx
	movq	-256(%rbp), %r13
	movq	(%r14), %r15
.L3597:
	testq	%r15, %r15
	je	.L3605
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3605:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3596:
	movq	152(%r13), %rax
	movq	%rax, -200(%rbp)
	testq	%rax, %rax
	je	.L3606
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L3607
	movq	%r12, -248(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L3616
	.p2align 4,,10
	.p2align 3
.L4317:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3610
	call	_ZdlPv@PLT
.L3610:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3611
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3612
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3611:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3613
	call	_ZdlPv@PLT
.L3613:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3614
	call	_ZdlPv@PLT
.L3614:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3615
	call	_ZdlPv@PLT
.L3615:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3608:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L4316
.L3616:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3608
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L4317
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L3616
	.p2align 4,,10
	.p2align 3
.L4316:
	movq	-200(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %r13
	movq	(%rax), %r14
.L3607:
	testq	%r14, %r14
	je	.L3617
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3617:
	movq	-200(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3606:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3618
	call	_ZdlPv@PLT
.L3618:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3619
	call	_ZdlPv@PLT
.L3619:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3620
	call	_ZdlPv@PLT
.L3620:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3594:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3621
	call	_ZdlPv@PLT
.L3621:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3622
	call	_ZdlPv@PLT
.L3622:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3623
	call	_ZdlPv@PLT
.L3623:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3591:
	addq	$8, %rbx
	cmpq	%rbx, -152(%rbp)
	jne	.L3624
	movq	-176(%rbp), %rax
	movq	-240(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L3590:
	testq	%rdi, %rdi
	je	.L3625
	call	_ZdlPv@PLT
.L3625:
	movq	-176(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3589:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3626
	call	_ZdlPv@PLT
.L3626:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3627
	call	_ZdlPv@PLT
.L3627:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3628
	call	_ZdlPv@PLT
.L3628:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L3527:
	movq	-96(%rbp), %rax
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	je	.L3629
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3630
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L3631
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -152(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L3632
	movq	%rbx, -240(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L3689:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3633
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3634
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L3635
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3636
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -200(%rbp)
	testq	%rax, %rax
	je	.L3637
	movq	8(%rax), %rsi
	movq	(%rax), %r14
	cmpq	%r14, %rsi
	je	.L3638
	movq	%rbx, -256(%rbp)
	movq	%r14, %rbx
	movq	%rsi, %r14
	movq	%r12, -248(%rbp)
	jmp	.L3645
	.p2align 4,,10
	.p2align 3
.L4319:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L3641
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3642
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3641:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L3643
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3644
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3643:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3639:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L4318
.L3645:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L3639
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4319
	addq	$8, %rbx
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%rbx, %r14
	jne	.L3645
	.p2align 4,,10
	.p2align 3
.L4318:
	movq	-200(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %rbx
	movq	(%rax), %r14
.L3638:
	testq	%r14, %r14
	je	.L3646
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3646:
	movq	-200(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3637:
	movq	152(%r13), %rax
	movq	%rax, -200(%rbp)
	testq	%rax, %rax
	je	.L3647
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L3648
	movq	%r12, -248(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L3657
	.p2align 4,,10
	.p2align 3
.L4321:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3651
	call	_ZdlPv@PLT
.L3651:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3652
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3653
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3652:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3654
	call	_ZdlPv@PLT
.L3654:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3655
	call	_ZdlPv@PLT
.L3655:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3656
	call	_ZdlPv@PLT
.L3656:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3649:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L4320
.L3657:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3649
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L4321
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L3657
	.p2align 4,,10
	.p2align 3
.L4320:
	movq	-200(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %r13
	movq	(%rax), %r14
.L3648:
	testq	%r14, %r14
	je	.L3658
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3658:
	movq	-200(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3647:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3659
	call	_ZdlPv@PLT
.L3659:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3660
	call	_ZdlPv@PLT
.L3660:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3661
	call	_ZdlPv@PLT
.L3661:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3635:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L3662
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3663
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -200(%rbp)
	testq	%rax, %rax
	je	.L3664
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	cmpq	%r14, %rcx
	je	.L3665
	movq	%rbx, -256(%rbp)
	movq	%r14, %rbx
	movq	%rcx, %r14
	movq	%r12, -248(%rbp)
	jmp	.L3672
	.p2align 4,,10
	.p2align 3
.L4323:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L3668
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3669
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3668:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L3670
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3671
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3670:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3666:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L4322
.L3672:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L3666
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4323
	addq	$8, %rbx
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%rbx, %r14
	jne	.L3672
	.p2align 4,,10
	.p2align 3
.L4322:
	movq	-200(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %rbx
	movq	(%rax), %r14
.L3665:
	testq	%r14, %r14
	je	.L3673
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3673:
	movq	-200(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3664:
	movq	152(%r13), %rax
	movq	%rax, -200(%rbp)
	testq	%rax, %rax
	je	.L3674
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L3675
	movq	%r12, -248(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L3684
	.p2align 4,,10
	.p2align 3
.L4325:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3678
	call	_ZdlPv@PLT
.L3678:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3679
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3680
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3679:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3681
	call	_ZdlPv@PLT
.L3681:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3682
	call	_ZdlPv@PLT
.L3682:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3683
	call	_ZdlPv@PLT
.L3683:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3676:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L4324
.L3684:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3676
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L4325
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L3684
	.p2align 4,,10
	.p2align 3
.L4324:
	movq	-200(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %r13
	movq	(%rax), %r14
.L3675:
	testq	%r14, %r14
	je	.L3685
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3685:
	movq	-200(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3674:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3686
	call	_ZdlPv@PLT
.L3686:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3687
	call	_ZdlPv@PLT
.L3687:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3688
	call	_ZdlPv@PLT
.L3688:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3662:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3633:
	addq	$8, %rbx
	cmpq	%rbx, -152(%rbp)
	jne	.L3689
	movq	-176(%rbp), %rax
	movq	-240(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L3632:
	testq	%rdi, %rdi
	je	.L3690
	call	_ZdlPv@PLT
.L3690:
	movq	-176(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3631:
	movq	152(%rbx), %rax
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L3691
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -152(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L3692
	movq	%rbx, -240(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L3726:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3693
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3694
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3695
	call	_ZdlPv@PLT
.L3695:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3696
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3697
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L3698
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L3699
	movq	%rbx, -248(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -200(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L3706
	.p2align 4,,10
	.p2align 3
.L4327:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L3702
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3703
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3702:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L3704
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3705
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3704:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3700:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L4326
.L3706:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3700
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4327
	addq	$8, %rbx
	movq	%r12, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L3706
	.p2align 4,,10
	.p2align 3
.L4326:
	movq	-200(%rbp), %r12
	movq	-248(%rbp), %rbx
	movq	-256(%rbp), %r13
	movq	(%r14), %r15
.L3699:
	testq	%r15, %r15
	je	.L3707
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3707:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3698:
	movq	152(%r13), %rax
	movq	%rax, -200(%rbp)
	testq	%rax, %rax
	je	.L3708
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L3709
	movq	%r12, -248(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L3718
	.p2align 4,,10
	.p2align 3
.L4329:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3712
	call	_ZdlPv@PLT
.L3712:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3713
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3714
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3713:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3715
	call	_ZdlPv@PLT
.L3715:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3716
	call	_ZdlPv@PLT
.L3716:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3717
	call	_ZdlPv@PLT
.L3717:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3710:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L4328
.L3718:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3710
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L4329
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L3718
	.p2align 4,,10
	.p2align 3
.L4328:
	movq	-200(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %r13
	movq	(%rax), %r14
.L3709:
	testq	%r14, %r14
	je	.L3719
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3719:
	movq	-200(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3708:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3720
	call	_ZdlPv@PLT
.L3720:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3721
	call	_ZdlPv@PLT
.L3721:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3722
	call	_ZdlPv@PLT
.L3722:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3696:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3723
	call	_ZdlPv@PLT
.L3723:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3724
	call	_ZdlPv@PLT
.L3724:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3725
	call	_ZdlPv@PLT
.L3725:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3693:
	addq	$8, %rbx
	cmpq	%rbx, -152(%rbp)
	jne	.L3726
	movq	-176(%rbp), %rax
	movq	-240(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L3692:
	testq	%rdi, %rdi
	je	.L3727
	call	_ZdlPv@PLT
.L3727:
	movq	-176(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3691:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3728
	call	_ZdlPv@PLT
.L3728:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3729
	call	_ZdlPv@PLT
.L3729:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3730
	call	_ZdlPv@PLT
.L3730:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L3629:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3525:
	addq	$8, -72(%rbp)
	movq	-72(%rbp), %rax
	cmpq	%rax, -136(%rbp)
	jne	.L3731
	movq	-160(%rbp), %rax
	movq	-232(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -72(%rbp)
.L3524:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L3732
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L3732:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3523:
	movq	-88(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L3733
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -136(%rbp)
	movq	%rax, -72(%rbp)
	cmpq	%rax, %rsi
	je	.L3734
	movq	%rbx, -232(%rbp)
	.p2align 4,,10
	.p2align 3
.L3843:
	movq	-72(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L3735
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3736
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3737
	call	_ZdlPv@PLT
.L3737:
	movq	136(%rbx), %rdx
	movq	%rdx, -96(%rbp)
	testq	%rdx, %rdx
	je	.L3738
	movq	(%rdx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3739
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rdx)
	movq	160(%rdx), %rax
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L3740
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -152(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L3741
	movq	%rbx, -240(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L3798:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3742
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3743
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L3744
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3745
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -200(%rbp)
	testq	%rax, %rax
	je	.L3746
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	cmpq	%r14, %rdx
	je	.L3747
	movq	%rbx, -256(%rbp)
	movq	%r14, %rbx
	movq	%rdx, %r14
	movq	%r12, -248(%rbp)
	jmp	.L3754
	.p2align 4,,10
	.p2align 3
.L4331:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L3750
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3751
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3750:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L3752
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3753
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3752:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3748:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L4330
.L3754:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L3748
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4331
	addq	$8, %rbx
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%rbx, %r14
	jne	.L3754
	.p2align 4,,10
	.p2align 3
.L4330:
	movq	-200(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %rbx
	movq	(%rax), %r14
.L3747:
	testq	%r14, %r14
	je	.L3755
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3755:
	movq	-200(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3746:
	movq	152(%r13), %rax
	movq	%rax, -200(%rbp)
	testq	%rax, %rax
	je	.L3756
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L3757
	movq	%r12, -248(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L3766
	.p2align 4,,10
	.p2align 3
.L4333:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3760
	call	_ZdlPv@PLT
.L3760:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3761
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3762
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3761:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3763
	call	_ZdlPv@PLT
.L3763:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3764
	call	_ZdlPv@PLT
.L3764:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3765
	call	_ZdlPv@PLT
.L3765:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3758:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L4332
.L3766:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3758
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L4333
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L3766
	.p2align 4,,10
	.p2align 3
.L4332:
	movq	-200(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %r13
	movq	(%rax), %r14
.L3757:
	testq	%r14, %r14
	je	.L3767
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3767:
	movq	-200(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3756:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3768
	call	_ZdlPv@PLT
.L3768:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3769
	call	_ZdlPv@PLT
.L3769:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3770
	call	_ZdlPv@PLT
.L3770:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3744:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L3771
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3772
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -200(%rbp)
	testq	%rax, %rax
	je	.L3773
	movq	8(%rax), %rsi
	movq	(%rax), %r14
	cmpq	%r14, %rsi
	je	.L3774
	movq	%rbx, -256(%rbp)
	movq	%r14, %rbx
	movq	%rsi, %r14
	movq	%r12, -248(%rbp)
	jmp	.L3781
	.p2align 4,,10
	.p2align 3
.L4335:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L3777
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3778
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3777:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L3779
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3780
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3779:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3775:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L4334
.L3781:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L3775
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4335
	addq	$8, %rbx
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%rbx, %r14
	jne	.L3781
	.p2align 4,,10
	.p2align 3
.L4334:
	movq	-200(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %rbx
	movq	(%rax), %r14
.L3774:
	testq	%r14, %r14
	je	.L3782
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3782:
	movq	-200(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3773:
	movq	152(%r13), %rax
	movq	%rax, -200(%rbp)
	testq	%rax, %rax
	je	.L3783
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L3784
	movq	%r12, -248(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L3793
	.p2align 4,,10
	.p2align 3
.L4337:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3787
	call	_ZdlPv@PLT
.L3787:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3788
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3789
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3788:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3790
	call	_ZdlPv@PLT
.L3790:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3791
	call	_ZdlPv@PLT
.L3791:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3792
	call	_ZdlPv@PLT
.L3792:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3785:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L4336
.L3793:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3785
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L4337
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L3793
	.p2align 4,,10
	.p2align 3
.L4336:
	movq	-200(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %r13
	movq	(%rax), %r14
.L3784:
	testq	%r14, %r14
	je	.L3794
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3794:
	movq	-200(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3783:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3795
	call	_ZdlPv@PLT
.L3795:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3796
	call	_ZdlPv@PLT
.L3796:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3797
	call	_ZdlPv@PLT
.L3797:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3771:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3742:
	addq	$8, %rbx
	cmpq	%rbx, -152(%rbp)
	jne	.L3798
	movq	-176(%rbp), %rax
	movq	-240(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L3741:
	testq	%rdi, %rdi
	je	.L3799
	call	_ZdlPv@PLT
.L3799:
	movq	-176(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3740:
	movq	-96(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L3800
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -152(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L3801
	movq	%rbx, -240(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L3835:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3802
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3803
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3804
	call	_ZdlPv@PLT
.L3804:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3805
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3806
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L3807
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L3808
	movq	%rbx, -248(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -200(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L3815
	.p2align 4,,10
	.p2align 3
.L4339:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L3811
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3812
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3811:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L3813
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3814
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3813:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3809:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L4338
.L3815:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3809
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4339
	addq	$8, %rbx
	movq	%r12, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L3815
	.p2align 4,,10
	.p2align 3
.L4338:
	movq	-200(%rbp), %r12
	movq	-248(%rbp), %rbx
	movq	-256(%rbp), %r13
	movq	(%r14), %r15
.L3808:
	testq	%r15, %r15
	je	.L3816
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3816:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3807:
	movq	152(%r13), %rax
	movq	%rax, -200(%rbp)
	testq	%rax, %rax
	je	.L3817
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L3818
	movq	%r12, -248(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L3827
	.p2align 4,,10
	.p2align 3
.L4341:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3821
	call	_ZdlPv@PLT
.L3821:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3822
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3823
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3822:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3824
	call	_ZdlPv@PLT
.L3824:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3825
	call	_ZdlPv@PLT
.L3825:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3826
	call	_ZdlPv@PLT
.L3826:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3819:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L4340
.L3827:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3819
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L4341
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L3827
	.p2align 4,,10
	.p2align 3
.L4340:
	movq	-200(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %r13
	movq	(%rax), %r14
.L3818:
	testq	%r14, %r14
	je	.L3828
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3828:
	movq	-200(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3817:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3829
	call	_ZdlPv@PLT
.L3829:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3830
	call	_ZdlPv@PLT
.L3830:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3831
	call	_ZdlPv@PLT
.L3831:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3805:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3832
	call	_ZdlPv@PLT
.L3832:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3833
	call	_ZdlPv@PLT
.L3833:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3834
	call	_ZdlPv@PLT
.L3834:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3802:
	addq	$8, %rbx
	cmpq	%rbx, -152(%rbp)
	jne	.L3835
	movq	-176(%rbp), %rax
	movq	-240(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L3801:
	testq	%rdi, %rdi
	je	.L3836
	call	_ZdlPv@PLT
.L3836:
	movq	-176(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3800:
	movq	-96(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L3837
	call	_ZdlPv@PLT
.L3837:
	movq	-96(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L3838
	call	_ZdlPv@PLT
.L3838:
	movq	-96(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L3839
	call	_ZdlPv@PLT
.L3839:
	movq	-96(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L3738:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3840
	call	_ZdlPv@PLT
.L3840:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3841
	call	_ZdlPv@PLT
.L3841:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3842
	call	_ZdlPv@PLT
.L3842:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L3735:
	addq	$8, -72(%rbp)
	movq	-72(%rbp), %rax
	cmpq	%rax, -136(%rbp)
	jne	.L3843
	movq	-160(%rbp), %rax
	movq	-232(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -72(%rbp)
.L3734:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L3844
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L3844:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3733:
	movq	-88(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L3845
	call	_ZdlPv@PLT
.L3845:
	movq	-88(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L3846
	call	_ZdlPv@PLT
.L3846:
	movq	-88(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L3847
	call	_ZdlPv@PLT
.L3847:
	movq	-88(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L3521:
	movq	264(%rbx), %rdi
	leaq	280(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3848
	call	_ZdlPv@PLT
.L3848:
	movq	216(%rbx), %rdi
	leaq	232(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3849
	call	_ZdlPv@PLT
.L3849:
	movq	168(%rbx), %rdi
	leaq	184(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3850
	call	_ZdlPv@PLT
.L3850:
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3851
	movq	(%rdi), %rax
	call	*24(%rax)
.L3851:
	movq	112(%rbx), %rdi
	leaq	128(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3852
	call	_ZdlPv@PLT
.L3852:
	movq	64(%rbx), %rdi
	leaq	80(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3853
	call	_ZdlPv@PLT
.L3853:
	movq	16(%rbx), %rdi
	leaq	32(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3854
	call	_ZdlPv@PLT
.L3854:
	movl	$320, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L3515:
	movq	-56(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L3855
	call	_ZdlPv@PLT
.L3855:
	movq	-56(%rbp), %rdi
	movl	$120, %esi
	call	_ZdlPvm@PLT
.L3506:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -120(%rbp)
	jne	.L3856
	movq	-128(%rbp), %rax
	movq	-184(%rbp), %r15
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L3505:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L3857
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L3857:
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3504:
	movq	104(%r15), %rdi
	leaq	120(%r15), %rax
	cmpq	%rax, %rdi
	je	.L3858
	call	_ZdlPv@PLT
.L3858:
	movq	96(%r15), %r12
	testq	%r12, %r12
	je	.L3859
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3860
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3861
	call	_ZdlPv@PLT
.L3861:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3859:
	movq	88(%r15), %r12
	testq	%r12, %r12
	je	.L3862
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3863
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3864
	call	_ZdlPv@PLT
.L3864:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3862:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L3865
	call	_ZdlPv@PLT
.L3865:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L3866
	call	_ZdlPv@PLT
.L3866:
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3498:
	addq	$8, -80(%rbp)
	movq	-80(%rbp), %rax
	addq	$8, -104(%rbp)
	cmpq	%rax, -112(%rbp)
	jne	.L3867
	movq	-192(%rbp), %rsi
	movq	-112(%rbp), %rax
	subq	-168(%rbp), %rax
	leaq	8(%rsi,%rax), %rbx
.L3497:
	movq	-112(%rbp), %rdi
	movq	-216(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3868
	subq	%rdi, %rax
	leaq	-8(%rax), %rsi
	movq	%rsi, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rsi, %rsi
	je	.L3876
	movq	%rcx, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L3870:
	movdqu	(%rdi,%rax), %xmm2
	movups	%xmm2, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L3870
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	leaq	(%rbx,%rdi), %rdx
	addq	-112(%rbp), %rdi
	movq	%rdi, -144(%rbp)
	cmpq	%rcx, %rax
	je	.L3871
.L3869:
	movq	-144(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L3871:
	leaq	8(%rbx,%rsi), %rbx
.L3868:
	movq	-168(%rbp), %rax
	testq	%rax, %rax
	je	.L3872
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L3872:
	movq	-192(%rbp), %xmm0
	movq	-224(%rbp), %rax
	movq	%rbx, %xmm3
	movq	-208(%rbp), %rsi
	punpcklqdq	%xmm3, %xmm0
	movq	%rsi, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3507:
	.cfi_restore_state
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3509:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3508
	.p2align 4,,10
	.p2align 3
.L3512:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3511
	.p2align 4,,10
	.p2align 3
.L3516:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L3515
	.p2align 4,,10
	.p2align 3
.L3499:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L3498
	.p2align 4,,10
	.p2align 3
.L3526:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L3525
	.p2align 4,,10
	.p2align 3
.L3736:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L3735
	.p2align 4,,10
	.p2align 3
.L3522:
	movq	%rsi, %rdi
	call	*%rax
	jmp	.L3521
	.p2align 4,,10
	.p2align 3
.L3518:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3517
	.p2align 4,,10
	.p2align 3
.L3694:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3693
	.p2align 4,,10
	.p2align 3
.L3592:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3591
	.p2align 4,,10
	.p2align 3
.L3532:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3531
	.p2align 4,,10
	.p2align 3
.L3743:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3742
	.p2align 4,,10
	.p2align 3
.L3803:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3802
	.p2align 4,,10
	.p2align 3
.L3634:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3633
	.p2align 4,,10
	.p2align 3
.L3863:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3862
	.p2align 4,,10
	.p2align 3
.L3860:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3859
	.p2align 4,,10
	.p2align 3
.L3501:
	call	*%rax
	jmp	.L3500
	.p2align 4,,10
	.p2align 3
.L3503:
	call	*%rax
	jmp	.L3502
	.p2align 4,,10
	.p2align 3
.L3528:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L3527
	.p2align 4,,10
	.p2align 3
.L3630:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L3629
	.p2align 4,,10
	.p2align 3
.L3739:
	movq	%rdx, %rdi
	call	*%rax
	jmp	.L3738
	.p2align 4,,10
	.p2align 3
.L4305:
	testq	%rcx, %rcx
	jne	.L3496
	movq	$0, -208(%rbp)
	movl	$8, %ebx
	movq	$0, -192(%rbp)
	jmp	.L3873
	.p2align 4,,10
	.p2align 3
.L3534:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3533
	.p2align 4,,10
	.p2align 3
.L3697:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3696
	.p2align 4,,10
	.p2align 3
.L3772:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3771
	.p2align 4,,10
	.p2align 3
.L3595:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3594
	.p2align 4,,10
	.p2align 3
.L3636:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3635
	.p2align 4,,10
	.p2align 3
.L3561:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3560
	.p2align 4,,10
	.p2align 3
.L3745:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3744
	.p2align 4,,10
	.p2align 3
.L3663:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3662
	.p2align 4,,10
	.p2align 3
.L3806:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3805
	.p2align 4,,10
	.p2align 3
.L3874:
	movl	$8, %ebx
	jmp	.L3495
	.p2align 4,,10
	.p2align 3
.L3789:
	call	*%rax
	jmp	.L3788
	.p2align 4,,10
	.p2align 3
.L3540:
	call	*%rdx
	jmp	.L3539
	.p2align 4,,10
	.p2align 3
.L3751:
	call	*%rdx
	jmp	.L3750
	.p2align 4,,10
	.p2align 3
.L3714:
	call	*%rax
	jmp	.L3713
	.p2align 4,,10
	.p2align 3
.L3753:
	call	*%rdx
	jmp	.L3752
	.p2align 4,,10
	.p2align 3
.L3669:
	call	*%rdx
	jmp	.L3668
	.p2align 4,,10
	.p2align 3
.L3812:
	call	*%rdx
	jmp	.L3811
	.p2align 4,,10
	.p2align 3
.L3612:
	call	*%rax
	jmp	.L3611
	.p2align 4,,10
	.p2align 3
.L3814:
	call	*%rdx
	jmp	.L3813
	.p2align 4,,10
	.p2align 3
.L3671:
	call	*%rdx
	jmp	.L3670
	.p2align 4,,10
	.p2align 3
.L3653:
	call	*%rax
	jmp	.L3652
	.p2align 4,,10
	.p2align 3
.L3551:
	call	*%rax
	jmp	.L3550
	.p2align 4,,10
	.p2align 3
.L3680:
	call	*%rax
	jmp	.L3679
	.p2align 4,,10
	.p2align 3
.L3642:
	call	*%rdx
	jmp	.L3641
	.p2align 4,,10
	.p2align 3
.L3762:
	call	*%rax
	jmp	.L3761
	.p2align 4,,10
	.p2align 3
.L3778:
	call	*%rdx
	jmp	.L3777
	.p2align 4,,10
	.p2align 3
.L3780:
	call	*%rdx
	jmp	.L3779
	.p2align 4,,10
	.p2align 3
.L3644:
	call	*%rdx
	jmp	.L3643
	.p2align 4,,10
	.p2align 3
.L3578:
	call	*%rax
	jmp	.L3577
	.p2align 4,,10
	.p2align 3
.L3703:
	call	*%rdx
	jmp	.L3702
	.p2align 4,,10
	.p2align 3
.L3542:
	call	*%rdx
	jmp	.L3541
	.p2align 4,,10
	.p2align 3
.L3601:
	call	*%rdx
	jmp	.L3600
	.p2align 4,,10
	.p2align 3
.L3603:
	call	*%rdx
	jmp	.L3602
	.p2align 4,,10
	.p2align 3
.L3823:
	call	*%rax
	jmp	.L3822
	.p2align 4,,10
	.p2align 3
.L3569:
	call	*%rdx
	jmp	.L3568
	.p2align 4,,10
	.p2align 3
.L3567:
	call	*%rdx
	jmp	.L3566
	.p2align 4,,10
	.p2align 3
.L3705:
	call	*%rdx
	jmp	.L3704
.L3876:
	movq	%rbx, %rdx
	jmp	.L3869
.L3496:
	cmpq	%rdx, %rcx
	cmovbe	%rcx, %rdx
	leaq	0(,%rdx,8), %rbx
	jmp	.L3495
.L4304:
	leaq	.LC59(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10756:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.type	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, @function
_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_:
.LFB10779:
	.cfi_startproc
	endbr64
	movabsq	$-3689348814741910323, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	(%rdi), %r14
	movq	%rdi, -80(%rbp)
	movabsq	$230584300921369395, %rdi
	movq	%rsi, -56(%rbp)
	movq	%rbx, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	%rdi, %rax
	je	.L4370
	movq	%rsi, %rcx
	subq	%r14, %rcx
	testq	%rax, %rax
	je	.L4362
	movabsq	$9223372036854775800, %r12
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L4371
.L4344:
	movq	%r12, %rdi
	movq	%rdx, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rdx
	movq	%rax, -64(%rbp)
	addq	%rax, %r12
	movq	%r12, -72(%rbp)
	leaq	40(%rax), %r12
.L4361:
	movq	-64(%rbp), %rax
	movq	(%rdx), %rdi
	addq	%rcx, %rax
	leaq	16(%rax), %rcx
	movq	%rcx, (%rax)
	leaq	16(%rdx), %rcx
	cmpq	%rcx, %rdi
	je	.L4372
	movq	%rdi, (%rax)
	movq	16(%rdx), %rdi
	movq	%rdi, 16(%rax)
.L4347:
	movq	%rcx, (%rdx)
	xorl	%ecx, %ecx
	movq	8(%rdx), %rdi
	movw	%cx, 16(%rdx)
	movq	$0, 8(%rdx)
	movq	32(%rdx), %rdx
	movq	%rdi, 8(%rax)
	movq	%rdx, 32(%rax)
	movq	-56(%rbp), %rax
	cmpq	%r14, %rax
	je	.L4348
	leaq	-40(%rax), %rdx
	movq	-64(%rbp), %r15
	leaq	16(%r14), %r13
	movabsq	$922337203685477581, %rcx
	subq	%r14, %rdx
	shrq	$3, %rdx
	imulq	%rcx, %rdx
	movabsq	$2305843009213693951, %rcx
	andq	%rcx, %rdx
	movq	%rdx, -88(%rbp)
	leaq	(%rdx,%rdx,4), %rdx
	leaq	56(%r14,%rdx,8), %r12
	.p2align 4,,10
	.p2align 3
.L4354:
	leaq	16(%r15), %rcx
	movq	%rcx, (%r15)
	movq	-16(%r13), %rcx
	cmpq	%r13, %rcx
	je	.L4373
.L4349:
	movq	%rcx, (%r15)
	movq	0(%r13), %rcx
	movq	%rcx, 16(%r15)
.L4350:
	movq	-8(%r13), %rcx
	xorl	%eax, %eax
	movq	%rcx, 8(%r15)
	movq	16(%r13), %rcx
	movq	%r13, -16(%r13)
	movq	$0, -8(%r13)
	movw	%ax, 0(%r13)
	movq	%rcx, 32(%r15)
	movq	-16(%r13), %rdi
	cmpq	%r13, %rdi
	je	.L4351
	call	_ZdlPv@PLT
	addq	$40, %r13
	addq	$40, %r15
	cmpq	%r13, %r12
	jne	.L4354
.L4352:
	movq	-88(%rbp), %rax
	movq	-64(%rbp), %rsi
	leaq	10(%rax,%rax,4), %rax
	leaq	(%rsi,%rax,8), %r12
.L4348:
	movq	-56(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L4355
	movq	%r12, %rdx
	.p2align 4,,10
	.p2align 3
.L4359:
	leaq	16(%rdx), %rcx
	leaq	16(%rax), %rdi
	movq	%rcx, (%rdx)
	movq	(%rax), %rcx
	cmpq	%rdi, %rcx
	je	.L4374
	movq	%rcx, (%rdx)
	movq	16(%rax), %rcx
	addq	$40, %rax
	addq	$40, %rdx
	movq	%rcx, -24(%rdx)
	movq	-32(%rax), %rcx
	movq	%rcx, -32(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rbx, %rax
	jne	.L4359
.L4357:
	movq	%rbx, %rsi
	subq	-56(%rbp), %rsi
	leaq	-40(%rsi), %rax
	shrq	$3, %rax
	leaq	40(%r12,%rax,8), %r12
.L4355:
	testq	%r14, %r14
	je	.L4360
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4360:
	movq	-64(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%r12, %xmm3
	movq	-72(%rbp), %rsi
	punpcklqdq	%xmm3, %xmm0
	movq	%rsi, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4351:
	.cfi_restore_state
	addq	$40, %r13
	addq	$40, %r15
	cmpq	%r12, %r13
	je	.L4352
	leaq	16(%r15), %rcx
	movq	%rcx, (%r15)
	movq	-16(%r13), %rcx
	cmpq	%r13, %rcx
	jne	.L4349
.L4373:
	movdqu	0(%r13), %xmm1
	movups	%xmm1, 16(%r15)
	jmp	.L4350
	.p2align 4,,10
	.p2align 3
.L4374:
	movq	8(%rax), %rcx
	movdqu	16(%rax), %xmm2
	addq	$40, %rax
	addq	$40, %rdx
	movq	%rcx, -32(%rdx)
	movq	-8(%rax), %rcx
	movups	%xmm2, -24(%rdx)
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %rbx
	jne	.L4359
	jmp	.L4357
	.p2align 4,,10
	.p2align 3
.L4371:
	testq	%r8, %r8
	jne	.L4345
	movq	$0, -72(%rbp)
	movl	$40, %r12d
	movq	$0, -64(%rbp)
	jmp	.L4361
	.p2align 4,,10
	.p2align 3
.L4362:
	movl	$40, %r12d
	jmp	.L4344
	.p2align 4,,10
	.p2align 3
.L4372:
	movdqu	16(%rdx), %xmm4
	movups	%xmm4, 16(%rax)
	jmp	.L4347
.L4345:
	cmpq	%rdi, %r8
	cmovbe	%r8, %rdi
	imulq	$40, %rdi, %r12
	jmp	.L4344
.L4370:
	leaq	.LC59(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10779:
	.size	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, .-_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.section	.text._ZN12v8_inspector8protocol16ValueConversionsISt6vectorINS_8String16ESaIS3_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"axG",@progbits,_ZN12v8_inspector8protocol16ValueConversionsISt6vectorINS_8String16ESaIS3_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,comdat
	.p2align 4
	.weak	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorINS_8String16ESaIS3_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorINS_8String16ESaIS3_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol16ValueConversionsISt6vectorINS_8String16ESaIS3_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB7798:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L4376
	cmpl	$7, 8(%rsi)
	movq	%rsi, %r13
	jne	.L4376
	movq	%rdx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movabsq	$230584300921369395, %rdx
	movq	$0, 16(%rax)
	movq	%rax, %r12
	movups	%xmm0, (%rax)
	movq	24(%r13), %rax
	subq	16(%r13), %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	ja	.L4424
	testq	%rax, %rax
	jne	.L4380
.L4391:
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	je	.L4425
	movq	-112(%rbp), %rax
	movq	8(%r12), %rbx
	movq	(%r12), %r13
	movq	$0, (%rax)
	cmpq	%r13, %rbx
	je	.L4401
	.p2align 4,,10
	.p2align 3
.L4402:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4403
	call	_ZdlPv@PLT
	addq	$40, %r13
	cmpq	%r13, %rbx
	jne	.L4402
.L4404:
	movq	(%r12), %r13
.L4401:
	testq	%r13, %r13
	je	.L4406
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L4406:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4375:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4426
	movq	-112(%rbp), %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4403:
	.cfi_restore_state
	addq	$40, %r13
	cmpq	%r13, %rbx
	jne	.L4402
	jmp	.L4404
	.p2align 4,,10
	.p2align 3
.L4380:
	leaq	(%rax,%rax,4), %r14
	leaq	0(,%r14,8), %rax
	movq	%rax, %rdi
	movq	%rax, -128(%rbp)
	call	_Znwm@PLT
	movq	8(%r12), %rdx
	movq	(%r12), %rdi
	movq	%rax, -120(%rbp)
	cmpq	%rdi, %rdx
	je	.L4383
	subq	$40, %rdx
	leaq	16(%rdi), %rbx
	movq	%rax, %r14
	subq	%rdi, %rdx
	andq	$-8, %rdx
	leaq	56(%rdi,%rdx), %rcx
	movq	%rcx, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L4389:
	leaq	16(%r14), %rcx
	movq	%rcx, (%r14)
	movq	-16(%rbx), %rcx
	cmpq	%rbx, %rcx
	je	.L4427
.L4384:
	movq	%rcx, (%r14)
	movq	(%rbx), %rcx
	movq	%rcx, 16(%r14)
.L4385:
	movq	-8(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, 8(%r14)
	movq	16(%rbx), %rcx
	movq	%rbx, -16(%rbx)
	movq	$0, -8(%rbx)
	movw	%dx, (%rbx)
	movq	%rcx, 32(%r14)
	movq	-16(%rbx), %rdi
	cmpq	%rbx, %rdi
	je	.L4386
	call	_ZdlPv@PLT
	addq	$40, %r14
	addq	$40, %rbx
	cmpq	%rbx, -104(%rbp)
	jne	.L4389
.L4387:
	movq	(%r12), %rdi
.L4383:
	testq	%rdi, %rdi
	je	.L4390
	call	_ZdlPv@PLT
.L4390:
	movq	-120(%rbp), %r14
	movq	16(%r13), %rax
	movq	%r14, %xmm0
	addq	-128(%rbp), %r14
	punpcklqdq	%xmm0, %xmm0
	movq	%r14, 16(%r12)
	movups	%xmm0, (%r12)
	cmpq	%rax, 24(%r13)
	je	.L4391
	leaq	-80(%rbp), %rax
	xorl	%ebx, %ebx
	leaq	-96(%rbp), %r14
	movq	%rax, -104(%rbp)
	jmp	.L4400
	.p2align 4,,10
	.p2align 3
.L4397:
	movq	%rax, (%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%rsi)
.L4398:
	movq	-88(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-64(%rbp), %rax
	movq	%rax, 32(%rsi)
	addq	$40, 8(%r12)
.L4399:
	movq	24(%r13), %rax
	subq	16(%r13), %rax
	addq	$1, %rbx
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jnb	.L4391
.L4400:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String1611fromIntegerEm@PLT
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-104(%rbp), %rdi
	je	.L4392
	call	_ZdlPv@PLT
.L4392:
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol9ListValue2atEm@PLT
	movq	$0, -88(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	$0, -64(%rbp)
	movq	%rax, -96(%rbp)
	xorl	%eax, %eax
	movw	%ax, -80(%rbp)
	testq	%rdi, %rdi
	je	.L4395
	movq	(%rdi), %rax
	movq	%r14, %rsi
	call	*56(%rax)
	testb	%al, %al
	jne	.L4394
.L4395:
	leaq	.LC9(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L4394:
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L4396
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-96(%rbp), %rax
	cmpq	-104(%rbp), %rax
	jne	.L4397
	movdqa	-80(%rbp), %xmm1
	movups	%xmm1, 16(%rsi)
	jmp	.L4398
	.p2align 4,,10
	.p2align 3
.L4396:
	movq	%r12, %rdi
	movq	%r14, %rdx
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	movq	-96(%rbp), %rdi
	cmpq	-104(%rbp), %rdi
	je	.L4399
	call	_ZdlPv@PLT
	jmp	.L4399
	.p2align 4,,10
	.p2align 3
.L4376:
	leaq	.LC60(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	-112(%rbp), %rax
	movq	$0, (%rax)
	jmp	.L4375
	.p2align 4,,10
	.p2align 3
.L4425:
	movq	-112(%rbp), %rax
	movq	%r12, (%rax)
	jmp	.L4375
	.p2align 4,,10
	.p2align 3
.L4386:
	addq	$40, %r14
	addq	$40, %rbx
	cmpq	-104(%rbp), %rbx
	je	.L4387
	leaq	16(%r14), %rcx
	movq	%rcx, (%r14)
	movq	-16(%rbx), %rcx
	cmpq	%rbx, %rcx
	jne	.L4384
.L4427:
	movdqu	(%rbx), %xmm2
	movups	%xmm2, 16(%r14)
	jmp	.L4385
.L4426:
	call	__stack_chk_fail@PLT
.L4424:
	leaq	.LC21(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7798:
	.size	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorINS_8String16ESaIS3_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol16ValueConversionsISt6vectorINS_8String16ESaIS3_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger18PausedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC66:
	.string	"callFrames"
.LC67:
	.string	"reason"
.LC68:
	.string	"data"
.LC69:
	.string	"hitBreakpoints"
.LC70:
	.string	"asyncStackTrace"
.LC71:
	.string	"asyncStackTraceId"
.LC72:
	.string	"asyncCallStackTraceId"
	.section	.text._ZN12v8_inspector8protocol8Debugger18PausedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger18PausedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger18PausedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger18PausedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB6062:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L4429
	cmpl	$6, 8(%rsi)
	je	.L4430
.L4429:
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	-120(%rbp), %rax
	movq	$0, (%rax)
.L4428:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4568
	movq	-120(%rbp), %rax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4430:
	.cfi_restore_state
	movl	$96, %edi
	leaq	-96(%rbp), %r13
	call	_Znwm@PLT
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, -144(%rbp)
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger18PausedNotificationE(%rip), %rax
	movq	%rax, (%rcx)
	leaq	32(%rcx), %rax
	movw	%dx, 32(%rcx)
	movups	%xmm0, 56(%rcx)
	movups	%xmm0, 72(%rcx)
	movq	$0, 8(%rcx)
	movq	%rax, 16(%rcx)
	movl	$0, %eax
	movq	$0, 24(%rcx)
	movq	$0, 48(%rcx)
	movq	$0, 88(%rcx)
	movq	-128(%rbp), %rcx
	cmpl	$6, 8(%rcx)
	cmove	%rcx, %rax
	movq	%rax, %rbx
	movq	%rax, -128(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC66(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r15
	leaq	-80(%rbp), %rax
	movq	%rax, -136(%rbp)
	cmpq	%rax, %rdi
	je	.L4433
	call	_ZdlPv@PLT
.L4433:
	leaq	.LC66(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	testq	%r15, %r15
	je	.L4434
	cmpl	$7, 8(%r15)
	jne	.L4434
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	%rax, %r14
	movups	%xmm0, (%rax)
	movq	24(%r15), %rbx
	subq	16(%r15), %rbx
	movq	%rbx, %rax
	sarq	$3, %rax
	testq	%rbx, %rbx
	js	.L4569
	testq	%rax, %rax
	jne	.L4438
.L4445:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	je	.L4436
	movq	8(%r14), %rbx
	movq	(%r14), %r15
	cmpq	%r15, %rbx
	je	.L4452
	movq	%r12, -152(%rbp)
	jmp	.L4455
	.p2align 4,,10
	.p2align 3
.L4571:
	call	_ZN12v8_inspector8protocol8Debugger9CallFrameD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4453:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L4570
.L4455:
	movq	(%r15), %r12
	testq	%r12, %r12
	je	.L4453
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev(%rip), %rcx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L4571
	call	*%rax
	jmp	.L4453
	.p2align 4,,10
	.p2align 3
.L4434:
	leaq	.LC60(%rip), %rsi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L4436:
	movq	-144(%rbp), %rax
	movq	8(%rax), %rdi
	movq	%r14, 8(%rax)
	testq	%rdi, %rdi
	je	.L4457
	call	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0
.L4457:
	leaq	.LC67(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-136(%rbp), %rdi
	je	.L4458
	call	_ZdlPv@PLT
.L4458:
	leaq	.LC67(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-144(%rbp), %rbx
	movq	%r13, %rsi
	leaq	16(%rbx), %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 48(%rbx)
	cmpq	-136(%rbp), %rdi
	je	.L4459
	call	_ZdlPv@PLT
.L4459:
	leaq	.LC68(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-136(%rbp), %rdi
	je	.L4460
	call	_ZdlPv@PLT
.L4460:
	testq	%r14, %r14
	je	.L4461
	leaq	.LC68(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-104(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS0_15DictionaryValueEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-144(%rbp), %rdx
	movq	-104(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	56(%rdx), %rdi
	movq	%rax, 56(%rdx)
	testq	%rdi, %rdi
	je	.L4461
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4461
	movq	(%rdi), %rax
	call	*24(%rax)
.L4461:
	leaq	.LC69(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-136(%rbp), %rdi
	je	.L4465
	call	_ZdlPv@PLT
.L4465:
	testq	%r14, %r14
	je	.L4466
	leaq	.LC69(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%r12, %rdx
	movq	%r14, %rsi
	leaq	-104(%rbp), %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorINS_8String16ESaIS3_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-144(%rbp), %rdx
	movq	-104(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	64(%rdx), %r14
	movq	%rax, 64(%rdx)
	testq	%r14, %r14
	je	.L4466
	movq	8(%r14), %rbx
	movq	(%r14), %r15
	cmpq	%r15, %rbx
	je	.L4469
	.p2align 4,,10
	.p2align 3
.L4473:
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L4470
	call	_ZdlPv@PLT
	addq	$40, %r15
	cmpq	%rbx, %r15
	jne	.L4473
.L4471:
	movq	(%r14), %r15
.L4469:
	testq	%r15, %r15
	je	.L4474
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L4474:
	movq	%r14, %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	movq	-104(%rbp), %r14
	testq	%r14, %r14
	je	.L4466
	movq	8(%r14), %rbx
	movq	(%r14), %r15
	cmpq	%r15, %rbx
	je	.L4476
	.p2align 4,,10
	.p2align 3
.L4480:
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L4477
	call	_ZdlPv@PLT
	addq	$40, %r15
	cmpq	%r15, %rbx
	jne	.L4480
.L4478:
	movq	(%r14), %r15
.L4476:
	testq	%r15, %r15
	je	.L4481
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L4481:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4466:
	leaq	.LC70(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-136(%rbp), %rdi
	je	.L4482
	call	_ZdlPv@PLT
.L4482:
	testq	%r14, %r14
	je	.L4483
	leaq	.LC70(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-104(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol7Runtime10StackTrace9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE@PLT
	movq	-144(%rbp), %rcx
	movq	-104(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	72(%rcx), %rdi
	movq	%rax, 72(%rcx)
	testq	%rdi, %rdi
	je	.L4483
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4483
	movq	(%rdi), %rax
	call	*24(%rax)
.L4483:
	leaq	.LC71(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-136(%rbp), %rdi
	je	.L4487
	call	_ZdlPv@PLT
.L4487:
	testq	%r14, %r14
	je	.L4488
	leaq	.LC71(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-104(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol7Runtime12StackTraceId9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE@PLT
	movq	-144(%rbp), %rdx
	movq	-104(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	80(%rdx), %rdi
	movq	%rax, 80(%rdx)
	testq	%rdi, %rdi
	je	.L4488
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4488
	movq	(%rdi), %rax
	call	*24(%rax)
.L4488:
	movq	%r13, %rdi
	leaq	.LC72(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r13
	cmpq	-136(%rbp), %rdi
	je	.L4492
	call	_ZdlPv@PLT
.L4492:
	testq	%r13, %r13
	je	.L4493
	leaq	.LC72(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-104(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol7Runtime12StackTraceId9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE@PLT
	movq	-144(%rbp), %rcx
	movq	-104(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	88(%rcx), %rdi
	movq	%rax, 88(%rcx)
	testq	%rdi, %rdi
	je	.L4493
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4493
	movq	(%rdi), %rax
	call	*24(%rax)
.L4493:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L4572
	movq	-120(%rbp), %rax
	movq	-144(%rbp), %rdx
	movq	%rdx, (%rax)
	jmp	.L4428
	.p2align 4,,10
	.p2align 3
.L4572:
	movq	-120(%rbp), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger18PausedNotificationD0Ev(%rip), %rdx
	movq	$0, (%rax)
	movq	-144(%rbp), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4573
	movq	-144(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol8Debugger18PausedNotificationD1Ev
	movl	$96, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
	jmp	.L4428
	.p2align 4,,10
	.p2align 3
.L4570:
	movq	-152(%rbp), %r12
	movq	(%r14), %r15
.L4452:
	testq	%r15, %r15
	je	.L4456
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L4456:
	movq	%r14, %rdi
	movl	$24, %esi
	xorl	%r14d, %r14d
	call	_ZdlPvm@PLT
	jmp	.L4436
	.p2align 4,,10
	.p2align 3
.L4477:
	addq	$40, %r15
	cmpq	%r15, %rbx
	jne	.L4480
	jmp	.L4478
	.p2align 4,,10
	.p2align 3
.L4470:
	addq	$40, %r15
	cmpq	%r15, %rbx
	jne	.L4473
	jmp	.L4471
	.p2align 4,,10
	.p2align 3
.L4438:
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	8(%r14), %rdx
	movq	(%r14), %r8
	movq	%rax, -152(%rbp)
	cmpq	%r8, %rdx
	je	.L4440
	movq	%r15, -160(%rbp)
	movq	%rdx, %r15
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	movq	%r12, -176(%rbp)
	movq	%r8, %r12
	movq	%r13, -184(%rbp)
	jmp	.L4443
	.p2align 4,,10
	.p2align 3
.L4575:
	call	_ZN12v8_inspector8protocol8Debugger9CallFrameD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4441:
	addq	$8, %r12
	addq	$8, %rbx
	cmpq	%r12, %r15
	je	.L4574
.L4443:
	movq	(%r12), %rdx
	movq	$0, (%r12)
	movq	%rdx, (%rbx)
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L4441
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4575
	call	*%rdx
	jmp	.L4441
	.p2align 4,,10
	.p2align 3
.L4574:
	movq	-160(%rbp), %r15
	movq	-168(%rbp), %rbx
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%r14), %r8
.L4440:
	testq	%r8, %r8
	je	.L4444
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L4444:
	movq	-152(%rbp), %rax
	movq	%rax, %xmm0
	addq	%rax, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, 16(%r14)
	movups	%xmm0, (%r14)
	movq	16(%r15), %rax
	cmpq	%rax, 24(%r15)
	je	.L4445
	leaq	-104(%rbp), %rax
	xorl	%ebx, %ebx
	movq	%rax, -152(%rbp)
	jmp	.L4451
	.p2align 4,,10
	.p2align 3
.L4576:
	movq	-104(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%r14)
.L4448:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4449
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4450
	movq	%rdi, -160(%rbp)
	call	_ZN12v8_inspector8protocol8Debugger9CallFrameD1Ev
	movq	-160(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L4449:
	movq	24(%r15), %rax
	subq	16(%r15), %rax
	addq	$1, %rbx
	sarq	$3, %rax
	cmpq	%rbx, %rax
	jbe	.L4445
.L4451:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String1611fromIntegerEm@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L4446
	call	_ZdlPv@PLT
.L4446:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol9ListValue2atEm@PLT
	movq	-152(%rbp), %rdi
	movq	%r12, %rdx
	movq	%rax, %rsi
	call	_ZN12v8_inspector8protocol8Debugger9CallFrame9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	8(%r14), %rsi
	cmpq	16(%r14), %rsi
	jne	.L4576
	movq	-152(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L4448
	.p2align 4,,10
	.p2align 3
.L4450:
	call	*%rax
	jmp	.L4449
	.p2align 4,,10
	.p2align 3
.L4573:
	movq	-144(%rbp), %rdi
	call	*%rax
	jmp	.L4428
.L4568:
	call	__stack_chk_fail@PLT
.L4569:
	leaq	.LC21(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE6062:
	.size	_ZN12v8_inspector8protocol8Debugger18PausedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger18PausedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImpl19setBlackboxPatternsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC73:
	.string	"patterns"
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl19setBlackboxPatternsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl19setBlackboxPatternsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl19setBlackboxPatternsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl19setBlackboxPatternsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$136, %rsp
	movl	%esi, -148(%rbp)
	movq	(%r8), %r14
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L4578
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	jne	.L4579
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L4631
	call	_ZdlPv@PLT
.L4631:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC73(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	cmpq	%r15, %rdi
	je	.L4581
	call	_ZdlPv@PLT
.L4581:
	leaq	.LC73(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-136(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorINS_8String16ESaIS3_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L4632
	leaq	-128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r13), %rsi
	movq	-136(%rbp), %rdx
	movq	%r12, %rdi
	movq	(%rsi), %rax
	movq	%rdx, -120(%rbp)
	leaq	-120(%rbp), %rdx
	movq	$0, -136(%rbp)
	call	*128(%rax)
	movq	-120(%rbp), %r15
	testq	%r15, %r15
	je	.L4585
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L4586
	.p2align 4,,10
	.p2align 3
.L4590:
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L4587
	call	_ZdlPv@PLT
	addq	$40, %r14
	cmpq	%r14, %rbx
	jne	.L4590
.L4588:
	movq	(%r15), %r14
.L4586:
	testq	%r14, %r14
	je	.L4591
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4591:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L4585:
	cmpl	$2, -112(%rbp)
	je	.L4633
	movq	-128(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L4593
	movl	-148(%rbp), %esi
	movq	%r12, %rdx
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L4593:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4594
.L4636:
	call	_ZdlPv@PLT
.L4594:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4584
	call	_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0
.L4584:
	movq	-136(%rbp), %r13
	testq	%r13, %r13
	je	.L4577
	movq	8(%r13), %rbx
	movq	0(%r13), %r12
	cmpq	%r12, %rbx
	je	.L4597
	.p2align 4,,10
	.p2align 3
.L4601:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4598
	call	_ZdlPv@PLT
	addq	$40, %r12
	cmpq	%rbx, %r12
	jne	.L4601
	movq	0(%r13), %r12
.L4597:
	testq	%r12, %r12
	je	.L4602
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L4602:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4577:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4634
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4598:
	.cfi_restore_state
	addq	$40, %r12
	cmpq	%r12, %rbx
	jne	.L4601
	movq	0(%r13), %r12
	jmp	.L4597
	.p2align 4,,10
	.p2align 3
.L4587:
	addq	$40, %r14
	cmpq	%r14, %rbx
	jne	.L4590
	jmp	.L4588
	.p2align 4,,10
	.p2align 3
.L4578:
	movq	-112(%rbp), %rdi
.L4579:
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L4635
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L4581
	.p2align 4,,10
	.p2align 3
.L4632:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	-148(%rbp), %esi
	movl	$-32602, %edx
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4584
	call	_ZdlPv@PLT
	jmp	.L4584
	.p2align 4,,10
	.p2align 3
.L4635:
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L4581
	.p2align 4,,10
	.p2align 3
.L4633:
	movq	8(%r13), %rdi
	movq	-168(%rbp), %rcx
	movq	-160(%rbp), %rdx
	movl	-148(%rbp), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4636
	jmp	.L4594
.L4634:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6276:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl19setBlackboxPatternsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl19setBlackboxPatternsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger14ScriptPositionESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger14ScriptPositionESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger14ScriptPositionESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger14ScriptPositionESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger14ScriptPositionESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB11037:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -80(%rbp)
	movabsq	$1152921504606846975, %rdi
	movq	%rax, -72(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rdi, %rax
	je	.L4664
	movq	%rsi, %r13
	movq	%rsi, %rbx
	subq	%r12, %rsi
	testq	%rax, %rax
	je	.L4651
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %r9
	cmpq	%r9, %rax
	jbe	.L4665
.L4639:
	movq	%r14, %rdi
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdx
	movq	%rax, -56(%rbp)
	addq	%rax, %r14
	movq	%r14, -64(%rbp)
	leaq	8(%rax), %r14
.L4650:
	movq	(%rdx), %rax
	movq	-56(%rbp), %rcx
	movq	$0, (%rdx)
	movq	%rax, (%rcx,%rsi)
	cmpq	%r12, %r13
	je	.L4641
	movq	%rcx, %r14
	movq	%r12, %r15
	jmp	.L4644
	.p2align 4,,10
	.p2align 3
.L4667:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L4642:
	addq	$8, %r15
	addq	$8, %r14
	cmpq	%r15, %r13
	je	.L4666
.L4644:
	movq	(%r15), %rsi
	movq	$0, (%r15)
	movq	%rsi, (%r14)
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L4642
	movq	(%rdi), %rsi
	leaq	_ZN12v8_inspector8protocol8Debugger14ScriptPositionD0Ev(%rip), %rax
	movq	24(%rsi), %rsi
	cmpq	%rax, %rsi
	je	.L4667
	call	*%rsi
	addq	$8, %r15
	addq	$8, %r14
	cmpq	%r15, %r13
	jne	.L4644
	.p2align 4,,10
	.p2align 3
.L4666:
	movq	-56(%rbp), %rcx
	movq	%r13, %rax
	subq	%r12, %rax
	leaq	8(%rcx,%rax), %r14
.L4641:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r13
	je	.L4645
	movq	%rax, %r15
	subq	%r13, %r15
	leaq	-8(%r15), %rdi
	movq	%rdi, %rsi
	shrq	$3, %rsi
	addq	$1, %rsi
	testq	%rdi, %rdi
	je	.L4653
	movq	%rsi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L4647:
	movdqu	0(%r13,%rax), %xmm1
	movups	%xmm1, (%r14,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L4647
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%r14,%r8), %rdx
	leaq	0(%r13,%r8), %rbx
	cmpq	%rax, %rsi
	je	.L4648
.L4646:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L4648:
	leaq	8(%r14,%rdi), %r14
.L4645:
	testq	%r12, %r12
	je	.L4649
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L4649:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%r14, %xmm2
	movq	-64(%rbp), %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4665:
	.cfi_restore_state
	testq	%r9, %r9
	jne	.L4640
	movq	$0, -64(%rbp)
	movl	$8, %r14d
	movq	$0, -56(%rbp)
	jmp	.L4650
	.p2align 4,,10
	.p2align 3
.L4651:
	movl	$8, %r14d
	jmp	.L4639
.L4653:
	movq	%r14, %rdx
	jmp	.L4646
.L4640:
	cmpq	%rdi, %r9
	cmovbe	%r9, %rdi
	leaq	0(,%rdi,8), %r14
	jmp	.L4639
.L4664:
	leaq	.LC59(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11037:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger14ScriptPositionESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger14ScriptPositionESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImpl19setBlackboxedRangesEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC74:
	.string	"positions"
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl19setBlackboxedRangesEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl19setBlackboxedRangesEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl19setBlackboxedRangesEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl19setBlackboxedRangesEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-112(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$216, %rsp
	movl	%esi, -204(%rbp)
	movq	(%r8), %r12
	leaq	.LC0(%rip), %rsi
	movq	%rdi, -200(%rbp)
	movq	%r13, %rdi
	movq	%rdx, -224(%rbp)
	movq	%rcx, -232(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L4669
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r12
	jne	.L4670
	leaq	-96(%rbp), %rax
	movq	%rax, -184(%rbp)
	cmpq	%rax, %rdi
	je	.L4773
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L4673:
	leaq	.LC22(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-184(%rbp), %rdi
	je	.L4675
	call	_ZdlPv@PLT
.L4675:
	leaq	.LC22(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-160(%rbp), %rax
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC74(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-184(%rbp), %rdi
	je	.L4719
	call	_ZdlPv@PLT
.L4719:
	leaq	.LC74(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	testq	%r14, %r14
	je	.L4674
	cmpl	$7, 8(%r14)
	jne	.L4674
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	%rax, %r12
	movups	%xmm0, (%rax)
	movq	24(%r14), %r15
	subq	16(%r14), %r15
	movq	%r15, %rax
	sarq	$3, %rax
	testq	%r15, %r15
	js	.L4774
	testq	%rax, %rax
	jne	.L4679
.L4686:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	je	.L4677
	movq	8(%r12), %r14
	movq	(%r12), %r15
	cmpq	%r15, %r14
	jne	.L4696
	jmp	.L4693
	.p2align 4,,10
	.p2align 3
.L4776:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L4694:
	addq	$8, %r15
	cmpq	%r15, %r14
	je	.L4775
.L4696:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L4694
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger14ScriptPositionD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L4776
	call	*%rax
	addq	$8, %r15
	cmpq	%r15, %r14
	jne	.L4696
	.p2align 4,,10
	.p2align 3
.L4775:
	movq	(%r12), %r15
.L4693:
	testq	%r15, %r15
	je	.L4697
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L4697:
	movq	%r12, %rdi
	movl	$24, %esi
	xorl	%r12d, %r12d
	call	_ZdlPvm@PLT
.L4677:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L4777
	movq	-200(%rbp), %rbx
	leaq	-176(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	movq	-216(%rbp), %rdx
	movq	%r13, %rdi
	leaq	-168(%rbp), %rcx
	movq	(%rsi), %rax
	movq	136(%rax), %rax
	movq	%r12, -168(%rbp)
	call	*%rax
	movq	-168(%rbp), %r15
	testq	%r15, %r15
	je	.L4702
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L4703
	leaq	_ZN12v8_inspector8protocol8Debugger14ScriptPositionD0Ev(%rip), %r12
	jmp	.L4706
	.p2align 4,,10
	.p2align 3
.L4779:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L4704:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L4778
.L4706:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L4704
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r12, %rax
	je	.L4779
	call	*%rax
	addq	$8, %r14
	cmpq	%r14, %rbx
	jne	.L4706
	.p2align 4,,10
	.p2align 3
.L4778:
	movq	(%r15), %r14
.L4703:
	testq	%r14, %r14
	je	.L4707
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4707:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L4702:
	cmpl	$2, -112(%rbp)
	je	.L4780
	movq	-176(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L4709
	movl	-204(%rbp), %esi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L4709:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4710
	call	_ZdlPv@PLT
.L4710:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4701
	call	_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0
.L4701:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4668
	call	_ZdlPv@PLT
.L4668:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4781
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4773:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L4673
	.p2align 4,,10
	.p2align 3
.L4669:
	movq	-112(%rbp), %rdi
.L4670:
	leaq	-96(%rbp), %rax
	movq	%rax, -184(%rbp)
	cmpq	%rax, %rdi
	je	.L4782
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L4672:
	leaq	.LC22(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-160(%rbp), %rax
	xorl	%esi, %esi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC74(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
.L4674:
	leaq	.LC60(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%r12d, %r12d
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	jmp	.L4677
	.p2align 4,,10
	.p2align 3
.L4777:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %r8
	movq	%r13, %rcx
	movl	$-32602, %edx
	movq	-200(%rbp), %rdi
	movl	-204(%rbp), %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L4699
	call	_ZdlPv@PLT
.L4699:
	testq	%r12, %r12
	je	.L4701
	movq	8(%r12), %rbx
	movq	(%r12), %r13
	cmpq	%r13, %rbx
	je	.L4712
	leaq	_ZN12v8_inspector8protocol8Debugger14ScriptPositionD0Ev(%rip), %r14
	jmp	.L4715
	.p2align 4,,10
	.p2align 3
.L4784:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L4713:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L4783
.L4715:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L4713
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	je	.L4784
	call	*%rax
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L4715
	.p2align 4,,10
	.p2align 3
.L4783:
	movq	(%r12), %r13
.L4712:
	testq	%r13, %r13
	je	.L4716
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L4716:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L4701
	.p2align 4,,10
	.p2align 3
.L4679:
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	8(%r12), %rcx
	movq	(%r12), %r8
	movq	%rax, -192(%rbp)
	cmpq	%r8, %rcx
	je	.L4681
	movq	%r12, -240(%rbp)
	movq	%r8, %r12
	movq	%rbx, -248(%rbp)
	movq	%rcx, %rbx
	movq	%r13, -256(%rbp)
	movq	%rax, %r13
	jmp	.L4684
	.p2align 4,,10
	.p2align 3
.L4786:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L4682:
	addq	$8, %r12
	addq	$8, %r13
	cmpq	%r12, %rbx
	je	.L4785
.L4684:
	movq	(%r12), %rdx
	movq	$0, (%r12)
	movq	%rdx, 0(%r13)
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4682
	movq	(%rdi), %rdx
	leaq	_ZN12v8_inspector8protocol8Debugger14ScriptPositionD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4786
	call	*%rdx
	addq	$8, %r12
	addq	$8, %r13
	cmpq	%r12, %rbx
	jne	.L4684
	.p2align 4,,10
	.p2align 3
.L4785:
	movq	-240(%rbp), %r12
	movq	-248(%rbp), %rbx
	movq	-256(%rbp), %r13
	movq	(%r12), %r8
.L4681:
	testq	%r8, %r8
	je	.L4685
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L4685:
	movq	-192(%rbp), %rax
	movq	%rax, %xmm0
	addq	%rax, %r15
	punpcklqdq	%xmm0, %xmm0
	movq	%r15, 16(%r12)
	movups	%xmm0, (%r12)
	movq	16(%r14), %rax
	cmpq	%rax, 24(%r14)
	je	.L4686
	leaq	-168(%rbp), %rax
	xorl	%r15d, %r15d
	movq	%rax, -192(%rbp)
	jmp	.L4692
	.p2align 4,,10
	.p2align 3
.L4787:
	movq	-168(%rbp), %rax
	movq	$0, -168(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%r12)
.L4689:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4690
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger14ScriptPositionD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4691
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L4690:
	movq	24(%r14), %rax
	subq	16(%r14), %rax
	addq	$1, %r15
	sarq	$3, %rax
	cmpq	%r15, %rax
	jbe	.L4686
.L4692:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String1611fromIntegerEm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L4687
	call	_ZdlPv@PLT
.L4687:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol9ListValue2atEm@PLT
	movq	-192(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%rax, %rsi
	call	_ZN12v8_inspector8protocol8Debugger14ScriptPosition9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	jne	.L4787
	movq	-192(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger14ScriptPositionESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L4689
	.p2align 4,,10
	.p2align 3
.L4691:
	call	*%rax
	jmp	.L4690
	.p2align 4,,10
	.p2align 3
.L4782:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L4672
	.p2align 4,,10
	.p2align 3
.L4780:
	movq	-200(%rbp), %rax
	movq	-232(%rbp), %rcx
	movq	-224(%rbp), %rdx
	movl	-204(%rbp), %esi
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L4709
.L4781:
	call	__stack_chk_fail@PLT
.L4774:
	leaq	.LC21(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE6277:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl19setBlackboxedRangesEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl19setBlackboxedRangesEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.rodata._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.str1.8,"aMS",@progbits,1
	.align 8
.LC75:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag:
.LFB11209:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	testq	%rsi, %rsi
	jne	.L4789
	testq	%rdx, %rdx
	jne	.L4805
.L4789:
	subq	%r13, %rbx
	movq	%rbx, %r14
	sarq	%r14
	cmpq	$15, %rbx
	ja	.L4790
	movq	(%r12), %rdi
.L4791:
	cmpq	$2, %rbx
	je	.L4806
	testq	%rbx, %rbx
	je	.L4794
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	movq	(%r12), %rdi
.L4794:
	xorl	%eax, %eax
	movq	%r14, 8(%r12)
	movw	%ax, (%rdi,%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4806:
	.cfi_restore_state
	movzwl	0(%r13), %eax
	movw	%ax, (%rdi)
	movq	(%r12), %rdi
	jmp	.L4794
	.p2align 4,,10
	.p2align 3
.L4790:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r14
	ja	.L4807
	leaq	2(%rbx), %rdi
	call	_Znwm@PLT
	movq	%r14, 16(%r12)
	movq	%rax, (%r12)
	movq	%rax, %rdi
	jmp	.L4791
.L4805:
	leaq	.LC75(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L4807:
	leaq	.LC27(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11209:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	.section	.text._ZN12v8_inspector8protocol11StringValueC2ERKNS_8String16E,"axG",@progbits,_ZN12v8_inspector8protocol11StringValueC5ERKNS_8String16E,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol11StringValueC2ERKNS_8String16E
	.type	_ZN12v8_inspector8protocol11StringValueC2ERKNS_8String16E, @function
_ZN12v8_inspector8protocol11StringValueC2ERKNS_8String16E:
.LFB4320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	16(%rdi), %rdi
	movq	%rax, -16(%rdi)
	leaq	32(%rbx), %rax
	movl	$4, -8(%rdi)
	movq	%rax, 16(%rbx)
	movq	8(%r12), %rax
	movq	(%rsi), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	32(%r12), %rax
	movq	%rax, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4320:
	.size	_ZN12v8_inspector8protocol11StringValueC2ERKNS_8String16E, .-_ZN12v8_inspector8protocol11StringValueC2ERKNS_8String16E
	.weak	_ZN12v8_inspector8protocol11StringValueC1ERKNS_8String16E
	.set	_ZN12v8_inspector8protocol11StringValueC1ERKNS_8String16E,_ZN12v8_inspector8protocol11StringValueC2ERKNS_8String16E
	.section	.text._ZNK12v8_inspector8protocol8Debugger13BreakLocation7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Debugger13BreakLocation7toValueEv
	.type	_ZNK12v8_inspector8protocol8Debugger13BreakLocation7toValueEv, @function
_ZNK12v8_inspector8protocol8Debugger13BreakLocation7toValueEv:
.LFB6057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$96, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r14
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r14, (%r12)
	movl	$56, %edi
	call	_Znwm@PLT
	leaq	8(%rbx), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol11StringValueC1ERKNS_8String16E
	movq	%r13, -104(%rbp)
	leaq	-96(%rbp), %r13
	leaq	.LC22(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %r14
	cmpq	%r14, %rdi
	je	.L4811
	call	_ZdlPv@PLT
.L4811:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4812
	movq	(%rdi), %rax
	call	*24(%rax)
.L4812:
	movq	(%r12), %r8
	movl	48(%rbx), %edx
	movl	$24, %edi
	movq	%r8, -128(%rbp)
	movl	%edx, -120(%rbp)
	call	_Znwm@PLT
	movl	-120(%rbp), %edx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movq	%r13, %rdi
	movl	$2, 8(%rax)
	leaq	.LC18(%rip), %rsi
	movl	%edx, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L4813
	call	_ZdlPv@PLT
.L4813:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4814
	movq	(%rdi), %rax
	call	*24(%rax)
.L4814:
	cmpb	$0, 52(%rbx)
	jne	.L4835
.L4815:
	cmpb	$0, 64(%rbx)
	jne	.L4836
.L4810:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4837
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4835:
	.cfi_restore_state
	movq	(%r12), %r8
	movl	56(%rbx), %edx
	movl	$24, %edi
	movq	%r8, -128(%rbp)
	movl	%edx, -120(%rbp)
	call	_Znwm@PLT
	movl	-120(%rbp), %edx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movq	%r13, %rdi
	movl	$2, 8(%rax)
	leaq	.LC19(%rip), %rsi
	movl	%edx, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L4816
	call	_ZdlPv@PLT
.L4816:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4815
	movq	(%rdi), %rax
	call	*24(%rax)
	cmpb	$0, 64(%rbx)
	je	.L4810
.L4836:
	movq	(%r12), %r8
	movl	$56, %edi
	movq	%r8, -128(%rbp)
	call	_Znwm@PLT
	leaq	72(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN12v8_inspector8protocol11StringValueC1ERKNS_8String16E
	movq	-120(%rbp), %rax
	leaq	.LC28(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L4819
	call	_ZdlPv@PLT
.L4819:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4810
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4810
.L4837:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6057:
	.size	_ZNK12v8_inspector8protocol8Debugger13BreakLocation7toValueEv, .-_ZNK12v8_inspector8protocol8Debugger13BreakLocation7toValueEv
	.section	.text._ZN12v8_inspector8protocol8Debugger13BreakLocation17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger13BreakLocation17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger13BreakLocation17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Debugger13BreakLocation17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Debugger13BreakLocation17serializeToBinaryEv:
.LFB5743:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger13BreakLocation7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4838
	movq	(%rdi), %rax
	call	*24(%rax)
.L4838:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4845
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4845:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5743:
	.size	_ZN12v8_inspector8protocol8Debugger13BreakLocation17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Debugger13BreakLocation17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Debugger13BreakLocation15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger13BreakLocation15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger13BreakLocation15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Debugger13BreakLocation15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Debugger13BreakLocation15serializeToJSONEv:
.LFB5742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger13BreakLocation7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4846
	movq	(%rdi), %rax
	call	*24(%rax)
.L4846:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4853
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4853:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5742:
	.size	_ZN12v8_inspector8protocol8Debugger13BreakLocation15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Debugger13BreakLocation15serializeToJSONEv
	.section	.text._ZNK12v8_inspector8protocol8Debugger13BreakLocation5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Debugger13BreakLocation5cloneEv
	.type	_ZNK12v8_inspector8protocol8Debugger13BreakLocation5cloneEv, @function
_ZNK12v8_inspector8protocol8Debugger13BreakLocation5cloneEv:
.LFB6058:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol8Debugger13BreakLocation7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol8Debugger13BreakLocation9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4855
	movq	(%rdi), %rax
	call	*24(%rax)
.L4855:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4861
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4861:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6058:
	.size	_ZNK12v8_inspector8protocol8Debugger13BreakLocation5cloneEv, .-_ZNK12v8_inspector8protocol8Debugger13BreakLocation5cloneEv
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImpl22getPossibleBreakpointsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC76:
	.string	"start"
.LC77:
	.string	"end"
.LC78:
	.string	"restrictToFunction"
.LC79:
	.string	"locations"
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl22getPossibleBreakpointsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl22getPossibleBreakpointsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl22getPossibleBreakpointsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl22getPossibleBreakpointsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6186:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$216, %rsp
	movl	%esi, -220(%rbp)
	movq	(%r8), %r13
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -240(%rbp)
	movq	%rcx, -248(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L4863
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r13
	jne	.L4864
	leaq	-96(%rbp), %rax
	movq	%rax, -216(%rbp)
	cmpq	%rax, %rdi
	je	.L4949
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L4867:
	leaq	.LC76(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-216(%rbp), %rdi
	je	.L4869
	call	_ZdlPv@PLT
.L4869:
	leaq	.LC76(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%rbx, %rdx
	movq	%r14, %rsi
	leaq	-200(%rbp), %rdi
	call	_ZN12v8_inspector8protocol8Debugger8Location9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC77(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-216(%rbp), %rdi
	je	.L4905
	call	_ZdlPv@PLT
.L4905:
	testq	%r14, %r14
	je	.L4870
	leaq	.LC77(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%r14, %rsi
	leaq	-168(%rbp), %rdi
	movq	%rbx, %rdx
	call	_ZN12v8_inspector8protocol8Debugger8Location9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-168(%rbp), %r14
.L4870:
	leaq	.LC78(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r13
	cmpq	-216(%rbp), %rdi
	je	.L4903
	call	_ZdlPv@PLT
.L4903:
	testq	%r13, %r13
	je	.L4911
	leaq	.LC78(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %rsi
	movl	$1, %r13d
	call	_ZN12v8_inspector8protocol16ValueConversionsIbE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movb	%al, -232(%rbp)
.L4868:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L4950
	leaq	-184(%rbp), %rdi
	movq	%r15, %rsi
	movq	$0, -192(%rbp)
	leaq	-176(%rbp), %rbx
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r15), %rsi
	movq	-200(%rbp), %rdx
	movq	%r12, %rdi
	movzbl	-232(%rbp), %ecx
	leaq	-192(%rbp), %r9
	leaq	-202(%rbp), %r8
	movq	(%rsi), %rax
	movb	%r13b, -202(%rbp)
	leaq	-168(%rbp), %r13
	movb	%cl, -201(%rbp)
	movq	%r13, %rcx
	movq	$0, -200(%rbp)
	movq	48(%rax), %rax
	movq	%rdx, -176(%rbp)
	movq	%rbx, %rdx
	movq	%r14, -168(%rbp)
	call	*%rax
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4875
	movq	(%rdi), %rax
	call	*24(%rax)
.L4875:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4876
	movq	(%rdi), %rax
	call	*24(%rax)
.L4876:
	cmpl	$2, -112(%rbp)
	je	.L4951
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L4952
.L4879:
	movq	-184(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L4953
	movl	-220(%rbp), %esi
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r14, -168(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4878
	movq	(%rdi), %rax
	call	*24(%rax)
.L4878:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4889
	call	_ZdlPv@PLT
.L4889:
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4890
	call	_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0
.L4890:
	movq	-192(%rbp), %r13
	testq	%r13, %r13
	je	.L4874
	movq	8(%r13), %rbx
	movq	0(%r13), %r14
	cmpq	%r14, %rbx
	je	.L4892
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger13BreakLocationE(%rip), %r15
	jmp	.L4897
	.p2align 4,,10
	.p2align 3
.L4955:
	movq	72(%r12), %rdi
	leaq	88(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L4895
	call	_ZdlPv@PLT
.L4895:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4896
	call	_ZdlPv@PLT
.L4896:
	movl	$112, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4893:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L4954
.L4897:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L4893
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger13BreakLocationD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L4955
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L4897
	.p2align 4,,10
	.p2align 3
.L4954:
	movq	0(%r13), %r14
.L4892:
	testq	%r14, %r14
	je	.L4898
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4898:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4874:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4862
	movq	(%rdi), %rax
	call	*24(%rax)
.L4862:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4956
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4949:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L4867
	.p2align 4,,10
	.p2align 3
.L4863:
	movq	-112(%rbp), %rdi
.L4864:
	leaq	-96(%rbp), %rax
	movq	%rax, -216(%rbp)
	cmpq	%rax, %rdi
	je	.L4957
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L4866:
	leaq	.LC76(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-200(%rbp), %rdi
	movq	%rbx, %rdx
	xorl	%esi, %esi
	call	_ZN12v8_inspector8protocol8Debugger8Location9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movb	$0, -232(%rbp)
	jmp	.L4868
	.p2align 4,,10
	.p2align 3
.L4950:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	-220(%rbp), %esi
	movl	$-32602, %edx
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L4872
	call	_ZdlPv@PLT
.L4872:
	testq	%r14, %r14
	je	.L4874
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L4874
	.p2align 4,,10
	.p2align 3
.L4953:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L4878
	.p2align 4,,10
	.p2align 3
.L4951:
	movq	8(%r15), %rdi
	movq	-248(%rbp), %rcx
	movq	-240(%rbp), %rdx
	movl	-220(%rbp), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L4878
	.p2align 4,,10
	.p2align 3
.L4957:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L4866
	.p2align 4,,10
	.p2align 3
.L4952:
	movl	$40, %edi
	movq	-192(%rbp), %r15
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN12v8_inspector8protocol9ListValueC1Ev@PLT
	movq	-216(%rbp), %rax
	movq	8(%r15), %rsi
	subq	(%r15), %rsi
	leaq	16(%rax), %rdi
	sarq	$3, %rsi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	movq	8(%r15), %rax
	movq	(%r15), %r15
	movq	%rax, -232(%rbp)
	cmpq	%rax, %r15
	je	.L4885
	.p2align 4,,10
	.p2align 3
.L4886:
	movq	(%r15), %rsi
	movq	%rbx, %rdi
	call	_ZNK12v8_inspector8protocol8Debugger13BreakLocation7toValueEv
	movq	-176(%rbp), %rdx
	movq	-216(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rdx, -168(%rbp)
	call	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4883
	movq	(%rdi), %rdx
	addq	$8, %r15
	call	*24(%rdx)
	cmpq	%r15, -232(%rbp)
	jne	.L4886
.L4885:
	movq	-216(%rbp), %rax
	leaq	-160(%rbp), %r15
	leaq	.LC79(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4882
	call	_ZdlPv@PLT
.L4882:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4879
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4879
	.p2align 4,,10
	.p2align 3
.L4883:
	addq	$8, %r15
	cmpq	%r15, -232(%rbp)
	jne	.L4886
	jmp	.L4885
	.p2align 4,,10
	.p2align 3
.L4911:
	movb	$0, -232(%rbp)
	xorl	%r13d, %r13d
	jmp	.L4868
.L4956:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6186:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl22getPossibleBreakpointsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl22getPossibleBreakpointsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZNK12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification7toValueEv
	.type	_ZNK12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification7toValueEv, @function
_ZNK12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification7toValueEv:
.LFB6066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$96, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r15, 0(%r13)
	movl	$56, %edi
	call	_Znwm@PLT
	leaq	8(%rbx), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol11StringValueC1ERKNS_8String16E
	movq	%r12, -104(%rbp)
	leaq	-96(%rbp), %r12
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L4959
	call	_ZdlPv@PLT
.L4959:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4960
	movq	(%rdi), %rax
	call	*24(%rax)
.L4960:
	movq	0(%r13), %r8
	movl	$56, %edi
	movq	%r8, -128(%rbp)
	call	_Znwm@PLT
	leaq	48(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN12v8_inspector8protocol11StringValueC1ERKNS_8String16E
	movq	-120(%rbp), %rax
	leaq	.LC33(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4961
	call	_ZdlPv@PLT
.L4961:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4962
	movq	(%rdi), %rax
	call	*24(%rax)
.L4962:
	movq	0(%r13), %r8
	movl	88(%rbx), %edx
	movl	$24, %edi
	movq	%r8, -128(%rbp)
	movl	%edx, -120(%rbp)
	call	_Znwm@PLT
	movl	-120(%rbp), %edx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movq	%r12, %rdi
	movl	$2, 8(%rax)
	leaq	.LC34(%rip), %rsi
	movl	%edx, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4963
	call	_ZdlPv@PLT
.L4963:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4964
	movq	(%rdi), %rax
	call	*24(%rax)
.L4964:
	movq	0(%r13), %r8
	movl	92(%rbx), %edx
	movl	$24, %edi
	movq	%r8, -128(%rbp)
	movl	%edx, -120(%rbp)
	call	_Znwm@PLT
	movl	-120(%rbp), %edx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movq	%r12, %rdi
	movl	$2, 8(%rax)
	leaq	.LC35(%rip), %rsi
	movl	%edx, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4965
	call	_ZdlPv@PLT
.L4965:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4966
	movq	(%rdi), %rax
	call	*24(%rax)
.L4966:
	movq	0(%r13), %r8
	movl	96(%rbx), %edx
	movl	$24, %edi
	movq	%r8, -128(%rbp)
	movl	%edx, -120(%rbp)
	call	_Znwm@PLT
	movl	-120(%rbp), %edx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movq	%r12, %rdi
	movl	$2, 8(%rax)
	leaq	.LC36(%rip), %rsi
	movl	%edx, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4967
	call	_ZdlPv@PLT
.L4967:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4968
	movq	(%rdi), %rax
	call	*24(%rax)
.L4968:
	movq	0(%r13), %r8
	movl	100(%rbx), %edx
	movl	$24, %edi
	movq	%r8, -128(%rbp)
	movl	%edx, -120(%rbp)
	call	_Znwm@PLT
	movl	-120(%rbp), %edx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movq	%r12, %rdi
	movl	$2, 8(%rax)
	leaq	.LC37(%rip), %rsi
	movl	%edx, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4969
	call	_ZdlPv@PLT
.L4969:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4970
	movq	(%rdi), %rax
	call	*24(%rax)
.L4970:
	movq	0(%r13), %r8
	movl	104(%rbx), %edx
	movl	$24, %edi
	movq	%r8, -128(%rbp)
	movl	%edx, -120(%rbp)
	call	_Znwm@PLT
	movl	-120(%rbp), %edx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movq	%r12, %rdi
	movl	$2, 8(%rax)
	leaq	.LC38(%rip), %rsi
	movl	%edx, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4971
	call	_ZdlPv@PLT
.L4971:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4972
	movq	(%rdi), %rax
	call	*24(%rax)
.L4972:
	movq	0(%r13), %r8
	movl	$56, %edi
	movq	%r8, -128(%rbp)
	call	_Znwm@PLT
	leaq	112(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN12v8_inspector8protocol11StringValueC1ERKNS_8String16E
	movq	-120(%rbp), %rax
	leaq	.LC39(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4973
	call	_ZdlPv@PLT
.L4973:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4974
	movq	(%rdi), %rax
	call	*24(%rax)
.L4974:
	movq	152(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L4975
	movq	0(%r13), %r8
	movq	(%rsi), %rax
	movq	%r14, %rdi
	movq	%r8, -120(%rbp)
	call	*88(%rax)
	leaq	.LC40(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-120(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4976
	call	_ZdlPv@PLT
.L4976:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4975
	movq	(%rdi), %rax
	call	*24(%rax)
.L4975:
	cmpb	$0, 160(%rbx)
	jne	.L5043
.L4978:
	cmpb	$0, 208(%rbx)
	jne	.L5044
.L4981:
	cmpb	$0, 210(%rbx)
	jne	.L5045
.L4984:
	cmpb	$0, 212(%rbx)
	jne	.L5046
.L4987:
	movq	224(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L4958
	movq	%r14, %rdi
	movq	0(%r13), %rbx
	call	_ZNK12v8_inspector8protocol7Runtime10StackTrace7toValueEv@PLT
	movq	-104(%rbp), %rax
	leaq	.LC17(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rdi
	leaq	-112(%rbp), %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4991
	call	_ZdlPv@PLT
.L4991:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4958
	movq	(%rdi), %rax
	call	*24(%rax)
.L4958:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5047
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5046:
	.cfi_restore_state
	movq	0(%r13), %r8
	movl	216(%rbx), %edx
	movl	$24, %edi
	movq	%r8, -128(%rbp)
	movl	%edx, -120(%rbp)
	call	_Znwm@PLT
	movl	-120(%rbp), %edx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movq	%r12, %rdi
	movl	$2, 8(%rax)
	leaq	.LC44(%rip), %rsi
	movl	%edx, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4988
	call	_ZdlPv@PLT
.L4988:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4987
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4987
	.p2align 4,,10
	.p2align 3
.L5045:
	movq	0(%r13), %r8
	movzbl	211(%rbx), %edx
	movl	$24, %edi
	movq	%r8, -128(%rbp)
	movb	%dl, -120(%rbp)
	call	_Znwm@PLT
	movzbl	-120(%rbp), %edx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movq	%r12, %rdi
	movl	$1, 8(%rax)
	leaq	.LC43(%rip), %rsi
	movb	%dl, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4985
	call	_ZdlPv@PLT
.L4985:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4984
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4984
	.p2align 4,,10
	.p2align 3
.L5044:
	movq	0(%r13), %r8
	movzbl	209(%rbx), %edx
	movl	$24, %edi
	movq	%r8, -128(%rbp)
	movb	%dl, -120(%rbp)
	call	_Znwm@PLT
	movzbl	-120(%rbp), %edx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movq	%r12, %rdi
	movl	$1, 8(%rax)
	leaq	.LC42(%rip), %rsi
	movb	%dl, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4982
	call	_ZdlPv@PLT
.L4982:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4981
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4981
	.p2align 4,,10
	.p2align 3
.L5043:
	movq	0(%r13), %r8
	movl	$56, %edi
	movq	%r8, -128(%rbp)
	call	_Znwm@PLT
	leaq	168(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN12v8_inspector8protocol11StringValueC1ERKNS_8String16E
	movq	-120(%rbp), %rax
	leaq	.LC41(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4979
	call	_ZdlPv@PLT
.L4979:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4978
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4978
.L5047:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6066:
	.size	_ZNK12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification7toValueEv, .-_ZNK12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification7toValueEv
	.section	.text._ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification17serializeToBinaryEv:
.LFB5916:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5048
	movq	(%rdi), %rax
	call	*24(%rax)
.L5048:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5055
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5055:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5916:
	.size	_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification15serializeToJSONEv:
.LFB5915:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5056
	movq	(%rdi), %rax
	call	*24(%rax)
.L5056:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5063
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5063:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5915:
	.size	_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification15serializeToJSONEv
	.section	.text._ZNK12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification5cloneEv
	.type	_ZNK12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification5cloneEv, @function
_ZNK12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification5cloneEv:
.LFB6067:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5065
	movq	(%rdi), %rax
	call	*24(%rax)
.L5065:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5071
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5071:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6067:
	.size	_ZNK12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification5cloneEv, .-_ZNK12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification5cloneEv
	.section	.text._ZNK12v8_inspector8protocol8Debugger24ScriptParsedNotification7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Debugger24ScriptParsedNotification7toValueEv
	.type	_ZNK12v8_inspector8protocol8Debugger24ScriptParsedNotification7toValueEv, @function
_ZNK12v8_inspector8protocol8Debugger24ScriptParsedNotification7toValueEv:
.LFB6069:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$96, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r15, 0(%r13)
	movl	$56, %edi
	call	_Znwm@PLT
	leaq	8(%rbx), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol11StringValueC1ERKNS_8String16E
	movq	%r12, -104(%rbp)
	leaq	-96(%rbp), %r12
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L5073
	call	_ZdlPv@PLT
.L5073:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5074
	movq	(%rdi), %rax
	call	*24(%rax)
.L5074:
	movq	0(%r13), %r8
	movl	$56, %edi
	movq	%r8, -128(%rbp)
	call	_Znwm@PLT
	leaq	48(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN12v8_inspector8protocol11StringValueC1ERKNS_8String16E
	movq	-120(%rbp), %rax
	leaq	.LC33(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L5075
	call	_ZdlPv@PLT
.L5075:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5076
	movq	(%rdi), %rax
	call	*24(%rax)
.L5076:
	movq	0(%r13), %r8
	movl	88(%rbx), %edx
	movl	$24, %edi
	movq	%r8, -128(%rbp)
	movl	%edx, -120(%rbp)
	call	_Znwm@PLT
	movl	-120(%rbp), %edx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movq	%r12, %rdi
	movl	$2, 8(%rax)
	leaq	.LC34(%rip), %rsi
	movl	%edx, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L5077
	call	_ZdlPv@PLT
.L5077:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5078
	movq	(%rdi), %rax
	call	*24(%rax)
.L5078:
	movq	0(%r13), %r8
	movl	92(%rbx), %edx
	movl	$24, %edi
	movq	%r8, -128(%rbp)
	movl	%edx, -120(%rbp)
	call	_Znwm@PLT
	movl	-120(%rbp), %edx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movq	%r12, %rdi
	movl	$2, 8(%rax)
	leaq	.LC35(%rip), %rsi
	movl	%edx, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L5079
	call	_ZdlPv@PLT
.L5079:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5080
	movq	(%rdi), %rax
	call	*24(%rax)
.L5080:
	movq	0(%r13), %r8
	movl	96(%rbx), %edx
	movl	$24, %edi
	movq	%r8, -128(%rbp)
	movl	%edx, -120(%rbp)
	call	_Znwm@PLT
	movl	-120(%rbp), %edx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movq	%r12, %rdi
	movl	$2, 8(%rax)
	leaq	.LC36(%rip), %rsi
	movl	%edx, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L5081
	call	_ZdlPv@PLT
.L5081:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5082
	movq	(%rdi), %rax
	call	*24(%rax)
.L5082:
	movq	0(%r13), %r8
	movl	100(%rbx), %edx
	movl	$24, %edi
	movq	%r8, -128(%rbp)
	movl	%edx, -120(%rbp)
	call	_Znwm@PLT
	movl	-120(%rbp), %edx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movq	%r12, %rdi
	movl	$2, 8(%rax)
	leaq	.LC37(%rip), %rsi
	movl	%edx, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L5083
	call	_ZdlPv@PLT
.L5083:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5084
	movq	(%rdi), %rax
	call	*24(%rax)
.L5084:
	movq	0(%r13), %r8
	movl	104(%rbx), %edx
	movl	$24, %edi
	movq	%r8, -128(%rbp)
	movl	%edx, -120(%rbp)
	call	_Znwm@PLT
	movl	-120(%rbp), %edx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movq	%r12, %rdi
	movl	$2, 8(%rax)
	leaq	.LC38(%rip), %rsi
	movl	%edx, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L5085
	call	_ZdlPv@PLT
.L5085:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5086
	movq	(%rdi), %rax
	call	*24(%rax)
.L5086:
	movq	0(%r13), %r8
	movl	$56, %edi
	movq	%r8, -128(%rbp)
	call	_Znwm@PLT
	leaq	112(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN12v8_inspector8protocol11StringValueC1ERKNS_8String16E
	movq	-120(%rbp), %rax
	leaq	.LC39(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L5087
	call	_ZdlPv@PLT
.L5087:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5088
	movq	(%rdi), %rax
	call	*24(%rax)
.L5088:
	movq	152(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L5089
	movq	0(%r13), %r8
	movq	(%rsi), %rax
	movq	%r14, %rdi
	movq	%r8, -120(%rbp)
	call	*88(%rax)
	leaq	.LC40(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-120(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L5090
	call	_ZdlPv@PLT
.L5090:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5089
	movq	(%rdi), %rax
	call	*24(%rax)
.L5089:
	cmpb	$0, 160(%rbx)
	jne	.L5163
.L5092:
	cmpb	$0, 168(%rbx)
	jne	.L5164
.L5095:
	cmpb	$0, 216(%rbx)
	jne	.L5165
.L5098:
	cmpb	$0, 218(%rbx)
	jne	.L5166
.L5101:
	cmpb	$0, 220(%rbx)
	jne	.L5167
.L5104:
	movq	232(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L5072
	movq	%r14, %rdi
	movq	0(%r13), %rbx
	call	_ZNK12v8_inspector8protocol7Runtime10StackTrace7toValueEv@PLT
	movq	-104(%rbp), %rax
	leaq	.LC17(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rdi
	leaq	-112(%rbp), %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L5108
	call	_ZdlPv@PLT
.L5108:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5072
	movq	(%rdi), %rax
	call	*24(%rax)
.L5072:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5168
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5167:
	.cfi_restore_state
	movq	0(%r13), %r8
	movl	224(%rbx), %edx
	movl	$24, %edi
	movq	%r8, -128(%rbp)
	movl	%edx, -120(%rbp)
	call	_Znwm@PLT
	movl	-120(%rbp), %edx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movq	%r12, %rdi
	movl	$2, 8(%rax)
	leaq	.LC44(%rip), %rsi
	movl	%edx, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L5105
	call	_ZdlPv@PLT
.L5105:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5104
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L5104
	.p2align 4,,10
	.p2align 3
.L5166:
	movq	0(%r13), %r8
	movzbl	219(%rbx), %edx
	movl	$24, %edi
	movq	%r8, -128(%rbp)
	movb	%dl, -120(%rbp)
	call	_Znwm@PLT
	movzbl	-120(%rbp), %edx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movq	%r12, %rdi
	movl	$1, 8(%rax)
	leaq	.LC43(%rip), %rsi
	movb	%dl, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L5102
	call	_ZdlPv@PLT
.L5102:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5101
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L5101
	.p2align 4,,10
	.p2align 3
.L5165:
	movq	0(%r13), %r8
	movzbl	217(%rbx), %edx
	movl	$24, %edi
	movq	%r8, -128(%rbp)
	movb	%dl, -120(%rbp)
	call	_Znwm@PLT
	movzbl	-120(%rbp), %edx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movq	%r12, %rdi
	movl	$1, 8(%rax)
	leaq	.LC42(%rip), %rsi
	movb	%dl, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L5099
	call	_ZdlPv@PLT
.L5099:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5098
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L5098
	.p2align 4,,10
	.p2align 3
.L5164:
	movq	0(%r13), %r8
	movl	$56, %edi
	movq	%r8, -128(%rbp)
	call	_Znwm@PLT
	leaq	176(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN12v8_inspector8protocol11StringValueC1ERKNS_8String16E
	movq	-120(%rbp), %rax
	leaq	.LC41(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L5096
	call	_ZdlPv@PLT
.L5096:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5095
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L5095
	.p2align 4,,10
	.p2align 3
.L5163:
	movq	0(%r13), %r8
	movzbl	161(%rbx), %edx
	movl	$24, %edi
	movq	%r8, -128(%rbp)
	movb	%dl, -120(%rbp)
	call	_Znwm@PLT
	movzbl	-120(%rbp), %edx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movq	%r12, %rdi
	movl	$1, 8(%rax)
	leaq	.LC45(%rip), %rsi
	movb	%dl, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L5093
	call	_ZdlPv@PLT
.L5093:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5092
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L5092
.L5168:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6069:
	.size	_ZNK12v8_inspector8protocol8Debugger24ScriptParsedNotification7toValueEv, .-_ZNK12v8_inspector8protocol8Debugger24ScriptParsedNotification7toValueEv
	.section	.text._ZN12v8_inspector8protocol8Debugger24ScriptParsedNotification17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotification17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotification17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotification17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotification17serializeToBinaryEv:
.LFB5989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger24ScriptParsedNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5169
	movq	(%rdi), %rax
	call	*24(%rax)
.L5169:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5176
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5176:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5989:
	.size	_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotification17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotification17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Debugger24ScriptParsedNotification15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotification15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotification15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotification15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotification15serializeToJSONEv:
.LFB5988:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger24ScriptParsedNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5177
	movq	(%rdi), %rax
	call	*24(%rax)
.L5177:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5184
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5184:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5988:
	.size	_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotification15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotification15serializeToJSONEv
	.section	.text._ZNK12v8_inspector8protocol8Debugger24ScriptParsedNotification5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Debugger24ScriptParsedNotification5cloneEv
	.type	_ZNK12v8_inspector8protocol8Debugger24ScriptParsedNotification5cloneEv, @function
_ZNK12v8_inspector8protocol8Debugger24ScriptParsedNotification5cloneEv:
.LFB6070:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol8Debugger24ScriptParsedNotification7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5186
	movq	(%rdi), %rax
	call	*24(%rax)
.L5186:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5192
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5192:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6070:
	.size	_ZNK12v8_inspector8protocol8Debugger24ScriptParsedNotification5cloneEv, .-_ZNK12v8_inspector8protocol8Debugger24ScriptParsedNotification5cloneEv
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC80:
	.string	"maxScriptsCacheSize"
.LC81:
	.string	"debuggerId"
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	leaq	.LC0(%rip), %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$248, %rsp
	movq	%rdx, -272(%rbp)
	movq	(%r8), %r15
	movq	%rcx, -280(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	testq	%rax, %rax
	je	.L5195
	cmpl	$6, 8(%rax)
	jne	.L5195
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L5236
	movq	%rax, -248(%rbp)
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movq	-248(%rbp), %rax
.L5198:
	leaq	.LC80(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-248(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%r15, %rdi
	je	.L5200
	movq	%rax, -248(%rbp)
	call	_ZdlPv@PLT
	movq	-248(%rbp), %r8
.L5200:
	testq	%r8, %r8
	movq	%r8, -248(%rbp)
	je	.L5220
	leaq	.LC80(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-248(%rbp), %r8
	movq	$0x000000000, -224(%rbp)
	leaq	-224(%rbp), %rsi
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L5201
	leaq	.LC24(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L5201:
	movsd	-224(%rbp), %xmm1
	movb	$1, -249(%rbp)
	movsd	%xmm1, -248(%rbp)
.L5199:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L5237
	leaq	-192(%rbp), %rax
	xorl	%edx, %edx
	leaq	-232(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -264(%rbp)
	leaq	-208(%rbp), %rbx
	leaq	-224(%rbp), %r15
	movq	%rax, -208(%rbp)
	movw	%dx, -192(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -176(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r13), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	movzbl	-249(%rbp), %ecx
	movsd	-248(%rbp), %xmm0
	movq	(%rsi), %rax
	movq	32(%rax), %rax
	movb	%cl, -224(%rbp)
	movq	%rbx, %rcx
	movsd	%xmm0, -216(%rbp)
	call	*%rax
	cmpl	$2, -112(%rbp)
	je	.L5238
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L5239
.L5207:
	movq	-232(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L5240
	movq	%r15, %rcx
	movq	%r12, %rdx
	movl	%r14d, %esi
	movq	%r13, -224(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE@PLT
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5206
	movq	(%rdi), %rax
	call	*24(%rax)
.L5206:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5211
	call	_ZdlPv@PLT
.L5211:
	movq	-232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5212
	call	_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0
.L5212:
	movq	-208(%rbp), %rdi
	cmpq	-264(%rbp), %rdi
	je	.L5193
.L5235:
	call	_ZdlPv@PLT
.L5193:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5241
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5236:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movq	-248(%rbp), %rax
	jmp	.L5198
	.p2align 4,,10
	.p2align 3
.L5195:
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L5242
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L5197:
	pxor	%xmm2, %xmm2
	movb	$0, -249(%rbp)
	movsd	%xmm2, -248(%rbp)
	jmp	.L5199
	.p2align 4,,10
	.p2align 3
.L5237:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	$-32602, %edx
	movl	%r14d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r15, %rdi
	jne	.L5235
	jmp	.L5193
	.p2align 4,,10
	.p2align 3
.L5240:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L5206
	.p2align 4,,10
	.p2align 3
.L5238:
	movq	8(%r13), %rdi
	movq	-280(%rbp), %rcx
	movl	%r14d, %esi
	movq	-272(%rbp), %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L5206
	.p2align 4,,10
	.p2align 3
.L5242:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L5197
	.p2align 4,,10
	.p2align 3
.L5239:
	movl	$56, %edi
	call	_Znwm@PLT
	movq	%rbx, %rsi
	leaq	-160(%rbp), %rbx
	movq	%rax, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN12v8_inspector8protocol11StringValueC1ERKNS_8String16E
	movq	-248(%rbp), %rax
	leaq	.LC81(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5208
	call	_ZdlPv@PLT
.L5208:
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5207
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L5207
	.p2align 4,,10
	.p2align 3
.L5220:
	pxor	%xmm3, %xmm3
	movb	$0, -249(%rbp)
	movsd	%xmm3, -248(%rbp)
	jmp	.L5199
.L5241:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6152:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImpl15getScriptSourceEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC82:
	.string	"scriptSource"
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl15getScriptSourceEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl15getScriptSourceEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl15getScriptSourceEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl15getScriptSourceEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6215:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$264, %rsp
	movl	%esi, -284(%rbp)
	movq	(%r8), %r14
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -296(%rbp)
	movq	%rcx, -304(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L5244
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	jne	.L5245
	leaq	-96(%rbp), %rax
	movq	%rax, -280(%rbp)
	cmpq	%rax, %rdi
	je	.L5283
	call	_ZdlPv@PLT
.L5283:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-280(%rbp), %rdi
	je	.L5247
	call	_ZdlPv@PLT
.L5247:
	leaq	.LC22(%rip), %rsi
	movq	%rbx, %rdi
	leaq	-256(%rbp), %r15
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L5285
	xorl	%edx, %edx
	leaq	-272(%rbp), %rdi
	movq	%r13, %rsi
	movq	$0, -200(%rbp)
	leaq	-192(%rbp), %rbx
	movw	%dx, -192(%rbp)
	leaq	-208(%rbp), %r14
	movq	%rbx, -208(%rbp)
	movq	$0, -176(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	184(%r13), %rsi
	movq	(%rsi), %rax
	call	*56(%rax)
	cmpl	$2, -112(%rbp)
	je	.L5286
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L5287
.L5253:
	movq	-272(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L5288
	movl	-284(%rbp), %esi
	leaq	-264(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r13, -264(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE@PLT
	movq	-264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5252
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L5252
	.p2align 4,,10
	.p2align 3
.L5286:
	movq	8(%r13), %rdi
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rdx
	movl	-284(%rbp), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
.L5252:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5257
	call	_ZdlPv@PLT
.L5257:
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5258
	call	_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0
.L5258:
	movq	-208(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L5250
.L5284:
	call	_ZdlPv@PLT
.L5250:
	movq	-256(%rbp), %rdi
	leaq	-240(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5243
	call	_ZdlPv@PLT
.L5243:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5289
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5244:
	.cfi_restore_state
	movq	-112(%rbp), %rdi
.L5245:
	leaq	-96(%rbp), %rax
	movq	%rax, -280(%rbp)
	cmpq	%rax, %rdi
	je	.L5290
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L5247
	.p2align 4,,10
	.p2align 3
.L5285:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	-284(%rbp), %esi
	movl	$-32602, %edx
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	-280(%rbp), %rdi
	jne	.L5284
	jmp	.L5250
	.p2align 4,,10
	.p2align 3
.L5288:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L5252
	.p2align 4,,10
	.p2align 3
.L5290:
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L5247
	.p2align 4,,10
	.p2align 3
.L5287:
	movl	$56, %edi
	call	_Znwm@PLT
	movq	%r14, %rsi
	leaq	-160(%rbp), %r14
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN12v8_inspector8protocol11StringValueC1ERKNS_8String16E
	leaq	.LC82(%rip), %rsi
	movq	%r14, %rdi
	movq	%r15, -264(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	leaq	-264(%rbp), %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5254
	call	_ZdlPv@PLT
.L5254:
	movq	-264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5253
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L5253
.L5289:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6215:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl15getScriptSourceEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl15getScriptSourceEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImpl28setInstrumentationBreakpointEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC83:
	.string	"instrumentation"
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl28setInstrumentationBreakpointEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl28setInstrumentationBreakpointEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl28setInstrumentationBreakpointEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl28setInstrumentationBreakpointEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$264, %rsp
	movl	%esi, -284(%rbp)
	movq	(%r8), %r14
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -296(%rbp)
	movq	%rcx, -304(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L5292
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	jne	.L5293
	leaq	-96(%rbp), %rax
	movq	%rax, -280(%rbp)
	cmpq	%rax, %rdi
	je	.L5331
	call	_ZdlPv@PLT
.L5331:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC83(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-280(%rbp), %rdi
	je	.L5295
	call	_ZdlPv@PLT
.L5295:
	leaq	.LC83(%rip), %rsi
	movq	%rbx, %rdi
	leaq	-256(%rbp), %r15
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L5333
	xorl	%edx, %edx
	leaq	-272(%rbp), %rdi
	movq	%r13, %rsi
	movq	$0, -200(%rbp)
	leaq	-192(%rbp), %rbx
	movw	%dx, -192(%rbp)
	leaq	-208(%rbp), %r14
	movq	%rbx, -208(%rbp)
	movq	$0, -176(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	184(%r13), %rsi
	movq	(%rsi), %rax
	call	*152(%rax)
	cmpl	$2, -112(%rbp)
	je	.L5334
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L5335
.L5301:
	movq	-272(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L5336
	movl	-284(%rbp), %esi
	leaq	-264(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r13, -264(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE@PLT
	movq	-264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5300
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L5300
	.p2align 4,,10
	.p2align 3
.L5334:
	movq	8(%r13), %rdi
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rdx
	movl	-284(%rbp), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
.L5300:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5305
	call	_ZdlPv@PLT
.L5305:
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5306
	call	_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0
.L5306:
	movq	-208(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L5298
.L5332:
	call	_ZdlPv@PLT
.L5298:
	movq	-256(%rbp), %rdi
	leaq	-240(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5291
	call	_ZdlPv@PLT
.L5291:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5337
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5292:
	.cfi_restore_state
	movq	-112(%rbp), %rdi
.L5293:
	leaq	-96(%rbp), %rax
	movq	%rax, -280(%rbp)
	cmpq	%rax, %rdi
	je	.L5338
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L5295
	.p2align 4,,10
	.p2align 3
.L5333:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	-284(%rbp), %esi
	movl	$-32602, %edx
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	-280(%rbp), %rdi
	jne	.L5332
	jmp	.L5298
	.p2align 4,,10
	.p2align 3
.L5336:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L5300
	.p2align 4,,10
	.p2align 3
.L5338:
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L5295
	.p2align 4,,10
	.p2align 3
.L5335:
	movl	$56, %edi
	call	_Znwm@PLT
	movq	%r14, %rsi
	leaq	-160(%rbp), %r14
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN12v8_inspector8protocol11StringValueC1ERKNS_8String16E
	leaq	.LC10(%rip), %rsi
	movq	%r14, %rdi
	movq	%r15, -264(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	leaq	-264(%rbp), %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5302
	call	_ZdlPv@PLT
.L5302:
	movq	-264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5301
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L5301
.L5337:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6282:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl28setInstrumentationBreakpointEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl28setInstrumentationBreakpointEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImpl27setBreakpointOnFunctionCallEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC84:
	.string	"objectId"
.LC85:
	.string	"condition"
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl27setBreakpointOnFunctionCallEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl27setBreakpointOnFunctionCallEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl27setBreakpointOnFunctionCallEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl27setBreakpointOnFunctionCallEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$328, %rsp
	movl	%esi, -332(%rbp)
	movq	(%r8), %r13
	leaq	.LC0(%rip), %rsi
	movq	%rdi, -328(%rbp)
	movq	%r12, %rdi
	movq	%rdx, -360(%rbp)
	movq	%rcx, -368(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L5340
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r13
	jne	.L5341
	leaq	-96(%rbp), %r14
	cmpq	%r14, %rdi
	je	.L5392
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L5344:
	leaq	.LC84(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r15
	cmpq	%r14, %rdi
	je	.L5346
	call	_ZdlPv@PLT
.L5346:
	leaq	.LC84(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-304(%rbp), %rax
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC85(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r15
	cmpq	%r14, %rdi
	je	.L5368
	call	_ZdlPv@PLT
.L5368:
	leaq	-184(%rbp), %r13
	xorl	%esi, %esi
	movb	$0, -208(%rbp)
	movq	%r13, -200(%rbp)
	movq	$0, -192(%rbp)
	movw	%si, -184(%rbp)
	movq	$0, -168(%rbp)
	testq	%r15, %r15
	je	.L5345
	leaq	.LC85(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	-200(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-80(%rbp), %rax
	movq	-112(%rbp), %rdi
	movb	$1, -208(%rbp)
	movq	%rax, -168(%rbp)
	cmpq	%r14, %rdi
	je	.L5345
	call	_ZdlPv@PLT
.L5345:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L5393
	movq	-328(%rbp), %r15
	xorl	%ecx, %ecx
	leaq	-240(%rbp), %rbx
	leaq	-320(%rbp), %rdi
	movq	%rbx, -256(%rbp)
	movq	%r15, %rsi
	movq	$0, -248(%rbp)
	movw	%cx, -240(%rbp)
	movq	$0, -224(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r15), %rsi
	movq	-200(%rbp), %rdx
	movq	(%rsi), %rax
	movq	168(%rax), %r9
	movzbl	-208(%rbp), %eax
	movb	%al, -160(%rbp)
	leaq	-136(%rbp), %rax
	movq	%rax, -152(%rbp)
	cmpq	%r13, %rdx
	je	.L5394
	movq	%rdx, -152(%rbp)
	movq	-184(%rbp), %rdx
	movq	%rdx, -136(%rbp)
.L5352:
	movq	-192(%rbp), %rdx
	leaq	-256(%rbp), %r15
	movq	%r12, %rdi
	leaq	-160(%rbp), %r14
	movq	%rax, -352(%rbp)
	movq	%r15, %r8
	movq	%r14, %rcx
	movq	%rdx, -144(%rbp)
	xorl	%edx, %edx
	movw	%dx, -184(%rbp)
	movq	-168(%rbp), %rdx
	movq	%r13, -200(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-344(%rbp), %rdx
	movq	$0, -192(%rbp)
	call	*%r9
	movq	-152(%rbp), %rdi
	movq	-352(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5353
	call	_ZdlPv@PLT
.L5353:
	cmpl	$2, -112(%rbp)
	je	.L5395
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movl	-112(%rbp), %eax
	movq	-328(%rbp), %r8
	testl	%eax, %eax
	je	.L5396
.L5356:
	movq	-320(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L5397
	movl	-332(%rbp), %esi
	leaq	-312(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r8, -312(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE@PLT
	movq	-312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5355
	movq	(%rdi), %rax
	call	*24(%rax)
.L5355:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5360
	call	_ZdlPv@PLT
.L5360:
	movq	-320(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5361
	call	_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0
.L5361:
	movq	-256(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L5350
.L5391:
	call	_ZdlPv@PLT
.L5350:
	movq	-200(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L5363
	call	_ZdlPv@PLT
.L5363:
	movq	-304(%rbp), %rdi
	leaq	-288(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5339
	call	_ZdlPv@PLT
.L5339:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5398
	addq	$328, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5392:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L5344
	.p2align 4,,10
	.p2align 3
.L5340:
	movq	-112(%rbp), %rdi
.L5341:
	leaq	-96(%rbp), %r14
	cmpq	%r14, %rdi
	je	.L5399
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L5343:
	leaq	.LC84(%rip), %rsi
	movq	%rbx, %rdi
	leaq	-184(%rbp), %r13
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-304(%rbp), %rax
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	xorl	%edi, %edi
	movb	$0, -208(%rbp)
	movq	%r13, -200(%rbp)
	movq	$0, -192(%rbp)
	movw	%di, -184(%rbp)
	movq	$0, -168(%rbp)
	jmp	.L5345
	.p2align 4,,10
	.p2align 3
.L5393:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	$-32602, %edx
	movq	-328(%rbp), %rdi
	movl	-332(%rbp), %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L5391
	jmp	.L5350
	.p2align 4,,10
	.p2align 3
.L5397:
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*24(%rax)
	jmp	.L5355
	.p2align 4,,10
	.p2align 3
.L5395:
	movq	-328(%rbp), %rax
	movq	-368(%rbp), %rcx
	movq	-360(%rbp), %rdx
	movl	-332(%rbp), %esi
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L5355
	.p2align 4,,10
	.p2align 3
.L5399:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L5343
	.p2align 4,,10
	.p2align 3
.L5394:
	movdqu	-184(%rbp), %xmm0
	movups	%xmm0, -136(%rbp)
	jmp	.L5352
	.p2align 4,,10
	.p2align 3
.L5396:
	movl	$56, %edi
	movq	%r8, -344(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN12v8_inspector8protocol11StringValueC1ERKNS_8String16E
	movq	-328(%rbp), %rax
	leaq	.LC10(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -312(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-344(%rbp), %r8
	leaq	-312(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r8, %rdi
	movq	%r8, -328(%rbp)
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	movq	-328(%rbp), %r8
	cmpq	%rax, %rdi
	je	.L5357
	call	_ZdlPv@PLT
	movq	-328(%rbp), %r8
.L5357:
	movq	-312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5356
	movq	(%rdi), %rax
	movq	%r8, -328(%rbp)
	call	*24(%rax)
	movq	-328(%rbp), %r8
	jmp	.L5356
.L5398:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6311:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl27setBreakpointOnFunctionCallEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl27setBreakpointOnFunctionCallEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger8Frontend19scriptFailedToParseERKNS_8String16ES5_iiiiiS5_N30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS0_15DictionaryValueEEENS8_10ValueMaybeIS3_EENSC_IbEESE_NSC_IiEENS9_INS0_7Runtime10StackTraceEEE.str1.1,"aMS",@progbits,1
.LC86:
	.string	"Debugger.scriptFailedToParse"
	.section	.text._ZN12v8_inspector8protocol8Debugger8Frontend19scriptFailedToParseERKNS_8String16ES5_iiiiiS5_N30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS0_15DictionaryValueEEENS8_10ValueMaybeIS3_EENSC_IbEESE_NSC_IiEENS9_INS0_7Runtime10StackTraceEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger8Frontend19scriptFailedToParseERKNS_8String16ES5_iiiiiS5_N30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS0_15DictionaryValueEEENS8_10ValueMaybeIS3_EENSC_IbEESE_NSC_IiEENS9_INS0_7Runtime10StackTraceEEE
	.type	_ZN12v8_inspector8protocol8Debugger8Frontend19scriptFailedToParseERKNS_8String16ES5_iiiiiS5_N30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS0_15DictionaryValueEEENS8_10ValueMaybeIS3_EENSC_IbEESE_NSC_IiEENS9_INS0_7Runtime10StackTraceEEE, @function
_ZN12v8_inspector8protocol8Debugger8Frontend19scriptFailedToParseERKNS_8String16ES5_iiiiiS5_N30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS0_15DictionaryValueEEENS8_10ValueMaybeIS3_EENSC_IbEESE_NSC_IiEENS9_INS0_7Runtime10StackTraceEEE:
.LFB6082:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	56(%rbp), %rax
	movl	%r8d, -192(%rbp)
	movq	48(%rbp), %rdx
	movq	40(%rbp), %r8
	movl	%ecx, -184(%rbp)
	movq	%rax, -216(%rbp)
	movq	64(%rbp), %rax
	movl	%r9d, -208(%rbp)
	movq	32(%rbp), %r13
	movq	%rax, -224(%rbp)
	movq	72(%rbp), %rax
	movq	%r8, -256(%rbp)
	movq	%rax, -232(%rbp)
	movq	80(%rbp), %rax
	movq	%rdx, -248(%rbp)
	movq	%rax, -240(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, (%rdi)
	je	.L5400
	movd	16(%rbp), %xmm2
	movd	%r9d, %xmm1
	movq	%rdi, %rbx
	movq	%rsi, %r15
	movd	-192(%rbp), %xmm3
	movd	%ecx, %xmm0
	movl	$232, %edi
	punpckldq	%xmm2, %xmm1
	punpckldq	%xmm3, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -208(%rbp)
	call	_Znwm@PLT
	xorl	%esi, %esi
	pxor	%xmm1, %xmm1
	xorl	%edx, %edx
	movq	%rax, %r14
	xorl	%ecx, %ecx
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationE(%rip), %rax
	leaq	64(%r14), %r10
	leaq	24(%r14), %r9
	movq	%rax, (%r14)
	movq	%r10, 48(%r14)
	leaq	112(%r14), %r11
	leaq	8(%r14), %rdi
	leaq	128(%r14), %r10
	movq	%r9, 8(%r14)
	leaq	48(%r14), %r9
	movq	%r10, 112(%r14)
	leaq	184(%r14), %r10
	movw	%si, 128(%r14)
	movq	%r15, %rsi
	movups	%xmm1, 184(%r14)
	pxor	%xmm1, %xmm1
	movw	%dx, 24(%r14)
	movw	%cx, 64(%r14)
	movq	%r11, -184(%rbp)
	movq	%r10, 168(%r14)
	movups	%xmm1, 88(%r14)
	movq	%r9, -192(%rbp)
	movq	$0, 16(%r14)
	movq	$0, 40(%r14)
	movq	$0, 56(%r14)
	movq	$0, 80(%r14)
	movq	$0, 120(%r14)
	movq	$0, 144(%r14)
	movq	$0, 152(%r14)
	movb	$0, 160(%r14)
	movq	$0, 200(%r14)
	movq	$0, 176(%r14)
	movl	$0, 208(%r14)
	movb	$0, 212(%r14)
	movl	$0, 216(%r14)
	movq	$0, 224(%r14)
	movl	$0, 104(%r14)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r15), %rsi
	movq	-192(%rbp), %r9
	movq	%rsi, 40(%r14)
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r12), %rsi
	movq	-184(%rbp), %r11
	movdqa	-208(%rbp), %xmm0
	movq	%rsi, 80(%r14)
	movl	24(%rbp), %esi
	movq	%r11, %rdi
	movups	%xmm0, 88(%r14)
	movl	%esi, 104(%r14)
	movq	%r13, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r13), %rcx
	movq	-256(%rbp), %r8
	movq	-248(%rbp), %rdx
	movq	%rcx, 144(%r14)
	movq	(%r8), %rcx
	testq	%rcx, %rcx
	je	.L5403
	movq	152(%r14), %rdi
	movq	$0, (%r8)
	movq	%rcx, 152(%r14)
	testq	%rdi, %rdi
	je	.L5403
	movq	(%rdi), %rcx
	movq	%rdx, -184(%rbp)
	call	*24(%rcx)
	movq	-184(%rbp), %rdx
.L5403:
	cmpb	$0, (%rdx)
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %r15
	jne	.L5445
.L5405:
	movq	-216(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L5410
.L5448:
	movzbl	1(%rax), %edx
	movb	$1, 208(%r14)
	movb	%dl, 209(%r14)
.L5410:
	movq	-224(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L5411
	movzbl	1(%rax), %edx
	movb	$1, 210(%r14)
	movb	%dl, 211(%r14)
.L5411:
	movq	-232(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L5412
	movl	4(%rax), %edx
	movb	$1, 212(%r14)
	movl	%edx, 216(%r14)
.L5412:
	movq	-240(%rbp), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L5414
	movq	224(%r14), %r13
	movq	$0, (%rax)
	movq	%rdx, 224(%r14)
	testq	%r13, %r13
	je	.L5414
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L5416
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L5414:
	movq	(%rbx), %r13
	leaq	.LC86(%rip), %rsi
	movq	%r12, %rdi
	movq	0(%r13), %rdx
	movq	%r14, -168(%rbp)
	movq	24(%rdx), %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	-160(%rbp), %rdi
	leaq	-168(%rbp), %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol16InternalResponse18createNotificationERKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rax
	movq	%r13, %rdi
	movq	$0, -160(%rbp)
	leaq	-152(%rbp), %rsi
	movq	%rax, -152(%rbp)
	call	*%rbx
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5417
	movq	(%rdi), %rax
	call	*24(%rax)
.L5417:
	movq	-160(%rbp), %r13
	testq	%r13, %r13
	je	.L5418
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol16InternalResponseD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5419
	movq	56(%r13), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L5420
	movq	(%rdi), %rax
	call	*24(%rax)
.L5420:
	movq	16(%r13), %rdi
	leaq	32(%r13), %rax
	cmpq	%rax, %rdi
	je	.L5421
	call	_ZdlPv@PLT
.L5421:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L5418:
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L5422
	call	_ZdlPv@PLT
.L5422:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5400
	movq	(%rdi), %rax
	call	*24(%rax)
.L5400:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5446
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5445:
	.cfi_restore_state
	movq	8(%rdx), %rsi
	leaq	-128(%rbp), %rcx
	leaq	24(%rdx), %r8
	movq	%rcx, -144(%rbp)
	cmpq	%r8, %rsi
	je	.L5447
	movq	24(%rdx), %rdi
	movq	%rsi, -144(%rbp)
	movq	%rdi, -128(%rbp)
.L5407:
	xorl	%eax, %eax
	movq	16(%rdx), %rdi
	movq	%r8, 8(%rdx)
	leaq	-96(%rbp), %r12
	movw	%ax, 24(%rdx)
	leaq	-80(%rbp), %r15
	movq	$0, 16(%rdx)
	movq	40(%rdx), %rdx
	movq	%rdi, -136(%rbp)
	movq	%rdx, -112(%rbp)
	leaq	(%rsi,%rdi,2), %rdx
	movq	%r12, %rdi
	movq	%rcx, -184(%rbp)
	movq	%r15, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-112(%rbp), %rdx
	leaq	168(%r14), %rdi
	movq	%r12, %rsi
	movq	%rdx, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-96(%rbp), %rdi
	movq	-64(%rbp), %rdx
	movb	$1, 160(%r14)
	movq	-184(%rbp), %rcx
	cmpq	%r15, %rdi
	movq	%rdx, 200(%r14)
	je	.L5408
	call	_ZdlPv@PLT
	movq	-184(%rbp), %rcx
.L5408:
	movq	-144(%rbp), %rdi
	cmpq	%rcx, %rdi
	je	.L5405
	call	_ZdlPv@PLT
	movq	-216(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L5410
	jmp	.L5448
	.p2align 4,,10
	.p2align 3
.L5447:
	movdqu	24(%rdx), %xmm4
	movq	%rcx, %rsi
	movaps	%xmm4, -128(%rbp)
	jmp	.L5407
	.p2align 4,,10
	.p2align 3
.L5419:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L5418
	.p2align 4,,10
	.p2align 3
.L5416:
	call	*%rdx
	jmp	.L5414
.L5446:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6082:
	.size	_ZN12v8_inspector8protocol8Debugger8Frontend19scriptFailedToParseERKNS_8String16ES5_iiiiiS5_N30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS0_15DictionaryValueEEENS8_10ValueMaybeIS3_EENSC_IbEESE_NSC_IiEENS9_INS0_7Runtime10StackTraceEEE, .-_ZN12v8_inspector8protocol8Debugger8Frontend19scriptFailedToParseERKNS_8String16ES5_iiiiiS5_N30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS0_15DictionaryValueEEENS8_10ValueMaybeIS3_EENSC_IbEESE_NSC_IiEENS9_INS0_7Runtime10StackTraceEEE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger8Frontend12scriptParsedERKNS_8String16ES5_iiiiiS5_N30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS0_15DictionaryValueEEENS8_10ValueMaybeIbEENSC_IS3_EESD_SD_NSC_IiEENS9_INS0_7Runtime10StackTraceEEE.str1.1,"aMS",@progbits,1
.LC87:
	.string	"Debugger.scriptParsed"
	.section	.text._ZN12v8_inspector8protocol8Debugger8Frontend12scriptParsedERKNS_8String16ES5_iiiiiS5_N30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS0_15DictionaryValueEEENS8_10ValueMaybeIbEENSC_IS3_EESD_SD_NSC_IiEENS9_INS0_7Runtime10StackTraceEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger8Frontend12scriptParsedERKNS_8String16ES5_iiiiiS5_N30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS0_15DictionaryValueEEENS8_10ValueMaybeIbEENSC_IS3_EESD_SD_NSC_IiEENS9_INS0_7Runtime10StackTraceEEE
	.type	_ZN12v8_inspector8protocol8Debugger8Frontend12scriptParsedERKNS_8String16ES5_iiiiiS5_N30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS0_15DictionaryValueEEENS8_10ValueMaybeIbEENSC_IS3_EESD_SD_NSC_IiEENS9_INS0_7Runtime10StackTraceEEE, @function
_ZN12v8_inspector8protocol8Debugger8Frontend12scriptParsedERKNS_8String16ES5_iiiiiS5_N30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS0_15DictionaryValueEEENS8_10ValueMaybeIbEENSC_IS3_EESD_SD_NSC_IiEENS9_INS0_7Runtime10StackTraceEEE:
.LFB6087:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	64(%rbp), %rax
	movq	56(%rbp), %rdx
	movl	%r8d, -192(%rbp)
	movq	40(%rbp), %r8
	movq	32(%rbp), %r13
	movl	%r9d, -208(%rbp)
	movq	%rax, -216(%rbp)
	movq	72(%rbp), %rax
	movq	48(%rbp), %r9
	movl	%ecx, -184(%rbp)
	movq	%rax, -224(%rbp)
	movq	80(%rbp), %rax
	movq	%r8, -264(%rbp)
	movq	%rax, -232(%rbp)
	movq	88(%rbp), %rax
	movq	%r9, -256(%rbp)
	movq	%rdx, -248(%rbp)
	movq	%rax, -240(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, (%rdi)
	je	.L5449
	movd	16(%rbp), %xmm2
	movd	%ecx, %xmm0
	movq	%rdi, %rbx
	movq	%rsi, %r15
	movd	-192(%rbp), %xmm3
	movd	-208(%rbp), %xmm1
	movl	$240, %edi
	punpckldq	%xmm2, %xmm1
	punpckldq	%xmm3, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -208(%rbp)
	call	_Znwm@PLT
	xorl	%esi, %esi
	pxor	%xmm1, %xmm1
	xorl	%edx, %edx
	movq	%rax, %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger24ScriptParsedNotificationE(%rip), %rax
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	24(%r14), %r10
	leaq	112(%r14), %r11
	movq	%rax, (%r14)
	movq	%r10, 8(%r14)
	leaq	64(%r14), %r10
	leaq	48(%r14), %rax
	movq	%r10, 48(%r14)
	leaq	128(%r14), %r10
	leaq	8(%r14), %rdi
	movq	%r10, 112(%r14)
	leaq	192(%r14), %r10
	movw	%si, 128(%r14)
	movq	%r15, %rsi
	movups	%xmm1, 192(%r14)
	pxor	%xmm1, %xmm1
	movw	%dx, 24(%r14)
	movw	%cx, 64(%r14)
	movw	%r8w, 160(%r14)
	movq	%r11, -184(%rbp)
	movq	%r10, 176(%r14)
	movups	%xmm1, 88(%r14)
	movq	%rax, -192(%rbp)
	movq	$0, 16(%r14)
	movq	$0, 40(%r14)
	movq	$0, 56(%r14)
	movq	$0, 80(%r14)
	movq	$0, 120(%r14)
	movq	$0, 144(%r14)
	movq	$0, 152(%r14)
	movb	$0, 168(%r14)
	movq	$0, 208(%r14)
	movq	$0, 184(%r14)
	movl	$0, 216(%r14)
	movb	$0, 220(%r14)
	movl	$0, 224(%r14)
	movq	$0, 232(%r14)
	movl	$0, 104(%r14)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r15), %rsi
	movq	-192(%rbp), %rax
	movq	%rsi, 40(%r14)
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r12), %rsi
	movq	-184(%rbp), %r11
	movdqa	-208(%rbp), %xmm0
	movq	%rsi, 80(%r14)
	movl	24(%rbp), %esi
	movq	%r11, %rdi
	movups	%xmm0, 88(%r14)
	movl	%esi, 104(%r14)
	movq	%r13, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r13), %rcx
	movq	-264(%rbp), %r8
	movq	-248(%rbp), %rdx
	movq	-256(%rbp), %r9
	movq	%rcx, 144(%r14)
	movq	(%r8), %rcx
	testq	%rcx, %rcx
	je	.L5452
	movq	152(%r14), %rdi
	movq	$0, (%r8)
	movq	%rcx, 152(%r14)
	testq	%rdi, %rdi
	je	.L5452
	movq	(%rdi), %rcx
	movq	%rdx, -192(%rbp)
	movq	%r9, -184(%rbp)
	call	*24(%rcx)
	movq	-192(%rbp), %rdx
	movq	-184(%rbp), %r9
.L5452:
	cmpb	$0, (%r9)
	je	.L5454
	movzbl	1(%r9), %ecx
	movb	$1, 160(%r14)
	movb	%cl, 161(%r14)
.L5454:
	cmpb	$0, (%rdx)
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %r15
	jne	.L5495
.L5455:
	movq	-216(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L5460
.L5498:
	movzbl	1(%rax), %edx
	movb	$1, 216(%r14)
	movb	%dl, 217(%r14)
.L5460:
	movq	-224(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L5461
	movzbl	1(%rax), %edx
	movb	$1, 218(%r14)
	movb	%dl, 219(%r14)
.L5461:
	movq	-232(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L5462
	movl	4(%rax), %edx
	movb	$1, 220(%r14)
	movl	%edx, 224(%r14)
.L5462:
	movq	-240(%rbp), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L5464
	movq	232(%r14), %r13
	movq	$0, (%rax)
	movq	%rdx, 232(%r14)
	testq	%r13, %r13
	je	.L5464
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L5466
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L5464:
	movq	(%rbx), %r13
	leaq	.LC87(%rip), %rsi
	movq	%r12, %rdi
	movq	0(%r13), %rdx
	movq	%r14, -168(%rbp)
	movq	24(%rdx), %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	-160(%rbp), %rdi
	leaq	-168(%rbp), %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol16InternalResponse18createNotificationERKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rax
	movq	%r13, %rdi
	movq	$0, -160(%rbp)
	leaq	-152(%rbp), %rsi
	movq	%rax, -152(%rbp)
	call	*%rbx
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5467
	movq	(%rdi), %rax
	call	*24(%rax)
.L5467:
	movq	-160(%rbp), %r13
	testq	%r13, %r13
	je	.L5468
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol16InternalResponseD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5469
	movq	56(%r13), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L5470
	movq	(%rdi), %rax
	call	*24(%rax)
.L5470:
	movq	16(%r13), %rdi
	leaq	32(%r13), %rax
	cmpq	%rax, %rdi
	je	.L5471
	call	_ZdlPv@PLT
.L5471:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L5468:
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L5472
	call	_ZdlPv@PLT
.L5472:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5449
	movq	(%rdi), %rax
	call	*24(%rax)
.L5449:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5496
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5495:
	.cfi_restore_state
	movq	8(%rdx), %rsi
	leaq	-128(%rbp), %rcx
	leaq	24(%rdx), %r8
	movq	%rcx, -144(%rbp)
	cmpq	%r8, %rsi
	je	.L5497
	movq	24(%rdx), %rdi
	movq	%rsi, -144(%rbp)
	movq	%rdi, -128(%rbp)
.L5457:
	xorl	%eax, %eax
	movq	16(%rdx), %rdi
	movq	%r8, 8(%rdx)
	leaq	-96(%rbp), %r12
	movw	%ax, 24(%rdx)
	leaq	-80(%rbp), %r15
	movq	$0, 16(%rdx)
	movq	40(%rdx), %rdx
	movq	%rdi, -136(%rbp)
	movq	%rdx, -112(%rbp)
	leaq	(%rsi,%rdi,2), %rdx
	movq	%r12, %rdi
	movq	%rcx, -184(%rbp)
	movq	%r15, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-112(%rbp), %rdx
	leaq	176(%r14), %rdi
	movq	%r12, %rsi
	movq	%rdx, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-96(%rbp), %rdi
	movq	-64(%rbp), %rdx
	movb	$1, 168(%r14)
	movq	-184(%rbp), %rcx
	cmpq	%r15, %rdi
	movq	%rdx, 208(%r14)
	je	.L5458
	call	_ZdlPv@PLT
	movq	-184(%rbp), %rcx
.L5458:
	movq	-144(%rbp), %rdi
	cmpq	%rcx, %rdi
	je	.L5455
	call	_ZdlPv@PLT
	movq	-216(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L5460
	jmp	.L5498
	.p2align 4,,10
	.p2align 3
.L5497:
	movdqu	24(%rdx), %xmm4
	movq	%rcx, %rsi
	movaps	%xmm4, -128(%rbp)
	jmp	.L5457
	.p2align 4,,10
	.p2align 3
.L5469:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L5468
	.p2align 4,,10
	.p2align 3
.L5466:
	call	*%rdx
	jmp	.L5464
.L5496:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6087:
	.size	_ZN12v8_inspector8protocol8Debugger8Frontend12scriptParsedERKNS_8String16ES5_iiiiiS5_N30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS0_15DictionaryValueEEENS8_10ValueMaybeIbEENSC_IS3_EESD_SD_NSC_IiEENS9_INS0_7Runtime10StackTraceEEE, .-_ZN12v8_inspector8protocol8Debugger8Frontend12scriptParsedERKNS_8String16ES5_iiiiiS5_N30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS0_15DictionaryValueEEENS8_10ValueMaybeIbEENSC_IS3_EESD_SD_NSC_IiEENS9_INS0_7Runtime10StackTraceEEE
	.section	.text._ZNK12v8_inspector8protocol8Debugger11SearchMatch7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Debugger11SearchMatch7toValueEv
	.type	_ZNK12v8_inspector8protocol8Debugger11SearchMatch7toValueEv, @function
_ZNK12v8_inspector8protocol8Debugger11SearchMatch7toValueEv:
.LFB6046:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$96, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r14
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r14, (%r12)
	movsd	16(%rbx), %xmm0
	movl	$24, %edi
	movsd	%xmm0, -120(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movsd	-120(%rbp), %xmm0
	movq	%r13, %rdi
	movl	$3, 8(%rax)
	leaq	.LC18(%rip), %rsi
	movq	%rcx, (%rax)
	movsd	%xmm0, 16(%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %r14
	cmpq	%r14, %rdi
	je	.L5500
	call	_ZdlPv@PLT
.L5500:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5501
	movq	(%rdi), %rax
	call	*24(%rax)
.L5501:
	movq	(%r12), %r8
	movl	$56, %edi
	movq	%r8, -128(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rcx
	movq	24(%rbx), %rsi
	leaq	32(%rax), %rdx
	movq	%rcx, (%rax)
	leaq	16(%rax), %rdi
	movq	%rdx, 16(%rax)
	movq	32(%rbx), %rdx
	movl	$4, 8(%rax)
	leaq	(%rsi,%rdx,2), %rdx
	movq	%rax, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-120(%rbp), %rax
	movq	56(%rbx), %rdx
	movq	%r13, %rdi
	leaq	.LC25(%rip), %rsi
	movq	%rdx, 48(%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L5502
	call	_ZdlPv@PLT
.L5502:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5499
	movq	(%rdi), %rax
	call	*24(%rax)
.L5499:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5512
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5512:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6046:
	.size	_ZNK12v8_inspector8protocol8Debugger11SearchMatch7toValueEv, .-_ZNK12v8_inspector8protocol8Debugger11SearchMatch7toValueEv
	.section	.text._ZNK12v8_inspector8protocol8Debugger11SearchMatch5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Debugger11SearchMatch5cloneEv
	.type	_ZNK12v8_inspector8protocol8Debugger11SearchMatch5cloneEv, @function
_ZNK12v8_inspector8protocol8Debugger11SearchMatch5cloneEv:
.LFB6047:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol8Debugger11SearchMatch7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol8Debugger11SearchMatch9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5514
	movq	(%rdi), %rax
	call	*24(%rax)
.L5514:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5520
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5520:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6047:
	.size	_ZNK12v8_inspector8protocol8Debugger11SearchMatch5cloneEv, .-_ZNK12v8_inspector8protocol8Debugger11SearchMatch5cloneEv
	.section	.text._ZNK12v8_inspector8protocol8Debugger11SearchMatch12toJSONStringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Debugger11SearchMatch12toJSONStringEv
	.type	_ZNK12v8_inspector8protocol8Debugger11SearchMatch12toJSONStringEv, @function
_ZNK12v8_inspector8protocol8Debugger11SearchMatch12toJSONStringEv:
.LFB6048:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-88(%rbp), %r13
	leaq	-80(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger11SearchMatch7toValueEv
	movq	-88(%rbp), %rsi
	movq	%r14, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5522
	movq	(%rdi), %rax
	call	*24(%rax)
.L5522:
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector16StringBufferImpl5adoptERNS_8String16E@PLT
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rdi
	movq	%rax, (%r12)
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5521
	call	_ZdlPv@PLT
.L5521:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5529
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5529:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6048:
	.size	_ZNK12v8_inspector8protocol8Debugger11SearchMatch12toJSONStringEv, .-_ZNK12v8_inspector8protocol8Debugger11SearchMatch12toJSONStringEv
	.set	.LTHUNK8,_ZNK12v8_inspector8protocol8Debugger11SearchMatch12toJSONStringEv
	.section	.text._ZThn8_NK12v8_inspector8protocol8Debugger11SearchMatch12toJSONStringEv,"ax",@progbits
	.p2align 4
	.globl	_ZThn8_NK12v8_inspector8protocol8Debugger11SearchMatch12toJSONStringEv
	.type	_ZThn8_NK12v8_inspector8protocol8Debugger11SearchMatch12toJSONStringEv, @function
_ZThn8_NK12v8_inspector8protocol8Debugger11SearchMatch12toJSONStringEv:
.LFB12827:
	.cfi_startproc
	endbr64
	subq	$8, %rsi
	jmp	.LTHUNK8
	.cfi_endproc
.LFE12827:
	.size	_ZThn8_NK12v8_inspector8protocol8Debugger11SearchMatch12toJSONStringEv, .-_ZThn8_NK12v8_inspector8protocol8Debugger11SearchMatch12toJSONStringEv
	.section	.text._ZNK12v8_inspector8protocol8Debugger11SearchMatch11writeBinaryEPSt6vectorIhSaIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Debugger11SearchMatch11writeBinaryEPSt6vectorIhSaIhEE
	.type	_ZNK12v8_inspector8protocol8Debugger11SearchMatch11writeBinaryEPSt6vectorIhSaIhEE, @function
_ZNK12v8_inspector8protocol8Debugger11SearchMatch11writeBinaryEPSt6vectorIhSaIhEE:
.LFB6051:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movq	%r8, %rsi
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger11SearchMatch7toValueEv
	movq	-32(%rbp), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*80(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5530
	movq	(%rdi), %rax
	call	*24(%rax)
.L5530:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5537
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5537:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6051:
	.size	_ZNK12v8_inspector8protocol8Debugger11SearchMatch11writeBinaryEPSt6vectorIhSaIhEE, .-_ZNK12v8_inspector8protocol8Debugger11SearchMatch11writeBinaryEPSt6vectorIhSaIhEE
	.section	.text._ZN12v8_inspector8protocol8Debugger11SearchMatch17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger11SearchMatch17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger11SearchMatch17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Debugger11SearchMatch17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Debugger11SearchMatch17serializeToBinaryEv:
.LFB5706:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger11SearchMatch7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5538
	movq	(%rdi), %rax
	call	*24(%rax)
.L5538:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5545
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5545:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5706:
	.size	_ZN12v8_inspector8protocol8Debugger11SearchMatch17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Debugger11SearchMatch17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Debugger11SearchMatch15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger11SearchMatch15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger11SearchMatch15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Debugger11SearchMatch15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Debugger11SearchMatch15serializeToJSONEv:
.LFB5705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger11SearchMatch7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5546
	movq	(%rdi), %rax
	call	*24(%rax)
.L5546:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5553
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5553:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5705:
	.size	_ZN12v8_inspector8protocol8Debugger11SearchMatch15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Debugger11SearchMatch15serializeToJSONEv
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImpl15searchInContentEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC88:
	.string	"query"
.LC89:
	.string	"caseSensitive"
.LC90:
	.string	"isRegex"
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl15searchInContentEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl15searchInContentEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl15searchInContentEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl15searchInContentEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6247:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$328, %rsp
	movl	%esi, -304(%rbp)
	movq	(%r8), %r13
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -352(%rbp)
	movq	%rcx, -360(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L5555
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r13
	jne	.L5556
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L5629
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L5559:
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, -312(%rbp)
	cmpq	%r15, %rdi
	je	.L5561
	call	_ZdlPv@PLT
.L5561:
	leaq	.LC22(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-256(%rbp), %rax
	movq	-312(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	movq	%rax, -336(%rbp)
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC88(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, -312(%rbp)
	cmpq	%r15, %rdi
	je	.L5562
	call	_ZdlPv@PLT
.L5562:
	leaq	.LC88(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-208(%rbp), %rax
	movq	-312(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC89(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%r15, %rdi
	je	.L5595
	movq	%r8, -312(%rbp)
	call	_ZdlPv@PLT
	movq	-312(%rbp), %r8
.L5595:
	testq	%r8, %r8
	movq	%r8, -312(%rbp)
	je	.L5602
	leaq	.LC89(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-312(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsIbE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movb	$1, -321(%rbp)
	movb	%al, -320(%rbp)
.L5563:
	leaq	.LC90(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r13
	cmpq	%r15, %rdi
	je	.L5593
	call	_ZdlPv@PLT
.L5593:
	testq	%r13, %r13
	je	.L5603
	leaq	.LC90(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %rsi
	movl	$1, %r13d
	call	_ZN12v8_inspector8protocol16ValueConversionsIbE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movb	%al, -312(%rbp)
.L5560:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L5630
	leaq	-280(%rbp), %rdi
	movq	%r14, %rsi
	movq	$0, -288(%rbp)
	leaq	-272(%rbp), %rbx
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	subq	$8, %rsp
	movq	%rbx, %r8
	movq	%r12, %rdi
	movq	184(%r14), %rsi
	movzbl	-312(%rbp), %ecx
	leaq	-288(%rbp), %rdx
	movq	(%rsi), %rax
	movq	112(%rax), %rax
	movb	%cl, -263(%rbp)
	movzbl	-321(%rbp), %ecx
	movb	%r13b, -264(%rbp)
	leaq	-264(%rbp), %r13
	pushq	%rdx
	movq	%r13, %r9
	movq	-336(%rbp), %rdx
	movb	%cl, -272(%rbp)
	movzbl	-320(%rbp), %ecx
	movb	%cl, -271(%rbp)
	movq	-344(%rbp), %rcx
	call	*%rax
	cmpl	$2, -112(%rbp)
	popq	%rdx
	popq	%rcx
	je	.L5631
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L5632
.L5569:
	movq	-280(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L5633
	movl	-304(%rbp), %esi
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r14, -264(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE@PLT
	movq	-264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5568
	movq	(%rdi), %rax
	call	*24(%rax)
.L5568:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5579
	call	_ZdlPv@PLT
.L5579:
	movq	-280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5580
	call	_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0
.L5580:
	movq	-288(%rbp), %r14
	testq	%r14, %r14
	je	.L5566
	movq	8(%r14), %rbx
	movq	(%r14), %r13
	cmpq	%r13, %rbx
	je	.L5582
	leaq	80+_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE(%rip), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev(%rip), %r15
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm2
	movq	%rdx, %xmm1
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm1, -304(%rbp)
	jmp	.L5586
	.p2align 4,,10
	.p2align 3
.L5635:
	movdqa	-304(%rbp), %xmm0
	movq	24(%r12), %rdi
	leaq	40(%r12), %rax
	movups	%xmm0, (%r12)
	cmpq	%rax, %rdi
	je	.L5585
	call	_ZdlPv@PLT
.L5585:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L5583:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L5634
.L5586:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L5583
	movq	(%r12), %rax
	movq	24(%rax), %rax
	cmpq	%r15, %rax
	je	.L5635
	addq	$8, %r13
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r13, %rbx
	jne	.L5586
	.p2align 4,,10
	.p2align 3
.L5634:
	movq	(%r14), %r13
.L5582:
	testq	%r13, %r13
	je	.L5587
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L5587:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L5566:
	movq	-208(%rbp), %rdi
	leaq	-192(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5588
	call	_ZdlPv@PLT
.L5588:
	movq	-256(%rbp), %rdi
	leaq	-240(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5554
	call	_ZdlPv@PLT
.L5554:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5636
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5629:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L5559
	.p2align 4,,10
	.p2align 3
.L5555:
	movq	-112(%rbp), %rdi
.L5556:
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L5637
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L5558:
	leaq	.LC22(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%r13d, %r13d
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-256(%rbp), %rax
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, -336(%rbp)
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC88(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-208(%rbp), %rax
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movb	$0, -320(%rbp)
	movb	$0, -321(%rbp)
	movb	$0, -312(%rbp)
	jmp	.L5560
	.p2align 4,,10
	.p2align 3
.L5630:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	-304(%rbp), %esi
	movl	$-32602, %edx
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L5566
	call	_ZdlPv@PLT
	jmp	.L5566
	.p2align 4,,10
	.p2align 3
.L5633:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L5568
	.p2align 4,,10
	.p2align 3
.L5631:
	movq	8(%r14), %rdi
	movq	-360(%rbp), %rcx
	movq	-352(%rbp), %rdx
	movl	-304(%rbp), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L5568
	.p2align 4,,10
	.p2align 3
.L5637:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L5558
	.p2align 4,,10
	.p2align 3
.L5632:
	movl	$40, %edi
	movq	-288(%rbp), %r15
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -312(%rbp)
	call	_ZN12v8_inspector8protocol9ListValueC1Ev@PLT
	movq	-312(%rbp), %rax
	movq	8(%r15), %rsi
	subq	(%r15), %rsi
	leaq	16(%rax), %rdi
	sarq	$3, %rsi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	movq	8(%r15), %rax
	movq	(%r15), %r15
	movq	%rax, -320(%rbp)
	cmpq	%rax, %r15
	je	.L5575
	.p2align 4,,10
	.p2align 3
.L5576:
	movq	(%r15), %rsi
	movq	%rbx, %rdi
	call	_ZNK12v8_inspector8protocol8Debugger11SearchMatch7toValueEv
	movq	-272(%rbp), %rdx
	movq	-312(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rdx, -264(%rbp)
	call	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE@PLT
	movq	-264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5573
	movq	(%rdi), %rdx
	addq	$8, %r15
	call	*24(%rdx)
	cmpq	%r15, -320(%rbp)
	jne	.L5576
.L5575:
	movq	-312(%rbp), %rax
	leaq	-160(%rbp), %r15
	leaq	.LC55(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5572
	call	_ZdlPv@PLT
.L5572:
	movq	-264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5569
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L5569
	.p2align 4,,10
	.p2align 3
.L5573:
	addq	$8, %r15
	cmpq	%r15, -320(%rbp)
	jne	.L5576
	jmp	.L5575
	.p2align 4,,10
	.p2align 3
.L5603:
	movb	$0, -312(%rbp)
	xorl	%r13d, %r13d
	jmp	.L5560
	.p2align 4,,10
	.p2align 3
.L5602:
	movb	$0, -320(%rbp)
	movb	$0, -321(%rbp)
	jmp	.L5563
.L5636:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6247:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl15searchInContentEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl15searchInContentEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZThn8_NK12v8_inspector8protocol8Debugger11SearchMatch11writeBinaryEPSt6vectorIhSaIhEE,"ax",@progbits
	.p2align 4
	.globl	_ZThn8_NK12v8_inspector8protocol8Debugger11SearchMatch11writeBinaryEPSt6vectorIhSaIhEE
	.type	_ZThn8_NK12v8_inspector8protocol8Debugger11SearchMatch11writeBinaryEPSt6vectorIhSaIhEE, @function
_ZThn8_NK12v8_inspector8protocol8Debugger11SearchMatch11writeBinaryEPSt6vectorIhSaIhEE:
.LFB12781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	-8(%rdi), %rsi
	leaq	-32(%rbp), %r8
	movq	%r8, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger11SearchMatch7toValueEv
	movq	-32(%rbp), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*80(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5638
	movq	(%rdi), %rax
	call	*24(%rax)
.L5638:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5645
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5645:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12781:
	.size	_ZThn8_NK12v8_inspector8protocol8Debugger11SearchMatch11writeBinaryEPSt6vectorIhSaIhEE, .-_ZThn8_NK12v8_inspector8protocol8Debugger11SearchMatch11writeBinaryEPSt6vectorIhSaIhEE
	.section	.text._ZNK12v8_inspector8protocol8Debugger8Location7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Debugger8Location7toValueEv
	.type	_ZNK12v8_inspector8protocol8Debugger8Location7toValueEv, @function
_ZNK12v8_inspector8protocol8Debugger8Location7toValueEv:
.LFB6034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$96, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r14
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r14, (%r12)
	movl	$56, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rsi
	movq	%rax, %r13
	movl	$4, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, 0(%r13)
	leaq	32(%r13), %rax
	leaq	16(%r13), %rdi
	movq	%rax, 16(%r13)
	movq	16(%rbx), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	40(%rbx), %rax
	movq	%r13, -104(%rbp)
	leaq	.LC22(%rip), %rsi
	movq	%rax, 48(%r13)
	leaq	-96(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %r14
	cmpq	%r14, %rdi
	je	.L5647
	call	_ZdlPv@PLT
.L5647:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5648
	movq	(%rdi), %rax
	call	*24(%rax)
.L5648:
	movq	(%r12), %r8
	movl	48(%rbx), %edx
	movl	$24, %edi
	movq	%r8, -128(%rbp)
	movl	%edx, -120(%rbp)
	call	_Znwm@PLT
	movl	-120(%rbp), %edx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movq	%r13, %rdi
	movl	$2, 8(%rax)
	leaq	.LC18(%rip), %rsi
	movl	%edx, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L5649
	call	_ZdlPv@PLT
.L5649:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5650
	movq	(%rdi), %rax
	call	*24(%rax)
.L5650:
	cmpb	$0, 52(%rbx)
	jne	.L5665
.L5646:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5666
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5665:
	.cfi_restore_state
	movq	(%r12), %r8
	movl	56(%rbx), %ebx
	movl	$24, %edi
	movq	%r8, -120(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	leaq	.LC19(%rip), %rsi
	movq	%r13, %rdi
	movl	$2, 8(%rax)
	movq	%rcx, (%rax)
	movl	%ebx, 16(%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-120(%rbp), %r8
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L5652
	call	_ZdlPv@PLT
.L5652:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5646
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L5646
.L5666:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6034:
	.size	_ZNK12v8_inspector8protocol8Debugger8Location7toValueEv, .-_ZNK12v8_inspector8protocol8Debugger8Location7toValueEv
	.section	.text._ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv:
.LFB5522:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger8Location7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5667
	movq	(%rdi), %rax
	call	*24(%rax)
.L5667:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5674
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5674:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5522:
	.size	_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv:
.LFB5521:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger8Location7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5675
	movq	(%rdi), %rax
	call	*24(%rax)
.L5675:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5682
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5682:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5521:
	.size	_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv
	.section	.text._ZNK12v8_inspector8protocol8Debugger8Location5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Debugger8Location5cloneEv
	.type	_ZNK12v8_inspector8protocol8Debugger8Location5cloneEv, @function
_ZNK12v8_inspector8protocol8Debugger8Location5cloneEv:
.LFB6035:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol8Debugger8Location7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol8Debugger8Location9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5684
	movq	(%rdi), %rax
	call	*24(%rax)
.L5684:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5690
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5690:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6035:
	.size	_ZNK12v8_inspector8protocol8Debugger8Location5cloneEv, .-_ZNK12v8_inspector8protocol8Debugger8Location5cloneEv
	.section	.text._ZNK12v8_inspector8protocol8Debugger5Scope7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Debugger5Scope7toValueEv
	.type	_ZNK12v8_inspector8protocol8Debugger5Scope7toValueEv, @function
_ZNK12v8_inspector8protocol8Debugger5Scope7toValueEv:
.LFB6043:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$96, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r15, 0(%r13)
	movl	$56, %edi
	call	_Znwm@PLT
	leaq	8(%rbx), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol11StringValueC1ERKNS_8String16E
	movq	%r12, -104(%rbp)
	leaq	-96(%rbp), %r12
	leaq	.LC28(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rdi
	je	.L5692
	call	_ZdlPv@PLT
.L5692:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5693
	movq	(%rdi), %rax
	call	*24(%rax)
.L5693:
	movq	0(%r13), %r8
	movq	48(%rbx), %rsi
	movq	%r14, %rdi
	leaq	-112(%rbp), %r15
	movq	%r8, -128(%rbp)
	call	_ZNK12v8_inspector8protocol7Runtime12RemoteObject7toValueEv@PLT
	movq	-104(%rbp), %rax
	leaq	.LC29(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L5694
	call	_ZdlPv@PLT
.L5694:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5695
	movq	(%rdi), %rax
	call	*24(%rax)
.L5695:
	cmpb	$0, 56(%rbx)
	jne	.L5728
.L5696:
	movq	104(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L5699
	movq	0(%r13), %r8
	movq	%r14, %rdi
	movq	%r8, -128(%rbp)
	call	_ZNK12v8_inspector8protocol8Debugger8Location7toValueEv
	movq	-104(%rbp), %rax
	leaq	.LC31(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L5700
	call	_ZdlPv@PLT
.L5700:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5699
	movq	(%rdi), %rax
	call	*24(%rax)
.L5699:
	movq	112(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L5691
	movq	%r14, %rdi
	movq	0(%r13), %rbx
	call	_ZNK12v8_inspector8protocol8Debugger8Location7toValueEv
	movq	-104(%rbp), %rax
	leaq	.LC32(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L5703
	call	_ZdlPv@PLT
.L5703:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5691
	movq	(%rdi), %rax
	call	*24(%rax)
.L5691:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5729
	addq	$104, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5728:
	.cfi_restore_state
	movq	0(%r13), %r8
	movl	$56, %edi
	movq	%r8, -136(%rbp)
	call	_Znwm@PLT
	leaq	64(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN12v8_inspector8protocol11StringValueC1ERKNS_8String16E
	movq	-128(%rbp), %rax
	leaq	.LC30(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L5697
	call	_ZdlPv@PLT
.L5697:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5696
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L5696
.L5729:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6043:
	.size	_ZNK12v8_inspector8protocol8Debugger5Scope7toValueEv, .-_ZNK12v8_inspector8protocol8Debugger5Scope7toValueEv
	.section	.text._ZN12v8_inspector8protocol8Debugger5Scope17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger5Scope17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger5Scope17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Debugger5Scope17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Debugger5Scope17serializeToBinaryEv:
.LFB5671:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger5Scope7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5730
	movq	(%rdi), %rax
	call	*24(%rax)
.L5730:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5737
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5737:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5671:
	.size	_ZN12v8_inspector8protocol8Debugger5Scope17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Debugger5Scope17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Debugger5Scope15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger5Scope15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger5Scope15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Debugger5Scope15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Debugger5Scope15serializeToJSONEv:
.LFB5670:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger5Scope7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5738
	movq	(%rdi), %rax
	call	*24(%rax)
.L5738:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5745
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5745:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5670:
	.size	_ZN12v8_inspector8protocol8Debugger5Scope15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Debugger5Scope15serializeToJSONEv
	.section	.text._ZNK12v8_inspector8protocol8Debugger5Scope5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Debugger5Scope5cloneEv
	.type	_ZNK12v8_inspector8protocol8Debugger5Scope5cloneEv, @function
_ZNK12v8_inspector8protocol8Debugger5Scope5cloneEv:
.LFB6044:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol8Debugger5Scope7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol8Debugger5Scope9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5747
	movq	(%rdi), %rax
	call	*24(%rax)
.L5747:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5753
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5753:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6044:
	.size	_ZNK12v8_inspector8protocol8Debugger5Scope5cloneEv, .-_ZNK12v8_inspector8protocol8Debugger5Scope5cloneEv
	.section	.text._ZNK12v8_inspector8protocol8Debugger30BreakpointResolvedNotification7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Debugger30BreakpointResolvedNotification7toValueEv
	.type	_ZNK12v8_inspector8protocol8Debugger30BreakpointResolvedNotification7toValueEv, @function
_ZNK12v8_inspector8protocol8Debugger30BreakpointResolvedNotification7toValueEv:
.LFB6060:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$96, %edi
	leaq	-96(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r13, (%r14)
	movl	$56, %edi
	call	_Znwm@PLT
	movq	8(%r12), %rsi
	movq	%rax, %rbx
	movl	$4, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	32(%rbx), %rax
	leaq	16(%rbx), %rdi
	movq	%rax, 16(%rbx)
	movq	16(%r12), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	40(%r12), %rax
	movq	%rbx, -104(%rbp)
	movq	%r15, %rdi
	leaq	.LC10(%rip), %rsi
	movq	%rax, 48(%rbx)
	leaq	-104(%rbp), %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %r13
	cmpq	%r13, %rdi
	je	.L5755
	call	_ZdlPv@PLT
.L5755:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5756
	movq	(%rdi), %rax
	call	*24(%rax)
.L5756:
	movq	(%r14), %r8
	movq	48(%r12), %rsi
	movq	%rbx, %rdi
	movq	%r8, -120(%rbp)
	call	_ZNK12v8_inspector8protocol8Debugger8Location7toValueEv
	movq	-104(%rbp), %rax
	leaq	.LC26(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-120(%rbp), %r8
	leaq	-112(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L5757
	call	_ZdlPv@PLT
.L5757:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5754
	movq	(%rdi), %rax
	call	*24(%rax)
.L5754:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5767
	addq	$88, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5767:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6060:
	.size	_ZNK12v8_inspector8protocol8Debugger30BreakpointResolvedNotification7toValueEv, .-_ZNK12v8_inspector8protocol8Debugger30BreakpointResolvedNotification7toValueEv
	.section	.text._ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotification17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotification17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotification17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotification17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotification17serializeToBinaryEv:
.LFB5773:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger30BreakpointResolvedNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5768
	movq	(%rdi), %rax
	call	*24(%rax)
.L5768:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5775
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5775:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5773:
	.size	_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotification17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotification17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotification15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotification15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotification15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotification15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotification15serializeToJSONEv:
.LFB5772:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger30BreakpointResolvedNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5776
	movq	(%rdi), %rax
	call	*24(%rax)
.L5776:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5783
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5783:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5772:
	.size	_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotification15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotification15serializeToJSONEv
	.section	.text._ZNK12v8_inspector8protocol8Debugger30BreakpointResolvedNotification5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Debugger30BreakpointResolvedNotification5cloneEv
	.type	_ZNK12v8_inspector8protocol8Debugger30BreakpointResolvedNotification5cloneEv, @function
_ZNK12v8_inspector8protocol8Debugger30BreakpointResolvedNotification5cloneEv:
.LFB6061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol8Debugger30BreakpointResolvedNotification7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5785
	movq	(%rdi), %rax
	call	*24(%rax)
.L5785:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5791
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5791:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6061:
	.size	_ZNK12v8_inspector8protocol8Debugger30BreakpointResolvedNotification5cloneEv, .-_ZNK12v8_inspector8protocol8Debugger30BreakpointResolvedNotification5cloneEv
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImpl13setBreakpointEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC91:
	.string	"actualLocation"
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl13setBreakpointEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl13setBreakpointEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl13setBreakpointEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl13setBreakpointEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$312, %rsp
	movl	%esi, -316(%rbp)
	movq	(%r8), %r13
	leaq	.LC0(%rip), %rsi
	movq	%rdi, -312(%rbp)
	movq	%r12, %rdi
	movq	%rdx, -336(%rbp)
	movq	%rcx, -344(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L5793
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r13
	jne	.L5794
	leaq	-96(%rbp), %r14
	cmpq	%r14, %rdi
	je	.L5861
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L5797:
	leaq	.LC26(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r15
	cmpq	%r14, %rdi
	je	.L5799
	call	_ZdlPv@PLT
.L5799:
	leaq	.LC26(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%rbx, %rdx
	movq	%r15, %rsi
	leaq	-296(%rbp), %rdi
	call	_ZN12v8_inspector8protocol8Debugger8Location9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC85(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r15
	cmpq	%r14, %rdi
	je	.L5825
	call	_ZdlPv@PLT
.L5825:
	leaq	-184(%rbp), %r13
	xorl	%esi, %esi
	movb	$0, -208(%rbp)
	movq	%r13, -200(%rbp)
	movq	$0, -192(%rbp)
	movw	%si, -184(%rbp)
	movq	$0, -168(%rbp)
	testq	%r15, %r15
	je	.L5798
	leaq	.LC85(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	-200(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-80(%rbp), %rax
	movq	-112(%rbp), %rdi
	movb	$1, -208(%rbp)
	movq	%rax, -168(%rbp)
	cmpq	%r14, %rdi
	je	.L5798
	call	_ZdlPv@PLT
.L5798:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L5862
	movq	-312(%rbp), %rbx
	leaq	-240(%rbp), %rax
	xorl	%ecx, %ecx
	leaq	-280(%rbp), %rdi
	movq	%rax, -328(%rbp)
	leaq	-136(%rbp), %r14
	movq	%rbx, %rsi
	movq	%rax, -256(%rbp)
	movq	$0, -248(%rbp)
	movw	%cx, -240(%rbp)
	movq	$0, -224(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	movq	(%rsi), %rax
	movq	144(%rax), %r10
	movzbl	-208(%rbp), %eax
	movq	%r14, -152(%rbp)
	movb	%al, -160(%rbp)
	movq	-200(%rbp), %rax
	cmpq	%r13, %rax
	je	.L5863
	movq	%rax, -152(%rbp)
	movq	-184(%rbp), %rax
	movq	%rax, -136(%rbp)
.L5805:
	movq	-192(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	-160(%rbp), %r15
	leaq	-264(%rbp), %rbx
	movq	%r15, %rcx
	movw	%dx, -184(%rbp)
	leaq	-288(%rbp), %r9
	movq	%rax, -144(%rbp)
	movq	-168(%rbp), %rax
	movq	%rbx, %rdx
	movq	%r13, -200(%rbp)
	movq	%rax, -120(%rbp)
	movq	-296(%rbp), %rax
	movq	$0, -192(%rbp)
	movq	%rax, -264(%rbp)
	leaq	-256(%rbp), %rax
	movq	$0, -296(%rbp)
	movq	%rax, %r8
	movq	%rax, -352(%rbp)
	call	*%r10
	movq	-264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5806
	movq	(%rdi), %rax
	call	*24(%rax)
.L5806:
	movq	-152(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L5807
	call	_ZdlPv@PLT
.L5807:
	cmpl	$2, -112(%rbp)
	je	.L5864
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L5865
.L5810:
	movq	-280(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L5866
	movl	-316(%rbp), %esi
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r14, -264(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE@PLT
	movq	-264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5809
	movq	(%rdi), %rax
	call	*24(%rax)
.L5809:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5816
	call	_ZdlPv@PLT
.L5816:
	movq	-280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5817
	call	_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0
.L5817:
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5818
	movq	(%rdi), %rax
	call	*24(%rax)
.L5818:
	movq	-256(%rbp), %rdi
	cmpq	-328(%rbp), %rdi
	je	.L5803
.L5860:
	call	_ZdlPv@PLT
.L5803:
	movq	-200(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L5820
	call	_ZdlPv@PLT
.L5820:
	movq	-296(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5792
	movq	(%rdi), %rax
	call	*24(%rax)
.L5792:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5867
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5861:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L5797
	.p2align 4,,10
	.p2align 3
.L5793:
	movq	-112(%rbp), %rdi
.L5794:
	leaq	-96(%rbp), %r14
	cmpq	%r14, %rdi
	je	.L5868
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L5796:
	leaq	.LC26(%rip), %rsi
	movq	%rbx, %rdi
	leaq	-184(%rbp), %r13
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-296(%rbp), %rdi
	movq	%rbx, %rdx
	xorl	%esi, %esi
	call	_ZN12v8_inspector8protocol8Debugger8Location9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	xorl	%edi, %edi
	movb	$0, -208(%rbp)
	movq	%r13, -200(%rbp)
	movq	$0, -192(%rbp)
	movw	%di, -184(%rbp)
	movq	$0, -168(%rbp)
	jmp	.L5798
	.p2align 4,,10
	.p2align 3
.L5862:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	$-32602, %edx
	movq	-312(%rbp), %rdi
	movl	-316(%rbp), %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L5860
	jmp	.L5803
	.p2align 4,,10
	.p2align 3
.L5866:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L5809
	.p2align 4,,10
	.p2align 3
.L5864:
	movq	-312(%rbp), %rax
	movq	-344(%rbp), %rcx
	movq	-336(%rbp), %rdx
	movl	-316(%rbp), %esi
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L5809
	.p2align 4,,10
	.p2align 3
.L5868:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L5796
	.p2align 4,,10
	.p2align 3
.L5863:
	movdqu	-184(%rbp), %xmm0
	movups	%xmm0, -136(%rbp)
	jmp	.L5805
	.p2align 4,,10
	.p2align 3
.L5865:
	movl	$56, %edi
	call	_Znwm@PLT
	movq	-352(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -312(%rbp)
	call	_ZN12v8_inspector8protocol11StringValueC1ERKNS_8String16E
	movq	-312(%rbp), %rax
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%rbx, %rdx
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	movq	%rax, -312(%rbp)
	cmpq	%rax, %rdi
	je	.L5811
	call	_ZdlPv@PLT
.L5811:
	movq	-264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5812
	movq	(%rdi), %rax
	call	*24(%rax)
.L5812:
	movq	-288(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZNK12v8_inspector8protocol8Debugger8Location7toValueEv
	movq	-264(%rbp), %rax
	leaq	.LC91(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	leaq	-272(%rbp), %rdx
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	cmpq	-312(%rbp), %rdi
	je	.L5813
	call	_ZdlPv@PLT
.L5813:
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5810
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L5810
.L5867:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6281:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl13setBreakpointEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl13setBreakpointEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZNK12v8_inspector8protocol8Debugger9CallFrame7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Debugger9CallFrame7toValueEv
	.type	_ZNK12v8_inspector8protocol8Debugger9CallFrame7toValueEv, @function
_ZNK12v8_inspector8protocol8Debugger9CallFrame7toValueEv:
.LFB6040:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$96, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r15, 0(%r13)
	movl	$56, %edi
	call	_Znwm@PLT
	leaq	8(%rbx), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol11StringValueC1ERKNS_8String16E
	movq	%r12, -104(%rbp)
	leaq	-96(%rbp), %r12
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rdi
	je	.L5870
	call	_ZdlPv@PLT
.L5870:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5871
	movq	(%rdi), %rax
	call	*24(%rax)
.L5871:
	movq	0(%r13), %r8
	movl	$56, %edi
	movq	%r8, -128(%rbp)
	call	_Znwm@PLT
	leaq	48(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN12v8_inspector8protocol11StringValueC1ERKNS_8String16E
	leaq	.LC61(%rip), %rsi
	movq	%r12, %rdi
	movq	%r15, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L5872
	call	_ZdlPv@PLT
.L5872:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5873
	movq	(%rdi), %rax
	call	*24(%rax)
.L5873:
	movq	88(%rbx), %rsi
	movq	0(%r13), %r8
	leaq	-112(%rbp), %r15
	testq	%rsi, %rsi
	je	.L5874
	movq	%r14, %rdi
	movq	%r8, -128(%rbp)
	call	_ZNK12v8_inspector8protocol8Debugger8Location7toValueEv
	movq	-104(%rbp), %rax
	leaq	.LC62(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L5875
	call	_ZdlPv@PLT
.L5875:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5876
	movq	(%rdi), %rax
	call	*24(%rax)
.L5876:
	movq	0(%r13), %r8
.L5874:
	movq	96(%rbx), %rsi
	movq	%r14, %rdi
	movq	%r8, -128(%rbp)
	call	_ZNK12v8_inspector8protocol8Debugger8Location7toValueEv
	movq	-104(%rbp), %rax
	leaq	.LC26(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L5877
	call	_ZdlPv@PLT
.L5877:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5878
	movq	(%rdi), %rax
	call	*24(%rax)
.L5878:
	movq	0(%r13), %r8
	movl	$56, %edi
	movq	%r8, -136(%rbp)
	call	_Znwm@PLT
	leaq	104(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN12v8_inspector8protocol11StringValueC1ERKNS_8String16E
	movq	-128(%rbp), %rax
	leaq	.LC33(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L5879
	call	_ZdlPv@PLT
.L5879:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5880
	movq	(%rdi), %rax
	call	*24(%rax)
.L5880:
	movq	144(%rbx), %rdx
	movq	0(%r13), %rax
	movl	$40, %edi
	movq	%rdx, -136(%rbp)
	movq	%rax, -144(%rbp)
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN12v8_inspector8protocol9ListValueC1Ev@PLT
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rax
	movq	8(%rdx), %rsi
	subq	(%rdx), %rsi
	leaq	16(%rax), %rdi
	sarq	$3, %rsi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	movq	-136(%rbp), %rdx
	movq	8(%rdx), %rcx
	movq	(%rdx), %rax
	movq	%rcx, -136(%rbp)
	cmpq	%rcx, %rax
	je	.L5886
	.p2align 4,,10
	.p2align 3
.L5887:
	movq	(%rax), %rsi
	movq	%r15, %rdi
	movq	%rax, -152(%rbp)
	call	_ZNK12v8_inspector8protocol8Debugger5Scope7toValueEv
	movq	-112(%rbp), %rdx
	movq	-128(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rdx, -104(%rbp)
	call	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE@PLT
	movq	-104(%rbp), %rdi
	movq	-152(%rbp), %rax
	testq	%rdi, %rdi
	je	.L5884
	movq	(%rdi), %rdx
	call	*24(%rdx)
	movq	-152(%rbp), %rax
	addq	$8, %rax
	cmpq	%rax, -136(%rbp)
	jne	.L5887
.L5886:
	movq	-128(%rbp), %rax
	leaq	.LC63(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-144(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L5883
	call	_ZdlPv@PLT
.L5883:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5888
	movq	(%rdi), %rax
	call	*24(%rax)
.L5888:
	movq	0(%r13), %r8
	movq	152(%rbx), %rsi
	movq	%r14, %rdi
	movq	%r8, -128(%rbp)
	call	_ZNK12v8_inspector8protocol7Runtime12RemoteObject7toValueEv@PLT
	movq	-104(%rbp), %rax
	leaq	.LC64(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L5889
	call	_ZdlPv@PLT
.L5889:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5890
	movq	(%rdi), %rax
	call	*24(%rax)
.L5890:
	movq	160(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L5869
	movq	%r14, %rdi
	movq	0(%r13), %rbx
	call	_ZNK12v8_inspector8protocol7Runtime12RemoteObject7toValueEv@PLT
	movq	-104(%rbp), %rax
	leaq	.LC65(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L5892
	call	_ZdlPv@PLT
.L5892:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5869
	movq	(%rdi), %rax
	call	*24(%rax)
.L5869:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5927
	addq	$120, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5884:
	.cfi_restore_state
	addq	$8, %rax
	cmpq	%rax, -136(%rbp)
	jne	.L5887
	jmp	.L5886
.L5927:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6040:
	.size	_ZNK12v8_inspector8protocol8Debugger9CallFrame7toValueEv, .-_ZNK12v8_inspector8protocol8Debugger9CallFrame7toValueEv
	.section	.text._ZN12v8_inspector8protocol8Debugger9CallFrame17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger9CallFrame17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger9CallFrame17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Debugger9CallFrame17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Debugger9CallFrame17serializeToBinaryEv:
.LFB5600:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger9CallFrame7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5928
	movq	(%rdi), %rax
	call	*24(%rax)
.L5928:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5935
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5935:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5600:
	.size	_ZN12v8_inspector8protocol8Debugger9CallFrame17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Debugger9CallFrame17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Debugger9CallFrame15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger9CallFrame15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger9CallFrame15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Debugger9CallFrame15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Debugger9CallFrame15serializeToJSONEv:
.LFB5599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger9CallFrame7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5936
	movq	(%rdi), %rax
	call	*24(%rax)
.L5936:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5943
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5943:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5599:
	.size	_ZN12v8_inspector8protocol8Debugger9CallFrame15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Debugger9CallFrame15serializeToJSONEv
	.section	.text._ZNK12v8_inspector8protocol8Debugger9CallFrame5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Debugger9CallFrame5cloneEv
	.type	_ZNK12v8_inspector8protocol8Debugger9CallFrame5cloneEv, @function
_ZNK12v8_inspector8protocol8Debugger9CallFrame5cloneEv:
.LFB6041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol8Debugger9CallFrame7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol8Debugger9CallFrame9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5945
	movq	(%rdi), %rax
	call	*24(%rax)
.L5945:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5951
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5951:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6041:
	.size	_ZNK12v8_inspector8protocol8Debugger9CallFrame5cloneEv, .-_ZNK12v8_inspector8protocol8Debugger9CallFrame5cloneEv
	.section	.text._ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Debugger9CallFrameESt14default_deleteIS5_EESaIS8_EEE7toValueEPSA_,"axG",@progbits,_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Debugger9CallFrameESt14default_deleteIS5_EESaIS8_EEE7toValueEPSA_,comdat
	.p2align 4
	.weak	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Debugger9CallFrameESt14default_deleteIS5_EESaIS8_EEE7toValueEPSA_
	.type	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Debugger9CallFrameESt14default_deleteIS5_EESaIS8_EEE7toValueEPSA_, @function
_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Debugger9CallFrameESt14default_deleteIS5_EESaIS8_EEE7toValueEPSA_:
.LFB7810:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$40, %edi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %rbx
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol9ListValueC1Ev@PLT
	movq	%rbx, (%r12)
	movq	8(%r13), %rsi
	leaq	16(%rbx), %rdi
	subq	0(%r13), %rsi
	sarq	$3, %rsi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	movq	8(%r13), %rax
	movq	0(%r13), %rbx
	movq	%rax, -88(%rbp)
	cmpq	%rax, %rbx
	je	.L5952
	leaq	-64(%rbp), %r15
	leaq	-72(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L5956:
	movq	(%rbx), %rsi
	movq	(%r12), %r13
	movq	%r15, %rdi
	call	_ZNK12v8_inspector8protocol8Debugger9CallFrame7toValueEv
	movq	-64(%rbp), %rax
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%rax, -72(%rbp)
	call	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5954
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*24(%rax)
	cmpq	%rbx, -88(%rbp)
	jne	.L5956
.L5952:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5959
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5954:
	.cfi_restore_state
	addq	$8, %rbx
	cmpq	%rbx, -88(%rbp)
	jne	.L5956
	jmp	.L5952
.L5959:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7810:
	.size	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Debugger9CallFrameESt14default_deleteIS5_EESaIS8_EEE7toValueEPSA_, .-_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Debugger9CallFrameESt14default_deleteIS5_EESaIS8_EEE7toValueEPSA_
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl12restartFrameEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl12restartFrameEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl12restartFrameEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl12restartFrameEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6220:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$248, %rsp
	movl	%esi, -268(%rbp)
	movq	(%r8), %r14
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -280(%rbp)
	movq	%rcx, -288(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L5961
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	jne	.L5962
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L6049
	call	_ZdlPv@PLT
.L6049:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, -264(%rbp)
	cmpq	%r15, %rdi
	je	.L5964
	call	_ZdlPv@PLT
.L5964:
	leaq	.LC14(%rip), %rsi
	movq	%rbx, %rdi
	leaq	-208(%rbp), %r14
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-264(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L6050
	leaq	-232(%rbp), %rdi
	movq	%r13, %rsi
	movq	$0, -256(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r13), %rsi
	movq	%r14, %rdx
	leaq	-256(%rbp), %rcx
	leaq	-240(%rbp), %r9
	leaq	-248(%rbp), %r8
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*96(%rax)
	cmpl	$2, -112(%rbp)
	je	.L6051
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L6052
.L5979:
	movq	-232(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L6053
	movl	-268(%rbp), %esi
	leaq	-216(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r13, -216(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5969
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L5969
	.p2align 4,,10
	.p2align 3
.L6051:
	movq	8(%r13), %rdi
	movq	-288(%rbp), %rcx
	movq	-280(%rbp), %rdx
	movl	-268(%rbp), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
.L5969:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5981
	call	_ZdlPv@PLT
.L5981:
	movq	-232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5982
	call	_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0
.L5982:
	movq	-240(%rbp), %r12
	testq	%r12, %r12
	je	.L5983
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5984
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r12), %rdi
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm1
	leaq	80(%r12), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	cmpq	%rax, %rdi
	je	.L5985
	call	_ZdlPv@PLT
.L5985:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L5986
	call	_ZdlPv@PLT
.L5986:
	movl	$104, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L5983:
	movq	-248(%rbp), %r12
	testq	%r12, %r12
	je	.L5987
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5988
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L5987:
	movq	-256(%rbp), %r14
	testq	%r14, %r14
	je	.L5967
	movq	8(%r14), %rbx
	movq	(%r14), %r12
	cmpq	%r12, %rbx
	je	.L5990
	leaq	_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev(%rip), %r15
	jmp	.L5993
	.p2align 4,,10
	.p2align 3
.L6055:
	call	_ZN12v8_inspector8protocol8Debugger9CallFrameD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L5991:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L6054
.L5993:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L5991
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%r15, %rax
	je	.L6055
	call	*%rax
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L5993
	.p2align 4,,10
	.p2align 3
.L6054:
	movq	(%r14), %r12
.L5990:
	testq	%r12, %r12
	je	.L5994
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L5994:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L5967:
	movq	-208(%rbp), %rdi
	leaq	-192(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5960
	call	_ZdlPv@PLT
.L5960:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L6056
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5961:
	.cfi_restore_state
	movq	-112(%rbp), %rdi
.L5962:
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L6057
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movq	$0, -264(%rbp)
	jmp	.L5964
	.p2align 4,,10
	.p2align 3
.L6050:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	-268(%rbp), %esi
	movl	$-32602, %edx
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L5967
	call	_ZdlPv@PLT
	jmp	.L5967
	.p2align 4,,10
	.p2align 3
.L6053:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L5969
	.p2align 4,,10
	.p2align 3
.L6057:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movq	$0, -264(%rbp)
	jmp	.L5964
	.p2align 4,,10
	.p2align 3
.L6052:
	movq	-256(%rbp), %rsi
	leaq	-224(%rbp), %rbx
	leaq	-160(%rbp), %r14
	movq	%rbx, %rdi
	leaq	-216(%rbp), %r15
	call	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Debugger9CallFrameESt14default_deleteIS5_EESaIS8_EEE7toValueEPSA_
	movq	-224(%rbp), %rax
	leaq	.LC66(%rip), %rsi
	movq	%r14, %rdi
	movq	$0, -224(%rbp)
	movq	%rax, -216(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	movq	%rax, -264(%rbp)
	cmpq	%rax, %rdi
	je	.L5971
	call	_ZdlPv@PLT
.L5971:
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5972
	movq	(%rdi), %rax
	call	*24(%rax)
.L5972:
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5973
	movq	(%rdi), %rax
	call	*24(%rax)
.L5973:
	movq	-248(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L5974
	movq	%r15, %rdi
	call	_ZNK12v8_inspector8protocol7Runtime10StackTrace7toValueEv@PLT
	movq	-216(%rbp), %rax
	leaq	.LC70(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	cmpq	-264(%rbp), %rdi
	je	.L5975
	call	_ZdlPv@PLT
.L5975:
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5974
	movq	(%rdi), %rax
	call	*24(%rax)
.L5974:
	movq	-240(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L5979
	movq	%r15, %rdi
	call	_ZNK12v8_inspector8protocol7Runtime12StackTraceId7toValueEv@PLT
	movq	-216(%rbp), %rax
	leaq	.LC71(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	cmpq	-264(%rbp), %rdi
	je	.L5977
	call	_ZdlPv@PLT
.L5977:
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5979
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L5979
	.p2align 4,,10
	.p2align 3
.L5984:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L5983
	.p2align 4,,10
	.p2align 3
.L5988:
	call	*%rax
	jmp	.L5987
.L6056:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6220:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl12restartFrameEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl12restartFrameEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImpl15setScriptSourceEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC92:
	.string	"dryRun"
.LC93:
	.string	"stackChanged"
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl15setScriptSourceEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl15setScriptSourceEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl15setScriptSourceEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl15setScriptSourceEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$328, %rsp
	movl	%esi, -332(%rbp)
	movq	(%r8), %r13
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -360(%rbp)
	movq	%rcx, -368(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L6059
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r13
	jne	.L6060
	leaq	-96(%rbp), %rax
	movq	%rax, -328(%rbp)
	cmpq	%rax, %rdi
	je	.L6175
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L6063:
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r15
	cmpq	-328(%rbp), %rdi
	je	.L6065
	call	_ZdlPv@PLT
.L6065:
	leaq	.LC22(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-256(%rbp), %rax
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -352(%rbp)
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC82(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, -344(%rbp)
	cmpq	-328(%rbp), %rdi
	je	.L6066
	call	_ZdlPv@PLT
.L6066:
	leaq	.LC82(%rip), %rsi
	movq	%rbx, %rdi
	leaq	-208(%rbp), %r15
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-344(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC92(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r13
	cmpq	-328(%rbp), %rdi
	je	.L6107
	call	_ZdlPv@PLT
.L6107:
	testq	%r13, %r13
	je	.L6114
	leaq	.LC92(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %rsi
	movl	$1, %r13d
	call	_ZN12v8_inspector8protocol16ValueConversionsIbE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movb	%al, -344(%rbp)
.L6064:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L6176
	xorl	%edx, %edx
	movq	%r14, %rsi
	leaq	-280(%rbp), %rdi
	movq	$0, -312(%rbp)
	movw	%dx, -314(%rbp)
	movq	$0, -304(%rbp)
	movq	$0, -296(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r14), %rsi
	movq	%r12, %rdi
	leaq	-288(%rbp), %rdx
	movzbl	-344(%rbp), %ecx
	leaq	-312(%rbp), %r9
	movq	(%rsi), %rax
	movq	200(%rax), %rax
	pushq	%rdx
	leaq	-296(%rbp), %rdx
	pushq	%rdx
	leaq	-304(%rbp), %rdx
	pushq	%rdx
	leaq	-314(%rbp), %rdx
	pushq	%rdx
	movq	-352(%rbp), %rdx
	movb	%r13b, -264(%rbp)
	leaq	-264(%rbp), %r13
	movb	%cl, -263(%rbp)
	movq	%r13, %r8
	movq	%r15, %rcx
	call	*%rax
	addq	$32, %rsp
	cmpl	$2, -112(%rbp)
	je	.L6177
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	jne	.L6088
	movq	-312(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L6073
	leaq	-272(%rbp), %rdi
	leaq	-160(%rbp), %r15
	call	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Debugger9CallFrameESt14default_deleteIS5_EESaIS8_EEE7toValueEPSA_
	movq	-272(%rbp), %rax
	leaq	.LC66(%rip), %rsi
	movq	%r15, %rdi
	movq	$0, -272(%rbp)
	movq	%rax, -264(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L6074
	call	_ZdlPv@PLT
.L6074:
	movq	-264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6075
	movq	(%rdi), %rax
	call	*24(%rax)
.L6075:
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6073
	movq	(%rdi), %rax
	call	*24(%rax)
.L6073:
	cmpb	$0, -314(%rbp)
	jne	.L6178
.L6077:
	movq	-304(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L6080
	movq	%r13, %rdi
	leaq	-160(%rbp), %r15
	call	_ZNK12v8_inspector8protocol7Runtime10StackTrace7toValueEv@PLT
	movq	-264(%rbp), %rax
	leaq	.LC70(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	leaq	-272(%rbp), %rdx
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L6081
	call	_ZdlPv@PLT
.L6081:
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6080
	movq	(%rdi), %rax
	call	*24(%rax)
.L6080:
	movq	-296(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L6083
	movq	%r13, %rdi
	leaq	-160(%rbp), %r15
	call	_ZNK12v8_inspector8protocol7Runtime12StackTraceId7toValueEv@PLT
	movq	-264(%rbp), %rax
	leaq	.LC71(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	leaq	-272(%rbp), %rdx
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L6084
	call	_ZdlPv@PLT
.L6084:
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6083
	movq	(%rdi), %rax
	call	*24(%rax)
.L6083:
	movq	-288(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L6088
	movq	%r13, %rdi
	leaq	-160(%rbp), %r15
	call	_ZNK12v8_inspector8protocol7Runtime16ExceptionDetails7toValueEv@PLT
	movq	-264(%rbp), %rax
	leaq	.LC56(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	leaq	-272(%rbp), %rdx
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L6086
	call	_ZdlPv@PLT
.L6086:
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6088
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L6088:
	movq	-280(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L6179
	movl	-332(%rbp), %esi
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r14, -264(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE@PLT
	movq	-264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6071
	movq	(%rdi), %rax
	call	*24(%rax)
.L6071:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L6090
	call	_ZdlPv@PLT
.L6090:
	movq	-280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6091
	call	_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0
.L6091:
	movq	-288(%rbp), %r12
	testq	%r12, %r12
	je	.L6092
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L6093
	call	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD1Ev
	movl	$184, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L6092:
	movq	-296(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6094
	movq	(%rdi), %rax
	call	*24(%rax)
.L6094:
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6095
	movq	(%rdi), %rax
	call	*24(%rax)
.L6095:
	movq	-312(%rbp), %r14
	testq	%r14, %r14
	je	.L6069
	movq	8(%r14), %rbx
	movq	(%r14), %r12
	cmpq	%r12, %rbx
	je	.L6097
	leaq	_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev(%rip), %r15
	jmp	.L6100
	.p2align 4,,10
	.p2align 3
.L6181:
	call	_ZN12v8_inspector8protocol8Debugger9CallFrameD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L6098:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L6180
.L6100:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L6098
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%r15, %rax
	je	.L6181
	call	*%rax
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L6100
	.p2align 4,,10
	.p2align 3
.L6180:
	movq	(%r14), %r12
.L6097:
	testq	%r12, %r12
	je	.L6101
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L6101:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L6069:
	movq	-208(%rbp), %rdi
	leaq	-192(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L6102
	call	_ZdlPv@PLT
.L6102:
	movq	-256(%rbp), %rdi
	leaq	-240(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L6058
	call	_ZdlPv@PLT
.L6058:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L6182
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6175:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L6063
	.p2align 4,,10
	.p2align 3
.L6059:
	movq	-112(%rbp), %rdi
.L6060:
	leaq	-96(%rbp), %rax
	movq	%rax, -328(%rbp)
	cmpq	%rax, %rdi
	je	.L6183
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L6062:
	leaq	.LC22(%rip), %rsi
	movq	%rbx, %rdi
	leaq	-208(%rbp), %r15
	xorl	%r13d, %r13d
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-256(%rbp), %rax
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, -352(%rbp)
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC82(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movb	$0, -344(%rbp)
	jmp	.L6064
	.p2align 4,,10
	.p2align 3
.L6176:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	-332(%rbp), %esi
	movl	$-32602, %edx
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	-328(%rbp), %rdi
	je	.L6069
	call	_ZdlPv@PLT
	jmp	.L6069
	.p2align 4,,10
	.p2align 3
.L6179:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L6071
	.p2align 4,,10
	.p2align 3
.L6177:
	movq	8(%r14), %rdi
	movq	-368(%rbp), %rcx
	movq	-360(%rbp), %rdx
	movl	-332(%rbp), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L6071
	.p2align 4,,10
	.p2align 3
.L6183:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L6062
	.p2align 4,,10
	.p2align 3
.L6178:
	movzbl	-313(%rbp), %ebx
	movl	$24, %edi
	leaq	-160(%rbp), %r15
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	leaq	.LC93(%rip), %rsi
	movq	%r15, %rdi
	movl	$1, 8(%rax)
	movq	%rcx, (%rax)
	movb	%bl, 16(%rax)
	movq	%rax, -264(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L6078
	call	_ZdlPv@PLT
.L6078:
	movq	-264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6077
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L6077
	.p2align 4,,10
	.p2align 3
.L6114:
	movb	$0, -344(%rbp)
	xorl	%r13d, %r13d
	jmp	.L6064
	.p2align 4,,10
	.p2align 3
.L6093:
	call	*%rax
	jmp	.L6092
.L6182:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6316:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl15setScriptSourceEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl15setScriptSourceEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZNK12v8_inspector8protocol8Debugger18PausedNotification7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Debugger18PausedNotification7toValueEv
	.type	_ZNK12v8_inspector8protocol8Debugger18PausedNotification7toValueEv, @function
_ZNK12v8_inspector8protocol8Debugger18PausedNotification7toValueEv:
.LFB6063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$96, %edi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r15, 0(%r13)
	leaq	-112(%rbp), %rax
	movq	8(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Debugger9CallFrameESt14default_deleteIS5_EESaIS8_EEE7toValueEPSA_
	movq	-112(%rbp), %rax
	leaq	.LC66(%rip), %rsi
	movq	%r12, %rdi
	movq	$0, -112(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rdi
	je	.L6185
	call	_ZdlPv@PLT
.L6185:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6186
	movq	(%rdi), %rax
	call	*24(%rax)
.L6186:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6187
	movq	(%rdi), %rax
	call	*24(%rax)
.L6187:
	movl	$56, %edi
	movq	0(%r13), %r15
	call	_Znwm@PLT
	leaq	16(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN12v8_inspector8protocol11StringValueC1ERKNS_8String16E
	movq	-128(%rbp), %rax
	leaq	.LC67(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L6188
	call	_ZdlPv@PLT
.L6188:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6189
	movq	(%rdi), %rax
	call	*24(%rax)
.L6189:
	movq	56(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L6190
	movq	(%rsi), %rax
	movq	0(%r13), %r15
	movq	%r14, %rdi
	call	*88(%rax)
	leaq	.LC68(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L6191
	call	_ZdlPv@PLT
.L6191:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6190
	movq	(%rdi), %rax
	call	*24(%rax)
.L6190:
	movq	64(%rbx), %r15
	testq	%r15, %r15
	je	.L6193
	movq	0(%r13), %rax
	movl	$40, %edi
	movq	%rax, -152(%rbp)
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN12v8_inspector8protocol9ListValueC1Ev@PLT
	movq	8(%r15), %rsi
	subq	(%r15), %rsi
	movabsq	$-3689348814741910323, %rax
	sarq	$3, %rsi
	imulq	%rax, %rsi
	movq	-128(%rbp), %rax
	leaq	16(%rax), %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	movq	8(%r15), %rax
	movq	(%r15), %rcx
	movq	%rax, -144(%rbp)
	cmpq	%rax, %rcx
	je	.L6194
	movq	%rbx, -160(%rbp)
	movq	%rcx, %rbx
	.p2align 4,,10
	.p2align 3
.L6197:
	movl	$56, %edi
	call	_Znwm@PLT
	movq	%rax, %r15
	movl	$4, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%r15)
	leaq	32(%r15), %rax
	leaq	16(%r15), %rdi
	movq	%rax, 16(%r15)
	movq	(%rbx), %rsi
	movq	8(%rbx), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	32(%rbx), %rax
	movq	-128(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r15, -104(%rbp)
	movq	%rax, 48(%r15)
	call	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE@PLT
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6195
	movq	(%rdi), %rax
	addq	$40, %rbx
	call	*24(%rax)
	cmpq	%rbx, -144(%rbp)
	jne	.L6197
	movq	-160(%rbp), %rbx
.L6194:
	movq	-128(%rbp), %rax
	leaq	.LC69(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-152(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L6198
	call	_ZdlPv@PLT
.L6198:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6193
	movq	(%rdi), %rax
	call	*24(%rax)
.L6193:
	movq	72(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L6200
	movq	0(%r13), %r8
	movq	%r14, %rdi
	movq	%r8, -128(%rbp)
	call	_ZNK12v8_inspector8protocol7Runtime10StackTrace7toValueEv@PLT
	movq	-104(%rbp), %rax
	leaq	.LC70(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	-136(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L6201
	call	_ZdlPv@PLT
.L6201:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6200
	movq	(%rdi), %rax
	call	*24(%rax)
.L6200:
	movq	80(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L6203
	movq	0(%r13), %r8
	movq	%r14, %rdi
	movq	%r8, -128(%rbp)
	call	_ZNK12v8_inspector8protocol7Runtime12StackTraceId7toValueEv@PLT
	movq	-104(%rbp), %rax
	leaq	.LC71(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	-136(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L6204
	call	_ZdlPv@PLT
.L6204:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6203
	movq	(%rdi), %rax
	call	*24(%rax)
.L6203:
	movq	88(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L6184
	movq	%r14, %rdi
	movq	0(%r13), %rbx
	call	_ZNK12v8_inspector8protocol7Runtime12StackTraceId7toValueEv@PLT
	movq	-104(%rbp), %rax
	leaq	.LC72(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L6207
	call	_ZdlPv@PLT
.L6207:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6184
	movq	(%rdi), %rax
	call	*24(%rax)
.L6184:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L6251
	addq	$120, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6195:
	.cfi_restore_state
	addq	$40, %rbx
	cmpq	%rbx, -144(%rbp)
	jne	.L6197
	movq	-160(%rbp), %rbx
	jmp	.L6194
.L6251:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6063:
	.size	_ZNK12v8_inspector8protocol8Debugger18PausedNotification7toValueEv, .-_ZNK12v8_inspector8protocol8Debugger18PausedNotification7toValueEv
	.section	.text._ZN12v8_inspector8protocol8Debugger18PausedNotification17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger18PausedNotification17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger18PausedNotification17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Debugger18PausedNotification17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Debugger18PausedNotification17serializeToBinaryEv:
.LFB5825:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger18PausedNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6252
	movq	(%rdi), %rax
	call	*24(%rax)
.L6252:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L6259
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L6259:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5825:
	.size	_ZN12v8_inspector8protocol8Debugger18PausedNotification17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Debugger18PausedNotification17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Debugger18PausedNotification15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger18PausedNotification15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger18PausedNotification15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Debugger18PausedNotification15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Debugger18PausedNotification15serializeToJSONEv:
.LFB5824:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger18PausedNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6260
	movq	(%rdi), %rax
	call	*24(%rax)
.L6260:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L6267
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L6267:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5824:
	.size	_ZN12v8_inspector8protocol8Debugger18PausedNotification15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Debugger18PausedNotification15serializeToJSONEv
	.section	.text._ZNK12v8_inspector8protocol8Debugger18PausedNotification5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Debugger18PausedNotification5cloneEv
	.type	_ZNK12v8_inspector8protocol8Debugger18PausedNotification5cloneEv, @function
_ZNK12v8_inspector8protocol8Debugger18PausedNotification5cloneEv:
.LFB6064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol8Debugger18PausedNotification7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol8Debugger18PausedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6269
	movq	(%rdi), %rax
	call	*24(%rax)
.L6269:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L6275
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L6275:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6064:
	.size	_ZNK12v8_inspector8protocol8Debugger18PausedNotification5cloneEv, .-_ZNK12v8_inspector8protocol8Debugger18PausedNotification5cloneEv
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImpl18setBreakpointByUrlEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC94:
	.string	"urlRegex"
.LC95:
	.string	"scriptHash"
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl18setBreakpointByUrlEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl18setBreakpointByUrlEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl18setBreakpointByUrlEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl18setBreakpointByUrlEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$648, %rsp
	movl	%esi, -620(%rbp)
	movq	(%r8), %r13
	leaq	.LC0(%rip), %rsi
	movq	%rdi, -616(%rbp)
	movq	%r12, %rdi
	movq	%rdx, -664(%rbp)
	movq	%rcx, -672(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L6277
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r13
	jne	.L6278
	leaq	-96(%rbp), %r14
	cmpq	%r14, %rdi
	je	.L6391
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L6281:
	leaq	.LC18(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r15
	cmpq	%r14, %rdi
	je	.L6283
	call	_ZdlPv@PLT
.L6283:
	leaq	.LC18(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%r15, %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	.LC33(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, -640(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r15
	cmpq	%r14, %rdi
	je	.L6346
	call	_ZdlPv@PLT
.L6346:
	leaq	-472(%rbp), %rax
	movb	$0, -496(%rbp)
	movq	%rax, -584(%rbp)
	movq	%rax, -488(%rbp)
	xorl	%eax, %eax
	movq	$0, -480(%rbp)
	movw	%ax, -472(%rbp)
	movq	$0, -456(%rbp)
	testq	%r15, %r15
	je	.L6284
	leaq	.LC33(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	-488(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-80(%rbp), %rax
	movq	-112(%rbp), %rdi
	movb	$1, -496(%rbp)
	movq	%rax, -456(%rbp)
	cmpq	%r14, %rdi
	je	.L6284
	call	_ZdlPv@PLT
.L6284:
	leaq	.LC94(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r15
	cmpq	%r14, %rdi
	je	.L6344
	call	_ZdlPv@PLT
.L6344:
	leaq	-424(%rbp), %rax
	movb	$0, -448(%rbp)
	movq	%rax, -592(%rbp)
	movq	%rax, -440(%rbp)
	xorl	%eax, %eax
	movq	$0, -432(%rbp)
	movw	%ax, -424(%rbp)
	movq	$0, -408(%rbp)
	testq	%r15, %r15
	je	.L6286
	leaq	.LC94(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	-440(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-80(%rbp), %rax
	movq	-112(%rbp), %rdi
	movb	$1, -448(%rbp)
	movq	%rax, -408(%rbp)
	cmpq	%r14, %rdi
	je	.L6286
	call	_ZdlPv@PLT
.L6286:
	leaq	.LC95(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r15
	cmpq	%r14, %rdi
	je	.L6342
	call	_ZdlPv@PLT
.L6342:
	leaq	-376(%rbp), %rax
	xorl	%r11d, %r11d
	movb	$0, -400(%rbp)
	movq	%rax, -600(%rbp)
	movq	%rax, -392(%rbp)
	movq	$0, -384(%rbp)
	movw	%r11w, -376(%rbp)
	movq	$0, -360(%rbp)
	testq	%r15, %r15
	je	.L6288
	leaq	.LC95(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	-392(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-80(%rbp), %rax
	movq	-112(%rbp), %rdi
	movb	$1, -400(%rbp)
	movq	%rax, -360(%rbp)
	cmpq	%r14, %rdi
	je	.L6288
	call	_ZdlPv@PLT
.L6288:
	leaq	.LC19(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r15
	cmpq	%r14, %rdi
	je	.L6340
	call	_ZdlPv@PLT
.L6340:
	testq	%r15, %r15
	je	.L6352
	leaq	.LC19(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movl	$1, %r15d
	call	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movl	%eax, -632(%rbp)
.L6290:
	leaq	.LC85(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r13
	cmpq	%r14, %rdi
	je	.L6338
	call	_ZdlPv@PLT
.L6338:
	leaq	-328(%rbp), %rax
	xorl	%r10d, %r10d
	movb	$0, -352(%rbp)
	movq	%rax, -608(%rbp)
	movq	%rax, -344(%rbp)
	movq	$0, -336(%rbp)
	movw	%r10w, -328(%rbp)
	movq	$0, -312(%rbp)
	testq	%r13, %r13
	je	.L6282
	leaq	.LC85(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsINS_8String16EE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	leaq	-344(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-80(%rbp), %rax
	movq	-112(%rbp), %rdi
	movb	$1, -352(%rbp)
	movq	%rax, -312(%rbp)
	cmpq	%r14, %rdi
	je	.L6282
	call	_ZdlPv@PLT
.L6282:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L6392
	movq	-616(%rbp), %rbx
	leaq	-528(%rbp), %rax
	xorl	%r9d, %r9d
	leaq	-568(%rbp), %rdi
	movq	%rax, -656(%rbp)
	movq	%rbx, %rsi
	movq	%rax, -544(%rbp)
	movq	$0, -536(%rbp)
	movw	%r9w, -528(%rbp)
	movq	$0, -512(%rbp)
	movq	$0, -576(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	movq	(%rsi), %rax
	movq	160(%rax), %r10
	movzbl	-352(%rbp), %eax
	movb	%al, -160(%rbp)
	leaq	-136(%rbp), %rax
	movq	%rax, -648(%rbp)
	movq	%rax, -152(%rbp)
	movq	-344(%rbp), %rax
	cmpq	-608(%rbp), %rax
	je	.L6393
	movq	%rax, -152(%rbp)
	movq	-328(%rbp), %rax
	movq	%rax, -136(%rbp)
.L6296:
	movq	-336(%rbp), %rax
	xorl	%r8d, %r8d
	leaq	-184(%rbp), %r14
	movq	$0, -336(%rbp)
	movw	%r8w, -328(%rbp)
	movq	%rax, -144(%rbp)
	movq	-608(%rbp), %rax
	movb	%r15b, -552(%rbp)
	movq	%rax, -344(%rbp)
	movq	-312(%rbp), %rax
	movq	%r14, -200(%rbp)
	movq	%rax, -120(%rbp)
	movl	-632(%rbp), %eax
	movl	%eax, -548(%rbp)
	movzbl	-400(%rbp), %eax
	movb	%al, -208(%rbp)
	movq	-392(%rbp), %rax
	cmpq	-600(%rbp), %rax
	je	.L6394
	movq	%rax, -200(%rbp)
	movq	-376(%rbp), %rax
	movq	%rax, -184(%rbp)
.L6298:
	movq	-384(%rbp), %rax
	xorl	%edi, %edi
	leaq	-232(%rbp), %rbx
	movq	$0, -384(%rbp)
	movw	%di, -376(%rbp)
	movq	%rax, -192(%rbp)
	movq	-600(%rbp), %rax
	movq	%rbx, -248(%rbp)
	movq	%rax, -392(%rbp)
	movq	-360(%rbp), %rax
	movq	%rax, -168(%rbp)
	movzbl	-448(%rbp), %eax
	movb	%al, -256(%rbp)
	movq	-440(%rbp), %rax
	cmpq	-592(%rbp), %rax
	je	.L6395
	movq	%rax, -248(%rbp)
	movq	-424(%rbp), %rax
	movq	%rax, -232(%rbp)
.L6300:
	movq	-432(%rbp), %rax
	xorl	%ecx, %ecx
	movq	-488(%rbp), %rdx
	movq	$0, -432(%rbp)
	movw	%cx, -424(%rbp)
	movq	%rax, -240(%rbp)
	movq	-592(%rbp), %rax
	movq	%rax, -440(%rbp)
	movq	-408(%rbp), %rax
	movq	%rax, -216(%rbp)
	movzbl	-496(%rbp), %eax
	movb	%al, -304(%rbp)
	leaq	-280(%rbp), %rax
	movq	%rax, -296(%rbp)
	cmpq	-584(%rbp), %rdx
	je	.L6396
	movq	%rdx, -296(%rbp)
	movq	-472(%rbp), %rdx
	movq	%rdx, -280(%rbp)
.L6302:
	movq	-480(%rbp), %rdx
	movq	%rax, -632(%rbp)
	movq	%r12, %rdi
	leaq	-544(%rbp), %r15
	movq	-584(%rbp), %rax
	leaq	-552(%rbp), %r13
	movq	$0, -480(%rbp)
	leaq	-304(%rbp), %rcx
	movq	%rdx, -288(%rbp)
	xorl	%edx, %edx
	leaq	-208(%rbp), %r9
	leaq	-256(%rbp), %r8
	movw	%dx, -472(%rbp)
	movq	-456(%rbp), %rdx
	movq	%rax, -488(%rbp)
	leaq	-160(%rbp), %rax
	movq	%rdx, -264(%rbp)
	leaq	-576(%rbp), %rdx
	pushq	%rdx
	movl	-640(%rbp), %edx
	pushq	%r15
	pushq	%rax
	pushq	%r13
	movq	%rax, -680(%rbp)
	call	*%r10
	movq	-296(%rbp), %rdi
	movq	-632(%rbp), %rax
	addq	$32, %rsp
	cmpq	%rax, %rdi
	je	.L6303
	call	_ZdlPv@PLT
.L6303:
	movq	-248(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6304
	call	_ZdlPv@PLT
.L6304:
	movq	-200(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L6305
	call	_ZdlPv@PLT
.L6305:
	movq	-152(%rbp), %rdi
	cmpq	-648(%rbp), %rdi
	je	.L6306
	call	_ZdlPv@PLT
.L6306:
	cmpl	$2, -112(%rbp)
	je	.L6397
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L6398
.L6309:
	movq	-568(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L6399
	movl	-620(%rbp), %esi
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r14, -552(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE@PLT
	movq	-552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6308
	movq	(%rdi), %rax
	call	*24(%rax)
.L6308:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L6321
	call	_ZdlPv@PLT
.L6321:
	movq	-568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6322
	call	_ZNKSt14default_deleteIN12v8_inspector8protocol14DispatcherBase7WeakPtrEEclEPS3_.isra.0
.L6322:
	movq	-576(%rbp), %r13
	testq	%r13, %r13
	je	.L6323
	movq	8(%r13), %rbx
	movq	0(%r13), %r12
	cmpq	%r12, %rbx
	je	.L6324
	.p2align 4,,10
	.p2align 3
.L6328:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L6325
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*24(%rax)
	cmpq	%r12, %rbx
	jne	.L6328
.L6326:
	movq	0(%r13), %r12
.L6324:
	testq	%r12, %r12
	je	.L6329
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L6329:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L6323:
	movq	-544(%rbp), %rdi
	cmpq	-656(%rbp), %rdi
	je	.L6294
.L6390:
	call	_ZdlPv@PLT
.L6294:
	movq	-344(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	je	.L6331
	call	_ZdlPv@PLT
.L6331:
	movq	-392(%rbp), %rdi
	cmpq	-600(%rbp), %rdi
	je	.L6332
	call	_ZdlPv@PLT
.L6332:
	movq	-440(%rbp), %rdi
	cmpq	-592(%rbp), %rdi
	je	.L6333
	call	_ZdlPv@PLT
.L6333:
	movq	-488(%rbp), %rdi
	cmpq	-584(%rbp), %rdi
	je	.L6276
	call	_ZdlPv@PLT
.L6276:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L6400
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6391:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L6281
	.p2align 4,,10
	.p2align 3
.L6325:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L6328
	jmp	.L6326
	.p2align 4,,10
	.p2align 3
.L6277:
	movq	-112(%rbp), %rdi
.L6278:
	leaq	-96(%rbp), %r14
	cmpq	%r14, %rdi
	je	.L6401
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L6280:
	leaq	.LC18(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%r15d, %r15d
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%rbx, %rsi
	xorl	%edi, %edi
	call	_ZN12v8_inspector8protocol16ValueConversionsIiE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	xorl	%edx, %edx
	movb	$0, -496(%rbp)
	movl	%eax, -640(%rbp)
	leaq	-472(%rbp), %rax
	movq	%rax, -584(%rbp)
	movq	%rax, -488(%rbp)
	xorl	%eax, %eax
	movw	%ax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, -592(%rbp)
	movq	%rax, -440(%rbp)
	xorl	%eax, %eax
	movw	%ax, -424(%rbp)
	leaq	-376(%rbp), %rax
	movq	%rax, -600(%rbp)
	movq	%rax, -392(%rbp)
	xorl	%eax, %eax
	movw	%ax, -376(%rbp)
	leaq	-328(%rbp), %rax
	movq	$0, -480(%rbp)
	movq	$0, -456(%rbp)
	movb	$0, -448(%rbp)
	movq	$0, -432(%rbp)
	movq	$0, -408(%rbp)
	movb	$0, -400(%rbp)
	movq	$0, -384(%rbp)
	movq	$0, -360(%rbp)
	movb	$0, -352(%rbp)
	movq	%rax, -608(%rbp)
	movq	%rax, -344(%rbp)
	movq	$0, -336(%rbp)
	movw	%dx, -328(%rbp)
	movq	$0, -312(%rbp)
	movl	$0, -632(%rbp)
	jmp	.L6282
	.p2align 4,,10
	.p2align 3
.L6392:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	$-32602, %edx
	movq	-616(%rbp), %rdi
	movl	-620(%rbp), %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L6390
	jmp	.L6294
	.p2align 4,,10
	.p2align 3
.L6399:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L6308
	.p2align 4,,10
	.p2align 3
.L6397:
	movq	-616(%rbp), %rax
	movq	-672(%rbp), %rcx
	movq	-664(%rbp), %rdx
	movl	-620(%rbp), %esi
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L6308
	.p2align 4,,10
	.p2align 3
.L6401:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L6280
	.p2align 4,,10
	.p2align 3
.L6393:
	movdqu	-328(%rbp), %xmm0
	movups	%xmm0, -136(%rbp)
	jmp	.L6296
	.p2align 4,,10
	.p2align 3
.L6396:
	movdqu	-472(%rbp), %xmm3
	movups	%xmm3, -280(%rbp)
	jmp	.L6302
	.p2align 4,,10
	.p2align 3
.L6395:
	movdqu	-424(%rbp), %xmm2
	movups	%xmm2, -232(%rbp)
	jmp	.L6300
	.p2align 4,,10
	.p2align 3
.L6394:
	movdqu	-376(%rbp), %xmm1
	movups	%xmm1, -184(%rbp)
	jmp	.L6298
	.p2align 4,,10
	.p2align 3
.L6398:
	movl	$56, %edi
	call	_Znwm@PLT
	movq	%r15, %rsi
	movq	%rax, %rbx
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol11StringValueC1ERKNS_8String16E
	movq	%rbx, -552(%rbp)
	movq	-680(%rbp), %rbx
	leaq	.LC10(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	movq	%rax, -640(%rbp)
	cmpq	%rax, %rdi
	je	.L6310
	call	_ZdlPv@PLT
.L6310:
	movq	-552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6311
	movq	(%rdi), %rax
	call	*24(%rax)
.L6311:
	movl	$40, %edi
	movq	-576(%rbp), %r15
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8protocol9ListValueC1Ev@PLT
	movq	8(%r15), %rsi
	subq	(%r15), %rsi
	leaq	16(%rbx), %rdi
	sarq	$3, %rsi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	movq	8(%r15), %rax
	movq	(%r15), %r15
	leaq	-560(%rbp), %rcx
	movq	%rcx, -632(%rbp)
	movq	%rax, -616(%rbp)
	cmpq	%rax, %r15
	je	.L6317
	.p2align 4,,10
	.p2align 3
.L6318:
	movq	(%r15), %rsi
	movq	-632(%rbp), %rdi
	call	_ZNK12v8_inspector8protocol8Debugger8Location7toValueEv
	movq	-560(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	%rdx, -552(%rbp)
	call	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE@PLT
	movq	-552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6315
	movq	(%rdi), %rdx
	addq	$8, %r15
	call	*24(%rdx)
	cmpq	%r15, -616(%rbp)
	jne	.L6318
.L6317:
	movq	%rbx, -552(%rbp)
	movq	-680(%rbp), %rbx
	leaq	.LC79(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	cmpq	-640(%rbp), %rdi
	je	.L6314
	call	_ZdlPv@PLT
.L6314:
	movq	-552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6309
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L6309
	.p2align 4,,10
	.p2align 3
.L6315:
	addq	$8, %r15
	cmpq	%r15, -616(%rbp)
	jne	.L6318
	jmp	.L6317
	.p2align 4,,10
	.p2align 3
.L6352:
	movl	$0, -632(%rbp)
	xorl	%r15d, %r15d
	jmp	.L6290
.L6400:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6283:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl18setBreakpointByUrlEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl18setBreakpointByUrlEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol8Debugger14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m,"axG",@progbits,_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol8Debugger14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol8Debugger14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m
	.type	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol8Debugger14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m, @function
_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol8Debugger14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m:
.LFB11863:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax,%rsi,8), %r10
	testq	%r10, %r10
	je	.L6419
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%r10), %rdx
	movl	$2147483648, %ebx
	movq	64(%rdx), %r8
	cmpq	%r8, %rcx
	je	.L6422
	.p2align 4,,10
	.p2align 3
.L6404:
	movq	(%rdx), %r9
	testq	%r9, %r9
	je	.L6409
	movq	64(%r9), %r8
	movq	%rdx, %r10
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	8(%rdi)
	cmpq	%rdx, %rsi
	jne	.L6409
	movq	%r9, %rdx
	cmpq	%r8, %rcx
	jne	.L6404
.L6422:
	movq	8(%r11), %r8
	movq	16(%rdx), %r15
	movq	8(%rdx), %r13
	movq	(%r11), %r14
	cmpq	%r15, %r8
	movq	%r15, %r9
	cmovbe	%r8, %r9
	testq	%r9, %r9
	je	.L6405
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L6406:
	movzwl	0(%r13,%rax,2), %r12d
	cmpw	%r12w, (%r14,%rax,2)
	jne	.L6404
	addq	$1, %rax
	cmpq	%rax, %r9
	jne	.L6406
.L6405:
	subq	%r15, %r8
	cmpq	%rbx, %r8
	jge	.L6404
	movabsq	$-2147483649, %rax
	cmpq	%rax, %r8
	jle	.L6404
	testl	%r8d, %r8d
	jne	.L6404
	popq	%rbx
	movq	%r10, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6409:
	.cfi_restore_state
	xorl	%r10d, %r10d
	popq	%rbx
	popq	%r12
	movq	%r10, %rax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6419:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movq	%r10, %rax
	ret
	.cfi_endproc
.LFE11863:
	.size	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol8Debugger14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m, .-_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol8Debugger14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m
	.section	.text.unlikely._ZN12v8_inspector8protocol8Debugger14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE,"ax",@progbits
	.align 2
.LCOLDB96:
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE,"ax",@progbits
.LHOTB96:
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE:
.LFB6143:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$72, %rdi
	subq	$104, %rsp
	movl	%esi, -132(%rbp)
	movq	32(%rdx), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	jne	.L6424
	movq	(%rdx), %rax
	movq	8(%rdx), %rdx
	leaq	(%rax,%rdx,2), %rsi
	cmpq	%rsi, %rax
	je	.L6427
	.p2align 4,,10
	.p2align 3
.L6426:
	movq	%rcx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rcx, %rdx
	movsbq	-2(%rax), %rcx
	addq	%rdx, %rcx
	movq	%rcx, 32(%r14)
	cmpq	%rax, %rsi
	jne	.L6426
	testq	%rcx, %rcx
	je	.L6427
.L6424:
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	80(%rbx)
	movq	%rdx, %rsi
	movq	%r14, %rdx
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol8Debugger14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m
	testq	%rax, %rax
	je	.L6428
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	movq	%rdx, -144(%rbp)
	je	.L6428
	leaq	-112(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	movq	-144(%rbp), %rdx
	movq	48(%rdx), %rax
	addq	56(%rdx), %rbx
	movq	%rbx, %rdi
	testb	$1, %al
	jne	.L6451
.L6429:
	movq	(%r15), %rdx
	movl	-132(%rbp), %esi
	movq	%r12, %r9
	leaq	-120(%rbp), %r8
	movq	$0, (%r15)
	movq	%r13, %rcx
	movq	%rdx, -120(%rbp)
	movq	%r14, %rdx
	call	*%rax
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6430
	movq	(%rdi), %rax
	call	*24(%rax)
.L6430:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L6452
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6451:
	.cfi_restore_state
	movq	(%rbx), %rdx
	movq	-1(%rdx,%rax), %rax
	jmp	.L6429
	.p2align 4,,10
	.p2align 3
.L6427:
	movq	$1, 32(%r14)
	movl	$1, %ecx
	jmp	.L6424
.L6452:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN12v8_inspector8protocol8Debugger14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.cfi_startproc
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE.cold, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE.cold:
.LFSB6143:
.L6428:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	-112(%rbp), %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	movq	48, %rax
	ud2
	.cfi_endproc
.LFE6143:
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.section	.text.unlikely._ZN12v8_inspector8protocol8Debugger14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE.cold, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE.cold
.LCOLDE96:
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
.LHOTE96:
	.section	.text._ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_,"axG",@progbits,_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	.type	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_, @function
_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_:
.LFB9167:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	32(%rsi), %r12
	testq	%r12, %r12
	jne	.L6454
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %rcx
	cmpq	%rcx, %rax
	je	.L6457
	.p2align 4,,10
	.p2align 3
.L6456:
	movq	%r12, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%r12, %rdx
	movq	%rdx, %r12
	movsbq	-2(%rax), %rdx
	addq	%rdx, %r12
	movq	%r12, 32(%rbx)
	cmpq	%rax, %rcx
	jne	.L6456
	testq	%r12, %r12
	je	.L6457
.L6454:
	xorl	%edx, %edx
	movq	%r12, %rax
	movq	%r12, %rcx
	movq	%r13, %rdi
	divq	8(%r13)
	movq	%rdx, %r15
	movq	%rbx, %rdx
	movq	%r15, %rsi
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol8Debugger14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m
	testq	%rax, %rax
	je	.L6458
	movq	(%rax), %rdx
	leaq	48(%rdx), %rax
	testq	%rdx, %rdx
	je	.L6458
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6457:
	.cfi_restore_state
	movq	$1, 32(%rbx)
	movl	$1, %r12d
	jmp	.L6454
	.p2align 4,,10
	.p2align 3
.L6458:
	movl	$72, %edi
	call	_Znwm@PLT
	movq	(%rbx), %rdx
	movq	$0, (%rax)
	movq	%rax, %r14
	leaq	24(%rax), %rax
	movq	%rax, 8(%r14)
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdx
	je	.L6495
	movq	%rdx, 8(%r14)
	movq	16(%rbx), %rdx
	movq	%rdx, 24(%r14)
.L6461:
	movq	%rax, (%rbx)
	movq	8(%rbx), %rdx
	xorl	%eax, %eax
	leaq	32(%r13), %rdi
	movw	%ax, 16(%rbx)
	movq	32(%rbx), %rax
	movl	$1, %ecx
	movq	$0, 8(%rbx)
	movq	8(%r13), %rsi
	movq	%rdx, 16(%r14)
	movq	24(%r13), %rdx
	movq	%rax, 40(%r14)
	movq	$0, 48(%r14)
	movq	$0, 56(%r14)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %rbx
	testb	%al, %al
	jne	.L6462
	movq	0(%r13), %r8
.L6463:
	salq	$3, %r15
	movq	%r12, 64(%r14)
	leaq	(%r8,%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L6472
	movq	(%rdx), %rdx
	movq	%rdx, (%r14)
	movq	(%rax), %rax
	movq	%r14, (%rax)
.L6473:
	addq	$1, 24(%r13)
	addq	$24, %rsp
	leaq	48(%r14), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6462:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L6496
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L6497
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%r13), %r10
	movq	%rax, %r8
.L6465:
	movq	16(%r13), %rsi
	movq	$0, 16(%r13)
	testq	%rsi, %rsi
	je	.L6467
	xorl	%edi, %edi
	leaq	16(%r13), %r9
	jmp	.L6468
	.p2align 4,,10
	.p2align 3
.L6469:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L6470:
	testq	%rsi, %rsi
	je	.L6467
.L6468:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	64(%rcx), %rax
	divq	%rbx
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L6469
	movq	16(%r13), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r13)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L6476
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L6468
	.p2align 4,,10
	.p2align 3
.L6467:
	movq	0(%r13), %rdi
	cmpq	%rdi, %r10
	je	.L6471
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L6471:
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%rbx, 8(%r13)
	divq	%rbx
	movq	%r8, 0(%r13)
	movq	%rdx, %r15
	jmp	.L6463
	.p2align 4,,10
	.p2align 3
.L6495:
	movdqu	16(%rbx), %xmm0
	movups	%xmm0, 24(%r14)
	jmp	.L6461
	.p2align 4,,10
	.p2align 3
.L6472:
	movq	16(%r13), %rdx
	movq	%r14, 16(%r13)
	movq	%rdx, (%r14)
	testq	%rdx, %rdx
	je	.L6474
	movq	64(%rdx), %rax
	xorl	%edx, %edx
	divq	8(%r13)
	movq	%r14, (%r8,%rdx,8)
	movq	0(%r13), %rax
	addq	%r15, %rax
.L6474:
	leaq	16(%r13), %rdx
	movq	%rdx, (%rax)
	jmp	.L6473
	.p2align 4,,10
	.p2align 3
.L6476:
	movq	%rdx, %rdi
	jmp	.L6470
.L6496:
	leaq	48(%r13), %r8
	movq	$0, 48(%r13)
	movq	%r8, %r10
	jmp	.L6465
.L6497:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE9167:
	.size	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_, .-_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImplC2EPNS0_15FrontendChannelEPNS1_7BackendE.str1.1,"aMS",@progbits,1
.LC98:
	.string	"Debugger.continueToLocation"
.LC99:
	.string	"Debugger.disable"
.LC100:
	.string	"Debugger.enable"
.LC101:
	.string	"Debugger.evaluateOnCallFrame"
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImplC2EPNS0_15FrontendChannelEPNS1_7BackendE.str1.8,"aMS",@progbits,1
	.align 8
.LC102:
	.string	"Debugger.getPossibleBreakpoints"
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImplC2EPNS0_15FrontendChannelEPNS1_7BackendE.str1.1
.LC103:
	.string	"Debugger.getScriptSource"
.LC104:
	.string	"Debugger.getStackTrace"
.LC105:
	.string	"Debugger.pause"
.LC106:
	.string	"Debugger.pauseOnAsyncCall"
.LC107:
	.string	"Debugger.removeBreakpoint"
.LC108:
	.string	"Debugger.restartFrame"
.LC109:
	.string	"Debugger.resume"
.LC110:
	.string	"Debugger.searchInContent"
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImplC2EPNS0_15FrontendChannelEPNS1_7BackendE.str1.8
	.align 8
.LC111:
	.string	"Debugger.setAsyncCallStackDepth"
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImplC2EPNS0_15FrontendChannelEPNS1_7BackendE.str1.1
.LC112:
	.string	"Debugger.setBlackboxPatterns"
.LC113:
	.string	"Debugger.setBlackboxedRanges"
.LC114:
	.string	"Debugger.setBreakpoint"
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImplC2EPNS0_15FrontendChannelEPNS1_7BackendE.str1.8
	.align 8
.LC115:
	.string	"Debugger.setInstrumentationBreakpoint"
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImplC2EPNS0_15FrontendChannelEPNS1_7BackendE.str1.1
.LC116:
	.string	"Debugger.setBreakpointByUrl"
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImplC2EPNS0_15FrontendChannelEPNS1_7BackendE.str1.8
	.align 8
.LC117:
	.string	"Debugger.setBreakpointOnFunctionCall"
	.section	.rodata._ZN12v8_inspector8protocol8Debugger14DispatcherImplC2EPNS0_15FrontendChannelEPNS1_7BackendE.str1.1
.LC118:
	.string	"Debugger.setBreakpointsActive"
.LC119:
	.string	"Debugger.setPauseOnExceptions"
.LC120:
	.string	"Debugger.setReturnValue"
.LC121:
	.string	"Debugger.setScriptSource"
.LC122:
	.string	"Debugger.setSkipAllPauses"
.LC123:
	.string	"Debugger.setVariableValue"
.LC124:
	.string	"Debugger.stepInto"
.LC125:
	.string	"Debugger.stepOut"
.LC126:
	.string	"Debugger.stepOver"
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImplC2EPNS0_15FrontendChannelEPNS1_7BackendE,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger14DispatcherImplC5EPNS0_15FrontendChannelEPNS1_7BackendE,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger14DispatcherImplC2EPNS0_15FrontendChannelEPNS1_7BackendE
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImplC2EPNS0_15FrontendChannelEPNS1_7BackendE, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImplC2EPNS0_15FrontendChannelEPNS1_7BackendE:
.LFB6135:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	72(%rbx), %r13
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol14DispatcherBaseC2EPNS0_15FrontendChannelE@PLT
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger14DispatcherImplE(%rip), %rax
	movss	.LC97(%rip), %xmm0
	movq	%r12, 184(%rbx)
	movq	%rax, (%rbx)
	leaq	120(%rbx), %rax
	leaq	-80(%rbp), %r12
	movq	%rax, 72(%rbx)
	leaq	176(%rbx), %rax
	movq	%r12, %rdi
	leaq	.LC98(%rip), %rsi
	movq	%rax, 128(%rbx)
	movq	$1, 80(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 112(%rbx)
	movq	$0, 120(%rbx)
	movq	$1, 136(%rbx)
	movq	$0, 144(%rbx)
	movq	$0, 152(%rbx)
	movq	$0, 168(%rbx)
	movq	$0, 176(%rbx)
	movss	%xmm0, 104(%rbx)
	movss	%xmm0, 160(%rbx)
	leaq	-64(%rbp), %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl18continueToLocationEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6499
	call	_ZdlPv@PLT
.L6499:
	leaq	.LC99(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6500
	call	_ZdlPv@PLT
.L6500:
	leaq	.LC100(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rdx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6501
	call	_ZdlPv@PLT
.L6501:
	leaq	.LC101(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl19evaluateOnCallFrameEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6502
	call	_ZdlPv@PLT
.L6502:
	leaq	.LC102(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl22getPossibleBreakpointsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rdx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6503
	call	_ZdlPv@PLT
.L6503:
	leaq	.LC103(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl15getScriptSourceEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6504
	call	_ZdlPv@PLT
.L6504:
	leaq	.LC104(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl13getStackTraceEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rdx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6505
	call	_ZdlPv@PLT
.L6505:
	leaq	.LC105(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl5pauseEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6506
	call	_ZdlPv@PLT
.L6506:
	leaq	.LC106(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl16pauseOnAsyncCallEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rdx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6507
	call	_ZdlPv@PLT
.L6507:
	leaq	.LC107(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl16removeBreakpointEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6508
	call	_ZdlPv@PLT
.L6508:
	leaq	.LC108(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl12restartFrameEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rdx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6509
	call	_ZdlPv@PLT
.L6509:
	leaq	.LC109(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl6resumeEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6510
	call	_ZdlPv@PLT
.L6510:
	leaq	.LC110(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl15searchInContentEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rdx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6511
	call	_ZdlPv@PLT
.L6511:
	leaq	.LC111(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl22setAsyncCallStackDepthEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6512
	call	_ZdlPv@PLT
.L6512:
	leaq	.LC112(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl19setBlackboxPatternsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rdx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6513
	call	_ZdlPv@PLT
.L6513:
	leaq	.LC113(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl19setBlackboxedRangesEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6514
	call	_ZdlPv@PLT
.L6514:
	leaq	.LC114(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl13setBreakpointEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rdx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6515
	call	_ZdlPv@PLT
.L6515:
	leaq	.LC115(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl28setInstrumentationBreakpointEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6516
	call	_ZdlPv@PLT
.L6516:
	leaq	.LC116(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl18setBreakpointByUrlEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rdx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6517
	call	_ZdlPv@PLT
.L6517:
	leaq	.LC117(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl27setBreakpointOnFunctionCallEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6518
	call	_ZdlPv@PLT
.L6518:
	leaq	.LC118(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl20setBreakpointsActiveEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rdx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6519
	call	_ZdlPv@PLT
.L6519:
	leaq	.LC119(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl20setPauseOnExceptionsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6520
	call	_ZdlPv@PLT
.L6520:
	leaq	.LC120(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl14setReturnValueEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rdx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6521
	call	_ZdlPv@PLT
.L6521:
	leaq	.LC121(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl15setScriptSourceEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6522
	call	_ZdlPv@PLT
.L6522:
	leaq	.LC122(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl16setSkipAllPausesEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rdx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6523
	call	_ZdlPv@PLT
.L6523:
	leaq	.LC123(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl16setVariableValueEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6524
	call	_ZdlPv@PLT
.L6524:
	leaq	.LC124(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl8stepIntoEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rdx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6525
	call	_ZdlPv@PLT
.L6525:
	leaq	.LC125(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl7stepOutEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6526
	call	_ZdlPv@PLT
.L6526:
	leaq	.LC126(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Debugger14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl8stepOverEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rdx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6498
	call	_ZdlPv@PLT
.L6498:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L6530
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L6530:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6135:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImplC2EPNS0_15FrontendChannelEPNS1_7BackendE, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImplC2EPNS0_15FrontendChannelEPNS1_7BackendE
	.weak	_ZN12v8_inspector8protocol8Debugger14DispatcherImplC1EPNS0_15FrontendChannelEPNS1_7BackendE
	.set	_ZN12v8_inspector8protocol8Debugger14DispatcherImplC1EPNS0_15FrontendChannelEPNS1_7BackendE,_ZN12v8_inspector8protocol8Debugger14DispatcherImplC2EPNS0_15FrontendChannelEPNS1_7BackendE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE.str1.1,"aMS",@progbits,1
.LC127:
	.string	"Debugger"
	.section	.text._ZN12v8_inspector8protocol8Debugger10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE
	.type	_ZN12v8_inspector8protocol8Debugger10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE, @function
_ZN12v8_inspector8protocol8Debugger10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE:
.LFB6328:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$64, %rsp
	movq	8(%rdi), %r15
	movl	$192, %edi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol8Debugger14DispatcherImplC1EPNS0_15FrontendChannelEPNS1_7BackendE
	leaq	128(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14UberDispatcher14setupRedirectsERKSt13unordered_mapINS_8String16ES3_St4hashIS3_ESt8equal_toIS3_ESaISt4pairIKS3_S3_EEE@PLT
	movq	%r13, -88(%rbp)
	leaq	-80(%rbp), %r13
	leaq	.LC127(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	leaq	-88(%rbp), %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol14UberDispatcher15registerBackendERKNS_8String16ESt10unique_ptrINS0_14DispatcherBaseESt14default_deleteIS6_EE@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L6532
	call	_ZdlPv@PLT
.L6532:
	movq	-88(%rbp), %r12
	testq	%r12, %r12
	je	.L6531
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger14DispatcherImplD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L6534
	movq	144(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger14DispatcherImplE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	jne	.L6539
	.p2align 4,,10
	.p2align 3
.L6535:
	movq	136(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%r12), %rdi
	leaq	176(%r12), %rax
	movq	$0, 152(%r12)
	movq	$0, 144(%r12)
	cmpq	%rax, %rdi
	je	.L6540
	call	_ZdlPv@PLT
.L6540:
	movq	88(%r12), %r13
	testq	%r13, %r13
	jne	.L6544
	.p2align 4,,10
	.p2align 3
.L6541:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L6545
	call	_ZdlPv@PLT
.L6545:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBaseD2Ev@PLT
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L6531:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L6569
	addq	$64, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6570:
	.cfi_restore_state
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L6535
.L6538:
	movq	%r14, %r13
.L6539:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	movq	0(%r13), %r14
	cmpq	%rax, %rdi
	je	.L6536
	call	_ZdlPv@PLT
.L6536:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L6570
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L6538
	jmp	.L6535
	.p2align 4,,10
	.p2align 3
.L6571:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L6541
.L6543:
	movq	%r14, %r13
.L6544:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	movq	0(%r13), %r14
	cmpq	%rax, %rdi
	jne	.L6571
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L6543
	jmp	.L6541
	.p2align 4,,10
	.p2align 3
.L6534:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L6531
.L6569:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6328:
	.size	_ZN12v8_inspector8protocol8Debugger10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE, .-_ZN12v8_inspector8protocol8Debugger10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE
	.section	.text._ZN12v8_inspector8protocol8Debugger14DispatcherImpl11canDispatchERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl11canDispatchERKNS_8String16E
	.type	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl11canDispatchERKNS_8String16E, @function
_ZN12v8_inspector8protocol8Debugger14DispatcherImpl11canDispatchERKNS_8String16E:
.LFB6142:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	32(%rsi), %rcx
	movq	%rsi, %r8
	leaq	72(%rdi), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rcx, %rcx
	jne	.L6573
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %rsi
	cmpq	%rsi, %rax
	je	.L6576
	.p2align 4,,10
	.p2align 3
.L6575:
	movq	%rcx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rcx, %rdx
	movsbq	-2(%rax), %rcx
	addq	%rdx, %rcx
	movq	%rcx, 32(%r8)
	cmpq	%rax, %rsi
	jne	.L6575
	testq	%rcx, %rcx
	je	.L6576
.L6573:
	movq	%rcx, %rax
	xorl	%edx, %edx
	divq	80(%rdi)
	movq	%r9, %rdi
	movq	%rdx, %rsi
	movq	%r8, %rdx
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol8Debugger14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L6572
	cmpq	$0, (%rax)
	setne	%r8b
.L6572:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6576:
	.cfi_restore_state
	movq	$1, 32(%r8)
	movl	$1, %ecx
	jmp	.L6573
	.cfi_endproc
.LFE6142:
	.size	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl11canDispatchERKNS_8String16E, .-_ZN12v8_inspector8protocol8Debugger14DispatcherImpl11canDispatchERKNS_8String16E
	.weak	_ZTVN12v8_inspector8protocol23InternalRawNotificationE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol23InternalRawNotificationE,"awG",@progbits,_ZTVN12v8_inspector8protocol23InternalRawNotificationE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol23InternalRawNotificationE, @object
	.size	_ZTVN12v8_inspector8protocol23InternalRawNotificationE, 48
_ZTVN12v8_inspector8protocol23InternalRawNotificationE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol23InternalRawNotificationD1Ev
	.quad	_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime12CallArgumentE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime12CallArgumentE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime12CallArgumentE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime12CallArgumentE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime12CallArgumentE, 48
_ZTVN12v8_inspector8protocol7Runtime12CallArgumentE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime12CallArgument15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime12CallArgument17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime12CallArgumentD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime12CallArgumentD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE, 48
_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime9CallFrameE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime9CallFrameE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE, 48
_ZTVN12v8_inspector8protocol7Runtime9CallFrameE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Debugger8LocationE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Debugger8LocationE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Debugger8LocationE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Debugger8LocationE, @object
	.size	_ZTVN12v8_inspector8protocol8Debugger8LocationE, 48
_ZTVN12v8_inspector8protocol8Debugger8LocationE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Debugger8LocationD1Ev
	.quad	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Debugger14ScriptPositionE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Debugger14ScriptPositionE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Debugger14ScriptPositionE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Debugger14ScriptPositionE, @object
	.size	_ZTVN12v8_inspector8protocol8Debugger14ScriptPositionE, 48
_ZTVN12v8_inspector8protocol8Debugger14ScriptPositionE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Debugger14ScriptPosition15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Debugger14ScriptPosition17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Debugger14ScriptPositionD1Ev
	.quad	_ZN12v8_inspector8protocol8Debugger14ScriptPositionD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Debugger9CallFrameE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Debugger9CallFrameE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Debugger9CallFrameE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Debugger9CallFrameE, @object
	.size	_ZTVN12v8_inspector8protocol8Debugger9CallFrameE, 48
_ZTVN12v8_inspector8protocol8Debugger9CallFrameE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Debugger9CallFrame15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Debugger9CallFrame17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Debugger9CallFrameD1Ev
	.quad	_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Debugger5ScopeE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Debugger5ScopeE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Debugger5ScopeE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Debugger5ScopeE, @object
	.size	_ZTVN12v8_inspector8protocol8Debugger5ScopeE, 48
_ZTVN12v8_inspector8protocol8Debugger5ScopeE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Debugger5Scope15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Debugger5Scope17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Debugger5ScopeD1Ev
	.quad	_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Debugger13BreakLocationE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Debugger13BreakLocationE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Debugger13BreakLocationE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Debugger13BreakLocationE, @object
	.size	_ZTVN12v8_inspector8protocol8Debugger13BreakLocationE, 48
_ZTVN12v8_inspector8protocol8Debugger13BreakLocationE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Debugger13BreakLocation15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Debugger13BreakLocation17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Debugger13BreakLocationD1Ev
	.quad	_ZN12v8_inspector8protocol8Debugger13BreakLocationD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationE, @object
	.size	_ZTVN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationE, 48
_ZTVN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotification15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotification17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationD1Ev
	.quad	_ZN12v8_inspector8protocol8Debugger30BreakpointResolvedNotificationD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Debugger18PausedNotificationE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Debugger18PausedNotificationE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Debugger18PausedNotificationE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Debugger18PausedNotificationE, @object
	.size	_ZTVN12v8_inspector8protocol8Debugger18PausedNotificationE, 48
_ZTVN12v8_inspector8protocol8Debugger18PausedNotificationE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Debugger18PausedNotification15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Debugger18PausedNotification17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Debugger18PausedNotificationD1Ev
	.quad	_ZN12v8_inspector8protocol8Debugger18PausedNotificationD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationE, @object
	.size	_ZTVN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationE, 48
_ZTVN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotification17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationD1Ev
	.quad	_ZN12v8_inspector8protocol8Debugger31ScriptFailedToParseNotificationD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Debugger24ScriptParsedNotificationE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Debugger24ScriptParsedNotificationE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Debugger24ScriptParsedNotificationE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Debugger24ScriptParsedNotificationE, @object
	.size	_ZTVN12v8_inspector8protocol8Debugger24ScriptParsedNotificationE, 48
_ZTVN12v8_inspector8protocol8Debugger24ScriptParsedNotificationE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotification15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotification17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotificationD1Ev
	.quad	_ZN12v8_inspector8protocol8Debugger24ScriptParsedNotificationD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Debugger11SearchMatchE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE, @object
	.size	_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE, 112
_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Debugger11SearchMatch15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Debugger11SearchMatch17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Debugger11SearchMatchD1Ev
	.quad	_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev
	.quad	_ZNK12v8_inspector8protocol8Debugger11SearchMatch12toJSONStringEv
	.quad	_ZNK12v8_inspector8protocol8Debugger11SearchMatch11writeBinaryEPSt6vectorIhSaIhEE
	.quad	-8
	.quad	0
	.quad	_ZThn8_NK12v8_inspector8protocol8Debugger11SearchMatch12toJSONStringEv
	.quad	_ZThn8_NK12v8_inspector8protocol8Debugger11SearchMatch11writeBinaryEPSt6vectorIhSaIhEE
	.quad	_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD1Ev
	.quad	_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Debugger14DispatcherImplE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Debugger14DispatcherImplE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Debugger14DispatcherImplE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Debugger14DispatcherImplE, @object
	.size	_ZTVN12v8_inspector8protocol8Debugger14DispatcherImplE, 48
_ZTVN12v8_inspector8protocol8Debugger14DispatcherImplE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Debugger14DispatcherImplD1Ev
	.quad	_ZN12v8_inspector8protocol8Debugger14DispatcherImplD0Ev
	.quad	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl11canDispatchERKNS_8String16E
	.quad	_ZN12v8_inspector8protocol8Debugger14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.globl	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum3XHRE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC128:
	.string	"XHR"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum3XHRE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum3XHRE, @object
	.size	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum3XHRE, 8
_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum3XHRE:
	.quad	.LC128
	.globl	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum16PromiseRejectionE
	.section	.rodata.str1.1
.LC129:
	.string	"promiseRejection"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum16PromiseRejectionE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum16PromiseRejectionE, @object
	.size	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum16PromiseRejectionE, 8
_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum16PromiseRejectionE:
	.quad	.LC129
	.globl	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum5OtherE
	.section	.rodata.str1.1
.LC130:
	.string	"other"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum5OtherE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum5OtherE, @object
	.size	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum5OtherE, 8
_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum5OtherE:
	.quad	.LC130
	.globl	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum3OOME
	.section	.rodata.str1.1
.LC131:
	.string	"OOM"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum3OOME,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum3OOME, @object
	.size	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum3OOME, 8
_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum3OOME:
	.quad	.LC131
	.globl	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum15InstrumentationE
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum15InstrumentationE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum15InstrumentationE, @object
	.size	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum15InstrumentationE, 8
_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum15InstrumentationE:
	.quad	.LC83
	.globl	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum9ExceptionE
	.section	.rodata.str1.1
.LC132:
	.string	"exception"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum9ExceptionE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum9ExceptionE, @object
	.size	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum9ExceptionE, 8
_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum9ExceptionE:
	.quad	.LC132
	.globl	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum13EventListenerE
	.section	.rodata.str1.1
.LC133:
	.string	"EventListener"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum13EventListenerE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum13EventListenerE, @object
	.size	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum13EventListenerE, 8
_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum13EventListenerE:
	.quad	.LC133
	.globl	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum3DOME
	.section	.rodata.str1.1
.LC134:
	.string	"DOM"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum3DOME,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum3DOME, @object
	.size	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum3DOME, 8
_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum3DOME:
	.quad	.LC134
	.globl	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum12DebugCommandE
	.section	.rodata.str1.1
.LC135:
	.string	"debugCommand"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum12DebugCommandE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum12DebugCommandE, @object
	.size	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum12DebugCommandE, 8
_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum12DebugCommandE:
	.quad	.LC135
	.globl	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum6AssertE
	.section	.rodata.str1.1
.LC136:
	.string	"assert"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum6AssertE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum6AssertE, @object
	.size	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum6AssertE, 8
_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum6AssertE:
	.quad	.LC136
	.globl	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum9AmbiguousE
	.section	.rodata.str1.1
.LC137:
	.string	"ambiguous"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum9AmbiguousE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum9AmbiguousE, @object
	.size	_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum9AmbiguousE, 8
_ZN12v8_inspector8protocol8Debugger3API6Paused10ReasonEnum9AmbiguousE:
	.quad	.LC137
	.globl	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum3XHRE
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum3XHRE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum3XHRE, @object
	.size	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum3XHRE, 8
_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum3XHRE:
	.quad	.LC128
	.globl	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum16PromiseRejectionE
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum16PromiseRejectionE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum16PromiseRejectionE, @object
	.size	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum16PromiseRejectionE, 8
_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum16PromiseRejectionE:
	.quad	.LC129
	.globl	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum5OtherE
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum5OtherE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum5OtherE, @object
	.size	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum5OtherE, 8
_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum5OtherE:
	.quad	.LC130
	.globl	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum3OOME
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum3OOME,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum3OOME, @object
	.size	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum3OOME, 8
_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum3OOME:
	.quad	.LC131
	.globl	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum15InstrumentationE
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum15InstrumentationE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum15InstrumentationE, @object
	.size	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum15InstrumentationE, 8
_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum15InstrumentationE:
	.quad	.LC83
	.globl	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum9ExceptionE
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum9ExceptionE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum9ExceptionE, @object
	.size	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum9ExceptionE, 8
_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum9ExceptionE:
	.quad	.LC132
	.globl	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum13EventListenerE
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum13EventListenerE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum13EventListenerE, @object
	.size	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum13EventListenerE, 8
_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum13EventListenerE:
	.quad	.LC133
	.globl	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum3DOME
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum3DOME,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum3DOME, @object
	.size	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum3DOME, 8
_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum3DOME:
	.quad	.LC134
	.globl	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum12DebugCommandE
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum12DebugCommandE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum12DebugCommandE, @object
	.size	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum12DebugCommandE, 8
_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum12DebugCommandE:
	.quad	.LC135
	.globl	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum6AssertE
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum6AssertE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum6AssertE, @object
	.size	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum6AssertE, 8
_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum6AssertE:
	.quad	.LC136
	.globl	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum9AmbiguousE
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum9AmbiguousE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum9AmbiguousE, @object
	.size	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum9AmbiguousE, 8
_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum9AmbiguousE:
	.quad	.LC137
	.globl	_ZN12v8_inspector8protocol8Debugger20SetPauseOnExceptions9StateEnum3AllE
	.section	.rodata.str1.1
.LC138:
	.string	"all"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger20SetPauseOnExceptions9StateEnum3AllE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger20SetPauseOnExceptions9StateEnum3AllE, @object
	.size	_ZN12v8_inspector8protocol8Debugger20SetPauseOnExceptions9StateEnum3AllE, 8
_ZN12v8_inspector8protocol8Debugger20SetPauseOnExceptions9StateEnum3AllE:
	.quad	.LC138
	.globl	_ZN12v8_inspector8protocol8Debugger20SetPauseOnExceptions9StateEnum8UncaughtE
	.section	.rodata.str1.1
.LC139:
	.string	"uncaught"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger20SetPauseOnExceptions9StateEnum8UncaughtE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger20SetPauseOnExceptions9StateEnum8UncaughtE, @object
	.size	_ZN12v8_inspector8protocol8Debugger20SetPauseOnExceptions9StateEnum8UncaughtE, 8
_ZN12v8_inspector8protocol8Debugger20SetPauseOnExceptions9StateEnum8UncaughtE:
	.quad	.LC139
	.globl	_ZN12v8_inspector8protocol8Debugger20SetPauseOnExceptions9StateEnum4NoneE
	.section	.rodata.str1.1
.LC140:
	.string	"none"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger20SetPauseOnExceptions9StateEnum4NoneE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger20SetPauseOnExceptions9StateEnum4NoneE, @object
	.size	_ZN12v8_inspector8protocol8Debugger20SetPauseOnExceptions9StateEnum4NoneE, 8
_ZN12v8_inspector8protocol8Debugger20SetPauseOnExceptions9StateEnum4NoneE:
	.quad	.LC140
	.globl	_ZN12v8_inspector8protocol8Debugger28SetInstrumentationBreakpoint19InstrumentationEnum34BeforeScriptWithSourceMapExecutionE
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC141:
	.string	"beforeScriptWithSourceMapExecution"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger28SetInstrumentationBreakpoint19InstrumentationEnum34BeforeScriptWithSourceMapExecutionE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger28SetInstrumentationBreakpoint19InstrumentationEnum34BeforeScriptWithSourceMapExecutionE, @object
	.size	_ZN12v8_inspector8protocol8Debugger28SetInstrumentationBreakpoint19InstrumentationEnum34BeforeScriptWithSourceMapExecutionE, 8
_ZN12v8_inspector8protocol8Debugger28SetInstrumentationBreakpoint19InstrumentationEnum34BeforeScriptWithSourceMapExecutionE:
	.quad	.LC141
	.globl	_ZN12v8_inspector8protocol8Debugger28SetInstrumentationBreakpoint19InstrumentationEnum21BeforeScriptExecutionE
	.section	.rodata.str1.1
.LC142:
	.string	"beforeScriptExecution"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger28SetInstrumentationBreakpoint19InstrumentationEnum21BeforeScriptExecutionE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger28SetInstrumentationBreakpoint19InstrumentationEnum21BeforeScriptExecutionE, @object
	.size	_ZN12v8_inspector8protocol8Debugger28SetInstrumentationBreakpoint19InstrumentationEnum21BeforeScriptExecutionE, 8
_ZN12v8_inspector8protocol8Debugger28SetInstrumentationBreakpoint19InstrumentationEnum21BeforeScriptExecutionE:
	.quad	.LC142
	.globl	_ZN12v8_inspector8protocol8Debugger18ContinueToLocation20TargetCallFramesEnum7CurrentE
	.section	.rodata.str1.1
.LC143:
	.string	"current"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger18ContinueToLocation20TargetCallFramesEnum7CurrentE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger18ContinueToLocation20TargetCallFramesEnum7CurrentE, @object
	.size	_ZN12v8_inspector8protocol8Debugger18ContinueToLocation20TargetCallFramesEnum7CurrentE, 8
_ZN12v8_inspector8protocol8Debugger18ContinueToLocation20TargetCallFramesEnum7CurrentE:
	.quad	.LC143
	.globl	_ZN12v8_inspector8protocol8Debugger18ContinueToLocation20TargetCallFramesEnum3AnyE
	.section	.rodata.str1.1
.LC144:
	.string	"any"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger18ContinueToLocation20TargetCallFramesEnum3AnyE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger18ContinueToLocation20TargetCallFramesEnum3AnyE, @object
	.size	_ZN12v8_inspector8protocol8Debugger18ContinueToLocation20TargetCallFramesEnum3AnyE, 8
_ZN12v8_inspector8protocol8Debugger18ContinueToLocation20TargetCallFramesEnum3AnyE:
	.quad	.LC144
	.globl	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum3XHRE
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum3XHRE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum3XHRE, @object
	.size	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum3XHRE, 8
_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum3XHRE:
	.quad	.LC128
	.globl	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum16PromiseRejectionE
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum16PromiseRejectionE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum16PromiseRejectionE, @object
	.size	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum16PromiseRejectionE, 8
_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum16PromiseRejectionE:
	.quad	.LC129
	.globl	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum5OtherE
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum5OtherE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum5OtherE, @object
	.size	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum5OtherE, 8
_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum5OtherE:
	.quad	.LC130
	.globl	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum3OOME
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum3OOME,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum3OOME, @object
	.size	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum3OOME, 8
_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum3OOME:
	.quad	.LC131
	.globl	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum15InstrumentationE
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum15InstrumentationE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum15InstrumentationE, @object
	.size	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum15InstrumentationE, 8
_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum15InstrumentationE:
	.quad	.LC83
	.globl	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum9ExceptionE
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum9ExceptionE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum9ExceptionE, @object
	.size	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum9ExceptionE, 8
_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum9ExceptionE:
	.quad	.LC132
	.globl	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum13EventListenerE
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum13EventListenerE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum13EventListenerE, @object
	.size	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum13EventListenerE, 8
_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum13EventListenerE:
	.quad	.LC133
	.globl	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum3DOME
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum3DOME,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum3DOME, @object
	.size	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum3DOME, 8
_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum3DOME:
	.quad	.LC134
	.globl	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum12DebugCommandE
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum12DebugCommandE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum12DebugCommandE, @object
	.size	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum12DebugCommandE, 8
_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum12DebugCommandE:
	.quad	.LC135
	.globl	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum6AssertE
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum6AssertE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum6AssertE, @object
	.size	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum6AssertE, 8
_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum6AssertE:
	.quad	.LC136
	.globl	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum9AmbiguousE
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum9AmbiguousE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum9AmbiguousE, @object
	.size	_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum9AmbiguousE, 8
_ZN12v8_inspector8protocol8Debugger18PausedNotification10ReasonEnum9AmbiguousE:
	.quad	.LC137
	.globl	_ZN12v8_inspector8protocol8Debugger13BreakLocation8TypeEnum6ReturnE
	.section	.rodata.str1.1
.LC145:
	.string	"return"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger13BreakLocation8TypeEnum6ReturnE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger13BreakLocation8TypeEnum6ReturnE, @object
	.size	_ZN12v8_inspector8protocol8Debugger13BreakLocation8TypeEnum6ReturnE, 8
_ZN12v8_inspector8protocol8Debugger13BreakLocation8TypeEnum6ReturnE:
	.quad	.LC145
	.globl	_ZN12v8_inspector8protocol8Debugger13BreakLocation8TypeEnum4CallE
	.section	.rodata.str1.1
.LC146:
	.string	"call"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger13BreakLocation8TypeEnum4CallE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger13BreakLocation8TypeEnum4CallE, @object
	.size	_ZN12v8_inspector8protocol8Debugger13BreakLocation8TypeEnum4CallE, 8
_ZN12v8_inspector8protocol8Debugger13BreakLocation8TypeEnum4CallE:
	.quad	.LC146
	.globl	_ZN12v8_inspector8protocol8Debugger13BreakLocation8TypeEnum17DebuggerStatementE
	.section	.rodata.str1.1
.LC147:
	.string	"debuggerStatement"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger13BreakLocation8TypeEnum17DebuggerStatementE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger13BreakLocation8TypeEnum17DebuggerStatementE, @object
	.size	_ZN12v8_inspector8protocol8Debugger13BreakLocation8TypeEnum17DebuggerStatementE, 8
_ZN12v8_inspector8protocol8Debugger13BreakLocation8TypeEnum17DebuggerStatementE:
	.quad	.LC147
	.globl	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum6ModuleE
	.section	.rodata.str1.1
.LC148:
	.string	"module"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum6ModuleE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum6ModuleE, @object
	.size	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum6ModuleE, 8
_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum6ModuleE:
	.quad	.LC148
	.globl	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum4EvalE
	.section	.rodata.str1.1
.LC149:
	.string	"eval"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum4EvalE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum4EvalE, @object
	.size	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum4EvalE, 8
_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum4EvalE:
	.quad	.LC149
	.globl	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum6ScriptE
	.section	.rodata.str1.1
.LC150:
	.string	"script"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum6ScriptE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum6ScriptE, @object
	.size	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum6ScriptE, 8
_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum6ScriptE:
	.quad	.LC150
	.globl	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum5BlockE
	.section	.rodata.str1.1
.LC151:
	.string	"block"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum5BlockE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum5BlockE, @object
	.size	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum5BlockE, 8
_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum5BlockE:
	.quad	.LC151
	.globl	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum5CatchE
	.section	.rodata.str1.1
.LC152:
	.string	"catch"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum5CatchE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum5CatchE, @object
	.size	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum5CatchE, 8
_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum5CatchE:
	.quad	.LC152
	.globl	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum7ClosureE
	.section	.rodata.str1.1
.LC153:
	.string	"closure"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum7ClosureE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum7ClosureE, @object
	.size	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum7ClosureE, 8
_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum7ClosureE:
	.quad	.LC153
	.globl	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum4WithE
	.section	.rodata.str1.1
.LC154:
	.string	"with"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum4WithE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum4WithE, @object
	.size	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum4WithE, 8
_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum4WithE:
	.quad	.LC154
	.globl	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum5LocalE
	.section	.rodata.str1.1
.LC155:
	.string	"local"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum5LocalE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum5LocalE, @object
	.size	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum5LocalE, 8
_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum5LocalE:
	.quad	.LC155
	.globl	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum6GlobalE
	.section	.rodata.str1.1
.LC156:
	.string	"global"
	.section	.data.rel.local._ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum6GlobalE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum6GlobalE, @object
	.size	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum6GlobalE, 8
_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum6GlobalE:
	.quad	.LC156
	.globl	_ZN12v8_inspector8protocol8Debugger8Metainfo7versionE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger8Metainfo7versionE,"a"
	.type	_ZN12v8_inspector8protocol8Debugger8Metainfo7versionE, @object
	.size	_ZN12v8_inspector8protocol8Debugger8Metainfo7versionE, 4
_ZN12v8_inspector8protocol8Debugger8Metainfo7versionE:
	.string	"1.3"
	.globl	_ZN12v8_inspector8protocol8Debugger8Metainfo13commandPrefixE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger8Metainfo13commandPrefixE,"a"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger8Metainfo13commandPrefixE, @object
	.size	_ZN12v8_inspector8protocol8Debugger8Metainfo13commandPrefixE, 10
_ZN12v8_inspector8protocol8Debugger8Metainfo13commandPrefixE:
	.string	"Debugger."
	.globl	_ZN12v8_inspector8protocol8Debugger8Metainfo10domainNameE
	.section	.rodata._ZN12v8_inspector8protocol8Debugger8Metainfo10domainNameE,"a"
	.align 8
	.type	_ZN12v8_inspector8protocol8Debugger8Metainfo10domainNameE, @object
	.size	_ZN12v8_inspector8protocol8Debugger8Metainfo10domainNameE, 9
_ZN12v8_inspector8protocol8Debugger8Metainfo10domainNameE:
	.string	"Debugger"
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC97:
	.long	1065353216
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
