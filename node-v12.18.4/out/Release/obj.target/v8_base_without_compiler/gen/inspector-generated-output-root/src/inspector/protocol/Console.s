	.file	"Console.cpp"
	.text
	.section	.text._ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv, @function
_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv:
.LFB4422:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movq	8(%rsi), %rcx
	leaq	24(%rsi), %rdx
	cmpq	%rdx, %rcx
	je	.L5
	movq	%rcx, (%rdi)
	movq	24(%rsi), %rcx
	movq	%rcx, 16(%rdi)
.L3:
	movq	16(%rsi), %rcx
	movq	%rdx, 8(%rsi)
	xorl	%edx, %edx
	movq	$0, 16(%rsi)
	movq	%rcx, 8(%rax)
	movw	%dx, 24(%rsi)
	movq	40(%rsi), %rdx
	movq	%rdx, 32(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movdqu	24(%rsi), %xmm0
	movups	%xmm0, 16(%rdi)
	jmp	.L3
	.cfi_endproc
.LFE4422:
	.size	_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv, .-_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv:
.LFB4423:
	.cfi_startproc
	endbr64
	movdqu	48(%rsi), %xmm1
	movq	64(%rsi), %rdx
	pxor	%xmm0, %xmm0
	movq	%rdi, %rax
	movq	$0, 64(%rsi)
	movq	%rdx, 16(%rdi)
	movups	%xmm1, (%rdi)
	movups	%xmm0, 48(%rsi)
	ret
	.cfi_endproc
.LFE4423:
	.size	_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv, .-_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Console14ConsoleMessageD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Console14ConsoleMessageD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Console14ConsoleMessageD2Ev
	.type	_ZN12v8_inspector8protocol7Console14ConsoleMessageD2Ev, @function
_ZN12v8_inspector8protocol7Console14ConsoleMessageD2Ev:
.LFB5511:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Console14ConsoleMessageE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	136(%rdi), %rdi
	leaq	152(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L8
	call	_ZdlPv@PLT
.L8:
	movq	88(%rbx), %rdi
	leaq	104(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L9
	call	_ZdlPv@PLT
.L9:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L7
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5511:
	.size	_ZN12v8_inspector8protocol7Console14ConsoleMessageD2Ev, .-_ZN12v8_inspector8protocol7Console14ConsoleMessageD2Ev
	.weak	_ZN12v8_inspector8protocol7Console14ConsoleMessageD1Ev
	.set	_ZN12v8_inspector8protocol7Console14ConsoleMessageD1Ev,_ZN12v8_inspector8protocol7Console14ConsoleMessageD2Ev
	.section	.text._ZN12v8_inspector8protocol23InternalRawNotificationD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol23InternalRawNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev
	.type	_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev, @function
_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev:
.LFB4419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol23InternalRawNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L14
	call	_ZdlPv@PLT
.L14:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L13
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4419:
	.size	_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev, .-_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev
	.weak	_ZN12v8_inspector8protocol23InternalRawNotificationD1Ev
	.set	_ZN12v8_inspector8protocol23InternalRawNotificationD1Ev,_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev
	.section	.text._ZN12v8_inspector8protocol7Console14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol7Console14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol7Console14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol7Console14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB5664:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-120(%rbp), %rdi
	movq	%rbx, %rsi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	movq	%r15, %rdi
	movq	(%rsi), %rax
	call	*32(%rax)
	cmpl	$2, -112(%rbp)
	je	.L33
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L22
	movq	%r15, %rdx
	movl	%r12d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L22:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L23
	call	_ZdlPv@PLT
.L23:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L20
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L20:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L34
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L22
.L34:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5664:
	.size	_ZN12v8_inspector8protocol7Console14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol7Console14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol7Console14DispatcherImplD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Console14DispatcherImplD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Console14DispatcherImplD2Ev
	.type	_ZN12v8_inspector8protocol7Console14DispatcherImplD2Ev, @function
_ZN12v8_inspector8protocol7Console14DispatcherImplD2Ev:
.LFB5650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Console14DispatcherImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	144(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	jne	.L40
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L66:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L36
.L39:
	movq	%rbx, %r13
.L40:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L37
	call	_ZdlPv@PLT
.L37:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L66
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L39
.L36:
	movq	136(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%r12), %rdi
	leaq	176(%r12), %rax
	movq	$0, 152(%r12)
	movq	$0, 144(%r12)
	cmpq	%rax, %rdi
	je	.L41
	call	_ZdlPv@PLT
.L41:
	movq	88(%r12), %r13
	testq	%r13, %r13
	jne	.L45
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L67:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L42
.L44:
	movq	%rbx, %r13
.L45:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	jne	.L67
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L44
.L42:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L46
	call	_ZdlPv@PLT
.L46:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector8protocol14DispatcherBaseD2Ev@PLT
	.cfi_endproc
.LFE5650:
	.size	_ZN12v8_inspector8protocol7Console14DispatcherImplD2Ev, .-_ZN12v8_inspector8protocol7Console14DispatcherImplD2Ev
	.weak	_ZN12v8_inspector8protocol7Console14DispatcherImplD1Ev
	.set	_ZN12v8_inspector8protocol7Console14DispatcherImplD1Ev,_ZN12v8_inspector8protocol7Console14DispatcherImplD2Ev
	.section	.text._ZN12v8_inspector8protocol23InternalRawNotificationD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol23InternalRawNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev
	.type	_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev, @function
_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev:
.LFB4421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol23InternalRawNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L69
	call	_ZdlPv@PLT
.L69:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L70
	call	_ZdlPv@PLT
.L70:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4421:
	.size	_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev, .-_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev
	.section	.text._ZN12v8_inspector8protocol7Console14DispatcherImpl13clearMessagesEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol7Console14DispatcherImpl13clearMessagesEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol7Console14DispatcherImpl13clearMessagesEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol7Console14DispatcherImpl13clearMessagesEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB5656:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-120(%rbp), %rdi
	movq	%rbx, %rsi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	movq	%r15, %rdi
	movq	(%rsi), %rax
	call	*16(%rax)
	cmpl	$2, -112(%rbp)
	je	.L88
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L77
	movq	%r15, %rdx
	movl	%r12d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L77:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L78
	call	_ZdlPv@PLT
.L78:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L75
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L75:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L89
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L77
.L89:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5656:
	.size	_ZN12v8_inspector8protocol7Console14DispatcherImpl13clearMessagesEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol7Console14DispatcherImpl13clearMessagesEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol7Console14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol7Console14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol7Console14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol7Console14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB5663:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-120(%rbp), %rdi
	movq	%rbx, %rsi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	movq	%r15, %rdi
	movq	(%rsi), %rax
	call	*24(%rax)
	cmpl	$2, -112(%rbp)
	je	.L103
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L92
	movq	%r15, %rdx
	movl	%r12d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L92:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L93
	call	_ZdlPv@PLT
.L93:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L90
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L90:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L104
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L92
.L104:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5663:
	.size	_ZN12v8_inspector8protocol7Console14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol7Console14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol16InternalResponseD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol16InternalResponseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol16InternalResponseD0Ev
	.type	_ZN12v8_inspector8protocol16InternalResponseD0Ev, @function
_ZN12v8_inspector8protocol16InternalResponseD0Ev:
.LFB4412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L106
	movq	(%rdi), %rax
	call	*24(%rax)
.L106:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L107
	call	_ZdlPv@PLT
.L107:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4412:
	.size	_ZN12v8_inspector8protocol16InternalResponseD0Ev, .-_ZN12v8_inspector8protocol16InternalResponseD0Ev
	.section	.text._ZN12v8_inspector8protocol7Console14DispatcherImplD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Console14DispatcherImplD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Console14DispatcherImplD0Ev
	.type	_ZN12v8_inspector8protocol7Console14DispatcherImplD0Ev, @function
_ZN12v8_inspector8protocol7Console14DispatcherImplD0Ev:
.LFB5652:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Console14DispatcherImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	144(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	jne	.L117
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L143:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L113
.L116:
	movq	%rbx, %r13
.L117:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L114
	call	_ZdlPv@PLT
.L114:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L143
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L116
.L113:
	movq	136(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%r12), %rdi
	leaq	176(%r12), %rax
	movq	$0, 152(%r12)
	movq	$0, 144(%r12)
	cmpq	%rax, %rdi
	je	.L118
	call	_ZdlPv@PLT
.L118:
	movq	88(%r12), %r13
	testq	%r13, %r13
	jne	.L122
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L144:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L119
.L121:
	movq	%rbx, %r13
.L122:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	jne	.L144
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L121
.L119:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L123
	call	_ZdlPv@PLT
.L123:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$192, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5652:
	.size	_ZN12v8_inspector8protocol7Console14DispatcherImplD0Ev, .-_ZN12v8_inspector8protocol7Console14DispatcherImplD0Ev
	.section	.text._ZN12v8_inspector8protocol7Console14ConsoleMessageD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Console14ConsoleMessageD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Console14ConsoleMessageD0Ev
	.type	_ZN12v8_inspector8protocol7Console14ConsoleMessageD0Ev, @function
_ZN12v8_inspector8protocol7Console14ConsoleMessageD0Ev:
.LFB5513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Console14ConsoleMessageE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	136(%rdi), %rdi
	leaq	152(%r12), %rax
	cmpq	%rax, %rdi
	je	.L146
	call	_ZdlPv@PLT
.L146:
	movq	88(%r12), %rdi
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L147
	call	_ZdlPv@PLT
.L147:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L148
	call	_ZdlPv@PLT
.L148:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L149
	call	_ZdlPv@PLT
.L149:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$192, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5513:
	.size	_ZN12v8_inspector8protocol7Console14ConsoleMessageD0Ev, .-_ZN12v8_inspector8protocol7Console14ConsoleMessageD0Ev
	.section	.text._ZN12v8_inspector8protocol7Console24MessageAddedNotificationD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Console24MessageAddedNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Console24MessageAddedNotificationD2Ev
	.type	_ZN12v8_inspector8protocol7Console24MessageAddedNotificationD2Ev, @function
_ZN12v8_inspector8protocol7Console24MessageAddedNotificationD2Ev:
.LFB5554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Console24MessageAddedNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L151
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Console14ConsoleMessageD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L153
	leaq	16+_ZTVN12v8_inspector8protocol7Console14ConsoleMessageE(%rip), %rax
	movq	136(%r12), %rdi
	movq	%rax, (%r12)
	leaq	152(%r12), %rax
	cmpq	%rax, %rdi
	je	.L154
	call	_ZdlPv@PLT
.L154:
	movq	88(%r12), %rdi
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L155
	call	_ZdlPv@PLT
.L155:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L156
	call	_ZdlPv@PLT
.L156:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L157
	call	_ZdlPv@PLT
.L157:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$192, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE5554:
	.size	_ZN12v8_inspector8protocol7Console24MessageAddedNotificationD2Ev, .-_ZN12v8_inspector8protocol7Console24MessageAddedNotificationD2Ev
	.weak	_ZN12v8_inspector8protocol7Console24MessageAddedNotificationD1Ev
	.set	_ZN12v8_inspector8protocol7Console24MessageAddedNotificationD1Ev,_ZN12v8_inspector8protocol7Console24MessageAddedNotificationD2Ev
	.section	.text._ZN12v8_inspector8protocol7Console24MessageAddedNotificationD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Console24MessageAddedNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Console24MessageAddedNotificationD0Ev
	.type	_ZN12v8_inspector8protocol7Console24MessageAddedNotificationD0Ev, @function
_ZN12v8_inspector8protocol7Console24MessageAddedNotificationD0Ev:
.LFB5556:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Console24MessageAddedNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	8(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L160
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Console14ConsoleMessageD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L161
	leaq	16+_ZTVN12v8_inspector8protocol7Console14ConsoleMessageE(%rip), %rax
	movq	136(%r12), %rdi
	movq	%rax, (%r12)
	leaq	152(%r12), %rax
	cmpq	%rax, %rdi
	je	.L162
	call	_ZdlPv@PLT
.L162:
	movq	88(%r12), %rdi
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L163
	call	_ZdlPv@PLT
.L163:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L164
	call	_ZdlPv@PLT
.L164:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L165
	call	_ZdlPv@PLT
.L165:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L160:
	popq	%r12
	movq	%r13, %rdi
	movl	$16, %esi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L160
	.cfi_endproc
.LFE5556:
	.size	_ZN12v8_inspector8protocol7Console24MessageAddedNotificationD0Ev, .-_ZN12v8_inspector8protocol7Console24MessageAddedNotificationD0Ev
	.section	.text._ZN12v8_inspector8protocol16InternalResponseD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol16InternalResponseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol16InternalResponseD2Ev
	.type	_ZN12v8_inspector8protocol16InternalResponseD2Ev, @function
_ZN12v8_inspector8protocol16InternalResponseD2Ev:
.LFB4410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L171
	movq	(%rdi), %rax
	call	*24(%rax)
.L171:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L170
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4410:
	.size	_ZN12v8_inspector8protocol16InternalResponseD2Ev, .-_ZN12v8_inspector8protocol16InternalResponseD2Ev
	.weak	_ZN12v8_inspector8protocol16InternalResponseD1Ev
	.set	_ZN12v8_inspector8protocol16InternalResponseD1Ev,_ZN12v8_inspector8protocol16InternalResponseD2Ev
	.section	.rodata._ZN12v8_inspector8protocol7Console14ConsoleMessage9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"object expected"
.LC1:
	.string	"source"
.LC2:
	.string	"string value expected"
.LC3:
	.string	"level"
.LC4:
	.string	"text"
.LC5:
	.string	"url"
.LC6:
	.string	"basic_string::_M_create"
.LC7:
	.string	"line"
.LC8:
	.string	"integer value expected"
.LC9:
	.string	"column"
	.section	.text._ZN12v8_inspector8protocol7Console14ConsoleMessage9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol7Console14ConsoleMessage9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol7Console14ConsoleMessage9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol7Console14ConsoleMessage9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB5587:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L178
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r12
	je	.L179
.L178:
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, (%r14)
.L177:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L298
	addq	$136, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	movl	$192, %edi
	leaq	-96(%rbp), %r15
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol7Console14ConsoleMessageE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	24(%rbx), %rax
	movq	%rax, -136(%rbp)
	movq	%rax, 8(%rbx)
	xorl	%eax, %eax
	movw	%ax, 24(%rbx)
	leaq	64(%rbx), %rax
	movq	%rax, -144(%rbp)
	movq	%rax, 48(%rbx)
	xorl	%eax, %eax
	movw	%ax, 64(%rbx)
	leaq	104(%rbx), %rax
	movq	%rax, -152(%rbp)
	movq	%rax, 88(%rbx)
	xorl	%eax, %eax
	cmpl	$6, 8(%r12)
	movw	%ax, 104(%rbx)
	leaq	152(%rbx), %rax
	movq	%rax, -128(%rbp)
	movq	%rax, 136(%rbx)
	movl	$0, %eax
	cmovne	%rax, %r12
	movups	%xmm0, 152(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 120(%rbx)
	movb	$0, 128(%rbx)
	movq	$0, 168(%rbx)
	movq	$0, 144(%rbx)
	movb	$0, 176(%rbx)
	movl	$0, 180(%rbx)
	movb	$0, 184(%rbx)
	movl	$0, 188(%rbx)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r9
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rdi
	je	.L182
	movq	%r9, -160(%rbp)
	call	_ZdlPv@PLT
	movq	-160(%rbp), %r9
.L182:
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	movq	%r9, -160(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-120(%rbp), %rax
	movq	-160(%rbp), %r9
	movq	$0, -88(%rbp)
	movq	$0, -64(%rbp)
	movq	%rax, -96(%rbp)
	xorl	%eax, %eax
	movw	%ax, -80(%rbp)
	testq	%r9, %r9
	je	.L185
	movq	(%r9), %rax
	movq	%r15, %rsi
	movq	%r9, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L184
.L185:
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L184:
	movq	8(%rbx), %rdi
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rax
	cmpq	-120(%rbp), %rdx
	je	.L299
	movq	-80(%rbp), %rcx
	cmpq	%rdi, -136(%rbp)
	je	.L300
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	24(%rbx), %rsi
	movq	%rdx, 8(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 16(%rbx)
	testq	%rdi, %rdi
	je	.L191
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L189:
	xorl	%r11d, %r11d
	movq	$0, -88(%rbp)
	movw	%r11w, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 40(%rbx)
	cmpq	-120(%rbp), %rdi
	je	.L192
	call	_ZdlPv@PLT
.L192:
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r9
	cmpq	-120(%rbp), %rdi
	je	.L193
	movq	%rax, -160(%rbp)
	call	_ZdlPv@PLT
	movq	-160(%rbp), %r9
.L193:
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	movq	%r9, -160(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-120(%rbp), %rax
	xorl	%r10d, %r10d
	movq	-160(%rbp), %r9
	movq	$0, -88(%rbp)
	movq	%rax, -96(%rbp)
	movw	%r10w, -80(%rbp)
	movq	$0, -64(%rbp)
	testq	%r9, %r9
	je	.L196
	movq	(%r9), %rax
	movq	%r15, %rsi
	movq	%r9, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L195
.L196:
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L195:
	movq	48(%rbx), %rdi
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rax
	cmpq	-120(%rbp), %rdx
	je	.L301
	movq	-80(%rbp), %rcx
	cmpq	%rdi, -144(%rbp)
	je	.L302
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	64(%rbx), %rsi
	movq	%rdx, 48(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 56(%rbx)
	testq	%rdi, %rdi
	je	.L202
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L200:
	xorl	%r8d, %r8d
	movq	$0, -88(%rbp)
	movw	%r8w, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 80(%rbx)
	cmpq	-120(%rbp), %rdi
	je	.L203
	call	_ZdlPv@PLT
.L203:
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r9
	cmpq	-120(%rbp), %rdi
	je	.L204
	movq	%rax, -160(%rbp)
	call	_ZdlPv@PLT
	movq	-160(%rbp), %r9
.L204:
	movq	%r13, %rdi
	leaq	.LC4(%rip), %rsi
	movq	%r9, -160(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-120(%rbp), %rax
	xorl	%edi, %edi
	movq	-160(%rbp), %r9
	movq	$0, -88(%rbp)
	movq	%rax, -96(%rbp)
	movw	%di, -80(%rbp)
	movq	$0, -64(%rbp)
	testq	%r9, %r9
	je	.L207
	movq	(%r9), %rax
	movq	%r15, %rsi
	movq	%r9, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L206
.L207:
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L206:
	movq	88(%rbx), %rdi
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rax
	cmpq	-120(%rbp), %rdx
	je	.L303
	movq	-80(%rbp), %rcx
	cmpq	%rdi, -152(%rbp)
	je	.L304
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	104(%rbx), %rsi
	movq	%rdx, 88(%rbx)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 96(%rbx)
	testq	%rdi, %rdi
	je	.L213
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L211:
	xorl	%ecx, %ecx
	movq	$0, -88(%rbp)
	movw	%cx, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 120(%rbx)
	cmpq	-120(%rbp), %rdi
	je	.L214
	call	_ZdlPv@PLT
.L214:
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r9
	cmpq	-120(%rbp), %rdi
	je	.L215
	movq	%rax, -160(%rbp)
	call	_ZdlPv@PLT
	movq	-160(%rbp), %r9
.L215:
	testq	%r9, %r9
	movq	%r9, -160(%rbp)
	je	.L216
	leaq	.LC5(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-120(%rbp), %rax
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	-160(%rbp), %r9
	movq	$0, -88(%rbp)
	movq	%rax, -96(%rbp)
	movw	%dx, -80(%rbp)
	movq	%r9, %rdi
	movq	$0, -64(%rbp)
	movq	(%r9), %rax
	call	*56(%rax)
	testb	%al, %al
	je	.L305
.L217:
	movq	-88(%rbp), %rcx
	movq	136(%rbx), %r9
	cmpq	%r9, -128(%rbp)
	je	.L245
	movq	152(%rbx), %rax
.L218:
	cmpq	%rax, %rcx
	ja	.L306
.L219:
	leaq	(%rcx,%rcx), %rdx
	testq	%rcx, %rcx
	je	.L222
	movq	-96(%rbp), %rsi
	cmpq	$1, %rcx
	je	.L307
	testq	%rdx, %rdx
	je	.L222
	movq	%r9, %rdi
	movq	%rcx, -160(%rbp)
	call	memmove@PLT
	movq	136(%rbx), %r9
	movq	-160(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L222:
	xorl	%eax, %eax
	movq	%rcx, 144(%rbx)
	movw	%ax, (%r9,%rcx,2)
	movq	-64(%rbp), %rax
	movb	$1, 128(%rbx)
	movq	-96(%rbp), %rdi
	movq	%rax, 168(%rbx)
	cmpq	-120(%rbp), %rdi
	je	.L216
	call	_ZdlPv@PLT
.L216:
	leaq	.LC7(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r9
	cmpq	-120(%rbp), %rdi
	je	.L225
	movq	%rax, -160(%rbp)
	call	_ZdlPv@PLT
	movq	-160(%rbp), %r9
.L225:
	testq	%r9, %r9
	movq	%r9, -160(%rbp)
	je	.L226
	leaq	.LC7(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-160(%rbp), %r9
	movl	$0, -100(%rbp)
	leaq	-100(%rbp), %rsi
	movq	(%r9), %rax
	movq	%r9, %rdi
	call	*48(%rax)
	testb	%al, %al
	je	.L308
.L227:
	movl	-100(%rbp), %eax
	movb	$1, 176(%rbx)
	movl	%eax, 180(%rbx)
.L226:
	leaq	.LC9(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r12
	cmpq	-120(%rbp), %rdi
	je	.L228
	call	_ZdlPv@PLT
.L228:
	testq	%r12, %r12
	je	.L229
	leaq	.LC9(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movl	$0, -100(%rbp)
	movq	(%r12), %rax
	leaq	-100(%rbp), %rsi
	movq	%r12, %rdi
	call	*48(%rax)
	testb	%al, %al
	je	.L309
.L230:
	movl	-100(%rbp), %eax
	movb	$1, 184(%rbx)
	movl	%eax, 188(%rbx)
.L229:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L310
	movq	%rbx, (%r14)
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L305:
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L308:
	leaq	.LC8(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L309:
	leaq	.LC8(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L310:
	movq	(%rbx), %rax
	movq	$0, (%r14)
	leaq	_ZN12v8_inspector8protocol7Console14ConsoleMessageD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L311
	leaq	16+_ZTVN12v8_inspector8protocol7Console14ConsoleMessageE(%rip), %rax
	movq	136(%rbx), %rdi
	movq	%rax, (%rbx)
	cmpq	%rdi, -128(%rbp)
	je	.L231
	call	_ZdlPv@PLT
.L231:
	movq	88(%rbx), %rdi
	cmpq	%rdi, -152(%rbp)
	je	.L232
	call	_ZdlPv@PLT
.L232:
	movq	48(%rbx), %rdi
	cmpq	%rdi, -144(%rbp)
	je	.L233
	call	_ZdlPv@PLT
.L233:
	movq	8(%rbx), %rdi
	cmpq	%rdi, -136(%rbp)
	je	.L234
	call	_ZdlPv@PLT
.L234:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L303:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L209
	cmpq	$1, %rax
	je	.L312
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L209
	movq	-120(%rbp), %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	88(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L209:
	xorl	%esi, %esi
	movq	%rax, 96(%rbx)
	movw	%si, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L301:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L198
	cmpq	$1, %rax
	je	.L313
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L198
	movq	-120(%rbp), %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	48(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L198:
	xorl	%r9d, %r9d
	movq	%rax, 56(%rbx)
	movw	%r9w, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L299:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L187
	cmpq	$1, %rax
	je	.L314
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L187
	movq	-120(%rbp), %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	8(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L187:
	movq	%rax, 16(%rbx)
	xorl	%eax, %eax
	movw	%ax, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L304:
	movq	%rax, %xmm0
	movq	%rcx, %xmm6
	movq	%rdx, 88(%rbx)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 96(%rbx)
.L213:
	movq	-120(%rbp), %rax
	movq	%rax, -96(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	%rax, %rdi
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L302:
	movq	%rax, %xmm0
	movq	%rcx, %xmm5
	movq	%rdx, 48(%rbx)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, 56(%rbx)
.L202:
	movq	-120(%rbp), %rax
	movq	%rax, -96(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	%rax, %rdi
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L300:
	movq	%rax, %xmm0
	movq	%rcx, %xmm4
	movq	%rdx, 8(%rbx)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 16(%rbx)
.L191:
	movq	-120(%rbp), %rax
	movq	%rax, -96(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	%rax, %rdi
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L306:
	movabsq	$2305843009213693951, %rdx
	cmpq	%rdx, %rcx
	ja	.L315
	addq	%rax, %rax
	movq	%rcx, -160(%rbp)
	cmpq	%rax, %rcx
	jnb	.L221
	cmpq	%rdx, %rax
	cmovbe	%rax, %rdx
	movq	%rdx, -160(%rbp)
.L221:
	movq	-160(%rbp), %rax
	movq	%rcx, -168(%rbp)
	leaq	2(%rax,%rax), %rdi
	call	_Znwm@PLT
	movq	136(%rbx), %rdi
	cmpq	%rdi, -128(%rbp)
	movq	-168(%rbp), %rcx
	movq	%rax, %r9
	je	.L240
	movq	%r9, -176(%rbp)
	movq	%rcx, -168(%rbp)
	call	_ZdlPv@PLT
	movq	-176(%rbp), %r9
	movq	-168(%rbp), %rcx
.L240:
	movq	-160(%rbp), %rax
	movq	%r9, 136(%rbx)
	movq	%rax, 152(%rbx)
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L311:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L245:
	movl	$7, %eax
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L307:
	movzwl	(%rsi), %eax
	movw	%ax, (%r9)
	movq	136(%rbx), %r9
	jmp	.L222
.L312:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	88(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L209
.L314:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	8(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L187
.L313:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	48(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L198
.L298:
	call	__stack_chk_fail@PLT
.L315:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5587:
	.size	_ZN12v8_inspector8protocol7Console14ConsoleMessage9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol7Console14ConsoleMessage9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.rodata._ZNK12v8_inspector8protocol7Console14ConsoleMessage7toValueEv.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNK12v8_inspector8protocol7Console14ConsoleMessage7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol7Console14ConsoleMessage7toValueEv
	.type	_ZNK12v8_inspector8protocol7Console14ConsoleMessage7toValueEv, @function
_ZNK12v8_inspector8protocol7Console14ConsoleMessage7toValueEv:
.LFB5592:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$96, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r14
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r14, 0(%r13)
	movl	$56, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rcx
	movq	%rax, %r12
	movl	$4, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%r12)
	leaq	(%rcx,%rcx), %r15
	movq	%rsi, %rax
	leaq	32(%r12), %rdi
	addq	%r15, %rax
	movq	%rdi, 16(%r12)
	je	.L317
	testq	%rsi, %rsi
	je	.L324
.L317:
	movq	%r15, %r8
	sarq	%r8
	cmpq	$14, %r15
	ja	.L401
.L318:
	cmpq	$2, %r15
	je	.L402
	testq	%r15, %r15
	je	.L321
	movq	%r15, %rdx
	movq	%r8, -128(%rbp)
	movq	%rcx, -120(%rbp)
	call	memmove@PLT
	movq	16(%r12), %rdi
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %rcx
.L321:
	movq	%r8, 24(%r12)
	xorl	%r8d, %r8d
	leaq	.LC1(%rip), %rsi
	leaq	-104(%rbp), %r15
	movw	%r8w, (%rdi,%rcx,2)
	movq	40(%rbx), %rax
	movq	%r12, -104(%rbp)
	movq	%rax, 48(%r12)
	leaq	-96(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %r14
	cmpq	%r14, %rdi
	je	.L322
	call	_ZdlPv@PLT
.L322:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L323
	movq	(%rdi), %rax
	call	*24(%rax)
.L323:
	movq	0(%r13), %rax
	movl	$56, %edi
	movq	%rax, -120(%rbp)
	call	_Znwm@PLT
	movq	48(%rbx), %rsi
	movq	56(%rbx), %r8
	movq	%rax, %rcx
	movl	$4, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%rcx)
	leaq	(%r8,%r8), %rdx
	movq	%rsi, %rax
	leaq	32(%rcx), %rdi
	addq	%rdx, %rax
	movq	%rdi, 16(%rcx)
	je	.L352
	testq	%rsi, %rsi
	je	.L324
.L352:
	movq	%rdx, %r9
	sarq	%r9
	cmpq	$14, %rdx
	ja	.L403
.L326:
	cmpq	$2, %rdx
	je	.L404
	testq	%rdx, %rdx
	je	.L329
	movq	%r9, -144(%rbp)
	movq	%r8, -136(%rbp)
	movq	%rcx, -128(%rbp)
	call	memmove@PLT
	movq	-128(%rbp), %rcx
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %r8
	movq	16(%rcx), %rdi
.L329:
	xorl	%esi, %esi
	movq	%r9, 24(%rcx)
	movw	%si, (%rdi,%r8,2)
	movq	80(%rbx), %rax
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	movq	%rcx, -104(%rbp)
	movq	%rax, 48(%rcx)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-120(%rbp), %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L330
	call	_ZdlPv@PLT
.L330:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L331
	movq	(%rdi), %rax
	call	*24(%rax)
.L331:
	movq	0(%r13), %rax
	movl	$56, %edi
	movq	%rax, -120(%rbp)
	call	_Znwm@PLT
	movq	88(%rbx), %rsi
	movq	96(%rbx), %r8
	movq	%rax, %rcx
	movl	$4, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%rcx)
	leaq	(%r8,%r8), %rdx
	movq	%rsi, %rax
	leaq	32(%rcx), %rdi
	addq	%rdx, %rax
	movq	%rdi, 16(%rcx)
	je	.L353
	testq	%rsi, %rsi
	je	.L324
.L353:
	movq	%rdx, %r9
	sarq	%r9
	cmpq	$14, %rdx
	ja	.L405
.L333:
	cmpq	$2, %rdx
	je	.L406
	testq	%rdx, %rdx
	je	.L335
	movq	%r9, -144(%rbp)
	movq	%r8, -136(%rbp)
	movq	%rcx, -128(%rbp)
	call	memmove@PLT
	movq	-128(%rbp), %rcx
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %r8
	movq	16(%rcx), %rdi
.L335:
	xorl	%edx, %edx
	movq	%r9, 24(%rcx)
	leaq	.LC4(%rip), %rsi
	movw	%dx, (%rdi,%r8,2)
	movq	120(%rbx), %rax
	movq	%r12, %rdi
	movq	%rcx, -104(%rbp)
	movq	%rax, 48(%rcx)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-120(%rbp), %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L336
	call	_ZdlPv@PLT
.L336:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L337
	movq	(%rdi), %rax
	call	*24(%rax)
.L337:
	cmpb	$0, 128(%rbx)
	jne	.L407
.L338:
	cmpb	$0, 176(%rbx)
	jne	.L408
.L345:
	cmpb	$0, 184(%rbx)
	jne	.L409
.L316:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L410
	addq	$120, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L402:
	.cfi_restore_state
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	16(%r12), %rdi
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L404:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	16(%rcx), %rdi
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L406:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	16(%rcx), %rdi
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L401:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r8
	ja	.L327
	leaq	2(%r15), %rdi
	movq	%r8, -136(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%rsi, -120(%rbp)
	call	_Znwm@PLT
	movq	-136(%rbp), %r8
	movq	-128(%rbp), %rcx
	movq	%rax, 16(%r12)
	movq	-120(%rbp), %rsi
	movq	%rax, %rdi
	movq	%r8, 32(%r12)
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L403:
	movabsq	$2305843009213693951, %rax
	movq	%r8, -160(%rbp)
	cmpq	%rax, %r9
	movq	%rsi, -152(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%r9, -136(%rbp)
	ja	.L327
	leaq	2(%rdx), %rdi
	movq	%rdx, -128(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	-128(%rbp), %rdx
	movq	-136(%rbp), %r9
	movq	-160(%rbp), %r8
	movq	%rax, %rdi
	movq	%rax, 16(%rcx)
	movq	-152(%rbp), %rsi
	movq	%r9, 32(%rcx)
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L405:
	movabsq	$2305843009213693951, %rax
	movq	%r8, -160(%rbp)
	cmpq	%rax, %r9
	movq	%rsi, -152(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%r9, -136(%rbp)
	ja	.L327
	leaq	2(%rdx), %rdi
	movq	%rdx, -128(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	-128(%rbp), %rdx
	movq	-136(%rbp), %r9
	movq	-160(%rbp), %r8
	movq	%rax, %rdi
	movq	%rax, 16(%rcx)
	movq	-152(%rbp), %rsi
	movq	%r9, 32(%rcx)
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L407:
	movq	0(%r13), %rax
	movl	$56, %edi
	movq	%rax, -120(%rbp)
	call	_Znwm@PLT
	movq	136(%rbx), %rsi
	movq	144(%rbx), %r8
	movq	%rax, %rcx
	movl	$4, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%rcx)
	leaq	(%r8,%r8), %rdx
	movq	%rsi, %rax
	leaq	32(%rcx), %rdi
	addq	%rdx, %rax
	movq	%rdi, 16(%rcx)
	je	.L354
	testq	%rsi, %rsi
	je	.L324
.L354:
	movq	%rdx, %r9
	sarq	%r9
	cmpq	$14, %rdx
	ja	.L411
.L340:
	cmpq	$2, %rdx
	je	.L412
	testq	%rdx, %rdx
	je	.L342
	movq	%r9, -144(%rbp)
	movq	%r8, -136(%rbp)
	movq	%rcx, -128(%rbp)
	call	memmove@PLT
	movq	-128(%rbp), %rcx
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %r8
	movq	16(%rcx), %rdi
.L342:
	xorl	%eax, %eax
	movq	%r9, 24(%rcx)
	leaq	.LC5(%rip), %rsi
	movw	%ax, (%rdi,%r8,2)
	movq	168(%rbx), %rax
	movq	%r12, %rdi
	movq	%rcx, -104(%rbp)
	movq	%rax, 48(%rcx)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-120(%rbp), %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L343
	call	_ZdlPv@PLT
.L343:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L338
	movq	(%rdi), %rax
	call	*24(%rax)
	cmpb	$0, 176(%rbx)
	je	.L345
.L408:
	movq	0(%r13), %r8
	movl	180(%rbx), %edx
	movl	$24, %edi
	movq	%r8, -128(%rbp)
	movl	%edx, -120(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	movq	%rdx, (%rax)
	movl	-120(%rbp), %edx
	movl	$2, 8(%rax)
	movl	%edx, 16(%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L346
	call	_ZdlPv@PLT
.L346:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L345
	movq	(%rdi), %rax
	call	*24(%rax)
	cmpb	$0, 184(%rbx)
	je	.L316
.L409:
	movq	0(%r13), %r8
	movl	188(%rbx), %ebx
	movl	$24, %edi
	movq	%r8, -120(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rdx
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	movq	%rdx, (%rax)
	movl	$2, 8(%rax)
	movl	%ebx, 16(%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-120(%rbp), %r8
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L349
	call	_ZdlPv@PLT
.L349:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L316
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L412:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	16(%rcx), %rdi
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L411:
	movabsq	$2305843009213693951, %rax
	movq	%r8, -160(%rbp)
	cmpq	%rax, %r9
	movq	%rsi, -152(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%r9, -136(%rbp)
	ja	.L327
	leaq	2(%rdx), %rdi
	movq	%rdx, -128(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	-128(%rbp), %rdx
	movq	-136(%rbp), %r9
	movq	-160(%rbp), %r8
	movq	%rax, %rdi
	movq	%rax, 16(%rcx)
	movq	-152(%rbp), %rsi
	movq	%r9, 32(%rcx)
	jmp	.L340
.L324:
	leaq	.LC10(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L410:
	call	__stack_chk_fail@PLT
.L327:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5592:
	.size	_ZNK12v8_inspector8protocol7Console14ConsoleMessage7toValueEv, .-_ZNK12v8_inspector8protocol7Console14ConsoleMessage7toValueEv
	.section	.text._ZN12v8_inspector8protocol7Console14ConsoleMessage17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Console14ConsoleMessage17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Console14ConsoleMessage17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Console14ConsoleMessage17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Console14ConsoleMessage17serializeToBinaryEv:
.LFB5530:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Console14ConsoleMessage7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L413
	movq	(%rdi), %rax
	call	*24(%rax)
.L413:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L420
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L420:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5530:
	.size	_ZN12v8_inspector8protocol7Console14ConsoleMessage17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Console14ConsoleMessage17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Console14ConsoleMessage15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Console14ConsoleMessage15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Console14ConsoleMessage15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Console14ConsoleMessage15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Console14ConsoleMessage15serializeToJSONEv:
.LFB5529:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Console14ConsoleMessage7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L421
	movq	(%rdi), %rax
	call	*24(%rax)
.L421:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L428
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L428:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5529:
	.size	_ZN12v8_inspector8protocol7Console14ConsoleMessage15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Console14ConsoleMessage15serializeToJSONEv
	.section	.rodata._ZN12v8_inspector8protocol7Console24MessageAddedNotification17serializeToBinaryEv.str1.1,"aMS",@progbits,1
.LC11:
	.string	"message"
	.section	.text._ZN12v8_inspector8protocol7Console24MessageAddedNotification17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Console24MessageAddedNotification17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Console24MessageAddedNotification17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Console24MessageAddedNotification17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Console24MessageAddedNotification17serializeToBinaryEv:
.LFB5561:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$96, %edi
	leaq	-80(%rbp), %r14
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	8(%rbx), %rsi
	leaq	-96(%rbp), %rdi
	call	_ZNK12v8_inspector8protocol7Console14ConsoleMessage7toValueEv
	movq	-96(%rbp), %rax
	leaq	.LC11(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	leaq	-88(%rbp), %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L430
	call	_ZdlPv@PLT
.L430:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L431
	movq	(%rdi), %rax
	call	*24(%rax)
.L431:
	movq	(%r12), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	*8(%rax)
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L437
	addq	$64, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L437:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5561:
	.size	_ZN12v8_inspector8protocol7Console24MessageAddedNotification17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Console24MessageAddedNotification17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Console24MessageAddedNotification15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Console24MessageAddedNotification15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Console24MessageAddedNotification15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Console24MessageAddedNotification15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Console24MessageAddedNotification15serializeToJSONEv:
.LFB5560:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$96, %edi
	leaq	-80(%rbp), %r14
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	8(%rbx), %rsi
	leaq	-96(%rbp), %rdi
	call	_ZNK12v8_inspector8protocol7Console14ConsoleMessage7toValueEv
	movq	-96(%rbp), %rax
	leaq	.LC11(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	leaq	-88(%rbp), %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L439
	call	_ZdlPv@PLT
.L439:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L440
	movq	(%rdi), %rax
	call	*24(%rax)
.L440:
	movq	(%r12), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	*(%rax)
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L446
	addq	$64, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L446:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5560:
	.size	_ZN12v8_inspector8protocol7Console24MessageAddedNotification15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Console24MessageAddedNotification15serializeToJSONEv
	.section	.text._ZNK12v8_inspector8protocol7Console14ConsoleMessage5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol7Console14ConsoleMessage5cloneEv
	.type	_ZNK12v8_inspector8protocol7Console14ConsoleMessage5cloneEv, @function
_ZNK12v8_inspector8protocol7Console14ConsoleMessage5cloneEv:
.LFB5593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol7Console14ConsoleMessage7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol7Console14ConsoleMessage9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L448
	movq	(%rdi), %rax
	call	*24(%rax)
.L448:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L454
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L454:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5593:
	.size	_ZNK12v8_inspector8protocol7Console14ConsoleMessage5cloneEv, .-_ZNK12v8_inspector8protocol7Console14ConsoleMessage5cloneEv
	.section	.text._ZN12v8_inspector8protocol7Console24MessageAddedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol7Console24MessageAddedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol7Console24MessageAddedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol7Console24MessageAddedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB5594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L456
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r12
	je	.L457
.L456:
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, 0(%r13)
.L455:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L500
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L457:
	.cfi_restore_state
	movl	$16, %edi
	leaq	16+_ZTVN12v8_inspector8protocol7Console24MessageAddedNotificationE(%rip), %rbx
	call	_Znwm@PLT
	cmpl	$6, 8(%r12)
	movq	%r14, %rdi
	movq	%rbx, (%rax)
	movq	%rax, %r15
	movq	$0, 8(%rax)
	movl	$0, %eax
	cmovne	%rax, %r12
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	-96(%rbp), %r8
	leaq	.LC11(%rip), %rsi
	movq	%r8, %rdi
	movq	%r8, -120(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-120(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r12
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L460
	call	_ZdlPv@PLT
.L460:
	leaq	.LC11(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%r12, %rsi
	leaq	-104(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZN12v8_inspector8protocol7Console14ConsoleMessage9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rax
	movq	8(%r15), %r12
	movq	$0, -104(%rbp)
	movq	%rax, 8(%r15)
	testq	%r12, %r12
	je	.L473
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Console14ConsoleMessageD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L462
	leaq	16+_ZTVN12v8_inspector8protocol7Console14ConsoleMessageE(%rip), %rax
	movq	136(%r12), %rdi
	movq	%rax, (%r12)
	leaq	152(%r12), %rax
	cmpq	%rax, %rdi
	je	.L463
	call	_ZdlPv@PLT
.L463:
	movq	88(%r12), %rdi
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L464
	call	_ZdlPv@PLT
.L464:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L465
	call	_ZdlPv@PLT
.L465:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L466
	call	_ZdlPv@PLT
.L466:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L467:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L473
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Console14ConsoleMessageD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L468
	leaq	16+_ZTVN12v8_inspector8protocol7Console14ConsoleMessageE(%rip), %rax
	movq	136(%r12), %rdi
	movq	%rax, (%r12)
	leaq	152(%r12), %rax
	cmpq	%rax, %rdi
	je	.L469
	call	_ZdlPv@PLT
.L469:
	movq	88(%r12), %rdi
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L470
	call	_ZdlPv@PLT
.L470:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L471
	call	_ZdlPv@PLT
.L471:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L472
	call	_ZdlPv@PLT
.L472:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L473:
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L501
	movq	%r15, 0(%r13)
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L501:
	movq	(%r15), %rax
	movq	$0, 0(%r13)
	leaq	_ZN12v8_inspector8protocol7Console24MessageAddedNotificationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L502
	movq	8(%r15), %r12
	movq	%rbx, (%r15)
	testq	%r12, %r12
	je	.L474
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Console14ConsoleMessageD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L475
	leaq	16+_ZTVN12v8_inspector8protocol7Console14ConsoleMessageE(%rip), %rax
	movq	136(%r12), %rdi
	movq	%rax, (%r12)
	leaq	152(%r12), %rax
	cmpq	%rax, %rdi
	je	.L476
	call	_ZdlPv@PLT
.L476:
	movq	88(%r12), %rdi
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L477
	call	_ZdlPv@PLT
.L477:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L478
	call	_ZdlPv@PLT
.L478:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L479
	call	_ZdlPv@PLT
.L479:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L474:
	movl	$16, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L502:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L468:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L462:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L475:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L474
.L500:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5594:
	.size	_ZN12v8_inspector8protocol7Console24MessageAddedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol7Console24MessageAddedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.text._ZNK12v8_inspector8protocol7Console24MessageAddedNotification7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol7Console24MessageAddedNotification7toValueEv
	.type	_ZNK12v8_inspector8protocol7Console24MessageAddedNotification7toValueEv, @function
_ZNK12v8_inspector8protocol7Console24MessageAddedNotification7toValueEv:
.LFB5595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$96, %edi
	leaq	-80(%rbp), %r14
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r12, 0(%r13)
	movq	8(%rbx), %rsi
	leaq	-88(%rbp), %rdi
	call	_ZNK12v8_inspector8protocol7Console14ConsoleMessage7toValueEv
	movq	-88(%rbp), %rax
	leaq	.LC11(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	leaq	-96(%rbp), %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L504
	call	_ZdlPv@PLT
.L504:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L503
	movq	(%rdi), %rax
	call	*24(%rax)
.L503:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L511
	addq	$64, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L511:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5595:
	.size	_ZNK12v8_inspector8protocol7Console24MessageAddedNotification7toValueEv, .-_ZNK12v8_inspector8protocol7Console24MessageAddedNotification7toValueEv
	.section	.text._ZNK12v8_inspector8protocol7Console24MessageAddedNotification5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol7Console24MessageAddedNotification5cloneEv
	.type	_ZNK12v8_inspector8protocol7Console24MessageAddedNotification5cloneEv, @function
_ZNK12v8_inspector8protocol7Console24MessageAddedNotification5cloneEv:
.LFB5596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-144(%rbp), %r14
	leaq	-96(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	8(%rbx), %rsi
	leaq	-152(%rbp), %rdi
	call	_ZNK12v8_inspector8protocol7Console14ConsoleMessage7toValueEv
	movq	-152(%rbp), %rax
	leaq	.LC11(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	leaq	-160(%rbp), %rdx
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L513
	call	_ZdlPv@PLT
.L513:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L514
	movq	(%rdi), %rax
	call	*24(%rax)
.L514:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol7Console24MessageAddedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L520
	addq	$120, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L520:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5596:
	.size	_ZNK12v8_inspector8protocol7Console24MessageAddedNotification5cloneEv, .-_ZNK12v8_inspector8protocol7Console24MessageAddedNotification5cloneEv
	.section	.rodata._ZN12v8_inspector8protocol7Console8Frontend12messageAddedESt10unique_ptrINS1_14ConsoleMessageESt14default_deleteIS4_EE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"Console.messageAdded"
	.section	.text._ZN12v8_inspector8protocol7Console8Frontend12messageAddedESt10unique_ptrINS1_14ConsoleMessageESt14default_deleteIS4_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol7Console8Frontend12messageAddedESt10unique_ptrINS1_14ConsoleMessageESt14default_deleteIS4_EE
	.type	_ZN12v8_inspector8protocol7Console8Frontend12messageAddedESt10unique_ptrINS1_14ConsoleMessageESt14default_deleteIS4_EE, @function
_ZN12v8_inspector8protocol7Console8Frontend12messageAddedESt10unique_ptrINS1_14ConsoleMessageESt14default_deleteIS4_EE:
.LFB5597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, (%rdi)
	je	.L521
	movq	%rsi, %r12
	movq	%rdi, %rbx
	leaq	-80(%rbp), %r13
	movl	$16, %edi
	call	_Znwm@PLT
	movq	(%r12), %rdx
	leaq	16+_ZTVN12v8_inspector8protocol7Console24MessageAddedNotificationE(%rip), %rcx
	movq	%r13, %rdi
	movq	$0, (%r12)
	movq	(%rbx), %r12
	leaq	.LC12(%rip), %rsi
	movq	%rcx, (%rax)
	movq	%rdx, 8(%rax)
	movq	(%r12), %rdx
	movq	%rax, -104(%rbp)
	movq	24(%rdx), %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	-96(%rbp), %rdi
	leaq	-104(%rbp), %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16InternalResponse18createNotificationERKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rax
	movq	%r12, %rdi
	leaq	-88(%rbp), %rsi
	movq	$0, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	*%rbx
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L523
	movq	(%rdi), %rax
	call	*24(%rax)
.L523:
	movq	-96(%rbp), %r12
	testq	%r12, %r12
	je	.L524
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol16InternalResponseD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L525
	movq	56(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L526
	movq	(%rdi), %rax
	call	*24(%rax)
.L526:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L527
	call	_ZdlPv@PLT
.L527:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L524:
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L528
	call	_ZdlPv@PLT
.L528:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L521
	movq	(%rdi), %rax
	call	*24(%rax)
.L521:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L544
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L525:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L524
.L544:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5597:
	.size	_ZN12v8_inspector8protocol7Console8Frontend12messageAddedESt10unique_ptrINS1_14ConsoleMessageESt14default_deleteIS4_EE, .-_ZN12v8_inspector8protocol7Console8Frontend12messageAddedESt10unique_ptrINS1_14ConsoleMessageESt14default_deleteIS4_EE
	.section	.text._ZN12v8_inspector8protocol7Console8Frontend5flushEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol7Console8Frontend5flushEv
	.type	_ZN12v8_inspector8protocol7Console8Frontend5flushEv, @function
_ZN12v8_inspector8protocol7Console8Frontend5flushEv:
.LFB5601:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.cfi_endproc
.LFE5601:
	.size	_ZN12v8_inspector8protocol7Console8Frontend5flushEv, .-_ZN12v8_inspector8protocol7Console8Frontend5flushEv
	.section	.text._ZN12v8_inspector8protocol7Console8Frontend23sendRawJSONNotificationENS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol7Console8Frontend23sendRawJSONNotificationENS_8String16E
	.type	_ZN12v8_inspector8protocol7Console8Frontend23sendRawJSONNotificationENS_8String16E, @function
_ZN12v8_inspector8protocol7Console8Frontend23sendRawJSONNotificationENS_8String16E:
.LFB5602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rsi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	movq	24(%rax), %r14
	movq	(%rsi), %rax
	cmpq	%rdx, %rax
	je	.L560
	movq	%rdx, (%rsi)
	xorl	%edx, %edx
	movq	16(%rsi), %rcx
	leaq	-64(%rbp), %r13
	movw	%dx, 16(%rsi)
	movq	32(%rsi), %rdx
	leaq	-112(%rbp), %rbx
	movq	8(%rsi), %rdi
	movq	%rcx, -112(%rbp)
	movq	$0, 8(%rsi)
	movq	%rdx, -96(%rbp)
	movq	%r13, -80(%rbp)
	cmpq	%rbx, %rax
	je	.L548
	movq	%rax, -80(%rbp)
	movq	%rcx, -64(%rbp)
.L550:
	movq	%rdi, -72(%rbp)
	xorl	%eax, %eax
	movl	$72, %edi
	movq	%rdx, -48(%rbp)
	movq	%rbx, -128(%rbp)
	movq	$0, -120(%rbp)
	movw	%ax, -112(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol23InternalRawNotificationE(%rip), %rcx
	leaq	24(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%rdx, 8(%rax)
	movq	-80(%rbp), %rdx
	cmpq	%r13, %rdx
	je	.L561
	movq	%rdx, 8(%rax)
	movq	-64(%rbp), %rdx
	movq	%rdx, 24(%rax)
.L552:
	movq	-72(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	$0, 64(%rax)
	movq	%rax, -136(%rbp)
	leaq	-136(%rbp), %rsi
	movq	%rdx, 16(%rax)
	movq	-48(%rbp), %rdx
	movups	%xmm0, 48(%rax)
	movq	%rdx, 40(%rax)
	call	*%r14
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L553
	movq	(%rdi), %rax
	call	*24(%rax)
.L553:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L546
	call	_ZdlPv@PLT
.L546:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L562
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L560:
	.cfi_restore_state
	movq	32(%rsi), %rdx
	xorl	%ecx, %ecx
	leaq	-64(%rbp), %r13
	movdqu	16(%rsi), %xmm1
	movq	8(%rsi), %rdi
	movw	%cx, 16(%rsi)
	leaq	-112(%rbp), %rbx
	movq	$0, 8(%rsi)
	movq	%rdx, -96(%rbp)
	movq	%r13, -80(%rbp)
	movaps	%xmm1, -112(%rbp)
.L548:
	movdqa	-112(%rbp), %xmm2
	movaps	%xmm2, -64(%rbp)
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L561:
	movdqa	-64(%rbp), %xmm3
	movups	%xmm3, 24(%rax)
	jmp	.L552
.L562:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5602:
	.size	_ZN12v8_inspector8protocol7Console8Frontend23sendRawJSONNotificationENS_8String16E, .-_ZN12v8_inspector8protocol7Console8Frontend23sendRawJSONNotificationENS_8String16E
	.section	.text._ZN12v8_inspector8protocol7Console8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol7Console8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE
	.type	_ZN12v8_inspector8protocol7Console8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE, @function
_ZN12v8_inspector8protocol7Console8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE:
.LFB5603:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r13
	movq	(%rsi), %rbx
	movl	$72, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %r15
	movq	8(%rsi), %r12
	movq	0(%r13), %rax
	movq	24(%rax), %r14
	movq	$0, 16(%rsi)
	movups	%xmm0, (%rsi)
	call	_Znwm@PLT
	movq	%r12, %xmm1
	movq	%rbx, %xmm0
	movq	%r13, %rdi
	leaq	24(%rax), %rdx
	punpcklqdq	%xmm1, %xmm0
	movq	%r15, 64(%rax)
	leaq	-64(%rbp), %rsi
	movq	%rdx, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol23InternalRawNotificationE(%rip), %rcx
	xorl	%edx, %edx
	movq	%rcx, (%rax)
	movq	$0, 16(%rax)
	movw	%dx, 24(%rax)
	movq	$0, 40(%rax)
	movq	%rax, -64(%rbp)
	movups	%xmm0, 48(%rax)
	call	*%r14
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L563
	movq	(%rdi), %rax
	call	*24(%rax)
.L563:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L570
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L570:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5603:
	.size	_ZN12v8_inspector8protocol7Console8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE, .-_ZN12v8_inspector8protocol7Console8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE
	.section	.text._ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol7Console14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m,"axG",@progbits,_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol7Console14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol7Console14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m
	.type	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol7Console14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m, @function
_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol7Console14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m:
.LFB9577:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax,%rsi,8), %r10
	testq	%r10, %r10
	je	.L588
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%r10), %rdx
	movl	$2147483648, %ebx
	movq	64(%rdx), %r8
	cmpq	%r8, %rcx
	je	.L591
	.p2align 4,,10
	.p2align 3
.L573:
	movq	(%rdx), %r9
	testq	%r9, %r9
	je	.L578
	movq	64(%r9), %r8
	movq	%rdx, %r10
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	8(%rdi)
	cmpq	%rdx, %rsi
	jne	.L578
	movq	%r9, %rdx
	cmpq	%r8, %rcx
	jne	.L573
.L591:
	movq	8(%r11), %r8
	movq	16(%rdx), %r15
	movq	8(%rdx), %r13
	movq	(%r11), %r14
	cmpq	%r15, %r8
	movq	%r15, %r9
	cmovbe	%r8, %r9
	testq	%r9, %r9
	je	.L574
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L575:
	movzwl	0(%r13,%rax,2), %r12d
	cmpw	%r12w, (%r14,%rax,2)
	jne	.L573
	addq	$1, %rax
	cmpq	%rax, %r9
	jne	.L575
.L574:
	subq	%r15, %r8
	cmpq	%rbx, %r8
	jge	.L573
	movabsq	$-2147483649, %rax
	cmpq	%rax, %r8
	jle	.L573
	testl	%r8d, %r8d
	jne	.L573
	popq	%rbx
	movq	%r10, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L578:
	.cfi_restore_state
	xorl	%r10d, %r10d
	popq	%rbx
	popq	%r12
	movq	%r10, %rax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L588:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movq	%r10, %rax
	ret
	.cfi_endproc
.LFE9577:
	.size	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol7Console14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m, .-_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol7Console14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m
	.section	.text.unlikely._ZN12v8_inspector8protocol7Console14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE,"ax",@progbits
	.align 2
.LCOLDB13:
	.section	.text._ZN12v8_inspector8protocol7Console14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE,"ax",@progbits
.LHOTB13:
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol7Console14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.type	_ZN12v8_inspector8protocol7Console14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE, @function
_ZN12v8_inspector8protocol7Console14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE:
.LFB5655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$72, %rdi
	subq	$104, %rsp
	movl	%esi, -132(%rbp)
	movq	32(%rdx), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	jne	.L593
	movq	(%rdx), %rax
	movq	8(%rdx), %rdx
	leaq	(%rax,%rdx,2), %rsi
	cmpq	%rsi, %rax
	je	.L596
	.p2align 4,,10
	.p2align 3
.L595:
	movq	%rcx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rcx, %rdx
	movsbq	-2(%rax), %rcx
	addq	%rdx, %rcx
	movq	%rcx, 32(%r14)
	cmpq	%rax, %rsi
	jne	.L595
	testq	%rcx, %rcx
	je	.L596
.L593:
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	80(%rbx)
	movq	%rdx, %rsi
	movq	%r14, %rdx
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol7Console14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m
	testq	%rax, %rax
	je	.L597
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	movq	%rdx, -144(%rbp)
	je	.L597
	leaq	-112(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	movq	-144(%rbp), %rdx
	movq	48(%rdx), %rax
	addq	56(%rdx), %rbx
	movq	%rbx, %rdi
	testb	$1, %al
	jne	.L620
.L598:
	movq	(%r15), %rdx
	movl	-132(%rbp), %esi
	movq	%r12, %r9
	leaq	-120(%rbp), %r8
	movq	$0, (%r15)
	movq	%r13, %rcx
	movq	%rdx, -120(%rbp)
	movq	%r14, %rdx
	call	*%rax
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L599
	movq	(%rdi), %rax
	call	*24(%rax)
.L599:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L621
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L620:
	.cfi_restore_state
	movq	(%rbx), %rdx
	movq	-1(%rdx,%rax), %rax
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L596:
	movq	$1, 32(%r14)
	movl	$1, %ecx
	jmp	.L593
.L621:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN12v8_inspector8protocol7Console14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.cfi_startproc
	.type	_ZN12v8_inspector8protocol7Console14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE.cold, @function
_ZN12v8_inspector8protocol7Console14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE.cold:
.LFSB5655:
.L597:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	-112(%rbp), %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	movq	48, %rax
	ud2
	.cfi_endproc
.LFE5655:
	.section	.text._ZN12v8_inspector8protocol7Console14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.size	_ZN12v8_inspector8protocol7Console14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE, .-_ZN12v8_inspector8protocol7Console14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.section	.text.unlikely._ZN12v8_inspector8protocol7Console14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.size	_ZN12v8_inspector8protocol7Console14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE.cold, .-_ZN12v8_inspector8protocol7Console14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE.cold
.LCOLDE13:
	.section	.text._ZN12v8_inspector8protocol7Console14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
.LHOTE13:
	.section	.text._ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol7Console14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_,"axG",@progbits,_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol7Console14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol7Console14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	.type	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol7Console14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_, @function
_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol7Console14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_:
.LFB7672:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	32(%rsi), %r12
	testq	%r12, %r12
	jne	.L623
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %rcx
	cmpq	%rcx, %rax
	je	.L626
	.p2align 4,,10
	.p2align 3
.L625:
	movq	%r12, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%r12, %rdx
	movq	%rdx, %r12
	movsbq	-2(%rax), %rdx
	addq	%rdx, %r12
	movq	%r12, 32(%rbx)
	cmpq	%rax, %rcx
	jne	.L625
	testq	%r12, %r12
	je	.L626
.L623:
	xorl	%edx, %edx
	movq	%r12, %rax
	movq	%r12, %rcx
	movq	%r13, %rdi
	divq	8(%r13)
	movq	%rdx, %r15
	movq	%rbx, %rdx
	movq	%r15, %rsi
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol7Console14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m
	testq	%rax, %rax
	je	.L627
	movq	(%rax), %rdx
	leaq	48(%rdx), %rax
	testq	%rdx, %rdx
	je	.L627
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L626:
	.cfi_restore_state
	movq	$1, 32(%rbx)
	movl	$1, %r12d
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L627:
	movl	$72, %edi
	call	_Znwm@PLT
	movq	(%rbx), %rdx
	movq	$0, (%rax)
	movq	%rax, %r14
	leaq	24(%rax), %rax
	movq	%rax, 8(%r14)
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdx
	je	.L664
	movq	%rdx, 8(%r14)
	movq	16(%rbx), %rdx
	movq	%rdx, 24(%r14)
.L630:
	movq	%rax, (%rbx)
	movq	8(%rbx), %rdx
	xorl	%eax, %eax
	leaq	32(%r13), %rdi
	movw	%ax, 16(%rbx)
	movq	32(%rbx), %rax
	movl	$1, %ecx
	movq	$0, 8(%rbx)
	movq	8(%r13), %rsi
	movq	%rdx, 16(%r14)
	movq	24(%r13), %rdx
	movq	%rax, 40(%r14)
	movq	$0, 48(%r14)
	movq	$0, 56(%r14)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %rbx
	testb	%al, %al
	jne	.L631
	movq	0(%r13), %r8
.L632:
	salq	$3, %r15
	movq	%r12, 64(%r14)
	leaq	(%r8,%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L641
	movq	(%rdx), %rdx
	movq	%rdx, (%r14)
	movq	(%rax), %rax
	movq	%r14, (%rax)
.L642:
	addq	$1, 24(%r13)
	addq	$24, %rsp
	leaq	48(%r14), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L631:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L665
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L666
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%r13), %r10
	movq	%rax, %r8
.L634:
	movq	16(%r13), %rsi
	movq	$0, 16(%r13)
	testq	%rsi, %rsi
	je	.L636
	xorl	%edi, %edi
	leaq	16(%r13), %r9
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L638:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L639:
	testq	%rsi, %rsi
	je	.L636
.L637:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	64(%rcx), %rax
	divq	%rbx
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L638
	movq	16(%r13), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r13)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L645
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L637
	.p2align 4,,10
	.p2align 3
.L636:
	movq	0(%r13), %rdi
	cmpq	%rdi, %r10
	je	.L640
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L640:
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%rbx, 8(%r13)
	divq	%rbx
	movq	%r8, 0(%r13)
	movq	%rdx, %r15
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L664:
	movdqu	16(%rbx), %xmm0
	movups	%xmm0, 24(%r14)
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L641:
	movq	16(%r13), %rdx
	movq	%r14, 16(%r13)
	movq	%rdx, (%r14)
	testq	%rdx, %rdx
	je	.L643
	movq	64(%rdx), %rax
	xorl	%edx, %edx
	divq	8(%r13)
	movq	%r14, (%r8,%rdx,8)
	movq	0(%r13), %rax
	addq	%r15, %rax
.L643:
	leaq	16(%r13), %rdx
	movq	%rdx, (%rax)
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L645:
	movq	%rdx, %rdi
	jmp	.L639
.L665:
	leaq	48(%r13), %r8
	movq	$0, 48(%r13)
	movq	%r8, %r10
	jmp	.L634
.L666:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE7672:
	.size	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol7Console14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_, .-_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol7Console14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	.section	.rodata._ZN12v8_inspector8protocol7Console10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"Console.clearMessages"
.LC16:
	.string	"Console.disable"
.LC17:
	.string	"Console.enable"
.LC18:
	.string	"Console"
	.section	.text._ZN12v8_inspector8protocol7Console10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol7Console10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE
	.type	_ZN12v8_inspector8protocol7Console10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE, @function
_ZN12v8_inspector8protocol7Console10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE:
.LFB5665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	8(%rdi), %r12
	movl	$192, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%r12, %rsi
	leaq	-96(%rbp), %r12
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN12v8_inspector8protocol14DispatcherBaseC2EPNS0_15FrontendChannelE@PLT
	leaq	16+_ZTVN12v8_inspector8protocol7Console14DispatcherImplE(%rip), %rax
	movq	%r12, %rdi
	movss	.LC14(%rip), %xmm0
	movq	%rax, (%r15)
	leaq	120(%r15), %rax
	leaq	72(%r15), %r14
	movq	%rax, 72(%r15)
	leaq	.LC15(%rip), %rsi
	leaq	176(%r15), %rax
	movq	%rax, 128(%r15)
	movq	%rbx, 184(%r15)
	leaq	-80(%rbp), %rbx
	movss	%xmm0, 104(%r15)
	movss	%xmm0, 160(%r15)
	movq	$1, 80(%r15)
	movq	$0, 88(%r15)
	movq	$0, 96(%r15)
	movq	$0, 112(%r15)
	movq	$0, 120(%r15)
	movq	$1, 136(%r15)
	movq	$0, 144(%r15)
	movq	$0, 152(%r15)
	movq	$0, 168(%r15)
	movq	$0, 176(%r15)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol7Console14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol7Console14DispatcherImpl13clearMessagesEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L668
	call	_ZdlPv@PLT
.L668:
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol7Console14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol7Console14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L669
	call	_ZdlPv@PLT
.L669:
	leaq	.LC17(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol7Console14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol7Console14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L670
	call	_ZdlPv@PLT
.L670:
	leaq	128(%r15), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol14UberDispatcher14setupRedirectsERKSt13unordered_mapINS_8String16ES3_St4hashIS3_ESt8equal_toIS3_ESaISt4pairIKS3_S3_EEE@PLT
	leaq	.LC18(%rip), %rsi
	movq	%r12, %rdi
	movq	%r15, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	leaq	-104(%rbp), %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol14UberDispatcher15registerBackendERKNS_8String16ESt10unique_ptrINS0_14DispatcherBaseESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L671
	call	_ZdlPv@PLT
.L671:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L667
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Console14DispatcherImplD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L673
	movq	144(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Console14DispatcherImplE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	jne	.L678
	.p2align 4,,10
	.p2align 3
.L674:
	movq	136(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%r12), %rdi
	leaq	176(%r12), %rax
	movq	$0, 152(%r12)
	movq	$0, 144(%r12)
	cmpq	%rax, %rdi
	je	.L679
	call	_ZdlPv@PLT
.L679:
	movq	88(%r12), %r13
	testq	%r13, %r13
	jne	.L683
	.p2align 4,,10
	.p2align 3
.L680:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L684
	call	_ZdlPv@PLT
.L684:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBaseD2Ev@PLT
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L667:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L708
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L709:
	.cfi_restore_state
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L674
.L677:
	movq	%rbx, %r13
.L678:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L675
	call	_ZdlPv@PLT
.L675:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L709
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L677
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L710:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L680
.L682:
	movq	%rbx, %r13
.L683:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	jne	.L710
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L682
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L673:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L667
.L708:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5665:
	.size	_ZN12v8_inspector8protocol7Console10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE, .-_ZN12v8_inspector8protocol7Console10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE
	.section	.text._ZN12v8_inspector8protocol7Console14DispatcherImpl11canDispatchERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol7Console14DispatcherImpl11canDispatchERKNS_8String16E
	.type	_ZN12v8_inspector8protocol7Console14DispatcherImpl11canDispatchERKNS_8String16E, @function
_ZN12v8_inspector8protocol7Console14DispatcherImpl11canDispatchERKNS_8String16E:
.LFB5654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	32(%rsi), %rcx
	movq	%rsi, %r8
	leaq	72(%rdi), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rcx, %rcx
	jne	.L712
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %rsi
	cmpq	%rsi, %rax
	je	.L715
	.p2align 4,,10
	.p2align 3
.L714:
	movq	%rcx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rcx, %rdx
	movsbq	-2(%rax), %rcx
	addq	%rdx, %rcx
	movq	%rcx, 32(%r8)
	cmpq	%rax, %rsi
	jne	.L714
	testq	%rcx, %rcx
	je	.L715
.L712:
	movq	%rcx, %rax
	xorl	%edx, %edx
	divq	80(%rdi)
	movq	%r9, %rdi
	movq	%rdx, %rsi
	movq	%r8, %rdx
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol7Console14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L711
	cmpq	$0, (%rax)
	setne	%r8b
.L711:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L715:
	.cfi_restore_state
	movq	$1, 32(%r8)
	movl	$1, %ecx
	jmp	.L712
	.cfi_endproc
.LFE5654:
	.size	_ZN12v8_inspector8protocol7Console14DispatcherImpl11canDispatchERKNS_8String16E, .-_ZN12v8_inspector8protocol7Console14DispatcherImpl11canDispatchERKNS_8String16E
	.weak	_ZTVN12v8_inspector8protocol23InternalRawNotificationE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol23InternalRawNotificationE,"awG",@progbits,_ZTVN12v8_inspector8protocol23InternalRawNotificationE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol23InternalRawNotificationE, @object
	.size	_ZTVN12v8_inspector8protocol23InternalRawNotificationE, 48
_ZTVN12v8_inspector8protocol23InternalRawNotificationE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol23InternalRawNotificationD1Ev
	.quad	_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Console14ConsoleMessageE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Console14ConsoleMessageE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Console14ConsoleMessageE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Console14ConsoleMessageE, @object
	.size	_ZTVN12v8_inspector8protocol7Console14ConsoleMessageE, 48
_ZTVN12v8_inspector8protocol7Console14ConsoleMessageE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Console14ConsoleMessage15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Console14ConsoleMessage17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Console14ConsoleMessageD1Ev
	.quad	_ZN12v8_inspector8protocol7Console14ConsoleMessageD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Console24MessageAddedNotificationE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Console24MessageAddedNotificationE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Console24MessageAddedNotificationE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Console24MessageAddedNotificationE, @object
	.size	_ZTVN12v8_inspector8protocol7Console24MessageAddedNotificationE, 48
_ZTVN12v8_inspector8protocol7Console24MessageAddedNotificationE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Console24MessageAddedNotification15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Console24MessageAddedNotification17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Console24MessageAddedNotificationD1Ev
	.quad	_ZN12v8_inspector8protocol7Console24MessageAddedNotificationD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Console14DispatcherImplE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Console14DispatcherImplE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Console14DispatcherImplE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Console14DispatcherImplE, @object
	.size	_ZTVN12v8_inspector8protocol7Console14DispatcherImplE, 48
_ZTVN12v8_inspector8protocol7Console14DispatcherImplE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Console14DispatcherImplD1Ev
	.quad	_ZN12v8_inspector8protocol7Console14DispatcherImplD0Ev
	.quad	_ZN12v8_inspector8protocol7Console14DispatcherImpl11canDispatchERKNS_8String16E
	.quad	_ZN12v8_inspector8protocol7Console14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.globl	_ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum4InfoE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC19:
	.string	"info"
	.section	.data.rel.local._ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum4InfoE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum4InfoE, @object
	.size	_ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum4InfoE, 8
_ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum4InfoE:
	.quad	.LC19
	.globl	_ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum5DebugE
	.section	.rodata.str1.1
.LC20:
	.string	"debug"
	.section	.data.rel.local._ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum5DebugE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum5DebugE, @object
	.size	_ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum5DebugE, 8
_ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum5DebugE:
	.quad	.LC20
	.globl	_ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum5ErrorE
	.section	.rodata.str1.1
.LC21:
	.string	"error"
	.section	.data.rel.local._ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum5ErrorE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum5ErrorE, @object
	.size	_ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum5ErrorE, 8
_ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum5ErrorE:
	.quad	.LC21
	.globl	_ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum7WarningE
	.section	.rodata.str1.1
.LC22:
	.string	"warning"
	.section	.data.rel.local._ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum7WarningE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum7WarningE, @object
	.size	_ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum7WarningE, 8
_ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum7WarningE:
	.quad	.LC22
	.globl	_ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum3LogE
	.section	.rodata.str1.1
.LC23:
	.string	"log"
	.section	.data.rel.local._ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum3LogE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum3LogE, @object
	.size	_ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum3LogE, 8
_ZN12v8_inspector8protocol7Console14ConsoleMessage9LevelEnum3LogE:
	.quad	.LC23
	.globl	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum6WorkerE
	.section	.rodata.str1.1
.LC24:
	.string	"worker"
	.section	.data.rel.local._ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum6WorkerE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum6WorkerE, @object
	.size	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum6WorkerE, 8
_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum6WorkerE:
	.quad	.LC24
	.globl	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum11DeprecationE
	.section	.rodata.str1.1
.LC25:
	.string	"deprecation"
	.section	.data.rel.local._ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum11DeprecationE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum11DeprecationE, @object
	.size	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum11DeprecationE, 8
_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum11DeprecationE:
	.quad	.LC25
	.globl	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum5OtherE
	.section	.rodata.str1.1
.LC26:
	.string	"other"
	.section	.data.rel.local._ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum5OtherE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum5OtherE, @object
	.size	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum5OtherE, 8
_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum5OtherE:
	.quad	.LC26
	.globl	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum8SecurityE
	.section	.rodata.str1.1
.LC27:
	.string	"security"
	.section	.data.rel.local._ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum8SecurityE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum8SecurityE, @object
	.size	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum8SecurityE, 8
_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum8SecurityE:
	.quad	.LC27
	.globl	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum9RenderingE
	.section	.rodata.str1.1
.LC28:
	.string	"rendering"
	.section	.data.rel.local._ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum9RenderingE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum9RenderingE, @object
	.size	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum9RenderingE, 8
_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum9RenderingE:
	.quad	.LC28
	.globl	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum8AppcacheE
	.section	.rodata.str1.1
.LC29:
	.string	"appcache"
	.section	.data.rel.local._ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum8AppcacheE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum8AppcacheE, @object
	.size	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum8AppcacheE, 8
_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum8AppcacheE:
	.quad	.LC29
	.globl	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum7StorageE
	.section	.rodata.str1.1
.LC30:
	.string	"storage"
	.section	.data.rel.local._ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum7StorageE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum7StorageE, @object
	.size	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum7StorageE, 8
_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum7StorageE:
	.quad	.LC30
	.globl	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum10ConsoleApiE
	.section	.rodata.str1.1
.LC31:
	.string	"console-api"
	.section	.data.rel.local._ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum10ConsoleApiE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum10ConsoleApiE, @object
	.size	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum10ConsoleApiE, 8
_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum10ConsoleApiE:
	.quad	.LC31
	.globl	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum7NetworkE
	.section	.rodata.str1.1
.LC32:
	.string	"network"
	.section	.data.rel.local._ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum7NetworkE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum7NetworkE, @object
	.size	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum7NetworkE, 8
_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum7NetworkE:
	.quad	.LC32
	.globl	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum10JavascriptE
	.section	.rodata.str1.1
.LC33:
	.string	"javascript"
	.section	.data.rel.local._ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum10JavascriptE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum10JavascriptE, @object
	.size	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum10JavascriptE, 8
_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum10JavascriptE:
	.quad	.LC33
	.globl	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum3XmlE
	.section	.rodata.str1.1
.LC34:
	.string	"xml"
	.section	.data.rel.local._ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum3XmlE,"aw"
	.align 8
	.type	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum3XmlE, @object
	.size	_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum3XmlE, 8
_ZN12v8_inspector8protocol7Console14ConsoleMessage10SourceEnum3XmlE:
	.quad	.LC34
	.globl	_ZN12v8_inspector8protocol7Console8Metainfo7versionE
	.section	.rodata._ZN12v8_inspector8protocol7Console8Metainfo7versionE,"a"
	.type	_ZN12v8_inspector8protocol7Console8Metainfo7versionE, @object
	.size	_ZN12v8_inspector8protocol7Console8Metainfo7versionE, 4
_ZN12v8_inspector8protocol7Console8Metainfo7versionE:
	.string	"1.3"
	.globl	_ZN12v8_inspector8protocol7Console8Metainfo13commandPrefixE
	.section	.rodata._ZN12v8_inspector8protocol7Console8Metainfo13commandPrefixE,"a"
	.align 8
	.type	_ZN12v8_inspector8protocol7Console8Metainfo13commandPrefixE, @object
	.size	_ZN12v8_inspector8protocol7Console8Metainfo13commandPrefixE, 9
_ZN12v8_inspector8protocol7Console8Metainfo13commandPrefixE:
	.string	"Console."
	.globl	_ZN12v8_inspector8protocol7Console8Metainfo10domainNameE
	.section	.rodata._ZN12v8_inspector8protocol7Console8Metainfo10domainNameE,"a"
	.align 8
	.type	_ZN12v8_inspector8protocol7Console8Metainfo10domainNameE, @object
	.size	_ZN12v8_inspector8protocol7Console8Metainfo10domainNameE, 8
_ZN12v8_inspector8protocol7Console8Metainfo10domainNameE:
	.string	"Console"
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC14:
	.long	1065353216
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
