	.file	"debug-support.cc"
	.text
	.section	.text.startup._GLOBAL__sub_I_v8dbg_frametype_EntryFrame,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I_v8dbg_frametype_EntryFrame, @function
_GLOBAL__sub_I_v8dbg_frametype_EntryFrame:
.LFB21901:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21901:
	.size	_GLOBAL__sub_I_v8dbg_frametype_EntryFrame, .-_GLOBAL__sub_I_v8dbg_frametype_EntryFrame
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I_v8dbg_frametype_EntryFrame
	.globl	v8dbg_class_Symbol__name__Object
	.section	.data.v8dbg_class_Symbol__name__Object,"aw"
	.align 4
	.type	v8dbg_class_Symbol__name__Object, @object
	.size	v8dbg_class_Symbol__name__Object, 4
v8dbg_class_Symbol__name__Object:
	.long	16
	.globl	v8dbg_class_ThinString__actual__String
	.section	.data.v8dbg_class_ThinString__actual__String,"aw"
	.align 4
	.type	v8dbg_class_ThinString__actual__String, @object
	.size	v8dbg_class_ThinString__actual__String, 4
v8dbg_class_ThinString__actual__String:
	.long	16
	.globl	v8dbg_class_SlicedString__offset__SMI
	.section	.data.v8dbg_class_SlicedString__offset__SMI,"aw"
	.align 4
	.type	v8dbg_class_SlicedString__offset__SMI, @object
	.size	v8dbg_class_SlicedString__offset__SMI, 4
v8dbg_class_SlicedString__offset__SMI:
	.long	24
	.globl	v8dbg_class_ConsString__second__String
	.section	.data.v8dbg_class_ConsString__second__String,"aw"
	.align 4
	.type	v8dbg_class_ConsString__second__String, @object
	.size	v8dbg_class_ConsString__second__String, 4
v8dbg_class_ConsString__second__String:
	.long	24
	.globl	v8dbg_class_ConsString__first__String
	.section	.data.v8dbg_class_ConsString__first__String,"aw"
	.align 4
	.type	v8dbg_class_ConsString__first__String, @object
	.size	v8dbg_class_ConsString__first__String, 4
v8dbg_class_ConsString__first__String:
	.long	16
	.globl	v8dbg_class_DescriptorArray__header_size__uintptr_t
	.section	.data.v8dbg_class_DescriptorArray__header_size__uintptr_t,"aw"
	.align 4
	.type	v8dbg_class_DescriptorArray__header_size__uintptr_t, @object
	.size	v8dbg_class_DescriptorArray__header_size__uintptr_t, 4
v8dbg_class_DescriptorArray__header_size__uintptr_t:
	.long	24
	.globl	v8dbg_class_String__length__int32_t
	.section	.data.v8dbg_class_String__length__int32_t,"aw"
	.align 4
	.type	v8dbg_class_String__length__int32_t, @object
	.size	v8dbg_class_String__length__int32_t, 4
v8dbg_class_String__length__int32_t:
	.long	12
	.globl	v8dbg_class_Code__instruction_size__int
	.section	.data.v8dbg_class_Code__instruction_size__int,"aw"
	.align 4
	.type	v8dbg_class_Code__instruction_size__int, @object
	.size	v8dbg_class_Code__instruction_size__int, 4
v8dbg_class_Code__instruction_size__int:
	.long	40
	.globl	v8dbg_class_Code__instruction_start__uintptr_t
	.section	.data.v8dbg_class_Code__instruction_start__uintptr_t,"aw"
	.align 4
	.type	v8dbg_class_Code__instruction_start__uintptr_t, @object
	.size	v8dbg_class_Code__instruction_start__uintptr_t, 4
v8dbg_class_Code__instruction_start__uintptr_t:
	.long	64
	.globl	v8dbg_class_SlicedString__parent__String
	.section	.data.v8dbg_class_SlicedString__parent__String,"aw"
	.align 4
	.type	v8dbg_class_SlicedString__parent__String, @object
	.size	v8dbg_class_SlicedString__parent__String, 4
v8dbg_class_SlicedString__parent__String:
	.long	16
	.globl	v8dbg_class_SharedFunctionInfo__length__uint16_t
	.section	.data.v8dbg_class_SharedFunctionInfo__length__uint16_t,"aw"
	.align 4
	.type	v8dbg_class_SharedFunctionInfo__length__uint16_t, @object
	.size	v8dbg_class_SharedFunctionInfo__length__uint16_t, 4
v8dbg_class_SharedFunctionInfo__length__uint16_t:
	.long	40
	.globl	v8dbg_class_SharedFunctionInfo__flags__int
	.section	.data.v8dbg_class_SharedFunctionInfo__flags__int,"aw"
	.align 4
	.type	v8dbg_class_SharedFunctionInfo__flags__int, @object
	.size	v8dbg_class_SharedFunctionInfo__flags__int, 4
v8dbg_class_SharedFunctionInfo__flags__int:
	.long	48
	.globl	v8dbg_class_SharedFunctionInfo__internal_formal_parameter_count__uint16_t
	.section	.data.v8dbg_class_SharedFunctionInfo__internal_formal_parameter_count__uint16_t,"aw"
	.align 4
	.type	v8dbg_class_SharedFunctionInfo__internal_formal_parameter_count__uint16_t, @object
	.size	v8dbg_class_SharedFunctionInfo__internal_formal_parameter_count__uint16_t, 4
v8dbg_class_SharedFunctionInfo__internal_formal_parameter_count__uint16_t:
	.long	42
	.globl	v8dbg_class_SharedFunctionInfo__raw_function_token_offset__int16_t
	.section	.data.v8dbg_class_SharedFunctionInfo__raw_function_token_offset__int16_t,"aw"
	.align 4
	.type	v8dbg_class_SharedFunctionInfo__raw_function_token_offset__int16_t, @object
	.size	v8dbg_class_SharedFunctionInfo__raw_function_token_offset__int16_t, 4
v8dbg_class_SharedFunctionInfo__raw_function_token_offset__int16_t:
	.long	46
	.globl	v8dbg_class_UncompiledData__end_position__int32_t
	.section	.data.v8dbg_class_UncompiledData__end_position__int32_t,"aw"
	.align 4
	.type	v8dbg_class_UncompiledData__end_position__int32_t, @object
	.size	v8dbg_class_UncompiledData__end_position__int32_t, 4
v8dbg_class_UncompiledData__end_position__int32_t:
	.long	20
	.globl	v8dbg_class_UncompiledData__start_position__int32_t
	.section	.data.v8dbg_class_UncompiledData__start_position__int32_t,"aw"
	.align 4
	.type	v8dbg_class_UncompiledData__start_position__int32_t, @object
	.size	v8dbg_class_UncompiledData__start_position__int32_t, 4
v8dbg_class_UncompiledData__start_position__int32_t:
	.long	16
	.globl	v8dbg_class_SeqTwoByteString__chars__char
	.section	.data.v8dbg_class_SeqTwoByteString__chars__char,"aw"
	.align 4
	.type	v8dbg_class_SeqTwoByteString__chars__char, @object
	.size	v8dbg_class_SeqTwoByteString__chars__char, 4
v8dbg_class_SeqTwoByteString__chars__char:
	.long	16
	.globl	v8dbg_class_SeqOneByteString__chars__char
	.section	.data.v8dbg_class_SeqOneByteString__chars__char,"aw"
	.align 4
	.type	v8dbg_class_SeqOneByteString__chars__char, @object
	.size	v8dbg_class_SeqOneByteString__chars__char, 4
v8dbg_class_SeqOneByteString__chars__char:
	.long	16
	.globl	v8dbg_class_ExternalString__resource__Object
	.section	.data.v8dbg_class_ExternalString__resource__Object,"aw"
	.align 4
	.type	v8dbg_class_ExternalString__resource__Object, @object
	.size	v8dbg_class_ExternalString__resource__Object, 4
v8dbg_class_ExternalString__resource__Object:
	.long	16
	.globl	v8dbg_class_HeapNumber__value__double
	.section	.data.v8dbg_class_HeapNumber__value__double,"aw"
	.align 4
	.type	v8dbg_class_HeapNumber__value__double, @object
	.size	v8dbg_class_HeapNumber__value__double, 4
v8dbg_class_HeapNumber__value__double:
	.long	8
	.globl	v8dbg_class_Oddball__kind_offset__int
	.section	.data.v8dbg_class_Oddball__kind_offset__int,"aw"
	.align 4
	.type	v8dbg_class_Oddball__kind_offset__int, @object
	.size	v8dbg_class_Oddball__kind_offset__int, 4
v8dbg_class_Oddball__kind_offset__int:
	.long	40
	.globl	v8dbg_class_Map__prototype__Object
	.section	.data.v8dbg_class_Map__prototype__Object,"aw"
	.align 4
	.type	v8dbg_class_Map__prototype__Object, @object
	.size	v8dbg_class_Map__prototype__Object, 4
v8dbg_class_Map__prototype__Object:
	.long	24
	.globl	v8dbg_class_Map__bit_field3__int
	.section	.data.v8dbg_class_Map__bit_field3__int,"aw"
	.align 4
	.type	v8dbg_class_Map__bit_field3__int, @object
	.size	v8dbg_class_Map__bit_field3__int, 4
v8dbg_class_Map__bit_field3__int:
	.long	16
	.globl	v8dbg_class_Map__bit_field2__char
	.section	.data.v8dbg_class_Map__bit_field2__char,"aw"
	.align 4
	.type	v8dbg_class_Map__bit_field2__char, @object
	.size	v8dbg_class_Map__bit_field2__char, 4
v8dbg_class_Map__bit_field2__char:
	.long	15
	.globl	v8dbg_class_Map__bit_field__char
	.section	.data.v8dbg_class_Map__bit_field__char,"aw"
	.align 4
	.type	v8dbg_class_Map__bit_field__char, @object
	.size	v8dbg_class_Map__bit_field__char, 4
v8dbg_class_Map__bit_field__char:
	.long	14
	.globl	v8dbg_class_Map__instance_type__uint16_t
	.section	.data.v8dbg_class_Map__instance_type__uint16_t,"aw"
	.align 4
	.type	v8dbg_class_Map__instance_type__uint16_t, @object
	.size	v8dbg_class_Map__instance_type__uint16_t, 4
v8dbg_class_Map__instance_type__uint16_t:
	.long	12
	.globl	v8dbg_class_Map__inobject_properties_start_or_constructor_function_index__char
	.section	.data.v8dbg_class_Map__inobject_properties_start_or_constructor_function_index__char,"aw"
	.align 4
	.type	v8dbg_class_Map__inobject_properties_start_or_constructor_function_index__char, @object
	.size	v8dbg_class_Map__inobject_properties_start_or_constructor_function_index__char, 4
v8dbg_class_Map__inobject_properties_start_or_constructor_function_index__char:
	.long	9
	.globl	v8dbg_class_Map__instance_size_in_words__char
	.section	.data.v8dbg_class_Map__instance_size_in_words__char,"aw"
	.align 4
	.type	v8dbg_class_Map__instance_size_in_words__char, @object
	.size	v8dbg_class_Map__instance_size_in_words__char, 4
v8dbg_class_Map__instance_size_in_words__char:
	.long	8
	.globl	v8dbg_class_JSTypedArray__length__Object
	.section	.data.v8dbg_class_JSTypedArray__length__Object,"aw"
	.align 4
	.type	v8dbg_class_JSTypedArray__length__Object, @object
	.size	v8dbg_class_JSTypedArray__length__Object, 4
v8dbg_class_JSTypedArray__length__Object:
	.long	48
	.globl	v8dbg_class_JSTypedArray__external_pointer__uintptr_t
	.section	.data.v8dbg_class_JSTypedArray__external_pointer__uintptr_t,"aw"
	.align 4
	.type	v8dbg_class_JSTypedArray__external_pointer__uintptr_t, @object
	.size	v8dbg_class_JSTypedArray__external_pointer__uintptr_t, 4
v8dbg_class_JSTypedArray__external_pointer__uintptr_t:
	.long	56
	.globl	v8dbg_class_JSRegExp__source__Object
	.section	.data.v8dbg_class_JSRegExp__source__Object,"aw"
	.align 4
	.type	v8dbg_class_JSRegExp__source__Object, @object
	.size	v8dbg_class_JSRegExp__source__Object, 4
v8dbg_class_JSRegExp__source__Object:
	.long	32
	.globl	v8dbg_class_JSDate__value__Object
	.section	.data.v8dbg_class_JSDate__value__Object,"aw"
	.align 4
	.type	v8dbg_class_JSDate__value__Object, @object
	.size	v8dbg_class_JSDate__value__Object, 4
v8dbg_class_JSDate__value__Object:
	.long	24
	.globl	v8dbg_class_JSArrayBufferView__byte_offset__size_t
	.section	.data.v8dbg_class_JSArrayBufferView__byte_offset__size_t,"aw"
	.align 4
	.type	v8dbg_class_JSArrayBufferView__byte_offset__size_t, @object
	.size	v8dbg_class_JSArrayBufferView__byte_offset__size_t, 4
v8dbg_class_JSArrayBufferView__byte_offset__size_t:
	.long	32
	.globl	v8dbg_class_JSArrayBufferView__byte_length__size_t
	.section	.data.v8dbg_class_JSArrayBufferView__byte_length__size_t,"aw"
	.align 4
	.type	v8dbg_class_JSArrayBufferView__byte_length__size_t, @object
	.size	v8dbg_class_JSArrayBufferView__byte_length__size_t, 4
v8dbg_class_JSArrayBufferView__byte_length__size_t:
	.long	40
	.globl	v8dbg_class_JSArrayBuffer__byte_length__size_t
	.section	.data.v8dbg_class_JSArrayBuffer__byte_length__size_t,"aw"
	.align 4
	.type	v8dbg_class_JSArrayBuffer__byte_length__size_t, @object
	.size	v8dbg_class_JSArrayBuffer__byte_length__size_t, 4
v8dbg_class_JSArrayBuffer__byte_length__size_t:
	.long	24
	.globl	v8dbg_class_JSArrayBuffer__backing_store__uintptr_t
	.section	.data.v8dbg_class_JSArrayBuffer__backing_store__uintptr_t,"aw"
	.align 4
	.type	v8dbg_class_JSArrayBuffer__backing_store__uintptr_t, @object
	.size	v8dbg_class_JSArrayBuffer__backing_store__uintptr_t, 4
v8dbg_class_JSArrayBuffer__backing_store__uintptr_t:
	.long	32
	.globl	v8dbg_class_FixedArray__data__uintptr_t
	.section	.data.v8dbg_class_FixedArray__data__uintptr_t,"aw"
	.align 4
	.type	v8dbg_class_FixedArray__data__uintptr_t, @object
	.size	v8dbg_class_FixedArray__data__uintptr_t, 4
v8dbg_class_FixedArray__data__uintptr_t:
	.long	16
	.globl	v8dbg_class_JSObject__internal_fields__uintptr_t
	.section	.data.v8dbg_class_JSObject__internal_fields__uintptr_t,"aw"
	.align 4
	.type	v8dbg_class_JSObject__internal_fields__uintptr_t, @object
	.size	v8dbg_class_JSObject__internal_fields__uintptr_t, 4
v8dbg_class_JSObject__internal_fields__uintptr_t:
	.long	24
	.globl	v8dbg_class_JSObject__elements__Object
	.section	.data.v8dbg_class_JSObject__elements__Object,"aw"
	.align 4
	.type	v8dbg_class_JSObject__elements__Object, @object
	.size	v8dbg_class_JSObject__elements__Object, 4
v8dbg_class_JSObject__elements__Object:
	.long	16
	.globl	v8dbg_class_HeapObject__map__Map
	.section	.bss.v8dbg_class_HeapObject__map__Map,"aw",@nobits
	.align 4
	.type	v8dbg_class_HeapObject__map__Map, @object
	.size	v8dbg_class_HeapObject__map__Map, 4
v8dbg_class_HeapObject__map__Map:
	.zero	4
	.globl	v8dbg_class_JSFunction__shared__SharedFunctionInfo
	.section	.data.v8dbg_class_JSFunction__shared__SharedFunctionInfo,"aw"
	.align 4
	.type	v8dbg_class_JSFunction__shared__SharedFunctionInfo, @object
	.size	v8dbg_class_JSFunction__shared__SharedFunctionInfo, 4
v8dbg_class_JSFunction__shared__SharedFunctionInfo:
	.long	24
	.globl	v8dbg_class_JSFunction__context__Context
	.section	.data.v8dbg_class_JSFunction__context__Context,"aw"
	.align 4
	.type	v8dbg_class_JSFunction__context__Context, @object
	.size	v8dbg_class_JSFunction__context__Context, 4
v8dbg_class_JSFunction__context__Context:
	.long	32
	.globl	v8dbg_class_AccessorPair__setter__Object
	.section	.data.v8dbg_class_AccessorPair__setter__Object,"aw"
	.align 4
	.type	v8dbg_class_AccessorPair__setter__Object, @object
	.size	v8dbg_class_AccessorPair__setter__Object, 4
v8dbg_class_AccessorPair__setter__Object:
	.long	16
	.globl	v8dbg_class_AccessorPair__getter__Object
	.section	.data.v8dbg_class_AccessorPair__getter__Object,"aw"
	.align 4
	.type	v8dbg_class_AccessorPair__getter__Object, @object
	.size	v8dbg_class_AccessorPair__getter__Object, 4
v8dbg_class_AccessorPair__getter__Object:
	.long	8
	.globl	v8dbg_class_SharedFunctionInfo__raw_outer_scope_info_or_feedback_metadata__HeapObject
	.section	.data.v8dbg_class_SharedFunctionInfo__raw_outer_scope_info_or_feedback_metadata__HeapObject,"aw"
	.align 4
	.type	v8dbg_class_SharedFunctionInfo__raw_outer_scope_info_or_feedback_metadata__HeapObject, @object
	.size	v8dbg_class_SharedFunctionInfo__raw_outer_scope_info_or_feedback_metadata__HeapObject, 4
v8dbg_class_SharedFunctionInfo__raw_outer_scope_info_or_feedback_metadata__HeapObject:
	.long	24
	.globl	v8dbg_class_SharedFunctionInfo__script_or_debug_info__Object
	.section	.data.v8dbg_class_SharedFunctionInfo__script_or_debug_info__Object,"aw"
	.align 4
	.type	v8dbg_class_SharedFunctionInfo__script_or_debug_info__Object, @object
	.size	v8dbg_class_SharedFunctionInfo__script_or_debug_info__Object, 4
v8dbg_class_SharedFunctionInfo__script_or_debug_info__Object:
	.long	32
	.globl	v8dbg_class_SharedFunctionInfo__name_or_scope_info__Object
	.section	.data.v8dbg_class_SharedFunctionInfo__name_or_scope_info__Object,"aw"
	.align 4
	.type	v8dbg_class_SharedFunctionInfo__name_or_scope_info__Object, @object
	.size	v8dbg_class_SharedFunctionInfo__name_or_scope_info__Object, 4
v8dbg_class_SharedFunctionInfo__name_or_scope_info__Object:
	.long	16
	.globl	v8dbg_class_InterpreterData__interpreter_trampoline__Code
	.section	.data.v8dbg_class_InterpreterData__interpreter_trampoline__Code,"aw"
	.align 4
	.type	v8dbg_class_InterpreterData__interpreter_trampoline__Code, @object
	.size	v8dbg_class_InterpreterData__interpreter_trampoline__Code, 4
v8dbg_class_InterpreterData__interpreter_trampoline__Code:
	.long	16
	.globl	v8dbg_class_InterpreterData__bytecode_array__BytecodeArray
	.section	.data.v8dbg_class_InterpreterData__bytecode_array__BytecodeArray,"aw"
	.align 4
	.type	v8dbg_class_InterpreterData__bytecode_array__BytecodeArray, @object
	.size	v8dbg_class_InterpreterData__bytecode_array__BytecodeArray, 4
v8dbg_class_InterpreterData__bytecode_array__BytecodeArray:
	.long	8
	.globl	v8dbg_class_UncompiledDataWithPreparseData__preparse_data__PreparseData
	.section	.data.v8dbg_class_UncompiledDataWithPreparseData__preparse_data__PreparseData,"aw"
	.align 4
	.type	v8dbg_class_UncompiledDataWithPreparseData__preparse_data__PreparseData, @object
	.size	v8dbg_class_UncompiledDataWithPreparseData__preparse_data__PreparseData, 4
v8dbg_class_UncompiledDataWithPreparseData__preparse_data__PreparseData:
	.long	24
	.globl	v8dbg_class_UncompiledData__inferred_name__String
	.section	.data.v8dbg_class_UncompiledData__inferred_name__String,"aw"
	.align 4
	.type	v8dbg_class_UncompiledData__inferred_name__String, @object
	.size	v8dbg_class_UncompiledData__inferred_name__String, 4
v8dbg_class_UncompiledData__inferred_name__String:
	.long	8
	.globl	v8dbg_class_Script__host_defined_options__FixedArray
	.section	.data.v8dbg_class_Script__host_defined_options__FixedArray,"aw"
	.align 4
	.type	v8dbg_class_Script__host_defined_options__FixedArray, @object
	.size	v8dbg_class_Script__host_defined_options__FixedArray, 4
v8dbg_class_Script__host_defined_options__FixedArray:
	.long	120
	.globl	v8dbg_class_Script__source_mapping_url__Object
	.section	.data.v8dbg_class_Script__source_mapping_url__Object,"aw"
	.align 4
	.type	v8dbg_class_Script__source_mapping_url__Object, @object
	.size	v8dbg_class_Script__source_mapping_url__Object, 4
v8dbg_class_Script__source_mapping_url__Object:
	.long	112
	.globl	v8dbg_class_Script__source_url__Object
	.section	.data.v8dbg_class_Script__source_url__Object,"aw"
	.align 4
	.type	v8dbg_class_Script__source_url__Object, @object
	.size	v8dbg_class_Script__source_url__Object, 4
v8dbg_class_Script__source_url__Object:
	.long	104
	.globl	v8dbg_class_Script__flags__SMI
	.section	.data.v8dbg_class_Script__flags__SMI,"aw"
	.align 4
	.type	v8dbg_class_Script__flags__SMI, @object
	.size	v8dbg_class_Script__flags__SMI, 4
v8dbg_class_Script__flags__SMI:
	.long	96
	.globl	v8dbg_class_Script__shared_function_infos__WeakFixedArray
	.section	.data.v8dbg_class_Script__shared_function_infos__WeakFixedArray,"aw"
	.align 4
	.type	v8dbg_class_Script__shared_function_infos__WeakFixedArray, @object
	.size	v8dbg_class_Script__shared_function_infos__WeakFixedArray, 4
v8dbg_class_Script__shared_function_infos__WeakFixedArray:
	.long	88
	.globl	v8dbg_class_Script__line_ends__Object
	.section	.data.v8dbg_class_Script__line_ends__Object,"aw"
	.align 4
	.type	v8dbg_class_Script__line_ends__Object, @object
	.size	v8dbg_class_Script__line_ends__Object, 4
v8dbg_class_Script__line_ends__Object:
	.long	56
	.globl	v8dbg_class_Script__type__SMI
	.section	.data.v8dbg_class_Script__type__SMI,"aw"
	.align 4
	.type	v8dbg_class_Script__type__SMI, @object
	.size	v8dbg_class_Script__type__SMI, 4
v8dbg_class_Script__type__SMI:
	.long	48
	.globl	v8dbg_class_Script__context_data__Object
	.section	.data.v8dbg_class_Script__context_data__Object,"aw"
	.align 4
	.type	v8dbg_class_Script__context_data__Object, @object
	.size	v8dbg_class_Script__context_data__Object, 4
v8dbg_class_Script__context_data__Object:
	.long	40
	.globl	v8dbg_class_Script__column_offset__SMI
	.section	.data.v8dbg_class_Script__column_offset__SMI,"aw"
	.align 4
	.type	v8dbg_class_Script__column_offset__SMI, @object
	.size	v8dbg_class_Script__column_offset__SMI, 4
v8dbg_class_Script__column_offset__SMI:
	.long	32
	.globl	v8dbg_class_Script__line_offset__SMI
	.section	.data.v8dbg_class_Script__line_offset__SMI,"aw"
	.align 4
	.type	v8dbg_class_Script__line_offset__SMI, @object
	.size	v8dbg_class_Script__line_offset__SMI, 4
v8dbg_class_Script__line_offset__SMI:
	.long	24
	.globl	v8dbg_class_Script__id__SMI
	.section	.data.v8dbg_class_Script__id__SMI,"aw"
	.align 4
	.type	v8dbg_class_Script__id__SMI, @object
	.size	v8dbg_class_Script__id__SMI, 4
v8dbg_class_Script__id__SMI:
	.long	64
	.globl	v8dbg_class_Script__name__Object
	.section	.data.v8dbg_class_Script__name__Object,"aw"
	.align 4
	.type	v8dbg_class_Script__name__Object, @object
	.size	v8dbg_class_Script__name__Object, 4
v8dbg_class_Script__name__Object:
	.long	16
	.globl	v8dbg_class_Script__source__Object
	.section	.data.v8dbg_class_Script__source__Object,"aw"
	.align 4
	.type	v8dbg_class_Script__source__Object, @object
	.size	v8dbg_class_Script__source__Object, 4
v8dbg_class_Script__source__Object:
	.long	8
	.globl	v8dbg_class_Map__constructor_or_backpointer__Object
	.section	.data.v8dbg_class_Map__constructor_or_backpointer__Object,"aw"
	.align 4
	.type	v8dbg_class_Map__constructor_or_backpointer__Object, @object
	.size	v8dbg_class_Map__constructor_or_backpointer__Object, 4
v8dbg_class_Map__constructor_or_backpointer__Object:
	.long	32
	.globl	v8dbg_class_Map__prototype_validity_cell__Object
	.section	.data.v8dbg_class_Map__prototype_validity_cell__Object,"aw"
	.align 4
	.type	v8dbg_class_Map__prototype_validity_cell__Object, @object
	.size	v8dbg_class_Map__prototype_validity_cell__Object, 4
v8dbg_class_Map__prototype_validity_cell__Object:
	.long	64
	.globl	v8dbg_class_Map__dependent_code__DependentCode
	.section	.data.v8dbg_class_Map__dependent_code__DependentCode,"aw"
	.align 4
	.type	v8dbg_class_Map__dependent_code__DependentCode, @object
	.size	v8dbg_class_Map__dependent_code__DependentCode, 4
v8dbg_class_Map__dependent_code__DependentCode:
	.long	56
	.globl	v8dbg_class_JSRegExp__last_index__Object
	.section	.data.v8dbg_class_JSRegExp__last_index__Object,"aw"
	.align 4
	.type	v8dbg_class_JSRegExp__last_index__Object, @object
	.size	v8dbg_class_JSRegExp__last_index__Object, 4
v8dbg_class_JSRegExp__last_index__Object:
	.long	48
	.globl	v8dbg_class_JSStringIterator__index__SMI
	.section	.data.v8dbg_class_JSStringIterator__index__SMI,"aw"
	.align 4
	.type	v8dbg_class_JSStringIterator__index__SMI, @object
	.size	v8dbg_class_JSStringIterator__index__SMI, 4
v8dbg_class_JSStringIterator__index__SMI:
	.long	32
	.globl	v8dbg_class_JSStringIterator__string__String
	.section	.data.v8dbg_class_JSStringIterator__string__String,"aw"
	.align 4
	.type	v8dbg_class_JSStringIterator__string__String, @object
	.size	v8dbg_class_JSStringIterator__string__String, 4
v8dbg_class_JSStringIterator__string__String:
	.long	24
	.globl	v8dbg_class_JSIteratorResult__done__Object
	.section	.data.v8dbg_class_JSIteratorResult__done__Object,"aw"
	.align 4
	.type	v8dbg_class_JSIteratorResult__done__Object, @object
	.size	v8dbg_class_JSIteratorResult__done__Object, 4
v8dbg_class_JSIteratorResult__done__Object:
	.long	32
	.globl	v8dbg_class_JSIteratorResult__value__Object
	.section	.data.v8dbg_class_JSIteratorResult__value__Object,"aw"
	.align 4
	.type	v8dbg_class_JSIteratorResult__value__Object, @object
	.size	v8dbg_class_JSIteratorResult__value__Object, 4
v8dbg_class_JSIteratorResult__value__Object:
	.long	24
	.globl	v8dbg_class_JSMessageObject__raw_type__SMI
	.section	.data.v8dbg_class_JSMessageObject__raw_type__SMI,"aw"
	.align 4
	.type	v8dbg_class_JSMessageObject__raw_type__SMI, @object
	.size	v8dbg_class_JSMessageObject__raw_type__SMI, 4
v8dbg_class_JSMessageObject__raw_type__SMI:
	.long	24
	.globl	v8dbg_class_JSMessageObject__error_level__SMI
	.section	.data.v8dbg_class_JSMessageObject__error_level__SMI,"aw"
	.align 4
	.type	v8dbg_class_JSMessageObject__error_level__SMI, @object
	.size	v8dbg_class_JSMessageObject__error_level__SMI, 4
v8dbg_class_JSMessageObject__error_level__SMI:
	.long	88
	.globl	v8dbg_class_JSMessageObject__end_position__SMI
	.section	.data.v8dbg_class_JSMessageObject__end_position__SMI,"aw"
	.align 4
	.type	v8dbg_class_JSMessageObject__end_position__SMI, @object
	.size	v8dbg_class_JSMessageObject__end_position__SMI, 4
v8dbg_class_JSMessageObject__end_position__SMI:
	.long	80
	.globl	v8dbg_class_JSMessageObject__start_position__SMI
	.section	.data.v8dbg_class_JSMessageObject__start_position__SMI,"aw"
	.align 4
	.type	v8dbg_class_JSMessageObject__start_position__SMI, @object
	.size	v8dbg_class_JSMessageObject__start_position__SMI, 4
v8dbg_class_JSMessageObject__start_position__SMI:
	.long	72
	.globl	v8dbg_class_JSMessageObject__bytecode_offset__Smi
	.section	.data.v8dbg_class_JSMessageObject__bytecode_offset__Smi,"aw"
	.align 4
	.type	v8dbg_class_JSMessageObject__bytecode_offset__Smi, @object
	.size	v8dbg_class_JSMessageObject__bytecode_offset__Smi, 4
v8dbg_class_JSMessageObject__bytecode_offset__Smi:
	.long	64
	.globl	v8dbg_class_JSMessageObject__shared_info__HeapObject
	.section	.data.v8dbg_class_JSMessageObject__shared_info__HeapObject,"aw"
	.align 4
	.type	v8dbg_class_JSMessageObject__shared_info__HeapObject, @object
	.size	v8dbg_class_JSMessageObject__shared_info__HeapObject, 4
v8dbg_class_JSMessageObject__shared_info__HeapObject:
	.long	56
	.globl	v8dbg_class_JSMessageObject__stack_frames__Object
	.section	.data.v8dbg_class_JSMessageObject__stack_frames__Object,"aw"
	.align 4
	.type	v8dbg_class_JSMessageObject__stack_frames__Object, @object
	.size	v8dbg_class_JSMessageObject__stack_frames__Object, 4
v8dbg_class_JSMessageObject__stack_frames__Object:
	.long	48
	.globl	v8dbg_class_JSMessageObject__script__Script
	.section	.data.v8dbg_class_JSMessageObject__script__Script,"aw"
	.align 4
	.type	v8dbg_class_JSMessageObject__script__Script, @object
	.size	v8dbg_class_JSMessageObject__script__Script, 4
v8dbg_class_JSMessageObject__script__Script:
	.long	40
	.globl	v8dbg_class_JSMessageObject__argument__Object
	.section	.data.v8dbg_class_JSMessageObject__argument__Object,"aw"
	.align 4
	.type	v8dbg_class_JSMessageObject__argument__Object, @object
	.size	v8dbg_class_JSMessageObject__argument__Object, 4
v8dbg_class_JSMessageObject__argument__Object:
	.long	32
	.globl	v8dbg_class_JSGlobalObject__global_proxy__JSGlobalProxy
	.section	.data.v8dbg_class_JSGlobalObject__global_proxy__JSGlobalProxy,"aw"
	.align 4
	.type	v8dbg_class_JSGlobalObject__global_proxy__JSGlobalProxy, @object
	.size	v8dbg_class_JSGlobalObject__global_proxy__JSGlobalProxy, 4
v8dbg_class_JSGlobalObject__global_proxy__JSGlobalProxy:
	.long	32
	.globl	v8dbg_class_JSGlobalObject__native_context__NativeContext
	.section	.data.v8dbg_class_JSGlobalObject__native_context__NativeContext,"aw"
	.align 4
	.type	v8dbg_class_JSGlobalObject__native_context__NativeContext, @object
	.size	v8dbg_class_JSGlobalObject__native_context__NativeContext, 4
v8dbg_class_JSGlobalObject__native_context__NativeContext:
	.long	24
	.globl	v8dbg_class_JSFunction__raw_feedback_cell__FeedbackCell
	.section	.data.v8dbg_class_JSFunction__raw_feedback_cell__FeedbackCell,"aw"
	.align 4
	.type	v8dbg_class_JSFunction__raw_feedback_cell__FeedbackCell, @object
	.size	v8dbg_class_JSFunction__raw_feedback_cell__FeedbackCell, 4
v8dbg_class_JSFunction__raw_feedback_cell__FeedbackCell:
	.long	40
	.globl	v8dbg_class_JSReceiver__raw_properties_or_hash__Object
	.section	.data.v8dbg_class_JSReceiver__raw_properties_or_hash__Object,"aw"
	.align 4
	.type	v8dbg_class_JSReceiver__raw_properties_or_hash__Object, @object
	.size	v8dbg_class_JSReceiver__raw_properties_or_hash__Object, 4
v8dbg_class_JSReceiver__raw_properties_or_hash__Object:
	.long	8
	.globl	v8dbg_class_JSTypedArray__base_pointer__Object
	.section	.data.v8dbg_class_JSTypedArray__base_pointer__Object,"aw"
	.align 4
	.type	v8dbg_class_JSTypedArray__base_pointer__Object, @object
	.size	v8dbg_class_JSTypedArray__base_pointer__Object, 4
v8dbg_class_JSTypedArray__base_pointer__Object:
	.long	64
	.globl	v8dbg_class_JSArrayBufferView__buffer__Object
	.section	.data.v8dbg_class_JSArrayBufferView__buffer__Object,"aw"
	.align 4
	.type	v8dbg_class_JSArrayBufferView__buffer__Object, @object
	.size	v8dbg_class_JSArrayBufferView__buffer__Object, 4
v8dbg_class_JSArrayBufferView__buffer__Object:
	.long	24
	.globl	v8dbg_class_JSArrayIterator__raw_kind__SMI
	.section	.data.v8dbg_class_JSArrayIterator__raw_kind__SMI,"aw"
	.align 4
	.type	v8dbg_class_JSArrayIterator__raw_kind__SMI, @object
	.size	v8dbg_class_JSArrayIterator__raw_kind__SMI, 4
v8dbg_class_JSArrayIterator__raw_kind__SMI:
	.long	40
	.globl	v8dbg_class_JSArrayIterator__next_index__Object
	.section	.data.v8dbg_class_JSArrayIterator__next_index__Object,"aw"
	.align 4
	.type	v8dbg_class_JSArrayIterator__next_index__Object, @object
	.size	v8dbg_class_JSArrayIterator__next_index__Object, 4
v8dbg_class_JSArrayIterator__next_index__Object:
	.long	32
	.globl	v8dbg_class_JSArrayIterator__iterated_object__Object
	.section	.data.v8dbg_class_JSArrayIterator__iterated_object__Object,"aw"
	.align 4
	.type	v8dbg_class_JSArrayIterator__iterated_object__Object, @object
	.size	v8dbg_class_JSArrayIterator__iterated_object__Object, 4
v8dbg_class_JSArrayIterator__iterated_object__Object:
	.long	24
	.globl	v8dbg_class_JSArray__length__Object
	.section	.data.v8dbg_class_JSArray__length__Object,"aw"
	.align 4
	.type	v8dbg_class_JSArray__length__Object, @object
	.size	v8dbg_class_JSArray__length__Object, 4
v8dbg_class_JSArray__length__Object:
	.long	24
	.globl	v8dbg_class_WeakArrayList__length__SMI
	.section	.data.v8dbg_class_WeakArrayList__length__SMI,"aw"
	.align 4
	.type	v8dbg_class_WeakArrayList__length__SMI, @object
	.size	v8dbg_class_WeakArrayList__length__SMI, 4
v8dbg_class_WeakArrayList__length__SMI:
	.long	16
	.globl	v8dbg_class_WeakArrayList__capacity__SMI
	.section	.data.v8dbg_class_WeakArrayList__capacity__SMI,"aw"
	.align 4
	.type	v8dbg_class_WeakArrayList__capacity__SMI, @object
	.size	v8dbg_class_WeakArrayList__capacity__SMI, 4
v8dbg_class_WeakArrayList__capacity__SMI:
	.long	8
	.globl	v8dbg_class_WeakFixedArray__length__SMI
	.section	.data.v8dbg_class_WeakFixedArray__length__SMI,"aw"
	.align 4
	.type	v8dbg_class_WeakFixedArray__length__SMI, @object
	.size	v8dbg_class_WeakFixedArray__length__SMI, 4
v8dbg_class_WeakFixedArray__length__SMI:
	.long	8
	.globl	v8dbg_class_FixedArrayBase__length__SMI
	.section	.data.v8dbg_class_FixedArrayBase__length__SMI,"aw"
	.align 4
	.type	v8dbg_class_FixedArrayBase__length__SMI, @object
	.size	v8dbg_class_FixedArrayBase__length__SMI, 4
v8dbg_class_FixedArrayBase__length__SMI:
	.long	8
	.globl	v8dbg_class_FeedbackCell__value__HeapObject
	.section	.data.v8dbg_class_FeedbackCell__value__HeapObject,"aw"
	.align 4
	.type	v8dbg_class_FeedbackCell__value__HeapObject, @object
	.size	v8dbg_class_FeedbackCell__value__HeapObject, 4
v8dbg_class_FeedbackCell__value__HeapObject:
	.long	8
	.globl	v8dbg_class_DescriptorArray__enum_cache__EnumCache
	.section	.data.v8dbg_class_DescriptorArray__enum_cache__EnumCache,"aw"
	.align 4
	.type	v8dbg_class_DescriptorArray__enum_cache__EnumCache, @object
	.size	v8dbg_class_DescriptorArray__enum_cache__EnumCache, 4
v8dbg_class_DescriptorArray__enum_cache__EnumCache:
	.long	16
	.globl	v8dbg_class_DataHandler__validity_cell__Object
	.section	.data.v8dbg_class_DataHandler__validity_cell__Object,"aw"
	.align 4
	.type	v8dbg_class_DataHandler__validity_cell__Object, @object
	.size	v8dbg_class_DataHandler__validity_cell__Object, 4
v8dbg_class_DataHandler__validity_cell__Object:
	.long	16
	.globl	v8dbg_class_DataHandler__smi_handler__Object
	.section	.data.v8dbg_class_DataHandler__smi_handler__Object,"aw"
	.align 4
	.type	v8dbg_class_DataHandler__smi_handler__Object, @object
	.size	v8dbg_class_DataHandler__smi_handler__Object, 4
v8dbg_class_DataHandler__smi_handler__Object:
	.long	8
	.globl	v8dbg_class_BytecodeArray__source_position_table__Object
	.section	.data.v8dbg_class_BytecodeArray__source_position_table__Object,"aw"
	.align 4
	.type	v8dbg_class_BytecodeArray__source_position_table__Object, @object
	.size	v8dbg_class_BytecodeArray__source_position_table__Object, 4
v8dbg_class_BytecodeArray__source_position_table__Object:
	.long	32
	.globl	v8dbg_class_BytecodeArray__handler_table__ByteArray
	.section	.data.v8dbg_class_BytecodeArray__handler_table__ByteArray,"aw"
	.align 4
	.type	v8dbg_class_BytecodeArray__handler_table__ByteArray, @object
	.size	v8dbg_class_BytecodeArray__handler_table__ByteArray, 4
v8dbg_class_BytecodeArray__handler_table__ByteArray:
	.long	24
	.globl	v8dbg_class_BytecodeArray__constant_pool__FixedArray
	.section	.data.v8dbg_class_BytecodeArray__constant_pool__FixedArray,"aw"
	.align 4
	.type	v8dbg_class_BytecodeArray__constant_pool__FixedArray, @object
	.size	v8dbg_class_BytecodeArray__constant_pool__FixedArray, 4
v8dbg_class_BytecodeArray__constant_pool__FixedArray:
	.long	16
	.globl	v8dbg_class_CodeDataContainer__next_code_link__Object
	.section	.data.v8dbg_class_CodeDataContainer__next_code_link__Object,"aw"
	.align 4
	.type	v8dbg_class_CodeDataContainer__next_code_link__Object, @object
	.size	v8dbg_class_CodeDataContainer__next_code_link__Object, 4
v8dbg_class_CodeDataContainer__next_code_link__Object:
	.long	8
	.globl	v8dbg_class_AllocationMemento__allocation_site__Object
	.section	.data.v8dbg_class_AllocationMemento__allocation_site__Object,"aw"
	.align 4
	.type	v8dbg_class_AllocationMemento__allocation_site__Object, @object
	.size	v8dbg_class_AllocationMemento__allocation_site__Object, 4
v8dbg_class_AllocationMemento__allocation_site__Object:
	.long	8
	.globl	v8dbg_class_AllocationSite__dependent_code__DependentCode
	.section	.data.v8dbg_class_AllocationSite__dependent_code__DependentCode,"aw"
	.align 4
	.type	v8dbg_class_AllocationSite__dependent_code__DependentCode, @object
	.size	v8dbg_class_AllocationSite__dependent_code__DependentCode, 4
v8dbg_class_AllocationSite__dependent_code__DependentCode:
	.long	24
	.globl	v8dbg_class_AllocationSite__nested_site__Object
	.section	.data.v8dbg_class_AllocationSite__nested_site__Object,"aw"
	.align 4
	.type	v8dbg_class_AllocationSite__nested_site__Object, @object
	.size	v8dbg_class_AllocationSite__nested_site__Object, 4
v8dbg_class_AllocationSite__nested_site__Object:
	.long	16
	.globl	v8dbg_class_AllocationSite__transition_info_or_boilerplate__Object
	.section	.data.v8dbg_class_AllocationSite__transition_info_or_boilerplate__Object,"aw"
	.align 4
	.type	v8dbg_class_AllocationSite__transition_info_or_boilerplate__Object, @object
	.size	v8dbg_class_AllocationSite__transition_info_or_boilerplate__Object, 4
v8dbg_class_AllocationSite__transition_info_or_boilerplate__Object:
	.long	8
	.globl	v8dbg_parent_WeakFixedArray__HeapObject
	.section	.bss.v8dbg_parent_WeakFixedArray__HeapObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_WeakFixedArray__HeapObject, @object
	.size	v8dbg_parent_WeakFixedArray__HeapObject, 4
v8dbg_parent_WeakFixedArray__HeapObject:
	.zero	4
	.globl	v8dbg_parent_WeakArrayList__HeapObject
	.section	.bss.v8dbg_parent_WeakArrayList__HeapObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_WeakArrayList__HeapObject, @object
	.size	v8dbg_parent_WeakArrayList__HeapObject, 4
v8dbg_parent_WeakArrayList__HeapObject:
	.zero	4
	.globl	v8dbg_parent_UncompiledDataWithoutPreparseData__UncompiledData
	.section	.bss.v8dbg_parent_UncompiledDataWithoutPreparseData__UncompiledData,"aw",@nobits
	.align 4
	.type	v8dbg_parent_UncompiledDataWithoutPreparseData__UncompiledData, @object
	.size	v8dbg_parent_UncompiledDataWithoutPreparseData__UncompiledData, 4
v8dbg_parent_UncompiledDataWithoutPreparseData__UncompiledData:
	.zero	4
	.globl	v8dbg_parent_UncompiledDataWithPreparseData__UncompiledData
	.section	.bss.v8dbg_parent_UncompiledDataWithPreparseData__UncompiledData,"aw",@nobits
	.align 4
	.type	v8dbg_parent_UncompiledDataWithPreparseData__UncompiledData, @object
	.size	v8dbg_parent_UncompiledDataWithPreparseData__UncompiledData, 4
v8dbg_parent_UncompiledDataWithPreparseData__UncompiledData:
	.zero	4
	.globl	v8dbg_parent_UncompiledData__HeapObject
	.section	.bss.v8dbg_parent_UncompiledData__HeapObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_UncompiledData__HeapObject, @object
	.size	v8dbg_parent_UncompiledData__HeapObject, 4
v8dbg_parent_UncompiledData__HeapObject:
	.zero	4
	.globl	v8dbg_parent_Tuple3__Tuple2
	.section	.bss.v8dbg_parent_Tuple3__Tuple2,"aw",@nobits
	.align 4
	.type	v8dbg_parent_Tuple3__Tuple2, @object
	.size	v8dbg_parent_Tuple3__Tuple2, 4
v8dbg_parent_Tuple3__Tuple2:
	.zero	4
	.globl	v8dbg_parent_Tuple2__Struct
	.section	.bss.v8dbg_parent_Tuple2__Struct,"aw",@nobits
	.align 4
	.type	v8dbg_parent_Tuple2__Struct, @object
	.size	v8dbg_parent_Tuple2__Struct, 4
v8dbg_parent_Tuple2__Struct:
	.zero	4
	.globl	v8dbg_parent_ThinString__String
	.section	.bss.v8dbg_parent_ThinString__String,"aw",@nobits
	.align 4
	.type	v8dbg_parent_ThinString__String, @object
	.size	v8dbg_parent_ThinString__String, 4
v8dbg_parent_ThinString__String:
	.zero	4
	.globl	v8dbg_parent_TemplateList__FixedArray
	.section	.bss.v8dbg_parent_TemplateList__FixedArray,"aw",@nobits
	.align 4
	.type	v8dbg_parent_TemplateList__FixedArray, @object
	.size	v8dbg_parent_TemplateList__FixedArray, 4
v8dbg_parent_TemplateList__FixedArray:
	.zero	4
	.globl	v8dbg_parent_Symbol__Name
	.section	.bss.v8dbg_parent_Symbol__Name,"aw",@nobits
	.align 4
	.type	v8dbg_parent_Symbol__Name, @object
	.size	v8dbg_parent_Symbol__Name, 4
v8dbg_parent_Symbol__Name:
	.zero	4
	.globl	v8dbg_parent_Struct__HeapObject
	.section	.bss.v8dbg_parent_Struct__HeapObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_Struct__HeapObject, @object
	.size	v8dbg_parent_Struct__HeapObject, 4
v8dbg_parent_Struct__HeapObject:
	.zero	4
	.globl	v8dbg_parent_String__Name
	.section	.bss.v8dbg_parent_String__Name,"aw",@nobits
	.align 4
	.type	v8dbg_parent_String__Name, @object
	.size	v8dbg_parent_String__Name, 4
v8dbg_parent_String__Name:
	.zero	4
	.globl	v8dbg_parent_SourcePositionTableWithFrameCache__Struct
	.section	.bss.v8dbg_parent_SourcePositionTableWithFrameCache__Struct,"aw",@nobits
	.align 4
	.type	v8dbg_parent_SourcePositionTableWithFrameCache__Struct, @object
	.size	v8dbg_parent_SourcePositionTableWithFrameCache__Struct, 4
v8dbg_parent_SourcePositionTableWithFrameCache__Struct:
	.zero	4
	.globl	v8dbg_parent_SlicedString__String
	.section	.bss.v8dbg_parent_SlicedString__String,"aw",@nobits
	.align 4
	.type	v8dbg_parent_SlicedString__String, @object
	.size	v8dbg_parent_SlicedString__String, 4
v8dbg_parent_SlicedString__String:
	.zero	4
	.globl	v8dbg_parent_SharedFunctionInfo__HeapObject
	.section	.bss.v8dbg_parent_SharedFunctionInfo__HeapObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_SharedFunctionInfo__HeapObject, @object
	.size	v8dbg_parent_SharedFunctionInfo__HeapObject, 4
v8dbg_parent_SharedFunctionInfo__HeapObject:
	.zero	4
	.globl	v8dbg_parent_SeqTwoByteString__SeqString
	.section	.bss.v8dbg_parent_SeqTwoByteString__SeqString,"aw",@nobits
	.align 4
	.type	v8dbg_parent_SeqTwoByteString__SeqString, @object
	.size	v8dbg_parent_SeqTwoByteString__SeqString, 4
v8dbg_parent_SeqTwoByteString__SeqString:
	.zero	4
	.globl	v8dbg_parent_SeqString__String
	.section	.bss.v8dbg_parent_SeqString__String,"aw",@nobits
	.align 4
	.type	v8dbg_parent_SeqString__String, @object
	.size	v8dbg_parent_SeqString__String, 4
v8dbg_parent_SeqString__String:
	.zero	4
	.globl	v8dbg_parent_SeqOneByteString__SeqString
	.section	.bss.v8dbg_parent_SeqOneByteString__SeqString,"aw",@nobits
	.align 4
	.type	v8dbg_parent_SeqOneByteString__SeqString, @object
	.size	v8dbg_parent_SeqOneByteString__SeqString, 4
v8dbg_parent_SeqOneByteString__SeqString:
	.zero	4
	.globl	v8dbg_parent_Script__Struct
	.section	.bss.v8dbg_parent_Script__Struct,"aw",@nobits
	.align 4
	.type	v8dbg_parent_Script__Struct, @object
	.size	v8dbg_parent_Script__Struct, 4
v8dbg_parent_Script__Struct:
	.zero	4
	.globl	v8dbg_parent_ScopeInfo__FixedArray
	.section	.bss.v8dbg_parent_ScopeInfo__FixedArray,"aw",@nobits
	.align 4
	.type	v8dbg_parent_ScopeInfo__FixedArray, @object
	.size	v8dbg_parent_ScopeInfo__FixedArray, 4
v8dbg_parent_ScopeInfo__FixedArray:
	.zero	4
	.globl	v8dbg_parent_PreparseData__HeapObject
	.section	.bss.v8dbg_parent_PreparseData__HeapObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_PreparseData__HeapObject, @object
	.size	v8dbg_parent_PreparseData__HeapObject, 4
v8dbg_parent_PreparseData__HeapObject:
	.zero	4
	.globl	v8dbg_parent_PodArray__ByteArray
	.section	.bss.v8dbg_parent_PodArray__ByteArray,"aw",@nobits
	.align 4
	.type	v8dbg_parent_PodArray__ByteArray, @object
	.size	v8dbg_parent_PodArray__ByteArray, 4
v8dbg_parent_PodArray__ByteArray:
	.zero	4
	.globl	v8dbg_parent_Oddball__HeapObject
	.section	.bss.v8dbg_parent_Oddball__HeapObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_Oddball__HeapObject, @object
	.size	v8dbg_parent_Oddball__HeapObject, 4
v8dbg_parent_Oddball__HeapObject:
	.zero	4
	.globl	v8dbg_parent_Object__TaggedImpl
	.section	.bss.v8dbg_parent_Object__TaggedImpl,"aw",@nobits
	.align 4
	.type	v8dbg_parent_Object__TaggedImpl, @object
	.size	v8dbg_parent_Object__TaggedImpl, 4
v8dbg_parent_Object__TaggedImpl:
	.zero	4
	.globl	v8dbg_parent_NormalizedMapCache__WeakFixedArray
	.section	.bss.v8dbg_parent_NormalizedMapCache__WeakFixedArray,"aw",@nobits
	.align 4
	.type	v8dbg_parent_NormalizedMapCache__WeakFixedArray, @object
	.size	v8dbg_parent_NormalizedMapCache__WeakFixedArray, 4
v8dbg_parent_NormalizedMapCache__WeakFixedArray:
	.zero	4
	.globl	v8dbg_parent_Name__HeapObject
	.section	.bss.v8dbg_parent_Name__HeapObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_Name__HeapObject, @object
	.size	v8dbg_parent_Name__HeapObject, 4
v8dbg_parent_Name__HeapObject:
	.zero	4
	.globl	v8dbg_parent_Map__HeapObject
	.section	.bss.v8dbg_parent_Map__HeapObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_Map__HeapObject, @object
	.size	v8dbg_parent_Map__HeapObject, 4
v8dbg_parent_Map__HeapObject:
	.zero	4
	.globl	v8dbg_parent_JSTypedArray__JSArrayBufferView
	.section	.bss.v8dbg_parent_JSTypedArray__JSArrayBufferView,"aw",@nobits
	.align 4
	.type	v8dbg_parent_JSTypedArray__JSArrayBufferView, @object
	.size	v8dbg_parent_JSTypedArray__JSArrayBufferView, 4
v8dbg_parent_JSTypedArray__JSArrayBufferView:
	.zero	4
	.globl	v8dbg_parent_JSStringIterator__JSObject
	.section	.bss.v8dbg_parent_JSStringIterator__JSObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_JSStringIterator__JSObject, @object
	.size	v8dbg_parent_JSStringIterator__JSObject, 4
v8dbg_parent_JSStringIterator__JSObject:
	.zero	4
	.globl	v8dbg_parent_JSRegExpStringIterator__JSObject
	.section	.bss.v8dbg_parent_JSRegExpStringIterator__JSObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_JSRegExpStringIterator__JSObject, @object
	.size	v8dbg_parent_JSRegExpStringIterator__JSObject, 4
v8dbg_parent_JSRegExpStringIterator__JSObject:
	.zero	4
	.globl	v8dbg_parent_JSRegExpResult__JSArray
	.section	.bss.v8dbg_parent_JSRegExpResult__JSArray,"aw",@nobits
	.align 4
	.type	v8dbg_parent_JSRegExpResult__JSArray, @object
	.size	v8dbg_parent_JSRegExpResult__JSArray, 4
v8dbg_parent_JSRegExpResult__JSArray:
	.zero	4
	.globl	v8dbg_parent_JSRegExp__JSObject
	.section	.bss.v8dbg_parent_JSRegExp__JSObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_JSRegExp__JSObject, @object
	.size	v8dbg_parent_JSRegExp__JSObject, 4
v8dbg_parent_JSRegExp__JSObject:
	.zero	4
	.globl	v8dbg_parent_JSReceiver__HeapObject
	.section	.bss.v8dbg_parent_JSReceiver__HeapObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_JSReceiver__HeapObject, @object
	.size	v8dbg_parent_JSReceiver__HeapObject, 4
v8dbg_parent_JSReceiver__HeapObject:
	.zero	4
	.globl	v8dbg_parent_JSPromise__JSObject
	.section	.bss.v8dbg_parent_JSPromise__JSObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_JSPromise__JSObject, @object
	.size	v8dbg_parent_JSPromise__JSObject, 4
v8dbg_parent_JSPromise__JSObject:
	.zero	4
	.globl	v8dbg_parent_JSPrimitiveWrapper__JSObject
	.section	.bss.v8dbg_parent_JSPrimitiveWrapper__JSObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_JSPrimitiveWrapper__JSObject, @object
	.size	v8dbg_parent_JSPrimitiveWrapper__JSObject, 4
v8dbg_parent_JSPrimitiveWrapper__JSObject:
	.zero	4
	.globl	v8dbg_parent_JSObject__JSReceiver
	.section	.bss.v8dbg_parent_JSObject__JSReceiver,"aw",@nobits
	.align 4
	.type	v8dbg_parent_JSObject__JSReceiver, @object
	.size	v8dbg_parent_JSObject__JSReceiver, 4
v8dbg_parent_JSObject__JSReceiver:
	.zero	4
	.globl	v8dbg_parent_JSMessageObject__JSObject
	.section	.bss.v8dbg_parent_JSMessageObject__JSObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_JSMessageObject__JSObject, @object
	.size	v8dbg_parent_JSMessageObject__JSObject, 4
v8dbg_parent_JSMessageObject__JSObject:
	.zero	4
	.globl	v8dbg_parent_JSIteratorResult__JSObject
	.section	.bss.v8dbg_parent_JSIteratorResult__JSObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_JSIteratorResult__JSObject, @object
	.size	v8dbg_parent_JSIteratorResult__JSObject, 4
v8dbg_parent_JSIteratorResult__JSObject:
	.zero	4
	.globl	v8dbg_parent_JSGlobalProxy__JSObject
	.section	.bss.v8dbg_parent_JSGlobalProxy__JSObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_JSGlobalProxy__JSObject, @object
	.size	v8dbg_parent_JSGlobalProxy__JSObject, 4
v8dbg_parent_JSGlobalProxy__JSObject:
	.zero	4
	.globl	v8dbg_parent_JSGlobalObject__JSObject
	.section	.bss.v8dbg_parent_JSGlobalObject__JSObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_JSGlobalObject__JSObject, @object
	.size	v8dbg_parent_JSGlobalObject__JSObject, 4
v8dbg_parent_JSGlobalObject__JSObject:
	.zero	4
	.globl	v8dbg_parent_JSFunction__JSObject
	.section	.bss.v8dbg_parent_JSFunction__JSObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_JSFunction__JSObject, @object
	.size	v8dbg_parent_JSFunction__JSObject, 4
v8dbg_parent_JSFunction__JSObject:
	.zero	4
	.globl	v8dbg_parent_JSDate__JSObject
	.section	.bss.v8dbg_parent_JSDate__JSObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_JSDate__JSObject, @object
	.size	v8dbg_parent_JSDate__JSObject, 4
v8dbg_parent_JSDate__JSObject:
	.zero	4
	.globl	v8dbg_parent_JSDataView__JSArrayBufferView
	.section	.bss.v8dbg_parent_JSDataView__JSArrayBufferView,"aw",@nobits
	.align 4
	.type	v8dbg_parent_JSDataView__JSArrayBufferView, @object
	.size	v8dbg_parent_JSDataView__JSArrayBufferView, 4
v8dbg_parent_JSDataView__JSArrayBufferView:
	.zero	4
	.globl	v8dbg_parent_JSDataPropertyDescriptor__JSObject
	.section	.bss.v8dbg_parent_JSDataPropertyDescriptor__JSObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_JSDataPropertyDescriptor__JSObject, @object
	.size	v8dbg_parent_JSDataPropertyDescriptor__JSObject, 4
v8dbg_parent_JSDataPropertyDescriptor__JSObject:
	.zero	4
	.globl	v8dbg_parent_JSBoundFunction__JSObject
	.section	.bss.v8dbg_parent_JSBoundFunction__JSObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_JSBoundFunction__JSObject, @object
	.size	v8dbg_parent_JSBoundFunction__JSObject, 4
v8dbg_parent_JSBoundFunction__JSObject:
	.zero	4
	.globl	v8dbg_parent_JSAsyncFromSyncIterator__JSObject
	.section	.bss.v8dbg_parent_JSAsyncFromSyncIterator__JSObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_JSAsyncFromSyncIterator__JSObject, @object
	.size	v8dbg_parent_JSAsyncFromSyncIterator__JSObject, 4
v8dbg_parent_JSAsyncFromSyncIterator__JSObject:
	.zero	4
	.globl	v8dbg_parent_JSArrayIterator__JSObject
	.section	.bss.v8dbg_parent_JSArrayIterator__JSObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_JSArrayIterator__JSObject, @object
	.size	v8dbg_parent_JSArrayIterator__JSObject, 4
v8dbg_parent_JSArrayIterator__JSObject:
	.zero	4
	.globl	v8dbg_parent_JSArrayBufferView__JSObject
	.section	.bss.v8dbg_parent_JSArrayBufferView__JSObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_JSArrayBufferView__JSObject, @object
	.size	v8dbg_parent_JSArrayBufferView__JSObject, 4
v8dbg_parent_JSArrayBufferView__JSObject:
	.zero	4
	.globl	v8dbg_parent_JSArrayBuffer__JSObject
	.section	.bss.v8dbg_parent_JSArrayBuffer__JSObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_JSArrayBuffer__JSObject, @object
	.size	v8dbg_parent_JSArrayBuffer__JSObject, 4
v8dbg_parent_JSArrayBuffer__JSObject:
	.zero	4
	.globl	v8dbg_parent_JSArray__JSObject
	.section	.bss.v8dbg_parent_JSArray__JSObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_JSArray__JSObject, @object
	.size	v8dbg_parent_JSArray__JSObject, 4
v8dbg_parent_JSArray__JSObject:
	.zero	4
	.globl	v8dbg_parent_JSAccessorPropertyDescriptor__JSObject
	.section	.bss.v8dbg_parent_JSAccessorPropertyDescriptor__JSObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_JSAccessorPropertyDescriptor__JSObject, @object
	.size	v8dbg_parent_JSAccessorPropertyDescriptor__JSObject, 4
v8dbg_parent_JSAccessorPropertyDescriptor__JSObject:
	.zero	4
	.globl	v8dbg_parent_InterpreterData__Struct
	.section	.bss.v8dbg_parent_InterpreterData__Struct,"aw",@nobits
	.align 4
	.type	v8dbg_parent_InterpreterData__Struct, @object
	.size	v8dbg_parent_InterpreterData__Struct, 4
v8dbg_parent_InterpreterData__Struct:
	.zero	4
	.globl	v8dbg_parent_InternalizedString__String
	.section	.bss.v8dbg_parent_InternalizedString__String,"aw",@nobits
	.align 4
	.type	v8dbg_parent_InternalizedString__String, @object
	.size	v8dbg_parent_InternalizedString__String, 4
v8dbg_parent_InternalizedString__String:
	.zero	4
	.globl	v8dbg_parent_HeapObject__Object
	.section	.bss.v8dbg_parent_HeapObject__Object,"aw",@nobits
	.align 4
	.type	v8dbg_parent_HeapObject__Object, @object
	.size	v8dbg_parent_HeapObject__Object, 4
v8dbg_parent_HeapObject__Object:
	.zero	4
	.globl	v8dbg_parent_HeapNumber__HeapObject
	.section	.bss.v8dbg_parent_HeapNumber__HeapObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_HeapNumber__HeapObject, @object
	.size	v8dbg_parent_HeapNumber__HeapObject, 4
v8dbg_parent_HeapNumber__HeapObject:
	.zero	4
	.globl	v8dbg_parent_FixedDoubleArray__FixedArrayBase
	.section	.bss.v8dbg_parent_FixedDoubleArray__FixedArrayBase,"aw",@nobits
	.align 4
	.type	v8dbg_parent_FixedDoubleArray__FixedArrayBase, @object
	.size	v8dbg_parent_FixedDoubleArray__FixedArrayBase, 4
v8dbg_parent_FixedDoubleArray__FixedArrayBase:
	.zero	4
	.globl	v8dbg_parent_FixedArrayExactfinal__FixedArray
	.section	.bss.v8dbg_parent_FixedArrayExactfinal__FixedArray,"aw",@nobits
	.align 4
	.type	v8dbg_parent_FixedArrayExactfinal__FixedArray, @object
	.size	v8dbg_parent_FixedArrayExactfinal__FixedArray, 4
v8dbg_parent_FixedArrayExactfinal__FixedArray:
	.zero	4
	.globl	v8dbg_parent_FixedArrayBase__HeapObject
	.section	.bss.v8dbg_parent_FixedArrayBase__HeapObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_FixedArrayBase__HeapObject, @object
	.size	v8dbg_parent_FixedArrayBase__HeapObject, 4
v8dbg_parent_FixedArrayBase__HeapObject:
	.zero	4
	.globl	v8dbg_parent_FixedArray__FixedArrayBase
	.section	.bss.v8dbg_parent_FixedArray__FixedArrayBase,"aw",@nobits
	.align 4
	.type	v8dbg_parent_FixedArray__FixedArrayBase, @object
	.size	v8dbg_parent_FixedArray__FixedArrayBase, 4
v8dbg_parent_FixedArray__FixedArrayBase:
	.zero	4
	.globl	v8dbg_parent_FeedbackCell__Struct
	.section	.bss.v8dbg_parent_FeedbackCell__Struct,"aw",@nobits
	.align 4
	.type	v8dbg_parent_FeedbackCell__Struct, @object
	.size	v8dbg_parent_FeedbackCell__Struct, 4
v8dbg_parent_FeedbackCell__Struct:
	.zero	4
	.globl	v8dbg_parent_ExternalTwoByteString__ExternalString
	.section	.bss.v8dbg_parent_ExternalTwoByteString__ExternalString,"aw",@nobits
	.align 4
	.type	v8dbg_parent_ExternalTwoByteString__ExternalString, @object
	.size	v8dbg_parent_ExternalTwoByteString__ExternalString, 4
v8dbg_parent_ExternalTwoByteString__ExternalString:
	.zero	4
	.globl	v8dbg_parent_ExternalString__String
	.section	.bss.v8dbg_parent_ExternalString__String,"aw",@nobits
	.align 4
	.type	v8dbg_parent_ExternalString__String, @object
	.size	v8dbg_parent_ExternalString__String, 4
v8dbg_parent_ExternalString__String:
	.zero	4
	.globl	v8dbg_parent_ExternalOneByteString__ExternalString
	.section	.bss.v8dbg_parent_ExternalOneByteString__ExternalString,"aw",@nobits
	.align 4
	.type	v8dbg_parent_ExternalOneByteString__ExternalString, @object
	.size	v8dbg_parent_ExternalOneByteString__ExternalString, 4
v8dbg_parent_ExternalOneByteString__ExternalString:
	.zero	4
	.globl	v8dbg_parent_EnumCache__Struct
	.section	.bss.v8dbg_parent_EnumCache__Struct,"aw",@nobits
	.align 4
	.type	v8dbg_parent_EnumCache__Struct, @object
	.size	v8dbg_parent_EnumCache__Struct, 4
v8dbg_parent_EnumCache__Struct:
	.zero	4
	.globl	v8dbg_parent_DescriptorArray__HeapObject
	.section	.bss.v8dbg_parent_DescriptorArray__HeapObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_DescriptorArray__HeapObject, @object
	.size	v8dbg_parent_DescriptorArray__HeapObject, 4
v8dbg_parent_DescriptorArray__HeapObject:
	.zero	4
	.globl	v8dbg_parent_DependentCode__WeakFixedArray
	.section	.bss.v8dbg_parent_DependentCode__WeakFixedArray,"aw",@nobits
	.align 4
	.type	v8dbg_parent_DependentCode__WeakFixedArray, @object
	.size	v8dbg_parent_DependentCode__WeakFixedArray, 4
v8dbg_parent_DependentCode__WeakFixedArray:
	.zero	4
	.globl	v8dbg_parent_DeoptimizationData__FixedArray
	.section	.bss.v8dbg_parent_DeoptimizationData__FixedArray,"aw",@nobits
	.align 4
	.type	v8dbg_parent_DeoptimizationData__FixedArray, @object
	.size	v8dbg_parent_DeoptimizationData__FixedArray, 4
v8dbg_parent_DeoptimizationData__FixedArray:
	.zero	4
	.globl	v8dbg_parent_DataHandler__Struct
	.section	.bss.v8dbg_parent_DataHandler__Struct,"aw",@nobits
	.align 4
	.type	v8dbg_parent_DataHandler__Struct, @object
	.size	v8dbg_parent_DataHandler__Struct, 4
v8dbg_parent_DataHandler__Struct:
	.zero	4
	.globl	v8dbg_parent_ConsString__String
	.section	.bss.v8dbg_parent_ConsString__String,"aw",@nobits
	.align 4
	.type	v8dbg_parent_ConsString__String, @object
	.size	v8dbg_parent_ConsString__String, 4
v8dbg_parent_ConsString__String:
	.zero	4
	.globl	v8dbg_parent_CodeDataContainer__HeapObject
	.section	.bss.v8dbg_parent_CodeDataContainer__HeapObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_CodeDataContainer__HeapObject, @object
	.size	v8dbg_parent_CodeDataContainer__HeapObject, 4
v8dbg_parent_CodeDataContainer__HeapObject:
	.zero	4
	.globl	v8dbg_parent_Code__HeapObject
	.section	.bss.v8dbg_parent_Code__HeapObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_Code__HeapObject, @object
	.size	v8dbg_parent_Code__HeapObject, 4
v8dbg_parent_Code__HeapObject:
	.zero	4
	.globl	v8dbg_parent_ClassPositions__Struct
	.section	.bss.v8dbg_parent_ClassPositions__Struct,"aw",@nobits
	.align 4
	.type	v8dbg_parent_ClassPositions__Struct, @object
	.size	v8dbg_parent_ClassPositions__Struct, 4
v8dbg_parent_ClassPositions__Struct:
	.zero	4
	.globl	v8dbg_parent_Cell__HeapObject
	.section	.bss.v8dbg_parent_Cell__HeapObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_Cell__HeapObject, @object
	.size	v8dbg_parent_Cell__HeapObject, 4
v8dbg_parent_Cell__HeapObject:
	.zero	4
	.globl	v8dbg_parent_BytecodeArray__FixedArrayBase
	.section	.bss.v8dbg_parent_BytecodeArray__FixedArrayBase,"aw",@nobits
	.align 4
	.type	v8dbg_parent_BytecodeArray__FixedArrayBase, @object
	.size	v8dbg_parent_BytecodeArray__FixedArrayBase, 4
v8dbg_parent_BytecodeArray__FixedArrayBase:
	.zero	4
	.globl	v8dbg_parent_ByteArray__FixedArrayBase
	.section	.bss.v8dbg_parent_ByteArray__FixedArrayBase,"aw",@nobits
	.align 4
	.type	v8dbg_parent_ByteArray__FixedArrayBase, @object
	.size	v8dbg_parent_ByteArray__FixedArrayBase, 4
v8dbg_parent_ByteArray__FixedArrayBase:
	.zero	4
	.globl	v8dbg_parent_ArrayList__FixedArray
	.section	.bss.v8dbg_parent_ArrayList__FixedArray,"aw",@nobits
	.align 4
	.type	v8dbg_parent_ArrayList__FixedArray, @object
	.size	v8dbg_parent_ArrayList__FixedArray, 4
v8dbg_parent_ArrayList__FixedArray:
	.zero	4
	.globl	v8dbg_parent_AllocationSite__Struct
	.section	.bss.v8dbg_parent_AllocationSite__Struct,"aw",@nobits
	.align 4
	.type	v8dbg_parent_AllocationSite__Struct, @object
	.size	v8dbg_parent_AllocationSite__Struct, 4
v8dbg_parent_AllocationSite__Struct:
	.zero	4
	.globl	v8dbg_parent_AllocationMemento__Struct
	.section	.bss.v8dbg_parent_AllocationMemento__Struct,"aw",@nobits
	.align 4
	.type	v8dbg_parent_AllocationMemento__Struct, @object
	.size	v8dbg_parent_AllocationMemento__Struct, 4
v8dbg_parent_AllocationMemento__Struct:
	.zero	4
	.globl	v8dbg_parent_AccessorPair__Struct
	.section	.bss.v8dbg_parent_AccessorPair__Struct,"aw",@nobits
	.align 4
	.type	v8dbg_parent_AccessorPair__Struct, @object
	.size	v8dbg_parent_AccessorPair__Struct, 4
v8dbg_parent_AccessorPair__Struct:
	.zero	4
	.globl	v8dbg_parent_AbstractCode__HeapObject
	.section	.bss.v8dbg_parent_AbstractCode__HeapObject,"aw",@nobits
	.align 4
	.type	v8dbg_parent_AbstractCode__HeapObject, @object
	.size	v8dbg_parent_AbstractCode__HeapObject, 4
v8dbg_parent_AbstractCode__HeapObject:
	.zero	4
	.globl	v8dbg_type_WeakFixedArray__WEAK_FIXED_ARRAY_TYPE
	.section	.data.v8dbg_type_WeakFixedArray__WEAK_FIXED_ARRAY_TYPE,"aw"
	.align 4
	.type	v8dbg_type_WeakFixedArray__WEAK_FIXED_ARRAY_TYPE, @object
	.size	v8dbg_type_WeakFixedArray__WEAK_FIXED_ARRAY_TYPE, 4
v8dbg_type_WeakFixedArray__WEAK_FIXED_ARRAY_TYPE:
	.long	148
	.globl	v8dbg_type_WeakArrayList__WEAK_ARRAY_LIST_TYPE
	.section	.data.v8dbg_type_WeakArrayList__WEAK_ARRAY_LIST_TYPE,"aw"
	.align 4
	.type	v8dbg_type_WeakArrayList__WEAK_ARRAY_LIST_TYPE, @object
	.size	v8dbg_type_WeakArrayList__WEAK_ARRAY_LIST_TYPE, 4
v8dbg_type_WeakArrayList__WEAK_ARRAY_LIST_TYPE:
	.long	167
	.globl	v8dbg_type_UncompiledDataWithPreparseData__UNCOMPILED_DATA_WITH_PREPARSE_DATA_TYPE
	.section	.data.v8dbg_type_UncompiledDataWithPreparseData__UNCOMPILED_DATA_WITH_PREPARSE_DATA_TYPE,"aw"
	.align 4
	.type	v8dbg_type_UncompiledDataWithPreparseData__UNCOMPILED_DATA_WITH_PREPARSE_DATA_TYPE, @object
	.size	v8dbg_type_UncompiledDataWithPreparseData__UNCOMPILED_DATA_WITH_PREPARSE_DATA_TYPE, 4
v8dbg_type_UncompiledDataWithPreparseData__UNCOMPILED_DATA_WITH_PREPARSE_DATA_TYPE:
	.long	166
	.globl	v8dbg_type_UncompiledDataWithoutPreparseData__UNCOMPILED_DATA_WITHOUT_PREPARSE_DATA_TYPE
	.section	.data.v8dbg_type_UncompiledDataWithoutPreparseData__UNCOMPILED_DATA_WITHOUT_PREPARSE_DATA_TYPE,"aw"
	.align 4
	.type	v8dbg_type_UncompiledDataWithoutPreparseData__UNCOMPILED_DATA_WITHOUT_PREPARSE_DATA_TYPE, @object
	.size	v8dbg_type_UncompiledDataWithoutPreparseData__UNCOMPILED_DATA_WITHOUT_PREPARSE_DATA_TYPE, 4
v8dbg_type_UncompiledDataWithoutPreparseData__UNCOMPILED_DATA_WITHOUT_PREPARSE_DATA_TYPE:
	.long	165
	.globl	v8dbg_type_Tuple3__TUPLE3_TYPE
	.section	.data.v8dbg_type_Tuple3__TUPLE3_TYPE,"aw"
	.align 4
	.type	v8dbg_type_Tuple3__TUPLE3_TYPE, @object
	.size	v8dbg_type_Tuple3__TUPLE3_TYPE, 4
v8dbg_type_Tuple3__TUPLE3_TYPE:
	.long	103
	.globl	v8dbg_type_Tuple2__TUPLE2_TYPE
	.section	.data.v8dbg_type_Tuple2__TUPLE2_TYPE,"aw"
	.align 4
	.type	v8dbg_type_Tuple2__TUPLE2_TYPE, @object
	.size	v8dbg_type_Tuple2__TUPLE2_TYPE, 4
v8dbg_type_Tuple2__TUPLE2_TYPE:
	.long	102
	.globl	v8dbg_type_Symbol__SYMBOL_TYPE
	.section	.data.v8dbg_type_Symbol__SYMBOL_TYPE,"aw"
	.align 4
	.type	v8dbg_type_Symbol__SYMBOL_TYPE, @object
	.size	v8dbg_type_Symbol__SYMBOL_TYPE, 4
v8dbg_type_Symbol__SYMBOL_TYPE:
	.long	64
	.globl	v8dbg_type_SeqTwoByteString__STRING_TYPE
	.section	.data.v8dbg_type_SeqTwoByteString__STRING_TYPE,"aw"
	.align 4
	.type	v8dbg_type_SeqTwoByteString__STRING_TYPE, @object
	.size	v8dbg_type_SeqTwoByteString__STRING_TYPE, 4
v8dbg_type_SeqTwoByteString__STRING_TYPE:
	.long	32
	.globl	v8dbg_type_SourcePositionTableWithFrameCache__SOURCE_POSITION_TABLE_WITH_FRAME_CACHE_TYPE
	.section	.data.v8dbg_type_SourcePositionTableWithFrameCache__SOURCE_POSITION_TABLE_WITH_FRAME_CACHE_TYPE,"aw"
	.align 4
	.type	v8dbg_type_SourcePositionTableWithFrameCache__SOURCE_POSITION_TABLE_WITH_FRAME_CACHE_TYPE, @object
	.size	v8dbg_type_SourcePositionTableWithFrameCache__SOURCE_POSITION_TABLE_WITH_FRAME_CACHE_TYPE, 4
v8dbg_type_SourcePositionTableWithFrameCache__SOURCE_POSITION_TABLE_WITH_FRAME_CACHE_TYPE:
	.long	97
	.globl	v8dbg_type_SlicedString__SLICED_STRING_TYPE
	.section	.data.v8dbg_type_SlicedString__SLICED_STRING_TYPE,"aw"
	.align 4
	.type	v8dbg_type_SlicedString__SLICED_STRING_TYPE, @object
	.size	v8dbg_type_SlicedString__SLICED_STRING_TYPE, 4
v8dbg_type_SlicedString__SLICED_STRING_TYPE:
	.long	35
	.globl	v8dbg_type_SlicedString__SLICED_ONE_BYTE_STRING_TYPE
	.section	.data.v8dbg_type_SlicedString__SLICED_ONE_BYTE_STRING_TYPE,"aw"
	.align 4
	.type	v8dbg_type_SlicedString__SLICED_ONE_BYTE_STRING_TYPE, @object
	.size	v8dbg_type_SlicedString__SLICED_ONE_BYTE_STRING_TYPE, 4
v8dbg_type_SlicedString__SLICED_ONE_BYTE_STRING_TYPE:
	.long	43
	.globl	v8dbg_type_SharedFunctionInfo__SHARED_FUNCTION_INFO_TYPE
	.section	.data.v8dbg_type_SharedFunctionInfo__SHARED_FUNCTION_INFO_TYPE,"aw"
	.align 4
	.type	v8dbg_type_SharedFunctionInfo__SHARED_FUNCTION_INFO_TYPE, @object
	.size	v8dbg_type_SharedFunctionInfo__SHARED_FUNCTION_INFO_TYPE, 4
v8dbg_type_SharedFunctionInfo__SHARED_FUNCTION_INFO_TYPE:
	.long	160
	.globl	v8dbg_type_Script__SCRIPT_TYPE
	.section	.data.v8dbg_type_Script__SCRIPT_TYPE,"aw"
	.align 4
	.type	v8dbg_type_Script__SCRIPT_TYPE, @object
	.size	v8dbg_type_Script__SCRIPT_TYPE, 4
v8dbg_type_Script__SCRIPT_TYPE:
	.long	96
	.globl	v8dbg_type_ScopeInfo__SCOPE_INFO_TYPE
	.section	.data.v8dbg_type_ScopeInfo__SCOPE_INFO_TYPE,"aw"
	.align 4
	.type	v8dbg_type_ScopeInfo__SCOPE_INFO_TYPE, @object
	.size	v8dbg_type_ScopeInfo__SCOPE_INFO_TYPE, 4
v8dbg_type_ScopeInfo__SCOPE_INFO_TYPE:
	.long	136
	.globl	v8dbg_type_PreparseData__PREPARSE_DATA_TYPE
	.section	.data.v8dbg_type_PreparseData__PREPARSE_DATA_TYPE,"aw"
	.align 4
	.type	v8dbg_type_PreparseData__PREPARSE_DATA_TYPE, @object
	.size	v8dbg_type_PreparseData__PREPARSE_DATA_TYPE, 4
v8dbg_type_PreparseData__PREPARSE_DATA_TYPE:
	.long	157
	.globl	v8dbg_type_SeqOneByteString__ONE_BYTE_STRING_TYPE
	.section	.data.v8dbg_type_SeqOneByteString__ONE_BYTE_STRING_TYPE,"aw"
	.align 4
	.type	v8dbg_type_SeqOneByteString__ONE_BYTE_STRING_TYPE, @object
	.size	v8dbg_type_SeqOneByteString__ONE_BYTE_STRING_TYPE, 4
v8dbg_type_SeqOneByteString__ONE_BYTE_STRING_TYPE:
	.long	40
	.globl	v8dbg_type_InternalizedString__ONE_BYTE_INTERNALIZED_STRING_TYPE
	.section	.data.v8dbg_type_InternalizedString__ONE_BYTE_INTERNALIZED_STRING_TYPE,"aw"
	.align 4
	.type	v8dbg_type_InternalizedString__ONE_BYTE_INTERNALIZED_STRING_TYPE, @object
	.size	v8dbg_type_InternalizedString__ONE_BYTE_INTERNALIZED_STRING_TYPE, 4
v8dbg_type_InternalizedString__ONE_BYTE_INTERNALIZED_STRING_TYPE:
	.long	8
	.globl	v8dbg_type_Oddball__ODDBALL_TYPE
	.section	.data.v8dbg_type_Oddball__ODDBALL_TYPE,"aw"
	.align 4
	.type	v8dbg_type_Oddball__ODDBALL_TYPE, @object
	.size	v8dbg_type_Oddball__ODDBALL_TYPE, 4
v8dbg_type_Oddball__ODDBALL_TYPE:
	.long	67
	.globl	v8dbg_type_Map__MAP_TYPE
	.section	.data.v8dbg_type_Map__MAP_TYPE,"aw"
	.align 4
	.type	v8dbg_type_Map__MAP_TYPE, @object
	.size	v8dbg_type_Map__MAP_TYPE, 4
v8dbg_type_Map__MAP_TYPE:
	.long	68
	.globl	v8dbg_type_JSTypedArray__JS_TYPED_ARRAY_TYPE
	.section	.data.v8dbg_type_JSTypedArray__JS_TYPED_ARRAY_TYPE,"aw"
	.align 4
	.type	v8dbg_type_JSTypedArray__JS_TYPED_ARRAY_TYPE, @object
	.size	v8dbg_type_JSTypedArray__JS_TYPED_ARRAY_TYPE, 4
v8dbg_type_JSTypedArray__JS_TYPED_ARRAY_TYPE:
	.long	1086
	.globl	v8dbg_type_JSStringIterator__JS_STRING_ITERATOR_TYPE
	.section	.data.v8dbg_type_JSStringIterator__JS_STRING_ITERATOR_TYPE,"aw"
	.align 4
	.type	v8dbg_type_JSStringIterator__JS_STRING_ITERATOR_TYPE, @object
	.size	v8dbg_type_JSStringIterator__JS_STRING_ITERATOR_TYPE, 4
v8dbg_type_JSStringIterator__JS_STRING_ITERATOR_TYPE:
	.long	1080
	.globl	v8dbg_type_JSRegExp__JS_REGEXP_TYPE
	.section	.data.v8dbg_type_JSRegExp__JS_REGEXP_TYPE,"aw"
	.align 4
	.type	v8dbg_type_JSRegExp__JS_REGEXP_TYPE, @object
	.size	v8dbg_type_JSRegExp__JS_REGEXP_TYPE, 4
v8dbg_type_JSRegExp__JS_REGEXP_TYPE:
	.long	1075
	.globl	v8dbg_type_JSRegExpStringIterator__JS_REGEXP_STRING_ITERATOR_TYPE
	.section	.data.v8dbg_type_JSRegExpStringIterator__JS_REGEXP_STRING_ITERATOR_TYPE,"aw"
	.align 4
	.type	v8dbg_type_JSRegExpStringIterator__JS_REGEXP_STRING_ITERATOR_TYPE, @object
	.size	v8dbg_type_JSRegExpStringIterator__JS_REGEXP_STRING_ITERATOR_TYPE, 4
v8dbg_type_JSRegExpStringIterator__JS_REGEXP_STRING_ITERATOR_TYPE:
	.long	1076
	.globl	v8dbg_type_JSPromise__JS_PROMISE_TYPE
	.section	.data.v8dbg_type_JSPromise__JS_PROMISE_TYPE,"aw"
	.align 4
	.type	v8dbg_type_JSPromise__JS_PROMISE_TYPE, @object
	.size	v8dbg_type_JSPromise__JS_PROMISE_TYPE, 4
v8dbg_type_JSPromise__JS_PROMISE_TYPE:
	.long	1074
	.globl	v8dbg_type_JSPrimitiveWrapper__JS_PRIMITIVE_WRAPPER_TYPE
	.section	.data.v8dbg_type_JSPrimitiveWrapper__JS_PRIMITIVE_WRAPPER_TYPE,"aw"
	.align 4
	.type	v8dbg_type_JSPrimitiveWrapper__JS_PRIMITIVE_WRAPPER_TYPE, @object
	.size	v8dbg_type_JSPrimitiveWrapper__JS_PRIMITIVE_WRAPPER_TYPE, 4
v8dbg_type_JSPrimitiveWrapper__JS_PRIMITIVE_WRAPPER_TYPE:
	.long	1041
	.globl	v8dbg_type_JSObject__JS_OBJECT_TYPE
	.section	.data.v8dbg_type_JSObject__JS_OBJECT_TYPE,"aw"
	.align 4
	.type	v8dbg_type_JSObject__JS_OBJECT_TYPE, @object
	.size	v8dbg_type_JSObject__JS_OBJECT_TYPE, 4
v8dbg_type_JSObject__JS_OBJECT_TYPE:
	.long	1057
	.globl	v8dbg_type_JSMessageObject__JS_MESSAGE_OBJECT_TYPE
	.section	.data.v8dbg_type_JSMessageObject__JS_MESSAGE_OBJECT_TYPE,"aw"
	.align 4
	.type	v8dbg_type_JSMessageObject__JS_MESSAGE_OBJECT_TYPE, @object
	.size	v8dbg_type_JSMessageObject__JS_MESSAGE_OBJECT_TYPE, 4
v8dbg_type_JSMessageObject__JS_MESSAGE_OBJECT_TYPE:
	.long	1073
	.globl	v8dbg_type_JSGlobalProxy__JS_GLOBAL_PROXY_TYPE
	.section	.data.v8dbg_type_JSGlobalProxy__JS_GLOBAL_PROXY_TYPE,"aw"
	.align 4
	.type	v8dbg_type_JSGlobalProxy__JS_GLOBAL_PROXY_TYPE, @object
	.size	v8dbg_type_JSGlobalProxy__JS_GLOBAL_PROXY_TYPE, 4
v8dbg_type_JSGlobalProxy__JS_GLOBAL_PROXY_TYPE:
	.long	1026
	.globl	v8dbg_type_JSGlobalObject__JS_GLOBAL_OBJECT_TYPE
	.section	.data.v8dbg_type_JSGlobalObject__JS_GLOBAL_OBJECT_TYPE,"aw"
	.align 4
	.type	v8dbg_type_JSGlobalObject__JS_GLOBAL_OBJECT_TYPE, @object
	.size	v8dbg_type_JSGlobalObject__JS_GLOBAL_OBJECT_TYPE, 4
v8dbg_type_JSGlobalObject__JS_GLOBAL_OBJECT_TYPE:
	.long	1025
	.globl	v8dbg_type_JSFunction__JS_FUNCTION_TYPE
	.section	.data.v8dbg_type_JSFunction__JS_FUNCTION_TYPE,"aw"
	.align 4
	.type	v8dbg_type_JSFunction__JS_FUNCTION_TYPE, @object
	.size	v8dbg_type_JSFunction__JS_FUNCTION_TYPE, 4
v8dbg_type_JSFunction__JS_FUNCTION_TYPE:
	.long	1105
	.globl	v8dbg_type_JSDate__JS_DATE_TYPE
	.section	.data.v8dbg_type_JSDate__JS_DATE_TYPE,"aw"
	.align 4
	.type	v8dbg_type_JSDate__JS_DATE_TYPE, @object
	.size	v8dbg_type_JSDate__JS_DATE_TYPE, 4
v8dbg_type_JSDate__JS_DATE_TYPE:
	.long	1066
	.globl	v8dbg_type_JSDataView__JS_DATA_VIEW_TYPE
	.section	.data.v8dbg_type_JSDataView__JS_DATA_VIEW_TYPE,"aw"
	.align 4
	.type	v8dbg_type_JSDataView__JS_DATA_VIEW_TYPE, @object
	.size	v8dbg_type_JSDataView__JS_DATA_VIEW_TYPE, 4
v8dbg_type_JSDataView__JS_DATA_VIEW_TYPE:
	.long	1087
	.globl	v8dbg_type_JSBoundFunction__JS_BOUND_FUNCTION_TYPE
	.section	.data.v8dbg_type_JSBoundFunction__JS_BOUND_FUNCTION_TYPE,"aw"
	.align 4
	.type	v8dbg_type_JSBoundFunction__JS_BOUND_FUNCTION_TYPE, @object
	.size	v8dbg_type_JSBoundFunction__JS_BOUND_FUNCTION_TYPE, 4
v8dbg_type_JSBoundFunction__JS_BOUND_FUNCTION_TYPE:
	.long	1104
	.globl	v8dbg_type_JSAsyncFromSyncIterator__JS_ASYNC_FROM_SYNC_ITERATOR_TYPE
	.section	.data.v8dbg_type_JSAsyncFromSyncIterator__JS_ASYNC_FROM_SYNC_ITERATOR_TYPE,"aw"
	.align 4
	.type	v8dbg_type_JSAsyncFromSyncIterator__JS_ASYNC_FROM_SYNC_ITERATOR_TYPE, @object
	.size	v8dbg_type_JSAsyncFromSyncIterator__JS_ASYNC_FROM_SYNC_ITERATOR_TYPE, 4
v8dbg_type_JSAsyncFromSyncIterator__JS_ASYNC_FROM_SYNC_ITERATOR_TYPE:
	.long	1062
	.globl	v8dbg_type_JSArray__JS_ARRAY_TYPE
	.section	.data.v8dbg_type_JSArray__JS_ARRAY_TYPE,"aw"
	.align 4
	.type	v8dbg_type_JSArray__JS_ARRAY_TYPE, @object
	.size	v8dbg_type_JSArray__JS_ARRAY_TYPE, 4
v8dbg_type_JSArray__JS_ARRAY_TYPE:
	.long	1061
	.globl	v8dbg_type_JSArrayIterator__JS_ARRAY_ITERATOR_TYPE
	.section	.data.v8dbg_type_JSArrayIterator__JS_ARRAY_ITERATOR_TYPE,"aw"
	.align 4
	.type	v8dbg_type_JSArrayIterator__JS_ARRAY_ITERATOR_TYPE, @object
	.size	v8dbg_type_JSArrayIterator__JS_ARRAY_ITERATOR_TYPE, 4
v8dbg_type_JSArrayIterator__JS_ARRAY_ITERATOR_TYPE:
	.long	1060
	.globl	v8dbg_type_JSArrayBuffer__JS_ARRAY_BUFFER_TYPE
	.section	.data.v8dbg_type_JSArrayBuffer__JS_ARRAY_BUFFER_TYPE,"aw"
	.align 4
	.type	v8dbg_type_JSArrayBuffer__JS_ARRAY_BUFFER_TYPE, @object
	.size	v8dbg_type_JSArrayBuffer__JS_ARRAY_BUFFER_TYPE, 4
v8dbg_type_JSArrayBuffer__JS_ARRAY_BUFFER_TYPE:
	.long	1059
	.globl	v8dbg_type_InterpreterData__INTERPRETER_DATA_TYPE
	.section	.data.v8dbg_type_InterpreterData__INTERPRETER_DATA_TYPE,"aw"
	.align 4
	.type	v8dbg_type_InterpreterData__INTERPRETER_DATA_TYPE, @object
	.size	v8dbg_type_InterpreterData__INTERPRETER_DATA_TYPE, 4
v8dbg_type_InterpreterData__INTERPRETER_DATA_TYPE:
	.long	91
	.globl	v8dbg_type_HeapNumber__HEAP_NUMBER_TYPE
	.section	.data.v8dbg_type_HeapNumber__HEAP_NUMBER_TYPE,"aw"
	.align 4
	.type	v8dbg_type_HeapNumber__HEAP_NUMBER_TYPE, @object
	.size	v8dbg_type_HeapNumber__HEAP_NUMBER_TYPE, 4
v8dbg_type_HeapNumber__HEAP_NUMBER_TYPE:
	.long	65
	.globl	v8dbg_type_FixedDoubleArray__FIXED_DOUBLE_ARRAY_TYPE
	.section	.data.v8dbg_type_FixedDoubleArray__FIXED_DOUBLE_ARRAY_TYPE,"aw"
	.align 4
	.type	v8dbg_type_FixedDoubleArray__FIXED_DOUBLE_ARRAY_TYPE, @object
	.size	v8dbg_type_FixedDoubleArray__FIXED_DOUBLE_ARRAY_TYPE, 4
v8dbg_type_FixedDoubleArray__FIXED_DOUBLE_ARRAY_TYPE:
	.long	74
	.globl	v8dbg_type_FixedArray__FIXED_ARRAY_TYPE
	.section	.data.v8dbg_type_FixedArray__FIXED_ARRAY_TYPE,"aw"
	.align 4
	.type	v8dbg_type_FixedArray__FIXED_ARRAY_TYPE, @object
	.size	v8dbg_type_FixedArray__FIXED_ARRAY_TYPE, 4
v8dbg_type_FixedArray__FIXED_ARRAY_TYPE:
	.long	123
	.globl	v8dbg_type_FeedbackCell__FEEDBACK_CELL_TYPE
	.section	.data.v8dbg_type_FeedbackCell__FEEDBACK_CELL_TYPE,"aw"
	.align 4
	.type	v8dbg_type_FeedbackCell__FEEDBACK_CELL_TYPE, @object
	.size	v8dbg_type_FeedbackCell__FEEDBACK_CELL_TYPE, 4
v8dbg_type_FeedbackCell__FEEDBACK_CELL_TYPE:
	.long	154
	.globl	v8dbg_type_ExternalTwoByteString__EXTERNAL_STRING_TYPE
	.section	.data.v8dbg_type_ExternalTwoByteString__EXTERNAL_STRING_TYPE,"aw"
	.align 4
	.type	v8dbg_type_ExternalTwoByteString__EXTERNAL_STRING_TYPE, @object
	.size	v8dbg_type_ExternalTwoByteString__EXTERNAL_STRING_TYPE, 4
v8dbg_type_ExternalTwoByteString__EXTERNAL_STRING_TYPE:
	.long	34
	.globl	v8dbg_type_ExternalOneByteString__EXTERNAL_ONE_BYTE_STRING_TYPE
	.section	.data.v8dbg_type_ExternalOneByteString__EXTERNAL_ONE_BYTE_STRING_TYPE,"aw"
	.align 4
	.type	v8dbg_type_ExternalOneByteString__EXTERNAL_ONE_BYTE_STRING_TYPE, @object
	.size	v8dbg_type_ExternalOneByteString__EXTERNAL_ONE_BYTE_STRING_TYPE, 4
v8dbg_type_ExternalOneByteString__EXTERNAL_ONE_BYTE_STRING_TYPE:
	.long	42
	.globl	v8dbg_type_EnumCache__ENUM_CACHE_TYPE
	.section	.data.v8dbg_type_EnumCache__ENUM_CACHE_TYPE,"aw"
	.align 4
	.type	v8dbg_type_EnumCache__ENUM_CACHE_TYPE, @object
	.size	v8dbg_type_EnumCache__ENUM_CACHE_TYPE, 4
v8dbg_type_EnumCache__ENUM_CACHE_TYPE:
	.long	87
	.globl	v8dbg_type_DescriptorArray__DESCRIPTOR_ARRAY_TYPE
	.section	.data.v8dbg_type_DescriptorArray__DESCRIPTOR_ARRAY_TYPE,"aw"
	.align 4
	.type	v8dbg_type_DescriptorArray__DESCRIPTOR_ARRAY_TYPE, @object
	.size	v8dbg_type_DescriptorArray__DESCRIPTOR_ARRAY_TYPE, 4
v8dbg_type_DescriptorArray__DESCRIPTOR_ARRAY_TYPE:
	.long	153
	.globl	v8dbg_type_ConsString__CONS_STRING_TYPE
	.section	.data.v8dbg_type_ConsString__CONS_STRING_TYPE,"aw"
	.align 4
	.type	v8dbg_type_ConsString__CONS_STRING_TYPE, @object
	.size	v8dbg_type_ConsString__CONS_STRING_TYPE, 4
v8dbg_type_ConsString__CONS_STRING_TYPE:
	.long	33
	.globl	v8dbg_type_ConsString__CONS_ONE_BYTE_STRING_TYPE
	.section	.data.v8dbg_type_ConsString__CONS_ONE_BYTE_STRING_TYPE,"aw"
	.align 4
	.type	v8dbg_type_ConsString__CONS_ONE_BYTE_STRING_TYPE, @object
	.size	v8dbg_type_ConsString__CONS_ONE_BYTE_STRING_TYPE, 4
v8dbg_type_ConsString__CONS_ONE_BYTE_STRING_TYPE:
	.long	41
	.globl	v8dbg_type_Code__CODE_TYPE
	.section	.data.v8dbg_type_Code__CODE_TYPE,"aw"
	.align 4
	.type	v8dbg_type_Code__CODE_TYPE, @object
	.size	v8dbg_type_Code__CODE_TYPE, 4
v8dbg_type_Code__CODE_TYPE:
	.long	69
	.globl	v8dbg_type_CodeDataContainer__CODE_DATA_CONTAINER_TYPE
	.section	.data.v8dbg_type_CodeDataContainer__CODE_DATA_CONTAINER_TYPE,"aw"
	.align 4
	.type	v8dbg_type_CodeDataContainer__CODE_DATA_CONTAINER_TYPE, @object
	.size	v8dbg_type_CodeDataContainer__CODE_DATA_CONTAINER_TYPE, 4
v8dbg_type_CodeDataContainer__CODE_DATA_CONTAINER_TYPE:
	.long	152
	.globl	v8dbg_type_ClassPositions__CLASS_POSITIONS_TYPE
	.section	.data.v8dbg_type_ClassPositions__CLASS_POSITIONS_TYPE,"aw"
	.align 4
	.type	v8dbg_type_ClassPositions__CLASS_POSITIONS_TYPE, @object
	.size	v8dbg_type_ClassPositions__CLASS_POSITIONS_TYPE, 4
v8dbg_type_ClassPositions__CLASS_POSITIONS_TYPE:
	.long	85
	.globl	v8dbg_type_Cell__CELL_TYPE
	.section	.data.v8dbg_type_Cell__CELL_TYPE,"aw"
	.align 4
	.type	v8dbg_type_Cell__CELL_TYPE, @object
	.size	v8dbg_type_Cell__CELL_TYPE, 4
v8dbg_type_Cell__CELL_TYPE:
	.long	151
	.globl	v8dbg_type_ByteArray__BYTE_ARRAY_TYPE
	.section	.data.v8dbg_type_ByteArray__BYTE_ARRAY_TYPE,"aw"
	.align 4
	.type	v8dbg_type_ByteArray__BYTE_ARRAY_TYPE, @object
	.size	v8dbg_type_ByteArray__BYTE_ARRAY_TYPE, 4
v8dbg_type_ByteArray__BYTE_ARRAY_TYPE:
	.long	70
	.globl	v8dbg_type_BytecodeArray__BYTECODE_ARRAY_TYPE
	.section	.data.v8dbg_type_BytecodeArray__BYTECODE_ARRAY_TYPE,"aw"
	.align 4
	.type	v8dbg_type_BytecodeArray__BYTECODE_ARRAY_TYPE, @object
	.size	v8dbg_type_BytecodeArray__BYTECODE_ARRAY_TYPE, 4
v8dbg_type_BytecodeArray__BYTECODE_ARRAY_TYPE:
	.long	72
	.globl	v8dbg_type_AllocationSite__ALLOCATION_SITE_TYPE
	.section	.data.v8dbg_type_AllocationSite__ALLOCATION_SITE_TYPE,"aw"
	.align 4
	.type	v8dbg_type_AllocationSite__ALLOCATION_SITE_TYPE, @object
	.size	v8dbg_type_AllocationSite__ALLOCATION_SITE_TYPE, 4
v8dbg_type_AllocationSite__ALLOCATION_SITE_TYPE:
	.long	121
	.globl	v8dbg_type_AllocationMemento__ALLOCATION_MEMENTO_TYPE
	.section	.data.v8dbg_type_AllocationMemento__ALLOCATION_MEMENTO_TYPE,"aw"
	.align 4
	.type	v8dbg_type_AllocationMemento__ALLOCATION_MEMENTO_TYPE, @object
	.size	v8dbg_type_AllocationMemento__ALLOCATION_MEMENTO_TYPE, 4
v8dbg_type_AllocationMemento__ALLOCATION_MEMENTO_TYPE:
	.long	81
	.globl	v8dbg_type_AccessorPair__ACCESSOR_PAIR_TYPE
	.section	.data.v8dbg_type_AccessorPair__ACCESSOR_PAIR_TYPE,"aw"
	.align 4
	.type	v8dbg_type_AccessorPair__ACCESSOR_PAIR_TYPE, @object
	.size	v8dbg_type_AccessorPair__ACCESSOR_PAIR_TYPE, 4
v8dbg_type_AccessorPair__ACCESSOR_PAIR_TYPE:
	.long	79
	.globl	v8dbg_class_SharedFunctionInfo__function_data__Object
	.section	.data.v8dbg_class_SharedFunctionInfo__function_data__Object,"aw"
	.align 4
	.type	v8dbg_class_SharedFunctionInfo__function_data__Object, @object
	.size	v8dbg_class_SharedFunctionInfo__function_data__Object, 4
v8dbg_class_SharedFunctionInfo__function_data__Object:
	.long	8
	.globl	v8dbg_type_JSError__JS_ERROR_TYPE
	.section	.data.v8dbg_type_JSError__JS_ERROR_TYPE,"aw"
	.align 4
	.type	v8dbg_type_JSError__JS_ERROR_TYPE, @object
	.size	v8dbg_type_JSError__JS_ERROR_TYPE, 4
v8dbg_type_JSError__JS_ERROR_TYPE:
	.long	1067
	.globl	v8dbg_simplenumberdictionaryshape_entry_size
	.section	.data.v8dbg_simplenumberdictionaryshape_entry_size,"aw"
	.align 4
	.type	v8dbg_simplenumberdictionaryshape_entry_size, @object
	.size	v8dbg_simplenumberdictionaryshape_entry_size, 4
v8dbg_simplenumberdictionaryshape_entry_size:
	.long	2
	.globl	v8dbg_simplenumberdictionaryshape_prefix_size
	.section	.bss.v8dbg_simplenumberdictionaryshape_prefix_size,"aw",@nobits
	.align 4
	.type	v8dbg_simplenumberdictionaryshape_prefix_size, @object
	.size	v8dbg_simplenumberdictionaryshape_prefix_size, 4
v8dbg_simplenumberdictionaryshape_prefix_size:
	.zero	4
	.globl	v8dbg_numberdictionaryshape_entry_size
	.section	.data.v8dbg_numberdictionaryshape_entry_size,"aw"
	.align 4
	.type	v8dbg_numberdictionaryshape_entry_size, @object
	.size	v8dbg_numberdictionaryshape_entry_size, 4
v8dbg_numberdictionaryshape_entry_size:
	.long	3
	.globl	v8dbg_numberdictionaryshape_prefix_size
	.section	.data.v8dbg_numberdictionaryshape_prefix_size,"aw"
	.align 4
	.type	v8dbg_numberdictionaryshape_prefix_size, @object
	.size	v8dbg_numberdictionaryshape_prefix_size, 4
v8dbg_numberdictionaryshape_prefix_size:
	.long	1
	.globl	v8dbg_namedictionary_prefix_start_index
	.section	.data.v8dbg_namedictionary_prefix_start_index,"aw"
	.align 4
	.type	v8dbg_namedictionary_prefix_start_index, @object
	.size	v8dbg_namedictionary_prefix_start_index, 4
v8dbg_namedictionary_prefix_start_index:
	.long	3
	.globl	v8dbg_globaldictionaryshape_entry_size
	.section	.data.v8dbg_globaldictionaryshape_entry_size,"aw"
	.align 4
	.type	v8dbg_globaldictionaryshape_entry_size, @object
	.size	v8dbg_globaldictionaryshape_entry_size, 4
v8dbg_globaldictionaryshape_entry_size:
	.long	1
	.globl	v8dbg_namedictionaryshape_entry_size
	.section	.data.v8dbg_namedictionaryshape_entry_size,"aw"
	.align 4
	.type	v8dbg_namedictionaryshape_entry_size, @object
	.size	v8dbg_namedictionaryshape_entry_size, 4
v8dbg_namedictionaryshape_entry_size:
	.long	3
	.globl	v8dbg_namedictionaryshape_prefix_size
	.section	.data.v8dbg_namedictionaryshape_prefix_size,"aw"
	.align 4
	.type	v8dbg_namedictionaryshape_prefix_size, @object
	.size	v8dbg_namedictionaryshape_prefix_size, 4
v8dbg_namedictionaryshape_prefix_size:
	.long	2
	.globl	v8dbg_native_context_embedder_data_offset
	.section	.data.v8dbg_native_context_embedder_data_offset,"aw"
	.align 4
	.type	v8dbg_native_context_embedder_data_offset, @object
	.size	v8dbg_native_context_embedder_data_offset, 4
v8dbg_native_context_embedder_data_offset:
	.long	56
	.globl	v8dbg_context_min_slots
	.section	.data.v8dbg_context_min_slots,"aw"
	.align 4
	.type	v8dbg_context_min_slots, @object
	.size	v8dbg_context_min_slots, 4
v8dbg_context_min_slots:
	.long	4
	.globl	v8dbg_context_idx_ext
	.section	.data.v8dbg_context_idx_ext,"aw"
	.align 4
	.type	v8dbg_context_idx_ext, @object
	.size	v8dbg_context_idx_ext, 4
v8dbg_context_idx_ext:
	.long	2
	.globl	v8dbg_context_idx_prev
	.section	.data.v8dbg_context_idx_prev,"aw"
	.align 4
	.type	v8dbg_context_idx_prev, @object
	.size	v8dbg_context_idx_prev, 4
v8dbg_context_idx_prev:
	.long	1
	.globl	v8dbg_context_idx_native
	.section	.data.v8dbg_context_idx_native,"aw"
	.align 4
	.type	v8dbg_context_idx_native, @object
	.size	v8dbg_context_idx_native, 4
v8dbg_context_idx_native:
	.long	3
	.globl	v8dbg_context_idx_scope_info
	.section	.bss.v8dbg_context_idx_scope_info,"aw",@nobits
	.align 4
	.type	v8dbg_context_idx_scope_info, @object
	.size	v8dbg_context_idx_scope_info, 4
v8dbg_context_idx_scope_info:
	.zero	4
	.globl	v8dbg_jsarray_buffer_was_detached_shift
	.section	.data.v8dbg_jsarray_buffer_was_detached_shift,"aw"
	.align 4
	.type	v8dbg_jsarray_buffer_was_detached_shift, @object
	.size	v8dbg_jsarray_buffer_was_detached_shift, 4
v8dbg_jsarray_buffer_was_detached_shift:
	.long	2
	.globl	v8dbg_jsarray_buffer_was_detached_mask
	.section	.data.v8dbg_jsarray_buffer_was_detached_mask,"aw"
	.align 4
	.type	v8dbg_jsarray_buffer_was_detached_mask, @object
	.size	v8dbg_jsarray_buffer_was_detached_mask, 4
v8dbg_jsarray_buffer_was_detached_mask:
	.long	4
	.globl	v8dbg_scopeinfo_idx_first_vars
	.section	.data.v8dbg_scopeinfo_idx_first_vars,"aw"
	.align 4
	.type	v8dbg_scopeinfo_idx_first_vars, @object
	.size	v8dbg_scopeinfo_idx_first_vars, 4
v8dbg_scopeinfo_idx_first_vars:
	.long	3
	.globl	v8dbg_scopeinfo_idx_ncontextlocals
	.section	.data.v8dbg_scopeinfo_idx_ncontextlocals,"aw"
	.align 4
	.type	v8dbg_scopeinfo_idx_ncontextlocals, @object
	.size	v8dbg_scopeinfo_idx_ncontextlocals, 4
v8dbg_scopeinfo_idx_ncontextlocals:
	.long	2
	.globl	v8dbg_scopeinfo_idx_nparams
	.section	.data.v8dbg_scopeinfo_idx_nparams,"aw"
	.align 4
	.type	v8dbg_scopeinfo_idx_nparams, @object
	.size	v8dbg_scopeinfo_idx_nparams, 4
v8dbg_scopeinfo_idx_nparams:
	.long	1
	.globl	v8dbg_off_fp_args
	.section	.data.v8dbg_off_fp_args,"aw"
	.align 4
	.type	v8dbg_off_fp_args, @object
	.size	v8dbg_off_fp_args, 4
v8dbg_off_fp_args:
	.long	16
	.globl	v8dbg_off_fp_function
	.section	.data.v8dbg_off_fp_function,"aw"
	.align 4
	.type	v8dbg_off_fp_function, @object
	.size	v8dbg_off_fp_function, 4
v8dbg_off_fp_function:
	.long	-16
	.globl	v8dbg_off_fp_constant_pool
	.section	.bss.v8dbg_off_fp_constant_pool,"aw",@nobits
	.align 4
	.type	v8dbg_off_fp_constant_pool, @object
	.size	v8dbg_off_fp_constant_pool, 4
v8dbg_off_fp_constant_pool:
	.zero	4
	.globl	v8dbg_off_fp_context
	.section	.data.v8dbg_off_fp_context,"aw"
	.align 4
	.type	v8dbg_off_fp_context, @object
	.size	v8dbg_off_fp_context, 4
v8dbg_off_fp_context:
	.long	-8
	.globl	v8dbg_off_fp_context_or_frame_type
	.section	.data.v8dbg_off_fp_context_or_frame_type,"aw"
	.align 4
	.type	v8dbg_off_fp_context_or_frame_type, @object
	.size	v8dbg_off_fp_context_or_frame_type, 4
v8dbg_off_fp_context_or_frame_type:
	.long	-8
	.globl	v8dbg_class_Map__instance_descriptors_offset
	.section	.data.v8dbg_class_Map__instance_descriptors_offset,"aw"
	.align 4
	.type	v8dbg_class_Map__instance_descriptors_offset, @object
	.size	v8dbg_class_Map__instance_descriptors_offset, 4
v8dbg_class_Map__instance_descriptors_offset:
	.long	40
	.globl	v8dbg_bit_field3_number_of_own_descriptors_shift
	.section	.data.v8dbg_bit_field3_number_of_own_descriptors_shift,"aw"
	.align 4
	.type	v8dbg_bit_field3_number_of_own_descriptors_shift, @object
	.size	v8dbg_bit_field3_number_of_own_descriptors_shift, 4
v8dbg_bit_field3_number_of_own_descriptors_shift:
	.long	10
	.globl	v8dbg_bit_field3_number_of_own_descriptors_mask
	.section	.data.v8dbg_bit_field3_number_of_own_descriptors_mask,"aw"
	.align 4
	.type	v8dbg_bit_field3_number_of_own_descriptors_mask, @object
	.size	v8dbg_bit_field3_number_of_own_descriptors_mask, 4
v8dbg_bit_field3_number_of_own_descriptors_mask:
	.long	1047552
	.globl	v8dbg_bit_field3_is_dictionary_map_shift
	.section	.data.v8dbg_bit_field3_is_dictionary_map_shift,"aw"
	.align 4
	.type	v8dbg_bit_field3_is_dictionary_map_shift, @object
	.size	v8dbg_bit_field3_is_dictionary_map_shift, 4
v8dbg_bit_field3_is_dictionary_map_shift:
	.long	21
	.globl	v8dbg_bit_field2_elements_kind_shift
	.section	.data.v8dbg_bit_field2_elements_kind_shift,"aw"
	.align 4
	.type	v8dbg_bit_field2_elements_kind_shift, @object
	.size	v8dbg_bit_field2_elements_kind_shift, 4
v8dbg_bit_field2_elements_kind_shift:
	.long	3
	.globl	v8dbg_bit_field2_elements_kind_mask
	.section	.data.v8dbg_bit_field2_elements_kind_mask,"aw"
	.align 4
	.type	v8dbg_bit_field2_elements_kind_mask, @object
	.size	v8dbg_bit_field2_elements_kind_mask, 4
v8dbg_bit_field2_elements_kind_mask:
	.long	248
	.globl	v8dbg_elements_dictionary_elements
	.section	.data.v8dbg_elements_dictionary_elements,"aw"
	.align 4
	.type	v8dbg_elements_dictionary_elements, @object
	.size	v8dbg_elements_dictionary_elements, 4
v8dbg_elements_dictionary_elements:
	.long	12
	.globl	v8dbg_elements_fast_elements
	.section	.data.v8dbg_elements_fast_elements,"aw"
	.align 4
	.type	v8dbg_elements_fast_elements, @object
	.size	v8dbg_elements_fast_elements, 4
v8dbg_elements_fast_elements:
	.long	2
	.globl	v8dbg_elements_fast_holey_elements
	.section	.data.v8dbg_elements_fast_holey_elements,"aw"
	.align 4
	.type	v8dbg_elements_fast_holey_elements, @object
	.size	v8dbg_elements_fast_holey_elements, 4
v8dbg_elements_fast_holey_elements:
	.long	3
	.globl	v8dbg_prop_desc_size
	.section	.data.v8dbg_prop_desc_size,"aw"
	.align 4
	.type	v8dbg_prop_desc_size, @object
	.size	v8dbg_prop_desc_size, 4
v8dbg_prop_desc_size:
	.long	3
	.globl	v8dbg_prop_desc_value
	.section	.data.v8dbg_prop_desc_value,"aw"
	.align 4
	.type	v8dbg_prop_desc_value, @object
	.size	v8dbg_prop_desc_value, 4
v8dbg_prop_desc_value:
	.long	2
	.globl	v8dbg_prop_desc_details
	.section	.data.v8dbg_prop_desc_details,"aw"
	.align 4
	.type	v8dbg_prop_desc_details, @object
	.size	v8dbg_prop_desc_details, 4
v8dbg_prop_desc_details:
	.long	1
	.globl	v8dbg_prop_desc_key
	.section	.bss.v8dbg_prop_desc_key,"aw",@nobits
	.align 4
	.type	v8dbg_prop_desc_key, @object
	.size	v8dbg_prop_desc_key, 4
v8dbg_prop_desc_key:
	.zero	4
	.globl	v8dbg_prop_representation_tagged
	.section	.data.v8dbg_prop_representation_tagged,"aw"
	.align 4
	.type	v8dbg_prop_representation_tagged, @object
	.size	v8dbg_prop_representation_tagged, 4
v8dbg_prop_representation_tagged:
	.long	4
	.globl	v8dbg_prop_representation_heapobject
	.section	.data.v8dbg_prop_representation_heapobject,"aw"
	.align 4
	.type	v8dbg_prop_representation_heapobject, @object
	.size	v8dbg_prop_representation_heapobject, 4
v8dbg_prop_representation_heapobject:
	.long	3
	.globl	v8dbg_prop_representation_double
	.section	.data.v8dbg_prop_representation_double,"aw"
	.align 4
	.type	v8dbg_prop_representation_double, @object
	.size	v8dbg_prop_representation_double, 4
v8dbg_prop_representation_double:
	.long	2
	.globl	v8dbg_prop_representation_smi
	.section	.data.v8dbg_prop_representation_smi,"aw"
	.align 4
	.type	v8dbg_prop_representation_smi, @object
	.size	v8dbg_prop_representation_smi, 4
v8dbg_prop_representation_smi:
	.long	1
	.globl	v8dbg_prop_representation_shift
	.section	.data.v8dbg_prop_representation_shift,"aw"
	.align 4
	.type	v8dbg_prop_representation_shift, @object
	.size	v8dbg_prop_representation_shift, 4
v8dbg_prop_representation_shift:
	.long	6
	.globl	v8dbg_prop_representation_mask
	.section	.data.v8dbg_prop_representation_mask,"aw"
	.align 4
	.type	v8dbg_prop_representation_mask, @object
	.size	v8dbg_prop_representation_mask, 4
v8dbg_prop_representation_mask:
	.long	448
	.globl	v8dbg_prop_index_shift
	.section	.data.v8dbg_prop_index_shift,"aw"
	.align 4
	.type	v8dbg_prop_index_shift, @object
	.size	v8dbg_prop_index_shift, 4
v8dbg_prop_index_shift:
	.long	19
	.globl	v8dbg_prop_index_mask
	.section	.data.v8dbg_prop_index_mask,"aw"
	.align 4
	.type	v8dbg_prop_index_mask, @object
	.size	v8dbg_prop_index_mask, 4
v8dbg_prop_index_mask:
	.long	536346624
	.globl	v8dbg_prop_attributes_shift
	.section	.data.v8dbg_prop_attributes_shift,"aw"
	.align 4
	.type	v8dbg_prop_attributes_shift, @object
	.size	v8dbg_prop_attributes_shift, 4
v8dbg_prop_attributes_shift:
	.long	3
	.globl	v8dbg_prop_attributes_mask
	.section	.data.v8dbg_prop_attributes_mask,"aw"
	.align 4
	.type	v8dbg_prop_attributes_mask, @object
	.size	v8dbg_prop_attributes_mask, 4
v8dbg_prop_attributes_mask:
	.long	56
	.globl	v8dbg_prop_attributes_DONT_DELETE
	.section	.data.v8dbg_prop_attributes_DONT_DELETE,"aw"
	.align 4
	.type	v8dbg_prop_attributes_DONT_DELETE, @object
	.size	v8dbg_prop_attributes_DONT_DELETE, 4
v8dbg_prop_attributes_DONT_DELETE:
	.long	4
	.globl	v8dbg_prop_attributes_DONT_ENUM
	.section	.data.v8dbg_prop_attributes_DONT_ENUM,"aw"
	.align 4
	.type	v8dbg_prop_attributes_DONT_ENUM, @object
	.size	v8dbg_prop_attributes_DONT_ENUM, 4
v8dbg_prop_attributes_DONT_ENUM:
	.long	2
	.globl	v8dbg_prop_attributes_READ_ONLY
	.section	.data.v8dbg_prop_attributes_READ_ONLY,"aw"
	.align 4
	.type	v8dbg_prop_attributes_READ_ONLY, @object
	.size	v8dbg_prop_attributes_READ_ONLY, 4
v8dbg_prop_attributes_READ_ONLY:
	.long	1
	.globl	v8dbg_prop_attributes_NONE
	.section	.bss.v8dbg_prop_attributes_NONE,"aw",@nobits
	.align 4
	.type	v8dbg_prop_attributes_NONE, @object
	.size	v8dbg_prop_attributes_NONE, 4
v8dbg_prop_attributes_NONE:
	.zero	4
	.globl	v8dbg_prop_location_shift
	.section	.data.v8dbg_prop_location_shift,"aw"
	.align 4
	.type	v8dbg_prop_location_shift, @object
	.size	v8dbg_prop_location_shift, 4
v8dbg_prop_location_shift:
	.long	1
	.globl	v8dbg_prop_location_mask
	.section	.data.v8dbg_prop_location_mask,"aw"
	.align 4
	.type	v8dbg_prop_location_mask, @object
	.size	v8dbg_prop_location_mask, 4
v8dbg_prop_location_mask:
	.long	2
	.globl	v8dbg_prop_location_Field
	.section	.bss.v8dbg_prop_location_Field,"aw",@nobits
	.align 4
	.type	v8dbg_prop_location_Field, @object
	.size	v8dbg_prop_location_Field, 4
v8dbg_prop_location_Field:
	.zero	4
	.globl	v8dbg_prop_location_Descriptor
	.section	.data.v8dbg_prop_location_Descriptor,"aw"
	.align 4
	.type	v8dbg_prop_location_Descriptor, @object
	.size	v8dbg_prop_location_Descriptor, 4
v8dbg_prop_location_Descriptor:
	.long	1
	.globl	v8dbg_prop_kind_mask
	.section	.data.v8dbg_prop_kind_mask,"aw"
	.align 4
	.type	v8dbg_prop_kind_mask, @object
	.size	v8dbg_prop_kind_mask, 4
v8dbg_prop_kind_mask:
	.long	1
	.globl	v8dbg_prop_kind_Accessor
	.section	.data.v8dbg_prop_kind_Accessor,"aw"
	.align 4
	.type	v8dbg_prop_kind_Accessor, @object
	.size	v8dbg_prop_kind_Accessor, 4
v8dbg_prop_kind_Accessor:
	.long	1
	.globl	v8dbg_prop_kind_Data
	.section	.bss.v8dbg_prop_kind_Data,"aw",@nobits
	.align 4
	.type	v8dbg_prop_kind_Data, @object
	.size	v8dbg_prop_kind_Data, 4
v8dbg_prop_kind_Data:
	.zero	4
	.globl	v8dbg_OddballException
	.section	.data.v8dbg_OddballException,"aw"
	.align 4
	.type	v8dbg_OddballException, @object
	.size	v8dbg_OddballException, 4
v8dbg_OddballException:
	.long	8
	.globl	v8dbg_OddballOther
	.section	.data.v8dbg_OddballOther,"aw"
	.align 4
	.type	v8dbg_OddballOther, @object
	.size	v8dbg_OddballOther, 4
v8dbg_OddballOther:
	.long	7
	.globl	v8dbg_OddballUninitialized
	.section	.data.v8dbg_OddballUninitialized,"aw"
	.align 4
	.type	v8dbg_OddballUninitialized, @object
	.size	v8dbg_OddballUninitialized, 4
v8dbg_OddballUninitialized:
	.long	6
	.globl	v8dbg_OddballUndefined
	.section	.data.v8dbg_OddballUndefined,"aw"
	.align 4
	.type	v8dbg_OddballUndefined, @object
	.size	v8dbg_OddballUndefined, 4
v8dbg_OddballUndefined:
	.long	5
	.globl	v8dbg_OddballArgumentsMarker
	.section	.data.v8dbg_OddballArgumentsMarker,"aw"
	.align 4
	.type	v8dbg_OddballArgumentsMarker, @object
	.size	v8dbg_OddballArgumentsMarker, 4
v8dbg_OddballArgumentsMarker:
	.long	4
	.globl	v8dbg_OddballNull
	.section	.data.v8dbg_OddballNull,"aw"
	.align 4
	.type	v8dbg_OddballNull, @object
	.size	v8dbg_OddballNull, 4
v8dbg_OddballNull:
	.long	3
	.globl	v8dbg_OddballTheHole
	.section	.data.v8dbg_OddballTheHole,"aw"
	.align 4
	.type	v8dbg_OddballTheHole, @object
	.size	v8dbg_OddballTheHole, 4
v8dbg_OddballTheHole:
	.long	2
	.globl	v8dbg_OddballTrue
	.section	.data.v8dbg_OddballTrue,"aw"
	.align 4
	.type	v8dbg_OddballTrue, @object
	.size	v8dbg_OddballTrue, 4
v8dbg_OddballTrue:
	.long	1
	.globl	v8dbg_OddballFalse
	.section	.bss.v8dbg_OddballFalse,"aw",@nobits
	.align 4
	.type	v8dbg_OddballFalse, @object
	.size	v8dbg_OddballFalse, 4
v8dbg_OddballFalse:
	.zero	4
	.globl	v8dbg_TaggedSizeLog2
	.section	.data.v8dbg_TaggedSizeLog2,"aw"
	.align 4
	.type	v8dbg_TaggedSizeLog2, @object
	.size	v8dbg_TaggedSizeLog2, 4
v8dbg_TaggedSizeLog2:
	.long	3
	.globl	v8dbg_TaggedSize
	.section	.data.v8dbg_TaggedSize,"aw"
	.align 4
	.type	v8dbg_TaggedSize, @object
	.size	v8dbg_TaggedSize, 4
v8dbg_TaggedSize:
	.long	8
	.globl	v8dbg_SystemPointerSizeLog2
	.section	.data.v8dbg_SystemPointerSizeLog2,"aw"
	.align 4
	.type	v8dbg_SystemPointerSizeLog2, @object
	.size	v8dbg_SystemPointerSizeLog2, 4
v8dbg_SystemPointerSizeLog2:
	.long	3
	.globl	v8dbg_SystemPointerSize
	.section	.data.v8dbg_SystemPointerSize,"aw"
	.align 4
	.type	v8dbg_SystemPointerSize, @object
	.size	v8dbg_SystemPointerSize, 4
v8dbg_SystemPointerSize:
	.long	8
	.globl	v8dbg_SmiShiftSize
	.section	.data.v8dbg_SmiShiftSize,"aw"
	.align 4
	.type	v8dbg_SmiShiftSize, @object
	.size	v8dbg_SmiShiftSize, 4
v8dbg_SmiShiftSize:
	.long	31
	.globl	v8dbg_SmiValueShift
	.section	.data.v8dbg_SmiValueShift,"aw"
	.align 4
	.type	v8dbg_SmiValueShift, @object
	.size	v8dbg_SmiValueShift, 4
v8dbg_SmiValueShift:
	.long	1
	.globl	v8dbg_SmiTagMask
	.section	.data.v8dbg_SmiTagMask,"aw"
	.align 4
	.type	v8dbg_SmiTagMask, @object
	.size	v8dbg_SmiTagMask, 4
v8dbg_SmiTagMask:
	.long	1
	.globl	v8dbg_SmiTag
	.section	.bss.v8dbg_SmiTag,"aw",@nobits
	.align 4
	.type	v8dbg_SmiTag, @object
	.size	v8dbg_SmiTag, 4
v8dbg_SmiTag:
	.zero	4
	.globl	v8dbg_HeapObjectTagMask
	.section	.data.v8dbg_HeapObjectTagMask,"aw"
	.align 4
	.type	v8dbg_HeapObjectTagMask, @object
	.size	v8dbg_HeapObjectTagMask, 4
v8dbg_HeapObjectTagMask:
	.long	3
	.globl	v8dbg_HeapObjectTag
	.section	.data.v8dbg_HeapObjectTag,"aw"
	.align 4
	.type	v8dbg_HeapObjectTag, @object
	.size	v8dbg_HeapObjectTag, 4
v8dbg_HeapObjectTag:
	.long	1
	.globl	v8dbg_ThinStringTag
	.section	.data.v8dbg_ThinStringTag,"aw"
	.align 4
	.type	v8dbg_ThinStringTag, @object
	.size	v8dbg_ThinStringTag, 4
v8dbg_ThinStringTag:
	.long	5
	.globl	v8dbg_SlicedStringTag
	.section	.data.v8dbg_SlicedStringTag,"aw"
	.align 4
	.type	v8dbg_SlicedStringTag, @object
	.size	v8dbg_SlicedStringTag, 4
v8dbg_SlicedStringTag:
	.long	3
	.globl	v8dbg_ExternalStringTag
	.section	.data.v8dbg_ExternalStringTag,"aw"
	.align 4
	.type	v8dbg_ExternalStringTag, @object
	.size	v8dbg_ExternalStringTag, 4
v8dbg_ExternalStringTag:
	.long	2
	.globl	v8dbg_ConsStringTag
	.section	.data.v8dbg_ConsStringTag,"aw"
	.align 4
	.type	v8dbg_ConsStringTag, @object
	.size	v8dbg_ConsStringTag, 4
v8dbg_ConsStringTag:
	.long	1
	.globl	v8dbg_SeqStringTag
	.section	.bss.v8dbg_SeqStringTag,"aw",@nobits
	.align 4
	.type	v8dbg_SeqStringTag, @object
	.size	v8dbg_SeqStringTag, 4
v8dbg_SeqStringTag:
	.zero	4
	.globl	v8dbg_StringRepresentationMask
	.section	.data.v8dbg_StringRepresentationMask,"aw"
	.align 4
	.type	v8dbg_StringRepresentationMask, @object
	.size	v8dbg_StringRepresentationMask, 4
v8dbg_StringRepresentationMask:
	.long	7
	.globl	v8dbg_OneByteStringTag
	.section	.data.v8dbg_OneByteStringTag,"aw"
	.align 4
	.type	v8dbg_OneByteStringTag, @object
	.size	v8dbg_OneByteStringTag, 4
v8dbg_OneByteStringTag:
	.long	8
	.globl	v8dbg_TwoByteStringTag
	.section	.bss.v8dbg_TwoByteStringTag,"aw",@nobits
	.align 4
	.type	v8dbg_TwoByteStringTag, @object
	.size	v8dbg_TwoByteStringTag, 4
v8dbg_TwoByteStringTag:
	.zero	4
	.globl	v8dbg_StringEncodingMask
	.section	.data.v8dbg_StringEncodingMask,"aw"
	.align 4
	.type	v8dbg_StringEncodingMask, @object
	.size	v8dbg_StringEncodingMask, 4
v8dbg_StringEncodingMask:
	.long	8
	.globl	v8dbg_StringTag
	.section	.bss.v8dbg_StringTag,"aw",@nobits
	.align 4
	.type	v8dbg_StringTag, @object
	.size	v8dbg_StringTag, 4
v8dbg_StringTag:
	.zero	4
	.globl	v8dbg_IsNotStringMask
	.section	.data.v8dbg_IsNotStringMask,"aw"
	.align 4
	.type	v8dbg_IsNotStringMask, @object
	.size	v8dbg_IsNotStringMask, 4
v8dbg_IsNotStringMask:
	.long	-64
	.globl	v8dbg_LastContextType
	.section	.data.v8dbg_LastContextType,"aw"
	.align 4
	.type	v8dbg_LastContextType, @object
	.size	v8dbg_LastContextType, 4
v8dbg_LastContextType:
	.long	147
	.globl	v8dbg_FirstContextType
	.section	.data.v8dbg_FirstContextType,"aw"
	.align 4
	.type	v8dbg_FirstContextType, @object
	.size	v8dbg_FirstContextType, 4
v8dbg_FirstContextType:
	.long	138
	.globl	v8dbg_SpecialAPIObjectType
	.section	.data.v8dbg_SpecialAPIObjectType,"aw"
	.align 4
	.type	v8dbg_SpecialAPIObjectType, @object
	.size	v8dbg_SpecialAPIObjectType, 4
v8dbg_SpecialAPIObjectType:
	.long	1040
	.globl	v8dbg_APIObjectType
	.section	.data.v8dbg_APIObjectType,"aw"
	.align 4
	.type	v8dbg_APIObjectType, @object
	.size	v8dbg_APIObjectType, 4
v8dbg_APIObjectType:
	.long	1056
	.globl	v8dbg_FirstNonstringType
	.section	.data.v8dbg_FirstNonstringType,"aw"
	.align 4
	.type	v8dbg_FirstNonstringType, @object
	.size	v8dbg_FirstNonstringType, 4
v8dbg_FirstNonstringType:
	.long	64
	.globl	v8dbg_frametype_NativeFrame
	.section	.data.v8dbg_frametype_NativeFrame,"aw"
	.align 4
	.type	v8dbg_frametype_NativeFrame, @object
	.size	v8dbg_frametype_NativeFrame, 4
v8dbg_frametype_NativeFrame:
	.long	22
	.globl	v8dbg_frametype_BuiltinExitFrame
	.section	.data.v8dbg_frametype_BuiltinExitFrame,"aw"
	.align 4
	.type	v8dbg_frametype_BuiltinExitFrame, @object
	.size	v8dbg_frametype_BuiltinExitFrame, 4
v8dbg_frametype_BuiltinExitFrame:
	.long	21
	.globl	v8dbg_frametype_BuiltinFrame
	.section	.data.v8dbg_frametype_BuiltinFrame,"aw"
	.align 4
	.type	v8dbg_frametype_BuiltinFrame, @object
	.size	v8dbg_frametype_BuiltinFrame, 4
v8dbg_frametype_BuiltinFrame:
	.long	20
	.globl	v8dbg_frametype_ArgumentsAdaptorFrame
	.section	.data.v8dbg_frametype_ArgumentsAdaptorFrame,"aw"
	.align 4
	.type	v8dbg_frametype_ArgumentsAdaptorFrame, @object
	.size	v8dbg_frametype_ArgumentsAdaptorFrame, 4
v8dbg_frametype_ArgumentsAdaptorFrame:
	.long	19
	.globl	v8dbg_frametype_ConstructFrame
	.section	.data.v8dbg_frametype_ConstructFrame,"aw"
	.align 4
	.type	v8dbg_frametype_ConstructFrame, @object
	.size	v8dbg_frametype_ConstructFrame, 4
v8dbg_frametype_ConstructFrame:
	.long	18
	.globl	v8dbg_frametype_InternalFrame
	.section	.data.v8dbg_frametype_InternalFrame,"aw"
	.align 4
	.type	v8dbg_frametype_InternalFrame, @object
	.size	v8dbg_frametype_InternalFrame, 4
v8dbg_frametype_InternalFrame:
	.long	17
	.globl	v8dbg_frametype_JavaScriptBuiltinContinuationWithCatchFrame
	.section	.data.v8dbg_frametype_JavaScriptBuiltinContinuationWithCatchFrame,"aw"
	.align 4
	.type	v8dbg_frametype_JavaScriptBuiltinContinuationWithCatchFrame, @object
	.size	v8dbg_frametype_JavaScriptBuiltinContinuationWithCatchFrame, 4
v8dbg_frametype_JavaScriptBuiltinContinuationWithCatchFrame:
	.long	16
	.globl	v8dbg_frametype_JavaScriptBuiltinContinuationFrame
	.section	.data.v8dbg_frametype_JavaScriptBuiltinContinuationFrame,"aw"
	.align 4
	.type	v8dbg_frametype_JavaScriptBuiltinContinuationFrame, @object
	.size	v8dbg_frametype_JavaScriptBuiltinContinuationFrame, 4
v8dbg_frametype_JavaScriptBuiltinContinuationFrame:
	.long	15
	.globl	v8dbg_frametype_BuiltinContinuationFrame
	.section	.data.v8dbg_frametype_BuiltinContinuationFrame,"aw"
	.align 4
	.type	v8dbg_frametype_BuiltinContinuationFrame, @object
	.size	v8dbg_frametype_BuiltinContinuationFrame, 4
v8dbg_frametype_BuiltinContinuationFrame:
	.long	14
	.globl	v8dbg_frametype_StubFrame
	.section	.data.v8dbg_frametype_StubFrame,"aw"
	.align 4
	.type	v8dbg_frametype_StubFrame, @object
	.size	v8dbg_frametype_StubFrame, 4
v8dbg_frametype_StubFrame:
	.long	13
	.globl	v8dbg_frametype_InterpretedFrame
	.section	.data.v8dbg_frametype_InterpretedFrame,"aw"
	.align 4
	.type	v8dbg_frametype_InterpretedFrame, @object
	.size	v8dbg_frametype_InterpretedFrame, 4
v8dbg_frametype_InterpretedFrame:
	.long	12
	.globl	v8dbg_frametype_WasmCompileLazyFrame
	.section	.data.v8dbg_frametype_WasmCompileLazyFrame,"aw"
	.align 4
	.type	v8dbg_frametype_WasmCompileLazyFrame, @object
	.size	v8dbg_frametype_WasmCompileLazyFrame, 4
v8dbg_frametype_WasmCompileLazyFrame:
	.long	11
	.globl	v8dbg_frametype_WasmExitFrame
	.section	.data.v8dbg_frametype_WasmExitFrame,"aw"
	.align 4
	.type	v8dbg_frametype_WasmExitFrame, @object
	.size	v8dbg_frametype_WasmExitFrame, 4
v8dbg_frametype_WasmExitFrame:
	.long	10
	.globl	v8dbg_frametype_CWasmEntryFrame
	.section	.data.v8dbg_frametype_CWasmEntryFrame,"aw"
	.align 4
	.type	v8dbg_frametype_CWasmEntryFrame, @object
	.size	v8dbg_frametype_CWasmEntryFrame, 4
v8dbg_frametype_CWasmEntryFrame:
	.long	9
	.globl	v8dbg_frametype_WasmInterpreterEntryFrame
	.section	.data.v8dbg_frametype_WasmInterpreterEntryFrame,"aw"
	.align 4
	.type	v8dbg_frametype_WasmInterpreterEntryFrame, @object
	.size	v8dbg_frametype_WasmInterpreterEntryFrame, 4
v8dbg_frametype_WasmInterpreterEntryFrame:
	.long	8
	.globl	v8dbg_frametype_JsToWasmFrame
	.section	.data.v8dbg_frametype_JsToWasmFrame,"aw"
	.align 4
	.type	v8dbg_frametype_JsToWasmFrame, @object
	.size	v8dbg_frametype_JsToWasmFrame, 4
v8dbg_frametype_JsToWasmFrame:
	.long	7
	.globl	v8dbg_frametype_WasmToJsFrame
	.section	.data.v8dbg_frametype_WasmToJsFrame,"aw"
	.align 4
	.type	v8dbg_frametype_WasmToJsFrame, @object
	.size	v8dbg_frametype_WasmToJsFrame, 4
v8dbg_frametype_WasmToJsFrame:
	.long	6
	.globl	v8dbg_frametype_WasmCompiledFrame
	.section	.data.v8dbg_frametype_WasmCompiledFrame,"aw"
	.align 4
	.type	v8dbg_frametype_WasmCompiledFrame, @object
	.size	v8dbg_frametype_WasmCompiledFrame, 4
v8dbg_frametype_WasmCompiledFrame:
	.long	5
	.globl	v8dbg_frametype_OptimizedFrame
	.section	.data.v8dbg_frametype_OptimizedFrame,"aw"
	.align 4
	.type	v8dbg_frametype_OptimizedFrame, @object
	.size	v8dbg_frametype_OptimizedFrame, 4
v8dbg_frametype_OptimizedFrame:
	.long	4
	.globl	v8dbg_frametype_ExitFrame
	.section	.data.v8dbg_frametype_ExitFrame,"aw"
	.align 4
	.type	v8dbg_frametype_ExitFrame, @object
	.size	v8dbg_frametype_ExitFrame, 4
v8dbg_frametype_ExitFrame:
	.long	3
	.globl	v8dbg_frametype_ConstructEntryFrame
	.section	.data.v8dbg_frametype_ConstructEntryFrame,"aw"
	.align 4
	.type	v8dbg_frametype_ConstructEntryFrame, @object
	.size	v8dbg_frametype_ConstructEntryFrame, 4
v8dbg_frametype_ConstructEntryFrame:
	.long	2
	.globl	v8dbg_frametype_EntryFrame
	.section	.data.v8dbg_frametype_EntryFrame,"aw"
	.align 4
	.type	v8dbg_frametype_EntryFrame, @object
	.size	v8dbg_frametype_EntryFrame, 4
v8dbg_frametype_EntryFrame:
	.long	1
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
