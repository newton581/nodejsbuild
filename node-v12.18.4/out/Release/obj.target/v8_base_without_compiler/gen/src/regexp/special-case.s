	.file	"special-case.cc"
	.text
	.section	.text._ZN2v88internal18BuildSpecialAddSetEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18BuildSpecialAddSetEv
	.type	_ZN2v88internal18BuildSpecialAddSetEv, @function
_ZN2v88internal18BuildSpecialAddSetEv:
.LFB2230:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r12, %rdi
	movl	$197, %esi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movq	%r12, %rdi
	movl	$229, %esi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movq	%r12, %rdi
	movl	$920, %esi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movq	%r12, %rdi
	movl	$937, %esi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movq	%r12, %rdi
	movl	$952, %esi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movq	%r12, %rdi
	movl	$969, %esi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movq	%r12, %rdi
	movl	$977, %esi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2230:
	.size	_ZN2v88internal18BuildSpecialAddSetEv, .-_ZN2v88internal18BuildSpecialAddSetEv
	.section	.text._ZN2v88internal14BuildIgnoreSetEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal14BuildIgnoreSetEv
	.type	_ZN2v88internal14BuildIgnoreSetEv, @function
_ZN2v88internal14BuildIgnoreSetEv:
.LFB2229:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r12, %rdi
	movl	$1012, %esi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movq	%r12, %rdi
	movl	$8486, %esi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movq	%r12, %rdi
	movl	$8491, %edx
	movl	$8490, %esi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2229:
	.size	_ZN2v88internal14BuildIgnoreSetEv, .-_ZN2v88internal14BuildIgnoreSetEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
