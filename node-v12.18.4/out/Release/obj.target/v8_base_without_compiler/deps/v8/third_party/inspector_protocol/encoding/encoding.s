	.file	"encoding.cc"
	.text
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS_6StatusE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS_6StatusE, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS_6StatusE:
.LFB4477:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L1
	movq	%rsi, (%rax)
	movq	%rdx, 8(%rax)
	movq	8(%rdi), %rax
	movq	$0, 8(%rax)
	movq	(%rax), %rax
	movb	$0, (%rax)
.L1:
	ret
	.cfi_endproc
.LFE4477:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS_6StatusE, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS_6StatusE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE11HandleErrorENS_6StatusE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE11HandleErrorENS_6StatusE, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE11HandleErrorENS_6StatusE:
.LFB4489:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L4
	movq	%rsi, (%rax)
	movq	%rdx, 8(%rax)
	movq	8(%rdi), %rax
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	je	.L4
	movq	%rdx, 8(%rax)
.L4:
	ret
	.cfi_endproc
.LFE4489:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE11HandleErrorENS_6StatusE, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE11HandleErrorENS_6StatusE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEED2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEED2Ev, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEED2Ev:
.LFB4439:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEEE(%rip), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	ret
	.cfi_endproc
.LFE4439:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEED2Ev, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEED2Ev
	.set	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEED1Ev,_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEED2Ev
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev:
.LFB4435:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L10
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	ret
	.cfi_endproc
.LFE4435:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev
	.set	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED1Ev,_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEED0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEED0Ev, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEED0Ev:
.LFB4433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L13
	movq	104(%r12), %rax
	movq	72(%r12), %rbx
	leaq	8(%rax), %r13
	cmpq	%rbx, %r13
	jbe	.L14
	.p2align 4,,10
	.p2align 3
.L15:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r13
	ja	.L15
	movq	32(%r12), %rdi
.L14:
	call	_ZdlPv@PLT
.L13:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$112, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4433:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEED0Ev, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEED0Ev
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEED0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEED0Ev, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEED0Ev:
.LFB4441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L22
	call	_ZdlPv@PLT
.L22:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4441:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEED0Ev, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEED0Ev
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev:
.LFB4437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L28
	call	_ZdlPv@PLT
.L28:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4437:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE8HexToIntEh.part.0.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"static int v8_inspector_protocol_encoding::json::{anonymous}::JsonParser<Char>::HexToInt(Char) [with Char = unsigned char]"
	.align 8
.LC1:
	.string	"../deps/v8/third_party/inspector_protocol/encoding/encoding.cc"
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE8HexToIntEh.part.0.str1.1,"aMS",@progbits,1
.LC2:
	.string	"false"
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE8HexToIntEh.part.0,"ax",@progbits
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE8HexToIntEh.part.0, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE8HexToIntEh.part.0:
.LFB4711:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rcx
	movl	$1887, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4711:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE8HexToIntEh.part.0, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE8HexToIntEh.part.0
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE8HexToIntEt.part.0.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"static int v8_inspector_protocol_encoding::json::{anonymous}::JsonParser<Char>::HexToInt(Char) [with Char = short unsigned int]"
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE8HexToIntEt.part.0,"ax",@progbits
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE8HexToIntEt.part.0, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE8HexToIntEt.part.0:
.LFB4712:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC3(%rip), %rcx
	movl	$1887, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4712:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE8HexToIntEt.part.0, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE8HexToIntEt.part.0
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS_6StatusE.part.0.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"void v8_inspector_protocol_encoding::json::{anonymous}::JSONEncoder<C>::HandleError(v8_inspector_protocol_encoding::Status) [with C = std::__cxx11::basic_string<char>]"
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS_6StatusE.part.0.str1.1,"aMS",@progbits,1
.LC5:
	.string	"!error.ok()"
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS_6StatusE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS_6StatusE.part.0, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS_6StatusE.part.0:
.LFB4726:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rcx
	movl	$1517, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4726:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS_6StatusE.part.0, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS_6StatusE.part.0
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS_6StatusE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS_6StatusE, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS_6StatusE:
.LFB4453:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	je	.L44
	movq	24(%rdi), %rax
	movq	%rsi, (%rax)
	movq	%rdx, 8(%rax)
	movq	16(%rdi), %rax
	movq	$0, 8(%rax)
	movq	(%rax), %rax
	movb	$0, (%rax)
	ret
.L44:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS_6StatusE.part.0
	.cfi_endproc
.LFE4453:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS_6StatusE, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS_6StatusE
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS_5ErrorEPKh,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS_5ErrorEPKh, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS_5ErrorEPKh:
.LFB3860:
	.cfi_startproc
	cmpb	$0, 8(%rdi)
	jne	.L48
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	subq	(%rbx), %rdx
	movq	(%rdi), %rax
	call	*104(%rax)
	movb	$1, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE3860:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS_5ErrorEPKh, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS_5ErrorEPKh
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS_5ErrorEPKt,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS_5ErrorEPKt, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS_5ErrorEPKt:
.LFB3862:
	.cfi_startproc
	cmpb	$0, 8(%rdi)
	jne	.L54
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	subq	(%rbx), %rdx
	sarq	%rdx
	movq	(%rdi), %rax
	call	*104(%rax)
	movb	$1, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE3862:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS_5ErrorEPKt, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS_5ErrorEPKt
	.section	.rodata._ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8GetInt32Ev.part.0.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"int32_t v8_inspector_protocol_encoding::cbor::CBORTokenizer::GetInt32() const"
	.align 8
.LC7:
	.string	"token_tag_ == CBORTokenTag::INT32"
	.section	.text._ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8GetInt32Ev.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8GetInt32Ev.part.0, @function
_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8GetInt32Ev.part.0:
.LFB4729:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC6(%rip), %rcx
	movl	$673, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4729:
	.size	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8GetInt32Ev.part.0, .-_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8GetInt32Ev.part.0
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstIjEET_NS_4spanIhEE.part.0.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"T v8_inspector_protocol_encoding::cbor::{anonymous}::ReadBytesMostSignificantByteFirst(v8_inspector_protocol_encoding::span<unsigned char>) [with T = unsigned int]"
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstIjEET_NS_4spanIhEE.part.0.str1.1,"aMS",@progbits,1
.LC9:
	.string	"in.size() >= sizeof(T)"
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstIjEET_NS_4spanIhEE.part.0,"ax",@progbits
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstIjEET_NS_4spanIhEE.part.0, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstIjEET_NS_4spanIhEE.part.0:
.LFB4736:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC8(%rip), %rcx
	movl	$179, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4736:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstIjEET_NS_4spanIhEE.part.0, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstIjEET_NS_4spanIhEE.part.0
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstImEET_NS_4spanIhEE.part.0.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"T v8_inspector_protocol_encoding::cbor::{anonymous}::ReadBytesMostSignificantByteFirst(v8_inspector_protocol_encoding::span<unsigned char>) [with T = long unsigned int]"
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstImEET_NS_4spanIhEE.part.0,"ax",@progbits
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstImEET_NS_4spanIhEE.part.0, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstImEET_NS_4spanIhEE.part.0:
.LFB4737:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC10(%rip), %rcx
	movl	$179, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4737:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstImEET_NS_4spanIhEE.part.0, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstImEET_NS_4spanIhEE.part.0
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13EnterEnvelopeEv.part.0.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"void v8_inspector_protocol_encoding::cbor::CBORTokenizer::EnterEnvelope()"
	.align 8
.LC12:
	.string	"token_tag_ == CBORTokenTag::ENVELOPE"
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13EnterEnvelopeEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13EnterEnvelopeEv.part.0, @function
_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13EnterEnvelopeEv.part.0:
.LFB4740:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC11(%rip), %rcx
	movl	$657, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4740:
	.size	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13EnterEnvelopeEv.part.0, .-_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13EnterEnvelopeEv.part.0
	.section	.rodata._ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer10GetString8Ev.part.0.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"v8_inspector_protocol_encoding::span<unsigned char> v8_inspector_protocol_encoding::cbor::CBORTokenizer::GetString8() const"
	.align 8
.LC14:
	.string	"token_tag_ == CBORTokenTag::STRING8"
	.section	.text._ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer10GetString8Ev.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer10GetString8Ev.part.0, @function
_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer10GetString8Ev.part.0:
.LFB4742:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC13(%rip), %rcx
	movl	$693, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4742:
	.size	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer10GetString8Ev.part.0, .-_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer10GetString8Ev.part.0
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_115ParseUTF8StringEPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE.part.0.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"bool v8_inspector_protocol_encoding::cbor::{anonymous}::ParseUTF8String(v8_inspector_protocol_encoding::cbor::CBORTokenizer*, v8_inspector_protocol_encoding::StreamingParserHandler*)"
	.align 8
.LC16:
	.string	"tokenizer->TokenTag() == CBORTokenTag::STRING8"
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_115ParseUTF8StringEPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE.part.0,"ax",@progbits
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_115ParseUTF8StringEPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE.part.0, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_115ParseUTF8StringEPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE.part.0:
.LFB4743:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC15(%rip), %rcx
	movl	$950, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4743:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_115ParseUTF8StringEPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE.part.0, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_115ParseUTF8StringEPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE.part.0
	.section	.rodata._ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer18GetString16WireRepEv.part.0.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"v8_inspector_protocol_encoding::span<unsigned char> v8_inspector_protocol_encoding::cbor::CBORTokenizer::GetString16WireRep() const"
	.align 8
.LC18:
	.string	"token_tag_ == CBORTokenTag::STRING16"
	.section	.text._ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer18GetString16WireRepEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer18GetString16WireRepEv.part.0, @function
_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer18GetString16WireRepEv.part.0:
.LFB4744:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC17(%rip), %rcx
	movl	$699, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC18(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4744:
	.size	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer18GetString16WireRepEv.part.0, .-_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer18GetString16WireRepEv.part.0
	.section	.rodata._ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer9GetBinaryEv.part.0.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"v8_inspector_protocol_encoding::span<unsigned char> v8_inspector_protocol_encoding::cbor::CBORTokenizer::GetBinary() const"
	.align 8
.LC20:
	.string	"token_tag_ == CBORTokenTag::BINARY"
	.section	.text._ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer9GetBinaryEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer9GetBinaryEv.part.0, @function
_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer9GetBinaryEv.part.0:
.LFB4745:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC19(%rip), %rcx
	movl	$705, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC20(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4745:
	.size	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer9GetBinaryEv.part.0, .-_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer9GetBinaryEv.part.0
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleErrorENS_6StatusE.part.0.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"void v8_inspector_protocol_encoding::json::{anonymous}::JSONEncoder<C>::HandleError(v8_inspector_protocol_encoding::Status) [with C = std::vector<unsigned char>]"
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleErrorENS_6StatusE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleErrorENS_6StatusE.part.0, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleErrorENS_6StatusE.part.0:
.LFB4881:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC21(%rip), %rcx
	movl	$1517, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4881:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleErrorENS_6StatusE.part.0, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleErrorENS_6StatusE.part.0
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleErrorENS_6StatusE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleErrorENS_6StatusE, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleErrorENS_6StatusE:
.LFB4465:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	je	.L81
	movq	24(%rdi), %rax
	movq	%rsi, (%rax)
	movq	%rdx, 8(%rax)
	movq	16(%rdi), %rax
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	je	.L75
	movq	%rdx, 8(%rax)
.L75:
	ret
.L81:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleErrorENS_6StatusE.part.0
	.cfi_endproc
.LFE4465:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleErrorENS_6StatusE, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleErrorENS_6StatusE
	.section	.text._ZNSt11_Deque_baseIN30v8_inspector_protocol_encoding4json12_GLOBAL__N_15StateESaIS3_EE17_M_initialize_mapEm.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt11_Deque_baseIN30v8_inspector_protocol_encoding4json12_GLOBAL__N_15StateESaIS3_EE17_M_initialize_mapEm.constprop.0, @function
_ZNSt11_Deque_baseIN30v8_inspector_protocol_encoding4json12_GLOBAL__N_15StateESaIS3_EE17_M_initialize_mapEm.constprop.0:
.LFB4944:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	$8, 8(%rdi)
	movl	$64, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	movl	$512, %edi
	movq	%rax, (%rbx)
	leaq	-4(,%rdx,4), %r12
	andq	$-8, %r12
	addq	%rax, %r12
	call	_Znwm@PLT
	movq	%r12, 40(%rbx)
	movq	%rax, %xmm0
	leaq	512(%rax), %rdx
	movq	%rax, (%r12)
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, 72(%rbx)
	movq	%rdx, 32(%rbx)
	movq	%rax, 56(%rbx)
	movq	%rdx, 64(%rbx)
	movq	%rax, 48(%rbx)
	movups	%xmm0, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4944:
	.size	_ZNSt11_Deque_baseIN30v8_inspector_protocol_encoding4json12_GLOBAL__N_15StateESaIS3_EE17_M_initialize_mapEm.constprop.0, .-_ZNSt11_Deque_baseIN30v8_inspector_protocol_encoding4json12_GLOBAL__N_15StateESaIS3_EE17_M_initialize_mapEm.constprop.0
	.section	.rodata._ZNSt5dequeIN30v8_inspector_protocol_encoding4json12_GLOBAL__N_15StateESaIS3_EE12emplace_backIJNS2_9ContainerEEEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC22:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIN30v8_inspector_protocol_encoding4json12_GLOBAL__N_15StateESaIS3_EE12emplace_backIJNS2_9ContainerEEEEvDpOT_,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt5dequeIN30v8_inspector_protocol_encoding4json12_GLOBAL__N_15StateESaIS3_EE12emplace_backIJNS2_9ContainerEEEEvDpOT_, @function
_ZNSt5dequeIN30v8_inspector_protocol_encoding4json12_GLOBAL__N_15StateESaIS3_EE12emplace_backIJNS2_9ContainerEEEEvDpOT_:
.LFB4020:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	64(%rdi), %rcx
	movq	48(%rdi), %rax
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L85
	movl	(%rsi), %edx
	movl	$0, 4(%rax)
	movl	%edx, (%rax)
	addq	$8, 48(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	movq	72(%rdi), %r14
	movq	40(%rdi), %rsi
	subq	56(%rdi), %rax
	movq	%r14, %r13
	sarq	$3, %rax
	subq	%rsi, %r13
	movq	%r13, %rcx
	sarq	$3, %rcx
	leaq	-1(%rcx), %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movq	32(%rdi), %rax
	subq	16(%rdi), %rax
	movabsq	$1152921504606846975, %rdi
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rdi, %rax
	je	.L95
	movq	(%rbx), %r8
	movq	8(%rbx), %rdx
	movq	%r14, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L96
.L88:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r14)
	movl	(%r12), %edx
	movq	48(%rbx), %rax
	movl	%edx, (%rax)
	movl	$0, 4(%rax)
	movq	72(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 72(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 56(%rbx)
	movq	%rdx, 64(%rbx)
	movq	%rax, 48(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	leaq	2(%rcx), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L97
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	cmpq	%rdi, %r14
	ja	.L98
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	40(%rbx), %rsi
	movq	%rax, %rcx
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rcx,%rax,8), %r15
	movq	72(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L93
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L93:
	movq	(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 8(%rbx)
	movq	%rax, (%rbx)
.L91:
	movq	%r15, 40(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r13), %r14
	movq	(%r15), %xmm0
	movq	%r14, 72(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	(%r14), %rax
	movq	%rax, 56(%rbx)
	addq	$512, %rax
	movq	%rax, 64(%rbx)
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L97:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L90
	cmpq	%r14, %rsi
	je	.L91
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L90:
	cmpq	%r14, %rsi
	je	.L91
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L91
.L95:
	leaq	.LC22(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L98:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE4020:
	.size	_ZNSt5dequeIN30v8_inspector_protocol_encoding4json12_GLOBAL__N_15StateESaIS3_EE12emplace_backIJNS2_9ContainerEEEEvDpOT_, .-_ZNSt5dequeIN30v8_inspector_protocol_encoding4json12_GLOBAL__N_15StateESaIS3_EE12emplace_backIJNS2_9ContainerEEEEvDpOT_
	.section	.text._ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0:
.LFB4938:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$232, %rsp
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L100
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L100:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	leaq	23(%rsi), %rax
	movq	%rsp, %rdi
	movq	%rax, %rdx
	andq	$-4096, %rax
	subq	%rax, %rdi
	andq	$-16, %rdx
	movq	%rdi, %rax
	cmpq	%rax, %rsp
	je	.L102
.L116:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L116
.L102:
	andl	$4095, %edx
	subq	%rdx, %rsp
	testq	%rdx, %rdx
	jne	.L117
.L103:
	leaq	15(%rsp), %r14
	leaq	16(%rbp), %rax
	movq	%rcx, %r8
	movl	$1, %edx
	andq	$-16, %r14
	movq	%rax, -232(%rbp)
	leaq	-240(%rbp), %r9
	leaq	-208(%rbp), %rax
	movq	%r14, %rdi
	movq	$-1, %rcx
	movl	$32, -240(%rbp)
	movl	$48, -236(%rbp)
	movq	%rax, -224(%rbp)
	call	__vsnprintf_chk@PLT
	leaq	16(%r12), %rdi
	movslq	%eax, %r13
	movq	%rdi, (%r12)
	movq	%r13, -248(%rbp)
	cmpq	$15, %r13
	ja	.L118
	cmpq	$1, %r13
	jne	.L106
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L107:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L119
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L107
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L118:
	movq	%r12, %rdi
	leaq	-248(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-248(%rbp), %rax
	movq	%rax, 16(%r12)
.L105:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-248(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L117:
	orq	$0, -8(%rsp,%rdx)
	jmp	.L103
.L119:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4938:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev:
.LFB4427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L120
	movq	104(%rbx), %rax
	movq	72(%rbx), %r12
	leaq	8(%rax), %r13
	cmpq	%r12, %r13
	jbe	.L122
	.p2align 4,,10
	.p2align 3
.L123:
	movq	(%r12), %rdi
	addq	$8, %r12
	call	_ZdlPv@PLT
	cmpq	%r12, %r13
	ja	.L123
	movq	32(%rbx), %rdi
.L122:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4427:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev
	.set	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED1Ev,_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEED2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEED2Ev, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEED2Ev:
.LFB4431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L126
	movq	104(%rbx), %rax
	movq	72(%rbx), %r12
	leaq	8(%rax), %r13
	cmpq	%r12, %r13
	jbe	.L128
	.p2align 4,,10
	.p2align 3
.L129:
	movq	(%r12), %rdi
	addq	$8, %r12
	call	_ZdlPv@PLT
	cmpq	%r12, %r13
	ja	.L129
	movq	32(%rbx), %rdi
.L128:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4431:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEED2Ev, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEED2Ev
	.set	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEED1Ev,_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEED2Ev
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev:
.LFB4429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L133
	movq	104(%r12), %rax
	movq	72(%r12), %rbx
	leaq	8(%rax), %r13
	cmpq	%rbx, %r13
	jbe	.L134
	.p2align 4,,10
	.p2align 3
.L135:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r13
	ja	.L135
	movq	32(%r12), %rdi
.L134:
	call	_ZdlPv@PLT
.L133:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$112, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4429:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS5_PS5_S6_.str1.1,"aMS",@progbits,1
.LC23:
	.string	"null"
.LC24:
	.string	"true"
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS5_PS5_S6_,"ax",@progbits
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS5_PS5_S6_, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS5_PS5_S6_:
.LFB4021:
	.cfi_startproc
	cmpq	%rsi, %rdi
	jnb	.L142
	.p2align 4,,10
	.p2align 3
.L143:
	movzbl	(%rdi), %eax
	cmpb	$32, %al
	je	.L144
	leal	-9(%rax), %r8d
	cmpb	$4, %r8b
	jbe	.L144
	cmpb	$47, %al
	jne	.L142
	cmpq	%rdi, %rsi
	je	.L150
	leaq	1(%rdi), %rax
	cmpq	%rax, %rsi
	jbe	.L148
	movzbl	1(%rdi), %eax
	cmpb	$47, %al
	je	.L269
	cmpb	$42, %al
	je	.L270
.L148:
	movq	%rdi, (%rdx)
.L199:
	movzbl	(%rdi), %edx
	leal	-34(%rdx), %eax
	cmpb	$91, %al
	ja	.L237
	leaq	.L157(%rip), %r8
	movzbl	%al, %eax
	movslq	(%r8,%rax,4), %rax
	addq	%r8, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS5_PS5_S6_,"a",@progbits
	.align 4
	.align 4
.L157:
	.long	.L167-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L166-.L157
	.long	.L165-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L165-.L157
	.long	.L165-.L157
	.long	.L165-.L157
	.long	.L165-.L157
	.long	.L165-.L157
	.long	.L165-.L157
	.long	.L165-.L157
	.long	.L165-.L157
	.long	.L165-.L157
	.long	.L165-.L157
	.long	.L164-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L163-.L157
	.long	.L237-.L157
	.long	.L162-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L204-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L205-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L206-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L237-.L157
	.long	.L158-.L157
	.long	.L237-.L157
	.long	.L156-.L157
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS5_PS5_S6_
	.p2align 4,,10
	.p2align 3
.L280:
	movzbl	(%rdi), %r9d
	addq	$1, %rdx
	movsbl	%al, %r8d
	addq	$1, %rdi
	movzbl	(%rdx), %eax
	cmpl	%r8d, %r9d
	je	.L160
.L168:
	testb	%al, %al
	je	.L169
.L237:
	movl	$11, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	addq	$1, %rdi
.L146:
	cmpq	%rdi, %rsi
	ja	.L143
.L142:
	movq	%rdi, (%rdx)
	cmpq	%rdi, %rsi
	jne	.L199
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L269:
	addq	$2, %rdi
	cmpq	%rdi, %rsi
	jbe	.L150
	.p2align 4,,10
	.p2align 3
.L151:
	movzbl	(%rdi), %eax
	addq	$1, %rdi
	cmpb	$10, %al
	je	.L146
	cmpb	$13, %al
	je	.L146
	cmpq	%rdi, %rsi
	jne	.L151
	.p2align 4,,10
	.p2align 3
.L150:
	movq	%rsi, (%rdx)
.L266:
	movl	$12, %eax
.L141:
	ret
	.p2align 4,,10
	.p2align 3
.L270:
	leaq	2(%rdi), %r8
	cmpq	%r8, %rsi
	ja	.L152
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L153:
	movq	%rax, %r8
.L152:
	leaq	1(%r8), %rax
	movzbl	-1(%rax), %r9d
	cmpq	%rax, %rsi
	je	.L148
	cmpb	$42, %r9b
	jne	.L153
	cmpb	$47, (%rax)
	jne	.L153
	leaq	2(%r8), %rdi
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L165:
	cmpb	$45, %dl
	je	.L271
.L174:
	cmpq	%rdi, %rsi
	jbe	.L237
	xorl	%r8d, %r8d
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L273:
	addq	$1, %rdi
	addl	$1, %r8d
	cmpq	%rdi, %rsi
	je	.L272
.L177:
	movzbl	(%rdi), %eax
	subl	$48, %eax
	cmpb	$9, %al
	jbe	.L273
	movl	$11, %eax
	testl	%r8d, %r8d
	je	.L141
	cmpb	$48, %dl
	jne	.L239
	cmpl	$1, %r8d
	jne	.L141
.L239:
	cmpq	%rdi, %rsi
	je	.L182
	movzbl	(%rdi), %edx
	cmpb	$46, %dl
	je	.L274
.L180:
	andl	$-33, %edx
	cmpb	$69, %dl
	jne	.L185
	leaq	1(%rdi), %rdx
	cmpq	%rdx, %rsi
	je	.L237
	movzbl	1(%rdi), %eax
	subl	$43, %eax
	testb	$-3, %al
	jne	.L186
	leaq	2(%rdi), %rdx
	cmpq	%rdx, %rsi
	je	.L237
.L186:
	cmpq	%rdx, %rsi
	jbe	.L237
	xorl	%edi, %edi
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L276:
	addq	$1, %rdx
	addl	$1, %edi
	cmpq	%rdx, %rsi
	je	.L275
.L188:
	movzbl	(%rdx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	jbe	.L276
	movl	$11, %eax
	testl	%edi, %edi
	je	.L141
	movq	%rdx, %rdi
.L185:
	movq	%rdi, (%rcx)
	movl	$5, %eax
	ret
.L162:
	addq	$1, %rdi
	movl	$3, %eax
	movq	%rdi, (%rcx)
	ret
.L164:
	addq	$1, %rdi
	movl	$10, %eax
	movq	%rdi, (%rcx)
	ret
.L167:
	addq	$1, %rdi
	cmpq	%rsi, %rdi
	jnb	.L237
	leaq	.L193(%rip), %r8
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L191:
	cmpb	$34, %al
	je	.L277
.L236:
	movq	%rdx, %rdi
.L190:
	cmpq	%rdi, %rsi
	jbe	.L237
.L189:
	movzbl	(%rdi), %eax
	leaq	1(%rdi), %rdx
	cmpb	$92, %al
	jne	.L191
	cmpq	%rdx, %rsi
	je	.L237
	movzbl	1(%rdi), %eax
	leaq	2(%rdi), %rdx
	subl	$34, %eax
	cmpb	$86, %al
	ja	.L237
	movzbl	%al, %eax
	movslq	(%r8,%rax,4), %rax
	addq	%r8, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS5_PS5_S6_
	.align 4
	.align 4
.L193:
	.long	.L236-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L236-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L236-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L236-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L236-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L236-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L237-.L193
	.long	.L236-.L193
	.long	.L237-.L193
	.long	.L236-.L193
	.long	.L194-.L193
	.long	.L236-.L193
	.long	.L237-.L193
	.long	.L192-.L193
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS5_PS5_S6_
.L166:
	addq	$1, %rdi
	movl	$9, %eax
	movq	%rdi, (%rcx)
	ret
.L156:
	addq	$1, %rdi
	movl	$1, %eax
	movq	%rdi, (%rcx)
	ret
.L158:
	addq	$1, %rdi
	xorl	%eax, %eax
	movq	%rdi, (%rcx)
	ret
.L163:
	addq	$1, %rdi
	movl	$2, %eax
	movq	%rdi, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	leaq	1(%rdi), %r8
	movl	$11, %eax
	cmpq	%r8, %rsi
	je	.L141
	movzbl	1(%rdi), %edx
	movq	%r8, %rdi
	jmp	.L174
.L204:
	movl	$102, %eax
	leaq	.LC2(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L161:
	cmpq	%rdi, %rsi
	jbe	.L172
	testb	%al, %al
	jne	.L278
.L173:
	movq	%rdi, (%rcx)
	movl	$7, %eax
	ret
.L206:
	movl	$116, %eax
	leaq	.LC24(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L159:
	cmpq	%rdi, %rsi
	jbe	.L170
	testb	%al, %al
	jne	.L279
.L171:
	movq	%rdi, (%rcx)
	movl	$6, %eax
	ret
.L205:
	movl	$110, %eax
	leaq	.LC23(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L160:
	cmpq	%rdi, %rsi
	jbe	.L168
	testb	%al, %al
	jne	.L280
.L169:
	movq	%rdi, (%rcx)
	movl	$8, %eax
	ret
.L192:
	movq	%rsi, %rax
	subq	%rdx, %rax
	cmpq	$1, %rax
	jle	.L237
	movzbl	2(%rdi), %eax
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L195
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$5, %al
	ja	.L237
.L195:
	movzbl	3(%rdi), %eax
	leaq	4(%rdi), %r9
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L265
.L268:
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$5, %al
	ja	.L237
.L265:
	movq	%r9, %rdi
	jmp	.L190
.L194:
	movq	%rsi, %rax
	subq	%rdx, %rax
	cmpq	$3, %rax
	jle	.L237
	movzbl	2(%rdi), %eax
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L196
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$5, %al
	ja	.L237
.L196:
	movzbl	3(%rdi), %eax
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L197
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$5, %al
	ja	.L237
.L197:
	movzbl	4(%rdi), %eax
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L198
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$5, %al
	ja	.L237
.L198:
	movzbl	5(%rdi), %eax
	leaq	6(%rdi), %r9
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L265
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L278:
	movzbl	(%rdi), %r9d
	addq	$1, %rdx
	movsbl	%al, %r8d
	addq	$1, %rdi
	movzbl	(%rdx), %eax
	cmpl	%r8d, %r9d
	je	.L161
.L172:
	testb	%al, %al
	jne	.L237
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L279:
	movzbl	(%rdi), %r9d
	addq	$1, %rdx
	movsbl	%al, %r8d
	addq	$1, %rdi
	movzbl	(%rdx), %eax
	cmpl	%r8d, %r9d
	je	.L159
.L170:
	testb	%al, %al
	jne	.L237
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L277:
	movq	%rdx, (%rcx)
	movl	$4, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	cmpl	$1, %r8d
	je	.L182
	movl	$11, %eax
	cmpb	$48, %dl
	je	.L141
.L182:
	movq	%rsi, (%rcx)
	movl	$5, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	addq	$1, %rdi
	cmpq	%rdi, %rsi
	je	.L237
	jbe	.L237
	xorl	%r8d, %r8d
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L281:
	addq	$1, %rdi
	addl	$1, %r8d
	cmpq	%rdi, %rsi
	je	.L182
.L183:
	movzbl	(%rdi), %edx
	leal	-48(%rdx), %eax
	cmpb	$9, %al
	jbe	.L281
	movl	$11, %eax
	testl	%r8d, %r8d
	je	.L141
	cmpq	%rdi, %rsi
	jne	.L180
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L275:
	movq	%rsi, %rdi
	jmp	.L185
	.cfi_endproc
.LFE4021:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS5_PS5_S6_, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS5_PS5_S6_
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS5_PS5_S6_,"ax",@progbits
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS5_PS5_S6_, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS5_PS5_S6_:
.LFB4025:
	.cfi_startproc
	cmpq	%rsi, %rdi
	jnb	.L283
	.p2align 4,,10
	.p2align 3
.L284:
	movzwl	(%rdi), %eax
	cmpw	$32, %ax
	je	.L285
	leal	-9(%rax), %r8d
	cmpw	$4, %r8w
	jbe	.L285
	cmpw	$47, %ax
	jne	.L283
	cmpq	%rdi, %rsi
	je	.L291
	leaq	2(%rdi), %rax
	cmpq	%rax, %rsi
	jbe	.L289
	movzwl	2(%rdi), %eax
	cmpw	$47, %ax
	je	.L406
	cmpw	$42, %ax
	je	.L407
.L289:
	movq	%rdi, (%rdx)
.L340:
	movzwl	(%rdi), %edx
	leal	-34(%rdx), %eax
	cmpw	$91, %ax
	ja	.L376
	leaq	.L298(%rip), %r8
	movzwl	%ax, %eax
	movslq	(%r8,%rax,4), %rax
	addq	%r8, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS5_PS5_S6_,"a",@progbits
	.align 4
	.align 4
.L298:
	.long	.L308-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L307-.L298
	.long	.L306-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L306-.L298
	.long	.L306-.L298
	.long	.L306-.L298
	.long	.L306-.L298
	.long	.L306-.L298
	.long	.L306-.L298
	.long	.L306-.L298
	.long	.L306-.L298
	.long	.L306-.L298
	.long	.L306-.L298
	.long	.L305-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L304-.L298
	.long	.L376-.L298
	.long	.L303-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L343-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L344-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L345-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L376-.L298
	.long	.L299-.L298
	.long	.L376-.L298
	.long	.L297-.L298
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS5_PS5_S6_
	.p2align 4,,10
	.p2align 3
.L416:
	movzwl	(%rdi), %r9d
	addq	$1, %rdx
	movsbl	%al, %r8d
	addq	$2, %rdi
	movzbl	(%rdx), %eax
	cmpl	%r8d, %r9d
	je	.L301
.L309:
	testb	%al, %al
	je	.L310
.L376:
	movl	$11, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	addq	$2, %rdi
.L287:
	cmpq	%rdi, %rsi
	ja	.L284
.L283:
	movq	%rdi, (%rdx)
	cmpq	%rdi, %rsi
	jne	.L340
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L406:
	addq	$4, %rdi
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L408:
	movzwl	(%rdi), %eax
	addq	$2, %rdi
	cmpw	$10, %ax
	je	.L287
	cmpw	$13, %ax
	je	.L287
.L400:
	cmpq	%rdi, %rsi
	ja	.L408
.L291:
	movq	%rsi, (%rdx)
.L403:
	movl	$12, %eax
.L282:
	ret
	.p2align 4,,10
	.p2align 3
.L407:
	leaq	4(%rdi), %r8
	cmpq	%r8, %rsi
	ja	.L293
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L294:
	movq	%rax, %r8
.L293:
	leaq	2(%r8), %rax
	movzwl	-2(%rax), %r9d
	cmpq	%rax, %rsi
	jbe	.L289
	cmpw	$42, %r9w
	jne	.L294
	cmpw	$47, (%rax)
	jne	.L294
	leaq	4(%r8), %rdi
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L306:
	cmpw	$45, %dx
	je	.L409
.L315:
	cmpq	%rdi, %rsi
	jbe	.L376
	xorl	%r8d, %r8d
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L410:
	addq	$2, %rdi
	addl	$1, %r8d
	cmpq	%rdi, %rsi
	jbe	.L317
.L318:
	movzwl	(%rdi), %eax
	subl	$48, %eax
	cmpw	$9, %ax
	jbe	.L410
	movl	$11, %eax
	testl	%r8d, %r8d
	je	.L282
.L317:
	cmpw	$48, %dx
	jne	.L377
	cmpl	$1, %r8d
	jne	.L376
.L377:
	cmpq	%rdi, %rsi
	je	.L402
	movzwl	(%rdi), %eax
	cmpw	$46, %ax
	je	.L411
.L321:
	andl	$-33, %eax
	cmpw	$69, %ax
	jne	.L326
	leaq	2(%rdi), %rdx
	cmpq	%rdx, %rsi
	je	.L376
	movzwl	2(%rdi), %eax
	subl	$43, %eax
	testw	$-3, %ax
	jne	.L327
	leaq	4(%rdi), %rdx
	cmpq	%rdx, %rsi
	je	.L376
.L327:
	cmpq	%rdx, %rsi
	jbe	.L376
	xorl	%edi, %edi
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L412:
	addq	$2, %rdx
	addl	$1, %edi
	cmpq	%rdx, %rsi
	jbe	.L358
.L329:
	movzwl	(%rdx), %eax
	subl	$48, %eax
	cmpw	$9, %ax
	jbe	.L412
	movl	$11, %eax
	testl	%edi, %edi
	je	.L282
.L358:
	movq	%rdx, %rdi
.L326:
	movq	%rdi, (%rcx)
	movl	$5, %eax
	ret
.L303:
	addq	$2, %rdi
	movl	$3, %eax
	movq	%rdi, (%rcx)
	ret
.L305:
	addq	$2, %rdi
	movl	$10, %eax
	movq	%rdi, (%rcx)
	ret
.L308:
	addq	$2, %rdi
	cmpq	%rsi, %rdi
	jnb	.L376
	leaq	.L334(%rip), %r8
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L332:
	cmpw	$34, %ax
	je	.L413
.L375:
	movq	%rdx, %rdi
.L331:
	cmpq	%rdi, %rsi
	jbe	.L376
.L330:
	movzwl	(%rdi), %eax
	leaq	2(%rdi), %rdx
	cmpw	$92, %ax
	jne	.L332
	cmpq	%rdx, %rsi
	je	.L376
	movzwl	2(%rdi), %eax
	leaq	4(%rdi), %rdx
	subl	$34, %eax
	cmpw	$86, %ax
	ja	.L376
	movzwl	%ax, %eax
	movslq	(%r8,%rax,4), %rax
	addq	%r8, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS5_PS5_S6_
	.align 4
	.align 4
.L334:
	.long	.L375-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L375-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L375-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L375-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L375-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L375-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L376-.L334
	.long	.L375-.L334
	.long	.L376-.L334
	.long	.L375-.L334
	.long	.L335-.L334
	.long	.L375-.L334
	.long	.L376-.L334
	.long	.L333-.L334
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS5_PS5_S6_
.L307:
	addq	$2, %rdi
	movl	$9, %eax
	movq	%rdi, (%rcx)
	ret
.L297:
	addq	$2, %rdi
	movl	$1, %eax
	movq	%rdi, (%rcx)
	ret
.L299:
	addq	$2, %rdi
	xorl	%eax, %eax
	movq	%rdi, (%rcx)
	ret
.L304:
	addq	$2, %rdi
	movl	$2, %eax
	movq	%rdi, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L409:
	leaq	2(%rdi), %r8
	movl	$11, %eax
	cmpq	%r8, %rsi
	je	.L282
	movzwl	2(%rdi), %edx
	movq	%r8, %rdi
	jmp	.L315
.L343:
	movl	$102, %eax
	leaq	.LC2(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L302:
	cmpq	%rdi, %rsi
	jbe	.L313
	testb	%al, %al
	jne	.L414
.L314:
	movq	%rdi, (%rcx)
	movl	$7, %eax
	ret
.L345:
	movl	$116, %eax
	leaq	.LC24(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L300:
	cmpq	%rdi, %rsi
	jbe	.L311
	testb	%al, %al
	jne	.L415
.L312:
	movq	%rdi, (%rcx)
	movl	$6, %eax
	ret
.L344:
	movl	$110, %eax
	leaq	.LC23(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L301:
	cmpq	%rdi, %rsi
	jbe	.L309
	testb	%al, %al
	jne	.L416
.L310:
	movq	%rdi, (%rcx)
	movl	$8, %eax
	ret
.L333:
	movq	%rsi, %rax
	subq	%rdx, %rax
	cmpq	$2, %rax
	jle	.L376
	movzwl	4(%rdi), %eax
	leal	-48(%rax), %edx
	cmpw	$9, %dx
	jbe	.L336
	andl	$-33, %eax
	subl	$65, %eax
	cmpw	$5, %ax
	ja	.L376
.L336:
	movzwl	6(%rdi), %eax
	leaq	8(%rdi), %r9
	leal	-48(%rax), %edx
	cmpw	$9, %dx
	jbe	.L401
.L405:
	andl	$-33, %eax
	subl	$65, %eax
	cmpw	$5, %ax
	ja	.L376
.L401:
	movq	%r9, %rdi
	jmp	.L331
.L335:
	movq	%rsi, %rax
	subq	%rdx, %rax
	cmpq	$6, %rax
	jle	.L376
	movzwl	4(%rdi), %eax
	leal	-48(%rax), %edx
	cmpw	$9, %dx
	jbe	.L337
	andl	$-33, %eax
	subl	$65, %eax
	cmpw	$5, %ax
	ja	.L376
.L337:
	movzwl	6(%rdi), %eax
	leal	-48(%rax), %edx
	cmpw	$9, %dx
	jbe	.L338
	andl	$-33, %eax
	subl	$65, %eax
	cmpw	$5, %ax
	ja	.L376
.L338:
	movzwl	8(%rdi), %eax
	leal	-48(%rax), %edx
	cmpw	$9, %dx
	jbe	.L339
	andl	$-33, %eax
	subl	$65, %eax
	cmpw	$5, %ax
	ja	.L376
.L339:
	movzwl	10(%rdi), %eax
	leaq	12(%rdi), %r9
	leal	-48(%rax), %edx
	cmpw	$9, %dx
	jbe	.L401
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L415:
	movzwl	(%rdi), %r9d
	addq	$1, %rdx
	movsbl	%al, %r8d
	addq	$2, %rdi
	movzbl	(%rdx), %eax
	cmpl	%r8d, %r9d
	je	.L300
.L311:
	testb	%al, %al
	jne	.L376
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L414:
	movzwl	(%rdi), %r9d
	addq	$1, %rdx
	movsbl	%al, %r8d
	addq	$2, %rdi
	movzbl	(%rdx), %eax
	cmpl	%r8d, %r9d
	je	.L302
.L313:
	testb	%al, %al
	jne	.L376
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L413:
	movq	%rdx, (%rcx)
	movl	$4, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L411:
	addq	$2, %rdi
	cmpq	%rdi, %rsi
	je	.L376
	jbe	.L376
	xorl	%edx, %edx
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L417:
	addq	$2, %rdi
	addl	$1, %edx
	cmpq	%rdi, %rsi
	jbe	.L323
.L324:
	movzwl	(%rdi), %eax
	subl	$48, %eax
	cmpw	$9, %ax
	jbe	.L417
	movl	$11, %eax
	testl	%edx, %edx
	je	.L282
.L323:
	cmpq	%rdi, %rsi
	je	.L402
	movzwl	(%rdi), %eax
	jmp	.L321
.L402:
	movq	%rsi, (%rcx)
	movl	$5, %eax
	ret
	.cfi_endproc
.LFE4025:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS5_PS5_S6_, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS5_PS5_S6_
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0.str1.1,"aMS",@progbits,1
.LC25:
	.string	"vector::_M_range_insert"
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0:
.LFB4917:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	call	strlen@PLT
	testq	%rax, %rax
	je	.L418
	movq	8(%r14), %r13
	movq	%rax, %r12
	movq	%rax, %r11
	movq	16(%r14), %rax
	subq	%r13, %rax
	cmpq	%rax, %r12
	ja	.L420
	leaq	15(%rbx), %rax
	subq	%r13, %rax
	cmpq	$30, %rax
	jbe	.L436
	leaq	-1(%r12), %rax
	cmpq	$14, %rax
	jbe	.L436
	movq	%r12, %rdx
	xorl	%eax, %eax
	andq	$-16, %rdx
.L422:
	movdqu	(%rbx,%rax), %xmm1
	movups	%xmm1, 0(%r13,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L422
	movq	%r12, %rdx
	movq	%r12, %rax
	andq	$-16, %rdx
	andl	$15, %eax
	addq	%rdx, %rbx
	addq	%rdx, %r13
	cmpq	%rdx, %r12
	je	.L424
	movzbl	(%rbx), %edx
	movb	%dl, 0(%r13)
	cmpq	$1, %rax
	je	.L424
	movzbl	1(%rbx), %edx
	movb	%dl, 1(%r13)
	cmpq	$2, %rax
	je	.L424
	movzbl	2(%rbx), %edx
	movb	%dl, 2(%r13)
	cmpq	$3, %rax
	je	.L424
	movzbl	3(%rbx), %edx
	movb	%dl, 3(%r13)
	cmpq	$4, %rax
	je	.L424
	movzbl	4(%rbx), %edx
	movb	%dl, 4(%r13)
	cmpq	$5, %rax
	je	.L424
	movzbl	5(%rbx), %edx
	movb	%dl, 5(%r13)
	cmpq	$6, %rax
	je	.L424
	movzbl	6(%rbx), %edx
	movb	%dl, 6(%r13)
	cmpq	$7, %rax
	je	.L424
	movzbl	7(%rbx), %edx
	movb	%dl, 7(%r13)
	cmpq	$8, %rax
	je	.L424
	movzbl	8(%rbx), %edx
	movb	%dl, 8(%r13)
	cmpq	$9, %rax
	je	.L424
	movzbl	9(%rbx), %edx
	movb	%dl, 9(%r13)
	cmpq	$10, %rax
	je	.L424
	movzbl	10(%rbx), %edx
	movb	%dl, 10(%r13)
	cmpq	$11, %rax
	je	.L424
	movzbl	11(%rbx), %edx
	movb	%dl, 11(%r13)
	cmpq	$12, %rax
	je	.L424
	movzbl	12(%rbx), %edx
	movb	%dl, 12(%r13)
	cmpq	$13, %rax
	je	.L424
	movzbl	13(%rbx), %edx
	movb	%dl, 13(%r13)
	cmpq	$14, %rax
	je	.L424
	movzbl	14(%rbx), %eax
	movb	%al, 14(%r13)
	.p2align 4,,10
	.p2align 3
.L424:
	addq	%r12, 8(%r14)
.L418:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L420:
	.cfi_restore_state
	movabsq	$9223372036854775807, %r15
	movq	(%r14), %r8
	movq	%r13, %rdx
	movq	%r15, %rax
	subq	%r8, %rdx
	subq	%rdx, %rax
	cmpq	%rax, %r12
	ja	.L546
	cmpq	%rdx, %r12
	movq	%rdx, %rax
	cmovnb	%r12, %rax
	addq	%rdx, %rax
	movq	%rax, %r10
	jc	.L428
	testq	%rax, %rax
	js	.L428
	jne	.L439
	xorl	%r15d, %r15d
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
.L435:
	testq	%rdx, %rdx
	jne	.L547
.L429:
	leaq	(%rcx,%rdx), %rax
	cmpq	$15, %r12
	jle	.L440
	movq	%r12, %rsi
	xorl	%edx, %edx
	andq	$-16, %rsi
	.p2align 4,,10
	.p2align 3
.L431:
	movdqu	(%rbx,%rdx), %xmm2
	movups	%xmm2, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L431
	movq	%r12, %rsi
	movq	%r12, %r11
	andq	$-16, %rsi
	andl	$15, %r11d
	addq	%rsi, %rbx
	leaq	(%rax,%rsi), %rdx
	cmpq	%rsi, %r12
	je	.L432
.L430:
	movzbl	(%rbx), %esi
	movb	%sil, (%rdx)
	cmpq	$1, %r11
	je	.L432
	movzbl	1(%rbx), %esi
	movb	%sil, 1(%rdx)
	cmpq	$2, %r11
	je	.L432
	movzbl	2(%rbx), %esi
	movb	%sil, 2(%rdx)
	cmpq	$3, %r11
	je	.L432
	movzbl	3(%rbx), %esi
	movb	%sil, 3(%rdx)
	cmpq	$4, %r11
	je	.L432
	movzbl	4(%rbx), %esi
	movb	%sil, 4(%rdx)
	cmpq	$5, %r11
	je	.L432
	movzbl	5(%rbx), %esi
	movb	%sil, 5(%rdx)
	cmpq	$6, %r11
	je	.L432
	movzbl	6(%rbx), %esi
	movb	%sil, 6(%rdx)
	cmpq	$7, %r11
	je	.L432
	movzbl	7(%rbx), %esi
	movb	%sil, 7(%rdx)
	cmpq	$8, %r11
	je	.L432
	movzbl	8(%rbx), %esi
	movb	%sil, 8(%rdx)
	cmpq	$9, %r11
	je	.L432
	movzbl	9(%rbx), %esi
	movb	%sil, 9(%rdx)
	cmpq	$10, %r11
	je	.L432
	movzbl	10(%rbx), %esi
	movb	%sil, 10(%rdx)
	cmpq	$11, %r11
	je	.L432
	movzbl	11(%rbx), %esi
	movb	%sil, 11(%rdx)
	cmpq	$12, %r11
	je	.L432
	movzbl	12(%rbx), %esi
	movb	%sil, 12(%rdx)
	cmpq	$13, %r11
	je	.L432
	movzbl	13(%rbx), %esi
	movb	%sil, 13(%rdx)
	cmpq	$14, %r11
	je	.L432
	movzbl	14(%rbx), %esi
	movb	%sil, 14(%rdx)
	.p2align 4,,10
	.p2align 3
.L432:
	addq	%rax, %r12
	testq	%r9, %r9
	jne	.L548
.L433:
	addq	%r10, %r12
	testq	%r8, %r8
	je	.L434
	movq	%r8, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rcx
.L434:
	movq	%r12, %xmm3
	movq	%rcx, %xmm0
	movq	%r15, 16(%r14)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%r14)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L439:
	.cfi_restore_state
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L428:
	movq	%r15, %rdi
	movq	%r11, -56(%rbp)
	call	_Znwm@PLT
	movq	8(%r14), %r9
	movq	(%r14), %r8
	movq	%r13, %rdx
	movq	-56(%rbp), %r11
	movq	%rax, %rcx
	addq	%rax, %r15
	subq	%r13, %r9
	subq	%r8, %rdx
	movq	%r9, %r10
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L436:
	xorl	%eax, %eax
.L421:
	movzbl	(%rbx,%rax), %edx
	movb	%dl, 0(%r13,%rax)
	addq	$1, %rax
	cmpq	%rax, %r12
	jne	.L421
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L547:
	movq	%r8, %rsi
	movq	%rcx, %rdi
	movq	%r10, -88(%rbp)
	movq	%r9, -80(%rbp)
	movq	%r11, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-88(%rbp), %r10
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %r11
	movq	-64(%rbp), %rdx
	movq	%rax, %rcx
	movq	-56(%rbp), %r8
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L548:
	movq	%r10, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rcx, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%r10, -56(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %rcx
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %r10
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L440:
	movq	%rax, %rdx
	jmp	.L430
.L546:
	leaq	.LC25(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4917:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleBoolEb,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleBoolEb, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleBoolEb:
.LFB4475:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L557
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	testb	%sil, %sil
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	setne	%r13b
	pushq	%r12
	subl	$12, %r13d
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %rbx
	movq	8(%rbx), %r12
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	leaq	1(%r12), %r14
	cmpq	%rdx, %rax
	je	.L555
	movq	16(%rbx), %rdx
.L552:
	cmpq	%rdx, %r14
	ja	.L560
.L553:
	movb	%r13b, (%rax,%r12)
	movq	(%rbx), %rax
	movq	%r14, 8(%rbx)
	movb	$0, 1(%rax,%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L560:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L557:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L555:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	$15, %edx
	jmp	.L552
	.cfi_endproc
.LFE4475:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleBoolEb, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleBoolEb
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleNullEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleNullEv, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleNullEv:
.LFB4476:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L570
	ret
	.p2align 4,,10
	.p2align 3
.L570:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %rbx
	movq	8(%rbx), %r12
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L565
	movq	16(%rbx), %rdx
.L563:
	cmpq	%rdx, %r13
	ja	.L571
.L564:
	movb	$-10, (%rax,%r12)
	movq	(%rbx), %rax
	movq	%r13, 8(%rbx)
	movb	$0, 1(%rax,%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L571:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L564
	.p2align 4,,10
	.p2align 3
.L565:
	movl	$15, %edx
	jmp	.L563
	.cfi_endproc
.LFE4476:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleNullEv, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleNullEv
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleDoubleEd,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleDoubleEd, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleDoubleEd:
.LFB4473:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L606
	ret
	.p2align 4,,10
	.p2align 3
.L606:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%xmm0, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	16(%rbx), %r13
	leaq	1(%r14), %r15
	cmpq	%r13, %rax
	je	.L593
	movq	16(%rbx), %rdx
.L574:
	cmpq	%rdx, %r15
	ja	.L607
.L575:
	movb	$-5, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$56, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L594
	movq	16(%rbx), %rdx
.L576:
	cmpq	%r15, %rdx
	jb	.L608
.L577:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$48, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L595
	movq	16(%rbx), %rdx
.L578:
	cmpq	%rdx, %r15
	ja	.L609
.L579:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$40, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L596
	movq	16(%rbx), %rdx
.L580:
	cmpq	%rdx, %r15
	ja	.L610
.L581:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$32, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L597
	movq	16(%rbx), %rdx
.L582:
	cmpq	%rdx, %r15
	ja	.L611
.L583:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$24, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L598
	movq	16(%rbx), %rdx
.L584:
	cmpq	%rdx, %r15
	ja	.L612
.L585:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$16, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L599
	movq	16(%rbx), %rdx
.L586:
	cmpq	%rdx, %r15
	ja	.L613
.L587:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$8, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L600
	movq	16(%rbx), %rdx
.L588:
	cmpq	%rdx, %r15
	ja	.L614
.L589:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r15, 8(%rbx)
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L601
	movq	16(%rbx), %rdx
.L590:
	cmpq	%rdx, %r15
	ja	.L615
.L591:
	movb	%r12b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r15, 8(%rbx)
	movb	$0, 1(%rax,%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L607:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L615:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L614:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L613:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L612:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L611:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L610:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L609:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L608:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L594:
	movl	$15, %edx
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L593:
	movl	$15, %edx
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L601:
	movl	$15, %edx
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L600:
	movl	$15, %edx
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L599:
	movl	$15, %edx
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L598:
	movl	$15, %edx
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L597:
	movl	$15, %edx
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L596:
	movl	$15, %edx
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L595:
	movl	$15, %edx
	jmp	.L578
	.cfi_endproc
.LFE4473:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleDoubleEd, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleDoubleEd
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleNullEv.str1.8,"aMS",@progbits,1
	.align 8
.LC26:
	.string	"void v8_inspector_protocol_encoding::json::{anonymous}::State::StartElementTmpl(C*) [with C = std::__cxx11::basic_string<char>]"
	.align 8
.LC27:
	.string	"container_ != Container::NONE || size_ == 0"
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleNullEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleNullEv, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleNullEv:
.LFB4452:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L616
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	80(%rdi), %r12
	cmpq	88(%rdi), %r12
	je	.L635
	movl	-8(%r12), %edx
	movl	-4(%r12), %eax
	testl	%edx, %edx
	je	.L636
.L619:
	testl	%eax, %eax
	je	.L620
	cmpl	$2, %edx
	je	.L624
	movl	$58, %r9d
	testb	$1, %al
	jne	.L621
.L624:
	movl	$44, %r9d
.L621:
	movq	16(%rbx), %r13
	movq	8(%r13), %r14
	movq	0(%r13), %rax
	leaq	16(%r13), %rdx
	leaq	1(%r14), %r15
	cmpq	%rdx, %rax
	je	.L625
	movq	16(%r13), %rdx
.L622:
	cmpq	%rdx, %r15
	ja	.L637
.L623:
	movb	%r9b, (%rax,%r14)
	movq	0(%r13), %rax
	movq	%r15, 8(%r13)
	movb	$0, 1(%rax,%r14)
	movl	-4(%r12), %eax
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L636:
	testl	%eax, %eax
	jne	.L638
.L620:
	addl	$1, %eax
	movl	$4, %r8d
	leaq	.LC23(%rip), %rcx
	xorl	%edx, %edx
	movl	%eax, -4(%r12)
	movq	16(%rbx), %rdi
	movq	8(%rdi), %rsi
	addq	$24, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	.p2align 4,,10
	.p2align 3
.L616:
	ret
	.p2align 4,,10
	.p2align 3
.L635:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	104(%rdi), %rax
	movq	-8(%rax), %r12
	movl	504(%r12), %edx
	addq	$512, %r12
	movl	-4(%r12), %eax
	testl	%edx, %edx
	jne	.L619
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L637:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movb	%r9b, -49(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rax
	movzbl	-49(%rbp), %r9d
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L625:
	movl	$15, %edx
	jmp	.L622
.L638:
	leaq	.LC26(%rip), %rcx
	movl	$1247, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4452:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleNullEv, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleNullEv
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleBoolEb,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleBoolEb, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleBoolEb:
.LFB4451:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L639
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	80(%rdi), %r13
	cmpq	88(%rdi), %r13
	je	.L660
	movl	-8(%r13), %edx
	movl	-4(%r13), %eax
	testl	%edx, %edx
	je	.L661
.L642:
	testl	%eax, %eax
	je	.L643
	cmpl	$2, %edx
	je	.L648
	movl	$58, %r10d
	testb	$1, %al
	jne	.L644
.L648:
	movl	$44, %r10d
.L644:
	movq	16(%rbx), %r14
	movq	8(%r14), %r15
	movq	(%r14), %rax
	leaq	16(%r14), %rdx
	leaq	1(%r15), %r9
	cmpq	%rdx, %rax
	je	.L649
	movq	16(%r14), %rdx
.L645:
	cmpq	%rdx, %r9
	ja	.L662
.L646:
	movb	%r10b, (%rax,%r15)
	movq	(%r14), %rax
	movq	%r9, 8(%r14)
	movb	$0, 1(%rax,%r15)
	movl	-4(%r13), %eax
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L661:
	testl	%eax, %eax
	jne	.L663
.L643:
	addl	$1, %eax
	cmpb	$1, %r12b
	leaq	.LC2(%rip), %rdx
	movl	%eax, -4(%r13)
	movq	16(%rbx), %rdi
	sbbq	%rax, %rax
	leaq	.LC24(%rip), %rcx
	testb	%r12b, %r12b
	notq	%rax
	cmove	%rdx, %rcx
	movq	8(%rdi), %rsi
	addq	$24, %rsp
	leaq	5(%rax), %r8
	popq	%rbx
	.cfi_restore 3
	xorl	%edx, %edx
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	.p2align 4,,10
	.p2align 3
.L639:
	ret
	.p2align 4,,10
	.p2align 3
.L660:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	104(%rdi), %rax
	movq	-8(%rax), %r13
	movl	504(%r13), %edx
	addq	$512, %r13
	movl	-4(%r13), %eax
	testl	%edx, %edx
	jne	.L642
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L662:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r9, -64(%rbp)
	movb	%r10b, -49(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r14), %rax
	movq	-64(%rbp), %r9
	movzbl	-49(%rbp), %r10d
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L649:
	movl	$15, %edx
	jmp	.L645
.L663:
	leaq	.LC26(%rip), %rcx
	movl	$1247, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4451:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleBoolEb, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleBoolEb
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleInt32Ei.str1.1,"aMS",@progbits,1
.LC28:
	.string	"%d"
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleInt32Ei,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleInt32Ei, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleInt32Ei:
.LFB4450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L664
	movq	80(%rdi), %r12
	movq	%rdi, %rbx
	movl	%esi, %r14d
	cmpq	88(%rdi), %r12
	je	.L683
	movl	-8(%r12), %edx
	movl	-4(%r12), %eax
	testl	%edx, %edx
	je	.L684
.L667:
	testl	%eax, %eax
	je	.L668
	cmpl	$2, %edx
	je	.L674
	movl	$58, %r10d
	testb	$1, %al
	jne	.L669
.L674:
	movl	$44, %r10d
.L669:
	movq	16(%rbx), %r13
	movq	8(%r13), %r15
	movq	0(%r13), %rax
	leaq	16(%r13), %rdx
	leaq	1(%r15), %r9
	cmpq	%rdx, %rax
	je	.L675
	movq	16(%r13), %rdx
.L670:
	cmpq	%rdx, %r9
	ja	.L685
.L671:
	movb	%r10b, (%rax,%r15)
	movq	0(%r13), %rax
	movq	%r9, 8(%r13)
	movb	$0, 1(%rax,%r15)
	movl	-4(%r12), %eax
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L684:
	testl	%eax, %eax
	jne	.L686
.L668:
	addl	$1, %eax
	leaq	-96(%rbp), %rdi
	movl	%r14d, %r8d
	movl	$16, %edx
	movl	%eax, -4(%r12)
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC28(%rip), %rcx
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movq	16(%rbx), %rdi
	movq	-88(%rbp), %r8
	xorl	%edx, %edx
	movq	-96(%rbp), %rcx
	movq	8(%rdi), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L664
	call	_ZdlPv@PLT
.L664:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L687
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L683:
	.cfi_restore_state
	movq	104(%rdi), %rax
	movq	-8(%rax), %r12
	movl	504(%r12), %edx
	addq	$512, %r12
	movl	-4(%r12), %eax
	testl	%edx, %edx
	jne	.L667
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L685:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	movb	%r10b, -97(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rax
	movq	-112(%rbp), %r9
	movzbl	-97(%rbp), %r10d
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L675:
	movl	$15, %edx
	jmp	.L670
.L687:
	call	__stack_chk_fail@PLT
.L686:
	leaq	.LC26(%rip), %rcx
	movl	$1247, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4450:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleInt32Ei, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleInt32Ei
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleMapEndEv.str1.8,"aMS",@progbits,1
	.align 8
.LC29:
	.string	"void v8_inspector_protocol_encoding::json::{anonymous}::JSONEncoder<C>::HandleMapEnd() [with C = std::__cxx11::basic_string<char>]"
	.align 8
.LC30:
	.string	"state_.size() >= 2 && state_.top().container() == Container::MAP"
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleMapEndEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleMapEndEv, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleMapEndEv:
.LFB4443:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L699
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	104(%rdi), %r8
	movq	80(%rdi), %rdi
	movq	88(%rbx), %rsi
	movq	%r8, %rdx
	subq	72(%rbx), %rdx
	movq	%rdi, %rax
	sarq	$3, %rdx
	subq	%rsi, %rax
	subq	$1, %rdx
	sarq	$3, %rax
	movq	%rdx, %rcx
	salq	$6, %rcx
	leaq	(%rcx,%rax), %rdx
	movq	64(%rbx), %rax
	subq	48(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$1, %rax
	jbe	.L690
	cmpq	%rsi, %rdi
	je	.L702
	cmpl	$1, -8(%rdi)
	jne	.L690
	subq	$8, %rdi
	movq	%rdi, 80(%rbx)
.L695:
	movq	16(%rbx), %rbx
	movq	8(%rbx), %r12
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L697
	movq	16(%rbx), %rdx
.L693:
	cmpq	%rdx, %r13
	ja	.L703
.L694:
	movb	$125, (%rax,%r12)
	movq	(%rbx), %rax
	movq	%r13, 8(%rbx)
	movb	$0, 1(%rax,%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L699:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L703:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L694
	.p2align 4,,10
	.p2align 3
.L702:
	movq	-8(%r8), %rax
	cmpl	$1, 504(%rax)
	jne	.L690
	call	_ZdlPv@PLT
	movq	104(%rbx), %rax
	leaq	-8(%rax), %rdx
	movq	%rdx, 104(%rbx)
	movq	-8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 88(%rbx)
	addq	$504, %rax
	movq	%rdx, 96(%rbx)
	movq	%rax, 80(%rbx)
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L697:
	movl	$15, %edx
	jmp	.L693
.L690:
	leaq	.LC29(%rip), %rcx
	movl	$1317, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC30(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4443:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleMapEndEv, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleMapEndEv
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv.str1.8,"aMS",@progbits,1
	.align 8
.LC31:
	.string	"void v8_inspector_protocol_encoding::json::{anonymous}::JSONEncoder<C>::HandleArrayEnd() [with C = std::__cxx11::basic_string<char>]"
	.align 8
.LC32:
	.string	"state_.size() >= 2 && state_.top().container() == Container::ARRAY"
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv:
.LFB4445:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L715
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	104(%rdi), %r8
	movq	80(%rdi), %rdi
	movq	88(%rbx), %rsi
	movq	%r8, %rdx
	subq	72(%rbx), %rdx
	movq	%rdi, %rax
	sarq	$3, %rdx
	subq	%rsi, %rax
	subq	$1, %rdx
	sarq	$3, %rax
	movq	%rdx, %rcx
	salq	$6, %rcx
	leaq	(%rcx,%rax), %rdx
	movq	64(%rbx), %rax
	subq	48(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$1, %rax
	jbe	.L706
	cmpq	%rsi, %rdi
	je	.L718
	cmpl	$2, -8(%rdi)
	jne	.L706
	subq	$8, %rdi
	movq	%rdi, 80(%rbx)
.L711:
	movq	16(%rbx), %rbx
	movq	8(%rbx), %r12
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L713
	movq	16(%rbx), %rdx
.L709:
	cmpq	%rdx, %r13
	ja	.L719
.L710:
	movb	$93, (%rax,%r12)
	movq	(%rbx), %rax
	movq	%r13, 8(%rbx)
	movb	$0, 1(%rax,%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L715:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L719:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L718:
	movq	-8(%r8), %rax
	cmpl	$2, 504(%rax)
	jne	.L706
	call	_ZdlPv@PLT
	movq	104(%rbx), %rax
	leaq	-8(%rax), %rdx
	movq	%rdx, 104(%rbx)
	movq	-8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 88(%rbx)
	addq	$504, %rax
	movq	%rdx, 96(%rbx)
	movq	%rax, 80(%rbx)
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L713:
	movl	$15, %edx
	jmp	.L709
.L706:
	leaq	.LC31(%rip), %rcx
	movl	$1333, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC32(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4445:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv.str1.8,"aMS",@progbits,1
	.align 8
.LC33:
	.string	"void v8_inspector_protocol_encoding::cbor::{anonymous}::CBOREncoder<C>::HandleArrayEnd() [with C = std::__cxx11::basic_string<char>]"
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv.str1.1,"aMS",@progbits,1
.LC34:
	.string	"!envelopes_.empty()"
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv.str1.8
	.align 8
.LC35:
	.string	"bool v8_inspector_protocol_encoding::cbor::EncodeStopTmpl(C*, size_t*) [with C = std::__cxx11::basic_string<char>; size_t = long unsigned int]"
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv.str1.1
.LC36:
	.string	"*byte_size_pos != 0"
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv:
.LFB4469:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	je	.L734
	ret
	.p2align 4,,10
	.p2align 3
.L734:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %r12
	movq	%rdi, %rbx
	movq	8(%r12), %r13
	movq	(%r12), %rax
	leaq	16(%r12), %rdx
	leaq	1(%r13), %r14
	cmpq	%rdx, %rax
	je	.L729
	movq	16(%r12), %rdx
.L723:
	cmpq	%rdx, %r14
	ja	.L735
.L724:
	movb	$-1, (%rax,%r13)
	movq	(%r12), %rax
	movq	%r14, 8(%r12)
	movb	$0, 1(%rax,%r13)
	movq	24(%rbx), %rax
	cmpq	%rax, 16(%rbx)
	je	.L736
	movq	-8(%rax), %rcx
	movq	8(%rbx), %r8
	testq	%rcx, %rcx
	je	.L737
	movq	8(%r8), %rsi
	movl	$4294967295, %edi
	leaq	-4(%rsi), %rdx
	subq	%rcx, %rdx
	cmpq	%rdi, %rdx
	ja	.L727
	leaq	1(%rcx), %rsi
	movq	%rdx, %rdi
	movq	%rsi, -8(%rax)
	movq	(%r8), %rsi
	shrq	$24, %rdi
	movb	%dil, (%rsi,%rcx)
	movq	-8(%rax), %rcx
	movq	%rdx, %rdi
	shrq	$16, %rdi
	leaq	1(%rcx), %rsi
	movq	%rsi, -8(%rax)
	movq	(%r8), %rsi
	movb	%dil, (%rsi,%rcx)
	movq	-8(%rax), %rcx
	movq	%rdx, %rdi
	shrq	$8, %rdi
	leaq	1(%rcx), %rsi
	movq	%rsi, -8(%rax)
	movq	(%r8), %rsi
	movb	%dil, (%rsi,%rcx)
	movq	-8(%rax), %rcx
	leaq	1(%rcx), %rsi
	movq	%rsi, -8(%rax)
	movq	(%r8), %rax
	movb	%dl, (%rax,%rcx)
	subq	$8, 24(%rbx)
.L720:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L727:
	.cfi_restore_state
	movq	40(%rbx), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jne	.L720
	movl	$33, (%rax)
	movq	%rsi, 8(%rax)
	movq	8(%rbx), %rax
	movq	$0, 8(%rax)
	movq	(%rax), %rax
	movb	$0, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L735:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L729:
	movl	$15, %edx
	jmp	.L723
.L737:
	leaq	.LC35(%rip), %rcx
	movl	$489, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	call	__assert_fail@PLT
.L736:
	leaq	.LC33(%rip), %rcx
	movl	$557, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC34(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4469:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleMapEndEv.str1.8,"aMS",@progbits,1
	.align 8
.LC37:
	.string	"void v8_inspector_protocol_encoding::cbor::{anonymous}::CBOREncoder<C>::HandleMapEnd() [with C = std::__cxx11::basic_string<char>]"
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleMapEndEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleMapEndEv, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleMapEndEv:
.LFB4467:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	je	.L752
	ret
	.p2align 4,,10
	.p2align 3
.L752:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %r12
	movq	%rdi, %rbx
	movq	8(%r12), %r13
	movq	(%r12), %rax
	leaq	16(%r12), %rdx
	leaq	1(%r13), %r14
	cmpq	%rdx, %rax
	je	.L747
	movq	16(%r12), %rdx
.L741:
	cmpq	%rdx, %r14
	ja	.L753
.L742:
	movb	$-1, (%rax,%r13)
	movq	(%r12), %rax
	movq	%r14, 8(%r12)
	movb	$0, 1(%rax,%r13)
	movq	24(%rbx), %rax
	cmpq	%rax, 16(%rbx)
	je	.L754
	movq	-8(%rax), %rcx
	movq	8(%rbx), %r8
	testq	%rcx, %rcx
	je	.L755
	movq	8(%r8), %rsi
	movl	$4294967295, %edi
	leaq	-4(%rsi), %rdx
	subq	%rcx, %rdx
	cmpq	%rdi, %rdx
	ja	.L745
	leaq	1(%rcx), %rsi
	movq	%rdx, %rdi
	movq	%rsi, -8(%rax)
	movq	(%r8), %rsi
	shrq	$24, %rdi
	movb	%dil, (%rsi,%rcx)
	movq	-8(%rax), %rcx
	movq	%rdx, %rdi
	shrq	$16, %rdi
	leaq	1(%rcx), %rsi
	movq	%rsi, -8(%rax)
	movq	(%r8), %rsi
	movb	%dil, (%rsi,%rcx)
	movq	-8(%rax), %rcx
	movq	%rdx, %rdi
	shrq	$8, %rdi
	leaq	1(%rcx), %rsi
	movq	%rsi, -8(%rax)
	movq	(%r8), %rsi
	movb	%dil, (%rsi,%rcx)
	movq	-8(%rax), %rcx
	leaq	1(%rcx), %rsi
	movq	%rsi, -8(%rax)
	movq	(%r8), %rax
	movb	%dl, (%rax,%rcx)
	subq	$8, 24(%rbx)
.L738:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L745:
	.cfi_restore_state
	movq	40(%rbx), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jne	.L738
	movl	$33, (%rax)
	movq	%rsi, 8(%rax)
	movq	8(%rbx), %rax
	movq	$0, 8(%rax)
	movq	(%rax), %rax
	movb	$0, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L753:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	jmp	.L742
	.p2align 4,,10
	.p2align 3
.L747:
	movl	$15, %edx
	jmp	.L741
.L755:
	leaq	.LC35(%rip), %rcx
	movl	$489, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	call	__assert_fail@PLT
.L754:
	leaq	.LC37(%rip), %rcx
	movl	$536, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC34(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4467:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleMapEndEv, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleMapEndEv
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE16HandleArrayBeginEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE16HandleArrayBeginEv, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE16HandleArrayBeginEv:
.LFB4444:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L785
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	80(%rdi), %r12
	cmpq	88(%rdi), %r12
	je	.L788
	movl	-8(%r12), %edx
	movl	-4(%r12), %eax
	testl	%edx, %edx
	jne	.L759
.L792:
	testl	%eax, %eax
	jne	.L789
.L760:
	addl	$1, %eax
	movl	%eax, -4(%r12)
	movq	96(%rbx), %rcx
	movq	80(%rbx), %rax
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L764
	movq	$2, (%rax)
	addq	$8, 80(%rbx)
.L765:
	movq	16(%rbx), %rbx
	movq	8(%rbx), %r12
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L777
	movq	16(%rbx), %rdx
.L773:
	cmpq	%rdx, %r13
	ja	.L790
.L774:
	movb	$91, (%rax,%r12)
	movq	(%rbx), %rax
	movq	%r13, 8(%rbx)
	movb	$0, 1(%rax,%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L759:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L760
	cmpl	$2, %edx
	je	.L775
	movl	$58, %r9d
	testb	$1, %al
	jne	.L761
.L775:
	movl	$44, %r9d
.L761:
	movq	16(%rbx), %r13
	movq	8(%r13), %r14
	movq	0(%r13), %rax
	leaq	16(%r13), %rdx
	leaq	1(%r14), %r15
	cmpq	%rdx, %rax
	je	.L776
	movq	16(%r13), %rdx
.L762:
	cmpq	%rdx, %r15
	ja	.L791
.L763:
	movb	%r9b, (%rax,%r14)
	movq	0(%r13), %rax
	movq	%r15, 8(%r13)
	movb	$0, 1(%rax,%r14)
	movl	-4(%r12), %eax
	jmp	.L760
	.p2align 4,,10
	.p2align 3
.L785:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L790:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L774
	.p2align 4,,10
	.p2align 3
.L788:
	movq	104(%rdi), %rax
	movq	-8(%rax), %r12
	movl	504(%r12), %edx
	addq	$512, %r12
	movl	-4(%r12), %eax
	testl	%edx, %edx
	jne	.L759
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L777:
	movl	$15, %edx
	jmp	.L773
	.p2align 4,,10
	.p2align 3
.L764:
	movq	104(%rbx), %r13
	movq	72(%rbx), %rsi
	movabsq	$1152921504606846975, %rdi
	subq	88(%rbx), %rax
	movq	%r13, %r12
	sarq	$3, %rax
	subq	%rsi, %r12
	movq	%r12, %rcx
	sarq	$3, %rcx
	leaq	-1(%rcx), %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movq	64(%rbx), %rax
	subq	48(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rdi, %rax
	je	.L793
	movq	32(%rbx), %r8
	movq	40(%rbx), %rdx
	movq	%r13, %rax
	subq	%r8, %rax
	movq	%rdx, %r10
	sarq	$3, %rax
	subq	%rax, %r10
	cmpq	$1, %r10
	jbe	.L794
.L767:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r13)
	movq	80(%rbx), %rax
	movq	$2, (%rax)
	movq	104(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 104(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 88(%rbx)
	movq	%rdx, 96(%rbx)
	movq	%rax, 80(%rbx)
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L791:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movb	%r9b, -49(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rax
	movzbl	-49(%rbp), %r9d
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L776:
	movl	$15, %edx
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L794:
	leaq	2(%rcx), %r14
	leaq	(%r14,%r14), %rax
	cmpq	%rax, %rdx
	jbe	.L768
	subq	%r14, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r14
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r14, %rsi
	jbe	.L769
	cmpq	%r13, %rsi
	je	.L770
	movq	%r14, %rdi
	call	memmove@PLT
	.p2align 4,,10
	.p2align 3
.L770:
	movq	%r14, 72(%rbx)
	movq	(%r14), %rax
	leaq	(%r14,%r12), %r13
	movq	(%r14), %xmm0
	movq	%r13, 104(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 56(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 88(%rbx)
	addq	$512, %rax
	movq	%rax, 96(%rbx)
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L768:
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r13
	cmpq	%rdi, %r13
	ja	.L795
	leaq	0(,%r13,8), %rdi
	call	_Znwm@PLT
	movq	72(%rbx), %rsi
	movq	%rax, %r15
	movq	%r13, %rax
	subq	%r14, %rax
	shrq	%rax
	leaq	(%r15,%rax,8), %r14
	movq	104(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L772
	subq	%rsi, %rdx
	movq	%r14, %rdi
	call	memmove@PLT
.L772:
	movq	32(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	%r15, 32(%rbx)
	movq	%r13, 40(%rbx)
	jmp	.L770
.L769:
	cmpq	%r13, %rsi
	je	.L770
	leaq	8(%r12), %rdi
	subq	%rdx, %rdi
	addq	%r14, %rdi
	call	memmove@PLT
	jmp	.L770
.L789:
	leaq	.LC26(%rip), %rcx
	movl	$1247, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	call	__assert_fail@PLT
.L795:
	call	_ZSt17__throw_bad_allocv@PLT
.L793:
	leaq	.LC22(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4444:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE16HandleArrayBeginEv, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE16HandleArrayBeginEv
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleMapBeginEv.str1.8,"aMS",@progbits,1
	.align 8
.LC38:
	.string	"void v8_inspector_protocol_encoding::json::{anonymous}::JSONEncoder<C>::HandleMapBegin() [with C = std::__cxx11::basic_string<char>]"
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleMapBeginEv.str1.1,"aMS",@progbits,1
.LC39:
	.string	"!state_.empty()"
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleMapBeginEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleMapBeginEv, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleMapBeginEv:
.LFB4442:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L826
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	80(%rdi), %r12
	cmpq	48(%rdi), %r12
	je	.L829
	cmpq	88(%rdi), %r12
	je	.L830
	movl	-8(%r12), %edx
	movl	-4(%r12), %eax
	testl	%edx, %edx
	jne	.L800
.L834:
	testl	%eax, %eax
	jne	.L831
.L801:
	addl	$1, %eax
	movl	%eax, -4(%r12)
	movq	96(%rbx), %rcx
	movq	80(%rbx), %rax
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L805
	movq	$1, (%rax)
	addq	$8, 80(%rbx)
.L806:
	movq	16(%rbx), %rbx
	movq	8(%rbx), %r12
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L818
	movq	16(%rbx), %rdx
.L814:
	cmpq	%rdx, %r13
	ja	.L832
.L815:
	movb	$123, (%rax,%r12)
	movq	(%rbx), %rax
	movq	%r13, 8(%rbx)
	movb	$0, 1(%rax,%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L800:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L801
	testb	$1, %al
	je	.L816
	movl	$58, %r9d
	cmpl	$2, %edx
	jne	.L802
.L816:
	movl	$44, %r9d
.L802:
	movq	16(%rbx), %r13
	movq	8(%r13), %r14
	movq	0(%r13), %rax
	leaq	16(%r13), %rdx
	leaq	1(%r14), %r15
	cmpq	%rdx, %rax
	je	.L817
	movq	16(%r13), %rdx
.L803:
	cmpq	%rdx, %r15
	ja	.L833
.L804:
	movb	%r9b, (%rax,%r14)
	movq	0(%r13), %rax
	movq	%r15, 8(%r13)
	movb	$0, 1(%rax,%r14)
	movl	-4(%r12), %eax
	jmp	.L801
	.p2align 4,,10
	.p2align 3
.L826:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L832:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L830:
	movq	104(%rdi), %rax
	movq	-8(%rax), %r12
	movl	504(%r12), %edx
	addq	$512, %r12
	movl	-4(%r12), %eax
	testl	%edx, %edx
	jne	.L800
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L818:
	movl	$15, %edx
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L805:
	movq	104(%rbx), %r13
	movq	72(%rbx), %rsi
	movabsq	$1152921504606846975, %rdi
	subq	88(%rbx), %rax
	movq	%r13, %r12
	sarq	$3, %rax
	subq	%rsi, %r12
	movq	%r12, %rcx
	sarq	$3, %rcx
	leaq	-1(%rcx), %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movq	64(%rbx), %rax
	subq	48(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rdi, %rax
	je	.L835
	movq	32(%rbx), %r8
	movq	40(%rbx), %rdx
	movq	%r13, %rax
	subq	%r8, %rax
	movq	%rdx, %r10
	sarq	$3, %rax
	subq	%rax, %r10
	cmpq	$1, %r10
	jbe	.L836
.L808:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r13)
	movq	80(%rbx), %rax
	movq	$1, (%rax)
	movq	104(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 104(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 88(%rbx)
	movq	%rdx, 96(%rbx)
	movq	%rax, 80(%rbx)
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L833:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movb	%r9b, -49(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rax
	movzbl	-49(%rbp), %r9d
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L817:
	movl	$15, %edx
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L836:
	leaq	2(%rcx), %r14
	leaq	(%r14,%r14), %rax
	cmpq	%rax, %rdx
	jbe	.L809
	subq	%r14, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r14
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r14, %rsi
	jbe	.L810
	cmpq	%r13, %rsi
	je	.L811
	movq	%r14, %rdi
	call	memmove@PLT
	.p2align 4,,10
	.p2align 3
.L811:
	movq	%r14, 72(%rbx)
	movq	(%r14), %rax
	leaq	(%r14,%r12), %r13
	movq	(%r14), %xmm0
	movq	%r13, 104(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 56(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 88(%rbx)
	addq	$512, %rax
	movq	%rax, 96(%rbx)
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L809:
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r13
	cmpq	%rdi, %r13
	ja	.L837
	leaq	0(,%r13,8), %rdi
	call	_Znwm@PLT
	movq	72(%rbx), %rsi
	movq	%rax, %r15
	movq	%r13, %rax
	subq	%r14, %rax
	shrq	%rax
	leaq	(%r15,%rax,8), %r14
	movq	104(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L813
	subq	%rsi, %rdx
	movq	%r14, %rdi
	call	memmove@PLT
.L813:
	movq	32(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	%r15, 32(%rbx)
	movq	%r13, 40(%rbx)
	jmp	.L811
.L810:
	cmpq	%r13, %rsi
	je	.L811
	leaq	8(%r12), %rdi
	subq	%rdx, %rdi
	addq	%r14, %rdi
	call	memmove@PLT
	jmp	.L811
.L829:
	leaq	.LC38(%rip), %rcx
	movl	$1308, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC39(%rip), %rdi
	call	__assert_fail@PLT
.L831:
	leaq	.LC26(%rip), %rcx
	movl	$1247, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	call	__assert_fail@PLT
.L837:
	call	_ZSt17__throw_bad_allocv@PLT
.L835:
	leaq	.LC22(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4442:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleMapBeginEv, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleMapBeginEv
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleString16ENS_4spanItEE.str1.1,"aMS",@progbits,1
.LC40:
	.string	"\\\""
.LC41:
	.string	"\\\\"
.LC42:
	.string	"\\b"
.LC43:
	.string	"\\f"
.LC44:
	.string	"\\n"
.LC45:
	.string	"\\r"
.LC46:
	.string	"\\t"
.LC47:
	.string	"\\u"
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleString16ENS_4spanItEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleString16ENS_4spanItEE, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleString16ENS_4spanItEE:
.LFB4447:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L900
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	80(%rdi), %rbx
	cmpq	88(%rdi), %rbx
	je	.L903
	movl	-8(%rbx), %edx
	movl	-4(%rbx), %eax
	testl	%edx, %edx
	jne	.L841
.L920:
	testl	%eax, %eax
	jne	.L904
.L842:
	addl	$1, %eax
	movl	%eax, -4(%rbx)
	movq	16(%r13), %rbx
	movq	8(%rbx), %r15
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	leaq	1(%r15), %r9
	cmpq	%rdx, %rax
	je	.L879
	movq	16(%rbx), %rdx
.L846:
	cmpq	%rdx, %r9
	ja	.L905
.L847:
	movb	$34, (%rax,%r15)
	movq	(%rbx), %rax
	leaq	(%r12,%r14,2), %r14
	movq	%r9, 8(%rbx)
	movb	$0, 1(%rax,%r15)
	movl	$48, %r15d
	cmpq	%r12, %r14
	jne	.L875
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L851:
	cmpw	$92, %bx
	je	.L906
	cmpw	$8, %bx
	je	.L907
	cmpw	$12, %bx
	je	.L908
	cmpw	$10, %bx
	je	.L909
	cmpw	$13, %bx
	je	.L910
	cmpw	$9, %bx
	je	.L911
	leal	-32(%rbx), %eax
	cmpw	$94, %ax
	ja	.L859
	movq	(%rdi), %rax
	leaq	16(%rdi), %rdx
	leaq	1(%rsi), %r9
	cmpq	%rdx, %rax
	je	.L881
	movq	16(%rdi), %rdx
.L860:
	cmpq	%rdx, %r9
	ja	.L912
.L861:
	movb	%bl, (%rax,%rsi)
	movq	(%rdi), %rax
	movq	%r9, 8(%rdi)
	movb	$0, 1(%rax,%rsi)
	.p2align 4,,10
	.p2align 3
.L852:
	addq	$2, %r12
	cmpq	%r12, %r14
	je	.L874
.L875:
	movq	16(%r13), %rdi
	movzwl	(%r12), %ebx
	movq	8(%rdi), %rsi
	cmpw	$34, %bx
	jne	.L851
	movl	$2, %r8d
	leaq	.LC40(%rip), %rcx
	xorl	%edx, %edx
	addq	$2, %r12
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	cmpq	%r12, %r14
	jne	.L875
	.p2align 4,,10
	.p2align 3
.L874:
	movq	16(%r13), %rbx
	movq	8(%rbx), %r12
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L913
	movq	16(%rbx), %rdx
.L849:
	cmpq	%rdx, %r13
	ja	.L914
.L876:
	movb	$34, (%rax,%r12)
	movq	(%rbx), %rax
	movq	%r13, 8(%rbx)
	movb	$0, 1(%rax,%r12)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L841:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L842
	cmpl	$2, %edx
	je	.L877
	movl	$58, %r10d
	testb	$1, %al
	jne	.L843
.L877:
	movl	$44, %r10d
.L843:
	movq	16(%r13), %r15
	movq	8(%r15), %rax
	movq	(%r15), %rdx
	leaq	16(%r15), %rcx
	leaq	1(%rax), %r9
	cmpq	%rcx, %rdx
	je	.L878
	movq	16(%r15), %rcx
.L844:
	cmpq	%rcx, %r9
	ja	.L915
.L845:
	movb	%r10b, (%rdx,%rax)
	movq	(%r15), %rdx
	movq	%r9, 8(%r15)
	movb	$0, 1(%rdx,%rax)
	movl	-4(%rbx), %eax
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L906:
	movl	$2, %r8d
	leaq	.LC41(%rip), %rcx
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L907:
	movl	$2, %r8d
	leaq	.LC42(%rip), %rcx
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L908:
	movl	$2, %r8d
	leaq	.LC43(%rip), %rcx
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L859:
	xorl	%edx, %edx
	movl	$2, %r8d
	leaq	.LC47(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movl	%ebx, %edx
	movq	16(%r13), %rdi
	movzwl	%bx, %eax
	shrw	$12, %dx
	cmpw	$10, %dx
	movq	8(%rdi), %rsi
	leaq	16(%rdi), %r10
	sbbl	%r9d, %r9d
	andl	$-39, %r9d
	leaq	1(%rsi), %r11
	leal	87(%r9,%rdx), %r9d
	movq	(%rdi), %rdx
	cmpq	%rdx, %r10
	je	.L883
	movq	16(%rdi), %rcx
.L863:
	cmpq	%r11, %rcx
	jb	.L916
.L864:
	movb	%r9b, (%rdx,%rsi)
	movl	%eax, %r9d
	movq	(%rdi), %rdx
	sarl	$8, %r9d
	movq	%r11, 8(%rdi)
	movb	$0, 1(%rdx,%rsi)
	movl	%r9d, %edx
	movl	$87, %r9d
	movq	8(%rdi), %rsi
	andl	$15, %edx
	cmpl	$10, %edx
	leaq	1(%rsi), %r11
	cmovl	%r15d, %r9d
	addl	%edx, %r9d
	movq	(%rdi), %rdx
	cmpq	%r10, %rdx
	je	.L885
	movq	16(%rdi), %rcx
.L866:
	cmpq	%rcx, %r11
	ja	.L917
.L867:
	movb	%r9b, (%rdx,%rsi)
	sarl	$4, %eax
	movq	(%rdi), %rdx
	andl	$15, %eax
	movq	%r11, 8(%rdi)
	cmpl	$10, %eax
	movb	$0, 1(%rdx,%rsi)
	movl	$87, %edx
	movq	8(%rdi), %rsi
	cmovl	%r15d, %edx
	leaq	1(%rsi), %r9
	addl	%edx, %eax
	movq	(%rdi), %rdx
	cmpq	%rdx, %r10
	je	.L887
	movq	16(%rdi), %rcx
.L869:
	cmpq	%rcx, %r9
	ja	.L918
.L870:
	movb	%al, (%rdx,%rsi)
	andl	$15, %ebx
	movq	(%rdi), %rax
	cmpw	$10, %bx
	movq	%r9, 8(%rdi)
	movb	$0, 1(%rax,%rsi)
	sbbl	%eax, %eax
	movq	(%rdi), %rdx
	andl	$-39, %eax
	leal	87(%rax,%rbx), %eax
	movq	8(%rdi), %rbx
	leaq	1(%rbx), %r9
	cmpq	%rdx, %r10
	je	.L889
	movq	16(%rdi), %rcx
.L872:
	cmpq	%rcx, %r9
	ja	.L919
.L873:
	movb	%al, (%rdx,%rbx)
	movq	(%rdi), %rax
	movq	%r9, 8(%rdi)
	movb	$0, 1(%rax,%rbx)
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L909:
	movl	$2, %r8d
	leaq	.LC44(%rip), %rcx
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L910:
	movl	$2, %r8d
	leaq	.LC45(%rip), %rcx
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L900:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L911:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$2, %r8d
	leaq	.LC46(%rip), %rcx
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L905:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L847
	.p2align 4,,10
	.p2align 3
.L914:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L903:
	movq	104(%rdi), %rax
	movq	-8(%rax), %rbx
	movl	504(%rbx), %edx
	addq	$512, %rbx
	movl	-4(%rbx), %eax
	testl	%edx, %edx
	jne	.L841
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L912:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rdi
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %rsi
	movq	(%rdi), %rax
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L919:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r9, -72(%rbp)
	movb	%al, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rdi
	movq	-72(%rbp), %r9
	movzbl	-64(%rbp), %eax
	movq	(%rdi), %rdx
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L918:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r9, -88(%rbp)
	movb	%al, -80(%rbp)
	movq	%r10, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rdi
	movq	-88(%rbp), %r9
	movzbl	-80(%rbp), %eax
	movq	-72(%rbp), %r10
	movq	(%rdi), %rdx
	movq	-64(%rbp), %rsi
	jmp	.L870
	.p2align 4,,10
	.p2align 3
.L917:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movb	%r9b, -96(%rbp)
	movq	%r10, -88(%rbp)
	movq	%r11, -80(%rbp)
	movl	%eax, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rdi
	movzbl	-96(%rbp), %r9d
	movq	-88(%rbp), %r10
	movq	-80(%rbp), %r11
	movq	(%rdi), %rdx
	movl	-72(%rbp), %eax
	movq	-64(%rbp), %rsi
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L916:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r11, -96(%rbp)
	movb	%r9b, -88(%rbp)
	movq	%r10, -80(%rbp)
	movl	%eax, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rdi
	movq	-96(%rbp), %r11
	movzbl	-88(%rbp), %r9d
	movq	-80(%rbp), %r10
	movq	(%rdi), %rdx
	movl	-72(%rbp), %eax
	movq	-64(%rbp), %rsi
	jmp	.L864
	.p2align 4,,10
	.p2align 3
.L879:
	movl	$15, %edx
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L913:
	movl	$15, %edx
	jmp	.L849
	.p2align 4,,10
	.p2align 3
.L915:
	xorl	%edx, %edx
	movq	%rax, %rsi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%r9, -72(%rbp)
	movb	%r10b, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r15), %rdx
	movq	-72(%rbp), %r9
	movzbl	-64(%rbp), %r10d
	movq	-56(%rbp), %rax
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L881:
	movl	$15, %edx
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L883:
	movl	$15, %ecx
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L887:
	movl	$15, %ecx
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L885:
	movl	$15, %ecx
	jmp	.L866
	.p2align 4,,10
	.p2align 3
.L889:
	movl	$15, %ecx
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L878:
	movl	$15, %ecx
	jmp	.L844
.L904:
	leaq	.LC26(%rip), %rcx
	movl	$1247, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4447:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleString16ENS_4spanItEE, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleString16ENS_4spanItEE
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleBinaryENS_4spanIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleBinaryENS_4spanIhEE, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleBinaryENS_4spanIhEE:
.LFB4448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %rax
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L921
	movq	80(%rdi), %rbx
	movq	%rdi, %r12
	cmpq	88(%rdi), %rbx
	je	.L995
	movl	-8(%rbx), %edx
	movl	-4(%rbx), %eax
	testl	%edx, %edx
	jne	.L924
.L1005:
	testl	%eax, %eax
	jne	.L996
.L925:
	addl	$1, %eax
	movl	%eax, -4(%rbx)
	movq	16(%r12), %rbx
	movq	8(%rbx), %r15
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	leaq	1(%r15), %r13
	cmpq	%rdx, %rax
	je	.L965
	movq	16(%rbx), %rdx
.L929:
	cmpq	%rdx, %r13
	ja	.L997
.L930:
	movb	$34, (%rax,%r15)
	movq	(%rbx), %rax
	cmpq	$2, -56(%rbp)
	movq	%r13, 8(%rbx)
	movb	$0, 1(%rax,%r15)
	movq	16(%r12), %r13
	jbe	.L966
	leaq	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_1L12kBase64TableE(%rip), %r15
	leaq	16(%r13), %rax
	movq	%r12, -96(%rbp)
	movl	$3, %r14d
	movq	%r15, %r12
	movq	%rax, %r15
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L933:
	movb	%r10b, (%rdx,%rsi)
	movq	0(%r13), %rdx
	movq	%r9, 8(%r13)
	movb	$0, 1(%rdx,%rsi)
	movl	%ebx, %edx
	movq	8(%r13), %rsi
	shrl	$12, %edx
	andl	$63, %edx
	leaq	1(%rsi), %r9
	movzbl	(%r12,%rdx), %r10d
	movq	0(%r13), %rdx
	cmpq	%r15, %rdx
	je	.L968
	movq	16(%r13), %rcx
.L934:
	cmpq	%rcx, %r9
	ja	.L998
.L935:
	movb	%r10b, (%rdx,%rsi)
	movq	0(%r13), %rdx
	movq	%r9, 8(%r13)
	movb	$0, 1(%rdx,%rsi)
	movl	%ebx, %edx
	movq	8(%r13), %rsi
	shrl	$6, %edx
	andl	$63, %edx
	leaq	1(%rsi), %r9
	movzbl	(%r12,%rdx), %r10d
	movq	0(%r13), %rdx
	cmpq	%r15, %rdx
	je	.L969
	movq	16(%r13), %rcx
.L936:
	cmpq	%rcx, %r9
	ja	.L999
.L937:
	movb	%r10b, (%rdx,%rsi)
	movq	0(%r13), %rdx
	andl	$63, %ebx
	movq	%r9, 8(%r13)
	movzbl	(%r12,%rbx), %r10d
	movb	$0, 1(%rdx,%rsi)
	movq	8(%r13), %rbx
	movq	0(%r13), %rdx
	leaq	1(%rbx), %r9
	cmpq	%r15, %rdx
	je	.L970
	movq	16(%r13), %rcx
.L938:
	cmpq	%rcx, %r9
	ja	.L1000
	movb	%r10b, (%rdx,%rbx)
.L989:
	movq	0(%r13), %rdx
	movq	%r9, 8(%r13)
	movb	$0, 1(%rdx,%rbx)
	leaq	3(%r14), %rdx
	cmpq	%rdx, -56(%rbp)
	jb	.L986
	movq	%rdx, %r14
.L942:
	movq	-64(%rbp), %rax
	movq	8(%r13), %rsi
	movzbl	-3(%rax,%r14), %edx
	movzbl	-2(%rax,%r14), %ecx
	leaq	1(%rsi), %r9
	movzbl	-1(%rax,%r14), %ebx
	sall	$16, %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	orl	%edx, %ebx
	shrl	$18, %edx
	movzbl	(%r12,%rdx), %r10d
	movq	0(%r13), %rdx
	cmpq	%r15, %rdx
	je	.L967
	movq	16(%r13), %rcx
.L932:
	cmpq	%rcx, %r9
	jbe	.L933
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	%r9, -88(%rbp)
	movb	%r10b, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rdx
	movq	-88(%rbp), %r9
	movzbl	-80(%rbp), %r10d
	movq	-72(%rbp), %rsi
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L986:
	movq	-96(%rbp), %r12
	leaq	2(%r14), %rcx
	leaq	1(%r14), %rdx
.L931:
	cmpq	%rcx, -56(%rbp)
	jnb	.L1001
	cmpq	%rdx, -56(%rbp)
	jnb	.L1002
.L952:
	movq	16(%r12), %rbx
	movq	8(%rbx), %r12
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L979
	movq	16(%rbx), %rdx
.L961:
	cmpq	%rdx, %r13
	ja	.L1003
.L962:
	movb	$34, (%rax,%r12)
	movq	(%rbx), %rax
	movq	%r13, 8(%rbx)
	movb	$0, 1(%rax,%r12)
.L921:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L924:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L925
	testb	$1, %al
	je	.L963
	movl	$58, %r10d
	cmpl	$2, %edx
	jne	.L926
.L963:
	movl	$44, %r10d
.L926:
	movq	16(%r12), %r15
	movq	8(%r15), %r13
	movq	(%r15), %rax
	leaq	16(%r15), %rdx
	leaq	1(%r13), %r14
	cmpq	%rdx, %rax
	je	.L964
	movq	16(%r15), %rdx
.L927:
	cmpq	%rdx, %r14
	ja	.L1004
.L928:
	movb	%r10b, (%rax,%r13)
	movq	(%r15), %rax
	movq	%r14, 8(%r15)
	movb	$0, 1(%rax,%r13)
	movl	-4(%rbx), %eax
	jmp	.L925
	.p2align 4,,10
	.p2align 3
.L967:
	movl	$15, %ecx
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L1000:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r9, -80(%rbp)
	movb	%r10b, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rdx
	movzbl	-72(%rbp), %r10d
	movq	-80(%rbp), %r9
	movb	%r10b, (%rdx,%rbx)
	jmp	.L989
	.p2align 4,,10
	.p2align 3
.L970:
	movl	$15, %ecx
	jmp	.L938
	.p2align 4,,10
	.p2align 3
.L999:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	%r9, -88(%rbp)
	movb	%r10b, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rdx
	movq	-88(%rbp), %r9
	movzbl	-80(%rbp), %r10d
	movq	-72(%rbp), %rsi
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L969:
	movl	$15, %ecx
	jmp	.L936
	.p2align 4,,10
	.p2align 3
.L998:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	%r9, -88(%rbp)
	movb	%r10b, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rdx
	movq	-88(%rbp), %r9
	movzbl	-80(%rbp), %r10d
	movq	-72(%rbp), %rsi
	jmp	.L935
	.p2align 4,,10
	.p2align 3
.L968:
	movl	$15, %ecx
	jmp	.L934
	.p2align 4,,10
	.p2align 3
.L997:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L930
	.p2align 4,,10
	.p2align 3
.L1003:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L995:
	movq	104(%rdi), %rax
	movq	-8(%rax), %rbx
	movl	504(%rbx), %edx
	addq	$512, %rbx
	movl	-4(%rbx), %eax
	testl	%edx, %edx
	jne	.L924
	jmp	.L1005
	.p2align 4,,10
	.p2align 3
.L1002:
	movq	-64(%rbp), %rax
	leaq	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_1L12kBase64TableE(%rip), %r15
	movzbl	(%rax,%r14), %edx
	movq	8(%r13), %r14
	leaq	16(%r13), %rax
	movl	%edx, %ebx
	shrl	$2, %edx
	leaq	1(%r14), %r9
	movzbl	(%r15,%rdx), %r10d
	movq	0(%r13), %rdx
	sall	$16, %ebx
	cmpq	%rax, %rdx
	je	.L975
	movq	16(%r13), %rcx
.L953:
	cmpq	%rcx, %r9
	ja	.L1006
.L954:
	movb	%r10b, (%rdx,%r14)
	movq	0(%r13), %rdx
	shrl	$12, %ebx
	movq	%r9, 8(%r13)
	movb	$0, 1(%rdx,%r14)
	movl	%ebx, %edx
	movq	8(%r13), %rbx
	andl	$48, %edx
	movzbl	(%r15,%rdx), %r15d
	movq	0(%r13), %rdx
	leaq	1(%rbx), %r14
	cmpq	%rax, %rdx
	je	.L976
	movq	16(%r13), %rcx
.L955:
	cmpq	%rcx, %r14
	ja	.L1007
.L956:
	movb	%r15b, (%rdx,%rbx)
	movq	0(%r13), %rdx
	movq	%r14, 8(%r13)
	movb	$0, 1(%rdx,%rbx)
	movq	8(%r13), %rbx
	movq	0(%r13), %rdx
	leaq	1(%rbx), %r14
	cmpq	%rax, %rdx
	je	.L977
	movq	16(%r13), %rcx
.L957:
	cmpq	%rcx, %r14
	ja	.L1008
.L958:
	movb	$61, (%rdx,%rbx)
.L994:
	movq	0(%r13), %rdx
	movq	%r14, 8(%r13)
	movb	$0, 1(%rdx,%rbx)
	movq	8(%r13), %rbx
	movq	0(%r13), %rdx
	leaq	1(%rbx), %r14
	cmpq	%rax, %rdx
	je	.L978
	movq	16(%r13), %rcx
.L959:
	cmpq	%rcx, %r14
	ja	.L1009
.L960:
	movb	$61, (%rdx,%rbx)
	movq	0(%r13), %rax
	movq	%r14, 8(%r13)
	movb	$0, 1(%rax,%rbx)
	jmp	.L952
	.p2align 4,,10
	.p2align 3
.L1001:
	movq	-64(%rbp), %rax
	leaq	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_1L12kBase64TableE(%rip), %r15
	movzbl	(%rax,%r14), %ecx
	movzbl	(%rax,%rdx), %ebx
	leaq	16(%r13), %rax
	movq	8(%r13), %r14
	movq	0(%r13), %rdx
	sall	$16, %ecx
	sall	$8, %ebx
	orl	%ecx, %ebx
	shrl	$18, %ecx
	leaq	1(%r14), %r9
	movzbl	(%r15,%rcx), %r10d
	cmpq	%rax, %rdx
	je	.L971
	movq	16(%r13), %rcx
.L944:
	cmpq	%rcx, %r9
	ja	.L1010
.L945:
	movb	%r10b, (%rdx,%r14)
	movq	0(%r13), %rdx
	movq	%r9, 8(%r13)
	movb	$0, 1(%rdx,%r14)
	movl	%ebx, %edx
	movq	8(%r13), %r14
	shrl	$12, %edx
	andl	$63, %edx
	leaq	1(%r14), %r9
	movzbl	(%r15,%rdx), %r10d
	movq	0(%r13), %rdx
	cmpq	%rax, %rdx
	je	.L972
	movq	16(%r13), %rcx
.L946:
	cmpq	%rcx, %r9
	ja	.L1011
.L947:
	movb	%r10b, (%rdx,%r14)
	shrl	$6, %ebx
	movq	0(%r13), %rdx
	andl	$60, %ebx
	movq	%r9, 8(%r13)
	movzbl	(%r15,%rbx), %r15d
	movb	$0, 1(%rdx,%r14)
	movq	8(%r13), %rbx
	movq	0(%r13), %rdx
	leaq	1(%rbx), %r14
	cmpq	%rax, %rdx
	je	.L973
	movq	16(%r13), %rcx
.L948:
	cmpq	%rcx, %r14
	ja	.L1012
.L949:
	movb	%r15b, (%rdx,%rbx)
	jmp	.L994
	.p2align 4,,10
	.p2align 3
.L965:
	movl	$15, %edx
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L979:
	movl	$15, %edx
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L1004:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movb	%r10b, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r15), %rax
	movzbl	-72(%rbp), %r10d
	jmp	.L928
	.p2align 4,,10
	.p2align 3
.L1009:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rdx
	jmp	.L960
	.p2align 4,,10
	.p2align 3
.L966:
	movl	$1, %edx
	movl	$2, %ecx
	xorl	%r14d, %r14d
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L964:
	movl	$15, %edx
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L978:
	movl	$15, %ecx
	jmp	.L959
	.p2align 4,,10
	.p2align 3
.L1008:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rdx
	movq	-56(%rbp), %rax
	jmp	.L958
	.p2align 4,,10
	.p2align 3
.L1007:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rdx
	movq	-56(%rbp), %rax
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L1006:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -72(%rbp)
	movq	%r9, -64(%rbp)
	movb	%r10b, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rdx
	movq	-72(%rbp), %rax
	movq	-64(%rbp), %r9
	movzbl	-56(%rbp), %r10d
	jmp	.L954
	.p2align 4,,10
	.p2align 3
.L1010:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -72(%rbp)
	movq	%r9, -64(%rbp)
	movb	%r10b, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rdx
	movq	-72(%rbp), %rax
	movq	-64(%rbp), %r9
	movzbl	-56(%rbp), %r10d
	jmp	.L945
	.p2align 4,,10
	.p2align 3
.L1012:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rdx
	movq	-56(%rbp), %rax
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L1011:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -72(%rbp)
	movq	%r9, -64(%rbp)
	movb	%r10b, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rdx
	movq	-72(%rbp), %rax
	movq	-64(%rbp), %r9
	movzbl	-56(%rbp), %r10d
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L977:
	movl	$15, %ecx
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L976:
	movl	$15, %ecx
	jmp	.L955
	.p2align 4,,10
	.p2align 3
.L975:
	movl	$15, %ecx
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L972:
	movl	$15, %ecx
	jmp	.L946
	.p2align 4,,10
	.p2align 3
.L971:
	movl	$15, %ecx
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L973:
	movl	$15, %ecx
	jmp	.L948
.L996:
	leaq	.LC26(%rip), %rcx
	movl	$1247, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4448:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleBinaryENS_4spanIhEE, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleBinaryENS_4spanIhEE
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleDoubleEd.str1.1,"aMS",@progbits,1
.LC50:
	.string	"-0"
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleDoubleEd,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleDoubleEd, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleDoubleEd:
.LFB4449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1013
	movq	80(%rdi), %r12
	movq	%rdi, %rbx
	cmpq	88(%rdi), %r12
	je	.L1044
	movl	-8(%r12), %edx
	movl	-4(%r12), %eax
	testl	%edx, %edx
	jne	.L1016
.L1051:
	testl	%eax, %eax
	jne	.L1045
.L1017:
	movsd	.LC49(%rip), %xmm2
	movapd	%xmm0, %xmm1
	addl	$1, %eax
	andpd	.LC48(%rip), %xmm1
	movl	%eax, -4(%r12)
	ucomisd	%xmm1, %xmm2
	jb	.L1046
	movq	8(%rbx), %rsi
	leaq	-64(%rbp), %rdi
	movq	(%rsi), %rax
	call	*24(%rax)
	movq	-64(%rbp), %r12
	movq	16(%rbx), %r13
	movzbl	(%r12), %eax
	cmpb	$46, %al
	je	.L1047
	cmpb	$45, %al
	je	.L1048
.L1026:
	movq	%r12, %rdi
	call	strlen@PLT
	movq	8(%r13), %rsi
	movq	%r13, %rdi
	movq	%r12, %rcx
	movq	%rax, %r8
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1013
	call	_ZdaPv@PLT
.L1013:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1049
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1016:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L1017
	cmpl	$2, %edx
	je	.L1029
	movl	$58, %r9d
	testb	$1, %al
	jne	.L1018
.L1029:
	movl	$44, %r9d
.L1018:
	movq	16(%rbx), %r13
	movq	8(%r13), %r14
	movq	0(%r13), %rax
	leaq	16(%r13), %rdx
	leaq	1(%r14), %r15
	cmpq	%rdx, %rax
	je	.L1030
	movq	16(%r13), %rdx
.L1019:
	cmpq	%rdx, %r15
	ja	.L1050
.L1020:
	movb	%r9b, (%rax,%r14)
	movq	0(%r13), %rax
	movq	%r15, 8(%r13)
	movb	$0, 1(%rax,%r14)
	movl	-4(%r12), %eax
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1044:
	movq	104(%rdi), %rax
	movq	-8(%rax), %r12
	movl	504(%r12), %edx
	addq	$512, %r12
	movl	-4(%r12), %eax
	testl	%edx, %edx
	jne	.L1016
	jmp	.L1051
	.p2align 4,,10
	.p2align 3
.L1048:
	cmpb	$46, 1(%r12)
	jne	.L1026
	movq	8(%r13), %rsi
	movq	%r13, %rdi
	movl	$2, %r8d
	xorl	%edx, %edx
	leaq	.LC50(%rip), %rcx
	addq	$1, %r12
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	16(%rbx), %r13
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1046:
	movq	16(%rbx), %rdi
	movl	$4, %r8d
	leaq	.LC23(%rip), %rcx
	xorl	%edx, %edx
	movq	8(%rdi), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1013
	.p2align 4,,10
	.p2align 3
.L1050:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movb	%r9b, -65(%rbp)
	movsd	%xmm0, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rax
	movsd	-80(%rbp), %xmm0
	movzbl	-65(%rbp), %r9d
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1047:
	movq	8(%r13), %r14
	movq	0(%r13), %rax
	leaq	16(%r13), %rdx
	leaq	1(%r14), %r15
	cmpq	%rdx, %rax
	je	.L1031
	movq	16(%r13), %rdx
.L1024:
	cmpq	%rdx, %r15
	ja	.L1052
.L1025:
	movb	$48, (%rax,%r14)
	movq	0(%r13), %rax
	movq	%r15, 8(%r13)
	movb	$0, 1(%rax,%r14)
	movq	16(%rbx), %r13
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1030:
	movl	$15, %edx
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1052:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rax
	jmp	.L1025
	.p2align 4,,10
	.p2align 3
.L1031:
	movl	$15, %edx
	jmp	.L1024
.L1049:
	call	__stack_chk_fail@PLT
.L1045:
	leaq	.LC26(%rip), %rcx
	movl	$1247, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4449:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleDoubleEd, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleDoubleEd
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13HandleString8ENS_4spanIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13HandleString8ENS_4spanIhEE, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13HandleString8ENS_4spanIhEE:
.LFB4446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %rax
	movq	%rdx, -56(%rbp)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1053
	movq	80(%rdi), %rbx
	movq	%rdi, %r13
	movq	%rsi, %r14
	cmpq	88(%rdi), %rbx
	je	.L1181
	movl	-8(%rbx), %edx
	movl	-4(%rbx), %eax
	testl	%edx, %edx
	jne	.L1056
.L1204:
	testl	%eax, %eax
	jne	.L1182
.L1057:
	addl	$1, %eax
	movl	%eax, -4(%rbx)
	movq	16(%r13), %rbx
	movq	8(%rbx), %r12
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	leaq	1(%r12), %r15
	cmpq	%rdx, %rax
	je	.L1137
	movq	16(%rbx), %rdx
.L1061:
	cmpq	%rdx, %r15
	ja	.L1183
.L1062:
	movb	$34, (%rax,%r12)
	movq	(%rbx), %rax
	movq	%r15, 8(%rbx)
	movq	-56(%rbp), %rbx
	movb	$0, 1(%rax,%r12)
	movq	16(%r13), %rdi
	movq	8(%rdi), %rsi
	testq	%rbx, %rbx
	je	.L1127
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L1126:
	movzbl	(%r14,%r12), %r15d
	cmpb	$34, %r15b
	je	.L1184
	cmpb	$92, %r15b
	je	.L1185
	cmpb	$8, %r15b
	je	.L1186
	cmpb	$12, %r15b
	je	.L1187
	cmpb	$10, %r15b
	je	.L1188
	cmpb	$13, %r15b
	je	.L1189
	cmpb	$9, %r15b
	je	.L1190
	leal	-32(%r15), %eax
	cmpb	$94, %al
	jbe	.L1191
	cmpb	$31, %r15b
	jbe	.L1192
	movl	%r15d, %eax
	andl	$-32, %eax
	cmpb	$-64, %al
	je	.L1193
	movl	%r15d, %eax
	andl	$-16, %eax
	cmpb	$-32, %al
	je	.L1194
	movl	%r15d, %eax
	andl	$-8, %eax
	cmpb	$-16, %al
	jne	.L1065
	andl	$7, %r15d
	movl	$3, %eax
	movl	$3, %ecx
.L1085:
	addq	%r12, %rax
	cmpq	%rbx, %rax
	jnb	.L1065
	movzbl	1(%r12,%r14), %edx
	leal	-1(%rcx), %eax
	leaq	1(%r12), %r9
	movl	%edx, %ecx
	andl	$-64, %ecx
	cmpb	$-128, %cl
	jne	.L1087
	sall	$6, %r15d
	andl	$63, %edx
	orl	%edx, %r15d
.L1087:
	testl	%eax, %eax
	je	.L1088
	movzbl	2(%r14,%r12), %edx
	movl	%edx, %ecx
	andl	$-64, %ecx
	cmpb	$-128, %cl
	jne	.L1089
	sall	$6, %r15d
	andl	$63, %edx
	orl	%edx, %r15d
.L1089:
	cmpl	$1, %eax
	je	.L1090
	movzbl	3(%r14,%r12), %edx
	movl	%edx, %ecx
	andl	$-64, %ecx
	cmpb	$-128, %cl
	jne	.L1090
	sall	$6, %r15d
	andl	$63, %edx
	orl	%edx, %r15d
.L1090:
	movslq	%eax, %r12
	leal	-128(%r15), %eax
	addq	%r9, %r12
	cmpl	$1113983, %eax
	jbe	.L1195
	.p2align 4,,10
	.p2align 3
.L1065:
	addq	$1, %r12
	cmpq	%r12, %rbx
	ja	.L1126
	.p2align 4,,10
	.p2align 3
.L1127:
	movq	(%rdi), %rax
	leaq	16(%rdi), %rdx
	leaq	1(%rsi), %rbx
	cmpq	%rdx, %rax
	je	.L1164
	movq	16(%rdi), %rdx
.L1128:
	cmpq	%rdx, %rbx
	ja	.L1196
.L1129:
	movb	$34, (%rax,%rsi)
	movq	(%rdi), %rax
	movq	%rbx, 8(%rdi)
	movb	$0, (%rax,%rbx)
.L1053:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1056:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L1057
	testb	$1, %al
	je	.L1135
	movl	$58, %r10d
	cmpl	$2, %edx
	jne	.L1058
.L1135:
	movl	$44, %r10d
.L1058:
	movq	16(%r13), %r12
	movq	8(%r12), %r15
	movq	(%r12), %rax
	leaq	16(%r12), %rdx
	leaq	1(%r15), %r9
	cmpq	%rdx, %rax
	je	.L1136
	movq	16(%r12), %rdx
.L1059:
	cmpq	%rdx, %r9
	ja	.L1197
.L1060:
	movb	%r10b, (%rax,%r15)
	movq	(%r12), %rax
	movq	%r9, 8(%r12)
	movb	$0, 1(%rax,%r15)
	movl	-4(%rbx), %eax
	jmp	.L1057
	.p2align 4,,10
	.p2align 3
.L1184:
	movl	$2, %r8d
	leaq	.LC40(%rip), %rcx
	xorl	%edx, %edx
	addq	$1, %r12
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	16(%r13), %rdi
	movq	8(%rdi), %rsi
	cmpq	%r12, %rbx
	ja	.L1126
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1185:
	movl	$2, %r8d
	leaq	.LC41(%rip), %rcx
	xorl	%edx, %edx
	addq	$1, %r12
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	16(%r13), %rdi
	movq	8(%rdi), %rsi
	cmpq	%r12, %rbx
	ja	.L1126
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1186:
	movl	$2, %r8d
	leaq	.LC42(%rip), %rcx
	xorl	%edx, %edx
	addq	$1, %r12
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	16(%r13), %rdi
	movq	8(%rdi), %rsi
	cmpq	%r12, %rbx
	ja	.L1126
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1187:
	movl	$2, %r8d
	leaq	.LC43(%rip), %rcx
	xorl	%edx, %edx
	addq	$1, %r12
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	16(%r13), %rdi
	movq	8(%rdi), %rsi
	cmpq	%r12, %rbx
	ja	.L1126
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1188:
	movl	$2, %r8d
	leaq	.LC44(%rip), %rcx
	xorl	%edx, %edx
	addq	$1, %r12
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	16(%r13), %rdi
	movq	8(%rdi), %rsi
	cmpq	%r12, %rbx
	ja	.L1126
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1191:
	movq	(%rdi), %rax
	leaq	16(%rdi), %rdx
	leaq	1(%rsi), %r9
	cmpq	%rdx, %rax
	je	.L1138
	movq	16(%rdi), %rdx
.L1073:
	cmpq	%rdx, %r9
	ja	.L1198
.L1074:
	movb	%r15b, (%rax,%rsi)
	movq	(%rdi), %rax
	addq	$1, %r12
	movq	%r9, 8(%rdi)
	movb	$0, (%rax,%r9)
	movq	16(%r13), %rdi
	movq	8(%rdi), %rsi
	cmpq	%r12, %rbx
	ja	.L1126
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1189:
	movl	$2, %r8d
	leaq	.LC45(%rip), %rcx
	xorl	%edx, %edx
	addq	$1, %r12
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	16(%r13), %rdi
	movq	8(%rdi), %rsi
	cmpq	%r12, %rbx
	ja	.L1126
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1190:
	movl	$2, %r8d
	leaq	.LC46(%rip), %rcx
	xorl	%edx, %edx
	addq	$1, %r12
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	16(%r13), %rdi
	movq	8(%rdi), %rsi
	cmpq	%r12, %rbx
	ja	.L1126
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1192:
	xorl	%edx, %edx
	movl	$2, %r8d
	leaq	.LC47(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	16(%r13), %rax
	movq	8(%rax), %rsi
	movq	(%rax), %rdx
	leaq	16(%rax), %r9
	leaq	1(%rsi), %r10
	cmpq	%r9, %rdx
	je	.L1139
	movq	16(%rax), %rcx
.L1076:
	cmpq	%r10, %rcx
	jb	.L1199
.L1077:
	movb	$48, (%rdx,%rsi)
	movq	(%rax), %rdx
	movq	%r10, 8(%rax)
	movb	$0, 1(%rdx,%rsi)
	movq	8(%rax), %rsi
	movq	(%rax), %rdx
	leaq	1(%rsi), %r10
	cmpq	%r9, %rdx
	je	.L1140
	movq	16(%rax), %rcx
.L1078:
	cmpq	%rcx, %r10
	ja	.L1200
.L1079:
	movb	$48, (%rdx,%rsi)
	movq	(%rax), %rdx
	movq	%r10, 8(%rax)
	movl	%r15d, %r10d
	movb	$0, 1(%rdx,%rsi)
	movq	8(%rax), %rsi
	shrb	$4, %r10b
	movq	(%rax), %rdx
	addl	$48, %r10d
	leaq	1(%rsi), %r11
	cmpq	%r9, %rdx
	je	.L1201
	movq	16(%rax), %rcx
.L1130:
	cmpq	%r11, %rcx
	jb	.L1202
.L1080:
	movb	%r10b, (%rdx,%rsi)
	andl	$15, %r15d
	movq	(%rax), %rdx
	cmpb	$10, %r15b
	movq	%r11, 8(%rax)
	movb	$0, 1(%rdx,%rsi)
	sbbl	%edx, %edx
	movq	8(%rax), %rsi
	andl	$-39, %edx
	leal	87(%rdx,%r15), %r15d
	movq	(%rax), %rdx
	leaq	1(%rsi), %r10
	cmpq	%rdx, %r9
	je	.L1142
	movq	16(%rax), %rcx
.L1082:
	cmpq	%rcx, %r10
	ja	.L1203
.L1083:
	movb	%r15b, (%rdx,%rsi)
	addq	$1, %r12
	movq	%r10, 8(%rax)
	movq	(%rax), %rax
	movb	$0, 1(%rax,%rsi)
	movq	16(%r13), %rdi
	movq	8(%rdi), %rsi
	cmpq	%r12, %rbx
	ja	.L1126
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1183:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1196:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rdi
	movq	-64(%rbp), %rsi
	movq	(%rdi), %rax
	jmp	.L1129
	.p2align 4,,10
	.p2align 3
.L1181:
	movq	104(%rdi), %rax
	movq	-8(%rax), %rbx
	movl	504(%rbx), %edx
	addq	$512, %rbx
	movl	-4(%rbx), %eax
	testl	%edx, %edx
	jne	.L1056
	jmp	.L1204
	.p2align 4,,10
	.p2align 3
.L1193:
	andl	$31, %r15d
	movl	$1, %eax
	movl	$1, %ecx
	jmp	.L1085
	.p2align 4,,10
	.p2align 3
.L1137:
	movl	$15, %edx
	jmp	.L1061
	.p2align 4,,10
	.p2align 3
.L1164:
	movl	$15, %edx
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1198:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rdi
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %rsi
	movq	(%rdi), %rax
	jmp	.L1074
	.p2align 4,,10
	.p2align 3
.L1194:
	andl	$15, %r15d
	movl	$2, %eax
	movl	$2, %ecx
	jmp	.L1085
	.p2align 4,,10
	.p2align 3
.L1197:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r9, -72(%rbp)
	movb	%r10b, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	movq	-72(%rbp), %r9
	movzbl	-64(%rbp), %r10d
	jmp	.L1060
	.p2align 4,,10
	.p2align 3
.L1203:
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r10, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rax
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %rsi
	movq	(%rax), %rdx
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L1202:
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r9, -88(%rbp)
	movq	%r11, -80(%rbp)
	movb	%r10b, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rax
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %r11
	movzbl	-72(%rbp), %r10d
	movq	(%rax), %rdx
	movq	-64(%rbp), %rsi
	jmp	.L1080
	.p2align 4,,10
	.p2align 3
.L1200:
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r9, -80(%rbp)
	movq	%r10, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %r10
	movq	-80(%rbp), %r9
	movq	(%rax), %rdx
	jmp	.L1079
	.p2align 4,,10
	.p2align 3
.L1199:
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r9, -80(%rbp)
	movq	%r10, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rax
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %rsi
	movq	(%rax), %rdx
	jmp	.L1077
	.p2align 4,,10
	.p2align 3
.L1138:
	movl	$15, %edx
	jmp	.L1073
	.p2align 4,,10
	.p2align 3
.L1136:
	movl	$15, %edx
	jmp	.L1059
.L1088:
	leal	-128(%r15), %eax
	movq	%r9, %r12
	cmpl	$1113983, %eax
	ja	.L1065
.L1134:
	xorl	%edx, %edx
	leaq	.LC47(%rip), %rcx
	movl	$2, %r8d
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movl	%r15d, %edx
	movq	16(%r13), %r12
	movq	-56(%rbp), %r9
	sarl	$12, %edx
	movl	$87, %eax
	movl	$48, %ecx
	cmpl	$40959, %r15d
	cmovle	%ecx, %eax
	movq	8(%r12), %rsi
	leaq	16(%r12), %r10
	addl	%edx, %eax
	movq	(%r12), %rdx
	leaq	1(%rsi), %r11
	cmpq	%r10, %rdx
	je	.L1144
	movq	16(%r12), %rcx
.L1094:
	cmpq	%rcx, %r11
	ja	.L1205
.L1095:
	movb	%al, (%rdx,%rsi)
	movq	(%r12), %rax
	movl	$87, %edx
	movl	$48, %ecx
	movq	%r11, 8(%r12)
	movb	$0, 1(%rax,%rsi)
	movl	%r15d, %eax
	movq	8(%r12), %rsi
	sarl	$8, %eax
	andl	$15, %eax
	leaq	1(%rsi), %r11
	cmpl	$10, %eax
	cmovl	%ecx, %edx
	addl	%edx, %eax
	movq	(%r12), %rdx
	cmpq	%rdx, %r10
	je	.L1146
	movq	16(%r12), %rcx
.L1097:
	cmpq	%rcx, %r11
	ja	.L1206
.L1098:
	movb	%al, (%rdx,%rsi)
	movq	(%r12), %rax
	movl	$87, %edx
	movl	$48, %ecx
	movq	%r11, 8(%r12)
	movb	$0, 1(%rax,%rsi)
	movl	%r15d, %eax
	movq	8(%r12), %rsi
	sarl	$4, %eax
	andl	$15, %eax
	leaq	1(%rsi), %r11
	cmpl	$10, %eax
	cmovl	%ecx, %edx
	addl	%edx, %eax
	movq	(%r12), %rdx
	cmpq	%rdx, %r10
	je	.L1148
	movq	16(%r12), %rcx
.L1100:
	cmpq	%rcx, %r11
	ja	.L1207
.L1101:
	movb	%al, (%rdx,%rsi)
	movq	(%r12), %rax
	andl	$15, %r15d
	movl	$48, %ecx
	movq	%r11, 8(%r12)
	cmpl	$10, %r15d
	movb	$0, 1(%rax,%rsi)
	movl	$87, %eax
	movq	(%r12), %rdx
	cmovl	%ecx, %eax
	addl	%eax, %r15d
	movq	8(%r12), %rax
	leaq	1(%rax), %r11
	cmpq	%rdx, %r10
	je	.L1150
	movq	16(%r12), %rcx
.L1103:
	cmpq	%rcx, %r11
	ja	.L1208
.L1104:
	movb	%r15b, (%rdx,%rax)
	movq	(%r12), %rdx
	movq	%r11, 8(%r12)
	movq	%r9, %r12
	movb	$0, 1(%rdx,%rax)
	movq	16(%r13), %rdi
	movq	8(%rdi), %rsi
	jmp	.L1065
.L1195:
	cmpl	$65534, %r15d
	jbe	.L1209
	subl	$65536, %r15d
	xorl	%edx, %edx
	movl	$2, %r8d
	movl	%r15d, -56(%rbp)
	shrl	$10, %r15d
	leaq	.LC47(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	subw	$10240, %r15w
	movq	16(%r13), %rdi
	movl	%r15d, %edx
	movzwl	%r15w, %eax
	shrw	$12, %dx
	movq	8(%rdi), %rsi
	leaq	16(%rdi), %r10
	cmpw	$10, %dx
	sbbl	%r9d, %r9d
	leaq	1(%rsi), %r11
	andl	$-39, %r9d
	leal	87(%r9,%rdx), %r9d
	movq	(%rdi), %rdx
	cmpq	%r10, %rdx
	je	.L1152
	movq	16(%rdi), %rcx
.L1106:
	cmpq	%rcx, %r11
	ja	.L1210
.L1107:
	movb	%r9b, (%rdx,%rsi)
	movl	%eax, %r9d
	movq	(%rdi), %rdx
	movl	$48, %ecx
	sarl	$8, %r9d
	movq	%r11, 8(%rdi)
	movb	$0, 1(%rdx,%rsi)
	movl	%r9d, %edx
	movl	$87, %r9d
	movq	8(%rdi), %rsi
	andl	$15, %edx
	cmpl	$10, %edx
	leaq	1(%rsi), %r11
	cmovl	%ecx, %r9d
	addl	%edx, %r9d
	movq	(%rdi), %rdx
	cmpq	%rdx, %r10
	je	.L1154
	movq	16(%rdi), %rcx
.L1109:
	cmpq	%rcx, %r11
	ja	.L1211
.L1110:
	movb	%r9b, (%rdx,%rsi)
	sarl	$4, %eax
	movq	(%rdi), %rdx
	movl	$48, %ecx
	andl	$15, %eax
	movq	%r11, 8(%rdi)
	cmpl	$10, %eax
	movb	$0, 1(%rdx,%rsi)
	movl	$87, %edx
	movq	8(%rdi), %rsi
	cmovl	%ecx, %edx
	leaq	1(%rsi), %r9
	addl	%edx, %eax
	movq	(%rdi), %rdx
	cmpq	%rdx, %r10
	je	.L1156
	movq	16(%rdi), %rcx
.L1112:
	cmpq	%rcx, %r9
	ja	.L1212
.L1113:
	movb	%al, (%rdx,%rsi)
	andl	$15, %r15d
	movq	(%rdi), %rax
	cmpw	$10, %r15w
	movq	%r9, 8(%rdi)
	movb	$0, 1(%rax,%rsi)
	sbbl	%eax, %eax
	movq	8(%rdi), %rsi
	movq	(%rdi), %rdx
	andl	$-39, %eax
	leal	87(%rax,%r15), %r15d
	leaq	1(%rsi), %rax
	cmpq	%rdx, %r10
	je	.L1158
	movq	16(%rdi), %rcx
.L1115:
	cmpq	%rcx, %rax
	ja	.L1213
.L1116:
	movb	%r15b, (%rdx,%rsi)
	movl	$2, %r8d
	xorl	%edx, %edx
	leaq	.LC47(%rip), %rcx
	movq	%rax, 8(%rdi)
	movq	(%rdi), %rax
	movb	$0, 1(%rax,%rsi)
	movq	16(%r13), %rdi
	movq	8(%rdi), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	16(%r13), %rdi
	movzwl	-56(%rbp), %r15d
	movq	8(%rdi), %rsi
	andw	$1023, %r15w
	movq	(%rdi), %rdx
	leaq	16(%rdi), %r10
	subw	$9216, %r15w
	movzwl	%r15w, %eax
	leaq	1(%rsi), %r9
	cmpq	%r10, %rdx
	je	.L1159
	movq	16(%rdi), %rcx
.L1117:
	cmpq	%rcx, %r9
	ja	.L1214
.L1118:
	movb	$100, (%rdx,%rsi)
	movq	(%rdi), %rdx
	movq	%r9, 8(%rdi)
	movl	%eax, %r9d
	movb	$0, 1(%rdx,%rsi)
	sarl	$8, %r9d
	movq	8(%rdi), %rsi
	movq	(%rdi), %rdx
	andl	$15, %r9d
	addl	$87, %r9d
	leaq	1(%rsi), %r11
	cmpq	%rdx, %r10
	je	.L1215
	movq	16(%rdi), %rcx
.L1132:
	cmpq	%rcx, %r11
	ja	.L1216
.L1119:
	movb	%r9b, (%rdx,%rsi)
	sarl	$4, %eax
	movq	(%rdi), %rdx
	movl	$48, %ecx
	andl	$15, %eax
	movq	%r11, 8(%rdi)
	cmpl	$10, %eax
	movb	$0, 1(%rdx,%rsi)
	movl	$87, %edx
	movq	8(%rdi), %rsi
	cmovl	%ecx, %edx
	leaq	1(%rsi), %r9
	addl	%edx, %eax
	movq	(%rdi), %rdx
	cmpq	%rdx, %r10
	je	.L1161
	movq	16(%rdi), %rcx
.L1121:
	cmpq	%rcx, %r9
	ja	.L1217
.L1122:
	movb	%al, (%rdx,%rsi)
	andl	$15, %r15d
	movq	(%rdi), %rax
	cmpw	$10, %r15w
	movq	%r9, 8(%rdi)
	movb	$0, 1(%rax,%rsi)
	sbbl	%eax, %eax
	movq	(%rdi), %rdx
	andl	$-39, %eax
	leal	87(%rax,%r15), %eax
	movq	8(%rdi), %r15
	leaq	1(%r15), %r9
	cmpq	%rdx, %r10
	je	.L1163
	movq	16(%rdi), %rcx
.L1124:
	cmpq	%rcx, %r9
	ja	.L1218
.L1125:
	movb	%al, (%rdx,%r15)
	movq	(%rdi), %rax
	movq	%r9, 8(%rdi)
	movb	$0, 1(%rax,%r15)
	movq	16(%r13), %rdi
	movq	8(%rdi), %rsi
	jmp	.L1065
.L1139:
	movl	$15, %ecx
	jmp	.L1076
.L1201:
	movl	$15, %ecx
	jmp	.L1130
.L1140:
	movl	$15, %ecx
	jmp	.L1078
.L1142:
	movl	$15, %ecx
	jmp	.L1082
.L1217:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r9, -88(%rbp)
	movb	%al, -80(%rbp)
	movq	%r10, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rdi
	movq	-88(%rbp), %r9
	movzbl	-80(%rbp), %eax
	movq	-72(%rbp), %r10
	movq	(%rdi), %rdx
	movq	-64(%rbp), %rsi
	jmp	.L1122
.L1218:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%r9, -72(%rbp)
	movb	%al, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rdi
	movq	-72(%rbp), %r9
	movzbl	-64(%rbp), %eax
	movq	(%rdi), %rdx
	jmp	.L1125
.L1216:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r11, -96(%rbp)
	movb	%r9b, -88(%rbp)
	movq	%r10, -80(%rbp)
	movl	%eax, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rdi
	movq	-96(%rbp), %r11
	movzbl	-88(%rbp), %r9d
	movq	-80(%rbp), %r10
	movq	(%rdi), %rdx
	movl	-72(%rbp), %eax
	movq	-64(%rbp), %rsi
	jmp	.L1119
.L1214:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r10, -88(%rbp)
	movq	%r9, -80(%rbp)
	movl	%eax, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rdi
	movq	-64(%rbp), %rsi
	movl	-72(%rbp), %eax
	movq	-80(%rbp), %r9
	movq	(%rdi), %rdx
	movq	-88(%rbp), %r10
	jmp	.L1118
.L1213:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rax, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-64(%rbp), %rdi
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rsi
	movq	(%rdi), %rdx
	jmp	.L1116
.L1212:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r9, -96(%rbp)
	movb	%al, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-64(%rbp), %rdi
	movq	-96(%rbp), %r9
	movzbl	-88(%rbp), %eax
	movq	-80(%rbp), %r10
	movq	(%rdi), %rdx
	movq	-72(%rbp), %rsi
	jmp	.L1113
.L1211:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r11, -104(%rbp)
	movb	%r9b, -96(%rbp)
	movq	%r10, -88(%rbp)
	movl	%eax, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-64(%rbp), %rdi
	movq	-104(%rbp), %r11
	movzbl	-96(%rbp), %r9d
	movq	-88(%rbp), %r10
	movq	(%rdi), %rdx
	movl	-80(%rbp), %eax
	movq	-72(%rbp), %rsi
	jmp	.L1110
.L1210:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r10, -104(%rbp)
	movq	%r11, -96(%rbp)
	movb	%r9b, -88(%rbp)
	movl	%eax, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-64(%rbp), %rdi
	movq	-104(%rbp), %r10
	movq	-96(%rbp), %r11
	movzbl	-88(%rbp), %r9d
	movq	(%rdi), %rdx
	movl	-80(%rbp), %eax
	movq	-72(%rbp), %rsi
	jmp	.L1107
.L1156:
	movl	$15, %ecx
	jmp	.L1112
.L1154:
	movl	$15, %ecx
	jmp	.L1109
.L1163:
	movl	$15, %ecx
	jmp	.L1124
.L1152:
	movl	$15, %ecx
	jmp	.L1106
.L1159:
	movl	$15, %ecx
	jmp	.L1117
.L1158:
	movl	$15, %ecx
	jmp	.L1115
.L1161:
	movl	$15, %ecx
	jmp	.L1121
.L1215:
	movl	$15, %ecx
	jmp	.L1132
.L1208:
	xorl	%edx, %edx
	movq	%rax, %rsi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r11, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rdx
	movq	-72(%rbp), %r11
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %rax
	jmp	.L1104
.L1207:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r11, -88(%rbp)
	movb	%al, -80(%rbp)
	movq	%r10, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rdx
	movq	-88(%rbp), %r11
	movzbl	-80(%rbp), %eax
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %rsi
	jmp	.L1101
.L1206:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r11, -88(%rbp)
	movb	%al, -80(%rbp)
	movq	%r10, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rdx
	movq	-88(%rbp), %r11
	movzbl	-80(%rbp), %eax
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %rsi
	jmp	.L1098
.L1205:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r10, -88(%rbp)
	movq	%r11, -80(%rbp)
	movb	%al, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rdx
	movq	-88(%rbp), %r10
	movq	-80(%rbp), %r11
	movzbl	-72(%rbp), %eax
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %rsi
	jmp	.L1095
.L1146:
	movl	$15, %ecx
	jmp	.L1097
.L1144:
	movl	$15, %ecx
	jmp	.L1094
.L1150:
	movl	$15, %ecx
	jmp	.L1103
.L1148:
	movl	$15, %ecx
	jmp	.L1100
.L1182:
	leaq	.LC26(%rip), %rcx
	movl	$1247, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	call	__assert_fail@PLT
.L1209:
	movq	%r12, %r9
	jmp	.L1134
	.cfi_endproc
.LFE4446:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13HandleString8ENS_4spanIhEE, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13HandleString8ENS_4spanIhEE
	.section	.rodata._ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc.str1.1,"aMS",@progbits,1
.LC51:
	.string	"%lu"
	.section	.rodata._ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc.str1.8,"aMS",@progbits,1
	.align 8
.LC52:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc.str1.1
.LC53:
	.string	"basic_string::append"
.LC54:
	.string	" at position "
	.section	.text._ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	.type	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc, @function
_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc:
.LFB3135:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC51(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movl	$32, %edx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-144(%rbp), %rbx
	subq	$152, %rsp
	movq	8(%rsi), %r8
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-96(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	%rax, %rdi
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movq	%rbx, -160(%rbp)
	testq	%r15, %r15
	je	.L1244
	movq	%r15, %rdi
	leaq	-160(%rbp), %r14
	call	strlen@PLT
	movq	%rax, -168(%rbp)
	movq	%rax, %r13
	cmpq	$15, %rax
	ja	.L1245
	cmpq	$1, %rax
	jne	.L1223
	movzbl	(%r15), %edx
	movb	%dl, -144(%rbp)
	movq	%rbx, %rdx
.L1224:
	movq	%rax, -152(%rbp)
	movb	$0, (%rdx,%rax)
	movabsq	$4611686018427387903, %rax
	subq	-152(%rbp), %rax
	cmpq	$12, %rax
	jbe	.L1246
	movl	$13, %edx
	movq	%r14, %rdi
	leaq	.LC54(%rip), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	leaq	-112(%rbp), %r14
	movq	%r14, -128(%rbp)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L1247
	movq	%rcx, -128(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -112(%rbp)
.L1227:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	leaq	-80(%rbp), %r13
	movq	%rcx, -120(%rbp)
	movq	%rdx, (%rax)
	movq	-128(%rbp), %r9
	movq	$0, 8(%rax)
	movl	$15, %eax
	movq	-120(%rbp), %r8
	movq	-88(%rbp), %rdx
	cmpq	%r14, %r9
	movq	%rax, %rsi
	cmovne	-112(%rbp), %rsi
	movq	-96(%rbp), %r10
	leaq	(%r8,%rdx), %rcx
	cmpq	%rsi, %rcx
	jbe	.L1229
	cmpq	%r13, %r10
	cmovne	-80(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L1248
.L1229:
	leaq	-128(%rbp), %rdi
	movq	%r10, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L1231:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L1249
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L1233:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	-128(%rbp), %rdi
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	cmpq	%r14, %rdi
	je	.L1234
	call	_ZdlPv@PLT
.L1234:
	movq	-160(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1235
	call	_ZdlPv@PLT
.L1235:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1219
	call	_ZdlPv@PLT
.L1219:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1250
	addq	$152, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1223:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L1251
	movq	%rbx, %rdx
	jmp	.L1224
	.p2align 4,,10
	.p2align 3
.L1245:
	movq	%r14, %rdi
	leaq	-168(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -160(%rbp)
	movq	%rax, %rdi
	movq	-168(%rbp), %rax
	movq	%rax, -144(%rbp)
.L1222:
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-168(%rbp), %rax
	movq	-160(%rbp), %rdx
	jmp	.L1224
	.p2align 4,,10
	.p2align 3
.L1244:
	leaq	.LC52(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L1249:
	movdqu	16(%rax), %xmm1
	movups	%xmm1, 16(%r12)
	jmp	.L1233
	.p2align 4,,10
	.p2align 3
.L1247:
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, -112(%rbp)
	jmp	.L1227
	.p2align 4,,10
	.p2align 3
.L1248:
	movq	-184(%rbp), %rdi
	movq	%r9, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1231
.L1246:
	leaq	.LC53(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1250:
	call	__stack_chk_fail@PLT
.L1251:
	movq	%rbx, %rdi
	jmp	.L1222
	.cfi_endproc
.LFE3135:
	.size	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc, .-_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	.section	.rodata._ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11Ev.str1.8,"aMS",@progbits,1
	.align 8
.LC55:
	.string	"JSON: unprocessed input remains"
	.section	.rodata._ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11Ev.str1.1,"aMS",@progbits,1
.LC56:
	.string	"JSON: stack limit exceeded"
.LC57:
	.string	"JSON: no input"
.LC58:
	.string	"JSON: invalid token"
.LC59:
	.string	"JSON: invalid number"
.LC60:
	.string	"JSON: invalid string"
.LC61:
	.string	"JSON: unexpected array end"
	.section	.rodata._ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11Ev.str1.8
	.align 8
.LC62:
	.string	"JSON: comma or array end expected"
	.section	.rodata._ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11Ev.str1.1
.LC63:
	.string	"JSON: string literal expected"
.LC64:
	.string	"JSON: colon expected"
.LC65:
	.string	"JSON: unexpected map end"
	.section	.rodata._ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11Ev.str1.8
	.align 8
.LC66:
	.string	"JSON: comma or map end expected"
	.section	.rodata._ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11Ev.str1.1
.LC67:
	.string	"JSON: value expected"
.LC68:
	.string	"CBOR: invalid int32"
.LC69:
	.string	"CBOR: invalid double"
.LC70:
	.string	"CBOR: invalid envelope"
	.section	.rodata._ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11Ev.str1.8
	.align 8
.LC71:
	.string	"CBOR: envelope contents length mismatch"
	.align 8
.LC72:
	.string	"CBOR: map or array expected in envelope"
	.section	.rodata._ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11Ev.str1.1
.LC73:
	.string	"CBOR: invalid string8"
.LC74:
	.string	"CBOR: invalid string16"
.LC75:
	.string	"CBOR: invalid binary"
.LC76:
	.string	"CBOR: unsupported value"
.LC77:
	.string	"CBOR: no input"
.LC78:
	.string	"CBOR: invalid start byte"
	.section	.rodata._ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11Ev.str1.8
	.align 8
.LC79:
	.string	"CBOR: unexpected eof expected value"
	.section	.rodata._ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11Ev.str1.1
.LC80:
	.string	"CBOR: unexpected eof in array"
.LC81:
	.string	"CBOR: unexpected eof in map"
.LC82:
	.string	"CBOR: invalid map key"
.LC83:
	.string	"CBOR: stack limit exceeded"
.LC84:
	.string	"CBOR: trailing junk"
.LC85:
	.string	"CBOR: map start expected"
.LC86:
	.string	"CBOR: map stop expected"
	.section	.rodata._ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11Ev.str1.8
	.align 8
.LC87:
	.string	"CBOR: envelope size limit exceeded"
	.section	.text._ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11Ev
	.type	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11Ev, @function
_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11Ev:
.LFB3134:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$33, (%rsi)
	ja	.L1253
	movl	(%rsi), %eax
	leaq	.L1255(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11Ev,"a",@progbits
	.align 4
	.align 4
.L1255:
	.long	.L1288-.L1255
	.long	.L1287-.L1255
	.long	.L1286-.L1255
	.long	.L1285-.L1255
	.long	.L1284-.L1255
	.long	.L1283-.L1255
	.long	.L1282-.L1255
	.long	.L1281-.L1255
	.long	.L1280-.L1255
	.long	.L1279-.L1255
	.long	.L1278-.L1255
	.long	.L1277-.L1255
	.long	.L1276-.L1255
	.long	.L1275-.L1255
	.long	.L1274-.L1255
	.long	.L1273-.L1255
	.long	.L1272-.L1255
	.long	.L1271-.L1255
	.long	.L1270-.L1255
	.long	.L1269-.L1255
	.long	.L1268-.L1255
	.long	.L1267-.L1255
	.long	.L1266-.L1255
	.long	.L1265-.L1255
	.long	.L1264-.L1255
	.long	.L1263-.L1255
	.long	.L1262-.L1255
	.long	.L1261-.L1255
	.long	.L1260-.L1255
	.long	.L1259-.L1255
	.long	.L1258-.L1255
	.long	.L1257-.L1255
	.long	.L1256-.L1255
	.long	.L1254-.L1255
	.section	.text._ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11Ev
	.p2align 4,,10
	.p2align 3
.L1256:
	leaq	.LC86(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	.p2align 4,,10
	.p2align 3
.L1252:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1292
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1257:
	.cfi_restore_state
	leaq	.LC85(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1258:
	leaq	.LC84(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1259:
	leaq	.LC83(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1260:
	leaq	.LC82(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1261:
	leaq	.LC81(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1262:
	leaq	.LC80(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1263:
	leaq	.LC79(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1264:
	leaq	.LC78(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1265:
	leaq	.LC77(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1266:
	leaq	.LC76(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1267:
	leaq	.LC75(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1268:
	leaq	.LC74(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1269:
	leaq	.LC73(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1270:
	leaq	.LC72(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1271:
	leaq	.LC71(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1272:
	leaq	.LC70(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1273:
	leaq	.LC69(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1274:
	leaq	.LC68(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1275:
	leaq	.LC67(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1276:
	leaq	.LC66(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1277:
	leaq	.LC65(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1278:
	leaq	.LC64(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1279:
	leaq	.LC63(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1280:
	leaq	.LC62(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1281:
	leaq	.LC61(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1282:
	leaq	.LC60(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1283:
	leaq	.LC59(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1284:
	leaq	.LC58(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1285:
	leaq	.LC57(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1286:
	leaq	.LC56(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1287:
	leaq	.LC55(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1288:
	leaq	16(%rdi), %rax
	movl	$19279, %ecx
	movq	$2, 8(%rdi)
	movq	%rax, (%rdi)
	movw	%cx, 16(%rdi)
	movb	$0, 18(%rdi)
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1254:
	leaq	.LC87(%rip), %rdx
	call	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L1252
.L1253:
	leaq	16(%rdi), %rax
	xorl	%edx, %edx
	leaq	-32(%rbp), %rsi
	movq	$18, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC88(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$17732, %edx
	movw	%dx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	jmp	.L1252
.L1292:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3134:
	.size	_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11Ev, .-_ZNK30v8_inspector_protocol_encoding6Status13ToASCIIStringB5cxx11Ev
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor9internals14ReadTokenStartENS_4spanIhEEPNS0_9MajorTypeEPm,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor9internals14ReadTokenStartENS_4spanIhEEPNS0_9MajorTypeEPm
	.type	_ZN30v8_inspector_protocol_encoding4cbor9internals14ReadTokenStartENS_4spanIhEEPNS0_9MajorTypeEPm, @function
_ZN30v8_inspector_protocol_encoding4cbor9internals14ReadTokenStartENS_4spanIhEEPNS0_9MajorTypeEPm:
.LFB3139:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1298
	movzbl	(%rdi), %eax
	movl	%eax, %r8d
	andl	$31, %eax
	shrb	$5, %r8b
	movzbl	%r8b, %r8d
	movl	%r8d, (%rdx)
	cmpb	$23, %al
	jbe	.L1301
	cmpb	$24, %al
	je	.L1302
	cmpb	$25, %al
	je	.L1303
	cmpb	$26, %al
	je	.L1304
	cmpq	$8, %rsi
	jbe	.L1298
	cmpb	$27, %al
	je	.L1305
.L1298:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1301:
	movzbl	%al, %eax
	movq	%rax, (%rcx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1303:
	cmpq	$2, %rsi
	jbe	.L1298
	movzbl	1(%rdi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	2(%rdi), %eax
	orl	%edx, %eax
	movzwl	%ax, %eax
	movq	%rax, (%rcx)
	movl	$3, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1302:
	cmpq	$1, %rsi
	jbe	.L1298
	movzbl	1(%rdi), %eax
	movq	%rax, (%rcx)
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1304:
	cmpq	$4, %rsi
	jbe	.L1298
	movzbl	3(%rdi), %edx
	movzbl	4(%rdi), %eax
	sall	$8, %edx
	orl	%eax, %edx
	movzbl	2(%rdi), %eax
	sall	$16, %eax
	orl	%eax, %edx
	movzbl	1(%rdi), %eax
	sall	$24, %eax
	orl	%edx, %eax
	movq	%rax, (%rcx)
	movl	$5, %eax
	ret
.L1305:
	movzbl	7(%rdi), %edx
	movzbl	8(%rdi), %eax
	salq	$8, %rdx
	orq	%rax, %rdx
	movzbl	6(%rdi), %eax
	salq	$16, %rax
	orq	%rax, %rdx
	movzbl	5(%rdi), %eax
	salq	$24, %rax
	orq	%rdx, %rax
	movzbl	4(%rdi), %edx
	salq	$32, %rdx
	orq	%rdx, %rax
	movzbl	3(%rdi), %edx
	salq	$40, %rdx
	orq	%rax, %rdx
	movzbl	2(%rdi), %eax
	salq	$48, %rax
	orq	%rax, %rdx
	movzbl	1(%rdi), %eax
	salq	$56, %rax
	orq	%rdx, %rax
	movq	%rax, (%rcx)
	movl	$9, %eax
	ret
	.cfi_endproc
.LFE3139:
	.size	_ZN30v8_inspector_protocol_encoding4cbor9internals14ReadTokenStartENS_4spanIhEEPNS0_9MajorTypeEPm, .-_ZN30v8_inspector_protocol_encoding4cbor9internals14ReadTokenStartENS_4spanIhEEPNS0_9MajorTypeEPm
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor22InitialByteForEnvelopeEv,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor22InitialByteForEnvelopeEv
	.type	_ZN30v8_inspector_protocol_encoding4cbor22InitialByteForEnvelopeEv, @function
_ZN30v8_inspector_protocol_encoding4cbor22InitialByteForEnvelopeEv:
.LFB3143:
	.cfi_startproc
	endbr64
	movl	$-40, %eax
	ret
	.cfi_endproc
.LFE3143:
	.size	_ZN30v8_inspector_protocol_encoding4cbor22InitialByteForEnvelopeEv, .-_ZN30v8_inspector_protocol_encoding4cbor22InitialByteForEnvelopeEv
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor35InitialByteFor32BitLengthByteStringEv,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor35InitialByteFor32BitLengthByteStringEv
	.type	_ZN30v8_inspector_protocol_encoding4cbor35InitialByteFor32BitLengthByteStringEv, @function
_ZN30v8_inspector_protocol_encoding4cbor35InitialByteFor32BitLengthByteStringEv:
.LFB3144:
	.cfi_startproc
	endbr64
	movl	$90, %eax
	ret
	.cfi_endproc
.LFE3144:
	.size	_ZN30v8_inspector_protocol_encoding4cbor35InitialByteFor32BitLengthByteStringEv, .-_ZN30v8_inspector_protocol_encoding4cbor35InitialByteFor32BitLengthByteStringEv
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor13IsCBORMessageENS_4spanIhEE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor13IsCBORMessageENS_4spanIhEE
	.type	_ZN30v8_inspector_protocol_encoding4cbor13IsCBORMessageENS_4spanIhEE, @function
_ZN30v8_inspector_protocol_encoding4cbor13IsCBORMessageENS_4spanIhEE:
.LFB3145:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$5, %rsi
	jbe	.L1308
	cmpb	$-40, (%rdi)
	je	.L1312
.L1308:
	ret
	.p2align 4,,10
	.p2align 3
.L1312:
	cmpb	$90, 1(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE3145:
	.size	_ZN30v8_inspector_protocol_encoding4cbor13IsCBORMessageENS_4spanIhEE, .-_ZN30v8_inspector_protocol_encoding4cbor13IsCBORMessageENS_4spanIhEE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor10EncodeTrueEv,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor10EncodeTrueEv
	.type	_ZN30v8_inspector_protocol_encoding4cbor10EncodeTrueEv, @function
_ZN30v8_inspector_protocol_encoding4cbor10EncodeTrueEv:
.LFB3146:
	.cfi_startproc
	endbr64
	movl	$-11, %eax
	ret
	.cfi_endproc
.LFE3146:
	.size	_ZN30v8_inspector_protocol_encoding4cbor10EncodeTrueEv, .-_ZN30v8_inspector_protocol_encoding4cbor10EncodeTrueEv
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor11EncodeFalseEv,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor11EncodeFalseEv
	.type	_ZN30v8_inspector_protocol_encoding4cbor11EncodeFalseEv, @function
_ZN30v8_inspector_protocol_encoding4cbor11EncodeFalseEv:
.LFB3147:
	.cfi_startproc
	endbr64
	movl	$-12, %eax
	ret
	.cfi_endproc
.LFE3147:
	.size	_ZN30v8_inspector_protocol_encoding4cbor11EncodeFalseEv, .-_ZN30v8_inspector_protocol_encoding4cbor11EncodeFalseEv
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor10EncodeNullEv,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor10EncodeNullEv
	.type	_ZN30v8_inspector_protocol_encoding4cbor10EncodeNullEv, @function
_ZN30v8_inspector_protocol_encoding4cbor10EncodeNullEv:
.LFB3148:
	.cfi_startproc
	endbr64
	movl	$-10, %eax
	ret
	.cfi_endproc
.LFE3148:
	.size	_ZN30v8_inspector_protocol_encoding4cbor10EncodeNullEv, .-_ZN30v8_inspector_protocol_encoding4cbor10EncodeNullEv
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor32EncodeIndefiniteLengthArrayStartEv,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor32EncodeIndefiniteLengthArrayStartEv
	.type	_ZN30v8_inspector_protocol_encoding4cbor32EncodeIndefiniteLengthArrayStartEv, @function
_ZN30v8_inspector_protocol_encoding4cbor32EncodeIndefiniteLengthArrayStartEv:
.LFB3149:
	.cfi_startproc
	endbr64
	movl	$-97, %eax
	ret
	.cfi_endproc
.LFE3149:
	.size	_ZN30v8_inspector_protocol_encoding4cbor32EncodeIndefiniteLengthArrayStartEv, .-_ZN30v8_inspector_protocol_encoding4cbor32EncodeIndefiniteLengthArrayStartEv
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor30EncodeIndefiniteLengthMapStartEv,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor30EncodeIndefiniteLengthMapStartEv
	.type	_ZN30v8_inspector_protocol_encoding4cbor30EncodeIndefiniteLengthMapStartEv, @function
_ZN30v8_inspector_protocol_encoding4cbor30EncodeIndefiniteLengthMapStartEv:
.LFB3150:
	.cfi_startproc
	endbr64
	movl	$-65, %eax
	ret
	.cfi_endproc
.LFE3150:
	.size	_ZN30v8_inspector_protocol_encoding4cbor30EncodeIndefiniteLengthMapStartEv, .-_ZN30v8_inspector_protocol_encoding4cbor30EncodeIndefiniteLengthMapStartEv
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor10EncodeStopEv,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor10EncodeStopEv
	.type	_ZN30v8_inspector_protocol_encoding4cbor10EncodeStopEv, @function
_ZN30v8_inspector_protocol_encoding4cbor10EncodeStopEv:
.LFB3151:
	.cfi_startproc
	endbr64
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE3151:
	.size	_ZN30v8_inspector_protocol_encoding4cbor10EncodeStopEv, .-_ZN30v8_inspector_protocol_encoding4cbor10EncodeStopEv
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12EncodeDoubleEdPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor12EncodeDoubleEdPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN30v8_inspector_protocol_encoding4cbor12EncodeDoubleEdPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN30v8_inspector_protocol_encoding4cbor12EncodeDoubleEdPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB3172:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	16(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%xmm0, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %r14
	movq	(%rdi), %rax
	leaq	1(%r14), %r15
	cmpq	%r13, %rax
	je	.L1338
	movq	16(%rdi), %rdx
.L1320:
	cmpq	%rdx, %r15
	ja	.L1348
.L1321:
	movb	$-5, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$56, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L1339
	movq	16(%rbx), %rdx
.L1322:
	cmpq	%rdx, %r15
	ja	.L1349
.L1323:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$48, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L1340
	movq	16(%rbx), %rdx
.L1324:
	cmpq	%rdx, %r15
	ja	.L1350
.L1325:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$40, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L1341
	movq	16(%rbx), %rdx
.L1326:
	cmpq	%rdx, %r15
	ja	.L1351
.L1327:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$32, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L1342
	movq	16(%rbx), %rdx
.L1328:
	cmpq	%rdx, %r15
	ja	.L1352
.L1329:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$24, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L1343
	movq	16(%rbx), %rdx
.L1330:
	cmpq	%rdx, %r15
	ja	.L1353
.L1331:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$16, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L1344
	movq	16(%rbx), %rdx
.L1332:
	cmpq	%rdx, %r15
	ja	.L1354
.L1333:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$8, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L1345
	movq	16(%rbx), %rdx
.L1334:
	cmpq	%rdx, %r15
	ja	.L1355
.L1335:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r15, 8(%rbx)
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L1346
	movq	16(%rbx), %rdx
.L1336:
	cmpq	%rdx, %r15
	ja	.L1356
.L1337:
	movb	%r12b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r15, 8(%rbx)
	movb	$0, 1(%rax,%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1348:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L1321
	.p2align 4,,10
	.p2align 3
.L1356:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L1337
	.p2align 4,,10
	.p2align 3
.L1355:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L1335
	.p2align 4,,10
	.p2align 3
.L1354:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L1333
	.p2align 4,,10
	.p2align 3
.L1353:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1352:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L1329
	.p2align 4,,10
	.p2align 3
.L1351:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L1327
	.p2align 4,,10
	.p2align 3
.L1350:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L1325
	.p2align 4,,10
	.p2align 3
.L1349:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L1323
	.p2align 4,,10
	.p2align 3
.L1339:
	movl	$15, %edx
	jmp	.L1322
	.p2align 4,,10
	.p2align 3
.L1344:
	movl	$15, %edx
	jmp	.L1332
	.p2align 4,,10
	.p2align 3
.L1345:
	movl	$15, %edx
	jmp	.L1334
	.p2align 4,,10
	.p2align 3
.L1346:
	movl	$15, %edx
	jmp	.L1336
	.p2align 4,,10
	.p2align 3
.L1338:
	movl	$15, %edx
	jmp	.L1320
	.p2align 4,,10
	.p2align 3
.L1340:
	movl	$15, %edx
	jmp	.L1324
	.p2align 4,,10
	.p2align 3
.L1341:
	movl	$15, %edx
	jmp	.L1326
	.p2align 4,,10
	.p2align 3
.L1342:
	movl	$15, %edx
	jmp	.L1328
	.p2align 4,,10
	.p2align 3
.L1343:
	movl	$15, %edx
	jmp	.L1330
	.cfi_endproc
.LFE3172:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12EncodeDoubleEdPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN30v8_inspector_protocol_encoding4cbor12EncodeDoubleEdPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder11EncodeStartEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC89:
	.string	"void v8_inspector_protocol_encoding::cbor::EncodeStartTmpl(C*, size_t*) [with C = std::__cxx11::basic_string<char>; size_t = long unsigned int]"
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder11EncodeStartEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.str1.1,"aMS",@progbits,1
.LC90:
	.string	"*byte_size_pos == 0"
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder11EncodeStartEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder11EncodeStartEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder11EncodeStartEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder11EncodeStartEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB3175:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	$0, (%rdi)
	jne	.L1366
	movq	8(%rsi), %r13
	movq	(%rsi), %rax
	leaq	16(%rsi), %r15
	movq	%rdi, %rbx
	movq	%rsi, %r12
	leaq	1(%r13), %r14
	cmpq	%r15, %rax
	je	.L1363
	movq	16(%rsi), %rdx
.L1359:
	cmpq	%rdx, %r14
	ja	.L1367
.L1360:
	movb	$-40, (%rax,%r13)
	movq	(%r12), %rax
	movq	%r14, 8(%r12)
	movb	$0, 1(%rax,%r13)
	movq	8(%r12), %r13
	movq	(%r12), %rax
	leaq	1(%r13), %r14
	cmpq	%r15, %rax
	je	.L1364
	movq	16(%r12), %rdx
.L1361:
	cmpq	%rdx, %r14
	ja	.L1368
.L1362:
	movb	$90, (%rax,%r13)
	movq	(%r12), %rax
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r14, 8(%r12)
	movb	$0, 1(%rax,%r13)
	movq	8(%r12), %rsi
	movq	%rsi, (%rbx)
	addq	$8, %rsp
	addq	$4, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6resizeEmc@PLT
	.p2align 4,,10
	.p2align 3
.L1367:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	jmp	.L1360
	.p2align 4,,10
	.p2align 3
.L1368:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	jmp	.L1362
	.p2align 4,,10
	.p2align 3
.L1364:
	movl	$15, %edx
	jmp	.L1361
	.p2align 4,,10
	.p2align 3
.L1363:
	movl	$15, %edx
	jmp	.L1359
.L1366:
	leaq	.LC89(%rip), %rcx
	movl	$472, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC90(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE3175:
	.size	_ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder11EncodeStartEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder11EncodeStartEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder10EncodeStopEPSt6vectorIhSaIhEE.str1.8,"aMS",@progbits,1
	.align 8
.LC91:
	.string	"bool v8_inspector_protocol_encoding::cbor::EncodeStopTmpl(C*, size_t*) [with C = std::vector<unsigned char>; size_t = long unsigned int]"
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder10EncodeStopEPSt6vectorIhSaIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder10EncodeStopEPSt6vectorIhSaIhEE
	.type	_ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder10EncodeStopEPSt6vectorIhSaIhEE, @function
_ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder10EncodeStopEPSt6vectorIhSaIhEE:
.LFB3177:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L1376
	movq	8(%rsi), %rax
	subq	(%rsi), %rax
	movl	$4294967295, %ecx
	xorl	%r8d, %r8d
	subq	$4, %rax
	subq	%rdx, %rax
	cmpq	%rcx, %rax
	ja	.L1369
	leaq	1(%rdx), %rcx
	movq	%rax, %r8
	movq	%rcx, (%rdi)
	movq	(%rsi), %rcx
	shrq	$24, %r8
	movb	%r8b, (%rcx,%rdx)
	movq	(%rdi), %rdx
	movq	%rax, %r8
	shrq	$16, %r8
	leaq	1(%rdx), %rcx
	movq	%rcx, (%rdi)
	movq	(%rsi), %rcx
	movb	%r8b, (%rcx,%rdx)
	movq	(%rdi), %rdx
	movq	%rax, %r8
	shrq	$8, %r8
	leaq	1(%rdx), %rcx
	movq	%rcx, (%rdi)
	movq	(%rsi), %rcx
	movb	%r8b, (%rcx,%rdx)
	movq	(%rdi), %rdx
	movl	$1, %r8d
	leaq	1(%rdx), %rcx
	movq	%rcx, (%rdi)
	movq	(%rsi), %rcx
	movb	%al, (%rcx,%rdx)
.L1369:
	movl	%r8d, %eax
	ret
.L1376:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC91(%rip), %rcx
	movl	$489, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE3177:
	.size	_ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder10EncodeStopEPSt6vectorIhSaIhEE, .-_ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder10EncodeStopEPSt6vectorIhSaIhEE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder10EncodeStopEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder10EncodeStopEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder10EncodeStopEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder10EncodeStopEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB3178:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L1384
	movq	8(%rsi), %rax
	movl	$4294967295, %ecx
	xorl	%r8d, %r8d
	subq	$4, %rax
	subq	%rdx, %rax
	cmpq	%rcx, %rax
	ja	.L1377
	leaq	1(%rdx), %rcx
	movq	%rax, %r8
	movq	%rcx, (%rdi)
	movq	(%rsi), %rcx
	shrq	$24, %r8
	movb	%r8b, (%rcx,%rdx)
	movq	(%rdi), %rdx
	movq	%rax, %r8
	shrq	$16, %r8
	leaq	1(%rdx), %rcx
	movq	%rcx, (%rdi)
	movq	(%rsi), %rcx
	movb	%r8b, (%rcx,%rdx)
	movq	(%rdi), %rdx
	movq	%rax, %r8
	shrq	$8, %r8
	leaq	1(%rdx), %rcx
	movq	%rcx, (%rdi)
	movq	(%rsi), %rcx
	movb	%r8b, (%rcx,%rdx)
	movq	(%rdi), %rdx
	movl	$1, %r8d
	leaq	1(%rdx), %rcx
	movq	%rcx, (%rdi)
	movq	(%rsi), %rcx
	movb	%al, (%rcx,%rdx)
.L1377:
	movl	%r8d, %eax
	ret
.L1384:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC35(%rip), %rcx
	movl	$489, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE3178:
	.size	_ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder10EncodeStopEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder10EncodeStopEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor14NewCBOREncoderEPSt6vectorIhSaIhEEPNS_6StatusE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor14NewCBOREncoderEPSt6vectorIhSaIhEEPNS_6StatusE
	.type	_ZN30v8_inspector_protocol_encoding4cbor14NewCBOREncoderEPSt6vectorIhSaIhEEPNS_6StatusE, @function
_ZN30v8_inspector_protocol_encoding4cbor14NewCBOREncoderEPSt6vectorIhSaIhEEPNS_6StatusE:
.LFB3194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$48, %edi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_Znwm@PLT
	leaq	16+_ZTVN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEEE(%rip), %rcx
	movl	$0, (%rbx)
	movq	%rcx, (%rax)
	movq	%r13, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	%rbx, 40(%rax)
	movq	$-1, 8(%rbx)
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3194:
	.size	_ZN30v8_inspector_protocol_encoding4cbor14NewCBOREncoderEPSt6vectorIhSaIhEEPNS_6StatusE, .-_ZN30v8_inspector_protocol_encoding4cbor14NewCBOREncoderEPSt6vectorIhSaIhEEPNS_6StatusE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor14NewCBOREncoderEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS_6StatusE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor14NewCBOREncoderEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS_6StatusE
	.type	_ZN30v8_inspector_protocol_encoding4cbor14NewCBOREncoderEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS_6StatusE, @function
_ZN30v8_inspector_protocol_encoding4cbor14NewCBOREncoderEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS_6StatusE:
.LFB3195:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$48, %edi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_Znwm@PLT
	leaq	16+_ZTVN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rcx
	movl	$0, (%rbx)
	movq	%rcx, (%rax)
	movq	%r13, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	%rbx, 40(%rax)
	movq	$-1, 8(%rbx)
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3195:
	.size	_ZN30v8_inspector_protocol_encoding4cbor14NewCBOREncoderEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS_6StatusE, .-_ZN30v8_inspector_protocol_encoding4cbor14NewCBOREncoderEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS_6StatusE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizerD2Ev
	.type	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizerD2Ev, @function
_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizerD2Ev:
.LFB3203:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3203:
	.size	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizerD2Ev, .-_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizerD2Ev
	.globl	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizerD1Ev
	.set	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizerD1Ev,_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizerD2Ev
	.section	.text._ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8TokenTagEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8TokenTagEv
	.type	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8TokenTagEv, @function
_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8TokenTagEv:
.LFB3205:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	ret
	.cfi_endproc
.LFE3205:
	.size	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8TokenTagEv, .-_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8TokenTagEv
	.section	.text._ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer6StatusEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer6StatusEv
	.type	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer6StatusEv, @function
_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer6StatusEv:
.LFB3208:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdx
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE3208:
	.size	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer6StatusEv, .-_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer6StatusEv
	.section	.text._ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8GetInt32Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8GetInt32Ev
	.type	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8GetInt32Ev, @function
_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8GetInt32Ev:
.LFB3209:
	.cfi_startproc
	endbr64
	cmpl	$4, 16(%rdi)
	jne	.L1398
	movq	56(%rdi), %rdx
	movl	%edx, %eax
	movl	48(%rdi), %edx
	testl	%edx, %edx
	jne	.L1399
	ret
	.p2align 4,,10
	.p2align 3
.L1399:
	notl	%eax
	ret
.L1398:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8GetInt32Ev.part.0
	.cfi_endproc
.LFE3209:
	.size	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8GetInt32Ev, .-_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8GetInt32Ev
	.section	.rodata._ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer9GetDoubleEv.str1.8,"aMS",@progbits,1
	.align 8
.LC92:
	.string	"double v8_inspector_protocol_encoding::cbor::CBORTokenizer::GetDouble() const"
	.align 8
.LC93:
	.string	"token_tag_ == CBORTokenTag::DOUBLE"
	.section	.text._ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer9GetDoubleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer9GetDoubleEv
	.type	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer9GetDoubleEv, @function
_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer9GetDoubleEv:
.LFB3210:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpl	$5, 16(%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	jne	.L1404
	movq	32(%rdi), %rax
	movq	(%rdi), %rdx
	movq	%rax, %rcx
	notq	%rcx
	addq	8(%rdi), %rcx
	cmpq	$7, %rcx
	jbe	.L1405
	movzbl	7(%rdx,%rax), %ecx
	movzbl	8(%rdx,%rax), %esi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	salq	$8, %rcx
	orq	%rcx, %rsi
	movzbl	6(%rdx,%rax), %ecx
	salq	$16, %rcx
	orq	%rsi, %rcx
	movzbl	5(%rdx,%rax), %esi
	salq	$24, %rsi
	orq	%rsi, %rcx
	movzbl	4(%rdx,%rax), %esi
	salq	$32, %rsi
	orq	%rcx, %rsi
	movzbl	3(%rdx,%rax), %ecx
	salq	$40, %rcx
	orq	%rcx, %rsi
	movzbl	2(%rdx,%rax), %ecx
	movzbl	1(%rdx,%rax), %eax
	salq	$48, %rcx
	salq	$56, %rax
	orq	%rsi, %rcx
	orq	%rcx, %rax
	movq	%rax, %xmm0
	ret
.L1404:
	.cfi_restore_state
	leaq	.LC92(%rip), %rcx
	movl	$682, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC93(%rip), %rdi
	call	__assert_fail@PLT
.L1405:
	call	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstImEET_NS_4spanIhEE.part.0
	.cfi_endproc
.LFE3210:
	.size	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer9GetDoubleEv, .-_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer9GetDoubleEv
	.section	.text._ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer10GetString8Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer10GetString8Ev
	.type	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer10GetString8Ev, @function
_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer10GetString8Ev:
.LFB3211:
	.cfi_startproc
	endbr64
	cmpl	$6, 16(%rdi)
	jne	.L1411
	movq	56(%rdi), %rdx
	movq	40(%rdi), %rax
	addq	32(%rdi), %rax
	subq	%rdx, %rax
	addq	(%rdi), %rax
	ret
.L1411:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer10GetString8Ev.part.0
	.cfi_endproc
.LFE3211:
	.size	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer10GetString8Ev, .-_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer10GetString8Ev
	.section	.text._ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer18GetString16WireRepEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer18GetString16WireRepEv
	.type	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer18GetString16WireRepEv, @function
_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer18GetString16WireRepEv:
.LFB3212:
	.cfi_startproc
	endbr64
	cmpl	$7, 16(%rdi)
	jne	.L1417
	movq	56(%rdi), %rdx
	movq	40(%rdi), %rax
	addq	32(%rdi), %rax
	subq	%rdx, %rax
	addq	(%rdi), %rax
	ret
.L1417:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer18GetString16WireRepEv.part.0
	.cfi_endproc
.LFE3212:
	.size	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer18GetString16WireRepEv, .-_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer18GetString16WireRepEv
	.section	.text._ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer9GetBinaryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer9GetBinaryEv
	.type	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer9GetBinaryEv, @function
_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer9GetBinaryEv:
.LFB3213:
	.cfi_startproc
	endbr64
	cmpl	$8, 16(%rdi)
	jne	.L1423
	movq	56(%rdi), %rdx
	movq	40(%rdi), %rax
	addq	32(%rdi), %rax
	subq	%rdx, %rax
	addq	(%rdi), %rax
	ret
.L1423:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer9GetBinaryEv.part.0
	.cfi_endproc
.LFE3213:
	.size	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer9GetBinaryEv, .-_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer9GetBinaryEv
	.section	.rodata._ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer19GetEnvelopeContentsEv.str1.8,"aMS",@progbits,1
	.align 8
.LC94:
	.string	"v8_inspector_protocol_encoding::span<unsigned char> v8_inspector_protocol_encoding::cbor::CBORTokenizer::GetEnvelopeContents() const"
	.section	.text._ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer19GetEnvelopeContentsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer19GetEnvelopeContentsEv
	.type	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer19GetEnvelopeContentsEv, @function
_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer19GetEnvelopeContentsEv:
.LFB3214:
	.cfi_startproc
	endbr64
	cmpl	$12, 16(%rdi)
	jne	.L1429
	movq	32(%rdi), %rax
	movq	(%rdi), %rcx
	movq	56(%rdi), %rdx
	leaq	6(%rcx,%rax), %rax
	ret
.L1429:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC94(%rip), %rcx
	movl	$711, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE3214:
	.size	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer19GetEnvelopeContentsEv, .-_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer19GetEnvelopeContentsEv
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb
	.type	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb, @function
_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb:
.LFB3216:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdx
	movq	%rdi, %r9
	testb	%sil, %sil
	je	.L1431
	leaq	6(%rdx), %rax
	movq	%rax, 32(%rdi)
.L1432:
	movq	8(%r9), %rsi
	movl	$0, 24(%r9)
	cmpq	%rax, %rsi
	jbe	.L1489
	movq	(%r9), %r11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	subq	%rax, %r10
	leaq	(%r11,%rax), %rdi
	movzbl	(%rdi), %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpb	$-43, %dl
	jbe	.L1490
	leal	42(%rdx), %ecx
	cmpb	$-42, %dl
	jb	.L1439
	leaq	.L1441(%rip), %r8
	movzbl	%cl, %ecx
	movslq	(%r8,%rcx,4), %rdx
	addq	%r8, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb,"a",@progbits
	.align 4
	.align 4
.L1441:
	.long	.L1447-.L1441
	.long	.L1439-.L1441
	.long	.L1446-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1445-.L1441
	.long	.L1444-.L1441
	.long	.L1443-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1442-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1439-.L1441
	.long	.L1440-.L1441
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb
	.p2align 4,,10
	.p2align 3
.L1490:
	cmpb	$-97, %dl
	je	.L1437
	cmpb	$-65, %dl
	jne	.L1439
	movl	$9, 16(%r9)
	movq	$1, 40(%r9)
.L1430:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1431:
	.cfi_restore 6
	xorl	%eax, %eax
	cmpq	$-1, %rdx
	je	.L1433
	movq	40(%rdi), %rax
	addq	%rdx, %rax
.L1433:
	movq	%rax, 32(%r9)
	jmp	.L1432
	.p2align 4,,10
	.p2align 3
.L1489:
	movl	$13, 16(%r9)
	ret
.L1440:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movl	$11, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	$1, 40(%r9)
	ret
.L1439:
	.cfi_restore_state
	leaq	56(%r9), %rcx
	leaq	48(%r9), %rdx
	movq	%r10, %rsi
	call	_ZN30v8_inspector_protocol_encoding4cbor9internals14ReadTokenStartENS_4spanIhEEPNS0_9MajorTypeEPm
	cmpl	$7, 48(%r9)
	ja	.L1430
	movl	48(%r9), %edx
	leaq	.L1455(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb
	.align 4
	.align 4
.L1455:
	.long	.L1459-.L1455
	.long	.L1459-.L1455
	.long	.L1457-.L1455
	.long	.L1456-.L1455
	.long	.L1454-.L1455
	.long	.L1454-.L1455
	.long	.L1454-.L1455
	.long	.L1454-.L1455
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb
.L1447:
	addq	$1, %rax
	leaq	56(%r9), %rcx
	leaq	48(%r9), %rdx
	subq	%rax, %rsi
	leaq	(%r11,%rax), %rdi
	call	_ZN30v8_inspector_protocol_encoding4cbor9internals14ReadTokenStartENS_4spanIhEEPNS0_9MajorTypeEPm
	testq	%rax, %rax
	je	.L1448
	cmpl	$2, 48(%r9)
	jne	.L1448
	movabsq	$4611686018427387903, %rcx
	movq	56(%r9), %rdx
	cmpq	%rcx, %rdx
	ja	.L1448
	leaq	1(%rdx,%rax), %rax
	cmpq	%rax, %r10
	jb	.L1448
	movl	$8, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%rax, 40(%r9)
	ret
.L1446:
	.cfi_restore_state
	cmpq	$5, %r10
	jbe	.L1452
	cmpb	$90, 1(%r11,%rax)
	jne	.L1452
	leaq	2(%rax), %rdx
	subq	%rdx, %rsi
	cmpq	$3, %rsi
	jbe	.L1491
	movzbl	4(%r11,%rax), %edx
	movzbl	5(%r11,%rax), %ecx
	sall	$8, %edx
	orl	%edx, %ecx
	movzbl	3(%r11,%rax), %edx
	movzbl	2(%r11,%rax), %eax
	sall	$16, %edx
	sall	$24, %eax
	orl	%ecx, %edx
	orl	%edx, %eax
	movq	%rax, 56(%r9)
	addq	$6, %rax
	cmpq	%rax, %r10
	jb	.L1452
	movl	$12, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%rax, 40(%r9)
	ret
.L1445:
	.cfi_restore_state
	movl	$2, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	$1, 40(%r9)
	ret
.L1443:
	.cfi_restore_state
	movl	$3, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	$1, 40(%r9)
	ret
.L1442:
	.cfi_restore_state
	cmpq	$8, %r10
	jbe	.L1492
	movl	$5, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	$9, 40(%r9)
	ret
.L1444:
	.cfi_restore_state
	movl	$1, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	$1, 40(%r9)
	ret
	.p2align 4,,10
	.p2align 3
.L1437:
	.cfi_restore_state
	movl	$10, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	$1, 40(%r9)
	ret
	.p2align 4,,10
	.p2align 3
.L1452:
	.cfi_restore_state
	movl	$0, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$16, 24(%r9)
	ret
	.p2align 4,,10
	.p2align 3
.L1448:
	.cfi_restore_state
	movl	$0, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$21, 24(%r9)
	ret
	.p2align 4,,10
	.p2align 3
.L1454:
	.cfi_restore_state
	movl	$0, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$22, 24(%r9)
	ret
	.p2align 4,,10
	.p2align 3
.L1492:
	.cfi_restore_state
	movl	$0, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$15, 24(%r9)
	ret
	.p2align 4,,10
	.p2align 3
.L1459:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L1460
	cmpq	$2147483647, 56(%r9)
	ja	.L1460
	movl	$4, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%rax, 40(%r9)
	ret
.L1457:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L1464
	movabsq	$4611686018427387903, %rcx
	movq	56(%r9), %rdx
	cmpq	%rcx, %rdx
	ja	.L1464
	testb	$1, %dl
	jne	.L1464
	addq	%rdx, %rax
	cmpq	%rax, %r10
	jb	.L1464
	movl	$7, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%rax, 40(%r9)
	ret
.L1456:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L1462
	movabsq	$4611686018427387903, %rcx
	movq	56(%r9), %rdx
	cmpq	%rcx, %rdx
	ja	.L1462
	addq	%rdx, %rax
	cmpq	%rax, %r10
	jb	.L1462
	movl	$6, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%rax, 40(%r9)
	ret
	.p2align 4,,10
	.p2align 3
.L1460:
	.cfi_restore_state
	movl	$0, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$14, 24(%r9)
	ret
.L1464:
	.cfi_restore_state
	movl	$0, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$20, 24(%r9)
	ret
.L1462:
	.cfi_restore_state
	movl	$0, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$19, 24(%r9)
	ret
.L1491:
	.cfi_restore_state
	call	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstIjEET_NS_4spanIhEE.part.0
	.cfi_endproc
.LFE3216:
	.size	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb, .-_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizerC2ENS_4spanIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizerC2ENS_4spanIhEE
	.type	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizerC2ENS_4spanIhEE, @function
_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizerC2ENS_4spanIhEE:
.LFB3200:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	xorl	%esi, %esi
	movq	%rdx, 8(%rdi)
	movl	$0, 24(%rdi)
	movq	$-1, 32(%rdi)
	jmp	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb
	.cfi_endproc
.LFE3200:
	.size	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizerC2ENS_4spanIhEE, .-_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizerC2ENS_4spanIhEE
	.globl	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizerC1ENS_4spanIhEE
	.set	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizerC1ENS_4spanIhEE,_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizerC2ENS_4spanIhEE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer4NextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer4NextEv
	.type	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer4NextEv, @function
_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer4NextEv:
.LFB3206:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	testl	%eax, %eax
	je	.L1494
	cmpl	$13, %eax
	je	.L1494
	xorl	%esi, %esi
	jmp	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb
	.p2align 4,,10
	.p2align 3
.L1494:
	ret
	.cfi_endproc
.LFE3206:
	.size	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer4NextEv, .-_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer4NextEv
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13EnterEnvelopeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13EnterEnvelopeEv
	.type	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13EnterEnvelopeEv, @function
_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13EnterEnvelopeEv:
.LFB3207:
	.cfi_startproc
	endbr64
	cmpl	$12, 16(%rdi)
	jne	.L1501
	movl	$1, %esi
	jmp	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb
.L1501:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13EnterEnvelopeEv.part.0
	.cfi_endproc
.LFE3207:
	.size	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13EnterEnvelopeEv, .-_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13EnterEnvelopeEv
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer8SetTokenENS0_12CBORTokenTagEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer8SetTokenENS0_12CBORTokenTagEm
	.type	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer8SetTokenENS0_12CBORTokenTagEm, @function
_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer8SetTokenENS0_12CBORTokenTagEm:
.LFB3217:
	.cfi_startproc
	endbr64
	movl	%esi, 16(%rdi)
	movq	%rdx, 40(%rdi)
	ret
	.cfi_endproc
.LFE3217:
	.size	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer8SetTokenENS0_12CBORTokenTagEm, .-_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer8SetTokenENS0_12CBORTokenTagEm
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer8SetErrorENS_5ErrorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer8SetErrorENS_5ErrorE
	.type	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer8SetErrorENS_5ErrorE, @function
_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer8SetErrorENS_5ErrorE:
.LFB3218:
	.cfi_startproc
	endbr64
	movl	$0, 16(%rdi)
	movl	%esi, 24(%rdi)
	ret
	.cfi_endproc
.LFE3218:
	.size	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer8SetErrorENS_5ErrorE, .-_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer8SetErrorENS_5ErrorE
	.section	.text._ZN30v8_inspector_protocol_encoding4json14NewJSONEncoderEPKNS0_8PlatformEPSt6vectorIhSaIhEEPNS_6StatusE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4json14NewJSONEncoderEPKNS0_8PlatformEPSt6vectorIhSaIhEEPNS_6StatusE
	.type	_ZN30v8_inspector_protocol_encoding4json14NewJSONEncoderEPKNS0_8PlatformEPSt6vectorIhSaIhEEPNS_6StatusE, @function
_ZN30v8_inspector_protocol_encoding4json14NewJSONEncoderEPKNS0_8PlatformEPSt6vectorIhSaIhEEPNS_6StatusE:
.LFB3265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$112, %edi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %rbx
	leaq	16+_ZTVN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEEE(%rip), %rax
	leaq	32(%rbx), %rdi
	movq	%rax, (%rbx)
	movq	%r15, 8(%rbx)
	movq	%r14, 16(%rbx)
	movq	%r13, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 96(%rbx)
	call	_ZNSt11_Deque_baseIN30v8_inspector_protocol_encoding4json12_GLOBAL__N_15StateESaIS3_EE17_M_initialize_mapEm.constprop.0
	movq	24(%rbx), %rax
	movl	$0, (%rax)
	movq	$-1, 8(%rax)
	movq	96(%rbx), %rcx
	movq	80(%rbx), %rax
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L1505
	movq	$0, (%rax)
	addq	$8, 80(%rbx)
.L1506:
	movq	%rbx, (%r12)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1505:
	.cfi_restore_state
	movq	104(%rbx), %r14
	movq	72(%rbx), %rsi
	movabsq	$1152921504606846975, %rdi
	subq	88(%rbx), %rax
	movq	%r14, %r13
	sarq	$3, %rax
	subq	%rsi, %r13
	movq	%r13, %rcx
	sarq	$3, %rcx
	leaq	-1(%rcx), %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movq	64(%rbx), %rax
	subq	48(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rdi, %rax
	je	.L1515
	movq	32(%rbx), %r8
	movq	40(%rbx), %rdx
	movq	%r14, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L1516
.L1508:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r14)
	movq	80(%rbx), %rax
	movq	$0, (%rax)
	movq	104(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 104(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 88(%rbx)
	movq	%rdx, 96(%rbx)
	movq	%rax, 80(%rbx)
	jmp	.L1506
	.p2align 4,,10
	.p2align 3
.L1516:
	leaq	2(%rcx), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L1517
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	cmpq	%rdi, %r14
	ja	.L1518
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	%rax, %rsi
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rsi,%rax,8), %r15
	movq	104(%rbx), %rax
	movq	72(%rbx), %rsi
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L1513
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L1513:
	movq	32(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 40(%rbx)
	movq	%rax, 32(%rbx)
.L1511:
	movq	%r15, 72(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r13), %r14
	movq	(%r15), %xmm0
	movq	%r14, 104(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 56(%rbx)
	movq	(%r14), %rax
	movq	%rax, 88(%rbx)
	addq	$512, %rax
	movq	%rax, 96(%rbx)
	jmp	.L1508
	.p2align 4,,10
	.p2align 3
.L1517:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L1510
	cmpq	%r14, %rsi
	je	.L1511
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L1511
	.p2align 4,,10
	.p2align 3
.L1510:
	cmpq	%r14, %rsi
	je	.L1511
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L1511
.L1515:
	leaq	.LC22(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1518:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE3265:
	.size	_ZN30v8_inspector_protocol_encoding4json14NewJSONEncoderEPKNS0_8PlatformEPSt6vectorIhSaIhEEPNS_6StatusE, .-_ZN30v8_inspector_protocol_encoding4json14NewJSONEncoderEPKNS0_8PlatformEPSt6vectorIhSaIhEEPNS_6StatusE
	.section	.text._ZN30v8_inspector_protocol_encoding4json14NewJSONEncoderEPKNS0_8PlatformEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS_6StatusE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4json14NewJSONEncoderEPKNS0_8PlatformEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS_6StatusE
	.type	_ZN30v8_inspector_protocol_encoding4json14NewJSONEncoderEPKNS0_8PlatformEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS_6StatusE, @function
_ZN30v8_inspector_protocol_encoding4json14NewJSONEncoderEPKNS0_8PlatformEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS_6StatusE:
.LFB3266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$112, %edi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %rbx
	leaq	16+_ZTVN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	leaq	32(%rbx), %rdi
	movq	%rax, (%rbx)
	movq	%r15, 8(%rbx)
	movq	%r14, 16(%rbx)
	movq	%r13, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 96(%rbx)
	call	_ZNSt11_Deque_baseIN30v8_inspector_protocol_encoding4json12_GLOBAL__N_15StateESaIS3_EE17_M_initialize_mapEm.constprop.0
	movq	24(%rbx), %rax
	movl	$0, (%rax)
	movq	$-1, 8(%rax)
	movq	96(%rbx), %rcx
	movq	80(%rbx), %rax
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L1520
	movq	$0, (%rax)
	addq	$8, 80(%rbx)
.L1521:
	movq	%rbx, (%r12)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1520:
	.cfi_restore_state
	movq	104(%rbx), %r14
	movq	72(%rbx), %rsi
	movabsq	$1152921504606846975, %rdi
	subq	88(%rbx), %rax
	movq	%r14, %r13
	sarq	$3, %rax
	subq	%rsi, %r13
	movq	%r13, %rcx
	sarq	$3, %rcx
	leaq	-1(%rcx), %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movq	64(%rbx), %rax
	subq	48(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rdi, %rax
	je	.L1530
	movq	32(%rbx), %r8
	movq	40(%rbx), %rdx
	movq	%r14, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L1531
.L1523:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r14)
	movq	80(%rbx), %rax
	movq	$0, (%rax)
	movq	104(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 104(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 88(%rbx)
	movq	%rdx, 96(%rbx)
	movq	%rax, 80(%rbx)
	jmp	.L1521
	.p2align 4,,10
	.p2align 3
.L1531:
	leaq	2(%rcx), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L1532
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	cmpq	%rdi, %r14
	ja	.L1533
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	%rax, %rsi
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rsi,%rax,8), %r15
	movq	104(%rbx), %rax
	movq	72(%rbx), %rsi
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L1528
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L1528:
	movq	32(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 40(%rbx)
	movq	%rax, 32(%rbx)
.L1526:
	movq	%r15, 72(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r13), %r14
	movq	(%r15), %xmm0
	movq	%r14, 104(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 56(%rbx)
	movq	(%r14), %rax
	movq	%rax, 88(%rbx)
	addq	$512, %rax
	movq	%rax, 96(%rbx)
	jmp	.L1523
	.p2align 4,,10
	.p2align 3
.L1532:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L1525
	cmpq	%r14, %rsi
	je	.L1526
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L1526
	.p2align 4,,10
	.p2align 3
.L1525:
	cmpq	%r14, %rsi
	je	.L1526
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L1526
.L1530:
	leaq	.LC22(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1533:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE3266:
	.size	_ZN30v8_inspector_protocol_encoding4json14NewJSONEncoderEPKNS0_8PlatformEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS_6StatusE, .-_ZN30v8_inspector_protocol_encoding4json14NewJSONEncoderEPKNS0_8PlatformEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS_6StatusE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS0_9MajorTypeEmPT_,"axG",@progbits,_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS0_9MajorTypeEmPT_,comdat
	.p2align 4
	.weak	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS0_9MajorTypeEmPT_
	.type	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS0_9MajorTypeEmPT_, @function
_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS0_9MajorTypeEmPT_:
.LFB3548:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	sall	$5, %edi
	andl	$8160, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	leaq	16(%rbx), %r14
	subq	$24, %rsp
	movq	8(%rdx), %rax
	movq	(%rdx), %rdx
	leaq	1(%rax), %r15
	cmpq	$23, %rsi
	jbe	.L1616
	cmpq	$255, %rsi
	jbe	.L1617
	cmpq	$65535, %rsi
	jbe	.L1618
	movl	$4294967295, %ecx
	movl	%edi, %r13d
	cmpq	%rcx, %rsi
	jbe	.L1619
	orl	$27, %r13d
	cmpq	%r14, %rdx
	je	.L1592
	movq	16(%rbx), %rcx
.L1563:
	cmpq	%rcx, %r15
	ja	.L1620
.L1564:
	movb	%r13b, (%rdx,%rax)
	movq	(%rbx), %rdx
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$56, %r9
	movb	$0, 1(%rdx,%rax)
	movq	8(%rbx), %r13
	movq	(%rbx), %rax
	leaq	1(%r13), %r15
	cmpq	%rax, %r14
	je	.L1593
	movq	16(%rbx), %rdx
.L1565:
	cmpq	%rdx, %r15
	ja	.L1621
.L1566:
	movb	%r9b, (%rax,%r13)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$48, %r9
	movb	$0, 1(%rax,%r13)
	movq	8(%rbx), %r13
	movq	(%rbx), %rax
	leaq	1(%r13), %r15
	cmpq	%rax, %r14
	je	.L1594
	movq	16(%rbx), %rdx
.L1567:
	cmpq	%rdx, %r15
	ja	.L1622
.L1568:
	movb	%r9b, (%rax,%r13)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$40, %r9
	movb	$0, 1(%rax,%r13)
	movq	8(%rbx), %r13
	movq	(%rbx), %rax
	leaq	1(%r13), %r15
	cmpq	%rax, %r14
	je	.L1595
	movq	16(%rbx), %rdx
.L1569:
	cmpq	%rdx, %r15
	ja	.L1623
.L1570:
	movb	%r9b, (%rax,%r13)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$32, %r9
	movb	$0, 1(%rax,%r13)
	movq	8(%rbx), %r13
	movq	(%rbx), %rax
	leaq	1(%r13), %r15
	cmpq	%rax, %r14
	je	.L1596
	movq	16(%rbx), %rdx
.L1571:
	cmpq	%rdx, %r15
	ja	.L1624
.L1572:
	movb	%r9b, (%rax,%r13)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$24, %r9
	movb	$0, 1(%rax,%r13)
	movq	8(%rbx), %r13
	movq	(%rbx), %rax
	leaq	1(%r13), %r15
	cmpq	%rax, %r14
	je	.L1597
	movq	16(%rbx), %rdx
.L1573:
	cmpq	%rdx, %r15
	ja	.L1625
.L1574:
	movb	%r9b, (%rax,%r13)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$16, %r9
	movb	$0, 1(%rax,%r13)
	movq	8(%rbx), %r13
	movq	(%rbx), %rax
	leaq	1(%r13), %r15
	cmpq	%rax, %r14
	je	.L1598
	movq	16(%rbx), %rdx
.L1575:
	cmpq	%rdx, %r15
	ja	.L1626
.L1576:
	movb	%r9b, (%rax,%r13)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$8, %r9
	movb	$0, 1(%rax,%r13)
	movq	8(%rbx), %r13
	movq	(%rbx), %rax
	leaq	1(%r13), %r15
	cmpq	%rax, %r14
	je	.L1599
	movq	16(%rbx), %rdx
.L1577:
	cmpq	%rdx, %r15
	ja	.L1627
.L1578:
	movb	%r9b, (%rax,%r13)
	movq	(%rbx), %rax
	movq	%r15, 8(%rbx)
	movb	$0, 1(%rax,%r13)
	movq	8(%rbx), %r13
	movq	(%rbx), %rax
	leaq	1(%r13), %r15
	cmpq	%r14, %rax
	je	.L1600
.L1634:
	movq	16(%rbx), %rdx
.L1579:
	cmpq	%rdx, %r15
	ja	.L1628
.L1580:
	movb	%r12b, (%rax,%r13)
	movq	(%rbx), %rax
	movq	%r15, 8(%rbx)
	movb	$0, 1(%rax,%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1628:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1616:
	orl	%edi, %r12d
	cmpq	%r14, %rdx
	je	.L1581
	movq	16(%rbx), %rcx
.L1536:
	cmpq	%rcx, %r15
	ja	.L1629
.L1537:
	movb	%r12b, (%rdx,%rax)
	movq	(%rbx), %rdx
	movq	%r15, 8(%rbx)
	movb	$0, 1(%rdx,%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1619:
	.cfi_restore_state
	orl	$26, %r13d
	cmpq	%r14, %rdx
	je	.L1587
	movq	16(%rbx), %rcx
.L1552:
	cmpq	%rcx, %r15
	ja	.L1630
.L1553:
	movb	%r13b, (%rdx,%rax)
	movq	(%rbx), %rdx
	movl	%r12d, %r9d
	movq	%r15, 8(%rbx)
	shrl	$24, %r9d
	movb	$0, 1(%rdx,%rax)
	movq	8(%rbx), %r13
	movq	(%rbx), %rax
	leaq	1(%r13), %r15
	cmpq	%r14, %rax
	je	.L1588
	movq	16(%rbx), %rdx
.L1554:
	cmpq	%rdx, %r15
	ja	.L1631
.L1555:
	movb	%r9b, (%rax,%r13)
	movq	(%rbx), %rax
	movl	%r12d, %r9d
	movq	%r15, 8(%rbx)
	shrl	$16, %r9d
	movb	$0, 1(%rax,%r13)
	movq	8(%rbx), %r13
	movq	(%rbx), %rax
	leaq	1(%r13), %r15
	cmpq	%r14, %rax
	je	.L1589
	movq	16(%rbx), %rdx
.L1556:
	cmpq	%r15, %rdx
	jb	.L1632
.L1557:
	movb	%r9b, (%rax,%r13)
	movq	(%rbx), %rax
	movl	%r12d, %r9d
	movq	%r15, 8(%rbx)
	shrl	$8, %r9d
	movb	$0, 1(%rax,%r13)
.L1615:
	movq	8(%rbx), %r13
	movq	(%rbx), %rax
	leaq	1(%r13), %r15
	cmpq	%r14, %rax
	je	.L1590
	movq	16(%rbx), %rdx
.L1558:
	cmpq	%r15, %rdx
	jnb	.L1578
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	%r9d, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movl	-56(%rbp), %r9d
	jmp	.L1578
	.p2align 4,,10
	.p2align 3
.L1617:
	movl	%edi, %r13d
	orl	$24, %r13d
	cmpq	%r14, %rdx
	je	.L1582
	movq	16(%rbx), %rcx
.L1540:
	cmpq	%rcx, %r15
	ja	.L1633
.L1541:
	movb	%r13b, (%rdx,%rax)
	movq	(%rbx), %rdx
	movq	%r15, 8(%rbx)
	movb	$0, 1(%rdx,%rax)
	movq	8(%rbx), %r13
	movq	(%rbx), %rax
	leaq	1(%r13), %r15
	cmpq	%r14, %rax
	jne	.L1634
.L1600:
	movl	$15, %edx
	jmp	.L1579
	.p2align 4,,10
	.p2align 3
.L1618:
	movl	%edi, %r13d
	orl	$25, %r13d
	cmpq	%r14, %rdx
	je	.L1584
	movq	16(%rbx), %rcx
.L1545:
	cmpq	%rcx, %r15
	ja	.L1635
.L1546:
	movb	%r13b, (%rdx,%rax)
	movq	(%rbx), %rdx
	movl	%r12d, %r9d
	movq	%r15, 8(%rbx)
	sarl	$8, %r9d
	movb	$0, 1(%rdx,%rax)
	jmp	.L1615
	.p2align 4,,10
	.p2align 3
.L1627:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L1578
	.p2align 4,,10
	.p2align 3
.L1626:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L1576
	.p2align 4,,10
	.p2align 3
.L1625:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L1574
	.p2align 4,,10
	.p2align 3
.L1624:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L1572
	.p2align 4,,10
	.p2align 3
.L1623:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L1570
	.p2align 4,,10
	.p2align 3
.L1622:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L1568
	.p2align 4,,10
	.p2align 3
.L1621:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L1566
	.p2align 4,,10
	.p2align 3
.L1620:
	xorl	%edx, %edx
	movq	%rax, %rsi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rdx
	movq	-56(%rbp), %rax
	jmp	.L1564
	.p2align 4,,10
	.p2align 3
.L1629:
	xorl	%edx, %edx
	movq	%rax, %rsi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rdx
	movq	-56(%rbp), %rax
	jmp	.L1537
	.p2align 4,,10
	.p2align 3
.L1593:
	movl	$15, %edx
	jmp	.L1565
	.p2align 4,,10
	.p2align 3
.L1592:
	movl	$15, %ecx
	jmp	.L1563
	.p2align 4,,10
	.p2align 3
.L1599:
	movl	$15, %edx
	jmp	.L1577
	.p2align 4,,10
	.p2align 3
.L1598:
	movl	$15, %edx
	jmp	.L1575
	.p2align 4,,10
	.p2align 3
.L1595:
	movl	$15, %edx
	jmp	.L1569
	.p2align 4,,10
	.p2align 3
.L1594:
	movl	$15, %edx
	jmp	.L1567
	.p2align 4,,10
	.p2align 3
.L1597:
	movl	$15, %edx
	jmp	.L1573
	.p2align 4,,10
	.p2align 3
.L1596:
	movl	$15, %edx
	jmp	.L1571
	.p2align 4,,10
	.p2align 3
.L1632:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	%r9d, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movl	-56(%rbp), %r9d
	jmp	.L1557
	.p2align 4,,10
	.p2align 3
.L1631:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	%r9d, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movl	-56(%rbp), %r9d
	jmp	.L1555
	.p2align 4,,10
	.p2align 3
.L1630:
	xorl	%edx, %edx
	movq	%rax, %rsi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rdx
	movq	-56(%rbp), %rax
	jmp	.L1553
	.p2align 4,,10
	.p2align 3
.L1590:
	movl	$15, %edx
	jmp	.L1558
	.p2align 4,,10
	.p2align 3
.L1633:
	xorl	%edx, %edx
	movq	%rax, %rsi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rdx
	movq	-56(%rbp), %rax
	jmp	.L1541
	.p2align 4,,10
	.p2align 3
.L1581:
	movl	$15, %ecx
	jmp	.L1536
	.p2align 4,,10
	.p2align 3
.L1635:
	xorl	%edx, %edx
	movq	%rax, %rsi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rdx
	movq	-56(%rbp), %rax
	jmp	.L1546
	.p2align 4,,10
	.p2align 3
.L1587:
	movl	$15, %ecx
	jmp	.L1552
	.p2align 4,,10
	.p2align 3
.L1589:
	movl	$15, %edx
	jmp	.L1556
	.p2align 4,,10
	.p2align 3
.L1588:
	movl	$15, %edx
	jmp	.L1554
	.p2align 4,,10
	.p2align 3
.L1582:
	movl	$15, %ecx
	jmp	.L1540
.L1584:
	movl	$15, %ecx
	jmp	.L1545
	.cfi_endproc
.LFE3548:
	.size	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS0_9MajorTypeEmPT_, .-_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS0_9MajorTypeEmPT_
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor9internals15WriteTokenStartENS0_9MajorTypeEmPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor9internals15WriteTokenStartENS0_9MajorTypeEmPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN30v8_inspector_protocol_encoding4cbor9internals15WriteTokenStartENS0_9MajorTypeEmPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN30v8_inspector_protocol_encoding4cbor9internals15WriteTokenStartENS0_9MajorTypeEmPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB3142:
	.cfi_startproc
	endbr64
	jmp	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS0_9MajorTypeEmPT_
	.cfi_endproc
.LFE3142:
	.size	_ZN30v8_inspector_protocol_encoding4cbor9internals15WriteTokenStartENS0_9MajorTypeEmPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN30v8_inspector_protocol_encoding4cbor9internals15WriteTokenStartENS0_9MajorTypeEmPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleInt32Ei,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleInt32Ei, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleInt32Ei:
.LFB4474:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L1640
	ret
	.p2align 4,,10
	.p2align 3
.L1640:
	movq	8(%rdi), %rdx
	testl	%esi, %esi
	js	.L1639
	movslq	%esi, %rsi
	xorl	%edi, %edi
	jmp	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS0_9MajorTypeEmPT_
	.p2align 4,,10
	.p2align 3
.L1639:
	notl	%esi
	movl	$1, %edi
	movslq	%esi, %rsi
	jmp	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS0_9MajorTypeEmPT_
	.cfi_endproc
.LFE4474:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleInt32Ei, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleInt32Ei
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor11EncodeInt32EiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor11EncodeInt32EiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN30v8_inspector_protocol_encoding4cbor11EncodeInt32EiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN30v8_inspector_protocol_encoding4cbor11EncodeInt32EiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB3154:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	testl	%edi, %edi
	js	.L1642
	movslq	%edi, %rsi
	xorl	%edi, %edi
	jmp	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS0_9MajorTypeEmPT_
	.p2align 4,,10
	.p2align 3
.L1642:
	notl	%edi
	movslq	%edi, %rsi
	movl	$1, %edi
	jmp	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS0_9MajorTypeEmPT_
	.cfi_endproc
.LFE3154:
	.size	_ZN30v8_inspector_protocol_encoding4cbor11EncodeInt32EiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN30v8_inspector_protocol_encoding4cbor11EncodeInt32EiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor14EncodeString16ENS_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor14EncodeString16ENS_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN30v8_inspector_protocol_encoding4cbor14EncodeString16ENS_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN30v8_inspector_protocol_encoding4cbor14EncodeString16ENS_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB3157:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	(%rsi,%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rsi
	movl	$2, %edi
	pushq	%rbx
	addq	%r12, %r13
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	leaq	16(%rbx), %r15
	subq	$24, %rsp
	call	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS0_9MajorTypeEmPT_
	cmpq	%r12, %r13
	jne	.L1651
	jmp	.L1643
	.p2align 4,,10
	.p2align 3
.L1646:
	movb	%al, (%rdx,%r14)
	movq	(%rbx), %rdx
	movzbl	%ah, %eax
	movq	%r9, 8(%rbx)
	movb	$0, 1(%rdx,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rdx
	leaq	1(%r14), %r9
	cmpq	%r15, %rdx
	je	.L1653
	movq	16(%rbx), %rcx
.L1647:
	cmpq	%rcx, %r9
	ja	.L1658
	movb	%al, (%rdx,%r14)
.L1657:
	movq	(%rbx), %rax
	addq	$2, %r12
	movq	%r9, 8(%rbx)
	movb	$0, 1(%rax,%r14)
	cmpq	%r12, %r13
	je	.L1643
.L1651:
	movq	8(%rbx), %r14
	movq	(%rbx), %rdx
	movzwl	(%r12), %eax
	leaq	1(%r14), %r9
	cmpq	%r15, %rdx
	je	.L1652
	movq	16(%rbx), %rcx
.L1645:
	cmpq	%rcx, %r9
	jbe	.L1646
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -64(%rbp)
	movl	%eax, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rdx
	movq	-64(%rbp), %r9
	movl	-56(%rbp), %eax
	jmp	.L1646
	.p2align 4,,10
	.p2align 3
.L1652:
	movl	$15, %ecx
	jmp	.L1645
	.p2align 4,,10
	.p2align 3
.L1643:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1658:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movl	%eax, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rdx
	movl	-64(%rbp), %eax
	movq	-56(%rbp), %r9
	movb	%al, (%rdx,%r14)
	jmp	.L1657
	.p2align 4,,10
	.p2align 3
.L1653:
	movl	$15, %ecx
	jmp	.L1647
	.cfi_endproc
.LFE3157:
	.size	_ZN30v8_inspector_protocol_encoding4cbor14EncodeString16ENS_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN30v8_inspector_protocol_encoding4cbor14EncodeString16ENS_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor17EncodeString8TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_,"axG",@progbits,_ZN30v8_inspector_protocol_encoding4cbor17EncodeString8TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_,comdat
	.p2align 4
	.weak	_ZN30v8_inspector_protocol_encoding4cbor17EncodeString8TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_
	.type	_ZN30v8_inspector_protocol_encoding4cbor17EncodeString8TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_, @function
_ZN30v8_inspector_protocol_encoding4cbor17EncodeString8TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_:
.LFB3554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$3, %edi
	movq	%rbx, %r14
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS0_9MajorTypeEmPT_
	movq	8(%r13), %rcx
	movq	%r15, -96(%rbp)
	addq	0(%r13), %rcx
	addq	%r12, %r14
	je	.L1660
	testq	%rbx, %rbx
	je	.L1685
.L1660:
	movq	%r12, -104(%rbp)
	movq	%r12, %rdx
	movq	%r15, %rax
	cmpq	$15, %r12
	ja	.L1686
.L1661:
	cmpq	%rbx, %r14
	je	.L1662
	leaq	15(%rbx), %rdx
	subq	%rax, %rdx
	cmpq	$30, %rdx
	jbe	.L1670
	leaq	-1(%r12), %rdx
	cmpq	$14, %rdx
	jbe	.L1670
	movq	%r12, %rsi
	xorl	%edx, %edx
	andq	$-16, %rsi
	.p2align 4,,10
	.p2align 3
.L1664:
	movdqu	(%rbx,%rdx), %xmm0
	movups	%xmm0, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L1664
	movq	%r12, %rdx
	andq	$-16, %rdx
	leaq	(%rbx,%rdx), %rdi
	addq	%rdx, %rax
	cmpq	%rdx, %r12
	je	.L1666
	movzbl	(%rdi), %edx
	movb	%dl, (%rax)
	leaq	1(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1666
	movzbl	1(%rdi), %edx
	movb	%dl, 1(%rax)
	leaq	2(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1666
	movzbl	2(%rdi), %edx
	movb	%dl, 2(%rax)
	leaq	3(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1666
	movzbl	3(%rdi), %edx
	movb	%dl, 3(%rax)
	leaq	4(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1666
	movzbl	4(%rdi), %edx
	movb	%dl, 4(%rax)
	leaq	5(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1666
	movzbl	5(%rdi), %edx
	movb	%dl, 5(%rax)
	leaq	6(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1666
	movzbl	6(%rdi), %edx
	movb	%dl, 6(%rax)
	leaq	7(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1666
	movzbl	7(%rdi), %edx
	movb	%dl, 7(%rax)
	leaq	8(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1666
	movzbl	8(%rdi), %edx
	movb	%dl, 8(%rax)
	leaq	9(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1666
	movzbl	9(%rdi), %edx
	movb	%dl, 9(%rax)
	leaq	10(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1666
	movzbl	10(%rdi), %edx
	movb	%dl, 10(%rax)
	leaq	11(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1666
	movzbl	11(%rdi), %edx
	movb	%dl, 11(%rax)
	leaq	12(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1666
	movzbl	12(%rdi), %edx
	movb	%dl, 12(%rax)
	leaq	13(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1666
	movzbl	13(%rdi), %edx
	movb	%dl, 13(%rax)
	leaq	14(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1666
	movzbl	14(%rdi), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L1666:
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rax
.L1662:
	movq	%rdx, -88(%rbp)
	movq	%r13, %rdi
	movb	$0, (%rax,%rdx)
	subq	0(%r13), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rsi
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1659
	call	_ZdlPv@PLT
.L1659:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1687
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1686:
	.cfi_restore_state
	xorl	%edx, %edx
	leaq	-104(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	movq	%rcx, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	movq	-120(%rbp), %rcx
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	jmp	.L1661
	.p2align 4,,10
	.p2align 3
.L1670:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1663:
	movzbl	(%rbx,%rdx), %esi
	movb	%sil, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %r12
	jne	.L1663
	jmp	.L1666
.L1685:
	leaq	.LC52(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1687:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3554:
	.size	_ZN30v8_inspector_protocol_encoding4cbor17EncodeString8TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_, .-_ZN30v8_inspector_protocol_encoding4cbor17EncodeString8TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor13EncodeString8ENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor13EncodeString8ENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN30v8_inspector_protocol_encoding4cbor13EncodeString8ENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN30v8_inspector_protocol_encoding4cbor13EncodeString8ENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB3160:
	.cfi_startproc
	endbr64
	jmp	_ZN30v8_inspector_protocol_encoding4cbor17EncodeString8TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_
	.cfi_endproc
.LFE3160:
	.size	_ZN30v8_inspector_protocol_encoding4cbor13EncodeString8ENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN30v8_inspector_protocol_encoding4cbor13EncodeString8ENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13HandleString8ENS_4spanIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13HandleString8ENS_4spanIhEE, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13HandleString8ENS_4spanIhEE:
.LFB4470:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movq	%rsi, %r8
	movq	%rdx, %rsi
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L1691
	ret
	.p2align 4,,10
	.p2align 3
.L1691:
	movq	8(%rdi), %rdx
	movq	%r8, %rdi
	jmp	_ZN30v8_inspector_protocol_encoding4cbor17EncodeString8TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_
	.cfi_endproc
.LFE4470:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13HandleString8ENS_4spanIhEE, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13HandleString8ENS_4spanIhEE
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor27AppendString8EntryToCBORMapENS_4spanIhEES2_PNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC95:
	.string	"basic_string::at: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor27AppendString8EntryToCBORMapENS_4spanIhEES2_PNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor27AppendString8EntryToCBORMapENS_4spanIhEES2_PNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN30v8_inspector_protocol_encoding4cbor27AppendString8EntryToCBORMapENS_4spanIhEES2_PNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN30v8_inspector_protocol_encoding4cbor27AppendString8EntryToCBORMapENS_4spanIhEES2_PNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB3237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	leaq	-128(%rbp), %rdi
	.cfi_offset 14, -32
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$120, %rsp
	movq	%rdx, -136(%rbp)
	movq	8(%r8), %r12
	movq	%rcx, -144(%rbp)
	movq	(%r8), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r13, -128(%rbp)
	movq	%r12, -120(%rbp)
	movl	$0, -104(%rbp)
	movq	$-1, -96(%rbp)
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb
	movl	-112(%rbp), %edx
	testl	%edx, %edx
	jne	.L1693
	movl	-104(%rbp), %eax
	movq	-96(%rbp), %r8
.L1694:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1704
	addq	$120, %rsp
	movq	%r8, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1693:
	.cfi_restore_state
	xorl	%r8d, %r8d
	movl	$16, %eax
	cmpl	$12, %edx
	jne	.L1694
	movq	-72(%rbp), %r10
	movq	8(%rbx), %r11
	leaq	6(%r10), %rdx
	cmpq	%rdx, %r11
	jne	.L1694
	movl	$6, %r8d
	movl	$31, %eax
	testq	%r10, %r10
	je	.L1694
	movq	-128(%rbp), %rsi
	movq	-96(%rbp), %rdx
	cmpb	$-65, 6(%rsi,%rdx)
	jne	.L1694
	cmpb	$-1, -1(%r13,%r12)
	leaq	5(%r10), %r8
	movl	$32, %eax
	jne	.L1694
	movq	%r8, %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r11, -160(%rbp)
	movq	%r10, -152(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE8_M_eraseEmm@PLT
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZN30v8_inspector_protocol_encoding4cbor17EncodeString8TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_
	movq	-136(%rbp), %rdi
	movq	-144(%rbp), %rsi
	movq	%rbx, %rdx
	call	_ZN30v8_inspector_protocol_encoding4cbor17EncodeString8TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_
	movl	$-1, %esi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9push_backEc@PLT
	movq	8(%rbx), %rdx
	movl	$4294967295, %ecx
	movq	-152(%rbp), %r10
	movq	-160(%rbp), %r11
	addq	%rdx, %r10
	movq	%r10, %rax
	subq	%r11, %rax
	cmpq	%rcx, %rax
	ja	.L1702
	leaq	-4(%r11,%rdx), %rsi
	subq	%r10, %rsi
	cmpq	%rsi, %rdx
	jbe	.L1705
	movq	(%rbx), %rdx
	bswap	%eax
	orq	$-1, %r8
	movl	%eax, (%rdx,%rsi)
	xorl	%eax, %eax
	jmp	.L1694
.L1702:
	xorl	%r8d, %r8d
	movl	$33, %eax
	jmp	.L1694
.L1704:
	call	__stack_chk_fail@PLT
.L1705:
	leaq	.LC95(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE3237:
	.size	_ZN30v8_inspector_protocol_encoding4cbor27AppendString8EntryToCBORMapENS_4spanIhEES2_PNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN30v8_inspector_protocol_encoding4cbor27AppendString8EntryToCBORMapENS_4spanIhEES2_PNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor19EncodeFromUTF16TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanItEEPT_,"axG",@progbits,_ZN30v8_inspector_protocol_encoding4cbor19EncodeFromUTF16TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanItEEPT_,comdat
	.p2align 4
	.weak	_ZN30v8_inspector_protocol_encoding4cbor19EncodeFromUTF16TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanItEEPT_
	.type	_ZN30v8_inspector_protocol_encoding4cbor19EncodeFromUTF16TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanItEEPT_, @function
_ZN30v8_inspector_protocol_encoding4cbor19EncodeFromUTF16TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanItEEPT_:
.LFB3558:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	(%rsi,%rsi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	(%rdi,%r15), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r14, %rdi
	je	.L1707
	movq	%rdi, %rbx
	movq	%rdi, %rax
.L1709:
	cmpw	$127, (%rax)
	ja	.L1708
	addq	$2, %rax
	cmpq	%rax, %r14
	jne	.L1709
.L1707:
	movq	%r12, %rdx
	movl	$3, %edi
	movq	%rcx, -120(%rbp)
	leaq	-80(%rbp), %rbx
	call	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS0_9MajorTypeEmPT_
	movq	8(%r12), %r8
	addq	(%r12), %r8
	testq	%r14, %r14
	movq	%rbx, -96(%rbp)
	movq	-120(%rbp), %rcx
	je	.L1718
	testq	%r13, %r13
	je	.L1745
.L1718:
	sarq	%r15
	movq	%rbx, %rax
	movq	%r15, -104(%rbp)
	cmpq	$15, %r15
	ja	.L1746
.L1719:
	cmpq	%r14, %r13
	je	.L1720
	movq	%r14, %rdx
	subq	%r13, %rdx
	subq	$2, %rdx
	movq	%rdx, %rsi
	leaq	2(%rdx,%r13), %rdi
	shrq	%rsi
	addq	$1, %rsi
	cmpq	%rdi, %rax
	leaq	(%rax,%rsi), %rdi
	setnb	%r9b
	cmpq	%rdi, %r13
	setnb	%dil
	orb	%dil, %r9b
	je	.L1737
	cmpq	$28, %rdx
	jbe	.L1737
	movq	%rsi, %rcx
	movdqa	.LC96(%rip), %xmm2
	xorl	%edx, %edx
	andq	$-16, %rcx
	.p2align 4,,10
	.p2align 3
.L1722:
	movdqu	0(%r13,%rdx,2), %xmm0
	movdqu	16(%r13,%rdx,2), %xmm1
	pand	%xmm2, %xmm0
	pand	%xmm2, %xmm1
	packuswb	%xmm1, %xmm0
	movups	%xmm0, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L1722
	movq	%rsi, %rcx
	andq	$-16, %rcx
	leaq	0(%r13,%rcx,2), %rdx
	addq	%rcx, %rax
	cmpq	%rcx, %rsi
	je	.L1724
	movzwl	(%rdx), %ecx
	movb	%cl, (%rax)
	leaq	2(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L1724
	movzwl	2(%rdx), %ecx
	movb	%cl, 1(%rax)
	leaq	4(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L1724
	movzwl	4(%rdx), %ecx
	movb	%cl, 2(%rax)
	leaq	6(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L1724
	movzwl	6(%rdx), %ecx
	movb	%cl, 3(%rax)
	leaq	8(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L1724
	movzwl	8(%rdx), %ecx
	movb	%cl, 4(%rax)
	leaq	10(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L1724
	movzwl	10(%rdx), %ecx
	movb	%cl, 5(%rax)
	leaq	12(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L1724
	movzwl	12(%rdx), %ecx
	movb	%cl, 6(%rax)
	leaq	14(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L1724
	movzwl	14(%rdx), %ecx
	movb	%cl, 7(%rax)
	leaq	16(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L1724
	movzwl	16(%rdx), %ecx
	movb	%cl, 8(%rax)
	leaq	18(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L1724
	movzwl	18(%rdx), %ecx
	movb	%cl, 9(%rax)
	leaq	20(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L1724
	movzwl	20(%rdx), %ecx
	movb	%cl, 10(%rax)
	leaq	22(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L1724
	movzwl	22(%rdx), %ecx
	movb	%cl, 11(%rax)
	leaq	24(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L1724
	movzwl	24(%rdx), %ecx
	movb	%cl, 12(%rax)
	leaq	26(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L1724
	movzwl	26(%rdx), %ecx
	movb	%cl, 13(%rax)
	leaq	28(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L1724
	movzwl	28(%rdx), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L1724:
	movq	-104(%rbp), %r15
	movq	-96(%rbp), %rax
.L1720:
	movq	%r15, -88(%rbp)
	movq	%r8, %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	movb	$0, (%rax,%r15)
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %rcx
	subq	(%r12), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1706
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1706:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1747
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1708:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r12, %rdx
	leaq	16(%r12), %r15
	movl	$2, %edi
	call	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS0_9MajorTypeEmPT_
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1711:
	movb	%al, (%rdx,%r13)
	movq	(%r12), %rdx
	movzbl	%ah, %eax
	movq	%r9, 8(%r12)
	movb	$0, 1(%rdx,%r13)
	movq	8(%r12), %r13
	movq	(%r12), %rdx
	leaq	1(%r13), %r9
	cmpq	%r15, %rdx
	je	.L1728
	movq	16(%r12), %rcx
.L1712:
	cmpq	%rcx, %r9
	ja	.L1748
	movb	%al, (%rdx,%r13)
.L1744:
	movq	(%r12), %rax
	addq	$2, %rbx
	movq	%r9, 8(%r12)
	movb	$0, 1(%rax,%r13)
	cmpq	%rbx, %r14
	je	.L1706
.L1716:
	movq	8(%r12), %r13
	movq	(%r12), %rdx
	movzwl	(%rbx), %eax
	leaq	1(%r13), %r9
	cmpq	%r15, %rdx
	je	.L1727
	movq	16(%r12), %rcx
.L1710:
	cmpq	%rcx, %r9
	jbe	.L1711
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, -128(%rbp)
	movl	%eax, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rdx
	movq	-128(%rbp), %r9
	movl	-120(%rbp), %eax
	jmp	.L1711
	.p2align 4,,10
	.p2align 3
.L1748:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, -128(%rbp)
	movq	%r9, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rdx
	movl	-128(%rbp), %eax
	movq	-120(%rbp), %r9
	movb	%al, (%rdx,%r13)
	jmp	.L1744
	.p2align 4,,10
	.p2align 3
.L1728:
	movl	$15, %ecx
	jmp	.L1712
	.p2align 4,,10
	.p2align 3
.L1727:
	movl	$15, %ecx
	jmp	.L1710
	.p2align 4,,10
	.p2align 3
.L1737:
	movzwl	(%rcx), %edx
	addq	$2, %rcx
	addq	$1, %rax
	movb	%dl, -1(%rax)
	cmpq	%rcx, %r14
	jne	.L1737
	jmp	.L1724
	.p2align 4,,10
	.p2align 3
.L1746:
	leaq	-104(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	xorl	%edx, %edx
	movq	%rcx, -128(%rbp)
	movq	%r8, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %r15
	movq	-128(%rbp), %rcx
	movq	%rax, -96(%rbp)
	movq	-120(%rbp), %r8
	movq	%r15, -80(%rbp)
	jmp	.L1719
.L1747:
	call	__stack_chk_fail@PLT
.L1745:
	leaq	.LC52(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE3558:
	.size	_ZN30v8_inspector_protocol_encoding4cbor19EncodeFromUTF16TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanItEEPT_, .-_ZN30v8_inspector_protocol_encoding4cbor19EncodeFromUTF16TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanItEEPT_
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor15EncodeFromUTF16ENS_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor15EncodeFromUTF16ENS_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN30v8_inspector_protocol_encoding4cbor15EncodeFromUTF16ENS_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN30v8_inspector_protocol_encoding4cbor15EncodeFromUTF16ENS_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB3166:
	.cfi_startproc
	endbr64
	jmp	_ZN30v8_inspector_protocol_encoding4cbor19EncodeFromUTF16TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanItEEPT_
	.cfi_endproc
.LFE3166:
	.size	_ZN30v8_inspector_protocol_encoding4cbor15EncodeFromUTF16ENS_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN30v8_inspector_protocol_encoding4cbor15EncodeFromUTF16ENS_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleString16ENS_4spanItEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleString16ENS_4spanItEE, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleString16ENS_4spanItEE:
.LFB4471:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movq	%rsi, %r8
	movq	%rdx, %rsi
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L1752
	ret
	.p2align 4,,10
	.p2align 3
.L1752:
	movq	8(%rdi), %rdx
	movq	%r8, %rdi
	jmp	_ZN30v8_inspector_protocol_encoding4cbor19EncodeFromUTF16TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanItEEPT_
	.cfi_endproc
.LFE4471:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleString16ENS_4spanItEE, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleString16ENS_4spanItEE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor16EncodeBinaryTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_,"axG",@progbits,_ZN30v8_inspector_protocol_encoding4cbor16EncodeBinaryTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_,comdat
	.p2align 4
	.weak	_ZN30v8_inspector_protocol_encoding4cbor16EncodeBinaryTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_
	.type	_ZN30v8_inspector_protocol_encoding4cbor16EncodeBinaryTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_, @function
_ZN30v8_inspector_protocol_encoding4cbor16EncodeBinaryTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_:
.LFB3560:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	addq	$16, %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	-8(%rdx), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-16(%rdx), %rax
	leaq	1(%r14), %r15
	cmpq	%rdx, %rax
	je	.L1765
	movq	16(%r12), %rdx
.L1754:
	cmpq	%rdx, %r15
	ja	.L1782
.L1755:
	movb	$-42, (%rax,%r14)
	movq	(%r12), %rax
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, 8(%r12)
	movl	$2, %edi
	leaq	-80(%rbp), %r15
	movb	$0, 1(%rax,%r14)
	movq	%rbx, %r14
	call	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS0_9MajorTypeEmPT_
	movq	8(%r12), %rcx
	movq	%r15, -96(%rbp)
	addq	(%r12), %rcx
	addq	%r13, %r14
	je	.L1756
	testq	%rbx, %rbx
	je	.L1783
.L1756:
	movq	%r13, -104(%rbp)
	movq	%r13, %rdx
	movq	%r15, %rax
	cmpq	$15, %r13
	ja	.L1784
.L1757:
	cmpq	%rbx, %r14
	je	.L1758
	leaq	15(%rbx), %rdx
	subq	%rax, %rdx
	cmpq	$30, %rdx
	jbe	.L1767
	leaq	-1(%r13), %rdx
	cmpq	$14, %rdx
	jbe	.L1767
	movq	%r13, %rsi
	xorl	%edx, %edx
	andq	$-16, %rsi
	.p2align 4,,10
	.p2align 3
.L1760:
	movdqu	(%rbx,%rdx), %xmm0
	movups	%xmm0, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L1760
	movq	%r13, %rdx
	andq	$-16, %rdx
	leaq	(%rbx,%rdx), %rdi
	addq	%rdx, %rax
	cmpq	%rdx, %r13
	je	.L1762
	movzbl	(%rdi), %edx
	movb	%dl, (%rax)
	leaq	1(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1762
	movzbl	1(%rdi), %edx
	movb	%dl, 1(%rax)
	leaq	2(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1762
	movzbl	2(%rdi), %edx
	movb	%dl, 2(%rax)
	leaq	3(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1762
	movzbl	3(%rdi), %edx
	movb	%dl, 3(%rax)
	leaq	4(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1762
	movzbl	4(%rdi), %edx
	movb	%dl, 4(%rax)
	leaq	5(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1762
	movzbl	5(%rdi), %edx
	movb	%dl, 5(%rax)
	leaq	6(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1762
	movzbl	6(%rdi), %edx
	movb	%dl, 6(%rax)
	leaq	7(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1762
	movzbl	7(%rdi), %edx
	movb	%dl, 7(%rax)
	leaq	8(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1762
	movzbl	8(%rdi), %edx
	movb	%dl, 8(%rax)
	leaq	9(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1762
	movzbl	9(%rdi), %edx
	movb	%dl, 9(%rax)
	leaq	10(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1762
	movzbl	10(%rdi), %edx
	movb	%dl, 10(%rax)
	leaq	11(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1762
	movzbl	11(%rdi), %edx
	movb	%dl, 11(%rax)
	leaq	12(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1762
	movzbl	12(%rdi), %edx
	movb	%dl, 12(%rax)
	leaq	13(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1762
	movzbl	13(%rdi), %edx
	movb	%dl, 13(%rax)
	leaq	14(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L1762
	movzbl	14(%rdi), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L1762:
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rax
.L1758:
	movq	%rdx, -88(%rbp)
	movq	%r12, %rdi
	movb	$0, (%rax,%rdx)
	subq	(%r12), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rsi
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1753
	call	_ZdlPv@PLT
.L1753:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1785
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1782:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	jmp	.L1755
	.p2align 4,,10
	.p2align 3
.L1784:
	xorl	%edx, %edx
	leaq	-104(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	movq	%rcx, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	movq	-120(%rbp), %rcx
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	jmp	.L1757
	.p2align 4,,10
	.p2align 3
.L1765:
	movl	$15, %edx
	jmp	.L1754
	.p2align 4,,10
	.p2align 3
.L1767:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1759:
	movzbl	(%rbx,%rdx), %esi
	movb	%sil, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %r13
	jne	.L1759
	jmp	.L1762
.L1783:
	leaq	.LC52(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1785:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3560:
	.size	_ZN30v8_inspector_protocol_encoding4cbor16EncodeBinaryTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_, .-_ZN30v8_inspector_protocol_encoding4cbor16EncodeBinaryTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12EncodeBinaryENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor12EncodeBinaryENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN30v8_inspector_protocol_encoding4cbor12EncodeBinaryENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN30v8_inspector_protocol_encoding4cbor12EncodeBinaryENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB3169:
	.cfi_startproc
	endbr64
	jmp	_ZN30v8_inspector_protocol_encoding4cbor16EncodeBinaryTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_
	.cfi_endproc
.LFE3169:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12EncodeBinaryENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN30v8_inspector_protocol_encoding4cbor12EncodeBinaryENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleBinaryENS_4spanIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleBinaryENS_4spanIhEE, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleBinaryENS_4spanIhEE:
.LFB4472:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movq	%rsi, %r8
	movq	%rdx, %rsi
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L1789
	ret
	.p2align 4,,10
	.p2align 3
.L1789:
	movq	8(%rdi), %rdx
	movq	%r8, %rdi
	jmp	_ZN30v8_inspector_protocol_encoding4cbor16EncodeBinaryTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_
	.cfi_endproc
.LFE4472:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleBinaryENS_4spanIhEE, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleBinaryENS_4spanIhEE
	.section	.rodata._ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_.str1.1,"aMS",@progbits,1
.LC97:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_,"axG",@progbits,_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	.type	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_, @function
_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_:
.LFB3896:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rdx
	cmpq	16(%rdi), %rdx
	je	.L1791
	movzbl	(%rsi), %eax
	movb	%al, (%rdx)
	addq	$1, 8(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1791:
	.cfi_restore_state
	movabsq	$9223372036854775807, %rax
	movq	(%rdi), %r15
	subq	%r15, %rdx
	movq	%rdx, %r12
	cmpq	%rax, %rdx
	je	.L1807
	movl	$1, %r14d
	testq	%rdx, %rdx
	je	.L1794
	leaq	(%rdx,%rdx), %r14
	cmpq	%r14, %rdx
	jbe	.L1808
	movq	%rax, %r14
.L1794:
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	addq	%rax, %r14
	movzbl	(%rsi), %eax
	movb	%al, 0(%r13,%r12)
	leaq	1(%r13,%r12), %rax
	movq	%rax, -56(%rbp)
	testq	%r12, %r12
	jg	.L1809
	testq	%r15, %r15
	jne	.L1795
.L1796:
	movq	%r13, %xmm0
	movq	%r14, 16(%rbx)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1808:
	.cfi_restore_state
	testq	%r14, %r14
	cmovs	%rax, %r14
	jmp	.L1794
	.p2align 4,,10
	.p2align 3
.L1809:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	memmove@PLT
.L1795:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L1796
.L1807:
	leaq	.LC97(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE3896:
	.size	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_, .-_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS0_9MajorTypeEmPT_,"axG",@progbits,_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS0_9MajorTypeEmPT_,comdat
	.p2align 4
	.weak	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS0_9MajorTypeEmPT_
	.type	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS0_9MajorTypeEmPT_, @function
_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS0_9MajorTypeEmPT_:
.LFB3547:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	sall	$5, %edi
	andl	$8160, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$23, %rsi
	jbe	.L1841
	cmpq	$255, %rsi
	jbe	.L1842
	cmpq	$65535, %rsi
	jbe	.L1843
	movl	$4294967295, %eax
	cmpq	%rax, %rsi
	jbe	.L1844
	orl	$27, %edi
	leaq	-57(%rbp), %rsi
	movl	$56, %r13d
	movb	%dil, -57(%rbp)
	movq	%rdx, %rdi
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movq	8(%r12), %rbx
	jmp	.L1824
	.p2align 4,,10
	.p2align 3
.L1845:
	movb	%dl, (%rbx)
	movq	8(%r12), %rax
	subl	$8, %r13d
	leaq	1(%rax), %rbx
	movq	%rbx, 8(%r12)
	cmpl	$-8, %r13d
	je	.L1810
.L1824:
	movq	%r15, %rdx
	movl	%r13d, %ecx
	shrq	%cl, %rdx
	cmpq	%rbx, 16(%r12)
	jne	.L1845
	movabsq	$9223372036854775807, %rax
	movq	(%r12), %r10
	subq	%r10, %rbx
	movq	%rbx, %r9
	cmpq	%rax, %rbx
	je	.L1846
	movl	$1, %r14d
	testq	%rbx, %rbx
	je	.L1820
	leaq	(%rbx,%rbx), %r14
	cmpq	%r14, %rbx
	jbe	.L1847
	movabsq	$9223372036854775807, %r14
	jmp	.L1820
	.p2align 4,,10
	.p2align 3
.L1847:
	movabsq	$9223372036854775807, %rax
	testq	%r14, %r14
	cmovs	%rax, %r14
.L1820:
	movq	%r14, %rdi
	movq	%rdx, -88(%rbp)
	movq	%r9, -80(%rbp)
	movq	%r10, -72(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %r9
	movq	-88(%rbp), %rdx
	addq	%rax, %r14
	movq	-72(%rbp), %r10
	movq	%rax, %rcx
	testq	%r9, %r9
	movb	%dl, (%rax,%r9)
	leaq	1(%rax,%r9), %rbx
	jg	.L1848
	testq	%r10, %r10
	jne	.L1821
.L1822:
	movq	%rcx, %xmm0
	movq	%rbx, %xmm1
	subl	$8, %r13d
	movq	%r14, 16(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	cmpl	$-8, %r13d
	jne	.L1824
	.p2align 4,,10
	.p2align 3
.L1810:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1849
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1848:
	.cfi_restore_state
	movq	%r10, %rsi
	movq	%rcx, %rdi
	movq	%r9, %rdx
	movq	%r10, -72(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %r10
	movq	%rax, %rcx
.L1821:
	movq	%r10, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rcx
	jmp	.L1822
	.p2align 4,,10
	.p2align 3
.L1841:
	orl	%esi, %edi
	leaq	-57(%rbp), %rsi
	movb	%dil, -57(%rbp)
	movq	%rdx, %rdi
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	jmp	.L1810
	.p2align 4,,10
	.p2align 3
.L1844:
	orl	$26, %edi
	leaq	-57(%rbp), %r13
	movl	$24, %ebx
	movb	%dil, -57(%rbp)
	movq	%r13, %rsi
	movq	%rdx, %rdi
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
.L1816:
	movl	%ebx, %ecx
	movl	%r15d, %eax
	movq	%r13, %rsi
	movq	%r12, %rdi
	shrl	%cl, %eax
	subl	$8, %ebx
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	cmpl	$-8, %ebx
	jne	.L1816
	jmp	.L1810
	.p2align 4,,10
	.p2align 3
.L1842:
	orl	$24, %edi
	leaq	-57(%rbp), %r13
	movb	%dil, -57(%rbp)
.L1840:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movq	%r13, %rsi
	movq	%r12, %rdi
	movb	%r15b, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	jmp	.L1810
	.p2align 4,,10
	.p2align 3
.L1843:
	orl	$25, %edi
	leaq	-57(%rbp), %r13
	movb	%dil, -57(%rbp)
	movq	%r13, %rsi
	movq	%rdx, %rdi
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movl	%r15d, %eax
	sarl	$8, %eax
	movb	%al, -57(%rbp)
	jmp	.L1840
.L1846:
	leaq	.LC97(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1849:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3547:
	.size	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS0_9MajorTypeEmPT_, .-_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS0_9MajorTypeEmPT_
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor9internals15WriteTokenStartENS0_9MajorTypeEmPSt6vectorIhSaIhEE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor9internals15WriteTokenStartENS0_9MajorTypeEmPSt6vectorIhSaIhEE
	.type	_ZN30v8_inspector_protocol_encoding4cbor9internals15WriteTokenStartENS0_9MajorTypeEmPSt6vectorIhSaIhEE, @function
_ZN30v8_inspector_protocol_encoding4cbor9internals15WriteTokenStartENS0_9MajorTypeEmPSt6vectorIhSaIhEE:
.LFB3141:
	.cfi_startproc
	endbr64
	jmp	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS0_9MajorTypeEmPT_
	.cfi_endproc
.LFE3141:
	.size	_ZN30v8_inspector_protocol_encoding4cbor9internals15WriteTokenStartENS0_9MajorTypeEmPSt6vectorIhSaIhEE, .-_ZN30v8_inspector_protocol_encoding4cbor9internals15WriteTokenStartENS0_9MajorTypeEmPSt6vectorIhSaIhEE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE11HandleInt32Ei,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE11HandleInt32Ei, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE11HandleInt32Ei:
.LFB4486:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L1854
	ret
	.p2align 4,,10
	.p2align 3
.L1854:
	movq	8(%rdi), %rdx
	testl	%esi, %esi
	js	.L1853
	movslq	%esi, %rsi
	xorl	%edi, %edi
	jmp	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS0_9MajorTypeEmPT_
	.p2align 4,,10
	.p2align 3
.L1853:
	notl	%esi
	movl	$1, %edi
	movslq	%esi, %rsi
	jmp	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS0_9MajorTypeEmPT_
	.cfi_endproc
.LFE4486:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE11HandleInt32Ei, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE11HandleInt32Ei
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor17EncodeString8TmplISt6vectorIhSaIhEEEEvNS_4spanIhEEPT_,"axG",@progbits,_ZN30v8_inspector_protocol_encoding4cbor17EncodeString8TmplISt6vectorIhSaIhEEEEvNS_4spanIhEEPT_,comdat
	.p2align 4
	.weak	_ZN30v8_inspector_protocol_encoding4cbor17EncodeString8TmplISt6vectorIhSaIhEEEEvNS_4spanIhEEPT_
	.type	_ZN30v8_inspector_protocol_encoding4cbor17EncodeString8TmplISt6vectorIhSaIhEEEEvNS_4spanIhEEPT_, @function
_ZN30v8_inspector_protocol_encoding4cbor17EncodeString8TmplISt6vectorIhSaIhEEEEvNS_4spanIhEEPT_:
.LFB3553:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$3, %edi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	call	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS0_9MajorTypeEmPT_
	testq	%r12, %r12
	je	.L1855
	movq	8(%rbx), %r14
	movq	16(%rbx), %rax
	subq	%r14, %rax
	cmpq	%rax, %r12
	ja	.L1857
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	memmove@PLT
	addq	%r12, 8(%rbx)
.L1855:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1857:
	.cfi_restore_state
	movabsq	$9223372036854775807, %rcx
	movq	(%rbx), %r9
	movq	%r14, %rdx
	movq	%rcx, %rax
	subq	%r9, %rdx
	subq	%rdx, %rax
	cmpq	%rax, %r12
	ja	.L1880
	cmpq	%rdx, %r12
	movq	%rdx, %rax
	cmovnb	%r12, %rax
	addq	%rdx, %rax
	movq	%rax, %r15
	jc	.L1869
	testq	%rax, %rax
	js	.L1869
	jne	.L1861
	movq	$0, -56(%rbp)
	xorl	%r15d, %r15d
.L1867:
	movq	-56(%rbp), %rax
	leaq	(%rax,%rdx), %r11
	leaq	(%r11,%r12), %r10
	testq	%rdx, %rdx
	jne	.L1881
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r11, %rdi
	movq	%r10, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %r9
	subq	%r14, %rdx
	leaq	(%r10,%rdx), %r12
	jne	.L1863
.L1865:
	testq	%r9, %r9
	jne	.L1864
.L1866:
	movq	-56(%rbp), %xmm0
	movq	%r12, %xmm1
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1869:
	.cfi_restore_state
	movq	%rcx, %r15
.L1861:
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	(%rbx), %r9
	movq	%r14, %rdx
	movq	%rax, -56(%rbp)
	addq	%rax, %r15
	subq	%r9, %rdx
	jmp	.L1867
	.p2align 4,,10
	.p2align 3
.L1881:
	movq	%r9, %rsi
	movq	%rax, %rdi
	movq	%r10, -80(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r11, -72(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %r11
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r11, %rdi
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-80(%rbp), %r10
	movq	-64(%rbp), %r9
	subq	%r14, %rdx
	leaq	(%r10,%rdx), %r12
	jne	.L1863
.L1864:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1866
	.p2align 4,,10
	.p2align 3
.L1863:
	movq	%r14, %rsi
	movq	%r10, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	jmp	.L1865
.L1880:
	leaq	.LC25(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE3553:
	.size	_ZN30v8_inspector_protocol_encoding4cbor17EncodeString8TmplISt6vectorIhSaIhEEEEvNS_4spanIhEEPT_, .-_ZN30v8_inspector_protocol_encoding4cbor17EncodeString8TmplISt6vectorIhSaIhEEEEvNS_4spanIhEEPT_
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor11EncodeInt32EiPSt6vectorIhSaIhEE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor11EncodeInt32EiPSt6vectorIhSaIhEE
	.type	_ZN30v8_inspector_protocol_encoding4cbor11EncodeInt32EiPSt6vectorIhSaIhEE, @function
_ZN30v8_inspector_protocol_encoding4cbor11EncodeInt32EiPSt6vectorIhSaIhEE:
.LFB3153:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	testl	%edi, %edi
	js	.L1883
	movslq	%edi, %rsi
	xorl	%edi, %edi
	jmp	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS0_9MajorTypeEmPT_
	.p2align 4,,10
	.p2align 3
.L1883:
	notl	%edi
	movslq	%edi, %rsi
	movl	$1, %edi
	jmp	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS0_9MajorTypeEmPT_
	.cfi_endproc
.LFE3153:
	.size	_ZN30v8_inspector_protocol_encoding4cbor11EncodeInt32EiPSt6vectorIhSaIhEE, .-_ZN30v8_inspector_protocol_encoding4cbor11EncodeInt32EiPSt6vectorIhSaIhEE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor14EncodeString16ENS_4spanItEEPSt6vectorIhSaIhEE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor14EncodeString16ENS_4spanItEEPSt6vectorIhSaIhEE
	.type	_ZN30v8_inspector_protocol_encoding4cbor14EncodeString16ENS_4spanItEEPSt6vectorIhSaIhEE, @function
_ZN30v8_inspector_protocol_encoding4cbor14EncodeString16ENS_4spanItEEPSt6vectorIhSaIhEE:
.LFB3156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	(%rsi,%rsi), %r14
	pushq	%r13
	movq	%r14, %rsi
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$2, %edi
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS0_9MajorTypeEmPT_
	leaq	0(%r13,%r14), %rax
	movq	%rax, -56(%rbp)
	cmpq	%r13, %rax
	je	.L1884
	movq	8(%r15), %r12
	jmp	.L1899
	.p2align 4,,10
	.p2align 3
.L1919:
	movb	%bl, (%r12)
	movq	8(%r15), %rax
	movzbl	%bh, %esi
	movq	16(%r15), %r9
	leaq	1(%rax), %r14
	movq	%r14, 8(%r15)
	cmpq	%r14, %r9
	je	.L1892
.L1922:
	movb	%sil, (%r14)
	movq	8(%r15), %rax
	addq	$2, %r13
	leaq	1(%rax), %r12
	movq	%r12, 8(%r15)
	cmpq	%r13, -56(%rbp)
	je	.L1884
.L1899:
	movzwl	0(%r13), %ebx
	cmpq	%r12, 16(%r15)
	jne	.L1919
	movabsq	$9223372036854775807, %rax
	movq	(%r15), %r11
	subq	%r11, %r12
	cmpq	%rax, %r12
	je	.L1894
	movl	$1, %r14d
	testq	%r12, %r12
	je	.L1889
	leaq	(%r12,%r12), %r14
	cmpq	%r14, %r12
	jbe	.L1920
	movabsq	$9223372036854775807, %r14
	jmp	.L1889
	.p2align 4,,10
	.p2align 3
.L1920:
	movabsq	$9223372036854775807, %rax
	testq	%r14, %r14
	cmovs	%rax, %r14
.L1889:
	movq	%r14, %rdi
	movq	%r11, -64(%rbp)
	call	_Znwm@PLT
	testq	%r12, %r12
	movq	-64(%rbp), %r11
	leaq	(%rax,%r14), %r9
	movb	%bl, (%rax,%r12)
	movq	%rax, %r10
	leaq	1(%rax,%r12), %r14
	jg	.L1921
	testq	%r11, %r11
	jne	.L1890
.L1891:
	movq	%r10, %xmm0
	movq	%r14, %xmm1
	movq	%r9, 16(%r15)
	movzbl	%bh, %esi
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r15)
	cmpq	%r14, %r9
	jne	.L1922
	.p2align 4,,10
	.p2align 3
.L1892:
	movabsq	$9223372036854775807, %rax
	movq	(%r15), %r14
	subq	%r14, %r9
	cmpq	%rax, %r9
	je	.L1894
	movl	$1, %r12d
	testq	%r9, %r9
	je	.L1895
	leaq	(%r9,%r9), %r12
	cmpq	%r12, %r9
	jbe	.L1923
	movabsq	$9223372036854775807, %r12
	jmp	.L1895
	.p2align 4,,10
	.p2align 3
.L1923:
	testq	%r12, %r12
	cmovs	%rax, %r12
.L1895:
	movq	%r12, %rdi
	movl	%esi, -76(%rbp)
	movq	%r9, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %r9
	movl	-76(%rbp), %esi
	movq	%rax, %rbx
	leaq	(%rax,%r12), %rax
	movq	%rax, -64(%rbp)
	leaq	1(%rbx,%r9), %r12
	movb	%sil, (%rbx,%r9)
	testq	%r9, %r9
	jg	.L1924
	testq	%r14, %r14
	jne	.L1896
.L1897:
	movq	-64(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r12, %xmm2
	addq	$2, %r13
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	cmpq	%r13, -56(%rbp)
	jne	.L1899
.L1884:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1921:
	.cfi_restore_state
	movq	%r11, %rsi
	movq	%r10, %rdi
	movq	%r12, %rdx
	movq	%r9, -72(%rbp)
	movq	%r11, -64(%rbp)
	call	memmove@PLT
	movq	-64(%rbp), %r11
	movq	-72(%rbp), %r9
	movq	%rax, %r10
.L1890:
	movq	%r11, %rdi
	movq	%r9, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %r10
	jmp	.L1891
	.p2align 4,,10
	.p2align 3
.L1924:
	movq	%r9, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	memmove@PLT
.L1896:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	jmp	.L1897
.L1894:
	leaq	.LC97(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE3156:
	.size	_ZN30v8_inspector_protocol_encoding4cbor14EncodeString16ENS_4spanItEEPSt6vectorIhSaIhEE, .-_ZN30v8_inspector_protocol_encoding4cbor14EncodeString16ENS_4spanItEEPSt6vectorIhSaIhEE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor13EncodeString8ENS_4spanIhEEPSt6vectorIhSaIhEE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor13EncodeString8ENS_4spanIhEEPSt6vectorIhSaIhEE
	.type	_ZN30v8_inspector_protocol_encoding4cbor13EncodeString8ENS_4spanIhEEPSt6vectorIhSaIhEE, @function
_ZN30v8_inspector_protocol_encoding4cbor13EncodeString8ENS_4spanIhEEPSt6vectorIhSaIhEE:
.LFB3159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$3, %edi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	call	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS0_9MajorTypeEmPT_
	testq	%r12, %r12
	je	.L1925
	movq	8(%rbx), %r14
	movq	16(%rbx), %rax
	subq	%r14, %rax
	cmpq	%r12, %rax
	jb	.L1927
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	memmove@PLT
	addq	%r12, 8(%rbx)
.L1925:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1927:
	.cfi_restore_state
	movabsq	$9223372036854775807, %rcx
	movq	(%rbx), %r9
	movq	%r14, %rdx
	movq	%rcx, %rax
	subq	%r9, %rdx
	subq	%rdx, %rax
	cmpq	%r12, %rax
	jb	.L1950
	cmpq	%r12, %rdx
	movq	%r12, %rax
	cmovnb	%rdx, %rax
	addq	%rdx, %rax
	movq	%rax, %r15
	jc	.L1939
	testq	%rax, %rax
	js	.L1939
	jne	.L1931
	movq	$0, -56(%rbp)
	xorl	%r15d, %r15d
.L1937:
	movq	-56(%rbp), %rax
	leaq	(%rax,%rdx), %r11
	leaq	(%r11,%r12), %r10
	testq	%rdx, %rdx
	jne	.L1951
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r11, %rdi
	movq	%r10, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %r9
	subq	%r14, %rdx
	leaq	(%r10,%rdx), %r12
	jne	.L1933
.L1935:
	testq	%r9, %r9
	jne	.L1934
.L1936:
	movq	-56(%rbp), %xmm0
	movq	%r12, %xmm1
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1939:
	.cfi_restore_state
	movq	%rcx, %r15
.L1931:
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	(%rbx), %r9
	movq	%r14, %rdx
	movq	%rax, -56(%rbp)
	addq	%rax, %r15
	subq	%r9, %rdx
	jmp	.L1937
	.p2align 4,,10
	.p2align 3
.L1951:
	movq	%r9, %rsi
	movq	%rax, %rdi
	movq	%r10, -80(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r11, -72(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %r11
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r11, %rdi
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-80(%rbp), %r10
	movq	-64(%rbp), %r9
	subq	%r14, %rdx
	leaq	(%r10,%rdx), %r12
	jne	.L1933
.L1934:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1936
	.p2align 4,,10
	.p2align 3
.L1933:
	movq	%r14, %rsi
	movq	%r10, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	jmp	.L1935
.L1950:
	leaq	.LC25(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE3159:
	.size	_ZN30v8_inspector_protocol_encoding4cbor13EncodeString8ENS_4spanIhEEPSt6vectorIhSaIhEE, .-_ZN30v8_inspector_protocol_encoding4cbor13EncodeString8ENS_4spanIhEEPSt6vectorIhSaIhEE
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleArrayEndEv.str1.8,"aMS",@progbits,1
	.align 8
.LC98:
	.string	"void v8_inspector_protocol_encoding::json::{anonymous}::JSONEncoder<C>::HandleArrayEnd() [with C = std::vector<unsigned char>]"
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleArrayEndEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleArrayEndEv, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleArrayEndEv:
.LFB4457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1952
	movq	104(%rdi), %r8
	movq	%rdi, %rbx
	movq	80(%rdi), %rdi
	movq	88(%rbx), %rsi
	movq	%r8, %rdx
	subq	72(%rbx), %rdx
	movq	%rdi, %rax
	sarq	$3, %rdx
	subq	%rsi, %rax
	subq	$1, %rdx
	sarq	$3, %rax
	movq	%rdx, %rcx
	salq	$6, %rcx
	leaq	(%rcx,%rax), %rdx
	movq	64(%rbx), %rax
	subq	48(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$1, %rax
	jbe	.L1954
	cmpq	%rsi, %rdi
	je	.L1961
	cmpl	$2, -8(%rdi)
	jne	.L1954
	subq	$8, %rdi
	movq	%rdi, 80(%rbx)
.L1957:
	movq	16(%rbx), %rdi
	leaq	-25(%rbp), %rsi
	movb	$93, -25(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
.L1952:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1962
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1961:
	.cfi_restore_state
	movq	-8(%r8), %rax
	cmpl	$2, 504(%rax)
	jne	.L1954
	call	_ZdlPv@PLT
	movq	104(%rbx), %rax
	leaq	-8(%rax), %rdx
	movq	%rdx, 104(%rbx)
	movq	-8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 88(%rbx)
	addq	$504, %rax
	movq	%rdx, 96(%rbx)
	movq	%rax, 80(%rbx)
	jmp	.L1957
.L1954:
	leaq	.LC98(%rip), %rcx
	movl	$1333, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC32(%rip), %rdi
	call	__assert_fail@PLT
.L1962:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4457:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleArrayEndEv, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleArrayEndEv
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleMapEndEv.str1.8,"aMS",@progbits,1
	.align 8
.LC99:
	.string	"void v8_inspector_protocol_encoding::json::{anonymous}::JSONEncoder<C>::HandleMapEnd() [with C = std::vector<unsigned char>]"
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleMapEndEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleMapEndEv, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleMapEndEv:
.LFB4455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1963
	movq	104(%rdi), %r8
	movq	%rdi, %rbx
	movq	80(%rdi), %rdi
	movq	88(%rbx), %rsi
	movq	%r8, %rdx
	subq	72(%rbx), %rdx
	movq	%rdi, %rax
	sarq	$3, %rdx
	subq	%rsi, %rax
	subq	$1, %rdx
	sarq	$3, %rax
	movq	%rdx, %rcx
	salq	$6, %rcx
	leaq	(%rcx,%rax), %rdx
	movq	64(%rbx), %rax
	subq	48(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$1, %rax
	jbe	.L1965
	cmpq	%rsi, %rdi
	je	.L1972
	cmpl	$1, -8(%rdi)
	jne	.L1965
	subq	$8, %rdi
	movq	%rdi, 80(%rbx)
.L1968:
	movq	16(%rbx), %rdi
	leaq	-25(%rbp), %rsi
	movb	$125, -25(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
.L1963:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1973
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1972:
	.cfi_restore_state
	movq	-8(%r8), %rax
	cmpl	$1, 504(%rax)
	jne	.L1965
	call	_ZdlPv@PLT
	movq	104(%rbx), %rax
	leaq	-8(%rax), %rdx
	movq	%rdx, 104(%rbx)
	movq	-8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 88(%rbx)
	addq	$504, %rax
	movq	%rdx, 96(%rbx)
	movq	%rax, 80(%rbx)
	jmp	.L1968
.L1965:
	leaq	.LC99(%rip), %rcx
	movl	$1317, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC30(%rip), %rdi
	call	__assert_fail@PLT
.L1973:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4455:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleMapEndEv, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleMapEndEv
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor27AppendString8EntryToCBORMapENS_4spanIhEES2_PSt6vectorIhSaIhEE.str1.8,"aMS",@progbits,1
	.align 8
.LC100:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor27AppendString8EntryToCBORMapENS_4spanIhEES2_PSt6vectorIhSaIhEE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor27AppendString8EntryToCBORMapENS_4spanIhEES2_PSt6vectorIhSaIhEE
	.type	_ZN30v8_inspector_protocol_encoding4cbor27AppendString8EntryToCBORMapENS_4spanIhEES2_PSt6vectorIhSaIhEE, @function
_ZN30v8_inspector_protocol_encoding4cbor27AppendString8EntryToCBORMapENS_4spanIhEES2_PSt6vectorIhSaIhEE:
.LFB3236:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	leaq	-128(%rbp), %rdi
	.cfi_offset 14, -32
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -152(%rbp)
	movq	8(%r8), %rbx
	movq	%rbx, %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r8), %rax
	movl	$0, -104(%rbp)
	movq	$-1, -96(%rbp)
	subq	%rax, %rdx
	movq	%rax, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb
	movl	-112(%rbp), %edx
	testl	%edx, %edx
	jne	.L1975
	movl	-104(%rbp), %eax
	movq	-96(%rbp), %r9
.L1976:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1986
	addq	$136, %rsp
	movq	%r9, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1975:
	.cfi_restore_state
	xorl	%r9d, %r9d
	movl	$16, %eax
	cmpl	$12, %edx
	jne	.L1976
	movq	-72(%rbp), %rcx
	movq	8(%r12), %rdx
	leaq	6(%rcx), %rsi
	movq	%rdx, %r10
	subq	(%r12), %r10
	cmpq	%rsi, %r10
	jne	.L1976
	movl	$6, %r9d
	movl	$31, %eax
	testq	%rcx, %rcx
	je	.L1976
	movq	-128(%rbp), %rdi
	movq	-96(%rbp), %rsi
	cmpb	$-65, 6(%rdi,%rsi)
	jne	.L1976
	cmpb	$-1, -1(%rbx)
	je	.L1977
	leaq	5(%rcx), %r9
	movl	$32, %eax
	jmp	.L1976
.L1977:
	subq	$1, %rdx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%r10, -168(%rbp)
	movq	%rdx, 8(%r12)
	movq	%r12, %rdx
	movq	%rcx, -160(%rbp)
	call	_ZN30v8_inspector_protocol_encoding4cbor17EncodeString8TmplISt6vectorIhSaIhEEEEvNS_4spanIhEEPT_
	movq	-152(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN30v8_inspector_protocol_encoding4cbor17EncodeString8TmplISt6vectorIhSaIhEEEEvNS_4spanIhEEPT_
	leaq	-129(%rbp), %rsi
	movq	%r12, %rdi
	movb	$-1, -129(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movq	(%r12), %rdi
	movq	8(%r12), %rdx
	movl	$4294967295, %esi
	movq	-160(%rbp), %rcx
	movq	-168(%rbp), %r10
	subq	%rdi, %rdx
	addq	%rdx, %rcx
	movq	%rcx, %rax
	subq	%r10, %rax
	cmpq	%rsi, %rax
	ja	.L1984
	leaq	-4(%r10,%rdx), %rsi
	subq	%rcx, %rsi
	cmpq	%rsi, %rdx
	jbe	.L1987
	bswap	%eax
	orq	$-1, %r9
	movl	%eax, (%rdi,%rsi)
	xorl	%eax, %eax
	jmp	.L1976
.L1984:
	xorl	%r9d, %r9d
	movl	$33, %eax
	jmp	.L1976
.L1986:
	call	__stack_chk_fail@PLT
.L1987:
	leaq	.LC100(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE3236:
	.size	_ZN30v8_inspector_protocol_encoding4cbor27AppendString8EntryToCBORMapENS_4spanIhEES2_PSt6vectorIhSaIhEE, .-_ZN30v8_inspector_protocol_encoding4cbor27AppendString8EntryToCBORMapENS_4spanIhEES2_PSt6vectorIhSaIhEE
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE16HandleArrayBeginEv.str1.8,"aMS",@progbits,1
	.align 8
.LC101:
	.string	"void v8_inspector_protocol_encoding::json::{anonymous}::State::StartElementTmpl(C*) [with C = std::vector<unsigned char>]"
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE16HandleArrayBeginEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE16HandleArrayBeginEv, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE16HandleArrayBeginEv:
.LFB4456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1988
	movq	80(%rdi), %r12
	movq	%rdi, %rbx
	cmpq	88(%rdi), %r12
	je	.L2012
	movl	-8(%r12), %edx
	movl	-4(%r12), %eax
	testl	%edx, %edx
	jne	.L1991
.L2016:
	testl	%eax, %eax
	jne	.L2013
.L1992:
	addl	$1, %eax
	movl	%eax, -4(%r12)
	movq	96(%rbx), %rcx
	movq	80(%rbx), %rax
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L1994
	movq	$2, (%rax)
	addq	$8, 80(%rbx)
.L1995:
	movq	16(%rbx), %rdi
	leaq	-57(%rbp), %rsi
	movb	$91, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
.L1988:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2014
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1991:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L1992
	cmpl	$2, %edx
	je	.L2004
	testb	$1, %al
	jne	.L2015
.L2004:
	movl	$44, %eax
.L1993:
	movq	16(%rbx), %rdi
	leaq	-57(%rbp), %rsi
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movl	-4(%r12), %eax
	jmp	.L1992
	.p2align 4,,10
	.p2align 3
.L2012:
	movq	104(%rdi), %rax
	movq	-8(%rax), %r12
	movl	504(%r12), %edx
	addq	$512, %r12
	movl	-4(%r12), %eax
	testl	%edx, %edx
	jne	.L1991
	jmp	.L2016
	.p2align 4,,10
	.p2align 3
.L2015:
	movl	$58, %eax
	jmp	.L1993
	.p2align 4,,10
	.p2align 3
.L1994:
	movq	104(%rbx), %r13
	movq	72(%rbx), %rsi
	movabsq	$1152921504606846975, %rdi
	subq	88(%rbx), %rax
	movq	%r13, %r12
	sarq	$3, %rax
	subq	%rsi, %r12
	movq	%r12, %rcx
	sarq	$3, %rcx
	leaq	-1(%rcx), %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movq	64(%rbx), %rax
	subq	48(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rdi, %rax
	je	.L2017
	movq	32(%rbx), %r8
	movq	40(%rbx), %rdx
	movq	%r13, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L2018
.L1997:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r13)
	movq	80(%rbx), %rax
	movq	$2, (%rax)
	movq	104(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 104(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 88(%rbx)
	movq	%rdx, 96(%rbx)
	movq	%rax, 80(%rbx)
	jmp	.L1995
	.p2align 4,,10
	.p2align 3
.L2018:
	leaq	2(%rcx), %r14
	leaq	(%r14,%r14), %rax
	cmpq	%rax, %rdx
	jbe	.L1998
	subq	%r14, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r14
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r14, %rsi
	jbe	.L1999
	cmpq	%rsi, %r13
	je	.L2000
	movq	%r14, %rdi
	call	memmove@PLT
	.p2align 4,,10
	.p2align 3
.L2000:
	movq	%r14, 72(%rbx)
	movq	(%r14), %rax
	leaq	(%r14,%r12), %r13
	movq	(%r14), %xmm0
	movq	%r13, 104(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 56(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 88(%rbx)
	addq	$512, %rax
	movq	%rax, 96(%rbx)
	jmp	.L1997
	.p2align 4,,10
	.p2align 3
.L1998:
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r13
	cmpq	%rdi, %r13
	ja	.L2019
	leaq	0(,%r13,8), %rdi
	call	_Znwm@PLT
	movq	72(%rbx), %rsi
	movq	%rax, %r15
	movq	%r13, %rax
	subq	%r14, %rax
	shrq	%rax
	leaq	(%r15,%rax,8), %r14
	movq	104(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L2002
	subq	%rsi, %rdx
	movq	%r14, %rdi
	call	memmove@PLT
.L2002:
	movq	32(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	%r15, 32(%rbx)
	movq	%r13, 40(%rbx)
	jmp	.L2000
.L1999:
	cmpq	%rsi, %r13
	je	.L2000
	leaq	8(%r12), %rdi
	subq	%rdx, %rdi
	addq	%r14, %rdi
	call	memmove@PLT
	jmp	.L2000
.L2014:
	call	__stack_chk_fail@PLT
.L2013:
	leaq	.LC101(%rip), %rcx
	movl	$1247, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	call	__assert_fail@PLT
.L2019:
	call	_ZSt17__throw_bad_allocv@PLT
.L2017:
	leaq	.LC22(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4456:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE16HandleArrayBeginEv, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE16HandleArrayBeginEv
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE10HandleNullEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE10HandleNullEv, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE10HandleNullEv:
.LFB4464:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L2020
	movq	80(%rdi), %r12
	movq	%rdi, %rbx
	cmpq	88(%rdi), %r12
	je	.L2056
	movl	-8(%r12), %edx
	movl	-4(%r12), %eax
	testl	%edx, %edx
	je	.L2057
.L2023:
	testl	%eax, %eax
	je	.L2024
	testb	$1, %al
	je	.L2037
	movl	$58, %eax
	cmpl	$2, %edx
	jne	.L2025
.L2037:
	movl	$44, %eax
.L2025:
	movq	16(%rbx), %rdi
	leaq	-57(%rbp), %rsi
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movl	-4(%r12), %eax
	jmp	.L2024
	.p2align 4,,10
	.p2align 3
.L2026:
	movabsq	$9223372036854775807, %rdx
	movq	(%rbx), %r15
	movq	%r14, %r12
	movq	%rdx, %rax
	subq	%r15, %r12
	subq	%r12, %rax
	cmpq	$3, %rax
	jbe	.L2058
	cmpq	$4, %r12
	movl	$4, %eax
	cmovnb	%r12, %rax
	addq	%r12, %rax
	movq	%rax, %r13
	jc	.L2039
	testq	%rax, %rax
	js	.L2039
	jne	.L2031
	xorl	%r13d, %r13d
	xorl	%ecx, %ecx
.L2035:
	testq	%r12, %r12
	jne	.L2059
.L2032:
	addq	%rcx, %r12
	movl	$1819047278, (%r12)
	movq	8(%rbx), %rdx
	addq	$4, %r12
	subq	%r14, %rdx
	jne	.L2060
.L2033:
	addq	%rdx, %r12
	testq	%r15, %r15
	je	.L2034
	movq	%r15, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rcx
.L2034:
	movq	%rcx, %xmm0
	movq	%r12, %xmm1
	movq	%r13, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	.p2align 4,,10
	.p2align 3
.L2020:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2061
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2057:
	.cfi_restore_state
	testl	%eax, %eax
	jne	.L2062
.L2024:
	addl	$1, %eax
	movl	%eax, -4(%r12)
	movq	16(%rbx), %rbx
	movq	8(%rbx), %r14
	movq	16(%rbx), %rax
	subq	%r14, %rax
	cmpq	$3, %rax
	jbe	.L2026
	movl	$1819047278, (%r14)
	addq	$4, 8(%rbx)
	jmp	.L2020
	.p2align 4,,10
	.p2align 3
.L2056:
	movq	104(%rdi), %rax
	movq	-8(%rax), %r12
	movl	504(%r12), %edx
	addq	$512, %r12
	movl	-4(%r12), %eax
	testl	%edx, %edx
	jne	.L2023
	jmp	.L2057
	.p2align 4,,10
	.p2align 3
.L2039:
	movq	%rdx, %r13
.L2031:
	movq	%r13, %rdi
	movq	%r14, %r12
	call	_Znwm@PLT
	movq	(%rbx), %r15
	movq	%rax, %rcx
	addq	%rax, %r13
	subq	%r15, %r12
	jmp	.L2035
	.p2align 4,,10
	.p2align 3
.L2060:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	memcpy@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rdx
	jmp	.L2033
	.p2align 4,,10
	.p2align 3
.L2059:
	movq	%rcx, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memmove@PLT
	movq	%rax, %rcx
	jmp	.L2032
.L2061:
	call	__stack_chk_fail@PLT
.L2058:
	leaq	.LC25(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2062:
	leaq	.LC101(%rip), %rcx
	movl	$1247, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4464:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE10HandleNullEv, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE10HandleNullEv
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE10HandleBoolEb,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE10HandleBoolEb, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE10HandleBoolEb:
.LFB4463:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L2063
	movq	80(%rdi), %r13
	movq	%rdi, %rbx
	movl	%esi, %r12d
	cmpq	88(%rdi), %r13
	je	.L2111
	movl	-8(%r13), %edx
	movl	-4(%r13), %eax
	testl	%edx, %edx
	jne	.L2066
.L2114:
	testl	%eax, %eax
	jne	.L2112
.L2067:
	addl	$1, %eax
	cmpb	$1, %r12b
	leaq	.LC2(%rip), %rdx
	movl	%eax, -4(%r13)
	sbbq	%rax, %rax
	leaq	.LC24(%rip), %r14
	notq	%rax
	addq	$5, %rax
	testb	%r12b, %r12b
	movq	16(%rbx), %r12
	cmove	%rdx, %r14
	movq	%rax, %r13
	movq	8(%r12), %r8
	movq	16(%r12), %rdx
	subq	%r8, %rdx
	cmpq	%rax, %rdx
	jb	.L2109
	movl	(%r14), %edx
	movl	%edx, (%r8)
	cmpq	$4, %rax
	je	.L2070
	movzbl	4(%r14), %edx
	movb	%dl, 4(%r8)
.L2070:
	addq	%rax, 8(%r12)
.L2063:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2113
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2066:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L2067
	testb	$1, %al
	je	.L2084
	movl	$58, %eax
	cmpl	$2, %edx
	jne	.L2068
.L2084:
	movl	$44, %eax
.L2068:
	movq	16(%rbx), %rdi
	leaq	-57(%rbp), %rsi
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movl	-4(%r13), %eax
	jmp	.L2067
	.p2align 4,,10
	.p2align 3
.L2111:
	movq	104(%rdi), %rax
	movq	-8(%rax), %r13
	movl	504(%r13), %edx
	addq	$512, %r13
	movl	-4(%r13), %eax
	testl	%edx, %edx
	jne	.L2066
	jmp	.L2114
	.p2align 4,,10
	.p2align 3
.L2109:
	movq	(%r12), %r9
	movq	%r8, %rbx
	movabsq	$9223372036854775807, %rdx
	movq	%rdx, %rcx
	subq	%r9, %rbx
	subq	%rbx, %rcx
	cmpq	%rax, %rcx
	jb	.L2115
	cmpq	%rax, %rbx
	cmovnb	%rbx, %rax
	addq	%rbx, %rax
	movq	%rax, %r15
	jc	.L2087
	testq	%rax, %rax
	js	.L2087
	jne	.L2075
	xorl	%r15d, %r15d
	xorl	%ecx, %ecx
.L2080:
	testq	%rbx, %rbx
	jne	.L2116
.L2076:
	movl	(%r14), %eax
	addq	%rcx, %rbx
	movl	%eax, (%rbx)
	cmpq	$4, %r13
	je	.L2077
	movzbl	4(%r14), %eax
	movb	%al, 4(%rbx)
.L2077:
	addq	%r13, %rbx
	movq	8(%r12), %r13
	subq	%r8, %r13
	jne	.L2117
.L2078:
	addq	%r13, %rbx
	testq	%r9, %r9
	je	.L2079
	movq	%r9, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rcx
.L2079:
	movq	%rcx, %xmm0
	movq	%rbx, %xmm1
	movq	%r15, 16(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	jmp	.L2063
	.p2align 4,,10
	.p2align 3
.L2087:
	movq	%rdx, %r15
.L2075:
	movq	%r15, %rdi
	movq	%r8, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %r8
	movq	(%r12), %r9
	movq	%rax, %rcx
	addq	%rax, %r15
	movq	%r8, %rbx
	subq	%r9, %rbx
	jmp	.L2080
	.p2align 4,,10
	.p2align 3
.L2117:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%rbx, %rdi
	movq	%r9, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	memcpy@PLT
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %rcx
	jmp	.L2078
	.p2align 4,,10
	.p2align 3
.L2116:
	movq	%r9, %rsi
	movq	%rcx, %rdi
	movq	%rbx, %rdx
	movq	%r8, -80(%rbp)
	movq	%r9, -72(%rbp)
	call	memmove@PLT
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %r9
	movq	%rax, %rcx
	jmp	.L2076
.L2113:
	call	__stack_chk_fail@PLT
.L2115:
	leaq	.LC25(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2112:
	leaq	.LC101(%rip), %rcx
	movl	$1247, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4463:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE10HandleBoolEb, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE10HandleBoolEb
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleDoubleEd,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleDoubleEd, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleDoubleEd:
.LFB4461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L2118
	movq	80(%rdi), %r12
	movq	%rdi, %rbx
	cmpq	88(%rdi), %r12
	je	.L2275
	movl	-8(%r12), %edx
	movl	-4(%r12), %eax
	testl	%edx, %edx
	jne	.L2121
.L2281:
	testl	%eax, %eax
	jne	.L2276
.L2122:
	movsd	.LC49(%rip), %xmm2
	movapd	%xmm0, %xmm1
	addl	$1, %eax
	andpd	.LC48(%rip), %xmm1
	movl	%eax, -4(%r12)
	ucomisd	%xmm1, %xmm2
	jb	.L2277
	movq	8(%rbx), %rsi
	leaq	-64(%rbp), %rdi
	movq	(%rsi), %rax
	call	*24(%rax)
	movq	-64(%rbp), %r12
	movzbl	(%r12), %eax
	cmpb	$46, %al
	je	.L2278
	movq	16(%rbx), %r15
	cmpb	$45, %al
	je	.L2279
.L2127:
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2128
	movq	8(%r15), %r13
	movq	16(%r15), %rax
	subq	%r13, %rax
	cmpq	%rax, %rbx
	ja	.L2129
	leaq	15(%r12), %rax
	subq	%r13, %rax
	cmpq	$30, %rax
	jbe	.L2149
	leaq	-1(%rbx), %rax
	cmpq	$14, %rax
	jbe	.L2149
	movq	%rbx, %rdx
	xorl	%eax, %eax
	andq	$-16, %rdx
.L2131:
	movdqu	(%r12,%rax), %xmm3
	movups	%xmm3, 0(%r13,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L2131
	movq	%rbx, %rdx
	movq	%rbx, %rax
	andq	$-16, %rdx
	andl	$15, %eax
	addq	%rdx, %r12
	addq	%rdx, %r13
	cmpq	%rbx, %rdx
	je	.L2133
	movzbl	(%r12), %edx
	movb	%dl, 0(%r13)
	cmpq	$1, %rax
	je	.L2133
	movzbl	1(%r12), %edx
	movb	%dl, 1(%r13)
	cmpq	$2, %rax
	je	.L2133
	movzbl	2(%r12), %edx
	movb	%dl, 2(%r13)
	cmpq	$3, %rax
	je	.L2133
	movzbl	3(%r12), %edx
	movb	%dl, 3(%r13)
	cmpq	$4, %rax
	je	.L2133
	movzbl	4(%r12), %edx
	movb	%dl, 4(%r13)
	cmpq	$5, %rax
	je	.L2133
	movzbl	5(%r12), %edx
	movb	%dl, 5(%r13)
	cmpq	$6, %rax
	je	.L2133
	movzbl	6(%r12), %edx
	movb	%dl, 6(%r13)
	cmpq	$7, %rax
	je	.L2133
	movzbl	7(%r12), %edx
	movb	%dl, 7(%r13)
	cmpq	$8, %rax
	je	.L2133
	movzbl	8(%r12), %edx
	movb	%dl, 8(%r13)
	cmpq	$9, %rax
	je	.L2133
	movzbl	9(%r12), %edx
	movb	%dl, 9(%r13)
	cmpq	$10, %rax
	je	.L2133
	movzbl	10(%r12), %edx
	movb	%dl, 10(%r13)
	cmpq	$11, %rax
	je	.L2133
	movzbl	11(%r12), %edx
	movb	%dl, 11(%r13)
	cmpq	$12, %rax
	je	.L2133
	movzbl	12(%r12), %edx
	movb	%dl, 12(%r13)
	cmpq	$13, %rax
	je	.L2133
	movzbl	13(%r12), %edx
	movb	%dl, 13(%r13)
	cmpq	$14, %rax
	je	.L2133
	movzbl	14(%r12), %eax
	movb	%al, 14(%r13)
	.p2align 4,,10
	.p2align 3
.L2133:
	addq	%rbx, 8(%r15)
.L2128:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2118
	call	_ZdaPv@PLT
.L2118:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2280
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2121:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L2122
	testb	$1, %al
	je	.L2148
	movl	$58, %eax
	cmpl	$2, %edx
	jne	.L2123
.L2148:
	movl	$44, %eax
.L2123:
	movq	16(%rbx), %rdi
	leaq	-64(%rbp), %rsi
	movb	%al, -64(%rbp)
	movsd	%xmm0, -88(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movl	-4(%r12), %eax
	movsd	-88(%rbp), %xmm0
	jmp	.L2122
	.p2align 4,,10
	.p2align 3
.L2275:
	movq	104(%rdi), %rax
	movq	-8(%rax), %r12
	movl	504(%r12), %edx
	addq	$512, %r12
	movl	-4(%r12), %eax
	testl	%edx, %edx
	jne	.L2121
	jmp	.L2281
	.p2align 4,,10
	.p2align 3
.L2279:
	cmpb	$46, 1(%r12)
	jne	.L2127
	movq	%r15, %rdi
	leaq	.LC50(%rip), %rsi
	addq	$1, %r12
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	movq	16(%rbx), %r15
	jmp	.L2127
	.p2align 4,,10
	.p2align 3
.L2277:
	movq	16(%rbx), %rdi
	leaq	.LC23(%rip), %rsi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	jmp	.L2118
	.p2align 4,,10
	.p2align 3
.L2129:
	movabsq	$9223372036854775807, %rdx
	movq	(%r15), %r8
	movq	%r13, %r9
	movq	%rdx, %rax
	subq	%r8, %r9
	subq	%r9, %rax
	cmpq	%rax, %rbx
	ja	.L2282
	cmpq	%r9, %rbx
	movq	%r9, %rax
	cmovnb	%rbx, %rax
	addq	%r9, %rax
	movq	%rax, %r14
	jc	.L2151
	testq	%rax, %rax
	js	.L2151
	jne	.L2137
	xorl	%r14d, %r14d
	xorl	%ecx, %ecx
.L2146:
	testq	%r9, %r9
	jne	.L2283
.L2138:
	leaq	16(%rcx,%r9), %rax
	leaq	(%rcx,%r9), %rdx
	cmpq	%rax, %r12
	leaq	16(%r12), %rax
	setnb	%sil
	cmpq	%rax, %rdx
	setnb	%al
	orb	%al, %sil
	je	.L2152
	leaq	-1(%rbx), %rax
	cmpq	$14, %rax
	jbe	.L2152
	movq	%rbx, %rsi
	xorl	%eax, %eax
	andq	$-16, %rsi
	.p2align 4,,10
	.p2align 3
.L2140:
	movdqu	(%r12,%rax), %xmm4
	movups	%xmm4, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L2140
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	andq	$-16, %rdi
	andl	$15, %esi
	addq	%rdi, %r12
	leaq	(%rdx,%rdi), %rax
	cmpq	%rdi, %rbx
	je	.L2142
	movzbl	(%r12), %edi
	movb	%dil, (%rax)
	cmpq	$1, %rsi
	je	.L2142
	movzbl	1(%r12), %edi
	movb	%dil, 1(%rax)
	cmpq	$2, %rsi
	je	.L2142
	movzbl	2(%r12), %edi
	movb	%dil, 2(%rax)
	cmpq	$3, %rsi
	je	.L2142
	movzbl	3(%r12), %edi
	movb	%dil, 3(%rax)
	cmpq	$4, %rsi
	je	.L2142
	movzbl	4(%r12), %edi
	movb	%dil, 4(%rax)
	cmpq	$5, %rsi
	je	.L2142
	movzbl	5(%r12), %edi
	movb	%dil, 5(%rax)
	cmpq	$6, %rsi
	je	.L2142
	movzbl	6(%r12), %edi
	movb	%dil, 6(%rax)
	cmpq	$7, %rsi
	je	.L2142
	movzbl	7(%r12), %edi
	movb	%dil, 7(%rax)
	cmpq	$8, %rsi
	je	.L2142
	movzbl	8(%r12), %edi
	movb	%dil, 8(%rax)
	cmpq	$9, %rsi
	je	.L2142
	movzbl	9(%r12), %edi
	movb	%dil, 9(%rax)
	cmpq	$10, %rsi
	je	.L2142
	movzbl	10(%r12), %edi
	movb	%dil, 10(%rax)
	cmpq	$11, %rsi
	je	.L2142
	movzbl	11(%r12), %edi
	movb	%dil, 11(%rax)
	cmpq	$12, %rsi
	je	.L2142
	movzbl	12(%r12), %edi
	movb	%dil, 12(%rax)
	cmpq	$13, %rsi
	je	.L2142
	movzbl	13(%r12), %edi
	movb	%dil, 13(%rax)
	cmpq	$14, %rsi
	je	.L2142
	movzbl	14(%r12), %esi
	movb	%sil, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L2142:
	movq	8(%r15), %r12
	addq	%rdx, %rbx
	subq	%r13, %r12
	jne	.L2284
.L2143:
	addq	%r12, %rbx
	testq	%r8, %r8
	je	.L2144
	movq	%r8, %rdi
	movq	%rcx, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %rcx
.L2144:
	movq	%rcx, %xmm0
	movq	%rbx, %xmm5
	movq	%r14, 16(%r15)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, (%r15)
	jmp	.L2128
	.p2align 4,,10
	.p2align 3
.L2278:
	movq	16(%rbx), %rdi
	leaq	-65(%rbp), %rsi
	movb	$48, -65(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movq	16(%rbx), %r15
	jmp	.L2127
	.p2align 4,,10
	.p2align 3
.L2151:
	movq	%rdx, %r14
.L2137:
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	(%r15), %r8
	movq	%r13, %r9
	movq	%rax, %rcx
	addq	%rax, %r14
	subq	%r8, %r9
	jmp	.L2146
	.p2align 4,,10
	.p2align 3
.L2149:
	xorl	%eax, %eax
.L2130:
	movzbl	(%r12,%rax), %edx
	movb	%dl, 0(%r13,%rax)
	addq	$1, %rax
	cmpq	%rax, %rbx
	jne	.L2130
	jmp	.L2133
	.p2align 4,,10
	.p2align 3
.L2283:
	movq	%r9, %rdx
	movq	%r8, %rsi
	movq	%rcx, %rdi
	movq	%r9, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	memmove@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %r8
	movq	%rax, %rcx
	jmp	.L2138
	.p2align 4,,10
	.p2align 3
.L2284:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rcx, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	memcpy@PLT
	movq	-96(%rbp), %rcx
	movq	-88(%rbp), %r8
	jmp	.L2143
	.p2align 4,,10
	.p2align 3
.L2152:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2139:
	movzbl	(%r12,%rax), %esi
	movb	%sil, (%rdx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rbx
	jne	.L2139
	jmp	.L2142
.L2280:
	call	__stack_chk_fail@PLT
.L2276:
	leaq	.LC101(%rip), %rcx
	movl	$1247, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	call	__assert_fail@PLT
.L2282:
	leaq	.LC25(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4461:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleDoubleEd, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleDoubleEd
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleMapBeginEv.str1.8,"aMS",@progbits,1
	.align 8
.LC102:
	.string	"void v8_inspector_protocol_encoding::json::{anonymous}::JSONEncoder<C>::HandleMapBegin() [with C = std::vector<unsigned char>]"
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleMapBeginEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleMapBeginEv, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleMapBeginEv:
.LFB4454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L2285
	movq	80(%rdi), %r12
	movq	%rdi, %rbx
	cmpq	48(%rdi), %r12
	je	.L2310
	cmpq	88(%rdi), %r12
	je	.L2311
	movl	-8(%r12), %edx
	movl	-4(%r12), %eax
	testl	%edx, %edx
	jne	.L2289
.L2314:
	testl	%eax, %eax
	jne	.L2312
.L2290:
	addl	$1, %eax
	movl	%eax, -4(%r12)
	movq	96(%rbx), %rcx
	movq	80(%rbx), %rax
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L2292
	movq	$1, (%rax)
	addq	$8, 80(%rbx)
.L2293:
	movq	16(%rbx), %rdi
	leaq	-57(%rbp), %rsi
	movb	$123, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
.L2285:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2313
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2289:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L2290
	testb	$1, %al
	je	.L2302
	movl	$58, %eax
	cmpl	$2, %edx
	jne	.L2291
.L2302:
	movl	$44, %eax
.L2291:
	movq	16(%rbx), %rdi
	leaq	-57(%rbp), %rsi
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movl	-4(%r12), %eax
	jmp	.L2290
	.p2align 4,,10
	.p2align 3
.L2311:
	movq	104(%rdi), %rax
	movq	-8(%rax), %r12
	movl	504(%r12), %edx
	addq	$512, %r12
	movl	-4(%r12), %eax
	testl	%edx, %edx
	jne	.L2289
	jmp	.L2314
	.p2align 4,,10
	.p2align 3
.L2292:
	movq	104(%rbx), %r13
	movq	72(%rbx), %rsi
	movabsq	$1152921504606846975, %rdi
	subq	88(%rbx), %rax
	movq	%r13, %r12
	sarq	$3, %rax
	subq	%rsi, %r12
	movq	%r12, %rcx
	sarq	$3, %rcx
	leaq	-1(%rcx), %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movq	64(%rbx), %rax
	subq	48(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rdi, %rax
	je	.L2315
	movq	32(%rbx), %r8
	movq	40(%rbx), %rdx
	movq	%r13, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L2316
.L2295:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r13)
	movq	80(%rbx), %rax
	movq	$1, (%rax)
	movq	104(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 104(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 88(%rbx)
	movq	%rdx, 96(%rbx)
	movq	%rax, 80(%rbx)
	jmp	.L2293
	.p2align 4,,10
	.p2align 3
.L2316:
	leaq	2(%rcx), %r14
	leaq	(%r14,%r14), %rax
	cmpq	%rax, %rdx
	jbe	.L2296
	subq	%r14, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r14
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r14, %rsi
	jbe	.L2297
	cmpq	%rsi, %r13
	je	.L2298
	movq	%r14, %rdi
	call	memmove@PLT
	.p2align 4,,10
	.p2align 3
.L2298:
	movq	%r14, 72(%rbx)
	movq	(%r14), %rax
	leaq	(%r14,%r12), %r13
	movq	(%r14), %xmm0
	movq	%r13, 104(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 56(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 88(%rbx)
	addq	$512, %rax
	movq	%rax, 96(%rbx)
	jmp	.L2295
	.p2align 4,,10
	.p2align 3
.L2296:
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r13
	cmpq	%rdi, %r13
	ja	.L2317
	leaq	0(,%r13,8), %rdi
	call	_Znwm@PLT
	movq	72(%rbx), %rsi
	movq	%rax, %r15
	movq	%r13, %rax
	subq	%r14, %rax
	shrq	%rax
	leaq	(%r15,%rax,8), %r14
	movq	104(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L2300
	subq	%rsi, %rdx
	movq	%r14, %rdi
	call	memmove@PLT
.L2300:
	movq	32(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	%r15, 32(%rbx)
	movq	%r13, 40(%rbx)
	jmp	.L2298
.L2297:
	cmpq	%rsi, %r13
	je	.L2298
	leaq	8(%r12), %rdi
	subq	%rdx, %rdi
	addq	%r14, %rdi
	call	memmove@PLT
	jmp	.L2298
.L2313:
	call	__stack_chk_fail@PLT
.L2310:
	leaq	.LC102(%rip), %rcx
	movl	$1308, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC39(%rip), %rdi
	call	__assert_fail@PLT
.L2312:
	leaq	.LC101(%rip), %rcx
	movl	$1247, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	call	__assert_fail@PLT
.L2317:
	call	_ZSt17__throw_bad_allocv@PLT
.L2315:
	leaq	.LC22(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4454:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleMapBeginEv, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleMapBeginEv
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleBinaryENS_4spanIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleBinaryENS_4spanIhEE, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleBinaryENS_4spanIhEE:
.LFB4460:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L2318
	movq	80(%rdi), %rbx
	movq	%rdi, %r12
	cmpq	88(%rdi), %rbx
	je	.L2399
	movl	-8(%rbx), %edx
	movl	-4(%rbx), %eax
	testl	%edx, %edx
	jne	.L2321
.L2413:
	testl	%eax, %eax
	jne	.L2390
.L2397:
	leaq	-57(%rbp), %r15
.L2322:
	addl	$1, %eax
	movq	%r15, %rsi
	movl	%eax, -4(%rbx)
	movq	16(%r12), %rdi
	movb	$34, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	cmpq	$2, -72(%rbp)
	movq	16(%r12), %r14
	jbe	.L2360
	movq	8(%r14), %rcx
	movq	%r15, -120(%rbp)
	movl	$3, %r13d
	movq	%r12, -112(%rbp)
	movq	%rcx, %r15
	jmp	.L2325
	.p2align 4,,10
	.p2align 3
.L2401:
	movb	%r11b, (%r15)
	movq	8(%r14), %rax
	movq	16(%r14), %r9
	leaq	1(%rax), %r15
	movq	%r15, 8(%r14)
.L2327:
	movl	%ebx, %eax
	leaq	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_1L12kBase64TableE(%rip), %rdi
	shrl	$12, %eax
	andl	$63, %eax
	movzbl	(%rdi,%rax), %r12d
	cmpq	%r15, %r9
	je	.L2332
	movb	%r12b, (%r15)
	movq	8(%r14), %rax
	movq	16(%r14), %rcx
	leaq	1(%rax), %r12
	movq	%r12, 8(%r14)
.L2333:
	movl	%ebx, %eax
	leaq	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_1L12kBase64TableE(%rip), %rsi
	shrl	$6, %eax
	andl	$63, %eax
	movzbl	(%rsi,%rax), %r9d
	cmpq	%r12, %rcx
	je	.L2338
	movb	%r9b, (%r12)
	movq	8(%r14), %rax
	movq	16(%r14), %r10
	leaq	1(%rax), %r12
	movq	%r12, 8(%r14)
.L2339:
	andl	$63, %ebx
	leaq	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_1L12kBase64TableE(%rip), %rax
	movzbl	(%rax,%rbx), %r15d
	cmpq	%r10, %r12
	je	.L2343
	movb	%r15b, (%r12)
	movq	8(%r14), %rax
	leaq	1(%rax), %r15
	movq	%r15, 8(%r14)
.L2344:
	leaq	3(%r13), %rax
	cmpq	-72(%rbp), %rax
	ja	.L2400
	movq	%rax, %r13
.L2325:
	movq	-80(%rbp), %rsi
	leaq	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_1L12kBase64TableE(%rip), %rcx
	movzbl	-3(%rsi,%r13), %eax
	movzbl	-2(%rsi,%r13), %edx
	movzbl	-1(%rsi,%r13), %ebx
	sall	$16, %eax
	sall	$8, %edx
	orl	%edx, %eax
	orl	%eax, %ebx
	shrl	$18, %eax
	movzbl	(%rcx,%rax), %r11d
	cmpq	%r15, 16(%r14)
	jne	.L2401
	movabsq	$9223372036854775807, %rax
	movq	(%r14), %r12
	movq	%r15, %rdx
	subq	%r12, %rdx
	cmpq	%rax, %rdx
	je	.L2334
	movl	$1, %r15d
	testq	%rdx, %rdx
	je	.L2329
	leaq	(%rdx,%rdx), %r15
	cmpq	%r15, %rdx
	jbe	.L2402
	movabsq	$9223372036854775807, %r15
.L2329:
	movq	%r15, %rdi
	movq	%rdx, -96(%rbp)
	movb	%r11b, -88(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rdx
	movzbl	-88(%rbp), %r11d
	leaq	(%rax,%r15), %r9
	movq	%rax, %r10
	movb	%r11b, (%rax,%rdx)
	leaq	1(%rax,%rdx), %r15
	testq	%rdx, %rdx
	jg	.L2403
	testq	%r12, %r12
	jne	.L2330
.L2331:
	movq	%r10, %xmm0
	movq	%r15, %xmm1
	movq	%r9, 16(%r14)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r14)
	jmp	.L2327
	.p2align 4,,10
	.p2align 3
.L2400:
	movq	-112(%rbp), %r12
	movq	-120(%rbp), %r15
	leaq	2(%r13), %rdx
	leaq	1(%r13), %rax
.L2324:
	cmpq	-72(%rbp), %rdx
	jbe	.L2404
	cmpq	-72(%rbp), %rax
	jbe	.L2405
.L2349:
	movq	16(%r12), %rdi
	movq	%r15, %rsi
	movb	$34, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
.L2318:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2406
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2321:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L2397
	testb	$1, %al
	je	.L2359
	movl	$58, %eax
	cmpl	$2, %edx
	jne	.L2323
.L2359:
	movl	$44, %eax
.L2323:
	movq	16(%r12), %rdi
	leaq	-57(%rbp), %r15
	movb	%al, -57(%rbp)
	movq	%r15, %rsi
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movl	-4(%rbx), %eax
	jmp	.L2322
	.p2align 4,,10
	.p2align 3
.L2332:
	movabsq	$9223372036854775807, %rax
	movq	(%r14), %r8
	subq	%r8, %r9
	cmpq	%rax, %r9
	je	.L2334
	movl	$1, %ecx
	testq	%r9, %r9
	je	.L2335
	leaq	(%r9,%r9), %rcx
	cmpq	%rcx, %r9
	jbe	.L2407
	movabsq	$9223372036854775807, %rcx
.L2335:
	movq	%rcx, %rdi
	movq	%r9, -104(%rbp)
	movq	%r8, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %r9
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %r8
	movq	%rax, %r15
	addq	%rax, %rcx
	testq	%r9, %r9
	movb	%r12b, (%rax,%r9)
	leaq	1(%rax,%r9), %r12
	jg	.L2408
	testq	%r8, %r8
	jne	.L2336
.L2337:
	movq	%r15, %xmm0
	movq	%r12, %xmm2
	movq	%rcx, 16(%r14)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r14)
	jmp	.L2333
	.p2align 4,,10
	.p2align 3
.L2338:
	movabsq	$9223372036854775807, %rax
	movq	(%r14), %r8
	subq	%r8, %rcx
	movq	%rcx, %rdx
	cmpq	%rax, %rcx
	je	.L2334
	movl	$1, %r12d
	testq	%rcx, %rcx
	je	.L2340
	leaq	(%rcx,%rcx), %r12
	cmpq	%r12, %rcx
	jbe	.L2409
	movabsq	$9223372036854775807, %r12
.L2340:
	movq	%r12, %rdi
	movq	%rdx, -104(%rbp)
	movq	%r8, -96(%rbp)
	movb	%r9b, -88(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %rdx
	movzbl	-88(%rbp), %r9d
	leaq	(%rax,%r12), %r10
	movq	-96(%rbp), %r8
	movq	%rax, %r15
	testq	%rdx, %rdx
	movb	%r9b, (%rax,%rdx)
	leaq	1(%rax,%rdx), %r12
	jg	.L2410
	testq	%r8, %r8
	jne	.L2341
.L2342:
	movq	%r15, %xmm0
	movq	%r12, %xmm3
	movq	%r10, 16(%r14)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%r14)
	jmp	.L2339
	.p2align 4,,10
	.p2align 3
.L2343:
	movabsq	$9223372036854775807, %rax
	movq	(%r14), %r11
	movq	%r12, %r9
	subq	%r11, %r9
	cmpq	%rax, %r9
	je	.L2334
	movl	$1, %ebx
	testq	%r9, %r9
	je	.L2345
	leaq	(%r9,%r9), %rbx
	cmpq	%rbx, %r9
	jbe	.L2411
	movabsq	$9223372036854775807, %rbx
.L2345:
	movq	%rbx, %rdi
	movq	%r9, -96(%rbp)
	movq	%r11, -88(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %r11
	addq	%rax, %rbx
	movq	%rax, %r12
	testq	%r9, %r9
	movb	%r15b, (%rax,%r9)
	leaq	1(%rax,%r9), %r15
	jg	.L2412
	testq	%r11, %r11
	jne	.L2346
.L2347:
	movq	%r12, %xmm0
	movq	%r15, %xmm4
	movq	%rbx, 16(%r14)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, (%r14)
	jmp	.L2344
	.p2align 4,,10
	.p2align 3
.L2402:
	movabsq	$9223372036854775807, %rax
	testq	%r15, %r15
	cmovs	%rax, %r15
	jmp	.L2329
	.p2align 4,,10
	.p2align 3
.L2403:
	movq	%r10, %rdi
	movq	%r12, %rsi
	movq	%r9, -88(%rbp)
	call	memmove@PLT
	movq	-88(%rbp), %r9
	movq	%rax, %r10
.L2330:
	movq	%r12, %rdi
	movq	%r9, -96(%rbp)
	movq	%r10, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %r10
	jmp	.L2331
	.p2align 4,,10
	.p2align 3
.L2408:
	movq	%r8, %rsi
	movq	%r9, %rdx
	movq	%r15, %rdi
	movq	%rcx, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	memmove@PLT
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %rcx
.L2336:
	movq	%r8, %rdi
	movq	%rcx, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %rcx
	jmp	.L2337
	.p2align 4,,10
	.p2align 3
.L2410:
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r10, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	memmove@PLT
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %r10
.L2341:
	movq	%r8, %rdi
	movq	%r10, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %r10
	jmp	.L2342
	.p2align 4,,10
	.p2align 3
.L2412:
	movq	%r11, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r11, -88(%rbp)
	call	memmove@PLT
	movq	-88(%rbp), %r11
.L2346:
	movq	%r11, %rdi
	call	_ZdlPv@PLT
	jmp	.L2347
	.p2align 4,,10
	.p2align 3
.L2407:
	testq	%rcx, %rcx
	cmovs	%rax, %rcx
	jmp	.L2335
	.p2align 4,,10
	.p2align 3
.L2409:
	testq	%r12, %r12
	cmovs	%rax, %r12
	jmp	.L2340
	.p2align 4,,10
	.p2align 3
.L2411:
	testq	%rbx, %rbx
	cmovs	%rax, %rbx
	jmp	.L2345
	.p2align 4,,10
	.p2align 3
.L2399:
	movq	104(%rdi), %rax
	movq	-8(%rax), %rbx
	movl	504(%rbx), %edx
	addq	$512, %rbx
	movl	-4(%rbx), %eax
	testl	%edx, %edx
	jne	.L2321
	jmp	.L2413
	.p2align 4,,10
	.p2align 3
.L2405:
	movq	-80(%rbp), %rax
	movq	%r15, %rsi
	movq	%r14, %rdi
	movzbl	(%rax,%r13), %eax
	leaq	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_1L12kBase64TableE(%rip), %r13
	movl	%eax, %ebx
	shrl	$2, %eax
	sall	$16, %ebx
	movzbl	0(%r13,%rax), %eax
	shrl	$12, %ebx
	andl	$48, %ebx
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movzbl	0(%r13,%rbx), %eax
	movq	%r15, %rsi
	movq	%r14, %rdi
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movb	$61, -57(%rbp)
.L2398:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movq	%r15, %rsi
	movq	%r14, %rdi
	movb	$61, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	jmp	.L2349
	.p2align 4,,10
	.p2align 3
.L2404:
	movq	-80(%rbp), %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movzbl	(%rcx,%r13), %edx
	movzbl	(%rcx,%rax), %ebx
	leaq	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_1L12kBase64TableE(%rip), %r13
	sall	$16, %edx
	sall	$8, %ebx
	orl	%edx, %ebx
	shrl	$18, %edx
	movzbl	0(%r13,%rdx), %eax
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movl	%ebx, %eax
	shrl	$6, %ebx
	movq	%r15, %rsi
	shrl	$12, %eax
	movq	%r14, %rdi
	andl	$60, %ebx
	andl	$63, %eax
	movzbl	0(%r13,%rax), %eax
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movzbl	0(%r13,%rbx), %eax
	movb	%al, -57(%rbp)
	jmp	.L2398
	.p2align 4,,10
	.p2align 3
.L2360:
	movl	$1, %eax
	movl	$2, %edx
	xorl	%r13d, %r13d
	jmp	.L2324
.L2334:
	leaq	.LC97(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2406:
	call	__stack_chk_fail@PLT
.L2390:
	leaq	.LC101(%rip), %rcx
	movl	$1247, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4460:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleBinaryENS_4spanIhEE, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleBinaryENS_4spanIhEE
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleString16ENS_4spanItEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleString16ENS_4spanItEE, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleString16ENS_4spanItEE:
.LFB4459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movl	(%rax), %r9d
	testl	%r9d, %r9d
	jne	.L2414
	movq	80(%rdi), %r13
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movq	%rdx, %r15
	cmpq	88(%rdi), %r13
	je	.L2496
	movl	-8(%r13), %edx
	movl	-4(%r13), %eax
	testl	%edx, %edx
	jne	.L2417
.L2507:
	testl	%eax, %eax
	jne	.L2488
.L2492:
	leaq	-57(%rbp), %r14
.L2418:
	addl	$1, %eax
	movq	%r14, %rsi
	movl	%eax, -4(%r13)
	movq	16(%r12), %rdi
	movq	%rbx, %r13
	movb	$34, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	leaq	(%rbx,%r15,2), %rax
	movq	%rax, -72(%rbp)
	cmpq	%rbx, %rax
	jne	.L2456
	jmp	.L2455
	.p2align 4,,10
	.p2align 3
.L2421:
	cmpw	$92, %bx
	je	.L2497
	cmpw	$8, %bx
	je	.L2498
	cmpw	$12, %bx
	je	.L2499
	cmpw	$10, %bx
	je	.L2500
	cmpw	$13, %bx
	je	.L2501
	cmpw	$9, %bx
	je	.L2502
	leal	-32(%rbx), %eax
	cmpw	$94, %ax
	ja	.L2451
	movq	%r14, %rsi
	movq	%r15, %rdi
	movb	%bl, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	.p2align 4,,10
	.p2align 3
.L2433:
	addq	$2, %r13
	cmpq	%r13, -72(%rbp)
	je	.L2455
.L2456:
	movzwl	0(%r13), %ebx
	movq	16(%r12), %r15
	cmpw	$34, %bx
	jne	.L2421
	movq	8(%r15), %r9
	movq	16(%r15), %rax
	subq	%r9, %rax
	cmpq	$1, %rax
	jbe	.L2422
	movl	$8796, %r8d
	addq	$2, %r13
	movw	%r8w, (%r9)
	addq	$2, 8(%r15)
	cmpq	%r13, -72(%rbp)
	jne	.L2456
.L2455:
	movq	16(%r12), %rdi
	movq	%r14, %rsi
	movb	$34, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
.L2414:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2503
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2417:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L2492
	testb	$1, %al
	je	.L2460
	movl	$58, %eax
	cmpl	$2, %edx
	jne	.L2419
.L2460:
	movl	$44, %eax
.L2419:
	movq	16(%r12), %rdi
	leaq	-57(%rbp), %r14
	movb	%al, -57(%rbp)
	movq	%r14, %rsi
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movl	-4(%r13), %eax
	jmp	.L2418
	.p2align 4,,10
	.p2align 3
.L2497:
	movq	8(%r15), %r9
	movq	16(%r15), %rax
	subq	%r9, %rax
	cmpq	$1, %rax
	jbe	.L2435
	movl	$23644, %ecx
	movw	%cx, (%r9)
	addq	$2, 8(%r15)
	jmp	.L2433
	.p2align 4,,10
	.p2align 3
.L2498:
	leaq	.LC42(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	jmp	.L2433
	.p2align 4,,10
	.p2align 3
.L2499:
	leaq	.LC43(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	jmp	.L2433
	.p2align 4,,10
	.p2align 3
.L2451:
	movq	%r15, %rdi
	leaq	.LC47(%rip), %rsi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	movzwl	%bx, %eax
	movl	$12, %ebx
	movq	16(%r12), %r15
	movl	%eax, -80(%rbp)
	movl	-80(%rbp), %eax
	movl	%ebx, %ecx
	sarl	%cl, %eax
	andl	$15, %eax
	cmpl	$9, %eax
	jle	.L2452
.L2504:
	addl	$87, %eax
.L2494:
	movq	%r14, %rsi
	movq	%r15, %rdi
	subl	$4, %ebx
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	cmpl	$-4, %ebx
	je	.L2433
	movl	-80(%rbp), %eax
	movl	%ebx, %ecx
	sarl	%cl, %eax
	andl	$15, %eax
	cmpl	$9, %eax
	jg	.L2504
.L2452:
	addl	$48, %eax
	jmp	.L2494
	.p2align 4,,10
	.p2align 3
.L2500:
	leaq	.LC44(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	jmp	.L2433
	.p2align 4,,10
	.p2align 3
.L2501:
	leaq	.LC45(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	jmp	.L2433
	.p2align 4,,10
	.p2align 3
.L2502:
	leaq	.LC46(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	jmp	.L2433
	.p2align 4,,10
	.p2align 3
.L2422:
	movabsq	$9223372036854775807, %rax
	movq	(%r15), %r10
	movq	%r9, %rdx
	subq	%r10, %rdx
	subq	%rdx, %rax
	cmpq	$1, %rax
	jbe	.L2437
	cmpq	$2, %rdx
	movl	$2, %eax
	cmovnb	%rdx, %rax
	addq	%rdx, %rax
	movq	%rax, %rbx
	jc	.L2462
	testq	%rax, %rax
	js	.L2462
	jne	.L2427
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
.L2457:
	leaq	(%rcx,%rdx), %r11
	leaq	2(%r11), %r8
	testq	%rdx, %rdx
	jne	.L2505
	movl	$8796, %esi
	movw	%si, (%r11)
	movq	8(%r15), %rdx
	subq	%r9, %rdx
	jne	.L2506
.L2431:
	testq	%r10, %r10
	jne	.L2430
.L2432:
	movq	%rcx, %xmm0
	movq	%r8, %xmm1
	movq	%rbx, 16(%r15)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r15)
	jmp	.L2433
	.p2align 4,,10
	.p2align 3
.L2496:
	movq	104(%rdi), %rax
	movq	-8(%rax), %r13
	movl	504(%r13), %edx
	addq	$512, %r13
	movl	-4(%r13), %eax
	testl	%edx, %edx
	jne	.L2417
	jmp	.L2507
	.p2align 4,,10
	.p2align 3
.L2462:
	movabsq	$9223372036854775807, %rbx
.L2427:
	movq	%rbx, %rdi
	movq	%r9, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %r9
	movq	(%r15), %r10
	movq	%rax, %rcx
	addq	%rax, %rbx
	movq	%r9, %rdx
	subq	%r10, %rdx
	jmp	.L2457
	.p2align 4,,10
	.p2align 3
.L2435:
	movabsq	$9223372036854775807, %rax
	movq	(%r15), %r10
	movq	%r9, %rdx
	subq	%r10, %rdx
	subq	%rdx, %rax
	cmpq	$1, %rax
	jbe	.L2437
	cmpq	$2, %rdx
	movl	$2, %eax
	cmovnb	%rdx, %rax
	addq	%rdx, %rax
	movq	%rax, %rbx
	jc	.L2464
	testq	%rax, %rax
	js	.L2464
	jne	.L2440
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
.L2458:
	leaq	(%rcx,%rdx), %r11
	leaq	2(%r11), %r8
	testq	%rdx, %rdx
	jne	.L2508
	movl	$23644, %eax
	movw	%ax, (%r11)
	movq	8(%r15), %rdx
	subq	%r9, %rdx
	jne	.L2509
.L2444:
	testq	%r10, %r10
	jne	.L2443
.L2445:
	movq	%rcx, %xmm0
	movq	%r8, %xmm2
	movq	%rbx, 16(%r15)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r15)
	jmp	.L2433
	.p2align 4,,10
	.p2align 3
.L2464:
	movabsq	$9223372036854775807, %rbx
.L2440:
	movq	%rbx, %rdi
	movq	%r9, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %r9
	movq	(%r15), %r10
	movq	%rax, %rcx
	addq	%rax, %rbx
	movq	%r9, %rdx
	subq	%r10, %rdx
	jmp	.L2458
	.p2align 4,,10
	.p2align 3
.L2505:
	movq	%rcx, %rdi
	movq	%r10, %rsi
	movq	%r11, -104(%rbp)
	movq	%r8, -96(%rbp)
	movq	%r9, -88(%rbp)
	movq	%r10, -80(%rbp)
	call	memmove@PLT
	movq	-104(%rbp), %r11
	movl	$8796, %edi
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %r8
	movq	-80(%rbp), %r10
	movq	%rax, %rcx
	movw	%di, (%r11)
	movq	8(%r15), %rdx
	subq	%r9, %rdx
	leaq	(%r8,%rdx), %r11
	jne	.L2429
	movq	%r11, %r8
.L2430:
	movq	%r10, %rdi
	movq	%rcx, -88(%rbp)
	movq	%r8, -80(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %rcx
	movq	-80(%rbp), %r8
	jmp	.L2432
	.p2align 4,,10
	.p2align 3
.L2508:
	movq	%r10, %rsi
	movq	%rcx, %rdi
	movq	%r9, -104(%rbp)
	movq	%r8, -96(%rbp)
	movq	%r11, -88(%rbp)
	movq	%r10, -80(%rbp)
	call	memmove@PLT
	movq	-88(%rbp), %r11
	movl	$23644, %edx
	movq	-104(%rbp), %r9
	movq	-96(%rbp), %r8
	movq	-80(%rbp), %r10
	movq	%rax, %rcx
	movw	%dx, (%r11)
	movq	8(%r15), %rdx
	subq	%r9, %rdx
	leaq	(%r8,%rdx), %r11
	jne	.L2442
	movq	%r11, %r8
.L2443:
	movq	%r10, %rdi
	movq	%rcx, -88(%rbp)
	movq	%r8, -80(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %rcx
	movq	-80(%rbp), %r8
	jmp	.L2445
.L2506:
	leaq	(%r8,%rdx), %r11
.L2429:
	movq	%r8, %rdi
	movq	%r9, %rsi
	movq	%r11, -96(%rbp)
	movq	%r10, -88(%rbp)
	movq	%rcx, -80(%rbp)
	call	memcpy@PLT
	movq	-96(%rbp), %r11
	movq	-88(%rbp), %r10
	movq	-80(%rbp), %rcx
	movq	%r11, %r8
	jmp	.L2431
.L2509:
	leaq	(%r8,%rdx), %r11
.L2442:
	movq	%r8, %rdi
	movq	%r9, %rsi
	movq	%r10, -96(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%r11, -80(%rbp)
	call	memcpy@PLT
	movq	-80(%rbp), %r11
	movq	-96(%rbp), %r10
	movq	-88(%rbp), %rcx
	movq	%r11, %r8
	jmp	.L2444
.L2503:
	call	__stack_chk_fail@PLT
.L2488:
	leaq	.LC101(%rip), %rcx
	movl	$1247, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	call	__assert_fail@PLT
.L2437:
	leaq	.LC25(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4459:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleString16ENS_4spanItEE, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleString16ENS_4spanItEE
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE13HandleString8ENS_4spanIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE13HandleString8ENS_4spanIhEE, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE13HandleString8ENS_4spanIhEE:
.LFB4458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	jne	.L2510
	movq	80(%rdi), %rbx
	movq	%rdi, %r12
	cmpq	88(%rdi), %rbx
	je	.L2672
	movl	-8(%rbx), %edx
	movl	-4(%rbx), %eax
	testl	%edx, %edx
	jne	.L2513
.L2691:
	testl	%eax, %eax
	jne	.L2654
.L2665:
	leaq	-57(%rbp), %rcx
	movq	%rcx, -88(%rbp)
.L2514:
	addl	$1, %eax
	movq	-88(%rbp), %rsi
	movl	%eax, -4(%rbx)
	movq	16(%r12), %rdi
	movb	$34, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	cmpq	$0, -72(%rbp)
	movq	16(%r12), %r14
	je	.L2603
	xorl	%r13d, %r13d
	movq	%r13, %r15
	.p2align 4,,10
	.p2align 3
.L2602:
	movq	-80(%rbp), %rax
	movzbl	(%rax,%r15), %ebx
	cmpb	$34, %bl
	je	.L2673
	cmpb	$92, %bl
	je	.L2674
	cmpb	$8, %bl
	je	.L2675
	cmpb	$12, %bl
	je	.L2676
	cmpb	$10, %bl
	je	.L2677
	cmpb	$13, %bl
	je	.L2678
	cmpb	$9, %bl
	je	.L2679
	leal	-32(%rbx), %eax
	cmpb	$94, %al
	jbe	.L2680
	cmpb	$31, %bl
	jbe	.L2681
	movl	%ebx, %eax
	andl	$-32, %eax
	cmpb	$-64, %al
	je	.L2682
	movl	%ebx, %eax
	andl	$-16, %eax
	cmpb	$-32, %al
	je	.L2683
	movl	%ebx, %eax
	andl	$-8, %eax
	cmpb	$-16, %al
	jne	.L2529
	andl	$7, %ebx
	movl	$3, %eax
	movl	$3, %esi
.L2543:
	addq	%r15, %rax
	cmpq	-72(%rbp), %rax
	jnb	.L2529
	movq	-80(%rbp), %rax
	leaq	1(%r15), %rcx
	movzbl	1(%r15,%rax), %edx
	leal	-1(%rsi), %eax
	movl	%edx, %esi
	andl	$-64, %esi
	cmpb	$-128, %sil
	jne	.L2545
	sall	$6, %ebx
	andl	$63, %edx
	orl	%edx, %ebx
.L2545:
	testl	%eax, %eax
	je	.L2546
	movq	-80(%rbp), %rdx
	movzbl	2(%rdx,%r15), %edx
	movl	%edx, %esi
	andl	$-64, %esi
	cmpb	$-128, %sil
	jne	.L2547
	sall	$6, %ebx
	andl	$63, %edx
	orl	%edx, %ebx
.L2547:
	cmpl	$1, %eax
	je	.L2548
	movq	-80(%rbp), %rdx
	movzbl	3(%rdx,%r15), %edx
	movl	%edx, %esi
	andl	$-64, %esi
	cmpb	$-128, %sil
	jne	.L2548
	sall	$6, %ebx
	andl	$63, %edx
	orl	%edx, %ebx
.L2548:
	movslq	%eax, %r8
	leal	-128(%rbx), %eax
	leaq	(%r8,%rcx), %r15
	cmpl	$1113983, %eax
	jbe	.L2684
	.p2align 4,,10
	.p2align 3
.L2529:
	addq	$1, %r15
	cmpq	%r15, -72(%rbp)
	ja	.L2602
	.p2align 4,,10
	.p2align 3
.L2603:
	movq	-88(%rbp), %rsi
	movq	%r14, %rdi
	movb	$34, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
.L2510:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2685
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2513:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L2665
	cmpl	$2, %edx
	je	.L2611
	testb	$1, %al
	jne	.L2686
.L2611:
	movl	$44, %eax
.L2515:
	movb	%al, -57(%rbp)
	movq	16(%r12), %rdi
	leaq	-57(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -88(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movl	-4(%rbx), %eax
	jmp	.L2514
	.p2align 4,,10
	.p2align 3
.L2673:
	movq	8(%r14), %r13
	movq	16(%r14), %rax
	subq	%r13, %rax
	cmpq	$1, %rax
	jbe	.L2518
	movl	$8796, %ecx
	movw	%cx, 0(%r13)
	addq	$2, 8(%r14)
.L2670:
	movq	16(%r12), %r14
	addq	$1, %r15
	cmpq	%r15, -72(%rbp)
	ja	.L2602
	jmp	.L2603
	.p2align 4,,10
	.p2align 3
.L2674:
	movq	%r14, %rdi
	leaq	.LC41(%rip), %rsi
	addq	$1, %r15
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	movq	16(%r12), %r14
	cmpq	%r15, -72(%rbp)
	ja	.L2602
	jmp	.L2603
	.p2align 4,,10
	.p2align 3
.L2675:
	movq	%r14, %rdi
	leaq	.LC42(%rip), %rsi
	addq	$1, %r15
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	movq	16(%r12), %r14
	cmpq	%r15, -72(%rbp)
	ja	.L2602
	jmp	.L2603
	.p2align 4,,10
	.p2align 3
.L2676:
	movq	%r14, %rdi
	leaq	.LC43(%rip), %rsi
	addq	$1, %r15
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	movq	16(%r12), %r14
	cmpq	%r15, -72(%rbp)
	ja	.L2602
	jmp	.L2603
	.p2align 4,,10
	.p2align 3
.L2677:
	movq	%r14, %rdi
	leaq	.LC44(%rip), %rsi
	addq	$1, %r15
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	movq	16(%r12), %r14
	cmpq	%r15, -72(%rbp)
	ja	.L2602
	jmp	.L2603
	.p2align 4,,10
	.p2align 3
.L2680:
	movq	-88(%rbp), %rsi
	movq	%r14, %rdi
	movb	%bl, -57(%rbp)
	addq	$1, %r15
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movq	16(%r12), %r14
	cmpq	%r15, -72(%rbp)
	ja	.L2602
	jmp	.L2603
	.p2align 4,,10
	.p2align 3
.L2678:
	movq	%r14, %rdi
	leaq	.LC45(%rip), %rsi
	addq	$1, %r15
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	movq	16(%r12), %r14
	cmpq	%r15, -72(%rbp)
	ja	.L2602
	jmp	.L2603
	.p2align 4,,10
	.p2align 3
.L2679:
	movq	%r14, %rdi
	leaq	.LC46(%rip), %rsi
	addq	$1, %r15
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	movq	16(%r12), %r14
	cmpq	%r15, -72(%rbp)
	ja	.L2602
	jmp	.L2603
	.p2align 4,,10
	.p2align 3
.L2518:
	movabsq	$9223372036854775807, %rax
	movq	(%r14), %r9
	movq	%r13, %rdx
	subq	%r9, %rdx
	subq	%rdx, %rax
	cmpq	$1, %rax
	jbe	.L2687
	cmpq	$2, %rdx
	movl	$2, %eax
	cmovnb	%rdx, %rax
	addq	%rdx, %rax
	movq	%rax, %rbx
	jc	.L2613
	testq	%rax, %rax
	js	.L2613
	jne	.L2523
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
.L2604:
	leaq	(%rcx,%rdx), %r10
	leaq	2(%r10), %r11
	testq	%rdx, %rdx
	jne	.L2688
	movl	$8796, %eax
	movw	%ax, (%r10)
	movq	8(%r14), %rdx
	subq	%r13, %rdx
	jne	.L2689
.L2527:
	testq	%r9, %r9
	jne	.L2526
.L2528:
	movq	%rcx, %xmm0
	movq	%r11, %xmm1
	movq	%rbx, 16(%r14)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r14)
	jmp	.L2670
	.p2align 4,,10
	.p2align 3
.L2681:
	movq	%r14, %rdi
	leaq	.LC47(%rip), %rsi
	movl	$12, %r13d
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	movq	16(%r12), %r14
	movl	%r13d, %eax
	movq	%r14, %r13
	movl	%eax, %r14d
	movl	%ebx, %eax
	movl	%r14d, %ecx
	sarl	%cl, %eax
	andl	$15, %eax
	cmpl	$9, %eax
	jle	.L2538
.L2690:
	addl	$87, %eax
.L2667:
	movq	-88(%rbp), %rsi
	movq	%r13, %rdi
	subl	$4, %r14d
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	cmpl	$-4, %r14d
	je	.L2670
	movl	%ebx, %eax
	movl	%r14d, %ecx
	sarl	%cl, %eax
	andl	$15, %eax
	cmpl	$9, %eax
	jg	.L2690
.L2538:
	addl	$48, %eax
	jmp	.L2667
	.p2align 4,,10
	.p2align 3
.L2672:
	movq	104(%rdi), %rax
	movq	-8(%rax), %rbx
	movl	504(%rbx), %edx
	addq	$512, %rbx
	movl	-4(%rbx), %eax
	testl	%edx, %edx
	jne	.L2513
	jmp	.L2691
	.p2align 4,,10
	.p2align 3
.L2686:
	movl	$58, %eax
	jmp	.L2515
	.p2align 4,,10
	.p2align 3
.L2613:
	movabsq	$9223372036854775807, %rbx
.L2523:
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	(%r14), %r9
	movq	%r13, %rdx
	movq	%rax, %rcx
	addq	%rax, %rbx
	subq	%r9, %rdx
	jmp	.L2604
	.p2align 4,,10
	.p2align 3
.L2682:
	andl	$31, %ebx
	movl	$1, %eax
	movl	$1, %esi
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2688:
	movq	%r9, %rsi
	movq	%rcx, %rdi
	movq	%r11, -112(%rbp)
	movq	%r10, -104(%rbp)
	movq	%r9, -96(%rbp)
	call	memmove@PLT
	movq	-104(%rbp), %r10
	movl	$8796, %edx
	movq	-112(%rbp), %r11
	movq	-96(%rbp), %r9
	movq	%rax, %rcx
	movw	%dx, (%r10)
	movq	8(%r14), %rdx
	subq	%r13, %rdx
	leaq	(%r11,%rdx), %r10
	jne	.L2525
	movq	%r10, %r11
.L2526:
	movq	%r9, %rdi
	movq	%r11, -104(%rbp)
	movq	%rcx, -96(%rbp)
	call	_ZdlPv@PLT
	movq	-104(%rbp), %r11
	movq	-96(%rbp), %rcx
	jmp	.L2528
	.p2align 4,,10
	.p2align 3
.L2683:
	andl	$15, %ebx
	movl	$2, %eax
	movl	$2, %esi
	jmp	.L2543
.L2689:
	leaq	(%r11,%rdx), %r10
.L2525:
	movq	%r11, %rdi
	movq	%r13, %rsi
	movq	%r10, -112(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%r9, -96(%rbp)
	call	memcpy@PLT
	movq	-112(%rbp), %r10
	movq	-104(%rbp), %rcx
	movq	-96(%rbp), %r9
	movq	%r10, %r11
	jmp	.L2527
.L2546:
	leal	-128(%rbx), %eax
	movq	%rcx, %r15
	cmpl	$1113983, %eax
	ja	.L2529
.L2609:
	leaq	.LC47(%rip), %rsi
	movq	%r14, %rdi
	movq	%rcx, -96(%rbp)
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	movl	%ebx, %eax
	movl	$48, %ecx
	movl	$87, %esi
	sarl	$12, %eax
	cmpl	$40959, %ebx
	movq	16(%r12), %r14
	cmovle	%ecx, %esi
	movq	-96(%rbp), %rcx
	movq	8(%r14), %rdx
	addl	%eax, %esi
	cmpq	16(%r14), %rdx
	je	.L2692
	movb	%sil, (%rdx)
	movq	8(%r14), %rax
	movq	16(%r14), %r13
	leaq	1(%rax), %r8
	movq	%r8, 8(%r14)
.L2560:
	movl	%ebx, %esi
	movl	$48, %edx
	sarl	$8, %esi
	movl	%esi, %eax
	movl	$87, %esi
	andl	$15, %eax
	cmpl	$10, %eax
	cmovl	%edx, %esi
	addl	%eax, %esi
	cmpq	%r8, %r13
	je	.L2693
	movb	%sil, (%r8)
	movq	8(%r14), %rax
	movq	16(%r14), %r13
	leaq	1(%rax), %r8
	movq	%r8, 8(%r14)
.L2571:
	movl	%ebx, %esi
	movl	$48, %edx
	sarl	$4, %esi
	movl	%esi, %eax
	movl	$87, %esi
	andl	$15, %eax
	cmpl	$10, %eax
	cmovl	%edx, %esi
	addl	%eax, %esi
	cmpq	%r8, %r13
	je	.L2694
	movb	%sil, (%r8)
	movq	8(%r14), %rax
	movq	16(%r14), %r13
	leaq	1(%rax), %r8
	movq	%r8, 8(%r14)
.L2582:
	andl	$15, %ebx
	movl	$87, %eax
	movl	$48, %edi
	cmpl	$10, %ebx
	cmovl	%edi, %eax
	addl	%eax, %ebx
	cmpq	%r8, %r13
	je	.L2695
	movb	%bl, (%r8)
	movq	%rcx, %r15
	addq	$1, 8(%r14)
	movq	16(%r12), %r14
	jmp	.L2529
.L2684:
	cmpl	$65534, %ebx
	jbe	.L2696
	subl	$65536, %ebx
	movq	%r14, %rdi
	leaq	.LC47(%rip), %rsi
	movl	%ebx, -96(%rbp)
	shrl	$10, %ebx
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	movl	%ebx, %r14d
	movq	16(%r12), %r13
	movl	$12, %ebx
	movl	%ebx, %eax
	subw	$10240, %r14w
	movq	%r13, %rbx
	movzwl	%r14w, %r14d
	movl	%eax, %r13d
.L2598:
	movl	%r14d, %eax
	movl	%r13d, %ecx
	sarl	%cl, %eax
	andl	$15, %eax
	cmpl	$9, %eax
	jle	.L2595
	addl	$87, %eax
.L2669:
	movq	-88(%rbp), %rsi
	movq	%rbx, %rdi
	subl	$4, %r13d
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	cmpl	$-4, %r13d
	jne	.L2598
	movq	16(%r12), %rdi
	leaq	.LC47(%rip), %rsi
	movl	$8, %r13d
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	movzwl	-96(%rbp), %ebx
	movq	16(%r12), %r14
	movq	%r12, -96(%rbp)
	movl	%r13d, %r12d
	movl	$13, %eax
	movl	$87, %edx
	andw	$1023, %bx
	subw	$9216, %bx
	movzwl	%bx, %ebx
	movl	%ebx, %r13d
	movq	-88(%rbp), %rbx
.L2599:
	addl	%edx, %eax
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	cmpl	$-4, %r12d
	je	.L2697
	movl	%r12d, %ecx
	movl	%r13d, %eax
	movl	$87, %edx
	sarl	%cl, %eax
	movl	$48, %ecx
	andl	$15, %eax
	cmpl	$10, %eax
	cmovl	%ecx, %edx
	subl	$4, %r12d
	jmp	.L2599
	.p2align 4,,10
	.p2align 3
.L2595:
	addl	$48, %eax
	jmp	.L2669
.L2697:
	movq	-96(%rbp), %r12
	jmp	.L2670
.L2695:
	movabsq	$9223372036854775807, %rax
	movq	(%r14), %r9
	movq	%r13, %rdx
	subq	%r9, %rdx
	cmpq	%rax, %rdx
	je	.L2553
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	addq	%rdx, %rax
	movq	%rax, %r13
	jc	.L2625
	testq	%rax, %rax
	js	.L2625
	jne	.L2589
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
.L2605:
	movb	%bl, (%r15,%rdx)
	leaq	1(%r15,%rdx), %rbx
	testq	%rdx, %rdx
	jg	.L2591
	testq	%r9, %r9
	jne	.L2592
.L2593:
	movq	%r15, %xmm0
	movq	%rbx, %xmm5
	movq	%r13, 16(%r14)
	movq	%rcx, %r15
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, (%r14)
	movq	16(%r12), %r14
	jmp	.L2529
.L2694:
	movabsq	$9223372036854775807, %rax
	movq	(%r14), %r10
	movq	%r13, %rdx
	subq	%r10, %rdx
	cmpq	%rax, %rdx
	je	.L2553
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	addq	%rdx, %rax
	movq	%rax, %r13
	jc	.L2622
	testq	%rax, %rax
	js	.L2622
	jne	.L2578
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
.L2606:
	movb	%sil, (%r15,%rdx)
	leaq	1(%r15,%rdx), %r8
	testq	%rdx, %rdx
	jg	.L2580
	testq	%r10, %r10
	jne	.L2581
.L2583:
	movq	%r15, %xmm0
	movq	%r8, %xmm4
	movq	%r13, 16(%r14)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, (%r14)
	jmp	.L2582
.L2693:
	movabsq	$9223372036854775807, %rax
	movq	(%r14), %r10
	movq	%r13, %rdx
	subq	%r10, %rdx
	cmpq	%rax, %rdx
	je	.L2553
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	addq	%rdx, %rax
	movq	%rax, %r13
	jc	.L2619
	testq	%rax, %rax
	js	.L2619
	jne	.L2567
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
.L2607:
	movb	%sil, (%r15,%rdx)
	leaq	1(%r15,%rdx), %r8
	testq	%rdx, %rdx
	jg	.L2569
	testq	%r10, %r10
	jne	.L2570
.L2572:
	movq	%r15, %xmm0
	movq	%r8, %xmm3
	movq	%r13, 16(%r14)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%r14)
	jmp	.L2571
.L2692:
	movabsq	$9223372036854775807, %rax
	movq	(%r14), %r10
	subq	%r10, %rdx
	cmpq	%rax, %rdx
	je	.L2553
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	addq	%rdx, %rax
	movq	%rax, %r13
	jc	.L2616
	testq	%rax, %rax
	js	.L2616
	jne	.L2556
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
.L2608:
	movb	%sil, (%r15,%rdx)
	leaq	1(%r15,%rdx), %r8
	testq	%rdx, %rdx
	jg	.L2558
	testq	%r10, %r10
	jne	.L2559
.L2561:
	movq	%r15, %xmm0
	movq	%r8, %xmm2
	movq	%r13, 16(%r14)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r14)
	jmp	.L2560
.L2625:
	movabsq	$9223372036854775807, %r13
.L2589:
	movq	%r13, %rdi
	movq	%rdx, -112(%rbp)
	movq	%r9, -104(%rbp)
	movq	%rcx, -96(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %r9
	movq	-112(%rbp), %rdx
	movq	%rax, %r15
	addq	%rax, %r13
	jmp	.L2605
.L2622:
	movabsq	$9223372036854775807, %r13
.L2578:
	movq	%r13, %rdi
	movq	%rdx, -120(%rbp)
	movq	%r10, -112(%rbp)
	movb	%sil, -104(%rbp)
	movq	%rcx, -96(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movzbl	-104(%rbp), %esi
	movq	-112(%rbp), %r10
	movq	-120(%rbp), %rdx
	movq	%rax, %r15
	addq	%rax, %r13
	jmp	.L2606
.L2619:
	movabsq	$9223372036854775807, %r13
.L2567:
	movq	%r13, %rdi
	movq	%rdx, -120(%rbp)
	movq	%r10, -112(%rbp)
	movb	%sil, -104(%rbp)
	movq	%rcx, -96(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movzbl	-104(%rbp), %esi
	movq	-112(%rbp), %r10
	movq	-120(%rbp), %rdx
	movq	%rax, %r15
	addq	%rax, %r13
	jmp	.L2607
.L2616:
	movabsq	$9223372036854775807, %r13
.L2556:
	movq	%r13, %rdi
	movq	%rdx, -120(%rbp)
	movq	%r10, -112(%rbp)
	movb	%sil, -104(%rbp)
	movq	%rcx, -96(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movzbl	-104(%rbp), %esi
	movq	-112(%rbp), %r10
	movq	-120(%rbp), %rdx
	movq	%rax, %r15
	addq	%rax, %r13
	jmp	.L2608
.L2580:
	movq	%r10, %rsi
	movq	%r15, %rdi
	movq	%r8, -112(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%r10, -96(%rbp)
	call	memmove@PLT
	movq	-96(%rbp), %r10
	movq	-104(%rbp), %rcx
	movq	-112(%rbp), %r8
.L2581:
	movq	%r10, %rdi
	movq	%r8, -104(%rbp)
	movq	%rcx, -96(%rbp)
	call	_ZdlPv@PLT
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %r8
	jmp	.L2583
.L2558:
	movq	%r10, %rsi
	movq	%r15, %rdi
	movq	%rcx, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	%r10, -96(%rbp)
	call	memmove@PLT
	movq	-96(%rbp), %r10
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %rcx
.L2559:
	movq	%r10, %rdi
	movq	%rcx, -104(%rbp)
	movq	%r8, -96(%rbp)
	call	_ZdlPv@PLT
	movq	-96(%rbp), %r8
	movq	-104(%rbp), %rcx
	jmp	.L2561
.L2569:
	movq	%r10, %rsi
	movq	%r15, %rdi
	movq	%rcx, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	%r10, -96(%rbp)
	call	memmove@PLT
	movq	-96(%rbp), %r10
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %rcx
.L2570:
	movq	%r10, %rdi
	movq	%rcx, -104(%rbp)
	movq	%r8, -96(%rbp)
	call	_ZdlPv@PLT
	movq	-96(%rbp), %r8
	movq	-104(%rbp), %rcx
	jmp	.L2572
.L2591:
	movq	%r9, %rsi
	movq	%r15, %rdi
	movq	%rcx, -104(%rbp)
	movq	%r9, -96(%rbp)
	call	memmove@PLT
	movq	-96(%rbp), %r9
	movq	-104(%rbp), %rcx
.L2592:
	movq	%r9, %rdi
	movq	%rcx, -96(%rbp)
	call	_ZdlPv@PLT
	movq	-96(%rbp), %rcx
	jmp	.L2593
.L2685:
	call	__stack_chk_fail@PLT
.L2553:
	leaq	.LC97(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2696:
	movq	%r15, %rcx
	jmp	.L2609
.L2654:
	leaq	.LC101(%rip), %rcx
	movl	$1247, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	call	__assert_fail@PLT
.L2687:
	leaq	.LC25(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4458:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE13HandleString8ENS_4spanIhEE, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE13HandleString8ENS_4spanIhEE
	.section	.text._ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.type	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_, @function
_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_:
.LFB3934:
	.cfi_startproc
	endbr64
	movabsq	$9223372036854775807, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r15
	movq	%rsi, %rax
	subq	%r15, %rax
	cmpq	%rcx, %rax
	je	.L2712
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L2707
	leaq	(%rax,%rax), %r14
	cmpq	%r14, %rax
	jbe	.L2713
.L2709:
	movq	%rcx, %r14
.L2700:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L2706:
	movzbl	0(%r13), %eax
	subq	%r8, %rsi
	leaq	1(%rbx,%rdx), %r10
	movq	%rsi, %r13
	movb	%al, (%rbx,%rdx)
	leaq	(%r10,%rsi), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L2714
	testq	%rsi, %rsi
	jg	.L2702
	testq	%r15, %r15
	jne	.L2705
.L2703:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2714:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L2702
.L2705:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L2703
	.p2align 4,,10
	.p2align 3
.L2713:
	testq	%r14, %r14
	js	.L2709
	jne	.L2700
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L2706
	.p2align 4,,10
	.p2align 3
.L2702:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L2703
	jmp	.L2705
	.p2align 4,,10
	.p2align 3
.L2707:
	movl	$1, %r14d
	jmp	.L2700
.L2712:
	leaq	.LC97(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE3934:
	.size	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_, .-_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleArrayEndEv.str1.8,"aMS",@progbits,1
	.align 8
.LC103:
	.string	"void v8_inspector_protocol_encoding::cbor::{anonymous}::CBOREncoder<C>::HandleArrayEnd() [with C = std::vector<unsigned char>]"
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleArrayEndEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleArrayEndEv, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleArrayEndEv:
.LFB4481:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	je	.L2728
	ret
	.p2align 4,,10
	.p2align 3
.L2728:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L2718
	movb	$-1, (%rsi)
	addq	$1, 8(%rdi)
.L2719:
	movq	24(%rbx), %rax
	cmpq	%rax, 16(%rbx)
	je	.L2729
	movq	-8(%rax), %rsi
	movq	8(%rbx), %rdx
	testq	%rsi, %rsi
	je	.L2730
	movq	8(%rdx), %rdi
	subq	(%rdx), %rdi
	movl	$4294967295, %r8d
	movq	%rdi, %rcx
	subq	%rsi, %rcx
	subq	$4, %rcx
	cmpq	%r8, %rcx
	ja	.L2722
	leaq	1(%rsi), %rdi
	movq	%rcx, %r8
	movq	%rdi, -8(%rax)
	movq	(%rdx), %rdi
	shrq	$24, %r8
	movb	%r8b, (%rdi,%rsi)
	movq	-8(%rax), %rsi
	movq	%rcx, %r8
	shrq	$16, %r8
	leaq	1(%rsi), %rdi
	movq	%rdi, -8(%rax)
	movq	(%rdx), %rdi
	movb	%r8b, (%rdi,%rsi)
	movq	-8(%rax), %rsi
	movq	%rcx, %r8
	shrq	$8, %r8
	leaq	1(%rsi), %rdi
	movq	%rdi, -8(%rax)
	movq	(%rdx), %rdi
	movb	%r8b, (%rdi,%rsi)
	movq	-8(%rax), %rsi
	leaq	1(%rsi), %rdi
	movq	%rdi, -8(%rax)
	movq	(%rdx), %rax
	movb	%cl, (%rax,%rsi)
	subq	$8, 24(%rbx)
.L2715:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2722:
	.cfi_restore_state
	movq	40(%rbx), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jne	.L2715
	movl	$33, (%rax)
	movq	%rdi, 8(%rax)
	movq	8(%rbx), %rax
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	je	.L2715
	movq	%rdx, 8(%rax)
	jmp	.L2715
	.p2align 4,,10
	.p2align 3
.L2718:
	leaq	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L9kStopByteE(%rip), %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L2719
.L2730:
	leaq	.LC91(%rip), %rcx
	movl	$489, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	call	__assert_fail@PLT
.L2729:
	leaq	.LC103(%rip), %rcx
	movl	$557, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC34(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4481:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleArrayEndEv, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleArrayEndEv
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor20EncodeFromLatin1TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_.str1.8,"aMS",@progbits,1
	.align 8
.LC104:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor20EncodeFromLatin1TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_,"axG",@progbits,_ZN30v8_inspector_protocol_encoding4cbor20EncodeFromLatin1TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_,comdat
	.p2align 4
	.weak	_ZN30v8_inspector_protocol_encoding4cbor20EncodeFromLatin1TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_
	.type	_ZN30v8_inspector_protocol_encoding4cbor20EncodeFromLatin1TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_, @function
_ZN30v8_inspector_protocol_encoding4cbor20EncodeFromLatin1TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_:
.LFB3556:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L2732
	xorl	%r13d, %r13d
.L2734:
	cmpb	$0, (%r15,%r13)
	js	.L2733
	addq	$1, %r13
	cmpq	%r13, %r8
	jne	.L2734
.L2732:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZN30v8_inspector_protocol_encoding4cbor17EncodeString8TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_
	.p2align 4,,10
	.p2align 3
.L2731:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2784
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2733:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	testq	%r13, %r13
	js	.L2785
	je	.L2736
	movq	%r13, %rdi
	movq	%r8, -88(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	leaq	(%rax,%r13), %r12
	movq	%rax, -80(%rbp)
	movq	%r12, -64(%rbp)
	call	memcpy@PLT
	movq	-88(%rbp), %r8
	movq	%rax, %rdi
.L2760:
	movq	%r12, -72(%rbp)
	cmpq	%r13, %r8
	jbe	.L2737
	leaq	(%r15,%r13), %rbx
	movq	%r12, %rdx
	leaq	(%r15,%r8), %r13
	jmp	.L2753
	.p2align 4,,10
	.p2align 3
.L2787:
	cmpq	%rdx, %r12
	je	.L2739
	movb	%al, (%r12)
	movq	-72(%rbp), %rax
	leaq	1(%rax), %r12
	movq	%r12, -72(%rbp)
.L2740:
	addq	$1, %rbx
	cmpq	%r13, %rbx
	je	.L2786
.L2752:
	movq	-64(%rbp), %rdx
.L2753:
	movzbl	(%rbx), %eax
	testb	%al, %al
	jns	.L2787
	shrb	$6, %al
	orl	$-64, %eax
	movl	%eax, %r15d
	cmpq	%rdx, %r12
	je	.L2741
	movb	%al, (%r12)
	movq	-72(%rbp), %rax
	movq	-64(%rbp), %r8
	leaq	1(%rax), %r15
	movq	%r15, -72(%rbp)
.L2742:
	movzbl	(%rbx), %eax
	andl	$63, %eax
	orl	$-128, %eax
	movl	%eax, %r12d
	cmpq	%r8, %r15
	je	.L2747
	movb	%al, (%r15)
	movq	-72(%rbp), %rax
	addq	$1, %rbx
	leaq	1(%rax), %r12
	movq	%r12, -72(%rbp)
	cmpq	%r13, %rbx
	jne	.L2752
.L2786:
	movq	-80(%rbp), %rdi
	movq	%r12, %r13
	subq	%rdi, %r13
.L2737:
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN30v8_inspector_protocol_encoding4cbor17EncodeString8TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2731
	call	_ZdlPv@PLT
	jmp	.L2731
	.p2align 4,,10
	.p2align 3
.L2739:
	movq	%r12, %rsi
	leaq	-80(%rbp), %rdi
	movq	%rbx, %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-72(%rbp), %r12
	jmp	.L2740
	.p2align 4,,10
	.p2align 3
.L2747:
	movq	-80(%rbp), %r9
	movq	%r15, %rdx
	movabsq	$9223372036854775807, %rax
	subq	%r9, %rdx
	cmpq	%rax, %rdx
	je	.L2748
	movl	$1, %r15d
	testq	%rdx, %rdx
	je	.L2749
	leaq	(%rdx,%rdx), %r15
	cmpq	%r15, %rdx
	jbe	.L2788
	movabsq	$9223372036854775807, %r15
.L2749:
	movq	%r15, %rdi
	movq	%rdx, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %r9
	addq	%rax, %r15
	movq	%rax, %r8
	testq	%rdx, %rdx
	movb	%r12b, (%rax,%rdx)
	leaq	1(%rax,%rdx), %r12
	jg	.L2789
	testq	%r9, %r9
	jne	.L2750
.L2751:
	movq	%r8, %xmm0
	movq	%r12, %xmm2
	movq	%r15, -64(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	jmp	.L2740
	.p2align 4,,10
	.p2align 3
.L2741:
	movq	-80(%rbp), %r9
	movq	%r12, %rdx
	movabsq	$9223372036854775807, %rax
	subq	%r9, %rdx
	cmpq	%rax, %rdx
	je	.L2748
	movl	$1, %r8d
	testq	%rdx, %rdx
	je	.L2744
	leaq	(%rdx,%rdx), %r8
	cmpq	%r8, %rdx
	jbe	.L2790
	movabsq	$9223372036854775807, %r8
.L2744:
	movq	%r8, %rdi
	movq	%rdx, -104(%rbp)
	movq	%r9, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %rdx
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %r9
	movq	%rax, %r12
	addq	%rax, %r8
	testq	%rdx, %rdx
	movb	%r15b, (%rax,%rdx)
	leaq	1(%rax,%rdx), %r15
	jg	.L2791
	testq	%r9, %r9
	jne	.L2745
.L2746:
	movq	%r12, %xmm0
	movq	%r15, %xmm1
	movq	%r8, -64(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	jmp	.L2742
	.p2align 4,,10
	.p2align 3
.L2789:
	movq	%r9, %rsi
	movq	%r8, %rdi
	movq	%r9, -88(%rbp)
	call	memmove@PLT
	movq	-88(%rbp), %r9
	movq	%rax, %r8
.L2750:
	movq	%r9, %rdi
	movq	%r8, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %r8
	jmp	.L2751
	.p2align 4,,10
	.p2align 3
.L2791:
	movq	%r9, %rsi
	movq	%r12, %rdi
	movq	%r8, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	memmove@PLT
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %r8
.L2745:
	movq	%r9, %rdi
	movq	%r8, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %r8
	jmp	.L2746
	.p2align 4,,10
	.p2align 3
.L2790:
	movabsq	$9223372036854775807, %rax
	testq	%r8, %r8
	cmovs	%rax, %r8
	jmp	.L2744
	.p2align 4,,10
	.p2align 3
.L2788:
	testq	%r15, %r15
	cmovs	%rax, %r15
	jmp	.L2749
	.p2align 4,,10
	.p2align 3
.L2736:
	xorl	%edi, %edi
	xorl	%r12d, %r12d
	jmp	.L2760
.L2748:
	leaq	.LC97(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2784:
	call	__stack_chk_fail@PLT
.L2785:
	leaq	.LC104(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE3556:
	.size	_ZN30v8_inspector_protocol_encoding4cbor20EncodeFromLatin1TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_, .-_ZN30v8_inspector_protocol_encoding4cbor20EncodeFromLatin1TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor16EncodeFromLatin1ENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor16EncodeFromLatin1ENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN30v8_inspector_protocol_encoding4cbor16EncodeFromLatin1ENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN30v8_inspector_protocol_encoding4cbor16EncodeFromLatin1ENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB3163:
	.cfi_startproc
	endbr64
	jmp	_ZN30v8_inspector_protocol_encoding4cbor20EncodeFromLatin1TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS_4spanIhEEPT_
	.cfi_endproc
.LFE3163:
	.size	_ZN30v8_inspector_protocol_encoding4cbor16EncodeFromLatin1ENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN30v8_inspector_protocol_encoding4cbor16EncodeFromLatin1ENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12EncodeDoubleEdPSt6vectorIhSaIhEE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor12EncodeDoubleEdPSt6vectorIhSaIhEE
	.type	_ZN30v8_inspector_protocol_encoding4cbor12EncodeDoubleEdPSt6vectorIhSaIhEE, @function
_ZN30v8_inspector_protocol_encoding4cbor12EncodeDoubleEdPSt6vectorIhSaIhEE:
.LFB3171:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%xmm0, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L2794
	movb	$-5, (%rsi)
	movq	8(%rdi), %rax
	leaq	1(%rax), %r12
	movq	%r12, 8(%rdi)
.L2795:
	movq	%r13, -56(%rbp)
	movl	$56, %r14d
	jmp	.L2802
	.p2align 4,,10
	.p2align 3
.L2816:
	movb	%sil, (%r12)
	movq	8(%rbx), %rax
	subl	$8, %r14d
	leaq	1(%rax), %r12
	movq	%r12, 8(%rbx)
	cmpl	$-8, %r14d
	je	.L2815
.L2802:
	movq	-56(%rbp), %rsi
	movl	%r14d, %ecx
	shrq	%cl, %rsi
	cmpq	%r12, 16(%rbx)
	jne	.L2816
	movabsq	$9223372036854775807, %rax
	movq	(%rbx), %r15
	movq	%r12, %rdx
	subq	%r15, %rdx
	cmpq	%rax, %rdx
	je	.L2817
	movl	$1, %r12d
	testq	%rdx, %rdx
	je	.L2799
	leaq	(%rdx,%rdx), %r12
	cmpq	%r12, %rdx
	jbe	.L2818
	movabsq	$9223372036854775807, %r12
	jmp	.L2799
	.p2align 4,,10
	.p2align 3
.L2818:
	movabsq	$9223372036854775807, %rax
	testq	%r12, %r12
	cmovs	%rax, %r12
.L2799:
	movq	%r12, %rdi
	movq	%rsi, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rsi
	movq	%rax, %r13
	leaq	(%rax,%r12), %rax
	movq	%rax, -64(%rbp)
	leaq	1(%r13,%rdx), %r12
	movb	%sil, 0(%r13,%rdx)
	testq	%rdx, %rdx
	jg	.L2819
	testq	%r15, %r15
	jne	.L2800
.L2801:
	movq	-64(%rbp), %rax
	movq	%r13, %xmm0
	movq	%r12, %xmm1
	subl	$8, %r14d
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 16(%rbx)
	movups	%xmm0, (%rbx)
	cmpl	$-8, %r14d
	jne	.L2802
.L2815:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2819:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	memmove@PLT
.L2800:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L2801
	.p2align 4,,10
	.p2align 3
.L2794:
	leaq	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L21kInitialByteForDoubleE(%rip), %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	8(%rbx), %r12
	jmp	.L2795
.L2817:
	leaq	.LC97(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE3171:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12EncodeDoubleEdPSt6vectorIhSaIhEE, .-_ZN30v8_inspector_protocol_encoding4cbor12EncodeDoubleEdPSt6vectorIhSaIhEE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12EncodeBinaryENS_4spanIhEEPSt6vectorIhSaIhEE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor12EncodeBinaryENS_4spanIhEEPSt6vectorIhSaIhEE
	.type	_ZN30v8_inspector_protocol_encoding4cbor12EncodeBinaryENS_4spanIhEEPSt6vectorIhSaIhEE, @function
_ZN30v8_inspector_protocol_encoding4cbor12EncodeBinaryENS_4spanIhEEPSt6vectorIhSaIhEE:
.LFB3168:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	8(%rdx), %rsi
	cmpq	16(%rdx), %rsi
	je	.L2821
	movb	$-42, (%rsi)
	addq	$1, 8(%rdx)
.L2822:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movl	$2, %edi
	call	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS0_9MajorTypeEmPT_
	testq	%r14, %r14
	je	.L2820
	movq	8(%rbx), %r13
	movq	16(%rbx), %rax
	subq	%r13, %rax
	cmpq	%r14, %rax
	jb	.L2824
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	memmove@PLT
	addq	%r14, 8(%rbx)
.L2820:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2824:
	.cfi_restore_state
	movabsq	$9223372036854775807, %rcx
	movq	(%rbx), %r9
	movq	%r13, %rdx
	movq	%rcx, %rax
	subq	%r9, %rdx
	subq	%rdx, %rax
	cmpq	%r14, %rax
	jb	.L2847
	cmpq	%r14, %rdx
	movq	%r14, %rax
	cmovnb	%rdx, %rax
	addq	%rdx, %rax
	movq	%rax, %r15
	jc	.L2836
	testq	%rax, %rax
	js	.L2836
	jne	.L2828
	movq	$0, -56(%rbp)
	xorl	%r15d, %r15d
.L2834:
	movq	-56(%rbp), %rax
	leaq	(%rax,%rdx), %r11
	leaq	(%r11,%r14), %r10
	testq	%rdx, %rdx
	jne	.L2848
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r10, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %r9
	subq	%r13, %rdx
	leaq	(%r10,%rdx), %r12
	jne	.L2830
.L2832:
	testq	%r9, %r9
	jne	.L2831
.L2833:
	movq	-56(%rbp), %xmm0
	movq	%r12, %xmm1
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2836:
	.cfi_restore_state
	movq	%rcx, %r15
.L2828:
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	(%rbx), %r9
	movq	%r13, %rdx
	movq	%rax, -56(%rbp)
	addq	%rax, %r15
	subq	%r9, %rdx
	jmp	.L2834
	.p2align 4,,10
	.p2align 3
.L2821:
	leaq	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L30kExpectedConversionToBase64TagE(%rip), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L2822
	.p2align 4,,10
	.p2align 3
.L2848:
	movq	%r9, %rsi
	movq	%rax, %rdi
	movq	%r10, -80(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r11, -72(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %r11
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r11, %rdi
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-80(%rbp), %r10
	movq	-64(%rbp), %r9
	subq	%r13, %rdx
	leaq	(%r10,%rdx), %r12
	jne	.L2830
.L2831:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L2833
	.p2align 4,,10
	.p2align 3
.L2830:
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	jmp	.L2832
.L2847:
	leaq	.LC25(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE3168:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12EncodeBinaryENS_4spanIhEEPSt6vectorIhSaIhEE, .-_ZN30v8_inspector_protocol_encoding4cbor12EncodeBinaryENS_4spanIhEEPSt6vectorIhSaIhEE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE10HandleBoolEb,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE10HandleBoolEb, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE10HandleBoolEb:
.LFB4487:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L2849
	cmpb	$1, %sil
	movq	8(%rdi), %rdi
	leaq	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L12kEncodedTrueE(%rip), %rdx
	leaq	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L13kEncodedFalseE(%rip), %rcx
	sbbl	%eax, %eax
	subl	$11, %eax
	testb	%sil, %sil
	movq	8(%rdi), %rsi
	cmove	%rcx, %rdx
	cmpq	16(%rdi), %rsi
	je	.L2852
	movb	%al, (%rsi)
	addq	$1, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L2849:
	ret
	.p2align 4,,10
	.p2align 3
.L2852:
	jmp	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.cfi_endproc
.LFE4487:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE10HandleBoolEb, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE10HandleBoolEb
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE10HandleNullEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE10HandleNullEv, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE10HandleNullEv:
.LFB4488:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L2857
	ret
	.p2align 4,,10
	.p2align 3
.L2857:
	movq	8(%rdi), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L2856
	movb	$-10, (%rsi)
	addq	$1, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L2856:
	leaq	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L12kEncodedNullE(%rip), %rdx
	jmp	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.cfi_endproc
.LFE4488:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE10HandleNullEv, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE10HandleNullEv
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleDoubleEd,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleDoubleEd, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleDoubleEd:
.LFB4485:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L2886
	ret
	.p2align 4,,10
	.p2align 3
.L2886:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%xmm0, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L2860
	movb	$-5, (%rsi)
	movq	8(%r12), %rax
	leaq	1(%rax), %rbx
	movq	%rbx, 8(%r12)
.L2861:
	movq	%r14, -56(%rbp)
	movl	$56, %r15d
	jmp	.L2869
	.p2align 4,,10
	.p2align 3
.L2888:
	movb	%sil, (%rbx)
	movq	8(%r12), %rax
	subl	$8, %r15d
	leaq	1(%rax), %rbx
	movq	%rbx, 8(%r12)
	cmpl	$-8, %r15d
	je	.L2887
.L2869:
	movq	-56(%rbp), %rsi
	movl	%r15d, %ecx
	shrq	%cl, %rsi
	cmpq	%rbx, 16(%r12)
	jne	.L2888
	movabsq	$9223372036854775807, %rax
	movq	(%r12), %r14
	subq	%r14, %rbx
	movq	%rbx, %rdx
	cmpq	%rax, %rbx
	je	.L2889
	movl	$1, %r13d
	testq	%rbx, %rbx
	je	.L2865
	leaq	(%rbx,%rbx), %r13
	cmpq	%r13, %rbx
	jbe	.L2890
	movabsq	$9223372036854775807, %r13
.L2865:
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	%rax, %r8
	addq	%rax, %r13
	movb	%sil, (%rax,%rdx)
	leaq	1(%rax,%rdx), %rbx
	testq	%rdx, %rdx
	jg	.L2891
	testq	%r14, %r14
	jne	.L2866
.L2867:
	movq	%r8, %xmm0
	movq	%rbx, %xmm1
	subl	$8, %r15d
	movq	%r13, 16(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	cmpl	$-8, %r15d
	jne	.L2869
.L2887:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2890:
	.cfi_restore_state
	movabsq	$9223372036854775807, %rax
	testq	%r13, %r13
	cmovs	%rax, %r13
	jmp	.L2865
	.p2align 4,,10
	.p2align 3
.L2891:
	movq	%r8, %rdi
	movq	%r14, %rsi
	call	memmove@PLT
	movq	%rax, %r8
.L2866:
	movq	%r14, %rdi
	movq	%r8, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %r8
	jmp	.L2867
	.p2align 4,,10
	.p2align 3
.L2860:
	leaq	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L21kInitialByteForDoubleE(%rip), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	8(%r12), %rbx
	jmp	.L2861
.L2889:
	leaq	.LC97(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4485:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleDoubleEd, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleDoubleEd
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleMapEndEv.str1.8,"aMS",@progbits,1
	.align 8
.LC105:
	.string	"void v8_inspector_protocol_encoding::cbor::{anonymous}::CBOREncoder<C>::HandleMapEnd() [with C = std::vector<unsigned char>]"
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleMapEndEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleMapEndEv, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleMapEndEv:
.LFB4479:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	je	.L2905
	ret
	.p2align 4,,10
	.p2align 3
.L2905:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L2895
	movb	$-1, (%rsi)
	addq	$1, 8(%rdi)
.L2896:
	movq	24(%rbx), %rax
	cmpq	%rax, 16(%rbx)
	je	.L2906
	movq	-8(%rax), %rsi
	movq	8(%rbx), %rdx
	testq	%rsi, %rsi
	je	.L2907
	movq	8(%rdx), %rdi
	subq	(%rdx), %rdi
	movl	$4294967295, %r8d
	movq	%rdi, %rcx
	subq	%rsi, %rcx
	subq	$4, %rcx
	cmpq	%r8, %rcx
	ja	.L2899
	leaq	1(%rsi), %rdi
	movq	%rcx, %r8
	movq	%rdi, -8(%rax)
	movq	(%rdx), %rdi
	shrq	$24, %r8
	movb	%r8b, (%rdi,%rsi)
	movq	-8(%rax), %rsi
	movq	%rcx, %r8
	shrq	$16, %r8
	leaq	1(%rsi), %rdi
	movq	%rdi, -8(%rax)
	movq	(%rdx), %rdi
	movb	%r8b, (%rdi,%rsi)
	movq	-8(%rax), %rsi
	movq	%rcx, %r8
	shrq	$8, %r8
	leaq	1(%rsi), %rdi
	movq	%rdi, -8(%rax)
	movq	(%rdx), %rdi
	movb	%r8b, (%rdi,%rsi)
	movq	-8(%rax), %rsi
	leaq	1(%rsi), %rdi
	movq	%rdi, -8(%rax)
	movq	(%rdx), %rax
	movb	%cl, (%rax,%rsi)
	subq	$8, 24(%rbx)
.L2892:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2899:
	.cfi_restore_state
	movq	40(%rbx), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jne	.L2892
	movl	$33, (%rax)
	movq	%rdi, 8(%rax)
	movq	8(%rbx), %rax
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	je	.L2892
	movq	%rdx, 8(%rax)
	jmp	.L2892
	.p2align 4,,10
	.p2align 3
.L2895:
	leaq	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L9kStopByteE(%rip), %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L2896
.L2907:
	leaq	.LC91(%rip), %rcx
	movl	$489, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	call	__assert_fail@PLT
.L2906:
	leaq	.LC105(%rip), %rcx
	movl	$536, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC34(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4479:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleMapEndEv, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleMapEndEv
	.section	.rodata._ZNSt6vectorIhSaIhEE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC106:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIhSaIhEE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIhSaIhEE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhSaIhEE17_M_default_appendEm
	.type	_ZNSt6vectorIhSaIhEE17_M_default_appendEm, @function
_ZNSt6vectorIhSaIhEE17_M_default_appendEm:
.LFB3941:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L2928
	movabsq	$9223372036854775807, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$24, %rsp
	movq	8(%rdi), %rcx
	movq	16(%rdi), %rax
	movq	%rcx, %r13
	subq	%rcx, %rax
	subq	(%rdi), %r13
	subq	%r13, %rsi
	cmpq	%rbx, %rax
	jb	.L2910
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%rcx, %rdi
	call	memset@PLT
	addq	%rax, %rbx
	movq	%rbx, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2910:
	.cfi_restore_state
	cmpq	%rbx, %rsi
	jb	.L2931
	cmpq	%r13, %rbx
	movq	%r13, %rax
	cmovnb	%rbx, %rax
	addq	%r13, %rax
	movq	%rax, %r15
	jc	.L2920
	testq	%rax, %rax
	js	.L2920
	jne	.L2914
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
.L2918:
	movq	%rbx, %rdx
	leaq	(%r14,%r13), %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movq	(%r12), %r8
	movq	8(%r12), %rdx
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L2932
	testq	%r8, %r8
	jne	.L2916
.L2917:
	addq	%r13, %rbx
	movq	%r14, (%r12)
	addq	%rbx, %r14
	movq	%r15, 16(%r12)
	movq	%r14, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2928:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L2920:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%rdx, %r15
.L2914:
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%rax, %r14
	addq	%rax, %r15
	jmp	.L2918
	.p2align 4,,10
	.p2align 3
.L2932:
	movq	%r8, %rsi
	movq	%r14, %rdi
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
.L2916:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L2917
.L2931:
	leaq	.LC106(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE3941:
	.size	_ZNSt6vectorIhSaIhEE17_M_default_appendEm, .-_ZNSt6vectorIhSaIhEE17_M_default_appendEm
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder11EncodeStartEPSt6vectorIhSaIhEE.str1.8,"aMS",@progbits,1
	.align 8
.LC107:
	.string	"void v8_inspector_protocol_encoding::cbor::EncodeStartTmpl(C*, size_t*) [with C = std::vector<unsigned char>; size_t = long unsigned int]"
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder11EncodeStartEPSt6vectorIhSaIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder11EncodeStartEPSt6vectorIhSaIhEE
	.type	_ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder11EncodeStartEPSt6vectorIhSaIhEE, @function
_ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder11EncodeStartEPSt6vectorIhSaIhEE:
.LFB3174:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpq	$0, (%rdi)
	jne	.L2942
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	8(%rsi), %rsi
	cmpq	16(%r12), %rsi
	je	.L2935
	movb	$-40, (%rsi)
	movq	8(%r12), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 8(%r12)
.L2936:
	cmpq	16(%r12), %rsi
	je	.L2937
	movb	$90, (%rsi)
	movq	8(%r12), %rax
	addq	$1, %rax
	movq	%rax, 8(%r12)
.L2938:
	subq	(%r12), %rax
	movq	%rax, (%rbx)
	movq	8(%r12), %rcx
	movq	(%r12), %rax
	movq	%rcx, %rdx
	subq	%rax, %rdx
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %rdx
	jb	.L2943
	addq	%rsi, %rax
	cmpq	%rax, %rcx
	je	.L2933
	movq	%rax, 8(%r12)
.L2933:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2943:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	movl	$4, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIhSaIhEE17_M_default_appendEm
	.p2align 4,,10
	.p2align 3
.L2937:
	.cfi_restore_state
	leaq	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L36kInitialByteFor32BitLengthByteStringE(%rip), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	8(%r12), %rax
	jmp	.L2938
	.p2align 4,,10
	.p2align 3
.L2935:
	leaq	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L23kInitialByteForEnvelopeE(%rip), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	8(%r12), %rsi
	jmp	.L2936
.L2942:
	leaq	.LC107(%rip), %rcx
	movl	$472, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC90(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE3174:
	.size	_ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder11EncodeStartEPSt6vectorIhSaIhEE, .-_ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder11EncodeStartEPSt6vectorIhSaIhEE
	.section	.text._ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	.type	_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_, @function
_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_:
.LFB3998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movabsq	$4611686018427387903, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r9
	movq	(%rdi), %r15
	movq	%r9, %rcx
	subq	%r15, %rcx
	movq	%rcx, %rax
	sarq	%rax
	cmpq	%rsi, %rax
	je	.L2958
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L2954
	movabsq	$9223372036854775806, %r14
	cmpq	%rcx, %rax
	jbe	.L2959
.L2946:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L2953:
	movzwl	0(%r13), %eax
	subq	%r8, %r9
	leaq	2(%rbx,%rdx), %r10
	movq	%r9, %r13
	movw	%ax, (%rbx,%rdx)
	leaq	(%r10,%r9), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L2960
	testq	%r9, %r9
	jg	.L2949
	testq	%r15, %r15
	jne	.L2952
.L2950:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2960:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L2949
.L2952:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L2950
	.p2align 4,,10
	.p2align 3
.L2959:
	testq	%rcx, %rcx
	jne	.L2947
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L2953
	.p2align 4,,10
	.p2align 3
.L2949:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L2950
	jmp	.L2952
	.p2align 4,,10
	.p2align 3
.L2954:
	movl	$2, %r14d
	jmp	.L2946
.L2958:
	leaq	.LC97(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2947:
	cmpq	%rsi, %rcx
	cmovbe	%rcx, %rsi
	movq	%rsi, %r14
	addq	%rsi, %r14
	jmp	.L2946
	.cfi_endproc
.LFE3998:
	.size	_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_, .-_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_116ParseUTF16StringEPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE,"ax",@progbits
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_116ParseUTF16StringEPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_116ParseUTF16StringEPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE:
.LFB3219:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$7, 16(%rdi)
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	jne	.L2976
	movq	56(%rdi), %r15
	movq	40(%rdi), %r12
	movq	%rdi, %r13
	movq	%rsi, %r14
	addq	32(%rdi), %r12
	subq	%r15, %r12
	addq	(%rdi), %r12
	testq	%r15, %r15
	je	.L2971
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-82(%rbp), %r8
	xorl	%ebx, %ebx
	jmp	.L2967
	.p2align 4,,10
	.p2align 3
.L2977:
	movw	%ax, (%rsi)
	movq	-72(%rbp), %rax
	addq	$2, %rbx
	leaq	2(%rax), %rsi
	movq	%rsi, -72(%rbp)
	cmpq	%r15, %rbx
	jnb	.L2966
.L2965:
	movq	-64(%rbp), %rcx
.L2967:
	movzbl	1(%r12,%rbx), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	(%r12,%rbx), %eax
	orl	%edx, %eax
	movw	%ax, -82(%rbp)
	cmpq	%rcx, %rsi
	jne	.L2977
	movq	%r8, %rdx
	leaq	-80(%rbp), %rdi
	addq	$2, %rbx
	movq	%r8, -104(%rbp)
	call	_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	cmpq	%r15, %rbx
	movq	-72(%rbp), %rsi
	movq	-104(%rbp), %r8
	jb	.L2965
	.p2align 4,,10
	.p2align 3
.L2966:
	movq	-80(%rbp), %r8
	subq	%r8, %rsi
	sarq	%rsi
	movq	%rsi, %r15
.L2963:
	movq	(%r14), %rax
	movq	%r8, %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	*56(%rax)
	movl	16(%r13), %eax
	testl	%eax, %eax
	je	.L2968
	cmpl	$13, %eax
	je	.L2968
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb
.L2968:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2961
	call	_ZdlPv@PLT
.L2961:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2978
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2971:
	.cfi_restore_state
	xorl	%r8d, %r8d
	jmp	.L2963
.L2978:
	call	__stack_chk_fail@PLT
.L2976:
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer18GetString16WireRepEv.part.0
	.cfi_endproc
.LFE3219:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_116ParseUTF16StringEPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_116ParseUTF16StringEPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_110ParseArrayEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE.str1.8,"aMS",@progbits,1
	.align 8
.LC108:
	.string	"bool v8_inspector_protocol_encoding::cbor::{anonymous}::ParseArray(int32_t, v8_inspector_protocol_encoding::cbor::CBORTokenizer*, v8_inspector_protocol_encoding::StreamingParserHandler*)"
	.align 8
.LC109:
	.string	"tokenizer->TokenTag() == CBORTokenTag::ARRAY_START"
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_110ParseArrayEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE,"ax",@progbits
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_110ParseArrayEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_110ParseArrayEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE:
.LFB3232:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpl	$10, 16(%rsi)
	jne	.L3022
	movq	%rsi, %r12
	movq	%rdx, %r13
	xorl	%esi, %esi
	movl	%edi, %ebx
	movq	%r12, %rdi
	leaq	.L2989(%rip), %r14
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*32(%rax)
	movl	16(%r12), %eax
	cmpl	$11, %eax
	je	.L2982
	.p2align 4,,10
	.p2align 3
.L2981:
	cmpl	$13, %eax
	je	.L3023
	testl	%eax, %eax
	je	.L3024
	cmpl	$300, %ebx
	jg	.L3025
	cmpl	$13, %eax
	ja	.L2987
	movslq	(%r14,%rax,4), %rax
	addq	%r14, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_110ParseArrayEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE,"a",@progbits
	.align 4
	.align 4
.L2989:
	.long	.L2987-.L2989
	.long	.L3000-.L2989
	.long	.L2999-.L2989
	.long	.L2998-.L2989
	.long	.L2997-.L2989
	.long	.L2996-.L2989
	.long	.L2995-.L2989
	.long	.L2994-.L2989
	.long	.L2993-.L2989
	.long	.L2992-.L2989
	.long	.L2991-.L2989
	.long	.L2987-.L2989
	.long	.L2990-.L2989
	.long	.L2988-.L2989
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_110ParseArrayEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE
	.p2align 4,,10
	.p2align 3
.L3025:
	movq	0(%r13), %rax
	movq	32(%r12), %rdx
	movl	$29, %esi
	movq	%r13, %rdi
	call	*104(%rax)
	xorl	%eax, %eax
.L2979:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2988:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	32(%r12), %rdx
	movq	%r13, %rdi
	movl	$25, %esi
	call	*104(%rax)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2990:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r12, %rsi
	movl	%ebx, %edi
	call	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_113ParseEnvelopeEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE
.L3001:
	testb	%al, %al
	je	.L2979
	movl	16(%r12), %eax
.L3002:
	cmpl	$11, %eax
	jne	.L2981
.L2982:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	movl	16(%r12), %edx
	testl	%edx, %edx
	sete	%al
	cmpl	$13, %edx
	sete	%dl
	orb	%dl, %al
	jne	.L2979
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2991:
	.cfi_restore_state
	leal	1(%rbx), %edi
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_110ParseArrayEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE
	jmp	.L3001
.L2992:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	leal	1(%rbx), %r15d
	call	*16(%rax)
	movl	16(%r12), %eax
	testl	%eax, %eax
	je	.L3006
	cmpl	$13, %eax
	je	.L3006
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb
.L3006:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movl	%r15d, %edi
	call	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_18ParseMapEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE.part.0
	jmp	.L3001
.L2993:
	movq	0(%r13), %rax
	movq	56(%r12), %rdx
	movq	%r13, %rdi
	movq	40(%r12), %rsi
	addq	32(%r12), %rsi
	subq	%rdx, %rsi
	addq	(%r12), %rsi
	call	*64(%rax)
	movl	16(%r12), %eax
	testl	%eax, %eax
	je	.L3002
	.p2align 4,,10
	.p2align 3
.L3019:
	cmpl	$13, %eax
	je	.L3002
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb
	movl	16(%r12), %eax
	jmp	.L3002
.L2994:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_116ParseUTF16StringEPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE
	movl	16(%r12), %eax
	jmp	.L3002
.L2995:
	movq	0(%r13), %rax
	movq	56(%r12), %rdx
	movq	%r13, %rdi
	movq	40(%r12), %rsi
	addq	32(%r12), %rsi
	subq	%rdx, %rsi
	addq	(%r12), %rsi
	call	*48(%rax)
	movl	16(%r12), %eax
	testl	%eax, %eax
	jne	.L3019
	jmp	.L3002
.L2996:
	movq	0(%r13), %rax
	movq	8(%r12), %rdi
	movq	(%r12), %rcx
	movq	72(%rax), %rdx
	movq	32(%r12), %rax
	addq	$1, %rax
	subq	%rax, %rdi
	addq	%rax, %rcx
	cmpq	$7, %rdi
	jbe	.L3004
	movq	(%rcx), %rax
	movq	%r13, %rdi
	bswap	%rax
	movq	%rax, %xmm0
	call	*%rdx
	movl	16(%r12), %eax
	testl	%eax, %eax
	jne	.L3019
	jmp	.L3002
.L2997:
	movq	0(%r13), %rax
	movq	80(%rax), %rdx
	movq	56(%r12), %rax
	movl	%eax, %esi
	movl	48(%r12), %eax
	testl	%eax, %eax
	je	.L3003
	notl	%esi
.L3003:
	movq	%r13, %rdi
	call	*%rdx
	movl	16(%r12), %eax
	testl	%eax, %eax
	jne	.L3019
	jmp	.L3002
.L2998:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*96(%rax)
	movl	16(%r12), %eax
	testl	%eax, %eax
	jne	.L3019
	jmp	.L3002
.L2999:
	movq	0(%r13), %rax
	xorl	%esi, %esi
.L3021:
	movq	%r13, %rdi
	call	*88(%rax)
	movl	16(%r12), %eax
	testl	%eax, %eax
	jne	.L3019
	jmp	.L3002
.L3000:
	movq	0(%r13), %rax
	movl	$1, %esi
	jmp	.L3021
.L2987:
	movq	0(%r13), %rax
	movq	32(%r12), %rdx
	movq	%r13, %rdi
	movl	$22, %esi
	call	*104(%rax)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3023:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	32(%r12), %rdx
	movq	%r13, %rdi
	movl	$26, %esi
	call	*104(%rax)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3024:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	32(%r12), %rdx
	movq	%r13, %rdi
	movl	24(%r12), %esi
	call	*104(%rax)
	xorl	%eax, %eax
	jmp	.L2979
.L3022:
	leaq	.LC108(%rip), %rcx
	movl	$1067, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC109(%rip), %rdi
	call	__assert_fail@PLT
.L3004:
	call	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstImEET_NS_4spanIhEE.part.0
	.cfi_endproc
.LFE3232:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_110ParseArrayEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_110ParseArrayEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_18ParseMapEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE.part.0,"ax",@progbits
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_18ParseMapEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE.part.0, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_18ParseMapEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE.part.0:
.LFB4859:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	16(%rsi), %eax
	cmpl	$11, %eax
	je	.L3028
	movl	%edi, %ebx
	leaq	.L3038(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L3027:
	cmpl	$13, %eax
	je	.L3072
	testl	%eax, %eax
	je	.L3050
	cmpl	$6, %eax
	je	.L3073
	cmpl	$7, %eax
	jne	.L3034
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_116ParseUTF16StringEPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE
.L3033:
	cmpl	$300, %ebx
	jg	.L3074
	cmpl	$13, 16(%r12)
	ja	.L3036
	movl	16(%r12), %eax
	movslq	(%r14,%rax,4), %rax
	addq	%r14, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_18ParseMapEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE.part.0,"a",@progbits
	.align 4
	.align 4
.L3038:
	.long	.L3050-.L3038
	.long	.L3049-.L3038
	.long	.L3048-.L3038
	.long	.L3047-.L3038
	.long	.L3046-.L3038
	.long	.L3045-.L3038
	.long	.L3044-.L3038
	.long	.L3043-.L3038
	.long	.L3042-.L3038
	.long	.L3041-.L3038
	.long	.L3040-.L3038
	.long	.L3036-.L3038
	.long	.L3039-.L3038
	.long	.L3037-.L3038
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_18ParseMapEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE.part.0
	.p2align 4,,10
	.p2align 3
.L3050:
	movq	0(%r13), %rax
	movq	32(%r12), %rdx
	movq	%r13, %rdi
	movl	24(%r12), %esi
	call	*104(%rax)
	xorl	%eax, %eax
.L3026:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3074:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	32(%r12), %rdx
	movq	%r13, %rdi
	movl	$29, %esi
	call	*104(%rax)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3073:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	56(%r12), %rdx
	movq	%r13, %rdi
	movq	40(%r12), %rsi
	addq	32(%r12), %rsi
	subq	%rdx, %rsi
	addq	(%r12), %rsi
	call	*48(%rax)
	movl	16(%r12), %eax
	testl	%eax, %eax
	je	.L3033
	cmpl	$13, %eax
	je	.L3033
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb
	jmp	.L3033
	.p2align 4,,10
	.p2align 3
.L3037:
	movq	0(%r13), %rax
	movq	32(%r12), %rdx
	movl	$25, %esi
	movq	%r13, %rdi
	call	*104(%rax)
	xorl	%eax, %eax
	jmp	.L3026
	.p2align 4,,10
	.p2align 3
.L3039:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movl	%ebx, %edi
	call	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_113ParseEnvelopeEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE
.L3051:
	testb	%al, %al
	je	.L3026
	movl	16(%r12), %eax
.L3052:
	cmpl	$11, %eax
	jne	.L3027
.L3028:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movl	16(%r12), %edx
	testl	%edx, %edx
	sete	%al
	cmpl	$13, %edx
	sete	%dl
	orb	%dl, %al
	jne	.L3026
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb
	movl	$1, %eax
	jmp	.L3026
	.p2align 4,,10
	.p2align 3
.L3040:
	leal	1(%rbx), %edi
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_110ParseArrayEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE
	jmp	.L3051
	.p2align 4,,10
	.p2align 3
.L3041:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	leal	1(%rbx), %r15d
	call	*16(%rax)
	movl	16(%r12), %eax
	testl	%eax, %eax
	je	.L3056
	cmpl	$13, %eax
	je	.L3056
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb
.L3056:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movl	%r15d, %edi
	call	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_18ParseMapEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE.part.0
	jmp	.L3051
	.p2align 4,,10
	.p2align 3
.L3042:
	movq	0(%r13), %rax
	movq	56(%r12), %rdx
	movq	%r13, %rdi
	movq	40(%r12), %rsi
	addq	32(%r12), %rsi
	subq	%rdx, %rsi
	addq	(%r12), %rsi
	call	*64(%rax)
	movl	16(%r12), %eax
	testl	%eax, %eax
	je	.L3052
	.p2align 4,,10
	.p2align 3
.L3069:
	cmpl	$13, %eax
	je	.L3052
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb
	movl	16(%r12), %eax
	jmp	.L3052
	.p2align 4,,10
	.p2align 3
.L3043:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_116ParseUTF16StringEPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE
	movl	16(%r12), %eax
	jmp	.L3052
	.p2align 4,,10
	.p2align 3
.L3044:
	movq	0(%r13), %rax
	movq	56(%r12), %rdx
	movq	%r13, %rdi
	movq	40(%r12), %rsi
	addq	32(%r12), %rsi
	subq	%rdx, %rsi
	addq	(%r12), %rsi
	call	*48(%rax)
	movl	16(%r12), %eax
	testl	%eax, %eax
	jne	.L3069
	jmp	.L3052
	.p2align 4,,10
	.p2align 3
.L3045:
	movq	0(%r13), %rax
	movq	8(%r12), %rdi
	movq	(%r12), %rcx
	movq	72(%rax), %rdx
	movq	32(%r12), %rax
	addq	$1, %rax
	subq	%rax, %rdi
	addq	%rax, %rcx
	cmpq	$7, %rdi
	jbe	.L3054
	movq	(%rcx), %rax
	movq	%r13, %rdi
	bswap	%rax
	movq	%rax, %xmm0
	call	*%rdx
	movl	16(%r12), %eax
	testl	%eax, %eax
	jne	.L3069
	jmp	.L3052
	.p2align 4,,10
	.p2align 3
.L3046:
	movq	0(%r13), %rax
	movq	80(%rax), %rdx
	movq	56(%r12), %rax
	movl	%eax, %esi
	movl	48(%r12), %eax
	testl	%eax, %eax
	je	.L3053
	notl	%esi
.L3053:
	movq	%r13, %rdi
	call	*%rdx
	movl	16(%r12), %eax
	testl	%eax, %eax
	jne	.L3069
	jmp	.L3052
	.p2align 4,,10
	.p2align 3
.L3047:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*96(%rax)
	movl	16(%r12), %eax
	testl	%eax, %eax
	jne	.L3069
	jmp	.L3052
	.p2align 4,,10
	.p2align 3
.L3048:
	movq	0(%r13), %rax
	xorl	%esi, %esi
.L3071:
	movq	%r13, %rdi
	call	*88(%rax)
	movl	16(%r12), %eax
	testl	%eax, %eax
	jne	.L3069
	jmp	.L3052
	.p2align 4,,10
	.p2align 3
.L3049:
	movq	0(%r13), %rax
	movl	$1, %esi
	jmp	.L3071
	.p2align 4,,10
	.p2align 3
.L3036:
	movq	0(%r13), %rax
	movq	32(%r12), %rdx
	movl	$22, %esi
	movq	%r13, %rdi
	call	*104(%rax)
	xorl	%eax, %eax
	jmp	.L3026
	.p2align 4,,10
	.p2align 3
.L3072:
	movq	0(%r13), %rax
	movq	32(%r12), %rdx
	movl	$27, %esi
	movq	%r13, %rdi
	call	*104(%rax)
	xorl	%eax, %eax
	jmp	.L3026
	.p2align 4,,10
	.p2align 3
.L3034:
	movq	0(%r13), %rax
	movq	32(%r12), %rdx
	movl	$28, %esi
	movq	%r13, %rdi
	call	*104(%rax)
	xorl	%eax, %eax
	jmp	.L3026
.L3054:
	call	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstImEET_NS_4spanIhEE.part.0
	.cfi_endproc
.LFE4859:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_18ParseMapEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE.part.0, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_18ParseMapEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE.part.0
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_113ParseEnvelopeEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE.str1.8,"aMS",@progbits,1
	.align 8
.LC110:
	.string	"bool v8_inspector_protocol_encoding::cbor::{anonymous}::ParseEnvelope(int32_t, v8_inspector_protocol_encoding::cbor::CBORTokenizer*, v8_inspector_protocol_encoding::StreamingParserHandler*)"
	.align 8
.LC111:
	.string	"tokenizer->TokenTag() == CBORTokenTag::ENVELOPE"
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_113ParseEnvelopeEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE,"ax",@progbits
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_113ParseEnvelopeEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_113ParseEnvelopeEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE:
.LFB3230:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpl	$12, 16(%rsi)
	jne	.L3090
	movq	%rsi, %rbx
	movq	32(%rsi), %r14
	movq	56(%rsi), %r13
	movl	%edi, %r12d
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%rdx, %r15
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb
	movl	16(%rbx), %eax
	cmpl	$9, %eax
	je	.L3077
	cmpl	$10, %eax
	je	.L3078
	testl	%eax, %eax
	je	.L3091
	cmpl	$1, %r12d
	movq	(%r15), %rcx
	movq	32(%rbx), %rdx
	movq	%r15, %rdi
	sbbl	%eax, %eax
	andl	$13, %eax
	leaq	18(%rax), %rsi
	call	*104(%rcx)
	xorl	%eax, %eax
.L3075:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3077:
	.cfi_restore_state
	movq	(%r15), %rax
	movq	%r15, %rdi
	addl	$1, %r12d
	call	*16(%rax)
	movl	16(%rbx), %eax
	testl	%eax, %eax
	je	.L3081
	cmpl	$13, %eax
	je	.L3081
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb
.L3081:
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movl	%r12d, %edi
	call	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_18ParseMapEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE.part.0
	testb	%al, %al
	je	.L3089
.L3082:
	movq	32(%rbx), %rdx
	leaq	6(%r14,%r13), %rcx
	movl	$1, %eax
	cmpq	%rdx, %rcx
	je	.L3075
	movq	(%r15), %rax
	movl	$17, %esi
	movq	%r15, %rdi
	call	*104(%rax)
.L3089:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3091:
	.cfi_restore_state
	movq	(%r15), %rax
	movq	32(%rbx), %rdx
	movq	%r15, %rdi
	movl	24(%rbx), %esi
	call	*104(%rax)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3078:
	.cfi_restore_state
	testl	%r12d, %r12d
	jne	.L3083
	movq	(%r15), %rax
	movq	32(%rbx), %rdx
	movq	%r15, %rdi
	movl	$31, %esi
	call	*104(%rax)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3083:
	.cfi_restore_state
	leal	1(%r12), %edi
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_110ParseArrayEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE
	testb	%al, %al
	jne	.L3082
	jmp	.L3089
.L3090:
	leaq	.LC110(%rip), %rcx
	movl	$959, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC111(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE3230:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_113ParseEnvelopeEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_113ParseEnvelopeEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor9ParseCBORENS_4spanIhEEPNS_22StreamingParserHandlerE.str1.8,"aMS",@progbits,1
	.align 8
.LC112:
	.string	"void v8_inspector_protocol_encoding::cbor::ParseCBOR(v8_inspector_protocol_encoding::span<unsigned char>, v8_inspector_protocol_encoding::StreamingParserHandler*)"
	.align 8
.LC113:
	.string	"tokenizer.TokenTag() == CBORTokenTag::ENVELOPE"
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor9ParseCBORENS_4spanIhEEPNS_22StreamingParserHandlerE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor9ParseCBORENS_4spanIhEEPNS_22StreamingParserHandlerE
	.type	_ZN30v8_inspector_protocol_encoding4cbor9ParseCBORENS_4spanIhEEPNS_22StreamingParserHandlerE, @function
_ZN30v8_inspector_protocol_encoding4cbor9ParseCBORENS_4spanIhEEPNS_22StreamingParserHandlerE:
.LFB3234:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L3108
	cmpb	$-40, (%rdi)
	je	.L3095
	movq	(%rdx), %rax
	movl	$24, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	*104(%rax)
.L3092:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3109
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3095:
	.cfi_restore_state
	leaq	-96(%rbp), %r13
	movq	%rdi, -96(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%r13, %rdi
	xorl	%esi, %esi
	movl	$0, -72(%rbp)
	movq	$-1, -64(%rbp)
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb
	movl	-80(%rbp), %eax
	testl	%eax, %eax
	je	.L3110
	cmpl	$12, %eax
	jne	.L3111
	xorl	%edi, %edi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_113ParseEnvelopeEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE
	testb	%al, %al
	je	.L3092
	movl	-80(%rbp), %eax
	cmpl	$13, %eax
	je	.L3092
	movq	(%r12), %rdx
	movq	104(%rdx), %rcx
	movq	-64(%rbp), %rdx
	testl	%eax, %eax
	je	.L3112
	movl	$30, %esi
	movq	%r12, %rdi
	call	*%rcx
	jmp	.L3092
	.p2align 4,,10
	.p2align 3
.L3108:
	movq	(%rdx), %rax
	movl	$23, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	*104(%rax)
	jmp	.L3092
	.p2align 4,,10
	.p2align 3
.L3110:
	movq	(%r12), %rax
	movl	-72(%rbp), %esi
	movq	%r12, %rdi
	movq	-64(%rbp), %rdx
	call	*104(%rax)
	jmp	.L3092
	.p2align 4,,10
	.p2align 3
.L3112:
	movl	-72(%rbp), %esi
	movq	%r12, %rdi
	call	*%rcx
	jmp	.L3092
.L3109:
	call	__stack_chk_fail@PLT
.L3111:
	leaq	.LC112(%rip), %rcx
	movl	$1145, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC113(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE3234:
	.size	_ZN30v8_inspector_protocol_encoding4cbor9ParseCBORENS_4spanIhEEPNS_22StreamingParserHandlerE, .-_ZN30v8_inspector_protocol_encoding4cbor9ParseCBORENS_4spanIhEEPNS_22StreamingParserHandlerE
	.section	.text._ZN30v8_inspector_protocol_encoding4json17ConvertCBORToJSONERKNS0_8PlatformENS_4spanIhEEPSt6vectorIhSaIhEE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4json17ConvertCBORToJSONERKNS0_8PlatformENS_4spanIhEEPSt6vectorIhSaIhEE
	.type	_ZN30v8_inspector_protocol_encoding4json17ConvertCBORToJSONERKNS0_8PlatformENS_4spanIhEEPSt6vectorIhSaIhEE, @function
_ZN30v8_inspector_protocol_encoding4json17ConvertCBORToJSONERKNS0_8PlatformENS_4spanIhEEPSt6vectorIhSaIhEE:
.LFB3287:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	$112, %edi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -144(%rbp)
	movq	$-1, -136(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %r12
	leaq	16+_ZTVN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEEE(%rip), %rax
	movq	%r13, 16(%r12)
	leaq	32(%r12), %r13
	movq	%rax, (%r12)
	movq	%r13, %rdi
	leaq	-144(%rbp), %rax
	movq	%r15, 8(%r12)
	leaq	-128(%rbp), %r15
	movq	%rax, 24(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	call	_ZNSt11_Deque_baseIN30v8_inspector_protocol_encoding4json12_GLOBAL__N_15StateESaIS3_EE17_M_initialize_mapEm.constprop.0
	movq	24(%r12), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	$0, (%rax)
	movq	$-1, 8(%rax)
	movl	$0, -128(%rbp)
	call	_ZNSt5dequeIN30v8_inspector_protocol_encoding4json12_GLOBAL__N_15StateESaIS3_EE12emplace_backIJNS2_9ContainerEEEEvDpOT_
	testq	%rbx, %rbx
	je	.L3141
	cmpb	$-40, (%r14)
	je	.L3118
	movq	24(%r12), %rax
	movl	$24, (%rax)
.L3138:
	movq	$0, 8(%rax)
	movq	16(%r12), %rax
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	je	.L3117
.L3136:
	movq	%rdx, 8(%rax)
.L3117:
	movq	(%r12), %rax
	movl	-144(%rbp), %ebx
	movq	%r12, %rdi
	movq	-136(%rbp), %r13
	call	*8(%rax)
	movl	%ebx, %eax
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L3142
	addq	$104, %rsp
	movq	%r13, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3118:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%r14, -128(%rbp)
	movq	%rbx, -120(%rbp)
	movl	$0, -104(%rbp)
	movq	$-1, -96(%rbp)
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L3143
	cmpl	$12, %eax
	jne	.L3144
	xorl	%edi, %edi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_113ParseEnvelopeEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE
	testb	%al, %al
	je	.L3117
	movl	-112(%rbp), %eax
	cmpl	$13, %eax
	je	.L3117
	movq	-96(%rbp), %rdx
	testl	%eax, %eax
	je	.L3145
	movq	24(%r12), %rax
	movl	$30, (%rax)
.L3140:
	movq	%rdx, 8(%rax)
	movq	16(%r12), %rax
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jne	.L3136
	jmp	.L3117
	.p2align 4,,10
	.p2align 3
.L3141:
	movq	24(%r12), %rax
	movl	$23, (%rax)
	jmp	.L3138
	.p2align 4,,10
	.p2align 3
.L3143:
	movl	-104(%rbp), %edx
	movq	-96(%rbp), %rcx
	testl	%edx, %edx
	je	.L3124
	movq	24(%r12), %rax
	movl	%edx, (%rax)
	movq	%rcx, 8(%rax)
	movq	16(%r12), %rax
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jne	.L3136
	jmp	.L3117
	.p2align 4,,10
	.p2align 3
.L3145:
	movl	-104(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L3124
	movq	24(%r12), %rax
	movl	%ecx, (%rax)
	jmp	.L3140
.L3142:
	call	__stack_chk_fail@PLT
.L3144:
	leaq	.LC112(%rip), %rcx
	movl	$1145, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC113(%rip), %rdi
	call	__assert_fail@PLT
.L3124:
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleErrorENS_6StatusE.part.0
	.cfi_endproc
.LFE3287:
	.size	_ZN30v8_inspector_protocol_encoding4json17ConvertCBORToJSONERKNS0_8PlatformENS_4spanIhEEPSt6vectorIhSaIhEE, .-_ZN30v8_inspector_protocol_encoding4json17ConvertCBORToJSONERKNS0_8PlatformENS_4spanIhEEPSt6vectorIhSaIhEE
	.section	.text._ZN30v8_inspector_protocol_encoding4json17ConvertCBORToJSONERKNS0_8PlatformENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4json17ConvertCBORToJSONERKNS0_8PlatformENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN30v8_inspector_protocol_encoding4json17ConvertCBORToJSONERKNS0_8PlatformENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN30v8_inspector_protocol_encoding4json17ConvertCBORToJSONERKNS0_8PlatformENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB3288:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	$112, %edi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -144(%rbp)
	movq	$-1, -136(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %r12
	leaq	16+_ZTVN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	movq	%r13, 16(%r12)
	leaq	32(%r12), %r13
	movq	%rax, (%r12)
	movq	%r13, %rdi
	leaq	-144(%rbp), %rax
	movq	%r15, 8(%r12)
	leaq	-128(%rbp), %r15
	movq	%rax, 24(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	call	_ZNSt11_Deque_baseIN30v8_inspector_protocol_encoding4json12_GLOBAL__N_15StateESaIS3_EE17_M_initialize_mapEm.constprop.0
	movq	24(%r12), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	$0, (%rax)
	movq	$-1, 8(%rax)
	movl	$0, -128(%rbp)
	call	_ZNSt5dequeIN30v8_inspector_protocol_encoding4json12_GLOBAL__N_15StateESaIS3_EE12emplace_backIJNS2_9ContainerEEEEvDpOT_
	testq	%rbx, %rbx
	je	.L3169
	cmpb	$-40, (%r14)
	je	.L3149
	movq	24(%r12), %rax
	movl	$24, (%rax)
.L3167:
	movq	$0, 8(%rax)
	movq	16(%r12), %rax
	movq	$0, 8(%rax)
	movq	(%rax), %rax
	movb	$0, (%rax)
.L3155:
	movq	(%r12), %rax
	movl	-144(%rbp), %ebx
	movq	%r12, %rdi
	movq	-136(%rbp), %r13
	call	*8(%rax)
	movl	%ebx, %eax
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L3170
	addq	$104, %rsp
	movq	%r13, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3149:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%r14, -128(%rbp)
	movq	%rbx, -120(%rbp)
	movl	$0, -104(%rbp)
	movq	$-1, -96(%rbp)
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13ReadNextTokenEb
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L3171
	cmpl	$12, %eax
	jne	.L3172
	xorl	%edi, %edi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_113ParseEnvelopeEiPNS0_13CBORTokenizerEPNS_22StreamingParserHandlerE
	testb	%al, %al
	je	.L3155
	movl	-112(%rbp), %eax
	cmpl	$13, %eax
	je	.L3155
	movq	-96(%rbp), %rdx
	testl	%eax, %eax
	je	.L3173
	movq	24(%r12), %rax
	movl	$30, (%rax)
.L3168:
	movq	%rdx, 8(%rax)
	movq	16(%r12), %rax
	movq	$0, 8(%rax)
	movq	(%rax), %rax
	movb	$0, (%rax)
	jmp	.L3155
	.p2align 4,,10
	.p2align 3
.L3169:
	movq	24(%r12), %rax
	movl	$23, (%rax)
	jmp	.L3167
	.p2align 4,,10
	.p2align 3
.L3171:
	movl	-104(%rbp), %edx
	movq	-96(%rbp), %rcx
	testl	%edx, %edx
	je	.L3154
	movq	24(%r12), %rax
	movl	%edx, (%rax)
	movq	%rcx, 8(%rax)
	movq	16(%r12), %rax
	movq	$0, 8(%rax)
	movq	(%rax), %rax
	movb	$0, (%rax)
	jmp	.L3155
	.p2align 4,,10
	.p2align 3
.L3173:
	movl	-104(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L3154
	movq	24(%r12), %rax
	movl	%ecx, (%rax)
	jmp	.L3168
.L3170:
	call	__stack_chk_fail@PLT
.L3172:
	leaq	.LC112(%rip), %rcx
	movl	$1145, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC113(%rip), %rdi
	call	__assert_fail@PLT
.L3154:
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS_6StatusE.part.0
	.cfi_endproc
.LFE3288:
	.size	_ZN30v8_inspector_protocol_encoding4json17ConvertCBORToJSONERKNS0_8PlatformENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN30v8_inspector_protocol_encoding4json17ConvertCBORToJSONERKNS0_8PlatformENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag,"axG",@progbits,_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
	.type	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag, @function
_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag:
.LFB4073:
	.cfi_startproc
	endbr64
	cmpq	%rcx, %rdx
	je	.L3211
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	subq	%rdx, %r14
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %rdi
	movq	16(%rbx), %rax
	subq	%rdi, %rax
	cmpq	%r14, %rax
	jb	.L3177
	movq	%rdi, %r15
	subq	%rsi, %r15
	cmpq	%r15, %r14
	jnb	.L3178
	movq	%rdi, %r15
	movq	%r14, %rdx
	subq	%r14, %r15
	movq	%r15, %rsi
	call	memmove@PLT
	addq	%r14, 8(%rbx)
	movq	%rax, %rdi
	subq	%r13, %r15
	jne	.L3215
.L3179:
	movq	%r14, %rdx
.L3214:
	addq	$40, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	memmove@PLT
	.p2align 4,,10
	.p2align 3
.L3177:
	.cfi_restore_state
	movabsq	$9223372036854775807, %rdx
	movq	(%rbx), %r8
	movq	%rdx, %rax
	subq	%r8, %rdi
	subq	%rdi, %rax
	cmpq	%rax, %r14
	ja	.L3216
	cmpq	%rdi, %r14
	movq	%rdi, %rax
	cmovnb	%r14, %rax
	addq	%rax, %rdi
	movq	%rdi, %r15
	jc	.L3194
	testq	%rdi, %rdi
	js	.L3194
	jne	.L3185
	xorl	%r15d, %r15d
	xorl	%ecx, %ecx
.L3191:
	movq	%r13, %rdx
	subq	%r8, %rdx
	leaq	(%r14,%rdx), %r9
	leaq	(%rcx,%rdx), %r10
	addq	%rcx, %r9
	testq	%rdx, %rdx
	jne	.L3217
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r10, %rdi
	movq	%r9, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-72(%rbp), %r9
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	subq	%r13, %rdx
	leaq	(%r9,%rdx), %r12
	jne	.L3187
.L3189:
	testq	%r8, %r8
	jne	.L3188
.L3190:
	movq	%rcx, %xmm0
	movq	%r12, %xmm1
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
.L3174:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3178:
	.cfi_restore_state
	leaq	(%rdx,%r15), %rsi
	subq	%rsi, %rcx
	jne	.L3218
.L3180:
	subq	%r15, %r14
	addq	%r14, %rdi
	movq	%rdi, 8(%rbx)
	testq	%r15, %r15
	je	.L3174
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	addq	%r15, 8(%rbx)
	movq	%r15, %rdx
	jmp	.L3214
	.p2align 4,,10
	.p2align 3
.L3211:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L3194:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%rdx, %r15
.L3185:
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	(%rbx), %r8
	movq	%rax, %rcx
	addq	%rax, %r15
	jmp	.L3191
	.p2align 4,,10
	.p2align 3
.L3215:
	subq	%r15, %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	jmp	.L3179
	.p2align 4,,10
	.p2align 3
.L3218:
	movq	%rcx, %rdx
	call	memmove@PLT
	movq	8(%rbx), %rdi
	jmp	.L3180
	.p2align 4,,10
	.p2align 3
.L3217:
	movq	%r8, %rsi
	movq	%rcx, %rdi
	movq	%r9, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	movq	%r10, -80(%rbp)
	call	memmove@PLT
	movq	-80(%rbp), %r10
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-72(%rbp), %r9
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	subq	%r13, %rdx
	leaq	(%r9,%rdx), %r12
	jne	.L3187
.L3188:
	movq	%r8, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L3190
	.p2align 4,,10
	.p2align 3
.L3187:
	movq	%r13, %rsi
	movq	%r9, %rdi
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
	jmp	.L3189
.L3216:
	leaq	.LC25(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4073:
	.size	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag, .-_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleBinaryENS_4spanIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleBinaryENS_4spanIhEE, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleBinaryENS_4spanIhEE:
.LFB4484:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L3226
	ret
	.p2align 4,,10
	.p2align 3
.L3226:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %r13
	movq	8(%r13), %rsi
	cmpq	16(%r13), %rsi
	je	.L3221
	movb	$-42, (%rsi)
	addq	$1, 8(%r13)
.L3222:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movl	$2, %edi
	call	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS0_9MajorTypeEmPT_
	movq	8(%r13), %rsi
	addq	$8, %rsp
	movq	%r12, %rdx
	leaq	(%r12,%rbx), %rcx
	movq	%r13, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
	.p2align 4,,10
	.p2align 3
.L3221:
	.cfi_restore_state
	leaq	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L30kExpectedConversionToBase64TagE(%rip), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L3222
	.cfi_endproc
.LFE4484:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleBinaryENS_4spanIhEE, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleBinaryENS_4spanIhEE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE13HandleString8ENS_4spanIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE13HandleString8ENS_4spanIhEE, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE13HandleString8ENS_4spanIhEE:
.LFB4482:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L3232
	ret
	.p2align 4,,10
	.p2align 3
.L3232:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	movq	%rbx, %rsi
	subq	$8, %rsp
	movq	8(%rdi), %r13
	movl	$3, %edi
	movq	%r13, %rdx
	call	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS0_9MajorTypeEmPT_
	movq	8(%r13), %rsi
	addq	$8, %rsp
	movq	%r12, %rdx
	leaq	(%r12,%rbx), %rcx
	movq	%r13, %rdi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
	.cfi_endproc
.LFE4482:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE13HandleString8ENS_4spanIhEE, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE13HandleString8ENS_4spanIhEE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor20EncodeFromLatin1TmplISt6vectorIhSaIhEEEEvNS_4spanIhEEPT_,"axG",@progbits,_ZN30v8_inspector_protocol_encoding4cbor20EncodeFromLatin1TmplISt6vectorIhSaIhEEEEvNS_4spanIhEEPT_,comdat
	.p2align 4
	.weak	_ZN30v8_inspector_protocol_encoding4cbor20EncodeFromLatin1TmplISt6vectorIhSaIhEEEEvNS_4spanIhEEPT_
	.type	_ZN30v8_inspector_protocol_encoding4cbor20EncodeFromLatin1TmplISt6vectorIhSaIhEEEEvNS_4spanIhEEPT_, @function
_ZN30v8_inspector_protocol_encoding4cbor20EncodeFromLatin1TmplISt6vectorIhSaIhEEEEvNS_4spanIhEEPT_:
.LFB3555:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L3234
	xorl	%r13d, %r13d
.L3236:
	cmpb	$0, (%r8,%r13)
	js	.L3235
	addq	$1, %r13
	cmpq	%r13, %r15
	jne	.L3236
.L3234:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movl	$3, %edi
	movq	%r8, -88(%rbp)
	call	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS0_9MajorTypeEmPT_
	movq	-88(%rbp), %r8
	movq	8(%r14), %rsi
	movq	%r14, %rdi
	leaq	(%r8,%r15), %rcx
	movq	%r8, %rdx
	call	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
	.p2align 4,,10
	.p2align 3
.L3233:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3286
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3235:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	testq	%r13, %r13
	js	.L3287
	je	.L3238
	movq	%r13, %rdi
	movq	%r8, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %r8
	movq	%r13, %rdx
	leaq	(%rax,%r13), %r12
	movq	%rax, %rdi
	movq	%rax, -80(%rbp)
	movq	%rax, %rbx
	movq	%r8, %rsi
	movq	%r12, -64(%rbp)
	call	memcpy@PLT
	movq	-88(%rbp), %r8
.L3262:
	movq	%r12, -72(%rbp)
	cmpq	%r13, %r15
	jbe	.L3239
	leaq	(%r8,%r13), %rbx
	movq	%r12, %rdx
	leaq	(%r8,%r15), %r13
	jmp	.L3255
	.p2align 4,,10
	.p2align 3
.L3289:
	cmpq	%rdx, %r12
	je	.L3241
	movb	%al, (%r12)
	movq	-72(%rbp), %rax
	leaq	1(%rax), %r12
	movq	%r12, -72(%rbp)
.L3242:
	addq	$1, %rbx
	cmpq	%r13, %rbx
	je	.L3288
.L3254:
	movq	-64(%rbp), %rdx
.L3255:
	movzbl	(%rbx), %eax
	testb	%al, %al
	jns	.L3289
	shrb	$6, %al
	orl	$-64, %eax
	movl	%eax, %r15d
	cmpq	%rdx, %r12
	je	.L3243
	movb	%al, (%r12)
	movq	-72(%rbp), %rax
	movq	-64(%rbp), %r9
	leaq	1(%rax), %r15
	movq	%r15, -72(%rbp)
.L3244:
	movzbl	(%rbx), %eax
	andl	$63, %eax
	orl	$-128, %eax
	movl	%eax, %r12d
	cmpq	%r15, %r9
	je	.L3249
	movb	%al, (%r15)
	movq	-72(%rbp), %rax
	addq	$1, %rbx
	leaq	1(%rax), %r12
	movq	%r12, -72(%rbp)
	cmpq	%r13, %rbx
	jne	.L3254
.L3288:
	movq	-80(%rbp), %rbx
	movq	%r12, %r13
	subq	%rbx, %r13
.L3239:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movl	$3, %edi
	call	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS0_9MajorTypeEmPT_
	movq	8(%r14), %rsi
	movq	%r14, %rdi
	movq	%r12, %rcx
	movq	%rbx, %rdx
	call	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3233
	call	_ZdlPv@PLT
	jmp	.L3233
	.p2align 4,,10
	.p2align 3
.L3241:
	movq	%r12, %rsi
	leaq	-80(%rbp), %rdi
	movq	%rbx, %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-72(%rbp), %r12
	jmp	.L3242
	.p2align 4,,10
	.p2align 3
.L3249:
	movabsq	$9223372036854775807, %rax
	movq	-80(%rbp), %r10
	subq	%r10, %r9
	cmpq	%rax, %r9
	je	.L3250
	movl	$1, %r15d
	testq	%r9, %r9
	je	.L3251
	leaq	(%r9,%r9), %r15
	cmpq	%r15, %r9
	jbe	.L3290
	movabsq	$9223372036854775807, %r15
.L3251:
	movq	%r15, %rdi
	movq	%r9, -96(%rbp)
	movq	%r10, -88(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %r10
	addq	%rax, %r15
	movq	%rax, %rcx
	testq	%r9, %r9
	movb	%r12b, (%rax,%r9)
	leaq	1(%rax,%r9), %r12
	jg	.L3291
	testq	%r10, %r10
	jne	.L3252
.L3253:
	movq	%rcx, %xmm0
	movq	%r12, %xmm2
	movq	%r15, -64(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	jmp	.L3242
	.p2align 4,,10
	.p2align 3
.L3243:
	movq	-80(%rbp), %r10
	movq	%r12, %rdx
	movabsq	$9223372036854775807, %rax
	subq	%r10, %rdx
	cmpq	%rax, %rdx
	je	.L3250
	movl	$1, %r9d
	testq	%rdx, %rdx
	je	.L3246
	leaq	(%rdx,%rdx), %r9
	cmpq	%r9, %rdx
	jbe	.L3292
	movabsq	$9223372036854775807, %r9
.L3246:
	movq	%r9, %rdi
	movq	%rdx, -104(%rbp)
	movq	%r10, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %rdx
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %r10
	movq	%rax, %r12
	addq	%rax, %r9
	testq	%rdx, %rdx
	movb	%r15b, (%rax,%rdx)
	leaq	1(%rax,%rdx), %r15
	jg	.L3293
	testq	%r10, %r10
	jne	.L3247
.L3248:
	movq	%r12, %xmm0
	movq	%r15, %xmm1
	movq	%r9, -64(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	jmp	.L3244
	.p2align 4,,10
	.p2align 3
.L3291:
	movq	%r10, %rsi
	movq	%rcx, %rdi
	movq	%r9, %rdx
	movq	%r10, -88(%rbp)
	call	memmove@PLT
	movq	-88(%rbp), %r10
	movq	%rax, %rcx
.L3252:
	movq	%r10, %rdi
	movq	%rcx, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %rcx
	jmp	.L3253
	.p2align 4,,10
	.p2align 3
.L3293:
	movq	%r10, %rsi
	movq	%r12, %rdi
	movq	%r9, -96(%rbp)
	movq	%r10, -88(%rbp)
	call	memmove@PLT
	movq	-88(%rbp), %r10
	movq	-96(%rbp), %r9
.L3247:
	movq	%r10, %rdi
	movq	%r9, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %r9
	jmp	.L3248
	.p2align 4,,10
	.p2align 3
.L3292:
	movabsq	$9223372036854775807, %rax
	testq	%r9, %r9
	cmovs	%rax, %r9
	jmp	.L3246
	.p2align 4,,10
	.p2align 3
.L3290:
	testq	%r15, %r15
	cmovs	%rax, %r15
	jmp	.L3251
	.p2align 4,,10
	.p2align 3
.L3238:
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	jmp	.L3262
.L3250:
	leaq	.LC97(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3286:
	call	__stack_chk_fail@PLT
.L3287:
	leaq	.LC104(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE3555:
	.size	_ZN30v8_inspector_protocol_encoding4cbor20EncodeFromLatin1TmplISt6vectorIhSaIhEEEEvNS_4spanIhEEPT_, .-_ZN30v8_inspector_protocol_encoding4cbor20EncodeFromLatin1TmplISt6vectorIhSaIhEEEEvNS_4spanIhEEPT_
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor16EncodeFromLatin1ENS_4spanIhEEPSt6vectorIhSaIhEE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor16EncodeFromLatin1ENS_4spanIhEEPSt6vectorIhSaIhEE
	.type	_ZN30v8_inspector_protocol_encoding4cbor16EncodeFromLatin1ENS_4spanIhEEPSt6vectorIhSaIhEE, @function
_ZN30v8_inspector_protocol_encoding4cbor16EncodeFromLatin1ENS_4spanIhEEPSt6vectorIhSaIhEE:
.LFB3162:
	.cfi_startproc
	endbr64
	jmp	_ZN30v8_inspector_protocol_encoding4cbor20EncodeFromLatin1TmplISt6vectorIhSaIhEEEEvNS_4spanIhEEPT_
	.cfi_endproc
.LFE3162:
	.size	_ZN30v8_inspector_protocol_encoding4cbor16EncodeFromLatin1ENS_4spanIhEEPSt6vectorIhSaIhEE, .-_ZN30v8_inspector_protocol_encoding4cbor16EncodeFromLatin1ENS_4spanIhEEPSt6vectorIhSaIhEE
	.section	.text._ZNSt6vectorIhSaIhEE15_M_range_insertIPKtEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag,"axG",@progbits,_ZNSt6vectorIhSaIhEE15_M_range_insertIPKtEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKtEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
	.type	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKtEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag, @function
_ZNSt6vectorIhSaIhEE15_M_range_insertIPKtEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag:
.LFB4091:
	.cfi_startproc
	endbr64
	cmpq	%rcx, %rdx
	je	.L3540
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r9
	movq	%rdi, %r8
	subq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	sarq	%r13
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	movq	%r13, %r10
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	8(%rdi), %r14
	movq	16(%rdi), %rax
	subq	%r14, %rax
	cmpq	%r13, %rax
	jb	.L3298
	movq	%r14, %rdx
	movq	%r9, -56(%rbp)
	subq	%rsi, %rdx
	cmpq	%rdx, %r13
	jnb	.L3299
	movq	%r14, %r15
	movq	%rdi, -64(%rbp)
	movq	%r13, %rdx
	movq	%r14, %rdi
	subq	%r13, %r15
	movq	%r15, %rsi
	call	memmove@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %r9
	addq	%r13, 8(%r8)
	subq	%r12, %r15
	jne	.L3544
.L3300:
	testq	%r9, %r9
	jle	.L3295
	leaq	(%rbx,%r9), %rax
	cmpq	%rax, %r12
	leaq	(%r12,%r13), %rax
	setnb	%dl
	cmpq	%rax, %rbx
	setnb	%al
	orb	%al, %dl
	je	.L3330
	cmpq	$30, %r9
	jle	.L3330
	movq	%r13, %rdx
	movdqa	.LC96(%rip), %xmm0
	xorl	%eax, %eax
	andq	$-16, %rdx
	.p2align 4,,10
	.p2align 3
.L3304:
	movdqu	(%rbx,%rax,2), %xmm1
	movdqu	16(%rbx,%rax,2), %xmm2
	pand	%xmm0, %xmm1
	pand	%xmm0, %xmm2
	packuswb	%xmm2, %xmm1
	movups	%xmm1, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L3304
	movq	%r13, %rcx
	movq	%r13, %rdx
	andq	$-16, %rcx
	addq	%rcx, %r12
	leaq	(%rbx,%rcx,2), %rax
	subq	%rcx, %rdx
	cmpq	%r13, %rcx
	je	.L3295
	movzwl	(%rax), %ecx
	movb	%cl, (%r12)
	cmpq	$1, %rdx
	je	.L3295
	movzwl	2(%rax), %ecx
	movb	%cl, 1(%r12)
	cmpq	$2, %rdx
	je	.L3295
	movzwl	4(%rax), %ecx
	movb	%cl, 2(%r12)
	cmpq	$3, %rdx
	je	.L3295
	movzwl	6(%rax), %ecx
	movb	%cl, 3(%r12)
	cmpq	$4, %rdx
	je	.L3295
	movzwl	8(%rax), %ecx
	movb	%cl, 4(%r12)
	cmpq	$5, %rdx
	je	.L3295
	movzwl	10(%rax), %ecx
	movb	%cl, 5(%r12)
	cmpq	$6, %rdx
	je	.L3295
	movzwl	12(%rax), %ecx
	movb	%cl, 6(%r12)
	cmpq	$7, %rdx
	je	.L3295
	movzwl	14(%rax), %ecx
	movb	%cl, 7(%r12)
	cmpq	$8, %rdx
	je	.L3295
	movzwl	16(%rax), %ecx
	movb	%cl, 8(%r12)
	cmpq	$9, %rdx
	je	.L3295
	movzwl	18(%rax), %ecx
	movb	%cl, 9(%r12)
	cmpq	$10, %rdx
	je	.L3295
	movzwl	20(%rax), %ecx
	movb	%cl, 10(%r12)
	cmpq	$11, %rdx
	je	.L3295
	movzwl	22(%rax), %ecx
	movb	%cl, 11(%r12)
	cmpq	$12, %rdx
	je	.L3295
	movzwl	24(%rax), %ecx
	movb	%cl, 12(%r12)
	cmpq	$13, %rdx
	je	.L3295
	movzwl	26(%rax), %ecx
	movb	%cl, 13(%r12)
	cmpq	$14, %rdx
	jne	.L3543
.L3295:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3298:
	.cfi_restore_state
	movabsq	$9223372036854775807, %rcx
	movq	(%rdi), %r11
	movq	%r14, %rax
	movq	%rcx, %rdx
	subq	%r11, %rax
	subq	%rax, %rdx
	cmpq	%rdx, %r13
	ja	.L3545
	cmpq	%rax, %r13
	movq	%rax, %rdx
	cmovnb	%r13, %rdx
	addq	%rdx, %rax
	movq	%rax, %r15
	jc	.L3334
	testq	%rax, %rax
	js	.L3334
	jne	.L3321
	xorl	%r15d, %r15d
	xorl	%ecx, %ecx
.L3329:
	movq	%r12, %rdx
	subq	%r11, %rdx
	jne	.L3546
.L3322:
	leaq	(%rcx,%rdx), %rdi
	testq	%r9, %r9
	jle	.L3323
	cmpq	$30, %r9
	jle	.L3335
	movq	%r13, %rdx
	movdqa	.LC96(%rip), %xmm0
	xorl	%eax, %eax
	andq	$-16, %rdx
	.p2align 4,,10
	.p2align 3
.L3325:
	movdqu	(%rbx,%rax,2), %xmm1
	movdqu	16(%rbx,%rax,2), %xmm2
	pand	%xmm0, %xmm1
	pand	%xmm0, %xmm2
	packuswb	%xmm2, %xmm1
	movups	%xmm1, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L3325
	movq	%r13, %rdx
	movq	%r13, %r10
	andq	$-16, %rdx
	andl	$15, %r10d
	leaq	(%rbx,%rdx,2), %rbx
	leaq	(%rdi,%rdx), %rax
	cmpq	%rdx, %r13
	je	.L3326
.L3324:
	movzwl	(%rbx), %edx
	movb	%dl, (%rax)
	cmpq	$1, %r10
	je	.L3326
	movzwl	2(%rbx), %edx
	movb	%dl, 1(%rax)
	cmpq	$2, %r10
	je	.L3326
	movzwl	4(%rbx), %edx
	movb	%dl, 2(%rax)
	cmpq	$3, %r10
	je	.L3326
	movzwl	6(%rbx), %edx
	movb	%dl, 3(%rax)
	cmpq	$4, %r10
	je	.L3326
	movzwl	8(%rbx), %edx
	movb	%dl, 4(%rax)
	cmpq	$5, %r10
	je	.L3326
	movzwl	10(%rbx), %edx
	movb	%dl, 5(%rax)
	cmpq	$6, %r10
	je	.L3326
	movzwl	12(%rbx), %edx
	movb	%dl, 6(%rax)
	cmpq	$7, %r10
	je	.L3326
	movzwl	14(%rbx), %edx
	movb	%dl, 7(%rax)
	cmpq	$8, %r10
	je	.L3326
	movzwl	16(%rbx), %edx
	movb	%dl, 8(%rax)
	cmpq	$9, %r10
	je	.L3326
	movzwl	18(%rbx), %edx
	movb	%dl, 9(%rax)
	cmpq	$10, %r10
	je	.L3326
	movzwl	20(%rbx), %edx
	movb	%dl, 10(%rax)
	cmpq	$11, %r10
	je	.L3326
	movzwl	22(%rbx), %edx
	movb	%dl, 11(%rax)
	cmpq	$12, %r10
	je	.L3326
	movzwl	24(%rbx), %edx
	movb	%dl, 12(%rax)
	cmpq	$13, %r10
	je	.L3326
	movzwl	26(%rbx), %edx
	movb	%dl, 13(%rax)
	cmpq	$14, %r10
	je	.L3326
	movzwl	28(%rbx), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L3326:
	addq	%r13, %rdi
.L3323:
	subq	%r12, %r14
	jne	.L3547
.L3327:
	addq	%rdi, %r14
	testq	%r11, %r11
	je	.L3328
	movq	%r11, %rdi
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
.L3328:
	movq	%r14, %xmm3
	movq	%rcx, %xmm0
	movq	%r15, 16(%r8)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%r8)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3540:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L3299:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	(%rdx,%rdx), %r9
	leaq	(%rbx,%r9), %r15
	subq	%r15, %rcx
	movq	%rcx, %rsi
	sarq	%rsi
	testq	%rcx, %rcx
	jle	.L3307
	leaq	(%rcx,%r9), %rax
	addq	%rbx, %rax
	cmpq	%rax, %r14
	leaq	(%r14,%rsi), %rax
	setnb	%dil
	cmpq	%rax, %r15
	setnb	%al
	orb	%al, %dil
	je	.L3331
	cmpq	$30, %rcx
	jle	.L3331
	movq	%rsi, %rcx
	movdqa	.LC96(%rip), %xmm0
	xorl	%eax, %eax
	andq	$-16, %rcx
	.p2align 4,,10
	.p2align 3
.L3309:
	movdqu	(%r15,%rax,2), %xmm1
	movdqu	16(%r15,%rax,2), %xmm2
	pand	%xmm0, %xmm1
	pand	%xmm0, %xmm2
	packuswb	%xmm2, %xmm1
	movups	%xmm1, (%r14,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L3309
	movq	%rsi, %rdi
	movq	%rsi, %rcx
	andq	$-16, %rdi
	andl	$15, %ecx
	leaq	(%r15,%rdi,2), %rax
	addq	%rdi, %r14
	cmpq	%rdi, %rsi
	je	.L3311
	movzwl	(%rax), %esi
	movb	%sil, (%r14)
	cmpq	$1, %rcx
	je	.L3311
	movzwl	2(%rax), %esi
	movb	%sil, 1(%r14)
	cmpq	$2, %rcx
	je	.L3311
	movzwl	4(%rax), %esi
	movb	%sil, 2(%r14)
	cmpq	$3, %rcx
	je	.L3311
	movzwl	6(%rax), %esi
	movb	%sil, 3(%r14)
	cmpq	$4, %rcx
	je	.L3311
	movzwl	8(%rax), %esi
	movb	%sil, 4(%r14)
	cmpq	$5, %rcx
	je	.L3311
	movzwl	10(%rax), %esi
	movb	%sil, 5(%r14)
	cmpq	$6, %rcx
	je	.L3311
	movzwl	12(%rax), %esi
	movb	%sil, 6(%r14)
	cmpq	$7, %rcx
	je	.L3311
	movzwl	14(%rax), %esi
	movb	%sil, 7(%r14)
	cmpq	$8, %rcx
	je	.L3311
	movzwl	16(%rax), %esi
	movb	%sil, 8(%r14)
	cmpq	$9, %rcx
	je	.L3311
	movzwl	18(%rax), %esi
	movb	%sil, 9(%r14)
	cmpq	$10, %rcx
	je	.L3311
	movzwl	20(%rax), %esi
	movb	%sil, 10(%r14)
	cmpq	$11, %rcx
	je	.L3311
	movzwl	22(%rax), %esi
	movb	%sil, 11(%r14)
	cmpq	$12, %rcx
	je	.L3311
	movzwl	24(%rax), %esi
	movb	%sil, 12(%r14)
	cmpq	$13, %rcx
	je	.L3311
	movzwl	26(%rax), %esi
	movb	%sil, 13(%r14)
	cmpq	$14, %rcx
	je	.L3311
	movzwl	28(%rax), %eax
	movb	%al, 14(%r14)
	.p2align 4,,10
	.p2align 3
.L3311:
	movq	8(%r8), %r14
.L3307:
	subq	%rdx, %r13
	leaq	(%r14,%r13), %rdi
	movq	%rdi, 8(%r8)
	testq	%rdx, %rdx
	jne	.L3548
.L3312:
	addq	%rdx, %rdi
	movq	%r9, %rdx
	movq	%rdi, 8(%r8)
	sarq	%rdx
	testq	%r9, %r9
	jle	.L3295
	cmpq	%r15, %r12
	leaq	(%r12,%rdx), %rax
	setnb	%cl
	cmpq	%rax, %rbx
	setnb	%al
	orb	%al, %cl
	je	.L3332
	cmpq	$30, %r9
	jle	.L3332
	movq	%rdx, %rcx
	movdqa	.LC96(%rip), %xmm0
	xorl	%eax, %eax
	andq	$-16, %rcx
	.p2align 4,,10
	.p2align 3
.L3315:
	movdqu	(%rbx,%rax,2), %xmm1
	movdqu	16(%rbx,%rax,2), %xmm2
	pand	%xmm0, %xmm1
	pand	%xmm0, %xmm2
	packuswb	%xmm2, %xmm1
	movups	%xmm1, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L3315
	movq	%rdx, %rsi
	movq	%rdx, %rcx
	andq	$-16, %rsi
	andl	$15, %ecx
	leaq	(%rbx,%rsi,2), %rax
	addq	%rsi, %r12
	cmpq	%rsi, %rdx
	je	.L3295
	movzwl	(%rax), %edx
	movb	%dl, (%r12)
	cmpq	$1, %rcx
	je	.L3295
	movzwl	2(%rax), %edx
	movb	%dl, 1(%r12)
	cmpq	$2, %rcx
	je	.L3295
	movzwl	4(%rax), %edx
	movb	%dl, 2(%r12)
	cmpq	$3, %rcx
	je	.L3295
	movzwl	6(%rax), %edx
	movb	%dl, 3(%r12)
	cmpq	$4, %rcx
	je	.L3295
	movzwl	8(%rax), %edx
	movb	%dl, 4(%r12)
	cmpq	$5, %rcx
	je	.L3295
	movzwl	10(%rax), %edx
	movb	%dl, 5(%r12)
	cmpq	$6, %rcx
	je	.L3295
	movzwl	12(%rax), %edx
	movb	%dl, 6(%r12)
	cmpq	$7, %rcx
	je	.L3295
	movzwl	14(%rax), %edx
	movb	%dl, 7(%r12)
	cmpq	$8, %rcx
	je	.L3295
	movzwl	16(%rax), %edx
	movb	%dl, 8(%r12)
	cmpq	$9, %rcx
	je	.L3295
	movzwl	18(%rax), %edx
	movb	%dl, 9(%r12)
	cmpq	$10, %rcx
	je	.L3295
	movzwl	20(%rax), %edx
	movb	%dl, 10(%r12)
	cmpq	$11, %rcx
	je	.L3295
	movzwl	22(%rax), %edx
	movb	%dl, 11(%r12)
	cmpq	$12, %rcx
	je	.L3295
	movzwl	24(%rax), %edx
	movb	%dl, 12(%r12)
	cmpq	$13, %rcx
	je	.L3295
	movzwl	26(%rax), %edx
	movb	%dl, 13(%r12)
	cmpq	$14, %rcx
	je	.L3295
.L3543:
	movzwl	28(%rax), %eax
	movb	%al, 14(%r12)
	jmp	.L3295
	.p2align 4,,10
	.p2align 3
.L3334:
	movq	%rcx, %r15
.L3321:
	movq	%r15, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %r8
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r10
	movq	%rax, %rcx
	addq	%rax, %r15
	movq	(%r8), %r11
	movq	8(%r8), %r14
	jmp	.L3329
	.p2align 4,,10
	.p2align 3
.L3548:
	movq	%r12, %rsi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %rdx
	movq	8(%r8), %rdi
	jmp	.L3312
	.p2align 4,,10
	.p2align 3
.L3544:
	movq	%r14, %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	subq	%r15, %rdi
	call	memmove@PLT
	movq	-56(%rbp), %r9
	jmp	.L3300
	.p2align 4,,10
	.p2align 3
.L3547:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r11
	movq	%rax, %rdi
	jmp	.L3327
	.p2align 4,,10
	.p2align 3
.L3546:
	movq	%r11, %rsi
	movq	%rcx, %rdi
	movq	%r8, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memmove@PLT
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	%rax, %rcx
	movq	-56(%rbp), %r11
	jmp	.L3322
	.p2align 4,,10
	.p2align 3
.L3330:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L3302:
	movzwl	(%rbx,%rax,2), %edx
	movb	%dl, (%r12,%rax)
	addq	$1, %rax
	movq	%r13, %rdx
	subq	%rax, %rdx
	testq	%rdx, %rdx
	jg	.L3302
	jmp	.L3295
	.p2align 4,,10
	.p2align 3
.L3331:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L3308:
	movzwl	(%r15,%rax,2), %ecx
	movb	%cl, (%r14,%rax)
	addq	$1, %rax
	movq	%rsi, %rcx
	subq	%rax, %rcx
	testq	%rcx, %rcx
	jg	.L3308
	jmp	.L3311
	.p2align 4,,10
	.p2align 3
.L3332:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L3313:
	movzwl	(%rbx,%rax,2), %ecx
	movb	%cl, (%r12,%rax)
	addq	$1, %rax
	movq	%rdx, %rcx
	subq	%rax, %rcx
	testq	%rcx, %rcx
	jg	.L3313
	jmp	.L3295
.L3335:
	movq	%rdi, %rax
	jmp	.L3324
.L3545:
	leaq	.LC25(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4091:
	.size	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKtEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag, .-_ZNSt6vectorIhSaIhEE15_M_range_insertIPKtEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor19EncodeFromUTF16TmplISt6vectorIhSaIhEEEEvNS_4spanItEEPT_,"axG",@progbits,_ZN30v8_inspector_protocol_encoding4cbor19EncodeFromUTF16TmplISt6vectorIhSaIhEEEEvNS_4spanItEEPT_,comdat
	.p2align 4
	.weak	_ZN30v8_inspector_protocol_encoding4cbor19EncodeFromUTF16TmplISt6vectorIhSaIhEEEEvNS_4spanItEEPT_
	.type	_ZN30v8_inspector_protocol_encoding4cbor19EncodeFromUTF16TmplISt6vectorIhSaIhEEEEvNS_4spanItEEPT_, @function
_ZN30v8_inspector_protocol_encoding4cbor19EncodeFromUTF16TmplISt6vectorIhSaIhEEEEvNS_4spanItEEPT_:
.LFB3557:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	(%rsi,%rsi), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	(%rdi,%r9), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	cmpq	%r14, %rdi
	je	.L3550
	movq	%rdi, %r13
	movq	%rdi, %rax
.L3552:
	cmpw	$127, (%rax)
	ja	.L3551
	addq	$2, %rax
	cmpq	%rax, %r14
	jne	.L3552
.L3550:
	movq	%r15, %rdx
	movl	$3, %edi
	call	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS0_9MajorTypeEmPT_
	movq	8(%r15), %rsi
	addq	$40, %rsp
	movq	%r14, %rcx
	popq	%rbx
	movq	%r12, %rdx
	movq	%r15, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKtEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
	.p2align 4,,10
	.p2align 3
.L3551:
	.cfi_restore_state
	movq	%r15, %rdx
	movq	%r9, %rsi
	movl	$2, %edi
	call	_ZN30v8_inspector_protocol_encoding4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS0_9MajorTypeEmPT_
	movq	8(%r15), %rbx
	jmp	.L3565
	.p2align 4,,10
	.p2align 3
.L3589:
	movb	%cl, (%rbx)
	movq	8(%r15), %rax
	movzbl	%ch, %ebx
	movq	16(%r15), %r11
	leaq	1(%rax), %r12
	movq	%r12, 8(%r15)
	cmpq	%r12, %r11
	je	.L3559
.L3592:
	movb	%bl, (%r12)
	movq	8(%r15), %rax
	addq	$2, %r13
	leaq	1(%rax), %rbx
	movq	%rbx, 8(%r15)
	cmpq	%r13, %r14
	je	.L3588
.L3565:
	movzwl	0(%r13), %ecx
	cmpq	%rbx, 16(%r15)
	jne	.L3589
	movabsq	$9223372036854775807, %rax
	movq	(%r15), %r10
	subq	%r10, %rbx
	cmpq	%rax, %rbx
	je	.L3561
	movl	$1, %r12d
	testq	%rbx, %rbx
	je	.L3556
	leaq	(%rbx,%rbx), %r12
	cmpq	%r12, %rbx
	jbe	.L3590
	movabsq	$9223372036854775807, %r12
	jmp	.L3556
	.p2align 4,,10
	.p2align 3
.L3590:
	movabsq	$9223372036854775807, %rax
	testq	%r12, %r12
	cmovs	%rax, %r12
.L3556:
	movq	%r12, %rdi
	movq	%r10, -64(%rbp)
	movl	%ecx, -56(%rbp)
	call	_Znwm@PLT
	movl	-56(%rbp), %ecx
	testq	%rbx, %rbx
	movq	-64(%rbp), %r10
	leaq	(%rax,%r12), %r11
	movq	%rax, %r9
	leaq	1(%rax,%rbx), %r12
	movb	%cl, (%rax,%rbx)
	jg	.L3591
	testq	%r10, %r10
	jne	.L3557
.L3558:
	movq	%r9, %xmm0
	movq	%r12, %xmm1
	movq	%r11, 16(%r15)
	movzbl	%ch, %ebx
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r15)
	cmpq	%r12, %r11
	jne	.L3592
	.p2align 4,,10
	.p2align 3
.L3559:
	movabsq	$9223372036854775807, %rax
	movq	(%r15), %r9
	subq	%r9, %r11
	cmpq	%rax, %r11
	je	.L3561
	movl	$1, %r12d
	testq	%r11, %r11
	je	.L3562
	leaq	(%r11,%r11), %r12
	cmpq	%r12, %r11
	jbe	.L3593
	movabsq	$9223372036854775807, %r12
	jmp	.L3562
	.p2align 4,,10
	.p2align 3
.L3593:
	testq	%r12, %r12
	cmovs	%rax, %r12
.L3562:
	movq	%r12, %rdi
	movq	%r11, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %r11
	movq	-56(%rbp), %r9
	addq	%rax, %r12
	movq	%rax, %rcx
	testq	%r11, %r11
	movb	%bl, (%rax,%r11)
	leaq	1(%rax,%r11), %rbx
	jg	.L3594
	testq	%r9, %r9
	jne	.L3563
.L3564:
	movq	%rcx, %xmm0
	movq	%rbx, %xmm2
	addq	$2, %r13
	movq	%r12, 16(%r15)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r15)
	cmpq	%r13, %r14
	jne	.L3565
.L3588:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3591:
	.cfi_restore_state
	movq	%r10, %rsi
	movq	%r9, %rdi
	movq	%rbx, %rdx
	movl	%ecx, -72(%rbp)
	movq	%r11, -64(%rbp)
	movq	%r10, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r10
	movq	-64(%rbp), %r11
	movl	-72(%rbp), %ecx
	movq	%rax, %r9
.L3557:
	movq	%r10, %rdi
	movq	%r9, -72(%rbp)
	movl	%ecx, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %r9
	movl	-64(%rbp), %ecx
	movq	-56(%rbp), %r11
	jmp	.L3558
	.p2align 4,,10
	.p2align 3
.L3594:
	movq	%r9, %rsi
	movq	%rcx, %rdi
	movq	%r11, %rdx
	movq	%r9, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r9
	movq	%rax, %rcx
.L3563:
	movq	%r9, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L3564
.L3561:
	leaq	.LC97(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE3557:
	.size	_ZN30v8_inspector_protocol_encoding4cbor19EncodeFromUTF16TmplISt6vectorIhSaIhEEEEvNS_4spanItEEPT_, .-_ZN30v8_inspector_protocol_encoding4cbor19EncodeFromUTF16TmplISt6vectorIhSaIhEEEEvNS_4spanItEEPT_
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor15EncodeFromUTF16ENS_4spanItEEPSt6vectorIhSaIhEE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4cbor15EncodeFromUTF16ENS_4spanItEEPSt6vectorIhSaIhEE
	.type	_ZN30v8_inspector_protocol_encoding4cbor15EncodeFromUTF16ENS_4spanItEEPSt6vectorIhSaIhEE, @function
_ZN30v8_inspector_protocol_encoding4cbor15EncodeFromUTF16ENS_4spanItEEPSt6vectorIhSaIhEE:
.LFB3165:
	.cfi_startproc
	endbr64
	jmp	_ZN30v8_inspector_protocol_encoding4cbor19EncodeFromUTF16TmplISt6vectorIhSaIhEEEEvNS_4spanItEEPT_
	.cfi_endproc
.LFE3165:
	.size	_ZN30v8_inspector_protocol_encoding4cbor15EncodeFromUTF16ENS_4spanItEEPSt6vectorIhSaIhEE, .-_ZN30v8_inspector_protocol_encoding4cbor15EncodeFromUTF16ENS_4spanItEEPSt6vectorIhSaIhEE
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleString16ENS_4spanItEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleString16ENS_4spanItEE, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleString16ENS_4spanItEE:
.LFB4483:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movq	%rsi, %r8
	movq	%rdx, %rsi
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L3598
	ret
	.p2align 4,,10
	.p2align 3
.L3598:
	movq	8(%rdi), %rdx
	movq	%r8, %rdi
	jmp	_ZN30v8_inspector_protocol_encoding4cbor19EncodeFromUTF16TmplISt6vectorIhSaIhEEEEvNS_4spanItEEPT_
	.cfi_endproc
.LFE4483:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleString16ENS_4spanItEE, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleString16ENS_4spanItEE
	.section	.rodata._ZNSt6vectorItSaItEE7reserveEm.str1.1,"aMS",@progbits,1
.LC114:
	.string	"vector::reserve"
	.section	.text._ZNSt6vectorItSaItEE7reserveEm,"axG",@progbits,_ZNSt6vectorItSaItEE7reserveEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorItSaItEE7reserveEm
	.type	_ZNSt6vectorItSaItEE7reserveEm, @function
_ZNSt6vectorItSaItEE7reserveEm:
.LFB4133:
	.cfi_startproc
	endbr64
	movabsq	$4611686018427387903, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	%rax, %rsi
	ja	.L3611
	movq	(%rdi), %r12
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	subq	%r12, %rax
	sarq	%rax
	cmpq	%rax, %rsi
	ja	.L3612
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3612:
	.cfi_restore_state
	movq	8(%rdi), %r13
	leaq	(%rsi,%rsi), %r14
	subq	%r12, %r13
	testq	%rsi, %rsi
	je	.L3606
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	(%rbx), %r12
	movq	8(%rbx), %rdx
	movq	%rax, %r15
	subq	%r12, %rdx
.L3602:
	testq	%rdx, %rdx
	jg	.L3613
	testq	%r12, %r12
	jne	.L3604
.L3605:
	addq	%r15, %r13
	addq	%r15, %r14
	movq	%r15, (%rbx)
	movq	%r13, 8(%rbx)
	movq	%r14, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3613:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	memmove@PLT
.L3604:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	jmp	.L3605
	.p2align 4,,10
	.p2align 3
.L3606:
	movq	%r13, %rdx
	xorl	%r15d, %r15d
	jmp	.L3602
.L3611:
	leaq	.LC114(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4133:
	.size	_ZNSt6vectorItSaItEE7reserveEm, .-_ZNSt6vectorItSaItEE7reserveEm
	.section	.text._ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	.type	_ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_, @function
_ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_:
.LFB4261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movabsq	$4611686018427387903, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r9
	movq	(%rdi), %r15
	movq	%r9, %rcx
	subq	%r15, %rcx
	movq	%rcx, %rax
	sarq	%rax
	cmpq	%rsi, %rax
	je	.L3628
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L3624
	movabsq	$9223372036854775806, %r14
	cmpq	%rcx, %rax
	jbe	.L3629
.L3616:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L3623:
	movzwl	0(%r13), %eax
	subq	%r8, %r9
	leaq	2(%rbx,%rdx), %r10
	movq	%r9, %r13
	movw	%ax, (%rbx,%rdx)
	leaq	(%r10,%r9), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L3630
	testq	%r9, %r9
	jg	.L3619
	testq	%r15, %r15
	jne	.L3622
.L3620:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3630:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L3619
.L3622:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L3620
	.p2align 4,,10
	.p2align 3
.L3629:
	testq	%rcx, %rcx
	jne	.L3617
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L3623
	.p2align 4,,10
	.p2align 3
.L3619:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L3620
	jmp	.L3622
	.p2align 4,,10
	.p2align 3
.L3624:
	movl	$2, %r14d
	jmp	.L3616
.L3628:
	leaq	.LC97(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3617:
	cmpq	%rsi, %rcx
	cmovbe	%rcx, %rsi
	movq	%rsi, %r14
	addq	%rsi, %r14
	jmp	.L3616
	.cfi_endproc
.LFE4261:
	.size	_ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_, .-_ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE12DecodeStringEPKtS5_PSt6vectorItSaItEE.part.0,"ax",@progbits
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE12DecodeStringEPKtS5_PSt6vectorItSaItEE.part.0, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE12DecodeStringEPKtS5_PSt6vectorItSaItEE.part.0:
.LFB4863:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	.L3643(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	subq	%rdi, %rsi
	pushq	%rbx
	sarq	%rsi
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt6vectorItSaItEE7reserveEm
	cmpq	%rbx, %r12
	jbe	.L3634
	.p2align 4,,10
	.p2align 3
.L3632:
	movzwl	(%rbx), %eax
	leaq	2(%rbx), %r15
	movw	%ax, -58(%rbp)
	cmpw	$92, %ax
	jne	.L3640
	cmpq	%r15, %r12
	je	.L3641
	movzwl	2(%rbx), %eax
	leaq	4(%rbx), %r15
	movw	%ax, -58(%rbp)
	cmpw	$120, %ax
	je	.L3641
	cmpw	$34, %ax
	je	.L3640
	leal	-47(%rax), %edx
	cmpw	$71, %dx
	ja	.L3641
	movzwl	%dx, %edx
	movslq	0(%r13,%rdx,4), %rdx
	addq	%r13, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE12DecodeStringEPKtS5_PSt6vectorItSaItEE.part.0,"a",@progbits
	.align 4
	.align 4
.L3643:
	.long	.L3640-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3640-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3649-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3648-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3647-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3641-.L3643
	.long	.L3646-.L3643
	.long	.L3641-.L3643
	.long	.L3645-.L3643
	.long	.L3644-.L3643
	.long	.L3642-.L3643
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE12DecodeStringEPKtS5_PSt6vectorItSaItEE.part.0
.L3644:
	movzwl	4(%rbx), %eax
	leal	-48(%rax), %edx
	cmpw	$9, %dx
	jbe	.L3650
	leal	-65(%rax), %edx
	cmpw	$5, %dx
	jbe	.L3673
	leal	-97(%rax), %edx
	cmpw	$5, %dx
	ja	.L3652
	leal	-87(%rax), %edx
.L3650:
	movzwl	6(%rbx), %ecx
	sall	$12, %edx
	leal	-48(%rcx), %eax
	cmpw	$9, %ax
	jbe	.L3653
.L3679:
	leal	-65(%rcx), %eax
	cmpw	$5, %ax
	jbe	.L3674
	leal	-97(%rcx), %eax
	cmpw	$5, %ax
	ja	.L3652
	leal	-87(%rcx), %eax
.L3653:
	movzwl	8(%rbx), %ecx
	sall	$8, %eax
	addl	%eax, %edx
	leal	-48(%rcx), %eax
	cmpw	$9, %ax
	jbe	.L3655
	leal	-65(%rcx), %eax
	cmpw	$5, %ax
	jbe	.L3675
	leal	-97(%rcx), %eax
	cmpw	$5, %ax
	ja	.L3652
	leal	-87(%rcx), %eax
.L3655:
	sall	$4, %eax
	addl	%edx, %eax
	movzwl	10(%rbx), %edx
	leal	-48(%rdx), %ecx
	cmpw	$9, %cx
	jbe	.L3657
	leal	-65(%rdx), %ecx
	cmpw	$5, %cx
	jbe	.L3676
	leal	-97(%rdx), %ecx
	cmpw	$5, %cx
	ja	.L3652
	leal	-87(%rdx), %ecx
.L3657:
	addl	%ecx, %eax
	leaq	12(%rbx), %r15
	movw	%ax, -58(%rbp)
.L3640:
	movq	8(%r14), %rsi
	cmpq	16(%r14), %rsi
	je	.L3659
.L3678:
	movw	%ax, (%rsi)
	addq	$2, 8(%r14)
.L3660:
	movq	%r15, %rbx
	cmpq	%rbx, %r12
	ja	.L3632
.L3634:
	movl	$1, %eax
	jmp	.L3631
.L3641:
	xorl	%eax, %eax
.L3631:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L3677
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3642:
	.cfi_restore_state
	movl	$11, %eax
	movq	8(%r14), %rsi
	movw	%ax, -58(%rbp)
	movl	$11, %eax
	cmpq	16(%r14), %rsi
	jne	.L3678
	.p2align 4,,10
	.p2align 3
.L3659:
	leaq	-58(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	jmp	.L3660
.L3645:
	movl	$9, %edx
	movl	$9, %eax
	movw	%dx, -58(%rbp)
	jmp	.L3640
.L3646:
	movl	$13, %ecx
	movl	$13, %eax
	movw	%cx, -58(%rbp)
	jmp	.L3640
.L3647:
	movl	$10, %esi
	movl	$10, %eax
	movw	%si, -58(%rbp)
	jmp	.L3640
.L3648:
	movl	$12, %edi
	movl	$12, %eax
	movw	%di, -58(%rbp)
	jmp	.L3640
.L3649:
	movl	$8, %r8d
	movl	$8, %eax
	movw	%r8w, -58(%rbp)
	jmp	.L3640
	.p2align 4,,10
	.p2align 3
.L3676:
	leal	-55(%rdx), %ecx
	leaq	12(%rbx), %r15
	addl	%ecx, %eax
	movw	%ax, -58(%rbp)
	jmp	.L3640
	.p2align 4,,10
	.p2align 3
.L3673:
	movzwl	6(%rbx), %ecx
	leal	-55(%rax), %edx
	sall	$12, %edx
	leal	-48(%rcx), %eax
	cmpw	$9, %ax
	ja	.L3679
	jmp	.L3653
	.p2align 4,,10
	.p2align 3
.L3674:
	leal	-55(%rcx), %eax
	jmp	.L3653
	.p2align 4,,10
	.p2align 3
.L3675:
	leal	-55(%rcx), %eax
	jmp	.L3655
.L3677:
	call	__stack_chk_fail@PLT
.L3652:
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE8HexToIntEt.part.0
	.cfi_endproc
.LFE4863:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE12DecodeStringEPKtS5_PSt6vectorItSaItEE.part.0, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE12DecodeStringEPKtS5_PSt6vectorItSaItEE.part.0
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseValueEPKtS5_PS5_i,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseValueEPKtS5_PS5_i, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseValueEPKtS5_PS5_i:
.LFB3861:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$301, %r8d
	je	.L3803
	movq	%rdx, %rbx
	movl	%r8d, %r12d
	leaq	-136(%rbp), %r14
	movq	$0, -144(%rbp)
	leaq	-144(%rbp), %r15
	movq	%r14, %rcx
	movq	%rbx, %rsi
	movq	$0, -136(%rbp)
	movq	%r15, %rdx
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS5_PS5_S6_
	cmpl	$12, %eax
	ja	.L3684
	leaq	.L3686(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseValueEPKtS5_PS5_i,"a",@progbits
	.align 4
	.align 4
.L3686:
	.long	.L3694-.L3686
	.long	.L3684-.L3686
	.long	.L3693-.L3686
	.long	.L3684-.L3686
	.long	.L3692-.L3686
	.long	.L3691-.L3686
	.long	.L3690-.L3686
	.long	.L3689-.L3686
	.long	.L3688-.L3686
	.long	.L3684-.L3686
	.long	.L3684-.L3686
	.long	.L3687-.L3686
	.long	.L3685-.L3686
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseValueEPKtS5_PS5_i
	.p2align 4,,10
	.p2align 3
.L3803:
	cmpb	$0, 8(%r13)
	je	.L3804
	.p2align 4,,10
	.p2align 3
.L3680:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3805
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3804:
	.cfi_restore_state
	movq	24(%r13), %r8
	subq	0(%r13), %rsi
	sarq	%rsi
	movq	(%r8), %rax
	movq	%rsi, %rdx
	movq	%r8, %rdi
	movl	$2, %esi
	call	*104(%rax)
	movb	$1, 8(%r13)
	jmp	.L3680
	.p2align 4,,10
	.p2align 3
.L3684:
	cmpb	$0, 8(%r13)
	jne	.L3680
	movq	24(%r13), %rdi
	movq	-144(%rbp), %rdx
	movl	$13, %esi
	subq	0(%r13), %rdx
	movq	(%rdi), %rax
	sarq	%rdx
	call	*104(%rax)
	movb	$1, 8(%r13)
	jmp	.L3680
	.p2align 4,,10
	.p2align 3
.L3693:
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-136(%rbp), %r10
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r10, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS5_PS5_S6_
	cmpl	$3, %eax
	je	.L3716
	addl	$1, %r12d
.L3718:
	movl	%r12d, %r8d
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r10, %rsi
	movq	%r13, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseValueEPKtS5_PS5_i
	cmpb	$0, 8(%r13)
	jne	.L3680
	movq	-136(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS5_PS5_S6_
	cmpl	$9, %eax
	je	.L3806
	cmpl	$3, %eax
	jne	.L3807
.L3716:
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
.L3695:
	movq	-136(%rbp), %rax
	cmpq	%rbx, %rax
	jnb	.L3735
	.p2align 4,,10
	.p2align 3
.L3736:
	movzwl	(%rax), %edx
	cmpw	$32, %dx
	je	.L3737
	leal	-9(%rdx), %ecx
	cmpw	$4, %cx
	jbe	.L3737
	cmpw	$47, %dx
	jne	.L3735
	cmpq	%rax, %rbx
	je	.L3735
	leaq	2(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L3735
	movzwl	2(%rax), %edx
	cmpw	$47, %dx
	je	.L3808
	cmpw	$42, %dx
	je	.L3809
	.p2align 4,,10
	.p2align 3
.L3735:
	movq	-152(%rbp), %rcx
	movq	%rax, (%rcx)
	jmp	.L3680
	.p2align 4,,10
	.p2align 3
.L3692:
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	$0, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	leaq	-2(%rax), %rsi
	leaq	2(%rdx), %rdi
	cmpq	%rdi, %rsi
	je	.L3751
	jb	.L3723
	leaq	-128(%rbp), %rdx
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE12DecodeStringEPKtS5_PSt6vectorItSaItEE.part.0
	testb	%al, %al
	je	.L3724
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	subq	%rsi, %rdx
	sarq	%rdx
.L3710:
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	call	*56(%rax)
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3695
	call	_ZdlPv@PLT
	jmp	.L3695
	.p2align 4,,10
	.p2align 3
.L3694:
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-136(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS5_PS5_S6_
	cmpl	$1, %eax
	je	.L3719
.L3797:
	cmpl	$4, %eax
	jne	.L3733
.L3720:
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %rdx
	pxor	%xmm2, %xmm2
	movq	$0, -112(%rbp)
	movaps	%xmm2, -128(%rbp)
	leaq	-2(%rax), %rsi
	leaq	2(%rdx), %rdi
	cmpq	%rdi, %rsi
	je	.L3752
	jnb	.L3810
.L3723:
	cmpb	$0, 8(%r13)
	je	.L3811
.L3726:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3680
	call	_ZdlPv@PLT
	jmp	.L3680
	.p2align 4,,10
	.p2align 3
.L3691:
	leaq	-80(%rbp), %r10
	leaq	-96(%rbp), %rdi
	movb	$0, -80(%rbp)
	xorl	%r12d, %r12d
	movq	-144(%rbp), %r11
	movq	-136(%rbp), %rax
	movq	%r10, -96(%rbp)
	movq	%r10, -168(%rbp)
	subq	%r11, %rax
	movq	%r11, -176(%rbp)
	sarq	%rax
	movq	%rdi, -192(%rbp)
	leaq	1(%rax), %rsi
	movq	%rax, -160(%rbp)
	movq	$0, -88(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %r10
	movl	$15, %r9d
	movq	-176(%rbp), %r11
	testq	%rax, %rax
	je	.L3702
	.p2align 4,,10
	.p2align 3
.L3696:
	movzwl	(%r11,%r12,2), %r14d
	movq	-96(%rbp), %rdi
	testl	$65408, %r14d
	jne	.L3699
	movq	-88(%rbp), %rsi
	cmpq	%r10, %rdi
	movq	%r9, %rdx
	cmovne	-80(%rbp), %rdx
	leaq	1(%rsi), %r15
	cmpq	%rdx, %r15
	ja	.L3812
	movb	%r14b, (%rdi,%rsi)
	movq	-96(%rbp), %rdx
	addq	$1, %r12
	movq	%r15, -88(%rbp)
	movb	$0, 1(%rdx,%rsi)
	cmpq	%r12, %rax
	jne	.L3696
.L3702:
	movq	16(%r13), %rdi
	movq	%r10, -160(%rbp)
	leaq	-128(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-96(%rbp), %rdi
	movq	-160(%rbp), %r10
	movl	%eax, %r12d
	cmpq	%r10, %rdi
	je	.L3698
	call	_ZdlPv@PLT
.L3698:
	testb	%r12b, %r12b
	je	.L3746
	movsd	-128(%rbp), %xmm0
	comisd	.LC115(%rip), %xmm0
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	jb	.L3705
	movsd	.LC116(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L3705
	cvttsd2sil	%xmm0, %esi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L3705
	jne	.L3705
	call	*80(%rax)
	jmp	.L3695
	.p2align 4,,10
	.p2align 3
.L3687:
	cmpb	$0, 8(%r13)
	jne	.L3680
	movq	24(%r13), %rdi
	movq	-144(%rbp), %rdx
	movl	$4, %esi
	subq	0(%r13), %rdx
	movq	(%rdi), %rax
	sarq	%rdx
	call	*104(%rax)
	movb	$1, 8(%r13)
	jmp	.L3680
	.p2align 4,,10
	.p2align 3
.L3685:
	cmpb	$0, 8(%r13)
	jne	.L3680
	movq	24(%r13), %rdi
	movq	-144(%rbp), %rdx
	movl	$3, %esi
	subq	0(%r13), %rdx
	movq	(%rdi), %rax
	sarq	%rdx
	call	*104(%rax)
	movb	$1, 8(%r13)
	jmp	.L3680
	.p2align 4,,10
	.p2align 3
.L3689:
	movq	24(%r13), %rdi
	xorl	%esi, %esi
	movq	(%rdi), %rax
	call	*88(%rax)
	jmp	.L3695
	.p2align 4,,10
	.p2align 3
.L3688:
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	call	*96(%rax)
	jmp	.L3695
	.p2align 4,,10
	.p2align 3
.L3690:
	movq	24(%r13), %rdi
	movl	$1, %esi
	movq	(%rdi), %rax
	call	*88(%rax)
	jmp	.L3695
	.p2align 4,,10
	.p2align 3
.L3737:
	addq	$2, %rax
.L3739:
	cmpq	%rax, %rbx
	ja	.L3736
	jmp	.L3735
	.p2align 4,,10
	.p2align 3
.L3812:
	movq	-192(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	addq	$1, %r12
	movl	$1, %r8d
	movq	%r10, -184(%rbp)
	movq	%rax, -176(%rbp)
	movq	%r11, -168(%rbp)
	movq	%rsi, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-96(%rbp), %rdx
	movq	-160(%rbp), %rsi
	movl	$15, %r9d
	movq	-176(%rbp), %rax
	movq	-168(%rbp), %r11
	movb	%r14b, (%rdx,%rsi)
	movq	-96(%rbp), %rdx
	cmpq	%r12, %rax
	movq	%r15, -88(%rbp)
	movq	-184(%rbp), %r10
	movb	$0, 1(%rdx,%rsi)
	jne	.L3696
	jmp	.L3702
	.p2align 4,,10
	.p2align 3
.L3808:
	addq	$4, %rax
	jmp	.L3800
	.p2align 4,,10
	.p2align 3
.L3813:
	movzwl	(%rax), %edx
	addq	$2, %rax
	cmpw	$10, %dx
	je	.L3739
	cmpw	$13, %dx
	je	.L3739
.L3800:
	cmpq	%rax, %rbx
	ja	.L3813
	movq	%rbx, %rax
	jmp	.L3735
	.p2align 4,,10
	.p2align 3
.L3806:
	movq	-136(%rbp), %r10
	movq	%r15, %rdx
	movq	%r10, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS5_PS5_S6_
	cmpl	$3, %eax
	jne	.L3718
	movq	-144(%rbp), %rdx
	movl	$7, %esi
	movq	%r13, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS_5ErrorEPKt
	jmp	.L3680
	.p2align 4,,10
	.p2align 3
.L3809:
	leaq	4(%rax), %rcx
	cmpq	%rcx, %rbx
	ja	.L3742
	jmp	.L3735
	.p2align 4,,10
	.p2align 3
.L3743:
	movq	%rdx, %rcx
.L3742:
	leaq	2(%rcx), %rdx
	movzwl	-2(%rdx), %esi
	cmpq	%rdx, %rbx
	jbe	.L3735
	cmpw	$42, %si
	jne	.L3743
	cmpw	$47, (%rdx)
	jne	.L3743
	leaq	4(%rcx), %rax
	jmp	.L3739
	.p2align 4,,10
	.p2align 3
.L3811:
	movq	24(%r13), %rdi
	subq	0(%r13), %rdx
	movl	$6, %esi
	sarq	%rdx
	movq	(%rdi), %rax
	call	*104(%rax)
	movb	$1, 8(%r13)
	jmp	.L3726
	.p2align 4,,10
	.p2align 3
.L3810:
	leaq	-128(%rbp), %rdx
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE12DecodeStringEPKtS5_PSt6vectorItSaItEE.part.0
	testb	%al, %al
	je	.L3724
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	subq	%rsi, %rdx
	sarq	%rdx
.L3722:
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	call	*56(%rax)
	movq	-136(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS5_PS5_S6_
	cmpl	$10, %eax
	jne	.L3814
	movq	-136(%rbp), %rsi
	leal	1(%r12), %r8d
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseValueEPKtS5_PS5_i
	cmpb	$0, 8(%r13)
	jne	.L3726
	movq	-136(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS5_PS5_S6_
	cmpl	$9, %eax
	je	.L3815
	cmpl	$1, %eax
	jne	.L3816
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3719
	call	_ZdlPv@PLT
.L3719:
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L3695
	.p2align 4,,10
	.p2align 3
.L3705:
	call	*72(%rax)
	jmp	.L3695
	.p2align 4,,10
	.p2align 3
.L3699:
	cmpq	%r10, %rdi
	je	.L3746
	call	_ZdlPv@PLT
.L3746:
	movq	-144(%rbp), %rdx
	movl	$5, %esi
	movq	%r13, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS_5ErrorEPKt
	jmp	.L3680
	.p2align 4,,10
	.p2align 3
.L3752:
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	.L3722
	.p2align 4,,10
	.p2align 3
.L3807:
	movq	-144(%rbp), %rdx
	movl	$8, %esi
	movq	%r13, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS_5ErrorEPKt
	jmp	.L3680
	.p2align 4,,10
	.p2align 3
.L3815:
	movq	-136(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS5_PS5_S6_
	cmpl	$1, %eax
	je	.L3817
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3797
	movl	%eax, -160(%rbp)
	call	_ZdlPv@PLT
	movl	-160(%rbp), %eax
	cmpl	$4, %eax
	je	.L3720
.L3733:
	movq	-144(%rbp), %rdx
	movl	$9, %esi
	movq	%r13, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS_5ErrorEPKt
	jmp	.L3680
	.p2align 4,,10
	.p2align 3
.L3751:
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	.L3710
	.p2align 4,,10
	.p2align 3
.L3724:
	movq	-144(%rbp), %rdx
	jmp	.L3723
.L3814:
	movq	-144(%rbp), %rdx
	movl	$10, %esi
	movq	%r13, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS_5ErrorEPKt
	jmp	.L3726
.L3816:
	movq	-144(%rbp), %rdx
	movl	$12, %esi
	movq	%r13, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS_5ErrorEPKt
	jmp	.L3726
.L3817:
	movq	-144(%rbp), %rdx
	movl	$11, %esi
	movq	%r13, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS_5ErrorEPKt
	jmp	.L3726
.L3805:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3861:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseValueEPKtS5_PS5_i, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseValueEPKtS5_PS5_i
	.section	.text._ZN30v8_inspector_protocol_encoding4json9ParseJSONERKNS0_8PlatformENS_4spanItEEPNS_22StreamingParserHandlerE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4json9ParseJSONERKNS0_8PlatformENS_4spanItEEPNS_22StreamingParserHandlerE
	.type	_ZN30v8_inspector_protocol_encoding4json9ParseJSONERKNS0_8PlatformENS_4spanItEEPNS_22StreamingParserHandlerE, @function
_ZN30v8_inspector_protocol_encoding4json9ParseJSONERKNS0_8PlatformENS_4spanItEEPNS_22StreamingParserHandlerE:
.LFB3285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm1
	movq	%rdi, %xmm0
	xorl	%r8d, %r8d
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	(%rsi,%rdx,2), %rbx
	leaq	-72(%rbp), %rcx
	leaq	-64(%rbp), %rdi
	movq	%rbx, %rdx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$0, -56(%rbp)
	movq	%rsi, -64(%rbp)
	movq	$0, -72(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseValueEPKtS5_PS5_i
	cmpb	$0, -56(%rbp)
	jne	.L3818
	movq	-72(%rbp), %rdx
	cmpq	%rdx, %rbx
	je	.L3818
	movq	-40(%rbp), %rdi
	subq	-64(%rbp), %rdx
	movl	$1, %esi
	sarq	%rdx
	movq	(%rdi), %rax
	call	*104(%rax)
.L3818:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3823
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3823:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3285:
	.size	_ZN30v8_inspector_protocol_encoding4json9ParseJSONERKNS0_8PlatformENS_4spanItEEPNS_22StreamingParserHandlerE, .-_ZN30v8_inspector_protocol_encoding4json9ParseJSONERKNS0_8PlatformENS_4spanItEEPNS_22StreamingParserHandlerE
	.section	.text._ZN30v8_inspector_protocol_encoding4json17ConvertJSONToCBORERKNS0_8PlatformENS_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4json17ConvertJSONToCBORERKNS0_8PlatformENS_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN30v8_inspector_protocol_encoding4json17ConvertJSONToCBORERKNS0_8PlatformENS_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN30v8_inspector_protocol_encoding4json17ConvertJSONToCBORERKNS0_8PlatformENS_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB3291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	leaq	0(%r13,%rbx,2), %rbx
	subq	$96, %rsp
	movq	%rdi, -120(%rbp)
	movl	$48, %edi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -96(%rbp)
	movq	$-1, -88(%rbp)
	call	_Znwm@PLT
	xorl	%r8d, %r8d
	leaq	-104(%rbp), %rcx
	leaq	-80(%rbp), %rdi
	movq	%rax, %r12
	movq	-120(%rbp), %xmm0
	movq	%rbx, %rdx
	movq	%r13, %rsi
	leaq	16+_ZTVN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	movq	%r12, %xmm1
	movq	%r14, 8(%r12)
	movq	%rax, (%r12)
	punpcklqdq	%xmm1, %xmm0
	leaq	-96(%rbp), %rax
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	movq	%rax, 40(%r12)
	movl	$0, -96(%rbp)
	movq	$-1, -88(%rbp)
	movb	$0, -72(%rbp)
	movq	%r13, -80(%rbp)
	movq	$0, -104(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseValueEPKtS5_PS5_i
	cmpb	$0, -72(%rbp)
	jne	.L3826
	movq	-104(%rbp), %rdx
	cmpq	%rdx, %rbx
	je	.L3826
	movq	-56(%rbp), %rdi
	subq	-80(%rbp), %rdx
	movl	$1, %esi
	sarq	%rdx
	movq	(%rdi), %rax
	call	*104(%rax)
.L3826:
	movq	(%r12), %rax
	movl	-96(%rbp), %ebx
	movq	%r12, %rdi
	movq	-88(%rbp), %r13
	call	*8(%rax)
	movl	%ebx, %eax
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L3830
	addq	$96, %rsp
	movq	%r13, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3830:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3291:
	.size	_ZN30v8_inspector_protocol_encoding4json17ConvertJSONToCBORERKNS0_8PlatformENS_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN30v8_inspector_protocol_encoding4json17ConvertJSONToCBORERKNS0_8PlatformENS_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN30v8_inspector_protocol_encoding4json17ConvertJSONToCBORERKNS0_8PlatformENS_4spanItEEPSt6vectorIhSaIhEE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4json17ConvertJSONToCBORERKNS0_8PlatformENS_4spanItEEPSt6vectorIhSaIhEE
	.type	_ZN30v8_inspector_protocol_encoding4json17ConvertJSONToCBORERKNS0_8PlatformENS_4spanItEEPSt6vectorIhSaIhEE, @function
_ZN30v8_inspector_protocol_encoding4json17ConvertJSONToCBORERKNS0_8PlatformENS_4spanItEEPSt6vectorIhSaIhEE:
.LFB3293:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	leaq	0(%r13,%rbx,2), %rbx
	subq	$96, %rsp
	movq	%rdi, -120(%rbp)
	movl	$48, %edi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -96(%rbp)
	movq	$-1, -88(%rbp)
	call	_Znwm@PLT
	xorl	%r8d, %r8d
	leaq	-104(%rbp), %rcx
	leaq	-80(%rbp), %rdi
	movq	%rax, %r12
	movq	-120(%rbp), %xmm0
	movq	%rbx, %rdx
	movq	%r13, %rsi
	leaq	16+_ZTVN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEEE(%rip), %rax
	movq	%r12, %xmm1
	movq	%r14, 8(%r12)
	movq	%rax, (%r12)
	punpcklqdq	%xmm1, %xmm0
	leaq	-96(%rbp), %rax
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	movq	%rax, 40(%r12)
	movl	$0, -96(%rbp)
	movq	$-1, -88(%rbp)
	movb	$0, -72(%rbp)
	movq	%r13, -80(%rbp)
	movq	$0, -104(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserItE10ParseValueEPKtS5_PS5_i
	cmpb	$0, -72(%rbp)
	jne	.L3833
	movq	-104(%rbp), %rdx
	cmpq	%rdx, %rbx
	je	.L3833
	movq	-56(%rbp), %rdi
	subq	-80(%rbp), %rdx
	movl	$1, %esi
	sarq	%rdx
	movq	(%rdi), %rax
	call	*104(%rax)
.L3833:
	movq	(%r12), %rax
	movl	-96(%rbp), %ebx
	movq	%r12, %rdi
	movq	-88(%rbp), %r13
	call	*8(%rax)
	movl	%ebx, %eax
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L3837
	addq	$96, %rsp
	movq	%r13, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3837:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3293:
	.size	_ZN30v8_inspector_protocol_encoding4json17ConvertJSONToCBORERKNS0_8PlatformENS_4spanItEEPSt6vectorIhSaIhEE, .-_ZN30v8_inspector_protocol_encoding4json17ConvertJSONToCBORERKNS0_8PlatformENS_4spanItEEPSt6vectorIhSaIhEE
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE12DecodeStringEPKhS5_PSt6vectorItSaItEE.part.0,"ax",@progbits
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE12DecodeStringEPKhS5_PSt6vectorItSaItEE.part.0, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE12DecodeStringEPKhS5_PSt6vectorItSaItEE.part.0:
.LFB4862:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	subq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt6vectorItSaItEE7reserveEm
	cmpq	%r12, %r13
	jbe	.L3839
	leaq	.L3862(%rip), %rbx
.L3840:
	movzbl	(%r12), %edx
	leaq	1(%r12), %r15
	movw	%dx, -60(%rbp)
	movl	%edx, %eax
	cmpw	$127, %dx
	jbe	.L3841
	andl	$-32, %edx
	cmpb	$-64, %dl
	je	.L3903
	movl	%eax, %edx
	andl	$-16, %edx
	cmpb	$-32, %dl
	je	.L3904
	movl	%eax, %edx
	andl	$-8, %edx
	cmpb	$-16, %dl
	jne	.L3846
	andl	$7, %eax
	movl	$3, %edx
	movl	$3, %ecx
.L3843:
	addq	%r15, %rdx
	cmpq	%rdx, %r13
	jb	.L3846
	movzbl	1(%r12), %edi
	leaq	2(%r12), %r15
	subl	$1, %ecx
	movl	%edi, %esi
	movl	%edi, %edx
	andl	$-64, %esi
	cmpb	$-128, %sil
	jne	.L3846
	sall	$6, %eax
	andl	$63, %edx
	orl	%edx, %eax
	testl	%ecx, %ecx
	je	.L3848
	movzbl	2(%r12), %edi
	leaq	3(%r12), %r15
	movl	%edi, %esi
	movl	%edi, %edx
	andl	$-64, %esi
	cmpb	$-128, %sil
	jne	.L3846
	sall	$6, %eax
	andl	$63, %edx
	orl	%edx, %eax
	cmpl	$1, %ecx
	je	.L3849
	movzbl	3(%r12), %edi
	leaq	4(%r12), %r15
	movl	%edi, %ecx
	movl	%edi, %edx
	andl	$-64, %ecx
	cmpb	$-128, %cl
	jne	.L3846
	sall	$6, %eax
	andl	$63, %edx
	orl	%edx, %eax
.L3849:
	leal	-128(%rax), %edx
	movw	%di, -60(%rbp)
	cmpl	$1113983, %edx
	ja	.L3846
	movq	8(%r14), %rsi
	movq	16(%r14), %r8
	cmpl	$65534, %eax
	jbe	.L3886
	leal	-65536(%rax), %r12d
	movl	%r12d, %eax
	shrl	$10, %eax
	subw	$10240, %ax
	movw	%ax, -58(%rbp)
	cmpq	%rsi, %r8
	je	.L3854
	movw	%ax, (%rsi)
	movq	8(%r14), %rax
	leaq	2(%rax), %rsi
	movq	%rsi, 8(%r14)
.L3855:
	movl	%r12d, %eax
	andw	$1023, %ax
	subw	$9216, %ax
	movw	%ax, -58(%rbp)
	cmpq	16(%r14), %rsi
	je	.L3856
.L3899:
	movw	%ax, (%rsi)
	addq	$2, 8(%r14)
	jmp	.L3898
	.p2align 4,,10
	.p2align 3
.L3841:
	cmpw	$92, %dx
	jne	.L3902
	cmpq	%r15, %r13
	je	.L3846
	movzbl	1(%r12), %edx
	leaq	2(%r12), %r15
	movw	%dx, -60(%rbp)
	movl	%edx, %eax
	cmpw	$120, %dx
	je	.L3846
	cmpb	$34, %dl
	je	.L3889
	subl	$47, %eax
	cmpb	$71, %al
	ja	.L3846
	movzbl	%al, %eax
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE12DecodeStringEPKhS5_PSt6vectorItSaItEE.part.0,"a",@progbits
	.align 4
	.align 4
.L3862:
	.long	.L3902-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3902-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3868-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3867-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3866-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3846-.L3862
	.long	.L3865-.L3862
	.long	.L3846-.L3862
	.long	.L3864-.L3862
	.long	.L3863-.L3862
	.long	.L3861-.L3862
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE12DecodeStringEPKhS5_PSt6vectorItSaItEE.part.0
.L3863:
	movzbl	2(%r12), %eax
	leal	-48(%rax), %ecx
	movl	%eax, %edx
	cmpb	$9, %cl
	jbe	.L3905
	leal	-65(%rax), %ecx
	cmpb	$5, %cl
	jbe	.L3906
	subl	$97, %edx
	cmpb	$5, %dl
	ja	.L3872
	subl	$87, %eax
.L3870:
	movl	%eax, %edx
	movzbl	3(%r12), %eax
	sall	$12, %edx
	leal	-48(%rax), %esi
	movl	%eax, %ecx
	cmpb	$9, %sil
	jbe	.L3907
	leal	-65(%rax), %esi
	cmpb	$5, %sil
	jbe	.L3908
	subl	$97, %ecx
	cmpb	$5, %cl
	ja	.L3872
	subl	$87, %eax
.L3874:
	sall	$8, %eax
	addl	%edx, %eax
	movzbl	4(%r12), %edx
	leal	-48(%rdx), %esi
	movl	%edx, %ecx
	cmpb	$9, %sil
	jbe	.L3909
	leal	-65(%rdx), %esi
	cmpb	$5, %sil
	jbe	.L3910
	subl	$97, %ecx
	cmpb	$5, %cl
	ja	.L3872
	subl	$87, %edx
.L3877:
	movzbl	5(%r12), %ecx
	sall	$4, %edx
	addl	%eax, %edx
	leal	-48(%rcx), %esi
	movl	%ecx, %eax
	cmpb	$9, %sil
	jbe	.L3911
	leal	-65(%rcx), %esi
	cmpb	$5, %sil
	jbe	.L3912
	subl	$97, %eax
	cmpb	$5, %al
	ja	.L3872
	leal	-87(%rcx), %eax
.L3880:
	addl	%eax, %edx
	leaq	6(%r12), %r15
	movw	%dx, -60(%rbp)
.L3902:
	movq	8(%r14), %rsi
	cmpq	16(%r14), %rsi
	je	.L3882
	movw	%dx, (%rsi)
	addq	$2, 8(%r14)
.L3898:
	movq	%r15, %r12
	cmpq	%r12, %r13
	ja	.L3840
.L3839:
	movl	$1, %eax
	jmp	.L3838
.L3846:
	xorl	%eax, %eax
.L3838:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L3913
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3848:
	.cfi_restore_state
	leal	-128(%rax), %edx
	movw	%di, -60(%rbp)
	cmpl	$1113983, %edx
	ja	.L3846
	movq	8(%r14), %rsi
	movq	16(%r14), %r8
.L3886:
	movw	%ax, -58(%rbp)
	cmpq	%r8, %rsi
	jne	.L3899
.L3856:
	leaq	-58(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	jmp	.L3898
	.p2align 4,,10
	.p2align 3
.L3904:
	andl	$15, %eax
	movl	$2, %edx
	movl	$2, %ecx
	jmp	.L3843
	.p2align 4,,10
	.p2align 3
.L3903:
	andl	$31, %eax
	movl	$1, %edx
	movl	$1, %ecx
	jmp	.L3843
	.p2align 4,,10
	.p2align 3
.L3882:
	leaq	-60(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	jmp	.L3898
.L3861:
	movl	$11, %eax
	movl	$11, %edx
	movw	%ax, -60(%rbp)
	jmp	.L3902
.L3868:
	movl	$8, %r8d
	movl	$8, %edx
	movw	%r8w, -60(%rbp)
	jmp	.L3902
.L3867:
	movl	$12, %edi
	movl	$12, %edx
	movw	%di, -60(%rbp)
	jmp	.L3902
.L3866:
	movl	$10, %esi
	movl	$10, %edx
	movw	%si, -60(%rbp)
	jmp	.L3902
.L3865:
	movl	$13, %ecx
	movl	$13, %edx
	movw	%cx, -60(%rbp)
	jmp	.L3902
.L3864:
	movl	$9, %edx
	movw	%dx, -60(%rbp)
	movl	$9, %edx
	jmp	.L3902
.L3854:
	movq	%r8, %rsi
	leaq	-58(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	movq	8(%r14), %rsi
	jmp	.L3855
.L3906:
	subl	$55, %eax
	jmp	.L3870
.L3889:
	movl	$34, %edx
	jmp	.L3902
.L3905:
	subl	$48, %eax
	jmp	.L3870
.L3911:
	leal	-48(%rcx), %eax
	jmp	.L3880
.L3909:
	subl	$48, %edx
	jmp	.L3877
.L3907:
	subl	$48, %eax
	jmp	.L3874
.L3912:
	leal	-55(%rcx), %eax
	jmp	.L3880
.L3910:
	subl	$55, %edx
	jmp	.L3877
.L3908:
	subl	$55, %eax
	jmp	.L3874
.L3872:
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE8HexToIntEh.part.0
.L3913:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4862:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE12DecodeStringEPKhS5_PSt6vectorItSaItEE.part.0, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE12DecodeStringEPKhS5_PSt6vectorItSaItEE.part.0
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseValueEPKhS5_PS5_i,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseValueEPKhS5_PS5_i, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseValueEPKhS5_PS5_i:
.LFB3859:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$301, %r8d
	je	.L4027
	movq	%rdx, %rbx
	movl	%r8d, %r12d
	leaq	-136(%rbp), %r14
	movq	$0, -144(%rbp)
	leaq	-144(%rbp), %r15
	movq	%r14, %rcx
	movq	%rbx, %rsi
	movq	$0, -136(%rbp)
	movq	%r15, %rdx
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS5_PS5_S6_
	cmpl	$12, %eax
	ja	.L3918
	leaq	.L3920(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseValueEPKhS5_PS5_i,"a",@progbits
	.align 4
	.align 4
.L3920:
	.long	.L3928-.L3920
	.long	.L3918-.L3920
	.long	.L3927-.L3920
	.long	.L3918-.L3920
	.long	.L3926-.L3920
	.long	.L3925-.L3920
	.long	.L3924-.L3920
	.long	.L3923-.L3920
	.long	.L3922-.L3920
	.long	.L3918-.L3920
	.long	.L3918-.L3920
	.long	.L3921-.L3920
	.long	.L3919-.L3920
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseValueEPKhS5_PS5_i
	.p2align 4,,10
	.p2align 3
.L4027:
	cmpb	$0, 8(%r13)
	je	.L4028
	.p2align 4,,10
	.p2align 3
.L3914:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4029
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4028:
	.cfi_restore_state
	movq	24(%r13), %r8
	subq	0(%r13), %rdi
	movl	$2, %esi
	movq	%rdi, %rdx
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*104(%rax)
	movb	$1, 8(%r13)
	jmp	.L3914
	.p2align 4,,10
	.p2align 3
.L3918:
	cmpb	$0, 8(%r13)
	jne	.L3914
	movq	24(%r13), %rdi
	movq	-144(%rbp), %rdx
	movl	$13, %esi
	subq	0(%r13), %rdx
	movq	(%rdi), %rax
	call	*104(%rax)
	movb	$1, 8(%r13)
	jmp	.L3914
	.p2align 4,,10
	.p2align 3
.L3927:
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-136(%rbp), %r10
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r10, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS5_PS5_S6_
	cmpl	$3, %eax
	je	.L3948
	addl	$1, %r12d
.L3950:
	movl	%r12d, %r8d
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r10, %rsi
	movq	%r13, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseValueEPKhS5_PS5_i
	cmpb	$0, 8(%r13)
	jne	.L3914
	movq	-136(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS5_PS5_S6_
	cmpl	$9, %eax
	je	.L4030
	cmpl	$3, %eax
	jne	.L4031
.L3948:
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
.L3929:
	movq	-136(%rbp), %rax
	cmpq	%rbx, %rax
	jnb	.L3967
	.p2align 4,,10
	.p2align 3
.L3968:
	movzbl	(%rax), %edx
	cmpb	$32, %dl
	je	.L3969
	leal	-9(%rdx), %ecx
	cmpb	$4, %cl
	jbe	.L3969
	cmpb	$47, %dl
	jne	.L3967
	cmpq	%rax, %rbx
	je	.L3967
	leaq	1(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L3967
	movzbl	1(%rax), %edx
	cmpb	$47, %dl
	je	.L4032
	cmpb	$42, %dl
	je	.L4033
	.p2align 4,,10
	.p2align 3
.L3967:
	movq	-152(%rbp), %rbx
	movq	%rax, (%rbx)
	jmp	.L3914
	.p2align 4,,10
	.p2align 3
.L3926:
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	$0, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	leaq	-1(%rax), %rsi
	leaq	1(%rdx), %rdi
	cmpq	%rdi, %rsi
	je	.L3980
	jb	.L3955
	leaq	-128(%rbp), %rdx
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE12DecodeStringEPKhS5_PSt6vectorItSaItEE.part.0
	testb	%al, %al
	je	.L3956
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	subq	%rsi, %rdx
	sarq	%rdx
.L3942:
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	call	*56(%rax)
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3929
	call	_ZdlPv@PLT
	jmp	.L3929
	.p2align 4,,10
	.p2align 3
.L3928:
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-136(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS5_PS5_S6_
	cmpl	$1, %eax
	je	.L3951
.L4022:
	cmpl	$4, %eax
	jne	.L3965
.L3952:
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %rdx
	pxor	%xmm2, %xmm2
	movq	$0, -112(%rbp)
	movaps	%xmm2, -128(%rbp)
	leaq	-1(%rax), %rsi
	leaq	1(%rdx), %rdi
	cmpq	%rdi, %rsi
	je	.L3981
	jnb	.L4034
.L3955:
	cmpb	$0, 8(%r13)
	je	.L4035
.L3958:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3914
	call	_ZdlPv@PLT
	jmp	.L3914
	.p2align 4,,10
	.p2align 3
.L3925:
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %r15
	leaq	-80(%rbp), %r14
	movq	%r14, -96(%rbp)
	movq	%rax, %r12
	subq	%r15, %r12
	testq	%rax, %rax
	je	.L3930
	testq	%r15, %r15
	jne	.L3930
	leaq	.LC52(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L3921:
	cmpb	$0, 8(%r13)
	jne	.L3914
	movq	24(%r13), %rdi
	movq	-144(%rbp), %rdx
	movl	$4, %esi
	subq	0(%r13), %rdx
	movq	(%rdi), %rax
	call	*104(%rax)
	movb	$1, 8(%r13)
	jmp	.L3914
	.p2align 4,,10
	.p2align 3
.L3919:
	cmpb	$0, 8(%r13)
	jne	.L3914
	movq	24(%r13), %rdi
	movq	-144(%rbp), %rdx
	movl	$3, %esi
	subq	0(%r13), %rdx
	movq	(%rdi), %rax
	call	*104(%rax)
	movb	$1, 8(%r13)
	jmp	.L3914
	.p2align 4,,10
	.p2align 3
.L3923:
	movq	24(%r13), %rdi
	xorl	%esi, %esi
	movq	(%rdi), %rax
	call	*88(%rax)
	jmp	.L3929
	.p2align 4,,10
	.p2align 3
.L3922:
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	call	*96(%rax)
	jmp	.L3929
	.p2align 4,,10
	.p2align 3
.L3924:
	movq	24(%r13), %rdi
	movl	$1, %esi
	movq	(%rdi), %rax
	call	*88(%rax)
	jmp	.L3929
	.p2align 4,,10
	.p2align 3
.L3930:
	movq	%r12, -128(%rbp)
	cmpq	$15, %r12
	ja	.L4036
	cmpq	$1, %r12
	jne	.L3933
	movzbl	(%r15), %eax
	leaq	-128(%rbp), %r8
	movb	%al, -80(%rbp)
	movq	%r14, %rax
.L3934:
	movq	%r12, -88(%rbp)
	movq	%r8, %rdx
	movb	$0, (%rax,%r12)
	movq	16(%r13), %rdi
	movq	-96(%rbp), %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-96(%rbp), %rdi
	movl	%eax, %r12d
	cmpq	%r14, %rdi
	je	.L3935
	call	_ZdlPv@PLT
.L3935:
	testb	%r12b, %r12b
	je	.L4037
	movsd	-128(%rbp), %xmm0
	comisd	.LC115(%rip), %xmm0
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	jb	.L3937
	movsd	.LC116(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L3937
	cvttsd2sil	%xmm0, %esi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L3937
	jne	.L3937
	call	*80(%rax)
	jmp	.L3929
	.p2align 4,,10
	.p2align 3
.L3969:
	addq	$1, %rax
.L3971:
	cmpq	%rax, %rbx
	ja	.L3968
	jmp	.L3967
	.p2align 4,,10
	.p2align 3
.L4032:
	addq	$2, %rax
	cmpq	%rax, %rbx
	jbe	.L3982
	.p2align 4,,10
	.p2align 3
.L3973:
	movzbl	(%rax), %edx
	addq	$1, %rax
	cmpb	$10, %dl
	je	.L3971
	cmpb	$13, %dl
	je	.L3971
	cmpq	%rax, %rbx
	jne	.L3973
	jmp	.L3967
	.p2align 4,,10
	.p2align 3
.L4030:
	movq	-136(%rbp), %r10
	movq	%r15, %rdx
	movq	%r10, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS5_PS5_S6_
	cmpl	$3, %eax
	jne	.L3950
	movq	-144(%rbp), %rdx
	movl	$7, %esi
	movq	%r13, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS_5ErrorEPKh
	jmp	.L3914
	.p2align 4,,10
	.p2align 3
.L4033:
	leaq	2(%rax), %rcx
	cmpq	%rcx, %rbx
	ja	.L3974
	jmp	.L3967
	.p2align 4,,10
	.p2align 3
.L3975:
	movq	%rdx, %rcx
.L3974:
	leaq	1(%rcx), %rdx
	movzbl	-1(%rdx), %esi
	cmpq	%rdx, %rbx
	je	.L3967
	cmpb	$42, %sil
	jne	.L3975
	cmpb	$47, (%rdx)
	jne	.L3975
	leaq	2(%rcx), %rax
	jmp	.L3971
	.p2align 4,,10
	.p2align 3
.L4035:
	movq	24(%r13), %rdi
	subq	0(%r13), %rdx
	movl	$6, %esi
	movq	(%rdi), %rax
	call	*104(%rax)
	movb	$1, 8(%r13)
	jmp	.L3958
	.p2align 4,,10
	.p2align 3
.L4036:
	leaq	-128(%rbp), %r8
	leaq	-96(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r8, %rsi
	movq	%r8, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-160(%rbp), %r8
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-128(%rbp), %rax
	movq	%rax, -80(%rbp)
.L3932:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r8, -160(%rbp)
	call	memcpy@PLT
	movq	-128(%rbp), %r12
	movq	-96(%rbp), %rax
	movq	-160(%rbp), %r8
	jmp	.L3934
	.p2align 4,,10
	.p2align 3
.L4034:
	leaq	-128(%rbp), %rdx
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE12DecodeStringEPKhS5_PSt6vectorItSaItEE.part.0
	testb	%al, %al
	je	.L3956
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	subq	%rsi, %rdx
	sarq	%rdx
.L3954:
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	call	*56(%rax)
	movq	-136(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS5_PS5_S6_
	cmpl	$10, %eax
	jne	.L4038
	movq	-136(%rbp), %rsi
	leal	1(%r12), %r8d
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseValueEPKhS5_PS5_i
	cmpb	$0, 8(%r13)
	jne	.L3958
	movq	-136(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS5_PS5_S6_
	cmpl	$9, %eax
	je	.L4039
	cmpl	$1, %eax
	jne	.L4040
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3951
	call	_ZdlPv@PLT
.L3951:
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L3929
	.p2align 4,,10
	.p2align 3
.L3937:
	call	*72(%rax)
	jmp	.L3929
	.p2align 4,,10
	.p2align 3
.L3981:
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	.L3954
	.p2align 4,,10
	.p2align 3
.L3933:
	testq	%r12, %r12
	jne	.L4041
	movq	%r14, %rax
	leaq	-128(%rbp), %r8
	jmp	.L3934
	.p2align 4,,10
	.p2align 3
.L4031:
	movq	-144(%rbp), %rdx
	movl	$8, %esi
	movq	%r13, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS_5ErrorEPKh
	jmp	.L3914
	.p2align 4,,10
	.p2align 3
.L4039:
	movq	-136(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS5_PS5_S6_
	cmpl	$1, %eax
	je	.L4042
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4022
	movl	%eax, -160(%rbp)
	call	_ZdlPv@PLT
	movl	-160(%rbp), %eax
	cmpl	$4, %eax
	je	.L3952
.L3965:
	movq	-144(%rbp), %rdx
	movl	$9, %esi
	movq	%r13, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS_5ErrorEPKh
	jmp	.L3914
	.p2align 4,,10
	.p2align 3
.L4037:
	movq	-144(%rbp), %rdx
	movl	$5, %esi
	movq	%r13, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS_5ErrorEPKh
	jmp	.L3914
	.p2align 4,,10
	.p2align 3
.L3980:
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	.L3942
	.p2align 4,,10
	.p2align 3
.L3956:
	movq	-144(%rbp), %rdx
	jmp	.L3955
.L3982:
	movq	%rbx, %rax
	jmp	.L3967
.L4038:
	movq	-144(%rbp), %rdx
	movl	$10, %esi
	movq	%r13, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS_5ErrorEPKh
	jmp	.L3958
.L4040:
	movq	-144(%rbp), %rdx
	movl	$12, %esi
	movq	%r13, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS_5ErrorEPKh
	jmp	.L3958
.L4029:
	call	__stack_chk_fail@PLT
.L4042:
	movq	-144(%rbp), %rdx
	movl	$11, %esi
	movq	%r13, %rdi
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS_5ErrorEPKh
	jmp	.L3958
.L4041:
	movq	%r14, %rdi
	leaq	-128(%rbp), %r8
	jmp	.L3932
	.cfi_endproc
.LFE3859:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseValueEPKhS5_PS5_i, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseValueEPKhS5_PS5_i
	.section	.text._ZN30v8_inspector_protocol_encoding4json9ParseJSONERKNS0_8PlatformENS_4spanIhEEPNS_22StreamingParserHandlerE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4json9ParseJSONERKNS0_8PlatformENS_4spanIhEEPNS_22StreamingParserHandlerE
	.type	_ZN30v8_inspector_protocol_encoding4json9ParseJSONERKNS0_8PlatformENS_4spanIhEEPNS_22StreamingParserHandlerE, @function
_ZN30v8_inspector_protocol_encoding4json9ParseJSONERKNS0_8PlatformENS_4spanIhEEPNS_22StreamingParserHandlerE:
.LFB3284:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm1
	movq	%rdi, %xmm0
	xorl	%r8d, %r8d
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	(%rsi,%rdx), %rbx
	leaq	-72(%rbp), %rcx
	leaq	-64(%rbp), %rdi
	movq	%rbx, %rdx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$0, -56(%rbp)
	movq	%rsi, -64(%rbp)
	movq	$0, -72(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseValueEPKhS5_PS5_i
	cmpb	$0, -56(%rbp)
	jne	.L4043
	movq	-72(%rbp), %rdx
	cmpq	%rdx, %rbx
	je	.L4043
	movq	-40(%rbp), %rdi
	subq	-64(%rbp), %rdx
	movl	$1, %esi
	movq	(%rdi), %rax
	call	*104(%rax)
.L4043:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4048
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4048:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3284:
	.size	_ZN30v8_inspector_protocol_encoding4json9ParseJSONERKNS0_8PlatformENS_4spanIhEEPNS_22StreamingParserHandlerE, .-_ZN30v8_inspector_protocol_encoding4json9ParseJSONERKNS0_8PlatformENS_4spanIhEEPNS_22StreamingParserHandlerE
	.section	.text._ZN30v8_inspector_protocol_encoding4json17ConvertJSONToCBORERKNS0_8PlatformENS_4spanIhEEPSt6vectorIhSaIhEE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4json17ConvertJSONToCBORERKNS0_8PlatformENS_4spanIhEEPSt6vectorIhSaIhEE
	.type	_ZN30v8_inspector_protocol_encoding4json17ConvertJSONToCBORERKNS0_8PlatformENS_4spanIhEEPSt6vectorIhSaIhEE, @function
_ZN30v8_inspector_protocol_encoding4json17ConvertJSONToCBORERKNS0_8PlatformENS_4spanIhEEPSt6vectorIhSaIhEE:
.LFB3292:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	addq	%r13, %rbx
	subq	$96, %rsp
	movq	%rdi, -120(%rbp)
	movl	$48, %edi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -96(%rbp)
	movq	$-1, -88(%rbp)
	call	_Znwm@PLT
	xorl	%r8d, %r8d
	leaq	-104(%rbp), %rcx
	leaq	-80(%rbp), %rdi
	movq	%rax, %r12
	movq	-120(%rbp), %xmm0
	movq	%rbx, %rdx
	movq	%r13, %rsi
	leaq	16+_ZTVN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEEE(%rip), %rax
	movq	%r12, %xmm1
	movq	%r14, 8(%r12)
	movq	%rax, (%r12)
	punpcklqdq	%xmm1, %xmm0
	leaq	-96(%rbp), %rax
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	movq	%rax, 40(%r12)
	movl	$0, -96(%rbp)
	movq	$-1, -88(%rbp)
	movb	$0, -72(%rbp)
	movq	%r13, -80(%rbp)
	movq	$0, -104(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseValueEPKhS5_PS5_i
	cmpb	$0, -72(%rbp)
	jne	.L4051
	movq	-104(%rbp), %rdx
	cmpq	%rdx, %rbx
	je	.L4051
	movq	-56(%rbp), %rdi
	subq	-80(%rbp), %rdx
	movl	$1, %esi
	movq	(%rdi), %rax
	call	*104(%rax)
.L4051:
	movq	(%r12), %rax
	movl	-96(%rbp), %ebx
	movq	%r12, %rdi
	movq	-88(%rbp), %r13
	call	*8(%rax)
	movl	%ebx, %eax
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L4055
	addq	$96, %rsp
	movq	%r13, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4055:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3292:
	.size	_ZN30v8_inspector_protocol_encoding4json17ConvertJSONToCBORERKNS0_8PlatformENS_4spanIhEEPSt6vectorIhSaIhEE, .-_ZN30v8_inspector_protocol_encoding4json17ConvertJSONToCBORERKNS0_8PlatformENS_4spanIhEEPSt6vectorIhSaIhEE
	.section	.text._ZN30v8_inspector_protocol_encoding4json17ConvertJSONToCBORERKNS0_8PlatformENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN30v8_inspector_protocol_encoding4json17ConvertJSONToCBORERKNS0_8PlatformENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN30v8_inspector_protocol_encoding4json17ConvertJSONToCBORERKNS0_8PlatformENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN30v8_inspector_protocol_encoding4json17ConvertJSONToCBORERKNS0_8PlatformENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB3290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	addq	%r13, %rbx
	subq	$96, %rsp
	movq	%rdi, -120(%rbp)
	movl	$48, %edi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -96(%rbp)
	movq	$-1, -88(%rbp)
	call	_Znwm@PLT
	xorl	%r8d, %r8d
	leaq	-104(%rbp), %rcx
	leaq	-80(%rbp), %rdi
	movq	%rax, %r12
	movq	-120(%rbp), %xmm0
	movq	%rbx, %rdx
	movq	%r13, %rsi
	leaq	16+_ZTVN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	movq	%r12, %xmm1
	movq	%r14, 8(%r12)
	movq	%rax, (%r12)
	punpcklqdq	%xmm1, %xmm0
	leaq	-96(%rbp), %rax
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	movq	%rax, 40(%r12)
	movl	$0, -96(%rbp)
	movq	$-1, -88(%rbp)
	movb	$0, -72(%rbp)
	movq	%r13, -80(%rbp)
	movq	$0, -104(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_110JsonParserIhE10ParseValueEPKhS5_PS5_i
	cmpb	$0, -72(%rbp)
	jne	.L4058
	movq	-104(%rbp), %rdx
	cmpq	%rdx, %rbx
	je	.L4058
	movq	-56(%rbp), %rdi
	subq	-80(%rbp), %rdx
	movl	$1, %esi
	movq	(%rdi), %rax
	call	*104(%rax)
.L4058:
	movq	(%r12), %rax
	movl	-96(%rbp), %ebx
	movq	%r12, %rdi
	movq	-88(%rbp), %r13
	call	*8(%rax)
	movl	%ebx, %eax
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L4062
	addq	$96, %rsp
	movq	%r13, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4062:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3290:
	.size	_ZN30v8_inspector_protocol_encoding4json17ConvertJSONToCBORERKNS0_8PlatformENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN30v8_inspector_protocol_encoding4json17ConvertJSONToCBORERKNS0_8PlatformENS_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZNSt6vectorIN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoderESaIS2_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoderESaIS2_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoderESaIS2_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoderESaIS2_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoderESaIS2_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB4543:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rcx
	movq	(%rdi), %r12
	movq	%rcx, %rax
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	je	.L4090
	movq	%rsi, %r13
	movq	%rdi, %r15
	subq	%r12, %rsi
	testq	%rax, %rax
	je	.L4075
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L4091
.L4065:
	movq	%r14, %rdi
	movq	%rsi, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	%rax, %rbx
	leaq	(%rax,%r14), %rax
	movq	%rax, -56(%rbp)
	leaq	8(%rbx), %r14
.L4074:
	movq	$0, (%rbx,%rsi)
	cmpq	%r12, %r13
	je	.L4067
	leaq	-8(%r13), %rsi
	leaq	15(%rbx), %rax
	subq	%r12, %rsi
	subq	%r12, %rax
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rax
	jbe	.L4077
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdi
	je	.L4077
	addq	$1, %rdi
	xorl	%eax, %eax
	movq	%rdi, %rdx
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L4069:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L4069
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rdx
	leaq	(%r12,%rdx), %rax
	addq	%rbx, %rdx
	cmpq	%r8, %rdi
	je	.L4071
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L4071:
	leaq	16(%rbx,%rsi), %r14
.L4067:
	cmpq	%rcx, %r13
	je	.L4072
	subq	%r13, %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rcx, %rdx
	movq	%rcx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rcx
	addq	%rcx, %r14
.L4072:
	testq	%r12, %r12
	je	.L4073
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L4073:
	movq	-56(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r14, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4091:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L4066
	movq	$0, -56(%rbp)
	movl	$8, %r14d
	xorl	%ebx, %ebx
	jmp	.L4074
	.p2align 4,,10
	.p2align 3
.L4075:
	movl	$8, %r14d
	jmp	.L4065
	.p2align 4,,10
	.p2align 3
.L4077:
	movq	%rbx, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L4068:
	movq	(%rax), %rdi
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rdi, -8(%rdx)
	cmpq	%rax, %r13
	jne	.L4068
	jmp	.L4071
.L4066:
	cmpq	%rdx, %rdi
	cmovbe	%rdi, %rdx
	movq	%rdx, %r14
	salq	$3, %r14
	jmp	.L4065
.L4090:
	leaq	.LC97(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4543:
	.size	_ZNSt6vectorIN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoderESaIS2_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoderESaIS2_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE16HandleArrayBeginEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE16HandleArrayBeginEv, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE16HandleArrayBeginEv:
.LFB4468:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L4110
	ret
	.p2align 4,,10
	.p2align 3
.L4110:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %rsi
	cmpq	32(%rdi), %rsi
	je	.L4094
	movq	$0, (%rsi)
	movq	24(%rdi), %rax
	leaq	8(%rax), %r13
	movq	%r13, 24(%rdi)
.L4095:
	cmpq	$0, -8(%r13)
	movq	8(%rbx), %r12
	jne	.L4111
	movq	8(%r12), %r14
	movq	(%r12), %rax
	leaq	16(%r12), %r9
	leaq	1(%r14), %r15
	cmpq	%r9, %rax
	je	.L4103
	movq	16(%r12), %rdx
.L4097:
	cmpq	%rdx, %r15
	ja	.L4112
.L4098:
	movb	$-40, (%rax,%r14)
	movq	(%r12), %rax
	movq	%r15, 8(%r12)
	movb	$0, 1(%rax,%r14)
	movq	8(%r12), %r14
	movq	(%r12), %rax
	leaq	1(%r14), %r15
	cmpq	%r9, %rax
	je	.L4104
	movq	16(%r12), %rdx
.L4099:
	cmpq	%rdx, %r15
	ja	.L4113
.L4100:
	movb	$90, (%rax,%r14)
	movq	(%r12), %rax
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r15, 8(%r12)
	movb	$0, 1(%rax,%r14)
	movq	8(%r12), %rsi
	movq	%rsi, -8(%r13)
	addq	$4, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6resizeEmc@PLT
	movq	8(%rbx), %rbx
	movq	8(%rbx), %r12
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L4105
	movq	16(%rbx), %rdx
.L4101:
	cmpq	%rdx, %r13
	ja	.L4114
.L4102:
	movb	$-97, (%rax,%r12)
	movq	(%rbx), %rax
	movq	%r13, 8(%rbx)
	movb	$0, 1(%rax,%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4112:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	movq	-56(%rbp), %r9
	jmp	.L4098
	.p2align 4,,10
	.p2align 3
.L4114:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L4102
	.p2align 4,,10
	.p2align 3
.L4113:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	jmp	.L4100
	.p2align 4,,10
	.p2align 3
.L4104:
	movl	$15, %edx
	jmp	.L4099
	.p2align 4,,10
	.p2align 3
.L4103:
	movl	$15, %edx
	jmp	.L4097
	.p2align 4,,10
	.p2align 3
.L4105:
	movl	$15, %edx
	jmp	.L4101
	.p2align 4,,10
	.p2align 3
.L4094:
	leaq	16(%rdi), %rdi
	call	_ZNSt6vectorIN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoderESaIS2_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	movq	24(%rbx), %r13
	jmp	.L4095
.L4111:
	leaq	.LC89(%rip), %rcx
	movl	$472, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC90(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4468:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE16HandleArrayBeginEv, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE16HandleArrayBeginEv
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleMapBeginEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleMapBeginEv, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleMapBeginEv:
.LFB4478:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L4131
	ret
	.p2align 4,,10
	.p2align 3
.L4131:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rsi
	cmpq	32(%rdi), %rsi
	je	.L4117
	movq	$0, (%rsi)
	movq	24(%rdi), %rax
	leaq	8(%rax), %r13
	movq	%r13, 24(%rdi)
.L4118:
	cmpq	$0, -8(%r13)
	movq	8(%rbx), %r12
	jne	.L4132
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L4120
	movb	$-40, (%rsi)
	movq	8(%r12), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 8(%r12)
.L4121:
	cmpq	%rsi, 16(%r12)
	je	.L4122
	movb	$90, (%rsi)
	movq	8(%r12), %rax
	addq	$1, %rax
	movq	%rax, 8(%r12)
.L4123:
	subq	(%r12), %rax
	movq	%rax, -8(%r13)
	movq	8(%r12), %rcx
	movq	(%r12), %rax
	movq	%rcx, %rdx
	subq	%rax, %rdx
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %rdx
	jb	.L4133
	addq	%rsi, %rax
	cmpq	%rax, %rcx
	je	.L4125
	movq	%rax, 8(%r12)
.L4125:
	movq	8(%rbx), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L4126
	movb	$-65, (%rsi)
	addq	$1, 8(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4133:
	.cfi_restore_state
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_default_appendEm
	jmp	.L4125
	.p2align 4,,10
	.p2align 3
.L4122:
	leaq	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L36kInitialByteFor32BitLengthByteStringE(%rip), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	8(%r12), %rax
	jmp	.L4123
	.p2align 4,,10
	.p2align 3
.L4120:
	leaq	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L23kInitialByteForEnvelopeE(%rip), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	8(%r12), %rsi
	jmp	.L4121
	.p2align 4,,10
	.p2align 3
.L4117:
	leaq	16(%rdi), %rdi
	call	_ZNSt6vectorIN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoderESaIS2_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	movq	24(%rbx), %r13
	jmp	.L4118
	.p2align 4,,10
	.p2align 3
.L4126:
	addq	$8, %rsp
	leaq	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L31kInitialByteIndefiniteLengthMapE(%rip), %rdx
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
.L4132:
	.cfi_restore_state
	leaq	.LC107(%rip), %rcx
	movl	$472, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC90(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4478:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleMapBeginEv, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleMapBeginEv
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE16HandleArrayBeginEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE16HandleArrayBeginEv, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE16HandleArrayBeginEv:
.LFB4480:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L4150
	ret
	.p2align 4,,10
	.p2align 3
.L4150:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rsi
	cmpq	32(%rdi), %rsi
	je	.L4136
	movq	$0, (%rsi)
	movq	24(%rdi), %rax
	leaq	8(%rax), %r13
	movq	%r13, 24(%rdi)
.L4137:
	cmpq	$0, -8(%r13)
	movq	8(%rbx), %r12
	jne	.L4151
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L4139
	movb	$-40, (%rsi)
	movq	8(%r12), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 8(%r12)
.L4140:
	cmpq	%rsi, 16(%r12)
	je	.L4141
	movb	$90, (%rsi)
	movq	8(%r12), %rax
	addq	$1, %rax
	movq	%rax, 8(%r12)
.L4142:
	subq	(%r12), %rax
	movq	%rax, -8(%r13)
	movq	8(%r12), %rcx
	movq	(%r12), %rax
	movq	%rcx, %rdx
	subq	%rax, %rdx
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %rdx
	jb	.L4152
	addq	%rsi, %rax
	cmpq	%rax, %rcx
	je	.L4144
	movq	%rax, 8(%r12)
.L4144:
	movq	8(%rbx), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L4145
	movb	$-97, (%rsi)
	addq	$1, 8(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4152:
	.cfi_restore_state
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_default_appendEm
	jmp	.L4144
	.p2align 4,,10
	.p2align 3
.L4141:
	leaq	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L36kInitialByteFor32BitLengthByteStringE(%rip), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	8(%r12), %rax
	jmp	.L4142
	.p2align 4,,10
	.p2align 3
.L4139:
	leaq	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L23kInitialByteForEnvelopeE(%rip), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	8(%r12), %rsi
	jmp	.L4140
	.p2align 4,,10
	.p2align 3
.L4136:
	leaq	16(%rdi), %rdi
	call	_ZNSt6vectorIN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoderESaIS2_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	movq	24(%rbx), %r13
	jmp	.L4137
	.p2align 4,,10
	.p2align 3
.L4145:
	addq	$8, %rsp
	leaq	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L33kInitialByteIndefiniteLengthArrayE(%rip), %rdx
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
.L4151:
	.cfi_restore_state
	leaq	.LC107(%rip), %rcx
	movl	$472, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC90(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4480:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE16HandleArrayBeginEv, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE16HandleArrayBeginEv
	.section	.text._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleMapBeginEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleMapBeginEv, @function
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleMapBeginEv:
.LFB4466:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L4171
	ret
	.p2align 4,,10
	.p2align 3
.L4171:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %rsi
	cmpq	32(%rdi), %rsi
	je	.L4155
	movq	$0, (%rsi)
	movq	24(%rdi), %rax
	leaq	8(%rax), %r13
	movq	%r13, 24(%rdi)
.L4156:
	cmpq	$0, -8(%r13)
	movq	8(%rbx), %r12
	jne	.L4172
	movq	8(%r12), %r14
	movq	(%r12), %rax
	leaq	16(%r12), %r9
	leaq	1(%r14), %r15
	cmpq	%r9, %rax
	je	.L4164
	movq	16(%r12), %rdx
.L4158:
	cmpq	%rdx, %r15
	ja	.L4173
.L4159:
	movb	$-40, (%rax,%r14)
	movq	(%r12), %rax
	movq	%r15, 8(%r12)
	movb	$0, 1(%rax,%r14)
	movq	8(%r12), %r14
	movq	(%r12), %rax
	leaq	1(%r14), %r15
	cmpq	%r9, %rax
	je	.L4165
	movq	16(%r12), %rdx
.L4160:
	cmpq	%rdx, %r15
	ja	.L4174
.L4161:
	movb	$90, (%rax,%r14)
	movq	(%r12), %rax
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r15, 8(%r12)
	movb	$0, 1(%rax,%r14)
	movq	8(%r12), %rsi
	movq	%rsi, -8(%r13)
	addq	$4, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6resizeEmc@PLT
	movq	8(%rbx), %rbx
	movq	8(%rbx), %r12
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L4166
	movq	16(%rbx), %rdx
.L4162:
	cmpq	%rdx, %r13
	ja	.L4175
.L4163:
	movb	$-65, (%rax,%r12)
	movq	(%rbx), %rax
	movq	%r13, 8(%rbx)
	movb	$0, 1(%rax,%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4173:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	movq	-56(%rbp), %r9
	jmp	.L4159
	.p2align 4,,10
	.p2align 3
.L4175:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L4163
	.p2align 4,,10
	.p2align 3
.L4174:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	jmp	.L4161
	.p2align 4,,10
	.p2align 3
.L4165:
	movl	$15, %edx
	jmp	.L4160
	.p2align 4,,10
	.p2align 3
.L4164:
	movl	$15, %edx
	jmp	.L4158
	.p2align 4,,10
	.p2align 3
.L4166:
	movl	$15, %edx
	jmp	.L4162
	.p2align 4,,10
	.p2align 3
.L4155:
	leaq	16(%rdi), %rdi
	call	_ZNSt6vectorIN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoderESaIS2_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	movq	24(%rbx), %r13
	jmp	.L4156
.L4172:
	leaq	.LC89(%rip), %rcx
	movl	$472, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC90(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4466:
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleMapBeginEv, .-_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleMapBeginEv
	.section	.text._ZNSt6vectorIhSaIhEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEEvNS4_IPhS1_EET_SG_St20forward_iterator_tag,"axG",@progbits,_ZNSt6vectorIhSaIhEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEEvNS4_IPhS1_EET_SG_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhSaIhEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEEvNS4_IPhS1_EET_SG_St20forward_iterator_tag
	.type	_ZNSt6vectorIhSaIhEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEEvNS4_IPhS1_EET_SG_St20forward_iterator_tag, @function
_ZNSt6vectorIhSaIhEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEEvNS4_IPhS1_EET_SG_St20forward_iterator_tag:
.LFB4615:
	.cfi_startproc
	endbr64
	cmpq	%rdx, %rcx
	je	.L4421
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	subq	%rdx, %r13
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	movq	%r13, %r11
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	8(%rdi), %r14
	movq	16(%rdi), %rax
	subq	%r14, %rax
	cmpq	%r13, %rax
	jb	.L4179
	movq	%r14, %r15
	subq	%rsi, %r15
	cmpq	%r15, %r13
	jnb	.L4180
	movq	%r14, %r15
	movq	%rdi, -56(%rbp)
	movq	%r13, %rdx
	movq	%r14, %rdi
	subq	%r13, %r15
	movq	%r15, %rsi
	call	memmove@PLT
	movq	-56(%rbp), %r8
	addq	%r13, 8(%r8)
	subq	%r12, %r15
	jne	.L4439
.L4181:
	testq	%r13, %r13
	jle	.L4176
	leaq	15(%r12), %rax
	subq	%rbx, %rax
	cmpq	$30, %rax
	jbe	.L4212
	leaq	-1(%r13), %rax
	cmpq	$14, %rax
	jbe	.L4212
	movq	%r13, %rdx
	xorl	%eax, %eax
	andq	$-16, %rdx
	.p2align 4,,10
	.p2align 3
.L4185:
	movdqu	(%rbx,%rax), %xmm2
	movups	%xmm2, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L4185
	movq	%r13, %rdx
	movq	%r13, %rax
	andq	$-16, %rdx
	addq	%rdx, %rbx
	subq	%rdx, %rax
	addq	%rdx, %r12
	cmpq	%rdx, %r13
	je	.L4176
.L4438:
	movzbl	(%rbx), %edx
	movb	%dl, (%r12)
	cmpq	$1, %rax
	je	.L4176
	movzbl	1(%rbx), %edx
	movb	%dl, 1(%r12)
	cmpq	$2, %rax
	je	.L4176
	movzbl	2(%rbx), %edx
	movb	%dl, 2(%r12)
	cmpq	$3, %rax
	je	.L4176
	movzbl	3(%rbx), %edx
	movb	%dl, 3(%r12)
	cmpq	$4, %rax
	je	.L4176
	movzbl	4(%rbx), %edx
	movb	%dl, 4(%r12)
	cmpq	$5, %rax
	je	.L4176
	movzbl	5(%rbx), %edx
	movb	%dl, 5(%r12)
	cmpq	$6, %rax
	je	.L4176
	movzbl	6(%rbx), %edx
	movb	%dl, 6(%r12)
	cmpq	$7, %rax
	je	.L4176
	movzbl	7(%rbx), %edx
	movb	%dl, 7(%r12)
	cmpq	$8, %rax
	je	.L4176
	movzbl	8(%rbx), %edx
	movb	%dl, 8(%r12)
	cmpq	$9, %rax
	je	.L4176
	movzbl	9(%rbx), %edx
	movb	%dl, 9(%r12)
	cmpq	$10, %rax
	je	.L4176
	movzbl	10(%rbx), %edx
	movb	%dl, 10(%r12)
	cmpq	$11, %rax
	je	.L4176
	movzbl	11(%rbx), %edx
	movb	%dl, 11(%r12)
	cmpq	$12, %rax
	je	.L4176
	movzbl	12(%rbx), %edx
	movb	%dl, 12(%r12)
	cmpq	$13, %rax
	je	.L4176
	movzbl	13(%rbx), %edx
	movb	%dl, 13(%r12)
	cmpq	$14, %rax
	je	.L4176
	movzbl	14(%rbx), %eax
	movb	%al, 14(%r12)
.L4176:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4179:
	.cfi_restore_state
	movq	(%rdi), %r10
	movq	%r14, %rax
	movq	%rdx, %r9
	movabsq	$9223372036854775807, %rcx
	movq	%rcx, %rdx
	subq	%r10, %rax
	subq	%rax, %rdx
	cmpq	%rdx, %r13
	ja	.L4440
	cmpq	%rax, %r13
	movq	%rax, %rdx
	cmovnb	%r13, %rdx
	addq	%rdx, %rax
	movq	%rax, %r15
	jc	.L4215
	testq	%rax, %rax
	js	.L4215
	jne	.L4203
	xorl	%r15d, %r15d
	xorl	%ecx, %ecx
.L4211:
	movq	%r12, %rdx
	subq	%r10, %rdx
	jne	.L4441
.L4204:
	leaq	(%rcx,%rdx), %rdi
	testq	%r13, %r13
	jle	.L4205
	cmpq	$15, %r13
	jle	.L4216
	movq	%r13, %rdx
	xorl	%eax, %eax
	andq	$-16, %rdx
	.p2align 4,,10
	.p2align 3
.L4207:
	movdqu	(%rbx,%rax), %xmm1
	movups	%xmm1, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L4207
	movq	%r13, %rdx
	andq	$-16, %rdx
	subq	%rdx, %r13
	leaq	(%rbx,%rdx), %r9
	leaq	(%rdi,%rdx), %rax
	cmpq	%rdx, %r11
	je	.L4208
.L4206:
	movzbl	(%r9), %edx
	movb	%dl, (%rax)
	cmpq	$1, %r13
	je	.L4208
	movzbl	1(%r9), %edx
	movb	%dl, 1(%rax)
	cmpq	$2, %r13
	je	.L4208
	movzbl	2(%r9), %edx
	movb	%dl, 2(%rax)
	cmpq	$3, %r13
	je	.L4208
	movzbl	3(%r9), %edx
	movb	%dl, 3(%rax)
	cmpq	$4, %r13
	je	.L4208
	movzbl	4(%r9), %edx
	movb	%dl, 4(%rax)
	cmpq	$5, %r13
	je	.L4208
	movzbl	5(%r9), %edx
	movb	%dl, 5(%rax)
	cmpq	$6, %r13
	je	.L4208
	movzbl	6(%r9), %edx
	movb	%dl, 6(%rax)
	cmpq	$7, %r13
	je	.L4208
	movzbl	7(%r9), %edx
	movb	%dl, 7(%rax)
	cmpq	$8, %r13
	je	.L4208
	movzbl	8(%r9), %edx
	movb	%dl, 8(%rax)
	cmpq	$9, %r13
	je	.L4208
	movzbl	9(%r9), %edx
	movb	%dl, 9(%rax)
	cmpq	$10, %r13
	je	.L4208
	movzbl	10(%r9), %edx
	movb	%dl, 10(%rax)
	cmpq	$11, %r13
	je	.L4208
	movzbl	11(%r9), %edx
	movb	%dl, 11(%rax)
	cmpq	$12, %r13
	je	.L4208
	movzbl	12(%r9), %edx
	movb	%dl, 12(%rax)
	cmpq	$13, %r13
	je	.L4208
	movzbl	13(%r9), %edx
	movb	%dl, 13(%rax)
	cmpq	$14, %r13
	je	.L4208
	movzbl	14(%r9), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L4208:
	addq	%r11, %rdi
.L4205:
	subq	%r12, %r14
	jne	.L4442
.L4209:
	addq	%rdi, %r14
	testq	%r10, %r10
	je	.L4210
	movq	%r10, %rdi
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
.L4210:
	movq	%r14, %xmm5
	movq	%rcx, %xmm0
	movq	%r15, 16(%r8)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, (%r8)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4421:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L4180:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	(%rdx,%r15), %rdx
	subq	%rdx, %rcx
	testq	%rcx, %rcx
	jle	.L4188
	leaq	16(%r15,%rbx), %rax
	cmpq	%rax, %r14
	leaq	16(%r14), %rax
	setnb	%sil
	cmpq	%rax, %rdx
	setnb	%al
	orb	%al, %sil
	je	.L4189
	leaq	-1(%rcx), %rax
	cmpq	$14, %rax
	jbe	.L4189
	movq	%rcx, %rsi
	xorl	%eax, %eax
	andq	$-16, %rsi
	.p2align 4,,10
	.p2align 3
.L4190:
	movdqu	(%rdx,%rax), %xmm3
	movups	%xmm3, (%r14,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L4190
	movq	%rcx, %rsi
	movq	%rcx, %rax
	andq	$-16, %rsi
	andl	$15, %eax
	addq	%rsi, %rdx
	addq	%rsi, %r14
	cmpq	%rsi, %rcx
	je	.L4192
	movzbl	(%rdx), %ecx
	movb	%cl, (%r14)
	cmpq	$1, %rax
	je	.L4192
	movzbl	1(%rdx), %ecx
	movb	%cl, 1(%r14)
	cmpq	$2, %rax
	je	.L4192
	movzbl	2(%rdx), %ecx
	movb	%cl, 2(%r14)
	cmpq	$3, %rax
	je	.L4192
	movzbl	3(%rdx), %ecx
	movb	%cl, 3(%r14)
	cmpq	$4, %rax
	je	.L4192
	movzbl	4(%rdx), %ecx
	movb	%cl, 4(%r14)
	cmpq	$5, %rax
	je	.L4192
	movzbl	5(%rdx), %ecx
	movb	%cl, 5(%r14)
	cmpq	$6, %rax
	je	.L4192
	movzbl	6(%rdx), %ecx
	movb	%cl, 6(%r14)
	cmpq	$7, %rax
	je	.L4192
	movzbl	7(%rdx), %ecx
	movb	%cl, 7(%r14)
	cmpq	$8, %rax
	je	.L4192
	movzbl	8(%rdx), %ecx
	movb	%cl, 8(%r14)
	cmpq	$9, %rax
	je	.L4192
	movzbl	9(%rdx), %ecx
	movb	%cl, 9(%r14)
	cmpq	$10, %rax
	je	.L4192
	movzbl	10(%rdx), %ecx
	movb	%cl, 10(%r14)
	cmpq	$11, %rax
	je	.L4192
	movzbl	11(%rdx), %ecx
	movb	%cl, 11(%r14)
	cmpq	$12, %rax
	je	.L4192
	movzbl	12(%rdx), %ecx
	movb	%cl, 12(%r14)
	cmpq	$13, %rax
	je	.L4192
	movzbl	13(%rdx), %ecx
	movb	%cl, 13(%r14)
	cmpq	$14, %rax
	je	.L4192
	movzbl	14(%rdx), %eax
	movb	%al, 14(%r14)
	.p2align 4,,10
	.p2align 3
.L4192:
	movq	8(%r8), %r14
.L4188:
	subq	%r15, %r13
	leaq	(%r14,%r13), %rdi
	movq	%rdi, 8(%r8)
	testq	%r15, %r15
	je	.L4176
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
	addq	%r15, 8(%r8)
	testq	%r15, %r15
	jle	.L4176
	leaq	15(%r12), %rax
	subq	%rbx, %rax
	cmpq	$30, %rax
	jbe	.L4213
	leaq	-1(%r15), %rax
	cmpq	$14, %rax
	jbe	.L4213
	movq	%r15, %rdx
	xorl	%eax, %eax
	andq	$-16, %rdx
	.p2align 4,,10
	.p2align 3
.L4197:
	movdqu	(%rbx,%rax), %xmm4
	movups	%xmm4, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L4197
	movq	%r15, %rdx
	movq	%r15, %rax
	andq	$-16, %rdx
	andl	$15, %eax
	addq	%rdx, %rbx
	addq	%rdx, %r12
	cmpq	%rdx, %r15
	jne	.L4438
	jmp	.L4176
	.p2align 4,,10
	.p2align 3
.L4215:
	movq	%rcx, %r15
.L4203:
	movq	%r15, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %r8
	movq	-56(%rbp), %r11
	movq	-64(%rbp), %r9
	movq	%rax, %rcx
	addq	%rax, %r15
	movq	(%r8), %r10
	movq	8(%r8), %r14
	jmp	.L4211
	.p2align 4,,10
	.p2align 3
.L4439:
	movq	%r14, %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	subq	%r15, %rdi
	call	memmove@PLT
	jmp	.L4181
	.p2align 4,,10
	.p2align 3
.L4442:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r10, -56(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r10
	movq	%rax, %rdi
	jmp	.L4209
	.p2align 4,,10
	.p2align 3
.L4441:
	movq	%r10, %rsi
	movq	%rcx, %rdi
	movq	%r8, -88(%rbp)
	movq	%r9, -80(%rbp)
	movq	%r11, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r10, -56(%rbp)
	call	memmove@PLT
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %r11
	movq	-64(%rbp), %rdx
	movq	%rax, %rcx
	movq	-56(%rbp), %r10
	jmp	.L4204
	.p2align 4,,10
	.p2align 3
.L4189:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L4193:
	movzbl	(%rdx,%rax), %esi
	movb	%sil, (%r14,%rax)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L4193
	jmp	.L4192
	.p2align 4,,10
	.p2align 3
.L4213:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L4195:
	movzbl	(%rbx,%rax), %edx
	movb	%dl, (%r12,%rax)
	addq	$1, %rax
	cmpq	%rax, %r15
	jne	.L4195
	jmp	.L4176
	.p2align 4,,10
	.p2align 3
.L4212:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L4183:
	movzbl	(%rbx,%rax), %edx
	movb	%dl, (%r12,%rax)
	addq	$1, %rax
	cmpq	%rax, %r13
	jne	.L4183
	jmp	.L4176
.L4216:
	movq	%rdi, %rax
	jmp	.L4206
.L4440:
	leaq	.LC25(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4615:
	.size	_ZNSt6vectorIhSaIhEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEEvNS4_IPhS1_EET_SG_St20forward_iterator_tag, .-_ZNSt6vectorIhSaIhEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEEvNS4_IPhS1_EET_SG_St20forward_iterator_tag
	.section	.text._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleInt32Ei,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleInt32Ei, @function
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleInt32Ei:
.LFB4462:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L4443
	movq	80(%rdi), %r12
	movq	%rdi, %rbx
	movl	%esi, %r13d
	cmpq	88(%rdi), %r12
	je	.L4459
	movl	-8(%r12), %edx
	movl	-4(%r12), %eax
	testl	%edx, %edx
	je	.L4460
.L4446:
	testl	%eax, %eax
	je	.L4447
	cmpl	$2, %edx
	je	.L4451
	testb	$1, %al
	jne	.L4461
.L4451:
	movl	$44, %eax
.L4448:
	movq	16(%rbx), %rdi
	leaq	-81(%rbp), %rsi
	movb	%al, -81(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movl	-4(%r12), %eax
	jmp	.L4447
	.p2align 4,,10
	.p2align 3
.L4460:
	testl	%eax, %eax
	jne	.L4462
.L4447:
	addl	$1, %eax
	leaq	-80(%rbp), %rdi
	movl	%r13d, %r8d
	movl	$16, %edx
	movl	%eax, -4(%r12)
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	xorl	%eax, %eax
	leaq	.LC28(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movq	16(%rbx), %rdi
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rcx
	movq	8(%rdi), %rsi
	addq	%rdx, %rcx
	call	_ZNSt6vectorIhSaIhEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEEvNS4_IPhS1_EET_SG_St20forward_iterator_tag
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4443
	call	_ZdlPv@PLT
.L4443:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4463
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4459:
	.cfi_restore_state
	movq	104(%rdi), %rax
	movq	-8(%rax), %r12
	movl	504(%r12), %edx
	addq	$512, %r12
	movl	-4(%r12), %eax
	testl	%edx, %edx
	jne	.L4446
	jmp	.L4460
	.p2align 4,,10
	.p2align 3
.L4461:
	movl	$58, %eax
	jmp	.L4448
.L4463:
	call	__stack_chk_fail@PLT
.L4462:
	leaq	.LC101(%rip), %rcx
	movl	$1247, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE4462:
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleInt32Ei, .-_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleInt32Ei
	.section	.data.rel.ro.local._ZTVN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEEE,"aw"
	.align 8
	.type	_ZTVN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEEE, @object
	.size	_ZTVN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEEE, 128
_ZTVN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEEE:
	.quad	0
	.quad	0
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEED1Ev
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEED0Ev
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleMapBeginEv
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleMapEndEv
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE16HandleArrayBeginEv
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleArrayEndEv
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE13HandleString8ENS_4spanIhEE
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleString16ENS_4spanItEE
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleBinaryENS_4spanIhEE
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleDoubleEd
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE11HandleInt32Ei
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE10HandleBoolEb
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE10HandleNullEv
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE11HandleErrorENS_6StatusE
	.section	.data.rel.ro.local._ZTVN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"aw"
	.align 8
	.type	_ZTVN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @object
	.size	_ZTVN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, 128
_ZTVN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
	.quad	0
	.quad	0
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED1Ev
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleMapBeginEv
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleMapEndEv
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE16HandleArrayBeginEv
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13HandleString8ENS_4spanIhEE
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleString16ENS_4spanItEE
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleBinaryENS_4spanIhEE
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleDoubleEd
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleInt32Ei
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleBoolEb
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleNullEv
	.quad	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS_6StatusE
	.section	.data.rel.ro.local._ZTVN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEEE,"aw"
	.align 8
	.type	_ZTVN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEEE, @object
	.size	_ZTVN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEEE, 128
_ZTVN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEEE:
	.quad	0
	.quad	0
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEED1Ev
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEED0Ev
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleMapBeginEv
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleMapEndEv
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE16HandleArrayBeginEv
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleArrayEndEv
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE13HandleString8ENS_4spanIhEE
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleString16ENS_4spanItEE
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleBinaryENS_4spanIhEE
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleDoubleEd
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleInt32Ei
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE10HandleBoolEb
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE10HandleNullEv
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleErrorENS_6StatusE
	.section	.data.rel.ro.local._ZTVN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"aw"
	.align 8
	.type	_ZTVN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @object
	.size	_ZTVN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, 128
_ZTVN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
	.quad	0
	.quad	0
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED1Ev
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleMapBeginEv
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleMapEndEv
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE16HandleArrayBeginEv
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13HandleString8ENS_4spanIhEE
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleString16ENS_4spanItEE
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleBinaryENS_4spanIhEE
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleDoubleEd
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleInt32Ei
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleBoolEb
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleNullEv
	.quad	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS_6StatusE
	.section	.rodata._ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_1L12kBase64TableE,"a"
	.align 32
	.type	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_1L12kBase64TableE, @object
	.size	_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_1L12kBase64TableE, 65
_ZN30v8_inspector_protocol_encoding4json12_GLOBAL__N_1L12kBase64TableE:
	.string	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L30kExpectedConversionToBase64TagE,"a"
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L30kExpectedConversionToBase64TagE, @object
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L30kExpectedConversionToBase64TagE, 1
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L30kExpectedConversionToBase64TagE:
	.byte	-42
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L21kInitialByteForDoubleE,"a"
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L21kInitialByteForDoubleE, @object
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L21kInitialByteForDoubleE, 1
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L21kInitialByteForDoubleE:
	.byte	-5
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L12kEncodedNullE,"a"
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L12kEncodedNullE, @object
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L12kEncodedNullE, 1
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L12kEncodedNullE:
	.byte	-10
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L13kEncodedFalseE,"a"
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L13kEncodedFalseE, @object
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L13kEncodedFalseE, 1
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L13kEncodedFalseE:
	.byte	-12
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L12kEncodedTrueE,"a"
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L12kEncodedTrueE, @object
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L12kEncodedTrueE, 1
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L12kEncodedTrueE:
	.byte	-11
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L9kStopByteE,"a"
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L9kStopByteE, @object
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L9kStopByteE, 1
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L9kStopByteE:
	.byte	-1
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L31kInitialByteIndefiniteLengthMapE,"a"
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L31kInitialByteIndefiniteLengthMapE, @object
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L31kInitialByteIndefiniteLengthMapE, 1
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L31kInitialByteIndefiniteLengthMapE:
	.byte	-65
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L33kInitialByteIndefiniteLengthArrayE,"a"
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L33kInitialByteIndefiniteLengthArrayE, @object
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L33kInitialByteIndefiniteLengthArrayE, 1
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L33kInitialByteIndefiniteLengthArrayE:
	.byte	-97
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L36kInitialByteFor32BitLengthByteStringE,"a"
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L36kInitialByteFor32BitLengthByteStringE, @object
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L36kInitialByteFor32BitLengthByteStringE, 1
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L36kInitialByteFor32BitLengthByteStringE:
	.byte	90
	.section	.rodata._ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L23kInitialByteForEnvelopeE,"a"
	.type	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L23kInitialByteForEnvelopeE, @object
	.size	_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L23kInitialByteForEnvelopeE, 1
_ZN30v8_inspector_protocol_encoding4cbor12_GLOBAL__N_1L23kInitialByteForEnvelopeE:
	.byte	-40
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC48:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC49:
	.long	4294967295
	.long	2146435071
	.section	.rodata.cst16
	.align 16
.LC88:
	.quad	2325063899492535881
	.quad	5711444290326123077
	.align 16
.LC96:
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.section	.rodata.cst8
	.align 8
.LC115:
	.long	0
	.long	-1042284544
	.align 8
.LC116:
	.long	4290772992
	.long	1105199103
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
