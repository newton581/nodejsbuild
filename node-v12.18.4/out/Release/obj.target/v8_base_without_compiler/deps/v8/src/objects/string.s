	.file	"string.cc"
	.text
	.section	.text._ZN2v88internal11Relocatable15IterateInstanceEPNS0_11RootVisitorE,"axG",@progbits,_ZN2v88internal11Relocatable15IterateInstanceEPNS0_11RootVisitorE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11Relocatable15IterateInstanceEPNS0_11RootVisitorE
	.type	_ZN2v88internal11Relocatable15IterateInstanceEPNS0_11RootVisitorE, @function
_ZN2v88internal11Relocatable15IterateInstanceEPNS0_11RootVisitorE:
.LFB5313:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5313:
	.size	_ZN2v88internal11Relocatable15IterateInstanceEPNS0_11RootVisitorE, .-_ZN2v88internal11Relocatable15IterateInstanceEPNS0_11RootVisitorE
	.section	.text._ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi,comdat
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi:
.LFB24489:
	.cfi_startproc
	endbr64
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE24489:
	.size	_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi:
.LFB24871:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r11
	movq	(%rdi), %r14
	movq	8(%rdi), %r13
	movslq	32(%rdi), %r8
	leal	-1(%r11), %edi
	subl	%r11d, %edx
	leaq	42372(%r14), %r10
	addq	$43396, %r14
	movslq	%edi, %r9
	movzbl	0(%r13,%r9), %r9d
	cmpl	%edx, %ecx
	jg	.L17
	movl	%r11d, %ecx
	movzbl	%r9b, %r11d
	movq	%r8, %r15
	leaq	(%r10,%r11,4), %rbx
	subl	$2, %ecx
	movq	%rbx, -56(%rbp)
	movslq	%ecx, %rbx
	movl	$1, %ecx
	subq	%r8, %rcx
	movq	%rbx, -48(%rbp)
	movq	%rcx, -64(%rbp)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L21:
	movl	%edi, %ebx
	subl	(%r10,%rcx,4), %ebx
	addl	%ebx, %eax
	cmpl	%eax, %edx
	jl	.L17
.L8:
	leal	(%rdi,%rax), %ecx
	movslq	%ecx, %rcx
	movzbl	(%rsi,%rcx), %ecx
	cmpb	%r9b, %cl
	jne	.L21
	testl	%edi, %edi
	js	.L4
	movslq	%eax, %r12
	movq	-48(%rbp), %rcx
	addq	%rsi, %r12
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L22:
	movq	%r8, %rcx
.L9:
	movl	%ecx, %ebx
	testl	%ecx, %ecx
	js	.L4
	movzbl	(%rcx,%r12), %r11d
	leaq	-1(%rcx), %r8
	cmpb	%r11b, 0(%r13,%rcx)
	je	.L22
	cmpl	%ebx, %r15d
	jle	.L23
	movq	-56(%rbp), %rbx
	movl	%edi, %ecx
	subl	(%rbx), %ecx
	addl	%ecx, %eax
	cmpl	%eax, %edx
	jge	.L8
	.p2align 4,,10
	.p2align 3
.L17:
	movl	$-1, %eax
.L4:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	addq	-64(%rbp), %rcx
	subl	(%r10,%r11,4), %ebx
	cmpl	%ebx, (%r14,%rcx,4)
	cmovge	(%r14,%rcx,4), %ebx
	addl	%ebx, %eax
	cmpl	%eax, %edx
	jge	.L8
	jmp	.L17
	.cfi_endproc
.LFE24871:
	.size	_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi:
.LFB24877:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rcx
	movq	16(%rdi), %r10
	movq	8(%rdi), %r15
	movl	32(%rdi), %ebx
	leaq	43396(%rcx), %rdi
	subl	%r10d, %edx
	movl	%r10d, -84(%rbp)
	movq	%rdi, -64(%rbp)
	leal	-1(%r10), %edi
	movslq	%edi, %r8
	movl	%ebx, -44(%rbp)
	movzbl	(%r15,%r8), %r8d
	cmpl	%edx, %eax
	jg	.L41
	movl	%r10d, %r14d
	leaq	42372(%rcx), %r9
	movzbl	%r8b, %r10d
	movslq	%ebx, %rcx
	leaq	(%r9,%r10,4), %rbx
	leal	-2(%r14), %r10d
	movq	%rbx, -72(%rbp)
	movslq	%r10d, %rbx
	movl	$1, %r10d
	subq	%rcx, %r10
	movq	%rbx, -56(%rbp)
	movq	%r10, -80(%rbp)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L46:
	cmpw	$255, %cx
	ja	.L28
	movl	%edi, %ebx
	subl	(%r9,%rcx,4), %ebx
	addl	%ebx, %eax
	cmpl	%eax, %edx
	jl	.L41
.L30:
	leal	(%rdi,%rax), %ecx
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %ecx
	cmpw	%r8w, %cx
	jne	.L46
	testl	%edi, %edi
	js	.L24
	movslq	%eax, %r10
	movl	%edi, -48(%rbp)
	movq	-56(%rbp), %rcx
	leaq	(%rsi,%r10,2), %r12
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L47:
	movq	%rbx, %rcx
.L31:
	leal	1(%rcx), %r14d
	movl	%ecx, %r13d
	testl	%ecx, %ecx
	js	.L24
	movzwl	(%r12,%rcx,2), %edi
	movzbl	(%r15,%rcx), %r11d
	leaq	-1(%rcx), %rbx
	movq	%rdi, %r10
	cmpl	%edi, %r11d
	je	.L47
	movl	-48(%rbp), %edi
	cmpl	%r13d, -44(%rbp)
	jg	.L36
	movq	-64(%rbp), %rbx
	addq	-80(%rbp), %rcx
	movl	(%rbx,%rcx,4), %ecx
	cmpw	$255, %r10w
	ja	.L35
	movl	%r13d, %r14d
	subl	(%r9,%r10,4), %r14d
.L35:
	cmpl	%r14d, %ecx
	cmovl	%r14d, %ecx
	addl	%ecx, %eax
.L45:
	cmpl	%edx, %eax
	jle	.L30
	.p2align 4,,10
	.p2align 3
.L41:
	movl	$-1, %eax
.L24:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movq	-72(%rbp), %rbx
	movl	%edi, %ecx
	subl	(%rbx), %ecx
	addl	%ecx, %eax
	jmp	.L45
.L28:
	addl	-84(%rbp), %eax
	jmp	.L45
	.cfi_endproc
.LFE24877:
	.size	_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi:
.LFB24882:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r11
	movq	8(%rdi), %r14
	movslq	32(%rdi), %r10
	movq	(%rdi), %rdi
	subl	%r11d, %edx
	leaq	42372(%rdi), %r9
	addq	$43396, %rdi
	movq	%rdi, -56(%rbp)
	leal	-1(%r11), %edi
	movslq	%edi, %r8
	movzwl	(%r14,%r8,2), %r8d
	cmpl	%edx, %ecx
	jg	.L61
	movl	%r11d, %ecx
	movzwl	%r8w, %r11d
	movq	%r10, %r15
	leaq	(%r9,%r11,4), %rbx
	subl	$2, %ecx
	movq	%rbx, -64(%rbp)
	movslq	%ecx, %rbx
	movl	$1, %ecx
	subq	%r10, %rcx
	movq	%rbx, -48(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L65:
	movl	%edi, %ebx
	subl	(%r9,%r10,4), %ebx
	addl	%ebx, %eax
	cmpl	%eax, %edx
	jl	.L61
.L52:
	leal	(%rdi,%rax), %ecx
	movslq	%ecx, %rcx
	movzbl	(%rsi,%rcx), %r10d
	cmpw	%r10w, %r8w
	jne	.L65
	testl	%edi, %edi
	js	.L48
	movslq	%eax, %r13
	movq	-48(%rbp), %rcx
	addq	%rsi, %r13
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%rbx, %rcx
.L53:
	movl	%ecx, %r12d
	testl	%ecx, %ecx
	js	.L48
	movzbl	0(%r13,%rcx), %r11d
	leaq	-1(%rcx), %rbx
	movq	%r11, %r10
	cmpw	%r11w, (%r14,%rcx,2)
	je	.L66
	cmpl	%r12d, %r15d
	jle	.L67
	movq	-64(%rbp), %rbx
	movl	%edi, %ecx
	subl	(%rbx), %ecx
	addl	%ecx, %eax
	cmpl	%eax, %edx
	jge	.L52
	.p2align 4,,10
	.p2align 3
.L61:
	movl	$-1, %eax
.L48:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movq	-56(%rbp), %rbx
	addq	-72(%rbp), %rcx
	subl	(%r9,%r10,4), %r12d
	cmpl	%r12d, (%rbx,%rcx,4)
	cmovge	(%rbx,%rcx,4), %r12d
	addl	%r12d, %eax
	cmpl	%eax, %edx
	jge	.L52
	jmp	.L61
	.cfi_endproc
.LFE24882:
	.size	_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi:
.LFB24887:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r11
	movq	(%rdi), %r14
	movq	8(%rdi), %r12
	movslq	32(%rdi), %r8
	leal	-1(%r11), %edi
	subl	%r11d, %edx
	leaq	42372(%r14), %r10
	addq	$43396, %r14
	movslq	%edi, %r9
	movzwl	(%r12,%r9,2), %r9d
	cmpl	%edx, %ecx
	jg	.L81
	movl	%r11d, %ecx
	movzbl	%r9b, %r11d
	movq	%r8, %r15
	leaq	(%r10,%r11,4), %rbx
	subl	$2, %ecx
	movq	%rbx, -56(%rbp)
	movslq	%ecx, %rbx
	movl	$1, %ecx
	subq	%r8, %rcx
	movq	%rbx, -48(%rbp)
	movq	%rcx, -64(%rbp)
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L85:
	movzbl	%cl, %ecx
	movl	%edi, %ebx
	subl	(%r10,%rcx,4), %ebx
	addl	%ebx, %eax
	cmpl	%eax, %edx
	jl	.L81
.L72:
	leal	(%rdi,%rax), %ecx
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %ecx
	cmpw	%r9w, %cx
	jne	.L85
	testl	%edi, %edi
	js	.L68
	movslq	%eax, %r8
	movq	-48(%rbp), %rcx
	leaq	(%rsi,%r8,2), %r13
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L86:
	movq	%r8, %rcx
.L73:
	movl	%ecx, %ebx
	testl	%ecx, %ecx
	js	.L68
	leaq	-1(%rcx), %r8
	movzwl	2(%r13,%r8,2), %r11d
	cmpw	%r11w, (%r12,%rcx,2)
	je	.L86
	cmpl	%ebx, %r15d
	jle	.L87
	movq	-56(%rbp), %rbx
	movl	%edi, %ecx
	subl	(%rbx), %ecx
	addl	%ecx, %eax
	cmpl	%eax, %edx
	jge	.L72
	.p2align 4,,10
	.p2align 3
.L81:
	movl	$-1, %eax
.L68:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	addq	-64(%rbp), %rcx
	movzbl	%r11b, %r11d
	subl	(%r10,%r11,4), %ebx
	cmpl	%ebx, (%r14,%rcx,4)
	cmovge	(%r14,%rcx,4), %ebx
	addl	%ebx, %eax
	cmpl	%eax, %edx
	jge	.L72
	jmp	.L81
	.cfi_endproc
.LFE24887:
	.size	_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal16FlatStringReaderD2Ev,"axG",@progbits,_ZN2v88internal16FlatStringReaderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16FlatStringReaderD2Ev
	.type	_ZN2v88internal16FlatStringReaderD2Ev, @function
_ZN2v88internal16FlatStringReaderD2Ev:
.LFB25040:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	16(%rdi), %rdx
	movq	%rdx, 41704(%rax)
	ret
	.cfi_endproc
.LFE25040:
	.size	_ZN2v88internal16FlatStringReaderD2Ev, .-_ZN2v88internal16FlatStringReaderD2Ev
	.weak	_ZN2v88internal16FlatStringReaderD1Ev
	.set	_ZN2v88internal16FlatStringReaderD1Ev,_ZN2v88internal16FlatStringReaderD2Ev
	.section	.text._ZN2v86String26ExternalStringResourceBaseD0Ev,"axG",@progbits,_ZN2v86String26ExternalStringResourceBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v86String26ExternalStringResourceBaseD0Ev
	.type	_ZN2v86String26ExternalStringResourceBaseD0Ev, @function
_ZN2v86String26ExternalStringResourceBaseD0Ev:
.LFB25075:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE25075:
	.size	_ZN2v86String26ExternalStringResourceBaseD0Ev, .-_ZN2v86String26ExternalStringResourceBaseD0Ev
	.section	.text._ZN2v88internal16FlatStringReaderD0Ev,"axG",@progbits,_ZN2v88internal16FlatStringReaderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16FlatStringReaderD0Ev
	.type	_ZN2v88internal16FlatStringReaderD0Ev, @function
_ZN2v88internal16FlatStringReaderD0Ev:
.LFB25042:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	16(%rdi), %rdx
	movl	$48, %esi
	movq	%rdx, 41704(%rax)
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE25042:
	.size	_ZN2v88internal16FlatStringReaderD0Ev, .-_ZN2v88internal16FlatStringReaderD0Ev
	.section	.text._ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi:
.LFB24482:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	subl	16(%rdi), %r13d
	leal	1(%r13), %ebx
	movzbl	(%rax), %r15d
	movl	%r15d, %r14d
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L97:
	subq	%r12, %rax
	movslq	%eax, %rdx
	movl	%eax, %r8d
	cmpb	(%r12,%rdx), %r14b
	je	.L91
	leal	1(%rax), %ecx
	cmpl	%eax, %r13d
	jle	.L94
.L93:
	movl	%ebx, %edx
	movl	%r15d, %esi
	subl	%ecx, %edx
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	leaq	(%r12,%rcx), %rdi
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L97
.L94:
	movl	$-1, %r8d
.L91:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24482:
	.size	_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi:
.LFB24490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movzwl	(%rax), %r15d
	cmpw	$255, %r15w
	ja	.L102
	movl	%edx, %r14d
	subl	16(%rdi), %r14d
	movq	%rsi, %r13
	movl	%r15d, %ebx
	leal	1(%r14), %r12d
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L106:
	subq	%r13, %rax
	movslq	%eax, %rdx
	movl	%eax, %r8d
	cmpb	0(%r13,%rdx), %bl
	je	.L98
	leal	1(%rax), %ecx
	cmpl	%eax, %r14d
	jle	.L102
.L101:
	movl	%r12d, %edx
	movl	%r15d, %esi
	subl	%ecx, %edx
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	leaq	0(%r13,%rcx), %rdi
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L106
.L102:
	movl	$-1, %r8d
.L98:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24490:
	.size	_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi:
.LFB24486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	subl	16(%rdi), %r14d
	leal	1(%r14), %ebx
	movzbl	(%rax), %r15d
	movl	%r15d, %r13d
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L113:
	andq	$-2, %rax
	subq	%r12, %rax
	sarq	%rax
	movslq	%eax, %rdx
	movl	%eax, %r8d
	cmpw	(%r12,%rdx,2), %r13w
	je	.L107
	leal	1(%rax), %ecx
	cmpl	%eax, %r14d
	jle	.L110
.L109:
	movl	%ebx, %edx
	movl	%r15d, %esi
	subl	%ecx, %edx
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	leaq	(%r12,%rcx,2), %rdi
	addq	%rdx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L113
.L110:
	movl	$-1, %r8d
.L107:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24486:
	.size	_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi:
.LFB24494:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	subl	16(%rdi), %r14d
	leal	1(%r14), %ebx
	movzwl	(%rax), %r13d
	movl	%r13d, %eax
	movzbl	%ah, %eax
	cmpb	%al, %r13b
	movl	%eax, %r15d
	cmova	%r13d, %r15d
	movzbl	%r15b, %r15d
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L120:
	andq	$-2, %rax
	subq	%r12, %rax
	sarq	%rax
	movslq	%eax, %rdx
	movl	%eax, %r8d
	cmpw	(%r12,%rdx,2), %r13w
	je	.L114
	leal	1(%rax), %ecx
	cmpl	%eax, %r14d
	jle	.L117
.L116:
	movl	%ebx, %edx
	movl	%r15d, %esi
	subl	%ecx, %edx
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	leaq	(%r12,%rcx,2), %rdi
	addq	%rdx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L120
.L117:
	movl	$-1, %r8d
.L114:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24494:
	.size	_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v86String26ExternalStringResourceBase7DisposeEv,"axG",@progbits,_ZN2v86String26ExternalStringResourceBase7DisposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v86String26ExternalStringResourceBase7DisposeEv
	.type	_ZN2v86String26ExternalStringResourceBase7DisposeEv, @function
_ZN2v86String26ExternalStringResourceBase7DisposeEv:
.LFB3805:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZN2v86String26ExternalStringResourceBaseD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L122
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L122:
	jmp	*%rax
	.cfi_endproc
.LFE3805:
	.size	_ZN2v86String26ExternalStringResourceBase7DisposeEv, .-_ZN2v86String26ExternalStringResourceBase7DisposeEv
	.section	.text._ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi:
.LFB24483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movl	%ecx, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	8(%rax), %r12
	movq	16(%rax), %rax
	subl	%eax, %ebx
	cmpl	%ebx, %ecx
	jg	.L128
	movl	%eax, %ecx
	movzbl	(%r12), %r14d
	leal	1(%rbx), %eax
	movq	%rsi, %r15
	movl	%eax, -52(%rbp)
	leal	-1(%rcx), %eax
	movl	%eax, -56(%rbp)
	movl	%r14d, %r13d
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L142:
	subq	%r15, %rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	addq	%r15, %rdx
	cmpb	(%rdx), %r13b
	je	.L129
	leal	1(%rax), %edi
	cmpl	%eax, %ebx
	jle	.L128
.L130:
	movl	-52(%rbp), %edx
	movl	%r14d, %esi
	subl	%edi, %edx
	movslq	%edi, %rdi
	addq	%r15, %rdi
	movslq	%edx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L142
.L128:
	movl	$-1, %r9d
.L123:
	addq	$24, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L128
	leal	1(%rax), %edi
	xorl	%eax, %eax
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L143:
	addq	$1, %rax
	cmpl	%eax, -56(%rbp)
	jle	.L123
.L132:
	movzbl	1(%rdx,%rax), %ecx
	cmpb	%cl, 1(%r12,%rax)
	je	.L143
	cmpl	%edi, %ebx
	jge	.L130
	jmp	.L128
	.cfi_endproc
.LFE24483:
	.size	_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi:
.LFB24491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movl	%ecx, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	8(%rax), %r13
	movq	16(%rax), %rax
	subl	%eax, %ebx
	cmpl	%ebx, %ecx
	jg	.L149
	movl	%eax, %ecx
	movzwl	0(%r13), %eax
	movq	%rsi, %r15
	leal	1(%rbx), %esi
	movl	%esi, -52(%rbp)
	movzbl	%ah, %edx
	movl	%eax, %r14d
	cmpb	%dl, %al
	cmovbe	%edx, %eax
	movzbl	%al, %r12d
	leal	-1(%rcx), %eax
	movl	%eax, -56(%rbp)
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L163:
	subq	%r15, %rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	addq	%r15, %rdx
	cmpb	%r14b, (%rdx)
	je	.L150
	leal	1(%rax), %edi
	cmpl	%eax, %ebx
	jle	.L149
.L151:
	movl	-52(%rbp), %edx
	movl	%r12d, %esi
	subl	%edi, %edx
	movslq	%edi, %rdi
	addq	%r15, %rdi
	movslq	%edx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L163
.L149:
	movl	$-1, %r9d
.L144:
	addq	$24, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L149
	leal	1(%rax), %edi
	xorl	%eax, %eax
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L164:
	addq	$1, %rax
	cmpl	%eax, -56(%rbp)
	jle	.L144
.L153:
	movzbl	1(%rdx,%rax), %ecx
	cmpw	%cx, 2(%r13,%rax,2)
	je	.L164
	cmpl	%edi, %ebx
	jge	.L151
	jmp	.L149
	.cfi_endproc
.LFE24491:
	.size	_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi:
.LFB24487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	16(%rdi), %rax
	movq	8(%rdi), %r13
	subl	%eax, %ebx
	cmpl	%ebx, %ecx
	jg	.L170
	movzbl	0(%r13), %r14d
	movq	%rsi, %r15
	movl	%eax, %esi
	leal	1(%rbx), %eax
	movl	%eax, -52(%rbp)
	leal	-1(%rsi), %eax
	movl	%eax, -56(%rbp)
	movl	%r14d, %r12d
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L184:
	andq	$-2, %rax
	subq	%r15, %rax
	sarq	%rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	leaq	(%r15,%rdx,2), %rdx
	cmpw	%r12w, (%rdx)
	je	.L171
	leal	1(%rax), %ecx
	cmpl	%eax, %ebx
	jle	.L170
.L172:
	movl	-52(%rbp), %edx
	movl	%r14d, %esi
	subl	%ecx, %edx
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	leaq	(%r15,%rcx,2), %rdi
	addq	%rdx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L184
.L170:
	movl	$-1, %r9d
.L165:
	addq	$24, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L170
	leal	1(%rax), %ecx
	xorl	%eax, %eax
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L185:
	addq	$1, %rax
	cmpl	%eax, -56(%rbp)
	jle	.L165
.L174:
	movzbl	1(%r13,%rax), %edi
	movzwl	2(%rdx,%rax,2), %esi
	cmpl	%esi, %edi
	je	.L185
	cmpl	%ecx, %ebx
	jge	.L172
	jmp	.L170
	.cfi_endproc
.LFE24487:
	.size	_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi:
.LFB24495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	16(%rdi), %rax
	movq	8(%rdi), %r14
	subl	%eax, %ebx
	cmpl	%ebx, %ecx
	jg	.L191
	movzwl	(%r14), %r13d
	movl	%eax, %edi
	leal	1(%rbx), %eax
	movq	%rsi, %r15
	movl	%eax, -52(%rbp)
	movslq	%ecx, %rsi
	movl	%r13d, %eax
	movzbl	%ah, %eax
	cmpb	%al, %r13b
	cmova	%r13d, %eax
	movzbl	%al, %r12d
	leal	-1(%rdi), %eax
	movl	%eax, -56(%rbp)
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L205:
	andq	$-2, %rax
	subq	%r15, %rax
	sarq	%rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	cmpw	%r13w, (%r15,%rdx,2)
	je	.L192
	leal	1(%rax), %ecx
	cmpl	%eax, %ebx
	jle	.L191
	movslq	%ecx, %rsi
.L193:
	movl	-52(%rbp), %edx
	leaq	(%r15,%rsi,2), %rdi
	movl	%r12d, %esi
	subl	%ecx, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L205
.L191:
	movl	$-1, %r9d
.L186:
	addq	$24, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L191
	leal	1(%rax), %ecx
	xorl	%eax, %eax
	movslq	%ecx, %rsi
	leaq	(%r15,%rsi,2), %rdx
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L206:
	addq	$1, %rax
	cmpl	%eax, -56(%rbp)
	jle	.L186
.L195:
	movzwl	(%rdx,%rax,2), %edi
	cmpw	%di, 2(%r14,%rax,2)
	je	.L206
	cmpl	%ecx, %ebx
	jge	.L193
	jmp	.L191
	.cfi_endproc
.LFE24495:
	.size	_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN7unibrow4Utf86EncodeEPcjib.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN7unibrow4Utf86EncodeEPcjib.part.0, @function
_ZN7unibrow4Utf86EncodeEPcjib.part.0:
.LFB25135:
	.cfi_startproc
	sall	$10, %edx
	andl	$1023, %esi
	andl	$1047552, %edx
	orl	%esi, %edx
	leal	65536(%rdx), %esi
	movl	%esi, %eax
	shrl	$18, %eax
	orl	$-16, %eax
	movb	%al, -3(%rdi)
	movl	%esi, %eax
	shrl	$12, %eax
	andl	$63, %eax
	orl	$-128, %eax
	movb	%al, -2(%rdi)
	movl	%esi, %eax
	andl	$63, %esi
	shrl	$6, %eax
	orl	$-128, %esi
	andl	$63, %eax
	movb	%sil, (%rdi)
	orl	$-128, %eax
	movb	%al, -1(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE25135:
	.size	_ZN7unibrow4Utf86EncodeEPcjib.part.0, .-_ZN7unibrow4Utf86EncodeEPcjib.part.0
	.section	.text._ZN2v88internal16FlatStringReader21PostGarbageCollectionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16FlatStringReader21PostGarbageCollectionEv
	.type	_ZN2v88internal16FlatStringReader21PostGarbageCollectionEv, @function
_ZN2v88internal16FlatStringReader21PostGarbageCollectionEv:
.LFB19729:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L227
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rax), %rsi
	movq	%rdi, %rbx
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %ecx
	movl	%ecx, %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L230
	leaq	15(%rsi), %rax
	cmpw	$3, %dx
	je	.L214
	movzwl	%dx, %edx
	xorl	%r12d, %r12d
.L213:
	cmpl	$5, %edx
	je	.L231
.L215:
	andl	$8, %ecx
	je	.L216
	testl	%edx, %edx
	jne	.L232
	addq	%r12, %rax
	movl	$1, %edx
.L212:
	movb	%dl, 32(%rbx)
	movq	%rax, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	movq	23(%rsi), %rcx
	xorl	%edx, %edx
	xorl	%eax, %eax
	movl	11(%rcx), %ecx
	testl	%ecx, %ecx
	jne	.L212
	movq	15(%rsi), %rax
	xorl	%r12d, %r12d
	movq	-1(%rax), %rdx
	addq	$15, %rax
	movzwl	11(%rdx), %ecx
	movl	%ecx, %edx
	andl	$7, %edx
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L227:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	testl	%edx, %edx
	jne	.L233
	leaq	(%rax,%r12,2), %rax
	xorl	%edx, %edx
.L234:
	movb	%dl, 32(%rbx)
	movq	%rax, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_restore_state
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	xorl	%edx, %edx
	leaq	(%rax,%r12,2), %rax
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L232:
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movl	$1, %edx
	addq	%r12, %rax
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L231:
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	addq	$15, %rax
	movzwl	11(%rdx), %ecx
	movl	%ecx, %edx
	andl	$7, %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L214:
	movq	15(%rsi), %rax
	movq	-1(%rax), %rdx
	movslq	27(%rsi), %r12
	addq	$15, %rax
	movzwl	11(%rdx), %ecx
	movl	%ecx, %edx
	andl	$7, %edx
	jmp	.L213
	.cfi_endproc
.LFE19729:
	.size	_ZN2v88internal16FlatStringReader21PostGarbageCollectionEv, .-_ZN2v88internal16FlatStringReader21PostGarbageCollectionEv
	.section	.text._ZN2v88internal6String8MakeThinEPNS0_7IsolateES1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String8MakeThinEPNS0_7IsolateES1_
	.type	_ZN2v88internal6String8MakeThinEPNS0_7IsolateES1_, @function
_ZN2v88internal6String8MakeThinEPNS0_7IsolateES1_:
.LFB19662:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	37592(%rsi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-1(%r13), %rax
	cmpw	$63, 11(%rax)
	jbe	.L236
.L314:
	leaq	-1(%r13), %rax
.L237:
	movq	(%rax), %rax
	movq	%rbx, %rdi
	movzwl	11(%rax), %eax
	movw	%ax, -72(%rbp)
	movq	-1(%r13), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	(%rbx), %rsi
	leaq	-57(%rbp), %rcx
	movq	%r15, %rdi
	movl	%eax, %edx
	movl	%eax, %r13d
	call	_ZN2v88internal4Heap24NotifyObjectLayoutChangeENS0_10HeapObjectEiRKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	-1(%r12), %rax
	testb	$8, 11(%rax)
	je	.L268
	movq	768(%r14), %rdx
.L269:
	movq	(%rbx), %rax
	movq	%rdx, -1(%rax)
	movq	(%rbx), %rdi
	testq	%rdx, %rdx
	jne	.L270
.L271:
	movq	%r12, 15(%rdi)
	leaq	15(%rdi), %r14
	testb	$1, %r12b
	je	.L278
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L316
	testb	$24, %al
	je	.L278
.L319:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L317
	.p2align 4,,10
	.p2align 3
.L278:
	movl	%r13d, %edx
	subl	$24, %edx
	je	.L235
	movzbl	-72(%rbp), %r14d
	leaq	23(%rdi), %rsi
	movl	$1, %r8d
	movq	%r15, %rdi
	andl	$1, %r14d
	xorl	$1, %r14d
	movzbl	%r14b, %ecx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
.L235:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L318
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	movq	776(%r14), %rdx
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L316:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-80(%rbp), %rdi
	testb	$24, %al
	jne	.L319
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L236:
	movq	-1(%r13), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$2, %ax
	jne	.L314
	movq	-1(%rdx), %rax
	cmpw	$63, 11(%rax)
	ja	.L239
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$2, %ax
	je	.L320
.L239:
	movq	-1(%r12), %rax
	cmpw	$63, 11(%rax)
	ja	.L251
	movq	-1(%r12), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$2, %ax
	je	.L321
.L251:
	movq	(%rbx), %r13
	movq	-1(%r13), %rax
	movq	%r13, %rdx
	andq	$-262144, %rdx
	testb	$8, 11(%rax)
	sete	%al
	movzbl	%al, %eax
	addl	$1, %eax
	imull	11(%r13), %eax
	cltq
	lock subq	%rax, 208(%rdx)
	movq	80(%rdx), %rdx
	movq	48(%rdx), %rcx
	lock subq	%rax, 8(%rcx)
	movq	64(%rdx), %rdx
	lock subq	%rax, 176(%rdx)
	movq	15(%r13), %rdi
	testq	%rdi, %rdi
	je	.L264
	movq	(%rdi), %rax
	leaq	_ZN2v86String26ExternalStringResourceBase7DisposeEv(%rip), %rcx
	movq	24(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L265
	movq	8(%rax), %rax
	leaq	_ZN2v86String26ExternalStringResourceBaseD0Ev(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L266
.L315:
	movl	$8, %esi
	call	_ZdlPvm@PLT
.L267:
	movq	$0, 15(%r13)
.L264:
	movq	(%rbx), %r13
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L270:
	testb	$1, %dl
	je	.L271
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L271
	xorl	%esi, %esi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L317:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rdi
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L321:
	movq	-1(%r12), %rax
	testb	$8, 11(%rax)
	jne	.L251
	movq	(%rbx), %r8
	movq	15(%r12), %rdx
	movq	15(%r8), %rdi
	leaq	-1(%r8), %rax
	testq	%rdx, %rdx
	je	.L322
	cmpq	%rdx, %rdi
	je	.L237
	movq	-1(%r8), %rax
	movq	%r8, %rdx
	andq	$-262144, %rdx
	testb	$8, 11(%rax)
	sete	%al
	movzbl	%al, %eax
	addl	$1, %eax
	imull	11(%r8), %eax
	cltq
	lock subq	%rax, 208(%rdx)
	movq	80(%rdx), %rdx
	movq	48(%rdx), %rcx
	lock subq	%rax, 8(%rcx)
	movq	64(%rdx), %rdx
	lock subq	%rax, 176(%rdx)
	movq	15(%r8), %rdi
	testq	%rdi, %rdi
	je	.L264
	movq	(%rdi), %rdx
	leaq	_ZN2v86String26ExternalStringResourceBase7DisposeEv(%rip), %rcx
	movq	24(%rdx), %rax
	cmpq	%rcx, %rax
	jne	.L260
	movq	8(%rdx), %rax
	leaq	_ZN2v86String26ExternalStringResourceBaseD0Ev(%rip), %rdx
	movq	%r8, -72(%rbp)
	cmpq	%rdx, %rax
	jne	.L261
	movl	$8, %esi
	call	_ZdlPvm@PLT
	movq	-72(%rbp), %r8
.L262:
	movq	$0, 15(%r8)
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L320:
	movq	-1(%rdx), %rax
	testb	$8, 11(%rax)
	je	.L239
	movq	15(%rdx), %rdx
	movq	15(%r13), %rdi
	leaq	-1(%r13), %rax
	testq	%rdx, %rdx
	je	.L323
	cmpq	%rdx, %rdi
	je	.L237
	movq	-1(%r13), %rax
	movq	%r13, %rdx
	andq	$-262144, %rdx
	testb	$8, 11(%rax)
	sete	%al
	movzbl	%al, %eax
	addl	$1, %eax
	imull	11(%r13), %eax
	cltq
	lock subq	%rax, 208(%rdx)
	movq	80(%rdx), %rdx
	movq	48(%rdx), %rcx
	lock subq	%rax, 8(%rcx)
	movq	64(%rdx), %rdx
	lock subq	%rax, 176(%rdx)
	movq	15(%r13), %rdi
	testq	%rdi, %rdi
	je	.L264
	movq	(%rdi), %rdx
	leaq	_ZN2v86String26ExternalStringResourceBase7DisposeEv(%rip), %rcx
	movq	24(%rdx), %rax
	cmpq	%rcx, %rax
	jne	.L248
	movq	8(%rdx), %rax
	leaq	_ZN2v86String26ExternalStringResourceBaseD0Ev(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L315
	.p2align 4,,10
	.p2align 3
.L266:
	call	*%rax
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L265:
	call	*%rdx
	jmp	.L267
.L323:
	movq	%rdi, 15(%r12)
	testq	%rdi, %rdi
	je	.L242
	movq	-1(%r12), %rax
	testb	$16, 11(%rax)
	jne	.L243
	movq	(%rdi), %rax
	movq	%rdi, -72(%rbp)
	call	*48(%rax)
	movq	-72(%rbp), %rdi
	movq	%rax, 23(%r12)
.L243:
	movq	(%rdi), %rax
	call	*56(%rax)
	testq	%rax, %rax
	jne	.L324
.L242:
	movq	-1(%r13), %rax
	movq	%r13, %rsi
	movq	%r15, %rdi
	testb	$8, 11(%rax)
	sete	%al
	xorl	%ecx, %ecx
	movzbl	%al, %eax
	addl	$1, %eax
	imull	11(%r13), %eax
	movslq	%eax, %rdx
	call	_ZN2v88internal4Heap20UpdateExternalStringENS0_6StringEmm@PLT
	movq	$0, 15(%r13)
	movq	(%rbx), %r13
	leaq	-1(%r13), %rax
	jmp	.L237
.L322:
	movq	%rdi, 15(%r12)
	testq	%rdi, %rdi
	je	.L254
	movq	-1(%r12), %rax
	testb	$16, 11(%rax)
	jne	.L255
	movq	(%rdi), %rax
	movq	%r8, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	*48(%rax)
	movq	-72(%rbp), %rdi
	movq	-80(%rbp), %r8
	movq	%rax, 23(%r12)
.L255:
	movq	(%rdi), %rax
	movq	%r8, -72(%rbp)
	call	*56(%rax)
	movq	-72(%rbp), %r8
	addq	%rax, %rax
	movq	%rax, %rcx
	jne	.L325
.L254:
	movq	-1(%r8), %rax
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r8, -72(%rbp)
	testb	$8, 11(%rax)
	sete	%al
	xorl	%ecx, %ecx
	movzbl	%al, %eax
	addl	$1, %eax
	imull	11(%r8), %eax
	movslq	%eax, %rdx
	call	_ZN2v88internal4Heap20UpdateExternalStringENS0_6StringEmm@PLT
	movq	-72(%rbp), %r8
	movq	$0, 15(%r8)
	movq	(%rbx), %r13
	leaq	-1(%r13), %rax
	jmp	.L237
.L324:
	movq	%rax, %rcx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4Heap20UpdateExternalStringENS0_6StringEmm@PLT
	jmp	.L242
.L325:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal4Heap20UpdateExternalStringENS0_6StringEmm@PLT
	movq	-72(%rbp), %r8
	jmp	.L254
.L248:
	call	*%rax
	jmp	.L267
.L260:
	movq	%r8, -72(%rbp)
	call	*%rax
	movq	-72(%rbp), %r8
	jmp	.L262
.L318:
	call	__stack_chk_fail@PLT
.L261:
	call	*%rax
	movq	-72(%rbp), %r8
	jmp	.L262
	.cfi_endproc
.LFE19662:
	.size	_ZN2v88internal6String8MakeThinEPNS0_7IsolateES1_, .-_ZN2v88internal6String8MakeThinEPNS0_7IsolateES1_
	.section	.text._ZN2v88internal6String23SupportsExternalizationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String23SupportsExternalizationEv
	.type	_ZN2v88internal6String23SupportsExternalizationEv, @function
_ZN2v88internal6String23SupportsExternalizationEv:
.LFB19665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L336
.L327:
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	testb	$32, 10(%rdx)
	je	.L329
.L330:
	xorl	%eax, %eax
.L326:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L337
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L329:
	.cfi_restore_state
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$2, %ax
	je	.L330
	movq	24(%rdx), %rax
	movl	396(%rax), %eax
	testl	%eax, %eax
	setle	%al
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L336:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$5, %dx
	jne	.L327
	movq	15(%rax), %rax
	leaq	-16(%rbp), %rdi
	movq	%rax, -16(%rbp)
	call	_ZN2v88internal6String23SupportsExternalizationEv
	jmp	.L326
.L337:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19665:
	.size	_ZN2v88internal6String23SupportsExternalizationEv, .-_ZN2v88internal6String23SupportsExternalizationEv
	.section	.text._ZN2v88internal6String10LooksValidEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String10LooksValidEv
	.type	_ZN2v88internal6String10LooksValidEv, @function
_ZN2v88internal6String10LooksValidEv:
.LFB19669:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v88internal12ReadOnlyHeap8ContainsENS0_10HeapObjectE@PLT
	testb	%al, %al
	jne	.L338
	andq	$-262144, %r12
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L338
	movq	(%rbx), %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4Heap8ContainsENS0_10HeapObjectE@PLT
	.p2align 4,,10
	.p2align 3
.L338:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19669:
	.size	_ZN2v88internal6String10LooksValidEv, .-_ZN2v88internal6String10LooksValidEv
	.section	.text._ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	.type	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE, @function
_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE:
.LFB19673:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rdx
	movl	11(%rdx), %ebx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %esi
	movl	%esi, %ecx
	andl	$7, %ecx
	cmpw	$1, %cx
	je	.L361
	leaq	15(%rdx), %rax
	cmpw	$3, %cx
	je	.L349
	movzwl	%cx, %ecx
	xorl	%r12d, %r12d
.L348:
	cmpl	$5, %ecx
	je	.L362
.L350:
	andl	$8, %esi
	je	.L351
	testl	%ecx, %ecx
	jne	.L363
.L352:
	movl	%ebx, %edx
	addq	%r12, %rax
	popq	%rbx
	popq	%r12
	btsq	$32, %rdx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L351:
	.cfi_restore_state
	testl	%ecx, %ecx
	jne	.L364
	movl	%ebx, %edx
	leaq	(%rax,%r12,2), %rax
	btsq	$33, %rdx
.L365:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L361:
	.cfi_restore_state
	movq	23(%rdx), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	je	.L346
	popq	%rbx
	xorl	%eax, %eax
	xorl	%edx, %edx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L363:
	.cfi_restore_state
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L364:
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movl	%ebx, %edx
	leaq	(%rax,%r12,2), %rax
	btsq	$33, %rdx
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L362:
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	addq	$15, %rax
	movzwl	11(%rdx), %esi
	movl	%esi, %ecx
	andl	$7, %ecx
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L349:
	movq	15(%rdx), %rax
	movq	-1(%rax), %rcx
	movslq	27(%rdx), %r12
	addq	$15, %rax
	movzwl	11(%rcx), %esi
	movl	%esi, %ecx
	andl	$7, %ecx
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L346:
	movq	15(%rdx), %rax
	xorl	%r12d, %r12d
	movq	-1(%rax), %rdx
	addq	$15, %rax
	movzwl	11(%rdx), %esi
	movl	%esi, %ecx
	andl	$7, %ecx
	jmp	.L348
	.cfi_endproc
.LFE19673:
	.size	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE, .-_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	.section	.text._ZN2v88internal6String9IsEqualToIhEEbNS0_6VectorIKT_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String9IsEqualToIhEEbNS0_6VectorIKT_EE
	.type	_ZN2v88internal6String9IsEqualToIhEEbNS0_6VectorIKT_EE, @function
_ZN2v88internal6String9IsEqualToIhEEbNS0_6VectorIKT_EE:
.LFB19706:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	11(%rax), %r12d
	xorl	%eax, %eax
	cmpl	%edx, %r12d
	je	.L377
.L367:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L378
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L377:
	.cfi_restore_state
	movq	%rsi, %r13
	movq	%rsi, %rbx
	leaq	-41(%rbp), %rsi
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	movq	%rax, %rdi
	movq	%rdx, %rax
	movslq	%r12d, %rdx
	shrq	$32, %rax
	movq	%rdi, %rcx
	cmpl	$1, %eax
	je	.L379
	leaq	(%rdi,%rdx,2), %rdx
	cmpq	%rdx, %rdi
	jb	.L370
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L380:
	addq	$2, %rcx
	addq	$1, %rbx
	cmpq	%rcx, %rdx
	jbe	.L373
.L370:
	movzbl	(%rbx), %eax
	cmpw	%ax, (%rcx)
	je	.L380
	xorl	%eax, %eax
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L379:
	movq	%r13, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	sete	%al
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L373:
	movl	$1, %eax
	jmp	.L367
.L378:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19706:
	.size	_ZN2v88internal6String9IsEqualToIhEEbNS0_6VectorIKT_EE, .-_ZN2v88internal6String9IsEqualToIhEEbNS0_6VectorIKT_EE
	.section	.text._ZN2v88internal6String9IsEqualToItEEbNS0_6VectorIKT_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String9IsEqualToItEEbNS0_6VectorIKT_EE
	.type	_ZN2v88internal6String9IsEqualToItEEbNS0_6VectorIKT_EE, @function
_ZN2v88internal6String9IsEqualToItEEbNS0_6VectorIKT_EE:
.LFB19707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movslq	11(%rax), %r12
	xorl	%eax, %eax
	cmpl	%edx, %r12d
	je	.L399
.L382:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L400
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L399:
	.cfi_restore_state
	movq	%rsi, %rbx
	movq	%rsi, %r13
	leaq	-41(%rbp), %rsi
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	shrq	$32, %rdx
	movq	%rax, %rcx
	cmpl	$1, %edx
	je	.L401
	leaq	(%rax,%r12,2), %rdx
	cmpq	%rdx, %rax
	jb	.L387
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L402:
	addq	$2, %rcx
	addq	$2, %r13
	cmpq	%rcx, %rdx
	jbe	.L392
.L387:
	movzwl	0(%r13), %eax
	cmpw	%ax, (%rcx)
	je	.L402
	xorl	%eax, %eax
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L392:
	movl	$1, %eax
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L401:
	leaq	(%rax,%r12), %rdx
	cmpq	%rdx, %rax
	jnb	.L392
	xorl	%edx, %edx
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L403:
	addq	$1, %rdx
	cmpq	%r12, %rdx
	je	.L392
.L385:
	movzbl	(%rax,%rdx), %esi
	movzwl	(%rbx,%rdx,2), %ecx
	cmpl	%ecx, %esi
	je	.L403
	xorl	%eax, %eax
	jmp	.L382
.L400:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19707:
	.size	_ZN2v88internal6String9IsEqualToItEEbNS0_6VectorIKT_EE, .-_ZN2v88internal6String9IsEqualToItEEbNS0_6VectorIKT_EE
	.section	.text._ZN2v88internal6String16HasOneBytePrefixENS0_6VectorIKcEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String16HasOneBytePrefixENS0_6VectorIKcEE
	.type	_ZN2v88internal6String16HasOneBytePrefixENS0_6VectorIKcEE, @function
_ZN2v88internal6String16HasOneBytePrefixENS0_6VectorIKcEE:
.LFB19708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	11(%rdx), %r12d
	jle	.L415
.L405:
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L416
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L415:
	.cfi_restore_state
	movq	%rsi, %r13
	movq	%rsi, %rbx
	leaq	-41(%rbp), %rsi
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	movq	%rax, %rdi
	movq	%rdx, %rax
	movslq	%r12d, %rdx
	shrq	$32, %rax
	movq	%rdi, %rcx
	cmpl	$1, %eax
	je	.L417
	leaq	(%rdi,%rdx,2), %rdx
	cmpq	%rdx, %rdi
	jb	.L408
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L418:
	addq	$2, %rcx
	addq	$1, %rbx
	cmpq	%rcx, %rdx
	jbe	.L411
.L408:
	movzbl	(%rbx), %eax
	cmpw	%ax, (%rcx)
	je	.L418
	xorl	%eax, %eax
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L417:
	movq	%r13, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	sete	%al
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L411:
	movl	$1, %eax
	jmp	.L405
.L416:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19708:
	.size	_ZN2v88internal6String16HasOneBytePrefixENS0_6VectorIKcEE, .-_ZN2v88internal6String16HasOneBytePrefixENS0_6VectorIKcEE
	.section	.text._ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE
	.type	_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE, @function
_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE:
.LFB19709:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	%r12d, 11(%rdx)
	je	.L430
.L420:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L431
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L430:
	.cfi_restore_state
	movq	%rsi, %r13
	movq	%rsi, %rbx
	leaq	-41(%rbp), %rsi
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	movq	%rax, %rdi
	movq	%rdx, %rax
	movslq	%r12d, %rdx
	shrq	$32, %rax
	movq	%rdi, %rcx
	cmpl	$1, %eax
	je	.L432
	leaq	(%rdi,%rdx,2), %rdx
	cmpq	%rdx, %rdi
	jb	.L423
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L433:
	addq	$2, %rcx
	addq	$1, %rbx
	cmpq	%rcx, %rdx
	jbe	.L426
.L423:
	movzbl	(%rbx), %eax
	cmpw	%ax, (%rcx)
	je	.L433
	xorl	%eax, %eax
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L432:
	movq	%r13, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	sete	%al
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L426:
	movl	$1, %eax
	jmp	.L420
.L431:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19709:
	.size	_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE, .-_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE
	.section	.text._ZN2v88internal6String16IsTwoByteEqualToENS0_6VectorIKtEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String16IsTwoByteEqualToENS0_6VectorIKtEE
	.type	_ZN2v88internal6String16IsTwoByteEqualToENS0_6VectorIKtEE, @function
_ZN2v88internal6String16IsTwoByteEqualToENS0_6VectorIKtEE:
.LFB19710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	%ebx, 11(%rdx)
	je	.L452
.L435:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L453
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L452:
	.cfi_restore_state
	movq	%rsi, %r12
	movq	%rsi, %r13
	leaq	-41(%rbp), %rsi
	movslq	%ebx, %rbx
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	shrq	$32, %rdx
	movq	%rax, %rcx
	cmpl	$1, %edx
	je	.L454
	leaq	(%rax,%rbx,2), %rdx
	cmpq	%rdx, %rax
	jb	.L440
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L455:
	addq	$2, %rcx
	addq	$2, %r13
	cmpq	%rcx, %rdx
	jbe	.L445
.L440:
	movzwl	0(%r13), %eax
	cmpw	%ax, (%rcx)
	je	.L455
	xorl	%eax, %eax
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L445:
	movl	$1, %eax
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L454:
	leaq	(%rax,%rbx), %rdx
	cmpq	%rdx, %rax
	jnb	.L445
	xorl	%ecx, %ecx
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L456:
	addq	$1, %rcx
	cmpq	%rbx, %rcx
	je	.L445
.L438:
	movzbl	(%rax,%rcx), %esi
	movzwl	(%r12,%rcx,2), %edx
	cmpl	%edx, %esi
	je	.L456
	xorl	%eax, %eax
	jmp	.L435
.L453:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19710:
	.size	_ZN2v88internal6String16IsTwoByteEqualToENS0_6VectorIKtEE, .-_ZN2v88internal6String16IsTwoByteEqualToENS0_6VectorIKtEE
	.section	.text._ZN2v88internal9SeqString8TruncateENS0_6HandleIS1_EEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9SeqString8TruncateENS0_6HandleIS1_EEi
	.type	_ZN2v88internal9SeqString8TruncateENS0_6HandleIS1_EEi, @function
_ZN2v88internal9SeqString8TruncateENS0_6HandleIS1_EEi:
.LFB19716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	testl	%esi, %esi
	je	.L469
	movl	11(%rax), %edx
	movq	%rdi, %rbx
	movl	%esi, %r12d
	cmpl	%edx, %esi
	jl	.L460
.L468:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L460:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$63, 11(%rcx)
	jbe	.L461
.L463:
	leal	23(%rdx,%rdx), %edx
	leal	23(%r12,%r12), %ecx
	andl	$-8, %edx
	andl	$-8, %ecx
.L462:
	subl	%ecx, %edx
	movslq	%ecx, %rcx
	movl	$1, %r8d
	leaq	-1(%rcx,%rax), %rsi
	andq	$-262144, %rax
	movl	$1, %ecx
	movq	24(%rax), %rdi
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	(%rbx), %rax
	movl	%r12d, 11(%rax)
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L469:
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	leaq	-37464(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	testb	$7, 11(%rcx)
	jne	.L463
	movq	-1(%rax), %rcx
	testb	$8, 11(%rcx)
	je	.L463
	addl	$23, %edx
	leal	23(%rsi), %ecx
	andl	$-8, %edx
	andl	$-8, %ecx
	jmp	.L462
	.cfi_endproc
.LFE19716:
	.size	_ZN2v88internal9SeqString8TruncateENS0_6HandleIS1_EEi, .-_ZN2v88internal9SeqString8TruncateENS0_6HandleIS1_EEi
	.section	.text._ZN2v88internal16SeqOneByteString13clear_paddingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16SeqOneByteString13clear_paddingEv
	.type	_ZN2v88internal16SeqOneByteString13clear_paddingEv, @function
_ZN2v88internal16SeqOneByteString13clear_paddingEv:
.LFB19717:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rsi
	movl	11(%rsi), %eax
	leal	23(%rax), %edx
	leal	16(%rax), %ecx
	andl	$-8, %edx
	subl	%ecx, %edx
	movslq	%ecx, %rcx
	leaq	-1(%rsi,%rcx), %rdi
	movslq	%edx, %rdx
	xorl	%esi, %esi
	jmp	memset@PLT
	.cfi_endproc
.LFE19717:
	.size	_ZN2v88internal16SeqOneByteString13clear_paddingEv, .-_ZN2v88internal16SeqOneByteString13clear_paddingEv
	.section	.text._ZN2v88internal16SeqTwoByteString13clear_paddingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16SeqTwoByteString13clear_paddingEv
	.type	_ZN2v88internal16SeqTwoByteString13clear_paddingEv, @function
_ZN2v88internal16SeqTwoByteString13clear_paddingEv:
.LFB19718:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	xorl	%esi, %esi
	movl	11(%rcx), %eax
	leal	16(%rax,%rax), %eax
	leal	7(%rax), %edx
	andl	$-8, %edx
	subl	%eax, %edx
	cltq
	movslq	%edx, %rdx
	leaq	-1(%rcx,%rax), %rdi
	jmp	memset@PLT
	.cfi_endproc
.LFE19718:
	.size	_ZN2v88internal16SeqTwoByteString13clear_paddingEv, .-_ZN2v88internal16SeqTwoByteString13clear_paddingEv
	.section	.text._ZNK2v88internal14ExternalString19ExternalPayloadSizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14ExternalString19ExternalPayloadSizeEv
	.type	_ZNK2v88internal14ExternalString19ExternalPayloadSizeEv, @function
_ZNK2v88internal14ExternalString19ExternalPayloadSizeEv:
.LFB19722:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movq	-1(%rdx), %rax
	testb	$8, 11(%rax)
	sete	%al
	movzbl	%al, %eax
	addl	$1, %eax
	imull	11(%rdx), %eax
	ret
	.cfi_endproc
.LFE19722:
	.size	_ZNK2v88internal14ExternalString19ExternalPayloadSizeEv, .-_ZNK2v88internal14ExternalString19ExternalPayloadSizeEv
	.section	.text._ZN2v88internal16FlatStringReaderC2EPNS0_7IsolateENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16FlatStringReaderC2EPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal16FlatStringReaderC2EPNS0_7IsolateENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal16FlatStringReaderC2EPNS0_7IsolateENS0_6HandleINS0_6StringEEE:
.LFB19724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	41704(%rsi), %rax
	movq	%rdi, %rbx
	movq	%rdi, 41704(%rsi)
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVN2v88internal16FlatStringReaderE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	(%rdx), %rcx
	movq	%rdx, 24(%rdi)
	movq	%rax, (%rdi)
	movl	11(%rcx), %eax
	movl	%eax, 36(%rdi)
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %esi
	movl	%esi, %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L492
	leaq	15(%rcx), %rax
	cmpw	$3, %dx
	je	.L479
	movzwl	%dx, %edx
	xorl	%r12d, %r12d
.L478:
	cmpl	$5, %edx
	je	.L493
.L480:
	andl	$8, %esi
	je	.L481
	testl	%edx, %edx
	jne	.L494
.L482:
	addq	%r12, %rax
	movl	$1, %edx
.L477:
	movb	%dl, 32(%rbx)
	movq	%rax, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L481:
	.cfi_restore_state
	testl	%edx, %edx
	jne	.L495
	leaq	(%rax,%r12,2), %rax
	xorl	%edx, %edx
.L496:
	movb	%dl, 32(%rbx)
	movq	%rax, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L492:
	.cfi_restore_state
	movq	23(%rcx), %rsi
	xorl	%edx, %edx
	xorl	%eax, %eax
	movl	11(%rsi), %esi
	testl	%esi, %esi
	jne	.L477
	movq	15(%rcx), %rax
	xorl	%r12d, %r12d
	movq	-1(%rax), %rdx
	addq	$15, %rax
	movzwl	11(%rdx), %esi
	movl	%esi, %edx
	andl	$7, %edx
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L495:
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	xorl	%edx, %edx
	leaq	(%rax,%r12,2), %rax
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L494:
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L479:
	movq	15(%rcx), %rax
	movq	-1(%rax), %rdx
	movslq	27(%rcx), %r12
	addq	$15, %rax
	movzwl	11(%rdx), %esi
	movl	%esi, %edx
	andl	$7, %edx
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L493:
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	addq	$15, %rax
	movzwl	11(%rdx), %esi
	movl	%esi, %edx
	andl	$7, %edx
	jmp	.L480
	.cfi_endproc
.LFE19724:
	.size	_ZN2v88internal16FlatStringReaderC2EPNS0_7IsolateENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal16FlatStringReaderC2EPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.globl	_ZN2v88internal16FlatStringReaderC1EPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.set	_ZN2v88internal16FlatStringReaderC1EPNS0_7IsolateENS0_6HandleINS0_6StringEEE,_ZN2v88internal16FlatStringReaderC2EPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal16FlatStringReaderC2EPNS0_7IsolateENS0_6VectorIKcEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16FlatStringReaderC2EPNS0_7IsolateENS0_6VectorIKcEE
	.type	_ZN2v88internal16FlatStringReaderC2EPNS0_7IsolateENS0_6VectorIKcEE, @function
_ZN2v88internal16FlatStringReaderC2EPNS0_7IsolateENS0_6VectorIKcEE:
.LFB19727:
	.cfi_startproc
	endbr64
	movq	41704(%rsi), %rax
	movq	%rsi, 8(%rdi)
	movq	%rdi, 41704(%rsi)
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVN2v88internal16FlatStringReaderE(%rip), %rax
	movq	%rax, (%rdi)
	movq	$0, 24(%rdi)
	movb	$1, 32(%rdi)
	movl	%ecx, 36(%rdi)
	movq	%rdx, 40(%rdi)
	ret
	.cfi_endproc
.LFE19727:
	.size	_ZN2v88internal16FlatStringReaderC2EPNS0_7IsolateENS0_6VectorIKcEE, .-_ZN2v88internal16FlatStringReaderC2EPNS0_7IsolateENS0_6VectorIKcEE
	.globl	_ZN2v88internal16FlatStringReaderC1EPNS0_7IsolateENS0_6VectorIKcEE
	.set	_ZN2v88internal16FlatStringReaderC1EPNS0_7IsolateENS0_6VectorIKcEE,_ZN2v88internal16FlatStringReaderC2EPNS0_7IsolateENS0_6VectorIKcEE
	.section	.text._ZN2v88internal18ConsStringIterator10InitializeENS0_10ConsStringEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ConsStringIterator10InitializeENS0_10ConsStringEi
	.type	_ZN2v88internal18ConsStringIterator10InitializeENS0_10ConsStringEi, @function
_ZN2v88internal18ConsStringIterator10InitializeENS0_10ConsStringEi:
.LFB19730:
	.cfi_startproc
	endbr64
	movabsq	$141733920769, %rax
	movq	%rsi, 256(%rdi)
	movl	%edx, 272(%rdi)
	movq	%rax, 264(%rdi)
	ret
	.cfi_endproc
.LFE19730:
	.size	_ZN2v88internal18ConsStringIterator10InitializeENS0_10ConsStringEi, .-_ZN2v88internal18ConsStringIterator10InitializeENS0_10ConsStringEi
	.section	.text._ZN2v88internal18ConsStringIterator6SearchEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ConsStringIterator6SearchEPi
	.type	_ZN2v88internal18ConsStringIterator6SearchEPi, @function
_ZN2v88internal18ConsStringIterator6SearchEPi:
.LFB19732:
	.cfi_startproc
	endbr64
	movq	256(%rdi), %r8
	movl	272(%rdi), %edx
	xorl	%r9d, %r9d
	movabsq	$4294967297, %rax
	movq	%rax, 264(%rdi)
	movq	%r8, (%rdi)
.L500:
	movq	15(%r8), %rax
	movl	11(%rax), %ecx
	addl	%r9d, %ecx
	cmpl	%edx, %ecx
	jle	.L501
	movq	-1(%rax), %r8
	movzwl	11(%r8), %r8d
	andl	$7, %r8d
	cmpw	$1, %r8w
	je	.L509
	movl	264(%rdi), %r8d
	cmpl	268(%rdi), %r8d
	jle	.L504
	movl	%r8d, 268(%rdi)
.L504:
	subl	%r9d, %edx
	movl	%ecx, 272(%rdi)
	movl	%edx, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L501:
	movq	23(%r8), %rax
	movq	-1(%rax), %r8
	movzwl	11(%r8), %r8d
	andl	$7, %r8d
	cmpw	$1, %r8w
	je	.L510
	movl	11(%rax), %r10d
	testl	%r10d, %r10d
	jne	.L506
	movl	$0, 264(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L506:
	movl	264(%rdi), %r8d
	cmpl	268(%rdi), %r8d
	jle	.L508
	movl	%r8d, 268(%rdi)
.L508:
	movl	%ecx, %r9d
	subl	$1, %r8d
	addl	%r10d, %ecx
	subl	%r9d, %edx
	movl	%r8d, 264(%rdi)
	movl	%ecx, 272(%rdi)
	movl	%edx, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L510:
	movl	264(%rdi), %r11d
	movl	%ecx, %r9d
	leal	-1(%r11), %r8d
	andl	$31, %r8d
	movq	%rax, (%rdi,%r8,8)
	movq	%rax, %r8
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L509:
	movl	264(%rdi), %ecx
	leal	1(%rcx), %r8d
	andl	$31, %ecx
	movl	%r8d, 264(%rdi)
	movq	%rax, %r8
	movq	%rax, (%rdi,%rcx,8)
	jmp	.L500
	.cfi_endproc
.LFE19732:
	.size	_ZN2v88internal18ConsStringIterator6SearchEPi, .-_ZN2v88internal18ConsStringIterator6SearchEPi
	.section	.text._ZN2v88internal18ConsStringIterator8NextLeafEPb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ConsStringIterator8NextLeafEPb
	.type	_ZN2v88internal18ConsStringIterator8NextLeafEPb, @function
_ZN2v88internal18ConsStringIterator8NextLeafEPb:
.LFB19733:
	.cfi_startproc
	endbr64
	movl	264(%rdi), %edx
.L531:
	testl	%edx, %edx
	je	.L533
	movl	268(%rdi), %eax
	subl	%edx, %eax
	cmpl	$32, %eax
	je	.L534
	subl	$1, %edx
	andl	$31, %edx
	movq	(%rdi,%rdx,8), %rax
	movq	23(%rax), %rax
	movq	-1(%rax), %rdx
	movl	264(%rdi), %ecx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	leal	-1(%rcx), %edx
	je	.L516
	movl	%edx, 264(%rdi)
	movl	11(%rax), %ecx
	testl	%ecx, %ecx
	je	.L531
.L532:
	addl	%ecx, 272(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L516:
	andl	$31, %edx
	movq	%rax, (%rdi,%rdx,8)
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L518:
	leal	1(%rdx), %ecx
	andl	$31, %edx
	movl	%ecx, 264(%rdi)
	movq	%rax, (%rdi,%rdx,8)
.L520:
	movq	15(%rax), %rax
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	movl	264(%rdi), %edx
	je	.L518
	cmpl	268(%rdi), %edx
	jle	.L519
	movl	%edx, 268(%rdi)
.L519:
	movl	11(%rax), %ecx
	testl	%ecx, %ecx
	je	.L531
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L533:
	movb	$0, (%rsi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L534:
	movb	$1, (%rsi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE19733:
	.size	_ZN2v88internal18ConsStringIterator8NextLeafEPb, .-_ZN2v88internal18ConsStringIterator8NextLeafEPb
	.section	.text._ZN2v88internal18ConsStringIterator8ContinueEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ConsStringIterator8ContinueEPi
	.type	_ZN2v88internal18ConsStringIterator8ContinueEPi, @function
_ZN2v88internal18ConsStringIterator8ContinueEPi:
.LFB19731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	268(%rdi), %eax
	subl	264(%rdi), %eax
	cmpl	$32, %eax
	sete	-9(%rbp)
	jne	.L536
.L538:
	movq	256(%rdi), %rdx
	movl	272(%rdi), %ecx
	xorl	%esi, %esi
	movabsq	$4294967297, %rax
	movq	%rax, 264(%rdi)
	movq	%rdx, (%rdi)
.L537:
	movq	15(%rdx), %rax
	movl	11(%rax), %r8d
	addl	%esi, %r8d
	cmpl	%r8d, %ecx
	jge	.L540
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	movl	264(%rdi), %edx
	je	.L553
	cmpl	268(%rdi), %edx
	jle	.L543
	movl	%edx, 268(%rdi)
.L543:
	subl	%esi, %ecx
	movl	%r8d, 272(%rdi)
	movl	%ecx, (%r9)
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L540:
	movq	23(%rdx), %rax
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L554
	movl	11(%rax), %r10d
	testl	%r10d, %r10d
	je	.L545
	movl	264(%rdi), %edx
	cmpl	268(%rdi), %edx
	jle	.L546
	movl	%edx, 268(%rdi)
.L546:
	subl	$1, %edx
	movl	%r8d, %esi
	addl	%r10d, %r8d
	movl	%edx, 264(%rdi)
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L536:
	leaq	-9(%rbp), %rsi
	call	_ZN2v88internal18ConsStringIterator8NextLeafEPb
	cmpb	$0, -9(%rbp)
	jne	.L538
.L539:
	testq	%rax, %rax
	je	.L545
.L547:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L555
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L545:
	.cfi_restore_state
	movl	$0, 264(%rdi)
	xorl	%eax, %eax
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L553:
	leal	1(%rdx), %r8d
	andl	$31, %edx
	movl	%r8d, 264(%rdi)
	movq	%rax, (%rdi,%rdx,8)
	movq	%rax, %rdx
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L554:
	movl	264(%rdi), %esi
	leal	-1(%rsi), %edx
	movl	%r8d, %esi
	andl	$31, %edx
	movq	%rax, (%rdi,%rdx,8)
	movq	%rax, %rdx
	jmp	.L537
.L555:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19731:
	.size	_ZN2v88internal18ConsStringIterator8ContinueEPi, .-_ZN2v88internal18ConsStringIterator8ContinueEPi
	.section	.rodata._ZN2v88internal6String9PrintUC16ERSoii.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal6String9PrintUC16ERSoii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String9PrintUC16ERSoii
	.type	_ZN2v88internal6String9PrintUC16ERSoii, @function
_ZN2v88internal6String9PrintUC16ERSoii:
.LFB19667:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$360, %rsp
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	11(%rdx), %rsi
	testl	%ecx, %ecx
	jns	.L557
	movl	11(%rdx), %r12d
.L557:
	movl	$32, %ecx
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	movb	$0, -88(%rbp)
	leaq	-368(%rbp), %r15
	movq	$0, -112(%rbp)
	movq	%r15, %rdi
	movl	%ebx, -380(%rbp)
	rep stosq
	movaps	%xmm0, -80(%rbp)
	movl	%ebx, %ecx
	movl	(%rsi), %r14d
	leaq	.L561(%rip), %rsi
.L558:
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L559
	movzwl	%ax, %eax
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6String9PrintUC16ERSoii,"a",@progbits
	.align 4
	.align 4
.L561:
	.long	.L567-.L561
	.long	.L564-.L561
	.long	.L566-.L561
	.long	.L562-.L561
	.long	.L559-.L561
	.long	.L560-.L561
	.long	.L559-.L561
	.long	.L559-.L561
	.long	.L565-.L561
	.long	.L564-.L561
	.long	.L563-.L561
	.long	.L562-.L561
	.long	.L559-.L561
	.long	.L560-.L561
	.section	.text._ZN2v88internal6String9PrintUC16ERSoii
	.p2align 4,,10
	.p2align 3
.L564:
	movl	$0, -104(%rbp)
	movl	-380(%rbp), %eax
	testq	%rdx, %rdx
	je	.L570
	movl	%eax, -96(%rbp)
	leaq	-380(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$141733920769, %rax
	movq	%rdx, -112(%rbp)
	movq	%rax, -104(%rbp)
	movl	$0, -380(%rbp)
	call	_ZN2v88internal18ConsStringIterator6SearchEPi
	testq	%rax, %rax
	je	.L568
	movl	-380(%rbp), %edi
	movl	11(%rax), %r14d
	leaq	.L574(%rip), %rsi
	movl	%edi, %ecx
.L623:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L559
	movzwl	%dx, %edx
	movslq	(%rsi,%rdx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal6String9PrintUC16ERSoii
	.align 4
	.align 4
.L574:
	.long	.L580-.L574
	.long	.L570-.L574
	.long	.L579-.L574
	.long	.L575-.L574
	.long	.L559-.L574
	.long	.L573-.L574
	.long	.L559-.L574
	.long	.L559-.L574
	.long	.L578-.L574
	.long	.L570-.L574
	.long	.L576-.L574
	.long	.L575-.L574
	.long	.L559-.L574
	.long	.L573-.L574
	.section	.text._ZN2v88internal6String9PrintUC16ERSoii
.L576:
	subl	%edi, %r14d
	movq	15(%rax), %rdi
	movl	%ecx, -388(%rbp)
	movslq	%r14d, %r14
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-388(%rbp), %rcx
	movb	$1, -88(%rbp)
	addq	%rcx, %rax
	movq	%rax, -80(%rbp)
	addq	%r14, %rax
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L570:
	leaq	-372(%rbp), %r14
	cmpl	%ebx, %r12d
	jg	.L582
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L616:
	leaq	1(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movzbl	(%rax), %eax
.L638:
	movq	%r14, %rsi
	movq	%r13, %rdi
	addl	$1, %ebx
	movw	%ax, -372(%rbp)
	call	_ZN2v88internallsERSoRKNS0_6AsUC16E@PLT
	cmpl	%ebx, %r12d
	je	.L556
.L582:
	movq	-80(%rbp), %rax
	cmpq	-72(%rbp), %rax
	je	.L584
.L639:
	movzbl	-88(%rbp), %edx
.L585:
	testb	%dl, %dl
	jne	.L616
	leaq	2(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movzwl	(%rax), %eax
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L584:
	movl	$0, -376(%rbp)
	movl	-104(%rbp), %eax
	testl	%eax, %eax
	jne	.L640
.L556:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L641
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L640:
	.cfi_restore_state
	movl	-100(%rbp), %ecx
	subl	%eax, %ecx
	cmpl	$32, %ecx
	sete	-372(%rbp)
	jne	.L587
.L589:
	leaq	-376(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal18ConsStringIterator6SearchEPi
.L588:
	testq	%rax, %rax
	je	.L556
	movslq	11(%rax), %rcx
	xorl	%esi, %esi
.L622:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L559
	leaq	.L592(%rip), %rdi
	movzwl	%dx, %edx
	movslq	(%rdi,%rdx,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal6String9PrintUC16ERSoii
	.align 4
	.align 4
.L592:
	.long	.L598-.L592
	.long	.L595-.L592
	.long	.L597-.L592
	.long	.L593-.L592
	.long	.L559-.L592
	.long	.L591-.L592
	.long	.L559-.L592
	.long	.L559-.L592
	.long	.L596-.L592
	.long	.L595-.L592
	.long	.L594-.L592
	.long	.L593-.L592
	.long	.L559-.L592
	.long	.L591-.L592
	.section	.text._ZN2v88internal6String9PrintUC16ERSoii
	.p2align 4,,10
	.p2align 3
.L591:
	movq	15(%rax), %rax
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L593:
	addl	27(%rax), %esi
	movq	15(%rax), %rax
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L595:
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L599:
	cmpq	%rcx, %rax
	jne	.L639
	movl	$0, -372(%rbp)
	movl	-104(%rbp), %edx
	testl	%edx, %edx
	je	.L639
	movl	-100(%rbp), %eax
	subl	%edx, %eax
	cmpl	$32, %eax
	sete	-376(%rbp)
	jne	.L604
.L606:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal18ConsStringIterator6SearchEPi
.L605:
	testq	%rax, %rax
	je	.L642
	movslq	11(%rax), %rcx
	xorl	%esi, %esi
.L621:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L559
	leaq	.L609(%rip), %rdi
	movzwl	%dx, %edx
	movslq	(%rdi,%rdx,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal6String9PrintUC16ERSoii
	.align 4
	.align 4
.L609:
	.long	.L615-.L609
	.long	.L612-.L609
	.long	.L614-.L609
	.long	.L610-.L609
	.long	.L559-.L609
	.long	.L608-.L609
	.long	.L559-.L609
	.long	.L559-.L609
	.long	.L613-.L609
	.long	.L612-.L609
	.long	.L611-.L609
	.long	.L610-.L609
	.long	.L559-.L609
	.long	.L608-.L609
	.section	.text._ZN2v88internal6String9PrintUC16ERSoii
	.p2align 4,,10
	.p2align 3
.L597:
	movq	15(%rax), %rdi
	movl	%esi, -392(%rbp)
	movl	%ecx, -388(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-392(%rbp), %rsi
	movslq	-388(%rbp), %rcx
	movb	$0, -88(%rbp)
	leaq	(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L598:
	movslq	%esi, %rsi
	movb	$0, -88(%rbp)
	leaq	15(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L594:
	movq	15(%rax), %rdi
	movl	%esi, -392(%rbp)
	movl	%ecx, -388(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-392(%rbp), %rsi
	movslq	-388(%rbp), %rcx
	movb	$1, -88(%rbp)
	addq	%rsi, %rax
	addq	%rax, %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L596:
	movslq	%esi, %rsi
	movb	$1, -88(%rbp)
	leaq	15(%rax,%rsi), %rax
	addq	%rax, %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L587:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal18ConsStringIterator8NextLeafEPb
	cmpb	$0, -372(%rbp)
	je	.L588
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L604:
	leaq	-376(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal18ConsStringIterator8NextLeafEPb
	cmpb	$0, -376(%rbp)
	je	.L605
	jmp	.L606
.L642:
	movl	$0, -104(%rbp)
	movq	-80(%rbp), %rax
	jmp	.L639
.L563:
	movq	15(%rdx), %rdi
	movl	%ecx, -388(%rbp)
	subl	%ebx, %r14d
	movslq	%r14d, %r14
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-388(%rbp), %rcx
	movb	$1, -88(%rbp)
	addq	%rcx, %rax
	movq	%rax, -80(%rbp)
	addq	%r14, %rax
	movq	%rax, -72(%rbp)
.L568:
	movl	$0, -104(%rbp)
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L562:
	addl	27(%rdx), %ecx
	movq	15(%rdx), %rdx
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L560:
	movq	15(%rdx), %rdx
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L610:
	addl	27(%rax), %esi
	movq	15(%rax), %rax
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L608:
	movq	15(%rax), %rax
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L612:
	movzbl	-88(%rbp), %edx
	movq	-80(%rbp), %rax
	jmp	.L585
.L565:
	movslq	%ecx, %rcx
	subl	%ebx, %r14d
	movb	$1, -88(%rbp)
	leaq	15(%rdx,%rcx), %rax
	movslq	%r14d, %rdx
	movl	$0, -104(%rbp)
	movq	%rax, -80(%rbp)
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	jmp	.L570
.L567:
	movslq	%ecx, %rcx
	subl	%ebx, %r14d
	movb	$0, -88(%rbp)
	leaq	15(%rdx,%rcx,2), %rdx
	movslq	%r14d, %rax
	movl	$0, -104(%rbp)
	leaq	(%rdx,%rax,2), %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
	jmp	.L570
.L566:
	movq	15(%rdx), %rdi
	movl	%ecx, -388(%rbp)
	subl	%ebx, %r14d
	movslq	%r14d, %r14
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-388(%rbp), %rcx
	movb	$0, -88(%rbp)
	movl	$0, -104(%rbp)
	leaq	(%rax,%rcx,2), %rax
	movq	%rax, -80(%rbp)
	leaq	(%rax,%r14,2), %rax
	movq	%rax, -72(%rbp)
	jmp	.L570
.L615:
	movslq	%esi, %rsi
	movb	$0, -88(%rbp)
	leaq	15(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rdx
	movq	%rdx, -72(%rbp)
	leaq	2(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movzwl	(%rax), %eax
	jmp	.L638
.L613:
	movslq	%esi, %rsi
	movslq	%ecx, %rdx
	movb	$1, -88(%rbp)
	leaq	15(%rax,%rsi), %rax
	addq	%rax, %rdx
	movq	%rdx, -72(%rbp)
	jmp	.L616
.L611:
	movq	15(%rax), %rdi
	movl	%esi, -388(%rbp)
	movl	%ecx, -392(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-388(%rbp), %rsi
	movslq	-392(%rbp), %rdx
	movb	$1, -88(%rbp)
	addq	%rsi, %rax
	addq	%rax, %rdx
	movq	%rdx, -72(%rbp)
	jmp	.L616
.L614:
	movq	15(%rax), %rdi
	movl	%ecx, -392(%rbp)
	movl	%esi, -388(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-388(%rbp), %rsi
	movslq	-392(%rbp), %rcx
	movb	$0, -88(%rbp)
	leaq	(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rdx
	movq	%rdx, -72(%rbp)
	leaq	2(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movzwl	(%rax), %eax
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L559:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L573:
	movq	15(%rax), %rax
	jmp	.L623
.L575:
	addl	27(%rax), %ecx
	movq	15(%rax), %rax
	jmp	.L623
.L578:
	movslq	%ecx, %rcx
	subl	%edi, %r14d
	movb	$1, -88(%rbp)
	leaq	15(%rax,%rcx), %rax
	movslq	%r14d, %rdx
	movq	%rax, -80(%rbp)
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	jmp	.L570
.L579:
	subl	%edi, %r14d
	movq	15(%rax), %rdi
	movl	%ecx, -388(%rbp)
	movslq	%r14d, %r14
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-388(%rbp), %rcx
	movb	$0, -88(%rbp)
	leaq	(%rax,%rcx,2), %rax
	movq	%rax, -80(%rbp)
	leaq	(%rax,%r14,2), %rax
	movq	%rax, -72(%rbp)
	jmp	.L570
.L580:
	movslq	%ecx, %rcx
	subl	%edi, %r14d
	movb	$0, -88(%rbp)
	leaq	15(%rax,%rcx,2), %rdx
	movslq	%r14d, %rax
	leaq	(%rdx,%rax,2), %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
	jmp	.L570
.L641:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19667:
	.size	_ZN2v88internal6String9PrintUC16ERSoii, .-_ZN2v88internal6String9PrintUC16ERSoii
	.section	.text._ZN2v88internal6String17ComputeArrayIndexEPj.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal6String17ComputeArrayIndexEPj.part.0, @function
_ZN2v88internal6String17ComputeArrayIndexEPj.part.0:
.LFB25190:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$32, %ecx
	pxor	%xmm0, %xmm0
	leaq	.L647(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-368(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$376, %rsp
	movq	(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -112(%rbp)
	movb	$0, -88(%rbp)
	movl	$0, -388(%rbp)
	movaps	%xmm0, -80(%rbp)
	rep stosq
	movslq	11(%rsi), %r13
.L644:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L645
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6String17ComputeArrayIndexEPj.part.0,"a",@progbits
	.align 4
	.align 4
.L647:
	.long	.L653-.L647
	.long	.L650-.L647
	.long	.L652-.L647
	.long	.L648-.L647
	.long	.L645-.L647
	.long	.L646-.L647
	.long	.L645-.L647
	.long	.L645-.L647
	.long	.L651-.L647
	.long	.L650-.L647
	.long	.L649-.L647
	.long	.L648-.L647
	.long	.L645-.L647
	.long	.L646-.L647
	.section	.text._ZN2v88internal6String17ComputeArrayIndexEPj.part.0
	.p2align 4,,10
	.p2align 3
.L650:
	movl	$0, -104(%rbp)
	movl	-388(%rbp), %eax
	testq	%rsi, %rsi
	je	.L663
	movq	%rsi, -112(%rbp)
	movq	%r12, %rdi
	leaq	-388(%rbp), %rsi
	movl	%eax, -96(%rbp)
	movabsq	$141733920769, %rax
	movq	%rax, -104(%rbp)
	movl	$0, -388(%rbp)
	call	_ZN2v88internal18ConsStringIterator6SearchEPi
	testq	%rax, %rax
	je	.L765
	movl	-388(%rbp), %edi
	movl	11(%rax), %ecx
	leaq	.L660(%rip), %rdx
	movl	%edi, %r14d
.L745:
	movq	-1(%rax), %rsi
	movzwl	11(%rsi), %esi
	andl	$15, %esi
	cmpw	$13, %si
	ja	.L645
	movzwl	%si, %esi
	movslq	(%rdx,%rsi,4), %rsi
	addq	%rdx, %rsi
	notrack jmp	*%rsi
	.section	.rodata._ZN2v88internal6String17ComputeArrayIndexEPj.part.0
	.align 4
	.align 4
.L660:
	.long	.L666-.L660
	.long	.L663-.L660
	.long	.L665-.L660
	.long	.L661-.L660
	.long	.L645-.L660
	.long	.L659-.L660
	.long	.L645-.L660
	.long	.L645-.L660
	.long	.L664-.L660
	.long	.L663-.L660
	.long	.L662-.L660
	.long	.L661-.L660
	.long	.L645-.L660
	.long	.L659-.L660
	.section	.text._ZN2v88internal6String17ComputeArrayIndexEPj.part.0
	.p2align 4,,10
	.p2align 3
.L646:
	movq	15(%rsi), %rsi
	jmp	.L644
	.p2align 4,,10
	.p2align 3
.L648:
	addl	27(%rsi), %r14d
	movq	15(%rsi), %rsi
	jmp	.L644
	.p2align 4,,10
	.p2align 3
.L652:
	movq	15(%rsi), %rdi
	movslq	%r14d, %r14
	movq	(%rdi), %rax
	call	*48(%rax)
	movb	$0, -88(%rbp)
	leaq	(%rax,%r14,2), %rdx
	leaq	(%rdx,%r13,2), %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
.L654:
	movl	$0, -104(%rbp)
.L657:
	cmpq	%rdx, %rax
	je	.L669
.L761:
	movzbl	-88(%rbp), %eax
.L670:
	testb	%al, %al
	je	.L686
.L685:
	leaq	1(%rdx), %rax
	movl	$1, %ecx
	movq	%rax, -80(%rbp)
	movzbl	(%rdx), %edx
	cmpw	$48, %dx
	je	.L766
.L689:
	subl	$48, %edx
	cmpl	$9, %edx
	ja	.L704
	movl	%edx, %r15d
	leaq	.L712(%rip), %r13
	leaq	.L729(%rip), %r14
	cmpq	%rax, -72(%rbp)
	je	.L767
	.p2align 4,,10
	.p2align 3
.L705:
	testb	%cl, %cl
	je	.L737
.L736:
	leaq	1(%rax), %rcx
	movq	%rcx, -80(%rbp)
	movzbl	(%rax), %eax
	movl	$1, %ecx
	leal	-48(%rax), %edi
	cmpl	$9, %edi
	ja	.L704
.L770:
	subl	$45, %eax
	movl	$429496729, %esi
	sarl	$3, %eax
	subl	%eax, %esi
	cmpl	%r15d, %esi
	jb	.L704
	leal	(%r15,%r15,4), %eax
	leal	(%rdi,%rax,2), %r15d
	movq	-80(%rbp), %rax
	cmpq	%rax, -72(%rbp)
	jne	.L705
.L767:
	movl	$0, -376(%rbp)
	movl	-104(%rbp), %eax
	testl	%eax, %eax
	jne	.L768
.L706:
	movl	%r15d, (%rbx)
.L764:
	movl	$1, %r8d
.L643:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L769
	addq	$376, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L651:
	.cfi_restore_state
	movslq	%r14d, %r14
	leaq	15(%rsi,%r14), %rdx
.L763:
	movslq	%r13d, %rax
	movb	$1, -88(%rbp)
	addq	%rdx, %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L649:
	movq	15(%rsi), %rdi
	movslq	%r14d, %r14
	movq	(%rdi), %rax
	call	*48(%rax)
	leaq	(%rax,%r14), %rdx
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L653:
	movslq	%r14d, %r14
	movb	$0, -88(%rbp)
	leaq	15(%rsi,%r14,2), %rdx
	leaq	(%rdx,%r13,2), %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L735:
	movslq	%r8d, %r8
	movb	$0, -88(%rbp)
	leaq	15(%rax,%r8,2), %rax
	leaq	(%rax,%rsi,2), %rcx
	movq	%rcx, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L737:
	leaq	2(%rax), %rcx
	movq	%rcx, -80(%rbp)
	movzwl	(%rax), %eax
	xorl	%ecx, %ecx
	leal	-48(%rax), %edi
	cmpl	$9, %edi
	jbe	.L770
.L704:
	xorl	%r8d, %r8d
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L768:
	movl	-100(%rbp), %edi
	subl	%eax, %edi
	cmpl	$32, %edi
	sete	-372(%rbp)
	jne	.L707
.L709:
	leaq	-376(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal18ConsStringIterator6SearchEPi
.L708:
	testq	%rax, %rax
	je	.L706
	movl	11(%rax), %esi
	xorl	%r8d, %r8d
.L743:
	movq	-1(%rax), %rcx
	movzwl	11(%rcx), %ecx
	andl	$15, %ecx
	cmpw	$13, %cx
	ja	.L645
	movzwl	%cx, %ecx
	movslq	0(%r13,%rcx,4), %rcx
	addq	%r13, %rcx
	notrack jmp	*%rcx
	.section	.rodata._ZN2v88internal6String17ComputeArrayIndexEPj.part.0
	.align 4
	.align 4
.L712:
	.long	.L718-.L712
	.long	.L715-.L712
	.long	.L717-.L712
	.long	.L713-.L712
	.long	.L645-.L712
	.long	.L711-.L712
	.long	.L645-.L712
	.long	.L645-.L712
	.long	.L716-.L712
	.long	.L715-.L712
	.long	.L714-.L712
	.long	.L713-.L712
	.long	.L645-.L712
	.long	.L711-.L712
	.section	.text._ZN2v88internal6String17ComputeArrayIndexEPj.part.0
	.p2align 4,,10
	.p2align 3
.L711:
	movq	15(%rax), %rax
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L713:
	addl	27(%rax), %r8d
	movq	15(%rax), %rax
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L715:
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rcx
.L719:
	cmpq	%rax, %rcx
	je	.L721
.L762:
	movzbl	-88(%rbp), %ecx
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L717:
	movq	15(%rax), %rdi
	movl	%r8d, -408(%rbp)
	movl	%esi, -404(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-408(%rbp), %r8
	movslq	-404(%rbp), %rcx
	movb	$0, -88(%rbp)
	leaq	(%rax,%r8,2), %rax
	leaq	(%rax,%rcx,2), %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	cmpq	%rax, %rcx
	jne	.L762
	.p2align 4,,10
	.p2align 3
.L721:
	movl	$0, -372(%rbp)
	movl	-104(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L762
	movl	-100(%rbp), %eax
	subl	%ecx, %eax
	cmpl	$32, %eax
	sete	-376(%rbp)
	jne	.L724
.L726:
	leaq	-372(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal18ConsStringIterator6SearchEPi
.L725:
	testq	%rax, %rax
	je	.L771
	movslq	11(%rax), %rsi
	xorl	%r8d, %r8d
.L742:
	movq	-1(%rax), %rcx
	movzwl	11(%rcx), %ecx
	andl	$15, %ecx
	cmpw	$13, %cx
	ja	.L645
	movzwl	%cx, %ecx
	movslq	(%r14,%rcx,4), %rcx
	addq	%r14, %rcx
	notrack jmp	*%rcx
	.section	.rodata._ZN2v88internal6String17ComputeArrayIndexEPj.part.0
	.align 4
	.align 4
.L729:
	.long	.L735-.L729
	.long	.L732-.L729
	.long	.L734-.L729
	.long	.L730-.L729
	.long	.L645-.L729
	.long	.L728-.L729
	.long	.L645-.L729
	.long	.L645-.L729
	.long	.L733-.L729
	.long	.L732-.L729
	.long	.L731-.L729
	.long	.L730-.L729
	.long	.L645-.L729
	.long	.L728-.L729
	.section	.text._ZN2v88internal6String17ComputeArrayIndexEPj.part.0
	.p2align 4,,10
	.p2align 3
.L718:
	movslq	%r8d, %r8
	movslq	%esi, %rcx
	movb	$0, -88(%rbp)
	leaq	15(%rax,%r8,2), %rax
	leaq	(%rax,%rcx,2), %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L714:
	movq	15(%rax), %rdi
	movl	%r8d, -408(%rbp)
	movl	%esi, -404(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-408(%rbp), %r8
	movslq	-404(%rbp), %rcx
	movb	$1, -88(%rbp)
	addq	%r8, %rax
	addq	%rax, %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L716:
	movslq	%r8d, %r8
	movslq	%esi, %rcx
	movb	$1, -88(%rbp)
	leaq	15(%rax,%r8), %rax
	addq	%rax, %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L728:
	movq	15(%rax), %rax
	jmp	.L742
	.p2align 4,,10
	.p2align 3
.L730:
	addl	27(%rax), %r8d
	movq	15(%rax), %rax
	jmp	.L742
	.p2align 4,,10
	.p2align 3
.L732:
	movzbl	-88(%rbp), %ecx
	movq	-80(%rbp), %rax
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L734:
	movq	15(%rax), %rdi
	movl	%esi, -408(%rbp)
	movl	%r8d, -404(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-404(%rbp), %r8
	movslq	-408(%rbp), %rsi
	movb	$0, -88(%rbp)
	leaq	(%rax,%r8,2), %rax
	leaq	(%rax,%rsi,2), %rcx
	movq	%rcx, -72(%rbp)
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L731:
	movq	15(%rax), %rdi
	movl	%esi, -408(%rbp)
	movl	%r8d, -404(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-404(%rbp), %r8
	movslq	-408(%rbp), %rsi
	movb	$1, -88(%rbp)
	addq	%r8, %rax
	addq	%rax, %rsi
	movq	%rsi, -72(%rbp)
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L733:
	movslq	%r8d, %r8
	movb	$1, -88(%rbp)
	leaq	15(%rax,%r8), %rax
	addq	%rax, %rsi
	movq	%rsi, -72(%rbp)
	jmp	.L736
.L684:
	movslq	%r14d, %r14
	movb	$0, -88(%rbp)
	leaq	15(%rax,%r14,2), %rdx
	leaq	(%rdx,%r13,2), %rax
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L686:
	leaq	2(%rdx), %rax
	xorl	%ecx, %ecx
	movq	%rax, -80(%rbp)
	movzwl	(%rdx), %edx
	cmpw	$48, %dx
	jne	.L689
.L766:
	movl	$0, (%rbx)
	xorl	%r8d, %r8d
	cmpq	%rax, -72(%rbp)
	jne	.L643
	movl	-104(%rbp), %eax
	movl	$1, %r8d
	movl	$0, -380(%rbp)
	testl	%eax, %eax
	je	.L643
	movl	-100(%rbp), %ebx
	subl	%eax, %ebx
	cmpl	$32, %ebx
	sete	-372(%rbp)
	jne	.L692
.L694:
	leaq	-380(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal18ConsStringIterator6SearchEPi
.L693:
	leaq	.L697(%rip), %rcx
	testq	%rax, %rax
	je	.L764
.L741:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L645
	movzwl	%dx, %edx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal6String17ComputeArrayIndexEPj.part.0
	.align 4
	.align 4
.L697:
	.long	.L704-.L697
	.long	.L704-.L697
	.long	.L701-.L697
	.long	.L696-.L697
	.long	.L645-.L697
	.long	.L696-.L697
	.long	.L645-.L697
	.long	.L645-.L697
	.long	.L704-.L697
	.long	.L704-.L697
	.long	.L701-.L697
	.long	.L696-.L697
	.long	.L645-.L697
	.long	.L696-.L697
	.section	.text._ZN2v88internal6String17ComputeArrayIndexEPj.part.0
	.p2align 4,,10
	.p2align 3
.L707:
	leaq	-372(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal18ConsStringIterator8NextLeafEPb
	cmpb	$0, -372(%rbp)
	je	.L708
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L669:
	movl	$0, -384(%rbp)
	movl	-104(%rbp), %eax
	testl	%eax, %eax
	je	.L761
	movl	-100(%rbp), %edx
	subl	%eax, %edx
	cmpl	$32, %edx
	sete	-372(%rbp)
	jne	.L673
.L675:
	leaq	-384(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal18ConsStringIterator6SearchEPi
.L674:
	testq	%rax, %rax
	je	.L772
	movslq	11(%rax), %r13
	xorl	%r14d, %r14d
	leaq	.L678(%rip), %rdx
.L744:
	movq	-1(%rax), %rcx
	movzwl	11(%rcx), %ecx
	andl	$15, %ecx
	cmpw	$13, %cx
	ja	.L645
	movzwl	%cx, %ecx
	movslq	(%rdx,%rcx,4), %rcx
	addq	%rdx, %rcx
	notrack jmp	*%rcx
	.section	.rodata._ZN2v88internal6String17ComputeArrayIndexEPj.part.0
	.align 4
	.align 4
.L678:
	.long	.L684-.L678
	.long	.L681-.L678
	.long	.L683-.L678
	.long	.L679-.L678
	.long	.L645-.L678
	.long	.L677-.L678
	.long	.L645-.L678
	.long	.L645-.L678
	.long	.L682-.L678
	.long	.L681-.L678
	.long	.L680-.L678
	.long	.L679-.L678
	.long	.L645-.L678
	.long	.L677-.L678
	.section	.text._ZN2v88internal6String17ComputeArrayIndexEPj.part.0
	.p2align 4,,10
	.p2align 3
.L771:
	movl	$0, -104(%rbp)
	movq	-80(%rbp), %rax
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L724:
	leaq	-376(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal18ConsStringIterator8NextLeafEPb
	cmpb	$0, -376(%rbp)
	je	.L725
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L765:
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rax
	movl	$0, -104(%rbp)
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L645:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L663:
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rax
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L659:
	movq	15(%rax), %rax
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L661:
	addl	27(%rax), %r14d
	movq	15(%rax), %rax
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L696:
	movq	15(%rax), %rax
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L673:
	leaq	-372(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal18ConsStringIterator8NextLeafEPb
	cmpb	$0, -372(%rbp)
	je	.L674
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L677:
	movq	15(%rax), %rax
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L679:
	addl	27(%rax), %r14d
	movq	15(%rax), %rax
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L681:
	movzbl	-88(%rbp), %eax
	movq	-80(%rbp), %rdx
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L692:
	leaq	-372(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal18ConsStringIterator8NextLeafEPb
	cmpb	$0, -372(%rbp)
	je	.L693
	jmp	.L694
	.p2align 4,,10
	.p2align 3
.L662:
	subl	%edi, %ecx
	movq	15(%rax), %rdi
	movslq	%r14d, %r14
	movl	%ecx, %r13d
	movq	(%rdi), %rax
	call	*48(%rax)
	movb	$1, -88(%rbp)
	leaq	(%rax,%r14), %rdx
	movslq	%r13d, %rax
	addq	%rdx, %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L664:
	movslq	%r14d, %r14
	subl	%edi, %ecx
	movb	$1, -88(%rbp)
	leaq	15(%rax,%r14), %rdx
	movslq	%ecx, %rax
	addq	%rdx, %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L665:
	subl	%edi, %ecx
	movq	15(%rax), %rdi
	movslq	%r14d, %r14
	movl	%ecx, %r13d
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	%r13d, %rcx
	movb	$0, -88(%rbp)
	leaq	(%rax,%r14,2), %rdx
	leaq	(%rdx,%rcx,2), %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L666:
	movslq	%r14d, %r14
	subl	%edi, %ecx
	movb	$0, -88(%rbp)
	leaq	15(%rax,%r14,2), %rdx
	movslq	%ecx, %rcx
	leaq	(%rdx,%rcx,2), %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L701:
	movq	15(%rax), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	jmp	.L704
.L680:
	movq	15(%rax), %rdi
	movslq	%r14d, %r14
	movq	(%rdi), %rax
	call	*48(%rax)
	movb	$1, -88(%rbp)
	leaq	(%rax,%r14), %rdx
	addq	%rdx, %r13
	movq	%r13, -72(%rbp)
	jmp	.L685
.L682:
	movslq	%r14d, %r14
	movb	$1, -88(%rbp)
	leaq	15(%rax,%r14), %rdx
	addq	%rdx, %r13
	movq	%r13, -72(%rbp)
	jmp	.L685
.L683:
	movq	15(%rax), %rdi
	movslq	%r14d, %r14
	movq	(%rdi), %rax
	call	*48(%rax)
	movb	$0, -88(%rbp)
	leaq	(%rax,%r14,2), %rdx
	leaq	(%rdx,%r13,2), %rax
	movq	%rax, -72(%rbp)
	jmp	.L686
.L772:
	movl	$0, -104(%rbp)
	movq	-80(%rbp), %rdx
	jmp	.L761
.L769:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25190:
	.size	_ZN2v88internal6String17ComputeArrayIndexEPj.part.0, .-_ZN2v88internal6String17ComputeArrayIndexEPj.part.0
	.section	.text._ZN2v88internal6String17ComputeArrayIndexEPj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String17ComputeArrayIndexEPj
	.type	_ZN2v88internal6String17ComputeArrayIndexEPj, @function
_ZN2v88internal6String17ComputeArrayIndexEPj:
.LFB19713:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	je	.L774
	cmpl	$10, %eax
	jg	.L774
	jmp	_ZN2v88internal6String17ComputeArrayIndexEPj.part.0
	.p2align 4,,10
	.p2align 3
.L774:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE19713:
	.size	_ZN2v88internal6String17ComputeArrayIndexEPj, .-_ZN2v88internal6String17ComputeArrayIndexEPj
	.section	.rodata._ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEiiPi.str1.1,"aMS",@progbits,1
.LC1:
	.string	"NewArray"
	.section	.text._ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEiiPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEiiPi
	.type	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEiiPi, @function
_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEiiPi:
.LFB19676:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%r8d, %r12d
	pushq	%rbx
	subq	$424, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	(%rsi), %r13
	movq	%rdi, -424(%rbp)
	movq	%rsi, -440(%rbp)
	movl	%edx, -444(%rbp)
	movl	%r9d, -416(%rbp)
	movq	%rax, -432(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	je	.L962
.L776:
	movl	$2147483647, %eax
	pxor	%xmm0, %xmm0
	movb	$0, -88(%rbp)
	movl	%r12d, %r15d
	movl	-416(%rbp), %ebx
	subl	%r12d, %eax
	leaq	-368(%rbp), %r14
	movl	$32, %ecx
	movq	%r14, %rdi
	movaps	%xmm0, -80(%rbp)
	leaq	.L784(%rip), %rsi
	testl	%ebx, %ebx
	movq	$0, -112(%rbp)
	cmovns	%ebx, %eax
	movl	%r12d, -392(%rbp)
	movl	%eax, -416(%rbp)
	xorl	%eax, %eax
	rep stosq
	movl	11(%r13), %ebx
.L781:
	movq	-1(%r13), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L782
	movzwl	%ax, %eax
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEiiPi,"a",@progbits
	.align 4
	.align 4
.L784:
	.long	.L790-.L784
	.long	.L787-.L784
	.long	.L789-.L784
	.long	.L785-.L784
	.long	.L782-.L784
	.long	.L783-.L784
	.long	.L782-.L784
	.long	.L782-.L784
	.long	.L788-.L784
	.long	.L787-.L784
	.long	.L786-.L784
	.long	.L785-.L784
	.long	.L782-.L784
	.long	.L783-.L784
	.section	.text._ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEiiPi
	.p2align 4,,10
	.p2align 3
.L787:
	movl	$0, -104(%rbp)
	movl	-392(%rbp), %eax
	testq	%r13, %r13
	je	.L800
	movl	%eax, -96(%rbp)
	leaq	-392(%rbp), %rsi
	movq	%r14, %rdi
	movabsq	$141733920769, %rax
	movq	%r13, -112(%rbp)
	movq	%rax, -104(%rbp)
	movl	$0, -392(%rbp)
	call	_ZN2v88internal18ConsStringIterator6SearchEPi
	testq	%rax, %rax
	je	.L963
	movl	-392(%rbp), %esi
	movl	11(%rax), %ebx
	leaq	.L797(%rip), %rdi
	movl	%esi, %r13d
.L921:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L782
	movzwl	%dx, %edx
	movslq	(%rdi,%rdx,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEiiPi
	.align 4
	.align 4
.L797:
	.long	.L803-.L797
	.long	.L800-.L797
	.long	.L802-.L797
	.long	.L798-.L797
	.long	.L782-.L797
	.long	.L796-.L797
	.long	.L782-.L797
	.long	.L782-.L797
	.long	.L801-.L797
	.long	.L800-.L797
	.long	.L799-.L797
	.long	.L798-.L797
	.long	.L782-.L797
	.long	.L796-.L797
	.section	.text._ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEiiPi
	.p2align 4,,10
	.p2align 3
.L962:
	movq	%r13, %rdi
	call	_ZN2v88internal12ReadOnlyHeap8ContainsENS0_10HeapObjectE@PLT
	testb	%al, %al
	jne	.L959
	movq	%r13, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L778
	movq	-440(%rbp), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal4Heap8ContainsENS0_10HeapObjectE@PLT
	testb	%al, %al
	jne	.L959
.L778:
	movq	-424(%rbp), %rax
	movq	$0, (%rax)
	.p2align 4,,10
	.p2align 3
.L775:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L964
	movq	-424(%rbp), %rax
	addq	$424, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L785:
	.cfi_restore_state
	addl	27(%r13), %r15d
	movq	15(%r13), %r13
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L783:
	movq	15(%r13), %r13
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L963:
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rsi
	movl	$0, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L794:
	movl	-416(%rbp), %ebx
	leal	1(%r12), %r13d
	movl	$-1, %r8d
	xorl	%r15d, %r15d
	movl	%r13d, %edx
	movl	%r13d, -408(%rbp)
	movq	%r14, %r13
	addl	%r12d, %ebx
	movl	%r12d, -412(%rbp)
	movl	%r8d, %r12d
	movl	%ebx, %r14d
	movl	%edx, %ebx
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L806:
	leal	-1(%rbx), %edx
	cmpl	%r14d, %edx
	jge	.L957
.L960:
	movzbl	-88(%rbp), %edx
.L826:
	testb	%dl, %dl
	je	.L842
.L841:
	leaq	1(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movzbl	(%rax), %esi
	movq	%rdx, %rax
.L844:
	movzwl	%si, %edx
	movl	$1, %ecx
	cmpw	$127, %si
	jbe	.L845
	movl	$2, %ecx
	cmpl	$2047, %edx
	jbe	.L845
	movl	%r12d, %r8d
	movl	$3, %ecx
	andl	$64512, %r8d
	cmpl	$55296, %r8d
	jne	.L845
	andw	$-1024, %si
	xorl	%ecx, %ecx
	cmpw	$-9216, %si
	setne	%cl
	leal	1(%rcx,%rcx), %ecx
	.p2align 4,,10
	.p2align 3
.L845:
	movq	-72(%rbp), %rsi
	addl	%ecx, %r15d
	movl	%edx, %r12d
	addl	$1, %ebx
.L846:
	cmpq	%rax, %rsi
	jne	.L806
	movl	$0, -388(%rbp)
	movl	-104(%rbp), %eax
	testl	%eax, %eax
	jne	.L965
.L957:
	movq	%r13, %r14
	movl	-412(%rbp), %r12d
	movl	-408(%rbp), %r13d
.L822:
	cmpq	$0, -432(%rbp)
	je	.L824
	movq	-432(%rbp), %rax
	movl	%r15d, (%rax)
.L824:
	leal	1(%r15), %ecx
	leaq	_ZSt7nothrow(%rip), %rsi
	movslq	%ecx, %rbx
	movq	%rbx, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L966
.L847:
	movq	-440(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	%r12d, %ecx
	movl	%r12d, -380(%rbp)
	movaps	%xmm0, -80(%rbp)
	leaq	.L850(%rip), %rsi
	movq	(%rax), %rdx
	movl	11(%rdx), %ebx
.L848:
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L782
	movzwl	%ax, %eax
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEiiPi
	.align 4
	.align 4
.L850:
	.long	.L856-.L850
	.long	.L853-.L850
	.long	.L855-.L850
	.long	.L851-.L850
	.long	.L782-.L850
	.long	.L849-.L850
	.long	.L782-.L850
	.long	.L782-.L850
	.long	.L854-.L850
	.long	.L853-.L850
	.long	.L852-.L850
	.long	.L851-.L850
	.long	.L782-.L850
	.long	.L849-.L850
	.section	.text._ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEiiPi
	.p2align 4,,10
	.p2align 3
.L853:
	movl	$0, -104(%rbp)
	movl	-380(%rbp), %eax
	testq	%rdx, %rdx
	je	.L866
	movl	%eax, -96(%rbp)
	leaq	-380(%rbp), %rsi
	movq	%r14, %rdi
	movabsq	$141733920769, %rax
	movq	%rdx, -112(%rbp)
	movq	%rax, -104(%rbp)
	movl	$0, -380(%rbp)
	call	_ZN2v88internal18ConsStringIterator6SearchEPi
	testq	%rax, %rax
	je	.L967
	movl	-380(%rbp), %esi
	movl	11(%rax), %ebx
	leaq	.L863(%rip), %rdi
	movl	%esi, %ecx
.L919:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L782
	movzwl	%dx, %edx
	movslq	(%rdi,%rdx,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEiiPi
	.align 4
	.align 4
.L863:
	.long	.L869-.L863
	.long	.L866-.L863
	.long	.L868-.L863
	.long	.L864-.L863
	.long	.L782-.L863
	.long	.L862-.L863
	.long	.L782-.L863
	.long	.L782-.L863
	.long	.L867-.L863
	.long	.L866-.L863
	.long	.L865-.L863
	.long	.L864-.L863
	.long	.L782-.L863
	.long	.L862-.L863
	.section	.text._ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEiiPi
	.p2align 4,,10
	.p2align 3
.L849:
	movq	15(%rdx), %rdx
	jmp	.L848
	.p2align 4,,10
	.p2align 3
.L851:
	addl	27(%rdx), %ecx
	movq	15(%rdx), %rdx
	jmp	.L848
.L788:
	movslq	%r15d, %rcx
	subl	%r12d, %ebx
	movb	$1, -88(%rbp)
	leaq	15(%r13,%rcx), %rax
	movslq	%ebx, %rsi
	addq	%rax, %rsi
	movq	%rax, -80(%rbp)
	movq	%rsi, -72(%rbp)
.L791:
	movl	$0, -104(%rbp)
	jmp	.L794
.L786:
	movq	15(%r13), %rdi
	subl	%r12d, %ebx
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	%r15d, %rcx
	movslq	%ebx, %rsi
	movb	$1, -88(%rbp)
	addq	%rcx, %rax
	addq	%rax, %rsi
	movq	%rax, -80(%rbp)
	movq	%rsi, -72(%rbp)
	jmp	.L791
.L790:
	movslq	%r15d, %rcx
	subl	%r12d, %ebx
	movb	$0, -88(%rbp)
	leaq	15(%r13,%rcx,2), %rax
	movslq	%ebx, %rdx
	leaq	(%rax,%rdx,2), %rsi
	movq	%rax, -80(%rbp)
	movq	%rsi, -72(%rbp)
	jmp	.L791
.L789:
	movq	15(%r13), %rdi
	subl	%r12d, %ebx
	movslq	%ebx, %rbx
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	%r15d, %rcx
	movb	$0, -88(%rbp)
	leaq	(%rax,%rcx,2), %rax
	leaq	(%rax,%rbx,2), %rsi
	movq	%rax, -80(%rbp)
	movq	%rsi, -72(%rbp)
	jmp	.L791
.L855:
	movq	15(%rdx), %rdi
	movl	%ecx, -408(%rbp)
	subl	%r12d, %ebx
	movslq	%ebx, %rbx
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-408(%rbp), %rcx
	movb	$0, -88(%rbp)
	leaq	(%rax,%rcx,2), %rdx
	leaq	(%rdx,%rbx,2), %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
.L857:
	movl	$0, -104(%rbp)
.L860:
	movl	$-1, %r11d
	movq	%r14, -408(%rbp)
	movl	-444(%rbp), %r8d
	xorl	%ebx, %ebx
	addl	-416(%rbp), %r12d
	movl	%r11d, %r14d
	jmp	.L915
	.p2align 4,,10
	.p2align 3
.L872:
	leal	-1(%r13), %eax
	cmpl	%r12d, %eax
	jge	.L888
.L961:
	movzbl	-88(%rbp), %eax
.L890:
	testb	%al, %al
	je	.L906
.L905:
	leaq	1(%rdx), %rcx
	movq	%rcx, -80(%rbp)
	movzbl	(%rdx), %eax
	movq	%rcx, %rdx
.L908:
	movslq	%ebx, %rcx
	addq	%r15, %rcx
	cmpl	$1, %r8d
	jne	.L930
	testw	%ax, %ax
	jne	.L930
	movl	$32, %r14d
	movl	$32, %eax
.L909:
	movb	%al, (%rcx)
	movl	$1, %eax
.L912:
	addl	%eax, %ebx
	movq	-72(%rbp), %rax
	addl	$1, %r13d
.L915:
	cmpq	%rax, %rdx
	jne	.L872
	movl	$0, -376(%rbp)
	movl	-104(%rbp), %eax
	testl	%eax, %eax
	jne	.L968
.L888:
	movq	-424(%rbp), %rax
	movslq	%ebx, %rcx
	movb	$0, (%r15,%rcx)
	movq	%r15, (%rax)
	jmp	.L775
.L854:
	movslq	%ecx, %rcx
	subl	%r12d, %ebx
	movb	$1, -88(%rbp)
	leaq	15(%rdx,%rcx), %rdx
	movslq	%ebx, %rax
	addq	%rdx, %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
	jmp	.L857
.L856:
	movslq	%ecx, %rcx
	subl	%r12d, %ebx
	movb	$0, -88(%rbp)
	leaq	15(%rdx,%rcx,2), %rdx
	movslq	%ebx, %rax
	leaq	(%rdx,%rax,2), %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
	jmp	.L857
.L852:
	movq	15(%rdx), %rdi
	movl	%ecx, -408(%rbp)
	subl	%r12d, %ebx
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-408(%rbp), %rcx
	movb	$1, -88(%rbp)
	leaq	(%rax,%rcx), %rdx
	movslq	%ebx, %rax
	addq	%rdx, %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L930:
	movzwl	%ax, %esi
	cmpw	$127, %ax
	jbe	.L969
	cmpl	$2047, %esi
	jbe	.L970
	movl	%r14d, %edi
	andl	$64512, %edi
	cmpl	$55296, %edi
	je	.L971
.L914:
	movl	%esi, %edi
	andl	$63, %eax
	movl	%esi, %r14d
	shrl	$12, %edi
	orl	$-128, %eax
	orl	$-32, %edi
	movb	%al, 2(%rcx)
	movl	$3, %eax
	movb	%dil, (%rcx)
	movl	%esi, %edi
	shrl	$6, %edi
	andl	$63, %edi
	orl	$-128, %edi
	movb	%dil, 1(%rcx)
	jmp	.L912
.L840:
	movslq	%esi, %rsi
	movb	$0, -88(%rbp)
	leaq	15(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rdx
	movq	%rdx, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L842:
	leaq	2(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movzwl	(%rax), %esi
	movq	%rdx, %rax
	jmp	.L844
.L904:
	movslq	%esi, %rsi
	movb	$0, -88(%rbp)
	leaq	15(%rax,%rsi,2), %rdx
	leaq	(%rdx,%rcx,2), %rax
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L906:
	leaq	2(%rdx), %rcx
	movq	%rcx, -80(%rbp)
	movzwl	(%rdx), %eax
	movq	%rcx, %rdx
	jmp	.L908
	.p2align 4,,10
	.p2align 3
.L965:
	movl	-100(%rbp), %edi
	subl	%eax, %edi
	cmpl	$32, %edi
	sete	-372(%rbp)
	jne	.L808
.L810:
	leaq	-388(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18ConsStringIterator6SearchEPi
.L809:
	testq	%rax, %rax
	je	.L972
	movslq	11(%rax), %rsi
	xorl	%ecx, %ecx
.L920:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L782
	leaq	.L813(%rip), %rdi
	movzwl	%dx, %edx
	movslq	(%rdi,%rdx,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEiiPi
	.align 4
	.align 4
.L813:
	.long	.L819-.L813
	.long	.L816-.L813
	.long	.L818-.L813
	.long	.L814-.L813
	.long	.L782-.L813
	.long	.L812-.L813
	.long	.L782-.L813
	.long	.L782-.L813
	.long	.L817-.L813
	.long	.L816-.L813
	.long	.L815-.L813
	.long	.L814-.L813
	.long	.L782-.L813
	.long	.L812-.L813
	.section	.text._ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEiiPi
	.p2align 4,,10
	.p2align 3
.L817:
	movslq	%ecx, %rcx
	movb	$1, -88(%rbp)
	leaq	15(%rax,%rcx), %rax
	movq	%rax, -80(%rbp)
	addq	%rsi, %rax
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L816:
	leal	-1(%rbx), %eax
	cmpl	%eax, %r14d
	jle	.L957
	movq	-80(%rbp), %rax
	cmpq	-72(%rbp), %rax
	jne	.L960
	movl	$0, -384(%rbp)
	movl	-104(%rbp), %edx
	testl	%edx, %edx
	je	.L960
	movl	-100(%rbp), %eax
	subl	%edx, %eax
	cmpl	$32, %eax
	sete	-372(%rbp)
	jne	.L829
.L831:
	leaq	-384(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18ConsStringIterator6SearchEPi
.L830:
	testq	%rax, %rax
	je	.L973
	movslq	11(%rax), %rcx
	xorl	%esi, %esi
.L916:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L782
	leaq	.L834(%rip), %rdi
	movzwl	%dx, %edx
	movslq	(%rdi,%rdx,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEiiPi
	.align 4
	.align 4
.L834:
	.long	.L840-.L834
	.long	.L837-.L834
	.long	.L839-.L834
	.long	.L835-.L834
	.long	.L782-.L834
	.long	.L833-.L834
	.long	.L782-.L834
	.long	.L782-.L834
	.long	.L838-.L834
	.long	.L837-.L834
	.long	.L836-.L834
	.long	.L835-.L834
	.long	.L782-.L834
	.long	.L833-.L834
	.section	.text._ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEiiPi
	.p2align 4,,10
	.p2align 3
.L812:
	movq	15(%rax), %rax
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L814:
	addl	27(%rax), %ecx
	movq	15(%rax), %rax
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L815:
	movq	15(%rax), %rdi
	movl	%ecx, -452(%rbp)
	movl	%esi, -448(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-452(%rbp), %rcx
	movslq	-448(%rbp), %rsi
	movb	$1, -88(%rbp)
	addq	%rcx, %rax
	movq	%rax, -80(%rbp)
	addq	%rsi, %rax
	movq	%rax, -72(%rbp)
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L819:
	movslq	%ecx, %rcx
	movb	$0, -88(%rbp)
	leaq	15(%rax,%rcx,2), %rax
	movq	%rax, -80(%rbp)
	leaq	(%rax,%rsi,2), %rax
	movq	%rax, -72(%rbp)
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L818:
	movq	15(%rax), %rdi
	movl	%ecx, -452(%rbp)
	movl	%esi, -448(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-452(%rbp), %rcx
	movslq	-448(%rbp), %rsi
	movb	$0, -88(%rbp)
	leaq	(%rax,%rcx,2), %rax
	movq	%rax, -80(%rbp)
	leaq	(%rax,%rsi,2), %rax
	movq	%rax, -72(%rbp)
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L833:
	movq	15(%rax), %rax
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L835:
	addl	27(%rax), %esi
	movq	15(%rax), %rax
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L837:
	movzbl	-88(%rbp), %edx
	movq	-80(%rbp), %rax
	jmp	.L826
.L838:
	movslq	%esi, %rsi
	movslq	%ecx, %rdx
	movb	$1, -88(%rbp)
	leaq	15(%rax,%rsi), %rax
	addq	%rax, %rdx
	movq	%rdx, -72(%rbp)
	jmp	.L841
.L839:
	movq	15(%rax), %rdi
	movl	%esi, -452(%rbp)
	movl	%ecx, -448(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-452(%rbp), %rsi
	movslq	-448(%rbp), %rcx
	movb	$0, -88(%rbp)
	leaq	(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rdx
	movq	%rdx, -72(%rbp)
	leaq	2(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movzwl	(%rax), %esi
	movq	%rdx, %rax
	jmp	.L844
.L836:
	movq	15(%rax), %rdi
	movl	%esi, -452(%rbp)
	movl	%ecx, -448(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-452(%rbp), %rsi
	movslq	-448(%rbp), %rdx
	movb	$1, -88(%rbp)
	addq	%rsi, %rax
	addq	%rax, %rdx
	movq	%rdx, -72(%rbp)
	jmp	.L841
	.p2align 4,,10
	.p2align 3
.L968:
	movl	-100(%rbp), %edi
	subl	%eax, %edi
	cmpl	$32, %edi
	sete	-372(%rbp)
	jne	.L874
.L876:
	movq	-408(%rbp), %rdi
	leaq	-376(%rbp), %rsi
	movl	%r8d, -412(%rbp)
	call	_ZN2v88internal18ConsStringIterator6SearchEPi
	movl	-412(%rbp), %r8d
.L875:
	testq	%rax, %rax
	je	.L888
	movslq	11(%rax), %rsi
	xorl	%ecx, %ecx
.L918:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L782
	leaq	.L879(%rip), %rdi
	movzwl	%dx, %edx
	movslq	(%rdi,%rdx,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEiiPi
	.align 4
	.align 4
.L879:
	.long	.L885-.L879
	.long	.L882-.L879
	.long	.L884-.L879
	.long	.L880-.L879
	.long	.L782-.L879
	.long	.L878-.L879
	.long	.L782-.L879
	.long	.L782-.L879
	.long	.L883-.L879
	.long	.L882-.L879
	.long	.L881-.L879
	.long	.L880-.L879
	.long	.L782-.L879
	.long	.L878-.L879
	.section	.text._ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEiiPi
	.p2align 4,,10
	.p2align 3
.L883:
	movslq	%ecx, %rcx
	movb	$1, -88(%rbp)
	leaq	15(%rax,%rcx), %rax
	movq	%rax, -80(%rbp)
	addq	%rsi, %rax
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L882:
	leal	-1(%r13), %eax
	cmpl	%eax, %r12d
	jle	.L888
	movq	-80(%rbp), %rdx
	cmpq	-72(%rbp), %rdx
	jne	.L961
	movl	$0, -372(%rbp)
	movl	-104(%rbp), %eax
	testl	%eax, %eax
	je	.L961
	movl	-100(%rbp), %esi
	subl	%eax, %esi
	cmpl	$32, %esi
	sete	-376(%rbp)
	jne	.L893
.L895:
	movq	-408(%rbp), %rdi
	leaq	-372(%rbp), %rsi
	movl	%r8d, -412(%rbp)
	call	_ZN2v88internal18ConsStringIterator6SearchEPi
	movl	-412(%rbp), %r8d
.L894:
	testq	%rax, %rax
	je	.L974
	movslq	11(%rax), %rcx
	xorl	%esi, %esi
.L917:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L782
	leaq	.L898(%rip), %rdi
	movzwl	%dx, %edx
	movslq	(%rdi,%rdx,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEiiPi
	.align 4
	.align 4
.L898:
	.long	.L904-.L898
	.long	.L901-.L898
	.long	.L903-.L898
	.long	.L899-.L898
	.long	.L782-.L898
	.long	.L897-.L898
	.long	.L782-.L898
	.long	.L782-.L898
	.long	.L902-.L898
	.long	.L901-.L898
	.long	.L900-.L898
	.long	.L899-.L898
	.long	.L782-.L898
	.long	.L897-.L898
	.section	.text._ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEiiPi
	.p2align 4,,10
	.p2align 3
.L878:
	movq	15(%rax), %rax
	jmp	.L918
	.p2align 4,,10
	.p2align 3
.L880:
	addl	27(%rax), %ecx
	movq	15(%rax), %rax
	jmp	.L918
	.p2align 4,,10
	.p2align 3
.L881:
	movq	15(%rax), %rdi
	movl	%ecx, -432(%rbp)
	movl	%esi, -416(%rbp)
	movq	(%rdi), %rax
	movl	%r8d, -412(%rbp)
	call	*48(%rax)
	movslq	-432(%rbp), %rcx
	movb	$1, -88(%rbp)
	movslq	-416(%rbp), %rsi
	movl	-412(%rbp), %r8d
	addq	%rcx, %rax
	movq	%rax, -80(%rbp)
	addq	%rsi, %rax
	movq	%rax, -72(%rbp)
	jmp	.L882
	.p2align 4,,10
	.p2align 3
.L885:
	movslq	%ecx, %rcx
	movb	$0, -88(%rbp)
	leaq	15(%rax,%rcx,2), %rax
	movq	%rax, -80(%rbp)
	leaq	(%rax,%rsi,2), %rax
	movq	%rax, -72(%rbp)
	jmp	.L882
	.p2align 4,,10
	.p2align 3
.L884:
	movq	15(%rax), %rdi
	movl	%ecx, -432(%rbp)
	movl	%esi, -416(%rbp)
	movq	(%rdi), %rax
	movl	%r8d, -412(%rbp)
	call	*48(%rax)
	movslq	-432(%rbp), %rcx
	movb	$0, -88(%rbp)
	movslq	-416(%rbp), %rsi
	movl	-412(%rbp), %r8d
	leaq	(%rax,%rcx,2), %rax
	movq	%rax, -80(%rbp)
	leaq	(%rax,%rsi,2), %rax
	movq	%rax, -72(%rbp)
	jmp	.L882
	.p2align 4,,10
	.p2align 3
.L897:
	movq	15(%rax), %rax
	jmp	.L917
	.p2align 4,,10
	.p2align 3
.L899:
	addl	27(%rax), %esi
	movq	15(%rax), %rax
	jmp	.L917
	.p2align 4,,10
	.p2align 3
.L901:
	movzbl	-88(%rbp), %eax
	movq	-80(%rbp), %rdx
	jmp	.L890
.L902:
	movslq	%esi, %rsi
	movb	$1, -88(%rbp)
	leaq	15(%rax,%rsi), %rdx
	movslq	%ecx, %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	jmp	.L905
.L903:
	movq	15(%rax), %rdi
	movl	%esi, -432(%rbp)
	movl	%ecx, -416(%rbp)
	movq	(%rdi), %rax
	movl	%r8d, -412(%rbp)
	call	*48(%rax)
	movslq	-432(%rbp), %rsi
	movb	$0, -88(%rbp)
	movslq	-416(%rbp), %rcx
	movl	-412(%rbp), %r8d
	leaq	(%rax,%rsi,2), %rdx
	leaq	(%rdx,%rcx,2), %rax
	leaq	2(%rdx), %rcx
	movq	%rax, -72(%rbp)
	movq	%rcx, -80(%rbp)
	movzwl	(%rdx), %eax
	movq	%rcx, %rdx
	jmp	.L908
.L900:
	movq	15(%rax), %rdi
	movl	%esi, -432(%rbp)
	movl	%r8d, -412(%rbp)
	movq	(%rdi), %rax
	movl	%ecx, -416(%rbp)
	call	*48(%rax)
	movslq	-432(%rbp), %rsi
	movb	$1, -88(%rbp)
	movl	-412(%rbp), %r8d
	leaq	(%rax,%rsi), %rdx
	movslq	-416(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L970:
	movl	%esi, %edi
	andl	$63, %eax
	movl	%esi, %r14d
	shrl	$6, %edi
	orl	$-128, %eax
	orl	$-64, %edi
	movb	%al, 1(%rcx)
	movl	$2, %eax
	movb	%dil, (%rcx)
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L971:
	movl	%eax, %edi
	andw	$-1024, %di
	cmpw	$-9216, %di
	jne	.L914
	movl	%r14d, %r11d
	andl	$1023, %eax
	movl	%esi, %r14d
	sall	$10, %r11d
	andl	$1047552, %r11d
	orl	%r11d, %eax
	addl	$65536, %eax
	movl	%eax, %edi
	shrl	$18, %edi
	orl	$-16, %edi
	movb	%dil, -3(%rcx)
	movl	%eax, %edi
	shrl	$12, %edi
	andl	$63, %edi
	orl	$-128, %edi
	movb	%dil, -2(%rcx)
	movl	%eax, %edi
	andl	$63, %eax
	shrl	$6, %edi
	orl	$-128, %eax
	andl	$63, %edi
	movb	%al, (%rcx)
	movl	$1, %eax
	orl	$-128, %edi
	movb	%dil, -1(%rcx)
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L972:
	movq	%r13, %r14
	movl	$0, -104(%rbp)
	movl	-412(%rbp), %r12d
	movl	-408(%rbp), %r13d
	jmp	.L822
	.p2align 4,,10
	.p2align 3
.L808:
	leaq	-372(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18ConsStringIterator8NextLeafEPb
	cmpb	$0, -372(%rbp)
	je	.L809
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L874:
	movq	-408(%rbp), %rdi
	leaq	-372(%rbp), %rsi
	movl	%r8d, -412(%rbp)
	call	_ZN2v88internal18ConsStringIterator8NextLeafEPb
	cmpb	$0, -372(%rbp)
	movl	-412(%rbp), %r8d
	je	.L875
	jmp	.L876
.L973:
	movl	$0, -104(%rbp)
	movq	-80(%rbp), %rax
	jmp	.L960
.L974:
	movl	$0, -104(%rbp)
	movq	-80(%rbp), %rdx
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L829:
	leaq	-372(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18ConsStringIterator8NextLeafEPb
	cmpb	$0, -372(%rbp)
	je	.L830
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L893:
	movq	-408(%rbp), %rdi
	leaq	-376(%rbp), %rsi
	movl	%r8d, -412(%rbp)
	call	_ZN2v88internal18ConsStringIterator8NextLeafEPb
	cmpb	$0, -376(%rbp)
	movl	-412(%rbp), %r8d
	je	.L894
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L959:
	movq	-440(%rbp), %rax
	movq	(%rax), %r13
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L967:
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rax
	movl	$0, -104(%rbp)
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L782:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L796:
	movq	15(%rax), %rax
	jmp	.L921
.L798:
	addl	27(%rax), %r13d
	movq	15(%rax), %rax
	jmp	.L921
.L800:
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rsi
	jmp	.L794
.L862:
	movq	15(%rax), %rax
	jmp	.L919
.L864:
	addl	27(%rax), %ecx
	movq	15(%rax), %rax
	jmp	.L919
.L866:
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rax
	jmp	.L860
.L869:
	movslq	%ecx, %rcx
	subl	%esi, %ebx
	movb	$0, -88(%rbp)
	leaq	15(%rax,%rcx,2), %rdx
	movslq	%ebx, %rax
	leaq	(%rdx,%rax,2), %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
	jmp	.L860
.L802:
	movq	15(%rax), %rdi
	subl	%esi, %ebx
	movslq	%ebx, %rbx
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	%r13d, %rcx
	movb	$0, -88(%rbp)
	leaq	(%rax,%rcx,2), %rax
	leaq	(%rax,%rbx,2), %rsi
	movq	%rax, -80(%rbp)
	movq	%rsi, -72(%rbp)
	jmp	.L794
.L865:
	movq	15(%rax), %rdi
	movl	%ecx, -408(%rbp)
	subl	%esi, %ebx
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-408(%rbp), %rcx
	movb	$1, -88(%rbp)
	leaq	(%rax,%rcx), %rdx
	movslq	%ebx, %rax
	addq	%rdx, %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
	jmp	.L860
.L803:
	subl	%esi, %ebx
	movslq	%r13d, %rcx
	movb	$0, -88(%rbp)
	leaq	15(%rax,%rcx,2), %rax
	movslq	%ebx, %rdx
	leaq	(%rax,%rdx,2), %rsi
	movq	%rax, -80(%rbp)
	movq	%rsi, -72(%rbp)
	jmp	.L794
.L867:
	movslq	%ecx, %rcx
	subl	%esi, %ebx
	movb	$1, -88(%rbp)
	leaq	15(%rax,%rcx), %rdx
	movslq	%ebx, %rax
	addq	%rdx, %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
	jmp	.L860
.L868:
	movq	15(%rax), %rdi
	movl	%ecx, -408(%rbp)
	subl	%esi, %ebx
	movslq	%ebx, %rbx
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-408(%rbp), %rcx
	movb	$0, -88(%rbp)
	leaq	(%rax,%rcx,2), %rdx
	leaq	(%rdx,%rbx,2), %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
	jmp	.L860
.L799:
	movq	15(%rax), %rdi
	subl	%esi, %ebx
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	%r13d, %rcx
	movslq	%ebx, %rsi
	movb	$1, -88(%rbp)
	addq	%rcx, %rax
	addq	%rax, %rsi
	movq	%rax, -80(%rbp)
	movq	%rsi, -72(%rbp)
	jmp	.L794
.L801:
	movslq	%r13d, %rcx
	subl	%esi, %ebx
	movb	$1, -88(%rbp)
	leaq	15(%rax,%rcx), %rax
	movslq	%ebx, %rsi
	addq	%rax, %rsi
	movq	%rax, -80(%rbp)
	movq	%rsi, -72(%rbp)
	jmp	.L794
.L969:
	movl	%esi, %r14d
	jmp	.L909
.L966:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L847
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L964:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19676:
	.size	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEiiPi, .-_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEiiPi
	.section	.text._ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi
	.type	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi, @function
_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi:
.LFB19677:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	pushq	%r8
	xorl	%r8d, %r8d
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEiiPi
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L978
	movq	%r12, %rax
	movq	-8(%rbp), %r12
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L978:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19677:
	.size	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi, .-_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi
	.section	.rodata._ZN2v88internal6String16StringShortPrintEPNS0_12StringStreamEb.str1.1,"aMS",@progbits,1
.LC2:
	.string	"#"
.LC3:
	.string	""
.LC4:
	.string	"<Very long string[%s%u]>"
.LC5:
	.string	"<Invalid String>"
.LC6:
	.string	"<String[%s%u]: "
.LC7:
	.string	"<String[%s%u]\\: "
.LC8:
	.string	"\\n"
.LC9:
	.string	"\\r"
.LC10:
	.string	"\\\\"
.LC11:
	.string	"\\x%02x"
	.section	.text._ZN2v88internal6String16StringShortPrintEPNS0_12StringStreamEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String16StringShortPrintEPNS0_12StringStreamEb
	.type	_ZN2v88internal6String16StringShortPrintEPNS0_12StringStreamEb, @function
_ZN2v88internal6String16StringShortPrintEPNS0_12StringStreamEb:
.LFB19666:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	leaq	.LC2(%rip), %rdx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$408, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-1(%r15), %rax
	movl	11(%r15), %ebx
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	leaq	.LC3(%rip), %rax
	cmove	%rdx, %rax
	movq	%rax, -424(%rbp)
	cmpl	$1024, %ebx
	jg	.L1148
	movq	%rdi, %r13
	movq	%r15, %rdi
	call	_ZN2v88internal12ReadOnlyHeap8ContainsENS0_10HeapObjectE@PLT
	testb	%al, %al
	jne	.L983
	andq	$-262144, %r15
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.L985
	movq	0(%r13), %rsi
	call	_ZN2v88internal4Heap8ContainsENS0_10HeapObjectE@PLT
	testb	%al, %al
	je	.L985
.L983:
	movl	$32, %ecx
	xorl	%eax, %eax
	movq	0(%r13), %rdx
	pxor	%xmm0, %xmm0
	leaq	-368(%rbp), %r15
	movl	$0, -404(%rbp)
	movq	%r15, %rdi
	rep stosq
	movb	$0, -88(%rbp)
	leaq	.L989(%rip), %rdi
	movq	$0, -112(%rbp)
	movaps	%xmm0, -80(%rbp)
	movslq	11(%rdx), %rsi
.L986:
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L987
	movzwl	%ax, %eax
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6String16StringShortPrintEPNS0_12StringStreamEb,"a",@progbits
	.align 4
	.align 4
.L989:
	.long	.L995-.L989
	.long	.L992-.L989
	.long	.L994-.L989
	.long	.L990-.L989
	.long	.L987-.L989
	.long	.L988-.L989
	.long	.L987-.L989
	.long	.L987-.L989
	.long	.L993-.L989
	.long	.L992-.L989
	.long	.L991-.L989
	.long	.L990-.L989
	.long	.L987-.L989
	.long	.L988-.L989
	.section	.text._ZN2v88internal6String16StringShortPrintEPNS0_12StringStreamEb
	.p2align 4,,10
	.p2align 3
.L985:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	movl	$16, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
.L979:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1149
	addq	$408, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L992:
	.cfi_restore_state
	movl	$0, -104(%rbp)
	movl	-404(%rbp), %eax
	testq	%rdx, %rdx
	je	.L998
	movl	%eax, -96(%rbp)
	leaq	-404(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$141733920769, %rax
	movq	%rdx, -112(%rbp)
	movq	%rax, -104(%rbp)
	movl	$0, -404(%rbp)
	call	_ZN2v88internal18ConsStringIterator6SearchEPi
	testq	%rax, %rax
	je	.L996
	movl	-404(%rbp), %r8d
	movl	11(%rax), %edi
	leaq	.L1002(%rip), %rcx
	movl	%r8d, %esi
.L1114:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L987
	movzwl	%dx, %edx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal6String16StringShortPrintEPNS0_12StringStreamEb
	.align 4
	.align 4
.L1002:
	.long	.L1008-.L1002
	.long	.L998-.L1002
	.long	.L1007-.L1002
	.long	.L1003-.L1002
	.long	.L987-.L1002
	.long	.L1001-.L1002
	.long	.L987-.L1002
	.long	.L987-.L1002
	.long	.L1006-.L1002
	.long	.L998-.L1002
	.long	.L1004-.L1002
	.long	.L1003-.L1002
	.long	.L987-.L1002
	.long	.L1001-.L1002
	.section	.text._ZN2v88internal6String16StringShortPrintEPNS0_12StringStreamEb
	.p2align 4,,10
	.p2align 3
.L994:
	movq	15(%rdx), %rdi
	movl	%ecx, -432(%rbp)
	movl	%esi, -428(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-432(%rbp), %rcx
	movslq	-428(%rbp), %rsi
	movb	$0, -88(%rbp)
	leaq	(%rax,%rcx,2), %rax
	movq	%rax, -80(%rbp)
	leaq	(%rax,%rsi,2), %rax
	movq	%rax, -72(%rbp)
.L996:
	movl	$0, -104(%rbp)
.L998:
	testl	%ebx, %ebx
	jle	.L1118
	movq	-80(%rbp), %rax
	xorl	%ecx, %ecx
	movl	$1, %r8d
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1027:
	leaq	1(%rax), %rsi
	movq	%rsi, -80(%rbp)
	movzbl	(%rax), %edx
	movq	%rsi, %rax
.L1030:
	subl	$32, %edx
	cmpw	$95, %dx
	movl	$0, %edx
	cmovnb	%edx, %r8d
	addl	$1, %ecx
	cmpl	%ebx, %ecx
	je	.L1010
.L1032:
	cmpq	%rax, -72(%rbp)
	je	.L1011
.L1142:
	movzbl	-88(%rbp), %edx
.L1012:
	testb	%dl, %dl
	jne	.L1027
.L1028:
	leaq	2(%rax), %rsi
	movq	%rsi, -80(%rbp)
	movzwl	(%rax), %edx
	movq	%rsi, %rax
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1011:
	movl	$0, -400(%rbp)
	movl	-104(%rbp), %edx
	testl	%edx, %edx
	je	.L1142
	movl	-100(%rbp), %eax
	subl	%edx, %eax
	cmpl	$32, %eax
	sete	-388(%rbp)
	jne	.L1015
.L1017:
	leaq	-400(%rbp), %rsi
	movq	%r15, %rdi
	movl	%ecx, -432(%rbp)
	movb	%r8b, -428(%rbp)
	call	_ZN2v88internal18ConsStringIterator6SearchEPi
	movzbl	-428(%rbp), %r8d
	movl	-432(%rbp), %ecx
	leaq	.L1020(%rip), %r9
.L1016:
	testq	%rax, %rax
	je	.L1150
	movslq	11(%rax), %rsi
	xorl	%r10d, %r10d
.L1113:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L987
	movzwl	%dx, %edx
	movslq	(%r9,%rdx,4), %rdx
	addq	%r9, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal6String16StringShortPrintEPNS0_12StringStreamEb
	.align 4
	.align 4
.L1020:
	.long	.L1026-.L1020
	.long	.L1023-.L1020
	.long	.L1025-.L1020
	.long	.L1021-.L1020
	.long	.L987-.L1020
	.long	.L1019-.L1020
	.long	.L987-.L1020
	.long	.L987-.L1020
	.long	.L1024-.L1020
	.long	.L1023-.L1020
	.long	.L1022-.L1020
	.long	.L1021-.L1020
	.long	.L987-.L1020
	.long	.L1019-.L1020
	.section	.text._ZN2v88internal6String16StringShortPrintEPNS0_12StringStreamEb
	.p2align 4,,10
	.p2align 3
.L1118:
	movl	$1, %r8d
	.p2align 4,,10
	.p2align 3
.L1010:
	movq	0(%r13), %rdx
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	.L1035(%rip), %rdi
	movl	$0, -396(%rbp)
	movaps	%xmm0, -80(%rbp)
	movslq	11(%rdx), %rsi
.L1033:
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L987
	movzwl	%ax, %eax
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6String16StringShortPrintEPNS0_12StringStreamEb
	.align 4
	.align 4
.L1035:
	.long	.L1041-.L1035
	.long	.L1038-.L1035
	.long	.L1040-.L1035
	.long	.L1036-.L1035
	.long	.L987-.L1035
	.long	.L1034-.L1035
	.long	.L987-.L1035
	.long	.L987-.L1035
	.long	.L1039-.L1035
	.long	.L1038-.L1035
	.long	.L1037-.L1035
	.long	.L1036-.L1035
	.long	.L987-.L1035
	.long	.L1034-.L1035
	.section	.text._ZN2v88internal6String16StringShortPrintEPNS0_12StringStreamEb
	.p2align 4,,10
	.p2align 3
.L1038:
	movl	$0, -104(%rbp)
	movl	-396(%rbp), %eax
	testq	%rdx, %rdx
	je	.L1044
	movl	%eax, -96(%rbp)
	leaq	-396(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$141733920769, %rax
	movb	%r8b, -428(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%rax, -104(%rbp)
	movl	$0, -396(%rbp)
	call	_ZN2v88internal18ConsStringIterator6SearchEPi
	movzbl	-428(%rbp), %r8d
	testq	%rax, %rax
	je	.L1042
	movl	-396(%rbp), %r9d
	movl	11(%rax), %edi
	leaq	.L1048(%rip), %rcx
	movl	%r9d, %esi
.L1112:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L987
	movzwl	%dx, %edx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal6String16StringShortPrintEPNS0_12StringStreamEb
	.align 4
	.align 4
.L1048:
	.long	.L1054-.L1048
	.long	.L1044-.L1048
	.long	.L1053-.L1048
	.long	.L1049-.L1048
	.long	.L987-.L1048
	.long	.L1047-.L1048
	.long	.L987-.L1048
	.long	.L987-.L1048
	.long	.L1052-.L1048
	.long	.L1044-.L1048
	.long	.L1050-.L1048
	.long	.L1049-.L1048
	.long	.L987-.L1048
	.long	.L1047-.L1048
	.section	.text._ZN2v88internal6String16StringShortPrintEPNS0_12StringStreamEb
	.p2align 4,,10
	.p2align 3
.L1039:
	movslq	%ecx, %rcx
	movb	$1, -88(%rbp)
	leaq	15(%rdx,%rcx), %rax
	movq	%rax, -80(%rbp)
	addq	%rsi, %rax
	movq	%rax, -72(%rbp)
.L1042:
	movl	$0, -104(%rbp)
.L1044:
	testb	%r8b, %r8b
	je	.L1056
	testb	%r14b, %r14b
	jne	.L1151
	testl	%ebx, %ebx
	jle	.L979
.L1058:
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L1080:
	movq	-80(%rbp), %rax
	cmpq	-72(%rbp), %rax
	je	.L1059
.L1145:
	movzbl	-88(%rbp), %ecx
.L1060:
	testb	%cl, %cl
	je	.L1076
.L1075:
	leaq	1(%rax), %rcx
	movq	%r12, %rdi
	addl	$1, %r13d
	movq	%rcx, -80(%rbp)
	movsbl	(%rax), %esi
	call	_ZN2v88internal12StringStream3PutEc@PLT
	cmpl	%r13d, %ebx
	jg	.L1080
.L1144:
	testb	%r14b, %r14b
	je	.L979
.L1081:
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream3PutEc@PLT
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L1148:
	leaq	-368(%rbp), %rcx
	movl	$2, %r8d
	movl	$24, %edx
	movq	%r12, %rdi
	leaq	.LC4(%rip), %rsi
	movq	%rax, -368(%rbp)
	movl	%ebx, -360(%rbp)
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L1056:
	testb	%r14b, %r14b
	jne	.L1152
	testl	%ebx, %ebx
	jle	.L979
.L1115:
	xorl	%r13d, %r13d
	jmp	.L1108
	.p2align 4,,10
	.p2align 3
.L1099:
	leaq	1(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movzbl	(%rax), %eax
	cmpw	$10, %ax
	je	.L1153
.L1103:
	cmpw	$13, %ax
	je	.L1154
	cmpw	$92, %ax
	je	.L1155
	movzwl	%ax, %esi
	subl	$32, %eax
	cmpw	$94, %ax
	jbe	.L1107
	movl	%esi, -384(%rbp)
	leaq	-384(%rbp), %rcx
	movl	$6, %edx
	movq	%r12, %rdi
	movl	$1, %r8d
	leaq	.LC11(%rip), %rsi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
.L1104:
	addl	$1, %r13d
	cmpl	%ebx, %r13d
	jge	.L1144
.L1108:
	movq	-80(%rbp), %rax
	cmpq	-72(%rbp), %rax
	je	.L1083
.L1146:
	movzbl	-88(%rbp), %edx
.L1084:
	testb	%dl, %dl
	jne	.L1099
.L1100:
	leaq	2(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movzwl	(%rax), %eax
	cmpw	$10, %ax
	jne	.L1103
.L1153:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	movl	$2, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	jmp	.L1104
	.p2align 4,,10
	.p2align 3
.L1034:
	movq	15(%rdx), %rdx
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1036:
	addl	27(%rdx), %ecx
	movq	15(%rdx), %rdx
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L988:
	movq	15(%rdx), %rdx
	jmp	.L986
	.p2align 4,,10
	.p2align 3
.L990:
	addl	27(%rdx), %ecx
	movq	15(%rdx), %rdx
	jmp	.L986
	.p2align 4,,10
	.p2align 3
.L1151:
	movq	0(%r13), %rax
	movq	-424(%rbp), %rdi
	leaq	-384(%rbp), %rcx
	movl	$2, %r8d
	leaq	.LC6(%rip), %rsi
	movl	$15, %edx
	movl	11(%rax), %eax
	movq	%rdi, -384(%rbp)
	movq	%r12, %rdi
	movl	%eax, -376(%rbp)
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	testl	%ebx, %ebx
	jg	.L1058
	jmp	.L1081
	.p2align 4,,10
	.p2align 3
.L1152:
	movq	0(%r13), %rax
	movq	-424(%rbp), %rdi
	leaq	-384(%rbp), %rcx
	movl	$2, %r8d
	leaq	.LC7(%rip), %rsi
	movl	$16, %edx
	movl	11(%rax), %eax
	movq	%rdi, -384(%rbp)
	movq	%r12, %rdi
	movl	%eax, -376(%rbp)
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	testl	%ebx, %ebx
	jg	.L1115
	jmp	.L1081
	.p2align 4,,10
	.p2align 3
.L1015:
	leaq	-388(%rbp), %rsi
	movq	%r15, %rdi
	movl	%ecx, -432(%rbp)
	movb	%r8b, -428(%rbp)
	call	_ZN2v88internal18ConsStringIterator8NextLeafEPb
	cmpb	$0, -388(%rbp)
	movzbl	-428(%rbp), %r8d
	leaq	.L1020(%rip), %r9
	movl	-432(%rbp), %ecx
	je	.L1016
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1019:
	movq	15(%rax), %rax
	jmp	.L1113
	.p2align 4,,10
	.p2align 3
.L1021:
	addl	27(%rax), %r10d
	movq	15(%rax), %rax
	jmp	.L1113
	.p2align 4,,10
	.p2align 3
.L1023:
	movzbl	-88(%rbp), %edx
	movq	-80(%rbp), %rax
	jmp	.L1012
	.p2align 4,,10
	.p2align 3
.L1037:
	movq	15(%rdx), %rdi
	movl	%ecx, -436(%rbp)
	movl	%esi, -432(%rbp)
	movq	(%rdi), %rax
	movb	%r8b, -428(%rbp)
	call	*48(%rax)
	movslq	-436(%rbp), %rcx
	movb	$1, -88(%rbp)
	movslq	-432(%rbp), %rsi
	movzbl	-428(%rbp), %r8d
	addq	%rcx, %rax
	movq	%rax, -80(%rbp)
	addq	%rsi, %rax
	movq	%rax, -72(%rbp)
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L1041:
	movslq	%ecx, %rcx
	movb	$0, -88(%rbp)
	leaq	15(%rdx,%rcx,2), %rax
	movq	%rax, -80(%rbp)
	leaq	(%rax,%rsi,2), %rax
	movq	%rax, -72(%rbp)
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L995:
	movslq	%ecx, %rcx
	movb	$0, -88(%rbp)
	leaq	15(%rdx,%rcx,2), %rax
	movq	%rax, -80(%rbp)
	leaq	(%rax,%rsi,2), %rax
	movq	%rax, -72(%rbp)
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1040:
	movq	15(%rdx), %rdi
	movl	%ecx, -436(%rbp)
	movl	%esi, -432(%rbp)
	movq	(%rdi), %rax
	movb	%r8b, -428(%rbp)
	call	*48(%rax)
	movslq	-436(%rbp), %rcx
	movb	$0, -88(%rbp)
	movslq	-432(%rbp), %rsi
	movzbl	-428(%rbp), %r8d
	leaq	(%rax,%rcx,2), %rax
	movq	%rax, -80(%rbp)
	leaq	(%rax,%rsi,2), %rax
	movq	%rax, -72(%rbp)
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L993:
	movslq	%ecx, %rcx
	movb	$1, -88(%rbp)
	leaq	15(%rdx,%rcx), %rax
	movq	%rax, -80(%rbp)
	addq	%rsi, %rax
	movq	%rax, -72(%rbp)
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L991:
	movq	15(%rdx), %rdi
	movl	%ecx, -432(%rbp)
	movl	%esi, -428(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-432(%rbp), %rcx
	movslq	-428(%rbp), %rsi
	movb	$1, -88(%rbp)
	addq	%rcx, %rax
	movq	%rax, -80(%rbp)
	addq	%rsi, %rax
	movq	%rax, -72(%rbp)
	jmp	.L996
.L1022:
	movq	15(%rax), %rdi
	movl	%r10d, -436(%rbp)
	movl	%ecx, -432(%rbp)
	movq	(%rdi), %rax
	movb	%r8b, -428(%rbp)
	movl	%esi, -440(%rbp)
	call	*48(%rax)
	movslq	-436(%rbp), %r10
	movb	$1, -88(%rbp)
	movslq	-440(%rbp), %rdx
	movzbl	-428(%rbp), %r8d
	addq	%r10, %rax
	movl	-432(%rbp), %ecx
	addq	%rax, %rdx
	movq	%rdx, -72(%rbp)
	jmp	.L1027
.L1024:
	movslq	%r10d, %r10
	movslq	%esi, %rdx
	movb	$1, -88(%rbp)
	leaq	15(%rax,%r10), %rax
	addq	%rax, %rdx
	movq	%rdx, -72(%rbp)
	jmp	.L1027
.L1025:
	movq	15(%rax), %rdi
	movl	%esi, -440(%rbp)
	movl	%r10d, -436(%rbp)
	movq	(%rdi), %rax
	movl	%ecx, -432(%rbp)
	movb	%r8b, -428(%rbp)
	call	*48(%rax)
	movslq	-436(%rbp), %r10
	movb	$0, -88(%rbp)
	movslq	-440(%rbp), %rsi
	movzbl	-428(%rbp), %r8d
	leaq	(%rax,%r10,2), %rax
	movl	-432(%rbp), %ecx
	leaq	(%rax,%rsi,2), %rdx
	movq	%rdx, -72(%rbp)
	jmp	.L1028
.L1026:
	movslq	%r10d, %r10
	movb	$0, -88(%rbp)
	leaq	15(%rax,%r10,2), %rax
	leaq	(%rax,%rsi,2), %rdx
	movq	%rdx, -72(%rbp)
	jmp	.L1028
.L1074:
	movslq	%r8d, %r8
	movb	$0, -88(%rbp)
	leaq	15(%rax,%r8,2), %rax
	leaq	(%rax,%rsi,2), %rcx
	movq	%rcx, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L1076:
	leaq	2(%rax), %rcx
	movq	%r12, %rdi
	addl	$1, %r13d
	movq	%rcx, -80(%rbp)
	movsbl	(%rax), %esi
	call	_ZN2v88internal12StringStream3PutEc@PLT
	cmpl	%ebx, %r13d
	jge	.L1144
	movq	-80(%rbp), %rax
	cmpq	-72(%rbp), %rax
	jne	.L1145
.L1059:
	movl	$0, -392(%rbp)
	movl	-104(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L1145
	movl	-100(%rbp), %eax
	subl	%ecx, %eax
	cmpl	$32, %eax
	sete	-388(%rbp)
	jne	.L1063
.L1065:
	leaq	-392(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal18ConsStringIterator6SearchEPi
.L1064:
	testq	%rax, %rax
	je	.L1156
	movslq	11(%rax), %rsi
	xorl	%r8d, %r8d
.L1110:
	movq	-1(%rax), %rcx
	movzwl	11(%rcx), %ecx
	andl	$15, %ecx
	cmpw	$13, %cx
	ja	.L987
	leaq	.L1068(%rip), %rdi
	movzwl	%cx, %ecx
	movslq	(%rdi,%rcx,4), %rcx
	addq	%rdi, %rcx
	notrack jmp	*%rcx
	.section	.rodata._ZN2v88internal6String16StringShortPrintEPNS0_12StringStreamEb
	.align 4
	.align 4
.L1068:
	.long	.L1074-.L1068
	.long	.L1071-.L1068
	.long	.L1073-.L1068
	.long	.L1069-.L1068
	.long	.L987-.L1068
	.long	.L1067-.L1068
	.long	.L987-.L1068
	.long	.L987-.L1068
	.long	.L1072-.L1068
	.long	.L1071-.L1068
	.long	.L1070-.L1068
	.long	.L1069-.L1068
	.long	.L987-.L1068
	.long	.L1067-.L1068
	.section	.text._ZN2v88internal6String16StringShortPrintEPNS0_12StringStreamEb
	.p2align 4,,10
	.p2align 3
.L1107:
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream3PutEc@PLT
	jmp	.L1104
.L1156:
	movl	$0, -104(%rbp)
	movq	-80(%rbp), %rax
	jmp	.L1145
	.p2align 4,,10
	.p2align 3
.L1083:
	movl	$0, -388(%rbp)
	movl	-104(%rbp), %edx
	testl	%edx, %edx
	je	.L1146
	movl	-100(%rbp), %eax
	subl	%edx, %eax
	cmpl	$32, %eax
	sete	-392(%rbp)
	jne	.L1087
.L1089:
	leaq	-388(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal18ConsStringIterator6SearchEPi
.L1088:
	testq	%rax, %rax
	je	.L1157
	movslq	11(%rax), %rcx
	xorl	%esi, %esi
.L1111:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L987
	leaq	.L1092(%rip), %rdi
	movzwl	%dx, %edx
	movslq	(%rdi,%rdx,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal6String16StringShortPrintEPNS0_12StringStreamEb
	.align 4
	.align 4
.L1092:
	.long	.L1098-.L1092
	.long	.L1095-.L1092
	.long	.L1097-.L1092
	.long	.L1093-.L1092
	.long	.L987-.L1092
	.long	.L1091-.L1092
	.long	.L987-.L1092
	.long	.L987-.L1092
	.long	.L1096-.L1092
	.long	.L1095-.L1092
	.long	.L1094-.L1092
	.long	.L1093-.L1092
	.long	.L987-.L1092
	.long	.L1091-.L1092
	.section	.text._ZN2v88internal6String16StringShortPrintEPNS0_12StringStreamEb
.L1157:
	movl	$0, -104(%rbp)
	movq	-80(%rbp), %rax
	jmp	.L1146
	.p2align 4,,10
	.p2align 3
.L1154:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	movl	$2, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	jmp	.L1104
	.p2align 4,,10
	.p2align 3
.L1155:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	movl	$2, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	jmp	.L1104
.L1150:
	movl	$0, -104(%rbp)
	movq	-80(%rbp), %rax
	jmp	.L1142
	.p2align 4,,10
	.p2align 3
.L987:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1091:
	movq	15(%rax), %rax
	jmp	.L1111
.L1093:
	addl	27(%rax), %esi
	movq	15(%rax), %rax
	jmp	.L1111
.L1095:
	movzbl	-88(%rbp), %edx
	movq	-80(%rbp), %rax
	jmp	.L1084
.L1067:
	movq	15(%rax), %rax
	jmp	.L1110
.L1069:
	addl	27(%rax), %r8d
	movq	15(%rax), %rax
	jmp	.L1110
.L1071:
	movzbl	-88(%rbp), %ecx
	movq	-80(%rbp), %rax
	jmp	.L1060
.L1098:
	movslq	%esi, %rsi
	movb	$0, -88(%rbp)
	leaq	15(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rdx
	movq	%rdx, -72(%rbp)
	jmp	.L1100
.L1070:
	movq	15(%rax), %rdi
	movl	%r8d, -424(%rbp)
	movl	%esi, -428(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-424(%rbp), %r8
	movslq	-428(%rbp), %rcx
	movb	$1, -88(%rbp)
	addq	%r8, %rax
	addq	%rax, %rcx
	movq	%rcx, -72(%rbp)
	jmp	.L1075
.L1072:
	movslq	%r8d, %r8
	movslq	%esi, %rcx
	movb	$1, -88(%rbp)
	leaq	15(%rax,%r8), %rax
	addq	%rax, %rcx
	movq	%rcx, -72(%rbp)
	jmp	.L1075
.L1094:
	movq	15(%rax), %rdi
	movl	%esi, -428(%rbp)
	movl	%ecx, -424(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-428(%rbp), %rsi
	movslq	-424(%rbp), %rdx
	movb	$1, -88(%rbp)
	addq	%rsi, %rax
	addq	%rax, %rdx
	movq	%rdx, -72(%rbp)
	jmp	.L1099
.L1096:
	movslq	%esi, %rsi
	movslq	%ecx, %rdx
	movb	$1, -88(%rbp)
	leaq	15(%rax,%rsi), %rax
	addq	%rax, %rdx
	movq	%rdx, -72(%rbp)
	jmp	.L1099
.L1097:
	movq	15(%rax), %rdi
	movl	%esi, -428(%rbp)
	movl	%ecx, -424(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-428(%rbp), %rsi
	movslq	-424(%rbp), %rcx
	movb	$0, -88(%rbp)
	leaq	(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rdx
	movq	%rdx, -72(%rbp)
	jmp	.L1100
.L1073:
	movq	15(%rax), %rdi
	movl	%esi, -428(%rbp)
	movl	%r8d, -424(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-424(%rbp), %r8
	movslq	-428(%rbp), %rsi
	movb	$0, -88(%rbp)
	leaq	(%rax,%r8,2), %rax
	leaq	(%rax,%rsi,2), %rcx
	movq	%rcx, -72(%rbp)
	jmp	.L1076
.L1047:
	movq	15(%rax), %rax
	jmp	.L1112
.L1049:
	addl	27(%rax), %esi
	movq	15(%rax), %rax
	jmp	.L1112
.L1001:
	movq	15(%rax), %rax
	jmp	.L1114
.L1003:
	addl	27(%rax), %esi
	movq	15(%rax), %rax
	jmp	.L1114
.L1050:
	subl	%r9d, %edi
	movl	%esi, -436(%rbp)
	movl	%edi, -428(%rbp)
	movq	15(%rax), %rdi
	movb	%r8b, -432(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-436(%rbp), %rsi
	movb	$1, -88(%rbp)
	movslq	-428(%rbp), %rdx
	movzbl	-432(%rbp), %r8d
	addq	%rsi, %rax
	movq	%rax, -80(%rbp)
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	jmp	.L1044
.L1052:
	movslq	%esi, %rsi
	subl	%r9d, %edi
	movb	$1, -88(%rbp)
	leaq	15(%rax,%rsi), %rax
	movslq	%edi, %rdx
	movq	%rax, -80(%rbp)
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	jmp	.L1044
.L1007:
	subl	%r8d, %edi
	movl	%esi, -432(%rbp)
	movl	%edi, -428(%rbp)
	movq	15(%rax), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-432(%rbp), %rsi
	movb	$0, -88(%rbp)
	leaq	(%rax,%rsi,2), %rdx
	movslq	-428(%rbp), %rax
	movq	%rdx, -80(%rbp)
	leaq	(%rdx,%rax,2), %rax
	movq	%rax, -72(%rbp)
	jmp	.L998
.L1008:
	movslq	%esi, %rsi
	subl	%r8d, %edi
	movb	$0, -88(%rbp)
	leaq	15(%rax,%rsi,2), %rdx
	movslq	%edi, %rax
	leaq	(%rdx,%rax,2), %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
	jmp	.L998
.L1053:
	subl	%r9d, %edi
	movl	%esi, -436(%rbp)
	movl	%edi, -428(%rbp)
	movq	15(%rax), %rdi
	movb	%r8b, -432(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-436(%rbp), %rsi
	movb	$0, -88(%rbp)
	movzbl	-432(%rbp), %r8d
	leaq	(%rax,%rsi,2), %rdx
	movslq	-428(%rbp), %rax
	movq	%rdx, -80(%rbp)
	leaq	(%rdx,%rax,2), %rax
	movq	%rax, -72(%rbp)
	jmp	.L1044
.L1054:
	movslq	%esi, %rsi
	subl	%r9d, %edi
	movb	$0, -88(%rbp)
	leaq	15(%rax,%rsi,2), %rdx
	movslq	%edi, %rax
	leaq	(%rdx,%rax,2), %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
	jmp	.L1044
.L1004:
	subl	%r8d, %edi
	movl	%esi, -432(%rbp)
	movl	%edi, -428(%rbp)
	movq	15(%rax), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-432(%rbp), %rsi
	movslq	-428(%rbp), %rdx
	movb	$1, -88(%rbp)
	addq	%rsi, %rax
	movq	%rax, -80(%rbp)
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	jmp	.L998
.L1006:
	movslq	%esi, %rsi
	subl	%r8d, %edi
	movb	$1, -88(%rbp)
	leaq	15(%rax,%rsi), %rax
	movslq	%edi, %rdx
	movq	%rax, -80(%rbp)
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	jmp	.L998
.L1087:
	leaq	-392(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal18ConsStringIterator8NextLeafEPb
	cmpb	$0, -392(%rbp)
	je	.L1088
	jmp	.L1089
.L1063:
	leaq	-388(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal18ConsStringIterator8NextLeafEPb
	cmpb	$0, -388(%rbp)
	je	.L1064
	jmp	.L1065
.L1149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19666:
	.size	_ZN2v88internal6String16StringShortPrintEPNS0_12StringStreamEb, .-_ZN2v88internal6String16StringShortPrintEPNS0_12StringStreamEb
	.section	.text._ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_,"axG",@progbits,_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
	.type	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_, @function
_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_:
.LFB22549:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	(%rdi), %eax
	andl	$15, %eax
	cmpl	$13, %eax
	ja	.L1159
	leaq	.L1161(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_,"aG",@progbits,_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_,comdat
	.align 4
	.align 4
.L1161:
	.long	.L1167-.L1161
	.long	.L1164-.L1161
	.long	.L1166-.L1161
	.long	.L1162-.L1161
	.long	.L1159-.L1161
	.long	.L1160-.L1161
	.long	.L1159-.L1161
	.long	.L1159-.L1161
	.long	.L1165-.L1161
	.long	.L1164-.L1161
	.long	.L1163-.L1161
	.long	.L1162-.L1161
	.long	.L1159-.L1161
	.long	.L1160-.L1161
	.section	.text._ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_,"axG",@progbits,_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_,comdat
	.p2align 4,,10
	.p2align 3
.L1160:
	movq	(%rsi), %rax
	movl	(%rdx), %edx
	leaq	-32(%rbp), %rsi
	leaq	-36(%rbp), %rdi
	movq	15(%rax), %rax
	movl	%edx, -40(%rbp)
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	movq	%rax, -32(%rbp)
	movl	%edx, -36(%rbp)
	leaq	-40(%rbp), %rdx
	call	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
	.p2align 4,,10
	.p2align 3
.L1158:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1171
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1164:
	.cfi_restore_state
	movl	(%rdx), %r8d
	movq	(%rsi), %rax
	leaq	-32(%rbp), %rdi
	movl	%r8d, %esi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal10ConsString3GetEi
	jmp	.L1158
	.p2align 4,,10
	.p2align 3
.L1162:
	movq	(%rsi), %rsi
	movl	(%rdx), %eax
	leaq	-36(%rbp), %rdi
	leaq	-40(%rbp), %rdx
	movq	15(%rsi), %rcx
	addl	27(%rsi), %eax
	leaq	-32(%rbp), %rsi
	movl	%eax, -40(%rbp)
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	movq	%rcx, -32(%rbp)
	movl	%eax, -36(%rbp)
	call	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
	jmp	.L1158
	.p2align 4,,10
	.p2align 3
.L1165:
	movl	(%rdx), %eax
	movq	(%rsi), %rdx
	addl	$16, %eax
	cltq
	movzbl	-1(%rax,%rdx), %eax
	jmp	.L1158
	.p2align 4,,10
	.p2align 3
.L1166:
	movq	(%rsi), %rax
	movslq	(%rdx), %rbx
	movq	15(%rax), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzwl	(%rax,%rbx,2), %eax
	jmp	.L1158
	.p2align 4,,10
	.p2align 3
.L1163:
	movq	(%rsi), %rax
	movslq	(%rdx), %rbx
	movq	15(%rax), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzbl	(%rax,%rbx), %eax
	jmp	.L1158
	.p2align 4,,10
	.p2align 3
.L1167:
	movl	(%rdx), %eax
	movq	(%rsi), %rdx
	leal	16(%rax,%rax), %eax
	cltq
	movzwl	-1(%rax,%rdx), %eax
	jmp	.L1158
.L1159:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1171:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22549:
	.size	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_, .-_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
	.section	.text._ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii,"axG",@progbits,_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii,comdat
	.p2align 4
	.weak	_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii
	.type	_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii, @function
_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii:
.LFB22001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	15(%rdi), %r8
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	.L1176(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.L1173:
	movq	-1(%rdi), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L1173
	movzwl	%ax, %eax
	movslq	(%r14,%rax,4), %rax
	addq	%r14, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii,"aG",@progbits,_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii,comdat
	.align 4
	.align 4
.L1176:
	.long	.L1182-.L1176
	.long	.L1179-.L1176
	.long	.L1181-.L1176
	.long	.L1177-.L1176
	.long	.L1173-.L1176
	.long	.L1175-.L1176
	.long	.L1173-.L1176
	.long	.L1173-.L1176
	.long	.L1180-.L1176
	.long	.L1179-.L1176
	.long	.L1178-.L1176
	.long	.L1177-.L1176
	.long	.L1173-.L1176
	.long	.L1175-.L1176
	.section	.text._ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii,"axG",@progbits,_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii,comdat
	.p2align 4,,10
	.p2align 3
.L1175:
	movq	(%r8), %rdi
	leaq	15(%rdi), %r8
	jmp	.L1173
	.p2align 4,,10
	.p2align 3
.L1179:
	movq	(%r8), %r9
	movl	%ebx, %r11d
	movslq	11(%r9), %r15
	movl	%r15d, %eax
	subl	%r15d, %r11d
	subl	%r13d, %eax
	cmpl	%eax, %r11d
	jl	.L1207
	leaq	23(%rdi), %rbx
	cmpl	%r15d, %r13d
	jl	.L1296
	movq	23(%rdi), %rdi
	subl	%r15d, %r13d
.L1213:
	movl	%r11d, %ebx
	leaq	15(%rdi), %r8
	jmp	.L1173
	.p2align 4,,10
	.p2align 3
.L1177:
	movslq	27(%rdi), %rdx
	movq	(%r8), %rdi
	movq	%r12, %rsi
	leal	(%rbx,%rdx), %ecx
	addl	%r13d, %edx
	call	_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii.localalias
	.p2align 4,,10
	.p2align 3
.L1172:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1293
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1180:
	.cfi_restore_state
	subl	%r13d, %ebx
	movslq	%r13d, %rsi
	movslq	%ebx, %rbx
	leaq	(%r8,%rsi), %rdx
	leaq	(%r12,%rbx,2), %rcx
	cmpq	%rcx, %r12
	jnb	.L1172
	movq	%r12, %rax
	notq	%rax
	addq	%rcx, %rax
	movq	%rax, %r9
	shrq	%r9
	leaq	1(%rsi,%r9), %rsi
	leaq	1(%r9), %rdi
	addq	%rsi, %r8
	leaq	(%r12,%rdi,2), %rsi
	cmpq	%r8, %r12
	setnb	%r8b
	cmpq	%rsi, %rdx
	setnb	%sil
	orb	%sil, %r8b
	je	.L1252
	cmpq	$29, %rax
	jbe	.L1252
	movq	%rdi, %rsi
	xorl	%eax, %eax
	pxor	%xmm1, %xmm1
	andq	$-16, %rsi
	.p2align 4,,10
	.p2align 3
.L1198:
	movdqu	(%rdx,%rax), %xmm0
	movdqa	%xmm0, %xmm2
	punpckhbw	%xmm1, %xmm0
	punpcklbw	%xmm1, %xmm2
	movups	%xmm0, 16(%r12,%rax,2)
	movups	%xmm2, (%r12,%rax,2)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L1198
	movq	%rdi, %rsi
	andq	$-16, %rsi
	leaq	(%r12,%rsi,2), %rax
	addq	%rsi, %rdx
	cmpq	%rsi, %rdi
	je	.L1172
.L1284:
	movzbl	(%rdx), %esi
	movw	%si, (%rax)
	leaq	2(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1172
	movzbl	1(%rdx), %esi
	movw	%si, 2(%rax)
	leaq	4(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1172
	movzbl	2(%rdx), %esi
	movw	%si, 4(%rax)
	leaq	6(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1172
	movzbl	3(%rdx), %esi
	movw	%si, 6(%rax)
	leaq	8(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1172
	movzbl	4(%rdx), %esi
	movw	%si, 8(%rax)
	leaq	10(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1172
	movzbl	5(%rdx), %esi
	movw	%si, 10(%rax)
	leaq	12(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1172
	movzbl	6(%rdx), %esi
	movw	%si, 12(%rax)
	leaq	14(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1172
	movzbl	7(%rdx), %esi
	movw	%si, 14(%rax)
	leaq	16(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1172
	movzbl	8(%rdx), %esi
	movw	%si, 16(%rax)
	leaq	18(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1172
	movzbl	9(%rdx), %esi
	movw	%si, 18(%rax)
	leaq	20(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1172
	movzbl	10(%rdx), %esi
	movw	%si, 20(%rax)
	leaq	22(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1172
	movzbl	11(%rdx), %esi
	movw	%si, 22(%rax)
	leaq	24(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1172
	movzbl	12(%rdx), %esi
	movw	%si, 24(%rax)
	leaq	26(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1172
	movzbl	13(%rdx), %esi
	movw	%si, 26(%rax)
	leaq	28(%rax), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1172
	movzbl	14(%rdx), %edx
	movw	%dx, 28(%rax)
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1252:
	movzbl	(%rdx), %eax
	addq	$2, %r12
	addq	$1, %rdx
	movw	%ax, -2(%r12)
	cmpq	%r12, %rcx
	ja	.L1252
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1178:
	movq	(%r8), %rdi
	subl	%r13d, %ebx
	movslq	%ebx, %rbx
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	%r13d, %r8
	leaq	(%r12,%rbx,2), %rcx
	leaq	(%rax,%r8), %rdx
	cmpq	%rcx, %r12
	jnb	.L1172
	movq	%r12, %rsi
	notq	%rsi
	addq	%rcx, %rsi
	movq	%rsi, %r9
	shrq	%r9
	leaq	1(%r8,%r9), %r8
	leaq	1(%r9), %rdi
	addq	%r8, %rax
	cmpq	%rax, %r12
	leaq	(%r12,%rdi,2), %rax
	setnb	%r8b
	cmpq	%rax, %rdx
	setnb	%al
	orb	%al, %r8b
	je	.L1250
	cmpq	$29, %rsi
	jbe	.L1250
	movq	%rdi, %rsi
	xorl	%eax, %eax
	pxor	%xmm1, %xmm1
	andq	$-16, %rsi
	.p2align 4,,10
	.p2align 3
.L1186:
	movdqu	(%rdx,%rax), %xmm0
	movdqa	%xmm0, %xmm2
	punpckhbw	%xmm1, %xmm0
	punpcklbw	%xmm1, %xmm2
	movups	%xmm0, 16(%r12,%rax,2)
	movups	%xmm2, (%r12,%rax,2)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L1186
	movq	%rdi, %rsi
	andq	$-16, %rsi
	addq	%rsi, %rdx
	leaq	(%r12,%rsi,2), %rax
	cmpq	%rsi, %rdi
	jne	.L1284
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1250:
	movzbl	(%rdx), %eax
	addq	$2, %r12
	addq	$1, %rdx
	movw	%ax, -2(%r12)
	cmpq	%r12, %rcx
	ja	.L1250
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1182:
	subl	%r13d, %ebx
	movslq	%r13d, %r13
	movslq	%ebx, %rbx
	addq	%r13, %r13
	leaq	(%r8,%r13), %rsi
	leaq	(%rbx,%rbx), %rdx
	cmpq	$3, %rbx
	ja	.L1285
	addq	%r12, %rdx
	cmpq	%rdx, %r12
	jnb	.L1172
	leaq	16(%r8,%r13), %rax
	cmpq	%rax, %r12
	leaq	16(%r12), %rax
	setnb	%cl
	cmpq	%rax, %rsi
	setnb	%al
	orb	%al, %cl
	je	.L1253
	movq	%r12, %rcx
	notq	%rcx
	addq	%rdx, %rcx
	cmpq	$13, %rcx
	jbe	.L1253
	shrq	%rcx
	xorl	%eax, %eax
	addq	$1, %rcx
	movq	%rcx, %rdi
	shrq	$3, %rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L1204:
	movdqu	(%rsi,%rax), %xmm4
	movups	%xmm4, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L1204
	movq	%rcx, %rax
	andq	$-8, %rax
	leaq	(%rax,%rax), %rdi
	addq	%rdi, %r12
	addq	%rdi, %rsi
	cmpq	%rcx, %rax
	je	.L1172
.L1292:
	movzwl	(%rsi), %eax
	movw	%ax, (%r12)
	leaq	2(%r12), %rax
	cmpq	%rax, %rdx
	jbe	.L1172
	movzwl	2(%rsi), %eax
	movw	%ax, 2(%r12)
	leaq	4(%r12), %rax
	cmpq	%rax, %rdx
	jbe	.L1172
	movzwl	4(%rsi), %eax
	movw	%ax, 4(%r12)
	leaq	6(%r12), %rax
	cmpq	%rax, %rdx
	jbe	.L1172
	movzwl	6(%rsi), %eax
	movw	%ax, 6(%r12)
	leaq	8(%r12), %rax
	cmpq	%rax, %rdx
	jbe	.L1172
	movzwl	8(%rsi), %eax
	movw	%ax, 8(%r12)
	leaq	10(%r12), %rax
	cmpq	%rax, %rdx
	jbe	.L1172
	movzwl	10(%rsi), %eax
	movw	%ax, 10(%r12)
	leaq	12(%r12), %rax
	cmpq	%rax, %rdx
	jbe	.L1172
	movzwl	12(%rsi), %eax
	movw	%ax, 12(%r12)
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1253:
	movq	%r12, %rdi
	movsw
	movq	%rdi, %r12
	cmpq	%rdi, %rdx
	jbe	.L1172
	movq	%r12, %rdi
	movsw
	movq	%rdi, %r12
	cmpq	%rdi, %rdx
	ja	.L1253
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1181:
	movq	(%r8), %rdi
	subl	%r13d, %ebx
	movslq	%r13d, %r13
	movslq	%ebx, %rbx
	addq	%r13, %r13
	movq	(%rdi), %rax
	call	*48(%rax)
	leaq	(%rbx,%rbx), %rdx
	leaq	(%rax,%r13), %rsi
	cmpq	$3, %rbx
	ja	.L1285
	addq	%r12, %rdx
	cmpq	%rdx, %r12
	jnb	.L1172
	leaq	16(%rax,%r13), %rax
	cmpq	%rax, %r12
	leaq	16(%r12), %rax
	setnb	%cl
	cmpq	%rax, %rsi
	setnb	%al
	orb	%al, %cl
	je	.L1251
	movq	%r12, %rcx
	notq	%rcx
	addq	%rdx, %rcx
	cmpq	$13, %rcx
	jbe	.L1251
	shrq	%rcx
	xorl	%eax, %eax
	addq	$1, %rcx
	movq	%rcx, %rdi
	shrq	$3, %rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L1193:
	movdqu	(%rsi,%rax), %xmm3
	movups	%xmm3, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L1193
	movq	%rcx, %rax
	andq	$-8, %rax
	leaq	(%rax,%rax), %rdi
	addq	%rdi, %r12
	addq	%rdi, %rsi
	cmpq	%rax, %rcx
	jne	.L1292
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1251:
	movq	%r12, %rdi
	movsw
	movq	%rdi, %r12
	cmpq	%rdi, %rdx
	jbe	.L1172
	movq	%r12, %rdi
	movsw
	movq	%rdi, %r12
	cmpq	%rdi, %rdx
	ja	.L1251
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1285:
	movq	%r12, %rdi
	call	memcpy@PLT
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1207:
	cmpl	%r15d, %ebx
	jle	.L1230
	movq	23(%rdi), %rdi
	cmpl	$1, %r11d
	je	.L1297
	movq	-1(%rdi), %rdx
	movslq	%r15d, %rax
	movslq	%r13d, %rcx
	subq	%rcx, %rax
	cmpw	$63, 11(%rdx)
	leaq	(%r12,%rax,2), %rsi
	ja	.L1221
	movq	-1(%rdi), %rdx
	testb	$7, 11(%rdx)
	je	.L1298
.L1221:
	movl	%r11d, %ecx
	xorl	%edx, %edx
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii.localalias
.L1269:
	movq	-88(%rbp), %r9
	movl	%r15d, %ebx
	pxor	%xmm0, %xmm0
	movq	%r9, %rdi
	leaq	15(%r9), %r8
	jmp	.L1173
	.p2align 4,,10
	.p2align 3
.L1230:
	movq	%r9, %rdi
	leaq	15(%r9), %r8
	jmp	.L1173
	.p2align 4,,10
	.p2align 3
.L1296:
	movq	%r9, %rdi
	movl	%r15d, %ecx
	movl	%r13d, %edx
	movq	%r12, %rsi
	movl	%eax, -92(%rbp)
	movl	%r11d, -88(%rbp)
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii.localalias
	testl	%r13d, %r13d
	movq	(%rbx), %rdi
	movl	-88(%rbp), %r11d
	movslq	-92(%rbp), %rax
	pxor	%xmm0, %xmm0
	jne	.L1210
	movq	-104(%rbp), %r9
	cmpq	%rdi, %r9
	je	.L1299
.L1210:
	leaq	(%r12,%rax,2), %r12
	xorl	%r13d, %r13d
	jmp	.L1213
.L1297:
	cltq
	movq	%r9, -88(%rbp)
	leaq	-72(%rbp), %rdx
	leaq	-64(%rbp), %rsi
	movl	$0, -72(%rbp)
	leaq	(%r12,%rax,2), %rbx
	movq	-1(%rdi), %rax
	movzwl	11(%rax), %eax
	movq	%rdi, -64(%rbp)
	leaq	-68(%rbp), %rdi
	movl	%eax, -68(%rbp)
	call	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
	movw	%ax, (%rbx)
	jmp	.L1269
.L1298:
	movq	-1(%rdi), %rdx
	testb	$8, 11(%rdx)
	je	.L1221
	movslq	%r11d, %r11
	leaq	15(%rdi), %rdx
	addq	%r11, %r11
	movq	%rdx, %rcx
	leaq	(%rsi,%r11), %r8
	cmpq	%rsi, %r8
	jbe	.L1231
	subq	$1, %r11
	movq	%r11, %rbx
	shrq	%rbx
	leaq	1(%rax,%rbx), %rax
	leaq	1(%rbx), %r10
	leaq	(%r12,%rax,2), %rax
	cmpq	%rax, %rdx
	leaq	(%rdx,%r10), %rax
	setnb	%bl
	cmpq	%rax, %rsi
	setnb	%al
	orb	%al, %bl
	je	.L1222
	cmpq	$29, %r11
	jbe	.L1222
	movq	%r10, %r11
	negq	%rdi
	andq	$-16, %r11
	leaq	(%rsi,%rdi,2), %rax
	addq	%rdx, %r11
.L1224:
	movdqu	(%rdx), %xmm1
	movdqa	%xmm1, %xmm2
	punpckhbw	%xmm0, %xmm1
	punpcklbw	%xmm0, %xmm2
	movups	%xmm1, -14(%rax,%rdx,2)
	movups	%xmm2, -30(%rax,%rdx,2)
	addq	$16, %rdx
	cmpq	%r11, %rdx
	jne	.L1224
	movq	%r10, %rdi
	andq	$-16, %rdi
	leaq	(%rsi,%rdi,2), %rax
	leaq	(%rcx,%rdi), %rdx
	cmpq	%rdi, %r10
	je	.L1231
	movzbl	(%rdx), %ecx
	movw	%cx, (%rax)
	leaq	2(%rax), %rcx
	cmpq	%rcx, %r8
	jbe	.L1231
	movzbl	1(%rdx), %ecx
	movw	%cx, 2(%rax)
	leaq	4(%rax), %rcx
	cmpq	%rcx, %r8
	jbe	.L1231
	movzbl	2(%rdx), %ecx
	movw	%cx, 4(%rax)
	leaq	6(%rax), %rcx
	cmpq	%rcx, %r8
	jbe	.L1231
	movzbl	3(%rdx), %ecx
	movw	%cx, 6(%rax)
	leaq	8(%rax), %rcx
	cmpq	%rcx, %r8
	jbe	.L1231
	movzbl	4(%rdx), %ecx
	movw	%cx, 8(%rax)
	leaq	10(%rax), %rcx
	cmpq	%rcx, %r8
	jbe	.L1231
	movzbl	5(%rdx), %ecx
	movw	%cx, 10(%rax)
	leaq	12(%rax), %rcx
	cmpq	%rcx, %r8
	jbe	.L1231
	movzbl	6(%rdx), %ecx
	movw	%cx, 12(%rax)
	leaq	14(%rax), %rcx
	cmpq	%rcx, %r8
	jbe	.L1231
	movzbl	7(%rdx), %ecx
	movw	%cx, 14(%rax)
	leaq	16(%rax), %rcx
	cmpq	%rcx, %r8
	jbe	.L1231
	movzbl	8(%rdx), %ecx
	movw	%cx, 16(%rax)
	leaq	18(%rax), %rcx
	cmpq	%rcx, %r8
	jbe	.L1231
	movzbl	9(%rdx), %ecx
	movw	%cx, 18(%rax)
	leaq	20(%rax), %rcx
	cmpq	%rcx, %r8
	jbe	.L1231
	movzbl	10(%rdx), %ecx
	movw	%cx, 20(%rax)
	leaq	22(%rax), %rcx
	cmpq	%rcx, %r8
	jbe	.L1231
	movzbl	11(%rdx), %ecx
	movw	%cx, 22(%rax)
	leaq	24(%rax), %rcx
	cmpq	%rcx, %r8
	jbe	.L1231
	movzbl	12(%rdx), %ecx
	movw	%cx, 24(%rax)
	leaq	26(%rax), %rcx
	cmpq	%rcx, %r8
	jbe	.L1231
	movzbl	13(%rdx), %ecx
	movw	%cx, 26(%rax)
	leaq	28(%rax), %rcx
	cmpq	%rcx, %r8
	jbe	.L1231
	movzbl	14(%rdx), %edx
	movw	%dx, 28(%rax)
.L1231:
	movq	%r9, %rdi
	movl	%r15d, %ebx
	leaq	15(%r9), %r8
	jmp	.L1173
.L1222:
	shrq	%r11
	leaq	16(%rdi,%r11), %rdx
	imulq	$-2, %rdi, %rdi
	addq	%rdi, %rsi
.L1227:
	movq	%rcx, %rax
	addq	$1, %rcx
	movzbl	(%rax), %eax
	movw	%ax, -32(%rsi,%rcx,2)
	cmpq	%rdx, %rcx
	jne	.L1227
	jmp	.L1231
.L1299:
	leaq	(%r15,%r15), %rdx
	leaq	(%r12,%rdx), %rdi
	cmpq	$3, %r15
	ja	.L1300
	leaq	(%rdi,%rdx), %rcx
	cmpq	%rcx, %rdi
	jnb	.L1172
	leaq	16(%r12), %rsi
	leaq	-1(%rdx), %rax
	cmpq	%rsi, %rdi
	setnb	%sil
	addq	$16, %rdx
	testq	%rdx, %rdx
	setle	%dl
	orb	%dl, %sil
	je	.L1229
	cmpq	$13, %rax
	jbe	.L1229
	shrq	%rax
	leaq	1(%rax), %rdx
	xorl	%eax, %eax
	movq	%rdx, %rsi
	shrq	$3, %rsi
	salq	$4, %rsi
.L1217:
	movdqu	(%r12,%rax), %xmm5
	movups	%xmm5, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L1217
	movq	%rdx, %rsi
	andq	$-8, %rsi
	leaq	(%rsi,%rsi), %r8
	leaq	(%rdi,%r8), %rax
	addq	%r8, %r12
	cmpq	%rdx, %rsi
	je	.L1172
	movzwl	(%r12), %edx
	movw	%dx, (%rax)
	leaq	2(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L1172
	movzwl	2(%r12), %edx
	movw	%dx, 2(%rax)
	leaq	4(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L1172
	movzwl	4(%r12), %edx
	movw	%dx, 4(%rax)
	leaq	6(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L1172
	movzwl	6(%r12), %edx
	movw	%dx, 6(%rax)
	leaq	8(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L1172
	movzwl	8(%r12), %edx
	movw	%dx, 8(%rax)
	leaq	10(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L1172
	movzwl	10(%r12), %edx
	movw	%dx, 10(%rax)
	leaq	12(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L1172
	movzwl	12(%r12), %edx
	movw	%dx, 12(%rax)
	jmp	.L1172
.L1293:
	call	__stack_chk_fail@PLT
.L1300:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1293
	addq	$72, %rsp
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	memcpy@PLT
.L1229:
	.cfi_restore_state
	xorl	%eax, %eax
.L1215:
	movzwl	(%r12,%rax), %edx
	movw	%dx, (%rdi,%rax)
	addq	$2, %rax
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %rcx
	ja	.L1215
	jmp	.L1172
	.cfi_endproc
.LFE22001:
	.size	_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii, .-_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii
	.set	_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii.localalias,_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii
	.section	.text._ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii,"axG",@progbits,_ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii,comdat
	.p2align 4
	.weak	_ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii
	.type	_ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii, @function
_ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii:
.LFB21844:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	15(%rdi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-72(%rbp), %r15
	leaq	.L1305(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.L1302:
	movq	-1(%rdi), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L1302
	movzwl	%ax, %eax
	movslq	(%r14,%rax,4), %rax
	addq	%r14, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii,"aG",@progbits,_ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii,comdat
	.align 4
	.align 4
.L1305:
	.long	.L1311-.L1305
	.long	.L1308-.L1305
	.long	.L1310-.L1305
	.long	.L1306-.L1305
	.long	.L1302-.L1305
	.long	.L1304-.L1305
	.long	.L1302-.L1305
	.long	.L1302-.L1305
	.long	.L1309-.L1305
	.long	.L1308-.L1305
	.long	.L1307-.L1305
	.long	.L1306-.L1305
	.long	.L1302-.L1305
	.long	.L1304-.L1305
	.section	.text._ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii,"axG",@progbits,_ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii,comdat
	.p2align 4,,10
	.p2align 3
.L1304:
	movq	(%r8), %rdi
	leaq	15(%rdi), %r8
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1308:
	movq	(%r8), %r9
	movl	%ebx, %r11d
	movl	11(%r9), %r8d
	movl	%r8d, %r10d
	subl	%r8d, %r11d
	subl	%r13d, %r10d
	cmpl	%r10d, %r11d
	jl	.L1336
	leaq	23(%rdi), %rbx
	cmpl	%r8d, %r13d
	jl	.L1426
	movq	23(%rdi), %rdi
	subl	%r8d, %r13d
.L1342:
	movl	%r11d, %ebx
	leaq	15(%rdi), %r8
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1306:
	movslq	27(%rdi), %rdx
	movq	(%r8), %rdi
	movq	%r12, %rsi
	leal	(%rbx,%rdx), %ecx
	addl	%r13d, %edx
	call	_ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii
	.p2align 4,,10
	.p2align 3
.L1301:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1423
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1309:
	.cfi_restore_state
	subl	%r13d, %ebx
	movslq	%r13d, %r13
	movslq	%ebx, %rdx
	leaq	(%r8,%r13), %rsi
	cmpq	$7, %rdx
	ja	.L1427
	leaq	(%r12,%rdx), %rcx
	cmpq	%rcx, %r12
	jnb	.L1301
	leaq	16(%r8,%r13), %rax
	cmpq	%rax, %r12
	leaq	16(%r12), %rax
	setnb	%dil
	cmpq	%rax, %rsi
	setnb	%al
	orb	%al, %dil
	je	.L1359
	movq	%r12, %rax
	notq	%rax
	addq	%rcx, %rax
	cmpq	$14, %rax
	jbe	.L1359
	movq	%rdx, %rdi
	xorl	%eax, %eax
	andq	$-16, %rdi
	.p2align 4,,10
	.p2align 3
.L1328:
	movdqu	(%rsi,%rax), %xmm4
	movups	%xmm4, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L1328
	movq	%rdx, %rax
	andq	$-16, %rax
	addq	%rax, %r12
	addq	%rax, %rsi
	cmpq	%rax, %rdx
	je	.L1301
	movzbl	(%rsi), %eax
	movb	%al, (%r12)
	leaq	1(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1301
	movzbl	1(%rsi), %eax
	movb	%al, 1(%r12)
	leaq	2(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1301
	movzbl	2(%rsi), %eax
	movb	%al, 2(%r12)
	leaq	3(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1301
	movzbl	3(%rsi), %eax
	movb	%al, 3(%r12)
	leaq	4(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1301
	movzbl	4(%rsi), %eax
	movb	%al, 4(%r12)
	leaq	5(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1301
	movzbl	5(%rsi), %eax
	movb	%al, 5(%r12)
	leaq	6(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1301
	movzbl	6(%rsi), %eax
	movb	%al, 6(%r12)
	leaq	7(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1301
	movzbl	7(%rsi), %eax
	movb	%al, 7(%r12)
	leaq	8(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1301
	movzbl	8(%rsi), %eax
	movb	%al, 8(%r12)
	leaq	9(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1301
	movzbl	9(%rsi), %eax
	movb	%al, 9(%r12)
	leaq	10(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1301
	movzbl	10(%rsi), %eax
	movb	%al, 10(%r12)
	leaq	11(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1301
	movzbl	11(%rsi), %eax
	movb	%al, 11(%r12)
	leaq	12(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1301
	movzbl	12(%rsi), %eax
	movb	%al, 12(%r12)
	leaq	13(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1301
	movzbl	13(%rsi), %eax
	movb	%al, 13(%r12)
	leaq	14(%r12), %rax
	cmpq	%rax, %rcx
	ja	.L1407
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1307:
	movq	(%r8), %rdi
	subl	%r13d, %ebx
	movslq	%r13d, %r13
	movslq	%ebx, %r14
	movq	(%rdi), %rax
	call	*48(%rax)
	leaq	(%rax,%r13), %rsi
	cmpq	$7, %r14
	ja	.L1428
	leaq	(%r12,%r14), %rdi
	cmpq	%rdi, %r12
	jnb	.L1301
	leaq	16(%rax,%r13), %rax
	cmpq	%rax, %r12
	leaq	16(%r12), %rax
	setnb	%dl
	cmpq	%rax, %rsi
	setnb	%al
	orb	%al, %dl
	je	.L1357
	movq	%r12, %rax
	notq	%rax
	addq	%rdi, %rax
	cmpq	$14, %rax
	jbe	.L1357
	movq	%r14, %rdx
	xorl	%eax, %eax
	andq	$-16, %rdx
	.p2align 4,,10
	.p2align 3
.L1317:
	movdqu	(%rsi,%rax), %xmm3
	movups	%xmm3, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L1317
	movq	%r14, %rax
	andq	$-16, %rax
	addq	%rax, %rsi
	addq	%rax, %r12
	cmpq	%rax, %r14
	je	.L1301
	movzbl	(%rsi), %eax
	movb	%al, (%r12)
	leaq	1(%r12), %rax
	cmpq	%rax, %rdi
	jbe	.L1301
	movzbl	1(%rsi), %eax
	movb	%al, 1(%r12)
	leaq	2(%r12), %rax
	cmpq	%rax, %rdi
	jbe	.L1301
	movzbl	2(%rsi), %eax
	movb	%al, 2(%r12)
	leaq	3(%r12), %rax
	cmpq	%rax, %rdi
	jbe	.L1301
	movzbl	3(%rsi), %eax
	movb	%al, 3(%r12)
	leaq	4(%r12), %rax
	cmpq	%rax, %rdi
	jbe	.L1301
	movzbl	4(%rsi), %eax
	movb	%al, 4(%r12)
	leaq	5(%r12), %rax
	cmpq	%rax, %rdi
	jbe	.L1301
	movzbl	5(%rsi), %eax
	movb	%al, 5(%r12)
	leaq	6(%r12), %rax
	cmpq	%rax, %rdi
	jbe	.L1301
	movzbl	6(%rsi), %eax
	movb	%al, 6(%r12)
	leaq	7(%r12), %rax
	cmpq	%rax, %rdi
	jbe	.L1301
	movzbl	7(%rsi), %eax
	movb	%al, 7(%r12)
	leaq	8(%r12), %rax
	cmpq	%rax, %rdi
	jbe	.L1301
	movzbl	8(%rsi), %eax
	movb	%al, 8(%r12)
	leaq	9(%r12), %rax
	cmpq	%rax, %rdi
	jbe	.L1301
	movzbl	9(%rsi), %eax
	movb	%al, 9(%r12)
	leaq	10(%r12), %rax
	cmpq	%rax, %rdi
	jbe	.L1301
	movzbl	10(%rsi), %eax
	movb	%al, 10(%r12)
	leaq	11(%r12), %rax
	cmpq	%rax, %rdi
	jbe	.L1301
	movzbl	11(%rsi), %eax
	movb	%al, 11(%r12)
	leaq	12(%r12), %rax
	cmpq	%rax, %rdi
	jbe	.L1301
	movzbl	12(%rsi), %eax
	movb	%al, 12(%r12)
	leaq	13(%r12), %rax
	cmpq	%rax, %rdi
	jbe	.L1301
	movzbl	13(%rsi), %eax
	movb	%al, 13(%r12)
	leaq	14(%r12), %rax
	cmpq	%rax, %rdi
	jbe	.L1301
.L1407:
	movzbl	14(%rsi), %eax
	movb	%al, 14(%r12)
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1311:
	subl	%r13d, %ebx
	movslq	%r13d, %r13
	movslq	%ebx, %rbx
	leaq	(%r8,%r13,2), %rsi
	leaq	(%r12,%rbx), %rdi
	cmpq	%rdi, %r12
	jnb	.L1301
	subq	%r12, %r13
	addq	%rdi, %r13
	leaq	(%r8,%r13,2), %rax
	cmpq	%rax, %r12
	setnb	%dl
	cmpq	%rdi, %rsi
	setnb	%al
	orb	%al, %dl
	je	.L1360
	movq	%r12, %rax
	notq	%rax
	addq	%rdi, %rax
	cmpq	$14, %rax
	jbe	.L1360
	movq	%rbx, %rdx
	movdqa	.LC12(%rip), %xmm1
	xorl	%eax, %eax
	andq	$-16, %rdx
	.p2align 4,,10
	.p2align 3
.L1333:
	movdqu	(%rsi,%rax,2), %xmm0
	movdqu	16(%rsi,%rax,2), %xmm2
	pand	%xmm1, %xmm0
	pand	%xmm1, %xmm2
	packuswb	%xmm2, %xmm0
	movups	%xmm0, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L1333
.L1425:
	movq	%rbx, %rdx
	andq	$-16, %rdx
	addq	%rdx, %r12
	leaq	(%rsi,%rdx,2), %rax
	cmpq	%rdx, %rbx
	je	.L1301
	movzwl	(%rax), %edx
	movb	%dl, (%r12)
	leaq	1(%r12), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1301
	movzwl	2(%rax), %edx
	movb	%dl, 1(%r12)
	leaq	2(%r12), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1301
	movzwl	4(%rax), %edx
	movb	%dl, 2(%r12)
	leaq	3(%r12), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1301
	movzwl	6(%rax), %edx
	movb	%dl, 3(%r12)
	leaq	4(%r12), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1301
	movzwl	8(%rax), %edx
	movb	%dl, 4(%r12)
	leaq	5(%r12), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1301
	movzwl	10(%rax), %edx
	movb	%dl, 5(%r12)
	leaq	6(%r12), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1301
	movzwl	12(%rax), %edx
	movb	%dl, 6(%r12)
	leaq	7(%r12), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1301
	movzwl	14(%rax), %edx
	movb	%dl, 7(%r12)
	leaq	8(%r12), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1301
	movzwl	16(%rax), %edx
	movb	%dl, 8(%r12)
	leaq	9(%r12), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1301
	movzwl	18(%rax), %edx
	movb	%dl, 9(%r12)
	leaq	10(%r12), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1301
	movzwl	20(%rax), %edx
	movb	%dl, 10(%r12)
	leaq	11(%r12), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1301
	movzwl	22(%rax), %edx
	movb	%dl, 11(%r12)
	leaq	12(%r12), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1301
	movzwl	24(%rax), %edx
	movb	%dl, 12(%r12)
	leaq	13(%r12), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1301
	movzwl	26(%rax), %edx
	movb	%dl, 13(%r12)
	leaq	14(%r12), %rdx
	cmpq	%rdx, %rdi
	jbe	.L1301
	movzwl	28(%rax), %eax
	movb	%al, 14(%r12)
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1310:
	movq	(%r8), %rdi
	subl	%r13d, %ebx
	movslq	%r13d, %r13
	movslq	%ebx, %rbx
	movq	(%rdi), %rax
	call	*48(%rax)
	leaq	(%r12,%rbx), %rdi
	leaq	(%rax,%r13,2), %rsi
	cmpq	%rdi, %r12
	jnb	.L1301
	subq	%r12, %r13
	addq	%rdi, %r13
	leaq	(%rax,%r13,2), %rax
	cmpq	%rax, %r12
	setnb	%dl
	cmpq	%rdi, %rsi
	setnb	%al
	orb	%al, %dl
	je	.L1358
	movq	%r12, %rax
	notq	%rax
	addq	%rdi, %rax
	cmpq	$14, %rax
	jbe	.L1358
	movq	%rbx, %rdx
	movdqa	.LC12(%rip), %xmm1
	xorl	%eax, %eax
	andq	$-16, %rdx
	.p2align 4,,10
	.p2align 3
.L1322:
	movdqu	(%rsi,%rax,2), %xmm0
	movdqu	16(%rsi,%rax,2), %xmm2
	pand	%xmm1, %xmm0
	pand	%xmm1, %xmm2
	packuswb	%xmm2, %xmm0
	movups	%xmm0, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L1322
	jmp	.L1425
	.p2align 4,,10
	.p2align 3
.L1336:
	cmpl	%r8d, %ebx
	jle	.L1362
	movq	23(%rdi), %rdi
	cmpl	$1, %r11d
	je	.L1429
	movq	-1(%rdi), %rdx
	movslq	%r8d, %rax
	movslq	%r13d, %rcx
	subq	%rcx, %rax
	cmpw	$63, 11(%rdx)
	leaq	(%r12,%rax), %r10
	ja	.L1350
	movq	-1(%rdi), %rdx
	testb	$7, 11(%rdx)
	je	.L1430
.L1350:
	movl	%r11d, %ecx
	xorl	%edx, %edx
	movq	%r10, %rsi
	movq	%r9, -96(%rbp)
	movl	%r8d, -84(%rbp)
	call	_ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii
.L1406:
	movq	-96(%rbp), %r9
	movl	-84(%rbp), %r8d
	movq	%r9, %rdi
	movl	%r8d, %ebx
	leaq	15(%r9), %r8
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1428:
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	memcpy@PLT
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1427:
	movq	%r12, %rdi
	call	memcpy@PLT
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1362:
	movq	%r9, %rdi
	leaq	15(%r9), %r8
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1358:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1320:
	movzwl	(%rsi,%rax,2), %edx
	movb	%dl, (%r12,%rax)
	addq	$1, %rax
	cmpq	%rax, %rbx
	jne	.L1320
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1360:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1331:
	movzwl	(%rsi,%rax,2), %edx
	movb	%dl, (%r12,%rax)
	addq	$1, %rax
	cmpq	%rax, %rbx
	jne	.L1331
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1426:
	movq	%r9, %rdi
	movl	%r8d, %ecx
	movl	%r13d, %edx
	movq	%r12, %rsi
	movl	%r10d, -96(%rbp)
	movl	%r11d, -84(%rbp)
	movl	%r8d, -88(%rbp)
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii
	testl	%r13d, %r13d
	movq	(%rbx), %rdi
	movl	-84(%rbp), %r11d
	movslq	-96(%rbp), %r10
	jne	.L1339
	movq	-104(%rbp), %r9
	movl	-88(%rbp), %r8d
	cmpq	%rdi, %r9
	je	.L1431
.L1339:
	addq	%r10, %r12
	xorl	%r13d, %r13d
	jmp	.L1342
	.p2align 4,,10
	.p2align 3
.L1357:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1315:
	movzbl	(%rsi,%rax), %edx
	movb	%dl, (%r12,%rax)
	addq	$1, %rax
	cmpq	%rax, %r14
	jne	.L1315
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1359:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1326:
	movzbl	(%rsi,%rax), %ecx
	movb	%cl, (%r12,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L1326
	jmp	.L1301
.L1429:
	movq	%r9, -104(%rbp)
	leaq	-64(%rbp), %rsi
	movq	%r15, %rdx
	movl	%r8d, -96(%rbp)
	movl	%r10d, -84(%rbp)
	movl	$0, -72(%rbp)
	movq	-1(%rdi), %rax
	movzwl	11(%rax), %eax
	movq	%rdi, -64(%rbp)
	leaq	-68(%rbp), %rdi
	movl	%eax, -68(%rbp)
	call	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
	movq	-104(%rbp), %r9
	movl	-96(%rbp), %r8d
	movslq	-84(%rbp), %r10
	movl	%r8d, %ebx
	movq	%r9, %rdi
	leaq	15(%r9), %r8
	movb	%al, (%r12,%r10)
	jmp	.L1302
.L1430:
	movq	-1(%rdi), %rdx
	testb	$8, 11(%rdx)
	je	.L1350
	leaq	15(%rdi), %rsi
	movslq	%r11d, %r11
	movq	%rsi, %rcx
	cmpq	$7, %r11
	ja	.L1432
	leaq	(%r10,%r11), %rdx
	cmpq	%r10, %rdx
	jbe	.L1363
	leaq	16(%r12,%rax), %rax
	cmpq	%rax, %rsi
	leaq	31(%rdi), %rax
	setnb	%bl
	cmpq	%rax, %r10
	setnb	%al
	orb	%al, %bl
	je	.L1352
	subq	$1, %r11
	cmpq	$14, %r11
	jbe	.L1352
	movq	%rsi, %rax
	subq	%rdi, %r10
.L1353:
	movdqu	(%rax), %xmm5
	movups	%xmm5, -15(%r10,%rax)
	addq	$16, %rax
	cmpq	%rax, %rsi
	jne	.L1353
.L1363:
	movl	%r8d, %ebx
	movq	%r9, %rdi
	leaq	15(%r9), %r8
	jmp	.L1302
.L1432:
	movq	%r11, %rdx
	movq	%r10, %rdi
	movq	%r9, -96(%rbp)
	movl	%r8d, -84(%rbp)
	call	memcpy@PLT
	jmp	.L1406
.L1431:
	movslq	%r8d, %rdx
	leaq	(%r12,%rdx), %rdi
	cmpq	$7, %rdx
	ja	.L1433
	leaq	(%rdi,%rdx), %rcx
	cmpq	%rcx, %rdi
	jnb	.L1301
	leaq	-1(%rdx), %rax
	cmpq	$14, %rax
	jbe	.L1361
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	jb	.L1361
	movq	%rdx, %rsi
	xorl	%eax, %eax
	andq	$-16, %rsi
.L1346:
	movdqu	(%r12,%rax), %xmm6
	movups	%xmm6, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L1346
	movq	%rdx, %rsi
	andq	$-16, %rsi
	leaq	(%rdi,%rsi), %rax
	addq	%rsi, %r12
	cmpq	%rsi, %rdx
	je	.L1301
	movzbl	(%r12), %edx
	movb	%dl, (%rax)
	leaq	1(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L1301
	movzbl	1(%r12), %edx
	movb	%dl, 1(%rax)
	leaq	2(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L1301
	movzbl	2(%r12), %edx
	movb	%dl, 2(%rax)
	leaq	3(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L1301
	movzbl	3(%r12), %edx
	movb	%dl, 3(%rax)
	leaq	4(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L1301
	movzbl	4(%r12), %edx
	movb	%dl, 4(%rax)
	leaq	5(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L1301
	movzbl	5(%r12), %edx
	movb	%dl, 5(%rax)
	leaq	6(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L1301
	movzbl	6(%r12), %edx
	movb	%dl, 6(%rax)
	leaq	7(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L1301
	movzbl	7(%r12), %edx
	movb	%dl, 7(%rax)
	leaq	8(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L1301
	movzbl	8(%r12), %edx
	movb	%dl, 8(%rax)
	leaq	9(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L1301
	movzbl	9(%r12), %edx
	movb	%dl, 9(%rax)
	leaq	10(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L1301
	movzbl	10(%r12), %edx
	movb	%dl, 10(%rax)
	leaq	11(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L1301
	movzbl	11(%r12), %edx
	movb	%dl, 11(%rax)
	leaq	12(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L1301
	movzbl	12(%r12), %edx
	movb	%dl, 12(%rax)
	leaq	13(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L1301
	movzbl	13(%r12), %edx
	movb	%dl, 13(%rax)
	leaq	14(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L1301
	movzbl	14(%r12), %edx
	movb	%dl, 14(%rax)
	jmp	.L1301
.L1352:
	leaq	(%rdx,%rsi), %rax
	subq	%r10, %rax
	subq	%rdi, %r10
.L1355:
	movq	%rcx, %rdx
	addq	$1, %rcx
	movzbl	(%rdx), %edx
	movb	%dl, -16(%rcx,%r10)
	cmpq	%rax, %rcx
	jne	.L1355
	jmp	.L1363
.L1423:
	call	__stack_chk_fail@PLT
.L1433:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1423
	addq	$72, %rsp
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	memcpy@PLT
.L1361:
	.cfi_restore_state
	xorl	%eax, %eax
.L1344:
	movzbl	(%r12,%rax), %ecx
	movb	%cl, (%rdi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L1344
	jmp	.L1301
	.cfi_endproc
.LFE21844:
	.size	_ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii, .-_ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii
	.section	.text._ZN2v88internal6String17ComputeAndSetHashEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String17ComputeAndSetHashEv
	.type	_ZN2v88internal6String17ComputeAndSetHashEv, @function
_ZN2v88internal6String17ComputeAndSetHashEv:
.LFB19712:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r13
	movq	%r13, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36416(%rax), %rax
	movq	15(%rax), %r14
	movq	-1(%r13), %rdx
	leaq	-1(%r13), %rax
	cmpw	$63, 11(%rdx)
	jbe	.L1493
.L1435:
	movq	(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L1494
.L1436:
	movq	(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L1487
.L1490:
	movq	(%r12), %rdx
.L1438:
	movq	(%rax), %rcx
	movslq	11(%rdx), %rbx
	testb	$8, 11(%rcx)
	jne	.L1495
	cmpl	$16383, %ebx
	jle	.L1457
.L1492:
	leal	2(,%rbx,4), %ebx
.L1456:
	movl	%ebx, 7(%rdx)
	movl	%ebx, %eax
	shrl	$2, %eax
.L1434:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1495:
	.cfi_restore_state
	cmpl	$16383, %ebx
	jg	.L1492
	movq	(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1444
	movq	(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L1496
.L1444:
	movq	(%rax), %rdx
	leaq	15(%r13), %rax
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$2, %dx
	jne	.L1446
	movq	15(%r13), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
.L1446:
	addq	%rax, %r15
	xorl	%r13d, %r13d
.L1445:
	leal	-1(%rbx), %eax
	cmpl	$9, %eax
	ja	.L1447
	movzbl	(%r15), %edi
	leal	-48(%rdi), %edx
	cmpl	$9, %edx
	ja	.L1447
	cmpl	$1, %ebx
	je	.L1475
	cmpb	$48, %dil
	je	.L1447
.L1475:
	subl	$48, %edi
	cmpl	$1, %ebx
	je	.L1467
	leal	-2(%rbx), %eax
	leaq	1(%r15), %rdx
	movl	$429496729, %esi
	leaq	2(%r15,%rax), %r8
.L1452:
	movzbl	(%rdx), %eax
	leal	-48(%rax), %ecx
	cmpl	$9, %ecx
	ja	.L1447
	subl	$45, %eax
	movl	%esi, %r9d
	sarl	$3, %eax
	subl	%eax, %r9d
	cmpl	%r9d, %edi
	ja	.L1447
	leal	(%rdi,%rdi,4), %eax
	addq	$1, %rdx
	leal	(%rcx,%rax,2), %edi
	cmpq	%rdx, %r8
	jne	.L1452
.L1467:
	movl	%ebx, %esi
	call	_ZN2v88internal12StringHasher18MakeArrayIndexHashEji@PLT
	movl	%eax, %ebx
	jmp	.L1466
	.p2align 4,,10
	.p2align 3
.L1487:
	movq	(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$5, %dx
	jne	.L1490
	movq	(%r12), %rdx
	movq	15(%r13), %r13
	movl	11(%rdx), %eax
	cmpl	%eax, 11(%r13)
	je	.L1439
	leaq	-1(%r13), %rax
	jmp	.L1438
	.p2align 4,,10
	.p2align 3
.L1494:
	movq	(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L1436
	movq	(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L1437
	movq	23(%r13), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	jne	.L1436
	.p2align 4,,10
	.p2align 3
.L1437:
	movq	15(%r13), %r13
	leaq	-1(%r13), %rax
	jmp	.L1436
	.p2align 4,,10
	.p2align 3
.L1493:
	movq	-1(%r13), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$3, %dx
	jne	.L1435
	movslq	27(%r13), %r15
	movq	15(%r13), %r13
	leaq	-1(%r13), %rax
	jmp	.L1435
	.p2align 4,,10
	.p2align 3
.L1439:
	movl	7(%r13), %eax
	movl	%eax, 7(%rdx)
	movq	(%r12), %rax
	movl	7(%rax), %eax
	shrl	$2, %eax
	jmp	.L1434
	.p2align 4,,10
	.p2align 3
.L1447:
	addq	%r15, %rbx
	movl	%r14d, %edx
	cmpq	%rbx, %r15
	je	.L1469
	.p2align 4,,10
	.p2align 3
.L1454:
	movzbl	(%r15), %eax
	addq	$1, %r15
	addl	%edx, %eax
	movl	%eax, %edx
	sall	$10, %edx
	addl	%eax, %edx
	movl	%edx, %eax
	shrl	$6, %eax
	xorl	%eax, %edx
	cmpq	%r15, %rbx
	jne	.L1454
.L1469:
	leal	(%rdx,%rdx,8), %ebx
	movl	%ebx, %eax
	shrl	$11, %eax
	xorl	%eax, %ebx
	movl	%ebx, %eax
	sall	$15, %eax
	addl	%ebx, %eax
	movl	%eax, %edx
	andl	$1073741823, %edx
	leal	-1(%rdx), %ebx
	sarl	$31, %ebx
	andl	$27, %ebx
	orl	%eax, %ebx
	leal	2(,%rbx,4), %ebx
.L1466:
	testq	%r13, %r13
	je	.L1491
	movq	%r13, %rdi
	call	_ZdaPv@PLT
.L1491:
	movq	(%r12), %rdx
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1457:
	movq	(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1459
	movq	(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L1497
.L1459:
	movq	(%rax), %rdx
	leaq	15(%r13), %rax
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$2, %dx
	jne	.L1462
	movq	15(%r13), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
.L1462:
	leaq	(%rax,%r15,2), %r15
	xorl	%r13d, %r13d
.L1461:
	leal	-1(%rbx), %eax
	cmpl	$9, %eax
	ja	.L1463
	movzwl	(%r15), %edi
	leal	-48(%rdi), %edx
	cmpl	$9, %edx
	ja	.L1463
	cmpl	$1, %ebx
	je	.L1476
	cmpw	$48, %di
	je	.L1463
.L1476:
	subl	$48, %edi
	cmpl	$1, %ebx
	je	.L1467
	leal	-2(%rbx), %eax
	leaq	2(%r15), %rdx
	movl	$429496729, %esi
	leaq	4(%r15,%rax,2), %r8
.L1468:
	movzwl	(%rdx), %eax
	leal	-48(%rax), %ecx
	cmpl	$9, %ecx
	ja	.L1463
	subl	$45, %eax
	movl	%esi, %r10d
	sarl	$3, %eax
	subl	%eax, %r10d
	cmpl	%r10d, %edi
	ja	.L1463
	leal	(%rdi,%rdi,4), %eax
	addq	$2, %rdx
	leal	(%rcx,%rax,2), %edi
	cmpq	%rdx, %r8
	jne	.L1468
	jmp	.L1467
	.p2align 4,,10
	.p2align 3
.L1463:
	leaq	(%r15,%rbx,2), %rsi
	movl	%r14d, %edx
	cmpq	%rsi, %r15
	je	.L1469
	.p2align 4,,10
	.p2align 3
.L1470:
	movzwl	(%r15), %ecx
	addq	$2, %r15
	addl	%edx, %ecx
	movl	%ecx, %edx
	sall	$10, %edx
	addl	%ecx, %edx
	movl	%edx, %eax
	shrl	$6, %eax
	xorl	%eax, %edx
	cmpq	%r15, %rsi
	jne	.L1470
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1497:
	movabsq	$4611686018427387900, %rdx
	movslq	%ebx, %rax
	cmpq	%rdx, %rax
	leaq	(%rax,%rax), %rdi
	movq	$-1, %rax
	cmova	%rax, %rdi
	call	_Znam@PLT
	movq	%r13, %rdi
	movl	%ebx, %ecx
	xorl	%edx, %edx
	movq	%rax, %r15
	movq	%rax, %rsi
	call	_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii
	movq	%r15, %r13
	jmp	.L1461
	.p2align 4,,10
	.p2align 3
.L1496:
	movslq	%ebx, %rdi
	call	_Znam@PLT
	movq	%r13, %rdi
	movl	%ebx, %ecx
	xorl	%edx, %edx
	movq	%rax, %r15
	movq	%rax, %rsi
	call	_ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii
	movq	%r15, %r13
	jmp	.L1445
	.cfi_endproc
.LFE19712:
	.size	_ZN2v88internal6String17ComputeAndSetHashEv, .-_ZN2v88internal6String17ComputeAndSetHashEv
	.section	.text._ZN2v88internal6String16SlowAsArrayIndexEPj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String16SlowAsArrayIndexEPj
	.type	_ZN2v88internal6String16SlowAsArrayIndexEPj, @function
_ZN2v88internal6String16SlowAsArrayIndexEPj:
.LFB19714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	11(%rdx), %eax
	cmpl	$7, %eax
	jg	.L1499
	movl	7(%rdx), %eax
	testb	$1, %al
	jne	.L1506
	testb	$2, %al
	je	.L1501
.L1503:
	xorl	%eax, %eax
.L1498:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1507
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1499:
	.cfi_restore_state
	cmpl	$10, %eax
	jg	.L1503
	call	_ZN2v88internal6String17ComputeArrayIndexEPj.part.0
	jmp	.L1498
	.p2align 4,,10
	.p2align 3
.L1501:
	shrl	$2, %eax
	andl	$16777215, %eax
	movl	%eax, 0(%r13)
	movl	$1, %eax
	jmp	.L1498
	.p2align 4,,10
	.p2align 3
.L1506:
	leaq	-32(%rbp), %rdi
	movq	%rdx, -32(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv
	movq	(%r12), %rax
	movl	7(%rax), %eax
	testb	$2, %al
	je	.L1501
	jmp	.L1503
.L1507:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19714:
	.size	_ZN2v88internal6String16SlowAsArrayIndexEPj, .-_ZN2v88internal6String16SlowAsArrayIndexEPj
	.section	.text._ZN2v88internal6String10SlowEqualsES1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String10SlowEqualsES1_
	.type	_ZN2v88internal6String10SlowEqualsES1_, @function
_ZN2v88internal6String10SlowEqualsES1_:
.LFB19690:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$648, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	11(%rdx), %ebx
	cmpl	%ebx, 11(%rsi)
	jne	.L1547
	movl	$1, %eax
	testl	%ebx, %ebx
	jne	.L1549
.L1508:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1550
	addq	$648, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1549:
	.cfi_restore_state
	movq	-1(%rdx), %rax
	movq	%rdi, %r13
	movq	%rsi, %r12
	cmpw	$63, 11(%rax)
	jbe	.L1511
.L1515:
	movq	-1(%r12), %rax
	cmpw	$63, 11(%rax)
	ja	.L1513
	movq	-1(%r12), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L1514
.L1513:
	movl	7(%rdx), %eax
	testb	$1, %al
	jne	.L1517
	movl	7(%r12), %ecx
	testb	$1, %cl
	je	.L1551
.L1517:
	leaq	-668(%rbp), %r15
	leaq	-664(%rbp), %rsi
	movl	$0, -672(%rbp)
	movq	-1(%rdx), %rax
	movq	%r15, %rdi
	leaq	-672(%rbp), %rdx
	movq	%rsi, -680(%rbp)
	movzwl	11(%rax), %eax
	movq	%rdx, -688(%rbp)
	movl	%eax, -668(%rbp)
	movq	0(%r13), %rax
	movq	%rax, -664(%rbp)
	call	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
	movq	%r15, %rdi
	movl	$0, -672(%rbp)
	movl	%eax, %r14d
	movq	-1(%r12), %rax
	movq	-688(%rbp), %rdx
	movq	-680(%rbp), %rsi
	movzwl	11(%rax), %eax
	movq	%r12, -664(%rbp)
	movl	%eax, -668(%rbp)
	call	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
	cmpw	%ax, %r14w
	je	.L1552
	.p2align 4,,10
	.p2align 3
.L1547:
	xorl	%eax, %eax
	jmp	.L1508
	.p2align 4,,10
	.p2align 3
.L1511:
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L1515
.L1514:
	movq	-1(%r12), %rax
	cmpw	$63, 11(%rax)
	ja	.L1518
	movq	-1(%r12), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L1553
.L1518:
	movq	0(%r13), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1519
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$5, %dx
	je	.L1554
.L1519:
	movq	0(%r13), %rdx
	movl	$1, %eax
	cmpq	%rdx, %rsi
	je	.L1508
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L1525
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	je	.L1547
.L1525:
	movq	%r13, %rdi
	call	_ZN2v88internal6String10SlowEqualsES1_
	jmp	.L1508
	.p2align 4,,10
	.p2align 3
.L1552:
	movq	0(%r13), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1527
	movq	-1(%rax), %rdx
	testb	$7, 11(%rdx)
	je	.L1555
.L1527:
	xorl	%eax, %eax
	leaq	-656(%rbp), %rdi
	movq	0(%r13), %rsi
	movq	%r12, %rdx
	movl	$32, %ecx
	movb	$1, -80(%rbp)
	rep stosq
	movl	$32, %ecx
	leaq	-360(%rbp), %rdi
	movq	$0, -400(%rbp)
	rep stosq
	leaq	-656(%rbp), %rdi
	movb	$1, -376(%rbp)
	movl	$0, -372(%rbp)
	movq	$0, -368(%rbp)
	movq	$0, -104(%rbp)
	movl	$0, -76(%rbp)
	movq	$0, -72(%rbp)
	call	_ZN2v88internal16StringComparator6EqualsENS0_6StringES2_@PLT
	jmp	.L1508
	.p2align 4,,10
	.p2align 3
.L1553:
	movq	15(%r12), %rsi
	jmp	.L1518
	.p2align 4,,10
	.p2align 3
.L1554:
	movq	15(%rax), %rdx
	movl	$1, %eax
	movq	%rdx, -664(%rbp)
	cmpq	%rdx, %rsi
	je	.L1508
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L1522
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	je	.L1547
.L1522:
	leaq	-664(%rbp), %rdi
	call	_ZN2v88internal6String10SlowEqualsES1_
	jmp	.L1508
	.p2align 4,,10
	.p2align 3
.L1551:
	shrl	$2, %ecx
	shrl	$2, %eax
	cmpl	%eax, %ecx
	je	.L1517
	xorl	%eax, %eax
	jmp	.L1508
.L1555:
	movq	-1(%rax), %rdx
	testb	$8, 11(%rdx)
	je	.L1527
	movq	-1(%r12), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1527
	movq	-1(%r12), %rdx
	testb	$7, 11(%rdx)
	jne	.L1527
	movq	-1(%r12), %rdx
	testb	$8, 11(%rdx)
	je	.L1527
	movslq	%ebx, %rdx
	leaq	15(%r12), %rsi
	leaq	15(%rax), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	sete	%al
	jmp	.L1508
.L1550:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19690:
	.size	_ZN2v88internal6String10SlowEqualsES1_, .-_ZN2v88internal6String10SlowEqualsES1_
	.section	.rodata._ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"(location_) != nullptr"
.LC14:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE
	.type	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE, @function
_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE:
.LFB19660:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rsi), %rsi
.L1604:
	movq	15(%rsi), %rax
	movl	11(%rax), %edx
	testl	%edx, %edx
	jne	.L1557
	movq	23(%rsi), %rax
	movq	-1(%rax), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L1605
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L1605
	movq	(%rbx), %rax
	movq	23(%rax), %r14
	movq	-1(%r14), %rax
	movq	%r14, %rsi
	movq	41112(%r12), %rdi
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	je	.L1561
.L1559:
	testq	%rdi, %rdi
	je	.L1606
	.p2align 4,,10
	.p2align 3
.L1563:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L1569:
	addq	$24, %rsp
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r8, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE
	.p2align 4,,10
	.p2align 3
.L1605:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	41112(%r12), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	jne	.L1563
.L1606:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L1607
.L1570:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L1569
	.p2align 4,,10
	.p2align 3
.L1561:
	movq	23(%r14), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	je	.L1559
	testq	%rdi, %rdi
	je	.L1565
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
	jmp	.L1604
	.p2align 4,,10
	.p2align 3
.L1565:
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L1608
.L1567:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rbx)
	jmp	.L1604
	.p2align 4,,10
	.p2align 3
.L1607:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L1570
	.p2align 4,,10
	.p2align 3
.L1608:
	movq	%r12, %rdi
	movq	%r14, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L1567
	.p2align 4,,10
	.p2align 3
.L1557:
	movl	11(%rsi), %r14d
	testb	$1, %sil
	je	.L1584
	movq	%rsi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	movl	$1, %eax
	cmove	%eax, %r13d
.L1571:
	movq	-1(%rsi), %rax
	movl	%r13d, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	testb	$8, 11(%rax)
	je	.L1572
	call	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1575
	movq	(%rax), %rax
	movq	(%rbx), %rdi
	movl	%r14d, %ecx
	xorl	%edx, %edx
	leaq	15(%rax), %rsi
	call	_ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii
.L1574:
	movq	(%rbx), %r15
	movq	0(%r13), %r14
	movq	%r14, 15(%r15)
	leaq	15(%r15), %rsi
	testb	$1, %r14b
	je	.L1583
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L1577
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
.L1577:
	testb	$24, %al
	je	.L1583
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1583
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1583:
	movq	(%rbx), %r14
	movq	128(%r12), %r12
	movq	%r12, 23(%r14)
	leaq	23(%r14), %r15
	testb	$1, %r12b
	je	.L1582
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L1580
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L1580:
	testb	$24, %al
	je	.L1582
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1582
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L1582:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1572:
	.cfi_restore_state
	call	_ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1575
	movq	(%rax), %rax
	movq	(%rbx), %rdi
	movl	%r14d, %ecx
	xorl	%edx, %edx
	leaq	15(%rax), %rsi
	call	_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii
	jmp	.L1574
.L1584:
	movl	$1, %r13d
	jmp	.L1571
.L1575:
	leaq	.LC13(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19660:
	.size	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE, .-_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE
	.section	.text._ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE,"axG",@progbits,_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE
	.type	_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE, @function
_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE:
.LFB12225:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -32
	movq	(%rsi), %rcx
	movq	-1(%rcx), %rdi
	movq	%rcx, %r12
	cmpw	$63, 11(%rdi)
	jbe	.L1625
.L1611:
	movq	-1(%r12), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L1626
.L1620:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1626:
	.cfi_restore_state
	movq	-1(%r12), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$5, %dx
	jne	.L1620
	movq	(%rax), %rax
	movq	41112(%r13), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1621
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1625:
	.cfi_restore_state
	movq	-1(%rcx), %rdi
	movzwl	11(%rdi), %edi
	andl	$7, %edi
	cmpw	$1, %di
	jne	.L1611
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L1614
	movq	23(%rcx), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	je	.L1614
	addq	$16, %rsp
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE
	.p2align 4,,10
	.p2align 3
.L1621:
	.cfi_restore_state
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L1627
.L1623:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1614:
	.cfi_restore_state
	movq	41112(%r13), %rdi
	movq	15(%rcx), %r12
	testq	%rdi, %rdi
	je	.L1616
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
	jmp	.L1611
	.p2align 4,,10
	.p2align 3
.L1616:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L1628
.L1618:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%r12, (%rax)
	jmp	.L1611
.L1628:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1618
	.p2align 4,,10
	.p2align 3
.L1627:
	movq	%r13, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L1623
	.cfi_endproc
.LFE12225:
	.size	_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE, .-_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE
	.section	.text._ZN2v88internal6String7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.type	_ZN2v88internal6String7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_, @function
_ZN2v88internal6String7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_:
.LFB19695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rsi, %rdx
	je	.L1646
	movq	%rdx, %r12
	movq	(%rsi), %rax
	movq	(%rdx), %rdx
	movq	%rdi, %r14
	movq	%rsi, %r13
	testq	%rsi, %rsi
	je	.L1631
	testq	%r12, %r12
	je	.L1631
	movl	$1, %r15d
	cmpq	%rax, %rdx
	je	.L1630
.L1631:
	movl	11(%rdx), %edx
	movl	11(%rax), %ecx
	testl	%edx, %edx
	jne	.L1632
	movl	$1, %r15d
	testl	%ecx, %ecx
	jne	.L1635
	.p2align 4,,10
	.p2align 3
.L1630:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1656
	addq	$72, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1632:
	.cfi_restore_state
	testl	%ecx, %ecx
	jne	.L1633
.L1634:
	xorl	%r15d, %r15d
	jmp	.L1630
.L1659:
	movq	%r10, %rdx
	movq	%r9, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	js	.L1634
	je	.L1630
	.p2align 4,,10
	.p2align 3
.L1635:
	movl	$2, %r15d
	jmp	.L1630
	.p2align 4,,10
	.p2align 3
.L1633:
	movl	$0, -72(%rbp)
	movq	-1(%rax), %rdx
	leaq	-68(%rbp), %r8
	leaq	-64(%rbp), %rbx
	movq	%r8, %rdi
	movq	%rbx, %rsi
	movq	%r8, -88(%rbp)
	movzwl	11(%rdx), %edx
	movq	%rax, -64(%rbp)
	movl	%edx, -68(%rbp)
	leaq	-72(%rbp), %rdx
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
	movq	%rbx, %rsi
	movzwl	%ax, %r15d
	movq	(%r12), %rax
	movl	$0, -72(%rbp)
	movq	-1(%rax), %rcx
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %rdx
	movzwl	11(%rcx), %ecx
	movq	%r8, %rdi
	movq	%rax, -64(%rbp)
	movl	%ecx, -68(%rbp)
	call	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
	movzwl	%ax, %eax
	cmpl	%eax, %r15d
	js	.L1634
	jne	.L1635
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movl	$2, %r15d
	call	_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE
	movq	-88(%rbp), %r8
	movq	%rax, %r12
	movq	0(%r13), %rax
	movq	(%r12), %rcx
	movl	11(%rax), %edx
	movl	11(%rcx), %r10d
	cmpl	%r10d, %edx
	jle	.L1657
.L1636:
	movq	%r8, %rsi
	movq	%rbx, %rdi
	movl	%r10d, -100(%rbp)
	movq	%r8, -96(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	movq	-96(%rbp), %r8
	movq	%rbx, %rdi
	movq	%rax, -88(%rbp)
	movq	%rax, %r13
	movq	(%r12), %rax
	movq	%rdx, %r14
	movq	%r8, %rsi
	shrq	$32, %r14
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	movslq	-100(%rbp), %r10
	movq	-88(%rbp), %r9
	movq	%rax, %rsi
	movq	%rax, %rcx
	movq	%rdx, %rax
	shrq	$32, %rax
	cmpl	$1, %r14d
	je	.L1658
	leaq	0(%r13,%r10,2), %rsi
	cmpl	$1, %eax
	jne	.L1655
	jmp	.L1654
	.p2align 4,,10
	.p2align 3
.L1644:
	movzwl	0(%r13), %eax
	movzwl	(%rcx), %edx
	subl	%edx, %eax
	jne	.L1640
	addq	$2, %r13
	addq	$2, %rcx
.L1655:
	cmpq	%rsi, %r13
	jb	.L1644
	jmp	.L1630
	.p2align 4,,10
	.p2align 3
.L1646:
	movl	$1, %r15d
	jmp	.L1630
	.p2align 4,,10
	.p2align 3
.L1643:
	movzwl	0(%r13), %eax
	movzbl	(%rcx), %edx
	subl	%edx, %eax
	jne	.L1640
	addq	$2, %r13
	addq	$1, %rcx
.L1654:
	cmpq	%rsi, %r13
	jb	.L1643
	jmp	.L1630
	.p2align 4,,10
	.p2align 3
.L1657:
	sete	%r15b
	movl	%edx, %r10d
	movzbl	%r15b, %r15d
	jmp	.L1636
	.p2align 4,,10
	.p2align 3
.L1658:
	cmpl	$1, %eax
	je	.L1659
	leaq	(%r9,%r10), %rax
	cmpq	%rax, %r9
	jnb	.L1630
	xorl	%edx, %edx
	jmp	.L1641
	.p2align 4,,10
	.p2align 3
.L1660:
	addq	$1, %rdx
	cmpq	%rdx, %r10
	je	.L1630
.L1641:
	movzbl	(%r9,%rdx), %eax
	movzwl	(%rsi,%rdx,2), %ecx
	subl	%ecx, %eax
	je	.L1660
.L1640:
	testl	%eax, %eax
	js	.L1634
	jmp	.L1635
.L1656:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19695:
	.size	_ZN2v88internal6String7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_, .-_ZN2v88internal6String7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.section	.rodata._ZN2v88internal6String11LastIndexOfEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_.str1.1,"aMS",@progbits,1
.LC15:
	.string	"String.prototype.lastIndexOf"
	.section	.text._ZN2v88internal6String11LastIndexOfEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String11LastIndexOfEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_
	.type	_ZN2v88internal6String11LastIndexOfEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_, @function
_ZN2v88internal6String11LastIndexOfEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_:
.LFB19705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	cmpq	104(%rdi), %rax
	jne	.L1752
.L1662:
	leaq	.LC15(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$28, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1753
	movl	$27, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L1668:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1754
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1752:
	.cfi_restore_state
	cmpq	88(%rdi), %rax
	je	.L1662
	movq	%rcx, %r15
	movq	%rcx, %r13
	testb	$1, %al
	jne	.L1663
.L1666:
	movq	%r12, %rdi
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-104(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L1749
.L1665:
	movq	(%rdx), %rax
	movq	%rdx, %r8
	testb	$1, %al
	jne	.L1669
.L1672:
	movq	%rdx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L1749
.L1671:
	movq	(%r15), %rax
	testb	$1, %al
	je	.L1676
	movq	-1(%rax), %rax
	cmpw	$65, 11(%rax)
	jne	.L1755
.L1676:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1756
.L1678:
	movq	(%r14), %rdx
.L1684:
	sarq	$32, %rax
	movl	$0, %ebx
	testq	%rax, %rax
	cmovle	%ebx, %eax
.L1687:
	movl	11(%rdx), %edx
	cmpl	%eax, %edx
	cmovbe	%edx, %eax
	movl	%eax, %ebx
.L1682:
	movq	(%r8), %rax
	movl	%edx, %ecx
	movl	11(%rax), %eax
	leal	(%rbx,%rax), %esi
	subl	%eax, %ecx
	cmpl	%edx, %esi
	cmova	%ecx, %ebx
	testl	%eax, %eax
	jne	.L1691
.L1751:
	movq	%rbx, %rax
	salq	$32, %rax
	jmp	.L1668
	.p2align 4,,10
	.p2align 3
.L1755:
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movq	-104(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.L1676
.L1749:
	movq	312(%r12), %rax
	jmp	.L1668
	.p2align 4,,10
	.p2align 3
.L1753:
	leaq	.LC13(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1663:
	movq	-1(%rax), %rax
	movq	%rsi, %r14
	cmpw	$63, 11(%rax)
	jbe	.L1665
	jmp	.L1666
	.p2align 4,,10
	.p2align 3
.L1669:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1672
	jmp	.L1671
	.p2align 4,,10
	.p2align 3
.L1756:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L1679
.L1747:
	xorl	%edx, %edx
	testb	%dl, %dl
	jne	.L1678
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-104(%rbp), %r8
	testq	%rax, %rax
	je	.L1749
	movq	(%rax), %rax
	movq	(%r14), %rdx
	testb	$1, %al
	je	.L1684
	movsd	7(%rax), %xmm0
	xorl	%eax, %eax
	comisd	.LC16(%rip), %xmm0
	jb	.L1687
	movsd	.LC17(%rip), %xmm1
	movl	$-1, %eax
	comisd	%xmm0, %xmm1
	jbe	.L1687
	cvttsd2siq	%xmm0, %rax
	jmp	.L1687
	.p2align 4,,10
	.p2align 3
.L1691:
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE
	movq	-104(%rbp), %r8
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%rax, %r13
	leaq	-80(%rbp), %r15
	movq	%r8, %rsi
	call	_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE
	leaq	-81(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %r14
	movq	0(%r13), %rax
	movq	%rsi, -104(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	movq	-104(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	movq	(%r14), %rax
	shrq	$32, %rdx
	movq	%rdx, %r13
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	movq	%rax, %rsi
	movq	%rdx, %rax
	movl	%edx, %edi
	shrq	$32, %rax
	cmpl	$1, %eax
	je	.L1757
	movzwl	(%rsi), %r9d
	cmpl	$1, %r13d
	je	.L1758
	movabsq	$-4294967296, %rax
	testl	%ebx, %ebx
	js	.L1668
	movslq	%ebx, %rbx
	leal	-2(%rdx), %eax
	leaq	(%r12,%rbx,2), %rcx
	addq	$3, %rax
	jmp	.L1719
	.p2align 4,,10
	.p2align 3
.L1716:
	subq	$1, %rbx
	subq	$2, %rcx
	testl	%ebx, %ebx
	js	.L1750
.L1719:
	cmpw	%r9w, (%rcx)
	jne	.L1716
	cmpl	$1, %edi
	jle	.L1732
	movl	$2, %edx
	.p2align 4,,10
	.p2align 3
.L1718:
	movzwl	-2(%rcx,%rdx,2), %r11d
	cmpw	%r11w, -2(%rsi,%rdx,2)
	jne	.L1716
	movl	%edx, %r8d
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L1718
.L1717:
	cmpl	%r8d, %edi
	jne	.L1716
	jmp	.L1751
.L1758:
	testl	%edx, %edx
	jle	.L1707
	subl	$1, %edx
	movq	%rsi, %rax
	leaq	2(%rsi,%rdx,2), %rdx
	jmp	.L1709
	.p2align 4,,10
	.p2align 3
.L1759:
	addq	$2, %rax
	cmpq	%rax, %rdx
	je	.L1707
.L1709:
	cmpw	$255, (%rax)
	jbe	.L1759
	.p2align 4,,10
	.p2align 3
.L1750:
	movabsq	$-4294967296, %rax
	jmp	.L1668
	.p2align 4,,10
	.p2align 3
.L1679:
	movsd	7(%rax), %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.L1747
	movq	(%r14), %rax
	movl	11(%rax), %ebx
	movl	%ebx, %edx
	jmp	.L1682
.L1757:
	movabsq	$-4294967296, %rax
	movzbl	(%rsi), %r10d
	cmpl	$1, %r13d
	je	.L1760
	testl	%ebx, %ebx
	js	.L1668
	movslq	%ebx, %rbx
	leal	-2(%rdx), %r9d
	leaq	(%r12,%rbx,2), %rax
	addq	$3, %r9
	jmp	.L1704
	.p2align 4,,10
	.p2align 3
.L1701:
	subq	$1, %rbx
	subq	$2, %rax
	testl	%ebx, %ebx
	js	.L1750
.L1704:
	cmpw	%r10w, (%rax)
	jne	.L1701
	cmpl	$1, %edi
	jle	.L1727
	movl	$2, %edx
	.p2align 4,,10
	.p2align 3
.L1703:
	movzbl	-1(%rsi,%rdx), %r8d
	movzwl	-2(%rax,%rdx,2), %ecx
	cmpl	%ecx, %r8d
	jne	.L1701
	movl	%edx, %ecx
	addq	$1, %rdx
	cmpq	%rdx, %r9
	jne	.L1703
.L1702:
	cmpl	%ecx, %edi
	jne	.L1701
	jmp	.L1751
.L1760:
	testl	%ebx, %ebx
	js	.L1668
	movslq	%ebx, %rbx
	leal	-2(%rdx), %edx
	addq	%rbx, %r12
	addq	$3, %rdx
	jmp	.L1698
	.p2align 4,,10
	.p2align 3
.L1695:
	subq	$1, %rbx
	subq	$1, %r12
	testl	%ebx, %ebx
	js	.L1750
.L1698:
	cmpb	%r10b, (%r12)
	jne	.L1695
	cmpl	$1, %edi
	jle	.L1725
	movl	$2, %eax
	.p2align 4,,10
	.p2align 3
.L1697:
	movzbl	-1(%rax,%r12), %ecx
	cmpb	%cl, -1(%rax,%rsi)
	jne	.L1695
	movl	%eax, %ecx
	addq	$1, %rax
	cmpq	%rdx, %rax
	jne	.L1697
.L1696:
	cmpl	%ecx, %edi
	jne	.L1695
	jmp	.L1751
.L1707:
	movabsq	$-4294967296, %rax
	testl	%ebx, %ebx
	js	.L1668
	movslq	%ebx, %rbx
	leal	-2(%rdi), %r8d
	addq	%rbx, %r12
	addq	$2, %r8
	jmp	.L1713
	.p2align 4,,10
	.p2align 3
.L1710:
	subq	$1, %rbx
	subq	$1, %r12
	testl	%ebx, %ebx
	js	.L1750
.L1713:
	movzbl	(%r12), %eax
	cmpl	%r9d, %eax
	jne	.L1710
	cmpl	$1, %edi
	jle	.L1730
	movl	$1, %eax
	jmp	.L1712
	.p2align 4,,10
	.p2align 3
.L1761:
	leal	1(%rax), %edx
	addq	$1, %rax
	cmpq	%rax, %r8
	je	.L1711
.L1712:
	movzbl	(%r12,%rax), %ecx
	movl	%eax, %edx
	cmpw	%cx, (%rsi,%rax,2)
	je	.L1761
.L1711:
	cmpl	%edx, %edi
	jne	.L1710
	jmp	.L1751
.L1732:
	movl	$1, %r8d
	jmp	.L1717
.L1727:
	movl	$1, %ecx
	jmp	.L1702
.L1730:
	movl	$1, %edx
	jmp	.L1711
.L1725:
	movl	$1, %ecx
	jmp	.L1696
.L1754:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19705:
	.size	_ZN2v88internal6String11LastIndexOfEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_, .-_ZN2v88internal6String11LastIndexOfEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_
	.section	.text._ZN2v88internal6String10SlowEqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String10SlowEqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.type	_ZN2v88internal6String10SlowEqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_, @function
_ZN2v88internal6String10SlowEqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_:
.LFB19694:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdx
	movq	(%r12), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	11(%rdx), %ebx
	cmpl	11(%rcx), %ebx
	jne	.L1815
	testl	%ebx, %ebx
	jne	.L1765
.L1799:
	movl	$1, %eax
	jmp	.L1764
	.p2align 4,,10
	.p2align 3
.L1765:
	movq	-1(%rdx), %rax
	movq	%rdi, %r14
	movq	%rsi, %r13
	movq	%rsi, %r8
	cmpw	$63, 11(%rax)
	jbe	.L1766
.L1770:
	movq	-1(%rcx), %rax
	cmpw	$63, 11(%rax)
	ja	.L1768
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L1769
.L1768:
	movl	7(%rdx), %eax
	testb	$1, %al
	jne	.L1787
	movl	7(%rcx), %ecx
	testb	$1, %cl
	je	.L1816
.L1787:
	movl	$0, -72(%rbp)
	movq	-1(%rdx), %rax
	leaq	-68(%rbp), %r9
	leaq	-64(%rbp), %r15
	movq	%r9, %rdi
	movq	%r15, %rsi
	movq	%r9, -96(%rbp)
	movzwl	11(%rax), %eax
	movq	%rdx, -64(%rbp)
	leaq	-72(%rbp), %rdx
	movq	%rdx, -104(%rbp)
	movl	%eax, -68(%rbp)
	call	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
	movq	%r15, %rsi
	movw	%ax, -88(%rbp)
	movq	(%r12), %rax
	movl	$0, -72(%rbp)
	movq	-1(%rax), %rcx
	movq	-96(%rbp), %r9
	movq	-104(%rbp), %rdx
	movzwl	11(%rcx), %ecx
	movq	%r9, %rdi
	movq	%rax, -64(%rbp)
	movl	%ecx, -68(%rbp)
	call	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
	cmpw	%ax, -88(%rbp)
	movq	-96(%rbp), %r9
	je	.L1817
	.p2align 4,,10
	.p2align 3
.L1815:
	xorl	%eax, %eax
.L1764:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1818
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1766:
	.cfi_restore_state
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L1770
.L1769:
	movq	-1(%rdx), %rax
	cmpw	$63, 11(%rax)
	ja	.L1774
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L1774
	movq	41112(%r14), %rdi
	movq	15(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L1775
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%r12), %rcx
	movq	%rax, %r8
	.p2align 4,,10
	.p2align 3
.L1774:
	movq	-1(%rcx), %rax
	cmpw	$63, 11(%rax)
	ja	.L1778
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L1778
	movq	41112(%r14), %rdi
	movq	15(%rcx), %rsi
	testq	%rdi, %rdi
	je	.L1779
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-88(%rbp), %r8
	movq	%rax, %r15
	cmpq	%r8, %rax
	je	.L1799
	movq	(%r8), %rdx
	testq	%rax, %rax
	je	.L1783
	testq	%r8, %r8
	jne	.L1796
.L1783:
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L1785
	movq	(%r15), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	je	.L1815
.L1785:
	movq	%r15, %rdx
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6String10SlowEqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_
	jmp	.L1764
	.p2align 4,,10
	.p2align 3
.L1779:
	movq	41088(%r14), %r15
	cmpq	41096(%r14), %r15
	je	.L1819
.L1782:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r15)
	.p2align 4,,10
	.p2align 3
.L1778:
	movl	$1, %eax
	cmpq	%r15, %r8
	je	.L1764
	movq	(%r8), %rdx
.L1796:
	movl	$1, %eax
	cmpq	(%r15), %rdx
	jne	.L1783
	jmp	.L1764
	.p2align 4,,10
	.p2align 3
.L1817:
	movq	%r13, %rsi
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE
	movq	%r12, %rsi
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%rax, %r13
	call	_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE
	movq	-88(%rbp), %r9
	movq	%r15, %rdi
	movq	%rax, %r12
	movq	0(%r13), %rax
	movq	%r9, %rsi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	movq	-88(%rbp), %r9
	movq	%r15, %rdi
	movq	%rax, %r13
	movq	(%r12), %rax
	shrq	$32, %rdx
	movq	%r9, %rsi
	movq	%rdx, %r14
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	shrq	$32, %rdx
	cmpq	$1, %r14
	jne	.L1789
	cmpq	$1, %rdx
	je	.L1820
.L1789:
	testl	%ebx, %ebx
	jle	.L1799
	leal	-1(%rbx), %esi
	cmpl	$1, %r14d
	je	.L1791
	cmpl	$1, %edx
	movl	$0, %edx
	jne	.L1793
	jmp	.L1794
	.p2align 4,,10
	.p2align 3
.L1821:
	leaq	1(%rdx), %rcx
	cmpq	%rsi, %rdx
	je	.L1799
	movq	%rcx, %rdx
.L1793:
	movzwl	0(%r13,%rdx,2), %ebx
	cmpw	%bx, (%rax,%rdx,2)
	je	.L1821
	jmp	.L1815
	.p2align 4,,10
	.p2align 3
.L1822:
	leaq	1(%rdx), %rcx
	cmpq	%rsi, %rdx
	je	.L1799
	movq	%rcx, %rdx
.L1794:
	movzbl	(%rax,%rdx), %ecx
	cmpw	0(%r13,%rdx,2), %cx
	je	.L1822
	jmp	.L1815
	.p2align 4,,10
	.p2align 3
.L1816:
	shrl	$2, %ecx
	shrl	$2, %eax
	cmpl	%eax, %ecx
	jne	.L1815
	jmp	.L1787
	.p2align 4,,10
	.p2align 3
.L1775:
	movq	41088(%r14), %r8
	cmpq	41096(%r14), %r8
	je	.L1823
.L1776:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r8)
	movq	(%r12), %rcx
	jmp	.L1774
.L1791:
	xorl	%edx, %edx
	jmp	.L1795
	.p2align 4,,10
	.p2align 3
.L1824:
	leaq	1(%rdx), %rcx
	cmpq	%rsi, %rdx
	je	.L1799
	movq	%rcx, %rdx
.L1795:
	movzbl	0(%r13,%rdx), %ecx
	cmpw	%cx, (%rax,%rdx,2)
	je	.L1824
	jmp	.L1815
.L1820:
	movslq	%ebx, %rdx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	sete	%al
	jmp	.L1764
.L1819:
	movq	%r14, %rdi
	movq	%rsi, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %r8
	movq	%rax, %r15
	jmp	.L1782
.L1823:
	movq	%r14, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L1776
.L1818:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19694:
	.size	_ZN2v88internal6String10SlowEqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_, .-_ZN2v88internal6String10SlowEqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.section	.text._ZN2v88internal6String8ToNumberEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String8ToNumberEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal6String8ToNumberEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal6String8ToNumberEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB19672:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, %rsi
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L1904
.L1827:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	jbe	.L1836
.L1902:
	movq	(%r12), %r14
.L1835:
	movq	%r14, -56(%rbp)
	movl	7(%r14), %esi
	testb	$1, %sil
	jne	.L1842
	testb	$2, %sil
	jne	.L1844
	movl	11(%r14), %eax
	cmpl	$7, %eax
	jle	.L1871
.L1872:
	cmpl	$10, %eax
	jle	.L1905
.L1844:
	movq	(%r12), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1849
	movq	-1(%rax), %rdx
	testb	$7, 11(%rdx)
	je	.L1906
.L1849:
	movq	%r12, %rsi
	movq	%r13, %rdi
	pxor	%xmm0, %xmm0
	movl	$11, %edx
	call	_ZN2v88internal14StringToDoubleEPNS0_7IsolateENS0_6HandleINS0_6StringEEEid@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
.L1847:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1907
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1836:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L1902
	movq	(%r12), %rax
	movq	41112(%r13), %rdi
	movq	15(%rax), %r14
	testq	%rdi, %rdi
	je	.L1839
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
	jmp	.L1902
	.p2align 4,,10
	.p2align 3
.L1904:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L1827
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L1830
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L1830
	movq	%r12, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE
	movq	(%rax), %r14
	movq	%rax, %r12
	jmp	.L1835
	.p2align 4,,10
	.p2align 3
.L1842:
	movl	11(%r14), %eax
	cmpl	$7, %eax
	jg	.L1872
	leaq	-48(%rbp), %rdi
	movq	%r14, -48(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv
	movq	-56(%rbp), %rax
	movl	7(%rax), %esi
	testb	$2, %sil
	jne	.L1844
	.p2align 4,,10
	.p2align 3
.L1871:
	shrl	$2, %esi
	andl	$16777215, %esi
	movl	%esi, -48(%rbp)
	jmp	.L1846
	.p2align 4,,10
	.p2align 3
.L1905:
	leaq	-48(%rbp), %rsi
	leaq	-56(%rbp), %rdi
	call	_ZN2v88internal6String17ComputeArrayIndexEPj.part.0
	testb	%al, %al
	je	.L1844
	movl	-48(%rbp), %esi
.L1846:
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	jmp	.L1847
	.p2align 4,,10
	.p2align 3
.L1839:
	movq	41088(%r13), %r12
	cmpq	41096(%r13), %r12
	je	.L1908
.L1841:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r13)
	movq	%r14, (%r12)
	jmp	.L1835
	.p2align 4,,10
	.p2align 3
.L1906:
	movq	-1(%rax), %rdx
	testb	$8, 11(%rdx)
	je	.L1849
	movl	11(%rax), %esi
	testl	%esi, %esi
	jne	.L1851
	movq	41112(%r13), %rdi
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rsi
	testq	%rdi, %rdi
	je	.L1852
.L1903:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L1847
	.p2align 4,,10
	.p2align 3
.L1830:
	movq	41112(%r13), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1832
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
	jmp	.L1827
	.p2align 4,,10
	.p2align 3
.L1832:
	movq	41088(%r13), %r12
	cmpq	41096(%r13), %r12
	je	.L1909
.L1834:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r12)
	jmp	.L1827
.L1909:
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L1834
	.p2align 4,,10
	.p2align 3
.L1851:
	movzbl	15(%rax), %r9d
	leaq	15(%rax), %r8
	cmpb	$45, %r9b
	je	.L1910
	movzbl	%r9b, %edi
	movl	$1, %r11d
	xorl	%edx, %edx
.L1855:
	cmpb	$57, %dil
	jbe	.L1857
	cmpb	$73, %dil
	je	.L1849
	cmpb	$-96, %dil
	je	.L1849
.L1856:
	leaq	1088(%r13), %rax
	jmp	.L1847
	.p2align 4,,10
	.p2align 3
.L1908:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r12
	jmp	.L1841
.L1852:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L1911
.L1854:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L1847
.L1910:
	cmpl	$1, %esi
	je	.L1856
	movzbl	1(%r8), %edi
	movl	$2, %r11d
	movl	$1, %edx
	jmp	.L1855
	.p2align 4,,10
	.p2align 3
.L1857:
	movl	%esi, %ecx
	subl	%edx, %ecx
	cmpl	$9, %ecx
	jg	.L1849
	cmpl	%edx, %esi
	jle	.L1859
	leal	-1(%rsi), %r14d
	movslq	%edx, %r10
	movl	%r14d, %ebx
	leaq	(%r8,%r10), %rcx
	leaq	16(%r10,%rax), %r10
	subl	%edx, %ebx
	addq	%rbx, %r10
	jmp	.L1860
	.p2align 4,,10
	.p2align 3
.L1912:
	addq	$1, %rcx
	cmpq	%rcx, %r10
	je	.L1859
.L1860:
	movzbl	(%rcx), %edx
	subl	$48, %edx
	cmpb	$9, %dl
	jbe	.L1912
	jmp	.L1849
.L1859:
	leal	-48(%rdi), %r14d
	cmpl	%r11d, %esi
	jle	.L1869
	movslq	%r11d, %rcx
	leaq	(%r8,%rcx), %rdx
	leaq	16(%rcx,%rax), %r8
	leal	-1(%rsi), %ecx
	subl	%r11d, %ecx
	addq	%rcx, %r8
	.p2align 4,,10
	.p2align 3
.L1861:
	movzbl	(%rdx), %ecx
	leal	(%r14,%r14,4), %edi
	addq	$1, %rdx
	leal	-48(%rcx,%rdi,2), %r14d
	cmpq	%rdx, %r8
	jne	.L1861
.L1869:
	cmpb	$45, %r9b
	je	.L1913
	testb	$1, 7(%rax)
	je	.L1864
	cmpl	$10, %esi
	jle	.L1914
.L1864:
	movq	41112(%r13), %rdi
	salq	$32, %r14
	testq	%rdi, %rdi
	je	.L1866
	movq	%r14, %rsi
	jmp	.L1903
.L1911:
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L1854
.L1913:
	leaq	1112(%r13), %rax
	testl	%r14d, %r14d
	je	.L1847
	negl	%r14d
	jmp	.L1864
.L1866:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L1915
.L1868:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%r14, (%rax)
	jmp	.L1847
.L1907:
	call	__stack_chk_fail@PLT
.L1914:
	cmpb	$48, %r9b
	jne	.L1876
	cmpl	$1, %esi
	jne	.L1864
.L1876:
	movl	%r14d, %edi
	call	_ZN2v88internal12StringHasher18MakeArrayIndexHashEji@PLT
	movl	%eax, %r8d
	movq	(%r12), %rax
	movl	%r8d, 7(%rax)
	jmp	.L1864
.L1915:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1868
	.cfi_endproc
.LFE19672:
	.size	_ZN2v88internal6String8ToNumberEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal6String8ToNumberEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal10ConsString3GetEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ConsString3GetEi
	.type	_ZN2v88internal10ConsString3GetEi, @function
_ZN2v88internal10ConsString3GetEi:
.LFB19719:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	jne	.L1917
	jmp	.L1922
	.p2align 4,,10
	.p2align 3
.L1934:
	movq	15(%rax), %rdx
	movl	11(%rdx), %ecx
	cmpl	%ecx, %r12d
	jl	.L1920
	movq	23(%rax), %rdx
	subl	%ecx, %r12d
.L1920:
	movq	%rdx, %rax
.L1917:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L1934
	movq	(%rcx), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L1921
	leaq	.L1923(%rip), %rsi
	movzwl	%dx, %edx
	movslq	(%rsi,%rdx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal10ConsString3GetEi,"a",@progbits
	.align 4
	.align 4
.L1923:
	.long	.L1929-.L1923
	.long	.L1926-.L1923
	.long	.L1928-.L1923
	.long	.L1924-.L1923
	.long	.L1921-.L1923
	.long	.L1922-.L1923
	.long	.L1921-.L1923
	.long	.L1921-.L1923
	.long	.L1927-.L1923
	.long	.L1926-.L1923
	.long	.L1925-.L1923
	.long	.L1924-.L1923
	.long	.L1921-.L1923
	.long	.L1922-.L1923
	.section	.text._ZN2v88internal10ConsString3GetEi
	.p2align 4,,10
	.p2align 3
.L1922:
	movq	15(%rax), %rax
	movl	%r12d, -40(%rbp)
	leaq	-32(%rbp), %rsi
	leaq	-36(%rbp), %rdi
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	movq	%rax, -32(%rbp)
	movl	%edx, -36(%rbp)
	leaq	-40(%rbp), %rdx
	call	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
.L1916:
	movq	-24(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L1935
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1924:
	.cfi_restore_state
	movq	15(%rax), %rdx
	addl	27(%rax), %r12d
	leaq	-32(%rbp), %rsi
	leaq	-36(%rbp), %rdi
	movl	%r12d, -40(%rbp)
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movq	%rdx, -32(%rbp)
	leaq	-40(%rbp), %rdx
	movl	%eax, -36(%rbp)
	call	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
	jmp	.L1916
	.p2align 4,,10
	.p2align 3
.L1926:
	leaq	-32(%rbp), %rdi
	movl	%r12d, %esi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal10ConsString3GetEi
	jmp	.L1916
.L1927:
	leal	16(%r12), %eax
	cltq
	movzbl	(%rax,%rcx), %eax
	jmp	.L1916
.L1925:
	movq	15(%rax), %rdi
	movslq	%r12d, %r12
	movq	(%rdi), %rax
	call	*48(%rax)
	movzbl	(%rax,%r12), %eax
	jmp	.L1916
.L1929:
	leal	16(%r12,%r12), %eax
	cltq
	movzwl	(%rax,%rcx), %eax
	jmp	.L1916
.L1928:
	movq	15(%rax), %rdi
	movslq	%r12d, %r12
	movq	(%rdi), %rax
	call	*48(%rax)
	movzwl	(%rax,%r12,2), %eax
	jmp	.L1916
.L1921:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1935:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19719:
	.size	_ZN2v88internal10ConsString3GetEi, .-_ZN2v88internal10ConsString3GetEi
	.section	.text._ZN2v88internal10ThinString3GetEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ThinString3GetEi
	.type	_ZN2v88internal10ThinString3GetEi, @function
_ZN2v88internal10ThinString3GetEi:
.LFB19720:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	15(%rax), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L1937
	leaq	.L1939(%rip), %rcx
	movzwl	%ax, %eax
	movl	%esi, %r12d
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal10ThinString3GetEi,"a",@progbits
	.align 4
	.align 4
.L1939:
	.long	.L1945-.L1939
	.long	.L1942-.L1939
	.long	.L1944-.L1939
	.long	.L1940-.L1939
	.long	.L1937-.L1939
	.long	.L1938-.L1939
	.long	.L1937-.L1939
	.long	.L1937-.L1939
	.long	.L1943-.L1939
	.long	.L1942-.L1939
	.long	.L1941-.L1939
	.long	.L1940-.L1939
	.long	.L1937-.L1939
	.long	.L1938-.L1939
	.section	.text._ZN2v88internal10ThinString3GetEi
	.p2align 4,,10
	.p2align 3
.L1938:
	movq	15(%rdx), %rax
	movl	%esi, -40(%rbp)
.L1949:
	movq	-1(%rax), %rdx
	leaq	-32(%rbp), %rsi
	leaq	-36(%rbp), %rdi
	movzwl	11(%rdx), %edx
	movq	%rax, -32(%rbp)
	movl	%edx, -36(%rbp)
	leaq	-40(%rbp), %rdx
	call	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
.L1936:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1950
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1942:
	.cfi_restore_state
	leaq	-32(%rbp), %rdi
	movq	%rdx, -32(%rbp)
	call	_ZN2v88internal10ConsString3GetEi
	jmp	.L1936
	.p2align 4,,10
	.p2align 3
.L1940:
	movl	27(%rdx), %esi
	movq	15(%rdx), %rax
	addl	%r12d, %esi
	movl	%esi, -40(%rbp)
	jmp	.L1949
	.p2align 4,,10
	.p2align 3
.L1943:
	leal	16(%rsi), %esi
	movslq	%esi, %rsi
	movzbl	-1(%rdx,%rsi), %eax
	jmp	.L1936
	.p2align 4,,10
	.p2align 3
.L1944:
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	%r12d, %rsi
	movzwl	(%rax,%rsi,2), %eax
	jmp	.L1936
	.p2align 4,,10
	.p2align 3
.L1941:
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	%r12d, %rsi
	movzbl	(%rax,%rsi), %eax
	jmp	.L1936
	.p2align 4,,10
	.p2align 3
.L1945:
	leal	16(%rsi,%rsi), %eax
	cltq
	movzwl	-1(%rdx,%rax), %eax
	jmp	.L1936
.L1937:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1950:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19720:
	.size	_ZN2v88internal10ThinString3GetEi, .-_ZN2v88internal10ThinString3GetEi
	.section	.text._ZN2v88internal12SlicedString3GetEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12SlicedString3GetEi
	.type	_ZN2v88internal12SlicedString3GetEi, @function
_ZN2v88internal12SlicedString3GetEi:
.LFB19721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	15(%rax), %rcx
	addl	27(%rax), %esi
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L1952
	leaq	.L1954(%rip), %rdx
	movzwl	%ax, %eax
	movl	%esi, %r12d
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal12SlicedString3GetEi,"a",@progbits
	.align 4
	.align 4
.L1954:
	.long	.L1960-.L1954
	.long	.L1957-.L1954
	.long	.L1959-.L1954
	.long	.L1955-.L1954
	.long	.L1952-.L1954
	.long	.L1953-.L1954
	.long	.L1952-.L1954
	.long	.L1952-.L1954
	.long	.L1958-.L1954
	.long	.L1957-.L1954
	.long	.L1956-.L1954
	.long	.L1955-.L1954
	.long	.L1952-.L1954
	.long	.L1953-.L1954
	.section	.text._ZN2v88internal12SlicedString3GetEi
	.p2align 4,,10
	.p2align 3
.L1953:
	movq	15(%rcx), %rax
	movl	%esi, -40(%rbp)
.L1964:
	movq	-1(%rax), %rdx
	leaq	-32(%rbp), %rsi
	leaq	-36(%rbp), %rdi
	movzwl	11(%rdx), %edx
	movq	%rax, -32(%rbp)
	movl	%edx, -36(%rbp)
	leaq	-40(%rbp), %rdx
	call	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
.L1951:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1965
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1957:
	.cfi_restore_state
	leaq	-32(%rbp), %rdi
	movq	%rcx, -32(%rbp)
	call	_ZN2v88internal10ConsString3GetEi
	jmp	.L1951
	.p2align 4,,10
	.p2align 3
.L1955:
	movl	27(%rcx), %esi
	movq	15(%rcx), %rax
	addl	%r12d, %esi
	movl	%esi, -40(%rbp)
	jmp	.L1964
	.p2align 4,,10
	.p2align 3
.L1958:
	leal	16(%rsi), %esi
	movslq	%esi, %rsi
	movzbl	-1(%rcx,%rsi), %eax
	jmp	.L1951
	.p2align 4,,10
	.p2align 3
.L1959:
	movq	15(%rcx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	%r12d, %rsi
	movzwl	(%rax,%rsi,2), %eax
	jmp	.L1951
	.p2align 4,,10
	.p2align 3
.L1956:
	movq	15(%rcx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	%r12d, %rsi
	movzbl	(%rax,%rsi), %eax
	jmp	.L1951
	.p2align 4,,10
	.p2align 3
.L1960:
	leal	16(%rsi,%rsi), %eax
	cltq
	movzwl	-1(%rcx,%rax), %eax
	jmp	.L1951
.L1952:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1965:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19721:
	.size	_ZN2v88internal12SlicedString3GetEi, .-_ZN2v88internal12SlicedString3GetEi
	.section	.text._ZN2v88internal6String4TrimEPNS0_7IsolateENS0_6HandleIS1_EENS1_8TrimModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String4TrimEPNS0_7IsolateENS0_6HandleIS1_EENS1_8TrimModeE
	.type	_ZN2v88internal6String4TrimEPNS0_7IsolateENS0_6HandleIS1_EENS1_8TrimModeE, @function
_ZN2v88internal6String4TrimEPNS0_7IsolateENS0_6HandleIS1_EENS1_8TrimModeE:
.LFB19668:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -104(%rbp)
	movl	%edx, -92(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, %rsi
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L2065
.L1968:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	jbe	.L1977
.L2062:
	movq	(%r15), %rsi
.L1976:
	cmpl	$1, -92(%rbp)
	movl	11(%rsi), %r14d
	jbe	.L2066
	cmpl	$2, -92(%rbp)
	jne	.L1986
	xorl	%r12d, %r12d
	leal	-1(%r14), %r13d
	testl	%r14d, %r14d
	jle	.L1986
.L2054:
	movslq	%r14d, %r9
	movl	%r12d, -88(%rbp)
	leaq	-1(%r9), %rbx
	movq	%rbx, %r12
	.p2align 4,,10
	.p2align 3
.L2023:
	movq	-1(%rsi), %rax
	leaq	-1(%rsi), %rdx
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L1987
	leaq	.L2025(%rip), %rcx
	movzwl	%ax, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6String4TrimEPNS0_7IsolateENS0_6HandleIS1_EENS1_8TrimModeE,"a",@progbits
	.align 4
	.align 4
.L2025:
	.long	.L2031-.L2025
	.long	.L2028-.L2025
	.long	.L2030-.L2025
	.long	.L2026-.L2025
	.long	.L1987-.L2025
	.long	.L2024-.L2025
	.long	.L1987-.L2025
	.long	.L1987-.L2025
	.long	.L2029-.L2025
	.long	.L2028-.L2025
	.long	.L2027-.L2025
	.long	.L2026-.L2025
	.long	.L1987-.L2025
	.long	.L2024-.L2025
	.section	.text._ZN2v88internal6String4TrimEPNS0_7IsolateENS0_6HandleIS1_EENS1_8TrimModeE
	.p2align 4,,10
	.p2align 3
.L2069:
	movl	%ebx, %edi
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	testb	%al, %al
	jne	.L2052
	subl	$8232, %ebx
	cmpl	$1, %ebx
	jbe	.L2052
.L2060:
	movl	-88(%rbp), %r12d
.L2021:
	testl	%r12d, %r12d
	je	.L2067
.L2018:
	movq	-104(%rbp), %rdi
	movq	%r15, %rsi
	movl	%r14d, %ecx
	movl	%r12d, %edx
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	%rax, %r15
.L1986:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2068
	addq	$72, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1977:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L2062
	movq	(%r15), %rax
	movq	15(%rax), %rsi
	movq	-104(%rbp), %rax
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1980
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
	jmp	.L2062
	.p2align 4,,10
	.p2align 3
.L2024:
	movq	15(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L1987
	leaq	.L2043(%rip), %rbx
	movzwl	%ax, %eax
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6String4TrimEPNS0_7IsolateENS0_6HandleIS1_EENS1_8TrimModeE
	.align 4
	.align 4
.L2043:
	.long	.L2049-.L2043
	.long	.L2046-.L2043
	.long	.L2048-.L2043
	.long	.L2044-.L2043
	.long	.L1987-.L2043
	.long	.L2042-.L2043
	.long	.L1987-.L2043
	.long	.L1987-.L2043
	.long	.L2047-.L2043
	.long	.L2046-.L2043
	.long	.L2045-.L2043
	.long	.L2044-.L2043
	.long	.L1987-.L2043
	.long	.L2042-.L2043
	.section	.text._ZN2v88internal6String4TrimEPNS0_7IsolateENS0_6HandleIS1_EENS1_8TrimModeE
	.p2align 4,,10
	.p2align 3
.L2026:
	movq	15(%rsi), %rdx
	movl	27(%rsi), %ebx
	movq	-1(%rdx), %rax
	addl	%r13d, %ebx
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L1987
	leaq	.L2034(%rip), %rcx
	movzwl	%ax, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6String4TrimEPNS0_7IsolateENS0_6HandleIS1_EENS1_8TrimModeE
	.align 4
	.align 4
.L2034:
	.long	.L2040-.L2034
	.long	.L2037-.L2034
	.long	.L2039-.L2034
	.long	.L2035-.L2034
	.long	.L1987-.L2034
	.long	.L2033-.L2034
	.long	.L1987-.L2034
	.long	.L1987-.L2034
	.long	.L2038-.L2034
	.long	.L2037-.L2034
	.long	.L2036-.L2034
	.long	.L2035-.L2034
	.long	.L1987-.L2034
	.long	.L2033-.L2034
	.section	.text._ZN2v88internal6String4TrimEPNS0_7IsolateENS0_6HandleIS1_EENS1_8TrimModeE
	.p2align 4,,10
	.p2align 3
.L2028:
	movq	%rsi, -64(%rbp)
	leaq	-64(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal10ConsString3GetEi
	.p2align 4,,10
	.p2align 3
.L2032:
	movzwl	%ax, %ebx
	cmpw	$127, %ax
	ja	.L2069
	movslq	%ebx, %rax
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rcx
	testb	$8, (%rcx,%rax)
	je	.L2060
.L2052:
	subq	$1, %r12
	cmpl	-88(%rbp), %r13d
	jle	.L2056
	movl	%r13d, %r14d
	movq	(%r15), %rsi
	subl	$1, %r13d
	jmp	.L2023
	.p2align 4,,10
	.p2align 3
.L2030:
	movq	15(%rsi), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzwl	(%rax,%r12,2), %eax
	jmp	.L2032
	.p2align 4,,10
	.p2align 3
.L2031:
	leal	16(%r13,%r13), %eax
	cltq
	movzwl	(%rax,%rdx), %eax
	jmp	.L2032
	.p2align 4,,10
	.p2align 3
.L2027:
	movq	15(%rsi), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzbl	(%rax,%r12), %eax
	jmp	.L2032
	.p2align 4,,10
	.p2align 3
.L2029:
	leal	15(%r14), %eax
	cltq
	movzbl	(%rax,%rdx), %eax
	jmp	.L2032
	.p2align 4,,10
	.p2align 3
.L2065:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L1968
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L1971
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L1971
	movq	%r15, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE
	movq	(%rax), %rsi
	movq	%rax, %r15
	jmp	.L1976
	.p2align 4,,10
	.p2align 3
.L2066:
	testl	%r14d, %r14d
	jle	.L1986
	leal	-1(%r14), %eax
	movl	%r14d, -96(%rbp)
	xorl	%ebx, %ebx
	movq	%rax, -88(%rbp)
	movq	%rbx, %r12
	movl	%eax, -108(%rbp)
	.p2align 4,,10
	.p2align 3
.L1985:
	movq	-1(%rsi), %rax
	movl	%r12d, %ebx
	movl	%r12d, %r13d
	leaq	-1(%rsi), %rdx
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L1987
	leaq	.L1989(%rip), %rdi
	movzwl	%ax, %eax
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6String4TrimEPNS0_7IsolateENS0_6HandleIS1_EENS1_8TrimModeE
	.align 4
	.align 4
.L1989:
	.long	.L1995-.L1989
	.long	.L1992-.L1989
	.long	.L1994-.L1989
	.long	.L1990-.L1989
	.long	.L1987-.L1989
	.long	.L1988-.L1989
	.long	.L1987-.L1989
	.long	.L1987-.L1989
	.long	.L1993-.L1989
	.long	.L1992-.L1989
	.long	.L1991-.L1989
	.long	.L1990-.L1989
	.long	.L1987-.L1989
	.long	.L1988-.L1989
	.section	.text._ZN2v88internal6String4TrimEPNS0_7IsolateENS0_6HandleIS1_EENS1_8TrimModeE
	.p2align 4,,10
	.p2align 3
.L1988:
	movq	15(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L1987
	leaq	.L2007(%rip), %rcx
	movzwl	%ax, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6String4TrimEPNS0_7IsolateENS0_6HandleIS1_EENS1_8TrimModeE
	.align 4
	.align 4
.L2007:
	.long	.L2013-.L2007
	.long	.L2010-.L2007
	.long	.L2012-.L2007
	.long	.L2008-.L2007
	.long	.L1987-.L2007
	.long	.L2006-.L2007
	.long	.L1987-.L2007
	.long	.L1987-.L2007
	.long	.L2011-.L2007
	.long	.L2010-.L2007
	.long	.L2009-.L2007
	.long	.L2008-.L2007
	.long	.L1987-.L2007
	.long	.L2006-.L2007
	.section	.text._ZN2v88internal6String4TrimEPNS0_7IsolateENS0_6HandleIS1_EENS1_8TrimModeE
	.p2align 4,,10
	.p2align 3
.L1990:
	movq	15(%rsi), %rdx
	movl	27(%rsi), %r14d
	movq	-1(%rdx), %rax
	addl	%r12d, %r14d
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L1987
	leaq	.L1998(%rip), %rcx
	movzwl	%ax, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6String4TrimEPNS0_7IsolateENS0_6HandleIS1_EENS1_8TrimModeE
	.align 4
	.align 4
.L1998:
	.long	.L2004-.L1998
	.long	.L2001-.L1998
	.long	.L2003-.L1998
	.long	.L1999-.L1998
	.long	.L1987-.L1998
	.long	.L1997-.L1998
	.long	.L1987-.L1998
	.long	.L1987-.L1998
	.long	.L2002-.L1998
	.long	.L2001-.L1998
	.long	.L2000-.L1998
	.long	.L1999-.L1998
	.long	.L1987-.L1998
	.long	.L1997-.L1998
	.section	.text._ZN2v88internal6String4TrimEPNS0_7IsolateENS0_6HandleIS1_EENS1_8TrimModeE
	.p2align 4,,10
	.p2align 3
.L1992:
	movq	%rsi, -64(%rbp)
	leaq	-64(%rbp), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal10ConsString3GetEi
	.p2align 4,,10
	.p2align 3
.L1996:
	movzwl	%ax, %r14d
	cmpw	$127, %ax
	ja	.L2070
.L2015:
	movslq	%r14d, %rdx
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rax
	testb	$8, (%rax,%rdx)
	je	.L2059
.L2019:
	leal	1(%rbx), %r13d
	leaq	1(%r12), %rax
	cmpq	%r12, -88(%rbp)
	je	.L2071
	movq	(%r15), %rsi
	movq	%rax, %r12
	jmp	.L1985
	.p2align 4,,10
	.p2align 3
.L1994:
	movq	15(%rsi), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzwl	(%rax,%r12,2), %eax
	movzwl	%ax, %r14d
	cmpw	$127, %ax
	jbe	.L2015
	.p2align 4,,10
	.p2align 3
.L2070:
	movl	%r14d, %edi
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	testb	%al, %al
	jne	.L2019
	leal	-8232(%r14), %edx
	cmpl	$1, %edx
	jbe	.L2019
.L2059:
	movl	-92(%rbp), %eax
	movl	%r13d, %r12d
	movl	-96(%rbp), %r14d
	movl	-108(%rbp), %r13d
	testl	%eax, %eax
	jne	.L2021
	cmpl	%r12d, %r14d
	jle	.L2018
	movq	(%r15), %rsi
	jmp	.L2054
	.p2align 4,,10
	.p2align 3
.L1995:
	leal	16(%r12,%r12), %eax
	cltq
	movzwl	(%rax,%rdx), %eax
	jmp	.L1996
	.p2align 4,,10
	.p2align 3
.L1991:
	movq	15(%rsi), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzbl	(%rax,%r12), %eax
	jmp	.L1996
	.p2align 4,,10
	.p2align 3
.L1993:
	leal	16(%r12), %eax
	cltq
	movzbl	(%rax,%rdx), %eax
	jmp	.L1996
	.p2align 4,,10
	.p2align 3
.L2067:
	movq	(%r15), %rax
	cmpl	11(%rax), %r14d
	jne	.L2018
	jmp	.L1986
	.p2align 4,,10
	.p2align 3
.L1980:
	movq	41088(%rax), %r15
	cmpq	41096(%rax), %r15
	je	.L2072
.L1982:
	movq	-104(%rbp), %rbx
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r15)
	jmp	.L1976
	.p2align 4,,10
	.p2align 3
.L2042:
	movq	15(%rdx), %rax
	movl	%r13d, -72(%rbp)
.L2064:
	movq	-1(%rax), %rdx
	leaq	-64(%rbp), %rsi
	leaq	-68(%rbp), %rdi
	movzwl	11(%rdx), %edx
	movq	%rax, -64(%rbp)
	movl	%edx, -68(%rbp)
	leaq	-72(%rbp), %rdx
	call	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
	jmp	.L2032
	.p2align 4,,10
	.p2align 3
.L2044:
	movl	27(%rdx), %ebx
	movq	15(%rdx), %rax
	addl	%r13d, %ebx
	movl	%ebx, -72(%rbp)
	jmp	.L2064
	.p2align 4,,10
	.p2align 3
.L2046:
	leaq	-64(%rbp), %rdi
	movl	%r13d, %esi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal10ConsString3GetEi
	jmp	.L2032
	.p2align 4,,10
	.p2align 3
.L2033:
	movq	15(%rdx), %rax
	movl	%ebx, -72(%rbp)
	jmp	.L2064
	.p2align 4,,10
	.p2align 3
.L2035:
	addl	27(%rdx), %ebx
	movq	15(%rdx), %rax
	movl	%ebx, -72(%rbp)
	jmp	.L2064
	.p2align 4,,10
	.p2align 3
.L2037:
	leaq	-64(%rbp), %rdi
	movl	%ebx, %esi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal10ConsString3GetEi
	jmp	.L2032
	.p2align 4,,10
	.p2align 3
.L1997:
	movq	15(%rdx), %rax
	movl	%r14d, -72(%rbp)
.L2063:
	movq	-1(%rax), %rdx
	leaq	-64(%rbp), %rsi
	leaq	-68(%rbp), %rdi
	movzwl	11(%rdx), %edx
	movq	%rax, -64(%rbp)
	movl	%edx, -68(%rbp)
	leaq	-72(%rbp), %rdx
	call	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
	jmp	.L1996
	.p2align 4,,10
	.p2align 3
.L1999:
	movl	27(%rdx), %esi
	movq	15(%rdx), %rax
	addl	%r14d, %esi
	movl	%esi, -72(%rbp)
	jmp	.L2063
	.p2align 4,,10
	.p2align 3
.L2001:
	leaq	-64(%rbp), %rdi
	movl	%r14d, %esi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal10ConsString3GetEi
	jmp	.L1996
	.p2align 4,,10
	.p2align 3
.L2006:
	movq	15(%rdx), %rax
	movl	%r12d, -72(%rbp)
	jmp	.L2063
	.p2align 4,,10
	.p2align 3
.L2008:
	movl	27(%rdx), %ecx
	movq	15(%rdx), %rax
	addl	%r12d, %ecx
	movl	%ecx, -72(%rbp)
	jmp	.L2063
	.p2align 4,,10
	.p2align 3
.L2010:
	leaq	-64(%rbp), %rdi
	movl	%r12d, %esi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal10ConsString3GetEi
	jmp	.L1996
.L2049:
	leal	16(%r13,%r13), %eax
	cltq
	movzwl	-1(%rdx,%rax), %eax
	jmp	.L2032
.L2039:
	movq	15(%rdx), %rdi
	movslq	%ebx, %rbx
	movq	(%rdi), %rax
	call	*48(%rax)
	movzwl	(%rax,%rbx,2), %eax
	jmp	.L2032
.L2040:
	leal	16(%rbx,%rbx), %eax
	cltq
	movzwl	-1(%rdx,%rax), %eax
	jmp	.L2032
.L2045:
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzbl	(%rax,%r12), %eax
	jmp	.L2032
.L2047:
	leal	15(%r14), %eax
	cltq
	movzbl	-1(%rdx,%rax), %eax
	jmp	.L2032
.L2048:
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzwl	(%rax,%r12,2), %eax
	jmp	.L2032
.L2036:
	movq	15(%rdx), %rdi
	movslq	%ebx, %rbx
	movq	(%rdi), %rax
	call	*48(%rax)
	movzbl	(%rax,%rbx), %eax
	jmp	.L2032
.L2038:
	addl	$16, %ebx
	movslq	%ebx, %rbx
	movzbl	-1(%rdx,%rbx), %eax
	jmp	.L2032
.L2012:
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzwl	(%rax,%r12,2), %eax
	jmp	.L1996
.L2013:
	leal	16(%r12,%r12), %eax
	cltq
	movzwl	-1(%rdx,%rax), %eax
	jmp	.L1996
.L2000:
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	%r14d, %rsi
	movzbl	(%rax,%rsi), %eax
	jmp	.L1996
.L2002:
	leal	16(%r14), %eax
	cltq
	movzbl	-1(%rdx,%rax), %eax
	jmp	.L1996
.L2003:
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	%r14d, %rsi
	movzwl	(%rax,%rsi,2), %eax
	jmp	.L1996
.L2011:
	leal	16(%r12), %eax
	cltq
	movzbl	-1(%rdx,%rax), %eax
	jmp	.L1996
.L2004:
	leal	16(%r14,%r14), %eax
	cltq
	movzwl	-1(%rdx,%rax), %eax
	jmp	.L1996
.L2009:
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzbl	(%rax,%r12), %eax
	jmp	.L1996
	.p2align 4,,10
	.p2align 3
.L2071:
	movl	-96(%rbp), %r14d
	movl	%r13d, %r12d
	jmp	.L2018
	.p2align 4,,10
	.p2align 3
.L2056:
	movl	-88(%rbp), %r12d
	movl	%r13d, %r14d
	jmp	.L2021
	.p2align 4,,10
	.p2align 3
.L1971:
	movq	15(%rax), %rsi
	movq	-104(%rbp), %rax
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1973
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
	jmp	.L1968
	.p2align 4,,10
	.p2align 3
.L1973:
	movq	41088(%rax), %r15
	cmpq	41096(%rax), %r15
	je	.L2073
.L1975:
	movq	-104(%rbp), %rbx
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r15)
	jmp	.L1968
	.p2align 4,,10
	.p2align 3
.L1987:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2073:
	movq	%rax, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1975
.L2072:
	movq	%rax, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1982
.L2068:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19668:
	.size	_ZN2v88internal6String4TrimEPNS0_7IsolateENS0_6HandleIS1_EENS1_8TrimModeE, .-_ZN2v88internal6String4TrimEPNS0_7IsolateENS0_6HandleIS1_EENS1_8TrimModeE
	.section	.rodata._ZN2v88internal6String7PrintOnEP8_IO_FILE.str1.1,"aMS",@progbits,1
.LC19:
	.string	"%c"
	.section	.text._ZN2v88internal6String7PrintOnEP8_IO_FILE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String7PrintOnEP8_IO_FILE
	.type	_ZN2v88internal6String7PrintOnEP8_IO_FILE, @function
_ZN2v88internal6String7PrintOnEP8_IO_FILE:
.LFB19715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	11(%rax), %edx
	testl	%edx, %edx
	jle	.L2074
	leal	-1(%rdx), %r14d
	movq	%rdi, %r12
	movq	%rsi, %r13
	xorl	%r15d, %r15d
	movq	%rax, %rdx
	leaq	.L2078(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L2140:
	movq	-1(%rax), %rax
	movl	%r15d, %edi
	movl	%r15d, %esi
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L2076
	movzwl	%ax, %eax
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6String7PrintOnEP8_IO_FILE,"a",@progbits
	.align 4
	.align 4
.L2078:
	.long	.L2138-.L2078
	.long	.L2135-.L2078
	.long	.L2137-.L2078
	.long	.L2079-.L2078
	.long	.L2076-.L2078
	.long	.L2077-.L2078
	.long	.L2076-.L2078
	.long	.L2076-.L2078
	.long	.L2118-.L2078
	.long	.L2135-.L2078
	.long	.L2134-.L2078
	.long	.L2079-.L2078
	.long	.L2076-.L2078
	.long	.L2077-.L2078
	.section	.text._ZN2v88internal6String7PrintOnEP8_IO_FILE
	.p2align 4,,10
	.p2align 3
.L2135:
	leaq	-64(%rbp), %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal10ConsString3GetEi
	.p2align 4,,10
	.p2align 3
.L2085:
	movzwl	%ax, %edx
	leaq	.LC19(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	leaq	1(%r15), %rsi
	cmpq	%r14, %r15
	je	.L2074
	movq	(%r12), %rdx
	movq	%rsi, %r15
	movq	%rdx, %rax
	jmp	.L2140
	.p2align 4,,10
	.p2align 3
.L2077:
	movq	15(%rdx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L2076
	leaq	.L2114(%rip), %rcx
	movzwl	%ax, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6String7PrintOnEP8_IO_FILE
	.align 4
	.align 4
.L2114:
	.long	.L2138-.L2114
	.long	.L2135-.L2114
	.long	.L2137-.L2114
	.long	.L2115-.L2114
	.long	.L2076-.L2114
	.long	.L2113-.L2114
	.long	.L2076-.L2114
	.long	.L2076-.L2114
	.long	.L2118-.L2114
	.long	.L2135-.L2114
	.long	.L2134-.L2114
	.long	.L2115-.L2114
	.long	.L2076-.L2114
	.long	.L2113-.L2114
	.section	.text._ZN2v88internal6String7PrintOnEP8_IO_FILE
	.p2align 4,,10
	.p2align 3
.L2079:
	movq	15(%rdx), %rdi
	addl	27(%rdx), %esi
	movq	-1(%rdi), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L2076
	leaq	.L2087(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6String7PrintOnEP8_IO_FILE
	.align 4
	.align 4
.L2087:
	.long	.L2129-.L2087
	.long	.L2126-.L2087
	.long	.L2128-.L2087
	.long	.L2088-.L2087
	.long	.L2076-.L2087
	.long	.L2086-.L2087
	.long	.L2076-.L2087
	.long	.L2076-.L2087
	.long	.L2091-.L2087
	.long	.L2126-.L2087
	.long	.L2125-.L2087
	.long	.L2088-.L2087
	.long	.L2076-.L2087
	.long	.L2086-.L2087
	.section	.text._ZN2v88internal6String7PrintOnEP8_IO_FILE
	.p2align 4,,10
	.p2align 3
.L2137:
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzwl	(%rax,%r15,2), %eax
	jmp	.L2085
	.p2align 4,,10
	.p2align 3
.L2134:
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzbl	(%rax,%r15), %eax
	jmp	.L2085
	.p2align 4,,10
	.p2align 3
.L2138:
	leal	16(%rdi,%rdi), %eax
	cltq
	movzwl	-1(%rdx,%rax), %eax
	jmp	.L2085
	.p2align 4,,10
	.p2align 3
.L2118:
	addl	$16, %edi
	movslq	%edi, %rdi
	movzbl	-1(%rdx,%rdi), %eax
	jmp	.L2085
	.p2align 4,,10
	.p2align 3
.L2074:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2144
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2126:
	.cfi_restore_state
	movq	%rdi, -64(%rbp)
	leaq	-64(%rbp), %rdi
	call	_ZN2v88internal10ConsString3GetEi
	jmp	.L2085
	.p2align 4,,10
	.p2align 3
.L2113:
	movq	15(%rdx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L2076
	leaq	.L2132(%rip), %rcx
	movzwl	%ax, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6String7PrintOnEP8_IO_FILE
	.align 4
	.align 4
.L2132:
	.long	.L2138-.L2132
	.long	.L2135-.L2132
	.long	.L2137-.L2132
	.long	.L2133-.L2132
	.long	.L2076-.L2132
	.long	.L2131-.L2132
	.long	.L2076-.L2132
	.long	.L2076-.L2132
	.long	.L2136-.L2132
	.long	.L2135-.L2132
	.long	.L2134-.L2132
	.long	.L2133-.L2132
	.long	.L2076-.L2132
	.long	.L2131-.L2132
	.section	.text._ZN2v88internal6String7PrintOnEP8_IO_FILE
	.p2align 4,,10
	.p2align 3
.L2086:
	movq	15(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L2076
	leaq	.L2105(%rip), %rdi
	movzwl	%ax, %eax
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6String7PrintOnEP8_IO_FILE
	.align 4
	.align 4
.L2105:
	.long	.L2111-.L2105
	.long	.L2135-.L2105
	.long	.L2110-.L2105
	.long	.L2133-.L2105
	.long	.L2076-.L2105
	.long	.L2131-.L2105
	.long	.L2076-.L2105
	.long	.L2076-.L2105
	.long	.L2109-.L2105
	.long	.L2135-.L2105
	.long	.L2107-.L2105
	.long	.L2133-.L2105
	.long	.L2076-.L2105
	.long	.L2131-.L2105
	.section	.text._ZN2v88internal6String7PrintOnEP8_IO_FILE
	.p2align 4,,10
	.p2align 3
.L2115:
	movq	15(%rdx), %rdi
	addl	27(%rdx), %esi
	movq	-1(%rdi), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L2076
	leaq	.L2123(%rip), %rcx
	movzwl	%ax, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6String7PrintOnEP8_IO_FILE
	.align 4
	.align 4
.L2123:
	.long	.L2129-.L2123
	.long	.L2126-.L2123
	.long	.L2128-.L2123
	.long	.L2124-.L2123
	.long	.L2076-.L2123
	.long	.L2122-.L2123
	.long	.L2076-.L2123
	.long	.L2076-.L2123
	.long	.L2127-.L2123
	.long	.L2126-.L2123
	.long	.L2125-.L2123
	.long	.L2124-.L2123
	.long	.L2076-.L2123
	.long	.L2122-.L2123
	.section	.text._ZN2v88internal6String7PrintOnEP8_IO_FILE
	.p2align 4,,10
	.p2align 3
.L2088:
	movq	15(%rdi), %rdx
	addl	27(%rdi), %esi
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L2076
	leaq	.L2096(%rip), %rdi
	movzwl	%ax, %eax
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6String7PrintOnEP8_IO_FILE
	.align 4
	.align 4
.L2096:
	.long	.L2111-.L2096
	.long	.L2135-.L2096
	.long	.L2110-.L2096
	.long	.L2133-.L2096
	.long	.L2076-.L2096
	.long	.L2131-.L2096
	.long	.L2076-.L2096
	.long	.L2076-.L2096
	.long	.L2109-.L2096
	.long	.L2135-.L2096
	.long	.L2107-.L2096
	.long	.L2133-.L2096
	.long	.L2076-.L2096
	.long	.L2131-.L2096
	.section	.text._ZN2v88internal6String7PrintOnEP8_IO_FILE
	.p2align 4,,10
	.p2align 3
.L2128:
	movl	%esi, -84(%rbp)
	movq	15(%rdi), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-84(%rbp), %rsi
	movzwl	(%rax,%rsi,2), %eax
	jmp	.L2085
	.p2align 4,,10
	.p2align 3
.L2129:
	leal	16(%rsi,%rsi), %eax
	cltq
	movzwl	-1(%rdi,%rax), %eax
	jmp	.L2085
	.p2align 4,,10
	.p2align 3
.L2125:
	movl	%esi, -84(%rbp)
	movq	15(%rdi), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-84(%rbp), %rsi
	movzbl	(%rax,%rsi), %eax
	jmp	.L2085
.L2091:
	addl	$16, %esi
	movslq	%esi, %rsi
	movzbl	-1(%rdi,%rsi), %eax
	jmp	.L2085
.L2131:
	movq	15(%rdx), %rax
.L2143:
	movl	%esi, -72(%rbp)
	movq	-1(%rax), %rdx
	leaq	-64(%rbp), %rsi
	leaq	-68(%rbp), %rdi
	movzwl	11(%rdx), %edx
	movq	%rax, -64(%rbp)
	movl	%edx, -68(%rbp)
	leaq	-72(%rbp), %rdx
	call	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
	jmp	.L2085
.L2133:
	movq	15(%rdx), %rax
	addl	27(%rdx), %esi
	jmp	.L2143
	.p2align 4,,10
	.p2align 3
.L2076:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2122:
	movq	15(%rdi), %rax
	jmp	.L2143
.L2124:
	movq	15(%rdi), %rax
	addl	27(%rdi), %esi
	jmp	.L2143
.L2107:
	movl	%esi, -84(%rbp)
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-84(%rbp), %rsi
	movzbl	(%rax,%rsi), %eax
	jmp	.L2085
.L2109:
	addl	$16, %esi
	movslq	%esi, %rax
	movzbl	-1(%rdx,%rax), %eax
	jmp	.L2085
.L2110:
	movl	%esi, -84(%rbp)
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-84(%rbp), %rsi
	movzwl	(%rax,%rsi,2), %eax
	jmp	.L2085
.L2111:
	leal	16(%rsi,%rsi), %eax
	cltq
	movzwl	-1(%rdx,%rax), %eax
	jmp	.L2085
.L2136:
	leal	16(%r15), %eax
	cltq
	movzbl	-1(%rdx,%rax), %eax
	jmp	.L2085
.L2127:
	addl	$16, %esi
	movslq	%esi, %rax
	movzbl	-1(%rdi,%rax), %eax
	jmp	.L2085
.L2144:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19715:
	.size	_ZN2v88internal6String7PrintOnEP8_IO_FILE, .-_ZN2v88internal6String7PrintOnEP8_IO_FILE
	.section	.rodata._ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_.str1.1,"aMS",@progbits,1
.LC20:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB23184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movabsq	$1152921504606846975, %rsi
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rcx
	movq	(%rdi), %r12
	movq	%rcx, %rax
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L2172
	movq	%r13, %r9
	movq	%rdi, %r15
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L2157
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L2173
.L2147:
	movq	%r14, %rdi
	movq	%rdx, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r9
	movq	%rax, %rbx
	leaq	(%rax,%r14), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	8(%rbx), %r14
.L2156:
	movq	(%rdx), %rax
	movq	%rax, (%rbx,%r9)
	cmpq	%r12, %r13
	je	.L2149
	leaq	-8(%r13), %rsi
	leaq	15(%rbx), %rax
	subq	%r12, %rsi
	subq	%r12, %rax
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rax
	jbe	.L2159
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdi
	je	.L2159
	addq	$1, %rdi
	xorl	%eax, %eax
	movq	%rdi, %rdx
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L2151:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L2151
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rdx
	leaq	(%r12,%rdx), %rax
	addq	%rbx, %rdx
	cmpq	%r8, %rdi
	je	.L2153
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L2153:
	leaq	16(%rbx,%rsi), %r14
.L2149:
	cmpq	%rcx, %r13
	je	.L2154
	subq	%r13, %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rcx, %rdx
	movq	%rcx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rcx
	addq	%rcx, %r14
.L2154:
	testq	%r12, %r12
	je	.L2155
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2155:
	movq	-56(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r14, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2173:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L2148
	movq	$0, -56(%rbp)
	movl	$8, %r14d
	xorl	%ebx, %ebx
	jmp	.L2156
	.p2align 4,,10
	.p2align 3
.L2157:
	movl	$8, %r14d
	jmp	.L2147
	.p2align 4,,10
	.p2align 3
.L2159:
	movq	%rbx, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L2150:
	movq	(%rax), %rdi
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rdi, -8(%rdx)
	cmpq	%rax, %r13
	jne	.L2150
	jmp	.L2153
.L2148:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	movq	%rsi, %r14
	salq	$3, %r14
	jmp	.L2147
.L2172:
	leaq	.LC20(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23184:
	.size	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.text._ZN2v88internal6String12MakeExternalEPNS_6String29ExternalOneByteStringResourceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String12MakeExternalEPNS_6String29ExternalOneByteStringResourceE
	.type	_ZN2v88internal6String12MakeExternalEPNS_6String29ExternalOneByteStringResourceE, @function
_ZN2v88internal6String12MakeExternalEPNS_6String29ExternalOneByteStringResourceE:
.LFB19664:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movl	%eax, %edx
	xorl	%eax, %eax
	cmpl	$23, %edx
	jle	.L2174
	movq	(%r15), %rsi
	movq	%rsi, %rcx
	andq	$-262144, %rcx
	testb	$32, 10(%rcx)
	je	.L2209
.L2174:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2210
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2209:
	.cfi_restore_state
	movq	24(%rcx), %rax
	movq	%rax, -96(%rbp)
	leaq	-37592(%rax), %rbx
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %r13d
	andl	$-32, %r13d
	movl	%r13d, -84(%rbp)
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %r14d
	andl	$1, %r14d
	jne	.L2211
.L2176:
	cmpl	$31, %edx
	jle	.L2212
	movl	-84(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L2180
	movq	840(%rbx), %r13
.L2179:
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	%edx, -88(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	testb	%r14b, %r14b
	movl	-88(%rbp), %edx
	je	.L2213
	movslq	%eax, %r14
	subl	%eax, %edx
	movq	(%r15), %rax
	movq	-96(%rbp), %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	leaq	-1(%r14,%rax), %rsi
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	(%r15), %rax
	movq	-96(%rbp), %rdi
	leaq	-1(%rax), %rsi
	leaq	(%rsi,%r14), %rdx
	call	_ZN2v88internal4Heap22ClearRecordedSlotRangeEmm@PLT
.L2196:
	movq	(%r15), %rax
	movq	%r13, -1(%rax)
	movq	(%r15), %r14
	testq	%r13, %r13
	jne	.L2183
.L2184:
	movq	%r12, 15(%r14)
	testq	%r12, %r12
	je	.L2188
	movq	-1(%r14), %rax
	testb	$16, 11(%rax)
	jne	.L2189
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*48(%rax)
	movq	%rax, 23(%r14)
.L2189:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*56(%rax)
	testq	%rax, %rax
	jne	.L2214
.L2188:
	movq	(%r15), %rax
	movq	%rax, %rdx
	movq	%rax, -64(%rbp)
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L2190
	movq	40376(%rbx), %rsi
	cmpq	40384(%rbx), %rsi
	je	.L2191
	movq	%rax, (%rsi)
	addq	$8, 40376(%rbx)
.L2192:
	movl	-84(%rbp), %edx
	movl	$1, %eax
	testl	%edx, %edx
	jne	.L2174
	movl	7(%r14), %eax
	notl	%eax
	andl	$1, %eax
	jne	.L2174
	leaq	-64(%rbp), %rdi
	movq	%r14, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv
	movl	$1, %eax
	jmp	.L2174
	.p2align 4,,10
	.p2align 3
.L2212:
	movl	-84(%rbp), %esi
	testl	%esi, %esi
	jne	.L2178
	movq	856(%rbx), %r13
	jmp	.L2179
	.p2align 4,,10
	.p2align 3
.L2213:
	movq	(%r15), %rcx
	subl	%eax, %edx
	movq	-96(%rbp), %rdi
	cltq
	movl	$1, %r8d
	leaq	-1(%rcx,%rax), %rsi
	movl	$1, %ecx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	jmp	.L2196
	.p2align 4,,10
	.p2align 3
.L2190:
	movq	40400(%rbx), %rsi
	cmpq	40408(%rbx), %rsi
	je	.L2193
	movq	%rax, (%rsi)
	addq	$8, 40400(%rbx)
	jmp	.L2192
	.p2align 4,,10
	.p2align 3
.L2183:
	testb	$1, %r13b
	je	.L2184
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2184
	movq	%r14, %rdi
	movq	%r13, %rdx
	xorl	%esi, %esi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%r15), %r14
	jmp	.L2184
	.p2align 4,,10
	.p2align 3
.L2211:
	movq	-96(%rbp), %rdi
	leaq	-65(%rbp), %rcx
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal4Heap24NotifyObjectLayoutChangeENS0_10HeapObjectEiRKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movl	-88(%rbp), %edx
	jmp	.L2176
	.p2align 4,,10
	.p2align 3
.L2178:
	movq	864(%rbx), %r13
	jmp	.L2179
	.p2align 4,,10
	.p2align 3
.L2180:
	movq	808(%rbx), %r13
	jmp	.L2179
	.p2align 4,,10
	.p2align 3
.L2214:
	movq	-96(%rbp), %rdi
	movq	%rax, %rcx
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal4Heap20UpdateExternalStringENS0_6StringEmm@PLT
	jmp	.L2188
	.p2align 4,,10
	.p2align 3
.L2193:
	leaq	-64(%rbp), %rdx
	leaq	40392(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L2192
	.p2align 4,,10
	.p2align 3
.L2191:
	leaq	-64(%rbp), %rdx
	leaq	40368(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L2192
.L2210:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19664:
	.size	_ZN2v88internal6String12MakeExternalEPNS_6String29ExternalOneByteStringResourceE, .-_ZN2v88internal6String12MakeExternalEPNS_6String29ExternalOneByteStringResourceE
	.section	.text._ZN2v88internal6String12MakeExternalEPNS_6String22ExternalStringResourceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String12MakeExternalEPNS_6String22ExternalStringResourceE
	.type	_ZN2v88internal6String12MakeExternalEPNS_6String22ExternalStringResourceE, @function
_ZN2v88internal6String12MakeExternalEPNS_6String22ExternalStringResourceE:
.LFB19663:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movl	%eax, %edx
	xorl	%eax, %eax
	cmpl	$23, %edx
	jle	.L2215
	movq	(%r15), %rsi
	movq	%rsi, %rcx
	andq	$-262144, %rcx
	testb	$32, 10(%rcx)
	je	.L2250
.L2215:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2251
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2250:
	.cfi_restore_state
	movq	24(%rcx), %rax
	movq	%rax, -96(%rbp)
	leaq	-37592(%rax), %rbx
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %r13d
	andl	$-32, %r13d
	movl	%r13d, -84(%rbp)
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %r14d
	andl	$1, %r14d
	jne	.L2252
.L2217:
	cmpl	$31, %edx
	jle	.L2253
	movl	-84(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L2221
	movq	832(%rbx), %r13
.L2220:
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	%edx, -88(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	testb	%r14b, %r14b
	movl	-88(%rbp), %edx
	je	.L2254
	movslq	%eax, %r14
	subl	%eax, %edx
	movq	(%r15), %rax
	movq	-96(%rbp), %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	leaq	-1(%r14,%rax), %rsi
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	(%r15), %rax
	movq	-96(%rbp), %rdi
	leaq	-1(%rax), %rsi
	leaq	(%rsi,%r14), %rdx
	call	_ZN2v88internal4Heap22ClearRecordedSlotRangeEmm@PLT
.L2237:
	movq	(%r15), %rax
	movq	%r13, -1(%rax)
	movq	(%r15), %r14
	testq	%r13, %r13
	jne	.L2224
.L2225:
	movq	%r12, 15(%r14)
	testq	%r12, %r12
	je	.L2229
	movq	-1(%r14), %rax
	testb	$16, 11(%rax)
	jne	.L2230
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*48(%rax)
	movq	%rax, 23(%r14)
.L2230:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*56(%rax)
	addq	%rax, %rax
	jne	.L2255
.L2229:
	movq	(%r15), %rax
	movq	%rax, %rdx
	movq	%rax, -64(%rbp)
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L2231
	movq	40376(%rbx), %rsi
	cmpq	40384(%rbx), %rsi
	je	.L2232
	movq	%rax, (%rsi)
	addq	$8, 40376(%rbx)
.L2233:
	movl	-84(%rbp), %edx
	movl	$1, %eax
	testl	%edx, %edx
	jne	.L2215
	movl	7(%r14), %eax
	notl	%eax
	andl	$1, %eax
	jne	.L2215
	leaq	-64(%rbp), %rdi
	movq	%r14, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv
	movl	$1, %eax
	jmp	.L2215
	.p2align 4,,10
	.p2align 3
.L2253:
	movl	-84(%rbp), %esi
	testl	%esi, %esi
	jne	.L2219
	movq	848(%rbx), %r13
	jmp	.L2220
	.p2align 4,,10
	.p2align 3
.L2254:
	movq	(%r15), %rcx
	subl	%eax, %edx
	movq	-96(%rbp), %rdi
	cltq
	movl	$1, %r8d
	leaq	-1(%rcx,%rax), %rsi
	movl	$1, %ecx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	jmp	.L2237
	.p2align 4,,10
	.p2align 3
.L2231:
	movq	40400(%rbx), %rsi
	cmpq	40408(%rbx), %rsi
	je	.L2234
	movq	%rax, (%rsi)
	addq	$8, 40400(%rbx)
	jmp	.L2233
	.p2align 4,,10
	.p2align 3
.L2224:
	testb	$1, %r13b
	je	.L2225
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2225
	movq	%r14, %rdi
	movq	%r13, %rdx
	xorl	%esi, %esi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%r15), %r14
	jmp	.L2225
	.p2align 4,,10
	.p2align 3
.L2252:
	movq	-96(%rbp), %rdi
	leaq	-65(%rbp), %rcx
	movl	%edx, -88(%rbp)
	call	_ZN2v88internal4Heap24NotifyObjectLayoutChangeENS0_10HeapObjectEiRKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movl	-88(%rbp), %edx
	jmp	.L2217
	.p2align 4,,10
	.p2align 3
.L2219:
	movq	816(%rbx), %r13
	jmp	.L2220
	.p2align 4,,10
	.p2align 3
.L2221:
	movq	800(%rbx), %r13
	jmp	.L2220
	.p2align 4,,10
	.p2align 3
.L2255:
	movq	-96(%rbp), %rdi
	movq	%rax, %rcx
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal4Heap20UpdateExternalStringENS0_6StringEmm@PLT
	jmp	.L2229
	.p2align 4,,10
	.p2align 3
.L2234:
	leaq	-64(%rbp), %rdx
	leaq	40392(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L2233
	.p2align 4,,10
	.p2align 3
.L2232:
	leaq	-64(%rbp), %rdx
	leaq	40368(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L2233
.L2251:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19663:
	.size	_ZN2v88internal6String12MakeExternalEPNS_6String22ExternalStringResourceE, .-_ZN2v88internal6String12MakeExternalEPNS_6String22ExternalStringResourceE
	.section	.text._ZN2v88internal12SearchStringIhtEEiPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEi,"axG",@progbits,_ZN2v88internal12SearchStringIhtEEiPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEi,comdat
	.p2align 4
	.weak	_ZN2v88internal12SearchStringIhtEEiPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEi
	.type	_ZN2v88internal12SearchStringIhtEEiPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEi, @function
_ZN2v88internal12SearchStringIhtEEiPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEi:
.LFB23264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leal	-250(%r8), %eax
	movq	%rdi, -48(%rbp)
	movl	$0, %edi
	testl	%eax, %eax
	movq	%rcx, -40(%rbp)
	cmovs	%edi, %eax
	movq	%r8, -32(%rbp)
	movl	%eax, -16(%rbp)
	movslq	%r8d, %rax
	cmpq	$7, %rax
	leaq	(%rcx,%rax,2), %rdi
	movq	%rcx, %rax
	jbe	.L2282
	testb	$7, %cl
	jne	.L2258
	.p2align 4,,10
	.p2align 3
.L2262:
	leaq	16(%rax), %r10
	cmpq	%r10, %rdi
	jb	.L2282
	leaq	-16(%rdi), %r10
	subq	%rax, %r10
	shrq	$3, %r10
	leaq	8(%rax,%r10,8), %r11
	movabsq	$-71777214294589696, %r10
	jmp	.L2266
	.p2align 4,,10
	.p2align 3
.L2263:
	addq	$8, %rax
	cmpq	%rax, %r11
	je	.L2282
.L2266:
	testq	%r10, (%rax)
	je	.L2263
	cmpq	%rax, %rdi
	jbe	.L2264
	.p2align 4,,10
	.p2align 3
.L2265:
	cmpw	$255, (%rax)
	ja	.L2264
	addq	$2, %rax
.L2282:
	cmpq	%rax, %rdi
	ja	.L2265
.L2264:
	subq	%rcx, %rax
	sarq	%rax
	cmpl	%eax, %r8d
	jg	.L2283
.L2268:
	cmpl	$6, %r8d
	jle	.L2284
	leaq	_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -24(%rbp)
.L2269:
	leaq	-48(%rbp), %rdi
	movl	%r9d, %ecx
	call	*%rax
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L2285
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2260:
	.cfi_restore_state
	addq	$2, %rax
	testb	$7, %al
	je	.L2262
.L2258:
	cmpw	$255, (%rax)
	jbe	.L2260
	subq	%rcx, %rax
	sarq	%rax
	cmpl	%eax, %r8d
	jle	.L2268
.L2283:
	leaq	_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -24(%rbp)
	jmp	.L2269
	.p2align 4,,10
	.p2align 3
.L2284:
	cmpl	$1, %r8d
	je	.L2286
	leaq	_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -24(%rbp)
	jmp	.L2269
	.p2align 4,,10
	.p2align 3
.L2286:
	leaq	_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -24(%rbp)
	jmp	.L2269
.L2285:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23264:
	.size	_ZN2v88internal12SearchStringIhtEEiPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEi, .-_ZN2v88internal12SearchStringIhtEEiPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEi
	.section	.text._ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleIS1_EES5_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleIS1_EES5_i
	.type	_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleIS1_EES5_i, @function
_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleIS1_EES5_i:
.LFB19699:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movl	11(%rax), %edx
	movl	%ecx, %eax
	testl	%edx, %edx
	je	.L2288
	movl	%ecx, %r15d
	movq	(%rsi), %rcx
	movq	%rsi, %r12
	addl	%eax, %edx
	cmpl	11(%rcx), %edx
	ja	.L2335
	movq	-1(%rcx), %rax
	movq	%rdi, %r13
	movq	%rcx, %rsi
	cmpw	$63, 11(%rax)
	jbe	.L2338
.L2290:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	jbe	.L2339
.L2298:
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	movq	%rax, %rsi
	cmpw	$63, 11(%rdx)
	jbe	.L2340
.L2305:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	jbe	.L2341
.L2313:
	movq	(%r12), %rax
	leaq	-96(%rbp), %r14
	leaq	-97(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rsi, -128(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	movq	-128(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	movq	(%rbx), %rax
	movq	%rdx, -120(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	movq	-120(%rbp), %r10
	movq	%rax, -128(%rbp)
	movq	%rdx, %rax
	shrq	$32, %rax
	cmpl	$1, %eax
	je	.L2342
	movq	%r10, %rax
	movslq	%edx, %r8
	movslq	%r10d, %r10
	shrq	$32, %rax
	cmpl	$1, %eax
	je	.L2343
	leal	-250(%rdx), %eax
	movl	$0, %ecx
	movq	%r13, %xmm0
	movq	%r8, -80(%rbp)
	testl	%eax, %eax
	movhps	-128(%rbp), %xmm0
	cmovs	%ecx, %eax
	movaps	%xmm0, -96(%rbp)
	movl	%eax, -64(%rbp)
	cmpl	$6, %edx
	jg	.L2330
	cmpl	$1, %edx
	je	.L2344
	leaq	_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L2332
	.p2align 4,,10
	.p2align 3
.L2330:
	leaq	_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -72(%rbp)
.L2332:
	movl	%r15d, %ecx
	movq	%r12, %rsi
	movq	%r10, %rdx
	movq	%r14, %rdi
	call	*%rax
.L2288:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2345
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2340:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L2305
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L2308
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L2308
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE
	movq	%rax, %rbx
	jmp	.L2313
	.p2align 4,,10
	.p2align 3
.L2338:
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L2290
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L2293
	movq	23(%rcx), %rax
	movl	11(%rax), %esi
	testl	%esi, %esi
	je	.L2293
	movq	%r12, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE
	movq	%rax, %r12
	jmp	.L2298
	.p2align 4,,10
	.p2align 3
.L2339:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L2298
	movq	(%r12), %rax
	movq	41112(%r13), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2301
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
	jmp	.L2298
	.p2align 4,,10
	.p2align 3
.L2341:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L2313
	movq	(%rbx), %rax
	movq	41112(%r13), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2316
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
	jmp	.L2313
	.p2align 4,,10
	.p2align 3
.L2342:
	movl	%edx, %eax
	movl	$0, %esi
	movq	%r13, %xmm0
	movslq	%edx, %rcx
	subl	$250, %eax
	movslq	%r10d, %r8
	movhps	-128(%rbp), %xmm0
	movq	%rcx, -80(%rbp)
	cmovs	%esi, %eax
	shrq	$32, %r10
	movaps	%xmm0, -96(%rbp)
	movl	%eax, -64(%rbp)
	cmpl	$1, %r10d
	je	.L2346
	cmpl	$6, %edx
	jg	.L2325
	cmpl	$1, %edx
	je	.L2347
	leaq	_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L2327
	.p2align 4,,10
	.p2align 3
.L2346:
	cmpl	$6, %edx
	jg	.L2321
	cmpl	$1, %edx
	je	.L2348
	leaq	_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L2327
	.p2align 4,,10
	.p2align 3
.L2293:
	movq	41112(%r13), %rdi
	movq	15(%rcx), %rsi
	testq	%rdi, %rdi
	je	.L2295
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
	jmp	.L2290
	.p2align 4,,10
	.p2align 3
.L2308:
	movq	41112(%r13), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2310
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
	jmp	.L2305
	.p2align 4,,10
	.p2align 3
.L2343:
	movq	-128(%rbp), %rcx
	movl	%r15d, %r9d
	movq	%r12, %rsi
	movq	%r10, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal12SearchStringIhtEEiPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEi
	jmp	.L2288
	.p2align 4,,10
	.p2align 3
.L2325:
	leaq	_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -72(%rbp)
.L2327:
	movl	%r15d, %ecx
	movq	%r12, %rsi
	movq	%r8, %rdx
	movq	%r14, %rdi
	call	*%rax
	jmp	.L2288
	.p2align 4,,10
	.p2align 3
.L2316:
	movq	41088(%r13), %rbx
	cmpq	41096(%r13), %rbx
	je	.L2349
.L2318:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rbx)
	jmp	.L2313
	.p2align 4,,10
	.p2align 3
.L2301:
	movq	41088(%r13), %r12
	cmpq	41096(%r13), %r12
	je	.L2350
.L2303:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r12)
	jmp	.L2298
	.p2align 4,,10
	.p2align 3
.L2321:
	leaq	_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L2327
	.p2align 4,,10
	.p2align 3
.L2295:
	movq	41088(%r13), %r12
	cmpq	41096(%r13), %r12
	je	.L2351
.L2297:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r12)
	jmp	.L2290
	.p2align 4,,10
	.p2align 3
.L2310:
	movq	41088(%r13), %rbx
	cmpq	41096(%r13), %rbx
	je	.L2352
.L2312:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rbx)
	jmp	.L2305
	.p2align 4,,10
	.p2align 3
.L2347:
	leaq	_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L2327
	.p2align 4,,10
	.p2align 3
.L2344:
	leaq	_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L2332
	.p2align 4,,10
	.p2align 3
.L2335:
	movl	$-1, %eax
	jmp	.L2288
	.p2align 4,,10
	.p2align 3
.L2348:
	leaq	_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L2327
	.p2align 4,,10
	.p2align 3
.L2350:
	movq	%r13, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L2303
	.p2align 4,,10
	.p2align 3
.L2349:
	movq	%r13, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L2318
.L2351:
	movq	%r13, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L2297
.L2352:
	movq	%r13, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L2312
.L2345:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19699:
	.size	_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleIS1_EES5_i, .-_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleIS1_EES5_i
	.section	.rodata._ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_.str1.1,"aMS",@progbits,1
.LC21:
	.string	"String.prototype.indexOf"
	.section	.text._ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_
	.type	_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_, @function
_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_:
.LFB19697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	cmpq	%rax, 104(%rdi)
	jne	.L2384
.L2354:
	leaq	.LC21(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$24, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2385
	movl	$27, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L2360:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2386
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2384:
	.cfi_restore_state
	cmpq	%rax, 88(%rdi)
	je	.L2354
	movq	%rcx, %r13
	testb	$1, %al
	jne	.L2355
.L2358:
	movq	%r12, %rdi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-72(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L2367
.L2357:
	movq	(%rdx), %rax
	movq	%rdx, %r15
	testb	$1, %al
	jne	.L2361
.L2364:
	movq	%rdx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2367
.L2363:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L2387
	movq	(%r14), %rdx
.L2372:
	sarq	$32, %rax
	movl	$0, %ecx
	testq	%rax, %rax
	cmovg	%eax, %ecx
.L2369:
	cmpl	%ecx, 11(%rdx)
	cmovbe	11(%rdx), %ecx
	movq	%r14, %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleIS1_EES5_i
	salq	$32, %rax
	jmp	.L2360
	.p2align 4,,10
	.p2align 3
.L2385:
	leaq	.LC13(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2367:
	movq	312(%r12), %rax
	jmp	.L2360
	.p2align 4,,10
	.p2align 3
.L2355:
	movq	-1(%rax), %rax
	movq	%rsi, %r14
	cmpw	$63, 11(%rax)
	jbe	.L2357
	jmp	.L2358
	.p2align 4,,10
	.p2align 3
.L2361:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L2364
	jmp	.L2363
	.p2align 4,,10
	.p2align 3
.L2387:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	je	.L2367
	movq	(%rax), %rax
	movq	(%r14), %rdx
	testb	$1, %al
	je	.L2372
	movsd	7(%rax), %xmm0
	xorl	%ecx, %ecx
	comisd	.LC16(%rip), %xmm0
	jb	.L2369
	movsd	.LC17(%rip), %xmm1
	movl	$-1, %ecx
	comisd	%xmm0, %xmm1
	jbe	.L2369
	cvttsd2siq	%xmm0, %rcx
	jmp	.L2369
.L2386:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19697:
	.size	_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_, .-_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_
	.section	.text._ZN2v88internal6String15GetSubstitutionEPNS0_7IsolateEPNS1_5MatchENS0_6HandleIS1_EEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String15GetSubstitutionEPNS0_7IsolateEPNS1_5MatchENS0_6HandleIS1_EEi
	.type	_ZN2v88internal6String15GetSubstitutionEPNS0_7IsolateEPNS1_5MatchENS0_6HandleIS1_EEi, @function
_ZN2v88internal6String15GetSubstitutionEPNS0_7IsolateEPNS1_5MatchENS0_6HandleIS1_EEi:
.LFB19700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movl	11(%rax), %eax
	movl	%eax, -116(%rbp)
	movq	(%rsi), %rax
	call	*24(%rax)
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, -120(%rbp)
	call	_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE
	movl	$36, %esi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt@PLT
	movl	%r15d, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleIS1_EES5_i
	testl	%eax, %eax
	jns	.L2389
	movq	%r12, %rax
.L2390:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2488
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2389:
	.cfi_restore_state
	movl	%eax, %r15d
	leaq	-96(%rbp), %rax
	movq	%r13, %rsi
	movl	$1, %r14d
	movq	%rax, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilderC1EPNS0_7IsolateE@PLT
	testl	%r15d, %r15d
	jne	.L2489
.L2391:
	cmpl	%r14d, -116(%rbp)
	jle	.L2397
	movl	%r15d, %eax
	movl	%r14d, %r15d
	movl	%eax, %r14d
	.p2align 4,,10
	.p2align 3
.L2394:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L2401
	leaq	.L2403(%rip), %rsi
	movzwl	%ax, %eax
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6String15GetSubstitutionEPNS0_7IsolateEPNS1_5MatchENS0_6HandleIS1_EEi,"a",@progbits
	.align 4
	.align 4
.L2403:
	.long	.L2427-.L2403
	.long	.L2424-.L2403
	.long	.L2426-.L2403
	.long	.L2404-.L2403
	.long	.L2401-.L2403
	.long	.L2402-.L2403
	.long	.L2401-.L2403
	.long	.L2401-.L2403
	.long	.L2425-.L2403
	.long	.L2424-.L2403
	.long	.L2423-.L2403
	.long	.L2404-.L2403
	.long	.L2401-.L2403
	.long	.L2402-.L2403
	.section	.text._ZN2v88internal6String15GetSubstitutionEPNS0_7IsolateEPNS1_5MatchENS0_6HandleIS1_EEi
	.p2align 4,,10
	.p2align 3
.L2424:
	leaq	-104(%rbp), %rdi
	movl	%r15d, %esi
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal10ConsString3GetEi
	.p2align 4,,10
	.p2align 3
.L2410:
	cmpw	$60, %ax
	ja	.L2429
.L2495:
	cmpw	$35, %ax
	jbe	.L2430
	leal	-36(%rax), %edx
	cmpw	$24, %dx
	ja	.L2430
	leaq	.L2432(%rip), %rcx
	movzwl	%dx, %edx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal6String15GetSubstitutionEPNS0_7IsolateEPNS1_5MatchENS0_6HandleIS1_EEi
	.align 4
	.align 4
.L2432:
	.long	.L2436-.L2432
	.long	.L2430-.L2432
	.long	.L2435-.L2432
	.long	.L2434-.L2432
	.long	.L2430-.L2432
	.long	.L2430-.L2432
	.long	.L2430-.L2432
	.long	.L2430-.L2432
	.long	.L2430-.L2432
	.long	.L2430-.L2432
	.long	.L2430-.L2432
	.long	.L2430-.L2432
	.long	.L2433-.L2432
	.long	.L2433-.L2432
	.long	.L2433-.L2432
	.long	.L2433-.L2432
	.long	.L2433-.L2432
	.long	.L2433-.L2432
	.long	.L2433-.L2432
	.long	.L2433-.L2432
	.long	.L2433-.L2432
	.long	.L2433-.L2432
	.long	.L2430-.L2432
	.long	.L2430-.L2432
	.long	.L2431-.L2432
	.section	.text._ZN2v88internal6String15GetSubstitutionEPNS0_7IsolateEPNS1_5MatchENS0_6HandleIS1_EEi
.L2431:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*32(%rax)
	testb	%al, %al
	je	.L2430
	movl	$62, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt@PLT
	leal	2(%r14), %r9d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%r9d, %ecx
	movq	%rax, %rdx
	movl	%r9d, -140(%rbp)
	call	_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleIS1_EES5_i
	movl	-140(%rbp), %r9d
	cmpl	$-1, %eax
	movl	%eax, %r14d
	jne	.L2453
.L2430:
	movl	-76(%rbp), %eax
	movl	-88(%rbp), %esi
	movq	-64(%rbp), %rdx
	leal	1(%rax), %ecx
	testl	%esi, %esi
	movq	(%rdx), %rdx
	movl	%ecx, -76(%rbp)
	jne	.L2457
	addl	$16, %eax
	cltq
	movb	$36, -1(%rdx,%rax)
	movl	-80(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	je	.L2476
	.p2align 4,,10
	.p2align 3
.L2442:
	movq	-128(%rbp), %rdx
	movl	%r15d, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleIS1_EES5_i
	movl	%eax, %r14d
	testl	%eax, %eax
	js	.L2490
	cmpl	%eax, %r15d
	jl	.L2491
.L2461:
	leal	1(%r14), %r15d
	cmpl	-116(%rbp), %r15d
	jl	.L2394
.L2397:
	movl	-76(%rbp), %eax
	movq	-64(%rbp), %rdx
	movl	-88(%rbp), %r10d
	movq	(%rdx), %rdx
	leal	1(%rax), %ecx
	testl	%r10d, %r10d
	jne	.L2492
	addl	$16, %eax
	movl	%ecx, -76(%rbp)
	cltq
	movb	$36, -1(%rdx,%rax)
	movl	-80(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	je	.L2398
.L2399:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	jmp	.L2390
.L2433:
	movzwl	%ax, %eax
	leal	-48(%rax), %r9d
	leal	2(%r14), %eax
	movl	$1, %r14d
	cmpl	-116(%rbp), %eax
	jl	.L2493
.L2443:
	testl	%r9d, %r9d
	je	.L2430
	cmpl	-120(%rbp), %r9d
	jge	.L2430
	movq	(%rbx), %rax
	leaq	-104(%rbp), %rdx
	movl	%r9d, %esi
	movq	%rbx, %rdi
	call	*40(%rax)
	testq	%rax, %rax
	je	.L2390
	cmpb	$0, -104(%rbp)
	jne	.L2494
.L2449:
	addl	%r14d, %r15d
	jmp	.L2442
.L2434:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	leal	2(%r14), %r15d
	call	*16(%rax)
	movq	-136(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L2442
.L2435:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*(%rax)
	movq	-136(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
.L2477:
	leal	2(%r14), %r15d
	jmp	.L2442
.L2436:
	movl	-76(%rbp), %eax
	movl	-88(%rbp), %r8d
	movq	-64(%rbp), %rdx
	leal	1(%rax), %ecx
	testl	%r8d, %r8d
	movq	(%rdx), %rdx
	movl	%ecx, -76(%rbp)
	jne	.L2438
	addl	$16, %eax
	cltq
	movb	$36, -1(%rdx,%rax)
	movl	-80(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	jne	.L2477
.L2439:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L2477
	.p2align 4,,10
	.p2align 3
.L2402:
	movq	15(%rdx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L2401
	leaq	.L2421(%rip), %rdi
	movzwl	%ax, %eax
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6String15GetSubstitutionEPNS0_7IsolateEPNS1_5MatchENS0_6HandleIS1_EEi
	.align 4
	.align 4
.L2421:
	.long	.L2427-.L2421
	.long	.L2424-.L2421
	.long	.L2426-.L2421
	.long	.L2422-.L2421
	.long	.L2401-.L2421
	.long	.L2420-.L2421
	.long	.L2401-.L2421
	.long	.L2401-.L2421
	.long	.L2425-.L2421
	.long	.L2424-.L2421
	.long	.L2423-.L2421
	.long	.L2422-.L2421
	.long	.L2401-.L2421
	.long	.L2420-.L2421
	.section	.text._ZN2v88internal6String15GetSubstitutionEPNS0_7IsolateEPNS1_5MatchENS0_6HandleIS1_EEi
	.p2align 4,,10
	.p2align 3
.L2404:
	movq	15(%rdx), %rcx
	movl	27(%rdx), %esi
	movq	-1(%rcx), %rax
	addl	%r15d, %esi
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L2401
	leaq	.L2412(%rip), %rdi
	movzwl	%ax, %eax
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6String15GetSubstitutionEPNS0_7IsolateEPNS1_5MatchENS0_6HandleIS1_EEi
	.align 4
	.align 4
.L2412:
	.long	.L2418-.L2412
	.long	.L2415-.L2412
	.long	.L2417-.L2412
	.long	.L2413-.L2412
	.long	.L2401-.L2412
	.long	.L2411-.L2412
	.long	.L2401-.L2412
	.long	.L2401-.L2412
	.long	.L2416-.L2412
	.long	.L2415-.L2412
	.long	.L2414-.L2412
	.long	.L2413-.L2412
	.long	.L2401-.L2412
	.long	.L2411-.L2412
	.section	.text._ZN2v88internal6String15GetSubstitutionEPNS0_7IsolateEPNS1_5MatchENS0_6HandleIS1_EEi
	.p2align 4,,10
	.p2align 3
.L2426:
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	%r15d, %rdx
	movzwl	(%rax,%rdx,2), %eax
	cmpw	$60, %ax
	jbe	.L2495
	.p2align 4,,10
	.p2align 3
.L2429:
	cmpw	$96, %ax
	jne	.L2430
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	leal	2(%r14), %r15d
	call	*8(%rax)
	movq	-136(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L2442
	.p2align 4,,10
	.p2align 3
.L2425:
	leal	17(%r14), %eax
	cltq
	movzbl	-1(%rdx,%rax), %eax
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2423:
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movq	%rax, %r9
	movslq	%r15d, %rax
	movzbl	(%r9,%rax), %eax
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2427:
	leal	18(%r14,%r14), %eax
	cltq
	movzwl	-1(%rdx,%rax), %eax
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2491:
	testl	%r15d, %r15d
	jne	.L2462
	movq	(%r12), %rax
	movq	%r12, %rsi
	cmpl	11(%rax), %r14d
	je	.L2463
.L2462:
	movq	%r12, %rsi
	movl	%r14d, %ecx
	movl	%r15d, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	%rax, %rsi
.L2463:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L2461
	.p2align 4,,10
	.p2align 3
.L2457:
	leal	16(%rax,%rax), %eax
	movl	$36, %ecx
	cltq
	movw	%cx, -1(%rdx,%rax)
	movl	-80(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	jne	.L2442
.L2476:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L2442
	.p2align 4,,10
	.p2align 3
.L2438:
	leal	16(%rax,%rax), %eax
	movl	$36, %edi
	cltq
	movw	%di, -1(%rdx,%rax)
	movl	-80(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	jne	.L2477
	jmp	.L2439
	.p2align 4,,10
	.p2align 3
.L2493:
	movq	(%r12), %rdx
	movl	%eax, -112(%rbp)
	leaq	-104(%rbp), %rsi
	leaq	-108(%rbp), %rdi
	movl	%r9d, -140(%rbp)
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movq	%rdx, -104(%rbp)
	leaq	-112(%rbp), %rdx
	movl	%eax, -108(%rbp)
	call	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
	movl	-140(%rbp), %r9d
	leal	-48(%rax), %edx
	cmpw	$9, %dx
	ja	.L2443
	leal	(%r9,%r9,4), %edx
	movl	-120(%rbp), %edi
	movzwl	%ax, %eax
	leal	-48(%rax,%rdx,2), %eax
	cmpl	%eax, %edi
	setg	%r14b
	cmovg	%eax, %r9d
	movzbl	%r14b, %r14d
	addl	$1, %r14d
	jmp	.L2443
	.p2align 4,,10
	.p2align 3
.L2453:
	movl	%r9d, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	leaq	-104(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	(%rbx), %rax
	call	*48(%rax)
	testq	%rax, %rax
	je	.L2390
	cmpl	$2, -104(%rbp)
	je	.L2496
.L2456:
	leal	1(%r14), %r15d
	jmp	.L2442
	.p2align 4,,10
	.p2align 3
.L2415:
	leaq	-104(%rbp), %rdi
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal10ConsString3GetEi
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2420:
	movq	15(%rdx), %rax
	movl	%r15d, -112(%rbp)
.L2475:
	movq	-1(%rax), %rdx
	leaq	-104(%rbp), %rsi
	leaq	-108(%rbp), %rdi
	movzwl	11(%rdx), %edx
	movq	%rax, -104(%rbp)
	movl	%edx, -108(%rbp)
	leaq	-112(%rbp), %rdx
	call	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2422:
	movl	27(%rdx), %edi
	movq	15(%rdx), %rax
	addl	%r15d, %edi
	movl	%edi, -112(%rbp)
	jmp	.L2475
	.p2align 4,,10
	.p2align 3
.L2411:
	movq	15(%rcx), %rax
	movl	%esi, -112(%rbp)
	jmp	.L2475
	.p2align 4,,10
	.p2align 3
.L2413:
	addl	27(%rcx), %esi
	movq	15(%rcx), %rax
	movl	%esi, -112(%rbp)
	jmp	.L2475
.L2416:
	addl	$16, %esi
	movslq	%esi, %rsi
	movzbl	-1(%rcx,%rsi), %eax
	jmp	.L2410
.L2417:
	movq	15(%rcx), %rdi
	movl	%esi, -140(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-140(%rbp), %rsi
	movzwl	(%rax,%rsi,2), %eax
	jmp	.L2410
.L2418:
	leal	16(%rsi,%rsi), %eax
	cltq
	movzwl	-1(%rcx,%rax), %eax
	jmp	.L2410
.L2414:
	movq	15(%rcx), %rdi
	movl	%esi, -140(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-140(%rbp), %rsi
	movzbl	(%rax,%rsi), %eax
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2490:
	movl	%r15d, %r14d
	cmpl	-116(%rbp), %r15d
	jge	.L2399
	testl	%r15d, %r15d
	je	.L2497
.L2459:
	movl	-116(%rbp), %ecx
	movq	%r12, %rsi
	movl	%r14d, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	%rax, %r12
.L2460:
	movq	-136(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L2399
	.p2align 4,,10
	.p2align 3
.L2489:
	movq	(%r12), %rax
	movq	%r12, %rsi
	cmpl	11(%rax), %r15d
	je	.L2393
	movl	%r15d, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	%rax, %rsi
.L2393:
	movq	-136(%rbp), %rdi
	leal	1(%r15), %r14d
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L2391
	.p2align 4,,10
	.p2align 3
.L2494:
	movq	-136(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L2449
	.p2align 4,,10
	.p2align 3
.L2492:
	leal	16(%rax,%rax), %eax
	movl	$36, %r9d
	movl	%ecx, -76(%rbp)
	cltq
	movw	%r9w, -1(%rdx,%rax)
	movl	-80(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	jne	.L2399
.L2398:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L2399
	.p2align 4,,10
	.p2align 3
.L2401:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2497:
	movq	(%r12), %rax
	movl	-116(%rbp), %ebx
	cmpl	11(%rax), %ebx
	jne	.L2459
	jmp	.L2460
.L2496:
	movq	-136(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L2456
.L2488:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19700:
	.size	_ZN2v88internal6String15GetSubstitutionEPNS0_7IsolateEPNS1_5MatchENS0_6HandleIS1_EEi, .-_ZN2v88internal6String15GetSubstitutionEPNS0_7IsolateEPNS1_5MatchENS0_6HandleIS1_EEi
	.section	.text._ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.type	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, @function
_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_:
.LFB24109:
	.cfi_startproc
	endbr64
	movabsq	$2305843009213693951, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$2, %rax
	cmpq	%rcx, %rax
	je	.L2512
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L2508
	movabsq	$9223372036854775804, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L2513
.L2500:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L2507:
	movl	(%r15), %eax
	subq	%r8, %r13
	leaq	4(%rbx,%rdx), %r15
	movl	%eax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L2514
	testq	%r13, %r13
	jg	.L2503
	testq	%r9, %r9
	jne	.L2506
.L2504:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2514:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L2503
.L2506:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L2504
	.p2align 4,,10
	.p2align 3
.L2513:
	testq	%rsi, %rsi
	jne	.L2501
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L2507
	.p2align 4,,10
	.p2align 3
.L2503:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L2504
	jmp	.L2506
	.p2align 4,,10
	.p2align 3
.L2508:
	movl	$4, %r14d
	jmp	.L2500
.L2512:
	leaq	.LC20(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2501:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$2, %r14
	jmp	.L2500
	.cfi_endproc
.LFE24109:
	.size	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, .-_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.section	.text._ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.type	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, @function
_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_:
.LFB24480:
	.cfi_startproc
	endbr64
	movabsq	$2305843009213693951, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$2, %rax
	cmpq	%rcx, %rax
	je	.L2529
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L2525
	movabsq	$9223372036854775804, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L2530
.L2517:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L2524:
	movl	(%r15), %eax
	subq	%r8, %r13
	leaq	4(%rbx,%rdx), %r15
	movl	%eax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L2531
	testq	%r13, %r13
	jg	.L2520
	testq	%r9, %r9
	jne	.L2523
.L2521:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2531:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L2520
.L2523:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L2521
	.p2align 4,,10
	.p2align 3
.L2530:
	testq	%rsi, %rsi
	jne	.L2518
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L2524
	.p2align 4,,10
	.p2align 3
.L2520:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L2521
	jmp	.L2523
	.p2align 4,,10
	.p2align 3
.L2525:
	movl	$4, %r14d
	jmp	.L2517
.L2529:
	leaq	.LC20(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2518:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$2, %r14
	jmp	.L2517
	.cfi_endproc
.LFE24480:
	.size	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, .-_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.section	.rodata._ZN2v88internal6String17CalculateLineEndsEPNS0_7IsolateENS0_6HandleIS1_EEb.str1.1,"aMS",@progbits,1
.LC22:
	.string	"vector::reserve"
	.section	.text._ZN2v88internal6String17CalculateLineEndsEPNS0_7IsolateENS0_6HandleIS1_EEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6String17CalculateLineEndsEPNS0_7IsolateENS0_6HandleIS1_EEb
	.type	_ZN2v88internal6String17CalculateLineEndsEPNS0_7IsolateENS0_6HandleIS1_EEb, @function
_ZN2v88internal6String17CalculateLineEndsEPNS0_7IsolateENS0_6HandleIS1_EEb:
.LFB19680:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, %rsi
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L2618
.L2534:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	jbe	.L2543
.L2611:
	movq	(%r14), %rsi
.L2542:
	movl	11(%rsi), %edx
	pxor	%xmm0, %xmm0
	movabsq	$2305843009213693951, %rax
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	sarl	$4, %edx
	movslq	%edx, %rdx
	cmpq	%rax, %rdx
	ja	.L2619
	testq	%rdx, %rdx
	jne	.L2620
.L2550:
	movq	(%r14), %rax
	leaq	-88(%rbp), %r13
	leaq	-93(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	movl	$0, -88(%rbp)
	movq	-72(%rbp), %rsi
	movq	%rax, %r14
	movq	%rdx, %rax
	movl	%edx, -92(%rbp)
	movl	%edx, %edi
	shrq	$32, %rax
	leal	-1(%rdx), %ecx
	cmpl	$1, %eax
	je	.L2621
	testl	%ecx, %ecx
	jle	.L2568
	xorl	%edx, %edx
	leaq	-80(%rbp), %r15
	jmp	.L2574
	.p2align 4,,10
	.p2align 3
.L2622:
	subl	$8232, %eax
	cmpl	$1, %eax
	jbe	.L2570
.L2571:
	addl	$1, %edx
	leal	-1(%rdi), %ecx
	movl	%edx, -88(%rbp)
	cmpl	%edx, %ecx
	jle	.L2568
.L2574:
	movslq	%edx, %rax
	leaq	(%rax,%rax), %r8
	movzwl	(%r14,%rax,2), %eax
	movl	%eax, %ecx
	cmpw	$10, %ax
	je	.L2569
	cmpl	$13, %eax
	jne	.L2622
.L2569:
	cmpw	$10, 2(%r8,%r14)
	jne	.L2570
	cmpw	$13, %cx
	je	.L2571
.L2570:
	cmpq	%rsi, -64(%rbp)
	je	.L2573
	movl	%edx, (%rsi)
	movq	-72(%rbp), %rax
	movl	-88(%rbp), %edx
	movl	-92(%rbp), %edi
	leaq	4(%rax), %rsi
	addl	$1, %edx
	leal	-1(%rdi), %ecx
	movq	%rsi, -72(%rbp)
	movl	%edx, -88(%rbp)
	cmpl	%edx, %ecx
	jg	.L2574
.L2568:
	testl	%edi, %edi
	jle	.L2575
	movslq	%ecx, %rax
	movzwl	(%r14,%rax,2), %eax
	cmpl	$10, %eax
	je	.L2576
	cmpl	$13, %eax
	je	.L2576
	subl	$8232, %eax
	cmpl	$1, %eax
	jbe	.L2576
	.p2align 4,,10
	.p2align 3
.L2575:
	testb	%bl, %bl
	jne	.L2623
.L2567:
	subq	-80(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%rsi, %rbx
	sarq	$2, %rbx
	movl	%ebx, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.L2584
	leal	-1(%rbx), %eax
	xorl	%edx, %edx
	leaq	4(,%rax,4), %rax
	.p2align 4,,10
	.p2align 3
.L2585:
	movq	-80(%rbp), %rcx
	movq	(%r12), %rsi
	movslq	(%rcx,%rdx), %rcx
	salq	$32, %rcx
	movq	%rcx, 15(%rsi,%rdx,2)
	addq	$4, %rdx
	cmpq	%rax, %rdx
	jne	.L2585
.L2584:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2583
	call	_ZdlPv@PLT
.L2583:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2624
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2543:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L2611
	movq	(%r14), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2546
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
	jmp	.L2611
	.p2align 4,,10
	.p2align 3
.L2555:
	testl	%edi, %edi
	jle	.L2575
	movslq	%ecx, %rax
	movzbl	(%r14,%rax), %eax
	cmpl	$10, %eax
	je	.L2576
	cmpl	$13, %eax
	jne	.L2575
	.p2align 4,,10
	.p2align 3
.L2576:
	movl	%ecx, -88(%rbp)
	cmpq	%rsi, -64(%rbp)
	je	.L2577
	movl	%ecx, (%rsi)
	movq	-72(%rbp), %rax
	leaq	4(%rax), %rsi
	movq	%rsi, -72(%rbp)
	testb	%bl, %bl
	je	.L2567
.L2623:
	cmpq	%rsi, -64(%rbp)
	je	.L2580
	movl	-92(%rbp), %eax
	movl	%eax, (%rsi)
	movq	-72(%rbp), %rax
	leaq	4(%rax), %rsi
	movq	%rsi, -72(%rbp)
	jmp	.L2567
	.p2align 4,,10
	.p2align 3
.L2618:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L2534
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L2537
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L2537
	movq	%r14, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE
	movq	(%rax), %rsi
	movq	%rax, %r14
	jmp	.L2542
	.p2align 4,,10
	.p2align 3
.L2573:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movq	-72(%rbp), %rsi
	movl	-92(%rbp), %edi
	movl	-88(%rbp), %edx
	jmp	.L2571
	.p2align 4,,10
	.p2align 3
.L2621:
	testl	%ecx, %ecx
	jle	.L2555
	xorl	%eax, %eax
	leaq	-80(%rbp), %r15
	jmp	.L2560
	.p2align 4,,10
	.p2align 3
.L2556:
	movl	%ecx, -88(%rbp)
	movl	%ecx, %eax
	leal	-1(%rdi), %ecx
	cmpl	%eax, %ecx
	jle	.L2555
.L2560:
	movslq	%eax, %rdx
	movzbl	(%r14,%rdx), %r8d
	movl	%r8d, %edx
	cmpb	$10, %r8b
	je	.L2587
	leal	1(%rax), %ecx
	cmpl	$13, %r8d
	jne	.L2556
.L2587:
	leal	1(%rax), %ecx
	movslq	%ecx, %r8
	cmpb	$10, (%r14,%r8)
	jne	.L2588
	cmpb	$13, %dl
	je	.L2556
.L2588:
	cmpq	%rsi, -64(%rbp)
	je	.L2559
	movl	%eax, (%rsi)
	movq	-72(%rbp), %rax
	movl	-92(%rbp), %edi
	leaq	4(%rax), %rsi
	movl	-88(%rbp), %eax
	movq	%rsi, -72(%rbp)
	leal	1(%rax), %ecx
	jmp	.L2556
	.p2align 4,,10
	.p2align 3
.L2620:
	leaq	0(,%rdx,4), %r13
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %rdx
	movq	%rax, %r15
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L2625
	testq	%r8, %r8
	jne	.L2552
.L2553:
	movq	%r15, %xmm0
	leaq	(%r15,%r13), %rdx
	punpcklqdq	%xmm0, %xmm0
	movq	%rdx, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	jmp	.L2550
	.p2align 4,,10
	.p2align 3
.L2625:
	movq	%r8, %rsi
	movq	%rax, %rdi
	movq	%r8, -104(%rbp)
	call	memmove@PLT
	movq	-104(%rbp), %r8
.L2552:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L2553
	.p2align 4,,10
	.p2align 3
.L2546:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L2626
.L2548:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L2542
	.p2align 4,,10
	.p2align 3
.L2559:
	movq	%r15, %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movl	-88(%rbp), %eax
	movl	-92(%rbp), %edi
	movq	-72(%rbp), %rsi
	leal	1(%rax), %ecx
	jmp	.L2556
	.p2align 4,,10
	.p2align 3
.L2537:
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2539
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
	jmp	.L2534
.L2577:
	leaq	-80(%rbp), %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movq	-72(%rbp), %rsi
	jmp	.L2575
.L2580:
	leaq	-92(%rbp), %rdx
	leaq	-80(%rbp), %rdi
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movq	-72(%rbp), %rsi
	jmp	.L2567
.L2539:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L2627
.L2541:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L2534
.L2627:
	movq	%r12, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L2541
.L2626:
	movq	%r12, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L2548
.L2624:
	call	__stack_chk_fail@PLT
.L2619:
	leaq	.LC22(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE19680:
	.size	_ZN2v88internal6String17CalculateLineEndsEPNS0_7IsolateENS0_6HandleIS1_EEb, .-_ZN2v88internal6String17CalculateLineEndsEPNS0_7IsolateENS0_6HandleIS1_EEb
	.section	.text._ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv,"axG",@progbits,_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv
	.type	_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv, @function
_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv:
.LFB24870:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rsi
	movslq	32(%rdi), %r13
	movq	8(%rdi), %r9
	movq	(%rdi), %rdi
	movq	%rsi, %rcx
	movslq	%esi, %r11
	movl	%esi, %r10d
	movq	%rsi, -64(%rbp)
	leaq	43396(%rdi), %rdx
	addq	$44400, %rdi
	movl	%esi, %r8d
	subl	%r13d, %r10d
	leaq	0(,%r13,4), %rax
	movq	%rdx, %rbx
	leaq	0(,%r11,4), %rsi
	subq	%rax, %rbx
	subq	%rax, %rdi
	movl	%ecx, %eax
	leaq	(%rbx,%rsi), %r14
	addl	$1, %eax
	addq	%rdi, %rsi
	cmpl	%ecx, %r13d
	jge	.L2629
	movl	%ecx, -52(%rbp)
	subl	$1, %ecx
	movq	%r13, %r12
	subl	%r13d, %ecx
	cmpl	$2, %ecx
	jbe	.L2653
	movl	%r10d, %ecx
	movd	%r10d, %xmm1
	shrl	$2, %ecx
	pshufd	$0, %xmm1, %xmm0
	salq	$4, %rcx
	addq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L2631:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L2631
	movl	%r10d, %ecx
	andl	$-4, %ecx
	leal	(%rcx,%r12), %edx
	cmpl	%r10d, %ecx
	je	.L2632
.L2630:
	movslq	%edx, %rcx
	movq	-64(%rbp), %r15
	movl	%r10d, (%rbx,%rcx,4)
	leal	1(%rdx), %ecx
	cmpl	%ecx, %r15d
	jle	.L2632
	movslq	%ecx, %rcx
	addl	$2, %edx
	movl	%r10d, (%rbx,%rcx,4)
	cmpl	%r15d, %edx
	jge	.L2632
	movslq	%edx, %rdx
	movl	%r10d, (%rbx,%rdx,4)
.L2632:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	movzbl	-1(%r9,%r11), %r15d
	movl	-64(%rbp), %esi
	movl	%r15d, %r11d
	.p2align 4,,10
	.p2align 3
.L2652:
	cmpl	%eax, %r8d
	jl	.L2634
	.p2align 4,,10
	.p2align 3
.L2638:
	movslq	%eax, %rdx
	cmpb	%r11b, -1(%r9,%rdx)
	je	.L2634
	leaq	(%rbx,%rdx,4), %rcx
	cmpl	%r10d, (%rcx)
	je	.L2659
	movl	(%rdi,%rdx,4), %eax
	cmpl	%r8d, %eax
	jle	.L2638
.L2634:
	subl	$1, %esi
	leal	-1(%rax), %edx
	movslq	%esi, %rcx
	movl	%edx, (%rdi,%rcx,4)
	cmpl	%r8d, %edx
	je	.L2660
.L2639:
	cmpl	%esi, %r12d
	jge	.L2633
.L2662:
	movslq	%esi, %rax
	movzbl	-1(%r9,%rax), %r11d
	movl	%edx, %eax
	jmp	.L2652
	.p2align 4,,10
	.p2align 3
.L2659:
	subl	%esi, %eax
	movl	%eax, (%rcx)
	movl	(%rdi,%rdx,4), %eax
	cmpl	%r8d, %eax
	jle	.L2638
	jmp	.L2634
	.p2align 4,,10
	.p2align 3
.L2660:
	cmpl	%esi, %r12d
	jl	.L2645
	jmp	.L2640
	.p2align 4,,10
	.p2align 3
.L2661:
	cmpl	%r10d, (%r14)
	jne	.L2642
	movl	-52(%rbp), %edx
	subl	%ecx, %edx
	movl	%edx, (%r14)
.L2642:
	movl	%r8d, -4(%rdi,%rcx,4)
	subq	$1, %rcx
	subl	$1, %esi
	cmpl	%ecx, %r12d
	jge	.L2640
.L2645:
	movl	%ecx, %esi
	cmpb	%r15b, -1(%r9,%rcx)
	jne	.L2661
	cmpl	%r12d, %ecx
	jle	.L2628
	subl	$1, %esi
	leal	-2(%rax), %edx
	movslq	%esi, %rax
	movl	%edx, (%rdi,%rax,4)
	cmpl	%esi, %r12d
	jl	.L2662
	.p2align 4,,10
	.p2align 3
.L2633:
	cmpl	-64(%rbp), %edx
	jge	.L2628
	.p2align 4,,10
	.p2align 3
.L2647:
	cmpl	%r10d, (%rbx,%r13,4)
	jne	.L2649
	movl	%edx, %eax
	subl	%r12d, %eax
	movl	%eax, (%rbx,%r13,4)
.L2649:
	cmpl	%r13d, %edx
	je	.L2663
	addq	$1, %r13
	cmpl	%r13d, %r8d
	jge	.L2647
.L2628:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2663:
	.cfi_restore_state
	movslq	%edx, %rdx
	addq	$1, %r13
	movl	(%rdi,%rdx,4), %edx
	cmpl	%r13d, %r8d
	jge	.L2647
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2640:
	.cfi_restore_state
	movl	%r8d, %edx
	jmp	.L2639
.L2629:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2653:
	.cfi_restore_state
	movl	%r13d, %edx
	jmp	.L2630
	.cfi_endproc
.LFE24870:
	.size	_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv, .-_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv
	.section	.text._ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi:
.LFB24727:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	movq	8(%rdi), %r13
	movq	%rdx, -72(%rbp)
	movq	(%rdi), %r10
	leal	-1(%rax), %edx
	movl	%eax, %r8d
	movl	%eax, -60(%rbp)
	leal	-2(%rax), %r15d
	movslq	%edx, %r9
	leaq	42372(%r10), %rdi
	movl	%edx, %ebx
	negl	%r8d
	movzbl	0(%r13,%r9), %r11d
	subl	42372(%r10,%r11,4), %ebx
	movl	%r14d, %r10d
	movq	%r11, %r9
	movl	$1, %r11d
	movl	%ebx, -56(%rbp)
	subl	%eax, %r10d
	jmp	.L2677
	.p2align 4,,10
	.p2align 3
.L2679:
	movl	%edx, %ebx
	subl	(%rdi,%rax,4), %ebx
	movl	%ebx, %eax
	addl	%ebx, %ecx
	movl	%r11d, %ebx
	subl	%eax, %ebx
	addl	%ebx, %r8d
.L2677:
	cmpl	%r10d, %ecx
	jg	.L2678
	leal	(%rdx,%rcx), %eax
	cltq
	movzbl	(%rsi,%rax), %eax
	cmpb	%r9b, %al
	jne	.L2679
	testl	%r15d, %r15d
	js	.L2674
	movslq	%ecx, %rbx
	movl	%edx, -64(%rbp)
	movslq	%r15d, %rax
	addq	%rsi, %rbx
	jmp	.L2669
	.p2align 4,,10
	.p2align 3
.L2680:
	subq	$1, %rax
	testl	%eax, %eax
	js	.L2674
.L2669:
	movzbl	(%rbx,%rax), %edx
	movl	%eax, %r14d
	cmpb	%dl, 0(%r13,%rax)
	je	.L2680
	movl	-60(%rbp), %eax
	movl	-56(%rbp), %ebx
	movl	-64(%rbp), %edx
	subl	%r14d, %eax
	addl	%ebx, %ecx
	subl	%ebx, %eax
	addl	%eax, %r8d
	testl	%r8d, %r8d
	jle	.L2677
	movq	%r12, %rdi
	movl	%ecx, -60(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv
	movl	-60(%rbp), %ecx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	leaq	_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	-72(%rbp), %rdx
	movq	%rax, 24(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi
	.p2align 4,,10
	.p2align 3
.L2678:
	.cfi_restore_state
	addq	$40, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2674:
	.cfi_restore_state
	addq	$40, %rsp
	movl	%ecx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24727:
	.size	_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi:
.LFB24484:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	16(%rdi), %rsi
	movq	8(%rdi), %r13
	movq	%rdi, -72(%rbp)
	movq	%rdx, -88(%rbp)
	movl	%esi, %eax
	subl	%esi, %ebx
	movq	%rsi, -80(%rbp)
	movl	%esi, -56(%rbp)
	sall	$2, %eax
	cmpl	%ebx, %ecx
	jg	.L2686
	movl	$-9, %edx
	movl	%ecx, %r9d
	subl	%eax, %edx
	movl	%edx, -60(%rbp)
	testl	%edx, %edx
	jg	.L2684
	movzbl	0(%r13), %r15d
	leal	1(%rbx), %eax
	movl	%eax, -52(%rbp)
	movl	%r15d, %r14d
	jmp	.L2688
	.p2align 4,,10
	.p2align 3
.L2712:
	subq	%r12, %rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	addq	%r12, %rdx
	cmpb	%r14b, (%rdx)
	je	.L2687
	leal	1(%rax), %r9d
	cmpl	%eax, %ebx
	jle	.L2686
.L2688:
	movl	-52(%rbp), %edx
	movslq	%r9d, %rdi
	movl	%r15d, %esi
	addq	%r12, %rdi
	subl	%r9d, %edx
	movslq	%edx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L2712
.L2686:
	movl	$-1, %r9d
.L2681:
	addq	$56, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2687:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L2686
	movl	-56(%rbp), %esi
	movl	$1, %eax
	jmp	.L2691
	.p2align 4,,10
	.p2align 3
.L2713:
	leal	1(%rax), %ecx
	addq	$1, %rax
	cmpl	%eax, %esi
	jle	.L2690
.L2691:
	movzbl	(%rdx,%rax), %edi
	movl	%eax, %ecx
	cmpb	%dil, 0(%r13,%rax)
	je	.L2713
.L2690:
	cmpl	-56(%rbp), %ecx
	je	.L2681
	addl	$1, %r9d
	addl	-60(%rbp), %ecx
	cmpl	%r9d, %ebx
	jl	.L2686
	leal	1(%rcx), %eax
	movl	%eax, -60(%rbp)
	testl	%eax, %eax
	jle	.L2688
.L2684:
	movq	-72(%rbp), %rax
	movq	(%rax), %rdx
	movl	32(%rax), %r10d
	leaq	42372(%rdx), %rsi
	testl	%r10d, %r10d
	je	.L2714
	leal	-1(%r10), %eax
	addq	$43396, %rdx
	movd	%eax, %xmm0
	movq	%rsi, %rax
	pshufd	$0, %xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L2696:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L2696
.L2695:
	movl	-80(%rbp), %eax
	subl	$1, %eax
	cmpl	%r10d, %eax
	jle	.L2697
	movl	-80(%rbp), %edx
	movslq	%r10d, %rcx
	movq	-72(%rbp), %r8
	leaq	1(%rcx), %rax
	subl	$2, %edx
	subl	%r10d, %edx
	addq	%rax, %rdx
	jmp	.L2698
	.p2align 4,,10
	.p2align 3
.L2715:
	addq	$1, %rax
.L2698:
	movq	8(%r8), %rdi
	movzbl	(%rdi,%rcx), %edi
	movl	%ecx, (%rsi,%rdi,4)
	movq	%rax, %rcx
	cmpq	%rax, %rdx
	jne	.L2715
.L2697:
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %rdx
	movq	%r12, %rsi
	movl	%r9d, %ecx
	leaq	_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi(%rip), %rbx
	movq	%rbx, 24(%rax)
	addq	$56, %rsp
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi
.L2714:
	.cfi_restore_state
	leaq	42380(%rdx), %rdi
	movq	%rsi, %rcx
	movq	$-1, %rax
	movq	$-1, 42372(%rdx)
	movq	$-1, 43388(%rdx)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	jmp	.L2695
	.cfi_endproc
.LFE24484:
	.size	_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv,"axG",@progbits,_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv
	.type	_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv, @function
_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv:
.LFB24876:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rsi
	movslq	32(%rdi), %r13
	movq	8(%rdi), %r9
	movq	(%rdi), %rdi
	movq	%rsi, %rcx
	movslq	%esi, %r11
	movl	%esi, %r10d
	movq	%rsi, -64(%rbp)
	leaq	43396(%rdi), %rdx
	addq	$44400, %rdi
	movl	%esi, %r8d
	subl	%r13d, %r10d
	leaq	0(,%r13,4), %rax
	movq	%rdx, %rbx
	leaq	0(,%r11,4), %rsi
	subq	%rax, %rbx
	subq	%rax, %rdi
	movl	%ecx, %eax
	leaq	(%rbx,%rsi), %r14
	addl	$1, %eax
	addq	%rdi, %rsi
	cmpl	%ecx, %r13d
	jge	.L2717
	movl	%ecx, -52(%rbp)
	subl	$1, %ecx
	movq	%r13, %r12
	subl	%r13d, %ecx
	cmpl	$2, %ecx
	jbe	.L2741
	movl	%r10d, %ecx
	movd	%r10d, %xmm1
	shrl	$2, %ecx
	pshufd	$0, %xmm1, %xmm0
	salq	$4, %rcx
	addq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L2719:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L2719
	movl	%r10d, %ecx
	andl	$-4, %ecx
	leal	(%rcx,%r12), %edx
	cmpl	%r10d, %ecx
	je	.L2720
.L2718:
	movslq	%edx, %rcx
	movq	-64(%rbp), %r15
	movl	%r10d, (%rbx,%rcx,4)
	leal	1(%rdx), %ecx
	cmpl	%ecx, %r15d
	jle	.L2720
	movslq	%ecx, %rcx
	addl	$2, %edx
	movl	%r10d, (%rbx,%rcx,4)
	cmpl	%r15d, %edx
	jge	.L2720
	movslq	%edx, %rdx
	movl	%r10d, (%rbx,%rdx,4)
.L2720:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	movzbl	-1(%r9,%r11), %r15d
	movl	-64(%rbp), %esi
	movl	%r15d, %r11d
	.p2align 4,,10
	.p2align 3
.L2740:
	cmpl	%eax, %r8d
	jl	.L2722
	.p2align 4,,10
	.p2align 3
.L2726:
	movslq	%eax, %rdx
	cmpb	%r11b, -1(%r9,%rdx)
	je	.L2722
	leaq	(%rbx,%rdx,4), %rcx
	cmpl	%r10d, (%rcx)
	je	.L2747
	movl	(%rdi,%rdx,4), %eax
	cmpl	%r8d, %eax
	jle	.L2726
.L2722:
	subl	$1, %esi
	leal	-1(%rax), %edx
	movslq	%esi, %rcx
	movl	%edx, (%rdi,%rcx,4)
	cmpl	%r8d, %edx
	je	.L2748
.L2727:
	cmpl	%esi, %r12d
	jge	.L2721
.L2750:
	movslq	%esi, %rax
	movzbl	-1(%r9,%rax), %r11d
	movl	%edx, %eax
	jmp	.L2740
	.p2align 4,,10
	.p2align 3
.L2747:
	subl	%esi, %eax
	movl	%eax, (%rcx)
	movl	(%rdi,%rdx,4), %eax
	cmpl	%r8d, %eax
	jle	.L2726
	jmp	.L2722
	.p2align 4,,10
	.p2align 3
.L2748:
	cmpl	%esi, %r12d
	jl	.L2733
	jmp	.L2728
	.p2align 4,,10
	.p2align 3
.L2749:
	cmpl	%r10d, (%r14)
	jne	.L2730
	movl	-52(%rbp), %edx
	subl	%ecx, %edx
	movl	%edx, (%r14)
.L2730:
	movl	%r8d, -4(%rdi,%rcx,4)
	subq	$1, %rcx
	subl	$1, %esi
	cmpl	%ecx, %r12d
	jge	.L2728
.L2733:
	movl	%ecx, %esi
	cmpb	%r15b, -1(%r9,%rcx)
	jne	.L2749
	cmpl	%r12d, %ecx
	jle	.L2716
	subl	$1, %esi
	leal	-2(%rax), %edx
	movslq	%esi, %rax
	movl	%edx, (%rdi,%rax,4)
	cmpl	%esi, %r12d
	jl	.L2750
	.p2align 4,,10
	.p2align 3
.L2721:
	cmpl	-64(%rbp), %edx
	jge	.L2716
	.p2align 4,,10
	.p2align 3
.L2735:
	cmpl	%r10d, (%rbx,%r13,4)
	jne	.L2737
	movl	%edx, %eax
	subl	%r12d, %eax
	movl	%eax, (%rbx,%r13,4)
.L2737:
	cmpl	%r13d, %edx
	je	.L2751
	addq	$1, %r13
	cmpl	%r13d, %r8d
	jge	.L2735
.L2716:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2751:
	.cfi_restore_state
	movslq	%edx, %rdx
	addq	$1, %r13
	movl	(%rdi,%rdx,4), %edx
	cmpl	%r13d, %r8d
	jge	.L2735
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2728:
	.cfi_restore_state
	movl	%r8d, %edx
	jmp	.L2727
.L2717:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2741:
	.cfi_restore_state
	movl	%r13d, %edx
	jmp	.L2718
	.cfi_endproc
.LFE24876:
	.size	_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv, .-_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv
	.section	.text._ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi:
.LFB24732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	movq	8(%rdi), %r13
	movq	%rdx, -88(%rbp)
	movq	(%rdi), %r10
	movq	%rdi, -80(%rbp)
	leal	-1(%rax), %edx
	movl	%eax, %r15d
	movl	%eax, %edi
	movl	%eax, -56(%rbp)
	movslq	%edx, %r8
	leaq	42372(%r10), %r11
	movl	%edx, %ebx
	negl	%edi
	movzbl	0(%r13,%r8), %r9d
	movl	%r14d, %r8d
	subl	%eax, %r8d
	subl	42372(%r10,%r9,4), %ebx
	movl	$1, %r10d
	movl	%ebx, -64(%rbp)
	leal	-2(%rax), %ebx
	movl	%r10d, %eax
	subl	%r15d, %eax
	movl	%ebx, -60(%rbp)
	movl	%eax, -68(%rbp)
.L2768:
	cmpl	%r8d, %ecx
	jle	.L2757
	jmp	.L2764
	.p2align 4,,10
	.p2align 3
.L2769:
	cmpw	$255, %bx
	ja	.L2755
	movl	%edx, %ebx
	subl	(%r11,%rax,4), %ebx
	movl	%ebx, %eax
	addl	%ebx, %ecx
	movl	%r10d, %ebx
	subl	%eax, %ebx
	addl	%ebx, %edi
	cmpl	%r8d, %ecx
	jg	.L2764
.L2757:
	leal	(%rdx,%rcx), %eax
	cltq
	movzwl	(%rsi,%rax,2), %ebx
	movq	%rbx, %rax
	cmpl	%ebx, %r9d
	jne	.L2769
	movslq	-60(%rbp), %rax
	testl	%eax, %eax
	js	.L2765
	movslq	%ecx, %rbx
	leaq	(%rsi,%rbx,2), %r15
	jmp	.L2759
	.p2align 4,,10
	.p2align 3
.L2770:
	subq	$1, %rax
	testl	%eax, %eax
	js	.L2765
.L2759:
	movzbl	0(%r13,%rax), %r12d
	movzwl	(%r15,%rax,2), %ebx
	movl	%eax, %r14d
	cmpl	%ebx, %r12d
	je	.L2770
	movl	-56(%rbp), %eax
	movl	-64(%rbp), %ebx
	subl	%r14d, %eax
	addl	%ebx, %ecx
	subl	%ebx, %eax
	addl	%eax, %edi
	testl	%edi, %edi
	jle	.L2768
	movq	-80(%rbp), %rbx
	movl	%ecx, -60(%rbp)
	movq	%rsi, -56(%rbp)
	movq	%rbx, %rdi
	call	_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv
	movl	-60(%rbp), %ecx
	movq	-56(%rbp), %rsi
	movq	%rbx, %rdi
	leaq	_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	-88(%rbp), %rdx
	movq	%rax, 24(%rbx)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi
	.p2align 4,,10
	.p2align 3
.L2764:
	.cfi_restore_state
	addq	$56, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2765:
	.cfi_restore_state
	addq	$56, %rsp
	movl	%ecx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2755:
	.cfi_restore_state
	addl	-56(%rbp), %ecx
	addl	-68(%rbp), %edi
	jmp	.L2768
	.cfi_endproc
.LFE24732:
	.size	_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi:
.LFB24488:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	16(%rdi), %rcx
	movq	8(%rdi), %r14
	movq	%rdi, -72(%rbp)
	movq	%rdx, -88(%rbp)
	movl	%ecx, %eax
	subl	%ecx, %ebx
	movq	%rcx, -80(%rbp)
	movl	%ecx, -56(%rbp)
	sall	$2, %eax
	cmpl	%ebx, %r9d
	jg	.L2776
	movl	$-9, %edx
	movq	%rsi, %r12
	subl	%eax, %edx
	movl	%edx, -60(%rbp)
	testl	%edx, %edx
	jg	.L2774
	movzbl	(%r14), %r15d
	leal	1(%rbx), %eax
	movl	%eax, -52(%rbp)
	movl	%r15d, %r13d
	jmp	.L2778
	.p2align 4,,10
	.p2align 3
.L2802:
	andq	$-2, %rax
	subq	%r12, %rax
	sarq	%rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	leaq	(%r12,%rdx,2), %rdx
	cmpw	%r13w, (%rdx)
	je	.L2777
	leal	1(%rax), %r9d
	cmpl	%eax, %ebx
	jle	.L2776
.L2778:
	movl	-52(%rbp), %edx
	movl	%r15d, %esi
	subl	%r9d, %edx
	movslq	%r9d, %r9
	movslq	%edx, %rdx
	leaq	(%r12,%r9,2), %rdi
	addq	%rdx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L2802
.L2776:
	movl	$-1, %r9d
.L2771:
	addq	$56, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2777:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L2776
	movl	-56(%rbp), %r8d
	movl	$1, %eax
	jmp	.L2781
	.p2align 4,,10
	.p2align 3
.L2803:
	leal	1(%rax), %ecx
	addq	$1, %rax
	cmpl	%eax, %r8d
	jle	.L2780
.L2781:
	movzbl	(%r14,%rax), %edi
	movzwl	(%rdx,%rax,2), %esi
	movl	%eax, %ecx
	cmpl	%esi, %edi
	je	.L2803
.L2780:
	cmpl	-56(%rbp), %ecx
	je	.L2771
	addl	$1, %r9d
	addl	-60(%rbp), %ecx
	cmpl	%r9d, %ebx
	jl	.L2776
	leal	1(%rcx), %eax
	movl	%eax, -60(%rbp)
	testl	%eax, %eax
	jle	.L2778
.L2774:
	movq	-72(%rbp), %rax
	movq	(%rax), %rdx
	movl	32(%rax), %r10d
	leaq	42372(%rdx), %rsi
	testl	%r10d, %r10d
	je	.L2804
	leal	-1(%r10), %eax
	addq	$43396, %rdx
	movd	%eax, %xmm0
	movq	%rsi, %rax
	pshufd	$0, %xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L2786:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L2786
.L2785:
	movl	-80(%rbp), %eax
	subl	$1, %eax
	cmpl	%r10d, %eax
	jle	.L2787
	movl	-80(%rbp), %edx
	movslq	%r10d, %rcx
	movq	-72(%rbp), %r8
	leaq	1(%rcx), %rax
	subl	$2, %edx
	subl	%r10d, %edx
	addq	%rax, %rdx
	jmp	.L2788
	.p2align 4,,10
	.p2align 3
.L2805:
	addq	$1, %rax
.L2788:
	movq	8(%r8), %rdi
	movzbl	(%rdi,%rcx), %edi
	movl	%ecx, (%rsi,%rdi,4)
	movq	%rax, %rcx
	cmpq	%rdx, %rax
	jne	.L2805
.L2787:
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %rdx
	movq	%r12, %rsi
	movl	%r9d, %ecx
	leaq	_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi(%rip), %rbx
	movq	%rbx, 24(%rax)
	addq	$56, %rsp
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi
.L2804:
	.cfi_restore_state
	leaq	42380(%rdx), %rdi
	movq	%rsi, %rcx
	movq	$-1, %rax
	movq	$-1, 42372(%rdx)
	movq	$-1, 43388(%rdx)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	jmp	.L2785
	.cfi_endproc
.LFE24488:
	.size	_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv,"axG",@progbits,_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv
	.type	_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv, @function
_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv:
.LFB24881:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r15
	movslq	32(%rdi), %r13
	movq	8(%rdi), %r9
	movq	(%rdi), %rdi
	leaq	0(,%r13,4), %rax
	movslq	%r15d, %r11
	movl	%r15d, %r10d
	leaq	43396(%rdi), %rdx
	addq	$44400, %rdi
	leaq	0(,%r11,4), %rsi
	subl	%r13d, %r10d
	movq	%rdx, %rbx
	subq	%rax, %rdi
	subq	%rax, %rbx
	leal	1(%r15), %eax
	leaq	(%rbx,%rsi), %r14
	addq	%rdi, %rsi
	cmpl	%r15d, %r13d
	jge	.L2807
	leal	-1(%r15), %ecx
	movl	%r15d, -52(%rbp)
	movl	%r15d, %r8d
	movq	%r13, %r12
	subl	%r13d, %ecx
	cmpl	$2, %ecx
	jbe	.L2831
	movl	%r10d, %ecx
	movd	%r10d, %xmm1
	shrl	$2, %ecx
	pshufd	$0, %xmm1, %xmm0
	salq	$4, %rcx
	addq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L2809:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L2809
	movl	%r10d, %ecx
	andl	$-4, %ecx
	leal	(%rcx,%r12), %edx
	cmpl	%r10d, %ecx
	je	.L2810
.L2808:
	movslq	%edx, %rcx
	movl	%r10d, (%rbx,%rcx,4)
	leal	1(%rdx), %ecx
	cmpl	%ecx, %r15d
	jle	.L2810
	movslq	%ecx, %rcx
	addl	$2, %edx
	movl	%r10d, (%rbx,%rcx,4)
	cmpl	%r15d, %edx
	jge	.L2810
	movslq	%edx, %rdx
	movl	%r10d, (%rbx,%rdx,4)
.L2810:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	movzwl	-2(%r9,%r11,2), %ecx
	movl	%r15d, %esi
	movq	%r13, -64(%rbp)
	movl	%ecx, %r11d
	movl	%ecx, %r13d
	.p2align 4,,10
	.p2align 3
.L2836:
	cmpl	%eax, %r8d
	jl	.L2812
.L2816:
	movslq	%eax, %rdx
	cmpw	%r11w, -2(%r9,%rdx,2)
	je	.L2812
	leaq	(%rbx,%rdx,4), %rcx
	cmpl	%r10d, (%rcx)
	je	.L2838
	movl	(%rdi,%rdx,4), %eax
	cmpl	%r8d, %eax
	jle	.L2816
.L2812:
	subl	$1, %esi
	leal	-1(%rax), %edx
	movslq	%esi, %rcx
	movl	%edx, (%rdi,%rcx,4)
	cmpl	%r8d, %edx
	je	.L2839
.L2817:
	cmpl	%esi, %r12d
	jge	.L2811
.L2841:
	movslq	%esi, %rax
	movzwl	-2(%r9,%rax,2), %r11d
	movl	%edx, %eax
	jmp	.L2836
	.p2align 4,,10
	.p2align 3
.L2838:
	subl	%esi, %eax
	movl	%eax, (%rcx)
	movl	(%rdi,%rdx,4), %eax
	jmp	.L2836
	.p2align 4,,10
	.p2align 3
.L2839:
	cmpl	%esi, %r12d
	jl	.L2823
	jmp	.L2818
	.p2align 4,,10
	.p2align 3
.L2840:
	cmpl	%r10d, (%r14)
	jne	.L2820
	movl	-52(%rbp), %edx
	subl	%ecx, %edx
	movl	%edx, (%r14)
.L2820:
	movl	%r8d, -4(%rdi,%rcx,4)
	subq	$1, %rcx
	subl	$1, %esi
	cmpl	%ecx, %r12d
	jge	.L2818
.L2823:
	movl	%ecx, %esi
	cmpw	%r13w, -2(%r9,%rcx,2)
	jne	.L2840
	cmpl	%r12d, %ecx
	jle	.L2806
	subl	$1, %esi
	leal	-2(%rax), %edx
	movslq	%esi, %rax
	movl	%edx, (%rdi,%rax,4)
	cmpl	%esi, %r12d
	jl	.L2841
	.p2align 4,,10
	.p2align 3
.L2811:
	movq	-64(%rbp), %r13
	cmpl	%r15d, %edx
	jge	.L2806
	.p2align 4,,10
	.p2align 3
.L2825:
	cmpl	%r10d, (%rbx,%r13,4)
	jne	.L2827
	movl	%edx, %eax
	subl	%r12d, %eax
	movl	%eax, (%rbx,%r13,4)
.L2827:
	cmpl	%r13d, %edx
	je	.L2842
	addq	$1, %r13
	cmpl	%r13d, %r8d
	jge	.L2825
.L2806:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2842:
	.cfi_restore_state
	movslq	%edx, %rdx
	addq	$1, %r13
	movl	(%rdi,%rdx,4), %edx
	cmpl	%r13d, %r8d
	jge	.L2825
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2818:
	.cfi_restore_state
	movl	%r8d, %edx
	jmp	.L2817
.L2807:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2831:
	.cfi_restore_state
	movl	%r13d, %edx
	jmp	.L2808
	.cfi_endproc
.LFE24881:
	.size	_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv, .-_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv
	.section	.text._ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi:
.LFB24737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	movq	8(%rdi), %r14
	movq	%rdx, -72(%rbp)
	movq	(%rdi), %r10
	leal	-1(%rax), %edx
	movl	%eax, %r8d
	movl	%eax, -64(%rbp)
	movslq	%edx, %r9
	movl	%edx, %ebx
	leaq	42372(%r10), %rdi
	negl	%r8d
	movzwl	(%r14,%r9,2), %r9d
	movzbl	%r9b, %r11d
	subl	42372(%r10,%r11,4), %ebx
	movl	%r15d, %r10d
	movl	$1, %r11d
	movl	%ebx, -60(%rbp)
	leal	-2(%rax), %ebx
	subl	%eax, %r10d
	movl	%ebx, -56(%rbp)
	jmp	.L2856
	.p2align 4,,10
	.p2align 3
.L2858:
	movl	%edx, %ebx
	subl	(%rdi,%rax,4), %ebx
	movl	%ebx, %eax
	addl	%ebx, %ecx
	movl	%r11d, %ebx
	subl	%eax, %ebx
	addl	%ebx, %r8d
.L2856:
	cmpl	%r10d, %ecx
	jg	.L2857
	leal	(%rdx,%rcx), %eax
	cltq
	movzbl	(%rsi,%rax), %ebx
	movq	%rbx, %rax
	cmpw	%bx, %r9w
	jne	.L2858
	movslq	-56(%rbp), %rax
	testl	%eax, %eax
	js	.L2853
	movslq	%ecx, %r13
	addq	%rsi, %r13
	jmp	.L2848
	.p2align 4,,10
	.p2align 3
.L2859:
	subq	$1, %rax
	testl	%eax, %eax
	js	.L2853
.L2848:
	movzbl	0(%r13,%rax), %ebx
	movl	%eax, %r15d
	cmpw	%bx, (%r14,%rax,2)
	je	.L2859
	movl	-64(%rbp), %eax
	movl	-60(%rbp), %ebx
	subl	%r15d, %eax
	addl	%ebx, %ecx
	subl	%ebx, %eax
	addl	%eax, %r8d
	testl	%r8d, %r8d
	jle	.L2856
	movq	%r12, %rdi
	movl	%ecx, -60(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv
	movl	-60(%rbp), %ecx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	leaq	_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	-72(%rbp), %rdx
	movq	%rax, 24(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi
	.p2align 4,,10
	.p2align 3
.L2857:
	.cfi_restore_state
	addq	$40, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2853:
	.cfi_restore_state
	addq	$40, %rsp
	movl	%ecx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24737:
	.size	_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi:
.LFB24492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	16(%rdi), %rcx
	movq	8(%rdi), %r14
	movq	%rdi, -72(%rbp)
	movq	%rdx, -88(%rbp)
	movl	%ecx, %eax
	subl	%ecx, %ebx
	movq	%rcx, -80(%rbp)
	movl	%ecx, -56(%rbp)
	sall	$2, %eax
	cmpl	%ebx, %r9d
	jg	.L2865
	movl	$-9, %edx
	movq	%rsi, %r13
	subl	%eax, %edx
	movl	%edx, -60(%rbp)
	testl	%edx, %edx
	jg	.L2863
	movzwl	(%r14), %eax
	leal	1(%rbx), %ecx
	movl	%ecx, -52(%rbp)
	movzbl	%ah, %edx
	movl	%eax, %r15d
	cmpb	%dl, %al
	cmovbe	%edx, %eax
	movzbl	%al, %r12d
	jmp	.L2867
	.p2align 4,,10
	.p2align 3
.L2892:
	subq	%r13, %rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	addq	%r13, %rdx
	cmpb	%r15b, (%rdx)
	je	.L2866
	leal	1(%rax), %r9d
	cmpl	%eax, %ebx
	jle	.L2865
.L2867:
	movl	-52(%rbp), %edx
	movslq	%r9d, %rdi
	movl	%r12d, %esi
	addq	%r13, %rdi
	subl	%r9d, %edx
	movslq	%edx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L2892
.L2865:
	movl	$-1, %r9d
.L2860:
	addq	$56, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2866:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L2865
	movl	-56(%rbp), %edi
	movl	$1, %eax
	jmp	.L2870
	.p2align 4,,10
	.p2align 3
.L2893:
	leal	1(%rax), %ecx
	addq	$1, %rax
	cmpl	%eax, %edi
	jle	.L2869
.L2870:
	movzbl	(%rdx,%rax), %esi
	movl	%eax, %ecx
	cmpw	%si, (%r14,%rax,2)
	je	.L2893
.L2869:
	cmpl	-56(%rbp), %ecx
	je	.L2860
	addl	$1, %r9d
	addl	-60(%rbp), %ecx
	cmpl	%r9d, %ebx
	jl	.L2865
	leal	1(%rcx), %eax
	movl	%eax, -60(%rbp)
	testl	%eax, %eax
	jle	.L2867
.L2863:
	movq	-72(%rbp), %rax
	movq	(%rax), %rcx
	movl	32(%rax), %edx
	leaq	42372(%rcx), %rsi
	testl	%edx, %edx
	je	.L2894
	leal	-1(%rdx), %eax
	addq	$43396, %rcx
	movd	%eax, %xmm0
	movq	%rsi, %rax
	pshufd	$0, %xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L2875:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L2875
.L2874:
	movl	-80(%rbp), %edi
	movslq	%edx, %rax
	movq	-72(%rbp), %r8
	addq	%rax, %rax
	subl	$1, %edi
	cmpl	%edx, %edi
	jle	.L2876
	.p2align 4,,10
	.p2align 3
.L2877:
	movq	8(%r8), %rcx
	movzbl	(%rcx,%rax), %ecx
	addq	$2, %rax
	movl	%edx, (%rsi,%rcx,4)
	addl	$1, %edx
	cmpl	%edi, %edx
	jne	.L2877
.L2876:
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %rdx
	movq	%r13, %rsi
	movl	%r9d, %ecx
	leaq	_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi(%rip), %rbx
	movq	%rbx, 24(%rax)
	addq	$56, %rsp
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi
.L2894:
	.cfi_restore_state
	leaq	42380(%rcx), %rdi
	movq	$-1, %rax
	movq	$-1, 42372(%rcx)
	movq	$-1, 43388(%rcx)
	andq	$-8, %rdi
	movq	%rsi, %rcx
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	jmp	.L2874
	.cfi_endproc
.LFE24492:
	.size	_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv,"axG",@progbits,_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv
	.type	_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv, @function
_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv:
.LFB24886:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r15
	movslq	32(%rdi), %r13
	movq	8(%rdi), %r9
	movq	(%rdi), %rdi
	leaq	0(,%r13,4), %rax
	movslq	%r15d, %r11
	movl	%r15d, %r10d
	leaq	43396(%rdi), %rdx
	addq	$44400, %rdi
	leaq	0(,%r11,4), %rsi
	subl	%r13d, %r10d
	movq	%rdx, %rbx
	subq	%rax, %rdi
	subq	%rax, %rbx
	leal	1(%r15), %eax
	leaq	(%rbx,%rsi), %r14
	addq	%rdi, %rsi
	cmpl	%r15d, %r13d
	jge	.L2896
	leal	-1(%r15), %ecx
	movl	%r15d, -52(%rbp)
	movl	%r15d, %r8d
	movq	%r13, %r12
	subl	%r13d, %ecx
	cmpl	$2, %ecx
	jbe	.L2920
	movl	%r10d, %ecx
	movd	%r10d, %xmm1
	shrl	$2, %ecx
	pshufd	$0, %xmm1, %xmm0
	salq	$4, %rcx
	addq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L2898:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L2898
	movl	%r10d, %ecx
	andl	$-4, %ecx
	leal	(%rcx,%r12), %edx
	cmpl	%r10d, %ecx
	je	.L2899
.L2897:
	movslq	%edx, %rcx
	movl	%r10d, (%rbx,%rcx,4)
	leal	1(%rdx), %ecx
	cmpl	%ecx, %r15d
	jle	.L2899
	movslq	%ecx, %rcx
	addl	$2, %edx
	movl	%r10d, (%rbx,%rcx,4)
	cmpl	%r15d, %edx
	jge	.L2899
	movslq	%edx, %rdx
	movl	%r10d, (%rbx,%rdx,4)
.L2899:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	movzwl	-2(%r9,%r11,2), %ecx
	movl	%r15d, %esi
	movq	%r13, -64(%rbp)
	movl	%ecx, %r11d
	movl	%ecx, %r13d
	.p2align 4,,10
	.p2align 3
.L2925:
	cmpl	%eax, %r8d
	jl	.L2901
.L2905:
	movslq	%eax, %rdx
	cmpw	%r11w, -2(%r9,%rdx,2)
	je	.L2901
	leaq	(%rbx,%rdx,4), %rcx
	cmpl	%r10d, (%rcx)
	je	.L2927
	movl	(%rdi,%rdx,4), %eax
	cmpl	%r8d, %eax
	jle	.L2905
.L2901:
	subl	$1, %esi
	leal	-1(%rax), %edx
	movslq	%esi, %rcx
	movl	%edx, (%rdi,%rcx,4)
	cmpl	%r8d, %edx
	je	.L2928
.L2906:
	cmpl	%esi, %r12d
	jge	.L2900
.L2930:
	movslq	%esi, %rax
	movzwl	-2(%r9,%rax,2), %r11d
	movl	%edx, %eax
	jmp	.L2925
	.p2align 4,,10
	.p2align 3
.L2927:
	subl	%esi, %eax
	movl	%eax, (%rcx)
	movl	(%rdi,%rdx,4), %eax
	jmp	.L2925
	.p2align 4,,10
	.p2align 3
.L2928:
	cmpl	%esi, %r12d
	jl	.L2912
	jmp	.L2907
	.p2align 4,,10
	.p2align 3
.L2929:
	cmpl	%r10d, (%r14)
	jne	.L2909
	movl	-52(%rbp), %edx
	subl	%ecx, %edx
	movl	%edx, (%r14)
.L2909:
	movl	%r8d, -4(%rdi,%rcx,4)
	subq	$1, %rcx
	subl	$1, %esi
	cmpl	%ecx, %r12d
	jge	.L2907
.L2912:
	movl	%ecx, %esi
	cmpw	%r13w, -2(%r9,%rcx,2)
	jne	.L2929
	cmpl	%r12d, %ecx
	jle	.L2895
	subl	$1, %esi
	leal	-2(%rax), %edx
	movslq	%esi, %rax
	movl	%edx, (%rdi,%rax,4)
	cmpl	%esi, %r12d
	jl	.L2930
	.p2align 4,,10
	.p2align 3
.L2900:
	movq	-64(%rbp), %r13
	cmpl	%r15d, %edx
	jge	.L2895
	.p2align 4,,10
	.p2align 3
.L2914:
	cmpl	%r10d, (%rbx,%r13,4)
	jne	.L2916
	movl	%edx, %eax
	subl	%r12d, %eax
	movl	%eax, (%rbx,%r13,4)
.L2916:
	cmpl	%r13d, %edx
	je	.L2931
	addq	$1, %r13
	cmpl	%r13d, %r8d
	jge	.L2914
.L2895:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2931:
	.cfi_restore_state
	movslq	%edx, %rdx
	addq	$1, %r13
	movl	(%rdi,%rdx,4), %edx
	cmpl	%r13d, %r8d
	jge	.L2914
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2907:
	.cfi_restore_state
	movl	%r8d, %edx
	jmp	.L2906
.L2896:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2920:
	.cfi_restore_state
	movl	%r13d, %edx
	jmp	.L2897
	.cfi_endproc
.LFE24886:
	.size	_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv, .-_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv
	.section	.text._ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi:
.LFB24742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	movq	8(%rdi), %rbx
	movq	%rdx, -72(%rbp)
	movq	(%rdi), %r10
	leal	-1(%rax), %edx
	movl	%eax, %r8d
	movl	%eax, -60(%rbp)
	movslq	%edx, %r9
	leaq	42372(%r10), %rdi
	movl	%edx, %r15d
	negl	%r8d
	movzwl	(%rbx,%r9,2), %r9d
	movzbl	%r9b, %r11d
	subl	42372(%r10,%r11,4), %r15d
	movl	%r14d, %r10d
	movl	$1, %r11d
	movl	%r15d, -56(%rbp)
	subl	%eax, %r10d
	leal	-2(%rax), %r15d
	jmp	.L2945
	.p2align 4,,10
	.p2align 3
.L2947:
	movzbl	%al, %eax
	movl	%edx, %r14d
	subl	(%rdi,%rax,4), %r14d
	movl	%r14d, %eax
	addl	%r14d, %ecx
	movl	%r11d, %r14d
	subl	%eax, %r14d
	addl	%r14d, %r8d
.L2945:
	cmpl	%r10d, %ecx
	jg	.L2946
	leal	(%rdx,%rcx), %eax
	cltq
	movzwl	(%rsi,%rax,2), %eax
	cmpw	%r9w, %ax
	jne	.L2947
	testl	%r15d, %r15d
	js	.L2942
	movslq	%ecx, %r13
	movl	%edx, -64(%rbp)
	movslq	%r15d, %rax
	leaq	(%rsi,%r13,2), %r14
	jmp	.L2937
	.p2align 4,,10
	.p2align 3
.L2948:
	subq	$1, %rax
	testl	%eax, %eax
	js	.L2942
.L2937:
	movzwl	(%r14,%rax,2), %edx
	movl	%eax, %r13d
	cmpw	%dx, (%rbx,%rax,2)
	je	.L2948
	movl	-60(%rbp), %eax
	movl	-56(%rbp), %r14d
	movl	-64(%rbp), %edx
	subl	%r13d, %eax
	addl	%r14d, %ecx
	subl	%r14d, %eax
	addl	%eax, %r8d
	testl	%r8d, %r8d
	jle	.L2945
	movq	%r12, %rdi
	movl	%ecx, -60(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv
	movl	-60(%rbp), %ecx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	leaq	_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	-72(%rbp), %rdx
	movq	%rax, 24(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi
	.p2align 4,,10
	.p2align 3
.L2946:
	.cfi_restore_state
	addq	$40, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2942:
	.cfi_restore_state
	addq	$40, %rsp
	movl	%ecx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24742:
	.size	_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi:
.LFB24496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	16(%rdi), %rsi
	movq	8(%rdi), %r15
	movq	%rdi, -72(%rbp)
	movq	%rdx, -88(%rbp)
	movl	%esi, %eax
	subl	%esi, %ebx
	movq	%rsi, -80(%rbp)
	movl	%esi, -56(%rbp)
	sall	$2, %eax
	cmpl	%ebx, %ecx
	jg	.L2954
	movl	$-9, %edx
	movl	%ecx, %r9d
	subl	%eax, %edx
	movl	%edx, -60(%rbp)
	testl	%edx, %edx
	jg	.L2952
	movzwl	(%r15), %r14d
	leal	1(%rbx), %eax
	movl	%eax, -52(%rbp)
	movl	%r14d, %eax
	movzbl	%ah, %eax
	cmpb	%al, %r14b
	cmova	%r14d, %eax
	movzbl	%al, %r12d
	jmp	.L2956
	.p2align 4,,10
	.p2align 3
.L2981:
	andq	$-2, %rax
	subq	%r13, %rax
	sarq	%rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	leaq	0(%r13,%rdx,2), %rdx
	cmpw	%r14w, (%rdx)
	je	.L2955
	leal	1(%rax), %r9d
	cmpl	%eax, %ebx
	jle	.L2954
.L2956:
	movl	-52(%rbp), %edx
	movl	%r12d, %esi
	subl	%r9d, %edx
	movslq	%r9d, %r9
	movslq	%edx, %rdx
	leaq	0(%r13,%r9,2), %rdi
	addq	%rdx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L2981
.L2954:
	movl	$-1, %r9d
.L2949:
	addq	$56, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2955:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L2954
	movl	-56(%rbp), %esi
	movl	$1, %eax
	jmp	.L2959
	.p2align 4,,10
	.p2align 3
.L2982:
	leal	1(%rax), %ecx
	addq	$1, %rax
	cmpl	%eax, %esi
	jle	.L2958
.L2959:
	movzwl	(%rdx,%rax,2), %edi
	movl	%eax, %ecx
	cmpw	%di, (%r15,%rax,2)
	je	.L2982
.L2958:
	cmpl	-56(%rbp), %ecx
	je	.L2949
	addl	$1, %r9d
	addl	-60(%rbp), %ecx
	cmpl	%r9d, %ebx
	jl	.L2954
	leal	1(%rcx), %eax
	movl	%eax, -60(%rbp)
	testl	%eax, %eax
	jle	.L2956
.L2952:
	movq	-72(%rbp), %rax
	movq	(%rax), %rcx
	movl	32(%rax), %edx
	leaq	42372(%rcx), %rsi
	testl	%edx, %edx
	je	.L2983
	leal	-1(%rdx), %eax
	addq	$43396, %rcx
	movd	%eax, %xmm0
	movq	%rsi, %rax
	pshufd	$0, %xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L2964:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L2964
.L2963:
	movl	-80(%rbp), %edi
	movslq	%edx, %rax
	movq	-72(%rbp), %r8
	addq	%rax, %rax
	subl	$1, %edi
	cmpl	%edx, %edi
	jle	.L2965
	.p2align 4,,10
	.p2align 3
.L2966:
	movq	8(%r8), %rcx
	movzbl	(%rcx,%rax), %ecx
	addq	$2, %rax
	movl	%edx, (%rsi,%rcx,4)
	addl	$1, %edx
	cmpl	%edi, %edx
	jne	.L2966
.L2965:
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %rdx
	movq	%r13, %rsi
	movl	%r9d, %ecx
	leaq	_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi(%rip), %rbx
	movq	%rbx, 24(%rax)
	addq	$56, %rsp
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi
.L2983:
	.cfi_restore_state
	leaq	42380(%rcx), %rdi
	movq	$-1, %rax
	movq	$-1, 42372(%rcx)
	movq	$-1, 43388(%rcx)
	andq	$-8, %rdi
	movq	%rsi, %rcx
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	jmp	.L2963
	.cfi_endproc
.LFE24496:
	.size	_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v86String26ExternalStringResourceBaseD2Ev,"axG",@progbits,_ZN2v86String26ExternalStringResourceBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v86String26ExternalStringResourceBaseD2Ev
	.type	_ZN2v86String26ExternalStringResourceBaseD2Ev, @function
_ZN2v86String26ExternalStringResourceBaseD2Ev:
.LFB25073:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE25073:
	.size	_ZN2v86String26ExternalStringResourceBaseD2Ev, .-_ZN2v86String26ExternalStringResourceBaseD2Ev
	.weak	_ZN2v86String26ExternalStringResourceBaseD1Ev
	.set	_ZN2v86String26ExternalStringResourceBaseD1Ev,_ZN2v86String26ExternalStringResourceBaseD2Ev
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE, @function
_GLOBAL__sub_I__ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE:
.LFB25104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25104:
	.size	_GLOBAL__sub_I__ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE, .-_GLOBAL__sub_I__ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE
	.weak	_ZTVN2v88internal16FlatStringReaderE
	.section	.data.rel.ro.local._ZTVN2v88internal16FlatStringReaderE,"awG",@progbits,_ZTVN2v88internal16FlatStringReaderE,comdat
	.align 8
	.type	_ZTVN2v88internal16FlatStringReaderE, @object
	.size	_ZTVN2v88internal16FlatStringReaderE, 48
_ZTVN2v88internal16FlatStringReaderE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal16FlatStringReaderD1Ev
	.quad	_ZN2v88internal16FlatStringReaderD0Ev
	.quad	_ZN2v88internal11Relocatable15IterateInstanceEPNS0_11RootVisitorE
	.quad	_ZN2v88internal16FlatStringReader21PostGarbageCollectionEv
	.section	.rodata._ZN2v88internalL15kAsciiCharFlagsE,"a"
	.align 32
	.type	_ZN2v88internalL15kAsciiCharFlagsE, @object
	.size	_ZN2v88internalL15kAsciiCharFlagsE, 128
_ZN2v88internalL15kAsciiCharFlagsE:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\f\b\f\f\b"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\f"
	.string	""
	.string	""
	.string	"\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\002\002\002\002\002\002\002\002\002\002"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"
	.string	"\003"
	.string	""
	.string	"\003"
	.string	"\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC12:
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC16:
	.long	0
	.long	1072693248
	.align 8
.LC17:
	.long	4292870144
	.long	1106247679
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
