	.file	"wasm-import-wrapper-cache.cc"
	.text
	.section	.text.unlikely._ZNK2v88internal4wasm22WasmImportWrapperCache3GetENS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEE,"ax",@progbits
	.align 2
.LCOLDB0:
	.section	.text._ZNK2v88internal4wasm22WasmImportWrapperCache3GetENS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEE,"ax",@progbits
.LHOTB0:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm22WasmImportWrapperCache3GetENS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEE
	.type	_ZNK2v88internal4wasm22WasmImportWrapperCache3GetENS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEE, @function
_ZNK2v88internal4wasm22WasmImportWrapperCache3GetENS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEE:
.LFB10007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movzbl	%sil, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r15, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	8(%rdx), %rdi
	movq	(%rdx), %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	16(%rbx), %r12
	movq	8(%rbx), %rdx
	addq	%r12, %rdx
	addq	(%rbx), %rdx
	cmpq	%rdx, %r12
	je	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%rax, %rdi
	movq	%rdx, -64(%rbp)
	addq	$1, %r12
	call	_ZN2v84base10hash_valueEm@PLT
	movzbl	-1(%r12), %esi
	xorl	%edi, %edi
	movq	%rax, -56(%rbp)
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rdi
	movq	%r8, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	-64(%rbp), %rdx
	cmpq	%rdx, %r12
	jne	.L3
.L2:
	movq	%rax, %rsi
	xorl	%edi, %edi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	48(%r14), %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%rdi
	movq	40(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L4
	movq	(%rax), %rcx
	movq	32(%rcx), %r8
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L5:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L4
	movq	32(%rcx), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L18
.L7:
	cmpq	%r8, %rsi
	jne	.L5
	cmpb	8(%rcx), %r13b
	jne	.L5
	cmpq	16(%rcx), %rbx
	jne	.L5
	movq	24(%rcx), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L18:
	.cfi_restore_state
	jmp	.L4
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal4wasm22WasmImportWrapperCache3GetENS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEE
	.cfi_startproc
	.type	_ZNK2v88internal4wasm22WasmImportWrapperCache3GetENS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEE.cold, @function
_ZNK2v88internal4wasm22WasmImportWrapperCache3GetENS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEE.cold:
.LFSB10007:
.L4:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	24, %rax
	ud2
	.cfi_endproc
.LFE10007:
	.section	.text._ZNK2v88internal4wasm22WasmImportWrapperCache3GetENS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEE
	.size	_ZNK2v88internal4wasm22WasmImportWrapperCache3GetENS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEE, .-_ZNK2v88internal4wasm22WasmImportWrapperCache3GetENS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEE
	.section	.text.unlikely._ZNK2v88internal4wasm22WasmImportWrapperCache3GetENS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEE
	.size	_ZNK2v88internal4wasm22WasmImportWrapperCache3GetENS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEE.cold, .-_ZNK2v88internal4wasm22WasmImportWrapperCache3GetENS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEE.cold
.LCOLDE0:
	.section	.text._ZNK2v88internal4wasm22WasmImportWrapperCache3GetENS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEE
.LHOTE0:
	.section	.text._ZNSt8__detail9_Map_baseISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS3_9SignatureINS3_4wasm9ValueTypeEEEES1_IKSB_PNS7_8WasmCodeEESaISF_ENS_10_Select1stESt8equal_toISB_ENS7_22WasmImportWrapperCache12CacheKeyHashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSC_,"axG",@progbits,_ZNSt8__detail9_Map_baseISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS3_9SignatureINS3_4wasm9ValueTypeEEEES1_IKSB_PNS7_8WasmCodeEESaISF_ENS_10_Select1stESt8equal_toISB_ENS7_22WasmImportWrapperCache12CacheKeyHashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSC_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS3_9SignatureINS3_4wasm9ValueTypeEEEES1_IKSB_PNS7_8WasmCodeEESaISF_ENS_10_Select1stESt8equal_toISB_ENS7_22WasmImportWrapperCache12CacheKeyHashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSC_
	.type	_ZNSt8__detail9_Map_baseISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS3_9SignatureINS3_4wasm9ValueTypeEEEES1_IKSB_PNS7_8WasmCodeEESaISF_ENS_10_Select1stESt8equal_toISB_ENS7_22WasmImportWrapperCache12CacheKeyHashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSC_, @function
_ZNSt8__detail9_Map_baseISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS3_9SignatureINS3_4wasm9ValueTypeEEEES1_IKSB_PNS7_8WasmCodeEESaISF_ENS_10_Select1stESt8equal_toISB_ENS7_22WasmImportWrapperCache12CacheKeyHashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSC_:
.LFB11184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %r15
	movzbl	(%rsi), %r14d
	movq	8(%r15), %rdi
	movq	(%r15), %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	16(%r15), %rbx
	movq	8(%r15), %rdx
	addq	%rbx, %rdx
	addq	(%r15), %rdx
	cmpq	%rdx, %rbx
	je	.L20
	movq	%rdx, %r15
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%rax, %rdi
	addq	$1, %rbx
	call	_ZN2v84base10hash_valueEm@PLT
	movzbl	-1(%rbx), %esi
	xorl	%edi, %edi
	movq	%rax, -56(%rbp)
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rdi
	movq	%r8, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	cmpq	%r15, %rbx
	jne	.L21
.L20:
	movq	%rax, %rsi
	xorl	%edi, %edi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	8(%r12), %rsi
	xorl	%edx, %edx
	movq	%rax, %r14
	divq	%rsi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	leaq	0(,%rdx,8), %r15
	testq	%rax, %rax
	je	.L22
	movq	(%rax), %rcx
	movq	32(%rcx), %rdi
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L23:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L22
	movq	32(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L22
.L25:
	cmpq	%r14, %rdi
	jne	.L23
	movzbl	8(%rcx), %eax
	cmpb	%al, 0(%r13)
	jne	.L23
	movq	16(%rcx), %rax
	cmpq	%rax, 8(%r13)
	jne	.L23
	addq	$24, %rsp
	leaq	24(%rcx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movl	$40, %edi
	call	_Znwm@PLT
	movdqu	0(%r13), %xmm0
	movq	24(%r12), %rdx
	leaq	32(%r12), %rdi
	movq	$0, (%rax)
	movq	8(%r12), %rsi
	movl	$1, %ecx
	movq	%rax, %rbx
	movq	$0, 24(%rax)
	movups	%xmm0, 8(%rax)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r13
	testb	%al, %al
	jne	.L26
	movq	(%r12), %r8
	movq	%r14, 32(%rbx)
	leaq	(%r8,%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L36
.L61:
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	(%rax), %rax
	movq	%rbx, (%rax)
.L37:
	addq	$1, 24(%r12)
	addq	$24, %rsp
	leaq	24(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L59
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L60
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%r12), %r10
	movq	%rax, %r8
.L29:
	movq	16(%r12), %rsi
	movq	$0, 16(%r12)
	testq	%rsi, %rsi
	je	.L31
	xorl	%edi, %edi
	leaq	16(%r12), %r9
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L33:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L34:
	testq	%rsi, %rsi
	je	.L31
.L32:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	32(%rcx), %rax
	divq	%r13
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L33
	movq	16(%r12), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r12)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L40
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L32
	.p2align 4,,10
	.p2align 3
.L31:
	movq	(%r12), %rdi
	cmpq	%r10, %rdi
	je	.L35
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L35:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r13, 8(%r12)
	divq	%r13
	movq	%r8, (%r12)
	movq	%r14, 32(%rbx)
	leaq	0(,%rdx,8), %r15
	leaq	(%r8,%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L61
.L36:
	movq	16(%r12), %rdx
	movq	%rbx, 16(%r12)
	movq	%rdx, (%rbx)
	testq	%rdx, %rdx
	je	.L38
	movq	32(%rdx), %rax
	xorl	%edx, %edx
	divq	8(%r12)
	movq	%rbx, (%r8,%rdx,8)
	movq	(%r12), %rax
	addq	%r15, %rax
.L38:
	leaq	16(%r12), %rdx
	movq	%rdx, (%rax)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%rdx, %rdi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L59:
	movq	$0, 48(%r12)
	leaq	48(%r12), %r8
	movq	%r8, %r10
	jmp	.L29
.L60:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE11184:
	.size	_ZNSt8__detail9_Map_baseISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS3_9SignatureINS3_4wasm9ValueTypeEEEES1_IKSB_PNS7_8WasmCodeEESaISF_ENS_10_Select1stESt8equal_toISB_ENS7_22WasmImportWrapperCache12CacheKeyHashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSC_, .-_ZNSt8__detail9_Map_baseISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS3_9SignatureINS3_4wasm9ValueTypeEEEES1_IKSB_PNS7_8WasmCodeEESaISF_ENS_10_Select1stESt8equal_toISB_ENS7_22WasmImportWrapperCache12CacheKeyHashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSC_
	.section	.text._ZN2v88internal4wasm22WasmImportWrapperCache17ModificationScopeixERKSt4pairINS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm22WasmImportWrapperCache17ModificationScopeixERKSt4pairINS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEEE
	.type	_ZN2v88internal4wasm22WasmImportWrapperCache17ModificationScopeixERKSt4pairINS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEEE, @function
_ZN2v88internal4wasm22WasmImportWrapperCache17ModificationScopeixERKSt4pairINS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEEE:
.LFB10005:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	addq	$40, %rdi
	jmp	_ZNSt8__detail9_Map_baseISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS3_9SignatureINS3_4wasm9ValueTypeEEEES1_IKSB_PNS7_8WasmCodeEESaISF_ENS_10_Select1stESt8equal_toISB_ENS7_22WasmImportWrapperCache12CacheKeyHashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSC_
	.cfi_endproc
.LFE10005:
	.size	_ZN2v88internal4wasm22WasmImportWrapperCache17ModificationScopeixERKSt4pairINS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEEE, .-_ZN2v88internal4wasm22WasmImportWrapperCache17ModificationScopeixERKSt4pairINS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEEE
	.section	.text._ZN2v88internal4wasm22WasmImportWrapperCacheixERKSt4pairINS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm22WasmImportWrapperCacheixERKSt4pairINS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEEE
	.type	_ZN2v88internal4wasm22WasmImportWrapperCacheixERKSt4pairINS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEEE, @function
_ZN2v88internal4wasm22WasmImportWrapperCacheixERKSt4pairINS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEEE:
.LFB10006:
	.cfi_startproc
	endbr64
	addq	$40, %rdi
	jmp	_ZNSt8__detail9_Map_baseISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS3_9SignatureINS3_4wasm9ValueTypeEEEES1_IKSB_PNS7_8WasmCodeEESaISF_ENS_10_Select1stESt8equal_toISB_ENS7_22WasmImportWrapperCache12CacheKeyHashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERSC_
	.cfi_endproc
.LFE10006:
	.size	_ZN2v88internal4wasm22WasmImportWrapperCacheixERKSt4pairINS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEEE, .-_ZN2v88internal4wasm22WasmImportWrapperCacheixERKSt4pairINS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEEE
	.section	.rodata._ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_.str1.1,"aMS",@progbits,1
.LC1:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB11224:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L78
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L74
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L79
.L66:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L73:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L80
	testq	%r13, %r13
	jg	.L69
	testq	%r9, %r9
	jne	.L72
.L70:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L69
.L72:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L79:
	testq	%rsi, %rsi
	jne	.L67
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L69:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L70
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L74:
	movl	$8, %r14d
	jmp	.L66
.L78:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L67:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L66
	.cfi_endproc
.LFE11224:
	.size	_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.rodata._ZN2v88internal4wasm22WasmImportWrapperCacheD2Ev.str1.1,"aMS",@progbits,1
.LC2:
	.string	"vector::reserve"
	.section	.text._ZN2v88internal4wasm22WasmImportWrapperCacheD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm22WasmImportWrapperCacheD2Ev
	.type	_ZN2v88internal4wasm22WasmImportWrapperCacheD2Ev, @function
_ZN2v88internal4wasm22WasmImportWrapperCacheD2Ev:
.LFB10030:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	64(%rdi), %rax
	movq	$0, -48(%rbp)
	movaps	%xmm0, -64(%rbp)
	cmpq	%rdx, %rax
	ja	.L116
	movq	%rdi, %r13
	xorl	%r12d, %r12d
	testq	%rax, %rax
	jne	.L117
	movq	56(%r13), %rbx
	testq	%rbx, %rbx
	je	.L98
.L120:
	leaq	-64(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L90:
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L88
	cmpq	%r12, -48(%rbp)
	je	.L89
	movq	%rax, (%r12)
	movq	-56(%rbp), %rax
	leaq	8(%rax), %r12
	movq	%r12, -56(%rbp)
.L88:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L90
	movq	-64(%rbp), %rdi
	movq	%r12, %rsi
	movabsq	$2305843009213693951, %r12
	subq	%rdi, %rsi
	sarq	$3, %rsi
	movslq	%esi, %rsi
	andq	%r12, %rsi
.L87:
	call	_ZN2v88internal4wasm8WasmCode17DecrementRefCountENS0_6VectorIKPS2_EE@PLT
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L91
	call	_ZdlPv@PLT
.L91:
	movq	56(%r13), %rbx
	testq	%rbx, %rbx
	je	.L95
	.p2align 4,,10
	.p2align 3
.L92:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L92
.L95:
	movq	48(%r13), %rax
	movq	40(%r13), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	40(%r13), %rdi
	leaq	88(%r13), %rax
	movq	$0, 64(%r13)
	movq	$0, 56(%r13)
	cmpq	%rax, %rdi
	je	.L93
	call	_ZdlPv@PLT
.L93:
	movq	%r13, %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L118
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	movq	%r12, %rsi
	leaq	24(%rbx), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	-56(%rbp), %r12
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L117:
	leaq	0(,%rax,8), %rbx
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	-64(%rbp), %r14
	movq	-56(%rbp), %rdx
	movq	%rax, %r12
	subq	%r14, %rdx
	testq	%rdx, %rdx
	jg	.L119
	testq	%r14, %r14
	jne	.L85
.L86:
	leaq	(%r12,%rbx), %rax
	movq	%r12, %xmm0
	movq	56(%r13), %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -48(%rbp)
	movaps	%xmm0, -64(%rbp)
	testq	%rbx, %rbx
	jne	.L120
.L98:
	movq	%r12, %rdi
	xorl	%esi, %esi
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L119:
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	memmove@PLT
.L85:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	jmp	.L86
.L118:
	call	__stack_chk_fail@PLT
.L116:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10030:
	.size	_ZN2v88internal4wasm22WasmImportWrapperCacheD2Ev, .-_ZN2v88internal4wasm22WasmImportWrapperCacheD2Ev
	.globl	_ZN2v88internal4wasm22WasmImportWrapperCacheD1Ev
	.set	_ZN2v88internal4wasm22WasmImportWrapperCacheD1Ev,_ZN2v88internal4wasm22WasmImportWrapperCacheD2Ev
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal4wasm22WasmImportWrapperCache17ModificationScopeixERKSt4pairINS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal4wasm22WasmImportWrapperCache17ModificationScopeixERKSt4pairINS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEEE, @function
_GLOBAL__sub_I__ZN2v88internal4wasm22WasmImportWrapperCache17ModificationScopeixERKSt4pairINS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEEE:
.LFB11942:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE11942:
	.size	_GLOBAL__sub_I__ZN2v88internal4wasm22WasmImportWrapperCache17ModificationScopeixERKSt4pairINS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEEE, .-_GLOBAL__sub_I__ZN2v88internal4wasm22WasmImportWrapperCache17ModificationScopeixERKSt4pairINS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal4wasm22WasmImportWrapperCache17ModificationScopeixERKSt4pairINS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEEE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
