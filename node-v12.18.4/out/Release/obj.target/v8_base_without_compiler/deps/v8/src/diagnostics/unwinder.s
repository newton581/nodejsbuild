	.file	"unwinder.cc"
	.text
	.section	.text._ZN2v88Unwinder17TryUnwindV8FramesERKNS_11UnwindStateEPNS_13RegisterStateEPKv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88Unwinder17TryUnwindV8FramesERKNS_11UnwindStateEPNS_13RegisterStateEPKv
	.type	_ZN2v88Unwinder17TryUnwindV8FramesERKNS_11UnwindStateEPNS_13RegisterStateEPKv, @function
_ZN2v88Unwinder17TryUnwindV8FramesERKNS_11UnwindStateEPNS_13RegisterStateEPKv:
.LFB3947:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L45
	movq	(%rdi), %r10
	movq	8(%rdi), %r11
	addq	%r10, %r11
	cmpq	%r10, %rcx
	jb	.L14
	cmpq	%r11, %rcx
	jnb	.L14
	movq	32(%rdi), %r8
	movq	40(%rdi), %rax
	addq	%r8, %rax
	cmpq	%rax, %rcx
	jnb	.L15
.L49:
	xorl	%eax, %eax
	cmpq	%r8, %rcx
	jb	.L15
.L45:
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	addq	%r8, %rax
	cmpq	%rax, %rcx
	setb	%al
	cmpq	%r8, %rcx
	setnb	%r8b
	andb	%r8b, %al
	je	.L45
	movq	32(%rdi), %r8
	movq	40(%rdi), %rax
	addq	%r8, %rax
	cmpq	%rax, %rcx
	jb	.L49
	.p2align 4,,10
	.p2align 3
.L15:
	movq	16(%rsi), %rcx
	movq	8(%rsi), %r9
	cmpq	%rcx, %r9
	setbe	%al
	cmpq	%rdx, %rcx
	setbe	%r8b
	andb	%r8b, %al
	je	.L45
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	8(%rcx), %r8
	testq	%r8, %r8
	je	.L13
	.p2align 4,,10
	.p2align 3
.L10:
	cmpq	%r8, %r10
	ja	.L16
	cmpq	%r8, %r11
	jbe	.L16
.L7:
	movq	(%rcx), %rcx
	cmpq	%rcx, %rdx
	setnb	%al
	cmpq	%rcx, %r9
	setbe	%r8b
	andb	%r8b, %al
	je	.L1
	movq	8(%rcx), %r8
	testq	%r8, %r8
	jne	.L10
.L6:
	leaq	16(%rcx), %rdi
	cmpq	%rdi, %rdx
	setnb	%al
	cmpq	%rdi, %r9
	setbe	%dl
	andb	%dl, %al
	jne	.L50
.L1:
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	16(%rdi), %rax
	movq	24(%rdi), %rbx
	addq	%rax, %rbx
	cmpq	%rbx, %r8
	jnb	.L6
	cmpq	%r8, %rax
	jbe	.L7
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%rdi, 8(%rsi)
	movq	(%rcx), %rdx
	movq	%r8, (%rsi)
	movq	%rdx, 16(%rsi)
	movq	$0, 24(%rsi)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L13:
	xorl	%r8d, %r8d
	jmp	.L6
	.cfi_endproc
.LFE3947:
	.size	_ZN2v88Unwinder17TryUnwindV8FramesERKNS_11UnwindStateEPNS_13RegisterStateEPKv, .-_ZN2v88Unwinder17TryUnwindV8FramesERKNS_11UnwindStateEPNS_13RegisterStateEPKv
	.section	.text._ZN2v88Unwinder8PCIsInV8ERKNS_11UnwindStateEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88Unwinder8PCIsInV8ERKNS_11UnwindStateEPv
	.type	_ZN2v88Unwinder8PCIsInV8ERKNS_11UnwindStateEPv, @function
_ZN2v88Unwinder8PCIsInV8ERKNS_11UnwindStateEPv:
.LFB3948:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L51
	movq	(%rdi), %rdx
	movq	8(%rdi), %rax
	addq	%rdx, %rax
	cmpq	%rax, %rsi
	setb	%al
	cmpq	%rdx, %rsi
	setnb	%dl
	andb	%dl, %al
	je	.L55
.L51:
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	addq	%rdx, %rax
	cmpq	%rax, %rsi
	setb	%al
	cmpq	%rdx, %rsi
	setnb	%dl
	andl	%edx, %eax
	ret
	.cfi_endproc
.LFE3948:
	.size	_ZN2v88Unwinder8PCIsInV8ERKNS_11UnwindStateEPv, .-_ZN2v88Unwinder8PCIsInV8ERKNS_11UnwindStateEPv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
