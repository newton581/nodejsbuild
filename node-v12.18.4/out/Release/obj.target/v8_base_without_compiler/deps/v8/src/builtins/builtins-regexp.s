	.file	"builtins-regexp.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.rodata._ZN2v88internalL36Builtin_Impl_RegExpPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"RegExp.prototype.toString"
.LC1:
	.string	"(location_) != nullptr"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL36Builtin_Impl_RegExpPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Builtin_Impl_RegExpPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL36Builtin_Impl_RegExpPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18386:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L6
.L9:
	leaq	.LC0(%rip), %rax
	xorl	%edx, %edx
	leaq	-144(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -144(%rbp)
	movq	$25, -136(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L64
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r14
.L21:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L50
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L50:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	addq	$168, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L9
	movq	12464(%rdx), %rax
	movq	39(%rax), %rax
	movq	1047(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L10
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L11:
	movq	-1(%rsi), %rax
	movzbl	13(%rax), %eax
	testb	$1, %al
	je	.L13
	movq	-1(%rsi), %rax
.L62:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L66
.L16:
	cmpq	%rax, (%r15)
	je	.L67
.L20:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal24IncrementalStringBuilderC1EPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rax
	movl	-184(%rbp), %r9d
	movq	(%rax), %rdx
	movl	-172(%rbp), %eax
	testl	%r9d, %r9d
	leal	1(%rax), %esi
	movl	%esi, -172(%rbp)
	jne	.L22
	addl	$16, %eax
	cltq
	movb	$47, -1(%rdx,%rax)
	movl	-176(%rbp), %eax
	cmpl	%eax, -172(%rbp)
	je	.L23
.L24:
	movq	3304(%r12), %rax
	leaq	3304(%r12), %rsi
	movl	$3, %edx
	movq	-1(%rax), %rdi
	cmpw	$64, 11(%rdi)
	jne	.L26
	movl	11(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%edx, %edx
	andl	$3, %edx
.L26:
	movabsq	$824633720832, %rax
	movl	%edx, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	3304(%r12), %rax
	movq	%r12, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L68
.L27:
	leaq	-144(%rbp), %rax
	movq	%rsi, -112(%rbp)
	movq	%rax, %rdi
	movq	$0, -104(%rbp)
	movq	%r15, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r15, -80(%rbp)
	movq	$-1, -72(%rbp)
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L28
	movq	-120(%rbp), %rax
	leaq	88(%rax), %rsi
.L29:
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L31
.L34:
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L63
.L33:
	movq	%r14, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	-160(%rbp), %rax
	movl	-184(%rbp), %edi
	movq	(%rax), %rdx
	movl	-172(%rbp), %eax
	testl	%edi, %edi
	leal	1(%rax), %esi
	movl	%esi, -172(%rbp)
	jne	.L35
	addl	$16, %eax
	cltq
	movb	$47, -1(%rdx,%rax)
	movl	-176(%rbp), %eax
	cmpl	%eax, -172(%rbp)
	je	.L36
.L37:
	movq	2528(%r12), %rax
	leaq	2528(%r12), %rsi
	movl	$3, %edx
	movq	-1(%rax), %rdi
	cmpw	$64, 11(%rdi)
	jne	.L39
	movl	11(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%edx, %edx
	andl	$3, %edx
.L39:
	movabsq	$824633720832, %rax
	movl	%edx, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	2528(%r12), %rax
	movq	%r12, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L69
.L40:
	movq	-200(%rbp), %rdi
	movq	%rsi, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r15, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r15, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L41
	movq	-120(%rbp), %rax
	leaq	88(%rax), %rsi
.L42:
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L44
.L47:
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L63
.L46:
	movq	%r14, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	testq	%rax, %rax
	je	.L63
	movq	(%rax), %r14
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L41:
	movq	-200(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L42
	.p2align 4,,10
	.p2align 3
.L63:
	movq	312(%r12), %r14
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L64:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L13:
	movq	55(%rsi), %rax
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L16
	movq	23(%rax), %rax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L22:
	leal	16(%rax,%rax), %eax
	movl	$47, %r8d
	cltq
	movw	%r8w, -1(%rdx,%rax)
	movl	-176(%rbp), %eax
	cmpl	%eax, -172(%rbp)
	jne	.L24
.L23:
	movq	%r14, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L28:
	movq	-200(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L29
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%r13, %rax
	cmpq	41096(%rdx), %r13
	je	.L70
.L12:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L35:
	leal	16(%rax,%rax), %eax
	movl	$47, %ecx
	cltq
	movw	%cx, -1(%rdx,%rax)
	movl	-176(%rbp), %eax
	cmpl	%eax, -172(%rbp)
	jne	.L37
.L36:
	movq	%r14, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L31:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L34
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L68:
	movq	%r12, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L67:
	movl	$12, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L66:
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L16
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L44:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L47
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L69:
	movq	%r12, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L70:
	movq	%rdx, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	jmp	.L12
.L65:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18386:
	.size	_ZN2v88internalL36Builtin_Impl_RegExpPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL36Builtin_Impl_RegExpPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internalL36Builtin_Impl_RegExpLeftContextGetterENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Builtin_Impl_RegExpLeftContextGetterENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL36Builtin_Impl_RegExpLeftContextGetterENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18428:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	12464(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r14
	movq	39(%rax), %rax
	movq	1055(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L72
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L73:
	movq	39(%rsi), %r15
	movq	(%rax), %rax
	movq	23(%rax), %r13
	movq	41112(%r12), %rdi
	sarq	$32, %r15
	testq	%rdi, %rdi
	je	.L75
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
	movq	%rax, %rsi
.L76:
	cmpl	11(%r13), %r15d
	je	.L79
	movl	%r15d, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	(%rax), %r13
.L79:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L82
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L82:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L84
.L77:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L72:
	movq	%rbx, %rax
	cmpq	41096(%rdx), %rbx
	je	.L85
.L74:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L84:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%rdx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L74
	.cfi_endproc
.LFE18428:
	.size	_ZN2v88internalL36Builtin_Impl_RegExpLeftContextGetterENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL36Builtin_Impl_RegExpLeftContextGetterENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internalL30Builtin_Impl_RegExpInputSetterENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Builtin_Impl_RegExpInputSetterENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL30Builtin_Impl_RegExpInputSetterENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18419:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subq	$8, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	88(%rdx), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	addl	$1, 41104(%rdx)
	cmpl	$5, %edi
	movq	41088(%rdx), %r14
	cmovg	%rsi, %r13
	movq	41096(%rdx), %rbx
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L89
.L92:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L111
.L91:
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	1055(%rax), %r15
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L94
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r15
.L95:
	movq	0(%r13), %r13
	leaq	31(%r15), %rsi
	movq	%r13, 31(%r15)
	testb	$1, %r13b
	je	.L102
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L98
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
.L98:
	testb	$24, %al
	je	.L102
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L112
	.p2align 4,,10
	.p2align 3
.L102:
	movq	88(%r12), %r13
.L93:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L109
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L109:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L113
.L96:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%r15, (%rax)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L89:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L92
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L112:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L111:
	movq	312(%r12), %r13
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L96
	.cfi_endproc
.LFE18419:
	.size	_ZN2v88internalL30Builtin_Impl_RegExpInputSetterENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL30Builtin_Impl_RegExpInputSetterENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internalL37Builtin_Impl_RegExpRightContextGetterENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Builtin_Impl_RegExpRightContextGetterENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL37Builtin_Impl_RegExpRightContextGetterENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18431:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	12464(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r14
	movq	39(%rax), %rax
	movq	1055(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L115
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L116:
	movq	47(%rsi), %r15
	movq	(%rax), %rax
	movq	23(%rax), %r13
	movq	41112(%r12), %rdi
	sarq	$32, %r15
	testq	%rdi, %rdi
	je	.L118
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
	movq	%rax, %rsi
.L119:
	movl	11(%r13), %ecx
	testq	%r15, %r15
	je	.L122
	movl	%r15d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	(%rax), %r13
.L122:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L125
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L125:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L127
.L120:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L115:
	movq	%rbx, %rax
	cmpq	41096(%rdx), %rbx
	je	.L128
.L117:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L127:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L128:
	movq	%rdx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L117
	.cfi_endproc
.LFE18431:
	.size	_ZN2v88internalL37Builtin_Impl_RegExpRightContextGetterENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL37Builtin_Impl_RegExpRightContextGetterENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L135
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L138
.L129:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L129
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture1GetterEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC4:
	.string	"V8.Builtin_RegExpCapture1Getter"
	.section	.text._ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture1GetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture1GetterEiPmPNS0_7IsolateE, @function
_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture1GetterEiPmPNS0_7IsolateE:
.LFB18387:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L173
.L140:
	movq	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture1GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic67(%rip), %rbx
	testq	%rbx, %rbx
	je	.L174
.L142:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L175
.L144:
	movq	12464(%r12), %rax
	movq	41088(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	41096(%r12), %r13
	movq	39(%rax), %rax
	movq	1055(%rax), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L148
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L149:
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L153
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L153:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L176
.L139:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L177
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L178
.L150:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L175:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L179
.L145:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L146
	movq	(%rdi), %rax
	call	*8(%rax)
.L146:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L147
	movq	(%rdi), %rax
	call	*8(%rax)
.L147:
	leaq	.LC4(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L174:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L180
.L143:
	movq	%rbx, _ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture1GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic67(%rip)
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L173:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$813, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L176:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L178:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L179:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC4(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L180:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L143
.L177:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18387:
	.size	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture1GetterEiPmPNS0_7IsolateE, .-_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture1GetterEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture2GetterEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"V8.Builtin_RegExpCapture2Getter"
	.section	.text._ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture2GetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture2GetterEiPmPNS0_7IsolateE, @function
_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture2GetterEiPmPNS0_7IsolateE:
.LFB18390:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L215
.L182:
	movq	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture2GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic68(%rip), %rbx
	testq	%rbx, %rbx
	je	.L216
.L184:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L217
.L186:
	movq	12464(%r12), %rax
	movq	41088(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	41096(%r12), %r13
	movq	39(%rax), %rax
	movq	1055(%rax), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L190
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L191:
	xorl	%ecx, %ecx
	movl	$2, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L195
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L195:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L218
.L181:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L219
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L220
.L192:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L217:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L221
.L187:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L188
	movq	(%rdi), %rax
	call	*8(%rax)
.L188:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L189
	movq	(%rdi), %rax
	call	*8(%rax)
.L189:
	leaq	.LC5(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L216:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L222
.L185:
	movq	%rbx, _ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture2GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic68(%rip)
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L215:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$814, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L218:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L220:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L221:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC5(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L222:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L185
.L219:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18390:
	.size	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture2GetterEiPmPNS0_7IsolateE, .-_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture2GetterEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture3GetterEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"V8.Builtin_RegExpCapture3Getter"
	.section	.text._ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture3GetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture3GetterEiPmPNS0_7IsolateE, @function
_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture3GetterEiPmPNS0_7IsolateE:
.LFB18393:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L257
.L224:
	movq	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture3GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic69(%rip), %rbx
	testq	%rbx, %rbx
	je	.L258
.L226:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L259
.L228:
	movq	12464(%r12), %rax
	movq	41088(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	41096(%r12), %r13
	movq	39(%rax), %rax
	movq	1055(%rax), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L232
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L233:
	xorl	%ecx, %ecx
	movl	$3, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L237
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L237:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L260
.L223:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L261
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L262
.L234:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L259:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L263
.L229:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L230
	movq	(%rdi), %rax
	call	*8(%rax)
.L230:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L231
	movq	(%rdi), %rax
	call	*8(%rax)
.L231:
	leaq	.LC6(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L258:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L264
.L227:
	movq	%rbx, _ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture3GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic69(%rip)
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L257:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$815, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L260:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L262:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L263:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC6(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L264:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L227
.L261:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18393:
	.size	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture3GetterEiPmPNS0_7IsolateE, .-_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture3GetterEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture4GetterEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"V8.Builtin_RegExpCapture4Getter"
	.section	.text._ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture4GetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture4GetterEiPmPNS0_7IsolateE, @function
_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture4GetterEiPmPNS0_7IsolateE:
.LFB18396:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L299
.L266:
	movq	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture4GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic70(%rip), %rbx
	testq	%rbx, %rbx
	je	.L300
.L268:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L301
.L270:
	movq	12464(%r12), %rax
	movq	41088(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	41096(%r12), %r13
	movq	39(%rax), %rax
	movq	1055(%rax), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L274
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L275:
	xorl	%ecx, %ecx
	movl	$4, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L279
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L279:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L302
.L265:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L303
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L304
.L276:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L301:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L305
.L271:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L272
	movq	(%rdi), %rax
	call	*8(%rax)
.L272:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L273
	movq	(%rdi), %rax
	call	*8(%rax)
.L273:
	leaq	.LC7(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L300:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L306
.L269:
	movq	%rbx, _ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture4GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic70(%rip)
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L299:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$816, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L302:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L304:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L305:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC7(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L306:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L269
.L303:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18396:
	.size	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture4GetterEiPmPNS0_7IsolateE, .-_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture4GetterEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture5GetterEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"V8.Builtin_RegExpCapture5Getter"
	.section	.text._ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture5GetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture5GetterEiPmPNS0_7IsolateE, @function
_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture5GetterEiPmPNS0_7IsolateE:
.LFB18399:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L341
.L308:
	movq	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture5GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic71(%rip), %rbx
	testq	%rbx, %rbx
	je	.L342
.L310:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L343
.L312:
	movq	12464(%r12), %rax
	movq	41088(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	41096(%r12), %r13
	movq	39(%rax), %rax
	movq	1055(%rax), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L316
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L317:
	xorl	%ecx, %ecx
	movl	$5, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L321
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L321:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L344
.L307:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L345
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L346
.L318:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L343:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L347
.L313:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L314
	movq	(%rdi), %rax
	call	*8(%rax)
.L314:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L315
	movq	(%rdi), %rax
	call	*8(%rax)
.L315:
	leaq	.LC8(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L342:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L348
.L311:
	movq	%rbx, _ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture5GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic71(%rip)
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L341:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$817, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L344:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L346:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L347:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC8(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L348:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L311
.L345:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18399:
	.size	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture5GetterEiPmPNS0_7IsolateE, .-_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture5GetterEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture6GetterEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"V8.Builtin_RegExpCapture6Getter"
	.section	.text._ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture6GetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture6GetterEiPmPNS0_7IsolateE, @function
_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture6GetterEiPmPNS0_7IsolateE:
.LFB18402:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L383
.L350:
	movq	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture6GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic72(%rip), %rbx
	testq	%rbx, %rbx
	je	.L384
.L352:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L385
.L354:
	movq	12464(%r12), %rax
	movq	41088(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	41096(%r12), %r13
	movq	39(%rax), %rax
	movq	1055(%rax), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L358
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L359:
	xorl	%ecx, %ecx
	movl	$6, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L363
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L363:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L386
.L349:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L387
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L388
.L360:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L385:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L389
.L355:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L356
	movq	(%rdi), %rax
	call	*8(%rax)
.L356:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L357
	movq	(%rdi), %rax
	call	*8(%rax)
.L357:
	leaq	.LC9(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L384:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L390
.L353:
	movq	%rbx, _ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture6GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic72(%rip)
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L383:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$818, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L386:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L388:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L389:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC9(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L390:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L353
.L387:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18402:
	.size	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture6GetterEiPmPNS0_7IsolateE, .-_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture6GetterEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture7GetterEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"V8.Builtin_RegExpCapture7Getter"
	.section	.text._ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture7GetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture7GetterEiPmPNS0_7IsolateE, @function
_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture7GetterEiPmPNS0_7IsolateE:
.LFB18405:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L425
.L392:
	movq	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture7GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic73(%rip), %rbx
	testq	%rbx, %rbx
	je	.L426
.L394:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L427
.L396:
	movq	12464(%r12), %rax
	movq	41088(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	41096(%r12), %r13
	movq	39(%rax), %rax
	movq	1055(%rax), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L400
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L401:
	xorl	%ecx, %ecx
	movl	$7, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L405
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L405:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L428
.L391:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L429
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L430
.L402:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L427:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L431
.L397:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L398
	movq	(%rdi), %rax
	call	*8(%rax)
.L398:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L399
	movq	(%rdi), %rax
	call	*8(%rax)
.L399:
	leaq	.LC10(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L426:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L432
.L395:
	movq	%rbx, _ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture7GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic73(%rip)
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L425:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$819, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L428:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L430:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L431:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC10(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L432:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L395
.L429:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18405:
	.size	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture7GetterEiPmPNS0_7IsolateE, .-_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture7GetterEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture8GetterEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"V8.Builtin_RegExpCapture8Getter"
	.section	.text._ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture8GetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture8GetterEiPmPNS0_7IsolateE, @function
_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture8GetterEiPmPNS0_7IsolateE:
.LFB18408:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L467
.L434:
	movq	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture8GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic74(%rip), %rbx
	testq	%rbx, %rbx
	je	.L468
.L436:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L469
.L438:
	movq	12464(%r12), %rax
	movq	41088(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	41096(%r12), %r13
	movq	39(%rax), %rax
	movq	1055(%rax), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L442
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L443:
	xorl	%ecx, %ecx
	movl	$8, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L447
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L447:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L470
.L433:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L471
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L442:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L472
.L444:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L469:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L473
.L439:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L440
	movq	(%rdi), %rax
	call	*8(%rax)
.L440:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L441
	movq	(%rdi), %rax
	call	*8(%rax)
.L441:
	leaq	.LC11(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L468:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L474
.L437:
	movq	%rbx, _ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture8GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic74(%rip)
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L467:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$820, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L470:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L472:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L473:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC11(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L474:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L437
.L471:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18408:
	.size	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture8GetterEiPmPNS0_7IsolateE, .-_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture8GetterEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture9GetterEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"V8.Builtin_RegExpCapture9Getter"
	.section	.text._ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture9GetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture9GetterEiPmPNS0_7IsolateE, @function
_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture9GetterEiPmPNS0_7IsolateE:
.LFB18411:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L509
.L476:
	movq	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture9GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic75(%rip), %rbx
	testq	%rbx, %rbx
	je	.L510
.L478:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L511
.L480:
	movq	12464(%r12), %rax
	movq	41088(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	41096(%r12), %r13
	movq	39(%rax), %rax
	movq	1055(%rax), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L484
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L485:
	xorl	%ecx, %ecx
	movl	$9, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L489
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L489:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L512
.L475:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L513
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L484:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L514
.L486:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L511:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L515
.L481:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L482
	movq	(%rdi), %rax
	call	*8(%rax)
.L482:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L483
	movq	(%rdi), %rax
	call	*8(%rax)
.L483:
	leaq	.LC12(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L510:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L516
.L479:
	movq	%rbx, _ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture9GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic75(%rip)
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L509:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$821, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L512:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L514:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L515:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC12(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L516:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L479
.L513:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18411:
	.size	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture9GetterEiPmPNS0_7IsolateE, .-_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture9GetterEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL40Builtin_Impl_Stats_RegExpLastMatchGetterEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"V8.Builtin_RegExpLastMatchGetter"
	.section	.text._ZN2v88internalL40Builtin_Impl_Stats_RegExpLastMatchGetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Builtin_Impl_Stats_RegExpLastMatchGetterEiPmPNS0_7IsolateE, @function
_ZN2v88internalL40Builtin_Impl_Stats_RegExpLastMatchGetterEiPmPNS0_7IsolateE:
.LFB18420:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L551
.L518:
	movq	_ZZN2v88internalL40Builtin_Impl_Stats_RegExpLastMatchGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic103(%rip), %rbx
	testq	%rbx, %rbx
	je	.L552
.L520:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L553
.L522:
	movq	12464(%r12), %rax
	movq	41088(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	41096(%r12), %r13
	movq	39(%rax), %rax
	movq	1055(%rax), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L526
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L527:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L531
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L531:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L554
.L517:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L555
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L526:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L556
.L528:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L553:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L557
.L523:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L524
	movq	(%rdi), %rax
	call	*8(%rax)
.L524:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L525
	movq	(%rdi), %rax
	call	*8(%rax)
.L525:
	leaq	.LC13(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L552:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L558
.L521:
	movq	%rbx, _ZZN2v88internalL40Builtin_Impl_Stats_RegExpLastMatchGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic103(%rip)
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L551:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$824, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L554:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L556:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L557:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC13(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L558:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L521
.L555:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18420:
	.size	_ZN2v88internalL40Builtin_Impl_Stats_RegExpLastMatchGetterEiPmPNS0_7IsolateE, .-_ZN2v88internalL40Builtin_Impl_Stats_RegExpLastMatchGetterEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL36Builtin_Impl_Stats_RegExpInputGetterEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"V8.Builtin_RegExpInputGetter"
	.section	.text._ZN2v88internalL36Builtin_Impl_Stats_RegExpInputGetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Builtin_Impl_Stats_RegExpInputGetterEiPmPNS0_7IsolateE, @function
_ZN2v88internalL36Builtin_Impl_Stats_RegExpInputGetterEiPmPNS0_7IsolateE:
.LFB18414:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L597
.L560:
	movq	_ZZN2v88internalL36Builtin_Impl_Stats_RegExpInputGetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic82(%rip), %rbx
	testq	%rbx, %rbx
	je	.L598
.L562:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L599
.L564:
	movq	12464(%r12), %rax
	movq	41088(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	41096(%r12), %r14
	movq	39(%rax), %rax
	movq	1055(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L568
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L569:
	movq	31(%rsi), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L571
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
.L572:
	cmpq	%r13, 88(%r12)
	jne	.L574
	movq	128(%r12), %r13
.L574:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L577
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L577:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L600
.L559:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L601
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L571:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L602
.L573:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%r13, (%rax)
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L568:
	movq	%rbx, %rax
	cmpq	41096(%r12), %rbx
	je	.L603
.L570:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L599:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L604
.L565:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L566
	movq	(%rdi), %rax
	call	*8(%rax)
.L566:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L567
	movq	(%rdi), %rax
	call	*8(%rax)
.L567:
	leaq	.LC14(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L564
	.p2align 4,,10
	.p2align 3
.L598:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L605
.L563:
	movq	%rbx, _ZZN2v88internalL36Builtin_Impl_Stats_RegExpInputGetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic82(%rip)
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L597:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$822, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L600:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L603:
	movq	%r12, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L602:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L605:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L604:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC14(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L565
.L601:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18414:
	.size	_ZN2v88internalL36Builtin_Impl_Stats_RegExpInputGetterEiPmPNS0_7IsolateE, .-_ZN2v88internalL36Builtin_Impl_Stats_RegExpInputGetterEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL40Builtin_Impl_Stats_RegExpLastParenGetterEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"V8.Builtin_RegExpLastParenGetter"
	.section	.text._ZN2v88internalL40Builtin_Impl_Stats_RegExpLastParenGetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Builtin_Impl_Stats_RegExpLastParenGetterEiPmPNS0_7IsolateE, @function
_ZN2v88internalL40Builtin_Impl_Stats_RegExpLastParenGetterEiPmPNS0_7IsolateE:
.LFB18423:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L642
.L607:
	movq	_ZZN2v88internalL40Builtin_Impl_Stats_RegExpLastParenGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic109(%rip), %rbx
	testq	%rbx, %rbx
	je	.L643
.L609:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L644
.L611:
	movq	12464(%r12), %rax
	movq	41088(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	41096(%r12), %r13
	movq	39(%rax), %rax
	movq	1055(%rax), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L615
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r14
	movq	%rax, %rsi
	movq	15(%r14), %rdx
	sarq	$32, %rdx
	cmpq	$2, %rdx
	jg	.L618
.L648:
	movq	128(%r12), %r14
.L619:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L622
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L622:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L645
.L606:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L646
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L615:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L647
.L617:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	movq	15(%r14), %rdx
	sarq	$32, %rdx
	cmpq	$2, %rdx
	jle	.L648
.L618:
	sarl	%edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	subl	$1, %edx
	call	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	movq	(%rax), %r14
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L644:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L649
.L612:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L613
	movq	(%rdi), %rax
	call	*8(%rax)
.L613:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L614
	movq	(%rdi), %rax
	call	*8(%rax)
.L614:
	leaq	.LC15(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L643:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L650
.L610:
	movq	%rbx, _ZZN2v88internalL40Builtin_Impl_Stats_RegExpLastParenGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic109(%rip)
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L642:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$825, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L645:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L647:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L649:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC15(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L650:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L610
.L646:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18423:
	.size	_ZN2v88internalL40Builtin_Impl_Stats_RegExpLastParenGetterEiPmPNS0_7IsolateE, .-_ZN2v88internalL40Builtin_Impl_Stats_RegExpLastParenGetterEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Builtin_Impl_Stats_RegExpLeftContextGetterEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"V8.Builtin_RegExpLeftContextGetter"
	.section	.text._ZN2v88internalL42Builtin_Impl_Stats_RegExpLeftContextGetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_Stats_RegExpLeftContextGetterEiPmPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_Stats_RegExpLeftContextGetterEiPmPNS0_7IsolateE:
.LFB18426:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L680
.L652:
	movq	_ZZN2v88internalL42Builtin_Impl_Stats_RegExpLeftContextGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic126(%rip), %rbx
	testq	%rbx, %rbx
	je	.L681
.L654:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L682
.L656:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL36Builtin_Impl_RegExpLeftContextGetterENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L683
.L660:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L684
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L681:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L685
.L655:
	movq	%rbx, _ZZN2v88internalL42Builtin_Impl_Stats_RegExpLeftContextGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic126(%rip)
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L682:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L686
.L657:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L658
	movq	(%rdi), %rax
	call	*8(%rax)
.L658:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L659
	movq	(%rdi), %rax
	call	*8(%rax)
.L659:
	leaq	.LC16(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L683:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L680:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$826, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L686:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC16(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L685:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L655
.L684:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18426:
	.size	_ZN2v88internalL42Builtin_Impl_Stats_RegExpLeftContextGetterEiPmPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_Stats_RegExpLeftContextGetterEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL43Builtin_Impl_Stats_RegExpRightContextGetterEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"V8.Builtin_RegExpRightContextGetter"
	.section	.text._ZN2v88internalL43Builtin_Impl_Stats_RegExpRightContextGetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Builtin_Impl_Stats_RegExpRightContextGetterEiPmPNS0_7IsolateE, @function
_ZN2v88internalL43Builtin_Impl_Stats_RegExpRightContextGetterEiPmPNS0_7IsolateE:
.LFB18429:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L716
.L688:
	movq	_ZZN2v88internalL43Builtin_Impl_Stats_RegExpRightContextGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic134(%rip), %rbx
	testq	%rbx, %rbx
	je	.L717
.L690:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L718
.L692:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL37Builtin_Impl_RegExpRightContextGetterENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L719
.L696:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L720
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L717:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L721
.L691:
	movq	%rbx, _ZZN2v88internalL43Builtin_Impl_Stats_RegExpRightContextGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic134(%rip)
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L718:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L722
.L693:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L694
	movq	(%rdi), %rax
	call	*8(%rax)
.L694:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L695
	movq	(%rdi), %rax
	call	*8(%rax)
.L695:
	leaq	.LC17(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L692
	.p2align 4,,10
	.p2align 3
.L719:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L716:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$828, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L722:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC17(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L721:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L691
.L720:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18429:
	.size	_ZN2v88internalL43Builtin_Impl_Stats_RegExpRightContextGetterEiPmPNS0_7IsolateE, .-_ZN2v88internalL43Builtin_Impl_Stats_RegExpRightContextGetterEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL36Builtin_Impl_Stats_RegExpInputSetterEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC18:
	.string	"V8.Builtin_RegExpInputSetter"
	.section	.text._ZN2v88internalL36Builtin_Impl_Stats_RegExpInputSetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Builtin_Impl_Stats_RegExpInputSetterEiPmPNS0_7IsolateE, @function
_ZN2v88internalL36Builtin_Impl_Stats_RegExpInputSetterEiPmPNS0_7IsolateE:
.LFB18417:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L752
.L724:
	movq	_ZZN2v88internalL36Builtin_Impl_Stats_RegExpInputSetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic89(%rip), %rbx
	testq	%rbx, %rbx
	je	.L753
.L726:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L754
.L728:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL30Builtin_Impl_RegExpInputSetterENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L755
.L732:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L756
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L753:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L757
.L727:
	movq	%rbx, _ZZN2v88internalL36Builtin_Impl_Stats_RegExpInputSetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic89(%rip)
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L754:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L758
.L729:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L730
	movq	(%rdi), %rax
	call	*8(%rax)
.L730:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L731
	movq	(%rdi), %rax
	call	*8(%rax)
.L731:
	leaq	.LC18(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L755:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L752:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$823, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L758:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC18(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L757:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L727
.L756:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18417:
	.size	_ZN2v88internalL36Builtin_Impl_Stats_RegExpInputSetterEiPmPNS0_7IsolateE, .-_ZN2v88internalL36Builtin_Impl_Stats_RegExpInputSetterEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Builtin_Impl_Stats_RegExpPrototypeToStringEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"V8.Builtin_RegExpPrototypeToString"
	.section	.text._ZN2v88internalL42Builtin_Impl_Stats_RegExpPrototypeToStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_Stats_RegExpPrototypeToStringEiPmPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_Stats_RegExpPrototypeToStringEiPmPNS0_7IsolateE:
.LFB18384:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L788
.L760:
	movq	_ZZN2v88internalL42Builtin_Impl_Stats_RegExpPrototypeToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic19(%rip), %rbx
	testq	%rbx, %rbx
	je	.L789
.L762:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L790
.L764:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL36Builtin_Impl_RegExpPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L791
.L768:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L792
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L789:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L793
.L763:
	movq	%rbx, _ZZN2v88internalL42Builtin_Impl_Stats_RegExpPrototypeToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic19(%rip)
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L790:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L794
.L765:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L766
	movq	(%rdi), %rax
	call	*8(%rax)
.L766:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L767
	movq	(%rdi), %rax
	call	*8(%rax)
.L767:
	leaq	.LC19(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L791:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L768
	.p2align 4,,10
	.p2align 3
.L788:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$827, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L760
	.p2align 4,,10
	.p2align 3
.L794:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC19(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L793:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L763
.L792:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18384:
	.size	_ZN2v88internalL42Builtin_Impl_Stats_RegExpPrototypeToStringEiPmPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_Stats_RegExpPrototypeToStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Builtin_RegExpPrototypeToStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Builtin_RegExpPrototypeToStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Builtin_RegExpPrototypeToStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Builtin_RegExpPrototypeToStringEiPmPNS0_7IsolateE:
.LFB18385:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L799
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL36Builtin_Impl_RegExpPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L799:
	.cfi_restore 6
	jmp	_ZN2v88internalL42Builtin_Impl_Stats_RegExpPrototypeToStringEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18385:
	.size	_ZN2v88internal31Builtin_RegExpPrototypeToStringEiPmPNS0_7IsolateE, .-_ZN2v88internal31Builtin_RegExpPrototypeToStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Builtin_RegExpCapture1GetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Builtin_RegExpCapture1GetterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Builtin_RegExpCapture1GetterEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Builtin_RegExpCapture1GetterEiPmPNS0_7IsolateE:
.LFB18388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L808
	movq	12464(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	movq	39(%rax), %rax
	movq	1055(%rax), %r14
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L802
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L803:
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L800
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L800:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L802:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%rdx), %rbx
	je	.L809
.L804:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L808:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture1GetterEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L809:
	.cfi_restore_state
	movq	%rdx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L804
	.cfi_endproc
.LFE18388:
	.size	_ZN2v88internal28Builtin_RegExpCapture1GetterEiPmPNS0_7IsolateE, .-_ZN2v88internal28Builtin_RegExpCapture1GetterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Builtin_RegExpCapture2GetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Builtin_RegExpCapture2GetterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Builtin_RegExpCapture2GetterEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Builtin_RegExpCapture2GetterEiPmPNS0_7IsolateE:
.LFB18391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L818
	movq	12464(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	movq	39(%rax), %rax
	movq	1055(%rax), %r14
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L812
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L813:
	xorl	%ecx, %ecx
	movl	$2, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L810
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L810:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L812:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%rdx), %rbx
	je	.L819
.L814:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L818:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture2GetterEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L819:
	.cfi_restore_state
	movq	%rdx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L814
	.cfi_endproc
.LFE18391:
	.size	_ZN2v88internal28Builtin_RegExpCapture2GetterEiPmPNS0_7IsolateE, .-_ZN2v88internal28Builtin_RegExpCapture2GetterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Builtin_RegExpCapture3GetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Builtin_RegExpCapture3GetterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Builtin_RegExpCapture3GetterEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Builtin_RegExpCapture3GetterEiPmPNS0_7IsolateE:
.LFB18394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L828
	movq	12464(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	movq	39(%rax), %rax
	movq	1055(%rax), %r14
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L822
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L823:
	xorl	%ecx, %ecx
	movl	$3, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L820
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L820:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L822:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%rdx), %rbx
	je	.L829
.L824:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L823
	.p2align 4,,10
	.p2align 3
.L828:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture3GetterEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L829:
	.cfi_restore_state
	movq	%rdx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L824
	.cfi_endproc
.LFE18394:
	.size	_ZN2v88internal28Builtin_RegExpCapture3GetterEiPmPNS0_7IsolateE, .-_ZN2v88internal28Builtin_RegExpCapture3GetterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Builtin_RegExpCapture4GetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Builtin_RegExpCapture4GetterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Builtin_RegExpCapture4GetterEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Builtin_RegExpCapture4GetterEiPmPNS0_7IsolateE:
.LFB18397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L838
	movq	12464(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	movq	39(%rax), %rax
	movq	1055(%rax), %r14
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L832
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L833:
	xorl	%ecx, %ecx
	movl	$4, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L830
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L830:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L832:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%rdx), %rbx
	je	.L839
.L834:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L838:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture4GetterEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L839:
	.cfi_restore_state
	movq	%rdx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L834
	.cfi_endproc
.LFE18397:
	.size	_ZN2v88internal28Builtin_RegExpCapture4GetterEiPmPNS0_7IsolateE, .-_ZN2v88internal28Builtin_RegExpCapture4GetterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Builtin_RegExpCapture5GetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Builtin_RegExpCapture5GetterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Builtin_RegExpCapture5GetterEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Builtin_RegExpCapture5GetterEiPmPNS0_7IsolateE:
.LFB18400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L848
	movq	12464(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	movq	39(%rax), %rax
	movq	1055(%rax), %r14
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L842
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L843:
	xorl	%ecx, %ecx
	movl	$5, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L840
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L840:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L842:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%rdx), %rbx
	je	.L849
.L844:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L848:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture5GetterEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L849:
	.cfi_restore_state
	movq	%rdx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L844
	.cfi_endproc
.LFE18400:
	.size	_ZN2v88internal28Builtin_RegExpCapture5GetterEiPmPNS0_7IsolateE, .-_ZN2v88internal28Builtin_RegExpCapture5GetterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Builtin_RegExpCapture6GetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Builtin_RegExpCapture6GetterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Builtin_RegExpCapture6GetterEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Builtin_RegExpCapture6GetterEiPmPNS0_7IsolateE:
.LFB18403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L858
	movq	12464(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	movq	39(%rax), %rax
	movq	1055(%rax), %r14
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L852
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L853:
	xorl	%ecx, %ecx
	movl	$6, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L850
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L850:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L852:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%rdx), %rbx
	je	.L859
.L854:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L858:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture6GetterEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L859:
	.cfi_restore_state
	movq	%rdx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L854
	.cfi_endproc
.LFE18403:
	.size	_ZN2v88internal28Builtin_RegExpCapture6GetterEiPmPNS0_7IsolateE, .-_ZN2v88internal28Builtin_RegExpCapture6GetterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Builtin_RegExpCapture7GetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Builtin_RegExpCapture7GetterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Builtin_RegExpCapture7GetterEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Builtin_RegExpCapture7GetterEiPmPNS0_7IsolateE:
.LFB18406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L868
	movq	12464(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	movq	39(%rax), %rax
	movq	1055(%rax), %r14
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L862
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L863:
	xorl	%ecx, %ecx
	movl	$7, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L860
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L860:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L862:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%rdx), %rbx
	je	.L869
.L864:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L868:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture7GetterEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L869:
	.cfi_restore_state
	movq	%rdx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L864
	.cfi_endproc
.LFE18406:
	.size	_ZN2v88internal28Builtin_RegExpCapture7GetterEiPmPNS0_7IsolateE, .-_ZN2v88internal28Builtin_RegExpCapture7GetterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Builtin_RegExpCapture8GetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Builtin_RegExpCapture8GetterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Builtin_RegExpCapture8GetterEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Builtin_RegExpCapture8GetterEiPmPNS0_7IsolateE:
.LFB18409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L878
	movq	12464(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	movq	39(%rax), %rax
	movq	1055(%rax), %r14
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L872
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L873:
	xorl	%ecx, %ecx
	movl	$8, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L870
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L870:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L872:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%rdx), %rbx
	je	.L879
.L874:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L878:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture8GetterEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L879:
	.cfi_restore_state
	movq	%rdx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L874
	.cfi_endproc
.LFE18409:
	.size	_ZN2v88internal28Builtin_RegExpCapture8GetterEiPmPNS0_7IsolateE, .-_ZN2v88internal28Builtin_RegExpCapture8GetterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Builtin_RegExpCapture9GetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Builtin_RegExpCapture9GetterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Builtin_RegExpCapture9GetterEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Builtin_RegExpCapture9GetterEiPmPNS0_7IsolateE:
.LFB18412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L888
	movq	12464(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	movq	39(%rax), %rax
	movq	1055(%rax), %r14
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L882
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L883:
	xorl	%ecx, %ecx
	movl	$9, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L880
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L880:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L882:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%rdx), %rbx
	je	.L889
.L884:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L888:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL39Builtin_Impl_Stats_RegExpCapture9GetterEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L889:
	.cfi_restore_state
	movq	%rdx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L884
	.cfi_endproc
.LFE18412:
	.size	_ZN2v88internal28Builtin_RegExpCapture9GetterEiPmPNS0_7IsolateE, .-_ZN2v88internal28Builtin_RegExpCapture9GetterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Builtin_RegExpInputGetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Builtin_RegExpInputGetterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Builtin_RegExpInputGetterEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Builtin_RegExpInputGetterEiPmPNS0_7IsolateE:
.LFB18415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L902
	movq	12464(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r14
	movq	39(%rax), %rax
	movq	1055(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L892
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L893:
	movq	31(%rsi), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L895
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
.L896:
	cmpq	%r13, 88(%r12)
	jne	.L898
	movq	128(%r12), %r13
.L898:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L890
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L890:
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L892:
	.cfi_restore_state
	movq	%rbx, %rax
	cmpq	41096(%rdx), %rbx
	je	.L903
.L894:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L895:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L904
.L897:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%r13, (%rax)
	jmp	.L896
	.p2align 4,,10
	.p2align 3
.L902:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL36Builtin_Impl_Stats_RegExpInputGetterEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L904:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L897
	.p2align 4,,10
	.p2align 3
.L903:
	movq	%rdx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L894
	.cfi_endproc
.LFE18415:
	.size	_ZN2v88internal25Builtin_RegExpInputGetterEiPmPNS0_7IsolateE, .-_ZN2v88internal25Builtin_RegExpInputGetterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Builtin_RegExpInputSetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Builtin_RegExpInputSetterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Builtin_RegExpInputSetterEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Builtin_RegExpInputSetterEiPmPNS0_7IsolateE:
.LFB18418:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L909
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL30Builtin_Impl_RegExpInputSetterENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L909:
	.cfi_restore 6
	jmp	_ZN2v88internalL36Builtin_Impl_Stats_RegExpInputSetterEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18418:
	.size	_ZN2v88internal25Builtin_RegExpInputSetterEiPmPNS0_7IsolateE, .-_ZN2v88internal25Builtin_RegExpInputSetterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Builtin_RegExpLastMatchGetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Builtin_RegExpLastMatchGetterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Builtin_RegExpLastMatchGetterEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Builtin_RegExpLastMatchGetterEiPmPNS0_7IsolateE:
.LFB18421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L918
	movq	12464(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	movq	39(%rax), %rax
	movq	1055(%rax), %r14
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L912
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L913:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L910
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L910:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L912:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%rdx), %rbx
	je	.L919
.L914:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L918:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL40Builtin_Impl_Stats_RegExpLastMatchGetterEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L919:
	.cfi_restore_state
	movq	%rdx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L914
	.cfi_endproc
.LFE18421:
	.size	_ZN2v88internal29Builtin_RegExpLastMatchGetterEiPmPNS0_7IsolateE, .-_ZN2v88internal29Builtin_RegExpLastMatchGetterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Builtin_RegExpLastParenGetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Builtin_RegExpLastParenGetterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Builtin_RegExpLastParenGetterEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Builtin_RegExpLastParenGetterEiPmPNS0_7IsolateE:
.LFB18424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L930
	movq	12464(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	movq	39(%rax), %rax
	movq	1055(%rax), %r14
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L922
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r14
	movq	%rax, %rsi
	movq	15(%r14), %rdx
	sarq	$32, %rdx
	cmpq	$2, %rdx
	jg	.L925
.L932:
	movq	128(%r12), %r14
.L926:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L920
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L920:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L922:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%rdx), %rbx
	je	.L931
.L924:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	movq	15(%r14), %rdx
	sarq	$32, %rdx
	cmpq	$2, %rdx
	jle	.L932
.L925:
	sarl	%edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	subl	$1, %edx
	call	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	movq	(%rax), %r14
	jmp	.L926
	.p2align 4,,10
	.p2align 3
.L930:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL40Builtin_Impl_Stats_RegExpLastParenGetterEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L931:
	.cfi_restore_state
	movq	%rdx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L924
	.cfi_endproc
.LFE18424:
	.size	_ZN2v88internal29Builtin_RegExpLastParenGetterEiPmPNS0_7IsolateE, .-_ZN2v88internal29Builtin_RegExpLastParenGetterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Builtin_RegExpLeftContextGetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Builtin_RegExpLeftContextGetterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Builtin_RegExpLeftContextGetterEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Builtin_RegExpLeftContextGetterEiPmPNS0_7IsolateE:
.LFB18427:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L937
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL36Builtin_Impl_RegExpLeftContextGetterENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L937:
	.cfi_restore 6
	jmp	_ZN2v88internalL42Builtin_Impl_Stats_RegExpLeftContextGetterEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18427:
	.size	_ZN2v88internal31Builtin_RegExpLeftContextGetterEiPmPNS0_7IsolateE, .-_ZN2v88internal31Builtin_RegExpLeftContextGetterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Builtin_RegExpRightContextGetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Builtin_RegExpRightContextGetterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Builtin_RegExpRightContextGetterEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Builtin_RegExpRightContextGetterEiPmPNS0_7IsolateE:
.LFB18430:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L942
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL37Builtin_Impl_RegExpRightContextGetterENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L942:
	.cfi_restore 6
	jmp	_ZN2v88internalL43Builtin_Impl_Stats_RegExpRightContextGetterEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18430:
	.size	_ZN2v88internal32Builtin_RegExpRightContextGetterEiPmPNS0_7IsolateE, .-_ZN2v88internal32Builtin_RegExpRightContextGetterEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal31Builtin_RegExpPrototypeToStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal31Builtin_RegExpPrototypeToStringEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal31Builtin_RegExpPrototypeToStringEiPmPNS0_7IsolateE:
.LFB22172:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22172:
	.size	_GLOBAL__sub_I__ZN2v88internal31Builtin_RegExpPrototypeToStringEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal31Builtin_RegExpPrototypeToStringEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal31Builtin_RegExpPrototypeToStringEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL43Builtin_Impl_Stats_RegExpRightContextGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic134,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL43Builtin_Impl_Stats_RegExpRightContextGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic134, @object
	.size	_ZZN2v88internalL43Builtin_Impl_Stats_RegExpRightContextGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic134, 8
_ZZN2v88internalL43Builtin_Impl_Stats_RegExpRightContextGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic134:
	.zero	8
	.section	.bss._ZZN2v88internalL42Builtin_Impl_Stats_RegExpLeftContextGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic126,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Builtin_Impl_Stats_RegExpLeftContextGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic126, @object
	.size	_ZZN2v88internalL42Builtin_Impl_Stats_RegExpLeftContextGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic126, 8
_ZZN2v88internalL42Builtin_Impl_Stats_RegExpLeftContextGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic126:
	.zero	8
	.section	.bss._ZZN2v88internalL40Builtin_Impl_Stats_RegExpLastParenGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic109,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Builtin_Impl_Stats_RegExpLastParenGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic109, @object
	.size	_ZZN2v88internalL40Builtin_Impl_Stats_RegExpLastParenGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic109, 8
_ZZN2v88internalL40Builtin_Impl_Stats_RegExpLastParenGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic109:
	.zero	8
	.section	.bss._ZZN2v88internalL40Builtin_Impl_Stats_RegExpLastMatchGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic103,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Builtin_Impl_Stats_RegExpLastMatchGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic103, @object
	.size	_ZZN2v88internalL40Builtin_Impl_Stats_RegExpLastMatchGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic103, 8
_ZZN2v88internalL40Builtin_Impl_Stats_RegExpLastMatchGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic103:
	.zero	8
	.section	.bss._ZZN2v88internalL36Builtin_Impl_Stats_RegExpInputSetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic89,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Builtin_Impl_Stats_RegExpInputSetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic89, @object
	.size	_ZZN2v88internalL36Builtin_Impl_Stats_RegExpInputSetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic89, 8
_ZZN2v88internalL36Builtin_Impl_Stats_RegExpInputSetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic89:
	.zero	8
	.section	.bss._ZZN2v88internalL36Builtin_Impl_Stats_RegExpInputGetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic82,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Builtin_Impl_Stats_RegExpInputGetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic82, @object
	.size	_ZZN2v88internalL36Builtin_Impl_Stats_RegExpInputGetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic82, 8
_ZZN2v88internalL36Builtin_Impl_Stats_RegExpInputGetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic82:
	.zero	8
	.section	.bss._ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture9GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic75,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture9GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic75, @object
	.size	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture9GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic75, 8
_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture9GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic75:
	.zero	8
	.section	.bss._ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture8GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic74,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture8GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic74, @object
	.size	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture8GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic74, 8
_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture8GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic74:
	.zero	8
	.section	.bss._ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture7GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic73,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture7GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic73, @object
	.size	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture7GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic73, 8
_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture7GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic73:
	.zero	8
	.section	.bss._ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture6GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic72,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture6GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic72, @object
	.size	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture6GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic72, 8
_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture6GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic72:
	.zero	8
	.section	.bss._ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture5GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic71,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture5GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic71, @object
	.size	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture5GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic71, 8
_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture5GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic71:
	.zero	8
	.section	.bss._ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture4GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic70,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture4GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic70, @object
	.size	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture4GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic70, 8
_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture4GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic70:
	.zero	8
	.section	.bss._ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture3GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic69,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture3GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic69, @object
	.size	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture3GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic69, 8
_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture3GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic69:
	.zero	8
	.section	.bss._ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture2GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic68,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture2GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic68, @object
	.size	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture2GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic68, 8
_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture2GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic68:
	.zero	8
	.section	.bss._ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture1GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic67,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture1GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic67, @object
	.size	_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture1GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic67, 8
_ZZN2v88internalL39Builtin_Impl_Stats_RegExpCapture1GetterEiPmPNS0_7IsolateEE27trace_event_unique_atomic67:
	.zero	8
	.section	.bss._ZZN2v88internalL42Builtin_Impl_Stats_RegExpPrototypeToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic19,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Builtin_Impl_Stats_RegExpPrototypeToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic19, @object
	.size	_ZZN2v88internalL42Builtin_Impl_Stats_RegExpPrototypeToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic19, 8
_ZZN2v88internalL42Builtin_Impl_Stats_RegExpPrototypeToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic19:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
