	.file	"handler-table-builder.cc"
	.text
	.section	.text._ZN2v88internal11interpreter19HandlerTableBuilderC2EPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19HandlerTableBuilderC2EPNS0_4ZoneE
	.type	_ZN2v88internal11interpreter19HandlerTableBuilderC2EPNS0_4ZoneE, @function
_ZN2v88internal11interpreter19HandlerTableBuilderC2EPNS0_4ZoneE:
.LFB17786:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	ret
	.cfi_endproc
.LFE17786:
	.size	_ZN2v88internal11interpreter19HandlerTableBuilderC2EPNS0_4ZoneE, .-_ZN2v88internal11interpreter19HandlerTableBuilderC2EPNS0_4ZoneE
	.globl	_ZN2v88internal11interpreter19HandlerTableBuilderC1EPNS0_4ZoneE
	.set	_ZN2v88internal11interpreter19HandlerTableBuilderC1EPNS0_4ZoneE,_ZN2v88internal11interpreter19HandlerTableBuilderC2EPNS0_4ZoneE
	.section	.text._ZN2v88internal11interpreter19HandlerTableBuilder14ToHandlerTableEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19HandlerTableBuilder14ToHandlerTableEPNS0_7IsolateE
	.type	_ZN2v88internal11interpreter19HandlerTableBuilder14ToHandlerTableEPNS0_7IsolateE, @function
_ZN2v88internal11interpreter19HandlerTableBuilder14ToHandlerTableEPNS0_7IsolateE:
.LFB17788:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	subq	8(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	sarq	$5, %rbx
	movl	%ebx, %edi
	call	_ZN2v88internal12HandlerTable14LengthForRangeEi@PLT
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	-80(%rbp), %r12
	movl	%eax, %esi
	call	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE@PLT
	movq	%r12, %rdi
	movq	(%rax), %rsi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal12HandlerTableC1ENS0_9ByteArrayE@PLT
	testl	%ebx, %ebx
	jle	.L4
	leal	-1(%rbx), %eax
	xorl	%ebx, %ebx
	movq	%rax, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L5:
	movq	%rbx, %rax
	movl	%ebx, %esi
	movq	%r12, %rdi
	salq	$5, %rax
	addq	8(%r13), %rax
	movl	(%rax), %edx
	movq	%rax, %r15
	movl	28(%rax), %r14d
	call	_ZN2v88internal12HandlerTable13SetRangeStartEii@PLT
	movl	8(%r15), %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal12HandlerTable11SetRangeEndEii@PLT
	movl	16(%r15), %edx
	movl	%ebx, %esi
	movl	%r14d, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12HandlerTable15SetRangeHandlerEiiNS1_15CatchPredictionE@PLT
	movl	24(%r15), %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal12HandlerTable12SetRangeDataEii@PLT
	movq	%rbx, %rax
	addq	$1, %rbx
	cmpq	%rax, -88(%rbp)
	jne	.L5
.L4:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L9
	movq	-96(%rbp), %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L9:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17788:
	.size	_ZN2v88internal11interpreter19HandlerTableBuilder14ToHandlerTableEPNS0_7IsolateE, .-_ZN2v88internal11interpreter19HandlerTableBuilder14ToHandlerTableEPNS0_7IsolateE
	.section	.text._ZN2v88internal11interpreter19HandlerTableBuilder17SetTryRegionStartEim,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19HandlerTableBuilder17SetTryRegionStartEim
	.type	_ZN2v88internal11interpreter19HandlerTableBuilder17SetTryRegionStartEim, @function
_ZN2v88internal11interpreter19HandlerTableBuilder17SetTryRegionStartEim:
.LFB17790:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	salq	$5, %rsi
	addq	8(%rdi), %rsi
	movq	%rdx, (%rsi)
	ret
	.cfi_endproc
.LFE17790:
	.size	_ZN2v88internal11interpreter19HandlerTableBuilder17SetTryRegionStartEim, .-_ZN2v88internal11interpreter19HandlerTableBuilder17SetTryRegionStartEim
	.section	.text._ZN2v88internal11interpreter19HandlerTableBuilder15SetTryRegionEndEim,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19HandlerTableBuilder15SetTryRegionEndEim
	.type	_ZN2v88internal11interpreter19HandlerTableBuilder15SetTryRegionEndEim, @function
_ZN2v88internal11interpreter19HandlerTableBuilder15SetTryRegionEndEim:
.LFB17791:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	salq	$5, %rsi
	addq	8(%rdi), %rsi
	movq	%rdx, 8(%rsi)
	ret
	.cfi_endproc
.LFE17791:
	.size	_ZN2v88internal11interpreter19HandlerTableBuilder15SetTryRegionEndEim, .-_ZN2v88internal11interpreter19HandlerTableBuilder15SetTryRegionEndEim
	.section	.text._ZN2v88internal11interpreter19HandlerTableBuilder16SetHandlerTargetEim,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19HandlerTableBuilder16SetHandlerTargetEim
	.type	_ZN2v88internal11interpreter19HandlerTableBuilder16SetHandlerTargetEim, @function
_ZN2v88internal11interpreter19HandlerTableBuilder16SetHandlerTargetEim:
.LFB17792:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	salq	$5, %rsi
	addq	8(%rdi), %rsi
	movq	%rdx, 16(%rsi)
	ret
	.cfi_endproc
.LFE17792:
	.size	_ZN2v88internal11interpreter19HandlerTableBuilder16SetHandlerTargetEim, .-_ZN2v88internal11interpreter19HandlerTableBuilder16SetHandlerTargetEim
	.section	.text._ZN2v88internal11interpreter19HandlerTableBuilder13SetPredictionEiNS0_12HandlerTable15CatchPredictionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19HandlerTableBuilder13SetPredictionEiNS0_12HandlerTable15CatchPredictionE
	.type	_ZN2v88internal11interpreter19HandlerTableBuilder13SetPredictionEiNS0_12HandlerTable15CatchPredictionE, @function
_ZN2v88internal11interpreter19HandlerTableBuilder13SetPredictionEiNS0_12HandlerTable15CatchPredictionE:
.LFB17793:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	salq	$5, %rsi
	addq	8(%rdi), %rsi
	movl	%edx, 28(%rsi)
	ret
	.cfi_endproc
.LFE17793:
	.size	_ZN2v88internal11interpreter19HandlerTableBuilder13SetPredictionEiNS0_12HandlerTable15CatchPredictionE, .-_ZN2v88internal11interpreter19HandlerTableBuilder13SetPredictionEiNS0_12HandlerTable15CatchPredictionE
	.section	.text._ZN2v88internal11interpreter19HandlerTableBuilder18SetContextRegisterEiNS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19HandlerTableBuilder18SetContextRegisterEiNS1_8RegisterE
	.type	_ZN2v88internal11interpreter19HandlerTableBuilder18SetContextRegisterEiNS1_8RegisterE, @function
_ZN2v88internal11interpreter19HandlerTableBuilder18SetContextRegisterEiNS1_8RegisterE:
.LFB17794:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	salq	$5, %rsi
	addq	8(%rdi), %rsi
	movl	%edx, 24(%rsi)
	ret
	.cfi_endproc
.LFE17794:
	.size	_ZN2v88internal11interpreter19HandlerTableBuilder18SetContextRegisterEiNS1_8RegisterE, .-_ZN2v88internal11interpreter19HandlerTableBuilder18SetContextRegisterEiNS1_8RegisterE
	.section	.rodata._ZNSt6vectorIN2v88internal11interpreter19HandlerTableBuilder5EntryENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal11interpreter19HandlerTableBuilder5EntryENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal11interpreter19HandlerTableBuilder5EntryENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal11interpreter19HandlerTableBuilder5EntryENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal11interpreter19HandlerTableBuilder5EntryENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIN2v88internal11interpreter19HandlerTableBuilder5EntryENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB20659:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movq	8(%rdi), %r14
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$5, %rax
	cmpq	$67108863, %rax
	je	.L31
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r13
	movq	%rsi, %rbx
	subq	%r14, %rdx
	testq	%rax, %rax
	je	.L25
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L32
	movl	$2147483616, %esi
	movl	$2147483616, %ecx
.L17:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r8
	subq	%rax, %r8
	cmpq	%rsi, %r8
	jb	.L33
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L20:
	leaq	(%rax,%rcx), %rdi
	leaq	32(%rax), %rsi
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L32:
	testq	%rcx, %rcx
	jne	.L34
	movl	$32, %esi
	xorl	%edi, %edi
	xorl	%eax, %eax
.L18:
	movdqu	(%r15), %xmm5
	movups	%xmm5, (%rax,%rdx)
	movdqu	16(%r15), %xmm6
	movups	%xmm6, 16(%rax,%rdx)
	cmpq	%r14, %rbx
	je	.L21
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L22:
	movdqu	(%rdx), %xmm1
	addq	$32, %rdx
	addq	$32, %rcx
	movups	%xmm1, -32(%rcx)
	movdqu	-16(%rdx), %xmm2
	movups	%xmm2, -16(%rcx)
	cmpq	%rdx, %rbx
	jne	.L22
	movq	%rbx, %rdx
	subq	%r14, %rdx
	leaq	32(%rax,%rdx), %rsi
.L21:
	cmpq	%r12, %rbx
	je	.L23
	movq	%rbx, %rdx
	movq	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L24:
	movdqu	(%rdx), %xmm3
	addq	$32, %rdx
	addq	$32, %rcx
	movups	%xmm3, -32(%rcx)
	movdqu	-16(%rdx), %xmm4
	movups	%xmm4, -16(%rcx)
	cmpq	%rdx, %r12
	jne	.L24
	subq	%rbx, %r12
	addq	%r12, %rsi
.L23:
	movq	%rax, %xmm0
	movq	%rsi, %xmm7
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movl	$32, %esi
	movl	$32, %ecx
	jmp	.L17
.L33:
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	jmp	.L20
.L34:
	cmpq	$67108863, %rcx
	movl	$67108863, %eax
	cmova	%rax, %rcx
	salq	$5, %rcx
	movq	%rcx, %rsi
	jmp	.L17
.L31:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE20659:
	.size	_ZNSt6vectorIN2v88internal11interpreter19HandlerTableBuilder5EntryENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIN2v88internal11interpreter19HandlerTableBuilder5EntryENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZN2v88internal11interpreter19HandlerTableBuilder15NewHandlerEntryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19HandlerTableBuilder15NewHandlerEntryEv
	.type	_ZN2v88internal11interpreter19HandlerTableBuilder15NewHandlerEntryEv, @function
_ZN2v88internal11interpreter19HandlerTableBuilder15NewHandlerEntryEv:
.LFB17789:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -32(%rbp)
	movq	%rsi, %rax
	subq	8(%rdi), %rax
	movaps	%xmm0, -48(%rbp)
	movl	$2147483647, -24(%rbp)
	sarq	$5, %rax
	cmpq	24(%rdi), %rsi
	je	.L36
	movups	%xmm0, (%rsi)
	movdqa	-32(%rbp), %xmm1
	movups	%xmm1, 16(%rsi)
	addq	$32, 16(%rdi)
.L35:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L40
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	leaq	-48(%rbp), %rdx
	movq	%rax, -56(%rbp)
	call	_ZNSt6vectorIN2v88internal11interpreter19HandlerTableBuilder5EntryENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-56(%rbp), %rax
	jmp	.L35
.L40:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17789:
	.size	_ZN2v88internal11interpreter19HandlerTableBuilder15NewHandlerEntryEv, .-_ZN2v88internal11interpreter19HandlerTableBuilder15NewHandlerEntryEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11interpreter19HandlerTableBuilderC2EPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11interpreter19HandlerTableBuilderC2EPNS0_4ZoneE, @function
_GLOBAL__sub_I__ZN2v88internal11interpreter19HandlerTableBuilderC2EPNS0_4ZoneE:
.LFB21530:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21530:
	.size	_GLOBAL__sub_I__ZN2v88internal11interpreter19HandlerTableBuilderC2EPNS0_4ZoneE, .-_GLOBAL__sub_I__ZN2v88internal11interpreter19HandlerTableBuilderC2EPNS0_4ZoneE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11interpreter19HandlerTableBuilderC2EPNS0_4ZoneE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
