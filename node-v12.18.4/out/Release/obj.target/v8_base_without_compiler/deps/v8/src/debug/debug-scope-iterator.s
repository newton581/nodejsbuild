	.file	"debug-scope-iterator.cc"
	.text
	.section	.text._ZN2v88internal18DebugScopeIterator4DoneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18DebugScopeIterator4DoneEv
	.type	_ZN2v88internal18DebugScopeIterator4DoneEv, @function
_ZN2v88internal18DebugScopeIterator4DoneEv:
.LFB18865:
	.cfi_startproc
	endbr64
	cmpq	$0, 48(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE18865:
	.size	_ZN2v88internal18DebugScopeIterator4DoneEv, .-_ZN2v88internal18DebugScopeIterator4DoneEv
	.section	.text._ZN2v88internal18DebugScopeIterator11GetScriptIdEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18DebugScopeIterator11GetScriptIdEv
	.type	_ZN2v88internal18DebugScopeIterator11GetScriptIdEv, @function
_ZN2v88internal18DebugScopeIterator11GetScriptIdEv:
.LFB18870:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	movq	(%rax), %rax
	movslq	67(%rax), %rax
	ret
	.cfi_endproc
.LFE18870:
	.size	_ZN2v88internal18DebugScopeIterator11GetScriptIdEv, .-_ZN2v88internal18DebugScopeIterator11GetScriptIdEv
	.section	.text._ZN2v88internal22DebugWasmScopeIterator4DoneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22DebugWasmScopeIterator4DoneEv
	.type	_ZN2v88internal22DebugWasmScopeIterator4DoneEv, @function
_ZN2v88internal22DebugWasmScopeIterator4DoneEv:
.LFB18879:
	.cfi_startproc
	endbr64
	cmpl	$1, 28(%rdi)
	seta	%al
	ret
	.cfi_endproc
.LFE18879:
	.size	_ZN2v88internal22DebugWasmScopeIterator4DoneEv, .-_ZN2v88internal22DebugWasmScopeIterator4DoneEv
	.section	.text._ZN2v88internal22DebugWasmScopeIterator7AdvanceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22DebugWasmScopeIterator7AdvanceEv
	.type	_ZN2v88internal22DebugWasmScopeIterator7AdvanceEv, @function
_ZN2v88internal22DebugWasmScopeIterator7AdvanceEv:
.LFB18880:
	.cfi_startproc
	endbr64
	movl	28(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	setne	%al
	addl	$1, %eax
	movl	%eax, 28(%rdi)
	ret
	.cfi_endproc
.LFE18880:
	.size	_ZN2v88internal22DebugWasmScopeIterator7AdvanceEv, .-_ZN2v88internal22DebugWasmScopeIterator7AdvanceEv
	.section	.text._ZN2v88internal22DebugWasmScopeIterator7GetTypeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22DebugWasmScopeIterator7GetTypeEv
	.type	_ZN2v88internal22DebugWasmScopeIterator7GetTypeEv, @function
_ZN2v88internal22DebugWasmScopeIterator7GetTypeEv:
.LFB18881:
	.cfi_startproc
	endbr64
	movl	28(%rdi), %eax
	ret
	.cfi_endproc
.LFE18881:
	.size	_ZN2v88internal22DebugWasmScopeIterator7GetTypeEv, .-_ZN2v88internal22DebugWasmScopeIterator7GetTypeEv
	.section	.text._ZN2v88internal22DebugWasmScopeIterator11GetScriptIdEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22DebugWasmScopeIterator11GetScriptIdEv
	.type	_ZN2v88internal22DebugWasmScopeIterator11GetScriptIdEv, @function
_ZN2v88internal22DebugWasmScopeIterator11GetScriptIdEv:
.LFB18883:
	.cfi_startproc
	endbr64
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE18883:
	.size	_ZN2v88internal22DebugWasmScopeIterator11GetScriptIdEv, .-_ZN2v88internal22DebugWasmScopeIterator11GetScriptIdEv
	.section	.text._ZN2v88internal22DebugWasmScopeIterator20GetFunctionDebugNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22DebugWasmScopeIterator20GetFunctionDebugNameEv
	.type	_ZN2v88internal22DebugWasmScopeIterator20GetFunctionDebugNameEv, @function
_ZN2v88internal22DebugWasmScopeIterator20GetFunctionDebugNameEv:
.LFB18884:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	subq	$-128, %rax
	ret
	.cfi_endproc
.LFE18884:
	.size	_ZN2v88internal22DebugWasmScopeIterator20GetFunctionDebugNameEv, .-_ZN2v88internal22DebugWasmScopeIterator20GetFunctionDebugNameEv
	.section	.text._ZN2v88internal22DebugWasmScopeIterator15HasLocationInfoEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22DebugWasmScopeIterator15HasLocationInfoEv
	.type	_ZN2v88internal22DebugWasmScopeIterator15HasLocationInfoEv, @function
_ZN2v88internal22DebugWasmScopeIterator15HasLocationInfoEv:
.LFB18885:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE18885:
	.size	_ZN2v88internal22DebugWasmScopeIterator15HasLocationInfoEv, .-_ZN2v88internal22DebugWasmScopeIterator15HasLocationInfoEv
	.section	.text._ZN2v88internal22DebugWasmScopeIterator16SetVariableValueENS_5LocalINS_6StringEEENS2_INS_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22DebugWasmScopeIterator16SetVariableValueENS_5LocalINS_6StringEEENS2_INS_5ValueEEE
	.type	_ZN2v88internal22DebugWasmScopeIterator16SetVariableValueENS_5LocalINS_6StringEEENS2_INS_5ValueEEE, @function
_ZN2v88internal22DebugWasmScopeIterator16SetVariableValueENS_5LocalINS_6StringEEENS2_INS_5ValueEEE:
.LFB18888:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE18888:
	.size	_ZN2v88internal22DebugWasmScopeIterator16SetVariableValueENS_5LocalINS_6StringEEENS2_INS_5ValueEEE, .-_ZN2v88internal22DebugWasmScopeIterator16SetVariableValueENS_5LocalINS_6StringEEENS2_INS_5ValueEEE
	.section	.text._ZN2v88internal22DebugWasmScopeIteratorD2Ev,"axG",@progbits,_ZN2v88internal22DebugWasmScopeIteratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal22DebugWasmScopeIteratorD2Ev
	.type	_ZN2v88internal22DebugWasmScopeIteratorD2Ev, @function
_ZN2v88internal22DebugWasmScopeIteratorD2Ev:
.LFB23253:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE23253:
	.size	_ZN2v88internal22DebugWasmScopeIteratorD2Ev, .-_ZN2v88internal22DebugWasmScopeIteratorD2Ev
	.weak	_ZN2v88internal22DebugWasmScopeIteratorD1Ev
	.set	_ZN2v88internal22DebugWasmScopeIteratorD1Ev,_ZN2v88internal22DebugWasmScopeIteratorD2Ev
	.section	.text._ZN2v88internal18DebugScopeIterator7GetTypeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18DebugScopeIterator7GetTypeEv
	.type	_ZN2v88internal18DebugScopeIterator7GetTypeEv, @function
_ZN2v88internal18DebugScopeIterator7GetTypeEv:
.LFB18868:
	.cfi_startproc
	endbr64
	addq	$8, %rdi
	jmp	_ZNK2v88internal13ScopeIterator4TypeEv@PLT
	.cfi_endproc
.LFE18868:
	.size	_ZN2v88internal18DebugScopeIterator7GetTypeEv, .-_ZN2v88internal18DebugScopeIterator7GetTypeEv
	.section	.text._ZN2v88internal18DebugScopeIterator9GetObjectEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18DebugScopeIterator9GetObjectEv
	.type	_ZN2v88internal18DebugScopeIterator9GetObjectEv, @function
_ZN2v88internal18DebugScopeIterator9GetObjectEv:
.LFB18869:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$8, %rdi
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal13ScopeIterator11ScopeObjectENS1_4ModeE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18869:
	.size	_ZN2v88internal18DebugScopeIterator9GetObjectEv, .-_ZN2v88internal18DebugScopeIterator9GetObjectEv
	.section	.text._ZN2v88internal18DebugScopeIterator20GetFunctionDebugNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18DebugScopeIterator20GetFunctionDebugNameEv
	.type	_ZN2v88internal18DebugScopeIterator20GetFunctionDebugNameEv, @function
_ZN2v88internal18DebugScopeIterator20GetFunctionDebugNameEv:
.LFB18871:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal13ScopeIterator20GetFunctionDebugNameEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18871:
	.size	_ZN2v88internal18DebugScopeIterator20GetFunctionDebugNameEv, .-_ZN2v88internal18DebugScopeIterator20GetFunctionDebugNameEv
	.section	.text._ZN2v88internal18DebugScopeIterator15HasLocationInfoEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18DebugScopeIterator15HasLocationInfoEv
	.type	_ZN2v88internal18DebugScopeIterator15HasLocationInfoEv, @function
_ZN2v88internal18DebugScopeIterator15HasLocationInfoEv:
.LFB18872:
	.cfi_startproc
	endbr64
	addq	$8, %rdi
	jmp	_ZN2v88internal13ScopeIterator15HasPositionInfoEv@PLT
	.cfi_endproc
.LFE18872:
	.size	_ZN2v88internal18DebugScopeIterator15HasLocationInfoEv, .-_ZN2v88internal18DebugScopeIterator15HasLocationInfoEv
	.section	.text._ZN2v88internal18DebugScopeIterator16GetStartLocationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18DebugScopeIterator16GetStartLocationEv
	.type	_ZN2v88internal18DebugScopeIterator16GetStartLocationEv, @function
_ZN2v88internal18DebugScopeIterator16GetStartLocationEv:
.LFB18873:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	48(%rdi), %r12
	call	_ZN2v88internal13ScopeIterator14start_positionEv@PLT
	addq	$24, %rsp
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNK2v85debug6Script17GetSourceLocationEi@PLT
	.cfi_endproc
.LFE18873:
	.size	_ZN2v88internal18DebugScopeIterator16GetStartLocationEv, .-_ZN2v88internal18DebugScopeIterator16GetStartLocationEv
	.section	.text._ZN2v88internal18DebugScopeIterator14GetEndLocationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18DebugScopeIterator14GetEndLocationEv
	.type	_ZN2v88internal18DebugScopeIterator14GetEndLocationEv, @function
_ZN2v88internal18DebugScopeIterator14GetEndLocationEv:
.LFB18874:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	48(%rdi), %r12
	call	_ZN2v88internal13ScopeIterator12end_positionEv@PLT
	addq	$24, %rsp
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNK2v85debug6Script17GetSourceLocationEi@PLT
	.cfi_endproc
.LFE18874:
	.size	_ZN2v88internal18DebugScopeIterator14GetEndLocationEv, .-_ZN2v88internal18DebugScopeIterator14GetEndLocationEv
	.section	.text._ZN2v88internal18DebugScopeIterator16SetVariableValueENS_5LocalINS_6StringEEENS2_INS_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18DebugScopeIterator16SetVariableValueENS_5LocalINS_6StringEEENS2_INS_5ValueEEE
	.type	_ZN2v88internal18DebugScopeIterator16SetVariableValueENS_5LocalINS_6StringEEENS2_INS_5ValueEEE, @function
_ZN2v88internal18DebugScopeIterator16SetVariableValueENS_5LocalINS_6StringEEENS2_INS_5ValueEEE:
.LFB18875:
	.cfi_startproc
	endbr64
	addq	$8, %rdi
	jmp	_ZN2v88internal13ScopeIterator16SetVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE18875:
	.size	_ZN2v88internal18DebugScopeIterator16SetVariableValueENS_5LocalINS_6StringEEENS2_INS_5ValueEEE, .-_ZN2v88internal18DebugScopeIterator16SetVariableValueENS_5LocalINS_6StringEEENS2_INS_5ValueEEE
	.section	.text._ZN2v88internal22DebugWasmScopeIterator9GetObjectEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22DebugWasmScopeIterator9GetObjectEv
	.type	_ZN2v88internal22DebugWasmScopeIterator9GetObjectEv, @function
_ZN2v88internal22DebugWasmScopeIterator9GetObjectEv:
.LFB18882:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	8(%rdi), %r12
	movq	16(%rdi), %rdi
	call	_ZNK2v88internal25WasmInterpreterEntryFrame10debug_infoEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L26
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
	movl	28(%rbx), %eax
	testl	%eax, %eax
	je	.L29
.L36:
	cmpl	$1, %eax
	je	.L30
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	41088(%r12), %rdi
	cmpq	41096(%r12), %rdi
	je	.L35
.L28:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdi)
	movl	28(%rbx), %eax
	testl	%eax, %eax
	jne	.L36
.L29:
	movq	16(%rbx), %rax
	movl	24(%rbx), %edx
	movq	32(%rax), %rsi
	call	_ZN2v88internal13WasmDebugInfo20GetGlobalScopeObjectENS0_6HandleIS1_EEmi@PLT
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movl	24(%rbx), %edx
	movq	32(%rax), %rsi
	call	_ZN2v88internal13WasmDebugInfo19GetLocalScopeObjectENS0_6HandleIS1_EEmi@PLT
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rax, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L28
	.cfi_endproc
.LFE18882:
	.size	_ZN2v88internal22DebugWasmScopeIterator9GetObjectEv, .-_ZN2v88internal22DebugWasmScopeIterator9GetObjectEv
	.section	.text._ZN2v88internal22DebugWasmScopeIterator16GetStartLocationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22DebugWasmScopeIterator16GetStartLocationEv
	.type	_ZN2v88internal22DebugWasmScopeIterator16GetStartLocationEv, @function
_ZN2v88internal22DebugWasmScopeIterator16GetStartLocationEv:
.LFB18886:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rdi
	call	_ZN2v85debug8LocationC1Ev@PLT
	movq	-32(%rbp), %rax
	movl	-24(%rbp), %edx
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L40
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L40:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18886:
	.size	_ZN2v88internal22DebugWasmScopeIterator16GetStartLocationEv, .-_ZN2v88internal22DebugWasmScopeIterator16GetStartLocationEv
	.globl	_ZN2v88internal22DebugWasmScopeIterator14GetEndLocationEv
	.set	_ZN2v88internal22DebugWasmScopeIterator14GetEndLocationEv,_ZN2v88internal22DebugWasmScopeIterator16GetStartLocationEv
	.section	.text._ZN2v88internal22DebugWasmScopeIteratorD0Ev,"axG",@progbits,_ZN2v88internal22DebugWasmScopeIteratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal22DebugWasmScopeIteratorD0Ev
	.type	_ZN2v88internal22DebugWasmScopeIteratorD0Ev, @function
_ZN2v88internal22DebugWasmScopeIteratorD0Ev:
.LFB23255:
	.cfi_startproc
	endbr64
	movl	$32, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23255:
	.size	_ZN2v88internal22DebugWasmScopeIteratorD0Ev, .-_ZN2v88internal22DebugWasmScopeIteratorD0Ev
	.section	.text._ZN2v88internal18DebugScopeIteratorD2Ev,"axG",@progbits,_ZN2v88internal18DebugScopeIteratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18DebugScopeIteratorD2Ev
	.type	_ZN2v88internal18DebugScopeIteratorD2Ev, @function
_ZN2v88internal18DebugScopeIteratorD2Ev:
.LFB23257:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal18DebugScopeIteratorE(%rip), %rax
	addq	$8, %rdi
	movq	%rax, -8(%rdi)
	jmp	_ZN2v88internal13ScopeIteratorD1Ev@PLT
	.cfi_endproc
.LFE23257:
	.size	_ZN2v88internal18DebugScopeIteratorD2Ev, .-_ZN2v88internal18DebugScopeIteratorD2Ev
	.weak	_ZN2v88internal18DebugScopeIteratorD1Ev
	.set	_ZN2v88internal18DebugScopeIteratorD1Ev,_ZN2v88internal18DebugScopeIteratorD2Ev
	.section	.text._ZN2v88internal18DebugScopeIteratorD0Ev,"axG",@progbits,_ZN2v88internal18DebugScopeIteratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18DebugScopeIteratorD0Ev
	.type	_ZN2v88internal18DebugScopeIteratorD0Ev, @function
_ZN2v88internal18DebugScopeIteratorD0Ev:
.LFB23259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal18DebugScopeIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	call	_ZN2v88internal13ScopeIteratorD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23259:
	.size	_ZN2v88internal18DebugScopeIteratorD0Ev, .-_ZN2v88internal18DebugScopeIteratorD0Ev
	.section	.text._ZN2v88internal18DebugScopeIterator7AdvanceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18DebugScopeIterator7AdvanceEv
	.type	_ZN2v88internal18DebugScopeIterator7AdvanceEv, @function
_ZN2v88internal18DebugScopeIterator7AdvanceEv:
.LFB18866:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	8(%rdi), %rbx
	.p2align 4,,10
	.p2align 3
.L53:
	movq	%rbx, %rdi
	call	_ZN2v88internal13ScopeIterator4NextEv@PLT
	cmpq	$0, 48(%r12)
	je	.L45
	movq	%rbx, %rdi
	call	_ZNK2v88internal13ScopeIterator4TypeEv@PLT
	cmpl	$1, %eax
	je	.L45
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZNK2v88internal13ScopeIterator14DeclaresLocalsENS1_4ModeE@PLT
	testb	%al, %al
	je	.L53
.L45:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18866:
	.size	_ZN2v88internal18DebugScopeIterator7AdvanceEv, .-_ZN2v88internal18DebugScopeIterator7AdvanceEv
	.section	.text._ZN2v85debug13ScopeIterator24CreateForGeneratorObjectEPNS_7IsolateENS_5LocalINS_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v85debug13ScopeIterator24CreateForGeneratorObjectEPNS_7IsolateENS_5LocalINS_6ObjectEEE
	.type	_ZN2v85debug13ScopeIterator24CreateForGeneratorObjectEPNS_7IsolateENS_5LocalINS_6ObjectEEE, @function
_ZN2v85debug13ScopeIterator24CreateForGeneratorObjectEPNS_7IsolateENS_5LocalINS_6ObjectEEE:
.LFB18848:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$104, %edi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_Znwm@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %rbx
	leaq	16+_ZTVN2v88internal18DebugScopeIteratorE(%rip), %rax
	leaq	8(%rbx), %r15
	movq	%rax, (%rbx)
	movq	%r15, %rdi
	call	_ZN2v88internal13ScopeIteratorC1EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE@PLT
	cmpq	$0, 48(%rbx)
	je	.L55
.L69:
	movq	%r15, %rdi
	call	_ZNK2v88internal13ScopeIterator4TypeEv@PLT
	cmpl	$1, %eax
	je	.L55
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal13ScopeIterator14DeclaresLocalsENS1_4ModeE@PLT
	testb	%al, %al
	jne	.L55
	movq	%r15, %rdi
	call	_ZN2v88internal13ScopeIterator4NextEv@PLT
	cmpq	$0, 48(%rbx)
	jne	.L69
.L55:
	movq	%rbx, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18848:
	.size	_ZN2v85debug13ScopeIterator24CreateForGeneratorObjectEPNS_7IsolateENS_5LocalINS_6ObjectEEE, .-_ZN2v85debug13ScopeIterator24CreateForGeneratorObjectEPNS_7IsolateENS_5LocalINS_6ObjectEEE
	.section	.text._ZN2v88internal18DebugScopeIteratorC2EPNS0_7IsolateEPNS0_14FrameInspectorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18DebugScopeIteratorC2EPNS0_7IsolateEPNS0_14FrameInspectorE
	.type	_ZN2v88internal18DebugScopeIteratorC2EPNS0_7IsolateEPNS0_14FrameInspectorE, @function
_ZN2v88internal18DebugScopeIteratorC2EPNS0_7IsolateEPNS0_14FrameInspectorE:
.LFB18857:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal18DebugScopeIteratorE(%rip), %rax
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movq	%r12, %rdi
	call	_ZN2v88internal13ScopeIteratorC1EPNS0_7IsolateEPNS0_14FrameInspectorENS1_6OptionE@PLT
	cmpq	$0, 48(%rbx)
	je	.L70
.L85:
	movq	%r12, %rdi
	call	_ZNK2v88internal13ScopeIterator4TypeEv@PLT
	cmpl	$1, %eax
	je	.L70
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal13ScopeIterator14DeclaresLocalsENS1_4ModeE@PLT
	testb	%al, %al
	jne	.L70
	movq	%r12, %rdi
	call	_ZN2v88internal13ScopeIterator4NextEv@PLT
	cmpq	$0, 48(%rbx)
	jne	.L85
.L70:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18857:
	.size	_ZN2v88internal18DebugScopeIteratorC2EPNS0_7IsolateEPNS0_14FrameInspectorE, .-_ZN2v88internal18DebugScopeIteratorC2EPNS0_7IsolateEPNS0_14FrameInspectorE
	.globl	_ZN2v88internal18DebugScopeIteratorC1EPNS0_7IsolateEPNS0_14FrameInspectorE
	.set	_ZN2v88internal18DebugScopeIteratorC1EPNS0_7IsolateEPNS0_14FrameInspectorE,_ZN2v88internal18DebugScopeIteratorC2EPNS0_7IsolateEPNS0_14FrameInspectorE
	.section	.text._ZN2v88internal18DebugScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18DebugScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	.type	_ZN2v88internal18DebugScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE, @function
_ZN2v88internal18DebugScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE:
.LFB18860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal18DebugScopeIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movq	%r12, %rdi
	call	_ZN2v88internal13ScopeIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE@PLT
	cmpq	$0, 48(%rbx)
	je	.L86
.L101:
	movq	%r12, %rdi
	call	_ZNK2v88internal13ScopeIterator4TypeEv@PLT
	cmpl	$1, %eax
	je	.L86
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal13ScopeIterator14DeclaresLocalsENS1_4ModeE@PLT
	testb	%al, %al
	jne	.L86
	movq	%r12, %rdi
	call	_ZN2v88internal13ScopeIterator4NextEv@PLT
	cmpq	$0, 48(%rbx)
	jne	.L101
.L86:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18860:
	.size	_ZN2v88internal18DebugScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE, .-_ZN2v88internal18DebugScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	.globl	_ZN2v88internal18DebugScopeIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	.set	_ZN2v88internal18DebugScopeIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE,_ZN2v88internal18DebugScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	.section	.text._ZN2v85debug13ScopeIterator17CreateForFunctionEPNS_7IsolateENS_5LocalINS_8FunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v85debug13ScopeIterator17CreateForFunctionEPNS_7IsolateENS_5LocalINS_8FunctionEEE
	.type	_ZN2v85debug13ScopeIterator17CreateForFunctionEPNS_7IsolateENS_5LocalINS_8FunctionEEE, @function
_ZN2v85debug13ScopeIterator17CreateForFunctionEPNS_7IsolateENS_5LocalINS_8FunctionEEE:
.LFB18847:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	(%rdx), %rax
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	je	.L103
.L107:
	movq	$0, (%r12)
.L102:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movq	31(%rax), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	subw	$138, %ax
	cmpw	$9, %ax
	ja	.L107
	movl	$104, %edi
	movq	%rsi, -40(%rbp)
	call	_Znwm@PLT
	movq	-40(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rax, %rbx
	movq	%rax, %rdi
	call	_ZN2v88internal18DebugScopeIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	movq	%rbx, (%r12)
	jmp	.L102
	.cfi_endproc
.LFE18847:
	.size	_ZN2v85debug13ScopeIterator17CreateForFunctionEPNS_7IsolateENS_5LocalINS_8FunctionEEE, .-_ZN2v85debug13ScopeIterator17CreateForFunctionEPNS_7IsolateENS_5LocalINS_8FunctionEEE
	.section	.text._ZN2v88internal18DebugScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18DebugScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE
	.type	_ZN2v88internal18DebugScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE, @function
_ZN2v88internal18DebugScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE:
.LFB18863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal18DebugScopeIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movq	%r12, %rdi
	call	_ZN2v88internal13ScopeIteratorC1EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE@PLT
	cmpq	$0, 48(%rbx)
	je	.L108
.L123:
	movq	%r12, %rdi
	call	_ZNK2v88internal13ScopeIterator4TypeEv@PLT
	cmpl	$1, %eax
	je	.L108
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal13ScopeIterator14DeclaresLocalsENS1_4ModeE@PLT
	testb	%al, %al
	jne	.L108
	movq	%r12, %rdi
	call	_ZN2v88internal13ScopeIterator4NextEv@PLT
	cmpq	$0, 48(%rbx)
	jne	.L123
.L108:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18863:
	.size	_ZN2v88internal18DebugScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE, .-_ZN2v88internal18DebugScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE
	.globl	_ZN2v88internal18DebugScopeIteratorC1EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE
	.set	_ZN2v88internal18DebugScopeIteratorC1EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE,_ZN2v88internal18DebugScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE
	.section	.text._ZN2v88internal18DebugScopeIterator12ShouldIgnoreEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18DebugScopeIterator12ShouldIgnoreEv
	.type	_ZN2v88internal18DebugScopeIterator12ShouldIgnoreEv, @function
_ZN2v88internal18DebugScopeIterator12ShouldIgnoreEv:
.LFB18867:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	8(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZNK2v88internal13ScopeIterator4TypeEv@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpl	$1, %r8d
	je	.L124
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal13ScopeIterator14DeclaresLocalsENS1_4ModeE@PLT
	xorl	$1, %eax
.L124:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18867:
	.size	_ZN2v88internal18DebugScopeIterator12ShouldIgnoreEv, .-_ZN2v88internal18DebugScopeIterator12ShouldIgnoreEv
	.section	.text._ZN2v88internal22DebugWasmScopeIteratorC2EPNS0_7IsolateEPNS0_13StandardFrameEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22DebugWasmScopeIteratorC2EPNS0_7IsolateEPNS0_13StandardFrameEi
	.type	_ZN2v88internal22DebugWasmScopeIteratorC2EPNS0_7IsolateEPNS0_13StandardFrameEi, @function
_ZN2v88internal22DebugWasmScopeIteratorC2EPNS0_7IsolateEPNS0_13StandardFrameEi:
.LFB18877:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal22DebugWasmScopeIteratorE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movq	%rdx, 16(%rdi)
	movl	%ecx, 24(%rdi)
	movl	$0, 28(%rdi)
	ret
	.cfi_endproc
.LFE18877:
	.size	_ZN2v88internal22DebugWasmScopeIteratorC2EPNS0_7IsolateEPNS0_13StandardFrameEi, .-_ZN2v88internal22DebugWasmScopeIteratorC2EPNS0_7IsolateEPNS0_13StandardFrameEi
	.globl	_ZN2v88internal22DebugWasmScopeIteratorC1EPNS0_7IsolateEPNS0_13StandardFrameEi
	.set	_ZN2v88internal22DebugWasmScopeIteratorC1EPNS0_7IsolateEPNS0_13StandardFrameEi,_ZN2v88internal22DebugWasmScopeIteratorC2EPNS0_7IsolateEPNS0_13StandardFrameEi
	.section	.text.startup._GLOBAL__sub_I__ZN2v85debug13ScopeIterator17CreateForFunctionEPNS_7IsolateENS_5LocalINS_8FunctionEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v85debug13ScopeIterator17CreateForFunctionEPNS_7IsolateENS_5LocalINS_8FunctionEEE, @function
_GLOBAL__sub_I__ZN2v85debug13ScopeIterator17CreateForFunctionEPNS_7IsolateENS_5LocalINS_8FunctionEEE:
.LFB23349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23349:
	.size	_GLOBAL__sub_I__ZN2v85debug13ScopeIterator17CreateForFunctionEPNS_7IsolateENS_5LocalINS_8FunctionEEE, .-_GLOBAL__sub_I__ZN2v85debug13ScopeIterator17CreateForFunctionEPNS_7IsolateENS_5LocalINS_8FunctionEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v85debug13ScopeIterator17CreateForFunctionEPNS_7IsolateENS_5LocalINS_8FunctionEEE
	.weak	_ZTVN2v88internal18DebugScopeIteratorE
	.section	.data.rel.ro.local._ZTVN2v88internal18DebugScopeIteratorE,"awG",@progbits,_ZTVN2v88internal18DebugScopeIteratorE,comdat
	.align 8
	.type	_ZTVN2v88internal18DebugScopeIteratorE, @object
	.size	_ZTVN2v88internal18DebugScopeIteratorE, 112
_ZTVN2v88internal18DebugScopeIteratorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal18DebugScopeIteratorD1Ev
	.quad	_ZN2v88internal18DebugScopeIteratorD0Ev
	.quad	_ZN2v88internal18DebugScopeIterator4DoneEv
	.quad	_ZN2v88internal18DebugScopeIterator7AdvanceEv
	.quad	_ZN2v88internal18DebugScopeIterator7GetTypeEv
	.quad	_ZN2v88internal18DebugScopeIterator9GetObjectEv
	.quad	_ZN2v88internal18DebugScopeIterator20GetFunctionDebugNameEv
	.quad	_ZN2v88internal18DebugScopeIterator11GetScriptIdEv
	.quad	_ZN2v88internal18DebugScopeIterator15HasLocationInfoEv
	.quad	_ZN2v88internal18DebugScopeIterator16GetStartLocationEv
	.quad	_ZN2v88internal18DebugScopeIterator14GetEndLocationEv
	.quad	_ZN2v88internal18DebugScopeIterator16SetVariableValueENS_5LocalINS_6StringEEENS2_INS_5ValueEEE
	.weak	_ZTVN2v88internal22DebugWasmScopeIteratorE
	.section	.data.rel.ro.local._ZTVN2v88internal22DebugWasmScopeIteratorE,"awG",@progbits,_ZTVN2v88internal22DebugWasmScopeIteratorE,comdat
	.align 8
	.type	_ZTVN2v88internal22DebugWasmScopeIteratorE, @object
	.size	_ZTVN2v88internal22DebugWasmScopeIteratorE, 112
_ZTVN2v88internal22DebugWasmScopeIteratorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal22DebugWasmScopeIteratorD1Ev
	.quad	_ZN2v88internal22DebugWasmScopeIteratorD0Ev
	.quad	_ZN2v88internal22DebugWasmScopeIterator4DoneEv
	.quad	_ZN2v88internal22DebugWasmScopeIterator7AdvanceEv
	.quad	_ZN2v88internal22DebugWasmScopeIterator7GetTypeEv
	.quad	_ZN2v88internal22DebugWasmScopeIterator9GetObjectEv
	.quad	_ZN2v88internal22DebugWasmScopeIterator20GetFunctionDebugNameEv
	.quad	_ZN2v88internal22DebugWasmScopeIterator11GetScriptIdEv
	.quad	_ZN2v88internal22DebugWasmScopeIterator15HasLocationInfoEv
	.quad	_ZN2v88internal22DebugWasmScopeIterator16GetStartLocationEv
	.quad	_ZN2v88internal22DebugWasmScopeIterator14GetEndLocationEv
	.quad	_ZN2v88internal22DebugWasmScopeIterator16SetVariableValueENS_5LocalINS_6StringEEENS2_INS_5ValueEEE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
