	.file	"runtime-function.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB3941:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE3941:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB3942:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3942:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB3944:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3944:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB9017:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L14
.L5:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L5
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE9017:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL45Stats_Runtime_FunctionGetScriptSourcePositionEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC1:
	.string	"V8.Runtime_Runtime_FunctionGetScriptSourcePosition"
	.section	.rodata._ZN2v88internalL45Stats_Runtime_FunctionGetScriptSourcePositionEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC2:
	.string	"args[0].IsJSFunction()"
.LC3:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL45Stats_Runtime_FunctionGetScriptSourcePositionEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL45Stats_Runtime_FunctionGetScriptSourcePositionEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL45Stats_Runtime_FunctionGetScriptSourcePositionEiPmPNS0_7IsolateE.isra.0:
.LFB25202:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L46
.L16:
	movq	_ZZN2v88internalL45Stats_Runtime_FunctionGetScriptSourcePositionEiPmPNS0_7IsolateEE27trace_event_unique_atomic58(%rip), %rbx
	testq	%rbx, %rbx
	je	.L47
.L18:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L48
.L20:
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L24
.L25:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L47:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L49
.L19:
	movq	%rbx, _ZZN2v88internalL45Stats_Runtime_FunctionGetScriptSourcePositionEiPmPNS0_7IsolateEE27trace_event_unique_atomic58(%rip)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L24:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L25
	movq	23(%rax), %rax
	leaq	-152(%rbp), %rdi
	movq	%rax, -152(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	leaq	-144(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	salq	$32, %r12
	testq	%rdi, %rdi
	jne	.L50
.L15:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L52
.L21:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L22
	movq	(%rdi), %rax
	call	*8(%rax)
.L22:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L23
	movq	(%rdi), %rax
	call	*8(%rax)
.L23:
	leaq	.LC1(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L46:
	movq	40960(%rsi), %rdi
	movl	$298, %edx
	leaq	-104(%rbp), %rsi
	addq	$23240, %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L50:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L49:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L52:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC1(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L21
.L51:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25202:
	.size	_ZN2v88internalL45Stats_Runtime_FunctionGetScriptSourcePositionEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL45Stats_Runtime_FunctionGetScriptSourcePositionEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL35Stats_Runtime_FunctionIsAPIFunctionEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"V8.Runtime_Runtime_FunctionIsAPIFunction"
	.section	.text._ZN2v88internalL35Stats_Runtime_FunctionIsAPIFunctionEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_FunctionIsAPIFunctionEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL35Stats_Runtime_FunctionIsAPIFunctionEiPmPNS0_7IsolateE.isra.0:
.LFB25203:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L86
.L54:
	movq	_ZZN2v88internalL35Stats_Runtime_FunctionIsAPIFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic68(%rip), %rbx
	testq	%rbx, %rbx
	je	.L87
.L56:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L88
.L58:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L62
.L63:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L87:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L89
.L57:
	movq	%rbx, _ZZN2v88internalL35Stats_Runtime_FunctionIsAPIFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic68(%rip)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L62:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L63
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L90
.L64:
	movq	120(%r12), %r12
.L65:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L91
.L53:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L92
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L93
.L59:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L60
	movq	(%rdi), %rax
	call	*8(%rax)
.L60:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L61
	movq	(%rdi), %rax
	call	*8(%rax)
.L61:
	leaq	.LC4(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L90:
	movq	-1(%rax), %rax
	cmpw	$88, 11(%rax)
	jne	.L64
	movq	112(%r12), %r12
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L86:
	movq	40960(%rsi), %rax
	movl	$300, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L91:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L89:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L93:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC4(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L59
.L92:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25203:
	.size	_ZN2v88internalL35Stats_Runtime_FunctionIsAPIFunctionEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL35Stats_Runtime_FunctionIsAPIFunctionEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL24Stats_Runtime_IsFunctionEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC5:
	.string	"V8.Runtime_Runtime_IsFunction"
	.section	.text._ZN2v88internalL24Stats_Runtime_IsFunctionEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL24Stats_Runtime_IsFunctionEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL24Stats_Runtime_IsFunctionEiPmPNS0_7IsolateE.isra.0:
.LFB25204:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L126
.L95:
	movq	_ZZN2v88internalL24Stats_Runtime_IsFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic92(%rip), %rbx
	testq	%rbx, %rbx
	je	.L127
.L97:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L128
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L103
.L105:
	movq	120(%r12), %r12
.L104:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L129
.L94:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L130
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L131
.L98:
	movq	%rbx, _ZZN2v88internalL24Stats_Runtime_IsFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic92(%rip)
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L128:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L132
.L100:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L101
	movq	(%rdi), %rax
	call	*8(%rax)
.L101:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L102
	movq	(%rdi), %rax
	call	*8(%rax)
.L102:
	leaq	.LC5(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	0(%r13), %rax
	movq	%r14, -120(%rbp)
	testb	$1, %al
	je	.L105
.L103:
	movq	-1(%rax), %rax
	cmpw	$1103, 11(%rax)
	jbe	.L105
	movq	112(%r12), %r12
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L126:
	movq	40960(%rsi), %rax
	movl	$301, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L129:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L132:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC5(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L131:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L98
.L130:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25204:
	.size	_ZN2v88internalL24Stats_Runtime_IsFunctionEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL24Stats_Runtime_IsFunctionEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL35Stats_Runtime_FunctionGetSourceCodeEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"V8.Runtime_Runtime_FunctionGetSourceCode"
	.section	.rodata._ZN2v88internalL35Stats_Runtime_FunctionGetSourceCodeEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC7:
	.string	"args[0].IsJSReceiver()"
	.section	.text._ZN2v88internalL35Stats_Runtime_FunctionGetSourceCodeEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_FunctionGetSourceCodeEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL35Stats_Runtime_FunctionGetSourceCodeEiPmPNS0_7IsolateE.isra.0:
.LFB25221:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L172
.L134:
	movq	_ZZN2v88internalL35Stats_Runtime_FunctionGetSourceCodeEiPmPNS0_7IsolateEE27trace_event_unique_atomic45(%rip), %rbx
	testq	%rbx, %rbx
	je	.L173
.L136:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L174
.L138:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L175
.L142:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L173:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L176
.L137:
	movq	%rbx, _ZZN2v88internalL35Stats_Runtime_FunctionGetSourceCodeEiPmPNS0_7IsolateEE27trace_event_unique_atomic45(%rip)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L175:
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L142
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	je	.L177
	movq	88(%r12), %r13
.L148:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L151
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L151:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L178
.L133:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L179
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L180
.L139:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L140
	movq	(%rdi), %rax
	call	*8(%rax)
.L140:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L141
	movq	(%rdi), %rax
	call	*8(%rax)
.L141:
	leaq	.LC6(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L177:
	movq	23(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L145
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L146:
	call	_ZN2v88internal18SharedFunctionInfo13GetSourceCodeENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r13
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L145:
	movq	%r14, %rdi
	cmpq	41096(%r12), %r14
	je	.L181
.L147:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdi)
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L172:
	movq	40960(%rsi), %rax
	movl	$299, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L178:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L176:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L180:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC6(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L181:
	movq	%r12, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L147
.L179:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25221:
	.size	_ZN2v88internalL35Stats_Runtime_FunctionGetSourceCodeEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL35Stats_Runtime_FunctionGetSourceCodeEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL37Stats_Runtime_FunctionGetScriptSourceEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"V8.Runtime_Runtime_FunctionGetScriptSource"
	.section	.text._ZN2v88internalL37Stats_Runtime_FunctionGetScriptSourceEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Stats_Runtime_FunctionGetScriptSourceEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL37Stats_Runtime_FunctionGetScriptSourceEiPmPNS0_7IsolateE.isra.0:
.LFB25222:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L223
.L183:
	movq	_ZZN2v88internalL37Stats_Runtime_FunctionGetScriptSourceEiPmPNS0_7IsolateEE27trace_event_unique_atomic17(%rip), %rbx
	testq	%rbx, %rbx
	je	.L224
.L185:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L225
.L187:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L226
.L191:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L224:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L227
.L186:
	movq	%rbx, _ZZN2v88internalL37Stats_Runtime_FunctionGetScriptSourceEiPmPNS0_7IsolateEE27trace_event_unique_atomic17(%rip)
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L226:
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L191
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	je	.L228
.L193:
	movq	88(%r12), %r13
.L200:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L203
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L203:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L229
.L182:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L230
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L231
.L188:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L189
	movq	(%rdi), %rax
	call	*8(%rax)
.L189:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L190
	movq	(%rdi), %rax
	call	*8(%rax)
.L190:
	leaq	.LC8(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L228:
	movq	23(%rax), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L232
.L194:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L195
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L196:
	testb	$1, %sil
	je	.L193
	movq	-1(%rsi), %rdx
	cmpw	$96, 11(%rdx)
	jne	.L193
	movq	(%rax), %rax
	movq	7(%rax), %r13
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L195:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L233
.L197:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L232:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L194
	movq	23(%rsi), %rsi
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L223:
	movq	40960(%rsi), %rax
	movl	$296, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L229:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L227:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L231:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC8(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L233:
	movq	%r12, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	jmp	.L197
.L230:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25222:
	.size	_ZN2v88internalL37Stats_Runtime_FunctionGetScriptSourceEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL37Stats_Runtime_FunctionGetScriptSourceEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL33Stats_Runtime_FunctionGetScriptIdEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"V8.Runtime_Runtime_FunctionGetScriptId"
	.section	.text._ZN2v88internalL33Stats_Runtime_FunctionGetScriptIdEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_FunctionGetScriptIdEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL33Stats_Runtime_FunctionGetScriptIdEiPmPNS0_7IsolateE.isra.0:
.LFB25223:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L274
.L235:
	movq	_ZZN2v88internalL33Stats_Runtime_FunctionGetScriptIdEiPmPNS0_7IsolateEE27trace_event_unique_atomic30(%rip), %rbx
	testq	%rbx, %rbx
	je	.L275
.L237:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L276
.L239:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L277
.L243:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L275:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L278
.L238:
	movq	%rbx, _ZZN2v88internalL33Stats_Runtime_FunctionGetScriptIdEiPmPNS0_7IsolateEE27trace_event_unique_atomic30(%rip)
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L277:
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L243
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	je	.L244
.L251:
	movabsq	$-4294967296, %r13
.L245:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L254
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L254:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L279
.L234:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L280
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L281
.L240:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L241
	movq	(%rdi), %rax
	call	*8(%rax)
.L241:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L242
	movq	(%rdi), %rax
	call	*8(%rax)
.L242:
	leaq	.LC9(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L244:
	movq	23(%rax), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L282
.L246:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L247
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L248:
	testb	$1, %sil
	je	.L251
	movq	-1(%rsi), %rdx
	cmpw	$96, 11(%rdx)
	jne	.L251
	movq	(%rax), %rax
	movslq	67(%rax), %r13
	salq	$32, %r13
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L274:
	movq	40960(%rsi), %rax
	movl	$297, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L279:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L247:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L283
.L249:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L278:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L281:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC9(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L282:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L246
	movq	23(%rsi), %rsi
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L283:
	movq	%r12, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	jmp	.L249
.L280:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25223:
	.size	_ZN2v88internalL33Stats_Runtime_FunctionGetScriptIdEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL33Stats_Runtime_FunctionGetScriptIdEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal31Runtime_FunctionGetScriptSourceEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Runtime_FunctionGetScriptSourceEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Runtime_FunctionGetScriptSourceEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Runtime_FunctionGetScriptSourceEiPmPNS0_7IsolateE:
.LFB20095:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L299
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L300
.L286:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L300:
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L286
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	je	.L301
.L288:
	movq	88(%r12), %r13
.L295:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L284
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L284:
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L301:
	.cfi_restore_state
	movq	23(%rax), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L302
.L289:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L290
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L291:
	testb	$1, %sil
	je	.L288
	movq	-1(%rsi), %rdx
	cmpw	$96, 11(%rdx)
	jne	.L288
	movq	(%rax), %rax
	movq	7(%rax), %r13
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L290:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L303
.L292:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L302:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L289
	movq	23(%rsi), %rsi
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L299:
	addq	$16, %rsp
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL37Stats_Runtime_FunctionGetScriptSourceEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L303:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L292
	.cfi_endproc
.LFE20095:
	.size	_ZN2v88internal31Runtime_FunctionGetScriptSourceEiPmPNS0_7IsolateE, .-_ZN2v88internal31Runtime_FunctionGetScriptSourceEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Runtime_FunctionGetScriptIdEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_FunctionGetScriptIdEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_FunctionGetScriptIdEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_FunctionGetScriptIdEiPmPNS0_7IsolateE:
.LFB20098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L318
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L319
.L306:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L319:
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L306
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	je	.L307
.L314:
	movabsq	$-4294967296, %r13
.L308:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L304
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L304:
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L307:
	.cfi_restore_state
	movq	23(%rax), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L320
.L309:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L310
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L311:
	testb	$1, %sil
	je	.L314
	movq	-1(%rsi), %rdx
	cmpw	$96, 11(%rdx)
	jne	.L314
	movq	(%rax), %rax
	movslq	67(%rax), %r13
	salq	$32, %r13
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L318:
	addq	$16, %rsp
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL33Stats_Runtime_FunctionGetScriptIdEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L310:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L321
.L312:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L320:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L309
	movq	23(%rsi), %rsi
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L321:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L312
	.cfi_endproc
.LFE20098:
	.size	_ZN2v88internal27Runtime_FunctionGetScriptIdEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_FunctionGetScriptIdEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Runtime_FunctionGetSourceCodeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_FunctionGetSourceCodeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_FunctionGetSourceCodeEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_FunctionGetSourceCodeEiPmPNS0_7IsolateE:
.LFB20101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L335
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L336
.L324:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L336:
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L324
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	je	.L337
	movq	88(%r12), %r14
.L330:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L322
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L322:
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L337:
	.cfi_restore_state
	movq	23(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L327
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L328:
	call	_ZN2v88internal18SharedFunctionInfo13GetSourceCodeENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r14
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L327:
	movq	%r13, %rdi
	cmpq	41096(%r12), %r13
	je	.L338
.L329:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdi)
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L335:
	addq	$16, %rsp
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL35Stats_Runtime_FunctionGetSourceCodeEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L338:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L329
	.cfi_endproc
.LFE20101:
	.size	_ZN2v88internal29Runtime_FunctionGetSourceCodeEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_FunctionGetSourceCodeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal39Runtime_FunctionGetScriptSourcePositionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal39Runtime_FunctionGetScriptSourcePositionEiPmPNS0_7IsolateE
	.type	_ZN2v88internal39Runtime_FunctionGetScriptSourcePositionEiPmPNS0_7IsolateE, @function
_ZN2v88internal39Runtime_FunctionGetScriptSourcePositionEiPmPNS0_7IsolateE:
.LFB20104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L346
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L342
.L343:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L342:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L343
	movq	23(%rax), %rax
	leaq	-16(%rbp), %rdi
	movq	%rax, -16(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	salq	$32, %rax
.L339:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L347
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L346:
	.cfi_restore_state
	movq	%rdx, %rsi
	call	_ZN2v88internalL45Stats_Runtime_FunctionGetScriptSourcePositionEiPmPNS0_7IsolateE.isra.0
	jmp	.L339
.L347:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20104:
	.size	_ZN2v88internal39Runtime_FunctionGetScriptSourcePositionEiPmPNS0_7IsolateE, .-_ZN2v88internal39Runtime_FunctionGetScriptSourcePositionEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Runtime_FunctionIsAPIFunctionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_FunctionIsAPIFunctionEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_FunctionIsAPIFunctionEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_FunctionIsAPIFunctionEiPmPNS0_7IsolateE:
.LFB20107:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	testl	%eax, %eax
	jne	.L356
	movq	(%rdi), %rax
	testb	$1, %al
	jne	.L350
.L351:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L350:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L351
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L357
.L352:
	movq	120(%rsi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L357:
	movq	-1(%rax), %rax
	cmpw	$88, 11(%rax)
	jne	.L352
	movq	112(%rsi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	jmp	_ZN2v88internalL35Stats_Runtime_FunctionIsAPIFunctionEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20107:
	.size	_ZN2v88internal29Runtime_FunctionIsAPIFunctionEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_FunctionIsAPIFunctionEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal18Runtime_IsFunctionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18Runtime_IsFunctionEiPmPNS0_7IsolateE
	.type	_ZN2v88internal18Runtime_IsFunctionEiPmPNS0_7IsolateE, @function
_ZN2v88internal18Runtime_IsFunctionEiPmPNS0_7IsolateE:
.LFB20114:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L363
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L360
.L362:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L360:
	movq	-1(%rax), %rax
	cmpw	$1103, 11(%rax)
	jbe	.L362
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L363:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL24Stats_Runtime_IsFunctionEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20114:
	.size	_ZN2v88internal18Runtime_IsFunctionEiPmPNS0_7IsolateE, .-_ZN2v88internal18Runtime_IsFunctionEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m.str1.1,"aMS",@progbits,1
.LC10:
	.string	"NewArray"
	.section	.text._ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m,"axG",@progbits,_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m,comdat
	.p2align 4
	.weak	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m
	.type	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m, @function
_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m:
.LFB23469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZSt7nothrow(%rip), %rsi
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdi
	movq	$-1, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	0(,%rdi,8), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmova	%rax, %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L366
	movq	%rbx, %rsi
	subq	$1, %rsi
	js	.L364
	leaq	-2(%rbx), %rdx
	movl	$1, %edi
	cmpq	$-1, %rdx
	cmovge	%rbx, %rdi
	cmpq	$1, %rbx
	je	.L379
	cmpq	$-1, %rdx
	jl	.L379
	movq	%rdi, %rsi
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	shrq	%rsi
	.p2align 4,,10
	.p2align 3
.L370:
	movq	%rdx, %rcx
	addq	$1, %rdx
	salq	$4, %rcx
	movups	%xmm0, (%rax,%rcx)
	cmpq	%rdx, %rsi
	jne	.L370
	movq	%rdi, %rcx
	andq	$-2, %rcx
	leaq	(%rax,%rcx,8), %rdx
	cmpq	%rcx, %rdi
	je	.L364
.L368:
	movq	$0, (%rdx)
.L364:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L379:
	.cfi_restore_state
	movq	%rax, %rdx
	jmp	.L368
.L366:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L383
	movq	%rbx, %rdi
	subq	$1, %rdi
	js	.L364
	leaq	-2(%rbx), %rcx
	movl	$1, %edx
	addq	$1, %rcx
	cmovge	%rbx, %rdx
	jl	.L380
	subq	$1, %rbx
	je	.L380
	movq	%rdx, %rsi
	xorl	%ecx, %ecx
	pxor	%xmm0, %xmm0
	shrq	%rsi
.L374:
	movq	%rcx, %rdi
	addq	$1, %rcx
	salq	$4, %rdi
	movups	%xmm0, (%rax,%rdi)
	cmpq	%rcx, %rsi
	jne	.L374
	movq	%rdx, %rsi
	andq	$-2, %rsi
	leaq	(%rax,%rsi,8), %rcx
	cmpq	%rsi, %rdx
	je	.L364
.L372:
	movq	$0, (%rcx)
	jmp	.L364
.L383:
	leaq	.LC10(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L380:
	movq	%rax, %rcx
	jmp	.L372
	.cfi_endproc
.LFE23469:
	.size	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m, .-_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m
	.section	.rodata._ZN2v88internalL18Stats_Runtime_CallEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"V8.Runtime_Runtime_Call"
	.section	.text._ZN2v88internalL18Stats_Runtime_CallEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL18Stats_Runtime_CallEiPmPNS0_7IsolateE, @function
_ZN2v88internalL18Stats_Runtime_CallEiPmPNS0_7IsolateE:
.LFB20109:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L427
.L385:
	movq	_ZZN2v88internalL18Stats_Runtime_CallEiPmPNS0_7IsolateEE27trace_event_unique_atomic77(%rip), %rbx
	testq	%rbx, %rbx
	je	.L428
.L387:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L429
.L389:
	leal	-2(%r15), %ecx
	movq	41088(%r12), %rax
	leaq	-8(%r14), %r13
	addl	$1, 41104(%r12)
	movslq	%ecx, %rdi
	movl	%ecx, -168(%rbp)
	movq	41096(%r12), %rbx
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m
	movl	-168(%rbp), %ecx
	movq	%rax, %r8
	testl	%ecx, %ecx
	jle	.L393
	subl	$3, %r15d
	cmpl	$2, %r15d
	jbe	.L406
	movl	%ecx, %edx
	movq	%r14, %xmm3
	movdqa	.LC11(%rip), %xmm1
	movdqa	.LC13(%rip), %xmm7
	shrl	$2, %edx
	movdqa	.LC14(%rip), %xmm6
	punpcklqdq	%xmm3, %xmm3
	pxor	%xmm5, %xmm5
	salq	$5, %rdx
	addq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L395:
	movdqa	%xmm1, %xmm0
	movdqa	%xmm5, %xmm2
	paddd	%xmm7, %xmm1
	addq	$32, %rax
	paddd	%xmm6, %xmm0
	movdqa	%xmm3, %xmm8
	pslld	$3, %xmm0
	pcmpgtd	%xmm0, %xmm2
	movdqa	%xmm0, %xmm4
	punpckldq	%xmm2, %xmm4
	punpckhdq	%xmm2, %xmm0
	movdqa	%xmm3, %xmm2
	psubq	%xmm4, %xmm8
	psubq	%xmm0, %xmm2
	movups	%xmm8, -32(%rax)
	movups	%xmm2, -16(%rax)
	cmpq	%rax, %rdx
	jne	.L395
	movl	%ecx, %eax
	andl	$-4, %eax
	testb	$3, %cl
	je	.L393
.L394:
	leal	2(%rax), %edx
	movq	%r14, %r11
	movslq	%eax, %rdi
	addl	$1, %eax
	leal	0(,%rdx,8), %esi
	movslq	%esi, %r10
	subq	%r10, %r11
	movq	%r11, (%r8,%rdi,8)
	cmpl	%eax, %ecx
	jle	.L393
	leal	8(%rsi), %edi
	movq	%r14, %r9
	cltq
	movslq	%edi, %rdi
	subq	%rdi, %r9
	movq	%r9, (%r8,%rax,8)
	cmpl	%edx, %ecx
	jle	.L393
	leal	16(%rsi), %eax
	movq	%r14, %rsi
	movslq	%edx, %rdx
	cltq
	subq	%rax, %rsi
	movq	%rsi, (%r8,%rdx,8)
.L393:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	movq	-168(%rbp), %r8
	testq	%rax, %rax
	je	.L430
	movq	(%rax), %r14
.L398:
	testq	%r8, %r8
	je	.L399
	movq	%r8, %rdi
	call	_ZdaPv@PLT
.L399:
	subl	$1, 41104(%r12)
	movq	-176(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L402
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L402:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L431
.L384:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L432
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L429:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L433
.L390:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L391
	movq	(%rdi), %rax
	call	*8(%rax)
.L391:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L392
	movq	(%rdi), %rax
	call	*8(%rax)
.L392:
	leaq	.LC12(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L428:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L434
.L388:
	movq	%rbx, _ZZN2v88internalL18Stats_Runtime_CallEiPmPNS0_7IsolateEE27trace_event_unique_atomic77(%rip)
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L430:
	movq	312(%r12), %r14
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L431:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L427:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$295, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L385
.L406:
	xorl	%eax, %eax
	jmp	.L394
.L434:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L388
.L433:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC12(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L390
.L432:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20109:
	.size	_ZN2v88internalL18Stats_Runtime_CallEiPmPNS0_7IsolateE, .-_ZN2v88internalL18Stats_Runtime_CallEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal12Runtime_CallEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal12Runtime_CallEiPmPNS0_7IsolateE
	.type	_ZN2v88internal12Runtime_CallEiPmPNS0_7IsolateE, @function
_ZN2v88internal12Runtime_CallEiPmPNS0_7IsolateE:
.LFB20110:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L452
	addl	$1, 41104(%rdx)
	leal	-2(%rdi), %ecx
	movq	41088(%rdx), %rax
	leaq	-8(%rsi), %r13
	movl	%edi, -56(%rbp)
	movslq	%ecx, %rdi
	movq	41096(%rdx), %rbx
	movl	%ecx, -52(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m
	movl	-52(%rbp), %ecx
	movq	%rax, %r14
	testl	%ecx, %ecx
	jle	.L437
	movl	-56(%rbp), %r10d
	subl	$3, %r10d
	cmpl	$2, %r10d
	jbe	.L446
	movl	%ecx, %edx
	movq	%r15, %xmm3
	movdqa	.LC11(%rip), %xmm1
	movdqa	.LC13(%rip), %xmm7
	shrl	$2, %edx
	movdqa	.LC14(%rip), %xmm6
	punpcklqdq	%xmm3, %xmm3
	pxor	%xmm5, %xmm5
	salq	$5, %rdx
	addq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L439:
	movdqa	%xmm1, %xmm0
	movdqa	%xmm5, %xmm2
	paddd	%xmm7, %xmm1
	addq	$32, %rax
	paddd	%xmm6, %xmm0
	movdqa	%xmm3, %xmm8
	pslld	$3, %xmm0
	pcmpgtd	%xmm0, %xmm2
	movdqa	%xmm0, %xmm4
	punpckldq	%xmm2, %xmm4
	punpckhdq	%xmm2, %xmm0
	movdqa	%xmm3, %xmm2
	psubq	%xmm4, %xmm8
	psubq	%xmm0, %xmm2
	movups	%xmm8, -32(%rax)
	movups	%xmm2, -16(%rax)
	cmpq	%rdx, %rax
	jne	.L439
	movl	%ecx, %eax
	andl	$-4, %eax
	testb	$3, %cl
	je	.L437
.L438:
	leal	2(%rax), %esi
	movq	%r15, %r9
	movslq	%eax, %rdi
	addl	$1, %eax
	leal	0(,%rsi,8), %edx
	movslq	%edx, %r8
	subq	%r8, %r9
	movq	%r9, (%r14,%rdi,8)
	cmpl	%eax, %ecx
	jle	.L437
	leal	8(%rdx), %edi
	movq	%r15, %r11
	cltq
	movslq	%edi, %rdi
	subq	%rdi, %r11
	movq	%r11, (%r14,%rax,8)
	cmpl	%esi, %ecx
	jle	.L437
	addl	$16, %edx
	movq	%r15, %rax
	movslq	%esi, %rsi
	movslq	%edx, %rdx
	subq	%rdx, %rax
	movq	%rax, (%r14,%rsi,8)
.L437:
	movq	%r14, %r8
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	testq	%rax, %rax
	je	.L453
	movq	(%rax), %r15
.L442:
	testq	%r14, %r14
	je	.L443
	movq	%r14, %rdi
	call	_ZdaPv@PLT
.L443:
	subl	$1, 41104(%r12)
	movq	-64(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L435
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L435:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L453:
	.cfi_restore_state
	movq	312(%r12), %r15
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L452:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL18Stats_Runtime_CallEiPmPNS0_7IsolateE
.L446:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L438
	.cfi_endproc
.LFE20110:
	.size	_ZN2v88internal12Runtime_CallEiPmPNS0_7IsolateE, .-_ZN2v88internal12Runtime_CallEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal31Runtime_FunctionGetScriptSourceEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal31Runtime_FunctionGetScriptSourceEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal31Runtime_FunctionGetScriptSourceEiPmPNS0_7IsolateE:
.LFB25194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25194:
	.size	_GLOBAL__sub_I__ZN2v88internal31Runtime_FunctionGetScriptSourceEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal31Runtime_FunctionGetScriptSourceEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal31Runtime_FunctionGetScriptSourceEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL24Stats_Runtime_IsFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic92,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL24Stats_Runtime_IsFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic92, @object
	.size	_ZZN2v88internalL24Stats_Runtime_IsFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic92, 8
_ZZN2v88internalL24Stats_Runtime_IsFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic92:
	.zero	8
	.section	.bss._ZZN2v88internalL18Stats_Runtime_CallEiPmPNS0_7IsolateEE27trace_event_unique_atomic77,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL18Stats_Runtime_CallEiPmPNS0_7IsolateEE27trace_event_unique_atomic77, @object
	.size	_ZZN2v88internalL18Stats_Runtime_CallEiPmPNS0_7IsolateEE27trace_event_unique_atomic77, 8
_ZZN2v88internalL18Stats_Runtime_CallEiPmPNS0_7IsolateEE27trace_event_unique_atomic77:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_FunctionIsAPIFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic68,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_FunctionIsAPIFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic68, @object
	.size	_ZZN2v88internalL35Stats_Runtime_FunctionIsAPIFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic68, 8
_ZZN2v88internalL35Stats_Runtime_FunctionIsAPIFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic68:
	.zero	8
	.section	.bss._ZZN2v88internalL45Stats_Runtime_FunctionGetScriptSourcePositionEiPmPNS0_7IsolateEE27trace_event_unique_atomic58,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL45Stats_Runtime_FunctionGetScriptSourcePositionEiPmPNS0_7IsolateEE27trace_event_unique_atomic58, @object
	.size	_ZZN2v88internalL45Stats_Runtime_FunctionGetScriptSourcePositionEiPmPNS0_7IsolateEE27trace_event_unique_atomic58, 8
_ZZN2v88internalL45Stats_Runtime_FunctionGetScriptSourcePositionEiPmPNS0_7IsolateEE27trace_event_unique_atomic58:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_FunctionGetSourceCodeEiPmPNS0_7IsolateEE27trace_event_unique_atomic45,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_FunctionGetSourceCodeEiPmPNS0_7IsolateEE27trace_event_unique_atomic45, @object
	.size	_ZZN2v88internalL35Stats_Runtime_FunctionGetSourceCodeEiPmPNS0_7IsolateEE27trace_event_unique_atomic45, 8
_ZZN2v88internalL35Stats_Runtime_FunctionGetSourceCodeEiPmPNS0_7IsolateEE27trace_event_unique_atomic45:
	.zero	8
	.section	.bss._ZZN2v88internalL33Stats_Runtime_FunctionGetScriptIdEiPmPNS0_7IsolateEE27trace_event_unique_atomic30,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_FunctionGetScriptIdEiPmPNS0_7IsolateEE27trace_event_unique_atomic30, @object
	.size	_ZZN2v88internalL33Stats_Runtime_FunctionGetScriptIdEiPmPNS0_7IsolateEE27trace_event_unique_atomic30, 8
_ZZN2v88internalL33Stats_Runtime_FunctionGetScriptIdEiPmPNS0_7IsolateEE27trace_event_unique_atomic30:
	.zero	8
	.section	.bss._ZZN2v88internalL37Stats_Runtime_FunctionGetScriptSourceEiPmPNS0_7IsolateEE27trace_event_unique_atomic17,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Stats_Runtime_FunctionGetScriptSourceEiPmPNS0_7IsolateEE27trace_event_unique_atomic17, @object
	.size	_ZZN2v88internalL37Stats_Runtime_FunctionGetScriptSourceEiPmPNS0_7IsolateEE27trace_event_unique_atomic17, 8
_ZZN2v88internalL37Stats_Runtime_FunctionGetScriptSourceEiPmPNS0_7IsolateEE27trace_event_unique_atomic17:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC11:
	.long	0
	.long	1
	.long	2
	.long	3
	.align 16
.LC13:
	.long	4
	.long	4
	.long	4
	.long	4
	.align 16
.LC14:
	.long	2
	.long	2
	.long	2
	.long	2
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
