	.file	"date.cc"
	.text
	.section	.text._ZN2v88internal9DateCacheD2Ev,"axG",@progbits,_ZN2v88internal9DateCacheD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9DateCacheD2Ev
	.type	_ZN2v88internal9DateCacheD2Ev, @function
_ZN2v88internal9DateCacheD2Ev:
.LFB5455:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal9DateCacheE(%rip), %rax
	movq	%rax, (%rdi)
	movq	592(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L1:
	ret
	.cfi_endproc
.LFE5455:
	.size	_ZN2v88internal9DateCacheD2Ev, .-_ZN2v88internal9DateCacheD2Ev
	.weak	_ZN2v88internal9DateCacheD1Ev
	.set	_ZN2v88internal9DateCacheD1Ev,_ZN2v88internal9DateCacheD2Ev
	.section	.text._ZN2v88internal9DateCache30GetDaylightSavingsOffsetFromOSEl,"axG",@progbits,_ZN2v88internal9DateCache30GetDaylightSavingsOffsetFromOSEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9DateCache30GetDaylightSavingsOffsetFromOSEl
	.type	_ZN2v88internal9DateCache30GetDaylightSavingsOffsetFromOSEl, @function
_ZN2v88internal9DateCache30GetDaylightSavingsOffsetFromOSEl:
.LFB5471:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	592(%rdi), %rdi
	imulq	$1000, %rsi, %rsi
	pxor	%xmm0, %xmm0
	movq	(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cvtsi2sdq	%rsi, %xmm0
	call	*8(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	cvttsd2sil	%xmm0, %eax
	ret
	.cfi_endproc
.LFE5471:
	.size	_ZN2v88internal9DateCache30GetDaylightSavingsOffsetFromOSEl, .-_ZN2v88internal9DateCache30GetDaylightSavingsOffsetFromOSEl
	.section	.text._ZN2v88internal9DateCacheD0Ev,"axG",@progbits,_ZN2v88internal9DateCacheD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9DateCacheD0Ev
	.type	_ZN2v88internal9DateCacheD0Ev, @function
_ZN2v88internal9DateCacheD0Ev:
.LFB5457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9DateCacheE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	592(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L7
	movq	(%rdi), %rax
	call	*40(%rax)
.L7:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$600, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5457:
	.size	_ZN2v88internal9DateCacheD0Ev, .-_ZN2v88internal9DateCacheD0Ev
	.section	.text._ZN2v88internal9DateCacheC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9DateCacheC2Ev
	.type	_ZN2v88internal9DateCacheC2Ev, @function
_ZN2v88internal9DateCacheC2Ev:
.LFB18124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9DateCacheE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	$0, 8(%rdi)
	call	_ZN2v88internal4Intl19CreateTimeZoneCacheEv@PLT
	movq	%rax, 592(%rbx)
	movq	%rax, %rdi
	movslq	12(%rbx), %rax
	cmpq	$2147483647, %rax
	je	.L19
	addl	$1, %eax
	salq	$32, %rax
	movq	%rax, 8(%rbx)
.L14:
	leaq	16(%rbx), %rax
	movdqa	.LC0(%rip), %xmm0
	leaq	528(%rbx), %rcx
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L15:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L15
	leaq	32(%rbx), %rdx
	movq	%rax, %xmm0
	cmpb	$0, _ZN2v88internal22FLAG_icu_timezone_dataE(%rip)
	movl	$0, 528(%rbx)
	movq	%rdx, %xmm1
	movb	$0, 556(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 536(%rbx)
	jne	.L16
	movl	$2147483647, 552(%rbx)
.L16:
	movq	(%rdi), %rax
	xorl	%esi, %esi
	call	*24(%rax)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 576(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movq	$0, 8(%rbx)
	jmp	.L14
	.cfi_endproc
.LFE18124:
	.size	_ZN2v88internal9DateCacheC2Ev, .-_ZN2v88internal9DateCacheC2Ev
	.globl	_ZN2v88internal9DateCacheC1Ev
	.set	_ZN2v88internal9DateCacheC1Ev,_ZN2v88internal9DateCacheC2Ev
	.section	.text._ZN2v88internal9DateCache14ResetDateCacheENS_4base13TimezoneCache17TimeZoneDetectionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9DateCache14ResetDateCacheENS_4base13TimezoneCache17TimeZoneDetectionE
	.type	_ZN2v88internal9DateCache14ResetDateCacheENS_4base13TimezoneCache17TimeZoneDetectionE, @function
_ZN2v88internal9DateCache14ResetDateCacheENS_4base13TimezoneCache17TimeZoneDetectionE:
.LFB18126:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	12(%rdi), %rax
	cmpq	$2147483647, %rax
	je	.L27
	addl	$1, %eax
	salq	$32, %rax
	movq	%rax, 8(%rdi)
.L23:
	leaq	16(%rbx), %rcx
	movdqa	.LC0(%rip), %xmm0
	leaq	528(%rbx), %rdx
	movq	%rcx, %rax
	.p2align 4,,10
	.p2align 3
.L22:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L22
	leaq	32(%rbx), %rax
	movq	%rcx, %xmm0
	cmpb	$0, _ZN2v88internal22FLAG_icu_timezone_dataE(%rip)
	movl	$0, 528(%rbx)
	movq	%rax, %xmm1
	movb	$0, 556(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 536(%rbx)
	jne	.L24
	movl	$2147483647, 552(%rbx)
.L24:
	movq	592(%rbx), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 576(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movq	$0, 8(%rdi)
	jmp	.L23
	.cfi_endproc
.LFE18126:
	.size	_ZN2v88internal9DateCache14ResetDateCacheENS_4base13TimezoneCache17TimeZoneDetectionE, .-_ZN2v88internal9DateCache14ResetDateCacheENS_4base13TimezoneCache17TimeZoneDetectionE
	.section	.text._ZN2v88internal9DateCache8TimeClipEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9DateCache8TimeClipEd
	.type	_ZN2v88internal9DateCache8TimeClipEd, @function
_ZN2v88internal9DateCache8TimeClipEd:
.LFB18127:
	.cfi_startproc
	endbr64
	comisd	.LC3(%rip), %xmm0
	jb	.L50
	movsd	.LC4(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L50
	ucomisd	%xmm0, %xmm0
	jp	.L43
	movq	.LC5(%rip), %xmm2
	movsd	.LC6(%rip), %xmm3
	movapd	%xmm0, %xmm1
	andpd	%xmm2, %xmm1
	ucomisd	%xmm1, %xmm3
	pxor	%xmm1, %xmm1
	jb	.L35
	ucomisd	%xmm1, %xmm0
	jnp	.L53
.L46:
	comisd	%xmm1, %xmm0
	movapd	%xmm0, %xmm4
	movsd	.LC7(%rip), %xmm5
	movapd	%xmm0, %xmm3
	andpd	%xmm2, %xmm4
	jb	.L52
	ucomisd	%xmm4, %xmm5
	ja	.L54
.L40:
	addsd	%xmm1, %xmm3
	movapd	%xmm3, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	movsd	.LC2(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	jne	.L46
.L35:
	addsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm4, %xmm4
	movsd	.LC8(%rip), %xmm5
	andnpd	%xmm0, %xmm2
	cvtsi2sdq	%rax, %xmm4
	movapd	%xmm4, %xmm6
	cmpnlesd	%xmm0, %xmm6
	movapd	%xmm6, %xmm3
	andpd	%xmm5, %xmm3
	subsd	%xmm3, %xmm4
	movapd	%xmm4, %xmm3
	orpd	%xmm2, %xmm3
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L52:
	ucomisd	%xmm4, %xmm5
	jbe	.L40
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm4, %xmm4
	movsd	.LC8(%rip), %xmm5
	andnpd	%xmm0, %xmm2
	cvtsi2sdq	%rax, %xmm4
	cmpnlesd	%xmm4, %xmm3
	andpd	%xmm5, %xmm3
	addsd	%xmm4, %xmm3
	orpd	%xmm2, %xmm3
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L43:
	pxor	%xmm0, %xmm0
	ret
	.cfi_endproc
.LFE18127:
	.size	_ZN2v88internal9DateCache8TimeClipEd, .-_ZN2v88internal9DateCache8TimeClipEd
	.section	.text._ZN2v88internal9DateCache20YearMonthDayFromDaysEiPiS2_S2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9DateCache20YearMonthDayFromDaysEiPiS2_S2_
	.type	_ZN2v88internal9DateCache20YearMonthDayFromDaysEiPiS2_S2_, @function
_ZN2v88internal9DateCache20YearMonthDayFromDaysEiPiS2_S2_:
.LFB18129:
	.cfi_startproc
	endbr64
	cmpb	$0, 556(%rdi)
	je	.L56
	movl	%esi, %eax
	subl	560(%rdi), %eax
	addl	572(%rdi), %eax
	leal	-1(%rax), %r9d
	cmpl	$27, %r9d
	jbe	.L85
.L56:
	leal	146816528(%rsi), %r9d
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%r9d, %r11
	imulq	$963315389, %r11, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	sarq	$47, %r11
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%r9d, %ebx
	sarl	$31, %ebx
	movl	%r11d, %eax
	subl	%ebx, %eax
	subl	%ebx, %r11d
	imull	$146097, %eax, %eax
	subl	%eax, %r9d
	movl	%r9d, %eax
	subl	$1, %eax
	movslq	%eax, %r9
	movl	%eax, %r10d
	imulq	$-441679365, %r9, %r9
	sarl	$31, %r10d
	shrq	$32, %r9
	addl	%eax, %r9d
	sarl	$15, %r9d
	subl	%r10d, %r9d
	imull	$36524, %r9d, %r10d
	subl	%r10d, %eax
	addl	$1, %eax
	movslq	%eax, %r12
	movl	%eax, %r10d
	imulq	$376287347, %r12, %r12
	sarl	$31, %r10d
	sarq	$39, %r12
	subl	%r10d, %r12d
	imull	$1461, %r12d, %r10d
	subl	%r10d, %eax
	subl	$1, %eax
	movslq	%eax, %r10
	movl	%eax, %r13d
	imulq	$-1282606671, %r10, %r10
	sarl	$31, %r13d
	shrq	$32, %r10
	addl	%eax, %r10d
	sarl	$8, %r10d
	subl	%r13d, %r10d
	imull	$365, %r10d, %r13d
	subl	%r13d, %eax
	imull	$400, %r11d, %r11d
	imull	$100, %r9d, %ebx
	leal	-400000(%r11,%rbx), %r11d
	leal	(%r11,%r12,4), %r11d
	addl	%r10d, %r11d
	movl	%r11d, (%rdx)
	testl	%r9d, %r9d
	je	.L77
	testl	%r12d, %r12d
	jne	.L77
.L58:
	cmpl	$58, %eax
	jle	.L62
	movl	$59, %r9d
.L61:
	subl	%r9d, %eax
	cmpl	$30, %eax
	jle	.L68
	leal	-31(%rax), %r9d
	cmpl	$29, %r9d
	jle	.L69
	leal	-61(%rax), %r9d
	cmpl	$30, %r9d
	jle	.L70
	leal	-92(%rax), %r9d
	cmpl	$29, %r9d
	jle	.L71
	leal	-122(%rax), %r9d
	cmpl	$30, %r9d
	jle	.L72
	leal	-153(%rax), %r9d
	cmpl	$30, %r9d
	jle	.L73
	leal	-184(%rax), %r9d
	cmpl	$29, %r9d
	jle	.L74
	leal	-214(%rax), %r9d
	cmpl	$30, %r9d
	jle	.L75
	leal	-245(%rax), %r9d
	cmpl	$29, %r9d
	jle	.L76
	subl	$275, %eax
	movl	$11, %r9d
	cmpl	$31, %eax
	je	.L64
	.p2align 4,,10
	.p2align 3
.L63:
	addl	$1, %eax
	movl	%r9d, (%rcx)
	movl	%eax, (%r8)
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L77:
	testl	%r10d, %r10d
	jne	.L58
	addl	$1, %eax
	cmpl	$59, %eax
	jg	.L66
.L62:
	cmpl	$30, %eax
	jg	.L65
	addl	$1, %eax
	movl	$0, (%rcx)
	movl	%eax, (%r8)
.L64:
	movb	$1, 556(%rdi)
	movl	(%rdx), %eax
	popq	%rbx
	popq	%r12
	movl	%eax, 564(%rdi)
	movl	(%rcx), %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%eax, 568(%rdi)
	movl	(%r8), %eax
	movl	%esi, 560(%rdi)
	movl	%eax, 572(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	subl	$30, %eax
	movl	$1, (%rcx)
	movl	%eax, (%r8)
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	%esi, 560(%rdi)
	movl	564(%rdi), %esi
	movl	%eax, 572(%rdi)
	movl	%esi, (%rdx)
	movl	568(%rdi), %edx
	movl	%edx, (%rcx)
	movl	%eax, (%r8)
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$60, %r9d
	jmp	.L61
.L68:
	movl	$2, %r9d
	jmp	.L63
.L69:
	movl	%r9d, %eax
	movl	$3, %r9d
	jmp	.L63
.L70:
	movl	%r9d, %eax
	movl	$4, %r9d
	jmp	.L63
.L71:
	movl	%r9d, %eax
	movl	$5, %r9d
	jmp	.L63
.L72:
	movl	%r9d, %eax
	movl	$6, %r9d
	jmp	.L63
.L73:
	movl	%r9d, %eax
	movl	$7, %r9d
	jmp	.L63
.L74:
	movl	%r9d, %eax
	movl	$8, %r9d
	jmp	.L63
.L75:
	movl	%r9d, %eax
	movl	$9, %r9d
	jmp	.L63
.L76:
	movl	%r9d, %eax
	movl	$10, %r9d
	jmp	.L63
	.cfi_endproc
.LFE18129:
	.size	_ZN2v88internal9DateCache20YearMonthDayFromDaysEiPiS2_S2_, .-_ZN2v88internal9DateCache20YearMonthDayFromDaysEiPiS2_S2_
	.section	.text._ZN2v88internal9DateCache17DaysFromYearMonthEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9DateCache17DaysFromYearMonthEii
	.type	_ZN2v88internal9DateCache17DaysFromYearMonthEii, @function
_ZN2v88internal9DateCache17DaysFromYearMonthEii:
.LFB18130:
	.cfi_startproc
	endbr64
	movslq	%edx, %rax
	movl	%edx, %ecx
	imulq	$715827883, %rax, %rax
	sarl	$31, %ecx
	sarq	$33, %rax
	subl	%ecx, %eax
	addl	%eax, %esi
	leal	(%rax,%rax,2), %eax
	sall	$2, %eax
	subl	%eax, %edx
	jns	.L87
	subl	$1, %esi
	addl	$12, %edx
.L87:
	leal	399999(%rsi), %ecx
	leal	400002(%rsi), %edi
	imull	$365, %ecx, %eax
	testl	%ecx, %ecx
	cmovns	%ecx, %edi
	sarl	$2, %edi
	addl	%eax, %edi
	movslq	%ecx, %rax
	sarl	$31, %ecx
	imulq	$1374389535, %rax, %rax
	movl	%ecx, %r10d
	movq	%rax, %r9
	sarq	$39, %rax
	sarq	$37, %r9
	subl	%ecx, %eax
	subl	%r9d, %r10d
	addl	%r10d, %edi
	leal	-146816162(%rdi,%rax), %eax
	testb	$3, %sil
	jne	.L88
	imull	$-1030792151, %esi, %esi
	addl	$85899344, %esi
	movl	%esi, %ecx
	rorl	$2, %ecx
	cmpl	$42949672, %ecx
	ja	.L89
	rorl	$4, %esi
	cmpl	$10737418, %esi
	ja	.L88
.L89:
	movslq	%edx, %rdx
	leaq	_ZZN2v88internal9DateCache17DaysFromYearMonthEiiE19day_from_month_leap(%rip), %rcx
	addl	(%rcx,%rdx,4), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	movslq	%edx, %rdx
	leaq	_ZZN2v88internal9DateCache17DaysFromYearMonthEiiE14day_from_month(%rip), %rcx
	addl	(%rcx,%rdx,4), %eax
	ret
	.cfi_endproc
.LFE18130:
	.size	_ZN2v88internal9DateCache17DaysFromYearMonthEii, .-_ZN2v88internal9DateCache17DaysFromYearMonthEii
	.section	.text._ZN2v88internal9DateCache13BreakDownTimeElPiS2_S2_S2_S2_S2_S2_S2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9DateCache13BreakDownTimeElPiS2_S2_S2_S2_S2_S2_S2_
	.type	_ZN2v88internal9DateCache13BreakDownTimeElPiS2_S2_S2_S2_S2_S2_S2_, @function
_ZN2v88internal9DateCache13BreakDownTimeElPiS2_S2_S2_S2_S2_S2_S2_:
.LFB18131:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	leaq	-86399999(%rsi), %r10
	movq	%rdx, %r11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movabsq	$7164004856975580295, %rdx
	cmovns	%rsi, %r10
	movq	%r10, %rax
	sarq	$63, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	imulq	%rdx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	sarq	$25, %rdx
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	subq	%r10, %rdx
	imull	$-86400000, %edx, %eax
	addl	%eax, %esi
	cmpb	$0, 556(%rdi)
	je	.L93
	movl	%edx, %eax
	subl	560(%rdi), %eax
	addl	572(%rdi), %eax
	leal	-1(%rax), %r10d
	cmpl	$27, %r10d
	jbe	.L120
.L93:
	leal	146816528(%rdx), %r10d
	movslq	%r10d, %r12
	movl	%r10d, %r13d
	imulq	$963315389, %r12, %r12
	sarl	$31, %r13d
	sarq	$47, %r12
	movl	%r12d, %eax
	subl	%r13d, %r12d
	subl	%r13d, %eax
	imull	$146097, %eax, %eax
	subl	%eax, %r10d
	movl	%r10d, %eax
	subl	$1, %eax
	movslq	%eax, %rbx
	movl	%eax, %r10d
	imulq	$-441679365, %rbx, %rbx
	sarl	$31, %r10d
	shrq	$32, %rbx
	addl	%eax, %ebx
	sarl	$15, %ebx
	subl	%r10d, %ebx
	imull	$36524, %ebx, %r10d
	subl	%r10d, %eax
	addl	$1, %eax
	movslq	%eax, %r14
	movl	%eax, %r10d
	imulq	$376287347, %r14, %r14
	sarl	$31, %r10d
	sarq	$39, %r14
	subl	%r10d, %r14d
	imull	$1461, %r14d, %r10d
	subl	%r10d, %eax
	subl	$1, %eax
	movslq	%eax, %r10
	movl	%eax, %r15d
	imulq	$-1282606671, %r10, %r10
	sarl	$31, %r15d
	shrq	$32, %r10
	addl	%eax, %r10d
	sarl	$8, %r10d
	subl	%r15d, %r10d
	imull	$365, %r10d, %r15d
	subl	%r15d, %eax
	imull	$400, %r12d, %r12d
	imull	$100, %ebx, %r13d
	leal	-400000(%r12,%r13), %r12d
	leal	(%r12,%r14,4), %r12d
	addl	%r10d, %r12d
	movl	%r12d, (%r11)
	testl	%ebx, %ebx
	je	.L95
	testl	%r14d, %r14d
	jne	.L95
.L98:
	movl	$59, %r10d
	cmpl	$58, %eax
	jle	.L97
.L96:
	subl	%r10d, %eax
	cmpl	$30, %eax
	jle	.L106
	leal	-31(%rax), %r10d
	cmpl	$29, %r10d
	jle	.L107
	leal	-61(%rax), %r10d
	cmpl	$30, %r10d
	jle	.L108
	leal	-92(%rax), %r10d
	cmpl	$29, %r10d
	jle	.L109
	leal	-122(%rax), %r10d
	cmpl	$30, %r10d
	jle	.L110
	leal	-153(%rax), %r10d
	cmpl	$30, %r10d
	jle	.L111
	leal	-184(%rax), %r10d
	cmpl	$29, %r10d
	jle	.L112
	leal	-214(%rax), %r10d
	cmpl	$30, %r10d
	jle	.L113
	leal	-245(%rax), %r10d
	cmpl	$29, %r10d
	jle	.L114
	subl	$275, %eax
	movl	$11, %r10d
	cmpl	$31, %eax
	jne	.L99
.L100:
	movb	$1, 556(%rdi)
	movl	(%r11), %eax
	movl	%eax, 564(%rdi)
	movl	(%rcx), %eax
	movl	%eax, 568(%rdi)
	movl	(%r8), %eax
	movl	%edx, 560(%rdi)
	movl	%eax, 572(%rdi)
.L94:
	addl	$4, %edx
	movl	%esi, %edi
	movslq	%edx, %rax
	movl	%edx, %ecx
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %ecx
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$2, %eax
	subl	%ecx, %eax
	leal	0(,%rax,8), %ecx
	subl	%eax, %ecx
	subl	%ecx, %edx
	movq	16(%rbp), %rcx
	movl	%edx, %eax
	leal	7(%rdx), %edx
	cmovs	%edx, %eax
	sarl	$31, %edi
	movl	%eax, (%r9)
	movslq	%esi, %rax
	imulq	$1250999897, %rax, %rdx
	sarq	$52, %rdx
	subl	%edi, %edx
	movl	%edx, (%rcx)
	imulq	$1172812403, %rax, %rcx
	imulq	$274877907, %rax, %rax
	sarq	$46, %rcx
	subl	%edi, %ecx
	sarq	$38, %rax
	movslq	%ecx, %rdx
	movl	%ecx, %r8d
	subl	%edi, %eax
	imulq	$-2004318071, %rdx, %rdx
	sarl	$31, %r8d
	shrq	$32, %rdx
	addl	%ecx, %edx
	sarl	$5, %edx
	subl	%r8d, %edx
	imull	$60, %edx, %edx
	subl	%edx, %ecx
	movq	24(%rbp), %rdx
	movl	%ecx, (%rdx)
	movslq	%eax, %rdx
	movl	%eax, %ecx
	imulq	$-2004318071, %rdx, %rdx
	sarl	$31, %ecx
	shrq	$32, %rdx
	addl	%eax, %edx
	sarl	$5, %edx
	subl	%ecx, %edx
	movl	%eax, %ecx
	imull	$60, %edx, %edx
	imull	$1000, %eax, %eax
	subl	%edx, %ecx
	movq	32(%rbp), %rdx
	subl	%eax, %esi
	movq	40(%rbp), %rax
	movl	%ecx, (%rdx)
	movl	%esi, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	testl	%r10d, %r10d
	jne	.L98
	addl	$1, %eax
	cmpl	$59, %eax
	jg	.L105
.L97:
	cmpl	$30, %eax
	jg	.L101
	addl	$1, %eax
	movl	$0, (%rcx)
	movl	%eax, (%r8)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L101:
	subl	$30, %eax
	movl	$1, (%rcx)
	movl	%eax, (%r8)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L120:
	movl	564(%rdi), %r10d
	movl	%eax, 572(%rdi)
	movl	%edx, 560(%rdi)
	movl	%r10d, (%r11)
	movl	568(%rdi), %edi
	movl	%edi, (%rcx)
	movl	%eax, (%r8)
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L105:
	movl	$60, %r10d
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L111:
	movl	%r10d, %eax
	movl	$7, %r10d
	.p2align 4,,10
	.p2align 3
.L99:
	addl	$1, %eax
	movl	%r10d, (%rcx)
	movl	%eax, (%r8)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L106:
	movl	$2, %r10d
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L107:
	movl	%r10d, %eax
	movl	$3, %r10d
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L108:
	movl	%r10d, %eax
	movl	$4, %r10d
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L109:
	movl	%r10d, %eax
	movl	$5, %r10d
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L110:
	movl	%r10d, %eax
	movl	$6, %r10d
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L112:
	movl	%r10d, %eax
	movl	$8, %r10d
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L113:
	movl	%r10d, %eax
	movl	$9, %r10d
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L114:
	movl	%r10d, %eax
	movl	$10, %r10d
	jmp	.L99
	.cfi_endproc
.LFE18131:
	.size	_ZN2v88internal9DateCache13BreakDownTimeElPiS2_S2_S2_S2_S2_S2_S2_, .-_ZN2v88internal9DateCache13BreakDownTimeElPiS2_S2_S2_S2_S2_S2_S2_
	.section	.text._ZN2v88internal9DateCache8ProbeDSTEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9DateCache8ProbeDSTEi
	.type	_ZN2v88internal9DateCache8ProbeDSTEi, @function
_ZN2v88internal9DateCache8ProbeDSTEi:
.LFB18135:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %r10
	leaq	528(%rdi), %r9
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r10, %rax
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L149:
	testq	%rdx, %rdx
	je	.L132
	cmpl	(%rdx), %ecx
	cmovg	%rax, %rdx
.L123:
	addq	$16, %rax
	cmpq	%r9, %rax
	je	.L148
.L124:
	movl	(%rax), %ecx
	cmpl	%esi, %ecx
	jle	.L149
	movl	4(%rax), %ecx
	cmpl	%esi, %ecx
	jle	.L123
	testq	%r8, %r8
	je	.L134
	cmpl	4(%r8), %ecx
	cmovl	%rax, %r8
	addq	$16, %rax
	cmpq	%r9, %rax
	jne	.L124
	.p2align 4,,10
	.p2align 3
.L148:
	testq	%rdx, %rdx
	je	.L150
.L125:
	testq	%r8, %r8
	je	.L151
.L128:
	movq	%rdx, %xmm0
	movq	%r8, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 536(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	movq	%rax, %rdx
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L134:
	movq	%rax, %r8
	jmp	.L123
.L150:
	movq	536(%rdi), %rax
	movl	4(%rax), %esi
	cmpl	%esi, (%rax)
	jg	.L136
	movq	%r10, %rax
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L153:
	movl	12(%rax), %esi
	cmpl	%esi, 12(%rdx)
	cmovg	%rax, %rdx
.L126:
	addq	$16, %rax
	cmpq	%r9, %rax
	je	.L152
.L127:
	cmpq	%rax, %r8
	je	.L126
	testq	%rdx, %rdx
	jne	.L153
	movq	%rax, %rdx
	addq	$16, %rax
	cmpq	%r9, %rax
	jne	.L127
.L152:
	movdqa	.LC0(%rip), %xmm0
	movups	%xmm0, (%rdx)
	testq	%r8, %r8
	jne	.L128
.L151:
	movq	544(%rdi), %rax
	movl	4(%rax), %esi
	cmpl	%esi, (%rax)
	jle	.L131
	cmpq	%rdx, %rax
	je	.L131
	movq	%rax, %r8
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L155:
	movl	12(%r10), %eax
	cmpl	%eax, 12(%r8)
	cmovg	%r10, %r8
.L130:
	addq	$16, %r10
	cmpq	%r9, %r10
	je	.L154
.L131:
	cmpq	%r10, %rdx
	je	.L130
	testq	%r8, %r8
	jne	.L155
	movq	%r10, %r8
	addq	$16, %r10
	cmpq	%r9, %r10
	jne	.L131
.L154:
	movdqa	.LC0(%rip), %xmm0
	movups	%xmm0, (%r8)
	jmp	.L128
.L136:
	movq	%rax, %rdx
	jmp	.L125
	.cfi_endproc
.LFE18135:
	.size	_ZN2v88internal9DateCache8ProbeDSTEi, .-_ZN2v88internal9DateCache8ProbeDSTEi
	.section	.text._ZN2v88internal9DateCache25DaylightSavingsOffsetInMsEl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9DateCache25DaylightSavingsOffsetInMsEl
	.type	_ZN2v88internal9DateCache25DaylightSavingsOffsetInMsEl, @function
_ZN2v88internal9DateCache25DaylightSavingsOffsetInMsEl:
.LFB18134:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movabsq	$2147483647000, %rax
	cmpq	%rax, %rsi
	ja	.L157
.L215:
	movabsq	$2361183241434822607, %rdx
	movq	%rcx, %rax
	sarq	$63, %rcx
	imulq	%rdx
	movl	528(%rbx), %eax
	sarq	$7, %rdx
	movl	%edx, %r12d
	subl	%ecx, %r12d
	cmpl	$2147483636, %eax
	jle	.L167
	movl	$0, 528(%rbx)
	movdqa	.LC0(%rip), %xmm0
	leaq	16(%rbx), %rax
	leaq	528(%rbx), %rcx
	.p2align 4,,10
	.p2align 3
.L168:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L168
	xorl	%eax, %eax
.L167:
	movq	536(%rbx), %rdx
	cmpl	%r12d, (%rdx)
	jg	.L169
	cmpl	%r12d, 4(%rdx)
	jge	.L218
.L169:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9DateCache8ProbeDSTEi
	movq	536(%rbx), %rax
	movl	4(%rax), %edx
	cmpl	%edx, (%rax)
	jg	.L219
	cmpl	%edx, %r12d
	jle	.L220
	leal	-1641600(%r12), %ecx
	cmpl	%edx, %ecx
	jg	.L221
	movl	528(%rbx), %edx
	movl	$2145842047, %r13d
	leal	1(%rdx), %ecx
	movl	%ecx, 528(%rbx)
	movl	%ecx, 12(%rax)
	movq	536(%rbx), %rax
	cmpl	$2145842047, 4(%rax)
	cmovle	4(%rax), %r13d
	movq	544(%rbx), %rax
	addl	$1641600, %r13d
	cmpl	%r13d, (%rax)
	jge	.L222
	addl	$2, %edx
	movl	%edx, 528(%rbx)
	movl	%edx, 12(%rax)
.L187:
	movq	544(%rbx), %rax
	movq	536(%rbx), %rdx
	movl	$4, %r15d
	leaq	_ZN2v88internal9DateCache30GetDaylightSavingsOffsetFromOSEl(%rip), %r14
	movl	8(%rax), %edi
	cmpl	%edi, 8(%rdx)
	je	.L223
	movl	(%rax), %eax
	movl	4(%rdx), %edx
	movl	%r12d, %r13d
	testl	%r15d, %r15d
	je	.L192
.L226:
	subl	%edx, %eax
	movl	%eax, %r13d
	shrl	$31, %r13d
	addl	%eax, %r13d
	sarl	%r13d
	addl	%edx, %r13d
.L192:
	movq	(%rbx), %rax
	movslq	%r13d, %rsi
	movq	16(%rax), %rax
	cmpq	%r14, %rax
	jne	.L193
.L227:
	movq	592(%rbx), %rdi
	imulq	$1000, %rsi, %rsi
	pxor	%xmm0, %xmm0
	movq	(%rdi), %rax
	cvtsi2sdq	%rsi, %xmm0
	call	*8(%rax)
	movq	536(%rbx), %rdx
	cvttsd2sil	%xmm0, %eax
	cmpl	%eax, 8(%rdx)
	je	.L224
.L195:
	movq	544(%rbx), %rdx
	movl	%r13d, (%rdx)
	movq	544(%rbx), %rdx
	cmpl	%r12d, (%rdx)
	jle	.L225
	subl	$1, %r15d
	cmpl	$-1, %r15d
	je	.L207
.L228:
	movq	544(%rbx), %rax
	movq	536(%rbx), %rdx
	movl	%r12d, %r13d
	movl	(%rax), %eax
	movl	4(%rdx), %edx
	testl	%r15d, %r15d
	jne	.L226
	movq	(%rbx), %rax
	movslq	%r13d, %rsi
	movq	16(%rax), %rax
	cmpq	%r14, %rax
	je	.L227
.L193:
	movq	%rbx, %rdi
	call	*%rax
	movq	536(%rbx), %rdx
	cmpl	%eax, 8(%rdx)
	jne	.L195
	.p2align 4,,10
	.p2align 3
.L224:
	movl	%r13d, 4(%rdx)
	movq	536(%rbx), %rdx
	cmpl	%r12d, 4(%rdx)
	jge	.L156
	subl	$1, %r15d
	cmpl	$-1, %r15d
	jne	.L228
.L207:
	xorl	%eax, %eax
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L218:
	addl	$1, %eax
	movl	%eax, 528(%rbx)
	movl	%eax, 12(%rdx)
	movq	536(%rbx), %rax
	movl	8(%rax), %eax
.L156:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L229
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	testq	%rsi, %rsi
	leaq	-86399999(%rsi), %rdi
	leaq	-60(%rbp), %r8
	movabsq	$7164004856975580295, %rdx
	cmovns	%rsi, %rdi
	movq	%rdi, %rax
	sarq	$63, %rdi
	imulq	%rdx
	movq	%rdx, %rsi
	leaq	-68(%rbp), %rdx
	sarq	$25, %rsi
	subq	%rdi, %rsi
	movq	%rbx, %rdi
	imull	$-86400000, %esi, %r12d
	addl	%ecx, %r12d
	leaq	-64(%rbp), %rcx
	call	_ZN2v88internal9DateCache20YearMonthDayFromDaysEiPiS2_S2_
	movl	-68(%rbp), %ecx
	movl	-64(%rbp), %edx
	leal	399999(%rcx), %esi
	leal	400002(%rcx), %edi
	imull	$365, %esi, %eax
	testl	%esi, %esi
	cmovns	%esi, %edi
	sarl	$2, %edi
	addl	%eax, %edi
	movslq	%esi, %rax
	sarl	$31, %esi
	imulq	$1374389535, %rax, %rax
	movq	%rax, %r11
	sarq	$39, %rax
	sarq	$37, %r11
	subl	%esi, %eax
	movq	%r11, %r8
	movl	%esi, %r11d
	subl	%r8d, %r11d
	addl	%r11d, %edi
	leal	-146816158(%rdi,%rax), %esi
	movslq	%esi, %rax
	movl	%esi, %edi
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %edi
	shrq	$32, %rax
	addl	%esi, %eax
	sarl	$2, %eax
	subl	%edi, %eax
	leal	0(,%rax,8), %edi
	subl	%eax, %edi
	subl	%edi, %esi
	movl	%esi, %eax
	testb	$3, %cl
	jne	.L160
	testl	%eax, %eax
	leal	7(%rsi), %esi
	cmovs	%esi, %eax
	imull	$-1030792151, %ecx, %ecx
	movl	$1956, %esi
	addl	$85899344, %ecx
	movl	%ecx, %edi
	rorl	$2, %edi
	cmpl	$42949672, %edi
	jbe	.L230
.L162:
	leal	(%rax,%rax,2), %ecx
	movl	%ecx, %eax
	leal	(%rsi,%rcx,4), %ecx
	movl	%edx, %esi
	andl	$1073741823, %eax
	sarl	$31, %esi
	imulq	$613566757, %rax, %rax
	shrq	$32, %rax
	imull	$28, %eax, %eax
	subl	%eax, %ecx
	subl	$1924, %ecx
	movl	%ecx, %eax
	shrl	$2, %eax
	imulq	$613566757, %rax, %rax
	shrq	$32, %rax
	imull	$28, %eax, %eax
	subl	%eax, %ecx
	movslq	%edx, %rax
	imulq	$715827883, %rax, %rax
	sarq	$33, %rax
	subl	%esi, %eax
	leal	2008(%rcx,%rax), %esi
	leal	(%rax,%rax,2), %eax
	sall	$2, %eax
	subl	%eax, %edx
	js	.L231
.L163:
	leal	399999(%rsi), %ecx
	leal	400002(%rsi), %edi
	imull	$365, %ecx, %eax
	testl	%ecx, %ecx
	cmovns	%ecx, %edi
	sarl	$2, %edi
	addl	%eax, %edi
	movslq	%ecx, %rax
	sarl	$31, %ecx
	imulq	$1374389535, %rax, %rax
	movl	%ecx, %r10d
	movq	%rax, %r14
	sarq	$39, %rax
	sarq	$37, %r14
	subl	%ecx, %eax
	subl	%r14d, %r10d
	addl	%r10d, %edi
	leal	-146816162(%rdi,%rax), %ecx
	testb	$3, %sil
	jne	.L164
	imull	$-1030792151, %esi, %eax
	addl	$85899344, %eax
	movl	%eax, %esi
	rorl	$2, %esi
	cmpl	$42949672, %esi
	jbe	.L232
.L165:
	movslq	%edx, %rdx
	leaq	_ZZN2v88internal9DateCache17DaysFromYearMonthEiiE19day_from_month_leap(%rip), %rax
	addl	(%rax,%rdx,4), %ecx
.L166:
	addl	-60(%rbp), %ecx
	subl	$1, %ecx
	movslq	%ecx, %rcx
	imulq	$86400000, %rcx, %rax
	movslq	%r12d, %rcx
	addq	%rax, %rcx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L219:
	movl	%r12d, (%rax)
	movq	536(%rbx), %rax
	leaq	_ZN2v88internal9DateCache30GetDaylightSavingsOffsetFromOSEl(%rip), %rdx
	movslq	%r12d, %rsi
	movl	%r12d, 4(%rax)
	movq	(%rbx), %rax
	movq	536(%rbx), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L172
	movq	592(%rbx), %rdi
	imulq	$1000, %rsi, %rsi
	pxor	%xmm0, %xmm0
	movq	(%rdi), %rax
	cvtsi2sdq	%rsi, %xmm0
	call	*8(%rax)
	cvttsd2sil	%xmm0, %eax
.L173:
	movl	%eax, 8(%r12)
	movl	528(%rbx), %eax
	movq	536(%rbx), %rdx
	addl	$1, %eax
	movl	%eax, 528(%rbx)
	movl	%eax, 12(%rdx)
	movq	536(%rbx), %rax
	movl	8(%rax), %eax
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L220:
	movl	528(%rbx), %edi
	leal	1(%rdi), %edx
	movl	%edx, 528(%rbx)
	movl	%edx, 12(%rax)
	movq	536(%rbx), %rax
	movl	8(%rax), %eax
	jmp	.L156
.L231:
	subl	$1, %esi
	addl	$12, %edx
	jmp	.L163
.L222:
	movq	(%rbx), %rax
	leaq	_ZN2v88internal9DateCache30GetDaylightSavingsOffsetFromOSEl(%rip), %r14
	movslq	%r13d, %rsi
	movq	16(%rax), %rax
	cmpq	%r14, %rax
	jne	.L184
	movq	592(%rbx), %rdi
	imulq	$1000, %rsi, %rsi
	pxor	%xmm0, %xmm0
	movq	(%rdi), %rax
	cvtsi2sdq	%rsi, %xmm0
	call	*8(%rax)
	cvttsd2sil	%xmm0, %edi
.L185:
	movq	544(%rbx), %rdx
	movl	(%rdx), %eax
	movl	4(%rdx), %ecx
	cmpl	8(%rdx), %edi
	je	.L233
.L186:
	cmpl	%ecx, %eax
	jg	.L188
	movq	536(%rbx), %rsi
	leaq	16(%rbx), %rax
	leaq	528(%rbx), %rcx
	xorl	%edx, %edx
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L235:
	movl	12(%rax), %r10d
	cmpl	%r10d, 12(%rdx)
	cmovg	%rax, %rdx
.L189:
	addq	$16, %rax
	cmpq	%rcx, %rax
	je	.L234
.L190:
	cmpq	%rax, %rsi
	je	.L189
	testq	%rdx, %rdx
	jne	.L235
	movq	%rax, %rdx
	jmp	.L189
.L232:
	rorl	$4, %eax
	cmpl	$10737418, %eax
	jbe	.L165
.L164:
	movslq	%edx, %rdx
	leaq	_ZZN2v88internal9DateCache17DaysFromYearMonthEiiE14day_from_month(%rip), %rax
	addl	(%rax,%rdx,4), %ecx
	jmp	.L166
.L230:
	rorl	$4, %ecx
	cmpl	$10737419, %ecx
	sbbl	%esi, %esi
	andl	$-11, %esi
	addl	$1967, %esi
	jmp	.L162
.L221:
	movq	(%rbx), %rax
	leaq	_ZN2v88internal9DateCache30GetDaylightSavingsOffsetFromOSEl(%rip), %rdx
	movslq	%r12d, %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L176
	movq	592(%rbx), %rdi
	imulq	$1000, %rsi, %rsi
	pxor	%xmm0, %xmm0
	movq	(%rdi), %rax
	cvtsi2sdq	%rsi, %xmm0
	call	*8(%rax)
	cvttsd2sil	%xmm0, %eax
.L177:
	movq	544(%rbx), %rcx
	movl	(%rcx), %edx
	movl	4(%rcx), %esi
	cmpl	8(%rcx), %eax
	je	.L236
.L178:
	cmpl	%esi, %edx
	jg	.L180
	movq	536(%rbx), %rdi
	leaq	16(%rbx), %rdx
	leaq	528(%rbx), %rsi
	xorl	%ecx, %ecx
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L238:
	movl	12(%rdx), %r9d
	cmpl	%r9d, 12(%rcx)
	cmovg	%rdx, %rcx
.L181:
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	je	.L237
.L182:
	cmpq	%rdx, %rdi
	je	.L181
	testq	%rcx, %rcx
	jne	.L238
	movq	%rdx, %rcx
	jmp	.L181
.L160:
	testl	%esi, %esi
	jns	.L214
	addl	$7, %eax
.L214:
	movl	$1967, %esi
	jmp	.L162
.L237:
	movdqa	.LC0(%rip), %xmm0
	movups	%xmm0, (%rcx)
	movq	%rcx, 544(%rbx)
.L180:
	movl	%r12d, (%rcx)
	movq	544(%rbx), %rdx
	movl	%r12d, 4(%rdx)
	movq	544(%rbx), %rdx
	movl	%eax, 8(%rdx)
	movl	528(%rbx), %edi
	movq	544(%rbx), %rcx
	leal	1(%rdi), %edx
	movl	%edx, 528(%rbx)
	movl	%edx, 12(%rcx)
.L179:
	movdqu	536(%rbx), %xmm0
	shufpd	$1, %xmm0, %xmm0
	movups	%xmm0, 536(%rbx)
	jmp	.L156
.L234:
	movdqa	.LC0(%rip), %xmm0
	movups	%xmm0, (%rdx)
	movq	%rdx, 544(%rbx)
.L188:
	movl	%r13d, (%rdx)
	movq	544(%rbx), %rax
	movl	%r13d, 4(%rax)
	movq	544(%rbx), %rax
	movl	%edi, 8(%rax)
	movl	528(%rbx), %eax
	movq	544(%rbx), %rdx
	addl	$1, %eax
	movl	%eax, 528(%rbx)
	movl	%eax, 12(%rdx)
	jmp	.L187
.L172:
	call	*%rax
	jmp	.L173
.L223:
	movl	4(%rax), %eax
	movdqa	.LC0(%rip), %xmm0
	movl	%eax, 4(%rdx)
	movq	544(%rbx), %rax
	movups	%xmm0, (%rax)
	movq	536(%rbx), %rax
	movl	8(%rax), %eax
	jmp	.L156
.L236:
	leal	-1641600(%rdx), %edi
	cmpl	%edi, %r12d
	jl	.L178
	cmpl	%esi, %r12d
	jg	.L178
	movl	%r12d, (%rcx)
	jmp	.L179
.L233:
	leal	-1641600(%rax), %esi
	cmpl	%esi, %r13d
	jl	.L186
	cmpl	%ecx, %r13d
	jg	.L186
	movl	%r13d, (%rdx)
	jmp	.L187
.L176:
	call	*%rax
	jmp	.L177
.L184:
	call	*%rax
	movl	%eax, %edi
	jmp	.L185
.L225:
	movq	536(%rbx), %rcx
	movq	%rdx, 536(%rbx)
	movq	%rcx, 544(%rbx)
	jmp	.L156
.L229:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18134:
	.size	_ZN2v88internal9DateCache25DaylightSavingsOffsetInMsEl, .-_ZN2v88internal9DateCache25DaylightSavingsOffsetInMsEl
	.section	.text._ZN2v88internal9DateCache20GetLocalOffsetFromOSElb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9DateCache20GetLocalOffsetFromOSElb
	.type	_ZN2v88internal9DateCache20GetLocalOffsetFromOSElb, @function
_ZN2v88internal9DateCache20GetLocalOffsetFromOSElb:
.LFB18132:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$24, %rsp
	cmpb	$0, _ZN2v88internal22FLAG_icu_timezone_dataE(%rip)
	je	.L240
	movq	592(%rdi), %rdi
	pxor	%xmm0, %xmm0
	movzbl	%dl, %esi
	cvtsi2sdq	%r12, %xmm0
	movq	(%rdi), %rax
	call	*16(%rax)
	addq	$24, %rsp
	popq	%rbx
	cvttsd2sil	%xmm0, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore_state
	movl	552(%rdi), %eax
	cmpl	$2147483647, %eax
	je	.L245
	pxor	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	testb	%bl, %bl
	jne	.L243
.L246:
	movsd	.LC9(%rip), %xmm0
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%r12, %xmm1
	addsd	%xmm2, %xmm0
	subsd	%xmm0, %xmm1
	cvttsd2siq	%xmm1, %r12
.L243:
	movq	%r12, %rsi
	movq	%r13, %rdi
	movsd	%xmm2, -40(%rbp)
	call	_ZN2v88internal9DateCache25DaylightSavingsOffsetInMsEl
	pxor	%xmm0, %xmm0
	movsd	-40(%rbp), %xmm2
	addq	$24, %rsp
	cvtsi2sdl	%eax, %xmm0
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addsd	%xmm2, %xmm0
	cvttsd2sil	%xmm0, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	movq	592(%rdi), %rdi
	pxor	%xmm0, %xmm0
	movzbl	%dl, %esi
	cvtsi2sdq	%r12, %xmm0
	movq	(%rdi), %rax
	call	*16(%rax)
	pxor	%xmm2, %xmm2
	cvttsd2sil	%xmm0, %eax
	cvtsi2sdl	%eax, %xmm2
	movl	%eax, 552(%r13)
	testb	%bl, %bl
	jne	.L243
	jmp	.L246
	.cfi_endproc
.LFE18132:
	.size	_ZN2v88internal9DateCache20GetLocalOffsetFromOSElb, .-_ZN2v88internal9DateCache20GetLocalOffsetFromOSElb
	.section	.text._ZN2v88internal9DateCache20LeastRecentlyUsedDSTEPNS1_3DSTE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9DateCache20LeastRecentlyUsedDSTEPNS1_3DSTE
	.type	_ZN2v88internal9DateCache20LeastRecentlyUsedDSTEPNS1_3DSTE, @function
_ZN2v88internal9DateCache20LeastRecentlyUsedDSTEPNS1_3DSTE:
.LFB18136:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rax
	leaq	528(%rdi), %rdx
	xorl	%r8d, %r8d
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L254:
	movl	12(%rax), %ecx
	cmpl	%ecx, 12(%r8)
	cmovg	%rax, %r8
.L248:
	addq	$16, %rax
	cmpq	%rdx, %rax
	je	.L253
.L249:
	cmpq	%rsi, %rax
	je	.L248
	testq	%r8, %r8
	jne	.L254
	movq	%rax, %r8
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L249
.L253:
	movdqa	.LC0(%rip), %xmm0
	movq	%r8, %rax
	movups	%xmm0, (%r8)
	ret
	.cfi_endproc
.LFE18136:
	.size	_ZN2v88internal9DateCache20LeastRecentlyUsedDSTEPNS1_3DSTE, .-_ZN2v88internal9DateCache20LeastRecentlyUsedDSTEPNS1_3DSTE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal9DateCacheC2Ev,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal9DateCacheC2Ev, @function
_GLOBAL__sub_I__ZN2v88internal9DateCacheC2Ev:
.LFB21862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21862:
	.size	_GLOBAL__sub_I__ZN2v88internal9DateCacheC2Ev, .-_GLOBAL__sub_I__ZN2v88internal9DateCacheC2Ev
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal9DateCacheC2Ev
	.weak	_ZTVN2v88internal9DateCacheE
	.section	.data.rel.ro.local._ZTVN2v88internal9DateCacheE,"awG",@progbits,_ZTVN2v88internal9DateCacheE,comdat
	.align 8
	.type	_ZTVN2v88internal9DateCacheE, @object
	.size	_ZTVN2v88internal9DateCacheE, 48
_ZTVN2v88internal9DateCacheE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal9DateCacheD1Ev
	.quad	_ZN2v88internal9DateCacheD0Ev
	.quad	_ZN2v88internal9DateCache30GetDaylightSavingsOffsetFromOSEl
	.quad	_ZN2v88internal9DateCache20GetLocalOffsetFromOSElb
	.section	.rodata._ZZN2v88internal9DateCache17DaysFromYearMonthEiiE19day_from_month_leap,"a"
	.align 32
	.type	_ZZN2v88internal9DateCache17DaysFromYearMonthEiiE19day_from_month_leap, @object
	.size	_ZZN2v88internal9DateCache17DaysFromYearMonthEiiE19day_from_month_leap, 48
_ZZN2v88internal9DateCache17DaysFromYearMonthEiiE19day_from_month_leap:
	.long	0
	.long	31
	.long	60
	.long	91
	.long	121
	.long	152
	.long	182
	.long	213
	.long	244
	.long	274
	.long	305
	.long	335
	.section	.rodata._ZZN2v88internal9DateCache17DaysFromYearMonthEiiE14day_from_month,"a"
	.align 32
	.type	_ZZN2v88internal9DateCache17DaysFromYearMonthEiiE14day_from_month, @object
	.size	_ZZN2v88internal9DateCache17DaysFromYearMonthEiiE14day_from_month, 48
_ZZN2v88internal9DateCache17DaysFromYearMonthEiiE14day_from_month:
	.long	0
	.long	31
	.long	59
	.long	90
	.long	120
	.long	151
	.long	181
	.long	212
	.long	243
	.long	273
	.long	304
	.long	334
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	-2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	0
	.long	2146959360
	.align 8
.LC3:
	.long	3269197824
	.long	-1019301368
	.align 8
.LC4:
	.long	3269197824
	.long	1128182280
	.section	.rodata.cst16
	.align 16
.LC5:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC6:
	.long	4294967295
	.long	2146435071
	.align 8
.LC7:
	.long	0
	.long	1127219200
	.align 8
.LC8:
	.long	0
	.long	1072693248
	.align 8
.LC9:
	.long	0
	.long	1095464768
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
