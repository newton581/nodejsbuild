	.file	"snapshot-source-sink.cc"
	.text
	.section	.rodata._ZN2v88internal16SnapshotByteSink6PutRawEPKhiPKc.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vector::_M_range_insert"
	.section	.text._ZN2v88internal16SnapshotByteSink6PutRawEPKhiPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16SnapshotByteSink6PutRawEPKhiPKc
	.type	_ZN2v88internal16SnapshotByteSink6PutRawEPKhiPKc, @function
_ZN2v88internal16SnapshotByteSink6PutRawEPKhiPKc:
.LFB17911:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movslq	%edx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	testq	%r12, %r12
	je	.L1
	movq	8(%rdi), %r15
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rsi, %r8
	subq	%r15, %rax
	cmpq	%rax, %r12
	ja	.L3
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
	addq	%r12, 8(%rbx)
.L1:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	movabsq	$9223372036854775807, %rcx
	movq	(%rdi), %r9
	movq	%r15, %rdx
	movq	%rcx, %rax
	subq	%r9, %rdx
	subq	%rdx, %rax
	cmpq	%rax, %r12
	ja	.L27
	cmpq	%rdx, %r12
	movq	%rdx, %rax
	cmovnb	%r12, %rax
	addq	%rdx, %rax
	movq	%rax, %r14
	jc	.L15
	testq	%rax, %rax
	js	.L15
	jne	.L7
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
.L13:
	leaq	0(%r13,%rdx), %r11
	leaq	(%r11,%r12), %r10
	testq	%rdx, %rdx
	jne	.L28
	movq	%r12, %rdx
	movq	%r8, %rsi
	movq	%r11, %rdi
	movq	%r9, -64(%rbp)
	movq	%r10, -56(%rbp)
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-56(%rbp), %r10
	movq	-64(%rbp), %r9
	subq	%r15, %rdx
	leaq	(%r10,%rdx), %r12
	jne	.L9
.L11:
	testq	%r9, %r9
	jne	.L10
.L12:
	movq	%r13, %xmm0
	movq	%r12, %xmm1
	movq	%r14, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movq	%rcx, %r14
.L7:
	movq	%r14, %rdi
	movq	%r8, -56(%rbp)
	call	_Znwm@PLT
	movq	(%rbx), %r9
	movq	%r15, %rdx
	movq	-56(%rbp), %r8
	movq	%rax, %r13
	addq	%rax, %r14
	subq	%r9, %rdx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L28:
	movq	%r9, %rsi
	movq	%r13, %rdi
	movq	%r10, -72(%rbp)
	movq	%r9, -56(%rbp)
	movq	%r8, -80(%rbp)
	movq	%r11, -64(%rbp)
	call	memmove@PLT
	movq	-80(%rbp), %r8
	movq	-64(%rbp), %r11
	movq	%r12, %rdx
	movq	%r8, %rsi
	movq	%r11, %rdi
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-72(%rbp), %r10
	movq	-56(%rbp), %r9
	subq	%r15, %rdx
	leaq	(%r10,%rdx), %r12
	jne	.L9
.L10:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%r15, %rsi
	movq	%r10, %rdi
	movq	%r9, -56(%rbp)
	call	memcpy@PLT
	movq	-56(%rbp), %r9
	jmp	.L11
.L27:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE17911:
	.size	_ZN2v88internal16SnapshotByteSink6PutRawEPKhiPKc, .-_ZN2v88internal16SnapshotByteSink6PutRawEPKhiPKc
	.section	.text._ZN2v88internal16SnapshotByteSink6AppendERKS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16SnapshotByteSink6AppendERKS1_
	.type	_ZN2v88internal16SnapshotByteSink6AppendERKS1_, @function
_ZN2v88internal16SnapshotByteSink6AppendERKS1_:
.LFB17912:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %r12
	movq	(%rsi), %r13
	movq	8(%rdi), %r14
	cmpq	%r13, %r12
	je	.L29
	movq	16(%rdi), %rax
	subq	%r13, %r12
	movq	%rdi, %rbx
	subq	%r14, %rax
	cmpq	%rax, %r12
	ja	.L31
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	memmove@PLT
	addq	%r12, 8(%rbx)
.L29:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movabsq	$9223372036854775807, %rcx
	movq	(%rdi), %r9
	movq	%r14, %rdx
	movq	%rcx, %rax
	subq	%r9, %rdx
	subq	%rdx, %rax
	cmpq	%rax, %r12
	ja	.L51
	cmpq	%r12, %rdx
	movq	%r12, %rax
	cmovnb	%rdx, %rax
	addq	%rdx, %rax
	movq	%rax, %r15
	jc	.L43
	testq	%rax, %rax
	js	.L43
	jne	.L35
	movq	$0, -56(%rbp)
	xorl	%r15d, %r15d
.L41:
	movq	-56(%rbp), %rax
	leaq	(%rax,%rdx), %r11
	leaq	(%r11,%r12), %r10
	testq	%rdx, %rdx
	jne	.L52
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r11, %rdi
	movq	%r10, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %r9
	subq	%r14, %rdx
	leaq	(%r10,%rdx), %r12
	jne	.L37
.L39:
	testq	%r9, %r9
	jne	.L38
.L40:
	movq	-56(%rbp), %xmm0
	movq	%r12, %xmm1
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movq	%rcx, %r15
.L35:
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	(%rbx), %r9
	movq	%r14, %rdx
	movq	%rax, -56(%rbp)
	addq	%rax, %r15
	subq	%r9, %rdx
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L52:
	movq	%r9, %rsi
	movq	%rax, %rdi
	movq	%r10, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r11, -80(%rbp)
	call	memmove@PLT
	movq	-80(%rbp), %r11
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r11, %rdi
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %r9
	subq	%r14, %rdx
	leaq	(%r10,%rdx), %r12
	jne	.L37
.L38:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%r14, %rsi
	movq	%r10, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	jmp	.L39
.L51:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE17912:
	.size	_ZN2v88internal16SnapshotByteSink6AppendERKS1_, .-_ZN2v88internal16SnapshotByteSink6AppendERKS1_
	.section	.rodata._ZN2v88internal18SnapshotByteSource7GetBlobEPPKh.str1.1,"aMS",@progbits,1
.LC1:
	.string	"position_ + size <= length_"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal18SnapshotByteSource7GetBlobEPPKh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18SnapshotByteSource7GetBlobEPPKh
	.type	_ZN2v88internal18SnapshotByteSource7GetBlobEPPKh, @function
_ZN2v88internal18SnapshotByteSource7GetBlobEPPKh:
.LFB17913:
	.cfi_startproc
	endbr64
	movslq	12(%rdi), %rax
	movq	(%rdi), %r8
	movzbl	1(%r8,%rax), %edx
	movzbl	2(%r8,%rax), %ecx
	movq	%rax, %r9
	movzbl	(%r8,%rax), %r10d
	movzbl	3(%r8,%rax), %eax
	sall	$16, %ecx
	sall	$8, %edx
	orl	%ecx, %edx
	sall	$24, %eax
	movl	$32, %ecx
	orl	%r10d, %edx
	orl	%edx, %eax
	andl	$3, %edx
	addl	$1, %edx
	movl	%eax, %r10d
	addl	%edx, %r9d
	sall	$3, %edx
	subl	%edx, %ecx
	movl	$-1, %edx
	movl	%r9d, 12(%rdi)
	movl	%edx, %eax
	shrl	%cl, %eax
	andl	%r10d, %eax
	shrl	$2, %eax
	leal	(%r9,%rax), %edx
	cmpl	8(%rdi), %edx
	jg	.L58
	movslq	%r9d, %r9
	addq	%r9, %r8
	movq	%r8, (%rsi)
	addl	%eax, 12(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17913:
	.size	_ZN2v88internal18SnapshotByteSource7GetBlobEPPKh, .-_ZN2v88internal18SnapshotByteSource7GetBlobEPPKh
	.section	.rodata._ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_.str1.1,"aMS",@progbits,1
.LC3:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.type	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_, @function
_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_:
.LFB19999:
	.cfi_startproc
	endbr64
	movabsq	$9223372036854775807, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r15
	movq	%rsi, %rax
	subq	%r15, %rax
	cmpq	%rcx, %rax
	je	.L73
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L68
	leaq	(%rax,%rax), %r14
	cmpq	%r14, %rax
	jbe	.L74
.L70:
	movq	%rcx, %r14
.L61:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L67:
	movzbl	0(%r13), %eax
	subq	%r8, %rsi
	leaq	1(%rbx,%rdx), %r10
	movq	%rsi, %r13
	movb	%al, (%rbx,%rdx)
	leaq	(%r10,%rsi), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L75
	testq	%rsi, %rsi
	jg	.L63
	testq	%r15, %r15
	jne	.L66
.L64:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L63
.L66:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L74:
	testq	%r14, %r14
	js	.L70
	jne	.L61
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L63:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L64
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L68:
	movl	$1, %r14d
	jmp	.L61
.L73:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE19999:
	.size	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_, .-_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.section	.text._ZN2v88internal16SnapshotByteSink6PutIntEmPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc
	.type	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc, @function
_ZN2v88internal16SnapshotByteSink6PutIntEmPKc:
.LFB17910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$1, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	0(,%rsi,4), %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$255, %rbx
	jbe	.L77
	cmpq	$65535, %rbx
	jbe	.L97
	cmpq	$16777215, %rbx
	jbe	.L79
	orq	$3, %rbx
	movl	$4, %r13d
.L77:
	movb	%bl, -41(%rbp)
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L80
	movb	%bl, (%rsi)
	addq	$1, 8(%r12)
.L81:
	cmpl	$1, %r13d
	jne	.L98
.L76:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L99
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movq	%rbx, %rax
	movq	8(%r12), %rsi
	shrq	$8, %rax
	movb	%al, -41(%rbp)
	cmpq	16(%r12), %rsi
	je	.L84
	movb	%al, (%rsi)
	addq	$1, 8(%r12)
.L85:
	cmpl	$2, %r13d
	je	.L76
	movq	%rbx, %rax
	movq	8(%r12), %rsi
	shrq	$16, %rax
	movb	%al, -41(%rbp)
	cmpq	16(%r12), %rsi
	je	.L87
	movb	%al, (%rsi)
	addq	$1, 8(%r12)
.L88:
	cmpl	$4, %r13d
	jne	.L76
	shrq	$24, %rbx
	movq	8(%r12), %rsi
	movb	%bl, -41(%rbp)
	cmpq	16(%r12), %rsi
	je	.L89
	movb	%bl, (%rsi)
	addq	$1, 8(%r12)
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L80:
	leaq	-41(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L84:
	leaq	-41(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L87:
	leaq	-41(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L89:
	leaq	-41(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L76
.L99:
	call	__stack_chk_fail@PLT
.L79:
	orq	$2, %rbx
	movl	$3, %r13d
	jmp	.L77
.L97:
	orq	$1, %rbx
	movl	$2, %r13d
	jmp	.L77
	.cfi_endproc
.LFE17910:
	.size	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc, .-_ZN2v88internal16SnapshotByteSink6PutIntEmPKc
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal16SnapshotByteSink6PutIntEmPKc,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal16SnapshotByteSink6PutIntEmPKc, @function
_GLOBAL__sub_I__ZN2v88internal16SnapshotByteSink6PutIntEmPKc:
.LFB21775:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21775:
	.size	_GLOBAL__sub_I__ZN2v88internal16SnapshotByteSink6PutIntEmPKc, .-_GLOBAL__sub_I__ZN2v88internal16SnapshotByteSink6PutIntEmPKc
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal16SnapshotByteSink6PutIntEmPKc
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
