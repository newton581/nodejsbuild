	.file	"scanner.cc"
	.text
	.section	.text._ZN2v88internal14IsDecimalDigitEi,"axG",@progbits,_ZN2v88internal14IsDecimalDigitEi,comdat
	.p2align 4
	.weak	_ZN2v88internal14IsDecimalDigitEi
	.type	_ZN2v88internal14IsDecimalDigitEi, @function
_ZN2v88internal14IsDecimalDigitEi:
.LFB16362:
	.cfi_startproc
	endbr64
	subl	$48, %edi
	cmpl	$9, %edi
	setbe	%al
	ret
	.cfi_endproc
.LFE16362:
	.size	_ZN2v88internal14IsDecimalDigitEi, .-_ZN2v88internal14IsDecimalDigitEi
	.section	.text._ZN2v88internal12IsOctalDigitEi,"axG",@progbits,_ZN2v88internal12IsOctalDigitEi,comdat
	.p2align 4
	.weak	_ZN2v88internal12IsOctalDigitEi
	.type	_ZN2v88internal12IsOctalDigitEi, @function
_ZN2v88internal12IsOctalDigitEi:
.LFB16364:
	.cfi_startproc
	endbr64
	subl	$48, %edi
	cmpl	$7, %edi
	setbe	%al
	ret
	.cfi_endproc
.LFE16364:
	.size	_ZN2v88internal12IsOctalDigitEi, .-_ZN2v88internal12IsOctalDigitEi
	.section	.text._ZN2v88internal13IsBinaryDigitEi,"axG",@progbits,_ZN2v88internal13IsBinaryDigitEi,comdat
	.p2align 4
	.weak	_ZN2v88internal13IsBinaryDigitEi
	.type	_ZN2v88internal13IsBinaryDigitEi, @function
_ZN2v88internal13IsBinaryDigitEi:
.LFB16366:
	.cfi_startproc
	endbr64
	subl	$48, %edi
	cmpl	$1, %edi
	setbe	%al
	ret
	.cfi_endproc
.LFE16366:
	.size	_ZN2v88internal13IsBinaryDigitEi, .-_ZN2v88internal13IsBinaryDigitEi
	.section	.text._ZN2v88internal18PerfectKeywordHash8GetTokenEPKci.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal18PerfectKeywordHash8GetTokenEPKci.part.0, @function
_ZN2v88internal18PerfectKeywordHash8GetTokenEPKci.part.0:
.LFB21974:
	.cfi_startproc
	movzbl	1(%rdi), %edx
	leaq	_ZZN2v88internal18PerfectKeywordHash4HashEPKciE11asso_values(%rip), %rax
	movzbl	(%rax,%rdx), %ecx
	movzbl	(%rdi), %edx
	movzbl	(%rax,%rdx), %eax
	addl	%esi, %ecx
	addl	%eax, %ecx
	leaq	_ZN2v88internalL26kPerfectKeywordLengthTableE(%rip), %rax
	andl	$63, %ecx
	movzbl	(%rax,%rcx), %edx
	movl	$92, %eax
	cmpl	%edx, %esi
	je	.L11
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	%rcx, %rax
	leaq	_ZN2v88internalL24kPerfectKeywordHashTableE(%rip), %r8
	salq	$4, %rax
	movq	(%r8,%rax), %rsi
	xorl	%eax, %eax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L13:
	addq	$1, %rax
	cmpb	-1(%rdi,%rax), %dl
	jne	.L12
.L8:
	movzbl	(%rsi,%rax), %edx
	testb	%dl, %dl
	jne	.L13
	salq	$4, %rcx
	movzbl	8(%r8,%rcx), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$92, %eax
	ret
	.cfi_endproc
.LFE21974:
	.size	_ZN2v88internal18PerfectKeywordHash8GetTokenEPKci.part.0, .-_ZN2v88internal18PerfectKeywordHash8GetTokenEPKci.part.0
	.section	.text._ZN2v88internal20Utf16CharacterStream4PeekEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0, @function
_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0:
.LFB21995:
	.cfi_startproc
	cmpb	$0, 48(%rdi)
	je	.L15
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	call	*40(%rax)
	testb	%al, %al
	je	.L17
	movq	16(%rbx), %rax
	movzwl	(%rax), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21995:
	.size	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0, .-_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	.section	.text._ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb.constprop.1,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb.constprop.1, @function
_ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb.constprop.1:
.LFB22016:
	.cfi_startproc
	movl	32(%rdi), %esi
	leal	-48(%rsi), %eax
	cmpl	$1, %eax
	ja	.L52
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
.L27:
	movq	8(%rbx), %r12
	cmpb	$0, 28(%r12)
	leaq	8(%r12), %rdi
	je	.L36
	movslq	24(%r12), %rax
	movl	%esi, %r13d
	cmpl	16(%r12), %eax
	jge	.L53
.L37:
	movq	8(%r12), %rdx
	movb	%r13b, (%rdx,%rax)
	addl	$1, 24(%r12)
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L39
.L41:
	movzwl	(%rax), %esi
.L40:
	addq	$2, %rax
	xorl	%edx, %edx
	movq	%rax, 16(%r12)
	leal	-48(%rsi), %eax
	movl	%esi, 32(%rbx)
	cmpl	$1, %eax
	jbe	.L27
.L56:
	cmpl	$95, %esi
	jne	.L54
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L30
.L50:
	movzwl	(%rax), %esi
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	%esi, 32(%rbx)
	cmpl	$95, %esi
	je	.L55
	leal	-48(%rsi), %eax
	movl	$1, %edx
	cmpl	$1, %eax
	ja	.L56
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L36:
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jb	.L41
.L39:
	cmpb	$0, 48(%r12)
	movl	$-1, %esi
	jne	.L40
	movq	(%r12), %rax
	movl	%esi, -36(%rbp)
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L41
	movl	-36(%rbp), %esi
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L55:
	movl	348(%rbx), %edx
	testl	%edx, %edx
	jne	.L51
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	movl	$214, 348(%rbx)
	leal	-1(%rax), %edx
	movl	%eax, 356(%rbx)
	xorl	%eax, %eax
	movl	%edx, 352(%rbx)
.L24:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r12), %rax
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L30:
	cmpb	$0, 48(%r12)
	je	.L32
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$-1, 32(%rbx)
.L29:
	movl	348(%rbx), %eax
	testl	%eax, %eax
	jne	.L51
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	movl	$213, 348(%rbx)
	leal	-1(%rax), %edx
	movl	%eax, 356(%rbx)
	movl	%edx, 352(%rbx)
.L51:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L33
	movq	16(%r12), %rax
	jmp	.L50
.L33:
	addq	$2, 16(%r12)
	movl	$-1, 32(%rbx)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L54:
	testb	%dl, %dl
	jne	.L29
	movl	$1, %eax
	jmp	.L24
	.cfi_endproc
.LFE22016:
	.size	_ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb.constprop.1, .-_ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb.constprop.1
	.section	.text._ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb.constprop.0, @function
_ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb.constprop.0:
.LFB22017:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	32(%rdi), %esi
	.p2align 4,,10
	.p2align 3
.L58:
	leal	-48(%rsi), %eax
	cmpl	$9, %eax
	jbe	.L59
.L83:
	cmpl	$95, %esi
	jne	.L81
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L63
.L79:
	movzwl	(%rax), %esi
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	%esi, 32(%rbx)
	cmpl	$95, %esi
	je	.L82
	leal	-48(%rsi), %eax
	movl	$1, %edx
	cmpl	$9, %eax
	ja	.L83
.L59:
	movq	8(%rbx), %r12
	cmpb	$0, 28(%r12)
	leaq	8(%r12), %rdi
	je	.L69
	movslq	24(%r12), %rax
	movl	%esi, %r13d
	cmpl	16(%r12), %eax
	jge	.L84
.L70:
	movq	8(%r12), %rdx
	movb	%r13b, (%rdx,%rax)
	addl	$1, 24(%r12)
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L72
.L74:
	movzwl	(%rax), %esi
.L73:
	addq	$2, %rax
	xorl	%edx, %edx
	movq	%rax, 16(%r12)
	movl	%esi, 32(%rbx)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L69:
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jb	.L74
.L72:
	cmpb	$0, 48(%r12)
	movl	$-1, %esi
	jne	.L73
	movq	(%r12), %rax
	movl	%esi, -36(%rbp)
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L74
	movl	-36(%rbp), %esi
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L82:
	movl	348(%rbx), %edx
	testl	%edx, %edx
	jne	.L80
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	movl	$214, 348(%rbx)
	leal	-1(%rax), %edx
	movl	%eax, 356(%rbx)
	xorl	%eax, %eax
	movl	%edx, 352(%rbx)
.L57:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	cmpb	$0, 48(%r12)
	je	.L65
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$-1, 32(%rbx)
.L61:
	movl	348(%rbx), %eax
	testl	%eax, %eax
	jne	.L80
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	movl	$213, 348(%rbx)
	leal	-1(%rax), %edx
	movl	%eax, 356(%rbx)
	movl	%edx, 352(%rbx)
.L80:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r12), %rax
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L65:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L85
	addq	$2, 16(%r12)
	movl	$-1, 32(%rbx)
	jmp	.L61
.L85:
	movq	16(%r12), %rax
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L81:
	testb	%dl, %dl
	jne	.L61
	movl	$1, %eax
	jmp	.L57
	.cfi_endproc
.LFE22017:
	.size	_ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb.constprop.0, .-_ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb.constprop.0
	.section	.text._ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb.constprop.2,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb.constprop.2, @function
_ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb.constprop.2:
.LFB22015:
	.cfi_startproc
	movl	32(%rdi), %esi
	leal	-48(%rsi), %eax
	cmpl	$7, %eax
	ja	.L114
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
.L89:
	movq	8(%rbx), %r12
	cmpb	$0, 28(%r12)
	leaq	8(%r12), %rdi
	je	.L98
	movslq	24(%r12), %rax
	movl	%esi, %r13d
	cmpl	16(%r12), %eax
	jge	.L115
.L99:
	movq	8(%r12), %rdx
	movb	%r13b, (%rdx,%rax)
	addl	$1, 24(%r12)
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L101
.L103:
	movzwl	(%rax), %esi
.L102:
	addq	$2, %rax
	xorl	%edx, %edx
	movq	%rax, 16(%r12)
	leal	-48(%rsi), %eax
	movl	%esi, 32(%rbx)
	cmpl	$7, %eax
	jbe	.L89
.L118:
	cmpl	$95, %esi
	jne	.L116
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L92
.L112:
	movzwl	(%rax), %esi
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	%esi, 32(%rbx)
	cmpl	$95, %esi
	je	.L117
	leal	-48(%rsi), %eax
	movl	$1, %edx
	cmpl	$7, %eax
	ja	.L118
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L98:
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jb	.L103
.L101:
	cmpb	$0, 48(%r12)
	movl	$-1, %esi
	jne	.L102
	movq	(%r12), %rax
	movl	%esi, -36(%rbp)
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L103
	movl	-36(%rbp), %esi
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L117:
	movl	348(%rbx), %edx
	testl	%edx, %edx
	jne	.L113
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	movl	$214, 348(%rbx)
	leal	-1(%rax), %edx
	movl	%eax, 356(%rbx)
	xorl	%eax, %eax
	movl	%edx, 352(%rbx)
.L86:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r12), %rax
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L92:
	cmpb	$0, 48(%r12)
	je	.L94
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$-1, 32(%rbx)
.L91:
	movl	348(%rbx), %eax
	testl	%eax, %eax
	jne	.L113
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	movl	$213, 348(%rbx)
	leal	-1(%rax), %edx
	movl	%eax, 356(%rbx)
	movl	%edx, 352(%rbx)
.L113:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L95
	movq	16(%r12), %rax
	jmp	.L112
.L95:
	addq	$2, 16(%r12)
	movl	$-1, 32(%rbx)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L116:
	testb	%dl, %dl
	jne	.L91
	movl	$1, %eax
	jmp	.L86
	.cfi_endproc
.LFE22015:
	.size	_ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb.constprop.2, .-_ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb.constprop.2
	.section	.text._ZN2v88internal20Utf16CharacterStream4BackEv,"axG",@progbits,_ZN2v88internal20Utf16CharacterStream4BackEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20Utf16CharacterStream4BackEv
	.type	_ZN2v88internal20Utf16CharacterStream4BackEv, @function
_ZN2v88internal20Utf16CharacterStream4BackEv:
.LFB5078:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	8(%rdi), %rdx
	cmpq	%rdx, %rax
	jbe	.L120
	subq	$2, %rax
	movq	%rax, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	movq	32(%rdi), %rcx
	subq	%rdx, %rax
	movq	%rdx, 16(%rdi)
	sarq	%rax
	cmpb	$0, 48(%rdi)
	leaq	-1(%rcx,%rax), %rax
	movq	%rax, 32(%rdi)
	jne	.L119
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L119:
	ret
	.cfi_endproc
.LFE5078:
	.size	_ZN2v88internal20Utf16CharacterStream4BackEv, .-_ZN2v88internal20Utf16CharacterStream4BackEv
	.section	.text._ZN2v88internal7Scanner6SelectENS0_5Token5ValueE,"axG",@progbits,_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	.type	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE, @function
_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE:
.LFB5151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L123
.L125:
	movzwl	(%rax), %r14d
.L124:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	%esi, %eax
	movl	%r14d, 32(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	cmpb	$0, 48(%r12)
	movl	$-1, %r14d
	jne	.L124
	movq	(%r12), %rax
	movl	%esi, -36(%rbp)
	movq	%r12, %rdi
	call	*40(%rax)
	movl	-36(%rbp), %esi
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L125
	jmp	.L124
	.cfi_endproc
.LFE5151:
	.size	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE, .-_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	.section	.text._ZN2v88internal7Scanner6SelectEiNS0_5Token5ValueES3_,"axG",@progbits,_ZN2v88internal7Scanner6SelectEiNS0_5Token5ValueES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Scanner6SelectEiNS0_5Token5ValueES3_
	.type	_ZN2v88internal7Scanner6SelectEiNS0_5Token5ValueES3_, @function
_ZN2v88internal7Scanner6SelectEiNS0_5Token5ValueES3_:
.LFB5152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	24(%rdi), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L129
.L131:
	movzwl	(%rax), %r15d
.L130:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%r15d, 32(%rbx)
	cmpl	%r15d, %esi
	je	.L139
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L133
.L135:
	movzwl	(%rax), %r13d
.L134:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	%edx, %r12d
	movl	%r13d, 32(%rbx)
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	cmpb	$0, 48(%r13)
	movl	$-1, %r15d
	jne	.L130
	movq	0(%r13), %rax
	movl	%edx, -40(%rbp)
	movq	%r13, %rdi
	movl	%esi, -36(%rbp)
	call	*40(%rax)
	movl	-36(%rbp), %esi
	movl	-40(%rbp), %edx
	testb	%al, %al
	movq	16(%r13), %rax
	jne	.L131
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L133:
	cmpb	$0, 48(%r12)
	movl	$-1, %r13d
	jne	.L134
	movq	(%r12), %rax
	movl	%edx, -36(%rbp)
	movq	%r12, %rdi
	call	*40(%rax)
	movl	-36(%rbp), %edx
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L135
	jmp	.L134
	.cfi_endproc
.LFE5152:
	.size	_ZN2v88internal7Scanner6SelectEiNS0_5Token5ValueES3_, .-_ZN2v88internal7Scanner6SelectEiNS0_5Token5ValueES3_
	.section	.text._ZN2v88internal7Scanner13BookmarkScope3SetEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner13BookmarkScope3SetEm
	.type	_ZN2v88internal7Scanner13BookmarkScope3SetEm, @function
_ZN2v88internal7Scanner13BookmarkScope3SetEm:
.LFB18103:
	.cfi_startproc
	endbr64
	movq	%rsi, 8(%rdi)
	ret
	.cfi_endproc
.LFE18103:
	.size	_ZN2v88internal7Scanner13BookmarkScope3SetEm, .-_ZN2v88internal7Scanner13BookmarkScope3SetEm
	.section	.text._ZNK2v88internal7Scanner13BookmarkScope10HasBeenSetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal7Scanner13BookmarkScope10HasBeenSetEv
	.type	_ZNK2v88internal7Scanner13BookmarkScope10HasBeenSetEv, @function
_ZNK2v88internal7Scanner13BookmarkScope10HasBeenSetEv:
.LFB18105:
	.cfi_startproc
	endbr64
	cmpq	$-3, 8(%rdi)
	setbe	%al
	ret
	.cfi_endproc
.LFE18105:
	.size	_ZNK2v88internal7Scanner13BookmarkScope10HasBeenSetEv, .-_ZNK2v88internal7Scanner13BookmarkScope10HasBeenSetEv
	.section	.text._ZNK2v88internal7Scanner13BookmarkScope14HasBeenAppliedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal7Scanner13BookmarkScope14HasBeenAppliedEv
	.type	_ZNK2v88internal7Scanner13BookmarkScope14HasBeenAppliedEv, @function
_ZNK2v88internal7Scanner13BookmarkScope14HasBeenAppliedEv:
.LFB18106:
	.cfi_startproc
	endbr64
	cmpq	$-1, 8(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE18106:
	.size	_ZNK2v88internal7Scanner13BookmarkScope14HasBeenAppliedEv, .-_ZNK2v88internal7Scanner13BookmarkScope14HasBeenAppliedEv
	.section	.text._ZN2v88internal7ScannerC2EPNS0_20Utf16CharacterStreamEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7ScannerC2EPNS0_20Utf16CharacterStreamEb
	.type	_ZN2v88internal7ScannerC2EPNS0_20Utf16CharacterStreamEb, @function
_ZN2v88internal7ScannerC2EPNS0_20Utf16CharacterStreamEb:
.LFB18114:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	movq	%rsi, 24(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 56(%rdi)
	movl	$0, 64(%rdi)
	movb	$1, 68(%rdi)
	movq	$0, 72(%rdi)
	movq	$0, 80(%rdi)
	movl	$0, 88(%rdi)
	movb	$1, 92(%rdi)
	movb	$112, 96(%rdi)
	movq	$0, 100(%rdi)
	movq	$0, 108(%rdi)
	movb	$0, 116(%rdi)
	movq	$0, 120(%rdi)
	movq	$0, 128(%rdi)
	movq	$0, 136(%rdi)
	movl	$0, 144(%rdi)
	movb	$1, 148(%rdi)
	movq	$0, 152(%rdi)
	movq	$0, 160(%rdi)
	movl	$0, 168(%rdi)
	movb	$1, 172(%rdi)
	movb	$112, 176(%rdi)
	movq	$0, 180(%rdi)
	movq	$0, 188(%rdi)
	movb	$0, 196(%rdi)
	movq	$0, 200(%rdi)
	movq	$0, 208(%rdi)
	movq	$0, 216(%rdi)
	movl	$0, 224(%rdi)
	movb	$1, 228(%rdi)
	movq	$0, 232(%rdi)
	movw	%ax, 280(%rdi)
	movl	$4294967295, %eax
	movq	$0, 240(%rdi)
	movl	$0, 248(%rdi)
	movb	$1, 252(%rdi)
	movb	$112, 256(%rdi)
	movq	$0, 260(%rdi)
	movq	$0, 268(%rdi)
	movb	$0, 276(%rdi)
	movb	$0, 282(%rdi)
	movb	%dl, 283(%rdi)
	movq	$0, 288(%rdi)
	movq	$0, 296(%rdi)
	movl	$0, 304(%rdi)
	movb	$1, 308(%rdi)
	movq	$0, 312(%rdi)
	movq	$0, 320(%rdi)
	movl	$0, 328(%rdi)
	movb	$1, 332(%rdi)
	movq	%rax, 336(%rdi)
	movl	$0, 344(%rdi)
	movq	$0, 352(%rdi)
	ret
	.cfi_endproc
.LFE18114:
	.size	_ZN2v88internal7ScannerC2EPNS0_20Utf16CharacterStreamEb, .-_ZN2v88internal7ScannerC2EPNS0_20Utf16CharacterStreamEb
	.globl	_ZN2v88internal7ScannerC1EPNS0_20Utf16CharacterStreamEb
	.set	_ZN2v88internal7ScannerC1EPNS0_20Utf16CharacterStreamEb,_ZN2v88internal7ScannerC2EPNS0_20Utf16CharacterStreamEb
	.section	.text._ZN2v88internal7Scanner21SkipSingleLineCommentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner21SkipSingleLineCommentEv
	.type	_ZN2v88internal7Scanner21SkipSingleLineCommentEv, @function
_ZN2v88internal7Scanner21SkipSingleLineCommentEv:
.LFB18122:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	24(%rdi), %rbx
.L151:
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rax
	movq	%rcx, %rdx
	subq	%rax, %rdx
	movq	%rdx, %rsi
	sarq	$3, %rdx
	sarq	%rsi
	testq	%rdx, %rdx
	jle	.L145
	leaq	(%rax,%rdx,8), %rsi
.L147:
	movzwl	(%rax), %edx
	cmpl	$10, %edx
	je	.L146
	cmpl	$13, %edx
	je	.L146
	subl	$8232, %edx
	cmpl	$1, %edx
	jbe	.L146
	movzwl	2(%rax), %edx
	leaq	2(%rax), %rdi
	cmpl	$10, %edx
	je	.L161
	cmpl	$13, %edx
	je	.L161
	subl	$8232, %edx
	cmpl	$1, %edx
	jbe	.L161
	movzwl	4(%rax), %edx
	leaq	4(%rax), %rdi
	cmpl	$10, %edx
	je	.L161
	cmpl	$13, %edx
	je	.L161
	subl	$8232, %edx
	cmpl	$1, %edx
	jbe	.L161
	movzwl	6(%rax), %edx
	leaq	6(%rax), %rdi
	cmpl	$10, %edx
	je	.L161
	cmpl	$13, %edx
	je	.L161
	subl	$8232, %edx
	cmpl	$1, %edx
	jbe	.L161
	addq	$8, %rax
	cmpq	%rax, %rsi
	jne	.L147
	movq	%rcx, %rsi
	subq	%rax, %rsi
	sarq	%rsi
	.p2align 4,,10
	.p2align 3
.L145:
	cmpq	$2, %rsi
	je	.L153
	cmpq	$3, %rsi
	je	.L154
	cmpq	$1, %rsi
	jne	.L148
.L155:
	movzwl	(%rax), %edx
	cmpl	$10, %edx
	je	.L146
	cmpl	$13, %edx
	je	.L146
	subl	$8232, %edx
	cmpl	$1, %edx
	ja	.L148
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L161:
	movq	%rdi, %rax
.L146:
	cmpq	%rax, %rcx
	jne	.L149
.L148:
	cmpb	$0, 48(%rbx)
	movq	%rcx, 16(%rbx)
	je	.L172
.L150:
	movl	$-1, %eax
	addq	$2, %rcx
	movq	%rcx, 16(%rbx)
	popq	%rbx
	movl	%eax, 32(%r12)
	movl	$111, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L151
	movq	16(%rbx), %rcx
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L149:
	leaq	2(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movzwl	(%rax), %eax
	popq	%rbx
	movl	%eax, 32(%r12)
	movl	$111, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L154:
	.cfi_restore_state
	movzwl	(%rax), %edx
	cmpl	$10, %edx
	je	.L146
	cmpl	$13, %edx
	je	.L146
	subl	$8232, %edx
	cmpl	$1, %edx
	jbe	.L146
	addq	$2, %rax
.L153:
	movzwl	(%rax), %edx
	cmpl	$10, %edx
	je	.L146
	cmpl	$13, %edx
	je	.L146
	subl	$8232, %edx
	cmpl	$1, %edx
	jbe	.L146
	addq	$2, %rax
	jmp	.L155
	.cfi_endproc
.LFE18122:
	.size	_ZN2v88internal7Scanner21SkipSingleLineCommentEv, .-_ZN2v88internal7Scanner21SkipSingleLineCommentEv
	.section	.text._ZN2v88internal7Scanner21SkipSingleHTMLCommentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner21SkipSingleHTMLCommentEv
	.type	_ZN2v88internal7Scanner21SkipSingleHTMLCommentEv, @function
_ZN2v88internal7Scanner21SkipSingleHTMLCommentEv:
.LFB18121:
	.cfi_startproc
	endbr64
	cmpb	$0, 283(%rdi)
	jne	.L177
	jmp	_ZN2v88internal7Scanner21SkipSingleLineCommentEv
	.p2align 4,,10
	.p2align 3
.L177:
	movl	348(%rdi), %eax
	testl	%eax, %eax
	jne	.L173
	movq	24(%rdi), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	movl	$275, 348(%rdi)
	leal	-1(%rax), %edx
	movl	%eax, 356(%rdi)
	movl	%edx, 352(%rdi)
.L173:
	movl	$109, %eax
	ret
	.cfi_endproc
.LFE18121:
	.size	_ZN2v88internal7Scanner21SkipSingleHTMLCommentEv, .-_ZN2v88internal7Scanner21SkipSingleHTMLCommentEv
	.section	.rodata._ZN2v88internal7Scanner26TryToParseSourceURLCommentEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"sourceURL"
.LC1:
	.string	"sourceMappingURL"
	.section	.text._ZN2v88internal7Scanner26TryToParseSourceURLCommentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner26TryToParseSourceURLCommentEv
	.type	_ZN2v88internal7Scanner26TryToParseSourceURLCommentEv, @function
_ZN2v88internal7Scanner26TryToParseSourceURLCommentEv:
.LFB18127:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movslq	32(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$127, %edi
	ja	.L274
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rax
	movzbl	(%rax,%rdi), %eax
	shrb	$2, %al
	andl	$1, %eax
.L180:
	testb	%al, %al
	je	.L178
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L183
.L186:
	movzwl	(%rax), %r12d
	addq	$2, %rax
	movq	$0, -80(%rbp)
	movq	%rax, 16(%r13)
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r13
	movl	%r12d, 32(%rbx)
	movq	$0, -72(%rbp)
	movl	$0, -64(%rbp)
	movb	$1, -60(%rbp)
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L187:
	movslq	%r12d, %rax
	testb	$8, 0(%r13,%rax)
	jne	.L202
.L189:
	movzbl	-60(%rbp), %eax
	cmpl	$61, %r12d
	je	.L190
	leaq	-80(%rbp), %r14
	testb	%al, %al
	je	.L192
	cmpl	$255, %r12d
	jle	.L275
	leaq	-80(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
.L192:
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L198:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L199
.L271:
	movzwl	(%rax), %r12d
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%r12d, 32(%rbx)
.L204:
	cmpl	$127, %r12d
	jle	.L187
	movl	%r12d, %edi
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	testb	%al, %al
	jne	.L202
	subl	$8232, %r12d
	cmpl	$1, %r12d
	jbe	.L202
	movl	32(%rbx), %r12d
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L199:
	cmpb	$0, 48(%r14)
	je	.L201
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	$-1, 32(%rbx)
	.p2align 4,,10
	.p2align 3
.L202:
	movzbl	-60(%rbp), %eax
.L190:
	movq	-80(%rbp), %rdi
	testb	%al, %al
	je	.L193
	movslq	-64(%rbp), %rax
	cmpq	$9, %rax
	je	.L276
	cmpq	$16, %rax
	jne	.L193
	leaq	.LC1(%rip), %rax
	cmpq	%rax, %rdi
	je	.L208
	cmpb	$115, (%rdi)
	jne	.L206
	cmpb	$111, 1(%rdi)
	jne	.L206
	cmpb	$117, 2(%rdi)
	jne	.L206
	cmpb	$114, 3(%rdi)
	jne	.L206
	cmpb	$99, 4(%rdi)
	jne	.L206
	cmpb	$101, 5(%rdi)
	jne	.L206
	cmpb	$77, 6(%rdi)
	jne	.L206
	cmpb	$97, 7(%rdi)
	jne	.L206
	cmpb	$112, 8(%rdi)
	jne	.L206
	cmpb	$112, 9(%rdi)
	jne	.L206
	cmpb	$105, 10(%rdi)
	jne	.L206
	cmpb	$110, 11(%rdi)
	jne	.L206
	cmpb	$103, 12(%rdi)
	jne	.L206
	cmpb	$85, 13(%rdi)
	jne	.L206
	cmpb	$82, 14(%rdi)
	jne	.L206
	cmpb	$76, 15(%rdi)
	jne	.L206
.L208:
	cmpl	$61, 32(%rbx)
	leaq	312(%rbx), %r13
	je	.L277
	.p2align 4,,10
	.p2align 3
.L193:
	testq	%rdi, %rdi
	jne	.L206
.L178:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L278
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L275:
	.cfi_restore_state
	movslq	-64(%rbp), %rax
	cmpl	-72(%rbp), %eax
	jge	.L279
.L197:
	movq	-80(%rbp), %rdx
	movb	%r12b, (%rdx,%rax)
	addl	$1, -64(%rbp)
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L201:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L280
	movq	16(%r14), %rax
	jmp	.L271
.L241:
	cmpb	$0, 48(%r14)
	je	.L243
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	$-1, 32(%rbx)
	.p2align 4,,10
	.p2align 3
.L240:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L178
	.p2align 4,,10
	.p2align 3
.L206:
	call	_ZdaPv@PLT
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L279:
	leaq	-80(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	-64(%rbp), %rax
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L274:
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L276:
	leaq	.LC0(%rip), %rax
	cmpq	%rax, %rdi
	je	.L205
	cmpb	$115, (%rdi)
	jne	.L206
	cmpb	$111, 1(%rdi)
	jne	.L206
	cmpb	$117, 2(%rdi)
	jne	.L206
	cmpb	$114, 3(%rdi)
	jne	.L206
	cmpb	$99, 4(%rdi)
	jne	.L206
	cmpb	$101, 5(%rdi)
	jne	.L206
	cmpb	$85, 6(%rdi)
	jne	.L206
	cmpb	$82, 7(%rdi)
	jne	.L206
	cmpb	$76, 8(%rdi)
	jne	.L206
.L205:
	cmpl	$61, 32(%rbx)
	leaq	288(%rbx), %r13
	jne	.L193
.L277:
	movl	$0, 16(%r13)
	movb	$1, 20(%r13)
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L209
.L211:
	movzwl	(%rax), %r12d
.L210:
	addq	$2, %rax
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r15
	movq	%rax, 16(%r14)
	movl	%r12d, 32(%rbx)
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L212:
	movzbl	(%r15,%r12), %eax
	shrb	$2, %al
	andl	$1, %eax
	testb	%al, %al
	je	.L214
.L281:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L215
.L217:
	movzwl	(%rax), %r12d
.L216:
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%r12d, 32(%rbx)
.L218:
	cmpl	$127, %r12d
	jbe	.L212
	movl	%r12d, %edi
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	testb	%al, %al
	jne	.L281
.L214:
	movslq	32(%rbx), %r12
	cmpl	$-1, %r12d
	je	.L240
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r14
.L236:
	cmpl	$10, %r12d
	je	.L221
	cmpl	$13, %r12d
	je	.L221
	leal	-8232(%r12), %eax
	cmpl	$1, %eax
	jbe	.L221
	cmpl	$34, %r12d
	je	.L250
	cmpl	$39, %r12d
	je	.L250
	cmpl	$127, %r12d
	ja	.L282
	movslq	%r12d, %rax
	movzbl	(%r14,%rax), %eax
	shrb	$2, %al
	andl	$1, %eax
.L225:
	testb	%al, %al
	jne	.L283
	cmpb	$0, 20(%r13)
	je	.L228
	cmpl	$255, %r12d
	jle	.L284
	movq	%r13, %rdi
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
.L228:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L231:
	movq	24(%rbx), %r15
	movq	16(%r15), %rax
	cmpq	24(%r15), %rax
	jnb	.L232
.L272:
	movzwl	(%rax), %r12d
	addq	$2, %rax
	movq	%rax, 16(%r15)
	movl	%r12d, 32(%rbx)
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L280:
	addq	$2, 16(%r14)
	movl	$-1, 32(%rbx)
	jmp	.L202
.L283:
	cmpl	$-1, %r12d
	je	.L240
	.p2align 4,,10
	.p2align 3
.L221:
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r15
.L245:
	cmpl	$10, %r12d
	je	.L240
	cmpl	$13, %r12d
	je	.L240
	leal	-8232(%r12), %eax
	cmpl	$1, %eax
	jbe	.L240
	cmpl	$127, %r12d
	ja	.L285
	movzbl	(%r15,%r12), %eax
	shrb	$2, %al
	andl	$1, %eax
.L238:
	testb	%al, %al
	je	.L286
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L241
.L273:
	movzwl	(%rax), %r12d
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%r12d, 32(%rbx)
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L215:
	cmpb	$0, 48(%r14)
	movq	$-1, %r12
	jne	.L216
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r14), %rax
	jne	.L217
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L183:
	cmpb	$0, 48(%r13)
	jne	.L185
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r13), %rax
	jne	.L186
.L185:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	$-1, 32(%rbx)
	jmp	.L178
.L282:
	movl	%r12d, %edi
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	movslq	32(%rbx), %r12
	jmp	.L225
.L285:
	movl	%r12d, %edi
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	jmp	.L238
.L209:
	cmpb	$0, 48(%r14)
	movq	$-1, %r12
	jne	.L210
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r14), %rax
	jne	.L211
	jmp	.L210
.L284:
	movslq	16(%r13), %rax
	cmpl	8(%r13), %eax
	jge	.L287
.L230:
	movq	0(%r13), %rdx
	movb	%r12b, (%rdx,%rax)
	addl	$1, 16(%r13)
	jmp	.L231
.L232:
	cmpb	$0, 48(%r15)
	je	.L234
	addq	$2, %rax
	movq	%rax, 16(%r15)
	movl	$-1, 32(%rbx)
	jmp	.L240
.L287:
	movq	%r13, %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	16(%r13), %rax
	jmp	.L230
.L243:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L244
	addq	$2, 16(%r14)
	movl	$-1, 32(%rbx)
	jmp	.L240
.L234:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L235
	addq	$2, 16(%r15)
	movl	$-1, 32(%rbx)
	jmp	.L240
.L244:
	movq	16(%r14), %rax
	jmp	.L273
.L235:
	movq	16(%r15), %rax
	jmp	.L272
.L250:
	movl	$0, 16(%r13)
	movq	-80(%rbp), %rdi
	movb	$1, 20(%r13)
	jmp	.L193
.L286:
	movl	$0, 16(%r13)
	movb	$1, 20(%r13)
	jmp	.L240
.L278:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18127:
	.size	_ZN2v88internal7Scanner26TryToParseSourceURLCommentEv, .-_ZN2v88internal7Scanner26TryToParseSourceURLCommentEv
	.section	.text._ZN2v88internal7Scanner20SkipSourceURLCommentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner20SkipSourceURLCommentEv
	.type	_ZN2v88internal7Scanner20SkipSourceURLCommentEv, @function
_ZN2v88internal7Scanner20SkipSourceURLCommentEv:
.LFB18126:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal7Scanner26TryToParseSourceURLCommentEv
	movl	32(%r12), %eax
	xorl	%edx, %edx
	leal	1(%rax), %ecx
	cmpl	$14, %ecx
	ja	.L289
	movl	$18433, %edx
	shrq	%cl, %rdx
	andl	$1, %edx
.L289:
	subl	$8232, %eax
	cmpl	$1, %eax
	jbe	.L290
	testb	%dl, %dl
	jne	.L290
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Scanner21SkipSingleLineCommentEv
	.p2align 4,,10
	.p2align 3
.L290:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$111, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18126:
	.size	_ZN2v88internal7Scanner20SkipSourceURLCommentEv, .-_ZN2v88internal7Scanner20SkipSourceURLCommentEv
	.section	.text._ZN2v88internal7Scanner20SkipMultiLineCommentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner20SkipMultiLineCommentEv
	.type	_ZN2v88internal7Scanner20SkipMultiLineCommentEv, @function
_ZN2v88internal7Scanner20SkipMultiLineCommentEv:
.LFB18133:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %rax
	cmpb	$0, 76(%rax)
	jne	.L401
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %r13
.L295:
	movq	24(%r12), %rbx
.L315:
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rax
	movq	%rcx, %rdx
	subq	%rax, %rdx
	movq	%rdx, %rsi
	sarq	$3, %rdx
	sarq	%rsi
	testq	%rdx, %rdx
	jle	.L297
	leaq	(%rax,%rdx,8), %rsi
.L304:
	movzwl	(%rax), %edx
	cmpl	$127, %edx
	ja	.L410
	testb	$32, 0(%r13,%rdx)
	jne	.L299
.L300:
	movzwl	2(%rax), %edx
	leaq	2(%rax), %rdi
	cmpl	$127, %edx
	ja	.L411
	testb	$32, 0(%r13,%rdx)
	jne	.L371
.L301:
	movzwl	4(%rax), %edx
	leaq	4(%rax), %rdi
	cmpl	$127, %edx
	ja	.L412
	testb	$32, 0(%r13,%rdx)
	jne	.L371
.L302:
	movzwl	6(%rax), %edx
	leaq	6(%rax), %rdi
	cmpl	$127, %edx
	ja	.L413
	testb	$32, 0(%r13,%rdx)
	jne	.L371
.L303:
	addq	$8, %rax
	cmpq	%rax, %rsi
	jne	.L304
	movq	%rcx, %rsi
	subq	%rax, %rsi
	sarq	%rsi
.L297:
	cmpq	$2, %rsi
	je	.L362
	cmpq	$3, %rsi
	je	.L363
	cmpq	$1, %rsi
	jne	.L312
	movzwl	(%rax), %edx
	movq	%rax, %rdi
	movl	%edx, %esi
	cmpl	$127, %edx
	ja	.L310
.L311:
	testb	$32, 0(%r13,%rdx)
	je	.L312
	.p2align 4,,10
	.p2align 3
.L371:
	movq	%rdi, %rax
.L299:
	cmpq	%rax, %rcx
	jne	.L313
.L312:
	cmpb	$0, 48(%rbx)
	movq	%rcx, 16(%rbx)
	jne	.L314
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L315
.L405:
	movq	16(%rbx), %rcx
.L314:
	addq	$2, %rcx
	movq	%rcx, 16(%rbx)
	movl	$-1, 32(%r12)
.L330:
	addq	$8, %rsp
	movl	$109, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L316:
	.cfi_restore_state
	cmpl	$10, %edx
	je	.L328
	cmpl	$13, %edx
	jne	.L395
.L328:
	movq	8(%r12), %rax
	movb	$1, 76(%rax)
.L401:
	movl	32(%r12), %eax
	cmpl	$-1, %eax
	je	.L330
.L331:
	movq	24(%r12), %rbx
.L341:
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rax
	movq	%rcx, %rdx
	subq	%rax, %rdx
	movq	%rdx, %rsi
	sarq	$3, %rdx
	sarq	%rsi
	testq	%rdx, %rdx
	jle	.L332
	leaq	(%rax,%rdx,8), %rdx
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L418:
	cmpw	$42, 2(%rax)
	je	.L414
	cmpw	$42, 4(%rax)
	je	.L415
	cmpw	$42, 6(%rax)
	je	.L416
	addq	$8, %rax
	cmpq	%rax, %rdx
	je	.L417
.L337:
	cmpw	$42, (%rax)
	jne	.L418
.L333:
	cmpq	%rax, %rcx
	jne	.L339
.L338:
	cmpb	$0, 48(%rbx)
	movq	%rcx, 16(%rbx)
	jne	.L314
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L341
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L417:
	movq	%rcx, %rsi
	subq	%rax, %rsi
	sarq	%rsi
.L332:
	cmpq	$2, %rsi
	je	.L359
	cmpq	$3, %rsi
	je	.L360
	cmpq	$1, %rsi
	jne	.L338
	cmpw	$42, (%rax)
	jne	.L338
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L414:
	addq	$2, %rax
	cmpq	%rax, %rcx
	je	.L338
.L339:
	leaq	2(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movzwl	(%rax), %eax
	movl	%eax, 32(%r12)
	cmpl	$42, %eax
	je	.L343
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L404:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	movl	%edx, 32(%r12)
	cmpl	$47, %edx
	je	.L409
	cmpl	$42, %edx
	jne	.L331
.L343:
	movq	24(%r12), %rbx
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jb	.L404
	cmpb	$0, 48(%rbx)
	jne	.L403
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L348
.L402:
	addq	$2, 16(%rbx)
	movl	$-1, 32(%r12)
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L415:
	addq	$4, %rax
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L416:
	addq	$6, %rax
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L360:
	cmpw	$42, (%rax)
	je	.L333
	addq	$2, %rax
.L359:
	cmpw	$42, (%rax)
	je	.L333
	addq	$2, %rax
	cmpw	$42, (%rax)
	jne	.L338
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L410:
	subl	$8232, %edx
	cmpl	$1, %edx
	ja	.L300
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L411:
	subl	$8232, %edx
	cmpl	$1, %edx
	ja	.L301
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L412:
	subl	$8232, %edx
	cmpl	$1, %edx
	ja	.L302
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L409:
	movq	24(%r12), %rbx
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jnb	.L350
.L352:
	movzwl	(%rax), %r13d
.L351:
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	movl	$111, %eax
	movl	%r13d, 32(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L403:
	.cfi_restore_state
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	movl	$-1, 32(%r12)
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L348:
	movq	16(%rbx), %rax
	jmp	.L404
.L413:
	subl	$8232, %edx
	cmpl	$1, %edx
	ja	.L303
	jmp	.L371
.L350:
	cmpb	$0, 48(%rbx)
	movl	$-1, %r13d
	jne	.L351
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%rbx), %rax
	jne	.L352
	jmp	.L351
.L313:
	leaq	2(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movzwl	(%rax), %edx
	movl	%edx, 32(%r12)
	cmpl	$42, %edx
	je	.L317
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L400:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	movl	%edx, 32(%r12)
	cmpl	$47, %edx
	je	.L409
	cmpl	$42, %edx
	jne	.L316
.L317:
	movq	24(%r12), %rbx
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jb	.L400
	cmpb	$0, 48(%rbx)
	jne	.L403
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L402
	movq	16(%rbx), %rax
	jmp	.L400
.L363:
	movzwl	(%rax), %edx
	cmpl	$127, %edx
	ja	.L419
	testb	$32, 0(%r13,%rdx)
	jne	.L299
.L306:
	movzwl	2(%rax), %edx
	leaq	2(%rax), %rsi
	movq	%rsi, %rax
	cmpl	$127, %edx
	ja	.L307
.L308:
	testb	$32, 0(%r13,%rdx)
	jne	.L299
.L309:
	movzwl	2(%rax), %esi
	leaq	2(%rax), %rdi
	movslq	%esi, %rdx
	cmpl	$127, %esi
	jbe	.L311
	movq	%rdi, %rax
.L310:
	subl	$8232, %esi
	cmpl	$1, %esi
	ja	.L312
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L362:
	movzwl	(%rax), %edx
	cmpl	$127, %edx
	jbe	.L308
.L307:
	subl	$8232, %edx
	cmpl	$1, %edx
	ja	.L309
	jmp	.L299
.L419:
	subl	$8232, %edx
	cmpl	$1, %edx
	ja	.L306
	jmp	.L299
.L395:
	subl	$8232, %edx
	cmpl	$1, %edx
	ja	.L295
	jmp	.L328
	.cfi_endproc
.LFE18133:
	.size	_ZN2v88internal7Scanner20SkipMultiLineCommentEv, .-_ZN2v88internal7Scanner20SkipMultiLineCommentEv
	.section	.text._ZN2v88internal7Scanner15ScanHtmlCommentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner15ScanHtmlCommentEv
	.type	_ZN2v88internal7Scanner15ScanHtmlCommentEv, @function
_ZN2v88internal7Scanner15ScanHtmlCommentEv:
.LFB18140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	24(%rdi), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L421
.L441:
	movzwl	(%rax), %ebx
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movq	24(%r12), %r13
	movl	%ebx, 32(%r12)
	movq	16(%r13), %rax
	cmpl	$45, %ebx
	jne	.L424
	cmpq	24(%r13), %rax
	jnb	.L427
	cmpw	$45, (%rax)
	jne	.L424
.L435:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	cmpb	$0, 283(%r12)
	movl	%ebx, 32(%r12)
	movb	$1, 280(%r12)
	jne	.L442
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Scanner21SkipSingleLineCommentEv
	.p2align 4,,10
	.p2align 3
.L421:
	.cfi_restore_state
	cmpb	$0, 48(%r13)
	je	.L423
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movq	24(%rdi), %r13
	movl	$-1, 32(%rdi)
	movq	16(%r13), %rax
	.p2align 4,,10
	.p2align 3
.L424:
	movq	8(%r13), %rdx
	cmpq	%rax, %rdx
	jnb	.L431
	subq	$2, %rax
	movq	%rax, 16(%r13)
.L432:
	movl	$33, 32(%r12)
	movl	$57, %eax
.L420:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L442:
	.cfi_restore_state
	movl	348(%r12), %edx
	movl	$109, %eax
	testl	%edx, %edx
	jne	.L420
	movq	24(%r12), %rcx
	movq	16(%rcx), %rdx
	subq	8(%rcx), %rdx
	sarq	%rdx
	addq	32(%rcx), %rdx
	movl	$275, 348(%r12)
	leal	-1(%rdx), %ecx
	movl	%edx, 356(%r12)
	movl	%ecx, 352(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L427:
	.cfi_restore_state
	cmpb	$0, 48(%r13)
	jne	.L424
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L429
	movq	24(%r12), %r13
	movq	16(%r13), %rax
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L423:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L425
	movq	16(%r13), %rax
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L425:
	addq	$2, 16(%r13)
	movq	24(%r12), %r13
	movl	$-1, 32(%r12)
	movq	16(%r13), %rax
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L431:
	movq	32(%r13), %rcx
	subq	%rdx, %rax
	movq	%rdx, 16(%r13)
	sarq	%rax
	cmpb	$0, 48(%r13)
	leaq	-1(%rax,%rcx), %rax
	movq	%rax, 32(%r13)
	jne	.L432
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L429:
	movq	16(%r13), %rdx
	movq	24(%r12), %r13
	cmpw	$45, (%rdx)
	movq	16(%r13), %rax
	jne	.L424
	cmpq	%rax, 24(%r13)
	ja	.L436
	cmpb	$0, 48(%r13)
	movl	$-1, %ebx
	jne	.L435
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r13), %rax
	je	.L435
.L436:
	movzwl	(%rax), %ebx
	jmp	.L435
	.cfi_endproc
.LFE18140:
	.size	_ZN2v88internal7Scanner15ScanHtmlCommentEv, .-_ZN2v88internal7Scanner15ScanHtmlCommentEv
	.section	.text._ZNK2v88internal7Scanner9SourceUrlEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal7Scanner9SourceUrlEPNS0_7IsolateE
	.type	_ZNK2v88internal7Scanner9SourceUrlEPNS0_7IsolateE, @function
_ZNK2v88internal7Scanner9SourceUrlEPNS0_7IsolateE:
.LFB18148:
	.cfi_startproc
	endbr64
	cmpb	$0, 308(%rdi)
	movl	304(%rdi), %edx
	jne	.L444
	sarl	%edx
.L444:
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L451
	ret
	.p2align 4,,10
	.p2align 3
.L451:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$288, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal13LiteralBuffer11InternalizeEPNS0_7IsolateE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18148:
	.size	_ZNK2v88internal7Scanner9SourceUrlEPNS0_7IsolateE, .-_ZNK2v88internal7Scanner9SourceUrlEPNS0_7IsolateE
	.section	.text._ZNK2v88internal7Scanner16SourceMappingUrlEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal7Scanner16SourceMappingUrlEPNS0_7IsolateE
	.type	_ZNK2v88internal7Scanner16SourceMappingUrlEPNS0_7IsolateE, @function
_ZNK2v88internal7Scanner16SourceMappingUrlEPNS0_7IsolateE:
.LFB18149:
	.cfi_startproc
	endbr64
	cmpb	$0, 332(%rdi)
	movl	328(%rdi), %edx
	jne	.L453
	sarl	%edx
.L453:
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L460
	ret
	.p2align 4,,10
	.p2align 3
.L460:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$312, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal13LiteralBuffer11InternalizeEPNS0_7IsolateE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18149:
	.size	_ZNK2v88internal7Scanner16SourceMappingUrlEPNS0_7IsolateE, .-_ZNK2v88internal7Scanner16SourceMappingUrlEPNS0_7IsolateE
	.section	.text._ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb
	.type	_ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb, @function
_ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb:
.LFB18150:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	32(%rdi), %r14d
	testb	%dl, %dl
	je	.L487
	movl	%r14d, %edi
	call	*%rsi
	testb	%al, %al
	je	.L489
	movl	32(%rbx), %r14d
	.p2align 4,,10
	.p2align 3
.L487:
	xorl	%r13d, %r13d
.L463:
	movl	%r14d, %edi
	call	*%r12
	testb	%al, %al
	jne	.L467
.L492:
	cmpl	$95, 32(%rbx)
	jne	.L490
.L468:
	movq	24(%rbx), %r15
	movq	16(%r15), %rax
	cmpq	24(%r15), %rax
	jnb	.L471
.L488:
	movzwl	(%rax), %r14d
	addq	$2, %rax
	movq	%rax, 16(%r15)
	movl	%r14d, 32(%rbx)
	cmpl	$95, %r14d
	je	.L491
	movl	$1, %r13d
	movl	%r14d, %edi
	call	*%r12
	testb	%al, %al
	je	.L492
	.p2align 4,,10
	.p2align 3
.L467:
	movl	32(%rbx), %r14d
	cmpl	$95, %r14d
	je	.L468
	movq	8(%rbx), %r13
	cmpb	$0, 28(%r13)
	leaq	8(%r13), %r15
	je	.L477
	cmpl	$255, %r14d
	jle	.L493
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
.L477:
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L481
.L483:
	movzwl	(%rax), %r14d
.L482:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%r14d, 32(%rbx)
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L491:
	movl	348(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L489
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	movl	$214, 348(%rbx)
	leal	-1(%rax), %edx
	movl	%eax, 356(%rbx)
	movl	%edx, 352(%rbx)
.L489:
	xorl	%eax, %eax
.L461:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L493:
	.cfi_restore_state
	movslq	24(%r13), %rax
	cmpl	16(%r13), %eax
	jge	.L494
.L479:
	movq	8(%r13), %rdx
	movb	%r14b, (%rdx,%rax)
	addl	$1, 24(%r13)
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jb	.L483
.L481:
	cmpb	$0, 48(%r13)
	movl	$-1, %r14d
	jne	.L482
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r13), %rax
	jne	.L483
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L494:
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r13), %rax
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L471:
	movzbl	48(%r15), %r13d
	testb	%r13b, %r13b
	je	.L473
	addq	$2, %rax
	movl	$-1, %r14d
	movq	%rax, 16(%r15)
	movl	$-1, 32(%rbx)
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L490:
	testb	%r13b, %r13b
	jne	.L469
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L473:
	.cfi_restore_state
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L475
	movq	16(%r15), %rax
	jmp	.L488
.L469:
	movl	348(%rbx), %edx
	testl	%edx, %edx
	jne	.L489
	movq	24(%rbx), %rcx
	movq	16(%rcx), %rdx
	subq	8(%rcx), %rdx
	sarq	%rdx
	addq	32(%rcx), %rdx
	movl	$213, 348(%rbx)
	leal	-1(%rdx), %ecx
	movl	%edx, 356(%rbx)
	movl	%ecx, 352(%rbx)
	jmp	.L461
.L475:
	addq	$2, 16(%r15)
	movl	$-1, %r14d
	movl	$1, %r13d
	movl	$-1, 32(%rbx)
	jmp	.L463
	.cfi_endproc
.LFE18150:
	.size	_ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb, .-_ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb
	.section	.text._ZN2v88internal7Scanner17ScanDecimalDigitsEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner17ScanDecimalDigitsEb
	.type	_ZN2v88internal7Scanner17ScanDecimalDigitsEb, @function
_ZN2v88internal7Scanner17ScanDecimalDigitsEb:
.LFB18151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testb	%sil, %sil
	jne	.L513
	movl	32(%rdi), %r13d
	movl	%esi, %r12d
	leal	-48(%r13), %eax
	cmpl	$9, %eax
	jbe	.L506
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L515:
	movslq	24(%rbx), %rax
	cmpl	16(%rbx), %eax
	jge	.L514
.L499:
	movq	8(%rbx), %rdx
	movb	%r13b, (%rdx,%rax)
	addl	$1, 24(%rbx)
	movq	24(%r14), %rbx
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jnb	.L501
.L512:
	movzwl	(%rax), %r13d
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	leal	-48(%r13), %edx
	movl	%r13d, 32(%r14)
	cmpl	$9, %edx
	ja	.L497
.L506:
	movq	8(%r14), %rbx
	cmpb	$0, 28(%rbx)
	leaq	8(%rbx), %rdi
	jne	.L515
	movl	%r13d, %esi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	movq	24(%r14), %rbx
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jb	.L512
.L501:
	cmpb	$0, 48(%rbx)
	je	.L503
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	movl	$-1, 32(%r14)
.L504:
	movl	$1, %eax
.L495:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L514:
	.cfi_restore_state
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%rbx), %rax
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L503:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L516
	movq	16(%rbx), %rax
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L513:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb.constprop.0
	.p2align 4,,10
	.p2align 3
.L497:
	.cfi_restore_state
	movl	$1, %eax
	cmpl	$95, %r13d
	jne	.L495
	movl	348(%r14), %edx
	movl	%r12d, %eax
	testl	%edx, %edx
	jne	.L495
	movq	24(%r14), %rcx
	movq	16(%rcx), %rdx
	subq	8(%rcx), %rdx
	sarq	%rdx
	addq	32(%rcx), %rdx
	movl	$257, 348(%r14)
	leal	-1(%rdx), %ecx
	movl	%edx, 356(%r14)
	movl	%ecx, 352(%r14)
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L516:
	addq	$2, 16(%rbx)
	movl	$-1, 32(%r14)
	jmp	.L504
	.cfi_endproc
.LFE18151:
	.size	_ZN2v88internal7Scanner17ScanDecimalDigitsEb, .-_ZN2v88internal7Scanner17ScanDecimalDigitsEb
	.section	.text._ZN2v88internal7Scanner37ScanDecimalAsSmiWithNumericSeparatorsEPm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner37ScanDecimalAsSmiWithNumericSeparatorsEPm
	.type	_ZN2v88internal7Scanner37ScanDecimalAsSmiWithNumericSeparatorsEPm, @function
_ZN2v88internal7Scanner37ScanDecimalAsSmiWithNumericSeparatorsEPm:
.LFB18152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	32(%rdi), %edx
	.p2align 4,,10
	.p2align 3
.L518:
	leal	-48(%rdx), %eax
	cmpl	$9, %eax
	jbe	.L519
.L543:
	cmpl	$95, %edx
	jne	.L541
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L523
.L539:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	%edx, 32(%rbx)
	cmpl	$95, %edx
	je	.L542
	leal	-48(%rdx), %eax
	movl	$1, %ecx
	cmpl	$9, %eax
	ja	.L543
.L519:
	movq	0(%r13), %rdx
	cltq
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rax,%rdx,2), %rax
	movq	%rax, 0(%r13)
	movq	24(%rbx), %r12
	movl	32(%rbx), %r14d
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L529
.L531:
	movzwl	(%rax), %r15d
.L530:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movq	8(%rbx), %r12
	movl	%r15d, 32(%rbx)
	cmpb	$0, 28(%r12)
	leaq	8(%r12), %r15
	je	.L532
	cmpl	$255, %r14d
	jle	.L544
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
.L532:
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	movl	32(%rbx), %edx
	xorl	%ecx, %ecx
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L544:
	movslq	24(%r12), %rax
	cmpl	16(%r12), %eax
	jge	.L545
.L534:
	movq	8(%r12), %rdx
	xorl	%ecx, %ecx
	movb	%r14b, (%rdx,%rax)
	addl	$1, 24(%r12)
	movl	32(%rbx), %edx
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L542:
	movl	348(%rbx), %edx
	testl	%edx, %edx
	jne	.L540
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	movl	$214, 348(%rbx)
	leal	-1(%rax), %edx
	movl	%eax, 356(%rbx)
	xorl	%eax, %eax
	movl	%edx, 352(%rbx)
.L517:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L529:
	.cfi_restore_state
	cmpb	$0, 48(%r12)
	movl	$-1, %r15d
	jne	.L530
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L531
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L523:
	cmpb	$0, 48(%r12)
	je	.L525
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$-1, 32(%rbx)
.L521:
	movl	348(%rbx), %eax
	testl	%eax, %eax
	jne	.L540
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	movl	$213, 348(%rbx)
	leal	-1(%rax), %edx
	movl	%eax, 356(%rbx)
	movl	%edx, 352(%rbx)
.L540:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L545:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r12), %rax
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L525:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L546
	addq	$2, 16(%r12)
	movl	$-1, 32(%rbx)
	jmp	.L521
.L546:
	movq	16(%r12), %rax
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L541:
	testb	%cl, %cl
	jne	.L521
	movl	$1, %eax
	jmp	.L517
	.cfi_endproc
.LFE18152:
	.size	_ZN2v88internal7Scanner37ScanDecimalAsSmiWithNumericSeparatorsEPm, .-_ZN2v88internal7Scanner37ScanDecimalAsSmiWithNumericSeparatorsEPm
	.section	.text._ZN2v88internal7Scanner16ScanDecimalAsSmiEPmb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner16ScanDecimalAsSmiEPmb
	.type	_ZN2v88internal7Scanner16ScanDecimalAsSmiEPmb, @function
_ZN2v88internal7Scanner16ScanDecimalAsSmiEPmb:
.LFB18153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	testb	%dl, %dl
	jne	.L548
.L557:
	movl	32(%r12), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L559
.L563:
	movq	0(%r13), %rdx
	cltq
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rax,%rdx,2), %rax
	movq	%rax, 0(%r13)
	movq	24(%r12), %rbx
	movl	32(%r12), %r14d
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jnb	.L551
.L553:
	movzwl	(%rax), %r15d
.L552:
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	movq	8(%r12), %rbx
	movl	%r15d, 32(%r12)
	cmpb	$0, 28(%rbx)
	leaq	8(%rbx), %r15
	je	.L554
	cmpl	$255, %r14d
	jle	.L562
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
.L554:
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	movl	32(%r12), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L563
.L559:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L562:
	.cfi_restore_state
	movslq	24(%rbx), %rax
	cmpl	16(%rbx), %eax
	jge	.L564
.L556:
	movq	8(%rbx), %rdx
	movb	%r14b, (%rdx,%rax)
	addl	$1, 24(%rbx)
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L551:
	cmpb	$0, 48(%rbx)
	movl	$-1, %r15d
	jne	.L552
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%rbx), %rax
	jne	.L553
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L564:
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%rbx), %rax
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L548:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Scanner37ScanDecimalAsSmiWithNumericSeparatorsEPm
	.cfi_endproc
.LFE18153:
	.size	_ZN2v88internal7Scanner16ScanDecimalAsSmiEPmb, .-_ZN2v88internal7Scanner16ScanDecimalAsSmiEPmb
	.section	.text._ZN2v88internal7Scanner16ScanBinaryDigitsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner16ScanBinaryDigitsEv
	.type	_ZN2v88internal7Scanner16ScanBinaryDigitsEv, @function
_ZN2v88internal7Scanner16ScanBinaryDigitsEv:
.LFB18154:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb.constprop.1
	.cfi_endproc
.LFE18154:
	.size	_ZN2v88internal7Scanner16ScanBinaryDigitsEv, .-_ZN2v88internal7Scanner16ScanBinaryDigitsEv
	.section	.text._ZN2v88internal7Scanner15ScanOctalDigitsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner15ScanOctalDigitsEv
	.type	_ZN2v88internal7Scanner15ScanOctalDigitsEv, @function
_ZN2v88internal7Scanner15ScanOctalDigitsEv:
.LFB18155:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb.constprop.2
	.cfi_endproc
.LFE18155:
	.size	_ZN2v88internal7Scanner15ScanOctalDigitsEv, .-_ZN2v88internal7Scanner15ScanOctalDigitsEv
	.section	.text._ZN2v88internal7Scanner23ScanImplicitOctalDigitsEiPNS1_10NumberKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner23ScanImplicitOctalDigitsEiPNS1_10NumberKindE
	.type	_ZN2v88internal7Scanner23ScanImplicitOctalDigitsEiPNS1_10NumberKindE, @function
_ZN2v88internal7Scanner23ScanImplicitOctalDigitsEiPNS1_10NumberKindE:
.LFB18156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$0, (%rdx)
	movl	32(%rdi), %r12d
	leal	-56(%r12), %eax
	movl	%r12d, %edx
	cmpl	$1, %eax
	ja	.L568
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L585:
	movslq	24(%rbx), %rax
	cmpl	16(%rbx), %eax
	jge	.L583
.L572:
	movq	8(%rbx), %rdx
	movb	%r12b, (%rdx,%rax)
	addl	$1, 24(%rbx)
	movq	24(%r15), %rbx
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jnb	.L574
.L582:
	movzwl	(%rax), %r12d
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	leal	-56(%r12), %ecx
	movl	%r12d, 32(%r15)
	movl	%r12d, %edx
	cmpl	$1, %ecx
	jbe	.L579
.L568:
	subl	$48, %edx
	cmpl	$7, %edx
	ja	.L584
.L570:
	movq	8(%r15), %rbx
	cmpb	$0, 28(%rbx)
	leaq	8(%rbx), %rdi
	jne	.L585
	movl	%r12d, %esi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	movq	24(%r15), %rbx
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jb	.L582
.L574:
	cmpb	$0, 48(%rbx)
	jne	.L586
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L578
	movl	$-1, %edx
	addq	$2, 16(%rbx)
	movl	$-1, %r12d
	subl	$48, %edx
	movl	$-1, 32(%r15)
	cmpl	$7, %edx
	jbe	.L570
	.p2align 4,,10
	.p2align 3
.L584:
	movq	24(%r15), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	movl	%r14d, 336(%r15)
	movl	$297, 344(%r15)
	subl	$1, %eax
	movl	%eax, 340(%r15)
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L583:
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%rbx), %rax
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L579:
	movl	$5, 0(%r13)
.L569:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L586:
	.cfi_restore_state
	addq	$2, %rax
	movl	$-1, %edx
	movl	$-1, %r12d
	movq	%rax, 16(%rbx)
	movl	$-1, 32(%r15)
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L578:
	movq	16(%rbx), %rax
	jmp	.L582
	.cfi_endproc
.LFE18156:
	.size	_ZN2v88internal7Scanner23ScanImplicitOctalDigitsEiPNS1_10NumberKindE, .-_ZN2v88internal7Scanner23ScanImplicitOctalDigitsEiPNS1_10NumberKindE
	.section	.text._ZN2v88internal7Scanner13ScanHexDigitsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner13ScanHexDigitsEv
	.type	_ZN2v88internal7Scanner13ScanHexDigitsEv, @function
_ZN2v88internal7Scanner13ScanHexDigitsEv:
.LFB18157:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	32(%rdi), %r12d
	movq	%rdi, %rbx
	leal	-48(%r12), %eax
	cmpl	$9, %eax
	jbe	.L608
	movl	%r12d, %edx
	orl	$32, %edx
	subl	$97, %edx
	cmpl	$5, %edx
	ja	.L612
.L608:
	xorl	%edx, %edx
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L615:
	movl	%r12d, %eax
	orl	$32, %eax
	subl	$97, %eax
	cmpl	$5, %eax
	setbe	%r13b
	cmpl	$95, %r12d
	sete	%al
	orb	%al, %r13b
	je	.L613
	cmpl	$95, %r12d
	jne	.L591
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L594
.L611:
	movzwl	(%rax), %r12d
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%r12d, 32(%rbx)
	cmpl	$95, %r12d
	je	.L614
.L597:
	movl	%r13d, %edx
	leal	-48(%r12), %eax
.L590:
	cmpl	$9, %eax
	ja	.L615
.L591:
	movq	8(%rbx), %r13
	cmpb	$0, 28(%r13)
	leaq	8(%r13), %r14
	je	.L600
	cmpl	$255, %r12d
	jle	.L616
	movq	%r14, %rdi
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
.L600:
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L604
.L606:
	movzwl	(%rax), %r12d
.L605:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	xorl	%r13d, %r13d
	movl	%r12d, 32(%rbx)
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L614:
	movl	348(%rbx), %edx
	testl	%edx, %edx
	jne	.L612
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	movl	$214, 348(%rbx)
	leal	-1(%rax), %edx
	movl	%eax, 356(%rbx)
	movl	%edx, 352(%rbx)
.L612:
	xorl	%r13d, %r13d
.L587:
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L616:
	.cfi_restore_state
	movslq	24(%r13), %rax
	cmpl	16(%r13), %eax
	jge	.L617
	movq	8(%r13), %rdx
	movb	%r12b, (%rdx,%rax)
	addl	$1, 24(%r13)
.L618:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jb	.L606
.L604:
	cmpb	$0, 48(%r13)
	movl	$-1, %r12d
	jne	.L605
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r13), %rax
	jne	.L606
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L617:
	movq	%r14, %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r13), %rax
	movq	8(%r13), %rdx
	movb	%r12b, (%rdx,%rax)
	addl	$1, 24(%r13)
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L594:
	movzbl	48(%r14), %edx
	testb	%dl, %dl
	je	.L596
	addq	$2, %rax
	movl	%edx, %r13d
	movl	$-1, %r12d
	movq	%rax, 16(%r14)
	movl	$-1, 32(%rbx)
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L613:
	testb	%dl, %dl
	jne	.L593
	movl	$1, %r13d
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L596:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L619
	movq	16(%r14), %rax
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L619:
	addq	$2, 16(%r14)
	movl	$-1, %r12d
	movl	$-1, 32(%rbx)
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L593:
	movl	348(%rbx), %eax
	testl	%eax, %eax
	jne	.L612
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	movl	$213, 348(%rbx)
	leal	-1(%rax), %edx
	movl	%eax, 356(%rbx)
	movl	%edx, 352(%rbx)
	jmp	.L587
	.cfi_endproc
.LFE18157:
	.size	_ZN2v88internal7Scanner13ScanHexDigitsEv, .-_ZN2v88internal7Scanner13ScanHexDigitsEv
	.section	.text._ZN2v88internal7Scanner17ScanSignedIntegerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner17ScanSignedIntegerEv
	.type	_ZN2v88internal7Scanner17ScanSignedIntegerEv, @function
_ZN2v88internal7Scanner17ScanSignedIntegerEv:
.LFB18158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	32(%rdi), %r13d
	leal	-43(%r13), %eax
	andl	$-3, %eax
	je	.L621
	subl	$48, %r13d
.L622:
	cmpl	$9, %r13d
	ja	.L620
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb.constprop.0
	.p2align 4,,10
	.p2align 3
.L627:
	.cfi_restore_state
	cmpb	$0, 48(%rbx)
	jne	.L635
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L631
	addq	$2, 16(%rbx)
	movl	$-1, 32(%r12)
.L620:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L621:
	.cfi_restore_state
	movq	8(%rdi), %rbx
	cmpb	$0, 28(%rbx)
	leaq	8(%rbx), %r14
	je	.L623
	cmpl	$255, %r13d
	jle	.L636
	movq	%r14, %rdi
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
.L623:
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L626:
	movq	24(%r12), %rbx
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jnb	.L627
.L634:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	leal	-48(%rdx), %r13d
	movl	%edx, 32(%r12)
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L636:
	movslq	24(%rbx), %rax
	cmpl	16(%rbx), %eax
	jge	.L637
.L625:
	movq	8(%rbx), %rdx
	movb	%r13b, (%rdx,%rax)
	addl	$1, 24(%rbx)
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L635:
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	movl	$-1, 32(%r12)
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L637:
	movq	%r14, %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%rbx), %rax
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L631:
	movq	16(%rbx), %rax
	jmp	.L634
	.cfi_endproc
.LFE18158:
	.size	_ZN2v88internal7Scanner17ScanSignedIntegerEv, .-_ZN2v88internal7Scanner17ScanSignedIntegerEv
	.section	.text._ZN2v88internal7Scanner10ScanNumberEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner10ScanNumberEb
	.type	_ZN2v88internal7Scanner10ScanNumberEb, @function
_ZN2v88internal7Scanner10ScanNumberEb:
.LFB18159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movl	$4, -68(%rbp)
	movl	$0, 24(%rax)
	movb	$1, 28(%rax)
	movq	24(%rdi), %rax
	movq	16(%rax), %r12
	subq	8(%rax), %r12
	sarq	%r12
	addq	32(%rax), %r12
	leal	-1(%r12), %r15d
	testb	%sil, %sil
	je	.L639
	movq	8(%rdi), %r12
	movslq	24(%r12), %rax
	cmpl	16(%r12), %eax
	jge	.L794
.L640:
	movq	8(%r12), %rdx
	movb	$46, (%rdx,%rax)
	addl	$1, 24(%r12)
	cmpl	$95, 32(%rbx)
	je	.L791
.L716:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb.constprop.0
	testb	%al, %al
	je	.L791
	movslq	32(%rbx), %r14
.L709:
	movl	%r14d, %eax
	orl	$32, %eax
	cmpl	$101, %eax
	je	.L721
.L790:
	leal	-48(%r14), %eax
	xorl	%r13d, %r13d
.L722:
	cmpl	$9, %eax
	jbe	.L791
	cmpl	$127, %r14d
	ja	.L741
.L720:
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rax
	movzbl	(%rax,%r14), %eax
	andl	$1, %eax
.L737:
	testb	%al, %al
	jne	.L791
	cmpl	$5, -68(%rbp)
	jne	.L738
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	movl	%r15d, 336(%rbx)
	movl	$298, 344(%rbx)
	subl	$1, %eax
	movl	%eax, 340(%rbx)
.L738:
	cmpb	$1, %r13b
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$89, %eax
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L806:
	movl	348(%rbx), %eax
	testl	%eax, %eax
	jne	.L791
	subl	$1, %ecx
	movl	%r15d, 352(%rbx)
	movl	$183, 348(%rbx)
	movl	%ecx, 356(%rbx)
	.p2align 4,,10
	.p2align 3
.L791:
	movl	$109, %eax
.L638:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L795
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L639:
	.cfi_restore_state
	cmpl	$48, 32(%rdi)
	movl	%esi, %r13d
	movl	$1, %r8d
	je	.L796
.L740:
	leaq	-64(%rbp), %rsi
	movl	%r8d, %edx
	movq	%rbx, %rdi
	movl	%r8d, -88(%rbp)
	movq	$0, -64(%rbp)
	call	_ZN2v88internal7Scanner16ScanDecimalAsSmiEPmb
	movl	%eax, %r9d
	movl	$109, %eax
	testb	%r9b, %r9b
	je	.L638
	movq	8(%rbx), %rax
	movl	-88(%rbp), %r8d
	cmpl	$10, 24(%rax)
	jg	.L698
	cmpq	$2147483647, -64(%rbp)
	ja	.L698
	movslq	32(%rbx), %rdi
	cmpl	$46, %edi
	je	.L698
	cmpl	$127, %edi
	ja	.L797
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rax
	movzbl	(%rax,%rdi), %eax
	andl	$1, %eax
.L705:
	testb	%al, %al
	jne	.L698
	movq	8(%rbx), %rax
	movq	-64(%rbp), %rdx
	cmpl	$5, -68(%rbp)
	movl	%edx, 72(%rax)
	jne	.L792
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	movl	%r15d, 336(%rbx)
	movl	$298, 344(%rbx)
	subl	$1, %eax
	movl	%eax, 340(%rbx)
.L792:
	movl	$88, %eax
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L721:
	movl	-68(%rbp), %eax
	subl	$4, %eax
	cmpl	$1, %eax
	ja	.L791
	movq	8(%rbx), %r12
	cmpb	$0, 28(%r12)
	leaq	8(%r12), %r13
	je	.L730
	cmpl	$255, %r14d
	jle	.L798
	movq	%r13, %rdi
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
.L730:
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L733:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L734
.L736:
	movzwl	(%rax), %r13d
.L735:
	addq	$2, %rax
	movq	%rbx, %rdi
	movq	%rax, 16(%r12)
	movl	%r13d, 32(%rbx)
	call	_ZN2v88internal7Scanner17ScanSignedIntegerEv
	testb	%al, %al
	je	.L791
	movslq	32(%rbx), %r14
	jmp	.L790
	.p2align 4,,10
	.p2align 3
.L796:
	movq	8(%rdi), %rax
	movl	%esi, %r14d
	cmpb	$0, 28(%rax)
	leaq	8(%rax), %rdi
	je	.L646
	movslq	24(%rax), %rdx
	cmpl	16(%rax), %edx
	jge	.L799
.L647:
	movq	8(%rax), %rcx
	movb	$48, (%rcx,%rdx)
	addl	$1, 24(%rax)
.L648:
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	cmpq	24(%rdx), %rax
	jnb	.L649
.L652:
	movzwl	(%rax), %esi
	addq	$2, %rax
	movq	%rax, 16(%rdx)
	movl	%esi, %eax
	movl	%esi, 32(%rbx)
	orl	$32, %eax
	cmpl	$120, %eax
	je	.L800
	cmpl	$111, %eax
	je	.L801
	cmpl	$98, %eax
	je	.L802
	leal	-48(%rsi), %eax
	cmpl	$7, %eax
	ja	.L697
	leaq	-68(%rbp), %rdx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	movl	$0, -68(%rbp)
	call	_ZN2v88internal7Scanner23ScanImplicitOctalDigitsEiPNS1_10NumberKindE
	testb	%al, %al
	je	.L791
	movl	-68(%rbp), %eax
	cmpl	$5, %eax
	je	.L803
	.p2align 4,,10
	.p2align 3
.L688:
	leal	-4(%rax), %edx
	cmpl	$1, %edx
	jbe	.L804
	movslq	32(%rbx), %r14
	cmpl	$110, %r14d
	jne	.L709
	subl	$1, %eax
	cmpl	$3, %eax
	jbe	.L805
.L744:
	movl	$110, %r14d
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L794:
	leaq	8(%r12), %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r12), %rax
	jmp	.L640
.L803:
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L698:
	movl	%r8d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner17ScanDecimalDigitsEb
	testb	%al, %al
	je	.L791
	movslq	32(%rbx), %r14
	cmpl	$46, %r14d
	je	.L707
	cmpl	$110, %r14d
	jne	.L709
	movl	-68(%rbp), %esi
	leal	-1(%rsi), %eax
	cmpl	$3, %eax
	ja	.L744
	movq	24(%rbx), %r13
	movq	16(%r13), %rdx
	movq	%rdx, %rax
	subq	8(%r13), %rax
	sarq	%rax
	addq	32(%r13), %rax
	movl	%eax, %ecx
	subl	%r12d, %eax
	cmpl	$4, %esi
	jne	.L742
.L723:
	cmpl	$268435456, %eax
	jg	.L806
	cmpq	%rdx, 24(%r13)
	jbe	.L725
.L789:
	movzwl	(%rdx), %r14d
	addq	$2, %rdx
	movq	%rdx, 16(%r13)
	movl	$1, %r13d
	movl	%r14d, 32(%rbx)
	leal	-48(%r14), %eax
	jmp	.L722
.L725:
	cmpb	$0, 48(%r13)
	je	.L727
	addq	$2, %rdx
	movq	%rdx, 16(%r13)
	movl	$-1, 32(%rbx)
.L728:
	movl	$-1, %r14d
	movl	$1, %r13d
	.p2align 4,,10
	.p2align 3
.L741:
	movl	%r14d, %edi
	call	_ZN2v88internal21IsIdentifierStartSlowEi@PLT
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L800:
	movq	8(%rbx), %rax
	cmpb	$0, 28(%rax)
	leaq	8(%rax), %rdi
	je	.L654
	cmpl	$255, %esi
	jle	.L807
	movl	%esi, -92(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
	movl	-92(%rbp), %esi
	movq	-88(%rbp), %rdi
.L654:
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L657:
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	cmpq	24(%rdx), %rax
	jnb	.L658
.L661:
	movzwl	(%rax), %esi
	addq	$2, %rax
	movl	$3, -68(%rbp)
	movq	%rax, 16(%rdx)
	leal	-48(%rsi), %eax
	movl	%esi, 32(%rbx)
	cmpl	$9, %eax
	ja	.L808
	.p2align 4,,10
	.p2align 3
.L664:
	movq	8(%rbx), %r14
	cmpb	$0, 28(%r14)
	leaq	8(%r14), %rdi
	je	.L673
	cmpl	$255, %esi
	jle	.L809
	movl	%esi, -92(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
	movl	-92(%rbp), %esi
	movq	-88(%rbp), %rdi
.L673:
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L676:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L677
.L679:
	movzwl	(%rax), %esi
.L678:
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%r13d, %r14d
	movl	%esi, 32(%rbx)
.L671:
	leal	-48(%rsi), %eax
.L663:
	cmpl	$9, %eax
	jbe	.L664
	movl	%esi, %eax
	orl	$32, %eax
	subl	$97, %eax
	cmpl	$5, %eax
	setbe	%al
	cmpl	$95, %esi
	sete	%dl
	orb	%al, %dl
	je	.L810
	cmpl	$95, %esi
	jne	.L664
	movq	24(%rbx), %rcx
	movq	16(%rcx), %rax
	cmpq	24(%rcx), %rax
	jnb	.L668
.L787:
	movzwl	(%rax), %esi
	addq	$2, %rax
	movq	%rax, 16(%rcx)
	movl	%esi, 32(%rbx)
	cmpl	$95, %esi
	je	.L811
	movl	%edx, %r14d
	leal	-48(%rsi), %eax
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L646:
	movl	$48, %esi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L802:
	movq	8(%rbx), %r14
	cmpb	$0, 28(%r14)
	leaq	8(%r14), %rdi
	je	.L690
	cmpl	$255, %esi
	jle	.L812
	movl	%esi, -92(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
	movl	-92(%rbp), %esi
	movq	-88(%rbp), %rdi
.L690:
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L693:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L694
.L696:
	movzwl	(%rax), %edx
.L695:
	addq	$2, %rax
	movq	%rbx, %rdi
	movl	$1, -68(%rbp)
	movq	%rax, 16(%r14)
	movl	%edx, 32(%rbx)
	call	_ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb.constprop.1
	testb	%al, %al
	je	.L791
.L785:
	movl	-68(%rbp), %eax
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L707:
	movq	8(%rbx), %r12
	cmpb	$0, 28(%r12)
	leaq	8(%r12), %rdi
	je	.L710
	movslq	24(%r12), %rax
	cmpl	16(%r12), %eax
	jge	.L813
.L711:
	movq	8(%r12), %rdx
	movb	$46, (%rdx,%rax)
	addl	$1, 24(%r12)
.L712:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L713
.L788:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	%edx, 32(%rbx)
	cmpl	$95, %edx
	jne	.L716
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L809:
	movslq	24(%r14), %rax
	cmpl	16(%r14), %eax
	jge	.L814
.L675:
	movq	8(%r14), %rdx
	movb	%sil, (%rdx,%rax)
	addl	$1, 24(%r14)
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L805:
	movq	24(%rbx), %r13
	movq	16(%r13), %rdx
	movq	%rdx, %rax
	subq	8(%r13), %rax
	sarq	%rax
	addq	32(%r13), %rax
	movl	%eax, %ecx
	subl	%r12d, %eax
.L742:
	subl	$2, %eax
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L801:
	movq	8(%rbx), %r14
	cmpb	$0, 28(%r14)
	leaq	8(%r14), %rdi
	je	.L681
	cmpl	$255, %esi
	jle	.L815
	movl	%esi, -92(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
	movl	-92(%rbp), %esi
	movq	-88(%rbp), %rdi
.L681:
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L684:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L685
.L687:
	movzwl	(%rax), %edx
.L686:
	addq	$2, %rax
	movq	%rbx, %rdi
	movl	$2, -68(%rbp)
	movq	%rax, 16(%r14)
	movl	%edx, 32(%rbx)
	call	_ZN2v88internal7Scanner31ScanDigitsWithNumericSeparatorsEPFbiEb.constprop.2
	testb	%al, %al
	jne	.L785
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L799:
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movq	-88(%rbp), %rax
	movslq	24(%rax), %rdx
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L798:
	movslq	24(%r12), %rax
	cmpl	16(%r12), %eax
	jge	.L816
.L732:
	movq	8(%r12), %rdx
	movb	%r14b, (%rdx,%rax)
	addl	$1, 24(%r12)
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L811:
	movl	348(%rbx), %esi
	testl	%esi, %esi
	jne	.L791
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	movl	$214, 348(%rbx)
	leal	-1(%rax), %edx
	movl	%eax, 356(%rbx)
	movl	$109, %eax
	movl	%edx, 352(%rbx)
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L808:
	movl	%esi, %edx
	orl	$32, %edx
	subl	$97, %edx
	cmpl	$5, %edx
	jbe	.L663
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L697:
	leal	-56(%rsi), %eax
	cmpl	$1, %eax
	ja	.L699
	movl	$5, -68(%rbp)
	xorl	%r8d, %r8d
	jmp	.L740
	.p2align 4,,10
	.p2align 3
.L677:
	cmpb	$0, 48(%r14)
	movl	$-1, %esi
	jne	.L678
	movq	(%r14), %rax
	movl	%esi, -88(%rbp)
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r14), %rax
	jne	.L679
	movl	-88(%rbp), %esi
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L649:
	cmpb	$0, 48(%rdx)
	jne	.L651
	movq	(%rdx), %rax
	movq	%rdx, -88(%rbp)
	movq	%rdx, %rdi
	call	*40(%rax)
	movq	-88(%rbp), %rdx
	testb	%al, %al
	movq	16(%rdx), %rax
	jne	.L652
.L651:
	addq	$2, %rax
	movq	%rax, 16(%rdx)
	movl	$-1, 32(%rbx)
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L734:
	cmpb	$0, 48(%r12)
	movl	$-1, %r13d
	jne	.L735
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L736
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L710:
	movl	$46, %esi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L807:
	movslq	24(%rax), %rdx
	cmpl	16(%rax), %edx
	jge	.L817
.L656:
	movq	8(%rax), %rcx
	movb	%sil, (%rcx,%rdx)
	addl	$1, 24(%rax)
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L814:
	movb	%sil, -88(%rbp)
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r14), %rax
	movzbl	-88(%rbp), %esi
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L816:
	movq	%r13, %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r12), %rax
	jmp	.L732
.L813:
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r12), %rax
	jmp	.L711
.L668:
	movzbl	48(%rcx), %r14d
	testb	%r14b, %r14b
	je	.L670
	addq	$2, %rax
	movl	$-1, %esi
	movq	%rax, 16(%rcx)
	movl	$-1, 32(%rbx)
	jmp	.L671
.L815:
	movslq	24(%r14), %rax
	cmpl	16(%r14), %eax
	jge	.L818
.L683:
	movq	8(%r14), %rdx
	movb	%sil, (%rdx,%rax)
	addl	$1, 24(%r14)
	jmp	.L684
.L810:
	testb	%r14b, %r14b
	je	.L785
	movl	348(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L791
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	movl	$213, 348(%rbx)
	leal	-1(%rax), %edx
	movl	%eax, 356(%rbx)
	movl	$109, %eax
	movl	%edx, 352(%rbx)
	jmp	.L638
.L699:
	cmpl	$95, %esi
	jne	.L785
	movl	348(%rbx), %edx
	testl	%edx, %edx
	jne	.L791
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	movl	$211, 348(%rbx)
	leal	-1(%rax), %edx
	movl	%eax, 356(%rbx)
	movl	$109, %eax
	movl	%edx, 352(%rbx)
	jmp	.L638
.L658:
	cmpb	$0, 48(%rdx)
	jne	.L660
	movq	(%rdx), %rax
	movq	%rdx, -88(%rbp)
	movq	%rdx, %rdi
	call	*40(%rax)
	movq	-88(%rbp), %rdx
	testb	%al, %al
	movq	16(%rdx), %rax
	jne	.L661
.L660:
	addq	$2, %rax
	movq	%rax, 16(%rdx)
	movl	$109, %eax
	movl	$-1, 32(%rbx)
	jmp	.L638
.L812:
	movslq	24(%r14), %rax
	cmpl	16(%r14), %eax
	jge	.L819
.L692:
	movq	8(%r14), %rdx
	movb	%sil, (%rdx,%rax)
	addl	$1, 24(%r14)
	jmp	.L693
.L670:
	movq	(%rcx), %rax
	movb	%dl, -92(%rbp)
	movq	%rcx, %rdi
	movq	%rcx, -88(%rbp)
	call	*40(%rax)
	movq	-88(%rbp), %rcx
	movzbl	-92(%rbp), %edx
	testb	%al, %al
	jne	.L672
	addq	$2, 16(%rcx)
	movl	%edx, %r14d
	movl	$-1, %esi
	movl	$-1, 32(%rbx)
	jmp	.L671
.L817:
	movb	%sil, -92(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movq	-88(%rbp), %rax
	movzbl	-92(%rbp), %esi
	movslq	24(%rax), %rdx
	jmp	.L656
.L672:
	movq	16(%rcx), %rax
	jmp	.L787
.L713:
	cmpb	$0, 48(%r12)
	je	.L715
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$-1, 32(%rbx)
	jmp	.L716
.L685:
	cmpb	$0, 48(%r14)
	movl	$-1, %edx
	jne	.L686
	movq	(%r14), %rax
	movl	%edx, -88(%rbp)
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r14), %rax
	jne	.L687
	movl	-88(%rbp), %edx
	jmp	.L686
.L797:
	call	_ZN2v88internal21IsIdentifierStartSlowEi@PLT
	movl	-88(%rbp), %r8d
	jmp	.L705
.L818:
	movb	%sil, -88(%rbp)
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r14), %rax
	movzbl	-88(%rbp), %esi
	jmp	.L683
.L694:
	cmpb	$0, 48(%r14)
	movl	$-1, %edx
	jne	.L695
	movq	(%r14), %rax
	movl	%edx, -88(%rbp)
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r14), %rax
	jne	.L696
	movl	-88(%rbp), %edx
	jmp	.L695
.L727:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L729
	addq	$2, 16(%r13)
	movl	$-1, 32(%rbx)
	jmp	.L728
.L729:
	movq	16(%r13), %rdx
	jmp	.L789
.L715:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L717
	addq	$2, 16(%r12)
	movl	$-1, 32(%rbx)
	jmp	.L716
.L717:
	movq	16(%r12), %rax
	jmp	.L788
.L819:
	movb	%sil, -88(%rbp)
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r14), %rax
	movzbl	-88(%rbp), %esi
	jmp	.L692
.L795:
	call	__stack_chk_fail@PLT
.L804:
	xorl	%r8d, %r8d
	cmpl	$5, %eax
	setne	%r8b
	jmp	.L740
	.cfi_endproc
.LFE18159:
	.size	_ZN2v88internal7Scanner10ScanNumberEb, .-_ZN2v88internal7Scanner10ScanNumberEb
	.section	.text._ZN2v88internal7Scanner17ScanRegExpPatternEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner17ScanRegExpPatternEv
	.type	_ZN2v88internal7Scanner17ScanRegExpPatternEv, @function
_ZN2v88internal7Scanner17ScanRegExpPatternEv:
.LFB18163:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movl	$0, 24(%rax)
	movb	$1, 28(%rax)
	movq	8(%rdi), %r12
	cmpb	$25, 56(%r12)
	je	.L881
.L821:
	movl	32(%rbx), %r12d
	xorl	%r14d, %r14d
.L823:
	cmpl	$47, %r12d
	je	.L882
	cmpl	$-1, %r12d
	je	.L873
	cmpl	$10, %r12d
	je	.L873
	cmpl	$13, %r12d
	jne	.L883
.L873:
	xorl	%eax, %eax
.L820:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L883:
	.cfi_restore_state
	leal	-8232(%r12), %eax
	cmpl	$1, %eax
	jbe	.L873
	jmp	.L825
	.p2align 4,,10
	.p2align 3
.L882:
	testb	%r14b, %r14b
	je	.L884
.L825:
	movq	8(%rbx), %r13
	movzbl	28(%r13), %eax
	leaq	8(%r13), %r15
	cmpl	$92, %r12d
	je	.L885
	cmpl	$91, %r12d
	je	.L847
	cmpl	$93, %r12d
	je	.L848
	testb	%al, %al
	je	.L849
.L880:
	cmpl	$255, %r12d
	jle	.L860
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
.L849:
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L852:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L853
.L871:
	movzwl	(%rax), %r12d
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%r12d, 32(%rbx)
	jmp	.L823
	.p2align 4,,10
	.p2align 3
.L881:
	movslq	24(%r12), %rax
	cmpl	16(%r12), %eax
	jge	.L886
.L822:
	movq	8(%r12), %rdx
	movb	$61, (%rdx,%rax)
	addl	$1, 24(%r12)
	jmp	.L821
	.p2align 4,,10
	.p2align 3
.L847:
	testb	%al, %al
	jne	.L862
	movl	$1, %r14d
	jmp	.L849
	.p2align 4,,10
	.p2align 3
.L885:
	testb	%al, %al
	je	.L831
	movslq	24(%r13), %rax
	cmpl	16(%r13), %eax
	jge	.L887
.L832:
	movq	8(%r13), %rdx
	movb	$92, (%rdx,%rax)
	addl	$1, 24(%r13)
.L833:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L834
.L872:
	movzwl	(%rax), %r12d
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%r12d, 32(%rbx)
	cmpl	$10, %r12d
	je	.L873
	cmpl	$13, %r12d
	je	.L873
	leal	-8232(%r12), %eax
	cmpl	$1, %eax
	jbe	.L873
	movq	8(%rbx), %r13
	cmpb	$0, 28(%r13)
	leaq	8(%r13), %r15
	jne	.L880
	jmp	.L849
	.p2align 4,,10
	.p2align 3
.L886:
	leaq	8(%r12), %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r12), %rax
	jmp	.L822
	.p2align 4,,10
	.p2align 3
.L848:
	xorl	%r14d, %r14d
	testb	%al, %al
	je	.L849
.L860:
	movslq	24(%r13), %rax
	cmpl	16(%r13), %eax
	jge	.L888
.L851:
	movq	8(%r13), %rdx
	movb	%r12b, (%rdx,%rax)
	addl	$1, 24(%r13)
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L888:
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r13), %rax
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L831:
	movl	$92, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L853:
	cmpb	$0, 48(%r13)
	jne	.L875
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L856
	addq	$2, 16(%r13)
	movl	$-1, 32(%rbx)
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L887:
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r13), %rax
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L875:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	$-1, 32(%rbx)
	jmp	.L873
.L856:
	movq	16(%r13), %rax
	jmp	.L871
.L834:
	cmpb	$0, 48(%r13)
	jne	.L875
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L889
	addq	$2, 16(%r13)
	movl	$-1, 32(%rbx)
	jmp	.L820
.L884:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L857
	movzwl	(%rax), %edx
.L858:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movq	8(%rbx), %rax
	movl	%edx, 32(%rbx)
	movb	$113, 56(%rax)
	movl	$1, %eax
	jmp	.L820
.L889:
	movq	16(%r13), %rax
	jmp	.L872
.L857:
	movq	%r12, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r12), %rax
	jmp	.L858
.L862:
	movl	%eax, %r14d
	jmp	.L860
	.cfi_endproc
.LFE18163:
	.size	_ZN2v88internal7Scanner17ScanRegExpPatternEv, .-_ZN2v88internal7Scanner17ScanRegExpPatternEv
	.section	.text._ZN2v88internal7Scanner15ScanRegExpFlagsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner15ScanRegExpFlagsEv
	.type	_ZN2v88internal7Scanner15ScanRegExpFlagsEv, @function
_ZN2v88internal7Scanner15ScanRegExpFlagsEv:
.LFB18164:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movslq	32(%rdi), %r12
	cmpl	$127, %r12d
	ja	.L914
	.p2align 4,,10
	.p2align 3
.L891:
	movzbl	(%r14,%r12), %eax
	shrb	%al
	andl	$1, %eax
	testb	%al, %al
	je	.L893
.L916:
	movl	32(%rbx), %eax
	cmpb	$103, %al
	je	.L903
	cmpb	$105, %al
	je	.L904
	cmpb	$109, %al
	je	.L905
	cmpb	$121, %al
	je	.L906
	cmpb	$117, %al
	je	.L907
	cmpb	$115, %al
	je	.L915
.L910:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L915:
	.cfi_restore_state
	movl	$32, %edx
	.p2align 4,,10
	.p2align 3
.L894:
	testl	%edx, %r13d
	jne	.L910
	movq	24(%rbx), %r15
	movq	16(%r15), %rax
	cmpq	24(%r15), %rax
	jnb	.L897
.L899:
	movzwl	(%rax), %r12d
.L898:
	addq	$2, %rax
	orl	%edx, %r13d
	movq	%rax, 16(%r15)
	movl	%r12d, 32(%rbx)
	cmpl	$127, %r12d
	jbe	.L891
.L914:
	movl	%r12d, %edi
	call	_ZN2v88internal20IsIdentifierPartSlowEi@PLT
	testb	%al, %al
	jne	.L916
.L893:
	movq	24(%rbx), %rdx
	movq	8(%rbx), %rcx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	subl	$1, %eax
	movl	%eax, 4(%rcx)
	movq	%r13, %rax
	addq	$24, %rsp
	salq	$32, %rax
	popq	%rbx
	popq	%r12
	orq	$1, %rax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L903:
	.cfi_restore_state
	movl	$1, %edx
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L904:
	movl	$2, %edx
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L905:
	movl	$4, %edx
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L897:
	cmpb	$0, 48(%r15)
	movq	$-1, %r12
	jne	.L898
	movq	(%r15), %rax
	movl	%edx, -52(%rbp)
	movq	%r15, %rdi
	call	*40(%rax)
	movl	-52(%rbp), %edx
	testb	%al, %al
	movq	16(%r15), %rax
	jne	.L899
	jmp	.L898
	.p2align 4,,10
	.p2align 3
.L906:
	movl	$8, %edx
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L907:
	movl	$16, %edx
	jmp	.L894
	.cfi_endproc
.LFE18164:
	.size	_ZN2v88internal7Scanner15ScanRegExpFlagsEv, .-_ZN2v88internal7Scanner15ScanRegExpFlagsEv
	.section	.text._ZNK2v88internal7Scanner13CurrentSymbolEPNS0_15AstValueFactoryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal7Scanner13CurrentSymbolEPNS0_15AstValueFactoryE
	.type	_ZNK2v88internal7Scanner13CurrentSymbolEPNS0_15AstValueFactoryE, @function
_ZNK2v88internal7Scanner13CurrentSymbolEPNS0_15AstValueFactoryE:
.LFB18173:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	(%r8), %rax
	cmpb	$0, 28(%rax)
	movslq	24(%rax), %rdx
	movq	8(%rax), %rsi
	je	.L918
	jmp	_ZN2v88internal15AstValueFactory24GetOneByteStringInternalENS0_6VectorIKhEE@PLT
	.p2align 4,,10
	.p2align 3
.L918:
	sarl	%edx
	movslq	%edx, %rdx
	jmp	_ZN2v88internal15AstValueFactory24GetTwoByteStringInternalENS0_6VectorIKtEE@PLT
	.cfi_endproc
.LFE18173:
	.size	_ZNK2v88internal7Scanner13CurrentSymbolEPNS0_15AstValueFactoryE, .-_ZNK2v88internal7Scanner13CurrentSymbolEPNS0_15AstValueFactoryE
	.section	.text._ZNK2v88internal7Scanner10NextSymbolEPNS0_15AstValueFactoryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal7Scanner10NextSymbolEPNS0_15AstValueFactoryE
	.type	_ZNK2v88internal7Scanner10NextSymbolEPNS0_15AstValueFactoryE, @function
_ZNK2v88internal7Scanner10NextSymbolEPNS0_15AstValueFactoryE:
.LFB18174:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	8(%r8), %rax
	cmpb	$0, 28(%rax)
	movslq	24(%rax), %rdx
	movq	8(%rax), %rsi
	je	.L920
	jmp	_ZN2v88internal15AstValueFactory24GetOneByteStringInternalENS0_6VectorIKhEE@PLT
	.p2align 4,,10
	.p2align 3
.L920:
	sarl	%edx
	movslq	%edx, %rdx
	jmp	_ZN2v88internal15AstValueFactory24GetTwoByteStringInternalENS0_6VectorIKtEE@PLT
	.cfi_endproc
.LFE18174:
	.size	_ZNK2v88internal7Scanner10NextSymbolEPNS0_15AstValueFactoryE, .-_ZNK2v88internal7Scanner10NextSymbolEPNS0_15AstValueFactoryE
	.section	.text._ZNK2v88internal7Scanner16CurrentRawSymbolEPNS0_15AstValueFactoryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal7Scanner16CurrentRawSymbolEPNS0_15AstValueFactoryE
	.type	_ZNK2v88internal7Scanner16CurrentRawSymbolEPNS0_15AstValueFactoryE, @function
_ZNK2v88internal7Scanner16CurrentRawSymbolEPNS0_15AstValueFactoryE:
.LFB18175:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	(%r8), %rax
	cmpb	$0, 52(%rax)
	movslq	48(%rax), %rdx
	movq	32(%rax), %rsi
	je	.L922
	jmp	_ZN2v88internal15AstValueFactory24GetOneByteStringInternalENS0_6VectorIKhEE@PLT
	.p2align 4,,10
	.p2align 3
.L922:
	sarl	%edx
	movslq	%edx, %rdx
	jmp	_ZN2v88internal15AstValueFactory24GetTwoByteStringInternalENS0_6VectorIKtEE@PLT
	.cfi_endproc
.LFE18175:
	.size	_ZNK2v88internal7Scanner16CurrentRawSymbolEPNS0_15AstValueFactoryE, .-_ZNK2v88internal7Scanner16CurrentRawSymbolEPNS0_15AstValueFactoryE
	.section	.text._ZN2v88internal7Scanner11DoubleValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner11DoubleValueEv
	.type	_ZN2v88internal7Scanner11DoubleValueEv, @function
_ZN2v88internal7Scanner11DoubleValueEv:
.LFB18176:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	pxor	%xmm0, %xmm0
	movl	$15, %edx
	movslq	24(%rax), %rsi
	movq	8(%rax), %rdi
	jmp	_ZN2v88internal14StringToDoubleENS0_6VectorIKhEEid@PLT
	.cfi_endproc
.LFE18176:
	.size	_ZN2v88internal7Scanner11DoubleValueEv, .-_ZN2v88internal7Scanner11DoubleValueEv
	.section	.text._ZNK2v88internal7Scanner23CurrentLiteralAsCStringEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal7Scanner23CurrentLiteralAsCStringEPNS0_4ZoneE
	.type	_ZNK2v88internal7Scanner23CurrentLiteralAsCStringEPNS0_4ZoneE, @function
_ZNK2v88internal7Scanner23CurrentLiteralAsCStringEPNS0_4ZoneE:
.LFB18177:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%r8), %rax
	movq	16(%rdi), %r8
	movslq	24(%rax), %rbx
	movq	8(%rax), %r12
	movq	24(%rdi), %rax
	movq	%rbx, %rsi
	addl	$1, %esi
	subq	%r8, %rax
	movslq	%esi, %rsi
	addq	$7, %rsi
	andq	$-8, %rsi
	cmpq	%rax, %rsi
	ja	.L928
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L926:
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	memcpy@PLT
	movb	$0, (%rax,%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L928:
	.cfi_restore_state
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r8
	jmp	.L926
	.cfi_endproc
.LFE18177:
	.size	_ZNK2v88internal7Scanner23CurrentLiteralAsCStringEPNS0_4ZoneE, .-_ZNK2v88internal7Scanner23CurrentLiteralAsCStringEPNS0_4ZoneE
	.section	.text._ZN2v88internal7Scanner17ScanUnicodeEscapeILb0EEEiv,"axG",@progbits,_ZN2v88internal7Scanner17ScanUnicodeEscapeILb0EEEiv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Scanner17ScanUnicodeEscapeILb0EEEiv
	.type	_ZN2v88internal7Scanner17ScanUnicodeEscapeILb0EEEiv, @function
_ZN2v88internal7Scanner17ScanUnicodeEscapeILb0EEEiv:
.LFB20050:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %r15
	movl	32(%rdi), %eax
	movq	16(%r15), %rdx
	movq	%rdx, %r13
	subq	8(%r15), %r13
	sarq	%r13
	addq	32(%r15), %r13
	leal	-3(%r13), %r12d
	cmpl	$123, %eax
	jne	.L930
	cmpq	%rdx, 24(%r15)
	jbe	.L931
.L934:
	movzwl	(%rdx), %eax
	addq	$2, %rdx
	movq	%rdx, 16(%r15)
	movl	%eax, 32(%rbx)
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L935
	movq	24(%rbx), %r13
	orl	$32, %eax
	leal	-49(%rax), %ecx
	movq	16(%r13), %rdx
	cmpl	$5, %ecx
	ja	.L936
	subl	$39, %eax
	js	.L936
.L937:
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L939:
	sall	$4, %r14d
	addl	%eax, %r14d
	cmpl	$1114111, %r14d
	jg	.L981
.L940:
	cmpq	%rdx, 24(%r13)
	jbe	.L944
.L947:
	movzwl	(%rdx), %esi
	addq	$2, %rdx
	movq	%rdx, 16(%r13)
	leal	-48(%rsi), %eax
	movl	%esi, 32(%rbx)
	cmpl	$9, %eax
	jbe	.L948
	movq	24(%rbx), %r13
	orl	$32, %eax
	leal	-49(%rax), %edi
	movq	16(%r13), %rdx
	cmpl	$5, %edi
	ja	.L950
	subl	$39, %eax
	jns	.L939
.L950:
	cmpl	$125, %esi
	jne	.L936
	cmpq	%rdx, 24(%r13)
	jbe	.L952
.L954:
	movzwl	(%rdx), %r12d
.L953:
	addq	$2, %rdx
	movq	%rdx, 16(%r13)
	movl	%r12d, 32(%rbx)
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L948:
	movq	24(%rbx), %r13
	sall	$4, %r14d
	addl	%eax, %r14d
	movq	16(%r13), %rdx
	cmpl	$1114111, %r14d
	jle	.L940
.L981:
	movl	348(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L980
	movq	8(%r13), %rcx
	movq	32(%r13), %rax
	movl	%r12d, 352(%rbx)
	movl	$335, 348(%rbx)
	subq	%rcx, %rdx
	sarq	%rdx
	addq	%rax, %rdx
	movl	%edx, 356(%rbx)
.L980:
	movl	$-1, %r14d
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L930:
	leal	-48(%rax), %r14d
	cmpl	$9, %r14d
	jbe	.L955
	movl	%r14d, %eax
	orl	$32, %eax
	leal	-49(%rax), %ecx
	cmpl	$5, %ecx
	ja	.L956
	subl	$39, %eax
	movl	%eax, %r14d
	js	.L956
.L955:
	cmpq	24(%r15), %rdx
	jnb	.L982
.L957:
	movzwl	(%rdx), %eax
	addq	$2, %rdx
	movq	%rdx, 16(%r15)
	movl	%eax, 32(%rbx)
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L961
	orl	$32, %eax
	leal	-49(%rax), %edx
	cmpl	$5, %edx
	ja	.L956
	subl	$39, %eax
	js	.L956
.L961:
	movl	%r14d, %edx
	movq	24(%rbx), %r15
	sall	$4, %edx
	movq	16(%r15), %rcx
	leal	(%rdx,%rax), %r14d
	cmpq	%rcx, 24(%r15)
	jbe	.L983
.L962:
	movzwl	(%rcx), %edx
	addq	$2, %rcx
	movq	%rcx, 16(%r15)
	movl	%edx, 32(%rbx)
	subl	$48, %edx
	cmpl	$9, %edx
	jbe	.L966
	orl	$32, %edx
	leal	-49(%rdx), %eax
	cmpl	$5, %eax
	ja	.L956
	subl	$39, %edx
	js	.L956
.L966:
	movq	24(%rbx), %r15
	movl	%r14d, %eax
	sall	$4, %eax
	addl	%eax, %edx
	movq	16(%r15), %rax
	cmpq	24(%r15), %rax
	jnb	.L984
	movzwl	(%rax), %ecx
.L970:
	addq	$2, %rax
	movq	%rax, 16(%r15)
	movl	%ecx, 32(%rbx)
	subl	$48, %ecx
	cmpl	$9, %ecx
	jbe	.L971
	orl	$32, %ecx
	leal	-49(%rcx), %eax
	cmpl	$5, %eax
	ja	.L956
	subl	$39, %ecx
	js	.L956
.L971:
	movq	24(%rbx), %r12
	sall	$4, %edx
	leal	(%rdx,%rcx), %r14d
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L972
.L974:
	movzwl	(%rax), %r13d
.L973:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	%r13d, 32(%rbx)
.L929:
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L944:
	.cfi_restore_state
	cmpb	$0, 48(%r13)
	jne	.L946
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	movq	16(%r13), %rdx
	testb	%al, %al
	jne	.L947
.L946:
	addq	$2, %rdx
	movq	%rdx, 16(%r13)
	movq	24(%rbx), %r13
	movl	$-1, 32(%rbx)
	movq	16(%r13), %rdx
.L936:
	movl	348(%rbx), %ecx
	jmp	.L938
	.p2align 4,,10
	.p2align 3
.L983:
	cmpb	$0, 48(%r15)
	jne	.L963
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*40(%rax)
	movq	16(%r15), %rcx
	testb	%al, %al
	jne	.L962
.L963:
	addq	$2, %rcx
	movq	%rcx, 16(%r15)
	movl	$-1, 32(%rbx)
	.p2align 4,,10
	.p2align 3
.L956:
	movl	348(%rbx), %eax
	testl	%eax, %eax
	jne	.L980
	addl	$3, %r13d
	movl	%r12d, 352(%rbx)
	movl	$-1, %r14d
	movl	$334, 348(%rbx)
	movl	%r13d, 356(%rbx)
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L931:
	cmpb	$0, 48(%r15)
	je	.L985
.L933:
	addq	$2, %rdx
	movq	%rdx, 16(%r15)
	movq	24(%rbx), %r13
	movl	$-1, 32(%rbx)
	movl	348(%rbx), %ecx
	movq	16(%r13), %rdx
.L938:
	movq	32(%r13), %rax
	subq	8(%r13), %rdx
	sarq	%rdx
	addq	%rdx, %rax
	leal	-1(%rax), %edx
	testl	%ecx, %ecx
	jne	.L980
	movl	$334, 348(%rbx)
	movl	$-1, %r14d
	movl	%edx, 352(%rbx)
	movl	%eax, 356(%rbx)
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L935:
	movq	24(%rbx), %r13
	movq	16(%r13), %rdx
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L985:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*40(%rax)
	movq	16(%r15), %rdx
	testb	%al, %al
	jne	.L934
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L952:
	cmpb	$0, 48(%r13)
	movl	$-1, %r12d
	jne	.L953
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	movq	16(%r13), %rdx
	testb	%al, %al
	jne	.L954
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L982:
	cmpb	$0, 48(%r15)
	jne	.L958
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*40(%rax)
	movq	16(%r15), %rdx
	testb	%al, %al
	jne	.L957
.L958:
	addq	$2, %rdx
	movq	%rdx, 16(%r15)
	movl	$-1, 32(%rbx)
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L984:
	cmpb	$0, 48(%r15)
	movl	%edx, -52(%rbp)
	jne	.L968
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r15), %rax
	je	.L968
	movzwl	(%rax), %ecx
	movl	-52(%rbp), %edx
	jmp	.L970
	.p2align 4,,10
	.p2align 3
.L972:
	cmpb	$0, 48(%r12)
	movl	$-1, %r13d
	jne	.L973
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L974
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L968:
	addq	$2, %rax
	movq	%rax, 16(%r15)
	movl	$-1, 32(%rbx)
	jmp	.L956
	.cfi_endproc
.LFE20050:
	.size	_ZN2v88internal7Scanner17ScanUnicodeEscapeILb0EEEiv, .-_ZN2v88internal7Scanner17ScanUnicodeEscapeILb0EEEiv
	.section	.text._ZN2v88internal7Scanner10ScanEscapeILb0EEEbv,"axG",@progbits,_ZN2v88internal7Scanner10ScanEscapeILb0EEEbv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Scanner10ScanEscapeILb0EEEbv
	.type	_ZN2v88internal7Scanner10ScanEscapeILb0EEEbv, @function
_ZN2v88internal7Scanner10ScanEscapeILb0EEEbv:
.LFB20048:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %r13
	movl	32(%rdi), %r12d
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L987
.L990:
	movzwl	(%rax), %edi
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%edi, 32(%rbx)
	cmpl	$13, %r12d
	je	.L991
	cmpl	$10, %r12d
	je	.L991
.L1037:
	leal	-8232(%r12), %eax
	cmpl	$1, %eax
	jbe	.L1055
	leal	-48(%r12), %ecx
	cmpl	$72, %ecx
	ja	.L994
	leaq	.L996(%rip), %rsi
	movl	%ecx, %edx
	movslq	(%rsi,%rdx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal7Scanner10ScanEscapeILb0EEEbv,"aG",@progbits,_ZN2v88internal7Scanner10ScanEscapeILb0EEEbv,comdat
	.align 4
	.align 4
.L996:
	.long	.L1004-.L996
	.long	.L1004-.L996
	.long	.L1004-.L996
	.long	.L1004-.L996
	.long	.L1004-.L996
	.long	.L1004-.L996
	.long	.L1004-.L996
	.long	.L1004-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L1003-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L1002-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L1001-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L994-.L996
	.long	.L1000-.L996
	.long	.L994-.L996
	.long	.L999-.L996
	.long	.L998-.L996
	.long	.L997-.L996
	.long	.L994-.L996
	.long	.L995-.L996
	.section	.text._ZN2v88internal7Scanner10ScanEscapeILb0EEEbv,"axG",@progbits,_ZN2v88internal7Scanner10ScanEscapeILb0EEEbv,comdat
	.p2align 4,,10
	.p2align 3
.L991:
	cmpl	$13, %r12d
	sete	%r12b
	cmpl	$10, %edi
	sete	%al
	andb	%al, %r12b
	jne	.L1057
.L1055:
	movl	$1, %r12d
.L986:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L995:
	.cfi_restore_state
	movq	24(%rbx), %rcx
	leal	-48(%rdi), %r12d
	movq	32(%rcx), %r13
	movq	16(%rcx), %r14
	movq	8(%rcx), %r15
	cmpl	$9, %r12d
	jbe	.L1011
	orl	$32, %r12d
	leal	-49(%r12), %eax
	cmpl	$5, %eax
	ja	.L1012
	subl	$39, %r12d
	js	.L1012
.L1011:
	cmpq	24(%rcx), %r14
	jnb	.L1058
	movzwl	(%r14), %eax
	movq	%r14, %rdx
.L1016:
	addq	$2, %rdx
	movq	%rdx, 16(%rcx)
	movl	%eax, 32(%rbx)
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L1017
	orl	$32, %eax
	leal	-49(%rax), %edx
	cmpl	$5, %edx
	ja	.L1012
	subl	$39, %eax
	js	.L1012
.L1017:
	movq	24(%rbx), %r13
	sall	$4, %r12d
	addl	%eax, %r12d
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L1018
.L1020:
	movzwl	(%rax), %r14d
.L1019:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%r14d, 32(%rbx)
.L994:
	movq	8(%rbx), %rbx
	cmpb	$0, 28(%rbx)
	leaq	8(%rbx), %r13
	je	.L1010
	cmpl	$255, %r12d
	jle	.L1032
	movq	%r13, %rdi
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
.L1010:
	movl	%r12d, %esi
	movq	%r13, %rdi
	movl	$1, %r12d
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	jmp	.L986
	.p2align 4,,10
	.p2align 3
.L987:
	cmpb	$0, 48(%r13)
	jne	.L989
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r13), %rax
	jne	.L990
.L989:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	$-1, 32(%rbx)
	cmpl	$13, %r12d
	je	.L1055
	movl	$-1, %edi
	cmpl	$10, %r12d
	jne	.L1037
	jmp	.L1055
	.p2align 4,,10
	.p2align 3
.L1057:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L1006
.L1008:
	movzwl	(%rax), %r14d
.L1007:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%r14d, 32(%rbx)
	jmp	.L986
.L1004:
	leal	-48(%rdi), %edx
	movl	%ecx, %eax
	cmpl	$7, %edx
	ja	.L1021
	movq	24(%rbx), %r12
	leal	(%rdx,%rcx,8), %r13d
	movq	16(%r12), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L1059
.L1022:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r12)
	leal	-48(%rdx), %eax
	movl	%edx, 32(%rbx)
	cmpl	$7, %eax
	ja	.L1026
	leal	(%rax,%r13,8), %r12d
	cmpl	$255, %r12d
	jg	.L1026
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L1027
.L1029:
	movzwl	(%rax), %r14d
.L1028:
	addq	$2, %rax
	movl	$2, %ecx
	movq	%rax, 16(%r13)
	movl	%r14d, 32(%rbx)
.L1030:
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	movl	$299, 344(%rbx)
	movl	%eax, %edx
	subl	$2, %eax
	subl	%ecx, %edx
	movl	%eax, 340(%rbx)
	subl	$2, %edx
	movl	%edx, 336(%rbx)
	movq	8(%rbx), %rbx
	cmpb	$0, 28(%rbx)
	leaq	8(%rbx), %r13
	je	.L1010
.L1032:
	movl	%r12d, %eax
.L1038:
	movl	%eax, %r12d
	jmp	.L1009
.L1000:
	movq	8(%rbx), %rbx
	movl	$13, %r12d
	cmpb	$0, 28(%rbx)
	leaq	8(%rbx), %r13
	je	.L1060
	.p2align 4,,10
	.p2align 3
.L1009:
	movslq	24(%rbx), %rax
	cmpl	16(%rbx), %eax
	jge	.L1061
.L1034:
	movq	8(%rbx), %rdx
	movb	%r12b, (%rdx,%rax)
	addl	$1, 24(%rbx)
	jmp	.L1055
.L999:
	movq	8(%rbx), %rbx
	movl	$9, %r12d
	cmpb	$0, 28(%rbx)
	leaq	8(%rbx), %r13
	jne	.L1009
	movl	$9, %r12d
	jmp	.L1010
.L998:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner17ScanUnicodeEscapeILb0EEEiv
	movl	%eax, %r12d
	testl	%eax, %eax
	jns	.L994
.L1056:
	xorl	%r12d, %r12d
	jmp	.L986
.L997:
	movq	8(%rbx), %rbx
	movl	$11, %r12d
	cmpb	$0, 28(%rbx)
	leaq	8(%rbx), %r13
	jne	.L1009
	movl	$11, %r12d
	jmp	.L1010
.L1002:
	movq	8(%rbx), %rbx
	movl	$12, %r12d
	cmpb	$0, 28(%rbx)
	leaq	8(%rbx), %r13
	jne	.L1009
	movl	$12, %r12d
	jmp	.L1010
.L1001:
	movq	8(%rbx), %rbx
	movl	$10, %r12d
	cmpb	$0, 28(%rbx)
	leaq	8(%rbx), %r13
	jne	.L1009
	movl	$10, %r12d
	jmp	.L1010
.L1003:
	movq	8(%rbx), %rbx
	movl	$8, %r12d
	cmpb	$0, 28(%rbx)
	leaq	8(%rbx), %r13
	jne	.L1009
	movl	$8, %r12d
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L1061:
	movq	%r13, %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%rbx), %rax
	jmp	.L1034
.L1058:
	cmpb	$0, 48(%rcx)
	jne	.L1046
	movq	(%rcx), %rax
	movq	%rcx, -56(%rbp)
	movq	%rcx, %rdi
	call	*40(%rax)
	movq	-56(%rbp), %rcx
	testb	%al, %al
	jne	.L1062
	movq	16(%rcx), %rax
.L1014:
	addq	$2, %rax
	movq	%rax, 16(%rcx)
	movl	$-1, 32(%rbx)
.L1012:
	movl	348(%rbx), %eax
	testl	%eax, %eax
	jne	.L1056
	movl	$333, 348(%rbx)
	movq	%r14, %rax
	subq	%r15, %rax
	sarq	%rax
	addq	%r13, %rax
	leal	-3(%rax), %edx
	addl	$1, %eax
	movl	%edx, 352(%rbx)
	movl	%eax, 356(%rbx)
	jmp	.L1056
.L1021:
	cmpl	$48, %r12d
	jne	.L1049
	subl	$56, %edi
	cmpl	$1, %edi
	jbe	.L1049
	movq	8(%rbx), %rbx
	cmpb	$0, 28(%rbx)
	leaq	8(%rbx), %r13
	jne	.L1038
	movl	%ecx, %r12d
	jmp	.L1010
.L1060:
	movl	$13, %r12d
	jmp	.L1010
.L1049:
	movl	%ecx, %r12d
	xorl	%ecx, %ecx
	jmp	.L1030
.L1006:
	cmpb	$0, 48(%r13)
	movl	$-1, %r14d
	jne	.L1007
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r13), %rax
	jne	.L1008
	jmp	.L1007
.L1059:
	cmpb	$0, 48(%r12)
	je	.L1063
.L1023:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$-1, 32(%rbx)
.L1026:
	movl	%r13d, %r12d
	movl	$1, %ecx
	jmp	.L1030
.L1018:
	cmpb	$0, 48(%r13)
	movl	$-1, %r14d
	jne	.L1019
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r13), %rax
	jne	.L1020
	jmp	.L1019
.L1027:
	cmpb	$0, 48(%r13)
	movl	$-1, %r14d
	jne	.L1028
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r13), %rax
	jne	.L1029
	jmp	.L1028
.L1063:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	je	.L1023
	jmp	.L1022
.L1062:
	movq	16(%rcx), %rdx
	movzwl	(%rdx), %eax
	jmp	.L1016
.L1046:
	movq	%r14, %rax
	jmp	.L1014
	.cfi_endproc
.LFE20048:
	.size	_ZN2v88internal7Scanner10ScanEscapeILb0EEEbv, .-_ZN2v88internal7Scanner10ScanEscapeILb0EEEbv
	.section	.text._ZN2v88internal7Scanner10ScanStringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner10ScanStringEv
	.type	_ZN2v88internal7Scanner10ScanStringEv, @function
_ZN2v88internal7Scanner10ScanStringEv:
.LFB18144:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movl	32(%rdi), %eax
	movl	%eax, -84(%rbp)
	movq	8(%rdi), %rax
	movl	$0, 24(%rax)
	movb	$1, 28(%rax)
.L1065:
	movq	24(%r12), %r14
.L1131:
	movq	24(%r14), %rax
	movq	16(%r14), %rbx
	movq	%rax, -80(%rbp)
	subq	%rbx, %rax
	movq	%rax, %rdx
	sarq	$3, %rax
	sarq	%rdx
	testq	%rax, %rax
	jle	.L1066
	leaq	(%rbx,%rax,8), %rax
	movq	%rax, -72(%rbp)
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1206:
	movq	8(%r12), %rax
	cmpb	$0, 28(%rax)
	leaq	8(%rax), %r15
	je	.L1073
	cmpl	$255, %esi
	jle	.L1216
	movq	%r15, %rdi
	movl	%esi, -56(%rbp)
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
	movl	-56(%rbp), %esi
.L1073:
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L1071:
	movzwl	2(%rbx), %r15d
	movl	%r15d, %edx
	cmpw	$127, %r15w
	ja	.L1080
	movslq	%r15d, %rax
	testb	$8, 0(%r13,%rax)
	jne	.L1217
.L1080:
	movq	8(%r12), %rax
	cmpb	$0, 28(%rax)
	leaq	8(%rax), %rdi
	je	.L1081
	cmpl	$255, %r15d
	jle	.L1218
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
	movq	-56(%rbp), %rdi
.L1081:
	movl	%r15d, %esi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L1079:
	movzwl	4(%rbx), %esi
	movl	%esi, %r15d
	cmpw	$127, %si
	ja	.L1088
	movslq	%esi, %rax
	testb	$8, 0(%r13,%rax)
	jne	.L1219
.L1088:
	movq	8(%r12), %rax
	cmpb	$0, 28(%rax)
	leaq	8(%rax), %rdi
	je	.L1089
	cmpl	$255, %esi
	jle	.L1220
	movl	%esi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
	movl	-64(%rbp), %esi
	movq	-56(%rbp), %rdi
.L1089:
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L1087:
	movzwl	6(%rbx), %esi
	movl	%esi, %r15d
	cmpw	$127, %si
	ja	.L1096
	movslq	%esi, %rax
	testb	$8, 0(%r13,%rax)
	jne	.L1221
.L1096:
	movq	8(%r12), %rax
	cmpb	$0, 28(%rax)
	leaq	8(%rax), %rdi
	je	.L1097
	cmpl	$255, %esi
	jle	.L1222
	movl	%esi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
	movl	-64(%rbp), %esi
	movq	-56(%rbp), %rdi
.L1097:
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	addq	$8, %rbx
	cmpq	%rbx, -72(%rbp)
	je	.L1223
.L1100:
	movzwl	(%rbx), %esi
	movl	%esi, %edx
	cmpw	$127, %si
	ja	.L1206
	movslq	%esi, %rax
	testb	$8, 0(%r13,%rax)
	je	.L1206
.L1072:
	cmpq	%rbx, 24(%r14)
	jne	.L1129
	.p2align 4,,10
	.p2align 3
.L1228:
	cmpb	$0, 48(%r14)
	movq	%rbx, 16(%r14)
	jne	.L1130
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L1131
	movq	16(%r14), %rbx
.L1130:
	addq	$2, %rbx
	movq	%rbx, 16(%r14)
	movl	$-1, %r14d
.L1132:
	movl	%r14d, 32(%r12)
	cmpl	$92, %r14d
	jne	.L1133
.L1224:
	movq	24(%r12), %rbx
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jnb	.L1134
.L1169:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%r12, %rdi
	movq	%rax, 16(%rbx)
	movl	%edx, 32(%r12)
	call	_ZN2v88internal7Scanner10ScanEscapeILb0EEEbv
	testb	%al, %al
	je	.L1146
	movl	32(%r12), %r14d
	cmpl	$92, %r14d
	je	.L1224
.L1133:
	cmpl	%r14d, -84(%rbp)
	je	.L1225
	leal	1(%r14), %eax
	cmpl	$14, %eax
	ja	.L1226
	movl	$18433, %edx
	btq	%rax, %rdx
	jc	.L1146
	movq	8(%r12), %rbx
	cmpb	$0, 28(%rbx)
	leaq	8(%rbx), %r15
	je	.L1144
.L1157:
	movslq	24(%rbx), %rax
	cmpl	16(%rbx), %eax
	jge	.L1227
.L1148:
	movq	8(%rbx), %rdx
	movb	%r14b, (%rdx,%rax)
	addl	$1, 24(%rbx)
	jmp	.L1065
	.p2align 4,,10
	.p2align 3
.L1217:
	addq	$2, %rbx
	cmpq	%rbx, 24(%r14)
	je	.L1228
.L1129:
	leaq	2(%rbx), %rax
	movq	%rax, 16(%r14)
	movzwl	(%rbx), %r14d
	jmp	.L1132
	.p2align 4,,10
	.p2align 3
.L1219:
	addq	$4, %rbx
	jmp	.L1072
	.p2align 4,,10
	.p2align 3
.L1221:
	addq	$6, %rbx
	jmp	.L1072
	.p2align 4,,10
	.p2align 3
.L1216:
	movslq	24(%rax), %rcx
	cmpl	16(%rax), %ecx
	jge	.L1229
.L1075:
	movq	8(%rax), %rsi
	movb	%dl, (%rsi,%rcx)
	addl	$1, 24(%rax)
	jmp	.L1071
	.p2align 4,,10
	.p2align 3
.L1218:
	movslq	24(%rax), %rcx
	cmpl	16(%rax), %ecx
	jge	.L1230
.L1083:
	movq	8(%rax), %rsi
	movb	%dl, (%rsi,%rcx)
	addl	$1, 24(%rax)
	jmp	.L1079
	.p2align 4,,10
	.p2align 3
.L1220:
	movslq	24(%rax), %rcx
	cmpl	16(%rax), %ecx
	jge	.L1231
.L1091:
	movq	8(%rax), %rsi
	movb	%r15b, (%rsi,%rcx)
	addl	$1, 24(%rax)
	jmp	.L1087
	.p2align 4,,10
	.p2align 3
.L1222:
	movslq	24(%rax), %rcx
	cmpl	16(%rax), %ecx
	jge	.L1232
.L1099:
	movq	8(%rax), %rsi
	addq	$8, %rbx
	movb	%r15b, (%rsi,%rcx)
	addl	$1, 24(%rax)
	cmpq	%rbx, -72(%rbp)
	jne	.L1100
.L1223:
	movq	-80(%rbp), %rdx
	subq	%rbx, %rdx
	sarq	%rdx
.L1066:
	cmpq	$2, %rdx
	je	.L1101
	cmpq	$3, %rdx
	je	.L1102
	cmpq	$1, %rdx
	je	.L1103
.L1168:
	movq	-80(%rbp), %rbx
	jmp	.L1072
	.p2align 4,,10
	.p2align 3
.L1229:
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movl	%edx, -56(%rbp)
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movq	-64(%rbp), %rax
	movl	-56(%rbp), %edx
	movslq	24(%rax), %rcx
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1230:
	movl	%edx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movq	-56(%rbp), %rax
	movl	-64(%rbp), %edx
	movslq	24(%rax), %rcx
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L1231:
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movq	-56(%rbp), %rax
	movslq	24(%rax), %rcx
	jmp	.L1091
	.p2align 4,,10
	.p2align 3
.L1232:
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movq	-56(%rbp), %rax
	movslq	24(%rax), %rcx
	jmp	.L1099
	.p2align 4,,10
	.p2align 3
.L1134:
	cmpb	$0, 48(%rbx)
	je	.L1136
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	movl	$109, %eax
	movl	$-1, 32(%r12)
.L1064:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1136:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L1233
	movq	16(%rbx), %rax
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1146:
	addq	$56, %rsp
	movl	$109, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1233:
	.cfi_restore_state
	addq	$2, 16(%rbx)
	movl	$109, %eax
	movl	$-1, 32(%r12)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1226:
	.cfi_restore_state
	movq	8(%r12), %rbx
	cmpb	$0, 28(%rbx)
	leaq	8(%rbx), %r15
	je	.L1144
	cmpl	$255, %r14d
	jle	.L1157
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
.L1144:
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	jmp	.L1065
	.p2align 4,,10
	.p2align 3
.L1103:
	movzwl	(%rbx), %r15d
	movl	%r15d, %eax
	cmpw	$127, %r15w
	ja	.L1215
.L1122:
	movslq	%r15d, %rdx
	testb	$8, 0(%r13,%rdx)
	jne	.L1072
.L1215:
	movq	8(%r12), %rdx
	cmpb	$0, 28(%rdx)
	leaq	8(%rdx), %rdi
	je	.L1126
	cmpl	$255, %r15d
	jle	.L1234
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
	movq	-56(%rbp), %rdi
.L1126:
	movl	%r15d, %esi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	jmp	.L1168
	.p2align 4,,10
	.p2align 3
.L1102:
	movzwl	(%rbx), %esi
	movl	%esi, %r15d
	cmpw	$127, %si
	ja	.L1211
	movslq	%esi, %rax
	testb	$8, 0(%r13,%rax)
	jne	.L1072
.L1211:
	movq	8(%r12), %rax
	cmpb	$0, 28(%rax)
	leaq	8(%rax), %rdi
	je	.L1109
	cmpl	$255, %esi
	jle	.L1235
	movl	%esi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
	movl	-64(%rbp), %esi
	movq	-56(%rbp), %rdi
.L1109:
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L1108:
	movzwl	2(%rbx), %r15d
	leaq	2(%rbx), %rdx
	movq	%rdx, %rbx
	movl	%r15d, %eax
	cmpw	$127, %r15w
	ja	.L1213
.L1113:
	movslq	%r15d, %rdx
	testb	$8, 0(%r13,%rdx)
	jne	.L1072
.L1213:
	movq	8(%r12), %rdx
	cmpb	$0, 28(%rdx)
	leaq	8(%rdx), %rdi
	je	.L1118
	cmpl	$255, %r15d
	jle	.L1236
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
	movq	-56(%rbp), %rdi
.L1118:
	movl	%r15d, %esi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	movq	%rbx, %rax
.L1117:
	movzwl	2(%rax), %r15d
	addq	$2, %rbx
	movl	%r15d, %eax
	cmpw	$127, %r15w
	jbe	.L1122
	jmp	.L1215
	.p2align 4,,10
	.p2align 3
.L1101:
	movzwl	(%rbx), %r15d
	movl	%r15d, %eax
	cmpw	$127, %r15w
	jbe	.L1113
	jmp	.L1213
.L1234:
	movl	%eax, %ebx
	movslq	24(%rdx), %rax
	cmpl	16(%rdx), %eax
	jge	.L1237
.L1128:
	movq	8(%rdx), %rcx
	movb	%bl, (%rcx,%rax)
	addl	$1, 24(%rdx)
	movq	-80(%rbp), %rbx
	jmp	.L1072
.L1236:
	movl	%eax, %r15d
	movslq	24(%rdx), %rax
	cmpl	16(%rdx), %eax
	jge	.L1238
.L1120:
	movq	8(%rdx), %rcx
	movb	%r15b, (%rcx,%rax)
	movq	%rbx, %rax
	addl	$1, 24(%rdx)
	jmp	.L1117
.L1235:
	movslq	24(%rax), %rdx
	cmpl	16(%rax), %edx
	jge	.L1239
.L1111:
	movq	8(%rax), %rcx
	movb	%r15b, (%rcx,%rdx)
	addl	$1, 24(%rax)
	jmp	.L1108
.L1237:
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movq	-56(%rbp), %rdx
	movslq	24(%rdx), %rax
	jmp	.L1128
.L1227:
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%rbx), %rax
	jmp	.L1148
.L1238:
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movq	-56(%rbp), %rdx
	movslq	24(%rdx), %rax
	jmp	.L1120
.L1239:
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movq	-56(%rbp), %rax
	movslq	24(%rax), %rdx
	jmp	.L1111
.L1225:
	movq	24(%r12), %rbx
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jnb	.L1140
	movzwl	(%rax), %edx
.L1141:
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	movl	$90, %eax
	movl	%edx, 32(%r12)
	jmp	.L1064
.L1140:
	movq	%rbx, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%rbx), %rax
	jmp	.L1141
	.cfi_endproc
.LFE18144:
	.size	_ZN2v88internal7Scanner10ScanStringEv, .-_ZN2v88internal7Scanner10ScanStringEv
	.section	.text._ZN2v88internal7Scanner27ScanIdentifierUnicodeEscapeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner27ScanIdentifierUnicodeEscapeEv
	.type	_ZN2v88internal7Scanner27ScanIdentifierUnicodeEscapeEv, @function
_ZN2v88internal7Scanner27ScanIdentifierUnicodeEscapeEv:
.LFB18160:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	24(%rdi), %rbx
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jnb	.L1241
.L1252:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	movl	%edx, 32(%r12)
	cmpl	$117, %edx
	jne	.L1240
	movq	24(%r12), %rbx
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jnb	.L1246
.L1248:
	movzwl	(%rax), %r13d
.L1247:
	addq	$2, %rax
	movq	%r12, %rdi
	movq	%rax, 16(%rbx)
	movl	%r13d, 32(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Scanner17ScanUnicodeEscapeILb0EEEiv
	.p2align 4,,10
	.p2align 3
.L1241:
	.cfi_restore_state
	cmpb	$0, 48(%rbx)
	je	.L1243
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	movl	$-1, 32(%rdi)
.L1240:
	addq	$8, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1246:
	.cfi_restore_state
	cmpb	$0, 48(%rbx)
	movl	$-1, %r13d
	jne	.L1247
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%rbx), %rax
	jne	.L1248
	jmp	.L1247
	.p2align 4,,10
	.p2align 3
.L1243:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L1245
	movq	16(%rbx), %rax
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1245:
	addq	$2, 16(%rbx)
	movl	$-1, 32(%r12)
	jmp	.L1240
	.cfi_endproc
.LFE18160:
	.size	_ZN2v88internal7Scanner27ScanIdentifierUnicodeEscapeEv, .-_ZN2v88internal7Scanner27ScanIdentifierUnicodeEscapeEv
	.section	.text._ZN2v88internal7Scanner32ScanIdentifierOrKeywordInnerSlowEbb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner32ScanIdentifierOrKeywordInnerSlowEbb
	.type	_ZN2v88internal7Scanner32ScanIdentifierOrKeywordInnerSlowEbb, @function
_ZN2v88internal7Scanner32ScanIdentifierOrKeywordInnerSlowEbb:
.LFB18162:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movl	32(%rdi), %r12d
.L1254:
	cmpl	$92, %r12d
	je	.L1315
.L1319:
	cmpl	$127, %r12d
	ja	.L1316
	movslq	%r12d, %rdx
	testb	$2, (%r14,%rdx)
	je	.L1279
	movq	8(%r15), %rax
	movzbl	28(%rax), %esi
	leaq	8(%rax), %rdi
	testb	%bl, %bl
	jne	.L1296
	testb	%sil, %sil
	jne	.L1297
.L1284:
	movl	%r12d, %esi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L1287:
	movq	24(%r15), %rdx
	movq	16(%rdx), %rax
	cmpq	24(%rdx), %rax
	jnb	.L1288
.L1318:
	movzwl	(%rax), %r12d
	addq	$2, %rax
	movq	%rax, 16(%rdx)
	movl	%r12d, 32(%r15)
	cmpl	$92, %r12d
	jne	.L1319
.L1315:
	movq	%r15, %rdi
	call	_ZN2v88internal7Scanner27ScanIdentifierUnicodeEscapeEv
	movl	%eax, %r12d
	cmpl	$92, %eax
	je	.L1260
	cmpl	$127, %eax
	ja	.L1320
	cltq
	testb	$2, (%r14,%rax)
	je	.L1260
	movq	8(%r15), %r13
	movzbl	28(%r13), %edx
	leaq	8(%r13), %rdi
	testb	%bl, %bl
	jne	.L1321
	testb	%dl, %dl
	jne	.L1262
.L1263:
	movl	%r12d, %esi
	movl	$1, %r13d
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	movl	32(%r15), %r12d
	jmp	.L1254
	.p2align 4,,10
	.p2align 3
.L1288:
	cmpb	$0, 48(%rdx)
	je	.L1290
	addq	$2, %rax
	movq	%rax, 16(%rdx)
	movl	$-1, 32(%r15)
.L1291:
	movl	$-1, %r12d
	.p2align 4,,10
	.p2align 3
.L1316:
	movl	%r12d, %edi
	call	_ZN2v88internal20IsIdentifierPartSlowEi@PLT
	testb	%al, %al
	jne	.L1277
	movl	32(%r15), %eax
	andl	$64512, %eax
	cmpl	$55296, %eax
	jne	.L1279
	movq	24(%r15), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L1272
.L1275:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	%edx, %eax
	andl	$64512, %eax
	cmpl	$56320, %eax
	je	.L1322
.L1276:
	movq	24(%r15), %rdi
	movq	16(%rdi), %rax
	movq	8(%rdi), %rdx
	cmpq	%rdx, %rax
	jbe	.L1278
	subq	$2, %rax
	movq	%rax, 16(%rdi)
.L1279:
	testb	%bl, %bl
	je	.L1302
	movq	8(%r15), %rdx
	movl	$92, %eax
	cmpb	$0, 28(%rdx)
	je	.L1253
	movl	24(%rdx), %edi
	leal	-2(%rdi), %ecx
	cmpl	$8, %ecx
	ja	.L1253
	movq	8(%rdx), %rsi
	leaq	_ZZN2v88internal18PerfectKeywordHash4HashEPKciE11asso_values(%rip), %rdx
	movzbl	1(%rsi), %ecx
	movzbl	(%rsi), %r8d
	movzbl	(%rdx,%rcx), %ecx
	movzbl	(%rdx,%r8), %edx
	addl	%edi, %ecx
	addl	%edx, %ecx
	leaq	_ZN2v88internalL26kPerfectKeywordLengthTableE(%rip), %rdx
	andl	$63, %ecx
	movzbl	(%rdx,%rcx), %edx
	cmpl	%edx, %edi
	je	.L1323
.L1253:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1322:
	.cfi_restore_state
	movl	32(%r15), %edi
	andl	$1023, %edx
	sall	$10, %edi
	andl	$1047552, %edi
	orl	%edx, %edi
	addl	$65536, %edi
	movl	%edi, 32(%r15)
	call	_ZN2v88internal20IsIdentifierPartSlowEi@PLT
	testb	%al, %al
	je	.L1279
.L1277:
	movq	8(%r15), %rax
	movl	32(%r15), %r12d
	movzbl	28(%rax), %esi
	leaq	8(%rax), %rdi
	testb	%bl, %bl
	je	.L1281
	cmpl	$127, %r12d
	jbe	.L1324
.L1281:
	testb	%sil, %sil
	je	.L1298
	cmpl	$255, %r12d
	jle	.L1297
	movq	%rdi, -56(%rbp)
	xorl	%ebx, %ebx
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
	movq	-56(%rbp), %rdi
	jmp	.L1284
	.p2align 4,,10
	.p2align 3
.L1320:
	movl	%eax, %edi
	call	_ZN2v88internal20IsIdentifierPartSlowEi@PLT
	testb	%al, %al
	je	.L1260
	movq	8(%r15), %r13
	movzbl	28(%r13), %ebx
	leaq	8(%r13), %rdi
	testb	%bl, %bl
	je	.L1263
	cmpl	$255, %r12d
	jle	.L1325
	movq	%rdi, -56(%rbp)
	xorl	%ebx, %ebx
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
	movq	-56(%rbp), %rdi
	jmp	.L1263
.L1324:
	movslq	%r12d, %rdx
	.p2align 4,,10
	.p2align 3
.L1296:
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rcx
	movzbl	(%rcx,%rdx), %ebx
	shrb	%bl
	xorl	$1, %ebx
	andl	$1, %ebx
	testb	%sil, %sil
	je	.L1284
	movslq	24(%rax), %rdx
	cmpl	16(%rax), %edx
	jge	.L1326
.L1286:
	movq	8(%rax), %rsi
	movb	%r12b, (%rsi,%rdx)
	addl	$1, 24(%rax)
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1302:
	addq	$24, %rsp
	movl	$92, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1323:
	.cfi_restore_state
	movq	%rcx, %rax
	leaq	_ZN2v88internalL24kPerfectKeywordHashTableE(%rip), %r8
	salq	$4, %rax
	movq	(%r8,%rax), %rdi
	xorl	%eax, %eax
	jmp	.L1294
	.p2align 4,,10
	.p2align 3
.L1327:
	addq	$1, %rax
	cmpb	-1(%rsi,%rax), %dl
	jne	.L1302
.L1294:
	movzbl	(%rdi,%rax), %edx
	testb	%dl, %dl
	jne	.L1327
	salq	$4, %rcx
	movzbl	8(%r8,%rcx), %eax
	leal	-92(%rax), %edx
	cmpb	$5, %dl
	jbe	.L1253
	cmpb	$100, %al
	jne	.L1295
	testb	%r13b, %r13b
	movl	$101, %edx
	cmovne	%edx, %eax
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1297:
	movslq	24(%rax), %rdx
	xorl	%ebx, %ebx
	cmpl	16(%rax), %edx
	jl	.L1286
.L1326:
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movq	-56(%rbp), %rax
	movslq	24(%rax), %rdx
	jmp	.L1286
	.p2align 4,,10
	.p2align 3
.L1321:
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rcx
	movzbl	(%rcx,%rax), %ebx
	shrb	%bl
	xorl	$1, %ebx
	andl	$1, %ebx
	testb	%dl, %dl
	je	.L1263
.L1262:
	movslq	24(%r13), %rax
	cmpl	16(%r13), %eax
	jge	.L1328
.L1265:
	movq	8(%r13), %rdx
	movb	%r12b, (%rdx,%rax)
	addl	$1, 24(%r13)
	movl	$1, %r13d
	movl	32(%r15), %r12d
	jmp	.L1254
	.p2align 4,,10
	.p2align 3
.L1290:
	movq	(%rdx), %rax
	movq	%rdx, -56(%rbp)
	movq	%rdx, %rdi
	call	*40(%rax)
	movq	-56(%rbp), %rdx
	testb	%al, %al
	je	.L1329
	movq	16(%rdx), %rax
	jmp	.L1318
	.p2align 4,,10
	.p2align 3
.L1298:
	xorl	%ebx, %ebx
	jmp	.L1284
	.p2align 4,,10
	.p2align 3
.L1328:
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r13), %rax
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1329:
	addq	$2, 16(%rdx)
	movl	$-1, 32(%r15)
	jmp	.L1291
	.p2align 4,,10
	.p2align 3
.L1272:
	cmpb	$0, 48(%r12)
	jne	.L1274
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L1275
.L1274:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	jmp	.L1276
.L1260:
	movl	$109, %eax
	jmp	.L1253
.L1278:
	movq	32(%rdi), %rsi
	subq	%rdx, %rax
	movq	%rdx, 16(%rdi)
	sarq	%rax
	cmpb	$0, 48(%rdi)
	leaq	-1(%rsi,%rax), %rax
	movq	%rax, 32(%rdi)
	jne	.L1279
	movq	(%rdi), %rax
	call	*40(%rax)
	jmp	.L1279
.L1295:
	testb	%r13b, %r13b
	je	.L1253
	subl	$98, %eax
	cmpb	$2, %al
	sbbl	%eax, %eax
	andl	$-9, %eax
	addl	$110, %eax
	jmp	.L1253
.L1325:
	xorl	%ebx, %ebx
	jmp	.L1262
	.cfi_endproc
.LFE18162:
	.size	_ZN2v88internal7Scanner32ScanIdentifierOrKeywordInnerSlowEbb, .-_ZN2v88internal7Scanner32ScanIdentifierOrKeywordInnerSlowEbb
	.section	.text._ZN2v88internal7Scanner17ScanUnicodeEscapeILb1EEEiv,"axG",@progbits,_ZN2v88internal7Scanner17ScanUnicodeEscapeILb1EEEiv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Scanner17ScanUnicodeEscapeILb1EEEiv
	.type	_ZN2v88internal7Scanner17ScanUnicodeEscapeILb1EEEiv, @function
_ZN2v88internal7Scanner17ScanUnicodeEscapeILb1EEEiv:
.LFB21067:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	$4, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %rax
	movl	32(%rdi), %r15d
	movq	16(%rax), %r13
	subq	8(%rax), %r13
	sarq	%r13
	addq	32(%rax), %r13
	leal	-3(%r13), %eax
	movl	%eax, -60(%rbp)
	cmpl	$123, %r15d
	jne	.L1331
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1366:
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
	movq	-56(%rbp), %rdi
.L1365:
	movl	%r15d, %esi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	cmpq	24(%rdx), %rax
	jnb	.L1369
.L1371:
	movzwl	(%rax), %r15d
.L1370:
	addq	$2, %rax
	movq	%rax, 16(%rdx)
	movl	%r15d, 32(%rbx)
	subl	$1, %r14d
	je	.L1330
.L1331:
	leal	-48(%r15), %eax
	cmpl	$9, %eax
	jbe	.L1363
	orl	$32, %eax
	leal	-49(%rax), %edx
	cmpl	$5, %edx
	ja	.L1364
	subl	$39, %eax
	js	.L1364
.L1363:
	sall	$4, %r12d
	addl	%eax, %r12d
	movq	8(%rbx), %rax
	cmpb	$0, 52(%rax)
	leaq	32(%rax), %rdi
	je	.L1365
	cmpl	$255, %r15d
	jg	.L1366
	movslq	48(%rax), %rdx
	cmpl	40(%rax), %edx
	jge	.L1384
.L1367:
	movq	32(%rax), %rcx
	movb	%r15b, (%rcx,%rdx)
	addl	$1, 48(%rax)
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	cmpq	24(%rdx), %rax
	jb	.L1371
.L1369:
	cmpb	$0, 48(%rdx)
	movl	$-1, %r15d
	jne	.L1370
	movq	(%rdx), %rax
	movq	%rdx, -56(%rbp)
	movq	%rdx, %rdi
	call	*40(%rax)
	movq	-56(%rbp), %rdx
	testb	%al, %al
	movq	16(%rdx), %rax
	jne	.L1371
	jmp	.L1370
	.p2align 4,,10
	.p2align 3
.L1332:
	movl	$123, %esi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L1334:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L1335
.L1338:
	movzwl	(%rax), %r13d
	addq	$2, %rax
	movq	%rax, 16(%r12)
	leal	-48(%r13), %eax
	movl	%r13d, 32(%rbx)
	cmpl	$9, %eax
	jbe	.L1339
	orl	$32, %eax
	leal	-49(%rax), %edx
	cmpl	$5, %edx
	ja	.L1381
	subl	$39, %eax
	js	.L1381
.L1339:
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L1342:
	sall	$4, %r12d
	addl	%eax, %r12d
	cmpl	$1114111, %r12d
	jg	.L1385
	movq	8(%rbx), %r14
	cmpb	$0, 52(%r14)
	leaq	32(%r14), %r15
	je	.L1347
	cmpl	$255, %r13d
	jle	.L1386
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
.L1347:
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L1351
.L1354:
	movzwl	(%rax), %r13d
	addq	$2, %rax
	movq	%rax, 16(%r14)
	leal	-48(%r13), %eax
	movl	%r13d, 32(%rbx)
	cmpl	$9, %eax
	jbe	.L1342
	orl	$32, %eax
	leal	-49(%rax), %edx
	cmpl	$5, %edx
	ja	.L1356
	subl	$39, %eax
	jns	.L1342
.L1356:
	cmpl	$125, %r13d
	jne	.L1381
	movq	8(%rbx), %r13
	cmpb	$0, 52(%r13)
	leaq	32(%r13), %rdi
	je	.L1357
	movslq	48(%r13), %rax
	cmpl	40(%r13), %eax
	jl	.L1358
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	48(%r13), %rax
.L1358:
	movq	32(%r13), %rdx
	movb	$125, (%rdx,%rax)
	addl	$1, 48(%r13)
.L1359:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L1360
.L1362:
	movzwl	(%rax), %r14d
.L1361:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%r14d, 32(%rbx)
.L1330:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1384:
	.cfi_restore_state
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movq	-56(%rbp), %rax
	movslq	48(%rax), %rdx
	jmp	.L1367
	.p2align 4,,10
	.p2align 3
.L1364:
	movl	348(%rbx), %eax
	testl	%eax, %eax
	jne	.L1382
	movl	-60(%rbp), %eax
	addl	$3, %r13d
	movl	$334, 348(%rbx)
	movl	%r13d, 356(%rbx)
	movl	%eax, 352(%rbx)
.L1382:
	movl	$-1, %r12d
	jmp	.L1330
	.p2align 4,,10
	.p2align 3
.L1383:
	movq	8(%rdi), %r12
	cmpb	$0, 52(%r12)
	leaq	32(%r12), %rdi
	je	.L1332
	movslq	48(%r12), %rax
	cmpl	40(%r12), %eax
	jge	.L1387
.L1333:
	movq	32(%r12), %rdx
	movb	$123, (%rdx,%rax)
	addl	$1, 48(%r12)
	jmp	.L1334
	.p2align 4,,10
	.p2align 3
.L1386:
	movslq	48(%r14), %rax
	cmpl	40(%r14), %eax
	jge	.L1388
.L1349:
	movq	32(%r14), %rdx
	movb	%r13b, (%rdx,%rax)
	addl	$1, 48(%r14)
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jb	.L1354
.L1351:
	cmpb	$0, 48(%r14)
	je	.L1389
.L1353:
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	$-1, 32(%rbx)
.L1381:
	movq	24(%rbx), %rdx
	movl	348(%rbx), %ecx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	leal	-1(%rax), %edx
	testl	%ecx, %ecx
	jne	.L1382
	movl	$334, 348(%rbx)
	movl	$-1, %r12d
	movl	%edx, 352(%rbx)
	movl	%eax, 356(%rbx)
	jmp	.L1330
	.p2align 4,,10
	.p2align 3
.L1388:
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	48(%r14), %rax
	jmp	.L1349
	.p2align 4,,10
	.p2align 3
.L1389:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r14), %rax
	jne	.L1354
	jmp	.L1353
	.p2align 4,,10
	.p2align 3
.L1357:
	movl	$125, %esi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	jmp	.L1359
	.p2align 4,,10
	.p2align 3
.L1385:
	movl	348(%rbx), %edx
	testl	%edx, %edx
	jne	.L1382
	movq	24(%rbx), %rdx
	movl	-60(%rbp), %esi
	movq	32(%rdx), %rcx
	movq	16(%rdx), %rax
	movq	8(%rdx), %rdx
	movl	%esi, 352(%rbx)
	movl	$335, 348(%rbx)
	subq	%rdx, %rax
	sarq	%rax
	addq	%rcx, %rax
	movl	%eax, 356(%rbx)
	jmp	.L1382
	.p2align 4,,10
	.p2align 3
.L1387:
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	48(%r12), %rax
	jmp	.L1333
	.p2align 4,,10
	.p2align 3
.L1335:
	cmpb	$0, 48(%r12)
	jne	.L1337
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L1338
.L1337:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$-1, 32(%rbx)
	jmp	.L1381
	.p2align 4,,10
	.p2align 3
.L1360:
	cmpb	$0, 48(%r13)
	movl	$-1, %r14d
	jne	.L1361
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r13), %rax
	jne	.L1362
	jmp	.L1361
	.cfi_endproc
.LFE21067:
	.size	_ZN2v88internal7Scanner17ScanUnicodeEscapeILb1EEEiv, .-_ZN2v88internal7Scanner17ScanUnicodeEscapeILb1EEEiv
	.section	.text._ZN2v88internal7Scanner10ScanEscapeILb1EEEbv,"axG",@progbits,_ZN2v88internal7Scanner10ScanEscapeILb1EEEbv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Scanner10ScanEscapeILb1EEEbv
	.type	_ZN2v88internal7Scanner10ScanEscapeILb1EEEbv, @function
_ZN2v88internal7Scanner10ScanEscapeILb1EEEbv:
.LFB20049:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movl	32(%rdi), %r12d
	cmpb	$0, 52(%rbx)
	leaq	32(%rbx), %r13
	je	.L1391
	cmpl	$255, %r12d
	jle	.L1454
	movq	%r13, %rdi
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
.L1391:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	movq	24(%r15), %rbx
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jnb	.L1395
.L1397:
	movzwl	(%rax), %esi
.L1396:
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	leal	-48(%r12), %eax
	movl	%esi, 32(%r15)
	cmpl	$72, %eax
	ja	.L1452
	leaq	.L1400(%rip), %r8
	movl	%eax, %edi
	movslq	(%r8,%rdi,4), %rcx
	addq	%r8, %rcx
	notrack jmp	*%rcx
	.section	.rodata._ZN2v88internal7Scanner10ScanEscapeILb1EEEbv,"aG",@progbits,_ZN2v88internal7Scanner10ScanEscapeILb1EEEbv,comdat
	.align 4
	.align 4
.L1400:
	.long	.L1408-.L1400
	.long	.L1408-.L1400
	.long	.L1408-.L1400
	.long	.L1408-.L1400
	.long	.L1408-.L1400
	.long	.L1408-.L1400
	.long	.L1408-.L1400
	.long	.L1408-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1407-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1406-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1405-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1452-.L1400
	.long	.L1404-.L1400
	.long	.L1452-.L1400
	.long	.L1403-.L1400
	.long	.L1402-.L1400
	.long	.L1401-.L1400
	.long	.L1452-.L1400
	.long	.L1399-.L1400
	.section	.text._ZN2v88internal7Scanner10ScanEscapeILb1EEEbv,"axG",@progbits,_ZN2v88internal7Scanner10ScanEscapeILb1EEEbv,comdat
	.p2align 4,,10
	.p2align 3
.L1402:
	movq	%r15, %rdi
	call	_ZN2v88internal7Scanner17ScanUnicodeEscapeILb1EEEiv
	movl	%eax, %r12d
	testl	%eax, %eax
	js	.L1453
	.p2align 4,,10
	.p2align 3
.L1452:
	movq	8(%r15), %rbx
.L1409:
	cmpb	$0, 28(%rbx)
	leaq	8(%rbx), %r13
	je	.L1411
	cmpl	$255, %r12d
	jle	.L1410
	movq	%r13, %rdi
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
.L1411:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	movl	$1, %eax
.L1390:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1454:
	.cfi_restore_state
	movslq	48(%rbx), %rax
	movl	%r12d, %r14d
	cmpl	40(%rbx), %eax
	jge	.L1455
.L1393:
	movq	32(%rbx), %rcx
	movb	%r14b, (%rcx,%rax)
	addl	$1, 48(%rbx)
	movq	24(%r15), %rbx
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jb	.L1397
.L1395:
	cmpb	$0, 48(%rbx)
	movl	$-1, %esi
	jne	.L1396
	movq	(%rbx), %rax
	movl	%esi, -56(%rbp)
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%rbx), %rax
	jne	.L1397
	movl	-56(%rbp), %esi
	jmp	.L1396
	.p2align 4,,10
	.p2align 3
.L1401:
	movq	8(%r15), %rbx
	movl	$11, %r12d
	cmpb	$0, 28(%rbx)
	leaq	8(%rbx), %r13
	je	.L1456
	.p2align 4,,10
	.p2align 3
.L1410:
	movslq	24(%rbx), %rax
	cmpl	16(%rbx), %eax
	jge	.L1457
.L1435:
	movq	8(%rbx), %rdx
	movb	%r12b, (%rdx,%rax)
	movl	$1, %eax
	addl	$1, 24(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1408:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L1432
	.p2align 4,,10
	.p2align 3
.L1426:
	movl	%esi, -60(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
	movl	-60(%rbp), %esi
	movq	-56(%rbp), %rdi
.L1425:
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L1428:
	movq	24(%r15), %rbx
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jnb	.L1429
.L1431:
	movzwl	(%rax), %esi
.L1430:
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	movl	%esi, 32(%r15)
	cmpl	$1, %r13d
	je	.L1458
	movl	%r14d, %eax
	movl	$1, %r13d
.L1432:
	leal	-48(%rsi), %ecx
	movq	8(%r15), %rbx
	cmpl	$7, %ecx
	ja	.L1424
	leal	(%rcx,%rax,8), %r14d
	cmpl	$255, %r14d
	jg	.L1424
	cmpb	$0, 52(%rbx)
	leaq	32(%rbx), %rdi
	je	.L1425
	cmpl	$255, %esi
	jg	.L1426
	movslq	48(%rbx), %rax
	cmpl	40(%rbx), %eax
	jge	.L1459
.L1427:
	movq	32(%rbx), %rcx
	movb	%sil, (%rcx,%rax)
	addl	$1, 48(%rbx)
	jmp	.L1428
	.p2align 4,,10
	.p2align 3
.L1455:
	movq	%r13, %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	48(%rbx), %rax
	jmp	.L1393
	.p2align 4,,10
	.p2align 3
.L1457:
	movq	%r13, %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%rbx), %rax
	jmp	.L1435
	.p2align 4,,10
	.p2align 3
.L1406:
	movq	8(%r15), %rbx
	movl	$12, %r12d
	cmpb	$0, 28(%rbx)
	leaq	8(%rbx), %r13
	jne	.L1410
	movl	$12, %r12d
	jmp	.L1411
	.p2align 4,,10
	.p2align 3
.L1405:
	movq	8(%r15), %rbx
	movl	$10, %r12d
	cmpb	$0, 28(%rbx)
	leaq	8(%rbx), %r13
	jne	.L1410
	movl	$10, %r12d
	jmp	.L1411
	.p2align 4,,10
	.p2align 3
.L1404:
	movq	8(%r15), %rbx
	movl	$13, %r12d
	cmpb	$0, 28(%rbx)
	leaq	8(%rbx), %r13
	jne	.L1410
	movl	$13, %r12d
	jmp	.L1411
	.p2align 4,,10
	.p2align 3
.L1403:
	movq	8(%r15), %rbx
	movl	$9, %r12d
	cmpb	$0, 28(%rbx)
	leaq	8(%rbx), %r13
	jne	.L1410
	movl	$9, %r12d
	jmp	.L1411
	.p2align 4,,10
	.p2align 3
.L1407:
	movq	8(%r15), %rbx
	movl	$8, %r12d
	cmpb	$0, 28(%rbx)
	leaq	8(%rbx), %r13
	jne	.L1410
	movl	$8, %r12d
	jmp	.L1411
	.p2align 4,,10
	.p2align 3
.L1399:
	movq	24(%r15), %rax
	movl	$2, %ebx
	xorl	%r12d, %r12d
	movq	32(%rax), %rdx
	movq	16(%rax), %r13
	movq	8(%rax), %rax
	movq	%rdx, -72(%rbp)
	movq	%rax, -80(%rbp)
	jmp	.L1423
	.p2align 4,,10
	.p2align 3
.L1417:
	movq	%r14, %rdi
	movl	%esi, -56(%rbp)
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
	movl	-56(%rbp), %esi
.L1416:
	movq	%r14, %rdi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L1419:
	movq	24(%r15), %rcx
	movq	16(%rcx), %rax
	cmpq	24(%rcx), %rax
	jnb	.L1420
.L1422:
	movzwl	(%rax), %esi
.L1421:
	addq	$2, %rax
	movq	%rax, 16(%rcx)
	movl	%esi, 32(%r15)
	cmpl	$1, %ebx
	je	.L1452
	movl	$1, %ebx
.L1423:
	leal	-48(%rsi), %ecx
	cmpl	$9, %ecx
	jbe	.L1414
	orl	$32, %ecx
	leal	-49(%rcx), %eax
	cmpl	$5, %eax
	ja	.L1415
	subl	$39, %ecx
	js	.L1415
.L1414:
	movq	8(%r15), %rax
	sall	$4, %r12d
	addl	%ecx, %r12d
	cmpb	$0, 52(%rax)
	leaq	32(%rax), %r14
	je	.L1416
	cmpl	$255, %esi
	jg	.L1417
	movslq	48(%rax), %rcx
	cmpl	40(%rax), %ecx
	jge	.L1460
.L1418:
	movq	32(%rax), %rdi
	movb	%sil, (%rdi,%rcx)
	addl	$1, 48(%rax)
	jmp	.L1419
	.p2align 4,,10
	.p2align 3
.L1415:
	movl	348(%r15), %eax
	testl	%eax, %eax
	jne	.L1453
	movq	-72(%rbp), %r14
	subq	-80(%rbp), %r13
	movl	$333, 348(%r15)
	sarq	%r13
	addq	%r13, %r14
	leal	-3(%r14), %eax
	addl	$1, %r14d
	movl	%eax, 352(%r15)
	movl	%r14d, 356(%r15)
.L1453:
	xorl	%eax, %eax
	jmp	.L1390
	.p2align 4,,10
	.p2align 3
.L1420:
	cmpb	$0, 48(%rcx)
	movl	$-1, %esi
	jne	.L1421
	movq	(%rcx), %rax
	movq	%rcx, -56(%rbp)
	movq	%rcx, %rdi
	movl	%esi, -60(%rbp)
	call	*40(%rax)
	movq	-56(%rbp), %rcx
	testb	%al, %al
	movq	16(%rcx), %rax
	jne	.L1422
	movl	-60(%rbp), %esi
	jmp	.L1421
	.p2align 4,,10
	.p2align 3
.L1429:
	cmpb	$0, 48(%rbx)
	movl	$-1, %esi
	jne	.L1430
	movq	(%rbx), %rax
	movl	%esi, -56(%rbp)
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%rbx), %rax
	jne	.L1431
	movl	-56(%rbp), %esi
	jmp	.L1430
	.p2align 4,,10
	.p2align 3
.L1460:
	movq	%r14, %rdi
	movb	%sil, -60(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movq	-56(%rbp), %rax
	movzbl	-60(%rbp), %esi
	movslq	48(%rax), %rcx
	jmp	.L1418
	.p2align 4,,10
	.p2align 3
.L1424:
	cmpl	$48, %r12d
	jne	.L1433
	testl	%r13d, %r13d
	jne	.L1433
	subl	$56, %esi
	movl	%eax, %r12d
	cmpl	$1, %esi
	ja	.L1409
.L1433:
	movq	24(%r15), %rsi
	movl	%eax, %r12d
	movq	16(%rsi), %rcx
	subq	8(%rsi), %rcx
	sarq	%rcx
	addq	32(%rsi), %rcx
	movl	$301, 344(%r15)
	movl	%ecx, %esi
	subl	$2, %ecx
	subl	%r13d, %esi
	movl	%ecx, 340(%r15)
	subl	$2, %esi
	movl	%esi, 336(%r15)
	jmp	.L1409
	.p2align 4,,10
	.p2align 3
.L1459:
	movb	%sil, -56(%rbp)
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	48(%rbx), %rax
	movzbl	-56(%rbp), %esi
	jmp	.L1427
	.p2align 4,,10
	.p2align 3
.L1456:
	movl	$11, %r12d
	jmp	.L1411
	.p2align 4,,10
	.p2align 3
.L1458:
	movq	8(%r15), %rbx
	movl	%r14d, %eax
	movl	$2, %r13d
	jmp	.L1433
	.cfi_endproc
.LFE20049:
	.size	_ZN2v88internal7Scanner10ScanEscapeILb1EEEbv, .-_ZN2v88internal7Scanner10ScanEscapeILb1EEEbv
	.section	.text._ZN2v88internal7Scanner16ScanTemplateSpanEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner16ScanTemplateSpanEv
	.type	_ZN2v88internal7Scanner16ScanTemplateSpanEv, @function
_ZN2v88internal7Scanner16ScanTemplateSpanEv:
.LFB18147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movabsq	$-4294967296, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movl	348(%rdi), %eax
	movl	%eax, -120(%rbp)
	movq	352(%rdi), %rax
	movq	%r12, 348(%rdi)
	movq	%rax, -88(%rbp)
	movl	344(%rdi), %eax
	movl	$0, 356(%rdi)
	movl	%eax, -116(%rbp)
	movq	336(%rdi), %rax
	movl	$0, 344(%rdi)
	movq	%rax, -56(%rbp)
	movl	$4294967295, %eax
	movq	%rax, 336(%rdi)
	movq	8(%rdi), %rax
	movl	$0, 24(%rax)
	movb	$1, 28(%rax)
	movq	8(%rdi), %rax
	movl	$0, 48(%rax)
	movb	$1, 52(%rax)
	movl	32(%rdi), %r15d
	cmpl	$96, %r15d
	je	.L1465
	.p2align 4,,10
	.p2align 3
.L1462:
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	cmpl	$36, %r15d
	je	.L1568
	cmpl	$92, %r15d
	jne	.L1569
	cmpq	%rax, 24(%rdx)
	jbe	.L1484
.L1486:
	movzwl	(%rax), %r15d
.L1485:
	addq	$2, %rax
	movq	%rax, 16(%rdx)
	movl	%r15d, 32(%rbx)
	movq	8(%rbx), %r15
	cmpb	$0, 52(%r15)
	leaq	32(%r15), %rdi
	je	.L1487
	movslq	48(%r15), %rax
	cmpl	40(%r15), %eax
	jge	.L1570
.L1488:
	movq	32(%r15), %rdx
	movb	$92, (%rdx,%rax)
	addl	$1, 48(%r15)
.L1489:
	movl	32(%rbx), %r15d
	cmpl	$13, %r15d
	je	.L1490
	cmpl	$10, %r15d
	je	.L1490
	leal	-8232(%r15), %eax
	cmpl	$1, %eax
	jbe	.L1490
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner10ScanEscapeILb1EEEbv
	movl	348(%rbx), %edx
	movq	8(%rbx), %rax
	testl	%edx, %edx
	je	.L1491
	movl	60(%rax), %esi
	testl	%esi, %esi
	je	.L1571
.L1507:
	movq	%r12, 348(%rbx)
	movl	$0, 356(%rbx)
.L1491:
	movl	344(%rbx), %edx
	testl	%edx, %edx
	je	.L1506
	movl	60(%rax), %ecx
	testl	%ecx, %ecx
	je	.L1572
.L1508:
	movl	$0, 344(%rbx)
	movl	$4294967295, %eax
	movq	%rax, 336(%rbx)
	.p2align 4,,10
	.p2align 3
.L1506:
	movl	32(%rbx), %r15d
	cmpl	$96, %r15d
	jne	.L1462
.L1465:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L1573
.L1467:
	movzwl	(%rax), %r15d
.L1466:
	addq	$2, %rax
	movl	$1, %r8d
	movq	%rax, 16(%r12)
	movq	24(%rbx), %rdx
	movl	%r15d, 32(%rbx)
	movq	16(%rdx), %rax
.L1468:
	subq	8(%rdx), %rax
	movq	8(%rbx), %rcx
	sarq	%rax
	addq	32(%rdx), %rax
	subl	$1, %eax
	movl	%eax, 4(%rcx)
	movq	8(%rbx), %rax
	movb	%r8b, 56(%rax)
	movl	-116(%rbp), %eax
	movl	%eax, 344(%rbx)
	movq	-56(%rbp), %rax
	movq	%rax, 336(%rbx)
	movl	-120(%rbp), %eax
	movl	%eax, 348(%rbx)
	movq	-88(%rbp), %rax
	movq	%rax, 352(%rbx)
	addq	$104, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1568:
	.cfi_restore_state
	movq	24(%rdx), %rcx
	cmpq	%rax, %rcx
	jbe	.L1470
	movzwl	(%rax), %esi
	cmpl	$123, %esi
	je	.L1574
.L1526:
	cmpq	%rcx, %rax
	jnb	.L1472
.L1562:
	movzwl	(%rax), %ecx
	addq	$2, %rax
	movq	%rax, 16(%rdx)
	movl	%ecx, 32(%rbx)
	cmpl	$13, %r15d
	je	.L1575
.L1511:
	movq	8(%rbx), %rax
	cmpb	$0, 52(%rax)
	leaq	32(%rax), %r13
	je	.L1519
.L1578:
	cmpl	$255, %r15d
	jle	.L1576
	movq	%r13, %rdi
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
	movq	%r13, %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	movq	8(%rbx), %rax
	cmpb	$0, 28(%rax)
	leaq	8(%rax), %r13
	je	.L1523
.L1524:
	movq	%r13, %rdi
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
.L1523:
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	jmp	.L1506
	.p2align 4,,10
	.p2align 3
.L1569:
	testl	%r15d, %r15d
	js	.L1534
	movq	24(%rdx), %rcx
	cmpq	%rcx, %rax
	jb	.L1562
.L1472:
	cmpb	$0, 48(%rdx)
	jne	.L1577
	movq	(%rdx), %rax
	movq	%rdx, -128(%rbp)
	movq	%rdx, %rdi
	call	*40(%rax)
	movq	-128(%rbp), %rdx
	testb	%al, %al
	jne	.L1514
	addq	$2, 16(%rdx)
	movl	$-1, 32(%rbx)
	cmpl	$13, %r15d
	je	.L1515
	movq	8(%rbx), %rax
	cmpb	$0, 52(%rax)
	leaq	32(%rax), %r13
	jne	.L1578
	.p2align 4,,10
	.p2align 3
.L1519:
	movq	%r13, %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	movq	8(%rbx), %rax
	cmpb	$0, 28(%rax)
	leaq	8(%rax), %r13
	je	.L1523
	cmpl	$255, %r15d
	jg	.L1524
	movl	%r15d, %r14d
	jmp	.L1522
	.p2align 4,,10
	.p2align 3
.L1490:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L1493
.L1561:
	movzwl	(%rax), %ecx
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%ecx, 32(%rbx)
	cmpl	$13, %r15d
	je	.L1579
.L1496:
	movq	8(%rbx), %r14
	cmpb	$0, 52(%r14)
	leaq	32(%r14), %r13
	je	.L1523
	cmpl	$255, %r15d
	jg	.L1524
	.p2align 4,,10
	.p2align 3
.L1497:
	movslq	48(%r14), %rdx
	cmpl	40(%r14), %edx
	jge	.L1580
	movq	32(%r14), %rcx
	movb	%r15b, (%rcx,%rdx)
	addl	$1, 48(%r14)
	jmp	.L1506
	.p2align 4,,10
	.p2align 3
.L1575:
	cmpl	$10, %ecx
	je	.L1581
.L1515:
	movq	8(%rbx), %rax
	movl	$10, %r14d
	movl	$10, %ecx
	cmpb	$0, 52(%rax)
	leaq	32(%rax), %r13
	je	.L1513
.L1512:
	movslq	48(%rax), %rdx
	cmpl	40(%rax), %edx
	jge	.L1582
.L1521:
	movq	32(%rax), %rdi
	movl	%ecx, %r15d
	movb	%r14b, (%rdi,%rdx)
	addl	$1, 48(%rax)
	movq	8(%rbx), %rax
	cmpb	$0, 28(%rax)
	leaq	8(%rax), %r13
	je	.L1523
.L1522:
	movslq	24(%rax), %rdx
	cmpl	16(%rax), %edx
	jge	.L1583
.L1525:
	movq	8(%rax), %rcx
	movb	%r14b, (%rcx,%rdx)
	addl	$1, 24(%rax)
	jmp	.L1506
	.p2align 4,,10
	.p2align 3
.L1487:
	movl	$92, %esi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	jmp	.L1489
	.p2align 4,,10
	.p2align 3
.L1579:
	cmpl	$10, %ecx
	je	.L1584
.L1565:
	movq	8(%rbx), %r14
	movl	$10, %r15d
	cmpb	$0, 52(%r14)
	leaq	32(%r14), %r13
	jne	.L1497
.L1563:
	movl	$10, %r15d
	jmp	.L1523
	.p2align 4,,10
	.p2align 3
.L1570:
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	48(%r15), %rax
	jmp	.L1488
	.p2align 4,,10
	.p2align 3
.L1581:
	movq	24(%rbx), %r15
	movq	16(%r15), %rax
	cmpq	24(%r15), %rax
	jnb	.L1516
.L1518:
	movzwl	(%rax), %r13d
.L1517:
	addq	$2, %rax
	movl	$10, %r14d
	movq	%rax, 16(%r15)
	movq	8(%rbx), %rax
	movl	%r13d, 32(%rbx)
	cmpb	$0, 52(%rax)
	leaq	32(%rax), %r13
	jne	.L1512
.L1513:
	movq	%r13, %rdi
	movl	$10, %esi
	movl	$10, %r14d
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
	movq	8(%rbx), %rax
	cmpb	$0, 28(%rax)
	leaq	8(%rax), %r13
	je	.L1563
	movslq	24(%rax), %rdx
	cmpl	16(%rax), %edx
	jl	.L1525
.L1583:
	movq	%r13, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movq	-128(%rbp), %rax
	movslq	24(%rax), %rdx
	jmp	.L1525
	.p2align 4,,10
	.p2align 3
.L1582:
	movq	%r13, %rdi
	movq	%rax, -136(%rbp)
	movl	%ecx, -128(%rbp)
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movq	-136(%rbp), %rax
	movl	-128(%rbp), %ecx
	movslq	48(%rax), %rdx
	jmp	.L1521
	.p2align 4,,10
	.p2align 3
.L1572:
	movl	%edx, 60(%rax)
	movq	336(%rbx), %rdx
	movq	%rdx, 64(%rax)
	jmp	.L1508
	.p2align 4,,10
	.p2align 3
.L1571:
	movl	%edx, 60(%rax)
	movq	352(%rbx), %rdx
	movq	%rdx, 64(%rax)
	movq	8(%rbx), %rax
	jmp	.L1507
	.p2align 4,,10
	.p2align 3
.L1577:
	addq	$2, %rax
	movq	%rax, 16(%rdx)
	movl	$-1, 32(%rbx)
	cmpl	$13, %r15d
	jne	.L1511
	jmp	.L1515
	.p2align 4,,10
	.p2align 3
.L1580:
	movq	%r13, %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	48(%r14), %rdx
	movq	32(%r14), %rcx
	movb	%r15b, (%rcx,%rdx)
	addl	$1, 48(%r14)
	jmp	.L1506
	.p2align 4,,10
	.p2align 3
.L1470:
	cmpb	$0, 48(%rdx)
	jne	.L1472
	movq	(%rdx), %rax
	movq	%rdx, -128(%rbp)
	movq	%rdx, %rdi
	call	*40(%rax)
	movq	-128(%rbp), %rdx
	testb	%al, %al
	je	.L1585
	movq	16(%rdx), %rax
	movq	24(%rbx), %rdx
	movzwl	(%rax), %esi
	movq	24(%rdx), %rcx
	movq	16(%rdx), %rax
	cmpl	$123, %esi
	jne	.L1526
	.p2align 4,,10
	.p2align 3
.L1574:
	cmpq	%rcx, %rax
	jnb	.L1586
.L1480:
	movzwl	(%rax), %r12d
.L1479:
	addq	$2, %rax
	movq	%rax, 16(%rdx)
	movl	%r12d, 32(%rbx)
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L1481
.L1483:
	movzwl	(%rax), %r15d
.L1482:
	addq	$2, %rax
	xorl	%r8d, %r8d
	movq	%rax, 16(%r12)
	movq	24(%rbx), %rdx
	movl	%r15d, 32(%rbx)
	movq	16(%rdx), %rax
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1484:
	cmpb	$0, 48(%rdx)
	movl	$-1, %r15d
	jne	.L1485
	movq	(%rdx), %rax
	movq	%rdx, -128(%rbp)
	movq	%rdx, %rdi
	call	*40(%rax)
	movq	-128(%rbp), %rdx
	testb	%al, %al
	movq	16(%rdx), %rax
	jne	.L1486
	jmp	.L1485
	.p2align 4,,10
	.p2align 3
.L1514:
	movq	16(%rdx), %rax
	jmp	.L1562
	.p2align 4,,10
	.p2align 3
.L1584:
	movq	24(%rbx), %r15
	movq	16(%r15), %rax
	cmpq	24(%r15), %rax
	jnb	.L1501
.L1503:
	movzwl	(%rax), %r13d
.L1502:
	addq	$2, %rax
	movq	%rax, 16(%r15)
	movl	%r13d, 32(%rbx)
	jmp	.L1565
	.p2align 4,,10
	.p2align 3
.L1534:
	xorl	%r8d, %r8d
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1493:
	cmpb	$0, 48(%r13)
	jne	.L1587
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L1499
	addq	$2, 16(%r13)
	movl	$-1, 32(%rbx)
	cmpl	$13, %r15d
	jne	.L1496
	jmp	.L1565
	.p2align 4,,10
	.p2align 3
.L1587:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	$-1, 32(%rbx)
	cmpl	$13, %r15d
	jne	.L1496
	jmp	.L1565
.L1585:
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	movq	24(%rdx), %rcx
	jmp	.L1526
.L1573:
	cmpb	$0, 48(%r12)
	movl	$-1, %r15d
	jne	.L1466
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L1467
	jmp	.L1466
	.p2align 4,,10
	.p2align 3
.L1499:
	movq	16(%r13), %rax
	jmp	.L1561
.L1516:
	cmpb	$0, 48(%r15)
	movl	$-1, %r13d
	jne	.L1517
	movq	(%r15), %rax
	movl	%ecx, -128(%rbp)
	movq	%r15, %rdi
	call	*40(%rax)
	movl	-128(%rbp), %ecx
	testb	%al, %al
	movq	16(%r15), %rax
	jne	.L1518
	jmp	.L1517
.L1576:
	movl	%r15d, %r14d
	movl	%r15d, %ecx
	jmp	.L1512
.L1501:
	cmpb	$0, 48(%r15)
	movl	$-1, %r13d
	jne	.L1502
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r15), %rax
	jne	.L1503
	jmp	.L1502
.L1481:
	cmpb	$0, 48(%r12)
	movl	$-1, %r15d
	jne	.L1482
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L1483
	jmp	.L1482
.L1586:
	cmpb	$0, 48(%rdx)
	movl	$-1, %r12d
	jne	.L1479
	movq	(%rdx), %rax
	movq	%rdx, -128(%rbp)
	movq	%rdx, %rdi
	call	*40(%rax)
	movq	-128(%rbp), %rdx
	testb	%al, %al
	movq	16(%rdx), %rax
	jne	.L1480
	jmp	.L1479
	.cfi_endproc
.LFE18147:
	.size	_ZN2v88internal7Scanner16ScanTemplateSpanEv, .-_ZN2v88internal7Scanner16ScanTemplateSpanEv
	.section	.text._ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal20Utf16CharacterStream12AdvanceUntilIZNS6_7Scanner28ScanIdentifierOrKeywordInnerEvEUliE_EEiT_EUltE_EEESB_SB_SB_T0_St26random_access_iterator_tag,"axG",@progbits,_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal20Utf16CharacterStream12AdvanceUntilIZNS6_7Scanner28ScanIdentifierOrKeywordInnerEvEUliE_EEiT_EUltE_EEESB_SB_SB_T0_St26random_access_iterator_tag,comdat
	.p2align 4
	.weak	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal20Utf16CharacterStream12AdvanceUntilIZNS6_7Scanner28ScanIdentifierOrKeywordInnerEvEUliE_EEiT_EUltE_EEESB_SB_SB_T0_St26random_access_iterator_tag
	.type	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal20Utf16CharacterStream12AdvanceUntilIZNS6_7Scanner28ScanIdentifierOrKeywordInnerEvEUliE_EEiT_EUltE_EEESB_SB_SB_T0_St26random_access_iterator_tag, @function
_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal20Utf16CharacterStream12AdvanceUntilIZNS6_7Scanner28ScanIdentifierOrKeywordInnerEvEUliE_EEiT_EUltE_EEESB_SB_SB_T0_St26random_access_iterator_tag:
.LFB21833:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movq	%rsi, %rdx
	subq	%rdi, %rdx
	movq	%rdx, %rax
	sarq	$3, %rdx
	subq	$24, %rsp
	sarq	%rax
	movq	%rsi, -64(%rbp)
	testq	%rdx, %rdx
	jle	.L1589
	leaq	(%rdi,%rdx,8), %rcx
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L1601:
	movzwl	(%r12), %esi
	movq	8(%rbx), %rdi
	movl	%esi, %r15d
	movzbl	(%rdi), %eax
	cmpw	$127, %si
	ja	.L1626
	movslq	%esi, %rsi
	movzbl	(%r14,%rsi), %esi
	orl	%esi, %eax
	andl	$1, %esi
	movb	%al, (%rdi)
	je	.L1627
.L1588:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1627:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	movslq	24(%r13), %rax
	cmpl	16(%r13), %eax
	jge	.L1628
.L1592:
	movq	8(%r13), %rsi
	leaq	2(%r12), %rdi
	movb	%r15b, (%rsi,%rax)
	addl	$1, 24(%r13)
	movzwl	2(%r12), %eax
	movl	%eax, %r15d
	cmpw	$127, %ax
	ja	.L1623
	cltq
	movq	8(%rbx), %rsi
	movzbl	(%r14,%rax), %eax
	orb	%al, (%rsi)
	testb	$1, %al
	je	.L1629
.L1614:
	movq	%rdi, %r12
	jmp	.L1588
	.p2align 4,,10
	.p2align 3
.L1629:
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	movslq	24(%r13), %rax
	cmpl	16(%r13), %eax
	jge	.L1630
.L1594:
	movq	8(%r13), %rsi
	leaq	4(%r12), %rdi
	movb	%r15b, (%rsi,%rax)
	addl	$1, 24(%r13)
	movzwl	4(%r12), %eax
	movl	%eax, %r15d
	cmpw	$127, %ax
	ja	.L1623
	cltq
	movq	8(%rbx), %rsi
	movzbl	(%r14,%rax), %eax
	orb	%al, (%rsi)
	testb	$1, %al
	jne	.L1614
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	movslq	24(%r13), %rax
	cmpl	16(%r13), %eax
	jge	.L1631
.L1596:
	movq	8(%r13), %rsi
	leaq	6(%r12), %rdi
	movb	%r15b, (%rsi,%rax)
	addl	$1, 24(%r13)
	movzwl	6(%r12), %eax
	movl	%eax, %r15d
	cmpw	$127, %ax
	ja	.L1623
	cltq
	movq	8(%rbx), %rsi
	movzbl	(%r14,%rax), %eax
	orb	%al, (%rsi)
	testb	$1, %al
	jne	.L1614
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	movslq	24(%r13), %rax
	cmpl	16(%r13), %eax
	jge	.L1632
	movq	8(%r13), %rsi
	addq	$8, %r12
	movb	%r15b, (%rsi,%rax)
	addl	$1, 24(%r13)
	cmpq	%r12, %rcx
	jne	.L1601
.L1599:
	movq	-64(%rbp), %rax
	subq	%r12, %rax
	sarq	%rax
.L1589:
	cmpq	$2, %rax
	je	.L1602
	cmpq	$3, %rax
	je	.L1603
	cmpq	$1, %rax
	je	.L1604
.L1624:
	movq	-64(%rbp), %r12
	jmp	.L1588
	.p2align 4,,10
	.p2align 3
.L1623:
	movq	8(%rbx), %rax
	movq	%rdi, %r12
	orb	$16, (%rax)
	jmp	.L1588
	.p2align 4,,10
	.p2align 3
.L1626:
	orl	$16, %eax
	movb	%al, (%rdi)
	jmp	.L1588
	.p2align 4,,10
	.p2align 3
.L1628:
	leaq	8(%r13), %rdi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r13), %rax
	movq	-56(%rbp), %rcx
	jmp	.L1592
	.p2align 4,,10
	.p2align 3
.L1630:
	leaq	8(%r13), %rdi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r13), %rax
	movq	-56(%rbp), %rcx
	jmp	.L1594
	.p2align 4,,10
	.p2align 3
.L1631:
	leaq	8(%r13), %rdi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r13), %rax
	movq	-56(%rbp), %rcx
	jmp	.L1596
	.p2align 4,,10
	.p2align 3
.L1632:
	leaq	8(%r13), %rdi
	movq	%rcx, -56(%rbp)
	addq	$8, %r12
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r13), %rax
	movq	8(%r13), %rsi
	movq	-56(%rbp), %rcx
	movb	%r15b, (%rsi,%rax)
	addl	$1, 24(%r13)
	cmpq	%rcx, %r12
	jne	.L1601
	jmp	.L1599
.L1603:
	movzwl	(%r12), %edx
	movq	8(%rbx), %rcx
	movl	%edx, %r13d
	movzbl	(%rcx), %eax
	cmpw	$127, %dx
	ja	.L1625
	movslq	%edx, %rdx
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rsi
	movzbl	(%rsi,%rdx), %edx
	orl	%edx, %eax
	andl	$1, %edx
	movb	%al, (%rcx)
	jne	.L1588
	movq	(%rbx), %rax
	movq	8(%rax), %r15
	movslq	24(%r15), %rax
	cmpl	16(%r15), %eax
	jge	.L1633
.L1606:
	movq	8(%r15), %rdx
	addq	$2, %r12
	movb	%r13b, (%rdx,%rax)
	addl	$1, 24(%r15)
.L1602:
	movzwl	(%r12), %edx
	movq	8(%rbx), %rcx
	movl	%edx, %r13d
	movzbl	(%rcx), %eax
	cmpw	$127, %dx
	ja	.L1625
	movslq	%edx, %rdx
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rsi
	movzbl	(%rsi,%rdx), %edx
	orl	%edx, %eax
	andl	$1, %edx
	movb	%al, (%rcx)
	jne	.L1588
	movq	(%rbx), %rax
	movq	8(%rax), %r15
	movslq	24(%r15), %rax
	cmpl	16(%r15), %eax
	jge	.L1634
.L1608:
	movq	8(%r15), %rdx
	addq	$2, %r12
	movb	%r13b, (%rdx,%rax)
	addl	$1, 24(%r15)
.L1604:
	movzwl	(%r12), %edx
	movq	8(%rbx), %rcx
	movl	%edx, %r13d
	movzbl	(%rcx), %eax
	cmpw	$127, %dx
	ja	.L1625
	movslq	%edx, %rdx
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rsi
	movzbl	(%rsi,%rdx), %edx
	orl	%edx, %eax
	andl	$1, %edx
	movb	%al, (%rcx)
	jne	.L1588
	movq	(%rbx), %rax
	movq	8(%rax), %rbx
	movslq	24(%rbx), %rax
	cmpl	16(%rbx), %eax
	jge	.L1635
.L1610:
	movq	8(%rbx), %rdx
	movb	%r13b, (%rdx,%rax)
	addl	$1, 24(%rbx)
	jmp	.L1624
.L1625:
	orl	$16, %eax
	movb	%al, (%rcx)
	jmp	.L1588
.L1635:
	leaq	8(%rbx), %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%rbx), %rax
	jmp	.L1610
.L1634:
	leaq	8(%r15), %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r15), %rax
	jmp	.L1608
.L1633:
	leaq	8(%r15), %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r15), %rax
	jmp	.L1606
	.cfi_endproc
.LFE21833:
	.size	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal20Utf16CharacterStream12AdvanceUntilIZNS6_7Scanner28ScanIdentifierOrKeywordInnerEvEUliE_EEiT_EUltE_EEESB_SB_SB_T0_St26random_access_iterator_tag, .-_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal20Utf16CharacterStream12AdvanceUntilIZNS6_7Scanner28ScanIdentifierOrKeywordInnerEvEUliE_EEiT_EUltE_EEESB_SB_SB_T0_St26random_access_iterator_tag
	.section	.text._ZN2v88internal7Scanner15ScanPrivateNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner15ScanPrivateNameEv
	.type	_ZN2v88internal7Scanner15ScanPrivateNameEv, @function
_ZN2v88internal7Scanner15ScanPrivateNameEv:
.LFB18146:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movl	$0, 24(%rax)
	movb	$1, 28(%rax)
	movq	24(%rdi), %rbx
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jnb	.L1637
.L1691:
	movzwl	(%rax), %edi
	cmpl	$127, %edi
	jg	.L1640
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rax
	movzbl	(%rax,%rdi), %eax
	andl	$1, %eax
.L1643:
	testb	%al, %al
	je	.L1693
	movq	8(%r12), %rbx
	movl	32(%r12), %r13d
	cmpb	$0, 28(%rbx)
	leaq	8(%rbx), %r14
	je	.L1647
	cmpl	$255, %r13d
	jle	.L1694
	movq	%r14, %rdi
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
.L1647:
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L1650:
	movq	24(%r12), %rbx
	movq	16(%rbx), %rdx
	cmpq	24(%rbx), %rdx
	jnb	.L1651
.L1654:
	movzwl	(%rdx), %eax
	addq	$2, %rdx
	movq	%rdx, 16(%rbx)
	movl	%eax, 32(%r12)
	cmpl	$127, %eax
	jg	.L1692
	cmpl	$92, %eax
	je	.L1656
	movslq	%eax, %rdx
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rcx
	movq	8(%r12), %r13
	movl	%eax, %ebx
	movzbl	(%rcx,%rdx), %edx
	movslq	24(%r13), %rax
	sarl	%edx
	movb	%dl, -65(%rbp)
	cmpl	16(%r13), %eax
	jge	.L1695
.L1657:
	movq	8(%r13), %rdx
	movb	%bl, (%rdx,%rax)
	leaq	-65(%rbp), %rax
	movq	%r12, -64(%rbp)
	movq	%rax, -56(%rbp)
	addl	$1, 24(%r13)
	movq	24(%r12), %rbx
	leaq	-64(%rbp), %r13
.L1660:
	movq	24(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%r13, %rdx
	call	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal20Utf16CharacterStream12AdvanceUntilIZNS6_7Scanner28ScanIdentifierOrKeywordInnerEvEUliE_EEiT_EUltE_EEESB_SB_SB_T0_St26random_access_iterator_tag
	cmpq	%rax, 24(%rbx)
	jne	.L1658
	cmpb	$0, 48(%rbx)
	movq	%rax, 16(%rbx)
	je	.L1696
.L1659:
	addq	$2, %rax
	movq	%rax, 16(%rbx)
	movl	$-1, %eax
.L1661:
	movl	%eax, 32(%r12)
	movzbl	-65(%rbp), %eax
	movl	%eax, %edx
	andl	$2, %edx
	testb	$16, %al
	jne	.L1662
	testb	%dl, %dl
	jne	.L1679
	movq	8(%r12), %rdx
	movl	24(%rdx), %eax
	leal	-2(%rax), %ecx
	cmpl	$8, %ecx
	ja	.L1679
	movq	8(%rdx), %rsi
	leaq	_ZZN2v88internal18PerfectKeywordHash4HashEPKciE11asso_values(%rip), %rdx
	movzbl	1(%rsi), %ecx
	movzbl	(%rsi), %edi
	movzbl	(%rdx,%rcx), %ecx
	movzbl	(%rdx,%rdi), %edx
	addl	%eax, %ecx
	addl	%edx, %ecx
	leaq	_ZN2v88internalL26kPerfectKeywordLengthTableE(%rip), %rdx
	andl	$63, %ecx
	movzbl	(%rdx,%rcx), %edx
	cmpl	%edx, %eax
	jne	.L1679
	movq	%rcx, %rax
	leaq	_ZN2v88internalL24kPerfectKeywordHashTableE(%rip), %r8
	salq	$4, %rax
	movq	(%r8,%rax), %rdi
	xorl	%eax, %eax
	jmp	.L1667
	.p2align 4,,10
	.p2align 3
.L1697:
	addq	$1, %rax
	cmpb	-1(%rsi,%rax), %dl
	jne	.L1679
.L1667:
	movzbl	(%rdi,%rax), %edx
	testb	%dl, %dl
	jne	.L1697
	salq	$4, %rcx
	movzbl	8(%r8,%rcx), %eax
	jmp	.L1668
	.p2align 4,,10
	.p2align 3
.L1696:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L1660
	movq	16(%rbx), %rax
	jmp	.L1659
	.p2align 4,,10
	.p2align 3
.L1651:
	cmpb	$0, 48(%rbx)
	je	.L1698
.L1653:
	addq	$2, %rdx
	movq	%rdx, 16(%rbx)
	movl	$-1, 32(%r12)
.L1692:
	xorl	%esi, %esi
	movl	$1, %edx
.L1655:
	movq	%r12, %rdi
	call	_ZN2v88internal7Scanner32ScanIdentifierOrKeywordInnerSlowEbb
.L1668:
	cmpb	$109, %al
	je	.L1645
	.p2align 4,,10
	.p2align 3
.L1679:
	movl	$108, %eax
.L1636:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1699
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1693:
	.cfi_restore_state
	movl	348(%r12), %eax
	testl	%eax, %eax
	jne	.L1645
	movq	24(%r12), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	movl	$257, 348(%r12)
	leal	-1(%rax), %edx
	movl	%eax, 356(%r12)
	movl	$109, %eax
	movl	%edx, 352(%r12)
	jmp	.L1636
	.p2align 4,,10
	.p2align 3
.L1637:
	cmpb	$0, 48(%rbx)
	jne	.L1641
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L1700
.L1641:
	movl	$-1, %edi
.L1640:
	call	_ZN2v88internal21IsIdentifierStartSlowEi@PLT
	jmp	.L1643
	.p2align 4,,10
	.p2align 3
.L1656:
	movq	%r12, %rdi
	call	_ZN2v88internal7Scanner27ScanIdentifierUnicodeEscapeEv
	movslq	%eax, %rbx
	cmpl	$92, %ebx
	jne	.L1701
	.p2align 4,,10
	.p2align 3
.L1645:
	movl	$109, %eax
	jmp	.L1636
	.p2align 4,,10
	.p2align 3
.L1695:
	leaq	8(%r13), %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r13), %rax
	jmp	.L1657
	.p2align 4,,10
	.p2align 3
.L1694:
	movslq	24(%rbx), %rax
	cmpl	16(%rbx), %eax
	jge	.L1702
.L1649:
	movq	8(%rbx), %rdx
	movb	%r13b, (%rdx,%rax)
	addl	$1, 24(%rbx)
	jmp	.L1650
	.p2align 4,,10
	.p2align 3
.L1662:
	testb	%dl, %dl
	sete	%dl
	xorl	%esi, %esi
	movzbl	%dl, %edx
	jmp	.L1655
	.p2align 4,,10
	.p2align 3
.L1658:
	leaq	2(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movzwl	(%rax), %eax
	jmp	.L1661
	.p2align 4,,10
	.p2align 3
.L1701:
	cmpl	$127, %ebx
	ja	.L1703
	movslq	%ebx, %rax
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
	andl	$1, %eax
.L1670:
	testb	%al, %al
	je	.L1645
	movq	8(%r12), %r13
	cmpb	$0, 28(%r13)
	leaq	8(%r13), %r14
	je	.L1671
	cmpl	$255, %ebx
	jle	.L1704
	movq	%r14, %rdi
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
.L1671:
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L1674:
	cmpl	$127, %ebx
	ja	.L1678
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rax
	movl	$1, %esi
	movzbl	(%rax,%rbx), %edx
	shrb	%dl
	xorl	$1, %edx
	andl	$1, %edx
	jmp	.L1655
	.p2align 4,,10
	.p2align 3
.L1698:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	movq	16(%rbx), %rdx
	testb	%al, %al
	jne	.L1654
	jmp	.L1653
	.p2align 4,,10
	.p2align 3
.L1700:
	movq	16(%rbx), %rax
	jmp	.L1691
	.p2align 4,,10
	.p2align 3
.L1702:
	movq	%r14, %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%rbx), %rax
	jmp	.L1649
	.p2align 4,,10
	.p2align 3
.L1703:
	movl	%ebx, %edi
	call	_ZN2v88internal21IsIdentifierStartSlowEi@PLT
	jmp	.L1670
	.p2align 4,,10
	.p2align 3
.L1678:
	movl	$1, %esi
	xorl	%edx, %edx
	jmp	.L1655
.L1704:
	movslq	24(%r13), %rax
	cmpl	16(%r13), %eax
	jge	.L1705
.L1673:
	movq	8(%r13), %rdx
	movb	%bl, (%rdx,%rax)
	addl	$1, 24(%r13)
	jmp	.L1674
.L1705:
	movq	%r14, %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r13), %rax
	jmp	.L1673
.L1699:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18146:
	.size	_ZN2v88internal7Scanner15ScanPrivateNameEv, .-_ZN2v88internal7Scanner15ScanPrivateNameEv
	.section	.rodata._ZN2v88internal7Scanner9PeekAheadEv.str1.1,"aMS",@progbits,1
.LC3:
	.string	"unreachable code"
	.section	.text._ZN2v88internal7Scanner9PeekAheadEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner9PeekAheadEv
	.type	_ZN2v88internal7Scanner9PeekAheadEv, @function
_ZN2v88internal7Scanner9PeekAheadEv:
.LFB18120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	56(%rdx), %eax
	cmpb	$112, %al
	je	.L1957
.L1706:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1958
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1957:
	.cfi_restore_state
	movq	8(%rdi), %r12
	movq	%rdx, 8(%rdi)
	movq	%rdi, %rbx
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	movb	$0, 76(%rdx)
	movq	24(%rdi), %rdx
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rcx
	movq	8(%rdi), %r13
	movq	32(%rdx), %r8
	movq	16(%rdx), %rax
	movq	8(%rdx), %r9
	movq	%r13, %rdi
	leaq	.L1712(%rip), %rdx
.L1708:
	subq	%r9, %rax
	sarq	%rax
	leal	-1(%rax,%r8), %eax
	movl	%eax, (%rdi)
	movslq	32(%rbx), %r15
	cmpl	$127, %r15d
	ja	.L1709
	movslq	%r15d, %rax
	movzbl	(%rcx,%rax), %r14d
	cmpb	$111, %r14b
	ja	.L1710
	movzbl	%r14b, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal7Scanner9PeekAheadEv,"a",@progbits
	.align 4
	.align 4
.L1712:
	.long	.L1732-.L1712
	.long	.L1710-.L1712
	.long	.L1731-.L1712
	.long	.L1713-.L1712
	.long	.L1710-.L1712
	.long	.L1713-.L1712
	.long	.L1713-.L1712
	.long	.L1713-.L1712
	.long	.L1713-.L1712
	.long	.L1713-.L1712
	.long	.L1710-.L1712
	.long	.L1730-.L1712
	.long	.L1713-.L1712
	.long	.L1713-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1729-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1713-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1728-.L1712
	.long	.L1727-.L1712
	.long	.L1726-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1725-.L1712
	.long	.L1724-.L1712
	.long	.L1723-.L1712
	.long	.L1710-.L1712
	.long	.L1722-.L1712
	.long	.L1721-.L1712
	.long	.L1720-.L1712
	.long	.L1713-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1719-.L1712
	.long	.L1718-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1717-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1716-.L1712
	.long	.L1710-.L1712
	.long	.L1715-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1710-.L1712
	.long	.L1714-.L1712
	.long	.L1713-.L1712
	.long	.L1710-.L1712
	.long	.L1711-.L1712
	.section	.text._ZN2v88internal7Scanner9PeekAheadEv
.L1710:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1713:
	movq	24(%rbx), %r15
	movq	16(%r15), %rax
	cmpq	24(%r15), %rax
	jnb	.L1733
.L1735:
	movzwl	(%rax), %edx
.L1734:
	addq	$2, %rax
	movq	%rax, 16(%r15)
	movl	%edx, 32(%rbx)
	.p2align 4,,10
	.p2align 3
.L1736:
	movb	%r14b, 56(%r13)
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	subl	$1, %eax
	movl	%eax, 4(%r13)
	movq	8(%rbx), %rax
	movq	%r12, 8(%rbx)
	movq	%rax, 16(%rbx)
	movzbl	56(%rax), %eax
	jmp	.L1706
.L1725:
	movq	24(%rbx), %r15
	movq	16(%r15), %rax
	cmpq	24(%r15), %rax
	jnb	.L1776
	movzwl	(%rax), %edx
.L1777:
	addq	$2, %rax
	movq	%rax, 16(%r15)
	movl	%edx, 32(%rbx)
	cmpl	$42, %edx
	je	.L1959
	cmpl	$61, %edx
	jne	.L1736
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movl	%eax, %r14d
	jmp	.L1736
.L1724:
	movq	24(%rbx), %r15
	movq	16(%r15), %rax
	cmpq	24(%r15), %rax
	jnb	.L1786
	movzwl	(%rax), %edi
.L1787:
	addq	$2, %rax
	movq	%rax, 16(%r15)
	movl	%edi, 32(%rbx)
	cmpl	$47, %edi
	je	.L1960
	cmpl	$42, %edi
	je	.L1961
	cmpl	$61, %edi
	jne	.L1736
	movl	$25, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movl	%eax, %r14d
	jmp	.L1736
.L1723:
	movq	24(%rbx), %r15
	movq	16(%r15), %rax
	cmpq	24(%r15), %rax
	jnb	.L1779
.L1949:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r15)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	jne	.L1736
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L1783
.L1785:
	movzwl	(%rax), %r15d
.L1784:
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	$26, %r14d
	movl	%r15d, 32(%rbx)
	jmp	.L1736
.L1722:
	movq	24(%rbx), %r15
	movq	16(%r15), %rax
	cmpq	24(%r15), %rax
	jnb	.L1764
	movzwl	(%rax), %edx
.L1765:
	addq	$2, %rax
	movq	%rax, 16(%r15)
	movl	%edx, 32(%rbx)
	cmpl	$43, %edx
	je	.L1962
	cmpl	$61, %edx
	jne	.L1736
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L1767
.L1769:
	movzwl	(%rax), %r15d
.L1768:
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	$28, %r14d
	movl	%r15d, 32(%rbx)
	jmp	.L1736
.L1721:
	movq	24(%rbx), %r15
	movq	16(%r15), %rax
	cmpq	24(%r15), %rax
	jnb	.L1770
	movzwl	(%rax), %edi
.L1771:
	addq	$2, %rax
	movq	%rax, 16(%r15)
	movl	%edi, 32(%rbx)
	cmpl	$45, %edi
	jne	.L1772
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L1773
	movzwl	(%rax), %edi
.L1774:
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%edi, 32(%rbx)
	cmpl	$62, %edi
	jne	.L1907
	movq	8(%rbx), %rax
	cmpb	$0, 76(%rax)
	jne	.L1963
.L1907:
	movl	$52, %r14d
	jmp	.L1736
.L1711:
	movq	24(%rbx), %rax
	movq	16(%rax), %r14
	subq	8(%rax), %r14
	sarq	%r14
	addq	32(%rax), %r14
	jmp	.L1841
	.p2align 4,,10
	.p2align 3
.L1831:
	testb	$8, (%rsi,%r15)
	je	.L1836
.L1835:
	movq	8(%rbx), %rax
	cmpb	$0, 76(%rax)
	jne	.L1833
	movl	32(%rbx), %edi
	cmpl	$10, %edi
	je	.L1837
	cmpl	$13, %edi
	je	.L1837
	subl	$8232, %edi
	cmpl	$1, %edi
	jbe	.L1837
	.p2align 4,,10
	.p2align 3
.L1833:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %rax
	cmpq	24(%rdi), %rax
	jnb	.L1838
.L1840:
	movzwl	(%rax), %r15d
.L1839:
	addq	$2, %rax
	movq	%rax, 16(%rdi)
	movl	%r15d, 32(%rbx)
.L1841:
	cmpl	$127, %r15d
	jbe	.L1831
	movl	%r15d, %edi
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rcx
	testb	%al, %al
	leaq	.L1712(%rip), %rdx
	jne	.L1835
	subl	$8232, %r15d
	cmpl	$1, %r15d
	jbe	.L1835
	.p2align 4,,10
	.p2align 3
.L1836:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	8(%rdi), %r9
	movq	32(%rdi), %r8
	movq	%rax, %rdi
	subq	%r9, %rdi
	sarq	%rdi
	addq	%r8, %rdi
	cmpl	%edi, %r14d
	je	.L1854
.L1842:
	movq	8(%rbx), %rdi
	jmp	.L1708
.L1720:
	movq	24(%rbx), %r15
	movq	16(%r15), %rax
	cmpq	24(%r15), %rax
	jnb	.L1762
	movzwl	(%rax), %edx
.L1763:
	addq	$2, %rax
	movq	%rax, 16(%r15)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	jne	.L1736
	movl	$55, %ecx
	movl	$56, %edx
	movl	$61, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectEiNS0_5Token5ValueES3_
	movl	%eax, %r14d
	jmp	.L1736
.L1731:
	movq	24(%rbx), %r15
	movq	16(%r15), %rax
	cmpq	24(%r15), %rax
	jnb	.L1814
	movzwl	(%rax), %edx
.L1815:
	addq	$2, %rax
	movq	%rax, 16(%r15)
	leal	-48(%rdx), %eax
	movl	%edx, 32(%rbx)
	cmpl	$9, %eax
	jbe	.L1964
	cmpl	$46, %edx
	jne	.L1736
	movq	24(%rbx), %r15
	movq	16(%r15), %rax
	cmpq	24(%r15), %rax
	jnb	.L1818
	cmpw	$46, (%rax)
	jne	.L1736
.L1822:
	addq	$2, %rax
	movq	%rax, 16(%r15)
	movq	24(%rbx), %r14
	movl	%edx, 32(%rbx)
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L1823
	movzwl	(%rax), %edx
.L1824:
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	$10, %r14d
	movl	%edx, 32(%rbx)
	jmp	.L1736
	.p2align 4,,10
	.p2align 3
.L1728:
	movq	24(%rbx), %r15
	movq	16(%r15), %rax
	cmpq	24(%r15), %rax
	jnb	.L1801
	movzwl	(%rax), %edx
.L1802:
	addq	$2, %rax
	movq	%rax, 16(%r15)
	movl	%edx, 32(%rbx)
	cmpl	$124, %edx
	je	.L1965
	cmpl	$61, %edx
	jne	.L1736
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L1804
.L1806:
	movzwl	(%rax), %r15d
.L1805:
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	$18, %r14d
	movl	%r15d, 32(%rbx)
	jmp	.L1736
.L1727:
	movq	24(%rbx), %r15
	movq	16(%r15), %rax
	cmpq	24(%r15), %rax
	jnb	.L1807
.L1950:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r15)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	jne	.L1736
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L1811
.L1813:
	movzwl	(%rax), %r15d
.L1812:
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	$19, %r14d
	movl	%r15d, 32(%rbx)
	jmp	.L1736
.L1726:
	movq	24(%rbx), %r15
	movq	16(%r15), %rax
	cmpq	24(%r15), %rax
	jnb	.L1798
	movzwl	(%rax), %edx
.L1799:
	addq	$2, %rax
	movq	%rax, 16(%r15)
	movl	%edx, 32(%rbx)
	cmpl	$38, %edx
	je	.L1966
	cmpl	$61, %edx
	jne	.L1736
	movl	$20, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movl	%eax, %r14d
	jmp	.L1736
.L1732:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L1825
	movzwl	(%rax), %edx
.L1826:
	addq	$2, %rax
	movq	%rbx, %rdi
	movq	%rax, 16(%r14)
	movl	%edx, 32(%rbx)
	call	_ZN2v88internal7Scanner16ScanTemplateSpanEv
	movl	%eax, %r14d
	jmp	.L1736
.L1715:
	movq	8(%rbx), %rax
	movl	$0, 24(%rax)
	movb	$1, 28(%rax)
	movl	32(%rbx), %eax
	cmpl	$127, %eax
	ja	.L1913
	cmpl	$92, %eax
	je	.L1844
	movslq	%eax, %rdx
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rcx
	movq	8(%rbx), %r15
	movzbl	(%rcx,%rdx), %edx
	sarl	%edx
	movb	%dl, -81(%rbp)
	movslq	24(%r15), %rdx
	cmpl	16(%r15), %edx
	jge	.L1967
.L1845:
	movq	8(%r15), %rcx
	movb	%al, (%rcx,%rdx)
	leaq	-81(%rbp), %rax
	addl	$1, 24(%r15)
	movq	24(%rbx), %r15
	movq	%rax, -72(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rbx, -80(%rbp)
	movq	%rax, -104(%rbp)
.L1848:
	movq	24(%r15), %rsi
	movq	16(%r15), %rdi
	movq	-104(%rbp), %rdx
	call	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal20Utf16CharacterStream12AdvanceUntilIZNS6_7Scanner28ScanIdentifierOrKeywordInnerEvEUliE_EEiT_EUltE_EEESB_SB_SB_T0_St26random_access_iterator_tag
	cmpq	%rax, 24(%r15)
	jne	.L1846
	cmpb	$0, 48(%r15)
	movq	%rax, 16(%r15)
	jne	.L1847
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L1848
	movq	16(%r15), %rax
.L1847:
	addq	$2, %rax
	movq	%rax, 16(%r15)
	movl	$-1, %eax
.L1849:
	movl	%eax, 32(%rbx)
	movzbl	-81(%rbp), %eax
	movl	%eax, %edx
	andl	$2, %edx
	testb	$16, %al
	jne	.L1850
	testb	%dl, %dl
	jne	.L1736
	movq	8(%rbx), %rdx
	movl	24(%rdx), %eax
	leal	-2(%rax), %ecx
	cmpl	$8, %ecx
	ja	.L1736
	movq	8(%rdx), %rsi
	leaq	_ZZN2v88internal18PerfectKeywordHash4HashEPKciE11asso_values(%rip), %rdx
	movzbl	1(%rsi), %ecx
	movzbl	(%rsi), %edi
	movzbl	(%rdx,%rcx), %ecx
	movzbl	(%rdx,%rdi), %edx
	addl	%eax, %ecx
	addl	%edx, %ecx
	leaq	_ZN2v88internalL26kPerfectKeywordLengthTableE(%rip), %rdx
	andl	$63, %ecx
	movzbl	(%rdx,%rcx), %edx
	cmpl	%edx, %eax
	jne	.L1736
	movq	%rcx, %rax
	leaq	_ZN2v88internalL24kPerfectKeywordHashTableE(%rip), %r8
	salq	$4, %rax
	movq	(%r8,%rax), %rdi
	xorl	%eax, %eax
	jmp	.L1853
	.p2align 4,,10
	.p2align 3
.L1968:
	addq	$1, %rax
	cmpb	-1(%rsi,%rax), %dl
	jne	.L1736
.L1853:
	movzbl	(%rdi,%rax), %edx
	testb	%dl, %dl
	jne	.L1968
	salq	$4, %rcx
	movzbl	8(%r8,%rcx), %r14d
	jmp	.L1736
	.p2align 4,,10
	.p2align 3
.L1718:
	movq	24(%rbx), %r15
	movq	16(%r15), %rax
	cmpq	24(%r15), %rax
	jnb	.L1750
	movzwl	(%rax), %edx
.L1751:
	addq	$2, %rax
	movq	%rax, 16(%r15)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	je	.L1969
	cmpl	$62, %edx
	jne	.L1736
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L1753
	movzwl	(%rax), %edx
.L1754:
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	je	.L1970
	movl	$38, %r14d
	cmpl	$62, %edx
	jne	.L1736
	movl	$39, %ecx
	movl	$23, %edx
	movl	$61, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectEiNS0_5Token5ValueES3_
	movl	%eax, %r14d
	jmp	.L1736
.L1729:
	movq	24(%rbx), %r15
	movq	16(%r15), %rax
	cmpq	24(%r15), %rax
	jnb	.L1756
	movzwl	(%rax), %edx
.L1757:
	addq	$2, %rax
	movq	%rax, 16(%r15)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	je	.L1971
	cmpl	$62, %edx
	jne	.L1736
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L1759
.L1761:
	movzwl	(%rax), %r15d
.L1760:
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	$15, %r14d
	movl	%r15d, 32(%rbx)
	jmp	.L1736
.L1717:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner10ScanNumberEb
	movl	%eax, %r14d
	jmp	.L1736
.L1730:
	movq	24(%rbx), %r15
	movq	16(%r15), %rax
	cmpq	24(%r15), %rax
	jnb	.L1737
	movzwl	(%rax), %edx
.L1738:
	addq	$2, %rax
	movq	%rax, 16(%r15)
	cmpb	$0, 281(%rbx)
	movl	%edx, 32(%rbx)
	je	.L1739
	cmpl	$46, %edx
	je	.L1972
.L1739:
	cmpb	$0, 282(%rbx)
	je	.L1736
	cmpl	$63, %edx
	jne	.L1736
	movl	$31, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movl	%eax, %r14d
	jmp	.L1736
	.p2align 4,,10
	.p2align 3
.L1719:
	movq	24(%rbx), %r15
	movq	16(%r15), %rax
	cmpq	24(%r15), %rax
	jnb	.L1745
	movzwl	(%rax), %edi
.L1746:
	addq	$2, %rax
	movq	%rax, 16(%r15)
	movl	%edi, 32(%rbx)
	cmpl	$61, %edi
	je	.L1973
	cmpl	$60, %edi
	je	.L1974
	cmpl	$33, %edi
	jne	.L1736
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner15ScanHtmlCommentEv
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rcx
	movl	%eax, %r14d
	leaq	.L1712(%rip), %rdx
.L1749:
	cmpb	$111, %r14b
	jne	.L1736
	movq	24(%rbx), %rdi
	movq	32(%rdi), %r8
	movq	16(%rdi), %rax
	movq	8(%rdi), %r9
	jmp	.L1842
.L1714:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %r8
	movq	%r8, %rax
	subq	8(%rdi), %rax
	sarq	%rax
	addq	32(%rdi), %rax
	cmpl	$1, %eax
	jne	.L1830
	cmpq	24(%rdi), %r8
	jnb	.L1828
	movzwl	(%r8), %eax
.L1829:
	cmpl	$33, %eax
	je	.L1953
.L1830:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner15ScanPrivateNameEv
	movl	%eax, %r14d
	jmp	.L1736
.L1716:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner10ScanStringEv
	movl	%eax, %r14d
	jmp	.L1736
	.p2align 4,,10
	.p2align 3
.L1837:
	movb	$1, 76(%rax)
	jmp	.L1833
	.p2align 4,,10
	.p2align 3
.L1709:
	movl	%r15d, %edi
	call	_ZN2v88internal21IsIdentifierStartSlowEi@PLT
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rcx
	testb	%al, %al
	leaq	.L1712(%rip), %rdx
	jne	.L1869
	movslq	32(%rbx), %r15
	movq	24(%rbx), %rdi
	movl	%r15d, %eax
	andl	$64512, %eax
	cmpl	$55296, %eax
	je	.L1975
.L1864:
	cmpl	$-1, %r15d
	je	.L1976
	movq	16(%rdi), %r14
	subq	8(%rdi), %r14
	sarq	%r14
	addq	32(%rdi), %r14
	jmp	.L1900
	.p2align 4,,10
	.p2align 3
.L1890:
	testb	$8, (%rsi,%r15)
	je	.L1836
.L1894:
	movq	8(%rbx), %rax
	cmpb	$0, 76(%rax)
	jne	.L1892
	movl	32(%rbx), %edi
	cmpl	$10, %edi
	je	.L1896
	cmpl	$13, %edi
	je	.L1896
	subl	$8232, %edi
	cmpl	$1, %edi
	jbe	.L1896
	.p2align 4,,10
	.p2align 3
.L1892:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %rax
	cmpq	24(%rdi), %rax
	jnb	.L1897
.L1899:
	movzwl	(%rax), %r15d
.L1898:
	addq	$2, %rax
	movq	%rax, 16(%rdi)
	movl	%r15d, 32(%rbx)
.L1900:
	cmpl	$127, %r15d
	jbe	.L1890
	movl	%r15d, %edi
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rcx
	testb	%al, %al
	leaq	.L1712(%rip), %rdx
	jne	.L1894
	subl	$8232, %r15d
	cmpl	$1, %r15d
	ja	.L1836
	jmp	.L1894
	.p2align 4,,10
	.p2align 3
.L1896:
	movb	$1, 76(%rax)
	jmp	.L1892
	.p2align 4,,10
	.p2align 3
.L1897:
	cmpb	$0, 48(%rdi)
	movq	$-1, %r15
	jne	.L1898
	movq	(%rdi), %rax
	movq	%rdi, -104(%rbp)
	call	*40(%rax)
	movq	-104(%rbp), %rdi
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rcx
	testb	%al, %al
	leaq	.L1712(%rip), %rdx
	movq	16(%rdi), %rax
	jne	.L1899
	jmp	.L1898
	.p2align 4,,10
	.p2align 3
.L1838:
	cmpb	$0, 48(%rdi)
	movq	$-1, %r15
	jne	.L1839
	movq	(%rdi), %rax
	movq	%rdi, -104(%rbp)
	call	*40(%rax)
	movq	-104(%rbp), %rdi
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rcx
	testb	%al, %al
	leaq	.L1712(%rip), %rdx
	movq	16(%rdi), %rax
	jne	.L1840
	jmp	.L1839
	.p2align 4,,10
	.p2align 3
.L1975:
	movq	16(%rdi), %rax
	cmpq	24(%rdi), %rax
	jnb	.L1865
.L1952:
	movzwl	(%rax), %r8d
	addq	$2, %rax
	movq	%rax, 16(%rdi)
	movl	%r8d, %eax
	andl	$64512, %eax
	cmpl	$56320, %eax
	je	.L1977
.L1868:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	8(%rdi), %r8
	cmpq	%r8, %rax
	jbe	.L1871
	subq	$2, %rax
	movq	%rax, 16(%rdi)
	movslq	32(%rbx), %r15
	movq	24(%rbx), %rdi
	jmp	.L1864
	.p2align 4,,10
	.p2align 3
.L1854:
	movl	$109, %r14d
	jmp	.L1736
.L1953:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner21SkipSingleLineCommentEv
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rcx
	movl	%eax, %r14d
	leaq	.L1712(%rip), %rdx
	jmp	.L1749
.L1963:
	cmpb	$0, 283(%rbx)
	je	.L1953
	movl	348(%rbx), %eax
	movl	$109, %r14d
	testl	%eax, %eax
	jne	.L1736
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	movl	$275, 348(%rbx)
	leal	-1(%rax), %edx
	movl	%eax, 356(%rbx)
	movl	%edx, 352(%rbx)
	jmp	.L1736
.L1964:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner10ScanNumberEb
	movl	%eax, %r14d
	jmp	.L1736
.L1977:
	movl	32(%rbx), %edi
	andl	$1023, %r8d
	sall	$10, %edi
	andl	$1047552, %edi
	orl	%r8d, %edi
	addl	$65536, %edi
	movl	%edi, 32(%rbx)
	call	_ZN2v88internal21IsIdentifierStartSlowEi@PLT
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rcx
	testb	%al, %al
	leaq	.L1712(%rip), %rdx
	jne	.L1869
.L1945:
	movslq	32(%rbx), %r15
	movq	24(%rbx), %rdi
	jmp	.L1864
.L1967:
	leaq	8(%r15), %rdi
	movl	%eax, -104(%rbp)
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r15), %rdx
	movl	-104(%rbp), %eax
	jmp	.L1845
.L1969:
	movl	$60, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movl	%eax, %r14d
	jmp	.L1736
.L1965:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movl	%eax, %r14d
	jmp	.L1736
.L1966:
	movl	$33, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movl	%eax, %r14d
	jmp	.L1736
.L1971:
	movl	$53, %ecx
	movl	$54, %edx
	movl	$61, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectEiNS0_5Token5ValueES3_
	movl	%eax, %r14d
	jmp	.L1736
.L1962:
	movl	$51, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movl	%eax, %r14d
	jmp	.L1736
.L1960:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L1789
	movzwl	(%rax), %edi
	cmpl	$35, %edi
	je	.L1790
	cmpl	$64, %edi
	jne	.L1953
.L1790:
	movzwl	(%rax), %eax
.L1794:
	addq	$2, 16(%r14)
	movq	24(%rbx), %r14
	movl	%eax, 32(%rbx)
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L1795
	movzwl	(%rax), %edi
.L1796:
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%edi, 32(%rbx)
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner20SkipSourceURLCommentEv
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rcx
	movl	%eax, %r14d
	leaq	.L1712(%rip), %rdx
	jmp	.L1749
.L1959:
	movl	$43, %ecx
	movl	$27, %edx
	movl	$61, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectEiNS0_5Token5ValueES3_
	movl	%eax, %r14d
	jmp	.L1736
.L1807:
	cmpb	$0, 48(%r15)
	jne	.L1956
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L1810
.L1955:
	addq	$2, 16(%r15)
	movl	$-1, 32(%rbx)
	jmp	.L1736
.L1750:
	movq	%r15, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r15), %rax
	jmp	.L1751
.L1801:
	movq	%r15, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r15), %rax
	jmp	.L1802
.L1956:
	addq	$2, %rax
	movq	%rax, 16(%r15)
	movl	$-1, 32(%rbx)
	jmp	.L1736
.L1798:
	movq	%r15, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r15), %rax
	jmp	.L1799
.L1756:
	movq	%r15, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r15), %rax
	jmp	.L1757
.L1828:
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	jmp	.L1829
.L1825:
	movq	%r14, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r14), %rax
	jmp	.L1826
.L1745:
	movq	%r15, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edi
	movq	16(%r15), %rax
	jmp	.L1746
.L1773:
	movq	%r14, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edi
	movq	16(%r14), %rax
	jmp	.L1774
.L1737:
	movq	%r15, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r15), %rax
	jmp	.L1738
.L1770:
	movq	%r15, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edi
	movq	16(%r15), %rax
	jmp	.L1771
.L1814:
	movq	%r15, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r15), %rax
	jmp	.L1815
.L1913:
	xorl	%esi, %esi
	movl	$1, %edx
.L1954:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner32ScanIdentifierOrKeywordInnerSlowEbb
	movl	%eax, %r14d
	jmp	.L1736
.L1762:
	movq	%r15, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r15), %rax
	jmp	.L1763
.L1764:
	movq	%r15, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r15), %rax
	jmp	.L1765
.L1786:
	movq	%r15, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edi
	movq	16(%r15), %rax
	jmp	.L1787
.L1733:
	cmpb	$0, 48(%r15)
	movl	$-1, %edx
	jne	.L1734
	movq	(%r15), %rax
	movl	%edx, -104(%rbp)
	movq	%r15, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r15), %rax
	jne	.L1735
	movl	-104(%rbp), %edx
	jmp	.L1734
.L1779:
	cmpb	$0, 48(%r15)
	jne	.L1956
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L1955
	movq	16(%r15), %rax
	jmp	.L1949
.L1776:
	movq	%r15, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r15), %rax
	jmp	.L1777
.L1844:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner27ScanIdentifierUnicodeEscapeEv
	movslq	%eax, %r14
	cmpl	$92, %r14d
	je	.L1854
	cmpl	$127, %r14d
	ja	.L1978
	movslq	%r14d, %rax
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
	andl	$1, %eax
.L1856:
	testb	%al, %al
	je	.L1854
	movq	8(%rbx), %r15
	cmpb	$0, 28(%r15)
	leaq	8(%r15), %rdi
	je	.L1857
	cmpl	$255, %r14d
	jle	.L1979
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
	movq	-104(%rbp), %rdi
.L1857:
	movl	%r14d, %esi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L1860:
	cmpl	$127, %r14d
	ja	.L1914
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rax
	movl	$1, %esi
	movzbl	(%rax,%r14), %edx
	shrb	%dl
	xorl	$1, %edx
	andl	$1, %edx
	jmp	.L1954
.L1961:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner20SkipMultiLineCommentEv
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rcx
	movl	%eax, %r14d
	leaq	.L1712(%rip), %rdx
	jmp	.L1749
.L1850:
	testb	%dl, %dl
	sete	%dl
	xorl	%esi, %esi
	movzbl	%dl, %edx
	jmp	.L1954
.L1846:
	leaq	2(%rax), %rdx
	movq	%rdx, 16(%r15)
	movzwl	(%rax), %eax
	jmp	.L1849
.L1865:
	cmpb	$0, 48(%rdi)
	jne	.L1867
	movq	(%rdi), %rax
	movq	%rdi, -104(%rbp)
	call	*40(%rax)
	movq	-104(%rbp), %rdi
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rcx
	testb	%al, %al
	leaq	.L1712(%rip), %rdx
	je	.L1867
	movq	16(%rdi), %rax
	jmp	.L1952
.L1970:
	movl	$22, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movl	%eax, %r14d
	jmp	.L1736
.L1867:
	addq	$2, 16(%rdi)
	jmp	.L1868
.L1869:
	movq	8(%rbx), %rax
	movl	$0, 24(%rax)
	movb	$1, 28(%rax)
	movl	32(%rbx), %r15d
	cmpl	$127, %r15d
	ja	.L1980
	cmpl	$92, %r15d
	je	.L1875
	movslq	%r15d, %rax
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rdx
	movq	8(%rbx), %r14
	movzbl	(%rdx,%rax), %eax
	sarl	%eax
	movb	%al, -81(%rbp)
	movl	16(%r14), %eax
	cmpl	%eax, 24(%r14)
	jge	.L1981
.L1876:
	movslq	24(%r14), %rax
	movq	8(%r14), %rdx
	movb	%r15b, (%rdx,%rax)
	leaq	-81(%rbp), %rax
	addl	$1, 24(%r14)
	movq	24(%rbx), %r14
	movq	%rax, -72(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rbx, -80(%rbp)
	movq	%rax, -104(%rbp)
.L1880:
	movq	24(%r14), %rsi
	movq	16(%r14), %rdi
	movq	-104(%rbp), %rdx
	call	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal20Utf16CharacterStream12AdvanceUntilIZNS6_7Scanner28ScanIdentifierOrKeywordInnerEvEUliE_EEiT_EUltE_EEESB_SB_SB_T0_St26random_access_iterator_tag
	cmpq	%rax, 24(%r14)
	jne	.L1877
	cmpb	$0, 48(%r14)
	movq	%rax, 16(%r14)
	jne	.L1881
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L1880
.L1881:
	addq	$2, 16(%r14)
	orl	$-1, %eax
.L1879:
	movzbl	-81(%rbp), %edx
	movl	%eax, 32(%rbx)
	movl	%edx, %eax
	andl	$2, %eax
	andb	$16, %dl
	jne	.L1882
	movl	$92, %r14d
	testb	%al, %al
	jne	.L1736
	movq	8(%rbx), %rax
	movl	24(%rax), %esi
	leal	-2(%rsi), %edx
	cmpl	$8, %edx
	ja	.L1736
	movq	8(%rax), %rdi
	call	_ZN2v88internal18PerfectKeywordHash8GetTokenEPKci.part.0
	movl	%eax, %r14d
	jmp	.L1736
.L1871:
	movq	32(%rdi), %r9
	subq	%r8, %rax
	movq	%r8, 16(%rdi)
	sarq	%rax
	cmpb	$0, 48(%rdi)
	leaq	-1(%r9,%rax), %rax
	movq	%rax, 32(%rdi)
	jne	.L1945
	movq	(%rdi), %rax
	call	*40(%rax)
	movslq	32(%rbx), %r15
	movq	24(%rbx), %rdi
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rcx
	leaq	.L1712(%rip), %rdx
	jmp	.L1864
.L1810:
	movq	16(%r15), %rax
	jmp	.L1950
.L1976:
	cmpb	$1, 48(%rdi)
	sbbl	%r14d, %r14d
	andl	$-95, %r14d
	addl	$109, %r14d
	jmp	.L1736
.L1804:
	orl	$-1, %r15d
	cmpb	$0, 48(%r14)
	jne	.L1805
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r14), %rax
	jne	.L1806
	jmp	.L1805
.L1767:
	orl	$-1, %r15d
	cmpb	$0, 48(%r14)
	jne	.L1768
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r14), %rax
	jne	.L1769
	jmp	.L1768
.L1759:
	orl	$-1, %r15d
	cmpb	$0, 48(%r14)
	jne	.L1760
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r14), %rax
	jne	.L1761
	jmp	.L1760
.L1753:
	movq	%r14, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r14), %rax
	jmp	.L1754
.L1811:
	orl	$-1, %r15d
	cmpb	$0, 48(%r14)
	jne	.L1812
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r14), %rax
	jne	.L1813
	jmp	.L1812
.L1818:
	movq	%r15, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	cmpl	$46, %eax
	jne	.L1736
	movq	24(%rbx), %r15
	movq	16(%r15), %rax
	cmpq	24(%r15), %rax
	jb	.L1982
	movq	%r15, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r15), %rax
	jmp	.L1822
.L1783:
	orl	$-1, %r15d
	cmpb	$0, 48(%r14)
	jne	.L1784
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r14), %rax
	jne	.L1785
	jmp	.L1784
.L1978:
	movl	%r14d, %edi
	call	_ZN2v88internal21IsIdentifierStartSlowEi@PLT
	jmp	.L1856
.L1914:
	movl	$1, %esi
	xorl	%edx, %edx
	jmp	.L1954
.L1789:
	movq	%r14, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	cmpl	$35, %eax
	je	.L1920
	cmpl	$64, %eax
	jne	.L1953
.L1920:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jb	.L1790
	movq	%r14, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	jmp	.L1794
.L1958:
	call	__stack_chk_fail@PLT
.L1772:
	cmpl	$61, %edi
	jne	.L1736
	movl	$29, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movl	%eax, %r14d
	jmp	.L1736
.L1974:
	movl	$37, %ecx
	movl	$21, %edx
	movl	$61, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectEiNS0_5Token5ValueES3_
	movl	%eax, %r14d
	jmp	.L1736
.L1973:
	movl	$59, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movl	%eax, %r14d
	jmp	.L1736
.L1979:
	movl	16(%r15), %eax
	cmpl	%eax, 24(%r15)
	jge	.L1983
.L1859:
	movslq	24(%r15), %rax
	movq	8(%r15), %rdx
	movb	%r14b, (%rdx,%rax)
	addl	$1, 24(%r15)
	jmp	.L1860
.L1795:
	movq	%r14, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edi
	movq	16(%r14), %rax
	jmp	.L1796
.L1981:
	leaq	8(%r14), %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	jmp	.L1876
.L1972:
	movq	24(%rbx), %r15
	movq	16(%r15), %rax
	cmpq	24(%r15), %rax
	jnb	.L1740
	movzwl	(%rax), %eax
.L1741:
	addq	$2, 16(%r15)
	movl	%eax, 32(%rbx)
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L1742
	movl	$4, %r14d
	jmp	.L1736
.L1983:
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	jmp	.L1859
.L1742:
	movq	24(%rbx), %rdi
	call	_ZN2v88internal20Utf16CharacterStream4BackEv
	movl	$46, 32(%rbx)
	jmp	.L1736
.L1882:
	testb	%al, %al
	sete	%al
	xorl	%r15d, %r15d
.L1863:
	movzbl	%al, %edx
	movzbl	%r15b, %esi
	jmp	.L1954
.L1823:
	movq	%r14, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r14), %rax
	jmp	.L1824
.L1875:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner27ScanIdentifierUnicodeEscapeEv
	movslq	%eax, %r14
	cmpl	$92, %r14d
	je	.L1854
	cmpl	$127, %r14d
	ja	.L1984
	movslq	%r14d, %rax
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	movzbl	(%rdx,%rax), %r15d
	andl	$1, %r15d
.L1885:
	testb	%r15b, %r15b
	je	.L1854
	movq	8(%rbx), %rax
	cmpb	$0, 28(%rax)
	leaq	8(%rax), %rdi
	je	.L1886
	cmpl	$255, %r14d
	jle	.L1985
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
	movq	-104(%rbp), %rdi
.L1886:
	movl	%r14d, %esi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L1889:
	xorl	%eax, %eax
	cmpl	$127, %r14d
	ja	.L1863
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rax
	movzbl	(%rax,%r14), %eax
	shrb	%al
	xorl	$1, %eax
	andl	$1, %eax
	jmp	.L1863
.L1877:
	leaq	2(%rax), %rdx
	movq	%rdx, 16(%r14)
	movzwl	(%rax), %eax
	jmp	.L1879
.L1980:
	movl	$1, %eax
	xorl	%r15d, %r15d
	jmp	.L1863
.L1984:
	movl	%r14d, %edi
	call	_ZN2v88internal21IsIdentifierStartSlowEi@PLT
	movl	%eax, %r15d
	jmp	.L1885
.L1985:
	movl	16(%rax), %esi
	cmpl	%esi, 24(%rax)
	jge	.L1986
.L1888:
	movslq	24(%rax), %rdx
	movq	8(%rax), %rcx
	movb	%r14b, (%rcx,%rdx)
	addl	$1, 24(%rax)
	jmp	.L1889
.L1740:
	movq	%r15, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	jmp	.L1741
.L1982:
	movzwl	(%rax), %edx
	jmp	.L1822
.L1986:
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movq	-104(%rbp), %rax
	jmp	.L1888
	.cfi_endproc
.LFE18120:
	.size	_ZN2v88internal7Scanner9PeekAheadEv, .-_ZN2v88internal7Scanner9PeekAheadEv
	.section	.text._ZN2v88internal7Scanner10InitializeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner10InitializeEv
	.type	_ZN2v88internal7Scanner10InitializeEv, @function
_ZN2v88internal7Scanner10InitializeEv:
.LFB18116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L1988
.L1990:
	movzwl	(%rax), %r13d
.L1989:
	addq	$2, %rax
	leaq	120(%rbx), %rcx
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %r15
	movq	%rax, 16(%r12)
	leaq	40(%rbx), %rax
	movq	24(%rbx), %rdx
	leaq	.L1995(%rip), %r14
	movq	%rax, (%rbx)
	leaq	200(%rbx), %rax
	movl	%r13d, 32(%rbx)
	movq	%rcx, 8(%rbx)
	movq	%rax, 16(%rbx)
	movb	$0, 280(%rbx)
	movl	$0, 348(%rbx)
	movb	$1, 196(%rbx)
	movq	32(%rdx), %r8
	movq	16(%rdx), %rax
	movq	8(%rdx), %rsi
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
.L1991:
	subq	%rsi, %rax
	sarq	%rax
	leal	-1(%rax,%r8), %eax
	movl	%eax, (%rcx)
	movslq	32(%rbx), %r13
	cmpl	$127, %r13d
	ja	.L1992
	movslq	%r13d, %rax
	movzbl	(%r15,%rax), %r12d
	cmpb	$111, %r12b
	ja	.L1993
	movzbl	%r12b, %eax
	movslq	(%r14,%rax,4), %rax
	addq	%r14, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal7Scanner10InitializeEv,"a",@progbits
	.align 4
	.align 4
.L1995:
	.long	.L2015-.L1995
	.long	.L1993-.L1995
	.long	.L2014-.L1995
	.long	.L1996-.L1995
	.long	.L1993-.L1995
	.long	.L1996-.L1995
	.long	.L1996-.L1995
	.long	.L1996-.L1995
	.long	.L1996-.L1995
	.long	.L1996-.L1995
	.long	.L1993-.L1995
	.long	.L2013-.L1995
	.long	.L1996-.L1995
	.long	.L1996-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L2012-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1996-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L2011-.L1995
	.long	.L2010-.L1995
	.long	.L2009-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L2008-.L1995
	.long	.L2007-.L1995
	.long	.L2006-.L1995
	.long	.L1993-.L1995
	.long	.L2005-.L1995
	.long	.L2004-.L1995
	.long	.L2003-.L1995
	.long	.L1996-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L2002-.L1995
	.long	.L2001-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L2000-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1999-.L1995
	.long	.L1993-.L1995
	.long	.L1998-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1993-.L1995
	.long	.L1997-.L1995
	.long	.L1996-.L1995
	.long	.L1993-.L1995
	.long	.L1994-.L1995
	.section	.text._ZN2v88internal7Scanner10InitializeEv
	.p2align 4,,10
	.p2align 3
.L1996:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L2016
.L2018:
	movzwl	(%rax), %r14d
.L2017:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movq	24(%rbx), %rdi
	movl	%r14d, 32(%rbx)
	jmp	.L2019
.L2008:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L2095
.L2305:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%edx, 32(%rbx)
	cmpl	$42, %edx
	je	.L2327
	movq	24(%rbx), %rdi
	cmpl	$61, %edx
	jne	.L2019
	movq	16(%rdi), %rax
	cmpq	24(%rdi), %rax
	jnb	.L2100
.L2102:
	movzwl	(%rax), %r12d
.L2101:
	addq	$2, %rax
	movq	%rax, 16(%rdi)
	movq	24(%rbx), %rdi
	movl	%r12d, 32(%rbx)
	movl	$24, %r12d
	jmp	.L2019
.L2007:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L2111
.L2307:
	movzwl	(%rax), %ecx
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%ecx, 32(%rbx)
	cmpl	$47, %ecx
	je	.L2328
	cmpl	$42, %ecx
	je	.L2329
	movq	24(%rbx), %rdi
	cmpl	$61, %ecx
	jne	.L2019
	movq	16(%rdi), %rax
	cmpq	24(%rdi), %rax
	jnb	.L2127
.L2129:
	movzwl	(%rax), %r12d
.L2128:
	addq	$2, %rax
	movq	%rax, 16(%rdi)
	movq	24(%rbx), %rdi
	movl	%r12d, 32(%rbx)
	movl	$25, %r12d
	jmp	.L2019
.L2006:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L2103
.L2306:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	jne	.L2314
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2108
.L2110:
	movzwl	(%rax), %r13d
.L2109:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$26, %r12d
	movq	24(%rbx), %rdi
	movl	%r13d, 32(%rbx)
	jmp	.L2019
.L2005:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L2071
.L2302:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%edx, 32(%rbx)
	cmpl	$43, %edx
	je	.L2330
	movq	24(%rbx), %rdi
	cmpl	$61, %edx
	jne	.L2019
	movq	16(%rdi), %rax
	cmpq	24(%rdi), %rax
	jnb	.L2079
.L2081:
	movzwl	(%rax), %r12d
.L2080:
	addq	$2, %rax
	movq	%rax, 16(%rdi)
	movq	24(%rbx), %rdi
	movl	%r12d, 32(%rbx)
	movl	$28, %r12d
	jmp	.L2019
.L2004:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L2082
.L2303:
	movzwl	(%rax), %ecx
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%ecx, 32(%rbx)
	cmpl	$45, %ecx
	jne	.L2086
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2087
.L2304:
	movzwl	(%rax), %ecx
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	%ecx, 32(%rbx)
	cmpl	$62, %ecx
	jne	.L2092
	movq	8(%rbx), %rax
	cmpb	$0, 76(%rax)
	jne	.L2331
.L2092:
	movq	24(%rbx), %rdi
	movl	$52, %r12d
	jmp	.L2019
.L1994:
	movq	24(%rbx), %rax
	movq	16(%rax), %r12
	subq	8(%rax), %r12
	sarq	%r12
	addq	32(%rax), %r12
	jmp	.L2189
	.p2align 4,,10
	.p2align 3
.L2179:
	testb	$8, (%rdx,%r13)
	je	.L2184
.L2183:
	movq	8(%rbx), %rax
	cmpb	$0, 76(%rax)
	jne	.L2181
	movl	32(%rbx), %ecx
	cmpl	$10, %ecx
	je	.L2185
	cmpl	$13, %ecx
	je	.L2185
	subl	$8232, %ecx
	cmpl	$1, %ecx
	jbe	.L2185
	.p2align 4,,10
	.p2align 3
.L2181:
	movq	24(%rbx), %rcx
	movq	16(%rcx), %rax
	cmpq	24(%rcx), %rax
	jnb	.L2186
.L2188:
	movzwl	(%rax), %r13d
.L2187:
	addq	$2, %rax
	movq	%rax, 16(%rcx)
	movl	%r13d, 32(%rbx)
.L2189:
	cmpl	$127, %r13d
	jbe	.L2179
	movl	%r13d, %edi
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	testb	%al, %al
	jne	.L2183
	subl	$8232, %r13d
	cmpl	$1, %r13d
	jbe	.L2183
.L2184:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	8(%rdi), %rsi
	movq	32(%rdi), %r8
	movq	%rax, %rcx
	subq	%rsi, %rcx
	sarq	%rcx
	addq	%r8, %rcx
	cmpl	%ecx, %r12d
	je	.L2280
.L2190:
	movq	8(%rbx), %rcx
	jmp	.L1991
.L1998:
	movq	8(%rbx), %rax
	movl	$0, 24(%rax)
	movb	$1, 28(%rax)
	movl	32(%rbx), %r14d
	cmpl	$127, %r14d
	ja	.L2273
	cmpl	$92, %r14d
	je	.L2192
	movslq	%r14d, %rax
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rdx
	movq	8(%rbx), %r13
	movzbl	(%rdx,%rax), %eax
	sarl	%eax
	movb	%al, -81(%rbp)
	movslq	24(%r13), %rax
	cmpl	16(%r13), %eax
	jge	.L2332
.L2193:
	movq	8(%r13), %rdx
	movb	%r14b, (%rdx,%rax)
	leaq	-81(%rbp), %rax
	leaq	-80(%rbp), %r14
	addl	$1, 24(%r13)
	movq	24(%rbx), %r13
	movq	%rbx, -80(%rbp)
	movq	%rax, -72(%rbp)
.L2196:
	movq	24(%r13), %rsi
	movq	16(%r13), %rdi
	movq	%r14, %rdx
	call	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal20Utf16CharacterStream12AdvanceUntilIZNS6_7Scanner28ScanIdentifierOrKeywordInnerEvEUliE_EEiT_EUltE_EEESB_SB_SB_T0_St26random_access_iterator_tag
	cmpq	%rax, 24(%r13)
	jne	.L2194
	cmpb	$0, 48(%r13)
	movq	%rax, 16(%r13)
	jne	.L2195
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L2196
	movq	16(%r13), %rax
.L2195:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	$-1, %eax
.L2197:
	movl	%eax, 32(%rbx)
	movzbl	-81(%rbp), %eax
	movl	%eax, %edx
	andl	$2, %edx
	testb	$16, %al
	jne	.L2198
	testb	%dl, %dl
	jne	.L2314
	movq	8(%rbx), %rdx
	movl	24(%rdx), %eax
	leal	-2(%rax), %ecx
	cmpl	$8, %ecx
	ja	.L2314
	movq	8(%rdx), %rsi
	leaq	_ZZN2v88internal18PerfectKeywordHash4HashEPKciE11asso_values(%rip), %rdx
	movzbl	1(%rsi), %ecx
	movzbl	(%rsi), %edi
	movzbl	(%rdx,%rcx), %ecx
	movzbl	(%rdx,%rdi), %edx
	addl	%eax, %ecx
	addl	%edx, %ecx
	leaq	_ZN2v88internalL26kPerfectKeywordLengthTableE(%rip), %rdx
	andl	$63, %ecx
	movzbl	(%rdx,%rcx), %edx
	cmpl	%edx, %eax
	jne	.L2314
	movq	%rcx, %rax
	leaq	_ZN2v88internalL24kPerfectKeywordHashTableE(%rip), %r8
	salq	$4, %rax
	movq	(%r8,%rax), %rdi
	xorl	%eax, %eax
	jmp	.L2201
	.p2align 4,,10
	.p2align 3
.L2333:
	addq	$1, %rax
	cmpb	-1(%rsi,%rax), %dl
	jne	.L2314
.L2201:
	movzbl	(%rdi,%rax), %edx
	testb	%dl, %dl
	jne	.L2333
	salq	$4, %rcx
	movzbl	8(%r8,%rcx), %r12d
	jmp	.L2314
	.p2align 4,,10
	.p2align 3
.L1997:
	movq	24(%rbx), %r12
	movq	16(%r12), %rcx
	movq	%rcx, %rax
	subq	8(%r12), %rax
	sarq	%rax
	addq	32(%r12), %rax
	cmpl	$1, %eax
	jne	.L2178
	cmpq	24(%r12), %rcx
	jnb	.L2176
	movzwl	(%rcx), %eax
.L2177:
	cmpl	$33, %eax
	je	.L2313
.L2178:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner15ScanPrivateNameEv
	movq	24(%rbx), %rdi
	movl	%eax, %r12d
	jmp	.L2019
.L2014:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L2160
.L2311:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r13)
	leal	-48(%rdx), %eax
	movl	%edx, 32(%rbx)
	cmpl	$9, %eax
	jbe	.L2334
	movq	24(%rbx), %rdi
	cmpl	$46, %edx
	jne	.L2019
	movq	16(%rdi), %rax
	cmpq	24(%rdi), %rax
	jnb	.L2165
	cmpw	$46, (%rax)
	jne	.L2019
.L2169:
	addq	$2, %rax
	movq	%rax, 16(%rdi)
	movq	24(%rbx), %r12
	movl	%edx, 32(%rbx)
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2170
	movzwl	(%rax), %edx
.L2171:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$10, %r12d
	movq	24(%rbx), %rdi
	movl	%edx, 32(%rbx)
	jmp	.L2019
	.p2align 4,,10
	.p2align 3
.L2013:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L2020
.L2023:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r13)
	cmpb	$0, 281(%rbx)
	movl	%edx, 32(%rbx)
	jne	.L2335
.L2024:
	cmpb	$0, 282(%rbx)
	je	.L2314
	cmpl	$63, %edx
	jne	.L2314
	movq	%rbx, %rdi
	movl	$31, %esi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movq	24(%rbx), %rdi
	movl	%eax, %r12d
	jmp	.L2019
	.p2align 4,,10
	.p2align 3
.L2000:
	movq	%rbx, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal7Scanner10ScanNumberEb
	movq	24(%rbx), %rdi
	movl	%eax, %r12d
	.p2align 4,,10
	.p2align 3
.L2019:
	movb	%r12b, 176(%rbx)
	movq	16(%rdi), %rax
	subq	8(%rdi), %rax
	sarq	%rax
	addq	32(%rdi), %rax
	subl	$1, %eax
	movl	%eax, 124(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2336
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1999:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner10ScanStringEv
	movq	24(%rbx), %rdi
	movl	%eax, %r12d
	jmp	.L2019
.L2003:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L2058
.L2300:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	jne	.L2314
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2063
.L2301:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	je	.L2067
	movq	24(%rbx), %rdi
	movl	$55, %r12d
	jmp	.L2019
.L2002:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L2030
.L2297:
	movzwl	(%rax), %ecx
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%ecx, 32(%rbx)
	cmpl	$61, %ecx
	je	.L2337
	cmpl	$60, %ecx
	je	.L2338
	cmpl	$33, %ecx
	jne	.L2314
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner15ScanHtmlCommentEv
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	movl	%eax, %r12d
.L2037:
	movq	24(%rbx), %rdi
	cmpb	$111, %r12b
	jne	.L2019
	movq	32(%rdi), %r8
	movq	16(%rdi), %rax
	movq	8(%rdi), %rsi
	jmp	.L2190
.L2001:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L2038
.L2298:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	je	.L2339
	movq	24(%rbx), %rdi
	cmpl	$62, %edx
	jne	.L2019
	movq	16(%rdi), %rax
	cmpq	24(%rdi), %rax
	jnb	.L2046
	movzwl	(%rax), %edx
.L2047:
	addq	$2, %rax
	movq	%rax, 16(%rdi)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	je	.L2340
	cmpl	$62, %edx
	je	.L2049
	movq	24(%rbx), %rdi
	movl	$38, %r12d
	jmp	.L2019
.L2012:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L2050
.L2299:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	je	.L2341
	movq	24(%rbx), %rdi
	cmpl	$62, %edx
	jne	.L2019
	movq	16(%rdi), %rax
	cmpq	24(%rdi), %rax
	jnb	.L2055
.L2057:
	movzwl	(%rax), %r12d
.L2056:
	addq	$2, %rax
	movq	%rax, 16(%rdi)
	movq	24(%rbx), %rdi
	movl	%r12d, 32(%rbx)
	movl	$15, %r12d
	jmp	.L2019
.L2011:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L2141
.L2309:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%edx, 32(%rbx)
	cmpl	$124, %edx
	je	.L2342
	movq	24(%rbx), %rdi
	cmpl	$61, %edx
	jne	.L2019
	movq	16(%rdi), %rax
	cmpq	24(%rdi), %rax
	jnb	.L2149
.L2151:
	movzwl	(%rax), %r12d
.L2150:
	addq	$2, %rax
	movq	%rax, 16(%rdi)
	movq	24(%rbx), %rdi
	movl	%r12d, 32(%rbx)
	movl	$18, %r12d
	jmp	.L2019
.L2010:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L2152
.L2310:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	je	.L2156
	.p2align 4,,10
	.p2align 3
.L2314:
	movq	24(%rbx), %rdi
	jmp	.L2019
.L2009:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L2130
.L2308:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%edx, 32(%rbx)
	cmpl	$38, %edx
	je	.L2343
	movq	24(%rbx), %rdi
	cmpl	$61, %edx
	jne	.L2019
	movq	16(%rdi), %rax
	cmpq	24(%rdi), %rax
	jnb	.L2138
.L2140:
	movzwl	(%rax), %r12d
.L2139:
	addq	$2, %rax
	movq	%rax, 16(%rdi)
	movq	24(%rbx), %rdi
	movl	%r12d, 32(%rbx)
	movl	$20, %r12d
	jmp	.L2019
.L2015:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2172
.L2174:
	movzwl	(%rax), %r13d
.L2173:
	addq	$2, %rax
	movq	%rbx, %rdi
	movq	%rax, 16(%r12)
	movl	%r13d, 32(%rbx)
	call	_ZN2v88internal7Scanner16ScanTemplateSpanEv
	movq	24(%rbx), %rdi
	movl	%eax, %r12d
	jmp	.L2019
	.p2align 4,,10
	.p2align 3
.L2185:
	movb	$1, 76(%rax)
	jmp	.L2181
	.p2align 4,,10
	.p2align 3
.L1992:
	movl	%r13d, %edi
	call	_ZN2v88internal21IsIdentifierStartSlowEi@PLT
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	testb	%al, %al
	jne	.L2219
	movslq	32(%rbx), %r12
	movq	24(%rbx), %rdi
	movl	%r12d, %eax
	andl	$64512, %eax
	cmpl	$55296, %eax
	je	.L2344
.L2213:
	cmpl	$-1, %r12d
	je	.L2345
	movq	16(%rdi), %r13
	subq	8(%rdi), %r13
	sarq	%r13
	addq	32(%rdi), %r13
	jmp	.L2251
	.p2align 4,,10
	.p2align 3
.L2241:
	testb	$8, (%rdx,%r12)
	je	.L2246
.L2245:
	movq	8(%rbx), %rax
	cmpb	$0, 76(%rax)
	jne	.L2243
	movl	32(%rbx), %ecx
	cmpl	$10, %ecx
	je	.L2247
	cmpl	$13, %ecx
	je	.L2247
	subl	$8232, %ecx
	cmpl	$1, %ecx
	jbe	.L2247
	.p2align 4,,10
	.p2align 3
.L2243:
	movq	24(%rbx), %rcx
	movq	16(%rcx), %rax
	cmpq	24(%rcx), %rax
	jnb	.L2248
.L2250:
	movzwl	(%rax), %r12d
.L2249:
	addq	$2, %rax
	movq	%rax, 16(%rcx)
	movl	%r12d, 32(%rbx)
.L2251:
	cmpl	$127, %r12d
	jbe	.L2241
	movl	%r12d, %edi
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	testb	%al, %al
	jne	.L2245
	subl	$8232, %r12d
	cmpl	$1, %r12d
	jbe	.L2245
.L2246:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	8(%rdi), %rsi
	movq	32(%rdi), %r8
	movq	%rax, %rcx
	subq	%rsi, %rcx
	sarq	%rcx
	addq	%r8, %rcx
	cmpl	%ecx, %r13d
	jne	.L2190
.L2280:
	movl	$109, %r12d
	jmp	.L2019
	.p2align 4,,10
	.p2align 3
.L2247:
	movb	$1, 76(%rax)
	jmp	.L2243
	.p2align 4,,10
	.p2align 3
.L1988:
	cmpb	$0, 48(%r12)
	movl	$-1, %r13d
	jne	.L1989
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L1990
	jmp	.L1989
	.p2align 4,,10
	.p2align 3
.L2248:
	cmpb	$0, 48(%rcx)
	movq	$-1, %r12
	jne	.L2249
	movq	(%rcx), %rax
	movq	%rcx, -104(%rbp)
	movq	%rcx, %rdi
	call	*40(%rax)
	movq	-104(%rbp), %rcx
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	testb	%al, %al
	movq	16(%rcx), %rax
	jne	.L2250
	jmp	.L2249
.L1993:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2030:
	cmpb	$0, 48(%r13)
	jne	.L2022
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2324
	movq	16(%r13), %rax
	jmp	.L2297
.L2095:
	cmpb	$0, 48(%r13)
	jne	.L2022
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2324
	movq	16(%r13), %rax
	jmp	.L2305
.L2111:
	cmpb	$0, 48(%r13)
	jne	.L2022
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2324
	movq	16(%r13), %rax
	jmp	.L2307
.L2130:
	cmpb	$0, 48(%r13)
	jne	.L2022
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2324
	movq	16(%r13), %rax
	jmp	.L2308
.L2160:
	cmpb	$0, 48(%r13)
	jne	.L2022
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2324
	movq	16(%r13), %rax
	jmp	.L2311
.L2152:
	cmpb	$0, 48(%r13)
	jne	.L2022
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2324
	movq	16(%r13), %rax
	jmp	.L2310
.L2172:
	cmpb	$0, 48(%r12)
	movl	$-1, %r13d
	jne	.L2173
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2174
	jmp	.L2173
.L2141:
	cmpb	$0, 48(%r13)
	jne	.L2022
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2324
	movq	16(%r13), %rax
	jmp	.L2309
.L2082:
	cmpb	$0, 48(%r13)
	jne	.L2022
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2324
	movq	16(%r13), %rax
	jmp	.L2303
.L2038:
	cmpb	$0, 48(%r13)
	jne	.L2022
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2324
	movq	16(%r13), %rax
	jmp	.L2298
.L2103:
	cmpb	$0, 48(%r13)
	jne	.L2022
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2324
	movq	16(%r13), %rax
	jmp	.L2306
.L2016:
	cmpb	$0, 48(%r13)
	movl	$-1, %r14d
	jne	.L2017
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r13), %rax
	jne	.L2018
	jmp	.L2017
.L2058:
	cmpb	$0, 48(%r13)
	jne	.L2022
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2324
	movq	16(%r13), %rax
	jmp	.L2300
.L2050:
	cmpb	$0, 48(%r13)
	jne	.L2022
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2324
	movq	16(%r13), %rax
	jmp	.L2299
.L2020:
	cmpb	$0, 48(%r13)
	jne	.L2022
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r13), %rax
	jne	.L2023
	.p2align 4,,10
	.p2align 3
.L2022:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movq	24(%rbx), %rdi
	movl	$-1, 32(%rbx)
	jmp	.L2019
.L2071:
	cmpb	$0, 48(%r13)
	jne	.L2022
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2324
	movq	16(%r13), %rax
	jmp	.L2302
.L2273:
	xorl	%esi, %esi
	movl	$1, %edx
.L2326:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner32ScanIdentifierOrKeywordInnerSlowEbb
	movl	%eax, %r12d
	jmp	.L2314
	.p2align 4,,10
	.p2align 3
.L2186:
	cmpb	$0, 48(%rcx)
	movq	$-1, %r13
	jne	.L2187
	movq	(%rcx), %rax
	movq	%rcx, -104(%rbp)
	movq	%rcx, %rdi
	call	*40(%rax)
	movq	-104(%rbp), %rcx
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	testb	%al, %al
	movq	16(%rcx), %rax
	jne	.L2188
	jmp	.L2187
	.p2align 4,,10
	.p2align 3
.L2344:
	movq	16(%rdi), %rax
	cmpq	24(%rdi), %rax
	jnb	.L2214
.L2217:
	movzwl	(%rax), %ecx
	addq	$2, %rax
	movq	%rax, 16(%rdi)
	movl	%ecx, %eax
	andl	$64512, %eax
	cmpl	$56320, %eax
	je	.L2346
.L2218:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	8(%rdi), %rcx
	cmpq	%rcx, %rax
	jbe	.L2221
	subq	$2, %rax
	movq	%rax, 16(%rdi)
	movslq	32(%rbx), %r12
	movq	24(%rbx), %rdi
	jmp	.L2213
	.p2align 4,,10
	.p2align 3
.L2313:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner21SkipSingleLineCommentEv
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	movl	%eax, %r12d
	jmp	.L2037
	.p2align 4,,10
	.p2align 3
.L2331:
	cmpb	$0, 283(%rbx)
	je	.L2313
	movl	348(%rbx), %eax
	movq	24(%rbx), %rdi
	movl	$109, %r12d
	testl	%eax, %eax
	jne	.L2019
	movq	16(%rdi), %rax
	subq	8(%rdi), %rax
	sarq	%rax
	addq	32(%rdi), %rax
	movl	$275, 348(%rbx)
	leal	-1(%rax), %edx
	movl	%eax, 356(%rbx)
	movl	%edx, 352(%rbx)
	jmp	.L2019
	.p2align 4,,10
	.p2align 3
.L2156:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2157
.L2159:
	movzwl	(%rax), %r13d
.L2158:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$19, %r12d
	movq	24(%rbx), %rdi
	movl	%r13d, 32(%rbx)
	jmp	.L2019
	.p2align 4,,10
	.p2align 3
.L2334:
	movq	%rbx, %rdi
	movl	$1, %esi
	call	_ZN2v88internal7Scanner10ScanNumberEb
	movq	24(%rbx), %rdi
	movl	%eax, %r12d
	jmp	.L2019
	.p2align 4,,10
	.p2align 3
.L2324:
	addq	$2, 16(%r13)
	movq	24(%rbx), %rdi
	movl	$-1, 32(%rbx)
	jmp	.L2019
	.p2align 4,,10
	.p2align 3
.L2346:
	movl	32(%rbx), %edi
	andl	$1023, %ecx
	sall	$10, %edi
	andl	$1047552, %edi
	orl	%ecx, %edi
	addl	$65536, %edi
	movl	%edi, 32(%rbx)
	call	_ZN2v88internal21IsIdentifierStartSlowEi@PLT
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	testb	%al, %al
	jne	.L2219
.L2292:
	movslq	32(%rbx), %r12
	movq	24(%rbx), %rdi
	jmp	.L2213
	.p2align 4,,10
	.p2align 3
.L2332:
	leaq	8(%r13), %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r13), %rax
	jmp	.L2193
.L2328:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2116
	movzwl	(%rax), %ecx
	cmpl	$35, %ecx
	je	.L2117
	cmpl	$64, %ecx
	jne	.L2313
.L2117:
	movzwl	(%rax), %eax
.L2121:
	addq	$2, 16(%r12)
	movq	24(%rbx), %r12
	movl	%eax, 32(%rbx)
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2122
	movzwl	(%rax), %ecx
.L2123:
	addq	$2, %rax
	movq	%rbx, %rdi
	movq	%rax, 16(%r12)
	movl	%ecx, 32(%rbx)
	call	_ZN2v88internal7Scanner26TryToParseSourceURLCommentEv
	movl	32(%rbx), %eax
	xorl	%esi, %esi
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	leal	1(%rax), %ecx
	cmpl	$14, %ecx
	ja	.L2124
	movl	$18433, %esi
	shrq	%cl, %rsi
	andl	$1, %esi
.L2124:
	subl	$8232, %eax
	cmpl	$1, %eax
	jbe	.L2125
	testb	%sil, %sil
	je	.L2313
.L2125:
	movq	24(%rbx), %rcx
	movq	32(%rcx), %r8
	movq	16(%rcx), %rax
	movq	8(%rcx), %rsi
	jmp	.L2190
.L2343:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2135
.L2137:
	movzwl	(%rax), %r13d
.L2136:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$33, %r12d
	movq	24(%rbx), %rdi
	movl	%r13d, 32(%rbx)
	jmp	.L2019
.L2339:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2043
.L2045:
	movzwl	(%rax), %r13d
.L2044:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$60, %r12d
	movq	24(%rbx), %rdi
	movl	%r13d, 32(%rbx)
	jmp	.L2019
.L2330:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2076
.L2078:
	movzwl	(%rax), %r13d
.L2077:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$51, %r12d
	movq	24(%rbx), %rdi
	movl	%r13d, 32(%rbx)
	jmp	.L2019
.L2341:
	movl	$53, %ecx
	movl	$54, %edx
.L2317:
	movq	%rbx, %rdi
	movl	$61, %esi
	call	_ZN2v88internal7Scanner6SelectEiNS0_5Token5ValueES3_
	movq	24(%rbx), %rdi
	movl	%eax, %r12d
	jmp	.L2019
.L2342:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2146
.L2148:
	movzwl	(%rax), %r13d
.L2147:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$32, %r12d
	movq	24(%rbx), %rdi
	movl	%r13d, 32(%rbx)
	jmp	.L2019
.L2327:
	movl	$43, %ecx
	movl	$27, %edx
	jmp	.L2317
.L2067:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2068
.L2070:
	movzwl	(%rax), %r13d
.L2069:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$56, %r12d
	movq	24(%rbx), %rdi
	movl	%r13d, 32(%rbx)
	jmp	.L2019
.L2049:
	movl	$39, %ecx
	movl	$23, %edx
	jmp	.L2317
.L2176:
	cmpb	$0, 48(%r12)
	jne	.L2178
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2178
	movq	16(%r12), %rax
	movzwl	(%rax), %eax
	jmp	.L2177
.L2087:
	cmpb	$0, 48(%r12)
	je	.L2089
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$52, %r12d
	movq	24(%rbx), %rdi
	movl	$-1, 32(%rbx)
	jmp	.L2019
.L2335:
	cmpl	$46, %edx
	jne	.L2024
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L2025
	movzwl	(%rax), %eax
.L2026:
	addq	$2, 16(%r13)
	movq	24(%rbx), %rdi
	movl	%eax, 32(%rbx)
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L2027
	movl	$4, %r12d
	jmp	.L2019
	.p2align 4,,10
	.p2align 3
.L2192:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner27ScanIdentifierUnicodeEscapeEv
	movslq	%eax, %r12
	cmpl	$92, %r12d
	je	.L2325
	cmpl	$127, %r12d
	ja	.L2347
	movslq	%r12d, %rax
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
	andl	$1, %eax
.L2204:
	testb	%al, %al
	je	.L2325
	movq	8(%rbx), %r13
	cmpb	$0, 28(%r13)
	leaq	8(%r13), %r14
	je	.L2206
	cmpl	$255, %r12d
	jle	.L2348
	movq	%r14, %rdi
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
.L2206:
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L2209:
	cmpl	$127, %r12d
	ja	.L2274
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rax
	movl	$1, %esi
	movzbl	(%rax,%r12), %edx
	shrb	%dl
	xorl	$1, %edx
	andl	$1, %edx
	jmp	.L2326
.L2329:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner20SkipMultiLineCommentEv
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	movl	%eax, %r12d
	jmp	.L2037
.L2198:
	testb	%dl, %dl
	sete	%dl
	xorl	%esi, %esi
	movzbl	%dl, %edx
	jmp	.L2326
.L2194:
	leaq	2(%rax), %rdx
	movq	%rdx, 16(%r13)
	movzwl	(%rax), %eax
	jmp	.L2197
.L2340:
	movq	%rbx, %rdi
	movl	$22, %esi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movq	24(%rbx), %rdi
	movl	%eax, %r12d
	jmp	.L2019
.L2214:
	cmpb	$0, 48(%rdi)
	jne	.L2216
	movq	(%rdi), %rax
	movq	%rdi, -104(%rbp)
	call	*40(%rax)
	movq	-104(%rbp), %rdi
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	testb	%al, %al
	movq	16(%rdi), %rax
	jne	.L2217
.L2216:
	addq	$2, %rax
	movq	%rax, 16(%rdi)
	jmp	.L2218
.L2221:
	movq	32(%rdi), %rsi
	subq	%rcx, %rax
	movq	%rcx, 16(%rdi)
	sarq	%rax
	cmpb	$0, 48(%rdi)
	leaq	-1(%rsi,%rax), %rax
	movq	%rax, 32(%rdi)
	jne	.L2292
	movq	(%rdi), %rax
	call	*40(%rax)
	movslq	32(%rbx), %r12
	movq	24(%rbx), %rdi
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	jmp	.L2213
.L2219:
	movq	8(%rbx), %rax
	movl	$0, 24(%rax)
	movb	$1, 28(%rax)
	movl	32(%rbx), %r13d
	cmpl	$127, %r13d
	ja	.L2349
	cmpl	$92, %r13d
	je	.L2225
	movslq	%r13d, %rax
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rdx
	movq	8(%rbx), %r12
	movzbl	(%rdx,%rax), %eax
	sarl	%eax
	movb	%al, -81(%rbp)
	movslq	24(%r12), %rax
	cmpl	16(%r12), %eax
	jge	.L2350
.L2226:
	movq	8(%r12), %rdx
	leaq	-80(%rbp), %r14
	movb	%r13b, (%rdx,%rax)
	leaq	-81(%rbp), %rax
	addl	$1, 24(%r12)
	movq	24(%rbx), %r12
	movq	%rbx, -80(%rbp)
	movq	%rax, -72(%rbp)
.L2229:
	movq	24(%r12), %rsi
	movq	16(%r12), %rdi
	movq	%r14, %rdx
	call	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal20Utf16CharacterStream12AdvanceUntilIZNS6_7Scanner28ScanIdentifierOrKeywordInnerEvEUliE_EEiT_EUltE_EEESB_SB_SB_T0_St26random_access_iterator_tag
	cmpq	%rax, 24(%r12)
	jne	.L2227
	cmpb	$0, 48(%r12)
	movq	%rax, 16(%r12)
	jne	.L2228
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L2229
	movq	16(%r12), %rax
.L2228:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$-1, %eax
.L2230:
	movzbl	-81(%rbp), %edx
	movl	%eax, 32(%rbx)
	movl	%edx, %eax
	andl	$2, %eax
	andl	$16, %edx
	jne	.L2231
	movl	$92, %r12d
	testb	%al, %al
	jne	.L2314
	movq	8(%rbx), %rax
	movl	24(%rax), %esi
	leal	-2(%rsi), %edx
	cmpl	$8, %edx
	ja	.L2314
	movq	8(%rax), %rdi
	call	_ZN2v88internal18PerfectKeywordHash8GetTokenEPKci.part.0
	movl	%eax, %r12d
	jmp	.L2314
.L2089:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L2090
	addq	$2, 16(%r12)
	movl	$52, %r12d
	movq	24(%rbx), %rdi
	movl	$-1, 32(%rbx)
	jmp	.L2019
.L2090:
	movq	16(%r12), %rax
	jmp	.L2304
.L2325:
	movq	24(%rbx), %rdi
	movl	$109, %r12d
	jmp	.L2019
.L2345:
	cmpb	$1, 48(%rdi)
	sbbl	%r12d, %r12d
	andl	$-95, %r12d
	addl	$109, %r12d
	jmp	.L2019
.L2063:
	cmpb	$0, 48(%r12)
	je	.L2065
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$55, %r12d
	movq	24(%rbx), %rdi
	movl	$-1, 32(%rbx)
	jmp	.L2019
.L2138:
	cmpb	$0, 48(%rdi)
	movl	$-1, %r12d
	jne	.L2139
	movq	(%rdi), %rax
	movq	%rdi, -104(%rbp)
	call	*40(%rax)
	movq	-104(%rbp), %rdi
	testb	%al, %al
	movq	16(%rdi), %rax
	jne	.L2140
	jmp	.L2139
.L2046:
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movq	-104(%rbp), %rdi
	movl	%eax, %edx
	movq	16(%rdi), %rax
	jmp	.L2047
.L2055:
	cmpb	$0, 48(%rdi)
	movl	$-1, %r12d
	jne	.L2056
	movq	(%rdi), %rax
	movq	%rdi, -104(%rbp)
	call	*40(%rax)
	movq	-104(%rbp), %rdi
	testb	%al, %al
	movq	16(%rdi), %rax
	jne	.L2057
	jmp	.L2056
.L2079:
	cmpb	$0, 48(%rdi)
	movl	$-1, %r12d
	jne	.L2080
	movq	(%rdi), %rax
	movq	%rdi, -104(%rbp)
	call	*40(%rax)
	movq	-104(%rbp), %rdi
	testb	%al, %al
	movq	16(%rdi), %rax
	jne	.L2081
	jmp	.L2080
.L2100:
	cmpb	$0, 48(%rdi)
	movl	$-1, %r12d
	jne	.L2101
	movq	(%rdi), %rax
	movq	%rdi, -104(%rbp)
	call	*40(%rax)
	movq	-104(%rbp), %rdi
	testb	%al, %al
	movq	16(%rdi), %rax
	jne	.L2102
	jmp	.L2101
.L2149:
	cmpb	$0, 48(%rdi)
	movl	$-1, %r12d
	jne	.L2150
	movq	(%rdi), %rax
	movq	%rdi, -104(%rbp)
	call	*40(%rax)
	movq	-104(%rbp), %rdi
	testb	%al, %al
	movq	16(%rdi), %rax
	jne	.L2151
	jmp	.L2150
.L2108:
	cmpb	$0, 48(%r12)
	movl	$-1, %r13d
	jne	.L2109
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2110
	jmp	.L2109
.L2127:
	cmpb	$0, 48(%rdi)
	movl	$-1, %r12d
	jne	.L2128
	movq	(%rdi), %rax
	movq	%rdi, -104(%rbp)
	call	*40(%rax)
	movq	-104(%rbp), %rdi
	testb	%al, %al
	movq	16(%rdi), %rax
	jne	.L2129
	jmp	.L2128
.L2157:
	cmpb	$0, 48(%r12)
	movl	$-1, %r13d
	jne	.L2158
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2159
	jmp	.L2158
.L2165:
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movq	24(%rbx), %rdi
	cmpl	$46, %eax
	jne	.L2019
	movq	16(%rdi), %rax
	cmpq	24(%rdi), %rax
	jb	.L2351
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movq	-104(%rbp), %rdi
	movl	%eax, %edx
	movq	16(%rdi), %rax
	jmp	.L2169
.L2347:
	movl	%r12d, %edi
	call	_ZN2v88internal21IsIdentifierStartSlowEi@PLT
	jmp	.L2204
.L2065:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L2066
	addq	$2, 16(%r12)
	movl	$55, %r12d
	movq	24(%rbx), %rdi
	movl	$-1, 32(%rbx)
	jmp	.L2019
.L2274:
	movl	$1, %esi
	xorl	%edx, %edx
	jmp	.L2326
.L2066:
	movq	16(%r12), %rax
	jmp	.L2301
.L2043:
	cmpb	$0, 48(%r12)
	movl	$-1, %r13d
	jne	.L2044
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2045
	jmp	.L2044
.L2146:
	cmpb	$0, 48(%r12)
	movl	$-1, %r13d
	jne	.L2147
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2148
	jmp	.L2147
.L2135:
	cmpb	$0, 48(%r12)
	movl	$-1, %r13d
	jne	.L2136
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2137
	jmp	.L2136
.L2076:
	cmpb	$0, 48(%r12)
	movl	$-1, %r13d
	jne	.L2077
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2078
	jmp	.L2077
.L2337:
	movq	%rbx, %rdi
	movl	$59, %esi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movq	24(%rbx), %rdi
	movl	%eax, %r12d
	jmp	.L2019
.L2116:
	movq	%r12, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	cmpl	$35, %eax
	je	.L2282
	cmpl	$64, %eax
	jne	.L2313
.L2282:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jb	.L2117
	movq	%r12, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	jmp	.L2121
.L2338:
	movl	$37, %ecx
	movl	$21, %edx
	jmp	.L2317
.L2068:
	orl	$-1, %r13d
	cmpb	$0, 48(%r12)
	jne	.L2069
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2070
	jmp	.L2069
.L2350:
	leaq	8(%r12), %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r12), %rax
	jmp	.L2226
.L2122:
	movq	%r12, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %ecx
	movq	16(%r12), %rax
	jmp	.L2123
.L2348:
	movl	16(%r13), %eax
	cmpl	%eax, 24(%r13)
	jge	.L2352
.L2208:
	movslq	24(%r13), %rax
	movq	8(%r13), %rdx
	movb	%r12b, (%rdx,%rax)
	addl	$1, 24(%r13)
	jmp	.L2209
.L2086:
	cmpl	$61, %ecx
	jne	.L2314
	movq	%rbx, %rdi
	movl	$29, %esi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movq	24(%rbx), %rdi
	movl	%eax, %r12d
	jmp	.L2019
.L2170:
	movq	%r12, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r12), %rax
	jmp	.L2171
.L2336:
	call	__stack_chk_fail@PLT
.L2349:
	movl	$1, %eax
	xorl	%r13d, %r13d
.L2212:
	movzbl	%al, %edx
	movzbl	%r13b, %esi
	jmp	.L2326
.L2225:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner27ScanIdentifierUnicodeEscapeEv
	movslq	%eax, %r12
	cmpl	$92, %r12d
	je	.L2325
	cmpl	$127, %r12d
	ja	.L2353
	movslq	%r12d, %rax
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	movzbl	(%rdx,%rax), %r13d
	andl	$1, %r13d
.L2235:
	testb	%r13b, %r13b
	je	.L2325
	movq	8(%rbx), %r14
	cmpb	$0, 28(%r14)
	leaq	8(%r14), %r15
	je	.L2237
	cmpl	$255, %r12d
	jle	.L2354
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
.L2237:
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L2240:
	xorl	%eax, %eax
	cmpl	$127, %r12d
	ja	.L2212
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rax
	movzbl	(%rax,%r12), %eax
	shrb	%al
	xorl	$1, %eax
	andl	$1, %eax
	jmp	.L2212
.L2227:
	leaq	2(%rax), %rdx
	movq	%rdx, 16(%r12)
	movzwl	(%rax), %eax
	jmp	.L2230
.L2231:
	testb	%al, %al
	sete	%al
	xorl	%r13d, %r13d
	jmp	.L2212
.L2352:
	movq	%r14, %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	jmp	.L2208
.L2353:
	movl	%r12d, %edi
	call	_ZN2v88internal21IsIdentifierStartSlowEi@PLT
	movl	%eax, %r13d
	jmp	.L2235
.L2027:
	call	_ZN2v88internal20Utf16CharacterStream4BackEv
	movl	$46, 32(%rbx)
	movq	24(%rbx), %rdi
	jmp	.L2019
.L2025:
	movq	%r13, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	jmp	.L2026
.L2351:
	movzwl	(%rax), %edx
	jmp	.L2169
.L2354:
	movl	16(%r14), %eax
	cmpl	%eax, 24(%r14)
	jge	.L2355
.L2239:
	movslq	24(%r14), %rax
	movq	8(%r14), %rdx
	movb	%r12b, (%rdx,%rax)
	addl	$1, 24(%r14)
	jmp	.L2240
.L2355:
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	jmp	.L2239
	.cfi_endproc
.LFE18116:
	.size	_ZN2v88internal7Scanner10InitializeEv, .-_ZN2v88internal7Scanner10InitializeEv
	.section	.text._ZN2v88internal7Scanner8SeekNextEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner8SeekNextEm
	.type	_ZN2v88internal7Scanner8SeekNextEm, @function
_ZN2v88internal7Scanner8SeekNextEm:
.LFB18178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$112, 96(%rdi)
	movl	$0, 100(%rdi)
	movb	$112, 176(%rdi)
	movl	$0, 180(%rdi)
	movb	$112, 256(%rdi)
	movl	$0, 260(%rdi)
	movq	24(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	8(%rdi), %rcx
	cmpq	%rdx, %rsi
	jb	.L2357
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	sarq	%rax
	addq	%rdx, %rax
	cmpq	%rax, %rsi
	jnb	.L2357
	subq	%rdx, %rsi
	leaq	(%rcx,%rsi,2), %rax
	movq	%rax, 16(%rdi)
.L2358:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2359
.L2361:
	movzwl	(%rax), %r13d
.L2360:
	addq	$2, %rax
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	.L2366(%rip), %r15
	movq	%rax, 16(%r12)
	movq	8(%rbx), %rax
	movl	%r13d, 32(%rbx)
	movb	$0, 76(%rax)
	movq	24(%rbx), %rdx
	movq	8(%rbx), %r13
	movq	32(%rdx), %r8
	movq	16(%rdx), %rax
	movq	8(%rdx), %rcx
	movq	%r13, %rdi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
.L2362:
	subq	%rcx, %rax
	sarq	%rax
	leal	-1(%rax,%r8), %eax
	movl	%eax, (%rdi)
	movslq	32(%rbx), %r14
	cmpl	$127, %r14d
	ja	.L2363
	movslq	%r14d, %rax
	movzbl	(%rdx,%rax), %r12d
	cmpb	$111, %r12b
	ja	.L2364
	movzbl	%r12b, %eax
	movslq	(%r15,%rax,4), %rax
	addq	%r15, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal7Scanner8SeekNextEm,"a",@progbits
	.align 4
	.align 4
.L2366:
	.long	.L2386-.L2366
	.long	.L2364-.L2366
	.long	.L2385-.L2366
	.long	.L2367-.L2366
	.long	.L2364-.L2366
	.long	.L2367-.L2366
	.long	.L2367-.L2366
	.long	.L2367-.L2366
	.long	.L2367-.L2366
	.long	.L2367-.L2366
	.long	.L2364-.L2366
	.long	.L2384-.L2366
	.long	.L2367-.L2366
	.long	.L2367-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2383-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2367-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2382-.L2366
	.long	.L2381-.L2366
	.long	.L2380-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2379-.L2366
	.long	.L2378-.L2366
	.long	.L2377-.L2366
	.long	.L2364-.L2366
	.long	.L2376-.L2366
	.long	.L2375-.L2366
	.long	.L2374-.L2366
	.long	.L2367-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2373-.L2366
	.long	.L2372-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2371-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2370-.L2366
	.long	.L2364-.L2366
	.long	.L2369-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2364-.L2366
	.long	.L2368-.L2366
	.long	.L2367-.L2366
	.long	.L2364-.L2366
	.long	.L2365-.L2366
	.section	.text._ZN2v88internal7Scanner8SeekNextEm
	.p2align 4,,10
	.p2align 3
.L2367:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2387
.L2389:
	movzwl	(%rax), %r15d
.L2388:
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%r15d, 32(%rbx)
	jmp	.L2390
.L2384:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2391
.L2394:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	cmpb	$0, 281(%rbx)
	movl	%edx, 32(%rbx)
	jne	.L2698
.L2395:
	cmpb	$0, 282(%rbx)
	je	.L2390
	cmpl	$63, %edx
	jne	.L2390
	movl	$31, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movl	%eax, %r12d
	jmp	.L2390
	.p2align 4,,10
	.p2align 3
.L2386:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2535
.L2537:
	movzwl	(%rax), %r14d
.L2536:
	addq	$2, %rax
	movq	%rbx, %rdi
	movq	%rax, 16(%r12)
	movl	%r14d, 32(%rbx)
	call	_ZN2v88internal7Scanner16ScanTemplateSpanEv
	movl	%eax, %r12d
	jmp	.L2390
.L2385:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2522
.L2684:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	leal	-48(%rdx), %eax
	movl	%edx, 32(%rbx)
	cmpl	$9, %eax
	jbe	.L2699
	cmpl	$46, %edx
	jne	.L2390
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2528
	cmpw	$46, (%rax)
	jne	.L2390
.L2532:
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movq	24(%rbx), %r12
	movl	%edx, 32(%rbx)
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2533
	movzwl	(%rax), %edx
.L2534:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$10, %r12d
	movl	%edx, 32(%rbx)
	jmp	.L2390
	.p2align 4,,10
	.p2align 3
.L2383:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2419
.L2672:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	je	.L2700
	cmpl	$62, %edx
	jne	.L2390
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2424
.L2426:
	movzwl	(%rax), %r14d
.L2425:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$15, %r12d
	movl	%r14d, 32(%rbx)
	jmp	.L2390
.L2382:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2504
.L2682:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%edx, 32(%rbx)
	cmpl	$124, %edx
	je	.L2701
	cmpl	$61, %edx
	jne	.L2390
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2512
.L2514:
	movzwl	(%rax), %r14d
.L2513:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$18, %r12d
	movl	%r14d, 32(%rbx)
	jmp	.L2390
.L2381:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2515
.L2683:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	jne	.L2390
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2519
.L2521:
	movzwl	(%rax), %r14d
.L2520:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$19, %r12d
	movl	%r14d, 32(%rbx)
	jmp	.L2390
.L2380:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2493
.L2681:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%edx, 32(%rbx)
	cmpl	$38, %edx
	je	.L2702
	cmpl	$61, %edx
	jne	.L2390
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2501
.L2503:
	movzwl	(%rax), %r14d
.L2502:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$20, %r12d
	movl	%r14d, 32(%rbx)
	jmp	.L2390
.L2379:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2459
.L2678:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%edx, 32(%rbx)
	cmpl	$42, %edx
	je	.L2703
	cmpl	$61, %edx
	jne	.L2390
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2464
.L2466:
	movzwl	(%rax), %r14d
.L2465:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$24, %r12d
	movl	%r14d, 32(%rbx)
	jmp	.L2390
.L2378:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2474
.L2680:
	movzwl	(%rax), %ecx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%ecx, 32(%rbx)
	cmpl	$47, %ecx
	je	.L2704
	cmpl	$42, %ecx
	je	.L2705
	cmpl	$61, %ecx
	jne	.L2390
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2490
.L2492:
	movzwl	(%rax), %r14d
.L2491:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$25, %r12d
	movl	%r14d, 32(%rbx)
	jmp	.L2390
.L2377:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2467
.L2679:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	jne	.L2390
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2471
.L2473:
	movzwl	(%rax), %r14d
.L2472:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$26, %r12d
	movl	%r14d, 32(%rbx)
	jmp	.L2390
.L2376:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2438
.L2675:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%edx, 32(%rbx)
	cmpl	$43, %edx
	je	.L2706
	cmpl	$61, %edx
	jne	.L2390
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2446
.L2448:
	movzwl	(%rax), %r14d
.L2447:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$28, %r12d
	movl	%r14d, 32(%rbx)
	jmp	.L2390
.L2375:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2449
.L2676:
	movzwl	(%rax), %ecx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%ecx, 32(%rbx)
	cmpl	$45, %ecx
	jne	.L2453
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2454
.L2677:
	movzwl	(%rax), %ecx
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	%ecx, 32(%rbx)
	cmpl	$62, %ecx
	jne	.L2623
	movq	8(%rbx), %rax
	cmpb	$0, 76(%rax)
	jne	.L2707
.L2623:
	movl	$52, %r12d
	jmp	.L2390
.L2374:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2427
.L2673:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	jne	.L2390
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2431
.L2674:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$55, %r12d
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	jne	.L2390
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2435
.L2437:
	movzwl	(%rax), %r14d
.L2436:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$56, %r12d
	movl	%r14d, 32(%rbx)
	jmp	.L2390
.L2373:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2401
.L2670:
	movzwl	(%rax), %ecx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%ecx, 32(%rbx)
	cmpl	$61, %ecx
	je	.L2708
	cmpl	$60, %ecx
	je	.L2709
	cmpl	$33, %ecx
	jne	.L2390
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner15ScanHtmlCommentEv
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	movl	%eax, %r12d
.L2407:
	cmpb	$111, %r12b
	jne	.L2390
.L2488:
	movq	24(%rbx), %rcx
	movq	32(%rcx), %r8
	movq	16(%rcx), %rax
	movq	8(%rcx), %rcx
.L2553:
	movq	8(%rbx), %rdi
	jmp	.L2362
.L2372:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2408
.L2671:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	je	.L2710
	cmpl	$62, %edx
	jne	.L2390
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2416
	movzwl	(%rax), %edx
.L2417:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	je	.L2711
	movl	$38, %r12d
	cmpl	$62, %edx
	jne	.L2390
	movl	$39, %ecx
	movl	$23, %edx
	movl	$61, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectEiNS0_5Token5ValueES3_
	movl	%eax, %r12d
	jmp	.L2390
.L2371:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner10ScanNumberEb
	movl	%eax, %r12d
	.p2align 4,,10
	.p2align 3
.L2390:
	movb	%r12b, 56(%r13)
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	subl	$1, %eax
	movl	%eax, 4(%r13)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2712
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2370:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner10ScanStringEv
	movl	%eax, %r12d
	jmp	.L2390
.L2369:
	movq	8(%rbx), %rax
	movl	$0, 24(%rax)
	movb	$1, 28(%rax)
	movl	32(%rbx), %r15d
	cmpl	$127, %r15d
	ja	.L2636
	cmpl	$92, %r15d
	je	.L2555
	movslq	%r15d, %rax
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rdx
	movq	8(%rbx), %r14
	movzbl	(%rdx,%rax), %eax
	sarl	%eax
	movb	%al, -81(%rbp)
	movslq	24(%r14), %rax
	cmpl	16(%r14), %eax
	jge	.L2713
.L2556:
	movq	8(%r14), %rdx
	movb	%r15b, (%rdx,%rax)
	leaq	-81(%rbp), %rax
	leaq	-80(%rbp), %r15
	addl	$1, 24(%r14)
	movq	24(%rbx), %r14
	movq	%rbx, -80(%rbp)
	movq	%rax, -72(%rbp)
.L2559:
	movq	24(%r14), %rsi
	movq	16(%r14), %rdi
	movq	%r15, %rdx
	call	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal20Utf16CharacterStream12AdvanceUntilIZNS6_7Scanner28ScanIdentifierOrKeywordInnerEvEUliE_EEiT_EUltE_EEESB_SB_SB_T0_St26random_access_iterator_tag
	cmpq	%rax, 24(%r14)
	jne	.L2557
	cmpb	$0, 48(%r14)
	movq	%rax, 16(%r14)
	jne	.L2558
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L2559
	movq	16(%r14), %rax
.L2558:
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	$-1, %eax
.L2560:
	movl	%eax, 32(%rbx)
	movzbl	-81(%rbp), %eax
	movl	%eax, %edx
	andl	$2, %edx
	testb	$16, %al
	jne	.L2561
	testb	%dl, %dl
	jne	.L2390
	movq	8(%rbx), %rdx
	movl	24(%rdx), %eax
	leal	-2(%rax), %ecx
	cmpl	$8, %ecx
	ja	.L2390
	movq	8(%rdx), %rsi
	leaq	_ZZN2v88internal18PerfectKeywordHash4HashEPKciE11asso_values(%rip), %rdx
	movzbl	1(%rsi), %ecx
	movzbl	(%rsi), %edi
	movzbl	(%rdx,%rcx), %ecx
	movzbl	(%rdx,%rdi), %edx
	addl	%eax, %ecx
	addl	%edx, %ecx
	leaq	_ZN2v88internalL26kPerfectKeywordLengthTableE(%rip), %rdx
	andl	$63, %ecx
	movzbl	(%rdx,%rcx), %edx
	cmpl	%edx, %eax
	jne	.L2390
	movq	%rcx, %rax
	leaq	_ZN2v88internalL24kPerfectKeywordHashTableE(%rip), %r8
	salq	$4, %rax
	movq	(%r8,%rax), %rdi
	xorl	%eax, %eax
	jmp	.L2564
	.p2align 4,,10
	.p2align 3
.L2714:
	addq	$1, %rax
	cmpb	-1(%rsi,%rax), %dl
	jne	.L2390
.L2564:
	movzbl	(%rdi,%rax), %edx
	testb	%dl, %dl
	jne	.L2714
	salq	$4, %rcx
	movzbl	8(%r8,%rcx), %r12d
	jmp	.L2390
	.p2align 4,,10
	.p2align 3
.L2368:
	movq	24(%rbx), %r12
	movq	16(%r12), %rcx
	movq	%rcx, %rax
	subq	8(%r12), %rax
	sarq	%rax
	addq	32(%r12), %rax
	cmpl	$1, %eax
	jne	.L2541
	cmpq	24(%r12), %rcx
	jnb	.L2539
	movzwl	(%rcx), %eax
.L2540:
	cmpl	$33, %eax
	je	.L2686
.L2541:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner15ScanPrivateNameEv
	movl	%eax, %r12d
	jmp	.L2390
.L2365:
	movq	24(%rbx), %rax
	movq	16(%rax), %r12
	subq	8(%rax), %r12
	sarq	%r12
	addq	32(%rax), %r12
	jmp	.L2552
	.p2align 4,,10
	.p2align 3
.L2542:
	testb	$8, (%rsi,%r14)
	je	.L2547
.L2546:
	movq	8(%rbx), %rax
	cmpb	$0, 76(%rax)
	jne	.L2544
	movl	32(%rbx), %ecx
	cmpl	$10, %ecx
	je	.L2548
	cmpl	$13, %ecx
	je	.L2548
	subl	$8232, %ecx
	cmpl	$1, %ecx
	jbe	.L2548
	.p2align 4,,10
	.p2align 3
.L2544:
	movq	24(%rbx), %rcx
	movq	16(%rcx), %rax
	cmpq	24(%rcx), %rax
	jnb	.L2549
.L2551:
	movzwl	(%rax), %r14d
.L2550:
	addq	$2, %rax
	movq	%rax, 16(%rcx)
	movl	%r14d, 32(%rbx)
.L2552:
	cmpl	$127, %r14d
	jbe	.L2542
	movl	%r14d, %edi
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	testb	%al, %al
	jne	.L2546
	subl	$8232, %r14d
	cmpl	$1, %r14d
	jbe	.L2546
.L2547:
	movq	24(%rbx), %rcx
	movq	16(%rcx), %rax
	movq	32(%rcx), %r8
	movq	8(%rcx), %rcx
	movq	%rax, %rdi
	subq	%rcx, %rdi
	sarq	%rdi
	addq	%r8, %rdi
	cmpl	%edi, %r12d
	jne	.L2553
.L2565:
	movl	$109, %r12d
	jmp	.L2390
	.p2align 4,,10
	.p2align 3
.L2357:
	cmpb	$0, 48(%rdi)
	movq	%rsi, 32(%rdi)
	movq	%rcx, 16(%rdi)
	jne	.L2358
	movq	(%rdi), %rax
	call	*40(%rax)
	jmp	.L2358
	.p2align 4,,10
	.p2align 3
.L2548:
	movb	$1, 76(%rax)
	jmp	.L2544
	.p2align 4,,10
	.p2align 3
.L2363:
	movl	%r14d, %edi
	call	_ZN2v88internal21IsIdentifierStartSlowEi@PLT
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	testb	%al, %al
	jne	.L2581
	movslq	32(%rbx), %r12
	movq	24(%rbx), %rcx
	movl	%r12d, %eax
	andl	$64512, %eax
	cmpl	$55296, %eax
	je	.L2715
.L2575:
	cmpl	$-1, %r12d
	je	.L2716
	movq	16(%rcx), %r14
	subq	8(%rcx), %r14
	sarq	%r14
	addq	32(%rcx), %r14
	jmp	.L2611
	.p2align 4,,10
	.p2align 3
.L2601:
	testb	$8, (%rsi,%r12)
	je	.L2606
.L2605:
	movq	8(%rbx), %rax
	cmpb	$0, 76(%rax)
	jne	.L2603
	movl	32(%rbx), %ecx
	cmpl	$10, %ecx
	je	.L2607
	cmpl	$13, %ecx
	je	.L2607
	subl	$8232, %ecx
	cmpl	$1, %ecx
	jbe	.L2607
	.p2align 4,,10
	.p2align 3
.L2603:
	movq	24(%rbx), %rcx
	movq	16(%rcx), %rax
	cmpq	24(%rcx), %rax
	jnb	.L2608
.L2610:
	movzwl	(%rax), %r12d
.L2609:
	addq	$2, %rax
	movq	%rax, 16(%rcx)
	movl	%r12d, 32(%rbx)
.L2611:
	cmpl	$127, %r12d
	jbe	.L2601
	movl	%r12d, %edi
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	testb	%al, %al
	jne	.L2605
	subl	$8232, %r12d
	cmpl	$1, %r12d
	jbe	.L2605
.L2606:
	movq	24(%rbx), %rcx
	movq	16(%rcx), %rax
	movq	32(%rcx), %r8
	movq	8(%rcx), %rcx
	movq	%rax, %rdi
	subq	%rcx, %rdi
	sarq	%rdi
	addq	%r8, %rdi
	cmpl	%edi, %r14d
	jne	.L2553
	jmp	.L2565
	.p2align 4,,10
	.p2align 3
.L2607:
	movb	$1, 76(%rax)
	jmp	.L2603
	.p2align 4,,10
	.p2align 3
.L2359:
	cmpb	$0, 48(%r12)
	movl	$-1, %r13d
	jne	.L2360
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2361
	jmp	.L2360
	.p2align 4,,10
	.p2align 3
.L2608:
	cmpb	$0, 48(%rcx)
	movq	$-1, %r12
	jne	.L2609
	movq	(%rcx), %rax
	movq	%rcx, -104(%rbp)
	movq	%rcx, %rdi
	call	*40(%rax)
	movq	-104(%rbp), %rcx
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	testb	%al, %al
	movq	16(%rcx), %rax
	jne	.L2610
	jmp	.L2609
.L2364:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2387:
	cmpb	$0, 48(%r14)
	movl	$-1, %r15d
	jne	.L2388
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r14), %rax
	jne	.L2389
	jmp	.L2388
.L2504:
	cmpb	$0, 48(%r14)
	jne	.L2393
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2697
	movq	16(%r14), %rax
	jmp	.L2682
.L2419:
	cmpb	$0, 48(%r14)
	jne	.L2393
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2697
	movq	16(%r14), %rax
	jmp	.L2672
.L2636:
	xorl	%esi, %esi
	movl	$1, %edx
.L2689:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner32ScanIdentifierOrKeywordInnerSlowEbb
	movl	%eax, %r12d
	jmp	.L2390
.L2408:
	cmpb	$0, 48(%r14)
	jne	.L2393
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2697
	movq	16(%r14), %rax
	jmp	.L2671
.L2493:
	cmpb	$0, 48(%r14)
	jne	.L2393
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2697
	movq	16(%r14), %rax
	jmp	.L2681
.L2438:
	cmpb	$0, 48(%r14)
	jne	.L2393
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2697
	movq	16(%r14), %rax
	jmp	.L2675
.L2522:
	cmpb	$0, 48(%r14)
	jne	.L2393
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2697
	movq	16(%r14), %rax
	jmp	.L2684
.L2535:
	cmpb	$0, 48(%r12)
	movl	$-1, %r14d
	jne	.L2536
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2537
	jmp	.L2536
.L2401:
	cmpb	$0, 48(%r14)
	jne	.L2393
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2697
	movq	16(%r14), %rax
	jmp	.L2670
.L2427:
	cmpb	$0, 48(%r14)
	jne	.L2393
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2697
	movq	16(%r14), %rax
	jmp	.L2673
.L2515:
	cmpb	$0, 48(%r14)
	jne	.L2393
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2697
	movq	16(%r14), %rax
	jmp	.L2683
.L2467:
	cmpb	$0, 48(%r14)
	jne	.L2393
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2697
	movq	16(%r14), %rax
	jmp	.L2679
.L2474:
	cmpb	$0, 48(%r14)
	jne	.L2393
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2697
	movq	16(%r14), %rax
	jmp	.L2680
.L2459:
	cmpb	$0, 48(%r14)
	jne	.L2393
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2697
	movq	16(%r14), %rax
	jmp	.L2678
.L2449:
	cmpb	$0, 48(%r14)
	jne	.L2393
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2697
	movq	16(%r14), %rax
	jmp	.L2676
.L2391:
	cmpb	$0, 48(%r14)
	jne	.L2393
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r14), %rax
	jne	.L2394
.L2393:
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	$-1, 32(%rbx)
	jmp	.L2390
	.p2align 4,,10
	.p2align 3
.L2549:
	cmpb	$0, 48(%rcx)
	movq	$-1, %r14
	jne	.L2550
	movq	(%rcx), %rax
	movq	%rcx, -104(%rbp)
	movq	%rcx, %rdi
	call	*40(%rax)
	movq	-104(%rbp), %rcx
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	testb	%al, %al
	movq	16(%rcx), %rax
	jne	.L2551
	jmp	.L2550
.L2715:
	movq	16(%rcx), %rax
	cmpq	24(%rcx), %rax
	jnb	.L2576
.L2579:
	movzwl	(%rax), %r8d
	addq	$2, %rax
	movq	%rax, 16(%rcx)
	movl	%r8d, %eax
	andl	$64512, %eax
	cmpl	$56320, %eax
	je	.L2717
.L2580:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	8(%rdi), %rcx
	cmpq	%rcx, %rax
	jbe	.L2583
	subq	$2, %rax
	movq	%rax, 16(%rdi)
	movslq	32(%rbx), %r12
	movq	24(%rbx), %rcx
	jmp	.L2575
.L2704:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2479
	movzwl	(%rax), %ecx
	cmpl	$64, %ecx
	je	.L2480
	cmpl	$35, %ecx
	jne	.L2686
.L2480:
	movzwl	(%rax), %eax
.L2484:
	addq	$2, 16(%r12)
	movq	24(%rbx), %r12
	movl	%eax, 32(%rbx)
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2485
	movzwl	(%rax), %ecx
.L2486:
	addq	$2, %rax
	movq	%rbx, %rdi
	movq	%rax, 16(%r12)
	movl	%ecx, 32(%rbx)
	call	_ZN2v88internal7Scanner26TryToParseSourceURLCommentEv
	movl	32(%rbx), %edi
	xorl	%eax, %eax
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	leal	1(%rdi), %ecx
	cmpl	$14, %ecx
	ja	.L2487
	movl	$18433, %eax
	shrq	%cl, %rax
	andl	$1, %eax
.L2487:
	subl	$8232, %edi
	cmpl	$1, %edi
	jbe	.L2488
	testb	%al, %al
	jne	.L2488
.L2686:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner21SkipSingleLineCommentEv
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	movl	%eax, %r12d
	jmp	.L2407
.L2707:
	cmpb	$0, 283(%rbx)
	je	.L2686
	movl	348(%rbx), %eax
	movl	$109, %r12d
	testl	%eax, %eax
	jne	.L2390
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	movl	$275, 348(%rbx)
	leal	-1(%rax), %edx
	movl	%eax, 356(%rbx)
	movl	%edx, 352(%rbx)
	jmp	.L2390
.L2699:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner10ScanNumberEb
	movl	%eax, %r12d
	jmp	.L2390
.L2697:
	addq	$2, 16(%r14)
	movl	$-1, 32(%rbx)
	jmp	.L2390
.L2717:
	movl	32(%rbx), %edi
	andl	$1023, %r8d
	sall	$10, %edi
	andl	$1047552, %edi
	orl	%r8d, %edi
	addl	$65536, %edi
	movl	%edi, 32(%rbx)
	call	_ZN2v88internal21IsIdentifierStartSlowEi@PLT
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	testb	%al, %al
	jne	.L2581
.L2665:
	movslq	32(%rbx), %r12
	movq	24(%rbx), %rcx
	jmp	.L2575
.L2713:
	leaq	8(%r14), %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r14), %rax
	jmp	.L2556
.L2710:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2413
.L2415:
	movzwl	(%rax), %r14d
.L2414:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$60, %r12d
	movl	%r14d, 32(%rbx)
	jmp	.L2390
.L2701:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2509
.L2511:
	movzwl	(%rax), %r14d
.L2510:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$32, %r12d
	movl	%r14d, 32(%rbx)
	jmp	.L2390
.L2700:
	movl	$53, %ecx
	movl	$54, %edx
	movl	$61, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectEiNS0_5Token5ValueES3_
	movl	%eax, %r12d
	jmp	.L2390
.L2702:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2498
.L2500:
	movzwl	(%rax), %r14d
.L2499:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$33, %r12d
	movl	%r14d, 32(%rbx)
	jmp	.L2390
.L2706:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2443
.L2445:
	movzwl	(%rax), %r14d
.L2444:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$51, %r12d
	movl	%r14d, 32(%rbx)
	jmp	.L2390
.L2703:
	movl	$43, %ecx
	movl	$27, %edx
	movl	$61, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectEiNS0_5Token5ValueES3_
	movl	%eax, %r12d
	jmp	.L2390
.L2698:
	cmpl	$46, %edx
	jne	.L2395
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2396
	movzwl	(%rax), %eax
.L2397:
	addq	$2, 16(%r14)
	movl	%eax, 32(%rbx)
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L2398
	movl	$4, %r12d
	jmp	.L2390
	.p2align 4,,10
	.p2align 3
.L2539:
	cmpb	$0, 48(%r12)
	jne	.L2541
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2541
	movq	16(%r12), %rax
	movzwl	(%rax), %eax
	jmp	.L2540
.L2454:
	cmpb	$0, 48(%r12)
	je	.L2456
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$52, %r12d
	movl	$-1, 32(%rbx)
	jmp	.L2390
.L2555:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner27ScanIdentifierUnicodeEscapeEv
	movslq	%eax, %r12
	cmpl	$92, %r12d
	je	.L2565
	cmpl	$127, %r12d
	ja	.L2718
	movslq	%r12d, %rax
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
	andl	$1, %eax
.L2567:
	testb	%al, %al
	je	.L2565
	movq	8(%rbx), %r14
	cmpb	$0, 28(%r14)
	leaq	8(%r14), %r15
	je	.L2568
	cmpl	$255, %r12d
	jle	.L2719
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
.L2568:
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L2571:
	cmpl	$127, %r12d
	ja	.L2637
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rax
	movl	$1, %esi
	movzbl	(%rax,%r12), %edx
	shrb	%dl
	xorl	$1, %edx
	andl	$1, %edx
	jmp	.L2689
.L2705:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner20SkipMultiLineCommentEv
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	movl	%eax, %r12d
	jmp	.L2407
.L2561:
	testb	%dl, %dl
	sete	%dl
	xorl	%esi, %esi
	movzbl	%dl, %edx
	jmp	.L2689
.L2711:
	movl	$22, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movl	%eax, %r12d
	jmp	.L2390
.L2557:
	leaq	2(%rax), %rdx
	movq	%rdx, 16(%r14)
	movzwl	(%rax), %eax
	jmp	.L2560
.L2576:
	cmpb	$0, 48(%rcx)
	jne	.L2578
	movq	(%rcx), %rax
	movq	%rcx, -104(%rbp)
	movq	%rcx, %rdi
	call	*40(%rax)
	movq	-104(%rbp), %rcx
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	testb	%al, %al
	movq	16(%rcx), %rax
	jne	.L2579
.L2578:
	addq	$2, %rax
	movq	%rax, 16(%rcx)
	jmp	.L2580
.L2581:
	movq	8(%rbx), %rax
	movl	$0, 24(%rax)
	movb	$1, 28(%rax)
	movl	32(%rbx), %r14d
	cmpl	$127, %r14d
	ja	.L2720
	cmpl	$92, %r14d
	je	.L2587
	movslq	%r14d, %rax
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rdx
	movq	8(%rbx), %r12
	movzbl	(%rdx,%rax), %eax
	sarl	%eax
	movb	%al, -81(%rbp)
	movslq	24(%r12), %rax
	cmpl	16(%r12), %eax
	jge	.L2721
.L2588:
	movq	8(%r12), %rdx
	leaq	-80(%rbp), %r15
	movb	%r14b, (%rdx,%rax)
	leaq	-81(%rbp), %rax
	addl	$1, 24(%r12)
	movq	24(%rbx), %r12
	movq	%rbx, -80(%rbp)
	movq	%rax, -72(%rbp)
.L2591:
	movq	24(%r12), %rsi
	movq	16(%r12), %rdi
	movq	%r15, %rdx
	call	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal20Utf16CharacterStream12AdvanceUntilIZNS6_7Scanner28ScanIdentifierOrKeywordInnerEvEUliE_EEiT_EUltE_EEESB_SB_SB_T0_St26random_access_iterator_tag
	cmpq	%rax, 24(%r12)
	jne	.L2589
	cmpb	$0, 48(%r12)
	movq	%rax, 16(%r12)
	jne	.L2590
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L2591
	movq	16(%r12), %rax
.L2590:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$-1, %eax
.L2592:
	movzbl	-81(%rbp), %edx
	movl	%eax, 32(%rbx)
	movl	%edx, %eax
	andl	$2, %eax
	andl	$16, %edx
	jne	.L2593
	movl	$92, %r12d
	testb	%al, %al
	jne	.L2390
	movq	8(%rbx), %rax
	movl	24(%rax), %esi
	leal	-2(%rsi), %edx
	cmpl	$8, %edx
	ja	.L2390
	movq	8(%rax), %rdi
	call	_ZN2v88internal18PerfectKeywordHash8GetTokenEPKci.part.0
	movl	%eax, %r12d
	jmp	.L2390
.L2456:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L2457
	addq	$2, 16(%r12)
	movl	$52, %r12d
	movl	$-1, 32(%rbx)
	jmp	.L2390
.L2431:
	cmpb	$0, 48(%r12)
	je	.L2433
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$55, %r12d
	movl	$-1, 32(%rbx)
	jmp	.L2390
.L2716:
	cmpb	$1, 48(%rcx)
	sbbl	%r12d, %r12d
	andl	$-95, %r12d
	addl	$109, %r12d
	jmp	.L2390
.L2583:
	movq	32(%rdi), %r8
	subq	%rcx, %rax
	movq	%rcx, 16(%rdi)
	sarq	%rax
	cmpb	$0, 48(%rdi)
	leaq	-1(%r8,%rax), %rax
	movq	%rax, 32(%rdi)
	jne	.L2665
	movq	(%rdi), %rax
	call	*40(%rax)
	movslq	32(%rbx), %r12
	movq	24(%rbx), %rcx
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	jmp	.L2575
.L2457:
	movq	16(%r12), %rax
	jmp	.L2677
.L2501:
	cmpb	$0, 48(%r12)
	movl	$-1, %r14d
	jne	.L2502
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2503
	jmp	.L2502
.L2416:
	movq	%r12, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r12), %rax
	jmp	.L2417
.L2512:
	cmpb	$0, 48(%r12)
	movl	$-1, %r14d
	jne	.L2513
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2514
	jmp	.L2513
.L2446:
	cmpb	$0, 48(%r12)
	movl	$-1, %r14d
	jne	.L2447
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2448
	jmp	.L2447
.L2464:
	cmpb	$0, 48(%r12)
	movl	$-1, %r14d
	jne	.L2465
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2466
	jmp	.L2465
.L2424:
	cmpb	$0, 48(%r12)
	movl	$-1, %r14d
	jne	.L2425
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2426
	jmp	.L2425
.L2528:
	movq	%r14, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	cmpl	$46, %eax
	jne	.L2390
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jb	.L2722
	movq	%r14, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r14), %rax
	jmp	.L2532
.L2490:
	cmpb	$0, 48(%r12)
	movl	$-1, %r14d
	jne	.L2491
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2492
	jmp	.L2491
.L2471:
	cmpb	$0, 48(%r12)
	movl	$-1, %r14d
	jne	.L2472
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2473
	jmp	.L2472
.L2519:
	cmpb	$0, 48(%r12)
	movl	$-1, %r14d
	jne	.L2520
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2521
	jmp	.L2520
.L2708:
	movl	$59, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movl	%eax, %r12d
	jmp	.L2390
.L2637:
	movl	$1, %esi
	xorl	%edx, %edx
	jmp	.L2689
.L2413:
	cmpb	$0, 48(%r12)
	movl	$-1, %r14d
	jne	.L2414
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2415
	jmp	.L2414
.L2509:
	cmpb	$0, 48(%r12)
	movl	$-1, %r14d
	jne	.L2510
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2511
	jmp	.L2510
.L2433:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L2434
	addq	$2, 16(%r12)
	movl	$55, %r12d
	movl	$-1, 32(%rbx)
	jmp	.L2390
.L2718:
	movl	%r12d, %edi
	call	_ZN2v88internal21IsIdentifierStartSlowEi@PLT
	jmp	.L2567
.L2434:
	movq	16(%r12), %rax
	jmp	.L2674
.L2498:
	cmpb	$0, 48(%r12)
	movl	$-1, %r14d
	jne	.L2499
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2500
	jmp	.L2499
.L2443:
	cmpb	$0, 48(%r12)
	movl	$-1, %r14d
	jne	.L2444
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2445
	jmp	.L2444
.L2479:
	movq	%r12, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	cmpl	$35, %eax
	je	.L2643
	cmpl	$64, %eax
	jne	.L2686
.L2643:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jb	.L2480
	movq	%r12, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	jmp	.L2484
.L2435:
	orl	$-1, %r14d
	cmpb	$0, 48(%r12)
	jne	.L2436
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2437
	jmp	.L2436
.L2709:
	movl	$37, %ecx
	movl	$21, %edx
	movl	$61, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectEiNS0_5Token5ValueES3_
	movl	%eax, %r12d
	jmp	.L2390
.L2721:
	leaq	8(%r12), %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r12), %rax
	jmp	.L2588
.L2485:
	movq	%r12, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %ecx
	movq	16(%r12), %rax
	jmp	.L2486
.L2589:
	leaq	2(%rax), %rdx
	movq	%rdx, 16(%r12)
	movzwl	(%rax), %eax
	jmp	.L2592
.L2533:
	movq	%r12, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r12), %rax
	jmp	.L2534
.L2719:
	movl	16(%r14), %eax
	cmpl	%eax, 24(%r14)
	jge	.L2723
.L2570:
	movslq	24(%r14), %rax
	movq	8(%r14), %rdx
	movb	%r12b, (%rdx,%rax)
	addl	$1, 24(%r14)
	jmp	.L2571
.L2587:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner27ScanIdentifierUnicodeEscapeEv
	movslq	%eax, %r12
	cmpl	$92, %r12d
	je	.L2565
	cmpl	$127, %r12d
	ja	.L2724
	movslq	%r12d, %rax
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	movzbl	(%rdx,%rax), %r14d
	andl	$1, %r14d
.L2596:
	testb	%r14b, %r14b
	je	.L2565
	movq	8(%rbx), %r15
	cmpb	$0, 28(%r15)
	leaq	8(%r15), %rdi
	je	.L2597
	cmpl	$255, %r12d
	jle	.L2725
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
	movq	-104(%rbp), %rdi
.L2597:
	movl	%r12d, %esi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L2600:
	xorl	%eax, %eax
	cmpl	$127, %r12d
	ja	.L2574
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rax
	movzbl	(%rax,%r12), %eax
	shrb	%al
	xorl	$1, %eax
	andl	$1, %eax
.L2574:
	movzbl	%al, %edx
	movzbl	%r14b, %esi
	jmp	.L2689
.L2453:
	cmpl	$61, %ecx
	jne	.L2390
	movl	$29, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movl	%eax, %r12d
	jmp	.L2390
.L2712:
	call	__stack_chk_fail@PLT
.L2720:
	movl	$1, %eax
	xorl	%r14d, %r14d
	jmp	.L2574
.L2593:
	testb	%al, %al
	sete	%al
	xorl	%r14d, %r14d
	jmp	.L2574
.L2724:
	movl	%r12d, %edi
	call	_ZN2v88internal21IsIdentifierStartSlowEi@PLT
	movl	%eax, %r14d
	jmp	.L2596
.L2725:
	movl	16(%r15), %eax
	cmpl	%eax, 24(%r15)
	jge	.L2726
.L2599:
	movslq	24(%r15), %rax
	movq	8(%r15), %rdx
	movb	%r12b, (%rdx,%rax)
	addl	$1, 24(%r15)
	jmp	.L2600
.L2398:
	movq	24(%rbx), %rdi
	call	_ZN2v88internal20Utf16CharacterStream4BackEv
	movl	$46, 32(%rbx)
	jmp	.L2390
.L2722:
	movzwl	(%rax), %edx
	jmp	.L2532
.L2723:
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	jmp	.L2570
.L2396:
	movq	%r14, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	jmp	.L2397
.L2726:
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	jmp	.L2599
	.cfi_endproc
.LFE18178:
	.size	_ZN2v88internal7Scanner8SeekNextEm, .-_ZN2v88internal7Scanner8SeekNextEm
	.section	.text._ZN2v88internal7Scanner13BookmarkScope5ApplyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner13BookmarkScope5ApplyEv
	.type	_ZN2v88internal7Scanner13BookmarkScope5ApplyEv, @function
_ZN2v88internal7Scanner13BookmarkScope5ApplyEv:
.LFB18104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	cmpb	$0, 16(%rdi)
	movq	24(%rax), %rdx
	je	.L2728
	cmpb	$0, 48(%rdx)
	jne	.L2729
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L2729:
	movq	$-1, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2728:
	.cfi_restore_state
	movb	$0, 48(%rdx)
	movq	8(%rdi), %rsi
	movq	(%rdi), %rdi
	call	_ZN2v88internal7Scanner8SeekNextEm
	movq	$-1, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18104:
	.size	_ZN2v88internal7Scanner13BookmarkScope5ApplyEv, .-_ZN2v88internal7Scanner13BookmarkScope5ApplyEv
	.section	.text._ZN2v88internal7Scanner4NextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner4NextEv
	.type	_ZN2v88internal7Scanner4NextEv, @function
_ZN2v88internal7Scanner4NextEv:
.LFB18119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rax
	cmpb	$112, 56(%rax)
	jne	.L2732
	movq	%r13, 8(%rdi)
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	.L2737(%rip), %r15
	movb	$0, 76(%r13)
	movq	24(%rdi), %rdx
	movq	32(%rdx), %r8
	movq	16(%rdx), %rax
	movq	8(%rdx), %rcx
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
.L2733:
	subq	%rcx, %rax
	movq	8(%rbx), %rdi
	sarq	%rax
	leal	-1(%rax,%r8), %eax
	movl	%eax, (%rdi)
	movslq	32(%rbx), %r14
	cmpl	$127, %r14d
	ja	.L2734
	movslq	%r14d, %rax
	movzbl	(%rdx,%rax), %r12d
	cmpb	$111, %r12b
	ja	.L2735
	movzbl	%r12b, %eax
	movslq	(%r15,%rax,4), %rax
	addq	%r15, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal7Scanner4NextEv,"a",@progbits
	.align 4
	.align 4
.L2737:
	.long	.L2757-.L2737
	.long	.L2735-.L2737
	.long	.L2756-.L2737
	.long	.L2738-.L2737
	.long	.L2735-.L2737
	.long	.L2738-.L2737
	.long	.L2738-.L2737
	.long	.L2738-.L2737
	.long	.L2738-.L2737
	.long	.L2738-.L2737
	.long	.L2735-.L2737
	.long	.L2755-.L2737
	.long	.L2738-.L2737
	.long	.L2738-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2754-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2738-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2753-.L2737
	.long	.L2752-.L2737
	.long	.L2751-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2750-.L2737
	.long	.L2749-.L2737
	.long	.L2748-.L2737
	.long	.L2735-.L2737
	.long	.L2747-.L2737
	.long	.L2746-.L2737
	.long	.L2745-.L2737
	.long	.L2738-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2744-.L2737
	.long	.L2743-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2742-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2741-.L2737
	.long	.L2735-.L2737
	.long	.L2740-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2735-.L2737
	.long	.L2739-.L2737
	.long	.L2738-.L2737
	.long	.L2735-.L2737
	.long	.L2736-.L2737
	.section	.text._ZN2v88internal7Scanner4NextEv
	.p2align 4,,10
	.p2align 3
.L2734:
	movl	%r14d, %edi
	call	_ZN2v88internal21IsIdentifierStartSlowEi@PLT
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	testb	%al, %al
	jne	.L2952
	movslq	32(%rbx), %r12
	movq	24(%rbx), %rcx
	movl	%r12d, %eax
	andl	$64512, %eax
	cmpl	$55296, %eax
	je	.L3069
.L2946:
	cmpl	$-1, %r12d
	je	.L3070
	movq	16(%rcx), %r14
	subq	8(%rcx), %r14
	sarq	%r14
	addq	32(%rcx), %r14
	jmp	.L2982
	.p2align 4,,10
	.p2align 3
.L2972:
	testb	$8, (%rsi,%r12)
	je	.L2977
.L2976:
	movq	8(%rbx), %rax
	cmpb	$0, 76(%rax)
	jne	.L2974
	movl	32(%rbx), %ecx
	cmpl	$10, %ecx
	je	.L2978
	cmpl	$13, %ecx
	je	.L2978
	subl	$8232, %ecx
	cmpl	$1, %ecx
	jbe	.L2978
	.p2align 4,,10
	.p2align 3
.L2974:
	movq	24(%rbx), %rcx
	movq	16(%rcx), %rax
	cmpq	24(%rcx), %rax
	jnb	.L2979
.L2981:
	movzwl	(%rax), %r12d
.L2980:
	addq	$2, %rax
	movq	%rax, 16(%rcx)
	movl	%r12d, 32(%rbx)
.L2982:
	cmpl	$127, %r12d
	jbe	.L2972
	movl	%r12d, %edi
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	testb	%al, %al
	jne	.L2976
	subl	$8232, %r12d
	cmpl	$1, %r12d
	jbe	.L2976
.L2977:
	movq	24(%rbx), %rcx
	movq	16(%rcx), %rax
	movq	32(%rcx), %r8
	movq	8(%rcx), %rcx
	movq	%rax, %rdi
	subq	%rcx, %rdi
	sarq	%rdi
	addq	%r8, %rdi
	cmpl	%edi, %r14d
	jne	.L2733
.L2936:
	movl	$109, %r12d
	jmp	.L2761
	.p2align 4,,10
	.p2align 3
.L2978:
	movb	$1, 76(%rax)
	jmp	.L2974
	.p2align 4,,10
	.p2align 3
.L2732:
	movq	%rax, %xmm0
	movq	%r13, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%rdi)
	movb	$112, 56(%r13)
	.p2align 4,,10
	.p2align 3
.L2983:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	movq	(%rbx), %rax
	movzbl	56(%rax), %eax
	jne	.L3071
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2979:
	.cfi_restore_state
	cmpb	$0, 48(%rcx)
	movq	$-1, %r12
	jne	.L2980
	movq	(%rcx), %rax
	movq	%rcx, -104(%rbp)
	movq	%rcx, %rdi
	call	*40(%rax)
	movq	-104(%rbp), %rcx
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	testb	%al, %al
	movq	16(%rcx), %rax
	jne	.L2981
	jmp	.L2980
.L2735:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2738:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2758
.L2760:
	movzwl	(%rax), %r15d
.L2759:
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%r15d, 32(%rbx)
	.p2align 4,,10
	.p2align 3
.L2761:
	movb	%r12b, 56(%r13)
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	subl	$1, %eax
	movl	%eax, 4(%r13)
	jmp	.L2983
.L2757:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2906
.L2908:
	movzwl	(%rax), %r14d
.L2907:
	addq	$2, %rax
	movq	%rbx, %rdi
	movq	%rax, 16(%r12)
	movl	%r14d, 32(%rbx)
	call	_ZN2v88internal7Scanner16ScanTemplateSpanEv
	movl	%eax, %r12d
	jmp	.L2761
.L2751:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2864
.L3052:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%edx, 32(%rbx)
	cmpl	$38, %edx
	je	.L3072
	cmpl	$61, %edx
	jne	.L2761
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2872
.L2874:
	movzwl	(%rax), %r14d
.L2873:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$20, %r12d
	movl	%r14d, 32(%rbx)
	jmp	.L2761
.L2753:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2875
.L3053:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%edx, 32(%rbx)
	cmpl	$124, %edx
	je	.L3073
	cmpl	$61, %edx
	jne	.L2761
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2883
.L2885:
	movzwl	(%rax), %r14d
.L2884:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$18, %r12d
	movl	%r14d, 32(%rbx)
	jmp	.L2761
.L2752:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2886
.L3054:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	jne	.L2761
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2890
.L2892:
	movzwl	(%rax), %r14d
.L2891:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$19, %r12d
	movl	%r14d, 32(%rbx)
	jmp	.L2761
.L2754:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2790
.L3043:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	je	.L3074
	cmpl	$62, %edx
	jne	.L2761
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2795
.L2797:
	movzwl	(%rax), %r14d
.L2796:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$15, %r12d
	movl	%r14d, 32(%rbx)
	jmp	.L2761
.L2743:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2779
.L3042:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	je	.L3075
	cmpl	$62, %edx
	jne	.L2761
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2787
	movzwl	(%rax), %edx
.L2788:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	je	.L3076
	movl	$38, %r12d
	cmpl	$62, %edx
	jne	.L2761
	movl	$39, %ecx
	movl	$23, %edx
	movl	$61, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectEiNS0_5Token5ValueES3_
	movl	%eax, %r12d
	jmp	.L2761
.L2745:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2798
.L3044:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	jne	.L2761
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2802
.L3045:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$55, %r12d
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	jne	.L2761
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2806
.L2808:
	movzwl	(%rax), %r14d
.L2807:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$56, %r12d
	movl	%r14d, 32(%rbx)
	jmp	.L2761
.L2741:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner10ScanStringEv
	movl	%eax, %r12d
	jmp	.L2761
.L2740:
	movq	8(%rbx), %rax
	movl	$0, 24(%rax)
	movb	$1, 28(%rax)
	movl	32(%rbx), %r15d
	cmpl	$127, %r15d
	ja	.L3007
	cmpl	$92, %r15d
	je	.L2926
	movslq	%r15d, %rax
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rdx
	movq	8(%rbx), %r14
	movzbl	(%rdx,%rax), %eax
	sarl	%eax
	movb	%al, -81(%rbp)
	movslq	24(%r14), %rax
	cmpl	16(%r14), %eax
	jge	.L3077
.L2927:
	movq	8(%r14), %rdx
	movb	%r15b, (%rdx,%rax)
	leaq	-81(%rbp), %rax
	leaq	-80(%rbp), %r15
	addl	$1, 24(%r14)
	movq	24(%rbx), %r14
	movq	%rbx, -80(%rbp)
	movq	%rax, -72(%rbp)
.L2930:
	movq	24(%r14), %rsi
	movq	16(%r14), %rdi
	movq	%r15, %rdx
	call	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal20Utf16CharacterStream12AdvanceUntilIZNS6_7Scanner28ScanIdentifierOrKeywordInnerEvEUliE_EEiT_EUltE_EEESB_SB_SB_T0_St26random_access_iterator_tag
	cmpq	%rax, 24(%r14)
	jne	.L2928
	cmpb	$0, 48(%r14)
	movq	%rax, 16(%r14)
	jne	.L2929
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L2930
	movq	16(%r14), %rax
.L2929:
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	$-1, %eax
.L2931:
	movl	%eax, 32(%rbx)
	movzbl	-81(%rbp), %eax
	movl	%eax, %edx
	andl	$2, %edx
	testb	$16, %al
	jne	.L2932
	testb	%dl, %dl
	jne	.L2761
	movq	8(%rbx), %rdx
	movl	24(%rdx), %eax
	leal	-2(%rax), %ecx
	cmpl	$8, %ecx
	ja	.L2761
	movq	8(%rdx), %rsi
	leaq	_ZZN2v88internal18PerfectKeywordHash4HashEPKciE11asso_values(%rip), %rdx
	movzbl	1(%rsi), %ecx
	movzbl	(%rsi), %edi
	movzbl	(%rdx,%rcx), %ecx
	movzbl	(%rdx,%rdi), %edx
	addl	%eax, %ecx
	addl	%edx, %ecx
	leaq	_ZN2v88internalL26kPerfectKeywordLengthTableE(%rip), %rdx
	andl	$63, %ecx
	movzbl	(%rdx,%rcx), %edx
	cmpl	%edx, %eax
	jne	.L2761
	movq	%rcx, %rax
	leaq	_ZN2v88internalL24kPerfectKeywordHashTableE(%rip), %r8
	salq	$4, %rax
	movq	(%r8,%rax), %rdi
	xorl	%eax, %eax
	jmp	.L2935
	.p2align 4,,10
	.p2align 3
.L3078:
	addq	$1, %rax
	cmpb	-1(%rsi,%rax), %dl
	jne	.L2761
.L2935:
	movzbl	(%rdi,%rax), %edx
	testb	%dl, %dl
	jne	.L3078
	salq	$4, %rcx
	movzbl	8(%r8,%rcx), %r12d
	jmp	.L2761
	.p2align 4,,10
	.p2align 3
.L2736:
	movq	24(%rbx), %rax
	movq	16(%rax), %r12
	subq	8(%rax), %r12
	sarq	%r12
	addq	32(%rax), %r12
	jmp	.L2923
	.p2align 4,,10
	.p2align 3
.L2913:
	testb	$8, (%rsi,%r14)
	je	.L2918
.L2917:
	movq	8(%rbx), %rax
	cmpb	$0, 76(%rax)
	jne	.L2915
	movl	32(%rbx), %ecx
	cmpl	$10, %ecx
	je	.L2919
	cmpl	$13, %ecx
	je	.L2919
	subl	$8232, %ecx
	cmpl	$1, %ecx
	jbe	.L2919
	.p2align 4,,10
	.p2align 3
.L2915:
	movq	24(%rbx), %rcx
	movq	16(%rcx), %rax
	cmpq	24(%rcx), %rax
	jnb	.L2920
.L2922:
	movzwl	(%rax), %r14d
.L2921:
	addq	$2, %rax
	movq	%rax, 16(%rcx)
	movl	%r14d, 32(%rbx)
.L2923:
	cmpl	$127, %r14d
	jbe	.L2913
	movl	%r14d, %edi
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	testb	%al, %al
	jne	.L2917
	subl	$8232, %r14d
	cmpl	$1, %r14d
	jbe	.L2917
.L2918:
	movq	24(%rbx), %rcx
	movq	16(%rcx), %rax
	movq	32(%rcx), %r8
	movq	8(%rcx), %rcx
	movq	%rax, %rdi
	subq	%rcx, %rdi
	sarq	%rdi
	addq	%r8, %rdi
	cmpl	%edi, %r12d
	jne	.L2733
	jmp	.L2936
.L2746:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2820
.L3047:
	movzwl	(%rax), %ecx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%ecx, 32(%rbx)
	cmpl	$45, %ecx
	jne	.L2824
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2825
.L3048:
	movzwl	(%rax), %ecx
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	%ecx, 32(%rbx)
	cmpl	$62, %ecx
	jne	.L2994
	movq	8(%rbx), %rax
	cmpb	$0, 76(%rax)
	jne	.L3079
.L2994:
	movl	$52, %r12d
	jmp	.L2761
.L2747:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2809
.L3046:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%edx, 32(%rbx)
	cmpl	$43, %edx
	je	.L3080
	cmpl	$61, %edx
	jne	.L2761
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2817
.L2819:
	movzwl	(%rax), %r14d
.L2818:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$28, %r12d
	movl	%r14d, 32(%rbx)
	jmp	.L2761
.L2748:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2838
.L3050:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	jne	.L2761
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2842
.L2844:
	movzwl	(%rax), %r14d
.L2843:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$26, %r12d
	movl	%r14d, 32(%rbx)
	jmp	.L2761
.L2750:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2830
.L3049:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%edx, 32(%rbx)
	cmpl	$42, %edx
	je	.L3081
	cmpl	$61, %edx
	jne	.L2761
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2835
.L2837:
	movzwl	(%rax), %r14d
.L2836:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$24, %r12d
	movl	%r14d, 32(%rbx)
	jmp	.L2761
.L2742:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner10ScanNumberEb
	movl	%eax, %r12d
	jmp	.L2761
.L2756:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2893
.L3055:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	leal	-48(%rdx), %eax
	movl	%edx, 32(%rbx)
	cmpl	$9, %eax
	jbe	.L3082
	cmpl	$46, %edx
	jne	.L2761
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2899
	cmpw	$46, (%rax)
	jne	.L2761
.L2903:
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movq	24(%rbx), %r12
	movl	%edx, 32(%rbx)
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2904
	movzwl	(%rax), %edx
.L2905:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$10, %r12d
	movl	%edx, 32(%rbx)
	jmp	.L2761
	.p2align 4,,10
	.p2align 3
.L2739:
	movq	24(%rbx), %r12
	movq	16(%r12), %rcx
	movq	%rcx, %rax
	subq	8(%r12), %rax
	sarq	%rax
	addq	32(%r12), %rax
	cmpl	$1, %eax
	jne	.L2912
	cmpq	24(%r12), %rcx
	jnb	.L2910
	movzwl	(%rcx), %eax
.L2911:
	cmpl	$33, %eax
	je	.L3057
.L2912:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner15ScanPrivateNameEv
	movl	%eax, %r12d
	jmp	.L2761
.L2749:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2845
.L3051:
	movzwl	(%rax), %ecx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%ecx, 32(%rbx)
	cmpl	$47, %ecx
	je	.L3083
	cmpl	$42, %ecx
	je	.L3084
	cmpl	$61, %ecx
	jne	.L2761
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2861
.L2863:
	movzwl	(%rax), %r14d
.L2862:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$25, %r12d
	movl	%r14d, 32(%rbx)
	jmp	.L2761
.L2744:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2772
.L3041:
	movzwl	(%rax), %ecx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	%ecx, 32(%rbx)
	cmpl	$61, %ecx
	je	.L3085
	cmpl	$60, %ecx
	je	.L3086
	cmpl	$33, %ecx
	jne	.L2761
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner15ScanHtmlCommentEv
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	movl	%eax, %r12d
.L2778:
	cmpb	$111, %r12b
	jne	.L2761
.L2859:
	movq	24(%rbx), %rcx
	movq	32(%rcx), %r8
	movq	16(%rcx), %rax
	movq	8(%rcx), %rcx
	jmp	.L2733
.L2755:
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2762
.L2765:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r14)
	cmpb	$0, 281(%rbx)
	movl	%edx, 32(%rbx)
	jne	.L3087
.L2766:
	cmpb	$0, 282(%rbx)
	je	.L2761
	cmpl	$63, %edx
	jne	.L2761
	movl	$31, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movl	%eax, %r12d
	jmp	.L2761
	.p2align 4,,10
	.p2align 3
.L2919:
	movb	$1, 76(%rax)
	jmp	.L2915
	.p2align 4,,10
	.p2align 3
.L2920:
	cmpb	$0, 48(%rcx)
	movq	$-1, %r14
	jne	.L2921
	movq	(%rcx), %rax
	movq	%rcx, -104(%rbp)
	movq	%rcx, %rdi
	call	*40(%rax)
	movq	-104(%rbp), %rcx
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	testb	%al, %al
	movq	16(%rcx), %rax
	jne	.L2922
	jmp	.L2921
	.p2align 4,,10
	.p2align 3
.L3069:
	movq	16(%rcx), %rax
	cmpq	24(%rcx), %rax
	jnb	.L2947
.L2950:
	movzwl	(%rax), %r8d
	addq	$2, %rax
	movq	%rax, 16(%rcx)
	movl	%r8d, %eax
	andl	$64512, %eax
	cmpl	$56320, %eax
	je	.L3088
.L2951:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	8(%rdi), %rcx
	cmpq	%rcx, %rax
	jbe	.L2954
	subq	$2, %rax
	movq	%rax, 16(%rdi)
	movslq	32(%rbx), %r12
	movq	24(%rbx), %rcx
	jmp	.L2946
.L3083:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2850
	movzwl	(%rax), %ecx
	cmpl	$35, %ecx
	je	.L2851
	cmpl	$64, %ecx
	jne	.L3057
.L2851:
	movzwl	(%rax), %eax
.L2855:
	addq	$2, 16(%r12)
	movq	24(%rbx), %r12
	movl	%eax, 32(%rbx)
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2856
	movzwl	(%rax), %ecx
.L2857:
	addq	$2, %rax
	movq	%rbx, %rdi
	movq	%rax, 16(%r12)
	movl	%ecx, 32(%rbx)
	call	_ZN2v88internal7Scanner26TryToParseSourceURLCommentEv
	movl	32(%rbx), %eax
	xorl	%edi, %edi
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	leal	1(%rax), %ecx
	cmpl	$14, %ecx
	ja	.L2858
	movl	$18433, %edi
	shrq	%cl, %rdi
	andl	$1, %edi
.L2858:
	subl	$8232, %eax
	cmpl	$1, %eax
	jbe	.L2859
	testb	%dil, %dil
	jne	.L2859
	.p2align 4,,10
	.p2align 3
.L3057:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner21SkipSingleLineCommentEv
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	movl	%eax, %r12d
	jmp	.L2778
	.p2align 4,,10
	.p2align 3
.L3079:
	cmpb	$0, 283(%rbx)
	je	.L3057
	movl	348(%rbx), %eax
	movl	$109, %r12d
	testl	%eax, %eax
	jne	.L2761
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	movl	$275, 348(%rbx)
	leal	-1(%rax), %edx
	movl	%eax, 356(%rbx)
	movl	%edx, 352(%rbx)
	jmp	.L2761
	.p2align 4,,10
	.p2align 3
.L3082:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner10ScanNumberEb
	movl	%eax, %r12d
	jmp	.L2761
	.p2align 4,,10
	.p2align 3
.L3088:
	movl	32(%rbx), %edi
	andl	$1023, %r8d
	sall	$10, %edi
	andl	$1047552, %edi
	orl	%r8d, %edi
	addl	$65536, %edi
	movl	%edi, 32(%rbx)
	call	_ZN2v88internal21IsIdentifierStartSlowEi@PLT
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	testb	%al, %al
	jne	.L2952
.L3036:
	movslq	32(%rbx), %r12
	movq	24(%rbx), %rcx
	jmp	.L2946
	.p2align 4,,10
	.p2align 3
.L3077:
	leaq	8(%r14), %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r14), %rax
	jmp	.L2927
.L3072:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2869
.L2871:
	movzwl	(%rax), %r14d
.L2870:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$33, %r12d
	movl	%r14d, 32(%rbx)
	jmp	.L2761
.L3074:
	movl	$53, %ecx
	movl	$54, %edx
	movl	$61, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectEiNS0_5Token5ValueES3_
	movl	%eax, %r12d
	jmp	.L2761
.L3080:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2814
.L2816:
	movzwl	(%rax), %r14d
.L2815:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$51, %r12d
	movl	%r14d, 32(%rbx)
	jmp	.L2761
.L3075:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2784
.L2786:
	movzwl	(%rax), %r14d
.L2785:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$60, %r12d
	movl	%r14d, 32(%rbx)
	jmp	.L2761
.L3073:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L2880
.L2882:
	movzwl	(%rax), %r14d
.L2881:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$32, %r12d
	movl	%r14d, 32(%rbx)
	jmp	.L2761
.L3081:
	movl	$43, %ecx
	movl	$27, %edx
	movl	$61, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectEiNS0_5Token5ValueES3_
	movl	%eax, %r12d
	jmp	.L2761
.L2809:
	cmpb	$0, 48(%r14)
	jne	.L2764
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L3068
	movq	16(%r14), %rax
	jmp	.L3046
.L2762:
	cmpb	$0, 48(%r14)
	jne	.L2764
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r14), %rax
	jne	.L2765
	.p2align 4,,10
	.p2align 3
.L2764:
	addq	$2, %rax
	movq	%rax, 16(%r14)
	movl	$-1, 32(%rbx)
	jmp	.L2761
	.p2align 4,,10
	.p2align 3
.L3068:
	addq	$2, 16(%r14)
	movl	$-1, 32(%rbx)
	jmp	.L2761
.L2758:
	cmpb	$0, 48(%r14)
	movl	$-1, %r15d
	jne	.L2759
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r14), %rax
	jne	.L2760
	jmp	.L2759
.L2864:
	cmpb	$0, 48(%r14)
	jne	.L2764
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L3068
	movq	16(%r14), %rax
	jmp	.L3052
.L2886:
	cmpb	$0, 48(%r14)
	jne	.L2764
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L3068
	movq	16(%r14), %rax
	jmp	.L3054
.L2838:
	cmpb	$0, 48(%r14)
	jne	.L2764
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L3068
	movq	16(%r14), %rax
	jmp	.L3050
.L3007:
	xorl	%esi, %esi
	movl	$1, %edx
.L3060:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner32ScanIdentifierOrKeywordInnerSlowEbb
	movl	%eax, %r12d
	jmp	.L2761
.L2820:
	cmpb	$0, 48(%r14)
	jne	.L2764
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L3068
	movq	16(%r14), %rax
	jmp	.L3047
.L2798:
	cmpb	$0, 48(%r14)
	jne	.L2764
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L3068
	movq	16(%r14), %rax
	jmp	.L3044
.L2772:
	cmpb	$0, 48(%r14)
	jne	.L2764
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L3068
	movq	16(%r14), %rax
	jmp	.L3041
.L2845:
	cmpb	$0, 48(%r14)
	jne	.L2764
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L3068
	movq	16(%r14), %rax
	jmp	.L3051
.L2779:
	cmpb	$0, 48(%r14)
	jne	.L2764
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L3068
	movq	16(%r14), %rax
	jmp	.L3042
.L2906:
	cmpb	$0, 48(%r12)
	movl	$-1, %r14d
	jne	.L2907
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2908
	jmp	.L2907
.L2875:
	cmpb	$0, 48(%r14)
	jne	.L2764
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L3068
	movq	16(%r14), %rax
	jmp	.L3053
.L2893:
	cmpb	$0, 48(%r14)
	jne	.L2764
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L3068
	movq	16(%r14), %rax
	jmp	.L3055
.L2790:
	cmpb	$0, 48(%r14)
	jne	.L2764
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L3068
	movq	16(%r14), %rax
	jmp	.L3043
.L2830:
	cmpb	$0, 48(%r14)
	jne	.L2764
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L3068
	movq	16(%r14), %rax
	jmp	.L3049
.L2910:
	cmpb	$0, 48(%r12)
	jne	.L2912
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L2912
	movq	16(%r12), %rax
	movzwl	(%rax), %eax
	jmp	.L2911
.L2825:
	cmpb	$0, 48(%r12)
	je	.L2827
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$52, %r12d
	movl	$-1, 32(%rbx)
	jmp	.L2761
.L3087:
	cmpl	$46, %edx
	jne	.L2766
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jnb	.L2767
	movzwl	(%rax), %eax
.L2768:
	addq	$2, 16(%r14)
	movl	%eax, 32(%rbx)
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L2769
	movl	$4, %r12d
	jmp	.L2761
	.p2align 4,,10
	.p2align 3
.L2926:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner27ScanIdentifierUnicodeEscapeEv
	movslq	%eax, %r12
	cmpl	$92, %r12d
	je	.L2936
	cmpl	$127, %r12d
	ja	.L3089
	movslq	%r12d, %rax
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
	andl	$1, %eax
.L2938:
	testb	%al, %al
	je	.L2936
	movq	8(%rbx), %r14
	cmpb	$0, 28(%r14)
	leaq	8(%r14), %r15
	je	.L2939
	cmpl	$255, %r12d
	jle	.L3090
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
.L2939:
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L2942:
	cmpl	$127, %r12d
	ja	.L3008
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rax
	movl	$1, %esi
	movzbl	(%rax,%r12), %edx
	shrb	%dl
	xorl	$1, %edx
	andl	$1, %edx
	jmp	.L3060
.L3084:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner20SkipMultiLineCommentEv
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	movl	%eax, %r12d
	jmp	.L2778
.L2932:
	testb	%dl, %dl
	sete	%dl
	xorl	%esi, %esi
	movzbl	%dl, %edx
	jmp	.L3060
.L2928:
	leaq	2(%rax), %rdx
	movq	%rdx, 16(%r14)
	movzwl	(%rax), %eax
	jmp	.L2931
.L3076:
	movl	$22, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movl	%eax, %r12d
	jmp	.L2761
.L2947:
	cmpb	$0, 48(%rcx)
	jne	.L2949
	movq	(%rcx), %rax
	movq	%rcx, -104(%rbp)
	movq	%rcx, %rdi
	call	*40(%rax)
	movq	-104(%rbp), %rcx
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	testb	%al, %al
	movq	16(%rcx), %rax
	jne	.L2950
.L2949:
	addq	$2, %rax
	movq	%rax, 16(%rcx)
	jmp	.L2951
.L2952:
	movq	8(%rbx), %rax
	xorl	%r12d, %r12d
	movl	$0, 24(%rax)
	movb	$1, 28(%rax)
	movl	32(%rbx), %r14d
	movl	$1, %eax
	cmpl	$127, %r14d
	ja	.L2945
	cmpl	$92, %r14d
	je	.L2958
	movslq	%r14d, %rax
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rdx
	movq	8(%rbx), %r12
	movzbl	(%rdx,%rax), %eax
	sarl	%eax
	movb	%al, -81(%rbp)
	movslq	24(%r12), %rax
	cmpl	16(%r12), %eax
	jge	.L3091
.L2959:
	movq	8(%r12), %rdx
	leaq	-80(%rbp), %r15
	movb	%r14b, (%rdx,%rax)
	leaq	-81(%rbp), %rax
	addl	$1, 24(%r12)
	movq	24(%rbx), %r12
	movq	%rbx, -80(%rbp)
	movq	%rax, -72(%rbp)
.L2962:
	movq	24(%r12), %rsi
	movq	16(%r12), %rdi
	movq	%r15, %rdx
	call	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal20Utf16CharacterStream12AdvanceUntilIZNS6_7Scanner28ScanIdentifierOrKeywordInnerEvEUliE_EEiT_EUltE_EEESB_SB_SB_T0_St26random_access_iterator_tag
	cmpq	%rax, 24(%r12)
	jne	.L2960
	cmpb	$0, 48(%r12)
	movq	%rax, 16(%r12)
	jne	.L2961
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L2962
	movq	16(%r12), %rax
.L2961:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$-1, %eax
.L2963:
	movzbl	-81(%rbp), %edx
	movl	%eax, 32(%rbx)
	movl	%edx, %eax
	andl	$2, %eax
	andl	$16, %edx
	jne	.L2964
	movl	$92, %r12d
	testb	%al, %al
	jne	.L2761
	movq	8(%rbx), %rax
	movl	24(%rax), %esi
	leal	-2(%rsi), %edx
	cmpl	$8, %edx
	ja	.L2761
	movq	8(%rax), %rdi
	call	_ZN2v88internal18PerfectKeywordHash8GetTokenEPKci.part.0
	movl	%eax, %r12d
	jmp	.L2761
.L2954:
	movq	32(%rdi), %r8
	subq	%rcx, %rax
	movq	%rcx, 16(%rdi)
	sarq	%rax
	cmpb	$0, 48(%rdi)
	leaq	-1(%r8,%rax), %rax
	movq	%rax, 32(%rdi)
	jne	.L3036
	movq	(%rdi), %rax
	call	*40(%rax)
	movslq	32(%rbx), %r12
	movq	24(%rbx), %rcx
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	jmp	.L2946
.L2827:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L2828
	addq	$2, 16(%r12)
	movl	$52, %r12d
	movl	$-1, 32(%rbx)
	jmp	.L2761
.L2828:
	movq	16(%r12), %rax
	jmp	.L3048
.L2802:
	cmpb	$0, 48(%r12)
	je	.L2804
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movl	$55, %r12d
	movl	$-1, 32(%rbx)
	jmp	.L2761
.L3070:
	cmpb	$1, 48(%rcx)
	sbbl	%r12d, %r12d
	andl	$-95, %r12d
	addl	$109, %r12d
	jmp	.L2761
.L2835:
	cmpb	$0, 48(%r12)
	movl	$-1, %r14d
	jne	.L2836
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2837
	jmp	.L2836
.L2872:
	cmpb	$0, 48(%r12)
	movl	$-1, %r14d
	jne	.L2873
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2874
	jmp	.L2873
.L2787:
	movq	%r12, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r12), %rax
	jmp	.L2788
.L2817:
	cmpb	$0, 48(%r12)
	movl	$-1, %r14d
	jne	.L2818
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2819
	jmp	.L2818
.L2883:
	cmpb	$0, 48(%r12)
	movl	$-1, %r14d
	jne	.L2884
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2885
	jmp	.L2884
.L2795:
	cmpb	$0, 48(%r12)
	movl	$-1, %r14d
	jne	.L2796
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2797
	jmp	.L2796
.L2842:
	cmpb	$0, 48(%r12)
	movl	$-1, %r14d
	jne	.L2843
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2844
	jmp	.L2843
.L2861:
	cmpb	$0, 48(%r12)
	movl	$-1, %r14d
	jne	.L2862
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2863
	jmp	.L2862
.L2890:
	cmpb	$0, 48(%r12)
	movl	$-1, %r14d
	jne	.L2891
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2892
	jmp	.L2891
.L2899:
	movq	%r14, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	cmpl	$46, %eax
	jne	.L2761
	movq	24(%rbx), %r14
	movq	16(%r14), %rax
	cmpq	24(%r14), %rax
	jb	.L3092
	movq	%r14, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r14), %rax
	jmp	.L2903
.L3008:
	movl	$1, %esi
	xorl	%edx, %edx
	jmp	.L3060
.L3089:
	movl	%r12d, %edi
	call	_ZN2v88internal21IsIdentifierStartSlowEi@PLT
	jmp	.L2938
.L2804:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L2805
	addq	$2, 16(%r12)
	movl	$55, %r12d
	movl	$-1, 32(%rbx)
	jmp	.L2761
.L2805:
	movq	16(%r12), %rax
	jmp	.L3045
.L2869:
	orl	$-1, %r14d
	cmpb	$0, 48(%r12)
	jne	.L2870
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2871
	jmp	.L2870
.L3085:
	movl	$59, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movl	%eax, %r12d
	jmp	.L2761
.L2850:
	movq	%r12, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	cmpl	$35, %eax
	je	.L3014
	cmpl	$64, %eax
	jne	.L3057
.L3014:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jb	.L2851
	movq	%r12, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	jmp	.L2855
.L2784:
	orl	$-1, %r14d
	cmpb	$0, 48(%r12)
	jne	.L2785
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2786
	jmp	.L2785
.L2880:
	orl	$-1, %r14d
	cmpb	$0, 48(%r12)
	jne	.L2881
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2882
	jmp	.L2881
.L2814:
	orl	$-1, %r14d
	cmpb	$0, 48(%r12)
	jne	.L2815
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2816
	jmp	.L2815
.L3086:
	movl	$37, %ecx
	movl	$21, %edx
	movl	$61, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectEiNS0_5Token5ValueES3_
	movl	%eax, %r12d
	jmp	.L2761
.L2806:
	orl	$-1, %r14d
	cmpb	$0, 48(%r12)
	jne	.L2807
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L2808
	jmp	.L2807
.L3091:
	leaq	8(%r12), %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r12), %rax
	jmp	.L2959
.L2958:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner27ScanIdentifierUnicodeEscapeEv
	movl	%eax, %r15d
	cmpl	$92, %eax
	je	.L2936
	cmpl	$127, %eax
	ja	.L3093
	cltq
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	movzbl	(%rdx,%rax), %r12d
	andl	$1, %r12d
.L2967:
	testb	%r12b, %r12b
	je	.L2936
	movq	8(%rbx), %r14
	cmpb	$0, 28(%r14)
	leaq	8(%r14), %rdi
	je	.L2968
	cmpl	$255, %r15d
	jle	.L3094
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
	movq	-104(%rbp), %rdi
.L2968:
	movl	%r15d, %esi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L2971:
	xorl	%eax, %eax
	cmpl	$127, %r15d
	ja	.L2945
	movslq	%r15d, %rax
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
	shrb	%al
	xorl	$1, %eax
	andl	$1, %eax
.L2945:
	movzbl	%al, %edx
	movzbl	%r12b, %esi
	jmp	.L3060
.L3071:
	call	__stack_chk_fail@PLT
.L2824:
	cmpl	$61, %ecx
	jne	.L2761
	movl	$29, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movl	%eax, %r12d
	jmp	.L2761
.L2856:
	movq	%r12, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %ecx
	movq	16(%r12), %rax
	jmp	.L2857
.L3090:
	movl	16(%r14), %eax
	cmpl	%eax, 24(%r14)
	jge	.L3095
.L2941:
	movslq	24(%r14), %rax
	movq	8(%r14), %rdx
	movb	%r12b, (%rdx,%rax)
	addl	$1, 24(%r14)
	jmp	.L2942
.L2904:
	movq	%r12, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r12), %rax
	jmp	.L2905
.L2960:
	leaq	2(%rax), %rdx
	movq	%rdx, 16(%r12)
	movzwl	(%rax), %eax
	jmp	.L2963
.L2964:
	testb	%al, %al
	sete	%al
	xorl	%r12d, %r12d
	jmp	.L2945
.L3093:
	movl	%eax, %edi
	call	_ZN2v88internal21IsIdentifierStartSlowEi@PLT
	movl	%eax, %r12d
	jmp	.L2967
.L3095:
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	jmp	.L2941
.L2769:
	movq	24(%rbx), %rdi
	call	_ZN2v88internal20Utf16CharacterStream4BackEv
	movl	$46, 32(%rbx)
	jmp	.L2761
.L2767:
	movq	%r14, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	jmp	.L2768
.L3094:
	movl	16(%r14), %eax
	cmpl	%eax, 24(%r14)
	jge	.L3096
.L2970:
	movslq	24(%r14), %rax
	movq	8(%r14), %rdx
	movb	%r15b, (%rdx,%rax)
	addl	$1, 24(%r14)
	jmp	.L2971
.L3092:
	movzwl	(%rax), %edx
	jmp	.L2903
.L3096:
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	jmp	.L2970
	.cfi_endproc
.LFE18119:
	.size	_ZN2v88internal7Scanner4NextEv, .-_ZN2v88internal7Scanner4NextEv
	.section	.text._ZN2v88internal7Scanner11SeekForwardEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Scanner11SeekForwardEi
	.type	_ZN2v88internal7Scanner11SeekForwardEi, @function
_ZN2v88internal7Scanner11SeekForwardEi:
.LFB18141:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	%esi, (%r12)
	je	.L3097
	movq	%rdi, %rbx
	movq	24(%rdi), %rdi
	movq	16(%rdi), %rax
	movq	8(%rdi), %r8
	movq	32(%rdi), %rcx
	movq	%rax, %rdx
	subq	%r8, %rdx
	sarq	%rdx
	leal	-1(%rdx,%rcx), %edx
	cmpl	%edx, %esi
	jne	.L3398
.L3099:
	movq	%r12, %rdi
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	leaq	.L3109(%rip), %r15
.L3105:
	subq	%r8, %rax
	sarq	%rax
	leal	-1(%rax,%rcx), %eax
	movl	%eax, (%rdi)
	movslq	32(%rbx), %r13
	cmpl	$127, %r13d
	ja	.L3106
	movslq	%r13d, %rax
	movzbl	(%rdx,%rax), %r14d
	cmpb	$111, %r14b
	ja	.L3107
	movzbl	%r14b, %eax
	movslq	(%r15,%rax,4), %rax
	addq	%r15, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal7Scanner11SeekForwardEi,"a",@progbits
	.align 4
	.align 4
.L3109:
	.long	.L3129-.L3109
	.long	.L3107-.L3109
	.long	.L3128-.L3109
	.long	.L3110-.L3109
	.long	.L3107-.L3109
	.long	.L3110-.L3109
	.long	.L3110-.L3109
	.long	.L3110-.L3109
	.long	.L3110-.L3109
	.long	.L3110-.L3109
	.long	.L3107-.L3109
	.long	.L3127-.L3109
	.long	.L3110-.L3109
	.long	.L3110-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3126-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3110-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3125-.L3109
	.long	.L3124-.L3109
	.long	.L3123-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3122-.L3109
	.long	.L3121-.L3109
	.long	.L3120-.L3109
	.long	.L3107-.L3109
	.long	.L3119-.L3109
	.long	.L3118-.L3109
	.long	.L3117-.L3109
	.long	.L3110-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3116-.L3109
	.long	.L3115-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3114-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3113-.L3109
	.long	.L3107-.L3109
	.long	.L3112-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3107-.L3109
	.long	.L3111-.L3109
	.long	.L3110-.L3109
	.long	.L3107-.L3109
	.long	.L3108-.L3109
	.section	.text._ZN2v88internal7Scanner11SeekForwardEi
	.p2align 4,,10
	.p2align 3
.L3110:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3130
.L3132:
	movzwl	(%rax), %r15d
.L3131:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%r15d, 32(%rbx)
	.p2align 4,,10
	.p2align 3
.L3133:
	movb	%r14b, 56(%r12)
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	subl	$1, %eax
	movl	%eax, 4(%r12)
.L3097:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3399
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3398:
	.cfi_restore_state
	movslq	%esi, %rsi
	cmpq	%rcx, %rsi
	jb	.L3100
	movq	24(%rdi), %rax
	subq	%r8, %rax
	sarq	%rax
	addq	%rcx, %rax
	cmpq	%rax, %rsi
	jnb	.L3100
	subq	%rcx, %rsi
	leaq	(%r8,%rsi,2), %rax
	movq	%rax, 16(%rdi)
.L3101:
	movq	24(%rbx), %r12
	movq	16(%r12), %rax
	cmpq	24(%r12), %rax
	jnb	.L3102
.L3104:
	movzwl	(%rax), %r13d
.L3103:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	movq	8(%rbx), %rax
	movl	%r13d, 32(%rbx)
	movb	$0, 76(%rax)
	movq	24(%rbx), %rdx
	movq	8(%rbx), %r12
	movq	32(%rdx), %rcx
	movq	16(%rdx), %rax
	movq	8(%rdx), %r8
	jmp	.L3099
	.p2align 4,,10
	.p2align 3
.L3106:
	movl	%r13d, %edi
	call	_ZN2v88internal21IsIdentifierStartSlowEi@PLT
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	testb	%al, %al
	jne	.L3299
	movslq	32(%rbx), %r13
	movq	24(%rbx), %rcx
	movl	%r13d, %eax
	andl	$64512, %eax
	cmpl	$55296, %eax
	je	.L3400
.L3293:
	cmpl	$-1, %r13d
	je	.L3401
	movq	16(%rcx), %r14
	subq	8(%rcx), %r14
	sarq	%r14
	addq	32(%rcx), %r14
	jmp	.L3329
	.p2align 4,,10
	.p2align 3
.L3319:
	testb	$8, (%rsi,%r13)
	je	.L3265
.L3323:
	movq	8(%rbx), %rax
	cmpb	$0, 76(%rax)
	jne	.L3321
	movl	32(%rbx), %ecx
	cmpl	$10, %ecx
	je	.L3325
	cmpl	$13, %ecx
	je	.L3325
	subl	$8232, %ecx
	cmpl	$1, %ecx
	jbe	.L3325
	.p2align 4,,10
	.p2align 3
.L3321:
	movq	24(%rbx), %rcx
	movq	16(%rcx), %rax
	cmpq	24(%rcx), %rax
	jnb	.L3326
.L3328:
	movzwl	(%rax), %r13d
.L3327:
	addq	$2, %rax
	movq	%rax, 16(%rcx)
	movl	%r13d, 32(%rbx)
.L3329:
	cmpl	$127, %r13d
	jbe	.L3319
	movl	%r13d, %edi
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	testb	%al, %al
	jne	.L3323
	subl	$8232, %r13d
	cmpl	$1, %r13d
	jbe	.L3323
.L3265:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	8(%rdi), %r8
	movq	32(%rdi), %rcx
	movq	%rax, %rdi
	subq	%r8, %rdi
	sarq	%rdi
	addq	%rcx, %rdi
	cmpl	%edi, %r14d
	je	.L3283
.L3271:
	movq	8(%rbx), %rdi
	jmp	.L3105
	.p2align 4,,10
	.p2align 3
.L3325:
	movb	$1, 76(%rax)
	jmp	.L3321
	.p2align 4,,10
	.p2align 3
.L3100:
	cmpb	$0, 48(%rdi)
	movq	%rsi, 32(%rdi)
	movq	%r8, 16(%rdi)
	jne	.L3101
	movq	(%rdi), %rax
	call	*40(%rax)
	jmp	.L3101
	.p2align 4,,10
	.p2align 3
.L3326:
	cmpb	$0, 48(%rcx)
	movq	$-1, %r13
	jne	.L3327
	movq	(%rcx), %rax
	movq	%rcx, -104(%rbp)
	movq	%rcx, %rdi
	call	*40(%rax)
	movq	-104(%rbp), %rcx
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	testb	%al, %al
	movq	16(%rcx), %rax
	jne	.L3328
	jmp	.L3327
.L3107:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3112:
	movq	8(%rbx), %rax
	movl	$0, 24(%rax)
	movb	$1, 28(%rax)
	movl	32(%rbx), %r15d
	cmpl	$127, %r15d
	ja	.L3353
	cmpl	$92, %r15d
	je	.L3273
	movslq	%r15d, %rax
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rdx
	movq	8(%rbx), %r13
	movzbl	(%rdx,%rax), %eax
	sarl	%eax
	movb	%al, -81(%rbp)
	movslq	24(%r13), %rax
	cmpl	16(%r13), %eax
	jge	.L3402
.L3274:
	movq	8(%r13), %rdx
	movb	%r15b, (%rdx,%rax)
	leaq	-81(%rbp), %rax
	leaq	-80(%rbp), %r15
	addl	$1, 24(%r13)
	movq	24(%rbx), %r13
	movq	%rbx, -80(%rbp)
	movq	%rax, -72(%rbp)
.L3277:
	movq	24(%r13), %rsi
	movq	16(%r13), %rdi
	movq	%r15, %rdx
	call	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal20Utf16CharacterStream12AdvanceUntilIZNS6_7Scanner28ScanIdentifierOrKeywordInnerEvEUliE_EEiT_EUltE_EEESB_SB_SB_T0_St26random_access_iterator_tag
	cmpq	%rax, 24(%r13)
	jne	.L3275
	cmpb	$0, 48(%r13)
	movq	%rax, 16(%r13)
	jne	.L3276
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L3277
	movq	16(%r13), %rax
.L3276:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	$-1, %eax
.L3278:
	movl	%eax, 32(%rbx)
	movzbl	-81(%rbp), %eax
	movl	%eax, %edx
	andl	$2, %edx
	testb	$16, %al
	jne	.L3279
	testb	%dl, %dl
	jne	.L3133
	movq	8(%rbx), %rdx
	movl	24(%rdx), %eax
	leal	-2(%rax), %ecx
	cmpl	$8, %ecx
	ja	.L3133
	movq	8(%rdx), %rsi
	leaq	_ZZN2v88internal18PerfectKeywordHash4HashEPKciE11asso_values(%rip), %rdx
	movzbl	1(%rsi), %ecx
	movzbl	(%rsi), %edi
	movzbl	(%rdx,%rcx), %ecx
	movzbl	(%rdx,%rdi), %edx
	addl	%eax, %ecx
	addl	%edx, %ecx
	leaq	_ZN2v88internalL26kPerfectKeywordLengthTableE(%rip), %rdx
	andl	$63, %ecx
	movzbl	(%rdx,%rcx), %edx
	cmpl	%edx, %eax
	jne	.L3133
	movq	%rcx, %rax
	leaq	_ZN2v88internalL24kPerfectKeywordHashTableE(%rip), %r8
	salq	$4, %rax
	movq	(%r8,%rax), %rdi
	xorl	%eax, %eax
	jmp	.L3282
	.p2align 4,,10
	.p2align 3
.L3403:
	addq	$1, %rax
	cmpb	-1(%rsi,%rax), %dl
	jne	.L3133
.L3282:
	movzbl	(%rdi,%rax), %edx
	testb	%dl, %dl
	jne	.L3403
	salq	$4, %rcx
	movzbl	8(%r8,%rcx), %r14d
	jmp	.L3133
	.p2align 4,,10
	.p2align 3
.L3113:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner10ScanStringEv
	movl	%eax, %r14d
	jmp	.L3133
.L3114:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner10ScanNumberEb
	movl	%eax, %r14d
	jmp	.L3133
.L3115:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3147
	movzwl	(%rax), %edx
.L3148:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	je	.L3404
	cmpl	$62, %edx
	jne	.L3133
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3153
	movzwl	(%rax), %edx
.L3154:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	je	.L3405
	movl	$38, %r14d
	cmpl	$62, %edx
	jne	.L3133
	movl	$39, %ecx
	movl	$23, %edx
	movl	$61, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectEiNS0_5Token5ValueES3_
	movl	%eax, %r14d
	jmp	.L3133
.L3116:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3142
	movzwl	(%rax), %ecx
.L3143:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%ecx, 32(%rbx)
	cmpl	$61, %ecx
	je	.L3406
	cmpl	$60, %ecx
	je	.L3407
	cmpl	$33, %ecx
	jne	.L3133
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner15ScanHtmlCommentEv
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	movl	%eax, %r14d
.L3146:
	cmpb	$111, %r14b
	jne	.L3133
.L3211:
	movq	24(%rbx), %rdi
	movq	32(%rdi), %rcx
	movq	16(%rdi), %rax
	movq	8(%rdi), %r8
	jmp	.L3271
.L3124:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3234
.L3388:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	jne	.L3133
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3238
.L3240:
	movzwl	(%rax), %r14d
.L3239:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%r14d, 32(%rbx)
	movl	$19, %r14d
	jmp	.L3133
.L3122:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3186
	movzwl	(%rax), %edx
.L3187:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%edx, 32(%rbx)
	cmpl	$42, %edx
	je	.L3408
	cmpl	$61, %edx
	jne	.L3133
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3189
.L3191:
	movzwl	(%rax), %r14d
.L3190:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%r14d, 32(%rbx)
	movl	$24, %r14d
	jmp	.L3133
.L3123:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3216
	movzwl	(%rax), %edx
.L3217:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%edx, 32(%rbx)
	cmpl	$38, %edx
	je	.L3409
	cmpl	$61, %edx
	jne	.L3133
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3222
.L3224:
	movzwl	(%rax), %r14d
.L3223:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%r14d, 32(%rbx)
	movl	$20, %r14d
	jmp	.L3133
.L3117:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3162
	movzwl	(%rax), %edx
.L3163:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	jne	.L3133
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3164
.L3386:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movl	$55, %r14d
	movq	%rax, 16(%r13)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	jne	.L3133
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3168
.L3170:
	movzwl	(%rax), %r14d
.L3169:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%r14d, 32(%rbx)
	movl	$56, %r14d
	jmp	.L3133
.L3119:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3171
	movzwl	(%rax), %edx
.L3172:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%edx, 32(%rbx)
	cmpl	$43, %edx
	je	.L3410
	cmpl	$61, %edx
	jne	.L3133
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3177
.L3179:
	movzwl	(%rax), %r14d
.L3178:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%r14d, 32(%rbx)
	movl	$28, %r14d
	jmp	.L3133
.L3125:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3225
	movzwl	(%rax), %edx
.L3226:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%edx, 32(%rbx)
	cmpl	$124, %edx
	je	.L3411
	cmpl	$61, %edx
	jne	.L3133
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3231
.L3233:
	movzwl	(%rax), %r14d
.L3232:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%r14d, 32(%rbx)
	movl	$18, %r14d
	jmp	.L3133
.L3127:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3134
	movzwl	(%rax), %edx
.L3135:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	cmpb	$0, 281(%rbx)
	movl	%edx, 32(%rbx)
	je	.L3136
	cmpl	$46, %edx
	je	.L3412
.L3136:
	cmpb	$0, 282(%rbx)
	je	.L3133
	cmpl	$63, %edx
	jne	.L3133
	movl	$31, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movl	%eax, %r14d
	jmp	.L3133
	.p2align 4,,10
	.p2align 3
.L3120:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3192
.L3387:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	jne	.L3133
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3196
.L3198:
	movzwl	(%rax), %r14d
.L3197:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%r14d, 32(%rbx)
	movl	$26, %r14d
	jmp	.L3133
.L3121:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3199
	movzwl	(%rax), %ecx
.L3200:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%ecx, 32(%rbx)
	cmpl	$47, %ecx
	je	.L3413
	cmpl	$42, %ecx
	je	.L3414
	cmpl	$61, %ecx
	jne	.L3133
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3213
.L3215:
	movzwl	(%rax), %r14d
.L3214:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%r14d, 32(%rbx)
	movl	$25, %r14d
	jmp	.L3133
.L3126:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3156
	movzwl	(%rax), %edx
.L3157:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%edx, 32(%rbx)
	cmpl	$61, %edx
	je	.L3415
	cmpl	$62, %edx
	jne	.L3133
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3159
.L3161:
	movzwl	(%rax), %r14d
.L3160:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%r14d, 32(%rbx)
	movl	$15, %r14d
	jmp	.L3133
.L3108:
	movq	24(%rbx), %rax
	movq	16(%rax), %r14
	subq	8(%rax), %r14
	sarq	%r14
	addq	32(%rax), %r14
	jmp	.L3270
	.p2align 4,,10
	.p2align 3
.L3260:
	testb	$8, (%rsi,%r13)
	je	.L3265
.L3264:
	movq	8(%rbx), %rax
	cmpb	$0, 76(%rax)
	jne	.L3262
	movl	32(%rbx), %ecx
	cmpl	$10, %ecx
	je	.L3266
	cmpl	$13, %ecx
	je	.L3266
	subl	$8232, %ecx
	cmpl	$1, %ecx
	jbe	.L3266
	.p2align 4,,10
	.p2align 3
.L3262:
	movq	24(%rbx), %rcx
	movq	16(%rcx), %rax
	cmpq	24(%rcx), %rax
	jnb	.L3267
.L3269:
	movzwl	(%rax), %r13d
.L3268:
	addq	$2, %rax
	movq	%rax, 16(%rcx)
	movl	%r13d, 32(%rbx)
.L3270:
	cmpl	$127, %r13d
	jbe	.L3260
	movl	%r13d, %edi
	call	_ZN2v88internal16IsWhiteSpaceSlowEi@PLT
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	testb	%al, %al
	jne	.L3264
	subl	$8232, %r13d
	cmpl	$1, %r13d
	ja	.L3265
	jmp	.L3264
.L3111:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %rcx
	movq	%rcx, %rax
	subq	8(%rdi), %rax
	sarq	%rax
	addq	32(%rdi), %rax
	cmpl	$1, %eax
	jne	.L3259
	cmpq	24(%rdi), %rcx
	jnb	.L3257
	movzwl	(%rcx), %eax
.L3258:
	cmpl	$33, %eax
	je	.L3392
.L3259:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner15ScanPrivateNameEv
	movl	%eax, %r14d
	jmp	.L3133
.L3129:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3254
	movzwl	(%rax), %edx
.L3255:
	addq	$2, %rax
	movq	%rbx, %rdi
	movq	%rax, 16(%r13)
	movl	%edx, 32(%rbx)
	call	_ZN2v88internal7Scanner16ScanTemplateSpanEv
	movl	%eax, %r14d
	jmp	.L3133
.L3128:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3241
.L3389:
	movzwl	(%rax), %edx
	addq	$2, %rax
	movq	%rax, 16(%r13)
	leal	-48(%rdx), %eax
	movl	%edx, 32(%rbx)
	cmpl	$9, %eax
	jbe	.L3416
	cmpl	$46, %edx
	jne	.L3133
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3247
	cmpw	$46, (%rax)
	jne	.L3133
.L3251:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movq	24(%rbx), %r13
	movl	%edx, 32(%rbx)
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3252
	movzwl	(%rax), %edx
.L3253:
	addq	$2, %rax
	movl	$10, %r14d
	movq	%rax, 16(%r13)
	movl	%edx, 32(%rbx)
	jmp	.L3133
	.p2align 4,,10
	.p2align 3
.L3118:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3180
	movzwl	(%rax), %ecx
.L3181:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%ecx, 32(%rbx)
	cmpl	$45, %ecx
	jne	.L3182
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3183
	movzwl	(%rax), %ecx
.L3184:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%ecx, 32(%rbx)
	cmpl	$62, %ecx
	jne	.L3341
	movq	8(%rbx), %rax
	cmpb	$0, 76(%rax)
	jne	.L3417
.L3341:
	movl	$52, %r14d
	jmp	.L3133
	.p2align 4,,10
	.p2align 3
.L3266:
	movb	$1, 76(%rax)
	jmp	.L3262
	.p2align 4,,10
	.p2align 3
.L3102:
	cmpb	$0, 48(%r12)
	movl	$-1, %r13d
	jne	.L3103
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r12), %rax
	jne	.L3104
	jmp	.L3103
	.p2align 4,,10
	.p2align 3
.L3267:
	cmpb	$0, 48(%rcx)
	movq	$-1, %r13
	jne	.L3268
	movq	(%rcx), %rax
	movq	%rcx, -104(%rbp)
	movq	%rcx, %rdi
	call	*40(%rax)
	movq	-104(%rbp), %rcx
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	testb	%al, %al
	movq	16(%rcx), %rax
	jne	.L3269
	jmp	.L3268
	.p2align 4,,10
	.p2align 3
.L3400:
	movq	16(%rcx), %rax
	cmpq	24(%rcx), %rax
	jnb	.L3294
.L3297:
	movzwl	(%rax), %r8d
	addq	$2, %rax
	movq	%rax, 16(%rcx)
	movl	%r8d, %eax
	andl	$64512, %eax
	cmpl	$56320, %eax
	je	.L3418
.L3298:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	8(%rdi), %rcx
	cmpq	%rcx, %rax
	jbe	.L3301
	subq	$2, %rax
	movq	%rax, 16(%rdi)
	movslq	32(%rbx), %r13
	movq	24(%rbx), %rcx
	jmp	.L3293
	.p2align 4,,10
	.p2align 3
.L3283:
	movl	$109, %r14d
	jmp	.L3133
.L3413:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3202
	movzwl	(%rax), %ecx
	cmpl	$35, %ecx
	je	.L3203
	cmpl	$64, %ecx
	jne	.L3392
.L3203:
	movzwl	(%rax), %eax
.L3207:
	addq	$2, 16(%r13)
	movq	24(%rbx), %r13
	movl	%eax, 32(%rbx)
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3208
	movzwl	(%rax), %ecx
.L3209:
	addq	$2, %rax
	movq	%rbx, %rdi
	movq	%rax, 16(%r13)
	movl	%ecx, 32(%rbx)
	call	_ZN2v88internal7Scanner26TryToParseSourceURLCommentEv
	movl	32(%rbx), %eax
	xorl	%edi, %edi
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	leal	1(%rax), %ecx
	cmpl	$14, %ecx
	ja	.L3210
	movl	$18433, %edi
	shrq	%cl, %rdi
	andl	$1, %edi
.L3210:
	subl	$8232, %eax
	cmpl	$1, %eax
	jbe	.L3211
	testb	%dil, %dil
	jne	.L3211
	.p2align 4,,10
	.p2align 3
.L3392:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner21SkipSingleLineCommentEv
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	movl	%eax, %r14d
	jmp	.L3146
	.p2align 4,,10
	.p2align 3
.L3417:
	cmpb	$0, 283(%rbx)
	je	.L3392
	movl	348(%rbx), %eax
	movl	$109, %r14d
	testl	%eax, %eax
	jne	.L3133
	movq	24(%rbx), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	%rax
	addq	32(%rdx), %rax
	movl	$275, 348(%rbx)
	leal	-1(%rax), %edx
	movl	%eax, 356(%rbx)
	movl	%edx, 352(%rbx)
	jmp	.L3133
	.p2align 4,,10
	.p2align 3
.L3416:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner10ScanNumberEb
	movl	%eax, %r14d
	jmp	.L3133
	.p2align 4,,10
	.p2align 3
.L3418:
	movl	32(%rbx), %edi
	andl	$1023, %r8d
	sall	$10, %edi
	andl	$1047552, %edi
	orl	%r8d, %edi
	addl	$65536, %edi
	movl	%edi, 32(%rbx)
	call	_ZN2v88internal21IsIdentifierStartSlowEi@PLT
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	testb	%al, %al
	jne	.L3299
.L3382:
	movslq	32(%rbx), %r13
	movq	24(%rbx), %rcx
	jmp	.L3293
	.p2align 4,,10
	.p2align 3
.L3402:
	leaq	8(%r13), %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r13), %rax
	jmp	.L3274
.L3408:
	movl	$43, %ecx
	movl	$27, %edx
	movl	$61, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectEiNS0_5Token5ValueES3_
	movl	%eax, %r14d
	jmp	.L3133
.L3410:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3174
.L3176:
	movzwl	(%rax), %r14d
.L3175:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%r14d, 32(%rbx)
	movl	$51, %r14d
	jmp	.L3133
.L3415:
	movl	$53, %ecx
	movl	$54, %edx
	movl	$61, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectEiNS0_5Token5ValueES3_
	movl	%eax, %r14d
	jmp	.L3133
.L3404:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3150
.L3152:
	movzwl	(%rax), %r14d
.L3151:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%r14d, 32(%rbx)
	movl	$60, %r14d
	jmp	.L3133
.L3409:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3219
.L3221:
	movzwl	(%rax), %r14d
.L3220:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%r14d, 32(%rbx)
	movl	$33, %r14d
	jmp	.L3133
.L3411:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3228
.L3230:
	movzwl	(%rax), %r14d
.L3229:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	%r14d, 32(%rbx)
	movl	$32, %r14d
	jmp	.L3133
.L3147:
	movq	%r13, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r13), %rax
	jmp	.L3148
.L3186:
	movq	%r13, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r13), %rax
	jmp	.L3187
.L3171:
	movq	%r13, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r13), %rax
	jmp	.L3172
.L3234:
	cmpb	$0, 48(%r13)
	jne	.L3397
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L3395
	movq	16(%r13), %rax
	jmp	.L3388
.L3397:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	$-1, 32(%rbx)
	jmp	.L3133
.L3162:
	movq	%r13, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r13), %rax
	jmp	.L3163
.L3192:
	cmpb	$0, 48(%r13)
	jne	.L3397
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L3395
	movq	16(%r13), %rax
	jmp	.L3387
.L3134:
	movq	%r13, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r13), %rax
	jmp	.L3135
.L3156:
	movq	%r13, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r13), %rax
	jmp	.L3157
.L3199:
	movq	%r13, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %ecx
	movq	16(%r13), %rax
	jmp	.L3200
.L3241:
	cmpb	$0, 48(%r13)
	jne	.L3397
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L3395
	movq	16(%r13), %rax
	jmp	.L3389
.L3130:
	cmpb	$0, 48(%r13)
	movl	$-1, %r15d
	jne	.L3131
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r13), %rax
	jne	.L3132
	jmp	.L3131
.L3180:
	movq	%r13, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %ecx
	movq	16(%r13), %rax
	jmp	.L3181
.L3353:
	xorl	%esi, %esi
	movl	$1, %edx
.L3393:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner32ScanIdentifierOrKeywordInnerSlowEbb
	movl	%eax, %r14d
	jmp	.L3133
.L3216:
	movq	%r13, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r13), %rax
	jmp	.L3217
.L3142:
	movq	%r13, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %ecx
	movq	16(%r13), %rax
	jmp	.L3143
.L3254:
	movq	%r13, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r13), %rax
	jmp	.L3255
.L3225:
	movq	%r13, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r13), %rax
	jmp	.L3226
.L3257:
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	jmp	.L3258
.L3183:
	movq	%r13, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %ecx
	movq	16(%r13), %rax
	jmp	.L3184
.L3414:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner20SkipMultiLineCommentEv
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	movl	%eax, %r14d
	jmp	.L3146
.L3273:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner27ScanIdentifierUnicodeEscapeEv
	movslq	%eax, %r13
	cmpl	$92, %r13d
	je	.L3283
	cmpl	$127, %r13d
	ja	.L3419
	movslq	%r13d, %rax
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
	andl	$1, %eax
.L3285:
	testb	%al, %al
	je	.L3283
	movq	8(%rbx), %r14
	cmpb	$0, 28(%r14)
	leaq	8(%r14), %r15
	je	.L3286
	cmpl	$255, %r13d
	jle	.L3420
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
.L3286:
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L3289:
	cmpl	$127, %r13d
	ja	.L3354
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rax
	movl	$1, %esi
	movzbl	(%rax,%r13), %edx
	shrb	%dl
	xorl	$1, %edx
	andl	$1, %edx
	jmp	.L3393
.L3279:
	testb	%dl, %dl
	sete	%dl
	xorl	%esi, %esi
	movzbl	%dl, %edx
	jmp	.L3393
.L3275:
	leaq	2(%rax), %rdx
	movq	%rdx, 16(%r13)
	movzwl	(%rax), %eax
	jmp	.L3278
.L3294:
	cmpb	$0, 48(%rcx)
	jne	.L3296
	movq	(%rcx), %rax
	movq	%rcx, -104(%rbp)
	movq	%rcx, %rdi
	call	*40(%rax)
	movq	-104(%rbp), %rcx
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	testb	%al, %al
	movq	16(%rcx), %rax
	jne	.L3297
.L3296:
	addq	$2, %rax
	movq	%rax, 16(%rcx)
	jmp	.L3298
.L3405:
	movl	$22, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movl	%eax, %r14d
	jmp	.L3133
.L3395:
	addq	$2, 16(%r13)
	movl	$-1, 32(%rbx)
	jmp	.L3133
.L3299:
	movq	8(%rbx), %rax
	xorl	%r13d, %r13d
	movl	$0, 24(%rax)
	movb	$1, 28(%rax)
	movl	32(%rbx), %r14d
	movl	$1, %eax
	cmpl	$127, %r14d
	ja	.L3292
	cmpl	$92, %r14d
	je	.L3305
	movslq	%r14d, %rax
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rdx
	movq	8(%rbx), %r13
	movzbl	(%rdx,%rax), %eax
	sarl	%eax
	movb	%al, -81(%rbp)
	movslq	24(%r13), %rax
	cmpl	16(%r13), %eax
	jge	.L3421
.L3306:
	movq	8(%r13), %rdx
	leaq	-80(%rbp), %r15
	movb	%r14b, (%rdx,%rax)
	leaq	-81(%rbp), %rax
	addl	$1, 24(%r13)
	movq	24(%rbx), %r13
	movq	%rbx, -80(%rbp)
	movq	%rax, -72(%rbp)
.L3309:
	movq	24(%r13), %rsi
	movq	16(%r13), %rdi
	movq	%r15, %rdx
	call	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal20Utf16CharacterStream12AdvanceUntilIZNS6_7Scanner28ScanIdentifierOrKeywordInnerEvEUliE_EEiT_EUltE_EEESB_SB_SB_T0_St26random_access_iterator_tag
	cmpq	%rax, 24(%r13)
	jne	.L3307
	cmpb	$0, 48(%r13)
	movq	%rax, 16(%r13)
	jne	.L3308
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L3309
	movq	16(%r13), %rax
.L3308:
	addq	$2, %rax
	movq	%rax, 16(%r13)
	movl	$-1, %eax
.L3310:
	movzbl	-81(%rbp), %edx
	movl	%eax, 32(%rbx)
	movl	%edx, %eax
	andl	$2, %eax
	andl	$16, %edx
	jne	.L3311
	movl	$92, %r14d
	testb	%al, %al
	jne	.L3133
	movq	8(%rbx), %rax
	movl	24(%rax), %esi
	leal	-2(%rsi), %edx
	cmpl	$8, %edx
	ja	.L3133
	movq	8(%rax), %rdi
	call	_ZN2v88internal18PerfectKeywordHash8GetTokenEPKci.part.0
	movl	%eax, %r14d
	jmp	.L3133
.L3301:
	movq	32(%rdi), %r8
	subq	%rcx, %rax
	movq	%rcx, 16(%rdi)
	sarq	%rax
	cmpb	$0, 48(%rdi)
	leaq	-1(%r8,%rax), %rax
	movq	%rax, 32(%rdi)
	jne	.L3382
	movq	(%rdi), %rax
	call	*40(%rax)
	movslq	32(%rbx), %r13
	movq	24(%rbx), %rcx
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rsi
	leaq	_ZN2v88internalL15one_char_tokensE(%rip), %rdx
	jmp	.L3293
.L3164:
	cmpb	$0, 48(%r13)
	je	.L3166
	addq	$2, %rax
	movl	$55, %r14d
	movq	%rax, 16(%r13)
	movl	$-1, 32(%rbx)
	jmp	.L3133
.L3401:
	cmpb	$1, 48(%rcx)
	sbbl	%r14d, %r14d
	andl	$-95, %r14d
	addl	$109, %r14d
	jmp	.L3133
.L3153:
	movq	%r13, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r13), %rax
	jmp	.L3154
.L3159:
	cmpb	$0, 48(%r13)
	movl	$-1, %r14d
	jne	.L3160
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r13), %rax
	jne	.L3161
	jmp	.L3160
.L3177:
	cmpb	$0, 48(%r13)
	movl	$-1, %r14d
	jne	.L3178
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r13), %rax
	jne	.L3179
	jmp	.L3178
.L3189:
	cmpb	$0, 48(%r13)
	movl	$-1, %r14d
	jne	.L3190
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r13), %rax
	jne	.L3191
	jmp	.L3190
.L3222:
	cmpb	$0, 48(%r13)
	movl	$-1, %r14d
	jne	.L3223
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r13), %rax
	jne	.L3224
	jmp	.L3223
.L3231:
	cmpb	$0, 48(%r13)
	movl	$-1, %r14d
	jne	.L3232
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r13), %rax
	jne	.L3233
	jmp	.L3232
.L3196:
	cmpb	$0, 48(%r13)
	movl	$-1, %r14d
	jne	.L3197
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r13), %rax
	jne	.L3198
	jmp	.L3197
.L3213:
	cmpb	$0, 48(%r13)
	movl	$-1, %r14d
	jne	.L3214
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r13), %rax
	jne	.L3215
	jmp	.L3214
.L3238:
	cmpb	$0, 48(%r13)
	movl	$-1, %r14d
	jne	.L3239
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r13), %rax
	jne	.L3240
	jmp	.L3239
.L3247:
	movq	%r13, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	cmpl	$46, %eax
	jne	.L3133
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jb	.L3422
	movq	%r13, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r13), %rax
	jmp	.L3251
.L3166:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L3167
	addq	$2, 16(%r13)
	movl	$55, %r14d
	movl	$-1, 32(%rbx)
	jmp	.L3133
.L3419:
	movl	%r13d, %edi
	call	_ZN2v88internal21IsIdentifierStartSlowEi@PLT
	jmp	.L3285
.L3354:
	movl	$1, %esi
	xorl	%edx, %edx
	jmp	.L3393
.L3167:
	movq	16(%r13), %rax
	jmp	.L3386
.L3219:
	orl	$-1, %r14d
	cmpb	$0, 48(%r13)
	jne	.L3220
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r13), %rax
	jne	.L3221
	jmp	.L3220
.L3174:
	orl	$-1, %r14d
	cmpb	$0, 48(%r13)
	jne	.L3175
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r13), %rax
	jne	.L3176
	jmp	.L3175
.L3228:
	orl	$-1, %r14d
	cmpb	$0, 48(%r13)
	jne	.L3229
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r13), %rax
	jne	.L3230
	jmp	.L3229
.L3202:
	movq	%r13, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	cmpl	$35, %eax
	je	.L3360
	cmpl	$64, %eax
	jne	.L3392
.L3360:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jb	.L3203
	movq	%r13, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	jmp	.L3207
.L3182:
	cmpl	$61, %ecx
	jne	.L3133
	movl	$29, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movl	%eax, %r14d
	jmp	.L3133
.L3150:
	orl	$-1, %r14d
	cmpb	$0, 48(%r13)
	jne	.L3151
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r13), %rax
	jne	.L3152
	jmp	.L3151
.L3406:
	movl	$59, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectENS0_5Token5ValueE
	movl	%eax, %r14d
	jmp	.L3133
.L3407:
	movl	$37, %ecx
	movl	$21, %edx
	movl	$61, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner6SelectEiNS0_5Token5ValueES3_
	movl	%eax, %r14d
	jmp	.L3133
.L3168:
	orl	$-1, %r14d
	cmpb	$0, 48(%r13)
	jne	.L3169
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	movq	16(%r13), %rax
	jne	.L3170
	jmp	.L3169
.L3421:
	leaq	8(%r13), %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	movslq	24(%r13), %rax
	jmp	.L3306
.L3305:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner27ScanIdentifierUnicodeEscapeEv
	movl	%eax, %r15d
	cmpl	$92, %eax
	je	.L3283
	cmpl	$127, %eax
	ja	.L3423
	cltq
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %rdx
	movzbl	(%rdx,%rax), %r13d
	andl	$1, %r13d
.L3314:
	testb	%r13b, %r13b
	je	.L3283
	movq	8(%rbx), %r14
	cmpb	$0, 28(%r14)
	leaq	8(%r14), %rdi
	je	.L3315
	cmpl	$255, %r15d
	jle	.L3424
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv@PLT
	movq	-104(%rbp), %rdi
.L3315:
	movl	%r15d, %esi
	call	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi@PLT
.L3318:
	xorl	%eax, %eax
	cmpl	$127, %r15d
	ja	.L3292
	movslq	%r15d, %rax
	leaq	_ZN2v88internalL20character_scan_flagsE(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
	shrb	%al
	xorl	$1, %eax
	andl	$1, %eax
.L3292:
	movzbl	%al, %edx
	movzbl	%r13b, %esi
	jmp	.L3393
.L3420:
	movl	16(%r14), %eax
	cmpl	%eax, 24(%r14)
	jge	.L3425
.L3288:
	movslq	24(%r14), %rax
	movq	8(%r14), %rdx
	movb	%r13b, (%rdx,%rax)
	addl	$1, 24(%r14)
	jmp	.L3289
.L3412:
	movq	24(%rbx), %r13
	movq	16(%r13), %rax
	cmpq	24(%r13), %rax
	jnb	.L3137
	movzwl	(%rax), %eax
.L3138:
	addq	$2, 16(%r13)
	movl	%eax, 32(%rbx)
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L3139
	movl	$4, %r14d
	jmp	.L3133
.L3399:
	call	__stack_chk_fail@PLT
.L3208:
	movq	%r13, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %ecx
	movq	16(%r13), %rax
	jmp	.L3209
.L3252:
	movq	%r13, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	movl	%eax, %edx
	movq	16(%r13), %rax
	jmp	.L3253
.L3307:
	leaq	2(%rax), %rdx
	movq	%rdx, 16(%r13)
	movzwl	(%rax), %eax
	jmp	.L3310
.L3311:
	testb	%al, %al
	sete	%al
	xorl	%r13d, %r13d
	jmp	.L3292
.L3425:
	movq	%r15, %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	jmp	.L3288
.L3139:
	movq	24(%rbx), %rdi
	call	_ZN2v88internal20Utf16CharacterStream4BackEv
	movl	$46, 32(%rbx)
	jmp	.L3133
.L3422:
	movzwl	(%rax), %edx
	jmp	.L3251
.L3137:
	movq	%r13, %rdi
	call	_ZN2v88internal20Utf16CharacterStream4PeekEv.part.0
	jmp	.L3138
.L3423:
	movl	%eax, %edi
	call	_ZN2v88internal21IsIdentifierStartSlowEi@PLT
	movl	%eax, %r13d
	jmp	.L3314
.L3424:
	movl	16(%r14), %eax
	cmpl	%eax, 24(%r14)
	jge	.L3426
.L3317:
	movslq	24(%r14), %rax
	movq	8(%r14), %rdx
	movb	%r15b, (%rdx,%rax)
	addl	$1, 24(%r14)
	jmp	.L3318
.L3426:
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv@PLT
	jmp	.L3317
	.cfi_endproc
.LFE18141:
	.size	_ZN2v88internal7Scanner11SeekForwardEi, .-_ZN2v88internal7Scanner11SeekForwardEi
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal7Scanner13BookmarkScope11kNoBookmarkE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal7Scanner13BookmarkScope11kNoBookmarkE, @function
_GLOBAL__sub_I__ZN2v88internal7Scanner13BookmarkScope11kNoBookmarkE:
.LFB21947:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21947:
	.size	_GLOBAL__sub_I__ZN2v88internal7Scanner13BookmarkScope11kNoBookmarkE, .-_GLOBAL__sub_I__ZN2v88internal7Scanner13BookmarkScope11kNoBookmarkE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal7Scanner13BookmarkScope11kNoBookmarkE
	.globl	_ZN2v88internal7Scanner13BookmarkScope19kBookmarkWasAppliedE
	.section	.rodata._ZN2v88internal7Scanner13BookmarkScope19kBookmarkWasAppliedE,"a"
	.align 8
	.type	_ZN2v88internal7Scanner13BookmarkScope19kBookmarkWasAppliedE, @object
	.size	_ZN2v88internal7Scanner13BookmarkScope19kBookmarkWasAppliedE, 8
_ZN2v88internal7Scanner13BookmarkScope19kBookmarkWasAppliedE:
	.quad	-1
	.globl	_ZN2v88internal7Scanner13BookmarkScope11kNoBookmarkE
	.section	.rodata._ZN2v88internal7Scanner13BookmarkScope11kNoBookmarkE,"a"
	.align 8
	.type	_ZN2v88internal7Scanner13BookmarkScope11kNoBookmarkE, @object
	.size	_ZN2v88internal7Scanner13BookmarkScope11kNoBookmarkE, 8
_ZN2v88internal7Scanner13BookmarkScope11kNoBookmarkE:
	.quad	-2
	.section	.rodata._ZN2v88internalL20character_scan_flagsE,"a"
	.align 32
	.type	_ZN2v88internalL20character_scan_flagsE, @object
	.size	_ZN2v88internalL20character_scan_flagsE, 128
_ZN2v88internalL20character_scan_flagsE:
	.string	"\005\005\005\005\005\005\005\005\005\005-\005\005-\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\r\005\006\005\005\r\005\005%\005\005\005\005\005\006\006\006\006\006\006\006\006\006\006\005\005\005\005\005\005\005\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\005\035\005\005\006\005"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\004"
	.string	"\006\004"
	.string	"\004"
	.string	"\004"
	.string	"\006"
	.string	""
	.string	""
	.string	"\004"
	.string	""
	.string	"\004"
	.ascii	"\006\005\005\005\005\005"
	.section	.rodata._ZN2v88internalL15one_char_tokensE,"a"
	.align 32
	.type	_ZN2v88internalL15one_char_tokensE, @object
	.size	_ZN2v88internalL15one_char_tokensE, 128
_ZN2v88internalL15one_char_tokensE:
	.byte	109
	.byte	109
	.byte	109
	.byte	109
	.byte	109
	.byte	109
	.byte	109
	.byte	109
	.byte	109
	.byte	111
	.byte	111
	.byte	111
	.byte	111
	.byte	111
	.byte	109
	.byte	109
	.byte	109
	.byte	109
	.byte	109
	.byte	109
	.byte	109
	.byte	109
	.byte	109
	.byte	109
	.byte	109
	.byte	109
	.byte	109
	.byte	109
	.byte	109
	.byte	109
	.byte	109
	.byte	109
	.byte	111
	.byte	46
	.byte	90
	.byte	108
	.byte	92
	.byte	42
	.byte	36
	.byte	90
	.byte	5
	.byte	6
	.byte	40
	.byte	44
	.byte	30
	.byte	45
	.byte	2
	.byte	41
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	9
	.byte	12
	.byte	57
	.byte	17
	.byte	58
	.byte	11
	.byte	109
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	3
	.byte	92
	.byte	7
	.byte	35
	.byte	92
	.byte	0
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	92
	.byte	8
	.byte	34
	.byte	13
	.byte	47
	.byte	109
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC4:
	.string	""
.LC5:
	.string	"in"
.LC6:
	.string	"new"
.LC7:
	.string	"enum"
.LC8:
	.string	"do"
.LC9:
	.string	"delete"
.LC10:
	.string	"default"
.LC11:
	.string	"debugger"
.LC12:
	.string	"interface"
.LC13:
	.string	"instanceof"
.LC14:
	.string	"if"
.LC15:
	.string	"get"
.LC16:
	.string	"set"
.LC17:
	.string	"const"
.LC18:
	.string	"for"
.LC19:
	.string	"finally"
.LC20:
	.string	"continue"
.LC21:
	.string	"case"
.LC22:
	.string	"catch"
.LC23:
	.string	"null"
.LC24:
	.string	"package"
.LC25:
	.string	"false"
.LC26:
	.string	"async"
.LC27:
	.string	"break"
.LC28:
	.string	"return"
.LC29:
	.string	"this"
.LC30:
	.string	"throw"
.LC31:
	.string	"public"
.LC32:
	.string	"static"
.LC33:
	.string	"with"
.LC34:
	.string	"super"
.LC35:
	.string	"private"
.LC36:
	.string	"function"
.LC37:
	.string	"protected"
.LC38:
	.string	"try"
.LC39:
	.string	"true"
.LC40:
	.string	"let"
.LC41:
	.string	"else"
.LC42:
	.string	"await"
.LC43:
	.string	"while"
.LC44:
	.string	"yield"
.LC45:
	.string	"switch"
.LC46:
	.string	"export"
.LC47:
	.string	"extends"
.LC48:
	.string	"class"
.LC49:
	.string	"void"
.LC50:
	.string	"import"
.LC51:
	.string	"var"
.LC52:
	.string	"implements"
.LC53:
	.string	"typeof"
	.section	.data.rel.ro.local._ZN2v88internalL24kPerfectKeywordHashTableE,"aw"
	.align 32
	.type	_ZN2v88internalL24kPerfectKeywordHashTableE, @object
	.size	_ZN2v88internalL24kPerfectKeywordHashTableE, 1024
_ZN2v88internalL24kPerfectKeywordHashTableE:
	.quad	.LC4
	.byte	92
	.zero	7
	.quad	.LC4
	.byte	92
	.zero	7
	.quad	.LC5
	.byte	62
	.zero	7
	.quad	.LC6
	.byte	75
	.zero	7
	.quad	.LC7
	.byte	102
	.zero	7
	.quad	.LC8
	.byte	69
	.zero	7
	.quad	.LC9
	.byte	48
	.zero	7
	.quad	.LC10
	.byte	68
	.zero	7
	.quad	.LC11
	.byte	67
	.zero	7
	.quad	.LC12
	.byte	100
	.zero	7
	.quad	.LC13
	.byte	61
	.zero	7
	.quad	.LC14
	.byte	74
	.zero	7
	.quad	.LC15
	.byte	93
	.zero	7
	.quad	.LC16
	.byte	94
	.zero	7
	.quad	.LC17
	.byte	104
	.zero	7
	.quad	.LC18
	.byte	72
	.zero	7
	.quad	.LC19
	.byte	71
	.zero	7
	.quad	.LC20
	.byte	66
	.zero	7
	.quad	.LC21
	.byte	64
	.zero	7
	.quad	.LC22
	.byte	65
	.zero	7
	.quad	.LC23
	.byte	84
	.zero	7
	.quad	.LC24
	.byte	100
	.zero	7
	.quad	.LC25
	.byte	86
	.zero	7
	.quad	.LC26
	.byte	95
	.zero	7
	.quad	.LC27
	.byte	63
	.zero	7
	.quad	.LC28
	.byte	76
	.zero	7
	.quad	.LC29
	.byte	83
	.zero	7
	.quad	.LC30
	.byte	78
	.zero	7
	.quad	.LC31
	.byte	100
	.zero	7
	.quad	.LC32
	.byte	99
	.zero	7
	.quad	.LC33
	.byte	82
	.zero	7
	.quad	.LC34
	.byte	91
	.zero	7
	.quad	.LC35
	.byte	100
	.zero	7
	.quad	.LC36
	.byte	73
	.zero	7
	.quad	.LC37
	.byte	100
	.zero	7
	.quad	.LC38
	.byte	79
	.zero	7
	.quad	.LC39
	.byte	85
	.zero	7
	.quad	.LC40
	.byte	98
	.zero	7
	.quad	.LC41
	.byte	70
	.zero	7
	.quad	.LC42
	.byte	96
	.zero	7
	.quad	.LC43
	.byte	81
	.zero	7
	.quad	.LC44
	.byte	97
	.zero	7
	.quad	.LC45
	.byte	77
	.zero	7
	.quad	.LC46
	.byte	105
	.zero	7
	.quad	.LC47
	.byte	106
	.zero	7
	.quad	.LC48
	.byte	103
	.zero	7
	.quad	.LC49
	.byte	50
	.zero	7
	.quad	.LC50
	.byte	107
	.zero	7
	.quad	.LC4
	.byte	92
	.zero	7
	.quad	.LC4
	.byte	92
	.zero	7
	.quad	.LC51
	.byte	80
	.zero	7
	.quad	.LC52
	.byte	100
	.zero	7
	.quad	.LC4
	.byte	92
	.zero	7
	.quad	.LC4
	.byte	92
	.zero	7
	.quad	.LC4
	.byte	92
	.zero	7
	.quad	.LC53
	.byte	49
	.zero	7
	.quad	.LC4
	.byte	92
	.zero	7
	.quad	.LC4
	.byte	92
	.zero	7
	.quad	.LC4
	.byte	92
	.zero	7
	.quad	.LC4
	.byte	92
	.zero	7
	.quad	.LC4
	.byte	92
	.zero	7
	.quad	.LC4
	.byte	92
	.zero	7
	.quad	.LC4
	.byte	92
	.zero	7
	.quad	.LC4
	.byte	92
	.zero	7
	.section	.rodata._ZN2v88internalL26kPerfectKeywordLengthTableE,"a"
	.align 32
	.type	_ZN2v88internalL26kPerfectKeywordLengthTableE, @object
	.size	_ZN2v88internalL26kPerfectKeywordLengthTableE, 64
_ZN2v88internalL26kPerfectKeywordLengthTableE:
	.string	""
	.string	""
	.string	"\002\003\004\002\006\007\b\t\n\002\003\003\005\003\007\b\004\005\004\007\005\005\005\006\004\005\006\006\004\005\007\b\t\003\004\003\004\005\005\005\006\006\007\005\004\006"
	.string	""
	.string	"\003\n"
	.string	""
	.string	""
	.string	"\006"
	.zero	7
	.weak	_ZZN2v88internal18PerfectKeywordHash4HashEPKciE11asso_values
	.section	.rodata._ZZN2v88internal18PerfectKeywordHash4HashEPKciE11asso_values,"aG",@progbits,_ZZN2v88internal18PerfectKeywordHash4HashEPKciE11asso_values,comdat
	.align 32
	.type	_ZZN2v88internal18PerfectKeywordHash4HashEPKciE11asso_values, @gnu_unique_object
	.size	_ZZN2v88internal18PerfectKeywordHash4HashEPKciE11asso_values, 128
_ZZN2v88internal18PerfectKeywordHash4HashEPKciE11asso_values:
	.string	"8888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888\b"
	.string	"\006"
	.string	""
	.string	"\t\t\t"
	.string	"88\")"
	.ascii	"\003\0068\023\n\r\020'\032%$888888"
	.section	.rodata._ZN2v88internalL15kAsciiCharFlagsE,"a"
	.align 32
	.type	_ZN2v88internalL15kAsciiCharFlagsE, @object
	.size	_ZN2v88internalL15kAsciiCharFlagsE, 128
_ZN2v88internalL15kAsciiCharFlagsE:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\f\b\f\f\b"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\f"
	.string	""
	.string	""
	.string	"\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\002\002\002\002\002\002\002\002\002\002"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"
	.string	"\003"
	.string	""
	.string	"\003"
	.string	"\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
