	.file	"liftoff-assembler.cc"
	.text
	.section	.text._ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv,"axG",@progbits,_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.type	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv, @function
_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv:
.LFB6928:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6928:
	.size	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv, .-_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.section	.text._ZN2v88internal4wasm16LiftoffAssembler10CacheState5StealERKS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16LiftoffAssembler10CacheState5StealERKS3_
	.type	_ZN2v88internal4wasm16LiftoffAssembler10CacheState5StealERKS3_, @function
_ZN2v88internal4wasm16LiftoffAssembler10CacheState5StealERKS3_:
.LFB23160:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	%rdi, %rsi
	je	.L4
	movq	(%rsi), %rsi
	movq	8(%r12), %r13
	movq	(%rdi), %rdi
	movq	16(%rbx), %rax
	subq	%rsi, %r13
	movq	%r13, %rdx
	subq	%rdi, %rax
	sarq	$3, %rdx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jbe	.L5
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L6
	call	free@PLT
.L6:
	movq	%r13, %rdi
	call	malloc@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	leaq	(%rax,%r13), %rax
	movq	%rax, 16(%rbx)
	movq	(%r12), %rsi
.L5:
	movq	%r13, %rdx
	call	memcpy@PLT
	addq	(%rbx), %r13
	movq	%r13, 8(%rbx)
.L4:
	movl	88(%r12), %eax
	leaq	92(%rbx), %rdx
	movl	%eax, 88(%rbx)
	leaq	108(%r12), %rax
	cmpq	%rax, %rdx
	jnb	.L11
	leaq	108(%rbx), %rdx
	leaq	92(%r12), %rax
	cmpq	%rax, %rdx
	ja	.L10
.L11:
	movdqu	92(%r12), %xmm0
	movups	%xmm0, 92(%rbx)
	movdqu	108(%r12), %xmm1
	movups	%xmm1, 108(%rbx)
	movdqu	124(%r12), %xmm2
	movups	%xmm2, 124(%rbx)
	movdqu	140(%r12), %xmm3
	movups	%xmm3, 140(%rbx)
	movl	156(%r12), %eax
	movl	%eax, 156(%rbx)
	movl	160(%r12), %eax
	movl	%eax, 160(%rbx)
.L9:
	movl	164(%r12), %eax
	movl	%eax, 164(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movl	$92, %eax
	.p2align 4,,10
	.p2align 3
.L7:
	movl	(%r12,%rax), %edx
	movl	%edx, (%rbx,%rax)
	addq	$4, %rax
	cmpq	$164, %rax
	jne	.L7
	jmp	.L9
	.cfi_endproc
.LFE23160:
	.size	_ZN2v88internal4wasm16LiftoffAssembler10CacheState5StealERKS3_, .-_ZN2v88internal4wasm16LiftoffAssembler10CacheState5StealERKS3_
	.section	.text._ZN2v88internal4wasm16LiftoffAssembler10CacheState5SplitERKS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16LiftoffAssembler10CacheState5SplitERKS3_
	.type	_ZN2v88internal4wasm16LiftoffAssembler10CacheState5SplitERKS3_, @function
_ZN2v88internal4wasm16LiftoffAssembler10CacheState5SplitERKS3_:
.LFB28379:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	%rsi, %rdi
	je	.L15
	movq	(%rsi), %rsi
	movq	8(%r12), %r13
	movq	(%rdi), %rdi
	movq	16(%rbx), %rax
	subq	%rsi, %r13
	movq	%r13, %rdx
	subq	%rdi, %rax
	sarq	$3, %rdx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jbe	.L16
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L17
	call	free@PLT
.L17:
	movq	%r13, %rdi
	call	malloc@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	leaq	(%rax,%r13), %rax
	movq	%rax, 16(%rbx)
	movq	(%r12), %rsi
.L16:
	movq	%r13, %rdx
	call	memcpy@PLT
	addq	(%rbx), %r13
	movq	%r13, 8(%rbx)
.L15:
	movl	88(%r12), %eax
	leaq	92(%r12), %rdx
	movl	%eax, 88(%rbx)
	leaq	108(%rbx), %rax
	cmpq	%rax, %rdx
	jnb	.L22
	leaq	108(%r12), %rdx
	leaq	92(%rbx), %rax
	cmpq	%rax, %rdx
	ja	.L21
.L22:
	movdqu	92(%r12), %xmm0
	movups	%xmm0, 92(%rbx)
	movdqu	108(%r12), %xmm1
	movups	%xmm1, 108(%rbx)
	movdqu	124(%r12), %xmm2
	movups	%xmm2, 124(%rbx)
	movdqu	140(%r12), %xmm3
	movups	%xmm3, 140(%rbx)
	movl	156(%r12), %eax
	movl	%eax, 156(%rbx)
	movl	160(%r12), %eax
	movl	%eax, 160(%rbx)
.L20:
	movl	164(%r12), %eax
	movl	%eax, 164(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movl	$92, %eax
	.p2align 4,,10
	.p2align 3
.L18:
	movl	(%r12,%rax), %edx
	movl	%edx, (%rbx,%rax)
	addq	$4, %rax
	cmpq	$164, %rax
	jne	.L18
	jmp	.L20
	.cfi_endproc
.LFE28379:
	.size	_ZN2v88internal4wasm16LiftoffAssembler10CacheState5SplitERKS3_, .-_ZN2v88internal4wasm16LiftoffAssembler10CacheState5SplitERKS3_
	.section	.text._ZN2v88internal4wasm16LiftoffAssemblerC2ESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS4_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16LiftoffAssemblerC2ESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS4_EE
	.type	_ZN2v88internal4wasm16LiftoffAssemblerC2ESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS4_EE, @function
_ZN2v88internal4wasm16LiftoffAssemblerC2ESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS4_EE:
.LFB23196:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	-56(%rbp), %r8
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movw	%dx, -32(%rbp)
	leaq	-48(%rbp), %rdx
	movq	$0, (%rsi)
	xorl	%esi, %esi
	movq	%rax, -56(%rbp)
	movl	$256, %eax
	movw	%ax, -44(%rbp)
	movl	$1, -48(%rbp)
	movq	$0, -40(%rbp)
	call	_ZN2v88internal18TurboAssemblerBaseC2EPNS0_7IsolateERKNS0_16AssemblerOptionsENS0_18CodeObjectRequiredESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS9_EE@PLT
	movq	-56(%rbp), %rdi
	leaq	16+_ZTVN2v88internal14TurboAssemblerE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L26
	movq	(%rdi), %rax
	call	*8(%rax)
.L26:
	leaq	16+_ZTVN2v88internal4wasm16LiftoffAssemblerE(%rip), %rax
	pxor	%xmm0, %xmm0
	movl	$0, 540(%rbx)
	movq	%rax, (%rbx)
	leaq	576(%rbx), %rax
	movq	%rax, 552(%rbx)
	movq	%rax, 560(%rbx)
	leaq	640(%rbx), %rax
	movq	%rax, 568(%rbx)
	movl	$0, 640(%rbx)
	movq	$0, 708(%rbx)
	movq	$0, 716(%rbx)
	movb	$0, 724(%rbx)
	movq	$0, 728(%rbx)
	movb	$1, 530(%rbx)
	movups	%xmm0, 644(%rbx)
	movups	%xmm0, 660(%rbx)
	movups	%xmm0, 676(%rbx)
	movups	%xmm0, 692(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L32
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L32:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23196:
	.size	_ZN2v88internal4wasm16LiftoffAssemblerC2ESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS4_EE, .-_ZN2v88internal4wasm16LiftoffAssemblerC2ESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS4_EE
	.globl	_ZN2v88internal4wasm16LiftoffAssemblerC1ESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS4_EE
	.set	_ZN2v88internal4wasm16LiftoffAssemblerC1ESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS4_EE,_ZN2v88internal4wasm16LiftoffAssemblerC2ESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS4_EE
	.section	.rodata._ZN2v88internal4wasm16LiftoffAssembler5SpillEj.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal4wasm16LiftoffAssembler5SpillEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16LiftoffAssembler5SpillEj
	.type	_ZN2v88internal4wasm16LiftoffAssembler5SpillEj, @function
_ZN2v88internal4wasm16LiftoffAssembler5SpillEj:
.LFB23205:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	552(%rdi), %rax
	leaq	(%rax,%rdx,8), %r13
	movzbl	0(%r13), %ebx
	cmpb	$1, %bl
	je	.L34
	cmpb	$2, %bl
	je	.L35
	testb	%bl, %bl
	je	.L33
.L37:
	movb	$0, 0(%r13)
.L33:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L62
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movzbl	1(%r13), %r14d
	movzbl	4(%r13), %ebx
	cmpl	720(%rdi), %esi
	jb	.L38
	leal	1(%rsi), %eax
	movl	%eax, 720(%rdi)
.L38:
	sall	$3, %esi
	movl	$-24, %edx
	leaq	-64(%rbp), %rdi
	subl	%esi, %edx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-64(%rbp), %rsi
	movl	-56(%rbp), %edx
	movq	%rsi, -132(%rbp)
	movl	%edx, -124(%rbp)
	movq	%rsi, -144(%rbp)
	movl	%edx, -136(%rbp)
	cmpb	$3, %r14b
	je	.L39
	ja	.L40
	cmpb	$1, %r14b
	je	.L41
	cmpb	$2, %r14b
	jne	.L43
	movzbl	%bl, %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
.L45:
	movzbl	4(%r13), %eax
	leaq	(%r12,%rax,4), %rdx
	movq	%rax, %rcx
	subl	$1, 644(%rdx)
	jne	.L37
	movl	$-2, %eax
	roll	%cl, %eax
	andl	%eax, 640(%r12)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L40:
	cmpb	$4, %r14b
	jne	.L43
	movq	%rsi, -120(%rbp)
	leal	-10(%rbx), %r10d
	movl	%edx, -112(%rbp)
	movq	%rsi, -108(%rbp)
	movl	%edx, -100(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L48
	subq	$8, %rsp
	movl	-56(%rbp), %r9d
	movq	%rsi, -96(%rbp)
	movq	%rsi, %r8
	pushq	$0
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	$17, %esi
	movq	%r12, %rdi
	pushq	$1
	pushq	$3
	movl	%edx, -88(%rbp)
	movl	%r10d, %edx
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L35:
	cmpb	$1, 1(%r13)
	movslq	4(%r13), %rax
	je	.L63
	movq	$0, -87(%rbp)
	movq	%rax, -95(%rbp)
.L51:
	movb	%bl, -96(%rbp)
	movzbl	-80(%rbp), %eax
	movdqa	-96(%rbp), %xmm1
	movb	%al, -48(%rbp)
	movaps	%xmm1, -64(%rbp)
	cmpl	720(%r12), %esi
	jb	.L52
	leal	1(%rsi), %eax
	movl	%eax, 720(%r12)
.L52:
	sall	$3, %esi
	movl	$-24, %edx
	leaq	-108(%rbp), %rdi
	subl	%esi, %edx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-108(%rbp), %rsi
	movl	-100(%rbp), %r9d
	movq	%rsi, -132(%rbp)
	movl	%r9d, -124(%rbp)
	cmpb	$1, %bl
	jne	.L53
	movl	-63(%rbp), %ecx
	movl	$4, %r8d
	movl	%r9d, %edx
	movq	%r12, %rdi
	movabsq	$81604378624, %rax
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L41:
	movzbl	%bl, %ecx
	movl	$4, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L63:
	pxor	%xmm0, %xmm0
	movl	$1, %ebx
	movups	%xmm0, -95(%rbp)
	movl	%eax, -95(%rbp)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%rsi, -108(%rbp)
	leal	-10(%rbx), %r10d
	movl	%edx, -100(%rbp)
	movq	%rsi, -96(%rbp)
	movl	%edx, -88(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L46
	movl	-56(%rbp), %r9d
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movq	%rsi, %r8
	movl	%r10d, %edx
	movl	$17, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L53:
	movq	-63(%rbp), %rdx
	movl	$2147483648, %eax
	movl	$4294967295, %ecx
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L64
	movq	%rdx, %rax
	shrq	$32, %rax
	jne	.L56
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r14d
	movl	%edx, %edx
	movq	%r12, %rdi
	movl	$4, %ecx
	movabsq	$81604378624, %rax
	orq	%rax, %rdx
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movl	-124(%rbp), %edx
	movl	%r14d, %ecx
	movq	%r12, %rdi
	movq	-132(%rbp), %rsi
	movl	$8, %r8d
	movl	%edx, -100(%rbp)
	movq	%rsi, -108(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L56:
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$19, %ecx
	movl	$10, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movq	-132(%rbp), %rsi
	movl	-124(%rbp), %edx
	movl	$8, %r8d
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %ecx
	movq	%r12, %rdi
	movq	%rsi, -108(%rbp)
	movl	%edx, -100(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L48:
	movl	%r10d, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L46:
	movl	%r10d, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5movssENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L64:
	movl	%edx, %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	movabsq	$81604378624, %rdx
	orq	%rdx, %rcx
	movl	%r9d, %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L37
.L62:
	call	__stack_chk_fail@PLT
.L43:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23205:
	.size	_ZN2v88internal4wasm16LiftoffAssembler5SpillEj, .-_ZN2v88internal4wasm16LiftoffAssembler5SpillEj
	.section	.text._ZN2v88internal4wasm16LiftoffAssembler11SpillLocalsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16LiftoffAssembler11SpillLocalsEv
	.type	_ZN2v88internal4wasm16LiftoffAssembler11SpillLocalsEv, @function
_ZN2v88internal4wasm16LiftoffAssembler11SpillLocalsEv:
.LFB23206:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	540(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	je	.L65
	movl	_ZN2v88internalL4xmm0E(%rip), %eax
	xorl	%r15d, %r15d
	movl	%r15d, %r13d
	movq	%rdi, %r15
	movl	%eax, -212(%rbp)
	movl	_ZN2v88internalL3rbpE(%rip), %eax
	movl	%eax, -200(%rbp)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L100:
	leal	1(%r13), %r14d
	testb	%bl, %bl
	je	.L69
.L70:
	movb	$0, (%r12)
	movl	540(%r15), %esi
.L69:
	movl	%r14d, %r13d
	cmpl	%esi, %r14d
	jnb	.L65
.L66:
	movq	552(%r15), %rcx
	movl	%r13d, %edx
	leaq	(%rcx,%rdx,8), %r12
	movzbl	(%r12), %ebx
	cmpb	$1, %bl
	je	.L67
	cmpb	$2, %bl
	jne	.L100
	cmpb	$1, 1(%r12)
	movslq	4(%r12), %rdx
	je	.L101
	movq	$0, -103(%rbp)
	movq	%rdx, -111(%rbp)
.L84:
	movb	%bl, -112(%rbp)
	movzbl	-96(%rbp), %edx
	leal	1(%r13), %r14d
	movdqa	-112(%rbp), %xmm1
	movb	%dl, -64(%rbp)
	movaps	%xmm1, -80(%rbp)
	cmpl	%r13d, 720(%r15)
	ja	.L85
	movl	%r14d, 720(%r15)
.L85:
	movl	%r13d, %eax
	movl	-200(%rbp), %esi
	leaq	-124(%rbp), %rdi
	negl	%eax
	leal	-24(,%rax,8), %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-124(%rbp), %rsi
	movl	-116(%rbp), %edx
	movq	%rsi, -136(%rbp)
	movl	%edx, -128(%rbp)
	movq	%rsi, -148(%rbp)
	movl	%edx, -140(%rbp)
	cmpb	$1, %bl
	jne	.L86
	movl	-79(%rbp), %ecx
	movl	$4, %r8d
	movq	%r15, %rdi
	movabsq	$-4294967296, %rax
	andq	-208(%rbp), %rax
	orq	%rax, %rcx
	movabsq	$-1095216660481, %rax
	andq	%rax, %rcx
	movabsq	$81604378624, %rax
	orq	%rax, %rcx
	movq	%rcx, -208(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L67:
	movzbl	4(%r12), %eax
	movzbl	1(%r12), %ebx
	movb	%al, -193(%rbp)
	cmpl	%r13d, 720(%r15)
	ja	.L71
	leal	1(%r13), %r14d
	movl	%r14d, 720(%r15)
.L71:
	movl	%r13d, %edx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	leaq	-80(%rbp), %rdi
	negl	%edx
	leal	-24(,%rdx,8), %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rsi
	movl	-72(%rbp), %edx
	movq	%rsi, -160(%rbp)
	movl	%edx, -152(%rbp)
	movq	%rsi, -184(%rbp)
	movl	%edx, -176(%rbp)
	cmpb	$3, %bl
	je	.L72
	ja	.L73
	cmpb	$1, %bl
	jne	.L102
	movzbl	-193(%rbp), %ecx
	movl	$4, %r8d
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
.L78:
	movzbl	4(%r12), %edx
	leal	1(%r13), %r14d
	leaq	(%r15,%rdx,4), %rsi
	movq	%rdx, %rcx
	movl	644(%rsi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 644(%rsi)
	testl	%edx, %edx
	jne	.L70
	movl	$-2, %eax
	roll	%cl, %eax
	andl	%eax, 640(%r15)
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L73:
	cmpb	$4, %bl
	jne	.L76
	movzbl	-193(%rbp), %ebx
	movq	%rsi, -172(%rbp)
	movl	%edx, -164(%rbp)
	movq	%rsi, -148(%rbp)
	leal	-10(%rbx), %r10d
	movl	%edx, -140(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L81
	subq	$8, %rsp
	movl	-72(%rbp), %r9d
	movq	%rsi, -124(%rbp)
	movq	%rsi, %r8
	pushq	$0
	movl	-212(%rbp), %ecx
	movl	$17, %esi
	movq	%r15, %rdi
	pushq	$1
	pushq	$3
	movl	%edx, -116(%rbp)
	movl	%r10d, %edx
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L102:
	cmpb	$2, %bl
	jne	.L76
	movzbl	-193(%rbp), %ecx
	movl	$8, %r8d
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L86:
	movq	-79(%rbp), %r9
	movl	$2147483648, %ecx
	movl	$4294967295, %edi
	addq	%r9, %rcx
	cmpq	%rdi, %rcx
	jbe	.L103
	movq	%r9, %rax
	shrq	$32, %rax
	jne	.L89
	movq	-232(%rbp), %r13
	movl	%r9d, %edx
	movq	%r15, %rdi
	movabsq	$-4294967296, %rax
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %ebx
	movl	$4, %ecx
	andq	%rax, %r13
	movabsq	$-1095216660481, %rax
	orq	%rdx, %r13
	movl	%ebx, %esi
	andq	%r13, %rax
	movabsq	$81604378624, %r13
	orq	%r13, %rax
	movq	%rax, %rdx
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movl	-140(%rbp), %edx
	movl	%ebx, %ecx
	movq	%r15, %rdi
	movq	-148(%rbp), %rsi
	movl	$8, %r8d
	movl	%edx, -116(%rbp)
	movq	%rsi, -124(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L65:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L104
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movl	$1, %ebx
	movups	%xmm0, -111(%rbp)
	movl	%edx, -111(%rbp)
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L72:
	movzbl	-193(%rbp), %ebx
	movq	%rsi, -148(%rbp)
	movl	%edx, -140(%rbp)
	movq	%rsi, -124(%rbp)
	leal	-10(%rbx), %r10d
	movl	%edx, -116(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L79
	movl	-72(%rbp), %r9d
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movq	%rsi, %r8
	movl	%r10d, %edx
	movl	$17, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L89:
	movb	$19, -224(%rbp)
	movq	-224(%rbp), %rcx
	movq	%r9, %rdx
	movq	%r15, %rdi
	movl	$8, %r8d
	movl	$10, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movq	-148(%rbp), %rsi
	movq	%r15, %rdi
	movl	-140(%rbp), %edx
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %ecx
	movl	$8, %r8d
	movq	%rsi, -124(%rbp)
	movl	%edx, -116(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L81:
	movl	%r10d, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L79:
	movl	%r10d, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5movssENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L103:
	movl	%r9d, %ecx
	movl	$8, %r8d
	movq	%r15, %rdi
	movabsq	$-4294967296, %rax
	andq	-240(%rbp), %rax
	orq	%rcx, %rax
	movabsq	$-1095216660481, %rcx
	andq	%rcx, %rax
	movabsq	$81604378624, %rcx
	orq	%rcx, %rax
	movq	%rax, %rcx
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L70
.L76:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L104:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23206:
	.size	_ZN2v88internal4wasm16LiftoffAssembler11SpillLocalsEv, .-_ZN2v88internal4wasm16LiftoffAssembler11SpillLocalsEv
	.section	.text._ZN2v88internal4wasm16LiftoffAssembler17SpillAllRegistersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16LiftoffAssembler17SpillAllRegistersEv
	.type	_ZN2v88internal4wasm16LiftoffAssembler17SpillAllRegistersEv, @function
_ZN2v88internal4wasm16LiftoffAssembler17SpillAllRegistersEv:
.LFB23207:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	552(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	560(%rdi), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	testl	%eax, %eax
	je	.L106
	leal	-1(%rax), %r12d
	leaq	-68(%rbp), %rax
	xorl	%r14d, %r14d
	movq	%rax, -144(%rbp)
	movl	_ZN2v88internalL4xmm0E(%rip), %eax
	movl	%eax, -136(%rbp)
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L107:
	leaq	1(%r14), %rcx
	cmpq	%r12, %r14
	je	.L106
.L124:
	movq	552(%r15), %rdx
	movq	%rcx, %r14
.L120:
	leaq	(%rdx,%r14,8), %r13
	cmpb	$1, 0(%r13)
	jne	.L107
	movzbl	4(%r13), %eax
	movzbl	1(%r13), %ebx
	movl	%r14d, %edx
	movb	%al, -129(%rbp)
	cmpl	%r14d, 720(%r15)
	ja	.L108
	leal	1(%r14), %esi
	movl	%esi, 720(%r15)
.L108:
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movq	-144(%rbp), %rdi
	negl	%edx
	leal	-24(,%rdx,8), %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%rsi, -104(%rbp)
	movl	%edx, -96(%rbp)
	movq	%rsi, -128(%rbp)
	movl	%edx, -120(%rbp)
	cmpb	$3, %bl
	je	.L109
	ja	.L110
	cmpb	$1, %bl
	jne	.L123
	movzbl	-129(%rbp), %ecx
	movl	$4, %r8d
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
.L115:
	movb	$0, 0(%r13)
	leaq	1(%r14), %rcx
	cmpq	%r12, %r14
	jne	.L124
.L106:
	movl	$0, 640(%r15)
	pxor	%xmm0, %xmm0
	movq	$0, 708(%r15)
	movups	%xmm0, 644(%r15)
	movups	%xmm0, 660(%r15)
	movups	%xmm0, 676(%r15)
	movups	%xmm0, 692(%r15)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L125
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	cmpb	$4, %bl
	jne	.L113
	movzbl	-129(%rbp), %ebx
	movq	%rsi, -116(%rbp)
	movl	%edx, -108(%rbp)
	movq	%rsi, -92(%rbp)
	leal	-10(%rbx), %r10d
	movl	%edx, -84(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L118
	subq	$8, %rsp
	movl	-60(%rbp), %r9d
	movq	%rsi, -80(%rbp)
	movq	%rsi, %r8
	pushq	$0
	movl	-136(%rbp), %ecx
	movl	$17, %esi
	movq	%r15, %rdi
	pushq	$1
	pushq	$3
	movl	%edx, -72(%rbp)
	movl	%r10d, %edx
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L123:
	cmpb	$2, %bl
	jne	.L113
	movzbl	-129(%rbp), %ecx
	movl	$8, %r8d
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L109:
	movzbl	-129(%rbp), %ebx
	movq	%rsi, -92(%rbp)
	movl	%edx, -84(%rbp)
	movq	%rsi, -80(%rbp)
	leal	-10(%rbx), %r10d
	movl	%edx, -72(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L116
	movl	-60(%rbp), %r9d
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movq	%rsi, %r8
	movl	%r10d, %edx
	movl	$17, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L116:
	movl	%r10d, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5movssENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L118:
	movl	%r10d, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L115
.L113:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L125:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23207:
	.size	_ZN2v88internal4wasm16LiftoffAssembler17SpillAllRegistersEv, .-_ZN2v88internal4wasm16LiftoffAssembler17SpillAllRegistersEv
	.section	.text._ZN2v88internal4wasm16LiftoffAssembler4MoveENS1_15LiftoffRegisterES3_NS1_9ValueTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16LiftoffAssembler4MoveENS1_15LiftoffRegisterES3_NS1_9ValueTypeE
	.type	_ZN2v88internal4wasm16LiftoffAssembler4MoveENS1_15LiftoffRegisterES3_NS1_9ValueTypeE, @function
_ZN2v88internal4wasm16LiftoffAssembler4MoveENS1_15LiftoffRegisterES3_NS1_9ValueTypeE:
.LFB23213:
	.cfi_startproc
	endbr64
	movzbl	%sil, %r10d
	movzbl	%dl, %edx
	cmpb	$9, %sil
	ja	.L127
	cmpb	$1, %cl
	je	.L138
	movl	$8, %ecx
	movl	%r10d, %esi
	jmp	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	.p2align 4,,10
	.p2align 3
.L127:
	movl	_ZN2v88internal11CpuFeatures10supported_E(%rip), %eax
	subl	$10, %edx
	subl	$10, %r10d
	shrl	$5, %eax
	andl	$1, %eax
	cmpb	$3, %cl
	je	.L139
	testb	%al, %al
	je	.L132
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r8d
	movl	$3, %r9d
	movl	%r10d, %edx
	movl	%r10d, %ecx
	movl	$16, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	$0
	pushq	$1
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore 6
	testb	%al, %al
	je	.L130
	movl	%edx, %r8d
	movl	%r10d, %ecx
	movl	%r10d, %edx
	movl	$16, %esi
	jmp	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_S2_@PLT
	.p2align 4,,10
	.p2align 3
.L138:
	movl	$4, %ecx
	movl	%r10d, %esi
	jmp	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	.p2align 4,,10
	.p2align 3
.L132:
	movl	%r10d, %esi
	jmp	_ZN2v88internal9Assembler5movsdENS0_11XMMRegisterES2_@PLT
	.p2align 4,,10
	.p2align 3
.L130:
	movl	%r10d, %esi
	jmp	_ZN2v88internal9Assembler5movssENS0_11XMMRegisterES2_@PLT
	.cfi_endproc
.LFE23213:
	.size	_ZN2v88internal4wasm16LiftoffAssembler4MoveENS1_15LiftoffRegisterES3_NS1_9ValueTypeE, .-_ZN2v88internal4wasm16LiftoffAssembler4MoveENS1_15LiftoffRegisterES3_NS1_9ValueTypeE
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_119StackTransferRecipe17ClearExecutedMoveENS1_15LiftoffRegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_119StackTransferRecipe17ClearExecutedMoveENS1_15LiftoffRegisterE, @function
_ZN2v88internal4wasm12_GLOBAL__N_119StackTransferRecipe17ClearExecutedMoveENS1_15LiftoffRegisterE:
.LFB23128:
	.cfi_startproc
	movl	$1, %edx
	movzbl	%sil, %ecx
	movl	%edx, %eax
	leaq	(%rdi,%rcx,2), %rsi
	sall	%cl, %eax
	notl	%eax
	andl	%eax, 252(%rdi)
	movzbl	(%rsi), %eax
	leaq	180(%rdi,%rax,4), %rcx
	subl	$1, (%rcx)
	jne	.L146
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movzbl	(%rsi), %r13d
	movq	%rdi, %r12
	movl	%r13d, %ecx
	sall	%cl, %edx
	testl	%edx, 252(%rdi)
	jne	.L149
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	movzbl	%r13b, %eax
	movl	%r13d, %esi
	leaq	(%rdi,%rax,2), %rax
	movq	264(%rdi), %rdi
	movzbl	1(%rax), %ecx
	movzbl	(%rax), %edx
	call	_ZN2v88internal4wasm16LiftoffAssembler4MoveENS1_15LiftoffRegisterES3_NS1_9ValueTypeE
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm12_GLOBAL__N_119StackTransferRecipe17ClearExecutedMoveENS1_15LiftoffRegisterE
	.p2align 4,,10
	.p2align 3
.L146:
	ret
	.cfi_endproc
.LFE23128:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_119StackTransferRecipe17ClearExecutedMoveENS1_15LiftoffRegisterE, .-_ZN2v88internal4wasm12_GLOBAL__N_119StackTransferRecipe17ClearExecutedMoveENS1_15LiftoffRegisterE
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_119StackTransferRecipe12ExecuteMovesEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_119StackTransferRecipe12ExecuteMovesEv, @function
_ZN2v88internal4wasm12_GLOBAL__N_119StackTransferRecipe12ExecuteMovesEv:
.LFB23129:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	252(%rdi), %ebx
	movq	264(%rdi), %r11
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ebx, %ebx
	je	.L150
	movq	%rdi, %r15
	movl	%ebx, %r12d
	movl	$1, %r14d
	movq	%r11, %rdi
	.p2align 4,,10
	.p2align 3
.L167:
	xorl	%esi, %esi
	movl	%r14d, %eax
	rep bsfl	%r12d, %esi
	movl	%esi, %ecx
	sall	%cl, %eax
	movl	%eax, %r13d
	notl	%r13d
	testl	%ebx, %eax
	je	.L152
	movslq	%esi, %rax
	leaq	180(%r15), %r9
	movl	180(%r15,%rax,4), %edx
	movq	%r9, -136(%rbp)
	testl	%edx, %edx
	jne	.L152
	leaq	(%r15,%rax,2), %r8
	movzbl	(%r8), %edx
	movzbl	1(%r8), %ecx
	movq	%r8, -144(%rbp)
	call	_ZN2v88internal4wasm16LiftoffAssembler4MoveENS1_15LiftoffRegisterES3_NS1_9ValueTypeE
	movq	-144(%rbp), %r8
	andl	%r13d, 252(%r15)
	movq	-136(%rbp), %r9
	movzbl	(%r8), %eax
	leaq	(%r9,%rax,4), %rdx
	subl	$1, (%rdx)
	jne	.L253
	movzbl	(%r8), %esi
	movl	252(%r15), %ebx
	movl	%r14d, %eax
	movq	%r9, -144(%rbp)
	movq	264(%r15), %rdi
	movl	%esi, %ecx
	sall	%cl, %eax
	testl	%eax, %ebx
	movl	%eax, -136(%rbp)
	je	.L152
	movzbl	%sil, %edx
	leaq	(%r15,%rdx,2), %r8
	movzbl	(%r8), %edx
	movzbl	1(%r8), %ecx
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal4wasm16LiftoffAssembler4MoveENS1_15LiftoffRegisterES3_NS1_9ValueTypeE
	movl	-136(%rbp), %eax
	movq	-152(%rbp), %r8
	movq	-144(%rbp), %r9
	notl	%eax
	andl	%eax, 252(%r15)
	movzbl	(%r8), %eax
	leaq	(%r9,%rax,4), %rdx
	subl	$1, (%rdx)
	jne	.L253
	movzbl	(%r8), %esi
	movl	252(%r15), %ebx
	movl	%r14d, %eax
	movq	%r9, -144(%rbp)
	movq	264(%r15), %rdi
	movl	%esi, %ecx
	sall	%cl, %eax
	testl	%eax, %ebx
	movl	%eax, -136(%rbp)
	jne	.L255
	.p2align 4,,10
	.p2align 3
.L152:
	andl	%r13d, %r12d
	jne	.L167
.L258:
	movq	560(%rdi), %r12
	subq	552(%rdi), %r12
	sarq	$3, %r12
	testl	%ebx, %ebx
	je	.L150
	leaq	-68(%rbp), %rax
	movq	%rdi, %r10
	movq	%rax, -160(%rbp)
	movl	_ZN2v88internalL4xmm0E(%rip), %eax
	movl	%eax, -164(%rbp)
	.p2align 4,,10
	.p2align 3
.L195:
	xorl	%eax, %eax
	rep bsfl	%ebx, %eax
	movslq	%eax, %rbx
	movl	%eax, -144(%rbp)
	leaq	(%r15,%rbx,2), %r14
	movzbl	(%r14), %eax
	movzbl	1(%r14), %r13d
	movb	%al, -152(%rbp)
	cmpl	%r12d, 720(%r10)
	ja	.L168
	leal	1(%r12), %edx
	movl	%edx, 720(%r10)
.L168:
	leal	0(,%r12,8), %edx
	movl	$-24, %eax
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movq	-160(%rbp), %rdi
	subl	%edx, %eax
	movq	%r10, -136(%rbp)
	movl	%eax, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	cmpb	$3, %r13b
	movq	-136(%rbp), %r10
	movq	%rsi, -104(%rbp)
	movl	%edx, -96(%rbp)
	movq	%rsi, -128(%rbp)
	movl	%edx, -120(%rbp)
	je	.L169
	ja	.L170
	cmpb	$1, %r13b
	jne	.L256
	movzbl	-152(%rbp), %ecx
	movl	$4, %r8d
	movq	%r10, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
.L175:
	movzbl	-144(%rbp), %ecx
	movl	$1, %edx
	movl	256(%r15), %esi
	sall	%cl, %edx
	testl	%edx, %esi
	jne	.L180
	movzbl	1(%r14), %edi
	leaq	36(%r15,%rbx,8), %rcx
	orl	%edx, %esi
	movl	%esi, 256(%r15)
	movb	$1, (%rcx)
	movb	%dil, 1(%rcx)
	movl	%r12d, 4(%rcx)
.L180:
	notl	%edx
	andl	%edx, 252(%r15)
	movzbl	(%r14), %edx
	addl	$1, %r12d
	leaq	180(%r15), %r13
	leaq	0(%r13,%rdx,4), %rcx
	movl	(%rcx), %eax
	leal	-1(%rax), %edx
	movl	%edx, (%rcx)
	movl	252(%r15), %ebx
	testl	%edx, %edx
	jne	.L182
	movzbl	(%r14), %esi
	movl	$1, %r14d
	movl	%esi, %ecx
	sall	%cl, %r14d
	testl	%r14d, %ebx
	je	.L182
	movzbl	%sil, %edx
	movq	264(%r15), %rdi
	notl	%r14d
	leaq	(%r15,%rdx,2), %r8
	movzbl	1(%r8), %ecx
	movzbl	(%r8), %edx
	movq	%r8, -136(%rbp)
	call	_ZN2v88internal4wasm16LiftoffAssembler4MoveENS1_15LiftoffRegisterES3_NS1_9ValueTypeE
	movq	-136(%rbp), %r8
	andl	%r14d, 252(%r15)
	movzbl	(%r8), %edx
	leaq	0(%r13,%rdx,4), %rcx
	movl	(%rcx), %eax
	leal	-1(%rax), %edx
	movl	%edx, (%rcx)
	movl	252(%r15), %ebx
	testl	%edx, %edx
	jne	.L182
	movzbl	(%r8), %esi
	movl	$1, %r14d
	movl	%esi, %ecx
	sall	%cl, %r14d
	testl	%r14d, %ebx
	je	.L182
	movzbl	%sil, %edx
	movq	264(%r15), %rdi
	notl	%r14d
	leaq	(%r15,%rdx,2), %r8
	movzbl	1(%r8), %ecx
	movzbl	(%r8), %edx
	movq	%r8, -136(%rbp)
	call	_ZN2v88internal4wasm16LiftoffAssembler4MoveENS1_15LiftoffRegisterES3_NS1_9ValueTypeE
	movq	-136(%rbp), %r8
	andl	%r14d, 252(%r15)
	movzbl	(%r8), %edx
	leaq	0(%r13,%rdx,4), %rcx
	movl	(%rcx), %eax
	leal	-1(%rax), %edx
	movl	%edx, (%rcx)
	movl	252(%r15), %ebx
	testl	%edx, %edx
	jne	.L182
	movzbl	(%r8), %esi
	movl	$1, %r14d
	movl	%esi, %ecx
	sall	%cl, %r14d
	testl	%r14d, %ebx
	jne	.L257
	.p2align 4,,10
	.p2align 3
.L182:
	testl	%ebx, %ebx
	je	.L150
	movq	264(%r15), %r10
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L256:
	cmpb	$2, %r13b
	jne	.L173
	movzbl	-152(%rbp), %ecx
	movl	$8, %r8d
	movq	%r10, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L170:
	cmpb	$4, %r13b
	jne	.L173
	movzbl	-152(%rbp), %r13d
	movq	%rsi, -116(%rbp)
	movl	%edx, -108(%rbp)
	movq	%rsi, -92(%rbp)
	subl	$10, %r13d
	movl	%edx, -84(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L178
	subq	$8, %rsp
	movl	-60(%rbp), %r9d
	movq	%rsi, -80(%rbp)
	movq	%rsi, %r8
	pushq	$0
	movl	-164(%rbp), %ecx
	movl	$17, %esi
	movq	%r10, %rdi
	pushq	$1
	pushq	$3
	movl	%edx, -72(%rbp)
	movl	%r13d, %edx
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L169:
	movzbl	-152(%rbp), %r13d
	movq	%rsi, -92(%rbp)
	movl	%edx, -84(%rbp)
	movq	%rsi, -80(%rbp)
	subl	$10, %r13d
	movl	%edx, -72(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L176
	movl	-60(%rbp), %r9d
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movq	%rsi, %r8
	movl	%r13d, %edx
	movl	$17, %esi
	movq	%r10, %rdi
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L255:
	movzbl	%sil, %edx
	leaq	(%r15,%rdx,2), %r8
	movzbl	(%r8), %edx
	movzbl	1(%r8), %ecx
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal4wasm16LiftoffAssembler4MoveENS1_15LiftoffRegisterES3_NS1_9ValueTypeE
	movl	-136(%rbp), %eax
	movq	-152(%rbp), %r8
	movq	-144(%rbp), %r9
	notl	%eax
	andl	%eax, 252(%r15)
	movzbl	(%r8), %eax
	leaq	(%r9,%rax,4), %rdx
	subl	$1, (%rdx)
	jne	.L253
	movzbl	(%r8), %esi
	movl	252(%r15), %ebx
	movl	%r14d, %eax
	movq	%r9, -144(%rbp)
	movq	264(%r15), %rdi
	movl	%esi, %ecx
	sall	%cl, %eax
	testl	%eax, %ebx
	movl	%eax, -136(%rbp)
	je	.L152
	movzbl	%sil, %edx
	leaq	(%r15,%rdx,2), %r8
	movzbl	(%r8), %edx
	movzbl	1(%r8), %ecx
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal4wasm16LiftoffAssembler4MoveENS1_15LiftoffRegisterES3_NS1_9ValueTypeE
	movl	-136(%rbp), %eax
	movq	-152(%rbp), %r8
	movq	-144(%rbp), %r9
	notl	%eax
	andl	%eax, 252(%r15)
	movzbl	(%r8), %eax
	leaq	(%r9,%rax,4), %rdx
	subl	$1, (%rdx)
	jne	.L253
	movzbl	(%r8), %ecx
	movl	252(%r15), %ebx
	movl	%r14d, %eax
	movq	%r9, -144(%rbp)
	movq	264(%r15), %rdi
	sall	%cl, %eax
	testl	%eax, %ebx
	movl	%eax, -136(%rbp)
	je	.L152
	leaq	(%r15,%rcx,2), %r8
	movl	%ecx, %esi
	movzbl	(%r8), %edx
	movzbl	1(%r8), %ecx
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal4wasm16LiftoffAssembler4MoveENS1_15LiftoffRegisterES3_NS1_9ValueTypeE
	movl	-136(%rbp), %eax
	movq	-152(%rbp), %r8
	movq	-144(%rbp), %r9
	notl	%eax
	andl	%eax, 252(%r15)
	movzbl	(%r8), %eax
	leaq	(%r9,%rax,4), %rdx
	subl	$1, (%rdx)
	jne	.L253
	movzbl	(%r8), %esi
	movl	252(%r15), %ebx
	movl	%r14d, %eax
	movq	%r9, -144(%rbp)
	movq	264(%r15), %rdi
	movl	%esi, %ecx
	sall	%cl, %eax
	testl	%eax, %ebx
	movl	%eax, -136(%rbp)
	je	.L152
	movzbl	%sil, %edx
	leaq	(%r15,%rdx,2), %r8
	movzbl	(%r8), %edx
	movzbl	1(%r8), %ecx
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal4wasm16LiftoffAssembler4MoveENS1_15LiftoffRegisterES3_NS1_9ValueTypeE
	movl	-136(%rbp), %eax
	movq	-152(%rbp), %r8
	movq	-144(%rbp), %r9
	notl	%eax
	andl	%eax, 252(%r15)
	movzbl	(%r8), %eax
	leaq	(%r9,%rax,4), %rdx
	subl	$1, (%rdx)
	jne	.L253
	movzbl	(%r8), %esi
	movl	252(%r15), %ebx
	movl	%r14d, %r8d
	movq	%r9, -152(%rbp)
	movq	264(%r15), %rdi
	movl	%esi, %ecx
	sall	%cl, %r8d
	testl	%r8d, %ebx
	movl	%r8d, -144(%rbp)
	je	.L152
	movzbl	%sil, %eax
	leaq	(%r15,%rax,2), %rax
	movzbl	1(%rax), %ecx
	movzbl	(%rax), %edx
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal4wasm16LiftoffAssembler4MoveENS1_15LiftoffRegisterES3_NS1_9ValueTypeE
	movl	-144(%rbp), %r8d
	movq	-136(%rbp), %rax
	movq	-152(%rbp), %r9
	notl	%r8d
	andl	%r8d, 252(%r15)
	movzbl	(%rax), %edx
	leaq	(%r9,%rdx,4), %rcx
	movl	(%rcx), %edi
	leal	-1(%rdi), %edx
	movl	%edx, (%rcx)
	movl	252(%r15), %ebx
	testl	%edx, %edx
	jne	.L254
	movzbl	(%rax), %esi
	movl	%r14d, %eax
	movq	264(%r15), %rdi
	movl	%esi, %ecx
	sall	%cl, %eax
	testl	%ebx, %eax
	je	.L152
	movzbl	%sil, %eax
	movb	%sil, -136(%rbp)
	addq	%rax, %rax
	addq	%r15, %rax
	movzbl	1(%rax), %ecx
	movzbl	(%rax), %edx
	call	_ZN2v88internal4wasm16LiftoffAssembler4MoveENS1_15LiftoffRegisterES3_NS1_9ValueTypeE
	movzbl	-136(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_119StackTransferRecipe17ClearExecutedMoveENS1_15LiftoffRegisterE
	.p2align 4,,10
	.p2align 3
.L253:
	movl	252(%r15), %ebx
.L254:
	movq	264(%r15), %rdi
	andl	%r13d, %r12d
	jne	.L167
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L178:
	movl	%r13d, %ecx
	movq	%r10, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L176:
	movl	%r13d, %ecx
	movq	%r10, %rdi
	call	_ZN2v88internal9Assembler5movssENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L150:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L259
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_restore_state
	movzbl	%sil, %edx
	movq	264(%r15), %rdi
	notl	%r14d
	leaq	(%r15,%rdx,2), %r8
	movzbl	1(%r8), %ecx
	movzbl	(%r8), %edx
	movq	%r8, -136(%rbp)
	call	_ZN2v88internal4wasm16LiftoffAssembler4MoveENS1_15LiftoffRegisterES3_NS1_9ValueTypeE
	movq	-136(%rbp), %r8
	andl	%r14d, 252(%r15)
	movzbl	(%r8), %edx
	leaq	0(%r13,%rdx,4), %rcx
	movl	(%rcx), %eax
	leal	-1(%rax), %edx
	movl	%edx, (%rcx)
	movl	252(%r15), %ebx
	testl	%edx, %edx
	jne	.L182
	movzbl	(%r8), %esi
	movl	$1, %r14d
	movl	%esi, %ecx
	sall	%cl, %r14d
	testl	%r14d, %ebx
	je	.L182
	movzbl	%sil, %edx
	movq	264(%r15), %rdi
	notl	%r14d
	leaq	(%r15,%rdx,2), %r8
	movzbl	1(%r8), %ecx
	movzbl	(%r8), %edx
	movq	%r8, -136(%rbp)
	call	_ZN2v88internal4wasm16LiftoffAssembler4MoveENS1_15LiftoffRegisterES3_NS1_9ValueTypeE
	movq	-136(%rbp), %r8
	andl	%r14d, 252(%r15)
	movzbl	(%r8), %edx
	leaq	0(%r13,%rdx,4), %rcx
	movl	(%rcx), %eax
	leal	-1(%rax), %edx
	movl	%edx, (%rcx)
	movl	252(%r15), %ebx
	testl	%edx, %edx
	jne	.L182
	movzbl	(%r8), %ecx
	movl	$1, %r14d
	sall	%cl, %r14d
	testl	%r14d, %ebx
	je	.L182
	leaq	(%r15,%rcx,2), %r8
	movl	%ecx, %esi
	movq	264(%r15), %rdi
	notl	%r14d
	movzbl	1(%r8), %ecx
	movzbl	(%r8), %edx
	movq	%r8, -136(%rbp)
	call	_ZN2v88internal4wasm16LiftoffAssembler4MoveENS1_15LiftoffRegisterES3_NS1_9ValueTypeE
	movq	-136(%rbp), %r8
	andl	%r14d, 252(%r15)
	movzbl	(%r8), %edx
	leaq	0(%r13,%rdx,4), %rcx
	movl	(%rcx), %eax
	leal	-1(%rax), %edx
	movl	%edx, (%rcx)
	movl	252(%r15), %ebx
	testl	%edx, %edx
	jne	.L182
	movzbl	(%r8), %esi
	movl	$1, %r14d
	movl	%esi, %ecx
	sall	%cl, %r14d
	testl	%r14d, %ebx
	je	.L182
	movzbl	%sil, %edx
	movq	264(%r15), %rdi
	notl	%r14d
	leaq	(%r15,%rdx,2), %r8
	movzbl	1(%r8), %ecx
	movzbl	(%r8), %edx
	movq	%r8, -136(%rbp)
	call	_ZN2v88internal4wasm16LiftoffAssembler4MoveENS1_15LiftoffRegisterES3_NS1_9ValueTypeE
	movq	-136(%rbp), %r8
	andl	%r14d, 252(%r15)
	movzbl	(%r8), %edx
	leaq	0(%r13,%rdx,4), %rcx
	movl	(%rcx), %eax
	leal	-1(%rax), %edx
	movl	%edx, (%rcx)
	movl	252(%r15), %ebx
	testl	%edx, %edx
	jne	.L182
	movzbl	(%r8), %r13d
	movl	$1, %edx
	movl	%r13d, %ecx
	sall	%cl, %edx
	testl	%ebx, %edx
	je	.L182
	movzbl	%r13b, %edx
	movq	264(%r15), %rdi
	movl	%r13d, %esi
	leaq	(%r15,%rdx,2), %rdx
	movzbl	1(%rdx), %ecx
	movzbl	(%rdx), %edx
	call	_ZN2v88internal4wasm16LiftoffAssembler4MoveENS1_15LiftoffRegisterES3_NS1_9ValueTypeE
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_119StackTransferRecipe17ClearExecutedMoveENS1_15LiftoffRegisterE
	movl	252(%r15), %ebx
	jmp	.L182
.L173:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L259:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23129:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_119StackTransferRecipe12ExecuteMovesEv, .-_ZN2v88internal4wasm12_GLOBAL__N_119StackTransferRecipe12ExecuteMovesEv
	.section	.text._ZN2v88internal4wasm16LiftoffAssembler18MergeFullStackWithERKNS2_10CacheStateES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16LiftoffAssembler18MergeFullStackWithERKNS2_10CacheStateES5_
	.type	_ZN2v88internal4wasm16LiftoffAssembler18MergeFullStackWithERKNS2_10CacheStateES5_, @function
_ZN2v88internal4wasm16LiftoffAssembler18MergeFullStackWithERKNS2_10CacheStateES5_:
.LFB23203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$552, %rsp
	.cfi_offset 3, -56
	movq	(%rdx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%r12), %rax
	movq	$0, -92(%rbp)
	movq	$0, -84(%rbp)
	subq	%rdx, %rax
	movq	%rdi, -72(%rbp)
	sarq	$3, %rax
	movups	%xmm0, -156(%rbp)
	movups	%xmm0, -140(%rbp)
	movups	%xmm0, -124(%rbp)
	movups	%xmm0, -108(%rbp)
	testl	%eax, %eax
	je	.L261
	leal	-1(%rax), %r14d
	movl	_ZN2v88internalL4xmm0E(%rip), %eax
	movq	%rsi, %r15
	xorl	%ebx, %ebx
	movl	%eax, -560(%rbp)
	movl	_ZN2v88internalL3rbpE(%rip), %eax
	movl	%eax, -536(%rbp)
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L318:
	cmpb	$1, %al
	jne	.L264
	movzbl	(%rdx), %eax
	movzbl	4(%rcx), %ecx
	cmpb	$1, %al
	je	.L286
	cmpb	$2, %al
	je	.L287
	testb	%al, %al
	je	.L317
	.p2align 4,,10
	.p2align 3
.L264:
	leaq	1(%rbx), %rax
	cmpq	%rbx, %r14
	je	.L261
.L320:
	movq	(%r12), %rdx
	movq	%rax, %rbx
.L293:
	movq	(%r15), %rcx
	leaq	0(,%rbx,8), %rax
	addq	%rax, %rdx
	addq	%rax, %rcx
	movzbl	(%rcx), %eax
	testb	%al, %al
	jne	.L318
	movzbl	(%rdx), %r13d
	cmpb	$1, %r13b
	je	.L265
	cmpb	$2, %r13b
	jne	.L264
	cmpb	$1, 1(%rdx)
	movq	-72(%rbp), %r9
	movslq	4(%rdx), %rax
	je	.L319
	movq	$0, -423(%rbp)
	movq	%rax, -431(%rbp)
.L280:
	movzbl	-416(%rbp), %eax
	movb	%r13b, -432(%rbp)
	movdqa	-432(%rbp), %xmm3
	movb	%al, -352(%rbp)
	movl	%ebx, %eax
	movaps	%xmm3, -368(%rbp)
	cmpl	%ebx, 720(%r9)
	ja	.L281
	leal	1(%rbx), %edx
	movl	%edx, 720(%r9)
.L281:
	negl	%eax
	movl	-536(%rbp), %esi
	leaq	-400(%rbp), %rdi
	movq	%r9, -544(%rbp)
	leal	-24(,%rax,8), %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-400(%rbp), %rsi
	cmpb	$1, %r13b
	movl	-392(%rbp), %edx
	movq	-544(%rbp), %r9
	movq	%rsi, -504(%rbp)
	movl	%edx, -496(%rbp)
	movq	%rsi, -444(%rbp)
	movl	%edx, -436(%rbp)
	jne	.L282
	movl	-367(%rbp), %ecx
	movl	$4, %r8d
	movq	%r9, %rdi
	movabsq	$-4294967296, %rax
	andq	-568(%rbp), %rax
	orq	%rcx, %rax
	movabsq	$-1095216660481, %rcx
	andq	%rcx, %rax
	movabsq	$81604378624, %rcx
	orq	%rcx, %rax
	movq	%rax, %rcx
	movq	%rax, -568(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_9ImmediateEi@PLT
	leaq	1(%rbx), %rax
	cmpq	%rbx, %r14
	jne	.L320
	.p2align 4,,10
	.p2align 3
.L261:
	leaq	-336(%rbp), %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_119StackTransferRecipe12ExecuteMovesEv
	movl	-80(%rbp), %ebx
	leaq	-300(%rbp), %rax
	movq	%rax, -536(%rbp)
	testl	%ebx, %ebx
	je	.L260
	movl	_ZN2v88internalL4xmm0E(%rip), %eax
	movl	_ZN2v88internalL3rbpE(%rip), %r13d
	movl	%eax, -544(%rbp)
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L323:
	ja	.L321
	cmpb	$2, 1(%rax)
	movslq	4(%rax), %rdx
	movq	-72(%rbp), %rdi
	je	.L322
	pxor	%xmm0, %xmm0
	movb	$1, -400(%rbp)
	movups	%xmm0, -399(%rbp)
	movzbl	-384(%rbp), %eax
	movl	%edx, -399(%rbp)
	movdqa	-400(%rbp), %xmm1
	movb	%al, -352(%rbp)
	movaps	%xmm1, -368(%rbp)
	movl	-367(%rbp), %eax
	testl	%eax, %eax
	jne	.L300
	movl	$4, %r8d
	movl	%r15d, %ecx
	movl	%r15d, %edx
	movl	$51, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	.p2align 4,,10
	.p2align 3
.L297:
	movl	$-2, %eax
	movl	%r15d, %ecx
	roll	%cl, %eax
	andl	%eax, %ebx
	je	.L260
.L294:
	xorl	%r15d, %r15d
	movq	-536(%rbp), %rsi
	rep bsfl	%ebx, %r15d
	movslq	%r15d, %rax
	leaq	(%rsi,%rax,8), %rax
	movzbl	(%rax), %edx
	cmpb	$1, %dl
	jne	.L323
	movzbl	1(%rax), %r12d
	movl	4(%rax), %eax
	movl	$-24, %edx
	movl	%r13d, %esi
	leaq	-368(%rbp), %rdi
	movq	-72(%rbp), %r14
	sall	$3, %eax
	subl	%eax, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-368(%rbp), %rdx
	movl	-360(%rbp), %eax
	movq	%rdx, -468(%rbp)
	movl	%eax, -460(%rbp)
	movq	%rdx, -492(%rbp)
	movl	%eax, -484(%rbp)
	cmpb	$3, %r12b
	je	.L301
	ja	.L302
	cmpb	$1, %r12b
	jne	.L324
	movl	-360(%rbp), %ecx
	movl	$4, %r8d
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$-2, %eax
	movl	%r15d, %ecx
	roll	%cl, %eax
	andl	%eax, %ebx
	jne	.L294
	.p2align 4,,10
	.p2align 3
.L260:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L325
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L321:
	.cfi_restore_state
	subl	$2, %edx
	cmpb	$1, %dl
	ja	.L297
.L272:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L324:
	cmpb	$2, %r12b
	jne	.L272
	movl	-360(%rbp), %ecx
	movl	$8, %r8d
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L302:
	cmpb	$4, %r12b
	jne	.L272
	movq	%rdx, -480(%rbp)
	leal	-10(%r15), %esi
	movl	%eax, -472(%rbp)
	movq	%rdx, -456(%rbp)
	movl	%eax, -448(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L309
	subq	$8, %rsp
	movl	-544(%rbp), %ecx
	movq	%rdx, %r8
	movq	%r14, %rdi
	pushq	$0
	movl	-360(%rbp), %r9d
	pushq	$1
	pushq	$3
	movq	%rdx, -444(%rbp)
	movl	%esi, %edx
	movl	$16, %esi
	movl	%eax, -436(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L322:
	movq	$0, -391(%rbp)
	movzbl	-384(%rbp), %eax
	movl	%r15d, %esi
	movq	%rdx, -399(%rbp)
	movb	$2, -400(%rbp)
	movdqa	-400(%rbp), %xmm2
	movb	%al, -352(%rbp)
	movaps	%xmm2, -368(%rbp)
	movq	-367(%rbp), %rdx
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L301:
	movq	%rdx, -456(%rbp)
	leal	-10(%r15), %esi
	movl	%eax, -448(%rbp)
	movq	%rdx, -444(%rbp)
	movl	%eax, -436(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L307
	movl	-360(%rbp), %r9d
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movq	%rdx, %r8
	movq	%r14, %rdi
	movl	%esi, %edx
	movl	$16, %esi
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L309:
	movl	-448(%rbp), %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L307:
	movl	-436(%rbp), %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler5movssENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L300:
	movabsq	$-4294967296, %r14
	andq	-552(%rbp), %r14
	movl	$4, %ecx
	movl	%r15d, %esi
	orq	%r14, %rax
	movabsq	$-1095216660481, %r14
	andq	%rax, %r14
	movabsq	$81604378624, %rax
	orq	%rax, %r14
	movq	%r14, %rdx
	movq	%r14, -552(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L265:
	movzbl	4(%rdx), %eax
	movq	-72(%rbp), %r10
	movzbl	1(%rdx), %r13d
	movl	%ebx, %edx
	movb	%al, -553(%rbp)
	cmpl	%ebx, 720(%r10)
	jbe	.L326
.L267:
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	negl	%edx
	leaq	-368(%rbp), %rdi
	movq	%r10, -544(%rbp)
	leal	-24(,%rdx,8), %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-368(%rbp), %rsi
	cmpb	$3, %r13b
	movl	-360(%rbp), %edx
	movq	-544(%rbp), %r10
	movq	%rsi, -516(%rbp)
	movl	%edx, -508(%rbp)
	movq	%rsi, -480(%rbp)
	movl	%edx, -472(%rbp)
	je	.L268
	ja	.L269
	cmpb	$1, %r13b
	je	.L270
	cmpb	$2, %r13b
	jne	.L272
	movzbl	-553(%rbp), %ecx
	movl	$8, %r8d
	movq	%r10, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L269:
	cmpb	$4, %r13b
	jne	.L272
	movzbl	-553(%rbp), %r13d
	movq	%rsi, -456(%rbp)
	movl	%edx, -448(%rbp)
	movq	%rsi, -444(%rbp)
	subl	$10, %r13d
	movl	%edx, -436(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L277
	subq	$8, %rsp
	movl	-560(%rbp), %ecx
	movq	%rsi, %r8
	movq	%r10, %rdi
	pushq	$0
	movl	-360(%rbp), %r9d
	pushq	$1
	pushq	$3
	movq	%rsi, -400(%rbp)
	movl	$17, %esi
	movl	%edx, -392(%rbp)
	movl	%r13d, %edx
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L317:
	movl	$1, %eax
	movl	-80(%rbp), %esi
	sall	%cl, %eax
	testl	%eax, %esi
	jne	.L264
	movzbl	1(%rdx), %edx
	orl	%esi, %eax
	movl	%eax, -80(%rbp)
	leaq	-300(%rbp,%rcx,8), %rax
	movb	$1, (%rax)
	movb	%dl, 1(%rax)
	movl	%ebx, 4(%rax)
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L287:
	movzbl	%cl, %esi
	movl	4(%rdx), %r8d
	leaq	-300(%rbp,%rsi,8), %rdi
	movl	$1, %esi
	sall	%cl, %esi
	orl	-80(%rbp), %esi
	cmpb	$1, 1(%rdx)
	movl	%esi, -80(%rbp)
	je	.L327
	movb	$0, (%rdi)
	movb	%al, 1(%rdi)
	movl	%r8d, 4(%rdi)
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L286:
	movzbl	4(%rdx), %eax
	cmpb	%al, %cl
	je	.L264
	movzbl	1(%rdx), %edi
	movl	-84(%rbp), %esi
	movl	$1, %edx
	sall	%cl, %edx
	testl	%edx, %esi
	jne	.L328
	orl	%esi, %edx
	movl	%edx, -84(%rbp)
	movzbl	%al, %edx
	addl	$1, -156(%rbp,%rdx,4)
	leaq	-336(%rbp,%rcx,2), %rdx
	movb	%al, (%rdx)
	movb	%dil, 1(%rdx)
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L326:
	leal	1(%rbx), %ecx
	movl	%ecx, 720(%r10)
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L327:
	movl	$1, %eax
	movb	$0, (%rdi)
	movb	%al, 1(%rdi)
	movl	%r8d, 4(%rdi)
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L319:
	pxor	%xmm0, %xmm0
	movl	$1, %r13d
	movups	%xmm0, -431(%rbp)
	movl	%eax, -431(%rbp)
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L282:
	movq	-367(%rbp), %r10
	movl	$2147483648, %eax
	movl	$4294967295, %ecx
	addq	%r10, %rax
	cmpq	%rcx, %rax
	jbe	.L329
	movq	%r10, %rax
	shrq	$32, %rax
	jne	.L285
	movl	%r10d, %eax
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r13d
	movq	%r9, %rdi
	movabsq	$-4294967296, %r10
	andq	-584(%rbp), %r10
	movl	$4, %ecx
	movq	%r9, -544(%rbp)
	orq	%rax, %r10
	movl	%r13d, %esi
	movabsq	$-1095216660481, %rax
	andq	%rax, %r10
	movabsq	$81604378624, %rax
	orq	%rax, %r10
	movq	%r10, %rdx
	movq	%r10, -584(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movq	-544(%rbp), %r9
	movl	%r13d, %ecx
	movq	-444(%rbp), %rsi
	movl	-436(%rbp), %edx
	movl	$8, %r8d
	movq	%r9, %rdi
	movq	%rsi, -400(%rbp)
	movl	%edx, -392(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L270:
	movzbl	-553(%rbp), %ecx
	movl	$4, %r8d
	movq	%r10, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L285:
	movb	$19, -576(%rbp)
	movq	-576(%rbp), %rcx
	movq	%r10, %rdx
	movq	%r9, %rdi
	movl	$8, %r8d
	movl	$10, %esi
	movq	%r9, -544(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movq	-544(%rbp), %r9
	movq	-444(%rbp), %rsi
	movl	$8, %r8d
	movl	-436(%rbp), %edx
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %ecx
	movq	%r9, %rdi
	movq	%rsi, -400(%rbp)
	movl	%edx, -392(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L264
.L277:
	movl	%r13d, %ecx
	movq	%r10, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L268:
	movzbl	-553(%rbp), %r13d
	movq	%rsi, -444(%rbp)
	movl	%edx, -436(%rbp)
	movq	%rsi, -400(%rbp)
	subl	$10, %r13d
	movl	%edx, -392(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L275
	movl	-360(%rbp), %r9d
	movq	%rsi, %r8
	movl	%r13d, %edx
	movq	%r10, %rdi
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	$17, %esi
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L328:
	cmpb	$4, %dil
	jne	.L264
	movb	$4, -335(%rbp,%rcx,2)
	jmp	.L264
.L329:
	movl	%r10d, %eax
	movl	$8, %r8d
	movq	%r9, %rdi
	movabsq	$-4294967296, %r10
	andq	-592(%rbp), %r10
	orq	%rax, %r10
	movabsq	$-1095216660481, %rax
	andq	%rax, %r10
	movabsq	$81604378624, %rax
	orq	%rax, %r10
	movq	%r10, %rcx
	movq	%r10, -592(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L264
.L275:
	movl	%r13d, %ecx
	movq	%r10, %rdi
	call	_ZN2v88internal9Assembler5movssENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L264
.L325:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23203:
	.size	_ZN2v88internal4wasm16LiftoffAssembler18MergeFullStackWithERKNS2_10CacheStateES5_, .-_ZN2v88internal4wasm16LiftoffAssembler18MergeFullStackWithERKNS2_10CacheStateES5_
	.section	.text._ZN2v88internal4wasm16LiftoffAssembler14MergeStackWithERKNS2_10CacheStateEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16LiftoffAssembler14MergeStackWithERKNS2_10CacheStateEj
	.type	_ZN2v88internal4wasm16LiftoffAssembler14MergeStackWithERKNS2_10CacheStateEj, @function
_ZN2v88internal4wasm16LiftoffAssembler14MergeStackWithERKNS2_10CacheStateEj:
.LFB23204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	%edx, %edi
	pushq	%r12
	pushq	%rbx
	subq	$696, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -620(%rbp)
	movq	(%r14), %rcx
	movq	552(%r13), %rsi
	movq	560(%r13), %rdx
	movq	8(%r14), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	subq	%rsi, %rdx
	movq	$0, -92(%rbp)
	sarq	$3, %rdx
	subq	%rcx, %rbx
	movq	%r13, -72(%rbp)
	movl	%edx, -616(%rbp)
	sarq	$3, %rbx
	subl	%edi, %edx
	movl	%edx, -632(%rbp)
	movl	%ebx, %eax
	movq	$0, -84(%rbp)
	movups	%xmm0, -156(%rbp)
	movups	%xmm0, -140(%rbp)
	movups	%xmm0, -124(%rbp)
	movups	%xmm0, -108(%rbp)
	subl	%edi, %ebx
	je	.L331
	movl	_ZN2v88internalL4xmm0E(%rip), %edi
	leal	-1(%rbx), %r10d
	movl	%eax, -640(%rbp)
	xorl	%r12d, %r12d
	movl	%ebx, -624(%rbp)
	movq	%r10, %r15
	movl	%edi, -668(%rbp)
	movl	_ZN2v88internalL3rbpE(%rip), %edi
	movl	%edi, -644(%rbp)
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L438:
	cmpb	$1, %dl
	jne	.L338
	movzbl	(%rsi), %edx
	movzbl	4(%rcx), %ecx
	cmpb	$1, %dl
	je	.L360
	cmpb	$2, %dl
	je	.L361
	testb	%dl, %dl
	je	.L437
	.p2align 4,,10
	.p2align 3
.L338:
	leaq	1(%r12), %rdx
	cmpq	%r12, %r15
	je	.L433
.L440:
	movq	(%r14), %rcx
	movq	552(%r13), %rsi
	movq	%rdx, %r12
.L332:
	leaq	0(,%r12,8), %rdx
	addq	%rdx, %rcx
	addq	%rdx, %rsi
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	jne	.L438
	movzbl	(%rsi), %ebx
	cmpb	$1, %bl
	je	.L339
	cmpb	$2, %bl
	jne	.L338
	cmpb	$1, 1(%rsi)
	movq	-72(%rbp), %r9
	movslq	4(%rsi), %rdx
	je	.L439
	movq	$0, -455(%rbp)
	movq	%rdx, -463(%rbp)
.L354:
	movzbl	-448(%rbp), %edx
	movb	%bl, -464(%rbp)
	movdqa	-464(%rbp), %xmm3
	movb	%dl, -352(%rbp)
	movl	%r12d, %edx
	movaps	%xmm3, -368(%rbp)
	cmpl	%r12d, 720(%r9)
	ja	.L355
	leal	1(%r12), %ecx
	movl	%ecx, 720(%r9)
.L355:
	sall	$3, %edx
	movl	-644(%rbp), %esi
	leaq	-400(%rbp), %rdi
	movq	%r9, -664(%rbp)
	negl	%edx
	subl	$24, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-400(%rbp), %rsi
	cmpb	$1, %bl
	movl	-392(%rbp), %edx
	movq	-664(%rbp), %r9
	movq	%rsi, -584(%rbp)
	movl	%edx, -576(%rbp)
	movq	%rsi, -432(%rbp)
	movl	%edx, -424(%rbp)
	jne	.L356
	movl	-367(%rbp), %edi
	movl	$4, %r8d
	movabsq	$-4294967296, %rcx
	andq	-680(%rbp), %rcx
	orq	%rdi, %rcx
	movabsq	$-1095216660481, %rdi
	andq	%rdi, %rcx
	movabsq	$81604378624, %rdi
	orq	%rdi, %rcx
	movq	%r9, %rdi
	movq	%rcx, -680(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_9ImmediateEi@PLT
	leaq	1(%r12), %rdx
	cmpq	%r12, %r15
	jne	.L440
	.p2align 4,,10
	.p2align 3
.L433:
	movl	-640(%rbp), %eax
	movl	-624(%rbp), %ebx
.L331:
	movl	-620(%rbp), %edx
	testl	%edx, %edx
	je	.L401
	movl	%ebx, %edx
	subl	-632(%rbp), %edx
	movq	%r14, %r15
	movl	%eax, %r12d
	leal	0(,%rdx,8), %edi
	movl	_ZN2v88internalL4xmm0E(%rip), %esi
	movq	%r13, %r14
	movl	%edi, -624(%rbp)
	movl	_ZN2v88internalL3rbpE(%rip), %edi
	movl	%esi, -644(%rbp)
	movl	%edi, -620(%rbp)
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L368:
	movzbl	4(%rdi), %ecx
	movzbl	(%rsi), %edi
	cmpb	$1, %dil
	je	.L394
	cmpb	$2, %dil
	je	.L395
	leal	1(%rbx), %r9d
	testb	%dil, %dil
	je	.L441
.L369:
	movl	%r9d, %ebx
	cmpl	%r9d, %r12d
	je	.L401
.L402:
	movl	-616(%rbp), %edx
	movq	(%r15), %rsi
	movl	%ebx, %ecx
	subl	%r12d, %edx
	leaq	(%rsi,%rcx,8), %rdi
	movq	552(%r14), %rcx
	addl	%ebx, %edx
	movl	%edx, %esi
	leaq	(%rcx,%rsi,8), %rsi
	movzbl	(%rdi), %ecx
	testb	%cl, %cl
	je	.L367
	cmpb	$1, %cl
	je	.L368
	leal	1(%rbx), %r9d
.L453:
	movl	%r9d, %ebx
	cmpl	%r9d, %r12d
	jne	.L402
.L401:
	leaq	-336(%rbp), %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_119StackTransferRecipe12ExecuteMovesEv
	movl	-80(%rbp), %ebx
	leaq	-300(%rbp), %rax
	movq	%rax, -616(%rbp)
	testl	%ebx, %ebx
	je	.L330
	movl	_ZN2v88internalL4xmm0E(%rip), %eax
	movl	_ZN2v88internalL3rbpE(%rip), %r13d
	movl	%eax, -620(%rbp)
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L444:
	ja	.L442
	cmpb	$2, 1(%rax)
	movslq	4(%rax), %rdx
	movq	-72(%rbp), %rdi
	je	.L443
	pxor	%xmm0, %xmm0
	movb	$1, -400(%rbp)
	movups	%xmm0, -399(%rbp)
	movzbl	-384(%rbp), %eax
	movl	%edx, -399(%rbp)
	movdqa	-400(%rbp), %xmm1
	movb	%al, -352(%rbp)
	movaps	%xmm1, -368(%rbp)
	movl	-367(%rbp), %eax
	testl	%eax, %eax
	jne	.L408
	movl	$4, %r8d
	movl	%r15d, %ecx
	movl	%r15d, %edx
	movl	$51, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	.p2align 4,,10
	.p2align 3
.L405:
	movl	$-2, %eax
	movl	%r15d, %ecx
	roll	%cl, %eax
	andl	%eax, %ebx
	je	.L330
.L334:
	xorl	%r15d, %r15d
	movq	-616(%rbp), %rdi
	rep bsfl	%ebx, %r15d
	movslq	%r15d, %rax
	leaq	(%rdi,%rax,8), %rax
	movzbl	(%rax), %edx
	cmpb	$1, %dl
	jne	.L444
	movzbl	1(%rax), %r12d
	movl	4(%rax), %eax
	movl	$-24, %edx
	movl	%r13d, %esi
	leaq	-368(%rbp), %rdi
	movq	-72(%rbp), %r14
	sall	$3, %eax
	subl	%eax, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-368(%rbp), %rdx
	movl	-360(%rbp), %eax
	movq	%rdx, -500(%rbp)
	movl	%eax, -492(%rbp)
	movq	%rdx, -524(%rbp)
	movl	%eax, -516(%rbp)
	cmpb	$3, %r12b
	je	.L409
	ja	.L410
	cmpb	$1, %r12b
	jne	.L445
	movl	-360(%rbp), %ecx
	movl	$4, %r8d
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$-2, %eax
	movl	%r15d, %ecx
	roll	%cl, %eax
	andl	%eax, %ebx
	jne	.L334
	.p2align 4,,10
	.p2align 3
.L330:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L446
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L367:
	.cfi_restore_state
	movzbl	(%rsi), %ecx
	cmpb	$1, %cl
	je	.L370
	cmpb	$2, %cl
	je	.L371
	leal	1(%rbx), %r9d
	testb	%cl, %cl
	jne	.L369
	cmpl	-616(%rbp), %r12d
	je	.L369
	movl	-624(%rbp), %edi
	movzbl	1(%rsi), %ecx
	leal	0(,%rbx,8), %esi
	movl	%esi, %eax
	movq	-72(%rbp), %r13
	leal	-24(%rdi), %edx
	negl	%eax
	leaq	-368(%rbp), %rdi
	movb	%cl, -664(%rbp)
	subl	%esi, %edx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movl	%eax, -632(%rbp)
	movq	%rdi, -640(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-360(%rbp), %edx
	movl	-632(%rbp), %eax
	movq	-368(%rbp), %r8
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movq	-640(%rbp), %rdi
	movl	%edx, -564(%rbp)
	movl	%edx, -480(%rbp)
	leal	-24(%rax), %edx
	movq	%r8, -572(%rbp)
	movq	%r8, -488(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-368(%rbp), %rsi
	movl	-360(%rbp), %edx
	movzbl	-664(%rbp), %ecx
	movq	%rsi, -560(%rbp)
	movl	%edx, -552(%rbp)
	movq	%rsi, -476(%rbp)
	movl	%edx, -468(%rbp)
	cmpb	$9, %cl
	ja	.L346
	leaq	.L373(%rip), %rsi
	movslq	(%rsi,%rcx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal4wasm16LiftoffAssembler14MergeStackWithERKNS2_10CacheStateEj,"a",@progbits
	.align 4
	.align 4
.L373:
	.long	.L346-.L373
	.long	.L374-.L373
	.long	.L372-.L373
	.long	.L374-.L373
	.long	.L372-.L373
	.long	.L372-.L373
	.long	.L372-.L373
	.long	.L372-.L373
	.long	.L346-.L373
	.long	.L372-.L373
	.section	.text._ZN2v88internal4wasm16LiftoffAssembler14MergeStackWithERKNS2_10CacheStateEj
	.p2align 4,,10
	.p2align 3
.L442:
	subl	$2, %edx
	cmpb	$1, %dl
	ja	.L405
.L346:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L445:
	cmpb	$2, %r12b
	jne	.L346
	movl	-360(%rbp), %ecx
	movl	$8, %r8d
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L410:
	cmpb	$4, %r12b
	jne	.L346
	movq	%rdx, -512(%rbp)
	leal	-10(%r15), %esi
	movl	%eax, -504(%rbp)
	movq	%rdx, -488(%rbp)
	movl	%eax, -480(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L417
	subq	$8, %rsp
	movl	-620(%rbp), %ecx
	movq	%rdx, %r8
	movq	%r14, %rdi
	pushq	$0
	movl	-360(%rbp), %r9d
	pushq	$1
	pushq	$3
	movq	%rdx, -476(%rbp)
	movl	%esi, %edx
	movl	$16, %esi
	movl	%eax, -468(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L443:
	movq	$0, -391(%rbp)
	movzbl	-384(%rbp), %eax
	movl	%r15d, %esi
	movq	%rdx, -399(%rbp)
	movb	$2, -400(%rbp)
	movdqa	-400(%rbp), %xmm2
	movb	%al, -352(%rbp)
	movaps	%xmm2, -368(%rbp)
	movq	-367(%rbp), %rdx
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L409:
	movq	%rdx, -488(%rbp)
	leal	-10(%r15), %esi
	movl	%eax, -480(%rbp)
	movq	%rdx, -476(%rbp)
	movl	%eax, -468(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L415
	movl	-360(%rbp), %r9d
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movq	%rdx, %r8
	movq	%r14, %rdi
	movl	%esi, %edx
	movl	$16, %esi
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L417:
	movl	-480(%rbp), %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L415:
	movl	-468(%rbp), %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler5movssENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L408:
	movabsq	$-4294967296, %r14
	andq	-656(%rbp), %r14
	movl	$4, %ecx
	movl	%r15d, %esi
	orq	%r14, %rax
	movabsq	$-1095216660481, %r14
	andq	%rax, %r14
	movabsq	$81604378624, %rax
	orq	%rax, %r14
	movq	%r14, %rdx
	movq	%r14, -656(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L339:
	movzbl	4(%rsi), %eax
	movq	-72(%rbp), %r11
	movl	%r12d, %edx
	movzbl	1(%rsi), %ebx
	movb	%al, -645(%rbp)
	cmpl	%r12d, 720(%r11)
	jbe	.L447
.L341:
	sall	$3, %edx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	leaq	-368(%rbp), %rdi
	movq	%r11, -664(%rbp)
	negl	%edx
	subl	$24, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-368(%rbp), %rsi
	cmpb	$3, %bl
	movl	-360(%rbp), %edx
	movq	-664(%rbp), %r11
	movq	%rsi, -596(%rbp)
	movl	%edx, -588(%rbp)
	movq	%rsi, -608(%rbp)
	movl	%edx, -600(%rbp)
	je	.L342
	ja	.L343
	cmpb	$1, %bl
	je	.L344
	cmpb	$2, %bl
	jne	.L346
	movzbl	-645(%rbp), %ecx
	movl	$8, %r8d
	movq	%r11, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L343:
	cmpb	$4, %bl
	jne	.L346
	movzbl	-645(%rbp), %ebx
	movq	%rsi, -476(%rbp)
	movl	%edx, -468(%rbp)
	movq	%rsi, -432(%rbp)
	leal	-10(%rbx), %r10d
	movl	%edx, -424(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L351
	subq	$8, %rsp
	movl	-668(%rbp), %ecx
	movq	%rsi, %r8
	movq	%r11, %rdi
	pushq	$0
	movl	-360(%rbp), %r9d
	pushq	$1
	pushq	$3
	movq	%rsi, -400(%rbp)
	movl	$17, %esi
	movl	%edx, -392(%rbp)
	movl	%r10d, %edx
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L361:
	movzbl	%cl, %edi
	movl	4(%rsi), %r9d
	leaq	-300(%rbp,%rdi,8), %r8
	movl	$1, %edi
	sall	%cl, %edi
	orl	-80(%rbp), %edi
	cmpb	$1, 1(%rsi)
	movl	%edi, -80(%rbp)
	je	.L448
	movb	$0, (%r8)
	movb	%dl, 1(%r8)
	movl	%r9d, 4(%r8)
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L360:
	movzbl	4(%rsi), %edx
	cmpb	%dl, %cl
	je	.L338
	movzbl	1(%rsi), %r8d
	movl	-84(%rbp), %edi
	movl	$1, %esi
	sall	%cl, %esi
	testl	%esi, %edi
	jne	.L449
	orl	%edi, %esi
	leaq	-336(%rbp,%rcx,2), %rcx
	movl	%esi, -84(%rbp)
	movzbl	%dl, %esi
	addl	$1, -156(%rbp,%rsi,4)
	movb	%dl, (%rcx)
	movb	%r8b, 1(%rcx)
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L437:
	movl	$1, %edx
	movl	-80(%rbp), %edi
	sall	%cl, %edx
	testl	%edx, %edi
	jne	.L338
	movzbl	1(%rsi), %esi
	orl	%edi, %edx
	movl	%edx, -80(%rbp)
	leaq	-300(%rbp,%rcx,8), %rdx
	movb	$1, (%rdx)
	movb	%sil, 1(%rdx)
	movl	%r12d, 4(%rdx)
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L441:
	movl	$1, %edi
	movl	-80(%rbp), %r8d
	sall	%cl, %edi
	testl	%edi, %r8d
	jne	.L369
	movzbl	1(%rsi), %esi
	leaq	-300(%rbp,%rcx,8), %rcx
	orl	%r8d, %edi
	movl	%edi, -80(%rbp)
	movb	$1, (%rcx)
	movb	%sil, 1(%rcx)
	movl	%edx, 4(%rcx)
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L371:
	cmpb	$1, 1(%rsi)
	movq	-72(%rbp), %r13
	movslq	4(%rsi), %rdx
	je	.L450
	movq	$0, -423(%rbp)
	movq	%rdx, -431(%rbp)
.L388:
	movb	%cl, -432(%rbp)
	movzbl	-416(%rbp), %edx
	leal	1(%rbx), %r9d
	movdqa	-432(%rbp), %xmm4
	movb	%dl, -352(%rbp)
	movaps	%xmm4, -368(%rbp)
	cmpl	%ebx, 720(%r13)
	ja	.L389
	movl	%r9d, 720(%r13)
.L389:
	leal	0(,%rbx,8), %edx
	movl	-620(%rbp), %esi
	leaq	-400(%rbp), %rdi
	movl	%r9d, -640(%rbp)
	movl	%edx, %ebx
	movb	%cl, -632(%rbp)
	negl	%ebx
	leal	-24(%rbx), %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movzbl	-632(%rbp), %ecx
	movq	-400(%rbp), %rsi
	movl	-392(%rbp), %r10d
	movl	-640(%rbp), %r9d
	cmpb	$1, %cl
	movq	%rsi, -536(%rbp)
	movl	%r10d, -528(%rbp)
	movq	%rsi, -476(%rbp)
	movl	%r10d, -468(%rbp)
	jne	.L390
	movl	-367(%rbp), %ecx
	movl	$4, %r8d
	movq	%r13, %rdi
	movabsq	$-4294967296, %rdx
	andq	-688(%rbp), %rdx
	movl	%r9d, -632(%rbp)
	orq	%rcx, %rdx
	movabsq	$-1095216660481, %rcx
	andq	%rcx, %rdx
	movabsq	$81604378624, %rcx
	orq	%rcx, %rdx
	movq	%rdx, -688(%rbp)
	movq	%rdx, %rcx
	movl	%r10d, %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_9ImmediateEi@PLT
	movl	-632(%rbp), %r9d
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L370:
	movzbl	4(%rsi), %eax
	movq	-72(%rbp), %r10
	movzbl	1(%rsi), %r13d
	movb	%al, -640(%rbp)
	cmpl	%ebx, 720(%r10)
	jbe	.L451
.L376:
	leal	0(,%rbx,8), %edx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	leaq	-368(%rbp), %rdi
	movq	%r10, -632(%rbp)
	negl	%edx
	subl	$24, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-368(%rbp), %rsi
	cmpb	$3, %r13b
	movl	-360(%rbp), %edx
	movq	-632(%rbp), %r10
	movq	%rsi, -548(%rbp)
	movl	%edx, -540(%rbp)
	movq	%rsi, -512(%rbp)
	movl	%edx, -504(%rbp)
	je	.L377
	ja	.L378
	cmpb	$1, %r13b
	je	.L379
	cmpb	$2, %r13b
	jne	.L346
	movzbl	-640(%rbp), %ecx
	movl	$8, %r8d
	movq	%r10, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	leal	1(%rbx), %r9d
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L378:
	cmpb	$4, %r13b
	jne	.L346
	movzbl	-640(%rbp), %r13d
	movq	%rsi, -488(%rbp)
	movl	%edx, -480(%rbp)
	movq	%rsi, -476(%rbp)
	subl	$10, %r13d
	movl	%edx, -468(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L385
	subq	$8, %rsp
	movl	-644(%rbp), %ecx
	movq	%rsi, %r8
	movq	%r10, %rdi
	pushq	$0
	movl	-360(%rbp), %r9d
	pushq	$1
	pushq	$3
	movq	%rsi, -400(%rbp)
	movl	$17, %esi
	movl	%edx, -392(%rbp)
	movl	%r13d, %edx
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	leal	1(%rbx), %r9d
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L395:
	movzbl	%cl, %edx
	movl	4(%rsi), %r9d
	leaq	-300(%rbp,%rdx,8), %r8
	movl	$1, %edx
	sall	%cl, %edx
	orl	-80(%rbp), %edx
	cmpb	$1, 1(%rsi)
	movl	%edx, -80(%rbp)
	je	.L452
	movb	$0, (%r8)
	movb	%dil, 1(%r8)
	movl	%r9d, 4(%r8)
.L455:
	leal	1(%rbx), %r9d
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L394:
	movzbl	4(%rsi), %edx
	leal	1(%rbx), %r9d
	cmpb	%dl, %cl
	je	.L369
	movzbl	1(%rsi), %r8d
	movl	-84(%rbp), %edi
	movl	$1, %esi
	sall	%cl, %esi
	testl	%esi, %edi
	jne	.L454
	orl	%edi, %esi
	leaq	-336(%rbp,%rcx,2), %rcx
	movl	%esi, -84(%rbp)
	movzbl	%dl, %esi
	addl	$1, -156(%rbp,%rsi,4)
	movb	%dl, (%rcx)
	movb	%r8b, 1(%rcx)
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L447:
	leal	1(%r12), %esi
	movl	%esi, 720(%r11)
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L451:
	leal	1(%rbx), %r9d
	movl	%r9d, 720(%r10)
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L448:
	movl	$1, %edx
	movb	$0, (%r8)
	movb	%dl, 1(%r8)
	movl	%r9d, 4(%r8)
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L439:
	pxor	%xmm0, %xmm0
	movl	$1, %ebx
	movups	%xmm0, -463(%rbp)
	movl	%edx, -463(%rbp)
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L452:
	movl	$1, %edi
	movb	$0, (%r8)
	movb	%dil, 1(%r8)
	movl	%r9d, 4(%r8)
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L450:
	pxor	%xmm0, %xmm0
	movl	$1, %ecx
	movups	%xmm0, -431(%rbp)
	movl	%edx, -431(%rbp)
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L372:
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r9d
	movq	-488(%rbp), %rdx
	movl	$8, %r8d
	movq	%r13, %rdi
	movl	-480(%rbp), %ecx
	movl	%r9d, %esi
	movq	%rdx, -368(%rbp)
	movl	%ecx, -360(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-476(%rbp), %rsi
	movl	-468(%rbp), %edx
	movl	$8, %r8d
	movq	%rsi, -368(%rbp)
	movl	%edx, -360(%rbp)
.L435:
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r9d
	movq	%r13, %rdi
	movl	%r9d, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	leal	1(%rbx), %r9d
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L356:
	movq	-367(%rbp), %r11
	movl	$2147483648, %ecx
	movl	$4294967295, %edi
	addq	%r11, %rcx
	cmpq	%rdi, %rcx
	jbe	.L456
	movq	%r11, %rax
	shrq	$32, %rax
	jne	.L359
	movl	%r11d, %edx
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %ebx
	movq	%r9, %rdi
	movabsq	$-4294967296, %r11
	andq	-712(%rbp), %r11
	movl	$4, %ecx
	movq	%r9, -664(%rbp)
	orq	%rdx, %r11
	movl	%ebx, %esi
	movabsq	$-1095216660481, %rdx
	andq	%rdx, %r11
	movabsq	$81604378624, %rdx
	orq	%rdx, %r11
	movq	%r11, %rdx
	movq	%r11, -712(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movq	-664(%rbp), %r9
	movl	%ebx, %ecx
	movq	-432(%rbp), %rsi
	movl	-424(%rbp), %edx
	movl	$8, %r8d
	movq	%r9, %rdi
	movq	%rsi, -400(%rbp)
	movl	%edx, -392(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L390:
	movq	-367(%rbp), %rdx
	movl	$2147483648, %ecx
	movl	$4294967295, %edi
	addq	%rdx, %rcx
	cmpq	%rdi, %rcx
	jbe	.L457
	movq	%rdx, %rax
	movl	%r9d, -632(%rbp)
	shrq	$32, %rax
	jne	.L393
	movl	%edx, %ecx
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %ebx
	movq	%r13, %rdi
	movabsq	$-4294967296, %rdx
	andq	-720(%rbp), %rdx
	orq	%rcx, %rdx
	movl	%ebx, %esi
	movabsq	$-1095216660481, %rcx
	andq	%rcx, %rdx
	movabsq	$81604378624, %rcx
	orq	%rcx, %rdx
	movl	$4, %ecx
	movq	%rdx, -720(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movl	-468(%rbp), %edx
	movl	%ebx, %ecx
	movq	%r13, %rdi
	movq	-476(%rbp), %rsi
	movl	$8, %r8d
	movl	%edx, -392(%rbp)
	movq	%rsi, -400(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movl	-632(%rbp), %r9d
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L344:
	movzbl	-645(%rbp), %ecx
	movl	$4, %r8d
	movq	%r11, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L379:
	movzbl	-640(%rbp), %ecx
	movl	$4, %r8d
	movq	%r10, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	leal	1(%rbx), %r9d
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L393:
	movb	$19, -704(%rbp)
	movq	-704(%rbp), %rcx
	movl	$8, %r8d
	movq	%r13, %rdi
	movl	$10, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movq	-476(%rbp), %rsi
	movq	%r13, %rdi
	movl	-468(%rbp), %edx
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %ecx
	movl	$8, %r8d
	movq	%rsi, -400(%rbp)
	movl	%edx, -392(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movl	-632(%rbp), %r9d
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L359:
	movb	$19, -696(%rbp)
	movq	-696(%rbp), %rcx
	movq	%r11, %rdx
	movq	%r9, %rdi
	movl	$8, %r8d
	movl	$10, %esi
	movq	%r9, -664(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movq	-664(%rbp), %r9
	movq	-432(%rbp), %rsi
	movl	$8, %r8d
	movl	-424(%rbp), %edx
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %ecx
	movq	%r9, %rdi
	movq	%rsi, -400(%rbp)
	movl	%edx, -392(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L338
.L351:
	movl	%r10d, %ecx
	movq	%r11, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L338
.L385:
	movl	%r13d, %ecx
	movq	%r10, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_7OperandENS0_11XMMRegisterE@PLT
	leal	1(%rbx), %r9d
	jmp	.L369
.L374:
	movq	-488(%rbp), %r10
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r9d
	movl	$4, %r8d
	movq	%r13, %rdi
	movl	-480(%rbp), %ecx
	movq	%r10, %rdx
	movl	%r9d, %esi
	movq	%r10, -368(%rbp)
	movl	%ecx, -360(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-476(%rbp), %rsi
	movl	-468(%rbp), %edx
	movl	$4, %r8d
	movq	%rsi, -368(%rbp)
	movl	%edx, -360(%rbp)
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L342:
	movzbl	-645(%rbp), %ebx
	movq	%rsi, -432(%rbp)
	movl	%edx, -424(%rbp)
	movq	%rsi, -400(%rbp)
	leal	-10(%rbx), %r10d
	movl	%edx, -392(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L349
	movl	-360(%rbp), %r9d
	movq	%rsi, %r8
	movl	%r10d, %edx
	movq	%r11, %rdi
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	$17, %esi
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L377:
	movzbl	-640(%rbp), %r13d
	movq	%rsi, -476(%rbp)
	movl	%edx, -468(%rbp)
	movq	%rsi, -400(%rbp)
	subl	$10, %r13d
	movl	%edx, -392(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L383
	movl	-360(%rbp), %r9d
	movq	%rsi, %r8
	movl	%r13d, %edx
	movq	%r10, %rdi
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	$17, %esi
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	leal	1(%rbx), %r9d
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L454:
	cmpb	$4, %r8b
	jne	.L369
	movb	$4, -335(%rbp,%rcx,2)
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L449:
	cmpb	$4, %r8b
	jne	.L338
	movb	$4, -335(%rbp,%rcx,2)
	jmp	.L338
.L456:
	movl	%r11d, %ecx
	movl	$8, %r8d
	movq	%r9, %rdi
	movabsq	$-4294967296, %r11
	andq	-728(%rbp), %r11
	orq	%rcx, %r11
	movabsq	$-1095216660481, %rcx
	andq	%rcx, %r11
	movabsq	$81604378624, %rcx
	orq	%rcx, %r11
	movq	%r11, %rcx
	movq	%r11, -728(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L338
.L349:
	movl	%r10d, %ecx
	movq	%r11, %rdi
	call	_ZN2v88internal9Assembler5movssENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L338
.L457:
	movl	%edx, %ecx
	movl	$8, %r8d
	movabsq	$-4294967296, %rdx
	andq	-736(%rbp), %rdx
	orq	%rcx, %rdx
	movq	%r13, %rdi
	movl	%r9d, -632(%rbp)
	movabsq	$-1095216660481, %rcx
	andq	%rcx, %rdx
	movabsq	$81604378624, %rcx
	orq	%rcx, %rdx
	movq	%rdx, -736(%rbp)
	movq	%rdx, %rcx
	movl	%r10d, %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_9ImmediateEi@PLT
	movl	-632(%rbp), %r9d
	jmp	.L369
.L383:
	movl	%r13d, %ecx
	movq	%r10, %rdi
	call	_ZN2v88internal9Assembler5movssENS0_7OperandENS0_11XMMRegisterE@PLT
	leal	1(%rbx), %r9d
	jmp	.L369
.L446:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23204:
	.size	_ZN2v88internal4wasm16LiftoffAssembler14MergeStackWithERKNS2_10CacheStateEj, .-_ZN2v88internal4wasm16LiftoffAssembler14MergeStackWithERKNS2_10CacheStateEj
	.section	.text._ZN2v88internal4wasm16LiftoffAssembler20ParallelRegisterMoveENS0_6VectorINS2_25ParallelRegisterMoveTupleEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16LiftoffAssembler20ParallelRegisterMoveENS0_6VectorINS2_25ParallelRegisterMoveTupleEEE
	.type	_ZN2v88internal4wasm16LiftoffAssembler20ParallelRegisterMoveENS0_6VectorINS2_25ParallelRegisterMoveTupleEEE, @function
_ZN2v88internal4wasm16LiftoffAssembler20ParallelRegisterMoveENS0_6VectorINS2_25ParallelRegisterMoveTupleEEE:
.LFB23214:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	(%rdx,%rdx,2), %r8
	pxor	%xmm0, %xmm0
	movl	$1, %r10d
	addq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-336(%rbp), %r11
	leaq	-156(%rbp), %r9
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$456, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -92(%rbp)
	movq	$0, -84(%rbp)
	movq	%rdi, -72(%rbp)
	movups	%xmm0, -156(%rbp)
	movups	%xmm0, -140(%rbp)
	movups	%xmm0, -124(%rbp)
	movups	%xmm0, -108(%rbp)
	cmpq	%rsi, %r8
	jne	.L466
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L463:
	orl	%edi, %edx
	movl	%edx, -84(%rbp)
	movzbl	%al, %edx
	addl	$1, (%r9,%rdx,4)
	leaq	(%r11,%rcx,2), %rdx
	movb	%al, (%rdx)
	movb	%bl, 1(%rdx)
.L462:
	addq	$3, %rsi
	cmpq	%r8, %rsi
	je	.L465
.L466:
	movzbl	1(%rsi), %eax
	movzbl	(%rsi), %ecx
	cmpb	%al, %cl
	je	.L462
	movl	%r10d, %edi
	movl	-84(%rbp), %edx
	movzbl	2(%rsi), %ebx
	sall	%cl, %edi
	testl	%edi, %edx
	je	.L463
	cmpb	$4, %bl
	jne	.L462
	addq	$3, %rsi
	movb	$4, -335(%rbp,%rcx,2)
	cmpq	%r8, %rsi
	jne	.L466
	.p2align 4,,10
	.p2align 3
.L465:
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_119StackTransferRecipe12ExecuteMovesEv
	movl	-80(%rbp), %ebx
	leaq	-300(%rbp), %rax
	movq	%rax, -472(%rbp)
	testl	%ebx, %ebx
	je	.L458
	movl	_ZN2v88internalL4xmm0E(%rip), %eax
	movl	_ZN2v88internalL3rbpE(%rip), %r13d
	movl	%eax, -476(%rbp)
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L493:
	ja	.L491
	cmpb	$2, 1(%rax)
	movslq	4(%rax), %rdx
	movq	-72(%rbp), %rdi
	je	.L492
	pxor	%xmm0, %xmm0
	movb	$1, -400(%rbp)
	movups	%xmm0, -399(%rbp)
	movzbl	-384(%rbp), %eax
	movl	%edx, -399(%rbp)
	movdqa	-400(%rbp), %xmm1
	movb	%al, -352(%rbp)
	movaps	%xmm1, -368(%rbp)
	movl	-367(%rbp), %eax
	testl	%eax, %eax
	jne	.L473
	movl	$4, %r8d
	movl	%r15d, %ecx
	movl	%r15d, %edx
	movl	$51, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	.p2align 4,,10
	.p2align 3
.L469:
	movl	$-2, %eax
	movl	%r15d, %ecx
	roll	%cl, %eax
	andl	%eax, %ebx
	je	.L458
.L460:
	xorl	%r15d, %r15d
	movq	-472(%rbp), %rdi
	rep bsfl	%ebx, %r15d
	movslq	%r15d, %rax
	leaq	(%rdi,%rax,8), %rax
	movzbl	(%rax), %edx
	cmpb	$1, %dl
	jne	.L493
	movzbl	1(%rax), %r12d
	movl	4(%rax), %eax
	movl	$-24, %edx
	movl	%r13d, %esi
	leaq	-368(%rbp), %rdi
	movq	-72(%rbp), %r14
	sall	$3, %eax
	subl	%eax, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-368(%rbp), %rdx
	movl	-360(%rbp), %eax
	movq	%rdx, -436(%rbp)
	movl	%eax, -428(%rbp)
	movq	%rdx, -460(%rbp)
	movl	%eax, -452(%rbp)
	cmpb	$3, %r12b
	je	.L474
	ja	.L475
	cmpb	$1, %r12b
	jne	.L494
	movl	-360(%rbp), %ecx
	movl	$4, %r8d
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$-2, %eax
	movl	%r15d, %ecx
	roll	%cl, %eax
	andl	%eax, %ebx
	jne	.L460
	.p2align 4,,10
	.p2align 3
.L458:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L495
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L491:
	.cfi_restore_state
	subl	$2, %edx
	cmpb	$1, %dl
	ja	.L469
.L470:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L494:
	cmpb	$2, %r12b
	jne	.L470
	movl	-360(%rbp), %ecx
	movl	$8, %r8d
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L475:
	cmpb	$4, %r12b
	jne	.L470
	movq	%rdx, -448(%rbp)
	leal	-10(%r15), %esi
	movl	%eax, -440(%rbp)
	movq	%rdx, -424(%rbp)
	movl	%eax, -416(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L482
	subq	$8, %rsp
	movl	-476(%rbp), %ecx
	movq	%rdx, %r8
	movq	%r14, %rdi
	pushq	$0
	movl	-360(%rbp), %r9d
	pushq	$1
	pushq	$3
	movq	%rdx, -412(%rbp)
	movl	%esi, %edx
	movl	$16, %esi
	movl	%eax, -404(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L492:
	movq	$0, -391(%rbp)
	movzbl	-384(%rbp), %eax
	movl	%r15d, %esi
	movq	%rdx, -399(%rbp)
	movb	$2, -400(%rbp)
	movdqa	-400(%rbp), %xmm2
	movb	%al, -352(%rbp)
	movaps	%xmm2, -368(%rbp)
	movq	-367(%rbp), %rdx
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L474:
	movq	%rdx, -424(%rbp)
	leal	-10(%r15), %esi
	movl	%eax, -416(%rbp)
	movq	%rdx, -412(%rbp)
	movl	%eax, -404(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L480
	movl	-360(%rbp), %r9d
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movq	%rdx, %r8
	movq	%r14, %rdi
	movl	%esi, %edx
	movl	$16, %esi
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L482:
	movl	-416(%rbp), %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L480:
	movl	-404(%rbp), %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal9Assembler5movssENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L473:
	movabsq	$-4294967296, %r14
	andq	-488(%rbp), %r14
	movl	$4, %ecx
	movl	%r15d, %esi
	orq	%r14, %rax
	movabsq	$-1095216660481, %r14
	andq	%rax, %r14
	movabsq	$81604378624, %rax
	orq	%rax, %r14
	movq	%r14, %rdx
	movq	%r14, -488(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L469
.L495:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23214:
	.size	_ZN2v88internal4wasm16LiftoffAssembler20ParallelRegisterMoveENS0_6VectorINS2_25ParallelRegisterMoveTupleEEE, .-_ZN2v88internal4wasm16LiftoffAssembler20ParallelRegisterMoveENS0_6VectorINS2_25ParallelRegisterMoveTupleEEE
	.section	.text._ZN2v88internal4wasm16LiftoffAssembler21MoveToReturnRegistersEPNS0_9SignatureINS1_9ValueTypeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16LiftoffAssembler21MoveToReturnRegistersEPNS0_9SignatureINS1_9ValueTypeEEE
	.type	_ZN2v88internal4wasm16LiftoffAssembler21MoveToReturnRegistersEPNS0_9SignatureINS1_9ValueTypeEEE, @function
_ZN2v88internal4wasm16LiftoffAssembler21MoveToReturnRegistersEPNS0_9SignatureINS1_9ValueTypeEEE:
.LFB23215:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$440, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	560(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %rax
	movzbl	(%rax), %eax
	movq	$0, -92(%rbp)
	movq	$0, -84(%rbp)
	subl	$1, %eax
	movq	%rdi, -72(%rbp)
	cmpb	$2, %al
	movups	%xmm0, -156(%rbp)
	sbbq	%rcx, %rcx
	movups	%xmm0, -140(%rbp)
	notq	%rcx
	movups	%xmm0, -124(%rbp)
	andl	$88, %ecx
	cmpb	$2, %al
	movups	%xmm0, -108(%rbp)
	movzbl	-8(%r8), %r9d
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$22, %esi
	cmpb	$2, %al
	sbbl	%edx, %edx
	andl	$-2047, %edx
	addl	$2048, %edx
	cmpb	$2, %al
	sbbl	%eax, %eax
	notl	%eax
	andl	$11, %eax
	cmpb	$1, %r9b
	je	.L498
	cmpb	$2, %r9b
	je	.L499
	leaq	-336(%rbp), %r10
	testb	%r9b, %r9b
	je	.L530
.L500:
	movq	%r10, %rdi
	leaq	-300(%rbp), %r14
	call	_ZN2v88internal4wasm12_GLOBAL__N_119StackTransferRecipe12ExecuteMovesEv
	movl	-80(%rbp), %r13d
	testl	%r13d, %r13d
	je	.L496
	movl	_ZN2v88internalL4xmm0E(%rip), %r15d
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L533:
	ja	.L531
	cmpb	$2, 1(%rax)
	movslq	4(%rax), %rdx
	movq	-72(%rbp), %rdi
	je	.L532
	pxor	%xmm0, %xmm0
	movb	$1, -400(%rbp)
	movups	%xmm0, -399(%rbp)
	movzbl	-384(%rbp), %eax
	movl	%edx, -399(%rbp)
	movdqa	-400(%rbp), %xmm2
	movb	%al, -352(%rbp)
	movaps	%xmm2, -368(%rbp)
	movl	-367(%rbp), %eax
	testl	%eax, %eax
	jne	.L510
	movl	$4, %r8d
	movl	%r12d, %ecx
	movl	%r12d, %edx
	movl	$51, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	.p2align 4,,10
	.p2align 3
.L506:
	movl	$-2, %eax
	movl	%r12d, %ecx
	roll	%cl, %eax
	andl	%eax, %r13d
	je	.L496
.L503:
	xorl	%r12d, %r12d
	rep bsfl	%r13d, %r12d
	movslq	%r12d, %rax
	leaq	(%r14,%rax,8), %rax
	movzbl	(%rax), %edx
	cmpb	$1, %dl
	jne	.L533
	movzbl	1(%rax), %ecx
	movl	4(%rax), %eax
	movl	$-24, %edx
	leaq	-368(%rbp), %rdi
	movq	-72(%rbp), %r10
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	sall	$3, %eax
	movb	%cl, -465(%rbp)
	subl	%eax, %edx
	movq	%r10, -480(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movzbl	-465(%rbp), %ecx
	movq	-368(%rbp), %rdx
	movl	-360(%rbp), %eax
	movq	-480(%rbp), %r10
	cmpb	$3, %cl
	movq	%rdx, -436(%rbp)
	movl	%eax, -428(%rbp)
	movq	%rdx, -460(%rbp)
	movl	%eax, -452(%rbp)
	je	.L511
	ja	.L512
	cmpb	$1, %cl
	jne	.L534
	movl	-360(%rbp), %ecx
	movl	$4, %r8d
	movl	%r12d, %esi
	movq	%r10, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$-2, %eax
	movl	%r12d, %ecx
	roll	%cl, %eax
	andl	%eax, %r13d
	jne	.L503
	.p2align 4,,10
	.p2align 3
.L496:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L535
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L532:
	.cfi_restore_state
	movq	$0, -391(%rbp)
	movzbl	-384(%rbp), %eax
	movl	%r12d, %esi
	movq	%rdx, -399(%rbp)
	movb	$2, -400(%rbp)
	movdqa	-400(%rbp), %xmm1
	movb	%al, -352(%rbp)
	movaps	%xmm1, -368(%rbp)
	movq	-367(%rbp), %rdx
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L531:
	subl	$2, %edx
	cmpb	$1, %dl
	ja	.L506
.L507:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L534:
	cmpb	$2, %cl
	jne	.L507
	movl	-360(%rbp), %ecx
	movl	$8, %r8d
	movl	%r12d, %esi
	movq	%r10, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L512:
	cmpb	$4, %cl
	jne	.L507
	movq	%rdx, -448(%rbp)
	leal	-10(%r12), %esi
	movl	%eax, -440(%rbp)
	movq	%rdx, -424(%rbp)
	movl	%eax, -416(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L519
	subq	$8, %rsp
	movq	%rdx, %r8
	movl	%r15d, %ecx
	movq	%r10, %rdi
	pushq	$0
	movl	-360(%rbp), %r9d
	pushq	$1
	pushq	$3
	movq	%rdx, -412(%rbp)
	movl	%esi, %edx
	movl	$16, %esi
	movl	%eax, -404(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L511:
	movq	%rdx, -424(%rbp)
	leal	-10(%r12), %esi
	movl	%eax, -416(%rbp)
	movq	%rdx, -412(%rbp)
	movl	%eax, -404(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L517
	movl	-360(%rbp), %r9d
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movq	%rdx, %r8
	movq	%r10, %rdi
	movl	%esi, %edx
	movl	$16, %esi
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L519:
	movl	-416(%rbp), %ecx
	movq	%r10, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L517:
	movl	-404(%rbp), %ecx
	movq	%r10, %rdi
	call	_ZN2v88internal9Assembler5movssENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L510:
	movabsq	$-4294967296, %rsi
	movl	$4, %ecx
	andq	%rsi, %rbx
	movl	%r12d, %esi
	orq	%rax, %rbx
	movabsq	$-1095216660481, %rax
	andq	%rax, %rbx
	movabsq	$81604378624, %rax
	orq	%rax, %rbx
	movq	%rbx, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L530:
	movzbl	-7(%r8), %esi
	subq	552(%rdi), %r8
	leaq	-300(%rbp,%rcx), %rax
	movl	%edx, -80(%rbp)
	sarq	$3, %r8
	movb	$1, (%rax)
	subl	$1, %r8d
	movb	%sil, 1(%rax)
	movl	%r8d, 4(%rax)
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L499:
	cmpb	$1, -7(%r8)
	movl	-4(%r8), %esi
	leaq	-300(%rbp,%rcx), %rax
	movl	%edx, -80(%rbp)
	je	.L536
.L502:
	movb	$0, (%rax)
	leaq	-336(%rbp), %r10
	movb	%r9b, 1(%rax)
	movl	%esi, 4(%rax)
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L498:
	movzbl	-4(%r8), %ecx
	leaq	-336(%rbp), %r10
	cmpb	%al, %cl
	je	.L500
	movzbl	-7(%r8), %edi
	addq	%r10, %rsi
	movzbl	%cl, %eax
	movl	%edx, -84(%rbp)
	addl	$1, -156(%rbp,%rax,4)
	movb	%cl, (%rsi)
	movb	%dil, 1(%rsi)
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L536:
	movl	$1, %r9d
	jmp	.L502
.L535:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23215:
	.size	_ZN2v88internal4wasm16LiftoffAssembler21MoveToReturnRegistersEPNS0_9SignatureINS1_9ValueTypeEEE, .-_ZN2v88internal4wasm16LiftoffAssembler21MoveToReturnRegistersEPNS0_9SignatureINS1_9ValueTypeEEE
	.section	.text._ZN2v88internal4wasm16LiftoffAssembler16SpillOneRegisterENS1_14LiftoffRegListES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16LiftoffAssembler16SpillOneRegisterENS1_14LiftoffRegListES3_
	.type	_ZN2v88internal4wasm16LiftoffAssembler16SpillOneRegisterENS1_14LiftoffRegListES3_, @function
_ZN2v88internal4wasm16LiftoffAssembler16SpillOneRegisterENS1_14LiftoffRegListES3_:
.LFB23216:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	notl	%ebx
	andl	%esi, %ebx
	subq	$120, %rsp
	movl	716(%rdi), %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%edi, %ecx
	notl	%ecx
	andl	%ebx, %ecx
	jne	.L538
	movl	$0, 716(%r15)
	xorl	%edi, %edi
	testl	%ebx, %ebx
	jne	.L558
	movb	$32, -129(%rbp)
	movl	$32, %ecx
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L558:
	movl	%ebx, %ecx
.L538:
	rep bsfl	%ecx, %ecx
	movl	$1, %ebx
	movb	%cl, -129(%rbp)
	sall	%cl, %ebx
	orl	%ebx, %edi
.L539:
	movslq	%ecx, %rcx
	movl	%edi, 716(%r15)
	movq	552(%r15), %rdx
	leaq	(%r15,%rcx,4), %rax
	movq	560(%r15), %r12
	movl	%ebx, -152(%rbp)
	movq	%r15, %rbx
	movq	%rax, -160(%rbp)
	movl	644(%rax), %eax
	subq	%rdx, %r12
	movl	720(%r15), %esi
	movl	%eax, -136(%rbp)
	leaq	-68(%rbp), %rax
	sarq	$3, %r12
	movq	%rax, -144(%rbp)
	movl	_ZN2v88internalL4xmm0E(%rip), %eax
	subl	$1, %r12d
	movl	%eax, -148(%rbp)
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L544:
	movzbl	%r13b, %ecx
	movl	$4, %r8d
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
.L548:
	subl	$1, -136(%rbp)
	movb	$0, (%r14)
	je	.L553
.L559:
	movq	552(%rbx), %rdx
	movl	720(%rbx), %esi
.L540:
	subl	$1, %r12d
.L554:
	movl	%r12d, %ecx
	leaq	(%rdx,%rcx,8), %r14
	cmpb	$1, (%r14)
	jne	.L540
	movzbl	4(%r14), %r13d
	cmpb	%r13b, -129(%rbp)
	jne	.L540
	movzbl	1(%r14), %r15d
	cmpl	%esi, %r12d
	jb	.L541
	leal	1(%r12), %edx
	movl	%edx, 720(%rbx)
.L541:
	leal	0(,%r12,8), %edx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movq	-144(%rbp), %rdi
	negl	%edx
	subl	$24, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%rsi, -104(%rbp)
	movl	%edx, -96(%rbp)
	movq	%rsi, -128(%rbp)
	movl	%edx, -120(%rbp)
	cmpb	$3, %r15b
	je	.L542
	ja	.L543
	cmpb	$1, %r15b
	je	.L544
	cmpb	$2, %r15b
	jne	.L546
	movzbl	%r13b, %ecx
	movl	$8, %r8d
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	subl	$1, -136(%rbp)
	movb	$0, (%r14)
	jne	.L559
.L553:
	movq	-160(%rbp), %rax
	movq	%rbx, %r15
	movl	-152(%rbp), %ebx
	movl	$0, 644(%rax)
	notl	%ebx
	andl	%ebx, 640(%r15)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L560
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L543:
	.cfi_restore_state
	cmpb	$4, %r15b
	jne	.L546
	movzbl	%r13b, %r10d
	movq	%rsi, -116(%rbp)
	movl	%edx, -108(%rbp)
	subl	$10, %r10d
	movq	%rsi, -92(%rbp)
	movl	%edx, -84(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L551
	subq	$8, %rsp
	movl	-60(%rbp), %r9d
	movq	%rsi, -80(%rbp)
	movq	%rsi, %r8
	pushq	$0
	movl	-148(%rbp), %ecx
	movl	$17, %esi
	movq	%rbx, %rdi
	pushq	$1
	pushq	$3
	movl	%edx, -72(%rbp)
	movl	%r10d, %edx
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L542:
	movzbl	%r13b, %r10d
	movq	%rsi, -92(%rbp)
	movl	%edx, -84(%rbp)
	subl	$10, %r10d
	movq	%rsi, -80(%rbp)
	movl	%edx, -72(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L549
	movl	-60(%rbp), %r9d
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movq	%rsi, %r8
	movl	%r10d, %edx
	movl	$17, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L551:
	movl	%r10d, %ecx
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L549:
	movl	%r10d, %ecx
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler5movssENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L548
.L546:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L560:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23216:
	.size	_ZN2v88internal4wasm16LiftoffAssembler16SpillOneRegisterENS1_14LiftoffRegListES3_, .-_ZN2v88internal4wasm16LiftoffAssembler16SpillOneRegisterENS1_14LiftoffRegListES3_
	.section	.text._ZN2v88internal4wasm16LiftoffAssembler13PopToRegisterENS1_14LiftoffRegListE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16LiftoffAssembler13PopToRegisterENS1_14LiftoffRegListE
	.type	_ZN2v88internal4wasm16LiftoffAssembler13PopToRegisterENS1_14LiftoffRegListE, @function
_ZN2v88internal4wasm16LiftoffAssembler13PopToRegisterENS1_14LiftoffRegListE:
.LFB23202:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	560(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	-8(%rcx), %edi
	movq	-8(%rcx), %r14
	subq	$8, %rcx
	movzbl	1(%rcx), %r13d
	movq	%rcx, 560(%r12)
	cmpb	$1, %dil
	je	.L562
	movl	%esi, %edx
	cmpb	$2, %dil
	je	.L563
	testb	%dil, %dil
	je	.L591
.L564:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L562:
	movq	%r14, %rax
	shrq	$32, %rax
	movzbl	%al, %edx
	movzbl	%al, %ecx
	leaq	(%r12,%rdx,4), %rsi
	subl	$1, 644(%rsi)
	je	.L592
	.p2align 4,,10
	.p2align 3
.L578:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L593
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L591:
	.cfi_restore_state
	leal	-1(%r13), %eax
	movl	$719, %esi
	cmpb	$1, %al
	jbe	.L565
	leal	-3(%r13), %eax
	cmpb	$2, %al
	sbbl	%esi, %esi
	andl	$260401, %esi
	addl	$719, %esi
.L565:
	movl	640(%r12), %eax
	orl	%edx, %eax
	notl	%eax
	andl	%esi, %eax
	je	.L566
	xorl	%ebx, %ebx
	rep bsfl	%eax, %ebx
.L567:
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	subq	552(%r12), %rcx
	movl	$-24, %edx
	leaq	-64(%rbp), %rdi
	subl	%ecx, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-64(%rbp), %rdx
	movl	-56(%rbp), %eax
	cmpb	$3, %r13b
	je	.L568
	ja	.L569
	cmpb	$1, %r13b
	je	.L570
	cmpb	$2, %r13b
	jne	.L564
	movl	-56(%rbp), %ecx
	movzbl	%bl, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	%ebx, %eax
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L569:
	cmpb	$4, %r13b
	jne	.L564
	movzbl	%bl, %ecx
	movl	%eax, -100(%rbp)
	leal	-10(%rcx), %esi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L576
	subq	$8, %rsp
	movl	-56(%rbp), %r9d
	movq	%rdx, %r8
	movq	%r12, %rdi
	pushq	$0
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%esi, %edx
	movl	$16, %esi
	pushq	$1
	pushq	$3
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	movl	%ebx, %eax
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L563:
	movl	640(%r12), %eax
	orl	%esi, %eax
	notl	%eax
	andl	$719, %eax
	je	.L580
	xorl	%esi, %esi
	rep bsfl	%eax, %esi
	movl	%esi, %ebx
.L581:
	movq	%r14, %rax
	sarq	$32, %rax
	cmpb	$1, %r13b
	je	.L594
	movq	$0, -87(%rbp)
	movq	%r12, %rdi
	movq	%rax, -95(%rbp)
	movzbl	-80(%rbp), %eax
	movb	$2, -96(%rbp)
	movdqa	-96(%rbp), %xmm1
	movb	%al, -48(%rbp)
	movaps	%xmm1, -64(%rbp)
	movq	-63(%rbp), %rdx
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	movl	%ebx, %eax
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L592:
	movl	$-2, %edx
	roll	%cl, %edx
	andl	%edx, 640(%r12)
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L570:
	movl	-56(%rbp), %ecx
	movzbl	%bl, %esi
	movl	$4, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	%ebx, %eax
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L594:
	pxor	%xmm0, %xmm0
	movb	$1, -96(%rbp)
	movups	%xmm0, -95(%rbp)
	movl	%eax, -95(%rbp)
	movzbl	-80(%rbp), %eax
	movdqa	-96(%rbp), %xmm2
	movb	%al, -48(%rbp)
	movaps	%xmm2, -64(%rbp)
	movl	-63(%rbp), %eax
	testl	%eax, %eax
	jne	.L595
	movl	%esi, %ecx
	movl	%esi, %edx
	movl	$4, %r8d
	movl	$51, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	%ebx, %eax
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L580:
	movl	$719, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm16LiftoffAssembler16SpillOneRegisterENS1_14LiftoffRegListES3_
	movzbl	%al, %esi
	movl	%esi, %ebx
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L566:
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm16LiftoffAssembler16SpillOneRegisterENS1_14LiftoffRegListES3_
	movq	560(%r12), %rcx
	movl	%eax, %ebx
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L568:
	movzbl	%bl, %ecx
	movl	%eax, -88(%rbp)
	leal	-10(%rcx), %esi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L574
	movl	-56(%rbp), %r9d
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movq	%rdx, %r8
	movq	%r12, %rdi
	movl	%esi, %edx
	movl	$16, %esi
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	movl	%ebx, %eax
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L574:
	movl	-88(%rbp), %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5movssENS0_11XMMRegisterENS0_7OperandE@PLT
	movl	%ebx, %eax
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L576:
	movl	-100(%rbp), %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_11XMMRegisterENS0_7OperandE@PLT
	movl	%ebx, %eax
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L595:
	movl	%eax, %edx
	movl	$4, %ecx
	movq	%r12, %rdi
	movabsq	$81604378624, %rax
	orq	%rax, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movl	%ebx, %eax
	jmp	.L578
.L593:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23202:
	.size	_ZN2v88internal4wasm16LiftoffAssembler13PopToRegisterENS1_14LiftoffRegListE, .-_ZN2v88internal4wasm16LiftoffAssembler13PopToRegisterENS1_14LiftoffRegListE
	.section	.text._ZN2v88internal4wasm16LiftoffAssembler13SpillRegisterENS1_15LiftoffRegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16LiftoffAssembler13SpillRegisterENS1_15LiftoffRegisterE
	.type	_ZN2v88internal4wasm16LiftoffAssembler13SpillRegisterENS1_15LiftoffRegisterE, @function
_ZN2v88internal4wasm16LiftoffAssembler13SpillRegisterENS1_15LiftoffRegisterE:
.LFB23217:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%sil, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movb	%al, -129(%rbp)
	leaq	(%rdi,%rax,4), %rax
	movq	552(%rdi), %rsi
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -160(%rbp)
	movl	644(%rax), %eax
	movl	%eax, -136(%rbp)
	movq	560(%rdi), %rax
	movl	720(%rdi), %edi
	subq	%rsi, %rax
	sarq	$3, %rax
	leal	-1(%rax), %r14d
	leaq	-68(%rbp), %rax
	movq	%rax, -144(%rbp)
	movl	_ZN2v88internalL4xmm0E(%rip), %eax
	movl	%eax, -148(%rbp)
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L601:
	movzbl	%bl, %ecx
	movl	$4, %r8d
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
.L605:
	subl	$1, -136(%rbp)
	movb	$0, (%r12)
	je	.L610
.L614:
	movq	552(%r15), %rsi
	movl	720(%r15), %edi
.L597:
	subl	$1, %r14d
.L611:
	movl	%r14d, %eax
	leaq	(%rsi,%rax,8), %r12
	cmpb	$1, (%r12)
	jne	.L597
	movzbl	4(%r12), %ebx
	cmpb	%bl, -129(%rbp)
	jne	.L597
	movzbl	1(%r12), %r13d
	cmpl	%edi, %r14d
	jb	.L598
	leal	1(%r14), %edx
	movl	%edx, 720(%r15)
.L598:
	leal	0(,%r14,8), %edx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movq	-144(%rbp), %rdi
	negl	%edx
	subl	$24, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%rsi, -104(%rbp)
	movl	%edx, -96(%rbp)
	movq	%rsi, -128(%rbp)
	movl	%edx, -120(%rbp)
	cmpb	$3, %r13b
	je	.L599
	ja	.L600
	cmpb	$1, %r13b
	je	.L601
	cmpb	$2, %r13b
	jne	.L603
	movzbl	%bl, %ecx
	movl	$8, %r8d
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	subl	$1, -136(%rbp)
	movb	$0, (%r12)
	jne	.L614
.L610:
	movq	-160(%rbp), %rax
	movzbl	-129(%rbp), %ecx
	movl	$0, 644(%rax)
	movl	$-2, %eax
	roll	%cl, %eax
	andl	%eax, 640(%r15)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L615
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L600:
	.cfi_restore_state
	cmpb	$4, %r13b
	jne	.L603
	movzbl	%bl, %ecx
	movq	%rsi, -116(%rbp)
	movl	%edx, -108(%rbp)
	leal	-10(%rcx), %r11d
	movq	%rsi, -92(%rbp)
	movl	%edx, -84(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L608
	subq	$8, %rsp
	movl	-60(%rbp), %r9d
	movq	%rsi, -80(%rbp)
	movq	%rsi, %r8
	pushq	$0
	movl	-148(%rbp), %ecx
	movl	$17, %esi
	movq	%r15, %rdi
	pushq	$1
	pushq	$3
	movl	%edx, -72(%rbp)
	movl	%r11d, %edx
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L599:
	movzbl	%bl, %ecx
	movq	%rsi, -92(%rbp)
	movl	%edx, -84(%rbp)
	leal	-10(%rcx), %r11d
	movq	%rsi, -80(%rbp)
	movl	%edx, -72(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L606
	movl	-60(%rbp), %r9d
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movq	%rsi, %r8
	movl	%r11d, %edx
	movl	$17, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L608:
	movl	%r11d, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L606:
	movl	%r11d, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5movssENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L605
.L603:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L615:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23217:
	.size	_ZN2v88internal4wasm16LiftoffAssembler13SpillRegisterENS1_15LiftoffRegisterE, .-_ZN2v88internal4wasm16LiftoffAssembler13SpillRegisterENS1_15LiftoffRegisterE
	.section	.text._ZN2v88internal4wasm16LiftoffAssembler14set_num_localsEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16LiftoffAssembler14set_num_localsEj
	.type	_ZN2v88internal4wasm16LiftoffAssembler14set_num_localsEj, @function
_ZN2v88internal4wasm16LiftoffAssembler14set_num_localsEj:
.LFB23218:
	.cfi_startproc
	endbr64
	movl	%esi, 540(%rdi)
	cmpl	$8, %esi
	ja	.L622
	ret
	.p2align 4,,10
	.p2align 3
.L622:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	%esi, %edi
	subq	$8, %rsp
	call	malloc@PLT
	movq	%rax, 544(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23218:
	.size	_ZN2v88internal4wasm16LiftoffAssembler14set_num_localsEj, .-_ZN2v88internal4wasm16LiftoffAssembler14set_num_localsEj
	.section	.rodata._ZN2v88internal4wasmlsERSoNS1_16LiftoffAssembler8VarStateE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"<stmt>"
.LC2:
	.string	"<bot>"
.LC3:
	.string	"<unknown>"
.LC4:
	.string	"i64"
.LC5:
	.string	"i32"
.LC6:
	.string	"f64"
.LC7:
	.string	"anyref"
.LC8:
	.string	"funcref"
.LC9:
	.string	"nullref"
.LC10:
	.string	"exn"
.LC11:
	.string	"s128"
.LC12:
	.string	"f32"
.LC13:
	.string	":"
.LC14:
	.string	"s"
.LC15:
	.string	"c"
	.section	.text._ZN2v88internal4wasmlsERSoNS1_16LiftoffAssembler8VarStateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasmlsERSoNS1_16LiftoffAssembler8VarStateE
	.type	_ZN2v88internal4wasmlsERSoNS1_16LiftoffAssembler8VarStateE, @function
_ZN2v88internal4wasmlsERSoNS1_16LiftoffAssembler8VarStateE:
.LFB23219:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	movzbl	%bh, %eax
	subq	$8, %rsp
	cmpb	$10, %al
	ja	.L624
	leaq	.L626(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasmlsERSoNS1_16LiftoffAssembler8VarStateE,"a",@progbits
	.align 4
	.align 4
.L626:
	.long	.L636-.L626
	.long	.L645-.L626
	.long	.L634-.L626
	.long	.L633-.L626
	.long	.L632-.L626
	.long	.L631-.L626
	.long	.L630-.L626
	.long	.L629-.L626
	.long	.L628-.L626
	.long	.L627-.L626
	.long	.L625-.L626
	.section	.text._ZN2v88internal4wasmlsERSoNS1_16LiftoffAssembler8VarStateE
	.p2align 4,,10
	.p2align 3
.L633:
	movl	$3, %edx
	leaq	.LC12(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L635:
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$1, %bl
	je	.L637
	cmpb	$2, %bl
	jne	.L648
	movq	%r12, %rdi
	leaq	.LC15(%rip), %rsi
	movl	$1, %edx
	sarq	$32, %rbx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%rbx, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSolsEi@PLT
	.p2align 4,,10
	.p2align 3
.L645:
	.cfi_restore_state
	movl	$3, %edx
	leaq	.LC5(%rip), %rsi
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L632:
	movl	$3, %edx
	leaq	.LC6(%rip), %rsi
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L631:
	movl	$4, %edx
	leaq	.LC11(%rip), %rsi
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L634:
	movl	$3, %edx
	leaq	.LC4(%rip), %rsi
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L630:
	movl	$6, %edx
	leaq	.LC7(%rip), %rsi
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L629:
	movl	$7, %edx
	leaq	.LC8(%rip), %rsi
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L628:
	movl	$7, %edx
	leaq	.LC9(%rip), %rsi
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L627:
	movl	$3, %edx
	leaq	.LC10(%rip), %rsi
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L625:
	movl	$5, %edx
	leaq	.LC2(%rip), %rsi
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L636:
	movl	$6, %edx
	leaq	.LC1(%rip), %rsi
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L648:
	testb	%bl, %bl
	je	.L649
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L637:
	shrq	$32, %rbx
	movzbl	%bl, %eax
	cmpb	$9, %bl
	ja	.L641
	leaq	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names(%rip), %rdx
	movq	(%rdx,%rax,8), %r13
	testq	%r13, %r13
	je	.L647
.L644:
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L623:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L649:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L641:
	.cfi_restore_state
	subl	$10, %eax
	leaq	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names(%rip), %rdx
	cltq
	movq	(%rdx,%rax,8), %r13
	testq	%r13, %r13
	jne	.L644
.L647:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L623
.L624:
	movl	$9, %edx
	leaq	.LC3(%rip), %rsi
	jmp	.L635
	.cfi_endproc
.LFE23219:
	.size	_ZN2v88internal4wasmlsERSoNS1_16LiftoffAssembler8VarStateE, .-_ZN2v88internal4wasmlsERSoNS1_16LiftoffAssembler8VarStateE
	.section	.text._ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,"axG",@progbits,_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.type	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, @function
_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E:
.LFB25556:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L658
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L652:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L652
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L658:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE25556:
	.size	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, .-_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.section	.text._ZN2v84base11SmallVectorINS_8internal4wasm16LiftoffAssembler8VarStateELm8EE4GrowEm,"axG",@progbits,_ZN2v84base11SmallVectorINS_8internal4wasm16LiftoffAssembler8VarStateELm8EE4GrowEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base11SmallVectorINS_8internal4wasm16LiftoffAssembler8VarStateELm8EE4GrowEm
	.type	_ZN2v84base11SmallVectorINS_8internal4wasm16LiftoffAssembler8VarStateELm8EE4GrowEm, @function
_ZN2v84base11SmallVectorINS_8internal4wasm16LiftoffAssembler8VarStateELm8EE4GrowEm:
.LFB26471:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	8(%rdi), %r12
	movq	16(%rdi), %rdi
	subq	%rax, %r12
	subq	%rax, %rdi
	sarq	$3, %rdi
	addq	%rdi, %rdi
	cmpq	%rsi, %rdi
	cmovb	%rsi, %rdi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo64Em@PLT
	leaq	0(,%rax,8), %r14
	movq	%r14, %rdi
	call	malloc@PLT
	movq	(%rbx), %r15
	movq	%r12, %rdx
	movq	%rax, %rdi
	movq	%rax, %r13
	movq	%r15, %rsi
	call	memcpy@PLT
	leaq	24(%rbx), %rax
	cmpq	%rax, %r15
	je	.L662
	movq	%r15, %rdi
	call	free@PLT
.L662:
	leaq	0(%r13,%r12), %rax
	movq	%r13, (%rbx)
	addq	%r14, %r13
	movq	%r13, 16(%rbx)
	movq	%rax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26471:
	.size	_ZN2v84base11SmallVectorINS_8internal4wasm16LiftoffAssembler8VarStateELm8EE4GrowEm, .-_ZN2v84base11SmallVectorINS_8internal4wasm16LiftoffAssembler8VarStateELm8EE4GrowEm
	.section	.text._ZN2v84base11SmallVectorINS_8internal4wasm16LiftoffAssembler8VarStateELm8EE4GrowEv,"axG",@progbits,_ZN2v84base11SmallVectorINS_8internal4wasm16LiftoffAssembler8VarStateELm8EE4GrowEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base11SmallVectorINS_8internal4wasm16LiftoffAssembler8VarStateELm8EE4GrowEv
	.type	_ZN2v84base11SmallVectorINS_8internal4wasm16LiftoffAssembler8VarStateELm8EE4GrowEv, @function
_ZN2v84base11SmallVectorINS_8internal4wasm16LiftoffAssembler8VarStateELm8EE4GrowEv:
.LFB25689:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	_ZN2v84base11SmallVectorINS_8internal4wasm16LiftoffAssembler8VarStateELm8EE4GrowEm
	.cfi_endproc
.LFE25689:
	.size	_ZN2v84base11SmallVectorINS_8internal4wasm16LiftoffAssembler8VarStateELm8EE4GrowEv, .-_ZN2v84base11SmallVectorINS_8internal4wasm16LiftoffAssembler8VarStateELm8EE4GrowEv
	.section	.text._ZN2v88internal4wasm16LiftoffAssembler10FinishCallEPNS0_9SignatureINS1_9ValueTypeEEEPNS0_8compiler14CallDescriptorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16LiftoffAssembler10FinishCallEPNS0_9SignatureINS1_9ValueTypeEEEPNS0_8compiler14CallDescriptorE
	.type	_ZN2v88internal4wasm16LiftoffAssembler10FinishCallEPNS0_9SignatureINS1_9ValueTypeEEEPNS0_8compiler14CallDescriptorE, @function
_ZN2v88internal4wasm16LiftoffAssembler10FinishCallEPNS0_9SignatureINS1_9ValueTypeEEEPNS0_8compiler14CallDescriptorE:
.LFB23212:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rsi)
	je	.L672
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	16(%rsi), %rax
	movzbl	(%rax), %r12d
	leal	-1(%r12), %eax
	cmpb	$1, %al
	jbe	.L675
	leal	-3(%r12), %eax
	cmpb	$1, %al
	ja	.L676
	movq	24(%rdx), %rax
	movq	16(%rax), %rax
	movl	(%rax), %ecx
	sarl	%ecx
	addl	$10, %ecx
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L675:
	movq	24(%rdx), %rax
	movq	16(%rax), %rax
	movl	(%rax), %ecx
	sarl	%ecx
.L668:
	movl	$1, %eax
	sall	%cl, %eax
	orl	%eax, 640(%rbx)
	movzbl	%cl, %eax
	addl	$1, 644(%rbx,%rax,4)
	movq	560(%rbx), %rax
	cmpq	568(%rbx), %rax
	je	.L677
.L670:
	movb	$1, (%rax)
	addq	$8, %rax
	movb	%r12b, -7(%rax)
	movb	%cl, -4(%rax)
	movq	%rax, 560(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L672:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L676:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L677:
	leaq	552(%rbx), %rdi
	movb	%cl, -17(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal4wasm16LiftoffAssembler8VarStateELm8EE4GrowEv
	movzbl	-17(%rbp), %ecx
	jmp	.L670
	.cfi_endproc
.LFE23212:
	.size	_ZN2v88internal4wasm16LiftoffAssembler10FinishCallEPNS0_9SignatureINS1_9ValueTypeEEEPNS0_8compiler14CallDescriptorE, .-_ZN2v88internal4wasm16LiftoffAssembler10FinishCallEPNS0_9SignatureINS1_9ValueTypeEEEPNS0_8compiler14CallDescriptorE
	.section	.text._ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	.type	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E, @function
_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E:
.LFB26492:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L693
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L682:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r12), %rdi
	movq	16(%r12), %rbx
	testq	%rdi, %rdi
	je	.L680
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L678
.L681:
	movq	%rbx, %r12
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L680:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L681
.L678:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L693:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE26492:
	.size	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E, .-_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	.section	.text._ZN2v88internal9AssemblerD0Ev,"axG",@progbits,_ZN2v88internal9AssemblerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9AssemblerD0Ev
	.type	_ZN2v88internal9AssemblerD0Ev, @function
_ZN2v88internal9AssemblerD0Ev:
.LFB23177:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	480(%rdi), %r13
	leaq	464(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L700
.L697:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L697
.L700:
	movq	424(%r12), %r13
	leaq	408(%r12), %r14
	testq	%r13, %r13
	je	.L698
.L699:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L703
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L698
.L704:
	movq	%rbx, %r13
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L703:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L704
.L698:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L702
	movq	400(%r12), %rax
	movq	368(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L705
	.p2align 4,,10
	.p2align 3
.L706:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L706
	movq	328(%r12), %rdi
.L705:
	call	_ZdlPv@PLT
.L702:
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L707
	movq	312(%r12), %rax
	movq	280(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L708
	.p2align 4,,10
	.p2align 3
.L709:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L709
	movq	240(%r12), %rdi
.L708:
	call	_ZdlPv@PLT
.L707:
	movq	%r12, %rdi
	call	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE23177:
	.size	_ZN2v88internal9AssemblerD0Ev, .-_ZN2v88internal9AssemblerD0Ev
	.section	.text._ZN2v88internal4wasm16LiftoffAssemblerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16LiftoffAssemblerD0Ev
	.type	_ZN2v88internal4wasm16LiftoffAssemblerD0Ev, @function
_ZN2v88internal4wasm16LiftoffAssemblerD0Ev:
.LFB23201:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpl	$8, 540(%rdi)
	ja	.L765
.L731:
	movq	552(%r12), %rdi
	leaq	576(%r12), %rax
	cmpq	%rax, %rdi
	je	.L732
	call	free@PLT
.L732:
	movq	480(%r12), %r13
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	leaq	464(%r12), %rbx
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L736
.L733:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L733
.L736:
	movq	424(%r12), %r13
	leaq	408(%r12), %r14
	testq	%r13, %r13
	je	.L734
.L735:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L739
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L734
.L740:
	movq	%rbx, %r13
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L739:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L740
.L734:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L738
	movq	400(%r12), %rax
	movq	368(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L741
	.p2align 4,,10
	.p2align 3
.L742:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L742
	movq	328(%r12), %rdi
.L741:
	call	_ZdlPv@PLT
.L738:
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L743
	movq	312(%r12), %rax
	movq	280(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L744
	.p2align 4,,10
	.p2align 3
.L745:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L745
	movq	240(%r12), %rdi
.L744:
	call	_ZdlPv@PLT
.L743:
	movq	%r12, %rdi
	call	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L765:
	.cfi_restore_state
	movq	544(%rdi), %rdi
	call	free@PLT
	jmp	.L731
	.cfi_endproc
.LFE23201:
	.size	_ZN2v88internal4wasm16LiftoffAssemblerD0Ev, .-_ZN2v88internal4wasm16LiftoffAssemblerD0Ev
	.section	.text._ZN2v88internal9AssemblerD2Ev,"axG",@progbits,_ZN2v88internal9AssemblerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9AssemblerD2Ev
	.type	_ZN2v88internal9AssemblerD2Ev, @function
_ZN2v88internal9AssemblerD2Ev:
.LFB23175:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	480(%rdi), %r13
	leaq	464(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L770
.L767:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L767
.L770:
	movq	424(%r12), %r13
	leaq	408(%r12), %r14
	testq	%r13, %r13
	je	.L768
.L769:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L773
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L768
.L774:
	movq	%rbx, %r13
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L773:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L774
.L768:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L772
	movq	400(%r12), %rax
	movq	368(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L775
	.p2align 4,,10
	.p2align 3
.L776:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L776
	movq	328(%r12), %rdi
.L775:
	call	_ZdlPv@PLT
.L772:
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L777
	movq	312(%r12), %rax
	movq	280(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L778
	.p2align 4,,10
	.p2align 3
.L779:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L779
	movq	240(%r12), %rdi
.L778:
	call	_ZdlPv@PLT
.L777:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	.cfi_endproc
.LFE23175:
	.size	_ZN2v88internal9AssemblerD2Ev, .-_ZN2v88internal9AssemblerD2Ev
	.weak	_ZN2v88internal9AssemblerD1Ev
	.set	_ZN2v88internal9AssemblerD1Ev,_ZN2v88internal9AssemblerD2Ev
	.section	.text._ZN2v88internal4wasm16LiftoffAssemblerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16LiftoffAssemblerD2Ev
	.type	_ZN2v88internal4wasm16LiftoffAssemblerD2Ev, @function
_ZN2v88internal4wasm16LiftoffAssemblerD2Ev:
.LFB23199:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpl	$8, 540(%rdi)
	ja	.L835
.L801:
	movq	552(%r12), %rdi
	leaq	576(%r12), %rax
	cmpq	%rax, %rdi
	je	.L802
	call	free@PLT
.L802:
	movq	480(%r12), %r13
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	leaq	464(%r12), %rbx
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L806
.L803:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L803
.L806:
	movq	424(%r12), %r13
	leaq	408(%r12), %r14
	testq	%r13, %r13
	je	.L804
.L805:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L809
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L804
.L810:
	movq	%rbx, %r13
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L809:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L810
.L804:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L808
	movq	400(%r12), %rax
	movq	368(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L811
	.p2align 4,,10
	.p2align 3
.L812:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L812
	movq	328(%r12), %rdi
.L811:
	call	_ZdlPv@PLT
.L808:
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L813
	movq	312(%r12), %rax
	movq	280(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L814
	.p2align 4,,10
	.p2align 3
.L815:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L815
	movq	240(%r12), %rdi
.L814:
	call	_ZdlPv@PLT
.L813:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L835:
	.cfi_restore_state
	movq	544(%rdi), %rdi
	call	free@PLT
	jmp	.L801
	.cfi_endproc
.LFE23199:
	.size	_ZN2v88internal4wasm16LiftoffAssemblerD2Ev, .-_ZN2v88internal4wasm16LiftoffAssemblerD2Ev
	.globl	_ZN2v88internal4wasm16LiftoffAssemblerD1Ev
	.set	_ZN2v88internal4wasm16LiftoffAssemblerD1Ev,_ZN2v88internal4wasm16LiftoffAssemblerD2Ev
	.section	.text._ZN2v84base11SmallVectorINS_8internal4wasm17LiftoffStackSlots4SlotELm8EE4GrowEm,"axG",@progbits,_ZN2v84base11SmallVectorINS_8internal4wasm17LiftoffStackSlots4SlotELm8EE4GrowEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base11SmallVectorINS_8internal4wasm17LiftoffStackSlots4SlotELm8EE4GrowEm
	.type	_ZN2v84base11SmallVectorINS_8internal4wasm17LiftoffStackSlots4SlotELm8EE4GrowEm, @function
_ZN2v84base11SmallVectorINS_8internal4wasm17LiftoffStackSlots4SlotELm8EE4GrowEm:
.LFB26739:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	8(%rdi), %r12
	movq	16(%rdi), %rdi
	subq	%rax, %r12
	subq	%rax, %rdi
	sarq	$4, %rdi
	addq	%rdi, %rdi
	cmpq	%rsi, %rdi
	cmovb	%rsi, %rdi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo64Em@PLT
	salq	$4, %rax
	movq	%rax, %rdi
	movq	%rax, %r14
	call	malloc@PLT
	movq	(%rbx), %r15
	movq	%r12, %rdx
	movq	%rax, %rdi
	movq	%rax, %r13
	movq	%r15, %rsi
	call	memcpy@PLT
	leaq	24(%rbx), %rax
	cmpq	%rax, %r15
	je	.L837
	movq	%r15, %rdi
	call	free@PLT
.L837:
	leaq	0(%r13,%r12), %rax
	movq	%r13, (%rbx)
	addq	%r14, %r13
	movq	%r13, 16(%rbx)
	movq	%rax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26739:
	.size	_ZN2v84base11SmallVectorINS_8internal4wasm17LiftoffStackSlots4SlotELm8EE4GrowEm, .-_ZN2v84base11SmallVectorINS_8internal4wasm17LiftoffStackSlots4SlotELm8EE4GrowEm
	.section	.text._ZN2v84base11SmallVectorINS_8internal4wasm17LiftoffStackSlots4SlotELm8EE4GrowEv,"axG",@progbits,_ZN2v84base11SmallVectorINS_8internal4wasm17LiftoffStackSlots4SlotELm8EE4GrowEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base11SmallVectorINS_8internal4wasm17LiftoffStackSlots4SlotELm8EE4GrowEv
	.type	_ZN2v84base11SmallVectorINS_8internal4wasm17LiftoffStackSlots4SlotELm8EE4GrowEv, @function
_ZN2v84base11SmallVectorINS_8internal4wasm17LiftoffStackSlots4SlotELm8EE4GrowEv:
.LFB25691:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	_ZN2v84base11SmallVectorINS_8internal4wasm17LiftoffStackSlots4SlotELm8EE4GrowEm
	.cfi_endproc
.LFE25691:
	.size	_ZN2v84base11SmallVectorINS_8internal4wasm17LiftoffStackSlots4SlotELm8EE4GrowEv, .-_ZN2v84base11SmallVectorINS_8internal4wasm17LiftoffStackSlots4SlotELm8EE4GrowEv
	.section	.text._ZN2v88internal4wasm16LiftoffAssembler11PrepareCallEPNS0_9SignatureINS1_9ValueTypeEEEPNS0_8compiler14CallDescriptorEPNS0_8RegisterESB_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16LiftoffAssembler11PrepareCallEPNS0_9SignatureINS1_9ValueTypeEEEPNS0_8compiler14CallDescriptorEPNS0_8RegisterESB_
	.type	_ZN2v88internal4wasm16LiftoffAssembler11PrepareCallEPNS0_9SignatureINS1_9ValueTypeEEEPNS0_8compiler14CallDescriptorEPNS0_8RegisterESB_, @function
_ZN2v88internal4wasm16LiftoffAssembler11PrepareCallEPNS0_9SignatureINS1_9ValueTypeEEEPNS0_8compiler14CallDescriptorEPNS0_8RegisterESB_:
.LFB23208:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r10
	movq	%rsi, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$840, %rsp
	.cfi_offset 3, -56
	movq	%r8, -792(%rbp)
	movq	552(%rdi), %rdx
	movq	%rcx, -784(%rbp)
	movq	560(%rdi), %r8
	subq	%rdx, %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	sarq	$3, %r8
	movq	%rax, -768(%rbp)
	subl	%eax, %r8d
	je	.L841
	leaq	-336(%rbp), %rax
	movq	%rsi, -800(%rbp)
	leal	-1(%r8), %r14d
	xorl	%ebx, %ebx
	movq	%rax, -776(%rbp)
	movl	_ZN2v88internalL4xmm0E(%rip), %eax
	movq	%r10, -808(%rbp)
	movl	%eax, -844(%rbp)
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L842:
	leaq	1(%rbx), %rcx
	cmpq	%rbx, %r14
	je	.L964
.L936:
	movq	%rcx, %rbx
.L855:
	leaq	(%rdx,%rbx,8), %r15
	cmpb	$1, (%r15)
	jne	.L842
	movzbl	4(%r15), %eax
	movzbl	1(%r15), %r13d
	movl	%ebx, %edx
	movb	%al, -760(%rbp)
	cmpl	%ebx, 720(%r12)
	ja	.L843
	leal	1(%rbx), %esi
	movl	%esi, 720(%r12)
.L843:
	sall	$3, %edx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movq	-776(%rbp), %rdi
	negl	%edx
	subl	$24, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-336(%rbp), %rsi
	movl	-328(%rbp), %edx
	movq	%rsi, -748(%rbp)
	movl	%edx, -740(%rbp)
	movq	%rsi, -604(%rbp)
	movl	%edx, -596(%rbp)
	cmpb	$3, %r13b
	je	.L844
	ja	.L845
	cmpb	$1, %r13b
	jne	.L965
	movzbl	-760(%rbp), %ecx
	movl	$4, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
.L850:
	movb	$0, (%r15)
	leaq	1(%rbx), %rcx
	movq	552(%r12), %rdx
	cmpq	%rbx, %r14
	jne	.L936
.L964:
	movq	560(%r12), %r8
	movq	-800(%rbp), %r11
	movq	-808(%rbp), %r10
	subq	%rdx, %r8
	sarq	$3, %r8
	subl	-768(%rbp), %r8d
.L841:
	leaq	-472(%rbp), %rax
	movq	%r12, -344(%rbp)
	movq	-792(%rbp), %rbx
	movq	%rax, %xmm0
	movq	%rax, -800(%rbp)
	leaq	-344(%rbp), %rax
	movq	%rax, -480(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movq	24(%r10), %rax
	movaps	%xmm0, -496(%rbp)
	pxor	%xmm0, %xmm0
	movq	$0, -92(%rbp)
	movq	$0, -84(%rbp)
	movq	%r12, -72(%rbp)
	movups	%xmm0, -156(%rbp)
	movups	%xmm0, -140(%rbp)
	movups	%xmm0, -124(%rbp)
	movups	%xmm0, -108(%rbp)
	movq	(%rax), %rsi
	movq	16(%rax), %rcx
	movl	(%rcx,%rsi,8), %ecx
	movl	$1, %esi
	sarl	%ecx
	movl	%ecx, -808(%rbp)
	movl	%ecx, %edi
	sall	%cl, %esi
	testq	%rbx, %rbx
	je	.L856
	movl	(%rbx), %ecx
	movl	%edi, %ebx
	cmpl	%edi, %ecx
	je	.L856
	movzbl	%cl, %edi
	movl	%esi, -84(%rbp)
	addl	$1, -156(%rbp,%rdi,4)
	movzbl	%bl, %edi
	leaq	-336(%rbp,%rdi,2), %rdi
	movb	%cl, (%rdi)
	movb	$2, 1(%rdi)
.L856:
	movq	8(%rax), %rax
	leal	1(%rax), %r13d
	movq	-768(%rbp), %rax
	testl	%eax, %eax
	je	.L857
	movq	%r12, %r9
	leal	-1(%rax), %ebx
	leaq	-496(%rbp), %rdi
	movq	%r10, %r12
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L967:
	movq	16(%r11), %rax
	addq	%rbx, %rax
	addq	(%r11), %rax
	movzbl	(%rax), %eax
	leal	-1(%rax), %edx
	cmpb	$1, %dl
	jbe	.L862
	subl	$3, %eax
	cmpb	$1, %al
	ja	.L963
	sarl	%ecx
	addl	$10, %ecx
.L864:
	movl	$1, %eax
	movzbl	(%r14), %edx
	sall	%cl, %eax
	orl	%eax, %esi
	cmpb	$1, %dl
	je	.L865
	cmpb	$2, %dl
	je	.L866
	testb	%dl, %dl
	je	.L966
.L872:
	subq	$1, %rbx
	cmpq	$-1, %rbx
	je	.L859
.L969:
	movq	552(%r9), %rdx
.L858:
	leal	(%r8,%rbx), %eax
	movl	16(%r12), %ecx
	movq	%rax, %r15
	leaq	(%rdx,%rax,8), %r14
	subl	$1, %r13d
	je	.L860
	movq	24(%r12), %rdx
	movl	%r13d, %eax
	addq	(%rdx), %rax
	movq	16(%rdx), %rdx
	movl	-8(%rdx,%rax,8), %ecx
.L860:
	testb	$1, %cl
	je	.L967
	movq	-488(%rbp), %rax
	cmpq	-480(%rbp), %rax
	je	.L968
.L873:
	movq	(%r14), %rdx
	addq	$16, %rax
	subq	$1, %rbx
	movl	%r15d, -8(%rax)
	movb	$0, -4(%rax)
	movq	%rdx, -16(%rax)
	movq	%rax, -488(%rbp)
	cmpq	$-1, %rbx
	jne	.L969
.L859:
	cmpq	$0, -784(%rbp)
	movq	-488(%rbp), %rbx
	movq	%r9, %r12
	je	.L874
.L934:
	movq	-784(%rbp), %rax
	movl	(%rax), %r13d
	btl	%r13d, %esi
	jc	.L970
.L874:
	movq	-496(%rbp), %r13
	movabsq	$81604378624, %r15
	cmpq	%rbx, %r13
	je	.L894
	movq	%r12, -760(%rbp)
	movq	-872(%rbp), %r14
	movq	%r13, %r12
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L973:
	testb	%dl, %dl
	je	.L971
.L883:
	addq	$16, %r12
	cmpq	%r12, %rbx
	je	.L972
.L895:
	movzbl	(%r12), %edx
	cmpb	$1, %dl
	je	.L881
	cmpb	$2, %dl
	jne	.L973
	movl	4(%r12), %edx
	movq	-344(%rbp), %rdi
	addq	$16, %r12
	movabsq	$-4294967296, %rax
	andq	%rax, %r14
	movabsq	$-1095216660481, %rax
	orq	%rdx, %r14
	andq	%rax, %r14
	orq	%r15, %r14
	movq	%r14, %rsi
	call	_ZN2v88internal9Assembler5pushqENS0_9ImmediateE@PLT
	cmpq	%r12, %rbx
	jne	.L895
.L972:
	movq	-760(%rbp), %r12
.L894:
	leaq	-336(%rbp), %rax
	leaq	-300(%rbp), %rbx
	movq	%rax, %rdi
	movq	%rax, -776(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_119StackTransferRecipe12ExecuteMovesEv
	movl	-80(%rbp), %r15d
	testl	%r15d, %r15d
	je	.L880
	movl	_ZN2v88internalL4xmm0E(%rip), %eax
	movq	%r12, -784(%rbp)
	movl	%eax, -760(%rbp)
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L977:
	ja	.L974
	cmpb	$2, 1(%rdx)
	movslq	4(%rdx), %rax
	movq	-72(%rbp), %rdi
	je	.L975
	pxor	%xmm0, %xmm0
	movb	$1, -592(%rbp)
	movups	%xmm0, -591(%rbp)
	movl	%eax, -591(%rbp)
	movzbl	-576(%rbp), %eax
	movdqa	-592(%rbp), %xmm1
	movb	%al, -512(%rbp)
	movaps	%xmm1, -528(%rbp)
	movl	-527(%rbp), %eax
	testl	%eax, %eax
	jne	.L903
	movl	$4, %r8d
	movl	%r14d, %ecx
	movl	%r14d, %edx
	movl	$51, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	.p2align 4,,10
	.p2align 3
.L900:
	movl	$-2, %eax
	movl	%r14d, %ecx
	roll	%cl, %eax
	andl	%eax, %r15d
	je	.L976
.L879:
	xorl	%r14d, %r14d
	rep bsfl	%r15d, %r14d
	movslq	%r14d, %rax
	leaq	(%rbx,%rax,8), %rdx
	movzbl	(%rdx), %eax
	cmpb	$1, %al
	jne	.L977
	movl	4(%rdx), %ecx
	movzbl	1(%rdx), %r12d
	movl	$-24, %edx
	leaq	-528(%rbp), %rdi
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movq	-72(%rbp), %r13
	sall	$3, %ecx
	subl	%ecx, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-528(%rbp), %rdx
	movl	-520(%rbp), %ecx
	movq	%rdx, -688(%rbp)
	movl	%ecx, -680(%rbp)
	movq	%rdx, -640(%rbp)
	movl	%ecx, -632(%rbp)
	cmpb	$3, %r12b
	je	.L904
	ja	.L905
	cmpb	$1, %r12b
	jne	.L978
	movl	-520(%rbp), %ecx
	movl	$4, %r8d
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$-2, %eax
	movl	%r14d, %ecx
	roll	%cl, %eax
	andl	%eax, %r15d
	jne	.L879
	.p2align 4,,10
	.p2align 3
.L976:
	movq	-784(%rbp), %r12
.L880:
	movl	-768(%rbp), %eax
	pxor	%xmm0, %xmm0
	movl	$0, -80(%rbp)
	movl	$0, 640(%r12)
	movq	$0, 708(%r12)
	salq	$3, %rax
	subq	%rax, 560(%r12)
	cmpq	$0, -792(%rbp)
	movups	%xmm0, 644(%r12)
	movups	%xmm0, 660(%r12)
	movups	%xmm0, 676(%r12)
	movups	%xmm0, 692(%r12)
	je	.L979
.L897:
	movq	-776(%rbp), %rdi
	leaq	-300(%rbp), %rbx
	call	_ZN2v88internal4wasm12_GLOBAL__N_119StackTransferRecipe12ExecuteMovesEv
	movl	-80(%rbp), %r15d
	testl	%r15d, %r15d
	je	.L933
	movl	_ZN2v88internalL4xmm0E(%rip), %eax
	movl	%eax, -760(%rbp)
	jmp	.L914
	.p2align 4,,10
	.p2align 3
.L982:
	ja	.L980
	cmpb	$2, 1(%rdx)
	movslq	4(%rdx), %rax
	movq	-72(%rbp), %rdi
	je	.L981
	pxor	%xmm0, %xmm0
	movb	$1, -560(%rbp)
	movups	%xmm0, -559(%rbp)
	movl	%eax, -559(%rbp)
	movzbl	-544(%rbp), %eax
	movdqa	-560(%rbp), %xmm2
	movb	%al, -512(%rbp)
	movaps	%xmm2, -528(%rbp)
	movl	-527(%rbp), %eax
	testl	%eax, %eax
	jne	.L922
	movl	$4, %r8d
	movl	%r14d, %ecx
	movl	%r14d, %edx
	movl	$51, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	.p2align 4,,10
	.p2align 3
.L919:
	movl	$-2, %eax
	movl	%r14d, %ecx
	roll	%cl, %eax
	andl	%eax, %r15d
	je	.L933
.L914:
	xorl	%r14d, %r14d
	rep bsfl	%r15d, %r14d
	movslq	%r14d, %rax
	leaq	(%rbx,%rax,8), %rdx
	movzbl	(%rdx), %eax
	cmpb	$1, %al
	jne	.L982
	movl	4(%rdx), %ecx
	movzbl	1(%rdx), %r12d
	movl	$-24, %edx
	leaq	-528(%rbp), %rdi
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movq	-72(%rbp), %r13
	sall	$3, %ecx
	subl	%ecx, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-528(%rbp), %rdx
	movl	-520(%rbp), %ecx
	movq	%rdx, -628(%rbp)
	movl	%ecx, -620(%rbp)
	movq	%rdx, -652(%rbp)
	movl	%ecx, -644(%rbp)
	cmpb	$3, %r12b
	je	.L923
	ja	.L924
	cmpb	$1, %r12b
	jne	.L983
	movl	-520(%rbp), %ecx
	movl	$4, %r8d
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$-2, %eax
	movl	%r14d, %ecx
	roll	%cl, %eax
	andl	%eax, %r15d
	jne	.L914
	.p2align 4,,10
	.p2align 3
.L933:
	movq	-496(%rbp), %rdi
	cmpq	-800(%rbp), %rdi
	je	.L840
	call	free@PLT
.L840:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L984
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L845:
	.cfi_restore_state
	cmpb	$4, %r13b
	jne	.L963
	movzbl	-760(%rbp), %r13d
	movq	%rsi, -560(%rbp)
	movl	%edx, -552(%rbp)
	movq	%rsi, -528(%rbp)
	subl	$10, %r13d
	movl	%edx, -520(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L853
	subq	$8, %rsp
	movl	-844(%rbp), %ecx
	movq	%rsi, %r8
	movq	%r12, %rdi
	pushq	$0
	movl	-328(%rbp), %r9d
	pushq	$1
	pushq	$3
	movq	%rsi, -496(%rbp)
	movl	$17, %esi
	movl	%edx, -488(%rbp)
	movl	%r13d, %edx
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L974:
	subl	$2, %eax
	cmpb	$1, %al
	ja	.L900
	.p2align 4,,10
	.p2align 3
.L963:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L965:
	cmpb	$2, %r13b
	jne	.L963
	movzbl	-760(%rbp), %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L862:
	sarl	%ecx
	jmp	.L864
	.p2align 4,,10
	.p2align 3
.L966:
	movl	-80(%rbp), %edx
	testl	%edx, %eax
	jne	.L872
	movzbl	1(%r14), %r10d
	orl	%edx, %eax
	movzbl	%cl, %ecx
	movl	%eax, -80(%rbp)
	leaq	-300(%rbp,%rcx,8), %rax
	movb	$1, (%rax)
	movb	%r10b, 1(%rax)
	movl	%r15d, 4(%rax)
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L866:
	orl	-80(%rbp), %eax
	movzbl	%cl, %ecx
	cmpb	$1, 1(%r14)
	movl	4(%r14), %r10d
	leaq	-300(%rbp,%rcx,8), %rcx
	movl	%eax, -80(%rbp)
	jne	.L871
	movl	$1, %edx
.L871:
	movb	$0, (%rcx)
	movb	%dl, 1(%rcx)
	movl	%r10d, 4(%rcx)
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L865:
	movzbl	4(%r14), %edx
	cmpb	%dl, %cl
	je	.L872
	movl	-84(%rbp), %r10d
	movzbl	1(%r14), %r14d
	testl	%r10d, %eax
	jne	.L985
	orl	%r10d, %eax
	movzbl	%cl, %ecx
	movl	%eax, -84(%rbp)
	movzbl	%dl, %eax
	addl	$1, -156(%rbp,%rax,4)
	leaq	-336(%rbp,%rcx,2), %rax
	movb	%dl, (%rax)
	movb	%r14b, 1(%rax)
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L881:
	movzbl	1(%r12), %edx
	movzbl	4(%r12), %r9d
	movq	-344(%rbp), %r10
	cmpb	$3, %dl
	je	.L885
	ja	.L886
	subl	$1, %edx
	cmpb	$1, %dl
	ja	.L963
	movzbl	%r9b, %esi
	movq	%r10, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L971:
	movl	8(%r12), %eax
	movl	$-24, %edx
	movq	-344(%rbp), %r13
	leaq	-528(%rbp), %rdi
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	leal	0(,%rax,8), %ecx
	subl	%ecx, %edx
	cmpb	$1, 1(%r12)
	je	.L986
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-528(%rbp), %rsi
	movl	-520(%rbp), %edx
	movq	%r13, %rdi
	movq	%rsi, -736(%rbp)
	movl	%edx, -728(%rbp)
	movq	%rsi, -712(%rbp)
	movl	%edx, -704(%rbp)
	call	_ZN2v88internal9Assembler5pushqENS0_7OperandE@PLT
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L886:
	cmpb	$4, %dl
	jne	.L963
	movq	%r10, %rdi
	movl	$8, %r8d
	movabsq	$-4294967296, %rdx
	andq	-824(%rbp), %rdx
	orq	$8, %rdx
	movl	$5, %esi
	movabsq	$-1095216660481, %rax
	movq	%r10, -784(%rbp)
	andq	%rax, %rdx
	movb	%r9b, -776(%rbp)
	orq	%r15, %rdx
	movq	%rdx, %rcx
	movq	%rdx, -824(%rbp)
	movl	$4, %edx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movzbl	-776(%rbp), %r9d
	xorl	%edx, %edx
	movl	_ZN2v88internalL3rspE(%rip), %esi
	leaq	-604(%rbp), %rdi
	leal	-10(%r9), %r11d
	movl	%r11d, -776(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-604(%rbp), %r8
	movl	-596(%rbp), %edx
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	movl	-776(%rbp), %r11d
	movq	%r8, -616(%rbp)
	movq	-784(%rbp), %r10
	movl	%edx, -608(%rbp)
	movq	%r8, -592(%rbp)
	movl	%edx, -584(%rbp)
	je	.L892
	subq	$8, %rsp
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, %r9d
	movq	%r10, %rdi
	pushq	$0
	movl	$17, %esi
	pushq	$1
	pushq	$3
	movl	%edx, -552(%rbp)
	movl	%edx, -520(%rbp)
	movl	%r11d, %edx
	movq	%r8, -560(%rbp)
	movq	%r8, -528(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L978:
	cmpb	$2, %r12b
	jne	.L963
	movl	-520(%rbp), %ecx
	movl	$8, %r8d
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L905:
	cmpb	$4, %r12b
	jne	.L963
	movq	%rdx, -616(%rbp)
	leal	-10(%r14), %esi
	movl	%ecx, -608(%rbp)
	movq	%rdx, -604(%rbp)
	movl	%ecx, -596(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L912
	subq	$8, %rsp
	movl	%ecx, -552(%rbp)
	movq	%rdx, %r8
	movq	%r13, %rdi
	movl	-520(%rbp), %r9d
	movl	-760(%rbp), %ecx
	pushq	$0
	pushq	$1
	pushq	$3
	movq	%rdx, -560(%rbp)
	movl	%esi, %edx
	movl	$16, %esi
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L975:
	movq	$0, -583(%rbp)
	movl	%r14d, %esi
	movq	%rax, -591(%rbp)
	movzbl	-576(%rbp), %eax
	movb	$2, -592(%rbp)
	movdqa	-592(%rbp), %xmm3
	movb	%al, -512(%rbp)
	movaps	%xmm3, -528(%rbp)
	movq	-527(%rbp), %rdx
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L904:
	movq	%rdx, -604(%rbp)
	leal	-10(%r14), %esi
	movl	%ecx, -596(%rbp)
	movq	%rdx, -560(%rbp)
	movl	%ecx, -552(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L910
	movl	-520(%rbp), %r9d
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movq	%rdx, %r8
	movq	%r13, %rdi
	movl	%esi, %edx
	movl	$16, %esi
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L912:
	movl	-596(%rbp), %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L910:
	movl	-552(%rbp), %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler5movssENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L980:
	subl	$2, %eax
	cmpb	$1, %al
	ja	.L919
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L983:
	cmpb	$2, %r12b
	jne	.L963
	movl	-520(%rbp), %ecx
	movl	$8, %r8d
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L924:
	cmpb	$4, %r12b
	jne	.L963
	movq	%rdx, -640(%rbp)
	leal	-10(%r14), %esi
	movl	%ecx, -632(%rbp)
	movq	%rdx, -616(%rbp)
	movl	%ecx, -608(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L931
	subq	$8, %rsp
	movl	%ecx, -596(%rbp)
	movq	%rdx, %r8
	movq	%r13, %rdi
	movl	-520(%rbp), %r9d
	movl	-760(%rbp), %ecx
	pushq	$0
	pushq	$1
	pushq	$3
	movq	%rdx, -604(%rbp)
	movl	%esi, %edx
	movl	$16, %esi
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L981:
	movq	$0, -551(%rbp)
	movl	%r14d, %esi
	movq	%rax, -559(%rbp)
	movzbl	-544(%rbp), %eax
	movb	$2, -560(%rbp)
	movdqa	-560(%rbp), %xmm4
	movb	%al, -512(%rbp)
	movaps	%xmm4, -528(%rbp)
	movq	-527(%rbp), %rdx
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L923:
	movq	%rdx, -616(%rbp)
	leal	-10(%r14), %esi
	movl	%ecx, -608(%rbp)
	movq	%rdx, -604(%rbp)
	movl	%ecx, -596(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L929
	movl	-520(%rbp), %r9d
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movq	%rdx, %r8
	movq	%r13, %rdi
	movl	%esi, %edx
	movl	$16, %esi
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L931:
	movl	-608(%rbp), %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L929:
	movl	-596(%rbp), %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler5movssENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L903:
	movabsq	$-4294967296, %r13
	andq	-816(%rbp), %r13
	movl	$4, %ecx
	movl	%r14d, %esi
	orq	%r13, %rax
	movabsq	$-1095216660481, %r13
	andq	%rax, %r13
	movabsq	$81604378624, %rax
	orq	%rax, %r13
	movq	%r13, %rdx
	movq	%r13, -816(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L844:
	movzbl	-760(%rbp), %r13d
	movq	%rsi, -528(%rbp)
	movl	%edx, -520(%rbp)
	movq	%rsi, -496(%rbp)
	subl	$10, %r13d
	movl	%edx, -488(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L851
	movl	-328(%rbp), %r9d
	movq	%rsi, %r8
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	$17, %esi
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L986:
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-528(%rbp), %rdx
	movl	-520(%rbp), %ecx
	movq	%r13, %rdi
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	movl	$4, %r8d
	movq	%rdx, -724(%rbp)
	movl	%ecx, -716(%rbp)
	movq	%rdx, -700(%rbp)
	movl	%ecx, -692(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	movq	-344(%rbp), %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L922:
	movabsq	$-4294967296, %r12
	andq	-840(%rbp), %r12
	movl	$4, %ecx
	movl	%r14d, %esi
	orq	%r12, %rax
	movabsq	$-1095216660481, %r12
	andq	%rax, %r12
	movabsq	$81604378624, %rax
	orq	%rax, %r12
	movq	%r12, %rdx
	movq	%r12, -840(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L885:
	movq	%r10, %rdi
	movl	$4, %edx
	movabsq	$-4294967296, %r13
	andq	-832(%rbp), %r13
	orq	$8, %r13
	movl	$8, %r8d
	movabsq	$-1095216660481, %rax
	movl	$5, %esi
	andq	%rax, %r13
	movq	%r10, -784(%rbp)
	orq	%r15, %r13
	movb	%r9b, -776(%rbp)
	movq	%r13, %rcx
	movq	%r13, -832(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movzbl	-776(%rbp), %r9d
	xorl	%edx, %edx
	movl	_ZN2v88internalL3rspE(%rip), %esi
	leaq	-592(%rbp), %rdi
	leal	-10(%r9), %r11d
	movl	%r11d, -776(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-592(%rbp), %r8
	movl	-584(%rbp), %edx
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	movl	-776(%rbp), %r11d
	movq	%r8, -604(%rbp)
	movq	-784(%rbp), %r10
	movl	%edx, -596(%rbp)
	movq	%r8, -560(%rbp)
	movl	%edx, -552(%rbp)
	je	.L890
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, %r9d
	movl	$17, %esi
	movq	%r10, %rdi
	movl	%edx, -520(%rbp)
	movl	%r11d, %edx
	movq	%r8, -528(%rbp)
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L968:
	movq	%r11, -864(%rbp)
	movq	%r9, -856(%rbp)
	movl	%r8d, -844(%rbp)
	movl	%esi, -776(%rbp)
	movq	%rdi, -760(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal4wasm17LiftoffStackSlots4SlotELm8EE4GrowEv
	movq	-864(%rbp), %r11
	movq	-856(%rbp), %r9
	movl	-844(%rbp), %r8d
	movl	-776(%rbp), %esi
	movq	-760(%rbp), %rdi
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L970:
	notl	%esi
	movzbl	%r13b, %edx
	andl	$719, %esi
	je	.L875
	xorl	%ecx, %ecx
	movl	$1, %eax
	rep bsfl	%esi, %ecx
	movl	-84(%rbp), %esi
	sall	%cl, %eax
	testl	%eax, %esi
	jne	.L876
	orl	%esi, %eax
	movl	%eax, -84(%rbp)
	movslq	%ecx, %rax
	leaq	-336(%rbp,%rax,2), %rax
	addl	$1, -156(%rbp,%rdx,4)
	movb	%r13b, (%rax)
	movb	$2, 1(%rax)
.L876:
	movq	-784(%rbp), %rax
	movl	%ecx, (%rax)
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L853:
	movl	%r13d, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L851:
	movl	%r13d, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5movssENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L850
.L892:
	movl	%r11d, %ecx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L883
.L890:
	movl	%r11d, %ecx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal9Assembler5movssENS0_7OperandENS0_11XMMRegisterE@PLT
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L985:
	cmpb	$4, %r14b
	jne	.L872
	movzbl	%cl, %ecx
	movb	$4, -335(%rbp,%rcx,2)
	jmp	.L872
.L979:
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	leaq	-528(%rbp), %rdi
	movl	$-16, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-528(%rbp), %rdx
	movq	%r12, %rdi
	movl	-520(%rbp), %ecx
	movl	-808(%rbp), %esi
	movl	$8, %r8d
	movq	%rdx, -676(%rbp)
	movl	%ecx, -668(%rbp)
	movq	%rdx, -664(%rbp)
	movl	%ecx, -656(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L897
.L875:
	cmpq	%rbx, -480(%rbp)
	je	.L987
.L877:
	movl	$513, %eax
	movb	%r13b, 4(%rbx)
	addq	$16, %rbx
	movw	%ax, -16(%rbx)
	movq	-784(%rbp), %rax
	movl	$0, -8(%rbx)
	movb	$0, -4(%rbx)
	movq	%rbx, -488(%rbp)
	movl	$-1, (%rax)
	jmp	.L874
.L857:
	cmpq	$0, -784(%rbp)
	movq	-800(%rbp), %rbx
	jne	.L934
	jmp	.L894
.L987:
	leaq	-496(%rbp), %rdi
	call	_ZN2v84base11SmallVectorINS_8internal4wasm17LiftoffStackSlots4SlotELm8EE4GrowEv
	movq	%rax, %rbx
	jmp	.L877
.L984:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23208:
	.size	_ZN2v88internal4wasm16LiftoffAssembler11PrepareCallEPNS0_9SignatureINS1_9ValueTypeEEEPNS0_8compiler14CallDescriptorEPNS0_8RegisterESB_, .-_ZN2v88internal4wasm16LiftoffAssembler11PrepareCallEPNS0_9SignatureINS1_9ValueTypeEEEPNS0_8compiler14CallDescriptorEPNS0_8RegisterESB_
	.section	.text._ZN2v84base11SmallVectorINS_8internal4wasm15LiftoffRegisterELm8EE4GrowEm,"axG",@progbits,_ZN2v84base11SmallVectorINS_8internal4wasm15LiftoffRegisterELm8EE4GrowEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base11SmallVectorINS_8internal4wasm15LiftoffRegisterELm8EE4GrowEm
	.type	_ZN2v84base11SmallVectorINS_8internal4wasm15LiftoffRegisterELm8EE4GrowEm, @function
_ZN2v84base11SmallVectorINS_8internal4wasm15LiftoffRegisterELm8EE4GrowEm:
.LFB27048:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	8(%rdi), %r13
	movq	16(%rdi), %rdi
	subq	%rax, %r13
	subq	%rax, %rdi
	addq	%rdi, %rdi
	cmpq	%rsi, %rdi
	cmovb	%rsi, %rdi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo64Em@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	malloc@PLT
	movq	(%rbx), %r15
	movq	%r13, %rdx
	movq	%rax, %rdi
	movq	%rax, %r12
	movq	%r15, %rsi
	call	memcpy@PLT
	leaq	24(%rbx), %rax
	cmpq	%rax, %r15
	je	.L989
	movq	%r15, %rdi
	call	free@PLT
.L989:
	leaq	(%r12,%r13), %rax
	movq	%r12, (%rbx)
	addq	%r14, %r12
	movq	%r12, 16(%rbx)
	movq	%rax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE27048:
	.size	_ZN2v84base11SmallVectorINS_8internal4wasm15LiftoffRegisterELm8EE4GrowEm, .-_ZN2v84base11SmallVectorINS_8internal4wasm15LiftoffRegisterELm8EE4GrowEm
	.section	.text._ZN2v84base11SmallVectorINS_8internal4wasm15LiftoffRegisterELm8EE4GrowEv,"axG",@progbits,_ZN2v84base11SmallVectorINS_8internal4wasm15LiftoffRegisterELm8EE4GrowEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base11SmallVectorINS_8internal4wasm15LiftoffRegisterELm8EE4GrowEv
	.type	_ZN2v84base11SmallVectorINS_8internal4wasm15LiftoffRegisterELm8EE4GrowEv, @function
_ZN2v84base11SmallVectorINS_8internal4wasm15LiftoffRegisterELm8EE4GrowEv:
.LFB26462:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	_ZN2v84base11SmallVectorINS_8internal4wasm15LiftoffRegisterELm8EE4GrowEm
	.cfi_endproc
.LFE26462:
	.size	_ZN2v84base11SmallVectorINS_8internal4wasm15LiftoffRegisterELm8EE4GrowEv, .-_ZN2v84base11SmallVectorINS_8internal4wasm15LiftoffRegisterELm8EE4GrowEv
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_115InitMergeRegionEPNS1_16LiftoffAssembler10CacheStateEPKNS3_8VarStateEPS6_jNS2_19MergeKeepStackSlotsENS2_19MergeAllowConstantsENS2_14ReuseRegistersENS1_14LiftoffRegListE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_115InitMergeRegionEPNS1_16LiftoffAssembler10CacheStateEPKNS3_8VarStateEPS6_jNS2_19MergeKeepStackSlotsENS2_19MergeAllowConstantsENS2_14ReuseRegistersENS1_14LiftoffRegListE, @function
_ZN2v88internal4wasm12_GLOBAL__N_115InitMergeRegionEPNS1_16LiftoffAssembler10CacheStateEPKNS3_8VarStateEPS6_jNS2_19MergeKeepStackSlotsENS2_19MergeAllowConstantsENS2_14ReuseRegistersENS1_14LiftoffRegListE:
.LFB23146:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	(%rsi,%rcx,8), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	-72(%rbp), %rdi
	pushq	%r12
	movq	%rdi, %xmm0
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	punpcklqdq	%xmm0, %xmm0
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movzbl	16(%rbp), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-64(%rbp), %rax
	movq	%rdi, -104(%rbp)
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	cmpq	%r15, %rsi
	jnb	.L992
	movl	$1, %esi
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L1045:
	testb	%r8b, %r8b
	jne	.L996
.L997:
	testb	%dl, %dl
	jne	.L1002
.L1008:
	movzbl	1(%rbx), %eax
	movl	$719, %r10d
	leal	-1(%rax), %ecx
	cmpb	$1, %cl
	jbe	.L1004
	leal	-3(%rax), %ecx
	cmpb	$2, %cl
	sbbl	%r10d, %r10d
	andl	$260401, %r10d
	addl	$719, %r10d
.L1004:
	movl	24(%rbp), %ecx
	orl	88(%r13), %ecx
	notl	%ecx
	andl	%r10d, %ecx
	je	.L1042
	rep bsfl	%ecx, %ecx
	movl	%ecx, %r14d
	testb	%dl, %dl
	jne	.L1043
.L1021:
	movl	%esi, %eax
	sall	%cl, %eax
	movslq	%ecx, %rcx
	orl	%eax, 88(%r13)
	addl	$1, 92(%r13,%rcx,4)
	movzbl	1(%rbx), %eax
	movb	$1, (%r12)
	movb	%al, 1(%r12)
	movb	%r14b, 4(%r12)
.L1016:
	addq	$8, %rbx
	addq	$8, %r12
	cmpq	%rbx, %r15
	jbe	.L1044
.L993:
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L1045
	cmpb	$2, %al
	jne	.L998
	testb	%r9b, %r9b
	je	.L997
.L996:
	movq	(%rbx), %rax
	addq	$8, %rbx
	addq	$8, %r12
	movq	%rax, -8(%r12)
	cmpq	%rbx, %r15
	ja	.L993
.L1044:
	cmpq	-104(%rbp), %rdi
	je	.L992
	call	free@PLT
.L992:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1046
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L998:
	.cfi_restore_state
	cmpb	$1, %al
	jne	.L997
	movzbl	4(%rbx), %ecx
	movl	%esi, %eax
	sall	%cl, %eax
	movl	%ecx, %r14d
	testl	%eax, 88(%r13)
	jne	.L997
.L1001:
	movzbl	%r14b, %ecx
	testb	%dl, %dl
	je	.L1021
.L1043:
	movq	-88(%rbp), %rax
	movzbl	4(%rbx), %r11d
	cmpq	%rdi, %rax
	je	.L1012
	movq	%rdi, %r10
	.p2align 4,,10
	.p2align 3
.L1014:
	cmpb	(%r10), %r11b
	je	.L1021
	addq	$2, %r10
	cmpq	%r10, %rax
	jne	.L1014
.L1012:
	cmpq	-80(%rbp), %rax
	je	.L1047
.L1019:
	movb	%r11b, (%rax)
	addq	$1, %rax
	movq	%rax, -88(%rbp)
	cmpq	-80(%rbp), %rax
	je	.L1048
.L1015:
	movb	%r14b, (%rax)
	addq	$1, %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -88(%rbp)
	jmp	.L1021
	.p2align 4,,10
	.p2align 3
.L1042:
	movb	$0, (%r12)
	movb	%al, 1(%r12)
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L1002:
	movq	-88(%rbp), %rcx
	movzbl	4(%rbx), %r10d
	cmpq	%rdi, %rcx
	je	.L1008
	movq	%rdi, %rax
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1006:
	addq	$2, %rax
	cmpq	%rax, %rcx
	je	.L1008
.L1009:
	cmpb	%r10b, (%rax)
	jne	.L1006
	movzbl	1(%rax), %r14d
	jmp	.L1001
.L1048:
	leaq	-96(%rbp), %rdi
	movb	%dl, -111(%rbp)
	movb	%r9b, -110(%rbp)
	movb	%r8b, -109(%rbp)
	movl	%ecx, -108(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal4wasm15LiftoffRegisterELm8EE4GrowEv
	movzbl	-111(%rbp), %edx
	movl	-108(%rbp), %ecx
	movl	$1, %esi
	movzbl	-110(%rbp), %r9d
	movzbl	-109(%rbp), %r8d
	jmp	.L1015
.L1047:
	leaq	-96(%rbp), %rdi
	movb	%dl, -112(%rbp)
	movb	%r9b, -111(%rbp)
	movb	%r8b, -110(%rbp)
	movb	%r11b, -109(%rbp)
	movl	%ecx, -108(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal4wasm15LiftoffRegisterELm8EE4GrowEv
	movzbl	-112(%rbp), %edx
	movzbl	-111(%rbp), %r9d
	movl	$1, %esi
	movzbl	-110(%rbp), %r8d
	movzbl	-109(%rbp), %r11d
	movl	-108(%rbp), %ecx
	jmp	.L1019
.L1046:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23146:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_115InitMergeRegionEPNS1_16LiftoffAssembler10CacheStateEPKNS3_8VarStateEPS6_jNS2_19MergeKeepStackSlotsENS2_19MergeAllowConstantsENS2_14ReuseRegistersENS1_14LiftoffRegListE, .-_ZN2v88internal4wasm12_GLOBAL__N_115InitMergeRegionEPNS1_16LiftoffAssembler10CacheStateEPKNS3_8VarStateEPS6_jNS2_19MergeKeepStackSlotsENS2_19MergeAllowConstantsENS2_14ReuseRegistersENS1_14LiftoffRegListE
	.section	.text._ZN2v88internal4wasm16LiftoffAssembler10CacheState9InitMergeERKS3_jjj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16LiftoffAssembler10CacheState9InitMergeERKS3_jjj
	.type	_ZN2v88internal4wasm16LiftoffAssembler10CacheState9InitMergeERKS3_jjj, @function
_ZN2v88internal4wasm16LiftoffAssembler10CacheState9InitMergeERKS3_jjj:
.LFB23157:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	8(%rsi), %rax
	subq	(%rsi), %rax
	movl	%r8d, -60(%rbp)
	sarq	$3, %rax
	movq	(%rdi), %r14
	addl	%edx, %r8d
	movl	%eax, %r11d
	movq	16(%rdi), %rax
	leal	(%r8,%rcx), %r12d
	subl	%r12d, %r11d
	subq	%r14, %rax
	sarq	$3, %rax
	cmpq	%rax, %r12
	ja	.L1061
.L1050:
	leaq	(%r14,%r12,8), %rax
	movl	%r15d, %edx
	movq	%rax, 8(%r13)
	movq	(%rbx), %rax
	leaq	0(,%rdx,8), %rsi
	movq	%rsi, -56(%rbp)
	leaq	(%rax,%rsi), %r12
	cmpq	%rax, %r12
	je	.L1057
	movq	%rax, %rdx
	xorl	%ebx, %ebx
	movl	$1, %esi
	.p2align 4,,10
	.p2align 3
.L1053:
	cmpb	$1, (%rdx)
	jne	.L1052
	movzbl	4(%rdx), %ecx
	movl	%esi, %edi
	sall	%cl, %edi
	orl	%edi, %ebx
.L1052:
	addq	$8, %rdx
	cmpq	%rdx, %r12
	jne	.L1053
.L1051:
	movl	%r11d, %edx
	addq	%r8, %rdx
	leaq	(%rax,%rdx,8), %rsi
	movl	%r10d, %edx
	leaq	(%rsi,%rdx,8), %rdi
	cmpq	%rdi, %rsi
	je	.L1054
	movq	%rax, -72(%rbp)
	movq	%rsi, %rdx
	movl	$1, %r9d
	.p2align 4,,10
	.p2align 3
.L1056:
	cmpb	$1, (%rdx)
	jne	.L1055
	movzbl	4(%rdx), %ecx
	movl	%r9d, %eax
	sall	%cl, %eax
	orl	%eax, %ebx
.L1055:
	addq	$8, %rdx
	cmpq	%rdx, %rdi
	jne	.L1056
	movq	-72(%rbp), %rax
.L1054:
	testl	%r11d, %r11d
	pushq	%rbx
	leaq	(%r14,%r8,8), %rdx
	movl	%r10d, %ecx
	pushq	$0
	sete	%r8b
	movq	%r13, %rdi
	movl	$0, %r9d
	movzbl	%r8b, %r8d
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_115InitMergeRegionEPNS1_16LiftoffAssembler10CacheStateEPKNS3_8VarStateEPS6_jNS2_19MergeKeepStackSlotsENS2_19MergeAllowConstantsENS2_14ReuseRegistersENS1_14LiftoffRegListE
	movq	-72(%rbp), %rax
	pushq	%rbx
	movl	%r15d, %ecx
	pushq	$0
	movq	%r14, %rdx
	movq	%r13, %rdi
	xorl	%r9d, %r9d
	movq	%rax, %rsi
	movl	$1, %r8d
	call	_ZN2v88internal4wasm12_GLOBAL__N_115InitMergeRegionEPNS1_16LiftoffAssembler10CacheStateEPKNS3_8VarStateEPS6_jNS2_19MergeKeepStackSlotsENS2_19MergeAllowConstantsENS2_14ReuseRegistersENS1_14LiftoffRegListE
	addq	$32, %rsp
	movq	-56(%rbp), %rdx
	movl	-60(%rbp), %ecx
	pushq	%rbx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$1, %r9d
	pushq	$1
	addq	%r14, %rdx
	movl	$1, %r8d
	call	_ZN2v88internal4wasm12_GLOBAL__N_115InitMergeRegionEPNS1_16LiftoffAssembler10CacheStateEPKNS3_8VarStateEPS6_jNS2_19MergeKeepStackSlotsENS2_19MergeAllowConstantsENS2_14ReuseRegistersENS1_14LiftoffRegListE
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1061:
	.cfi_restore_state
	movq	%r12, %rsi
	movl	%r11d, -72(%rbp)
	movl	%r8d, -56(%rbp)
	movl	%ecx, -64(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal4wasm16LiftoffAssembler8VarStateELm8EE4GrowEm
	movq	0(%r13), %r14
	movl	-64(%rbp), %r10d
	movl	-72(%rbp), %r11d
	movl	-56(%rbp), %r8d
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1057:
	xorl	%ebx, %ebx
	jmp	.L1051
	.cfi_endproc
.LFE23157:
	.size	_ZN2v88internal4wasm16LiftoffAssembler10CacheState9InitMergeERKS3_jjj, .-_ZN2v88internal4wasm16LiftoffAssembler10CacheState9InitMergeERKS3_jjj
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal4wasm16LiftoffAssembler10CacheState9InitMergeERKS3_jjj,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal4wasm16LiftoffAssembler10CacheState9InitMergeERKS3_jjj, @function
_GLOBAL__sub_I__ZN2v88internal4wasm16LiftoffAssembler10CacheState9InitMergeERKS3_jjj:
.LFB28260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28260:
	.size	_GLOBAL__sub_I__ZN2v88internal4wasm16LiftoffAssembler10CacheState9InitMergeERKS3_jjj, .-_GLOBAL__sub_I__ZN2v88internal4wasm16LiftoffAssembler10CacheState9InitMergeERKS3_jjj
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal4wasm16LiftoffAssembler10CacheState9InitMergeERKS3_jjj
	.weak	_ZTVN2v88internal9AssemblerE
	.section	.data.rel.ro.local._ZTVN2v88internal9AssemblerE,"awG",@progbits,_ZTVN2v88internal9AssemblerE,comdat
	.align 8
	.type	_ZTVN2v88internal9AssemblerE, @object
	.size	_ZTVN2v88internal9AssemblerE, 40
_ZTVN2v88internal9AssemblerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal9AssemblerD1Ev
	.quad	_ZN2v88internal9AssemblerD0Ev
	.quad	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.weak	_ZTVN2v88internal4wasm16LiftoffAssemblerE
	.section	.data.rel.ro._ZTVN2v88internal4wasm16LiftoffAssemblerE,"awG",@progbits,_ZTVN2v88internal4wasm16LiftoffAssemblerE,comdat
	.align 8
	.type	_ZTVN2v88internal4wasm16LiftoffAssemblerE, @object
	.size	_ZTVN2v88internal4wasm16LiftoffAssemblerE, 112
_ZTVN2v88internal4wasm16LiftoffAssemblerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm16LiftoffAssemblerD1Ev
	.quad	_ZN2v88internal4wasm16LiftoffAssemblerD0Ev
	.quad	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.quad	_ZN2v88internal14TurboAssembler4JumpERKNS0_17ExternalReferenceE
	.quad	_ZN2v88internal14TurboAssembler18CallBuiltinByIndexENS0_8RegisterE
	.quad	_ZN2v88internal14TurboAssembler14CallCodeObjectENS0_8RegisterE
	.quad	_ZN2v88internal14TurboAssembler14JumpCodeObjectENS0_8RegisterE
	.quad	_ZN2v88internal14TurboAssembler19LoadCodeObjectEntryENS0_8RegisterES2_
	.quad	_ZN2v88internal14TurboAssembler22LoadFromConstantsTableENS0_8RegisterEi
	.quad	_ZN2v88internal14TurboAssembler22LoadRootRegisterOffsetENS0_8RegisterEl
	.quad	_ZN2v88internal14TurboAssembler16LoadRootRelativeENS0_8RegisterEi
	.quad	_ZN2v88internal14TurboAssembler8LoadRootENS0_8RegisterENS0_9RootIndexE
	.section	.rodata._ZN2v88internalL16kScratchRegisterE,"a"
	.align 4
	.type	_ZN2v88internalL16kScratchRegisterE, @object
	.size	_ZN2v88internalL16kScratchRegisterE, 4
_ZN2v88internalL16kScratchRegisterE:
	.long	10
	.weak	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC16:
	.string	"xmm0"
.LC17:
	.string	"xmm1"
.LC18:
	.string	"xmm2"
.LC19:
	.string	"xmm3"
.LC20:
	.string	"xmm4"
.LC21:
	.string	"xmm5"
.LC22:
	.string	"xmm6"
.LC23:
	.string	"xmm7"
.LC24:
	.string	"xmm8"
.LC25:
	.string	"xmm9"
.LC26:
	.string	"xmm10"
.LC27:
	.string	"xmm11"
.LC28:
	.string	"xmm12"
.LC29:
	.string	"xmm13"
.LC30:
	.string	"xmm14"
.LC31:
	.string	"xmm15"
	.section	.data.rel.ro.local._ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names,"awG",@progbits,_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names,comdat
	.align 32
	.type	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names, @gnu_unique_object
	.size	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names, 128
_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names:
	.quad	.LC16
	.quad	.LC17
	.quad	.LC18
	.quad	.LC19
	.quad	.LC20
	.quad	.LC21
	.quad	.LC22
	.quad	.LC23
	.quad	.LC24
	.quad	.LC25
	.quad	.LC26
	.quad	.LC27
	.quad	.LC28
	.quad	.LC29
	.quad	.LC30
	.quad	.LC31
	.weak	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names
	.section	.rodata.str1.1
.LC32:
	.string	"rax"
.LC33:
	.string	"rcx"
.LC34:
	.string	"rdx"
.LC35:
	.string	"rbx"
.LC36:
	.string	"rsp"
.LC37:
	.string	"rbp"
.LC38:
	.string	"rsi"
.LC39:
	.string	"rdi"
.LC40:
	.string	"r8"
.LC41:
	.string	"r9"
.LC42:
	.string	"r10"
.LC43:
	.string	"r11"
.LC44:
	.string	"r12"
.LC45:
	.string	"r13"
.LC46:
	.string	"r14"
.LC47:
	.string	"r15"
	.section	.data.rel.ro.local._ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names,"awG",@progbits,_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names,comdat
	.align 32
	.type	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names, @gnu_unique_object
	.size	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names, 128
_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names:
	.quad	.LC32
	.quad	.LC33
	.quad	.LC34
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.quad	.LC38
	.quad	.LC39
	.quad	.LC40
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.quad	.LC45
	.quad	.LC46
	.quad	.LC47
	.section	.rodata._ZN2v88internalL4xmm0E,"a"
	.align 4
	.type	_ZN2v88internalL4xmm0E, @object
	.size	_ZN2v88internalL4xmm0E, 4
_ZN2v88internalL4xmm0E:
	.zero	4
	.section	.rodata._ZN2v88internalL3rbpE,"a"
	.align 4
	.type	_ZN2v88internalL3rbpE, @object
	.size	_ZN2v88internalL3rbpE, 4
_ZN2v88internalL3rbpE:
	.long	5
	.section	.rodata._ZN2v88internalL3rspE,"a"
	.align 4
	.type	_ZN2v88internalL3rspE, @object
	.size	_ZN2v88internalL3rspE, 4
_ZN2v88internalL3rspE:
	.long	4
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
