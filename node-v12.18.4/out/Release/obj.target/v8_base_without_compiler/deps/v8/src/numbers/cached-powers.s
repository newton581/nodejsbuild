	.file	"cached-powers.cc"
	.text
	.section	.text._ZN2v88internal16PowersOfTenCache36GetCachedPowerForBinaryExponentRangeEiiPNS0_5DiyFpEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16PowersOfTenCache36GetCachedPowerForBinaryExponentRangeEiiPNS0_5DiyFpEPi
	.type	_ZN2v88internal16PowersOfTenCache36GetCachedPowerForBinaryExponentRangeEiiPNS0_5DiyFpEPi, @function
_ZN2v88internal16PowersOfTenCache36GetCachedPowerForBinaryExponentRangeEiiPNS0_5DiyFpEPi:
.LFB3384:
	.cfi_startproc
	endbr64
	addl	$63, %edi
	pxor	%xmm1, %xmm1
	movsd	.LC2(%rip), %xmm3
	movsd	.LC1(%rip), %xmm4
	cvtsi2sdl	%edi, %xmm1
	mulsd	.LC0(%rip), %xmm1
	movapd	%xmm1, %xmm2
	movapd	%xmm1, %xmm0
	andpd	%xmm3, %xmm2
	ucomisd	%xmm2, %xmm4
	jbe	.L2
	cvttsd2siq	%xmm1, %rax
	pxor	%xmm2, %xmm2
	movsd	.LC3(%rip), %xmm4
	andnpd	%xmm1, %xmm3
	cvtsi2sdq	%rax, %xmm2
	cmpnlesd	%xmm2, %xmm0
	andpd	%xmm4, %xmm0
	addsd	%xmm2, %xmm0
	orpd	%xmm3, %xmm0
.L2:
	cvttsd2sil	%xmm0, %esi
	leal	354(%rsi), %eax
	addl	$347, %esi
	cmovns	%esi, %eax
	leaq	_ZN2v88internalL13kCachedPowersE(%rip), %rsi
	sarl	$3, %eax
	addl	$1, %eax
	cltq
	salq	$4, %rax
	addq	%rsi, %rax
	movzwl	8(%rax), %esi
	movq	(%rax), %rdi
	movswl	10(%rax), %eax
	movl	%eax, (%rcx)
	movswl	%si, %eax
	movq	%rdi, (%rdx)
	movl	%eax, 8(%rdx)
	ret
	.cfi_endproc
.LFE3384:
	.size	_ZN2v88internal16PowersOfTenCache36GetCachedPowerForBinaryExponentRangeEiiPNS0_5DiyFpEPi, .-_ZN2v88internal16PowersOfTenCache36GetCachedPowerForBinaryExponentRangeEiiPNS0_5DiyFpEPi
	.section	.text._ZN2v88internal16PowersOfTenCache32GetCachedPowerForDecimalExponentEiPNS0_5DiyFpEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16PowersOfTenCache32GetCachedPowerForDecimalExponentEiPNS0_5DiyFpEPi
	.type	_ZN2v88internal16PowersOfTenCache32GetCachedPowerForDecimalExponentEiPNS0_5DiyFpEPi, @function
_ZN2v88internal16PowersOfTenCache32GetCachedPowerForDecimalExponentEiPNS0_5DiyFpEPi:
.LFB3385:
	.cfi_startproc
	endbr64
	leal	355(%rdi), %eax
	addl	$348, %edi
	movq	%rdx, %r8
	cmovns	%edi, %eax
	leaq	_ZN2v88internalL13kCachedPowersE(%rip), %rdx
	sarl	$3, %eax
	cltq
	salq	$4, %rax
	addq	%rdx, %rax
	movzwl	10(%rax), %edx
	movq	(%rax), %rcx
	movswl	8(%rax), %eax
	movq	%rcx, (%rsi)
	movl	%eax, 8(%rsi)
	movswl	%dx, %eax
	movl	%eax, (%r8)
	ret
	.cfi_endproc
.LFE3385:
	.size	_ZN2v88internal16PowersOfTenCache32GetCachedPowerForDecimalExponentEiPNS0_5DiyFpEPi, .-_ZN2v88internal16PowersOfTenCache32GetCachedPowerForDecimalExponentEiPNS0_5DiyFpEPi
	.globl	_ZN2v88internal16PowersOfTenCache19kMaxDecimalExponentE
	.section	.rodata._ZN2v88internal16PowersOfTenCache19kMaxDecimalExponentE,"a"
	.align 4
	.type	_ZN2v88internal16PowersOfTenCache19kMaxDecimalExponentE, @object
	.size	_ZN2v88internal16PowersOfTenCache19kMaxDecimalExponentE, 4
_ZN2v88internal16PowersOfTenCache19kMaxDecimalExponentE:
	.long	340
	.globl	_ZN2v88internal16PowersOfTenCache19kMinDecimalExponentE
	.section	.rodata._ZN2v88internal16PowersOfTenCache19kMinDecimalExponentE,"a"
	.align 4
	.type	_ZN2v88internal16PowersOfTenCache19kMinDecimalExponentE, @object
	.size	_ZN2v88internal16PowersOfTenCache19kMinDecimalExponentE, 4
_ZN2v88internal16PowersOfTenCache19kMinDecimalExponentE:
	.long	-348
	.globl	_ZN2v88internal16PowersOfTenCache24kDecimalExponentDistanceE
	.section	.rodata._ZN2v88internal16PowersOfTenCache24kDecimalExponentDistanceE,"a"
	.align 4
	.type	_ZN2v88internal16PowersOfTenCache24kDecimalExponentDistanceE, @object
	.size	_ZN2v88internal16PowersOfTenCache24kDecimalExponentDistanceE, 4
_ZN2v88internal16PowersOfTenCache24kDecimalExponentDistanceE:
	.long	8
	.section	.rodata._ZN2v88internalL13kCachedPowersE,"a"
	.align 32
	.type	_ZN2v88internalL13kCachedPowersE, @object
	.size	_ZN2v88internalL13kCachedPowersE, 1392
_ZN2v88internalL13kCachedPowersE:
	.quad	-391859759250406776
	.value	-1220
	.value	-348
	.zero	4
	.quad	-4994806998408183946
	.value	-1193
	.value	-340
	.zero	4
	.quad	-8424269937281487754
	.value	-1166
	.value	-332
	.zero	4
	.quad	-3512093806901185046
	.value	-1140
	.value	-324
	.zero	4
	.quad	-7319562523736982739
	.value	-1113
	.value	-316
	.zero	4
	.quad	-1865951482774665761
	.value	-1087
	.value	-308
	.zero	4
	.quad	-6093090917745768758
	.value	-1060
	.value	-300
	.zero	4
	.quad	-38366372719436721
	.value	-1034
	.value	-292
	.zero	4
	.quad	-4731433901725329908
	.value	-1007
	.value	-284
	.zero	4
	.quad	-8228041688891786180
	.value	-980
	.value	-276
	.zero	4
	.quad	-3219690930897053053
	.value	-954
	.value	-268
	.zero	4
	.quad	-7101705404292871755
	.value	-927
	.value	-260
	.zero	4
	.quad	-1541319077368263733
	.value	-901
	.value	-252
	.zero	4
	.quad	-5851220927660403859
	.value	-874
	.value	-244
	.zero	4
	.quad	-9062348037703676329
	.value	-847
	.value	-236
	.zero	4
	.quad	-4462904269766699465
	.value	-821
	.value	-228
	.zero	4
	.quad	-8027971522334779313
	.value	-794
	.value	-220
	.zero	4
	.quad	-2921563150702462265
	.value	-768
	.value	-212
	.zero	4
	.quad	-6879582898840692748
	.value	-741
	.value	-204
	.zero	4
	.quad	-1210330751515841307
	.value	-715
	.value	-196
	.zero	4
	.quad	-5604615407819967858
	.value	-688
	.value	-188
	.zero	4
	.quad	-8878612607581929669
	.value	-661
	.value	-180
	.zero	4
	.quad	-4189117143640191558
	.value	-635
	.value	-172
	.zero	4
	.quad	-7823984217374209642
	.value	-608
	.value	-164
	.zero	4
	.quad	-2617598379430861436
	.value	-582
	.value	-156
	.zero	4
	.quad	-6653111496142234890
	.value	-555
	.value	-148
	.zero	4
	.quad	-872862063775190746
	.value	-529
	.value	-140
	.zero	4
	.quad	-5353181642124984136
	.value	-502
	.value	-132
	.zero	4
	.quad	-8691279853972075893
	.value	-475
	.value	-124
	.zero	4
	.quad	-3909969587797413805
	.value	-449
	.value	-116
	.zero	4
	.quad	-7616003081050118571
	.value	-422
	.value	-108
	.zero	4
	.quad	-2307682335666372931
	.value	-396
	.value	-100
	.zero	4
	.quad	-6422206049907525489
	.value	-369
	.value	-92
	.zero	4
	.quad	-528786136287117932
	.value	-343
	.value	-84
	.zero	4
	.quad	-5096825099203863601
	.value	-316
	.value	-76
	.zero	4
	.quad	-8500279345513818773
	.value	-289
	.value	-68
	.zero	4
	.quad	-3625356651333078602
	.value	-263
	.value	-60
	.zero	4
	.quad	-7403949918844649556
	.value	-236
	.value	-52
	.zero	4
	.quad	-1991698500497491194
	.value	-210
	.value	-44
	.zero	4
	.quad	-6186779746782440749
	.value	-183
	.value	-36
	.zero	4
	.quad	-177973607073265138
	.value	-157
	.value	-28
	.zero	4
	.quad	-4835449396872013077
	.value	-130
	.value	-20
	.zero	4
	.quad	-8305539271883716404
	.value	-103
	.value	-12
	.zero	4
	.quad	-3335171328526686932
	.value	-77
	.value	-4
	.zero	4
	.quad	-7187745005283311616
	.value	-50
	.value	4
	.zero	4
	.quad	-1669528073709551616
	.value	-24
	.value	12
	.zero	4
	.quad	-5946744073709551616
	.value	3
	.value	20
	.zero	4
	.quad	-9133518327554766460
	.value	30
	.value	28
	.zero	4
	.quad	-4568956265895094861
	.value	56
	.value	36
	.zero	4
	.quad	-8106986416796705680
	.value	83
	.value	44
	.zero	4
	.quad	-3039304518611664792
	.value	109
	.value	52
	.zero	4
	.quad	-6967307053960650171
	.value	136
	.value	60
	.zero	4
	.quad	-1341049929119499481
	.value	162
	.value	68
	.zero	4
	.quad	-5702008784649933400
	.value	189
	.value	76
	.zero	4
	.quad	-8951176327949752869
	.value	216
	.value	84
	.zero	4
	.quad	-4297245513042813542
	.value	242
	.value	92
	.zero	4
	.quad	-7904546130479028392
	.value	269
	.value	100
	.zero	4
	.quad	-2737644984756826646
	.value	295
	.value	108
	.zero	4
	.quad	-6742553186979055798
	.value	322
	.value	116
	.zero	4
	.quad	-1006140569036166267
	.value	348
	.value	124
	.zero	4
	.quad	-5452481866653427593
	.value	375
	.value	132
	.zero	4
	.quad	-8765264286586255934
	.value	402
	.value	140
	.zero	4
	.quad	-4020214983419339459
	.value	428
	.value	148
	.zero	4
	.quad	-7698142301602209613
	.value	455
	.value	156
	.zero	4
	.quad	-2430079312244744221
	.value	481
	.value	164
	.zero	4
	.quad	-6513398903789220827
	.value	508
	.value	172
	.zero	4
	.quad	-664674077828931748
	.value	534
	.value	180
	.zero	4
	.quad	-5198069505264599346
	.value	561
	.value	188
	.zero	4
	.quad	-8575712306248138270
	.value	588
	.value	196
	.zero	4
	.quad	-3737760522056206171
	.value	614
	.value	204
	.zero	4
	.quad	-7487697328667536417
	.value	641
	.value	212
	.zero	4
	.quad	-2116491865831296966
	.value	667
	.value	220
	.zero	4
	.quad	-6279758049420528746
	.value	694
	.value	228
	.zero	4
	.quad	-316522074587315140
	.value	720
	.value	236
	.zero	4
	.quad	-4938676049251384304
	.value	747
	.value	244
	.zero	4
	.quad	-8382449121214030822
	.value	774
	.value	252
	.zero	4
	.quad	-3449775934753242068
	.value	800
	.value	260
	.zero	4
	.quad	-7273132090830278359
	.value	827
	.value	268
	.zero	4
	.quad	-1796764746270372707
	.value	853
	.value	276
	.zero	4
	.quad	-6041542782089432023
	.value	880
	.value	284
	.zero	4
	.quad	-9204148869281624187
	.value	907
	.value	292
	.zero	4
	.quad	-4674203974643163859
	.value	933
	.value	300
	.zero	4
	.quad	-8185402070463610993
	.value	960
	.value	308
	.zero	4
	.quad	-3156152948152813503
	.value	986
	.value	316
	.zero	4
	.quad	-7054365918152680535
	.value	1013
	.value	324
	.zero	4
	.quad	-1470777745987373095
	.value	1039
	.value	332
	.zero	4
	.quad	-5798663540173640085
	.value	1066
	.value	340
	.zero	4
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	1352628734
	.long	1070810131
	.align 8
.LC1:
	.long	0
	.long	1127219200
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC3:
	.long	0
	.long	1072693248
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
