	.file	"thread-id.cc"
	.text
	.section	.text._ZN2v88internal8ThreadId13TryGetCurrentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8ThreadId13TryGetCurrentEv
	.type	_ZN2v88internal8ThreadId13TryGetCurrentEv, @function
_ZN2v88internal8ThreadId13TryGetCurrentEv:
.LFB3443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_114GetThreadIdKeyEvE6object(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testb	%al, %al
	je	.L12
.L3:
	movl	_ZZN2v88internal12_GLOBAL__N_114GetThreadIdKeyEvE6object(%rip), %edi
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movl	$-1, %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_114GetThreadIdKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L3
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_114GetThreadIdKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_114GetThreadIdKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L3
	.cfi_endproc
.LFE3443:
	.size	_ZN2v88internal8ThreadId13TryGetCurrentEv, .-_ZN2v88internal8ThreadId13TryGetCurrentEv
	.section	.rodata._ZN2v88internal8ThreadId18GetCurrentThreadIdEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"1 <= thread_id"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8ThreadId18GetCurrentThreadIdEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8ThreadId18GetCurrentThreadIdEv
	.type	_ZN2v88internal8ThreadId18GetCurrentThreadIdEv, @function
_ZN2v88internal8ThreadId18GetCurrentThreadIdEv:
.LFB3444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_114GetThreadIdKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L23
.L15:
	movl	_ZZN2v88internal12_GLOBAL__N_114GetThreadIdKeyEvE6object(%rip), %r13d
	movl	%r13d, %edi
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L24
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_114GetThreadIdKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L15
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_114GetThreadIdKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_114GetThreadIdKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$1, %r12d
	lock xaddl	%r12d, _ZN2v88internal12_GLOBAL__N_114next_thread_idE(%rip)
	testl	%r12d, %r12d
	jle	.L25
	movslq	%r12d, %rsi
	movl	%r13d, %edi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE3444:
	.size	_ZN2v88internal8ThreadId18GetCurrentThreadIdEv, .-_ZN2v88internal8ThreadId18GetCurrentThreadIdEv
	.section	.data._ZN2v88internal12_GLOBAL__N_114next_thread_idE,"aw"
	.align 4
	.type	_ZN2v88internal12_GLOBAL__N_114next_thread_idE, @object
	.size	_ZN2v88internal12_GLOBAL__N_114next_thread_idE, 4
_ZN2v88internal12_GLOBAL__N_114next_thread_idE:
	.long	1
	.section	.bss._ZGVZN2v88internal12_GLOBAL__N_114GetThreadIdKeyEvE6object,"aw",@nobits
	.align 8
	.type	_ZGVZN2v88internal12_GLOBAL__N_114GetThreadIdKeyEvE6object, @object
	.size	_ZGVZN2v88internal12_GLOBAL__N_114GetThreadIdKeyEvE6object, 8
_ZGVZN2v88internal12_GLOBAL__N_114GetThreadIdKeyEvE6object:
	.zero	8
	.section	.bss._ZZN2v88internal12_GLOBAL__N_114GetThreadIdKeyEvE6object,"aw",@nobits
	.align 4
	.type	_ZZN2v88internal12_GLOBAL__N_114GetThreadIdKeyEvE6object, @object
	.size	_ZZN2v88internal12_GLOBAL__N_114GetThreadIdKeyEvE6object, 4
_ZZN2v88internal12_GLOBAL__N_114GetThreadIdKeyEvE6object:
	.zero	4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
