	.file	"eh-frame.cc"
	.text
	.section	.text._ZN2v88internal13EhFrameWriter17WriteEmptyEhFrameERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13EhFrameWriter17WriteEmptyEhFrameERSo
	.type	_ZN2v88internal13EhFrameWriter17WriteEmptyEhFrameERSo, @function
_ZN2v88internal13EhFrameWriter17WriteEmptyEhFrameERSo:
.LFB5864:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNSo3putEc@PLT
	movl	$27, %esi
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movl	$59, %esi
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	pxor	%xmm0, %xmm0
	leaq	-48(%rbp), %rsi
	movq	%r12, %rdi
	movl	$16, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZNSo5writeEPKcl@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5864:
	.size	_ZN2v88internal13EhFrameWriter17WriteEmptyEhFrameERSo, .-_ZN2v88internal13EhFrameWriter17WriteEmptyEhFrameERSo
	.section	.text._ZN2v88internal13EhFrameWriterC2EPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13EhFrameWriterC2EPNS0_4ZoneE
	.type	_ZN2v88internal13EhFrameWriterC2EPNS0_4ZoneE, @function
_ZN2v88internal13EhFrameWriterC2EPNS0_4ZoneE:
.LFB5869:
	.cfi_startproc
	endbr64
	movdqa	.LC0(%rip), %xmm0
	movl	$0, 16(%rdi)
	movq	%rsi, 24(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 48(%rdi)
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE5869:
	.size	_ZN2v88internal13EhFrameWriterC2EPNS0_4ZoneE, .-_ZN2v88internal13EhFrameWriterC2EPNS0_4ZoneE
	.globl	_ZN2v88internal13EhFrameWriterC1EPNS0_4ZoneE
	.set	_ZN2v88internal13EhFrameWriterC1EPNS0_4ZoneE,_ZN2v88internal13EhFrameWriterC2EPNS0_4ZoneE
	.section	.text._ZN2v88internal13EhFrameWriter10GetEhFrameEPNS0_8CodeDescE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13EhFrameWriter10GetEhFrameEPNS0_8CodeDescE
	.type	_ZN2v88internal13EhFrameWriter10GetEhFrameEPNS0_8CodeDescE, @function
_ZN2v88internal13EhFrameWriter10GetEhFrameEPNS0_8CodeDescE:
.LFB5884:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	subq	32(%rdi), %rax
	movl	%eax, 64(%rsi)
	movq	32(%rdi), %rax
	movq	%rax, 56(%rsi)
	ret
	.cfi_endproc
.LFE5884:
	.size	_ZN2v88internal13EhFrameWriter10GetEhFrameEPNS0_8CodeDescE, .-_ZN2v88internal13EhFrameWriter10GetEhFrameEPNS0_8CodeDescE
	.section	.text._ZN2v88internal15EhFrameIterator14GetNextULeb128Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15EhFrameIterator14GetNextULeb128Ev
	.type	_ZN2v88internal15EhFrameIterator14GetNextULeb128Ev, @function
_ZN2v88internal15EhFrameIterator14GetNextULeb128Ev:
.LFB5887:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r9
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r9, %rdx
	.p2align 4,,10
	.p2align 3
.L9:
	movzbl	(%rdx), %esi
	addq	$1, %rdx
	movl	%esi, %eax
	andl	$127, %eax
	sall	%cl, %eax
	addl	$7, %ecx
	orl	%eax, %r8d
	testb	%sil, %sil
	js	.L9
	subq	%r9, %rdx
	movl	%r8d, %eax
	movslq	%edx, %rdx
	addq	%r9, %rdx
	movq	%rdx, 8(%rdi)
	ret
	.cfi_endproc
.LFE5887:
	.size	_ZN2v88internal15EhFrameIterator14GetNextULeb128Ev, .-_ZN2v88internal15EhFrameIterator14GetNextULeb128Ev
	.section	.text._ZN2v88internal15EhFrameIterator14GetNextSLeb128Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15EhFrameIterator14GetNextSLeb128Ev
	.type	_ZN2v88internal15EhFrameIterator14GetNextSLeb128Ev, @function
_ZN2v88internal15EhFrameIterator14GetNextSLeb128Ev:
.LFB5888:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r9
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r9, %rdx
	.p2align 4,,10
	.p2align 3
.L12:
	movzbl	(%rdx), %esi
	addq	$1, %rdx
	movl	%esi, %eax
	andl	$127, %eax
	sall	%cl, %eax
	addl	$7, %ecx
	orl	%eax, %r8d
	testb	%sil, %sil
	js	.L12
	andl	$64, %esi
	je	.L13
	movq	$-1, %rax
	salq	%cl, %rax
	orl	%eax, %r8d
.L13:
	subq	%r9, %rdx
	movl	%r8d, %eax
	movslq	%edx, %rdx
	addq	%r9, %rdx
	movq	%rdx, 8(%rdi)
	ret
	.cfi_endproc
.LFE5888:
	.size	_ZN2v88internal15EhFrameIterator14GetNextSLeb128Ev, .-_ZN2v88internal15EhFrameIterator14GetNextSLeb128Ev
	.section	.text._ZN2v88internal15EhFrameIterator13DecodeULeb128EPKhPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15EhFrameIterator13DecodeULeb128EPKhPi
	.type	_ZN2v88internal15EhFrameIterator13DecodeULeb128EPKhPi, @function
_ZN2v88internal15EhFrameIterator13DecodeULeb128EPKhPi:
.LFB5889:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	.p2align 4,,10
	.p2align 3
.L19:
	movzbl	(%rdx), %r8d
	addq	$1, %rdx
	movl	%r8d, %eax
	andl	$127, %eax
	sall	%cl, %eax
	addl	$7, %ecx
	orl	%eax, %r9d
	testb	%r8b, %r8b
	js	.L19
	subq	%rdi, %rdx
	movl	%r9d, %eax
	movl	%edx, (%rsi)
	ret
	.cfi_endproc
.LFE5889:
	.size	_ZN2v88internal15EhFrameIterator13DecodeULeb128EPKhPi, .-_ZN2v88internal15EhFrameIterator13DecodeULeb128EPKhPi
	.section	.text._ZN2v88internal15EhFrameIterator13DecodeSLeb128EPKhPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15EhFrameIterator13DecodeSLeb128EPKhPi
	.type	_ZN2v88internal15EhFrameIterator13DecodeSLeb128EPKhPi, @function
_ZN2v88internal15EhFrameIterator13DecodeSLeb128EPKhPi:
.LFB5890:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	.p2align 4,,10
	.p2align 3
.L22:
	movzbl	(%rdx), %r8d
	addq	$1, %rdx
	movl	%r8d, %eax
	andl	$127, %eax
	sall	%cl, %eax
	addl	$7, %ecx
	orl	%eax, %r9d
	testb	%r8b, %r8b
	js	.L22
	andl	$64, %r8d
	je	.L23
	movq	$-1, %rax
	salq	%cl, %rax
	orl	%eax, %r9d
.L23:
	subq	%rdi, %rdx
	movl	%r9d, %eax
	movl	%edx, (%rsi)
	ret
	.cfi_endproc
.LFE5890:
	.size	_ZN2v88internal15EhFrameIterator13DecodeSLeb128EPKhPi, .-_ZN2v88internal15EhFrameIterator13DecodeSLeb128EPKhPi
	.section	.rodata._ZN2v88internal19EhFrameDisassembler19DumpDwarfDirectivesERSoPKhS4_.str1.1,"aMS",@progbits,1
.LC1:
	.string	"  "
.LC2:
	.string	"| pc_offset="
.LC3:
	.string	" (delta="
.LC4:
	.string	")\n"
.LC5:
	.string	"| "
.LC6:
	.string	" saved at base"
.LC7:
	.string	" follows rule in CIE\n"
.LC8:
	.string	"| base_register="
.LC9:
	.string	", base_offset="
.LC10:
	.string	"| base_offset="
	.section	.rodata._ZN2v88internal19EhFrameDisassembler19DumpDwarfDirectivesERSoPKhS4_.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	" not modified from previous frame\n"
	.section	.rodata._ZN2v88internal19EhFrameDisassembler19DumpDwarfDirectivesERSoPKhS4_.str1.1
.LC12:
	.string	"| nop\n"
.LC13:
	.string	"unreachable code"
	.section	.text._ZN2v88internal19EhFrameDisassembler19DumpDwarfDirectivesERSoPKhS4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19EhFrameDisassembler19DumpDwarfDirectivesERSoPKhS4_
	.type	_ZN2v88internal19EhFrameDisassembler19DumpDwarfDirectivesERSoPKhS4_, @function
_ZN2v88internal19EhFrameDisassembler19DumpDwarfDirectivesERSoPKhS4_:
.LFB5897:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	$0, -68(%rbp)
	movq	-24(%rax), %rcx
	addq	%rdi, %rcx
	movq	%rcx, %rax
	movl	24(%rcx), %ecx
	movl	%ecx, -84(%rbp)
	cmpq	%rdx, %rsi
	je	.L30
	movq	%rdi, %rbx
	movq	%rsi, %r12
	leaq	.L42(%rip), %r13
	movq	%rdx, %r14
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	1(%r12), %r15
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	movl	$2, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	(%r12), %eax
	movq	%rax, %r8
	sarl	$6, %eax
	cmpl	$1, %eax
	je	.L87
	cmpl	$2, %eax
	je	.L88
	cmpl	$3, %eax
	je	.L89
	cmpb	$17, %r8b
	ja	.L40
	movslq	0(%r13,%r8,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal19EhFrameDisassembler19DumpDwarfDirectivesERSoPKhS4_,"a",@progbits
	.align 4
	.align 4
.L42:
	.long	.L50-.L42
	.long	.L40-.L42
	.long	.L49-.L42
	.long	.L48-.L42
	.long	.L47-.L42
	.long	.L40-.L42
	.long	.L40-.L42
	.long	.L40-.L42
	.long	.L46-.L42
	.long	.L40-.L42
	.long	.L40-.L42
	.long	.L40-.L42
	.long	.L68-.L42
	.long	.L44-.L42
	.long	.L43-.L42
	.long	.L40-.L42
	.long	.L40-.L42
	.long	.L41-.L42
	.section	.text._ZN2v88internal19EhFrameDisassembler19DumpDwarfDirectivesERSoPKhS4_
.L68:
	movq	%r15, %r12
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
.L45:
	movzbl	(%r12), %edx
	addq	$1, %r12
	movl	%edx, %eax
	andl	$127, %eax
	sall	%cl, %eax
	addl	$7, %ecx
	orl	%eax, %r8d
	testb	%dl, %dl
	js	.L45
	subq	%r15, %r12
	xorl	%ecx, %ecx
	movslq	%r12d, %r12
	addq	%r15, %r12
	xorl	%r15d, %r15d
	movq	%r12, %rdx
	.p2align 4,,10
	.p2align 3
.L56:
	movzbl	(%rdx), %esi
	addq	$1, %rdx
	movl	%esi, %eax
	andl	$127, %eax
	sall	%cl, %eax
	addl	$7, %ecx
	orl	%eax, %r15d
	testb	%sil, %sil
	js	.L56
	subq	%r12, %rdx
	movq	%rbx, %rdi
	leaq	.LC8(%rip), %rsi
	movl	%r8d, -80(%rbp)
	movslq	%edx, %rdx
	addq	%rdx, %r12
	movl	$16, %edx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-80(%rbp), %r8d
	movl	%r8d, %edi
	call	_ZN2v88internal19EhFrameDisassembler25DwarfRegisterCodeToStringEi@PLT
	testq	%rax, %rax
	je	.L90
	movq	%rax, %rdi
	movq	%rax, -80(%rbp)
	call	strlen@PLT
	movq	-80(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L58:
	leaq	.LC9(%rip), %rsi
	movl	$14, %edx
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r15d, %esi
.L86:
	movq	%rbx, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	leaq	-57(%rbp), %rsi
	movl	$1, %edx
	movb	$10, -57(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	%r14, %r12
	jne	.L29
	.p2align 4,,10
	.p2align 3
.L93:
	movq	(%rbx), %rax
	addq	-24(%rax), %rbx
	movq	%rbx, %rax
.L30:
	movl	-84(%rbp), %ebx
	movl	%ebx, 24(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L91
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L46:
	.cfi_restore_state
	movq	%rbx, %rdi
	movl	$2, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r15, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L63:
	movzbl	(%r12), %edx
	addq	$1, %r12
	movl	%edx, %eax
	andl	$127, %eax
	sall	%cl, %eax
	addl	$7, %ecx
	orl	%eax, %edi
	testb	%dl, %dl
	js	.L63
	call	_ZN2v88internal19EhFrameDisassembler25DwarfRegisterCodeToStringEi@PLT
	subq	%r15, %r12
	movslq	%r12d, %r12
	addq	%r15, %r12
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L92
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L65:
	movl	$34, %edx
	leaq	.LC11(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L32
.L48:
	movzwl	1(%r12), %r8d
	addq	$3, %r12
.L85:
	imull	_ZN2v88internal16EhFrameConstants20kCodeAlignmentFactorE(%rip), %r8d
	addl	%r8d, -68(%rbp)
	movl	$12, %edx
	movq	%rbx, %rdi
	movl	-68(%rbp), %r15d
	leaq	.LC2(%rip), %rsi
	movl	%r8d, -80(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-80(%rbp), %r8d
	movq	%r15, %rdi
	movl	%r8d, %esi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L32:
	cmpq	%r14, %r12
	jne	.L29
	jmp	.L93
.L47:
	movl	1(%r12), %r8d
	addq	$5, %r12
	jmp	.L85
.L49:
	movzbl	1(%r12), %r8d
	addq	$2, %r12
	jmp	.L85
.L50:
	movl	$6, %edx
	leaq	.LC12(%rip), %rsi
	movq	%rbx, %rdi
	movq	%r15, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L32
.L41:
	movq	%rbx, %rdi
	movl	$2, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r15, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L51:
	movzbl	(%r12), %edx
	addq	$1, %r12
	movl	%edx, %eax
	andl	$127, %eax
	sall	%cl, %eax
	addl	$7, %ecx
	orl	%eax, %edi
	testb	%dl, %dl
	js	.L51
	call	_ZN2v88internal19EhFrameDisassembler25DwarfRegisterCodeToStringEi@PLT
	subq	%r15, %r12
	movslq	%r12d, %r12
	addq	%r15, %r12
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L94
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L53:
	movq	%r12, %rdx
	xorl	%ecx, %ecx
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L54:
	movzbl	(%rdx), %esi
	addq	$1, %rdx
	movl	%esi, %eax
	andl	$127, %eax
	sall	%cl, %eax
	addl	$7, %ecx
	orl	%eax, %r15d
	testb	%sil, %sil
	js	.L54
	andl	$64, %esi
	je	.L55
	movq	$-1, %rax
	salq	%cl, %rax
	orl	%eax, %r15d
.L55:
	subq	%r12, %rdx
	leaq	.LC6(%rip), %rsi
	movq	%rbx, %rdi
	movslq	%edx, %rdx
	addq	%rdx, %r12
	movl	$14, %edx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movl	%r15d, %esi
	movq	-24(%rax), %rcx
	addq	%rbx, %rcx
	orl	$2048, 24(%rcx)
.L84:
	imull	_ZN2v88internal16EhFrameConstants20kDataAlignmentFactorE(%rip), %esi
	movq	%rbx, %rdi
	call	_ZNSolsEi@PLT
	leaq	-57(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	-24(%rax), %rcx
	addq	%rdi, %rcx
	andl	$-2049, 24(%rcx)
	movb	$10, -57(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L32
.L43:
	leaq	.LC10(%rip), %rsi
	movl	$14, %edx
	movq	%rbx, %rdi
	movq	%r15, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L59:
	movzbl	(%r12), %edx
	addq	$1, %r12
	movl	%edx, %eax
	andl	$127, %eax
	sall	%cl, %eax
	addl	$7, %ecx
	orl	%eax, %esi
	testb	%dl, %dl
	js	.L59
	subq	%r15, %r12
	movslq	%r12d, %r12
	addq	%r15, %r12
	jmp	.L86
.L44:
	movq	%rbx, %rdi
	movl	$16, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r15, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L60:
	movzbl	(%r12), %edx
	addq	$1, %r12
	movl	%edx, %eax
	andl	$127, %eax
	sall	%cl, %eax
	addl	$7, %ecx
	orl	%eax, %edi
	testb	%dl, %dl
	js	.L60
	call	_ZN2v88internal19EhFrameDisassembler25DwarfRegisterCodeToStringEi@PLT
	subq	%r15, %r12
	movslq	%r12d, %r12
	addq	%r15, %r12
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L95
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L62:
	leaq	-57(%rbp), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	movb	$10, -57(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L87:
	andl	$63, %r8d
	imull	_ZN2v88internal16EhFrameConstants20kCodeAlignmentFactorE(%rip), %r8d
	addl	%r8d, -68(%rbp)
	movl	$12, %edx
	movl	-68(%rbp), %r12d
	leaq	.LC2(%rip), %rsi
	movq	%rbx, %rdi
	movl	%r8d, -80(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-80(%rbp), %r8d
	movq	%r12, %rdi
	movq	%r15, %r12
	movl	%r8d, %esi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L89:
	movl	$2, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rbx, %rdi
	movb	%r8b, -80(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	-80(%rbp), %r8d
	movl	%r8d, %edi
	andl	$63, %edi
	call	_ZN2v88internal19EhFrameDisassembler25DwarfRegisterCodeToStringEi@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L96
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L39:
	movl	$21, %edx
	leaq	.LC7(%rip), %rsi
	movq	%rbx, %rdi
	movq	%r15, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L88:
	movq	%r15, %r12
	xorl	%ecx, %ecx
	xorl	%r10d, %r10d
	.p2align 4,,10
	.p2align 3
.L34:
	movzbl	(%r12), %edx
	addq	$1, %r12
	movl	%edx, %eax
	andl	$127, %eax
	sall	%cl, %eax
	addl	$7, %ecx
	orl	%eax, %r10d
	testb	%dl, %dl
	js	.L34
	movl	$2, %edx
	movq	%rbx, %rdi
	movl	%r10d, -72(%rbp)
	subq	%r15, %r12
	leaq	.LC5(%rip), %rsi
	movb	%r8b, -80(%rbp)
	movslq	%r12d, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	-80(%rbp), %r8d
	addq	%r15, %r12
	movl	%r8d, %edi
	andl	$63, %edi
	call	_ZN2v88internal19EhFrameDisassembler25DwarfRegisterCodeToStringEi@PLT
	movl	-72(%rbp), %r10d
	testq	%rax, %rax
	movq	%rax, %r15
	movl	%r10d, -80(%rbp)
	je	.L97
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-80(%rbp), %r10d
.L36:
	leaq	.LC6(%rip), %rsi
	movl	$14, %edx
	movq	%rbx, %rdi
	movl	%r10d, -80(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movl	-80(%rbp), %r10d
	movq	-24(%rax), %rcx
	movl	%r10d, %esi
	addq	%rbx, %rcx
	orl	$2048, 24(%rcx)
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L96:
	movq	(%rbx), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L94:
	movq	(%rbx), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L90:
	movq	(%rbx), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L92:
	movq	(%rbx), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L95:
	movq	(%rbx), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L97:
	movq	(%rbx), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	movl	-80(%rbp), %r10d
	jmp	.L36
.L40:
	leaq	.LC13(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L91:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5897:
	.size	_ZN2v88internal19EhFrameDisassembler19DumpDwarfDirectivesERSoPKhS4_, .-_ZN2v88internal19EhFrameDisassembler19DumpDwarfDirectivesERSoPKhS4_
	.section	.rodata._ZN2v88internal19EhFrameDisassembler19DisassembleToStreamERSo.str1.1,"aMS",@progbits,1
.LC14:
	.string	"  .eh_frame: CIE\n"
.LC15:
	.string	"  .eh_frame: FDE\n"
.LC16:
	.string	"  | procedure_offset="
.LC17:
	.string	"  | procedure_size="
.LC18:
	.string	"  .eh_frame: terminator\n"
.LC19:
	.string	"  .eh_frame_hdr\n"
	.section	.text._ZN2v88internal19EhFrameDisassembler19DisassembleToStreamERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19EhFrameDisassembler19DisassembleToStreamERSo
	.type	_ZN2v88internal19EhFrameDisassembler19DisassembleToStreamERSo, @function
_ZN2v88internal19EhFrameDisassembler19DisassembleToStreamERSo:
.LFB5898:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r15), %r13d
	leal	4(%r13), %esi
	movslq	%esi, %rbx
	movq	%r15, %rsi
	leaq	(%r15,%rbx), %r8
	movq	%r8, -72(%rbp)
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	movl	$17, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-72(%rbp), %r8
	leaq	19(%r15), %rsi
	movq	%r12, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal19EhFrameDisassembler19DumpDwarfDirectivesERSoPKhS4_
	addq	(%r14), %rbx
	movq	%r12, %rdi
	movl	8(%rbx), %r9d
	leaq	12(%rbx), %r8
	leaq	8(%rbx), %r10
	movq	%rbx, %rsi
	movq	%r8, -80(%rbp)
	movl	12(%rbx), %ebx
	movl	%r9d, -72(%rbp)
	movq	%r10, -88(%rbp)
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	movl	$17, %edx
	leaq	.LC15(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-88(%rbp), %r10
	movq	%r15, %rdi
	movq	%r10, %rsi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	movl	$21, %edx
	leaq	.LC16(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-72(%rbp), %r9d
	movq	%r15, %rdi
	movl	%r9d, %esi
	call	_ZNSolsEi@PLT
	leaq	-57(%rbp), %r9
	movl	$1, %edx
	movb	$10, -57(%rbp)
	movq	%r9, %rsi
	movq	%rax, %rdi
	movq	%r9, -72(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-80(%rbp), %r8
	movq	%rax, %rdi
	movq	%r8, %rsi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	movl	$19, %edx
	leaq	.LC17(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	-72(%rbp), %r9
	movl	$1, %edx
	movb	$10, -57(%rbp)
	movq	%rax, %rdi
	movq	%r9, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r14), %rbx
	leal	21(%r13), %esi
	movq	%r12, %rdi
	movslq	%esi, %rsi
	addq	(%r14), %rsi
	leaq	-24(%rbx), %r15
	movq	%r15, %rdx
	call	_ZN2v88internal19EhFrameDisassembler19DumpDwarfDirectivesERSoPKhS4_
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	movl	$24, %edx
	leaq	.LC18(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-20(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	movl	$16, %edx
	leaq	.LC19(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L101
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L101:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5898:
	.size	_ZN2v88internal19EhFrameDisassembler19DisassembleToStreamERSo, .-_ZN2v88internal19EhFrameDisassembler19DisassembleToStreamERSo
	.section	.rodata._ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_.str1.1,"aMS",@progbits,1
.LC20:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	.type	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_, @function
_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_:
.LFB6354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	movq	8(%rdi), %r12
	movq	%r13, %rax
	subq	%r12, %rax
	cmpq	$2147483647, %rax
	je	.L140
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r14
	movq	%rsi, %rbx
	subq	%r12, %rdx
	testq	%rax, %rax
	je	.L120
	leaq	(%rax,%rax), %rdi
	movl	$2147483648, %esi
	movl	$2147483647, %ecx
	cmpq	%rdi, %rax
	jbe	.L141
.L104:
	movq	(%r14), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r8
	subq	%rax, %r8
	cmpq	%rsi, %r8
	jb	.L142
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L107:
	leaq	(%rax,%rcx), %rsi
	leaq	1(%rax), %rcx
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L141:
	testq	%rdi, %rdi
	jne	.L143
	movl	$1, %ecx
	xorl	%esi, %esi
	xorl	%eax, %eax
.L105:
	movzbl	(%r15), %edi
	movb	%dil, (%rax,%rdx)
	cmpq	%r12, %rbx
	je	.L108
	leaq	-1(%rbx), %rdx
	subq	%r12, %rdx
	cmpq	$14, %rdx
	jbe	.L109
	leaq	15(%rax), %rdx
	subq	%r12, %rdx
	cmpq	$30, %rdx
	jbe	.L109
	movq	%rbx, %r8
	xorl	%edx, %edx
	subq	%r12, %r8
	movq	%r8, %rcx
	andq	$-16, %rcx
	.p2align 4,,10
	.p2align 3
.L110:
	movdqu	(%r12,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L110
	movq	%r8, %rdi
	andq	$-16, %rdi
	leaq	(%r12,%rdi), %rdx
	leaq	(%rax,%rdi), %rcx
	cmpq	%rdi, %r8
	je	.L113
	movzbl	(%rdx), %edi
	movb	%dil, (%rcx)
	leaq	1(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L113
	movzbl	1(%rdx), %edi
	movb	%dil, 1(%rcx)
	leaq	2(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L113
	movzbl	2(%rdx), %edi
	movb	%dil, 2(%rcx)
	leaq	3(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L113
	movzbl	3(%rdx), %edi
	movb	%dil, 3(%rcx)
	leaq	4(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L113
	movzbl	4(%rdx), %edi
	movb	%dil, 4(%rcx)
	leaq	5(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L113
	movzbl	5(%rdx), %edi
	movb	%dil, 5(%rcx)
	leaq	6(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L113
	movzbl	6(%rdx), %edi
	movb	%dil, 6(%rcx)
	leaq	7(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L113
	movzbl	7(%rdx), %edi
	movb	%dil, 7(%rcx)
	leaq	8(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L113
	movzbl	8(%rdx), %edi
	movb	%dil, 8(%rcx)
	leaq	9(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L113
	movzbl	9(%rdx), %edi
	movb	%dil, 9(%rcx)
	leaq	10(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L113
	movzbl	10(%rdx), %edi
	movb	%dil, 10(%rcx)
	leaq	11(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L113
	movzbl	11(%rdx), %edi
	movb	%dil, 11(%rcx)
	leaq	12(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L113
	movzbl	12(%rdx), %edi
	movb	%dil, 12(%rcx)
	leaq	13(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L113
	movzbl	13(%rdx), %edi
	movb	%dil, 13(%rcx)
	leaq	14(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L113
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rcx)
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%rbx, %rdx
	subq	%r12, %rdx
	leaq	1(%rax,%rdx), %rcx
.L108:
	cmpq	%r13, %rbx
	je	.L114
	leaq	15(%rbx), %rdx
	subq	%rcx, %rdx
	cmpq	$30, %rdx
	jbe	.L115
	movq	%rbx, %rdx
	notq	%rdx
	addq	%r13, %rdx
	cmpq	$14, %rdx
	jbe	.L115
	movq	%r13, %r9
	xorl	%edx, %edx
	subq	%rbx, %r9
	movq	%r9, %rdi
	andq	$-16, %rdi
	.p2align 4,,10
	.p2align 3
.L116:
	movdqu	(%rbx,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L116
	movq	%r9, %r8
	andq	$-16, %r8
	leaq	(%rcx,%r8), %rdi
	leaq	(%rbx,%r8), %rdx
	cmpq	%r8, %r9
	je	.L119
	movzbl	(%rdx), %r8d
	movb	%r8b, (%rdi)
	leaq	1(%rdx), %r8
	cmpq	%r8, %r13
	je	.L119
	movzbl	1(%rdx), %r8d
	movb	%r8b, 1(%rdi)
	leaq	2(%rdx), %r8
	cmpq	%r8, %r13
	je	.L119
	movzbl	2(%rdx), %r8d
	movb	%r8b, 2(%rdi)
	leaq	3(%rdx), %r8
	cmpq	%r8, %r13
	je	.L119
	movzbl	3(%rdx), %r8d
	movb	%r8b, 3(%rdi)
	leaq	4(%rdx), %r8
	cmpq	%r8, %r13
	je	.L119
	movzbl	4(%rdx), %r8d
	movb	%r8b, 4(%rdi)
	leaq	5(%rdx), %r8
	cmpq	%r8, %r13
	je	.L119
	movzbl	5(%rdx), %r8d
	movb	%r8b, 5(%rdi)
	leaq	6(%rdx), %r8
	cmpq	%r8, %r13
	je	.L119
	movzbl	6(%rdx), %r8d
	movb	%r8b, 6(%rdi)
	leaq	7(%rdx), %r8
	cmpq	%r8, %r13
	je	.L119
	movzbl	7(%rdx), %r8d
	movb	%r8b, 7(%rdi)
	leaq	8(%rdx), %r8
	cmpq	%r8, %r13
	je	.L119
	movzbl	8(%rdx), %r8d
	movb	%r8b, 8(%rdi)
	leaq	9(%rdx), %r8
	cmpq	%r8, %r13
	je	.L119
	movzbl	9(%rdx), %r8d
	movb	%r8b, 9(%rdi)
	leaq	10(%rdx), %r8
	cmpq	%r8, %r13
	je	.L119
	movzbl	10(%rdx), %r8d
	movb	%r8b, 10(%rdi)
	leaq	11(%rdx), %r8
	cmpq	%r8, %r13
	je	.L119
	movzbl	11(%rdx), %r8d
	movb	%r8b, 11(%rdi)
	leaq	12(%rdx), %r8
	cmpq	%r8, %r13
	je	.L119
	movzbl	12(%rdx), %r8d
	movb	%r8b, 12(%rdi)
	leaq	13(%rdx), %r8
	cmpq	%r8, %r13
	je	.L119
	movzbl	13(%rdx), %r8d
	movb	%r8b, 13(%rdi)
	leaq	14(%rdx), %r8
	cmpq	%r8, %r13
	je	.L119
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rdi)
	.p2align 4,,10
	.p2align 3
.L119:
	subq	%rbx, %r13
	addq	%r13, %rcx
.L114:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rsi, 24(%r14)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	movl	$8, %esi
	movl	$1, %ecx
	jmp	.L104
.L142:
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L115:
	movq	%r13, %r8
	xorl	%edx, %edx
	subq	%rbx, %r8
	.p2align 4,,10
	.p2align 3
.L118:
	movzbl	(%rbx,%rdx), %edi
	movb	%dil, (%rcx,%rdx)
	addq	$1, %rdx
	cmpq	%r8, %rdx
	jne	.L118
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L109:
	movq	%rbx, %rdi
	xorl	%edx, %edx
	subq	%r12, %rdi
	.p2align 4,,10
	.p2align 3
.L112:
	movzbl	(%r12,%rdx), %ecx
	movb	%cl, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%rdi, %rdx
	jne	.L112
	jmp	.L113
.L143:
	cmpq	%rsi, %rdi
	cmovb	%rdi, %rcx
	leaq	7(%rcx), %rsi
	andq	$-8, %rsi
	jmp	.L104
.L140:
	leaq	.LC20(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE6354:
	.size	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_, .-_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	.section	.text._ZN2v88internal13EhFrameWriter12WriteULeb128Ej,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13EhFrameWriter12WriteULeb128Ej
	.type	_ZN2v88internal13EhFrameWriter12WriteULeb128Ej, @function
_ZN2v88internal13EhFrameWriter12WriteULeb128Ej:
.LFB5885:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-41(%rbp), %r14
	leaq	24(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.L149:
	movl	%ebx, %eax
	andl	$127, %eax
	shrl	$7, %ebx
	je	.L145
.L158:
	orl	$-128, %eax
	movq	40(%r12), %rsi
	movb	%al, -41(%rbp)
	cmpq	48(%r12), %rsi
	je	.L157
	movb	%al, (%rsi)
	movl	%ebx, %eax
	addq	$1, 40(%r12)
	andl	$127, %eax
	shrl	$7, %ebx
	jne	.L158
.L145:
	movb	%al, -41(%rbp)
	movq	40(%r12), %rsi
	cmpq	48(%r12), %rsi
	je	.L159
	movb	%al, (%rsi)
	addq	$1, 40(%r12)
.L144:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L160
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L159:
	leaq	-41(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	jmp	.L144
.L160:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5885:
	.size	_ZN2v88internal13EhFrameWriter12WriteULeb128Ej, .-_ZN2v88internal13EhFrameWriter12WriteULeb128Ej
	.section	.text._ZN2v88internal13EhFrameWriter12WriteSLeb128Ei,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13EhFrameWriter12WriteSLeb128Ei
	.type	_ZN2v88internal13EhFrameWriter12WriteSLeb128Ei, @function
_ZN2v88internal13EhFrameWriter12WriteSLeb128Ei:
.LFB5886:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-41(%rbp), %r14
	leaq	24(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.L166:
	movl	%ebx, %eax
	movl	%ebx, %edx
	andl	$127, %eax
	sarl	$7, %ebx
	jne	.L162
.L177:
	andl	$64, %edx
	je	.L163
.L164:
	orl	$-128, %eax
	movq	40(%r12), %rsi
	movb	%al, -41(%rbp)
	cmpq	48(%r12), %rsi
	je	.L165
	movb	%al, (%rsi)
	movl	%ebx, %eax
	movl	%ebx, %edx
	addq	$1, 40(%r12)
	andl	$127, %eax
	sarl	$7, %ebx
	je	.L177
.L162:
	cmpl	$-1, %ebx
	jne	.L164
	andl	$64, %edx
	je	.L164
.L163:
	movb	%al, -41(%rbp)
	movq	40(%r12), %rsi
	cmpq	48(%r12), %rsi
	je	.L178
	movb	%al, (%rsi)
	addq	$1, 40(%r12)
.L161:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L179
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	leaq	-41(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	jmp	.L161
.L165:
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	jmp	.L166
.L179:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5886:
	.size	_ZN2v88internal13EhFrameWriter12WriteSLeb128Ei, .-_ZN2v88internal13EhFrameWriter12WriteSLeb128Ei
	.section	.text._ZN2v88internal13EhFrameWriter32RecordRegisterFollowsInitialRuleENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13EhFrameWriter32RecordRegisterFollowsInitialRuleENS0_8RegisterE
	.type	_ZN2v88internal13EhFrameWriter32RecordRegisterFollowsInitialRuleENS0_8RegisterE, @function
_ZN2v88internal13EhFrameWriter32RecordRegisterFollowsInitialRuleENS0_8RegisterE:
.LFB5882:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	%esi, %edi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal13EhFrameWriter19RegisterToDwarfCodeENS0_8RegisterE@PLT
	movq	40(%rbx), %rsi
	orl	$-64, %eax
	movb	%al, -25(%rbp)
	cmpq	48(%rbx), %rsi
	je	.L181
	movb	%al, (%rsi)
	addq	$1, 40(%rbx)
.L180:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L185
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	leaq	-25(%rbp), %rdx
	leaq	24(%rbx), %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	jmp	.L180
.L185:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5882:
	.size	_ZN2v88internal13EhFrameWriter32RecordRegisterFollowsInitialRuleENS0_8RegisterE, .-_ZN2v88internal13EhFrameWriter32RecordRegisterFollowsInitialRuleENS0_8RegisterE
	.section	.rodata._ZN2v88internal13EhFrameWriter14WriteFdeHeaderEv.str1.1,"aMS",@progbits,1
.LC21:
	.string	"vector::_M_range_insert"
	.section	.text._ZN2v88internal13EhFrameWriter14WriteFdeHeaderEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13EhFrameWriter14WriteFdeHeaderEv
	.type	_ZN2v88internal13EhFrameWriter14WriteFdeHeaderEv, @function
_ZN2v88internal13EhFrameWriter14WriteFdeHeaderEv:
.LFB5873:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	40(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	48(%rdi), %rax
	movl	$-559038242, -44(%rbp)
	subq	%r13, %rax
	cmpq	$3, %rax
	jbe	.L187
	movl	$-559038242, 0(%r13)
	movq	40(%rdi), %rax
	movq	48(%rdi), %rdx
	leaq	4(%rax), %r12
	movq	%r12, 40(%rdi)
.L188:
	movl	(%rbx), %eax
	subq	%r12, %rdx
	addl	$4, %eax
	movl	%eax, -48(%rbp)
	cmpq	$3, %rdx
	jbe	.L208
	movl	%eax, (%r12)
	movq	40(%rbx), %rax
	movq	48(%rbx), %rcx
	movl	$-559038242, -52(%rbp)
	leaq	4(%rax), %r13
	subq	%r13, %rcx
	movq	%r13, 40(%rbx)
	cmpq	$3, %rcx
	jbe	.L229
.L356:
	movl	$-559038242, 0(%r13)
	movq	40(%rbx), %rax
	movq	48(%rbx), %rdx
	movl	$-559038242, -56(%rbp)
	leaq	4(%rax), %r12
	subq	%r12, %rdx
	movq	%r12, 40(%rbx)
	cmpq	$3, %rdx
	jbe	.L249
.L358:
	movl	$-559038242, (%r12)
	movq	40(%rbx), %rax
	movq	48(%rbx), %r8
	movb	$0, -44(%rbp)
	leaq	4(%rax), %rsi
	movq	%rsi, 40(%rbx)
	cmpq	%r8, %rsi
	je	.L269
.L360:
	movb	$0, (%rsi)
	addq	$1, 40(%rbx)
.L186:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L353
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movq	32(%rdi), %rsi
	movq	%r13, %rcx
	movl	$2147483647, %r12d
	movq	%r12, %rax
	subq	%rsi, %rcx
	subq	%rcx, %rax
	cmpq	$3, %rax
	jbe	.L210
	movl	$4, %edx
	cmpq	$4, %rcx
	movq	%rdx, %rax
	cmovnb	%rcx, %rax
	addq	%rcx, %rax
	jc	.L191
	testq	%rax, %rax
	jne	.L354
	xorl	%edx, %edx
	xorl	%eax, %eax
.L193:
	cmpq	%rsi, %r13
	je	.L274
	leaq	-1(%r13), %rcx
	subq	%rsi, %rcx
	cmpq	$14, %rcx
	jbe	.L197
	leaq	15(%rax), %rcx
	subq	%rsi, %rcx
	cmpq	$30, %rcx
	jbe	.L197
	movq	%r13, %r9
	xorl	%ecx, %ecx
	subq	%rsi, %r9
	movq	%r9, %rdi
	andq	$-16, %rdi
	.p2align 4,,10
	.p2align 3
.L198:
	movdqu	(%rsi,%rcx), %xmm1
	movups	%xmm1, (%rax,%rcx)
	addq	$16, %rcx
	cmpq	%rdi, %rcx
	jne	.L198
	movq	%r9, %r8
	andq	$-16, %r8
	leaq	(%rsi,%r8), %rcx
	leaq	(%rax,%r8), %rdi
	cmpq	%r8, %r9
	je	.L201
	movzbl	(%rcx), %r8d
	movb	%r8b, (%rdi)
	leaq	1(%rcx), %r8
	cmpq	%r8, %r13
	je	.L201
	movzbl	1(%rcx), %r8d
	movb	%r8b, 1(%rdi)
	leaq	2(%rcx), %r8
	cmpq	%r8, %r13
	je	.L201
	movzbl	2(%rcx), %r8d
	movb	%r8b, 2(%rdi)
	leaq	3(%rcx), %r8
	cmpq	%r8, %r13
	je	.L201
	movzbl	3(%rcx), %r8d
	movb	%r8b, 3(%rdi)
	leaq	4(%rcx), %r8
	cmpq	%r8, %r13
	je	.L201
	movzbl	4(%rcx), %r8d
	movb	%r8b, 4(%rdi)
	leaq	5(%rcx), %r8
	cmpq	%r8, %r13
	je	.L201
	movzbl	5(%rcx), %r8d
	movb	%r8b, 5(%rdi)
	leaq	6(%rcx), %r8
	cmpq	%r8, %r13
	je	.L201
	movzbl	6(%rcx), %r8d
	movb	%r8b, 6(%rdi)
	leaq	7(%rcx), %r8
	cmpq	%r8, %r13
	je	.L201
	movzbl	7(%rcx), %r8d
	movb	%r8b, 7(%rdi)
	leaq	8(%rcx), %r8
	cmpq	%r8, %r13
	je	.L201
	movzbl	8(%rcx), %r8d
	movb	%r8b, 8(%rdi)
	leaq	9(%rcx), %r8
	cmpq	%r8, %r13
	je	.L201
	movzbl	9(%rcx), %r8d
	movb	%r8b, 9(%rdi)
	leaq	10(%rcx), %r8
	cmpq	%r8, %r13
	je	.L201
	movzbl	10(%rcx), %r8d
	movb	%r8b, 10(%rdi)
	leaq	11(%rcx), %r8
	cmpq	%r8, %r13
	je	.L201
	movzbl	11(%rcx), %r8d
	movb	%r8b, 11(%rdi)
	leaq	12(%rcx), %r8
	cmpq	%r8, %r13
	je	.L201
	movzbl	12(%rcx), %r8d
	movb	%r8b, 12(%rdi)
	leaq	13(%rcx), %r8
	cmpq	%r8, %r13
	je	.L201
	movzbl	13(%rcx), %r8d
	movb	%r8b, 13(%rdi)
	leaq	14(%rcx), %r8
	cmpq	%r8, %r13
	je	.L201
	movzbl	14(%rcx), %ecx
	movb	%cl, 14(%rdi)
	.p2align 4,,10
	.p2align 3
.L201:
	movq	%r13, %rdi
	subq	%rsi, %rdi
	addq	%rax, %rdi
.L196:
	movl	-44(%rbp), %ecx
	leaq	4(%rdi), %r12
	movl	%ecx, (%rdi)
	movq	40(%rbx), %rsi
	cmpq	%rsi, %r13
	je	.L202
	leaq	16(%r13), %rcx
	movq	%rsi, %r9
	cmpq	%rcx, %r12
	leaq	20(%rdi), %rcx
	setnb	%r8b
	cmpq	%rcx, %r13
	setnb	%cl
	subq	%r13, %r9
	orb	%cl, %r8b
	je	.L203
	leaq	-1(%rsi), %rcx
	subq	%r13, %rcx
	cmpq	$14, %rcx
	jbe	.L203
	movq	%r9, %r8
	xorl	%ecx, %ecx
	andq	$-16, %r8
	.p2align 4,,10
	.p2align 3
.L204:
	movdqu	0(%r13,%rcx), %xmm2
	movups	%xmm2, 4(%rdi,%rcx)
	addq	$16, %rcx
	cmpq	%r8, %rcx
	jne	.L204
	movq	%r9, %r8
	andq	$-16, %r8
	leaq	0(%r13,%r8), %rcx
	leaq	(%r12,%r8), %rdi
	cmpq	%r8, %r9
	je	.L207
	movzbl	(%rcx), %r8d
	movb	%r8b, (%rdi)
	leaq	1(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L207
	movzbl	1(%rcx), %r8d
	movb	%r8b, 1(%rdi)
	leaq	2(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L207
	movzbl	2(%rcx), %r8d
	movb	%r8b, 2(%rdi)
	leaq	3(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L207
	movzbl	3(%rcx), %r8d
	movb	%r8b, 3(%rdi)
	leaq	4(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L207
	movzbl	4(%rcx), %r8d
	movb	%r8b, 4(%rdi)
	leaq	5(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L207
	movzbl	5(%rcx), %r8d
	movb	%r8b, 5(%rdi)
	leaq	6(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L207
	movzbl	6(%rcx), %r8d
	movb	%r8b, 6(%rdi)
	leaq	7(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L207
	movzbl	7(%rcx), %r8d
	movb	%r8b, 7(%rdi)
	leaq	8(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L207
	movzbl	8(%rcx), %r8d
	movb	%r8b, 8(%rdi)
	leaq	9(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L207
	movzbl	9(%rcx), %r8d
	movb	%r8b, 9(%rdi)
	leaq	10(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L207
	movzbl	10(%rcx), %r8d
	movb	%r8b, 10(%rdi)
	leaq	11(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L207
	movzbl	11(%rcx), %r8d
	movb	%r8b, 11(%rdi)
	leaq	12(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L207
	movzbl	12(%rcx), %r8d
	movb	%r8b, 12(%rdi)
	leaq	13(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L207
	movzbl	13(%rcx), %r8d
	movb	%r8b, 13(%rdi)
	leaq	14(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L207
	movzbl	14(%rcx), %ecx
	movb	%cl, 14(%rdi)
	.p2align 4,,10
	.p2align 3
.L207:
	subq	%r13, %rsi
	addq	%rsi, %r12
.L202:
	movq	%rax, %xmm0
	movq	%r12, %xmm2
	movq	%rdx, 48(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 32(%rbx)
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L208:
	movq	32(%rbx), %rsi
	movq	%r12, %rcx
	movl	$2147483647, %r13d
	movq	%r13, %rax
	subq	%rsi, %rcx
	subq	%rcx, %rax
	cmpq	$3, %rax
	jbe	.L210
	movl	$4, %edx
	cmpq	$4, %rcx
	movq	%rdx, %rax
	cmovnb	%rcx, %rax
	addq	%rcx, %rax
	jc	.L212
	testq	%rax, %rax
	jne	.L355
	xorl	%ecx, %ecx
	xorl	%eax, %eax
.L214:
	cmpq	%r12, %rsi
	je	.L277
	leaq	15(%rax), %rdx
	subq	%rsi, %rdx
	cmpq	$30, %rdx
	jbe	.L218
	leaq	-1(%r12), %rdx
	subq	%rsi, %rdx
	cmpq	$14, %rdx
	jbe	.L218
	movq	%r12, %r9
	xorl	%edx, %edx
	subq	%rsi, %r9
	movq	%r9, %rdi
	andq	$-16, %rdi
	.p2align 4,,10
	.p2align 3
.L219:
	movdqu	(%rsi,%rdx), %xmm3
	movups	%xmm3, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L219
	movq	%r9, %r8
	andq	$-16, %r8
	leaq	(%rsi,%r8), %rdx
	leaq	(%rax,%r8), %rdi
	cmpq	%r8, %r9
	je	.L222
	movzbl	(%rdx), %r8d
	movb	%r8b, (%rdi)
	leaq	1(%rdx), %r8
	cmpq	%r8, %r12
	je	.L222
	movzbl	1(%rdx), %r8d
	movb	%r8b, 1(%rdi)
	leaq	2(%rdx), %r8
	cmpq	%r8, %r12
	je	.L222
	movzbl	2(%rdx), %r8d
	movb	%r8b, 2(%rdi)
	leaq	3(%rdx), %r8
	cmpq	%r8, %r12
	je	.L222
	movzbl	3(%rdx), %r8d
	movb	%r8b, 3(%rdi)
	leaq	4(%rdx), %r8
	cmpq	%r8, %r12
	je	.L222
	movzbl	4(%rdx), %r8d
	movb	%r8b, 4(%rdi)
	leaq	5(%rdx), %r8
	cmpq	%r8, %r12
	je	.L222
	movzbl	5(%rdx), %r8d
	movb	%r8b, 5(%rdi)
	leaq	6(%rdx), %r8
	cmpq	%r8, %r12
	je	.L222
	movzbl	6(%rdx), %r8d
	movb	%r8b, 6(%rdi)
	leaq	7(%rdx), %r8
	cmpq	%r8, %r12
	je	.L222
	movzbl	7(%rdx), %r8d
	movb	%r8b, 7(%rdi)
	leaq	8(%rdx), %r8
	cmpq	%r8, %r12
	je	.L222
	movzbl	8(%rdx), %r8d
	movb	%r8b, 8(%rdi)
	leaq	9(%rdx), %r8
	cmpq	%r8, %r12
	je	.L222
	movzbl	9(%rdx), %r8d
	movb	%r8b, 9(%rdi)
	leaq	10(%rdx), %r8
	cmpq	%r8, %r12
	je	.L222
	movzbl	10(%rdx), %r8d
	movb	%r8b, 10(%rdi)
	leaq	11(%rdx), %r8
	cmpq	%r8, %r12
	je	.L222
	movzbl	11(%rdx), %r8d
	movb	%r8b, 11(%rdi)
	leaq	12(%rdx), %r8
	cmpq	%r8, %r12
	je	.L222
	movzbl	12(%rdx), %r8d
	movb	%r8b, 12(%rdi)
	leaq	13(%rdx), %r8
	cmpq	%r8, %r12
	je	.L222
	movzbl	13(%rdx), %r8d
	movb	%r8b, 13(%rdi)
	leaq	14(%rdx), %r8
	cmpq	%r8, %r12
	je	.L222
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rdi)
	.p2align 4,,10
	.p2align 3
.L222:
	movq	%r12, %rdi
	subq	%rsi, %rdi
	addq	%rax, %rdi
.L217:
	movl	-48(%rbp), %edx
	leaq	4(%rdi), %r13
	movl	%edx, (%rdi)
	movq	40(%rbx), %rsi
	cmpq	%r12, %rsi
	je	.L223
	leaq	20(%rdi), %rdx
	movq	%rsi, %r9
	cmpq	%rdx, %r12
	leaq	16(%r12), %rdx
	setnb	%r8b
	cmpq	%rdx, %r13
	setnb	%dl
	subq	%r12, %r9
	orb	%dl, %r8b
	je	.L224
	leaq	-1(%rsi), %rdx
	subq	%r12, %rdx
	cmpq	$14, %rdx
	jbe	.L224
	movq	%r9, %r8
	xorl	%edx, %edx
	andq	$-16, %r8
	.p2align 4,,10
	.p2align 3
.L225:
	movdqu	(%r12,%rdx), %xmm4
	movups	%xmm4, 4(%rdi,%rdx)
	addq	$16, %rdx
	cmpq	%r8, %rdx
	jne	.L225
	movq	%r9, %r8
	andq	$-16, %r8
	leaq	(%r12,%r8), %rdx
	leaq	0(%r13,%r8), %rdi
	cmpq	%r8, %r9
	je	.L228
	movzbl	(%rdx), %r8d
	movb	%r8b, (%rdi)
	leaq	1(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L228
	movzbl	1(%rdx), %r8d
	movb	%r8b, 1(%rdi)
	leaq	2(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L228
	movzbl	2(%rdx), %r8d
	movb	%r8b, 2(%rdi)
	leaq	3(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L228
	movzbl	3(%rdx), %r8d
	movb	%r8b, 3(%rdi)
	leaq	4(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L228
	movzbl	4(%rdx), %r8d
	movb	%r8b, 4(%rdi)
	leaq	5(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L228
	movzbl	5(%rdx), %r8d
	movb	%r8b, 5(%rdi)
	leaq	6(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L228
	movzbl	6(%rdx), %r8d
	movb	%r8b, 6(%rdi)
	leaq	7(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L228
	movzbl	7(%rdx), %r8d
	movb	%r8b, 7(%rdi)
	leaq	8(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L228
	movzbl	8(%rdx), %r8d
	movb	%r8b, 8(%rdi)
	leaq	9(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L228
	movzbl	9(%rdx), %r8d
	movb	%r8b, 9(%rdi)
	leaq	10(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L228
	movzbl	10(%rdx), %r8d
	movb	%r8b, 10(%rdi)
	leaq	11(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L228
	movzbl	11(%rdx), %r8d
	movb	%r8b, 11(%rdi)
	leaq	12(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L228
	movzbl	12(%rdx), %r8d
	movb	%r8b, 12(%rdi)
	leaq	13(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L228
	movzbl	13(%rdx), %r8d
	movb	%r8b, 13(%rdi)
	leaq	14(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L228
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rdi)
	.p2align 4,,10
	.p2align 3
.L228:
	subq	%r12, %rsi
	addq	%rsi, %r13
.L223:
	movq	%rax, %xmm0
	movq	%r13, %xmm3
	movq	%rcx, 48(%rbx)
	subq	%r13, %rcx
	punpcklqdq	%xmm3, %xmm0
	movl	$-559038242, -52(%rbp)
	movups	%xmm0, 32(%rbx)
	cmpq	$3, %rcx
	ja	.L356
.L229:
	movq	32(%rbx), %rsi
	movq	%r13, %rax
	movl	$2147483647, %r12d
	movq	%r12, %rdx
	subq	%rsi, %rax
	subq	%rax, %rdx
	cmpq	$3, %rdx
	jbe	.L210
	cmpq	$4, %rax
	movl	$4, %edx
	cmovnb	%rax, %rdx
	addq	%rdx, %rax
	jc	.L232
	testq	%rax, %rax
	jne	.L357
	xorl	%edx, %edx
	xorl	%eax, %eax
.L234:
	cmpq	%rsi, %r13
	je	.L280
	leaq	15(%rax), %rcx
	subq	%rsi, %rcx
	cmpq	$30, %rcx
	jbe	.L238
	leaq	-1(%r13), %rcx
	subq	%rsi, %rcx
	cmpq	$14, %rcx
	jbe	.L238
	movq	%r13, %r9
	xorl	%ecx, %ecx
	subq	%rsi, %r9
	movq	%r9, %rdi
	andq	$-16, %rdi
	.p2align 4,,10
	.p2align 3
.L239:
	movdqu	(%rsi,%rcx), %xmm5
	movups	%xmm5, (%rax,%rcx)
	addq	$16, %rcx
	cmpq	%rdi, %rcx
	jne	.L239
	movq	%r9, %r8
	andq	$-16, %r8
	leaq	(%rsi,%r8), %rcx
	leaq	(%rax,%r8), %rdi
	cmpq	%r8, %r9
	je	.L242
	movzbl	(%rcx), %r8d
	movb	%r8b, (%rdi)
	leaq	1(%rcx), %r8
	cmpq	%r8, %r13
	je	.L242
	movzbl	1(%rcx), %r8d
	movb	%r8b, 1(%rdi)
	leaq	2(%rcx), %r8
	cmpq	%r8, %r13
	je	.L242
	movzbl	2(%rcx), %r8d
	movb	%r8b, 2(%rdi)
	leaq	3(%rcx), %r8
	cmpq	%r8, %r13
	je	.L242
	movzbl	3(%rcx), %r8d
	movb	%r8b, 3(%rdi)
	leaq	4(%rcx), %r8
	cmpq	%r8, %r13
	je	.L242
	movzbl	4(%rcx), %r8d
	movb	%r8b, 4(%rdi)
	leaq	5(%rcx), %r8
	cmpq	%r8, %r13
	je	.L242
	movzbl	5(%rcx), %r8d
	movb	%r8b, 5(%rdi)
	leaq	6(%rcx), %r8
	cmpq	%r8, %r13
	je	.L242
	movzbl	6(%rcx), %r8d
	movb	%r8b, 6(%rdi)
	leaq	7(%rcx), %r8
	cmpq	%r8, %r13
	je	.L242
	movzbl	7(%rcx), %r8d
	movb	%r8b, 7(%rdi)
	leaq	8(%rcx), %r8
	cmpq	%r8, %r13
	je	.L242
	movzbl	8(%rcx), %r8d
	movb	%r8b, 8(%rdi)
	leaq	9(%rcx), %r8
	cmpq	%r8, %r13
	je	.L242
	movzbl	9(%rcx), %r8d
	movb	%r8b, 9(%rdi)
	leaq	10(%rcx), %r8
	cmpq	%r8, %r13
	je	.L242
	movzbl	10(%rcx), %r8d
	movb	%r8b, 10(%rdi)
	leaq	11(%rcx), %r8
	cmpq	%r8, %r13
	je	.L242
	movzbl	11(%rcx), %r8d
	movb	%r8b, 11(%rdi)
	leaq	12(%rcx), %r8
	cmpq	%r8, %r13
	je	.L242
	movzbl	12(%rcx), %r8d
	movb	%r8b, 12(%rdi)
	leaq	13(%rcx), %r8
	cmpq	%r8, %r13
	je	.L242
	movzbl	13(%rcx), %r8d
	movb	%r8b, 13(%rdi)
	leaq	14(%rcx), %r8
	cmpq	%r8, %r13
	je	.L242
	movzbl	14(%rcx), %ecx
	movb	%cl, 14(%rdi)
	.p2align 4,,10
	.p2align 3
.L242:
	movq	%r13, %rdi
	subq	%rsi, %rdi
	addq	%rax, %rdi
.L237:
	movl	-52(%rbp), %ecx
	leaq	4(%rdi), %r12
	movl	%ecx, (%rdi)
	movq	40(%rbx), %rsi
	cmpq	%rsi, %r13
	je	.L243
	leaq	16(%r13), %rcx
	movq	%rsi, %r9
	cmpq	%rcx, %r12
	leaq	20(%rdi), %rcx
	setnb	%r8b
	cmpq	%rcx, %r13
	setnb	%cl
	subq	%r13, %r9
	orb	%cl, %r8b
	je	.L244
	leaq	-1(%rsi), %rcx
	subq	%r13, %rcx
	cmpq	$14, %rcx
	jbe	.L244
	movq	%r9, %r8
	xorl	%ecx, %ecx
	andq	$-16, %r8
	.p2align 4,,10
	.p2align 3
.L245:
	movdqu	0(%r13,%rcx), %xmm6
	movups	%xmm6, 4(%rdi,%rcx)
	addq	$16, %rcx
	cmpq	%r8, %rcx
	jne	.L245
	movq	%r9, %r8
	andq	$-16, %r8
	leaq	0(%r13,%r8), %rcx
	leaq	(%r12,%r8), %rdi
	cmpq	%r9, %r8
	je	.L248
	movzbl	(%rcx), %r8d
	movb	%r8b, (%rdi)
	leaq	1(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L248
	movzbl	1(%rcx), %r8d
	movb	%r8b, 1(%rdi)
	leaq	2(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L248
	movzbl	2(%rcx), %r8d
	movb	%r8b, 2(%rdi)
	leaq	3(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L248
	movzbl	3(%rcx), %r8d
	movb	%r8b, 3(%rdi)
	leaq	4(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L248
	movzbl	4(%rcx), %r8d
	movb	%r8b, 4(%rdi)
	leaq	5(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L248
	movzbl	5(%rcx), %r8d
	movb	%r8b, 5(%rdi)
	leaq	6(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L248
	movzbl	6(%rcx), %r8d
	movb	%r8b, 6(%rdi)
	leaq	7(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L248
	movzbl	7(%rcx), %r8d
	movb	%r8b, 7(%rdi)
	leaq	8(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L248
	movzbl	8(%rcx), %r8d
	movb	%r8b, 8(%rdi)
	leaq	9(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L248
	movzbl	9(%rcx), %r8d
	movb	%r8b, 9(%rdi)
	leaq	10(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L248
	movzbl	10(%rcx), %r8d
	movb	%r8b, 10(%rdi)
	leaq	11(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L248
	movzbl	11(%rcx), %r8d
	movb	%r8b, 11(%rdi)
	leaq	12(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L248
	movzbl	12(%rcx), %r8d
	movb	%r8b, 12(%rdi)
	leaq	13(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L248
	movzbl	13(%rcx), %r8d
	movb	%r8b, 13(%rdi)
	leaq	14(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L248
	movzbl	14(%rcx), %ecx
	movb	%cl, 14(%rdi)
	.p2align 4,,10
	.p2align 3
.L248:
	subq	%r13, %rsi
	addq	%rsi, %r12
.L243:
	movq	%rax, %xmm0
	movq	%r12, %xmm4
	movq	%rdx, 48(%rbx)
	subq	%r12, %rdx
	punpcklqdq	%xmm4, %xmm0
	movl	$-559038242, -56(%rbp)
	movups	%xmm0, 32(%rbx)
	cmpq	$3, %rdx
	ja	.L358
.L249:
	movq	32(%rbx), %rcx
	movq	%r12, %rsi
	movl	$2147483647, %r8d
	movq	%r8, %rax
	subq	%rcx, %rsi
	subq	%rsi, %rax
	cmpq	$3, %rax
	jbe	.L210
	movl	$4, %edx
	cmpq	$4, %rsi
	movq	%rdx, %rax
	cmovnb	%rsi, %rax
	addq	%rsi, %rax
	jc	.L252
	testq	%rax, %rax
	jne	.L359
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L254:
	cmpq	%rcx, %r12
	je	.L283
	leaq	15(%rax), %rdx
	movq	%r12, %rdi
	subq	%rcx, %rdx
	subq	%rcx, %rdi
	cmpq	$30, %rdx
	jbe	.L258
	leaq	-1(%r12), %rdx
	subq	%rcx, %rdx
	cmpq	$14, %rdx
	jbe	.L258
	movq	%rdi, %rsi
	xorl	%edx, %edx
	andq	$-16, %rsi
	.p2align 4,,10
	.p2align 3
.L259:
	movdqu	(%rcx,%rdx), %xmm7
	movups	%xmm7, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L259
	movq	%rdi, %r9
	andq	$-16, %r9
	leaq	(%rcx,%r9), %rdx
	leaq	(%rax,%r9), %rsi
	cmpq	%rdi, %r9
	je	.L262
	movzbl	(%rdx), %edi
	movb	%dil, (%rsi)
	leaq	1(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L262
	movzbl	1(%rdx), %edi
	movb	%dil, 1(%rsi)
	leaq	2(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L262
	movzbl	2(%rdx), %edi
	movb	%dil, 2(%rsi)
	leaq	3(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L262
	movzbl	3(%rdx), %edi
	movb	%dil, 3(%rsi)
	leaq	4(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L262
	movzbl	4(%rdx), %edi
	movb	%dil, 4(%rsi)
	leaq	5(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L262
	movzbl	5(%rdx), %edi
	movb	%dil, 5(%rsi)
	leaq	6(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L262
	movzbl	6(%rdx), %edi
	movb	%dil, 6(%rsi)
	leaq	7(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L262
	movzbl	7(%rdx), %edi
	movb	%dil, 7(%rsi)
	leaq	8(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L262
	movzbl	8(%rdx), %edi
	movb	%dil, 8(%rsi)
	leaq	9(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L262
	movzbl	9(%rdx), %edi
	movb	%dil, 9(%rsi)
	leaq	10(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L262
	movzbl	10(%rdx), %edi
	movb	%dil, 10(%rsi)
	leaq	11(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L262
	movzbl	11(%rdx), %edi
	movb	%dil, 11(%rsi)
	leaq	12(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L262
	movzbl	12(%rdx), %edi
	movb	%dil, 12(%rsi)
	leaq	13(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L262
	movzbl	13(%rdx), %edi
	movb	%dil, 13(%rsi)
	leaq	14(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L262
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rsi)
	.p2align 4,,10
	.p2align 3
.L262:
	movq	%r12, %rdi
	subq	%rcx, %rdi
	addq	%rax, %rdi
.L257:
	movl	-56(%rbp), %edx
	leaq	4(%rdi), %rsi
	movl	%edx, (%rdi)
	movq	40(%rbx), %rcx
	cmpq	%r12, %rcx
	je	.L263
	leaq	16(%r12), %rdx
	movq	%rcx, %r10
	cmpq	%rdx, %rsi
	leaq	20(%rdi), %rdx
	setnb	%r9b
	cmpq	%rdx, %r12
	setnb	%dl
	subq	%r12, %r10
	orb	%dl, %r9b
	je	.L264
	leaq	-1(%rcx), %rdx
	subq	%r12, %rdx
	cmpq	$14, %rdx
	jbe	.L264
	movq	%r10, %r9
	xorl	%edx, %edx
	andq	$-16, %r9
	.p2align 4,,10
	.p2align 3
.L265:
	movdqu	(%r12,%rdx), %xmm1
	movups	%xmm1, 4(%rdi,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %r9
	jne	.L265
	movq	%r10, %r9
	andq	$-16, %r9
	leaq	(%r12,%r9), %rdx
	leaq	(%rsi,%r9), %rdi
	cmpq	%r9, %r10
	je	.L268
	movzbl	(%rdx), %r9d
	movb	%r9b, (%rdi)
	leaq	1(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L268
	movzbl	1(%rdx), %r9d
	movb	%r9b, 1(%rdi)
	leaq	2(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L268
	movzbl	2(%rdx), %r9d
	movb	%r9b, 2(%rdi)
	leaq	3(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L268
	movzbl	3(%rdx), %r9d
	movb	%r9b, 3(%rdi)
	leaq	4(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L268
	movzbl	4(%rdx), %r9d
	movb	%r9b, 4(%rdi)
	leaq	5(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L268
	movzbl	5(%rdx), %r9d
	movb	%r9b, 5(%rdi)
	leaq	6(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L268
	movzbl	6(%rdx), %r9d
	movb	%r9b, 6(%rdi)
	leaq	7(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L268
	movzbl	7(%rdx), %r9d
	movb	%r9b, 7(%rdi)
	leaq	8(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L268
	movzbl	8(%rdx), %r9d
	movb	%r9b, 8(%rdi)
	leaq	9(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L268
	movzbl	9(%rdx), %r9d
	movb	%r9b, 9(%rdi)
	leaq	10(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L268
	movzbl	10(%rdx), %r9d
	movb	%r9b, 10(%rdi)
	leaq	11(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L268
	movzbl	11(%rdx), %r9d
	movb	%r9b, 11(%rdi)
	leaq	12(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L268
	movzbl	12(%rdx), %r9d
	movb	%r9b, 12(%rdi)
	leaq	13(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L268
	movzbl	13(%rdx), %r9d
	movb	%r9b, 13(%rdi)
	leaq	14(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L268
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rdi)
	.p2align 4,,10
	.p2align 3
.L268:
	subq	%r12, %rcx
	addq	%rcx, %rsi
.L263:
	movq	%rax, %xmm0
	movq	%rsi, %xmm5
	movq	%r8, 48(%rbx)
	punpcklqdq	%xmm5, %xmm0
	movb	$0, -44(%rbp)
	movups	%xmm0, 32(%rbx)
	cmpq	%r8, %rsi
	jne	.L360
.L269:
	leaq	-44(%rbp), %rdx
	leaq	24(%rbx), %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L238:
	movq	%r13, %r8
	xorl	%ecx, %ecx
	subq	%rsi, %r8
	.p2align 4,,10
	.p2align 3
.L241:
	movzbl	(%rsi,%rcx), %edi
	movb	%dil, (%rax,%rcx)
	addq	$1, %rcx
	cmpq	%r8, %rcx
	jne	.L241
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L218:
	movq	%r12, %r8
	xorl	%edx, %edx
	subq	%rsi, %r8
	.p2align 4,,10
	.p2align 3
.L221:
	movzbl	(%rsi,%rdx), %edi
	movb	%dil, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%r8, %rdx
	jne	.L221
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L224:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L227:
	movzbl	(%r12,%rdx), %r8d
	movb	%r8b, 4(%rdi,%rdx)
	addq	$1, %rdx
	cmpq	%r9, %rdx
	jne	.L227
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L203:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L206:
	movzbl	0(%r13,%rcx), %r8d
	movb	%r8b, 4(%rdi,%rcx)
	addq	$1, %rcx
	cmpq	%r9, %rcx
	jne	.L206
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L197:
	movq	%r13, %r8
	xorl	%ecx, %ecx
	subq	%rsi, %r8
	.p2align 4,,10
	.p2align 3
.L200:
	movzbl	(%rsi,%rcx), %edi
	movb	%dil, (%rax,%rcx)
	addq	$1, %rcx
	cmpq	%r8, %rcx
	jne	.L200
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L258:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L261:
	movzbl	(%rcx,%rdx), %esi
	movb	%sil, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%rdi, %rdx
	jne	.L261
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L264:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L267:
	movzbl	(%r12,%rdx), %r9d
	movb	%r9b, 4(%rdi,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %r10
	jne	.L267
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L244:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L247:
	movzbl	0(%r13,%rcx), %r8d
	movb	%r8b, 4(%rdi,%rcx)
	addq	$1, %rcx
	cmpq	%r9, %rcx
	jne	.L247
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L277:
	movq	%rax, %rdi
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L280:
	movq	%rax, %rdi
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L283:
	movq	%rax, %rdi
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L274:
	movq	%rax, %rdi
	jmp	.L196
.L354:
	cmpq	$2147483647, %rax
	cmovbe	%rax, %r12
	leaq	7(%r12), %rsi
	andq	$-8, %rsi
.L192:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L361
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L195:
	movq	32(%rbx), %rsi
	leaq	(%rax,%r12), %rdx
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L361:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L195
.L355:
	cmpq	$2147483647, %rax
	cmovbe	%rax, %r13
	leaq	7(%r13), %rsi
	andq	$-8, %rsi
.L213:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L362
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L216:
	movq	32(%rbx), %rsi
	leaq	(%rax,%r13), %rcx
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L362:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L216
.L357:
	cmpq	$2147483647, %rax
	cmovbe	%rax, %r12
	leaq	7(%r12), %rsi
	andq	$-8, %rsi
.L233:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L363
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L236:
	movq	32(%rbx), %rsi
	leaq	(%rax,%r12), %rdx
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L363:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L236
.L359:
	cmpq	$2147483647, %rax
	cmovbe	%rax, %r8
	leaq	7(%r8), %rsi
	movq	%r8, %r13
	andq	$-8, %rsi
.L253:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L364
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L256:
	movq	32(%rbx), %rcx
	leaq	(%rax,%r13), %r8
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L364:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L256
.L210:
	leaq	.LC21(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L353:
	call	__stack_chk_fail@PLT
.L191:
	movl	$2147483648, %esi
	movl	$2147483647, %r12d
	jmp	.L192
.L212:
	movl	$2147483648, %esi
	movl	$2147483647, %r13d
	jmp	.L213
.L232:
	movl	$2147483648, %esi
	movl	$2147483647, %r12d
	jmp	.L233
.L252:
	movl	$2147483648, %esi
	movl	$2147483647, %r13d
	jmp	.L253
	.cfi_endproc
.LFE5873:
	.size	_ZN2v88internal13EhFrameWriter14WriteFdeHeaderEv, .-_ZN2v88internal13EhFrameWriter14WriteFdeHeaderEv
	.section	.text._ZN2v88internal13EhFrameWriter15WriteEhFrameHdrEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13EhFrameWriter15WriteEhFrameHdrEi
	.type	_ZN2v88internal13EhFrameWriter15WriteEhFrameHdrEi, @function
_ZN2v88internal13EhFrameWriter15WriteEhFrameHdrEi:
.LFB5874:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	24(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	40(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$1, -60(%rbp)
	movq	%rsi, %r15
	subq	32(%rdi), %r15
	cmpq	48(%rdi), %rsi
	je	.L366
	movb	$1, (%rsi)
	movq	40(%rdi), %rax
	movb	$27, -60(%rbp)
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rdi)
	cmpq	48(%rbx), %rsi
	je	.L368
.L544:
	movb	$27, (%rsi)
	movq	40(%rbx), %rax
	movb	$3, -60(%rbp)
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rbx)
	cmpq	%rsi, 48(%rbx)
	je	.L370
.L545:
	movb	$3, (%rsi)
	movq	40(%rbx), %rax
	movb	$59, -60(%rbp)
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rbx)
	cmpq	48(%rbx), %rsi
	je	.L372
.L546:
	movb	$59, (%rsi)
	movq	40(%rbx), %rax
	leaq	1(%rax), %r12
	movq	%r12, 40(%rbx)
.L373:
	movq	48(%rbx), %rdx
	movl	$-4, %eax
	subl	%r15d, %eax
	subq	%r12, %rdx
	movl	%eax, -60(%rbp)
	cmpq	$3, %rdx
	jbe	.L374
	movl	%eax, (%r12)
	movq	40(%rbx), %rax
	movq	48(%rbx), %rdx
	movl	$1, -64(%rbp)
	leaq	4(%rax), %r13
	subq	%r13, %rdx
	movq	%r13, 40(%rbx)
	cmpq	$3, %rdx
	jbe	.L395
.L540:
	movl	$1, 0(%r13)
	movq	40(%rbx), %rax
	movq	48(%rbx), %rcx
	leaq	4(%rax), %r12
	movq	%r12, 40(%rbx)
.L396:
	leal	7(%r14), %eax
	subq	%r12, %rcx
	andl	$-8, %eax
	addl	%r15d, %eax
	negl	%eax
	movl	%eax, -68(%rbp)
	cmpq	$3, %rcx
	jbe	.L416
	movl	%eax, (%r12)
	movq	40(%rbx), %rax
	movq	48(%rbx), %rcx
	leaq	4(%rax), %r13
	movq	%r13, 40(%rbx)
.L417:
	movl	(%rbx), %eax
	subq	%r13, %rcx
	subl	%r15d, %eax
	movl	%eax, -72(%rbp)
	cmpq	$3, %rcx
	jbe	.L436
	movl	%eax, 0(%r13)
	addq	$4, 40(%rbx)
.L365:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L538
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L374:
	.cfi_restore_state
	movq	32(%rbx), %rsi
	movq	%r12, %rax
	movl	$2147483647, %r13d
	movq	%r13, %rdx
	subq	%rsi, %rax
	subq	%rax, %rdx
	cmpq	$3, %rdx
	jbe	.L397
	cmpq	$4, %rax
	movl	$4, %edx
	cmovnb	%rax, %rdx
	addq	%rdx, %rax
	jc	.L378
	testq	%rax, %rax
	jne	.L539
	xorl	%edx, %edx
	xorl	%eax, %eax
.L380:
	cmpq	%rsi, %r12
	je	.L459
	leaq	-1(%r12), %rcx
	subq	%rsi, %rcx
	cmpq	$14, %rcx
	jbe	.L384
	leaq	15(%rsi), %rcx
	subq	%rax, %rcx
	cmpq	$30, %rcx
	jbe	.L384
	movq	%r12, %r9
	xorl	%ecx, %ecx
	subq	%rsi, %r9
	movq	%r9, %rdi
	andq	$-16, %rdi
	.p2align 4,,10
	.p2align 3
.L385:
	movdqu	(%rsi,%rcx), %xmm1
	movups	%xmm1, (%rax,%rcx)
	addq	$16, %rcx
	cmpq	%rdi, %rcx
	jne	.L385
	movq	%r9, %r8
	andq	$-16, %r8
	leaq	(%rsi,%r8), %rcx
	leaq	(%rax,%r8), %rdi
	cmpq	%r8, %r9
	je	.L388
	movzbl	(%rcx), %r8d
	movb	%r8b, (%rdi)
	leaq	1(%rcx), %r8
	cmpq	%r8, %r12
	je	.L388
	movzbl	1(%rcx), %r8d
	movb	%r8b, 1(%rdi)
	leaq	2(%rcx), %r8
	cmpq	%r8, %r12
	je	.L388
	movzbl	2(%rcx), %r8d
	movb	%r8b, 2(%rdi)
	leaq	3(%rcx), %r8
	cmpq	%r8, %r12
	je	.L388
	movzbl	3(%rcx), %r8d
	movb	%r8b, 3(%rdi)
	leaq	4(%rcx), %r8
	cmpq	%r8, %r12
	je	.L388
	movzbl	4(%rcx), %r8d
	movb	%r8b, 4(%rdi)
	leaq	5(%rcx), %r8
	cmpq	%r8, %r12
	je	.L388
	movzbl	5(%rcx), %r8d
	movb	%r8b, 5(%rdi)
	leaq	6(%rcx), %r8
	cmpq	%r8, %r12
	je	.L388
	movzbl	6(%rcx), %r8d
	movb	%r8b, 6(%rdi)
	leaq	7(%rcx), %r8
	cmpq	%r8, %r12
	je	.L388
	movzbl	7(%rcx), %r8d
	movb	%r8b, 7(%rdi)
	leaq	8(%rcx), %r8
	cmpq	%r8, %r12
	je	.L388
	movzbl	8(%rcx), %r8d
	movb	%r8b, 8(%rdi)
	leaq	9(%rcx), %r8
	cmpq	%r8, %r12
	je	.L388
	movzbl	9(%rcx), %r8d
	movb	%r8b, 9(%rdi)
	leaq	10(%rcx), %r8
	cmpq	%r8, %r12
	je	.L388
	movzbl	10(%rcx), %r8d
	movb	%r8b, 10(%rdi)
	leaq	11(%rcx), %r8
	cmpq	%r8, %r12
	je	.L388
	movzbl	11(%rcx), %r8d
	movb	%r8b, 11(%rdi)
	leaq	12(%rcx), %r8
	cmpq	%r8, %r12
	je	.L388
	movzbl	12(%rcx), %r8d
	movb	%r8b, 12(%rdi)
	leaq	13(%rcx), %r8
	cmpq	%r8, %r12
	je	.L388
	movzbl	13(%rcx), %r8d
	movb	%r8b, 13(%rdi)
	leaq	14(%rcx), %r8
	cmpq	%r8, %r12
	je	.L388
	movzbl	14(%rcx), %ecx
	movb	%cl, 14(%rdi)
	.p2align 4,,10
	.p2align 3
.L388:
	movq	%r12, %rdi
	subq	%rsi, %rdi
	addq	%rax, %rdi
.L383:
	movl	-60(%rbp), %ecx
	leaq	4(%rdi), %r13
	movl	%ecx, (%rdi)
	movq	40(%rbx), %rsi
	cmpq	%rsi, %r12
	je	.L389
	leaq	16(%r12), %rcx
	movq	%rsi, %r9
	cmpq	%rcx, %r13
	leaq	20(%rdi), %rcx
	setnb	%r8b
	cmpq	%rcx, %r12
	setnb	%cl
	subq	%r12, %r9
	orb	%cl, %r8b
	je	.L390
	leaq	-1(%rsi), %rcx
	subq	%r12, %rcx
	cmpq	$14, %rcx
	jbe	.L390
	movq	%r9, %r8
	xorl	%ecx, %ecx
	andq	$-16, %r8
	.p2align 4,,10
	.p2align 3
.L391:
	movdqu	(%r12,%rcx), %xmm2
	movups	%xmm2, 4(%rdi,%rcx)
	addq	$16, %rcx
	cmpq	%r8, %rcx
	jne	.L391
	movq	%r9, %r8
	andq	$-16, %r8
	leaq	(%r12,%r8), %rcx
	leaq	0(%r13,%r8), %rdi
	cmpq	%r8, %r9
	je	.L394
	movzbl	(%rcx), %r8d
	movb	%r8b, (%rdi)
	leaq	1(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L394
	movzbl	1(%rcx), %r8d
	movb	%r8b, 1(%rdi)
	leaq	2(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L394
	movzbl	2(%rcx), %r8d
	movb	%r8b, 2(%rdi)
	leaq	3(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L394
	movzbl	3(%rcx), %r8d
	movb	%r8b, 3(%rdi)
	leaq	4(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L394
	movzbl	4(%rcx), %r8d
	movb	%r8b, 4(%rdi)
	leaq	5(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L394
	movzbl	5(%rcx), %r8d
	movb	%r8b, 5(%rdi)
	leaq	6(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L394
	movzbl	6(%rcx), %r8d
	movb	%r8b, 6(%rdi)
	leaq	7(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L394
	movzbl	7(%rcx), %r8d
	movb	%r8b, 7(%rdi)
	leaq	8(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L394
	movzbl	8(%rcx), %r8d
	movb	%r8b, 8(%rdi)
	leaq	9(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L394
	movzbl	9(%rcx), %r8d
	movb	%r8b, 9(%rdi)
	leaq	10(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L394
	movzbl	10(%rcx), %r8d
	movb	%r8b, 10(%rdi)
	leaq	11(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L394
	movzbl	11(%rcx), %r8d
	movb	%r8b, 11(%rdi)
	leaq	12(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L394
	movzbl	12(%rcx), %r8d
	movb	%r8b, 12(%rdi)
	leaq	13(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L394
	movzbl	13(%rcx), %r8d
	movb	%r8b, 13(%rdi)
	leaq	14(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L394
	movzbl	14(%rcx), %ecx
	movb	%cl, 14(%rdi)
	.p2align 4,,10
	.p2align 3
.L394:
	subq	%r12, %rsi
	addq	%rsi, %r13
.L389:
	movq	%rax, %xmm0
	movq	%r13, %xmm2
	movq	%rdx, 48(%rbx)
	subq	%r13, %rdx
	punpcklqdq	%xmm2, %xmm0
	movl	$1, -64(%rbp)
	movups	%xmm0, 32(%rbx)
	cmpq	$3, %rdx
	ja	.L540
.L395:
	movq	32(%rbx), %rsi
	movq	%r13, %rcx
	movl	$2147483647, %r12d
	movq	%r12, %rax
	subq	%rsi, %rcx
	subq	%rcx, %rax
	cmpq	$3, %rax
	jbe	.L397
	movl	$4, %edx
	cmpq	$4, %rcx
	movq	%rdx, %rax
	cmovnb	%rcx, %rax
	addq	%rcx, %rax
	jc	.L399
	testq	%rax, %rax
	jne	.L541
	xorl	%ecx, %ecx
	xorl	%eax, %eax
.L401:
	cmpq	%rsi, %r13
	je	.L462
	leaq	15(%rsi), %rdx
	subq	%rax, %rdx
	cmpq	$30, %rdx
	jbe	.L405
	leaq	-1(%r13), %rdx
	subq	%rsi, %rdx
	cmpq	$14, %rdx
	jbe	.L405
	movq	%r13, %r9
	xorl	%edx, %edx
	subq	%rsi, %r9
	movq	%r9, %rdi
	andq	$-16, %rdi
	.p2align 4,,10
	.p2align 3
.L406:
	movdqu	(%rsi,%rdx), %xmm3
	movups	%xmm3, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L406
	movq	%r9, %r8
	andq	$-16, %r8
	leaq	(%rsi,%r8), %rdx
	leaq	(%rax,%r8), %rdi
	cmpq	%r8, %r9
	je	.L409
	movzbl	(%rdx), %r8d
	movb	%r8b, (%rdi)
	leaq	1(%rdx), %r8
	cmpq	%r8, %r13
	je	.L409
	movzbl	1(%rdx), %r8d
	movb	%r8b, 1(%rdi)
	leaq	2(%rdx), %r8
	cmpq	%r8, %r13
	je	.L409
	movzbl	2(%rdx), %r8d
	movb	%r8b, 2(%rdi)
	leaq	3(%rdx), %r8
	cmpq	%r8, %r13
	je	.L409
	movzbl	3(%rdx), %r8d
	movb	%r8b, 3(%rdi)
	leaq	4(%rdx), %r8
	cmpq	%r8, %r13
	je	.L409
	movzbl	4(%rdx), %r8d
	movb	%r8b, 4(%rdi)
	leaq	5(%rdx), %r8
	cmpq	%r8, %r13
	je	.L409
	movzbl	5(%rdx), %r8d
	movb	%r8b, 5(%rdi)
	leaq	6(%rdx), %r8
	cmpq	%r8, %r13
	je	.L409
	movzbl	6(%rdx), %r8d
	movb	%r8b, 6(%rdi)
	leaq	7(%rdx), %r8
	cmpq	%r8, %r13
	je	.L409
	movzbl	7(%rdx), %r8d
	movb	%r8b, 7(%rdi)
	leaq	8(%rdx), %r8
	cmpq	%r8, %r13
	je	.L409
	movzbl	8(%rdx), %r8d
	movb	%r8b, 8(%rdi)
	leaq	9(%rdx), %r8
	cmpq	%r8, %r13
	je	.L409
	movzbl	9(%rdx), %r8d
	movb	%r8b, 9(%rdi)
	leaq	10(%rdx), %r8
	cmpq	%r8, %r13
	je	.L409
	movzbl	10(%rdx), %r8d
	movb	%r8b, 10(%rdi)
	leaq	11(%rdx), %r8
	cmpq	%r8, %r13
	je	.L409
	movzbl	11(%rdx), %r8d
	movb	%r8b, 11(%rdi)
	leaq	12(%rdx), %r8
	cmpq	%r8, %r13
	je	.L409
	movzbl	12(%rdx), %r8d
	movb	%r8b, 12(%rdi)
	leaq	13(%rdx), %r8
	cmpq	%r8, %r13
	je	.L409
	movzbl	13(%rdx), %r8d
	movb	%r8b, 13(%rdi)
	leaq	14(%rdx), %r8
	cmpq	%r8, %r13
	je	.L409
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rdi)
	.p2align 4,,10
	.p2align 3
.L409:
	movq	%r13, %rdi
	subq	%rsi, %rdi
	addq	%rax, %rdi
.L404:
	movl	-64(%rbp), %edx
	leaq	4(%rdi), %r12
	movl	%edx, (%rdi)
	movq	40(%rbx), %rsi
	cmpq	%rsi, %r13
	je	.L410
	leaq	20(%rdi), %rdx
	movq	%rsi, %r9
	cmpq	%rdx, %r13
	leaq	16(%r13), %rdx
	setnb	%r8b
	cmpq	%rdx, %r12
	setnb	%dl
	subq	%r13, %r9
	orb	%dl, %r8b
	je	.L411
	leaq	-1(%rsi), %rdx
	subq	%r13, %rdx
	cmpq	$14, %rdx
	jbe	.L411
	movq	%r9, %r8
	xorl	%edx, %edx
	andq	$-16, %r8
	.p2align 4,,10
	.p2align 3
.L412:
	movdqu	0(%r13,%rdx), %xmm4
	movups	%xmm4, 4(%rdi,%rdx)
	addq	$16, %rdx
	cmpq	%r8, %rdx
	jne	.L412
	movq	%r9, %r8
	andq	$-16, %r8
	leaq	0(%r13,%r8), %rdx
	leaq	(%r12,%r8), %rdi
	cmpq	%r9, %r8
	je	.L415
	movzbl	(%rdx), %r8d
	movb	%r8b, (%rdi)
	leaq	1(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L415
	movzbl	1(%rdx), %r8d
	movb	%r8b, 1(%rdi)
	leaq	2(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L415
	movzbl	2(%rdx), %r8d
	movb	%r8b, 2(%rdi)
	leaq	3(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L415
	movzbl	3(%rdx), %r8d
	movb	%r8b, 3(%rdi)
	leaq	4(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L415
	movzbl	4(%rdx), %r8d
	movb	%r8b, 4(%rdi)
	leaq	5(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L415
	movzbl	5(%rdx), %r8d
	movb	%r8b, 5(%rdi)
	leaq	6(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L415
	movzbl	6(%rdx), %r8d
	movb	%r8b, 6(%rdi)
	leaq	7(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L415
	movzbl	7(%rdx), %r8d
	movb	%r8b, 7(%rdi)
	leaq	8(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L415
	movzbl	8(%rdx), %r8d
	movb	%r8b, 8(%rdi)
	leaq	9(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L415
	movzbl	9(%rdx), %r8d
	movb	%r8b, 9(%rdi)
	leaq	10(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L415
	movzbl	10(%rdx), %r8d
	movb	%r8b, 10(%rdi)
	leaq	11(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L415
	movzbl	11(%rdx), %r8d
	movb	%r8b, 11(%rdi)
	leaq	12(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L415
	movzbl	12(%rdx), %r8d
	movb	%r8b, 12(%rdi)
	leaq	13(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L415
	movzbl	13(%rdx), %r8d
	movb	%r8b, 13(%rdi)
	leaq	14(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L415
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rdi)
	.p2align 4,,10
	.p2align 3
.L415:
	subq	%r13, %rsi
	addq	%rsi, %r12
.L410:
	movq	%rax, %xmm0
	movq	%r12, %xmm3
	movq	%rcx, 48(%rbx)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 32(%rbx)
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L416:
	movq	32(%rbx), %rsi
	movq	%r12, %rax
	movl	$2147483647, %ecx
	movq	%rcx, %rdx
	subq	%rsi, %rax
	subq	%rax, %rdx
	cmpq	$3, %rdx
	jbe	.L397
	cmpq	$4, %rax
	movl	$4, %edx
	cmovnb	%rax, %rdx
	addq	%rdx, %rax
	jc	.L419
	testq	%rax, %rax
	jne	.L542
	xorl	%ecx, %ecx
	xorl	%eax, %eax
.L421:
	cmpq	%rsi, %r12
	je	.L465
	leaq	-1(%r12), %rdx
	movq	%r12, %r8
	subq	%rsi, %rdx
	subq	%rsi, %r8
	cmpq	$14, %rdx
	jbe	.L425
	leaq	15(%rax), %rdx
	subq	%rsi, %rdx
	cmpq	$30, %rdx
	jbe	.L425
	movq	%r8, %rdi
	xorl	%edx, %edx
	andq	$-16, %rdi
	.p2align 4,,10
	.p2align 3
.L426:
	movdqu	(%rsi,%rdx), %xmm5
	movups	%xmm5, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L426
	movq	%r8, %r9
	andq	$-16, %r9
	leaq	(%rsi,%r9), %rdx
	leaq	(%rax,%r9), %rdi
	cmpq	%r8, %r9
	je	.L429
	movzbl	(%rdx), %r8d
	movb	%r8b, (%rdi)
	leaq	1(%rdx), %r8
	cmpq	%r8, %r12
	je	.L429
	movzbl	1(%rdx), %r8d
	movb	%r8b, 1(%rdi)
	leaq	2(%rdx), %r8
	cmpq	%r8, %r12
	je	.L429
	movzbl	2(%rdx), %r8d
	movb	%r8b, 2(%rdi)
	leaq	3(%rdx), %r8
	cmpq	%r8, %r12
	je	.L429
	movzbl	3(%rdx), %r8d
	movb	%r8b, 3(%rdi)
	leaq	4(%rdx), %r8
	cmpq	%r8, %r12
	je	.L429
	movzbl	4(%rdx), %r8d
	movb	%r8b, 4(%rdi)
	leaq	5(%rdx), %r8
	cmpq	%r8, %r12
	je	.L429
	movzbl	5(%rdx), %r8d
	movb	%r8b, 5(%rdi)
	leaq	6(%rdx), %r8
	cmpq	%r8, %r12
	je	.L429
	movzbl	6(%rdx), %r8d
	movb	%r8b, 6(%rdi)
	leaq	7(%rdx), %r8
	cmpq	%r8, %r12
	je	.L429
	movzbl	7(%rdx), %r8d
	movb	%r8b, 7(%rdi)
	leaq	8(%rdx), %r8
	cmpq	%r8, %r12
	je	.L429
	movzbl	8(%rdx), %r8d
	movb	%r8b, 8(%rdi)
	leaq	9(%rdx), %r8
	cmpq	%r8, %r12
	je	.L429
	movzbl	9(%rdx), %r8d
	movb	%r8b, 9(%rdi)
	leaq	10(%rdx), %r8
	cmpq	%r8, %r12
	je	.L429
	movzbl	10(%rdx), %r8d
	movb	%r8b, 10(%rdi)
	leaq	11(%rdx), %r8
	cmpq	%r8, %r12
	je	.L429
	movzbl	11(%rdx), %r8d
	movb	%r8b, 11(%rdi)
	leaq	12(%rdx), %r8
	cmpq	%r8, %r12
	je	.L429
	movzbl	12(%rdx), %r8d
	movb	%r8b, 12(%rdi)
	leaq	13(%rdx), %r8
	cmpq	%r8, %r12
	je	.L429
	movzbl	13(%rdx), %r8d
	movb	%r8b, 13(%rdi)
	leaq	14(%rdx), %r8
	cmpq	%r8, %r12
	je	.L429
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rdi)
	.p2align 4,,10
	.p2align 3
.L429:
	movq	%r12, %rdi
	subq	%rsi, %rdi
	addq	%rax, %rdi
.L424:
	movl	-68(%rbp), %edx
	leaq	4(%rdi), %r13
	movl	%edx, (%rdi)
	movq	40(%rbx), %rsi
	cmpq	%r12, %rsi
	je	.L430
	leaq	16(%r12), %rdx
	movq	%rsi, %r9
	cmpq	%rdx, %r13
	leaq	20(%rdi), %rdx
	setnb	%r8b
	cmpq	%rdx, %r12
	setnb	%dl
	subq	%r12, %r9
	orb	%dl, %r8b
	je	.L431
	leaq	-1(%rsi), %rdx
	subq	%r12, %rdx
	cmpq	$14, %rdx
	jbe	.L431
	movq	%r9, %r8
	xorl	%edx, %edx
	andq	$-16, %r8
	.p2align 4,,10
	.p2align 3
.L432:
	movdqu	(%r12,%rdx), %xmm6
	movups	%xmm6, 4(%rdi,%rdx)
	addq	$16, %rdx
	cmpq	%r8, %rdx
	jne	.L432
	movq	%r9, %r8
	andq	$-16, %r8
	leaq	(%r12,%r8), %rdx
	leaq	0(%r13,%r8), %rdi
	cmpq	%r9, %r8
	je	.L435
	movzbl	(%rdx), %r8d
	movb	%r8b, (%rdi)
	leaq	1(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L435
	movzbl	1(%rdx), %r8d
	movb	%r8b, 1(%rdi)
	leaq	2(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L435
	movzbl	2(%rdx), %r8d
	movb	%r8b, 2(%rdi)
	leaq	3(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L435
	movzbl	3(%rdx), %r8d
	movb	%r8b, 3(%rdi)
	leaq	4(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L435
	movzbl	4(%rdx), %r8d
	movb	%r8b, 4(%rdi)
	leaq	5(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L435
	movzbl	5(%rdx), %r8d
	movb	%r8b, 5(%rdi)
	leaq	6(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L435
	movzbl	6(%rdx), %r8d
	movb	%r8b, 6(%rdi)
	leaq	7(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L435
	movzbl	7(%rdx), %r8d
	movb	%r8b, 7(%rdi)
	leaq	8(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L435
	movzbl	8(%rdx), %r8d
	movb	%r8b, 8(%rdi)
	leaq	9(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L435
	movzbl	9(%rdx), %r8d
	movb	%r8b, 9(%rdi)
	leaq	10(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L435
	movzbl	10(%rdx), %r8d
	movb	%r8b, 10(%rdi)
	leaq	11(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L435
	movzbl	11(%rdx), %r8d
	movb	%r8b, 11(%rdi)
	leaq	12(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L435
	movzbl	12(%rdx), %r8d
	movb	%r8b, 12(%rdi)
	leaq	13(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L435
	movzbl	13(%rdx), %r8d
	movb	%r8b, 13(%rdi)
	leaq	14(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L435
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rdi)
	.p2align 4,,10
	.p2align 3
.L435:
	subq	%r12, %rsi
	addq	%rsi, %r13
.L430:
	movq	%rax, %xmm0
	movq	%r13, %xmm4
	movq	%rcx, 48(%rbx)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 32(%rbx)
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L436:
	movq	32(%rbx), %rcx
	movq	%r13, %rsi
	movl	$2147483647, %edi
	movq	%rdi, %rax
	subq	%rcx, %rsi
	subq	%rsi, %rax
	cmpq	$3, %rax
	jbe	.L397
	movl	$4, %edx
	cmpq	$4, %rsi
	movq	%rdx, %rax
	cmovnb	%rsi, %rax
	addq	%rsi, %rax
	jc	.L439
	testq	%rax, %rax
	jne	.L543
	xorl	%edi, %edi
	xorl	%eax, %eax
.L441:
	cmpq	%r13, %rcx
	je	.L468
	leaq	15(%rcx), %rdx
	subq	%rax, %rdx
	cmpq	$30, %rdx
	jbe	.L445
	leaq	-1(%r13), %rdx
	subq	%rcx, %rdx
	cmpq	$14, %rdx
	jbe	.L445
	movq	%r13, %r9
	xorl	%edx, %edx
	subq	%rcx, %r9
	movq	%r9, %rsi
	andq	$-16, %rsi
	.p2align 4,,10
	.p2align 3
.L446:
	movdqu	(%rcx,%rdx), %xmm7
	movups	%xmm7, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L446
	movq	%r9, %r8
	andq	$-16, %r8
	leaq	(%rcx,%r8), %rdx
	leaq	(%rax,%r8), %rsi
	cmpq	%r8, %r9
	je	.L449
	movzbl	(%rdx), %r8d
	movb	%r8b, (%rsi)
	leaq	1(%rdx), %r8
	cmpq	%r8, %r13
	je	.L449
	movzbl	1(%rdx), %r8d
	movb	%r8b, 1(%rsi)
	leaq	2(%rdx), %r8
	cmpq	%r8, %r13
	je	.L449
	movzbl	2(%rdx), %r8d
	movb	%r8b, 2(%rsi)
	leaq	3(%rdx), %r8
	cmpq	%r8, %r13
	je	.L449
	movzbl	3(%rdx), %r8d
	movb	%r8b, 3(%rsi)
	leaq	4(%rdx), %r8
	cmpq	%r8, %r13
	je	.L449
	movzbl	4(%rdx), %r8d
	movb	%r8b, 4(%rsi)
	leaq	5(%rdx), %r8
	cmpq	%r8, %r13
	je	.L449
	movzbl	5(%rdx), %r8d
	movb	%r8b, 5(%rsi)
	leaq	6(%rdx), %r8
	cmpq	%r8, %r13
	je	.L449
	movzbl	6(%rdx), %r8d
	movb	%r8b, 6(%rsi)
	leaq	7(%rdx), %r8
	cmpq	%r8, %r13
	je	.L449
	movzbl	7(%rdx), %r8d
	movb	%r8b, 7(%rsi)
	leaq	8(%rdx), %r8
	cmpq	%r8, %r13
	je	.L449
	movzbl	8(%rdx), %r8d
	movb	%r8b, 8(%rsi)
	leaq	9(%rdx), %r8
	cmpq	%r8, %r13
	je	.L449
	movzbl	9(%rdx), %r8d
	movb	%r8b, 9(%rsi)
	leaq	10(%rdx), %r8
	cmpq	%r8, %r13
	je	.L449
	movzbl	10(%rdx), %r8d
	movb	%r8b, 10(%rsi)
	leaq	11(%rdx), %r8
	cmpq	%r8, %r13
	je	.L449
	movzbl	11(%rdx), %r8d
	movb	%r8b, 11(%rsi)
	leaq	12(%rdx), %r8
	cmpq	%r8, %r13
	je	.L449
	movzbl	12(%rdx), %r8d
	movb	%r8b, 12(%rsi)
	leaq	13(%rdx), %r8
	cmpq	%r8, %r13
	je	.L449
	movzbl	13(%rdx), %r8d
	movb	%r8b, 13(%rsi)
	leaq	14(%rdx), %r8
	cmpq	%r8, %r13
	je	.L449
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rsi)
	.p2align 4,,10
	.p2align 3
.L449:
	movq	%r13, %rsi
	subq	%rcx, %rsi
	addq	%rax, %rsi
.L444:
	movl	-72(%rbp), %edx
	leaq	4(%rsi), %r8
	movl	%edx, (%rsi)
	movq	40(%rbx), %rcx
	cmpq	%r13, %rcx
	je	.L450
	leaq	16(%r13), %rdx
	movq	%rcx, %r10
	cmpq	%rdx, %r8
	leaq	20(%rsi), %rdx
	setnb	%r9b
	cmpq	%rdx, %r13
	setnb	%dl
	subq	%r13, %r10
	orb	%dl, %r9b
	je	.L451
	leaq	-1(%rcx), %rdx
	subq	%r13, %rdx
	cmpq	$14, %rdx
	jbe	.L451
	movq	%r10, %r9
	xorl	%edx, %edx
	andq	$-16, %r9
	.p2align 4,,10
	.p2align 3
.L452:
	movdqu	0(%r13,%rdx), %xmm1
	movups	%xmm1, 4(%rsi,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %r9
	jne	.L452
	movq	%r10, %r9
	andq	$-16, %r9
	leaq	0(%r13,%r9), %rdx
	leaq	(%r8,%r9), %rsi
	cmpq	%r9, %r10
	je	.L455
	movzbl	(%rdx), %r9d
	movb	%r9b, (%rsi)
	leaq	1(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L455
	movzbl	1(%rdx), %r9d
	movb	%r9b, 1(%rsi)
	leaq	2(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L455
	movzbl	2(%rdx), %r9d
	movb	%r9b, 2(%rsi)
	leaq	3(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L455
	movzbl	3(%rdx), %r9d
	movb	%r9b, 3(%rsi)
	leaq	4(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L455
	movzbl	4(%rdx), %r9d
	movb	%r9b, 4(%rsi)
	leaq	5(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L455
	movzbl	5(%rdx), %r9d
	movb	%r9b, 5(%rsi)
	leaq	6(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L455
	movzbl	6(%rdx), %r9d
	movb	%r9b, 6(%rsi)
	leaq	7(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L455
	movzbl	7(%rdx), %r9d
	movb	%r9b, 7(%rsi)
	leaq	8(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L455
	movzbl	8(%rdx), %r9d
	movb	%r9b, 8(%rsi)
	leaq	9(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L455
	movzbl	9(%rdx), %r9d
	movb	%r9b, 9(%rsi)
	leaq	10(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L455
	movzbl	10(%rdx), %r9d
	movb	%r9b, 10(%rsi)
	leaq	11(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L455
	movzbl	11(%rdx), %r9d
	movb	%r9b, 11(%rsi)
	leaq	12(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L455
	movzbl	12(%rdx), %r9d
	movb	%r9b, 12(%rsi)
	leaq	13(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L455
	movzbl	13(%rdx), %r9d
	movb	%r9b, 13(%rsi)
	leaq	14(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L455
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rsi)
	.p2align 4,,10
	.p2align 3
.L455:
	subq	%r13, %rcx
	addq	%rcx, %r8
.L450:
	movq	%rax, %xmm0
	movq	%r8, %xmm5
	movq	%rdi, 48(%rbx)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, 32(%rbx)
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L366:
	leaq	-60(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movb	$27, -60(%rbp)
	movq	40(%rbx), %rsi
	cmpq	48(%rbx), %rsi
	jne	.L544
	.p2align 4,,10
	.p2align 3
.L368:
	leaq	-60(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movb	$3, -60(%rbp)
	movq	40(%rbx), %rsi
	cmpq	%rsi, 48(%rbx)
	jne	.L545
	.p2align 4,,10
	.p2align 3
.L370:
	leaq	-60(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movb	$59, -60(%rbp)
	movq	40(%rbx), %rsi
	cmpq	48(%rbx), %rsi
	jne	.L546
	.p2align 4,,10
	.p2align 3
.L372:
	movq	%r12, %rdi
	leaq	-60(%rbp), %rdx
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movq	40(%rbx), %r12
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L431:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L434:
	movzbl	(%r12,%rdx), %r8d
	movb	%r8b, 4(%rdi,%rdx)
	addq	$1, %rdx
	cmpq	%r9, %rdx
	jne	.L434
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L425:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L428:
	movzbl	(%rsi,%rdx), %edi
	movb	%dil, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%r8, %rdx
	jne	.L428
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L411:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L414:
	movzbl	0(%r13,%rdx), %r8d
	movb	%r8b, 4(%rdi,%rdx)
	addq	$1, %rdx
	cmpq	%r9, %rdx
	jne	.L414
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L390:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L393:
	movzbl	(%r12,%rcx), %r8d
	movb	%r8b, 4(%rdi,%rcx)
	addq	$1, %rcx
	cmpq	%r9, %rcx
	jne	.L393
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L384:
	movq	%r12, %r8
	xorl	%ecx, %ecx
	subq	%rsi, %r8
	.p2align 4,,10
	.p2align 3
.L387:
	movzbl	(%rsi,%rcx), %edi
	movb	%dil, (%rax,%rcx)
	addq	$1, %rcx
	cmpq	%r8, %rcx
	jne	.L387
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L451:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L454:
	movzbl	0(%r13,%rdx), %r9d
	movb	%r9b, 4(%rsi,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %r10
	jne	.L454
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L445:
	movq	%r13, %r8
	xorl	%edx, %edx
	subq	%rcx, %r8
	.p2align 4,,10
	.p2align 3
.L448:
	movzbl	(%rcx,%rdx), %esi
	movb	%sil, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%r8, %rdx
	jne	.L448
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L405:
	movq	%r13, %r8
	xorl	%edx, %edx
	subq	%rsi, %r8
	.p2align 4,,10
	.p2align 3
.L408:
	movzbl	(%rsi,%rdx), %edi
	movb	%dil, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%r8, %rdx
	jne	.L408
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L465:
	movq	%rax, %rdi
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L459:
	movq	%rax, %rdi
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L462:
	movq	%rax, %rdi
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L468:
	movq	%rax, %rsi
	jmp	.L444
.L541:
	cmpq	$2147483647, %rax
	cmovbe	%rax, %r12
	leaq	7(%r12), %rsi
	andq	$-8, %rsi
.L400:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L547
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L403:
	movq	32(%rbx), %rsi
	leaq	(%rax,%r12), %rcx
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L547:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L403
.L543:
	cmpq	$2147483647, %rax
	cmovbe	%rax, %rdi
	leaq	7(%rdi), %rsi
	movq	%rdi, %r12
	andq	$-8, %rsi
.L440:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rsi, %rdx
	jb	.L548
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L443:
	movq	32(%rbx), %rcx
	leaq	(%rax,%r12), %rdi
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L548:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L443
.L542:
	cmpq	$2147483647, %rax
	cmovbe	%rax, %rcx
	leaq	7(%rcx), %rsi
	movq	%rcx, %r13
	andq	$-8, %rsi
.L420:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L549
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L423:
	movq	32(%rbx), %rsi
	leaq	(%rax,%r13), %rcx
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L549:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L423
.L539:
	cmpq	$2147483647, %rax
	cmovbe	%rax, %r13
	leaq	7(%r13), %rsi
	andq	$-8, %rsi
.L379:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L550
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L382:
	movq	32(%rbx), %rsi
	leaq	(%rax,%r13), %rdx
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L550:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L382
.L397:
	leaq	.LC21(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L538:
	call	__stack_chk_fail@PLT
.L399:
	movl	$2147483648, %esi
	movl	$2147483647, %r12d
	jmp	.L400
.L439:
	movl	$2147483648, %esi
	movl	$2147483647, %r12d
	jmp	.L440
.L419:
	movl	$2147483648, %esi
	movl	$2147483647, %r13d
	jmp	.L420
.L378:
	movl	$2147483648, %esi
	movl	$2147483647, %r13d
	jmp	.L379
	.cfi_endproc
.LFE5874:
	.size	_ZN2v88internal13EhFrameWriter15WriteEhFrameHdrEi, .-_ZN2v88internal13EhFrameWriter15WriteEhFrameHdrEi
	.section	.text._ZN2v88internal13EhFrameWriter20SetBaseAddressOffsetEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13EhFrameWriter20SetBaseAddressOffsetEi
	.type	_ZN2v88internal13EhFrameWriter20SetBaseAddressOffsetEi, @function
_ZN2v88internal13EhFrameWriter20SetBaseAddressOffsetEi:
.LFB5877:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	24(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	40(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$14, -57(%rbp)
	cmpq	48(%rdi), %rsi
	je	.L552
	movb	$14, (%rsi)
	movq	40(%rdi), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rdi)
.L553:
	movl	%r12d, %r13d
	leaq	-57(%rbp), %r15
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L568:
	orl	$-128, %eax
	movb	%al, -57(%rbp)
	cmpq	48(%rbx), %rsi
	je	.L567
	movb	%al, (%rsi)
	addq	$1, 40(%rbx)
.L559:
	movq	40(%rbx), %rsi
.L558:
	movl	%r13d, %eax
	andl	$127, %eax
	shrl	$7, %r13d
	jne	.L568
	movb	%al, -57(%rbp)
	cmpq	48(%rbx), %rsi
	je	.L569
	movb	%al, (%rsi)
	addq	$1, 40(%rbx)
.L557:
	movl	%r12d, 16(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L570
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L567:
	.cfi_restore_state
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L552:
	leaq	-57(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movq	40(%rbx), %rsi
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L569:
	leaq	-57(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	jmp	.L557
.L570:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5877:
	.size	_ZN2v88internal13EhFrameWriter20SetBaseAddressOffsetEi, .-_ZN2v88internal13EhFrameWriter20SetBaseAddressOffsetEi
	.section	.text._ZN2v88internal13EhFrameWriter25RecordRegisterNotModifiedENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13EhFrameWriter25RecordRegisterNotModifiedENS0_8RegisterE
	.type	_ZN2v88internal13EhFrameWriter25RecordRegisterNotModifiedENS0_8RegisterE, @function
_ZN2v88internal13EhFrameWriter25RecordRegisterNotModifiedENS0_8RegisterE:
.LFB5881:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	24(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	40(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$8, -41(%rbp)
	cmpq	48(%rdi), %rsi
	je	.L572
	movb	$8, (%rsi)
	addq	$1, 40(%rdi)
.L573:
	movl	%r12d, %edi
	leaq	-41(%rbp), %r14
	call	_ZN2v88internal13EhFrameWriter19RegisterToDwarfCodeENS0_8RegisterE@PLT
	movl	%eax, %r12d
.L578:
	movl	%r12d, %edx
	andl	$127, %edx
	shrl	$7, %r12d
	je	.L574
.L587:
	orl	$-128, %edx
	movq	40(%rbx), %rsi
	movb	%dl, -41(%rbp)
	cmpq	48(%rbx), %rsi
	je	.L586
	movb	%dl, (%rsi)
	movl	%r12d, %edx
	addq	$1, 40(%rbx)
	andl	$127, %edx
	shrl	$7, %r12d
	jne	.L587
.L574:
	movb	%dl, -41(%rbp)
	movq	40(%rbx), %rsi
	cmpq	48(%rbx), %rsi
	je	.L588
	movb	%dl, (%rsi)
	addq	$1, 40(%rbx)
.L571:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L589
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L586:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L572:
	leaq	-41(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L588:
	leaq	-41(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	jmp	.L571
.L589:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5881:
	.size	_ZN2v88internal13EhFrameWriter25RecordRegisterNotModifiedENS0_8RegisterE, .-_ZN2v88internal13EhFrameWriter25RecordRegisterNotModifiedENS0_8RegisterE
	.section	.text._ZN2v88internal13EhFrameWriter22SetBaseAddressRegisterENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13EhFrameWriter22SetBaseAddressRegisterENS0_8RegisterE
	.type	_ZN2v88internal13EhFrameWriter22SetBaseAddressRegisterENS0_8RegisterE, @function
_ZN2v88internal13EhFrameWriter22SetBaseAddressRegisterENS0_8RegisterE:
.LFB5878:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	%esi, %edi
	leaq	24(%rbx), %r14
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal13EhFrameWriter19RegisterToDwarfCodeENS0_8RegisterE@PLT
	movb	$13, -57(%rbp)
	movq	40(%rbx), %rsi
	movl	%eax, %r12d
	cmpq	48(%rbx), %rsi
	je	.L591
	movb	$13, (%rsi)
	movq	40(%rbx), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rbx)
.L592:
	leaq	-57(%rbp), %r15
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L607:
	orl	$-128, %eax
	movb	%al, -57(%rbp)
	cmpq	48(%rbx), %rsi
	je	.L606
	movb	%al, (%rsi)
	addq	$1, 40(%rbx)
.L598:
	movq	40(%rbx), %rsi
.L597:
	movl	%r12d, %eax
	andl	$127, %eax
	shrl	$7, %r12d
	jne	.L607
	movb	%al, -57(%rbp)
	cmpq	48(%rbx), %rsi
	je	.L608
	movb	%al, (%rsi)
	addq	$1, 40(%rbx)
.L596:
	movl	%r13d, 12(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L609
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L606:
	.cfi_restore_state
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L591:
	leaq	-57(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movq	40(%rbx), %rsi
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L608:
	leaq	-57(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	jmp	.L596
.L609:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5878:
	.size	_ZN2v88internal13EhFrameWriter22SetBaseAddressRegisterENS0_8RegisterE, .-_ZN2v88internal13EhFrameWriter22SetBaseAddressRegisterENS0_8RegisterE
	.section	.text._ZN2v88internal13EhFrameWriter31SetBaseAddressRegisterAndOffsetENS0_8RegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13EhFrameWriter31SetBaseAddressRegisterAndOffsetENS0_8RegisterEi
	.type	_ZN2v88internal13EhFrameWriter31SetBaseAddressRegisterAndOffsetENS0_8RegisterEi, @function
_ZN2v88internal13EhFrameWriter31SetBaseAddressRegisterAndOffsetENS0_8RegisterEi:
.LFB5879:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	%esi, %edi
	leaq	24(%rbx), %r15
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal13EhFrameWriter19RegisterToDwarfCodeENS0_8RegisterE@PLT
	movb	$12, -57(%rbp)
	movq	40(%rbx), %rsi
	movl	%eax, %r14d
	cmpq	48(%rbx), %rsi
	je	.L611
	movb	$12, (%rsi)
	movq	40(%rbx), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rbx)
.L612:
	leaq	-57(%rbp), %rdx
.L617:
	movl	%r14d, %eax
	andl	$127, %eax
	shrl	$7, %r14d
	je	.L613
.L637:
	orl	$-128, %eax
	movb	%al, -57(%rbp)
	cmpq	48(%rbx), %rsi
	je	.L636
	movb	%al, (%rsi)
	movq	40(%rbx), %rax
	leaq	1(%rax), %rsi
	movl	%r14d, %eax
	andl	$127, %eax
	shrl	$7, %r14d
	movq	%rsi, 40(%rbx)
	jne	.L637
.L613:
	movb	%al, -57(%rbp)
	cmpq	48(%rbx), %rsi
	je	.L638
	movb	%al, (%rsi)
	movq	40(%rbx), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rbx)
.L616:
	movl	%r13d, %r14d
	leaq	-57(%rbp), %rdx
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L640:
	orl	$-128, %eax
	movb	%al, -57(%rbp)
	cmpq	48(%rbx), %rsi
	je	.L639
	movb	%al, (%rsi)
	addq	$1, 40(%rbx)
.L623:
	movq	40(%rbx), %rsi
.L622:
	movl	%r14d, %eax
	andl	$127, %eax
	shrl	$7, %r14d
	jne	.L640
	movb	%al, -57(%rbp)
	cmpq	48(%rbx), %rsi
	je	.L641
	movb	%al, (%rsi)
	addq	$1, 40(%rbx)
.L621:
	movl	%r13d, 16(%rbx)
	movl	%r12d, 12(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L642
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L636:
	.cfi_restore_state
	movq	%r15, %rdi
	movq	%rdx, -72(%rbp)
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movq	40(%rbx), %rsi
	movq	-72(%rbp), %rdx
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L639:
	movq	%r15, %rdi
	movq	%rdx, -72(%rbp)
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movq	-72(%rbp), %rdx
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L611:
	leaq	-57(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movq	40(%rbx), %rsi
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L638:
	leaq	-57(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movq	40(%rbx), %rsi
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L641:
	leaq	-57(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	jmp	.L621
.L642:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5879:
	.size	_ZN2v88internal13EhFrameWriter31SetBaseAddressRegisterAndOffsetENS0_8RegisterEi, .-_ZN2v88internal13EhFrameWriter31SetBaseAddressRegisterAndOffsetENS0_8RegisterEi
	.section	.text._ZN2v88internal13EhFrameWriter15AdvanceLocationEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13EhFrameWriter15AdvanceLocationEi
	.type	_ZN2v88internal13EhFrameWriter15AdvanceLocationEi, @function
_ZN2v88internal13EhFrameWriter15AdvanceLocationEi:
.LFB5876:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$24, %rdi
	subq	$40, %rsp
	movq	40(%rbx), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%esi, %eax
	subl	-20(%rdi), %eax
	movq	48(%rbx), %rsi
	divl	_ZN2v88internal16EhFrameConstants20kCodeAlignmentFactorE(%rip)
	movl	%eax, %r12d
	cmpl	$63, %eax
	jbe	.L745
	cmpl	$255, %eax
	jbe	.L746
	cmpl	$65535, %eax
	ja	.L653
	movb	$3, -60(%rbp)
	cmpq	%rcx, %rsi
	je	.L654
	movb	$3, (%rcx)
	movq	40(%rbx), %rax
	leaq	1(%rax), %r14
	movq	%r14, 40(%rbx)
.L655:
	movq	48(%rbx), %rax
	movw	%r12w, -62(%rbp)
	subq	%r14, %rax
	cmpq	$1, %rax
	jbe	.L656
	movw	%r12w, (%r14)
	addq	$2, 40(%rbx)
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L746:
	movb	$2, -60(%rbp)
	cmpq	%rcx, %rsi
	je	.L649
	movb	$2, (%rcx)
	movq	40(%rbx), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rbx)
.L650:
	movb	%r12b, -60(%rbp)
	cmpq	%rsi, 48(%rbx)
	je	.L651
	movb	%r12b, (%rsi)
	addq	$1, 40(%rbx)
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L745:
	orl	$64, %r12d
	movb	%r12b, -60(%rbp)
	cmpq	%rcx, %rsi
	je	.L651
	movb	%r12b, (%rcx)
	addq	$1, 40(%rbx)
.L647:
	movl	%r13d, 4(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L747
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L653:
	.cfi_restore_state
	movb	$4, -60(%rbp)
	cmpq	%rcx, %rsi
	je	.L680
	movb	$4, (%rcx)
	movq	40(%rbx), %rax
	leaq	1(%rax), %r14
	movq	%r14, 40(%rbx)
.L681:
	movq	48(%rbx), %rax
	movl	%r12d, -60(%rbp)
	subq	%r14, %rax
	cmpq	$3, %rax
	jbe	.L682
	movl	%r12d, (%r14)
	addq	$4, 40(%rbx)
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L651:
	leaq	-60(%rbp), %rdx
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L656:
	movq	32(%rbx), %rcx
	movq	%r14, %rsi
	movl	$2147483647, %edi
	movq	%rdi, %rax
	subq	%rcx, %rsi
	subq	%rsi, %rax
	cmpq	$1, %rax
	jbe	.L684
	movl	$2, %edx
	cmpq	$2, %rsi
	movq	%rdx, %rax
	cmovnb	%rsi, %rax
	addq	%rsi, %rax
	jc	.L660
	testq	%rax, %rax
	jne	.L748
	xorl	%edi, %edi
	xorl	%eax, %eax
.L662:
	cmpq	%rcx, %r14
	je	.L749
	leaq	-1(%r14), %rdx
	movq	%r14, %r8
	subq	%rcx, %rdx
	subq	%rcx, %r8
	cmpq	$14, %rdx
	jbe	.L668
	leaq	15(%rax), %rdx
	subq	%rcx, %rdx
	cmpq	$30, %rdx
	jbe	.L668
	movq	%r8, %rsi
	xorl	%edx, %edx
	andq	$-16, %rsi
	.p2align 4,,10
	.p2align 3
.L669:
	movdqu	(%rcx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L669
	movq	%r8, %r9
	andq	$-16, %r9
	leaq	(%rcx,%r9), %rdx
	leaq	(%rax,%r9), %rsi
	cmpq	%r8, %r9
	je	.L674
	movzbl	(%rdx), %r8d
	movb	%r8b, (%rsi)
	leaq	1(%rdx), %r8
	cmpq	%r8, %r14
	je	.L674
	movzbl	1(%rdx), %r8d
	movb	%r8b, 1(%rsi)
	leaq	2(%rdx), %r8
	cmpq	%r8, %r14
	je	.L674
	movzbl	2(%rdx), %r8d
	movb	%r8b, 2(%rsi)
	leaq	3(%rdx), %r8
	cmpq	%r8, %r14
	je	.L674
	movzbl	3(%rdx), %r8d
	movb	%r8b, 3(%rsi)
	leaq	4(%rdx), %r8
	cmpq	%r8, %r14
	je	.L674
	movzbl	4(%rdx), %r8d
	movb	%r8b, 4(%rsi)
	leaq	5(%rdx), %r8
	cmpq	%r8, %r14
	je	.L674
	movzbl	5(%rdx), %r8d
	movb	%r8b, 5(%rsi)
	leaq	6(%rdx), %r8
	cmpq	%r8, %r14
	je	.L674
	movzbl	6(%rdx), %r8d
	movb	%r8b, 6(%rsi)
	leaq	7(%rdx), %r8
	cmpq	%r8, %r14
	je	.L674
	movzbl	7(%rdx), %r8d
	movb	%r8b, 7(%rsi)
	leaq	8(%rdx), %r8
	cmpq	%r8, %r14
	je	.L674
	movzbl	8(%rdx), %r8d
	movb	%r8b, 8(%rsi)
	leaq	9(%rdx), %r8
	cmpq	%r8, %r14
	je	.L674
	movzbl	9(%rdx), %r8d
	movb	%r8b, 9(%rsi)
	leaq	10(%rdx), %r8
	cmpq	%r8, %r14
	je	.L674
	movzbl	10(%rdx), %r8d
	movb	%r8b, 10(%rsi)
	leaq	11(%rdx), %r8
	cmpq	%r8, %r14
	je	.L674
	movzbl	11(%rdx), %r8d
	movb	%r8b, 11(%rsi)
	leaq	12(%rdx), %r8
	cmpq	%r8, %r14
	je	.L674
	movzbl	12(%rdx), %r8d
	movb	%r8b, 12(%rsi)
	leaq	13(%rdx), %r8
	cmpq	%r8, %r14
	je	.L674
	movzbl	13(%rdx), %r8d
	movb	%r8b, 13(%rsi)
	leaq	14(%rdx), %r8
	cmpq	%r8, %r14
	je	.L674
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rsi)
	.p2align 4,,10
	.p2align 3
.L674:
	movq	%r14, %rsi
	subq	%rcx, %rsi
	movq	%rsi, %rcx
	addq	%rax, %rcx
.L671:
	movl	%r12d, %edx
	movb	%r12b, (%rcx)
	leaq	2(%rcx), %r9
	movb	%dh, 1(%rcx)
	movq	40(%rbx), %rsi
	cmpq	%r14, %rsi
	je	.L667
	leaq	-1(%rsi), %rdx
	movq	%rsi, %r10
	subq	%r14, %rdx
	subq	%r14, %r10
	cmpq	$14, %rdx
	jbe	.L675
	leaq	17(%rcx), %rdx
	subq	%r14, %rdx
	cmpq	$30, %rdx
	jbe	.L675
	movq	%r10, %r8
	xorl	%edx, %edx
	andq	$-16, %r8
	.p2align 4,,10
	.p2align 3
.L676:
	movdqu	(%r14,%rdx), %xmm2
	movups	%xmm2, 2(%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%r8, %rdx
	jne	.L676
	movq	%r10, %r8
	andq	$-16, %r8
	leaq	(%r14,%r8), %rdx
	leaq	(%r9,%r8), %rcx
	cmpq	%r8, %r10
	je	.L679
	movzbl	(%rdx), %r8d
	movb	%r8b, (%rcx)
	leaq	1(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L679
	movzbl	1(%rdx), %r8d
	movb	%r8b, 1(%rcx)
	leaq	2(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L679
	movzbl	2(%rdx), %r8d
	movb	%r8b, 2(%rcx)
	leaq	3(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L679
	movzbl	3(%rdx), %r8d
	movb	%r8b, 3(%rcx)
	leaq	4(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L679
	movzbl	4(%rdx), %r8d
	movb	%r8b, 4(%rcx)
	leaq	5(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L679
	movzbl	5(%rdx), %r8d
	movb	%r8b, 5(%rcx)
	leaq	6(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L679
	movzbl	6(%rdx), %r8d
	movb	%r8b, 6(%rcx)
	leaq	7(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L679
	movzbl	7(%rdx), %r8d
	movb	%r8b, 7(%rcx)
	leaq	8(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L679
	movzbl	8(%rdx), %r8d
	movb	%r8b, 8(%rcx)
	leaq	9(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L679
	movzbl	9(%rdx), %r8d
	movb	%r8b, 9(%rcx)
	leaq	10(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L679
	movzbl	10(%rdx), %r8d
	movb	%r8b, 10(%rcx)
	leaq	11(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L679
	movzbl	11(%rdx), %r8d
	movb	%r8b, 11(%rcx)
	leaq	12(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L679
	movzbl	12(%rdx), %r8d
	movb	%r8b, 12(%rcx)
	leaq	13(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L679
	movzbl	13(%rdx), %r8d
	movb	%r8b, 13(%rcx)
	leaq	14(%rdx), %r8
	cmpq	%r8, %rsi
	je	.L679
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rcx)
	.p2align 4,,10
	.p2align 3
.L679:
	subq	%r14, %rsi
	addq	%rsi, %r9
.L667:
	movq	%rax, %xmm0
	movq	%r9, %xmm5
	movq	%rdi, 48(%rbx)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, 32(%rbx)
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L682:
	movq	32(%rbx), %rcx
	movq	%r14, %rax
	movl	$2147483647, %edi
	movq	%rdi, %rdx
	subq	%rcx, %rax
	subq	%rax, %rdx
	cmpq	$3, %rdx
	jbe	.L684
	cmpq	$4, %rax
	movl	$4, %edx
	cmovnb	%rax, %rdx
	addq	%rdx, %rax
	jc	.L686
	testq	%rax, %rax
	jne	.L750
	xorl	%edi, %edi
	xorl	%eax, %eax
.L688:
	cmpq	%r14, %rcx
	je	.L708
	leaq	-1(%r14), %rdx
	subq	%rcx, %rdx
	cmpq	$14, %rdx
	jbe	.L692
	leaq	15(%rcx), %rdx
	subq	%rax, %rdx
	cmpq	$30, %rdx
	jbe	.L692
	movq	%r14, %r9
	xorl	%edx, %edx
	subq	%rcx, %r9
	movq	%r9, %rsi
	andq	$-16, %rsi
	.p2align 4,,10
	.p2align 3
.L693:
	movdqu	(%rcx,%rdx), %xmm3
	movups	%xmm3, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L693
	movq	%r9, %r8
	andq	$-16, %r8
	leaq	(%rcx,%r8), %rdx
	leaq	(%rax,%r8), %rsi
	cmpq	%r8, %r9
	je	.L696
	movzbl	(%rdx), %r8d
	movb	%r8b, (%rsi)
	leaq	1(%rdx), %r8
	cmpq	%r8, %r14
	je	.L696
	movzbl	1(%rdx), %r8d
	movb	%r8b, 1(%rsi)
	leaq	2(%rdx), %r8
	cmpq	%r8, %r14
	je	.L696
	movzbl	2(%rdx), %r8d
	movb	%r8b, 2(%rsi)
	leaq	3(%rdx), %r8
	cmpq	%r8, %r14
	je	.L696
	movzbl	3(%rdx), %r8d
	movb	%r8b, 3(%rsi)
	leaq	4(%rdx), %r8
	cmpq	%r8, %r14
	je	.L696
	movzbl	4(%rdx), %r8d
	movb	%r8b, 4(%rsi)
	leaq	5(%rdx), %r8
	cmpq	%r8, %r14
	je	.L696
	movzbl	5(%rdx), %r8d
	movb	%r8b, 5(%rsi)
	leaq	6(%rdx), %r8
	cmpq	%r8, %r14
	je	.L696
	movzbl	6(%rdx), %r8d
	movb	%r8b, 6(%rsi)
	leaq	7(%rdx), %r8
	cmpq	%r8, %r14
	je	.L696
	movzbl	7(%rdx), %r8d
	movb	%r8b, 7(%rsi)
	leaq	8(%rdx), %r8
	cmpq	%r8, %r14
	je	.L696
	movzbl	8(%rdx), %r8d
	movb	%r8b, 8(%rsi)
	leaq	9(%rdx), %r8
	cmpq	%r8, %r14
	je	.L696
	movzbl	9(%rdx), %r8d
	movb	%r8b, 9(%rsi)
	leaq	10(%rdx), %r8
	cmpq	%r8, %r14
	je	.L696
	movzbl	10(%rdx), %r8d
	movb	%r8b, 10(%rsi)
	leaq	11(%rdx), %r8
	cmpq	%r8, %r14
	je	.L696
	movzbl	11(%rdx), %r8d
	movb	%r8b, 11(%rsi)
	leaq	12(%rdx), %r8
	cmpq	%r8, %r14
	je	.L696
	movzbl	12(%rdx), %r8d
	movb	%r8b, 12(%rsi)
	leaq	13(%rdx), %r8
	cmpq	%r8, %r14
	je	.L696
	movzbl	13(%rdx), %r8d
	movb	%r8b, 13(%rsi)
	leaq	14(%rdx), %r8
	cmpq	%r8, %r14
	je	.L696
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rsi)
	.p2align 4,,10
	.p2align 3
.L696:
	movq	%r14, %rsi
	subq	%rcx, %rsi
	movq	%rsi, %rcx
	addq	%rax, %rcx
.L691:
	movl	-60(%rbp), %edx
	leaq	4(%rcx), %r8
	movl	%edx, (%rcx)
	movq	40(%rbx), %rsi
	cmpq	%r14, %rsi
	je	.L697
	leaq	20(%rcx), %rdx
	movq	%rsi, %r10
	cmpq	%rdx, %r14
	leaq	16(%r14), %rdx
	setnb	%r9b
	cmpq	%rdx, %r8
	setnb	%dl
	subq	%r14, %r10
	orb	%dl, %r9b
	je	.L698
	leaq	-1(%rsi), %rdx
	subq	%r14, %rdx
	cmpq	$14, %rdx
	jbe	.L698
	movq	%r10, %r9
	xorl	%edx, %edx
	andq	$-16, %r9
	.p2align 4,,10
	.p2align 3
.L699:
	movdqu	(%r14,%rdx), %xmm4
	movups	%xmm4, 4(%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%r9, %rdx
	jne	.L699
	movq	%r10, %r9
	andq	$-16, %r9
	leaq	(%r14,%r9), %rdx
	leaq	(%r8,%r9), %rcx
	cmpq	%r10, %r9
	je	.L702
	movzbl	(%rdx), %r9d
	movb	%r9b, (%rcx)
	leaq	1(%rdx), %r9
	cmpq	%r9, %rsi
	je	.L702
	movzbl	1(%rdx), %r9d
	movb	%r9b, 1(%rcx)
	leaq	2(%rdx), %r9
	cmpq	%r9, %rsi
	je	.L702
	movzbl	2(%rdx), %r9d
	movb	%r9b, 2(%rcx)
	leaq	3(%rdx), %r9
	cmpq	%r9, %rsi
	je	.L702
	movzbl	3(%rdx), %r9d
	movb	%r9b, 3(%rcx)
	leaq	4(%rdx), %r9
	cmpq	%r9, %rsi
	je	.L702
	movzbl	4(%rdx), %r9d
	movb	%r9b, 4(%rcx)
	leaq	5(%rdx), %r9
	cmpq	%r9, %rsi
	je	.L702
	movzbl	5(%rdx), %r9d
	movb	%r9b, 5(%rcx)
	leaq	6(%rdx), %r9
	cmpq	%r9, %rsi
	je	.L702
	movzbl	6(%rdx), %r9d
	movb	%r9b, 6(%rcx)
	leaq	7(%rdx), %r9
	cmpq	%r9, %rsi
	je	.L702
	movzbl	7(%rdx), %r9d
	movb	%r9b, 7(%rcx)
	leaq	8(%rdx), %r9
	cmpq	%r9, %rsi
	je	.L702
	movzbl	8(%rdx), %r9d
	movb	%r9b, 8(%rcx)
	leaq	9(%rdx), %r9
	cmpq	%r9, %rsi
	je	.L702
	movzbl	9(%rdx), %r9d
	movb	%r9b, 9(%rcx)
	leaq	10(%rdx), %r9
	cmpq	%r9, %rsi
	je	.L702
	movzbl	10(%rdx), %r9d
	movb	%r9b, 10(%rcx)
	leaq	11(%rdx), %r9
	cmpq	%r9, %rsi
	je	.L702
	movzbl	11(%rdx), %r9d
	movb	%r9b, 11(%rcx)
	leaq	12(%rdx), %r9
	cmpq	%r9, %rsi
	je	.L702
	movzbl	12(%rdx), %r9d
	movb	%r9b, 12(%rcx)
	leaq	13(%rdx), %r9
	cmpq	%r9, %rsi
	je	.L702
	movzbl	13(%rdx), %r9d
	movb	%r9b, 13(%rcx)
	leaq	14(%rdx), %r9
	cmpq	%r9, %rsi
	je	.L702
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rcx)
	.p2align 4,,10
	.p2align 3
.L702:
	subq	%r14, %rsi
	addq	%rsi, %r8
.L697:
	movq	%rax, %xmm0
	movq	%r8, %xmm6
	movq	%rdi, 48(%rbx)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 32(%rbx)
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L654:
	leaq	-60(%rbp), %rdx
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movq	40(%rbx), %r14
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L680:
	leaq	-60(%rbp), %rdx
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movq	40(%rbx), %r14
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L649:
	leaq	-60(%rbp), %rdx
	movq	%rdi, -72(%rbp)
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movq	40(%rbx), %rsi
	movq	-72(%rbp), %rdi
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L692:
	movq	%r14, %r8
	xorl	%edx, %edx
	subq	%rcx, %r8
	.p2align 4,,10
	.p2align 3
.L695:
	movzbl	(%rcx,%rdx), %esi
	movb	%sil, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%r8, %rdx
	jne	.L695
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L668:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L673:
	movzbl	(%rcx,%rdx), %esi
	movb	%sil, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%r8, %rdx
	jne	.L673
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L698:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L701:
	movzbl	(%r14,%rdx), %r9d
	movb	%r9b, 4(%rcx,%rdx)
	addq	$1, %rdx
	cmpq	%r10, %rdx
	jne	.L701
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L675:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L678:
	movzbl	(%r14,%rdx), %r8d
	movb	%r8b, 2(%rcx,%rdx)
	addq	$1, %rdx
	cmpq	%r10, %rdx
	jne	.L678
	jmp	.L679
.L708:
	movq	%rax, %rcx
	jmp	.L691
.L749:
	movq	%rax, %rcx
	jmp	.L671
.L750:
	cmpq	$2147483647, %rax
	cmovbe	%rax, %rdi
	leaq	7(%rdi), %rsi
	movq	%rdi, %r12
	andq	$-8, %rsi
.L687:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L751
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L690:
	movq	32(%rbx), %rcx
	leaq	(%rax,%r12), %rdi
	jmp	.L688
.L751:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L690
.L748:
	cmpq	$2147483647, %rax
	cmovbe	%rax, %rdi
	leaq	7(%rdi), %rsi
	movq	%rdi, %r15
	andq	$-8, %rsi
.L661:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rsi, %rdx
	jb	.L752
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L664:
	movq	32(%rbx), %rcx
	leaq	(%rax,%r15), %rdi
	jmp	.L662
.L752:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L664
.L747:
	call	__stack_chk_fail@PLT
.L684:
	leaq	.LC21(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L686:
	movl	$2147483648, %esi
	movl	$2147483647, %r12d
	jmp	.L687
.L660:
	movl	$2147483648, %esi
	movl	$2147483647, %r15d
	jmp	.L661
	.cfi_endproc
.LFE5876:
	.size	_ZN2v88internal13EhFrameWriter15AdvanceLocationEi, .-_ZN2v88internal13EhFrameWriter15AdvanceLocationEi
	.section	.text._ZN2v88internal13EhFrameWriter26RecordRegisterSavedToStackEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13EhFrameWriter26RecordRegisterSavedToStackEii
	.type	_ZN2v88internal13EhFrameWriter26RecordRegisterSavedToStackEii, @function
_ZN2v88internal13EhFrameWriter26RecordRegisterSavedToStackEii:
.LFB5880:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	24(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	40(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%edx, %eax
	cltd
	idivl	_ZN2v88internal16EhFrameConstants20kDataAlignmentFactorE(%rip)
	movq	48(%rdi), %rdx
	testl	%eax, %eax
	js	.L754
	andl	$63, %r13d
	movl	%eax, %r12d
	orl	$-128, %r13d
	movb	%r13b, -57(%rbp)
	cmpq	%rdx, %rsi
	je	.L755
	movb	%r13b, (%rsi)
	movq	40(%rdi), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rdi)
.L756:
	leaq	-57(%rbp), %r13
	jmp	.L760
	.p2align 4,,10
	.p2align 3
.L790:
	orl	$-128, %eax
	movb	%al, -57(%rbp)
	cmpq	48(%rbx), %rsi
	je	.L789
	movb	%al, (%rsi)
	addq	$1, 40(%rbx)
.L776:
	movq	40(%rbx), %rsi
.L760:
	movl	%r12d, %eax
	andl	$127, %eax
	shrl	$7, %r12d
	jne	.L790
.L769:
	movb	%al, -57(%rbp)
	cmpq	48(%rbx), %rsi
	je	.L791
	movb	%al, (%rsi)
	addq	$1, 40(%rbx)
.L753:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L792
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L789:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L791:
	leaq	-57(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L754:
	movb	$17, -57(%rbp)
	movl	%eax, %r15d
	cmpq	%rdx, %rsi
	je	.L761
	movb	$17, (%rsi)
	movq	40(%rdi), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rdi)
.L762:
	movl	%r13d, %r12d
	leaq	-57(%rbp), %r13
.L767:
	movl	%r12d, %eax
	andl	$127, %eax
	shrl	$7, %r12d
	je	.L763
.L794:
	orl	$-128, %eax
	movb	%al, -57(%rbp)
	cmpq	48(%rbx), %rsi
	je	.L793
	movb	%al, (%rsi)
	movq	40(%rbx), %rax
	leaq	1(%rax), %rsi
	movl	%r12d, %eax
	andl	$127, %eax
	shrl	$7, %r12d
	movq	%rsi, 40(%rbx)
	jne	.L794
.L763:
	movb	%al, -57(%rbp)
	cmpq	48(%rbx), %rsi
	je	.L795
	movb	%al, (%rsi)
	movq	40(%rbx), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rbx)
.L766:
	leaq	-57(%rbp), %r12
	jmp	.L773
	.p2align 4,,10
	.p2align 3
.L768:
	orl	$-128, %eax
	movb	%al, -57(%rbp)
	cmpq	48(%rbx), %rsi
	je	.L796
	movb	%al, (%rsi)
	addq	$1, 40(%rbx)
.L771:
	movq	40(%rbx), %rsi
.L773:
	movl	%r15d, %eax
	movl	%r15d, %edx
	sarl	$7, %r15d
	andl	$127, %eax
	cmpl	$-1, %r15d
	jne	.L768
	andl	$64, %edx
	je	.L768
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L793:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movq	40(%rbx), %rsi
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L755:
	leaq	-57(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movq	40(%rbx), %rsi
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L795:
	leaq	-57(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movq	40(%rbx), %rsi
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L761:
	leaq	-57(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movq	40(%rbx), %rsi
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L796:
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	jmp	.L771
.L792:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5880:
	.size	_ZN2v88internal13EhFrameWriter26RecordRegisterSavedToStackEii, .-_ZN2v88internal13EhFrameWriter26RecordRegisterSavedToStackEii
	.section	.text._ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS4_EET_SC_St20forward_iterator_tag,"axG",@progbits,_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS4_EET_SC_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS4_EET_SC_St20forward_iterator_tag
	.type	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS4_EET_SC_St20forward_iterator_tag, @function
_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS4_EET_SC_St20forward_iterator_tag:
.LFB6432:
	.cfi_startproc
	endbr64
	cmpq	%rcx, %rdx
	je	.L903
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	subq	%rdx, %r14
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	16(%rdi), %rdi
	movq	24(%rbx), %rax
	subq	%rdi, %rax
	cmpq	%r14, %rax
	jb	.L800
	movq	%rdi, %r8
	subq	%rsi, %r8
	cmpq	%r8, %r14
	jnb	.L801
	movq	%rdi, %rdx
	leaq	16(%rdi), %rax
	subq	%r14, %rdx
	cmpq	%rax, %rdx
	movl	$16, %eax
	setnb	%cl
	subq	%r14, %rax
	testq	%rax, %rax
	setle	%al
	orb	%al, %cl
	je	.L844
	leaq	-1(%rdi), %rax
	subq	%rdx, %rax
	cmpq	$14, %rax
	jbe	.L844
	movq	%r14, %rcx
	xorl	%eax, %eax
	andq	$-16, %rcx
	.p2align 4,,10
	.p2align 3
.L803:
	movdqu	(%rdx,%rax), %xmm1
	movups	%xmm1, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L803
	movq	%r14, %rsi
	andq	$-16, %rsi
	leaq	(%rdx,%rsi), %rax
	leaq	(%rdi,%rsi), %rcx
	cmpq	%rsi, %r14
	je	.L805
	movzbl	(%rax), %esi
	movb	%sil, (%rcx)
	leaq	1(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L805
	movzbl	1(%rax), %esi
	movb	%sil, 1(%rcx)
	leaq	2(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L805
	movzbl	2(%rax), %esi
	movb	%sil, 2(%rcx)
	leaq	3(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L805
	movzbl	3(%rax), %esi
	movb	%sil, 3(%rcx)
	leaq	4(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L805
	movzbl	4(%rax), %esi
	movb	%sil, 4(%rcx)
	leaq	5(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L805
	movzbl	5(%rax), %esi
	movb	%sil, 5(%rcx)
	leaq	6(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L805
	movzbl	6(%rax), %esi
	movb	%sil, 6(%rcx)
	leaq	7(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L805
	movzbl	7(%rax), %esi
	movb	%sil, 7(%rcx)
	leaq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L805
	movzbl	8(%rax), %esi
	movb	%sil, 8(%rcx)
	leaq	9(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L805
	movzbl	9(%rax), %esi
	movb	%sil, 9(%rcx)
	leaq	10(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L805
	movzbl	10(%rax), %esi
	movb	%sil, 10(%rcx)
	leaq	11(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L805
	movzbl	11(%rax), %esi
	movb	%sil, 11(%rcx)
	leaq	12(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L805
	movzbl	12(%rax), %esi
	movb	%sil, 12(%rcx)
	leaq	13(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L805
	movzbl	13(%rax), %esi
	movb	%sil, 13(%rcx)
	leaq	14(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L805
	movzbl	14(%rax), %eax
	movb	%al, 14(%rcx)
	.p2align 4,,10
	.p2align 3
.L805:
	addq	%r14, 16(%rbx)
	subq	%r12, %rdx
	jne	.L907
.L806:
	movq	%r14, %rdx
.L906:
	addq	$16, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	memmove@PLT
	.p2align 4,,10
	.p2align 3
.L800:
	.cfi_restore_state
	movq	8(%rbx), %r8
	movl	$2147483647, %edx
	movq	%rdx, %rax
	subq	%r8, %rdi
	subq	%rdi, %rax
	cmpq	%rax, %r14
	ja	.L908
	cmpq	%rdi, %r14
	cmovb	%rdi, %r14
	addq	%r14, %rdi
	jc	.L822
	testq	%rdi, %rdi
	jne	.L909
	xorl	%edx, %edx
	xorl	%eax, %eax
.L824:
	cmpq	%r8, %r12
	je	.L848
	leaq	-1(%r12), %rsi
	subq	%r8, %rsi
	cmpq	$14, %rsi
	jbe	.L828
	leaq	15(%rax), %rsi
	subq	%r8, %rsi
	cmpq	$30, %rsi
	jbe	.L828
	movq	%r12, %r10
	xorl	%esi, %esi
	subq	%r8, %r10
	movq	%r10, %rdi
	andq	$-16, %rdi
	.p2align 4,,10
	.p2align 3
.L829:
	movdqu	(%r8,%rsi), %xmm5
	movups	%xmm5, (%rax,%rsi)
	addq	$16, %rsi
	cmpq	%rdi, %rsi
	jne	.L829
	movq	%r10, %r9
	andq	$-16, %r9
	leaq	(%r8,%r9), %rsi
	leaq	(%rax,%r9), %rdi
	cmpq	%r9, %r10
	je	.L832
	movzbl	(%rsi), %r9d
	movb	%r9b, (%rdi)
	leaq	1(%rsi), %r9
	cmpq	%r9, %r12
	je	.L832
	movzbl	1(%rsi), %r9d
	movb	%r9b, 1(%rdi)
	leaq	2(%rsi), %r9
	cmpq	%r9, %r12
	je	.L832
	movzbl	2(%rsi), %r9d
	movb	%r9b, 2(%rdi)
	leaq	3(%rsi), %r9
	cmpq	%r9, %r12
	je	.L832
	movzbl	3(%rsi), %r9d
	movb	%r9b, 3(%rdi)
	leaq	4(%rsi), %r9
	cmpq	%r9, %r12
	je	.L832
	movzbl	4(%rsi), %r9d
	movb	%r9b, 4(%rdi)
	leaq	5(%rsi), %r9
	cmpq	%r9, %r12
	je	.L832
	movzbl	5(%rsi), %r9d
	movb	%r9b, 5(%rdi)
	leaq	6(%rsi), %r9
	cmpq	%r9, %r12
	je	.L832
	movzbl	6(%rsi), %r9d
	movb	%r9b, 6(%rdi)
	leaq	7(%rsi), %r9
	cmpq	%r9, %r12
	je	.L832
	movzbl	7(%rsi), %r9d
	movb	%r9b, 7(%rdi)
	leaq	8(%rsi), %r9
	cmpq	%r9, %r12
	je	.L832
	movzbl	8(%rsi), %r9d
	movb	%r9b, 8(%rdi)
	leaq	9(%rsi), %r9
	cmpq	%r9, %r12
	je	.L832
	movzbl	9(%rsi), %r9d
	movb	%r9b, 9(%rdi)
	leaq	10(%rsi), %r9
	cmpq	%r9, %r12
	je	.L832
	movzbl	10(%rsi), %r9d
	movb	%r9b, 10(%rdi)
	leaq	11(%rsi), %r9
	cmpq	%r9, %r12
	je	.L832
	movzbl	11(%rsi), %r9d
	movb	%r9b, 11(%rdi)
	leaq	12(%rsi), %r9
	cmpq	%r9, %r12
	je	.L832
	movzbl	12(%rsi), %r9d
	movb	%r9b, 12(%rdi)
	leaq	13(%rsi), %r9
	cmpq	%r9, %r12
	je	.L832
	movzbl	13(%rsi), %r9d
	movb	%r9b, 13(%rdi)
	leaq	14(%rsi), %r9
	cmpq	%r9, %r12
	je	.L832
	movzbl	14(%rsi), %esi
	movb	%sil, 14(%rdi)
	.p2align 4,,10
	.p2align 3
.L832:
	movq	%r12, %rsi
	subq	%r8, %rsi
	leaq	(%rax,%rsi), %r9
.L827:
	leaq	-1(%rcx), %rsi
	subq	%r13, %rsi
	cmpq	$14, %rsi
	jbe	.L833
	leaq	15(%r13), %rsi
	subq	%r9, %rsi
	cmpq	$30, %rsi
	jbe	.L833
	movq	%rcx, %r10
	xorl	%esi, %esi
	subq	%r13, %r10
	movq	%r10, %rdi
	andq	$-16, %rdi
	.p2align 4,,10
	.p2align 3
.L834:
	movdqu	0(%r13,%rsi), %xmm2
	movups	%xmm2, (%r9,%rsi)
	addq	$16, %rsi
	cmpq	%rdi, %rsi
	jne	.L834
	movq	%r10, %rsi
	andq	$-16, %rsi
	leaq	0(%r13,%rsi), %rdi
	leaq	(%r9,%rsi), %r8
	cmpq	%rsi, %r10
	je	.L836
	movzbl	(%rdi), %esi
	movb	%sil, (%r8)
	leaq	1(%rdi), %rsi
	cmpq	%rsi, %rcx
	je	.L836
	movzbl	1(%rdi), %esi
	movb	%sil, 1(%r8)
	leaq	2(%rdi), %rsi
	cmpq	%rsi, %rcx
	je	.L836
	movzbl	2(%rdi), %esi
	movb	%sil, 2(%r8)
	leaq	3(%rdi), %rsi
	cmpq	%rsi, %rcx
	je	.L836
	movzbl	3(%rdi), %esi
	movb	%sil, 3(%r8)
	leaq	4(%rdi), %rsi
	cmpq	%rsi, %rcx
	je	.L836
	movzbl	4(%rdi), %esi
	movb	%sil, 4(%r8)
	leaq	5(%rdi), %rsi
	cmpq	%rsi, %rcx
	je	.L836
	movzbl	5(%rdi), %esi
	movb	%sil, 5(%r8)
	leaq	6(%rdi), %rsi
	cmpq	%rsi, %rcx
	je	.L836
	movzbl	6(%rdi), %esi
	movb	%sil, 6(%r8)
	leaq	7(%rdi), %rsi
	cmpq	%rsi, %rcx
	je	.L836
	movzbl	7(%rdi), %esi
	movb	%sil, 7(%r8)
	leaq	8(%rdi), %rsi
	cmpq	%rsi, %rcx
	je	.L836
	movzbl	8(%rdi), %esi
	movb	%sil, 8(%r8)
	leaq	9(%rdi), %rsi
	cmpq	%rsi, %rcx
	je	.L836
	movzbl	9(%rdi), %esi
	movb	%sil, 9(%r8)
	leaq	10(%rdi), %rsi
	cmpq	%rsi, %rcx
	je	.L836
	movzbl	10(%rdi), %esi
	movb	%sil, 10(%r8)
	leaq	11(%rdi), %rsi
	cmpq	%rsi, %rcx
	je	.L836
	movzbl	11(%rdi), %esi
	movb	%sil, 11(%r8)
	leaq	12(%rdi), %rsi
	cmpq	%rsi, %rcx
	je	.L836
	movzbl	12(%rdi), %esi
	movb	%sil, 12(%r8)
	leaq	13(%rdi), %rsi
	cmpq	%rsi, %rcx
	je	.L836
	movzbl	13(%rdi), %esi
	movb	%sil, 13(%r8)
	leaq	14(%rdi), %rsi
	cmpq	%rsi, %rcx
	je	.L836
	movzbl	14(%rdi), %esi
	movb	%sil, 14(%r8)
	.p2align 4,,10
	.p2align 3
.L836:
	movq	%rcx, %rdi
	movq	16(%rbx), %r8
	subq	%r13, %rdi
	addq	%r9, %rdi
	cmpq	%r8, %r12
	je	.L838
	movq	%r9, %rsi
	subq	%r13, %rsi
	leaq	16(%rsi,%rcx), %rcx
	cmpq	%rcx, %r12
	leaq	16(%r12), %rcx
	setnb	%sil
	cmpq	%rcx, %rdi
	setnb	%cl
	orb	%cl, %sil
	je	.L839
	movq	%r12, %rcx
	notq	%rcx
	addq	%r8, %rcx
	cmpq	$14, %rcx
	jbe	.L839
	movq	%r8, %r10
	xorl	%ecx, %ecx
	subq	%r12, %r10
	movq	%r10, %rsi
	andq	$-16, %rsi
	.p2align 4,,10
	.p2align 3
.L840:
	movdqu	(%r12,%rcx), %xmm6
	movups	%xmm6, (%rdi,%rcx)
	addq	$16, %rcx
	cmpq	%rsi, %rcx
	jne	.L840
	movq	%r10, %r9
	andq	$-16, %r9
	leaq	(%r12,%r9), %rsi
	leaq	(%rdi,%r9), %rcx
	cmpq	%r9, %r10
	je	.L843
	movzbl	(%rsi), %r9d
	movb	%r9b, (%rcx)
	leaq	1(%rsi), %r9
	cmpq	%r9, %r8
	je	.L843
	movzbl	1(%rsi), %r9d
	movb	%r9b, 1(%rcx)
	leaq	2(%rsi), %r9
	cmpq	%r9, %r8
	je	.L843
	movzbl	2(%rsi), %r9d
	movb	%r9b, 2(%rcx)
	leaq	3(%rsi), %r9
	cmpq	%r9, %r8
	je	.L843
	movzbl	3(%rsi), %r9d
	movb	%r9b, 3(%rcx)
	leaq	4(%rsi), %r9
	cmpq	%r9, %r8
	je	.L843
	movzbl	4(%rsi), %r9d
	movb	%r9b, 4(%rcx)
	leaq	5(%rsi), %r9
	cmpq	%r9, %r8
	je	.L843
	movzbl	5(%rsi), %r9d
	movb	%r9b, 5(%rcx)
	leaq	6(%rsi), %r9
	cmpq	%r9, %r8
	je	.L843
	movzbl	6(%rsi), %r9d
	movb	%r9b, 6(%rcx)
	leaq	7(%rsi), %r9
	cmpq	%r9, %r8
	je	.L843
	movzbl	7(%rsi), %r9d
	movb	%r9b, 7(%rcx)
	leaq	8(%rsi), %r9
	cmpq	%r9, %r8
	je	.L843
	movzbl	8(%rsi), %r9d
	movb	%r9b, 8(%rcx)
	leaq	9(%rsi), %r9
	cmpq	%r9, %r8
	je	.L843
	movzbl	9(%rsi), %r9d
	movb	%r9b, 9(%rcx)
	leaq	10(%rsi), %r9
	cmpq	%r9, %r8
	je	.L843
	movzbl	10(%rsi), %r9d
	movb	%r9b, 10(%rcx)
	leaq	11(%rsi), %r9
	cmpq	%r9, %r8
	je	.L843
	movzbl	11(%rsi), %r9d
	movb	%r9b, 11(%rcx)
	leaq	12(%rsi), %r9
	cmpq	%r9, %r8
	je	.L843
	movzbl	12(%rsi), %r9d
	movb	%r9b, 12(%rcx)
	leaq	13(%rsi), %r9
	cmpq	%r9, %r8
	je	.L843
	movzbl	13(%rsi), %r9d
	movb	%r9b, 13(%rcx)
	leaq	14(%rsi), %r9
	cmpq	%r9, %r8
	je	.L843
	movzbl	14(%rsi), %esi
	movb	%sil, 14(%rcx)
	.p2align 4,,10
	.p2align 3
.L843:
	movq	%r8, %rcx
	subq	%r12, %rcx
	addq	%rcx, %rdi
.L838:
	movq	%rax, %xmm0
	movq	%rdi, %xmm7
	movq	%rdx, 24(%rbx)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L801:
	.cfi_restore_state
	leaq	(%rdx,%r8), %rdx
	cmpq	%rdx, %rcx
	je	.L845
	leaq	16(%r13,%r8), %rax
	cmpq	%rax, %rdi
	leaq	16(%rdi), %rax
	setnb	%sil
	cmpq	%rax, %rdx
	setnb	%al
	orb	%al, %sil
	je	.L808
	leaq	-1(%rcx), %rax
	subq	%rdx, %rax
	cmpq	$14, %rax
	jbe	.L808
	movq	%rcx, %r9
	xorl	%eax, %eax
	subq	%rdx, %r9
	movq	%r9, %rsi
	andq	$-16, %rsi
	.p2align 4,,10
	.p2align 3
.L809:
	movdqu	(%rdx,%rax), %xmm3
	movups	%xmm3, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L809
	movq	%r9, %rsi
	andq	$-16, %rsi
	addq	%rsi, %rdx
	leaq	(%rdi,%rsi), %rax
	cmpq	%rsi, %r9
	je	.L811
	movzbl	(%rdx), %esi
	movb	%sil, (%rax)
	leaq	1(%rdx), %rsi
	cmpq	%rsi, %rcx
	je	.L811
	movzbl	1(%rdx), %esi
	movb	%sil, 1(%rax)
	leaq	2(%rdx), %rsi
	cmpq	%rsi, %rcx
	je	.L811
	movzbl	2(%rdx), %esi
	movb	%sil, 2(%rax)
	leaq	3(%rdx), %rsi
	cmpq	%rsi, %rcx
	je	.L811
	movzbl	3(%rdx), %esi
	movb	%sil, 3(%rax)
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %rcx
	je	.L811
	movzbl	4(%rdx), %esi
	movb	%sil, 4(%rax)
	leaq	5(%rdx), %rsi
	cmpq	%rsi, %rcx
	je	.L811
	movzbl	5(%rdx), %esi
	movb	%sil, 5(%rax)
	leaq	6(%rdx), %rsi
	cmpq	%rsi, %rcx
	je	.L811
	movzbl	6(%rdx), %esi
	movb	%sil, 6(%rax)
	leaq	7(%rdx), %rsi
	cmpq	%rsi, %rcx
	je	.L811
	movzbl	7(%rdx), %esi
	movb	%sil, 7(%rax)
	leaq	8(%rdx), %rsi
	cmpq	%rsi, %rcx
	je	.L811
	movzbl	8(%rdx), %esi
	movb	%sil, 8(%rax)
	leaq	9(%rdx), %rsi
	cmpq	%rsi, %rcx
	je	.L811
	movzbl	9(%rdx), %esi
	movb	%sil, 9(%rax)
	leaq	10(%rdx), %rsi
	cmpq	%rsi, %rcx
	je	.L811
	movzbl	10(%rdx), %esi
	movb	%sil, 10(%rax)
	leaq	11(%rdx), %rsi
	cmpq	%rsi, %rcx
	je	.L811
	movzbl	11(%rdx), %esi
	movb	%sil, 11(%rax)
	leaq	12(%rdx), %rsi
	cmpq	%rsi, %rcx
	je	.L811
	movzbl	12(%rdx), %esi
	movb	%sil, 12(%rax)
	leaq	13(%rdx), %rsi
	cmpq	%rsi, %rcx
	je	.L811
	movzbl	13(%rdx), %esi
	movb	%sil, 13(%rax)
	leaq	14(%rdx), %rsi
	cmpq	%rsi, %rcx
	je	.L811
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L811:
	movq	16(%rbx), %rdx
.L807:
	movq	%r14, %rax
	subq	%r8, %rax
	leaq	(%rdx,%rax), %rcx
	movq	%rcx, 16(%rbx)
	cmpq	%r12, %rdi
	je	.L813
	leaq	16(%rdx,%rax), %rax
	cmpq	%rax, %r12
	leaq	16(%r12), %rax
	setnb	%dl
	cmpq	%rax, %rcx
	setnb	%al
	orb	%al, %dl
	je	.L814
	movq	%r12, %rax
	notq	%rax
	addq	%rdi, %rax
	cmpq	$14, %rax
	jbe	.L814
	movq	%rdi, %r9
	xorl	%eax, %eax
	subq	%r12, %r9
	movq	%r9, %rdx
	andq	$-16, %rdx
	.p2align 4,,10
	.p2align 3
.L815:
	movdqu	(%r12,%rax), %xmm4
	movups	%xmm4, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L815
	movq	%r9, %rsi
	andq	$-16, %rsi
	leaq	(%r12,%rsi), %rdx
	leaq	(%rcx,%rsi), %rax
	cmpq	%rsi, %r9
	je	.L818
	movzbl	(%rdx), %ecx
	movb	%cl, (%rax)
	leaq	1(%rdx), %rcx
	cmpq	%rcx, %rdi
	je	.L818
	movzbl	1(%rdx), %ecx
	movb	%cl, 1(%rax)
	leaq	2(%rdx), %rcx
	cmpq	%rcx, %rdi
	je	.L818
	movzbl	2(%rdx), %ecx
	movb	%cl, 2(%rax)
	leaq	3(%rdx), %rcx
	cmpq	%rcx, %rdi
	je	.L818
	movzbl	3(%rdx), %ecx
	movb	%cl, 3(%rax)
	leaq	4(%rdx), %rcx
	cmpq	%rcx, %rdi
	je	.L818
	movzbl	4(%rdx), %ecx
	movb	%cl, 4(%rax)
	leaq	5(%rdx), %rcx
	cmpq	%rcx, %rdi
	je	.L818
	movzbl	5(%rdx), %ecx
	movb	%cl, 5(%rax)
	leaq	6(%rdx), %rcx
	cmpq	%rcx, %rdi
	je	.L818
	movzbl	6(%rdx), %ecx
	movb	%cl, 6(%rax)
	leaq	7(%rdx), %rcx
	cmpq	%rcx, %rdi
	je	.L818
	movzbl	7(%rdx), %ecx
	movb	%cl, 7(%rax)
	leaq	8(%rdx), %rcx
	cmpq	%rcx, %rdi
	je	.L818
	movzbl	8(%rdx), %ecx
	movb	%cl, 8(%rax)
	leaq	9(%rdx), %rcx
	cmpq	%rcx, %rdi
	je	.L818
	movzbl	9(%rdx), %ecx
	movb	%cl, 9(%rax)
	leaq	10(%rdx), %rcx
	cmpq	%rcx, %rdi
	je	.L818
	movzbl	10(%rdx), %ecx
	movb	%cl, 10(%rax)
	leaq	11(%rdx), %rcx
	cmpq	%rcx, %rdi
	je	.L818
	movzbl	11(%rdx), %ecx
	movb	%cl, 11(%rax)
	leaq	12(%rdx), %rcx
	cmpq	%rcx, %rdi
	je	.L818
	movzbl	12(%rdx), %ecx
	movb	%cl, 12(%rax)
	leaq	13(%rdx), %rcx
	cmpq	%rcx, %rdi
	je	.L818
	movzbl	13(%rdx), %ecx
	movb	%cl, 13(%rax)
	leaq	14(%rdx), %rcx
	cmpq	%rcx, %rdi
	je	.L818
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L818:
	movq	16(%rbx), %rcx
.L813:
	leaq	(%rcx,%r8), %rax
	movq	%rax, 16(%rbx)
	testq	%r8, %r8
	jne	.L910
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L903:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L907:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	subq	%rdx, %rdi
	movq	%r12, %rsi
	call	memmove@PLT
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L910:
	movq	%r8, %rdx
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L844:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L802:
	movzbl	(%rdx,%rax), %ecx
	movb	%cl, (%rdi,%rax)
	addq	$1, %rax
	cmpq	%rax, %r14
	jne	.L802
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L808:
	subq	%rdx, %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L812:
	movzbl	(%rdx,%rax), %esi
	movb	%sil, (%rdi,%rax)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L812
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L828:
	movq	%r12, %r9
	xorl	%esi, %esi
	subq	%r8, %r9
	.p2align 4,,10
	.p2align 3
.L831:
	movzbl	(%r8,%rsi), %edi
	movb	%dil, (%rax,%rsi)
	addq	$1, %rsi
	cmpq	%r9, %rsi
	jne	.L831
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L833:
	movq	%rcx, %r8
	xorl	%edi, %edi
	subq	%r13, %r8
	.p2align 4,,10
	.p2align 3
.L837:
	movzbl	0(%r13,%rdi), %esi
	movb	%sil, (%r9,%rdi)
	addq	$1, %rdi
	cmpq	%r8, %rdi
	jne	.L837
	jmp	.L836
	.p2align 4,,10
	.p2align 3
.L814:
	subq	%r12, %rdi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L817:
	movzbl	(%r12,%rax), %edx
	movb	%dl, (%rcx,%rax)
	addq	$1, %rax
	cmpq	%rdi, %rax
	jne	.L817
	jmp	.L818
	.p2align 4,,10
	.p2align 3
.L839:
	movq	%r8, %r9
	xorl	%esi, %esi
	subq	%r12, %r9
	.p2align 4,,10
	.p2align 3
.L842:
	movzbl	(%r12,%rsi), %ecx
	movb	%cl, (%rdi,%rsi)
	addq	$1, %rsi
	cmpq	%r9, %rsi
	jne	.L842
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L845:
	movq	%rdi, %rdx
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L848:
	movq	%rax, %r9
	jmp	.L827
.L909:
	cmpq	$2147483647, %rdi
	cmovbe	%rdi, %rdx
	leaq	7(%rdx), %rsi
	movq	%rdx, %r14
	andq	$-8, %rsi
.L823:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rsi, %rdx
	jb	.L911
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L826:
	movq	8(%rbx), %r8
	leaq	(%rax,%r14), %rdx
	jmp	.L824
.L911:
	movq	%rcx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rcx
	jmp	.L826
.L822:
	movl	$2147483648, %esi
	movl	$2147483647, %r14d
	jmp	.L823
.L908:
	leaq	.LC21(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE6432:
	.size	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS4_EET_SC_St20forward_iterator_tag, .-_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS4_EET_SC_St20forward_iterator_tag
	.section	.text._ZN2v88internal13EhFrameWriter25WritePaddingToAlignedSizeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13EhFrameWriter25WritePaddingToAlignedSizeEi
	.type	_ZN2v88internal13EhFrameWriter25WritePaddingToAlignedSizeEi, @function
_ZN2v88internal13EhFrameWriter25WritePaddingToAlignedSizeEi:
.LFB5875:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	leal	7(%rsi), %ebx
	movzbl	_ZGVZN2v88internal13EhFrameWriter25WritePaddingToAlignedSizeEiE8kPadding(%rip), %eax
	andl	$-8, %ebx
	subl	%esi, %ebx
	testb	%al, %al
	je	.L920
.L914:
	movslq	%ebx, %rcx
	movq	40(%r12), %rsi
	popq	%rbx
	leaq	24(%r12), %rdi
	leaq	_ZZN2v88internal13EhFrameWriter25WritePaddingToAlignedSizeEiE8kPadding(%rip), %rdx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addq	%rdx, %rcx
	jmp	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS4_EET_SC_St20forward_iterator_tag
	.p2align 4,,10
	.p2align 3
.L920:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal13EhFrameWriter25WritePaddingToAlignedSizeEiE8kPadding(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L914
	leaq	_ZGVZN2v88internal13EhFrameWriter25WritePaddingToAlignedSizeEiE8kPadding(%rip), %rdi
	movq	$0, _ZZN2v88internal13EhFrameWriter25WritePaddingToAlignedSizeEiE8kPadding(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L914
	.cfi_endproc
.LFE5875:
	.size	_ZN2v88internal13EhFrameWriter25WritePaddingToAlignedSizeEi, .-_ZN2v88internal13EhFrameWriter25WritePaddingToAlignedSizeEi
	.section	.text._ZN2v88internal13EhFrameWriter8WriteCieEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13EhFrameWriter8WriteCieEv
	.type	_ZN2v88internal13EhFrameWriter8WriteCieEv, @function
_ZN2v88internal13EhFrameWriter8WriteCieEv:
.LFB5872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	24(%rdi), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	40(%rdi), %r15
	movq	32(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	48(%rdi), %rax
	movl	$-559038242, -60(%rbp)
	movq	%r15, %r13
	subq	%r15, %rax
	subq	%rcx, %r13
	cmpq	$3, %rax
	jbe	.L922
	movl	$-559038242, (%r15)
	movq	40(%rdi), %rax
	movq	48(%rdi), %rdx
	leaq	4(%rax), %r12
	movq	32(%rdi), %rax
	movq	%r12, 40(%rdi)
.L923:
	movq	%r12, %r15
	subq	%r12, %rdx
	movl	$0, -64(%rbp)
	subq	%rax, %r15
	cmpq	$3, %rdx
	jbe	.L943
	movl	$0, (%r12)
	movq	40(%rbx), %rax
	movq	48(%rbx), %rsi
	movb	$3, -60(%rbp)
	leaq	4(%rax), %rdx
	movq	%rdx, 40(%rbx)
	cmpq	%rdx, %rsi
	je	.L964
.L1091:
	movb	$3, (%rdx)
	movq	40(%rbx), %rax
	leaq	1(%rax), %r12
	movq	48(%rbx), %rax
	movq	%r12, 40(%rbx)
	subq	%r12, %rax
	cmpq	$3, %rax
	jbe	.L966
.L1092:
	movl	$5393530, (%r12)
	movq	40(%rbx), %rax
	movq	48(%rbx), %rsi
	leaq	4(%rax), %rdx
	movq	%rdx, 40(%rbx)
.L967:
	movl	_ZN2v88internal16EhFrameConstants20kCodeAlignmentFactorE(%rip), %r12d
	leaq	-60(%rbp), %r8
.L992:
	movl	%r12d, %eax
	movl	%r12d, %ecx
	andl	$127, %eax
	sarl	$7, %r12d
	jne	.L986
.L1082:
	andl	$64, %ecx
	je	.L987
.L988:
	orl	$-128, %eax
	movb	%al, -60(%rbp)
	cmpq	%rdx, %rsi
	je	.L1081
	movb	%al, (%rdx)
	movq	40(%rbx), %rax
	movl	%r12d, %ecx
	movq	48(%rbx), %rsi
	leaq	1(%rax), %rdx
	movl	%r12d, %eax
	andl	$127, %eax
	sarl	$7, %r12d
	movq	%rdx, 40(%rbx)
	je	.L1082
.L986:
	cmpl	$-1, %r12d
	jne	.L988
	andl	$64, %ecx
	je	.L988
.L987:
	movb	%al, -60(%rbp)
	cmpq	%rdx, %rsi
	jne	.L1083
	leaq	-60(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movq	40(%rbx), %rax
	movq	48(%rbx), %rsi
.L991:
	movl	_ZN2v88internal16EhFrameConstants20kDataAlignmentFactorE(%rip), %r12d
	leaq	-60(%rbp), %r8
	jmp	.L997
	.p2align 4,,10
	.p2align 3
.L1084:
	andl	$64, %ecx
	je	.L994
.L995:
	orl	$-128, %edx
	movb	%dl, -60(%rbp)
	cmpq	%rax, %rsi
	je	.L996
	movb	%dl, (%rax)
	addq	$1, 40(%rbx)
.L1005:
	movq	40(%rbx), %rax
	movq	48(%rbx), %rsi
.L997:
	movl	%r12d, %edx
	movl	%r12d, %ecx
	andl	$127, %edx
	sarl	$7, %r12d
	je	.L1084
	cmpl	$-1, %r12d
	jne	.L995
	andl	$64, %ecx
	je	.L995
.L994:
	movb	%dl, -60(%rbp)
	cmpq	%rax, %rsi
	je	.L1085
	movb	%dl, (%rax)
	addq	$1, 40(%rbx)
.L1004:
	movq	%rbx, %rdi
	call	_ZN2v88internal13EhFrameWriter30WriteReturnAddressRegisterCodeEv@PLT
	movb	$2, -60(%rbp)
	movq	40(%rbx), %rsi
	cmpq	48(%rbx), %rsi
	je	.L998
	movb	$2, (%rsi)
	movq	40(%rbx), %rax
	movb	$-1, -60(%rbp)
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rbx)
	cmpq	48(%rbx), %rsi
	je	.L1000
.L1087:
	movb	$-1, (%rsi)
	movq	40(%rbx), %rax
	movb	$27, -60(%rbp)
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rbx)
	cmpq	%rsi, 48(%rbx)
	je	.L1002
.L1088:
	movb	$27, (%rsi)
	addq	$1, 40(%rbx)
.L1003:
	movq	%rbx, %rdi
	call	_ZN2v88internal13EhFrameWriter22WriteInitialStateInCieEv@PLT
	movq	40(%rbx), %rsi
	subq	32(%rbx), %rsi
	movq	%rbx, %rdi
	subl	%r15d, %esi
	call	_ZN2v88internal13EhFrameWriter25WritePaddingToAlignedSizeEi
	movq	32(%rbx), %rdx
	movq	40(%rbx), %rax
	subq	%rdx, %rax
	movl	%eax, %ecx
	subl	%r15d, %eax
	subl	%r13d, %ecx
	movslq	%r13d, %r13
	movl	%ecx, (%rbx)
	movl	%eax, (%rdx,%r13)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1086
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L996:
	.cfi_restore_state
	movq	%r8, %rdx
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movq	-72(%rbp), %r8
	jmp	.L1005
	.p2align 4,,10
	.p2align 3
.L998:
	leaq	-60(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movb	$-1, -60(%rbp)
	movq	40(%rbx), %rsi
	cmpq	48(%rbx), %rsi
	jne	.L1087
	.p2align 4,,10
	.p2align 3
.L1000:
	leaq	-60(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movb	$27, -60(%rbp)
	movq	40(%rbx), %rsi
	cmpq	%rsi, 48(%rbx)
	jne	.L1088
	.p2align 4,,10
	.p2align 3
.L1002:
	leaq	-60(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	jmp	.L1003
	.p2align 4,,10
	.p2align 3
.L1083:
	movb	%al, (%rdx)
	movq	40(%rbx), %rax
	movq	48(%rbx), %rsi
	addq	$1, %rax
	movq	%rax, 40(%rbx)
	jmp	.L991
	.p2align 4,,10
	.p2align 3
.L922:
	movl	$2147483647, %edx
	movq	%rdx, %rax
	subq	%r13, %rax
	cmpq	$3, %rax
	jbe	.L945
	cmpq	$4, %r13
	movl	$4, %eax
	cmovnb	%r13, %rax
	addq	%r13, %rax
	jc	.L926
	testq	%rax, %rax
	jne	.L1089
	xorl	%edx, %edx
	xorl	%eax, %eax
.L928:
	cmpq	%rcx, %r15
	je	.L1013
	leaq	-1(%r15), %rsi
	subq	%rcx, %rsi
	cmpq	$14, %rsi
	jbe	.L932
	leaq	15(%rcx), %rsi
	subq	%rax, %rsi
	cmpq	$30, %rsi
	jbe	.L932
	movq	%r15, %r9
	xorl	%esi, %esi
	subq	%rcx, %r9
	movq	%r9, %rdi
	andq	$-16, %rdi
	.p2align 4,,10
	.p2align 3
.L933:
	movdqu	(%rcx,%rsi), %xmm1
	movups	%xmm1, (%rax,%rsi)
	addq	$16, %rsi
	cmpq	%rdi, %rsi
	jne	.L933
	movq	%r9, %r8
	andq	$-16, %r8
	leaq	(%rcx,%r8), %rsi
	leaq	(%rax,%r8), %rdi
	cmpq	%r8, %r9
	je	.L936
	movzbl	(%rsi), %r8d
	movb	%r8b, (%rdi)
	leaq	1(%rsi), %r8
	cmpq	%r8, %r15
	je	.L936
	movzbl	1(%rsi), %r8d
	movb	%r8b, 1(%rdi)
	leaq	2(%rsi), %r8
	cmpq	%r8, %r15
	je	.L936
	movzbl	2(%rsi), %r8d
	movb	%r8b, 2(%rdi)
	leaq	3(%rsi), %r8
	cmpq	%r8, %r15
	je	.L936
	movzbl	3(%rsi), %r8d
	movb	%r8b, 3(%rdi)
	leaq	4(%rsi), %r8
	cmpq	%r8, %r15
	je	.L936
	movzbl	4(%rsi), %r8d
	movb	%r8b, 4(%rdi)
	leaq	5(%rsi), %r8
	cmpq	%r8, %r15
	je	.L936
	movzbl	5(%rsi), %r8d
	movb	%r8b, 5(%rdi)
	leaq	6(%rsi), %r8
	cmpq	%r8, %r15
	je	.L936
	movzbl	6(%rsi), %r8d
	movb	%r8b, 6(%rdi)
	leaq	7(%rsi), %r8
	cmpq	%r8, %r15
	je	.L936
	movzbl	7(%rsi), %r8d
	movb	%r8b, 7(%rdi)
	leaq	8(%rsi), %r8
	cmpq	%r8, %r15
	je	.L936
	movzbl	8(%rsi), %r8d
	movb	%r8b, 8(%rdi)
	leaq	9(%rsi), %r8
	cmpq	%r8, %r15
	je	.L936
	movzbl	9(%rsi), %r8d
	movb	%r8b, 9(%rdi)
	leaq	10(%rsi), %r8
	cmpq	%r8, %r15
	je	.L936
	movzbl	10(%rsi), %r8d
	movb	%r8b, 10(%rdi)
	leaq	11(%rsi), %r8
	cmpq	%r8, %r15
	je	.L936
	movzbl	11(%rsi), %r8d
	movb	%r8b, 11(%rdi)
	leaq	12(%rsi), %r8
	cmpq	%r8, %r15
	je	.L936
	movzbl	12(%rsi), %r8d
	movb	%r8b, 12(%rdi)
	leaq	13(%rsi), %r8
	cmpq	%r8, %r15
	je	.L936
	movzbl	13(%rsi), %r8d
	movb	%r8b, 13(%rdi)
	leaq	14(%rsi), %r8
	cmpq	%r8, %r15
	je	.L936
	movzbl	14(%rsi), %esi
	movb	%sil, 14(%rdi)
	.p2align 4,,10
	.p2align 3
.L936:
	movq	%r15, %rdi
	subq	%rcx, %rdi
	addq	%rax, %rdi
.L931:
	movl	-60(%rbp), %ecx
	leaq	4(%rdi), %r12
	movl	%ecx, (%rdi)
	movq	40(%rbx), %rsi
	cmpq	%rsi, %r15
	je	.L937
	leaq	16(%r15), %rcx
	movq	%rsi, %r9
	cmpq	%rcx, %r12
	leaq	20(%rdi), %rcx
	setnb	%r8b
	cmpq	%rcx, %r15
	setnb	%cl
	subq	%r15, %r9
	orb	%cl, %r8b
	je	.L938
	leaq	-1(%rsi), %rcx
	subq	%r15, %rcx
	cmpq	$14, %rcx
	jbe	.L938
	movq	%r9, %r8
	xorl	%ecx, %ecx
	andq	$-16, %r8
	.p2align 4,,10
	.p2align 3
.L939:
	movdqu	(%r15,%rcx), %xmm2
	movups	%xmm2, 4(%rdi,%rcx)
	addq	$16, %rcx
	cmpq	%r8, %rcx
	jne	.L939
	movq	%r9, %r8
	andq	$-16, %r8
	leaq	(%r15,%r8), %rcx
	leaq	(%r12,%r8), %rdi
	cmpq	%r8, %r9
	je	.L942
	movzbl	(%rcx), %r8d
	movb	%r8b, (%rdi)
	leaq	1(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L942
	movzbl	1(%rcx), %r8d
	movb	%r8b, 1(%rdi)
	leaq	2(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L942
	movzbl	2(%rcx), %r8d
	movb	%r8b, 2(%rdi)
	leaq	3(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L942
	movzbl	3(%rcx), %r8d
	movb	%r8b, 3(%rdi)
	leaq	4(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L942
	movzbl	4(%rcx), %r8d
	movb	%r8b, 4(%rdi)
	leaq	5(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L942
	movzbl	5(%rcx), %r8d
	movb	%r8b, 5(%rdi)
	leaq	6(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L942
	movzbl	6(%rcx), %r8d
	movb	%r8b, 6(%rdi)
	leaq	7(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L942
	movzbl	7(%rcx), %r8d
	movb	%r8b, 7(%rdi)
	leaq	8(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L942
	movzbl	8(%rcx), %r8d
	movb	%r8b, 8(%rdi)
	leaq	9(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L942
	movzbl	9(%rcx), %r8d
	movb	%r8b, 9(%rdi)
	leaq	10(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L942
	movzbl	10(%rcx), %r8d
	movb	%r8b, 10(%rdi)
	leaq	11(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L942
	movzbl	11(%rcx), %r8d
	movb	%r8b, 11(%rdi)
	leaq	12(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L942
	movzbl	12(%rcx), %r8d
	movb	%r8b, 12(%rdi)
	leaq	13(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L942
	movzbl	13(%rcx), %r8d
	movb	%r8b, 13(%rdi)
	leaq	14(%rcx), %r8
	cmpq	%r8, %rsi
	je	.L942
	movzbl	14(%rcx), %ecx
	movb	%cl, 14(%rdi)
	.p2align 4,,10
	.p2align 3
.L942:
	subq	%r15, %rsi
	addq	%rsi, %r12
.L937:
	movq	%rax, %xmm0
	movq	%r12, %xmm7
	movq	%rdx, 48(%rbx)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 32(%rbx)
	jmp	.L923
	.p2align 4,,10
	.p2align 3
.L943:
	movl	$2147483647, %esi
	movq	%rsi, %rdx
	subq	%r15, %rdx
	cmpq	$3, %rdx
	jbe	.L945
	cmpq	$4, %r15
	movl	$4, %edx
	cmovnb	%r15, %rdx
	addq	%r15, %rdx
	jc	.L947
	testq	%rdx, %rdx
	jne	.L1090
	xorl	%esi, %esi
	xorl	%edi, %edi
.L949:
	cmpq	%r12, %rax
	je	.L1016
	leaq	15(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$30, %rdx
	jbe	.L953
	leaq	-1(%r12), %rdx
	subq	%rax, %rdx
	cmpq	$14, %rdx
	jbe	.L953
	movq	%r12, %r9
	xorl	%edx, %edx
	subq	%rax, %r9
	movq	%r9, %rcx
	andq	$-16, %rcx
	.p2align 4,,10
	.p2align 3
.L954:
	movdqu	(%rax,%rdx), %xmm3
	movups	%xmm3, (%rdi,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L954
	movq	%r9, %r8
	andq	$-16, %r8
	leaq	(%rax,%r8), %rdx
	leaq	(%rdi,%r8), %rcx
	cmpq	%r8, %r9
	je	.L957
	movzbl	(%rdx), %r8d
	movb	%r8b, (%rcx)
	leaq	1(%rdx), %r8
	cmpq	%r8, %r12
	je	.L957
	movzbl	1(%rdx), %r8d
	movb	%r8b, 1(%rcx)
	leaq	2(%rdx), %r8
	cmpq	%r8, %r12
	je	.L957
	movzbl	2(%rdx), %r8d
	movb	%r8b, 2(%rcx)
	leaq	3(%rdx), %r8
	cmpq	%r8, %r12
	je	.L957
	movzbl	3(%rdx), %r8d
	movb	%r8b, 3(%rcx)
	leaq	4(%rdx), %r8
	cmpq	%r8, %r12
	je	.L957
	movzbl	4(%rdx), %r8d
	movb	%r8b, 4(%rcx)
	leaq	5(%rdx), %r8
	cmpq	%r8, %r12
	je	.L957
	movzbl	5(%rdx), %r8d
	movb	%r8b, 5(%rcx)
	leaq	6(%rdx), %r8
	cmpq	%r8, %r12
	je	.L957
	movzbl	6(%rdx), %r8d
	movb	%r8b, 6(%rcx)
	leaq	7(%rdx), %r8
	cmpq	%r8, %r12
	je	.L957
	movzbl	7(%rdx), %r8d
	movb	%r8b, 7(%rcx)
	leaq	8(%rdx), %r8
	cmpq	%r8, %r12
	je	.L957
	movzbl	8(%rdx), %r8d
	movb	%r8b, 8(%rcx)
	leaq	9(%rdx), %r8
	cmpq	%r8, %r12
	je	.L957
	movzbl	9(%rdx), %r8d
	movb	%r8b, 9(%rcx)
	leaq	10(%rdx), %r8
	cmpq	%r8, %r12
	je	.L957
	movzbl	10(%rdx), %r8d
	movb	%r8b, 10(%rcx)
	leaq	11(%rdx), %r8
	cmpq	%r8, %r12
	je	.L957
	movzbl	11(%rdx), %r8d
	movb	%r8b, 11(%rcx)
	leaq	12(%rdx), %r8
	cmpq	%r8, %r12
	je	.L957
	movzbl	12(%rdx), %r8d
	movb	%r8b, 12(%rcx)
	leaq	13(%rdx), %r8
	cmpq	%r8, %r12
	je	.L957
	movzbl	13(%rdx), %r8d
	movb	%r8b, 13(%rcx)
	leaq	14(%rdx), %r8
	cmpq	%r8, %r12
	je	.L957
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rcx)
	.p2align 4,,10
	.p2align 3
.L957:
	movq	%r12, %r8
	subq	%rax, %r8
	addq	%rdi, %r8
.L952:
	movl	-64(%rbp), %eax
	leaq	4(%r8), %rdx
	movl	%eax, (%r8)
	movq	40(%rbx), %rax
	cmpq	%r12, %rax
	je	.L958
	leaq	20(%r8), %rcx
	cmpq	%rcx, %r12
	leaq	16(%r12), %rcx
	setnb	%r9b
	cmpq	%rcx, %rdx
	setnb	%cl
	orb	%cl, %r9b
	je	.L959
	leaq	-1(%rax), %rcx
	subq	%r12, %rcx
	cmpq	$14, %rcx
	jbe	.L959
	movq	%rax, %r9
	xorl	%ecx, %ecx
	subq	%r12, %r9
	movq	%r9, %r10
	andq	$-16, %r10
	.p2align 4,,10
	.p2align 3
.L960:
	movdqu	(%r12,%rcx), %xmm4
	movups	%xmm4, 4(%r8,%rcx)
	addq	$16, %rcx
	cmpq	%r10, %rcx
	jne	.L960
	movq	%r9, %r10
	andq	$-16, %r10
	leaq	(%r12,%r10), %rcx
	leaq	(%rdx,%r10), %r8
	cmpq	%r9, %r10
	je	.L963
	movzbl	(%rcx), %r9d
	movb	%r9b, (%r8)
	leaq	1(%rcx), %r9
	cmpq	%r9, %rax
	je	.L963
	movzbl	1(%rcx), %r9d
	movb	%r9b, 1(%r8)
	leaq	2(%rcx), %r9
	cmpq	%r9, %rax
	je	.L963
	movzbl	2(%rcx), %r9d
	movb	%r9b, 2(%r8)
	leaq	3(%rcx), %r9
	cmpq	%r9, %rax
	je	.L963
	movzbl	3(%rcx), %r9d
	movb	%r9b, 3(%r8)
	leaq	4(%rcx), %r9
	cmpq	%r9, %rax
	je	.L963
	movzbl	4(%rcx), %r9d
	movb	%r9b, 4(%r8)
	leaq	5(%rcx), %r9
	cmpq	%r9, %rax
	je	.L963
	movzbl	5(%rcx), %r9d
	movb	%r9b, 5(%r8)
	leaq	6(%rcx), %r9
	cmpq	%r9, %rax
	je	.L963
	movzbl	6(%rcx), %r9d
	movb	%r9b, 6(%r8)
	leaq	7(%rcx), %r9
	cmpq	%r9, %rax
	je	.L963
	movzbl	7(%rcx), %r9d
	movb	%r9b, 7(%r8)
	leaq	8(%rcx), %r9
	cmpq	%r9, %rax
	je	.L963
	movzbl	8(%rcx), %r9d
	movb	%r9b, 8(%r8)
	leaq	9(%rcx), %r9
	cmpq	%r9, %rax
	je	.L963
	movzbl	9(%rcx), %r9d
	movb	%r9b, 9(%r8)
	leaq	10(%rcx), %r9
	cmpq	%r9, %rax
	je	.L963
	movzbl	10(%rcx), %r9d
	movb	%r9b, 10(%r8)
	leaq	11(%rcx), %r9
	cmpq	%r9, %rax
	je	.L963
	movzbl	11(%rcx), %r9d
	movb	%r9b, 11(%r8)
	leaq	12(%rcx), %r9
	cmpq	%r9, %rax
	je	.L963
	movzbl	12(%rcx), %r9d
	movb	%r9b, 12(%r8)
	leaq	13(%rcx), %r9
	cmpq	%r9, %rax
	je	.L963
	movzbl	13(%rcx), %r9d
	movb	%r9b, 13(%r8)
	leaq	14(%rcx), %r9
	cmpq	%r9, %rax
	je	.L963
	movzbl	14(%rcx), %ecx
	movb	%cl, 14(%r8)
	.p2align 4,,10
	.p2align 3
.L963:
	subq	%r12, %rax
	addq	%rax, %rdx
.L958:
	movq	%rdi, %xmm0
	movq	%rdx, %xmm7
	movq	%rsi, 48(%rbx)
	punpcklqdq	%xmm7, %xmm0
	movb	$3, -60(%rbp)
	movups	%xmm0, 32(%rbx)
	cmpq	%rdx, %rsi
	jne	.L1091
.L964:
	leaq	-60(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movq	40(%rbx), %r12
	movq	48(%rbx), %rax
	subq	%r12, %rax
	cmpq	$3, %rax
	ja	.L1092
	.p2align 4,,10
	.p2align 3
.L966:
	movq	32(%rbx), %rcx
	movq	%r12, %rsi
	movl	$2147483647, %edi
	movq	%rdi, %rax
	subq	%rcx, %rsi
	subq	%rsi, %rax
	cmpq	$3, %rax
	jbe	.L945
	movl	$4, %edx
	cmpq	$4, %rsi
	movq	%rdx, %rax
	cmovnb	%rsi, %rax
	addq	%rsi, %rax
	jc	.L969
	testq	%rax, %rax
	jne	.L1093
	xorl	%esi, %esi
	xorl	%eax, %eax
.L971:
	cmpq	%rcx, %r12
	je	.L1019
	leaq	15(%rax), %rdx
	movq	%r12, %r8
	subq	%rcx, %rdx
	subq	%rcx, %r8
	cmpq	$30, %rdx
	jbe	.L975
	leaq	-1(%r12), %rdx
	subq	%rcx, %rdx
	cmpq	$14, %rdx
	jbe	.L975
	movq	%r8, %rdi
	xorl	%edx, %edx
	andq	$-16, %rdi
	.p2align 4,,10
	.p2align 3
.L976:
	movdqu	(%rcx,%rdx), %xmm5
	movups	%xmm5, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L976
	movq	%r8, %r9
	andq	$-16, %r9
	leaq	(%rcx,%r9), %rdx
	leaq	(%rax,%r9), %rdi
	cmpq	%r8, %r9
	je	.L979
	movzbl	(%rdx), %r8d
	movb	%r8b, (%rdi)
	leaq	1(%rdx), %r8
	cmpq	%r8, %r12
	je	.L979
	movzbl	1(%rdx), %r8d
	movb	%r8b, 1(%rdi)
	leaq	2(%rdx), %r8
	cmpq	%r8, %r12
	je	.L979
	movzbl	2(%rdx), %r8d
	movb	%r8b, 2(%rdi)
	leaq	3(%rdx), %r8
	cmpq	%r8, %r12
	je	.L979
	movzbl	3(%rdx), %r8d
	movb	%r8b, 3(%rdi)
	leaq	4(%rdx), %r8
	cmpq	%r8, %r12
	je	.L979
	movzbl	4(%rdx), %r8d
	movb	%r8b, 4(%rdi)
	leaq	5(%rdx), %r8
	cmpq	%r8, %r12
	je	.L979
	movzbl	5(%rdx), %r8d
	movb	%r8b, 5(%rdi)
	leaq	6(%rdx), %r8
	cmpq	%r8, %r12
	je	.L979
	movzbl	6(%rdx), %r8d
	movb	%r8b, 6(%rdi)
	leaq	7(%rdx), %r8
	cmpq	%r8, %r12
	je	.L979
	movzbl	7(%rdx), %r8d
	movb	%r8b, 7(%rdi)
	leaq	8(%rdx), %r8
	cmpq	%r8, %r12
	je	.L979
	movzbl	8(%rdx), %r8d
	movb	%r8b, 8(%rdi)
	leaq	9(%rdx), %r8
	cmpq	%r8, %r12
	je	.L979
	movzbl	9(%rdx), %r8d
	movb	%r8b, 9(%rdi)
	leaq	10(%rdx), %r8
	cmpq	%r8, %r12
	je	.L979
	movzbl	10(%rdx), %r8d
	movb	%r8b, 10(%rdi)
	leaq	11(%rdx), %r8
	cmpq	%r8, %r12
	je	.L979
	movzbl	11(%rdx), %r8d
	movb	%r8b, 11(%rdi)
	leaq	12(%rdx), %r8
	cmpq	%r8, %r12
	je	.L979
	movzbl	12(%rdx), %r8d
	movb	%r8b, 12(%rdi)
	leaq	13(%rdx), %r8
	cmpq	%r8, %r12
	je	.L979
	movzbl	13(%rdx), %r8d
	movb	%r8b, 13(%rdi)
	leaq	14(%rdx), %r8
	cmpq	%r8, %r12
	je	.L979
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rdi)
	.p2align 4,,10
	.p2align 3
.L979:
	movq	%r12, %r8
	subq	%rcx, %r8
	addq	%rax, %r8
.L974:
	movl	$5393530, (%r8)
	movq	40(%rbx), %rdi
	leaq	4(%r8), %rdx
	cmpq	%r12, %rdi
	je	.L980
	leaq	16(%r12), %rcx
	cmpq	%rcx, %rdx
	leaq	20(%r8), %rcx
	setnb	%r9b
	cmpq	%rcx, %r12
	setnb	%cl
	orb	%cl, %r9b
	je	.L981
	leaq	-1(%rdi), %rcx
	subq	%r12, %rcx
	cmpq	$14, %rcx
	jbe	.L981
	movq	%rdi, %r9
	xorl	%ecx, %ecx
	subq	%r12, %r9
	movq	%r9, %r10
	andq	$-16, %r10
	.p2align 4,,10
	.p2align 3
.L982:
	movdqu	(%r12,%rcx), %xmm6
	movups	%xmm6, 4(%r8,%rcx)
	addq	$16, %rcx
	cmpq	%r10, %rcx
	jne	.L982
	movq	%r9, %r10
	andq	$-16, %r10
	leaq	(%r12,%r10), %rcx
	leaq	(%rdx,%r10), %r8
	cmpq	%r10, %r9
	je	.L985
	movzbl	(%rcx), %r9d
	movb	%r9b, (%r8)
	leaq	1(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L985
	movzbl	1(%rcx), %r9d
	movb	%r9b, 1(%r8)
	leaq	2(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L985
	movzbl	2(%rcx), %r9d
	movb	%r9b, 2(%r8)
	leaq	3(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L985
	movzbl	3(%rcx), %r9d
	movb	%r9b, 3(%r8)
	leaq	4(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L985
	movzbl	4(%rcx), %r9d
	movb	%r9b, 4(%r8)
	leaq	5(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L985
	movzbl	5(%rcx), %r9d
	movb	%r9b, 5(%r8)
	leaq	6(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L985
	movzbl	6(%rcx), %r9d
	movb	%r9b, 6(%r8)
	leaq	7(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L985
	movzbl	7(%rcx), %r9d
	movb	%r9b, 7(%r8)
	leaq	8(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L985
	movzbl	8(%rcx), %r9d
	movb	%r9b, 8(%r8)
	leaq	9(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L985
	movzbl	9(%rcx), %r9d
	movb	%r9b, 9(%r8)
	leaq	10(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L985
	movzbl	10(%rcx), %r9d
	movb	%r9b, 10(%r8)
	leaq	11(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L985
	movzbl	11(%rcx), %r9d
	movb	%r9b, 11(%r8)
	leaq	12(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L985
	movzbl	12(%rcx), %r9d
	movb	%r9b, 12(%r8)
	leaq	13(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L985
	movzbl	13(%rcx), %r9d
	movb	%r9b, 13(%r8)
	leaq	14(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L985
	movzbl	14(%rcx), %ecx
	movb	%cl, 14(%r8)
	.p2align 4,,10
	.p2align 3
.L985:
	subq	%r12, %rdi
	addq	%rdi, %rdx
.L980:
	movq	%rax, %xmm0
	movq	%rdx, %xmm7
	movq	%rsi, 48(%rbx)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 32(%rbx)
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L1085:
	leaq	-60(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	jmp	.L1004
	.p2align 4,,10
	.p2align 3
.L953:
	movq	%r12, %r8
	xorl	%edx, %edx
	subq	%rax, %r8
	.p2align 4,,10
	.p2align 3
.L956:
	movzbl	(%rax,%rdx), %ecx
	movb	%cl, (%rdi,%rdx)
	addq	$1, %rdx
	cmpq	%r8, %rdx
	jne	.L956
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L959:
	movq	%rax, %r10
	xorl	%ecx, %ecx
	subq	%r12, %r10
	.p2align 4,,10
	.p2align 3
.L962:
	movzbl	(%r12,%rcx), %r9d
	movb	%r9b, 4(%r8,%rcx)
	addq	$1, %rcx
	cmpq	%r10, %rcx
	jne	.L962
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L938:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L941:
	movzbl	(%r15,%rcx), %r8d
	movb	%r8b, 4(%rdi,%rcx)
	addq	$1, %rcx
	cmpq	%r9, %rcx
	jne	.L941
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L932:
	movq	%r15, %r8
	xorl	%esi, %esi
	subq	%rcx, %r8
	.p2align 4,,10
	.p2align 3
.L935:
	movzbl	(%rcx,%rsi), %edi
	movb	%dil, (%rax,%rsi)
	addq	$1, %rsi
	cmpq	%r8, %rsi
	jne	.L935
	jmp	.L936
	.p2align 4,,10
	.p2align 3
.L981:
	movq	%rdi, %r10
	xorl	%ecx, %ecx
	subq	%r12, %r10
	.p2align 4,,10
	.p2align 3
.L984:
	movzbl	(%r12,%rcx), %r9d
	movb	%r9b, 4(%r8,%rcx)
	addq	$1, %rcx
	cmpq	%rcx, %r10
	jne	.L984
	jmp	.L985
	.p2align 4,,10
	.p2align 3
.L975:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L978:
	movzbl	(%rcx,%rdx), %edi
	movb	%dil, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%r8, %rdx
	jne	.L978
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L1081:
	movq	%r8, %rdx
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movq	40(%rbx), %rdx
	movq	48(%rbx), %rsi
	movq	-72(%rbp), %r8
	jmp	.L992
	.p2align 4,,10
	.p2align 3
.L1013:
	movq	%rax, %rdi
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L1019:
	movq	%rax, %r8
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L1016:
	movq	%rdi, %r8
	jmp	.L952
.L1093:
	cmpq	$2147483647, %rax
	cmovbe	%rax, %rdi
	leaq	7(%rdi), %rsi
	movq	%rdi, %rdx
	andq	$-8, %rsi
.L970:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L1094
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L973:
	movq	32(%rbx), %rcx
	leaq	(%rax,%rdx), %rsi
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L1094:
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	jmp	.L973
.L1090:
	cmpq	$2147483647, %rdx
	cmova	%rsi, %rdx
	leaq	7(%rdx), %rsi
	andq	$-8, %rsi
.L948:
	movq	24(%rbx), %r8
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1095
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L951:
	movq	32(%rbx), %rax
	leaq	(%rdi,%rdx), %rsi
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L1095:
	movq	%r8, %rdi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L951
.L1089:
	cmpq	$2147483647, %rax
	cmovbe	%rax, %rdx
	leaq	7(%rdx), %rsi
	movq	%rdx, %r12
	andq	$-8, %rsi
.L927:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rsi, %rdx
	jb	.L1096
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L930:
	movq	32(%rbx), %rcx
	leaq	(%rax,%r12), %rdx
	jmp	.L928
	.p2align 4,,10
	.p2align 3
.L1096:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L930
.L1086:
	call	__stack_chk_fail@PLT
.L945:
	leaq	.LC21(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L926:
	movl	$2147483648, %esi
	movl	$2147483647, %r12d
	jmp	.L927
.L947:
	movl	$2147483648, %esi
	movl	$2147483647, %edx
	jmp	.L948
.L969:
	movl	$2147483648, %esi
	movl	$2147483647, %edx
	jmp	.L970
	.cfi_endproc
.LFE5872:
	.size	_ZN2v88internal13EhFrameWriter8WriteCieEv, .-_ZN2v88internal13EhFrameWriter8WriteCieEv
	.section	.text._ZN2v88internal13EhFrameWriter10InitializeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13EhFrameWriter10InitializeEv
	.type	_ZN2v88internal13EhFrameWriter10InitializeEv, @function
_ZN2v88internal13EhFrameWriter10InitializeEv:
.LFB5871:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	32(%rdi), %rbx
	movq	48(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$127, %rax
	jbe	.L1116
.L1098:
	movl	$1, 8(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal13EhFrameWriter8WriteCieEv
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13EhFrameWriter14WriteFdeHeaderEv
	.p2align 4,,10
	.p2align 3
.L1116:
	.cfi_restore_state
	movq	40(%rdi), %r13
	movq	24(%rdi), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	movq	%r13, %r14
	subq	%rbx, %r14
	subq	%rax, %rdx
	cmpq	$127, %rdx
	jbe	.L1117
	leaq	128(%rax), %rcx
	movq	%rcx, 16(%rdi)
.L1100:
	cmpq	%r13, %rbx
	je	.L1101
	leaq	-1(%r13), %rdx
	subq	%rbx, %rdx
	cmpq	$14, %rdx
	jbe	.L1102
	leaq	15(%rbx), %rdx
	subq	%rax, %rdx
	cmpq	$30, %rdx
	jbe	.L1102
	movq	%r13, %r8
	movq	%rbx, %rdi
	movq	%rax, %rdx
	subq	%rbx, %r8
	subq	%rax, %rdi
	movq	%r8, %rsi
	andq	$-16, %rsi
	addq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L1103:
	movdqu	(%rdx,%rdi), %xmm0
	addq	$16, %rdx
	movups	%xmm0, -16(%rdx)
	cmpq	%rsi, %rdx
	jne	.L1103
	movq	%r8, %rsi
	andq	$-16, %rsi
	addq	%rsi, %rbx
	leaq	(%rax,%rsi), %rdx
	cmpq	%rsi, %r8
	je	.L1101
	movzbl	(%rbx), %esi
	movb	%sil, (%rdx)
	leaq	1(%rbx), %rsi
	cmpq	%rsi, %r13
	je	.L1101
	movzbl	1(%rbx), %esi
	movb	%sil, 1(%rdx)
	leaq	2(%rbx), %rsi
	cmpq	%rsi, %r13
	je	.L1101
	movzbl	2(%rbx), %esi
	movb	%sil, 2(%rdx)
	leaq	3(%rbx), %rsi
	cmpq	%rsi, %r13
	je	.L1101
	movzbl	3(%rbx), %esi
	movb	%sil, 3(%rdx)
	leaq	4(%rbx), %rsi
	cmpq	%rsi, %r13
	je	.L1101
	movzbl	4(%rbx), %esi
	movb	%sil, 4(%rdx)
	leaq	5(%rbx), %rsi
	cmpq	%rsi, %r13
	je	.L1101
	movzbl	5(%rbx), %esi
	movb	%sil, 5(%rdx)
	leaq	6(%rbx), %rsi
	cmpq	%rsi, %r13
	je	.L1101
	movzbl	6(%rbx), %esi
	movb	%sil, 6(%rdx)
	leaq	7(%rbx), %rsi
	cmpq	%rsi, %r13
	je	.L1101
	movzbl	7(%rbx), %esi
	movb	%sil, 7(%rdx)
	leaq	8(%rbx), %rsi
	cmpq	%rsi, %r13
	je	.L1101
	movzbl	8(%rbx), %esi
	movb	%sil, 8(%rdx)
	leaq	9(%rbx), %rsi
	cmpq	%rsi, %r13
	je	.L1101
	movzbl	9(%rbx), %esi
	movb	%sil, 9(%rdx)
	leaq	10(%rbx), %rsi
	cmpq	%rsi, %r13
	je	.L1101
	movzbl	10(%rbx), %esi
	movb	%sil, 10(%rdx)
	leaq	11(%rbx), %rsi
	cmpq	%rsi, %r13
	je	.L1101
	movzbl	11(%rbx), %esi
	movb	%sil, 11(%rdx)
	leaq	12(%rbx), %rsi
	cmpq	%rsi, %r13
	je	.L1101
	movzbl	12(%rbx), %esi
	movb	%sil, 12(%rdx)
	leaq	13(%rbx), %rsi
	cmpq	%rsi, %r13
	je	.L1101
	movzbl	13(%rbx), %esi
	movb	%sil, 13(%rdx)
	leaq	14(%rbx), %rsi
	cmpq	%rsi, %r13
	je	.L1101
	movzbl	14(%rbx), %esi
	movb	%sil, 14(%rdx)
	.p2align 4,,10
	.p2align 3
.L1101:
	movq	%rax, 32(%r12)
	addq	%r14, %rax
	movq	%rax, 40(%r12)
	movq	%rcx, 48(%r12)
	jmp	.L1098
	.p2align 4,,10
	.p2align 3
.L1102:
	subq	%rbx, %r13
	movq	%rax, %rdx
	subq	%rax, %rbx
	addq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L1105:
	movzbl	(%rdx,%rbx), %esi
	addq	$1, %rdx
	movb	%sil, -1(%rdx)
	cmpq	%r13, %rdx
	jne	.L1105
	jmp	.L1101
	.p2align 4,,10
	.p2align 3
.L1117:
	movl	$128, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	leaq	128(%rax), %rcx
	jmp	.L1100
	.cfi_endproc
.LFE5871:
	.size	_ZN2v88internal13EhFrameWriter10InitializeEv, .-_ZN2v88internal13EhFrameWriter10InitializeEv
	.section	.text._ZN2v88internal13EhFrameWriter6FinishEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13EhFrameWriter6FinishEi
	.type	_ZN2v88internal13EhFrameWriter6FinishEi, @function
_ZN2v88internal13EhFrameWriter6FinishEi:
.LFB5883:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	40(%rdi), %rsi
	movq	%rdi, %rbx
	subq	32(%rdi), %rsi
	subl	(%rdi), %esi
	subl	$4, %esi
	call	_ZN2v88internal13EhFrameWriter25WritePaddingToAlignedSizeEi
	movq	32(%rbx), %rdx
	movq	40(%rbx), %rax
	movslq	(%rbx), %rsi
	subq	%rdx, %rax
	subl	%esi, %eax
	subl	$4, %eax
	movl	%eax, (%rdx,%rsi)
	movl	(%rbx), %eax
	movq	32(%rbx), %rsi
	leal	8(%rax), %edx
	leal	7(%r13), %eax
	andl	$-8, %eax
	movslq	%edx, %rcx
	addl	%edx, %eax
	negl	%eax
	movl	%eax, (%rsi,%rcx)
	movl	(%rbx), %eax
	movq	32(%rbx), %rdx
	addl	$12, %eax
	cltq
	movl	%r13d, (%rdx,%rax)
	movq	40(%rbx), %r12
	movq	48(%rbx), %rax
	subq	%r12, %rax
	cmpq	$3, %rax
	jbe	.L1119
	movl	_ZZN2v88internal13EhFrameWriter6FinishEiE11kTerminator(%rip), %eax
	movl	%eax, (%r12)
	addq	$4, 40(%rbx)
.L1120:
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal13EhFrameWriter15WriteEhFrameHdrEi
	movl	$2, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1119:
	.cfi_restore_state
	movq	32(%rbx), %rcx
	movq	%r12, %rax
	movl	$2147483647, %edi
	movq	%rdi, %rdx
	subq	%rcx, %rax
	subq	%rax, %rdx
	cmpq	$3, %rdx
	jbe	.L1161
	cmpq	$4, %rax
	movl	$4, %edx
	cmovnb	%rax, %rdx
	addq	%rdx, %rax
	jc	.L1123
	testq	%rax, %rax
	jne	.L1162
	xorl	%edi, %edi
	xorl	%eax, %eax
.L1125:
	cmpq	%rcx, %r12
	je	.L1142
	leaq	-1(%r12), %rdx
	subq	%rcx, %rdx
	cmpq	$14, %rdx
	jbe	.L1129
	leaq	15(%rax), %rdx
	subq	%rcx, %rdx
	cmpq	$30, %rdx
	jbe	.L1129
	movq	%r12, %r9
	xorl	%edx, %edx
	subq	%rcx, %r9
	movq	%r9, %rsi
	andq	$-16, %rsi
	.p2align 4,,10
	.p2align 3
.L1130:
	movdqu	(%rcx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L1130
	movq	%r9, %r8
	andq	$-16, %r8
	leaq	(%rcx,%r8), %rdx
	leaq	(%rax,%r8), %rsi
	cmpq	%r8, %r9
	je	.L1133
	movzbl	(%rdx), %r8d
	movb	%r8b, (%rsi)
	leaq	1(%rdx), %r8
	cmpq	%r8, %r12
	je	.L1133
	movzbl	1(%rdx), %r8d
	movb	%r8b, 1(%rsi)
	leaq	2(%rdx), %r8
	cmpq	%r8, %r12
	je	.L1133
	movzbl	2(%rdx), %r8d
	movb	%r8b, 2(%rsi)
	leaq	3(%rdx), %r8
	cmpq	%r8, %r12
	je	.L1133
	movzbl	3(%rdx), %r8d
	movb	%r8b, 3(%rsi)
	leaq	4(%rdx), %r8
	cmpq	%r8, %r12
	je	.L1133
	movzbl	4(%rdx), %r8d
	movb	%r8b, 4(%rsi)
	leaq	5(%rdx), %r8
	cmpq	%r8, %r12
	je	.L1133
	movzbl	5(%rdx), %r8d
	movb	%r8b, 5(%rsi)
	leaq	6(%rdx), %r8
	cmpq	%r8, %r12
	je	.L1133
	movzbl	6(%rdx), %r8d
	movb	%r8b, 6(%rsi)
	leaq	7(%rdx), %r8
	cmpq	%r8, %r12
	je	.L1133
	movzbl	7(%rdx), %r8d
	movb	%r8b, 7(%rsi)
	leaq	8(%rdx), %r8
	cmpq	%r8, %r12
	je	.L1133
	movzbl	8(%rdx), %r8d
	movb	%r8b, 8(%rsi)
	leaq	9(%rdx), %r8
	cmpq	%r8, %r12
	je	.L1133
	movzbl	9(%rdx), %r8d
	movb	%r8b, 9(%rsi)
	leaq	10(%rdx), %r8
	cmpq	%r8, %r12
	je	.L1133
	movzbl	10(%rdx), %r8d
	movb	%r8b, 10(%rsi)
	leaq	11(%rdx), %r8
	cmpq	%r8, %r12
	je	.L1133
	movzbl	11(%rdx), %r8d
	movb	%r8b, 11(%rsi)
	leaq	12(%rdx), %r8
	cmpq	%r8, %r12
	je	.L1133
	movzbl	12(%rdx), %r8d
	movb	%r8b, 12(%rsi)
	leaq	13(%rdx), %r8
	cmpq	%r8, %r12
	je	.L1133
	movzbl	13(%rdx), %r8d
	movb	%r8b, 13(%rsi)
	leaq	14(%rdx), %r8
	cmpq	%r8, %r12
	je	.L1133
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rsi)
	.p2align 4,,10
	.p2align 3
.L1133:
	movq	%r12, %rsi
	subq	%rcx, %rsi
	addq	%rax, %rsi
.L1128:
	movl	_ZZN2v88internal13EhFrameWriter6FinishEiE11kTerminator(%rip), %edx
	leaq	4(%rsi), %r8
	movl	%edx, (%rsi)
	movq	40(%rbx), %rcx
	cmpq	%rcx, %r12
	je	.L1134
	leaq	16(%r12), %rdx
	movq	%rcx, %r10
	cmpq	%rdx, %r8
	leaq	20(%rsi), %rdx
	setnb	%r9b
	cmpq	%rdx, %r12
	setnb	%dl
	subq	%r12, %r10
	orb	%dl, %r9b
	je	.L1135
	leaq	-1(%rcx), %rdx
	subq	%r12, %rdx
	cmpq	$14, %rdx
	jbe	.L1135
	movq	%r10, %r9
	xorl	%edx, %edx
	andq	$-16, %r9
	.p2align 4,,10
	.p2align 3
.L1136:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, 4(%rsi,%rdx)
	addq	$16, %rdx
	cmpq	%r9, %rdx
	jne	.L1136
	movq	%r10, %r9
	andq	$-16, %r9
	leaq	(%r12,%r9), %rdx
	leaq	(%r8,%r9), %rsi
	cmpq	%r9, %r10
	je	.L1139
	movzbl	(%rdx), %r9d
	movb	%r9b, (%rsi)
	leaq	1(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L1139
	movzbl	1(%rdx), %r9d
	movb	%r9b, 1(%rsi)
	leaq	2(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L1139
	movzbl	2(%rdx), %r9d
	movb	%r9b, 2(%rsi)
	leaq	3(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L1139
	movzbl	3(%rdx), %r9d
	movb	%r9b, 3(%rsi)
	leaq	4(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L1139
	movzbl	4(%rdx), %r9d
	movb	%r9b, 4(%rsi)
	leaq	5(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L1139
	movzbl	5(%rdx), %r9d
	movb	%r9b, 5(%rsi)
	leaq	6(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L1139
	movzbl	6(%rdx), %r9d
	movb	%r9b, 6(%rsi)
	leaq	7(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L1139
	movzbl	7(%rdx), %r9d
	movb	%r9b, 7(%rsi)
	leaq	8(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L1139
	movzbl	8(%rdx), %r9d
	movb	%r9b, 8(%rsi)
	leaq	9(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L1139
	movzbl	9(%rdx), %r9d
	movb	%r9b, 9(%rsi)
	leaq	10(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L1139
	movzbl	10(%rdx), %r9d
	movb	%r9b, 10(%rsi)
	leaq	11(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L1139
	movzbl	11(%rdx), %r9d
	movb	%r9b, 11(%rsi)
	leaq	12(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L1139
	movzbl	12(%rdx), %r9d
	movb	%r9b, 12(%rsi)
	leaq	13(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L1139
	movzbl	13(%rdx), %r9d
	movb	%r9b, 13(%rsi)
	leaq	14(%rdx), %r9
	cmpq	%r9, %rcx
	je	.L1139
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rsi)
	.p2align 4,,10
	.p2align 3
.L1139:
	subq	%r12, %rcx
	addq	%rcx, %r8
.L1134:
	movq	%rax, %xmm0
	movq	%r8, %xmm3
	movq	%rdi, 48(%rbx)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 32(%rbx)
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L1135:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1138:
	movzbl	(%r12,%rdx), %r9d
	movb	%r9b, 4(%rsi,%rdx)
	addq	$1, %rdx
	cmpq	%r10, %rdx
	jne	.L1138
	jmp	.L1139
	.p2align 4,,10
	.p2align 3
.L1129:
	movq	%r12, %r8
	xorl	%edx, %edx
	subq	%rcx, %r8
	.p2align 4,,10
	.p2align 3
.L1132:
	movzbl	(%rcx,%rdx), %esi
	movb	%sil, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%r8, %rdx
	jne	.L1132
	jmp	.L1133
	.p2align 4,,10
	.p2align 3
.L1142:
	movq	%rax, %rsi
	jmp	.L1128
.L1162:
	cmpq	$2147483647, %rax
	cmovbe	%rax, %rdi
	leaq	7(%rdi), %rsi
	movq	%rdi, %r14
	andq	$-8, %rsi
.L1124:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1163
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1127:
	movq	32(%rbx), %rcx
	leaq	(%rax,%r14), %rdi
	jmp	.L1125
	.p2align 4,,10
	.p2align 3
.L1163:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1127
.L1123:
	movl	$2147483648, %esi
	movl	$2147483647, %r14d
	jmp	.L1124
.L1161:
	leaq	.LC21(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5883:
	.size	_ZN2v88internal13EhFrameWriter6FinishEi, .-_ZN2v88internal13EhFrameWriter6FinishEi
	.section	.rodata._ZZN2v88internal13EhFrameWriter6FinishEiE11kTerminator,"a"
	.type	_ZZN2v88internal13EhFrameWriter6FinishEiE11kTerminator, @object
	.size	_ZZN2v88internal13EhFrameWriter6FinishEiE11kTerminator, 4
_ZZN2v88internal13EhFrameWriter6FinishEiE11kTerminator:
	.zero	4
	.section	.bss._ZGVZN2v88internal13EhFrameWriter25WritePaddingToAlignedSizeEiE8kPadding,"aw",@nobits
	.align 8
	.type	_ZGVZN2v88internal13EhFrameWriter25WritePaddingToAlignedSizeEiE8kPadding, @object
	.size	_ZGVZN2v88internal13EhFrameWriter25WritePaddingToAlignedSizeEiE8kPadding, 8
_ZGVZN2v88internal13EhFrameWriter25WritePaddingToAlignedSizeEiE8kPadding:
	.zero	8
	.section	.bss._ZZN2v88internal13EhFrameWriter25WritePaddingToAlignedSizeEiE8kPadding,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal13EhFrameWriter25WritePaddingToAlignedSizeEiE8kPadding, @object
	.size	_ZZN2v88internal13EhFrameWriter25WritePaddingToAlignedSizeEiE8kPadding, 8
_ZZN2v88internal13EhFrameWriter25WritePaddingToAlignedSizeEiE8kPadding:
	.zero	8
	.globl	_ZN2v88internal13EhFrameWriter17kInt32PlaceholderE
	.section	.rodata._ZN2v88internal13EhFrameWriter17kInt32PlaceholderE,"a"
	.align 4
	.type	_ZN2v88internal13EhFrameWriter17kInt32PlaceholderE, @object
	.size	_ZN2v88internal13EhFrameWriter17kInt32PlaceholderE, 4
_ZN2v88internal13EhFrameWriter17kInt32PlaceholderE:
	.long	-559038242
	.globl	_ZN2v88internal16EhFrameConstants15kEhFrameHdrSizeE
	.section	.rodata._ZN2v88internal16EhFrameConstants15kEhFrameHdrSizeE,"a"
	.align 4
	.type	_ZN2v88internal16EhFrameConstants15kEhFrameHdrSizeE, @object
	.size	_ZN2v88internal16EhFrameConstants15kEhFrameHdrSizeE, 4
_ZN2v88internal16EhFrameConstants15kEhFrameHdrSizeE:
	.long	20
	.globl	_ZN2v88internal16EhFrameConstants18kEhFrameHdrVersionE
	.section	.rodata._ZN2v88internal16EhFrameConstants18kEhFrameHdrVersionE,"a"
	.align 4
	.type	_ZN2v88internal16EhFrameConstants18kEhFrameHdrVersionE, @object
	.size	_ZN2v88internal16EhFrameConstants18kEhFrameHdrVersionE, 4
_ZN2v88internal16EhFrameConstants18kEhFrameHdrVersionE:
	.long	1
	.globl	_ZN2v88internal16EhFrameConstants22kEhFrameTerminatorSizeE
	.section	.rodata._ZN2v88internal16EhFrameConstants22kEhFrameTerminatorSizeE,"a"
	.align 4
	.type	_ZN2v88internal16EhFrameConstants22kEhFrameTerminatorSizeE, @object
	.size	_ZN2v88internal16EhFrameConstants22kEhFrameTerminatorSizeE, 4
_ZN2v88internal16EhFrameConstants22kEhFrameTerminatorSizeE:
	.long	4
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	-1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
