	.file	"wasm-features.cc"
	.text
	.section	.text._ZN2v88internal4wasm17UnionFeaturesIntoEPNS1_12WasmFeaturesERKS2_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm17UnionFeaturesIntoEPNS1_12WasmFeaturesERKS2_
	.type	_ZN2v88internal4wasm17UnionFeaturesIntoEPNS1_12WasmFeaturesERKS2_, @function
_ZN2v88internal4wasm17UnionFeaturesIntoEPNS1_12WasmFeaturesERKS2_:
.LFB8631:
	.cfi_startproc
	endbr64
	movzbl	(%rsi), %eax
	orb	%al, (%rdi)
	movzbl	1(%rsi), %eax
	orb	%al, 1(%rdi)
	movzbl	2(%rsi), %eax
	orb	%al, 2(%rdi)
	movzbl	3(%rsi), %eax
	orb	%al, 3(%rdi)
	movzbl	4(%rsi), %eax
	orb	%al, 4(%rdi)
	movzbl	5(%rsi), %eax
	orb	%al, 5(%rdi)
	movzbl	6(%rsi), %eax
	orb	%al, 6(%rdi)
	movzbl	7(%rsi), %eax
	orb	%al, 7(%rdi)
	movzbl	8(%rsi), %eax
	orb	%al, 8(%rdi)
	movzbl	9(%rsi), %eax
	orb	%al, 9(%rdi)
	movzbl	10(%rsi), %eax
	orb	%al, 10(%rdi)
	movzbl	11(%rsi), %eax
	orb	%al, 11(%rdi)
	ret
	.cfi_endproc
.LFE8631:
	.size	_ZN2v88internal4wasm17UnionFeaturesIntoEPNS1_12WasmFeaturesERKS2_, .-_ZN2v88internal4wasm17UnionFeaturesIntoEPNS1_12WasmFeaturesERKS2_
	.section	.text._ZN2v88internal4wasm21WasmFeaturesFromFlagsEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm21WasmFeaturesFromFlagsEv
	.type	_ZN2v88internal4wasm21WasmFeaturesFromFlagsEv, @function
_ZN2v88internal4wasm21WasmFeaturesFromFlagsEv:
.LFB8632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movzbl	_ZN2v88internal25FLAG_experimental_wasm_mvE(%rip), %eax
	movzbl	_ZN2v88internal38FLAG_experimental_wasm_type_reflectionE(%rip), %edx
	movb	%al, -13(%rbp)
	movzbl	_ZN2v88internal25FLAG_experimental_wasm_ehE(%rip), %eax
	movb	%al, -12(%rbp)
	movzbl	_ZN2v88internal30FLAG_experimental_wasm_threadsE(%rip), %eax
	movb	%al, -11(%rbp)
	movzbl	_ZN2v88internal27FLAG_experimental_wasm_simdE(%rip), %eax
	movb	%al, -10(%rbp)
	movzbl	_ZN2v88internal29FLAG_experimental_wasm_bigintE(%rip), %eax
	movb	%al, -9(%rbp)
	movzbl	_ZN2v88internal34FLAG_experimental_wasm_return_callE(%rip), %eax
	movb	%al, -8(%rbp)
	movzbl	_ZN2v88internal40FLAG_experimental_wasm_compilation_hintsE(%rip), %eax
	movb	%al, -7(%rbp)
	movzbl	_ZN2v88internal29FLAG_experimental_wasm_anyrefE(%rip), %eax
	movb	%al, -6(%rbp)
	movzbl	_ZN2v88internal34FLAG_experimental_wasm_bulk_memoryE(%rip), %eax
	salq	$8, %rax
	orq	%rax, %rdx
	movzbl	_ZN2v88internal42FLAG_experimental_wasm_sat_f2i_conversionsE(%rip), %eax
	salq	$16, %rax
	orq	%rdx, %rax
	movzbl	_ZN2v88internal25FLAG_experimental_wasm_seE(%rip), %edx
	salq	$24, %rdx
	orq	%rdx, %rax
	movzbl	-1(%rbp), %edx
	salq	$32, %rdx
	orq	%rax, %rdx
	movq	-13(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8632:
	.size	_ZN2v88internal4wasm21WasmFeaturesFromFlagsEv, .-_ZN2v88internal4wasm21WasmFeaturesFromFlagsEv
	.section	.text._ZN2v88internal4wasm23WasmFeaturesFromIsolateEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm23WasmFeaturesFromIsolateEPNS0_7IsolateE
	.type	_ZN2v88internal4wasm23WasmFeaturesFromIsolateEPNS0_7IsolateE, @function
_ZN2v88internal4wasm23WasmFeaturesFromIsolateEPNS0_7IsolateE:
.LFB8633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	_ZN2v88internal30FLAG_experimental_wasm_threadsE(%rip), %eax
	movq	12464(%rdi), %r9
	movq	41112(%rdi), %rdi
	movzbl	_ZN2v88internal25FLAG_experimental_wasm_mvE(%rip), %r15d
	movb	%al, -81(%rbp)
	movzbl	_ZN2v88internal40FLAG_experimental_wasm_compilation_hintsE(%rip), %eax
	movzbl	_ZN2v88internal25FLAG_experimental_wasm_ehE(%rip), %r14d
	movzbl	_ZN2v88internal27FLAG_experimental_wasm_simdE(%rip), %r13d
	movb	%al, -82(%rbp)
	movzbl	_ZN2v88internal29FLAG_experimental_wasm_anyrefE(%rip), %eax
	movzbl	_ZN2v88internal29FLAG_experimental_wasm_bigintE(%rip), %r12d
	movzbl	_ZN2v88internal34FLAG_experimental_wasm_return_callE(%rip), %ebx
	movb	%al, -83(%rbp)
	movzbl	_ZN2v88internal38FLAG_experimental_wasm_type_reflectionE(%rip), %eax
	movb	%al, -84(%rbp)
	movzbl	_ZN2v88internal34FLAG_experimental_wasm_bulk_memoryE(%rip), %eax
	movb	%al, -85(%rbp)
	movzbl	_ZN2v88internal42FLAG_experimental_wasm_sat_f2i_conversionsE(%rip), %eax
	movb	%al, -86(%rbp)
	movzbl	_ZN2v88internal25FLAG_experimental_wasm_seE(%rip), %eax
	movb	%al, -87(%rbp)
	testq	%rdi, %rdi
	je	.L6
	movq	%r9, %rsi
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-96(%rbp), %r8
	movq	%rax, %rsi
.L7:
	movq	%r8, %rdi
	call	_ZN2v88internal7Isolate21AreWasmThreadsEnabledENS0_6HandleINS0_7ContextEEE@PLT
	orb	-81(%rbp), %al
	movzbl	-84(%rbp), %edx
	movb	%r15b, -61(%rbp)
	movb	%al, -59(%rbp)
	movzbl	-82(%rbp), %eax
	movb	%r14b, -60(%rbp)
	movb	%al, -55(%rbp)
	movzbl	-83(%rbp), %eax
	movb	%r13b, -58(%rbp)
	movb	%al, -54(%rbp)
	movzbl	-85(%rbp), %eax
	movb	%r12b, -57(%rbp)
	salq	$8, %rax
	movb	%bl, -56(%rbp)
	orq	%rdx, %rax
	movzbl	-86(%rbp), %edx
	salq	$16, %rdx
	orq	%rdx, %rax
	movzbl	-87(%rbp), %edx
	salq	$24, %rdx
	orq	%rax, %rdx
	movzbl	-49(%rbp), %eax
	salq	$32, %rax
	orq	%rax, %rdx
	movq	-61(%rbp), %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movq	41088(%r8), %rsi
	cmpq	41096(%r8), %rsi
	je	.L10
.L8:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r8)
	movq	%r9, (%rsi)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%r8, %rdi
	movq	%r9, -104(%rbp)
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %r9
	movq	-96(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L8
	.cfi_endproc
.LFE8633:
	.size	_ZN2v88internal4wasm23WasmFeaturesFromIsolateEPNS0_7IsolateE, .-_ZN2v88internal4wasm23WasmFeaturesFromIsolateEPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal4wasm17UnionFeaturesIntoEPNS1_12WasmFeaturesERKS2_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal4wasm17UnionFeaturesIntoEPNS1_12WasmFeaturesERKS2_, @function
_GLOBAL__sub_I__ZN2v88internal4wasm17UnionFeaturesIntoEPNS1_12WasmFeaturesERKS2_:
.LFB9892:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE9892:
	.size	_GLOBAL__sub_I__ZN2v88internal4wasm17UnionFeaturesIntoEPNS1_12WasmFeaturesERKS2_, .-_GLOBAL__sub_I__ZN2v88internal4wasm17UnionFeaturesIntoEPNS1_12WasmFeaturesERKS2_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal4wasm17UnionFeaturesIntoEPNS1_12WasmFeaturesERKS2_
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
