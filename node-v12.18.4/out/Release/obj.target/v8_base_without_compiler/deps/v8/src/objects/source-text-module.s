	.file	"source-text-module.cc"
	.text
	.section	.rodata._ZNK2v88internal16SourceTextModule21GetSharedFunctionInfoEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZNK2v88internal16SourceTextModule21GetSharedFunctionInfoEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16SourceTextModule21GetSharedFunctionInfoEv
	.type	_ZNK2v88internal16SourceTextModule21GetSharedFunctionInfoEv, @function
_ZNK2v88internal16SourceTextModule21GetSharedFunctionInfoEv:
.LFB18385:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movslq	27(%rdx), %rax
	cmpq	$2, %rax
	je	.L2
	cmpl	$2, %eax
	jg	.L3
	cmpl	$1, %eax
	ja	.L4
	movq	47(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	cmpl	$3, %eax
	jne	.L4
	movq	47(%rdx), %rax
	movq	23(%rax), %rax
	movq	23(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	47(%rdx), %rax
	movq	23(%rax), %rax
	ret
.L4:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18385:
	.size	_ZNK2v88internal16SourceTextModule21GetSharedFunctionInfoEv, .-_ZNK2v88internal16SourceTextModule21GetSharedFunctionInfoEv
	.section	.text._ZN2v88internal16SourceTextModule11ExportIndexEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16SourceTextModule11ExportIndexEi
	.type	_ZN2v88internal16SourceTextModule11ExportIndexEi, @function
_ZN2v88internal16SourceTextModule11ExportIndexEi:
.LFB18386:
	.cfi_startproc
	endbr64
	leal	-1(%rdi), %eax
	ret
	.cfi_endproc
.LFE18386:
	.size	_ZN2v88internal16SourceTextModule11ExportIndexEi, .-_ZN2v88internal16SourceTextModule11ExportIndexEi
	.section	.text._ZN2v88internal16SourceTextModule11ImportIndexEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16SourceTextModule11ImportIndexEi
	.type	_ZN2v88internal16SourceTextModule11ImportIndexEi, @function
_ZN2v88internal16SourceTextModule11ImportIndexEi:
.LFB18387:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	notl	%eax
	ret
	.cfi_endproc
.LFE18387:
	.size	_ZN2v88internal16SourceTextModule11ImportIndexEi, .-_ZN2v88internal16SourceTextModule11ImportIndexEi
	.section	.text._ZN2v88internal16SourceTextModule20CreateIndirectExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_25SourceTextModuleInfoEntryEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16SourceTextModule20CreateIndirectExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_25SourceTextModuleInfoEntryEEE
	.type	_ZN2v88internal16SourceTextModule20CreateIndirectExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_25SourceTextModuleInfoEntryEEE, @function
_ZN2v88internal16SourceTextModule20CreateIndirectExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_25SourceTextModuleInfoEntryEEE:
.LFB18388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rsi), %rax
	movq	41112(%rdi), %rdi
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L15
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L16:
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE3PutENS0_6HandleIS2_EENS5_INS0_6ObjectEEES8_@PLT
	movq	(%r12), %r13
	movq	(%rax), %r12
	leaq	7(%r13), %r14
	movq	%r12, 7(%r13)
	testb	$1, %r12b
	je	.L14
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L29
	testb	$24, %al
	je	.L14
.L31:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L30
.L14:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L31
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L15:
	movq	41088(%rbx), %rdi
	cmpq	41096(%rbx), %rdi
	je	.L32
.L17:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdi)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L30:
	addq	$16, %rsp
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L17
	.cfi_endproc
.LFE18388:
	.size	_ZN2v88internal16SourceTextModule20CreateIndirectExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_25SourceTextModuleInfoEntryEEE, .-_ZN2v88internal16SourceTextModule20CreateIndirectExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_25SourceTextModuleInfoEntryEEE
	.section	.text._ZN2v88internal16SourceTextModule12CreateExportEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_10FixedArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16SourceTextModule12CreateExportEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_10FixedArrayEEE
	.type	_ZN2v88internal16SourceTextModule12CreateExportEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_10FixedArrayEEE, @function
_ZN2v88internal16SourceTextModule12CreateExportEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_10FixedArrayEEE:
.LFB18389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%rsi, -64(%rbp)
	leaq	88(%rdi), %rsi
	movl	%edx, -56(%rbp)
	call	_ZN2v88internal7Factory7NewCellENS0_6HandleINS0_6ObjectEEE@PLT
	movl	-56(%rbp), %edx
	movq	%rax, %r12
	movq	(%r15), %rax
	movq	(%r12), %r14
	movq	55(%rax), %r15
	leal	8(,%rdx,8), %eax
	cltq
	leaq	-1(%r15,%rax), %rsi
	movq	%r14, (%rsi)
	testb	$1, %r14b
	je	.L49
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L63
	testb	$24, %al
	je	.L49
.L69:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L64
	.p2align 4,,10
	.p2align 3
.L49:
	movq	-64(%rbp), %rax
	movq	41112(%rbx), %rdi
	movq	(%rax), %rax
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L37
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L38:
	movq	0(%r13), %rax
	movslq	11(%rax), %rdx
	testq	%rdx, %rdx
	jle	.L40
	subl	$1, %edx
	movl	$16, %r14d
	leaq	24(,%rdx,8), %rcx
	movq	%rcx, -56(%rbp)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L65:
	movq	%r9, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L42:
	movq	%r15, %rdi
	movq	%r12, %rdx
	addq	$8, %r14
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE3PutENS0_6HandleIS2_EENS5_INS0_6ObjectEEES8_@PLT
	movq	%rax, %r15
	cmpq	%r14, -56(%rbp)
	je	.L40
	movq	0(%r13), %rax
.L44:
	movq	-1(%rax,%r14), %r9
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L65
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L66
.L43:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r9, (%rsi)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L40:
	movq	-64(%rbp), %rax
	movq	(%r15), %r12
	movq	(%rax), %r13
	movq	%r12, 7(%r13)
	leaq	7(%r13), %r14
	testb	$1, %r12b
	je	.L33
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L67
	testb	$24, %al
	je	.L33
.L70:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L68
.L33:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L63:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L69
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L67:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L70
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L37:
	movq	41088(%rbx), %r15
	cmpq	41096(%rbx), %r15
	je	.L71
.L39:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r15)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L64:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L68:
	addq	$40, %rsp
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L71:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L39
	.cfi_endproc
.LFE18389:
	.size	_ZN2v88internal16SourceTextModule12CreateExportEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_10FixedArrayEEE, .-_ZN2v88internal16SourceTextModule12CreateExportEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_10FixedArrayEEE
	.section	.text._ZN2v88internal16SourceTextModule7GetCellEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16SourceTextModule7GetCellEi
	.type	_ZN2v88internal16SourceTextModule7GetCellEi, @function
_ZN2v88internal16SourceTextModule7GetCellEi:
.LFB18390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	%esi, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	call	_ZN2v88internal26SourceTextModuleDescriptor16GetCellIndexKindEi@PLT
	cmpl	$1, %eax
	je	.L73
	cmpl	$2, %eax
	je	.L74
	testl	%eax, %eax
	je	.L75
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	movq	(%r12), %rdx
	leal	8(,%rbx,8), %eax
	cltq
	movq	55(%rdx), %rdx
	movq	-1(%rax,%rdx), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movq	(%r12), %rdx
	notl	%ebx
	leal	16(,%rbx,8), %eax
	movq	63(%rdx), %rdx
	cltq
	movq	-1(%rax,%rdx), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L75:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18390:
	.size	_ZN2v88internal16SourceTextModule7GetCellEi, .-_ZN2v88internal16SourceTextModule7GetCellEi
	.section	.text._ZN2v88internal16SourceTextModule12LoadVariableEPNS0_7IsolateENS0_6HandleIS1_EEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16SourceTextModule12LoadVariableEPNS0_7IsolateENS0_6HandleIS1_EEi
	.type	_ZN2v88internal16SourceTextModule12LoadVariableEPNS0_7IsolateENS0_6HandleIS1_EEi, @function
_ZN2v88internal16SourceTextModule12LoadVariableEPNS0_7IsolateENS0_6HandleIS1_EEi:
.LFB18391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	%edx, %edi
	subq	$24, %rsp
	movq	(%rsi), %r13
	call	_ZN2v88internal26SourceTextModuleDescriptor16GetCellIndexKindEi@PLT
	cmpl	$1, %eax
	je	.L79
	cmpl	$2, %eax
	je	.L80
	testl	%eax, %eax
	je	.L81
	movl	$7, %eax
.L82:
	movq	41112(%rbx), %rdi
	movq	(%rax), %rsi
	testq	%rdi, %rdi
	je	.L83
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L87
.L85:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	notl	%r12d
	movq	63(%r13), %rdx
	leal	16(,%r12,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rax
	addq	$7, %rax
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L79:
	movq	55(%r13), %rdx
	leal	8(,%r12,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rax
	addq	$7, %rax
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L87:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L85
.L81:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18391:
	.size	_ZN2v88internal16SourceTextModule12LoadVariableEPNS0_7IsolateENS0_6HandleIS1_EEi, .-_ZN2v88internal16SourceTextModule12LoadVariableEPNS0_7IsolateENS0_6HandleIS1_EEi
	.section	.text._ZN2v88internal16SourceTextModule13StoreVariableENS0_6HandleIS1_EEiNS2_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16SourceTextModule13StoreVariableENS0_6HandleIS1_EEiNS2_INS0_6ObjectEEE
	.type	_ZN2v88internal16SourceTextModule13StoreVariableENS0_6HandleIS1_EEiNS2_INS0_6ObjectEEE, @function
_ZN2v88internal16SourceTextModule13StoreVariableENS0_6HandleIS1_EEiNS2_INS0_6ObjectEEE:
.LFB18392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %r13
	movl	%esi, %edi
	movl	%esi, %ebx
	call	_ZN2v88internal26SourceTextModuleDescriptor16GetCellIndexKindEi@PLT
	cmpl	$1, %eax
	je	.L89
	cmpl	$2, %eax
	je	.L90
	testl	%eax, %eax
	je	.L91
	movl	$7, %eax
	movl	$7, %r14d
	xorl	%r13d, %r13d
.L92:
	movq	(%r12), %r12
	movq	%r12, (%rax)
	testb	$1, %r12b
	je	.L88
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L104
	testb	$24, %al
	je	.L88
.L106:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L105
.L88:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L106
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L90:
	notl	%ebx
	movq	63(%r13), %rdx
	leal	16(,%rbx,8), %eax
	cltq
	movq	-1(%rax,%rdx), %r13
	leaq	7(%r13), %r14
	movq	%r14, %rax
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L89:
	movq	55(%r13), %rdx
	leal	8(,%rbx,8), %eax
	cltq
	movq	-1(%rax,%rdx), %r13
	leaq	7(%r13), %r14
	movq	%r14, %rax
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L105:
	popq	%rbx
	movq	%r12, %rdx
	movq	%r14, %rsi
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L91:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18392:
	.size	_ZN2v88internal16SourceTextModule13StoreVariableENS0_6HandleIS1_EEiNS2_INS0_6ObjectEEE, .-_ZN2v88internal16SourceTextModule13StoreVariableENS0_6HandleIS1_EEiNS2_INS0_6ObjectEEE
	.section	.text._ZN2v88internal16SourceTextModule13ResolveImportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEEiNS0_15MessageLocationEbPNS0_6Module10ResolveSetE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16SourceTextModule13ResolveImportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEEiNS0_15MessageLocationEbPNS0_6Module10ResolveSetE
	.type	_ZN2v88internal16SourceTextModule13ResolveImportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEEiNS0_15MessageLocationEbPNS0_6Module10ResolveSetE, @function
_ZN2v88internal16SourceTextModule13ResolveImportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEEiNS0_15MessageLocationEbPNS0_6Module10ResolveSetE:
.LFB18409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leal	16(,%rcx,8), %ebx
	movslq	%ebx, %rbx
	subq	$72, %rsp
	movl	%r8d, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	71(%rax), %rax
	movq	-1(%rbx,%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L108
	movq	%r10, -96(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-96(%rbp), %r10
	movq	%rax, %r14
	movq	(%r10), %rax
	movq	%rax, -72(%rbp)
	cmpl	$3, 27(%rax)
	jle	.L111
.L122:
	movq	47(%rax), %rax
.L112:
	movq	15(%rax), %rax
	movq	-1(%rbx,%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L115
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L116:
	pushq	40(%rbp)
	movzbl	-84(%rbp), %r8d
	movq	%r13, %rcx
	movq	%r15, %r9
	pushq	32(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	_ZN2v88internal6Module13ResolveExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEbPNS1_10ResolveSetE@PLT
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L120
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L121
.L110:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	movq	(%r10), %rax
	movq	%rax, -72(%rbp)
	cmpl	$3, 27(%rax)
	jg	.L122
.L111:
	leaq	-72(%rbp), %rdi
	call	_ZNK2v88internal16SourceTextModule21GetSharedFunctionInfoEv
	movq	15(%rax), %rdx
	testb	$1, %dl
	jne	.L123
.L113:
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal9ScopeInfo5EmptyEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
.L114:
	leaq	-64(%rbp), %rdi
	movq	%rdx, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo20ModuleDescriptorInfoEv@PLT
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L115:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L124
.L117:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L123:
	movq	-1(%rdx), %rcx
	cmpw	$136, 11(%rcx)
	jne	.L113
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L124:
	movq	%r12, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L121:
	movq	%r12, %rdi
	movq	%r10, -104(%rbp)
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %r10
	movq	-96(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L110
.L120:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18409:
	.size	_ZN2v88internal16SourceTextModule13ResolveImportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEEiNS0_15MessageLocationEbPNS0_6Module10ResolveSetE, .-_ZN2v88internal16SourceTextModule13ResolveImportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEEiNS0_15MessageLocationEbPNS0_6Module10ResolveSetE
	.section	.text._ZN2v88internal16SourceTextModule29ResolveExportUsingStarExportsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEbPNS0_6Module10ResolveSetE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16SourceTextModule29ResolveExportUsingStarExportsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEbPNS0_6Module10ResolveSetE
	.type	_ZN2v88internal16SourceTextModule29ResolveExportUsingStarExportsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEbPNS0_6Module10ResolveSetE, @function
_ZN2v88internal16SourceTextModule29ResolveExportUsingStarExportsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEbPNS0_6Module10ResolveSetE:
.LFB18410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -168(%rbp)
	movq	2328(%rdi), %rsi
	movq	%rcx, -136(%rbp)
	movl	%r8d, -156(%rbp)
	movq	%r9, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rcx), %rax
	movq	%rax, -96(%rbp)
	cmpq	%rsi, %rax
	je	.L127
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L128
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	je	.L129
.L128:
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal6String10SlowEqualsES1_@PLT
	testb	%al, %al
	jne	.L127
.L129:
	movq	(%r12), %rax
	movq	%rax, -104(%rbp)
	cmpl	$3, 27(%rax)
	jle	.L131
	movq	47(%rax), %rax
.L132:
	movq	23(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L136
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -120(%rbp)
.L137:
	movq	(%rax), %rax
	movslq	11(%rax), %rdx
	testq	%rdx, %rdx
	jle	.L127
	subl	$1, %edx
	movl	$16, %r14d
	leaq	24(,%rdx,8), %rbx
	leaq	-96(%rbp), %rdx
	movq	%rbx, -128(%rbp)
	xorl	%ebx, %ebx
	movq	%rdx, -152(%rbp)
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L176:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L140:
	movq	7(%rsi), %rax
	cmpq	%rax, 88(%r15)
	jne	.L143
	movq	(%r12), %rax
	movq	41112(%r15), %rdi
	movq	79(%rax), %r9
	testq	%rdi, %rdi
	je	.L144
	movq	%r9, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L145:
	movq	0(%r13), %rax
	movq	-152(%rbp), %rdi
	movslq	59(%rax), %rcx
	movslq	51(%rax), %rdx
	call	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEEii@PLT
	movq	0(%r13), %rax
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %rdx
	movq	%r15, %rdi
	movslq	35(%rax), %rcx
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	call	_ZN2v88internal16SourceTextModule13ResolveImportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEEiNS0_15MessageLocationEbPNS0_6Module10ResolveSetE
	addq	$32, %rsp
	testq	%rax, %rax
	je	.L174
	testq	%rbx, %rbx
	cmove	%rax, %rbx
	movq	(%rax), %rax
	cmpq	%rax, (%rbx)
	jne	.L175
.L143:
	addq	$8, %r14
	cmpq	%r14, -128(%rbp)
	je	.L153
	movq	-120(%rbp), %rax
	movq	(%rax), %rax
.L154:
	movq	-1(%r14,%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	jne	.L176
	movq	41088(%r15), %r13
	cmpq	41096(%r15), %r13
	je	.L177
.L141:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, 0(%r13)
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L127:
	xorl	%r12d, %r12d
	cmpb	$0, -156(%rbp)
	jne	.L178
.L152:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L179
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movq	-168(%rbp), %rdx
	movq	-136(%rbp), %rcx
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movl	$327, %esi
	call	_ZN2v88internal7Factory14NewSyntaxErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	leaq	16(%rbp), %rdx
	movq	%r15, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L136:
	movq	41088(%r15), %rax
	movq	%rax, -120(%rbp)
	cmpq	%rax, 41096(%r15)
	je	.L180
.L138:
	movq	-120(%rbp), %rbx
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r15)
	movq	%rbx, %rax
	movq	%rsi, (%rbx)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L144:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L181
.L146:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r9, (%rsi)
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L174:
	movq	12480(%r15), %rax
	cmpq	%rax, 96(%r15)
	je	.L143
	xorl	%r12d, %r12d
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L177:
	movq	%r15, %rdi
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L181:
	movq	%r15, %rdi
	movq	%r9, -176(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L153:
	testq	%rbx, %rbx
	je	.L127
	movq	(%r12), %rax
	movq	41112(%r15), %rdi
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L155
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L156:
	movq	-136(%rbp), %rsi
	movq	%rbx, %rdx
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE3PutENS0_6HandleIS2_EENS5_INS0_6ObjectEEES8_@PLT
	movq	(%r12), %r13
	movq	(%rax), %r12
	leaq	7(%r13), %r15
	movq	%r12, 7(%r13)
	testb	$1, %r12b
	je	.L162
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	je	.L159
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
.L159:
	testb	$24, %al
	je	.L162
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L182
	.p2align 4,,10
	.p2align 3
.L162:
	movq	%rbx, %r12
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L131:
	leaq	-104(%rbp), %rdi
	call	_ZNK2v88internal16SourceTextModule21GetSharedFunctionInfoEv
	movq	15(%rax), %rdx
	testb	$1, %dl
	jne	.L133
.L135:
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal9ScopeInfo5EmptyEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
.L134:
	leaq	-96(%rbp), %rdi
	movq	%rdx, -96(%rbp)
	call	_ZNK2v88internal9ScopeInfo20ModuleDescriptorInfoEv@PLT
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L175:
	movq	-136(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	movq	-168(%rbp), %rdx
	movl	$222, %esi
	call	_ZN2v88internal7Factory14NewSyntaxErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	leaq	16(%rbp), %rdx
	movq	%r15, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L155:
	movq	41088(%r15), %rdi
	cmpq	41096(%r15), %rdi
	je	.L183
.L157:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rdi)
	jmp	.L156
.L182:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L162
.L133:
	movq	-1(%rdx), %rcx
	cmpw	$136, 11(%rcx)
	jne	.L135
	jmp	.L134
.L180:
	movq	%r15, %rdi
	movq	%rsi, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	movq	%rax, -120(%rbp)
	jmp	.L138
.L183:
	movq	%r15, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L157
.L179:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18410:
	.size	_ZN2v88internal16SourceTextModule29ResolveExportUsingStarExportsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEbPNS0_6Module10ResolveSetE, .-_ZN2v88internal16SourceTextModule29ResolveExportUsingStarExportsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEbPNS0_6Module10ResolveSetE
	.section	.text._ZN2v88internal16SourceTextModule18PrepareInstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16SourceTextModule18PrepareInstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE
	.type	_ZN2v88internal16SourceTextModule18PrepareInstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE, @function
_ZN2v88internal16SourceTextModule18PrepareInstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE:
.LFB18411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%rsi, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, -72(%rbp)
	cmpl	$3, 27(%rax)
	jle	.L185
	movq	47(%rax), %rsi
.L186:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L190
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -152(%rbp)
.L191:
	movq	(%rax), %rax
	movq	15(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L193
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -88(%rbp)
.L194:
	movq	-144(%rbp), %rax
	movq	41112(%rbx), %rdi
	movq	(%rax), %rax
	movq	71(%rax), %rsi
	testq	%rdi, %rdi
	je	.L196
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L197:
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	movslq	11(%rax), %rdx
	testq	%rdx, %rdx
	jle	.L199
	subl	$1, %edx
	movl	$16, %r15d
	movq	%r12, -96(%rbp)
	leaq	24(,%rdx,8), %rcx
	movq	%r15, %r12
	movq	-144(%rbp), %r15
	movq	%rcx, -104(%rbp)
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L254:
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	*%r14
	testq	%rax, %rax
	je	.L253
.L245:
	movq	-96(%rbp), %rcx
	movq	(%rax), %rdx
	movq	(%rcx), %rdi
	leaq	-1(%rdi,%r12), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L233
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -112(%rbp)
	testl	$262144, %eax
	je	.L205
	movq	%rdx, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdi, -120(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-112(%rbp), %r8
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdi
	movq	8(%r8), %rax
.L205:
	testb	$24, %al
	je	.L233
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L233
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L233:
	addq	$8, %r12
	cmpq	%r12, -104(%rbp)
	je	.L248
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
.L207:
	movq	-1(%r12,%rax), %r8
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L254
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L255
.L202:
	leaq	8(%rsi), %rax
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%rax, 41088(%rbx)
	movq	%r8, (%rsi)
	call	*%r14
	testq	%rax, %rax
	jne	.L245
.L253:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	xorl	%eax, %eax
.L184:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L256
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%r8, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L248:
	movq	-96(%rbp), %r12
.L199:
	movq	(%r12), %rax
	movslq	11(%rax), %rdx
	testq	%rdx, %rdx
	jle	.L208
	subl	$1, %edx
	movl	$16, %r15d
	leaq	24(,%rdx,8), %rcx
	movq	%rcx, -88(%rbp)
	movq	%rbx, %rcx
	movq	%r15, %rbx
	movq	%r12, %r15
	movq	%rcx, %r12
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L257:
	movq	%r9, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L210:
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal6Module18PrepareInstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE@PLT
	testb	%al, %al
	je	.L184
	addq	$8, %rbx
	cmpq	%rbx, -88(%rbp)
	je	.L250
	movq	(%r15), %rax
.L214:
	movq	-1(%rax,%rbx), %r9
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L257
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L258
.L211:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r9, (%rsi)
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L250:
	movq	%r12, %rbx
.L208:
	movq	-152(%rbp), %rax
	leaq	-64(%rbp), %r14
	xorl	%r12d, %r12d
	movq	%r14, %rdi
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal20SourceTextModuleInfo18RegularExportCountEv@PLT
	testl	%eax, %eax
	jle	.L221
	movl	%eax, -88(%rbp)
	movq	-152(%rbp), %r15
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L259:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L219:
	movq	-144(%rbp), %rsi
	movl	%r13d, %edx
	movq	%rbx, %rdi
	addl	$1, %r12d
	call	_ZN2v88internal16SourceTextModule12CreateExportEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_10FixedArrayEEE
	cmpl	%r12d, -88(%rbp)
	je	.L221
.L215:
	movq	(%r15), %rax
	movl	%r12d, %esi
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal20SourceTextModuleInfo22RegularExportCellIndexEi@PLT
	movl	%r12d, %esi
	movq	%r14, %rdi
	movl	%eax, %r13d
	movq	(%r15), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal20SourceTextModuleInfo24RegularExportExportNamesEi@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	jne	.L259
	movq	41088(%rbx), %rcx
	cmpq	41096(%rbx), %rcx
	je	.L260
.L220:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rcx)
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L196:
	movq	41088(%rbx), %r12
	cmpq	%r12, 41096(%rbx)
	je	.L261
.L198:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L193:
	movq	41088(%rbx), %rax
	movq	%rax, -88(%rbp)
	cmpq	41096(%rbx), %rax
	je	.L262
.L195:
	movq	-88(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rcx)
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L190:
	movq	41088(%rbx), %rax
	movq	%rax, -152(%rbp)
	cmpq	41096(%rbx), %rax
	je	.L263
.L192:
	movq	-152(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rcx, %rax
	movq	%rsi, (%rcx)
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L258:
	movq	%r12, %rdi
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L185:
	leaq	-72(%rbp), %rdi
	call	_ZNK2v88internal16SourceTextModule21GetSharedFunctionInfoEv
	movq	15(%rax), %rdx
	testb	$1, %dl
	jne	.L187
.L189:
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal9ScopeInfo5EmptyEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
.L188:
	leaq	-64(%rbp), %rdi
	movq	%rdx, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo20ModuleDescriptorInfoEv@PLT
	movq	%rax, %rsi
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L221:
	movq	-152(%rbp), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L264
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L222:
	movslq	11(%rsi), %rax
	testq	%rax, %rax
	jle	.L224
	subl	$1, %eax
	movq	-144(%rbp), %r15
	movl	$16, %r12d
	leaq	24(,%rax,8), %r14
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L265:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	41112(%rbx), %rdi
	movq	(%rax), %rsi
	movq	%rax, %rcx
	movq	7(%rsi), %rsi
	testq	%rdi, %rdi
	je	.L228
.L267:
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-88(%rbp), %rcx
	movq	(%rax), %rsi
	movq	%rax, %rdx
.L229:
	cmpq	%rsi, 88(%rbx)
	je	.L231
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal16SourceTextModule20CreateIndirectExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_25SourceTextModuleInfoEntryEEE
.L231:
	addq	$8, %r12
	cmpq	%r14, %r12
	je	.L224
	movq	0(%r13), %rsi
.L232:
	movq	-1(%r12,%rsi), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L265
	movq	41088(%rbx), %rcx
	cmpq	41096(%rbx), %rcx
	je	.L266
.L227:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rcx)
	movq	41112(%rbx), %rdi
	movq	7(%rsi), %rsi
	testq	%rdi, %rdi
	jne	.L267
.L228:
	movq	41088(%rbx), %rdx
	cmpq	41096(%rbx), %rdx
	je	.L268
.L230:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdx)
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L264:
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L269
.L223:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L224:
	movl	$1, %eax
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L260:
	movq	%rbx, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L266:
	movq	%rbx, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L268:
	movq	%rbx, %rdi
	movq	%rsi, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L187:
	movq	-1(%rdx), %rcx
	cmpw	$136, 11(%rcx)
	jne	.L189
	jmp	.L188
.L261:
	movq	%rbx, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L198
.L263:
	movq	%rbx, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, -152(%rbp)
	jmp	.L192
.L262:
	movq	%rbx, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, -88(%rbp)
	jmp	.L195
.L269:
	movq	%rbx, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L223
.L256:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18411:
	.size	_ZN2v88internal16SourceTextModule18PrepareInstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE, .-_ZN2v88internal16SourceTextModule18PrepareInstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE
	.section	.text._ZN2v88internal16SourceTextModule21RunInitializationCodeEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16SourceTextModule21RunInitializationCodeEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal16SourceTextModule21RunInitializationCodeEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal16SourceTextModule21RunInitializationCodeEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	41112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	47(%rax), %r13
	testq	%rdi, %rdi
	je	.L271
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L272:
	leaq	88(%r12), %rdx
	leaq	-48(%rbp), %r8
	movq	%r12, %rdi
	movq	%rbx, -48(%rbp)
	movl	$1, %ecx
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	testq	%rax, %rax
	je	.L280
	movq	(%rbx), %r13
	movq	(%rax), %r12
	movq	%r12, 47(%r13)
	leaq	47(%r13), %r14
	testb	$1, %r12b
	je	.L278
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L288
	testb	$24, %al
	je	.L278
.L291:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L289
	.p2align 4,,10
	.p2align 3
.L278:
	movl	$1, %eax
.L270:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L290
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L291
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L271:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L292
.L273:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L289:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L280:
	xorl	%eax, %eax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L292:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L273
.L290:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18412:
	.size	_ZN2v88internal16SourceTextModule21RunInitializationCodeEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal16SourceTextModule21RunInitializationCodeEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal16SourceTextModule24MaybeTransitionComponentEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListIS5_EENS0_6Module6StatusE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16SourceTextModule24MaybeTransitionComponentEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListIS5_EENS0_6Module6StatusE
	.type	_ZN2v88internal16SourceTextModule24MaybeTransitionComponentEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListIS5_EENS0_6Module6StatusE, @function
_ZN2v88internal16SourceTextModule24MaybeTransitionComponentEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListIS5_EENS0_6Module6StatusE:
.LFB18413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	107(%rax), %edx
	cmpl	%edx, 99(%rax)
	je	.L306
.L294:
	movl	$1, %eax
.L293:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L307
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	movq	%rsi, %r13
	movl	%ecx, %r15d
	leaq	-64(%rbp), %r14
	cmpl	$3, %ecx
	je	.L308
	.p2align 4,,10
	.p2align 3
.L296:
	movq	8(%rbx), %rax
	movl	%r15d, %esi
	movq	%r14, %rdi
	movq	8(%rax), %r12
	movq	(%rax), %rax
	movq	%rax, 8(%rbx)
	movq	(%r12), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Module9SetStatusENS1_6StatusE@PLT
	movq	(%r12), %rax
	cmpq	%rax, 0(%r13)
	jne	.L296
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L308:
	movq	%rdi, %r12
	leaq	-64(%rbp), %r14
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L309:
	movq	(%r15), %rax
	movl	$3, %esi
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Module9SetStatusENS1_6StatusE@PLT
	movq	0(%r13), %rax
	cmpq	%rax, (%r15)
	je	.L294
.L295:
	movq	8(%rbx), %rax
	movq	%r12, %rdi
	movq	8(%rax), %r15
	movq	(%rax), %rax
	movq	%rax, 8(%rbx)
	movq	%r15, %rsi
	call	_ZN2v88internal16SourceTextModule21RunInitializationCodeEPNS0_7IsolateENS0_6HandleIS1_EE
	testb	%al, %al
	jne	.L309
	jmp	.L293
.L307:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18413:
	.size	_ZN2v88internal16SourceTextModule24MaybeTransitionComponentEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListIS5_EENS0_6Module6StatusE, .-_ZN2v88internal16SourceTextModule24MaybeTransitionComponentEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListIS5_EENS0_6Module6StatusE
	.section	.text._ZN2v88internal16SourceTextModule18GetModuleNamespaceEPNS0_7IsolateENS0_6HandleIS1_EEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16SourceTextModule18GetModuleNamespaceEPNS0_7IsolateENS0_6HandleIS1_EEi
	.type	_ZN2v88internal16SourceTextModule18GetModuleNamespaceEPNS0_7IsolateENS0_6HandleIS1_EEi, @function
_ZN2v88internal16SourceTextModule18GetModuleNamespaceEPNS0_7IsolateENS0_6HandleIS1_EEi:
.LFB18445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r8d
	leal	16(,%r8,8), %eax
	cltq
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	(%rsi), %rdx
	movq	%rdi, %r12
	movq	71(%rdx), %rdx
	movq	-1(%rax,%rdx), %r13
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L311
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6Module18GetModuleNamespaceEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	.p2align 4,,10
	.p2align 3
.L311:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L315
.L313:
	leaq	8(%rsi), %rax
	movq	%r12, %rdi
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6Module18GetModuleNamespaceEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L313
	.cfi_endproc
.LFE18445:
	.size	_ZN2v88internal16SourceTextModule18GetModuleNamespaceEPNS0_7IsolateENS0_6HandleIS1_EEi, .-_ZN2v88internal16SourceTextModule18GetModuleNamespaceEPNS0_7IsolateENS0_6HandleIS1_EEi
	.section	.text._ZN2v88internal16SourceTextModule8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListIS5_EEPj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16SourceTextModule8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListIS5_EEPj
	.type	_ZN2v88internal16SourceTextModule8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListIS5_EEPj, @function
_ZN2v88internal16SourceTextModule8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListIS5_EEPj:
.LFB18446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$72, %rsp
	movq	41112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	47(%rax), %rsi
	testq	%rdi, %rdi
	je	.L317
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -104(%rbp)
	movq	(%rax), %rsi
.L318:
	movq	23(%rsi), %rax
	movq	(%r12), %r14
	movq	23(%rax), %rdx
	movq	15(%rdx), %rax
	testb	$1, %al
	jne	.L320
.L322:
	andq	$-262144, %rdx
	movq	24(%rdx), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal9ScopeInfo5EmptyEPNS0_7IsolateE@PLT
.L321:
	movq	%rax, -64(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -96(%rbp)
	call	_ZNK2v88internal9ScopeInfo20ModuleDescriptorInfoEv@PLT
	leaq	47(%r14), %rsi
	movq	%rax, 47(%r14)
	movq	%rax, %rdx
	testb	$1, %al
	je	.L352
	movq	%rax, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	jne	.L364
	testb	$24, %al
	je	.L352
.L371:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L365
	.p2align 4,,10
	.p2align 3
.L352:
	movq	(%r12), %rax
	movq	-96(%rbp), %rdi
	movl	$4, %esi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Module9SetStatusENS1_6StatusE@PLT
	movslq	0(%r13), %rax
	movq	(%r12), %rdx
	salq	$32, %rax
	movq	%rax, 95(%rdx)
	movslq	0(%r13), %rax
	movq	(%r12), %rdx
	salq	$32, %rax
	movq	%rax, 103(%rdx)
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L366
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L327:
	movq	$0, (%rax)
	movq	%r12, 8(%rax)
	movq	8(%rbx), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 8(%rbx)
	addl	$1, 0(%r13)
	movq	(%r12), %rax
	movq	41112(%r15), %rdi
	movq	71(%rax), %rsi
	testq	%rdi, %rdi
	je	.L328
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -80(%rbp)
	movq	(%rax), %rsi
.L329:
	movslq	11(%rsi), %rax
	testq	%rax, %rax
	jle	.L331
	subl	$1, %eax
	movl	$16, %r14d
	leaq	24(,%rax,8), %rax
	movq	%rax, -88(%rbp)
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L367:
	movq	%r10, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L333:
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal6Module8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListINS4_INS0_16SourceTextModuleEEEEEPj@PLT
	movq	-72(%rbp), %rsi
	testq	%rax, %rax
	je	.L336
	movq	(%rsi), %rax
	cmpl	$4, 27(%rax)
	jne	.L337
	movq	(%r12), %rdx
	movslq	107(%rax), %rcx
	movslq	107(%rdx), %rax
	cmpl	%ecx, %eax
	cmovge	%ecx, %eax
	salq	$32, %rax
	movq	%rax, 103(%rdx)
.L337:
	addq	$8, %r14
	cmpq	-88(%rbp), %r14
	je	.L331
	movq	-80(%rbp), %rax
	movq	(%rax), %rsi
.L338:
	movq	-1(%rsi,%r14), %r10
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	jne	.L367
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L368
.L334:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r10, (%rsi)
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L331:
	movq	12464(%r15), %rax
	movq	39(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L339
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L340:
	movq	1831(%rsi), %r13
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L342
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L343:
	movq	-104(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	movq	%rax, %r13
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L336
	movq	(%r12), %rax
	movl	107(%rax), %ecx
	cmpl	%ecx, 99(%rax)
	je	.L346
.L349:
	movq	0(%r13), %rax
	movq	41112(%r15), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L369
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L336:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L370
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	movq	%r15, %rdi
	movq	%r10, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %r10
	movq	%rax, %rsi
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L364:
	movq	%r14, %rdi
	movq	%rdx, -88(%rbp)
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L371
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L346:
	movq	8(%rbx), %rax
	movq	-96(%rbp), %rdi
	movl	$5, %esi
	movq	8(%rax), %r14
	movq	(%rax), %rax
	movq	%rax, 8(%rbx)
	movq	(%r14), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Module9SetStatusENS1_6StatusE@PLT
	movq	(%r14), %rax
	cmpq	%rax, (%r12)
	jne	.L346
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L339:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L372
.L341:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L317:
	movq	41088(%r15), %rax
	movq	%rax, -104(%rbp)
	cmpq	41096(%r15), %rax
	je	.L373
.L319:
	movq	-104(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rcx)
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L328:
	movq	41088(%r15), %rax
	movq	%rax, -80(%rbp)
	cmpq	%rax, 41096(%r15)
	je	.L374
.L330:
	movq	-80(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rcx)
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L320:
	movq	-1(%rax), %rcx
	cmpw	$136, 11(%rcx)
	jne	.L322
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L342:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L375
.L344:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r13, (%rsi)
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L365:
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L369:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L376
.L351:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L366:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L327
.L374:
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, -80(%rbp)
	jmp	.L330
.L373:
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, -104(%rbp)
	jmp	.L319
.L375:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L344
.L372:
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L341
.L376:
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L351
.L370:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18446:
	.size	_ZN2v88internal16SourceTextModule8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListIS5_EEPj, .-_ZN2v88internal16SourceTextModule8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListIS5_EEPj
	.section	.text._ZN2v88internal16SourceTextModule5ResetEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16SourceTextModule5ResetEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal16SourceTextModule5ResetEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal16SourceTextModule5ResetEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	(%rsi), %rax
	movq	55(%rax), %rax
	movslq	11(%rax), %rsi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	63(%rax), %rax
	movslq	11(%rax), %rsi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	71(%rax), %rax
	movslq	11(%rax), %rsi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%rbx), %r12
	movq	%rax, %r15
	cmpl	$2, 27(%r12)
	je	.L420
.L378:
	movq	(%r14), %r14
	leaq	55(%r12), %rsi
	movq	%r14, 55(%r12)
	testb	$1, %r14b
	je	.L393
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L421
	testb	$24, %al
	je	.L393
.L429:
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L422
	.p2align 4,,10
	.p2align 3
.L393:
	movq	(%rbx), %r14
	movq	0(%r13), %r12
	movq	%r12, 63(%r14)
	leaq	63(%r14), %rsi
	testb	$1, %r12b
	je	.L392
	movq	%r12, %r13
	andq	$-262144, %r13
	movq	8(%r13), %rax
	testl	$262144, %eax
	jne	.L423
	testb	$24, %al
	je	.L392
.L428:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L424
	.p2align 4,,10
	.p2align 3
.L392:
	movq	(%rbx), %r13
	movq	(%r15), %r12
	movq	%r12, 71(%r13)
	leaq	71(%r13), %r15
	testb	$1, %r12b
	je	.L391
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	jne	.L425
	testb	$24, %al
	je	.L391
.L427:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L426
.L391:
	movabsq	$-4294967296, %rax
	movq	(%rbx), %rdx
	movq	%rax, 95(%rdx)
	movq	(%rbx), %rdx
	movq	%rax, 103(%rdx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L425:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	testb	$24, %al
	jne	.L427
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L423:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r13), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L428
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L421:
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L429
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L420:
	movq	47(%r12), %rax
	leaq	47(%r12), %rsi
	movq	23(%rax), %rdx
	movq	%rdx, 47(%r12)
	testb	$1, %dl
	je	.L394
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L380
	movq	%r12, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
.L380:
	testb	$24, %al
	je	.L394
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L430
	.p2align 4,,10
	.p2align 3
.L394:
	movq	(%rbx), %r12
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L422:
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L424:
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L426:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L430:
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L394
	.cfi_endproc
.LFE18447:
	.size	_ZN2v88internal16SourceTextModule5ResetEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal16SourceTextModule5ResetEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZNSt10_HashtableIN2v88internal6HandleINS1_6ModuleEEES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS1_17ModuleHandleEqualENS1_16ModuleHandleHashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS4_NS7_10_AllocNodeINS5_INS7_10_Hash_nodeIS4_Lb1EEEEEEEEESt4pairINS7_14_Node_iteratorIS4_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6HandleINS1_6ModuleEEES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS1_17ModuleHandleEqualENS1_16ModuleHandleHashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS4_NS7_10_AllocNodeINS5_INS7_10_Hash_nodeIS4_Lb1EEEEEEEEESt4pairINS7_14_Node_iteratorIS4_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal6HandleINS1_6ModuleEEES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS1_17ModuleHandleEqualENS1_16ModuleHandleHashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS4_NS7_10_AllocNodeINS5_INS7_10_Hash_nodeIS4_Lb1EEEEEEEEESt4pairINS7_14_Node_iteratorIS4_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	.type	_ZNSt10_HashtableIN2v88internal6HandleINS1_6ModuleEEES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS1_17ModuleHandleEqualENS1_16ModuleHandleHashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS4_NS7_10_AllocNodeINS5_INS7_10_Hash_nodeIS4_Lb1EEEEEEEEESt4pairINS7_14_Node_iteratorIS4_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm, @function
_ZNSt10_HashtableIN2v88internal6HandleINS1_6ModuleEEES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS1_17ModuleHandleEqualENS1_16ModuleHandleHashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS4_NS7_10_AllocNodeINS5_INS7_10_Hash_nodeIS4_Lb1EEEEEEEEESt4pairINS7_14_Node_iteratorIS4_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm:
.LFB22733:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r10
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movq	16(%rdi), %rdi
	movq	(%rax), %r11
	movslq	19(%r11), %r13
	movq	%r13, %rax
	divq	%rdi
	movq	8(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	leaq	0(,%rdx,8), %r15
	testq	%rax, %rax
	je	.L432
	movq	(%rax), %rsi
	movq	%rdx, %r9
	movq	16(%rsi), %r8
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L433:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L432
	movq	16(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L432
.L435:
	cmpq	%r8, %r13
	jne	.L433
	movq	8(%rsi), %rax
	cmpq	(%rax), %r11
	jne	.L433
	addq	$24, %rsp
	movq	%rsi, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	.cfi_restore_state
	movq	(%r10), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$23, %rax
	jbe	.L470
	leaq	24(%rbx), %rax
	movq	%rax, 16(%rdi)
.L437:
	movq	$0, (%rbx)
	movq	(%r14), %rax
	leaq	40(%r12), %rdi
	movq	%rax, 8(%rbx)
	movq	32(%r12), %rdx
	movq	16(%r12), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r14
	testb	%al, %al
	jne	.L471
.L438:
	movq	%r13, 16(%rbx)
	movq	8(%r12), %rax
	movq	(%rax,%r15), %rax
	testq	%rax, %rax
	je	.L447
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	8(%r12), %rax
	movq	(%rax,%r15), %rax
	movq	%rbx, (%rax)
.L448:
	addq	$1, 32(%r12)
	addq	$24, %rsp
	movq	%rbx, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L471:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L472
	movq	(%r12), %rdi
	leaq	0(,%rdx,8), %rdx
	movq	%rdx, %rsi
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	subq	%r8, %rax
	cmpq	%rax, %rdx
	ja	.L473
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L442:
	movq	%r8, %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movq	%rax, %r8
.L440:
	movq	24(%r12), %rsi
	movq	$0, 24(%r12)
	testq	%rsi, %rsi
	je	.L443
	xorl	%r9d, %r9d
	leaq	24(%r12), %r10
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L445:
	movq	(%rdi), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L446:
	testq	%rsi, %rsi
	je	.L443
.L444:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	16(%rcx), %rax
	divq	%r14
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L445
	movq	24(%r12), %rdi
	movq	%rdi, (%rcx)
	movq	%rcx, 24(%r12)
	movq	%r10, (%rax)
	cmpq	$0, (%rcx)
	je	.L451
	movq	%rcx, (%r8,%r9,8)
	movq	%rdx, %r9
	testq	%rsi, %rsi
	jne	.L444
	.p2align 4,,10
	.p2align 3
.L443:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%r14, 16(%r12)
	divq	%r14
	movq	%r8, 8(%r12)
	leaq	0(,%rdx,8), %r15
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L447:
	movq	24(%r12), %rax
	movq	%rax, (%rbx)
	movq	%rbx, 24(%r12)
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L449
	movq	16(%rax), %rax
	xorl	%edx, %edx
	divq	16(%r12)
	movq	8(%r12), %rax
	movq	%rbx, (%rax,%rdx,8)
.L449:
	movq	8(%r12), %rax
	leaq	24(%r12), %rdx
	movq	%rdx, (%rax,%r15)
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L451:
	movq	%rdx, %r9
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L470:
	movl	$24, %esi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %rbx
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L472:
	movq	$0, 56(%r12)
	leaq	56(%r12), %r8
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L473:
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L442
	.cfi_endproc
.LFE22733:
	.size	_ZNSt10_HashtableIN2v88internal6HandleINS1_6ModuleEEES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS1_17ModuleHandleEqualENS1_16ModuleHandleHashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS4_NS7_10_AllocNodeINS5_INS7_10_Hash_nodeIS4_Lb1EEEEEEEEESt4pairINS7_14_Node_iteratorIS4_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm, .-_ZNSt10_HashtableIN2v88internal6HandleINS1_6ModuleEEES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS1_17ModuleHandleEqualENS1_16ModuleHandleHashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS4_NS7_10_AllocNodeINS5_INS7_10_Hash_nodeIS4_Lb1EEEEEEEEESt4pairINS7_14_Node_iteratorIS4_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	.section	.text._ZNSt10_HashtableIN2v88internal6HandleINS1_6ModuleEEESt4pairIKS4_PNS1_18UnorderedStringSetEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17ModuleHandleEqualENS1_16ModuleHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSC_10_Hash_nodeIS9_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6HandleINS1_6ModuleEEESt4pairIKS4_PNS1_18UnorderedStringSetEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17ModuleHandleEqualENS1_16ModuleHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSC_10_Hash_nodeIS9_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal6HandleINS1_6ModuleEEESt4pairIKS4_PNS1_18UnorderedStringSetEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17ModuleHandleEqualENS1_16ModuleHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSC_10_Hash_nodeIS9_Lb1EEEm
	.type	_ZNSt10_HashtableIN2v88internal6HandleINS1_6ModuleEEESt4pairIKS4_PNS1_18UnorderedStringSetEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17ModuleHandleEqualENS1_16ModuleHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSC_10_Hash_nodeIS9_Lb1EEEm, @function
_ZNSt10_HashtableIN2v88internal6HandleINS1_6ModuleEEESt4pairIKS4_PNS1_18UnorderedStringSetEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17ModuleHandleEqualENS1_16ModuleHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSC_10_Hash_nodeIS9_Lb1EEEm:
.LFB23015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$40, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L498
.L475:
	movq	%r14, 24(%r12)
	movq	8(%rbx), %rax
	leaq	0(,%r15,8), %rcx
	movq	(%rax,%r15,8), %rax
	testq	%rax, %rax
	je	.L484
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r12, (%rax)
.L485:
	addq	$1, 32(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L498:
	.cfi_restore_state
	movq	%rdx, %r13
	cmpq	$1, %rdx
	je	.L499
	movq	(%rbx), %r8
	leaq	0(,%rdx,8), %rdx
	movq	%rdx, %rsi
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L500
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L479:
	xorl	%esi, %esi
	call	memset@PLT
	movq	%rax, %rdi
.L477:
	movq	24(%rbx), %rsi
	movq	$0, 24(%rbx)
	testq	%rsi, %rsi
	je	.L480
	xorl	%r9d, %r9d
	leaq	24(%rbx), %r10
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L482:
	movq	(%r8), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L483:
	testq	%rsi, %rsi
	je	.L480
.L481:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	24(%rcx), %rax
	divq	%r13
	leaq	(%rdi,%rdx,8), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	jne	.L482
	movq	24(%rbx), %r8
	movq	%r8, (%rcx)
	movq	%rcx, 24(%rbx)
	movq	%r10, (%rax)
	cmpq	$0, (%rcx)
	je	.L487
	movq	%rcx, (%rdi,%r9,8)
	movq	%rdx, %r9
	testq	%rsi, %rsi
	jne	.L481
	.p2align 4,,10
	.p2align 3
.L480:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r13, 16(%rbx)
	divq	%r13
	movq	%rdi, 8(%rbx)
	movq	%rdx, %r15
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L484:
	movq	24(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 24(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L486
	movq	24(%rax), %rax
	xorl	%edx, %edx
	divq	16(%rbx)
	movq	8(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L486:
	movq	8(%rbx), %rax
	leaq	24(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L487:
	movq	%rdx, %r9
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L499:
	movq	$0, 56(%rbx)
	leaq	56(%rbx), %rdi
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L500:
	movq	%r8, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L479
	.cfi_endproc
.LFE23015:
	.size	_ZNSt10_HashtableIN2v88internal6HandleINS1_6ModuleEEESt4pairIKS4_PNS1_18UnorderedStringSetEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17ModuleHandleEqualENS1_16ModuleHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSC_10_Hash_nodeIS9_Lb1EEEm, .-_ZNSt10_HashtableIN2v88internal6HandleINS1_6ModuleEEESt4pairIKS4_PNS1_18UnorderedStringSetEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17ModuleHandleEqualENS1_16ModuleHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSC_10_Hash_nodeIS9_Lb1EEEm
	.section	.text._ZNSt10_HashtableIN2v88internal6HandleINS1_6ModuleEEESt4pairIKS4_PNS1_18UnorderedStringSetEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17ModuleHandleEqualENS1_16ModuleHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS9_EEES5_INSC_14_Node_iteratorIS9_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6HandleINS1_6ModuleEEESt4pairIKS4_PNS1_18UnorderedStringSetEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17ModuleHandleEqualENS1_16ModuleHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS9_EEES5_INSC_14_Node_iteratorIS9_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal6HandleINS1_6ModuleEEESt4pairIKS4_PNS1_18UnorderedStringSetEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17ModuleHandleEqualENS1_16ModuleHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS9_EEES5_INSC_14_Node_iteratorIS9_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	.type	_ZNSt10_HashtableIN2v88internal6HandleINS1_6ModuleEEESt4pairIKS4_PNS1_18UnorderedStringSetEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17ModuleHandleEqualENS1_16ModuleHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS9_EEES5_INSC_14_Node_iteratorIS9_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_, @function
_ZNSt10_HashtableIN2v88internal6HandleINS1_6ModuleEEESt4pairIKS4_PNS1_18UnorderedStringSetEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17ModuleHandleEqualENS1_16ModuleHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS9_EEES5_INSC_14_Node_iteratorIS9_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_:
.LFB22684:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rdi
	movq	%rsi, %rbx
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	$31, %rax
	jbe	.L518
	leaq	32(%rcx), %rax
	movq	%rax, 16(%rdi)
.L503:
	movq	$0, (%rcx)
	movq	(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	%rax, 8(%rcx)
	movq	%rdx, 16(%rcx)
	movq	(%rax), %r11
	xorl	%edx, %edx
	movq	16(%r12), %rdi
	movslq	19(%r11), %r9
	movq	%r9, %rax
	divq	%rdi
	movq	8(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L504
	movq	(%rax), %rsi
	movq	24(%rsi), %r8
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L505:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L504
	movq	24(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L504
.L507:
	cmpq	%r8, %r9
	jne	.L505
	movq	8(%rsi), %rax
	cmpq	(%rax), %r11
	jne	.L505
	popq	%rbx
	movq	%rsi, %rax
	xorl	%edx, %edx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L504:
	.cfi_restore_state
	movq	%r9, %rdx
	movq	%r12, %rdi
	movl	$1, %r8d
	movq	%r10, %rsi
	call	_ZNSt10_HashtableIN2v88internal6HandleINS1_6ModuleEEESt4pairIKS4_PNS1_18UnorderedStringSetEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17ModuleHandleEqualENS1_16ModuleHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSC_10_Hash_nodeIS9_Lb1EEEm
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L518:
	.cfi_restore_state
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L503
	.cfi_endproc
.LFE22684:
	.size	_ZNSt10_HashtableIN2v88internal6HandleINS1_6ModuleEEESt4pairIKS4_PNS1_18UnorderedStringSetEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17ModuleHandleEqualENS1_16ModuleHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS9_EEES5_INSC_14_Node_iteratorIS9_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_, .-_ZNSt10_HashtableIN2v88internal6HandleINS1_6ModuleEEESt4pairIKS4_PNS1_18UnorderedStringSetEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17ModuleHandleEqualENS1_16ModuleHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS9_EEES5_INSC_14_Node_iteratorIS9_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	.section	.text._ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS1_17StringHandleEqualENS1_16StringHandleHashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS1_17StringHandleEqualENS1_16StringHandleHashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS1_17StringHandleEqualENS1_16StringHandleHashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm
	.type	_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS1_17StringHandleEqualENS1_16StringHandleHashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm, @function
_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS1_17StringHandleEqualENS1_16StringHandleHashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm:
.LFB23032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$40, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L543
.L520:
	movq	%r14, 16(%r12)
	movq	8(%rbx), %rax
	leaq	0(,%r15,8), %rcx
	movq	(%rax,%r15,8), %rax
	testq	%rax, %rax
	je	.L529
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r12, (%rax)
.L530:
	addq	$1, 32(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L543:
	.cfi_restore_state
	movq	%rdx, %r13
	cmpq	$1, %rdx
	je	.L544
	movq	(%rbx), %r8
	leaq	0(,%rdx,8), %rdx
	movq	%rdx, %rsi
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L545
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L524:
	xorl	%esi, %esi
	call	memset@PLT
	movq	%rax, %rdi
.L522:
	movq	24(%rbx), %rsi
	movq	$0, 24(%rbx)
	testq	%rsi, %rsi
	je	.L525
	xorl	%r9d, %r9d
	leaq	24(%rbx), %r10
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L527:
	movq	(%r8), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L528:
	testq	%rsi, %rsi
	je	.L525
.L526:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	16(%rcx), %rax
	divq	%r13
	leaq	(%rdi,%rdx,8), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	jne	.L527
	movq	24(%rbx), %r8
	movq	%r8, (%rcx)
	movq	%rcx, 24(%rbx)
	movq	%r10, (%rax)
	cmpq	$0, (%rcx)
	je	.L532
	movq	%rcx, (%rdi,%r9,8)
	movq	%rdx, %r9
	testq	%rsi, %rsi
	jne	.L526
	.p2align 4,,10
	.p2align 3
.L525:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r13, 16(%rbx)
	divq	%r13
	movq	%rdi, 8(%rbx)
	movq	%rdx, %r15
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L529:
	movq	24(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 24(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L531
	movq	16(%rax), %rax
	xorl	%edx, %edx
	divq	16(%rbx)
	movq	8(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L531:
	movq	8(%rbx), %rax
	leaq	24(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L532:
	movq	%rdx, %r9
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L544:
	movq	$0, 56(%rbx)
	leaq	56(%rbx), %rdi
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L545:
	movq	%r8, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L524
	.cfi_endproc
.LFE23032:
	.size	_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS1_17StringHandleEqualENS1_16StringHandleHashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm, .-_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS1_17StringHandleEqualENS1_16StringHandleHashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm
	.section	.text._ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS1_17StringHandleEqualENS1_16StringHandleHashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS4_NS7_10_AllocNodeINS5_INS7_10_Hash_nodeIS4_Lb1EEEEEEEEESt4pairINS7_14_Node_iteratorIS4_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS1_17StringHandleEqualENS1_16StringHandleHashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS4_NS7_10_AllocNodeINS5_INS7_10_Hash_nodeIS4_Lb1EEEEEEEEESt4pairINS7_14_Node_iteratorIS4_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS1_17StringHandleEqualENS1_16StringHandleHashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS4_NS7_10_AllocNodeINS5_INS7_10_Hash_nodeIS4_Lb1EEEEEEEEESt4pairINS7_14_Node_iteratorIS4_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	.type	_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS1_17StringHandleEqualENS1_16StringHandleHashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS4_NS7_10_AllocNodeINS5_INS7_10_Hash_nodeIS4_Lb1EEEEEEEEESt4pairINS7_14_Node_iteratorIS4_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm, @function
_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS1_17StringHandleEqualENS1_16StringHandleHashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS4_NS7_10_AllocNodeINS5_INS7_10_Hash_nodeIS4_Lb1EEEEEEEEESt4pairINS7_14_Node_iteratorIS4_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm:
.LFB22696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -72(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	(%rax), %rax
	movl	7(%rax), %r12d
	testb	$1, %r12b
	jne	.L547
	shrl	$2, %r12d
.L548:
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	16(%r15)
	movq	8(%r15), %rax
	movq	(%rax,%rdx,8), %r11
	movq	%rdx, %r10
	testq	%r11, %r11
	je	.L549
	movq	(%r11), %rbx
	leaq	-64(%rbp), %rdi
	movq	16(%rbx), %rcx
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L551:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L549
	movq	16(%rsi), %rcx
	xorl	%edx, %edx
	movq	%rbx, %r11
	movq	%rcx, %rax
	divq	16(%r15)
	cmpq	%rdx, %r10
	jne	.L549
	movq	%rsi, %rbx
.L556:
	cmpq	%rcx, %r12
	jne	.L551
	movq	0(%r13), %rax
	movq	8(%rbx), %rdx
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	movq	(%rdx), %rsi
	cmpq	%rsi, %rax
	je	.L553
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L554
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	je	.L551
.L554:
	movq	%r10, -96(%rbp)
	movq	%r11, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal6String10SlowEqualsES1_@PLT
	movq	-80(%rbp), %rdi
	movq	-88(%rbp), %r11
	testb	%al, %al
	movq	-96(%rbp), %r10
	je	.L551
.L553:
	movq	(%r11), %rax
	xorl	%r14d, %r14d
	testq	%rax, %rax
	je	.L549
.L557:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L570
	addq	$72, %rsp
	movq	%r14, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L549:
	.cfi_restore_state
	movq	-72(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	$23, %rax
	jbe	.L571
	leaq	24(%rcx), %rax
	movq	%rax, 16(%rdi)
.L559:
	movq	$0, (%rcx)
	movq	%r12, %rdx
	movq	%r10, %rsi
	movq	%r15, %rdi
	movq	0(%r13), %rax
	movq	-104(%rbp), %r8
	movb	$1, %r14b
	movq	%rax, 8(%rcx)
	call	_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS1_17StringHandleEqualENS1_16StringHandleHashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L547:
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movl	%eax, %r12d
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L571:
	movl	$24, %esi
	movq	%r10, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %r10
	movq	%rax, %rcx
	jmp	.L559
.L570:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22696:
	.size	_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS1_17StringHandleEqualENS1_16StringHandleHashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS4_NS7_10_AllocNodeINS5_INS7_10_Hash_nodeIS4_Lb1EEEEEEEEESt4pairINS7_14_Node_iteratorIS4_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm, .-_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS1_17StringHandleEqualENS1_16StringHandleHashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS4_NS7_10_AllocNodeINS5_INS7_10_Hash_nodeIS4_Lb1EEEEEEEEESt4pairINS7_14_Node_iteratorIS4_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	.section	.text._ZN2v88internal16SourceTextModule13ResolveExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEbPNS0_6Module10ResolveSetE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16SourceTextModule13ResolveExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEbPNS0_6Module10ResolveSetE
	.type	_ZN2v88internal16SourceTextModule13ResolveExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEbPNS0_6Module10ResolveSetE, @function
_ZN2v88internal16SourceTextModule13ResolveExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEbPNS0_6Module10ResolveSetE:
.LFB18393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -104(%rbp)
	movq	%rdx, -128(%rbp)
	movl	%r8d, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rcx, %rsi
	movq	7(%rax), %rax
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE6LookupENS0_6HandleINS0_6ObjectEEE@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L573
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L576
.L579:
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	call	_ZNSt10_HashtableIN2v88internal6HandleINS1_6ModuleEEESt4pairIKS4_PNS1_18UnorderedStringSetEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17ModuleHandleEqualENS1_16ModuleHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS9_EEES5_INSC_14_Node_iteratorIS9_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	movq	%rax, -112(%rbp)
	testb	%dl, %dl
	je	.L637
	movq	64(%r14), %rdx
	movq	16(%rdx), %rcx
	movq	24(%rdx), %rax
	subq	%rcx, %rax
	cmpq	$63, %rax
	jbe	.L638
	leaq	64(%rcx), %rax
	movq	%rax, 16(%rdx)
.L582:
	leaq	56(%rcx), %r9
	movq	%rdx, (%rcx)
	leaq	40(%rcx), %rdi
	movl	$2, %esi
	movq	%r9, 8(%rcx)
	movq	$1, 16(%rcx)
	movq	$0, 24(%rcx)
	movq	$0, 32(%rcx)
	movl	$0x3f800000, 40(%rcx)
	movq	$0, 48(%rcx)
	movq	$0, 56(%rcx)
	movq	%rcx, -144(%rbp)
	movq	%rcx, -136(%rbp)
	movq	%r9, -152(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy11_M_next_bktEm@PLT
	movq	-136(%rbp), %rcx
	movq	-144(%rbp), %r10
	movq	%rax, %r8
	cmpq	16(%rcx), %rax
	jbe	.L583
	cmpq	$1, %rax
	movq	-152(%rbp), %r9
	je	.L639
	movq	(%rcx), %rdi
	leaq	0(,%rax,8), %rdx
	movq	%rdx, %rsi
	movq	16(%rdi), %r9
	movq	24(%rdi), %rax
	subq	%r9, %rax
	cmpq	%rax, %rdx
	ja	.L640
	addq	%r9, %rsi
	movq	%rsi, 16(%rdi)
.L587:
	movq	%r9, %rdi
	xorl	%esi, %esi
	movq	%r8, -152(%rbp)
	movq	%r10, -144(%rbp)
	movq	%rcx, -136(%rbp)
	call	memset@PLT
	movq	-152(%rbp), %r8
	movq	-144(%rbp), %r10
	movq	-136(%rbp), %rcx
	movq	%rax, %r9
.L585:
	movq	%r9, 8(%rcx)
	movq	%r8, 16(%rcx)
.L583:
	movq	-112(%rbp), %rax
	movq	%rcx, 16(%rax)
.L588:
	leaq	-104(%rbp), %rsi
	movl	$1, %ecx
	movq	%r15, %rdx
	movq	%r10, %rdi
	movq	%r10, -96(%rbp)
	call	_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS1_17StringHandleEqualENS1_16StringHandleHashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS4_NS7_10_AllocNodeINS5_INS7_10_Hash_nodeIS4_Lb1EEEEEEEEESt4pairINS7_14_Node_iteratorIS4_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L641
.L603:
	pushq	40(%rbp)
	movzbl	-120(%rbp), %r8d
	movq	%r13, %rsi
	movq	%r14, %r9
	movq	-104(%rbp), %rcx
	pushq	32(%rbp)
	movq	%r12, %rdi
	movq	-128(%rbp), %rdx
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	_ZN2v88internal16SourceTextModule29ResolveExportUsingStarExportsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEbPNS0_6Module10ResolveSetE
	addq	$32, %rsp
	movq	%rax, %r13
.L580:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L642
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L641:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$98, 11(%rdx)
	jne	.L603
	movq	41112(%r12), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L605
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r10
.L606:
	movq	0(%r13), %rax
	movq	41112(%r12), %rdi
	movq	79(%rax), %r8
	testq	%rdi, %rdi
	je	.L608
	movq	%r8, %rsi
	movq	%r10, -112(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-112(%rbp), %r10
	movq	%rax, %rsi
.L609:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	%r10, -112(%rbp)
	movslq	59(%rax), %rcx
	movslq	51(%rax), %rdx
	call	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEEii@PLT
	movq	(%rbx), %rax
	movq	-112(%rbp), %r10
	movq	%r14, %r9
	movl	$1, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movslq	35(%rax), %rcx
	pushq	-72(%rbp)
	movq	%r10, %rdx
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	call	_ZN2v88internal16SourceTextModule13ResolveImportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEEiNS0_15MessageLocationEbPNS0_6Module10ResolveSetE
	addq	$32, %rsp
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L643
	movq	0(%r13), %rax
	movq	41112(%r12), %rdi
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L613
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L614:
	movq	-104(%rbp), %rsi
	movq	%rbx, %rdx
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE3PutENS0_6HandleIS2_EENS5_INS0_6ObjectEEES8_@PLT
	movq	0(%r13), %r13
	movq	(%rax), %r12
	leaq	7(%r13), %r15
	movq	%r12, 7(%r13)
	testb	$1, %r12b
	je	.L619
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	je	.L617
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
.L617:
	testb	$24, %al
	je	.L619
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L619
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L619:
	movq	%rbx, %r13
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L573:
	movq	41088(%r12), %rbx
	cmpq	%rbx, 41096(%r12)
	je	.L644
.L575:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	movq	(%rbx), %rax
	testb	$1, %al
	je	.L579
.L576:
	movq	-1(%rax), %rax
	cmpw	$151, 11(%rax)
	jne	.L579
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L637:
	movq	16(%rax), %r10
	movq	-104(%rbp), %rax
	movq	(%rax), %rax
	movl	7(%rax), %r8d
	testb	$1, %r8b
	jne	.L589
	shrl	$2, %r8d
.L590:
	movq	%r8, %rax
	xorl	%edx, %edx
	divq	16(%r10)
	movq	8(%r10), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L591
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.L591
	xorl	%r9d, %r9d
	movq	%rbx, -152(%rbp)
	movq	16(%rcx), %rsi
	movq	%rcx, %rbx
	movq	%r12, -144(%rbp)
	movq	%r15, %rcx
	movq	%rdx, %r12
	movq	%r9, %r15
	movq	%r13, -160(%rbp)
	movq	%r10, %r13
	movq	%r14, -168(%rbp)
	movq	%r8, %r14
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L597:
	testq	%r15, %r15
	jne	.L635
.L598:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L600
	movq	16(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	16(%r13)
	cmpq	%rdx, %r12
	jne	.L600
.L601:
	cmpq	%rsi, %r14
	jne	.L597
	movq	-104(%rbp), %rax
	movq	8(%rbx), %rdx
	movq	(%rax), %rax
	movq	%rax, -96(%rbp)
	movq	(%rdx), %rsi
	cmpq	%rsi, %rax
	je	.L595
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L596
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	je	.L597
.L596:
	movq	%rcx, %rdi
	movq	%rcx, -136(%rbp)
	call	_ZN2v88internal6String10SlowEqualsES1_@PLT
	movq	-136(%rbp), %rcx
	testb	%al, %al
	je	.L597
.L595:
	addq	$1, %r15
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L589:
	movq	%r15, %rdi
	movq	%r10, -136(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-136(%rbp), %r10
	movl	%eax, %r8d
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L600:
	movq	%r15, %r9
	movq	-152(%rbp), %rbx
	movq	-144(%rbp), %r12
	movq	%rcx, %r15
	movq	-160(%rbp), %r13
	movq	-168(%rbp), %r14
	testq	%r9, %r9
	je	.L591
.L599:
	xorl	%r13d, %r13d
	cmpb	$0, -120(%rbp)
	je	.L580
	movq	-104(%rbp), %rdx
	movq	-128(%rbp), %rcx
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movl	$43, %esi
	call	_ZN2v88internal7Factory14NewSyntaxErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	leaq	16(%rbp), %rdx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L591:
	movq	-112(%rbp), %rax
	movq	16(%rax), %r10
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L644:
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L638:
	movq	%rdx, %rdi
	movl	$64, %esi
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-136(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L605:
	movq	41088(%r12), %r10
	cmpq	41096(%r12), %r10
	je	.L645
.L607:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r10)
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L635:
	movq	-144(%rbp), %r12
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L608:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L646
.L610:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L613:
	movq	41088(%r12), %rdi
	cmpq	41096(%r12), %rdi
	je	.L647
.L615:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdi)
	jmp	.L614
	.p2align 4,,10
	.p2align 3
.L639:
	movq	$0, 56(%rcx)
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L640:
	movq	%rdx, -160(%rbp)
	movq	%r8, -152(%rbp)
	movq	%r10, -144(%rbp)
	movq	%rcx, -136(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-136(%rbp), %rcx
	movq	-144(%rbp), %r10
	movq	-152(%rbp), %r8
	movq	-160(%rbp), %rdx
	movq	%rax, %r9
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L643:
	xorl	%r13d, %r13d
	jmp	.L580
.L646:
	movq	%r12, %rdi
	movq	%r8, -120(%rbp)
	movq	%r10, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %r8
	movq	-112(%rbp), %r10
	movq	%rax, %rsi
	jmp	.L610
.L645:
	movq	%r12, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	%rax, %r10
	jmp	.L607
.L647:
	movq	%r12, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L615
.L642:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18393:
	.size	_ZN2v88internal16SourceTextModule13ResolveExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEbPNS0_6Module10ResolveSetE, .-_ZN2v88internal16SourceTextModule13ResolveExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEbPNS0_6Module10ResolveSetE
	.section	.text._ZN2v88internal16SourceTextModule17FinishInstantiateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListIS5_EEPjPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16SourceTextModule17FinishInstantiateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListIS5_EEPjPNS0_4ZoneE
	.type	_ZN2v88internal16SourceTextModule17FinishInstantiateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListIS5_EEPjPNS0_4ZoneE, @function
_ZN2v88internal16SourceTextModule17FinishInstantiateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListIS5_EEPjPNS0_4ZoneE:
.LFB18414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$232, %rsp
	movq	%rdx, -168(%rbp)
	movq	41112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	47(%rax), %rsi
	testq	%rdi, %rdi
	je	.L649
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L650:
	movq	12464(%r15), %rax
	movq	39(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L652
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L653:
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	movq	(%rax), %r14
	leaq	47(%rdi), %rsi
	movq	%r14, 47(%rdi)
	testb	$1, %r14b
	je	.L732
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -176(%rbp)
	testl	$262144, %eax
	jne	.L780
	testb	$24, %al
	je	.L732
.L789:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L781
	.p2align 4,,10
	.p2align 3
.L732:
	movq	(%rbx), %rax
	movl	$2, %esi
	movq	%rax, -128(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal6Module9SetStatusENS1_6StatusE@PLT
	movslq	0(%r13), %rax
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 95(%rdx)
	movslq	0(%r13), %rax
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 103(%rdx)
	movq	-168(%rbp), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L782
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L659:
	movq	-168(%rbp), %rcx
	movq	$0, (%rax)
	movq	%rbx, 8(%rax)
	movq	8(%rcx), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 8(%rcx)
	addl	$1, 0(%r13)
	movq	(%rbx), %rax
	movq	41112(%r15), %rdi
	movq	71(%rax), %rsi
	testq	%rdi, %rdi
	je	.L660
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -176(%rbp)
	movq	(%rax), %rsi
.L661:
	movslq	11(%rsi), %rax
	testq	%rax, %rax
	jle	.L663
	subl	$1, %eax
	movl	$16, %r14d
	movq	%rbx, -200(%rbp)
	leaq	24(,%rax,8), %rax
	movq	%r14, %rbx
	movq	%r13, %r14
	movq	%rax, -184(%rbp)
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L783:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L665:
	movq	-168(%rbp), %rdx
	movq	%r12, %r8
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Module17FinishInstantiateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListINS4_INS0_16SourceTextModuleEEEEEPjPNS0_4ZoneE@PLT
	testb	%al, %al
	je	.L648
	movq	0(%r13), %rax
	cmpl	$2, 27(%rax)
	jne	.L668
	movq	-200(%rbp), %rcx
	movq	(%rcx), %rdx
	movslq	107(%rax), %rcx
	movslq	107(%rdx), %rax
	cmpl	%ecx, %eax
	cmovge	%ecx, %eax
	salq	$32, %rax
	movq	%rax, 103(%rdx)
.L668:
	addq	$8, %rbx
	cmpq	%rbx, -184(%rbp)
	je	.L771
	movq	-176(%rbp), %rax
	movq	(%rax), %rsi
.L669:
	movq	-1(%rsi,%rbx), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	jne	.L783
	movq	41088(%r15), %r13
	cmpq	41096(%r15), %r13
	je	.L784
.L666:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, 0(%r13)
	jmp	.L665
.L773:
	movq	-208(%rbp), %rbx
.L708:
	movq	(%rbx), %rax
	movl	107(%rax), %ecx
	cmpl	%ecx, 99(%rax)
	je	.L774
.L730:
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L648:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L785
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L784:
	.cfi_restore_state
	movq	%r15, %rdi
	movq	%rsi, -208(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-208(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L771:
	movq	-200(%rbp), %rbx
.L663:
	movq	(%rbx), %rax
	movq	41112(%r15), %rdi
	movq	79(%rax), %rsi
	testq	%rdi, %rdi
	je	.L670
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -184(%rbp)
	movq	(%rbx), %rax
	movq	%rax, -160(%rbp)
	cmpl	$3, 27(%rax)
	jle	.L673
.L791:
	movq	41112(%r15), %rdi
	movq	47(%rax), %rsi
	testq	%rdi, %rdi
	je	.L678
.L792:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -248(%rbp)
.L679:
	movq	(%rax), %rax
	movq	47(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L681
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -224(%rbp)
	movq	(%rax), %rsi
.L682:
	movslq	11(%rsi), %rax
	testq	%rax, %rax
	jle	.L684
	subl	$1, %eax
	movq	%r12, -176(%rbp)
	movl	$16, %r13d
	leaq	24(,%rax,8), %rax
	movq	%rax, -232(%rbp)
	leaq	-160(%rbp), %rax
	movq	%rax, -208(%rbp)
	leaq	-72(%rbp), %rax
	movq	%rax, -200(%rbp)
	leaq	-88(%rbp), %rax
	movq	%rax, -216(%rbp)
.L704:
	movq	-1(%rsi,%r13), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L685
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L686:
	movq	41112(%r15), %rdi
	movq	23(%rsi), %rsi
	testq	%rdi, %rdi
	je	.L688
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L689:
	movq	(%r14), %rax
	movq	-184(%rbp), %rsi
	movq	-208(%rbp), %rdi
	movslq	59(%rax), %rcx
	movslq	51(%rax), %rdx
	call	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEEii@PLT
	movq	-176(%rbp), %rax
	movq	-216(%rbp), %rdi
	movl	$2, %esi
	movq	$1, -112(%rbp)
	movq	%rax, -128(%rbp)
	movq	-200(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	%rax, -120(%rbp)
	movq	$0, -96(%rbp)
	movl	$0x3f800000, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy11_M_next_bktEm@PLT
	movq	%rax, %rcx
	cmpq	-112(%rbp), %rax
	jbe	.L691
	cmpq	$1, %rax
	je	.L786
	movq	-128(%rbp), %rdi
	leaq	0(,%rax,8), %rdx
	movq	%rdx, %rsi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r8
	subq	%rax, %r8
	cmpq	%r8, %rdx
	ja	.L787
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L695:
	movq	%rax, %rdi
	xorl	%esi, %esi
	movq	%rcx, -240(%rbp)
	call	memset@PLT
	movq	-240(%rbp), %rcx
	movq	%rax, %rdi
.L693:
	movq	%rdi, -120(%rbp)
	movq	%rcx, -112(%rbp)
.L691:
	movq	-176(%rbp), %rax
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	-192(%rbp), %r9
	movl	$1, %r8d
	movq	%rax, -64(%rbp)
	movq	(%r14), %rax
	movslq	35(%rax), %rcx
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-152(%rbp)
	pushq	-160(%rbp)
	call	_ZN2v88internal16SourceTextModule13ResolveImportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEEiNS0_15MessageLocationEbPNS0_6Module10ResolveSetE
	addq	$32, %rsp
	testq	%rax, %rax
	je	.L788
	movq	(%rbx), %rdx
	movq	63(%rdx), %rdi
	movq	(%rax), %rdx
	movq	(%r14), %rax
	movslq	43(%rax), %rax
	notl	%eax
	leal	16(,%rax,8), %eax
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L731
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -240(%rbp)
	testl	$262144, %eax
	je	.L700
	movq	%rdx, -272(%rbp)
	movq	%rsi, -264(%rbp)
	movq	%rdi, -256(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-240(%rbp), %rcx
	movq	-272(%rbp), %rdx
	movq	-264(%rbp), %rsi
	movq	-256(%rbp), %rdi
	movq	8(%rcx), %rax
.L700:
	testb	$24, %al
	je	.L731
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L731
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L731:
	movq	-104(%rbp), %rax
	testq	%rax, %rax
	je	.L702
	.p2align 4,,10
	.p2align 3
.L703:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L703
.L702:
	movq	-112(%rbp), %rax
	movq	-120(%rbp), %rdi
	xorl	%esi, %esi
	addq	$8, %r13
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	cmpq	%r13, -232(%rbp)
	je	.L772
	movq	-224(%rbp), %rax
	movq	(%rax), %rsi
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L780:
	movq	%r14, %rdx
	movq	%rsi, -192(%rbp)
	movq	%rdi, -184(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-176(%rbp), %rcx
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L789
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L670:
	movq	41088(%r15), %rax
	movq	%rax, -184(%rbp)
	cmpq	41096(%r15), %rax
	je	.L790
.L672:
	movq	-184(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rdx)
	movq	(%rbx), %rax
	movq	%rax, -160(%rbp)
	cmpl	$3, 27(%rax)
	jg	.L791
.L673:
	leaq	-160(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNK2v88internal16SourceTextModule21GetSharedFunctionInfoEv
	movq	15(%rax), %rdx
	testb	$1, %dl
	jne	.L675
.L677:
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal9ScopeInfo5EmptyEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
.L676:
	movq	-192(%rbp), %rdi
	movq	%rdx, -128(%rbp)
	call	_ZNK2v88internal9ScopeInfo20ModuleDescriptorInfoEv@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	jne	.L792
.L678:
	movq	41088(%r15), %rax
	movq	%rax, -248(%rbp)
	cmpq	41096(%r15), %rax
	je	.L793
.L680:
	movq	-248(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rcx, %rax
	movq	%rsi, (%rcx)
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L688:
	movq	41088(%r15), %r12
	cmpq	41096(%r15), %r12
	je	.L794
.L690:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r12)
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L685:
	movq	41088(%r15), %r14
	cmpq	41096(%r15), %r14
	je	.L795
.L687:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	jmp	.L686
	.p2align 4,,10
	.p2align 3
.L652:
	movq	41088(%r15), %rdx
	cmpq	41096(%r15), %rdx
	je	.L796
.L654:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rdx)
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L649:
	movq	41088(%r15), %r14
	cmpq	%r14, 41096(%r15)
	je	.L797
.L651:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L660:
	movq	41088(%r15), %rax
	movq	%rax, -176(%rbp)
	cmpq	%rax, 41096(%r15)
	je	.L798
.L662:
	movq	-176(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rcx)
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L788:
	movq	-104(%rbp), %rax
	testq	%rax, %rax
	je	.L779
	.p2align 4,,10
	.p2align 3
.L698:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L698
.L779:
	movq	-112(%rbp), %rax
	movq	-120(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	xorl	%eax, %eax
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L681:
	movq	41088(%r15), %rax
	movq	%rax, -224(%rbp)
	cmpq	41096(%r15), %rax
	je	.L799
.L683:
	movq	-224(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rdx)
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L781:
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L795:
	movq	%r15, %rdi
	movq	%rsi, -240(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-240(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L794:
	movq	%r15, %rdi
	movq	%rsi, -240(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-240(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L772:
	movq	-176(%rbp), %r12
.L684:
	movq	-248(%rbp), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L705
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -200(%rbp)
	movq	(%rax), %rsi
.L706:
	movslq	11(%rsi), %rax
	testq	%rax, %rax
	jle	.L708
	subl	$1, %eax
	movq	%rbx, -208(%rbp)
	movl	$16, %r13d
	leaq	24(,%rax,8), %rax
	movq	%rax, -176(%rbp)
	.p2align 4,,10
	.p2align 3
.L727:
	movq	-1(%r13,%rsi), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L709
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
.L710:
	movq	41112(%r15), %rdi
	movq	7(%rsi), %rsi
	testq	%rdi, %rdi
	je	.L712
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L713:
	cmpq	%rsi, 88(%r15)
	je	.L716
	movq	(%rbx), %rax
	movq	-184(%rbp), %rsi
	leaq	-72(%rbp), %rbx
	leaq	-160(%rbp), %rdi
	movslq	59(%rax), %rcx
	movslq	51(%rax), %rdx
	call	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEEii@PLT
	leaq	-88(%rbp), %rdi
	movl	$2, %esi
	movq	%r12, -128(%rbp)
	movq	%rbx, -120(%rbp)
	movq	$1, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movl	$0x3f800000, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy11_M_next_bktEm@PLT
	movq	%rax, %rcx
	cmpq	-112(%rbp), %rax
	jbe	.L717
	cmpq	$1, %rax
	je	.L800
	movq	-128(%rbp), %rdi
	leaq	0(,%rax,8), %r8
	movq	%r8, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %r8
	ja	.L801
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L721:
	movq	%rdx, %rdi
	xorl	%esi, %esi
	movq	%r8, %rdx
	movq	%rcx, -216(%rbp)
	call	memset@PLT
	movq	-216(%rbp), %rcx
	movq	%rax, %rdi
.L719:
	movq	%rdi, -120(%rbp)
	movq	%rcx, -112(%rbp)
.L717:
	pushq	-136(%rbp)
	xorl	%edx, %edx
	movq	%r14, %rcx
	movq	%r15, %rdi
	pushq	-144(%rbp)
	movl	$1, %r8d
	movq	-192(%rbp), %r9
	pushq	-152(%rbp)
	movq	-208(%rbp), %rsi
	pushq	-160(%rbp)
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal16SourceTextModule13ResolveExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEbPNS0_6Module10ResolveSetE
	addq	$32, %rsp
	testq	%rax, %rax
	movq	-104(%rbp), %rax
	jne	.L778
	jmp	.L777
	.p2align 4,,10
	.p2align 3
.L726:
	movq	(%rax), %rax
.L778:
	testq	%rax, %rax
	jne	.L726
	movq	-112(%rbp), %rax
	movq	-120(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
.L716:
	addq	$8, %r13
	cmpq	-176(%rbp), %r13
	je	.L773
	movq	-200(%rbp), %rax
	movq	(%rax), %rsi
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L724:
	movq	(%rax), %rax
.L777:
	testq	%rax, %rax
	jne	.L724
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L712:
	movq	41088(%r15), %r14
	cmpq	41096(%r15), %r14
	je	.L802
.L714:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L709:
	movq	41088(%r15), %rbx
	cmpq	41096(%r15), %rbx
	je	.L803
.L711:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rbx)
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L802:
	movq	%r15, %rdi
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L803:
	movq	%r15, %rdi
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L786:
	movq	$0, -72(%rbp)
	movq	-200(%rbp), %rdi
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L782:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L787:
	movq	%rdx, -256(%rbp)
	movq	%rcx, -240(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-240(%rbp), %rcx
	movq	-256(%rbp), %rdx
	jmp	.L695
.L798:
	movq	%r15, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rsi
	movq	%rax, -176(%rbp)
	jmp	.L662
.L797:
	movq	%r15, %rdi
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L651
.L796:
	movq	%r15, %rdi
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L654
.L705:
	movq	41088(%r15), %rax
	movq	%rax, -200(%rbp)
	cmpq	41096(%r15), %rax
	je	.L804
.L707:
	movq	-200(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rcx)
	jmp	.L706
.L675:
	movq	-1(%rdx), %rcx
	cmpw	$136, 11(%rcx)
	jne	.L677
	jmp	.L676
.L799:
	movq	%r15, %rdi
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	movq	%rax, -224(%rbp)
	jmp	.L683
.L790:
	movq	%r15, %rdi
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	movq	%rax, -184(%rbp)
	jmp	.L672
.L793:
	movq	%r15, %rdi
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	movq	%rax, -248(%rbp)
	jmp	.L680
.L804:
	movq	%r15, %rdi
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	movq	%rax, -200(%rbp)
	jmp	.L707
.L800:
	movq	$0, -72(%rbp)
	movq	%rbx, %rdi
	jmp	.L719
.L801:
	movq	%r8, -224(%rbp)
	movq	%rcx, -216(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-216(%rbp), %rcx
	movq	-224(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L721
.L774:
	movq	-168(%rbp), %r12
	movq	-192(%rbp), %r13
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L805:
	movq	(%r14), %rax
	movl	$3, %esi
	movq	%r13, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal6Module9SetStatusENS1_6StatusE@PLT
	movq	(%r14), %rax
	cmpq	%rax, (%rbx)
	je	.L730
.L728:
	movq	8(%r12), %rax
	movq	%r15, %rdi
	movq	8(%rax), %r14
	movq	(%rax), %rax
	movq	%rax, 8(%r12)
	movq	%r14, %rsi
	call	_ZN2v88internal16SourceTextModule21RunInitializationCodeEPNS0_7IsolateENS0_6HandleIS1_EE
	testb	%al, %al
	jne	.L805
	jmp	.L648
.L785:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18414:
	.size	_ZN2v88internal16SourceTextModule17FinishInstantiateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListIS5_EEPjPNS0_4ZoneE, .-_ZN2v88internal16SourceTextModule17FinishInstantiateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListIS5_EEPjPNS0_4ZoneE
	.section	.text._ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEESt4pairIKS4_NS2_INS1_6ObjectEEEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17StringHandleEqualENS1_16StringHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSC_10_Hash_nodeIS9_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEESt4pairIKS4_NS2_INS1_6ObjectEEEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17StringHandleEqualENS1_16StringHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSC_10_Hash_nodeIS9_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEESt4pairIKS4_NS2_INS1_6ObjectEEEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17StringHandleEqualENS1_16StringHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSC_10_Hash_nodeIS9_Lb1EEEm
	.type	_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEESt4pairIKS4_NS2_INS1_6ObjectEEEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17StringHandleEqualENS1_16StringHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSC_10_Hash_nodeIS9_Lb1EEEm, @function
_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEESt4pairIKS4_NS2_INS1_6ObjectEEEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17StringHandleEqualENS1_16StringHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSC_10_Hash_nodeIS9_Lb1EEEm:
.LFB23092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$40, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L830
.L807:
	movq	%r14, 24(%r12)
	movq	8(%rbx), %rax
	leaq	0(,%r15,8), %rcx
	movq	(%rax,%r15,8), %rax
	testq	%rax, %rax
	je	.L816
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r12, (%rax)
.L817:
	addq	$1, 32(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L830:
	.cfi_restore_state
	movq	%rdx, %r13
	cmpq	$1, %rdx
	je	.L831
	movq	(%rbx), %r8
	leaq	0(,%rdx,8), %rdx
	movq	%rdx, %rsi
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L832
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L811:
	xorl	%esi, %esi
	call	memset@PLT
	movq	%rax, %rdi
.L809:
	movq	24(%rbx), %rsi
	movq	$0, 24(%rbx)
	testq	%rsi, %rsi
	je	.L812
	xorl	%r9d, %r9d
	leaq	24(%rbx), %r10
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L814:
	movq	(%r8), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L815:
	testq	%rsi, %rsi
	je	.L812
.L813:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	24(%rcx), %rax
	divq	%r13
	leaq	(%rdi,%rdx,8), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	jne	.L814
	movq	24(%rbx), %r8
	movq	%r8, (%rcx)
	movq	%rcx, 24(%rbx)
	movq	%r10, (%rax)
	cmpq	$0, (%rcx)
	je	.L819
	movq	%rcx, (%rdi,%r9,8)
	movq	%rdx, %r9
	testq	%rsi, %rsi
	jne	.L813
	.p2align 4,,10
	.p2align 3
.L812:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r13, 16(%rbx)
	divq	%r13
	movq	%rdi, 8(%rbx)
	movq	%rdx, %r15
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L816:
	movq	24(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 24(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L818
	movq	24(%rax), %rax
	xorl	%edx, %edx
	divq	16(%rbx)
	movq	8(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L818:
	movq	8(%rbx), %rax
	leaq	24(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L817
	.p2align 4,,10
	.p2align 3
.L819:
	movq	%rdx, %r9
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L831:
	movq	$0, 56(%rbx)
	leaq	56(%rbx), %rdi
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L832:
	movq	%r8, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L811
	.cfi_endproc
.LFE23092:
	.size	_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEESt4pairIKS4_NS2_INS1_6ObjectEEEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17StringHandleEqualENS1_16StringHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSC_10_Hash_nodeIS9_Lb1EEEm, .-_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEESt4pairIKS4_NS2_INS1_6ObjectEEEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17StringHandleEqualENS1_16StringHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSC_10_Hash_nodeIS9_Lb1EEEm
	.section	.text._ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEESt4pairIKS4_NS2_INS1_6ObjectEEEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17StringHandleEqualENS1_16StringHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS5_IS4_NS2_INS1_4CellEEEEEEES5_INSC_14_Node_iteratorIS9_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEESt4pairIKS4_NS2_INS1_6ObjectEEEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17StringHandleEqualENS1_16StringHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS5_IS4_NS2_INS1_4CellEEEEEEES5_INSC_14_Node_iteratorIS9_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEESt4pairIKS4_NS2_INS1_6ObjectEEEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17StringHandleEqualENS1_16StringHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS5_IS4_NS2_INS1_4CellEEEEEEES5_INSC_14_Node_iteratorIS9_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	.type	_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEESt4pairIKS4_NS2_INS1_6ObjectEEEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17StringHandleEqualENS1_16StringHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS5_IS4_NS2_INS1_4CellEEEEEEES5_INSC_14_Node_iteratorIS9_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_, @function
_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEESt4pairIKS4_NS2_INS1_6ObjectEEEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17StringHandleEqualENS1_16StringHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS5_IS4_NS2_INS1_4CellEEEEEEES5_INSC_14_Node_iteratorIS9_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_:
.LFB22744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$31, %rax
	jbe	.L857
	leaq	32(%r14), %rax
	movq	%rax, 16(%rdi)
.L835:
	movq	$0, (%r14)
	movq	(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	%rax, 8(%r14)
	movq	%rdx, 16(%r14)
	movq	(%rax), %rax
	movl	7(%rax), %r12d
	testb	$1, %r12b
	jne	.L836
	shrl	$2, %r12d
.L837:
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	16(%r13)
	movq	8(%r13), %rax
	movq	(%rax,%rdx,8), %r8
	movq	%rdx, %r9
	testq	%r8, %r8
	je	.L838
	movq	(%r8), %rbx
	leaq	-64(%rbp), %rdi
	movq	24(%rbx), %rcx
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L840:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L838
	movq	24(%rsi), %rcx
	xorl	%edx, %edx
	movq	%rbx, %r8
	movq	%rcx, %rax
	divq	16(%r13)
	cmpq	%rdx, %r9
	jne	.L838
	movq	%rsi, %rbx
.L845:
	cmpq	%rcx, %r12
	jne	.L840
	movq	8(%r14), %rax
	movq	8(%rbx), %rdx
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	movq	(%rdx), %rsi
	cmpq	%rsi, %rax
	je	.L842
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L843
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	je	.L840
.L843:
	movq	%r9, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal6String10SlowEqualsES1_@PLT
	movq	-72(%rbp), %rdi
	movq	-80(%rbp), %r8
	testb	%al, %al
	movq	-88(%rbp), %r9
	je	.L840
.L842:
	movq	(%r8), %rax
	xorl	%r15d, %r15d
	testq	%rax, %rax
	je	.L838
.L846:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L858
	addq	$56, %rsp
	movq	%r15, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L838:
	.cfi_restore_state
	movl	$1, %r8d
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r9, %rsi
	movq	%r13, %rdi
	movb	$1, %r15b
	call	_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEESt4pairIKS4_NS2_INS1_6ObjectEEEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17StringHandleEqualENS1_16StringHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSC_10_Hash_nodeIS9_Lb1EEEm
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L836:
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movl	%eax, %r12d
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L857:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L835
.L858:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22744:
	.size	_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEESt4pairIKS4_NS2_INS1_6ObjectEEEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17StringHandleEqualENS1_16StringHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS5_IS4_NS2_INS1_4CellEEEEEEES5_INSC_14_Node_iteratorIS9_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_, .-_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEESt4pairIKS4_NS2_INS1_6ObjectEEEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17StringHandleEqualENS1_16StringHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS5_IS4_NS2_INS1_4CellEEEEEEES5_INSC_14_Node_iteratorIS9_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	.section	.text._ZN2v88internal16SourceTextModule16FetchStarExportsEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_4ZoneEPNS0_18UnorderedModuleSetE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16SourceTextModule16FetchStarExportsEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_4ZoneEPNS0_18UnorderedModuleSetE
	.type	_ZN2v88internal16SourceTextModule16FetchStarExportsEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_4ZoneEPNS0_18UnorderedModuleSetE, @function
_ZN2v88internal16SourceTextModule16FetchStarExportsEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_4ZoneEPNS0_18UnorderedModuleSetE:
.LFB18418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -216(%rbp)
	movq	%rcx, -232(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	31(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$1027, 11(%rax)
	je	.L859
	movq	%rcx, -128(%rbp)
	movq	%rdi, %rbx
	movq	%rsi, %r14
	movq	%rcx, %rdi
	leaq	-144(%rbp), %rax
	movq	%rsi, -144(%rbp)
	leaq	-128(%rbp), %rdx
	movl	$1, %ecx
	movq	%rax, %rsi
	movq	%rdx, -248(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZNSt10_HashtableIN2v88internal6HandleINS1_6ModuleEEES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS1_17ModuleHandleEqualENS1_16ModuleHandleHashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS4_NS7_10_AllocNodeINS5_INS7_10_Hash_nodeIS4_Lb1EEEEEEEEESt4pairINS7_14_Node_iteratorIS4_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	testb	%dl, %dl
	je	.L859
	movq	(%r14), %rax
	movq	41112(%rbx), %rdi
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L861
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L862:
	movq	-216(%rbp), %rax
	leaq	-72(%rbp), %r13
	leaq	-88(%rbp), %rdi
	movl	$2, %esi
	movq	%r13, -120(%rbp)
	movq	%rax, -128(%rbp)
	movq	$1, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movl	$0x3f800000, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy11_M_next_bktEm@PLT
	movq	%rax, %r12
	cmpq	-112(%rbp), %rax
	jbe	.L864
	cmpq	$1, %rax
	je	.L937
	movq	-128(%rbp), %rdi
	leaq	0(,%rax,8), %rdx
	movq	%rdx, %rsi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	%rax, %rdx
	ja	.L938
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L868:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	memset@PLT
.L866:
	movq	%r13, -120(%rbp)
	movq	%r12, -112(%rbp)
.L864:
	leaq	56(%rbx), %rax
	movq	%rax, %r12
	movq	(%r14), %rax
	movq	%rax, -152(%rbp)
	cmpl	$3, 27(%rax)
	jle	.L869
	movq	47(%rax), %rax
.L870:
	movq	23(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L874
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -184(%rbp)
.L875:
	movq	(%rax), %rax
	movslq	11(%rax), %rdx
	testq	%rdx, %rdx
	jle	.L877
	subl	$1, %edx
	movq	%r14, -240(%rbp)
	movl	$16, %r13d
	leaq	24(,%rdx,8), %rcx
	movq	%r15, -224(%rbp)
	movq	%rbx, %r15
	movq	%rcx, -208(%rbp)
	leaq	88(%rbx), %rcx
	movq	%r12, %rbx
	movq	%rcx, -264(%rbp)
.L903:
	movq	-1(%rax,%r13), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L878
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	7(%rsi), %rax
	cmpq	%rax, 88(%r15)
	je	.L939
.L881:
	addq	$8, %r13
	cmpq	%r13, -208(%rbp)
	je	.L934
	movq	-184(%rbp), %rax
	movq	(%rax), %rax
	jmp	.L903
	.p2align 4,,10
	.p2align 3
.L934:
	movq	%r15, %rbx
	movq	-240(%rbp), %r14
	movq	-224(%rbp), %r15
.L877:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L904
	movq	88(%rbx), %rax
	movq	%r15, %rdi
.L907:
	movq	16(%r12), %rdx
	cmpq	(%rdx), %rax
	je	.L905
.L940:
	movq	8(%r12), %rsi
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE3PutENS0_6HandleIS2_EENS5_INS0_6ObjectEEES8_@PLT
	movq	(%r12), %r12
	movq	%rax, %rdi
	testq	%r12, %r12
	je	.L935
	movq	16(%r12), %rdx
	movq	88(%rbx), %rax
	cmpq	(%rdx), %rax
	jne	.L940
.L905:
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.L907
.L935:
	movq	%rdi, %r15
.L904:
	movq	(%r14), %r13
	movq	(%r15), %r12
	movq	%r12, 7(%r13)
	leaq	7(%r13), %r14
	testb	$1, %r12b
	je	.L914
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L909
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L909:
	testb	$24, %al
	je	.L914
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L914
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L914:
	movq	-104(%rbp), %rax
	testq	%rax, %rax
	je	.L911
	.p2align 4,,10
	.p2align 3
.L912:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L912
.L911:
	movq	-112(%rbp), %rax
	movq	-120(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
.L859:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L941
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L878:
	.cfi_restore_state
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L942
.L880:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	movq	7(%rsi), %rax
	cmpq	%rax, 88(%r15)
	jne	.L881
.L939:
	movq	-240(%rbp), %rcx
	movslq	35(%rsi), %rax
	movq	(%rcx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	71(%rdx), %rdx
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L882
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L883:
	movq	-1(%rsi), %rax
	cmpw	$119, 11(%rax)
	je	.L943
.L885:
	movq	(%r12), %rax
	movq	41112(%r15), %rdi
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L886
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -176(%rbp)
	movq	(%rax), %rsi
.L887:
	movslq	35(%rsi), %rax
	testq	%rax, %rax
	jle	.L881
	subl	$1, %eax
	movq	%r13, -256(%rbp)
	xorl	%r12d, %r12d
	movq	%rax, -192(%rbp)
	jmp	.L902
	.p2align 4,,10
	.p2align 3
.L945:
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
	movq	%rax, %r14
.L892:
	movq	2328(%r15), %r8
	movq	%r13, -144(%rbp)
	cmpq	%r13, %r8
	je	.L890
	movq	-1(%r13), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L895
	movq	-1(%r8), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	je	.L896
.L895:
	movq	-168(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal6String10SlowEqualsES1_@PLT
	testb	%al, %al
	jne	.L890
.L896:
	movq	-224(%rbp), %rax
	movq	-168(%rbp), %rdi
	movq	%r14, %rsi
	movq	(%rax), %rax
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE6LookupENS0_6HandleINS0_6ObjectEEE@PLT
	cmpq	%rax, 96(%r15)
	je	.L944
.L890:
	leaq	1(%r12), %rax
	cmpq	-192(%rbp), %r12
	je	.L933
	movq	-176(%rbp), %rcx
	movq	%rax, %r12
	movq	(%rcx), %rsi
.L902:
	movq	%r12, %rax
	movl	%r12d, -200(%rbp)
	movq	%rbx, %rdi
	salq	$4, %rax
	addq	%rax, %rsi
	movq	39(%rsi), %r13
	movq	%r13, %rsi
	call	_ZN2v88internal9HashTableINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE5IsKeyENS0_13ReadOnlyRootsENS0_6ObjectE@PLT
	testb	%al, %al
	je	.L890
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	jne	.L945
	movq	41088(%r15), %r14
	cmpq	41096(%r15), %r14
	je	.L946
.L893:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%r13, (%r14)
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L882:
	movq	41088(%r15), %r12
	cmpq	41096(%r15), %r12
	je	.L947
.L884:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r12)
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L944:
	movq	-176(%rbp), %rax
	movq	-168(%rbp), %rdi
	movl	%r12d, %esi
	movq	(%rax), %rax
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE7ValueAtEi@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L898
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L899:
	movq	%r14, %xmm0
	movq	%r13, %xmm1
	movq	-168(%rbp), %rsi
	movq	-248(%rbp), %rdi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -144(%rbp)
	call	_ZNSt10_HashtableIN2v88internal6HandleINS1_6StringEEESt4pairIKS4_NS2_INS1_6ObjectEEEENS1_13ZoneAllocatorIS9_EENSt8__detail10_Select1stENS1_17StringHandleEqualENS1_16StringHandleHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS5_IS4_NS2_INS1_4CellEEEEEEES5_INSC_14_Node_iteratorIS9_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	testb	%dl, %dl
	jne	.L890
	movq	16(%rax), %rdx
	movq	(%rdx), %rdx
	cmpq	0(%r13), %rdx
	je	.L890
	cmpq	88(%r15), %rdx
	je	.L890
	movq	-264(%rbp), %rcx
	movq	%rcx, 16(%rax)
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L933:
	movq	-256(%rbp), %r13
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L861:
	movq	41088(%rbx), %r15
	cmpq	41096(%rbx), %r15
	je	.L948
.L863:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r15)
	jmp	.L862
	.p2align 4,,10
	.p2align 3
.L874:
	movq	41088(%rbx), %rax
	movq	%rax, -184(%rbp)
	cmpq	%rax, 41096(%rbx)
	je	.L949
.L876:
	movq	-184(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rcx, %rax
	movq	%rsi, (%rcx)
	jmp	.L875
	.p2align 4,,10
	.p2align 3
.L869:
	leaq	-152(%rbp), %rdi
	call	_ZNK2v88internal16SourceTextModule21GetSharedFunctionInfoEv
	movq	15(%rax), %rdx
	testb	$1, %dl
	jne	.L871
.L873:
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal9ScopeInfo5EmptyEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
.L872:
	movq	-168(%rbp), %rdi
	movq	%rdx, -144(%rbp)
	call	_ZNK2v88internal9ScopeInfo20ModuleDescriptorInfoEv@PLT
	jmp	.L870
	.p2align 4,,10
	.p2align 3
.L942:
	movq	%r15, %rdi
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L886:
	movq	41088(%r15), %rax
	movq	%rax, -176(%rbp)
	cmpq	41096(%r15), %rax
	je	.L950
.L888:
	movq	-176(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rcx)
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L946:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r14
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L898:
	movq	41088(%r15), %r13
	cmpq	41096(%r15), %r13
	je	.L951
.L900:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, 0(%r13)
	jmp	.L899
.L937:
	movq	$0, -72(%rbp)
	jmp	.L866
	.p2align 4,,10
	.p2align 3
.L943:
	movq	-232(%rbp), %rcx
	movq	-216(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal16SourceTextModule16FetchStarExportsEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_4ZoneEPNS0_18UnorderedModuleSetE
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L871:
	movq	-1(%rdx), %rcx
	cmpw	$136, 11(%rcx)
	jne	.L873
	jmp	.L872
.L949:
	movq	%rbx, %rdi
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	movq	%rax, -184(%rbp)
	jmp	.L876
.L948:
	movq	%rbx, %rdi
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L863
.L950:
	movq	%r15, %rdi
	movq	%rsi, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-192(%rbp), %rsi
	movq	%rax, -176(%rbp)
	jmp	.L888
.L947:
	movq	%r15, %rdi
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L884
.L938:
	movq	%rdx, -176(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-176(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L868
.L951:
	movq	%r15, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L900
.L941:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18418:
	.size	_ZN2v88internal16SourceTextModule16FetchStarExportsEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_4ZoneEPNS0_18UnorderedModuleSetE, .-_ZN2v88internal16SourceTextModule16FetchStarExportsEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_4ZoneEPNS0_18UnorderedModuleSetE
	.section	.text.startup._GLOBAL__sub_I__ZNK2v88internal16SourceTextModule21GetSharedFunctionInfoEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZNK2v88internal16SourceTextModule21GetSharedFunctionInfoEv, @function
_GLOBAL__sub_I__ZNK2v88internal16SourceTextModule21GetSharedFunctionInfoEv:
.LFB23689:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23689:
	.size	_GLOBAL__sub_I__ZNK2v88internal16SourceTextModule21GetSharedFunctionInfoEv, .-_GLOBAL__sub_I__ZNK2v88internal16SourceTextModule21GetSharedFunctionInfoEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZNK2v88internal16SourceTextModule21GetSharedFunctionInfoEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
