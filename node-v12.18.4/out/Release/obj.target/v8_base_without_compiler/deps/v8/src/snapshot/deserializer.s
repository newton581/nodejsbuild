	.file	"deserializer.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4479:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4479:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4480:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4480:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZNK2v88internal29NativesExternalStringResource4dataEv,"axG",@progbits,_ZNK2v88internal29NativesExternalStringResource4dataEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal29NativesExternalStringResource4dataEv
	.type	_ZNK2v88internal29NativesExternalStringResource4dataEv, @function
_ZNK2v88internal29NativesExternalStringResource4dataEv:
.LFB21255:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE21255:
	.size	_ZNK2v88internal29NativesExternalStringResource4dataEv, .-_ZNK2v88internal29NativesExternalStringResource4dataEv
	.section	.text._ZNK2v88internal29NativesExternalStringResource6lengthEv,"axG",@progbits,_ZNK2v88internal29NativesExternalStringResource6lengthEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal29NativesExternalStringResource6lengthEv
	.type	_ZNK2v88internal29NativesExternalStringResource6lengthEv, @function
_ZNK2v88internal29NativesExternalStringResource6lengthEv:
.LFB21256:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE21256:
	.size	_ZNK2v88internal29NativesExternalStringResource6lengthEv, .-_ZNK2v88internal29NativesExternalStringResource6lengthEv
	.section	.text._ZN2v88internal23StringTableInsertionKeyD2Ev,"axG",@progbits,_ZN2v88internal23StringTableInsertionKeyD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23StringTableInsertionKeyD2Ev
	.type	_ZN2v88internal23StringTableInsertionKeyD2Ev, @function
_ZN2v88internal23StringTableInsertionKeyD2Ev:
.LFB27745:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE27745:
	.size	_ZN2v88internal23StringTableInsertionKeyD2Ev, .-_ZN2v88internal23StringTableInsertionKeyD2Ev
	.weak	_ZN2v88internal23StringTableInsertionKeyD1Ev
	.set	_ZN2v88internal23StringTableInsertionKeyD1Ev,_ZN2v88internal23StringTableInsertionKeyD2Ev
	.section	.rodata._ZN2v88internalL28NoExternalReferencesCallbackEv.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"No external references provided via API"
	.section	.rodata._ZN2v88internalL28NoExternalReferencesCallbackEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL28NoExternalReferencesCallbackEv,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28NoExternalReferencesCallbackEv, @function
_ZN2v88internalL28NoExternalReferencesCallbackEv:
.LFB21750:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21750:
	.size	_ZN2v88internalL28NoExternalReferencesCallbackEv, .-_ZN2v88internalL28NoExternalReferencesCallbackEv
	.section	.text._ZN2v88internal12DeserializerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12DeserializerD2Ev
	.type	_ZN2v88internal12DeserializerD2Ev, @function
_ZN2v88internal12DeserializerD2Ev:
.LFB21722:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal12DeserializerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	600(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	movq	560(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L11
	call	_ZdlPv@PLT
.L11:
	movq	536(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L12
	call	_ZdlPv@PLT
.L12:
	leaq	448(%rbx), %r12
	leaq	304(%rbx), %r13
	.p2align 4,,10
	.p2align 3
.L16:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L13
	call	_ZdlPv@PLT
	subq	$24, %r12
	cmpq	%r13, %r12
	jne	.L16
.L14:
	movq	304(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L17
	call	_ZdlPv@PLT
.L17:
	movq	280(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L18
	call	_ZdlPv@PLT
.L18:
	movq	256(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L19
	call	_ZdlPv@PLT
.L19:
	movq	232(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L20
	call	_ZdlPv@PLT
.L20:
	movq	208(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L21
	call	_ZdlPv@PLT
.L21:
	movq	184(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L22
	call	_ZdlPv@PLT
.L22:
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L23
	call	_ZdlPv@PLT
.L23:
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L24
	call	_ZdlPv@PLT
.L24:
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L9
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	subq	$24, %r12
	cmpq	%r13, %r12
	jne	.L16
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L9:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21722:
	.size	_ZN2v88internal12DeserializerD2Ev, .-_ZN2v88internal12DeserializerD2Ev
	.globl	_ZN2v88internal12DeserializerD1Ev
	.set	_ZN2v88internal12DeserializerD1Ev,_ZN2v88internal12DeserializerD2Ev
	.section	.text._ZN2v88internal23StringTableInsertionKeyD0Ev,"axG",@progbits,_ZN2v88internal23StringTableInsertionKeyD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23StringTableInsertionKeyD0Ev
	.type	_ZN2v88internal23StringTableInsertionKeyD0Ev, @function
_ZN2v88internal23StringTableInsertionKeyD0Ev:
.LFB27747:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27747:
	.size	_ZN2v88internal23StringTableInsertionKeyD0Ev, .-_ZN2v88internal23StringTableInsertionKeyD0Ev
	.section	.text._ZN2v88internal12DeserializerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12DeserializerD0Ev
	.type	_ZN2v88internal12DeserializerD0Ev, @function
_ZN2v88internal12DeserializerD0Ev:
.LFB21724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal12DeserializerD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$624, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE21724:
	.size	_ZN2v88internal12DeserializerD0Ev, .-_ZN2v88internal12DeserializerD0Ev
	.section	.text._ZN2v88internal6Logger27is_listening_to_code_eventsEv,"axG",@progbits,_ZN2v88internal6Logger27is_listening_to_code_eventsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6Logger27is_listening_to_code_eventsEv
	.type	_ZN2v88internal6Logger27is_listening_to_code_eventsEv, @function
_ZN2v88internal6Logger27is_listening_to_code_eventsEv:
.LFB20934:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L63
	cmpq	$0, 80(%rbx)
	setne	%al
.L63:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20934:
	.size	_ZN2v88internal6Logger27is_listening_to_code_eventsEv, .-_ZN2v88internal6Logger27is_listening_to_code_eventsEv
	.section	.text._ZN2v88internal23StringTableInsertionKey7IsMatchENS0_6StringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23StringTableInsertionKey7IsMatchENS0_6StringE
	.type	_ZN2v88internal23StringTableInsertionKey7IsMatchENS0_6StringE, @function
_ZN2v88internal23StringTableInsertionKey7IsMatchENS0_6StringE:
.LFB21734:
	.cfi_startproc
	endbr64
	addq	$16, %rdi
	jmp	_ZN2v88internal6String10SlowEqualsES1_@PLT
	.cfi_endproc
.LFE21734:
	.size	_ZN2v88internal23StringTableInsertionKey7IsMatchENS0_6StringE, .-_ZN2v88internal23StringTableInsertionKey7IsMatchENS0_6StringE
	.section	.text._ZN2v88internal23StringTableInsertionKey8AsHandleEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23StringTableInsertionKey8AsHandleEPNS0_7IsolateE
	.type	_ZN2v88internal23StringTableInsertionKey8AsHandleEPNS0_7IsolateE, @function
_ZN2v88internal23StringTableInsertionKey8AsHandleEPNS0_7IsolateE:
.LFB21735:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L68
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L72
.L70:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L70
	.cfi_endproc
.LFE21735:
	.size	_ZN2v88internal23StringTableInsertionKey8AsHandleEPNS0_7IsolateE, .-_ZN2v88internal23StringTableInsertionKey8AsHandleEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal12Deserializer11SynchronizeENS0_22VisitorSynchronization7SyncTagE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"expected == source_.Get()"
	.section	.text._ZN2v88internal12Deserializer11SynchronizeENS0_22VisitorSynchronization7SyncTagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Deserializer11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.type	_ZN2v88internal12Deserializer11SynchronizeENS0_22VisitorSynchronization7SyncTagE, @function
_ZN2v88internal12Deserializer11SynchronizeENS0_22VisitorSynchronization7SyncTagE:
.LFB21726:
	.cfi_startproc
	endbr64
	movslq	124(%rdi), %rax
	movq	112(%rdi), %rdx
	leal	1(%rax), %ecx
	movl	%ecx, 124(%rdi)
	cmpb	$26, (%rdx,%rax)
	jne	.L78
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21726:
	.size	_ZN2v88internal12Deserializer11SynchronizeENS0_22VisitorSynchronization7SyncTagE, .-_ZN2v88internal12Deserializer11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.section	.rodata._ZN2v88internal12Deserializer10InitializeEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"magic_number_ == SerializedData::kMagicNumber"
	.section	.text._ZN2v88internal12Deserializer10InitializeEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Deserializer10InitializeEPNS0_7IsolateE
	.type	_ZN2v88internal12Deserializer10InitializeEPNS0_7IsolateE, @function
_ZN2v88internal12Deserializer10InitializeEPNS0_7IsolateE:
.LFB21712:
	.cfi_startproc
	endbr64
	movq	%rsi, 80(%rdi)
	addq	$37592, %rsi
	cmpl	$-1059191884, 128(%rdi)
	movq	%rsi, 584(%rdi)
	jne	.L84
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21712:
	.size	_ZN2v88internal12Deserializer10InitializeEPNS0_7IsolateE, .-_ZN2v88internal12Deserializer10InitializeEPNS0_7IsolateE
	.section	.text._ZN2v88internal12Deserializer6RehashEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Deserializer6RehashEv
	.type	_ZN2v88internal12Deserializer6RehashEv, @function
_ZN2v88internal12Deserializer6RehashEv:
.LFB21713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	600(%rdi), %rbx
	movq	608(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	%rbx, %r12
	je	.L85
	movq	%rdi, %r14
	leaq	-48(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L87:
	movq	(%rbx), %rax
	movq	%r13, %rdi
	addq	$8, %rbx
	movq	%rax, -48(%rbp)
	movq	80(%r14), %rax
	leaq	56(%rax), %rsi
	call	_ZN2v88internal10HeapObject16RehashBasedOnMapENS0_13ReadOnlyRootsE@PLT
	cmpq	%rbx, %r12
	jne	.L87
.L85:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L91
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L91:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21713:
	.size	_ZN2v88internal12Deserializer6RehashEv, .-_ZN2v88internal12Deserializer6RehashEv
	.section	.text._ZN2v88internal12Deserializer18LogNewObjectEventsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Deserializer18LogNewObjectEventsEv
	.type	_ZN2v88internal12Deserializer18LogNewObjectEventsEv, @function
_ZN2v88internal12Deserializer18LogNewObjectEventsEv:
.LFB21728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	_ZN2v88internal6Logger27is_listening_to_code_eventsEv(%rip), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	80(%rdi), %rax
	movq	41016(%rax), %r12
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	136(%rax), %rax
	cmpq	%r13, %rax
	jne	.L93
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L94
.L96:
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger14LogCodeObjectsEv@PLT
.L95:
	movq	80(%rbx), %rax
	movq	41016(%rax), %r12
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	136(%rax), %rax
	cmpq	%r13, %rax
	jne	.L98
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L99
.L101:
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger20LogCompiledFunctionsEv@PLT
.L100:
	movq	136(%rbx), %r12
	movq	144(%rbx), %r13
	cmpq	%r13, %r12
	jne	.L106
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L104:
	movq	80(%rbx), %rax
	movq	41016(%rax), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L115
.L105:
	addq	$8, %r12
	cmpq	%r12, %r13
	je	.L92
.L106:
	movq	80(%rbx), %rax
	movq	(%r12), %r15
	movq	41016(%rax), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L104
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6Logger9MapCreateENS0_3MapE@PLT
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L92:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6Logger10MapDetailsENS0_3MapE@PLT
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L99:
	cmpq	$0, 80(%r12)
	jne	.L101
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L94:
	cmpq	$0, 80(%r12)
	jne	.L96
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L98:
	call	*%rax
	testb	%al, %al
	je	.L100
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L93:
	call	*%rax
	testb	%al, %al
	je	.L95
	jmp	.L96
	.cfi_endproc
.LFE21728:
	.size	_ZN2v88internal12Deserializer18LogNewObjectEventsEv, .-_ZN2v88internal12Deserializer18LogNewObjectEventsEv
	.section	.text._ZN2v88internal12Deserializer15LogNewMapEventsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Deserializer15LogNewMapEventsEv
	.type	_ZN2v88internal12Deserializer15LogNewMapEventsEv, @function
_ZN2v88internal12Deserializer15LogNewMapEventsEv:
.LFB21729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	136(%rdi), %rbx
	movq	144(%rdi), %r15
	cmpq	%r15, %rbx
	je	.L116
	movq	%rdi, %r12
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L118:
	movq	80(%r12), %rax
	movq	41016(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L129
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L116
.L120:
	movq	80(%r12), %rax
	movq	(%rbx), %r14
	movq	41016(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L118
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger9MapCreateENS0_3MapE@PLT
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L129:
	movq	%r14, %rsi
	movq	%r13, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal6Logger10MapDetailsENS0_3MapE@PLT
	cmpq	%rbx, %r15
	jne	.L120
.L116:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21729:
	.size	_ZN2v88internal12Deserializer15LogNewMapEventsEv, .-_ZN2v88internal12Deserializer15LogNewMapEventsEv
	.section	.rodata._ZN2v88internal12Deserializer15LogScriptEventsENS0_6ScriptE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"disabled-by-default-v8.compile"
	.section	.rodata._ZN2v88internal12Deserializer15LogScriptEventsENS0_6ScriptE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"v8::internal::Script"
.LC6:
	.string	"Script"
.LC7:
	.string	"snapshot"
	.section	.text._ZN2v88internal12Deserializer15LogScriptEventsENS0_6ScriptE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Deserializer15LogScriptEventsENS0_6ScriptE
	.type	_ZN2v88internal12Deserializer15LogScriptEventsENS0_6ScriptE, @function
_ZN2v88internal12Deserializer15LogScriptEventsENS0_6ScriptE:
.LFB21730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$80, %rsp
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	80(%rdi), %rax
	movq	41016(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L179
.L131:
	movq	80(%rbx), %rax
	movq	41016(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L180
.L132:
	movq	_ZZN2v88internal12Deserializer15LogScriptEventsENS0_6ScriptEE28trace_event_unique_atomic160(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L181
.L134:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L182
.L136:
	movq	_ZZN2v88internal12Deserializer15LogScriptEventsENS0_6ScriptEE28trace_event_unique_atomic163(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L183
.L141:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L184
.L130:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L185
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	movq	-88(%rbp), %rax
	movl	$2, %esi
	movq	%r12, %rdi
	movslq	67(%rax), %rdx
	call	_ZN2v88internal6Logger11ScriptEventENS1_15ScriptEventTypeEi@PLT
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L184:
	movq	-88(%rbp), %rax
	leaq	-72(%rbp), %rdi
	leaq	-88(%rbp), %rsi
	movq	63(%rax), %rbx
	call	_ZN2v88internal6Script13ToTracedValueEv@PLT
	leaq	.LC7(%rip), %rax
	movb	$8, -73(%rbp)
	movq	%rax, -64(%rbp)
	movq	-72(%rbp), %rax
	movq	$0, -40(%rbp)
	movq	$0, -72(%rbp)
	movq	%rax, -56(%rbp)
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L186
.L144:
	movq	-40(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L145
	movq	(%rdi), %rax
	call	*8(%rax)
.L145:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L146
	movq	(%rdi), %rax
	call	*8(%rax)
.L146:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L130
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L183:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L187
.L142:
	movq	%r12, _ZZN2v88internal12Deserializer15LogScriptEventsENS0_6ScriptEE28trace_event_unique_atomic163(%rip)
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L182:
	movq	-88(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	63(%rax), %rbx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L188
.L137:
	movq	-40(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L138
	movq	(%rdi), %rax
	call	*8(%rax)
.L138:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L136
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L181:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L189
.L135:
	movq	%r12, _ZZN2v88internal12Deserializer15LogScriptEventsENS0_6ScriptEE28trace_event_unique_atomic160(%rip)
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L180:
	movq	-88(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger13ScriptDetailsENS0_6ScriptE@PLT
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L186:
	subq	$8, %rsp
	leaq	-48(%rbp), %rdx
	sarq	$32, %rbx
	leaq	.LC5(%rip), %r8
	pushq	$2
	movq	%rbx, %r9
	movl	$79, %esi
	leaq	.LC6(%rip), %rcx
	pushq	%rdx
	leaq	-56(%rbp), %rdx
	pushq	%rdx
	leaq	-73(%rbp), %rdx
	pushq	%rdx
	leaq	-64(%rbp), %rdx
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$1
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L189:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L188:
	subq	$8, %rsp
	leaq	-48(%rbp), %rdx
	sarq	$32, %rbx
	leaq	.LC5(%rip), %r8
	pushq	$2
	movq	%rbx, %r9
	movl	$78, %esi
	leaq	.LC6(%rip), %rcx
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L187:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L142
.L185:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21730:
	.size	_ZN2v88internal12Deserializer15LogScriptEventsENS0_6ScriptE, .-_ZN2v88internal12Deserializer15LogScriptEventsENS0_6ScriptE
	.section	.text._ZN2v88internal23StringTableInsertionKeyC2ENS0_6StringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23StringTableInsertionKeyC2ENS0_6StringE
	.type	_ZN2v88internal23StringTableInsertionKeyC2ENS0_6StringE, @function
_ZN2v88internal23StringTableInsertionKeyC2ENS0_6StringE:
.LFB21732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	11(%rsi), %r13d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	7(%rsi), %eax
	testb	$1, %al
	je	.L191
	leaq	-48(%rbp), %rdi
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movl	7(%r12), %eax
.L191:
	movl	%eax, 8(%rbx)
	leaq	16+_ZTVN2v88internal23StringTableInsertionKeyE(%rip), %rax
	movl	%r13d, 12(%rbx)
	movq	%rax, (%rbx)
	movq	%r12, 16(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L194
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L194:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21732:
	.size	_ZN2v88internal23StringTableInsertionKeyC2ENS0_6StringE, .-_ZN2v88internal23StringTableInsertionKeyC2ENS0_6StringE
	.globl	_ZN2v88internal23StringTableInsertionKeyC1ENS0_6StringE
	.set	_ZN2v88internal23StringTableInsertionKeyC1ENS0_6StringE,_ZN2v88internal23StringTableInsertionKeyC2ENS0_6StringE
	.section	.text._ZN2v88internal23StringTableInsertionKey16ComputeHashFieldENS0_6StringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23StringTableInsertionKey16ComputeHashFieldENS0_6StringE
	.type	_ZN2v88internal23StringTableInsertionKey16ComputeHashFieldENS0_6StringE, @function
_ZN2v88internal23StringTableInsertionKey16ComputeHashFieldENS0_6StringE:
.LFB21736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	7(%rsi), %eax
	testb	$1, %al
	je	.L195
	movq	%rsi, %rbx
	leaq	-32(%rbp), %rdi
	movq	%rsi, -32(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movl	7(%rbx), %eax
.L195:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L199
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L199:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21736:
	.size	_ZN2v88internal23StringTableInsertionKey16ComputeHashFieldENS0_6StringE, .-_ZN2v88internal23StringTableInsertionKey16ComputeHashFieldENS0_6StringE
	.section	.text._ZN2v88internal12Deserializer23GetBackReferencedObjectENS0_13SnapshotSpaceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Deserializer23GetBackReferencedObjectENS0_13SnapshotSpaceE
	.type	_ZN2v88internal12Deserializer23GetBackReferencedObjectENS0_13SnapshotSpaceE, @function
_ZN2v88internal12Deserializer23GetBackReferencedObjectENS0_13SnapshotSpaceE:
.LFB21739:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r8d
	movl	$32, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	112(%rdi), %rdi
	movslq	124(%rbx), %rcx
	movzbl	1(%rdi,%rcx), %eax
	movzbl	2(%rdi,%rcx), %esi
	movq	%rcx, %rdx
	sall	$16, %esi
	sall	$8, %eax
	orl	%esi, %eax
	movzbl	(%rdi,%rcx), %esi
	orl	%esi, %eax
	movzbl	3(%rdi,%rcx), %esi
	movl	%r10d, %ecx
	sall	$24, %esi
	orl	%eax, %esi
	andl	$3, %eax
	addl	$1, %eax
	addl	%eax, %edx
	sall	$3, %eax
	subl	%eax, %ecx
	movl	$-1, %eax
	movl	%eax, %r11d
	shrl	%cl, %r11d
	andl	%r11d, %esi
	shrl	$2, %esi
	cmpl	$4, %r8d
	je	.L201
	cmpl	$5, %r8d
	je	.L202
	testl	%r8d, %r8d
	je	.L216
	movslq	%edx, %rcx
	movl	%edx, 124(%rbx)
	movzbl	1(%rdi,%rcx), %r9d
	movzbl	2(%rdi,%rcx), %r11d
	movzbl	(%rdi,%rcx), %r12d
	movzbl	3(%rdi,%rcx), %ecx
	sall	$16, %r11d
	sall	$8, %r9d
	orl	%r11d, %r9d
	sall	$24, %ecx
	orl	%r12d, %r9d
	orl	%r9d, %ecx
	andl	$3, %r9d
	leal	1(%r9), %edi
	movl	%ecx, %r11d
	movl	%r10d, %ecx
	addl	%edi, %edx
	sall	$3, %edi
	subl	%edi, %ecx
	movl	%edx, 124(%rbx)
	leaq	328(%rbx), %rdi
	movl	%esi, %edx
	shrl	%cl, %eax
	movl	%r8d, %esi
	movl	%eax, %ecx
	andl	%r11d, %ecx
	shrl	$2, %ecx
	call	_ZN2v88internal21DeserializerAllocator9GetObjectENS0_13SnapshotSpaceEjj@PLT
.L205:
	cmpb	$0, 592(%rbx)
	jne	.L217
.L209:
	movslq	72(%rbx), %rcx
	movq	%rcx, %rdx
	movq	%rax, 8(%rbx,%rcx,8)
	addl	$1, %edx
	andl	$7, %edx
	movl	%edx, 72(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L217:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L209
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$5, %dx
	jne	.L209
	movq	15(%rax), %rax
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L216:
	movslq	%edx, %rcx
	movl	%edx, 124(%rbx)
	movzbl	1(%rdi,%rcx), %r8d
	movzbl	2(%rdi,%rcx), %r9d
	movzbl	(%rdi,%rcx), %r11d
	movzbl	3(%rdi,%rcx), %ecx
	sall	$16, %r9d
	sall	$8, %r8d
	orl	%r9d, %r8d
	sall	$24, %ecx
	orl	%r11d, %r8d
	orl	%r8d, %ecx
	andl	$3, %r8d
	leal	1(%r8), %edi
	movl	%ecx, %r9d
	movl	%r10d, %ecx
	addl	%edi, %edx
	sall	$3, %edi
	subl	%edi, %ecx
	movl	%edx, 124(%rbx)
	shrl	%cl, %eax
	movl	%eax, %ecx
	movq	80(%rbx), %rax
	andl	%r9d, %ecx
	shrl	$2, %ecx
	cmpb	$0, 40460(%rax)
	je	.L206
	movq	37896(%rax), %rax
	movq	32(%rax), %rdi
	testl	%esi, %esi
	je	.L207
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L208:
	addl	$1, %edx
	movq	224(%rdi), %rdi
	cmpl	%esi, %edx
	jne	.L208
.L207:
	leaq	1(%rdi,%rcx), %rax
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L202:
	movl	%edx, 124(%rbx)
	leaq	328(%rbx), %rdi
	call	_ZN2v88internal21DeserializerAllocator14GetLargeObjectEj@PLT
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L201:
	movl	%edx, 124(%rbx)
	leaq	328(%rbx), %rdi
	call	_ZN2v88internal21DeserializerAllocator6GetMapEj@PLT
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L206:
	movl	%esi, %edx
	leaq	328(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal21DeserializerAllocator9GetObjectENS0_13SnapshotSpaceEjj@PLT
	jmp	.L205
	.cfi_endproc
.LFE21739:
	.size	_ZN2v88internal12Deserializer23GetBackReferencedObjectENS0_13SnapshotSpaceE, .-_ZN2v88internal12Deserializer23GetBackReferencedObjectENS0_13SnapshotSpaceE
	.section	.rodata._ZN2v88internal12Deserializer17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"unreachable code"
	.section	.text._ZN2v88internal12Deserializer17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Deserializer17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal12Deserializer17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal12Deserializer17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE:
.LFB21745:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21745:
	.size	_ZN2v88internal12Deserializer17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal12Deserializer17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE
	.section	.rodata._ZN2v88internal12Deserializer22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"data == kExternalReference"
	.section	.text._ZN2v88internal12Deserializer22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Deserializer22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal12Deserializer22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal12Deserializer22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE:
.LFB21746:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	112(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movslq	124(%rdi), %rax
	leal	1(%rax), %edx
	movl	%edx, 124(%rdi)
	cmpb	$33, (%rcx,%rax)
	jne	.L232
	movq	%rsi, %rbx
	movslq	%edx, %rsi
	movzbl	1(%rcx,%rsi), %eax
	movzbl	2(%rcx,%rsi), %r8d
	movzbl	(%rcx,%rsi), %r9d
	movzbl	3(%rcx,%rsi), %esi
	movl	$32, %ecx
	sall	$8, %eax
	sall	$16, %r8d
	orl	%r8d, %eax
	sall	$24, %esi
	orl	%r9d, %eax
	orl	%eax, %esi
	andl	$3, %eax
	addl	$1, %eax
	addl	%eax, %edx
	sall	$3, %eax
	subl	%eax, %ecx
	movl	$-1, %eax
	movl	%edx, 124(%rdi)
	movq	80(%rdi), %rdx
	shrl	%cl, %eax
	movq	%r12, %rdi
	andl	%esi, %eax
	shrl	$2, %eax
	movq	4856(%rdx,%rax,8), %r13
	call	_ZN2v88internal9RelocInfo16IsCodedSpeciallyEv@PLT
	testb	%al, %al
	je	.L222
	movq	%rbx, -48(%rbp)
	movq	(%r12), %r12
	testq	%rbx, %rbx
	jne	.L233
.L224:
	subl	%r12d, %r13d
	movl	$4, %esi
	movq	%r12, %rdi
	leal	-4(%r13), %eax
	movl	%eax, (%r12)
	call	_ZN2v88internal21FlushInstructionCacheEPvm@PLT
.L220:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L234
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r13, (%rax)
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L233:
	leaq	-48(%rbp), %r14
	movq	%r14, %rdi
	call	_ZNK2v88internal4Code17has_constant_poolEv@PLT
	testb	%al, %al
	je	.L224
	movq	-48(%rbp), %rax
	movl	43(%rax), %eax
	testl	%eax, %eax
	jns	.L224
	movq	%r14, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L232:
	leaq	.LC9(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L234:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21746:
	.size	_ZN2v88internal12Deserializer22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal12Deserializer22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.section	.rodata._ZN2v88internal12Deserializer22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"data == kInternalReference"
	.section	.text._ZN2v88internal12Deserializer22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Deserializer22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal12Deserializer22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal12Deserializer22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE:
.LFB21747:
	.cfi_startproc
	endbr64
	movslq	124(%rdi), %rax
	movq	112(%rdi), %r8
	leal	1(%rax), %ecx
	movl	%ecx, 124(%rdi)
	cmpb	$34, (%r8,%rax)
	jne	.L240
	movslq	%ecx, %r9
	movzbl	1(%r8,%r9), %eax
	movzbl	2(%r8,%r9), %r10d
	movzbl	(%r8,%r9), %r11d
	movzbl	3(%r8,%r9), %r8d
	sall	$8, %eax
	sall	$16, %r10d
	orl	%r10d, %eax
	sall	$24, %r8d
	orl	%r11d, %eax
	orl	%eax, %r8d
	andl	$3, %eax
	addl	$1, %eax
	addl	%eax, %ecx
	sall	$3, %eax
	movl	%ecx, 124(%rdi)
	movl	$32, %ecx
	movq	(%rdx), %rdx
	subl	%eax, %ecx
	movl	$-1, %eax
	shrl	%cl, %eax
	andl	%r8d, %eax
	shrl	$2, %eax
	leaq	63(%rsi,%rax), %rax
	movq	%rax, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC10(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21747:
	.size	_ZN2v88internal12Deserializer22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal12Deserializer22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.section	.rodata._ZN2v88internal12Deserializer18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"data == kOffHeapTarget"
	.section	.rodata._ZN2v88internal12Deserializer18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"(isolate_->embedded_blob()) != nullptr"
	.section	.rodata._ZN2v88internal12Deserializer18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE.str1.1
.LC13:
	.string	"kNullAddress != address"
	.section	.text._ZN2v88internal12Deserializer18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Deserializer18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal12Deserializer18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal12Deserializer18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE:
.LFB21748:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	112(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movslq	124(%rdi), %rax
	leal	1(%rax), %edx
	movl	%edx, 124(%rdi)
	cmpb	$37, (%rcx,%rax)
	jne	.L255
	movq	%rsi, %rbx
	movslq	%edx, %rsi
	movzbl	1(%rcx,%rsi), %eax
	movzbl	2(%rcx,%rsi), %r8d
	movzbl	(%rcx,%rsi), %r9d
	movzbl	3(%rcx,%rsi), %esi
	movl	$32, %ecx
	sall	$8, %eax
	sall	$16, %r8d
	orl	%r8d, %eax
	sall	$24, %esi
	orl	%r9d, %eax
	orl	%eax, %esi
	andl	$3, %eax
	addl	$1, %eax
	addl	%eax, %edx
	sall	$3, %eax
	subl	%eax, %ecx
	movl	$-1, %eax
	movl	%edx, 124(%rdi)
	movq	80(%rdi), %rdi
	shrl	%cl, %eax
	andl	%esi, %eax
	shrl	$2, %eax
	movl	%eax, %r13d
	call	_ZNK2v88internal7Isolate13embedded_blobEv@PLT
	testq	%rax, %rax
	je	.L256
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %r14d
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movl	%r13d, %esi
	leaq	-64(%rbp), %rdi
	movl	%r14d, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal12EmbeddedData25InstructionStartOfBuiltinEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L257
	call	_ZN2v88internal9RelocInfo29OffHeapTargetIsCodedSpeciallyEv@PLT
	testb	%al, %al
	je	.L245
	movq	%rbx, -72(%rbp)
	movq	(%r12), %r12
	testq	%rbx, %rbx
	jne	.L258
.L247:
	subl	%r12d, %r13d
	movl	$4, %esi
	movq	%r12, %rdi
	leal	-4(%r13), %eax
	movl	%eax, (%r12)
	call	_ZN2v88internal21FlushInstructionCacheEPvm@PLT
.L241:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L259
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r13, (%rax)
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L258:
	leaq	-72(%rbp), %r14
	movq	%r14, %rdi
	call	_ZNK2v88internal4Code17has_constant_poolEv@PLT
	testb	%al, %al
	je	.L247
	movq	-72(%rbp), %rax
	movl	43(%rax), %eax
	testl	%eax, %eax
	jns	.L247
	movq	%r14, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L255:
	leaq	.LC11(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L256:
	leaq	.LC12(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L257:
	leaq	.LC13(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L259:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21748:
	.size	_ZN2v88internal12Deserializer18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal12Deserializer18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE
	.section	.rodata._ZNSt6vectorIN2v88internal10HeapObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_.str1.1,"aMS",@progbits,1
.LC14:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal10HeapObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal10HeapObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal10HeapObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal10HeapObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN2v88internal10HeapObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB25256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movabsq	$1152921504606846975, %rsi
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rcx
	movq	(%rdi), %r12
	movq	%rcx, %rax
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L287
	movq	%r13, %r9
	movq	%rdi, %r15
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L272
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L288
.L262:
	movq	%r14, %rdi
	movq	%rdx, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r9
	movq	%rax, %rbx
	leaq	(%rax,%r14), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	8(%rbx), %r14
.L271:
	movq	(%rdx), %rax
	movq	%rax, (%rbx,%r9)
	cmpq	%r12, %r13
	je	.L264
	leaq	-8(%r13), %rsi
	leaq	15(%rbx), %rax
	subq	%r12, %rsi
	subq	%r12, %rax
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rax
	jbe	.L274
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdi
	je	.L274
	addq	$1, %rdi
	xorl	%eax, %eax
	movq	%rdi, %rdx
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L266:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L266
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rdx
	leaq	(%r12,%rdx), %rax
	addq	%rbx, %rdx
	cmpq	%r8, %rdi
	je	.L268
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L268:
	leaq	16(%rbx,%rsi), %r14
.L264:
	cmpq	%rcx, %r13
	je	.L269
	subq	%r13, %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rcx, %rdx
	movq	%rcx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rcx
	addq	%rcx, %r14
.L269:
	testq	%r12, %r12
	je	.L270
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L270:
	movq	-56(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r14, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L263
	movq	$0, -56(%rbp)
	movl	$8, %r14d
	xorl	%ebx, %ebx
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L272:
	movl	$8, %r14d
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L274:
	movq	%rbx, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L265:
	movq	(%rax), %rdi
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rdi, -8(%rdx)
	cmpq	%rax, %r13
	jne	.L265
	jmp	.L268
.L263:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	movq	%rsi, %r14
	salq	$3, %r14
	jmp	.L262
.L287:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE25256:
	.size	_ZNSt6vectorIN2v88internal10HeapObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN2v88internal10HeapObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB25287:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movabsq	$1152921504606846975, %rsi
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rcx
	movq	(%rdi), %r12
	movq	%rcx, %rax
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L316
	movq	%r13, %r9
	movq	%rdi, %r15
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L301
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L317
.L291:
	movq	%r14, %rdi
	movq	%rdx, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r9
	movq	%rax, %rbx
	leaq	(%rax,%r14), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	8(%rbx), %r14
.L300:
	movq	(%rdx), %rax
	movq	%rax, (%rbx,%r9)
	cmpq	%r12, %r13
	je	.L293
	leaq	-8(%r13), %rsi
	leaq	15(%rbx), %rax
	subq	%r12, %rsi
	subq	%r12, %rax
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rax
	jbe	.L303
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdi
	je	.L303
	addq	$1, %rdi
	xorl	%eax, %eax
	movq	%rdi, %rdx
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L295:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L295
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rdx
	leaq	(%r12,%rdx), %rax
	addq	%rbx, %rdx
	cmpq	%r8, %rdi
	je	.L297
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L297:
	leaq	16(%rbx,%rsi), %r14
.L293:
	cmpq	%rcx, %r13
	je	.L298
	subq	%r13, %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rcx, %rdx
	movq	%rcx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rcx
	addq	%rcx, %r14
.L298:
	testq	%r12, %r12
	je	.L299
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L299:
	movq	-56(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r14, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L317:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L292
	movq	$0, -56(%rbp)
	movl	$8, %r14d
	xorl	%ebx, %ebx
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L301:
	movl	$8, %r14d
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L303:
	movq	%rbx, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L294:
	movq	(%rax), %rdi
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rdi, -8(%rdx)
	cmpq	%rax, %r13
	jne	.L294
	jmp	.L297
.L292:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	movq	%rsi, %r14
	salq	$3, %r14
	jmp	.L291
.L316:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE25287:
	.size	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal6HandleINS1_6StringEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6HandleINS1_6StringEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6HandleINS1_6StringEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_
	.type	_ZNSt6vectorIN2v88internal6HandleINS1_6StringEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_, @function
_ZNSt6vectorIN2v88internal6HandleINS1_6StringEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_:
.LFB25472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %r12
	cmpq	16(%rdi), %r12
	je	.L319
	movq	(%rsi), %rax
	movq	%rax, (%r12)
	addq	$8, 8(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	.cfi_restore_state
	movabsq	$1152921504606846975, %rcx
	movq	(%rdi), %r14
	movq	%r12, %rdx
	subq	%r14, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L345
	testq	%rax, %rax
	je	.L330
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L346
.L322:
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	addq	%rax, %r15
	leaq	8(%rax), %rax
.L323:
	movq	(%rsi), %rcx
	movq	%rcx, 0(%r13,%rdx)
	cmpq	%r14, %r12
	je	.L324
	leaq	-8(%r12), %rsi
	leaq	15(%r13), %rax
	subq	%r14, %rsi
	subq	%r14, %rax
	movq	%rsi, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L333
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L333
	addq	$1, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	shrq	%rax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L326:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, 0(%r13,%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rdx
	jne	.L326
	movq	%rcx, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r14,%rdx), %rax
	addq	%r13, %rdx
	cmpq	%rdi, %rcx
	je	.L328
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L328:
	leaq	16(%r13,%rsi), %rax
.L324:
	testq	%r14, %r14
	je	.L329
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
.L329:
	movq	%r13, %xmm0
	movq	%rax, %xmm2
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L346:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L347
	movl	$8, %eax
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L330:
	movl	$8, %r15d
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L333:
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L325:
	movq	(%rax), %rcx
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L325
	jmp	.L328
.L345:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L347:
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	movq	%rcx, %r15
	salq	$3, %r15
	jmp	.L322
	.cfi_endproc
.LFE25472:
	.size	_ZNSt6vectorIN2v88internal6HandleINS1_6StringEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_, .-_ZNSt6vectorIN2v88internal6HandleINS1_6StringEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_
	.section	.text._ZNSt6vectorIN2v88internal6HandleINS1_6ScriptEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6HandleINS1_6ScriptEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6HandleINS1_6ScriptEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_
	.type	_ZNSt6vectorIN2v88internal6HandleINS1_6ScriptEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_, @function
_ZNSt6vectorIN2v88internal6HandleINS1_6ScriptEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_:
.LFB25477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %r12
	cmpq	16(%rdi), %r12
	je	.L349
	movq	(%rsi), %rax
	movq	%rax, (%r12)
	addq	$8, 8(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L349:
	.cfi_restore_state
	movabsq	$1152921504606846975, %rcx
	movq	(%rdi), %r14
	movq	%r12, %rdx
	subq	%r14, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L375
	testq	%rax, %rax
	je	.L360
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L376
.L352:
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	addq	%rax, %r15
	leaq	8(%rax), %rax
.L353:
	movq	(%rsi), %rcx
	movq	%rcx, 0(%r13,%rdx)
	cmpq	%r14, %r12
	je	.L354
	leaq	-8(%r12), %rsi
	leaq	15(%r13), %rax
	subq	%r14, %rsi
	subq	%r14, %rax
	movq	%rsi, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L363
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L363
	addq	$1, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	shrq	%rax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L356:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, 0(%r13,%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rdx
	jne	.L356
	movq	%rcx, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r14,%rdx), %rax
	addq	%r13, %rdx
	cmpq	%rdi, %rcx
	je	.L358
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L358:
	leaq	16(%r13,%rsi), %rax
.L354:
	testq	%r14, %r14
	je	.L359
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
.L359:
	movq	%r13, %xmm0
	movq	%rax, %xmm2
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L376:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L377
	movl	$8, %eax
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L360:
	movl	$8, %r15d
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L363:
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L355:
	movq	(%rax), %rcx
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L355
	jmp	.L358
.L375:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L377:
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	movq	%rcx, %r15
	salq	$3, %r15
	jmp	.L352
	.cfi_endproc
.LFE25477:
	.size	_ZNSt6vectorIN2v88internal6HandleINS1_6ScriptEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_, .-_ZNSt6vectorIN2v88internal6HandleINS1_6ScriptEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_
	.section	.text._ZNSt6vectorIN2v88internal14AllocationSiteESaIS2_EE12emplace_backIJS2_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal14AllocationSiteESaIS2_EE12emplace_backIJS2_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal14AllocationSiteESaIS2_EE12emplace_backIJS2_EEEvDpOT_
	.type	_ZNSt6vectorIN2v88internal14AllocationSiteESaIS2_EE12emplace_backIJS2_EEEvDpOT_, @function
_ZNSt6vectorIN2v88internal14AllocationSiteESaIS2_EE12emplace_backIJS2_EEEvDpOT_:
.LFB25479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %r12
	cmpq	16(%rdi), %r12
	je	.L379
	movq	(%rsi), %rax
	movq	%rax, (%r12)
	addq	$8, 8(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L379:
	.cfi_restore_state
	movabsq	$1152921504606846975, %rcx
	movq	(%rdi), %r14
	movq	%r12, %rdx
	subq	%r14, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L405
	testq	%rax, %rax
	je	.L390
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L406
.L382:
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	addq	%rax, %r15
	leaq	8(%rax), %rax
.L383:
	movq	(%rsi), %rcx
	movq	%rcx, 0(%r13,%rdx)
	cmpq	%r14, %r12
	je	.L384
	leaq	-8(%r12), %rsi
	leaq	15(%r13), %rax
	subq	%r14, %rsi
	subq	%r14, %rax
	movq	%rsi, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L393
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L393
	addq	$1, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	shrq	%rax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L386:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, 0(%r13,%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rdx
	jne	.L386
	movq	%rcx, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r14,%rdx), %rax
	addq	%r13, %rdx
	cmpq	%rdi, %rcx
	je	.L388
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L388:
	leaq	16(%r13,%rsi), %rax
.L384:
	testq	%r14, %r14
	je	.L389
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
.L389:
	movq	%r13, %xmm0
	movq	%rax, %xmm2
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L406:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L407
	movl	$8, %eax
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L390:
	movl	$8, %r15d
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L393:
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L385:
	movq	(%rax), %rcx
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L385
	jmp	.L388
.L405:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L407:
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	movq	%rcx, %r15
	salq	$3, %r15
	jmp	.L382
	.cfi_endproc
.LFE25479:
	.size	_ZNSt6vectorIN2v88internal14AllocationSiteESaIS2_EE12emplace_backIJS2_EEEvDpOT_, .-_ZNSt6vectorIN2v88internal14AllocationSiteESaIS2_EE12emplace_backIJS2_EEEvDpOT_
	.section	.text._ZNSt6vectorIN2v88internal4CodeESaIS2_EE12emplace_backIJS2_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4CodeESaIS2_EE12emplace_backIJS2_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4CodeESaIS2_EE12emplace_backIJS2_EEEvDpOT_
	.type	_ZNSt6vectorIN2v88internal4CodeESaIS2_EE12emplace_backIJS2_EEEvDpOT_, @function
_ZNSt6vectorIN2v88internal4CodeESaIS2_EE12emplace_backIJS2_EEEvDpOT_:
.LFB25481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %r12
	cmpq	16(%rdi), %r12
	je	.L409
	movq	(%rsi), %rax
	movq	%rax, (%r12)
	addq	$8, 8(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L409:
	.cfi_restore_state
	movabsq	$1152921504606846975, %rcx
	movq	(%rdi), %r14
	movq	%r12, %rdx
	subq	%r14, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L435
	testq	%rax, %rax
	je	.L420
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L436
.L412:
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	addq	%rax, %r15
	leaq	8(%rax), %rax
.L413:
	movq	(%rsi), %rcx
	movq	%rcx, 0(%r13,%rdx)
	cmpq	%r14, %r12
	je	.L414
	leaq	-8(%r12), %rsi
	leaq	15(%r13), %rax
	subq	%r14, %rsi
	subq	%r14, %rax
	movq	%rsi, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L423
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L423
	addq	$1, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	shrq	%rax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L416:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, 0(%r13,%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rdx
	jne	.L416
	movq	%rcx, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r14,%rdx), %rax
	addq	%r13, %rdx
	cmpq	%rdi, %rcx
	je	.L418
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L418:
	leaq	16(%r13,%rsi), %rax
.L414:
	testq	%r14, %r14
	je	.L419
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
.L419:
	movq	%r13, %xmm0
	movq	%rax, %xmm2
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L436:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L437
	movl	$8, %eax
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L420:
	movl	$8, %r15d
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L423:
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L415:
	movq	(%rax), %rcx
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L415
	jmp	.L418
.L435:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L437:
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	movq	%rcx, %r15
	salq	$3, %r15
	jmp	.L412
	.cfi_endproc
.LFE25481:
	.size	_ZNSt6vectorIN2v88internal4CodeESaIS2_EE12emplace_backIJS2_EEEvDpOT_, .-_ZNSt6vectorIN2v88internal4CodeESaIS2_EE12emplace_backIJS2_EEEvDpOT_
	.section	.text._ZNSt6vectorIN2v88internal3MapESaIS2_EE12emplace_backIJS2_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal3MapESaIS2_EE12emplace_backIJS2_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal3MapESaIS2_EE12emplace_backIJS2_EEEvDpOT_
	.type	_ZNSt6vectorIN2v88internal3MapESaIS2_EE12emplace_backIJS2_EEEvDpOT_, @function
_ZNSt6vectorIN2v88internal3MapESaIS2_EE12emplace_backIJS2_EEEvDpOT_:
.LFB25483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %r12
	cmpq	16(%rdi), %r12
	je	.L439
	movq	(%rsi), %rax
	movq	%rax, (%r12)
	addq	$8, 8(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L439:
	.cfi_restore_state
	movabsq	$1152921504606846975, %rcx
	movq	(%rdi), %r14
	movq	%r12, %rdx
	subq	%r14, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L465
	testq	%rax, %rax
	je	.L450
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L466
.L442:
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	addq	%rax, %r15
	leaq	8(%rax), %rax
.L443:
	movq	(%rsi), %rcx
	movq	%rcx, 0(%r13,%rdx)
	cmpq	%r14, %r12
	je	.L444
	leaq	-8(%r12), %rsi
	leaq	15(%r13), %rax
	subq	%r14, %rsi
	subq	%r14, %rax
	movq	%rsi, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L453
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L453
	addq	$1, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	shrq	%rax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L446:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, 0(%r13,%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rdx
	jne	.L446
	movq	%rcx, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r14,%rdx), %rax
	addq	%r13, %rdx
	cmpq	%rdi, %rcx
	je	.L448
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L448:
	leaq	16(%r13,%rsi), %rax
.L444:
	testq	%r14, %r14
	je	.L449
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
.L449:
	movq	%r13, %xmm0
	movq	%rax, %xmm2
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L466:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L467
	movl	$8, %eax
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L450:
	movl	$8, %r15d
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L453:
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L445:
	movq	(%rax), %rcx
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L445
	jmp	.L448
.L465:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L467:
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	movq	%rcx, %r15
	salq	$3, %r15
	jmp	.L442
	.cfi_endproc
.LFE25483:
	.size	_ZNSt6vectorIN2v88internal3MapESaIS2_EE12emplace_backIJS2_EEEvDpOT_, .-_ZNSt6vectorIN2v88internal3MapESaIS2_EE12emplace_backIJS2_EEEvDpOT_
	.section	.text._ZN2v88internal12Deserializer20PostProcessNewObjectENS0_10HeapObjectENS0_13SnapshotSpaceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Deserializer20PostProcessNewObjectENS0_10HeapObjectENS0_13SnapshotSpaceE
	.type	_ZN2v88internal12Deserializer20PostProcessNewObjectENS0_10HeapObjectENS0_13SnapshotSpaceE, @function
_ZN2v88internal12Deserializer20PostProcessNewObjectENS0_10HeapObjectENS0_13SnapshotSpaceE:
.LFB21738:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$120, %rsp
	movq	%rsi, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal20FLAG_rehash_snapshotE(%rip)
	leaq	-1(%rsi), %rax
	je	.L469
	cmpb	$0, 593(%rdi)
	je	.L469
	movq	(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L472
.L561:
	movq	-104(%rbp), %rax
	movl	$3, 7(%rax)
	testl	%ebx, %ebx
	je	.L555
.L474:
	movq	-104(%rbp), %r12
	cmpb	$0, 592(%r13)
	leaq	-1(%r12), %rax
	je	.L471
	movq	-1(%r12), %rdx
	cmpw	$63, 11(%rdx)
	movq	-1(%r12), %rdx
	ja	.L478
	movzwl	11(%rdx), %edx
	andl	$65504, %edx
	je	.L556
	.p2align 4,,10
	.p2align 3
.L471:
	movq	(%rax), %rax
	cmpw	$96, 11(%rax)
	je	.L557
	movq	-1(%r12), %rax
	cmpw	$69, 11(%rax)
	je	.L558
	cmpb	$0, _ZN2v88internal15FLAG_trace_mapsE(%rip)
	leaq	-1(%r12), %rax
	je	.L501
	movq	-1(%r12), %rdx
	cmpw	$68, 11(%rdx)
	je	.L502
.L501:
	movq	(%rax), %rax
	cmpw	$78, 11(%rax)
	jne	.L559
.L498:
	movq	-104(%rbp), %rax
.L491:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L560
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L469:
	.cfi_restore_state
	cmpb	$0, 592(%r13)
	je	.L471
	movq	(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L561
.L472:
	leaq	-104(%rbp), %r12
	movq	%r12, %rdi
	call	_ZNK2v88internal10HeapObject14NeedsRehashingEv@PLT
	testb	%al, %al
	je	.L474
	movq	608(%r13), %rsi
	cmpq	616(%r13), %rsi
	je	.L477
.L551:
	movq	-104(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 608(%r13)
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L558:
	cmpb	$0, 592(%r13)
	jne	.L500
	cmpl	$5, %ebx
	jne	.L498
.L500:
	movq	-104(%rbp), %rax
	leaq	-80(%rbp), %rsi
	leaq	184(%r13), %rdi
	movq	%rax, -80(%rbp)
	call	_ZNSt6vectorIN2v88internal4CodeESaIS2_EE12emplace_backIJS2_EEEvDpOT_
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L557:
	movq	-104(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal12Deserializer15LogScriptEventsENS0_6ScriptE
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L478:
	cmpw	$96, 11(%rdx)
	jne	.L492
	movq	80(%r13), %r14
	leaq	280(%r13), %r15
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L493
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L494:
	leaq	-80(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	call	_ZNSt6vectorIN2v88internal6HandleINS1_6ScriptEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_
	movq	-104(%rbp), %r12
	leaq	-1(%r12), %rax
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L559:
	movq	-1(%r12), %rax
	cmpw	$150, 11(%rax)
	je	.L498
	movq	-1(%r12), %rax
	cmpw	$63, 11(%rax)
	jbe	.L562
.L505:
	movq	-1(%r12), %rax
	cmpw	$1087, 11(%rax)
	jne	.L523
	movq	23(%r12), %rdx
	movq	31(%r12), %rax
	addq	31(%rdx), %rax
	movq	%rax, 47(%r12)
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L492:
	movq	-1(%r12), %rdx
	cmpw	$121, 11(%rdx)
	jne	.L471
	leaq	-80(%rbp), %rsi
	leaq	160(%r13), %rdi
	movq	%r12, -80(%rbp)
	call	_ZNSt6vectorIN2v88internal14AllocationSiteESaIS2_EE12emplace_backIJS2_EEEvDpOT_
	movq	-104(%rbp), %r12
	leaq	-1(%r12), %rax
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L556:
	movl	7(%r12), %r14d
	movl	11(%r12), %ebx
	testb	$1, %r14b
	jne	.L563
.L480:
	movq	80(%r13), %r9
	movl	%ebx, -68(%rbp)
	movl	$1, %r15d
	leaq	-64(%rbp), %rdi
	leaq	16+_ZTVN2v88internal23StringTableInsertionKeyE(%rip), %rax
	movl	%r14d, -72(%rbp)
	shrl	$2, %r14d
	movq	%rax, -80(%rbp)
	movq	%r12, -64(%rbp)
	movq	4792(%r9), %rdx
	movq	88(%r9), %r10
	movq	96(%r9), %r8
	movslq	35(%rdx), %rcx
	subq	$1, %rdx
	subl	$1, %ecx
	andl	%ecx, %r14d
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L546:
	cmpq	%rsi, %r8
	je	.L484
	movl	-72(%rbp), %eax
	cmpl	%eax, 7(%rsi)
	jne	.L484
	movl	-68(%rbp), %eax
	cmpl	%eax, 11(%rsi)
	jne	.L484
	movq	%rdx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r10, -136(%rbp)
	movl	%ecx, -124(%rbp)
	movq	%r9, -120(%rbp)
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal6String10SlowEqualsES1_@PLT
	movq	-112(%rbp), %rdi
	movq	-120(%rbp), %r9
	testb	%al, %al
	movl	-124(%rbp), %ecx
	movq	-136(%rbp), %r10
	movq	-144(%rbp), %r8
	movq	-152(%rbp), %rdx
	jne	.L564
	.p2align 4,,10
	.p2align 3
.L484:
	addl	%r15d, %r14d
	addl	$1, %r15d
	andl	%ecx, %r14d
.L483:
	leal	40(,%r14,8), %ebx
	movslq	%ebx, %rbx
	addq	%rdx, %rbx
	movq	(%rbx), %rsi
	cmpq	%rsi, %r10
	jne	.L546
.L552:
	leaq	-88(%rbp), %r14
.L486:
	movq	80(%r13), %rbx
	leaq	256(%r13), %r15
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L488
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L489:
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, -88(%rbp)
	call	_ZNSt6vectorIN2v88internal6HandleINS1_6StringEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_
	movq	%r12, %rax
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L555:
	movq	608(%r13), %rsi
	cmpq	616(%r13), %rsi
	jne	.L551
	leaq	-104(%rbp), %rdx
	leaq	600(%r13), %rdi
	call	_ZNSt6vectorIN2v88internal10HeapObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L493:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L565
.L495:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%r12, (%rax)
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L562:
	movq	-1(%r12), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$2, %ax
	jne	.L505
	movq	80(%r13), %rax
	movq	-1(%r12), %rdx
	cmpq	%rdx, 736(%rax)
	je	.L566
	movq	%r12, -80(%rbp)
	movl	15(%r12), %edx
	movq	41728(%rax), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rax, 15(%r12)
	movq	-80(%rbp), %rbx
	movq	-1(%rbx), %rax
	cmpw	$63, 11(%rax)
	ja	.L513
	movq	-1(%rbx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$2, %ax
	je	.L567
.L513:
	movq	-80(%rbp), %rbx
.L554:
	movq	-1(%rbx), %rax
	testb	$16, 11(%rax)
	jne	.L517
	movq	15(%rbx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movq	%rax, 23(%rbx)
.L517:
	movq	80(%r13), %rax
	leaq	-80(%rbp), %rdi
	leaq	37592(%rax), %r12
	call	_ZNK2v88internal14ExternalString19ExternalPayloadSizeEv@PLT
	movq	-80(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movslq	%eax, %rcx
	call	_ZN2v88internal4Heap20UpdateExternalStringENS0_6StringEmm@PLT
.L512:
	movq	-104(%rbp), %rdx
	movq	80(%r13), %rax
	movq	%rdx, %rcx
	movq	%rdx, -80(%rbp)
	andq	$-262144, %rcx
	testb	$24, 8(%rcx)
	je	.L519
	movq	40376(%rax), %rsi
	cmpq	40384(%rax), %rsi
	je	.L520
	movq	%rdx, (%rsi)
	addq	$8, 40376(%rax)
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L502:
	leaq	-80(%rbp), %rsi
	leaq	136(%r13), %rdi
	movq	%r12, -80(%rbp)
	call	_ZNSt6vectorIN2v88internal3MapESaIS2_EE12emplace_backIJS2_EEEvDpOT_
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L563:
	leaq	-80(%rbp), %rdi
	movq	%r12, -80(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movl	7(%r12), %r14d
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L523:
	movq	-1(%r12), %rax
	cmpw	$1086, 11(%rax)
	jne	.L524
	movq	15(%r12), %rax
	cmpq	%rax, 63(%r12)
	je	.L498
	movslq	59(%r12), %rcx
	movq	304(%r13), %rdx
	movq	31(%r12), %rax
	addq	(%rdx,%rcx,8), %rax
	movq	%rax, 55(%r12)
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L488:
	movq	41088(%rbx), %rax
	cmpq	%rax, 41096(%rbx)
	je	.L568
.L490:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r12, (%rax)
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L565:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L564:
	cmpl	$-1, %r14d
	je	.L552
	movq	(%rbx), %rbx
	leaq	-88(%rbp), %r14
	movq	-64(%rbp), %rax
	movq	%r9, %rsi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal6String8MakeThinEPNS0_7IsolateES1_@PLT
	testq	%rbx, %rbx
	je	.L486
	movq	%rbx, %rax
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L524:
	movq	-1(%r12), %rax
	cmpw	$1059, 11(%rax)
	jne	.L525
	movq	31(%r12), %rax
	testq	%rax, %rax
	je	.L498
	movq	304(%r13), %rdx
	sarq	$32, %rax
	movq	%r12, %rsi
	movq	(%rdx,%rax,8), %rax
	movq	%rax, 31(%r12)
	movq	80(%r13), %rdi
	addq	$37592, %rdi
	call	_ZN2v88internal4Heap22RegisterNewArrayBufferENS0_13JSArrayBufferE@PLT
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L477:
	leaq	600(%r13), %rdi
	movq	%r12, %rdx
	call	_ZNSt6vectorIN2v88internal10HeapObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L474
.L525:
	movq	-1(%r12), %rax
	cmpw	$72, 11(%rax)
	jne	.L498
	movb	$0, 51(%r12)
	jmp	.L498
.L519:
	movq	40400(%rax), %rsi
	cmpq	40408(%rax), %rsi
	je	.L522
	movq	%rdx, (%rsi)
	addq	$8, 40400(%rax)
	jmp	.L498
.L566:
	movq	15(%r12), %rbx
	movl	$32, %edi
	call	_Znwm@PLT
	xorl	%esi, %esi
	sarq	$4, %rbx
	movq	%rax, %r14
	movq	%rax, %rdi
	movl	%ebx, %edx
	call	_ZN2v88internal29NativesExternalStringResourceC1ENS0_10NativeTypeEi@PLT
	movq	80(%r13), %rbx
	movq	%r14, 15(%r12)
	movq	-1(%r12), %rax
	testb	$16, 11(%rax)
	jne	.L507
	movq	(%r14), %rax
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L508
	movq	8(%r14), %rax
.L509:
	movq	%rax, 23(%r12)
.L507:
	movq	(%r14), %rax
	leaq	_ZNK2v88internal29NativesExternalStringResource6lengthEv(%rip), %rdx
	movq	56(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L569
	movq	16(%r14), %rcx
.L510:
	testq	%rcx, %rcx
	je	.L512
	leaq	37592(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal4Heap20UpdateExternalStringENS0_6StringEmm@PLT
	jmp	.L512
.L568:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L490
.L567:
	movq	-1(%rbx), %rax
	testb	$8, 11(%rax)
	je	.L513
	jmp	.L554
.L522:
	leaq	-80(%rbp), %rdx
	leaq	40392(%rax), %rdi
	call	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L498
.L520:
	leaq	-80(%rbp), %rdx
	leaq	40368(%rax), %rdi
	call	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L498
.L569:
	movq	%r14, %rdi
	call	*%rax
	movq	%rax, %rcx
	jmp	.L510
.L560:
	call	__stack_chk_fail@PLT
.L508:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L509
	.cfi_endproc
.LFE21738:
	.size	_ZN2v88internal12Deserializer20PostProcessNewObjectENS0_10HeapObjectENS0_13SnapshotSpaceE, .-_ZN2v88internal12Deserializer20PostProcessNewObjectENS0_10HeapObjectENS0_13SnapshotSpaceE
	.section	.text._ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	.type	_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_, @function
_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_:
.LFB26422:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L584
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L580
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L585
.L572:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L579:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L586
	testq	%r13, %r13
	jg	.L575
	testq	%r9, %r9
	jne	.L578
.L576:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L586:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L575
.L578:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L585:
	testq	%rsi, %rsi
	jne	.L573
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L575:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L576
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L580:
	movl	$8, %r14d
	jmp	.L572
.L584:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L573:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L572
	.cfi_endproc
.LFE26422:
	.size	_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_, .-_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	.section	.rodata._ZN2v88internal12Deserializer8ReadDataINS0_19FullMaybeObjectSlotEEEbT_S4_NS0_13SnapshotSpaceEm.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.rodata._ZN2v88internal12Deserializer8ReadDataINS0_19FullMaybeObjectSlotEEEbT_S4_NS0_13SnapshotSpaceEm.str1.1,"aMS",@progbits,1
.LC16:
	.string	"current == limit"
.LC17:
	.string	"filled"
.LC18:
	.string	"(backing_store) != nullptr"
.LC19:
	.string	"2 <= repeat_count"
.LC20:
	.string	"limit == current"
	.section	.text._ZN2v88internal12Deserializer8ReadDataINS0_19FullMaybeObjectSlotEEEbT_S4_NS0_13SnapshotSpaceEm,"axG",@progbits,_ZN2v88internal12Deserializer8ReadDataINS0_19FullMaybeObjectSlotEEEbT_S4_NS0_13SnapshotSpaceEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12Deserializer8ReadDataINS0_19FullMaybeObjectSlotEEEbT_S4_NS0_13SnapshotSpaceEm
	.type	_ZN2v88internal12Deserializer8ReadDataINS0_19FullMaybeObjectSlotEEEbT_S4_NS0_13SnapshotSpaceEm, @function
_ZN2v88internal12Deserializer8ReadDataINS0_19FullMaybeObjectSlotEEEbT_S4_NS0_13SnapshotSpaceEm:
.LFB24024:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	.L592(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movl	%ecx, -88(%rbp)
	movq	%r8, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	80(%rdi), %rax
	movq	%rax, -96(%rbp)
	movl	%ecx, %eax
	andl	$-3, %eax
	cmpl	$1, %eax
	setne	%dl
	testq	%r8, %r8
	setne	%al
	andl	%eax, %edx
	leaq	1(%r8), %rax
	movq	%rax, -120(%rbp)
	andq	$-262144, %rax
	movb	%dl, -81(%rbp)
	movq	%rax, -112(%rbp)
	.p2align 4,,10
	.p2align 3
.L730:
	cmpq	%r12, %r14
	jnb	.L590
.L779:
	movslq	124(%rbx), %rdx
	movq	112(%rbx), %rdi
	leal	1(%rdx), %eax
	movq	%rdx, %rcx
	movl	%eax, 124(%rbx)
	movzbl	(%rdi,%rdx), %r15d
	movl	%r15d, %esi
	cmpb	$-105, %r15b
	ja	.L730
	movzbl	%r15b, %edx
	movslq	0(%r13,%rdx,4), %rdx
	addq	%r13, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal12Deserializer8ReadDataINS0_19FullMaybeObjectSlotEEEbT_S4_NS0_13SnapshotSpaceEm,"aG",@progbits,_ZN2v88internal12Deserializer8ReadDataINS0_19FullMaybeObjectSlotEEEbT_S4_NS0_13SnapshotSpaceEm,comdat
	.align 4
	.align 4
.L592:
	.long	.L614-.L592
	.long	.L615-.L592
	.long	.L614-.L592
	.long	.L614-.L592
	.long	.L614-.L592
	.long	.L614-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L612-.L592
	.long	.L613-.L592
	.long	.L612-.L592
	.long	.L612-.L592
	.long	.L612-.L592
	.long	.L612-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L611-.L592
	.long	.L610-.L592
	.long	.L609-.L592
	.long	.L608-.L592
	.long	.L730-.L592
	.long	.L607-.L592
	.long	.L606-.L592
	.long	.L605-.L592
	.long	.L605-.L592
	.long	.L605-.L592
	.long	.L596-.L592
	.long	.L604-.L592
	.long	.L603-.L592
	.long	.L730-.L592
	.long	.L602-.L592
	.long	.L601-.L592
	.long	.L600-.L592
	.long	.L599-.L592
	.long	.L596-.L592
	.long	.L598-.L592
	.long	.L597-.L592
	.long	.L596-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L730-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L595-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L594-.L592
	.long	.L593-.L592
	.long	.L593-.L592
	.long	.L593-.L592
	.long	.L593-.L592
	.long	.L593-.L592
	.long	.L593-.L592
	.long	.L593-.L592
	.long	.L593-.L592
	.long	.L593-.L592
	.long	.L593-.L592
	.long	.L593-.L592
	.long	.L593-.L592
	.long	.L593-.L592
	.long	.L593-.L592
	.long	.L593-.L592
	.long	.L593-.L592
	.long	.L591-.L592
	.long	.L591-.L592
	.long	.L591-.L592
	.long	.L591-.L592
	.long	.L591-.L592
	.long	.L591-.L592
	.long	.L591-.L592
	.long	.L591-.L592
	.section	.text._ZN2v88internal12Deserializer8ReadDataINS0_19FullMaybeObjectSlotEEEbT_S4_NS0_13SnapshotSpaceEm,"axG",@progbits,_ZN2v88internal12Deserializer8ReadDataINS0_19FullMaybeObjectSlotEEEbT_S4_NS0_13SnapshotSpaceEm,comdat
	.p2align 4,,10
	.p2align 3
.L594:
	leal	-760(,%r15,8), %r8d
	cltq
	movslq	%r8d, %rdx
	leaq	(%rdi,%rax), %rsi
	cmpq	$8, %rdx
	jnb	.L656
	testq	%rdx, %rdx
	jne	.L778
	addl	%r8d, 124(%rbx)
	addq	%rdx, %r14
.L789:
	cmpq	%r12, %r14
	jb	.L779
	.p2align 4,,10
	.p2align 3
.L590:
	movl	$1, %eax
	jne	.L780
.L587:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L781
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L595:
	.cfi_restore_state
	movq	-96(%rbp), %rax
	andl	$31, %esi
	movq	56(%rax,%rsi,8), %rax
.L756:
	movq	%rax, (%r14)
	addq	$8, %r14
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L593:
	cmpl	$127, %r15d
	jle	.L782
	xorl	%r8d, %r8d
	leaq	-64(%rbp), %rsi
	leaq	-56(%rbp), %rdx
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	$0, -64(%rbp)
	call	_ZN2v88internal12Deserializer8ReadDataINS0_19FullMaybeObjectSlotEEEbT_S4_NS0_13SnapshotSpaceEm
	testb	%al, %al
	je	.L661
	leal	-126(%r15), %esi
	movq	-64(%rbp), %rcx
	movq	%r14, %rax
	movl	%esi, %edx
	shrl	%edx
	movq	%rcx, %xmm0
	salq	$4, %rdx
	punpcklqdq	%xmm0, %xmm0
	addq	%r14, %rdx
	.p2align 4,,10
	.p2align 3
.L663:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L663
	movl	%esi, %edx
	andl	$-2, %edx
	movl	%edx, %eax
	leaq	(%r14,%rax,8), %rax
	cmpl	%esi, %edx
	je	.L664
	movq	%rcx, (%rax)
.L664:
	leal	-127(%r15), %eax
	leaq	8(%r14,%rax,8), %r14
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L591:
	andl	$7, %esi
	movzbl	524(%rbx), %ecx
	leaq	8(%r14), %r15
	movq	8(%rbx,%rsi,8), %rdx
	movb	$0, 524(%rbx)
	movq	%rdx, %rax
	orq	$2, %rax
	testb	%cl, %cl
	cmove	%rdx, %rax
	cmpb	$0, -81(%rbp)
	movq	%rax, (%r14)
	je	.L671
	testb	$1, %dl
	je	.L671
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L770
	.p2align 4,,10
	.p2align 3
.L671:
	movq	%r15, %r14
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L612:
	movzbl	524(%rbx), %eax
	andl	$7, %esi
	movb	$0, 524(%rbx)
	movq	%rbx, %rdi
	cmpl	$1, %esi
	sete	%r15b
	testb	%al, %al
	jne	.L629
	call	_ZN2v88internal12Deserializer23GetBackReferencedObjectENS0_13SnapshotSpaceE
.L630:
	movq	%rax, (%r14)
	leaq	8(%r14), %rcx
	testb	%r15b, %r15b
	je	.L631
	cmpb	$0, -81(%rbp)
	je	.L631
	testb	$1, %al
	je	.L631
	cmpl	$3, %eax
	je	.L631
	movq	%rax, %rdx
	andq	$-262144, %rax
	andq	$-3, %rdx
	testb	$24, 8(%rax)
	je	.L631
	movq	-112(%rbp), %rax
	testb	$24, 8(%rax)
	je	.L783
	.p2align 4,,10
	.p2align 3
.L631:
	movq	%rcx, %r14
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L614:
	movzbl	524(%rbx), %eax
	cmpl	$1, %r15d
	movb	$0, 524(%rbx)
	movq	%rbx, %rdi
	sete	-128(%rbp)
	testb	%al, %al
	jne	.L621
	call	_ZN2v88internal12Deserializer10ReadObjectENS0_13SnapshotSpaceE
.L622:
	cmpb	$0, -81(%rbp)
	movq	%rax, (%r14)
	leaq	8(%r14), %r15
	je	.L671
	cmpb	$0, -128(%rbp)
	je	.L671
.L770:
	testb	$1, %al
	je	.L671
	cmpl	$3, %eax
	je	.L671
	movq	%rax, %rdx
	andq	$-262144, %rax
	andq	$-3, %rdx
	testb	$24, 8(%rax)
	je	.L671
.L758:
	movq	-112(%rbp), %rax
	testb	$24, 8(%rax)
	jne	.L671
.L786:
	movq	-120(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r15, %r14
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L605:
	leal	-22(%r15), %r9d
	movl	%r9d, 520(%rbx)
	jmp	.L730
.L615:
	movzbl	524(%rbx), %eax
	movl	$1, %esi
	movb	$0, 524(%rbx)
	movq	%rbx, %rdi
	testb	%al, %al
	jne	.L784
	call	_ZN2v88internal12Deserializer10ReadObjectENS0_13SnapshotSpaceE
.L626:
	movq	%rax, (%r14)
	cmpb	$0, -81(%rbp)
	leaq	8(%r14), %r15
	jne	.L770
	movq	%r15, %r14
	jmp	.L730
.L611:
	movslq	%eax, %rcx
	movzbl	524(%rbx), %r8d
	movb	$0, 524(%rbx)
	movzbl	1(%rdi,%rcx), %edx
	movzbl	2(%rdi,%rcx), %esi
	movzbl	(%rdi,%rcx), %r9d
	sall	$16, %esi
	sall	$8, %edx
	orl	%esi, %edx
	movzbl	3(%rdi,%rcx), %esi
	movl	$32, %ecx
	orl	%r9d, %edx
	sall	$24, %esi
	movl	%esi, %edi
	orl	%edx, %edi
	andl	$3, %edx
	addl	$1, %edx
	addl	%edx, %eax
	sall	$3, %edx
	movl	%eax, 124(%rbx)
	subl	%edx, %ecx
	movl	$-1, %eax
	shrl	%cl, %eax
	movl	%eax, %esi
	andl	%edi, %esi
	movq	-96(%rbp), %rdi
	shrl	$2, %esi
	movq	45496(%rdi), %rax
	movq	45504(%rdi), %rdi
	movq	%rdi, %rdx
	movq	%rdi, -128(%rbp)
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L785
	movq	(%rax,%rsi,8), %rdx
	leaq	8(%r14), %r15
	movq	%rdx, %rax
	movq	%rdx, %rsi
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	setne	%cl
	orq	$2, %rsi
	testb	%r8b, %r8b
	cmovne	%rsi, %rdx
	movq	%rdx, (%r14)
	testb	%cl, %cl
	je	.L671
.L774:
	cmpb	$0, -81(%rbp)
	je	.L671
	testb	$1, %dl
	je	.L671
	cmpl	$3, %edx
	je	.L671
	andq	$-3, %rdx
	testb	$24, 8(%rax)
	je	.L671
	movq	-112(%rbp), %rax
	testb	$24, 8(%rax)
	jne	.L671
	jmp	.L786
.L613:
	movzbl	524(%rbx), %eax
	movl	$1, %esi
	movb	$0, 524(%rbx)
	movq	%rbx, %rdi
	testb	%al, %al
	jne	.L625
	call	_ZN2v88internal12Deserializer23GetBackReferencedObjectENS0_13SnapshotSpaceE
	jmp	.L626
.L598:
	movq	$3, (%r14)
	addq	$8, %r14
	jmp	.L730
.L606:
	movq	-104(%rbp), %rax
	movq	(%rax), %rdx
	xorl	%eax, %eax
	cmpw	$68, 11(%rdx)
	jne	.L587
	movq	-104(%rbp), %rbx
	movl	$76, %edx
	movw	%dx, 12(%rbx)
	jmp	.L587
.L604:
	movslq	%eax, %rcx
	movzbl	1(%rdi,%rcx), %edx
	movzbl	2(%rdi,%rcx), %esi
	movzbl	3(%rdi,%rcx), %r15d
	movzbl	(%rdi,%rcx), %r8d
	movl	$32, %ecx
	movq	%rbx, %rdi
	sall	$16, %esi
	sall	$8, %edx
	movq	$0, -72(%rbp)
	orl	%esi, %edx
	sall	$24, %r15d
	orl	%r8d, %edx
	movl	%r15d, %esi
	xorl	%r8d, %r8d
	orl	%edx, %esi
	andl	$3, %edx
	addl	$1, %edx
	addl	%edx, %eax
	sall	$3, %edx
	subl	%edx, %ecx
	movl	%eax, 124(%rbx)
	movl	$-1, %eax
	leaq	-64(%rbp), %rdx
	shrl	%cl, %eax
	movl	$1, %ecx
	movl	%eax, %r15d
	andl	%esi, %r15d
	leaq	-72(%rbp), %rsi
	shrl	$2, %r15d
	call	_ZN2v88internal12Deserializer8ReadDataINS0_19FullMaybeObjectSlotEEEbT_S4_NS0_13SnapshotSpaceEm
	testb	%al, %al
	je	.L661
	leal	18(%r15), %esi
	movq	-72(%rbp), %rcx
	movq	%r14, %rax
	movl	%esi, %edx
	shrl	%edx
	movq	%rcx, %xmm0
	salq	$4, %rdx
	punpcklqdq	%xmm0, %xmm0
	addq	%r14, %rdx
	.p2align 4,,10
	.p2align 3
.L646:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L646
	movl	%esi, %edx
	andl	$-2, %edx
	movl	%edx, %eax
	leaq	(%r14,%rax,8), %rax
	cmpl	%esi, %edx
	je	.L647
	movq	%rcx, (%rax)
.L647:
	addl	$17, %r15d
	leaq	8(%r14,%r15,8), %r14
	jmp	.L730
.L597:
	movb	$1, 524(%rbx)
	jmp	.L730
.L607:
	addl	$2, %ecx
	cltq
	movl	%ecx, 124(%rbx)
	movzbl	(%rdi,%rax), %esi
	leaq	328(%rbx), %rdi
	call	_ZN2v88internal21DeserializerAllocator15MoveToNextChunkENS0_13SnapshotSpaceE@PLT
	jmp	.L730
.L608:
	movslq	%eax, %rcx
	movzbl	524(%rbx), %r15d
	movb	$0, 524(%rbx)
	movzbl	1(%rdi,%rcx), %edx
	movzbl	2(%rdi,%rcx), %esi
	movzbl	(%rdi,%rcx), %r8d
	sall	$16, %esi
	sall	$8, %edx
	orl	%esi, %edx
	movzbl	3(%rdi,%rcx), %esi
	movl	$32, %ecx
	orl	%r8d, %edx
	sall	$24, %esi
	movl	%esi, %edi
	orl	%edx, %edi
	andl	$3, %edx
	addl	$1, %edx
	addl	%edx, %eax
	sall	$3, %edx
	subl	%edx, %ecx
	movl	%eax, 124(%rbx)
	movl	$-1, %eax
	shrl	%cl, %eax
	movl	%eax, %esi
	movq	-96(%rbp), %rax
	andl	%edi, %esi
	movq	40792(%rax), %rdi
	shrl	$2, %esi
	call	_ZNK2v88internal12ReadOnlyHeap23cached_read_only_objectEm@PLT
	movq	%rax, %rdx
	orq	$2, %rdx
	testb	%r15b, %r15b
	cmovne	%rdx, %rax
	addq	$8, %r14
	movq	%rax, -8(%r14)
	jmp	.L730
.L609:
	movslq	%eax, %rcx
	movzbl	524(%rbx), %esi
	leaq	8(%r14), %r15
	movb	$0, 524(%rbx)
	movzbl	1(%rdi,%rcx), %edx
	movzbl	2(%rdi,%rcx), %r8d
	movzbl	(%rdi,%rcx), %r9d
	movzbl	3(%rdi,%rcx), %edi
	movl	$32, %ecx
	sall	$8, %edx
	sall	$16, %r8d
	orl	%r8d, %edx
	sall	$24, %edi
	orl	%r9d, %edx
	orl	%edx, %edi
	andl	$3, %edx
	addl	$1, %edx
	addl	%edx, %eax
	sall	$3, %edx
	movl	%eax, 124(%rbx)
	subl	%edx, %ecx
	movl	$-1, %eax
	movq	88(%rbx), %rdx
	shrl	%cl, %eax
	andl	%edi, %eax
	shrl	$2, %eax
	movq	(%rdx,%rax,8), %rax
	movq	(%rax), %rdx
	movq	%rdx, %rax
	movq	%rdx, %rdi
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	setne	%cl
	orq	$2, %rdi
	testb	%sil, %sil
	cmovne	%rdi, %rdx
	movq	%rdx, (%r14)
	testb	%cl, %cl
	jne	.L774
	movq	%r15, %r14
	jmp	.L730
.L610:
	movslq	%eax, %rcx
	movzbl	524(%rbx), %esi
	leaq	8(%r14), %r15
	movb	$0, 524(%rbx)
	movzbl	1(%rdi,%rcx), %edx
	movzbl	2(%rdi,%rcx), %r8d
	movzbl	(%rdi,%rcx), %r9d
	movzbl	3(%rdi,%rcx), %edi
	movl	$32, %ecx
	sall	$16, %r8d
	sall	$8, %edx
	orl	%r8d, %edx
	sall	$24, %edi
	movslq	72(%rbx), %r8
	orl	%r9d, %edx
	orl	%edx, %edi
	andl	$3, %edx
	addl	$1, %edx
	addl	%edx, %eax
	sall	$3, %edx
	movl	%eax, 124(%rbx)
	subl	%edx, %ecx
	movl	$-1, %eax
	shrl	%cl, %eax
	andl	%edi, %eax
	movq	-96(%rbp), %rdi
	shrl	$2, %eax
	movzwl	%ax, %eax
	movq	56(%rdi,%rax,8), %rdx
	movq	%r8, %rax
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	testb	$24, 8(%rcx)
	movq	%rdx, 8(%rbx,%r8,8)
	setne	%dil
	addl	$1, %eax
	andl	$7, %eax
	movl	%eax, 72(%rbx)
	movq	%rdx, %rax
	orq	$2, %rax
	testb	%sil, %sil
	cmovne	%rax, %rdx
	movq	%rdx, (%r14)
	testb	%dil, %dil
	je	.L671
	cmpb	$0, -81(%rbp)
	je	.L671
	testb	$1, %dl
	je	.L671
	cmpl	$3, %edx
	je	.L671
	andq	$-3, %rdx
	testb	$24, 8(%rcx)
	jne	.L758
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L600:
	movslq	%eax, %rcx
	movzbl	1(%rdi,%rcx), %edx
	movzbl	2(%rdi,%rcx), %esi
	movzbl	(%rdi,%rcx), %r8d
	movzbl	3(%rdi,%rcx), %ecx
	sall	$16, %esi
	sall	$8, %edx
	orl	%esi, %edx
	sall	$24, %ecx
	orl	%r8d, %edx
	orl	%edx, %ecx
	andl	$3, %edx
	addl	$1, %edx
	movl	%ecx, %esi
	addl	%edx, %eax
	movl	%eax, 124(%rbx)
	movq	-96(%rbp), %rax
	movq	41728(%rax), %rdi
	leaq	_ZN2v88internalL28NoExternalReferencesCallbackEv(%rip), %rax
	testq	%rdi, %rdi
	je	.L756
	sall	$3, %edx
	movl	$32, %ecx
	movl	$-1, %eax
	subl	%edx, %ecx
	shrl	%cl, %eax
	andl	%esi, %eax
	shrl	$2, %eax
	movq	(%rdi,%rax,8), %rax
	jmp	.L756
.L601:
	movslq	%eax, %rcx
	movzbl	1(%rdi,%rcx), %edx
	movzbl	2(%rdi,%rcx), %esi
	movzbl	3(%rdi,%rcx), %r15d
	movzbl	(%rdi,%rcx), %r8d
	movl	$32, %ecx
	sall	$16, %esi
	sall	$8, %edx
	orl	%esi, %edx
	sall	$24, %r15d
	orl	%r8d, %edx
	movl	%r15d, %r8d
	orl	%edx, %r8d
	andl	$3, %edx
	addl	$1, %edx
	leal	(%rax,%rdx), %esi
	sall	$3, %edx
	subl	%edx, %ecx
	movl	$-1, %edx
	movl	%esi, 124(%rbx)
	movslq	%esi, %rsi
	shrl	%cl, %edx
	addq	%rdi, %rsi
	movq	%r14, %rdi
	movl	%edx, %r15d
	andl	%r8d, %r15d
	shrl	$2, %r15d
	movl	%r15d, %edx
	movq	%rdx, -128(%rbp)
	call	memcpy@PLT
	movq	-128(%rbp), %rdx
	addl	%r15d, 124(%rbx)
	addq	%rdx, %r14
	jmp	.L730
.L602:
	movslq	%eax, %rcx
	movzbl	1(%rdi,%rcx), %edx
	movzbl	2(%rdi,%rcx), %esi
	movzbl	3(%rdi,%rcx), %r15d
	movzbl	(%rdi,%rcx), %r8d
	movl	$32, %ecx
	sall	$16, %esi
	sall	$8, %edx
	orl	%esi, %edx
	sall	$24, %r15d
	orl	%r8d, %edx
	movl	%r15d, %r8d
	orl	%edx, %r8d
	andl	$3, %edx
	addl	$1, %edx
	leal	(%rax,%rdx), %esi
	sall	$3, %edx
	movq	-104(%rbp), %rax
	subl	%edx, %ecx
	movl	$-1, %edx
	movl	%esi, 124(%rbx)
	movslq	%esi, %rsi
	shrl	%cl, %edx
	addq	%rdi, %rsi
	movl	%edx, %r15d
	andl	%r8d, %r15d
	leaq	40(%rax), %r8
	shrl	$2, %r15d
	movq	%r8, %rdi
	movl	%r15d, %ecx
	movq	%rcx, %rdx
	movq	%rcx, -128(%rbp)
	call	memcpy@PLT
	addl	%r15d, 124(%rbx)
	movq	-104(%rbp), %rdx
	movq	%rbx, %rdi
	movl	-88(%rbp), %esi
	call	_ZN2v88internal12Deserializer18ReadCodeObjectBodyENS0_13SnapshotSpaceEm
	movq	-128(%rbp), %rcx
	leaq	32(%r14,%rcx), %rax
	cmpq	%r12, %rax
	jne	.L787
	movq	%r12, %r14
	jmp	.L730
.L603:
	movslq	%eax, %rcx
	movzbl	1(%rdi,%rcx), %edx
	movzbl	2(%rdi,%rcx), %esi
	movzbl	(%rdi,%rcx), %r8d
	sall	$16, %esi
	sall	$8, %edx
	orl	%esi, %edx
	movzbl	3(%rdi,%rcx), %esi
	movl	$32, %ecx
	orl	%r8d, %edx
	sall	$24, %esi
	orl	%edx, %esi
	andl	$3, %edx
	addl	$1, %edx
	addl	%edx, %eax
	sall	$3, %edx
	movl	%eax, 124(%rbx)
	subl	%edx, %ecx
	movl	$-1, %eax
	shrl	%cl, %eax
	andl	%esi, %eax
	shrl	$2, %eax
	movl	%eax, %edx
	movq	-96(%rbp), %rax
	movq	%rdx, -128(%rbp)
	movq	%rdx, %r15
	movq	%rdx, %rsi
	movq	45544(%rax), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-128(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, -64(%rbp)
	movq	%rax, %rdi
	je	.L788
	movslq	124(%rbx), %rsi
	addq	112(%rbx), %rsi
	call	memcpy@PLT
	addl	%r15d, 124(%rbx)
	movq	312(%rbx), %rsi
	cmpq	320(%rbx), %rsi
	je	.L649
	movq	-64(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 312(%rbx)
	jmp	.L730
.L599:
	movslq	%eax, %rcx
	addq	$8, %r14
	movzbl	1(%rdi,%rcx), %edx
	movzbl	2(%rdi,%rcx), %esi
	movzbl	(%rdi,%rcx), %r8d
	sall	$16, %esi
	sall	$8, %edx
	orl	%esi, %edx
	movzbl	3(%rdi,%rcx), %esi
	movl	$32, %ecx
	orl	%r8d, %edx
	sall	$24, %esi
	orl	%edx, %esi
	andl	$3, %edx
	addl	$1, %edx
	addl	%edx, %eax
	sall	$3, %edx
	movl	%eax, 124(%rbx)
	subl	%edx, %ecx
	movl	$-1, %eax
	movq	80(%rbx), %rdx
	shrl	%cl, %eax
	andl	%esi, %eax
	shrl	$2, %eax
	movq	4856(%rdx,%rax,8), %rax
	movq	%rax, -8(%r14)
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L621:
	call	_ZN2v88internal12Deserializer10ReadObjectENS0_13SnapshotSpaceE
	orq	$2, %rax
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L784:
	call	_ZN2v88internal12Deserializer10ReadObjectENS0_13SnapshotSpaceE
	orq	$2, %rax
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L625:
	call	_ZN2v88internal12Deserializer23GetBackReferencedObjectENS0_13SnapshotSpaceE
	orq	$2, %rax
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L629:
	call	_ZN2v88internal12Deserializer23GetBackReferencedObjectENS0_13SnapshotSpaceE
	orq	$2, %rax
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L656:
	movq	(%rsi), %rax
	leaq	8(%r14), %rdi
	movq	%r14, %rcx
	andq	$-8, %rdi
	movq	%rax, (%r14)
	subq	%rdi, %rcx
	movq	-8(%rsi,%rdx), %rax
	subq	%rcx, %rsi
	addq	%rdx, %rcx
	movq	%rax, -8(%r14,%rdx)
	shrq	$3, %rcx
	addq	%rdx, %r14
	rep movsq
	addl	%r8d, 124(%rbx)
	jmp	.L789
.L778:
	movzbl	(%rsi), %eax
	movb	%al, (%r14)
	addq	%rdx, %r14
	addl	%r8d, 124(%rbx)
	jmp	.L789
.L661:
	leaq	.LC17(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L649:
	leaq	-64(%rbp), %rdx
	leaq	304(%rbx), %rdi
	call	_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L596:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L783:
	movq	-120(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rcx, -128(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-128(%rbp), %rcx
	jmp	.L631
.L780:
	leaq	.LC20(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L787:
	leaq	.LC16(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L788:
	leaq	.LC18(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L782:
	leaq	.LC19(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L781:
	call	__stack_chk_fail@PLT
.L785:
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24024:
	.size	_ZN2v88internal12Deserializer8ReadDataINS0_19FullMaybeObjectSlotEEEbT_S4_NS0_13SnapshotSpaceEm, .-_ZN2v88internal12Deserializer8ReadDataINS0_19FullMaybeObjectSlotEEEbT_S4_NS0_13SnapshotSpaceEm
	.section	.text._ZN2v88internal12Deserializer10ReadObjectENS0_13SnapshotSpaceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Deserializer10ReadObjectENS0_13SnapshotSpaceE
	.type	_ZN2v88internal12Deserializer10ReadObjectENS0_13SnapshotSpaceE, @function
_ZN2v88internal12Deserializer10ReadObjectENS0_13SnapshotSpaceE:
.LFB21741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movslq	124(%rdi), %rdx
	movq	112(%rdi), %rsi
	movzbl	1(%rsi,%rdx), %eax
	movzbl	2(%rsi,%rdx), %edi
	movq	%rdx, %rcx
	movzbl	(%rsi,%rdx), %r8d
	movzbl	3(%rsi,%rdx), %ebx
	movl	%r12d, %esi
	sall	$16, %edi
	sall	$8, %eax
	orl	%edi, %eax
	sall	$24, %ebx
	leaq	328(%r13), %rdi
	orl	%r8d, %eax
	orl	%eax, %ebx
	andl	$3, %eax
	addl	$1, %eax
	movl	%ebx, %edx
	addl	%eax, %ecx
	sall	$3, %eax
	movl	%ecx, 124(%r13)
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	$-1, %eax
	shrl	%cl, %eax
	movl	%eax, %ebx
	andl	%edx, %ebx
	shrl	$2, %ebx
	sall	$3, %ebx
	movl	%ebx, %edx
	call	_ZN2v88internal21DeserializerAllocator8AllocateENS0_13SnapshotSpaceEi@PLT
	movq	80(%r13), %r8
	movq	%rax, %r15
	leaq	1(%rax), %r14
	movq	40776(%r8), %rcx
	movq	40768(%r8), %rax
	cmpq	%rcx, %rax
	je	.L791
	.p2align 4,,10
	.p2align 3
.L792:
	movq	(%rax), %rdi
	movq	%rcx, -72(%rbp)
	movl	%ebx, %edx
	movq	%r15, %rsi
	movq	%r8, -64(%rbp)
	movq	(%rdi), %r9
	movq	%rax, -56(%rbp)
	call	*(%r9)
	movq	-56(%rbp), %rax
	movq	-72(%rbp), %rcx
	movq	-64(%rbp), %r8
	addq	$8, %rax
	cmpq	%rax, %rcx
	jne	.L792
.L791:
	cmpb	$0, _ZN2v88internal23FLAG_fuzzer_gc_analysisE(%rip)
	je	.L793
	addl	$1, 37992(%r8)
.L794:
	movslq	%ebx, %rdx
	movq	%r15, %r8
	movl	%r12d, %ecx
	movq	%r15, %rsi
	addq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal12Deserializer8ReadDataINS0_19FullMaybeObjectSlotEEEbT_S4_NS0_13SnapshotSpaceEm
	testb	%al, %al
	jne	.L803
.L797:
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L793:
	.cfi_restore_state
	movl	_ZN2v88internal36FLAG_trace_allocation_stack_intervalE(%rip), %ecx
	testl	%ecx, %ecx
	jle	.L794
	movl	37992(%r8), %eax
	xorl	%edx, %edx
	addl	$1, %eax
	movl	%eax, 37992(%r8)
	divl	%ecx
	testl	%edx, %edx
	jne	.L794
	movq	stdout(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE@PLT
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L803:
	movq	%r14, %rsi
	movl	%r12d, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal12Deserializer20PostProcessNewObjectENS0_10HeapObjectENS0_13SnapshotSpaceE
	movq	%rax, %r14
	jmp	.L797
	.cfi_endproc
.LFE21741:
	.size	_ZN2v88internal12Deserializer10ReadObjectENS0_13SnapshotSpaceE, .-_ZN2v88internal12Deserializer10ReadObjectENS0_13SnapshotSpaceE
	.section	.text._ZN2v88internal12Deserializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Deserializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.type	_ZN2v88internal12Deserializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_, @function
_ZN2v88internal12Deserializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_:
.LFB21725:
	.cfi_startproc
	endbr64
	movq	%rcx, %rsi
	movq	%r8, %rdx
	movl	$1, %ecx
	xorl	%r8d, %r8d
	jmp	_ZN2v88internal12Deserializer8ReadDataINS0_19FullMaybeObjectSlotEEEbT_S4_NS0_13SnapshotSpaceEm
	.cfi_endproc
.LFE21725:
	.size	_ZN2v88internal12Deserializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_, .-_ZN2v88internal12Deserializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.section	.text._ZN2v88internal12Deserializer10ReadObjectEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Deserializer10ReadObjectEv
	.type	_ZN2v88internal12Deserializer10ReadObjectEv, @function
_ZN2v88internal12Deserializer10ReadObjectEv:
.LFB21740:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-16(%rbp), %rsi
	leaq	-8(%rbp), %rdx
	movq	$0, -16(%rbp)
	call	_ZN2v88internal12Deserializer8ReadDataINS0_19FullMaybeObjectSlotEEEbT_S4_NS0_13SnapshotSpaceEm
	testb	%al, %al
	je	.L809
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	movq	-16(%rbp), %rax
	jne	.L810
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L809:
	.cfi_restore_state
	leaq	.LC17(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L810:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21740:
	.size	_ZN2v88internal12Deserializer10ReadObjectEv, .-_ZN2v88internal12Deserializer10ReadObjectEv
	.section	.text._ZN2v88internal12Deserializer26DeserializeDeferredObjectsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Deserializer26DeserializeDeferredObjectsEv
	.type	_ZN2v88internal12Deserializer26DeserializeDeferredObjectsEv, @function
_ZN2v88internal12Deserializer26DeserializeDeferredObjectsEv:
.LFB21727:
	.cfi_startproc
	endbr64
	movl	124(%rdi), %edx
	movq	112(%rdi), %rsi
	movslq	%edx, %rax
	addl	$1, %edx
	movl	%edx, 124(%rdi)
	movzbl	(%rsi,%rax), %eax
	cmpl	$26, %eax
	je	.L822
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	$32, %ebx
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L826:
	subl	$22, %eax
	movl	%eax, 520(%r13)
.L814:
	movslq	%edx, %rax
	addl	$1, %edx
	movl	%edx, 124(%r13)
	movzbl	(%rsi,%rax), %eax
	cmpl	$26, %eax
	je	.L825
.L816:
	leal	-23(%rax), %ecx
	cmpl	$2, %ecx
	jbe	.L826
	andl	$7, %eax
	movq	%r13, %rdi
	movl	%eax, %esi
	movl	%eax, %r12d
	call	_ZN2v88internal12Deserializer23GetBackReferencedObjectENS0_13SnapshotSpaceE
	movslq	124(%r13), %rdx
	movq	112(%r13), %rsi
	movq	%rax, %r14
	movzbl	2(%rsi,%rdx), %edi
	movzbl	1(%rsi,%rdx), %eax
	movq	%rdx, %rcx
	movzbl	(%rsi,%rdx), %r8d
	movzbl	3(%rsi,%rdx), %edx
	sall	$16, %edi
	sall	$8, %eax
	orl	%edi, %eax
	sall	$24, %edx
	movq	%r13, %rdi
	orl	%r8d, %eax
	movl	%edx, %esi
	leaq	-1(%r14), %r8
	orl	%eax, %esi
	andl	$3, %eax
	addl	$1, %eax
	addl	%eax, %ecx
	sall	$3, %eax
	movl	%ecx, 124(%r13)
	movl	%ebx, %ecx
	subl	%eax, %ecx
	movl	$-1, %eax
	shrl	%cl, %eax
	movl	%r12d, %ecx
	movl	%eax, %edx
	andl	%esi, %edx
	leaq	7(%r14), %rsi
	shrl	$2, %edx
	sall	$3, %edx
	movslq	%edx, %rdx
	addq	%r8, %rdx
	call	_ZN2v88internal12Deserializer8ReadDataINS0_19FullMaybeObjectSlotEEEbT_S4_NS0_13SnapshotSpaceEm
	testb	%al, %al
	je	.L827
	movl	%r12d, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal12Deserializer20PostProcessNewObjectENS0_10HeapObjectENS0_13SnapshotSpaceE
	movq	112(%r13), %rsi
	movl	124(%r13), %edx
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L825:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L827:
	.cfi_restore_state
	leaq	.LC17(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L822:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE21727:
	.size	_ZN2v88internal12Deserializer26DeserializeDeferredObjectsEv, .-_ZN2v88internal12Deserializer26DeserializeDeferredObjectsEv
	.section	.text._ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.type	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, @function
_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE:
.LFB7245:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	%esi, %r9d
	leaq	8(%rcx), %r10
	movq	%rcx, %rsi
	leaq	_ZN2v88internal12Deserializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_(%rip), %rcx
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L829
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r10, %rdx
	jmp	_ZN2v88internal12Deserializer8ReadDataINS0_19FullMaybeObjectSlotEEEbT_S4_NS0_13SnapshotSpaceEm
	.p2align 4,,10
	.p2align 3
.L829:
	movq	%rsi, %rcx
	movq	%r10, %r8
	movl	%r9d, %esi
	jmp	*%rax
	.cfi_endproc
.LFE7245:
	.size	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, .-_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.section	.text._ZN2v88internal12Deserializer15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Deserializer15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal12Deserializer15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal12Deserializer15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE:
.LFB21743:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	leaq	-32(%rbp), %rsi
	leaq	-24(%rbp), %rdx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -32(%rbp)
	call	_ZN2v88internal12Deserializer8ReadDataINS0_19FullMaybeObjectSlotEEEbT_S4_NS0_13SnapshotSpaceEm
	testb	%al, %al
	je	.L834
	movq	-32(%rbp), %rax
	xorl	%ecx, %ecx
	movl	$4, %edx
	movq	%r12, %rdi
	leaq	63(%rax), %rsi
	call	_ZN2v88internal9RelocInfo18set_target_addressEmNS0_16WriteBarrierModeENS0_15ICacheFlushModeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L835
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L834:
	.cfi_restore_state
	leaq	.LC17(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L835:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21743:
	.size	_ZN2v88internal12Deserializer15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal12Deserializer15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal12Deserializer20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Deserializer20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal12Deserializer20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal12Deserializer20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE:
.LFB21744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	leaq	-48(%rbp), %rsi
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	leaq	-40(%rbp), %rdx
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	call	_ZN2v88internal12Deserializer8ReadDataINS0_19FullMaybeObjectSlotEEEbT_S4_NS0_13SnapshotSpaceEm
	testb	%al, %al
	je	.L853
	movq	-48(%rbp), %r13
	movq	(%r12), %rax
	movl	$8, %esi
	movq	%r13, (%rax)
	movq	(%r12), %rdi
	call	_ZN2v88internal21FlushInstructionCacheEPvm@PLT
	movq	24(%r12), %r14
	testq	%r14, %r14
	jne	.L854
.L836:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L855
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L854:
	.cfi_restore_state
	testb	$1, %r13b
	je	.L836
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testb	$24, %al
	je	.L840
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal35Heap_GenerationalBarrierForCodeSlowENS0_4CodeEPNS0_9RelocInfoENS0_10HeapObjectE@PLT
	movq	8(%rbx), %rax
.L840:
	testl	$262144, %eax
	je	.L836
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal30Heap_MarkingBarrierForCodeSlowENS0_4CodeEPNS0_9RelocInfoENS0_10HeapObjectE@PLT
	jmp	.L836
	.p2align 4,,10
	.p2align 3
.L853:
	leaq	.LC17(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L855:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21744:
	.size	_ZN2v88internal12Deserializer20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal12Deserializer20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal12Deserializer18ReadCodeObjectBodyENS0_13SnapshotSpaceEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Deserializer18ReadCodeObjectBodyENS0_13SnapshotSpaceEm
	.type	_ZN2v88internal12Deserializer18ReadCodeObjectBodyENS0_13SnapshotSpaceEm, @function
_ZN2v88internal12Deserializer18ReadCodeObjectBodyENS0_13SnapshotSpaceEm:
.LFB21742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	leaq	8(%rdx), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	addq	$40, %rdx
	pushq	%rbx
	movq	%r12, %r8
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal12Deserializer8ReadDataINS0_19FullMaybeObjectSlotEEEbT_S4_NS0_13SnapshotSpaceEm
	testb	%al, %al
	je	.L862
	leaq	1(%r12), %rsi
	leaq	-112(%rbp), %r12
	movl	$1999, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal13RelocIteratorC1ENS0_4CodeEi@PLT
	cmpb	$0, -56(%rbp)
	jne	.L856
	leaq	-160(%rbp), %r13
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L859:
	cmpb	$1, %al
	jle	.L874
	cmpb	$7, %al
	je	.L875
	leal	-8(%rax), %edx
	cmpb	$1, %dl
	jbe	.L876
	cmpb	$6, %al
	je	.L877
	cmpb	$10, %al
	je	.L878
.L860:
	movq	%r12, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, -56(%rbp)
	jne	.L856
.L867:
	movq	-64(%rbp), %rax
	movdqa	-96(%rbp), %xmm0
	movdqa	-80(%rbp), %xmm1
	movq	%rax, -128(%rbp)
	movaps	%xmm0, -160(%rbp)
	movzbl	-152(%rbp), %eax
	movaps	%xmm1, -144(%rbp)
	leal	-2(%rax), %edx
	cmpb	$1, %dl
	ja	.L859
	movq	-136(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal12Deserializer20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	movq	%r12, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, -56(%rbp)
	je	.L867
.L856:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L879
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L876:
	.cfi_restore_state
	movslq	124(%rbx), %rax
	movq	112(%rbx), %rsi
	movq	-136(%rbp), %rdx
	leal	1(%rax), %ecx
	movl	%ecx, 124(%rbx)
	cmpb	$34, (%rsi,%rax)
	jne	.L880
	movslq	%ecx, %rdi
	movzbl	1(%rsi,%rdi), %eax
	movzbl	2(%rsi,%rdi), %r8d
	movzbl	(%rsi,%rdi), %r9d
	movzbl	3(%rsi,%rdi), %esi
	sall	$8, %eax
	sall	$16, %r8d
	orl	%r8d, %eax
	sall	$24, %esi
	orl	%r9d, %eax
	orl	%eax, %esi
	andl	$3, %eax
	addl	$1, %eax
	addl	%eax, %ecx
	sall	$3, %eax
	movl	%ecx, 124(%rbx)
	movl	$32, %ecx
	subl	%eax, %ecx
	movl	$-1, %eax
	shrl	%cl, %eax
	andl	%esi, %eax
	shrl	$2, %eax
	leaq	63(%rdx,%rax), %rdx
	movq	-160(%rbp), %rax
	movq	%rdx, (%rax)
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L874:
	xorl	%r8d, %r8d
	leaq	-168(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	$0, -168(%rbp)
	movl	$1, %ecx
	call	_ZN2v88internal12Deserializer8ReadDataINS0_19FullMaybeObjectSlotEEEbT_S4_NS0_13SnapshotSpaceEm
	testb	%al, %al
	je	.L862
	movq	-168(%rbp), %rax
	xorl	%ecx, %ecx
	movl	$4, %edx
	movq	%r13, %rdi
	leaq	63(%rax), %rsi
	call	_ZN2v88internal9RelocInfo18set_target_addressEmNS0_16WriteBarrierModeENS0_15ICacheFlushModeE@PLT
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L875:
	movq	-136(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal12Deserializer22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L878:
	movq	-136(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal12Deserializer18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L862:
	leaq	.LC17(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L880:
	leaq	.LC10(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L877:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L879:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21742:
	.size	_ZN2v88internal12Deserializer18ReadCodeObjectBodyENS0_13SnapshotSpaceEm, .-_ZN2v88internal12Deserializer18ReadCodeObjectBodyENS0_13SnapshotSpaceEm
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal12Deserializer10InitializeEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal12Deserializer10InitializeEPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal12Deserializer10InitializeEPNS0_7IsolateE:
.LFB27797:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE27797:
	.size	_GLOBAL__sub_I__ZN2v88internal12Deserializer10InitializeEPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal12Deserializer10InitializeEPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal12Deserializer10InitializeEPNS0_7IsolateE
	.weak	_ZTVN2v88internal12DeserializerE
	.section	.data.rel.ro.local._ZTVN2v88internal12DeserializerE,"awG",@progbits,_ZTVN2v88internal12DeserializerE,comdat
	.align 8
	.type	_ZTVN2v88internal12DeserializerE, @object
	.size	_ZTVN2v88internal12DeserializerE, 56
_ZTVN2v88internal12DeserializerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12DeserializerD1Ev
	.quad	_ZN2v88internal12DeserializerD0Ev
	.quad	_ZN2v88internal12Deserializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.quad	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.quad	_ZN2v88internal12Deserializer11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.weak	_ZTVN2v88internal23StringTableInsertionKeyE
	.section	.data.rel.ro.local._ZTVN2v88internal23StringTableInsertionKeyE,"awG",@progbits,_ZTVN2v88internal23StringTableInsertionKeyE,comdat
	.align 8
	.type	_ZTVN2v88internal23StringTableInsertionKeyE, @object
	.size	_ZTVN2v88internal23StringTableInsertionKeyE, 48
_ZTVN2v88internal23StringTableInsertionKeyE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23StringTableInsertionKeyD1Ev
	.quad	_ZN2v88internal23StringTableInsertionKeyD0Ev
	.quad	_ZN2v88internal23StringTableInsertionKey8AsHandleEPNS0_7IsolateE
	.quad	_ZN2v88internal23StringTableInsertionKey7IsMatchENS0_6StringE
	.section	.bss._ZZN2v88internal12Deserializer15LogScriptEventsENS0_6ScriptEE28trace_event_unique_atomic163,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal12Deserializer15LogScriptEventsENS0_6ScriptEE28trace_event_unique_atomic163, @object
	.size	_ZZN2v88internal12Deserializer15LogScriptEventsENS0_6ScriptEE28trace_event_unique_atomic163, 8
_ZZN2v88internal12Deserializer15LogScriptEventsENS0_6ScriptEE28trace_event_unique_atomic163:
	.zero	8
	.section	.bss._ZZN2v88internal12Deserializer15LogScriptEventsENS0_6ScriptEE28trace_event_unique_atomic160,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal12Deserializer15LogScriptEventsENS0_6ScriptEE28trace_event_unique_atomic160, @object
	.size	_ZZN2v88internal12Deserializer15LogScriptEventsENS0_6ScriptEE28trace_event_unique_atomic160, 8
_ZZN2v88internal12Deserializer15LogScriptEventsENS0_6ScriptEE28trace_event_unique_atomic160:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
