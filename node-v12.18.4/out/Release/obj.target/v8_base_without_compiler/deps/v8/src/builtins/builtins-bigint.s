	.file	"builtins-bigint.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.rodata._ZN2v88internal12_GLOBAL__N_115ThisBigIntValueEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKc.str1.1,"aMS",@progbits,1
.LC0:
	.string	"(location_) != nullptr"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal12_GLOBAL__N_115ThisBigIntValueEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKc,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_115ThisBigIntValueEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKc, @function
_ZN2v88internal12_GLOBAL__N_115ThisBigIntValueEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKc:
.LFB18670:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L20
.L12:
	movq	%rbx, %rdi
	leaq	2120(%r12), %r13
	call	strlen@PLT
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L21
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$92, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
.L10:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L22
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$66, 11(%rdx)
	je	.L8
	movq	-1(%rax), %rdx
	cmpw	$1041, 11(%rdx)
	jne	.L12
	movq	23(%rax), %rsi
	testb	$1, %sil
	je	.L12
	movq	-1(%rsi), %rax
	cmpw	$66, 11(%rax)
	jne	.L12
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L14
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rsi, %rax
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L21:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L14:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L23
.L16:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L10
.L23:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L16
.L22:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18670:
	.size	_ZN2v88internal12_GLOBAL__N_115ThisBigIntValueEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKc, .-_ZN2v88internal12_GLOBAL__N_115ThisBigIntValueEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKc
	.section	.text._ZN2v88internal12_GLOBAL__N_118BigIntToStringImplENS0_6HandleINS0_6ObjectEEES4_PNS0_7IsolateEPKc,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_118BigIntToStringImplENS0_6HandleINS0_6ObjectEEES4_PNS0_7IsolateEPKc, @function
_ZN2v88internal12_GLOBAL__N_118BigIntToStringImplENS0_6HandleINS0_6ObjectEEES4_PNS0_7IsolateEPKc:
.LFB18671:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	movq	%rdi, %rsi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	movq	%rcx, %rdx
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal12_GLOBAL__N_115ThisBigIntValueEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKc
	testq	%rax, %rax
	je	.L41
	movq	%rax, %r13
	movq	(%r14), %rax
	cmpq	%rax, 88(%r12)
	je	.L37
	testb	$1, %al
	jne	.L42
.L29:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L32:
	movsd	.LC2(%rip), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L33
	comisd	.LC3(%rip), %xmm0
	jbe	.L38
.L33:
	movq	%r12, %rdi
	xorl	%edx, %edx
	movl	$218, %esi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	(%rax), %rsi
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movq	312(%r12), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movl	$10, %edx
.L27:
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6BigInt8ToStringEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_11ShouldThrowE@PLT
	testq	%rax, %rax
	je	.L41
	movq	(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	je	.L41
	movq	(%rax), %rax
	testb	$1, %al
	je	.L29
	movsd	7(%rax), %xmm0
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L38:
	cvttsd2sil	%xmm0, %edx
	jmp	.L27
	.cfi_endproc
.LFE18671:
	.size	_ZN2v88internal12_GLOBAL__N_118BigIntToStringImplENS0_6HandleINS0_6ObjectEEES4_PNS0_7IsolateEPKc, .-_ZN2v88internal12_GLOBAL__N_118BigIntToStringImplENS0_6HandleINS0_6ObjectEEES4_PNS0_7IsolateEPKc
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L49
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L52
.L43:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L43
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL36Builtin_Impl_Stats_BigIntConstructorEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"disabled-by-default-v8.runtime"
	.section	.rodata._ZN2v88internalL36Builtin_Impl_Stats_BigIntConstructorEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"V8.Builtin_BigIntConstructor"
	.section	.text._ZN2v88internalL36Builtin_Impl_Stats_BigIntConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Builtin_Impl_Stats_BigIntConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL36Builtin_Impl_Stats_BigIntConstructorEiPmPNS0_7IsolateE:
.LFB18661:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L97
.L54:
	movq	_ZZN2v88internalL36Builtin_Impl_Stats_BigIntConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic17(%rip), %r13
	testq	%r13, %r13
	je	.L98
.L56:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L99
.L58:
	leal	-8(,%r14,8), %eax
	movq	%rbx, %rcx
	addl	$1, 41104(%r12)
	movq	41088(%r12), %r13
	cltq
	movq	41096(%r12), %r15
	subq	%rax, %rcx
	movq	%rcx, %rax
	movq	88(%r12), %rcx
	cmpq	%rcx, (%rax)
	jne	.L100
	subq	$8, %rbx
	leaq	88(%r12), %r8
	cmpl	$5, %r14d
	cmovg	%rbx, %r8
	movq	(%r8), %rax
	testb	$1, %al
	jne	.L101
.L67:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6BigInt10FromNumberEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L96
.L73:
	movq	(%rax), %r14
.L63:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %r15
	je	.L76
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L76:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L102
.L53:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L103
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	leaq	2120(%r12), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movl	$90, %esi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r14
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L99:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L104
.L59:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L60
	movq	(%rdi), %rax
	call	*8(%rax)
.L60:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L61
	movq	(%rdi), %rax
	call	*8(%rax)
.L61:
	leaq	.LC5(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L98:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L105
.L57:
	movq	%r13, _ZZN2v88internalL36Builtin_Impl_Stats_BigIntConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic17(%rip)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L101:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L69
	movq	%r8, %rdi
	movl	$1, %esi
	call	_ZN2v88internal10JSReceiver11ToPrimitiveENS0_6HandleIS1_EENS0_15ToPrimitiveHintE@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L96
.L69:
	movq	(%r8), %rax
	testb	$1, %al
	je	.L67
	movq	-1(%rax), %rax
	cmpw	$65, 11(%rax)
	je	.L67
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6BigInt10FromObjectEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	testq	%rax, %rax
	jne	.L73
	.p2align 4,,10
	.p2align 3
.L96:
	movq	312(%r12), %r14
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L97:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$687, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L102:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L105:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L104:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC5(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L59
.L103:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18661:
	.size	_ZN2v88internalL36Builtin_Impl_Stats_BigIntConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internalL36Builtin_Impl_Stats_BigIntConstructorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL32Builtin_Impl_Stats_BigIntAsUintNEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"V8.Builtin_BigIntAsUintN"
	.section	.text._ZN2v88internalL32Builtin_Impl_Stats_BigIntAsUintNEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Builtin_Impl_Stats_BigIntAsUintNEiPmPNS0_7IsolateE, @function
_ZN2v88internalL32Builtin_Impl_Stats_BigIntAsUintNEiPmPNS0_7IsolateE:
.LFB18664:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L154
.L107:
	movq	_ZZN2v88internalL32Builtin_Impl_Stats_BigIntAsUintNEiPmPNS0_7IsolateEE27trace_event_unique_atomic41(%rip), %rbx
	testq	%rbx, %rbx
	je	.L155
.L109:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L156
.L111:
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	cmpl	$5, %r15d
	jg	.L115
	leaq	88(%r12), %r8
	movq	%r8, %r14
.L118:
	movq	(%r8), %rax
	testb	$1, %al
	jne	.L119
	testq	%rax, %rax
	js	.L119
.L120:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal6BigInt10FromObjectEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	-168(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L153
	movq	(%r8), %rcx
	testb	$1, %cl
	je	.L157
	movsd	7(%rcx), %xmm0
	movsd	.LC7(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L125
.L161:
	cvttsd2siq	%xmm0, %rsi
.L126:
	movq	%r12, %rdi
	call	_ZN2v88internal6BigInt7AsUintNEPNS0_7IsolateEmNS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	je	.L153
	movq	(%rax), %r14
.L121:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L130
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L130:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L158
.L106:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L159
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	movq	%r8, %rsi
	movl	$197, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object14ConvertToIndexEPNS0_7IsolateENS0_6HandleIS1_EENS0_15MessageTemplateE@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	jne	.L120
.L153:
	movq	312(%r12), %r14
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L115:
	leaq	-8(%r14), %r8
	cmpl	$6, %r15d
	je	.L160
	subq	$16, %r14
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L157:
	sarq	$32, %rcx
	pxor	%xmm0, %xmm0
	movsd	.LC7(%rip), %xmm1
	cvtsi2sdl	%ecx, %xmm0
	comisd	%xmm1, %xmm0
	jb	.L161
.L125:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rsi
	btcq	$63, %rsi
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L156:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L162
.L112:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L113
	movq	(%rdi), %rax
	call	*8(%rax)
.L113:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L114
	movq	(%rdi), %rax
	call	*8(%rax)
.L114:
	leaq	.LC6(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L155:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L163
.L110:
	movq	%rbx, _ZZN2v88internalL32Builtin_Impl_Stats_BigIntAsUintNEiPmPNS0_7IsolateEE27trace_event_unique_atomic41(%rip)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L158:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L154:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$688, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L163:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L162:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC6(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L112
.L159:
	call	__stack_chk_fail@PLT
.L160:
	leaq	88(%r12), %r14
	jmp	.L118
	.cfi_endproc
.LFE18664:
	.size	_ZN2v88internalL32Builtin_Impl_Stats_BigIntAsUintNEiPmPNS0_7IsolateE, .-_ZN2v88internalL32Builtin_Impl_Stats_BigIntAsUintNEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL31Builtin_Impl_Stats_BigIntAsIntNEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"V8.Builtin_BigIntAsIntN"
	.section	.text._ZN2v88internalL31Builtin_Impl_Stats_BigIntAsIntNEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Builtin_Impl_Stats_BigIntAsIntNEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Builtin_Impl_Stats_BigIntAsIntNEiPmPNS0_7IsolateE:
.LFB18667:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L211
.L165:
	movq	_ZZN2v88internalL31Builtin_Impl_Stats_BigIntAsIntNEiPmPNS0_7IsolateEE27trace_event_unique_atomic59(%rip), %rbx
	testq	%rbx, %rbx
	je	.L212
.L167:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L213
.L169:
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	cmpl	$5, %r15d
	jg	.L173
	leaq	88(%r12), %r8
	movq	%r8, %r14
.L176:
	movq	(%r8), %rax
	testb	$1, %al
	jne	.L177
	testq	%rax, %rax
	js	.L177
.L178:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal6BigInt10FromObjectEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	-168(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L210
	movq	(%r8), %rcx
	testb	$1, %cl
	je	.L214
	movsd	7(%rcx), %xmm0
	movsd	.LC7(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L183
.L218:
	cvttsd2siq	%xmm0, %rsi
.L184:
	movq	%r12, %rdi
	call	_ZN2v88internal6BigInt6AsIntNEPNS0_7IsolateEmNS0_6HandleIS1_EE@PLT
	movq	(%rax), %r14
.L179:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L187
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L187:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L215
.L164:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L216
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movq	%r8, %rsi
	movl	$197, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object14ConvertToIndexEPNS0_7IsolateENS0_6HandleIS1_EENS0_15MessageTemplateE@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	jne	.L178
.L210:
	movq	312(%r12), %r14
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L173:
	leaq	-8(%r14), %r8
	cmpl	$6, %r15d
	je	.L217
	subq	$16, %r14
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L214:
	sarq	$32, %rcx
	pxor	%xmm0, %xmm0
	movsd	.LC7(%rip), %xmm1
	cvtsi2sdl	%ecx, %xmm0
	comisd	%xmm1, %xmm0
	jb	.L218
.L183:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rsi
	btcq	$63, %rsi
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L213:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L219
.L170:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L171
	movq	(%rdi), %rax
	call	*8(%rax)
.L171:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L172
	movq	(%rdi), %rax
	call	*8(%rax)
.L172:
	leaq	.LC8(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L212:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L220
.L168:
	movq	%rbx, _ZZN2v88internalL31Builtin_Impl_Stats_BigIntAsIntNEiPmPNS0_7IsolateEE27trace_event_unique_atomic59(%rip)
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L215:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L211:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$689, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L220:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L219:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC8(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L170
.L216:
	call	__stack_chk_fail@PLT
.L217:
	leaq	88(%r12), %r14
	jmp	.L176
	.cfi_endproc
.LFE18667:
	.size	_ZN2v88internalL31Builtin_Impl_Stats_BigIntAsIntNEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Builtin_Impl_Stats_BigIntAsIntNEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Builtin_Impl_Stats_BigIntPrototypeToStringEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"V8.Builtin_BigIntPrototypeToString"
	.section	.rodata._ZN2v88internalL42Builtin_Impl_Stats_BigIntPrototypeToStringEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"BigInt.prototype.toString"
	.section	.text._ZN2v88internalL42Builtin_Impl_Stats_BigIntPrototypeToStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_Stats_BigIntPrototypeToStringEiPmPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_Stats_BigIntPrototypeToStringEiPmPNS0_7IsolateE:
.LFB18675:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L254
.L222:
	movq	_ZZN2v88internalL42Builtin_Impl_Stats_BigIntPrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic150(%rip), %rbx
	testq	%rbx, %rbx
	je	.L255
.L224:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L256
.L226:
	leaq	88(%r12), %rax
	leaq	-8(%r14), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	addl	$1, 41104(%r12)
	cmpl	$5, %r13d
	movq	41088(%r12), %r15
	leaq	.LC10(%rip), %rcx
	cmovle	%rax, %rsi
	movq	41096(%r12), %rbx
	call	_ZN2v88internal12_GLOBAL__N_118BigIntToStringImplENS0_6HandleINS0_6ObjectEEES4_PNS0_7IsolateEPKc
	movq	%r15, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L234
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L234:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L257
.L221:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L258
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L259
.L225:
	movq	%rbx, _ZZN2v88internalL42Builtin_Impl_Stats_BigIntPrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic150(%rip)
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L256:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L260
.L227:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L228
	movq	(%rdi), %rax
	call	*8(%rax)
.L228:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L229
	movq	(%rdi), %rax
	call	*8(%rax)
.L229:
	leaq	.LC9(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L257:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L254:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$691, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L260:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC9(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L259:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L225
.L258:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18675:
	.size	_ZN2v88internalL42Builtin_Impl_Stats_BigIntPrototypeToStringEiPmPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_Stats_BigIntPrototypeToStringEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL48Builtin_Impl_Stats_BigIntPrototypeToLocaleStringEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"V8.Builtin_BigIntPrototypeToLocaleString"
	.align 8
.LC12:
	.string	"BigInt.prototype.toLocaleString"
	.section	.text._ZN2v88internalL48Builtin_Impl_Stats_BigIntPrototypeToLocaleStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL48Builtin_Impl_Stats_BigIntPrototypeToLocaleStringEiPmPNS0_7IsolateE, @function
_ZN2v88internalL48Builtin_Impl_Stats_BigIntPrototypeToLocaleStringEiPmPNS0_7IsolateE:
.LFB18672:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L301
.L262:
	movq	_ZZN2v88internalL48Builtin_Impl_Stats_BigIntPrototypeToLocaleStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic126(%rip), %rbx
	testq	%rbx, %rbx
	je	.L302
.L264:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L303
.L266:
	movq	41088(%r12), %r15
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	cmpb	$0, _ZN2v88internal24FLAG_harmony_intl_bigintE(%rip)
	je	.L270
	movq	%r13, %rsi
	leaq	.LC12(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_115ThisBigIntValueEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKc
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L300
	cmpl	$6, %r14d
	jg	.L273
	leaq	88(%r12), %rdx
	movq	%rdx, %rcx
	je	.L276
.L274:
	movq	%r12, %rdi
	call	_ZN2v88internal4Intl20NumberToLocaleStringEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_@PLT
	testq	%rax, %rax
	je	.L300
	movq	(%rax), %r13
.L272:
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L280
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L280:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L304
.L261:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L305
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L270:
	.cfi_restore_state
	movq	%r13, %rdi
	leaq	88(%r12), %rsi
	leaq	.LC12(%rip), %rcx
	movq	%r12, %rdx
	call	_ZN2v88internal12_GLOBAL__N_118BigIntToStringImplENS0_6HandleINS0_6ObjectEEES4_PNS0_7IsolateEPKc
	movq	%rax, %r13
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L303:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L306
.L267:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L268
	movq	(%rdi), %rax
	call	*8(%rax)
.L268:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L269
	movq	(%rdi), %rax
	call	*8(%rax)
.L269:
	leaq	.LC11(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L302:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L307
.L265:
	movq	%rbx, _ZZN2v88internalL48Builtin_Impl_Stats_BigIntPrototypeToLocaleStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic126(%rip)
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L273:
	leaq	-16(%r13), %rcx
.L276:
	leaq	-8(%r13), %rdx
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L300:
	movq	312(%r12), %r13
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L304:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L301:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$690, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L307:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L306:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC11(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L267
.L305:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18672:
	.size	_ZN2v88internalL48Builtin_Impl_Stats_BigIntPrototypeToLocaleStringEiPmPNS0_7IsolateE, .-_ZN2v88internalL48Builtin_Impl_Stats_BigIntPrototypeToLocaleStringEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL41Builtin_Impl_Stats_BigIntPrototypeValueOfEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"V8.Builtin_BigIntPrototypeValueOf"
	.section	.rodata._ZN2v88internalL41Builtin_Impl_Stats_BigIntPrototypeValueOfEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"BigInt.prototype.valueOf"
	.section	.text._ZN2v88internalL41Builtin_Impl_Stats_BigIntPrototypeValueOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Builtin_Impl_Stats_BigIntPrototypeValueOfEiPmPNS0_7IsolateE, @function
_ZN2v88internalL41Builtin_Impl_Stats_BigIntPrototypeValueOfEiPmPNS0_7IsolateE:
.LFB18678:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L341
.L309:
	movq	_ZZN2v88internalL41Builtin_Impl_Stats_BigIntPrototypeValueOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic157(%rip), %rbx
	testq	%rbx, %rbx
	je	.L342
.L311:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L343
.L313:
	addl	$1, 41104(%r12)
	leaq	.LC14(%rip), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	call	_ZN2v88internal12_GLOBAL__N_115ThisBigIntValueEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKc
	testq	%rax, %rax
	je	.L344
	movq	(%rax), %r13
.L318:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L321
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L321:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L345
.L308:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L346
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L343:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L347
.L314:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L315
	movq	(%rdi), %rax
	call	*8(%rax)
.L315:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L316
	movq	(%rdi), %rax
	call	*8(%rax)
.L316:
	leaq	.LC13(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L342:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L348
.L312:
	movq	%rbx, _ZZN2v88internalL41Builtin_Impl_Stats_BigIntPrototypeValueOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic157(%rip)
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L344:
	movq	312(%r12), %r13
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L345:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L341:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$692, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L348:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L347:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC13(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L314
.L346:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18678:
	.size	_ZN2v88internalL41Builtin_Impl_Stats_BigIntPrototypeValueOfEiPmPNS0_7IsolateE, .-_ZN2v88internalL41Builtin_Impl_Stats_BigIntPrototypeValueOfEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Builtin_BigIntConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Builtin_BigIntConstructorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Builtin_BigIntConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Builtin_BigIntConstructorEiPmPNS0_7IsolateE:
.LFB18662:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L367
	leal	-8(,%rdi,8), %eax
	movq	%rsi, %rcx
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	cltq
	movq	41096(%rdx), %r13
	subq	%rax, %rcx
	movq	%rcx, %rax
	movq	88(%rdx), %rcx
	cmpq	%rcx, (%rax)
	jne	.L368
	subq	$8, %rsi
	leaq	88(%rdx), %r8
	cmpl	$5, %edi
	cmovg	%rsi, %r8
	movq	(%r8), %rax
	testb	$1, %al
	jne	.L369
.L356:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6BigInt10FromNumberEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L366
.L362:
	movq	(%rax), %r14
.L352:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L349
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L349:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	leaq	2120(%rdx), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movl	$90, %esi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r14
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L369:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L358
	movq	%r8, %rdi
	movl	$1, %esi
	call	_ZN2v88internal10JSReceiver11ToPrimitiveENS0_6HandleIS1_EENS0_15ToPrimitiveHintE@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L366
.L358:
	movq	(%r8), %rax
	testb	$1, %al
	je	.L356
	movq	-1(%rax), %rax
	cmpw	$65, 11(%rax)
	je	.L356
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6BigInt10FromObjectEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	testq	%rax, %rax
	jne	.L362
	.p2align 4,,10
	.p2align 3
.L366:
	movq	312(%r12), %r14
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L367:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL36Builtin_Impl_Stats_BigIntConstructorEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18662:
	.size	_ZN2v88internal25Builtin_BigIntConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internal25Builtin_BigIntConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Builtin_BigIntAsUintNEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Builtin_BigIntAsUintNEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Builtin_BigIntAsUintNEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Builtin_BigIntAsUintNEiPmPNS0_7IsolateE:
.LFB18665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L392
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	cmpl	$5, %edi
	jle	.L393
	leaq	-8(%rsi), %r15
	cmpl	$6, %edi
	je	.L394
	leaq	-16(%rsi), %r14
.L375:
	movq	(%r15), %rax
	testb	$1, %al
	jne	.L376
.L395:
	testq	%rax, %rax
	js	.L376
.L377:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6BigInt10FromObjectEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L391
	movq	(%r15), %rcx
	testb	$1, %cl
	jne	.L380
	sarq	$32, %rcx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
.L381:
	movsd	.LC7(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L382
	cvttsd2siq	%xmm0, %rsi
.L383:
	movq	%r12, %rdi
	call	_ZN2v88internal6BigInt7AsUintNEPNS0_7IsolateEmNS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	je	.L391
	movq	(%rax), %r14
.L378:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L370
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L370:
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L393:
	.cfi_restore_state
	movq	88(%rdx), %rax
	leaq	88(%rdx), %r15
	movq	%r15, %r14
	testb	$1, %al
	je	.L395
.L376:
	movq	%r15, %rsi
	movl	$197, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object14ConvertToIndexEPNS0_7IsolateENS0_6HandleIS1_EENS0_15MessageTemplateE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L377
.L391:
	movq	312(%r12), %r14
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L382:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rsi
	btcq	$63, %rsi
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L380:
	movsd	7(%rcx), %xmm0
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L392:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL32Builtin_Impl_Stats_BigIntAsUintNEiPmPNS0_7IsolateE
.L394:
	.cfi_restore_state
	leaq	88(%rdx), %r14
	jmp	.L375
	.cfi_endproc
.LFE18665:
	.size	_ZN2v88internal21Builtin_BigIntAsUintNEiPmPNS0_7IsolateE, .-_ZN2v88internal21Builtin_BigIntAsUintNEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal20Builtin_BigIntAsIntNEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20Builtin_BigIntAsIntNEiPmPNS0_7IsolateE
	.type	_ZN2v88internal20Builtin_BigIntAsIntNEiPmPNS0_7IsolateE, @function
_ZN2v88internal20Builtin_BigIntAsIntNEiPmPNS0_7IsolateE:
.LFB18668:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L417
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	cmpl	$5, %edi
	jle	.L418
	leaq	-8(%rsi), %r15
	cmpl	$6, %edi
	je	.L419
	leaq	-16(%rsi), %r14
.L401:
	movq	(%r15), %rax
	testb	$1, %al
	jne	.L402
.L420:
	testq	%rax, %rax
	js	.L402
.L403:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6BigInt10FromObjectEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L416
	movq	(%r15), %rcx
	testb	$1, %cl
	jne	.L406
	sarq	$32, %rcx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
.L407:
	movsd	.LC7(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L408
	cvttsd2siq	%xmm0, %rsi
.L409:
	movq	%r12, %rdi
	call	_ZN2v88internal6BigInt6AsIntNEPNS0_7IsolateEmNS0_6HandleIS1_EE@PLT
	movq	(%rax), %r14
.L404:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L396
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L396:
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L418:
	.cfi_restore_state
	movq	88(%rdx), %rax
	leaq	88(%rdx), %r15
	movq	%r15, %r14
	testb	$1, %al
	je	.L420
.L402:
	movq	%r15, %rsi
	movl	$197, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object14ConvertToIndexEPNS0_7IsolateENS0_6HandleIS1_EENS0_15MessageTemplateE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L403
.L416:
	movq	312(%r12), %r14
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L408:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rsi
	btcq	$63, %rsi
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L406:
	movsd	7(%rcx), %xmm0
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L417:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL31Builtin_Impl_Stats_BigIntAsIntNEiPmPNS0_7IsolateE
.L419:
	.cfi_restore_state
	leaq	88(%rdx), %r14
	jmp	.L401
	.cfi_endproc
.LFE18668:
	.size	_ZN2v88internal20Builtin_BigIntAsIntNEiPmPNS0_7IsolateE, .-_ZN2v88internal20Builtin_BigIntAsIntNEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal37Builtin_BigIntPrototypeToLocaleStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal37Builtin_BigIntPrototypeToLocaleStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal37Builtin_BigIntPrototypeToLocaleStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal37Builtin_BigIntPrototypeToLocaleStringEiPmPNS0_7IsolateE:
.LFB18673:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L435
	addl	$1, 41104(%rdx)
	cmpb	$0, _ZN2v88internal24FLAG_harmony_intl_bigintE(%rip)
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	jne	.L436
	movq	%r13, %rdi
	leaq	88(%rdx), %rsi
	leaq	.LC12(%rip), %rcx
	call	_ZN2v88internal12_GLOBAL__N_118BigIntToStringImplENS0_6HandleINS0_6ObjectEEES4_PNS0_7IsolateEPKc
	movq	%rax, %r13
.L425:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L421
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L421:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L436:
	.cfi_restore_state
	leaq	.LC12(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_115ThisBigIntValueEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKc
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L434
	cmpl	$6, %r15d
	jle	.L437
	leaq	-16(%r13), %rcx
.L429:
	leaq	-8(%r13), %rdx
.L427:
	movq	%r12, %rdi
	call	_ZN2v88internal4Intl20NumberToLocaleStringEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_@PLT
	testq	%rax, %rax
	je	.L434
	movq	(%rax), %r13
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L437:
	leaq	88(%r12), %rdx
	movq	%rdx, %rcx
	jne	.L427
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L434:
	movq	312(%r12), %r13
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L435:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL48Builtin_Impl_Stats_BigIntPrototypeToLocaleStringEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18673:
	.size	_ZN2v88internal37Builtin_BigIntPrototypeToLocaleStringEiPmPNS0_7IsolateE, .-_ZN2v88internal37Builtin_BigIntPrototypeToLocaleStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Builtin_BigIntPrototypeToStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Builtin_BigIntPrototypeToStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Builtin_BigIntPrototypeToStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Builtin_BigIntPrototypeToStringEiPmPNS0_7IsolateE:
.LFB18676:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L460
	addl	$1, 41104(%rdx)
	leaq	88(%rdx), %rax
	cmpl	$5, %edi
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	leaq	-8(%rsi), %r15
	leaq	.LC10(%rip), %rdx
	movq	%r12, %rdi
	cmovle	%rax, %r15
	call	_ZN2v88internal12_GLOBAL__N_115ThisBigIntValueEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKc
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L459
	movq	(%r15), %rax
	cmpq	88(%r12), %rax
	je	.L456
	testb	$1, %al
	jne	.L461
.L446:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L449:
	movsd	.LC2(%rip), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L450
	comisd	.LC3(%rip), %xmm0
	jbe	.L457
.L450:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$218, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r14
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L456:
	movl	$10, %edx
.L444:
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6BigInt8ToStringEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_11ShouldThrowE@PLT
	testq	%rax, %rax
	je	.L459
	movq	(%rax), %r14
.L443:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L438
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L438:
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L459:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L457:
	cvttsd2sil	%xmm0, %edx
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L461:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	je	.L459
	movq	(%rax), %rax
	testb	$1, %al
	je	.L446
	movsd	7(%rax), %xmm0
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L460:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL42Builtin_Impl_Stats_BigIntPrototypeToStringEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18676:
	.size	_ZN2v88internal31Builtin_BigIntPrototypeToStringEiPmPNS0_7IsolateE, .-_ZN2v88internal31Builtin_BigIntPrototypeToStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Builtin_BigIntPrototypeValueOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Builtin_BigIntPrototypeValueOfEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Builtin_BigIntPrototypeValueOfEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Builtin_BigIntPrototypeValueOfEiPmPNS0_7IsolateE:
.LFB18679:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L469
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	%r12, %rdi
	movq	41096(%rdx), %r13
	leaq	.LC14(%rip), %rdx
	call	_ZN2v88internal12_GLOBAL__N_115ThisBigIntValueEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKc
	testq	%rax, %rax
	je	.L470
	movq	(%rax), %r14
.L465:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L462
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L462:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L470:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L469:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL41Builtin_Impl_Stats_BigIntPrototypeValueOfEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18679:
	.size	_ZN2v88internal30Builtin_BigIntPrototypeValueOfEiPmPNS0_7IsolateE, .-_ZN2v88internal30Builtin_BigIntPrototypeValueOfEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal25Builtin_BigIntConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal25Builtin_BigIntConstructorEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal25Builtin_BigIntConstructorEiPmPNS0_7IsolateE:
.LFB22486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22486:
	.size	_GLOBAL__sub_I__ZN2v88internal25Builtin_BigIntConstructorEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal25Builtin_BigIntConstructorEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal25Builtin_BigIntConstructorEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL41Builtin_Impl_Stats_BigIntPrototypeValueOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic157,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL41Builtin_Impl_Stats_BigIntPrototypeValueOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic157, @object
	.size	_ZZN2v88internalL41Builtin_Impl_Stats_BigIntPrototypeValueOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic157, 8
_ZZN2v88internalL41Builtin_Impl_Stats_BigIntPrototypeValueOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic157:
	.zero	8
	.section	.bss._ZZN2v88internalL42Builtin_Impl_Stats_BigIntPrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic150,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Builtin_Impl_Stats_BigIntPrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic150, @object
	.size	_ZZN2v88internalL42Builtin_Impl_Stats_BigIntPrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic150, 8
_ZZN2v88internalL42Builtin_Impl_Stats_BigIntPrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic150:
	.zero	8
	.section	.bss._ZZN2v88internalL48Builtin_Impl_Stats_BigIntPrototypeToLocaleStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic126,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL48Builtin_Impl_Stats_BigIntPrototypeToLocaleStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic126, @object
	.size	_ZZN2v88internalL48Builtin_Impl_Stats_BigIntPrototypeToLocaleStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic126, 8
_ZZN2v88internalL48Builtin_Impl_Stats_BigIntPrototypeToLocaleStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic126:
	.zero	8
	.section	.bss._ZZN2v88internalL31Builtin_Impl_Stats_BigIntAsIntNEiPmPNS0_7IsolateEE27trace_event_unique_atomic59,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Builtin_Impl_Stats_BigIntAsIntNEiPmPNS0_7IsolateEE27trace_event_unique_atomic59, @object
	.size	_ZZN2v88internalL31Builtin_Impl_Stats_BigIntAsIntNEiPmPNS0_7IsolateEE27trace_event_unique_atomic59, 8
_ZZN2v88internalL31Builtin_Impl_Stats_BigIntAsIntNEiPmPNS0_7IsolateEE27trace_event_unique_atomic59:
	.zero	8
	.section	.bss._ZZN2v88internalL32Builtin_Impl_Stats_BigIntAsUintNEiPmPNS0_7IsolateEE27trace_event_unique_atomic41,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Builtin_Impl_Stats_BigIntAsUintNEiPmPNS0_7IsolateEE27trace_event_unique_atomic41, @object
	.size	_ZZN2v88internalL32Builtin_Impl_Stats_BigIntAsUintNEiPmPNS0_7IsolateEE27trace_event_unique_atomic41, 8
_ZZN2v88internalL32Builtin_Impl_Stats_BigIntAsUintNEiPmPNS0_7IsolateEE27trace_event_unique_atomic41:
	.zero	8
	.section	.bss._ZZN2v88internalL36Builtin_Impl_Stats_BigIntConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic17,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Builtin_Impl_Stats_BigIntConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic17, @object
	.size	_ZZN2v88internalL36Builtin_Impl_Stats_BigIntConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic17, 8
_ZZN2v88internalL36Builtin_Impl_Stats_BigIntConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic17:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	0
	.long	1073741824
	.align 8
.LC3:
	.long	0
	.long	1078067200
	.align 8
.LC7:
	.long	0
	.long	1138753536
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
