	.file	"v8-stack-trace-impl.cc"
	.text
	.section	.text._ZN12v8_inspector17V8InspectorClient17resourceNameToUrlERKNS_10StringViewE,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient17resourceNameToUrlERKNS_10StringViewE,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient17resourceNameToUrlERKNS_10StringViewE
	.type	_ZN12v8_inspector17V8InspectorClient17resourceNameToUrlERKNS_10StringViewE, @function
_ZN12v8_inspector17V8InspectorClient17resourceNameToUrlERKNS_10StringViewE:
.LFB2822:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE2822:
	.size	_ZN12v8_inspector17V8InspectorClient17resourceNameToUrlERKNS_10StringViewE, .-_ZN12v8_inspector17V8InspectorClient17resourceNameToUrlERKNS_10StringViewE
	.section	.text._ZNK12v8_inspector16V8StackTraceImpl7isEmptyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector16V8StackTraceImpl7isEmptyEv
	.type	_ZNK12v8_inspector16V8StackTraceImpl7isEmptyEv, @function
_ZNK12v8_inspector16V8StackTraceImpl7isEmptyEv:
.LFB7584:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	cmpq	%rax, 16(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE7584:
	.size	_ZNK12v8_inspector16V8StackTraceImpl7isEmptyEv, .-_ZNK12v8_inspector16V8StackTraceImpl7isEmptyEv
	.section	.text._ZNK12v8_inspector16V8StackTraceImpl13topLineNumberEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector16V8StackTraceImpl13topLineNumberEv
	.type	_ZNK12v8_inspector16V8StackTraceImpl13topLineNumberEv, @function
_ZNK12v8_inspector16V8StackTraceImpl13topLineNumberEv:
.LFB7586:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movl	120(%rax), %eax
	addl	$1, %eax
	ret
	.cfi_endproc
.LFE7586:
	.size	_ZNK12v8_inspector16V8StackTraceImpl13topLineNumberEv, .-_ZNK12v8_inspector16V8StackTraceImpl13topLineNumberEv
	.section	.text._ZNK12v8_inspector16V8StackTraceImpl15topColumnNumberEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector16V8StackTraceImpl15topColumnNumberEv
	.type	_ZNK12v8_inspector16V8StackTraceImpl15topColumnNumberEv, @function
_ZNK12v8_inspector16V8StackTraceImpl15topColumnNumberEv:
.LFB7587:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movl	124(%rax), %eax
	addl	$1, %eax
	ret
	.cfi_endproc
.LFE7587:
	.size	_ZNK12v8_inspector16V8StackTraceImpl15topColumnNumberEv, .-_ZNK12v8_inspector16V8StackTraceImpl15topColumnNumberEv
	.section	.text._ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB12975:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12975:
	.size	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB12992:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE12992:
	.size	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNK12v8_inspector16V8StackTraceImpl12topSourceURLEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector16V8StackTraceImpl12topSourceURLEv
	.type	_ZNK12v8_inspector16V8StackTraceImpl12topSourceURLEv, @function
_ZNK12v8_inspector16V8StackTraceImpl12topSourceURLEv:
.LFB7585:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	movq	(%rax), %rsi
	addq	$80, %rsi
	call	_ZN12v8_inspector12toStringViewERKNS_8String16E@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L11
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L11:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7585:
	.size	_ZNK12v8_inspector16V8StackTraceImpl12topSourceURLEv, .-_ZNK12v8_inspector16V8StackTraceImpl12topSourceURLEv
	.section	.text._ZNK12v8_inspector16V8StackTraceImpl11topScriptIdEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector16V8StackTraceImpl11topScriptIdEv
	.type	_ZNK12v8_inspector16V8StackTraceImpl11topScriptIdEv, @function
_ZNK12v8_inspector16V8StackTraceImpl11topScriptIdEv:
.LFB7588:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	movq	(%rax), %rsi
	addq	$40, %rsi
	call	_ZN12v8_inspector12toStringViewERKNS_8String16E@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L15
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L15:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7588:
	.size	_ZNK12v8_inspector16V8StackTraceImpl11topScriptIdEv, .-_ZNK12v8_inspector16V8StackTraceImpl11topScriptIdEv
	.section	.text._ZNK12v8_inspector16V8StackTraceImpl15topFunctionNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector16V8StackTraceImpl15topFunctionNameEv
	.type	_ZNK12v8_inspector16V8StackTraceImpl15topFunctionNameEv, @function
_ZNK12v8_inspector16V8StackTraceImpl15topFunctionNameEv:
.LFB7589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	movq	(%rax), %rsi
	call	_ZN12v8_inspector12toStringViewERKNS_8String16E@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L19
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L19:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7589:
	.size	_ZNK12v8_inspector16V8StackTraceImpl15topFunctionNameEv, .-_ZNK12v8_inspector16V8StackTraceImpl15topFunctionNameEv
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev, @function
_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev:
.LFB5119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	leaq	104(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L21
	call	_ZdlPv@PLT
.L21:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L22
	call	_ZdlPv@PLT
.L22:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L20
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5119:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev, .-_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev,_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB12977:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12977:
	.size	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB12991:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12991:
	.size	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv:
.LFB5133:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime9CallFrame7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L27
	movq	(%rdi), %rax
	call	*24(%rax)
.L27:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L34
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L34:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5133:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv:
.LFB5132:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime9CallFrame7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L35
	movq	(%rdi), %rax
	call	*24(%rax)
.L35:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L42
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L42:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5132:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev, @function
_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev:
.LFB5121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L44
	call	_ZdlPv@PLT
.L44:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L45
	call	_ZdlPv@PLT
.L45:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L46
	call	_ZdlPv@PLT
.L46:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$136, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5121:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev, .-_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12StackTraceIdD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev, @function
_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev:
.LFB5239:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	80(%r12), %rax
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movq	64(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L49
	call	_ZdlPv@PLT
.L49:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L50
	call	_ZdlPv@PLT
.L50:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5239:
	.size	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev, .-_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_114toFramesVectorEPNS_10V8DebuggerEN2v85LocalINS3_10StackTraceEEEi.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN12v8_inspector12_GLOBAL__N_114toFramesVectorEPNS_10V8DebuggerEN2v85LocalINS3_10StackTraceEEEi,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_114toFramesVectorEPNS_10V8DebuggerEN2v85LocalINS3_10StackTraceEEEi, @function
_ZN12v8_inspector12_GLOBAL__N_114toFramesVectorEPNS_10V8DebuggerEN2v85LocalINS3_10StackTraceEEEi:
.LFB7464:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v810StackTrace13GetFrameCountEv@PLT
	movabsq	$576460752303423487, %rdx
	cmpl	%r15d, %eax
	cmovle	%eax, %r15d
	movslq	%r15d, %rax
	cmpq	%rdx, %rax
	ja	.L80
	movq	$0, 16(%r12)
	pxor	%xmm0, %xmm0
	movq	%rax, %r14
	movups	%xmm0, (%r12)
	salq	$4, %r14
	testq	%rax, %rax
	je	.L52
	movq	%r14, %rdi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	(%rax,%r14), %rdx
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	.p2align 4,,10
	.p2align 3
.L55:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L55
	movq	%rdx, 8(%r12)
	testl	%r15d, %r15d
	jle	.L52
	leal	-1(%r15), %eax
	leaq	-80(%rbp), %r14
	xorl	%r15d, %r15d
	movq	%rax, -88(%rbp)
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L83:
	movl	$-1, %edx
	lock xaddl	%edx, 8(%rdi)
	cmpl	$1, %edx
	je	.L81
.L66:
	leaq	1(%r15), %rax
	cmpq	%r15, -88(%rbp)
	je	.L52
.L84:
	movq	%rax, %r15
.L73:
	movq	16(%rbx), %rsi
	movl	%r15d, %edx
	movq	%r13, %rdi
	call	_ZNK2v810StackTrace8GetFrameEPNS_7IsolateEj@PLT
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	call	_ZN12v8_inspector10V8Debugger9symbolizeEN2v85LocalINS1_10StackFrameEEE@PLT
	movq	%r15, %rax
	pxor	%xmm1, %xmm1
	movdqa	-80(%rbp), %xmm0
	salq	$4, %rax
	addq	(%r12), %rax
	movaps	%xmm1, -80(%rbp)
	movq	8(%rax), %rdi
	movups	%xmm0, (%rax)
	testq	%rdi, %rdi
	je	.L59
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L60
	movl	$-1, %edx
	lock xaddl	%edx, 8(%rdi)
	cmpl	$1, %edx
	je	.L82
	.p2align 4,,10
	.p2align 3
.L59:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L66
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	jne	.L83
	movl	8(%rdi), %edx
	leal	-1(%rdx), %esi
	movl	%esi, 8(%rdi)
	cmpl	$1, %edx
	jne	.L66
.L81:
	movq	(%rdi), %rdx
	movq	%rax, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	*16(%rdx)
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdi
	testq	%rax, %rax
	je	.L70
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L71:
	cmpl	$1, %eax
	jne	.L66
	movq	(%rdi), %rax
	call	*24(%rax)
	leaq	1(%r15), %rax
	cmpq	%r15, -88(%rbp)
	jne	.L84
	.p2align 4,,10
	.p2align 3
.L52:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movl	8(%rdi), %edx
	leal	-1(%rdx), %esi
	movl	%esi, 8(%rdi)
	cmpl	$1, %edx
	jne	.L59
.L82:
	movq	(%rdi), %rdx
	movq	%rax, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	*16(%rdx)
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdi
	testq	%rax, %rax
	je	.L63
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L64:
	cmpl	$1, %eax
	jne	.L59
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L63:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L70:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L71
.L80:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L85:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7464:
	.size	_ZN12v8_inspector12_GLOBAL__N_114toFramesVectorEPNS_10V8DebuggerEN2v85LocalINS3_10StackTraceEEEi, .-_ZN12v8_inspector12_GLOBAL__N_114toFramesVectorEPNS_10V8DebuggerEN2v85LocalINS3_10StackTraceEEEi
	.section	.rodata._ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator4nextEv.part.0.str1.1,"aMS",@progbits,1
.LC1:
	.string	"async function"
	.section	.text._ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator4nextEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator4nextEv.part.0, @function
_ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator4nextEv.part.0:
.LFB13066:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	cmpq	%rax, (%rdi)
	jne	.L86
	movq	%rdi, %rbx
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L87:
	movq	16(%rbx), %r15
	testq	%r15, %r15
	je	.L86
	movq	64(%r15), %rax
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, (%rbx)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	16(%rbx), %rcx
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	32(%rcx), %rax
	movq	%rsi, %r9
	movq	24(%rcx), %r10
	cmpq	%rsi, %rax
	cmovbe	%rax, %r9
	testq	%r9, %r9
	je	.L89
	xorl	%edx, %edx
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L136:
	ja	.L121
	addq	$1, %rdx
	cmpq	%r9, %rdx
	je	.L89
.L91:
	movzwl	(%rdi,%rdx,2), %r11d
	cmpw	%r11w, (%r10,%rdx,2)
	jnb	.L136
	movl	$-1, %r14d
.L90:
	cmpq	%r13, %rdi
	je	.L94
.L114:
	call	_ZdlPv@PLT
	movq	16(%rbx), %rcx
.L93:
	testl	%r14d, %r14d
	jne	.L94
	addq	$16, (%rbx)
.L94:
	movq	72(%r15), %rax
	movq	%rax, 8(%rbx)
	movq	96(%rcx), %rdi
	movq	88(%rcx), %rsi
	testq	%rdi, %rdi
	je	.L95
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	leaq	12(%rdi), %r8
	testq	%r15, %r15
	je	.L96
	lock addl	$1, (%r8)
.L97:
	movl	8(%rdi), %eax
	leaq	8(%rdi), %rcx
.L100:
	testl	%eax, %eax
	je	.L98
	leal	1(%rax), %r9d
	lock cmpxchgl	%r9d, (%rcx)
	jne	.L100
	movl	8(%rdi), %eax
	testl	%eax, %eax
	movl	$0, %eax
	cmove	%rax, %rsi
	movq	%rsi, 16(%rbx)
	testq	%r15, %r15
	je	.L137
	movl	$-1, %eax
	lock xaddl	%eax, (%rcx)
	cmpl	$1, %eax
	je	.L138
.L118:
	testq	%r15, %r15
	je	.L108
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L109:
	cmpl	$1, %eax
	jne	.L107
	movq	(%rdi), %rax
	call	*24(%rax)
.L107:
	movq	8(%rbx), %rax
	cmpq	%rax, (%rbx)
	je	.L87
.L86:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L139
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	subq	%rsi, %rax
	movl	$2147483648, %edx
	cmpq	%rdx, %rax
	jge	.L115
	movabsq	$-2147483649, %rsi
	cmpq	%rsi, %rax
	jle	.L115
	movl	%eax, %r14d
	cmpq	%r13, %rdi
	jne	.L114
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L96:
	addl	$1, 12(%rdi)
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L121:
	movl	$1, %r14d
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L95:
	movq	$0, 16(%rbx)
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L108:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L137:
	movl	8(%rdi), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%rdi)
	cmpl	$1, %eax
	jne	.L118
.L138:
	movq	(%rdi), %rax
	movq	%rdi, -104(%rbp)
	movq	%r8, -112(%rbp)
	call	*16(%rax)
	testq	%r15, %r15
	movq	-104(%rbp), %rdi
	je	.L104
	movq	-112(%rbp), %r8
	movl	$-1, %eax
	lock xaddl	%eax, (%r8)
.L105:
	cmpl	$1, %eax
	jne	.L118
	movq	(%rdi), %rax
	movq	%rdi, -104(%rbp)
	call	*24(%rax)
	movq	-104(%rbp), %rdi
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L115:
	cmpq	%r13, %rdi
	je	.L94
	call	_ZdlPv@PLT
	movq	16(%rbx), %rcx
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L104:
	movl	12(%rdi), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 12(%rdi)
	jmp	.L105
.L98:
	movq	$0, 16(%rbx)
	jmp	.L118
.L139:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13066:
	.size	_ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator4nextEv.part.0, .-_ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator4nextEv.part.0
	.section	.text._ZN12v8_inspector16V8StackTraceImplD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16V8StackTraceImplD0Ev
	.type	_ZN12v8_inspector16V8StackTraceImplD0Ev, @function
_ZN12v8_inspector16V8StackTraceImplD0Ev:
.LFB7579:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector16V8StackTraceImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L142
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L143
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
	cmpl	$1, %eax
	je	.L165
	.p2align 4,,10
	.p2align 3
.L142:
	movq	16(%r14), %rbx
	movq	8(%r14), %r12
	cmpq	%r12, %rbx
	je	.L146
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L153
	movq	8(%r12), %r13
	testq	%r13, %r13
	jne	.L166
	.p2align 4,,10
	.p2align 3
.L155:
	addq	$16, %r12
	cmpq	%r12, %rbx
	je	.L152
.L147:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L155
.L166:
	lock subl	$1, 8(%r13)
	jne	.L155
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L155
	movq	0(%r13), %rax
	addq	$16, %r12
	movq	%r13, %rdi
	call	*24(%rax)
	cmpq	%r12, %rbx
	jne	.L147
	.p2align 4,,10
	.p2align 3
.L152:
	movq	8(%r14), %r12
.L146:
	testq	%r12, %r12
	je	.L157
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L157:
	popq	%rbx
	movq	%r14, %rdi
	popq	%r12
	movl	$80, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L150
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L150:
	addq	$16, %r12
	cmpq	%r12, %rbx
	je	.L152
.L153:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L150
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L150
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L143:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L142
.L165:
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L142
	.cfi_endproc
.LFE7579:
	.size	_ZN12v8_inspector16V8StackTraceImplD0Ev, .-_ZN12v8_inspector16V8StackTraceImplD0Ev
	.section	.text._ZN12v8_inspector16V8StackTraceImplD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16V8StackTraceImplD2Ev
	.type	_ZN12v8_inspector16V8StackTraceImplD2Ev, @function
_ZN12v8_inspector16V8StackTraceImplD2Ev:
.LFB7577:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector16V8StackTraceImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L169
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L170
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
	cmpl	$1, %eax
	je	.L189
	.p2align 4,,10
	.p2align 3
.L169:
	movq	16(%rbx), %r14
	movq	8(%rbx), %r12
	cmpq	%r12, %r14
	je	.L173
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L180
	movq	8(%r12), %r13
	testq	%r13, %r13
	jne	.L190
	.p2align 4,,10
	.p2align 3
.L182:
	addq	$16, %r12
	cmpq	%r12, %r14
	je	.L179
.L174:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L182
.L190:
	lock subl	$1, 8(%r13)
	jne	.L182
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L182
	movq	0(%r13), %rax
	addq	$16, %r12
	movq	%r13, %rdi
	call	*24(%rax)
	cmpq	%r12, %r14
	jne	.L174
	.p2align 4,,10
	.p2align 3
.L179:
	movq	8(%rbx), %r12
.L173:
	testq	%r12, %r12
	je	.L167
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L177
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L177:
	addq	$16, %r12
	cmpq	%r12, %r14
	je	.L179
.L180:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L177
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L177
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L170:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L169
.L189:
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L167:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7577:
	.size	_ZN12v8_inspector16V8StackTraceImplD2Ev, .-_ZN12v8_inspector16V8StackTraceImplD2Ev
	.globl	_ZN12v8_inspector16V8StackTraceImplD1Ev
	.set	_ZN12v8_inspector16V8StackTraceImplD1Ev,_ZN12v8_inspector16V8StackTraceImplD2Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB12990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rdi), %r14
	testq	%r14, %r14
	je	.L191
	movq	96(%r14), %rdi
	testq	%rdi, %rdi
	je	.L194
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L195
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L196:
	cmpl	$1, %eax
	jne	.L194
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L194:
	movq	72(%r14), %rbx
	movq	64(%r14), %r12
	cmpq	%r12, %rbx
	je	.L198
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	jne	.L199
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L207:
	addq	$16, %r12
	cmpq	%r12, %rbx
	je	.L204
.L199:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L207
	lock subl	$1, 8(%r13)
	jne	.L207
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L207
	movq	0(%r13), %rax
	addq	$16, %r12
	movq	%r13, %rdi
	call	*24(%rax)
	cmpq	%r12, %rbx
	jne	.L199
	.p2align 4,,10
	.p2align 3
.L204:
	movq	64(%r14), %r12
.L198:
	testq	%r12, %r12
	je	.L209
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L209:
	movq	24(%r14), %rdi
	leaq	40(%r14), %rax
	cmpq	%rax, %rdi
	je	.L210
	call	_ZdlPv@PLT
.L210:
	popq	%rbx
	movq	%r14, %rdi
	popq	%r12
	movl	$128, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L202
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L202:
	addq	$16, %r12
	cmpq	%r12, %rbx
	je	.L204
.L205:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L202
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L202
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L191:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L196
	.cfi_endproc
.LFE12990:
	.size	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZN12v8_inspector16V8StackTraceImpl5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16V8StackTraceImpl5cloneEv
	.type	_ZN12v8_inspector16V8StackTraceImpl5cloneEv, @function
_ZN12v8_inspector16V8StackTraceImpl5cloneEv:
.LFB7582:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rsi), %rax
	movq	8(%rsi), %rdx
	movq	%rax, %r12
	subq	%rdx, %r12
	movq	%r12, %rcx
	sarq	$4, %rcx
	je	.L239
	movabsq	$576460752303423487, %rax
	cmpq	%rax, %rcx
	ja	.L240
	movq	%r12, %rdi
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	movq	%rax, %r14
	movq	16(%rbx), %rax
	addq	%r14, %r12
	cmpq	%rdx, %rax
	je	.L228
.L241:
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	%rdx, %rcx
	movq	%r14, %rdi
	je	.L224
	.p2align 4,,10
	.p2align 3
.L227:
	movq	(%rcx), %rsi
	movq	%rsi, (%rdi)
	movq	8(%rcx), %rsi
	movq	%rsi, 8(%rdi)
	testq	%rsi, %rsi
	je	.L226
	lock addl	$1, 8(%rsi)
.L226:
	addq	$16, %rcx
	addq	$16, %rdi
	cmpq	%rcx, %rax
	jne	.L227
.L238:
	subq	%rdx, %rax
	leaq	(%r14,%rax), %rbx
.L221:
	movl	$80, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN12v8_inspector16V8StackTraceImplE(%rip), %rdx
	movq	%rdx, (%rax)
	movq	%r14, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%r12, 24(%rax)
	movl	$0, 32(%rax)
	movq	$0, 56(%rax)
	movups	%xmm0, 40(%rax)
	movups	%xmm0, 64(%rax)
	popq	%rbx
	popq	%r12
	movq	%rax, 0(%r13)
	movq	%r13, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	.cfi_restore_state
	movq	(%rcx), %rsi
	movq	%rsi, (%rdi)
	movq	8(%rcx), %rsi
	movq	%rsi, 8(%rdi)
	testq	%rsi, %rsi
	je	.L223
	addl	$1, 8(%rsi)
.L223:
	addq	$16, %rcx
	addq	$16, %rdi
	cmpq	%rcx, %rax
	jne	.L224
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L239:
	xorl	%r14d, %r14d
	addq	%r14, %r12
	cmpq	%rdx, %rax
	jne	.L241
.L228:
	movq	%r14, %rbx
	jmp	.L221
.L240:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE7582:
	.size	_ZN12v8_inspector16V8StackTraceImpl5cloneEv, .-_ZN12v8_inspector16V8StackTraceImpl5cloneEv
	.section	.text._ZN12v8_inspector12_GLOBAL__N_119calculateAsyncChainEPNS_10V8DebuggerEiPSt10shared_ptrINS_15AsyncStackTraceEEPNS_14V8StackTraceIdEPi.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_119calculateAsyncChainEPNS_10V8DebuggerEiPSt10shared_ptrINS_15AsyncStackTraceEEPNS_14V8StackTraceIdEPi.constprop.0, @function
_ZN12v8_inspector12_GLOBAL__N_119calculateAsyncChainEPNS_10V8DebuggerEiPSt10shared_ptrINS_15AsyncStackTraceEEPNS_14V8StackTraceIdEPi.constprop.0:
.LFB13111:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$72, %rsp
	movl	%esi, -88(%rbp)
	movq	%rdi, %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector10V8Debugger18currentAsyncParentEv@PLT
	movdqa	-80(%rbp), %xmm0
	movq	8(%rbx), %rdi
	pxor	%xmm1, %xmm1
	movaps	%xmm1, -80(%rbp)
	movaps	%xmm0, (%rbx)
	testq	%rdi, %rdi
	je	.L244
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L245
	movl	$-1, %edx
	lock xaddl	%edx, 8(%rdi)
	cmpl	$1, %edx
	je	.L308
.L248:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L244
	testq	%rax, %rax
	je	.L253
	movl	$-1, %edx
	lock xaddl	%edx, 8(%rdi)
	cmpl	$1, %edx
	je	.L309
	.p2align 4,,10
	.p2align 3
.L244:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector10V8Debugger21currentExternalParentEv@PLT
	movq	-80(%rbp), %rax
	movdqu	-72(%rbp), %xmm2
	movq	%rax, 0(%r13)
	movups	%xmm2, 8(%r13)
	testq	%r14, %r14
	je	.L257
	movl	236(%r12), %eax
	movl	%eax, (%r14)
.L257:
	movl	-88(%rbp), %ecx
	movq	(%rbx), %rax
	testl	%ecx, %ecx
	je	.L258
	testq	%rax, %rax
	je	.L242
	cmpq	$0, 104(%rax)
	je	.L310
.L261:
	movq	64(%rax), %rsi
	cmpq	%rsi, 72(%rax)
	je	.L311
.L242:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L312
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L242
	movq	64(%rax), %rsi
	cmpq	%rsi, 72(%rax)
	jne	.L242
.L311:
	movq	96(%rax), %r12
	movq	88(%rax), %rdx
	testq	%r12, %r12
	je	.L273
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L271
	lock addl	$1, 12(%r12)
.L272:
	movl	8(%r12), %eax
	leaq	8(%r12), %rcx
.L275:
	testl	%eax, %eax
	je	.L273
	leal	1(%rax), %esi
	lock cmpxchgl	%esi, (%rcx)
	jne	.L275
	movl	8(%r12), %eax
	movq	%r12, -88(%rbp)
	testl	%eax, %eax
	movl	$0, %eax
	cmove	%rax, %rdx
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L245:
	movl	8(%rdi), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%rdi)
	cmpl	$1, %edx
	jne	.L248
.L308:
	movq	(%rdi), %rdx
	movq	%rax, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	*16(%rdx)
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdi
	testq	%rax, %rax
	je	.L249
	movl	$-1, %edx
	lock xaddl	%edx, 12(%rdi)
.L250:
	cmpl	$1, %edx
	jne	.L248
	movq	(%rdi), %rdx
	movq	%rax, -96(%rbp)
	call	*24(%rdx)
	movq	-96(%rbp), %rax
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L253:
	movl	8(%rdi), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%rdi)
	cmpl	$1, %edx
	jne	.L244
.L309:
	movq	(%rdi), %rdx
	movq	%rax, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	*16(%rdx)
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdi
	testq	%rax, %rax
	je	.L255
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L256:
	cmpl	$1, %eax
	jne	.L244
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L310:
	cmpl	(%rax), %ecx
	je	.L261
	movq	8(%rbx), %r12
	pxor	%xmm0, %xmm0
	movaps	%xmm0, (%rbx)
	testq	%r12, %r12
	je	.L263
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L265
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r12)
.L266:
	cmpl	$1, %edx
	jne	.L263
	movq	(%r12), %rdx
	movq	%rax, -88(%rbp)
	movq	%r12, %rdi
	call	*16(%rdx)
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L268
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L269:
	cmpl	$1, %eax
	jne	.L263
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L263:
	pxor	%xmm0, %xmm0
	movq	$0, 0(%r13)
	movups	%xmm0, 8(%r13)
	testq	%r14, %r14
	je	.L242
	movl	$0, (%r14)
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L255:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L249:
	movl	12(%rdi), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%rdi)
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L273:
	movq	$0, -88(%rbp)
	xorl	%edx, %edx
.L289:
	movq	8(%rbx), %r13
	movq	%rdx, %xmm0
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, (%rbx)
	testq	%r13, %r13
	je	.L277
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L278
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r13)
.L279:
	cmpl	$1, %edx
	jne	.L277
	movq	0(%r13), %rdx
	movq	%rax, -88(%rbp)
	movq	%r13, %rdi
	call	*16(%rdx)
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L281
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L282:
	cmpl	$1, %eax
	jne	.L277
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L277:
	testq	%r12, %r12
	je	.L242
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L285
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L286:
	cmpl	$1, %eax
	jne	.L242
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L271:
	addl	$1, 12(%r12)
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L278:
	movl	8(%r13), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r13)
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L285:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L265:
	movl	8(%r12), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r12)
	jmp	.L266
.L281:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L282
.L268:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L269
.L312:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13111:
	.size	_ZN12v8_inspector12_GLOBAL__N_119calculateAsyncChainEPNS_10V8DebuggerEiPSt10shared_ptrINS_15AsyncStackTraceEEPNS_14V8StackTraceIdEPi.constprop.0, .-_ZN12v8_inspector12_GLOBAL__N_119calculateAsyncChainEPNS_10V8DebuggerEiPSt10shared_ptrINS_15AsyncStackTraceEEPNS_14V8StackTraceIdEPi.constprop.0
	.section	.text._ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime10StackTraceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev, @function
_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev:
.LFB5165:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime10StackTraceE(%rip), %rax
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	80(%rdi), %r12
	movups	%xmm0, (%rdi)
	testq	%r12, %r12
	je	.L314
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L315
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r12), %rdi
	leaq	-64(%rax), %rsi
	movq	%rax, %xmm2
	leaq	80(%r12), %rax
	movq	%rsi, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r12)
	cmpq	%rax, %rdi
	je	.L316
	call	_ZdlPv@PLT
.L316:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L317
	call	_ZdlPv@PLT
.L317:
	movl	$104, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L314:
	movq	72(%rbx), %r12
	testq	%r12, %r12
	je	.L318
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L319
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L318:
	movq	64(%rbx), %r13
	testq	%r13, %r13
	je	.L320
	movq	8(%r13), %r14
	movq	0(%r13), %r12
	cmpq	%r12, %r14
	jne	.L327
	testq	%r12, %r12
	je	.L328
	.p2align 4,,10
	.p2align 3
.L349:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L328:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L320:
	movq	24(%rbx), %rdi
	addq	$40, %rbx
	cmpq	%rbx, %rdi
	je	.L313
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L348:
	.cfi_restore_state
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r15), %rdi
	movq	%rax, (%r15)
	leaq	104(%r15), %rax
	cmpq	%rax, %rdi
	je	.L324
	call	_ZdlPv@PLT
.L324:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L325
	call	_ZdlPv@PLT
.L325:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L326
	call	_ZdlPv@PLT
.L326:
	movl	$136, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L322:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L347
.L327:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L322
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L348
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rax
	cmpq	%r12, %r14
	jne	.L327
	.p2align 4,,10
	.p2align 3
.L347:
	movq	0(%r13), %r12
	testq	%r12, %r12
	jne	.L349
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L313:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	.cfi_restore_state
	call	*%rax
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L315:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L314
	.cfi_endproc
.LFE5165:
	.size	_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev, .-_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev,_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime10StackTraceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev, @function
_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev:
.LFB5167:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime10StackTraceE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm2
	movq	%rdx, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm2, %xmm1
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	80(%rdi), %r13
	movaps	%xmm1, -64(%rbp)
	movups	%xmm1, (%rdi)
	testq	%r13, %r13
	je	.L351
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L352
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r13), %rdi
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm3
	leaq	80(%r13), %rax
	movq	%rdx, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 0(%r13)
	cmpq	%rax, %rdi
	je	.L353
	call	_ZdlPv@PLT
.L353:
	movq	16(%r13), %rdi
	leaq	32(%r13), %rax
	cmpq	%rax, %rdi
	je	.L354
	call	_ZdlPv@PLT
.L354:
	movl	$104, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L351:
	movq	72(%r12), %r13
	testq	%r13, %r13
	je	.L355
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L356
	movdqa	-64(%rbp), %xmm4
	movq	80(%r13), %r14
	movups	%xmm4, 0(%r13)
	testq	%r14, %r14
	je	.L357
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L358
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r14), %rdi
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm5
	leaq	80(%r14), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, (%r14)
	cmpq	%rax, %rdi
	je	.L359
	call	_ZdlPv@PLT
.L359:
	movq	16(%r14), %rdi
	leaq	32(%r14), %rax
	cmpq	%rax, %rdi
	je	.L360
	call	_ZdlPv@PLT
.L360:
	movl	$104, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L357:
	movq	72(%r13), %r14
	testq	%r14, %r14
	je	.L361
	movq	(%r14), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L362
	movdqa	-64(%rbp), %xmm6
	movq	80(%r14), %r15
	movups	%xmm6, (%r14)
	testq	%r15, %r15
	je	.L363
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L364
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r15), %rdi
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm7
	leaq	80(%r15), %rax
	movq	%rdx, %xmm0
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, (%r15)
	cmpq	%rax, %rdi
	je	.L365
	call	_ZdlPv@PLT
.L365:
	movq	16(%r15), %rdi
	leaq	32(%r15), %rax
	cmpq	%rax, %rdi
	je	.L366
	call	_ZdlPv@PLT
.L366:
	movl	$104, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L363:
	movq	72(%r14), %r15
	testq	%r15, %r15
	je	.L367
	movq	(%r15), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L368
	movdqa	-64(%rbp), %xmm7
	movq	80(%r15), %r8
	movups	%xmm7, (%r15)
	testq	%r8, %r8
	je	.L369
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L370
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r8), %rdi
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm5
	leaq	80(%r8), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, (%r8)
	cmpq	%rax, %rdi
	je	.L371
	movq	%r8, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %r8
.L371:
	movq	16(%r8), %rdi
	leaq	32(%r8), %rax
	cmpq	%rax, %rdi
	je	.L372
	movq	%r8, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %r8
.L372:
	movl	$104, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L369:
	movq	72(%r15), %rdx
	movq	%rdx, -72(%rbp)
	testq	%rdx, %rdx
	je	.L373
	movq	(%rdx), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L374
	movdqa	-64(%rbp), %xmm6
	movq	80(%rdx), %r8
	movups	%xmm6, (%rdx)
	testq	%r8, %r8
	je	.L375
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L376
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r8), %rdi
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm7
	leaq	80(%r8), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, (%r8)
	cmpq	%rax, %rdi
	je	.L377
	movq	%r8, -80(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %r8
.L377:
	movq	16(%r8), %rdi
	leaq	32(%r8), %rax
	cmpq	%rax, %rdi
	je	.L378
	movq	%r8, -80(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %r8
.L378:
	movl	$104, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L375:
	movq	-72(%rbp), %rax
	movq	72(%rax), %rsi
	movq	%rsi, -80(%rbp)
	testq	%rsi, %rsi
	je	.L379
	movq	(%rsi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L380
	movdqa	-64(%rbp), %xmm7
	movq	80(%rsi), %r8
	movups	%xmm7, (%rsi)
	testq	%r8, %r8
	je	.L381
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L382
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r8), %rdi
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm5
	leaq	80(%r8), %rax
	movq	%rdx, %xmm0
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, (%r8)
	cmpq	%rax, %rdi
	je	.L383
	movq	%r8, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %r8
.L383:
	movq	16(%r8), %rdi
	leaq	32(%r8), %rax
	cmpq	%rax, %rdi
	je	.L384
	movq	%r8, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %r8
.L384:
	movl	$104, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L381:
	movq	-80(%rbp), %rax
	movq	72(%rax), %rdi
	testq	%rdi, %rdi
	je	.L385
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L386
	movq	%rdi, -64(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movq	-64(%rbp), %rdi
	movl	$88, %esi
	call	_ZdlPvm@PLT
.L385:
	movq	-80(%rbp), %rax
	movq	64(%rax), %rax
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L387
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	%rcx, -64(%rbp)
	cmpq	%rbx, %rcx
	jne	.L394
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L545:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r8), %rdi
	movq	%rax, (%r8)
	leaq	104(%r8), %rax
	cmpq	%rax, %rdi
	je	.L391
	movq	%r8, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %r8
.L391:
	movq	48(%r8), %rdi
	leaq	64(%r8), %rax
	cmpq	%rax, %rdi
	je	.L392
	movq	%r8, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %r8
.L392:
	movq	8(%r8), %rdi
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L393
	movq	%r8, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %r8
.L393:
	movl	$136, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L389:
	addq	$8, %rbx
	cmpq	%rbx, -64(%rbp)
	je	.L544
.L394:
	movq	(%rbx), %r8
	testq	%r8, %r8
	je	.L389
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L545
	movq	%r8, %rdi
	call	*%rax
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L362:
	movq	%r14, %rdi
	call	*%rax
.L361:
	movq	64(%r13), %r15
	testq	%r15, %r15
	je	.L427
.L553:
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -64(%rbp)
	cmpq	%r14, %rax
	jne	.L434
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L547:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	104(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L431
	call	_ZdlPv@PLT
.L431:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L432
	call	_ZdlPv@PLT
.L432:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L433
	call	_ZdlPv@PLT
.L433:
	movl	$136, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L429:
	addq	$8, %r14
	cmpq	%r14, -64(%rbp)
	je	.L546
.L434:
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.L429
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L547
	movq	%rbx, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -64(%rbp)
	jne	.L434
	.p2align 4,,10
	.p2align 3
.L546:
	movq	(%r15), %r14
.L428:
	testq	%r14, %r14
	je	.L435
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L435:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L427:
	movq	24(%r13), %rdi
	leaq	40(%r13), %rax
	cmpq	%rax, %rdi
	je	.L436
	call	_ZdlPv@PLT
.L436:
	movl	$88, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L355:
	movq	64(%r12), %r14
	testq	%r14, %r14
	je	.L437
	movq	8(%r14), %rbx
	movq	(%r14), %r13
	cmpq	%r13, %rbx
	jne	.L444
	testq	%r13, %r13
	je	.L445
	.p2align 4,,10
	.p2align 3
.L550:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L445:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L437:
	movq	24(%r12), %rdi
	leaq	40(%r12), %rax
	cmpq	%rax, %rdi
	je	.L446
	call	_ZdlPv@PLT
.L446:
	addq	$56, %rsp
	movq	%r12, %rdi
	movl	$88, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L549:
	.cfi_restore_state
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r15), %rdi
	movq	%rax, (%r15)
	leaq	104(%r15), %rax
	cmpq	%rax, %rdi
	je	.L441
	call	_ZdlPv@PLT
.L441:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L442
	call	_ZdlPv@PLT
.L442:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L443
	call	_ZdlPv@PLT
.L443:
	movl	$136, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L439:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L548
.L444:
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L439
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L549
	addq	$8, %r13
	movq	%r15, %rdi
	call	*%rax
	cmpq	%r13, %rbx
	jne	.L444
	.p2align 4,,10
	.p2align 3
.L548:
	movq	(%r14), %r13
	testq	%r13, %r13
	jne	.L550
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L554:
	movq	-80(%rbp), %rax
	movq	(%rax), %rbx
.L408:
	testq	%rbx, %rbx
	je	.L415
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L415:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L407:
	movq	24(%r15), %rdi
	leaq	40(%r15), %rax
	cmpq	%rax, %rdi
	je	.L416
	call	_ZdlPv@PLT
.L416:
	movl	$88, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L367:
	movq	64(%r14), %rax
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L417
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	%rcx, -64(%rbp)
	cmpq	%r15, %rcx
	jne	.L424
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L552:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	104(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L421
	call	_ZdlPv@PLT
.L421:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L422
	call	_ZdlPv@PLT
.L422:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L423
	call	_ZdlPv@PLT
.L423:
	movl	$136, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L419:
	addq	$8, %r15
	cmpq	%r15, -64(%rbp)
	je	.L551
.L424:
	movq	(%r15), %rbx
	testq	%rbx, %rbx
	je	.L419
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L552
	movq	%rbx, %rdi
	addq	$8, %r15
	call	*%rax
	cmpq	%r15, -64(%rbp)
	jne	.L424
	.p2align 4,,10
	.p2align 3
.L551:
	movq	-72(%rbp), %rax
	movq	(%rax), %r15
.L418:
	testq	%r15, %r15
	je	.L425
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L425:
	movq	-72(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L417:
	movq	24(%r14), %rdi
	leaq	40(%r14), %rax
	cmpq	%rax, %rdi
	je	.L426
	call	_ZdlPv@PLT
.L426:
	movl	$88, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	movq	64(%r13), %r15
	testq	%r15, %r15
	jne	.L553
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L544:
	movq	-96(%rbp), %rax
	movq	(%rax), %rbx
.L388:
	testq	%rbx, %rbx
	je	.L395
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L395:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L387:
	movq	-80(%rbp), %rax
	movq	24(%rax), %rdi
	addq	$40, %rax
	cmpq	%rax, %rdi
	je	.L396
	call	_ZdlPv@PLT
.L396:
	movq	-80(%rbp), %rdi
	movl	$88, %esi
	call	_ZdlPvm@PLT
.L379:
	movq	-72(%rbp), %rax
	movq	64(%rax), %rax
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L397
	movq	8(%rax), %rsi
	movq	(%rax), %rbx
	movq	%rsi, -64(%rbp)
	cmpq	%rbx, %rsi
	jne	.L404
	.p2align 4,,10
	.p2align 3
.L398:
	testq	%rbx, %rbx
	je	.L405
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L405:
	movq	-88(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L397:
	movq	-72(%rbp), %rax
	movq	24(%rax), %rdi
	addq	$40, %rax
	cmpq	%rax, %rdi
	je	.L406
	call	_ZdlPv@PLT
.L406:
	movq	-72(%rbp), %rdi
	movl	$88, %esi
	call	_ZdlPvm@PLT
.L373:
	movq	64(%r15), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L407
	movq	8(%rax), %rdx
	movq	(%rax), %rbx
	movq	%rdx, -64(%rbp)
	cmpq	%rbx, %rdx
	jne	.L414
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L555:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r8), %rdi
	movq	%rax, (%r8)
	leaq	104(%r8), %rax
	cmpq	%rax, %rdi
	je	.L411
	movq	%r8, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %r8
.L411:
	movq	48(%r8), %rdi
	leaq	64(%r8), %rax
	cmpq	%rax, %rdi
	je	.L412
	movq	%r8, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %r8
.L412:
	movq	8(%r8), %rdi
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L413
	movq	%r8, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %r8
.L413:
	movl	$136, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L409:
	addq	$8, %rbx
	cmpq	%rbx, -64(%rbp)
	je	.L554
.L414:
	movq	(%rbx), %r8
	testq	%r8, %r8
	je	.L409
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L555
	movq	%r8, %rdi
	call	*%rax
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L557:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r8), %rdi
	movq	%rax, (%r8)
	leaq	104(%r8), %rax
	cmpq	%rax, %rdi
	je	.L401
	movq	%r8, -80(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %r8
.L401:
	movq	48(%r8), %rdi
	leaq	64(%r8), %rax
	cmpq	%rax, %rdi
	je	.L402
	movq	%r8, -80(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %r8
.L402:
	movq	8(%r8), %rdi
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L403
	movq	%r8, -80(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %r8
.L403:
	movl	$136, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L399:
	addq	$8, %rbx
	cmpq	%rbx, -64(%rbp)
	je	.L556
.L404:
	movq	(%rbx), %r8
	testq	%r8, %r8
	je	.L399
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L557
	movq	%r8, %rdi
	call	*%rax
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L352:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L356:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L358:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L368:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L364:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L556:
	movq	-88(%rbp), %rax
	movq	(%rax), %rbx
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L370:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L374:
	movq	%rdx, %rdi
	call	*%rax
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L376:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L380:
	movq	%rsi, %rdi
	call	*%rax
	jmp	.L379
.L386:
	call	*%rax
	jmp	.L385
.L382:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L381
	.cfi_endproc
.LFE5167:
	.size	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev, .-_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime10StackTraceD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD1Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD1Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD1Ev:
.LFB13108:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime10StackTraceE(%rip), %rax
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	72(%rdi), %r12
	movups	%xmm0, -8(%rdi)
	testq	%r12, %r12
	je	.L559
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L592
	movq	%r12, %rdi
	call	*%rax
.L559:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L563
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L593
	call	*%rax
.L563:
	movq	56(%rbx), %r13
	testq	%r13, %r13
	je	.L565
	movq	8(%r13), %r12
	movq	0(%r13), %r15
	cmpq	%r15, %r12
	je	.L566
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L572:
	movq	(%r15), %r9
	testq	%r9, %r9
	je	.L567
	movq	(%r9), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	je	.L594
	movq	%r9, %rdi
	call	*%rax
.L567:
	addq	$8, %r15
	cmpq	%r15, %r12
	jne	.L572
	movq	0(%r13), %r15
.L566:
	testq	%r15, %r15
	je	.L573
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L573:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L565:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L558
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L558:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L594:
	.cfi_restore_state
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r9), %rdi
	movq	%rax, (%r9)
	leaq	104(%r9), %rax
	cmpq	%rax, %rdi
	je	.L569
	movq	%r9, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r9
.L569:
	movq	48(%r9), %rdi
	leaq	64(%r9), %rax
	cmpq	%rax, %rdi
	je	.L570
	movq	%r9, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r9
.L570:
	movq	8(%r9), %rdi
	leaq	24(%r9), %rax
	cmpq	%rax, %rdi
	je	.L571
	movq	%r9, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r9
.L571:
	movl	$136, %esi
	movq	%r9, %rdi
	call	_ZdlPvm@PLT
	jmp	.L567
.L593:
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movq	-56(%rbp), %rdi
	movl	$88, %esi
	call	_ZdlPvm@PLT
	jmp	.L563
.L592:
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r12), %rdi
	leaq	-64(%rax), %rsi
	movq	%rax, %xmm2
	leaq	80(%r12), %rax
	movq	%rsi, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r12)
	cmpq	%rax, %rdi
	je	.L561
	call	_ZdlPv@PLT
.L561:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L562
	call	_ZdlPv@PLT
.L562:
	movl	$104, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L559
	.cfi_endproc
.LFE13108:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD1Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD1Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime10StackTraceD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD0Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD0Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD0Ev:
.LFB13106:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rax, -56(%rbp)
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime10StackTraceE(%rip), %rax
	movq	72(%rdi), %r12
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -8(%rdi)
	testq	%r12, %r12
	je	.L596
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L629
	movq	%r12, %rdi
	call	*%rax
.L596:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L600
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L630
	call	*%rax
.L600:
	movq	56(%rbx), %r13
	testq	%r13, %r13
	je	.L602
	movq	8(%r13), %r12
	movq	0(%r13), %r15
	cmpq	%r15, %r12
	je	.L603
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L609:
	movq	(%r15), %r9
	testq	%r9, %r9
	je	.L604
	movq	(%r9), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	je	.L631
	movq	%r9, %rdi
	call	*%rax
.L604:
	addq	$8, %r15
	cmpq	%r15, %r12
	jne	.L609
	movq	0(%r13), %r15
.L603:
	testq	%r15, %r15
	je	.L610
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L610:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L602:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L611
	call	_ZdlPv@PLT
.L611:
	movq	-56(%rbp), %rdi
	addq	$24, %rsp
	movl	$88, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
.L631:
	.cfi_restore_state
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r9), %rdi
	movq	%rax, (%r9)
	leaq	104(%r9), %rax
	cmpq	%rax, %rdi
	je	.L606
	movq	%r9, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %r9
.L606:
	movq	48(%r9), %rdi
	leaq	64(%r9), %rax
	cmpq	%rax, %rdi
	je	.L607
	movq	%r9, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %r9
.L607:
	movq	8(%r9), %rdi
	leaq	24(%r9), %rax
	cmpq	%rax, %rdi
	je	.L608
	movq	%r9, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %r9
.L608:
	movl	$136, %esi
	movq	%r9, %rdi
	call	_ZdlPvm@PLT
	jmp	.L604
.L630:
	movq	%rdi, -64(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movq	-64(%rbp), %rdi
	movl	$88, %esi
	call	_ZdlPvm@PLT
	jmp	.L600
.L629:
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r12), %rdi
	leaq	-64(%rax), %rsi
	movq	%rax, %xmm2
	leaq	80(%r12), %rax
	movq	%rsi, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r12)
	cmpq	%rax, %rdi
	je	.L598
	call	_ZdlPv@PLT
.L598:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L599
	call	_ZdlPv@PLT
.L599:
	movl	$104, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L596
	.cfi_endproc
.LFE13106:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD0Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12StackTraceIdD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev, @function
_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev:
.LFB5237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	80(%rbx), %rax
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movq	64(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L633
	call	_ZdlPv@PLT
.L633:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L632
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L632:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5237:
	.size	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev, .-_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD1Ev,_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD1Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD1Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD1Ev:
.LFB13107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	72(%rbx), %rax
	subq	$8, %rsp
	movups	%xmm0, -8(%rdi)
	movq	56(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L637
	call	_ZdlPv@PLT
.L637:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L636
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L636:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE13107:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD1Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD1Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12StackTraceIdD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD0Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD0Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD0Ev:
.LFB13105:
	.cfi_startproc
	endbr64
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movups	%xmm0, -8(%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L641
	call	_ZdlPv@PLT
.L641:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L642
	call	_ZdlPv@PLT
.L642:
	popq	%rbx
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE13105:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD0Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD0Ev
	.section	.text._ZN12v8_inspector14V8StackTraceIdC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector14V8StackTraceIdC2Ev
	.type	_ZN12v8_inspector14V8StackTraceIdC2Ev, @function
_ZN12v8_inspector14V8StackTraceIdC2Ev:
.LFB7522:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	$0, (%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE7522:
	.size	_ZN12v8_inspector14V8StackTraceIdC2Ev, .-_ZN12v8_inspector14V8StackTraceIdC2Ev
	.globl	_ZN12v8_inspector14V8StackTraceIdC1Ev
	.set	_ZN12v8_inspector14V8StackTraceIdC1Ev,_ZN12v8_inspector14V8StackTraceIdC2Ev
	.section	.text._ZN12v8_inspector14V8StackTraceIdC2EmSt4pairIllE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector14V8StackTraceIdC2EmSt4pairIllE
	.type	_ZN12v8_inspector14V8StackTraceIdC2EmSt4pairIllE, @function
_ZN12v8_inspector14V8StackTraceIdC2EmSt4pairIllE:
.LFB7525:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movq	%rdx, 8(%rdi)
	movq	%rcx, 16(%rdi)
	ret
	.cfi_endproc
.LFE7525:
	.size	_ZN12v8_inspector14V8StackTraceIdC2EmSt4pairIllE, .-_ZN12v8_inspector14V8StackTraceIdC2EmSt4pairIllE
	.globl	_ZN12v8_inspector14V8StackTraceIdC1EmSt4pairIllE
	.set	_ZN12v8_inspector14V8StackTraceIdC1EmSt4pairIllE,_ZN12v8_inspector14V8StackTraceIdC2EmSt4pairIllE
	.section	.text._ZNK12v8_inspector14V8StackTraceId9IsInvalidEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector14V8StackTraceId9IsInvalidEv
	.type	_ZNK12v8_inspector14V8StackTraceId9IsInvalidEv, @function
_ZNK12v8_inspector14V8StackTraceId9IsInvalidEv:
.LFB7527:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE7527:
	.size	_ZNK12v8_inspector14V8StackTraceId9IsInvalidEv, .-_ZNK12v8_inspector14V8StackTraceId9IsInvalidEv
	.section	.text._ZN12v8_inspector10StackFrameC2EPN2v87IsolateENS1_5LocalINS1_10StackFrameEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10StackFrameC2EPN2v87IsolateENS1_5LocalINS1_10StackFrameEEE
	.type	_ZN12v8_inspector10StackFrameC2EPN2v87IsolateENS1_5LocalINS1_10StackFrameEEE, @function
_ZN12v8_inspector10StackFrameC2EPN2v87IsolateENS1_5LocalINS1_10StackFrameEEE:
.LFB7529:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v810StackFrame15GetFunctionNameEv@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	%r12, %rdi
	call	_ZNK2v810StackFrame11GetScriptIdEv@PLT
	leaq	40(%rbx), %rdi
	movl	%eax, %esi
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	movq	%r12, %rdi
	call	_ZNK2v810StackFrame24GetScriptNameOrSourceURLEv@PLT
	movq	%r13, %rsi
	leaq	80(%rbx), %rdi
	movq	%rax, %rdx
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	%r12, %rdi
	call	_ZNK2v810StackFrame13GetLineNumberEv@PLT
	movq	%r12, %rdi
	subl	$1, %eax
	movl	%eax, 120(%rbx)
	call	_ZNK2v810StackFrame9GetColumnEv@PLT
	movq	%r12, %rdi
	subl	$1, %eax
	movl	%eax, 124(%rbx)
	call	_ZNK2v810StackFrame24GetScriptNameOrSourceURLEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZNK2v810StackFrame13GetScriptNameEv@PLT
	testq	%rax, %rax
	je	.L653
	testq	%r13, %r13
	je	.L651
	movq	(%rax), %rax
	cmpq	%rax, 0(%r13)
	setne	%al
.L649:
	movb	%al, 128(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L654
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L653:
	.cfi_restore_state
	testq	%r13, %r13
	setne	%al
	jmp	.L649
	.p2align 4,,10
	.p2align 3
.L651:
	movl	$1, %eax
	jmp	.L649
.L654:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7529:
	.size	_ZN12v8_inspector10StackFrameC2EPN2v87IsolateENS1_5LocalINS1_10StackFrameEEE, .-_ZN12v8_inspector10StackFrameC2EPN2v87IsolateENS1_5LocalINS1_10StackFrameEEE
	.globl	_ZN12v8_inspector10StackFrameC1EPN2v87IsolateENS1_5LocalINS1_10StackFrameEEE
	.set	_ZN12v8_inspector10StackFrameC1EPN2v87IsolateENS1_5LocalINS1_10StackFrameEEE,_ZN12v8_inspector10StackFrameC2EPN2v87IsolateENS1_5LocalINS1_10StackFrameEEE
	.section	.text._ZN12v8_inspector10StackFrame9translateEPNS_15WasmTranslationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10StackFrame9translateEPNS_15WasmTranslationE
	.type	_ZN12v8_inspector10StackFrame9translateEPNS_15WasmTranslationE, @function
_ZN12v8_inspector10StackFrame9translateEPNS_15WasmTranslationE:
.LFB7531:
	.cfi_startproc
	endbr64
	movq	%rsi, %r8
	leaq	124(%rdi), %rcx
	leaq	120(%rdi), %rdx
	leaq	40(%rdi), %rsi
	movq	%r8, %rdi
	jmp	_ZN12v8_inspector15WasmTranslation45TranslateWasmScriptLocationToProtocolLocationEPNS_8String16EPiS3_@PLT
	.cfi_endproc
.LFE7531:
	.size	_ZN12v8_inspector10StackFrame9translateEPNS_15WasmTranslationE, .-_ZN12v8_inspector10StackFrame9translateEPNS_15WasmTranslationE
	.section	.text._ZNK12v8_inspector10StackFrame12functionNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector10StackFrame12functionNameEv
	.type	_ZNK12v8_inspector10StackFrame12functionNameEv, @function
_ZNK12v8_inspector10StackFrame12functionNameEv:
.LFB7532:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE7532:
	.size	_ZNK12v8_inspector10StackFrame12functionNameEv, .-_ZNK12v8_inspector10StackFrame12functionNameEv
	.section	.text._ZNK12v8_inspector10StackFrame8scriptIdEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector10StackFrame8scriptIdEv
	.type	_ZNK12v8_inspector10StackFrame8scriptIdEv, @function
_ZNK12v8_inspector10StackFrame8scriptIdEv:
.LFB7533:
	.cfi_startproc
	endbr64
	leaq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE7533:
	.size	_ZNK12v8_inspector10StackFrame8scriptIdEv, .-_ZNK12v8_inspector10StackFrame8scriptIdEv
	.section	.text._ZNK12v8_inspector10StackFrame9sourceURLEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector10StackFrame9sourceURLEv
	.type	_ZNK12v8_inspector10StackFrame9sourceURLEv, @function
_ZNK12v8_inspector10StackFrame9sourceURLEv:
.LFB7534:
	.cfi_startproc
	endbr64
	leaq	80(%rdi), %rax
	ret
	.cfi_endproc
.LFE7534:
	.size	_ZNK12v8_inspector10StackFrame9sourceURLEv, .-_ZNK12v8_inspector10StackFrame9sourceURLEv
	.section	.text._ZNK12v8_inspector10StackFrame10lineNumberEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector10StackFrame10lineNumberEv
	.type	_ZNK12v8_inspector10StackFrame10lineNumberEv, @function
_ZNK12v8_inspector10StackFrame10lineNumberEv:
.LFB7535:
	.cfi_startproc
	endbr64
	movl	120(%rdi), %eax
	ret
	.cfi_endproc
.LFE7535:
	.size	_ZNK12v8_inspector10StackFrame10lineNumberEv, .-_ZNK12v8_inspector10StackFrame10lineNumberEv
	.section	.text._ZNK12v8_inspector10StackFrame12columnNumberEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector10StackFrame12columnNumberEv
	.type	_ZNK12v8_inspector10StackFrame12columnNumberEv, @function
_ZNK12v8_inspector10StackFrame12columnNumberEv:
.LFB7536:
	.cfi_startproc
	endbr64
	movl	124(%rdi), %eax
	ret
	.cfi_endproc
.LFE7536:
	.size	_ZNK12v8_inspector10StackFrame12columnNumberEv, .-_ZNK12v8_inspector10StackFrame12columnNumberEv
	.section	.text._ZNK12v8_inspector10StackFrame7isEqualEPS0_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector10StackFrame7isEqualEPS0_
	.type	_ZNK12v8_inspector10StackFrame7isEqualEPS0_, @function
_ZNK12v8_inspector10StackFrame7isEqualEPS0_:
.LFB7542:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rdx
	movq	48(%rsi), %rcx
	movq	40(%rsi), %r9
	movq	40(%rdi), %r10
	cmpq	%rcx, %rdx
	movq	%rcx, %r8
	cmovbe	%rdx, %r8
	testq	%r8, %r8
	je	.L662
	xorl	%eax, %eax
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L674:
	addq	$1, %rax
	cmpq	%rax, %r8
	je	.L662
.L664:
	movzwl	(%r9,%rax,2), %r11d
	cmpw	%r11w, (%r10,%rax,2)
	je	.L674
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L662:
	subq	%rcx, %rdx
	xorl	%eax, %eax
	cmpq	$2147483647, %rdx
	jg	.L661
	cmpq	$-2147483648, %rdx
	jl	.L661
	testl	%edx, %edx
	jne	.L661
	movq	120(%rsi), %rax
	cmpq	%rax, 120(%rdi)
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L661:
	ret
	.cfi_endproc
.LFE7542:
	.size	_ZNK12v8_inspector10StackFrame7isEqualEPS0_, .-_ZNK12v8_inspector10StackFrame7isEqualEPS0_
	.section	.text._ZN12v8_inspector16V8StackTraceImpl41setCaptureStackTraceForUncaughtExceptionsEPN2v87IsolateEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16V8StackTraceImpl41setCaptureStackTraceForUncaughtExceptionsEPN2v87IsolateEb
	.type	_ZN12v8_inspector16V8StackTraceImpl41setCaptureStackTraceForUncaughtExceptionsEPN2v87IsolateEb, @function
_ZN12v8_inspector16V8StackTraceImpl41setCaptureStackTraceForUncaughtExceptionsEPN2v87IsolateEb:
.LFB7543:
	.cfi_startproc
	endbr64
	movl	_ZN12v8_inspector16V8StackTraceImpl25maxCallStackSizeToCaptureE(%rip), %edx
	movzbl	%sil, %esi
	movl	$15, %ecx
	jmp	_ZN2v87Isolate41SetCaptureStackTraceForUncaughtExceptionsEbiNS_10StackTrace17StackTraceOptionsE@PLT
	.cfi_endproc
.LFE7543:
	.size	_ZN12v8_inspector16V8StackTraceImpl41setCaptureStackTraceForUncaughtExceptionsEPN2v87IsolateEb, .-_ZN12v8_inspector16V8StackTraceImpl41setCaptureStackTraceForUncaughtExceptionsEPN2v87IsolateEb
	.section	.text._ZN12v8_inspector16V8StackTraceImpl6createEPNS_10V8DebuggerEiN2v85LocalINS3_10StackTraceEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16V8StackTraceImpl6createEPNS_10V8DebuggerEiN2v85LocalINS3_10StackTraceEEEi
	.type	_ZN12v8_inspector16V8StackTraceImpl6createEPNS_10V8DebuggerEiN2v85LocalINS3_10StackTraceEEEi, @function
_ZN12v8_inspector16V8StackTraceImpl6createEPNS_10V8DebuggerEiN2v85LocalINS3_10StackTraceEEEi:
.LFB7544:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-112(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$152, %rsp
	movl	%edx, -160(%rbp)
	movq	16(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	testq	%rbx, %rbx
	je	.L677
	movq	%rbx, %rdi
	call	_ZNK2v810StackTrace13GetFrameCountEv@PLT
	testl	%eax, %eax
	jne	.L722
.L677:
	movl	-160(%rbp), %esi
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rcx
	xorl	%ebx, %ebx
	leaq	-128(%rbp), %rdx
	movq	%r15, %rdi
	leaq	-132(%rbp), %r8
	movl	$0, -132(%rbp)
	movq	$0, -80(%rbp)
	movaps	%xmm0, -128(%rbp)
	movups	%xmm0, -72(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_119calculateAsyncChainEPNS_10V8DebuggerEiPSt10shared_ptrINS_15AsyncStackTraceEEPNS_14V8StackTraceIdEPi.constprop.0
	movq	$0, -168(%rbp)
.L678:
	movq	-128(%rbp), %rdx
	movq	%rbx, %r14
	testq	%rdx, %rdx
	je	.L723
.L679:
	movq	-120(%rbp), %r15
	testq	%r15, %r15
	je	.L681
.L729:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rcx
	leaq	8(%r15), %rax
	movq	%rax, -184(%rbp)
	testq	%rcx, %rcx
	je	.L682
	lock addl	$1, (%rax)
.L683:
	movq	%rdx, %xmm0
	movq	%r15, %xmm1
	movl	$80, %edi
	movq	%rcx, -176(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -160(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector16V8StackTraceImplE(%rip), %rsi
	movl	-132(%rbp), %edx
	movdqa	-160(%rbp), %xmm0
	movq	%rsi, (%rax)
	movq	-176(%rbp), %rcx
	movq	-168(%rbp), %rsi
	movq	%rbx, 16(%rax)
	leaq	12(%r15), %rbx
	movq	%r14, 8(%rax)
	movq	%rsi, 24(%rax)
	movl	%edx, 32(%rax)
	movups	%xmm0, 40(%rax)
	testq	%rcx, %rcx
	je	.L724
	lock addl	$1, (%rbx)
.L684:
	movdqa	-80(%rbp), %xmm2
	movq	-64(%rbp), %rdx
	movq	%rax, (%r12)
	movq	%rdx, 72(%rax)
	movups	%xmm2, 56(%rax)
	testq	%rcx, %rcx
	je	.L725
	movq	-184(%rbp), %rdi
	movl	$-1, %eax
	lock xaddl	%eax, (%rdi)
	cmpl	$1, %eax
	je	.L726
.L688:
	xorl	%ebx, %ebx
.L685:
	movq	-120(%rbp), %r14
	testq	%r14, %r14
	je	.L702
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rcx
	testq	%rcx, %rcx
	je	.L692
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
	cmpl	$1, %eax
	je	.L727
.L702:
	testq	%rbx, %rbx
	je	.L699
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L699:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L728
	addq	$152, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L722:
	.cfi_restore_state
	leaq	-80(%rbp), %r11
	movl	%r14d, %ecx
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r11, %rdi
	movq	%r11, -176(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_114toFramesVectorEPNS_10V8DebuggerEN2v85LocalINS3_10StackTraceEEEi
	movq	-80(%rbp), %r14
	movq	-72(%rbp), %rbx
	movq	%r15, %rdi
	movq	-176(%rbp), %r11
	movq	-64(%rbp), %rax
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %rdx
	movl	-160(%rbp), %esi
	leaq	-132(%rbp), %r8
	movaps	%xmm0, -128(%rbp)
	movq	%r11, %rcx
	movq	%rax, -168(%rbp)
	movl	$0, -132(%rbp)
	movq	$0, -80(%rbp)
	movups	%xmm0, -72(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_119calculateAsyncChainEPNS_10V8DebuggerEiPSt10shared_ptrINS_15AsyncStackTraceEEPNS_14V8StackTraceIdEPi.constprop.0
	cmpq	%rbx, %r14
	je	.L678
	movq	-120(%rbp), %r15
	movq	-128(%rbp), %rdx
	testq	%r15, %r15
	jne	.L729
.L681:
	movl	$80, %edi
	movq	%rdx, -160(%rbp)
	call	_Znwm@PLT
	movl	-132(%rbp), %ecx
	movdqa	-80(%rbp), %xmm3
	leaq	16+_ZTVN12v8_inspector16V8StackTraceImplE(%rip), %rdi
	movq	-160(%rbp), %rdx
	movq	%rdi, (%rax)
	movq	-168(%rbp), %rdi
	movq	%r14, 8(%rax)
	movq	%rdx, %xmm0
	movq	-64(%rbp), %rdx
	movq	%rbx, 16(%rax)
	movq	%rdi, 24(%rax)
	movl	%ecx, 32(%rax)
	movq	%rdx, 72(%rax)
	movq	%rax, (%r12)
	movups	%xmm0, 40(%rax)
	movups	%xmm3, 56(%rax)
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L682:
	addl	$1, 8(%r15)
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L724:
	addl	$1, 12(%r15)
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L692:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	cmpl	$1, %eax
	jne	.L702
.L727:
	movq	(%r14), %rax
	movq	%rcx, -160(%rbp)
	movq	%r14, %rdi
	call	*16(%rax)
	movq	-160(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L696
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L697:
	cmpl	$1, %eax
	jne	.L702
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L725:
	movl	8(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r15)
	cmpl	$1, %eax
	jne	.L688
.L726:
	movq	(%r15), %rax
	movq	%rcx, -160(%rbp)
	movq	%r15, %rdi
	call	*16(%rax)
	movq	-160(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L689
	movl	$-1, %eax
	lock xaddl	%eax, (%rbx)
.L690:
	cmpl	$1, %eax
	jne	.L688
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L723:
	cmpq	$0, -80(%rbp)
	jne	.L679
	movq	$0, (%r12)
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L696:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L689:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L690
.L728:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7544:
	.size	_ZN12v8_inspector16V8StackTraceImpl6createEPNS_10V8DebuggerEiN2v85LocalINS3_10StackTraceEEEi, .-_ZN12v8_inspector16V8StackTraceImpl6createEPNS_10V8DebuggerEiN2v85LocalINS3_10StackTraceEEEi
	.section	.text._ZN12v8_inspector16V8StackTraceImpl7captureEPNS_10V8DebuggerEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16V8StackTraceImpl7captureEPNS_10V8DebuggerEii
	.type	_ZN12v8_inspector16V8StackTraceImpl7captureEPNS_10V8DebuggerEii, @function
_ZN12v8_inspector16V8StackTraceImpl7captureEPNS_10V8DebuggerEii:
.LFB7564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$56, %rsp
	movl	%edx, -92(%rbp)
	movq	16(%rsi), %r14
	movq	%r14, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movq	%rcx, -88(%rbp)
	call	_ZN2v87Isolate9InContextEv@PLT
	movq	-88(%rbp), %rcx
	movl	-92(%rbp), %r9d
	testb	%al, %al
	jne	.L737
.L731:
	movl	%ebx, %r8d
	movl	%r9d, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector16V8StackTraceImpl6createEPNS_10V8DebuggerEiN2v85LocalINS3_10StackTraceEEEi
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L738
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L737:
	.cfi_restore_state
	movl	$383, %edx
	movl	%ebx, %esi
	movq	%r14, %rdi
	movl	%r9d, -88(%rbp)
	call	_ZN2v810StackTrace17CurrentStackTraceEPNS_7IsolateEiNS0_17StackTraceOptionsE@PLT
	movl	-88(%rbp), %r9d
	movq	%rax, %rcx
	jmp	.L731
.L738:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7564:
	.size	_ZN12v8_inspector16V8StackTraceImpl7captureEPNS_10V8DebuggerEii, .-_ZN12v8_inspector16V8StackTraceImpl7captureEPNS_10V8DebuggerEii
	.section	.text._ZN12v8_inspector16V8StackTraceImplC2ESt6vectorISt10shared_ptrINS_10StackFrameEESaIS4_EEiS2_INS_15AsyncStackTraceEERKNS_14V8StackTraceIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16V8StackTraceImplC2ESt6vectorISt10shared_ptrINS_10StackFrameEESaIS4_EEiS2_INS_15AsyncStackTraceEERKNS_14V8StackTraceIdE
	.type	_ZN12v8_inspector16V8StackTraceImplC2ESt6vectorISt10shared_ptrINS_10StackFrameEESaIS4_EEiS2_INS_15AsyncStackTraceEERKNS_14V8StackTraceIdE, @function
_ZN12v8_inspector16V8StackTraceImplC2ESt6vectorISt10shared_ptrINS_10StackFrameEESaIS4_EEiS2_INS_15AsyncStackTraceEERKNS_14V8StackTraceIdE:
.LFB7574:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12v8_inspector16V8StackTraceImplE(%rip), %rax
	movdqu	(%rcx), %xmm1
	pxor	%xmm0, %xmm0
	movl	%edx, 32(%rdi)
	movq	%rax, (%rdi)
	movq	(%rsi), %rax
	movups	%xmm1, 40(%rdi)
	movq	%rax, 8(%rdi)
	movq	8(%rsi), %rax
	movups	%xmm0, (%rsi)
	movq	%rax, 16(%rdi)
	movq	16(%rsi), %rax
	movq	$0, 16(%rsi)
	movq	%rax, 24(%rdi)
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L740
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L741
	lock addl	$1, 12(%rax)
.L740:
	movdqu	(%r8), %xmm2
	movq	16(%r8), %rax
	movups	%xmm2, 56(%rdi)
	movq	%rax, 72(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L741:
	addl	$1, 12(%rax)
	jmp	.L740
	.cfi_endproc
.LFE7574:
	.size	_ZN12v8_inspector16V8StackTraceImplC2ESt6vectorISt10shared_ptrINS_10StackFrameEESaIS4_EEiS2_INS_15AsyncStackTraceEERKNS_14V8StackTraceIdE, .-_ZN12v8_inspector16V8StackTraceImplC2ESt6vectorISt10shared_ptrINS_10StackFrameEESaIS4_EEiS2_INS_15AsyncStackTraceEERKNS_14V8StackTraceIdE
	.globl	_ZN12v8_inspector16V8StackTraceImplC1ESt6vectorISt10shared_ptrINS_10StackFrameEESaIS4_EEiS2_INS_15AsyncStackTraceEERKNS_14V8StackTraceIdE
	.set	_ZN12v8_inspector16V8StackTraceImplC1ESt6vectorISt10shared_ptrINS_10StackFrameEESaIS4_EEiS2_INS_15AsyncStackTraceEERKNS_14V8StackTraceIdE,_ZN12v8_inspector16V8StackTraceImplC2ESt6vectorISt10shared_ptrINS_10StackFrameEESaIS4_EEiS2_INS_15AsyncStackTraceEERKNS_14V8StackTraceIdE
	.section	.text._ZN12v8_inspector16V8StackTraceImpl18StackFrameIteratorC2EPKS0_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16V8StackTraceImpl18StackFrameIteratorC2EPKS0_
	.type	_ZN12v8_inspector16V8StackTraceImpl18StackFrameIteratorC2EPKS0_, @function
_ZN12v8_inspector16V8StackTraceImpl18StackFrameIteratorC2EPKS0_:
.LFB7604:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movdqu	8(%rsi), %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movups	%xmm0, (%rdi)
	movq	48(%rsi), %r12
	testq	%r12, %r12
	je	.L746
	movl	8(%r12), %eax
	leaq	8(%r12), %rdx
.L748:
	testl	%eax, %eax
	je	.L746
	leal	1(%rax), %ecx
	lock cmpxchgl	%ecx, (%rdx)
	jne	.L748
	movl	8(%r12), %eax
	testl	%eax, %eax
	jne	.L758
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	xorl	%eax, %eax
	movq	%rax, 16(%rdi)
	testq	%rbx, %rbx
	je	.L767
.L756:
	movl	$-1, %eax
	lock xaddl	%eax, (%rdx)
	cmpl	$1, %eax
	je	.L768
.L745:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L758:
	.cfi_restore_state
	movq	40(%rsi), %rax
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	movq	%rax, 16(%rdi)
	testq	%rbx, %rbx
	jne	.L756
.L767:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L745
.L768:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L753
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L754:
	cmpl	$1, %eax
	jne	.L745
	movq	(%r12), %rax
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	24(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L746:
	.cfi_restore_state
	movq	$0, 16(%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L753:
	.cfi_restore_state
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L754
	.cfi_endproc
.LFE7604:
	.size	_ZN12v8_inspector16V8StackTraceImpl18StackFrameIteratorC2EPKS0_, .-_ZN12v8_inspector16V8StackTraceImpl18StackFrameIteratorC2EPKS0_
	.globl	_ZN12v8_inspector16V8StackTraceImpl18StackFrameIteratorC1EPKS0_
	.set	_ZN12v8_inspector16V8StackTraceImpl18StackFrameIteratorC1EPKS0_,_ZN12v8_inspector16V8StackTraceImpl18StackFrameIteratorC2EPKS0_
	.section	.text._ZNK12v8_inspector16V8StackTraceImpl22firstNonEmptySourceURLEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector16V8StackTraceImpl22firstNonEmptySourceURLEv
	.type	_ZNK12v8_inspector16V8StackTraceImpl22firstNonEmptySourceURLEv, @function
_ZNK12v8_inspector16V8StackTraceImpl22firstNonEmptySourceURLEv:
.LFB7583:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	-48(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector16V8StackTraceImpl18StackFrameIteratorC1EPKS0_
	movq	-48(%rbp), %rax
	cmpq	%rax, -40(%rbp)
	jne	.L773
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L771:
	addq	$16, %rax
	movq	%rbx, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator4nextEv.part.0
	movq	-48(%rbp), %rax
	cmpq	%rax, -40(%rbp)
	je	.L770
.L773:
	movq	(%rax), %rsi
	cmpq	$0, 88(%rsi)
	je	.L771
	addq	$80, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector12toStringViewERKNS_8String16E@PLT
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L770:
	movb	$1, (%r12)
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
.L769:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L777
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L777:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7583:
	.size	_ZNK12v8_inspector16V8StackTraceImpl22firstNonEmptySourceURLEv, .-_ZNK12v8_inspector16V8StackTraceImpl22firstNonEmptySourceURLEv
	.section	.text._ZNK12v8_inspector16V8StackTraceImpl23isEqualIgnoringTopFrameEPS0_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector16V8StackTraceImpl23isEqualIgnoringTopFrameEPS0_
	.type	_ZNK12v8_inspector16V8StackTraceImpl23isEqualIgnoringTopFrameEPS0_, @function
_ZNK12v8_inspector16V8StackTraceImpl23isEqualIgnoringTopFrameEPS0_:
.LFB7602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-80(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-112(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector16V8StackTraceImpl18StackFrameIteratorC1EPKS0_
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector16V8StackTraceImpl18StackFrameIteratorC1EPKS0_
	movq	-112(%rbp), %rax
	cmpq	%rax, -104(%rbp)
	je	.L779
	addq	$16, %rax
	movq	%rbx, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator4nextEv.part.0
.L779:
	movq	-80(%rbp), %rax
	cmpq	%rax, -72(%rbp)
	je	.L790
	addq	$16, %rax
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator4nextEv.part.0
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rdx
.L780:
	movabsq	$-2147483649, %r14
	movl	$2147483648, %r13d
.L786:
	movq	-112(%rbp), %rcx
	movq	-104(%rbp), %rsi
	cmpq	%rsi, %rcx
	je	.L781
	cmpq	%rax, %rdx
	je	.L781
	movq	(%rcx), %r8
	movq	(%rax), %rdi
	movq	48(%r8), %rdx
	movq	48(%rdi), %rsi
	movq	40(%rdi), %r10
	movq	40(%r8), %r11
	cmpq	%rsi, %rdx
	movq	%rsi, %r9
	cmovbe	%rdx, %r9
	testq	%r9, %r9
	je	.L782
	xorl	%eax, %eax
	jmp	.L784
	.p2align 4,,10
	.p2align 3
.L803:
	addq	$1, %rax
	cmpq	%rax, %r9
	je	.L782
.L784:
	movzwl	(%r10,%rax,2), %r15d
	cmpw	%r15w, (%r11,%rax,2)
	je	.L803
.L796:
	xorl	%eax, %eax
.L778:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L804
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L782:
	.cfi_restore_state
	subq	%rsi, %rdx
	cmpq	%r13, %rdx
	jge	.L796
	cmpq	%r14, %rdx
	jle	.L796
	testl	%edx, %edx
	jne	.L796
	movq	120(%rdi), %rax
	cmpq	%rax, 120(%r8)
	jne	.L796
	addq	$16, %rcx
	movq	%rbx, %rdi
	movq	%rcx, -112(%rbp)
	call	_ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator4nextEv.part.0
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rdx
	cmpq	%rdx, %rax
	je	.L786
	addq	$16, %rax
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator4nextEv.part.0
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rdx
	jmp	.L786
	.p2align 4,,10
	.p2align 3
.L790:
	movq	%rax, %rdx
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L781:
	cmpq	%rcx, %rsi
	sete	%cl
	cmpq	%rax, %rdx
	sete	%al
	cmpb	%al, %cl
	sete	%al
	jmp	.L778
.L804:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7602:
	.size	_ZNK12v8_inspector16V8StackTraceImpl23isEqualIgnoringTopFrameEPS0_, .-_ZNK12v8_inspector16V8StackTraceImpl23isEqualIgnoringTopFrameEPS0_
	.section	.text._ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator4nextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator4nextEv
	.type	_ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator4nextEv, @function
_ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator4nextEv:
.LFB7606:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	cmpq	%rax, 8(%rdi)
	je	.L805
	addq	$16, %rax
	movq	%rax, (%rdi)
	jmp	_ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator4nextEv.part.0
	.p2align 4,,10
	.p2align 3
.L805:
	ret
	.cfi_endproc
.LFE7606:
	.size	_ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator4nextEv, .-_ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator4nextEv
	.section	.text._ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator4doneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator4doneEv
	.type	_ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator4doneEv, @function
_ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator4doneEv:
.LFB7607:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	cmpq	%rax, (%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE7607:
	.size	_ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator4doneEv, .-_ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator4doneEv
	.section	.text._ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator5frameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator5frameEv
	.type	_ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator5frameEv, @function
_ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator5frameEv:
.LFB7608:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE7608:
	.size	_ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator5frameEv, .-_ZN12v8_inspector16V8StackTraceImpl18StackFrameIterator5frameEv
	.section	.rodata._ZN12v8_inspector15AsyncStackTraceC2EiRKNS_8String16ESt6vectorISt10shared_ptrINS_10StackFrameEESaIS7_EES5_IS0_ERKNS_14V8StackTraceIdE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZN12v8_inspector15AsyncStackTraceC2EiRKNS_8String16ESt6vectorISt10shared_ptrINS_10StackFrameEESaIS7_EES5_IS0_ERKNS_14V8StackTraceIdE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"basic_string::_M_create"
	.section	.text._ZN12v8_inspector15AsyncStackTraceC2EiRKNS_8String16ESt6vectorISt10shared_ptrINS_10StackFrameEESaIS7_EES5_IS0_ERKNS_14V8StackTraceIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15AsyncStackTraceC2EiRKNS_8String16ESt6vectorISt10shared_ptrINS_10StackFrameEESaIS7_EES5_IS0_ERKNS_14V8StackTraceIdE
	.type	_ZN12v8_inspector15AsyncStackTraceC2EiRKNS_8String16ESt6vectorISt10shared_ptrINS_10StackFrameEESaIS7_EES5_IS0_ERKNS_14V8StackTraceIdE, @function
_ZN12v8_inspector15AsyncStackTraceC2EiRKNS_8String16ESt6vectorISt10shared_ptrINS_10StackFrameEESaIS7_EES5_IS0_ERKNS_14V8StackTraceIdE:
.LFB7611:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$40, %rdi
	subq	$40, %rsp
	movl	%esi, -40(%rdi)
	movq	$0, -32(%rdi)
	movq	$0, -24(%rdi)
	movq	%rdi, 24(%rbx)
	movq	(%rdx), %rsi
	movq	8(%rdx), %rcx
	movq	%rsi, %rax
	leaq	(%rcx,%rcx), %r13
	addq	%r13, %rax
	je	.L810
	testq	%rsi, %rsi
	je	.L830
.L810:
	movq	%r13, %r9
	sarq	%r9
	cmpq	$14, %r13
	ja	.L831
.L811:
	cmpq	$2, %r13
	je	.L832
	testq	%r13, %r13
	je	.L814
	movq	%r13, %rdx
	movq	%r9, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	24(%rbx), %rdi
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r8
.L814:
	xorl	%eax, %eax
	movq	%r9, 32(%rbx)
	pxor	%xmm0, %xmm0
	movw	%ax, (%rdi,%rcx,2)
	movq	32(%r14), %rax
	movdqu	(%r12), %xmm1
	movups	%xmm0, (%r12)
	movq	%rax, 56(%rbx)
	movq	16(%r12), %rax
	movups	%xmm1, 64(%rbx)
	movq	%rax, 80(%rbx)
	movq	(%r8), %rax
	movq	$0, 16(%r12)
	movq	%rax, 88(%rbx)
	movq	8(%r8), %rax
	movq	%rax, 96(%rbx)
	testq	%rax, %rax
	je	.L815
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L816
	lock addl	$1, 12(%rax)
.L815:
	movdqu	(%r15), %xmm2
	movq	16(%r15), %rax
	movups	%xmm2, 104(%rbx)
	movq	%rax, 120(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L816:
	.cfi_restore_state
	addl	$1, 12(%rax)
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L832:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	24(%rbx), %rdi
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L831:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r9
	ja	.L833
	leaq	2(%r13), %rdi
	movq	%r9, -80(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %rcx
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rsi
	movq	%rax, 24(%rbx)
	movq	%rax, %rdi
	movq	%r9, 40(%rbx)
	jmp	.L811
.L830:
	leaq	.LC2(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L833:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7611:
	.size	_ZN12v8_inspector15AsyncStackTraceC2EiRKNS_8String16ESt6vectorISt10shared_ptrINS_10StackFrameEESaIS7_EES5_IS0_ERKNS_14V8StackTraceIdE, .-_ZN12v8_inspector15AsyncStackTraceC2EiRKNS_8String16ESt6vectorISt10shared_ptrINS_10StackFrameEESaIS7_EES5_IS0_ERKNS_14V8StackTraceIdE
	.globl	_ZN12v8_inspector15AsyncStackTraceC1EiRKNS_8String16ESt6vectorISt10shared_ptrINS_10StackFrameEESaIS7_EES5_IS0_ERKNS_14V8StackTraceIdE
	.set	_ZN12v8_inspector15AsyncStackTraceC1EiRKNS_8String16ESt6vectorISt10shared_ptrINS_10StackFrameEESaIS7_EES5_IS0_ERKNS_14V8StackTraceIdE,_ZN12v8_inspector15AsyncStackTraceC2EiRKNS_8String16ESt6vectorISt10shared_ptrINS_10StackFrameEESaIS7_EES5_IS0_ERKNS_14V8StackTraceIdE
	.section	.text._ZNK12v8_inspector15AsyncStackTrace14contextGroupIdEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector15AsyncStackTrace14contextGroupIdEv
	.type	_ZNK12v8_inspector15AsyncStackTrace14contextGroupIdEv, @function
_ZNK12v8_inspector15AsyncStackTrace14contextGroupIdEv:
.LFB7614:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	ret
	.cfi_endproc
.LFE7614:
	.size	_ZNK12v8_inspector15AsyncStackTrace14contextGroupIdEv, .-_ZNK12v8_inspector15AsyncStackTrace14contextGroupIdEv
	.section	.text._ZN12v8_inspector15AsyncStackTrace18setSuspendedTaskIdEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15AsyncStackTrace18setSuspendedTaskIdEPv
	.type	_ZN12v8_inspector15AsyncStackTrace18setSuspendedTaskIdEPv, @function
_ZN12v8_inspector15AsyncStackTrace18setSuspendedTaskIdEPv:
.LFB7615:
	.cfi_startproc
	endbr64
	movq	%rsi, 16(%rdi)
	ret
	.cfi_endproc
.LFE7615:
	.size	_ZN12v8_inspector15AsyncStackTrace18setSuspendedTaskIdEPv, .-_ZN12v8_inspector15AsyncStackTrace18setSuspendedTaskIdEPv
	.section	.text._ZNK12v8_inspector15AsyncStackTrace15suspendedTaskIdEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector15AsyncStackTrace15suspendedTaskIdEv
	.type	_ZNK12v8_inspector15AsyncStackTrace15suspendedTaskIdEv, @function
_ZNK12v8_inspector15AsyncStackTrace15suspendedTaskIdEv:
.LFB7616:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE7616:
	.size	_ZNK12v8_inspector15AsyncStackTrace15suspendedTaskIdEv, .-_ZNK12v8_inspector15AsyncStackTrace15suspendedTaskIdEv
	.section	.text._ZN12v8_inspector15AsyncStackTrace5storeEPNS_10V8DebuggerESt10shared_ptrIS0_E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15AsyncStackTrace5storeEPNS_10V8DebuggerESt10shared_ptrIS0_E
	.type	_ZN12v8_inspector15AsyncStackTrace5storeEPNS_10V8DebuggerESt10shared_ptrIS0_E, @function
_ZN12v8_inspector15AsyncStackTrace5storeEPNS_10V8DebuggerESt10shared_ptrIS0_E:
.LFB7617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L853
.L837:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L854
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L853:
	.cfi_restore_state
	movq	8(%rsi), %rax
	movq	%r12, -64(%rbp)
	movq	%rsi, %rbx
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L839
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L840
	lock addl	$1, 8(%rax)
	movq	(%rsi), %r12
.L839:
	leaq	-64(%rbp), %rsi
	call	_ZN12v8_inspector10V8Debugger15storeStackTraceESt10shared_ptrINS_15AsyncStackTraceEE@PLT
	movq	%rax, 8(%r12)
	movq	-56(%rbp), %r12
	testq	%r12, %r12
	je	.L842
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L843
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L844:
	cmpl	$1, %eax
	jne	.L842
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L846
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L847:
	cmpl	$1, %eax
	jne	.L842
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L842:
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L840:
	addl	$1, 8(%rax)
	movq	(%rsi), %r12
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L843:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L846:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L847
.L854:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7617:
	.size	_ZN12v8_inspector15AsyncStackTrace5storeEPNS_10V8DebuggerESt10shared_ptrIS0_E, .-_ZN12v8_inspector15AsyncStackTrace5storeEPNS_10V8DebuggerESt10shared_ptrIS0_E
	.section	.text._ZNK12v8_inspector15AsyncStackTrace11descriptionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector15AsyncStackTrace11descriptionEv
	.type	_ZNK12v8_inspector15AsyncStackTrace11descriptionEv, @function
_ZNK12v8_inspector15AsyncStackTrace11descriptionEv:
.LFB7618:
	.cfi_startproc
	endbr64
	leaq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE7618:
	.size	_ZNK12v8_inspector15AsyncStackTrace11descriptionEv, .-_ZNK12v8_inspector15AsyncStackTrace11descriptionEv
	.section	.text._ZNK12v8_inspector15AsyncStackTrace6parentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector15AsyncStackTrace6parentEv
	.type	_ZNK12v8_inspector15AsyncStackTrace6parentEv, @function
_ZNK12v8_inspector15AsyncStackTrace6parentEv:
.LFB7619:
	.cfi_startproc
	endbr64
	movq	88(%rsi), %rdx
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movq	96(%rsi), %rdx
	movq	%rdx, 8(%rdi)
	testq	%rdx, %rdx
	je	.L856
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L858
	lock addl	$1, 12(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L858:
	addl	$1, 12(%rdx)
.L856:
	ret
	.cfi_endproc
.LFE7619:
	.size	_ZNK12v8_inspector15AsyncStackTrace6parentEv, .-_ZNK12v8_inspector15AsyncStackTrace6parentEv
	.section	.text._ZNK12v8_inspector15AsyncStackTrace7isEmptyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector15AsyncStackTrace7isEmptyEv
	.type	_ZNK12v8_inspector15AsyncStackTrace7isEmptyEv, @function
_ZNK12v8_inspector15AsyncStackTrace7isEmptyEv:
.LFB7626:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	cmpq	%rax, 72(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE7626:
	.size	_ZNK12v8_inspector15AsyncStackTrace7isEmptyEv, .-_ZNK12v8_inspector15AsyncStackTrace7isEmptyEv
	.section	.rodata._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime9CallFrameESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_.str1.1,"aMS",@progbits,1
.LC4:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime9CallFrameESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime9CallFrameESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime9CallFrameESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime9CallFrameESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime9CallFrameESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB10097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movabsq	$1152921504606846975, %rdx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rcx
	movq	8(%rdi), %rax
	movq	%rdi, -88(%rbp)
	movq	%rax, -80(%rbp)
	subq	%rcx, %rax
	sarq	$3, %rax
	movq	%rcx, -56(%rbp)
	cmpq	%rdx, %rax
	je	.L893
	movq	%rsi, %rcx
	movq	%rsi, %r12
	movq	%rsi, %r13
	subq	-56(%rbp), %rcx
	testq	%rax, %rax
	je	.L880
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L894
.L865:
	movq	%rbx, %rdi
	movq	%rcx, -96(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%rax, -64(%rbp)
	addq	%rax, %rbx
	movq	%rbx, -72(%rbp)
	leaq	8(%rax), %rbx
.L879:
	movq	(%r15), %rax
	movq	-64(%rbp), %rsi
	movq	$0, (%r15)
	movq	-56(%rbp), %r15
	movq	%rax, (%rsi,%rcx)
	cmpq	%r15, %r12
	je	.L867
	movq	%rsi, %rbx
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L896:
	movq	88(%r14), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	leaq	104(%r14), %rdx
	movq	%rax, (%r14)
	cmpq	%rdx, %rdi
	je	.L870
	call	_ZdlPv@PLT
.L870:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L871
	call	_ZdlPv@PLT
.L871:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L872
	call	_ZdlPv@PLT
.L872:
	movl	$136, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L868:
	addq	$8, %r15
	addq	$8, %rbx
	cmpq	%r15, %r12
	je	.L895
.L873:
	movq	(%r15), %rdx
	movq	$0, (%r15)
	movq	%rdx, (%rbx)
	movq	(%r15), %r14
	testq	%r14, %r14
	je	.L868
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L896
	addq	$8, %r15
	movq	%r14, %rdi
	addq	$8, %rbx
	call	*%rdx
	cmpq	%r15, %r12
	jne	.L873
	.p2align 4,,10
	.p2align 3
.L895:
	movq	-64(%rbp), %rsi
	movq	%r12, %rax
	subq	-56(%rbp), %rax
	leaq	8(%rsi,%rax), %rbx
.L867:
	movq	-80(%rbp), %rax
	cmpq	%rax, %r12
	je	.L874
	subq	%r12, %rax
	leaq	-8(%rax), %rsi
	movq	%rsi, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rsi, %rsi
	je	.L882
	movq	%rcx, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L876:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L876
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r13
	leaq	(%rbx,%r13), %rdx
	addq	%r12, %r13
	cmpq	%rax, %rcx
	je	.L877
.L875:
	movq	0(%r13), %rax
	movq	%rax, (%rdx)
.L877:
	leaq	8(%rbx,%rsi), %rbx
.L874:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L878
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L878:
	movq	-64(%rbp), %xmm0
	movq	-88(%rbp), %rax
	movq	%rbx, %xmm2
	movq	-72(%rbp), %rsi
	punpcklqdq	%xmm2, %xmm0
	movq	%rsi, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L894:
	.cfi_restore_state
	testq	%rsi, %rsi
	jne	.L866
	movq	$0, -72(%rbp)
	movl	$8, %ebx
	movq	$0, -64(%rbp)
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L880:
	movl	$8, %ebx
	jmp	.L865
.L882:
	movq	%rbx, %rdx
	jmp	.L875
.L866:
	cmpq	%rdx, %rsi
	cmovbe	%rsi, %rdx
	leaq	0(,%rdx,8), %rbx
	jmp	.L865
.L893:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10097:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime9CallFrameESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime9CallFrameESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_:
.LFB10651:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L916
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r13
	movq	8(%rsi), %r15
	cmpq	%r13, %rdx
	je	.L907
	movq	16(%rdi), %rax
.L899:
	cmpq	%r15, %rax
	jb	.L919
	leaq	(%r15,%r15), %rdx
	testq	%r15, %r15
	je	.L903
.L922:
	movq	(%r12), %rsi
	cmpq	$1, %r15
	je	.L920
	testq	%rdx, %rdx
	je	.L903
	movq	%r13, %rdi
	call	memmove@PLT
	movq	(%rbx), %r13
.L903:
	xorl	%eax, %eax
	movq	%r15, 8(%rbx)
	movw	%ax, 0(%r13,%r15,2)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L919:
	.cfi_restore_state
	movabsq	$2305843009213693951, %rcx
	cmpq	%rcx, %r15
	ja	.L921
	addq	%rax, %rax
	movq	%r15, %r14
	cmpq	%rax, %r15
	jnb	.L902
	cmpq	%rcx, %rax
	cmovbe	%rax, %rcx
	movq	%rcx, %r14
.L902:
	leaq	2(%r14,%r14), %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	(%rbx), %rdi
	movq	-56(%rbp), %rdx
	movq	%rax, %r13
	cmpq	%rdi, %rdx
	je	.L906
	call	_ZdlPv@PLT
.L906:
	movq	%r13, (%rbx)
	leaq	(%r15,%r15), %rdx
	movq	%r14, 16(%rbx)
	testq	%r15, %r15
	je	.L903
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L916:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L907:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$7, %eax
	jmp	.L899
	.p2align 4,,10
	.p2align 3
.L920:
	movzwl	(%rsi), %eax
	movw	%ax, 0(%r13)
	movq	(%rbx), %r13
	jmp	.L903
.L921:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10651:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm:
.LFB11848:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %r9
	subq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	addq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %rax
	movq	%rsi, -72(%rbp)
	movq	%rax, %r13
	leaq	(%r9,%rax), %r15
	subq	%rsi, %r13
	cmpq	(%rdi), %r14
	je	.L936
	movq	16(%rdi), %rax
.L924:
	movabsq	$2305843009213693951, %rdx
	cmpq	%rdx, %r15
	ja	.L957
	cmpq	%rax, %r15
	jbe	.L926
	addq	%rax, %rax
	cmpq	%rax, %r15
	jnb	.L926
	movabsq	$4611686018427387904, %rdi
	movq	%rdx, %r15
	cmpq	%rdx, %rax
	ja	.L927
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L926:
	leaq	2(%r15,%r15), %rdi
.L927:
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	testq	%r12, %r12
	movq	(%rbx), %r11
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	movq	%rax, %r10
	je	.L929
	cmpq	$1, %r12
	je	.L958
	movq	%r12, %rdx
	addq	%rdx, %rdx
	je	.L929
	movq	%r11, %rsi
	movq	%rax, %rdi
	movq	%r8, -80(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memmove@PLT
	movq	-80(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r11
	movq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L929:
	testq	%rcx, %rcx
	je	.L931
	testq	%r8, %r8
	je	.L931
	leaq	(%r10,%r12,2), %rdi
	cmpq	$1, %r8
	je	.L959
	movq	%r8, %rdx
	addq	%rdx, %rdx
	je	.L931
	movq	%rcx, %rsi
	movq	%r8, -80(%rbp)
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memcpy@PLT
	movq	-80(%rbp), %r8
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %r11
.L931:
	testq	%r13, %r13
	jne	.L960
.L933:
	cmpq	%r11, %r14
	je	.L935
	movq	%r11, %rdi
	movq	%r10, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r10
.L935:
	movq	%r15, 16(%rbx)
	movq	%r10, (%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L960:
	.cfi_restore_state
	movq	-72(%rbp), %rax
	addq	%r12, %r8
	leaq	(%r10,%r8,2), %rdi
	leaq	(%r11,%rax,2), %rsi
	cmpq	$1, %r13
	je	.L961
	addq	%r13, %r13
	je	.L933
	movq	%r13, %rdx
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memmove@PLT
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %r11
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L936:
	movl	$7, %eax
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L961:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L958:
	movzwl	(%r11), %eax
	movw	%ax, (%r10)
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L959:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	testq	%r13, %r13
	je	.L933
	jmp	.L960
.L957:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11848:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag:
.LFB11865:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	testq	%rsi, %rsi
	jne	.L963
	testq	%rdx, %rdx
	jne	.L979
.L963:
	subq	%r13, %rbx
	movq	%rbx, %r14
	sarq	%r14
	cmpq	$15, %rbx
	ja	.L964
	movq	(%r12), %rdi
.L965:
	cmpq	$2, %rbx
	je	.L980
	testq	%rbx, %rbx
	je	.L968
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	movq	(%r12), %rdi
.L968:
	xorl	%eax, %eax
	movq	%r14, 8(%r12)
	movw	%ax, (%rdi,%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L980:
	.cfi_restore_state
	movzwl	0(%r13), %eax
	movw	%ax, (%rdi)
	movq	(%r12), %rdi
	jmp	.L968
	.p2align 4,,10
	.p2align 3
.L964:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r14
	ja	.L981
	leaq	2(%rbx), %rdi
	call	_Znwm@PLT
	movq	%r14, 16(%r12)
	movq	%rax, (%r12)
	movq	%rax, %rdi
	jmp	.L965
.L979:
	leaq	.LC2(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L981:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11865:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	.section	.text._ZNK12v8_inspector10StackFrame20buildInspectorObjectEPNS_17V8InspectorClientE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector10StackFrame20buildInspectorObjectEPNS_17V8InspectorClientE
	.type	_ZNK12v8_inspector10StackFrame20buildInspectorObjectEPNS_17V8InspectorClientE, @function
_ZNK12v8_inspector10StackFrame20buildInspectorObjectEPNS_17V8InspectorClientE:
.LFB7537:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-144(%rbp), %r15
	leaq	-128(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$168, %rsp
	movq	80(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	88(%r12), %rax
	movq	%r14, -144(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	112(%r12), %rax
	movq	%rax, -112(%rbp)
	testq	%rbx, %rbx
	je	.L983
	cmpb	$0, 128(%r12)
	je	.L1012
.L983:
	movl	$136, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	24(%rbx), %rax
	leaq	88(%rbx), %r8
	movq	%rax, 8(%rbx)
	xorl	%eax, %eax
	leaq	48(%rbx), %r9
	leaq	8(%rbx), %rdi
	movw	%ax, 24(%rbx)
	leaq	64(%rbx), %rax
	movq	%rax, 48(%rbx)
	leaq	104(%rbx), %rax
	movw	%dx, 64(%rbx)
	movw	%cx, 104(%rbx)
	movq	%rax, 88(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 128(%rbx)
	movq	%r8, -200(%rbp)
	movq	%r9, -208(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r12), %rax
	leaq	40(%r12), %rsi
	movq	-208(%rbp), %r9
	movq	%rax, 40(%rbx)
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	72(%r12), %rax
	movq	-200(%rbp), %r8
	movq	%r15, %rsi
	movq	%rax, 80(%rbx)
	movq	%r8, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-112(%rbp), %rax
	movq	-144(%rbp), %rdi
	movq	%rax, 120(%rbx)
	movq	120(%r12), %rax
	movq	%rbx, 0(%r13)
	movq	%rax, 128(%rbx)
	cmpq	%r14, %rdi
	je	.L982
	call	_ZdlPv@PLT
.L982:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1013
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1012:
	.cfi_restore_state
	cmpq	$0, -136(%rbp)
	je	.L983
	movq	(%rbx), %rax
	leaq	-176(%rbp), %rdx
	leaq	80(%r12), %rsi
	movq	%rdx, %rdi
	movq	%rdx, -200(%rbp)
	movq	216(%rax), %rax
	movq	%rax, -208(%rbp)
	call	_ZN12v8_inspector12toStringViewERKNS_8String16E@PLT
	movq	-208(%rbp), %rax
	leaq	_ZN12v8_inspector17V8InspectorClient17resourceNameToUrlERKNS_10StringViewE(%rip), %rcx
	movq	-200(%rbp), %rdx
	cmpq	%rcx, %rax
	je	.L983
	leaq	-184(%rbp), %rdi
	movq	%rbx, %rsi
	call	*%rax
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L983
	movq	(%rdi), %rax
	leaq	-80(%rbp), %rbx
	call	*16(%rax)
	leaq	-96(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN12v8_inspector10toString16ERKNS_10StringViewE@PLT
	movq	-96(%rbp), %rdx
	movq	-144(%rbp), %rdi
	movq	-88(%rbp), %rax
	cmpq	%rbx, %rdx
	je	.L1014
	movq	-80(%rbp), %rcx
	cmpq	%r14, %rdi
	je	.L1015
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	-128(%rbp), %rsi
	movq	%rdx, -144(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -136(%rbp)
	testq	%rdi, %rdi
	je	.L993
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L991:
	xorl	%esi, %esi
	movq	$0, -88(%rbp)
	movw	%si, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -112(%rbp)
	cmpq	%rbx, %rdi
	je	.L994
	call	_ZdlPv@PLT
.L994:
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L983
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L983
	.p2align 4,,10
	.p2align 3
.L1014:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L989
	cmpq	$1, %rax
	je	.L1016
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L989
	movq	%rbx, %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	-144(%rbp), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L989:
	xorl	%r8d, %r8d
	movq	%rax, -136(%rbp)
	movw	%r8w, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L991
	.p2align 4,,10
	.p2align 3
.L1015:
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	%rdx, -144(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, -136(%rbp)
.L993:
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L991
.L1016:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	-144(%rbp), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L989
.L1013:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7537:
	.size	_ZNK12v8_inspector10StackFrame20buildInspectorObjectEPNS_17V8InspectorClientE, .-_ZNK12v8_inspector10StackFrame20buildInspectorObjectEPNS_17V8InspectorClientE
	.section	.text._ZN12v8_inspector12_GLOBAL__N_126buildInspectorObjectCommonEPNS_10V8DebuggerERKSt6vectorISt10shared_ptrINS_10StackFrameEESaIS6_EERKNS_8String16ERKS4_INS_15AsyncStackTraceEERKNS_14V8StackTraceIdEi,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_126buildInspectorObjectCommonEPNS_10V8DebuggerERKSt6vectorISt10shared_ptrINS_10StackFrameEESaIS6_EERKNS_8String16ERKS4_INS_15AsyncStackTraceEERKNS_14V8StackTraceIdEi, @function
_ZN12v8_inspector12_GLOBAL__N_126buildInspectorObjectCommonEPNS_10V8DebuggerERKSt6vectorISt10shared_ptrINS_10StackFrameEESaIS6_EERKNS_8String16ERKS4_INS_15AsyncStackTraceEERKNS_14V8StackTraceIdEi:
.LFB7494:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -232(%rbp)
	movq	(%r8), %rsi
	movq	%rcx, -240(%rbp)
	movq	%r8, -256(%rbp)
	movq	%r9, -248(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1018
	movq	(%rdx), %rax
	cmpq	%rax, 8(%rdx)
	je	.L1500
.L1018:
	movl	$24, %edi
	leaq	-208(%rbp), %r14
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	%rax, %rbx
	movups	%xmm0, (%rax)
	movq	8(%r12), %rax
	movq	(%r12), %r12
	movq	%rax, -224(%rbp)
	cmpq	%r12, %rax
	jne	.L1033
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1501:
	movq	-208(%rbp), %rax
	movq	$0, -208(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%rbx)
.L1027:
	movq	-208(%rbp), %r15
	testq	%r15, %r15
	je	.L1028
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1029
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r15), %rdi
	movq	%rax, (%r15)
	leaq	104(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1030
	call	_ZdlPv@PLT
.L1030:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1031
	call	_ZdlPv@PLT
.L1031:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1032
	call	_ZdlPv@PLT
.L1032:
	movl	$136, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1028:
	addq	$16, %r12
	cmpq	%r12, -224(%rbp)
	je	.L1034
.L1033:
	xorl	%edx, %edx
	testq	%r13, %r13
	je	.L1025
	movq	24(%r13), %rdx
	testq	%rdx, %rdx
	je	.L1025
	movq	16(%rdx), %rdx
.L1025:
	movq	(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNK12v8_inspector10StackFrame20buildInspectorObjectEPNS_17V8InspectorClientE
	movq	8(%rbx), %rsi
	cmpq	16(%rbx), %rsi
	jne	.L1501
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime9CallFrameESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L1027
	.p2align 4,,10
	.p2align 3
.L1029:
	movq	%r15, %rdi
	addq	$16, %r12
	call	*%rax
	cmpq	%r12, -224(%rbp)
	jne	.L1033
	.p2align 4,,10
	.p2align 3
.L1034:
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime10StackTraceE(%rip), %rax
	movl	$88, %edi
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm2
	movq	%rdx, %xmm1
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm1, -224(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movb	$0, 16(%rax)
	movq	%rax, %r12
	leaq	24(%rax), %r14
	leaq	40(%rax), %rax
	movups	%xmm1, -40(%rax)
	movups	%xmm0, (%rax)
	movq	$0, 16(%rax)
	movq	%rax, 24(%r12)
	movq	-240(%rbp), %rax
	movq	$0, 32(%r12)
	movq	8(%rax), %rax
	movq	%rbx, 64(%r12)
	movq	$0, 72(%r12)
	movq	$0, 80(%r12)
	testq	%rax, %rax
	jne	.L1502
.L1024:
	movq	-256(%rbp), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L1036
	movl	16(%rbp), %ecx
	testl	%ecx, %ecx
	jg	.L1503
	testq	%r13, %r13
	je	.L1036
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movl	$104, %edi
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm6
	movq	%rdx, %xmm4
	punpcklqdq	%xmm6, %xmm4
	movaps	%xmm4, -224(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm4
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movq	%rax, %rbx
	leaq	32(%rax), %rax
	movups	%xmm4, -32(%rax)
	movq	%rax, 16(%rbx)
	leaq	80(%rbx), %rax
	movq	%rax, 64(%rbx)
	movq	-256(%rbp), %rax
	movw	%dx, 32(%rbx)
	movq	8(%rax), %r14
	movq	$0, 24(%rbx)
	movq	$0, 48(%rbx)
	movq	(%rax), %rdx
	movb	$0, 56(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 72(%rbx)
	movups	%xmm0, 80(%rbx)
	testq	%r14, %r14
	je	.L1240
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	leaq	8(%r14), %rax
	testq	%r15, %r15
	je	.L1241
	lock addl	$1, (%rax)
.L1242:
	movq	8(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L1504
.L1273:
	leaq	-96(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN12v8_inspector20stackTraceIdToStringEm@PLT
	movq	%r13, %rsi
	leaq	16(%rbx), %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movq	80(%r12), %r13
	movq	%rbx, 80(%r12)
	movq	%rax, 48(%rbx)
	testq	%r13, %r13
	je	.L1251
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1252
	movdqa	-224(%rbp), %xmm5
	movq	64(%r13), %rdi
	leaq	80(%r13), %rax
	movups	%xmm5, 0(%r13)
	cmpq	%rax, %rdi
	je	.L1253
	call	_ZdlPv@PLT
.L1253:
	movq	16(%r13), %rdi
	leaq	32(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1254
	call	_ZdlPv@PLT
.L1254:
	movl	$104, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1251:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1255
	call	_ZdlPv@PLT
.L1255:
	testq	%r14, %r14
	je	.L1036
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L1258
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
.L1259:
	cmpl	$1, %eax
	je	.L1505
	.p2align 4,,10
	.p2align 3
.L1036:
	movq	-248(%rbp), %rax
	cmpq	$0, (%rax)
	jne	.L1506
.L1263:
	movq	-232(%rbp), %rax
	movq	%r12, (%rax)
.L1017:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1507
	movq	-232(%rbp), %rax
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1503:
	.cfi_restore_state
	movl	16(%rbp), %eax
	leaq	-208(%rbp), %rdi
	movq	%r13, %rdx
	leal	-1(%rax), %ecx
	call	_ZNK12v8_inspector15AsyncStackTrace20buildInspectorObjectEPNS_10V8DebuggerEi
	movq	-208(%rbp), %rax
	movq	72(%r12), %r15
	movq	$0, -208(%rbp)
	movq	%rax, 72(%r12)
	testq	%r15, %r15
	je	.L1036
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1040
	movdqa	-224(%rbp), %xmm7
	movq	80(%r15), %r13
	movups	%xmm7, (%r15)
	testq	%r13, %r13
	je	.L1041
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1042
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r13), %rdi
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm7
	leaq	80(%r13), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 0(%r13)
	cmpq	%rax, %rdi
	je	.L1043
	call	_ZdlPv@PLT
.L1043:
	movq	16(%r13), %rdi
	leaq	32(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1044
	call	_ZdlPv@PLT
.L1044:
	movl	$104, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1041:
	movq	72(%r15), %r14
	testq	%r14, %r14
	je	.L1045
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1046
	movdqa	-224(%rbp), %xmm6
	movq	80(%r14), %r13
	movups	%xmm6, (%r14)
	testq	%r13, %r13
	je	.L1047
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1048
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r13), %rdi
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm6
	leaq	80(%r13), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 0(%r13)
	cmpq	%rax, %rdi
	je	.L1049
	call	_ZdlPv@PLT
.L1049:
	movq	16(%r13), %rdi
	leaq	32(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1050
	call	_ZdlPv@PLT
.L1050:
	movl	$104, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1047:
	movq	72(%r14), %rbx
	testq	%rbx, %rbx
	je	.L1051
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1052
	movdqa	-224(%rbp), %xmm6
	movq	80(%rbx), %r13
	movups	%xmm6, (%rbx)
	testq	%r13, %r13
	je	.L1053
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1054
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r13), %rdi
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm7
	leaq	80(%r13), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 0(%r13)
	cmpq	%rax, %rdi
	je	.L1055
	call	_ZdlPv@PLT
.L1055:
	movq	16(%r13), %rdi
	leaq	32(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1056
	call	_ZdlPv@PLT
.L1056:
	movl	$104, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1053:
	movq	72(%rbx), %r13
	testq	%r13, %r13
	je	.L1057
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1058
	movdqa	-224(%rbp), %xmm5
	movq	80(%r13), %r8
	movups	%xmm5, 0(%r13)
	testq	%r8, %r8
	je	.L1059
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1060
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r8), %rdi
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm5
	leaq	80(%r8), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, (%r8)
	cmpq	%rax, %rdi
	je	.L1061
	movq	%r8, -240(%rbp)
	call	_ZdlPv@PLT
	movq	-240(%rbp), %r8
.L1061:
	movq	16(%r8), %rdi
	leaq	32(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1062
	movq	%r8, -240(%rbp)
	call	_ZdlPv@PLT
	movq	-240(%rbp), %r8
.L1062:
	movl	$104, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1059:
	movq	72(%r13), %rcx
	movq	%rcx, -240(%rbp)
	testq	%rcx, %rcx
	je	.L1063
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1064
	movdqa	-224(%rbp), %xmm5
	movq	80(%rcx), %r8
	movups	%xmm5, (%rcx)
	testq	%r8, %r8
	je	.L1065
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1066
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r8), %rdi
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm7
	leaq	80(%r8), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, (%r8)
	cmpq	%rax, %rdi
	je	.L1067
	movq	%r8, -256(%rbp)
	call	_ZdlPv@PLT
	movq	-256(%rbp), %r8
.L1067:
	movq	16(%r8), %rdi
	leaq	32(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1068
	movq	%r8, -256(%rbp)
	call	_ZdlPv@PLT
	movq	-256(%rbp), %r8
.L1068:
	movl	$104, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1065:
	movq	-240(%rbp), %rax
	movq	72(%rax), %rcx
	movq	%rcx, -256(%rbp)
	testq	%rcx, %rcx
	je	.L1069
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1070
	movdqa	-224(%rbp), %xmm5
	movq	80(%rcx), %r8
	movups	%xmm5, (%rcx)
	testq	%r8, %r8
	je	.L1071
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1072
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r8), %rdi
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm6
	leaq	80(%r8), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, (%r8)
	cmpq	%rax, %rdi
	je	.L1073
	movq	%r8, -264(%rbp)
	call	_ZdlPv@PLT
	movq	-264(%rbp), %r8
.L1073:
	movq	16(%r8), %rdi
	leaq	32(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1074
	movq	%r8, -264(%rbp)
	call	_ZdlPv@PLT
	movq	-264(%rbp), %r8
.L1074:
	movl	$104, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1071:
	movq	-256(%rbp), %rax
	movq	72(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1075
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1076
	movq	%rdi, -264(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movq	-264(%rbp), %rdi
	movl	$88, %esi
	call	_ZdlPvm@PLT
.L1075:
	movq	-256(%rbp), %rax
	movq	64(%rax), %rax
	movq	%rax, -272(%rbp)
	testq	%rax, %rax
	je	.L1077
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -264(%rbp)
	cmpq	%rax, %rcx
	je	.L1086
	movq	%rbx, -288(%rbp)
	movq	%rax, %rbx
	movq	%r12, -280(%rbp)
	jmp	.L1078
.L1509:
	movq	88(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	leaq	104(%r12), %rdx
	movq	%rax, (%r12)
	cmpq	%rdx, %rdi
	je	.L1083
	call	_ZdlPv@PLT
.L1083:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rdx
	cmpq	%rdx, %rdi
	je	.L1084
	call	_ZdlPv@PLT
.L1084:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rdx
	cmpq	%rdx, %rdi
	je	.L1085
	call	_ZdlPv@PLT
.L1085:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1081:
	addq	$8, %rbx
	cmpq	%rbx, -264(%rbp)
	je	.L1508
.L1078:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1081
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1509
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L1081
	.p2align 4,,10
	.p2align 3
.L1502:
	movq	-240(%rbp), %rcx
	leaq	-96(%rbp), %r15
	leaq	-80(%rbp), %rbx
	movq	%r15, %rdi
	movq	%rbx, -96(%rbp)
	movq	(%rcx), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-240(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	32(%rcx), %rax
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movb	$1, 16(%r12)
	movq	%rax, 56(%r12)
	cmpq	%rbx, %rdi
	je	.L1024
	call	_ZdlPv@PLT
	jmp	.L1024
	.p2align 4,,10
	.p2align 3
.L1506:
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movl	$104, %edi
	leaq	-192(%rbp), %r13
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm5
	movq	%rdx, %xmm3
	punpcklqdq	%xmm5, %xmm3
	movaps	%xmm3, -224(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	%rax, %rbx
	leaq	16(%rax), %r14
	leaq	32(%rax), %rax
	movups	%xmm3, -32(%rax)
	leaq	64(%rbx), %r15
	movq	%rax, 16(%rbx)
	xorl	%eax, %eax
	movw	%ax, 32(%rbx)
	leaq	80(%rbx), %rax
	movq	%rax, 64(%rbx)
	movq	-248(%rbp), %rax
	movq	$0, 24(%rbx)
	movq	$0, 48(%rbx)
	movq	(%rax), %rsi
	movb	$0, 56(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 72(%rbx)
	movups	%xmm0, 80(%rbx)
	call	_ZN12v8_inspector20stackTraceIdToStringEm@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-96(%rbp), %r14
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-160(%rbp), %rax
	leaq	-144(%rbp), %rdi
	leaq	-80(%rbp), %r13
	movq	%rax, 48(%rbx)
	movq	-248(%rbp), %rax
	leaq	8(%rax), %rsi
	call	_ZN12v8_inspector18debuggerIdToStringERKSt4pairIllE@PLT
	movq	-144(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r13, -96(%rbp)
	movq	-136(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-112(%rbp), %rax
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movb	$1, 56(%rbx)
	movq	%rax, 96(%rbx)
	cmpq	%r13, %rdi
	je	.L1264
	call	_ZdlPv@PLT
.L1264:
	movq	80(%r12), %r13
	movq	%rbx, 80(%r12)
	testq	%r13, %r13
	je	.L1265
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1266
	movdqa	-224(%rbp), %xmm7
	movq	64(%r13), %rdi
	leaq	80(%r13), %rax
	movups	%xmm7, 0(%r13)
	cmpq	%rax, %rdi
	je	.L1267
	call	_ZdlPv@PLT
.L1267:
	movq	16(%r13), %rdi
	leaq	32(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1268
	call	_ZdlPv@PLT
.L1268:
	movl	$104, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1265:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1269
	call	_ZdlPv@PLT
.L1269:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1263
	call	_ZdlPv@PLT
	jmp	.L1263
	.p2align 4,,10
	.p2align 3
.L1500:
	movq	8(%rcx), %r8
	movq	32(%rsi), %r9
	movq	%rcx, %rax
	movq	(%rax), %rdi
	movq	24(%rsi), %rcx
	cmpq	%r9, %r8
	movq	%r9, %rdx
	cmovbe	%r8, %rdx
	testq	%rdx, %rdx
	je	.L1019
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1020:
	movzwl	(%rcx,%rax,2), %ebx
	cmpw	%bx, (%rdi,%rax,2)
	jne	.L1018
	addq	$1, %rax
	cmpq	%rdx, %rax
	jne	.L1020
.L1019:
	subq	%r9, %r8
	cmpq	$2147483647, %r8
	jg	.L1018
	cmpq	$-2147483648, %r8
	jl	.L1018
	testl	%r8d, %r8d
	jne	.L1018
	movl	16(%rbp), %ecx
	movq	-232(%rbp), %rdi
	movq	%r13, %rdx
	call	_ZNK12v8_inspector15AsyncStackTrace20buildInspectorObjectEPNS_10V8DebuggerEi
	jmp	.L1017
.L1240:
	movq	8(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.L1273
	movq	%rdx, -208(%rbp)
	movq	$0, -200(%rbp)
.L1243:
	movq	%r13, %rdi
	leaq	-208(%rbp), %rsi
	movq	%rdx, -240(%rbp)
	call	_ZN12v8_inspector10V8Debugger15storeStackTraceESt10shared_ptrINS_15AsyncStackTraceEE@PLT
	movq	-240(%rbp), %rdx
	movq	%rax, 8(%rdx)
	movq	-200(%rbp), %r13
	testq	%r13, %r13
	je	.L1245
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L1246
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L1247:
	cmpl	$1, %eax
	je	.L1510
.L1245:
	movq	8(%rdx), %rsi
	jmp	.L1273
.L1046:
	movq	%r14, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L1045:
	movq	64(%r15), %rax
	movq	%rax, -240(%rbp)
	testq	%rax, %rax
	je	.L1129
	movq	8(%rax), %rbx
	movq	(%rax), %r14
	cmpq	%r14, %rbx
	jne	.L1136
	jmp	.L1130
	.p2align 4,,10
	.p2align 3
.L1512:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	104(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1133
	call	_ZdlPv@PLT
.L1133:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1134
	call	_ZdlPv@PLT
.L1134:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1135
	call	_ZdlPv@PLT
.L1135:
	movl	$136, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1131:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L1511
.L1136:
	movq	(%r14), %r13
	testq	%r13, %r13
	je	.L1131
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1512
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1131
.L1511:
	movq	-240(%rbp), %rax
	movq	(%rax), %r14
.L1130:
	testq	%r14, %r14
	je	.L1137
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1137:
	movq	-240(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1129:
	movq	24(%r15), %rdi
	leaq	40(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1138
	call	_ZdlPv@PLT
.L1138:
	movl	$88, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1139:
	movq	-208(%rbp), %r14
	testq	%r14, %r14
	je	.L1036
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1141
	movdqa	-224(%rbp), %xmm6
	movq	80(%r14), %r13
	movups	%xmm6, (%r14)
	testq	%r13, %r13
	je	.L1142
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1143
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r13), %rdi
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm7
	leaq	80(%r13), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 0(%r13)
	cmpq	%rax, %rdi
	je	.L1144
	call	_ZdlPv@PLT
.L1144:
	movq	16(%r13), %rdi
	leaq	32(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1145
	call	_ZdlPv@PLT
.L1145:
	movl	$104, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1142:
	movq	72(%r14), %r15
	testq	%r15, %r15
	je	.L1146
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1147
	movdqa	-224(%rbp), %xmm5
	movq	80(%r15), %r13
	movups	%xmm5, (%r15)
	testq	%r13, %r13
	je	.L1148
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1149
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r13), %rdi
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm7
	leaq	80(%r13), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 0(%r13)
	cmpq	%rax, %rdi
	je	.L1150
	call	_ZdlPv@PLT
.L1150:
	movq	16(%r13), %rdi
	leaq	32(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1151
	call	_ZdlPv@PLT
.L1151:
	movl	$104, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1148:
	movq	72(%r15), %rbx
	testq	%rbx, %rbx
	je	.L1152
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1153
	movdqa	-224(%rbp), %xmm5
	movq	80(%rbx), %r13
	movups	%xmm5, (%rbx)
	testq	%r13, %r13
	je	.L1154
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1155
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r13), %rdi
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm7
	leaq	80(%r13), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 0(%r13)
	cmpq	%rax, %rdi
	je	.L1156
	call	_ZdlPv@PLT
.L1156:
	movq	16(%r13), %rdi
	leaq	32(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1157
	call	_ZdlPv@PLT
.L1157:
	movl	$104, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1154:
	movq	72(%rbx), %rcx
	movq	%rcx, -240(%rbp)
	testq	%rcx, %rcx
	je	.L1158
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1159
	movdqa	-224(%rbp), %xmm6
	movq	80(%rcx), %r13
	movups	%xmm6, (%rcx)
	testq	%r13, %r13
	je	.L1160
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1161
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r13), %rdi
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm7
	leaq	80(%r13), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 0(%r13)
	cmpq	%rax, %rdi
	je	.L1162
	call	_ZdlPv@PLT
.L1162:
	movq	16(%r13), %rdi
	leaq	32(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1163
	call	_ZdlPv@PLT
.L1163:
	movl	$104, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1160:
	movq	-240(%rbp), %rax
	movq	72(%rax), %rcx
	movq	%rcx, -256(%rbp)
	testq	%rcx, %rcx
	je	.L1164
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1165
	movdqa	-224(%rbp), %xmm5
	movq	80(%rcx), %r13
	movups	%xmm5, (%rcx)
	testq	%r13, %r13
	je	.L1166
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1167
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r13), %rdi
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm6
	leaq	80(%r13), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 0(%r13)
	cmpq	%rax, %rdi
	je	.L1168
	call	_ZdlPv@PLT
.L1168:
	movq	16(%r13), %rdi
	leaq	32(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1169
	call	_ZdlPv@PLT
.L1169:
	movl	$104, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1166:
	movq	-256(%rbp), %rax
	movq	72(%rax), %r13
	testq	%r13, %r13
	je	.L1170
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1171
	movdqa	-224(%rbp), %xmm7
	movq	80(%r13), %r8
	movups	%xmm7, 0(%r13)
	testq	%r8, %r8
	je	.L1172
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1173
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r8), %rdi
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm5
	leaq	80(%r8), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, (%r8)
	cmpq	%rax, %rdi
	je	.L1174
	movq	%r8, -224(%rbp)
	call	_ZdlPv@PLT
	movq	-224(%rbp), %r8
.L1174:
	movq	16(%r8), %rdi
	leaq	32(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1175
	movq	%r8, -224(%rbp)
	call	_ZdlPv@PLT
	movq	-224(%rbp), %r8
.L1175:
	movl	$104, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1172:
	movq	72(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1176
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1177
	movq	%rdi, -224(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movq	-224(%rbp), %rdi
	movl	$88, %esi
	call	_ZdlPvm@PLT
.L1176:
	movq	64(%r13), %rax
	movq	%rax, -264(%rbp)
	testq	%rax, %rax
	je	.L1178
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -224(%rbp)
	cmpq	%rax, %rcx
	je	.L1187
	movq	%rbx, -280(%rbp)
	movq	%rax, %rbx
	movq	%r12, -272(%rbp)
	jmp	.L1179
.L1514:
	movq	88(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	leaq	104(%r12), %rdx
	movq	%rax, (%r12)
	cmpq	%rdx, %rdi
	je	.L1184
	call	_ZdlPv@PLT
.L1184:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rdx
	cmpq	%rdx, %rdi
	je	.L1185
	call	_ZdlPv@PLT
.L1185:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rdx
	cmpq	%rdx, %rdi
	je	.L1186
	call	_ZdlPv@PLT
.L1186:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1182:
	addq	$8, %rbx
	cmpq	%rbx, -224(%rbp)
	je	.L1513
.L1179:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1182
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1514
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L1182
.L1147:
	movq	%r15, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L1146:
	movq	64(%r14), %rax
	movq	%rax, -224(%rbp)
	testq	%rax, %rax
	je	.L1230
	movq	8(%rax), %rbx
	movq	(%rax), %r15
	cmpq	%r15, %rbx
	jne	.L1237
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1516:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	104(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1234
	call	_ZdlPv@PLT
.L1234:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1235
	call	_ZdlPv@PLT
.L1235:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1236
	call	_ZdlPv@PLT
.L1236:
	movl	$136, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1232:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L1515
.L1237:
	movq	(%r15), %r13
	testq	%r13, %r13
	je	.L1232
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1516
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1232
.L1515:
	movq	-224(%rbp), %rax
	movq	(%rax), %r15
.L1231:
	testq	%r15, %r15
	je	.L1238
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1238:
	movq	-224(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1230:
	movq	24(%r14), %rdi
	leaq	40(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1239
	call	_ZdlPv@PLT
.L1239:
	movl	$88, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1036
.L1523:
	movq	-224(%rbp), %rax
	movq	-240(%rbp), %r12
	movq	-256(%rbp), %rbx
	movq	(%rax), %r13
.L1211:
	testq	%r13, %r13
	je	.L1218
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1218:
	movq	-224(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1210:
	movq	24(%rbx), %rdi
	leaq	40(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1219
	call	_ZdlPv@PLT
.L1219:
	movl	$88, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1152:
	movq	64(%r15), %rax
	movq	%rax, -224(%rbp)
	testq	%rax, %rax
	je	.L1220
	movq	8(%rax), %rbx
	movq	(%rax), %r13
	cmpq	%r13, %rbx
	je	.L1221
	movq	%r12, -240(%rbp)
	movq	%r13, %r12
	jmp	.L1227
	.p2align 4,,10
	.p2align 3
.L1518:
	movq	88(%r13), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	leaq	104(%r13), %rdx
	movq	%rax, 0(%r13)
	cmpq	%rdx, %rdi
	je	.L1224
	call	_ZdlPv@PLT
.L1224:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L1225
	call	_ZdlPv@PLT
.L1225:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L1226
	call	_ZdlPv@PLT
.L1226:
	movl	$136, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1222:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L1517
.L1227:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L1222
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1518
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L1222
.L1517:
	movq	-224(%rbp), %rax
	movq	-240(%rbp), %r12
	movq	(%rax), %r13
.L1221:
	testq	%r13, %r13
	je	.L1228
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1228:
	movq	-224(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1220:
	movq	24(%r15), %rdi
	leaq	40(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1229
	call	_ZdlPv@PLT
.L1229:
	movl	$88, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1146
.L1521:
	movq	-240(%rbp), %rax
	movq	-256(%rbp), %r12
	movq	-264(%rbp), %rbx
	movq	(%rax), %r13
.L1110:
	testq	%r13, %r13
	je	.L1117
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1117:
	movq	-240(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1109:
	movq	24(%rbx), %rdi
	leaq	40(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1118
	call	_ZdlPv@PLT
.L1118:
	movl	$88, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1051:
	movq	64(%r14), %rax
	movq	%rax, -240(%rbp)
	testq	%rax, %rax
	je	.L1119
	movq	8(%rax), %rbx
	movq	(%rax), %r13
	cmpq	%r13, %rbx
	je	.L1120
	movq	%r12, -256(%rbp)
	movq	%r13, %r12
	jmp	.L1126
	.p2align 4,,10
	.p2align 3
.L1520:
	movq	88(%r13), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	leaq	104(%r13), %rdx
	movq	%rax, 0(%r13)
	cmpq	%rdx, %rdi
	je	.L1123
	call	_ZdlPv@PLT
.L1123:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L1124
	call	_ZdlPv@PLT
.L1124:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L1125
	call	_ZdlPv@PLT
.L1125:
	movl	$136, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1121:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L1519
.L1126:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L1121
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1520
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L1121
.L1519:
	movq	-240(%rbp), %rax
	movq	-256(%rbp), %r12
	movq	(%rax), %r13
.L1120:
	testq	%r13, %r13
	je	.L1127
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1127:
	movq	-240(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1119:
	movq	24(%r14), %rdi
	leaq	40(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1128
	call	_ZdlPv@PLT
.L1128:
	movl	$88, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1045
.L1241:
	addl	$1, 8(%r14)
	jmp	.L1242
.L1258:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	jmp	.L1259
.L1266:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1265
.L1505:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L1261
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L1262:
	cmpl	$1, %eax
	jne	.L1036
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L1036
.L1048:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1047
.L1149:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1148
.L1153:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L1152
.L1052:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L1051
.L1528:
	movq	-240(%rbp), %rax
	movq	-256(%rbp), %r15
	movq	-264(%rbp), %r12
	movq	-272(%rbp), %rbx
	movq	(%rax), %r9
.L1100:
	testq	%r9, %r9
	je	.L1107
	movq	%r9, %rdi
	call	_ZdlPv@PLT
.L1107:
	movq	-240(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1099:
	movq	24(%r13), %rdi
	leaq	40(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1108
	call	_ZdlPv@PLT
.L1108:
	movl	$88, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1057:
	movq	64(%rbx), %rax
	movq	%rax, -240(%rbp)
	testq	%rax, %rax
	je	.L1109
	movq	8(%rax), %rcx
	movq	(%rax), %r13
	cmpq	%r13, %rcx
	je	.L1110
	movq	%rbx, -264(%rbp)
	movq	%r13, %rbx
	movq	%rcx, %r13
	movq	%r12, -256(%rbp)
	jmp	.L1116
	.p2align 4,,10
	.p2align 3
.L1522:
	movq	88(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	leaq	104(%r12), %rdx
	movq	%rax, (%r12)
	cmpq	%rdx, %rdi
	je	.L1113
	call	_ZdlPv@PLT
.L1113:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rdx
	cmpq	%rdx, %rdi
	je	.L1114
	call	_ZdlPv@PLT
.L1114:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rdx
	cmpq	%rdx, %rdi
	je	.L1115
	call	_ZdlPv@PLT
.L1115:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1111:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	je	.L1521
.L1116:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1111
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1522
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L1111
.L1526:
	movq	-224(%rbp), %rax
	movq	-256(%rbp), %r12
	movq	-264(%rbp), %rbx
	movq	(%rax), %r13
.L1201:
	testq	%r13, %r13
	je	.L1208
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1208:
	movq	-224(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1200:
	movq	-240(%rbp), %rax
	movq	24(%rax), %rdi
	addq	$40, %rax
	cmpq	%rax, %rdi
	je	.L1209
	call	_ZdlPv@PLT
.L1209:
	movq	-240(%rbp), %rdi
	movl	$88, %esi
	call	_ZdlPvm@PLT
.L1158:
	movq	64(%rbx), %rax
	movq	%rax, -224(%rbp)
	testq	%rax, %rax
	je	.L1210
	movq	8(%rax), %rcx
	movq	(%rax), %r13
	cmpq	%r13, %rcx
	je	.L1211
	movq	%rbx, -256(%rbp)
	movq	%r13, %rbx
	movq	%rcx, %r13
	movq	%r12, -240(%rbp)
	jmp	.L1217
	.p2align 4,,10
	.p2align 3
.L1524:
	movq	88(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	leaq	104(%r12), %rdx
	movq	%rax, (%r12)
	cmpq	%rdx, %rdi
	je	.L1214
	call	_ZdlPv@PLT
.L1214:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rdx
	cmpq	%rdx, %rdi
	je	.L1215
	call	_ZdlPv@PLT
.L1215:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rdx
	cmpq	%rdx, %rdi
	je	.L1216
	call	_ZdlPv@PLT
.L1216:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1212:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	je	.L1523
.L1217:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1212
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1524
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L1212
.L1504:
	movq	%rdx, %xmm0
	movq	%r14, %xmm6
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -208(%rbp)
	testq	%r15, %r15
	je	.L1525
	lock addl	$1, (%rax)
	jmp	.L1243
.L1141:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1036
.L1040:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L1139
.L1252:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1251
.L1530:
	movq	-264(%rbp), %r12
	movq	-272(%rbp), %rbx
.L1198:
	movq	-224(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1192
	call	_ZdlPv@PLT
.L1192:
	movq	-224(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1189:
	movq	-256(%rbp), %rax
	movq	24(%rax), %rdi
	addq	$40, %rax
	cmpq	%rax, %rdi
	je	.L1199
	call	_ZdlPv@PLT
.L1199:
	movq	-256(%rbp), %rdi
	movl	$88, %esi
	call	_ZdlPvm@PLT
.L1164:
	movq	-240(%rbp), %rax
	movq	64(%rax), %rax
	movq	%rax, -224(%rbp)
	testq	%rax, %rax
	je	.L1200
	movq	8(%rax), %rcx
	movq	(%rax), %r13
	cmpq	%r13, %rcx
	je	.L1201
	movq	%r12, -256(%rbp)
	movq	%r13, %r12
	movq	%rbx, -264(%rbp)
	movq	%rcx, %rbx
	jmp	.L1207
.L1527:
	movq	88(%r13), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	leaq	104(%r13), %rdx
	movq	%rax, 0(%r13)
	cmpq	%rdx, %rdi
	je	.L1204
	call	_ZdlPv@PLT
.L1204:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L1205
	call	_ZdlPv@PLT
.L1205:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L1206
	call	_ZdlPv@PLT
.L1206:
	movl	$136, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1202:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L1526
.L1207:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L1202
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1527
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L1202
.L1532:
	movq	-264(%rbp), %r12
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r13
.L1097:
	movq	-256(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1091
	call	_ZdlPv@PLT
.L1091:
	movq	-256(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1088:
	movq	-240(%rbp), %rax
	movq	24(%rax), %rdi
	addq	$40, %rax
	cmpq	%rax, %rdi
	je	.L1098
	call	_ZdlPv@PLT
.L1098:
	movq	-240(%rbp), %rdi
	movl	$88, %esi
	call	_ZdlPvm@PLT
.L1063:
	movq	64(%r13), %rax
	movq	%rax, -240(%rbp)
	testq	%rax, %rax
	je	.L1099
	movq	8(%rax), %rcx
	movq	(%rax), %r9
	cmpq	%r9, %rcx
	je	.L1100
	movq	%r15, -256(%rbp)
	movq	%rcx, %r15
	movq	%rbx, -272(%rbp)
	movq	%r9, %rbx
	movq	%r12, -264(%rbp)
	jmp	.L1106
.L1529:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r12), %rdi
	movq	%rax, (%r12)
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1103
	call	_ZdlPv@PLT
.L1103:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1104
	call	_ZdlPv@PLT
.L1104:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1105
	call	_ZdlPv@PLT
.L1105:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1101:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L1528
.L1106:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1101
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1529
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1101
.L1525:
	addl	$1, 8(%r14)
	jmp	.L1243
.L1143:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1142
.L1042:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1041
.L1246:
	movl	8(%r13), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r13)
	jmp	.L1247
.L1261:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L1262
.L1513:
	movq	-272(%rbp), %r12
	movq	-280(%rbp), %rbx
.L1187:
	movq	-264(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1181
	call	_ZdlPv@PLT
.L1181:
	movq	-264(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1178:
	movq	24(%r13), %rdi
	leaq	40(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1188
	call	_ZdlPv@PLT
.L1188:
	movl	$88, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1170:
	movq	-256(%rbp), %rax
	movq	64(%rax), %rax
	movq	%rax, -224(%rbp)
	testq	%rax, %rax
	je	.L1189
	movq	8(%rax), %rcx
	movq	(%rax), %r13
	cmpq	%r13, %rcx
	je	.L1198
	movq	%r12, -264(%rbp)
	movq	%r13, %r12
	movq	%rbx, -272(%rbp)
	movq	%rcx, %rbx
	jmp	.L1190
.L1531:
	movq	88(%r13), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	leaq	104(%r13), %rdx
	movq	%rax, 0(%r13)
	cmpq	%rdx, %rdi
	je	.L1195
	call	_ZdlPv@PLT
.L1195:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L1196
	call	_ZdlPv@PLT
.L1196:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L1197
	call	_ZdlPv@PLT
.L1197:
	movl	$136, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1193:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L1530
.L1190:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L1193
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1531
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L1193
.L1508:
	movq	-280(%rbp), %r12
	movq	-288(%rbp), %rbx
.L1086:
	movq	-272(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1080
	call	_ZdlPv@PLT
.L1080:
	movq	-272(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1077:
	movq	-256(%rbp), %rax
	movq	24(%rax), %rdi
	addq	$40, %rax
	cmpq	%rax, %rdi
	je	.L1087
	call	_ZdlPv@PLT
.L1087:
	movq	-256(%rbp), %rdi
	movl	$88, %esi
	call	_ZdlPvm@PLT
.L1069:
	movq	-240(%rbp), %rax
	movq	64(%rax), %rax
	movq	%rax, -256(%rbp)
	testq	%rax, %rax
	je	.L1088
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	cmpq	%rax, %rcx
	je	.L1097
	movq	%r12, -264(%rbp)
	movq	%rcx, %r12
	movq	%rbx, -272(%rbp)
	movq	%rax, %rbx
	movq	%r13, -280(%rbp)
	jmp	.L1089
.L1533:
	movq	88(%r13), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	leaq	104(%r13), %rdx
	movq	%rax, 0(%r13)
	cmpq	%rdx, %rdi
	je	.L1094
	call	_ZdlPv@PLT
.L1094:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L1095
	call	_ZdlPv@PLT
.L1095:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L1096
	call	_ZdlPv@PLT
.L1096:
	movl	$136, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1092:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	je	.L1532
.L1089:
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L1092
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1533
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L1092
.L1510:
	movq	0(%r13), %rax
	movq	%rdx, -240(%rbp)
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	movq	-240(%rbp), %rdx
	je	.L1249
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1250:
	cmpl	$1, %eax
	jne	.L1245
	movq	0(%r13), %rax
	movq	%rdx, -240(%rbp)
	movq	%r13, %rdi
	call	*24(%rax)
	movq	-240(%rbp), %rdx
	jmp	.L1245
.L1155:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1154
.L1054:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1053
.L1058:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1057
.L1159:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L1158
.L1249:
	movl	12(%r13), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 12(%r13)
	jmp	.L1250
.L1161:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1160
.L1060:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L1059
.L1165:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L1164
.L1064:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L1063
.L1066:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L1065
.L1167:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1166
.L1171:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1170
.L1070:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L1069
.L1173:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L1172
.L1177:
	call	*%rax
	jmp	.L1176
.L1072:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L1071
.L1076:
	call	*%rax
	jmp	.L1075
.L1507:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7494:
	.size	_ZN12v8_inspector12_GLOBAL__N_126buildInspectorObjectCommonEPNS_10V8DebuggerERKSt6vectorISt10shared_ptrINS_10StackFrameEESaIS6_EERKNS_8String16ERKS4_INS_15AsyncStackTraceEERKNS_14V8StackTraceIdEi, .-_ZN12v8_inspector12_GLOBAL__N_126buildInspectorObjectCommonEPNS_10V8DebuggerERKSt6vectorISt10shared_ptrINS_10StackFrameEESaIS6_EERKNS_8String16ERKS4_INS_15AsyncStackTraceEERKNS_14V8StackTraceIdEi
	.section	.text._ZNK12v8_inspector16V8StackTraceImpl24buildInspectorObjectImplEPNS_10V8DebuggerEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector16V8StackTraceImpl24buildInspectorObjectImplEPNS_10V8DebuggerEi
	.type	_ZNK12v8_inspector16V8StackTraceImpl24buildInspectorObjectImplEPNS_10V8DebuggerEi, @function
_ZNK12v8_inspector16V8StackTraceImpl24buildInspectorObjectImplEPNS_10V8DebuggerEi:
.LFB7591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r10
	leaq	56(%rsi), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	48(%rsi), %rax
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L1540
	leaq	8(%rax), %rdx
	movl	8(%rax), %eax
.L1537:
	testl	%eax, %eax
	je	.L1536
	leal	1(%rax), %edi
	lock cmpxchgl	%edi, (%rdx)
	jne	.L1537
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L1540
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1559
.L1540:
	xorl	%eax, %eax
.L1539:
	subq	$8, %rsp
	leaq	-80(%rbp), %r11
	leaq	8(%rsi), %rdx
	movq	%rax, -96(%rbp)
	pushq	%rcx
	xorl	%eax, %eax
	movq	%r11, %rcx
	movq	%r12, %rdi
	leaq	-64(%rbp), %r13
	leaq	-96(%rbp), %r8
	movq	%r10, %rsi
	movw	%ax, -64(%rbp)
	movq	%r13, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -48(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_126buildInspectorObjectCommonEPNS_10V8DebuggerERKSt6vectorISt10shared_ptrINS_10StackFrameEESaIS6_EERKNS_8String16ERKS4_INS_15AsyncStackTraceEERKNS_14V8StackTraceIdEi
	movq	-80(%rbp), %rdi
	popq	%rdx
	popq	%rcx
	cmpq	%r13, %rdi
	je	.L1541
	call	_ZdlPv@PLT
.L1541:
	movq	-88(%rbp), %r13
	testq	%r13, %r13
	je	.L1534
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1544
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L1560
	.p2align 4,,10
	.p2align 3
.L1534:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1561
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1559:
	.cfi_restore_state
	movq	40(%rsi), %rax
	jmp	.L1539
	.p2align 4,,10
	.p2align 3
.L1544:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L1534
.L1560:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1547
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1548:
	cmpl	$1, %eax
	jne	.L1534
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1534
	.p2align 4,,10
	.p2align 3
.L1536:
	movq	$0, -88(%rbp)
	jmp	.L1540
	.p2align 4,,10
	.p2align 3
.L1547:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1548
.L1561:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7591:
	.size	_ZNK12v8_inspector16V8StackTraceImpl24buildInspectorObjectImplEPNS_10V8DebuggerEi, .-_ZNK12v8_inspector16V8StackTraceImpl24buildInspectorObjectImplEPNS_10V8DebuggerEi
	.section	.text._ZNK12v8_inspector15AsyncStackTrace20buildInspectorObjectEPNS_10V8DebuggerEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector15AsyncStackTrace20buildInspectorObjectEPNS_10V8DebuggerEi
	.type	_ZNK12v8_inspector15AsyncStackTrace20buildInspectorObjectEPNS_10V8DebuggerEi, @function
_ZNK12v8_inspector15AsyncStackTrace20buildInspectorObjectEPNS_10V8DebuggerEi:
.LFB7613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r10
	leaq	104(%rsi), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	96(%rsi), %rax
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L1568
	leaq	8(%rax), %rdx
	movl	8(%rax), %eax
.L1565:
	testl	%eax, %eax
	je	.L1564
	leal	1(%rax), %edi
	lock cmpxchgl	%edi, (%rdx)
	jne	.L1565
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L1568
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1586
.L1568:
	xorl	%eax, %eax
.L1567:
	subq	$8, %rsp
	leaq	24(%rsi), %r11
	leaq	64(%rsi), %rdx
	movq	%r12, %rdi
	pushq	%rcx
	leaq	-64(%rbp), %r8
	movq	%r11, %rcx
	movq	%r10, %rsi
	movq	%rax, -64(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_126buildInspectorObjectCommonEPNS_10V8DebuggerERKSt6vectorISt10shared_ptrINS_10StackFrameEESaIS6_EERKNS_8String16ERKS4_INS_15AsyncStackTraceEERKNS_14V8StackTraceIdEi
	movq	-56(%rbp), %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L1562
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1571
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L1587
	.p2align 4,,10
	.p2align 3
.L1562:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1588
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1586:
	.cfi_restore_state
	movq	88(%rsi), %rax
	jmp	.L1567
	.p2align 4,,10
	.p2align 3
.L1571:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L1562
.L1587:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1574
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1575:
	cmpl	$1, %eax
	jne	.L1562
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1562
	.p2align 4,,10
	.p2align 3
.L1564:
	movq	$0, -56(%rbp)
	jmp	.L1568
	.p2align 4,,10
	.p2align 3
.L1574:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1575
.L1588:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7613:
	.size	_ZNK12v8_inspector15AsyncStackTrace20buildInspectorObjectEPNS_10V8DebuggerEi, .-_ZNK12v8_inspector15AsyncStackTrace20buildInspectorObjectEPNS_10V8DebuggerEi
	.section	.text._ZNK12v8_inspector16V8StackTraceImpl24buildInspectorObjectImplEPNS_10V8DebuggerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector16V8StackTraceImpl24buildInspectorObjectImplEPNS_10V8DebuggerE
	.type	_ZNK12v8_inspector16V8StackTraceImpl24buildInspectorObjectImplEPNS_10V8DebuggerE, @function
_ZNK12v8_inspector16V8StackTraceImpl24buildInspectorObjectImplEPNS_10V8DebuggerE:
.LFB7590:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r10
	leaq	56(%rsi), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -40
	movl	32(%rsi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	48(%rsi), %rax
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L1595
	leaq	8(%rax), %rdx
	movl	8(%rax), %eax
.L1592:
	testl	%eax, %eax
	je	.L1591
	leal	1(%rax), %edi
	lock cmpxchgl	%edi, (%rdx)
	jne	.L1592
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L1595
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1614
.L1595:
	xorl	%eax, %eax
.L1594:
	subq	$8, %rsp
	leaq	-80(%rbp), %r11
	leaq	8(%rsi), %rdx
	movq	%rax, -96(%rbp)
	pushq	%rcx
	xorl	%eax, %eax
	movq	%r11, %rcx
	movq	%r12, %rdi
	leaq	-64(%rbp), %r13
	leaq	-96(%rbp), %r8
	movq	%r10, %rsi
	movw	%ax, -64(%rbp)
	movq	%r13, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -48(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_126buildInspectorObjectCommonEPNS_10V8DebuggerERKSt6vectorISt10shared_ptrINS_10StackFrameEESaIS6_EERKNS_8String16ERKS4_INS_15AsyncStackTraceEERKNS_14V8StackTraceIdEi
	movq	-80(%rbp), %rdi
	popq	%rdx
	popq	%rcx
	cmpq	%r13, %rdi
	je	.L1596
	call	_ZdlPv@PLT
.L1596:
	movq	-88(%rbp), %r13
	testq	%r13, %r13
	je	.L1589
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1599
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L1615
	.p2align 4,,10
	.p2align 3
.L1589:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1616
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1614:
	.cfi_restore_state
	movq	40(%rsi), %rax
	jmp	.L1594
	.p2align 4,,10
	.p2align 3
.L1599:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L1589
.L1615:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1602
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1603:
	cmpl	$1, %eax
	jne	.L1589
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1589
	.p2align 4,,10
	.p2align 3
.L1591:
	movq	$0, -88(%rbp)
	jmp	.L1595
	.p2align 4,,10
	.p2align 3
.L1602:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1603
.L1616:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7590:
	.size	_ZNK12v8_inspector16V8StackTraceImpl24buildInspectorObjectImplEPNS_10V8DebuggerE, .-_ZNK12v8_inspector16V8StackTraceImpl24buildInspectorObjectImplEPNS_10V8DebuggerE
	.section	.text._ZNK12v8_inspector16V8StackTraceImpl20buildInspectorObjectEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector16V8StackTraceImpl20buildInspectorObjectEv
	.type	_ZNK12v8_inspector16V8StackTraceImpl20buildInspectorObjectEv, @function
_ZNK12v8_inspector16V8StackTraceImpl20buildInspectorObjectEv:
.LFB7594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	56(%rsi), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -40
	movl	32(%rsi), %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	48(%rsi), %rax
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L1623
	leaq	8(%rax), %rdx
	movl	8(%rax), %eax
.L1620:
	testl	%eax, %eax
	je	.L1619
	leal	1(%rax), %ecx
	lock cmpxchgl	%ecx, (%rdx)
	jne	.L1620
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L1623
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1646
.L1623:
	xorl	%eax, %eax
.L1622:
	subq	$8, %rsp
	leaq	8(%rsi), %rdx
	movq	%rax, -96(%rbp)
	leaq	-80(%rbp), %rcx
	pushq	%r8
	xorl	%eax, %eax
	leaq	-104(%rbp), %rdi
	leaq	-64(%rbp), %r13
	leaq	-96(%rbp), %r8
	xorl	%esi, %esi
	movq	%r13, -80(%rbp)
	movq	$0, -72(%rbp)
	movw	%ax, -64(%rbp)
	movq	$0, -48(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_126buildInspectorObjectCommonEPNS_10V8DebuggerERKSt6vectorISt10shared_ptrINS_10StackFrameEESaIS6_EERKNS_8String16ERKS4_INS_15AsyncStackTraceEERKNS_14V8StackTraceIdEi
	movq	-80(%rbp), %rdi
	popq	%rdx
	popq	%rcx
	cmpq	%r13, %rdi
	je	.L1624
	call	_ZdlPv@PLT
.L1624:
	movq	-88(%rbp), %r13
	testq	%r13, %r13
	je	.L1626
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1627
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L1647
	.p2align 4,,10
	.p2align 3
.L1626:
	movq	-104(%rbp), %rax
	testq	%rax, %rax
	leaq	8(%rax), %rdx
	cmovne	%rdx, %rax
	movq	%rax, (%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1648
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1646:
	.cfi_restore_state
	movq	40(%rsi), %rax
	jmp	.L1622
	.p2align 4,,10
	.p2align 3
.L1627:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L1626
.L1647:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1630
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1631:
	cmpl	$1, %eax
	jne	.L1626
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1626
	.p2align 4,,10
	.p2align 3
.L1619:
	movq	$0, -88(%rbp)
	jmp	.L1623
	.p2align 4,,10
	.p2align 3
.L1630:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1631
.L1648:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7594:
	.size	_ZNK12v8_inspector16V8StackTraceImpl20buildInspectorObjectEv, .-_ZNK12v8_inspector16V8StackTraceImpl20buildInspectorObjectEv
	.section	.text._ZNK12v8_inspector16V8StackTraceImpl20buildInspectorObjectEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector16V8StackTraceImpl20buildInspectorObjectEi
	.type	_ZNK12v8_inspector16V8StackTraceImpl20buildInspectorObjectEi, @function
_ZNK12v8_inspector16V8StackTraceImpl20buildInspectorObjectEi:
.LFB7595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	56(%rsi), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	48(%rsi), %rax
	cmpl	%edx, 32(%rsi)
	cmovle	32(%rsi), %edx
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L1655
	leaq	8(%rax), %rcx
	movl	8(%rax), %eax
.L1652:
	testl	%eax, %eax
	je	.L1651
	leal	1(%rax), %edi
	lock cmpxchgl	%edi, (%rcx)
	jne	.L1652
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L1655
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1678
.L1655:
	xorl	%eax, %eax
.L1654:
	subq	$8, %rsp
	leaq	8(%rsi), %r10
	movq	%rax, -96(%rbp)
	leaq	-80(%rbp), %rcx
	pushq	%rdx
	xorl	%eax, %eax
	leaq	-104(%rbp), %rdi
	movq	%r10, %rdx
	leaq	-64(%rbp), %r13
	leaq	-96(%rbp), %r8
	xorl	%esi, %esi
	movw	%ax, -64(%rbp)
	movq	%r13, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -48(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_126buildInspectorObjectCommonEPNS_10V8DebuggerERKSt6vectorISt10shared_ptrINS_10StackFrameEESaIS6_EERKNS_8String16ERKS4_INS_15AsyncStackTraceEERKNS_14V8StackTraceIdEi
	movq	-80(%rbp), %rdi
	popq	%rdx
	popq	%rcx
	cmpq	%r13, %rdi
	je	.L1656
	call	_ZdlPv@PLT
.L1656:
	movq	-88(%rbp), %r13
	testq	%r13, %r13
	je	.L1658
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1659
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L1679
	.p2align 4,,10
	.p2align 3
.L1658:
	movq	-104(%rbp), %rax
	testq	%rax, %rax
	leaq	8(%rax), %rdx
	cmovne	%rdx, %rax
	movq	%rax, (%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1680
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1678:
	.cfi_restore_state
	movq	40(%rsi), %rax
	jmp	.L1654
	.p2align 4,,10
	.p2align 3
.L1659:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L1658
.L1679:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1662
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1663:
	cmpl	$1, %eax
	jne	.L1658
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1658
	.p2align 4,,10
	.p2align 3
.L1651:
	movq	$0, -88(%rbp)
	jmp	.L1655
	.p2align 4,,10
	.p2align 3
.L1662:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1663
.L1680:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7595:
	.size	_ZNK12v8_inspector16V8StackTraceImpl20buildInspectorObjectEi, .-_ZNK12v8_inspector16V8StackTraceImpl20buildInspectorObjectEi
	.section	.rodata._ZNK12v8_inspector16V8StackTraceImpl8toStringEv.str1.1,"aMS",@progbits,1
.LC5:
	.string	"(anonymous function)"
.LC6:
	.string	"\n    at "
.LC7:
	.string	" ("
	.section	.text._ZNK12v8_inspector16V8StackTraceImpl8toStringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector16V8StackTraceImpl8toStringEv
	.type	_ZNK12v8_inspector16V8StackTraceImpl8toStringEv, @function
_ZNK12v8_inspector16V8StackTraceImpl8toStringEv:
.LFB7596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-256(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$312, %rsp
	movq	%rdi, -344(%rbp)
	movq	%r12, %rdi
	movq	%rsi, -312(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	movq	8(%rbx), %rax
	cmpq	%rax, 16(%rbx)
	je	.L1715
	leaq	-192(%rbp), %rcx
	leaq	-96(%rbp), %r13
	movq	$0, -280(%rbp)
	movq	%rcx, -328(%rbp)
	leaq	-176(%rbp), %rcx
	leaq	-80(%rbp), %r14
	movq	%rcx, -304(%rbp)
	leaq	-208(%rbp), %rcx
	leaq	-224(%rbp), %r15
	movq	%rcx, -288(%rbp)
	leaq	-144(%rbp), %rcx
	movq	%rcx, -296(%rbp)
	leaq	-128(%rbp), %rcx
	movq	%rcx, -320(%rbp)
	jmp	.L1682
	.p2align 4,,10
	.p2align 3
.L1717:
	movq	-304(%rbp), %rax
	movq	-328(%rbp), %rdi
	movq	%rax, -192(%rbp)
	movq	8(%rbx), %rax
	movq	(%rbx), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	32(%rbx), %rax
	movq	%rax, -160(%rbp)
.L1686:
	leaq	.LC6(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-288(%rbp), %rax
	movq	-96(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -224(%rbp)
	movq	-88(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-184(%rbp), %r8
	movl	$7, %edx
	movq	-216(%rbp), %rsi
	movq	-224(%rbp), %rax
	cmpq	-288(%rbp), %rax
	cmovne	-208(%rbp), %rdx
	leaq	(%r8,%rsi), %r9
	movq	-192(%rbp), %rcx
	cmpq	%rdx, %r9
	ja	.L1688
	testq	%r8, %r8
	je	.L1689
	leaq	(%rax,%rsi,2), %rdi
	cmpq	$1, %r8
	je	.L1716
	addq	%r8, %r8
	je	.L1689
	movq	%r8, %rdx
	movq	%rcx, %rsi
	movq	%r9, -336(%rbp)
	call	memmove@PLT
	movq	-224(%rbp), %rax
	movq	-336(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L1689:
	xorl	%edx, %edx
	movq	%r9, -216(%rbp)
	movq	-296(%rbp), %rdi
	movq	%r15, %rsi
	movw	%dx, (%rax,%r9,2)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-224(%rbp), %rdi
	cmpq	-288(%rbp), %rdi
	je	.L1691
	call	_ZdlPv@PLT
.L1691:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1692
	call	_ZdlPv@PLT
.L1692:
	movq	-296(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-144(%rbp), %rdi
	cmpq	-320(%rbp), %rdi
	je	.L1693
	call	_ZdlPv@PLT
.L1693:
	movq	-192(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L1694
	call	_ZdlPv@PLT
.L1694:
	leaq	.LC7(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1695
	call	_ZdlPv@PLT
.L1695:
	leaq	80(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movl	$58, %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movl	120(%rbx), %eax
	movq	%r13, %rdi
	leal	1(%rax), %esi
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1696
	call	_ZdlPv@PLT
.L1696:
	movl	$58, %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movl	124(%rbx), %esi
	movq	%r13, %rdi
	addl	$1, %esi
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1697
	call	_ZdlPv@PLT
.L1697:
	movl	$41, %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movq	-312(%rbp), %rbx
	addq	$1, -280(%rbp)
	movq	-280(%rbp), %rcx
	movq	8(%rbx), %rax
	movq	16(%rbx), %rbx
	movq	%rbx, %rdx
	movq	%rbx, -336(%rbp)
	subq	%rax, %rdx
	sarq	$4, %rdx
	cmpq	%rcx, %rdx
	jbe	.L1699
.L1682:
	movq	-280(%rbp), %rdx
	salq	$4, %rdx
	movq	(%rax,%rdx), %rbx
	cmpq	$0, 8(%rbx)
	jne	.L1717
	movq	-328(%rbp), %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	jmp	.L1686
	.p2align 4,,10
	.p2align 3
.L1688:
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r9, -336(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-224(%rbp), %rax
	movq	-336(%rbp), %r9
	jmp	.L1689
	.p2align 4,,10
	.p2align 3
.L1715:
	leaq	-96(%rbp), %r13
	leaq	-80(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L1699:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	leaq	-264(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector16StringBufferImpl5adoptERNS_8String16E@PLT
	movq	-264(%rbp), %rax
	movq	-344(%rbp), %rcx
	movq	-96(%rbp), %rdi
	movq	%rax, (%rcx)
	cmpq	%r14, %rdi
	je	.L1684
	call	_ZdlPv@PLT
.L1684:
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1681
	call	_ZdlPv@PLT
.L1681:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1718
	movq	-344(%rbp), %rax
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1716:
	.cfi_restore_state
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	movq	-224(%rbp), %rax
	jmp	.L1689
.L1718:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7596:
	.size	_ZNK12v8_inspector16V8StackTraceImpl8toStringEv, .-_ZNK12v8_inspector16V8StackTraceImpl8toStringEv
	.section	.text._ZN12v8_inspector15AsyncStackTrace7captureEPNS_10V8DebuggerEiRKNS_8String16Ei,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15AsyncStackTrace7captureEPNS_10V8DebuggerEiRKNS_8String16Ei
	.type	_ZN12v8_inspector15AsyncStackTrace7captureEPNS_10V8DebuggerEiRKNS_8String16Ei, @function
_ZN12v8_inspector15AsyncStackTrace7captureEPNS_10V8DebuggerEiRKNS_8String16Ei:
.LFB7609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$136, %rsp
	movl	%edx, -136(%rbp)
	movq	16(%rsi), %r14
	movl	%r8d, -160(%rbp)
	movq	%r14, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r14, %rdi
	call	_ZN2v87Isolate9InContextEv@PLT
	movl	-160(%rbp), %ecx
	testb	%al, %al
	jne	.L1772
	movl	-136(%rbp), %esi
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	leaq	-80(%rbp), %rcx
	leaq	-128(%rbp), %rdx
	movaps	%xmm0, -128(%rbp)
	xorl	%r14d, %r14d
	movq	$0, -80(%rbp)
	movups	%xmm0, -72(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_119calculateAsyncChainEPNS_10V8DebuggerEiPSt10shared_ptrINS_15AsyncStackTraceEEPNS_14V8StackTraceIdEPi.constprop.0
	movq	$0, -144(%rbp)
.L1755:
	movq	-128(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1773
	movq	32(%rdx), %r8
	movq	8(%rbx), %r10
	movq	(%rbx), %rsi
	movq	24(%rdx), %rdi
	cmpq	%r10, %r8
	movq	%r10, %rcx
	cmovbe	%r8, %rcx
	testq	%rcx, %rcx
	je	.L1726
	xorl	%eax, %eax
	jmp	.L1727
	.p2align 4,,10
	.p2align 3
.L1774:
	addq	$1, %rax
	cmpq	%rax, %rcx
	je	.L1726
.L1727:
	movzwl	(%rsi,%rax,2), %r9d
	cmpw	%r9w, (%rdi,%rax,2)
	je	.L1774
.L1725:
	testq	%r10, %r10
	je	.L1728
	movq	%r14, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L1729:
	movl	-136(%rbp), %eax
	testl	%eax, %eax
	jne	.L1731
	movl	(%rdx), %eax
	movl	%eax, -136(%rbp)
.L1731:
	movq	-120(%rbp), %r13
	testq	%r13, %r13
	je	.L1732
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1733
	lock addl	$1, 8(%r13)
.L1732:
	movq	%rdx, %xmm2
	movq	%r14, %xmm1
	movl	$128, %edi
	movq	-144(%rbp), %xmm0
	movhps	-160(%rbp), %xmm1
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_Znwm@PLT
	movq	(%rbx), %rsi
	movq	%rax, %r14
	movl	-136(%rbp), %eax
	movq	$0, 8(%r14)
	leaq	24(%r14), %rdi
	movl	%eax, (%r14)
	leaq	40(%r14), %rax
	movq	%rax, 24(%r14)
	movq	8(%rbx), %rax
	movq	$0, 16(%r14)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	32(%rbx), %rax
	movq	%r13, 96(%r14)
	movdqa	-176(%rbp), %xmm1
	movdqa	-160(%rbp), %xmm0
	movq	%rax, 56(%r14)
	movups	%xmm1, 64(%r14)
	movups	%xmm0, 80(%r14)
	testq	%r13, %r13
	je	.L1734
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	leaq	12(%r13), %rax
	movq	%rax, -136(%rbp)
	testq	%rbx, %rbx
	je	.L1735
	lock addl	$1, (%rax)
.L1736:
	movq	-64(%rbp), %rax
	movdqa	-80(%rbp), %xmm3
	movq	%r14, (%r12)
	movl	$24, %edi
	movq	$0, 8(%r12)
	movq	%rax, 120(%r14)
	movups	%xmm3, 104(%r14)
	call	_Znwm@PLT
	movabsq	$4294967297, %rsi
	movq	%rsi, 8(%rax)
	leaq	16+_ZTVSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rsi
	movq	%rsi, (%rax)
	movq	%r14, 16(%rax)
	movq	%rax, 8(%r12)
	testq	%rbx, %rbx
	je	.L1775
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L1776
.L1739:
	xorl	%r14d, %r14d
.L1724:
	movq	-120(%rbp), %r13
	testq	%r13, %r13
	je	.L1752
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1743
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L1777
.L1752:
	testq	%r14, %r14
	je	.L1747
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1747:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1778
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1772:
	.cfi_restore_state
	movl	%ecx, %esi
	movq	%r14, %rdi
	movl	$383, %edx
	call	_ZN2v810StackTrace17CurrentStackTraceEPNS_7IsolateEiNS0_17StackTraceOptionsE@PLT
	leaq	-80(%rbp), %r11
	movl	-160(%rbp), %ecx
	movq	%r13, %rsi
	movq	%rax, %rdx
	movq	%r11, %rdi
	movq	%r11, -176(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_114toFramesVectorEPNS_10V8DebuggerEN2v85LocalINS3_10StackTraceEEEi
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %rax
	xorl	%r8d, %r8d
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %rdx
	movq	%r13, %rdi
	movq	-80(%rbp), %r14
	movq	-176(%rbp), %r11
	movq	%rsi, -144(%rbp)
	movl	-136(%rbp), %esi
	movq	%rax, -160(%rbp)
	movq	%r11, %rcx
	movq	$0, -80(%rbp)
	movaps	%xmm0, -128(%rbp)
	movups	%xmm0, -72(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_119calculateAsyncChainEPNS_10V8DebuggerEiPSt10shared_ptrINS_15AsyncStackTraceEEPNS_14V8StackTraceIdEPi.constprop.0
	cmpq	-160(%rbp), %r14
	je	.L1755
	movq	-128(%rbp), %rdx
	testq	%rdx, %rdx
	jne	.L1729
	xorl	%edx, %edx
	jmp	.L1731
	.p2align 4,,10
	.p2align 3
.L1735:
	addl	$1, 12(%r13)
	jmp	.L1736
	.p2align 4,,10
	.p2align 3
.L1733:
	addl	$1, 8(%r13)
	jmp	.L1732
	.p2align 4,,10
	.p2align 3
.L1743:
	movl	8(%r13), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r13)
	cmpl	$1, %eax
	jne	.L1752
.L1777:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1745
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1746:
	cmpl	$1, %eax
	jne	.L1752
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1752
	.p2align 4,,10
	.p2align 3
.L1734:
	movq	-64(%rbp), %rax
	movdqa	-80(%rbp), %xmm4
	movq	%r14, (%r12)
	movl	$24, %edi
	movq	$0, 8(%r12)
	movabsq	$4294967297, %rbx
	movq	%rax, 120(%r14)
	movups	%xmm4, 104(%r14)
	call	_Znwm@PLT
	movq	%rbx, 8(%rax)
	leaq	16+_ZTVSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rbx
	movq	%rbx, (%rax)
	movq	%r14, 16(%rax)
	movq	%rax, 8(%r12)
	jmp	.L1739
	.p2align 4,,10
	.p2align 3
.L1775:
	movl	8(%r13), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r13)
	cmpl	$1, %eax
	jne	.L1739
.L1776:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1740
	movq	-136(%rbp), %rbx
	movl	$-1, %eax
	lock xaddl	%eax, (%rbx)
.L1741:
	cmpl	$1, %eax
	jne	.L1739
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1739
	.p2align 4,,10
	.p2align 3
.L1726:
	subq	%r10, %r8
	cmpq	$2147483647, %r8
	jg	.L1725
	cmpq	$-2147483648, %r8
	jl	.L1725
	testl	%r8d, %r8d
	jne	.L1725
.L1728:
	movq	-120(%rbp), %rax
	movq	%rdx, (%r12)
	movq	%rax, 8(%r12)
	jmp	.L1752
	.p2align 4,,10
	.p2align 3
.L1773:
	cmpq	$0, -80(%rbp)
	jne	.L1757
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%r12)
	jmp	.L1724
	.p2align 4,,10
	.p2align 3
.L1745:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1746
	.p2align 4,,10
	.p2align 3
.L1740:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1741
	.p2align 4,,10
	.p2align 3
.L1757:
	movq	%r14, -160(%rbp)
	xorl	%edx, %edx
	jmp	.L1731
.L1778:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7609:
	.size	_ZN12v8_inspector15AsyncStackTrace7captureEPNS_10V8DebuggerEiRKNS_8String16Ei, .-_ZN12v8_inspector15AsyncStackTrace7captureEPNS_10V8DebuggerEiRKNS_8String16Ei
	.weak	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime9CallFrameE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime9CallFrameE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE, 48
_ZTVN12v8_inspector8protocol7Runtime9CallFrameE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.weak	_ZTVN12v8_inspector16V8StackTraceImplE
	.section	.data.rel.ro.local._ZTVN12v8_inspector16V8StackTraceImplE,"awG",@progbits,_ZTVN12v8_inspector16V8StackTraceImplE,comdat
	.align 8
	.type	_ZTVN12v8_inspector16V8StackTraceImplE, @object
	.size	_ZTVN12v8_inspector16V8StackTraceImplE, 120
_ZTVN12v8_inspector16V8StackTraceImplE:
	.quad	0
	.quad	0
	.quad	_ZNK12v8_inspector16V8StackTraceImpl22firstNonEmptySourceURLEv
	.quad	_ZNK12v8_inspector16V8StackTraceImpl7isEmptyEv
	.quad	_ZNK12v8_inspector16V8StackTraceImpl12topSourceURLEv
	.quad	_ZNK12v8_inspector16V8StackTraceImpl13topLineNumberEv
	.quad	_ZNK12v8_inspector16V8StackTraceImpl15topColumnNumberEv
	.quad	_ZNK12v8_inspector16V8StackTraceImpl11topScriptIdEv
	.quad	_ZNK12v8_inspector16V8StackTraceImpl15topFunctionNameEv
	.quad	_ZN12v8_inspector16V8StackTraceImplD1Ev
	.quad	_ZN12v8_inspector16V8StackTraceImplD0Ev
	.quad	_ZNK12v8_inspector16V8StackTraceImpl20buildInspectorObjectEv
	.quad	_ZNK12v8_inspector16V8StackTraceImpl20buildInspectorObjectEi
	.quad	_ZNK12v8_inspector16V8StackTraceImpl8toStringEv
	.quad	_ZN12v8_inspector16V8StackTraceImpl5cloneEv
	.weak	_ZTVSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15AsyncStackTraceELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.globl	_ZN12v8_inspector16V8StackTraceImpl25maxCallStackSizeToCaptureE
	.section	.data._ZN12v8_inspector16V8StackTraceImpl25maxCallStackSizeToCaptureE,"aw"
	.align 4
	.type	_ZN12v8_inspector16V8StackTraceImpl25maxCallStackSizeToCaptureE, @object
	.size	_ZN12v8_inspector16V8StackTraceImpl25maxCallStackSizeToCaptureE, 4
_ZN12v8_inspector16V8StackTraceImpl25maxCallStackSizeToCaptureE:
	.long	200
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
