	.file	"modules.cc"
	.text
	.section	.text._ZNK2v88internal26SourceTextModuleDescriptor20AstRawStringComparerclEPKNS0_12AstRawStringES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal26SourceTextModuleDescriptor20AstRawStringComparerclEPKNS0_12AstRawStringES5_
	.type	_ZNK2v88internal26SourceTextModuleDescriptor20AstRawStringComparerclEPKNS0_12AstRawStringES5_, @function
_ZNK2v88internal26SourceTextModuleDescriptor20AstRawStringComparerclEPKNS0_12AstRawStringES5_:
.LFB19293:
	.cfi_startproc
	endbr64
	cmpq	%rdx, %rsi
	je	.L4
	movzbl	28(%rsi), %eax
	cmpb	28(%rdx), %al
	je	.L9
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	16(%rsi), %rax
	cmpl	16(%rdx), %eax
	je	.L3
	setl	%al
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rdx), %r9
	movslq	%eax, %r8
	movq	8(%rsi), %rdi
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	memcmp@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	shrl	$31, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE19293:
	.size	_ZNK2v88internal26SourceTextModuleDescriptor20AstRawStringComparerclEPKNS0_12AstRawStringES5_, .-_ZNK2v88internal26SourceTextModuleDescriptor20AstRawStringComparerclEPKNS0_12AstRawStringES5_
	.section	.text._ZN2v88internal26SourceTextModuleDescriptor9AddExportEPKNS0_12AstRawStringES4_NS0_7Scanner8LocationEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26SourceTextModuleDescriptor9AddExportEPKNS0_12AstRawStringES4_NS0_7Scanner8LocationEPNS0_4ZoneE
	.type	_ZN2v88internal26SourceTextModuleDescriptor9AddExportEPKNS0_12AstRawStringES4_NS0_7Scanner8LocationEPNS0_4ZoneE, @function
_ZN2v88internal26SourceTextModuleDescriptor9AddExportEPKNS0_12AstRawStringES4_NS0_7Scanner8LocationEPNS0_4ZoneE:
.LFB19297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	16(%r8), %r12
	movq	24(%r8), %rax
	movq	%rdx, -56(%rbp)
	subq	%r12, %rax
	cmpq	$39, %rax
	jbe	.L31
	leaq	40(%r12), %rax
	movq	%rdx, %xmm0
	movq	%rax, 16(%r8)
.L12:
	movq	%rbx, %xmm1
	movl	$4294967295, %eax
	movq	%rcx, (%r12)
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 32(%r12)
	movq	$0, 24(%r12)
	movups	%xmm0, 8(%r12)
	movq	120(%r13), %rdi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	$47, %rax
	jbe	.L32
	leaq	48(%r15), %rax
	movq	%rax, 16(%rdi)
.L14:
	movq	%r12, %xmm2
	movq	%rbx, %xmm0
	leaq	136(%r13), %rcx
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 32(%r15)
	movq	144(%r13), %r12
	testq	%r12, %r12
	jne	.L16
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L35:
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L17
.L36:
	movq	%rax, %r12
.L16:
	movq	32(%r12), %r14
	cmpq	%r14, %rbx
	je	.L18
	movzbl	28(%rbx), %eax
	cmpb	28(%r14), %al
	je	.L34
.L19:
	testb	%al, %al
	jne	.L35
.L18:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L36
.L17:
	movl	$1, %edi
	cmpq	%rcx, %r12
	je	.L15
	cmpq	%r14, %rbx
	je	.L26
	movzbl	28(%rbx), %edi
	cmpb	28(%r14), %dil
	je	.L37
.L22:
	movzbl	%dil, %edi
.L15:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 168(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movq	16(%rbx), %rdx
	cmpl	16(%r14), %edx
	je	.L20
	setl	%al
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L20:
	movq	8(%r14), %rsi
	movq	8(%rbx), %rdi
	movslq	%edx, %rdx
	movq	%rcx, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %rcx
	shrl	$31, %eax
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%rcx, %r12
	movl	$1, %edi
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$40, %esi
	movq	%r8, %rdi
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %xmm0
	movq	%rax, %r12
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L37:
	movq	16(%rbx), %rdx
	cmpl	16(%r14), %edx
	je	.L23
	setl	%dil
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L26:
	xorl	%edi, %edi
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L23:
	movq	8(%rbx), %rdi
	movq	8(%r14), %rsi
	movslq	%edx, %rdx
	movq	%rcx, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %rcx
	shrl	$31, %eax
	movl	%eax, %edi
	jmp	.L22
	.cfi_endproc
.LFE19297:
	.size	_ZN2v88internal26SourceTextModuleDescriptor9AddExportEPKNS0_12AstRawStringES4_NS0_7Scanner8LocationEPNS0_4ZoneE, .-_ZN2v88internal26SourceTextModuleDescriptor9AddExportEPKNS0_12AstRawStringES4_NS0_7Scanner8LocationEPNS0_4ZoneE
	.section	.text._ZNK2v88internal26SourceTextModuleDescriptor5Entry9SerializeEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal26SourceTextModuleDescriptor5Entry9SerializeEPNS0_7IsolateE
	.type	_ZNK2v88internal26SourceTextModuleDescriptor5Entry9SerializeEPNS0_7IsolateE, @function
_ZNK2v88internal26SourceTextModuleDescriptor5Entry9SerializeEPNS0_7IsolateE:
.LFB19301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	leaq	88(%rsi), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	24(%rdi), %rax
	movl	32(%rdi), %r8d
	movl	4(%rdi), %ebx
	movl	(%rdi), %r11d
	movl	36(%rdi), %r9d
	testq	%rax, %rax
	je	.L40
	movq	(%rax), %rcx
.L40:
	movq	16(%rdi), %rax
	leaq	88(%r10), %rdx
	testq	%rax, %rax
	je	.L42
	movq	(%rax), %rdx
.L42:
	movq	8(%rdi), %rax
	leaq	88(%r10), %rsi
	testq	%rax, %rax
	je	.L44
	movq	(%rax), %rsi
.L44:
	pushq	%rbx
	movq	%r10, %rdi
	pushq	%r11
	call	_ZN2v88internal25SourceTextModuleInfoEntry3NewEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEES6_S6_iiii@PLT
	movq	-8(%rbp), %rbx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19301:
	.size	_ZNK2v88internal26SourceTextModuleDescriptor5Entry9SerializeEPNS0_7IsolateE, .-_ZNK2v88internal26SourceTextModuleDescriptor5Entry9SerializeEPNS0_7IsolateE
	.section	.text._ZN2v88internal26SourceTextModuleDescriptor16GetCellIndexKindEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26SourceTextModuleDescriptor16GetCellIndexKindEi
	.type	_ZN2v88internal26SourceTextModuleDescriptor16GetCellIndexKindEi, @function
_ZN2v88internal26SourceTextModuleDescriptor16GetCellIndexKindEi:
.LFB19307:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	testl	%edi, %edi
	jg	.L46
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
.L46:
	ret
	.cfi_endproc
.LFE19307:
	.size	_ZN2v88internal26SourceTextModuleDescriptor16GetCellIndexKindEi, .-_ZN2v88internal26SourceTextModuleDescriptor16GetCellIndexKindEi
	.section	.text._ZN2v88internal26SourceTextModuleDescriptor17AssignCellIndicesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26SourceTextModuleDescriptor17AssignCellIndicesEv
	.type	_ZN2v88internal26SourceTextModuleDescriptor17AssignCellIndicesEv, @function
_ZN2v88internal26SourceTextModuleDescriptor17AssignCellIndicesEv:
.LFB19308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	136(%r14), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	152(%rdi), %rdi
	cmpq	%r13, %rdi
	je	.L51
	movl	$1, %ebx
	.p2align 4,,10
	.p2align 3
.L56:
	movq	32(%rdi), %r12
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L62:
	cmpq	%r12, 32(%rax)
	jne	.L61
.L52:
	movq	40(%rdi), %rax
	movl	%ebx, 36(%rax)
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdi
	cmpq	%r13, %rax
	jne	.L62
.L51:
	movq	208(%r14), %rdi
	addq	$192, %r14
	cmpq	%rdi, %r14
	je	.L50
	movl	$-1, %ebx
	.p2align 4,,10
	.p2align 3
.L55:
	movq	40(%rdi), %rax
	movl	%ebx, 36(%rax)
	subl	$1, %ebx
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdi
	cmpq	%rax, %r14
	jne	.L55
.L50:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	addl	$1, %ebx
	jmp	.L56
	.cfi_endproc
.LFE19308:
	.size	_ZN2v88internal26SourceTextModuleDescriptor17AssignCellIndicesEv, .-_ZN2v88internal26SourceTextModuleDescriptor17AssignCellIndicesEv
	.section	.rodata._ZNSt6vectorIPKN2v88internal26SourceTextModuleDescriptor5EntryENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPKN2v88internal26SourceTextModuleDescriptor5EntryENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPKN2v88internal26SourceTextModuleDescriptor5EntryENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPKN2v88internal26SourceTextModuleDescriptor5EntryENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	.type	_ZNSt6vectorIPKN2v88internal26SourceTextModuleDescriptor5EntryENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_, @function
_ZNSt6vectorIPKN2v88internal26SourceTextModuleDescriptor5EntryENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_:
.LFB21671:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L101
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L79
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L102
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L65:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L103
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L68:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L102:
	testq	%rdx, %rdx
	jne	.L104
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L66:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L69
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L82
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L82
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L71:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L71
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L73
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L73:
	leaq	16(%rax,%r8), %rcx
.L69:
	cmpq	%r14, %r12
	je	.L74
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L83
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L83
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L76:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L76
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L78
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L78:
	leaq	8(%rcx,%r9), %rcx
.L74:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L83:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L75:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L75
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L82:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L70:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L70
	jmp	.L73
.L103:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L68
.L101:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L104:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L65
	.cfi_endproc
.LFE21671:
	.size	_ZNSt6vectorIPKN2v88internal26SourceTextModuleDescriptor5EntryENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_, .-_ZNSt6vectorIPKN2v88internal26SourceTextModuleDescriptor5EntryENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	.section	.text._ZN2v88internal26SourceTextModuleDescriptor27MakeIndirectExportsExplicitEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26SourceTextModuleDescriptor27MakeIndirectExportsExplicitEPNS0_4ZoneE
	.type	_ZN2v88internal26SourceTextModuleDescriptor27MakeIndirectExportsExplicitEPNS0_4ZoneE, @function
_ZN2v88internal26SourceTextModuleDescriptor27MakeIndirectExportsExplicitEPNS0_4ZoneE:
.LFB19306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	136(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	152(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r13, %r15
	je	.L105
	leaq	192(%rdi), %rax
	movq	%rdi, %r12
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L107:
	movq	200(%r12), %rbx
	movq	40(%r15), %r8
	testq	%rbx, %rbx
	je	.L108
	movq	16(%r8), %r14
	movq	-72(%rbp), %rcx
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L114:
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L110
.L109:
	movq	32(%rbx), %rsi
	cmpq	%rsi, %r14
	je	.L111
	movzbl	28(%rsi), %eax
	cmpb	28(%r14), %al
	je	.L129
.L112:
	testb	%al, %al
	jne	.L114
.L111:
	movq	%rbx, %rcx
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L109
.L110:
	cmpq	%rcx, -72(%rbp)
	je	.L108
	movq	32(%rcx), %rdx
	cmpq	%r14, %rdx
	je	.L116
	movzbl	28(%r14), %eax
	cmpb	28(%rdx), %al
	je	.L130
.L117:
	testb	%al, %al
	je	.L116
.L108:
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
.L120:
	cmpq	%r13, %r15
	jne	.L107
.L105:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L131
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movq	16(%rsi), %rdx
	cmpl	16(%r14), %edx
	je	.L113
	setl	%al
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L113:
	movq	8(%r14), %r10
	movq	8(%rsi), %rdi
	movslq	%edx, %rdx
	movq	%rcx, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%r10, %rsi
	call	memcmp@PLT
	movq	-88(%rbp), %rcx
	movq	-80(%rbp), %r8
	shrl	$31, %eax
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L116:
	movq	40(%rcx), %rax
	movq	24(%rax), %rax
	movq	%rax, 24(%r8)
	movq	40(%rcx), %rax
	movl	32(%rax), %eax
	movl	%eax, 32(%r8)
	movq	40(%rcx), %rax
	movq	(%rax), %rax
	movq	$0, 16(%r8)
	movq	%rax, (%r8)
	movq	72(%r12), %rsi
	movq	%r8, -64(%rbp)
	cmpq	80(%r12), %rsi
	je	.L132
	movq	%r8, (%rsi)
	addq	$8, 72(%r12)
.L119:
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%rax, %rbx
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	%rbx, %r15
	subq	$1, 168(%r12)
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L130:
	movq	16(%r14), %rax
	cmpl	16(%rdx), %eax
	je	.L118
	setl	%al
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L132:
	leaq	-64(%rbp), %rdx
	leaq	56(%r12), %rdi
	call	_ZNSt6vectorIPKN2v88internal26SourceTextModuleDescriptor5EntryENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L118:
	movq	8(%rdx), %rsi
	movslq	%eax, %r10
	movq	8(%r14), %rdi
	movq	%rcx, -88(%rbp)
	movq	%r10, %rdx
	movq	%r8, -80(%rbp)
	call	memcmp@PLT
	movq	-88(%rbp), %rcx
	movq	-80(%rbp), %r8
	shrl	$31, %eax
	jmp	.L117
.L131:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19306:
	.size	_ZN2v88internal26SourceTextModuleDescriptor27MakeIndirectExportsExplicitEPNS0_4ZoneE, .-_ZN2v88internal26SourceTextModuleDescriptor27MakeIndirectExportsExplicitEPNS0_4ZoneE
	.section	.rodata._ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC1:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	.type	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm, @function
_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm:
.LFB22635:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L173
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$268435455, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$8, %rsp
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	movq	%rcx, %r13
	subq	8(%rdi), %r13
	subq	%rcx, %rax
	movq	%r13, %r14
	sarq	$3, %rax
	sarq	$3, %r14
	subq	%r14, %rsi
	cmpq	%rbx, %rax
	jb	.L135
	cmpq	$1, %rbx
	je	.L155
	leaq	-2(%rbx), %rdx
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	addq	$1, %rdx
	.p2align 4,,10
	.p2align 3
.L137:
	movq	%rax, %rsi
	addq	$1, %rax
	salq	$4, %rsi
	movups	%xmm0, (%rcx,%rsi)
	cmpq	%rax, %rdx
	ja	.L137
	leaq	(%rdx,%rdx), %rax
	salq	$4, %rdx
	addq	%rcx, %rdx
	cmpq	%rax, %rbx
	je	.L138
.L136:
	movq	$0, (%rdx)
.L138:
	leaq	(%rcx,%rbx,8), %rax
	movq	%rax, 16(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rsi
	jb	.L176
	cmpq	%r14, %rbx
	movq	%r14, %r15
	movq	(%rdi), %rdi
	cmovnb	%rbx, %r15
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	addq	%r14, %r15
	cmpq	$268435455, %r15
	cmova	%rdx, %r15
	subq	%rsi, %rax
	salq	$3, %r15
	movq	%r15, %r8
	cmpq	%r15, %rax
	jb	.L177
	addq	%rsi, %r8
	movq	%r8, 16(%rdi)
.L142:
	leaq	(%rsi,%r13), %rdx
	cmpq	$1, %rbx
	je	.L143
	leaq	-2(%rbx), %rcx
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	shrq	%rcx
	addq	$1, %rcx
	.p2align 4,,10
	.p2align 3
.L144:
	movq	%rax, %rdi
	addq	$1, %rax
	salq	$4, %rdi
	movups	%xmm0, (%rdx,%rdi)
	cmpq	%rax, %rcx
	ja	.L144
	leaq	(%rcx,%rcx), %rax
	salq	$4, %rcx
	addq	%rcx, %rdx
	cmpq	%rax, %rbx
	je	.L145
.L143:
	movq	$0, (%rdx)
.L145:
	movq	16(%r12), %rax
	movq	8(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L150
	subq	$8, %rax
	leaq	15(%rdx), %rcx
	subq	%rdx, %rax
	subq	%rsi, %rcx
	movq	%rax, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rcx
	jbe	.L147
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdi
	je	.L147
	addq	$1, %rdi
	movq	%rdx, %r8
	movq	%rsi, %rax
	movq	%rdi, %rcx
	subq	%rsi, %r8
	shrq	%rcx
	salq	$4, %rcx
	addq	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L148:
	movdqu	(%rax,%r8), %xmm1
	addq	$16, %rax
	movups	%xmm1, -16(%rax)
	cmpq	%rax, %rcx
	jne	.L148
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	addq	%rcx, %rdx
	addq	%rsi, %rcx
	cmpq	%rax, %rdi
	je	.L150
	movq	(%rdx), %rax
	movq	%rax, (%rcx)
.L150:
	addq	%r14, %rbx
	addq	%rsi, %r15
	movq	%rsi, 8(%r12)
	leaq	(%rsi,%rbx,8), %rax
	movq	%r15, 24(%r12)
	movq	%rax, 16(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	movq	%rcx, %rdx
	jmp	.L136
.L147:
	leaq	8(%rsi,%rax), %rdi
	subq	%rsi, %rdx
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L152:
	movq	(%rax,%rdx), %rcx
	addq	$8, %rax
	movq	%rcx, -8(%rax)
	cmpq	%rax, %rdi
	jne	.L152
	jmp	.L150
.L177:
	movq	%r15, %rsi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rsi
	jmp	.L142
.L176:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22635:
	.size	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm, .-_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	.section	.rodata._ZNK2v88internal26SourceTextModuleDescriptor23SerializeRegularExportsEPNS0_7IsolateEPNS0_4ZoneE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZNK2v88internal26SourceTextModuleDescriptor23SerializeRegularExportsEPNS0_7IsolateEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal26SourceTextModuleDescriptor23SerializeRegularExportsEPNS0_7IsolateEPNS0_4ZoneE
	.type	_ZNK2v88internal26SourceTextModuleDescriptor23SerializeRegularExportsEPNS0_7IsolateEPNS0_4ZoneE, @function
_ZNK2v88internal26SourceTextModuleDescriptor23SerializeRegularExportsEPNS0_7IsolateEPNS0_4ZoneE:
.LFB19302:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	leaq	(%rax,%rax,2), %rax
	cmpq	$268435455, %rax
	ja	.L236
	movq	%rdx, -96(%rbp)
	movq	%rdi, %r13
	leaq	0(,%rax,8), %r12
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	testq	%rax, %rax
	je	.L180
	movq	16(%rdx), %rdi
	movq	24(%rdx), %rax
	movq	%r12, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %r12
	ja	.L237
	addq	%rdi, %rsi
	movq	%rsi, 16(%rdx)
.L182:
	leaq	(%rdi,%r12), %rbx
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%rdi, -88(%rbp)
	movq	%rbx, -72(%rbp)
	call	memset@PLT
.L207:
	movq	152(%r13), %r14
	addq	$136, %r13
	movq	%rbx, -80(%rbp)
	cmpq	%r13, %r14
	je	.L183
	movq	$8, -128(%rbp)
	movl	$0, -148(%rbp)
	.p2align 4,,10
	.p2align 3
.L194:
	xorl	%r12d, %r12d
	movq	%r14, %rdi
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L238:
	movq	32(%r14), %rax
	cmpq	%rax, 32(%rdi)
	jne	.L184
.L185:
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	addl	$1, %r12d
	movq	%rax, %rdi
	cmpq	%r13, %rax
	jne	.L238
.L184:
	movq	-136(%rbp), %rbx
	movl	%r12d, %esi
	xorl	%edx, %edx
	movq	%rdi, %r15
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	-128(%rbp), %rsi
	movq	%rax, %r12
	movq	40(%r14), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rdx
	movq	-88(%rbp), %rax
	movq	%rdx, -8(%rax,%rsi)
	movq	40(%r14), %rax
	movq	41112(%rbx), %rdi
	movslq	36(%rax), %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L186
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L187:
	movq	-88(%rbp), %rdx
	movq	-128(%rbp), %rbx
	addl	$3, -148(%rbp)
	movq	%rax, (%rdx,%rbx)
	movq	-88(%rbp), %rax
	movq	%r12, 8(%rax,%rbx)
	cmpq	%r15, %r14
	je	.L189
	movq	%r13, -144(%rbp)
	movl	$16, %ebx
	.p2align 4,,10
	.p2align 3
.L193:
	movq	40(%r14), %rax
	movq	(%r12), %r13
	movq	8(%rax), %rax
	leaq	-1(%r13,%rbx), %rsi
	movq	(%rax), %rax
	movq	(%rax), %rdx
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L203
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -104(%rbp)
	testl	$262144, %eax
	je	.L191
	movq	%r13, %rdi
	movq	%rdx, -120(%rbp)
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %rcx
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	8(%rcx), %rax
.L191:
	testb	$24, %al
	je	.L203
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L203
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L203:
	movq	%r14, %rdi
	addq	$8, %rbx
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r14
	cmpq	%r15, %rax
	jne	.L193
	movq	-144(%rbp), %r13
.L189:
	addq	$24, -128(%rbp)
	cmpq	%r14, %r13
	jne	.L194
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rcx
	movslq	-148(%rbp), %rsi
	movq	%rdx, %rax
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rsi, %rax
	jb	.L239
	jbe	.L234
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rdx, %rax
	je	.L234
.L210:
	movl	-148(%rbp), %ebx
	movq	-136(%rbp), %rdi
	xorl	%edx, %edx
	movq	%rax, -80(%rbp)
	movl	%ebx, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r15
	testl	%ebx, %ebx
	je	.L201
.L196:
	movl	-148(%rbp), %eax
	xorl	%ebx, %ebx
	subl	$1, %eax
	leaq	8(,%rax,8), %rax
	movq	%rax, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L202:
	movq	-88(%rbp), %rax
	movq	(%r15), %r13
	movq	(%rax,%rbx), %rax
	leaq	15(%r13,%rbx), %r14
	movq	(%rax), %r12
	movq	%r12, (%r14)
	testb	$1, %r12b
	je	.L204
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -112(%rbp)
	testl	$262144, %eax
	je	.L199
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-112(%rbp), %rcx
	movq	8(%rcx), %rax
.L199:
	testb	$24, %al
	je	.L204
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L204
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L204:
	addq	$8, %rbx
	cmpq	-104(%rbp), %rbx
	jne	.L202
.L201:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L240
	addq	$120, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L239:
	.cfi_restore_state
	subq	%rax, %rsi
	leaq	-96(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
.L234:
	movl	-148(%rbp), %esi
	movq	-136(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r15
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L186:
	movq	-136(%rbp), %rbx
	movq	41088(%rbx), %rax
	cmpq	%rax, 41096(%rbx)
	je	.L241
.L188:
	movq	-136(%rbp), %rbx
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L187
.L241:
	movq	%rbx, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	jmp	.L188
.L237:
	movq	%rdx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L182
.L183:
	movl	$0, -148(%rbp)
	movq	-88(%rbp), %rax
	cmpq	%rax, %rbx
	jne	.L210
	movq	-136(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r15
	jmp	.L201
.L180:
	movq	%r12, -72(%rbp)
	xorl	%ebx, %ebx
	jmp	.L207
.L240:
	call	__stack_chk_fail@PLT
.L236:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE19302:
	.size	_ZNK2v88internal26SourceTextModuleDescriptor23SerializeRegularExportsEPNS0_7IsolateEPNS0_4ZoneE, .-_ZNK2v88internal26SourceTextModuleDescriptor23SerializeRegularExportsEPNS0_7IsolateEPNS0_4ZoneE
	.section	.text._ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_PKNS1_26SourceTextModuleDescriptor5EntryEESt10_Select1stISB_ESt4lessIS4_ENS1_13ZoneAllocatorISB_EEE17_M_emplace_uniqueIJS5_IS4_SA_EEEES5_ISt17_Rb_tree_iteratorISB_EbEDpOT_,"axG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_PKNS1_26SourceTextModuleDescriptor5EntryEESt10_Select1stISB_ESt4lessIS4_ENS1_13ZoneAllocatorISB_EEE17_M_emplace_uniqueIJS5_IS4_SA_EEEES5_ISt17_Rb_tree_iteratorISB_EbEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_PKNS1_26SourceTextModuleDescriptor5EntryEESt10_Select1stISB_ESt4lessIS4_ENS1_13ZoneAllocatorISB_EEE17_M_emplace_uniqueIJS5_IS4_SA_EEEES5_ISt17_Rb_tree_iteratorISB_EbEDpOT_
	.type	_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_PKNS1_26SourceTextModuleDescriptor5EntryEESt10_Select1stISB_ESt4lessIS4_ENS1_13ZoneAllocatorISB_EEE17_M_emplace_uniqueIJS5_IS4_SA_EEEES5_ISt17_Rb_tree_iteratorISB_EbEDpOT_, @function
_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_PKNS1_26SourceTextModuleDescriptor5EntryEESt10_Select1stISB_ESt4lessIS4_ENS1_13ZoneAllocatorISB_EEE17_M_emplace_uniqueIJS5_IS4_SA_EEEES5_ISt17_Rb_tree_iteratorISB_EbEDpOT_:
.LFB22644:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$47, %rax
	jbe	.L262
	leaq	48(%rbx), %rax
	movq	%rax, 16(%rdi)
.L244:
	movq	(%r14), %r15
	movq	8(%r14), %rax
	leaq	16(%r13), %r14
	movq	%r15, 32(%rbx)
	movq	%rax, 40(%rbx)
	movq	24(%r13), %r12
	testq	%r12, %r12
	jne	.L246
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L264:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L247
.L265:
	movq	%rax, %r12
.L246:
	movq	32(%r12), %rcx
	cmpq	%r15, %rcx
	ja	.L264
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L265
.L247:
	testb	%dl, %dl
	jne	.L266
	cmpq	%r15, %rcx
	jnb	.L255
.L254:
	movl	$1, %edi
	cmpq	%r12, %r14
	jne	.L267
.L252:
	movq	%r12, %rdx
	movq	%r14, %rcx
	movq	%rbx, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r13)
	addq	$8, %rsp
	movq	%rbx, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L266:
	.cfi_restore_state
	cmpq	%r12, 32(%r13)
	je	.L254
.L256:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	cmpq	32(%rax), %r15
	ja	.L254
	movq	%rax, %r12
.L255:
	addq	$8, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	.cfi_restore_state
	xorl	%edi, %edi
	cmpq	32(%r12), %r15
	setb	%dil
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L263:
	movq	%r14, %r12
	cmpq	32(%r13), %r14
	jne	.L256
	movl	$1, %edi
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L262:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L244
	.cfi_endproc
.LFE22644:
	.size	_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_PKNS1_26SourceTextModuleDescriptor5EntryEESt10_Select1stISB_ESt4lessIS4_ENS1_13ZoneAllocatorISB_EEE17_M_emplace_uniqueIJS5_IS4_SA_EEEES5_ISt17_Rb_tree_iteratorISB_EbEDpOT_, .-_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_PKNS1_26SourceTextModuleDescriptor5EntryEESt10_Select1stISB_ESt4lessIS4_ENS1_13ZoneAllocatorISB_EEE17_M_emplace_uniqueIJS5_IS4_SA_EEEES5_ISt17_Rb_tree_iteratorISB_EbEDpOT_
	.section	.text._ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_PKNS1_26SourceTextModuleDescriptor5EntryEESt10_Select1stISB_ESt4lessIS4_ENS1_13ZoneAllocatorISB_EEE8_M_eraseEPSt13_Rb_tree_nodeISB_E,"axG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_PKNS1_26SourceTextModuleDescriptor5EntryEESt10_Select1stISB_ESt4lessIS4_ENS1_13ZoneAllocatorISB_EEE8_M_eraseEPSt13_Rb_tree_nodeISB_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_PKNS1_26SourceTextModuleDescriptor5EntryEESt10_Select1stISB_ESt4lessIS4_ENS1_13ZoneAllocatorISB_EEE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	.type	_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_PKNS1_26SourceTextModuleDescriptor5EntryEESt10_Select1stISB_ESt4lessIS4_ENS1_13ZoneAllocatorISB_EEE8_M_eraseEPSt13_Rb_tree_nodeISB_E, @function
_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_PKNS1_26SourceTextModuleDescriptor5EntryEESt10_Select1stISB_ESt4lessIS4_ENS1_13ZoneAllocatorISB_EEE8_M_eraseEPSt13_Rb_tree_nodeISB_E:
.LFB22660:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L276
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L270:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_PKNS1_26SourceTextModuleDescriptor5EntryEESt10_Select1stISB_ESt4lessIS4_ENS1_13ZoneAllocatorISB_EEE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L270
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE22660:
	.size	_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_PKNS1_26SourceTextModuleDescriptor5EntryEESt10_Select1stISB_ESt4lessIS4_ENS1_13ZoneAllocatorISB_EEE8_M_eraseEPSt13_Rb_tree_nodeISB_E, .-_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_PKNS1_26SourceTextModuleDescriptor5EntryEESt10_Select1stISB_ESt4lessIS4_ENS1_13ZoneAllocatorISB_EEE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	.section	.text._ZNK2v88internal26SourceTextModuleDescriptor19FindDuplicateExportEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal26SourceTextModuleDescriptor19FindDuplicateExportEPNS0_4ZoneE
	.type	_ZNK2v88internal26SourceTextModuleDescriptor19FindDuplicateExportEPNS0_4ZoneE, @function
_ZNK2v88internal26SourceTextModuleDescriptor19FindDuplicateExportEPNS0_4ZoneE:
.LFB19333:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -144(%rbp)
	movq	152(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-96(%rbp), %rax
	movq	%rsi, -112(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rdi, %rax
	addq	$136, %rax
	movl	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -64(%rbp)
	movq	%rax, -136(%rbp)
	cmpq	%rax, %r15
	je	.L280
	xorl	%r12d, %r12d
	leaq	-128(%rbp), %r14
	leaq	-112(%rbp), %r13
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L282:
	movl	(%r12), %eax
	cmpl	%eax, (%rbx)
	cmovg	%rbx, %r12
.L281:
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%rax, -136(%rbp)
	je	.L311
.L283:
	movq	40(%r15), %rbx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	8(%rbx), %xmm0
	movq	%rbx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -128(%rbp)
	call	_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_PKNS1_26SourceTextModuleDescriptor5EntryEESt10_Select1stISB_ESt4lessIS4_ENS1_13ZoneAllocatorISB_EEE17_M_emplace_uniqueIJS5_IS4_SA_EEEES5_ISt17_Rb_tree_iteratorISB_EbEDpOT_
	testb	%dl, %dl
	jne	.L281
	testq	%r12, %r12
	jne	.L282
	movq	40(%rax), %r12
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L311:
	movq	-144(%rbp), %rax
	movq	64(%rax), %rbx
	movq	72(%rax), %rax
	movq	%rax, -136(%rbp)
	cmpq	%rax, %rbx
	je	.L284
.L292:
	leaq	-128(%rbp), %r15
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L286:
	movl	(%r12), %eax
	cmpl	%eax, (%r14)
	cmovg	%r14, %r12
.L285:
	addq	$8, %rbx
	cmpq	%rbx, -136(%rbp)
	je	.L284
.L287:
	movq	(%rbx), %r14
	movq	8(%r14), %rax
	testq	%rax, %rax
	je	.L285
	movq	%rax, %xmm0
	movq	%r14, %xmm2
	leaq	-112(%rbp), %r13
	movq	%r15, %rsi
	punpcklqdq	%xmm2, %xmm0
	movq	%r13, %rdi
	movaps	%xmm0, -128(%rbp)
	call	_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_PKNS1_26SourceTextModuleDescriptor5EntryEESt10_Select1stISB_ESt4lessIS4_ENS1_13ZoneAllocatorISB_EEE17_M_emplace_uniqueIJS5_IS4_SA_EEEES5_ISt17_Rb_tree_iteratorISB_EbEDpOT_
	testb	%dl, %dl
	jne	.L285
	testq	%r12, %r12
	jne	.L286
	movq	40(%rax), %r12
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L284:
	movq	-88(%rbp), %r13
	testq	%r13, %r13
	je	.L279
	leaq	-112(%rbp), %r14
.L291:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	je	.L289
.L290:
	movq	24(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_PKNS1_26SourceTextModuleDescriptor5EntryEESt10_Select1stISB_ESt4lessIS4_ENS1_13ZoneAllocatorISB_EEE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L290
.L289:
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L291
.L279:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L312
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L280:
	.cfi_restore_state
	movq	72(%rdi), %rax
	movq	64(%rdi), %rbx
	xorl	%r12d, %r12d
	movq	%rax, -136(%rbp)
	cmpq	%rbx, %rax
	jne	.L292
	jmp	.L279
.L312:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19333:
	.size	_ZNK2v88internal26SourceTextModuleDescriptor19FindDuplicateExportEPNS0_4ZoneE, .-_ZNK2v88internal26SourceTextModuleDescriptor19FindDuplicateExportEPNS0_4ZoneE
	.section	.text._ZN2v88internal26SourceTextModuleDescriptor8ValidateEPNS0_11ModuleScopeEPNS0_30PendingCompilationErrorHandlerEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26SourceTextModuleDescriptor8ValidateEPNS0_11ModuleScopeEPNS0_30PendingCompilationErrorHandlerEPNS0_4ZoneE
	.type	_ZN2v88internal26SourceTextModuleDescriptor8ValidateEPNS0_11ModuleScopeEPNS0_30PendingCompilationErrorHandlerEPNS0_4ZoneE, @function
_ZN2v88internal26SourceTextModuleDescriptor8ValidateEPNS0_11ModuleScopeEPNS0_30PendingCompilationErrorHandlerEPNS0_4ZoneE:
.LFB19340:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rcx, %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZNK2v88internal26SourceTextModuleDescriptor19FindDuplicateExportEPNS0_4ZoneE
	testq	%rax, %rax
	jne	.L335
	movq	152(%rbx), %r14
	leaq	136(%rbx), %r12
	addq	$32, %r13
	cmpq	%r12, %r14
	jne	.L320
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L319:
	movq	%r14, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r14
	cmpq	%rax, %r12
	je	.L321
.L320:
	movq	40(%r14), %r15
	movq	%r13, %rdi
	movq	16(%r15), %rsi
	call	_ZN2v88internal11VariableMap6LookupEPKNS0_12AstRawStringE@PLT
	testq	%rax, %rax
	jne	.L319
	movl	4(%r15), %edx
	movq	16(%r15), %r8
	movl	$273, %ecx
	movl	(%r15), %esi
	movq	-56(%rbp), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKNS0_12AstRawStringE@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	movl	4(%rax), %edx
	movq	8(%rax), %r8
	movl	$232, %ecx
	movl	(%rax), %esi
	movq	-56(%rbp), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKNS0_12AstRawStringE@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L321:
	.cfi_restore_state
	movq	-64(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal26SourceTextModuleDescriptor27MakeIndirectExportsExplicitEPNS0_4ZoneE
	movq	152(%rbx), %rdi
	cmpq	%rdi, %r12
	je	.L318
	movq	32(%rdi), %r14
	movl	$1, %r13d
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L337:
	movq	32(%rax), %rax
	cmpq	%r14, %rax
	jne	.L336
.L322:
	movq	40(%rdi), %rax
	movl	%r13d, 36(%rax)
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdi
	cmpq	%rax, %r12
	jne	.L337
.L318:
	movq	208(%rbx), %rdi
	addq	$192, %rbx
	movl	$-1, %r12d
	cmpq	%rbx, %rdi
	je	.L325
.L324:
	movq	40(%rdi), %rax
	movl	%r12d, 36(%rax)
	subl	$1, %r12d
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdi
	cmpq	%rax, %rbx
	jne	.L324
.L325:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	.cfi_restore_state
	addl	$1, %r13d
	movq	%rax, %r14
	jmp	.L322
	.cfi_endproc
.LFE19340:
	.size	_ZN2v88internal26SourceTextModuleDescriptor8ValidateEPNS0_11ModuleScopeEPNS0_30PendingCompilationErrorHandlerEPNS0_4ZoneE, .-_ZN2v88internal26SourceTextModuleDescriptor8ValidateEPNS0_11ModuleScopeEPNS0_30PendingCompilationErrorHandlerEPNS0_4ZoneE
	.section	.text._ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_PNS1_26SourceTextModuleDescriptor5EntryEESt10_Select1stISA_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorISA_EEE24_M_get_insert_unique_posERS6_,"axG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_PNS1_26SourceTextModuleDescriptor5EntryEESt10_Select1stISA_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorISA_EEE24_M_get_insert_unique_posERS6_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_PNS1_26SourceTextModuleDescriptor5EntryEESt10_Select1stISA_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorISA_EEE24_M_get_insert_unique_posERS6_
	.type	_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_PNS1_26SourceTextModuleDescriptor5EntryEESt10_Select1stISA_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorISA_EEE24_M_get_insert_unique_posERS6_, @function
_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_PNS1_26SourceTextModuleDescriptor5EntryEESt10_Select1stISA_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorISA_EEE24_M_get_insert_unique_posERS6_:
.LFB22790:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L362
	movq	(%rsi), %r13
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L364:
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L342
.L365:
	movq	%rdx, %rbx
.L341:
	movq	32(%rbx), %r12
	cmpq	%r13, %r12
	je	.L343
	movzbl	28(%r13), %eax
	cmpb	%al, 28(%r12)
	je	.L363
.L344:
	testb	%al, %al
	jne	.L364
.L343:
	movq	24(%rbx), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	jne	.L365
.L342:
	movq	%rbx, %rcx
	testb	%al, %al
	jne	.L340
.L347:
	cmpq	%r13, %r12
	je	.L350
	movzbl	28(%r12), %eax
	cmpb	28(%r13), %al
	je	.L366
.L351:
	testb	%al, %al
	jne	.L367
.L350:
	movq	%rbx, %rax
	xorl	%edx, %edx
.L349:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L363:
	.cfi_restore_state
	movq	16(%r13), %rdx
	cmpl	16(%r12), %edx
	je	.L345
	setl	%al
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L345:
	movq	8(%r12), %rsi
	movq	8(%r13), %rdi
	movslq	%edx, %rdx
	call	memcmp@PLT
	shrl	$31, %eax
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L362:
	leaq	16(%rdi), %rbx
.L340:
	xorl	%eax, %eax
	movq	%rbx, %rdx
	cmpq	32(%r14), %rbx
	je	.L349
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rbx, %rcx
	movq	(%r15), %r13
	movq	32(%rax), %r12
	movq	%rax, %rbx
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L367:
	addq	$24, %rsp
	xorl	%eax, %eax
	movq	%rcx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L366:
	.cfi_restore_state
	movq	16(%r12), %rax
	cmpl	16(%r13), %eax
	je	.L352
	setl	%al
	testb	%al, %al
	je	.L350
	jmp	.L367
.L352:
	movq	8(%r13), %rsi
	movq	8(%r12), %rdi
	movslq	%eax, %rdx
	movq	%rcx, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %rcx
	shrl	$31, %eax
	jmp	.L351
	.cfi_endproc
.LFE22790:
	.size	_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_PNS1_26SourceTextModuleDescriptor5EntryEESt10_Select1stISA_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorISA_EEE24_M_get_insert_unique_posERS6_, .-_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_PNS1_26SourceTextModuleDescriptor5EntryEESt10_Select1stISA_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorISA_EEE24_M_get_insert_unique_posERS6_
	.section	.text._ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_NS1_26SourceTextModuleDescriptor13ModuleRequestEESt10_Select1stIS9_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorIS9_EEE24_M_get_insert_unique_posERS6_,"axG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_NS1_26SourceTextModuleDescriptor13ModuleRequestEESt10_Select1stIS9_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorIS9_EEE24_M_get_insert_unique_posERS6_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_NS1_26SourceTextModuleDescriptor13ModuleRequestEESt10_Select1stIS9_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorIS9_EEE24_M_get_insert_unique_posERS6_
	.type	_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_NS1_26SourceTextModuleDescriptor13ModuleRequestEESt10_Select1stIS9_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorIS9_EEE24_M_get_insert_unique_posERS6_, @function
_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_NS1_26SourceTextModuleDescriptor13ModuleRequestEESt10_Select1stIS9_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorIS9_EEE24_M_get_insert_unique_posERS6_:
.LFB22805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L392
	movq	(%rsi), %r13
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L394:
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L372
.L395:
	movq	%rdx, %rbx
.L371:
	movq	32(%rbx), %r12
	cmpq	%r13, %r12
	je	.L373
	movzbl	28(%r13), %eax
	cmpb	%al, 28(%r12)
	je	.L393
.L374:
	testb	%al, %al
	jne	.L394
.L373:
	movq	24(%rbx), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	jne	.L395
.L372:
	movq	%rbx, %rcx
	testb	%al, %al
	jne	.L370
.L377:
	cmpq	%r13, %r12
	je	.L380
	movzbl	28(%r12), %eax
	cmpb	28(%r13), %al
	je	.L396
.L381:
	testb	%al, %al
	jne	.L397
.L380:
	movq	%rbx, %rax
	xorl	%edx, %edx
.L379:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L393:
	.cfi_restore_state
	movq	16(%r13), %rdx
	cmpl	16(%r12), %edx
	je	.L375
	setl	%al
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L375:
	movq	8(%r12), %rsi
	movq	8(%r13), %rdi
	movslq	%edx, %rdx
	call	memcmp@PLT
	shrl	$31, %eax
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L392:
	leaq	16(%rdi), %rbx
.L370:
	xorl	%eax, %eax
	movq	%rbx, %rdx
	cmpq	32(%r14), %rbx
	je	.L379
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rbx, %rcx
	movq	(%r15), %r13
	movq	32(%rax), %r12
	movq	%rax, %rbx
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L397:
	addq	$24, %rsp
	xorl	%eax, %eax
	movq	%rcx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	.cfi_restore_state
	movq	16(%r12), %rax
	cmpl	16(%r13), %eax
	je	.L382
	setl	%al
	testb	%al, %al
	je	.L380
	jmp	.L397
.L382:
	movq	8(%r13), %rsi
	movq	8(%r12), %rdi
	movslq	%eax, %rdx
	movq	%rcx, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %rcx
	shrl	$31, %eax
	jmp	.L381
	.cfi_endproc
.LFE22805:
	.size	_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_NS1_26SourceTextModuleDescriptor13ModuleRequestEESt10_Select1stIS9_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorIS9_EEE24_M_get_insert_unique_posERS6_, .-_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_NS1_26SourceTextModuleDescriptor13ModuleRequestEESt10_Select1stIS9_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorIS9_EEE24_M_get_insert_unique_posERS6_
	.section	.text._ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_NS1_26SourceTextModuleDescriptor13ModuleRequestEESt10_Select1stIS9_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS5_IS4_S8_EEEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_,"axG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_NS1_26SourceTextModuleDescriptor13ModuleRequestEESt10_Select1stIS9_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS5_IS4_S8_EEEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_NS1_26SourceTextModuleDescriptor13ModuleRequestEESt10_Select1stIS9_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS5_IS4_S8_EEEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_
	.type	_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_NS1_26SourceTextModuleDescriptor13ModuleRequestEESt10_Select1stIS9_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS5_IS4_S8_EEEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_, @function
_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_NS1_26SourceTextModuleDescriptor13ModuleRequestEESt10_Select1stIS9_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS5_IS4_S8_EEEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_:
.LFB21687:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$47, %rax
	jbe	.L410
	leaq	48(%rbx), %rax
	movq	%rax, 16(%rdi)
.L400:
	movq	0(%r13), %rax
	leaq	32(%rbx), %rsi
	movq	%r12, %rdi
	movq	%rax, 32(%rbx)
	movq	8(%r13), %rax
	movq	%rax, 40(%rbx)
	call	_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_NS1_26SourceTextModuleDescriptor13ModuleRequestEESt10_Select1stIS9_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorIS9_EEE24_M_get_insert_unique_posERS6_
	movq	%rdx, %r13
	xorl	%edx, %edx
	testq	%r13, %r13
	je	.L405
	leaq	16(%r12), %rcx
	movl	$1, %edi
	testq	%rax, %rax
	je	.L411
.L402:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r12)
	movq	%rbx, %rax
	movl	$1, %edx
.L405:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L411:
	.cfi_restore_state
	cmpq	%rcx, %r13
	je	.L402
	movq	32(%r13), %rdx
	movq	32(%rbx), %rax
	cmpq	%rax, %rdx
	je	.L408
	movzbl	28(%rax), %edi
	cmpb	28(%rdx), %dil
	je	.L412
.L403:
	movzbl	%dil, %edi
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L410:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L412:
	movq	16(%rax), %rsi
	cmpl	16(%rdx), %esi
	je	.L404
	setl	%dil
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L408:
	xorl	%edi, %edi
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L404:
	movslq	%esi, %r8
	movq	8(%rax), %rdi
	movq	8(%rdx), %rsi
	movq	%rcx, -40(%rbp)
	movq	%r8, %rdx
	call	memcmp@PLT
	movq	-40(%rbp), %rcx
	shrl	$31, %eax
	movl	%eax, %edi
	jmp	.L403
	.cfi_endproc
.LFE21687:
	.size	_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_NS1_26SourceTextModuleDescriptor13ModuleRequestEESt10_Select1stIS9_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS5_IS4_S8_EEEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_, .-_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_NS1_26SourceTextModuleDescriptor13ModuleRequestEESt10_Select1stIS9_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS5_IS4_S8_EEEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_
	.section	.text._ZN2v88internal26SourceTextModuleDescriptor9AddImportEPKNS0_12AstRawStringES4_S4_NS0_7Scanner8LocationES6_PNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26SourceTextModuleDescriptor9AddImportEPKNS0_12AstRawStringES4_S4_NS0_7Scanner8LocationES6_PNS0_4ZoneE
	.type	_ZN2v88internal26SourceTextModuleDescriptor9AddImportEPKNS0_12AstRawStringES4_S4_NS0_7Scanner8LocationES6_PNS0_4ZoneE, @function
_ZN2v88internal26SourceTextModuleDescriptor9AddImportEPKNS0_12AstRawStringES4_S4_NS0_7Scanner8LocationES6_PNS0_4ZoneE:
.LFB19294:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$39, %rax
	jbe	.L430
	leaq	40(%rbx), %rax
	movq	%rax, 16(%rdi)
.L415:
	movl	$4294967295, %eax
	movq	%r8, (%rbx)
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movq	$0, 8(%rbx)
	leaq	176(%r12), %r14
	movq	%rax, 32(%rbx)
	movq	%rdx, 16(%rbx)
	movq	%r15, 24(%rbx)
	movq	48(%r12), %rax
	movq	%rcx, -80(%rbp)
	movl	%eax, -72(%rbp)
	movl	%r9d, -68(%rbp)
	call	_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_NS1_26SourceTextModuleDescriptor13ModuleRequestEESt10_Select1stIS9_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS5_IS4_S8_EEEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_
	movq	16(%rbx), %xmm0
	movl	40(%rax), %eax
	movl	%eax, 32(%rbx)
	movq	176(%r12), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$47, %rax
	jbe	.L431
	leaq	48(%r13), %rax
	movq	%rax, 16(%rdi)
.L417:
	movq	%rbx, %xmm1
	movq	%r14, %rdi
	leaq	32(%r13), %rsi
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 32(%r13)
	call	_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_PNS1_26SourceTextModuleDescriptor5EntryEESt10_Select1stISA_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorISA_EEE24_M_get_insert_unique_posERS6_
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L413
	leaq	192(%r12), %rcx
	movl	$1, %edi
	testq	%rax, %rax
	je	.L432
.L419:
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 224(%r12)
.L413:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L433
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	.cfi_restore_state
	cmpq	%rdx, %rcx
	je	.L419
	movq	32(%rdx), %rdx
	movq	32(%r13), %rax
	cmpq	%rax, %rdx
	je	.L425
	movzbl	28(%rax), %edi
	cmpb	28(%rdx), %dil
	je	.L434
.L420:
	movzbl	%dil, %edi
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L430:
	movl	$40, %esi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	movq	%rax, %rbx
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L431:
	movl	$48, %esi
	movq	%xmm0, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %xmm0
	movq	%rax, %r13
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L434:
	movq	16(%rax), %rsi
	cmpl	16(%rdx), %esi
	je	.L421
	setl	%dil
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L425:
	xorl	%edi, %edi
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L421:
	movslq	%esi, %r8
	movq	8(%rax), %rdi
	movq	8(%rdx), %rsi
	movq	%rcx, -88(%rbp)
	movq	%r8, %rdx
	call	memcmp@PLT
	movq	-88(%rbp), %rcx
	shrl	$31, %eax
	movl	%eax, %edi
	jmp	.L420
.L433:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19294:
	.size	_ZN2v88internal26SourceTextModuleDescriptor9AddImportEPKNS0_12AstRawStringES4_S4_NS0_7Scanner8LocationES6_PNS0_4ZoneE, .-_ZN2v88internal26SourceTextModuleDescriptor9AddImportEPKNS0_12AstRawStringES4_S4_NS0_7Scanner8LocationES6_PNS0_4ZoneE
	.section	.text._ZN2v88internal26SourceTextModuleDescriptor14AddEmptyImportEPKNS0_12AstRawStringENS0_7Scanner8LocationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26SourceTextModuleDescriptor14AddEmptyImportEPKNS0_12AstRawStringENS0_7Scanner8LocationE
	.type	_ZN2v88internal26SourceTextModuleDescriptor14AddEmptyImportEPKNS0_12AstRawStringENS0_7Scanner8LocationE, @function
_ZN2v88internal26SourceTextModuleDescriptor14AddEmptyImportEPKNS0_12AstRawStringENS0_7Scanner8LocationE:
.LFB19296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	48(%rdi), %r15
	movq	(%rdi), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$47, %rax
	jbe	.L449
	leaq	48(%r12), %rax
	movq	%rax, 16(%rdi)
.L437:
	movq	%r14, 32(%r12)
	leaq	32(%r12), %rsi
	movq	%rbx, %rdi
	movl	%r15d, 40(%r12)
	movl	%edx, 44(%r12)
	call	_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_NS1_26SourceTextModuleDescriptor13ModuleRequestEESt10_Select1stIS9_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorIS9_EEE24_M_get_insert_unique_posERS6_
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L435
	leaq	16(%rbx), %rcx
	movl	$1, %edi
	testq	%rax, %rax
	je	.L450
.L439:
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
.L435:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L450:
	.cfi_restore_state
	cmpq	%rcx, %rdx
	je	.L439
	movq	32(%rdx), %rdx
	movq	32(%r12), %rax
	cmpq	%rax, %rdx
	je	.L444
	movzbl	28(%rax), %edi
	cmpb	28(%rdx), %dil
	je	.L451
.L440:
	movzbl	%dil, %edi
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L449:
	movl	$48, %esi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L451:
	movq	16(%rax), %rsi
	cmpl	16(%rdx), %esi
	je	.L441
	setl	%dil
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L444:
	xorl	%edi, %edi
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L441:
	movslq	%esi, %r8
	movq	8(%rax), %rdi
	movq	8(%rdx), %rsi
	movq	%rcx, -56(%rbp)
	movq	%r8, %rdx
	call	memcmp@PLT
	movq	-56(%rbp), %rcx
	shrl	$31, %eax
	movl	%eax, %edi
	jmp	.L440
	.cfi_endproc
.LFE19296:
	.size	_ZN2v88internal26SourceTextModuleDescriptor14AddEmptyImportEPKNS0_12AstRawStringENS0_7Scanner8LocationE, .-_ZN2v88internal26SourceTextModuleDescriptor14AddEmptyImportEPKNS0_12AstRawStringENS0_7Scanner8LocationE
	.section	.text._ZN2v88internal26SourceTextModuleDescriptor13AddStarExportEPKNS0_12AstRawStringENS0_7Scanner8LocationES6_PNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26SourceTextModuleDescriptor13AddStarExportEPKNS0_12AstRawStringENS0_7Scanner8LocationES6_PNS0_4ZoneE
	.type	_ZN2v88internal26SourceTextModuleDescriptor13AddStarExportEPKNS0_12AstRawStringENS0_7Scanner8LocationES6_PNS0_4ZoneE, @function
_ZN2v88internal26SourceTextModuleDescriptor13AddStarExportEPKNS0_12AstRawStringENS0_7Scanner8LocationES6_PNS0_4ZoneE:
.LFB19299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	16(%r8), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%r8), %rax
	subq	%r12, %rax
	cmpq	$39, %rax
	jbe	.L471
	leaq	40(%r12), %rax
	movq	%rax, 16(%r8)
.L454:
	movl	$4294967295, %eax
	pxor	%xmm0, %xmm0
	movq	%rdx, (%r12)
	movq	$0, 24(%r12)
	movq	%rax, 32(%r12)
	movups	%xmm0, 8(%r12)
	movq	(%rbx), %rdi
	movq	48(%rbx), %rdx
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$47, %rax
	jbe	.L472
	leaq	48(%r13), %rax
	movq	%rax, 16(%rdi)
.L456:
	movq	%r14, 32(%r13)
	leaq	32(%r13), %rsi
	movq	%rbx, %rdi
	movl	%edx, 40(%r13)
	movl	%r15d, 44(%r13)
	call	_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_NS1_26SourceTextModuleDescriptor13ModuleRequestEESt10_Select1stIS9_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorIS9_EEE24_M_get_insert_unique_posERS6_
	movq	%rdx, %r14
	movq	%rax, %rdx
	testq	%r14, %r14
	je	.L457
	leaq	16(%rbx), %rcx
	movl	$1, %edi
	testq	%rax, %rax
	je	.L473
.L458:
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
	movq	%r13, %rdx
.L457:
	movl	40(%rdx), %eax
	movq	%r12, -64(%rbp)
	movl	%eax, 32(%r12)
	movq	72(%rbx), %rsi
	cmpq	80(%rbx), %rsi
	je	.L461
	movq	%r12, (%rsi)
	addq	$8, 72(%rbx)
.L452:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L474
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L473:
	.cfi_restore_state
	cmpq	%rcx, %r14
	je	.L458
	movq	32(%r14), %rdx
	movq	32(%r13), %rax
	cmpq	%rax, %rdx
	je	.L466
	movzbl	28(%rax), %edi
	cmpb	28(%rdx), %dil
	je	.L475
.L459:
	movzbl	%dil, %edi
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L461:
	leaq	-64(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIPKN2v88internal26SourceTextModuleDescriptor5EntryENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L472:
	movl	$48, %esi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L471:
	movl	$40, %esi
	movq	%r8, %rdi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L475:
	movq	16(%rax), %rsi
	cmpl	16(%rdx), %esi
	je	.L460
	setl	%dil
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L466:
	xorl	%edi, %edi
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L460:
	movslq	%esi, %r8
	movq	8(%rax), %rdi
	movq	8(%rdx), %rsi
	movq	%rcx, -72(%rbp)
	movq	%r8, %rdx
	call	memcmp@PLT
	movq	-72(%rbp), %rcx
	shrl	$31, %eax
	movl	%eax, %edi
	jmp	.L459
.L474:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19299:
	.size	_ZN2v88internal26SourceTextModuleDescriptor13AddStarExportEPKNS0_12AstRawStringENS0_7Scanner8LocationES6_PNS0_4ZoneE, .-_ZN2v88internal26SourceTextModuleDescriptor13AddStarExportEPKNS0_12AstRawStringENS0_7Scanner8LocationES6_PNS0_4ZoneE
	.section	.text._ZN2v88internal26SourceTextModuleDescriptor13AddStarImportEPKNS0_12AstRawStringES4_NS0_7Scanner8LocationES6_PNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26SourceTextModuleDescriptor13AddStarImportEPKNS0_12AstRawStringES4_NS0_7Scanner8LocationES6_PNS0_4ZoneE
	.type	_ZN2v88internal26SourceTextModuleDescriptor13AddStarImportEPKNS0_12AstRawStringES4_NS0_7Scanner8LocationES6_PNS0_4ZoneE, @function
_ZN2v88internal26SourceTextModuleDescriptor13AddStarImportEPKNS0_12AstRawStringES4_NS0_7Scanner8LocationES6_PNS0_4ZoneE:
.LFB19295:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	16(%r9), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%r9), %rax
	subq	%r12, %rax
	cmpq	$39, %rax
	jbe	.L495
	leaq	40(%r12), %rax
	movq	%rax, 16(%r9)
.L478:
	movl	$4294967295, %eax
	movq	%rcx, (%r12)
	movq	$0, 8(%r12)
	movq	$0, 24(%r12)
	movq	%rax, 32(%r12)
	movq	%r13, 16(%r12)
	movq	(%rbx), %rdi
	movq	48(%rbx), %rdx
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$47, %rax
	jbe	.L496
	leaq	48(%r13), %rax
	movq	%rax, 16(%rdi)
.L480:
	movl	%r14d, 44(%r13)
	leaq	32(%r13), %rsi
	movq	%rbx, %rdi
	movq	%r15, 32(%r13)
	movl	%edx, 40(%r13)
	call	_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_NS1_26SourceTextModuleDescriptor13ModuleRequestEESt10_Select1stIS9_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorIS9_EEE24_M_get_insert_unique_posERS6_
	movq	%rdx, %r14
	movq	%rax, %rdx
	testq	%r14, %r14
	je	.L481
	leaq	16(%rbx), %rcx
	movl	$1, %edi
	testq	%rax, %rax
	je	.L497
.L482:
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
	movq	%r13, %rdx
.L481:
	movl	40(%rdx), %eax
	movq	%r12, -64(%rbp)
	movl	%eax, 32(%r12)
	movq	104(%rbx), %rsi
	cmpq	112(%rbx), %rsi
	je	.L485
	movq	%r12, (%rsi)
	addq	$8, 104(%rbx)
.L476:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L498
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L497:
	.cfi_restore_state
	cmpq	%rcx, %r14
	je	.L482
	movq	32(%r14), %rdx
	movq	32(%r13), %rax
	cmpq	%rax, %rdx
	je	.L490
	movzbl	28(%rax), %edi
	cmpb	28(%rdx), %dil
	je	.L499
.L483:
	movzbl	%dil, %edi
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L485:
	leaq	-64(%rbp), %rdx
	leaq	88(%rbx), %rdi
	call	_ZNSt6vectorIPKN2v88internal26SourceTextModuleDescriptor5EntryENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L496:
	movl	$48, %esi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L495:
	movl	$40, %esi
	movq	%r9, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rcx
	movq	%rax, %r12
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L499:
	movq	16(%rax), %rsi
	cmpl	16(%rdx), %esi
	je	.L484
	setl	%dil
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L490:
	xorl	%edi, %edi
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L484:
	movslq	%esi, %r8
	movq	8(%rax), %rdi
	movq	8(%rdx), %rsi
	movq	%rcx, -72(%rbp)
	movq	%r8, %rdx
	call	memcmp@PLT
	movq	-72(%rbp), %rcx
	shrl	$31, %eax
	movl	%eax, %edi
	jmp	.L483
.L498:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19295:
	.size	_ZN2v88internal26SourceTextModuleDescriptor13AddStarImportEPKNS0_12AstRawStringES4_NS0_7Scanner8LocationES6_PNS0_4ZoneE, .-_ZN2v88internal26SourceTextModuleDescriptor13AddStarImportEPKNS0_12AstRawStringES4_NS0_7Scanner8LocationES6_PNS0_4ZoneE
	.section	.text._ZN2v88internal26SourceTextModuleDescriptor9AddExportEPKNS0_12AstRawStringES4_S4_NS0_7Scanner8LocationES6_PNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26SourceTextModuleDescriptor9AddExportEPKNS0_12AstRawStringES4_S4_NS0_7Scanner8LocationES6_PNS0_4ZoneE
	.type	_ZN2v88internal26SourceTextModuleDescriptor9AddExportEPKNS0_12AstRawStringES4_S4_NS0_7Scanner8LocationES6_PNS0_4ZoneE, @function
_ZN2v88internal26SourceTextModuleDescriptor9AddExportEPKNS0_12AstRawStringES4_S4_NS0_7Scanner8LocationES6_PNS0_4ZoneE:
.LFB19298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	16(%rbp), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$39, %rax
	jbe	.L519
	leaq	40(%r12), %rax
	movq	%rax, 16(%rdi)
.L502:
	movl	$4294967295, %eax
	movq	%r8, (%r12)
	movq	$0, 16(%r12)
	movq	%rax, 32(%r12)
	movq	%rdx, 8(%r12)
	movq	%r13, 24(%r12)
	movq	(%rbx), %rdi
	movq	48(%rbx), %rdx
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$47, %rax
	jbe	.L520
	leaq	48(%r13), %rax
	movq	%rax, 16(%rdi)
.L504:
	movl	%r14d, 44(%r13)
	leaq	32(%r13), %rsi
	movq	%rbx, %rdi
	movq	%r15, 32(%r13)
	movl	%edx, 40(%r13)
	call	_ZNSt8_Rb_treeIPKN2v88internal12AstRawStringESt4pairIKS4_NS1_26SourceTextModuleDescriptor13ModuleRequestEESt10_Select1stIS9_ENS7_20AstRawStringComparerENS1_13ZoneAllocatorIS9_EEE24_M_get_insert_unique_posERS6_
	movq	%rdx, %r14
	movq	%rax, %rdx
	testq	%r14, %r14
	je	.L505
	leaq	16(%rbx), %rcx
	movl	$1, %edi
	testq	%rax, %rax
	je	.L521
.L506:
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
	movq	%r13, %rdx
.L505:
	movl	40(%rdx), %eax
	movq	%r12, -64(%rbp)
	movl	%eax, 32(%r12)
	movq	72(%rbx), %rsi
	cmpq	80(%rbx), %rsi
	je	.L509
	movq	%r12, (%rsi)
	addq	$8, 72(%rbx)
.L500:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L522
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L521:
	.cfi_restore_state
	cmpq	%rcx, %r14
	je	.L506
	movq	32(%r14), %rdx
	movq	32(%r13), %rax
	cmpq	%rax, %rdx
	je	.L514
	movzbl	28(%rax), %edi
	cmpb	28(%rdx), %dil
	je	.L523
.L507:
	movzbl	%dil, %edi
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L509:
	leaq	-64(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIPKN2v88internal26SourceTextModuleDescriptor5EntryENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L520:
	movl	$48, %esi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L519:
	movl	$40, %esi
	movq	%r8, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %r8
	movq	%rax, %r12
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L523:
	movq	16(%rax), %rsi
	cmpl	16(%rdx), %esi
	je	.L508
	setl	%dil
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L514:
	xorl	%edi, %edi
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L508:
	movslq	%esi, %r8
	movq	8(%rax), %rdi
	movq	8(%rdx), %rsi
	movq	%rcx, -72(%rbp)
	movq	%r8, %rdx
	call	memcmp@PLT
	movq	-72(%rbp), %rcx
	shrl	$31, %eax
	movl	%eax, %edi
	jmp	.L507
.L522:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19298:
	.size	_ZN2v88internal26SourceTextModuleDescriptor9AddExportEPKNS0_12AstRawStringES4_S4_NS0_7Scanner8LocationES6_PNS0_4ZoneE, .-_ZN2v88internal26SourceTextModuleDescriptor9AddExportEPKNS0_12AstRawStringES4_S4_NS0_7Scanner8LocationES6_PNS0_4ZoneE
	.section	.text.startup._GLOBAL__sub_I__ZNK2v88internal26SourceTextModuleDescriptor20AstRawStringComparerclEPKNS0_12AstRawStringES5_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZNK2v88internal26SourceTextModuleDescriptor20AstRawStringComparerclEPKNS0_12AstRawStringES5_, @function
_GLOBAL__sub_I__ZNK2v88internal26SourceTextModuleDescriptor20AstRawStringComparerclEPKNS0_12AstRawStringES5_:
.LFB23971:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23971:
	.size	_GLOBAL__sub_I__ZNK2v88internal26SourceTextModuleDescriptor20AstRawStringComparerclEPKNS0_12AstRawStringES5_, .-_GLOBAL__sub_I__ZNK2v88internal26SourceTextModuleDescriptor20AstRawStringComparerclEPKNS0_12AstRawStringES5_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZNK2v88internal26SourceTextModuleDescriptor20AstRawStringComparerclEPKNS0_12AstRawStringES5_
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
