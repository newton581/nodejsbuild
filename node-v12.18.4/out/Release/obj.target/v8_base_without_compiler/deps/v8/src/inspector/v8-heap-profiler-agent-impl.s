	.file	"v8-heap-profiler-agent-impl.cc"
	.text
	.section	.text._ZN12v8_inspector17V8InspectorClient23isInspectableHeapObjectEN2v85LocalINS1_6ObjectEEE,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient23isInspectableHeapObjectEN2v85LocalINS1_6ObjectEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient23isInspectableHeapObjectEN2v85LocalINS1_6ObjectEEE
	.type	_ZN12v8_inspector17V8InspectorClient23isInspectableHeapObjectEN2v85LocalINS1_6ObjectEEE, @function
_ZN12v8_inspector17V8InspectorClient23isInspectableHeapObjectEN2v85LocalINS1_6ObjectEEE:
.LFB3635:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3635:
	.size	_ZN12v8_inspector17V8InspectorClient23isInspectableHeapObjectEN2v85LocalINS1_6ObjectEEE, .-_ZN12v8_inspector17V8InspectorClient23isInspectableHeapObjectEN2v85LocalINS1_6ObjectEEE
	.section	.text._ZN12v8_inspector17V8InspectorClient13currentTimeMSEv,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv
	.type	_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv, @function
_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv:
.LFB3646:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	ret
	.cfi_endproc
.LFE3646:
	.size	_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv, .-_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv
	.section	.text._ZN12v8_inspector17V8InspectorClient19startRepeatingTimerEdPFvPvES1_,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient19startRepeatingTimerEdPFvPvES1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient19startRepeatingTimerEdPFvPvES1_
	.type	_ZN12v8_inspector17V8InspectorClient19startRepeatingTimerEdPFvPvES1_, @function
_ZN12v8_inspector17V8InspectorClient19startRepeatingTimerEdPFvPvES1_:
.LFB3647:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3647:
	.size	_ZN12v8_inspector17V8InspectorClient19startRepeatingTimerEdPFvPvES1_, .-_ZN12v8_inspector17V8InspectorClient19startRepeatingTimerEdPFvPvES1_
	.section	.text._ZN12v8_inspector17V8InspectorClient11cancelTimerEPv,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient11cancelTimerEPv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient11cancelTimerEPv
	.type	_ZN12v8_inspector17V8InspectorClient11cancelTimerEPv, @function
_ZN12v8_inspector17V8InspectorClient11cancelTimerEPv:
.LFB3648:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3648:
	.size	_ZN12v8_inspector17V8InspectorClient11cancelTimerEPv, .-_ZN12v8_inspector17V8InspectorClient11cancelTimerEPv
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD2Ev
	.type	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD2Ev, @function
_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD2Ev:
.LFB5549:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5549:
	.size	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD2Ev, .-_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD2Ev
	.weak	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD1Ev
	.set	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD1Ev,_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD2Ev
	.section	.text._ZN2v812OutputStream12GetChunkSizeEv,"axG",@progbits,_ZN2v812OutputStream12GetChunkSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v812OutputStream12GetChunkSizeEv
	.type	_ZN2v812OutputStream12GetChunkSizeEv, @function
_ZN2v812OutputStream12GetChunkSizeEv:
.LFB7535:
	.cfi_startproc
	endbr64
	movl	$1024, %eax
	ret
	.cfi_endproc
.LFE7535:
	.size	_ZN2v812OutputStream12GetChunkSizeEv, .-_ZN2v812OutputStream12GetChunkSizeEv
	.section	.text._ZN2v812OutputStream19WriteHeapStatsChunkEPNS_15HeapStatsUpdateEi,"axG",@progbits,_ZN2v812OutputStream19WriteHeapStatsChunkEPNS_15HeapStatsUpdateEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v812OutputStream19WriteHeapStatsChunkEPNS_15HeapStatsUpdateEi
	.type	_ZN2v812OutputStream19WriteHeapStatsChunkEPNS_15HeapStatsUpdateEi, @function
_ZN2v812OutputStream19WriteHeapStatsChunkEPNS_15HeapStatsUpdateEi:
.LFB7536:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7536:
	.size	_ZN2v812OutputStream19WriteHeapStatsChunkEPNS_15HeapStatsUpdateEi, .-_ZN2v812OutputStream19WriteHeapStatsChunkEPNS_15HeapStatsUpdateEi
	.section	.text._ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStream11EndOfStreamEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStream11EndOfStreamEv, @function
_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStream11EndOfStreamEv:
.LFB7576:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7576:
	.size	_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStream11EndOfStreamEv, .-_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStream11EndOfStreamEv
	.set	_ZN12v8_inspector12_GLOBAL__N_115HeapStatsStream11EndOfStreamEv,_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStream11EndOfStreamEv
	.section	.text._ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStream12GetChunkSizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStream12GetChunkSizeEv, @function
_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStream12GetChunkSizeEv:
.LFB7577:
	.cfi_startproc
	endbr64
	movl	$102400, %eax
	ret
	.cfi_endproc
.LFE7577:
	.size	_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStream12GetChunkSizeEv, .-_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStream12GetChunkSizeEv
	.section	.text._ZN12v8_inspector12_GLOBAL__N_115HeapStatsStream15WriteAsciiChunkEPci,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_115HeapStatsStream15WriteAsciiChunkEPci, @function
_ZN12v8_inspector12_GLOBAL__N_115HeapStatsStream15WriteAsciiChunkEPci:
.LFB7595:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7595:
	.size	_ZN12v8_inspector12_GLOBAL__N_115HeapStatsStream15WriteAsciiChunkEPci, .-_ZN12v8_inspector12_GLOBAL__N_115HeapStatsStream15WriteAsciiChunkEPci
	.section	.text._ZN12v8_inspector23V8HeapProfilerAgentImplD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8HeapProfilerAgentImplD2Ev
	.type	_ZN12v8_inspector23V8HeapProfilerAgentImplD2Ev, @function
_ZN12v8_inspector23V8HeapProfilerAgentImplD2Ev:
.LFB7604:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7604:
	.size	_ZN12v8_inspector23V8HeapProfilerAgentImplD2Ev, .-_ZN12v8_inspector23V8HeapProfilerAgentImplD2Ev
	.globl	_ZN12v8_inspector23V8HeapProfilerAgentImplD1Ev
	.set	_ZN12v8_inspector23V8HeapProfilerAgentImplD1Ev,_ZN12v8_inspector23V8HeapProfilerAgentImplD2Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_120HeapSnapshotProgressD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_120HeapSnapshotProgressD2Ev, @function
_ZN12v8_inspector12_GLOBAL__N_120HeapSnapshotProgressD2Ev:
.LFB10496:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10496:
	.size	_ZN12v8_inspector12_GLOBAL__N_120HeapSnapshotProgressD2Ev, .-_ZN12v8_inspector12_GLOBAL__N_120HeapSnapshotProgressD2Ev
	.set	_ZN12v8_inspector12_GLOBAL__N_120HeapSnapshotProgressD1Ev,_ZN12v8_inspector12_GLOBAL__N_120HeapSnapshotProgressD2Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_121InspectableHeapObjectD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_121InspectableHeapObjectD2Ev, @function
_ZN12v8_inspector12_GLOBAL__N_121InspectableHeapObjectD2Ev:
.LFB10508:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10508:
	.size	_ZN12v8_inspector12_GLOBAL__N_121InspectableHeapObjectD2Ev, .-_ZN12v8_inspector12_GLOBAL__N_121InspectableHeapObjectD2Ev
	.set	_ZN12v8_inspector12_GLOBAL__N_121InspectableHeapObjectD1Ev,_ZN12v8_inspector12_GLOBAL__N_121InspectableHeapObjectD2Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_115HeapStatsStreamD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_115HeapStatsStreamD2Ev, @function
_ZN12v8_inspector12_GLOBAL__N_115HeapStatsStreamD2Ev:
.LFB13911:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE13911:
	.size	_ZN12v8_inspector12_GLOBAL__N_115HeapStatsStreamD2Ev, .-_ZN12v8_inspector12_GLOBAL__N_115HeapStatsStreamD2Ev
	.set	_ZN12v8_inspector12_GLOBAL__N_115HeapStatsStreamD1Ev,_ZN12v8_inspector12_GLOBAL__N_115HeapStatsStreamD2Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStreamD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStreamD2Ev, @function
_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStreamD2Ev:
.LFB13915:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE13915:
	.size	_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStreamD2Ev, .-_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStreamD2Ev
	.set	_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStreamD1Ev,_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStreamD2Ev
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev
	.type	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev, @function
_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev:
.LFB5551:
	.cfi_startproc
	endbr64
	movl	$32, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5551:
	.size	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev, .-_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev
	.section	.text._ZN12v8_inspector23V8HeapProfilerAgentImplD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8HeapProfilerAgentImplD0Ev
	.type	_ZN12v8_inspector23V8HeapProfilerAgentImplD0Ev, @function
_ZN12v8_inspector23V8HeapProfilerAgentImplD0Ev:
.LFB7606:
	.cfi_startproc
	endbr64
	movl	$48, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7606:
	.size	_ZN12v8_inspector23V8HeapProfilerAgentImplD0Ev, .-_ZN12v8_inspector23V8HeapProfilerAgentImplD0Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_120HeapSnapshotProgressD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_120HeapSnapshotProgressD0Ev, @function
_ZN12v8_inspector12_GLOBAL__N_120HeapSnapshotProgressD0Ev:
.LFB10498:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10498:
	.size	_ZN12v8_inspector12_GLOBAL__N_120HeapSnapshotProgressD0Ev, .-_ZN12v8_inspector12_GLOBAL__N_120HeapSnapshotProgressD0Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStreamD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStreamD0Ev, @function
_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStreamD0Ev:
.LFB13917:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE13917:
	.size	_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStreamD0Ev, .-_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStreamD0Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_115HeapStatsStreamD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_115HeapStatsStreamD0Ev, @function
_ZN12v8_inspector12_GLOBAL__N_115HeapStatsStreamD0Ev:
.LFB13913:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE13913:
	.size	_ZN12v8_inspector12_GLOBAL__N_115HeapStatsStreamD0Ev, .-_ZN12v8_inspector12_GLOBAL__N_115HeapStatsStreamD0Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_121InspectableHeapObjectD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_121InspectableHeapObjectD0Ev, @function
_ZN12v8_inspector12_GLOBAL__N_121InspectableHeapObjectD0Ev:
.LFB10510:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10510:
	.size	_ZN12v8_inspector12_GLOBAL__N_121InspectableHeapObjectD0Ev, .-_ZN12v8_inspector12_GLOBAL__N_121InspectableHeapObjectD0Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolverD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolverD2Ev, @function
_ZN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolverD2Ev:
.LFB13919:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolverE(%rip), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L23
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L23:
	ret
	.cfi_endproc
.LFE13919:
	.size	_ZN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolverD2Ev, .-_ZN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolverD2Ev
	.set	_ZN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolverD1Ev,_ZN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolverD2Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolverD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolverD0Ev, @function
_ZN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolverD0Ev:
.LFB13921:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolverE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L26
	call	_ZdlPv@PLT
.L26:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE13921:
	.size	_ZN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolverD0Ev, .-_ZN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolverD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev:
.LFB4606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L32
	call	_ZdlPv@PLT
.L32:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L31
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4606:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev, @function
_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev:
.LFB5119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	leaq	104(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L36
	call	_ZdlPv@PLT
.L36:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L37
	call	_ZdlPv@PLT
.L37:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L35
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5119:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev, .-_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev,_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.section	.text._ZN12v8_inspector23V8HeapProfilerAgentImpl14collectGarbageEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8HeapProfilerAgentImpl14collectGarbageEv
	.type	_ZN12v8_inspector23V8HeapProfilerAgentImpl14collectGarbageEv, @function
_ZN12v8_inspector23V8HeapProfilerAgentImpl14collectGarbageEv:
.LFB7608:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	16(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate21LowMemoryNotificationEv@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L43
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L43:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7608:
	.size	_ZN12v8_inspector23V8HeapProfilerAgentImpl14collectGarbageEv, .-_ZN12v8_inspector23V8HeapProfilerAgentImpl14collectGarbageEv
	.section	.text._ZN12v8_inspector23V8HeapProfilerAgentImpl6enableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8HeapProfilerAgentImpl6enableEv
	.type	_ZN12v8_inspector23V8HeapProfilerAgentImpl6enableEv, @function
_ZN12v8_inspector23V8HeapProfilerAgentImpl6enableEv:
.LFB7612:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$56, %rsp
	movq	32(%rsi), %r14
	leaq	_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL19heapProfilerEnabledE(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L45
	call	_ZdlPv@PLT
.L45:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L48
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L48:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7612:
	.size	_ZN12v8_inspector23V8HeapProfilerAgentImpl6enableEv, .-_ZN12v8_inspector23V8HeapProfilerAgentImpl6enableEv
	.section	.text._ZN12v8_inspector12_GLOBAL__N_120HeapSnapshotProgress19ReportProgressValueEii,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_120HeapSnapshotProgress19ReportProgressValueEii, @function
_ZN12v8_inspector12_GLOBAL__N_120HeapSnapshotProgress19ReportProgressValueEii:
.LFB7554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-42(%rbp), %r14
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movw	%cx, -42(%rbp)
	movq	%r14, %rcx
	call	_ZN12v8_inspector8protocol12HeapProfiler8Frontend26reportHeapSnapshotProgressEiiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE@PLT
	cmpl	%r12d, %r13d
	jge	.L53
.L50:
	movq	8(%rbx), %rdi
	call	_ZN12v8_inspector8protocol12HeapProfiler8Frontend5flushEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L54
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movl	$257, %eax
	movq	%r14, %rcx
	movl	%r12d, %edx
	movl	%r12d, %esi
	movw	%ax, -42(%rbp)
	call	_ZN12v8_inspector8protocol12HeapProfiler8Frontend26reportHeapSnapshotProgressEiiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE@PLT
	jmp	.L50
.L54:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7554:
	.size	_ZN12v8_inspector12_GLOBAL__N_120HeapSnapshotProgress19ReportProgressValueEii, .-_ZN12v8_inspector12_GLOBAL__N_120HeapSnapshotProgress19ReportProgressValueEii
	.section	.text._ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStream15WriteAsciiChunkEPci,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStream15WriteAsciiChunkEPci, @function
_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStream15WriteAsciiChunkEPci:
.LFB7578:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-80(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	8(%rdi), %r13
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKcm@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol12HeapProfiler8Frontend20addHeapSnapshotChunkERKNS_8String16E@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L56
	call	_ZdlPv@PLT
.L56:
	movq	8(%rbx), %rdi
	call	_ZN12v8_inspector8protocol12HeapProfiler8Frontend5flushEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L59
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L59:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7578:
	.size	_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStream15WriteAsciiChunkEPci, .-_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStream15WriteAsciiChunkEPci
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv:
.LFB4615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13CustomPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L60
	movq	(%rdi), %rax
	call	*24(%rax)
.L60:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L67
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L67:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4615:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv:
.LFB4614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13CustomPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L68
	movq	(%rdi), %rax
	call	*24(%rax)
.L68:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L75
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L75:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4614:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv:
.LFB4661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13ObjectPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L76
	movq	(%rdi), %rax
	call	*24(%rax)
.L76:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L83
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L83:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4661:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv:
.LFB4660:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13ObjectPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L84
	movq	(%rdi), %rax
	call	*24(%rax)
.L84:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L91
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L91:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4660:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv:
.LFB4762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime12EntryPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L92
	movq	(%rdi), %rax
	call	*24(%rax)
.L92:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L99
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L99:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4762:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv:
.LFB4761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime12EntryPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L100
	movq	(%rdi), %rax
	call	*24(%rax)
.L100:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L107
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L107:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4761:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv:
.LFB4730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime15PropertyPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L108
	movq	(%rdi), %rax
	call	*24(%rax)
.L108:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L115
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L115:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4730:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv:
.LFB4729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime15PropertyPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L116
	movq	(%rdi), %rax
	call	*24(%rax)
.L116:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L123
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L123:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4729:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector23V8HeapProfilerAgentImpl15getHeapObjectIdERKNS_8String16EPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8HeapProfilerAgentImpl15getHeapObjectIdERKNS_8String16EPS1_
	.type	_ZN12v8_inspector23V8HeapProfilerAgentImpl15getHeapObjectIdERKNS_8String16EPS1_, @function
_ZN12v8_inspector23V8HeapProfilerAgentImpl15getHeapObjectIdERKNS_8String16EPS1_:
.LFB7648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$168, %rsp
	movq	16(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%r9d, %r9d
	leaq	-208(%rbp), %rcx
	movq	%r14, %rdx
	leaq	-200(%rbp), %r8
	movq	$0, -208(%rbp)
	movq	$0, -200(%rbp)
	call	_ZN12v8_inspector22V8InspectorSessionImpl12unwrapObjectERKNS_8String16EPN2v85LocalINS4_5ValueEEEPNS5_INS4_7ContextEEEPS1_@PLT
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L125
	movl	%eax, (%r12)
	leaq	24(%r12), %rax
	leaq	-88(%rbp), %rdx
	movq	%rax, 8(%r12)
	movq	-104(%rbp), %rax
	cmpq	%rdx, %rax
	je	.L148
	movq	%rax, 8(%r12)
	movq	-88(%rbp), %rax
	movq	%rax, 24(%r12)
.L127:
	movq	-96(%rbp), %rax
	movq	%rax, 16(%r12)
	movq	-72(%rbp), %rax
	movq	%rax, 40(%r12)
	movl	-64(%rbp), %eax
	movl	%eax, 48(%r12)
.L128:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L149
	addq	$168, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	movq	-208(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L150
.L129:
	movq	16(%rbx), %rdi
	leaq	-144(%rbp), %rbx
	call	_ZN2v87Isolate15GetHeapProfilerEv@PLT
	movq	-208(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN2v812HeapProfiler11GetObjectIdENS_5LocalINS_5ValueEEE@PLT
	leaq	-160(%rbp), %rdi
	movl	%eax, %esi
	call	_ZN12v8_inspector8String1611fromIntegerEm@PLT
	movq	-160(%rbp), %rdx
	movq	0(%r13), %rdi
	movq	-152(%rbp), %rax
	cmpq	%rbx, %rdx
	je	.L151
	leaq	16(%r13), %rcx
	movq	-144(%rbp), %rsi
	cmpq	%rcx, %rdi
	je	.L152
	movq	%rax, %xmm0
	movq	%rsi, %xmm1
	movq	16(%r13), %rcx
	movq	%rdx, 0(%r13)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r13)
	testq	%rdi, %rdi
	je	.L136
	movq	%rdi, -160(%rbp)
	movq	%rcx, -144(%rbp)
.L134:
	movq	$0, -152(%rbp)
	xorl	%eax, %eax
	movw	%ax, (%rdi)
	movq	-128(%rbp), %rax
	movq	-160(%rbp), %rdi
	movq	%rax, 32(%r13)
	cmpq	%rbx, %rdi
	je	.L137
	call	_ZdlPv@PLT
.L137:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
.L130:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L128
	call	_ZdlPv@PLT
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L150:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L129
	cmpl	$5, 43(%rax)
	jne	.L129
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse13InternalErrorEv@PLT
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L148:
	movdqu	-88(%rbp), %xmm2
	movups	%xmm2, 24(%r12)
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L151:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L132
	cmpq	$1, %rax
	je	.L153
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L132
	movq	%rbx, %rsi
	call	memmove@PLT
	movq	-152(%rbp), %rax
	movq	0(%r13), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L132:
	xorl	%ecx, %ecx
	movq	%rax, 8(%r13)
	movw	%cx, (%rdi,%rdx)
	movq	-160(%rbp), %rdi
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L152:
	movq	%rax, %xmm0
	movq	%rsi, %xmm3
	movq	%rdx, 0(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
.L136:
	movq	%rbx, -160(%rbp)
	leaq	-144(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L153:
	movzwl	-144(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-152(%rbp), %rax
	movq	0(%r13), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L132
.L149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7648:
	.size	_ZN12v8_inspector23V8HeapProfilerAgentImpl15getHeapObjectIdERKNS_8String16EPS1_, .-_ZN12v8_inspector23V8HeapProfilerAgentImpl15getHeapObjectIdERKNS_8String16EPS1_
	.section	.rodata._ZN12v8_inspector23V8HeapProfilerAgentImpl13startSamplingEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIdEE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"Cannot access v8 heap profiler"
	.section	.text._ZN12v8_inspector23V8HeapProfilerAgentImpl13startSamplingEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIdEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8HeapProfilerAgentImpl13startSamplingEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIdEE
	.type	_ZN12v8_inspector23V8HeapProfilerAgentImpl13startSamplingEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIdEE, @function
_ZN12v8_inspector23V8HeapProfilerAgentImpl13startSamplingEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIdEE:
.LFB7657:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	16(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate15GetHeapProfilerEv@PLT
	testq	%rax, %rax
	je	.L166
	cmpb	$0, (%r14)
	movq	%rax, %r13
	jne	.L167
	movq	$32768, -112(%rbp)
	movsd	.LC1(%rip), %xmm0
.L158:
	leaq	-96(%rbp), %r15
	movq	32(%rbx), %r14
	movsd	%xmm0, -104(%rbp)
	leaq	_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL28samplingHeapProfilerIntervalE(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movsd	-104(%rbp), %xmm0
	movq	%r14, %rdi
	movq	%r15, %rsi
	leaq	-80(%rbp), %r14
	call	_ZN12v8_inspector8protocol15DictionaryValue9setDoubleERKNS_8String16Ed@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L161
	call	_ZdlPv@PLT
.L161:
	movq	32(%rbx), %rbx
	leaq	_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL27samplingHeapProfilerEnabledE(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L162
	call	_ZdlPv@PLT
.L162:
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	movl	$1, %ecx
	movl	$128, %edx
	call	_ZN2v812HeapProfiler25StartSamplingHeapProfilerEmiNS0_13SamplingFlagsE@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
.L154:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L168
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	movsd	8(%r14), %xmm0
	movsd	.LC3(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L159
	cvttsd2siq	%xmm0, %rax
	movq	%rax, -112(%rbp)
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L159:
	movapd	%xmm0, %xmm2
	subsd	%xmm1, %xmm2
	cvttsd2siq	%xmm2, %rax
	btcq	$63, %rax
	movq	%rax, -112(%rbp)
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L166:
	leaq	-96(%rbp), %r13
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L154
	call	_ZdlPv@PLT
	jmp	.L154
.L168:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7657:
	.size	_ZN12v8_inspector23V8HeapProfilerAgentImpl13startSamplingEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIdEE, .-_ZN12v8_inspector23V8HeapProfilerAgentImpl13startSamplingEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIdEE
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample17serializeToBinaryEv:
.LFB5559:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L169
	movq	(%rdi), %rax
	call	*24(%rax)
.L169:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L176
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L176:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5559:
	.size	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample17serializeToBinaryEv, .-_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample15serializeToJSONEv, @function
_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample15serializeToJSONEv:
.LFB5558:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L177
	movq	(%rdi), %rax
	call	*24(%rax)
.L177:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L184
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L184:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5558:
	.size	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample15serializeToJSONEv, .-_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile17serializeToBinaryEv:
.LFB5592:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L185
	movq	(%rdi), %rax
	call	*24(%rax)
.L185:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L192
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L192:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5592:
	.size	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile17serializeToBinaryEv, .-_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile15serializeToJSONEv, @function
_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile15serializeToJSONEv:
.LFB5591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L193
	movq	(%rdi), %rax
	call	*24(%rax)
.L193:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L200
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L200:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5591:
	.size	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile15serializeToJSONEv, .-_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv:
.LFB5133:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime9CallFrame7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L201
	movq	(%rdi), %rax
	call	*24(%rax)
.L201:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L208
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L208:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5133:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv:
.LFB5132:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime9CallFrame7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L209
	movq	(%rdi), %rax
	call	*24(%rax)
.L209:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L216
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L216:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5132:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode17serializeToBinaryEv:
.LFB5527:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L217
	movq	(%rdi), %rax
	call	*24(%rax)
.L217:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L224
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L224:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5527:
	.size	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode17serializeToBinaryEv, .-_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode15serializeToJSONEv, @function
_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode15serializeToJSONEv:
.LFB5526:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L225
	movq	(%rdi), %rax
	call	*24(%rax)
.L225:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L232
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L232:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5526:
	.size	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode15serializeToJSONEv, .-_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode15serializeToJSONEv
	.section	.text._ZN12v8_inspector23V8HeapProfilerAgentImpl12stopSamplingEPSt10unique_ptrINS_8protocol12HeapProfiler19SamplingHeapProfileESt14default_deleteIS4_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8HeapProfilerAgentImpl12stopSamplingEPSt10unique_ptrINS_8protocol12HeapProfiler19SamplingHeapProfileESt14default_deleteIS4_EE
	.type	_ZN12v8_inspector23V8HeapProfilerAgentImpl12stopSamplingEPSt10unique_ptrINS_8protocol12HeapProfiler19SamplingHeapProfileESt14default_deleteIS4_EE, @function
_ZN12v8_inspector23V8HeapProfilerAgentImpl12stopSamplingEPSt10unique_ptrINS_8protocol12HeapProfiler19SamplingHeapProfileESt14default_deleteIS4_EE:
.LFB7659:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	call	*64(%rax)
	movl	(%r12), %eax
	testl	%eax, %eax
	je	.L238
.L233:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L239
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L238:
	.cfi_restore_state
	movq	16(%rbx), %rdi
	leaq	-80(%rbp), %r13
	call	_ZN2v87Isolate15GetHeapProfilerEv@PLT
	movq	%rax, %rdi
	call	_ZN2v812HeapProfiler24StopSamplingHeapProfilerEv@PLT
	movq	32(%rbx), %r14
	leaq	_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL27samplingHeapProfilerEnabledE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L233
	call	_ZdlPv@PLT
	jmp	.L233
.L239:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7659:
	.size	_ZN12v8_inspector23V8HeapProfilerAgentImpl12stopSamplingEPSt10unique_ptrINS_8protocol12HeapProfiler19SamplingHeapProfileESt14default_deleteIS4_EE, .-_ZN12v8_inspector23V8HeapProfilerAgentImpl12stopSamplingEPSt10unique_ptrINS_8protocol12HeapProfiler19SamplingHeapProfileESt14default_deleteIS4_EE
	.section	.rodata._ZN12v8_inspector23V8HeapProfilerAgentImpl16takeHeapSnapshotEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"Failed to take heap snapshot"
	.section	.text._ZN12v8_inspector23V8HeapProfilerAgentImpl16takeHeapSnapshotEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8HeapProfilerAgentImpl16takeHeapSnapshotEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE
	.type	_ZN12v8_inspector23V8HeapProfilerAgentImpl16takeHeapSnapshotEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE, @function
_ZN12v8_inspector23V8HeapProfilerAgentImpl16takeHeapSnapshotEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE:
.LFB7614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	16(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate15GetHeapProfilerEv@PLT
	testq	%rax, %rax
	je	.L259
	xorl	%r13d, %r13d
	cmpb	$0, (%r14)
	movq	%rax, %r15
	jne	.L260
.L244:
	movq	8(%rbx), %r8
	pxor	%xmm0, %xmm0
	movl	$10000, %edi
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolverE(%rip), %r14
	movaps	%xmm0, -128(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r14, -144(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -112(%rbp)
	call	_Znwm@PLT
	movl	$10000, %edx
	xorl	%esi, %esi
	leaq	10000(%rax), %rcx
	movq	%rax, %rdi
	movq	%rax, -128(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%rcx, -168(%rbp)
	call	memset@PLT
	movq	-168(%rbp), %rcx
	movq	%r15, %rdi
	movq	-176(%rbp), %r8
	leaq	-144(%rbp), %rdx
	movq	%r13, %rsi
	movq	%rcx, -120(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v812HeapProfiler16TakeHeapSnapshotEPNS_15ActivityControlEPNS0_18ObjectNameResolverE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L261
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStreamE(%rip), %rax
	xorl	%edx, %edx
	movq	%r15, %rdi
	addq	$24, %rbx
	leaq	-160(%rbp), %rsi
	movq	%rax, -160(%rbp)
	movq	%rbx, -152(%rbp)
	call	_ZNK2v812HeapSnapshot9SerializeEPNS_12OutputStreamENS0_19SerializationFormatE@PLT
	movq	%r15, %rdi
	call	_ZN2v812HeapSnapshot6DeleteEv@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
.L247:
	movq	-128(%rbp), %rdi
	movq	%r14, -144(%rbp)
	testq	%rdi, %rdi
	je	.L248
	call	_ZdlPv@PLT
.L248:
	testq	%r13, %r13
	je	.L240
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L240:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L262
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	.cfi_restore_state
	cmpb	$0, 1(%r14)
	je	.L244
	movl	$16, %edi
	call	_Znwm@PLT
	movq	%rax, %r13
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_120HeapSnapshotProgressE(%rip), %rax
	movq	%rax, 0(%r13)
	leaq	24(%rbx), %rax
	movq	%rax, 8(%r13)
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L261:
	leaq	-96(%rbp), %r15
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L247
	call	_ZdlPv@PLT
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L259:
	leaq	-96(%rbp), %r13
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L240
	call	_ZdlPv@PLT
	jmp	.L240
.L262:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7614:
	.size	_ZN12v8_inspector23V8HeapProfilerAgentImpl16takeHeapSnapshotEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE, .-_ZN12v8_inspector23V8HeapProfilerAgentImpl16takeHeapSnapshotEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev:
.LFB4608:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L264
	call	_ZdlPv@PLT
.L264:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L265
	call	_ZdlPv@PLT
.L265:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4608:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev, @function
_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev:
.LFB5121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L268
	call	_ZdlPv@PLT
.L268:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L269
	call	_ZdlPv@PLT
.L269:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L270
	call	_ZdlPv@PLT
.L270:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$136, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5121:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev, .-_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD2Ev
	.type	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD2Ev, @function
_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD2Ev:
.LFB5513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %r15
	movq	%rax, (%rdi)
	testq	%r15, %r15
	je	.L273
	movq	8(%r15), %r14
	movq	(%r15), %r12
	cmpq	%r12, %r14
	jne	.L277
	testq	%r12, %r12
	je	.L278
	.p2align 4,,10
	.p2align 3
.L297:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L278:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L273:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L272
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L280
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r12), %rdi
	movq	%rax, (%r12)
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L281
	call	_ZdlPv@PLT
.L281:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L282
	call	_ZdlPv@PLT
.L282:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L283
	call	_ZdlPv@PLT
.L283:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$136, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L296:
	.cfi_restore_state
	call	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev
	movl	$40, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L275:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L295
.L277:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L275
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L296
	call	*%rax
	addq	$8, %r12
	cmpq	%r12, %r14
	jne	.L277
	.p2align 4,,10
	.p2align 3
.L295:
	movq	(%r15), %r12
	testq	%r12, %r12
	jne	.L297
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L272:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L280:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE5513:
	.size	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD2Ev, .-_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD2Ev
	.weak	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev
	.set	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev,_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD2Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_121InspectableHeapObject3getEN2v85LocalINS2_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_121InspectableHeapObject3getEN2v85LocalINS2_7ContextEEE, @function
_ZN12v8_inspector12_GLOBAL__N_121InspectableHeapObject3getEN2v85LocalINS2_7ContextEEE:
.LFB7590:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movl	8(%r8), %r12d
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v87Isolate15GetHeapProfilerEv@PLT
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	_ZN2v812HeapProfiler14FindObjectByIdEj@PLT
	testq	%rax, %rax
	je	.L299
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L300
.L299:
	xorl	%r12d, %r12d
.L300:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7590:
	.size	_ZN12v8_inspector12_GLOBAL__N_121InspectableHeapObject3getEN2v85LocalINS2_7ContextEEE, .-_ZN12v8_inspector12_GLOBAL__N_121InspectableHeapObject3getEN2v85LocalINS2_7ContextEEE
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolver7GetNameEN2v85LocalINS2_6ObjectEEE.str1.1,"aMS",@progbits,1
.LC5:
	.string	""
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolver7GetNameEN2v85LocalINS2_6ObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolver7GetNameEN2v85LocalINS2_6ObjectEEE.str1.1
.LC7:
	.string	"basic_string::_M_create"
	.section	.text._ZN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolver7GetNameEN2v85LocalINS2_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolver7GetNameEN2v85LocalINS2_6ObjectEEE, @function
_ZN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolver7GetNameEN2v85LocalINS2_6ObjectEEE:
.LFB7565:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rbx), %rax
	movq	24(%rax), %r12
	call	_ZN2v86Object15CreationContextEv@PLT
	movq	%rax, %rdi
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	%r12, %rdi
	movl	%eax, %edx
	movq	40(%rbx), %rax
	movl	16(%rax), %esi
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEii@PLT
	testq	%rax, %rax
	je	.L319
	movq	32(%rax), %rcx
	movq	24(%rax), %r15
	movq	%rax, %r13
	leaq	-80(%rbp), %r12
	movq	%r12, -96(%rbp)
	leaq	(%rcx,%rcx), %r14
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L307
	testq	%r15, %r15
	je	.L335
.L307:
	movq	%r14, %r8
	movq	%r12, %rdi
	sarq	%r8
	cmpq	$14, %r14
	ja	.L336
	cmpq	$2, %r14
	je	.L337
.L310:
	testq	%r14, %r14
	je	.L311
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r8, -112(%rbp)
	movq	%rcx, -104(%rbp)
	call	memmove@PLT
	movq	-96(%rbp), %rdi
	movq	-112(%rbp), %r8
	movq	-104(%rbp), %rcx
.L311:
	xorl	%eax, %eax
	movq	%r8, -88(%rbp)
	movw	%ax, (%rdi,%rcx,2)
	movq	56(%r13), %rax
	leaq	.LC5(%rip), %r13
	movq	-88(%rbp), %rdi
	movq	8(%rbx), %rsi
	movq	%rax, -64(%rbp)
	movq	16(%rbx), %rdx
	movq	24(%rbx), %rax
	leaq	1(%rdi), %r8
	leaq	(%rsi,%r8), %rcx
	subq	%rdx, %rax
	cmpq	%rax, %rcx
	jnb	.L312
	testq	%rdi, %rdi
	je	.L313
	xorl	%eax, %eax
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L338:
	addq	$1, %rax
	movb	%cl, (%rdx,%rsi)
	cmpq	%rax, %rdi
	je	.L315
.L316:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
.L317:
	movq	-96(%rbp), %rcx
	addq	%rax, %rdx
	movzwl	(%rcx,%rax,2), %ecx
	cmpw	$255, %cx
	jbe	.L338
	addq	$1, %rax
	movb	$63, (%rdx,%rsi)
	cmpq	%rax, %rdi
	jne	.L316
.L315:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
.L313:
	addq	%rdi, %rdx
	movb	$0, (%rdx,%rsi)
	movq	8(%rbx), %rax
	movq	16(%rbx), %r13
	addq	%rax, %r8
	movq	%r8, 8(%rbx)
	addq	%rax, %r13
.L312:
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L305
	call	_ZdlPv@PLT
.L305:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L339
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L337:
	.cfi_restore_state
	movzwl	(%r15), %eax
	movw	%ax, (%rdi)
	movq	-96(%rbp), %rdi
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L336:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r8
	ja	.L340
	leaq	2(%r14), %rdi
	movq	%r8, -112(%rbp)
	movq	%rcx, -104(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %r8
	movq	-104(%rbp), %rcx
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	%r8, -80(%rbp)
	cmpq	$2, %r14
	jne	.L310
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L319:
	leaq	.LC5(%rip), %r13
	jmp	.L305
.L339:
	call	__stack_chk_fail@PLT
.L335:
	leaq	.LC6(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L340:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7565:
	.size	_ZN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolver7GetNameEN2v85LocalINS2_6ObjectEEE, .-_ZN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolver7GetNameEN2v85LocalINS2_6ObjectEEE
	.section	.text._ZN12v8_inspector23V8HeapProfilerAgentImpl24startTrackingHeapObjectsEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8HeapProfilerAgentImpl24startTrackingHeapObjectsEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE
	.type	_ZN12v8_inspector23V8HeapProfilerAgentImpl24startTrackingHeapObjectsEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE, @function
_ZN12v8_inspector23V8HeapProfilerAgentImpl24startTrackingHeapObjectsEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE:
.LFB7609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$72, %rsp
	movq	32(%rsi), %r14
	leaq	_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL26heapObjectsTrackingEnabledE(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -104(%rbp)
	cmpq	%rax, %rdi
	je	.L342
	call	_ZdlPv@PLT
.L342:
	xorl	%r14d, %r14d
	cmpb	$0, (%rbx)
	je	.L343
	movzbl	1(%rbx), %r14d
.L343:
	movq	32(%r12), %rbx
	leaq	_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL25allocationTrackingEnabledE(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movl	%r14d, %edx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-96(%rbp), %rdi
	cmpq	-104(%rbp), %rdi
	je	.L344
	call	_ZdlPv@PLT
.L344:
	movq	16(%r12), %rdi
	call	_ZN2v87Isolate15GetHeapProfilerEv@PLT
	movl	%r14d, %esi
	movq	%rax, %rdi
	call	_ZN2v812HeapProfiler24StartTrackingHeapObjectsEb@PLT
	cmpb	$0, 40(%r12)
	jne	.L346
	movq	8(%r12), %rax
	movb	$1, 40(%r12)
	leaq	_ZN12v8_inspector17V8InspectorClient19startRepeatingTimerEdPFvPvES1_(%rip), %rdx
	movq	24(%rax), %rax
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	184(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L351
.L346:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L352
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L351:
	.cfi_restore_state
	movsd	.LC8(%rip), %xmm0
	movq	%r12, %rdx
	leaq	_ZN12v8_inspector23V8HeapProfilerAgentImpl7onTimerEPv(%rip), %rsi
	call	*%rax
	jmp	.L346
.L352:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7609:
	.size	_ZN12v8_inspector23V8HeapProfilerAgentImpl24startTrackingHeapObjectsEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE, .-_ZN12v8_inspector23V8HeapProfilerAgentImpl24startTrackingHeapObjectsEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD0Ev
	.type	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD0Ev, @function
_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD0Ev:
.LFB5584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %r15
	movq	%rax, (%rdi)
	testq	%r15, %r15
	je	.L354
	movq	8(%r15), %rbx
	movq	(%r15), %r13
	cmpq	%r13, %rbx
	je	.L355
	leaq	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev(%rip), %r14
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L401:
	movl	$32, %esi
	call	_ZdlPvm@PLT
.L356:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L400
.L358:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L356
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	je	.L401
	call	*%rax
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L358
	.p2align 4,,10
	.p2align 3
.L400:
	movq	(%r15), %r13
.L355:
	testq	%r13, %r13
	je	.L359
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L359:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L354:
	movq	8(%r12), %r15
	testq	%r15, %r15
	je	.L360
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L361
	movq	32(%r15), %r14
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r14, %r14
	je	.L362
	movq	8(%r14), %rax
	movq	(%r14), %rbx
	movq	%rax, -56(%rbp)
	cmpq	%rbx, %rax
	jne	.L366
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L403:
	call	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev
	movl	$40, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L364:
	addq	$8, %rbx
	cmpq	%rbx, -56(%rbp)
	je	.L402
.L366:
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L364
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L403
	call	*%rax
	addq	$8, %rbx
	cmpq	%rbx, -56(%rbp)
	jne	.L366
	.p2align 4,,10
	.p2align 3
.L402:
	movq	(%r14), %rbx
.L363:
	testq	%rbx, %rbx
	je	.L367
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L367:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L362:
	movq	8(%r15), %r13
	testq	%r13, %r13
	je	.L368
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L369
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	104(%r13), %rax
	cmpq	%rax, %rdi
	je	.L370
	call	_ZdlPv@PLT
.L370:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L371
	call	_ZdlPv@PLT
.L371:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L372
	call	_ZdlPv@PLT
.L372:
	movl	$136, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L368:
	movl	$40, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L360:
	addq	$24, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L361:
	.cfi_restore_state
	movq	%r15, %rdi
	call	*%rax
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L369:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L368
	.cfi_endproc
.LFE5584:
	.size	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD0Ev, .-_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD0Ev
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev
	.type	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev, @function
_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev:
.LFB5515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	32(%rdi), %r15
	movq	%rax, (%rdi)
	testq	%r15, %r15
	je	.L405
	movq	8(%r15), %rbx
	movq	(%r15), %r12
	cmpq	%r12, %rbx
	jne	.L409
	testq	%r12, %r12
	je	.L410
	.p2align 4,,10
	.p2align 3
.L432:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L410:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L405:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L411
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L412
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r12), %rdi
	movq	%rax, (%r12)
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L413
	call	_ZdlPv@PLT
.L413:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L414
	call	_ZdlPv@PLT
.L414:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L415
	call	_ZdlPv@PLT
.L415:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L411:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$40, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L431:
	.cfi_restore_state
	call	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev
	movl	$40, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L407:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L430
.L409:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L407
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rcx
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L431
	call	*%rax
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L409
	.p2align 4,,10
	.p2align 3
.L430:
	movq	(%r15), %r12
	testq	%r12, %r12
	jne	.L432
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L412:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L411
	.cfi_endproc
.LFE5515:
	.size	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev, .-_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD2Ev
	.type	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD2Ev, @function
_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD2Ev:
.LFB5582:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r15
	movq	%rax, (%rdi)
	testq	%r15, %r15
	je	.L434
	movq	8(%r15), %r13
	movq	(%r15), %r12
	cmpq	%r12, %r13
	je	.L435
	leaq	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev(%rip), %r14
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L478:
	movl	$32, %esi
	call	_ZdlPvm@PLT
.L436:
	addq	$8, %r12
	cmpq	%r12, %r13
	je	.L477
.L438:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L436
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	je	.L478
	call	*%rax
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L438
	.p2align 4,,10
	.p2align 3
.L477:
	movq	(%r15), %r12
.L435:
	testq	%r12, %r12
	je	.L439
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L439:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L434:
	movq	8(%rbx), %r15
	testq	%r15, %r15
	je	.L433
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L441
	movq	32(%r15), %r14
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r14, %r14
	je	.L442
	movq	8(%r14), %rax
	movq	(%r14), %r13
	movq	%rax, -56(%rbp)
	cmpq	%r13, %rax
	jne	.L446
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L480:
	call	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L444:
	addq	$8, %r13
	cmpq	%r13, -56(%rbp)
	je	.L479
.L446:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L444
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	je	.L480
	call	*%rax
	addq	$8, %r13
	cmpq	%r13, -56(%rbp)
	jne	.L446
	.p2align 4,,10
	.p2align 3
.L479:
	movq	(%r14), %r13
.L443:
	testq	%r13, %r13
	je	.L447
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L447:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L442:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L448
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L449
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r12), %rdi
	movq	%rax, (%r12)
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L450
	call	_ZdlPv@PLT
.L450:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L451
	call	_ZdlPv@PLT
.L451:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L452
	call	_ZdlPv@PLT
.L452:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L448:
	addq	$24, %rsp
	movq	%r15, %rdi
	movl	$40, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L433:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L441:
	.cfi_restore_state
	addq	$24, %rsp
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L449:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L448
	.cfi_endproc
.LFE5582:
	.size	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD2Ev, .-_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD2Ev
	.weak	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD1Ev
	.set	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD1Ev,_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev:
.LFB4640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	160(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L482
	movq	8(%r13), %rax
	movq	0(%r13), %r12
	movq	%rax, -64(%rbp)
	cmpq	%r12, %rax
	je	.L483
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %r14
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L538:
	movq	16(%r15), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%rdi, %rdi
	je	.L486
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	jne	.L487
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L486:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L488
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	jne	.L489
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L488:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L484:
	addq	$8, %r12
	cmpq	%r12, -64(%rbp)
	je	.L537
.L490:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L484
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L538
	movq	%r15, %rdi
	addq	$8, %r12
	call	*%rax
	cmpq	%r12, -64(%rbp)
	jne	.L490
	.p2align 4,,10
	.p2align 3
.L537:
	movq	0(%r13), %r12
.L483:
	testq	%r12, %r12
	je	.L491
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L491:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L482:
	movq	152(%rbx), %r13
	testq	%r13, %r13
	je	.L492
	movq	8(%r13), %r14
	movq	0(%r13), %r12
	cmpq	%r12, %r14
	jne	.L502
	testq	%r12, %r12
	je	.L503
	.p2align 4,,10
	.p2align 3
.L541:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L503:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L492:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L504
	call	_ZdlPv@PLT
.L504:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L505
	call	_ZdlPv@PLT
.L505:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L481
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L540:
	.cfi_restore_state
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L496
	call	_ZdlPv@PLT
.L496:
	movq	136(%r15), %rdi
	testq	%rdi, %rdi
	je	.L497
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L498
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L497:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L499
	call	_ZdlPv@PLT
.L499:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L500
	call	_ZdlPv@PLT
.L500:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L501
	call	_ZdlPv@PLT
.L501:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L494:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L539
.L502:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L494
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L540
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rax
	cmpq	%r12, %r14
	jne	.L502
	.p2align 4,,10
	.p2align 3
.L539:
	movq	0(%r13), %r12
	testq	%r12, %r12
	jne	.L541
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L481:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L489:
	.cfi_restore_state
	call	*%rax
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L487:
	call	*%rax
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L498:
	call	*%rax
	jmp	.L497
	.cfi_endproc
.LFE4640:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev
	.section	.rodata._ZN12v8_inspector23V8HeapProfilerAgentImpl22addInspectedHeapObjectERKNS_8String16E.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"Invalid heap snapshot object id"
	.section	.rodata._ZN12v8_inspector23V8HeapProfilerAgentImpl22addInspectedHeapObjectERKNS_8String16E.str1.1,"aMS",@progbits,1
.LC10:
	.string	"Object is not available"
	.section	.text._ZN12v8_inspector23V8HeapProfilerAgentImpl22addInspectedHeapObjectERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8HeapProfilerAgentImpl22addInspectedHeapObjectERKNS_8String16E
	.type	_ZN12v8_inspector23V8HeapProfilerAgentImpl22addInspectedHeapObjectERKNS_8String16E, @function
_ZN12v8_inspector23V8HeapProfilerAgentImpl22addInspectedHeapObjectERKNS_8String16E:
.LFB7643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	-137(%rbp), %rsi
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8String169toIntegerEPb@PLT
	cmpb	$0, -137(%rbp)
	jne	.L543
	leaq	-96(%rbp), %r13
	leaq	.LC9(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L542
	call	_ZdlPv@PLT
.L542:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L567
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L543:
	.cfi_restore_state
	movq	16(%rbx), %rsi
	leaq	-128(%rbp), %r15
	movl	%eax, %r13d
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%rbx), %rdi
	call	_ZN2v87Isolate15GetHeapProfilerEv@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	_ZN2v812HeapProfiler14FindObjectByIdEj@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L566
	movq	%rax, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L566
	movq	8(%rbx), %r8
	leaq	_ZN12v8_inspector17V8InspectorClient23isInspectableHeapObjectEN2v85LocalINS1_6ObjectEEE(%rip), %rdx
	movq	24(%r8), %rax
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	88(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L568
.L554:
	movq	(%r8), %rax
	movl	$16, %edi
	movq	%r8, -152(%rbp)
	movq	16(%rax), %rbx
	call	_Znwm@PLT
	movq	-152(%rbp), %r8
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_121InspectableHeapObjectE(%rip), %rcx
	leaq	-136(%rbp), %rsi
	movq	%rcx, (%rax)
	movq	%r8, %rdi
	movl	%r13d, 8(%rax)
	movq	%rax, -136(%rbp)
	call	*%rbx
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L551
	movq	(%rdi), %rax
	call	*16(%rax)
.L551:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
.L548:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L568:
	movq	%r14, %rsi
	call	*%rax
	testb	%al, %al
	jne	.L549
	.p2align 4,,10
	.p2align 3
.L566:
	leaq	-96(%rbp), %r13
	leaq	.LC10(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L548
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L549:
	movq	8(%rbx), %r8
	jmp	.L554
.L567:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7643:
	.size	_ZN12v8_inspector23V8HeapProfilerAgentImpl22addInspectedHeapObjectERKNS_8String16E, .-_ZN12v8_inspector23V8HeapProfilerAgentImpl22addInspectedHeapObjectERKNS_8String16E
	.section	.text._ZN12v8_inspector23V8HeapProfilerAgentImpl7onTimerEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8HeapProfilerAgentImpl7onTimerEPv
	.type	_ZN12v8_inspector23V8HeapProfilerAgentImpl7onTimerEPv, @function
_ZN12v8_inspector23V8HeapProfilerAgentImpl7onTimerEPv:
.LFB7654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	24(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_115HeapStatsStreamE(%rip), %rax
	movq	%r12, -40(%rbp)
	movq	%rax, -48(%rbp)
	call	_ZN2v87Isolate15GetHeapProfilerEv@PLT
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, %rdi
	call	_ZN2v812HeapProfiler12GetHeapStatsEPNS_12OutputStreamEPl@PLT
	leaq	_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv(%rip), %rdx
	pxor	%xmm0, %xmm0
	movl	%eax, %esi
	movq	8(%rbx), %rax
	movq	24(%rax), %rax
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	176(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L575
.L570:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12HeapProfiler8Frontend16lastSeenObjectIdEid@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L576
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L575:
	.cfi_restore_state
	movl	%esi, -52(%rbp)
	call	*%rax
	movl	-52(%rbp), %esi
	jmp	.L570
.L576:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7654:
	.size	_ZN12v8_inspector23V8HeapProfilerAgentImpl7onTimerEPv, .-_ZN12v8_inspector23V8HeapProfilerAgentImpl7onTimerEPv
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, @function
_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev:
.LFB4459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	312(%rdi), %r12
	movups	%xmm0, (%rdi)
	testq	%r12, %r12
	je	.L578
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L579
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	56(%r12), %rdi
	movq	%rax, (%r12)
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L580
	call	_ZdlPv@PLT
.L580:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L581
	call	_ZdlPv@PLT
.L581:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L578:
	movq	304(%r15), %rcx
	movq	%rcx, -72(%rbp)
	testq	%rcx, %rcx
	je	.L582
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L583
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L584
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -104(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rdx
	je	.L585
	movq	%r15, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L892:
	movq	-56(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -88(%rbp)
	testq	%rcx, %rcx
	je	.L586
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L587
	movq	16(%rcx), %rbx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	testq	%rbx, %rbx
	je	.L588
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L589
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L590
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -64(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L591
	movq	%rbx, -112(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L648:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L592
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L593
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L594
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L595
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L596
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	cmpq	%r14, %rcx
	je	.L597
	movq	%rbx, -136(%rbp)
	movq	%r14, %rbx
	movq	%rcx, %r14
	movq	%r12, -128(%rbp)
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L1682:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L600
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L601
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L600:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L602
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L603
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L602:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L598:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L1681
.L604:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L598
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1682
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L1713:
	movq	-112(%rbp), %rax
	movq	-128(%rbp), %r12
	movq	-136(%rbp), %r13
	movq	(%rax), %r14
.L870:
	testq	%r14, %r14
	je	.L880
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L880:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L869:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L881
	call	_ZdlPv@PLT
.L881:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L882
	call	_ZdlPv@PLT
.L882:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L883
	call	_ZdlPv@PLT
.L883:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L857:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L884
	call	_ZdlPv@PLT
.L884:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L885
	call	_ZdlPv@PLT
.L885:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L886
	call	_ZdlPv@PLT
.L886:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L854:
	addq	$8, %rbx
	cmpq	%rbx, -64(%rbp)
	jne	.L887
	movq	-96(%rbp), %rax
	movq	(%rax), %rbx
.L853:
	testq	%rbx, %rbx
	je	.L888
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L888:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L852:
	movq	-80(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L889
	call	_ZdlPv@PLT
.L889:
	movq	-80(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L890
	call	_ZdlPv@PLT
.L890:
	movq	-80(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L891
	call	_ZdlPv@PLT
.L891:
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L715:
	movq	-88(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L586:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -104(%rbp)
	jne	.L892
	movq	-120(%rbp), %rax
	movq	-160(%rbp), %r15
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
.L585:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L893
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L893:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L584:
	movq	-72(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L894
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -96(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rsi
	je	.L895
	movq	%r15, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L1079:
	movq	-56(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L896
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L897
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L898
	call	_ZdlPv@PLT
.L898:
	movq	136(%rbx), %rsi
	movq	%rsi, -80(%rbp)
	testq	%rsi, %rsi
	je	.L899
	movq	(%rsi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L900
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rsi)
	movq	160(%rsi), %rax
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L901
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -104(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rcx
	je	.L902
	movq	%rbx, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L1009:
	movq	-64(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L903
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L904
	movq	16(%rbx), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%r12, %r12
	je	.L905
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L906
	movq	160(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L907
	movq	8(%r13), %rax
	movq	0(%r13), %r15
	cmpq	%r15, %rax
	je	.L908
	movq	%rbx, -88(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -120(%rbp)
	jmp	.L915
	.p2align 4,,10
	.p2align 3
.L1684:
	movq	16(%r14), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r12, %r12
	je	.L911
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L912
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L911:
	movq	8(%r14), %r12
	testq	%r12, %r12
	je	.L913
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L914
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L913:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L909:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L1683
.L915:
	movq	(%rbx), %r14
	testq	%r14, %r14
	je	.L909
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1684
	movq	%r14, %rdi
	call	*%rdx
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L1699:
	movq	-120(%rbp), %r13
	movq	0(%r13), %r12
.L1054:
	testq	%r12, %r12
	je	.L1064
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1064:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1053:
	movq	-88(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L1065
	call	_ZdlPv@PLT
.L1065:
	movq	-88(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L1066
	call	_ZdlPv@PLT
.L1066:
	movq	-88(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1067
	call	_ZdlPv@PLT
.L1067:
	movq	-88(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1016:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1068
	call	_ZdlPv@PLT
.L1068:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1069
	call	_ZdlPv@PLT
.L1069:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1070
	call	_ZdlPv@PLT
.L1070:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1013:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -104(%rbp)
	jne	.L1071
	movq	-128(%rbp), %rax
	movq	-160(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L1012:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L1072
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1072:
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1011:
	movq	-80(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L1073
	call	_ZdlPv@PLT
.L1073:
	movq	-80(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L1074
	call	_ZdlPv@PLT
.L1074:
	movq	-80(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1075
	call	_ZdlPv@PLT
.L1075:
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L899:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1076
	call	_ZdlPv@PLT
.L1076:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1077
	call	_ZdlPv@PLT
.L1077:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1078
	call	_ZdlPv@PLT
.L1078:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L896:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -96(%rbp)
	jne	.L1079
	movq	-112(%rbp), %rax
	movq	-152(%rbp), %r15
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
.L895:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L1080
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1080:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L894:
	movq	-72(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L1081
	call	_ZdlPv@PLT
.L1081:
	movq	-72(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L1082
	call	_ZdlPv@PLT
.L1082:
	movq	-72(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1083
	call	_ZdlPv@PLT
.L1083:
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L582:
	movq	264(%r15), %rdi
	leaq	280(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1084
	call	_ZdlPv@PLT
.L1084:
	movq	216(%r15), %rdi
	leaq	232(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1085
	call	_ZdlPv@PLT
.L1085:
	movq	168(%r15), %rdi
	leaq	184(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1086
	call	_ZdlPv@PLT
.L1086:
	movq	152(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1087
	movq	(%rdi), %rax
	call	*24(%rax)
.L1087:
	movq	112(%r15), %rdi
	leaq	128(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1088
	call	_ZdlPv@PLT
.L1088:
	movq	64(%r15), %rdi
	leaq	80(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1089
	call	_ZdlPv@PLT
.L1089:
	movq	16(%r15), %rdi
	leaq	32(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1090
	call	_ZdlPv@PLT
.L1090:
	addq	$152, %rsp
	movq	%r15, %rdi
	movl	$320, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1721:
	.cfi_restore_state
	movq	-144(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L987:
	testq	%r14, %r14
	je	.L997
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L997:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L986:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L998
	call	_ZdlPv@PLT
.L998:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L999
	call	_ZdlPv@PLT
.L999:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1000
	call	_ZdlPv@PLT
.L1000:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L974:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1001
	call	_ZdlPv@PLT
.L1001:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1002
	call	_ZdlPv@PLT
.L1002:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1003
	call	_ZdlPv@PLT
.L1003:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L971:
	addq	$8, %rbx
	cmpq	%rbx, -120(%rbp)
	jne	.L1004
	movq	-136(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L970:
	testq	%rdi, %rdi
	je	.L1005
	call	_ZdlPv@PLT
.L1005:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L969:
	movq	-88(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L1006
	call	_ZdlPv@PLT
.L1006:
	movq	-88(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L1007
	call	_ZdlPv@PLT
.L1007:
	movq	-88(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1008
	call	_ZdlPv@PLT
.L1008:
	movq	-88(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L932:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L903:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -104(%rbp)
	jne	.L1009
	movq	-128(%rbp), %rax
	movq	-160(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L902:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L1010
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1010:
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L901:
	movq	-80(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L1011
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -104(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rdx
	je	.L1012
	movq	%rbx, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L1071:
	movq	-64(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1013
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1014
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1015
	call	_ZdlPv@PLT
.L1015:
	movq	136(%rbx), %rcx
	movq	%rcx, -88(%rbp)
	testq	%rcx, %rcx
	je	.L1016
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1017
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L1018
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -120(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L1019
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1051:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1020
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1021
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1022
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1023
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L1024
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L1025
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -144(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1686:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1028
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1029
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1028:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1030
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1031
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1030:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1026:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L1685
.L1032:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1026
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1686
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L593:
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L592:
	addq	$8, %rbx
	cmpq	%rbx, -64(%rbp)
	jne	.L648
	movq	-80(%rbp), %rax
	movq	-112(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L591:
	testq	%rdi, %rdi
	je	.L649
	call	_ZdlPv@PLT
.L649:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L590:
	movq	152(%rbx), %rax
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L650
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -96(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rcx
	je	.L651
	movq	%rbx, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L710:
	movq	-64(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L652
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L653
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L654
	call	_ZdlPv@PLT
.L654:
	movq	136(%rbx), %rdx
	movq	%rdx, -80(%rbp)
	testq	%rdx, %rdx
	je	.L655
	movq	(%rdx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L656
	movq	160(%rdx), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rdx)
	testq	%r12, %r12
	je	.L657
	movq	8(%r12), %r15
	movq	(%r12), %r14
	cmpq	%r14, %r15
	je	.L658
	movq	%r12, -112(%rbp)
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L1688:
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L661
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L662
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L661:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L663
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L664
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L663:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L659:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1687
.L665:
	movq	(%r14), %r13
	testq	%r13, %r13
	je	.L659
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1688
	movq	%r13, %rdi
	call	*%rax
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L1717:
	movq	-144(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L685:
	testq	%r14, %r14
	je	.L695
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L695:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L684:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L696
	call	_ZdlPv@PLT
.L696:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L697
	call	_ZdlPv@PLT
.L697:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L698
	call	_ZdlPv@PLT
.L698:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L672:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L699
	call	_ZdlPv@PLT
.L699:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L700
	call	_ZdlPv@PLT
.L700:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L701
	call	_ZdlPv@PLT
.L701:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L669:
	addq	$8, %rbx
	cmpq	%rbx, -112(%rbp)
	jne	.L702
	movq	-136(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L668:
	testq	%rdi, %rdi
	je	.L703
	call	_ZdlPv@PLT
.L703:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L667:
	movq	-80(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L704
	call	_ZdlPv@PLT
.L704:
	movq	-80(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L705
	call	_ZdlPv@PLT
.L705:
	movq	-80(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L706
	call	_ZdlPv@PLT
.L706:
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L655:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L707
	call	_ZdlPv@PLT
.L707:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L708
	call	_ZdlPv@PLT
.L708:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L709
	call	_ZdlPv@PLT
.L709:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L652:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -96(%rbp)
	jne	.L710
	movq	-128(%rbp), %rax
	movq	-152(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L651:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L711
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L711:
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L650:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L712
	call	_ZdlPv@PLT
.L712:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L713
	call	_ZdlPv@PLT
.L713:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L714
	call	_ZdlPv@PLT
.L714:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L588:
	movq	-88(%rbp), %rax
	movq	8(%rax), %rcx
	movq	%rcx, -80(%rbp)
	testq	%rcx, %rcx
	je	.L715
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L716
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L717
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -112(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rdx
	je	.L718
	.p2align 4,,10
	.p2align 3
.L850:
	movq	-64(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L719
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L720
	movq	16(%rbx), %rcx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	%rcx, -96(%rbp)
	testq	%rcx, %rcx
	je	.L721
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L722
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L723
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -128(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L724
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L756:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L725
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L726
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L727
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L728
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L727:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L729
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L730
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L731
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L732
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -152(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L739
	.p2align 4,,10
	.p2align 3
.L1690:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L735
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L736
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L735:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L737
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L738
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L737:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L733:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L1689
.L739:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L733
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1690
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L720:
	movq	%rbx, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L719:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -112(%rbp)
	jne	.L850
	movq	-136(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L718:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L851
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L851:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L717:
	movq	-80(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L852
	movq	8(%rax), %rsi
	movq	(%rax), %rbx
	movq	%rsi, -64(%rbp)
	cmpq	%rbx, %rsi
	je	.L853
	.p2align 4,,10
	.p2align 3
.L887:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L854
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L855
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L856
	call	_ZdlPv@PLT
.L856:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L857
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L858
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L859
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L860
	movq	%rbx, -128(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -112(%rbp)
	movq	%r13, -136(%rbp)
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L1692:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L863
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L864
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L863:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L865
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L866
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L865:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L861:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L1691
.L867:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L861
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1692
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L726:
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L725:
	addq	$8, %rbx
	cmpq	%rbx, -128(%rbp)
	jne	.L756
	movq	-144(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L724:
	testq	%rdi, %rdi
	je	.L757
	call	_ZdlPv@PLT
.L757:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L723:
	movq	-96(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L758
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -128(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L759
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L793:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L760
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L761
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L762
	call	_ZdlPv@PLT
.L762:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L763
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L764
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L765
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L766
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -152(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L773
	.p2align 4,,10
	.p2align 3
.L1694:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L769
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L770
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L769:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L771
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L772
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L771:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L767:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L1693
.L773:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L767
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1694
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L1725:
	movq	-152(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L776:
	testq	%r14, %r14
	je	.L786
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L786:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L775:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L787
	call	_ZdlPv@PLT
.L787:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L788
	call	_ZdlPv@PLT
.L788:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L789
	call	_ZdlPv@PLT
.L789:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L763:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L790
	call	_ZdlPv@PLT
.L790:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L791
	call	_ZdlPv@PLT
.L791:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L792
	call	_ZdlPv@PLT
.L792:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L760:
	addq	$8, %rbx
	cmpq	%rbx, -128(%rbp)
	jne	.L793
	movq	-144(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L759:
	testq	%rdi, %rdi
	je	.L794
	call	_ZdlPv@PLT
.L794:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L758:
	movq	-96(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L795
	call	_ZdlPv@PLT
.L795:
	movq	-96(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L796
	call	_ZdlPv@PLT
.L796:
	movq	-96(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L797
	call	_ZdlPv@PLT
.L797:
	movq	-96(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L721:
	movq	8(%rbx), %rdx
	movq	%rdx, -96(%rbp)
	testq	%rdx, %rdx
	je	.L798
	movq	(%rdx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L799
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rdx)
	movq	160(%rdx), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L800
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -128(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L801
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L833:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L802
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L803
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L804
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L805
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L804:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L806
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L807
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L808
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L809
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -152(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L1696:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L812
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L813
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L812:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L814
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L815
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L814:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L810:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L1695
.L816:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L810
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1696
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L803:
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L802:
	addq	$8, %rbx
	cmpq	%rbx, -128(%rbp)
	jne	.L833
	movq	-144(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L801:
	testq	%rdi, %rdi
	je	.L834
	call	_ZdlPv@PLT
.L834:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L800:
	movq	-96(%rbp), %rax
	movq	152(%rax), %r13
	testq	%r13, %r13
	je	.L835
	movq	8(%r13), %r15
	movq	0(%r13), %r12
	cmpq	%r12, %r15
	je	.L836
	movq	%r13, -128(%rbp)
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L1698:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r14), %rdi
	movq	%rax, (%r14)
	leaq	168(%r14), %rax
	cmpq	%rax, %rdi
	je	.L839
	call	_ZdlPv@PLT
.L839:
	movq	136(%r14), %r13
	testq	%r13, %r13
	je	.L840
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L841
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L840:
	movq	96(%r14), %rdi
	leaq	112(%r14), %rax
	cmpq	%rax, %rdi
	je	.L842
	call	_ZdlPv@PLT
.L842:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L843
	call	_ZdlPv@PLT
.L843:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L844
	call	_ZdlPv@PLT
.L844:
	movl	$192, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L837:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L1697
.L845:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L837
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1698
	movq	%r14, %rdi
	call	*%rax
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L1719:
	movq	-144(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L1035:
	testq	%r14, %r14
	je	.L1045
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1045:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1034:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1046
	call	_ZdlPv@PLT
.L1046:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1047
	call	_ZdlPv@PLT
.L1047:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1048
	call	_ZdlPv@PLT
.L1048:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1022:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1049
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1050
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1049:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1020:
	addq	$8, %rbx
	cmpq	%rbx, -120(%rbp)
	jne	.L1051
	movq	-136(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L1019:
	testq	%rdi, %rdi
	je	.L1052
	call	_ZdlPv@PLT
.L1052:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1018:
	movq	-88(%rbp), %rax
	movq	152(%rax), %r13
	testq	%r13, %r13
	je	.L1053
	movq	8(%r13), %r15
	movq	0(%r13), %r12
	cmpq	%r12, %r15
	je	.L1054
	movq	%r13, -120(%rbp)
	jmp	.L1063
	.p2align 4,,10
	.p2align 3
.L1700:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r14), %rdi
	movq	%rax, (%r14)
	leaq	168(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1057
	call	_ZdlPv@PLT
.L1057:
	movq	136(%r14), %r13
	testq	%r13, %r13
	je	.L1058
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1059
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1058:
	movq	96(%r14), %rdi
	leaq	112(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1060
	call	_ZdlPv@PLT
.L1060:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1061
	call	_ZdlPv@PLT
.L1061:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1062
	call	_ZdlPv@PLT
.L1062:
	movl	$192, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1055:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L1699
.L1063:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L1055
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1700
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1055
	.p2align 4,,10
	.p2align 3
.L1697:
	movq	-128(%rbp), %r13
	movq	0(%r13), %r12
.L836:
	testq	%r12, %r12
	je	.L846
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L846:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L835:
	movq	-96(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L847
	call	_ZdlPv@PLT
.L847:
	movq	-96(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L848
	call	_ZdlPv@PLT
.L848:
	movq	-96(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L849
	call	_ZdlPv@PLT
.L849:
	movq	-96(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L798:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L1709:
	movq	-120(%rbp), %r14
	movq	-88(%rbp), %r12
	movq	(%r14), %r13
.L918:
	testq	%r13, %r13
	je	.L928
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L928:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L917:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L929
	call	_ZdlPv@PLT
.L929:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L930
	call	_ZdlPv@PLT
.L930:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L931
	call	_ZdlPv@PLT
.L931:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L905:
	movq	8(%rbx), %rsi
	movq	%rsi, -88(%rbp)
	testq	%rsi, %rsi
	je	.L932
	movq	(%rsi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L933
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rsi)
	movq	160(%rsi), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L934
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -120(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L935
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L967:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L936
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L937
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L938
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L939
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L938:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L940
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L941
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L942
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L943
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -144(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L1702:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L946
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L947
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L946:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L948
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L949
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L948:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L944:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L1701
.L950:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L944
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1702
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L937:
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L936:
	addq	$8, %rbx
	cmpq	%rbx, -120(%rbp)
	jne	.L967
	movq	-136(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L935:
	testq	%rdi, %rdi
	je	.L968
	call	_ZdlPv@PLT
.L968:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L934:
	movq	-88(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L969
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -120(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L970
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1004:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L971
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L972
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L973
	call	_ZdlPv@PLT
.L973:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L974
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L975
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L976
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L977
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -144(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1704:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L980
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L981
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L980:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L982
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L983
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L982:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L978:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L1703
.L984:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L978
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1704
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L1715:
	movq	-96(%rbp), %rax
	movq	-128(%rbp), %r12
	movq	-136(%rbp), %r13
	movq	(%rax), %r14
.L607:
	testq	%r14, %r14
	je	.L617
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L617:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L606:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L618
	call	_ZdlPv@PLT
.L618:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L619
	call	_ZdlPv@PLT
.L619:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L620
	call	_ZdlPv@PLT
.L620:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L594:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L621
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L622
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L623
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	cmpq	%r14, %rdx
	je	.L624
	movq	%rbx, -136(%rbp)
	movq	%r14, %rbx
	movq	%rdx, %r14
	movq	%r12, -128(%rbp)
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L1706:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L627
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L628
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L627:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L629
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L630
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L629:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L625:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L1705
.L631:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L625
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1706
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L1711:
	movq	-96(%rbp), %rax
	movq	-128(%rbp), %r12
	movq	-136(%rbp), %r13
	movq	(%rax), %r14
.L634:
	testq	%r14, %r14
	je	.L644
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L644:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L633:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L645
	call	_ZdlPv@PLT
.L645:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L646
	call	_ZdlPv@PLT
.L646:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L647
	call	_ZdlPv@PLT
.L647:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L621:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L1729:
	movq	-152(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L742:
	testq	%r14, %r14
	je	.L752
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L752:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L741:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L753
	call	_ZdlPv@PLT
.L753:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L754
	call	_ZdlPv@PLT
.L754:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L755
	call	_ZdlPv@PLT
.L755:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L729:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L1727:
	movq	-144(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L953:
	testq	%r14, %r14
	je	.L963
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L963:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L952:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L964
	call	_ZdlPv@PLT
.L964:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L965
	call	_ZdlPv@PLT
.L965:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L966
	call	_ZdlPv@PLT
.L966:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L940:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L936
	.p2align 4,,10
	.p2align 3
.L1723:
	movq	-152(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L819:
	testq	%r14, %r14
	je	.L829
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L829:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L818:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L830
	call	_ZdlPv@PLT
.L830:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L831
	call	_ZdlPv@PLT
.L831:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L832
	call	_ZdlPv@PLT
.L832:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L806:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L1687:
	movq	-112(%rbp), %r12
	movq	(%r12), %r14
.L658:
	testq	%r14, %r14
	je	.L666
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L666:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L657:
	movq	-80(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L667
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -112(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L668
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L702:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L669
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L670
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L671
	call	_ZdlPv@PLT
.L671:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L672
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L673
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L674
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L675
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -144(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L1708:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L678
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L679
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L678:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L680
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L681
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L680:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L676:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L1707
.L682:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L676
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1708
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L1683:
	movq	-88(%rbp), %rbx
	movq	-120(%rbp), %r12
	movq	0(%r13), %r15
.L908:
	testq	%r15, %r15
	je	.L916
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L916:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L907:
	movq	152(%r12), %r14
	testq	%r14, %r14
	je	.L917
	movq	8(%r14), %r15
	movq	(%r14), %r13
	cmpq	%r13, %r15
	je	.L918
	movq	%r12, -88(%rbp)
	movq	%r14, -120(%rbp)
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L1710:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L921
	call	_ZdlPv@PLT
.L921:
	movq	136(%r12), %r14
	testq	%r14, %r14
	je	.L922
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L923
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L922:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L924
	call	_ZdlPv@PLT
.L924:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L925
	call	_ZdlPv@PLT
.L925:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L926
	call	_ZdlPv@PLT
.L926:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L919:
	addq	$8, %r13
	cmpq	%r13, %r15
	je	.L1709
.L927:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L919
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1710
	movq	%r12, %rdi
	call	*%rax
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L1705:
	movq	-96(%rbp), %rax
	movq	-128(%rbp), %r12
	movq	-136(%rbp), %rbx
	movq	(%rax), %r14
.L624:
	testq	%r14, %r14
	je	.L632
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L632:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L623:
	movq	152(%r13), %rax
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L633
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L634
	movq	%r12, -128(%rbp)
	movq	%r13, -136(%rbp)
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L1712:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L637
	call	_ZdlPv@PLT
.L637:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L638
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L639
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L638:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L640
	call	_ZdlPv@PLT
.L640:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L641
	call	_ZdlPv@PLT
.L641:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L642
	call	_ZdlPv@PLT
.L642:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L635:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1711
.L643:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L635
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1712
	movq	%r12, %rdi
	call	*%rax
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L1691:
	movq	-112(%rbp), %r12
	movq	-128(%rbp), %rbx
	movq	-136(%rbp), %r13
	movq	(%r14), %r15
.L860:
	testq	%r15, %r15
	je	.L868
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L868:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L859:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L869
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L870
	movq	%r12, -128(%rbp)
	movq	%r13, -136(%rbp)
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L1714:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L873
	call	_ZdlPv@PLT
.L873:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L874
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L875
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L874:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L876
	call	_ZdlPv@PLT
.L876:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L877
	call	_ZdlPv@PLT
.L877:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L878
	call	_ZdlPv@PLT
.L878:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L871:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1713
.L879:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L871
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1714
	movq	%r12, %rdi
	call	*%rax
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L1681:
	movq	-96(%rbp), %rax
	movq	-128(%rbp), %r12
	movq	-136(%rbp), %rbx
	movq	(%rax), %r14
.L597:
	testq	%r14, %r14
	je	.L605
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L605:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L596:
	movq	152(%r13), %rax
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L606
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L607
	movq	%r12, -128(%rbp)
	movq	%r13, -136(%rbp)
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L1716:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L610
	call	_ZdlPv@PLT
.L610:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L611
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L612
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L611:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L613
	call	_ZdlPv@PLT
.L613:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L614
	call	_ZdlPv@PLT
.L614:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L615
	call	_ZdlPv@PLT
.L615:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L608:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1715
.L616:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L608
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1716
	movq	%r12, %rdi
	call	*%rax
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L1707:
	movq	-144(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L675:
	testq	%r15, %r15
	je	.L683
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L683:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L674:
	movq	152(%r13), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L684
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L685
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L694
	.p2align 4,,10
	.p2align 3
.L1718:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L688
	call	_ZdlPv@PLT
.L688:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L689
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L690
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L689:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L691
	call	_ZdlPv@PLT
.L691:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L692
	call	_ZdlPv@PLT
.L692:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L693
	call	_ZdlPv@PLT
.L693:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L686:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1717
.L694:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L686
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1718
	movq	%r12, %rdi
	call	*%rax
	jmp	.L686
	.p2align 4,,10
	.p2align 3
.L1685:
	movq	-144(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L1025:
	testq	%r15, %r15
	je	.L1033
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1033:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1024:
	movq	152(%r13), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L1034
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1035
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1720:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1038
	call	_ZdlPv@PLT
.L1038:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1039
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1040
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1039:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1041
	call	_ZdlPv@PLT
.L1041:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1042
	call	_ZdlPv@PLT
.L1042:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1043
	call	_ZdlPv@PLT
.L1043:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1036:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1719
.L1044:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1036
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1720
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1036
	.p2align 4,,10
	.p2align 3
.L1703:
	movq	-144(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L977:
	testq	%r15, %r15
	je	.L985
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L985:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L976:
	movq	152(%r13), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L986
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L987
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1722:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L990
	call	_ZdlPv@PLT
.L990:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L991
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L992
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L991:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L993
	call	_ZdlPv@PLT
.L993:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L994
	call	_ZdlPv@PLT
.L994:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L995
	call	_ZdlPv@PLT
.L995:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L988:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1721
.L996:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L988
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1722
	movq	%r12, %rdi
	call	*%rax
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L1695:
	movq	-152(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L809:
	testq	%r15, %r15
	je	.L817
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L817:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L808:
	movq	152(%r13), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L818
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L819
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L1724:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L822
	call	_ZdlPv@PLT
.L822:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L823
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L824
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L823:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L825
	call	_ZdlPv@PLT
.L825:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L826
	call	_ZdlPv@PLT
.L826:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L827
	call	_ZdlPv@PLT
.L827:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L820:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1723
.L828:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L820
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1724
	movq	%r12, %rdi
	call	*%rax
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L1693:
	movq	-152(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L766:
	testq	%r15, %r15
	je	.L774
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L774:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L765:
	movq	152(%r13), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L775
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L776
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L1726:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L779
	call	_ZdlPv@PLT
.L779:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L780
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L781
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L780:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L782
	call	_ZdlPv@PLT
.L782:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L783
	call	_ZdlPv@PLT
.L783:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L784
	call	_ZdlPv@PLT
.L784:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L777:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1725
.L785:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L777
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1726
	movq	%r12, %rdi
	call	*%rax
	jmp	.L777
	.p2align 4,,10
	.p2align 3
.L1701:
	movq	-144(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L943:
	testq	%r15, %r15
	je	.L951
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L951:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L942:
	movq	152(%r13), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L952
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L953
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L1728:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L956
	call	_ZdlPv@PLT
.L956:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L957
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L958
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L957:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L959
	call	_ZdlPv@PLT
.L959:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L960
	call	_ZdlPv@PLT
.L960:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L961
	call	_ZdlPv@PLT
.L961:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L954:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1727
.L962:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L954
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1728
	movq	%r12, %rdi
	call	*%rax
	jmp	.L954
	.p2align 4,,10
	.p2align 3
.L1689:
	movq	-152(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L732:
	testq	%r15, %r15
	je	.L740
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L740:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L731:
	movq	152(%r13), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L741
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L742
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L1730:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L745
	call	_ZdlPv@PLT
.L745:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L746
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L747
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L746:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L748
	call	_ZdlPv@PLT
.L748:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L749
	call	_ZdlPv@PLT
.L749:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L750
	call	_ZdlPv@PLT
.L750:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L743:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1729
.L751:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L743
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1730
	movq	%r12, %rdi
	call	*%rax
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L587:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L897:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L896
	.p2align 4,,10
	.p2align 3
.L579:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L583:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L1014:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L1013
	.p2align 4,,10
	.p2align 3
.L904:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L903
	.p2align 4,,10
	.p2align 3
.L653:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L855:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L716:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L900:
	movq	%rsi, %rdi
	call	*%rax
	jmp	.L899
	.p2align 4,,10
	.p2align 3
.L589:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L972:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L761:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L760
	.p2align 4,,10
	.p2align 3
.L1021:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L670:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L933:
	movq	%rsi, %rdi
	call	*%rax
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L1017:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L622:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L656:
	movq	%rdx, %rdi
	call	*%rax
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L799:
	movq	%rdx, %rdi
	call	*%rax
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L906:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L595:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L722:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L721
	.p2align 4,,10
	.p2align 3
.L858:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L601:
	call	*%rdx
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L764:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L941:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L975:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L807:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L639:
	call	*%rax
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L612:
	call	*%rax
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L603:
	call	*%rdx
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L875:
	call	*%rax
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L673:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L1023:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L805:
	call	*%rax
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L664:
	call	*%rax
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L864:
	call	*%rdx
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L728:
	call	*%rax
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L630:
	call	*%rdx
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L923:
	call	*%rax
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L912:
	call	*%rdx
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L730:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L841:
	call	*%rax
	jmp	.L840
	.p2align 4,,10
	.p2align 3
.L866:
	call	*%rdx
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L1059:
	call	*%rax
	jmp	.L1058
	.p2align 4,,10
	.p2align 3
.L914:
	call	*%rdx
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L939:
	call	*%rax
	jmp	.L938
	.p2align 4,,10
	.p2align 3
.L1050:
	call	*%rax
	jmp	.L1049
	.p2align 4,,10
	.p2align 3
.L628:
	call	*%rdx
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L662:
	call	*%rax
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L770:
	call	*%rdx
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L738:
	call	*%rdx
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L736:
	call	*%rdx
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L1031:
	call	*%rdx
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1029:
	call	*%rdx
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L690:
	call	*%rax
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L983:
	call	*%rdx
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L949:
	call	*%rdx
	jmp	.L948
	.p2align 4,,10
	.p2align 3
.L947:
	call	*%rdx
	jmp	.L946
	.p2align 4,,10
	.p2align 3
.L981:
	call	*%rdx
	jmp	.L980
	.p2align 4,,10
	.p2align 3
.L772:
	call	*%rdx
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L815:
	call	*%rdx
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L813:
	call	*%rdx
	jmp	.L812
	.p2align 4,,10
	.p2align 3
.L747:
	call	*%rax
	jmp	.L746
	.p2align 4,,10
	.p2align 3
.L958:
	call	*%rax
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L681:
	call	*%rdx
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L679:
	call	*%rdx
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L781:
	call	*%rax
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L824:
	call	*%rax
	jmp	.L823
	.p2align 4,,10
	.p2align 3
.L992:
	call	*%rax
	jmp	.L991
	.p2align 4,,10
	.p2align 3
.L1040:
	call	*%rax
	jmp	.L1039
	.cfi_endproc
.LFE4459:
	.size	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, .-_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev:
.LFB4715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%rax, (%rdi)
	movq	152(%rdi), %rdi
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1732
	call	_ZdlPv@PLT
.L1732:
	movq	136(%rbx), %rcx
	movq	%rcx, -64(%rbp)
	testq	%rcx, %rcx
	je	.L1733
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1734
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L1735
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -72(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rcx
	je	.L1736
	movq	%rbx, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L1793:
	movq	-56(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L1737
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1738
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L1739
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1740
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L1741
	movq	8(%r14), %rax
	movq	(%r14), %rbx
	cmpq	%rbx, %rax
	je	.L1742
	movq	%r12, -104(%rbp)
	movq	%rbx, %r12
	movq	%rax, %rbx
	movq	%r13, -96(%rbp)
	jmp	.L1749
	.p2align 4,,10
	.p2align 3
.L1959:
	movq	16(%r15), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r13, %r13
	je	.L1745
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1746
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1745:
	movq	8(%r15), %r13
	testq	%r13, %r13
	je	.L1747
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1748
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1747:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1743:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L1958
.L1749:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L1743
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1959
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%r12, %rbx
	jne	.L1749
	.p2align 4,,10
	.p2align 3
.L1958:
	movq	-96(%rbp), %r13
	movq	-104(%rbp), %r12
	movq	(%r14), %rbx
.L1742:
	testq	%rbx, %rbx
	je	.L1750
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L1750:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1741:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L1751
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L1752
	movq	%r13, -96(%rbp)
	movq	%r12, -104(%rbp)
	jmp	.L1761
	.p2align 4,,10
	.p2align 3
.L1961:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1755
	call	_ZdlPv@PLT
.L1755:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1756
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1757
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1756:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1758
	call	_ZdlPv@PLT
.L1758:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1759
	call	_ZdlPv@PLT
.L1759:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1760
	call	_ZdlPv@PLT
.L1760:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1753:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L1960
.L1761:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1753
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1961
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L1761
	.p2align 4,,10
	.p2align 3
.L1960:
	movq	-96(%rbp), %r13
	movq	-104(%rbp), %r12
	movq	(%r15), %r14
.L1752:
	testq	%r14, %r14
	je	.L1762
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1762:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1751:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1763
	call	_ZdlPv@PLT
.L1763:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1764
	call	_ZdlPv@PLT
.L1764:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1765
	call	_ZdlPv@PLT
.L1765:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1739:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L1766
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1767
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L1768
	movq	8(%r14), %rax
	movq	(%r14), %rbx
	cmpq	%rbx, %rax
	je	.L1769
	movq	%r12, -104(%rbp)
	movq	%rbx, %r12
	movq	%rax, %rbx
	movq	%r13, -96(%rbp)
	jmp	.L1776
	.p2align 4,,10
	.p2align 3
.L1963:
	movq	16(%r15), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r13, %r13
	je	.L1772
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1773
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1772:
	movq	8(%r15), %r13
	testq	%r13, %r13
	je	.L1774
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1775
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1774:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1770:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L1962
.L1776:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L1770
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1963
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%r12, %rbx
	jne	.L1776
	.p2align 4,,10
	.p2align 3
.L1962:
	movq	-96(%rbp), %r13
	movq	-104(%rbp), %r12
	movq	(%r14), %rbx
.L1769:
	testq	%rbx, %rbx
	je	.L1777
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L1777:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1768:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L1778
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L1779
	movq	%r13, -96(%rbp)
	movq	%r12, -104(%rbp)
	jmp	.L1788
	.p2align 4,,10
	.p2align 3
.L1965:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1782
	call	_ZdlPv@PLT
.L1782:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1783
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1784
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1783:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1785
	call	_ZdlPv@PLT
.L1785:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1786
	call	_ZdlPv@PLT
.L1786:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1787
	call	_ZdlPv@PLT
.L1787:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1780:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L1964
.L1788:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1780
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1965
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L1788
	.p2align 4,,10
	.p2align 3
.L1964:
	movq	-96(%rbp), %r13
	movq	-104(%rbp), %r12
	movq	(%r15), %r14
.L1779:
	testq	%r14, %r14
	je	.L1789
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1789:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1778:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1790
	call	_ZdlPv@PLT
.L1790:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1791
	call	_ZdlPv@PLT
.L1791:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1792
	call	_ZdlPv@PLT
.L1792:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1766:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1737:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -72(%rbp)
	jne	.L1793
	movq	-80(%rbp), %rax
	movq	-88(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
.L1736:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L1794
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1794:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1735:
	movq	-64(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L1795
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -56(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L1796
	movq	%rbx, -88(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1830:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1797
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1798
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1799
	call	_ZdlPv@PLT
.L1799:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1800
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1801
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L1802
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L1803
	movq	%rbx, -96(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -80(%rbp)
	movq	%r13, -104(%rbp)
	jmp	.L1810
	.p2align 4,,10
	.p2align 3
.L1967:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1806
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1807
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1806:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1808
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1809
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1808:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1804:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L1966
.L1810:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1804
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1967
	addq	$8, %rbx
	movq	%r12, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L1810
	.p2align 4,,10
	.p2align 3
.L1966:
	movq	-80(%rbp), %r12
	movq	-96(%rbp), %rbx
	movq	-104(%rbp), %r13
	movq	(%r14), %r15
.L1803:
	testq	%r15, %r15
	je	.L1811
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1811:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1802:
	movq	152(%r13), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L1812
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1813
	movq	%r12, -96(%rbp)
	movq	%r13, -104(%rbp)
	jmp	.L1822
	.p2align 4,,10
	.p2align 3
.L1969:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1816
	call	_ZdlPv@PLT
.L1816:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1817
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1818
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1817:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1819
	call	_ZdlPv@PLT
.L1819:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1820
	call	_ZdlPv@PLT
.L1820:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1821
	call	_ZdlPv@PLT
.L1821:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1814:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1968
.L1822:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1814
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1969
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L1822
	.p2align 4,,10
	.p2align 3
.L1968:
	movq	-80(%rbp), %rax
	movq	-96(%rbp), %r12
	movq	-104(%rbp), %r13
	movq	(%rax), %r14
.L1813:
	testq	%r14, %r14
	je	.L1823
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1823:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1812:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1824
	call	_ZdlPv@PLT
.L1824:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1825
	call	_ZdlPv@PLT
.L1825:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1826
	call	_ZdlPv@PLT
.L1826:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1800:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1827
	call	_ZdlPv@PLT
.L1827:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1828
	call	_ZdlPv@PLT
.L1828:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1829
	call	_ZdlPv@PLT
.L1829:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1797:
	addq	$8, %rbx
	cmpq	%rbx, -56(%rbp)
	jne	.L1830
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L1796:
	testq	%rdi, %rdi
	je	.L1831
	call	_ZdlPv@PLT
.L1831:
	movq	-72(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1795:
	movq	-64(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L1832
	call	_ZdlPv@PLT
.L1832:
	movq	-64(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L1833
	call	_ZdlPv@PLT
.L1833:
	movq	-64(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1834
	call	_ZdlPv@PLT
.L1834:
	movq	-64(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1733:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1835
	call	_ZdlPv@PLT
.L1835:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1836
	call	_ZdlPv@PLT
.L1836:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1837
	call	_ZdlPv@PLT
.L1837:
	addq	$72, %rsp
	movq	%rbx, %rdi
	movl	$192, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1738:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1737
	.p2align 4,,10
	.p2align 3
.L1798:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1797
	.p2align 4,,10
	.p2align 3
.L1734:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L1733
	.p2align 4,,10
	.p2align 3
.L1740:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1739
	.p2align 4,,10
	.p2align 3
.L1801:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1800
	.p2align 4,,10
	.p2align 3
.L1767:
	movq	%r12, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1737
	.p2align 4,,10
	.p2align 3
.L1746:
	call	*%rdx
	jmp	.L1745
	.p2align 4,,10
	.p2align 3
.L1757:
	call	*%rax
	jmp	.L1756
	.p2align 4,,10
	.p2align 3
.L1784:
	call	*%rax
	jmp	.L1783
	.p2align 4,,10
	.p2align 3
.L1773:
	call	*%rdx
	jmp	.L1772
	.p2align 4,,10
	.p2align 3
.L1809:
	call	*%rdx
	jmp	.L1808
	.p2align 4,,10
	.p2align 3
.L1818:
	call	*%rax
	jmp	.L1817
	.p2align 4,,10
	.p2align 3
.L1775:
	call	*%rdx
	jmp	.L1774
	.p2align 4,,10
	.p2align 3
.L1807:
	call	*%rdx
	jmp	.L1806
	.p2align 4,,10
	.p2align 3
.L1748:
	call	*%rdx
	jmp	.L1747
	.cfi_endproc
.LFE4715:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev:
.LFB4755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	%rdi, -80(%rbp)
	movq	%rcx, (%rdi)
	testq	%rbx, %rbx
	je	.L1971
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%rcx, -56(%rbp)
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1972
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, -72(%rbp)
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L1973
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -88(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rdx
	je	.L1974
	movq	%rbx, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L2031:
	movq	-64(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L1975
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1976
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L1977
	movq	(%r12), %rax
	movq	-56(%rbp), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1978
	movq	-72(%rbp), %rax
	movq	160(%r12), %r14
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L1979
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L1980
	movq	%r12, -120(%rbp)
	movq	%rbx, %r12
	movq	%rsi, %rbx
	movq	%r13, -112(%rbp)
	movq	%r14, -128(%rbp)
	jmp	.L1987
	.p2align 4,,10
	.p2align 3
.L2415:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L1983
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L1984
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1983:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L1985
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L1986
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1985:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1981:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L2414
.L1987:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L1981
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2415
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L1987
	.p2align 4,,10
	.p2align 3
.L2414:
	movq	-128(%rbp), %r14
	movq	-112(%rbp), %r13
	movq	-120(%rbp), %r12
	movq	(%r14), %rbx
.L1980:
	testq	%rbx, %rbx
	je	.L1988
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L1988:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1979:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L1989
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L1990
	movq	%r13, -112(%rbp)
	movq	%r12, -120(%rbp)
	jmp	.L1999
	.p2align 4,,10
	.p2align 3
.L2417:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1993
	call	_ZdlPv@PLT
.L1993:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1994
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L1995
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1994:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1996
	call	_ZdlPv@PLT
.L1996:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1997
	call	_ZdlPv@PLT
.L1997:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1998
	call	_ZdlPv@PLT
.L1998:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1991:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L2416
.L1999:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1991
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L2417
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L1999
	.p2align 4,,10
	.p2align 3
.L2416:
	movq	-112(%rbp), %r13
	movq	-120(%rbp), %r12
	movq	(%r15), %r14
.L1990:
	testq	%r14, %r14
	je	.L2000
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2000:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1989:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2001
	call	_ZdlPv@PLT
.L2001:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2002
	call	_ZdlPv@PLT
.L2002:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2003
	call	_ZdlPv@PLT
.L2003:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1977:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L2004
	movq	(%r12), %rax
	movq	-56(%rbp), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2005
	movq	-72(%rbp), %rax
	movq	160(%r12), %r14
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L2006
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L2007
	movq	%r12, -120(%rbp)
	movq	%rbx, %r12
	movq	%rcx, %rbx
	movq	%r13, -112(%rbp)
	movq	%r14, -128(%rbp)
	jmp	.L2014
	.p2align 4,,10
	.p2align 3
.L2419:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2010
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L2011
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2010:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L2012
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L2013
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2012:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2008:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L2418
.L2014:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L2008
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2419
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L2014
	.p2align 4,,10
	.p2align 3
.L2418:
	movq	-128(%rbp), %r14
	movq	-112(%rbp), %r13
	movq	-120(%rbp), %r12
	movq	(%r14), %rbx
.L2007:
	testq	%rbx, %rbx
	je	.L2015
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L2015:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2006:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L2016
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L2017
	movq	%r13, -112(%rbp)
	movq	%r12, -120(%rbp)
	jmp	.L2026
	.p2align 4,,10
	.p2align 3
.L2421:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2020
	call	_ZdlPv@PLT
.L2020:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2021
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2022
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2021:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2023
	call	_ZdlPv@PLT
.L2023:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2024
	call	_ZdlPv@PLT
.L2024:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2025
	call	_ZdlPv@PLT
.L2025:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2018:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L2420
.L2026:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2018
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2421
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L2026
	.p2align 4,,10
	.p2align 3
.L2420:
	movq	-112(%rbp), %r13
	movq	-120(%rbp), %r12
	movq	(%r15), %r14
.L2017:
	testq	%r14, %r14
	je	.L2027
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2027:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2016:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2028
	call	_ZdlPv@PLT
.L2028:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2029
	call	_ZdlPv@PLT
.L2029:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2030
	call	_ZdlPv@PLT
.L2030:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2004:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1975:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -88(%rbp)
	jne	.L2031
	movq	-96(%rbp), %rax
	movq	-104(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L1974:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L2032
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2032:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1973:
	movq	152(%rbx), %rax
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L2033
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -64(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L2034
	movq	%rbx, -104(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2068:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2035
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2036
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2037
	call	_ZdlPv@PLT
.L2037:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2038
	movq	0(%r13), %rax
	movq	-56(%rbp), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2039
	movq	-72(%rbp), %rax
	movq	160(%r13), %r14
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2040
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2041
	movq	%r12, -96(%rbp)
	movq	%rsi, %r12
	movq	%rbx, -112(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r13, -120(%rbp)
	movq	%r14, -128(%rbp)
	jmp	.L2048
	.p2align 4,,10
	.p2align 3
.L2423:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2044
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L2045
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2044:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L2046
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L2047
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2046:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2042:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2422
.L2048:
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L2042
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2423
	addq	$8, %rbx
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L2048
	.p2align 4,,10
	.p2align 3
.L2422:
	movq	-128(%rbp), %r14
	movq	-96(%rbp), %r12
	movq	-112(%rbp), %rbx
	movq	-120(%rbp), %r13
	movq	(%r14), %r15
.L2041:
	testq	%r15, %r15
	je	.L2049
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2049:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2040:
	movq	152(%r13), %rax
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L2050
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2051
	movq	%r12, -112(%rbp)
	movq	%r13, -120(%rbp)
	jmp	.L2060
	.p2align 4,,10
	.p2align 3
.L2425:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2054
	call	_ZdlPv@PLT
.L2054:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2055
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2056
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2055:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2057
	call	_ZdlPv@PLT
.L2057:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2058
	call	_ZdlPv@PLT
.L2058:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2059
	call	_ZdlPv@PLT
.L2059:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2052:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2424
.L2060:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2052
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L2425
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L2060
	.p2align 4,,10
	.p2align 3
.L2424:
	movq	-96(%rbp), %rax
	movq	-112(%rbp), %r12
	movq	-120(%rbp), %r13
	movq	(%rax), %r14
.L2051:
	testq	%r14, %r14
	je	.L2061
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2061:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2050:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2062
	call	_ZdlPv@PLT
.L2062:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2063
	call	_ZdlPv@PLT
.L2063:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2064
	call	_ZdlPv@PLT
.L2064:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2038:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2065
	call	_ZdlPv@PLT
.L2065:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2066
	call	_ZdlPv@PLT
.L2066:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2067
	call	_ZdlPv@PLT
.L2067:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2035:
	addq	$8, %rbx
	cmpq	%rbx, -64(%rbp)
	jne	.L2068
	movq	-88(%rbp), %rax
	movq	-104(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2034:
	testq	%rdi, %rdi
	je	.L2069
	call	_ZdlPv@PLT
.L2069:
	movq	-88(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2033:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2070
	call	_ZdlPv@PLT
.L2070:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2071
	call	_ZdlPv@PLT
.L2071:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2072
	call	_ZdlPv@PLT
.L2072:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1971:
	movq	-80(%rbp), %rax
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	je	.L2073
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%rcx, -56(%rbp)
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2074
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, -72(%rbp)
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L2075
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -88(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rcx
	je	.L2076
	movq	%rbx, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L2133:
	movq	-64(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L2077
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2078
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L2079
	movq	(%r12), %rax
	movq	-56(%rbp), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2080
	movq	-72(%rbp), %rax
	movq	160(%r12), %r14
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L2081
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L2082
	movq	%r12, -120(%rbp)
	movq	%rbx, %r12
	movq	%rcx, %rbx
	movq	%r13, -112(%rbp)
	movq	%r14, -128(%rbp)
	jmp	.L2089
	.p2align 4,,10
	.p2align 3
.L2427:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2085
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L2086
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2085:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L2087
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L2088
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2087:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2083:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L2426
.L2089:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L2083
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2427
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L2089
	.p2align 4,,10
	.p2align 3
.L2426:
	movq	-128(%rbp), %r14
	movq	-112(%rbp), %r13
	movq	-120(%rbp), %r12
	movq	(%r14), %rbx
.L2082:
	testq	%rbx, %rbx
	je	.L2090
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L2090:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2081:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L2091
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L2092
	movq	%r13, -112(%rbp)
	movq	%r12, -120(%rbp)
	jmp	.L2101
	.p2align 4,,10
	.p2align 3
.L2429:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2095
	call	_ZdlPv@PLT
.L2095:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2096
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2097
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2096:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2098
	call	_ZdlPv@PLT
.L2098:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2099
	call	_ZdlPv@PLT
.L2099:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2100
	call	_ZdlPv@PLT
.L2100:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2093:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L2428
.L2101:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2093
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2429
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L2101
	.p2align 4,,10
	.p2align 3
.L2428:
	movq	-112(%rbp), %r13
	movq	-120(%rbp), %r12
	movq	(%r15), %r14
.L2092:
	testq	%r14, %r14
	je	.L2102
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2102:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2091:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2103
	call	_ZdlPv@PLT
.L2103:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2104
	call	_ZdlPv@PLT
.L2104:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2105
	call	_ZdlPv@PLT
.L2105:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2079:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L2106
	movq	(%r12), %rax
	movq	-56(%rbp), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2107
	movq	-72(%rbp), %rax
	movq	160(%r12), %r14
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L2108
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L2109
	movq	%r12, -120(%rbp)
	movq	%rbx, %r12
	movq	%rsi, %rbx
	movq	%r13, -112(%rbp)
	movq	%r14, -128(%rbp)
	jmp	.L2116
	.p2align 4,,10
	.p2align 3
.L2431:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2112
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L2113
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2112:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L2114
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L2115
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2114:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2110:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L2430
.L2116:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L2110
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2431
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L2116
	.p2align 4,,10
	.p2align 3
.L2430:
	movq	-128(%rbp), %r14
	movq	-112(%rbp), %r13
	movq	-120(%rbp), %r12
	movq	(%r14), %rbx
.L2109:
	testq	%rbx, %rbx
	je	.L2117
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L2117:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2108:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L2118
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L2119
	movq	%r13, -112(%rbp)
	movq	%r12, -120(%rbp)
	jmp	.L2128
	.p2align 4,,10
	.p2align 3
.L2433:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2122
	call	_ZdlPv@PLT
.L2122:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2123
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2124
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2123:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2125
	call	_ZdlPv@PLT
.L2125:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2126
	call	_ZdlPv@PLT
.L2126:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2127
	call	_ZdlPv@PLT
.L2127:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2120:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L2432
.L2128:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2120
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L2433
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L2128
	.p2align 4,,10
	.p2align 3
.L2432:
	movq	-112(%rbp), %r13
	movq	-120(%rbp), %r12
	movq	(%r15), %r14
.L2119:
	testq	%r14, %r14
	je	.L2129
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2129:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2118:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2130
	call	_ZdlPv@PLT
.L2130:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2131
	call	_ZdlPv@PLT
.L2131:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2132
	call	_ZdlPv@PLT
.L2132:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2106:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2077:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -88(%rbp)
	jne	.L2133
	movq	-96(%rbp), %rax
	movq	-104(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L2076:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L2134
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2134:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2075:
	movq	152(%rbx), %rax
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L2135
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -64(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L2136
	movq	%rbx, -104(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2170:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2137
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2138
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2139
	call	_ZdlPv@PLT
.L2139:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2140
	movq	0(%r13), %rax
	movq	-56(%rbp), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2141
	movq	-72(%rbp), %rax
	movq	160(%r13), %r14
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2142
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2143
	movq	%r12, -96(%rbp)
	movq	%rcx, %r12
	movq	%rbx, -112(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r13, -120(%rbp)
	movq	%r14, -128(%rbp)
	jmp	.L2150
	.p2align 4,,10
	.p2align 3
.L2435:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2146
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L2147
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2146:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L2148
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L2149
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2148:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2144:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2434
.L2150:
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L2144
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2435
	addq	$8, %rbx
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L2150
	.p2align 4,,10
	.p2align 3
.L2434:
	movq	-128(%rbp), %r14
	movq	-96(%rbp), %r12
	movq	-112(%rbp), %rbx
	movq	-120(%rbp), %r13
	movq	(%r14), %r15
.L2143:
	testq	%r15, %r15
	je	.L2151
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2151:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2142:
	movq	152(%r13), %rax
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L2152
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2153
	movq	%r12, -112(%rbp)
	movq	%r13, -120(%rbp)
	jmp	.L2162
	.p2align 4,,10
	.p2align 3
.L2437:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2156
	call	_ZdlPv@PLT
.L2156:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2157
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2158
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2157:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2159
	call	_ZdlPv@PLT
.L2159:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2160
	call	_ZdlPv@PLT
.L2160:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2161
	call	_ZdlPv@PLT
.L2161:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2154:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2436
.L2162:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2154
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2437
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L2162
	.p2align 4,,10
	.p2align 3
.L2436:
	movq	-96(%rbp), %rax
	movq	-112(%rbp), %r12
	movq	-120(%rbp), %r13
	movq	(%rax), %r14
.L2153:
	testq	%r14, %r14
	je	.L2163
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2163:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2152:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2164
	call	_ZdlPv@PLT
.L2164:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2165
	call	_ZdlPv@PLT
.L2165:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2166
	call	_ZdlPv@PLT
.L2166:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2140:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2167
	call	_ZdlPv@PLT
.L2167:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2168
	call	_ZdlPv@PLT
.L2168:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2169
	call	_ZdlPv@PLT
.L2169:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2137:
	addq	$8, %rbx
	cmpq	%rbx, -64(%rbp)
	jne	.L2170
	movq	-88(%rbp), %rax
	movq	-104(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2136:
	testq	%rdi, %rdi
	je	.L2171
	call	_ZdlPv@PLT
.L2171:
	movq	-88(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2135:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2172
	call	_ZdlPv@PLT
.L2172:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2173
	call	_ZdlPv@PLT
.L2173:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2174
	call	_ZdlPv@PLT
.L2174:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2073:
	movq	-80(%rbp), %rdi
	addq	$88, %rsp
	movl	$24, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1976:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1975
	.p2align 4,,10
	.p2align 3
.L2036:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2035
	.p2align 4,,10
	.p2align 3
.L2078:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2077
	.p2align 4,,10
	.p2align 3
.L2138:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2137
	.p2align 4,,10
	.p2align 3
.L2074:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L2073
	.p2align 4,,10
	.p2align 3
.L1972:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L1971
	.p2align 4,,10
	.p2align 3
.L2005:
	movq	%r12, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1975
	.p2align 4,,10
	.p2align 3
.L2080:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2079
	.p2align 4,,10
	.p2align 3
.L1978:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1977
	.p2align 4,,10
	.p2align 3
.L2039:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2038
	.p2align 4,,10
	.p2align 3
.L2107:
	movq	%r12, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2077
	.p2align 4,,10
	.p2align 3
.L2141:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2140
	.p2align 4,,10
	.p2align 3
.L2013:
	call	*%rdx
	jmp	.L2012
	.p2align 4,,10
	.p2align 3
.L1986:
	call	*%rdx
	jmp	.L1985
	.p2align 4,,10
	.p2align 3
.L2011:
	call	*%rdx
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2115:
	call	*%rdx
	jmp	.L2114
	.p2align 4,,10
	.p2align 3
.L2124:
	call	*%rax
	jmp	.L2123
	.p2align 4,,10
	.p2align 3
.L2113:
	call	*%rdx
	jmp	.L2112
	.p2align 4,,10
	.p2align 3
.L2086:
	call	*%rdx
	jmp	.L2085
	.p2align 4,,10
	.p2align 3
.L2149:
	call	*%rdx
	jmp	.L2148
	.p2align 4,,10
	.p2align 3
.L2022:
	call	*%rax
	jmp	.L2021
	.p2align 4,,10
	.p2align 3
.L2158:
	call	*%rax
	jmp	.L2157
	.p2align 4,,10
	.p2align 3
.L2147:
	call	*%rdx
	jmp	.L2146
	.p2align 4,,10
	.p2align 3
.L1995:
	call	*%rax
	jmp	.L1994
	.p2align 4,,10
	.p2align 3
.L2045:
	call	*%rdx
	jmp	.L2044
	.p2align 4,,10
	.p2align 3
.L2056:
	call	*%rax
	jmp	.L2055
	.p2align 4,,10
	.p2align 3
.L1984:
	call	*%rdx
	jmp	.L1983
	.p2align 4,,10
	.p2align 3
.L2097:
	call	*%rax
	jmp	.L2096
	.p2align 4,,10
	.p2align 3
.L2088:
	call	*%rdx
	jmp	.L2087
	.p2align 4,,10
	.p2align 3
.L2047:
	call	*%rdx
	jmp	.L2046
	.cfi_endproc
.LFE4755:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev:
.LFB4642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	160(%rdi), %rax
	movq	%rdi, -72(%rbp)
	movq	%rdx, (%rdi)
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L2439
	movq	8(%rax), %rbx
	movq	(%rax), %rax
	movq	%rbx, -128(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rbx
	je	.L2440
	.p2align 4,,10
	.p2align 3
.L2772:
	movq	-64(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -104(%rbp)
	testq	%rcx, %rcx
	je	.L2441
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2442
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	16(%rcx), %rcx
	movq	%rcx, -80(%rbp)
	testq	%rcx, %rcx
	je	.L2443
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%rdx, -56(%rbp)
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2444
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L2445
	movq	8(%rax), %rbx
	movq	(%rax), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -88(%rbp)
	cmpq	%rax, %rbx
	je	.L2446
	.p2align 4,,10
	.p2align 3
.L2553:
	movq	-88(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -96(%rbp)
	testq	%rcx, %rcx
	je	.L2447
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2448
	movq	16(%rcx), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	testq	%r12, %r12
	je	.L2449
	movq	(%r12), %rax
	movq	-56(%rbp), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2450
	movq	160(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2451
	movq	8(%r13), %rbx
	movq	0(%r13), %r15
	cmpq	%r15, %rbx
	je	.L2452
	movq	%r12, -144(%rbp)
	movq	%r15, %r12
	movq	%rdx, %r15
	movq	%r13, -152(%rbp)
	jmp	.L2459
	.p2align 4,,10
	.p2align 3
.L3575:
	movq	16(%r14), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r13, %r13
	je	.L2455
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r15, %rdx
	jne	.L2456
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2455:
	movq	8(%r14), %r13
	testq	%r13, %r13
	je	.L2457
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r15, %rdx
	jne	.L2458
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2457:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2453:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L3574
.L2459:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L2453
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3575
	addq	$8, %r12
	movq	%r14, %rdi
	call	*%rdx
	cmpq	%r12, %rbx
	jne	.L2459
	.p2align 4,,10
	.p2align 3
.L3574:
	movq	-152(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	0(%r13), %r15
.L2452:
	testq	%r15, %r15
	je	.L2460
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2460:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2451:
	movq	152(%r12), %r14
	testq	%r14, %r14
	je	.L2461
	movq	8(%r14), %rbx
	movq	(%r14), %r13
	cmpq	%r13, %rbx
	je	.L2462
	movq	%r12, -144(%rbp)
	jmp	.L2471
	.p2align 4,,10
	.p2align 3
.L3577:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2465
	call	_ZdlPv@PLT
.L2465:
	movq	136(%r15), %r12
	testq	%r12, %r12
	je	.L2466
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2467
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2466:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2468
	call	_ZdlPv@PLT
.L2468:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2469
	call	_ZdlPv@PLT
.L2469:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2470
	call	_ZdlPv@PLT
.L2470:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2463:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L3576
.L2471:
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L2463
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3577
	addq	$8, %r13
	movq	%r15, %rdi
	call	*%rax
	cmpq	%r13, %rbx
	jne	.L2471
	.p2align 4,,10
	.p2align 3
.L3576:
	movq	-144(%rbp), %r12
	movq	(%r14), %r13
.L2462:
	testq	%r13, %r13
	je	.L2472
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2472:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2461:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2473
	call	_ZdlPv@PLT
.L2473:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2474
	call	_ZdlPv@PLT
.L2474:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2475
	call	_ZdlPv@PLT
.L2475:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2449:
	movq	-96(%rbp), %rax
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	je	.L2476
	movq	(%rbx), %rax
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2477
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L2478
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -144(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L2479
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2511:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2480
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2481
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2482
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2483
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2482:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2484
	movq	0(%r13), %rax
	movq	-56(%rbp), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2485
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2486
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2487
	movq	%r12, -160(%rbp)
	movq	%rsi, %r12
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r13, -184(%rbp)
	movq	%r14, -192(%rbp)
	jmp	.L2494
	.p2align 4,,10
	.p2align 3
.L3579:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2490
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L2491
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2490:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L2492
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L2493
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2492:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2488:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L3578
.L2494:
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L2488
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3579
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L2488
	.p2align 4,,10
	.p2align 3
.L2448:
	movq	%rcx, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L2447:
	addq	$8, -88(%rbp)
	movq	-88(%rbp), %rax
	cmpq	%rax, -136(%rbp)
	jne	.L2553
	movq	-120(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -88(%rbp)
.L2446:
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L2554
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2554:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2445:
	movq	-80(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L2555
	movq	8(%rax), %rbx
	movq	(%rax), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -88(%rbp)
	cmpq	%rax, %rbx
	je	.L2556
	.p2align 4,,10
	.p2align 3
.L2615:
	movq	-88(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L2557
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2558
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2559
	call	_ZdlPv@PLT
.L2559:
	movq	136(%rbx), %rcx
	movq	%rcx, -96(%rbp)
	testq	%rcx, %rcx
	je	.L2560
	movq	(%rcx), %rax
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2561
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L2562
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -144(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L2563
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2595:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2564
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2565
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2566
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2567
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2566:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2568
	movq	0(%r13), %rax
	movq	-56(%rbp), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2569
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2570
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2571
	movq	%r12, -160(%rbp)
	movq	%rcx, %r12
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r13, -184(%rbp)
	movq	%r14, -192(%rbp)
	jmp	.L2578
	.p2align 4,,10
	.p2align 3
.L3581:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2574
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L2575
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2574:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L2576
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L2577
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2576:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2572:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L3580
.L2578:
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L2572
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3581
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L2572
	.p2align 4,,10
	.p2align 3
.L3602:
	movq	-144(%rbp), %r13
	movq	0(%r13), %r12
.L2598:
	testq	%r12, %r12
	je	.L2608
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2608:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2597:
	movq	-96(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2609
	call	_ZdlPv@PLT
.L2609:
	movq	-96(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2610
	call	_ZdlPv@PLT
.L2610:
	movq	-96(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2611
	call	_ZdlPv@PLT
.L2611:
	movq	-96(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2560:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2612
	call	_ZdlPv@PLT
.L2612:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2613
	call	_ZdlPv@PLT
.L2613:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2614
	call	_ZdlPv@PLT
.L2614:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2557:
	addq	$8, -88(%rbp)
	movq	-88(%rbp), %rax
	cmpq	%rax, -136(%rbp)
	jne	.L2615
	movq	-120(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -88(%rbp)
.L2556:
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L2616
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2616:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2555:
	movq	-80(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2617
	call	_ZdlPv@PLT
.L2617:
	movq	-80(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2618
	call	_ZdlPv@PLT
.L2618:
	movq	-80(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2619
	call	_ZdlPv@PLT
.L2619:
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2443:
	movq	-104(%rbp), %rax
	movq	8(%rax), %rsi
	movq	%rsi, -80(%rbp)
	testq	%rsi, %rsi
	je	.L2620
	movq	(%rsi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%rcx, -56(%rbp)
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2621
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rsi)
	movq	160(%rsi), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L2622
	movq	8(%rax), %rbx
	movq	(%rax), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -88(%rbp)
	cmpq	%rax, %rbx
	je	.L2623
	.p2align 4,,10
	.p2align 3
.L2730:
	movq	-88(%rbp), %rax
	movq	(%rax), %rsi
	movq	%rsi, -96(%rbp)
	testq	%rsi, %rsi
	je	.L2624
	movq	(%rsi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2625
	movq	16(%rsi), %rbx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rsi)
	testq	%rbx, %rbx
	je	.L2626
	movq	(%rbx), %rax
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2627
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L2628
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -144(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L2629
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2661:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2630
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2631
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2632
	movq	0(%r13), %rax
	movq	-56(%rbp), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2633
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2634
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2635
	movq	%r12, -160(%rbp)
	movq	%rsi, %r12
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r13, -184(%rbp)
	movq	%r14, -192(%rbp)
	jmp	.L2642
	.p2align 4,,10
	.p2align 3
.L3583:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2638
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L2639
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2638:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L2640
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L2641
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2640:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2636:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L3582
.L2642:
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L2636
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3583
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L2636
	.p2align 4,,10
	.p2align 3
.L2625:
	movq	%rsi, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L2624:
	addq	$8, -88(%rbp)
	movq	-88(%rbp), %rax
	cmpq	%rax, -136(%rbp)
	jne	.L2730
	movq	-120(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -88(%rbp)
.L2623:
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L2731
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2731:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2622:
	movq	-80(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L2732
	movq	8(%rax), %rdx
	movq	(%rax), %rbx
	movq	%rdx, -96(%rbp)
	cmpq	%rbx, %rdx
	je	.L2733
	.p2align 4,,10
	.p2align 3
.L2767:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2734
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2735
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2736
	call	_ZdlPv@PLT
.L2736:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2737
	movq	0(%r13), %rax
	movq	-56(%rbp), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2738
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2739
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2740
	movq	%r12, -120(%rbp)
	movq	%rcx, %r12
	movq	%rbx, -136(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r13, -144(%rbp)
	movq	%r14, -152(%rbp)
	jmp	.L2747
	.p2align 4,,10
	.p2align 3
.L3585:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2743
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L2744
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2743:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L2745
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L2746
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2745:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2741:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L3584
.L2747:
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L2741
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3585
	addq	$8, %rbx
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L2747
	.p2align 4,,10
	.p2align 3
.L3584:
	movq	-152(%rbp), %r14
	movq	-120(%rbp), %r12
	movq	-136(%rbp), %rbx
	movq	-144(%rbp), %r13
	movq	(%r14), %r15
.L2740:
	testq	%r15, %r15
	je	.L2748
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2748:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2739:
	movq	152(%r13), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L2749
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2750
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L2759
	.p2align 4,,10
	.p2align 3
.L3587:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2753
	call	_ZdlPv@PLT
.L2753:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2754
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2755
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2754:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2756
	call	_ZdlPv@PLT
.L2756:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2757
	call	_ZdlPv@PLT
.L2757:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2758
	call	_ZdlPv@PLT
.L2758:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2751:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3586
.L2759:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2751
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3587
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L2759
	.p2align 4,,10
	.p2align 3
.L3586:
	movq	-120(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L2750:
	testq	%r14, %r14
	je	.L2760
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2760:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2749:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2761
	call	_ZdlPv@PLT
.L2761:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2762
	call	_ZdlPv@PLT
.L2762:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2763
	call	_ZdlPv@PLT
.L2763:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2737:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2764
	call	_ZdlPv@PLT
.L2764:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2765
	call	_ZdlPv@PLT
.L2765:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2766
	call	_ZdlPv@PLT
.L2766:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2734:
	addq	$8, %rbx
	cmpq	%rbx, -96(%rbp)
	jne	.L2767
.L3610:
	movq	-88(%rbp), %rax
	movq	(%rax), %rbx
.L2733:
	testq	%rbx, %rbx
	je	.L2768
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L2768:
	movq	-88(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2732:
	movq	-80(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2769
	call	_ZdlPv@PLT
.L2769:
	movq	-80(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2770
	call	_ZdlPv@PLT
.L2770:
	movq	-80(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2771
	call	_ZdlPv@PLT
.L2771:
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2620:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2441:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -128(%rbp)
	jne	.L2772
	movq	-112(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L2440:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L2773
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2773:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2439:
	movq	-72(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L2774
	movq	8(%rax), %rbx
	movq	(%rax), %rax
	movq	%rbx, -128(%rbp)
	movq	%rax, -80(%rbp)
	cmpq	%rax, %rbx
	je	.L2775
	.p2align 4,,10
	.p2align 3
.L2959:
	movq	-80(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -64(%rbp)
	testq	%rcx, %rcx
	je	.L2776
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2777
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rcx), %rdi
	movq	%rax, (%rcx)
	leaq	168(%rcx), %rax
	cmpq	%rax, %rdi
	je	.L2778
	call	_ZdlPv@PLT
.L2778:
	movq	-64(%rbp), %rax
	movq	136(%rax), %rdx
	movq	%rdx, -88(%rbp)
	testq	%rdx, %rdx
	je	.L2779
	movq	(%rdx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%rcx, -56(%rbp)
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2780
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rdx)
	movq	160(%rdx), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L2781
	movq	8(%rax), %rbx
	movq	(%rax), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -96(%rbp)
	cmpq	%rax, %rbx
	je	.L2782
	.p2align 4,,10
	.p2align 3
.L2864:
	movq	-96(%rbp), %rax
	movq	(%rax), %rdx
	movq	%rdx, -104(%rbp)
	testq	%rdx, %rdx
	je	.L2783
	movq	(%rdx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2784
	movq	16(%rdx), %rbx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rdx)
	testq	%rbx, %rbx
	je	.L2785
	movq	(%rbx), %rax
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2786
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L2787
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -144(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L2788
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2820:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2789
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2790
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2791
	movq	0(%r13), %rax
	movq	-56(%rbp), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2792
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2793
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2794
	movq	%r12, -160(%rbp)
	movq	%rcx, %r12
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r13, -184(%rbp)
	movq	%r14, -192(%rbp)
	jmp	.L2801
	.p2align 4,,10
	.p2align 3
.L3589:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2797
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L2798
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2797:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L2799
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L2800
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2799:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2795:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L3588
.L2801:
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L2795
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3589
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L2795
	.p2align 4,,10
	.p2align 3
.L2784:
	movq	%rdx, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L2783:
	addq	$8, -96(%rbp)
	movq	-96(%rbp), %rax
	cmpq	%rax, -136(%rbp)
	jne	.L2864
	movq	-120(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -96(%rbp)
.L2782:
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L2865
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2865:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2781:
	movq	-88(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L2866
	movq	8(%rax), %rbx
	movq	(%rax), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -96(%rbp)
	cmpq	%rax, %rbx
	je	.L2867
	.p2align 4,,10
	.p2align 3
.L2951:
	movq	-96(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L2868
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2869
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2870
	call	_ZdlPv@PLT
.L2870:
	movq	136(%rbx), %rsi
	movq	%rsi, -104(%rbp)
	testq	%rsi, %rsi
	je	.L2871
	movq	(%rsi), %rax
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2872
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rsi)
	movq	160(%rsi), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L2873
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -144(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L2874
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2906:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2875
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2876
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2877
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2878
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2877:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2879
	movq	0(%r13), %rax
	movq	-56(%rbp), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2880
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2881
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2882
	movq	%r12, -160(%rbp)
	movq	%rsi, %r12
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r13, -184(%rbp)
	movq	%r14, -192(%rbp)
	jmp	.L2889
	.p2align 4,,10
	.p2align 3
.L3591:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2885
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L2886
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2885:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L2887
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L2888
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2887:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2883:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L3590
.L2889:
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L2883
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3591
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L2883
	.p2align 4,,10
	.p2align 3
.L3613:
	movq	-160(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L2926:
	testq	%r14, %r14
	je	.L2936
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2936:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2925:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2937
	call	_ZdlPv@PLT
.L2937:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2938
	call	_ZdlPv@PLT
.L2938:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2939
	call	_ZdlPv@PLT
.L2939:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2913:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2940
	call	_ZdlPv@PLT
.L2940:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2941
	call	_ZdlPv@PLT
.L2941:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2942
	call	_ZdlPv@PLT
.L2942:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2910:
	addq	$8, %rbx
	cmpq	%rbx, -144(%rbp)
	jne	.L2943
	movq	-152(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2909:
	testq	%rdi, %rdi
	je	.L2944
	call	_ZdlPv@PLT
.L2944:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2908:
	movq	-104(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2945
	call	_ZdlPv@PLT
.L2945:
	movq	-104(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2946
	call	_ZdlPv@PLT
.L2946:
	movq	-104(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2947
	call	_ZdlPv@PLT
.L2947:
	movq	-104(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2871:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2948
	call	_ZdlPv@PLT
.L2948:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2949
	call	_ZdlPv@PLT
.L2949:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2950
	call	_ZdlPv@PLT
.L2950:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2868:
	addq	$8, -96(%rbp)
	movq	-96(%rbp), %rax
	cmpq	%rax, -136(%rbp)
	jne	.L2951
	movq	-120(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -96(%rbp)
.L2867:
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L2952
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2952:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2866:
	movq	-88(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2953
	call	_ZdlPv@PLT
.L2953:
	movq	-88(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2954
	call	_ZdlPv@PLT
.L2954:
	movq	-88(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2955
	call	_ZdlPv@PLT
.L2955:
	movq	-88(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2779:
	movq	-64(%rbp), %rax
	movq	96(%rax), %rdi
	addq	$112, %rax
	cmpq	%rax, %rdi
	je	.L2956
	call	_ZdlPv@PLT
.L2956:
	movq	-64(%rbp), %rax
	movq	48(%rax), %rdi
	addq	$64, %rax
	cmpq	%rax, %rdi
	je	.L2957
	call	_ZdlPv@PLT
.L2957:
	movq	-64(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2958
	call	_ZdlPv@PLT
.L2958:
	movq	-64(%rbp), %rdi
	movl	$192, %esi
	call	_ZdlPvm@PLT
.L2776:
	addq	$8, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	%rax, -128(%rbp)
	jne	.L2959
	movq	-112(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -80(%rbp)
.L2775:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L2960
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2960:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2774:
	movq	-72(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2961
	call	_ZdlPv@PLT
.L2961:
	movq	-72(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2962
	call	_ZdlPv@PLT
.L2962:
	movq	-72(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2963
	call	_ZdlPv@PLT
.L2963:
	movq	-72(%rbp), %rdi
	addq	$152, %rsp
	movl	$168, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L3598:
	.cfi_restore_state
	movq	-144(%rbp), %r13
	movq	0(%r13), %r12
.L2664:
	testq	%r12, %r12
	je	.L2674
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2674:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2663:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2675
	call	_ZdlPv@PLT
.L2675:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2676
	call	_ZdlPv@PLT
.L2676:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2677
	call	_ZdlPv@PLT
.L2677:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2626:
	movq	-96(%rbp), %rax
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	je	.L2678
	movq	(%rbx), %rax
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2679
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L2680
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -144(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L2681
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2713:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2682
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2683
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2684
	movq	0(%r13), %rax
	movq	-56(%rbp), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2685
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2686
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2687
	movq	%r12, -160(%rbp)
	movq	%rdx, %r12
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r13, -184(%rbp)
	movq	%r14, -192(%rbp)
	jmp	.L2694
	.p2align 4,,10
	.p2align 3
.L3593:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2690
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L2691
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2690:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L2692
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L2693
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2692:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2688:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L3592
.L2694:
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L2688
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3593
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L2688
	.p2align 4,,10
	.p2align 3
.L3600:
	movq	-144(%rbp), %r13
	movq	0(%r13), %r12
.L2716:
	testq	%r12, %r12
	je	.L2726
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2726:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2715:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2727
	call	_ZdlPv@PLT
.L2727:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2728
	call	_ZdlPv@PLT
.L2728:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2729
	call	_ZdlPv@PLT
.L2729:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2678:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L2624
	.p2align 4,,10
	.p2align 3
.L3619:
	movq	-160(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L2531:
	testq	%r14, %r14
	je	.L2541
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2541:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2530:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2542
	call	_ZdlPv@PLT
.L2542:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2543
	call	_ZdlPv@PLT
.L2543:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2544
	call	_ZdlPv@PLT
.L2544:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2518:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2545
	call	_ZdlPv@PLT
.L2545:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2546
	call	_ZdlPv@PLT
.L2546:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2547
	call	_ZdlPv@PLT
.L2547:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2515:
	addq	$8, %rbx
	cmpq	%rbx, -144(%rbp)
	jne	.L2548
	movq	-152(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2514:
	testq	%rdi, %rdi
	je	.L2549
	call	_ZdlPv@PLT
.L2549:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2513:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2550
	call	_ZdlPv@PLT
.L2550:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2551
	call	_ZdlPv@PLT
.L2551:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2552
	call	_ZdlPv@PLT
.L2552:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2476:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L2447
	.p2align 4,,10
	.p2align 3
.L3608:
	movq	-144(%rbp), %r13
	movq	0(%r13), %r12
.L2823:
	testq	%r12, %r12
	je	.L2833
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2833:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2822:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2834
	call	_ZdlPv@PLT
.L2834:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2835
	call	_ZdlPv@PLT
.L2835:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2836
	call	_ZdlPv@PLT
.L2836:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2785:
	movq	-104(%rbp), %rax
	movq	8(%rax), %r12
	testq	%r12, %r12
	je	.L2837
	movq	(%r12), %rax
	movq	-56(%rbp), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2838
	movq	160(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2839
	movq	8(%r13), %rbx
	movq	0(%r13), %r15
	cmpq	%r15, %rbx
	je	.L2840
	movq	%r12, -144(%rbp)
	movq	%r15, %r12
	movq	%rsi, %r15
	movq	%r13, -152(%rbp)
	jmp	.L2847
	.p2align 4,,10
	.p2align 3
.L3595:
	movq	16(%r14), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r13, %r13
	je	.L2843
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r15, %rdx
	jne	.L2844
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2843:
	movq	8(%r14), %r13
	testq	%r13, %r13
	je	.L2845
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r15, %rdx
	jne	.L2846
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2845:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2841:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L3594
.L2847:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L2841
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3595
	addq	$8, %r12
	movq	%r14, %rdi
	call	*%rdx
	cmpq	%r12, %rbx
	jne	.L2847
	.p2align 4,,10
	.p2align 3
.L3594:
	movq	-152(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	0(%r13), %r15
.L2840:
	testq	%r15, %r15
	je	.L2848
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2848:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2839:
	movq	152(%r12), %r14
	testq	%r14, %r14
	je	.L2849
	movq	8(%r14), %rbx
	movq	(%r14), %r13
	cmpq	%r13, %rbx
	je	.L2850
	movq	%r12, -144(%rbp)
	jmp	.L2859
	.p2align 4,,10
	.p2align 3
.L3597:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2853
	call	_ZdlPv@PLT
.L2853:
	movq	136(%r15), %r12
	testq	%r12, %r12
	je	.L2854
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2855
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2854:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2856
	call	_ZdlPv@PLT
.L2856:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2857
	call	_ZdlPv@PLT
.L2857:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2858
	call	_ZdlPv@PLT
.L2858:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2851:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L3596
.L2859:
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L2851
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3597
	addq	$8, %r13
	movq	%r15, %rdi
	call	*%rax
	cmpq	%r13, %rbx
	jne	.L2859
	.p2align 4,,10
	.p2align 3
.L3596:
	movq	-144(%rbp), %r12
	movq	(%r14), %r13
.L2850:
	testq	%r13, %r13
	je	.L2860
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2860:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2849:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2861
	call	_ZdlPv@PLT
.L2861:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2862
	call	_ZdlPv@PLT
.L2862:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2863
	call	_ZdlPv@PLT
.L2863:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2837:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L2783
	.p2align 4,,10
	.p2align 3
.L3621:
	movq	-160(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L2645:
	testq	%r14, %r14
	je	.L2655
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2655:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2644:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2656
	call	_ZdlPv@PLT
.L2656:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2657
	call	_ZdlPv@PLT
.L2657:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2658
	call	_ZdlPv@PLT
.L2658:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2632:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2659
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2660
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2659:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2630:
	addq	$8, %rbx
	cmpq	%rbx, -144(%rbp)
	jne	.L2661
	movq	-152(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2629:
	testq	%rdi, %rdi
	je	.L2662
	call	_ZdlPv@PLT
.L2662:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2628:
	movq	152(%rbx), %r13
	testq	%r13, %r13
	je	.L2663
	movq	8(%r13), %r14
	movq	0(%r13), %r12
	cmpq	%r12, %r14
	je	.L2664
	movq	%r13, -144(%rbp)
	jmp	.L2673
	.p2align 4,,10
	.p2align 3
.L3599:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2667
	call	_ZdlPv@PLT
.L2667:
	movq	136(%r15), %r13
	testq	%r13, %r13
	je	.L2668
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2669
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2668:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2670
	call	_ZdlPv@PLT
.L2670:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2671
	call	_ZdlPv@PLT
.L2671:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2672
	call	_ZdlPv@PLT
.L2672:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2665:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L3598
.L2673:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L2665
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3599
	movq	%r15, %rdi
	call	*%rax
	jmp	.L2665
	.p2align 4,,10
	.p2align 3
.L3615:
	movq	-160(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L2697:
	testq	%r14, %r14
	je	.L2707
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2707:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2696:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2708
	call	_ZdlPv@PLT
.L2708:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2709
	call	_ZdlPv@PLT
.L2709:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2710
	call	_ZdlPv@PLT
.L2710:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2684:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2711
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2712
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2711:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2682:
	addq	$8, %rbx
	cmpq	%rbx, -144(%rbp)
	jne	.L2713
	movq	-152(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2681:
	testq	%rdi, %rdi
	je	.L2714
	call	_ZdlPv@PLT
.L2714:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2680:
	movq	152(%rbx), %r13
	testq	%r13, %r13
	je	.L2715
	movq	8(%r13), %r14
	movq	0(%r13), %r12
	cmpq	%r12, %r14
	je	.L2716
	movq	%r13, -144(%rbp)
	jmp	.L2725
	.p2align 4,,10
	.p2align 3
.L3601:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2719
	call	_ZdlPv@PLT
.L2719:
	movq	136(%r15), %r13
	testq	%r13, %r13
	je	.L2720
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2721
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2720:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2722
	call	_ZdlPv@PLT
.L2722:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2723
	call	_ZdlPv@PLT
.L2723:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2724
	call	_ZdlPv@PLT
.L2724:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2717:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L3600
.L2725:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L2717
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L3601
	movq	%r15, %rdi
	call	*%rax
	jmp	.L2717
	.p2align 4,,10
	.p2align 3
.L2565:
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L2564:
	addq	$8, %rbx
	cmpq	%rbx, -144(%rbp)
	jne	.L2595
	movq	-152(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2563:
	testq	%rdi, %rdi
	je	.L2596
	call	_ZdlPv@PLT
.L2596:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2562:
	movq	-96(%rbp), %rax
	movq	152(%rax), %r13
	testq	%r13, %r13
	je	.L2597
	movq	8(%r13), %r14
	movq	0(%r13), %r12
	cmpq	%r12, %r14
	je	.L2598
	movq	%r13, -144(%rbp)
	jmp	.L2607
	.p2align 4,,10
	.p2align 3
.L3603:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2601
	call	_ZdlPv@PLT
.L2601:
	movq	136(%r15), %r13
	testq	%r13, %r13
	je	.L2602
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2603
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2602:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2604
	call	_ZdlPv@PLT
.L2604:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2605
	call	_ZdlPv@PLT
.L2605:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2606
	call	_ZdlPv@PLT
.L2606:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2599:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L3602
.L2607:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L2599
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3603
	movq	%r15, %rdi
	call	*%rax
	jmp	.L2599
	.p2align 4,,10
	.p2align 3
.L2876:
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L2875:
	addq	$8, %rbx
	cmpq	%rbx, -144(%rbp)
	jne	.L2906
	movq	-152(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2874:
	testq	%rdi, %rdi
	je	.L2907
	call	_ZdlPv@PLT
.L2907:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2873:
	movq	-104(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L2908
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -144(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L2909
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2943:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2910
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2911
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2912
	call	_ZdlPv@PLT
.L2912:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2913
	movq	0(%r13), %rax
	movq	-56(%rbp), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2914
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2915
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2916
	movq	%r12, -160(%rbp)
	movq	%rdx, %r12
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r13, -184(%rbp)
	movq	%r14, -192(%rbp)
	jmp	.L2923
	.p2align 4,,10
	.p2align 3
.L3605:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2919
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L2920
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2919:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L2921
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L2922
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2921:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2917:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L3604
.L2923:
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L2917
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3605
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L2917
	.p2align 4,,10
	.p2align 3
.L2481:
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L2480:
	addq	$8, %rbx
	cmpq	%rbx, -144(%rbp)
	jne	.L2511
	movq	-152(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2479:
	testq	%rdi, %rdi
	je	.L2512
	call	_ZdlPv@PLT
.L2512:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2478:
	movq	152(%rbx), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L2513
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -144(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L2514
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2548:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2515
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2516
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2517
	call	_ZdlPv@PLT
.L2517:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2518
	movq	0(%r13), %rax
	movq	-56(%rbp), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2519
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2520
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2521
	movq	%r12, -160(%rbp)
	movq	%rdx, %r12
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r13, -184(%rbp)
	movq	%r14, -192(%rbp)
	jmp	.L2528
	.p2align 4,,10
	.p2align 3
.L3607:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2524
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L2525
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2524:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L2526
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L2527
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2526:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2522:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L3606
.L2528:
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L2522
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3607
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L2522
	.p2align 4,,10
	.p2align 3
.L3617:
	movq	-160(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L2804:
	testq	%r14, %r14
	je	.L2814
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2814:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2803:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2815
	call	_ZdlPv@PLT
.L2815:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2816
	call	_ZdlPv@PLT
.L2816:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2817
	call	_ZdlPv@PLT
.L2817:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2791:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2818
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2819
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2818:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2789:
	addq	$8, %rbx
	cmpq	%rbx, -144(%rbp)
	jne	.L2820
	movq	-152(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2788:
	testq	%rdi, %rdi
	je	.L2821
	call	_ZdlPv@PLT
.L2821:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2787:
	movq	152(%rbx), %r13
	testq	%r13, %r13
	je	.L2822
	movq	8(%r13), %r14
	movq	0(%r13), %r12
	cmpq	%r12, %r14
	je	.L2823
	movq	%r13, -144(%rbp)
	jmp	.L2832
	.p2align 4,,10
	.p2align 3
.L3609:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2826
	call	_ZdlPv@PLT
.L2826:
	movq	136(%r15), %r13
	testq	%r13, %r13
	je	.L2827
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2828
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2827:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2829
	call	_ZdlPv@PLT
.L2829:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2830
	call	_ZdlPv@PLT
.L2830:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2831
	call	_ZdlPv@PLT
.L2831:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2824:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L3608
.L2832:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L2824
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L3609
	movq	%r15, %rdi
	call	*%rax
	jmp	.L2824
	.p2align 4,,10
	.p2align 3
.L2777:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L2776
	.p2align 4,,10
	.p2align 3
.L2442:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L2441
	.p2align 4,,10
	.p2align 3
.L2735:
	movq	%r12, %rdi
	addq	$8, %rbx
	call	*%rax
	cmpq	%rbx, -96(%rbp)
	jne	.L2767
	jmp	.L3610
	.p2align 4,,10
	.p2align 3
.L2869:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L2868
	.p2align 4,,10
	.p2align 3
.L2558:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L2557
	.p2align 4,,10
	.p2align 3
.L3611:
	movq	-160(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L2497:
	testq	%r14, %r14
	je	.L2507
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2507:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2496:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2508
	call	_ZdlPv@PLT
.L2508:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2509
	call	_ZdlPv@PLT
.L2509:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2510
	call	_ZdlPv@PLT
.L2510:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2484:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2480
	.p2align 4,,10
	.p2align 3
.L3625:
	movq	-160(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L2892:
	testq	%r14, %r14
	je	.L2902
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2902:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2891:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2903
	call	_ZdlPv@PLT
.L2903:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2904
	call	_ZdlPv@PLT
.L2904:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2905
	call	_ZdlPv@PLT
.L2905:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2879:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2875
	.p2align 4,,10
	.p2align 3
.L3623:
	movq	-160(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L2581:
	testq	%r14, %r14
	je	.L2591
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2591:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2580:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2592
	call	_ZdlPv@PLT
.L2592:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2593
	call	_ZdlPv@PLT
.L2593:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2594
	call	_ZdlPv@PLT
.L2594:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2568:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2564
	.p2align 4,,10
	.p2align 3
.L3578:
	movq	-192(%rbp), %r14
	movq	-160(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L2487:
	testq	%r15, %r15
	je	.L2495
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2495:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2486:
	movq	152(%r13), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L2496
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2497
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L2506
	.p2align 4,,10
	.p2align 3
.L3612:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2500
	call	_ZdlPv@PLT
.L2500:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2501
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2502
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2501:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2503
	call	_ZdlPv@PLT
.L2503:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2504
	call	_ZdlPv@PLT
.L2504:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2505
	call	_ZdlPv@PLT
.L2505:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2498:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3611
.L2506:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2498
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L3612
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2498
	.p2align 4,,10
	.p2align 3
.L3604:
	movq	-192(%rbp), %r14
	movq	-160(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L2916:
	testq	%r15, %r15
	je	.L2924
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2924:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2915:
	movq	152(%r13), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L2925
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2926
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L2935
	.p2align 4,,10
	.p2align 3
.L3614:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2929
	call	_ZdlPv@PLT
.L2929:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2930
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2931
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2930:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2932
	call	_ZdlPv@PLT
.L2932:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2933
	call	_ZdlPv@PLT
.L2933:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2934
	call	_ZdlPv@PLT
.L2934:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2927:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3613
.L2935:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2927
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3614
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L3592:
	movq	-192(%rbp), %r14
	movq	-160(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L2687:
	testq	%r15, %r15
	je	.L2695
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2695:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2686:
	movq	152(%r13), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L2696
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2697
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L2706
	.p2align 4,,10
	.p2align 3
.L3616:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2700
	call	_ZdlPv@PLT
.L2700:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2701
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2702
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2701:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2703
	call	_ZdlPv@PLT
.L2703:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2704
	call	_ZdlPv@PLT
.L2704:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2705
	call	_ZdlPv@PLT
.L2705:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2698:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3615
.L2706:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2698
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3616
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2698
	.p2align 4,,10
	.p2align 3
.L3588:
	movq	-192(%rbp), %r14
	movq	-160(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L2794:
	testq	%r15, %r15
	je	.L2802
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2802:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2793:
	movq	152(%r13), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L2803
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2804
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L2813
	.p2align 4,,10
	.p2align 3
.L3618:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2807
	call	_ZdlPv@PLT
.L2807:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2808
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2809
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2808:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2810
	call	_ZdlPv@PLT
.L2810:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2811
	call	_ZdlPv@PLT
.L2811:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2812
	call	_ZdlPv@PLT
.L2812:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2805:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3617
.L2813:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2805
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L3618
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2805
	.p2align 4,,10
	.p2align 3
.L3606:
	movq	-192(%rbp), %r14
	movq	-160(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L2521:
	testq	%r15, %r15
	je	.L2529
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2529:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2520:
	movq	152(%r13), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L2530
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2531
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L2540
	.p2align 4,,10
	.p2align 3
.L3620:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2534
	call	_ZdlPv@PLT
.L2534:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2535
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2536
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2535:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2537
	call	_ZdlPv@PLT
.L2537:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2538
	call	_ZdlPv@PLT
.L2538:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2539
	call	_ZdlPv@PLT
.L2539:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2532:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3619
.L2540:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2532
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3620
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2532
	.p2align 4,,10
	.p2align 3
.L3582:
	movq	-192(%rbp), %r14
	movq	-160(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L2635:
	testq	%r15, %r15
	je	.L2643
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2643:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2634:
	movq	152(%r13), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L2644
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2645
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L2654
	.p2align 4,,10
	.p2align 3
.L3622:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2648
	call	_ZdlPv@PLT
.L2648:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2649
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2650
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2649:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2651
	call	_ZdlPv@PLT
.L2651:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2652
	call	_ZdlPv@PLT
.L2652:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2653
	call	_ZdlPv@PLT
.L2653:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2646:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3621
.L2654:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2646
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3622
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2646
	.p2align 4,,10
	.p2align 3
.L3580:
	movq	-192(%rbp), %r14
	movq	-160(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L2571:
	testq	%r15, %r15
	je	.L2579
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2579:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2570:
	movq	152(%r13), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L2580
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2581
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L2590
	.p2align 4,,10
	.p2align 3
.L3624:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2584
	call	_ZdlPv@PLT
.L2584:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2585
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2586
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2585:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2587
	call	_ZdlPv@PLT
.L2587:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2588
	call	_ZdlPv@PLT
.L2588:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2589
	call	_ZdlPv@PLT
.L2589:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2582:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3623
.L2590:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2582
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L3624
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2582
	.p2align 4,,10
	.p2align 3
.L3590:
	movq	-192(%rbp), %r14
	movq	-160(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L2882:
	testq	%r15, %r15
	je	.L2890
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2890:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2881:
	movq	152(%r13), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L2891
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2892
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L2901
	.p2align 4,,10
	.p2align 3
.L3626:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2895
	call	_ZdlPv@PLT
.L2895:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2896
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2897
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2896:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2898
	call	_ZdlPv@PLT
.L2898:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2899
	call	_ZdlPv@PLT
.L2899:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2900
	call	_ZdlPv@PLT
.L2900:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2893:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3625
.L2901:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2893
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3626
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2893
	.p2align 4,,10
	.p2align 3
.L2780:
	movq	%rdx, %rdi
	call	*%rax
	jmp	.L2779
	.p2align 4,,10
	.p2align 3
.L2444:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L2443
	.p2align 4,,10
	.p2align 3
.L2621:
	movq	%rsi, %rdi
	call	*%rax
	jmp	.L2620
	.p2align 4,,10
	.p2align 3
.L2631:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2630
	.p2align 4,,10
	.p2align 3
.L2516:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2515
	.p2align 4,,10
	.p2align 3
.L2911:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2910
	.p2align 4,,10
	.p2align 3
.L2683:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2682
	.p2align 4,,10
	.p2align 3
.L2790:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2789
	.p2align 4,,10
	.p2align 3
.L2561:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L2560
	.p2align 4,,10
	.p2align 3
.L2450:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2449
	.p2align 4,,10
	.p2align 3
.L2872:
	movq	%rsi, %rdi
	call	*%rax
	jmp	.L2871
	.p2align 4,,10
	.p2align 3
.L2738:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2737
	.p2align 4,,10
	.p2align 3
.L2786:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L2785
	.p2align 4,,10
	.p2align 3
.L2627:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L2626
	.p2align 4,,10
	.p2align 3
.L2838:
	movq	%r12, %rdi
	call	*%rax
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L2783
	.p2align 4,,10
	.p2align 3
.L2679:
	movq	%rbx, %rdi
	call	*%rax
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L2624
	.p2align 4,,10
	.p2align 3
.L2477:
	movq	%rbx, %rdi
	call	*%rax
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L2447
	.p2align 4,,10
	.p2align 3
.L2914:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2913
	.p2align 4,,10
	.p2align 3
.L2844:
	call	*%rdx
	jmp	.L2843
	.p2align 4,,10
	.p2align 3
.L2603:
	call	*%rax
	jmp	.L2602
	.p2align 4,,10
	.p2align 3
.L2744:
	call	*%rdx
	jmp	.L2743
	.p2align 4,,10
	.p2align 3
.L2855:
	call	*%rax
	jmp	.L2854
	.p2align 4,,10
	.p2align 3
.L2746:
	call	*%rdx
	jmp	.L2745
	.p2align 4,,10
	.p2align 3
.L2458:
	call	*%rdx
	jmp	.L2457
	.p2align 4,,10
	.p2align 3
.L2456:
	call	*%rdx
	jmp	.L2455
	.p2align 4,,10
	.p2align 3
.L2569:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2568
	.p2align 4,,10
	.p2align 3
.L2519:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2518
	.p2align 4,,10
	.p2align 3
.L2485:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2484
	.p2align 4,,10
	.p2align 3
.L2880:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2879
	.p2align 4,,10
	.p2align 3
.L2567:
	call	*%rax
	jmp	.L2566
	.p2align 4,,10
	.p2align 3
.L2755:
	call	*%rax
	jmp	.L2754
	.p2align 4,,10
	.p2align 3
.L2828:
	call	*%rax
	jmp	.L2827
	.p2align 4,,10
	.p2align 3
.L2467:
	call	*%rax
	jmp	.L2466
	.p2align 4,,10
	.p2align 3
.L2721:
	call	*%rax
	jmp	.L2720
	.p2align 4,,10
	.p2align 3
.L2669:
	call	*%rax
	jmp	.L2668
	.p2align 4,,10
	.p2align 3
.L2846:
	call	*%rdx
	jmp	.L2845
	.p2align 4,,10
	.p2align 3
.L2792:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2791
	.p2align 4,,10
	.p2align 3
.L2819:
	call	*%rax
	jmp	.L2818
	.p2align 4,,10
	.p2align 3
.L2878:
	call	*%rax
	jmp	.L2877
	.p2align 4,,10
	.p2align 3
.L2633:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2632
	.p2align 4,,10
	.p2align 3
.L2483:
	call	*%rax
	jmp	.L2482
	.p2align 4,,10
	.p2align 3
.L2660:
	call	*%rax
	jmp	.L2659
	.p2align 4,,10
	.p2align 3
.L2685:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2684
	.p2align 4,,10
	.p2align 3
.L2712:
	call	*%rax
	jmp	.L2711
	.p2align 4,,10
	.p2align 3
.L2800:
	call	*%rdx
	jmp	.L2799
	.p2align 4,,10
	.p2align 3
.L2888:
	call	*%rdx
	jmp	.L2887
	.p2align 4,,10
	.p2align 3
.L2527:
	call	*%rdx
	jmp	.L2526
	.p2align 4,,10
	.p2align 3
.L2641:
	call	*%rdx
	jmp	.L2640
	.p2align 4,,10
	.p2align 3
.L2702:
	call	*%rax
	jmp	.L2701
	.p2align 4,,10
	.p2align 3
.L2931:
	call	*%rax
	jmp	.L2930
	.p2align 4,,10
	.p2align 3
.L2897:
	call	*%rax
	jmp	.L2896
	.p2align 4,,10
	.p2align 3
.L2798:
	call	*%rdx
	jmp	.L2797
	.p2align 4,,10
	.p2align 3
.L2525:
	call	*%rdx
	jmp	.L2524
	.p2align 4,,10
	.p2align 3
.L2639:
	call	*%rdx
	jmp	.L2638
	.p2align 4,,10
	.p2align 3
.L2920:
	call	*%rdx
	jmp	.L2919
	.p2align 4,,10
	.p2align 3
.L2575:
	call	*%rdx
	jmp	.L2574
	.p2align 4,,10
	.p2align 3
.L2577:
	call	*%rdx
	jmp	.L2576
	.p2align 4,,10
	.p2align 3
.L2693:
	call	*%rdx
	jmp	.L2692
	.p2align 4,,10
	.p2align 3
.L2491:
	call	*%rdx
	jmp	.L2490
	.p2align 4,,10
	.p2align 3
.L2886:
	call	*%rdx
	jmp	.L2885
	.p2align 4,,10
	.p2align 3
.L2502:
	call	*%rax
	jmp	.L2501
	.p2align 4,,10
	.p2align 3
.L2691:
	call	*%rdx
	jmp	.L2690
	.p2align 4,,10
	.p2align 3
.L2493:
	call	*%rdx
	jmp	.L2492
	.p2align 4,,10
	.p2align 3
.L2922:
	call	*%rdx
	jmp	.L2921
	.p2align 4,,10
	.p2align 3
.L2586:
	call	*%rax
	jmp	.L2585
	.p2align 4,,10
	.p2align 3
.L2650:
	call	*%rax
	jmp	.L2649
	.p2align 4,,10
	.p2align 3
.L2536:
	call	*%rax
	jmp	.L2535
	.p2align 4,,10
	.p2align 3
.L2809:
	call	*%rax
	jmp	.L2808
	.cfi_endproc
.LFE4642:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev:
.LFB4753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	%rdi, -80(%rbp)
	movq	%rcx, (%rdi)
	testq	%rbx, %rbx
	je	.L3628
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%rcx, -56(%rbp)
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3629
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, -72(%rbp)
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L3630
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -88(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rcx
	je	.L3631
	movq	%rbx, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L3688:
	movq	-64(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L3632
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3633
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L3634
	movq	(%r12), %rax
	movq	-56(%rbp), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3635
	movq	-72(%rbp), %rax
	movq	160(%r12), %r14
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L3636
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L3637
	movq	%r12, -120(%rbp)
	movq	%rbx, %r12
	movq	%rsi, %rbx
	movq	%r13, -112(%rbp)
	movq	%r14, -128(%rbp)
	jmp	.L3644
	.p2align 4,,10
	.p2align 3
.L4069:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L3640
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L3641
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3640:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L3642
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L3643
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3642:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3638:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L4068
.L3644:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L3638
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4069
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L3644
	.p2align 4,,10
	.p2align 3
.L4068:
	movq	-128(%rbp), %r14
	movq	-112(%rbp), %r13
	movq	-120(%rbp), %r12
	movq	(%r14), %rbx
.L3637:
	testq	%rbx, %rbx
	je	.L3645
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L3645:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3636:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L3646
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L3647
	movq	%r13, -112(%rbp)
	movq	%r12, -120(%rbp)
	jmp	.L3656
	.p2align 4,,10
	.p2align 3
.L4071:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3650
	call	_ZdlPv@PLT
.L3650:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3651
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L3652
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3651:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3653
	call	_ZdlPv@PLT
.L3653:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3654
	call	_ZdlPv@PLT
.L3654:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3655
	call	_ZdlPv@PLT
.L3655:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3648:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L4070
.L3656:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3648
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L4071
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L3656
	.p2align 4,,10
	.p2align 3
.L4070:
	movq	-112(%rbp), %r13
	movq	-120(%rbp), %r12
	movq	(%r15), %r14
.L3647:
	testq	%r14, %r14
	je	.L3657
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3657:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3646:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3658
	call	_ZdlPv@PLT
.L3658:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3659
	call	_ZdlPv@PLT
.L3659:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3660
	call	_ZdlPv@PLT
.L3660:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3634:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L3661
	movq	(%r12), %rax
	movq	-56(%rbp), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3662
	movq	-72(%rbp), %rax
	movq	160(%r12), %r14
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L3663
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L3664
	movq	%r12, -120(%rbp)
	movq	%rbx, %r12
	movq	%rcx, %rbx
	movq	%r13, -112(%rbp)
	movq	%r14, -128(%rbp)
	jmp	.L3671
	.p2align 4,,10
	.p2align 3
.L4073:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L3667
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L3668
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3667:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L3669
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L3670
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3669:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3665:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L4072
.L3671:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L3665
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4073
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L3671
	.p2align 4,,10
	.p2align 3
.L4072:
	movq	-128(%rbp), %r14
	movq	-112(%rbp), %r13
	movq	-120(%rbp), %r12
	movq	(%r14), %rbx
.L3664:
	testq	%rbx, %rbx
	je	.L3672
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L3672:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3663:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L3673
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L3674
	movq	%r13, -112(%rbp)
	movq	%r12, -120(%rbp)
	jmp	.L3683
	.p2align 4,,10
	.p2align 3
.L4075:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3677
	call	_ZdlPv@PLT
.L3677:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3678
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L3679
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3678:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3680
	call	_ZdlPv@PLT
.L3680:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3681
	call	_ZdlPv@PLT
.L3681:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3682
	call	_ZdlPv@PLT
.L3682:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3675:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L4074
.L3683:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3675
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L4075
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L3683
	.p2align 4,,10
	.p2align 3
.L4074:
	movq	-112(%rbp), %r13
	movq	-120(%rbp), %r12
	movq	(%r15), %r14
.L3674:
	testq	%r14, %r14
	je	.L3684
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3684:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3673:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3685
	call	_ZdlPv@PLT
.L3685:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3686
	call	_ZdlPv@PLT
.L3686:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3687
	call	_ZdlPv@PLT
.L3687:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3661:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3632:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -88(%rbp)
	jne	.L3688
	movq	-96(%rbp), %rax
	movq	-104(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L3631:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L3689
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L3689:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3630:
	movq	152(%rbx), %rax
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L3690
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -64(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L3691
	movq	%rbx, -104(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L3725:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3692
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3693
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3694
	call	_ZdlPv@PLT
.L3694:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3695
	movq	0(%r13), %rax
	movq	-56(%rbp), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3696
	movq	-72(%rbp), %rax
	movq	160(%r13), %r14
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L3697
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L3698
	movq	%r12, -96(%rbp)
	movq	%rsi, %r12
	movq	%rbx, -112(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r13, -120(%rbp)
	movq	%r14, -128(%rbp)
	jmp	.L3705
	.p2align 4,,10
	.p2align 3
.L4077:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L3701
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L3702
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3701:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L3703
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L3704
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3703:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3699:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L4076
.L3705:
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L3699
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4077
	addq	$8, %rbx
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L3705
	.p2align 4,,10
	.p2align 3
.L4076:
	movq	-128(%rbp), %r14
	movq	-96(%rbp), %r12
	movq	-112(%rbp), %rbx
	movq	-120(%rbp), %r13
	movq	(%r14), %r15
.L3698:
	testq	%r15, %r15
	je	.L3706
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3706:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3697:
	movq	152(%r13), %rax
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L3707
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L3708
	movq	%r12, -112(%rbp)
	movq	%r13, -120(%rbp)
	jmp	.L3717
	.p2align 4,,10
	.p2align 3
.L4079:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3711
	call	_ZdlPv@PLT
.L3711:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3712
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L3713
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3712:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3714
	call	_ZdlPv@PLT
.L3714:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3715
	call	_ZdlPv@PLT
.L3715:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3716
	call	_ZdlPv@PLT
.L3716:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3709:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L4078
.L3717:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3709
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L4079
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L3717
	.p2align 4,,10
	.p2align 3
.L4078:
	movq	-96(%rbp), %rax
	movq	-112(%rbp), %r12
	movq	-120(%rbp), %r13
	movq	(%rax), %r14
.L3708:
	testq	%r14, %r14
	je	.L3718
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3718:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3707:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3719
	call	_ZdlPv@PLT
.L3719:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3720
	call	_ZdlPv@PLT
.L3720:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3721
	call	_ZdlPv@PLT
.L3721:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3695:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3722
	call	_ZdlPv@PLT
.L3722:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3723
	call	_ZdlPv@PLT
.L3723:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3724
	call	_ZdlPv@PLT
.L3724:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3692:
	addq	$8, %rbx
	cmpq	%rbx, -64(%rbp)
	jne	.L3725
	movq	-88(%rbp), %rax
	movq	-104(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L3691:
	testq	%rdi, %rdi
	je	.L3726
	call	_ZdlPv@PLT
.L3726:
	movq	-88(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3690:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3727
	call	_ZdlPv@PLT
.L3727:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3728
	call	_ZdlPv@PLT
.L3728:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3729
	call	_ZdlPv@PLT
.L3729:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L3628:
	movq	-80(%rbp), %rax
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	je	.L3627
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%rcx, -56(%rbp)
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3731
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, -72(%rbp)
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L3732
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -80(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rsi
	je	.L3733
	movq	%rbx, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L3790:
	movq	-64(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L3734
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3735
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L3736
	movq	(%r12), %rax
	movq	-56(%rbp), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3737
	movq	-72(%rbp), %rax
	movq	160(%r12), %r14
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L3738
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L3739
	movq	%r12, -112(%rbp)
	movq	%rbx, %r12
	movq	%rcx, %rbx
	movq	%r13, -104(%rbp)
	movq	%r14, -120(%rbp)
	jmp	.L3746
	.p2align 4,,10
	.p2align 3
.L4081:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L3742
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L3743
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3742:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L3744
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L3745
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3744:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3740:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L4080
.L3746:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L3740
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4081
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L3746
	.p2align 4,,10
	.p2align 3
.L4080:
	movq	-120(%rbp), %r14
	movq	-104(%rbp), %r13
	movq	-112(%rbp), %r12
	movq	(%r14), %rbx
.L3739:
	testq	%rbx, %rbx
	je	.L3747
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L3747:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3738:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L3748
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L3749
	movq	%r13, -104(%rbp)
	movq	%r12, -112(%rbp)
	jmp	.L3758
	.p2align 4,,10
	.p2align 3
.L4083:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3752
	call	_ZdlPv@PLT
.L3752:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3753
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L3754
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3753:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3755
	call	_ZdlPv@PLT
.L3755:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3756
	call	_ZdlPv@PLT
.L3756:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3757
	call	_ZdlPv@PLT
.L3757:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3750:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L4082
.L3758:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3750
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L4083
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L3758
	.p2align 4,,10
	.p2align 3
.L4082:
	movq	-104(%rbp), %r13
	movq	-112(%rbp), %r12
	movq	(%r15), %r14
.L3749:
	testq	%r14, %r14
	je	.L3759
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3759:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3748:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3760
	call	_ZdlPv@PLT
.L3760:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3761
	call	_ZdlPv@PLT
.L3761:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3762
	call	_ZdlPv@PLT
.L3762:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3736:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L3763
	movq	(%r12), %rax
	movq	-56(%rbp), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3764
	movq	-72(%rbp), %rax
	movq	160(%r12), %r14
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L3765
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L3766
	movq	%r12, -112(%rbp)
	movq	%rbx, %r12
	movq	%rsi, %rbx
	movq	%r13, -104(%rbp)
	movq	%r14, -120(%rbp)
	jmp	.L3773
	.p2align 4,,10
	.p2align 3
.L4085:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L3769
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L3770
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3769:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L3771
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L3772
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3771:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3767:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L4084
.L3773:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L3767
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4085
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L3773
	.p2align 4,,10
	.p2align 3
.L4084:
	movq	-120(%rbp), %r14
	movq	-104(%rbp), %r13
	movq	-112(%rbp), %r12
	movq	(%r14), %rbx
.L3766:
	testq	%rbx, %rbx
	je	.L3774
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L3774:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3765:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L3775
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L3776
	movq	%r13, -104(%rbp)
	movq	%r12, -112(%rbp)
	jmp	.L3785
	.p2align 4,,10
	.p2align 3
.L4087:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3779
	call	_ZdlPv@PLT
.L3779:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3780
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L3781
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3780:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3782
	call	_ZdlPv@PLT
.L3782:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3783
	call	_ZdlPv@PLT
.L3783:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3784
	call	_ZdlPv@PLT
.L3784:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3777:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L4086
.L3785:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3777
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L4087
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L3785
	.p2align 4,,10
	.p2align 3
.L4086:
	movq	-104(%rbp), %r13
	movq	-112(%rbp), %r12
	movq	(%r15), %r14
.L3776:
	testq	%r14, %r14
	je	.L3786
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3786:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3775:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3787
	call	_ZdlPv@PLT
.L3787:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3788
	call	_ZdlPv@PLT
.L3788:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3789
	call	_ZdlPv@PLT
.L3789:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3763:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3734:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -80(%rbp)
	jne	.L3790
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L3733:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L3791
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L3791:
	movq	-88(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3732:
	movq	152(%rbx), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L3792
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -64(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L3793
	movq	%rbx, -96(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L3827:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3794
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3795
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3796
	call	_ZdlPv@PLT
.L3796:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3797
	movq	0(%r13), %rax
	movq	-56(%rbp), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3798
	movq	-72(%rbp), %rax
	movq	160(%r13), %r14
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L3799
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L3800
	movq	%r12, -88(%rbp)
	movq	%rcx, %r12
	movq	%rbx, -104(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r13, -112(%rbp)
	movq	%r14, -120(%rbp)
	jmp	.L3807
	.p2align 4,,10
	.p2align 3
.L4089:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L3803
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L3804
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3803:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L3805
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L3806
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3805:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3801:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L4088
.L3807:
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L3801
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4089
	addq	$8, %rbx
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L3807
	.p2align 4,,10
	.p2align 3
.L4088:
	movq	-120(%rbp), %r14
	movq	-88(%rbp), %r12
	movq	-104(%rbp), %rbx
	movq	-112(%rbp), %r13
	movq	(%r14), %r15
.L3800:
	testq	%r15, %r15
	je	.L3808
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3808:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3799:
	movq	152(%r13), %rax
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L3809
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L3810
	movq	%r12, -104(%rbp)
	movq	%r13, -112(%rbp)
	jmp	.L3819
	.p2align 4,,10
	.p2align 3
.L4091:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3813
	call	_ZdlPv@PLT
.L3813:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3814
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L3815
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3814:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3816
	call	_ZdlPv@PLT
.L3816:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3817
	call	_ZdlPv@PLT
.L3817:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3818
	call	_ZdlPv@PLT
.L3818:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3811:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L4090
.L3819:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3811
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L4091
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L3819
	.p2align 4,,10
	.p2align 3
.L4090:
	movq	-88(%rbp), %rax
	movq	-104(%rbp), %r12
	movq	-112(%rbp), %r13
	movq	(%rax), %r14
.L3810:
	testq	%r14, %r14
	je	.L3820
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3820:
	movq	-88(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3809:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3821
	call	_ZdlPv@PLT
.L3821:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3822
	call	_ZdlPv@PLT
.L3822:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3823
	call	_ZdlPv@PLT
.L3823:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3797:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3824
	call	_ZdlPv@PLT
.L3824:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3825
	call	_ZdlPv@PLT
.L3825:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3826
	call	_ZdlPv@PLT
.L3826:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3794:
	addq	$8, %rbx
	cmpq	%rbx, -64(%rbp)
	jne	.L3827
	movq	-80(%rbp), %rax
	movq	-96(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L3793:
	testq	%rdi, %rdi
	je	.L3828
	call	_ZdlPv@PLT
.L3828:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3792:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3829
	call	_ZdlPv@PLT
.L3829:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3830
	call	_ZdlPv@PLT
.L3830:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3831
	call	_ZdlPv@PLT
.L3831:
	addq	$88, %rsp
	movq	%rbx, %rdi
	movl	$168, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L3627:
	.cfi_restore_state
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3735:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3734
	.p2align 4,,10
	.p2align 3
.L3633:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3632
	.p2align 4,,10
	.p2align 3
.L3693:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3692
	.p2align 4,,10
	.p2align 3
.L3795:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3794
	.p2align 4,,10
	.p2align 3
.L3731:
	addq	$88, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L3629:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L3628
	.p2align 4,,10
	.p2align 3
.L3764:
	movq	%r12, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3734
	.p2align 4,,10
	.p2align 3
.L3635:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3634
	.p2align 4,,10
	.p2align 3
.L3696:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3695
	.p2align 4,,10
	.p2align 3
.L3662:
	movq	%r12, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3632
	.p2align 4,,10
	.p2align 3
.L3737:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3736
	.p2align 4,,10
	.p2align 3
.L3798:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3797
	.p2align 4,,10
	.p2align 3
.L3772:
	call	*%rdx
	jmp	.L3771
	.p2align 4,,10
	.p2align 3
.L3754:
	call	*%rax
	jmp	.L3753
	.p2align 4,,10
	.p2align 3
.L3770:
	call	*%rdx
	jmp	.L3769
	.p2align 4,,10
	.p2align 3
.L3670:
	call	*%rdx
	jmp	.L3669
	.p2align 4,,10
	.p2align 3
.L3679:
	call	*%rax
	jmp	.L3678
	.p2align 4,,10
	.p2align 3
.L3668:
	call	*%rdx
	jmp	.L3667
	.p2align 4,,10
	.p2align 3
.L3804:
	call	*%rdx
	jmp	.L3803
	.p2align 4,,10
	.p2align 3
.L3702:
	call	*%rdx
	jmp	.L3701
	.p2align 4,,10
	.p2align 3
.L3781:
	call	*%rax
	jmp	.L3780
	.p2align 4,,10
	.p2align 3
.L3745:
	call	*%rdx
	jmp	.L3744
	.p2align 4,,10
	.p2align 3
.L3743:
	call	*%rdx
	jmp	.L3742
	.p2align 4,,10
	.p2align 3
.L3652:
	call	*%rax
	jmp	.L3651
	.p2align 4,,10
	.p2align 3
.L3641:
	call	*%rdx
	jmp	.L3640
	.p2align 4,,10
	.p2align 3
.L3713:
	call	*%rax
	jmp	.L3712
	.p2align 4,,10
	.p2align 3
.L3643:
	call	*%rdx
	jmp	.L3642
	.p2align 4,,10
	.p2align 3
.L3815:
	call	*%rax
	jmp	.L3814
	.p2align 4,,10
	.p2align 3
.L3704:
	call	*%rdx
	jmp	.L3703
	.p2align 4,,10
	.p2align 3
.L3806:
	call	*%rdx
	jmp	.L3805
	.cfi_endproc
.LFE4753:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev:
.LFB4713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%rax, (%rdi)
	movq	152(%rdi), %rdi
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L4093
	call	_ZdlPv@PLT
.L4093:
	movq	136(%rbx), %rcx
	movq	%rcx, -64(%rbp)
	testq	%rcx, %rcx
	je	.L4094
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L4095
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L4096
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -72(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rsi
	je	.L4097
	movq	%rbx, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L4154:
	movq	-56(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L4098
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L4099
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L4100
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4101
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L4102
	movq	8(%r14), %rax
	movq	(%r14), %rbx
	cmpq	%rbx, %rax
	je	.L4103
	movq	%r12, -104(%rbp)
	movq	%rbx, %r12
	movq	%rax, %rbx
	movq	%r13, -96(%rbp)
	jmp	.L4110
	.p2align 4,,10
	.p2align 3
.L4320:
	movq	16(%r15), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r13, %r13
	je	.L4106
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4107
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4106:
	movq	8(%r15), %r13
	testq	%r13, %r13
	je	.L4108
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4109
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4108:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L4104:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L4319
.L4110:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L4104
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4320
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%r12, %rbx
	jne	.L4110
	.p2align 4,,10
	.p2align 3
.L4319:
	movq	-96(%rbp), %r13
	movq	-104(%rbp), %r12
	movq	(%r14), %rbx
.L4103:
	testq	%rbx, %rbx
	je	.L4111
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L4111:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4102:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L4112
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L4113
	movq	%r13, -96(%rbp)
	movq	%r12, -104(%rbp)
	jmp	.L4122
	.p2align 4,,10
	.p2align 3
.L4322:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4116
	call	_ZdlPv@PLT
.L4116:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L4117
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4118
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4117:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4119
	call	_ZdlPv@PLT
.L4119:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4120
	call	_ZdlPv@PLT
.L4120:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4121
	call	_ZdlPv@PLT
.L4121:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4114:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L4321
.L4122:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L4114
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L4322
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L4122
	.p2align 4,,10
	.p2align 3
.L4321:
	movq	-96(%rbp), %r13
	movq	-104(%rbp), %r12
	movq	(%r15), %r14
.L4113:
	testq	%r14, %r14
	je	.L4123
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4123:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L4112:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4124
	call	_ZdlPv@PLT
.L4124:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4125
	call	_ZdlPv@PLT
.L4125:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4126
	call	_ZdlPv@PLT
.L4126:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4100:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L4127
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4128
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L4129
	movq	8(%r14), %rax
	movq	(%r14), %rbx
	cmpq	%rbx, %rax
	je	.L4130
	movq	%r12, -104(%rbp)
	movq	%rbx, %r12
	movq	%rax, %rbx
	movq	%r13, -96(%rbp)
	jmp	.L4137
	.p2align 4,,10
	.p2align 3
.L4324:
	movq	16(%r15), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r13, %r13
	je	.L4133
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4134
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4133:
	movq	8(%r15), %r13
	testq	%r13, %r13
	je	.L4135
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4136
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4135:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L4131:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L4323
.L4137:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L4131
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4324
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%r12, %rbx
	jne	.L4137
	.p2align 4,,10
	.p2align 3
.L4323:
	movq	-96(%rbp), %r13
	movq	-104(%rbp), %r12
	movq	(%r14), %rbx
.L4130:
	testq	%rbx, %rbx
	je	.L4138
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L4138:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4129:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L4139
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L4140
	movq	%r13, -96(%rbp)
	movq	%r12, -104(%rbp)
	jmp	.L4149
	.p2align 4,,10
	.p2align 3
.L4326:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4143
	call	_ZdlPv@PLT
.L4143:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L4144
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4145
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4144:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4146
	call	_ZdlPv@PLT
.L4146:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4147
	call	_ZdlPv@PLT
.L4147:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4148
	call	_ZdlPv@PLT
.L4148:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4141:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L4325
.L4149:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L4141
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L4326
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L4149
	.p2align 4,,10
	.p2align 3
.L4325:
	movq	-96(%rbp), %r13
	movq	-104(%rbp), %r12
	movq	(%r15), %r14
.L4140:
	testq	%r14, %r14
	je	.L4150
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4150:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L4139:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4151
	call	_ZdlPv@PLT
.L4151:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4152
	call	_ZdlPv@PLT
.L4152:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4153
	call	_ZdlPv@PLT
.L4153:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4127:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4098:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -72(%rbp)
	jne	.L4154
	movq	-80(%rbp), %rax
	movq	-88(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
.L4097:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L4155
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L4155:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4096:
	movq	-64(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L4156
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -56(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L4157
	movq	%rbx, -88(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L4191:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L4158
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L4159
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4160
	call	_ZdlPv@PLT
.L4160:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L4161
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L4162
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L4163
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L4164
	movq	%rbx, -96(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -80(%rbp)
	movq	%r13, -104(%rbp)
	jmp	.L4171
	.p2align 4,,10
	.p2align 3
.L4328:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L4167
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4168
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4167:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L4169
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4170
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4169:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4165:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L4327
.L4171:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L4165
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4328
	addq	$8, %rbx
	movq	%r12, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L4171
	.p2align 4,,10
	.p2align 3
.L4327:
	movq	-80(%rbp), %r12
	movq	-96(%rbp), %rbx
	movq	-104(%rbp), %r13
	movq	(%r14), %r15
.L4164:
	testq	%r15, %r15
	je	.L4172
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L4172:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4163:
	movq	152(%r13), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L4173
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L4174
	movq	%r12, -96(%rbp)
	movq	%r13, -104(%rbp)
	jmp	.L4183
	.p2align 4,,10
	.p2align 3
.L4330:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4177
	call	_ZdlPv@PLT
.L4177:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L4178
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4179
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4178:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4180
	call	_ZdlPv@PLT
.L4180:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4181
	call	_ZdlPv@PLT
.L4181:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4182
	call	_ZdlPv@PLT
.L4182:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4175:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L4329
.L4183:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L4175
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L4330
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L4183
	.p2align 4,,10
	.p2align 3
.L4329:
	movq	-80(%rbp), %rax
	movq	-96(%rbp), %r12
	movq	-104(%rbp), %r13
	movq	(%rax), %r14
.L4174:
	testq	%r14, %r14
	je	.L4184
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4184:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4173:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4185
	call	_ZdlPv@PLT
.L4185:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4186
	call	_ZdlPv@PLT
.L4186:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4187
	call	_ZdlPv@PLT
.L4187:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4161:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4188
	call	_ZdlPv@PLT
.L4188:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4189
	call	_ZdlPv@PLT
.L4189:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4190
	call	_ZdlPv@PLT
.L4190:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4158:
	addq	$8, %rbx
	cmpq	%rbx, -56(%rbp)
	jne	.L4191
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L4157:
	testq	%rdi, %rdi
	je	.L4192
	call	_ZdlPv@PLT
.L4192:
	movq	-72(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4156:
	movq	-64(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L4193
	call	_ZdlPv@PLT
.L4193:
	movq	-64(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L4194
	call	_ZdlPv@PLT
.L4194:
	movq	-64(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L4195
	call	_ZdlPv@PLT
.L4195:
	movq	-64(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L4094:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L4196
	call	_ZdlPv@PLT
.L4196:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L4197
	call	_ZdlPv@PLT
.L4197:
	movq	8(%rbx), %r8
	leaq	24(%rbx), %rdi
	cmpq	%rdi, %r8
	je	.L4092
	addq	$72, %rsp
	movq	%r8, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L4092:
	.cfi_restore_state
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4099:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L4098
	.p2align 4,,10
	.p2align 3
.L4159:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4158
	.p2align 4,,10
	.p2align 3
.L4095:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L4094
	.p2align 4,,10
	.p2align 3
.L4101:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4100
	.p2align 4,,10
	.p2align 3
.L4162:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L4161
	.p2align 4,,10
	.p2align 3
.L4128:
	movq	%r12, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L4098
	.p2align 4,,10
	.p2align 3
.L4107:
	call	*%rdx
	jmp	.L4106
	.p2align 4,,10
	.p2align 3
.L4118:
	call	*%rax
	jmp	.L4117
	.p2align 4,,10
	.p2align 3
.L4145:
	call	*%rax
	jmp	.L4144
	.p2align 4,,10
	.p2align 3
.L4134:
	call	*%rdx
	jmp	.L4133
	.p2align 4,,10
	.p2align 3
.L4170:
	call	*%rdx
	jmp	.L4169
	.p2align 4,,10
	.p2align 3
.L4179:
	call	*%rax
	jmp	.L4178
	.p2align 4,,10
	.p2align 3
.L4136:
	call	*%rdx
	jmp	.L4135
	.p2align 4,,10
	.p2align 3
.L4168:
	call	*%rdx
	jmp	.L4167
	.p2align 4,,10
	.p2align 3
.L4109:
	call	*%rdx
	jmp	.L4108
	.cfi_endproc
.LFE4713:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev, @function
_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev:
.LFB4457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rax
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	312(%rdi), %r12
	movups	%xmm0, (%rdi)
	testq	%r12, %r12
	je	.L4332
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4333
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	56(%r12), %rdi
	movq	%rax, (%r12)
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4334
	call	_ZdlPv@PLT
.L4334:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4335
	call	_ZdlPv@PLT
.L4335:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4332:
	movq	304(%rbx), %rcx
	movq	%rcx, -64(%rbp)
	testq	%rcx, %rcx
	je	.L4336
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L4337
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L4338
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -80(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rsi
	je	.L4339
	movq	%rbx, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L4546:
	movq	-56(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -72(%rbp)
	testq	%rcx, %rcx
	je	.L4340
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4341
	movq	16(%rcx), %rbx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	testq	%rbx, %rbx
	je	.L4342
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4343
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L4344
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -88(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L4345
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L4402:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L4346
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4347
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L4348
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L4349
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L4350
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	cmpq	%r14, %rcx
	je	.L4351
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rcx, %r14
	movq	%r12, -136(%rbp)
	jmp	.L4358
	.p2align 4,,10
	.p2align 3
.L5058:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L4354
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4355
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4354:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L4356
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4357
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4356:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L4352:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L5057
.L4358:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L4352
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L5058
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L4352
	.p2align 4,,10
	.p2align 3
.L5081:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L4524:
	testq	%r14, %r14
	je	.L4534
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4534:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4523:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4535
	call	_ZdlPv@PLT
.L4535:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4536
	call	_ZdlPv@PLT
.L4536:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4537
	call	_ZdlPv@PLT
.L4537:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4511:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4538
	call	_ZdlPv@PLT
.L4538:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4539
	call	_ZdlPv@PLT
.L4539:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4540
	call	_ZdlPv@PLT
.L4540:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4508:
	addq	$8, %rbx
	cmpq	%rbx, -88(%rbp)
	jne	.L4541
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L4507:
	testq	%rdi, %rdi
	je	.L4542
	call	_ZdlPv@PLT
.L4542:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4506:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L4543
	call	_ZdlPv@PLT
.L4543:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L4544
	call	_ZdlPv@PLT
.L4544:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L4545
	call	_ZdlPv@PLT
.L4545:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L4444:
	movq	-72(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4340:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -80(%rbp)
	jne	.L4546
	movq	-96(%rbp), %rax
	movq	-120(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
.L4339:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L4547
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L4547:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4338:
	movq	-64(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L4548
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -80(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rcx
	je	.L4549
	movq	%rbx, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L4658:
	movq	-56(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L4550
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4551
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L4552
	call	_ZdlPv@PLT
.L4552:
	movq	136(%rbx), %rsi
	movq	%rsi, -72(%rbp)
	testq	%rsi, %rsi
	je	.L4553
	movq	(%rsi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4554
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rsi)
	movq	160(%rsi), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L4555
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -88(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L4556
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L4613:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L4557
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4558
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L4559
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4560
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L4561
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	cmpq	%r14, %rcx
	je	.L4562
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rcx, %r14
	movq	%r12, -136(%rbp)
	jmp	.L4569
	.p2align 4,,10
	.p2align 3
.L5060:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L4565
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4566
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4565:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L4567
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4568
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4567:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L4563:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L5059
.L4569:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L4563
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L5060
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L4563
	.p2align 4,,10
	.p2align 3
.L5087:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L4633:
	testq	%r14, %r14
	je	.L4643
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4643:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4632:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4644
	call	_ZdlPv@PLT
.L4644:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4645
	call	_ZdlPv@PLT
.L4645:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4646
	call	_ZdlPv@PLT
.L4646:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4620:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4647
	call	_ZdlPv@PLT
.L4647:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4648
	call	_ZdlPv@PLT
.L4648:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4649
	call	_ZdlPv@PLT
.L4649:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4617:
	addq	$8, %rbx
	cmpq	%rbx, -88(%rbp)
	jne	.L4650
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L4616:
	testq	%rdi, %rdi
	je	.L4651
	call	_ZdlPv@PLT
.L4651:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4615:
	movq	-72(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L4652
	call	_ZdlPv@PLT
.L4652:
	movq	-72(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L4653
	call	_ZdlPv@PLT
.L4653:
	movq	-72(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L4654
	call	_ZdlPv@PLT
.L4654:
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L4553:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L4655
	call	_ZdlPv@PLT
.L4655:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L4656
	call	_ZdlPv@PLT
.L4656:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L4657
	call	_ZdlPv@PLT
.L4657:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L4550:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -80(%rbp)
	jne	.L4658
	movq	-96(%rbp), %rax
	movq	-120(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
.L4549:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L4659
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L4659:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4548:
	movq	-64(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L4660
	call	_ZdlPv@PLT
.L4660:
	movq	-64(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L4661
	call	_ZdlPv@PLT
.L4661:
	movq	-64(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L4662
	call	_ZdlPv@PLT
.L4662:
	movq	-64(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L4336:
	movq	264(%rbx), %rdi
	leaq	280(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L4663
	call	_ZdlPv@PLT
.L4663:
	movq	216(%rbx), %rdi
	leaq	232(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L4664
	call	_ZdlPv@PLT
.L4664:
	movq	168(%rbx), %rdi
	leaq	184(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L4665
	call	_ZdlPv@PLT
.L4665:
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L4666
	movq	(%rdi), %rax
	call	*24(%rax)
.L4666:
	movq	112(%rbx), %rdi
	leaq	128(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L4667
	call	_ZdlPv@PLT
.L4667:
	movq	64(%rbx), %rdi
	leaq	80(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L4668
	call	_ZdlPv@PLT
.L4668:
	movq	16(%rbx), %r8
	leaq	32(%rbx), %rdi
	cmpq	%rdi, %r8
	je	.L4331
	addq	$104, %rsp
	movq	%r8, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L4331:
	.cfi_restore_state
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4558:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L4557:
	addq	$8, %rbx
	cmpq	%rbx, -88(%rbp)
	jne	.L4613
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L4556:
	testq	%rdi, %rdi
	je	.L4614
	call	_ZdlPv@PLT
.L4614:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4555:
	movq	-72(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L4615
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -88(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L4616
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L4650:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L4617
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L4618
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4619
	call	_ZdlPv@PLT
.L4619:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L4620
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L4621
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L4622
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L4623
	movq	%rbx, -136(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -112(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L4630
	.p2align 4,,10
	.p2align 3
.L5062:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L4626
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4627
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4626:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L4628
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4629
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4628:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4624:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L5061
.L4630:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L4624
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L5062
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L4624
	.p2align 4,,10
	.p2align 3
.L4347:
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L4346:
	addq	$8, %rbx
	cmpq	%rbx, -88(%rbp)
	jne	.L4402
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L4345:
	testq	%rdi, %rdi
	je	.L4403
	call	_ZdlPv@PLT
.L4403:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4344:
	movq	152(%rbx), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L4404
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -88(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L4405
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L4439:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L4406
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4407
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4408
	call	_ZdlPv@PLT
.L4408:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L4409
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L4410
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L4411
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L4412
	movq	%rbx, -136(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -112(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L4419
	.p2align 4,,10
	.p2align 3
.L5064:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L4415
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4416
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4415:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L4417
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4418
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4417:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4413:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L5063
.L4419:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L4413
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L5064
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L4413
	.p2align 4,,10
	.p2align 3
.L5091:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L4422:
	testq	%r14, %r14
	je	.L4432
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4432:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4421:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4433
	call	_ZdlPv@PLT
.L4433:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4434
	call	_ZdlPv@PLT
.L4434:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4435
	call	_ZdlPv@PLT
.L4435:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4409:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4436
	call	_ZdlPv@PLT
.L4436:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4437
	call	_ZdlPv@PLT
.L4437:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4438
	call	_ZdlPv@PLT
.L4438:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4406:
	addq	$8, %rbx
	cmpq	%rbx, -88(%rbp)
	jne	.L4439
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L4405:
	testq	%rdi, %rdi
	je	.L4440
	call	_ZdlPv@PLT
.L4440:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4404:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L4441
	call	_ZdlPv@PLT
.L4441:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L4442
	call	_ZdlPv@PLT
.L4442:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L4443
	call	_ZdlPv@PLT
.L4443:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L4342:
	movq	-72(%rbp), %rax
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	je	.L4444
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4445
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L4446
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -88(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L4447
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L4504:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L4448
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L4449
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L4450
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4451
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L4452
	movq	8(%rax), %rsi
	movq	(%rax), %r14
	cmpq	%r14, %rsi
	je	.L4453
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rsi, %r14
	movq	%r12, -136(%rbp)
	jmp	.L4460
	.p2align 4,,10
	.p2align 3
.L5066:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L4456
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4457
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4456:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L4458
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4459
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4458:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L4454:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L5065
.L4460:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L4454
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L5066
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L4454
	.p2align 4,,10
	.p2align 3
.L4449:
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L4448:
	addq	$8, %rbx
	cmpq	%rbx, -88(%rbp)
	jne	.L4504
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L4447:
	testq	%rdi, %rdi
	je	.L4505
	call	_ZdlPv@PLT
.L4505:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4446:
	movq	152(%rbx), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L4506
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -88(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L4507
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L4541:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L4508
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4509
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4510
	call	_ZdlPv@PLT
.L4510:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L4511
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L4512
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L4513
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L4514
	movq	%rbx, -136(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -112(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L4521
	.p2align 4,,10
	.p2align 3
.L5068:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L4517
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4518
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4517:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L4519
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4520
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4519:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4515:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L5067
.L4521:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L4515
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L5068
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L4515
	.p2align 4,,10
	.p2align 3
.L5083:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L4463:
	testq	%r14, %r14
	je	.L4473
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4473:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4462:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4474
	call	_ZdlPv@PLT
.L4474:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4475
	call	_ZdlPv@PLT
.L4475:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4476
	call	_ZdlPv@PLT
.L4476:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4450:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L4477
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4478
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L4479
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	cmpq	%r14, %rdx
	je	.L4480
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rdx, %r14
	movq	%r12, -136(%rbp)
	jmp	.L4487
	.p2align 4,,10
	.p2align 3
.L5070:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L4483
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4484
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4483:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L4485
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4486
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4485:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L4481:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L5069
.L4487:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L4481
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L5070
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L4481
	.p2align 4,,10
	.p2align 3
.L5079:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L4490:
	testq	%r14, %r14
	je	.L4500
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4500:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4489:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4501
	call	_ZdlPv@PLT
.L4501:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4502
	call	_ZdlPv@PLT
.L4502:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4503
	call	_ZdlPv@PLT
.L4503:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4477:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L4448
	.p2align 4,,10
	.p2align 3
.L5089:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L4361:
	testq	%r14, %r14
	je	.L4371
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4371:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4360:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4372
	call	_ZdlPv@PLT
.L4372:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4373
	call	_ZdlPv@PLT
.L4373:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4374
	call	_ZdlPv@PLT
.L4374:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4348:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L4375
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4376
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L4377
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	cmpq	%r14, %rcx
	je	.L4378
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rcx, %r14
	movq	%r12, -136(%rbp)
	jmp	.L4385
	.p2align 4,,10
	.p2align 3
.L5072:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L4381
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4382
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4381:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L4383
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4384
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4383:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L4379:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L5071
.L4385:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L4379
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L5072
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L4379
	.p2align 4,,10
	.p2align 3
.L5077:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L4388:
	testq	%r14, %r14
	je	.L4398
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4398:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4387:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4399
	call	_ZdlPv@PLT
.L4399:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4400
	call	_ZdlPv@PLT
.L4400:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4401
	call	_ZdlPv@PLT
.L4401:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4375:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L4346
	.p2align 4,,10
	.p2align 3
.L5085:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L4572:
	testq	%r14, %r14
	je	.L4582
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4582:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4571:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4583
	call	_ZdlPv@PLT
.L4583:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4584
	call	_ZdlPv@PLT
.L4584:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4585
	call	_ZdlPv@PLT
.L4585:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4559:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L4586
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4587
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L4588
	movq	8(%rax), %rsi
	movq	(%rax), %r14
	cmpq	%r14, %rsi
	je	.L4589
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rsi, %r14
	movq	%r12, -136(%rbp)
	jmp	.L4596
	.p2align 4,,10
	.p2align 3
.L5074:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L4592
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4593
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4592:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L4594
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4595
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4594:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L4590:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L5073
.L4596:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L4590
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L5074
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L4590
	.p2align 4,,10
	.p2align 3
.L5075:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L4599:
	testq	%r14, %r14
	je	.L4609
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4609:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4598:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4610
	call	_ZdlPv@PLT
.L4610:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4611
	call	_ZdlPv@PLT
.L4611:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4612
	call	_ZdlPv@PLT
.L4612:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4586:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L4557
	.p2align 4,,10
	.p2align 3
.L5073:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	(%rax), %r14
.L4589:
	testq	%r14, %r14
	je	.L4597
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4597:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4588:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L4598
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L4599
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L4608
	.p2align 4,,10
	.p2align 3
.L5076:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4602
	call	_ZdlPv@PLT
.L4602:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L4603
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4604
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4603:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4605
	call	_ZdlPv@PLT
.L4605:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4606
	call	_ZdlPv@PLT
.L4606:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4607
	call	_ZdlPv@PLT
.L4607:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4600:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L5075
.L4608:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L4600
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L5076
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4600
	.p2align 4,,10
	.p2align 3
.L5071:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	(%rax), %r14
.L4378:
	testq	%r14, %r14
	je	.L4386
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4386:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4377:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L4387
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L4388
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L4397
	.p2align 4,,10
	.p2align 3
.L5078:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4391
	call	_ZdlPv@PLT
.L4391:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L4392
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4393
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4392:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4394
	call	_ZdlPv@PLT
.L4394:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4395
	call	_ZdlPv@PLT
.L4395:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4396
	call	_ZdlPv@PLT
.L4396:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4389:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L5077
.L4397:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L4389
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L5078
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4389
	.p2align 4,,10
	.p2align 3
.L5069:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	(%rax), %r14
.L4480:
	testq	%r14, %r14
	je	.L4488
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4488:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4479:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L4489
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L4490
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L4499
	.p2align 4,,10
	.p2align 3
.L5080:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4493
	call	_ZdlPv@PLT
.L4493:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L4494
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4495
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4494:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4496
	call	_ZdlPv@PLT
.L4496:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4497
	call	_ZdlPv@PLT
.L4497:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4498
	call	_ZdlPv@PLT
.L4498:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4491:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L5079
.L4499:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L4491
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L5080
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4491
	.p2align 4,,10
	.p2align 3
.L5067:
	movq	-112(%rbp), %r12
	movq	-136(%rbp), %rbx
	movq	-144(%rbp), %r13
	movq	(%r14), %r15
.L4514:
	testq	%r15, %r15
	je	.L4522
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L4522:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4513:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L4523
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L4524
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L4533
	.p2align 4,,10
	.p2align 3
.L5082:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4527
	call	_ZdlPv@PLT
.L4527:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L4528
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4529
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4528:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4530
	call	_ZdlPv@PLT
.L4530:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4531
	call	_ZdlPv@PLT
.L4531:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4532
	call	_ZdlPv@PLT
.L4532:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4525:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L5081
.L4533:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L4525
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L5082
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4525
	.p2align 4,,10
	.p2align 3
.L5065:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	(%rax), %r14
.L4453:
	testq	%r14, %r14
	je	.L4461
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4461:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4452:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L4462
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L4463
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L4472
	.p2align 4,,10
	.p2align 3
.L5084:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4466
	call	_ZdlPv@PLT
.L4466:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L4467
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L4468
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4467:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4469
	call	_ZdlPv@PLT
.L4469:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4470
	call	_ZdlPv@PLT
.L4470:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4471
	call	_ZdlPv@PLT
.L4471:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4464:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L5083
.L4472:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L4464
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L5084
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4464
	.p2align 4,,10
	.p2align 3
.L5059:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	(%rax), %r14
.L4562:
	testq	%r14, %r14
	je	.L4570
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4570:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4561:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L4571
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L4572
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L4581
	.p2align 4,,10
	.p2align 3
.L5086:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4575
	call	_ZdlPv@PLT
.L4575:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L4576
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L4577
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4576:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4578
	call	_ZdlPv@PLT
.L4578:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4579
	call	_ZdlPv@PLT
.L4579:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4580
	call	_ZdlPv@PLT
.L4580:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4573:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L5085
.L4581:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L4573
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L5086
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4573
	.p2align 4,,10
	.p2align 3
.L5061:
	movq	-112(%rbp), %r12
	movq	-136(%rbp), %rbx
	movq	-144(%rbp), %r13
	movq	(%r14), %r15
.L4623:
	testq	%r15, %r15
	je	.L4631
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L4631:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4622:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L4632
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L4633
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L4642
	.p2align 4,,10
	.p2align 3
.L5088:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4636
	call	_ZdlPv@PLT
.L4636:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L4637
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4638
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4637:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4639
	call	_ZdlPv@PLT
.L4639:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4640
	call	_ZdlPv@PLT
.L4640:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4641
	call	_ZdlPv@PLT
.L4641:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4634:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L5087
.L4642:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L4634
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L5088
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4634
	.p2align 4,,10
	.p2align 3
.L5057:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	(%rax), %r14
.L4351:
	testq	%r14, %r14
	je	.L4359
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4359:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4350:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L4360
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L4361
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L4370
	.p2align 4,,10
	.p2align 3
.L5090:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4364
	call	_ZdlPv@PLT
.L4364:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L4365
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4366
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4365:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4367
	call	_ZdlPv@PLT
.L4367:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4368
	call	_ZdlPv@PLT
.L4368:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4369
	call	_ZdlPv@PLT
.L4369:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4362:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L5089
.L4370:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L4362
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L5090
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4362
	.p2align 4,,10
	.p2align 3
.L5063:
	movq	-112(%rbp), %r12
	movq	-136(%rbp), %rbx
	movq	-144(%rbp), %r13
	movq	(%r14), %r15
.L4412:
	testq	%r15, %r15
	je	.L4420
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L4420:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4411:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L4421
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L4422
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L4431
	.p2align 4,,10
	.p2align 3
.L5092:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4425
	call	_ZdlPv@PLT
.L4425:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L4426
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4427
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4426:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4428
	call	_ZdlPv@PLT
.L4428:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4429
	call	_ZdlPv@PLT
.L4429:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4430
	call	_ZdlPv@PLT
.L4430:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4423:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L5091
.L4431:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L4423
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L5092
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4423
	.p2align 4,,10
	.p2align 3
.L4551:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L4550
	.p2align 4,,10
	.p2align 3
.L4341:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L4340
	.p2align 4,,10
	.p2align 3
.L4333:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4332
	.p2align 4,,10
	.p2align 3
.L4337:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L4336
	.p2align 4,,10
	.p2align 3
.L4618:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4617
	.p2align 4,,10
	.p2align 3
.L4407:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4406
	.p2align 4,,10
	.p2align 3
.L4509:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4508
	.p2align 4,,10
	.p2align 3
.L4343:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L4342
	.p2align 4,,10
	.p2align 3
.L4445:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L4444
	.p2align 4,,10
	.p2align 3
.L4554:
	movq	%rsi, %rdi
	call	*%rax
	jmp	.L4553
	.p2align 4,,10
	.p2align 3
.L4560:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L4559
	.p2align 4,,10
	.p2align 3
.L4478:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L4477
	.p2align 4,,10
	.p2align 3
.L4410:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L4409
	.p2align 4,,10
	.p2align 3
.L4451:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L4450
	.p2align 4,,10
	.p2align 3
.L4512:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L4511
	.p2align 4,,10
	.p2align 3
.L4349:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L4348
	.p2align 4,,10
	.p2align 3
.L4376:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L4375
	.p2align 4,,10
	.p2align 3
.L4621:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L4620
	.p2align 4,,10
	.p2align 3
.L4587:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L4586
	.p2align 4,,10
	.p2align 3
.L4484:
	call	*%rdx
	jmp	.L4483
	.p2align 4,,10
	.p2align 3
.L4366:
	call	*%rax
	jmp	.L4365
	.p2align 4,,10
	.p2align 3
.L4595:
	call	*%rdx
	jmp	.L4594
	.p2align 4,,10
	.p2align 3
.L4629:
	call	*%rdx
	jmp	.L4628
	.p2align 4,,10
	.p2align 3
.L4486:
	call	*%rdx
	jmp	.L4485
	.p2align 4,,10
	.p2align 3
.L4568:
	call	*%rdx
	jmp	.L4567
	.p2align 4,,10
	.p2align 3
.L4355:
	call	*%rdx
	jmp	.L4354
	.p2align 4,,10
	.p2align 3
.L4357:
	call	*%rdx
	jmp	.L4356
	.p2align 4,,10
	.p2align 3
.L4427:
	call	*%rax
	jmp	.L4426
	.p2align 4,,10
	.p2align 3
.L4457:
	call	*%rdx
	jmp	.L4456
	.p2align 4,,10
	.p2align 3
.L4518:
	call	*%rdx
	jmp	.L4517
	.p2align 4,,10
	.p2align 3
.L4382:
	call	*%rdx
	jmp	.L4381
	.p2align 4,,10
	.p2align 3
.L4566:
	call	*%rdx
	jmp	.L4565
	.p2align 4,,10
	.p2align 3
.L4418:
	call	*%rdx
	jmp	.L4417
	.p2align 4,,10
	.p2align 3
.L4459:
	call	*%rdx
	jmp	.L4458
	.p2align 4,,10
	.p2align 3
.L4495:
	call	*%rax
	jmp	.L4494
	.p2align 4,,10
	.p2align 3
.L4393:
	call	*%rax
	jmp	.L4392
	.p2align 4,,10
	.p2align 3
.L4593:
	call	*%rdx
	jmp	.L4592
	.p2align 4,,10
	.p2align 3
.L4604:
	call	*%rax
	jmp	.L4603
	.p2align 4,,10
	.p2align 3
.L4520:
	call	*%rdx
	jmp	.L4519
	.p2align 4,,10
	.p2align 3
.L4384:
	call	*%rdx
	jmp	.L4383
	.p2align 4,,10
	.p2align 3
.L4627:
	call	*%rdx
	jmp	.L4626
	.p2align 4,,10
	.p2align 3
.L4416:
	call	*%rdx
	jmp	.L4415
	.p2align 4,,10
	.p2align 3
.L4468:
	call	*%rax
	jmp	.L4467
	.p2align 4,,10
	.p2align 3
.L4529:
	call	*%rax
	jmp	.L4528
	.p2align 4,,10
	.p2align 3
.L4638:
	call	*%rax
	jmp	.L4637
	.p2align 4,,10
	.p2align 3
.L4577:
	call	*%rax
	jmp	.L4576
	.cfi_endproc
.LFE4457:
	.size	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev, .-_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.section	.text._ZN12v8_inspector23V8HeapProfilerAgentImpl23getObjectByHeapObjectIdERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISC_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8HeapProfilerAgentImpl23getObjectByHeapObjectIdERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISC_EE
	.type	_ZN12v8_inspector23V8HeapProfilerAgentImpl23getObjectByHeapObjectIdERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISC_EE, @function
_ZN12v8_inspector23V8HeapProfilerAgentImpl23getObjectByHeapObjectIdERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISC_EE:
.LFB7642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	-137(%rbp), %rsi
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8String169toIntegerEPb@PLT
	cmpb	$0, -137(%rbp)
	jne	.L5094
	leaq	-96(%rbp), %r13
	leaq	.LC9(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5093
	call	_ZdlPv@PLT
.L5093:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5128
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5094:
	.cfi_restore_state
	movq	16(%rbx), %rsi
	leaq	-128(%rbp), %r15
	movl	%eax, -152(%rbp)
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%rbx), %rdi
	call	_ZN2v87Isolate15GetHeapProfilerEv@PLT
	movl	-152(%rbp), %r8d
	movq	%rax, %rdi
	movl	%r8d, %esi
	call	_ZN2v812HeapProfiler14FindObjectByIdEj@PLT
	testq	%rax, %rax
	je	.L5097
	movq	%rax, %rdi
	movq	%rax, -152(%rbp)
	call	_ZNK2v85Value8IsObjectEv@PLT
	movq	-152(%rbp), %rcx
	testb	%al, %al
	jne	.L5098
.L5097:
	leaq	-96(%rbp), %r13
	leaq	.LC10(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5099
.L5126:
	call	_ZdlPv@PLT
.L5099:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L5093
	.p2align 4,,10
	.p2align 3
.L5098:
	movq	8(%rbx), %r10
	leaq	_ZN12v8_inspector17V8InspectorClient23isInspectableHeapObjectEN2v85LocalINS1_6ObjectEEE(%rip), %rdx
	movq	24(%r10), %rax
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	88(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5129
.L5114:
	leaq	-96(%rbp), %rbx
	leaq	.LC5(%rip), %rsi
	movq	%r10, -160(%rbp)
	movq	%rbx, %rdi
	movq	%rcx, -152(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-152(%rbp), %rcx
	leaq	8(%r14), %r8
	cmpb	$0, (%r14)
	movq	%r8, %r14
	movq	%rcx, %rdi
	cmove	%rbx, %r14
	call	_ZN2v86Object15CreationContextEv@PLT
	movq	-160(%rbp), %r10
	movq	-152(%rbp), %rcx
	xorl	%r9d, %r9d
	movq	%rax, %rdx
	movq	%r14, %r8
	leaq	-136(%rbp), %rdi
	movq	%r10, %rsi
	call	_ZN12v8_inspector22V8InspectorSessionImpl10wrapObjectEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEERKNS_8String16Eb@PLT
	movq	-136(%rbp), %rax
	movq	0(%r13), %r14
	movq	$0, -136(%rbp)
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L5104
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	24(%rax), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rax
	cmpq	%rax, %rdx
	jne	.L5105
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rax
.L5106:
	movq	-136(%rbp), %r14
	testq	%r14, %r14
	je	.L5104
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L5108
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L5104:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %r14
	cmpq	%r14, %rdi
	je	.L5109
	call	_ZdlPv@PLT
.L5109:
	cmpq	$0, 0(%r13)
	je	.L5130
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L5099
	.p2align 4,,10
	.p2align 3
.L5130:
	leaq	.LC10(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L5126
	jmp	.L5099
	.p2align 4,,10
	.p2align 3
.L5129:
	movq	%rcx, -152(%rbp)
	movq	%rcx, %rsi
	call	*%rax
	movq	-152(%rbp), %rcx
	testb	%al, %al
	je	.L5097
	movq	8(%rbx), %r10
	jmp	.L5114
	.p2align 4,,10
	.p2align 3
.L5108:
	call	*%rdx
	jmp	.L5104
	.p2align 4,,10
	.p2align 3
.L5105:
	call	*%rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rax
	jmp	.L5106
.L5128:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7642:
	.size	_ZN12v8_inspector23V8HeapProfilerAgentImpl23getObjectByHeapObjectIdERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISC_EE, .-_ZN12v8_inspector23V8HeapProfilerAgentImpl23getObjectByHeapObjectIdERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISC_EE
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev:
.LFB14112:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rdx
	leaq	-64(%rdx), %rax
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	304(%rdi), %r12
	movups	%xmm0, -8(%rdi)
	testq	%r12, %r12
	je	.L5132
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rcx
	movq	24(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L5207
	movq	%r12, %rdi
	call	*%rdx
.L5132:
	movq	296(%r14), %r15
	testq	%r15, %r15
	je	.L5136
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %r13
	movq	24(%rdx), %rdx
	cmpq	%r13, %rdx
	je	.L5208
	movq	%r15, %rdi
	call	*%rdx
.L5136:
	movq	256(%r14), %rdi
	leaq	272(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L5163
	call	_ZdlPv@PLT
.L5163:
	movq	208(%r14), %rdi
	leaq	224(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L5164
	call	_ZdlPv@PLT
.L5164:
	movq	160(%r14), %rdi
	leaq	176(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L5165
	call	_ZdlPv@PLT
.L5165:
	movq	144(%r14), %rdi
	testq	%rdi, %rdi
	je	.L5166
	movq	(%rdi), %rdx
	call	*24(%rdx)
.L5166:
	movq	104(%r14), %rdi
	leaq	120(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L5167
	call	_ZdlPv@PLT
.L5167:
	movq	56(%r14), %rdi
	leaq	72(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L5168
	call	_ZdlPv@PLT
.L5168:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L5131
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L5131:
	.cfi_restore_state
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5208:
	.cfi_restore_state
	movq	160(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L5138
	movq	8(%r12), %rax
	movq	%rax, -64(%rbp)
	movq	(%r12), %rax
	movq	%rax, -56(%rbp)
.L5146:
	movq	-56(%rbp), %rax
	cmpq	%rax, -64(%rbp)
	je	.L5139
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L5140
	movq	(%rbx), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L5141
	movq	16(%rbx), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L5142
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%r13, %rdx
	jne	.L5143
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L5142:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5144
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%r13, %rdx
	jne	.L5145
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L5144:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L5140:
	addq	$8, -56(%rbp)
	jmp	.L5146
.L5141:
	movq	%rbx, %rdi
	call	*%rdx
	jmp	.L5140
.L5145:
	call	*%rdx
	jmp	.L5144
.L5139:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5147
	call	_ZdlPv@PLT
.L5147:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L5138:
	movq	152(%r15), %r12
	testq	%r12, %r12
	je	.L5148
	movq	8(%r12), %rax
	movq	%rax, -64(%rbp)
	movq	(%r12), %rax
	movq	%rax, -56(%rbp)
.L5158:
	movq	-56(%rbp), %rax
	cmpq	%rax, -64(%rbp)
	je	.L5149
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L5150
	movq	(%rbx), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L5151
	movq	152(%rbx), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	leaq	168(%rbx), %rdx
	movq	%rax, (%rbx)
	cmpq	%rdx, %rdi
	je	.L5152
	call	_ZdlPv@PLT
.L5152:
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5153
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%r13, %rdx
	jne	.L5154
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L5153:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rdx
	cmpq	%rdx, %rdi
	je	.L5155
	call	_ZdlPv@PLT
.L5155:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rdx
	cmpq	%rdx, %rdi
	je	.L5156
	call	_ZdlPv@PLT
.L5156:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rdx
	cmpq	%rdx, %rdi
	je	.L5157
	call	_ZdlPv@PLT
.L5157:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L5150:
	addq	$8, -56(%rbp)
	jmp	.L5158
.L5151:
	movq	%rbx, %rdi
	call	*%rdx
	jmp	.L5150
.L5154:
	call	*%rdx
	jmp	.L5153
.L5149:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5159
	call	_ZdlPv@PLT
.L5159:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L5148:
	movq	104(%r15), %rdi
	leaq	120(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L5160
	call	_ZdlPv@PLT
.L5160:
	movq	56(%r15), %rdi
	leaq	72(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L5161
	call	_ZdlPv@PLT
.L5161:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L5162
	call	_ZdlPv@PLT
.L5162:
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	jmp	.L5136
.L5207:
	movq	56(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	leaq	72(%r12), %rdx
	movq	%rax, (%r12)
	cmpq	%rdx, %rdi
	je	.L5134
	call	_ZdlPv@PLT
.L5134:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rdx
	cmpq	%rdx, %rdi
	je	.L5135
	call	_ZdlPv@PLT
.L5135:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L5132
.L5143:
	call	*%rdx
	jmp	.L5142
	.cfi_endproc
.LFE14112:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev:
.LFB14111:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rdx
	leaq	-64(%rdx), %rax
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-8(%rdi), %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	304(%rdi), %r13
	movups	%xmm0, -8(%rdi)
	testq	%r13, %r13
	je	.L5210
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rcx
	movq	24(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L5285
	movq	%r13, %rdi
	call	*%rdx
.L5210:
	movq	296(%r15), %r14
	testq	%r14, %r14
	je	.L5214
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	je	.L5286
	movq	%r14, %rdi
	call	*%rdx
.L5214:
	movq	256(%r15), %rdi
	leaq	272(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L5241
	call	_ZdlPv@PLT
.L5241:
	movq	208(%r15), %rdi
	leaq	224(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L5242
	call	_ZdlPv@PLT
.L5242:
	movq	160(%r15), %rdi
	leaq	176(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L5243
	call	_ZdlPv@PLT
.L5243:
	movq	144(%r15), %rdi
	testq	%rdi, %rdi
	je	.L5244
	movq	(%rdi), %rdx
	call	*24(%rdx)
.L5244:
	movq	104(%r15), %rdi
	leaq	120(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L5245
	call	_ZdlPv@PLT
.L5245:
	movq	56(%r15), %rdi
	leaq	72(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L5246
	call	_ZdlPv@PLT
.L5246:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L5247
	call	_ZdlPv@PLT
.L5247:
	addq	$40, %rsp
	movq	%r12, %rdi
	movl	$320, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
.L5285:
	.cfi_restore_state
	movq	56(%r13), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	leaq	72(%r13), %rdx
	movq	%rax, 0(%r13)
	cmpq	%rdx, %rdi
	je	.L5212
	call	_ZdlPv@PLT
.L5212:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L5213
	call	_ZdlPv@PLT
.L5213:
	movl	$96, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L5210
.L5286:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r14)
	movq	160(%r14), %rax
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L5216
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -72(%rbp)
	movq	%rax, -64(%rbp)
.L5224:
	movq	-64(%rbp), %rax
	cmpq	%rax, -72(%rbp)
	je	.L5217
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L5218
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L5219
	movq	16(%r13), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L5220
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L5221
	movq	%rdi, -80(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L5220:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L5222
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L5223
	movq	%rdi, -80(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L5222:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L5218:
	addq	$8, -64(%rbp)
	jmp	.L5224
.L5217:
	movq	-56(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L5225
	call	_ZdlPv@PLT
.L5225:
	movq	-56(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L5216:
	movq	152(%r14), %rax
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L5226
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -72(%rbp)
	movq	%rax, -64(%rbp)
.L5236:
	movq	-64(%rbp), %rax
	cmpq	%rax, -72(%rbp)
	je	.L5227
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L5228
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L5229
	movq	152(%r13), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	leaq	168(%r13), %rdx
	movq	%rax, 0(%r13)
	cmpq	%rdx, %rdi
	je	.L5230
	call	_ZdlPv@PLT
.L5230:
	movq	136(%r13), %rdi
	testq	%rdi, %rdi
	je	.L5231
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L5232
	movq	%rdi, -80(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L5231:
	movq	96(%r13), %rdi
	leaq	112(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L5233
	call	_ZdlPv@PLT
.L5233:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L5234
	call	_ZdlPv@PLT
.L5234:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L5235
	call	_ZdlPv@PLT
.L5235:
	movl	$192, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L5228:
	addq	$8, -64(%rbp)
	jmp	.L5236
.L5227:
	movq	-56(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L5237
	call	_ZdlPv@PLT
.L5237:
	movq	-56(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L5226:
	movq	104(%r14), %rdi
	leaq	120(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L5238
	call	_ZdlPv@PLT
.L5238:
	movq	56(%r14), %rdi
	leaq	72(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L5239
	call	_ZdlPv@PLT
.L5239:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L5240
	call	_ZdlPv@PLT
.L5240:
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	jmp	.L5214
.L5221:
	call	*%rdx
	jmp	.L5220
.L5229:
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L5228
.L5232:
	call	*%rdx
	jmp	.L5231
.L5219:
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L5218
.L5223:
	call	*%rdx
	jmp	.L5222
	.cfi_endproc
.LFE14111:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.section	.text._ZN12v8_inspector23V8HeapProfilerAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8HeapProfilerAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE
	.type	_ZN12v8_inspector23V8HeapProfilerAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE, @function
_ZN12v8_inspector23V8HeapProfilerAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE:
.LFB7601:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12v8_inspector23V8HeapProfilerAgentImplE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movq	24(%rsi), %rax
	movq	%rcx, 32(%rdi)
	movq	8(%rax), %rax
	movb	$0, 40(%rdi)
	movq	%rdx, 24(%rdi)
	movq	%rax, 16(%rdi)
	ret
	.cfi_endproc
.LFE7601:
	.size	_ZN12v8_inspector23V8HeapProfilerAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE, .-_ZN12v8_inspector23V8HeapProfilerAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE
	.globl	_ZN12v8_inspector23V8HeapProfilerAgentImplC1EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE
	.set	_ZN12v8_inspector23V8HeapProfilerAgentImplC1EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE,_ZN12v8_inspector23V8HeapProfilerAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE
	.section	.text._ZN12v8_inspector23V8HeapProfilerAgentImpl7restoreEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8HeapProfilerAgentImpl7restoreEv
	.type	_ZN12v8_inspector23V8HeapProfilerAgentImpl7restoreEv, @function
_ZN12v8_inspector23V8HeapProfilerAgentImpl7restoreEv:
.LFB7607:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL19heapProfilerEnabledE(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-112(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-96(%rbp), %rbx
	subq	$104, %rsp
	movq	32(%rdi), %r14
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue15booleanPropertyERKNS_8String16Eb@PLT
	movq	-112(%rbp), %rdi
	movl	%eax, %r14d
	cmpq	%rbx, %rdi
	je	.L5289
	call	_ZdlPv@PLT
.L5289:
	testb	%r14b, %r14b
	jne	.L5312
.L5290:
	movq	32(%r12), %r14
	leaq	_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL26heapObjectsTrackingEnabledE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue15booleanPropertyERKNS_8String16Eb@PLT
	movq	-112(%rbp), %rdi
	movl	%eax, %r14d
	cmpq	%rbx, %rdi
	je	.L5291
	call	_ZdlPv@PLT
.L5291:
	movq	32(%r12), %r15
	testb	%r14b, %r14b
	jne	.L5313
.L5292:
	leaq	_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL27samplingHeapProfilerEnabledE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue15booleanPropertyERKNS_8String16Eb@PLT
	movq	-112(%rbp), %rdi
	movl	%eax, %r14d
	cmpq	%rbx, %rdi
	je	.L5297
	call	_ZdlPv@PLT
.L5297:
	testb	%r14b, %r14b
	jne	.L5314
.L5288:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5315
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5312:
	.cfi_restore_state
	leaq	24(%r12), %rdi
	call	_ZN12v8_inspector8protocol12HeapProfiler8Frontend13resetProfilesEv@PLT
	jmp	.L5290
	.p2align 4,,10
	.p2align 3
.L5314:
	movq	32(%r12), %r14
	leaq	_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL28samplingHeapProfilerIntervalE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movsd	.LC11(%rip), %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue14doublePropertyERKNS_8String16Ed@PLT
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L5299
	movsd	%xmm0, -136(%rbp)
	call	_ZdlPv@PLT
	movsd	-136(%rbp), %xmm0
.L5299:
	movq	(%r12), %rax
	movq	%r13, %rdi
	movb	$1, -128(%rbp)
	leaq	-128(%rbp), %rdx
	movsd	%xmm0, -120(%rbp)
	movq	%r12, %rsi
	call	*72(%rax)
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5288
	call	_ZdlPv@PLT
	jmp	.L5288
	.p2align 4,,10
	.p2align 3
.L5313:
	leaq	_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL25allocationTrackingEnabledE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue15booleanPropertyERKNS_8String16Eb@PLT
	movq	16(%r12), %rdi
	movl	%eax, %r14d
	call	_ZN2v87Isolate15GetHeapProfilerEv@PLT
	movzbl	%r14b, %esi
	movq	%rax, %rdi
	call	_ZN2v812HeapProfiler24StartTrackingHeapObjectsEb@PLT
	cmpb	$0, 40(%r12)
	jne	.L5294
	movq	8(%r12), %rax
	movb	$1, 40(%r12)
	leaq	_ZN12v8_inspector17V8InspectorClient19startRepeatingTimerEdPFvPvES1_(%rip), %rdx
	movq	24(%rax), %rax
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	184(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5316
.L5294:
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L5296
	call	_ZdlPv@PLT
.L5296:
	movq	32(%r12), %r15
	jmp	.L5292
	.p2align 4,,10
	.p2align 3
.L5316:
	movsd	.LC8(%rip), %xmm0
	movq	%r12, %rdx
	leaq	_ZN12v8_inspector23V8HeapProfilerAgentImpl7onTimerEPv(%rip), %rsi
	call	*%rax
	jmp	.L5294
.L5315:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7607:
	.size	_ZN12v8_inspector23V8HeapProfilerAgentImpl7restoreEv, .-_ZN12v8_inspector23V8HeapProfilerAgentImpl7restoreEv
	.section	.text._ZN12v8_inspector23V8HeapProfilerAgentImpl22requestHeapStatsUpdateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8HeapProfilerAgentImpl22requestHeapStatsUpdateEv
	.type	_ZN12v8_inspector23V8HeapProfilerAgentImpl22requestHeapStatsUpdateEv, @function
_ZN12v8_inspector23V8HeapProfilerAgentImpl22requestHeapStatsUpdateEv:
.LFB7653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	24(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_115HeapStatsStreamE(%rip), %rax
	movq	%r12, -40(%rbp)
	movq	%rax, -48(%rbp)
	call	_ZN2v87Isolate15GetHeapProfilerEv@PLT
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, %rdi
	call	_ZN2v812HeapProfiler12GetHeapStatsEPNS_12OutputStreamEPl@PLT
	leaq	_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv(%rip), %rdx
	pxor	%xmm0, %xmm0
	movl	%eax, %esi
	movq	8(%rbx), %rax
	movq	24(%rax), %rax
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	176(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5323
.L5318:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12HeapProfiler8Frontend16lastSeenObjectIdEid@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5324
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5323:
	.cfi_restore_state
	movl	%esi, -52(%rbp)
	call	*%rax
	movl	-52(%rbp), %esi
	jmp	.L5318
.L5324:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7653:
	.size	_ZN12v8_inspector23V8HeapProfilerAgentImpl22requestHeapStatsUpdateEv, .-_ZN12v8_inspector23V8HeapProfilerAgentImpl22requestHeapStatsUpdateEv
	.section	.text._ZN12v8_inspector23V8HeapProfilerAgentImpl32startTrackingHeapObjectsInternalEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8HeapProfilerAgentImpl32startTrackingHeapObjectsInternalEb
	.type	_ZN12v8_inspector23V8HeapProfilerAgentImpl32startTrackingHeapObjectsInternalEb, @function
_ZN12v8_inspector23V8HeapProfilerAgentImpl32startTrackingHeapObjectsInternalEb:
.LFB7655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	16(%rdi), %rdi
	movl	%esi, %ebx
	call	_ZN2v87Isolate15GetHeapProfilerEv@PLT
	movzbl	%bl, %esi
	movq	%rax, %rdi
	call	_ZN2v812HeapProfiler24StartTrackingHeapObjectsEb@PLT
	cmpb	$0, 40(%r12)
	jne	.L5325
	movq	8(%r12), %rax
	movb	$1, 40(%r12)
	leaq	_ZN12v8_inspector17V8InspectorClient19startRepeatingTimerEdPFvPvES1_(%rip), %rdx
	movq	24(%rax), %rax
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	184(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5330
.L5325:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5330:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdx
	popq	%r12
	movsd	.LC8(%rip), %xmm0
	leaq	_ZN12v8_inspector23V8HeapProfilerAgentImpl7onTimerEPv(%rip), %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE7655:
	.size	_ZN12v8_inspector23V8HeapProfilerAgentImpl32startTrackingHeapObjectsInternalEb, .-_ZN12v8_inspector23V8HeapProfilerAgentImpl32startTrackingHeapObjectsInternalEb
	.section	.text._ZN12v8_inspector23V8HeapProfilerAgentImpl31stopTrackingHeapObjectsInternalEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8HeapProfilerAgentImpl31stopTrackingHeapObjectsInternalEv
	.type	_ZN12v8_inspector23V8HeapProfilerAgentImpl31stopTrackingHeapObjectsInternalEv, @function
_ZN12v8_inspector23V8HeapProfilerAgentImpl31stopTrackingHeapObjectsInternalEv:
.LFB7656:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 40(%rdi)
	je	.L5332
	movq	8(%rdi), %rax
	leaq	_ZN12v8_inspector17V8InspectorClient11cancelTimerEPv(%rip), %rdx
	movq	24(%rax), %rax
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	192(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5338
.L5333:
	movb	$0, 40(%rbx)
.L5332:
	movq	16(%rbx), %rdi
	leaq	-80(%rbp), %r12
	call	_ZN2v87Isolate15GetHeapProfilerEv@PLT
	movq	%rax, %rdi
	call	_ZN2v812HeapProfiler23StopTrackingHeapObjectsEv@PLT
	movq	32(%rbx), %r13
	leaq	_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL26heapObjectsTrackingEnabledE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	-64(%rbp), %r13
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-80(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L5334
	call	_ZdlPv@PLT
.L5334:
	movq	32(%rbx), %r14
	leaq	_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL25allocationTrackingEnabledE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-80(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L5331
	call	_ZdlPv@PLT
.L5331:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5339
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5338:
	.cfi_restore_state
	movq	%rbx, %rsi
	call	*%rax
	jmp	.L5333
.L5339:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7656:
	.size	_ZN12v8_inspector23V8HeapProfilerAgentImpl31stopTrackingHeapObjectsInternalEv, .-_ZN12v8_inspector23V8HeapProfilerAgentImpl31stopTrackingHeapObjectsInternalEv
	.section	.text._ZN12v8_inspector23V8HeapProfilerAgentImpl23stopTrackingHeapObjectsEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8HeapProfilerAgentImpl23stopTrackingHeapObjectsEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE
	.type	_ZN12v8_inspector23V8HeapProfilerAgentImpl23stopTrackingHeapObjectsEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE, @function
_ZN12v8_inspector23V8HeapProfilerAgentImpl23stopTrackingHeapObjectsEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE:
.LFB7610:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	24(%rsi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$104, %rsp
	movq	16(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_115HeapStatsStreamE(%rip), %rax
	movq	%r15, -120(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN2v87Isolate15GetHeapProfilerEv@PLT
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v812HeapProfiler12GetHeapStatsEPNS_12OutputStreamEPl@PLT
	leaq	_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv(%rip), %rdx
	pxor	%xmm0, %xmm0
	movl	%eax, %esi
	movq	8(%r12), %rax
	movq	24(%rax), %rax
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	176(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5347
.L5341:
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12HeapProfiler8Frontend16lastSeenObjectIdEid@PLT
	movzwl	(%rbx), %edx
	movq	(%r12), %rax
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	movq	104(%rax), %rax
	movw	%dx, -128(%rbp)
	movq	%r14, %rdx
	call	*%rax
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5342
	call	_ZdlPv@PLT
.L5342:
	movq	%r12, %rdi
	call	_ZN12v8_inspector23V8HeapProfilerAgentImpl31stopTrackingHeapObjectsInternalEv
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5348
	addq	$104, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5347:
	.cfi_restore_state
	movl	%esi, -132(%rbp)
	call	*%rax
	movl	-132(%rbp), %esi
	jmp	.L5341
.L5348:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7610:
	.size	_ZN12v8_inspector23V8HeapProfilerAgentImpl23stopTrackingHeapObjectsEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE, .-_ZN12v8_inspector23V8HeapProfilerAgentImpl23stopTrackingHeapObjectsEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE
	.section	.text._ZN12v8_inspector23V8HeapProfilerAgentImpl7disableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8HeapProfilerAgentImpl7disableEv
	.type	_ZN12v8_inspector23V8HeapProfilerAgentImpl7disableEv, @function
_ZN12v8_inspector23V8HeapProfilerAgentImpl7disableEv:
.LFB7613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector23V8HeapProfilerAgentImpl31stopTrackingHeapObjectsInternalEv
	movq	32(%rbx), %r14
	leaq	_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL27samplingHeapProfilerEnabledE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-80(%rbp), %r14
	call	_ZNK12v8_inspector8protocol15DictionaryValue15booleanPropertyERKNS_8String16Eb@PLT
	movq	-96(%rbp), %rdi
	movl	%eax, %r15d
	cmpq	%r14, %rdi
	je	.L5350
	call	_ZdlPv@PLT
.L5350:
	movq	16(%rbx), %rdi
	testb	%r15b, %r15b
	jne	.L5360
.L5351:
	call	_ZN2v87Isolate15GetHeapProfilerEv@PLT
	movq	%rax, %rdi
	call	_ZN2v812HeapProfiler14ClearObjectIdsEv@PLT
	movq	32(%rbx), %r15
	leaq	_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL19heapProfilerEnabledE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L5353
	call	_ZdlPv@PLT
.L5353:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5361
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5360:
	.cfi_restore_state
	call	_ZN2v87Isolate15GetHeapProfilerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L5359
	call	_ZN2v812HeapProfiler24StopSamplingHeapProfilerEv@PLT
.L5359:
	movq	16(%rbx), %rdi
	jmp	.L5351
.L5361:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7613:
	.size	_ZN12v8_inspector23V8HeapProfilerAgentImpl7disableEv, .-_ZN12v8_inspector23V8HeapProfilerAgentImpl7disableEv
	.section	.rodata._ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRjEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_.str1.1,"aMS",@progbits,1
.LC12:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRjEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRjEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRjEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.type	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRjEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, @function
_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRjEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_:
.LFB10489:
	.cfi_startproc
	endbr64
	movabsq	$2305843009213693951, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$2, %rax
	cmpq	%rcx, %rax
	je	.L5376
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L5372
	movabsq	$9223372036854775804, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L5377
.L5364:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L5371:
	movl	(%r15), %eax
	subq	%r8, %r13
	leaq	4(%rbx,%rdx), %r15
	movl	%eax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L5378
	testq	%r13, %r13
	jg	.L5367
	testq	%r9, %r9
	jne	.L5370
.L5368:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5378:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L5367
.L5370:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L5368
	.p2align 4,,10
	.p2align 3
.L5377:
	testq	%rsi, %rsi
	jne	.L5365
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L5371
	.p2align 4,,10
	.p2align 3
.L5367:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L5368
	jmp	.L5370
	.p2align 4,,10
	.p2align 3
.L5372:
	movl	$4, %r14d
	jmp	.L5364
.L5376:
	leaq	.LC12(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L5365:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$2, %r14
	jmp	.L5364
	.cfi_endproc
.LFE10489:
	.size	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRjEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, .-_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRjEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.section	.text._ZN12v8_inspector12_GLOBAL__N_115HeapStatsStream19WriteHeapStatsChunkEPN2v815HeapStatsUpdateEi,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_115HeapStatsStream19WriteHeapStatsChunkEPN2v815HeapStatsUpdateEi, @function
_ZN12v8_inspector12_GLOBAL__N_115HeapStatsStream19WriteHeapStatsChunkEPN2v815HeapStatsUpdateEi:
.LFB7596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	movl	$24, %edi
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	%rax, %rbx
	movups	%xmm0, (%rax)
	testl	%r13d, %r13d
	jle	.L5380
	leal	-1(%r13), %eax
	xorl	%esi, %esi
	leaq	(%rax,%rax,2), %rax
	leaq	(%r12,%rax,4), %r13
	xorl	%eax, %eax
	jmp	.L5387
	.p2align 4,,10
	.p2align 3
.L5398:
	movl	(%r12), %eax
	movl	%eax, (%rsi)
	movq	8(%rbx), %rax
	leaq	4(%rax), %rsi
	movq	%rsi, 8(%rbx)
	cmpq	%rsi, 16(%rbx)
	je	.L5383
.L5399:
	movl	4(%r12), %eax
	movl	%eax, (%rsi)
	movq	8(%rbx), %rax
	leaq	4(%rax), %rsi
	movq	%rsi, 8(%rbx)
	cmpq	%rsi, 16(%rbx)
	je	.L5385
.L5400:
	movl	8(%r12), %eax
	movl	%eax, (%rsi)
	addq	$4, 8(%rbx)
	cmpq	%r13, %r12
	je	.L5380
.L5401:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rax
	addq	$12, %r12
.L5387:
	cmpq	%rax, %rsi
	jne	.L5398
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRjEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movq	8(%rbx), %rsi
	cmpq	%rsi, 16(%rbx)
	jne	.L5399
	.p2align 4,,10
	.p2align 3
.L5383:
	leaq	4(%r12), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRjEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movq	8(%rbx), %rsi
	cmpq	%rsi, 16(%rbx)
	jne	.L5400
	.p2align 4,,10
	.p2align 3
.L5385:
	leaq	8(%r12), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRjEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	cmpq	%r13, %r12
	jne	.L5401
	.p2align 4,,10
	.p2align 3
.L5380:
	movq	8(%r14), %rdi
	leaq	-48(%rbp), %rsi
	movq	%rbx, -48(%rbp)
	call	_ZN12v8_inspector8protocol12HeapProfiler8Frontend15heapStatsUpdateESt10unique_ptrISt6vectorIiSaIiEESt14default_deleteIS6_EE@PLT
	movq	-48(%rbp), %r12
	testq	%r12, %r12
	je	.L5388
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5389
	call	_ZdlPv@PLT
.L5389:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L5388:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5402
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5402:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7596:
	.size	_ZN12v8_inspector12_GLOBAL__N_115HeapStatsStream19WriteHeapStatsChunkEPN2v815HeapStatsUpdateEi, .-_ZN12v8_inspector12_GLOBAL__N_115HeapStatsStream19WriteHeapStatsChunkEPN2v815HeapStatsUpdateEi
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB10555:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movabsq	$1152921504606846975, %rdx
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rcx
	movq	8(%rdi), %rax
	movq	%rdi, -120(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rax, -112(%rbp)
	subq	%rcx, %rax
	sarq	$3, %rax
	movq	%rcx, -72(%rbp)
	cmpq	%rdx, %rax
	je	.L5454
	movq	%rsi, %r14
	movq	%rsi, %r13
	subq	-72(%rbp), %r14
	testq	%rax, %rax
	je	.L5428
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L5455
.L5405:
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	%rax, -96(%rbp)
	addq	%rax, %rbx
	movq	%rbx, -104(%rbp)
	leaq	8(%rax), %rbx
.L5427:
	movq	(%r12), %rax
	movq	$0, (%r12)
	movq	-96(%rbp), %r12
	movq	%rax, (%r12,%r14)
	movq	-72(%rbp), %rax
	cmpq	%rax, %r13
	je	.L5407
	movq	%r13, -56(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L5421:
	movq	(%rbx), %rax
	movq	$0, (%rbx)
	movq	%rax, (%r12)
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L5408
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L5409
	movq	32(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L5410
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L5411
	movq	%rbx, -80(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -88(%rbp)
	jmp	.L5414
	.p2align 4,,10
	.p2align 3
.L5457:
	call	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L5412:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L5456
.L5414:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L5412
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L5457
	call	*%rdx
	addq	$8, %rbx
	cmpq	%rbx, %r15
	jne	.L5414
	.p2align 4,,10
	.p2align 3
.L5456:
	movq	-80(%rbp), %rbx
	movq	-88(%rbp), %r12
	movq	(%r14), %r15
.L5411:
	testq	%r15, %r15
	je	.L5415
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L5415:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L5410:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L5416
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L5417
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r14), %rdi
	movq	%rax, (%r14)
	leaq	104(%r14), %rax
	cmpq	%rax, %rdi
	je	.L5418
	call	_ZdlPv@PLT
.L5418:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L5419
	call	_ZdlPv@PLT
.L5419:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L5420
	call	_ZdlPv@PLT
.L5420:
	movl	$136, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L5416:
	movl	$40, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L5408:
	addq	$8, %rbx
	addq	$8, %r12
	cmpq	%rbx, -56(%rbp)
	jne	.L5421
.L5458:
	movq	-56(%rbp), %r13
	movq	-96(%rbp), %rsi
	movq	%r13, %rax
	subq	-72(%rbp), %rax
	leaq	8(%rsi,%rax), %rbx
.L5407:
	movq	-112(%rbp), %rax
	cmpq	%rax, %r13
	je	.L5422
	subq	%r13, %rax
	leaq	-8(%rax), %rsi
	movq	%rsi, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rsi, %rsi
	je	.L5430
	movq	%rcx, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L5424:
	movdqu	0(%r13,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L5424
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	leaq	(%rbx,%rdi), %rdx
	addq	%r13, %rdi
	movq	%rdi, -64(%rbp)
	cmpq	%rax, %rcx
	je	.L5425
.L5423:
	movq	-64(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L5425:
	leaq	8(%rbx,%rsi), %rbx
.L5422:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L5426
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L5426:
	movq	-96(%rbp), %xmm0
	movq	-120(%rbp), %rax
	movq	%rbx, %xmm2
	movq	-104(%rbp), %rsi
	punpcklqdq	%xmm2, %xmm0
	movq	%rsi, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5409:
	.cfi_restore_state
	movq	%r13, %rdi
	addq	$8, %rbx
	addq	$8, %r12
	call	*%rax
	cmpq	%rbx, -56(%rbp)
	jne	.L5421
	jmp	.L5458
	.p2align 4,,10
	.p2align 3
.L5417:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L5416
	.p2align 4,,10
	.p2align 3
.L5455:
	testq	%rcx, %rcx
	jne	.L5406
	movq	$0, -104(%rbp)
	movl	$8, %ebx
	movq	$0, -96(%rbp)
	jmp	.L5427
	.p2align 4,,10
	.p2align 3
.L5428:
	movl	$8, %ebx
	jmp	.L5405
.L5430:
	movq	%rbx, %rdx
	jmp	.L5423
.L5406:
	cmpq	%rdx, %rcx
	cmovbe	%rcx, %rdx
	leaq	0(,%rdx,8), %rbx
	jmp	.L5405
.L5454:
	leaq	.LC12(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10555:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB10612:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -80(%rbp)
	movabsq	$1152921504606846975, %rdi
	movq	%rax, -72(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rdi, %rax
	je	.L5486
	movq	%rsi, %r13
	movq	%rsi, %rbx
	subq	%r12, %rsi
	testq	%rax, %rax
	je	.L5473
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %r9
	cmpq	%r9, %rax
	jbe	.L5487
.L5461:
	movq	%r14, %rdi
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdx
	movq	%rax, -56(%rbp)
	addq	%rax, %r14
	movq	%r14, -64(%rbp)
	leaq	8(%rax), %r14
.L5472:
	movq	(%rdx), %rax
	movq	-56(%rbp), %rcx
	movq	$0, (%rdx)
	movq	%rax, (%rcx,%rsi)
	cmpq	%r12, %r13
	je	.L5463
	movq	%rcx, %r14
	movq	%r12, %r15
	jmp	.L5466
	.p2align 4,,10
	.p2align 3
.L5489:
	movl	$32, %esi
	call	_ZdlPvm@PLT
.L5464:
	addq	$8, %r15
	addq	$8, %r14
	cmpq	%r15, %r13
	je	.L5488
.L5466:
	movq	(%r15), %rsi
	movq	$0, (%r15)
	movq	%rsi, (%r14)
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L5464
	movq	(%rdi), %rsi
	leaq	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev(%rip), %rax
	movq	24(%rsi), %rsi
	cmpq	%rax, %rsi
	je	.L5489
	call	*%rsi
	addq	$8, %r15
	addq	$8, %r14
	cmpq	%r15, %r13
	jne	.L5466
	.p2align 4,,10
	.p2align 3
.L5488:
	movq	-56(%rbp), %rcx
	movq	%r13, %rax
	subq	%r12, %rax
	leaq	8(%rcx,%rax), %r14
.L5463:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r13
	je	.L5467
	movq	%rax, %r15
	subq	%r13, %r15
	leaq	-8(%r15), %rdi
	movq	%rdi, %rsi
	shrq	$3, %rsi
	addq	$1, %rsi
	testq	%rdi, %rdi
	je	.L5475
	movq	%rsi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L5469:
	movdqu	0(%r13,%rax), %xmm1
	movups	%xmm1, (%r14,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L5469
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%r14,%r8), %rdx
	leaq	0(%r13,%r8), %rbx
	cmpq	%rax, %rsi
	je	.L5470
.L5468:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L5470:
	leaq	8(%r14,%rdi), %r14
.L5467:
	testq	%r12, %r12
	je	.L5471
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L5471:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%r14, %xmm2
	movq	-64(%rbp), %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5487:
	.cfi_restore_state
	testq	%r9, %r9
	jne	.L5462
	movq	$0, -64(%rbp)
	movl	$8, %r14d
	movq	$0, -56(%rbp)
	jmp	.L5472
	.p2align 4,,10
	.p2align 3
.L5473:
	movl	$8, %r14d
	jmp	.L5461
.L5475:
	movq	%r14, %rdx
	jmp	.L5468
.L5462:
	cmpq	%rdi, %r9
	cmovbe	%r9, %rdi
	leaq	0(,%rdi,8), %r14
	jmp	.L5461
.L5486:
	leaq	.LC12(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10612:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_:
.LFB11075:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L5509
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r13
	movq	8(%rsi), %r15
	cmpq	%r13, %rdx
	je	.L5500
	movq	16(%rdi), %rax
.L5492:
	cmpq	%r15, %rax
	jb	.L5512
	leaq	(%r15,%r15), %rdx
	testq	%r15, %r15
	je	.L5496
.L5515:
	movq	(%r12), %rsi
	cmpq	$1, %r15
	je	.L5513
	testq	%rdx, %rdx
	je	.L5496
	movq	%r13, %rdi
	call	memmove@PLT
	movq	(%rbx), %r13
.L5496:
	xorl	%eax, %eax
	movq	%r15, 8(%rbx)
	movw	%ax, 0(%r13,%r15,2)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5512:
	.cfi_restore_state
	movabsq	$2305843009213693951, %rcx
	cmpq	%rcx, %r15
	ja	.L5514
	addq	%rax, %rax
	movq	%r15, %r14
	cmpq	%rax, %r15
	jnb	.L5495
	cmpq	%rcx, %rax
	cmovbe	%rax, %rcx
	movq	%rcx, %r14
.L5495:
	leaq	2(%r14,%r14), %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	(%rbx), %rdi
	movq	-56(%rbp), %rdx
	movq	%rax, %r13
	cmpq	%rdi, %rdx
	je	.L5499
	call	_ZdlPv@PLT
.L5499:
	movq	%r13, (%rbx)
	leaq	(%r15,%r15), %rdx
	movq	%r14, 16(%rbx)
	testq	%r15, %r15
	je	.L5496
	jmp	.L5515
	.p2align 4,,10
	.p2align 3
.L5509:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L5500:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$7, %eax
	jmp	.L5492
	.p2align 4,,10
	.p2align 3
.L5513:
	movzwl	(%rsi), %eax
	movw	%ax, 0(%r13)
	movq	(%rbx), %r13
	jmp	.L5496
.L5514:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11075:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.section	.text._ZN12v8_inspector12_GLOBAL__N_127buildSampingHeapProfileNodeEPN2v87IsolateEPKNS1_17AllocationProfile4NodeE,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_127buildSampingHeapProfileNodeEPN2v87IsolateEPKNS1_17AllocationProfile4NodeE, @function
_ZN12v8_inspector12_GLOBAL__N_127buildSampingHeapProfileNodeEPN2v87IsolateEPKNS1_17AllocationProfile4NodeE:
.LFB7658:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -248(%rbp)
	movl	$24, %edi
	movq	%rsi, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	40(%r13), %r12
	leaq	-200(%rbp), %rcx
	movq	$0, 16(%rax)
	movq	%rax, %rbx
	movups	%xmm0, (%rax)
	movq	48(%r13), %rax
	movq	%rcx, -224(%rbp)
	movq	%rax, -232(%rbp)
	cmpq	%rax, %r12
	je	.L5536
	movq	%r13, -264(%rbp)
.L5535:
	movq	-216(%rbp), %rsi
	movq	(%r12), %rdx
	movq	-224(%rbp), %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_127buildSampingHeapProfileNodeEPN2v87IsolateEPKNS1_17AllocationProfile4NodeE
	movq	8(%rbx), %rsi
	cmpq	16(%rbx), %rsi
	je	.L5520
	movq	-200(%rbp), %rax
	movq	$0, -200(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%rbx)
.L5521:
	movq	-200(%rbp), %r15
	testq	%r15, %r15
	je	.L5522
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L5523
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE(%rip), %rax
	movq	%rax, (%r15)
	movq	32(%r15), %rax
	movq	%rax, -240(%rbp)
	testq	%rax, %rax
	je	.L5524
	movq	8(%rax), %r13
	movq	(%rax), %r14
	cmpq	%r14, %r13
	je	.L5525
	movq	%r12, -256(%rbp)
	jmp	.L5528
	.p2align 4,,10
	.p2align 3
.L5568:
	call	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L5526:
	addq	$8, %r14
	cmpq	%r14, %r13
	je	.L5567
.L5528:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L5526
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rcx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L5568
	call	*%rax
	addq	$8, %r14
	cmpq	%r14, %r13
	jne	.L5528
	.p2align 4,,10
	.p2align 3
.L5567:
	movq	-240(%rbp), %rax
	movq	-256(%rbp), %r12
	movq	(%rax), %r14
.L5525:
	testq	%r14, %r14
	je	.L5529
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L5529:
	movq	-240(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L5524:
	movq	8(%r15), %r13
	testq	%r13, %r13
	je	.L5530
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L5531
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	104(%r13), %rax
	cmpq	%rax, %rdi
	je	.L5532
	call	_ZdlPv@PLT
.L5532:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L5533
	call	_ZdlPv@PLT
.L5533:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L5534
	call	_ZdlPv@PLT
.L5534:
	movl	$136, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L5530:
	movl	$40, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L5522:
	addq	$8, %r12
	cmpq	%r12, -232(%rbp)
	jne	.L5535
.L5570:
	movq	-264(%rbp), %r13
.L5536:
	movq	72(%r13), %rcx
	movq	64(%r13), %rax
	xorl	%r14d, %r14d
	cmpq	%rcx, %rax
	je	.L5519
	.p2align 4,,10
	.p2align 3
.L5537:
	movl	8(%rax), %edx
	imulq	(%rax), %rdx
	addq	$16, %rax
	addq	%rdx, %r14
	cmpq	%rax, %rcx
	jne	.L5537
.L5519:
	movl	$136, %edi
	leaq	-192(%rbp), %r15
	call	_Znwm@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-216(%rbp), %rsi
	movq	%rax, %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	leaq	88(%r12), %r8
	movq	%rax, 8(%r12)
	xorl	%eax, %eax
	leaq	48(%r12), %r9
	leaq	8(%r12), %r10
	movw	%ax, 24(%r12)
	leaq	64(%r12), %rax
	movq	%rax, 48(%r12)
	leaq	104(%r12), %rax
	movw	%cx, 104(%r12)
	movq	%rax, 88(%r12)
	movw	%dx, 64(%r12)
	movq	0(%r13), %rdx
	movq	$0, 16(%r12)
	movq	$0, 40(%r12)
	movq	$0, 56(%r12)
	movq	$0, 80(%r12)
	movq	$0, 96(%r12)
	movq	$0, 120(%r12)
	movq	$0, 128(%r12)
	movq	%r8, -224(%rbp)
	movq	%r9, -232(%rbp)
	movq	%r10, -240(%rbp)
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	-240(%rbp), %r10
	movq	%r15, %rsi
	leaq	-144(%rbp), %r15
	movq	%r10, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-160(%rbp), %rax
	movl	16(%r13), %esi
	movq	%r15, %rdi
	movq	%rax, 40(%r12)
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	movq	-232(%rbp), %r9
	movq	%r15, %rsi
	leaq	-96(%rbp), %r15
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-112(%rbp), %rax
	movq	8(%r13), %rdx
	movq	%r15, %rdi
	movq	-216(%rbp), %rsi
	movq	%rax, 80(%r12)
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	-224(%rbp), %r8
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 120(%r12)
	movl	24(%r13), %eax
	subl	$1, %eax
	movl	%eax, 128(%r12)
	movl	28(%r13), %eax
	subl	$1, %eax
	movl	%eax, 132(%r12)
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5538
	call	_ZdlPv@PLT
.L5538:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5539
	call	_ZdlPv@PLT
.L5539:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5540
	call	_ZdlPv@PLT
.L5540:
	movl	$40, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	%r12, 8(%rax)
	testq	%r14, %r14
	js	.L5541
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r14, %xmm0
.L5542:
	movq	%rbx, 32(%rax)
	movl	32(%r13), %edx
	movq	-248(%rbp), %rbx
	movsd	%xmm0, 16(%rax)
	movl	%edx, 24(%rax)
	movq	%rax, (%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5569
	addq	$232, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5520:
	.cfi_restore_state
	movq	-224(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L5521
	.p2align 4,,10
	.p2align 3
.L5523:
	movq	%r15, %rdi
	addq	$8, %r12
	call	*%rax
	cmpq	%r12, -232(%rbp)
	jne	.L5535
	jmp	.L5570
	.p2align 4,,10
	.p2align 3
.L5541:
	movq	%r14, %rdx
	andl	$1, %r14d
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%r14, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L5542
	.p2align 4,,10
	.p2align 3
.L5531:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L5530
.L5569:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7658:
	.size	_ZN12v8_inspector12_GLOBAL__N_127buildSampingHeapProfileNodeEPN2v87IsolateEPKNS1_17AllocationProfile4NodeE, .-_ZN12v8_inspector12_GLOBAL__N_127buildSampingHeapProfileNodeEPN2v87IsolateEPKNS1_17AllocationProfile4NodeE
	.section	.rodata._ZN12v8_inspector23V8HeapProfilerAgentImpl18getSamplingProfileEPSt10unique_ptrINS_8protocol12HeapProfiler19SamplingHeapProfileESt14default_deleteIS4_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"V8 sampling heap profiler was not started."
	.section	.text._ZN12v8_inspector23V8HeapProfilerAgentImpl18getSamplingProfileEPSt10unique_ptrINS_8protocol12HeapProfiler19SamplingHeapProfileESt14default_deleteIS4_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector23V8HeapProfilerAgentImpl18getSamplingProfileEPSt10unique_ptrINS_8protocol12HeapProfiler19SamplingHeapProfileESt14default_deleteIS4_EE
	.type	_ZN12v8_inspector23V8HeapProfilerAgentImpl18getSamplingProfileEPSt10unique_ptrINS_8protocol12HeapProfiler19SamplingHeapProfileESt14default_deleteIS4_EE, @function
_ZN12v8_inspector23V8HeapProfilerAgentImpl18getSamplingProfileEPSt10unique_ptrINS_8protocol12HeapProfiler19SamplingHeapProfileESt14default_deleteIS4_EE:
.LFB7660:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%rdi, -152(%rbp)
	movq	16(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate15GetHeapProfilerEv@PLT
	movq	16(%r13), %rsi
	movq	%rax, %r12
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v812HeapProfiler20GetAllocationProfileEv@PLT
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	je	.L5716
	movq	%rax, %r14
	movq	(%rax), %rax
	movq	%r14, %rdi
	call	*(%rax)
	movl	$24, %edi
	movq	%rax, -192(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movq	$0, 16(%rax)
	movq	%rax, %rbx
	movups	%xmm0, (%rax)
	movq	(%r14), %rax
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleE(%rip), %r14
	call	*8(%rax)
	movq	(%rax), %rcx
	movq	8(%rax), %r12
	leaq	-136(%rbp), %rax
	movq	%rax, -160(%rbp)
	cmpq	%r12, %rcx
	je	.L5587
	movq	%rbx, %r15
	movq	%rcx, %rbx
	jmp	.L5586
	.p2align 4,,10
	.p2align 3
.L5718:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L5579:
	movsd	%xmm0, 8(%rax)
	movl	(%rbx), %edx
	movl	%edx, 16(%rax)
	movq	24(%rbx), %rdx
	testq	%rdx, %rdx
	js	.L5580
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L5581:
	movq	%rax, -136(%rbp)
	movq	8(%r15), %rsi
	movsd	%xmm0, 24(%rax)
	cmpq	16(%r15), %rsi
	je	.L5582
	movq	$0, -136(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%r15)
.L5583:
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5584
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L5585
	movl	$32, %esi
	call	_ZdlPvm@PLT
.L5584:
	addq	$32, %rbx
	cmpq	%rbx, %r12
	je	.L5717
.L5586:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%r14, (%rax)
	movq	$0x000000000, 8(%rax)
	movl	$0, 16(%rax)
	movq	$0x000000000, 24(%rax)
	movl	16(%rbx), %edx
	imulq	8(%rbx), %rdx
	testq	%rdx, %rdx
	jns	.L5718
	movq	%rdx, %rsi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rsi
	orq	%rdx, %rsi
	cvtsi2sdq	%rsi, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L5579
	.p2align 4,,10
	.p2align 3
.L5580:
	movq	%rdx, %rsi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rsi
	orq	%rdx, %rsi
	cvtsi2sdq	%rsi, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L5581
	.p2align 4,,10
	.p2align 3
.L5582:
	movq	-160(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L5583
	.p2align 4,,10
	.p2align 3
.L5585:
	call	*%rax
	addq	$32, %rbx
	cmpq	%rbx, %r12
	jne	.L5586
	.p2align 4,,10
	.p2align 3
.L5717:
	movq	%r15, %rbx
.L5587:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	16(%r13), %rsi
	movq	-192(%rbp), %rdx
	movq	%rax, %r12
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileE(%rip), %rax
	movq	-160(%rbp), %rdi
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	call	_ZN12v8_inspector12_GLOBAL__N_127buildSampingHeapProfileNodeEPN2v87IsolateEPKNS1_17AllocationProfile4NodeE
	movq	-136(%rbp), %rax
	movq	8(%r12), %r15
	movq	$0, -136(%rbp)
	movq	%rax, 8(%r12)
	testq	%r15, %r15
	je	.L5577
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %r13
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L5588
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE(%rip), %rax
	movq	%rax, (%r15)
	movq	32(%r15), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L5589
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	cmpq	%r14, %rcx
	je	.L5590
	movq	%rbx, -192(%rbp)
	movq	%r14, %rbx
	movq	%r13, %r14
	movq	%rcx, %r13
	movq	%r12, -200(%rbp)
	jmp	.L5593
	.p2align 4,,10
	.p2align 3
.L5720:
	call	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L5591:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	je	.L5719
.L5593:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L5591
	movq	(%r12), %rdx
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r14, %rdx
	je	.L5720
	call	*%rdx
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L5593
	.p2align 4,,10
	.p2align 3
.L5719:
	movq	-160(%rbp), %rax
	movq	-192(%rbp), %rbx
	movq	-200(%rbp), %r12
	movq	(%rax), %r14
.L5590:
	testq	%r14, %r14
	je	.L5594
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L5594:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L5589:
	movq	8(%r15), %r13
	testq	%r13, %r13
	je	.L5595
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5596
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	104(%r13), %rax
	cmpq	%rax, %rdi
	je	.L5597
	call	_ZdlPv@PLT
.L5597:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L5598
	call	_ZdlPv@PLT
.L5598:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L5599
	call	_ZdlPv@PLT
.L5599:
	movl	$136, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L5595:
	movl	$40, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L5577:
	movq	16(%r12), %r13
	movq	%rbx, 16(%r12)
	testq	%r13, %r13
	je	.L5600
.L5730:
	movq	8(%r13), %rbx
	movq	0(%r13), %r15
	cmpq	%r15, %rbx
	je	.L5601
	leaq	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev(%rip), %r14
	jmp	.L5604
	.p2align 4,,10
	.p2align 3
.L5722:
	movl	$32, %esi
	call	_ZdlPvm@PLT
.L5602:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L5721
.L5604:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L5602
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	je	.L5722
	call	*%rax
	addq	$8, %r15
	cmpq	%r15, %rbx
	jne	.L5604
	.p2align 4,,10
	.p2align 3
.L5721:
	movq	0(%r13), %r15
.L5601:
	testq	%r15, %r15
	je	.L5605
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L5605:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L5600:
	movq	-184(%rbp), %rax
	movq	(%rax), %r14
	movq	%r12, (%rax)
	testq	%r14, %r14
	je	.L5606
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5607
	movq	16(%r14), %r12
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r12, %r12
	je	.L5608
	movq	8(%r12), %rbx
	movq	(%r12), %r15
	cmpq	%r15, %rbx
	je	.L5609
	leaq	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev(%rip), %r13
	jmp	.L5612
	.p2align 4,,10
	.p2align 3
.L5724:
	movl	$32, %esi
	call	_ZdlPvm@PLT
.L5610:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L5723
.L5612:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L5610
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	je	.L5724
	call	*%rax
	addq	$8, %r15
	cmpq	%r15, %rbx
	jne	.L5612
	.p2align 4,,10
	.p2align 3
.L5723:
	movq	(%r12), %r15
.L5609:
	testq	%r15, %r15
	je	.L5613
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L5613:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L5608:
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	je	.L5614
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %r13
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L5615
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE(%rip), %rax
	movq	%rax, (%rbx)
	movq	32(%rbx), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L5616
	movq	8(%rax), %r12
	movq	(%rax), %r15
	cmpq	%r15, %r12
	je	.L5617
	movq	%r14, -184(%rbp)
	jmp	.L5620
	.p2align 4,,10
	.p2align 3
.L5726:
	call	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev
	movl	$40, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L5618:
	addq	$8, %r15
	cmpq	%r15, %r12
	je	.L5725
.L5620:
	movq	(%r15), %r14
	testq	%r14, %r14
	je	.L5618
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	je	.L5726
	call	*%rax
	addq	$8, %r15
	cmpq	%r15, %r12
	jne	.L5620
	.p2align 4,,10
	.p2align 3
.L5725:
	movq	-160(%rbp), %rax
	movq	-184(%rbp), %r14
	movq	(%rax), %r15
.L5617:
	testq	%r15, %r15
	je	.L5621
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L5621:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L5616:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L5622
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5623
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r12), %rdi
	movq	%rax, (%r12)
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L5624
	call	_ZdlPv@PLT
.L5624:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L5625
	call	_ZdlPv@PLT
.L5625:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L5626
	call	_ZdlPv@PLT
.L5626:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L5622:
	movl	$40, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L5614:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L5606:
	movq	-136(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L5627
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %r13
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L5628
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE(%rip), %rax
	movq	%rax, (%rbx)
	movq	32(%rbx), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L5629
	movq	8(%rax), %r12
	movq	(%rax), %r14
	cmpq	%r14, %r12
	jne	.L5633
	jmp	.L5630
	.p2align 4,,10
	.p2align 3
.L5728:
	call	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev
	movl	$40, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L5631:
	addq	$8, %r14
	cmpq	%r14, %r12
	je	.L5727
.L5633:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L5631
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	je	.L5728
	call	*%rax
	addq	$8, %r14
	cmpq	%r14, %r12
	jne	.L5633
	.p2align 4,,10
	.p2align 3
.L5727:
	movq	-160(%rbp), %rax
	movq	(%rax), %r14
.L5630:
	testq	%r14, %r14
	je	.L5634
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L5634:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L5629:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L5635
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5636
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r12), %rdi
	movq	%rax, (%r12)
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L5637
	call	_ZdlPv@PLT
.L5637:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L5638
	call	_ZdlPv@PLT
.L5638:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L5639
	call	_ZdlPv@PLT
.L5639:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L5635:
	movl	$40, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L5627:
	movq	-152(%rbp), %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-168(%rbp), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
.L5574:
	movq	-176(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5729
	movq	-152(%rbp), %rax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5607:
	.cfi_restore_state
	movq	%r14, %rdi
	call	*%rax
	jmp	.L5606
	.p2align 4,,10
	.p2align 3
.L5628:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L5627
	.p2align 4,,10
	.p2align 3
.L5588:
	movq	%r15, %rdi
	call	*%rax
	movq	16(%r12), %r13
	movq	%rbx, 16(%r12)
	testq	%r13, %r13
	jne	.L5730
	jmp	.L5600
	.p2align 4,,10
	.p2align 3
.L5716:
	leaq	-96(%rbp), %r12
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-152(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5574
	call	_ZdlPv@PLT
	jmp	.L5574
.L5596:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L5595
.L5615:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L5614
.L5636:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L5635
.L5623:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L5622
.L5729:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7660:
	.size	_ZN12v8_inspector23V8HeapProfilerAgentImpl18getSamplingProfileEPSt10unique_ptrINS_8protocol12HeapProfiler19SamplingHeapProfileESt14default_deleteIS4_EE, .-_ZN12v8_inspector23V8HeapProfilerAgentImpl18getSamplingProfileEPSt10unique_ptrINS_8protocol12HeapProfiler19SamplingHeapProfileESt14default_deleteIS4_EE
	.weak	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime9CallFrameE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime9CallFrameE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE, 48
_ZTVN12v8_inspector8protocol7Runtime9CallFrameE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.weak	_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE,"awG",@progbits,_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE, @object
	.size	_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE, 48
_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev
	.quad	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev
	.weak	_ZTVN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleE,"awG",@progbits,_ZTVN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleE, @object
	.size	_ZTVN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleE, 48
_ZTVN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD1Ev
	.quad	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev
	.weak	_ZTVN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileE,"awG",@progbits,_ZTVN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileE, @object
	.size	_ZTVN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileE, 48
_ZTVN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD1Ev
	.quad	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD0Ev
	.section	.data.rel.ro.local._ZTVN12v8_inspector12_GLOBAL__N_120HeapSnapshotProgressE,"aw"
	.align 8
	.type	_ZTVN12v8_inspector12_GLOBAL__N_120HeapSnapshotProgressE, @object
	.size	_ZTVN12v8_inspector12_GLOBAL__N_120HeapSnapshotProgressE, 40
_ZTVN12v8_inspector12_GLOBAL__N_120HeapSnapshotProgressE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector12_GLOBAL__N_120HeapSnapshotProgressD1Ev
	.quad	_ZN12v8_inspector12_GLOBAL__N_120HeapSnapshotProgressD0Ev
	.quad	_ZN12v8_inspector12_GLOBAL__N_120HeapSnapshotProgress19ReportProgressValueEii
	.section	.data.rel.ro.local._ZTVN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolverE,"aw"
	.align 8
	.type	_ZTVN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolverE, @object
	.size	_ZTVN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolverE, 40
_ZTVN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolverE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolver7GetNameEN2v85LocalINS2_6ObjectEEE
	.quad	_ZN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolverD1Ev
	.quad	_ZN12v8_inspector12_GLOBAL__N_124GlobalObjectNameResolverD0Ev
	.section	.data.rel.ro.local._ZTVN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStreamE,"aw"
	.align 8
	.type	_ZTVN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStreamE, @object
	.size	_ZTVN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStreamE, 64
_ZTVN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStreamE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStreamD1Ev
	.quad	_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStreamD0Ev
	.quad	_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStream11EndOfStreamEv
	.quad	_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStream12GetChunkSizeEv
	.quad	_ZN12v8_inspector12_GLOBAL__N_124HeapSnapshotOutputStream15WriteAsciiChunkEPci
	.quad	_ZN2v812OutputStream19WriteHeapStatsChunkEPNS_15HeapStatsUpdateEi
	.section	.data.rel.ro.local._ZTVN12v8_inspector12_GLOBAL__N_121InspectableHeapObjectE,"aw"
	.align 8
	.type	_ZTVN12v8_inspector12_GLOBAL__N_121InspectableHeapObjectE, @object
	.size	_ZTVN12v8_inspector12_GLOBAL__N_121InspectableHeapObjectE, 40
_ZTVN12v8_inspector12_GLOBAL__N_121InspectableHeapObjectE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector12_GLOBAL__N_121InspectableHeapObject3getEN2v85LocalINS2_7ContextEEE
	.quad	_ZN12v8_inspector12_GLOBAL__N_121InspectableHeapObjectD1Ev
	.quad	_ZN12v8_inspector12_GLOBAL__N_121InspectableHeapObjectD0Ev
	.section	.data.rel.ro.local._ZTVN12v8_inspector12_GLOBAL__N_115HeapStatsStreamE,"aw"
	.align 8
	.type	_ZTVN12v8_inspector12_GLOBAL__N_115HeapStatsStreamE, @object
	.size	_ZTVN12v8_inspector12_GLOBAL__N_115HeapStatsStreamE, 64
_ZTVN12v8_inspector12_GLOBAL__N_115HeapStatsStreamE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector12_GLOBAL__N_115HeapStatsStreamD1Ev
	.quad	_ZN12v8_inspector12_GLOBAL__N_115HeapStatsStreamD0Ev
	.quad	_ZN12v8_inspector12_GLOBAL__N_115HeapStatsStream11EndOfStreamEv
	.quad	_ZN2v812OutputStream12GetChunkSizeEv
	.quad	_ZN12v8_inspector12_GLOBAL__N_115HeapStatsStream15WriteAsciiChunkEPci
	.quad	_ZN12v8_inspector12_GLOBAL__N_115HeapStatsStream19WriteHeapStatsChunkEPN2v815HeapStatsUpdateEi
	.weak	_ZTVN12v8_inspector23V8HeapProfilerAgentImplE
	.section	.data.rel.ro.local._ZTVN12v8_inspector23V8HeapProfilerAgentImplE,"awG",@progbits,_ZTVN12v8_inspector23V8HeapProfilerAgentImplE,comdat
	.align 8
	.type	_ZTVN12v8_inspector23V8HeapProfilerAgentImplE, @object
	.size	_ZTVN12v8_inspector23V8HeapProfilerAgentImplE, 128
_ZTVN12v8_inspector23V8HeapProfilerAgentImplE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector23V8HeapProfilerAgentImplD1Ev
	.quad	_ZN12v8_inspector23V8HeapProfilerAgentImplD0Ev
	.quad	_ZN12v8_inspector23V8HeapProfilerAgentImpl22addInspectedHeapObjectERKNS_8String16E
	.quad	_ZN12v8_inspector23V8HeapProfilerAgentImpl14collectGarbageEv
	.quad	_ZN12v8_inspector23V8HeapProfilerAgentImpl7disableEv
	.quad	_ZN12v8_inspector23V8HeapProfilerAgentImpl6enableEv
	.quad	_ZN12v8_inspector23V8HeapProfilerAgentImpl15getHeapObjectIdERKNS_8String16EPS1_
	.quad	_ZN12v8_inspector23V8HeapProfilerAgentImpl23getObjectByHeapObjectIdERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISC_EE
	.quad	_ZN12v8_inspector23V8HeapProfilerAgentImpl18getSamplingProfileEPSt10unique_ptrINS_8protocol12HeapProfiler19SamplingHeapProfileESt14default_deleteIS4_EE
	.quad	_ZN12v8_inspector23V8HeapProfilerAgentImpl13startSamplingEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIdEE
	.quad	_ZN12v8_inspector23V8HeapProfilerAgentImpl24startTrackingHeapObjectsEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE
	.quad	_ZN12v8_inspector23V8HeapProfilerAgentImpl12stopSamplingEPSt10unique_ptrINS_8protocol12HeapProfiler19SamplingHeapProfileESt14default_deleteIS4_EE
	.quad	_ZN12v8_inspector23V8HeapProfilerAgentImpl23stopTrackingHeapObjectsEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE
	.quad	_ZN12v8_inspector23V8HeapProfilerAgentImpl16takeHeapSnapshotEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL28samplingHeapProfilerIntervalE,"a"
	.align 16
	.type	_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL28samplingHeapProfilerIntervalE, @object
	.size	_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL28samplingHeapProfilerIntervalE, 29
_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL28samplingHeapProfilerIntervalE:
	.string	"samplingHeapProfilerInterval"
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL27samplingHeapProfilerEnabledE,"a"
	.align 16
	.type	_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL27samplingHeapProfilerEnabledE, @object
	.size	_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL27samplingHeapProfilerEnabledE, 28
_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL27samplingHeapProfilerEnabledE:
	.string	"samplingHeapProfilerEnabled"
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL25allocationTrackingEnabledE,"a"
	.align 16
	.type	_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL25allocationTrackingEnabledE, @object
	.size	_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL25allocationTrackingEnabledE, 26
_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL25allocationTrackingEnabledE:
	.string	"allocationTrackingEnabled"
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL26heapObjectsTrackingEnabledE,"a"
	.align 16
	.type	_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL26heapObjectsTrackingEnabledE, @object
	.size	_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL26heapObjectsTrackingEnabledE, 27
_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL26heapObjectsTrackingEnabledE:
	.string	"heapObjectsTrackingEnabled"
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL19heapProfilerEnabledE,"a"
	.align 16
	.type	_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL19heapProfilerEnabledE, @object
	.size	_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL19heapProfilerEnabledE, 20
_ZN12v8_inspector12_GLOBAL__N_122HeapProfilerAgentStateL19heapProfilerEnabledE:
	.string	"heapProfilerEnabled"
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1088421888
	.align 8
.LC3:
	.long	0
	.long	1138753536
	.align 8
.LC8:
	.long	2576980378
	.long	1068079513
	.align 8
.LC11:
	.long	0
	.long	-1074790400
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
