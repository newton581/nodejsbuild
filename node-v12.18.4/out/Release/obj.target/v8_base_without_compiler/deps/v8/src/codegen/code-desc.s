	.file	"code-desc.cc"
	.text
	.section	.text._ZN2v88internal8CodeDesc10InitializeEPS1_PNS0_9AssemblerEiiiii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8CodeDesc10InitializeEPS1_PNS0_9AssemblerEiiiii
	.type	_ZN2v88internal8CodeDesc10InitializeEPS1_PNS0_9AssemblerEiiiii, @function
_ZN2v88internal8CodeDesc10InitializeEPS1_PNS0_9AssemblerEiiiii:
.LFB19042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rsi), %rdi
	movl	%r9d, -52(%rbp)
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%rax, (%rbx)
	movq	8(%r12), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movl	-52(%rbp), %r9d
	movl	16(%rbp), %edx
	movl	%eax, 8(%rbx)
	movq	32(%r12), %rcx
	subq	16(%r12), %rcx
	subl	%edx, %eax
	movl	%r9d, 40(%rbx)
	movl	%ecx, 12(%rbx)
	subl	%r9d, %ecx
	subl	%r14d, %r9d
	movl	%r14d, 32(%rbx)
	subl	%r13d, %r14d
	movl	%r13d, 24(%rbx)
	subl	%r15d, %r13d
	movl	%r14d, 28(%rbx)
	movl	%r15d, 16(%rbx)
	movl	%r13d, 20(%rbx)
	movq	%r12, 72(%rbx)
	movl	%ecx, 44(%rbx)
	movl	%r9d, 36(%rbx)
	movl	%edx, 48(%rbx)
	movl	%eax, 52(%rbx)
	movl	$0, 64(%rbx)
	movq	$0, 56(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19042:
	.size	_ZN2v88internal8CodeDesc10InitializeEPS1_PNS0_9AssemblerEiiiii, .-_ZN2v88internal8CodeDesc10InitializeEPS1_PNS0_9AssemblerEiiiii
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8CodeDesc10InitializeEPS1_PNS0_9AssemblerEiiiii,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8CodeDesc10InitializeEPS1_PNS0_9AssemblerEiiiii, @function
_GLOBAL__sub_I__ZN2v88internal8CodeDesc10InitializeEPS1_PNS0_9AssemblerEiiiii:
.LFB22756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22756:
	.size	_GLOBAL__sub_I__ZN2v88internal8CodeDesc10InitializeEPS1_PNS0_9AssemblerEiiiii, .-_GLOBAL__sub_I__ZN2v88internal8CodeDesc10InitializeEPS1_PNS0_9AssemblerEiiiii
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8CodeDesc10InitializeEPS1_PNS0_9AssemblerEiiiii
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
