	.file	"string-16.cc"
	.text
	.section	.rodata._ZN12v8_inspector8String16C2EPKtm.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZN12v8_inspector8String16C2EPKtm.str1.1,"aMS",@progbits,1
.LC1:
	.string	"basic_string::_M_create"
	.section	.text._ZN12v8_inspector8String16C2EPKtm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8String16C2EPKtm
	.type	_ZN12v8_inspector8String16C2EPKtm, @function
_ZN12v8_inspector8String16C2EPKtm:
.LFB3998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	(%rdx,%rdx), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$16, %rdi
	subq	$8, %rsp
	addq	%r12, %rax
	movq	%rdi, (%rbx)
	je	.L2
	testq	%rsi, %rsi
	je	.L18
.L2:
	movq	%r12, %r15
	sarq	%r15
	cmpq	$14, %r12
	ja	.L19
.L3:
	cmpq	$2, %r12
	je	.L20
	testq	%r12, %r12
	je	.L6
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	movq	(%rbx), %rdi
.L6:
	xorl	%eax, %eax
	movq	%r15, 8(%rbx)
	movw	%ax, (%rdi,%r14,2)
	movq	$0, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movzwl	0(%r13), %eax
	movw	%ax, (%rdi)
	movq	(%rbx), %rdi
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L19:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r15
	ja	.L21
	leaq	2(%r12), %rdi
	call	_Znwm@PLT
	movq	%r15, 16(%rbx)
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	jmp	.L3
.L18:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L21:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE3998:
	.size	_ZN12v8_inspector8String16C2EPKtm, .-_ZN12v8_inspector8String16C2EPKtm
	.globl	_ZN12v8_inspector8String16C1EPKtm
	.set	_ZN12v8_inspector8String16C1EPKtm,_ZN12v8_inspector8String16C2EPKtm
	.section	.text._ZN12v8_inspector8String16C2EPKt,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8String16C2EPKt
	.type	_ZN12v8_inspector8String16C2EPKt, @function
_ZN12v8_inspector8String16C2EPKt:
.LFB4001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$16, %rdi
	movq	%rdi, (%rbx)
	testq	%rsi, %rsi
	je	.L23
	cmpw	$0, (%rsi)
	movq	%rsi, %r12
	je	.L30
	movl	$2, %r8d
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%r8, %r13
	addq	$2, %r8
	cmpw	$0, -2(%r12,%r8)
	jne	.L25
	movq	%r13, %rax
	sarq	%rax
	movq	%rax, %r14
	cmpq	$7, %rax
	ja	.L37
.L27:
	cmpq	$2, %r13
	je	.L38
	testq	%r13, %r13
	je	.L24
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memmove@PLT
	movq	(%rbx), %rdi
.L24:
	xorl	%eax, %eax
	movq	%r14, 8(%rbx)
	movw	%ax, (%rdi,%r13)
	movq	$0, 32(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movzwl	(%r12), %eax
	movw	%ax, (%rdi)
	movq	(%rbx), %rdi
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L37:
	movabsq	$2305843009213693951, %rdx
	cmpq	%rdx, %rax
	ja	.L39
	movq	%r8, %rdi
	call	_Znwm@PLT
	movq	%r14, 16(%rbx)
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L23:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L30:
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	jmp	.L24
.L39:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4001:
	.size	_ZN12v8_inspector8String16C2EPKt, .-_ZN12v8_inspector8String16C2EPKt
	.globl	_ZN12v8_inspector8String16C1EPKt
	.set	_ZN12v8_inspector8String16C1EPKt,_ZN12v8_inspector8String16C2EPKt
	.section	.text._ZN12v8_inspector8String16C2EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8String16C2EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE
	.type	_ZN12v8_inspector8String16C2EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE, @function
_ZN12v8_inspector8String16C2EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE:
.LFB4013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$16, %rdi
	subq	$8, %rsp
	movq	%rdi, (%rbx)
	movq	(%rsi), %r13
	movq	8(%rsi), %r14
	movq	%r13, %rax
	leaq	(%r14,%r14), %r12
	addq	%r12, %rax
	je	.L41
	testq	%r13, %r13
	je	.L56
.L41:
	movq	%r12, %r15
	sarq	%r15
	cmpq	$14, %r12
	ja	.L57
.L42:
	cmpq	$2, %r12
	je	.L58
	testq	%r12, %r12
	je	.L45
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	movq	(%rbx), %rdi
.L45:
	xorl	%eax, %eax
	movq	%r15, 8(%rbx)
	movw	%ax, (%rdi,%r14,2)
	movq	$0, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movzwl	0(%r13), %eax
	movw	%ax, (%rdi)
	movq	(%rbx), %rdi
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L57:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r15
	ja	.L59
	leaq	2(%r12), %rdi
	call	_Znwm@PLT
	movq	%r15, 16(%rbx)
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	jmp	.L42
.L56:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L59:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4013:
	.size	_ZN12v8_inspector8String16C2EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE, .-_ZN12v8_inspector8String16C2EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE
	.globl	_ZN12v8_inspector8String16C1ERKNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE
	.set	_ZN12v8_inspector8String16C1ERKNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE,_ZN12v8_inspector8String16C2EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE
	.globl	_ZN12v8_inspector8String16C2ERKNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE
	.set	_ZN12v8_inspector8String16C2ERKNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE,_ZN12v8_inspector8String16C2EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE
	.globl	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE
	.set	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE,_ZN12v8_inspector8String16C2EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE
	.section	.text._ZNK12v8_inspector8String1615stripWhiteSpaceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8String1615stripWhiteSpaceEv
	.type	_ZNK12v8_inspector8String1615stripWhiteSpaceEv, @function
_ZNK12v8_inspector8String1615stripWhiteSpaceEv:
.LFB4026:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %rax
	testq	%rax, %rax
	je	.L120
	movq	%rsi, %rbx
	movq	(%rsi), %rsi
	leaq	-1(%rax), %rcx
	xorl	%r8d, %r8d
	movq	%rsi, %r13
.L69:
	movzwl	0(%r13), %edx
	testl	$65408, %edx
	jne	.L67
	cmpw	$32, %dx
	jbe	.L63
.L67:
	movq	%rcx, %rax
	testq	%rcx, %rcx
	je	.L66
.L65:
	movzwl	(%rsi,%rax,2), %edx
	testl	$65408, %edx
	jne	.L66
	cmpw	$32, %dx
	ja	.L66
	leal	-9(%rdx), %r9d
	cmpw	$4, %r9w
	jbe	.L88
	cmpw	$32, %dx
	je	.L88
	.p2align 4,,10
	.p2align 3
.L66:
	testq	%r8, %r8
	jne	.L72
	cmpq	%rax, %rcx
	je	.L122
.L72:
	subq	%r8, %rax
	movq	%rdi, (%r12)
	leaq	1(%rax), %rbx
	leaq	(%rbx,%rbx), %r14
	movq	%r14, %r15
	sarq	%r15
	cmpq	$14, %r14
	ja	.L123
.L85:
	cmpq	$2, %r14
	je	.L124
	testq	%r14, %r14
	je	.L77
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	movq	(%r12), %rdi
.L77:
	xorl	%edx, %edx
	movq	%r15, 8(%r12)
	movw	%dx, (%rdi,%rbx,2)
	movq	$0, 32(%r12)
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L63:
	leal	-9(%rdx), %r9d
	cmpw	$4, %r9w
	jbe	.L87
	cmpw	$32, %dx
	jne	.L67
.L87:
	addq	$1, %r8
	addq	$2, %r13
	cmpq	%r8, %rax
	jne	.L69
	.p2align 4,,10
	.p2align 3
.L120:
	movq	$0, 32(%r12)
	pxor	%xmm0, %xmm0
	movq	%rdi, (%r12)
	movq	$0, 8(%r12)
	movups	%xmm0, 16(%r12)
.L60:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	movzwl	0(%r13), %eax
	movw	%ax, (%rdi)
	movq	(%r12), %rdi
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L123:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r15
	ja	.L81
	leaq	2(%r14), %rdi
	call	_Znwm@PLT
	movq	%r15, 16(%r12)
	movq	%rax, (%r12)
	movq	%rax, %rdi
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L122:
	movq	%rdi, (%r12)
	movq	(%rbx), %r14
	movq	8(%rbx), %r15
	movq	%r14, %rax
	leaq	(%r15,%r15), %r13
	addq	%r13, %rax
	je	.L89
	testq	%r14, %r14
	je	.L78
.L89:
	movq	%r13, %rcx
	sarq	%rcx
	cmpq	$14, %r13
	ja	.L125
.L80:
	cmpq	$2, %r13
	je	.L126
	testq	%r13, %r13
	je	.L83
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	(%r12), %rdi
	movq	-56(%rbp), %rcx
.L83:
	xorl	%eax, %eax
	movq	%rcx, 8(%r12)
	movw	%ax, (%rdi,%r15,2)
	movq	32(%rbx), %rax
	movq	%rax, 32(%r12)
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L88:
	subq	$1, %rax
	jne	.L65
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L126:
	movzwl	(%r14), %eax
	movw	%ax, (%rdi)
	movq	(%r12), %rdi
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L125:
	movabsq	$2305843009213693951, %rax
	movq	%rcx, -56(%rbp)
	cmpq	%rax, %rcx
	ja	.L81
	leaq	2(%r13), %rdi
	call	_Znwm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	%rcx, 16(%r12)
	jmp	.L80
.L81:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L78:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE4026:
	.size	_ZNK12v8_inspector8String1615stripWhiteSpaceEv, .-_ZNK12v8_inspector8String1615stripWhiteSpaceEv
	.section	.text._ZN12v8_inspector15String16BuilderC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15String16BuilderC2Ev
	.type	_ZN12v8_inspector15String16BuilderC2Ev, @function
_ZN12v8_inspector15String16BuilderC2Ev:
.LFB4043:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE4043:
	.size	_ZN12v8_inspector15String16BuilderC2Ev, .-_ZN12v8_inspector15String16BuilderC2Ev
	.globl	_ZN12v8_inspector15String16BuilderC1Ev
	.set	_ZN12v8_inspector15String16BuilderC1Ev,_ZN12v8_inspector15String16BuilderC2Ev
	.section	.rodata._ZN12v8_inspector15String16Builder6appendERKNS_8String16E.str1.1,"aMS",@progbits,1
.LC2:
	.string	"vector::_M_range_insert"
	.section	.text._ZN12v8_inspector15String16Builder6appendERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E
	.type	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E, @function
_ZN12v8_inspector15String16Builder6appendERKNS_8String16E:
.LFB4045:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %r12
	addq	%r12, %r12
	je	.L128
	movq	8(%rdi), %r8
	movq	16(%rdi), %rax
	movq	%r12, %rdx
	movq	%rdi, %rbx
	sarq	%rdx
	movq	(%rsi), %r9
	subq	%r8, %rax
	sarq	%rax
	cmpq	%rax, %rdx
	ja	.L130
	movq	%r12, %rdx
	movq	%r9, %rsi
	movq	%r8, %rdi
	call	memmove@PLT
	addq	%r12, 8(%rbx)
.L128:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	movabsq	$4611686018427387903, %rcx
	movq	(%rdi), %r14
	movq	%r8, %r11
	movq	%rcx, %rsi
	subq	%r14, %r11
	movq	%r11, %rax
	sarq	%rax
	subq	%rax, %rsi
	cmpq	%rsi, %rdx
	ja	.L155
	cmpq	%rax, %rdx
	cmovb	%rax, %rdx
	addq	%rax, %rdx
	jc	.L133
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	testq	%rdx, %rdx
	jne	.L156
.L135:
	leaq	0(%r13,%r11), %r10
	leaq	(%r10,%r12), %rax
	movq	%rax, -56(%rbp)
	cmpq	%r14, %r8
	je	.L136
	movq	%r11, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r8, -64(%rbp)
	movq	%r10, -80(%rbp)
	movq	%r9, -72(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %r10
	movq	%r12, %rdx
	movq	%r9, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-64(%rbp), %r8
	movq	%rdx, %rax
	subq	%r8, %rax
	cmpq	%rdx, %r8
	je	.L157
.L137:
	movq	-56(%rbp), %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	movq	%rax, %r12
	call	memcpy@PLT
.L139:
	addq	-56(%rbp), %r12
	testq	%r14, %r14
	jne	.L141
.L140:
	movq	%r13, %xmm0
	movq	%r12, %xmm1
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r9, %rsi
	movq	%r10, %rdi
	movq	%r8, -64(%rbp)
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-64(%rbp), %r8
	xorl	%r12d, %r12d
	movq	%rdx, %rax
	subq	%r8, %rax
	cmpq	%rdx, %r8
	jne	.L137
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L156:
	cmpq	%rcx, %rdx
	cmova	%rcx, %rdx
	leaq	(%rdx,%rdx), %r15
.L134:
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r8
	movq	(%rbx), %r14
	movq	-64(%rbp), %r9
	movq	%rax, %r13
	addq	%rax, %r15
	movq	%r8, %r11
	subq	%r14, %r11
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L157:
	movq	-56(%rbp), %r12
	addq	%rax, %r12
.L141:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	jmp	.L140
.L155:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L133:
	movabsq	$9223372036854775806, %r15
	jmp	.L134
	.cfi_endproc
.LFE4045:
	.size	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E, .-_ZN12v8_inspector15String16Builder6appendERKNS_8String16E
	.section	.rodata._ZN12v8_inspector15String16Builder6appendEt.str1.1,"aMS",@progbits,1
.LC3:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN12v8_inspector15String16Builder6appendEt,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15String16Builder6appendEt
	.type	_ZN12v8_inspector15String16Builder6appendEt, @function
_ZN12v8_inspector15String16Builder6appendEt:
.LFB4046:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rdx
	cmpq	16(%rdi), %rdx
	je	.L159
	movw	%si, (%rdx)
	addq	$2, 8(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	movq	(%rdi), %r12
	subq	%r12, %rdx
	movq	%rdx, %rax
	movq	%rdx, %r13
	movabsq	$4611686018427387903, %rdx
	sarq	%rax
	cmpq	%rdx, %rax
	je	.L174
	testq	%rax, %rax
	je	.L168
	movabsq	$9223372036854775806, %r15
	cmpq	%r13, %rax
	jbe	.L175
.L162:
	movq	%r15, %rdi
	movl	%esi, -56(%rbp)
	call	_Znwm@PLT
	movl	-56(%rbp), %esi
	movq	%rax, %r14
	addq	%rax, %r15
	movw	%si, (%rax,%r13)
	leaq	2(%rax,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%r13, %r13
	jle	.L167
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	memmove@PLT
.L164:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L165:
	movq	%r14, %xmm0
	movq	%r15, 16(%rbx)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L176
	movw	%si, 0
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	movq	$2, -56(%rbp)
.L167:
	testq	%r12, %r12
	je	.L165
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L168:
	movl	$2, %r15d
	jmp	.L162
.L174:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L176:
	cmpq	%rdx, %r13
	cmovbe	%r13, %rdx
	movq	%rdx, %r15
	addq	%rdx, %r15
	jmp	.L162
	.cfi_endproc
.LFE4046:
	.size	_ZN12v8_inspector15String16Builder6appendEt, .-_ZN12v8_inspector15String16Builder6appendEt
	.section	.text._ZN12v8_inspector15String16Builder6appendEc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15String16Builder6appendEc
	.type	_ZN12v8_inspector15String16Builder6appendEc, @function
_ZN12v8_inspector15String16Builder6appendEc:
.LFB4047:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movsbw	%sil, %r12w
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rdx
	cmpq	16(%rdi), %rdx
	je	.L178
	movw	%r12w, (%rdx)
	addq	$2, 8(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movq	(%rdi), %r8
	subq	%r8, %rdx
	movq	%rdx, %rax
	movq	%rdx, %r13
	movabsq	$4611686018427387903, %rdx
	sarq	%rax
	cmpq	%rdx, %rax
	je	.L193
	testq	%rax, %rax
	je	.L187
	movabsq	$9223372036854775806, %r15
	cmpq	%r13, %rax
	jbe	.L194
.L181:
	movq	%r15, %rdi
	movq	%r8, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %r8
	addq	%rax, %r15
	movw	%r12w, (%rax,%r13)
	movq	%rax, %r14
	testq	%r13, %r13
	leaq	2(%rax,%r13), %rax
	movq	%rax, -56(%rbp)
	jle	.L186
	movq	%r8, %rsi
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%r8, -64(%rbp)
	call	memmove@PLT
	movq	-64(%rbp), %r8
.L183:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L184:
	movq	%r14, %xmm0
	movq	%r15, 16(%rbx)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L195
	movw	%r12w, 0
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	movq	$2, -56(%rbp)
.L186:
	testq	%r8, %r8
	je	.L184
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L187:
	movl	$2, %r15d
	jmp	.L181
.L193:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L195:
	cmpq	%rdx, %r13
	cmovbe	%r13, %rdx
	movq	%rdx, %r15
	addq	%rdx, %r15
	jmp	.L181
	.cfi_endproc
.LFE4047:
	.size	_ZN12v8_inspector15String16Builder6appendEc, .-_ZN12v8_inspector15String16Builder6appendEc
	.section	.text._ZN12v8_inspector15String16Builder6appendEPKtm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15String16Builder6appendEPKtm
	.type	_ZN12v8_inspector15String16Builder6appendEPKtm, @function
_ZN12v8_inspector15String16Builder6appendEPKtm:
.LFB4048:
	.cfi_startproc
	endbr64
	addq	%rdx, %rdx
	je	.L223
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	sarq	%rdx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %r9
	movq	16(%rdi), %rax
	subq	%r9, %rax
	sarq	%rax
	cmpq	%rax, %rdx
	ja	.L198
	movq	%r12, %rdx
	movq	%r9, %rdi
	call	memmove@PLT
	addq	%r12, 8(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	movabsq	$4611686018427387903, %rcx
	movq	(%rdi), %r14
	movq	%r9, %r11
	movq	%rcx, %rsi
	subq	%r14, %r11
	movq	%r11, %rax
	sarq	%rax
	subq	%rax, %rsi
	cmpq	%rsi, %rdx
	ja	.L226
	cmpq	%rax, %rdx
	cmovb	%rax, %rdx
	addq	%rax, %rdx
	jc	.L201
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	testq	%rdx, %rdx
	jne	.L227
.L203:
	leaq	0(%r13,%r11), %r10
	leaq	(%r10,%r12), %rax
	movq	%rax, -56(%rbp)
	cmpq	%r14, %r9
	je	.L204
	movq	%r11, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r9, -64(%rbp)
	movq	%r8, -80(%rbp)
	movq	%r10, -72(%rbp)
	call	memmove@PLT
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %r10
	movq	%r12, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-64(%rbp), %r9
	movq	%rdx, %rax
	subq	%r9, %rax
	cmpq	%rdx, %r9
	je	.L228
.L205:
	movq	-56(%rbp), %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	movq	%rax, %r12
	call	memcpy@PLT
.L207:
	addq	-56(%rbp), %r12
	testq	%r14, %r14
	jne	.L209
.L208:
	movq	%r13, %xmm0
	movq	%r12, %xmm1
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L204:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r12, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-64(%rbp), %r9
	xorl	%r12d, %r12d
	movq	%rdx, %rax
	subq	%r9, %rax
	cmpq	%rdx, %r9
	jne	.L205
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L227:
	cmpq	%rcx, %rdx
	cmova	%rcx, %rdx
	leaq	(%rdx,%rdx), %r15
.L202:
	movq	%r15, %rdi
	movq	%r8, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	(%rbx), %r14
	movq	-64(%rbp), %r8
	movq	%rax, %r13
	addq	%rax, %r15
	movq	%r9, %r11
	subq	%r14, %r11
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L228:
	movq	-56(%rbp), %r12
	addq	%rax, %r12
.L209:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	jmp	.L208
.L226:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L201:
	movabsq	$9223372036854775806, %r15
	jmp	.L202
	.cfi_endproc
.LFE4048:
	.size	_ZN12v8_inspector15String16Builder6appendEPKtm, .-_ZN12v8_inspector15String16Builder6appendEPKtm
	.section	.text._ZN12v8_inspector15String16Builder6appendEPKcm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15String16Builder6appendEPKcm
	.type	_ZN12v8_inspector15String16Builder6appendEPKcm, @function
_ZN12v8_inspector15String16Builder6appendEPKcm:
.LFB4049:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L352
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	8(%rdi), %r13
	movq	16(%rdi), %rax
	subq	%r13, %rax
	sarq	%rax
	cmpq	%rax, %rdx
	ja	.L231
	testq	%rdx, %rdx
	jle	.L232
	leaq	0(%r13,%rdx,2), %rax
	cmpq	%rax, %rsi
	leaq	(%rsi,%rbx), %rax
	setnb	%dl
	cmpq	%rax, %r13
	setnb	%al
	orb	%al, %dl
	je	.L249
	leaq	-1(%rbx), %rax
	cmpq	$14, %rax
	jbe	.L249
	movq	%rbx, %rdx
	xorl	%eax, %eax
	pxor	%xmm3, %xmm3
	andq	$-16, %rdx
.L234:
	movdqu	(%r12,%rax), %xmm0
	movdqa	%xmm3, %xmm1
	pcmpgtb	%xmm0, %xmm1
	movdqa	%xmm0, %xmm2
	punpcklbw	%xmm1, %xmm2
	punpckhbw	%xmm1, %xmm0
	movups	%xmm2, 0(%r13,%rax,2)
	movups	%xmm0, 16(%r13,%rax,2)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L234
	movq	%rbx, %rsi
	movq	%rbx, %rdx
	andq	$-16, %rsi
	andl	$15, %edx
	addq	%rsi, %r12
	leaq	0(%r13,%rsi,2), %rax
	cmpq	%rsi, %rbx
	je	.L236
	movsbw	(%r12), %si
	movw	%si, (%rax)
	cmpq	$1, %rdx
	je	.L236
	movsbw	1(%r12), %si
	movw	%si, 2(%rax)
	cmpq	$2, %rdx
	je	.L236
	movsbw	2(%r12), %si
	movw	%si, 4(%rax)
	cmpq	$3, %rdx
	je	.L236
	movsbw	3(%r12), %si
	movw	%si, 6(%rax)
	cmpq	$4, %rdx
	je	.L236
	movsbw	4(%r12), %si
	movw	%si, 8(%rax)
	cmpq	$5, %rdx
	je	.L236
	movsbw	5(%r12), %si
	movw	%si, 10(%rax)
	cmpq	$6, %rdx
	je	.L236
	movsbw	6(%r12), %si
	movw	%si, 12(%rax)
	cmpq	$7, %rdx
	je	.L236
	movsbw	7(%r12), %si
	movw	%si, 14(%rax)
	cmpq	$8, %rdx
	je	.L236
	movsbw	8(%r12), %si
	movw	%si, 16(%rax)
	cmpq	$9, %rdx
	je	.L236
	movsbw	9(%r12), %si
	movw	%si, 18(%rax)
	cmpq	$10, %rdx
	je	.L236
	movsbw	10(%r12), %si
	movw	%si, 20(%rax)
	cmpq	$11, %rdx
	je	.L236
	movsbw	11(%r12), %si
	movw	%si, 22(%rax)
	cmpq	$12, %rdx
	je	.L236
	movsbw	12(%r12), %si
	movw	%si, 24(%rax)
	cmpq	$13, %rdx
	je	.L236
	movsbw	13(%r12), %si
	movw	%si, 26(%rax)
	cmpq	$14, %rdx
	je	.L236
	movsbw	14(%r12), %dx
	movw	%dx, 28(%rax)
	.p2align 4,,10
	.p2align 3
.L236:
	movq	8(%r15), %r13
.L232:
	leaq	0(%r13,%rbx,2), %rax
	movq	%rax, 8(%r15)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	.cfi_restore_state
	movabsq	$4611686018427387903, %r14
	movq	(%rdi), %r9
	movq	%r13, %rdx
	movq	%r14, %rsi
	subq	%r9, %rdx
	movq	%rdx, %rax
	sarq	%rax
	subq	%rax, %rsi
	cmpq	%rsi, %rbx
	ja	.L355
	cmpq	%rax, %rbx
	movq	%rax, %rsi
	cmovnb	%rbx, %rsi
	addq	%rsi, %rax
	movq	%rax, %r10
	jc	.L250
	testq	%rax, %rax
	jne	.L356
	movq	$0, -56(%rbp)
	movq	%r13, %r11
	xorl	%r14d, %r14d
.L241:
	cmpq	%r9, %r13
	je	.L242
	movq	-56(%rbp), %rdi
	movq	%r9, %rsi
	movq	%r8, -96(%rbp)
	movq	%r10, -88(%rbp)
	movq	%r11, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	movq	-96(%rbp), %r8
	movq	-88(%rbp), %r10
	movq	-80(%rbp), %r11
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %r9
.L242:
	movq	-56(%rbp), %rax
	leaq	(%rax,%rdx), %rdi
	testq	%rbx, %rbx
	jle	.L243
	cmpq	$15, %rbx
	jle	.L252
	movq	%rbx, %rdx
	xorl	%eax, %eax
	pxor	%xmm3, %xmm3
	andq	$-16, %rdx
	.p2align 4,,10
	.p2align 3
.L245:
	movdqu	(%r12,%rax), %xmm0
	movdqa	%xmm3, %xmm1
	pcmpgtb	%xmm0, %xmm1
	movdqa	%xmm0, %xmm2
	punpcklbw	%xmm1, %xmm2
	punpckhbw	%xmm1, %xmm0
	movups	%xmm2, (%rdi,%rax,2)
	movups	%xmm0, 16(%rdi,%rax,2)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L245
	movq	%rbx, %rdx
	movq	%rbx, %r8
	andq	$-16, %rdx
	andl	$15, %r8d
	addq	%rdx, %r12
	leaq	(%rdi,%rdx,2), %rax
	cmpq	%rdx, %rbx
	je	.L246
.L244:
	movsbw	(%r12), %dx
	movw	%dx, (%rax)
	cmpq	$1, %r8
	je	.L246
	movsbw	1(%r12), %dx
	movw	%dx, 2(%rax)
	cmpq	$2, %r8
	je	.L246
	movsbw	2(%r12), %dx
	movw	%dx, 4(%rax)
	cmpq	$3, %r8
	je	.L246
	movsbw	3(%r12), %dx
	movw	%dx, 6(%rax)
	cmpq	$4, %r8
	je	.L246
	movsbw	4(%r12), %dx
	movw	%dx, 8(%rax)
	cmpq	$5, %r8
	je	.L246
	movsbw	5(%r12), %dx
	movw	%dx, 10(%rax)
	cmpq	$6, %r8
	je	.L246
	movsbw	6(%r12), %dx
	movw	%dx, 12(%rax)
	cmpq	$7, %r8
	je	.L246
	movsbw	7(%r12), %dx
	movw	%dx, 14(%rax)
	cmpq	$8, %r8
	je	.L246
	movsbw	8(%r12), %dx
	movw	%dx, 16(%rax)
	cmpq	$9, %r8
	je	.L246
	movsbw	9(%r12), %dx
	movw	%dx, 18(%rax)
	cmpq	$10, %r8
	je	.L246
	movsbw	10(%r12), %dx
	movw	%dx, 20(%rax)
	cmpq	$11, %r8
	je	.L246
	movsbw	11(%r12), %dx
	movw	%dx, 22(%rax)
	cmpq	$12, %r8
	je	.L246
	movsbw	12(%r12), %dx
	movw	%dx, 24(%rax)
	cmpq	$13, %r8
	je	.L246
	movsbw	13(%r12), %dx
	movw	%dx, 26(%rax)
	cmpq	$14, %r8
	je	.L246
	movsbw	14(%r12), %dx
	movw	%dx, 28(%rax)
	.p2align 4,,10
	.p2align 3
.L246:
	leaq	(%rdi,%rbx,2), %rdi
.L243:
	cmpq	%r11, %r13
	je	.L247
	movq	%r10, %rdx
	movq	%r13, %rsi
	movq	%r9, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %r10
	movq	%rax, %rdi
.L247:
	leaq	(%rdi,%r10), %rbx
	testq	%r9, %r9
	je	.L248
	movq	%r9, %rdi
	call	_ZdlPv@PLT
.L248:
	movq	-56(%rbp), %xmm0
	movq	%rbx, %xmm4
	movq	%r14, 16(%r15)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, (%r15)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L352:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movabsq	$9223372036854775806, %r14
.L240:
	movq	%r14, %rdi
	movq	%r8, -64(%rbp)
	call	_Znwm@PLT
	movq	8(%r15), %r11
	movq	(%r15), %r9
	movq	%r13, %rdx
	movq	%rax, -56(%rbp)
	movq	-64(%rbp), %r8
	addq	%rax, %r14
	movq	%r11, %r10
	subq	%r9, %rdx
	subq	%r13, %r10
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L249:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L233:
	movsbw	(%r12,%rax), %dx
	movw	%dx, 0(%r13,%rax,2)
	addq	$1, %rax
	cmpq	%rax, %rbx
	jne	.L233
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L356:
	cmpq	%r14, %rax
	cmova	%r14, %rax
	leaq	(%rax,%rax), %r14
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L252:
	movq	%rdi, %rax
	jmp	.L244
.L355:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4049:
	.size	_ZN12v8_inspector15String16Builder6appendEPKcm, .-_ZN12v8_inspector15String16Builder6appendEPKcm
	.section	.rodata._ZN12v8_inspector15String16Builder12appendNumberEi.str1.1,"aMS",@progbits,1
.LC4:
	.string	"%d"
	.section	.text._ZN12v8_inspector15String16Builder12appendNumberEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15String16Builder12appendNumberEi
	.type	_ZN12v8_inspector15String16Builder12appendNumberEi, @function
_ZN12v8_inspector15String16Builder12appendNumberEi:
.LFB4050:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	leaq	.LC4(%rip), %rdx
	movl	$11, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base2OS8SNPrintFEPciPKcz@PLT
	movslq	%eax, %r12
	testq	%r12, %r12
	je	.L357
	movq	8(%r15), %r13
	movq	16(%r15), %rax
	movq	%r12, %rcx
	subq	%r13, %rax
	sarq	%rax
	cmpq	%rax, %r12
	ja	.L359
	testq	%r12, %r12
	jle	.L360
	leaq	(%rbx,%r12), %rdx
	leaq	0(%r13,%r12,2), %rax
	cmpq	%rdx, %r13
	setnb	%cl
	cmpq	%rbx, %rax
	setbe	%dl
	orb	%dl, %cl
	je	.L361
	leaq	-1(%r12), %rdx
	cmpq	$14, %rdx
	jbe	.L361
	movq	%r12, %rdx
	xorl	%eax, %eax
	pxor	%xmm3, %xmm3
	andq	$-16, %rdx
.L362:
	movdqa	(%rbx,%rax), %xmm0
	movdqa	%xmm3, %xmm1
	pcmpgtb	%xmm0, %xmm1
	movdqa	%xmm0, %xmm2
	punpcklbw	%xmm1, %xmm2
	punpckhbw	%xmm1, %xmm0
	movups	%xmm2, 0(%r13,%rax,2)
	movups	%xmm0, 16(%r13,%rax,2)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L362
	movq	%r12, %rcx
	movq	%r12, %rdx
	andq	$-16, %rcx
	andl	$15, %edx
	addq	%rcx, %rbx
	leaq	0(%r13,%rcx,2), %rax
	cmpq	%rcx, %r12
	je	.L364
	movsbw	(%rbx), %cx
	movw	%cx, (%rax)
	cmpq	$1, %rdx
	je	.L364
	movsbw	1(%rbx), %cx
	movw	%cx, 2(%rax)
	cmpq	$2, %rdx
	je	.L364
	movsbw	2(%rbx), %cx
	movw	%cx, 4(%rax)
	cmpq	$3, %rdx
	je	.L364
	movsbw	3(%rbx), %cx
	movw	%cx, 6(%rax)
	cmpq	$4, %rdx
	je	.L364
	movsbw	4(%rbx), %cx
	movw	%cx, 8(%rax)
	cmpq	$5, %rdx
	je	.L364
	movsbw	5(%rbx), %cx
	movw	%cx, 10(%rax)
	cmpq	$6, %rdx
	je	.L364
	movsbw	6(%rbx), %cx
	movw	%cx, 12(%rax)
	cmpq	$7, %rdx
	je	.L364
	movsbw	7(%rbx), %cx
	movw	%cx, 14(%rax)
	cmpq	$8, %rdx
	je	.L364
	movsbw	8(%rbx), %cx
	movw	%cx, 16(%rax)
	cmpq	$9, %rdx
	je	.L364
	movsbw	9(%rbx), %cx
	movw	%cx, 18(%rax)
	cmpq	$10, %rdx
	je	.L364
	movsbw	10(%rbx), %cx
	movw	%cx, 20(%rax)
	cmpq	$11, %rdx
	je	.L364
	movsbw	11(%rbx), %cx
	movw	%cx, 22(%rax)
	cmpq	$12, %rdx
	je	.L364
	movsbw	12(%rbx), %cx
	movw	%cx, 24(%rax)
	cmpq	$13, %rdx
	je	.L364
	movsbw	13(%rbx), %cx
	movw	%cx, 26(%rax)
	cmpq	$14, %rdx
	je	.L364
	movsbw	14(%rbx), %dx
	movw	%dx, 28(%rax)
	.p2align 4,,10
	.p2align 3
.L364:
	movq	8(%r15), %r13
.L360:
	leaq	0(%r13,%r12,2), %rax
	movq	%rax, 8(%r15)
.L357:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L481
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L361:
	.cfi_restore_state
	movsbw	(%rbx), %dx
	addq	$2, %r13
	addq	$1, %rbx
	movw	%dx, -2(%r13)
	cmpq	%rax, %r13
	jne	.L361
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L359:
	movabsq	$4611686018427387903, %r14
	movq	(%r15), %r9
	movq	%r13, %rdx
	movq	%r14, %rsi
	subq	%r9, %rdx
	movq	%rdx, %rax
	sarq	%rax
	subq	%rax, %rsi
	cmpq	%rsi, %r12
	ja	.L482
	cmpq	%rax, %r12
	movq	%rax, %rsi
	cmovnb	%r12, %rsi
	addq	%rsi, %rax
	movq	%rax, %r10
	jc	.L379
	testq	%rax, %rax
	jne	.L483
	movq	$0, -88(%rbp)
	movq	%r13, %r11
	xorl	%r14d, %r14d
.L369:
	cmpq	%r9, %r13
	je	.L370
	movq	-88(%rbp), %rdi
	movq	%r9, %rsi
	movq	%rcx, -128(%rbp)
	movq	%r11, -120(%rbp)
	movq	%r10, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%r9, -96(%rbp)
	call	memmove@PLT
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %r11
	movq	-112(%rbp), %r10
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %r9
.L370:
	movq	-88(%rbp), %rax
	leaq	(%rax,%rdx), %rdi
	testq	%r12, %r12
	jle	.L371
	cmpq	$15, %r12
	jle	.L381
	movq	%r12, %rdx
	xorl	%eax, %eax
	pxor	%xmm3, %xmm3
	andq	$-16, %rdx
	.p2align 4,,10
	.p2align 3
.L373:
	movdqa	(%rbx,%rax), %xmm0
	movdqa	%xmm3, %xmm1
	pcmpgtb	%xmm0, %xmm1
	movdqa	%xmm0, %xmm2
	punpcklbw	%xmm1, %xmm2
	punpckhbw	%xmm1, %xmm0
	movups	%xmm2, (%rdi,%rax,2)
	movups	%xmm0, 16(%rdi,%rax,2)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L373
	movq	%r12, %rdx
	movq	%r12, %rcx
	andq	$-16, %rdx
	andl	$15, %ecx
	addq	%rdx, %rbx
	leaq	(%rdi,%rdx,2), %rax
	cmpq	%rdx, %r12
	je	.L374
.L372:
	movsbw	(%rbx), %dx
	movw	%dx, (%rax)
	cmpq	$1, %rcx
	je	.L374
	movsbw	1(%rbx), %dx
	movw	%dx, 2(%rax)
	cmpq	$2, %rcx
	je	.L374
	movsbw	2(%rbx), %dx
	movw	%dx, 4(%rax)
	cmpq	$3, %rcx
	je	.L374
	movsbw	3(%rbx), %dx
	movw	%dx, 6(%rax)
	cmpq	$4, %rcx
	je	.L374
	movsbw	4(%rbx), %dx
	movw	%dx, 8(%rax)
	cmpq	$5, %rcx
	je	.L374
	movsbw	5(%rbx), %dx
	movw	%dx, 10(%rax)
	cmpq	$6, %rcx
	je	.L374
	movsbw	6(%rbx), %dx
	movw	%dx, 12(%rax)
	cmpq	$7, %rcx
	je	.L374
	movsbw	7(%rbx), %dx
	movw	%dx, 14(%rax)
	cmpq	$8, %rcx
	je	.L374
	movsbw	8(%rbx), %dx
	movw	%dx, 16(%rax)
	cmpq	$9, %rcx
	je	.L374
	movsbw	9(%rbx), %dx
	movw	%dx, 18(%rax)
	cmpq	$10, %rcx
	je	.L374
	movsbw	10(%rbx), %dx
	movw	%dx, 20(%rax)
	cmpq	$11, %rcx
	je	.L374
	movsbw	11(%rbx), %dx
	movw	%dx, 22(%rax)
	cmpq	$12, %rcx
	je	.L374
	movsbw	12(%rbx), %dx
	movw	%dx, 24(%rax)
	cmpq	$13, %rcx
	je	.L374
	movsbw	13(%rbx), %dx
	movw	%dx, 26(%rax)
	cmpq	$14, %rcx
	je	.L374
	movsbw	14(%rbx), %dx
	movw	%dx, 28(%rax)
	.p2align 4,,10
	.p2align 3
.L374:
	leaq	(%rdi,%r12,2), %rdi
.L371:
	cmpq	%r11, %r13
	je	.L375
	movq	%r10, %rdx
	movq	%r13, %rsi
	movq	%r9, -104(%rbp)
	movq	%r10, -96(%rbp)
	call	memcpy@PLT
	movq	-104(%rbp), %r9
	movq	-96(%rbp), %r10
	movq	%rax, %rdi
.L375:
	leaq	(%rdi,%r10), %rbx
	testq	%r9, %r9
	je	.L376
	movq	%r9, %rdi
	call	_ZdlPv@PLT
.L376:
	movq	-88(%rbp), %xmm0
	movq	%rbx, %xmm4
	movq	%r14, 16(%r15)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, (%r15)
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L379:
	movabsq	$9223372036854775806, %r14
.L368:
	movq	%r14, %rdi
	movq	%rcx, -96(%rbp)
	call	_Znwm@PLT
	movq	8(%r15), %r11
	movq	(%r15), %r9
	movq	%r13, %rdx
	movq	%rax, -88(%rbp)
	movq	-96(%rbp), %rcx
	addq	%rax, %r14
	movq	%r11, %r10
	subq	%r9, %rdx
	subq	%r13, %r10
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L483:
	cmpq	%r14, %rax
	cmova	%r14, %rax
	leaq	(%rax,%rax), %r14
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L381:
	movq	%rdi, %rax
	jmp	.L372
.L481:
	call	__stack_chk_fail@PLT
.L482:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4050:
	.size	_ZN12v8_inspector15String16Builder12appendNumberEi, .-_ZN12v8_inspector15String16Builder12appendNumberEi
	.section	.rodata._ZN12v8_inspector15String16Builder12appendNumberEm.str1.1,"aMS",@progbits,1
.LC5:
	.string	"%zu"
	.section	.text._ZN12v8_inspector15String16Builder12appendNumberEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15String16Builder12appendNumberEm
	.type	_ZN12v8_inspector15String16Builder12appendNumberEm, @function
_ZN12v8_inspector15String16Builder12appendNumberEm:
.LFB4051:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	leaq	.LC5(%rip), %rdx
	movl	$20, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base2OS8SNPrintFEPciPKcz@PLT
	movslq	%eax, %r12
	testq	%r12, %r12
	je	.L484
	movq	8(%r15), %r13
	movq	16(%r15), %rax
	movq	%r12, %rcx
	subq	%r13, %rax
	sarq	%rax
	cmpq	%rax, %r12
	ja	.L486
	testq	%r12, %r12
	jle	.L487
	leaq	(%rbx,%r12), %rdx
	leaq	0(%r13,%r12,2), %rax
	cmpq	%rdx, %r13
	setnb	%cl
	cmpq	%rbx, %rax
	setbe	%dl
	orb	%dl, %cl
	je	.L488
	leaq	-1(%r12), %rdx
	cmpq	$14, %rdx
	jbe	.L488
	movq	%r12, %rdx
	xorl	%eax, %eax
	pxor	%xmm3, %xmm3
	andq	$-16, %rdx
.L489:
	movdqa	(%rbx,%rax), %xmm0
	movdqa	%xmm3, %xmm1
	pcmpgtb	%xmm0, %xmm1
	movdqa	%xmm0, %xmm2
	punpcklbw	%xmm1, %xmm2
	punpckhbw	%xmm1, %xmm0
	movups	%xmm2, 0(%r13,%rax,2)
	movups	%xmm0, 16(%r13,%rax,2)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L489
	movq	%r12, %rcx
	movq	%r12, %rdx
	andq	$-16, %rcx
	andl	$15, %edx
	addq	%rcx, %rbx
	leaq	0(%r13,%rcx,2), %rax
	cmpq	%rcx, %r12
	je	.L491
	movsbw	(%rbx), %cx
	movw	%cx, (%rax)
	cmpq	$1, %rdx
	je	.L491
	movsbw	1(%rbx), %cx
	movw	%cx, 2(%rax)
	cmpq	$2, %rdx
	je	.L491
	movsbw	2(%rbx), %cx
	movw	%cx, 4(%rax)
	cmpq	$3, %rdx
	je	.L491
	movsbw	3(%rbx), %cx
	movw	%cx, 6(%rax)
	cmpq	$4, %rdx
	je	.L491
	movsbw	4(%rbx), %cx
	movw	%cx, 8(%rax)
	cmpq	$5, %rdx
	je	.L491
	movsbw	5(%rbx), %cx
	movw	%cx, 10(%rax)
	cmpq	$6, %rdx
	je	.L491
	movsbw	6(%rbx), %cx
	movw	%cx, 12(%rax)
	cmpq	$7, %rdx
	je	.L491
	movsbw	7(%rbx), %cx
	movw	%cx, 14(%rax)
	cmpq	$8, %rdx
	je	.L491
	movsbw	8(%rbx), %cx
	movw	%cx, 16(%rax)
	cmpq	$9, %rdx
	je	.L491
	movsbw	9(%rbx), %cx
	movw	%cx, 18(%rax)
	cmpq	$10, %rdx
	je	.L491
	movsbw	10(%rbx), %cx
	movw	%cx, 20(%rax)
	cmpq	$11, %rdx
	je	.L491
	movsbw	11(%rbx), %cx
	movw	%cx, 22(%rax)
	cmpq	$12, %rdx
	je	.L491
	movsbw	12(%rbx), %cx
	movw	%cx, 24(%rax)
	cmpq	$13, %rdx
	je	.L491
	movsbw	13(%rbx), %cx
	movw	%cx, 26(%rax)
	cmpq	$14, %rdx
	je	.L491
	movsbw	14(%rbx), %dx
	movw	%dx, 28(%rax)
	.p2align 4,,10
	.p2align 3
.L491:
	movq	8(%r15), %r13
.L487:
	leaq	0(%r13,%r12,2), %rax
	movq	%rax, 8(%r15)
.L484:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L608
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L488:
	.cfi_restore_state
	movsbw	(%rbx), %dx
	addq	$2, %r13
	addq	$1, %rbx
	movw	%dx, -2(%r13)
	cmpq	%rax, %r13
	jne	.L488
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L486:
	movabsq	$4611686018427387903, %r14
	movq	(%r15), %r9
	movq	%r13, %rdx
	movq	%r14, %rsi
	subq	%r9, %rdx
	movq	%rdx, %rax
	sarq	%rax
	subq	%rax, %rsi
	cmpq	%rsi, %r12
	ja	.L609
	cmpq	%rax, %r12
	movq	%rax, %rsi
	cmovnb	%r12, %rsi
	addq	%rsi, %rax
	movq	%rax, %r10
	jc	.L506
	testq	%rax, %rax
	jne	.L610
	movq	$0, -88(%rbp)
	movq	%r13, %r11
	xorl	%r14d, %r14d
.L496:
	cmpq	%r9, %r13
	je	.L497
	movq	-88(%rbp), %rdi
	movq	%r9, %rsi
	movq	%rcx, -128(%rbp)
	movq	%r11, -120(%rbp)
	movq	%r10, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%r9, -96(%rbp)
	call	memmove@PLT
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %r11
	movq	-112(%rbp), %r10
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %r9
.L497:
	movq	-88(%rbp), %rax
	leaq	(%rax,%rdx), %rdi
	testq	%r12, %r12
	jle	.L498
	cmpq	$15, %r12
	jle	.L508
	movq	%r12, %rdx
	xorl	%eax, %eax
	pxor	%xmm3, %xmm3
	andq	$-16, %rdx
	.p2align 4,,10
	.p2align 3
.L500:
	movdqa	(%rbx,%rax), %xmm0
	movdqa	%xmm3, %xmm1
	pcmpgtb	%xmm0, %xmm1
	movdqa	%xmm0, %xmm2
	punpcklbw	%xmm1, %xmm2
	punpckhbw	%xmm1, %xmm0
	movups	%xmm2, (%rdi,%rax,2)
	movups	%xmm0, 16(%rdi,%rax,2)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L500
	movq	%r12, %rdx
	movq	%r12, %rcx
	andq	$-16, %rdx
	andl	$15, %ecx
	addq	%rdx, %rbx
	leaq	(%rdi,%rdx,2), %rax
	cmpq	%rdx, %r12
	je	.L501
.L499:
	movsbw	(%rbx), %dx
	movw	%dx, (%rax)
	cmpq	$1, %rcx
	je	.L501
	movsbw	1(%rbx), %dx
	movw	%dx, 2(%rax)
	cmpq	$2, %rcx
	je	.L501
	movsbw	2(%rbx), %dx
	movw	%dx, 4(%rax)
	cmpq	$3, %rcx
	je	.L501
	movsbw	3(%rbx), %dx
	movw	%dx, 6(%rax)
	cmpq	$4, %rcx
	je	.L501
	movsbw	4(%rbx), %dx
	movw	%dx, 8(%rax)
	cmpq	$5, %rcx
	je	.L501
	movsbw	5(%rbx), %dx
	movw	%dx, 10(%rax)
	cmpq	$6, %rcx
	je	.L501
	movsbw	6(%rbx), %dx
	movw	%dx, 12(%rax)
	cmpq	$7, %rcx
	je	.L501
	movsbw	7(%rbx), %dx
	movw	%dx, 14(%rax)
	cmpq	$8, %rcx
	je	.L501
	movsbw	8(%rbx), %dx
	movw	%dx, 16(%rax)
	cmpq	$9, %rcx
	je	.L501
	movsbw	9(%rbx), %dx
	movw	%dx, 18(%rax)
	cmpq	$10, %rcx
	je	.L501
	movsbw	10(%rbx), %dx
	movw	%dx, 20(%rax)
	cmpq	$11, %rcx
	je	.L501
	movsbw	11(%rbx), %dx
	movw	%dx, 22(%rax)
	cmpq	$12, %rcx
	je	.L501
	movsbw	12(%rbx), %dx
	movw	%dx, 24(%rax)
	cmpq	$13, %rcx
	je	.L501
	movsbw	13(%rbx), %dx
	movw	%dx, 26(%rax)
	cmpq	$14, %rcx
	je	.L501
	movsbw	14(%rbx), %dx
	movw	%dx, 28(%rax)
	.p2align 4,,10
	.p2align 3
.L501:
	leaq	(%rdi,%r12,2), %rdi
.L498:
	cmpq	%r11, %r13
	je	.L502
	movq	%r10, %rdx
	movq	%r13, %rsi
	movq	%r9, -104(%rbp)
	movq	%r10, -96(%rbp)
	call	memcpy@PLT
	movq	-104(%rbp), %r9
	movq	-96(%rbp), %r10
	movq	%rax, %rdi
.L502:
	leaq	(%rdi,%r10), %rbx
	testq	%r9, %r9
	je	.L503
	movq	%r9, %rdi
	call	_ZdlPv@PLT
.L503:
	movq	-88(%rbp), %xmm0
	movq	%rbx, %xmm4
	movq	%r14, 16(%r15)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, (%r15)
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L506:
	movabsq	$9223372036854775806, %r14
.L495:
	movq	%r14, %rdi
	movq	%rcx, -96(%rbp)
	call	_Znwm@PLT
	movq	8(%r15), %r11
	movq	(%r15), %r9
	movq	%r13, %rdx
	movq	%rax, -88(%rbp)
	movq	-96(%rbp), %rcx
	addq	%rax, %r14
	movq	%r11, %r10
	subq	%r9, %rdx
	subq	%r13, %r10
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L610:
	cmpq	%r14, %rax
	cmova	%r14, %rax
	leaq	(%rax,%rax), %r14
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L508:
	movq	%rdi, %rax
	jmp	.L499
.L608:
	call	__stack_chk_fail@PLT
.L609:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4051:
	.size	_ZN12v8_inspector15String16Builder12appendNumberEm, .-_ZN12v8_inspector15String16Builder12appendNumberEm
	.section	.rodata._ZN12v8_inspector15String16Builder19appendUnsignedAsHexEm.str1.1,"aMS",@progbits,1
.LC6:
	.string	"%016lx"
	.section	.text._ZN12v8_inspector15String16Builder19appendUnsignedAsHexEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15String16Builder19appendUnsignedAsHexEm
	.type	_ZN12v8_inspector15String16Builder19appendUnsignedAsHexEm, @function
_ZN12v8_inspector15String16Builder19appendUnsignedAsHexEm:
.LFB4052:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	leaq	.LC6(%rip), %rdx
	movl	$17, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base2OS8SNPrintFEPciPKcz@PLT
	movslq	%eax, %r12
	testq	%r12, %r12
	je	.L611
	movq	8(%r15), %r13
	movq	16(%r15), %rax
	movq	%r12, %rcx
	subq	%r13, %rax
	sarq	%rax
	cmpq	%rax, %r12
	ja	.L613
	testq	%r12, %r12
	jle	.L614
	leaq	(%rbx,%r12), %rdx
	leaq	0(%r13,%r12,2), %rax
	cmpq	%rdx, %r13
	setnb	%cl
	cmpq	%rbx, %rax
	setbe	%dl
	orb	%dl, %cl
	je	.L615
	leaq	-1(%r12), %rdx
	cmpq	$14, %rdx
	jbe	.L615
	movq	%r12, %rdx
	xorl	%eax, %eax
	pxor	%xmm3, %xmm3
	andq	$-16, %rdx
.L616:
	movdqa	(%rbx,%rax), %xmm0
	movdqa	%xmm3, %xmm1
	pcmpgtb	%xmm0, %xmm1
	movdqa	%xmm0, %xmm2
	punpcklbw	%xmm1, %xmm2
	punpckhbw	%xmm1, %xmm0
	movups	%xmm2, 0(%r13,%rax,2)
	movups	%xmm0, 16(%r13,%rax,2)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L616
	movq	%r12, %rcx
	movq	%r12, %rdx
	andq	$-16, %rcx
	andl	$15, %edx
	addq	%rcx, %rbx
	leaq	0(%r13,%rcx,2), %rax
	cmpq	%rcx, %r12
	je	.L618
	movsbw	(%rbx), %cx
	movw	%cx, (%rax)
	cmpq	$1, %rdx
	je	.L618
	movsbw	1(%rbx), %cx
	movw	%cx, 2(%rax)
	cmpq	$2, %rdx
	je	.L618
	movsbw	2(%rbx), %cx
	movw	%cx, 4(%rax)
	cmpq	$3, %rdx
	je	.L618
	movsbw	3(%rbx), %cx
	movw	%cx, 6(%rax)
	cmpq	$4, %rdx
	je	.L618
	movsbw	4(%rbx), %cx
	movw	%cx, 8(%rax)
	cmpq	$5, %rdx
	je	.L618
	movsbw	5(%rbx), %cx
	movw	%cx, 10(%rax)
	cmpq	$6, %rdx
	je	.L618
	movsbw	6(%rbx), %cx
	movw	%cx, 12(%rax)
	cmpq	$7, %rdx
	je	.L618
	movsbw	7(%rbx), %cx
	movw	%cx, 14(%rax)
	cmpq	$8, %rdx
	je	.L618
	movsbw	8(%rbx), %cx
	movw	%cx, 16(%rax)
	cmpq	$9, %rdx
	je	.L618
	movsbw	9(%rbx), %cx
	movw	%cx, 18(%rax)
	cmpq	$10, %rdx
	je	.L618
	movsbw	10(%rbx), %cx
	movw	%cx, 20(%rax)
	cmpq	$11, %rdx
	je	.L618
	movsbw	11(%rbx), %cx
	movw	%cx, 22(%rax)
	cmpq	$12, %rdx
	je	.L618
	movsbw	12(%rbx), %cx
	movw	%cx, 24(%rax)
	cmpq	$13, %rdx
	je	.L618
	movsbw	13(%rbx), %cx
	movw	%cx, 26(%rax)
	cmpq	$14, %rdx
	je	.L618
	movsbw	14(%rbx), %dx
	movw	%dx, 28(%rax)
	.p2align 4,,10
	.p2align 3
.L618:
	movq	8(%r15), %r13
.L614:
	leaq	0(%r13,%r12,2), %rax
	movq	%rax, 8(%r15)
.L611:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L735
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L615:
	.cfi_restore_state
	movsbw	(%rbx), %dx
	addq	$2, %r13
	addq	$1, %rbx
	movw	%dx, -2(%r13)
	cmpq	%rax, %r13
	jne	.L615
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L613:
	movabsq	$4611686018427387903, %r14
	movq	(%r15), %r9
	movq	%r13, %rdx
	movq	%r14, %rsi
	subq	%r9, %rdx
	movq	%rdx, %rax
	sarq	%rax
	subq	%rax, %rsi
	cmpq	%rsi, %r12
	ja	.L736
	cmpq	%rax, %r12
	movq	%rax, %rsi
	cmovnb	%r12, %rsi
	addq	%rsi, %rax
	movq	%rax, %r10
	jc	.L633
	testq	%rax, %rax
	jne	.L737
	movq	$0, -88(%rbp)
	movq	%r13, %r11
	xorl	%r14d, %r14d
.L623:
	cmpq	%r9, %r13
	je	.L624
	movq	-88(%rbp), %rdi
	movq	%r9, %rsi
	movq	%rcx, -128(%rbp)
	movq	%r11, -120(%rbp)
	movq	%r10, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%r9, -96(%rbp)
	call	memmove@PLT
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %r11
	movq	-112(%rbp), %r10
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %r9
.L624:
	movq	-88(%rbp), %rax
	leaq	(%rax,%rdx), %rdi
	testq	%r12, %r12
	jle	.L625
	cmpq	$15, %r12
	jle	.L635
	movq	%r12, %rdx
	xorl	%eax, %eax
	pxor	%xmm3, %xmm3
	andq	$-16, %rdx
	.p2align 4,,10
	.p2align 3
.L627:
	movdqa	(%rbx,%rax), %xmm0
	movdqa	%xmm3, %xmm1
	pcmpgtb	%xmm0, %xmm1
	movdqa	%xmm0, %xmm2
	punpcklbw	%xmm1, %xmm2
	punpckhbw	%xmm1, %xmm0
	movups	%xmm2, (%rdi,%rax,2)
	movups	%xmm0, 16(%rdi,%rax,2)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L627
	movq	%r12, %rdx
	movq	%r12, %rcx
	andq	$-16, %rdx
	andl	$15, %ecx
	addq	%rdx, %rbx
	leaq	(%rdi,%rdx,2), %rax
	cmpq	%rdx, %r12
	je	.L628
.L626:
	movsbw	(%rbx), %dx
	movw	%dx, (%rax)
	cmpq	$1, %rcx
	je	.L628
	movsbw	1(%rbx), %dx
	movw	%dx, 2(%rax)
	cmpq	$2, %rcx
	je	.L628
	movsbw	2(%rbx), %dx
	movw	%dx, 4(%rax)
	cmpq	$3, %rcx
	je	.L628
	movsbw	3(%rbx), %dx
	movw	%dx, 6(%rax)
	cmpq	$4, %rcx
	je	.L628
	movsbw	4(%rbx), %dx
	movw	%dx, 8(%rax)
	cmpq	$5, %rcx
	je	.L628
	movsbw	5(%rbx), %dx
	movw	%dx, 10(%rax)
	cmpq	$6, %rcx
	je	.L628
	movsbw	6(%rbx), %dx
	movw	%dx, 12(%rax)
	cmpq	$7, %rcx
	je	.L628
	movsbw	7(%rbx), %dx
	movw	%dx, 14(%rax)
	cmpq	$8, %rcx
	je	.L628
	movsbw	8(%rbx), %dx
	movw	%dx, 16(%rax)
	cmpq	$9, %rcx
	je	.L628
	movsbw	9(%rbx), %dx
	movw	%dx, 18(%rax)
	cmpq	$10, %rcx
	je	.L628
	movsbw	10(%rbx), %dx
	movw	%dx, 20(%rax)
	cmpq	$11, %rcx
	je	.L628
	movsbw	11(%rbx), %dx
	movw	%dx, 22(%rax)
	cmpq	$12, %rcx
	je	.L628
	movsbw	12(%rbx), %dx
	movw	%dx, 24(%rax)
	cmpq	$13, %rcx
	je	.L628
	movsbw	13(%rbx), %dx
	movw	%dx, 26(%rax)
	cmpq	$14, %rcx
	je	.L628
	movsbw	14(%rbx), %dx
	movw	%dx, 28(%rax)
	.p2align 4,,10
	.p2align 3
.L628:
	leaq	(%rdi,%r12,2), %rdi
.L625:
	cmpq	%r11, %r13
	je	.L629
	movq	%r10, %rdx
	movq	%r13, %rsi
	movq	%r9, -104(%rbp)
	movq	%r10, -96(%rbp)
	call	memcpy@PLT
	movq	-104(%rbp), %r9
	movq	-96(%rbp), %r10
	movq	%rax, %rdi
.L629:
	leaq	(%rdi,%r10), %rbx
	testq	%r9, %r9
	je	.L630
	movq	%r9, %rdi
	call	_ZdlPv@PLT
.L630:
	movq	-88(%rbp), %xmm0
	movq	%rbx, %xmm4
	movq	%r14, 16(%r15)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, (%r15)
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L633:
	movabsq	$9223372036854775806, %r14
.L622:
	movq	%r14, %rdi
	movq	%rcx, -96(%rbp)
	call	_Znwm@PLT
	movq	8(%r15), %r11
	movq	(%r15), %r9
	movq	%r13, %rdx
	movq	%rax, -88(%rbp)
	movq	-96(%rbp), %rcx
	addq	%rax, %r14
	movq	%r11, %r10
	subq	%r9, %rdx
	subq	%r13, %r10
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L737:
	cmpq	%r14, %rax
	cmova	%r14, %rax
	leaq	(%rax,%rax), %r14
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L635:
	movq	%rdi, %rax
	jmp	.L626
.L735:
	call	__stack_chk_fail@PLT
.L736:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4052:
	.size	_ZN12v8_inspector15String16Builder19appendUnsignedAsHexEm, .-_ZN12v8_inspector15String16Builder19appendUnsignedAsHexEm
	.section	.rodata._ZN12v8_inspector15String16Builder19appendUnsignedAsHexEj.str1.1,"aMS",@progbits,1
.LC7:
	.string	"%08x"
	.section	.text._ZN12v8_inspector15String16Builder19appendUnsignedAsHexEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15String16Builder19appendUnsignedAsHexEj
	.type	_ZN12v8_inspector15String16Builder19appendUnsignedAsHexEj, @function
_ZN12v8_inspector15String16Builder19appendUnsignedAsHexEj:
.LFB4053:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	leaq	.LC7(%rip), %rdx
	movl	$9, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base2OS8SNPrintFEPciPKcz@PLT
	movslq	%eax, %r12
	testq	%r12, %r12
	je	.L738
	movq	8(%r15), %r13
	movq	16(%r15), %rax
	movq	%r12, %rcx
	subq	%r13, %rax
	sarq	%rax
	cmpq	%rax, %r12
	ja	.L740
	testq	%r12, %r12
	jle	.L741
	leaq	(%rbx,%r12), %rdx
	leaq	0(%r13,%r12,2), %rax
	cmpq	%rdx, %r13
	setnb	%cl
	cmpq	%rbx, %rax
	setbe	%dl
	orb	%dl, %cl
	je	.L742
	leaq	-1(%r12), %rdx
	cmpq	$14, %rdx
	jbe	.L742
	movq	%r12, %rdx
	xorl	%eax, %eax
	pxor	%xmm3, %xmm3
	andq	$-16, %rdx
.L743:
	movdqa	(%rbx,%rax), %xmm0
	movdqa	%xmm3, %xmm1
	pcmpgtb	%xmm0, %xmm1
	movdqa	%xmm0, %xmm2
	punpcklbw	%xmm1, %xmm2
	punpckhbw	%xmm1, %xmm0
	movups	%xmm2, 0(%r13,%rax,2)
	movups	%xmm0, 16(%r13,%rax,2)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L743
	movq	%r12, %rcx
	movq	%r12, %rdx
	andq	$-16, %rcx
	andl	$15, %edx
	addq	%rcx, %rbx
	leaq	0(%r13,%rcx,2), %rax
	cmpq	%rcx, %r12
	je	.L745
	movsbw	(%rbx), %cx
	movw	%cx, (%rax)
	cmpq	$1, %rdx
	je	.L745
	movsbw	1(%rbx), %cx
	movw	%cx, 2(%rax)
	cmpq	$2, %rdx
	je	.L745
	movsbw	2(%rbx), %cx
	movw	%cx, 4(%rax)
	cmpq	$3, %rdx
	je	.L745
	movsbw	3(%rbx), %cx
	movw	%cx, 6(%rax)
	cmpq	$4, %rdx
	je	.L745
	movsbw	4(%rbx), %cx
	movw	%cx, 8(%rax)
	cmpq	$5, %rdx
	je	.L745
	movsbw	5(%rbx), %cx
	movw	%cx, 10(%rax)
	cmpq	$6, %rdx
	je	.L745
	movsbw	6(%rbx), %cx
	movw	%cx, 12(%rax)
	cmpq	$7, %rdx
	je	.L745
	movsbw	7(%rbx), %cx
	movw	%cx, 14(%rax)
	cmpq	$8, %rdx
	je	.L745
	movsbw	8(%rbx), %cx
	movw	%cx, 16(%rax)
	cmpq	$9, %rdx
	je	.L745
	movsbw	9(%rbx), %cx
	movw	%cx, 18(%rax)
	cmpq	$10, %rdx
	je	.L745
	movsbw	10(%rbx), %cx
	movw	%cx, 20(%rax)
	cmpq	$11, %rdx
	je	.L745
	movsbw	11(%rbx), %cx
	movw	%cx, 22(%rax)
	cmpq	$12, %rdx
	je	.L745
	movsbw	12(%rbx), %cx
	movw	%cx, 24(%rax)
	cmpq	$13, %rdx
	je	.L745
	movsbw	13(%rbx), %cx
	movw	%cx, 26(%rax)
	cmpq	$14, %rdx
	je	.L745
	movsbw	14(%rbx), %dx
	movw	%dx, 28(%rax)
	.p2align 4,,10
	.p2align 3
.L745:
	movq	8(%r15), %r13
.L741:
	leaq	0(%r13,%r12,2), %rax
	movq	%rax, 8(%r15)
.L738:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L862
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L742:
	.cfi_restore_state
	movsbw	(%rbx), %dx
	addq	$2, %r13
	addq	$1, %rbx
	movw	%dx, -2(%r13)
	cmpq	%rax, %r13
	jne	.L742
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L740:
	movabsq	$4611686018427387903, %r14
	movq	(%r15), %r9
	movq	%r13, %rdx
	movq	%r14, %rsi
	subq	%r9, %rdx
	movq	%rdx, %rax
	sarq	%rax
	subq	%rax, %rsi
	cmpq	%rsi, %r12
	ja	.L863
	cmpq	%rax, %r12
	movq	%rax, %rsi
	cmovnb	%r12, %rsi
	addq	%rsi, %rax
	movq	%rax, %r10
	jc	.L760
	testq	%rax, %rax
	jne	.L864
	movq	$0, -88(%rbp)
	movq	%r13, %r11
	xorl	%r14d, %r14d
.L750:
	cmpq	%r9, %r13
	je	.L751
	movq	-88(%rbp), %rdi
	movq	%r9, %rsi
	movq	%rcx, -128(%rbp)
	movq	%r11, -120(%rbp)
	movq	%r10, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%r9, -96(%rbp)
	call	memmove@PLT
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %r11
	movq	-112(%rbp), %r10
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %r9
.L751:
	movq	-88(%rbp), %rax
	leaq	(%rax,%rdx), %rdi
	testq	%r12, %r12
	jle	.L752
	cmpq	$15, %r12
	jle	.L762
	movq	%r12, %rdx
	xorl	%eax, %eax
	pxor	%xmm3, %xmm3
	andq	$-16, %rdx
	.p2align 4,,10
	.p2align 3
.L754:
	movdqa	(%rbx,%rax), %xmm0
	movdqa	%xmm3, %xmm1
	pcmpgtb	%xmm0, %xmm1
	movdqa	%xmm0, %xmm2
	punpcklbw	%xmm1, %xmm2
	punpckhbw	%xmm1, %xmm0
	movups	%xmm2, (%rdi,%rax,2)
	movups	%xmm0, 16(%rdi,%rax,2)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L754
	movq	%r12, %rdx
	movq	%r12, %rcx
	andq	$-16, %rdx
	andl	$15, %ecx
	addq	%rdx, %rbx
	leaq	(%rdi,%rdx,2), %rax
	cmpq	%rdx, %r12
	je	.L755
.L753:
	movsbw	(%rbx), %dx
	movw	%dx, (%rax)
	cmpq	$1, %rcx
	je	.L755
	movsbw	1(%rbx), %dx
	movw	%dx, 2(%rax)
	cmpq	$2, %rcx
	je	.L755
	movsbw	2(%rbx), %dx
	movw	%dx, 4(%rax)
	cmpq	$3, %rcx
	je	.L755
	movsbw	3(%rbx), %dx
	movw	%dx, 6(%rax)
	cmpq	$4, %rcx
	je	.L755
	movsbw	4(%rbx), %dx
	movw	%dx, 8(%rax)
	cmpq	$5, %rcx
	je	.L755
	movsbw	5(%rbx), %dx
	movw	%dx, 10(%rax)
	cmpq	$6, %rcx
	je	.L755
	movsbw	6(%rbx), %dx
	movw	%dx, 12(%rax)
	cmpq	$7, %rcx
	je	.L755
	movsbw	7(%rbx), %dx
	movw	%dx, 14(%rax)
	cmpq	$8, %rcx
	je	.L755
	movsbw	8(%rbx), %dx
	movw	%dx, 16(%rax)
	cmpq	$9, %rcx
	je	.L755
	movsbw	9(%rbx), %dx
	movw	%dx, 18(%rax)
	cmpq	$10, %rcx
	je	.L755
	movsbw	10(%rbx), %dx
	movw	%dx, 20(%rax)
	cmpq	$11, %rcx
	je	.L755
	movsbw	11(%rbx), %dx
	movw	%dx, 22(%rax)
	cmpq	$12, %rcx
	je	.L755
	movsbw	12(%rbx), %dx
	movw	%dx, 24(%rax)
	cmpq	$13, %rcx
	je	.L755
	movsbw	13(%rbx), %dx
	movw	%dx, 26(%rax)
	cmpq	$14, %rcx
	je	.L755
	movsbw	14(%rbx), %dx
	movw	%dx, 28(%rax)
	.p2align 4,,10
	.p2align 3
.L755:
	leaq	(%rdi,%r12,2), %rdi
.L752:
	cmpq	%r11, %r13
	je	.L756
	movq	%r10, %rdx
	movq	%r13, %rsi
	movq	%r9, -104(%rbp)
	movq	%r10, -96(%rbp)
	call	memcpy@PLT
	movq	-104(%rbp), %r9
	movq	-96(%rbp), %r10
	movq	%rax, %rdi
.L756:
	leaq	(%rdi,%r10), %rbx
	testq	%r9, %r9
	je	.L757
	movq	%r9, %rdi
	call	_ZdlPv@PLT
.L757:
	movq	-88(%rbp), %xmm0
	movq	%rbx, %xmm4
	movq	%r14, 16(%r15)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, (%r15)
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L760:
	movabsq	$9223372036854775806, %r14
.L749:
	movq	%r14, %rdi
	movq	%rcx, -96(%rbp)
	call	_Znwm@PLT
	movq	8(%r15), %r11
	movq	(%r15), %r9
	movq	%r13, %rdx
	movq	%rax, -88(%rbp)
	movq	-96(%rbp), %rcx
	addq	%rax, %r14
	movq	%r11, %r10
	subq	%r9, %rdx
	subq	%r13, %r10
	jmp	.L750
	.p2align 4,,10
	.p2align 3
.L864:
	cmpq	%r14, %rax
	cmova	%r14, %rax
	leaq	(%rax,%rax), %r14
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L762:
	movq	%rdi, %rax
	jmp	.L753
.L862:
	call	__stack_chk_fail@PLT
.L863:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4053:
	.size	_ZN12v8_inspector15String16Builder19appendUnsignedAsHexEj, .-_ZN12v8_inspector15String16Builder19appendUnsignedAsHexEj
	.section	.text._ZN12v8_inspector15String16Builder8toStringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15String16Builder8toStringEv
	.type	_ZN12v8_inspector15String16Builder8toStringEv, @function
_ZN12v8_inspector15String16Builder8toStringEv:
.LFB4054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rsi), %rbx
	movq	(%rsi), %r13
	movq	%rdi, (%r12)
	testq	%rbx, %rbx
	je	.L866
	testq	%r13, %r13
	je	.L882
.L866:
	subq	%r13, %rbx
	movq	%rbx, %r14
	sarq	%r14
	cmpq	$15, %rbx
	ja	.L883
.L868:
	cmpq	$2, %rbx
	je	.L884
	testq	%rbx, %rbx
	je	.L871
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	movq	(%r12), %rdi
.L871:
	xorl	%eax, %eax
	movq	%r14, 8(%r12)
	movw	%ax, (%rdi,%rbx)
	movq	%r12, %rax
	popq	%rbx
	movq	$0, 32(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L884:
	.cfi_restore_state
	movzwl	0(%r13), %eax
	movw	%ax, (%rdi)
	movq	(%r12), %rdi
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L883:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r14
	ja	.L885
	leaq	2(%rbx), %rdi
	call	_Znwm@PLT
	movq	%r14, 16(%r12)
	movq	%rax, (%r12)
	movq	%rax, %rdi
	jmp	.L868
.L882:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L885:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4054:
	.size	_ZN12v8_inspector15String16Builder8toStringEv, .-_ZN12v8_inspector15String16Builder8toStringEv
	.section	.rodata._ZN12v8_inspector15String16Builder15reserveCapacityEm.str1.1,"aMS",@progbits,1
.LC8:
	.string	"vector::reserve"
	.section	.text._ZN12v8_inspector15String16Builder15reserveCapacityEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15String16Builder15reserveCapacityEm
	.type	_ZN12v8_inspector15String16Builder15reserveCapacityEm, @function
_ZN12v8_inspector15String16Builder15reserveCapacityEm:
.LFB4055:
	.cfi_startproc
	endbr64
	movabsq	$4611686018427387903, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	%rax, %rsi
	ja	.L898
	movq	(%rdi), %r12
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	subq	%r12, %rax
	sarq	%rax
	cmpq	%rax, %rsi
	ja	.L899
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L899:
	.cfi_restore_state
	movq	8(%rdi), %r13
	leaq	(%rsi,%rsi), %r14
	subq	%r12, %r13
	testq	%rsi, %rsi
	je	.L893
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	(%rbx), %r12
	movq	8(%rbx), %rdx
	movq	%rax, %r15
	subq	%r12, %rdx
.L889:
	testq	%rdx, %rdx
	jg	.L900
	testq	%r12, %r12
	jne	.L891
.L892:
	addq	%r15, %r13
	addq	%r15, %r14
	movq	%r15, (%rbx)
	movq	%r13, 8(%rbx)
	movq	%r14, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L900:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	memmove@PLT
.L891:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L893:
	movq	%r13, %rdx
	xorl	%r15d, %r15d
	jmp	.L889
.L898:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4055:
	.size	_ZN12v8_inspector15String16Builder15reserveCapacityEm, .-_ZN12v8_inspector15String16Builder15reserveCapacityEm
	.section	.text._ZN12v8_inspector8String168fromUTF8EPKcm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8String168fromUTF8EPKcm
	.type	_ZN12v8_inspector8String168fromUTF8EPKcm, @function
_ZN12v8_inspector8String168fromUTF8EPKcm:
.LFB4056:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdi
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector11UTF8ToUTF16B5cxx11EPKcm@PLT
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %r13
	leaq	16(%r12), %rdi
	movq	%rdi, (%r12)
	leaq	(%rax,%rax), %rbx
	movq	%r13, %rax
	addq	%rbx, %rax
	je	.L902
	testq	%r13, %r13
	je	.L919
.L902:
	movq	%rbx, %r14
	sarq	%r14
	cmpq	$14, %rbx
	ja	.L920
.L903:
	cmpq	$2, %rbx
	je	.L921
	testq	%rbx, %rbx
	je	.L906
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	movq	(%r12), %rdi
.L906:
	xorl	%eax, %eax
	movq	%r14, 8(%r12)
	movw	%ax, (%rdi,%rbx)
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	movq	$0, 32(%r12)
	cmpq	%rax, %rdi
	je	.L901
	call	_ZdlPv@PLT
.L901:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L922
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L921:
	.cfi_restore_state
	movzwl	0(%r13), %eax
	movw	%ax, (%rdi)
	movq	(%r12), %rdi
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L920:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r14
	ja	.L923
	leaq	2(%rbx), %rdi
	call	_Znwm@PLT
	movq	%r14, 16(%r12)
	movq	%rax, (%r12)
	movq	%rax, %rdi
	jmp	.L903
.L919:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L922:
	call	__stack_chk_fail@PLT
.L923:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4056:
	.size	_ZN12v8_inspector8String168fromUTF8EPKcm, .-_ZN12v8_inspector8String168fromUTF8EPKcm
	.section	.text._ZN12v8_inspector8String1611fromUTF16LEEPKtm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8String1611fromUTF16LEEPKtm
	.type	_ZN12v8_inspector8String1611fromUTF16LEEPKtm, @function
_ZN12v8_inspector8String1611fromUTF16LEEPKtm:
.LFB4057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	(%rdx,%rdx), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	addq	%r13, %rax
	movq	%rdi, (%r12)
	je	.L925
	testq	%rsi, %rsi
	je	.L940
.L925:
	movq	%r13, %r15
	sarq	%r15
	cmpq	$14, %r13
	ja	.L941
.L926:
	cmpq	$2, %r13
	je	.L942
	testq	%r13, %r13
	je	.L929
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memmove@PLT
	movq	(%r12), %rdi
.L929:
	xorl	%eax, %eax
	movq	%r15, 8(%r12)
	movw	%ax, (%rdi,%rbx,2)
	movq	%r12, %rax
	movq	$0, 32(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L942:
	.cfi_restore_state
	movzwl	(%r14), %eax
	movw	%ax, (%rdi)
	movq	(%r12), %rdi
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L941:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r15
	ja	.L943
	leaq	2(%r13), %rdi
	call	_Znwm@PLT
	movq	%r15, 16(%r12)
	movq	%rax, (%r12)
	movq	%rax, %rdi
	jmp	.L926
.L940:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L943:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4057:
	.size	_ZN12v8_inspector8String1611fromUTF16LEEPKtm, .-_ZN12v8_inspector8String1611fromUTF16LEEPKtm
	.section	.text._ZNK12v8_inspector8String164utf8B5cxx11Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8String164utf8B5cxx11Ev
	.type	_ZNK12v8_inspector8String164utf8B5cxx11Ev, @function
_ZNK12v8_inspector8String164utf8B5cxx11Ev:
.LFB4058:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	8(%rsi), %rdx
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector11UTF16ToUTF8B5cxx11EPKtm@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L947
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L947:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4058:
	.size	_ZNK12v8_inspector8String164utf8B5cxx11Ev, .-_ZNK12v8_inspector8String164utf8B5cxx11Ev
	.section	.text._ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	.type	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_, @function
_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_:
.LFB4762:
	.cfi_startproc
	endbr64
	movabsq	$9223372036854775807, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r15
	movq	%rsi, %rax
	subq	%r15, %rax
	cmpq	%rcx, %rax
	je	.L962
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L957
	leaq	(%rax,%rax), %r14
	cmpq	%r14, %rax
	jbe	.L963
.L959:
	movq	%rcx, %r14
.L950:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L956:
	movzbl	0(%r13), %eax
	subq	%r8, %rsi
	leaq	1(%rbx,%rdx), %r10
	movq	%rsi, %r13
	movb	%al, (%rbx,%rdx)
	leaq	(%r10,%rsi), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L964
	testq	%rsi, %rsi
	jg	.L952
	testq	%r15, %r15
	jne	.L955
.L953:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L964:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L952
.L955:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L963:
	testq	%r14, %r14
	js	.L959
	jne	.L950
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L952:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L953
	jmp	.L955
	.p2align 4,,10
	.p2align 3
.L957:
	movl	$1, %r14d
	jmp	.L950
.L962:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4762:
	.size	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_, .-_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	.section	.text._ZN12v8_inspector12_GLOBAL__N_119charactersToIntegerEPKtmPb,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_119charactersToIntegerEPKtmPb, @function
_ZN12v8_inspector12_GLOBAL__N_119charactersToIntegerEPKtmPb:
.LFB3987:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%rdx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	addq	$1, %rbx
	js	.L999
	movq	%rdi, %r15
	movq	%rsi, %r12
	jne	.L967
.L973:
	xorl	%ebx, %ebx
	leaq	-88(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L968:
	movzwl	(%r15,%rbx,2), %eax
	testl	$65408, %eax
	jne	.L1000
.L974:
	movb	%al, -88(%rbp)
	movq	-72(%rbp), %rsi
	cmpq	-64(%rbp), %rsi
	je	.L976
	addq	$1, %rbx
	movb	%al, (%rsi)
	addq	$1, -72(%rbp)
	cmpq	%rbx, %r12
	ja	.L968
	leaq	-88(%rbp), %r14
.L977:
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rbx
.L972:
	movb	$0, -88(%rbp)
	cmpq	%rbx, %r8
	je	.L979
	movb	$0, (%r8)
	addq	$1, -72(%rbp)
.L980:
	movq	-80(%rbp), %rdi
	movl	$10, %edx
	movq	%r14, %rsi
	call	strtoll@PLT
	movq	-104(%rbp), %rcx
	movq	%rax, %r12
	testq	%rcx, %rcx
	je	.L975
	movq	-88(%rbp), %rax
	cmpb	$0, (%rax)
	sete	(%rcx)
.L975:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L965
	call	_ZdlPv@PLT
.L965:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1001
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L976:
	.cfi_restore_state
	leaq	-80(%rbp), %rdi
	movq	%r13, %rdx
	addq	$1, %rbx
	movq	%r13, %r14
	call	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	cmpq	%rbx, %r12
	jbe	.L977
	movzwl	(%r15,%rbx,2), %eax
	testl	$65408, %eax
	je	.L974
	.p2align 4,,10
	.p2align 3
.L1000:
	movq	-104(%rbp), %rax
	xorl	%r12d, %r12d
	testq	%rax, %rax
	je	.L975
	movb	$0, (%rax)
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L967:
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	-80(%rbp), %r14
	movq	-72(%rbp), %rdx
	movq	%rax, %r8
	subq	%r14, %rdx
	testq	%rdx, %rdx
	jg	.L1002
	testq	%r14, %r14
	jne	.L970
.L971:
	movq	%r8, %xmm0
	addq	%r8, %rbx
	leaq	-88(%rbp), %r14
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	testq	%r12, %r12
	jne	.L973
	jmp	.L972
	.p2align 4,,10
	.p2align 3
.L1002:
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	memmove@PLT
	movq	%rax, %r8
.L970:
	movq	%r14, %rdi
	movq	%r8, -112(%rbp)
	call	_ZdlPv@PLT
	movq	-112(%rbp), %r8
	jmp	.L971
.L979:
	leaq	-80(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r8, %rsi
	call	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	jmp	.L980
.L1001:
	call	__stack_chk_fail@PLT
.L999:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE3987:
	.size	_ZN12v8_inspector12_GLOBAL__N_119charactersToIntegerEPKtmPb, .-_ZN12v8_inspector12_GLOBAL__N_119charactersToIntegerEPKtmPb
	.section	.text._ZNK12v8_inspector8String1611toInteger64EPb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8String1611toInteger64EPb
	.type	_ZNK12v8_inspector8String1611toInteger64EPb, @function
_ZNK12v8_inspector8String1611toInteger64EPb:
.LFB4024:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	8(%rdi), %rsi
	movq	(%rdi), %rdi
	jmp	_ZN12v8_inspector12_GLOBAL__N_119charactersToIntegerEPKtmPb
	.cfi_endproc
.LFE4024:
	.size	_ZNK12v8_inspector8String1611toInteger64EPb, .-_ZNK12v8_inspector8String1611toInteger64EPb
	.section	.text._ZNK12v8_inspector8String169toIntegerEPb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8String169toIntegerEPb
	.type	_ZNK12v8_inspector8String169toIntegerEPb, @function
_ZNK12v8_inspector8String169toIntegerEPb:
.LFB4025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	movq	%rbx, %rdx
	subq	$8, %rsp
	movq	8(%rdi), %rsi
	movq	(%rdi), %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_119charactersToIntegerEPKtmPb
	testq	%rbx, %rbx
	je	.L1005
	cmpb	$0, (%rbx)
	je	.L1005
	movl	$2147483648, %edx
	movl	$4294967295, %ecx
	addq	%rax, %rdx
	cmpq	%rcx, %rdx
	setbe	(%rbx)
.L1005:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4025:
	.size	_ZNK12v8_inspector8String169toIntegerEPb, .-_ZNK12v8_inspector8String169toIntegerEPb
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm:
.LFB4856:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %r9
	subq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	addq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %rax
	movq	%rsi, -72(%rbp)
	movq	%rax, %r13
	leaq	(%r9,%rax), %r15
	subq	%rsi, %r13
	cmpq	(%rdi), %r14
	je	.L1023
	movq	16(%rdi), %rax
.L1011:
	movabsq	$2305843009213693951, %rdx
	cmpq	%rdx, %r15
	ja	.L1044
	cmpq	%rax, %r15
	jbe	.L1013
	addq	%rax, %rax
	cmpq	%rax, %r15
	jnb	.L1013
	movabsq	$4611686018427387904, %rdi
	movq	%rdx, %r15
	cmpq	%rdx, %rax
	ja	.L1014
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L1013:
	leaq	2(%r15,%r15), %rdi
.L1014:
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	testq	%r12, %r12
	movq	(%rbx), %r11
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	movq	%rax, %r10
	je	.L1016
	cmpq	$1, %r12
	je	.L1045
	movq	%r12, %rdx
	addq	%rdx, %rdx
	je	.L1016
	movq	%r11, %rsi
	movq	%rax, %rdi
	movq	%r8, -80(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memmove@PLT
	movq	-80(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r11
	movq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L1016:
	testq	%rcx, %rcx
	je	.L1018
	testq	%r8, %r8
	je	.L1018
	leaq	(%r10,%r12,2), %rdi
	cmpq	$1, %r8
	je	.L1046
	movq	%r8, %rdx
	addq	%rdx, %rdx
	je	.L1018
	movq	%rcx, %rsi
	movq	%r8, -80(%rbp)
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memcpy@PLT
	movq	-80(%rbp), %r8
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %r11
.L1018:
	testq	%r13, %r13
	jne	.L1047
.L1020:
	cmpq	%r11, %r14
	je	.L1022
	movq	%r11, %rdi
	movq	%r10, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r10
.L1022:
	movq	%r15, 16(%rbx)
	movq	%r10, (%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1047:
	.cfi_restore_state
	movq	-72(%rbp), %rax
	addq	%r12, %r8
	leaq	(%r10,%r8,2), %rdi
	leaq	(%r11,%rax,2), %rsi
	cmpq	$1, %r13
	je	.L1048
	addq	%r13, %r13
	je	.L1020
	movq	%r13, %rdx
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memmove@PLT
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %r11
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1023:
	movl	$7, %eax
	jmp	.L1011
	.p2align 4,,10
	.p2align 3
.L1048:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1045:
	movzwl	(%r11), %eax
	movw	%ax, (%r10)
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L1046:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	testq	%r13, %r13
	je	.L1020
	jmp	.L1047
.L1044:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4856:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	.section	.rodata._ZN12v8_inspector8String16C2EPKcm.str1.1,"aMS",@progbits,1
.LC9:
	.string	"basic_string::_M_replace_aux"
	.section	.text._ZN12v8_inspector8String16C2EPKcm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8String16C2EPKcm
	.type	_ZN12v8_inspector8String16C2EPKcm, @function
_ZN12v8_inspector8String16C2EPKcm:
.LFB4007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$16, %rdi
	subq	$8, %rsp
	movq	%rdi, (%rbx)
	movq	$0, 8(%rbx)
	movw	%cx, 16(%rbx)
	movq	$0, 32(%rbx)
	testq	%rdx, %rdx
	jne	.L1061
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1061:
	.cfi_restore_state
	movabsq	$2305843009213693951, %rax
	movq	%rdx, %r12
	cmpq	%rax, %rdx
	ja	.L1062
	movq	%rsi, %r13
	cmpq	$7, %rdx
	ja	.L1063
	cmpq	$1, %rdx
	jne	.L1053
.L1054:
	xorl	%eax, %eax
	movq	%r12, 8(%rbx)
	movw	%ax, (%rdi,%r12,2)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1056:
	movsbw	0(%r13,%rax), %cx
	movq	(%rbx), %rdx
	movw	%cx, (%rdx,%rax,2)
	addq	$1, %rax
	cmpq	%rax, %r12
	jne	.L1056
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1063:
	.cfi_restore_state
	movq	%rdx, %r8
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	(%rbx), %rdi
.L1053:
	leaq	(%r12,%r12), %rdx
	xorl	%esi, %esi
	call	memset@PLT
	movq	(%rbx), %rdi
	jmp	.L1054
.L1062:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4007:
	.size	_ZN12v8_inspector8String16C2EPKcm, .-_ZN12v8_inspector8String16C2EPKcm
	.globl	_ZN12v8_inspector8String16C1EPKcm
	.set	_ZN12v8_inspector8String16C1EPKcm,_ZN12v8_inspector8String16C2EPKcm
	.section	.text._ZN12v8_inspector8String1611fromIntegerEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8String1611fromIntegerEi
	.type	_ZN12v8_inspector8String1611fromIntegerEi, @function
_ZN12v8_inspector8String1611fromIntegerEi:
.LFB4015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$50, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	%esi, %edi
	pushq	%r12
	leaq	-80(%rbp), %rsi
	subq	$64, %rsp
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal12IntToCStringEiNS0_6VectorIcEE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN12v8_inspector8String16C1EPKcm
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1067
	addq	$64, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1067:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4015:
	.size	_ZN12v8_inspector8String1611fromIntegerEi, .-_ZN12v8_inspector8String1611fromIntegerEi
	.section	.text._ZN12v8_inspector8String1611fromIntegerEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8String1611fromIntegerEm
	.type	_ZN12v8_inspector8String1611fromIntegerEm, @function
_ZN12v8_inspector8String1611fromIntegerEm:
.LFB4019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	leaq	.LC5(%rip), %rdx
	movl	$50, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-80(%rbp), %r12
	movq	%r12, %rdi
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base2OS8SNPrintFEPciPKcz@PLT
	movq	%r12, %rdx
.L1069:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L1069
	movl	%eax, %ecx
	movq	%r13, %rdi
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %esi
	addb	%al, %sil
	movq	%r12, %rsi
	sbbq	$3, %rdx
	subq	%r12, %rdx
	call	_ZN12v8_inspector8String16C1EPKcm
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1075
	addq	$64, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1075:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4019:
	.size	_ZN12v8_inspector8String1611fromIntegerEm, .-_ZN12v8_inspector8String1611fromIntegerEm
	.section	.text._ZN12v8_inspector8String1610fromDoubleEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8String1610fromDoubleEd
	.type	_ZN12v8_inspector8String1610fromDoubleEd, @function
_ZN12v8_inspector8String1610fromDoubleEd:
.LFB4020:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$50, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	leaq	-80(%rbp), %rdi
	subq	$64, %rsp
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal15DoubleToCStringEdNS0_6VectorIcEE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN12v8_inspector8String16C1EPKcm
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1079
	addq	$64, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1079:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4020:
	.size	_ZN12v8_inspector8String1610fromDoubleEd, .-_ZN12v8_inspector8String1610fromDoubleEd
	.section	.text._ZN12v8_inspector8String1610fromDoubleEdi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8String1610fromDoubleEdi
	.type	_ZN12v8_inspector8String1610fromDoubleEdi, @function
_ZN12v8_inspector8String1610fromDoubleEdi:
.LFB4021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	%esi, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	_ZN2v88internal24DoubleToPrecisionCStringEdi@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN12v8_inspector8String16C1EPKcm
	movq	%r12, %rdi
	call	_ZdaPv@PLT
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4021:
	.size	_ZN12v8_inspector8String1610fromDoubleEdi, .-_ZN12v8_inspector8String1610fromDoubleEdi
	.section	.text._ZN12v8_inspector8String16C2EPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8String16C2EPKc
	.type	_ZN12v8_inspector8String16C2EPKc, @function
_ZN12v8_inspector8String16C2EPKc:
.LFB4004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	strlen@PLT
	leaq	16(%rbx), %rdi
	xorl	%edx, %edx
	movq	$0, 8(%rbx)
	movq	%rdi, (%rbx)
	movw	%dx, 16(%rbx)
	movq	$0, 32(%rbx)
	testq	%rax, %rax
	jne	.L1094
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1094:
	.cfi_restore_state
	movq	%rax, %r12
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r12
	ja	.L1095
	cmpq	$7, %r12
	ja	.L1096
	cmpq	$1, %r12
	jne	.L1086
.L1087:
	xorl	%eax, %eax
	movq	%r12, 8(%rbx)
	movw	%ax, (%rdi,%r12,2)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1089:
	movsbw	0(%r13,%rax), %cx
	movq	(%rbx), %rdx
	movw	%cx, (%rdx,%rax,2)
	addq	$1, %rax
	cmpq	%rax, %r12
	jne	.L1089
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1096:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%r12, %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	(%rbx), %rdi
.L1086:
	leaq	(%r12,%r12), %rdx
	xorl	%esi, %esi
	call	memset@PLT
	movq	(%rbx), %rdi
	jmp	.L1087
.L1095:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4004:
	.size	_ZN12v8_inspector8String16C2EPKc, .-_ZN12v8_inspector8String16C2EPKc
	.globl	_ZN12v8_inspector8String16C1EPKc
	.set	_ZN12v8_inspector8String16C1EPKc,_ZN12v8_inspector8String16C2EPKc
	.section	.text.startup._GLOBAL__sub_I__ZN12v8_inspector8String16C2EPKtm,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN12v8_inspector8String16C2EPKtm, @function
_GLOBAL__sub_I__ZN12v8_inspector8String16C2EPKtm:
.LFB5046:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE5046:
	.size	_GLOBAL__sub_I__ZN12v8_inspector8String16C2EPKtm, .-_GLOBAL__sub_I__ZN12v8_inspector8String16C2EPKtm
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN12v8_inspector8String16C2EPKtm
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
