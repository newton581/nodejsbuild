	.file	"runtime-typedarray.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5451:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5452:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5452:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5454:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5454:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal12_GLOBAL__N_110CompareNumIdEEbT_S3_,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_110CompareNumIdEEbT_S3_, @function
_ZN2v88internal12_GLOBAL__N_110CompareNumIdEEbT_S3_:
.LFB22238:
	.cfi_startproc
	endbr64
	comisd	%xmm0, %xmm1
	movl	$1, %eax
	ja	.L5
	xorl	%eax, %eax
	comisd	%xmm1, %xmm0
	ja	.L5
	pxor	%xmm2, %xmm2
	ucomisd	%xmm2, %xmm0
	setnp	%dl
	cmovne	%eax, %edx
	testb	%dl, %dl
	je	.L7
	ucomisd	%xmm1, %xmm0
	setnp	%dl
	cmovne	%eax, %edx
	testb	%dl, %dl
	je	.L7
	movmskpd	%xmm0, %edx
	andl	$1, %edx
	je	.L5
	movmskpd	%xmm1, %eax
	testb	$1, %al
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	ucomisd	%xmm1, %xmm1
	setp	%al
	ucomisd	%xmm0, %xmm0
	setnp	%dl
	andl	%edx, %eax
.L5:
	ret
	.cfi_endproc
.LFE22238:
	.size	_ZN2v88internal12_GLOBAL__N_110CompareNumIdEEbT_S3_, .-_ZN2v88internal12_GLOBAL__N_110CompareNumIdEEbT_S3_
	.section	.text._ZN2v88internal12_GLOBAL__N_110CompareNumIfEEbT_S3_,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_110CompareNumIfEEbT_S3_, @function
_ZN2v88internal12_GLOBAL__N_110CompareNumIfEEbT_S3_:
.LFB22228:
	.cfi_startproc
	endbr64
	comiss	%xmm0, %xmm1
	movl	$1, %eax
	ja	.L18
	xorl	%eax, %eax
	comiss	%xmm1, %xmm0
	ja	.L18
	pxor	%xmm3, %xmm3
	pxor	%xmm2, %xmm2
	pxor	%xmm4, %xmm4
	ucomiss	%xmm3, %xmm0
	cvtss2sd	%xmm0, %xmm2
	cvtss2sd	%xmm1, %xmm4
	setnp	%dl
	cmovne	%eax, %edx
	testb	%dl, %dl
	je	.L20
	ucomiss	%xmm1, %xmm0
	setnp	%dl
	cmovne	%eax, %edx
	testb	%dl, %dl
	je	.L20
	movmskpd	%xmm2, %edx
	andl	$1, %edx
	je	.L18
	movmskpd	%xmm4, %eax
	testb	$1, %al
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	ucomisd	%xmm4, %xmm4
	setp	%al
	ucomisd	%xmm2, %xmm2
	setnp	%dl
	andl	%edx, %eax
.L18:
	ret
	.cfi_endproc
.LFE22228:
	.size	_ZN2v88internal12_GLOBAL__N_110CompareNumIfEEbT_S3_, .-_ZN2v88internal12_GLOBAL__N_110CompareNumIfEEbT_S3_
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L37
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L40
.L31:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L31
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL36Stats_Runtime_TypedArrayCopyElementsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC3:
	.string	"V8.Runtime_Runtime_TypedArrayCopyElements"
	.section	.rodata._ZN2v88internalL36Stats_Runtime_TypedArrayCopyElementsEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"args[0].IsJSTypedArray()"
.LC5:
	.string	"Check failed: %s."
.LC6:
	.string	"args[2].IsNumber()"
	.section	.rodata._ZN2v88internalL36Stats_Runtime_TypedArrayCopyElementsEiPmPNS0_7IsolateE.str1.8
	.align 8
.LC9:
	.string	"TryNumberToSize(*length_obj, &length)"
	.section	.text._ZN2v88internalL36Stats_Runtime_TypedArrayCopyElementsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Stats_Runtime_TypedArrayCopyElementsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL36Stats_Runtime_TypedArrayCopyElementsEiPmPNS0_7IsolateE:
.LFB20034:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L86
.L42:
	movq	_ZZN2v88internalL36Stats_Runtime_TypedArrayCopyElementsEiPmPNS0_7IsolateEE27trace_event_unique_atomic49(%rip), %rbx
	testq	%rbx, %rbx
	je	.L87
.L44:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L88
.L46:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L50
.L51:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L87:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L89
.L45:
	movq	%rbx, _ZZN2v88internalL36Stats_Runtime_TypedArrayCopyElementsEiPmPNS0_7IsolateEE27trace_event_unique_atomic49(%rip)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L50:
	movq	-1(%rax), %rdx
	cmpw	$1086, 11(%rdx)
	jne	.L51
	movq	-16(%r13), %rcx
	leaq	-8(%r13), %rsi
	testb	$1, %cl
	je	.L84
	movq	-1(%rcx), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L90
	movsd	7(%rcx), %xmm0
	comisd	.LC0(%rip), %xmm0
	jb	.L56
	movsd	.LC7(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L56
	movsd	.LC8(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L60
	cvttsd2siq	%xmm0, %rcx
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L56:
	leaq	.LC9(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L84:
	sarq	$32, %rcx
	js	.L56
.L57:
	movq	-1(%rax), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	xorl	%r8d, %r8d
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %rdi
	movq	%r13, %rdx
	movq	(%rdi), %rax
	call	*240(%rax)
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L62
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L62:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L91
.L41:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L92
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L93
.L47:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L48
	movq	(%rdi), %rax
	call	*8(%rax)
.L48:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L49
	movq	(%rdi), %rax
	call	*8(%rax)
.L49:
	leaq	.LC3(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L86:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$642, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L90:
	leaq	.LC6(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L60:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rcx
	btcq	$63, %rcx
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L91:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L89:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L93:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC3(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L47
.L92:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20034:
	.size	_ZN2v88internalL36Stats_Runtime_TypedArrayCopyElementsEiPmPNS0_7IsolateE, .-_ZN2v88internalL36Stats_Runtime_TypedArrayCopyElementsEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL33Stats_Runtime_TypedArrayGetBufferEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"V8.Runtime_Runtime_TypedArrayGetBuffer"
	.section	.text._ZN2v88internalL33Stats_Runtime_TypedArrayGetBufferEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_TypedArrayGetBufferEiPmPNS0_7IsolateE, @function
_ZN2v88internalL33Stats_Runtime_TypedArrayGetBufferEiPmPNS0_7IsolateE:
.LFB20037:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L126
.L95:
	movq	_ZZN2v88internalL33Stats_Runtime_TypedArrayGetBufferEiPmPNS0_7IsolateEE27trace_event_unique_atomic63(%rip), %rbx
	testq	%rbx, %rbx
	je	.L127
.L97:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L128
.L99:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L103
.L104:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L127:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L129
.L98:
	movq	%rbx, _ZZN2v88internalL33Stats_Runtime_TypedArrayGetBufferEiPmPNS0_7IsolateEE27trace_event_unique_atomic63(%rip)
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L103:
	movq	-1(%rax), %rdx
	cmpw	$1086, 11(%rdx)
	jne	.L104
	leaq	-152(%rbp), %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal12JSTypedArray9GetBufferEv@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L105
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L105:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L130
.L94:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L131
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L132
.L100:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L101
	movq	(%rdi), %rax
	call	*8(%rax)
.L101:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L102
	movq	(%rdi), %rax
	call	*8(%rax)
.L102:
	leaq	.LC10(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L126:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$643, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L130:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L129:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L132:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC10(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L100
.L131:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20037:
	.size	_ZN2v88internalL33Stats_Runtime_TypedArrayGetBufferEiPmPNS0_7IsolateE, .-_ZN2v88internalL33Stats_Runtime_TypedArrayGetBufferEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL31Stats_Runtime_ArrayBufferDetachEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"V8.Runtime_Runtime_ArrayBufferDetach"
	.align 8
.LC12:
	.string	"0 == array_buffer->byte_length()"
	.section	.rodata._ZN2v88internalL31Stats_Runtime_ArrayBufferDetachEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC13:
	.string	"!array_buffer->is_shared()"
	.section	.text._ZN2v88internalL31Stats_Runtime_ArrayBufferDetachEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_ArrayBufferDetachEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL31Stats_Runtime_ArrayBufferDetachEiPmPNS0_7IsolateE.isra.0:
.LFB26440:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L172
.L134:
	movq	_ZZN2v88internalL31Stats_Runtime_ArrayBufferDetachEiPmPNS0_7IsolateEE27trace_event_unique_atomic19(%rip), %rbx
	testq	%rbx, %rbx
	je	.L173
.L136:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L174
.L138:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L142
.L144:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$100, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L143:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L151
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L151:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L175
.L133:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L176
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1059, 11(%rdx)
	jne	.L144
	movl	39(%rax), %edx
	andl	$2, %edx
	je	.L171
	cmpq	$0, 31(%rax)
	je	.L177
	movl	39(%rax), %edx
	andl	$8, %edx
	jne	.L178
	movq	23(%rax), %r8
	movq	31(%rax), %r15
	leaq	37592(%r12), %rdi
	movq	%r8, -184(%rbp)
	movl	39(%rax), %edx
	orl	$1, %edx
	movl	%edx, 39(%rax)
	movq	0(%r13), %rsi
	call	_ZN2v88internal4Heap21UnregisterArrayBufferENS0_13JSArrayBufferE@PLT
	movq	0(%r13), %rax
	leaq	-168(%rbp), %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal13JSArrayBuffer6DetachEv@PLT
	movq	45544(%r12), %rdi
	movq	-184(%rbp), %r8
	movq	%r15, %rsi
	movq	(%rdi), %rax
	movq	%r8, %rdx
	call	*32(%rax)
.L171:
	movq	88(%r12), %r13
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L174:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L179
.L139:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L140
	movq	(%rdi), %rax
	call	*8(%rax)
.L140:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L141
	movq	(%rdi), %rax
	call	*8(%rax)
.L141:
	leaq	.LC11(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L173:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L180
.L137:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_ArrayBufferDetachEiPmPNS0_7IsolateEE27trace_event_unique_atomic19(%rip)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L172:
	movq	40960(%rsi), %rax
	movl	$641, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L175:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L180:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L179:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC11(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L177:
	cmpq	$0, 23(%rax)
	je	.L171
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L178:
	leaq	.LC13(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L176:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26440:
	.size	_ZN2v88internalL31Stats_Runtime_ArrayBufferDetachEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL31Stats_Runtime_ArrayBufferDetachEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL27Stats_Runtime_TypedArraySetEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"V8.Runtime_Runtime_TypedArraySet"
	.align 8
.LC16:
	.string	"DoubleToUint32IfEqualToSelf(len->Number(), &int_l)"
	.section	.text._ZN2v88internalL27Stats_Runtime_TypedArraySetEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL27Stats_Runtime_TypedArraySetEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL27Stats_Runtime_TypedArraySetEiPmPNS0_7IsolateE.isra.0:
.LFB26450:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -208(%rbp)
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L250
.L182:
	movq	_ZZN2v88internalL27Stats_Runtime_TypedArraySetEiPmPNS0_7IsolateEE28trace_event_unique_atomic168(%rip), %rbx
	testq	%rbx, %rbx
	je	.L251
.L184:
	movq	$0, -240(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L252
.L186:
	addl	$1, 41104(%r12)
	movq	-8(%r13), %rax
	leaq	-8(%r13), %r15
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	testb	$1, %al
	jne	.L190
.L192:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
.L249:
	movq	(%rax), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L191:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L224
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L224:
	leaq	-240(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L253
.L181:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L254
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	movq	-16(%r13), %rcx
	movq	%rcx, -248(%rbp)
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L192
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	ja	.L195
	movq	%r15, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L248
.L195:
	movq	(%r15), %rax
	leaq	2768(%r12), %r8
	testb	$1, %al
	jne	.L197
.L199:
	movl	$-1, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r8, -256(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-256(%rbp), %r8
	movq	%rax, %rdx
.L198:
	movq	2768(%r12), %rcx
	movl	$3, %eax
	movq	-1(%rcx), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L200
	movl	11(%rcx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L200:
	movl	%eax, -160(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -148(%rbp)
	movq	2768(%r12), %rax
	movq	%r12, -136(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L255
.L201:
	leaq	-160(%rbp), %rdi
	movq	%r8, -128(%rbp)
	movq	%rdi, -256(%rbp)
	movq	$0, -120(%rbp)
	movq	%r15, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movq	$-1, -88(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -156(%rbp)
	movq	-256(%rbp), %rdi
	jne	.L202
	movq	-136(%rbp), %rax
	addq	$88, %rax
.L203:
	movq	(%rax), %rsi
	testb	$1, %sil
	jne	.L204
	sarq	$32, %rsi
	movl	$0, %eax
	movq	41112(%r12), %rdi
	cmovs	%rax, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L205
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L206:
	testq	%rax, %rax
	je	.L248
.L208:
	movq	-248(%rbp), %r8
	movq	(%rax), %rax
	pxor	%xmm2, %xmm2
	sarq	$32, %r8
	movl	%r8d, %edx
	cvtsi2sdq	%rdx, %xmm2
	testb	$1, %al
	jne	.L211
	sarq	$32, %rax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
.L212:
	movq	0(%r13), %rdx
	addsd	%xmm1, %xmm2
	movq	47(%rdx), %rax
	testq	%rax, %rax
	js	.L214
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L215:
	comisd	%xmm0, %xmm2
	jbe	.L246
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$220, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L252:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L256
.L187:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L188
	movq	(%rdi), %rax
	call	*8(%rax)
.L188:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L189
	movq	(%rdi), %rax
	call	*8(%rax)
.L189:
	leaq	.LC14(%rip), %rax
	movq	%rbx, -232(%rbp)
	movq	%rax, -224(%rbp)
	leaq	-232(%rbp), %rax
	movq	%r14, -216(%rbp)
	movq	%rax, -240(%rbp)
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L251:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L257
.L185:
	movq	%rbx, _ZZN2v88internalL27Stats_Runtime_TypedArraySetEiPmPNS0_7IsolateEE28trace_event_unique_atomic168(%rip)
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L250:
	movq	40960(%rsi), %rax
	movl	$644, %edx
	leaq	-200(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -208(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L253:
	leaq	-200(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L202:
	movl	$1, %esi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	jne	.L203
.L248:
	movq	312(%r12), %r13
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L257:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L256:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC14(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L205:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L258
.L207:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L246:
	movsd	.LC15(%rip), %xmm3
	addsd	%xmm1, %xmm3
	movq	%xmm3, %rax
	movq	%xmm3, %rcx
	shrq	$32, %rax
	cmpq	$1127219200, %rax
	je	.L259
.L217:
	leaq	.LC16(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L211:
	movsd	7(%rax), %xmm1
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L197:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L199
	movq	%r15, %rdx
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L214:
	movq	%rax, %rcx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rax, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L204:
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToLengthEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L255:
	movq	%r8, %rsi
	movq	%r12, %rdi
	movq	%rdx, -256(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-256(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L259:
	movl	%ecx, %ecx
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rcx, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L217
	jne	.L217
	movq	-1(%rdx), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movq	%r15, %rsi
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %rdi
	movq	%r13, %rdx
	movq	(%rdi), %rax
	call	*240(%rax)
	movq	%rax, %r13
	jmp	.L191
.L258:
	movq	%r12, %rdi
	movq	%rsi, -256(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-256(%rbp), %rsi
	jmp	.L207
.L254:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26450:
	.size	_ZN2v88internalL27Stats_Runtime_TypedArraySetEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL27Stats_Runtime_TypedArraySetEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal25Runtime_ArrayBufferDetachEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_ArrayBufferDetachEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_ArrayBufferDetachEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_ArrayBufferDetachEiPmPNS0_7IsolateE:
.LFB20032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L275
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r14
	testb	$1, %al
	jne	.L263
.L265:
	xorl	%edx, %edx
	movl	$100, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L264:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L260
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rax
.L260:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L276
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L263:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1059, 11(%rdx)
	jne	.L265
	movl	39(%rax), %edx
	andl	$2, %edx
	je	.L274
	cmpq	$0, 31(%rax)
	je	.L277
	movl	39(%rax), %edx
	andl	$8, %edx
	jne	.L278
	movq	23(%rax), %r8
	movq	31(%rax), %r15
	leaq	37592(%r12), %rdi
	movq	%r8, -72(%rbp)
	movl	39(%rax), %edx
	orl	$1, %edx
	movl	%edx, 39(%rax)
	movq	(%rsi), %rsi
	call	_ZN2v88internal4Heap21UnregisterArrayBufferENS0_13JSArrayBufferE@PLT
	movq	0(%r13), %rax
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal13JSArrayBuffer6DetachEv@PLT
	movq	45544(%r12), %rdi
	movq	-72(%rbp), %r8
	movq	%r15, %rsi
	movq	(%rdi), %rax
	movq	%r8, %rdx
	call	*32(%rax)
.L274:
	movq	88(%r12), %rax
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L275:
	movq	%rdx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internalL31Stats_Runtime_ArrayBufferDetachEiPmPNS0_7IsolateE.isra.0
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L277:
	cmpq	$0, 23(%rax)
	je	.L274
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L278:
	leaq	.LC13(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L276:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20032:
	.size	_ZN2v88internal25Runtime_ArrayBufferDetachEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_ArrayBufferDetachEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Runtime_TypedArrayCopyElementsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Runtime_TypedArrayCopyElementsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Runtime_TypedArrayCopyElementsEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Runtime_TypedArrayCopyElementsEiPmPNS0_7IsolateE:
.LFB20035:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L298
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L281
.L282:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L281:
	movq	-1(%rax), %rdx
	cmpw	$1086, 11(%rdx)
	jne	.L282
	movq	-16(%r9), %rcx
	leaq	-8(%rsi), %rsi
	testb	$1, %cl
	je	.L296
	movq	-1(%rcx), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L299
	movsd	7(%rcx), %xmm0
	comisd	.LC0(%rip), %xmm0
	jb	.L287
	movsd	.LC7(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L287
	movsd	.LC8(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L291
	cvttsd2siq	%xmm0, %rcx
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L287:
	leaq	.LC9(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L296:
	sarq	$32, %rcx
	js	.L287
.L288:
	movq	-1(%rax), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	xorl	%r8d, %r8d
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %rdi
	movq	%r9, %rdx
	movq	(%rdi), %rax
	call	*240(%rax)
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L293
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L293:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL36Stats_Runtime_TypedArrayCopyElementsEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L299:
	.cfi_restore_state
	leaq	.LC6(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L291:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rcx
	btcq	$63, %rcx
	jmp	.L288
	.cfi_endproc
.LFE20035:
	.size	_ZN2v88internal30Runtime_TypedArrayCopyElementsEiPmPNS0_7IsolateE, .-_ZN2v88internal30Runtime_TypedArrayCopyElementsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Runtime_TypedArrayGetBufferEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_TypedArrayGetBufferEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_TypedArrayGetBufferEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_TypedArrayGetBufferEiPmPNS0_7IsolateE:
.LFB20038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L308
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L303
.L304:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L303:
	movq	-1(%rax), %rdx
	cmpw	$1086, 11(%rdx)
	jne	.L304
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal12JSTypedArray9GetBufferEv@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L300
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L300:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L309
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	call	_ZN2v88internalL33Stats_Runtime_TypedArrayGetBufferEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L300
.L309:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20038:
	.size	_ZN2v88internal27Runtime_TypedArrayGetBufferEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_TypedArrayGetBufferEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Runtime_TypedArraySetEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Runtime_TypedArraySetEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Runtime_TypedArraySetEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Runtime_TypedArraySetEiPmPNS0_7IsolateE:
.LFB20045:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %ecx
	testl	%ecx, %ecx
	jne	.L354
	addl	$1, 41104(%rdx)
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %r15
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L313
.L315:
	xorl	%edx, %edx
	movl	$64, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L314:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L310
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rax
.L310:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L355
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L313:
	.cfi_restore_state
	movq	-16(%rsi), %rdi
	movq	%rdi, -152(%rbp)
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L315
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	ja	.L318
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%ecx, -160(%rbp)
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movl	-160(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L353
.L318:
	movq	(%r15), %rax
	leaq	2768(%r12), %r8
	testb	$1, %al
	jne	.L320
.L322:
	movl	$-1, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r8, -168(%rbp)
	movl	%ecx, -160(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movl	-160(%rbp), %ecx
	movq	-168(%rbp), %r8
	movq	%rax, %rdx
.L321:
	movq	2768(%r12), %rax
	movq	-1(%rax), %rsi
	cmpw	$64, 11(%rsi)
	je	.L356
	movl	$3, %ecx
.L323:
	movabsq	$824633720832, %rax
	movl	%ecx, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	2768(%r12), %rax
	movq	%r12, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L357
.L324:
	leaq	-144(%rbp), %rdi
	movq	%r8, -112(%rbp)
	movq	%rdi, -160(%rbp)
	movq	$0, -104(%rbp)
	movq	%r15, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	movq	-160(%rbp), %rdi
	jne	.L325
	movq	-120(%rbp), %rax
	addq	$88, %rax
.L326:
	movq	(%rax), %rsi
	testb	$1, %sil
	jne	.L327
	sarq	$32, %rsi
	movl	$0, %eax
	movq	41112(%r12), %rdi
	cmovs	%rax, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L328
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L329:
	testq	%rax, %rax
	je	.L353
.L331:
	movq	-152(%rbp), %r8
	movq	(%rax), %rax
	pxor	%xmm2, %xmm2
	sarq	$32, %r8
	movl	%r8d, %edx
	cvtsi2sdq	%rdx, %xmm2
	testb	$1, %al
	jne	.L334
	sarq	$32, %rax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
.L335:
	movq	0(%r13), %rdx
	addsd	%xmm1, %xmm2
	movq	47(%rdx), %rax
	testq	%rax, %rax
	js	.L337
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L338:
	comisd	%xmm0, %xmm2
	jbe	.L351
	xorl	%edx, %edx
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$220, %esi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L354:
	movq	%rdx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internalL27Stats_Runtime_TypedArraySetEiPmPNS0_7IsolateE.isra.0
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L325:
	movl	$1, %esi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	jne	.L326
.L353:
	movq	312(%r12), %rax
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L328:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L358
.L330:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L351:
	movsd	.LC15(%rip), %xmm3
	addsd	%xmm1, %xmm3
	movq	%xmm3, %rax
	movq	%xmm3, %rcx
	shrq	$32, %rax
	cmpq	$1127219200, %rax
	je	.L359
.L340:
	leaq	.LC16(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L334:
	movsd	7(%rax), %xmm1
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L356:
	testb	$1, 11(%rax)
	movl	$3, %eax
	cmove	%eax, %ecx
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L320:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L322
	movq	%r15, %rdx
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L337:
	movq	%rax, %rcx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rax, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L327:
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToLengthEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L357:
	movq	%r8, %rsi
	movq	%r12, %rdi
	movq	%rdx, -160(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-160(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L359:
	movl	%ecx, %ecx
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rcx, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L340
	jne	.L340
	movq	-1(%rdx), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movq	%r15, %rsi
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %rdi
	movq	%r13, %rdx
	movq	(%rdi), %rax
	call	*240(%rax)
	jmp	.L314
.L358:
	movq	%r12, %rdi
	movq	%rsi, -160(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rsi
	jmp	.L330
.L355:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20045:
	.size	_ZN2v88internal21Runtime_TypedArraySetEiPmPNS0_7IsolateE, .-_ZN2v88internal21Runtime_TypedArraySetEiPmPNS0_7IsolateE
	.section	.text._ZSt16__insertion_sortIPfN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_S7_T0_,"axG",@progbits,_ZSt16__insertion_sortIPfN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_S7_T0_,comdat
	.p2align 4
	.weak	_ZSt16__insertion_sortIPfN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_S7_T0_
	.type	_ZSt16__insertion_sortIPfN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_S7_T0_, @function
_ZSt16__insertion_sortIPfN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_S7_T0_:
.LFB24917:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L370
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	4(%rdi), %rbx
	subq	$24, %rsp
	cmpq	%rbx, %rsi
	je	.L360
	movq	%rsi, -64(%rbp)
	movq	%rdx, %r13
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L373:
	movss	(%rbx), %xmm0
	cmpq	%rbx, %r12
	je	.L364
	movl	$4, %eax
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movss	%xmm0, -52(%rbp)
	subq	%r12, %rdx
	leaq	(%r12,%rax), %rdi
	call	memmove@PLT
	movss	-52(%rbp), %xmm0
.L364:
	movss	%xmm0, (%r12)
	addq	$4, %rbx
	cmpq	-64(%rbp), %rbx
	je	.L360
.L368:
	movss	(%r12), %xmm1
	movss	(%rbx), %xmm0
	call	*%r13
	testb	%al, %al
	jne	.L373
	movss	(%rbx), %xmm2
	movq	%rbx, %r15
	movss	%xmm2, -52(%rbp)
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L374:
	movss	(%r15), %xmm0
	movss	%xmm0, 4(%r15)
.L367:
	movss	-4(%r15), %xmm1
	movss	-52(%rbp), %xmm0
	movq	%r15, %r14
	subq	$4, %r15
	call	*%r13
	testb	%al, %al
	jne	.L374
	movss	-52(%rbp), %xmm3
	addq	$4, %rbx
	movss	%xmm3, (%r14)
	cmpq	-64(%rbp), %rbx
	jne	.L368
.L360:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE24917:
	.size	_ZSt16__insertion_sortIPfN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_S7_T0_, .-_ZSt16__insertion_sortIPfN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_S7_T0_
	.section	.text._ZSt16__insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_,"axG",@progbits,_ZSt16__insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_,comdat
	.p2align 4
	.weak	_ZSt16__insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_
	.type	_ZSt16__insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_, @function
_ZSt16__insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_:
.LFB24936:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L385
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	8(%rdi), %rbx
	subq	$24, %rsp
	cmpq	%rbx, %rsi
	je	.L375
	movq	%rsi, -64(%rbp)
	movq	%rdx, %r13
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L388:
	movsd	(%rbx), %xmm0
	cmpq	%rbx, %r12
	je	.L379
	movl	$8, %eax
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movsd	%xmm0, -56(%rbp)
	subq	%r12, %rdx
	leaq	(%r12,%rax), %rdi
	call	memmove@PLT
	movsd	-56(%rbp), %xmm0
.L379:
	movsd	%xmm0, (%r12)
	addq	$8, %rbx
	cmpq	-64(%rbp), %rbx
	je	.L375
.L383:
	movsd	(%r12), %xmm1
	movsd	(%rbx), %xmm0
	call	*%r13
	testb	%al, %al
	jne	.L388
	movsd	(%rbx), %xmm2
	movq	%rbx, %r15
	movsd	%xmm2, -56(%rbp)
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L389:
	movsd	(%r15), %xmm0
	movsd	%xmm0, 8(%r15)
.L382:
	movsd	-8(%r15), %xmm1
	movsd	-56(%rbp), %xmm0
	movq	%r15, %r14
	subq	$8, %r15
	call	*%r13
	testb	%al, %al
	jne	.L389
	movsd	-56(%rbp), %xmm3
	addq	$8, %rbx
	movsd	%xmm3, (%r14)
	cmpq	-64(%rbp), %rbx
	jne	.L383
.L375:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L385:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE24936:
	.size	_ZSt16__insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_, .-_ZSt16__insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_
	.section	.text._ZSt13__adjust_heapIPhlhN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_,"axG",@progbits,_ZSt13__adjust_heapIPhlhN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_,comdat
	.p2align 4
	.weak	_ZSt13__adjust_heapIPhlhN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	.type	_ZSt13__adjust_heapIPhlhN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_, @function
_ZSt13__adjust_heapIPhlhN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_:
.LFB26055:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdx), %rax
	movl	%ecx, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, %rbx
	andl	$1, %r12d
	shrq	$63, %rbx
	addq	%rax, %rbx
	sarq	%rbx
	cmpq	%rbx, %rsi
	jge	.L391
	movq	%rsi, %r9
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L404:
	subq	$1, %rax
	leaq	(%rdi,%rax), %r8
	movzbl	(%r8), %r11d
	movb	%r11b, (%rdi,%r9)
	cmpq	%rbx, %rax
	jge	.L393
.L394:
	movq	%rax, %r9
.L395:
	leaq	2(%r9,%r9), %rax
	leaq	(%rdi,%rax), %r8
	movzbl	(%r8), %r11d
	cmpb	-1(%rdi,%rax), %r11b
	jb	.L404
	movb	%r11b, (%rdi,%r9)
	cmpq	%rbx, %rax
	jl	.L394
.L393:
	testq	%r12, %r12
	je	.L399
.L396:
	leaq	-1(%rax), %rdx
	movq	%rdx, %r9
	shrq	$63, %r9
	addq	%rdx, %r9
	sarq	%r9
	cmpq	%rsi, %rax
	jg	.L398
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L406:
	movb	%dl, (%r8)
	leaq	-1(%r9), %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	addq	%rdx, %rax
	sarq	%rax
	movq	%rax, %rdx
	movq	%r9, %rax
	cmpq	%r9, %rsi
	jge	.L405
	movq	%rdx, %r9
.L398:
	leaq	(%rdi,%r9), %r11
	leaq	(%rdi,%rax), %r8
	movzbl	(%r11), %edx
	cmpb	%dl, %r10b
	ja	.L406
.L397:
	movb	%cl, (%r8)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L391:
	.cfi_restore_state
	leaq	(%rdi,%rsi), %r8
	testq	%r12, %r12
	jne	.L397
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L399:
	leaq	-2(%rdx), %r9
	movq	%r9, %rdx
	shrq	$63, %rdx
	addq	%r9, %rdx
	sarq	%rdx
	cmpq	%rax, %rdx
	jne	.L396
	leaq	2(%rax,%rax), %rax
	movzbl	-1(%rdi,%rax), %edx
	subq	$1, %rax
	movb	%dl, (%r8)
	leaq	(%rdi,%rax), %r8
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L405:
	movq	%r11, %r8
	movb	%cl, (%r8)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26055:
	.size	_ZSt13__adjust_heapIPhlhN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_, .-_ZSt13__adjust_heapIPhlhN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	.section	.text._ZSt16__introsort_loopIPhlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZSt16__introsort_loopIPhlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0, @function
_ZSt16__introsort_loopIPhlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0:
.LFB26524:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	subq	%rdi, %r13
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	$16, %r13
	jle	.L407
	movq	%rdi, %rbx
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L435
	leaq	1(%rdi), %rax
	movq	%rax, -56(%rbp)
.L411:
	sarq	%r13
	movzwl	(%rbx), %eax
	movzbl	1(%rbx), %edi
	subq	$1, %r14
	addq	%rbx, %r13
	movzbl	-1(%rsi), %r9d
	movzbl	(%rbx), %ecx
	movzbl	0(%r13), %r8d
	rolw	$8, %ax
	cmpb	%r8b, %dil
	jnb	.L414
	cmpb	%r9b, %r8b
	jb	.L419
	cmpb	%r9b, %dil
	jb	.L433
.L434:
	movw	%ax, (%rbx)
	movzbl	-1(%rsi), %ecx
.L416:
	movq	-56(%rbp), %r12
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L420:
	movzbl	(%r12), %r8d
	movq	%r12, %r15
	cmpb	%dil, %r8b
	jb	.L421
	subq	$1, %rax
	cmpb	%cl, %dil
	jnb	.L422
	.p2align 4,,10
	.p2align 3
.L423:
	movzbl	-1(%rax), %ecx
	subq	$1, %rax
	cmpb	%dil, %cl
	ja	.L423
.L422:
	cmpq	%rax, %r12
	jnb	.L436
	movb	%cl, (%r12)
	movzbl	-1(%rax), %ecx
	movb	%r8b, (%rax)
	movzbl	(%rbx), %edi
.L421:
	addq	$1, %r12
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L414:
	cmpb	%r9b, %dil
	jb	.L434
	cmpb	%r9b, %r8b
	jnb	.L419
.L433:
	movb	%r9b, (%rbx)
	movb	%cl, -1(%rsi)
	movzbl	(%rbx), %edi
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L436:
	movq	%r12, %r13
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZSt16__introsort_loopIPhlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	subq	%rbx, %r13
	cmpq	$16, %r13
	jle	.L407
	testq	%r14, %r14
	je	.L409
	movq	%r12, %rsi
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L419:
	movb	%r8b, (%rbx)
	movb	%cl, 0(%r13)
	movzbl	(%rbx), %edi
	movzbl	-1(%rsi), %ecx
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L435:
	movq	%rsi, %r15
.L409:
	leaq	-2(%r13), %r12
	movq	%r13, %rdx
	movq	%rbx, %rdi
	sarq	%r12
	movzbl	(%rbx,%r12), %ecx
	movq	%r12, %rsi
	call	_ZSt13__adjust_heapIPhlhN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	.p2align 4,,10
	.p2align 3
.L412:
	subq	$1, %r12
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movzbl	(%rbx,%r12), %ecx
	movq	%r12, %rsi
	call	_ZSt13__adjust_heapIPhlhN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	testq	%r12, %r12
	jne	.L412
	.p2align 4,,10
	.p2align 3
.L413:
	movzbl	(%rbx), %eax
	subq	$1, %r15
	movzbl	(%r15), %ecx
	xorl	%esi, %esi
	movq	%r15, %r12
	movq	%rbx, %rdi
	movb	%al, (%r15)
	subq	%rbx, %r12
	movq	%r12, %rdx
	call	_ZSt13__adjust_heapIPhlhN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	cmpq	$1, %r12
	jg	.L413
.L407:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26524:
	.size	_ZSt16__introsort_loopIPhlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0, .-_ZSt16__introsort_loopIPhlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	.section	.text._ZSt13__adjust_heapIPalaN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_,"axG",@progbits,_ZSt13__adjust_heapIPalaN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_,comdat
	.p2align 4
	.weak	_ZSt13__adjust_heapIPalaN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	.type	_ZSt13__adjust_heapIPalaN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_, @function
_ZSt13__adjust_heapIPalaN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_:
.LFB26067:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdx), %rax
	movl	%ecx, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, %rbx
	andl	$1, %r12d
	shrq	$63, %rbx
	addq	%rax, %rbx
	sarq	%rbx
	cmpq	%rbx, %rsi
	jge	.L438
	movq	%rsi, %r9
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L451:
	subq	$1, %rax
	leaq	(%rdi,%rax), %r8
	movzbl	(%r8), %r11d
	movb	%r11b, (%rdi,%r9)
	cmpq	%rbx, %rax
	jge	.L440
.L441:
	movq	%rax, %r9
.L442:
	leaq	2(%r9,%r9), %rax
	leaq	(%rdi,%rax), %r8
	movzbl	(%r8), %r11d
	cmpb	-1(%rdi,%rax), %r11b
	jl	.L451
	movb	%r11b, (%rdi,%r9)
	cmpq	%rbx, %rax
	jl	.L441
.L440:
	testq	%r12, %r12
	je	.L446
.L443:
	leaq	-1(%rax), %rdx
	movq	%rdx, %r9
	shrq	$63, %r9
	addq	%rdx, %r9
	sarq	%r9
	cmpq	%rsi, %rax
	jg	.L445
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L453:
	movb	%dl, (%r8)
	leaq	-1(%r9), %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	addq	%rdx, %rax
	sarq	%rax
	movq	%rax, %rdx
	movq	%r9, %rax
	cmpq	%r9, %rsi
	jge	.L452
	movq	%rdx, %r9
.L445:
	leaq	(%rdi,%r9), %r11
	leaq	(%rdi,%rax), %r8
	movzbl	(%r11), %edx
	cmpb	%dl, %r10b
	jg	.L453
.L444:
	movb	%cl, (%r8)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L438:
	.cfi_restore_state
	leaq	(%rdi,%rsi), %r8
	testq	%r12, %r12
	jne	.L444
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L446:
	leaq	-2(%rdx), %r9
	movq	%r9, %rdx
	shrq	$63, %rdx
	addq	%r9, %rdx
	sarq	%rdx
	cmpq	%rax, %rdx
	jne	.L443
	leaq	2(%rax,%rax), %rax
	movzbl	-1(%rdi,%rax), %edx
	subq	$1, %rax
	movb	%dl, (%r8)
	leaq	(%rdi,%rax), %r8
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L452:
	movq	%r11, %r8
	movb	%cl, (%r8)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26067:
	.size	_ZSt13__adjust_heapIPalaN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_, .-_ZSt13__adjust_heapIPalaN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	.section	.text._ZSt16__introsort_loopIPalN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZSt16__introsort_loopIPalN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0, @function
_ZSt16__introsort_loopIPalN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0:
.LFB26532:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	subq	%rdi, %r13
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	$16, %r13
	jle	.L454
	movq	%rdi, %rbx
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L482
	leaq	1(%rdi), %rax
	movq	%rax, -56(%rbp)
.L458:
	sarq	%r13
	movzwl	(%rbx), %eax
	movzbl	1(%rbx), %edi
	subq	$1, %r14
	addq	%rbx, %r13
	movzbl	-1(%rsi), %r9d
	movzbl	(%rbx), %ecx
	movzbl	0(%r13), %r8d
	rolw	$8, %ax
	cmpb	%r8b, %dil
	jge	.L461
	cmpb	%r9b, %r8b
	jl	.L466
	cmpb	%r9b, %dil
	jl	.L480
.L481:
	movw	%ax, (%rbx)
	movzbl	-1(%rsi), %ecx
.L463:
	movq	-56(%rbp), %r12
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L467:
	movzbl	(%r12), %r8d
	movq	%r12, %r15
	cmpb	%dil, %r8b
	jl	.L468
	subq	$1, %rax
	cmpb	%cl, %dil
	jge	.L469
	.p2align 4,,10
	.p2align 3
.L470:
	movzbl	-1(%rax), %ecx
	subq	$1, %rax
	cmpb	%dil, %cl
	jg	.L470
.L469:
	cmpq	%rax, %r12
	jnb	.L483
	movb	%cl, (%r12)
	movzbl	-1(%rax), %ecx
	movb	%r8b, (%rax)
	movzbl	(%rbx), %edi
.L468:
	addq	$1, %r12
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L461:
	cmpb	%r9b, %dil
	jl	.L481
	cmpb	%r9b, %r8b
	jge	.L466
.L480:
	movb	%r9b, (%rbx)
	movb	%cl, -1(%rsi)
	movzbl	(%rbx), %edi
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L483:
	movq	%r12, %r13
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZSt16__introsort_loopIPalN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	subq	%rbx, %r13
	cmpq	$16, %r13
	jle	.L454
	testq	%r14, %r14
	je	.L456
	movq	%r12, %rsi
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L466:
	movb	%r8b, (%rbx)
	movb	%cl, 0(%r13)
	movzbl	(%rbx), %edi
	movzbl	-1(%rsi), %ecx
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L482:
	movq	%rsi, %r15
.L456:
	leaq	-2(%r13), %r12
	movq	%r13, %rdx
	movq	%rbx, %rdi
	sarq	%r12
	movsbl	(%rbx,%r12), %ecx
	movq	%r12, %rsi
	call	_ZSt13__adjust_heapIPalaN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	.p2align 4,,10
	.p2align 3
.L459:
	subq	$1, %r12
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movsbl	(%rbx,%r12), %ecx
	movq	%r12, %rsi
	call	_ZSt13__adjust_heapIPalaN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	testq	%r12, %r12
	jne	.L459
	.p2align 4,,10
	.p2align 3
.L460:
	movzbl	(%rbx), %eax
	subq	$1, %r15
	movsbl	(%r15), %ecx
	xorl	%esi, %esi
	movq	%r15, %r12
	movq	%rbx, %rdi
	movb	%al, (%r15)
	subq	%rbx, %r12
	movq	%r12, %rdx
	call	_ZSt13__adjust_heapIPalaN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	cmpq	$1, %r12
	jg	.L460
.L454:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26532:
	.size	_ZSt16__introsort_loopIPalN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0, .-_ZSt16__introsort_loopIPalN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	.section	.text._ZSt13__adjust_heapIPtltN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_,"axG",@progbits,_ZSt13__adjust_heapIPtltN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_,comdat
	.p2align 4
	.weak	_ZSt13__adjust_heapIPtltN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	.type	_ZSt13__adjust_heapIPtltN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_, @function
_ZSt13__adjust_heapIPtltN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_:
.LFB26079:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdx), %rax
	movl	%ecx, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	andl	$1, %r13d
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rax, %rbx
	shrq	$63, %rbx
	addq	%rax, %rbx
	sarq	%rbx
	cmpq	%rbx, %rsi
	jge	.L485
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L489:
	leaq	1(%rax), %r8
	leaq	(%r8,%r8), %rax
	salq	$2, %r8
	leaq	(%rdi,%r8), %r9
	movq	%rax, %r12
	movzwl	(%r9), %r11d
	cmpw	-2(%rdi,%r8), %r11w
	jnb	.L486
	subq	$1, %rax
	leaq	(%rdi,%rax,2), %r9
	movzwl	(%r9), %r8d
	movw	%r8w, -2(%rdi,%r12)
	cmpq	%rbx, %rax
	jl	.L489
	testq	%r13, %r13
	je	.L493
.L490:
	leaq	-1(%rax), %rdx
	movq	%rdx, %r8
	shrq	$63, %r8
	addq	%rdx, %r8
	sarq	%r8
	cmpq	%rsi, %rax
	jg	.L492
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L499:
	movw	%dx, (%r9)
	leaq	-1(%r8), %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	addq	%rdx, %rax
	sarq	%rax
	movq	%rax, %rdx
	movq	%r8, %rax
	cmpq	%r8, %rsi
	jge	.L498
	movq	%rdx, %r8
.L492:
	leaq	(%rdi,%r8,2), %r11
	leaq	(%rdi,%rax,2), %r9
	movzwl	(%r11), %edx
	cmpw	%dx, %r10w
	ja	.L499
.L491:
	movw	%cx, (%r9)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L485:
	.cfi_restore_state
	leaq	(%rdi,%rsi,2), %r9
	testq	%r13, %r13
	jne	.L491
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L493:
	leaq	-2(%rdx), %r8
	movq	%r8, %rdx
	shrq	$63, %rdx
	addq	%r8, %rdx
	sarq	%rdx
	cmpq	%rax, %rdx
	jne	.L490
	leaq	2(%rax,%rax), %rax
	movzwl	-2(%rdi,%rax,2), %edx
	subq	$1, %rax
	movw	%dx, (%r9)
	leaq	(%rdi,%rax,2), %r9
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L486:
	movw	%r11w, -2(%rdi,%rax)
	cmpq	%rbx, %rax
	jl	.L489
	testq	%r13, %r13
	jne	.L490
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L498:
	movq	%r11, %r9
	movw	%cx, (%r9)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26079:
	.size	_ZSt13__adjust_heapIPtltN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_, .-_ZSt13__adjust_heapIPtltN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	.section	.text._ZSt16__introsort_loopIPtlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZSt16__introsort_loopIPtlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0, @function
_ZSt16__introsort_loopIPtlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0:
.LFB26540:
	.cfi_startproc
	movq	%rsi, %rax
	subq	%rdi, %rax
	cmpq	$32, %rax
	jle	.L526
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	2(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L531
.L504:
	sarq	$2, %rax
	movzwl	2(%rbx), %edi
	movzwl	-2(%rsi), %edx
	subq	$1, %r15
	leaq	(%rbx,%rax,2), %r8
	movzwl	(%rbx), %ecx
	movzwl	(%r8), %eax
	cmpw	%ax, %di
	jnb	.L507
	cmpw	%dx, %ax
	jb	.L512
	cmpw	%dx, %di
	jb	.L529
.L530:
	movw	%di, (%rbx)
	movw	%cx, 2(%rbx)
	movzwl	-2(%rsi), %ecx
.L509:
	movq	%r13, %r12
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L513:
	movzwl	(%r12), %edx
	movq	%r12, %r14
	cmpw	%di, %dx
	jb	.L514
	subq	$2, %rax
	cmpw	%cx, %di
	jnb	.L515
	.p2align 4,,10
	.p2align 3
.L516:
	movzwl	-2(%rax), %ecx
	subq	$2, %rax
	cmpw	%di, %cx
	ja	.L516
.L515:
	cmpq	%rax, %r12
	jnb	.L532
	movw	%cx, (%r12)
	movzwl	-2(%rax), %ecx
	movw	%dx, (%rax)
	movzwl	(%rbx), %edi
.L514:
	addq	$2, %r12
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L507:
	cmpw	%dx, %di
	jb	.L530
	cmpw	%dx, %ax
	jnb	.L512
.L529:
	movw	%dx, (%rbx)
	movw	%cx, -2(%rsi)
	movzwl	(%rbx), %edi
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L532:
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZSt16__introsort_loopIPtlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	movq	%r12, %rax
	subq	%rbx, %rax
	cmpq	$32, %rax
	jle	.L500
	testq	%r15, %r15
	je	.L502
	movq	%r12, %rsi
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L512:
	movw	%ax, (%rbx)
	movw	%cx, (%r8)
	movzwl	(%rbx), %edi
	movzwl	-2(%rsi), %ecx
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L531:
	movq	%rsi, %r14
.L502:
	sarq	%rax
	movq	%rbx, %rdi
	leaq	-2(%rax), %r13
	movq	%rax, %rdx
	movq	%rax, %r12
	sarq	%r13
	movzwl	(%rbx,%r13,2), %ecx
	movq	%r13, %rsi
	call	_ZSt13__adjust_heapIPtltN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
.L505:
	subq	$1, %r13
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movzwl	(%rbx,%r13,2), %ecx
	movq	%r13, %rsi
	call	_ZSt13__adjust_heapIPtltN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	testq	%r13, %r13
	jne	.L505
	.p2align 4,,10
	.p2align 3
.L506:
	subq	$2, %r14
	movzwl	(%rbx), %eax
	movzwl	(%r14), %ecx
	xorl	%esi, %esi
	movq	%r14, %r12
	movq	%rbx, %rdi
	subq	%rbx, %r12
	movw	%ax, (%r14)
	movq	%r12, %rdx
	sarq	%rdx
	call	_ZSt13__adjust_heapIPtltN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	cmpq	$2, %r12
	jg	.L506
.L500:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L526:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE26540:
	.size	_ZSt16__introsort_loopIPtlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0, .-_ZSt16__introsort_loopIPtlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	.section	.text._ZSt13__adjust_heapIPslsN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_,"axG",@progbits,_ZSt13__adjust_heapIPslsN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_,comdat
	.p2align 4
	.weak	_ZSt13__adjust_heapIPslsN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	.type	_ZSt13__adjust_heapIPslsN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_, @function
_ZSt13__adjust_heapIPslsN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_:
.LFB26092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdx), %rax
	movl	%ecx, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	andl	$1, %r13d
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rax, %rbx
	shrq	$63, %rbx
	addq	%rax, %rbx
	sarq	%rbx
	cmpq	%rbx, %rsi
	jge	.L534
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L538:
	leaq	1(%rax), %r8
	leaq	(%r8,%r8), %rax
	salq	$2, %r8
	leaq	(%rdi,%r8), %r9
	movq	%rax, %r12
	movzwl	(%r9), %r11d
	cmpw	-2(%rdi,%r8), %r11w
	jge	.L535
	subq	$1, %rax
	leaq	(%rdi,%rax,2), %r9
	movzwl	(%r9), %r8d
	movw	%r8w, -2(%rdi,%r12)
	cmpq	%rbx, %rax
	jl	.L538
	testq	%r13, %r13
	je	.L542
.L539:
	leaq	-1(%rax), %rdx
	movq	%rdx, %r8
	shrq	$63, %r8
	addq	%rdx, %r8
	sarq	%r8
	cmpq	%rsi, %rax
	jg	.L541
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L548:
	movw	%dx, (%r9)
	leaq	-1(%r8), %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	addq	%rdx, %rax
	sarq	%rax
	movq	%rax, %rdx
	movq	%r8, %rax
	cmpq	%r8, %rsi
	jge	.L547
	movq	%rdx, %r8
.L541:
	leaq	(%rdi,%r8,2), %r11
	leaq	(%rdi,%rax,2), %r9
	movzwl	(%r11), %edx
	cmpw	%dx, %r10w
	jg	.L548
.L540:
	movw	%cx, (%r9)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L534:
	.cfi_restore_state
	leaq	(%rdi,%rsi,2), %r9
	testq	%r13, %r13
	jne	.L540
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L542:
	leaq	-2(%rdx), %r8
	movq	%r8, %rdx
	shrq	$63, %rdx
	addq	%r8, %rdx
	sarq	%rdx
	cmpq	%rax, %rdx
	jne	.L539
	leaq	2(%rax,%rax), %rax
	movzwl	-2(%rdi,%rax,2), %edx
	subq	$1, %rax
	movw	%dx, (%r9)
	leaq	(%rdi,%rax,2), %r9
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L535:
	movw	%r11w, -2(%rdi,%rax)
	cmpq	%rbx, %rax
	jl	.L538
	testq	%r13, %r13
	jne	.L539
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L547:
	movq	%r11, %r9
	movw	%cx, (%r9)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26092:
	.size	_ZSt13__adjust_heapIPslsN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_, .-_ZSt13__adjust_heapIPslsN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	.section	.text._ZSt16__introsort_loopIPslN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZSt16__introsort_loopIPslN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0, @function
_ZSt16__introsort_loopIPslN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0:
.LFB26548:
	.cfi_startproc
	movq	%rsi, %rax
	subq	%rdi, %rax
	cmpq	$32, %rax
	jle	.L575
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	2(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L580
.L553:
	sarq	$2, %rax
	movzwl	2(%rbx), %edi
	movzwl	-2(%rsi), %edx
	subq	$1, %r15
	leaq	(%rbx,%rax,2), %r8
	movzwl	(%rbx), %ecx
	movzwl	(%r8), %eax
	cmpw	%ax, %di
	jge	.L556
	cmpw	%dx, %ax
	jl	.L561
	cmpw	%dx, %di
	jl	.L578
.L579:
	movw	%di, (%rbx)
	movw	%cx, 2(%rbx)
	movzwl	-2(%rsi), %ecx
.L558:
	movq	%r13, %r12
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L562:
	movzwl	(%r12), %edx
	movq	%r12, %r14
	cmpw	%di, %dx
	jl	.L563
	subq	$2, %rax
	cmpw	%cx, %di
	jge	.L564
	.p2align 4,,10
	.p2align 3
.L565:
	movzwl	-2(%rax), %ecx
	subq	$2, %rax
	cmpw	%di, %cx
	jg	.L565
.L564:
	cmpq	%rax, %r12
	jnb	.L581
	movw	%cx, (%r12)
	movzwl	-2(%rax), %ecx
	movw	%dx, (%rax)
	movzwl	(%rbx), %edi
.L563:
	addq	$2, %r12
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L556:
	cmpw	%dx, %di
	jl	.L579
	cmpw	%dx, %ax
	jge	.L561
.L578:
	movw	%dx, (%rbx)
	movw	%cx, -2(%rsi)
	movzwl	(%rbx), %edi
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L581:
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZSt16__introsort_loopIPslN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	movq	%r12, %rax
	subq	%rbx, %rax
	cmpq	$32, %rax
	jle	.L549
	testq	%r15, %r15
	je	.L551
	movq	%r12, %rsi
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L561:
	movw	%ax, (%rbx)
	movw	%cx, (%r8)
	movzwl	(%rbx), %edi
	movzwl	-2(%rsi), %ecx
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L580:
	movq	%rsi, %r14
.L551:
	sarq	%rax
	movq	%rbx, %rdi
	leaq	-2(%rax), %r13
	movq	%rax, %rdx
	movq	%rax, %r12
	sarq	%r13
	movswl	(%rbx,%r13,2), %ecx
	movq	%r13, %rsi
	call	_ZSt13__adjust_heapIPslsN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
.L554:
	subq	$1, %r13
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movswl	(%rbx,%r13,2), %ecx
	movq	%r13, %rsi
	call	_ZSt13__adjust_heapIPslsN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	testq	%r13, %r13
	jne	.L554
	.p2align 4,,10
	.p2align 3
.L555:
	subq	$2, %r14
	movzwl	(%rbx), %eax
	movswl	(%r14), %ecx
	xorl	%esi, %esi
	movq	%r14, %r12
	movq	%rbx, %rdi
	subq	%rbx, %r12
	movw	%ax, (%r14)
	movq	%r12, %rdx
	sarq	%rdx
	call	_ZSt13__adjust_heapIPslsN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	cmpq	$2, %r12
	jg	.L555
.L549:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L575:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE26548:
	.size	_ZSt16__introsort_loopIPslN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0, .-_ZSt16__introsort_loopIPslN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	.section	.text._ZSt13__adjust_heapIPjljN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_,"axG",@progbits,_ZSt13__adjust_heapIPjljN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_,comdat
	.p2align 4
	.weak	_ZSt13__adjust_heapIPjljN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	.type	_ZSt13__adjust_heapIPjljN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_, @function
_ZSt13__adjust_heapIPjljN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_:
.LFB26104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdx), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, %rbx
	andl	$1, %r12d
	shrq	$63, %rbx
	addq	%rax, %rbx
	sarq	%rbx
	cmpq	%rbx, %rsi
	jge	.L583
	movq	%rsi, %r10
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L596:
	subq	$1, %rax
	leaq	(%rdi,%rax,4), %r9
	movl	(%r9), %r8d
	movl	%r8d, (%rdi,%r10,4)
	cmpq	%rbx, %rax
	jge	.L585
.L586:
	movq	%rax, %r10
.L587:
	leaq	1(%r10), %r8
	leaq	(%r8,%r8), %rax
	salq	$3, %r8
	leaq	(%rdi,%r8), %r9
	movl	(%r9), %r11d
	cmpl	-4(%rdi,%r8), %r11d
	jb	.L596
	movl	%r11d, (%rdi,%r10,4)
	cmpq	%rbx, %rax
	jl	.L586
.L585:
	testq	%r12, %r12
	je	.L591
.L588:
	leaq	-1(%rax), %rdx
	movq	%rdx, %r8
	shrq	$63, %r8
	addq	%rdx, %r8
	sarq	%r8
	cmpq	%rsi, %rax
	jg	.L590
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L598:
	movl	%edx, (%r9)
	leaq	-1(%r8), %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	addq	%rdx, %rax
	sarq	%rax
	movq	%rax, %rdx
	movq	%r8, %rax
	cmpq	%r8, %rsi
	jge	.L597
	movq	%rdx, %r8
.L590:
	leaq	(%rdi,%r8,4), %r10
	leaq	(%rdi,%rax,4), %r9
	movl	(%r10), %edx
	cmpl	%edx, %ecx
	ja	.L598
.L589:
	movl	%ecx, (%r9)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L583:
	.cfi_restore_state
	leaq	(%rdi,%rsi,4), %r9
	testq	%r12, %r12
	jne	.L589
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L591:
	leaq	-2(%rdx), %r8
	movq	%r8, %rdx
	shrq	$63, %rdx
	addq	%r8, %rdx
	sarq	%rdx
	cmpq	%rax, %rdx
	jne	.L588
	leaq	2(%rax,%rax), %rax
	movl	-4(%rdi,%rax,4), %edx
	subq	$1, %rax
	movl	%edx, (%r9)
	leaq	(%rdi,%rax,4), %r9
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L597:
	movq	%r10, %r9
	movl	%ecx, (%r9)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26104:
	.size	_ZSt13__adjust_heapIPjljN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_, .-_ZSt13__adjust_heapIPjljN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	.section	.text._ZSt16__introsort_loopIPjlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZSt16__introsort_loopIPjlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0, @function
_ZSt16__introsort_loopIPjlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0:
.LFB26557:
	.cfi_startproc
	movq	%rsi, %rax
	subq	%rdi, %rax
	cmpq	$64, %rax
	jle	.L625
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	4(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L630
.L603:
	sarq	$3, %rax
	movl	4(%rbx), %edi
	movl	-4(%rsi), %edx
	subq	$1, %r15
	leaq	(%rbx,%rax,4), %r8
	movl	(%rbx), %ecx
	movl	(%r8), %eax
	cmpl	%eax, %edi
	jnb	.L606
	cmpl	%edx, %eax
	jb	.L611
	cmpl	%edx, %edi
	jb	.L628
.L629:
	movl	%edi, (%rbx)
	movl	%ecx, 4(%rbx)
	movl	-4(%rsi), %ecx
.L608:
	movq	%r13, %r12
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L612:
	movl	(%r12), %edx
	movq	%r12, %r14
	cmpl	%edi, %edx
	jb	.L613
	subq	$4, %rax
	cmpl	%ecx, %edi
	jnb	.L614
	.p2align 4,,10
	.p2align 3
.L615:
	movl	-4(%rax), %ecx
	subq	$4, %rax
	cmpl	%edi, %ecx
	ja	.L615
.L614:
	cmpq	%rax, %r12
	jnb	.L631
	movl	%ecx, (%r12)
	movl	-4(%rax), %ecx
	movl	%edx, (%rax)
	movl	(%rbx), %edi
.L613:
	addq	$4, %r12
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L606:
	cmpl	%edx, %edi
	jb	.L629
	cmpl	%edx, %eax
	jnb	.L611
.L628:
	movl	%edx, (%rbx)
	movl	%ecx, -4(%rsi)
	movl	(%rbx), %edi
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L631:
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZSt16__introsort_loopIPjlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	movq	%r12, %rax
	subq	%rbx, %rax
	cmpq	$64, %rax
	jle	.L599
	testq	%r15, %r15
	je	.L601
	movq	%r12, %rsi
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L611:
	movl	%eax, (%rbx)
	movl	%ecx, (%r8)
	movl	(%rbx), %edi
	movl	-4(%rsi), %ecx
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L630:
	movq	%rsi, %r14
.L601:
	sarq	$2, %rax
	movq	%rbx, %rdi
	leaq	-2(%rax), %r13
	movq	%rax, %rdx
	movq	%rax, %r12
	sarq	%r13
	movl	(%rbx,%r13,4), %ecx
	movq	%r13, %rsi
	call	_ZSt13__adjust_heapIPjljN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
.L604:
	subq	$1, %r13
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movl	(%rbx,%r13,4), %ecx
	movq	%r13, %rsi
	call	_ZSt13__adjust_heapIPjljN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	testq	%r13, %r13
	jne	.L604
	.p2align 4,,10
	.p2align 3
.L605:
	subq	$4, %r14
	movl	(%rbx), %eax
	movl	(%r14), %ecx
	xorl	%esi, %esi
	movq	%r14, %r12
	movq	%rbx, %rdi
	subq	%rbx, %r12
	movl	%eax, (%r14)
	movq	%r12, %rdx
	sarq	$2, %rdx
	call	_ZSt13__adjust_heapIPjljN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	cmpq	$4, %r12
	jg	.L605
.L599:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L625:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE26557:
	.size	_ZSt16__introsort_loopIPjlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0, .-_ZSt16__introsort_loopIPjlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	.section	.text._ZSt13__adjust_heapIPiliN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_,"axG",@progbits,_ZSt13__adjust_heapIPiliN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_,comdat
	.p2align 4
	.weak	_ZSt13__adjust_heapIPiliN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	.type	_ZSt13__adjust_heapIPiliN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_, @function
_ZSt13__adjust_heapIPiliN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_:
.LFB26116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdx), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, %rbx
	andl	$1, %r12d
	shrq	$63, %rbx
	addq	%rax, %rbx
	sarq	%rbx
	cmpq	%rbx, %rsi
	jge	.L633
	movq	%rsi, %r10
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L646:
	subq	$1, %rax
	leaq	(%rdi,%rax,4), %r9
	movl	(%r9), %r8d
	movl	%r8d, (%rdi,%r10,4)
	cmpq	%rbx, %rax
	jge	.L635
.L636:
	movq	%rax, %r10
.L637:
	leaq	1(%r10), %r8
	leaq	(%r8,%r8), %rax
	salq	$3, %r8
	leaq	(%rdi,%r8), %r9
	movl	(%r9), %r11d
	cmpl	-4(%rdi,%r8), %r11d
	jl	.L646
	movl	%r11d, (%rdi,%r10,4)
	cmpq	%rbx, %rax
	jl	.L636
.L635:
	testq	%r12, %r12
	je	.L641
.L638:
	leaq	-1(%rax), %rdx
	movq	%rdx, %r8
	shrq	$63, %r8
	addq	%rdx, %r8
	sarq	%r8
	cmpq	%rsi, %rax
	jg	.L640
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L648:
	movl	%edx, (%r9)
	leaq	-1(%r8), %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	addq	%rdx, %rax
	sarq	%rax
	movq	%rax, %rdx
	movq	%r8, %rax
	cmpq	%r8, %rsi
	jge	.L647
	movq	%rdx, %r8
.L640:
	leaq	(%rdi,%r8,4), %r10
	leaq	(%rdi,%rax,4), %r9
	movl	(%r10), %edx
	cmpl	%edx, %ecx
	jg	.L648
.L639:
	movl	%ecx, (%r9)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L633:
	.cfi_restore_state
	leaq	(%rdi,%rsi,4), %r9
	testq	%r12, %r12
	jne	.L639
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L641:
	leaq	-2(%rdx), %r8
	movq	%r8, %rdx
	shrq	$63, %rdx
	addq	%r8, %rdx
	sarq	%rdx
	cmpq	%rax, %rdx
	jne	.L638
	leaq	2(%rax,%rax), %rax
	movl	-4(%rdi,%rax,4), %edx
	subq	$1, %rax
	movl	%edx, (%r9)
	leaq	(%rdi,%rax,4), %r9
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L647:
	movq	%r10, %r9
	movl	%ecx, (%r9)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26116:
	.size	_ZSt13__adjust_heapIPiliN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_, .-_ZSt13__adjust_heapIPiliN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	.section	.text._ZSt16__introsort_loopIPilN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZSt16__introsort_loopIPilN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0, @function
_ZSt16__introsort_loopIPilN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0:
.LFB26566:
	.cfi_startproc
	movq	%rsi, %rax
	subq	%rdi, %rax
	cmpq	$64, %rax
	jle	.L675
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	4(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L680
.L653:
	sarq	$3, %rax
	movl	4(%rbx), %edi
	movl	-4(%rsi), %edx
	subq	$1, %r15
	leaq	(%rbx,%rax,4), %r8
	movl	(%rbx), %ecx
	movl	(%r8), %eax
	cmpl	%eax, %edi
	jge	.L656
	cmpl	%edx, %eax
	jl	.L661
	cmpl	%edx, %edi
	jl	.L678
.L679:
	movl	%edi, (%rbx)
	movl	%ecx, 4(%rbx)
	movl	-4(%rsi), %ecx
.L658:
	movq	%r13, %r12
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L662:
	movl	(%r12), %edx
	movq	%r12, %r14
	cmpl	%edi, %edx
	jl	.L663
	subq	$4, %rax
	cmpl	%ecx, %edi
	jge	.L664
	.p2align 4,,10
	.p2align 3
.L665:
	movl	-4(%rax), %ecx
	subq	$4, %rax
	cmpl	%edi, %ecx
	jg	.L665
.L664:
	cmpq	%rax, %r12
	jnb	.L681
	movl	%ecx, (%r12)
	movl	-4(%rax), %ecx
	movl	%edx, (%rax)
	movl	(%rbx), %edi
.L663:
	addq	$4, %r12
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L656:
	cmpl	%edx, %edi
	jl	.L679
	cmpl	%edx, %eax
	jge	.L661
.L678:
	movl	%edx, (%rbx)
	movl	%ecx, -4(%rsi)
	movl	(%rbx), %edi
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L681:
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZSt16__introsort_loopIPilN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	movq	%r12, %rax
	subq	%rbx, %rax
	cmpq	$64, %rax
	jle	.L649
	testq	%r15, %r15
	je	.L651
	movq	%r12, %rsi
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L661:
	movl	%eax, (%rbx)
	movl	%ecx, (%r8)
	movl	(%rbx), %edi
	movl	-4(%rsi), %ecx
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L680:
	movq	%rsi, %r14
.L651:
	sarq	$2, %rax
	movq	%rbx, %rdi
	leaq	-2(%rax), %r13
	movq	%rax, %rdx
	movq	%rax, %r12
	sarq	%r13
	movl	(%rbx,%r13,4), %ecx
	movq	%r13, %rsi
	call	_ZSt13__adjust_heapIPiliN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
.L654:
	subq	$1, %r13
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movl	(%rbx,%r13,4), %ecx
	movq	%r13, %rsi
	call	_ZSt13__adjust_heapIPiliN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	testq	%r13, %r13
	jne	.L654
	.p2align 4,,10
	.p2align 3
.L655:
	subq	$4, %r14
	movl	(%rbx), %eax
	movl	(%r14), %ecx
	xorl	%esi, %esi
	movq	%r14, %r12
	movq	%rbx, %rdi
	subq	%rbx, %r12
	movl	%eax, (%r14)
	movq	%r12, %rdx
	sarq	$2, %rdx
	call	_ZSt13__adjust_heapIPiliN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	cmpq	$4, %r12
	jg	.L655
.L649:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L675:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE26566:
	.size	_ZSt16__introsort_loopIPilN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0, .-_ZSt16__introsort_loopIPilN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	.section	.text._ZSt13__adjust_heapIPflfN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_T0_S8_T1_T2_,"axG",@progbits,_ZSt13__adjust_heapIPflfN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_T0_S8_T1_T2_,comdat
	.p2align 4
	.weak	_ZSt13__adjust_heapIPflfN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_T0_S8_T1_T2_
	.type	_ZSt13__adjust_heapIPflfN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_T0_S8_T1_T2_, @function
_ZSt13__adjust_heapIPflfN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_T0_S8_T1_T2_:
.LFB26122:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdx), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rax, %r13
	pushq	%r12
	shrq	$63, %r13
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	addq	%rax, %r13
	andl	$1, %edi
	sarq	%r13
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -72(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rcx, -56(%rbp)
	movq	%rdi, -80(%rbp)
	movss	%xmm0, -60(%rbp)
	cmpq	%r13, %rsi
	jge	.L683
	movq	%rsi, %r14
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L700:
	subq	$1, %rbx
	leaq	(%r12,%rbx,4), %r15
	movss	(%r15), %xmm0
	movss	%xmm0, (%r12,%r14,4)
	cmpq	%r13, %rbx
	jge	.L685
.L699:
	movq	%rbx, %r14
.L687:
	leaq	1(%r14), %rax
	leaq	(%rax,%rax), %rbx
	salq	$3, %rax
	leaq	(%r12,%rax), %r15
	movss	-4(%r12,%rax), %xmm1
	movq	-56(%rbp), %rax
	movss	(%r15), %xmm0
	call	*%rax
	testb	%al, %al
	jne	.L700
	movss	(%r15), %xmm0
	movss	%xmm0, (%r12,%r14,4)
	cmpq	%r13, %rbx
	jl	.L699
.L685:
	cmpq	$0, -80(%rbp)
	je	.L691
.L688:
	leaq	-1(%rbx), %rax
	movq	%rax, %r13
	shrq	$63, %r13
	addq	%rax, %r13
	sarq	%r13
	cmpq	-72(%rbp), %rbx
	jg	.L690
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L702:
	leaq	-1(%r13), %rdx
	movss	(%r14), %xmm0
	movq	%r13, %rbx
	movq	%rdx, %rax
	shrq	$63, %rax
	movss	%xmm0, (%r15)
	addq	%rdx, %rax
	sarq	%rax
	cmpq	%r13, -72(%rbp)
	jge	.L701
	movq	%rax, %r13
.L690:
	leaq	(%r12,%r13,4), %r14
	movss	-60(%rbp), %xmm1
	movq	-56(%rbp), %rax
	leaq	(%r12,%rbx,4), %r15
	movss	(%r14), %xmm0
	call	*%rax
	testb	%al, %al
	jne	.L702
.L689:
	movss	-60(%rbp), %xmm2
	movss	%xmm2, (%r15)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L683:
	.cfi_restore_state
	cmpq	$0, -80(%rbp)
	movq	%rsi, %rax
	leaq	(%r12,%rsi,4), %r15
	jne	.L689
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L691:
	movq	-88(%rbp), %rdx
	subq	$2, %rdx
	movq	%rdx, %r14
	shrq	$63, %r14
	addq	%rdx, %r14
	sarq	%r14
	cmpq	%rbx, %r14
	jne	.L688
	leaq	2(%rbx,%rbx), %rcx
	movss	-4(%r12,%rcx,4), %xmm0
	leaq	-1(%rcx), %rbx
	movss	%xmm0, (%r15)
	leaq	(%r12,%rbx,4), %r15
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L701:
	movq	%r14, %r15
	jmp	.L689
	.cfi_endproc
.LFE26122:
	.size	_ZSt13__adjust_heapIPflfN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_T0_S8_T1_T2_, .-_ZSt13__adjust_heapIPflfN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_T0_S8_T1_T2_
	.section	.text._ZSt16__introsort_loopIPflN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_S7_T0_T1_,"axG",@progbits,_ZSt16__introsort_loopIPflN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_S7_T0_T1_,comdat
	.p2align 4
	.weak	_ZSt16__introsort_loopIPflN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_S7_T0_T1_
	.type	_ZSt16__introsort_loopIPflN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_S7_T0_T1_, @function
_ZSt16__introsort_loopIPflN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_S7_T0_T1_:
.LFB24444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	subq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -56(%rbp)
	cmpq	$64, %rax
	jle	.L703
	movq	%rdi, %r15
	movq	%rsi, %r13
	movq	%rcx, %rbx
	testq	%rdx, %rdx
	je	.L733
	leaq	4(%rdi), %rdi
	movq	%rdi, -72(%rbp)
.L707:
	sarq	$3, %rax
	subq	$1, -56(%rbp)
	movss	4(%r15), %xmm0
	leaq	(%r15,%rax,4), %r12
	movss	(%r12), %xmm1
	call	*%rbx
	testb	%al, %al
	je	.L712
	movss	-4(%r13), %xmm1
	movss	(%r12), %xmm0
	call	*%rbx
	testb	%al, %al
	je	.L713
	movss	(%r12), %xmm1
	movss	(%r15), %xmm0
	movss	%xmm1, (%r15)
	movss	%xmm0, (%r12)
	movss	(%r15), %xmm1
.L714:
	movq	-72(%rbp), %r14
	movq	%r13, %rdx
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L719:
	movss	(%r15), %xmm1
	addq	$4, %r14
.L718:
	movq	%rdx, -64(%rbp)
	movss	(%r14), %xmm0
	movq	%r14, %r12
	call	*%rbx
	movq	-64(%rbp), %rdx
	testb	%al, %al
	jne	.L719
	.p2align 4,,10
	.p2align 3
.L732:
	subq	$4, %rdx
	movss	(%rdx), %xmm1
	movss	(%r15), %xmm0
	movq	%rdx, -64(%rbp)
	call	*%rbx
	movq	-64(%rbp), %rdx
	testb	%al, %al
	jne	.L732
	cmpq	%rdx, %r14
	jnb	.L734
	movss	(%r14), %xmm0
	movss	(%rdx), %xmm1
	movss	%xmm1, (%r14)
	movss	%xmm0, (%rdx)
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L734:
	movq	-56(%rbp), %rdx
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZSt16__introsort_loopIPflN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_S7_T0_T1_
	movq	%r14, %rax
	subq	%r15, %rax
	cmpq	$64, %rax
	jle	.L703
	cmpq	$0, -56(%rbp)
	je	.L705
	movq	%r14, %r13
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L712:
	movss	4(%r15), %xmm0
	movss	-4(%r13), %xmm1
	call	*%rbx
	testb	%al, %al
	je	.L716
	movss	(%r15), %xmm0
.L731:
	movss	4(%r15), %xmm1
	movss	%xmm0, 4(%r15)
	movss	%xmm1, (%r15)
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L733:
	movq	%rsi, %r12
.L705:
	sarq	$2, %rax
	movq	%rbx, %rcx
	movq	%r15, %rdi
	leaq	-2(%rax), %r14
	movq	%rax, %rdx
	movq	%rax, %r13
	sarq	%r14
	movss	(%r15,%r14,4), %xmm0
	movq	%r14, %rsi
	call	_ZSt13__adjust_heapIPflfN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_T0_S8_T1_T2_
.L708:
	subq	$1, %r14
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
	movss	(%r15,%r14,4), %xmm0
	movq	%r14, %rsi
	call	_ZSt13__adjust_heapIPflfN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_T0_S8_T1_T2_
	testq	%r14, %r14
	jne	.L708
	.p2align 4,,10
	.p2align 3
.L709:
	subq	$4, %r12
	movss	(%r15), %xmm1
	xorl	%esi, %esi
	movq	%rbx, %rcx
	movq	%r12, %r13
	movss	(%r12), %xmm0
	movq	%r15, %rdi
	subq	%r15, %r13
	movss	%xmm1, (%r12)
	movq	%r13, %rdx
	sarq	$2, %rdx
	call	_ZSt13__adjust_heapIPflfN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_T0_S8_T1_T2_
	cmpq	$4, %r13
	jg	.L709
.L703:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L713:
	.cfi_restore_state
	movss	4(%r15), %xmm0
	movss	-4(%r13), %xmm1
	call	*%rbx
	movss	(%r15), %xmm0
	testb	%al, %al
	je	.L731
.L730:
	movss	-4(%r13), %xmm1
	movss	%xmm1, (%r15)
	movss	%xmm0, -4(%r13)
	movss	(%r15), %xmm1
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L716:
	movss	(%r12), %xmm0
	movss	-4(%r13), %xmm1
	call	*%rbx
	movss	(%r15), %xmm0
	testb	%al, %al
	jne	.L730
	movss	(%r12), %xmm1
	movss	%xmm1, (%r15)
	movss	%xmm0, (%r12)
	movss	(%r15), %xmm1
	jmp	.L714
	.cfi_endproc
.LFE24444:
	.size	_ZSt16__introsort_loopIPflN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_S7_T0_T1_, .-_ZSt16__introsort_loopIPflN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_S7_T0_T1_
	.section	.text._ZSt13__adjust_heapIPdldN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_T0_S8_T1_T2_,"axG",@progbits,_ZSt13__adjust_heapIPdldN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_T0_S8_T1_T2_,comdat
	.p2align 4
	.weak	_ZSt13__adjust_heapIPdldN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_T0_S8_T1_T2_
	.type	_ZSt13__adjust_heapIPdldN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_T0_S8_T1_T2_, @function
_ZSt13__adjust_heapIPdldN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_T0_S8_T1_T2_:
.LFB26134:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdx), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rax, %r13
	pushq	%r12
	shrq	$63, %r13
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	addq	%rax, %r13
	andl	$1, %edi
	sarq	%r13
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -72(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rcx, -56(%rbp)
	movq	%rdi, -80(%rbp)
	movsd	%xmm0, -64(%rbp)
	cmpq	%r13, %rsi
	jge	.L736
	movq	%rsi, %r14
	jmp	.L740
	.p2align 4,,10
	.p2align 3
.L753:
	subq	$1, %rbx
	leaq	(%r12,%rbx,8), %r15
	movsd	(%r15), %xmm0
	movsd	%xmm0, (%r12,%r14,8)
	cmpq	%r13, %rbx
	jge	.L738
.L752:
	movq	%rbx, %r14
.L740:
	leaq	1(%r14), %rax
	leaq	(%rax,%rax), %rbx
	salq	$4, %rax
	leaq	(%r12,%rax), %r15
	movsd	-8(%r12,%rax), %xmm1
	movq	-56(%rbp), %rax
	movsd	(%r15), %xmm0
	call	*%rax
	testb	%al, %al
	jne	.L753
	movsd	(%r15), %xmm0
	movsd	%xmm0, (%r12,%r14,8)
	cmpq	%r13, %rbx
	jl	.L752
.L738:
	cmpq	$0, -80(%rbp)
	je	.L744
.L741:
	leaq	-1(%rbx), %rax
	movq	%rax, %r13
	shrq	$63, %r13
	addq	%rax, %r13
	sarq	%r13
	cmpq	-72(%rbp), %rbx
	jg	.L743
	jmp	.L742
	.p2align 4,,10
	.p2align 3
.L755:
	leaq	-1(%r13), %rdx
	movsd	(%r14), %xmm0
	movq	%r13, %rbx
	movq	%rdx, %rax
	shrq	$63, %rax
	movsd	%xmm0, (%r15)
	addq	%rdx, %rax
	sarq	%rax
	cmpq	%r13, -72(%rbp)
	jge	.L754
	movq	%rax, %r13
.L743:
	leaq	(%r12,%r13,8), %r14
	movsd	-64(%rbp), %xmm1
	movq	-56(%rbp), %rax
	leaq	(%r12,%rbx,8), %r15
	movsd	(%r14), %xmm0
	call	*%rax
	testb	%al, %al
	jne	.L755
.L742:
	movsd	-64(%rbp), %xmm2
	movsd	%xmm2, (%r15)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L736:
	.cfi_restore_state
	cmpq	$0, -80(%rbp)
	movq	%rsi, %rax
	leaq	(%r12,%rsi,8), %r15
	jne	.L742
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L744:
	movq	-88(%rbp), %rdx
	subq	$2, %rdx
	movq	%rdx, %r14
	shrq	$63, %r14
	addq	%rdx, %r14
	sarq	%r14
	cmpq	%rbx, %r14
	jne	.L741
	leaq	2(%rbx,%rbx), %rcx
	movsd	-8(%r12,%rcx,8), %xmm0
	leaq	-1(%rcx), %rbx
	movsd	%xmm0, (%r15)
	leaq	(%r12,%rbx,8), %r15
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L754:
	movq	%r14, %r15
	jmp	.L742
	.cfi_endproc
.LFE26134:
	.size	_ZSt13__adjust_heapIPdldN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_T0_S8_T1_T2_, .-_ZSt13__adjust_heapIPdldN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_T0_S8_T1_T2_
	.section	.text._ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_T1_,"axG",@progbits,_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_T1_,comdat
	.p2align 4
	.weak	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_T1_
	.type	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_T1_, @function
_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_T1_:
.LFB24454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	subq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -56(%rbp)
	cmpq	$128, %rax
	jle	.L756
	movq	%rdi, %r15
	movq	%rsi, %r13
	movq	%rcx, %rbx
	testq	%rdx, %rdx
	je	.L784
	leaq	8(%rdi), %rdi
	movq	%rdi, -72(%rbp)
.L760:
	sarq	$4, %rax
	subq	$1, -56(%rbp)
	movsd	8(%r15), %xmm0
	leaq	(%r15,%rax,8), %r12
	movsd	(%r12), %xmm1
	call	*%rbx
	testb	%al, %al
	je	.L765
	movsd	-8(%r13), %xmm1
	movsd	(%r12), %xmm0
	call	*%rbx
	testb	%al, %al
	je	.L766
	movsd	(%r12), %xmm1
	movsd	(%r15), %xmm0
	movsd	%xmm1, (%r15)
	movsd	%xmm0, (%r12)
	movsd	(%r15), %xmm1
.L767:
	movq	-72(%rbp), %r14
	movq	%r13, %rdx
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L772:
	movsd	(%r15), %xmm1
	addq	$8, %r14
.L771:
	movq	%rdx, -64(%rbp)
	movsd	(%r14), %xmm0
	movq	%r14, %r12
	call	*%rbx
	movq	-64(%rbp), %rdx
	testb	%al, %al
	jne	.L772
	.p2align 4,,10
	.p2align 3
.L783:
	subq	$8, %rdx
	movsd	(%rdx), %xmm1
	movsd	(%r15), %xmm0
	movq	%rdx, -64(%rbp)
	call	*%rbx
	movq	-64(%rbp), %rdx
	testb	%al, %al
	jne	.L783
	cmpq	%rdx, %r14
	jnb	.L785
	movsd	(%r14), %xmm0
	movsd	(%rdx), %xmm1
	movsd	%xmm1, (%r14)
	movsd	%xmm0, (%rdx)
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L785:
	movq	-56(%rbp), %rdx
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_T1_
	movq	%r14, %rax
	subq	%r15, %rax
	cmpq	$128, %rax
	jle	.L756
	cmpq	$0, -56(%rbp)
	je	.L758
	movq	%r14, %r13
	jmp	.L760
	.p2align 4,,10
	.p2align 3
.L765:
	movsd	8(%r15), %xmm0
	movsd	-8(%r13), %xmm1
	call	*%rbx
	testb	%al, %al
	je	.L769
	movupd	(%r15), %xmm0
	movsd	8(%r15), %xmm1
	shufpd	$1, %xmm0, %xmm0
	movups	%xmm0, (%r15)
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L784:
	movq	%rsi, %r12
.L758:
	sarq	$3, %rax
	movq	%rbx, %rcx
	movq	%r15, %rdi
	leaq	-2(%rax), %r14
	movq	%rax, %rdx
	movq	%rax, %r13
	sarq	%r14
	movsd	(%r15,%r14,8), %xmm0
	movq	%r14, %rsi
	call	_ZSt13__adjust_heapIPdldN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_T0_S8_T1_T2_
.L761:
	subq	$1, %r14
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
	movsd	(%r15,%r14,8), %xmm0
	movq	%r14, %rsi
	call	_ZSt13__adjust_heapIPdldN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_T0_S8_T1_T2_
	testq	%r14, %r14
	jne	.L761
	.p2align 4,,10
	.p2align 3
.L762:
	subq	$8, %r12
	movsd	(%r15), %xmm1
	xorl	%esi, %esi
	movq	%rbx, %rcx
	movq	%r12, %r13
	movsd	(%r12), %xmm0
	movq	%r15, %rdi
	subq	%r15, %r13
	movsd	%xmm1, (%r12)
	movq	%r13, %rdx
	sarq	$3, %rdx
	call	_ZSt13__adjust_heapIPdldN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_T0_S8_T1_T2_
	cmpq	$8, %r13
	jg	.L762
.L756:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L766:
	.cfi_restore_state
	movsd	8(%r15), %xmm0
	movsd	-8(%r13), %xmm1
	call	*%rbx
	testb	%al, %al
	je	.L768
	movsd	-8(%r13), %xmm1
	movsd	(%r15), %xmm0
	movsd	%xmm1, (%r15)
	movsd	%xmm0, -8(%r13)
	movsd	(%r15), %xmm1
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L769:
	movsd	(%r12), %xmm0
	movsd	-8(%r13), %xmm1
	call	*%rbx
	movsd	(%r15), %xmm0
	testb	%al, %al
	je	.L770
	movsd	-8(%r13), %xmm1
	movsd	%xmm1, (%r15)
	movsd	%xmm0, -8(%r13)
	movsd	(%r15), %xmm1
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L770:
	movsd	(%r12), %xmm1
	movsd	%xmm1, (%r15)
	movsd	%xmm0, (%r12)
	movsd	(%r15), %xmm1
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L768:
	movupd	(%r15), %xmm0
	movsd	8(%r15), %xmm1
	shufpd	$1, %xmm0, %xmm0
	movups	%xmm0, (%r15)
	jmp	.L767
	.cfi_endproc
.LFE24454:
	.size	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_T1_, .-_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_T1_
	.section	.text._ZSt13__adjust_heapIPmlmN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_,"axG",@progbits,_ZSt13__adjust_heapIPmlmN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_,comdat
	.p2align 4
	.weak	_ZSt13__adjust_heapIPmlmN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	.type	_ZSt13__adjust_heapIPmlmN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_, @function
_ZSt13__adjust_heapIPmlmN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_:
.LFB26151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdx), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, %rbx
	andl	$1, %r12d
	shrq	$63, %rbx
	addq	%rax, %rbx
	sarq	%rbx
	cmpq	%rbx, %rsi
	jge	.L787
	movq	%rsi, %r10
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L800:
	subq	$1, %rax
	leaq	(%rdi,%rax,8), %r9
	movq	(%r9), %r8
	movq	%r8, (%rdi,%r10,8)
	cmpq	%rbx, %rax
	jge	.L789
.L790:
	movq	%rax, %r10
.L791:
	leaq	1(%r10), %r8
	leaq	(%r8,%r8), %rax
	salq	$4, %r8
	leaq	(%rdi,%r8), %r9
	movq	(%r9), %r11
	cmpq	-8(%rdi,%r8), %r11
	jb	.L800
	movq	%r11, (%rdi,%r10,8)
	cmpq	%rbx, %rax
	jl	.L790
.L789:
	testq	%r12, %r12
	je	.L795
.L792:
	leaq	-1(%rax), %rdx
	movq	%rdx, %r8
	shrq	$63, %r8
	addq	%rdx, %r8
	sarq	%r8
	cmpq	%rsi, %rax
	jg	.L794
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L802:
	movq	%rdx, (%r9)
	leaq	-1(%r8), %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	addq	%rdx, %rax
	sarq	%rax
	movq	%rax, %rdx
	movq	%r8, %rax
	cmpq	%r8, %rsi
	jge	.L801
	movq	%rdx, %r8
.L794:
	leaq	(%rdi,%r8,8), %r10
	leaq	(%rdi,%rax,8), %r9
	movq	(%r10), %rdx
	cmpq	%rdx, %rcx
	ja	.L802
.L793:
	movq	%rcx, (%r9)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L787:
	.cfi_restore_state
	leaq	(%rdi,%rsi,8), %r9
	testq	%r12, %r12
	jne	.L793
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L795:
	leaq	-2(%rdx), %r8
	movq	%r8, %rdx
	shrq	$63, %rdx
	addq	%r8, %rdx
	sarq	%rdx
	cmpq	%rax, %rdx
	jne	.L792
	leaq	2(%rax,%rax), %rax
	movq	-8(%rdi,%rax,8), %rdx
	subq	$1, %rax
	movq	%rdx, (%r9)
	leaq	(%rdi,%rax,8), %r9
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L801:
	movq	%r10, %r9
	movq	%rcx, (%r9)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26151:
	.size	_ZSt13__adjust_heapIPmlmN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_, .-_ZSt13__adjust_heapIPmlmN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	.section	.text._ZSt16__introsort_loopIPmlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZSt16__introsort_loopIPmlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0, @function
_ZSt16__introsort_loopIPmlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0:
.LFB26583:
	.cfi_startproc
	movq	%rsi, %rax
	subq	%rdi, %rax
	cmpq	$128, %rax
	jle	.L829
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	8(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L833
.L807:
	sarq	$4, %rax
	movq	8(%rbx), %rdi
	movq	-8(%rsi), %rdx
	subq	$1, %r15
	leaq	(%rbx,%rax,8), %r8
	movq	(%rbx), %rcx
	movq	(%r8), %rax
	cmpq	%rax, %rdi
	jnb	.L810
	cmpq	%rdx, %rax
	jb	.L815
	cmpq	%rdx, %rdi
	jnb	.L813
.L832:
	movq	%rdx, (%rbx)
	movq	%rcx, -8(%rsi)
	movq	(%rbx), %rdi
.L812:
	movq	%r13, %r12
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L816:
	movq	(%r12), %rdx
	movq	%r12, %r14
	cmpq	%rdi, %rdx
	jb	.L817
	subq	$8, %rax
	cmpq	%rcx, %rdi
	jnb	.L818
	.p2align 4,,10
	.p2align 3
.L819:
	movq	-8(%rax), %rcx
	subq	$8, %rax
	cmpq	%rdi, %rcx
	ja	.L819
.L818:
	cmpq	%rax, %r12
	jnb	.L834
	movq	%rcx, (%r12)
	movq	-8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	(%rbx), %rdi
.L817:
	addq	$8, %r12
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L834:
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZSt16__introsort_loopIPmlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	movq	%r12, %rax
	subq	%rbx, %rax
	cmpq	$128, %rax
	jle	.L803
	testq	%r15, %r15
	je	.L805
	movq	%r12, %rsi
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L810:
	cmpq	%rdx, %rdi
	jnb	.L814
	movq	%rcx, %xmm1
	movq	%rdi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	movq	-8(%rsi), %rcx
	jmp	.L812
	.p2align 4,,10
	.p2align 3
.L814:
	cmpq	%rdx, %rax
	jb	.L832
.L815:
	movq	%rax, (%rbx)
	movq	%rcx, (%r8)
	movq	(%rbx), %rdi
	movq	-8(%rsi), %rcx
	jmp	.L812
	.p2align 4,,10
	.p2align 3
.L833:
	movq	%rsi, %r14
.L805:
	sarq	$3, %rax
	movq	%rbx, %rdi
	leaq	-2(%rax), %r13
	movq	%rax, %rdx
	movq	%rax, %r12
	sarq	%r13
	movq	(%rbx,%r13,8), %rcx
	movq	%r13, %rsi
	call	_ZSt13__adjust_heapIPmlmN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
.L808:
	subq	$1, %r13
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movq	(%rbx,%r13,8), %rcx
	movq	%r13, %rsi
	call	_ZSt13__adjust_heapIPmlmN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	testq	%r13, %r13
	jne	.L808
	.p2align 4,,10
	.p2align 3
.L809:
	subq	$8, %r14
	movq	(%rbx), %rax
	movq	(%r14), %rcx
	xorl	%esi, %esi
	movq	%r14, %r12
	movq	%rbx, %rdi
	subq	%rbx, %r12
	movq	%rax, (%r14)
	movq	%r12, %rdx
	sarq	$3, %rdx
	call	_ZSt13__adjust_heapIPmlmN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	cmpq	$8, %r12
	jg	.L809
.L803:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L813:
	.cfi_restore_state
	movq	%rcx, %xmm2
	movq	%rdi, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rbx)
	movq	-8(%rsi), %rcx
	jmp	.L812
	.p2align 4,,10
	.p2align 3
.L829:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE26583:
	.size	_ZSt16__introsort_loopIPmlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0, .-_ZSt16__introsort_loopIPmlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	.section	.text._ZSt13__adjust_heapIPlllN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_,"axG",@progbits,_ZSt13__adjust_heapIPlllN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_,comdat
	.p2align 4
	.weak	_ZSt13__adjust_heapIPlllN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	.type	_ZSt13__adjust_heapIPlllN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_, @function
_ZSt13__adjust_heapIPlllN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_:
.LFB26163:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdx), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, %rbx
	andl	$1, %r12d
	shrq	$63, %rbx
	addq	%rax, %rbx
	sarq	%rbx
	cmpq	%rbx, %rsi
	jge	.L836
	movq	%rsi, %r10
	jmp	.L840
	.p2align 4,,10
	.p2align 3
.L849:
	subq	$1, %rax
	leaq	(%rdi,%rax,8), %r9
	movq	(%r9), %r8
	movq	%r8, (%rdi,%r10,8)
	cmpq	%rbx, %rax
	jge	.L838
.L839:
	movq	%rax, %r10
.L840:
	leaq	1(%r10), %r8
	leaq	(%r8,%r8), %rax
	salq	$4, %r8
	leaq	(%rdi,%r8), %r9
	movq	(%r9), %r11
	cmpq	-8(%rdi,%r8), %r11
	jl	.L849
	movq	%r11, (%rdi,%r10,8)
	cmpq	%rbx, %rax
	jl	.L839
.L838:
	testq	%r12, %r12
	je	.L844
.L841:
	leaq	-1(%rax), %rdx
	movq	%rdx, %r8
	shrq	$63, %r8
	addq	%rdx, %r8
	sarq	%r8
	cmpq	%rsi, %rax
	jg	.L843
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L851:
	movq	%rdx, (%r9)
	leaq	-1(%r8), %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	addq	%rdx, %rax
	sarq	%rax
	movq	%rax, %rdx
	movq	%r8, %rax
	cmpq	%r8, %rsi
	jge	.L850
	movq	%rdx, %r8
.L843:
	leaq	(%rdi,%r8,8), %r10
	leaq	(%rdi,%rax,8), %r9
	movq	(%r10), %rdx
	cmpq	%rdx, %rcx
	jg	.L851
.L842:
	movq	%rcx, (%r9)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L836:
	.cfi_restore_state
	leaq	(%rdi,%rsi,8), %r9
	testq	%r12, %r12
	jne	.L842
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L844:
	leaq	-2(%rdx), %r8
	movq	%r8, %rdx
	shrq	$63, %rdx
	addq	%r8, %rdx
	sarq	%rdx
	cmpq	%rax, %rdx
	jne	.L841
	leaq	2(%rax,%rax), %rax
	movq	-8(%rdi,%rax,8), %rdx
	subq	$1, %rax
	movq	%rdx, (%r9)
	leaq	(%rdi,%rax,8), %r9
	jmp	.L841
	.p2align 4,,10
	.p2align 3
.L850:
	movq	%r10, %r9
	movq	%rcx, (%r9)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26163:
	.size	_ZSt13__adjust_heapIPlllN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_, .-_ZSt13__adjust_heapIPlllN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	.section	.text._ZSt16__introsort_loopIPllN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZSt16__introsort_loopIPllN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0, @function
_ZSt16__introsort_loopIPllN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0:
.LFB26592:
	.cfi_startproc
	movq	%rsi, %rax
	subq	%rdi, %rax
	cmpq	$128, %rax
	jle	.L878
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	8(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L882
.L856:
	sarq	$4, %rax
	movq	8(%rbx), %rdi
	movq	-8(%rsi), %rdx
	subq	$1, %r15
	leaq	(%rbx,%rax,8), %r8
	movq	(%rbx), %rcx
	movq	(%r8), %rax
	cmpq	%rax, %rdi
	jge	.L859
	cmpq	%rdx, %rax
	jl	.L864
	cmpq	%rdx, %rdi
	jge	.L862
.L881:
	movq	%rdx, (%rbx)
	movq	%rcx, -8(%rsi)
	movq	(%rbx), %rdi
.L861:
	movq	%r13, %r12
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L865:
	movq	(%r12), %rdx
	movq	%r12, %r14
	cmpq	%rdi, %rdx
	jl	.L866
	subq	$8, %rax
	cmpq	%rcx, %rdi
	jge	.L867
	.p2align 4,,10
	.p2align 3
.L868:
	movq	-8(%rax), %rcx
	subq	$8, %rax
	cmpq	%rdi, %rcx
	jg	.L868
.L867:
	cmpq	%rax, %r12
	jnb	.L883
	movq	%rcx, (%r12)
	movq	-8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	(%rbx), %rdi
.L866:
	addq	$8, %r12
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L883:
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZSt16__introsort_loopIPllN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	movq	%r12, %rax
	subq	%rbx, %rax
	cmpq	$128, %rax
	jle	.L852
	testq	%r15, %r15
	je	.L854
	movq	%r12, %rsi
	jmp	.L856
	.p2align 4,,10
	.p2align 3
.L859:
	cmpq	%rdx, %rdi
	jge	.L863
	movq	%rcx, %xmm1
	movq	%rdi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	movq	-8(%rsi), %rcx
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L863:
	cmpq	%rdx, %rax
	jl	.L881
.L864:
	movq	%rax, (%rbx)
	movq	%rcx, (%r8)
	movq	(%rbx), %rdi
	movq	-8(%rsi), %rcx
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L882:
	movq	%rsi, %r14
.L854:
	sarq	$3, %rax
	movq	%rbx, %rdi
	leaq	-2(%rax), %r13
	movq	%rax, %rdx
	movq	%rax, %r12
	sarq	%r13
	movq	(%rbx,%r13,8), %rcx
	movq	%r13, %rsi
	call	_ZSt13__adjust_heapIPlllN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
.L857:
	subq	$1, %r13
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movq	(%rbx,%r13,8), %rcx
	movq	%r13, %rsi
	call	_ZSt13__adjust_heapIPlllN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	testq	%r13, %r13
	jne	.L857
	.p2align 4,,10
	.p2align 3
.L858:
	subq	$8, %r14
	movq	(%rbx), %rax
	movq	(%r14), %rcx
	xorl	%esi, %esi
	movq	%r14, %r12
	movq	%rbx, %rdi
	subq	%rbx, %r12
	movq	%rax, (%r14)
	movq	%r12, %rdx
	sarq	$3, %rdx
	call	_ZSt13__adjust_heapIPlllN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	cmpq	$8, %r12
	jg	.L858
.L852:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L862:
	.cfi_restore_state
	movq	%rcx, %xmm2
	movq	%rdi, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rbx)
	movq	-8(%rsi), %rcx
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L878:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE26592:
	.size	_ZSt16__introsort_loopIPllN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0, .-_ZSt16__introsort_loopIPllN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	.section	.rodata._ZN2v88internalL32Stats_Runtime_TypedArraySortFastEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"V8.Runtime_Runtime_TypedArraySortFast"
	.align 8
.LC18:
	.string	"array->buffer().IsJSArrayBuffer()"
	.section	.rodata._ZN2v88internalL32Stats_Runtime_TypedArraySortFastEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC19:
	.string	"bytes <= 0x7fffffff"
	.section	.text._ZN2v88internalL32Stats_Runtime_TypedArraySortFastEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_TypedArraySortFastEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL32Stats_Runtime_TypedArraySortFastEiPmPNS0_7IsolateE.isra.0:
.LFB26455:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1251
.L885:
	movq	_ZZN2v88internalL32Stats_Runtime_TypedArraySortFastEiPmPNS0_7IsolateEE27trace_event_unique_atomic94(%rip), %r12
	testq	%r12, %r12
	je	.L1252
.L887:
	movq	$0, -160(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L1253
.L889:
	movq	41096(%r13), %rax
	addl	$1, 41104(%r13)
	movq	41088(%r13), %r12
	movq	%rax, -184(%rbp)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L1254
.L893:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1252:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1255
.L888:
	movq	%r12, _ZZN2v88internalL32Stats_Runtime_TypedArraySortFastEiPmPNS0_7IsolateEE27trace_event_unique_atomic94(%rip)
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L1254:
	movq	-1(%rax), %rdx
	cmpw	$1086, 11(%rdx)
	jne	.L893
	movq	47(%rax), %r14
	movq	%rax, %r9
	cmpq	$1, %r14
	jbe	.L894
	movq	23(%rax), %r15
	testb	$1, %r15b
	jne	.L1256
.L896:
	leaq	.LC18(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1150:
	movq	$0, -200(%rbp)
.L1042:
	movq	(%rbx), %rax
	movq	55(%rax), %rdi
	addq	63(%rax), %rdi
.L1043:
	salq	$3, %r14
	leaq	(%rdi,%r14), %r15
	cmpq	%r15, %rdi
	je	.L927
	movq	%r14, %rax
	movl	$63, %edx
	movq	%r15, %rsi
	movq	%rdi, -208(%rbp)
	sarq	$3, %rax
	leaq	_ZN2v88internal12_GLOBAL__N_110CompareNumIdEEbT_S3_(%rip), %rcx
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_T1_
	cmpq	$128, %r14
	movq	-208(%rbp), %rdi
	jle	.L1045
	leaq	128(%rdi), %r14
	leaq	_ZN2v88internal12_GLOBAL__N_110CompareNumIdEEbT_S3_(%rip), %rdx
	movq	%r14, %rsi
	call	_ZSt16__insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_
	cmpq	%r14, %r15
	je	.L927
	.p2align 4,,10
	.p2align 3
.L1046:
	movsd	(%r14), %xmm0
	movq	%r14, %rcx
	jmp	.L1049
	.p2align 4,,10
	.p2align 3
.L1257:
	movsd	%xmm1, (%rcx)
	subq	$8, %rcx
.L1049:
	movsd	-8(%rcx), %xmm1
	call	_ZN2v88internal12_GLOBAL__N_110CompareNumIdEEbT_S3_
	testb	%al, %al
	jne	.L1257
	addq	$8, %r14
	movsd	%xmm0, (%rcx)
	cmpq	%r14, %r15
	jne	.L1046
	.p2align 4,,10
	.p2align 3
.L927:
	movl	-192(%rbp), %eax
	movq	(%rbx), %r9
	testl	%eax, %eax
	je	.L894
	movq	-200(%rbp), %rax
	movq	(%rax), %r15
	addq	$15, %r15
.L916:
	movq	39(%r9), %rdx
	movq	55(%r9), %rdi
	movq	%r15, %rsi
	addq	63(%r9), %rdi
	call	memcpy@PLT
	movq	(%rbx), %r9
	.p2align 4,,10
	.p2align 3
.L894:
	subl	$1, 41104(%r13)
	movq	-184(%rbp), %rax
	movq	%r12, 41088(%r13)
	cmpq	41096(%r13), %rax
	je	.L1110
	movq	%rax, 41096(%r13)
	movq	%r13, %rdi
	movq	%r9, -192(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-192(%rbp), %r9
.L1110:
	leaq	-160(%rbp), %rdi
	movq	%r9, -184(%rbp)
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	movq	-184(%rbp), %r9
	testq	%rdi, %rdi
	jne	.L1258
.L884:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1259
	leaq	-40(%rbp), %rsp
	movq	%r9, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1253:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1260
.L890:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L891
	movq	(%rdi), %rax
	call	*8(%rax)
.L891:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L892
	movq	(%rdi), %rax
	call	*8(%rax)
.L892:
	leaq	.LC17(%rip), %rax
	movq	%r12, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L1256:
	movq	-1(%r15), %rax
	cmpw	$1059, 11(%rax)
	jne	.L896
	movq	41112(%r13), %rdi
	movq	%r15, %rsi
	testq	%rdi, %rdi
	je	.L898
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L899:
	movl	39(%rsi), %eax
	andl	$8, %eax
	movl	%eax, -192(%rbp)
	movq	(%rbx), %rax
	je	.L901
	movq	39(%rax), %r15
	cmpq	$2147483647, %r15
	jbe	.L1261
	leaq	.LC19(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L901:
	leaq	-168(%rbp), %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal12JSTypedArray4typeEv@PLT
	cmpl	$11, %eax
	ja	.L1111
	leaq	.L1112(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internalL32Stats_Runtime_TypedArraySortFastEiPmPNS0_7IsolateE.isra.0,"a",@progbits
	.align 4
	.align 4
.L1112:
	.long	.L1111-.L1112
	.long	.L1143-.L1112
	.long	.L1144-.L1112
	.long	.L1145-.L1112
	.long	.L1146-.L1112
	.long	.L1147-.L1112
	.long	.L1148-.L1112
	.long	.L1149-.L1112
	.long	.L1150-.L1112
	.long	.L1151-.L1112
	.long	.L1152-.L1112
	.long	.L1153-.L1112
	.section	.text._ZN2v88internalL32Stats_Runtime_TypedArraySortFastEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L898:
	movq	%r12, %rax
	cmpq	41096(%r13), %r12
	je	.L1262
.L900:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%r15, (%rax)
	jmp	.L899
	.p2align 4,,10
	.p2align 3
.L1261:
	xorl	%edx, %edx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE@PLT
	movq	%r15, %rdx
	movq	%rax, %rcx
	movq	%rax, -200(%rbp)
	movq	(%rbx), %rax
	movq	(%rcx), %rcx
	movq	55(%rax), %rsi
	addq	63(%rax), %rsi
	leaq	15(%rcx), %rdi
	movq	%rcx, -208(%rbp)
	call	memcpy@PLT
	movq	(%rbx), %rax
	leaq	-168(%rbp), %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal12JSTypedArray4typeEv@PLT
	cmpl	$11, %eax
	ja	.L903
	leaq	.L905(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internalL32Stats_Runtime_TypedArraySortFastEiPmPNS0_7IsolateE.isra.0
	.align 4
	.align 4
.L905:
	.long	.L903-.L905
	.long	.L915-.L905
	.long	.L914-.L905
	.long	.L913-.L905
	.long	.L912-.L905
	.long	.L911-.L905
	.long	.L910-.L905
	.long	.L909-.L905
	.long	.L908-.L905
	.long	.L907-.L905
	.long	.L906-.L905
	.long	.L904-.L905
	.section	.text._ZN2v88internalL32Stats_Runtime_TypedArraySortFastEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L1251:
	movq	40960(%rsi), %rax
	movl	$645, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L1258:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movq	-184(%rbp), %r9
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L1255:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L888
	.p2align 4,,10
	.p2align 3
.L1260:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC17(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L1153:
	movq	$0, -200(%rbp)
.L904:
	movl	-192(%rbp), %ecx
	salq	$3, %r14
	testl	%ecx, %ecx
	je	.L1070
	movq	-200(%rbp), %rax
	movq	(%rax), %rax
	leaq	15(%rax), %r8
	movq	%rax, -208(%rbp)
	leaq	(%r8,%r14), %r15
	cmpq	%r15, %r8
	je	.L1250
.L1071:
	movq	%r14, %rax
	movl	$63, %edx
	movq	%r8, %rdi
	movq	%r15, %rsi
	sarq	$3, %rax
	movq	%r8, -208(%rbp)
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIPmlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	movq	-208(%rbp), %r8
	leaq	8(%r8), %rcx
	cmpq	$128, %r14
	jle	.L1072
	leaq	128(%r8), %r14
	movq	%r12, -208(%rbp)
	movq	%rbx, -216(%rbp)
	movq	%r14, %r12
	movq	%r8, %rbx
	movq	%rcx, %r14
	movq	%r13, -224(%rbp)
	jmp	.L1078
	.p2align 4,,10
	.p2align 3
.L1264:
	cmpq	%rbx, %r14
	je	.L1074
	movq	%r14, %rdx
	movl	$8, %eax
	movq	%rbx, %rsi
	subq	%rbx, %rdx
	leaq	(%rbx,%rax), %rdi
	call	memmove@PLT
.L1074:
	movq	%r13, (%rbx)
.L1075:
	addq	$8, %r14
	cmpq	%r14, %r12
	je	.L1263
.L1078:
	movq	(%r14), %r13
	cmpq	(%rbx), %r13
	jb	.L1264
	movq	-8(%r14), %rdx
	leaq	-8(%r14), %rax
	cmpq	%rdx, %r13
	jnb	.L1137
	.p2align 4,,10
	.p2align 3
.L1077:
	movq	%rdx, 8(%rax)
	movq	%rax, %rsi
	movq	-8(%rax), %rdx
	subq	$8, %rax
	cmpq	%rdx, %r13
	jb	.L1077
.L1076:
	movq	%r13, (%rsi)
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1152:
	movq	$0, -200(%rbp)
.L906:
	movl	-192(%rbp), %edx
	salq	$3, %r14
	testl	%edx, %edx
	je	.L1089
	movq	-200(%rbp), %rax
	movq	(%rax), %rax
	leaq	15(%rax), %r8
	movq	%rax, -208(%rbp)
	leaq	(%r8,%r14), %r15
	cmpq	%r15, %r8
	je	.L1250
.L1090:
	movq	%r14, %rax
	movl	$63, %edx
	movq	%r8, %rdi
	movq	%r15, %rsi
	sarq	$3, %rax
	movq	%r8, -208(%rbp)
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIPllN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	movq	-208(%rbp), %r8
	leaq	8(%r8), %rcx
	cmpq	$128, %r14
	jle	.L1091
	leaq	128(%r8), %r14
	movq	%r12, -208(%rbp)
	movq	%rbx, -216(%rbp)
	movq	%r14, %r12
	movq	%r8, %rbx
	movq	%rcx, %r14
	movq	%r13, -224(%rbp)
	jmp	.L1097
	.p2align 4,,10
	.p2align 3
.L1266:
	cmpq	%rbx, %r14
	je	.L1093
	movq	%r14, %rdx
	movl	$8, %eax
	movq	%rbx, %rsi
	subq	%rbx, %rdx
	leaq	(%rbx,%rax), %rdi
	call	memmove@PLT
.L1093:
	movq	%r13, (%rbx)
.L1094:
	addq	$8, %r14
	cmpq	%r14, %r12
	je	.L1265
.L1097:
	movq	(%r14), %r13
	cmpq	(%rbx), %r13
	jl	.L1266
	movq	-8(%r14), %rdx
	leaq	-8(%r14), %rax
	cmpq	%rdx, %r13
	jge	.L1140
	.p2align 4,,10
	.p2align 3
.L1096:
	movq	%rdx, 8(%rax)
	movq	%rax, %rsi
	movq	-8(%rax), %rdx
	subq	$8, %rax
	cmpq	%rdx, %r13
	jl	.L1096
.L1095:
	movq	%r13, (%rsi)
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1143:
	movq	$0, -200(%rbp)
.L938:
	movq	(%rbx), %r9
	movq	55(%r9), %r8
	addq	63(%r9), %r8
	leaq	(%r8,%r14), %r15
	cmpq	%r15, %r8
	je	.L894
.L939:
	bsrq	%r14, %rax
	movl	$63, %edx
	movq	%r8, %rdi
	movq	%r15, %rsi
	xorq	$63, %rax
	movq	%r8, -208(%rbp)
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIPalN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	movq	-208(%rbp), %r8
	leaq	1(%r8), %rcx
	cmpq	$16, %r14
	jle	.L940
	leaq	16(%r8), %r14
	movq	%r12, -208(%rbp)
	movq	%rbx, -216(%rbp)
	movq	%r14, %r12
	movq	%r8, %rbx
	movq	%rcx, %r14
	movq	%r13, -224(%rbp)
	jmp	.L946
	.p2align 4,,10
	.p2align 3
.L1269:
	movq	%r14, %rdx
	subq	%rbx, %rdx
	jne	.L1267
.L942:
	movb	%r13b, (%rbx)
.L943:
	addq	$1, %r14
	cmpq	%r14, %r12
	je	.L1268
.L946:
	movzbl	(%r14), %r13d
	cmpb	(%rbx), %r13b
	jl	.L1269
	movzbl	-1(%r14), %edx
	leaq	-1(%r14), %rax
	cmpb	%dl, %r13b
	jge	.L1119
	.p2align 4,,10
	.p2align 3
.L945:
	movb	%dl, 1(%rax)
	movq	%rax, %rsi
	movzbl	-1(%rax), %edx
	subq	$1, %rax
	cmpb	%dl, %r13b
	jl	.L945
.L944:
	movb	%r13b, (%rsi)
	jmp	.L943
	.p2align 4,,10
	.p2align 3
.L1151:
	movq	$0, -200(%rbp)
.L1051:
	movq	(%rbx), %r9
	movq	55(%r9), %r8
	addq	63(%r9), %r8
	leaq	(%r8,%r14), %r15
	cmpq	%r15, %r8
	je	.L894
.L1052:
	bsrq	%r14, %rax
	movl	$63, %edx
	movq	%r8, %rdi
	movq	%r15, %rsi
	xorq	$63, %rax
	movq	%r8, -208(%rbp)
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIPhlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	movq	-208(%rbp), %r8
	leaq	1(%r8), %rcx
	cmpq	$16, %r14
	jle	.L1053
	leaq	16(%r8), %r14
	movq	%r12, -208(%rbp)
	movq	%rbx, -216(%rbp)
	movq	%r14, %r12
	movq	%r8, %rbx
	movq	%rcx, %r14
	movq	%r13, -224(%rbp)
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1272:
	movq	%r14, %rdx
	subq	%rbx, %rdx
	jne	.L1270
.L1055:
	movb	%r13b, (%rbx)
.L1056:
	addq	$1, %r14
	cmpq	%r14, %r12
	je	.L1271
.L1059:
	movzbl	(%r14), %r13d
	cmpb	(%rbx), %r13b
	jb	.L1272
	movzbl	-1(%r14), %edx
	leaq	-1(%r14), %rax
	cmpb	%dl, %r13b
	jnb	.L1134
	.p2align 4,,10
	.p2align 3
.L1058:
	movb	%dl, 1(%rax)
	movq	%rax, %rsi
	movzbl	-1(%rax), %edx
	subq	$1, %rax
	cmpb	%dl, %r13b
	jb	.L1058
.L1057:
	movb	%r13b, (%rsi)
	jmp	.L1056
	.p2align 4,,10
	.p2align 3
.L1149:
	movq	$0, -200(%rbp)
.L1033:
	movq	(%rbx), %rax
	movq	55(%rax), %rdi
	addq	63(%rax), %rdi
.L1034:
	salq	$2, %r14
	leaq	(%rdi,%r14), %r15
	cmpq	%r15, %rdi
	je	.L927
	movq	%r14, %rax
	movl	$63, %edx
	movq	%r15, %rsi
	movq	%rdi, -208(%rbp)
	sarq	$2, %rax
	leaq	_ZN2v88internal12_GLOBAL__N_110CompareNumIfEEbT_S3_(%rip), %rcx
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIPflN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_S7_T0_T1_
	cmpq	$64, %r14
	movq	-208(%rbp), %rdi
	jle	.L1036
	leaq	64(%rdi), %r14
	leaq	_ZN2v88internal12_GLOBAL__N_110CompareNumIfEEbT_S3_(%rip), %rdx
	movq	%r14, %rsi
	call	_ZSt16__insertion_sortIPfN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_S7_T0_
	cmpq	%r14, %r15
	je	.L927
	.p2align 4,,10
	.p2align 3
.L1037:
	movss	(%r14), %xmm0
	movq	%r14, %rcx
	jmp	.L1040
	.p2align 4,,10
	.p2align 3
.L1273:
	movss	%xmm1, (%rcx)
	subq	$4, %rcx
.L1040:
	movss	-4(%rcx), %xmm1
	call	_ZN2v88internal12_GLOBAL__N_110CompareNumIfEEbT_S3_
	testb	%al, %al
	jne	.L1273
	addq	$4, %r14
	movss	%xmm0, (%rcx)
	cmpq	%r14, %r15
	jne	.L1037
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L1148:
	movq	$0, -200(%rbp)
.L910:
	movl	-192(%rbp), %r10d
	salq	$2, %r14
	testl	%r10d, %r10d
	je	.L995
	movq	-200(%rbp), %rax
	movq	(%rax), %rax
	leaq	15(%rax), %r8
	movq	%rax, -208(%rbp)
	leaq	(%r8,%r14), %r15
	cmpq	%r15, %r8
	je	.L1250
.L996:
	movq	%r14, %rax
	movl	$63, %edx
	movq	%r8, %rdi
	movq	%r15, %rsi
	sarq	$2, %rax
	movq	%r8, -208(%rbp)
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIPjlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	movq	-208(%rbp), %r8
	leaq	4(%r8), %rcx
	cmpq	$64, %r14
	jle	.L997
	leaq	64(%r8), %r14
	movq	%r12, -208(%rbp)
	movq	%rbx, -216(%rbp)
	movq	%r14, %r12
	movq	%r8, %rbx
	movq	%rcx, %r14
	movq	%r13, -224(%rbp)
	jmp	.L1003
	.p2align 4,,10
	.p2align 3
.L1275:
	cmpq	%rbx, %r14
	je	.L999
	movq	%r14, %rdx
	movl	$4, %eax
	movq	%rbx, %rsi
	subq	%rbx, %rdx
	leaq	(%rbx,%rax), %rdi
	call	memmove@PLT
.L999:
	movl	%r13d, (%rbx)
.L1000:
	addq	$4, %r14
	cmpq	%r14, %r12
	je	.L1274
.L1003:
	movl	(%r14), %r13d
	cmpl	(%rbx), %r13d
	jb	.L1275
	movl	-4(%r14), %edx
	leaq	-4(%r14), %rax
	cmpl	%edx, %r13d
	jnb	.L1128
	.p2align 4,,10
	.p2align 3
.L1002:
	movl	%edx, 4(%rax)
	movq	%rax, %rsi
	movl	-4(%rax), %edx
	subq	$4, %rax
	cmpl	%edx, %r13d
	jb	.L1002
.L1001:
	movl	%r13d, (%rsi)
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L1147:
	movq	$0, -200(%rbp)
.L911:
	movl	-192(%rbp), %r9d
	salq	$2, %r14
	testl	%r9d, %r9d
	je	.L1014
	movq	-200(%rbp), %rax
	movq	(%rax), %rax
	leaq	15(%rax), %r8
	movq	%rax, -208(%rbp)
	leaq	(%r8,%r14), %r15
	cmpq	%r15, %r8
	je	.L1250
.L1015:
	movq	%r14, %rax
	movl	$63, %edx
	movq	%r8, %rdi
	movq	%r15, %rsi
	sarq	$2, %rax
	movq	%r8, -208(%rbp)
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIPilN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	movq	-208(%rbp), %r8
	leaq	4(%r8), %rcx
	cmpq	$64, %r14
	jle	.L1016
	leaq	64(%r8), %r14
	movq	%r12, -208(%rbp)
	movq	%rbx, -216(%rbp)
	movq	%r14, %r12
	movq	%r8, %rbx
	movq	%rcx, %r14
	movq	%r13, -224(%rbp)
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1277:
	cmpq	%rbx, %r14
	je	.L1018
	movq	%r14, %rdx
	movl	$4, %eax
	movq	%rbx, %rsi
	subq	%rbx, %rdx
	leaq	(%rbx,%rax), %rdi
	call	memmove@PLT
.L1018:
	movl	%r13d, (%rbx)
.L1019:
	addq	$4, %r14
	cmpq	%r14, %r12
	je	.L1276
.L1022:
	movl	(%r14), %r13d
	cmpl	(%rbx), %r13d
	jl	.L1277
	movl	-4(%r14), %edx
	leaq	-4(%r14), %rax
	cmpl	%edx, %r13d
	jge	.L1131
	.p2align 4,,10
	.p2align 3
.L1021:
	movl	%edx, 4(%rax)
	movq	%rax, %rsi
	movl	-4(%rax), %edx
	subq	$4, %rax
	cmpl	%edx, %r13d
	jl	.L1021
.L1020:
	movl	%r13d, (%rsi)
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1145:
	movq	$0, -200(%rbp)
.L913:
	movl	-192(%rbp), %r11d
	addq	%r14, %r14
	testl	%r11d, %r11d
	je	.L976
	movq	-200(%rbp), %rax
	movq	(%rax), %rax
	leaq	15(%rax), %r8
	movq	%rax, -208(%rbp)
	leaq	(%r8,%r14), %r15
	cmpq	%r15, %r8
	je	.L1250
.L977:
	movq	%r14, %rax
	movl	$63, %edx
	movq	%r8, %rdi
	movq	%r15, %rsi
	sarq	%rax
	movq	%r8, -208(%rbp)
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIPslN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	movq	-208(%rbp), %r8
	leaq	2(%r8), %rcx
	cmpq	$32, %r14
	jle	.L978
	leaq	32(%r8), %r14
	movq	%r12, -208(%rbp)
	movq	%rbx, -216(%rbp)
	movq	%r14, %r12
	movq	%r8, %rbx
	movq	%rcx, %r14
	movq	%r13, -224(%rbp)
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1279:
	cmpq	%rbx, %r14
	je	.L980
	movq	%r14, %rdx
	movl	$2, %eax
	movq	%rbx, %rsi
	subq	%rbx, %rdx
	leaq	(%rbx,%rax), %rdi
	call	memmove@PLT
.L980:
	movw	%r13w, (%rbx)
.L981:
	addq	$2, %r14
	cmpq	%r14, %r12
	je	.L1278
.L984:
	movzwl	(%r14), %r13d
	cmpw	(%rbx), %r13w
	jl	.L1279
	movzwl	-2(%r14), %edx
	leaq	-2(%r14), %rax
	cmpw	%dx, %r13w
	jge	.L1125
	.p2align 4,,10
	.p2align 3
.L983:
	movw	%dx, 2(%rax)
	movq	%rax, %rsi
	movzwl	-2(%rax), %edx
	subq	$2, %rax
	cmpw	%dx, %r13w
	jl	.L983
.L982:
	movw	%r13w, (%rsi)
	jmp	.L981
	.p2align 4,,10
	.p2align 3
.L1146:
	movq	$0, -200(%rbp)
.L912:
	movl	-192(%rbp), %r15d
	addq	%r14, %r14
	testl	%r15d, %r15d
	je	.L957
	movq	-200(%rbp), %rax
	movq	(%rax), %rax
	leaq	15(%rax), %r8
	movq	%rax, -208(%rbp)
	leaq	(%r8,%r14), %r15
	cmpq	%r15, %r8
	je	.L1250
.L958:
	movq	%r14, %rax
	movl	$63, %edx
	movq	%r8, %rdi
	movq	%r15, %rsi
	sarq	%rax
	movq	%r8, -208(%rbp)
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIPtlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	movq	-208(%rbp), %r8
	leaq	2(%r8), %rcx
	cmpq	$32, %r14
	jle	.L959
	leaq	32(%r8), %r14
	movq	%r12, -208(%rbp)
	movq	%r8, %r12
	movq	%rbx, -216(%rbp)
	movq	%r14, %rbx
	movq	%rcx, %r14
	movq	%r13, -224(%rbp)
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L1281:
	cmpq	%r12, %r14
	je	.L961
	movq	%r14, %rdx
	movl	$2, %eax
	movq	%r12, %rsi
	subq	%r12, %rdx
	leaq	(%r12,%rax), %rdi
	call	memmove@PLT
.L961:
	movw	%r13w, (%r12)
.L962:
	addq	$2, %r14
	cmpq	%r14, %rbx
	je	.L1280
.L965:
	movzwl	(%r14), %r13d
	cmpw	(%r12), %r13w
	jb	.L1281
	movzwl	-2(%r14), %edx
	leaq	-2(%r14), %rax
	cmpw	%dx, %r13w
	jnb	.L1122
	.p2align 4,,10
	.p2align 3
.L964:
	movw	%dx, 2(%rax)
	movq	%rax, %rsi
	movzwl	-2(%rax), %edx
	subq	$2, %rax
	cmpw	%dx, %r13w
	jb	.L964
.L963:
	movw	%r13w, (%rsi)
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L1144:
	movq	$0, -200(%rbp)
.L917:
	movq	(%rbx), %r9
	movq	55(%r9), %r8
	addq	63(%r9), %r8
	leaq	(%r8,%r14), %r15
	cmpq	%r15, %r8
	je	.L894
.L918:
	bsrq	%r14, %rax
	movl	$63, %edx
	movq	%r8, %rdi
	movq	%r15, %rsi
	xorq	$63, %rax
	movq	%r8, -208(%rbp)
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIPhlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	movq	-208(%rbp), %r8
	leaq	1(%r8), %rcx
	cmpq	$16, %r14
	jle	.L920
	leaq	16(%r8), %r14
	movq	%r12, -208(%rbp)
	movq	%rbx, -216(%rbp)
	movq	%r14, %r12
	movq	%r8, %rbx
	movq	%rcx, %r14
	movq	%r13, -224(%rbp)
	jmp	.L926
	.p2align 4,,10
	.p2align 3
.L1284:
	movq	%r14, %rdx
	subq	%rbx, %rdx
	jne	.L1282
.L922:
	movb	%r13b, (%rbx)
.L923:
	addq	$1, %r14
	cmpq	%r14, %r12
	je	.L1283
.L926:
	movzbl	(%r14), %r13d
	cmpb	(%rbx), %r13b
	jb	.L1284
	movzbl	-1(%r14), %edx
	leaq	-1(%r14), %rax
	cmpb	%r13b, %dl
	jbe	.L1116
	.p2align 4,,10
	.p2align 3
.L925:
	movb	%dl, 1(%rax)
	movq	%rax, %rsi
	movzbl	-1(%rax), %edx
	subq	$1, %rax
	cmpb	%dl, %r13b
	jb	.L925
.L924:
	movb	%r13b, (%rsi)
	jmp	.L923
	.p2align 4,,10
	.p2align 3
.L907:
	movl	-192(%rbp), %esi
	testl	%esi, %esi
	je	.L1051
	movq	-200(%rbp), %rax
	movq	(%rax), %rax
	leaq	15(%rax), %r8
	movq	%rax, -208(%rbp)
	leaq	(%r8,%r14), %r15
	cmpq	%r15, %r8
	jne	.L1052
	.p2align 4,,10
	.p2align 3
.L1250:
	movq	(%rbx), %r9
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L914:
	movl	-192(%rbp), %eax
	testl	%eax, %eax
	je	.L917
	movq	-200(%rbp), %rax
	movq	(%rax), %rax
	leaq	15(%rax), %r8
	movq	%rax, -208(%rbp)
	leaq	(%r8,%r14), %r15
	cmpq	%r15, %r8
	jne	.L918
	jmp	.L1250
	.p2align 4,,10
	.p2align 3
.L915:
	movl	-192(%rbp), %eax
	testl	%eax, %eax
	je	.L938
	movq	-200(%rbp), %rax
	movq	(%rax), %rax
	leaq	15(%rax), %r8
	movq	%rax, -208(%rbp)
	leaq	(%r8,%r14), %r15
	cmpq	%r15, %r8
	jne	.L939
	jmp	.L1250
	.p2align 4,,10
	.p2align 3
.L908:
	movl	-192(%rbp), %edi
	testl	%edi, %edi
	je	.L1042
	movq	-200(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -208(%rbp)
	leaq	15(%rax), %rdi
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L909:
	movl	-192(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L1033
	movq	-200(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -208(%rbp)
	leaq	15(%rax), %rdi
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1262:
	movq	%r13, %rdi
	movq	%r15, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-192(%rbp), %rsi
	jmp	.L900
.L1111:
	movq	(%rbx), %r9
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L1014:
	movq	(%rbx), %r9
	movq	55(%r9), %r8
	addq	63(%r9), %r8
	leaq	(%r8,%r14), %r15
	cmpq	%r15, %r8
	jne	.L1015
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L995:
	movq	(%rbx), %r9
	movq	55(%r9), %r8
	addq	63(%r9), %r8
	leaq	(%r8,%r14), %r15
	cmpq	%r15, %r8
	jne	.L996
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L1089:
	movq	(%rbx), %r9
	movq	55(%r9), %r8
	addq	63(%r9), %r8
	leaq	(%r8,%r14), %r15
	cmpq	%r15, %r8
	jne	.L1090
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L1070:
	movq	(%rbx), %r9
	movq	55(%r9), %r8
	addq	63(%r9), %r8
	leaq	(%r8,%r14), %r15
	cmpq	%r15, %r8
	jne	.L1071
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L976:
	movq	(%rbx), %r9
	movq	55(%r9), %r8
	addq	63(%r9), %r8
	leaq	(%r8,%r14), %r15
	cmpq	%r15, %r8
	jne	.L977
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L957:
	movq	(%rbx), %r9
	movq	55(%r9), %r8
	addq	63(%r9), %r8
	leaq	(%r8,%r14), %r15
	cmpq	%r15, %r8
	jne	.L958
	jmp	.L894
.L903:
	movq	-200(%rbp), %rax
	movq	(%rbx), %r9
	movq	(%rax), %r15
	addq	$15, %r15
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L1282:
	movl	$1, %edi
	movq	%rbx, %rsi
	subq	%rdx, %rdi
	addq	%r14, %rdi
	call	memmove@PLT
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L1267:
	movl	$1, %edi
	movq	%rbx, %rsi
	subq	%rdx, %rdi
	addq	%r14, %rdi
	call	memmove@PLT
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L1270:
	movl	$1, %edi
	movq	%rbx, %rsi
	subq	%rdx, %rdi
	addq	%r14, %rdi
	call	memmove@PLT
	jmp	.L1055
	.p2align 4,,10
	.p2align 3
.L959:
	cmpq	%r15, %rcx
	je	.L927
	movq	%r12, -208(%rbp)
	movq	%rcx, %r14
	movq	%r8, %r12
	movq	%rbx, -216(%rbp)
	movq	%r15, %rbx
	jmp	.L969
	.p2align 4,,10
	.p2align 3
.L1285:
	cmpq	%r12, %r14
	je	.L971
	movq	%r14, %rdx
	movl	$2, %eax
	movq	%r12, %rsi
	subq	%r12, %rdx
	leaq	(%r12,%rax), %rdi
	call	memmove@PLT
.L971:
	movw	%r15w, (%r12)
.L972:
	addq	$2, %r14
	cmpq	%rbx, %r14
	je	.L1249
.L969:
	movzwl	(%r14), %r15d
	cmpw	(%r12), %r15w
	jb	.L1285
	movzwl	-2(%r14), %edx
	leaq	-2(%r14), %rax
	cmpw	%dx, %r15w
	jnb	.L1124
	.p2align 4,,10
	.p2align 3
.L974:
	movw	%dx, 2(%rax)
	movq	%rax, %rcx
	movzwl	-2(%rax), %edx
	subq	$2, %rax
	cmpw	%dx, %r15w
	jb	.L974
.L973:
	movw	%r15w, (%rcx)
	jmp	.L972
	.p2align 4,,10
	.p2align 3
.L1249:
	movq	-208(%rbp), %r12
	movq	-216(%rbp), %rbx
	jmp	.L927
.L1268:
	movq	%r12, %r14
	movq	-216(%rbp), %rbx
	movq	-208(%rbp), %r12
	movq	-224(%rbp), %r13
	cmpq	%r15, %r14
	je	.L927
	.p2align 4,,10
	.p2align 3
.L949:
	movzbl	(%r14), %ecx
	movzbl	-1(%r14), %edx
	leaq	-1(%r14), %rax
	cmpb	%dl, %cl
	jge	.L1120
	.p2align 4,,10
	.p2align 3
.L948:
	movb	%dl, 1(%rax)
	movq	%rax, %rsi
	movzbl	-1(%rax), %edx
	subq	$1, %rax
	cmpb	%dl, %cl
	jl	.L948
.L947:
	addq	$1, %r14
	movb	%cl, (%rsi)
	cmpq	%r15, %r14
	jne	.L949
	jmp	.L927
.L1271:
	movq	%r12, %r14
	movq	-216(%rbp), %rbx
	movq	-208(%rbp), %r12
	movq	-224(%rbp), %r13
	cmpq	%r15, %r14
	je	.L927
	.p2align 4,,10
	.p2align 3
.L1062:
	movzbl	(%r14), %ecx
	movzbl	-1(%r14), %edx
	leaq	-1(%r14), %rax
	cmpb	%cl, %dl
	jbe	.L1135
	.p2align 4,,10
	.p2align 3
.L1061:
	movb	%dl, 1(%rax)
	movq	%rax, %rsi
	movzbl	-1(%rax), %edx
	subq	$1, %rax
	cmpb	%dl, %cl
	jb	.L1061
.L1060:
	addq	$1, %r14
	movb	%cl, (%rsi)
	cmpq	%r15, %r14
	jne	.L1062
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L978:
	cmpq	%r15, %rcx
	je	.L927
	movq	%r12, -208(%rbp)
	movq	%rcx, %r14
	movq	%r8, %r12
	movq	%rbx, -216(%rbp)
	movq	%r15, %rbx
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L1286:
	cmpq	%r12, %r14
	je	.L990
	movq	%r14, %rdx
	movl	$2, %eax
	movq	%r12, %rsi
	subq	%r12, %rdx
	leaq	(%r12,%rax), %rdi
	call	memmove@PLT
.L990:
	movw	%r15w, (%r12)
.L991:
	addq	$2, %r14
	cmpq	%rbx, %r14
	je	.L1249
.L988:
	movzwl	(%r14), %r15d
	cmpw	(%r12), %r15w
	jl	.L1286
	movzwl	-2(%r14), %edx
	leaq	-2(%r14), %rax
	cmpw	%dx, %r15w
	jge	.L1127
	.p2align 4,,10
	.p2align 3
.L993:
	movw	%dx, 2(%rax)
	movq	%rax, %rcx
	movzwl	-2(%rax), %edx
	subq	$2, %rax
	cmpw	%dx, %r15w
	jl	.L993
.L992:
	movw	%r15w, (%rcx)
	jmp	.L991
	.p2align 4,,10
	.p2align 3
.L940:
	cmpq	%r15, %rcx
	je	.L927
	movq	%r12, -208(%rbp)
	movq	%rcx, %r14
	movq	%r8, %r12
	movq	%rbx, -216(%rbp)
	movq	%r15, %rbx
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L1288:
	movq	%r14, %rdx
	subq	%r12, %rdx
	jne	.L1287
.L952:
	movb	%r15b, (%r12)
.L953:
	addq	$1, %r14
	cmpq	%rbx, %r14
	je	.L1249
.L950:
	movzbl	(%r14), %r15d
	cmpb	(%r12), %r15b
	jl	.L1288
	movzbl	-1(%r14), %edx
	leaq	-1(%r14), %rax
	cmpb	%dl, %r15b
	jge	.L1121
	.p2align 4,,10
	.p2align 3
.L955:
	movb	%dl, 1(%rax)
	movq	%rax, %rcx
	movzbl	-1(%rax), %edx
	subq	$1, %rax
	cmpb	%dl, %r15b
	jl	.L955
.L954:
	movb	%r15b, (%rcx)
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L1287:
	movl	$1, %edi
	movq	%r12, %rsi
	subq	%rdx, %rdi
	addq	%r14, %rdi
	call	memmove@PLT
	jmp	.L952
	.p2align 4,,10
	.p2align 3
.L920:
	cmpq	%r15, %rcx
	je	.L927
	movq	%r12, -208(%rbp)
	movq	%rcx, %r14
	movq	%r8, %r12
	movq	%rbx, -216(%rbp)
	movq	%r15, %rbx
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L1290:
	movq	%r14, %rdx
	subq	%r12, %rdx
	jne	.L1289
.L933:
	movb	%r15b, (%r12)
.L934:
	addq	$1, %r14
	cmpq	%rbx, %r14
	je	.L1249
.L931:
	movzbl	(%r14), %r15d
	cmpb	(%r12), %r15b
	jb	.L1290
	movzbl	-1(%r14), %edx
	leaq	-1(%r14), %rax
	cmpb	%dl, %r15b
	jnb	.L1118
	.p2align 4,,10
	.p2align 3
.L936:
	movb	%dl, 1(%rax)
	movq	%rax, %rcx
	movzbl	-1(%rax), %edx
	subq	$1, %rax
	cmpb	%dl, %r15b
	jb	.L936
.L935:
	movb	%r15b, (%rcx)
	jmp	.L934
	.p2align 4,,10
	.p2align 3
.L1289:
	movl	$1, %edi
	movq	%r12, %rsi
	subq	%rdx, %rdi
	addq	%r14, %rdi
	call	memmove@PLT
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L1016:
	cmpq	%r15, %rcx
	je	.L927
	movq	%r12, -208(%rbp)
	movq	%rcx, %r14
	movq	%r8, %r12
	movq	%rbx, -216(%rbp)
	movq	%r15, %rbx
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1291:
	cmpq	%r12, %r14
	je	.L1028
	movq	%r14, %rdx
	movl	$4, %eax
	movq	%r12, %rsi
	subq	%r12, %rdx
	leaq	(%r12,%rax), %rdi
	call	memmove@PLT
.L1028:
	movl	%r15d, (%r12)
.L1029:
	addq	$4, %r14
	cmpq	%rbx, %r14
	je	.L1249
.L1026:
	movl	(%r14), %r15d
	cmpl	(%r12), %r15d
	jl	.L1291
	movl	-4(%r14), %edx
	leaq	-4(%r14), %rax
	cmpl	%edx, %r15d
	jge	.L1133
	.p2align 4,,10
	.p2align 3
.L1031:
	movl	%edx, 4(%rax)
	movq	%rax, %rcx
	movl	-4(%rax), %edx
	subq	$4, %rax
	cmpl	%edx, %r15d
	jl	.L1031
.L1030:
	movl	%r15d, (%rcx)
	jmp	.L1029
	.p2align 4,,10
	.p2align 3
.L1072:
	cmpq	%r15, %rcx
	je	.L927
	movq	%r12, -208(%rbp)
	movq	%rcx, %r14
	movq	%r8, %r12
	movq	%rbx, -216(%rbp)
	movq	%r15, %rbx
	jmp	.L1082
	.p2align 4,,10
	.p2align 3
.L1292:
	cmpq	%r12, %r14
	je	.L1084
	movq	%r14, %rdx
	movl	$8, %eax
	movq	%r12, %rsi
	subq	%r12, %rdx
	leaq	(%r12,%rax), %rdi
	call	memmove@PLT
.L1084:
	movq	%r15, (%r12)
.L1085:
	addq	$8, %r14
	cmpq	%rbx, %r14
	je	.L1249
.L1082:
	movq	(%r14), %r15
	cmpq	(%r12), %r15
	jb	.L1292
	movq	-8(%r14), %rdx
	leaq	-8(%r14), %rax
	cmpq	%r15, %rdx
	jbe	.L1139
	.p2align 4,,10
	.p2align 3
.L1087:
	movq	%rdx, 8(%rax)
	movq	%rax, %rcx
	movq	-8(%rax), %rdx
	subq	$8, %rax
	cmpq	%rdx, %r15
	jb	.L1087
.L1086:
	movq	%r15, (%rcx)
	jmp	.L1085
	.p2align 4,,10
	.p2align 3
.L1091:
	cmpq	%r15, %rcx
	je	.L927
	movq	%r12, -208(%rbp)
	movq	%rcx, %r14
	movq	%r8, %r12
	movq	%rbx, -216(%rbp)
	movq	%r15, %rbx
	jmp	.L1101
	.p2align 4,,10
	.p2align 3
.L1293:
	cmpq	%r12, %r14
	je	.L1103
	movq	%r14, %rdx
	movl	$8, %eax
	movq	%r12, %rsi
	subq	%r12, %rdx
	leaq	(%r12,%rax), %rdi
	call	memmove@PLT
.L1103:
	movq	%r15, (%r12)
.L1104:
	addq	$8, %r14
	cmpq	%rbx, %r14
	je	.L1249
.L1101:
	movq	(%r14), %r15
	cmpq	(%r12), %r15
	jl	.L1293
	movq	-8(%r14), %rdx
	leaq	-8(%r14), %rax
	cmpq	%rdx, %r15
	jge	.L1142
	.p2align 4,,10
	.p2align 3
.L1106:
	movq	%rdx, 8(%rax)
	movq	%rax, %rcx
	movq	-8(%rax), %rdx
	subq	$8, %rax
	cmpq	%rdx, %r15
	jl	.L1106
.L1105:
	movq	%r15, (%rcx)
	jmp	.L1104
	.p2align 4,,10
	.p2align 3
.L1053:
	cmpq	%r15, %rcx
	je	.L927
	movq	%r12, -208(%rbp)
	movq	%rcx, %r14
	movq	%r8, %r12
	movq	%rbx, -216(%rbp)
	movq	%r15, %rbx
	jmp	.L1063
	.p2align 4,,10
	.p2align 3
.L1295:
	movq	%r14, %rdx
	subq	%r12, %rdx
	jne	.L1294
.L1065:
	movb	%r15b, (%r12)
.L1066:
	addq	$1, %r14
	cmpq	%rbx, %r14
	je	.L1249
.L1063:
	movzbl	(%r14), %r15d
	cmpb	(%r12), %r15b
	jb	.L1295
	movzbl	-1(%r14), %edx
	leaq	-1(%r14), %rax
	cmpb	%r15b, %dl
	jbe	.L1136
	.p2align 4,,10
	.p2align 3
.L1068:
	movb	%dl, 1(%rax)
	movq	%rax, %rcx
	movzbl	-1(%rax), %edx
	subq	$1, %rax
	cmpb	%dl, %r15b
	jb	.L1068
.L1067:
	movb	%r15b, (%rcx)
	jmp	.L1066
	.p2align 4,,10
	.p2align 3
.L1294:
	movl	$1, %edi
	movq	%r12, %rsi
	subq	%rdx, %rdi
	addq	%r14, %rdi
	call	memmove@PLT
	jmp	.L1065
	.p2align 4,,10
	.p2align 3
.L997:
	cmpq	%r15, %rcx
	je	.L927
	movq	%r12, -208(%rbp)
	movq	%rcx, %r14
	movq	%r8, %r12
	movq	%rbx, -216(%rbp)
	movq	%r15, %rbx
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L1296:
	cmpq	%r12, %r14
	je	.L1009
	movq	%r14, %rdx
	movl	$4, %eax
	movq	%r12, %rsi
	subq	%r12, %rdx
	leaq	(%r12,%rax), %rdi
	call	memmove@PLT
.L1009:
	movl	%r15d, (%r12)
.L1010:
	addq	$4, %r14
	cmpq	%rbx, %r14
	je	.L1249
.L1007:
	movl	(%r14), %r15d
	cmpl	(%r12), %r15d
	jb	.L1296
	movl	-4(%r14), %edx
	leaq	-4(%r14), %rax
	cmpl	%edx, %r15d
	jnb	.L1130
	.p2align 4,,10
	.p2align 3
.L1012:
	movl	%edx, 4(%rax)
	movq	%rax, %rcx
	movl	-4(%rax), %edx
	subq	$4, %rax
	cmpl	%edx, %r15d
	jb	.L1012
.L1011:
	movl	%r15d, (%rcx)
	jmp	.L1010
.L1265:
	movq	%r12, %r14
	movq	-216(%rbp), %rbx
	movq	-208(%rbp), %r12
	movq	-224(%rbp), %r13
	cmpq	%r15, %r14
	je	.L927
	.p2align 4,,10
	.p2align 3
.L1100:
	movq	(%r14), %rcx
	movq	-8(%r14), %rdx
	leaq	-8(%r14), %rax
	cmpq	%rcx, %rdx
	jle	.L1141
	.p2align 4,,10
	.p2align 3
.L1099:
	movq	%rdx, 8(%rax)
	movq	%rax, %rsi
	movq	-8(%rax), %rdx
	subq	$8, %rax
	cmpq	%rdx, %rcx
	jl	.L1099
.L1098:
	addq	$8, %r14
	movq	%rcx, (%rsi)
	cmpq	%r15, %r14
	jne	.L1100
	jmp	.L927
.L1283:
	movq	%r12, %r14
	movq	-216(%rbp), %rbx
	movq	-208(%rbp), %r12
	movq	-224(%rbp), %r13
	cmpq	%r15, %r14
	je	.L927
	.p2align 4,,10
	.p2align 3
.L930:
	movzbl	(%r14), %ecx
	movzbl	-1(%r14), %edx
	leaq	-1(%r14), %rax
	cmpb	%cl, %dl
	jbe	.L1117
	.p2align 4,,10
	.p2align 3
.L929:
	movb	%dl, 1(%rax)
	movq	%rax, %rsi
	movzbl	-1(%rax), %edx
	subq	$1, %rax
	cmpb	%dl, %cl
	jb	.L929
.L928:
	addq	$1, %r14
	movb	%cl, (%rsi)
	cmpq	%r15, %r14
	jne	.L930
	jmp	.L927
.L1274:
	movq	%r12, %r14
	movq	-216(%rbp), %rbx
	movq	-208(%rbp), %r12
	movq	-224(%rbp), %r13
	cmpq	%r15, %r14
	je	.L927
	.p2align 4,,10
	.p2align 3
.L1006:
	movl	(%r14), %ecx
	movl	-4(%r14), %edx
	leaq	-4(%r14), %rax
	cmpl	%edx, %ecx
	jnb	.L1129
	.p2align 4,,10
	.p2align 3
.L1005:
	movl	%edx, 4(%rax)
	movq	%rax, %rsi
	movl	-4(%rax), %edx
	subq	$4, %rax
	cmpl	%edx, %ecx
	jb	.L1005
.L1004:
	addq	$4, %r14
	movl	%ecx, (%rsi)
	cmpq	%r15, %r14
	jne	.L1006
	jmp	.L927
.L1278:
	movq	%r12, %r14
	movq	-216(%rbp), %rbx
	movq	-208(%rbp), %r12
	movq	-224(%rbp), %r13
	cmpq	%r15, %r14
	je	.L927
	.p2align 4,,10
	.p2align 3
.L987:
	movzwl	(%r14), %ecx
	movzwl	-2(%r14), %edx
	leaq	-2(%r14), %rax
	cmpw	%cx, %dx
	jle	.L1126
	.p2align 4,,10
	.p2align 3
.L986:
	movw	%dx, 2(%rax)
	movq	%rax, %rsi
	movzwl	-2(%rax), %edx
	subq	$2, %rax
	cmpw	%dx, %cx
	jl	.L986
.L985:
	addq	$2, %r14
	movw	%cx, (%rsi)
	cmpq	%r15, %r14
	jne	.L987
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L1036:
	leaq	_ZN2v88internal12_GLOBAL__N_110CompareNumIfEEbT_S3_(%rip), %rdx
	movq	%r15, %rsi
	call	_ZSt16__insertion_sortIPfN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_S7_T0_
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L1045:
	leaq	_ZN2v88internal12_GLOBAL__N_110CompareNumIdEEbT_S3_(%rip), %rdx
	movq	%r15, %rsi
	call	_ZSt16__insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_
	jmp	.L927
.L1263:
	movq	%r12, %r14
	movq	-216(%rbp), %rbx
	movq	-208(%rbp), %r12
	movq	-224(%rbp), %r13
	cmpq	%r15, %r14
	je	.L927
	.p2align 4,,10
	.p2align 3
.L1081:
	movq	(%r14), %rcx
	movq	-8(%r14), %rdx
	leaq	-8(%r14), %rax
	cmpq	%rdx, %rcx
	jnb	.L1138
	.p2align 4,,10
	.p2align 3
.L1080:
	movq	%rdx, 8(%rax)
	movq	%rax, %rsi
	movq	-8(%rax), %rdx
	subq	$8, %rax
	cmpq	%rdx, %rcx
	jb	.L1080
.L1079:
	addq	$8, %r14
	movq	%rcx, (%rsi)
	cmpq	%r15, %r14
	jne	.L1081
	jmp	.L927
.L1276:
	movq	%r12, %r14
	movq	-216(%rbp), %rbx
	movq	-208(%rbp), %r12
	movq	-224(%rbp), %r13
	cmpq	%r15, %r14
	je	.L927
	.p2align 4,,10
	.p2align 3
.L1025:
	movl	(%r14), %ecx
	movl	-4(%r14), %edx
	leaq	-4(%r14), %rax
	cmpl	%edx, %ecx
	jge	.L1132
	.p2align 4,,10
	.p2align 3
.L1024:
	movl	%edx, 4(%rax)
	movq	%rax, %rsi
	movl	-4(%rax), %edx
	subq	$4, %rax
	cmpl	%edx, %ecx
	jl	.L1024
.L1023:
	addq	$4, %r14
	movl	%ecx, (%rsi)
	cmpq	%r15, %r14
	jne	.L1025
	jmp	.L927
.L1280:
	movq	%rbx, %r14
	movq	-208(%rbp), %r12
	movq	-216(%rbp), %rbx
	movq	-224(%rbp), %r13
	cmpq	%r15, %r14
	je	.L927
	.p2align 4,,10
	.p2align 3
.L968:
	movzwl	(%r14), %ecx
	movzwl	-2(%r14), %edx
	leaq	-2(%r14), %rax
	cmpw	%dx, %cx
	jnb	.L1123
	.p2align 4,,10
	.p2align 3
.L967:
	movw	%dx, 2(%rax)
	movq	%rax, %rsi
	movzwl	-2(%rax), %edx
	subq	$2, %rax
	cmpw	%dx, %cx
	jb	.L967
.L966:
	addq	$2, %r14
	movw	%cx, (%rsi)
	cmpq	%r15, %r14
	jne	.L968
	jmp	.L927
.L1123:
	movq	%r14, %rsi
	jmp	.L966
.L1132:
	movq	%r14, %rsi
	jmp	.L1023
.L1135:
	movq	%r14, %rsi
	jmp	.L1060
.L1120:
	movq	%r14, %rsi
	jmp	.L947
.L1138:
	movq	%r14, %rsi
	jmp	.L1079
.L1126:
	movq	%r14, %rsi
	jmp	.L985
.L1129:
	movq	%r14, %rsi
	jmp	.L1004
.L1117:
	movq	%r14, %rsi
	jmp	.L928
.L1141:
	movq	%r14, %rsi
	jmp	.L1098
.L1134:
	movq	%r14, %rsi
	jmp	.L1057
.L1119:
	movq	%r14, %rsi
	jmp	.L944
.L1124:
	movq	%r14, %rcx
	jmp	.L973
.L1122:
	movq	%r14, %rsi
	jmp	.L963
.L1137:
	movq	%r14, %rsi
	jmp	.L1076
.L1140:
	movq	%r14, %rsi
	jmp	.L1095
.L1130:
	movq	%r14, %rcx
	jmp	.L1011
.L1136:
	movq	%r14, %rcx
	jmp	.L1067
.L1116:
	movq	%r14, %rsi
	jmp	.L924
.L1125:
	movq	%r14, %rsi
	jmp	.L982
.L1128:
	movq	%r14, %rsi
	jmp	.L1001
.L1131:
	movq	%r14, %rsi
	jmp	.L1020
.L1121:
	movq	%r14, %rcx
	jmp	.L954
.L1127:
	movq	%r14, %rcx
	jmp	.L992
.L1142:
	movq	%r14, %rcx
	jmp	.L1105
.L1139:
	movq	%r14, %rcx
	jmp	.L1086
.L1133:
	movq	%r14, %rcx
	jmp	.L1030
.L1118:
	movq	%r14, %rcx
	jmp	.L935
.L1259:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26455:
	.size	_ZN2v88internalL32Stats_Runtime_TypedArraySortFastEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL32Stats_Runtime_TypedArraySortFastEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal26Runtime_TypedArraySortFastEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_TypedArraySortFastEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_TypedArraySortFastEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_TypedArraySortFastEiPmPNS0_7IsolateE:
.LFB20042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1640
	movq	41096(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	(%rsi), %rdx
	movq	%rax, -72(%rbp)
	testb	$1, %dl
	jne	.L1641
.L1300:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1641:
	movq	-1(%rdx), %rax
	cmpw	$1086, 11(%rax)
	jne	.L1300
	movq	47(%rdx), %r14
	movq	%rdx, %rax
	cmpq	$1, %r14
	jbe	.L1301
	movq	23(%rdx), %r15
	testb	$1, %r15b
	jne	.L1642
.L1303:
	leaq	.LC18(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1554:
	movq	$0, -88(%rbp)
.L1449:
	movq	(%r12), %rax
	movq	55(%rax), %rdi
	addq	63(%rax), %rdi
.L1450:
	salq	$3, %r14
	leaq	(%rdi,%r14), %r15
	cmpq	%r15, %rdi
	je	.L1334
	movq	%r14, %rax
	movl	$63, %edx
	movq	%r15, %rsi
	movq	%rdi, -96(%rbp)
	sarq	$3, %rax
	leaq	_ZN2v88internal12_GLOBAL__N_110CompareNumIdEEbT_S3_(%rip), %rcx
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_T1_
	cmpq	$128, %r14
	movq	-96(%rbp), %rdi
	jle	.L1452
	leaq	128(%rdi), %r14
	leaq	_ZN2v88internal12_GLOBAL__N_110CompareNumIdEEbT_S3_(%rip), %rdx
	movq	%r14, %rsi
	call	_ZSt16__insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_
	cmpq	%r14, %r15
	je	.L1334
	.p2align 4,,10
	.p2align 3
.L1453:
	movsd	(%r14), %xmm0
	movq	%r14, %rcx
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1643:
	movsd	%xmm1, (%rcx)
	subq	$8, %rcx
.L1456:
	movsd	-8(%rcx), %xmm1
	call	_ZN2v88internal12_GLOBAL__N_110CompareNumIdEEbT_S3_
	testb	%al, %al
	jne	.L1643
	addq	$8, %r14
	movsd	%xmm0, (%rcx)
	cmpq	%r14, %r15
	jne	.L1453
	.p2align 4,,10
	.p2align 3
.L1334:
	movl	-80(%rbp), %edx
	movq	(%r12), %rax
	testl	%edx, %edx
	je	.L1301
	movq	-88(%rbp), %rdi
	movq	(%rdi), %r15
	addq	$15, %r15
.L1323:
	movq	39(%rax), %rdx
	movq	55(%rax), %rdi
	movq	%r15, %rsi
	addq	63(%rax), %rdi
	call	memcpy@PLT
	movq	(%r12), %rax
	.p2align 4,,10
	.p2align 3
.L1301:
	movq	%rbx, 41088(%r13)
	movq	-72(%rbp), %rbx
	subl	$1, 41104(%r13)
	cmpq	41096(%r13), %rbx
	je	.L1297
	movq	%rax, -80(%rbp)
	movq	-72(%rbp), %rax
	movq	%r13, %rdi
	movq	%rax, 41096(%r13)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rax
.L1297:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1644
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1642:
	.cfi_restore_state
	movq	-1(%r15), %rax
	cmpw	$1059, 11(%rax)
	jne	.L1303
	movq	41112(%r13), %rdi
	movq	%r15, %rsi
	testq	%rdi, %rdi
	je	.L1305
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1306:
	movl	39(%rsi), %eax
	andl	$8, %eax
	movl	%eax, -80(%rbp)
	movq	(%r12), %rax
	je	.L1308
	movq	39(%rax), %r15
	cmpq	$2147483647, %r15
	jbe	.L1645
	leaq	.LC19(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1308:
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal12JSTypedArray4typeEv@PLT
	cmpl	$11, %eax
	ja	.L1517
	leaq	.L1518(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal26Runtime_TypedArraySortFastEiPmPNS0_7IsolateE,"a",@progbits
	.align 4
	.align 4
.L1518:
	.long	.L1517-.L1518
	.long	.L1547-.L1518
	.long	.L1548-.L1518
	.long	.L1549-.L1518
	.long	.L1550-.L1518
	.long	.L1551-.L1518
	.long	.L1552-.L1518
	.long	.L1553-.L1518
	.long	.L1554-.L1518
	.long	.L1555-.L1518
	.long	.L1556-.L1518
	.long	.L1557-.L1518
	.section	.text._ZN2v88internal26Runtime_TypedArraySortFastEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L1305:
	movq	%rbx, %rax
	cmpq	41096(%r13), %rbx
	je	.L1646
.L1307:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%r15, (%rax)
	jmp	.L1306
	.p2align 4,,10
	.p2align 3
.L1645:
	xorl	%edx, %edx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE@PLT
	movq	%r15, %rdx
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	movq	(%r12), %rax
	movq	(%rdi), %rdi
	movq	55(%rax), %rsi
	addq	63(%rax), %rsi
	movq	%rdi, -96(%rbp)
	addq	$15, %rdi
	call	memcpy@PLT
	movq	(%r12), %rax
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal12JSTypedArray4typeEv@PLT
	cmpl	$11, %eax
	ja	.L1310
	leaq	.L1312(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal26Runtime_TypedArraySortFastEiPmPNS0_7IsolateE
	.align 4
	.align 4
.L1312:
	.long	.L1310-.L1312
	.long	.L1322-.L1312
	.long	.L1321-.L1312
	.long	.L1320-.L1312
	.long	.L1319-.L1312
	.long	.L1318-.L1312
	.long	.L1317-.L1312
	.long	.L1316-.L1312
	.long	.L1315-.L1312
	.long	.L1314-.L1312
	.long	.L1313-.L1312
	.long	.L1311-.L1312
	.section	.text._ZN2v88internal26Runtime_TypedArraySortFastEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L1640:
	movq	%rdx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internalL32Stats_Runtime_TypedArraySortFastEiPmPNS0_7IsolateE.isra.0
	jmp	.L1297
	.p2align 4,,10
	.p2align 3
.L1557:
	movq	$0, -88(%rbp)
.L1311:
	movl	-80(%rbp), %esi
	salq	$3, %r14
	testl	%esi, %esi
	je	.L1477
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	leaq	15(%rax), %r9
	movq	%rax, -96(%rbp)
	leaq	(%r9,%r14), %r15
	cmpq	%r15, %r9
	je	.L1639
.L1478:
	movq	%r14, %rax
	movl	$63, %edx
	movq	%r9, %rdi
	movq	%r15, %rsi
	sarq	$3, %rax
	movq	%r9, -96(%rbp)
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIPmlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	movq	-96(%rbp), %r9
	leaq	8(%r9), %rcx
	cmpq	$128, %r14
	jle	.L1479
	leaq	128(%r9), %r14
	movq	%rbx, -96(%rbp)
	movq	%r9, %rbx
	movq	%r12, -104(%rbp)
	movq	%r14, %r12
	movq	%rcx, %r14
	movq	%r13, -112(%rbp)
	jmp	.L1485
	.p2align 4,,10
	.p2align 3
.L1648:
	cmpq	%rbx, %r14
	je	.L1481
	movq	%r14, %rdx
	movl	$8, %eax
	movq	%rbx, %rsi
	subq	%rbx, %rdx
	leaq	(%rbx,%rax), %rdi
	call	memmove@PLT
.L1481:
	movq	%r13, (%rbx)
.L1482:
	addq	$8, %r14
	cmpq	%r14, %r12
	je	.L1647
.L1485:
	movq	(%r14), %r13
	cmpq	(%rbx), %r13
	jb	.L1648
	movq	-8(%r14), %rdx
	leaq	-8(%r14), %rax
	cmpq	%r13, %rdx
	jbe	.L1541
	.p2align 4,,10
	.p2align 3
.L1484:
	movq	%rdx, 8(%rax)
	movq	%rax, %rsi
	movq	-8(%rax), %rdx
	subq	$8, %rax
	cmpq	%rdx, %r13
	jb	.L1484
.L1483:
	movq	%r13, (%rsi)
	jmp	.L1482
	.p2align 4,,10
	.p2align 3
.L1556:
	movq	$0, -88(%rbp)
.L1313:
	movl	-80(%rbp), %ecx
	salq	$3, %r14
	testl	%ecx, %ecx
	je	.L1496
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	leaq	15(%rax), %r9
	movq	%rax, -96(%rbp)
	leaq	(%r9,%r14), %r15
	cmpq	%r15, %r9
	je	.L1639
.L1497:
	movq	%r14, %rax
	movl	$63, %edx
	movq	%r9, %rdi
	movq	%r15, %rsi
	sarq	$3, %rax
	movq	%r9, -96(%rbp)
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIPllN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	movq	-96(%rbp), %r9
	leaq	8(%r9), %rcx
	cmpq	$128, %r14
	jle	.L1498
	leaq	128(%r9), %r14
	movq	%rbx, -96(%rbp)
	movq	%r9, %rbx
	movq	%r12, -104(%rbp)
	movq	%r14, %r12
	movq	%rcx, %r14
	movq	%r13, -112(%rbp)
	jmp	.L1504
	.p2align 4,,10
	.p2align 3
.L1650:
	cmpq	%rbx, %r14
	je	.L1500
	movq	%r14, %rdx
	movl	$8, %eax
	movq	%rbx, %rsi
	subq	%rbx, %rdx
	leaq	(%rbx,%rax), %rdi
	call	memmove@PLT
.L1500:
	movq	%r13, (%rbx)
.L1501:
	addq	$8, %r14
	cmpq	%r14, %r12
	je	.L1649
.L1504:
	movq	(%r14), %r13
	cmpq	(%rbx), %r13
	jl	.L1650
	movq	-8(%r14), %rdx
	leaq	-8(%r14), %rax
	cmpq	%rdx, %r13
	jge	.L1544
	.p2align 4,,10
	.p2align 3
.L1503:
	movq	%rdx, 8(%rax)
	movq	%rax, %rsi
	movq	-8(%rax), %rdx
	subq	$8, %rax
	cmpq	%rdx, %r13
	jl	.L1503
.L1502:
	movq	%r13, (%rsi)
	jmp	.L1501
	.p2align 4,,10
	.p2align 3
.L1547:
	movq	$0, -88(%rbp)
.L1345:
	movq	(%r12), %rax
	movq	55(%rax), %r9
	addq	63(%rax), %r9
	leaq	(%r9,%r14), %r15
	cmpq	%r15, %r9
	je	.L1301
.L1346:
	bsrq	%r14, %rax
	movl	$63, %edx
	movq	%r9, %rdi
	movq	%r15, %rsi
	xorq	$63, %rax
	movq	%r9, -96(%rbp)
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIPalN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	movq	-96(%rbp), %r9
	leaq	1(%r9), %rcx
	cmpq	$16, %r14
	jle	.L1347
	leaq	16(%r9), %r14
	movq	%rbx, -96(%rbp)
	movq	%r12, -104(%rbp)
	movq	%r14, %rbx
	movq	%r9, %r12
	movq	%rcx, %r14
	movq	%r13, -112(%rbp)
	jmp	.L1353
	.p2align 4,,10
	.p2align 3
.L1653:
	movq	%r14, %rdx
	subq	%r12, %rdx
	jne	.L1651
.L1349:
	movb	%r13b, (%r12)
.L1350:
	addq	$1, %r14
	cmpq	%r14, %rbx
	je	.L1652
.L1353:
	movzbl	(%r14), %r13d
	cmpb	(%r12), %r13b
	jl	.L1653
	movzbl	-1(%r14), %edx
	leaq	-1(%r14), %rax
	cmpb	%dl, %r13b
	jge	.L1523
	.p2align 4,,10
	.p2align 3
.L1352:
	movb	%dl, 1(%rax)
	movq	%rax, %rsi
	movzbl	-1(%rax), %edx
	subq	$1, %rax
	cmpb	%dl, %r13b
	jl	.L1352
.L1351:
	movb	%r13b, (%rsi)
	jmp	.L1350
	.p2align 4,,10
	.p2align 3
.L1555:
	movq	$0, -88(%rbp)
.L1458:
	movq	(%r12), %rax
	movq	55(%rax), %r9
	addq	63(%rax), %r9
	leaq	(%r9,%r14), %r15
	cmpq	%r15, %r9
	je	.L1301
.L1459:
	bsrq	%r14, %rax
	movl	$63, %edx
	movq	%r9, %rdi
	movq	%r15, %rsi
	xorq	$63, %rax
	movq	%r9, -96(%rbp)
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIPhlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	movq	-96(%rbp), %r9
	leaq	1(%r9), %rcx
	cmpq	$16, %r14
	jle	.L1460
	leaq	16(%r9), %r14
	movq	%rbx, -96(%rbp)
	movq	%r9, %rbx
	movq	%r12, -104(%rbp)
	movq	%r14, %r12
	movq	%rcx, %r14
	movq	%r13, -112(%rbp)
	jmp	.L1466
	.p2align 4,,10
	.p2align 3
.L1656:
	movq	%r14, %rdx
	subq	%rbx, %rdx
	jne	.L1654
.L1462:
	movb	%r13b, (%rbx)
.L1463:
	addq	$1, %r14
	cmpq	%r14, %r12
	je	.L1655
.L1466:
	movzbl	(%r14), %r13d
	cmpb	(%rbx), %r13b
	jb	.L1656
	movzbl	-1(%r14), %edx
	leaq	-1(%r14), %rax
	cmpb	%dl, %r13b
	jnb	.L1538
	.p2align 4,,10
	.p2align 3
.L1465:
	movb	%dl, 1(%rax)
	movq	%rax, %rsi
	movzbl	-1(%rax), %edx
	subq	$1, %rax
	cmpb	%dl, %r13b
	jb	.L1465
.L1464:
	movb	%r13b, (%rsi)
	jmp	.L1463
	.p2align 4,,10
	.p2align 3
.L1553:
	movq	$0, -88(%rbp)
.L1440:
	movq	(%r12), %rax
	movq	55(%rax), %rdi
	addq	63(%rax), %rdi
.L1441:
	salq	$2, %r14
	leaq	(%rdi,%r14), %r15
	cmpq	%r15, %rdi
	je	.L1334
	movq	%r14, %rax
	movl	$63, %edx
	movq	%r15, %rsi
	movq	%rdi, -96(%rbp)
	sarq	$2, %rax
	leaq	_ZN2v88internal12_GLOBAL__N_110CompareNumIfEEbT_S3_(%rip), %rcx
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIPflN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_S7_T0_T1_
	cmpq	$64, %r14
	movq	-96(%rbp), %rdi
	jle	.L1443
	leaq	64(%rdi), %r14
	leaq	_ZN2v88internal12_GLOBAL__N_110CompareNumIfEEbT_S3_(%rip), %rdx
	movq	%r14, %rsi
	call	_ZSt16__insertion_sortIPfN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_S7_T0_
	cmpq	%r14, %r15
	je	.L1334
	.p2align 4,,10
	.p2align 3
.L1444:
	movss	(%r14), %xmm0
	movq	%r14, %rcx
	jmp	.L1447
	.p2align 4,,10
	.p2align 3
.L1657:
	movss	%xmm1, (%rcx)
	subq	$4, %rcx
.L1447:
	movss	-4(%rcx), %xmm1
	call	_ZN2v88internal12_GLOBAL__N_110CompareNumIfEEbT_S3_
	testb	%al, %al
	jne	.L1657
	addq	$4, %r14
	movss	%xmm0, (%rcx)
	cmpq	%r14, %r15
	jne	.L1444
	jmp	.L1334
	.p2align 4,,10
	.p2align 3
.L1552:
	movq	$0, -88(%rbp)
.L1317:
	movl	-80(%rbp), %r11d
	salq	$2, %r14
	testl	%r11d, %r11d
	je	.L1402
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	leaq	15(%rax), %r9
	movq	%rax, -96(%rbp)
	leaq	(%r9,%r14), %r15
	cmpq	%r15, %r9
	je	.L1639
.L1403:
	movq	%r14, %rax
	movl	$63, %edx
	movq	%r9, %rdi
	movq	%r15, %rsi
	sarq	$2, %rax
	movq	%r9, -96(%rbp)
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIPjlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	movq	-96(%rbp), %r9
	leaq	4(%r9), %rcx
	cmpq	$64, %r14
	jle	.L1404
	leaq	64(%r9), %r14
	movq	%rbx, -96(%rbp)
	movq	%r9, %rbx
	movq	%r12, -104(%rbp)
	movq	%r14, %r12
	movq	%rcx, %r14
	movq	%r13, -112(%rbp)
	jmp	.L1410
	.p2align 4,,10
	.p2align 3
.L1659:
	cmpq	%rbx, %r14
	je	.L1406
	movq	%r14, %rdx
	movl	$4, %eax
	movq	%rbx, %rsi
	subq	%rbx, %rdx
	leaq	(%rbx,%rax), %rdi
	call	memmove@PLT
.L1406:
	movl	%r13d, (%rbx)
.L1407:
	addq	$4, %r14
	cmpq	%r14, %r12
	je	.L1658
.L1410:
	movl	(%r14), %r13d
	cmpl	(%rbx), %r13d
	jb	.L1659
	movl	-4(%r14), %edx
	leaq	-4(%r14), %rax
	cmpl	%edx, %r13d
	jnb	.L1532
	.p2align 4,,10
	.p2align 3
.L1409:
	movl	%edx, 4(%rax)
	movq	%rax, %rsi
	movl	-4(%rax), %edx
	subq	$4, %rax
	cmpl	%edx, %r13d
	jb	.L1409
.L1408:
	movl	%r13d, (%rsi)
	jmp	.L1407
	.p2align 4,,10
	.p2align 3
.L1551:
	movq	$0, -88(%rbp)
.L1318:
	movl	-80(%rbp), %r10d
	salq	$2, %r14
	testl	%r10d, %r10d
	je	.L1421
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	leaq	15(%rax), %r9
	movq	%rax, -96(%rbp)
	leaq	(%r9,%r14), %r15
	cmpq	%r15, %r9
	je	.L1639
.L1422:
	movq	%r14, %rax
	movl	$63, %edx
	movq	%r9, %rdi
	movq	%r15, %rsi
	sarq	$2, %rax
	movq	%r9, -96(%rbp)
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIPilN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	movq	-96(%rbp), %r9
	leaq	4(%r9), %rcx
	cmpq	$64, %r14
	jle	.L1423
	leaq	64(%r9), %r14
	movq	%rbx, -96(%rbp)
	movq	%r9, %rbx
	movq	%r12, -104(%rbp)
	movq	%r14, %r12
	movq	%rcx, %r14
	movq	%r13, -112(%rbp)
	jmp	.L1429
	.p2align 4,,10
	.p2align 3
.L1661:
	cmpq	%rbx, %r14
	je	.L1425
	movq	%r14, %rdx
	movl	$4, %eax
	movq	%rbx, %rsi
	subq	%rbx, %rdx
	leaq	(%rbx,%rax), %rdi
	call	memmove@PLT
.L1425:
	movl	%r13d, (%rbx)
.L1426:
	addq	$4, %r14
	cmpq	%r14, %r12
	je	.L1660
.L1429:
	movl	(%r14), %r13d
	cmpl	(%rbx), %r13d
	jl	.L1661
	movl	-4(%r14), %edx
	leaq	-4(%r14), %rax
	cmpl	%edx, %r13d
	jge	.L1535
	.p2align 4,,10
	.p2align 3
.L1428:
	movl	%edx, 4(%rax)
	movq	%rax, %rsi
	movl	-4(%rax), %edx
	subq	$4, %rax
	cmpl	%edx, %r13d
	jl	.L1428
.L1427:
	movl	%r13d, (%rsi)
	jmp	.L1426
	.p2align 4,,10
	.p2align 3
.L1549:
	movq	$0, -88(%rbp)
.L1320:
	movl	-80(%rbp), %r15d
	addq	%r14, %r14
	testl	%r15d, %r15d
	je	.L1383
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	leaq	15(%rax), %r9
	movq	%rax, -96(%rbp)
	leaq	(%r9,%r14), %r15
	cmpq	%r15, %r9
	je	.L1639
.L1384:
	movq	%r14, %rax
	movl	$63, %edx
	movq	%r9, %rdi
	movq	%r15, %rsi
	sarq	%rax
	movq	%r9, -96(%rbp)
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIPslN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	movq	-96(%rbp), %r9
	leaq	2(%r9), %rcx
	cmpq	$32, %r14
	jle	.L1385
	leaq	32(%r9), %r14
	movq	%rbx, -96(%rbp)
	movq	%r9, %rbx
	movq	%r12, -104(%rbp)
	movq	%r14, %r12
	movq	%rcx, %r14
	movq	%r13, -112(%rbp)
	jmp	.L1391
	.p2align 4,,10
	.p2align 3
.L1663:
	cmpq	%rbx, %r14
	je	.L1387
	movq	%r14, %rdx
	movl	$2, %eax
	movq	%rbx, %rsi
	subq	%rbx, %rdx
	leaq	(%rbx,%rax), %rdi
	call	memmove@PLT
.L1387:
	movw	%r13w, (%rbx)
.L1388:
	addq	$2, %r14
	cmpq	%r14, %r12
	je	.L1662
.L1391:
	movzwl	(%r14), %r13d
	cmpw	(%rbx), %r13w
	jl	.L1663
	movzwl	-2(%r14), %edx
	leaq	-2(%r14), %rax
	cmpw	%dx, %r13w
	jge	.L1529
	.p2align 4,,10
	.p2align 3
.L1390:
	movw	%dx, 2(%rax)
	movq	%rax, %rsi
	movzwl	-2(%rax), %edx
	subq	$2, %rax
	cmpw	%dx, %r13w
	jl	.L1390
.L1389:
	movw	%r13w, (%rsi)
	jmp	.L1388
	.p2align 4,,10
	.p2align 3
.L1550:
	movq	$0, -88(%rbp)
.L1319:
	movl	-80(%rbp), %eax
	addq	%r14, %r14
	testl	%eax, %eax
	je	.L1364
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	leaq	15(%rax), %r9
	movq	%rax, -96(%rbp)
	leaq	(%r9,%r14), %r15
	cmpq	%r15, %r9
	je	.L1639
.L1365:
	movq	%r14, %rax
	movl	$63, %edx
	movq	%r9, %rdi
	movq	%r15, %rsi
	sarq	%rax
	movq	%r9, -96(%rbp)
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIPtlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	movq	-96(%rbp), %r9
	leaq	2(%r9), %rcx
	cmpq	$32, %r14
	jle	.L1366
	leaq	32(%r9), %r14
	movq	%rbx, -96(%rbp)
	movq	%r9, %rbx
	movq	%r12, -104(%rbp)
	movq	%r14, %r12
	movq	%rcx, %r14
	movq	%r13, -112(%rbp)
	jmp	.L1372
	.p2align 4,,10
	.p2align 3
.L1665:
	cmpq	%rbx, %r14
	je	.L1368
	movq	%r14, %rdx
	movl	$2, %eax
	movq	%rbx, %rsi
	subq	%rbx, %rdx
	leaq	(%rbx,%rax), %rdi
	call	memmove@PLT
.L1368:
	movw	%r13w, (%rbx)
.L1369:
	addq	$2, %r14
	cmpq	%r14, %r12
	je	.L1664
.L1372:
	movzwl	(%r14), %r13d
	cmpw	(%rbx), %r13w
	jb	.L1665
	movzwl	-2(%r14), %edx
	leaq	-2(%r14), %rax
	cmpw	%dx, %r13w
	jnb	.L1526
	.p2align 4,,10
	.p2align 3
.L1371:
	movw	%dx, 2(%rax)
	movq	%rax, %rsi
	movzwl	-2(%rax), %edx
	subq	$2, %rax
	cmpw	%dx, %r13w
	jb	.L1371
.L1370:
	movw	%r13w, (%rsi)
	jmp	.L1369
	.p2align 4,,10
	.p2align 3
.L1548:
	movq	$0, -88(%rbp)
.L1324:
	movq	(%r12), %rax
	movq	55(%rax), %r9
	addq	63(%rax), %r9
	leaq	(%r9,%r14), %r15
	cmpq	%r15, %r9
	je	.L1301
.L1325:
	bsrq	%r14, %rax
	movl	$63, %edx
	movq	%r9, %rdi
	movq	%r15, %rsi
	xorq	$63, %rax
	movq	%r9, -96(%rbp)
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIPhlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	movq	-96(%rbp), %r9
	leaq	1(%r9), %rcx
	cmpq	$16, %r14
	jle	.L1327
	leaq	16(%r9), %r14
	movq	%rbx, -96(%rbp)
	movq	%r12, -104(%rbp)
	movq	%r14, %rbx
	movq	%r9, %r12
	movq	%rcx, %r14
	movq	%r13, -112(%rbp)
	jmp	.L1333
	.p2align 4,,10
	.p2align 3
.L1668:
	movq	%r14, %rdx
	subq	%r12, %rdx
	jne	.L1666
.L1329:
	movb	%r13b, (%r12)
.L1330:
	addq	$1, %r14
	cmpq	%r14, %rbx
	je	.L1667
.L1333:
	movzbl	(%r14), %r13d
	cmpb	(%r12), %r13b
	jb	.L1668
	movzbl	-1(%r14), %edx
	leaq	-1(%r14), %rax
	cmpb	%r13b, %dl
	jbe	.L1520
	.p2align 4,,10
	.p2align 3
.L1332:
	movb	%dl, 1(%rax)
	movq	%rax, %rsi
	movzbl	-1(%rax), %edx
	subq	$1, %rax
	cmpb	%dl, %r13b
	jb	.L1332
.L1331:
	movb	%r13b, (%rsi)
	jmp	.L1330
	.p2align 4,,10
	.p2align 3
.L1314:
	movl	-80(%rbp), %edi
	testl	%edi, %edi
	je	.L1458
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	leaq	15(%rax), %r9
	movq	%rax, -96(%rbp)
	leaq	(%r9,%r14), %r15
	cmpq	%r15, %r9
	jne	.L1459
	.p2align 4,,10
	.p2align 3
.L1639:
	movq	(%r12), %rax
	jmp	.L1323
	.p2align 4,,10
	.p2align 3
.L1321:
	movl	-80(%rbp), %eax
	testl	%eax, %eax
	je	.L1324
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	leaq	15(%rax), %r9
	movq	%rax, -96(%rbp)
	leaq	(%r9,%r14), %r15
	cmpq	%r15, %r9
	jne	.L1325
	jmp	.L1639
	.p2align 4,,10
	.p2align 3
.L1322:
	movl	-80(%rbp), %eax
	testl	%eax, %eax
	je	.L1345
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	leaq	15(%rax), %r9
	movq	%rax, -96(%rbp)
	leaq	(%r9,%r14), %r15
	cmpq	%r15, %r9
	jne	.L1346
	jmp	.L1639
	.p2align 4,,10
	.p2align 3
.L1315:
	movl	-80(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L1449
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -96(%rbp)
	leaq	15(%rax), %rdi
	jmp	.L1450
	.p2align 4,,10
	.p2align 3
.L1316:
	movl	-80(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L1440
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -96(%rbp)
	leaq	15(%rax), %rdi
	jmp	.L1441
	.p2align 4,,10
	.p2align 3
.L1646:
	movq	%r13, %rdi
	movq	%r15, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	jmp	.L1307
.L1517:
	movq	(%r12), %rax
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1421:
	movq	(%r12), %rax
	movq	55(%rax), %r9
	addq	63(%rax), %r9
	leaq	(%r9,%r14), %r15
	cmpq	%r15, %r9
	jne	.L1422
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1402:
	movq	(%r12), %rax
	movq	55(%rax), %r9
	addq	63(%rax), %r9
	leaq	(%r9,%r14), %r15
	cmpq	%r15, %r9
	jne	.L1403
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1496:
	movq	(%r12), %rax
	movq	55(%rax), %r9
	addq	63(%rax), %r9
	leaq	(%r9,%r14), %r15
	cmpq	%r15, %r9
	jne	.L1497
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1477:
	movq	(%r12), %rax
	movq	55(%rax), %r9
	addq	63(%rax), %r9
	leaq	(%r9,%r14), %r15
	cmpq	%r15, %r9
	jne	.L1478
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1383:
	movq	(%r12), %rax
	movq	55(%rax), %r9
	addq	63(%rax), %r9
	leaq	(%r9,%r14), %r15
	cmpq	%r15, %r9
	jne	.L1384
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1364:
	movq	(%r12), %rax
	movq	55(%rax), %r9
	addq	63(%rax), %r9
	leaq	(%r9,%r14), %r15
	cmpq	%r15, %r9
	jne	.L1365
	jmp	.L1301
.L1310:
	movq	-88(%rbp), %rax
	movq	(%rax), %r15
	movq	(%r12), %rax
	addq	$15, %r15
	jmp	.L1323
	.p2align 4,,10
	.p2align 3
.L1666:
	movl	$1, %edi
	movq	%r12, %rsi
	subq	%rdx, %rdi
	addq	%r14, %rdi
	call	memmove@PLT
	jmp	.L1329
	.p2align 4,,10
	.p2align 3
.L1651:
	movl	$1, %edi
	movq	%r12, %rsi
	subq	%rdx, %rdi
	addq	%r14, %rdi
	call	memmove@PLT
	jmp	.L1349
	.p2align 4,,10
	.p2align 3
.L1654:
	movl	$1, %edi
	movq	%rbx, %rsi
	subq	%rdx, %rdi
	addq	%r14, %rdi
	call	memmove@PLT
	jmp	.L1462
.L1366:
	cmpq	%r15, %rcx
	je	.L1334
	movq	%rbx, -96(%rbp)
	movq	%rcx, %r14
	movq	%r15, %rbx
	movq	%r12, -104(%rbp)
	movq	%r9, %r12
	jmp	.L1376
	.p2align 4,,10
	.p2align 3
.L1669:
	cmpq	%r12, %r14
	je	.L1378
	movq	%r14, %rdx
	movl	$2, %eax
	movq	%r12, %rsi
	subq	%r12, %rdx
	leaq	(%r12,%rax), %rdi
	call	memmove@PLT
.L1378:
	movw	%r15w, (%r12)
.L1379:
	addq	$2, %r14
	cmpq	%rbx, %r14
	je	.L1638
.L1376:
	movzwl	(%r14), %r15d
	cmpw	(%r12), %r15w
	jb	.L1669
	movzwl	-2(%r14), %edx
	leaq	-2(%r14), %rax
	cmpw	%dx, %r15w
	jnb	.L1528
	.p2align 4,,10
	.p2align 3
.L1381:
	movw	%dx, 2(%rax)
	movq	%rax, %rcx
	movzwl	-2(%rax), %edx
	subq	$2, %rax
	cmpw	%dx, %r15w
	jb	.L1381
.L1380:
	movw	%r15w, (%rcx)
	jmp	.L1379
	.p2align 4,,10
	.p2align 3
.L1638:
	movq	-96(%rbp), %rbx
	movq	-104(%rbp), %r12
	jmp	.L1334
.L1652:
	movq	%rbx, %r14
	movq	-104(%rbp), %r12
	movq	-96(%rbp), %rbx
	movq	-112(%rbp), %r13
	cmpq	%r15, %r14
	je	.L1334
	.p2align 4,,10
	.p2align 3
.L1356:
	movzbl	(%r14), %ecx
	movzbl	-1(%r14), %edx
	leaq	-1(%r14), %rax
	cmpb	%dl, %cl
	jge	.L1524
	.p2align 4,,10
	.p2align 3
.L1355:
	movb	%dl, 1(%rax)
	movq	%rax, %rsi
	movzbl	-1(%rax), %edx
	subq	$1, %rax
	cmpb	%dl, %cl
	jl	.L1355
.L1354:
	addq	$1, %r14
	movb	%cl, (%rsi)
	cmpq	%r15, %r14
	jne	.L1356
	jmp	.L1334
.L1655:
	movq	%r12, %r14
	movq	-96(%rbp), %rbx
	movq	-104(%rbp), %r12
	movq	-112(%rbp), %r13
	cmpq	%r15, %r14
	je	.L1334
	.p2align 4,,10
	.p2align 3
.L1469:
	movzbl	(%r14), %ecx
	movzbl	-1(%r14), %edx
	leaq	-1(%r14), %rax
	cmpb	%cl, %dl
	jbe	.L1539
	.p2align 4,,10
	.p2align 3
.L1468:
	movb	%dl, 1(%rax)
	movq	%rax, %rsi
	movzbl	-1(%rax), %edx
	subq	$1, %rax
	cmpb	%dl, %cl
	jb	.L1468
.L1467:
	addq	$1, %r14
	movb	%cl, (%rsi)
	cmpq	%r15, %r14
	jne	.L1469
	jmp	.L1334
.L1385:
	cmpq	%r15, %rcx
	je	.L1334
	movq	%rbx, -96(%rbp)
	movq	%rcx, %r14
	movq	%r15, %rbx
	movq	%r12, -104(%rbp)
	movq	%r9, %r12
	jmp	.L1395
	.p2align 4,,10
	.p2align 3
.L1670:
	cmpq	%r12, %r14
	je	.L1397
	movq	%r14, %rdx
	movl	$2, %eax
	movq	%r12, %rsi
	subq	%r12, %rdx
	leaq	(%r12,%rax), %rdi
	call	memmove@PLT
.L1397:
	movw	%r15w, (%r12)
.L1398:
	addq	$2, %r14
	cmpq	%rbx, %r14
	je	.L1638
.L1395:
	movzwl	(%r14), %r15d
	cmpw	(%r12), %r15w
	jl	.L1670
	movzwl	-2(%r14), %edx
	leaq	-2(%r14), %rax
	cmpw	%dx, %r15w
	jge	.L1531
	.p2align 4,,10
	.p2align 3
.L1400:
	movw	%dx, 2(%rax)
	movq	%rax, %rcx
	movzwl	-2(%rax), %edx
	subq	$2, %rax
	cmpw	%dx, %r15w
	jl	.L1400
.L1399:
	movw	%r15w, (%rcx)
	jmp	.L1398
.L1347:
	cmpq	%r15, %rcx
	je	.L1334
	movq	%rbx, -96(%rbp)
	movq	%rcx, %r14
	movq	%r15, %rbx
	movq	%r12, -104(%rbp)
	movq	%r9, %r12
	jmp	.L1357
	.p2align 4,,10
	.p2align 3
.L1672:
	movq	%r14, %rdx
	subq	%r12, %rdx
	jne	.L1671
.L1359:
	movb	%r15b, (%r12)
.L1360:
	addq	$1, %r14
	cmpq	%rbx, %r14
	je	.L1638
.L1357:
	movzbl	(%r14), %r15d
	cmpb	(%r12), %r15b
	jl	.L1672
	movzbl	-1(%r14), %edx
	leaq	-1(%r14), %rax
	cmpb	%dl, %r15b
	jge	.L1525
	.p2align 4,,10
	.p2align 3
.L1362:
	movb	%dl, 1(%rax)
	movq	%rax, %rcx
	movzbl	-1(%rax), %edx
	subq	$1, %rax
	cmpb	%dl, %r15b
	jl	.L1362
.L1361:
	movb	%r15b, (%rcx)
	jmp	.L1360
	.p2align 4,,10
	.p2align 3
.L1671:
	movl	$1, %edi
	movq	%r12, %rsi
	subq	%rdx, %rdi
	addq	%r14, %rdi
	call	memmove@PLT
	jmp	.L1359
.L1327:
	cmpq	%r15, %rcx
	je	.L1334
	movq	%rbx, -96(%rbp)
	movq	%rcx, %r14
	movq	%r15, %rbx
	movq	%r12, -104(%rbp)
	movq	%r9, %r12
	jmp	.L1338
	.p2align 4,,10
	.p2align 3
.L1674:
	movq	%r14, %rdx
	subq	%r12, %rdx
	jne	.L1673
.L1340:
	movb	%r15b, (%r12)
.L1341:
	addq	$1, %r14
	cmpq	%rbx, %r14
	je	.L1638
.L1338:
	movzbl	(%r14), %r15d
	cmpb	(%r12), %r15b
	jb	.L1674
	movzbl	-1(%r14), %edx
	leaq	-1(%r14), %rax
	cmpb	%dl, %r15b
	jnb	.L1522
	.p2align 4,,10
	.p2align 3
.L1343:
	movb	%dl, 1(%rax)
	movq	%rax, %rcx
	movzbl	-1(%rax), %edx
	subq	$1, %rax
	cmpb	%dl, %r15b
	jb	.L1343
.L1342:
	movb	%r15b, (%rcx)
	jmp	.L1341
	.p2align 4,,10
	.p2align 3
.L1673:
	movl	$1, %edi
	movq	%r12, %rsi
	subq	%rdx, %rdi
	addq	%r14, %rdi
	call	memmove@PLT
	jmp	.L1340
.L1423:
	cmpq	%r15, %rcx
	je	.L1334
	movq	%rbx, -96(%rbp)
	movq	%rcx, %r14
	movq	%r15, %rbx
	movq	%r12, -104(%rbp)
	movq	%r9, %r12
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L1675:
	cmpq	%r12, %r14
	je	.L1435
	movq	%r14, %rdx
	movl	$4, %eax
	movq	%r12, %rsi
	subq	%r12, %rdx
	leaq	(%r12,%rax), %rdi
	call	memmove@PLT
.L1435:
	movl	%r15d, (%r12)
.L1436:
	addq	$4, %r14
	cmpq	%rbx, %r14
	je	.L1638
.L1433:
	movl	(%r14), %r15d
	cmpl	(%r12), %r15d
	jl	.L1675
	movl	-4(%r14), %edx
	leaq	-4(%r14), %rax
	cmpl	%edx, %r15d
	jge	.L1537
	.p2align 4,,10
	.p2align 3
.L1438:
	movl	%edx, 4(%rax)
	movq	%rax, %rcx
	movl	-4(%rax), %edx
	subq	$4, %rax
	cmpl	%edx, %r15d
	jl	.L1438
.L1437:
	movl	%r15d, (%rcx)
	jmp	.L1436
.L1479:
	cmpq	%r15, %rcx
	je	.L1334
	movq	%rbx, -96(%rbp)
	movq	%rcx, %r14
	movq	%r15, %rbx
	movq	%r12, -104(%rbp)
	movq	%r9, %r12
	jmp	.L1489
	.p2align 4,,10
	.p2align 3
.L1676:
	cmpq	%r12, %r14
	je	.L1491
	movq	%r14, %rdx
	movl	$8, %eax
	movq	%r12, %rsi
	subq	%r12, %rdx
	leaq	(%r12,%rax), %rdi
	call	memmove@PLT
.L1491:
	movq	%r15, (%r12)
.L1492:
	addq	$8, %r14
	cmpq	%rbx, %r14
	je	.L1638
.L1489:
	movq	(%r14), %r15
	cmpq	(%r12), %r15
	jb	.L1676
	movq	-8(%r14), %rdx
	leaq	-8(%r14), %rax
	cmpq	%r15, %rdx
	jbe	.L1543
	.p2align 4,,10
	.p2align 3
.L1494:
	movq	%rdx, 8(%rax)
	movq	%rax, %rcx
	movq	-8(%rax), %rdx
	subq	$8, %rax
	cmpq	%rdx, %r15
	jb	.L1494
.L1493:
	movq	%r15, (%rcx)
	jmp	.L1492
.L1498:
	cmpq	%r15, %rcx
	je	.L1334
	movq	%rbx, -96(%rbp)
	movq	%rcx, %r14
	movq	%r15, %rbx
	movq	%r12, -104(%rbp)
	movq	%r9, %r12
	jmp	.L1508
	.p2align 4,,10
	.p2align 3
.L1677:
	cmpq	%r12, %r14
	je	.L1510
	movq	%r14, %rdx
	movl	$8, %eax
	movq	%r12, %rsi
	subq	%r12, %rdx
	leaq	(%r12,%rax), %rdi
	call	memmove@PLT
.L1510:
	movq	%r15, (%r12)
.L1511:
	addq	$8, %r14
	cmpq	%rbx, %r14
	je	.L1638
.L1508:
	movq	(%r14), %r15
	cmpq	(%r12), %r15
	jl	.L1677
	movq	-8(%r14), %rdx
	leaq	-8(%r14), %rax
	cmpq	%rdx, %r15
	jge	.L1546
	.p2align 4,,10
	.p2align 3
.L1513:
	movq	%rdx, 8(%rax)
	movq	%rax, %rcx
	movq	-8(%rax), %rdx
	subq	$8, %rax
	cmpq	%rdx, %r15
	jl	.L1513
.L1512:
	movq	%r15, (%rcx)
	jmp	.L1511
.L1460:
	cmpq	%r15, %rcx
	je	.L1334
	movq	%rbx, -96(%rbp)
	movq	%rcx, %r14
	movq	%r15, %rbx
	movq	%r12, -104(%rbp)
	movq	%r9, %r12
	jmp	.L1470
	.p2align 4,,10
	.p2align 3
.L1679:
	movq	%r14, %rdx
	subq	%r12, %rdx
	jne	.L1678
.L1472:
	movb	%r15b, (%r12)
.L1473:
	addq	$1, %r14
	cmpq	%rbx, %r14
	je	.L1638
.L1470:
	movzbl	(%r14), %r15d
	cmpb	(%r12), %r15b
	jb	.L1679
	movzbl	-1(%r14), %edx
	leaq	-1(%r14), %rax
	cmpb	%r15b, %dl
	jbe	.L1540
	.p2align 4,,10
	.p2align 3
.L1475:
	movb	%dl, 1(%rax)
	movq	%rax, %rcx
	movzbl	-1(%rax), %edx
	subq	$1, %rax
	cmpb	%dl, %r15b
	jb	.L1475
.L1474:
	movb	%r15b, (%rcx)
	jmp	.L1473
	.p2align 4,,10
	.p2align 3
.L1678:
	movl	$1, %edi
	movq	%r12, %rsi
	subq	%rdx, %rdi
	addq	%r14, %rdi
	call	memmove@PLT
	jmp	.L1472
.L1404:
	cmpq	%r15, %rcx
	je	.L1334
	movq	%rbx, -96(%rbp)
	movq	%rcx, %r14
	movq	%r15, %rbx
	movq	%r12, -104(%rbp)
	movq	%r9, %r12
	jmp	.L1414
	.p2align 4,,10
	.p2align 3
.L1680:
	cmpq	%r12, %r14
	je	.L1416
	movq	%r14, %rdx
	movl	$4, %eax
	movq	%r12, %rsi
	subq	%r12, %rdx
	leaq	(%r12,%rax), %rdi
	call	memmove@PLT
.L1416:
	movl	%r15d, (%r12)
.L1417:
	addq	$4, %r14
	cmpq	%rbx, %r14
	je	.L1638
.L1414:
	movl	(%r14), %r15d
	cmpl	(%r12), %r15d
	jb	.L1680
	movl	-4(%r14), %edx
	leaq	-4(%r14), %rax
	cmpl	%edx, %r15d
	jnb	.L1534
	.p2align 4,,10
	.p2align 3
.L1419:
	movl	%edx, 4(%rax)
	movq	%rax, %rcx
	movl	-4(%rax), %edx
	subq	$4, %rax
	cmpl	%edx, %r15d
	jb	.L1419
.L1418:
	movl	%r15d, (%rcx)
	jmp	.L1417
.L1649:
	movq	%r12, %r14
	movq	-96(%rbp), %rbx
	movq	-104(%rbp), %r12
	movq	-112(%rbp), %r13
	cmpq	%r15, %r14
	je	.L1334
	.p2align 4,,10
	.p2align 3
.L1507:
	movq	(%r14), %rcx
	movq	-8(%r14), %rdx
	leaq	-8(%r14), %rax
	cmpq	%rcx, %rdx
	jle	.L1545
	.p2align 4,,10
	.p2align 3
.L1506:
	movq	%rdx, 8(%rax)
	movq	%rax, %rsi
	movq	-8(%rax), %rdx
	subq	$8, %rax
	cmpq	%rdx, %rcx
	jl	.L1506
.L1505:
	addq	$8, %r14
	movq	%rcx, (%rsi)
	cmpq	%r15, %r14
	jne	.L1507
	jmp	.L1334
.L1667:
	movq	%rbx, %r14
	movq	-104(%rbp), %r12
	movq	-96(%rbp), %rbx
	movq	-112(%rbp), %r13
	cmpq	%r15, %r14
	je	.L1334
	.p2align 4,,10
	.p2align 3
.L1337:
	movzbl	(%r14), %ecx
	movzbl	-1(%r14), %edx
	leaq	-1(%r14), %rax
	cmpb	%cl, %dl
	jbe	.L1521
	.p2align 4,,10
	.p2align 3
.L1336:
	movb	%dl, 1(%rax)
	movq	%rax, %rsi
	movzbl	-1(%rax), %edx
	subq	$1, %rax
	cmpb	%dl, %cl
	jb	.L1336
.L1335:
	addq	$1, %r14
	movb	%cl, (%rsi)
	cmpq	%r15, %r14
	jne	.L1337
	jmp	.L1334
.L1658:
	movq	%r12, %r14
	movq	-96(%rbp), %rbx
	movq	-104(%rbp), %r12
	movq	-112(%rbp), %r13
	cmpq	%r15, %r14
	je	.L1334
	.p2align 4,,10
	.p2align 3
.L1413:
	movl	(%r14), %ecx
	movl	-4(%r14), %edx
	leaq	-4(%r14), %rax
	cmpl	%edx, %ecx
	jnb	.L1533
	.p2align 4,,10
	.p2align 3
.L1412:
	movl	%edx, 4(%rax)
	movq	%rax, %rsi
	movl	-4(%rax), %edx
	subq	$4, %rax
	cmpl	%edx, %ecx
	jb	.L1412
.L1411:
	addq	$4, %r14
	movl	%ecx, (%rsi)
	cmpq	%r15, %r14
	jne	.L1413
	jmp	.L1334
.L1662:
	movq	%r12, %r14
	movq	-96(%rbp), %rbx
	movq	-104(%rbp), %r12
	movq	-112(%rbp), %r13
	cmpq	%r15, %r14
	je	.L1334
	.p2align 4,,10
	.p2align 3
.L1394:
	movzwl	(%r14), %ecx
	movzwl	-2(%r14), %edx
	leaq	-2(%r14), %rax
	cmpw	%cx, %dx
	jle	.L1530
	.p2align 4,,10
	.p2align 3
.L1393:
	movw	%dx, 2(%rax)
	movq	%rax, %rsi
	movzwl	-2(%rax), %edx
	subq	$2, %rax
	cmpw	%dx, %cx
	jl	.L1393
.L1392:
	addq	$2, %r14
	movw	%cx, (%rsi)
	cmpq	%r15, %r14
	jne	.L1394
	jmp	.L1334
.L1443:
	leaq	_ZN2v88internal12_GLOBAL__N_110CompareNumIfEEbT_S3_(%rip), %rdx
	movq	%r15, %rsi
	call	_ZSt16__insertion_sortIPfN9__gnu_cxx5__ops15_Iter_comp_iterIPFbffEEEEvT_S7_T0_
	jmp	.L1334
.L1452:
	leaq	_ZN2v88internal12_GLOBAL__N_110CompareNumIdEEbT_S3_(%rip), %rdx
	movq	%r15, %rsi
	call	_ZSt16__insertion_sortIPdN9__gnu_cxx5__ops15_Iter_comp_iterIPFbddEEEEvT_S7_T0_
	jmp	.L1334
.L1647:
	movq	%r12, %r14
	movq	-96(%rbp), %rbx
	movq	-104(%rbp), %r12
	movq	-112(%rbp), %r13
	cmpq	%r15, %r14
	je	.L1334
	.p2align 4,,10
	.p2align 3
.L1488:
	movq	(%r14), %rcx
	movq	-8(%r14), %rdx
	leaq	-8(%r14), %rax
	cmpq	%rdx, %rcx
	jnb	.L1542
	.p2align 4,,10
	.p2align 3
.L1487:
	movq	%rdx, 8(%rax)
	movq	%rax, %rsi
	movq	-8(%rax), %rdx
	subq	$8, %rax
	cmpq	%rdx, %rcx
	jb	.L1487
.L1486:
	addq	$8, %r14
	movq	%rcx, (%rsi)
	cmpq	%r15, %r14
	jne	.L1488
	jmp	.L1334
.L1660:
	movq	%r12, %r14
	movq	-96(%rbp), %rbx
	movq	-104(%rbp), %r12
	movq	-112(%rbp), %r13
	cmpq	%r15, %r14
	je	.L1334
	.p2align 4,,10
	.p2align 3
.L1432:
	movl	(%r14), %ecx
	movl	-4(%r14), %edx
	leaq	-4(%r14), %rax
	cmpl	%edx, %ecx
	jge	.L1536
	.p2align 4,,10
	.p2align 3
.L1431:
	movl	%edx, 4(%rax)
	movq	%rax, %rsi
	movl	-4(%rax), %edx
	subq	$4, %rax
	cmpl	%edx, %ecx
	jl	.L1431
.L1430:
	addq	$4, %r14
	movl	%ecx, (%rsi)
	cmpq	%r15, %r14
	jne	.L1432
	jmp	.L1334
.L1664:
	movq	%r12, %r14
	movq	-96(%rbp), %rbx
	movq	-104(%rbp), %r12
	movq	-112(%rbp), %r13
	cmpq	%r15, %r14
	je	.L1334
	.p2align 4,,10
	.p2align 3
.L1375:
	movzwl	(%r14), %ecx
	movzwl	-2(%r14), %edx
	leaq	-2(%r14), %rax
	cmpw	%dx, %cx
	jnb	.L1527
	.p2align 4,,10
	.p2align 3
.L1374:
	movw	%dx, 2(%rax)
	movq	%rax, %rsi
	movzwl	-2(%rax), %edx
	subq	$2, %rax
	cmpw	%dx, %cx
	jb	.L1374
.L1373:
	addq	$2, %r14
	movw	%cx, (%rsi)
	cmpq	%r15, %r14
	jne	.L1375
	jmp	.L1334
.L1527:
	movq	%r14, %rsi
	jmp	.L1373
.L1536:
	movq	%r14, %rsi
	jmp	.L1430
.L1539:
	movq	%r14, %rsi
	jmp	.L1467
.L1524:
	movq	%r14, %rsi
	jmp	.L1354
.L1542:
	movq	%r14, %rsi
	jmp	.L1486
.L1530:
	movq	%r14, %rsi
	jmp	.L1392
.L1533:
	movq	%r14, %rsi
	jmp	.L1411
.L1521:
	movq	%r14, %rsi
	jmp	.L1335
.L1545:
	movq	%r14, %rsi
	jmp	.L1505
.L1538:
	movq	%r14, %rsi
	jmp	.L1464
.L1523:
	movq	%r14, %rsi
	jmp	.L1351
.L1528:
	movq	%r14, %rcx
	jmp	.L1380
.L1526:
	movq	%r14, %rsi
	jmp	.L1370
.L1541:
	movq	%r14, %rsi
	jmp	.L1483
.L1544:
	movq	%r14, %rsi
	jmp	.L1502
.L1534:
	movq	%r14, %rcx
	jmp	.L1418
.L1540:
	movq	%r14, %rcx
	jmp	.L1474
.L1520:
	movq	%r14, %rsi
	jmp	.L1331
.L1529:
	movq	%r14, %rsi
	jmp	.L1389
.L1532:
	movq	%r14, %rsi
	jmp	.L1408
.L1535:
	movq	%r14, %rsi
	jmp	.L1427
.L1525:
	movq	%r14, %rcx
	jmp	.L1361
.L1531:
	movq	%r14, %rcx
	jmp	.L1399
.L1546:
	movq	%r14, %rcx
	jmp	.L1512
.L1543:
	movq	%r14, %rcx
	jmp	.L1493
.L1537:
	movq	%r14, %rcx
	jmp	.L1437
.L1522:
	movq	%r14, %rcx
	jmp	.L1342
.L1644:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20042:
	.size	_ZN2v88internal26Runtime_TypedArraySortFastEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_TypedArraySortFastEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal25Runtime_ArrayBufferDetachEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal25Runtime_ArrayBufferDetachEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal25Runtime_ArrayBufferDetachEiPmPNS0_7IsolateE:
.LFB26422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE26422:
	.size	_GLOBAL__sub_I__ZN2v88internal25Runtime_ArrayBufferDetachEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal25Runtime_ArrayBufferDetachEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal25Runtime_ArrayBufferDetachEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL27Stats_Runtime_TypedArraySetEiPmPNS0_7IsolateEE28trace_event_unique_atomic168,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL27Stats_Runtime_TypedArraySetEiPmPNS0_7IsolateEE28trace_event_unique_atomic168, @object
	.size	_ZZN2v88internalL27Stats_Runtime_TypedArraySetEiPmPNS0_7IsolateEE28trace_event_unique_atomic168, 8
_ZZN2v88internalL27Stats_Runtime_TypedArraySetEiPmPNS0_7IsolateEE28trace_event_unique_atomic168:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_TypedArraySortFastEiPmPNS0_7IsolateEE27trace_event_unique_atomic94,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_TypedArraySortFastEiPmPNS0_7IsolateEE27trace_event_unique_atomic94, @object
	.size	_ZZN2v88internalL32Stats_Runtime_TypedArraySortFastEiPmPNS0_7IsolateEE27trace_event_unique_atomic94, 8
_ZZN2v88internalL32Stats_Runtime_TypedArraySortFastEiPmPNS0_7IsolateEE27trace_event_unique_atomic94:
	.zero	8
	.section	.bss._ZZN2v88internalL33Stats_Runtime_TypedArrayGetBufferEiPmPNS0_7IsolateEE27trace_event_unique_atomic63,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_TypedArrayGetBufferEiPmPNS0_7IsolateEE27trace_event_unique_atomic63, @object
	.size	_ZZN2v88internalL33Stats_Runtime_TypedArrayGetBufferEiPmPNS0_7IsolateEE27trace_event_unique_atomic63, 8
_ZZN2v88internalL33Stats_Runtime_TypedArrayGetBufferEiPmPNS0_7IsolateEE27trace_event_unique_atomic63:
	.zero	8
	.section	.bss._ZZN2v88internalL36Stats_Runtime_TypedArrayCopyElementsEiPmPNS0_7IsolateEE27trace_event_unique_atomic49,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Stats_Runtime_TypedArrayCopyElementsEiPmPNS0_7IsolateEE27trace_event_unique_atomic49, @object
	.size	_ZZN2v88internalL36Stats_Runtime_TypedArrayCopyElementsEiPmPNS0_7IsolateEE27trace_event_unique_atomic49, 8
_ZZN2v88internalL36Stats_Runtime_TypedArrayCopyElementsEiPmPNS0_7IsolateEE27trace_event_unique_atomic49:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_ArrayBufferDetachEiPmPNS0_7IsolateEE27trace_event_unique_atomic19,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_ArrayBufferDetachEiPmPNS0_7IsolateEE27trace_event_unique_atomic19, @object
	.size	_ZZN2v88internalL31Stats_Runtime_ArrayBufferDetachEiPmPNS0_7IsolateEE27trace_event_unique_atomic19, 8
_ZZN2v88internalL31Stats_Runtime_ArrayBufferDetachEiPmPNS0_7IsolateEE27trace_event_unique_atomic19:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	0
	.align 8
.LC7:
	.long	0
	.long	1139802112
	.align 8
.LC8:
	.long	0
	.long	1138753536
	.align 8
.LC15:
	.long	0
	.long	1127219200
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
