	.file	"address-map.cc"
	.text
	.section	.text._ZNK2v88internal12RootIndexMap6LookupEmPNS0_9RootIndexE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12RootIndexMap6LookupEmPNS0_9RootIndexE
	.type	_ZNK2v88internal12RootIndexMap6LookupEmPNS0_9RootIndexE, @function
_ZNK2v88internal12RootIndexMap6LookupEmPNS0_9RootIndexE:
.LFB17800:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	8(%rax), %ecx
	movq	(%rax), %r9
	subl	$1, %ecx
	movl	%ecx, %eax
	andl	%esi, %eax
	leaq	(%rax,%rax,2), %rdi
	leaq	(%r9,%rdi,8), %rdi
	movzbl	16(%rdi), %r8d
	testb	%r8b, %r8b
	jne	.L4
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L11:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdi
	leaq	(%r9,%rdi,8), %rdi
	cmpb	$0, 16(%rdi)
	je	.L2
.L4:
	cmpq	(%rdi), %rsi
	jne	.L11
	movl	8(%rdi), %eax
	movw	%ax, (%rdx)
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE17800:
	.size	_ZNK2v88internal12RootIndexMap6LookupEmPNS0_9RootIndexE, .-_ZNK2v88internal12RootIndexMap6LookupEmPNS0_9RootIndexE
	.section	.rodata._ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Out of memory: HashMap::Initialize"
	.section	.text._ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_,"axG",@progbits,_ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_
	.type	_ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_, @function
_ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_:
.LFB21404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	8(%rdi), %eax
	movq	(%rdi), %r13
	movl	12(%rdi), %r15d
	addl	%eax, %eax
	leaq	(%rax,%rax,2), %rdi
	movq	%rax, %rbx
	salq	$3, %rdi
	call	malloc@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L37
	movl	%ebx, 8(%r12)
	testl	%ebx, %ebx
	je	.L14
	movb	$0, 16(%rax)
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L15:
	movq	(%r12), %rcx
	addq	$1, %rax
	movb	$0, 16(%rcx,%rdx)
	movl	8(%r12), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L15
.L14:
	movl	$0, 12(%r12)
	movq	%r13, %r14
	testl	%r15d, %r15d
	je	.L21
.L16:
	cmpb	$0, 16(%r14)
	jne	.L38
.L17:
	addq	$24, %r14
	cmpb	$0, 16(%r14)
	je	.L17
.L38:
	movl	8(%r12), %eax
	movl	12(%r14), %ebx
	movq	(%r12), %rdi
	movq	(%r14), %rsi
	leal	-1(%rax), %ecx
	movl	%ebx, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	jne	.L19
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L39:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L18
.L19:
	cmpq	%rsi, (%rdx)
	jne	.L39
.L18:
	movl	8(%r14), %eax
	movq	%rsi, (%rdx)
	movl	%ebx, 12(%rdx)
	movl	%eax, 8(%rdx)
	movb	$1, 16(%rdx)
	movl	12(%r12), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 12(%r12)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	8(%r12), %eax
	jnb	.L20
.L23:
	addq	$24, %r14
	subl	$1, %r15d
	jne	.L16
.L21:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_
	movl	8(%r12), %eax
	movq	(%r12), %rsi
	leal	-1(%rax), %ecx
	movl	%ebx, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L23
	movq	(%r14), %rdi
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L40:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L23
.L24:
	cmpq	%rdi, (%rdx)
	jne	.L40
	jmp	.L23
.L37:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21404:
	.size	_ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_, .-_ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_
	.section	.text._ZN2v88internal12RootIndexMapC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RootIndexMapC2EPNS0_7IsolateE
	.type	_ZN2v88internal12RootIndexMapC2EPNS0_7IsolateE, @function
_ZN2v88internal12RootIndexMapC2EPNS0_7IsolateE:
.LFB17798:
	.cfi_startproc
	endbr64
	movq	41744(%rsi), %rax
	movq	%rax, (%rdi)
	testq	%rax, %rax
	je	.L77
	ret
.L77:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$24, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	_Znwm@PLT
	movl	$192, %edi
	movq	%rax, %r13
	call	malloc@PLT
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L78
	movl	$8, 8(%r13)
	movl	$24, %edx
	movb	$0, 16(%rax)
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L44:
	movq	0(%r13), %rcx
	addq	$1, %rax
	movb	$0, 16(%rcx,%rdx)
	movl	8(%r13), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L44
	xorl	%r12d, %r12d
	movq	%r13, (%r14)
	movq	56(%r15,%r12,8), %rbx
	movl	$0, 12(%r13)
	movl	%r12d, %eax
	testb	$1, %bl
	je	.L45
	.p2align 4,,10
	.p2align 3
.L81:
	cmpw	$573, %r12w
	ja	.L45
	movl	8(%r13), %eax
	movq	0(%r13), %rsi
	leal	-1(%rax), %edi
	movl	%edi, %eax
	andl	%ebx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L46
	movq	%rdx, %r8
	movq	%rax, %rcx
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L80:
	addq	$1, %rcx
	andq	%rdi, %rcx
	leaq	(%rcx,%rcx,2), %r8
	leaq	(%rsi,%r8,8), %r8
	cmpb	$0, 16(%r8)
	je	.L79
.L48:
	cmpq	(%r8), %rbx
	jne	.L80
.L55:
	addq	$1, %r12
.L83:
	movq	56(%r15,%r12,8), %rbx
	movl	%r12d, %eax
	testb	$1, %bl
	jne	.L81
.L45:
	addl	$1, %eax
	cmpw	$591, %ax
	jbe	.L55
	movq	%r13, 41744(%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movl	%r12d, %ecx
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L82:
	addq	$1, %rax
	andq	%rdi, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L52
.L51:
	cmpq	(%rdx), %rbx
	jne	.L82
	movl	%ecx, 8(%rdx)
.L84:
	addq	$1, %r12
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L46:
	movl	%r12d, %ecx
	.p2align 4,,10
	.p2align 3
.L52:
	movq	%rbx, (%rdx)
	movl	$0, 8(%rdx)
	movl	%ebx, 12(%rdx)
	movb	$1, 16(%rdx)
	movl	12(%r13), %eax
	addl	$1, %eax
	movl	%eax, %esi
	movl	%eax, 12(%r13)
	shrl	$2, %esi
	addl	%esi, %eax
	cmpl	8(%r13), %eax
	jnb	.L58
.L76:
	movl	%ecx, 8(%rdx)
	movq	(%r14), %r13
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%r13, %rdi
	movl	%ecx, -52(%rbp)
	call	_ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_
	movl	8(%r13), %eax
	movq	0(%r13), %rdi
	movl	-52(%rbp), %ecx
	leal	-1(%rax), %esi
	movl	%esi, %eax
	andl	%ebx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	jne	.L53
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L54:
	addq	$1, %rax
	andq	%rsi, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L76
.L53:
	cmpq	(%rdx), %rbx
	jne	.L54
	jmp	.L76
.L78:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17798:
	.size	_ZN2v88internal12RootIndexMapC2EPNS0_7IsolateE, .-_ZN2v88internal12RootIndexMapC2EPNS0_7IsolateE
	.globl	_ZN2v88internal12RootIndexMapC1EPNS0_7IsolateE
	.set	_ZN2v88internal12RootIndexMapC1EPNS0_7IsolateE,_ZN2v88internal12RootIndexMapC2EPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal12RootIndexMapC2EPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal12RootIndexMapC2EPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal12RootIndexMapC2EPNS0_7IsolateE:
.LFB21493:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21493:
	.size	_GLOBAL__sub_I__ZN2v88internal12RootIndexMapC2EPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal12RootIndexMapC2EPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal12RootIndexMapC2EPNS0_7IsolateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
