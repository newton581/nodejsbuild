	.file	"string-stream.cc"
	.text
	.section	.text._ZN2v88internal20FixedStringAllocator4growEPj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20FixedStringAllocator4growEPj
	.type	_ZN2v88internal20FixedStringAllocator4growEPj, @function
_ZN2v88internal20FixedStringAllocator4growEPj:
.LFB17976:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	movl	%eax, (%rsi)
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE17976:
	.size	_ZN2v88internal20FixedStringAllocator4growEPj, .-_ZN2v88internal20FixedStringAllocator4growEPj
	.section	.text._ZN2v88internal20FixedStringAllocatorD2Ev,"axG",@progbits,_ZN2v88internal20FixedStringAllocatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20FixedStringAllocatorD2Ev
	.type	_ZN2v88internal20FixedStringAllocatorD2Ev, @function
_ZN2v88internal20FixedStringAllocatorD2Ev:
.LFB21981:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE21981:
	.size	_ZN2v88internal20FixedStringAllocatorD2Ev, .-_ZN2v88internal20FixedStringAllocatorD2Ev
	.weak	_ZN2v88internal20FixedStringAllocatorD1Ev
	.set	_ZN2v88internal20FixedStringAllocatorD1Ev,_ZN2v88internal20FixedStringAllocatorD2Ev
	.section	.text._ZN2v88internal19HeapStringAllocatorD2Ev,"axG",@progbits,_ZN2v88internal19HeapStringAllocatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19HeapStringAllocatorD2Ev
	.type	_ZN2v88internal19HeapStringAllocatorD2Ev, @function
_ZN2v88internal19HeapStringAllocatorD2Ev:
.LFB5646:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal19HeapStringAllocatorE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L4
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	ret
	.cfi_endproc
.LFE5646:
	.size	_ZN2v88internal19HeapStringAllocatorD2Ev, .-_ZN2v88internal19HeapStringAllocatorD2Ev
	.weak	_ZN2v88internal19HeapStringAllocatorD1Ev
	.set	_ZN2v88internal19HeapStringAllocatorD1Ev,_ZN2v88internal19HeapStringAllocatorD2Ev
	.section	.rodata._ZN2v88internal19HeapStringAllocator4growEPj.str1.1,"aMS",@progbits,1
.LC0:
	.string	"NewArray"
	.section	.text._ZN2v88internal19HeapStringAllocator4growEPj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19HeapStringAllocator4growEPj
	.type	_ZN2v88internal19HeapStringAllocator4growEPj, @function
_ZN2v88internal19HeapStringAllocator4growEPj:
.LFB18008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %eax
	leal	(%rax,%rax), %r14d
	cmpl	%r14d, %eax
	jb	.L7
	movq	8(%rdi), %r12
.L6:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movl	%r14d, %r15d
	movq	%rsi, %rbx
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r15, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L13
.L9:
	movl	(%rbx), %edx
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	memcpy@PLT
	movl	%r14d, (%rbx)
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L11
	call	_ZdaPv@PLT
.L11:
	movq	%r12, 8(%r13)
	jmp	.L6
.L13:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r15, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L9
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE18008:
	.size	_ZN2v88internal19HeapStringAllocator4growEPj, .-_ZN2v88internal19HeapStringAllocator4growEPj
	.section	.text._ZN2v88internal20FixedStringAllocatorD0Ev,"axG",@progbits,_ZN2v88internal20FixedStringAllocatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20FixedStringAllocatorD0Ev
	.type	_ZN2v88internal20FixedStringAllocatorD0Ev, @function
_ZN2v88internal20FixedStringAllocatorD0Ev:
.LFB21983:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE21983:
	.size	_ZN2v88internal20FixedStringAllocatorD0Ev, .-_ZN2v88internal20FixedStringAllocatorD0Ev
	.section	.text._ZN2v88internal19HeapStringAllocatorD0Ev,"axG",@progbits,_ZN2v88internal19HeapStringAllocatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19HeapStringAllocatorD0Ev
	.type	_ZN2v88internal19HeapStringAllocatorD0Ev, @function
_ZN2v88internal19HeapStringAllocatorD0Ev:
.LFB5648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal19HeapStringAllocatorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L16
	call	_ZdaPv@PLT
.L16:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5648:
	.size	_ZN2v88internal19HeapStringAllocatorD0Ev, .-_ZN2v88internal19HeapStringAllocatorD0Ev
	.section	.rodata._ZN2v88internal20FixedStringAllocator8allocateEj.str1.1,"aMS",@progbits,1
.LC1:
	.string	"bytes <= length_"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal20FixedStringAllocator8allocateEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20FixedStringAllocator8allocateEj
	.type	_ZN2v88internal20FixedStringAllocator8allocateEj, @function
_ZN2v88internal20FixedStringAllocator8allocateEj:
.LFB17975:
	.cfi_startproc
	endbr64
	cmpl	%esi, 16(%rdi)
	jb	.L26
	movq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17975:
	.size	_ZN2v88internal20FixedStringAllocator8allocateEj, .-_ZN2v88internal20FixedStringAllocator8allocateEj
	.section	.text._ZN2v88internal19HeapStringAllocator8allocateEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19HeapStringAllocator8allocateEj
	.type	_ZN2v88internal19HeapStringAllocator8allocateEj, @function
_ZN2v88internal19HeapStringAllocator8allocateEj:
.LFB17974:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	leaq	_ZSt7nothrow(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L30
.L28:
	movq	%rax, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L30:
	.cfi_restore_state
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	jne	.L28
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE17974:
	.size	_ZN2v88internal19HeapStringAllocator8allocateEj, .-_ZN2v88internal19HeapStringAllocator8allocateEj
	.section	.text._ZN2v88internal12StringStream3PutEc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12StringStream3PutEc
	.type	_ZN2v88internal12StringStream3PutEc, @function
_ZN2v88internal12StringStream3PutEc:
.LFB17977:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movl	16(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	12(%rdi), %eax
	movl	%eax, %ecx
	subl	%edx, %ecx
	cmpl	$1, %ecx
	je	.L31
	leal	-2(%rax), %ecx
	movq	%rdi, %rbx
	movl	%esi, %r13d
	cmpl	%edx, %ecx
	je	.L40
	movq	24(%rdi), %rax
.L35:
	movb	%r13b, (%rax,%rdx)
	movl	16(%rbx), %eax
	movl	$1, %r12d
	movq	24(%rbx), %rdx
	addl	$1, %eax
	movb	$0, (%rdx,%rax)
	addl	$1, 16(%rbx)
.L31:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L41
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movq	(%rdi), %rdi
	movl	%eax, -44(%rbp)
	leaq	-44(%rbp), %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	movl	12(%rbx), %edx
	movl	-44(%rbp), %ecx
	cmpl	%ecx, %edx
	jnb	.L34
	movl	%ecx, 12(%rbx)
	movl	16(%rbx), %edx
	movq	%rax, 24(%rbx)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L34:
	leal	-1(%rdx), %eax
	movl	%eax, 16(%rbx)
	leal	-5(%rdx), %eax
	movq	24(%rbx), %rdx
	movb	$46, (%rdx,%rax)
	movl	16(%rbx), %eax
	movq	24(%rbx), %rdx
	subl	$3, %eax
	movb	$46, (%rdx,%rax)
	movl	16(%rbx), %eax
	movq	24(%rbx), %rdx
	subl	$2, %eax
	movb	$46, (%rdx,%rax)
	movl	16(%rbx), %eax
	movq	24(%rbx), %rdx
	subl	$1, %eax
	movb	$10, (%rdx,%rax)
	movl	16(%rbx), %eax
	movq	24(%rbx), %rdx
	movb	$0, (%rdx,%rax)
	jmp	.L31
.L41:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17977:
	.size	_ZN2v88internal12StringStream3PutEc, .-_ZN2v88internal12StringStream3PutEc
	.section	.text._ZNK2v88internal12StringStream9ToCStringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12StringStream9ToCStringEv
	.type	_ZNK2v88internal12StringStream9ToCStringEv, @function
_ZNK2v88internal12StringStream9ToCStringEv:
.LFB17984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	16(%rsi), %eax
	leaq	_ZSt7nothrow(%rip), %rsi
	leal	1(%rax), %r13d
	movq	%r13, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L46
	movq	%rax, %rcx
.L43:
	movl	16(%rbx), %r13d
	movq	24(%rbx), %rsi
	movq	%rcx, %rdi
	movq	%r13, %rdx
	call	memcpy@PLT
	movb	$0, (%rax,%r13)
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L46:
	.cfi_restore_state
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r13, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	jne	.L43
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE17984:
	.size	_ZNK2v88internal12StringStream9ToCStringEv, .-_ZNK2v88internal12StringStream9ToCStringEv
	.section	.rodata._ZN2v88internal12StringStream3LogEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"StackDump"
	.section	.text._ZN2v88internal12StringStream3LogEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12StringStream3LogEPNS0_7IsolateE
	.type	_ZN2v88internal12StringStream3LogEPNS0_7IsolateE, @function
_ZN2v88internal12StringStream3LogEPNS0_7IsolateE:
.LFB17985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	41016(%rsi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L50
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movq	24(%rbx), %rdx
	movq	%r12, %rdi
	popq	%rbx
	leaq	.LC3(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6Logger11StringEventEPKcS3_@PLT
	.cfi_endproc
.LFE17985:
	.size	_ZN2v88internal12StringStream3LogEPNS0_7IsolateE, .-_ZN2v88internal12StringStream3LogEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal12StringStream12OutputToFileEP8_IO_FILE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"%s"
	.section	.text._ZN2v88internal12StringStream12OutputToFileEP8_IO_FILE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12StringStream12OutputToFileEP8_IO_FILE
	.type	_ZN2v88internal12StringStream12OutputToFileEP8_IO_FILE, @function
_ZN2v88internal12StringStream12OutputToFileEP8_IO_FILE:
.LFB17986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpl	$2048, 16(%rdi)
	jbe	.L54
	movl	$2048, %ebx
	.p2align 4,,10
	.p2align 3
.L53:
	movq	24(%r12), %rax
	movl	%ebx, %r14d
	leal	-2048(%rbx), %edx
	movq	%r13, %rdi
	leaq	.LC4(%rip), %rsi
	addl	$2048, %ebx
	addq	%r14, %rax
	movzbl	(%rax), %r15d
	movb	$0, (%rax)
	xorl	%eax, %eax
	addq	24(%r12), %rdx
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	24(%r12), %rax
	movb	%r15b, (%rax,%r14)
	cmpl	%ebx, 16(%r12)
	ja	.L53
.L52:
	movq	24(%r12), %rdx
	addq	$8, %rsp
	movq	%r13, %rdi
	leaq	.LC4(%rip), %rsi
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	addq	%r14, %rdx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	xorl	%r14d, %r14d
	jmp	.L52
	.cfi_endproc
.LFE17986:
	.size	_ZN2v88internal12StringStream12OutputToFileEP8_IO_FILE, .-_ZN2v88internal12StringStream12OutputToFileEP8_IO_FILE
	.section	.rodata._ZN2v88internal12StringStream8ToStringEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"(location_) != nullptr"
	.section	.text._ZN2v88internal12StringStream8ToStringEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12StringStream8ToStringEPNS0_7IsolateE
	.type	_ZN2v88internal12StringStream8ToStringEPNS0_7IsolateE, @function
_ZN2v88internal12StringStream8ToStringEPNS0_7IsolateE:
.LFB17987:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	leaq	-32(%rbp), %rsi
	movq	%rax, -32(%rbp)
	movl	16(%rdi), %eax
	movq	%r8, %rdi
	movq	%rax, -24(%rbp)
	call	_ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L61
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L62
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	leaq	.LC5(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L62:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17987:
	.size	_ZN2v88internal12StringStream8ToStringEPNS0_7IsolateE, .-_ZN2v88internal12StringStream8ToStringEPNS0_7IsolateE
	.section	.text._ZN2v88internal12StringStream25ClearMentionedObjectCacheEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12StringStream25ClearMentionedObjectCacheEPNS0_7IsolateE
	.type	_ZN2v88internal12StringStream25ClearMentionedObjectCacheEPNS0_7IsolateE, @function
_ZN2v88internal12StringStream25ClearMentionedObjectCacheEPNS0_7IsolateE:
.LFB17988:
	.cfi_startproc
	endbr64
	movq	41712(%rdi), %rax
	movq	$0, 41720(%rdi)
	testq	%rax, %rax
	je	.L64
	movq	(%rax), %rdx
	cmpq	%rdx, 8(%rax)
	je	.L70
	movq	%rdx, 8(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	%rax, 41712(%rbx)
	movups	%xmm0, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE17988:
	.size	_ZN2v88internal12StringStream25ClearMentionedObjectCacheEPNS0_7IsolateE, .-_ZN2v88internal12StringStream25ClearMentionedObjectCacheEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal12StringStream3PutENS0_6StringEii.str1.1,"aMS",@progbits,1
.LC6:
	.string	"unreachable code"
	.section	.text._ZN2v88internal12StringStream3PutENS0_6StringEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12StringStream3PutENS0_6StringEii
	.type	_ZN2v88internal12StringStream3PutENS0_6StringEii, @function
_ZN2v88internal12StringStream3PutENS0_6StringEii:
.LFB17999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-368(%rbp), %r13
	pushq	%r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	movl	$32, %ecx
	subq	$360, %rsp
	movl	11(%rsi), %r12d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -112(%rbp)
	movb	$0, -88(%rbp)
	movl	%edx, -380(%rbp)
	movaps	%xmm0, -80(%rbp)
	rep stosq
	movl	%edx, %ecx
	leaq	.L75(%rip), %rdx
.L72:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L73
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal12StringStream3PutENS0_6StringEii,"a",@progbits
	.align 4
	.align 4
.L75:
	.long	.L81-.L75
	.long	.L78-.L75
	.long	.L80-.L75
	.long	.L76-.L75
	.long	.L73-.L75
	.long	.L74-.L75
	.long	.L73-.L75
	.long	.L73-.L75
	.long	.L79-.L75
	.long	.L78-.L75
	.long	.L77-.L75
	.long	.L76-.L75
	.long	.L73-.L75
	.long	.L74-.L75
	.section	.text._ZN2v88internal12StringStream3PutENS0_6StringEii
	.p2align 4,,10
	.p2align 3
.L78:
	movl	$0, -104(%rbp)
	movl	-380(%rbp), %edx
	testq	%rsi, %rsi
	je	.L85
	movq	%r13, %rdi
	call	_ZN2v88internal18ConsStringIterator10InitializeENS0_10ConsStringEi@PLT
	movl	-104(%rbp), %esi
	movl	$0, -380(%rbp)
	testl	%esi, %esi
	jne	.L150
.L85:
	leaq	-376(%rbp), %rax
	leaq	.L104(%rip), %r12
	movq	%rax, -400(%rbp)
	cmpl	%ebx, %r14d
	jl	.L97
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L126:
	leaq	1(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movzbl	(%rax), %eax
.L129:
	leal	-32(%rax), %edx
	movl	$63, %ecx
	cmpw	$95, %dx
	movl	16(%r15), %edx
	cmovb	%eax, %ecx
	movl	12(%r15), %eax
	movl	%eax, %esi
	subl	%edx, %esi
	cmpl	$1, %esi
	je	.L136
	leal	-2(%rax), %esi
	cmpl	%esi, %edx
	je	.L151
	movq	24(%r15), %rax
.L133:
	movb	%cl, (%rax,%rdx)
	movl	16(%r15), %eax
	addl	$1, %r14d
	movq	24(%r15), %rdx
	addl	$1, %eax
	movb	$0, (%rdx,%rax)
	addl	$1, 16(%r15)
	cmpl	%r14d, %ebx
	je	.L101
.L97:
	movq	-80(%rbp), %rax
	cmpq	-72(%rbp), %rax
	je	.L99
.L149:
	movzbl	-88(%rbp), %edx
.L100:
	testb	%dl, %dl
	jne	.L126
	leaq	2(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movzwl	(%rax), %eax
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L99:
	movl	$0, -376(%rbp)
	movl	-104(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L152
.L101:
	movl	$1, %eax
.L71:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L153
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	movq	(%r15), %rdi
	movl	%eax, -372(%rbp)
	leaq	-372(%rbp), %rsi
	movb	%cl, -388(%rbp)
	movq	(%rdi), %rax
	call	*24(%rax)
	movl	12(%r15), %edx
	movl	-372(%rbp), %esi
	cmpl	%esi, %edx
	jnb	.L132
	movl	16(%r15), %edx
	movl	%esi, 12(%r15)
	movq	%rax, 24(%r15)
	movzbl	-388(%rbp), %ecx
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L136:
	xorl	%eax, %eax
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L152:
	movq	-400(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18ConsStringIterator8ContinueEPi@PLT
	testq	%rax, %rax
	je	.L101
	movslq	11(%rax), %rcx
	xorl	%esi, %esi
.L102:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L73
	movzwl	%dx, %edx
	movslq	(%r12,%rdx,4), %rdx
	addq	%r12, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal12StringStream3PutENS0_6StringEii
	.align 4
	.align 4
.L104:
	.long	.L110-.L104
	.long	.L107-.L104
	.long	.L109-.L104
	.long	.L105-.L104
	.long	.L73-.L104
	.long	.L103-.L104
	.long	.L73-.L104
	.long	.L73-.L104
	.long	.L108-.L104
	.long	.L107-.L104
	.long	.L106-.L104
	.long	.L105-.L104
	.long	.L73-.L104
	.long	.L103-.L104
	.section	.text._ZN2v88internal12StringStream3PutENS0_6StringEii
	.p2align 4,,10
	.p2align 3
.L103:
	movq	15(%rax), %rax
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L105:
	addl	27(%rax), %esi
	movq	15(%rax), %rax
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L107:
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L111:
	cmpq	%rcx, %rax
	jne	.L149
	movl	$0, -372(%rbp)
	movl	-104(%rbp), %edx
	testl	%edx, %edx
	je	.L149
	leaq	-372(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18ConsStringIterator8ContinueEPi@PLT
	testq	%rax, %rax
	je	.L154
	movslq	11(%rax), %rcx
	xorl	%esi, %esi
.L117:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L73
	leaq	.L119(%rip), %rdi
	movzwl	%dx, %edx
	movslq	(%rdi,%rdx,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal12StringStream3PutENS0_6StringEii
	.align 4
	.align 4
.L119:
	.long	.L125-.L119
	.long	.L122-.L119
	.long	.L124-.L119
	.long	.L120-.L119
	.long	.L73-.L119
	.long	.L118-.L119
	.long	.L73-.L119
	.long	.L73-.L119
	.long	.L123-.L119
	.long	.L122-.L119
	.long	.L121-.L119
	.long	.L120-.L119
	.long	.L73-.L119
	.long	.L118-.L119
	.section	.text._ZN2v88internal12StringStream3PutENS0_6StringEii
	.p2align 4,,10
	.p2align 3
.L109:
	movq	15(%rax), %rdi
	movl	%esi, -392(%rbp)
	movl	%ecx, -388(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-392(%rbp), %rsi
	movslq	-388(%rbp), %rcx
	movb	$0, -88(%rbp)
	leaq	(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L110:
	movslq	%esi, %rsi
	movb	$0, -88(%rbp)
	leaq	15(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L106:
	movq	15(%rax), %rdi
	movl	%esi, -392(%rbp)
	movl	%ecx, -388(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-392(%rbp), %rsi
	movslq	-388(%rbp), %rcx
	movb	$1, -88(%rbp)
	addq	%rsi, %rax
	addq	%rax, %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L108:
	movslq	%esi, %rsi
	movb	$1, -88(%rbp)
	leaq	15(%rax,%rsi), %rax
	addq	%rax, %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L132:
	leal	-1(%rdx), %eax
	movl	%eax, 16(%r15)
	leal	-5(%rdx), %eax
	movq	24(%r15), %rdx
	movb	$46, (%rdx,%rax)
	movl	16(%r15), %eax
	movq	24(%r15), %rdx
	subl	$3, %eax
	movb	$46, (%rdx,%rax)
	movl	16(%r15), %eax
	movq	24(%r15), %rdx
	subl	$2, %eax
	movb	$46, (%rdx,%rax)
	movl	16(%r15), %eax
	movq	24(%r15), %rdx
	subl	$1, %eax
	movb	$10, (%rdx,%rax)
	movl	16(%r15), %eax
	movq	24(%r15), %rdx
	movb	$0, (%rdx,%rax)
	xorl	%eax, %eax
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L76:
	addl	27(%rsi), %ecx
	movq	15(%rsi), %rsi
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L74:
	movq	15(%rsi), %rsi
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L118:
	movq	15(%rax), %rax
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L120:
	addl	27(%rax), %esi
	movq	15(%rax), %rax
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L122:
	movzbl	-88(%rbp), %edx
	movq	-80(%rbp), %rax
	jmp	.L100
.L79:
	movslq	%ecx, %rax
	subl	%r14d, %r12d
	movb	$1, -88(%rbp)
	leaq	15(%rsi,%rax), %rax
	movslq	%r12d, %r12
	movq	%rax, -80(%rbp)
	addq	%r12, %rax
	movq	%rax, -72(%rbp)
.L82:
	movl	$0, -104(%rbp)
	jmp	.L85
.L81:
	movslq	%ecx, %rax
	subl	%r14d, %r12d
	movb	$0, -88(%rbp)
	leaq	15(%rsi,%rax,2), %rax
	movslq	%r12d, %r12
	movq	%rax, -80(%rbp)
	leaq	(%rax,%r12,2), %rax
	movq	%rax, -72(%rbp)
	jmp	.L82
.L77:
	movq	15(%rsi), %rdi
	movl	%ecx, -388(%rbp)
	subl	%r14d, %r12d
	movslq	%r12d, %r12
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-388(%rbp), %rdx
	movb	$1, -88(%rbp)
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	addq	%r12, %rax
	movq	%rax, -72(%rbp)
	jmp	.L82
.L80:
	movq	15(%rsi), %rdi
	movl	%ecx, -388(%rbp)
	subl	%r14d, %r12d
	movslq	%r12d, %r12
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-388(%rbp), %rdx
	movb	$0, -88(%rbp)
	leaq	(%rax,%rdx,2), %rax
	movq	%rax, -80(%rbp)
	leaq	(%rax,%r12,2), %rax
	movq	%rax, -72(%rbp)
	jmp	.L82
.L125:
	movslq	%esi, %rsi
	movb	$0, -88(%rbp)
	leaq	15(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rdx
	movq	%rdx, -72(%rbp)
	leaq	2(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movzwl	(%rax), %eax
	jmp	.L129
.L123:
	movslq	%esi, %rsi
	movb	$1, -88(%rbp)
	leaq	15(%rax,%rsi), %rax
	addq	%rax, %rcx
	movq	%rcx, -72(%rbp)
	jmp	.L126
.L124:
	movq	15(%rax), %rdi
	movl	%ecx, -392(%rbp)
	movl	%esi, -388(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-388(%rbp), %rsi
	movslq	-392(%rbp), %rcx
	movb	$0, -88(%rbp)
	leaq	(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rdx
	movq	%rdx, -72(%rbp)
	leaq	2(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movzwl	(%rax), %eax
	jmp	.L129
.L121:
	movq	15(%rax), %rdi
	movl	%ecx, -392(%rbp)
	movl	%esi, -388(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-388(%rbp), %rsi
	movslq	-392(%rbp), %rcx
	movb	$1, -88(%rbp)
	addq	%rsi, %rax
	addq	%rax, %rcx
	movq	%rcx, -72(%rbp)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L150:
	leaq	-380(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18ConsStringIterator8ContinueEPi@PLT
	testq	%rax, %rax
	je	.L85
	movl	-380(%rbp), %esi
	movl	11(%rax), %r12d
	leaq	.L89(%rip), %rcx
	movl	%esi, %r8d
.L87:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L73
	movzwl	%dx, %edx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal12StringStream3PutENS0_6StringEii
	.align 4
	.align 4
.L89:
	.long	.L95-.L89
	.long	.L85-.L89
	.long	.L94-.L89
	.long	.L90-.L89
	.long	.L73-.L89
	.long	.L88-.L89
	.long	.L73-.L89
	.long	.L73-.L89
	.long	.L93-.L89
	.long	.L85-.L89
	.long	.L91-.L89
	.long	.L90-.L89
	.long	.L73-.L89
	.long	.L88-.L89
	.section	.text._ZN2v88internal12StringStream3PutENS0_6StringEii
	.p2align 4,,10
	.p2align 3
.L154:
	movq	-80(%rbp), %rax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L73:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L88:
	movq	15(%rax), %rax
	jmp	.L87
.L90:
	addl	27(%rax), %r8d
	movq	15(%rax), %rax
	jmp	.L87
.L91:
	movq	15(%rax), %rdi
	subl	%esi, %r12d
	movl	%r8d, -388(%rbp)
	movslq	%r12d, %r12
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-388(%rbp), %rdx
	movb	$1, -88(%rbp)
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	addq	%r12, %rax
	movq	%rax, -72(%rbp)
	jmp	.L85
.L93:
	movslq	%r8d, %rdx
	subl	%esi, %r12d
	movb	$1, -88(%rbp)
	leaq	15(%rax,%rdx), %rax
	movslq	%r12d, %rdx
	movq	%rax, -80(%rbp)
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	jmp	.L85
.L94:
	movq	15(%rax), %rdi
	subl	%esi, %r12d
	movl	%r8d, -388(%rbp)
	movslq	%r12d, %r12
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-388(%rbp), %rdx
	movb	$0, -88(%rbp)
	leaq	(%rax,%rdx,2), %rax
	movq	%rax, -80(%rbp)
	leaq	(%rax,%r12,2), %rax
	movq	%rax, -72(%rbp)
	jmp	.L85
.L95:
	movslq	%r8d, %rdx
	subl	%esi, %r12d
	movb	$0, -88(%rbp)
	leaq	15(%rax,%rdx,2), %rdx
	movslq	%r12d, %rax
	leaq	(%rdx,%rax,2), %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
	jmp	.L85
.L153:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17999:
	.size	_ZN2v88internal12StringStream3PutENS0_6StringEii, .-_ZN2v88internal12StringStream3PutENS0_6StringEii
	.section	.text._ZN2v88internal12StringStream3PutENS0_6StringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12StringStream3PutENS0_6StringE
	.type	_ZN2v88internal12StringStream3PutENS0_6StringE, @function
_ZN2v88internal12StringStream3PutENS0_6StringE:
.LFB17998:
	.cfi_startproc
	endbr64
	movl	11(%rsi), %ecx
	xorl	%edx, %edx
	jmp	_ZN2v88internal12StringStream3PutENS0_6StringEii
	.cfi_endproc
.LFE17998:
	.size	_ZN2v88internal12StringStream3PutENS0_6StringE, .-_ZN2v88internal12StringStream3PutENS0_6StringE
	.section	.rodata._ZNSt6vectorIN2v88internal6HandleINS1_10HeapObjectEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_.str1.1,"aMS",@progbits,1
.LC7:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal6HandleINS1_10HeapObjectEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6HandleINS1_10HeapObjectEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6HandleINS1_10HeapObjectEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_
	.type	_ZNSt6vectorIN2v88internal6HandleINS1_10HeapObjectEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_, @function
_ZNSt6vectorIN2v88internal6HandleINS1_10HeapObjectEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_:
.LFB20944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %r12
	cmpq	16(%rdi), %r12
	je	.L157
	movq	(%rsi), %rax
	movq	%rax, (%r12)
	addq	$8, 8(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	movabsq	$1152921504606846975, %rcx
	movq	(%rdi), %r14
	movq	%r12, %rdx
	subq	%r14, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L183
	testq	%rax, %rax
	je	.L168
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L184
.L160:
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	addq	%rax, %r15
	leaq	8(%rax), %rax
.L161:
	movq	(%rsi), %rcx
	movq	%rcx, 0(%r13,%rdx)
	cmpq	%r14, %r12
	je	.L162
	leaq	-8(%r12), %rsi
	leaq	15(%r13), %rax
	subq	%r14, %rsi
	subq	%r14, %rax
	movq	%rsi, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L171
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L171
	addq	$1, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	shrq	%rax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L164:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, 0(%r13,%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rdx
	jne	.L164
	movq	%rcx, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r14,%rdx), %rax
	addq	%r13, %rdx
	cmpq	%rdi, %rcx
	je	.L166
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L166:
	leaq	16(%r13,%rsi), %rax
.L162:
	testq	%r14, %r14
	je	.L167
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
.L167:
	movq	%r13, %xmm0
	movq	%rax, %xmm2
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L185
	movl	$8, %eax
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L168:
	movl	$8, %r15d
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L171:
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L163:
	movq	(%rax), %rcx
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L163
	jmp	.L166
.L183:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L185:
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	movq	%rcx, %r15
	salq	$3, %r15
	jmp	.L160
	.cfi_endproc
.LFE20944:
	.size	_ZNSt6vectorIN2v88internal6HandleINS1_10HeapObjectEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_, .-_ZNSt6vectorIN2v88internal6HandleINS1_10HeapObjectEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_
	.section	.rodata._ZN2v88internal12StringStream11PrintObjectENS0_6ObjectE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"#%d#"
.LC9:
	.string	"@%p"
	.section	.text._ZN2v88internal12StringStream11PrintObjectENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12StringStream11PrintObjectENS0_6ObjectE
	.type	_ZN2v88internal12StringStream11PrintObjectENS0_6ObjectE, @function
_ZN2v88internal12StringStream11PrintObjectENS0_6ObjectE:
.LFB17981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	-72(%rbp), %rdi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%rsi, -72(%rbp)
	movq	%r12, %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal6Object10ShortPrintEPNS0_12StringStreamE@PLT
	movq	-72(%rbp), %rax
	testb	$1, %al
	jne	.L203
.L186:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L204
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	movq	%rax, %rdx
	notq	%rdx
	cmpw	$63, 11(%rcx)
	jbe	.L205
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	je	.L186
	movq	-1(%rax), %rax
	cmpw	$67, 11(%rax)
	je	.L186
.L191:
	andl	$1, %edx
	jne	.L186
	cmpl	$1, 8(%r12)
	jne	.L186
	movl	_ZN2v88internal7Isolate12isolate_key_E(%rip), %edi
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movq	41712(%rax), %r13
	movq	%rax, %rbx
	movq	0(%r13), %rsi
	movq	8(%r13), %rcx
	subq	%rsi, %rcx
	sarq	$3, %rcx
	je	.L200
	movq	-72(%rbp), %rdi
	xorl	%eax, %eax
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L193:
	addq	$1, %rax
	cmpq	%rcx, %rax
	je	.L206
.L194:
	movq	(%rsi,%rax,8), %rdx
	cmpq	%rdi, (%rdx)
	jne	.L193
	leaq	-48(%rbp), %rcx
	movl	$1, %r8d
	movq	%r12, %rdi
	movl	%eax, -48(%rbp)
	leaq	.LC8(%rip), %rsi
	movl	$4, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L205:
	cmpl	$1024, 11(%rax)
	jg	.L191
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L206:
	cmpq	$255, %rax
	jbe	.L192
	movq	%rdi, -48(%rbp)
	leaq	-48(%rbp), %rcx
	movq	%r12, %rdi
	movl	$1, %r8d
	leaq	.LC9(%rip), %rsi
	movl	$3, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	jmp	.L186
.L200:
	xorl	%eax, %eax
.L192:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movl	$1, %r8d
	movl	%eax, -48(%rbp)
	leaq	.LC8(%rip), %rsi
	movl	$4, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	movq	41112(%rbx), %rdi
	movq	-72(%rbp), %rsi
	testq	%rdi, %rdi
	je	.L196
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L197:
	leaq	-56(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	_ZNSt6vectorIN2v88internal6HandleINS1_10HeapObjectEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_
	jmp	.L186
.L196:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L207
.L198:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L197
.L207:
	movq	%rbx, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	jmp	.L198
.L204:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17981:
	.size	_ZN2v88internal12StringStream11PrintObjectENS0_6ObjectE, .-_ZN2v88internal12StringStream11PrintObjectENS0_6ObjectE
	.section	.rodata._ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"\\x%02x"
.LC11:
	.string	"\\u%04x"
.LC14:
	.string	"inf"
.LC15:
	.string	"nan"
	.section	.text._ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	.type	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE, @function
_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE:
.LFB17979:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -184(%rbp)
	movq	%rdx, -208(%rbp)
	movq	%rcx, -200(%rbp)
	movq	%r8, -192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	12(%rdi), %eax
	subl	16(%rdi), %eax
	cmpl	$1, %eax
	je	.L208
	movl	%edx, %ebx
	testl	%edx, %edx
	jle	.L208
	leaq	-164(%rbp), %rax
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	movq	%rdi, %r14
	movq	%rax, -224(%rbp)
	.p2align 4,,10
	.p2align 3
.L210:
	movslq	%r13d, %rdx
	addq	-184(%rbp), %rdx
	leal	1(%r13), %ecx
	movzbl	(%rdx), %r12d
	cmpb	$37, %r12b
	jne	.L211
	cmpl	%r15d, -192(%rbp)
	je	.L211
	leaq	-144(%rbp), %rax
	movb	$37, -144(%rbp)
	movq	%rax, -160(%rbp)
	movq	$24, -152(%rbp)
	cmpl	%ecx, %ebx
	jle	.L208
	movl	-208(%rbp), %eax
	movl	$1, %ecx
	leal	-2(%rax), %edi
	subl	%r13d, %edi
	addq	$2, %rdi
.L223:
	movzbl	(%rdx,%rcx), %eax
	leal	0(%r13,%rcx), %r8d
	movl	%ecx, %esi
	cmpb	$46, %al
	jg	.L220
	cmpb	$44, %al
	jg	.L221
.L222:
	cmpl	%r8d, %ebx
	jle	.L208
	movq	-160(%rbp), %rcx
	movslq	%esi, %rdx
	addl	$1, %esi
	leal	1(%r15), %r12d
	movq	-200(%rbp), %rdi
	movslq	%esi, %rsi
	leal	1(%r8), %r13d
	movb	%al, (%rcx,%rdx)
	movq	-160(%rbp), %rdx
	subl	$69, %eax
	movb	$0, (%rdx,%rsi)
	movq	(%rdi,%r15,8), %r15
	cmpb	$51, %al
	ja	.L225
	leaq	.L227(%rip), %rdi
	movzbl	%al, %eax
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE,"a",@progbits
	.align 4
	.align 4
.L227:
	.long	.L233-.L227
	.long	.L225-.L227
	.long	.L233-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L226-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L226-.L227
	.long	.L226-.L227
	.long	.L233-.L227
	.long	.L233-.L227
	.long	.L233-.L227
	.long	.L225-.L227
	.long	.L226-.L227
	.long	.L225-.L227
	.long	.L232-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L231-.L227
	.long	.L230-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L251-.L227
	.long	.L225-.L227
	.long	.L226-.L227
	.long	.L225-.L227
	.long	.L228-.L227
	.long	.L226-.L227
	.section	.text._ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	.p2align 4,,10
	.p2align 3
.L226:
	movq	-160(%rbp), %rdx
	leaq	-96(%rbp), %rdi
	movl	%r15d, %ecx
	xorl	%eax, %eax
	movl	$24, %esi
	movq	%rdi, -112(%rbp)
	movq	$24, -104(%rbp)
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	-112(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movslq	%eax, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	.p2align 4,,10
	.p2align 3
.L234:
	movslq	%r12d, %r15
.L217:
	cmpl	%r13d, %ebx
	jg	.L210
.L208:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L253
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_restore_state
	movq	%r15, %xmm1
	andpd	.LC12(%rip), %xmm1
	movq	%r15, %xmm0
	ucomisd	.LC13(%rip), %xmm1
	ja	.L254
	ucomisd	%xmm0, %xmm0
	jp	.L255
	movq	-160(%rbp), %rdx
	leaq	-96(%rbp), %rdi
	movl	$28, %esi
	movl	$1, %eax
	movq	%rdi, -112(%rbp)
	movq	$28, -104(%rbp)
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	-112(%rbp), %r15
.L251:
	movq	%r15, %rdi
	call	strlen@PLT
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movq	%rax, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	jmp	.L234
.L230:
	movq	-160(%rbp), %rdx
	leaq	-96(%rbp), %rdi
	movq	%r15, %rcx
	xorl	%eax, %eax
	movl	$20, %esi
	movq	%rdi, -112(%rbp)
	movq	$20, -104(%rbp)
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	-112(%rbp), %r15
	jmp	.L251
.L228:
	movq	8(%r15), %rax
	movq	(%r15), %rdx
	testl	%eax, %eax
	jle	.L234
	subl	$1, %eax
	movl	%ebx, -212(%rbp)
	movq	%rdx, %rbx
	leaq	2(%rdx,%rax,2), %rax
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L236:
	movsbl	(%rbx), %esi
	movq	%r14, %rdi
	addq	$2, %rbx
	call	_ZN2v88internal12StringStream3PutEc
	cmpq	%rbx, %r15
	jne	.L236
	movl	-212(%rbp), %ebx
	jmp	.L234
.L231:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12StringStream11PrintObjectENS0_6ObjectE
	jmp	.L234
.L232:
	leal	-32(%r15), %eax
	cmpl	$95, %eax
	jbe	.L256
	movl	%r15d, -112(%rbp)
	leaq	-112(%rbp), %rcx
	movl	$1, %r8d
	cmpl	$255, %r15d
	jg	.L238
	leaq	.LC10(%rip), %rsi
	movl	$6, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L220:
	leal	-48(%rax), %r9d
	cmpb	$9, %r9b
	ja	.L222
.L221:
	movq	-160(%rbp), %rsi
	movb	%al, (%rsi,%rcx)
	addq	$1, %rcx
	cmpq	%rdi, %rcx
	jne	.L223
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L211:
	movl	12(%r14), %eax
	movl	16(%r14), %edx
	movl	%eax, %esi
	subl	%edx, %esi
	cmpl	$1, %esi
	je	.L213
	leal	-2(%rax), %esi
	cmpl	%esi, %edx
	je	.L257
	movq	24(%r14), %rax
.L216:
	movb	%r12b, (%rax,%rdx)
	movl	16(%r14), %eax
	movq	24(%r14), %rdx
	addl	$1, %eax
	movb	$0, (%rdx,%rax)
	addl	$1, 16(%r14)
.L213:
	movl	%ecx, %r13d
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L257:
	movq	(%r14), %rdi
	movl	%eax, -164(%rbp)
	movl	%ecx, -212(%rbp)
	movq	-224(%rbp), %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	movl	12(%r14), %edx
	movl	-164(%rbp), %esi
	movl	-212(%rbp), %ecx
	cmpl	%esi, %edx
	jnb	.L215
	movl	%esi, 12(%r14)
	movl	16(%r14), %edx
	movq	%rax, 24(%r14)
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L215:
	leal	-1(%rdx), %eax
	movl	%eax, 16(%r14)
	leal	-5(%rdx), %eax
	movq	24(%r14), %rdx
	movb	$46, (%rdx,%rax)
	movl	16(%r14), %eax
	movq	24(%r14), %rdx
	subl	$3, %eax
	movb	$46, (%rdx,%rax)
	movl	16(%r14), %eax
	movq	24(%r14), %rdx
	subl	$2, %eax
	movb	$46, (%rdx,%rax)
	movl	16(%r14), %eax
	movq	24(%r14), %rdx
	subl	$1, %eax
	movb	$10, (%rdx,%rax)
	movl	16(%r14), %eax
	movq	24(%r14), %rdx
	movb	$0, (%rdx,%rax)
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L238:
	leaq	.LC11(%rip), %rsi
	movl	$6, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L254:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC14(%rip), %rsi
	movq	%r14, %rdi
	movl	$3, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L256:
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal12StringStream3PutEc
	jmp	.L234
.L255:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC15(%rip), %rsi
	movq	%r14, %rdi
	movl	$3, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L225:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L253:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17979:
	.size	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE, .-_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	.section	.rodata._ZN2v88internal12StringStream27PrintSecurityTokenIfChangedENS0_10JSFunctionE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"Security context: %o\n"
	.section	.text._ZN2v88internal12StringStream27PrintSecurityTokenIfChangedENS0_10JSFunctionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12StringStream27PrintSecurityTokenIfChangedENS0_10JSFunctionE
	.type	_ZN2v88internal12StringStream27PrintSecurityTokenIfChangedENS0_10JSFunctionE, @function
_ZN2v88internal12StringStream27PrintSecurityTokenIfChangedENS0_10JSFunctionE:
.LFB18005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	31(%rsi), %rax
	andq	$-262144, %rsi
	movq	39(%rax), %rax
	movq	1151(%rax), %rbx
	movq	24(%rsi), %rax
	cmpq	%rbx, 4128(%rax)
	je	.L258
	leaq	-37592(%rax), %r12
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	movl	$1, %r8d
	leaq	.LC16(%rip), %rsi
	movl	$21, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	movq	%rbx, 41720(%r12)
.L258:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L262
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L262:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18005:
	.size	_ZN2v88internal12StringStream27PrintSecurityTokenIfChangedENS0_10JSFunctionE, .-_ZN2v88internal12StringStream27PrintSecurityTokenIfChangedENS0_10JSFunctionE
	.section	.rodata._ZN2v88internal12StringStream13PrintUsingMapENS0_8JSObjectE.str1.1,"aMS",@progbits,1
.LC17:
	.string	": "
	.section	.rodata._ZN2v88internal12StringStream13PrintUsingMapENS0_8JSObjectE.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"(!IsSmi() && (*layout_word_index < length())) || (IsSmi() && (*layout_word_index < 1))"
	.section	.rodata._ZN2v88internal12StringStream13PrintUsingMapENS0_8JSObjectE.str1.1
.LC19:
	.string	"<unboxed double> %.16g\n"
.LC20:
	.string	"%o\n"
	.section	.text._ZN2v88internal12StringStream13PrintUsingMapENS0_8JSObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12StringStream13PrintUsingMapENS0_8JSObjectE
	.type	_ZN2v88internal12StringStream13PrintUsingMapENS0_8JSObjectE, @function
_ZN2v88internal12StringStream13PrintUsingMapENS0_8JSObjectE:
.LFB18001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-1(%rsi), %rbx
	subq	$104, %rsp
	movq	%rsi, -144(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	%rbx, -96(%rbp)
	movq	-1(%rsi), %r14
	movl	15(%r14), %eax
	leaq	39(%r14), %rbx
	movq	39(%r14), %rdx
	movq	%rbx, -120(%rbp)
	shrl	$10, %eax
	andl	$1023, %eax
	je	.L263
	subl	$1, %eax
	movq	%r14, -104(%rbp)
	movq	%rdi, %r15
	leaq	31(%rdx), %r13
	leaq	(%rax,%rax,2), %rax
	movq	$-7, %rbx
	leaq	55(%rdx,%rax,8), %rax
	subq	%rdx, %rbx
	movq	%rax, -88(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, -128(%rbp)
	leaq	-76(%rbp), %rax
	movq	%rax, -136(%rbp)
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L265:
	addq	$24, %r13
	cmpq	-88(%rbp), %r13
	je	.L263
.L302:
	movq	0(%r13), %rax
	leaq	(%rbx,%r13), %r12
	btq	$33, %rax
	jc	.L265
	movq	-8(%r13), %rax
	movq	%rax, -72(%rbp)
	testb	$1, %al
	jne	.L319
.L273:
	movl	$3, %r14d
	movq	%rbx, -112(%rbp)
	movl	%r14d, %ebx
	movq	-136(%rbp), %r14
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L276:
	movq	24(%r15), %rax
.L278:
	movb	$32, (%rax,%rdx)
	movl	16(%r15), %eax
	movq	24(%r15), %rdx
	addl	$1, %eax
	movb	$0, (%rdx,%rax)
	addl	$1, 16(%r15)
.L275:
	addl	$1, %ebx
	cmpl	$18, %ebx
	je	.L320
.L279:
	movl	12(%r15), %eax
	movl	16(%r15), %edx
	movl	%eax, %esi
	subl	%edx, %esi
	cmpl	$1, %esi
	je	.L275
	leal	-2(%rax), %esi
	cmpl	%esi, %edx
	jne	.L276
	movq	(%r15), %rdi
	movl	%eax, -76(%rbp)
	movq	%r14, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	movl	12(%r15), %edx
	movl	-76(%rbp), %esi
	cmpl	%esi, %edx
	jnb	.L277
	movl	%esi, 12(%r15)
	movl	16(%r15), %edx
	movq	%rax, 24(%r15)
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L320:
	movq	-112(%rbp), %rbx
	movq	-72(%rbp), %rax
.L274:
	testb	$1, %al
	jne	.L321
.L280:
	movq	stdout(%rip), %rsi
	leaq	-72(%rbp), %rdi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
.L281:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC17(%rip), %rsi
	movq	%r15, %rdi
	movl	$2, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	movq	-120(%rbp), %rax
	movq	(%rax), %rax
	movq	7(%r12,%rax), %rdx
	movq	%rdx, %rax
	shrq	$38, %rdx
	shrq	$51, %rax
	andl	$7, %edx
	movq	%rax, %rsi
	movq	-104(%rbp), %rax
	andl	$1023, %esi
	movzbl	7(%rax), %edi
	movzbl	8(%rax), %ecx
	subl	%ecx, %edi
	cmpl	%edi, %esi
	setl	%al
	jl	.L322
	subl	%edi, %esi
	movl	$16, %ecx
	leal	16(,%rsi,8), %esi
.L283:
	cmpl	$2, %edx
	jne	.L323
	movl	$32768, %edx
.L284:
	movzbl	%al, %eax
	movslq	%edi, %rdi
	movslq	%esi, %rsi
	movslq	%ecx, %rcx
	salq	$14, %rax
	salq	$17, %rdi
	orq	%rdi, %rax
	salq	$27, %rcx
	orq	%rsi, %rax
	orq	%rcx, %rax
	movq	-96(%rbp), %rcx
	orq	%rax, %rdx
	shrq	$30, %rax
	movq	(%rcx), %rsi
	movl	%edx, %ecx
	andl	$15, %eax
	sarl	$3, %ecx
	andl	$2047, %ecx
	subl	%eax, %ecx
	testb	$64, %dh
	jne	.L287
	movq	-144(%rbp), %rax
	movq	7(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	subq	$37592, %rax
	testb	$1, %dl
	jne	.L288
.L300:
	movq	968(%rax), %rdx
.L289:
	leal	16(,%rcx,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
.L303:
	movq	-128(%rbp), %rcx
	movl	$3, %edx
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movl	$1, %r8d
	leaq	.LC20(%rip), %rsi
	addq	$24, %r13
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	cmpq	-88(%rbp), %r13
	jne	.L302
	.p2align 4,,10
	.p2align 3
.L263:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L324
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L323:
	.cfi_restore_state
	cmpb	$2, %dl
	jg	.L285
	je	.L286
.L306:
	xorl	%edx, %edx
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L285:
	subl	$3, %edx
	cmpb	$1, %dl
	jbe	.L306
.L286:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L277:
	leal	-1(%rdx), %eax
	movl	%eax, 16(%r15)
	leal	-5(%rdx), %eax
	movq	24(%r15), %rdx
	movb	$46, (%rdx,%rax)
	movl	16(%r15), %eax
	movq	24(%r15), %rdx
	subl	$3, %eax
	movb	$46, (%rdx,%rax)
	movl	16(%r15), %eax
	movq	24(%r15), %rdx
	subl	$2, %eax
	movb	$46, (%rdx,%rax)
	movl	16(%r15), %eax
	movq	24(%r15), %rdx
	subl	$1, %eax
	movb	$10, (%rdx,%rax)
	movl	16(%r15), %eax
	movq	24(%r15), %rdx
	movb	$0, (%rdx,%rax)
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L287:
	movq	47(%rsi), %rax
	testq	%rax, %rax
	je	.L299
	movq	%rax, %rdi
	movl	$32, %esi
	notq	%rdi
	movl	%edi, %r8d
	andl	$1, %r8d
	je	.L325
	cmpl	%ecx, %esi
	jbe	.L299
.L327:
	testl	%ecx, %ecx
	leal	31(%rcx), %esi
	cmovns	%ecx, %esi
	sarl	$5, %esi
	andl	$1, %edi
	jne	.L292
	cmpl	%esi, 11(%rax)
	jle	.L292
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%edi, %ecx
	andl	$31, %ecx
	subl	%edi, %ecx
	movl	$1, %edi
	sall	%cl, %edi
	movl	%edi, %ecx
	testb	%r8b, %r8b
	je	.L326
.L296:
	sarq	$32, %rax
	testl	%eax, %ecx
	sete	%cl
.L298:
	movq	-96(%rbp), %rdi
	movq	%rdx, %rax
	andl	$16376, %eax
	movq	(%rax,%rdi), %rax
	testb	%cl, %cl
	jne	.L299
	movq	-128(%rbp), %rcx
	movl	$23, %edx
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movl	$1, %r8d
	leaq	.LC19(%rip), %rsi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L299:
	movq	-96(%rbp), %rax
	andl	$16376, %edx
	movq	(%rdx,%rax), %rax
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L319:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L267
.L270:
	movq	-72(%rbp), %rax
	testb	$1, %al
	je	.L273
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L273
	movl	11(%rax), %r14d
	cmpl	$17, %r14d
	jg	.L274
	movq	%rbx, -112(%rbp)
	movl	%r14d, %ebx
	movq	-136(%rbp), %r14
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L267:
	movq	-1(%rax), %rax
	cmpw	$65, 11(%rax)
	jne	.L265
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L322:
	movq	-104(%rbp), %r9
	movzbl	8(%r9), %ecx
	movzbl	8(%r9), %r8d
	addl	%r8d, %esi
	sall	$3, %ecx
	sall	$3, %esi
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L321:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L280
	movq	-72(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	11(%rsi), %ecx
	call	_ZN2v88internal12StringStream3PutENS0_6StringEii
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L325:
	movslq	11(%rax), %rsi
	sall	$3, %esi
	cmpl	%ecx, %esi
	ja	.L327
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L288:
	cmpq	288(%rax), %rdx
	jne	.L289
	jmp	.L300
.L326:
	sall	$2, %esi
	movslq	%esi, %rsi
	movl	15(%rax,%rsi), %eax
	testl	%eax, %edi
	sete	%cl
	jmp	.L298
.L292:
	cmpl	$31, %ecx
	jg	.L294
	testb	%r8b, %r8b
	je	.L294
	movl	$1, %esi
	sall	%cl, %esi
	movl	%esi, %ecx
	jmp	.L296
.L294:
	leaq	.LC18(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L324:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18001:
	.size	_ZN2v88internal12StringStream13PrintUsingMapENS0_8JSObjectE, .-_ZN2v88internal12StringStream13PrintUsingMapENS0_8JSObjectE
	.section	.rodata._ZN2v88internal12StringStream15PrintFixedArrayENS0_10FixedArrayEj.str1.1,"aMS",@progbits,1
.LC21:
	.string	"%d: %o\n"
.LC22:
	.string	"                  ...\n"
	.section	.text._ZN2v88internal12StringStream15PrintFixedArrayENS0_10FixedArrayEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12StringStream15PrintFixedArrayENS0_10FixedArrayEj
	.type	_ZN2v88internal12StringStream15PrintFixedArrayENS0_10FixedArrayEj, @function
_ZN2v88internal12StringStream15PrintFixedArrayENS0_10FixedArrayEj:
.LFB18002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -100(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	andq	$-262144, %rax
	movq	24(%rax), %r15
	leaq	-37592(%r15), %rax
	movq	%rax, -120(%rbp)
	leaq	-1(%rsi), %rax
	movq	%rax, -112(%rbp)
	testl	%edx, %edx
	je	.L328
	movq	%rdi, %r14
	xorl	%r12d, %r12d
	leaq	-84(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L337:
	leal	16(,%r12,8), %ebx
	movslq	%ebx, %rbx
	addq	-112(%rbp), %rbx
	movq	(%rbx), %rax
	movq	-120(%rbp), %rcx
	cmpq	%rax, 96(%rcx)
	je	.L331
	movl	$17, %r15d
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L333:
	movq	24(%r14), %rax
.L335:
	movb	$32, (%rax,%rdx)
	movl	16(%r14), %eax
	movq	24(%r14), %rdx
	addl	$1, %eax
	movb	$0, (%rdx,%rax)
	addl	$1, 16(%r14)
.L332:
	subl	$1, %r15d
	je	.L350
.L336:
	movl	12(%r14), %eax
	movl	16(%r14), %edx
	movl	%eax, %esi
	subl	%edx, %esi
	cmpl	$1, %esi
	je	.L332
	leal	-2(%rax), %esi
	cmpl	%esi, %edx
	jne	.L333
	movq	(%r14), %rdi
	movl	%eax, -84(%rbp)
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	movl	12(%r14), %edx
	movl	-84(%rbp), %esi
	cmpl	%esi, %edx
	jnb	.L334
	movl	%esi, 12(%r14)
	movl	16(%r14), %edx
	movq	%rax, 24(%r14)
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L350:
	movq	(%rbx), %rax
	leaq	-80(%rbp), %rcx
	movl	$2, %r8d
	movq	%r14, %rdi
	leaq	.LC21(%rip), %rsi
	movl	$7, %edx
	movl	%r12d, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
.L331:
	addl	$1, %r12d
	cmpl	$9, %r12d
	ja	.L341
	cmpl	%r12d, -100(%rbp)
	ja	.L337
.L341:
	cmpl	$9, -100(%rbp)
	jbe	.L328
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC22(%rip), %rsi
	movq	%r14, %rdi
	movl	$22, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
.L328:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L351
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	leal	-1(%rdx), %eax
	movl	%eax, 16(%r14)
	leal	-5(%rdx), %eax
	movq	24(%r14), %rdx
	movb	$46, (%rdx,%rax)
	movl	16(%r14), %eax
	movq	24(%r14), %rdx
	subl	$3, %eax
	movb	$46, (%rdx,%rax)
	movl	16(%r14), %eax
	movq	24(%r14), %rdx
	subl	$2, %eax
	movb	$46, (%rdx,%rax)
	movl	16(%r14), %eax
	movq	24(%r14), %rdx
	subl	$1, %eax
	movb	$10, (%rdx,%rax)
	movl	16(%r14), %eax
	movq	24(%r14), %rdx
	movb	$0, (%rdx,%rax)
	jmp	.L332
.L351:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18002:
	.size	_ZN2v88internal12StringStream15PrintFixedArrayENS0_10FixedArrayEj, .-_ZN2v88internal12StringStream15PrintFixedArrayENS0_10FixedArrayEj
	.section	.rodata._ZN2v88internal12StringStream14PrintByteArrayENS0_9ByteArrayE.str1.1,"aMS",@progbits,1
.LC23:
	.string	"             %d: %3d 0x%02x"
.LC24:
	.string	" '%c'"
.LC25:
	.string	" '\n'"
.LC26:
	.string	" '\r'"
.LC27:
	.string	" ^%c"
.LC28:
	.string	"\n"
	.section	.text._ZN2v88internal12StringStream14PrintByteArrayENS0_9ByteArrayE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12StringStream14PrintByteArrayENS0_9ByteArrayE
	.type	_ZN2v88internal12StringStream14PrintByteArrayENS0_9ByteArrayE, @function
_ZN2v88internal12StringStream14PrintByteArrayENS0_9ByteArrayE:
.LFB18003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	leaq	-1(%rsi), %rdi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	11(%rsi), %rax
	movq	%rdi, -88(%rbp)
	movq	%rax, -104(%rbp)
	movl	%eax, -92(%rbp)
	testl	%eax, %eax
	je	.L352
	xorl	%ebx, %ebx
	leaq	-80(%rbp), %r13
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L355:
	cmpb	$10, %r12b
	je	.L368
	cmpb	$13, %r12b
	je	.L369
	leal	-1(%r12), %eax
	cmpb	$25, %al
	jbe	.L370
.L356:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC28(%rip), %rsi
	movq	%r15, %rdi
	movl	$1, %edx
	addl	$1, %ebx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	cmpl	$9, %ebx
	ja	.L363
	cmpl	%ebx, -92(%rbp)
	jbe	.L363
.L359:
	movq	-88(%rbp), %rcx
	leal	16(%rbx), %eax
	movl	$27, %edx
	movq	%r15, %rdi
	cltq
	movl	$3, %r8d
	leaq	.LC23(%rip), %rsi
	movl	%ebx, -80(%rbp)
	movzbl	(%rax,%rcx), %r12d
	movq	%r13, %rcx
	movl	%r12d, -72(%rbp)
	movl	%r12d, -64(%rbp)
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	leal	-32(%r12), %edx
	cmpb	$94, %dl
	ja	.L355
	movq	%r13, %rcx
	movl	$1, %r8d
	movq	%r15, %rdi
	movl	%r12d, -80(%rbp)
	leaq	.LC24(%rip), %rsi
	movl	$5, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L370:
	leal	64(%r12), %r9d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	%r15, %rdi
	movl	$1, %r8d
	leaq	.LC27(%rip), %rsi
	movl	%r9d, -80(%rbp)
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L368:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC25(%rip), %rsi
	movq	%r15, %rdi
	movl	$4, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L363:
	cmpl	$9, -104(%rbp)
	jbe	.L352
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC22(%rip), %rsi
	movq	%r15, %rdi
	movl	$22, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
.L352:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L371
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L369:
	.cfi_restore_state
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC26(%rip), %rsi
	movq	%r15, %rdi
	movl	$4, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	jmp	.L356
.L371:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18003:
	.size	_ZN2v88internal12StringStream14PrintByteArrayENS0_9ByteArrayE, .-_ZN2v88internal12StringStream14PrintByteArrayENS0_9ByteArrayE
	.section	.rodata._ZN2v88internal12StringStream25PrintMentionedObjectCacheEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC29:
	.string	"==== Key         ============================================\n\n"
	.section	.rodata._ZN2v88internal12StringStream25PrintMentionedObjectCacheEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC30:
	.string	" #%d# %p: "
.LC31:
	.string	"           value(): %o\n"
	.section	.text._ZN2v88internal12StringStream25PrintMentionedObjectCacheEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12StringStream25PrintMentionedObjectCacheEPNS0_7IsolateE
	.type	_ZN2v88internal12StringStream25PrintMentionedObjectCacheEPNS0_7IsolateE, @function
_ZN2v88internal12StringStream25PrintMentionedObjectCacheEPNS0_7IsolateE:
.LFB18004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jne	.L387
.L372:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L388
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L387:
	.cfi_restore_state
	movq	41712(%rsi), %r12
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%rdi, %rbx
	movl	$63, %edx
	leaq	.LC29(%rip), %rsi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	movq	(%r12), %rdx
	cmpq	8(%r12), %rdx
	je	.L372
	xorl	%r14d, %r14d
	leaq	-80(%rbp), %r13
	leaq	-88(%rbp), %r15
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L391:
	cmpw	$1041, 11(%rax)
	je	.L389
.L375:
	movq	-88(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal12StringStream13PrintUsingMapENS0_8JSObjectE
	movq	-88(%rbp), %rax
	movq	-1(%rax), %rdx
	cmpw	$1061, 11(%rdx)
	je	.L390
.L377:
	movq	(%r12), %rdx
	movq	8(%r12), %rax
	addq	$1, %r14
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %r14
	jnb	.L372
.L383:
	movq	(%rdx,%r14,8), %rax
	movq	%r13, %rcx
	movl	$2, %r8d
	movq	%rbx, %rdi
	movl	$10, %edx
	leaq	.LC30(%rip), %rsi
	movq	(%rax), %rax
	movl	%r14d, -80(%rbp)
	movq	%rax, -88(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal6Object10ShortPrintEPNS0_12StringStreamE@PLT
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movl	$1, %edx
	leaq	.LC28(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	movq	-88(%rbp), %rsi
	movq	-1(%rsi), %rax
	cmpw	$1024, 11(%rax)
	movq	-1(%rsi), %rax
	ja	.L391
	cmpw	$70, 11(%rax)
	je	.L392
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	ja	.L377
	movslq	11(%rsi), %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal12StringStream15PrintFixedArrayENS0_10FixedArrayEj
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L392:
	movq	%rbx, %rdi
	call	_ZN2v88internal12StringStream14PrintByteArrayENS0_9ByteArrayE
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L390:
	movq	-1(%rax), %rdx
	movzbl	14(%rdx), %edx
	shrl	$3, %edx
	subl	$2, %edx
	cmpb	$1, %dl
	ja	.L377
	movq	15(%rax), %rsi
	movq	23(%rax), %rax
	movslq	11(%rsi), %rcx
	testb	$1, %al
	je	.L393
	movsd	7(%rax), %xmm0
.L380:
	cvttsd2siq	%xmm0, %rdx
	movq	%rbx, %rdi
	cmpl	%ecx, %edx
	cmova	%ecx, %edx
	call	_ZN2v88internal12StringStream15PrintFixedArrayENS0_10FixedArrayEj
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L389:
	movq	23(%rsi), %rax
	movq	%r13, %rcx
	movl	$1, %r8d
	movq	%rbx, %rdi
	leaq	.LC31(%rip), %rsi
	movl	$23, %edx
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L393:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	jmp	.L380
.L388:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18004:
	.size	_ZN2v88internal12StringStream25PrintMentionedObjectCacheEPNS0_7IsolateE, .-_ZN2v88internal12StringStream25PrintMentionedObjectCacheEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal12StringStream9PrintNameENS0_6ObjectE.str1.1,"aMS",@progbits,1
.LC32:
	.string	"/* anonymous */"
.LC33:
	.string	"%o"
	.section	.text._ZN2v88internal12StringStream9PrintNameENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12StringStream9PrintNameENS0_6ObjectE
	.type	_ZN2v88internal12StringStream9PrintNameENS0_6ObjectE, @function
_ZN2v88internal12StringStream9PrintNameENS0_6ObjectE:
.LFB18000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	testb	$1, %sil
	jne	.L400
.L395:
	movq	%rsi, -16(%rbp)
	leaq	-16(%rbp), %rcx
	movl	$1, %r8d
	leaq	.LC33(%rip), %rsi
	movl	$2, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
.L394:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L401
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L395
	movl	11(%rsi), %ecx
	testl	%ecx, %ecx
	jle	.L396
	xorl	%edx, %edx
	call	_ZN2v88internal12StringStream3PutENS0_6StringEii
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L396:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movl	$15, %edx
	leaq	.LC32(%rip), %rsi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	jmp	.L394
.L401:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18000:
	.size	_ZN2v88internal12StringStream9PrintNameENS0_6ObjectE, .-_ZN2v88internal12StringStream9PrintNameENS0_6ObjectE
	.section	.rodata._ZN2v88internal12StringStream14PrintPrototypeENS0_10JSFunctionENS0_6ObjectE.str1.1,"aMS",@progbits,1
.LC34:
	.string	"(aka "
	.section	.text._ZN2v88internal12StringStream14PrintPrototypeENS0_10JSFunctionENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12StringStream14PrintPrototypeENS0_10JSFunctionENS0_6ObjectE
	.type	_ZN2v88internal12StringStream14PrintPrototypeENS0_10JSFunctionENS0_6ObjectE, @function
_ZN2v88internal12StringStream14PrintPrototypeENS0_10JSFunctionENS0_6ObjectE:
.LFB18007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	23(%rsi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%rdx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	23(%rsi), %r13
	movq	15(%r13), %rax
	testb	$1, %al
	jne	.L454
.L403:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	setne	%al
.L404:
	testb	%al, %al
	je	.L408
	movq	15(%r13), %r15
	testb	$1, %r15b
	jne	.L455
.L406:
	movq	%rbx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdx
	movq	-72(%rbp), %rax
	leaq	-37592(%rdx), %rcx
	cmpq	%rax, -37488(%rdx)
	jne	.L409
.L453:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream9PrintNameENS0_6ObjectE
.L410:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC34(%rip), %rsi
	movq	%r12, %rdi
	movl	$5, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE
	movq	(%r14), %rbx
	movq	15(%rbx), %rax
	testb	$1, %al
	jne	.L456
.L432:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	setne	%al
.L433:
	testb	%al, %al
	je	.L437
	movq	15(%rbx), %rsi
	testb	$1, %sil
	jne	.L457
.L435:
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream9PrintNameENS0_6ObjectE
	movl	$41, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream3PutEc
.L402:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L458
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L455:
	.cfi_restore_state
	movq	-1(%r15), %rax
	cmpw	$136, 11(%rax)
	jne	.L406
	leaq	-64(%rbp), %rdi
	movq	%r15, -64(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv@PLT
	testb	%al, %al
	je	.L408
	movq	-80(%rbp), %rdi
	movq	%r15, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo12FunctionNameEv@PLT
	movq	%rax, %r15
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L408:
	andq	$-262144, %r13
	movq	24(%r13), %rax
	movq	-37464(%rax), %r15
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L409:
	cmpq	88(%rcx), %rax
	je	.L453
	cmpq	96(%rcx), %rax
	je	.L453
	testb	$1, %al
	jne	.L459
	cmpq	$0, 12464(%rcx)
	jne	.L417
.L419:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream9PrintNameENS0_6ObjectE
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L454:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L403
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L456:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L432
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L457:
	movq	-1(%rsi), %rax
	cmpw	$136, 11(%rax)
	jne	.L435
	leaq	-64(%rbp), %r13
	movq	%rsi, -64(%rbp)
	movq	%r13, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv@PLT
	testb	%al, %al
	je	.L437
	movq	-80(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo12FunctionNameEv@PLT
	movq	%rax, %rsi
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L437:
	andq	$-262144, %rbx
	movq	24(%rbx), %rax
	movq	-37464(%rax), %rsi
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L459:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	je	.L453
	cmpq	$0, 12464(%rcx)
	je	.L419
	movq	-1(%rax), %rdx
	movq	%rax, %r13
	cmpw	$1024, 11(%rdx)
	ja	.L418
	.p2align 4,,10
	.p2align 3
.L417:
	movq	%rcx, %rsi
	leaq	-72(%rbp), %rdi
	movq	%rcx, -80(%rbp)
	call	_ZNK2v88internal6Object24GetPrototypeChainRootMapEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rcx
	movq	23(%rax), %r13
	movq	%r13, -72(%rbp)
.L418:
	leaq	-64(%rbp), %rdi
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L431:
	movq	(%rdx), %rax
	movq	23(%rax), %r13
	cmpq	104(%rcx), %r13
	je	.L419
.L430:
	leaq	-1(%r13), %rdx
	movq	%rcx, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	-1(%r13), %rax
	cmpw	$1024, 11(%rax)
	je	.L419
	movq	%rbx, %rsi
	movq	%rdi, -96(%rbp)
	movq	%r13, -64(%rbp)
	call	_ZN2v88internal8JSObject17SlowReverseLookupENS0_6ObjectE@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rdi
	movq	-80(%rbp), %rdx
	movq	%rax, %rsi
	cmpq	88(%rcx), %rax
	jne	.L460
	andl	$1, %r13d
	je	.L431
	movq	(%rdx), %rax
	cmpw	$1024, 11(%rax)
	jne	.L431
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L460:
	testb	$1, %r15b
	jne	.L461
.L450:
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream9PrintNameENS0_6ObjectE
	jmp	.L410
.L461:
	movq	-1(%r15), %rax
	leaq	-1(%r15), %rbx
	cmpw	$63, 11(%rax)
	ja	.L426
	testb	$1, %sil
	jne	.L462
.L426:
	movq	(%rbx), %rax
	cmpw	$63, 11(%rax)
	ja	.L450
	movl	11(%r15), %eax
	testl	%eax, %eax
	jne	.L450
.L451:
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream9PrintNameENS0_6ObjectE
	jmp	.L402
.L462:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L426
	movq	%r15, -64(%rbp)
	cmpq	%r15, %rsi
	je	.L424
	movq	(%rbx), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L425
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	je	.L426
.L425:
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal6String10SlowEqualsES1_@PLT
	movq	-80(%rbp), %rsi
	testb	%al, %al
	je	.L426
.L424:
	movq	(%rbx), %rax
	jmp	.L451
.L458:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18007:
	.size	_ZN2v88internal12StringStream14PrintPrototypeENS0_10JSFunctionENS0_6ObjectE, .-_ZN2v88internal12StringStream14PrintPrototypeENS0_10JSFunctionENS0_6ObjectE
	.section	.text._ZN2v88internal12StringStream13PrintFunctionENS0_10JSFunctionENS0_6ObjectEPNS0_4CodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12StringStream13PrintFunctionENS0_10JSFunctionENS0_6ObjectEPNS0_4CodeE
	.type	_ZN2v88internal12StringStream13PrintFunctionENS0_10JSFunctionENS0_6ObjectEPNS0_4CodeE, @function
_ZN2v88internal12StringStream13PrintFunctionENS0_10JSFunctionENS0_6ObjectEPNS0_4CodeE:
.LFB18006:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZN2v88internal12StringStream14PrintPrototypeENS0_10JSFunctionENS0_6ObjectE
	movq	47(%rbx), %rax
	popq	%rbx
	movq	%rax, (%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18006:
	.size	_ZN2v88internal12StringStream13PrintFunctionENS0_10JSFunctionENS0_6ObjectEPNS0_4CodeE, .-_ZN2v88internal12StringStream13PrintFunctionENS0_10JSFunctionENS0_6ObjectEPNS0_4CodeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal19HeapStringAllocator8allocateEj,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal19HeapStringAllocator8allocateEj, @function
_GLOBAL__sub_I__ZN2v88internal19HeapStringAllocator8allocateEj:
.LFB22009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22009:
	.size	_GLOBAL__sub_I__ZN2v88internal19HeapStringAllocator8allocateEj, .-_GLOBAL__sub_I__ZN2v88internal19HeapStringAllocator8allocateEj
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal19HeapStringAllocator8allocateEj
	.weak	_ZTVN2v88internal19HeapStringAllocatorE
	.section	.data.rel.ro.local._ZTVN2v88internal19HeapStringAllocatorE,"awG",@progbits,_ZTVN2v88internal19HeapStringAllocatorE,comdat
	.align 8
	.type	_ZTVN2v88internal19HeapStringAllocatorE, @object
	.size	_ZTVN2v88internal19HeapStringAllocatorE, 48
_ZTVN2v88internal19HeapStringAllocatorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal19HeapStringAllocatorD1Ev
	.quad	_ZN2v88internal19HeapStringAllocatorD0Ev
	.quad	_ZN2v88internal19HeapStringAllocator8allocateEj
	.quad	_ZN2v88internal19HeapStringAllocator4growEPj
	.weak	_ZTVN2v88internal20FixedStringAllocatorE
	.section	.data.rel.ro.local._ZTVN2v88internal20FixedStringAllocatorE,"awG",@progbits,_ZTVN2v88internal20FixedStringAllocatorE,comdat
	.align 8
	.type	_ZTVN2v88internal20FixedStringAllocatorE, @object
	.size	_ZTVN2v88internal20FixedStringAllocatorE, 48
_ZTVN2v88internal20FixedStringAllocatorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal20FixedStringAllocatorD1Ev
	.quad	_ZN2v88internal20FixedStringAllocatorD0Ev
	.quad	_ZN2v88internal20FixedStringAllocator8allocateEj
	.quad	_ZN2v88internal20FixedStringAllocator4growEPj
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC12:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC13:
	.long	4294967295
	.long	2146435071
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
