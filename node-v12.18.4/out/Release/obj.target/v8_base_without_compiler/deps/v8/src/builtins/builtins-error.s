	.file	"builtins-error.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB3941:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE3941:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB3942:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3942:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB3944:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3944:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internalL26Builtin_Impl_MakeTypeErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL26Builtin_Impl_MakeTypeErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL26Builtin_Impl_MakeTypeErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18401:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	12464(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %r14
	movq	39(%rax), %rax
	movq	1767(%rax), %r8
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L6
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L7:
	cmpl	$5, %r15d
	jg	.L9
	leaq	88(%r12), %rax
	movq	%rax, %r9
.L10:
	movq	%r9, %rcx
.L12:
	movq	%r9, %r8
.L16:
	subq	$8, %rsp
	movq	(%rax), %rdx
	movq	%r12, %rdi
	pushq	$2
	sarq	$32, %rdx
	call	_ZN2v88internal10ErrorUtils16MakeGenericErrorEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS4_INS0_6ObjectEEES9_S9_NS0_13FrameSkipModeE@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L23
	movq	(%rax), %r15
.L18:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L21
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L21:
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	leaq	-8(%rbx), %rax
	cmpl	$6, %r15d
	je	.L24
	leaq	-16(%rbx), %rcx
	cmpl	$7, %r15d
	je	.L25
	leaq	-24(%rbx), %r8
	cmpl	$8, %r15d
	je	.L26
	leaq	-32(%rbx), %r9
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%r13, %rsi
	cmpq	41096(%rdx), %r13
	je	.L27
.L8:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L23:
	movq	312(%r12), %r15
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%rdx, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L8
.L24:
	leaq	88(%r12), %r9
	jmp	.L10
.L26:
	leaq	88(%r12), %r9
	jmp	.L16
.L25:
	leaq	88(%r12), %r9
	jmp	.L12
	.cfi_endproc
.LFE18401:
	.size	_ZN2v88internalL26Builtin_Impl_MakeTypeErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL26Builtin_Impl_MakeTypeErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internalL25Builtin_Impl_MakeURIErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL25Builtin_Impl_MakeURIErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL25Builtin_Impl_MakeURIErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18404:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	12464(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	movq	39(%rax), %rax
	movq	1775(%rax), %r14
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L29
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L30:
	subq	$8, %rsp
	leaq	88(%r12), %rcx
	movl	$339, %edx
	movq	%r12, %rdi
	pushq	$2
	movq	%rcx, %r9
	movq	%rcx, %r8
	call	_ZN2v88internal10ErrorUtils16MakeGenericErrorEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS4_INS0_6ObjectEEES9_S9_NS0_13FrameSkipModeE@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L38
	movq	(%rax), %r14
.L33:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L36
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L36:
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%rdx), %rbx
	je	.L39
.L31:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L38:
	movq	312(%r12), %r14
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%rdx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L31
	.cfi_endproc
.LFE18404:
	.size	_ZN2v88internalL25Builtin_Impl_MakeURIErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL25Builtin_Impl_MakeURIErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internalL22Builtin_Impl_MakeErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL22Builtin_Impl_MakeErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL22Builtin_Impl_MakeErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18392:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	12464(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %r14
	movq	39(%rax), %rax
	movq	1607(%rax), %r8
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L41
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L42:
	cmpl	$5, %r15d
	jg	.L44
	leaq	88(%r12), %rax
	movq	%rax, %r9
.L45:
	movq	%r9, %rcx
.L47:
	movq	%r9, %r8
.L51:
	subq	$8, %rsp
	movq	(%rax), %rdx
	movq	%r12, %rdi
	pushq	$2
	sarq	$32, %rdx
	call	_ZN2v88internal10ErrorUtils16MakeGenericErrorEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS4_INS0_6ObjectEEES9_S9_NS0_13FrameSkipModeE@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L58
	movq	(%rax), %r15
.L53:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L56
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L56:
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	leaq	-8(%rbx), %rax
	cmpl	$6, %r15d
	je	.L59
	leaq	-16(%rbx), %rcx
	cmpl	$7, %r15d
	je	.L60
	leaq	-24(%rbx), %r8
	cmpl	$8, %r15d
	je	.L61
	leaq	-32(%rbx), %r9
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%r13, %rsi
	cmpq	41096(%rdx), %r13
	je	.L62
.L43:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L58:
	movq	312(%r12), %r15
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L62:
	movq	%rdx, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L43
.L59:
	leaq	88(%r12), %r9
	jmp	.L45
.L61:
	leaq	88(%r12), %r9
	jmp	.L51
.L60:
	leaq	88(%r12), %r9
	jmp	.L47
	.cfi_endproc
.LFE18392:
	.size	_ZN2v88internalL22Builtin_Impl_MakeErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL22Builtin_Impl_MakeErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internalL27Builtin_Impl_MakeRangeErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL27Builtin_Impl_MakeRangeErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL27Builtin_Impl_MakeRangeErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18395:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	12464(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %r14
	movq	39(%rax), %rax
	movq	1719(%rax), %r8
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L64
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L65:
	cmpl	$5, %r15d
	jg	.L67
	leaq	88(%r12), %rax
	movq	%rax, %r9
.L68:
	movq	%r9, %rcx
.L70:
	movq	%r9, %r8
.L74:
	subq	$8, %rsp
	movq	(%rax), %rdx
	movq	%r12, %rdi
	pushq	$2
	sarq	$32, %rdx
	call	_ZN2v88internal10ErrorUtils16MakeGenericErrorEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS4_INS0_6ObjectEEES9_S9_NS0_13FrameSkipModeE@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L81
	movq	(%rax), %r15
.L76:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L79
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L79:
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	leaq	-8(%rbx), %rax
	cmpl	$6, %r15d
	je	.L82
	leaq	-16(%rbx), %rcx
	cmpl	$7, %r15d
	je	.L83
	leaq	-24(%rbx), %r8
	cmpl	$8, %r15d
	je	.L84
	leaq	-32(%rbx), %r9
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L64:
	movq	%r13, %rsi
	cmpq	41096(%rdx), %r13
	je	.L85
.L66:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L81:
	movq	312(%r12), %r15
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%rdx, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L66
.L82:
	leaq	88(%r12), %r9
	jmp	.L68
.L84:
	leaq	88(%r12), %r9
	jmp	.L74
.L83:
	leaq	88(%r12), %r9
	jmp	.L70
	.cfi_endproc
.LFE18395:
	.size	_ZN2v88internalL27Builtin_Impl_MakeRangeErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL27Builtin_Impl_MakeRangeErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internalL28Builtin_Impl_MakeSyntaxErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Builtin_Impl_MakeSyntaxErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL28Builtin_Impl_MakeSyntaxErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18398:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	12464(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %r14
	movq	39(%rax), %rax
	movq	1759(%rax), %r8
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L87
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L88:
	cmpl	$5, %r15d
	jg	.L90
	leaq	88(%r12), %rax
	movq	%rax, %r9
.L91:
	movq	%r9, %rcx
.L93:
	movq	%r9, %r8
.L97:
	subq	$8, %rsp
	movq	(%rax), %rdx
	movq	%r12, %rdi
	pushq	$2
	sarq	$32, %rdx
	call	_ZN2v88internal10ErrorUtils16MakeGenericErrorEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS4_INS0_6ObjectEEES9_S9_NS0_13FrameSkipModeE@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L104
	movq	(%rax), %r15
.L99:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L102
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L102:
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	leaq	-8(%rbx), %rax
	cmpl	$6, %r15d
	je	.L105
	leaq	-16(%rbx), %rcx
	cmpl	$7, %r15d
	je	.L106
	leaq	-24(%rbx), %r8
	cmpl	$8, %r15d
	je	.L107
	leaq	-32(%rbx), %r9
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L87:
	movq	%r13, %rsi
	cmpq	41096(%rdx), %r13
	je	.L108
.L89:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L104:
	movq	312(%r12), %r15
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L108:
	movq	%rdx, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L89
.L105:
	leaq	88(%r12), %r9
	jmp	.L91
.L107:
	leaq	88(%r12), %r9
	jmp	.L97
.L106:
	leaq	88(%r12), %r9
	jmp	.L93
	.cfi_endproc
.LFE18398:
	.size	_ZN2v88internalL28Builtin_Impl_MakeSyntaxErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL28Builtin_Impl_MakeSyntaxErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L115
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L118
.L109:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L109
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL41Builtin_Impl_Stats_ErrorPrototypeToStringEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC1:
	.string	"V8.Builtin_ErrorPrototypeToString"
	.section	.text._ZN2v88internalL41Builtin_Impl_Stats_ErrorPrototypeToStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Builtin_Impl_Stats_ErrorPrototypeToStringEiPmPNS0_7IsolateE, @function
_ZN2v88internalL41Builtin_Impl_Stats_ErrorPrototypeToStringEiPmPNS0_7IsolateE:
.LFB18386:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L152
.L120:
	movq	_ZZN2v88internalL41Builtin_Impl_Stats_ErrorPrototypeToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic82(%rip), %rbx
	testq	%rbx, %rbx
	je	.L153
.L122:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L154
.L124:
	addl	$1, 41104(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	call	_ZN2v88internal10ErrorUtils8ToStringEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L155
	movq	(%rax), %r13
.L129:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L132
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L132:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L156
.L119:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L157
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L158
.L125:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L126
	movq	(%rdi), %rax
	call	*8(%rax)
.L126:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L127
	movq	(%rdi), %rax
	call	*8(%rax)
.L127:
	leaq	.LC1(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L153:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L159
.L123:
	movq	%rbx, _ZZN2v88internalL41Builtin_Impl_Stats_ErrorPrototypeToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic82(%rip)
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L155:
	movq	312(%r12), %r13
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L156:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L152:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$765, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L159:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L158:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC1(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L125
.L157:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18386:
	.size	_ZN2v88internalL41Builtin_Impl_Stats_ErrorPrototypeToStringEiPmPNS0_7IsolateE, .-_ZN2v88internalL41Builtin_Impl_Stats_ErrorPrototypeToStringEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL28Builtin_Impl_Stats_MakeErrorEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"V8.Builtin_MakeError"
	.section	.text._ZN2v88internalL28Builtin_Impl_Stats_MakeErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Builtin_Impl_Stats_MakeErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL28Builtin_Impl_Stats_MakeErrorEiPmPNS0_7IsolateE:
.LFB18390:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L189
.L161:
	movq	_ZZN2v88internalL28Builtin_Impl_Stats_MakeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic108(%rip), %rbx
	testq	%rbx, %rbx
	je	.L190
.L163:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L191
.L165:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL22Builtin_Impl_MakeErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L192
.L169:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L193
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L194
.L164:
	movq	%rbx, _ZZN2v88internalL28Builtin_Impl_Stats_MakeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic108(%rip)
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L191:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L195
.L166:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L167
	movq	(%rdi), %rax
	call	*8(%rax)
.L167:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L168
	movq	(%rdi), %rax
	call	*8(%rax)
.L168:
	leaq	.LC2(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L192:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L189:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$766, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L195:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC2(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L194:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L164
.L193:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18390:
	.size	_ZN2v88internalL28Builtin_Impl_Stats_MakeErrorEiPmPNS0_7IsolateE, .-_ZN2v88internalL28Builtin_Impl_Stats_MakeErrorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL33Builtin_Impl_Stats_MakeRangeErrorEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"V8.Builtin_MakeRangeError"
	.section	.text._ZN2v88internalL33Builtin_Impl_Stats_MakeRangeErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Builtin_Impl_Stats_MakeRangeErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL33Builtin_Impl_Stats_MakeRangeErrorEiPmPNS0_7IsolateE:
.LFB18393:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L225
.L197:
	movq	_ZZN2v88internalL33Builtin_Impl_Stats_MakeRangeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic113(%rip), %rbx
	testq	%rbx, %rbx
	je	.L226
.L199:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L227
.L201:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL27Builtin_Impl_MakeRangeErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L228
.L205:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L229
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L230
.L200:
	movq	%rbx, _ZZN2v88internalL33Builtin_Impl_Stats_MakeRangeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic113(%rip)
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L227:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L231
.L202:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L203
	movq	(%rdi), %rax
	call	*8(%rax)
.L203:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L204
	movq	(%rdi), %rax
	call	*8(%rax)
.L204:
	leaq	.LC3(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L228:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L225:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$767, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L231:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC3(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L230:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L200
.L229:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18393:
	.size	_ZN2v88internalL33Builtin_Impl_Stats_MakeRangeErrorEiPmPNS0_7IsolateE, .-_ZN2v88internalL33Builtin_Impl_Stats_MakeRangeErrorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL34Builtin_Impl_Stats_MakeSyntaxErrorEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"V8.Builtin_MakeSyntaxError"
	.section	.text._ZN2v88internalL34Builtin_Impl_Stats_MakeSyntaxErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Builtin_Impl_Stats_MakeSyntaxErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL34Builtin_Impl_Stats_MakeSyntaxErrorEiPmPNS0_7IsolateE:
.LFB18396:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L261
.L233:
	movq	_ZZN2v88internalL34Builtin_Impl_Stats_MakeSyntaxErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic118(%rip), %rbx
	testq	%rbx, %rbx
	je	.L262
.L235:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L263
.L237:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL28Builtin_Impl_MakeSyntaxErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L264
.L241:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L265
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L266
.L236:
	movq	%rbx, _ZZN2v88internalL34Builtin_Impl_Stats_MakeSyntaxErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic118(%rip)
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L263:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L267
.L238:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L239
	movq	(%rdi), %rax
	call	*8(%rax)
.L239:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L240
	movq	(%rdi), %rax
	call	*8(%rax)
.L240:
	leaq	.LC4(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L264:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L261:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$768, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L267:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC4(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L266:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L236
.L265:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18396:
	.size	_ZN2v88internalL34Builtin_Impl_Stats_MakeSyntaxErrorEiPmPNS0_7IsolateE, .-_ZN2v88internalL34Builtin_Impl_Stats_MakeSyntaxErrorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL32Builtin_Impl_Stats_MakeTypeErrorEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"V8.Builtin_MakeTypeError"
	.section	.text._ZN2v88internalL32Builtin_Impl_Stats_MakeTypeErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Builtin_Impl_Stats_MakeTypeErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL32Builtin_Impl_Stats_MakeTypeErrorEiPmPNS0_7IsolateE:
.LFB18399:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L297
.L269:
	movq	_ZZN2v88internalL32Builtin_Impl_Stats_MakeTypeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic123(%rip), %rbx
	testq	%rbx, %rbx
	je	.L298
.L271:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L299
.L273:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL26Builtin_Impl_MakeTypeErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L300
.L277:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L301
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L302
.L272:
	movq	%rbx, _ZZN2v88internalL32Builtin_Impl_Stats_MakeTypeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic123(%rip)
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L299:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L303
.L274:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L275
	movq	(%rdi), %rax
	call	*8(%rax)
.L275:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L276
	movq	(%rdi), %rax
	call	*8(%rax)
.L276:
	leaq	.LC5(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L300:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L297:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$769, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L303:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC5(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L302:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L272
.L301:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18399:
	.size	_ZN2v88internalL32Builtin_Impl_Stats_MakeTypeErrorEiPmPNS0_7IsolateE, .-_ZN2v88internalL32Builtin_Impl_Stats_MakeTypeErrorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL31Builtin_Impl_Stats_MakeURIErrorEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"V8.Builtin_MakeURIError"
	.section	.text._ZN2v88internalL31Builtin_Impl_Stats_MakeURIErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Builtin_Impl_Stats_MakeURIErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Builtin_Impl_Stats_MakeURIErrorEiPmPNS0_7IsolateE:
.LFB18402:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L333
.L305:
	movq	_ZZN2v88internalL31Builtin_Impl_Stats_MakeURIErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic128(%rip), %rbx
	testq	%rbx, %rbx
	je	.L334
.L307:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L335
.L309:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL25Builtin_Impl_MakeURIErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L336
.L313:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L337
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L338
.L308:
	movq	%rbx, _ZZN2v88internalL31Builtin_Impl_Stats_MakeURIErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic128(%rip)
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L335:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L339
.L310:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L311
	movq	(%rdi), %rax
	call	*8(%rax)
.L311:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L312
	movq	(%rdi), %rax
	call	*8(%rax)
.L312:
	leaq	.LC6(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L336:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L333:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$770, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L339:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC6(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L338:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L308
.L337:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18402:
	.size	_ZN2v88internalL31Builtin_Impl_Stats_MakeURIErrorEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Builtin_Impl_Stats_MakeURIErrorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL41Builtin_Impl_Stats_ErrorCaptureStackTraceEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"V8.Builtin_ErrorCaptureStackTrace"
	.section	.text._ZN2v88internalL41Builtin_Impl_Stats_ErrorCaptureStackTraceEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Builtin_Impl_Stats_ErrorCaptureStackTraceEiPmPNS0_7IsolateE, @function
_ZN2v88internalL41Builtin_Impl_Stats_ErrorCaptureStackTraceEiPmPNS0_7IsolateE:
.LFB18383:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L389
.L341:
	movq	_ZZN2v88internalL41Builtin_Impl_Stats_ErrorCaptureStackTraceEiPmPNS0_7IsolateEE27trace_event_unique_atomic42(%rip), %r13
	testq	%r13, %r13
	je	.L390
.L343:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L391
.L345:
	leaq	88(%r15), %r14
	leaq	-8(%rbx), %r9
	movl	$43, %esi
	movq	%r15, %rdi
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	cmpl	$5, %r12d
	cmovle	%r14, %r9
	movq	%rax, -176(%rbp)
	movq	41096(%r15), %rax
	movq	%r9, %r13
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L351
.L353:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$64, %esi
.L388:
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r12
.L352:
	movq	-176(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-168(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L366
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L366:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L392
.L340:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L393
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L351:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L353
	subq	$16, %rbx
	cmpl	$6, %r12d
	cmovg	%rbx, %r14
	xorl	%r12d, %r12d
	movq	(%r14), %rax
	testb	$1, %al
	jne	.L394
.L356:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal7Isolate31CaptureAndSetDetailedStackTraceENS0_6HandleINS0_10JSReceiverEEE@PLT
	testq	%rax, %rax
	je	.L387
	movq	%r14, %rcx
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal7Isolate29CaptureAndSetSimpleStackTraceENS0_6HandleINS0_10JSReceiverEEENS0_13FrameSkipModeENS2_INS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L387
	movq	4400(%r15), %rax
	movq	41112(%r15), %rdi
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L359
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L360:
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject12IsExtensibleENS0_6HandleIS1_EE@PLT
	testb	%al, %al
	jne	.L362
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movl	$49, %esi
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L391:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L395
.L346:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L347
	movq	(%rdi), %rax
	call	*8(%rax)
.L347:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L348
	movq	(%rdi), %rax
	call	*8(%rax)
.L348:
	leaq	.LC7(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L390:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L396
.L344:
	movq	%r13, _ZZN2v88internalL41Builtin_Impl_Stats_ErrorCaptureStackTraceEiPmPNS0_7IsolateEE27trace_event_unique_atomic42(%rip)
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L387:
	movq	312(%r15), %r12
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L392:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L389:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$764, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L396:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L395:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC7(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L394:
	movq	-1(%rax), %rax
	xorl	%r12d, %r12d
	cmpw	$1105, 11(%rax)
	sete	%r12b
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L359:
	movq	41088(%r15), %r12
	cmpq	41096(%r15), %r12
	je	.L397
.L361:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r12)
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L362:
	leaq	4400(%r15), %rdx
	movl	$2, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject11SetAccessorENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_12AccessorInfoEEENS0_18PropertyAttributesE@PLT
	testq	%rax, %rax
	je	.L387
	movq	88(%r15), %r12
	jmp	.L352
.L397:
	movq	%r15, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L361
.L393:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18383:
	.size	_ZN2v88internalL41Builtin_Impl_Stats_ErrorCaptureStackTraceEiPmPNS0_7IsolateE, .-_ZN2v88internalL41Builtin_Impl_Stats_ErrorCaptureStackTraceEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL35Builtin_Impl_Stats_ErrorConstructorEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"V8.Builtin_ErrorConstructor"
	.section	.text._ZN2v88internalL35Builtin_Impl_Stats_ErrorConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Builtin_Impl_Stats_ErrorConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL35Builtin_Impl_Stats_ErrorConstructorEiPmPNS0_7IsolateE:
.LFB18380:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L435
.L399:
	movq	_ZZN2v88internalL35Builtin_Impl_Stats_ErrorConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic19(%rip), %r13
	testq	%r13, %r13
	je	.L436
.L401:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L437
.L403:
	leal	-8(,%r15,8), %eax
	movq	%rbx, %rsi
	xorl	%r9d, %r9d
	addl	$1, 41104(%r12)
	movslq	%eax, %rdx
	movq	41088(%r12), %r13
	movq	41096(%r12), %r14
	movq	%r12, %rdi
	subq	%rdx, %rsi
	movq	(%rsi), %rcx
	movq	%rsi, %rdx
	movq	-1(%rcx), %rcx
	cmpw	$1105, 11(%rcx)
	leaq	-8(%rbx), %rcx
	cmove	%rsi, %r9
	sete	%r8b
	leaq	88(%r12), %rsi
	cmpl	$5, %r15d
	cmovle	%rsi, %rcx
	subq	$8, %rsp
	subl	$8, %eax
	pushq	$0
	cltq
	movzbl	%r8b, %r8d
	subq	%rax, %rbx
	movq	%rbx, %rsi
	call	_ZN2v88internal10ErrorUtils9ConstructEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_6ObjectEEES8_NS0_13FrameSkipModeES8_NS1_20StackTraceCollectionE@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L438
	movq	(%rax), %r15
.L411:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L414
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L414:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L439
.L398:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L440
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L437:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L441
.L404:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L405
	movq	(%rdi), %rax
	call	*8(%rax)
.L405:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L406
	movq	(%rdi), %rax
	call	*8(%rax)
.L406:
	leaq	.LC8(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L436:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L442
.L402:
	movq	%r13, _ZZN2v88internalL35Builtin_Impl_Stats_ErrorConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic19(%rip)
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L438:
	movq	312(%r12), %r15
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L439:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L435:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$763, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L442:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L441:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC8(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L404
.L440:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18380:
	.size	_ZN2v88internalL35Builtin_Impl_Stats_ErrorConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internalL35Builtin_Impl_Stats_ErrorConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal24Builtin_ErrorConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24Builtin_ErrorConstructorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal24Builtin_ErrorConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internal24Builtin_ErrorConstructorEiPmPNS0_7IsolateE:
.LFB18381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %r8d
	testl	%r8d, %r8d
	jne	.L453
	leal	-8(,%rdi,8), %eax
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	movslq	%eax, %rdx
	leaq	88(%r12), %r10
	subq	%rdx, %rcx
	movq	%rcx, %rdx
	movq	(%rcx), %rcx
	movq	-1(%rcx), %rcx
	cmpw	$1105, 11(%rcx)
	leaq	-8(%rsi), %rcx
	cmove	%rdx, %r9
	sete	%r8b
	cmpl	$5, %edi
	movq	%r12, %rdi
	cmovle	%r10, %rcx
	subq	$8, %rsp
	subl	$8, %eax
	pushq	$0
	cltq
	movzbl	%r8b, %r8d
	subq	%rax, %rsi
	call	_ZN2v88internal10ErrorUtils9ConstructEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_6ObjectEEES8_NS0_13FrameSkipModeES8_NS1_20StackTraceCollectionE@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L454
	movq	(%rax), %r14
.L449:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L443
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L443:
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L454:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L453:
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL35Builtin_Impl_Stats_ErrorConstructorEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18381:
	.size	_ZN2v88internal24Builtin_ErrorConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internal24Builtin_ErrorConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Builtin_ErrorCaptureStackTraceEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Builtin_ErrorCaptureStackTraceEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Builtin_ErrorCaptureStackTraceEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Builtin_ErrorCaptureStackTraceEiPmPNS0_7IsolateE:
.LFB18384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %r15d
	testl	%r15d, %r15d
	jne	.L477
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	leaq	-8(%rsi), %r9
	cmpl	$5, %edi
	movl	%edi, -56(%rbp)
	movl	$43, %esi
	movq	%rdx, %rdi
	movq	41096(%rdx), %rbx
	movq	%rax, -64(%rbp)
	leaq	88(%rdx), %rax
	cmovle	%rax, %r9
	movq	%rax, -72(%rbp)
	movq	%r9, %r14
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	(%r14), %rax
	movl	-56(%rbp), %r8d
	testb	$1, %al
	jne	.L459
.L461:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$64, %esi
.L476:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L460:
	subl	$1, 41104(%r12)
	movq	-64(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L455
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L455:
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L459:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L461
	subq	$16, %r13
	cmpl	$6, %r8d
	cmovle	-72(%rbp), %r13
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L478
.L464:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate31CaptureAndSetDetailedStackTraceENS0_6HandleINS0_10JSReceiverEEE@PLT
	testq	%rax, %rax
	je	.L475
	movq	%r13, %rcx
	movl	%r15d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate29CaptureAndSetSimpleStackTraceENS0_6HandleINS0_10JSReceiverEEENS0_13FrameSkipModeENS2_INS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L475
	movq	4400(%r12), %rax
	movq	41112(%r12), %rdi
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L467
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L468:
	movq	%r14, %rdi
	call	_ZN2v88internal8JSObject12IsExtensibleENS0_6HandleIS1_EE@PLT
	testb	%al, %al
	jne	.L470
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$49, %esi
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L475:
	movq	312(%r12), %r13
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L477:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL41Builtin_Impl_Stats_ErrorCaptureStackTraceEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L467:
	.cfi_restore_state
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L479
.L469:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L478:
	movq	-1(%rax), %rax
	xorl	%r15d, %r15d
	cmpw	$1105, 11(%rax)
	sete	%r15b
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L470:
	leaq	4400(%r12), %rdx
	movl	$2, %ecx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8JSObject11SetAccessorENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_12AccessorInfoEEENS0_18PropertyAttributesE@PLT
	testq	%rax, %rax
	je	.L475
	movq	88(%r12), %r13
	jmp	.L460
.L479:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L469
	.cfi_endproc
.LFE18384:
	.size	_ZN2v88internal30Builtin_ErrorCaptureStackTraceEiPmPNS0_7IsolateE, .-_ZN2v88internal30Builtin_ErrorCaptureStackTraceEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Builtin_ErrorPrototypeToStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Builtin_ErrorPrototypeToStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Builtin_ErrorPrototypeToStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Builtin_ErrorPrototypeToStringEiPmPNS0_7IsolateE:
.LFB18387:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L487
	addl	$1, 41104(%rdx)
	movq	%rdx, %rdi
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	call	_ZN2v88internal10ErrorUtils8ToStringEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L488
	movq	(%rax), %r14
.L483:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L480
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L480:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L488:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L487:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL41Builtin_Impl_Stats_ErrorPrototypeToStringEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18387:
	.size	_ZN2v88internal30Builtin_ErrorPrototypeToStringEiPmPNS0_7IsolateE, .-_ZN2v88internal30Builtin_ErrorPrototypeToStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal17Builtin_MakeErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal17Builtin_MakeErrorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal17Builtin_MakeErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internal17Builtin_MakeErrorEiPmPNS0_7IsolateE:
.LFB18391:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L493
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL22Builtin_Impl_MakeErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L493:
	.cfi_restore 6
	jmp	_ZN2v88internalL28Builtin_Impl_Stats_MakeErrorEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18391:
	.size	_ZN2v88internal17Builtin_MakeErrorEiPmPNS0_7IsolateE, .-_ZN2v88internal17Builtin_MakeErrorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Builtin_MakeRangeErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Builtin_MakeRangeErrorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Builtin_MakeRangeErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Builtin_MakeRangeErrorEiPmPNS0_7IsolateE:
.LFB18394:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L498
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL27Builtin_Impl_MakeRangeErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L498:
	.cfi_restore 6
	jmp	_ZN2v88internalL33Builtin_Impl_Stats_MakeRangeErrorEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18394:
	.size	_ZN2v88internal22Builtin_MakeRangeErrorEiPmPNS0_7IsolateE, .-_ZN2v88internal22Builtin_MakeRangeErrorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Builtin_MakeSyntaxErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Builtin_MakeSyntaxErrorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Builtin_MakeSyntaxErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Builtin_MakeSyntaxErrorEiPmPNS0_7IsolateE:
.LFB18397:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L503
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL28Builtin_Impl_MakeSyntaxErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L503:
	.cfi_restore 6
	jmp	_ZN2v88internalL34Builtin_Impl_Stats_MakeSyntaxErrorEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18397:
	.size	_ZN2v88internal23Builtin_MakeSyntaxErrorEiPmPNS0_7IsolateE, .-_ZN2v88internal23Builtin_MakeSyntaxErrorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Builtin_MakeTypeErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Builtin_MakeTypeErrorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Builtin_MakeTypeErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Builtin_MakeTypeErrorEiPmPNS0_7IsolateE:
.LFB18400:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L508
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL26Builtin_Impl_MakeTypeErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L508:
	.cfi_restore 6
	jmp	_ZN2v88internalL32Builtin_Impl_Stats_MakeTypeErrorEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18400:
	.size	_ZN2v88internal21Builtin_MakeTypeErrorEiPmPNS0_7IsolateE, .-_ZN2v88internal21Builtin_MakeTypeErrorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal20Builtin_MakeURIErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20Builtin_MakeURIErrorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal20Builtin_MakeURIErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internal20Builtin_MakeURIErrorEiPmPNS0_7IsolateE:
.LFB18403:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L513
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL25Builtin_Impl_MakeURIErrorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L513:
	.cfi_restore 6
	jmp	_ZN2v88internalL31Builtin_Impl_Stats_MakeURIErrorEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18403:
	.size	_ZN2v88internal20Builtin_MakeURIErrorEiPmPNS0_7IsolateE, .-_ZN2v88internal20Builtin_MakeURIErrorEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal24Builtin_ErrorConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal24Builtin_ErrorConstructorEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal24Builtin_ErrorConstructorEiPmPNS0_7IsolateE:
.LFB22139:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22139:
	.size	_GLOBAL__sub_I__ZN2v88internal24Builtin_ErrorConstructorEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal24Builtin_ErrorConstructorEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal24Builtin_ErrorConstructorEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL31Builtin_Impl_Stats_MakeURIErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic128,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Builtin_Impl_Stats_MakeURIErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic128, @object
	.size	_ZZN2v88internalL31Builtin_Impl_Stats_MakeURIErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic128, 8
_ZZN2v88internalL31Builtin_Impl_Stats_MakeURIErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic128:
	.zero	8
	.section	.bss._ZZN2v88internalL32Builtin_Impl_Stats_MakeTypeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic123,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Builtin_Impl_Stats_MakeTypeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic123, @object
	.size	_ZZN2v88internalL32Builtin_Impl_Stats_MakeTypeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic123, 8
_ZZN2v88internalL32Builtin_Impl_Stats_MakeTypeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic123:
	.zero	8
	.section	.bss._ZZN2v88internalL34Builtin_Impl_Stats_MakeSyntaxErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic118,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL34Builtin_Impl_Stats_MakeSyntaxErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic118, @object
	.size	_ZZN2v88internalL34Builtin_Impl_Stats_MakeSyntaxErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic118, 8
_ZZN2v88internalL34Builtin_Impl_Stats_MakeSyntaxErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic118:
	.zero	8
	.section	.bss._ZZN2v88internalL33Builtin_Impl_Stats_MakeRangeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic113,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Builtin_Impl_Stats_MakeRangeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic113, @object
	.size	_ZZN2v88internalL33Builtin_Impl_Stats_MakeRangeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic113, 8
_ZZN2v88internalL33Builtin_Impl_Stats_MakeRangeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic113:
	.zero	8
	.section	.bss._ZZN2v88internalL28Builtin_Impl_Stats_MakeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic108,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Builtin_Impl_Stats_MakeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic108, @object
	.size	_ZZN2v88internalL28Builtin_Impl_Stats_MakeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic108, 8
_ZZN2v88internalL28Builtin_Impl_Stats_MakeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic108:
	.zero	8
	.section	.bss._ZZN2v88internalL41Builtin_Impl_Stats_ErrorPrototypeToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic82,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL41Builtin_Impl_Stats_ErrorPrototypeToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic82, @object
	.size	_ZZN2v88internalL41Builtin_Impl_Stats_ErrorPrototypeToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic82, 8
_ZZN2v88internalL41Builtin_Impl_Stats_ErrorPrototypeToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic82:
	.zero	8
	.section	.bss._ZZN2v88internalL41Builtin_Impl_Stats_ErrorCaptureStackTraceEiPmPNS0_7IsolateEE27trace_event_unique_atomic42,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL41Builtin_Impl_Stats_ErrorCaptureStackTraceEiPmPNS0_7IsolateEE27trace_event_unique_atomic42, @object
	.size	_ZZN2v88internalL41Builtin_Impl_Stats_ErrorCaptureStackTraceEiPmPNS0_7IsolateEE27trace_event_unique_atomic42, 8
_ZZN2v88internalL41Builtin_Impl_Stats_ErrorCaptureStackTraceEiPmPNS0_7IsolateEE27trace_event_unique_atomic42:
	.zero	8
	.section	.bss._ZZN2v88internalL35Builtin_Impl_Stats_ErrorConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic19,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Builtin_Impl_Stats_ErrorConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic19, @object
	.size	_ZZN2v88internalL35Builtin_Impl_Stats_ErrorConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic19, 8
_ZZN2v88internalL35Builtin_Impl_Stats_ErrorConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic19:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
