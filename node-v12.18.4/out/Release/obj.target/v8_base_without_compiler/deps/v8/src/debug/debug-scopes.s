	.file	"debug-scopes.cc"
	.text
	.section	.text._ZNSt17_Function_handlerIFbN2v88internal6HandleINS1_6StringEEENS2_INS1_6ObjectEEEEZNKS1_13ScopeIterator14DeclaresLocalsENS8_4ModeEEUlS4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_OS6_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFbN2v88internal6HandleINS1_6StringEEENS2_INS1_6ObjectEEEEZNKS1_13ScopeIterator14DeclaresLocalsENS8_4ModeEEUlS4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_OS6_, @function
_ZNSt17_Function_handlerIFbN2v88internal6HandleINS1_6StringEEENS2_INS1_6ObjectEEEEZNKS1_13ScopeIterator14DeclaresLocalsENS8_4ModeEEUlS4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_OS6_:
.LFB23785:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movb	$1, (%rax)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE23785:
	.size	_ZNSt17_Function_handlerIFbN2v88internal6HandleINS1_6StringEEENS2_INS1_6ObjectEEEEZNKS1_13ScopeIterator14DeclaresLocalsENS8_4ModeEEUlS4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_OS6_, .-_ZNSt17_Function_handlerIFbN2v88internal6HandleINS1_6StringEEENS2_INS1_6ObjectEEEEZNKS1_13ScopeIterator14DeclaresLocalsENS8_4ModeEEUlS4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_OS6_
	.section	.text._ZNSt14_Function_base13_Base_managerIZNK2v88internal13ScopeIterator14DeclaresLocalsENS3_4ModeEEUlNS2_6HandleINS2_6StringEEENS5_INS2_6ObjectEEEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZNK2v88internal13ScopeIterator14DeclaresLocalsENS3_4ModeEEUlNS2_6HandleINS2_6StringEEENS5_INS2_6ObjectEEEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZNK2v88internal13ScopeIterator14DeclaresLocalsENS3_4ModeEEUlNS2_6HandleINS2_6StringEEENS5_INS2_6ObjectEEEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation:
.LFB23786:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L4
	cmpl	$3, %edx
	je	.L5
	cmpl	$1, %edx
	je	.L9
.L5:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE23786:
	.size	_ZNSt14_Function_base13_Base_managerIZNK2v88internal13ScopeIterator14DeclaresLocalsENS3_4ModeEEUlNS2_6HandleINS2_6StringEEENS5_INS2_6ObjectEEEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZNK2v88internal13ScopeIterator14DeclaresLocalsENS3_4ModeEEUlNS2_6HandleINS2_6StringEEENS5_INS2_6ObjectEEEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal13ScopeIterator11ScopeObjectENS3_4ModeEEUlNS2_6HandleINS2_6StringEEENS5_INS2_6ObjectEEEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal13ScopeIterator11ScopeObjectENS3_4ModeEEUlNS2_6HandleINS2_6StringEEENS5_INS2_6ObjectEEEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal13ScopeIterator11ScopeObjectENS3_4ModeEEUlNS2_6HandleINS2_6StringEEENS5_INS2_6ObjectEEEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation:
.LFB23790:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L11
	cmpl	$3, %edx
	je	.L12
	cmpl	$1, %edx
	je	.L16
.L12:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE23790:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal13ScopeIterator11ScopeObjectENS3_4ModeEEUlNS2_6HandleINS2_6StringEEENS5_INS2_6ObjectEEEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal13ScopeIterator11ScopeObjectENS3_4ModeEEUlNS2_6HandleINS2_6StringEEENS5_INS2_6ObjectEEEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFbN2v88internal6HandleINS1_6StringEEENS2_INS1_6ObjectEEEEZNS1_13ScopeIterator11ScopeObjectENS8_4ModeEEUlS4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_OS6_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFbN2v88internal6HandleINS1_6StringEEENS2_INS1_6ObjectEEEEZNS1_13ScopeIterator11ScopeObjectENS8_4ModeEEUlS4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_OS6_, @function
_ZNSt17_Function_handlerIFbN2v88internal6HandleINS1_6StringEEENS2_INS1_6ObjectEEEEZNS1_13ScopeIterator11ScopeObjectENS8_4ModeEEUlS4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_OS6_:
.LFB23789:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	%rsi, %r9
	movq	%rdx, %r8
	movq	8(%rdi), %rsi
	movq	(%r8), %rcx
	xorl	%r8d, %r8d
	movq	(%rax), %rdi
	movq	(%r9), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23789:
	.size	_ZNSt17_Function_handlerIFbN2v88internal6HandleINS1_6StringEEENS2_INS1_6ObjectEEEEZNS1_13ScopeIterator11ScopeObjectENS8_4ModeEEUlS4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_OS6_, .-_ZNSt17_Function_handlerIFbN2v88internal6HandleINS1_6StringEEENS2_INS1_6ObjectEEEEZNS1_13ScopeIterator11ScopeObjectENS8_4ModeEEUlS4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_OS6_
	.section	.text._ZN2v88internal13ScopeIterator17GetSourcePositionEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal13ScopeIterator17GetSourcePositionEv.part.0, @function
_ZN2v88internal13ScopeIterator17GetSourcePositionEv.part.0:
.LFB25258:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movq	23(%rax), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L20
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L21:
	movq	(%rbx), %rdi
	call	_ZN2v88internal18SharedFunctionInfo30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	24(%rbx), %rax
	leaq	-48(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal17JSGeneratorObject15source_positionEv@PLT
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L25
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L26
.L22:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L26:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L22
.L25:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25258:
	.size	_ZN2v88internal13ScopeIterator17GetSourcePositionEv.part.0, .-_ZN2v88internal13ScopeIterator17GetSourcePositionEv.part.0
	.section	.text._ZN2v88internal13ScopeIterator23UnwrapEvaluationContextEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal13ScopeIterator23UnwrapEvaluationContextEv.part.0, @function
_ZN2v88internal13ScopeIterator23UnwrapEvaluationContextEv.part.0:
.LFB25232:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	40(%rdi), %rax
	movq	(%rax), %rsi
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L30:
	movq	23(%rsi), %rsi
	movq	-1(%rsi), %rax
	leaq	-1(%rsi), %rdx
	cmpw	$141, 11(%rax)
	jne	.L37
.L31:
	movq	47(%rsi), %rax
	testb	$1, %al
	je	.L30
	movq	-1(%rax), %rcx
	leaq	-1(%rax), %rdx
	movzwl	11(%rcx), %ecx
	subw	$138, %cx
	cmpw	$9, %cx
	ja	.L30
	movq	%rax, %rsi
	movq	(%rdx), %rax
	cmpw	$141, 11(%rax)
	je	.L31
	.p2align 4,,10
	.p2align 3
.L37:
	movq	(%rbx), %r12
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L32
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, 40(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L38
.L34:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	movq	%rax, 40(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L38:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L34
	.cfi_endproc
.LFE25232:
	.size	_ZN2v88internal13ScopeIterator23UnwrapEvaluationContextEv.part.0, .-_ZN2v88internal13ScopeIterator23UnwrapEvaluationContextEv.part.0
	.section	.text._ZN2v88internal13ScopeIteratorD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScopeIteratorD2Ev
	.type	_ZN2v88internal13ScopeIteratorD2Ev, @function
_ZN2v88internal13ScopeIteratorD2Ev:
.LFB20239:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	testq	%r12, %r12
	je	.L39
	movq	%r12, %rdi
	call	_ZN2v88internal9ParseInfoD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$224, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20239:
	.size	_ZN2v88internal13ScopeIteratorD2Ev, .-_ZN2v88internal13ScopeIteratorD2Ev
	.globl	_ZN2v88internal13ScopeIteratorD1Ev
	.set	_ZN2v88internal13ScopeIteratorD1Ev,_ZN2v88internal13ScopeIteratorD2Ev
	.section	.text._ZNK2v88internal13ScopeIterator20GetFunctionDebugNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13ScopeIterator20GetFunctionDebugNameEv
	.type	_ZNK2v88internal13ScopeIterator20GetFunctionDebugNameEv, @function
_ZNK2v88internal13ScopeIterator20GetFunctionDebugNameEv:
.LFB20241:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 32(%rdi)
	je	.L43
	movq	32(%rdi), %rdi
	call	_ZN2v88internal10JSFunction12GetDebugNameENS0_6HandleIS1_EE@PLT
.L44:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L53
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movq	40(%rdi), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$145, 11(%rdx)
	jne	.L54
.L45:
	movq	(%rbx), %rax
	addq	$88, %rax
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L54:
	leaq	-32(%rbp), %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal7Context15closure_contextEv@PLT
	leaq	-40(%rbp), %rdi
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal7Context10scope_infoEv@PLT
	movq	(%rbx), %r12
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal9ScopeInfo17FunctionDebugNameEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L46
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L47:
	movl	11(%rsi), %edx
	testl	%edx, %edx
	jg	.L44
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L46:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L55
.L48:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L55:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L48
.L53:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20241:
	.size	_ZNK2v88internal13ScopeIterator20GetFunctionDebugNameEv, .-_ZNK2v88internal13ScopeIterator20GetFunctionDebugNameEv
	.section	.text._ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	.type	_ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE, @function
_ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE:
.LFB20243:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, 24(%rdi)
	movq	$0, 32(%rdi)
	movq	(%rdx), %rax
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	41112(%rbx), %rdi
	movq	31(%rax), %rsi
	testq	%rdi, %rdi
	je	.L57
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L58:
	movq	%rax, 40(%r12)
	movq	$0, 48(%r12)
	movq	$0, 56(%r12)
	movq	0(%r13), %rax
	movq	$0, 64(%r12)
	movq	$0, 72(%r12)
	movq	$0, 80(%r12)
	movb	$0, 88(%r12)
	movq	23(%rax), %r14
	movq	31(%r14), %rax
	testb	$1, %al
	jne	.L77
.L60:
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	jne	.L78
.L62:
	movq	$0, 40(%r12)
.L56:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L79
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	movq	7(%r14), %rax
	testb	$1, %al
	jne	.L80
.L64:
	movq	0(%r13), %rax
	movq	23(%rax), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L81
.L65:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L68
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L69:
	movq	%rax, 48(%r12)
	movq	40(%r12), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$141, 11(%rax)
	jne	.L56
	movq	%r12, %rdi
	call	_ZN2v88internal13ScopeIterator23UnwrapEvaluationContextEv.part.0
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L57:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L82
.L59:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L77:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	je	.L83
.L61:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rax
	je	.L62
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L68:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L84
.L70:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L81:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L65
	movq	23(%rsi), %rsi
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L83:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L61
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L80:
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	jne	.L64
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L84:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L70
.L79:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20243:
	.size	_ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE, .-_ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	.globl	_ZN2v88internal13ScopeIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	.set	_ZN2v88internal13ScopeIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE,_ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	.section	.text._ZN2v88internal13ScopeIterator7RestartEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScopeIterator7RestartEv
	.type	_ZN2v88internal13ScopeIterator7RestartEv, @function
_ZN2v88internal13ScopeIterator7RestartEv:
.LFB20248:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	movq	56(%rdi), %rax
	movq	%rax, 32(%r12)
	call	_ZN2v88internal14FrameInspector10GetContextEv@PLT
	movq	72(%r12), %rdx
	movq	%rax, 40(%r12)
	movq	%rdx, 80(%r12)
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$141, 11(%rax)
	jne	.L85
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13ScopeIterator23UnwrapEvaluationContextEv.part.0
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20248:
	.size	_ZN2v88internal13ScopeIterator7RestartEv, .-_ZN2v88internal13ScopeIterator7RestartEv
	.section	.rodata._ZN2v88internal13ScopeIterator25TryParseAndRetrieveScopesENS1_6OptionE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"DeclarationScope::Analyze(info_)"
	.section	.rodata._ZN2v88internal13ScopeIterator25TryParseAndRetrieveScopesENS1_6OptionE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internal13ScopeIterator25TryParseAndRetrieveScopesENS1_6OptionE.str1.8
	.align 8
.LC2:
	.string	"isolate_->has_pending_exception()"
	.section	.text._ZN2v88internal13ScopeIterator25TryParseAndRetrieveScopesENS1_6OptionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScopeIterator25TryParseAndRetrieveScopesENS1_6OptionE
	.type	_ZN2v88internal13ScopeIterator25TryParseAndRetrieveScopesENS1_6OptionE, @function
_ZN2v88internal13ScopeIterator25TryParseAndRetrieveScopesENS1_6OptionE:
.LFB20249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movl	%esi, -88(%rbp)
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L89
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rbx), %r14
	movq	(%rax), %rsi
	movq	%rax, %r12
	movq	15(%rsi), %r13
	testb	$1, %r13b
	jne	.L92
.L94:
	andq	$-262144, %rsi
	movq	24(%rsi), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal9ScopeInfo5EmptyEPNS0_7IsolateE@PLT
	movq	%rax, %r13
.L93:
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L95
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%r12), %rdx
	movq	%rax, %r15
	movq	31(%rdx), %rax
	testb	$1, %al
	jne	.L167
.L98:
	movq	(%rbx), %r13
	cmpq	%rax, 88(%r13)
	je	.L168
.L99:
	movl	47(%rdx), %eax
	andl	$31, %eax
	cmpb	$17, %al
	je	.L169
	leaq	-80(%rbp), %r13
	movq	%rdx, -80(%rbp)
	movq	%r13, %rdi
	call	_ZNK2v88internal18SharedFunctionInfo12HasBreakInfoEv@PLT
	testb	%al, %al
	je	.L105
	cmpq	$0, 16(%rbx)
	je	.L105
	movq	(%rbx), %rdx
	movq	(%r12), %rax
	movq	41112(%rdx), %rdi
	movq	31(%rax), %rsi
	testq	%rdi, %rdi
	je	.L170
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L109:
	movq	16(%rbx), %rdi
	call	_ZN2v88internal14FrameInspector16javascript_frameEv@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal13BreakLocation9FromFrameENS0_6HandleINS0_9DebugInfoEEEPNS0_15JavaScriptFrameE@PLT
	cmpl	$4, -68(%rbp)
	sete	-104(%rbp)
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L169:
	movq	$0, 64(%rbx)
	pxor	%xmm0, %xmm0
	movq	$0, 80(%rbx)
	movups	%xmm0, 32(%rbx)
.L88:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L171
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	movb	$0, -104(%rbp)
.L108:
	movq	(%r15), %rax
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal9ScopeInfo10scope_typeEv@PLT
	cmpb	$2, %al
	je	.L172
	movq	(%r12), %rax
	movq	(%rbx), %r14
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L173
.L113:
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L114
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L115:
	movl	$224, %edi
	movq	%rdx, -96(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rdx
	movq	(%rbx), %rsi
	movq	%rax, %r14
	movq	%rax, %rdi
	call	_ZN2v88internal9ParseInfoC1EPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE@PLT
	movq	%r14, 8(%rbx)
	movq	(%r15), %rax
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal9ScopeInfo10scope_typeEv@PLT
	cmpb	$1, %al
	je	.L174
	movq	(%r15), %rax
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal9ScopeInfo10scope_typeEv@PLT
	movq	8(%rbx), %r15
.L112:
	movq	(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal7parsing8ParseAnyEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE@PLT
	testb	%al, %al
	jne	.L124
.L127:
	movq	(%rbx), %rax
	movq	96(%rax), %rdx
	cmpq	12480(%rax), %rdx
	je	.L175
	movq	%rdx, 12480(%rax)
	movq	$0, 40(%rbx)
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L95:
	movq	41088(%r14), %r15
	cmpq	%r15, 41096(%r14)
	je	.L176
.L97:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r14)
	movq	%r13, (%r15)
	movq	(%r12), %rdx
	movq	31(%rdx), %rax
	testb	$1, %al
	je	.L98
.L167:
	movq	-1(%rax), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L98
	movq	(%rbx), %r13
	movq	23(%rax), %rax
	cmpq	%rax, 88(%r13)
	jne	.L99
	.p2align 4,,10
	.p2align 3
.L168:
	movq	32(%rbx), %rax
	movq	$0, 64(%rbx)
	movq	$0, 80(%rbx)
	movq	41112(%r13), %rdi
	movq	(%rax), %rax
	movq	31(%rax), %rsi
	testq	%rdi, %rdi
	je	.L100
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L101:
	movq	%rax, 40(%rbx)
	movq	$0, 32(%rbx)
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L89:
	movq	41088(%r13), %r12
	cmpq	41096(%r13), %r12
	je	.L177
.L91:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r12)
	movq	15(%rsi), %r13
	movq	(%rbx), %r14
	testb	$1, %r13b
	je	.L94
.L92:
	movq	-1(%r13), %rax
	cmpw	$136, 11(%rax)
	jne	.L94
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L100:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L178
.L102:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L114:
	movq	41088(%r14), %rdx
	cmpq	41096(%r14), %rdx
	je	.L179
.L116:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rdx)
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L173:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L113
	movq	23(%rsi), %rsi
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L172:
	movl	$224, %edi
	call	_Znwm@PLT
	movq	(%rbx), %rsi
	movq	%r12, %rdx
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN2v88internal9ParseInfoC1EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	movq	%r15, 8(%rbx)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L124:
	movq	8(%rbx), %rdi
	call	_ZN2v88internal8Rewriter7RewriteEPNS0_9ParseInfoE@PLT
	testb	%al, %al
	je	.L127
	movq	8(%rbx), %rax
	movq	(%rbx), %rsi
	movq	112(%rax), %rdi
	call	_ZN2v88internal15AstValueFactory11InternalizeEPNS0_7IsolateE@PLT
	movq	8(%rbx), %rdi
	cmpl	$2, -88(%rbp)
	movq	168(%rdi), %rax
	movq	40(%rax), %rax
	movq	%rax, 64(%rbx)
	je	.L180
.L128:
	call	_ZN2v88internal16DeclarationScope7AnalyzeEPNS0_9ParseInfoE@PLT
	testb	%al, %al
	je	.L181
	cmpb	$0, -104(%rbp)
	movq	64(%rbx), %r12
	je	.L132
	movq	%r12, %xmm0
	movq	40(%rbx), %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 72(%rbx)
	movl	124(%r12), %edx
	testl	%edx, %edx
	jg	.L182
.L133:
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$141, 11(%rax)
	jne	.L88
	movq	%rbx, %rdi
	call	_ZN2v88internal13ScopeIterator23UnwrapEvaluationContextEv.part.0
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L174:
	movq	8(%rbx), %rax
	orl	$4, 8(%rax)
	movq	40(%rbx), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$145, 11(%rdx)
	jne	.L183
.L118:
	movq	(%r12), %rax
	movq	8(%rbx), %rdx
	movl	47(%rax), %eax
	testb	$64, %al
	movl	8(%rdx), %eax
	jne	.L184
	andl	$-9, %eax
.L123:
	movl	%eax, 8(%rdx)
	movq	8(%rbx), %r15
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L177:
	movq	%r13, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L176:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r15
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L170:
	movq	41088(%rdx), %r14
	cmpq	41096(%rdx), %r14
	je	.L185
.L110:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r14)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L184:
	orl	$8, %eax
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L183:
	movq	(%rbx), %r15
	movq	%r13, %rdi
	movq	8(%rbx), %r14
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Context10scope_infoEv@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L119
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L120:
	movq	%rax, 88(%r14)
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L175:
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L178:
	movq	%r13, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L179:
	movq	%r14, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L119:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L186
.L121:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L132:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L137
	movl	72(%rax), %edx
.L138:
	testq	%r12, %r12
	je	.L139
	.p2align 4,,10
	.p2align 3
.L140:
	movq	16(%r12), %rax
	testq	%rax, %rax
	jne	.L143
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L146:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L139
.L143:
	cmpl	%edx, 112(%rax)
	jge	.L146
	cmpl	%edx, 116(%rax)
	jle	.L146
	cmpq	%rax, %r12
	je	.L139
	movq	%rax, %r12
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L139:
	movq	%r12, %xmm0
	movq	40(%rbx), %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 72(%rbx)
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L185:
	movq	%rdx, %rdi
	movq	%rsi, -104(%rbp)
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L182:
	movq	(%rbx), %r12
	movq	(%rax), %rax
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Context15closure_contextEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L134
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L135:
	movq	%rax, 40(%rbx)
	jmp	.L133
.L180:
	movq	168(%rdi), %rax
	movq	(%rbx), %rdi
	movq	40(%rax), %r12
	call	_ZN2v88internal9StringSet3NewEPNS0_7IsolateE@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal16DeclarationScope16CollectNonLocalsEPNS0_7IsolateEPNS0_9ParseInfoENS0_6HandleINS0_9StringSetEEE@PLT
	movq	64(%rbx), %rdi
	movq	%rax, 56(%rbx)
	testb	$16, 132(%rdi)
	je	.L129
.L166:
	movq	8(%rbx), %rdi
	jmp	.L128
.L134:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L187
.L136:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L135
.L181:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L186:
	movq	%r15, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	jmp	.L121
.L129:
	call	_ZNK2v88internal5Scope16HasThisReferenceEv@PLT
	testb	%al, %al
	je	.L166
	movq	(%rbx), %rdi
	movq	56(%rbx), %rsi
	leaq	3424(%rdi), %rdx
	call	_ZN2v88internal9StringSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE@PLT
	movq	%rax, 56(%rbx)
	jmp	.L166
.L137:
	movq	%rbx, %rdi
	call	_ZN2v88internal13ScopeIterator17GetSourcePositionEv.part.0
	movl	%eax, %edx
	jmp	.L138
.L187:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L136
.L171:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20249:
	.size	_ZN2v88internal13ScopeIterator25TryParseAndRetrieveScopesENS1_6OptionE, .-_ZN2v88internal13ScopeIterator25TryParseAndRetrieveScopesENS1_6OptionE
	.section	.text._ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateEPNS0_14FrameInspectorENS1_6OptionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateEPNS0_14FrameInspectorENS1_6OptionE
	.type	_ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateEPNS0_14FrameInspectorENS1_6OptionE, @function
_ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateEPNS0_14FrameInspectorENS1_6OptionE:
.LFB20236:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	$0, 24(%rdi)
	movq	56(%rdx), %rax
	movq	$0, 40(%rdi)
	movq	%rax, 32(%rdi)
	movq	40(%rdx), %rax
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movq	%rdx, 16(%rdi)
	movq	%rax, 48(%rdi)
	movq	$0, 56(%rdi)
	movq	$0, 64(%rdi)
	movq	$0, 72(%rdi)
	movq	$0, 80(%rdi)
	movb	$0, 88(%rdi)
	movq	%rdx, %rdi
	call	_ZN2v88internal14FrameInspector10GetContextEv@PLT
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L193
.L188:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	subw	$138, %ax
	cmpw	$9, %ax
	ja	.L188
	movq	%r13, %rdi
	call	_ZN2v88internal14FrameInspector10GetContextEv@PLT
	movl	%r14d, %esi
	movq	%r12, %rdi
	movq	%rax, 40(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13ScopeIterator25TryParseAndRetrieveScopesENS1_6OptionE
	.cfi_endproc
.LFE20236:
	.size	_ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateEPNS0_14FrameInspectorENS1_6OptionE, .-_ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateEPNS0_14FrameInspectorENS1_6OptionE
	.globl	_ZN2v88internal13ScopeIteratorC1EPNS0_7IsolateEPNS0_14FrameInspectorENS1_6OptionE
	.set	_ZN2v88internal13ScopeIteratorC1EPNS0_7IsolateEPNS0_14FrameInspectorENS1_6OptionE,_ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateEPNS0_14FrameInspectorENS1_6OptionE
	.section	.rodata._ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"function_->shared().IsSubjectToDebugging()"
	.section	.text._ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE
	.type	_ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE, @function
_ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE:
.LFB20246:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, (%rdi)
	movq	(%rdx), %rax
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	%rdx, 24(%rdi)
	movq	41112(%rbx), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L195
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L196:
	movq	%rax, 32(%r12)
	movq	0(%r13), %rax
	movq	41112(%rbx), %rdi
	movq	31(%rax), %rsi
	testq	%rdi, %rdi
	je	.L198
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L199:
	movq	%rax, 40(%r12)
	movq	32(%r12), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L216
.L201:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L202
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L203:
	movq	%rax, 48(%r12)
	movq	32(%r12), %rax
	movq	$0, 56(%r12)
	movq	$0, 64(%r12)
	movq	$0, 72(%r12)
	movq	$0, 80(%r12)
	movb	$0, 88(%r12)
	movq	(%rax), %rax
	movq	23(%rax), %rbx
	movq	31(%rbx), %rax
	testb	$1, %al
	jne	.L217
.L205:
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	jne	.L218
.L207:
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L218:
	movq	7(%rbx), %rax
	testb	$1, %al
	jne	.L219
.L209:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal13ScopeIterator25TryParseAndRetrieveScopesENS1_6OptionE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L220
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L221
.L204:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L198:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L222
.L200:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L195:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L223
.L197:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L217:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	je	.L224
.L206:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37504(%rdx)
	je	.L207
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L216:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L201
	movq	23(%rsi), %rsi
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L224:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L206
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L219:
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	jne	.L209
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L223:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L222:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L221:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L204
.L220:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20246:
	.size	_ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE, .-_ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE
	.globl	_ZN2v88internal13ScopeIteratorC1EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE
	.set	_ZN2v88internal13ScopeIteratorC1EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE,_ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE
	.section	.text._ZN2v88internal13ScopeIterator23UnwrapEvaluationContextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScopeIterator23UnwrapEvaluationContextEv
	.type	_ZN2v88internal13ScopeIterator23UnwrapEvaluationContextEv, @function
_ZN2v88internal13ScopeIterator23UnwrapEvaluationContextEv:
.LFB20250:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$141, 11(%rax)
	jne	.L225
	jmp	_ZN2v88internal13ScopeIterator23UnwrapEvaluationContextEv.part.0
	.p2align 4,,10
	.p2align 3
.L225:
	ret
	.cfi_endproc
.LFE20250:
	.size	_ZN2v88internal13ScopeIterator23UnwrapEvaluationContextEv, .-_ZN2v88internal13ScopeIterator23UnwrapEvaluationContextEv
	.section	.text._ZN2v88internal13ScopeIterator15HasPositionInfoEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScopeIterator15HasPositionInfoEv
	.type	_ZN2v88internal13ScopeIterator15HasPositionInfoEv, @function
_ZN2v88internal13ScopeIterator15HasPositionInfoEv:
.LFB20252:
	.cfi_startproc
	endbr64
	cmpq	$0, 32(%rdi)
	movl	$1, %eax
	je	.L230
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	movq	40(%rdi), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$145, 11(%rax)
	setne	%al
	ret
	.cfi_endproc
.LFE20252:
	.size	_ZN2v88internal13ScopeIterator15HasPositionInfoEv, .-_ZN2v88internal13ScopeIterator15HasPositionInfoEv
	.section	.text._ZN2v88internal13ScopeIterator14start_positionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScopeIterator14start_positionEv
	.type	_ZN2v88internal13ScopeIterator14start_positionEv, @function
_ZN2v88internal13ScopeIterator14start_positionEv:
.LFB20253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 32(%rdi)
	je	.L232
	movq	80(%rdi), %rax
	movl	112(%rax), %eax
.L231:
	movq	-8(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L237
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	movq	40(%rdi), %rax
	movq	(%rax), %rdx
	xorl	%eax, %eax
	movq	-1(%rdx), %rcx
	cmpw	$145, 11(%rcx)
	je	.L231
	leaq	-16(%rbp), %rdi
	movq	%rdx, -16(%rbp)
	call	_ZN2v88internal7Context15closure_contextEv@PLT
	leaq	-24(%rbp), %rdi
	movq	%rax, -24(%rbp)
	call	_ZN2v88internal7Context10scope_infoEv@PLT
	leaq	-32(%rbp), %rdi
	movq	%rax, -32(%rbp)
	call	_ZNK2v88internal9ScopeInfo13StartPositionEv@PLT
	jmp	.L231
.L237:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20253:
	.size	_ZN2v88internal13ScopeIterator14start_positionEv, .-_ZN2v88internal13ScopeIterator14start_positionEv
	.section	.text._ZN2v88internal13ScopeIterator12end_positionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScopeIterator12end_positionEv
	.type	_ZN2v88internal13ScopeIterator12end_positionEv, @function
_ZN2v88internal13ScopeIterator12end_positionEv:
.LFB20254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 32(%rdi)
	je	.L239
	movq	80(%rdi), %rax
	movl	116(%rax), %eax
.L238:
	movq	-8(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L244
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	movq	40(%rdi), %rax
	movq	(%rax), %rdx
	xorl	%eax, %eax
	movq	-1(%rdx), %rcx
	cmpw	$145, 11(%rcx)
	je	.L238
	leaq	-16(%rbp), %rdi
	movq	%rdx, -16(%rbp)
	call	_ZN2v88internal7Context15closure_contextEv@PLT
	leaq	-24(%rbp), %rdi
	movq	%rax, -24(%rbp)
	call	_ZN2v88internal7Context10scope_infoEv@PLT
	leaq	-32(%rbp), %rdi
	movq	%rax, -32(%rbp)
	call	_ZNK2v88internal9ScopeInfo11EndPositionEv@PLT
	jmp	.L238
.L244:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20254:
	.size	_ZN2v88internal13ScopeIterator12end_positionEv, .-_ZN2v88internal13ScopeIterator12end_positionEv
	.section	.text._ZNK2v88internal13ScopeIterator10HasContextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13ScopeIterator10HasContextEv
	.type	_ZNK2v88internal13ScopeIterator10HasContextEv, @function
_ZNK2v88internal13ScopeIterator10HasContextEv:
.LFB20260:
	.cfi_startproc
	endbr64
	cmpq	$0, 32(%rdi)
	movl	$1, %eax
	je	.L245
	movq	80(%rdi), %rax
	movl	124(%rax), %eax
	testl	%eax, %eax
	setg	%al
.L245:
	ret
	.cfi_endproc
.LFE20260:
	.size	_ZNK2v88internal13ScopeIterator10HasContextEv, .-_ZNK2v88internal13ScopeIterator10HasContextEv
	.section	.rodata._ZNK2v88internal13ScopeIterator4TypeEv.str1.1,"aMS",@progbits,1
.LC4:
	.string	"unreachable code"
	.section	.text._ZNK2v88internal13ScopeIterator4TypeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13ScopeIterator4TypeEv
	.type	_ZNK2v88internal13ScopeIterator4TypeEv, @function
_ZNK2v88internal13ScopeIterator4TypeEv:
.LFB20262:
	.cfi_startproc
	endbr64
	cmpq	$0, 32(%rdi)
	je	.L249
	movq	80(%rdi), %rax
	movzbl	128(%rax), %eax
	cmpb	$7, %al
	ja	.L250
	leaq	CSWTCH.1074(%rip), %rdx
	movl	(%rdx,%rax,4), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L249:
	movq	40(%rdi), %rax
	movq	(%rax), %rdx
	movq	-1(%rdx), %rax
	cmpw	$145, 11(%rax)
	je	.L262
	movq	-1(%rdx), %rax
	cmpw	$143, 11(%rax)
	je	.L254
	movq	-1(%rdx), %rax
	cmpw	$142, 11(%rax)
	je	.L254
	movq	-1(%rdx), %rax
	cmpw	$141, 11(%rax)
	je	.L254
	movq	-1(%rdx), %rcx
	movl	$4, %eax
	cmpw	$140, 11(%rcx)
	je	.L248
	movq	-1(%rdx), %rcx
	movl	$5, %eax
	cmpw	$139, 11(%rcx)
	je	.L248
	movq	-1(%rdx), %rcx
	movl	$8, %eax
	cmpw	$144, 11(%rcx)
	je	.L248
	movq	-1(%rdx), %rax
	cmpw	$146, 11(%rax)
	je	.L255
	movl	$2, %eax
.L248:
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	xorl	%eax, %eax
	cmpb	$0, 88(%rdi)
	jne	.L248
.L255:
	movl	$6, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	movl	$3, %eax
	ret
.L250:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20262:
	.size	_ZNK2v88internal13ScopeIterator4TypeEv, .-_ZNK2v88internal13ScopeIterator4TypeEv
	.section	.text._ZN2v88internal13ScopeIterator4NextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScopeIterator4NextEv
	.type	_ZN2v88internal13ScopeIterator4NextEv, @function
_ZN2v88internal13ScopeIterator4NextEv:
.LFB20261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	call	_ZNK2v88internal13ScopeIterator4TypeEv
	testl	%eax, %eax
	je	.L284
	movq	32(%r12), %rsi
	movq	80(%r12), %rdx
	movl	%eax, %ecx
	cmpq	64(%r12), %rdx
	je	.L285
	movq	40(%r12), %rax
	cmpl	$6, %ecx
	je	.L286
.L267:
	testq	%rsi, %rsi
	jne	.L273
	movq	(%r12), %rbx
	movq	(%rax), %rax
	movq	23(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L274
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L275:
	movq	%rax, 40(%r12)
	.p2align 4,,10
	.p2align 3
.L272:
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$141, 11(%rax)
	je	.L287
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	.cfi_restore_state
	movq	(%r12), %rbx
	movq	(%rax), %rax
	movq	23(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L278
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L279:
	movq	%rax, 40(%r12)
	movq	80(%r12), %rdx
.L277:
	movq	8(%rdx), %rdx
	movq	%rdx, 80(%r12)
	testb	$16, 129(%rdx)
	je	.L272
.L273:
	movl	124(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L277
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L278:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L289
.L280:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L287:
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13ScopeIterator23UnwrapEvaluationContextEv.part.0
	.p2align 4,,10
	.p2align 3
.L284:
	.cfi_restore_state
	movq	$0, 40(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	.cfi_restore_state
	movq	$0, 32(%r12)
	movq	40(%r12), %rax
	cmpl	$6, %ecx
	jne	.L267
.L286:
	movb	$1, 88(%r12)
	movq	(%rax), %rdx
	movq	-1(%rdx), %rcx
	cmpw	$146, 11(%rcx)
	jne	.L272
	movq	(%r12), %rbx
	movq	23(%rdx), %r13
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L269
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L289:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L274:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L290
.L276:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L275
.L269:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L291
.L271:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r13, (%rax)
	jmp	.L275
.L290:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L276
.L291:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L271
	.cfi_endproc
.LFE20261:
	.size	_ZN2v88internal13ScopeIterator4NextEv, .-_ZN2v88internal13ScopeIterator4NextEv
	.section	.text._ZN2v88internal13ScopeIterator12GetNonLocalsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScopeIterator12GetNonLocalsEv
	.type	_ZN2v88internal13ScopeIterator12GetNonLocalsEv, @function
_ZN2v88internal13ScopeIterator12GetNonLocalsEv:
.LFB20267:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	ret
	.cfi_endproc
.LFE20267:
	.size	_ZN2v88internal13ScopeIterator12GetNonLocalsEv, .-_ZN2v88internal13ScopeIterator12GetNonLocalsEv
	.section	.text._ZN2v88internal13ScopeIterator17GetSourcePositionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScopeIterator17GetSourcePositionEv
	.type	_ZN2v88internal13ScopeIterator17GetSourcePositionEv, @function
_ZN2v88internal13ScopeIterator17GetSourcePositionEv:
.LFB20268:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L294
	movl	72(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L294:
	jmp	_ZN2v88internal13ScopeIterator17GetSourcePositionEv.part.0
	.cfi_endproc
.LFE20268:
	.size	_ZN2v88internal13ScopeIterator17GetSourcePositionEv, .-_ZN2v88internal13ScopeIterator17GetSourcePositionEv
	.section	.text._ZN2v88internal13ScopeIterator18RetrieveScopeChainEPNS0_16DeclarationScopeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScopeIterator18RetrieveScopeChainEPNS0_16DeclarationScopeE
	.type	_ZN2v88internal13ScopeIterator18RetrieveScopeChainEPNS0_16DeclarationScopeE, @function
_ZN2v88internal13ScopeIterator18RetrieveScopeChainEPNS0_16DeclarationScopeE:
.LFB20269:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L329
	movl	72(%rax), %edx
	testq	%rsi, %rsi
	je	.L321
	movq	16(%rsi), %rax
	testq	%rax, %rax
	jne	.L323
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L324:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L321
.L323:
	cmpl	112(%rax), %edx
	jle	.L324
	cmpl	116(%rax), %edx
	jge	.L324
	cmpq	%rax, %rsi
	je	.L321
	movq	%rax, %rsi
	movq	16(%rsi), %rax
	testq	%rax, %rax
	jne	.L323
.L321:
	movq	%rsi, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 72(%rdi)
	ret
.L329:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rsi, -16(%rbp)
	movq	%rdi, -8(%rbp)
	call	_ZN2v88internal13ScopeIterator17GetSourcePositionEv.part.0
	movq	-16(%rbp), %rsi
	movq	-8(%rbp), %rdi
	movl	%eax, %edx
	testq	%rsi, %rsi
	je	.L300
	.p2align 4,,10
	.p2align 3
.L301:
	movq	16(%rsi), %rax
	testq	%rax, %rax
	jne	.L304
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L306:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L300
.L304:
	cmpl	112(%rax), %edx
	jle	.L306
	cmpl	116(%rax), %edx
	jge	.L306
	cmpq	%rax, %rsi
	je	.L300
	movq	%rax, %rsi
	jmp	.L301
.L300:
	movq	%rsi, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 72(%rdi)
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20269:
	.size	_ZN2v88internal13ScopeIterator18RetrieveScopeChainEPNS0_16DeclarationScopeE, .-_ZN2v88internal13ScopeIterator18RetrieveScopeChainEPNS0_16DeclarationScopeE
	.section	.text._ZNK2v88internal13ScopeIterator18VisitContextLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS3_INS0_9ScopeInfoEEENS3_INS0_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13ScopeIterator18VisitContextLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS3_INS0_9ScopeInfoEEENS3_INS0_7ContextEEE
	.type	_ZNK2v88internal13ScopeIterator18VisitContextLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS3_INS0_9ScopeInfoEEENS3_INS0_7ContextEEE, @function
_ZNK2v88internal13ScopeIterator18VisitContextLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS3_INS0_9ScopeInfoEEENS3_INS0_7ContextEEE:
.LFB20272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movl	11(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L331
	movq	%rdi, %r12
	xorl	%ebx, %ebx
	leaq	-64(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L337:
	movq	31(%rax), %rdx
	sarq	$32, %rdx
	cmpl	%edx, %ebx
	jge	.L331
	movq	(%r12), %r14
	movl	%ebx, %esi
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo16ContextLocalNameEi@PLT
	movq	41112(%r14), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L333
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L334:
	movq	(%r15), %rdi
	call	_ZN2v88internal9ScopeInfo19VariableIsSyntheticENS0_6StringE@PLT
	testb	%al, %al
	jne	.L341
	movq	-104(%rbp), %rdx
	movq	(%r12), %rcx
	leal	48(,%rbx,8), %eax
	cltq
	movq	(%rdx), %rsi
	movq	-1(%rax,%rsi), %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L338
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L339:
	movq	(%r12), %rcx
	cmpq	%rsi, 96(%rcx)
	je	.L341
	movq	%rax, -64(%rbp)
	movq	-96(%rbp), %rax
	movq	%r15, -72(%rbp)
	cmpq	$0, 16(%rax)
	je	.L350
	leaq	-72(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rax, %rdi
	call	*24(%rax)
	testb	%al, %al
	jne	.L330
.L341:
	movq	-88(%rbp), %rax
	addl	$1, %ebx
	movq	(%rax), %rax
	movl	11(%rax), %edx
	testl	%edx, %edx
	jg	.L337
.L331:
	xorl	%eax, %eax
.L330:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L351
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	.cfi_restore_state
	movq	41088(%r14), %r15
	cmpq	41096(%r14), %r15
	je	.L352
.L335:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r15)
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L338:
	movq	41088(%rcx), %rax
	cmpq	41096(%rcx), %rax
	je	.L353
.L340:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rcx)
	movq	%rsi, (%rax)
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L352:
	movq	%r14, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L353:
	movq	%rcx, %rdi
	movq	%rsi, -120(%rbp)
	movq	%rcx, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %rcx
	jmp	.L340
.L350:
	call	_ZSt25__throw_bad_function_callv@PLT
.L351:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20272:
	.size	_ZNK2v88internal13ScopeIterator18VisitContextLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS3_INS0_9ScopeInfoEEENS3_INS0_7ContextEEE, .-_ZNK2v88internal13ScopeIterator18VisitContextLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS3_INS0_9ScopeInfoEEENS3_INS0_7ContextEEE
	.section	.text._ZNK2v88internal13ScopeIterator16VisitModuleScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13ScopeIterator16VisitModuleScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEE
	.type	_ZNK2v88internal13ScopeIterator16VisitModuleScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEE, @function
_ZNK2v88internal13ScopeIterator16VisitModuleScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEE:
.LFB20271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -88(%rbp)
	movq	(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context10scope_infoEv@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L355
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L356:
	movq	40(%r12), %rcx
	movq	-88(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZNK2v88internal13ScopeIterator18VisitContextLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS3_INS0_9ScopeInfoEEENS3_INS0_7ContextEEE
	testb	%al, %al
	je	.L375
.L354:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L376
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L375:
	.cfi_restore_state
	movq	(%r14), %rax
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo24ModuleVariableCountIndexEv@PLT
	movq	(%r14), %rdx
	movq	%r13, %rdi
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rbx
	movq	40(%r12), %rax
	movq	(%r12), %r15
	movq	(%rax), %rax
	sarq	$32, %rbx
	movl	%ebx, -108(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context6moduleEv@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L359
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -120(%rbp)
.L360:
	testq	%rbx, %rbx
	jle	.L354
	leaq	-76(%rbp), %rax
	xorl	%ebx, %ebx
	leaq	-72(%rbp), %r15
	movq	%rax, -104(%rbp)
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L378:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L365:
	movl	-76(%rbp), %edx
	movq	-120(%rbp), %rsi
	movq	%rcx, -96(%rbp)
	movq	(%r12), %rdi
	call	_ZN2v88internal16SourceTextModule12LoadVariableEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	(%r12), %rdx
	movq	(%rax), %rcx
	cmpq	%rcx, 96(%rdx)
	je	.L363
	movq	%rax, -64(%rbp)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rcx
	cmpq	$0, 16(%rax)
	movq	%rcx, -72(%rbp)
	je	.L377
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	*24(%rax)
	testb	%al, %al
	jne	.L354
.L363:
	addl	$1, %ebx
	cmpl	-108(%rbp), %ebx
	je	.L354
.L369:
	subq	$8, %rsp
	movq	-104(%rbp), %rcx
	movq	%r15, %rdx
	xorl	%r9d, %r9d
	movq	$0, -72(%rbp)
	movq	(%r14), %rax
	xorl	%r8d, %r8d
	movl	%ebx, %esi
	pushq	$0
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal9ScopeInfo14ModuleVariableEiPNS0_6StringEPiPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE@PLT
	movq	-72(%rbp), %rdi
	call	_ZN2v88internal9ScopeInfo19VariableIsSyntheticENS0_6StringE@PLT
	popq	%rdx
	popq	%rcx
	testb	%al, %al
	jne	.L363
	movq	(%r12), %rdx
	movq	-72(%rbp), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	jne	.L378
	movq	41088(%rdx), %rcx
	cmpq	41096(%rdx), %rcx
	je	.L379
.L366:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rcx)
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L355:
	movq	41088(%rbx), %r14
	cmpq	%r14, 41096(%rbx)
	je	.L380
.L357:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r14)
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L379:
	movq	%rdx, %rdi
	movq	%rsi, -128(%rbp)
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	movq	-96(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L359:
	movq	41088(%r15), %rax
	movq	%rax, -120(%rbp)
	cmpq	41096(%r15), %rax
	je	.L381
.L361:
	movq	-120(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rcx)
	jmp	.L360
.L380:
	movq	%rbx, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L357
.L381:
	movq	%r15, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, -120(%rbp)
	jmp	.L361
.L377:
	call	_ZSt25__throw_bad_function_callv@PLT
.L376:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20271:
	.size	_ZNK2v88internal13ScopeIterator16VisitModuleScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEE, .-_ZNK2v88internal13ScopeIterator16VisitModuleScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEE
	.section	.text._ZNK2v88internal13ScopeIterator16VisitScriptScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13ScopeIterator16VisitScriptScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEE
	.type	_ZNK2v88internal13ScopeIterator16VisitScriptScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEE, @function
_ZNK2v88internal13ScopeIterator16VisitScriptScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEE:
.LFB20270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -72(%rbp)
	movq	(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L383
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L384:
	movq	(%rax), %rax
	movq	(%r12), %rbx
	movq	23(%rax), %rax
	movq	1135(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L386
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L387:
	xorl	%ebx, %ebx
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L400:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L391:
	movq	(%r12), %r15
	movq	-80(%rbp), %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal7Context10scope_infoEv@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L393
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L394:
	movq	-72(%rbp), %rsi
	movq	%r14, %rcx
	movq	%r12, %rdi
	addq	$1, %rbx
	call	_ZNK2v88internal13ScopeIterator18VisitContextLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS3_INS0_9ScopeInfoEEENS3_INS0_7ContextEEE
	testb	%al, %al
	jne	.L382
	movq	0(%r13), %rsi
.L397:
	movq	15(%rsi), %rax
	leal	1(%rbx), %edx
	sarq	$32, %rax
	cmpl	%eax, %edx
	jge	.L382
	movq	(%r12), %r15
	movq	0(%r13), %rax
	movq	31(%rax,%rbx,8), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	jne	.L400
	movq	41088(%r15), %r14
	cmpq	41096(%r15), %r14
	je	.L401
.L392:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L393:
	movq	41088(%r15), %rdx
	cmpq	41096(%r15), %rdx
	je	.L402
.L395:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rdx)
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L382:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L403
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L401:
	.cfi_restore_state
	movq	%r15, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L402:
	movq	%r15, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L383:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L404
.L385:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L386:
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L405
.L388:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	jmp	.L387
.L404:
	movq	%rbx, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L385
.L405:
	movq	%rbx, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L388
.L403:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20270:
	.size	_ZNK2v88internal13ScopeIterator16VisitScriptScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEE, .-_ZNK2v88internal13ScopeIterator16VisitScriptScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEE
	.section	.text.unlikely._ZNK2v88internal13ScopeIterator11VisitLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE,"ax",@progbits
	.align 2
.LCOLDB5:
	.section	.text._ZNK2v88internal13ScopeIterator11VisitLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE,"ax",@progbits
.LHOTB5:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13ScopeIterator11VisitLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE
	.type	_ZNK2v88internal13ScopeIterator11VisitLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE, @function
_ZNK2v88internal13ScopeIterator11VisitLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE:
.LFB20273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movl	%edx, -124(%rbp)
	movq	80(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jne	.L407
	testb	$1, 130(%rdi)
	jne	.L475
.L407:
	cmpb	$2, 128(%rdi)
	je	.L476
.L421:
	movq	64(%rdi), %rax
	leaq	56(%rdi), %r15
	leaq	.L430(%rip), %r12
	movq	%rax, -120(%rbp)
	cmpq	%r15, %rax
	je	.L457
	.p2align 4,,10
	.p2align 3
.L425:
	movq	(%r15), %r14
	movq	8(%r14), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal9ScopeInfo19VariableIsSyntheticENS0_6StringE@PLT
	testb	%al, %al
	jne	.L427
	movzwl	40(%r14), %eax
	movl	32(%r14), %esi
	sarl	$7, %eax
	andl	$7, %eax
	cmpb	$5, %al
	ja	.L459
	movzbl	%al, %eax
	movslq	(%r12,%rax,4), %rax
	addq	%r12, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal13ScopeIterator11VisitLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE,"a",@progbits
	.align 4
	.align 4
.L430:
	.long	.L427-.L430
	.long	.L434-.L430
	.long	.L433-.L430
	.long	.L432-.L430
	.long	.L431-.L430
	.long	.L429-.L430
	.section	.text._ZNK2v88internal13ScopeIterator11VisitLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE
	.p2align 4,,10
	.p2align 3
.L432:
	movl	-124(%rbp), %edx
	testl	%edx, %edx
	je	.L427
	movq	40(%r13), %rdi
	movq	0(%r13), %rdx
	leal	16(,%rsi,8), %eax
	cltq
	movq	(%rdi), %rsi
	movq	-1(%rax,%rsi), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L449
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L450:
	movq	0(%r13), %rdx
	cmpq	%rsi, 96(%rdx)
	je	.L427
	.p2align 4,,10
	.p2align 3
.L428:
	movq	8(%r14), %rdx
	cmpq	$0, 16(%rbx)
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	movq	%rdx, -72(%rbp)
	je	.L423
	leaq	-64(%rbp), %rdx
	leaq	-72(%rbp), %rsi
	movq	%rbx, %rdi
	call	*24(%rbx)
	testb	%al, %al
	jne	.L424
.L427:
	movq	(%r15), %rcx
	leaq	24(%rcx), %r15
	cmpq	%r15, -120(%rbp)
	jne	.L425
.L457:
	xorl	%eax, %eax
.L406:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L477
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	.cfi_restore_state
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L478
	call	_ZN2v88internal14FrameInspector13GetExpressionEi@PLT
	movq	0(%r13), %rdx
	movq	(%rax), %rsi
	cmpq	%rsi, 328(%rdx)
	je	.L479
	cmpq	96(%rdx), %rsi
	jne	.L428
	movq	(%r15), %rcx
	leaq	24(%rcx), %r15
	cmpq	%r15, -120(%rbp)
	jne	.L425
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L434:
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L480
	call	_ZN2v88internal14FrameInspector12GetParameterEi@PLT
	movq	0(%r13), %rdx
	movq	(%rax), %rcx
	leaq	88(%rdx), %rsi
	cmpq	%rcx, 328(%rdx)
	cmove	%rsi, %rax
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L429:
	movl	-124(%rbp), %eax
	testl	%eax, %eax
	je	.L427
	movq	40(%r13), %rax
	movq	0(%r13), %rdx
	leaq	-64(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rdx, -136(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context6moduleEv@PLT
	movq	-136(%rbp), %rdx
	movq	%rax, %r9
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L453
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L454:
	movl	32(%r14), %edx
	movq	0(%r13), %rdi
	call	_ZN2v88internal16SourceTextModule12LoadVariableEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	0(%r13), %rdx
	movq	(%rax), %rcx
	cmpq	%rcx, 96(%rdx)
	jne	.L428
	movq	(%r15), %rcx
	leaq	24(%rcx), %r15
	cmpq	%r15, -120(%rbp)
	jne	.L425
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L480:
	movq	24(%r13), %rax
	movq	0(%r13), %rdx
	movq	(%rax), %rdi
	leal	16(,%rsi,8), %eax
	cltq
	movq	71(%rdi), %rsi
	movq	-1(%rax,%rsi), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L436
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L478:
	movq	24(%r13), %rax
	movq	(%rax), %rax
	movq	71(%rax), %r9
	movq	32(%r13), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rdx
	movq	15(%rdx), %rax
	testb	$1, %al
	jne	.L439
.L441:
	andq	$-262144, %rdx
	movq	%r9, -144(%rbp)
	movq	24(%rdx), %rdi
	movl	%esi, -136(%rbp)
	subq	$37592, %rdi
	call	_ZN2v88internal9ScopeInfo5EmptyEPNS0_7IsolateE@PLT
	movl	-136(%rbp), %esi
	movq	-144(%rbp), %r9
.L440:
	movl	11(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L442
	movq	23(%rax), %rax
	sarq	$32, %rax
	addl	%eax, %esi
.L442:
	movq	0(%r13), %rdx
	leal	16(,%rsi,8), %eax
	cltq
	movq	-1(%r9,%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L443
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L444:
	movq	0(%r13), %rdx
	cmpq	%rsi, 96(%rdx)
	jne	.L428
.L447:
	leaq	88(%rdx), %rax
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L479:
	movq	80(%r13), %rdi
	testb	$1, 130(%rdi)
	je	.L447
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	cmpq	200(%rax), %r14
	je	.L427
	movq	0(%r13), %rdx
	leaq	88(%rdx), %rax
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L449:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L481
.L451:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L453:
	movq	41088(%rdx), %rsi
	cmpq	41096(%rdx), %rsi
	je	.L482
.L455:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdx)
	movq	%r9, (%rsi)
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L443:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L483
.L445:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L436:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L484
.L437:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L424:
	movl	$1, %eax
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L439:
	movq	-1(%rax), %rdi
	cmpw	$136, 11(%rdi)
	jne	.L441
	jmp	.L440
.L475:
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movq	80(%r13), %rdi
	testb	$16, 132(%rax)
	je	.L407
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movq	176(%rax), %rdx
	movzwl	40(%rdx), %eax
	sarl	$7, %eax
	andl	$7, %eax
	cmpb	$3, %al
	je	.L485
	movq	16(%r13), %rax
	movq	0(%r13), %r12
	testq	%rax, %rax
	je	.L486
	movq	48(%rax), %rax
	movq	(%rax), %r14
.L411:
	cmpq	%r14, 328(%r12)
	je	.L416
	cmpq	%r14, 96(%r12)
	je	.L416
.L417:
	addq	$3424, %r12
	cmpq	$0, 16(%rbx)
	movq	%rax, -96(%rbp)
	movq	%r12, -104(%rbp)
	je	.L423
	leaq	-96(%rbp), %rdx
	leaq	-104(%rbp), %rsi
	movq	%rbx, %rdi
	call	*24(%rbx)
	testb	%al, %al
	jne	.L424
	movq	80(%r13), %rdi
	jmp	.L407
.L476:
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movq	184(%rax), %rax
	testq	%rax, %rax
	je	.L473
	movq	16(%r13), %rdx
	movq	8(%rax), %rax
	cmpq	$0, 16(%rbx)
	movq	(%rax), %rax
	movq	56(%rdx), %rdx
	movq	%rax, -88(%rbp)
	movq	%rdx, -80(%rbp)
	je	.L423
	leaq	-80(%rbp), %rdx
	leaq	-88(%rbp), %rsi
	movq	%rbx, %rdi
	call	*24(%rbx)
	testb	%al, %al
	jne	.L424
.L473:
	movq	80(%r13), %rdi
	jmp	.L421
.L482:
	movq	%rdx, %rdi
	movq	%rdx, -136(%rbp)
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L455
.L481:
	movq	%rdx, %rdi
	movq	%rsi, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdx
	jmp	.L451
.L484:
	movq	%rdx, %rdi
	movq	%rsi, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdx
	jmp	.L437
.L483:
	movq	%rdx, %rdi
	movq	%rsi, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdx
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L431:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L485:
	movl	32(%rdx), %eax
	movq	40(%r13), %rcx
	movq	0(%r13), %r12
	movq	(%rcx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L410
.L472:
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	0(%r13), %r12
	movq	(%rax), %r14
	jmp	.L411
.L416:
	leaq	88(%r12), %rax
	jmp	.L417
.L486:
	movq	24(%r13), %rax
	movq	41112(%r12), %rdi
	movq	(%rax), %rax
	movq	39(%rax), %r14
	testq	%rdi, %rdi
	jne	.L472
	movq	41088(%r12), %rax
	cmpq	%rax, 41096(%r12)
	je	.L474
.L415:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%r14, (%rax)
	movq	0(%r13), %r12
	jmp	.L411
.L410:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	jne	.L415
.L474:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L415
.L423:
	call	_ZSt25__throw_bad_function_callv@PLT
.L477:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal13ScopeIterator11VisitLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE
	.cfi_startproc
	.type	_ZNK2v88internal13ScopeIterator11VisitLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE.cold, @function
_ZNK2v88internal13ScopeIterator11VisitLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE.cold:
.LFSB20273:
.L459:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	xorl	%eax, %eax
	jmp	.L428
	.cfi_endproc
.LFE20273:
	.section	.text._ZNK2v88internal13ScopeIterator11VisitLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE
	.size	_ZNK2v88internal13ScopeIterator11VisitLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE, .-_ZNK2v88internal13ScopeIterator11VisitLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE
	.section	.text.unlikely._ZNK2v88internal13ScopeIterator11VisitLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE
	.size	_ZNK2v88internal13ScopeIterator11VisitLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE.cold, .-_ZNK2v88internal13ScopeIterator11VisitLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE.cold
.LCOLDE5:
	.section	.text._ZNK2v88internal13ScopeIterator11VisitLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE
.LHOTE5:
	.section	.text._ZN2v88internal13ScopeIterator20WithContextExtensionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScopeIterator20WithContextExtensionEv
	.type	_ZN2v88internal13ScopeIterator20WithContextExtensionEv, @function
_ZN2v88internal13ScopeIterator20WithContextExtensionEv:
.LFB20274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal7Context18extension_receiverEv@PLT
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	je	.L495
	movq	40(%rbx), %rax
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal7Context18extension_receiverEv@PLT
	movq	41112(%r13), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L490
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L489:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L496
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L490:
	.cfi_restore_state
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L497
.L492:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L495:
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE@PLT
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L497:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L492
.L496:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20274:
	.size	_ZN2v88internal13ScopeIterator20WithContextExtensionEv, .-_ZN2v88internal13ScopeIterator20WithContextExtensionEv
	.section	.rodata._ZNK2v88internal13ScopeIterator15VisitLocalScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"(location_) != nullptr"
	.section	.text._ZNK2v88internal13ScopeIterator15VisitLocalScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13ScopeIterator15VisitLocalScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE
	.type	_ZNK2v88internal13ScopeIterator15VisitLocalScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE, @function
_ZNK2v88internal13ScopeIterator15VisitLocalScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE:
.LFB20275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 32(%rdi)
	je	.L499
	call	_ZNK2v88internal13ScopeIterator11VisitLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE
	testb	%al, %al
	jne	.L498
	testl	%r12d, %r12d
	je	.L548
.L502:
	cmpl	$1, %r12d
	jne	.L498
	cmpq	$0, 32(%r15)
	je	.L517
	movq	80(%r15), %rax
	movl	124(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L498
.L517:
	movq	40(%r15), %rax
	leaq	-144(%rbp), %r13
	movq	(%rax), %rax
	movq	%rax, -152(%rbp)
	leaq	-152(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal7Context10scope_infoEv@PLT
	movq	%r13, %rdi
	movq	%rax, -144(%rbp)
	call	_ZNK2v88internal9ScopeInfo23SloppyEvalCanExtendVarsEv@PLT
	testb	%al, %al
	je	.L498
	movq	40(%r15), %rax
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal7Context16extension_objectEv@PLT
	testq	%rax, %rax
	jne	.L549
	.p2align 4,,10
	.p2align 3
.L498:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L550
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L499:
	.cfi_restore_state
	movq	40(%rdi), %rax
	movq	(%rdi), %r13
	leaq	-144(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal7Context10scope_infoEv@PLT
	movq	41112(%r13), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L513
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L514:
	movq	40(%r15), %rcx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal13ScopeIterator18VisitContextLocalsERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS3_INS0_9ScopeInfoEEENS3_INS0_7ContextEEE
	testb	%al, %al
	je	.L502
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L513:
	movq	41088(%r13), %rdx
	cmpq	%rdx, 41096(%r13)
	je	.L551
.L515:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rdx)
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L548:
	movq	%r15, %rdi
	call	_ZNK2v88internal13ScopeIterator4TypeEv
	cmpl	$1, %eax
	jne	.L498
	movq	64(%r15), %rdi
	testb	$16, 132(%rdi)
	je	.L504
.L507:
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.L498
	movq	64(%r15), %rax
	cmpb	$2, 128(%rax)
	je	.L552
.L509:
	movq	200(%rax), %rax
	testq	%rax, %rax
	je	.L510
	movl	32(%rax), %esi
	call	_ZN2v88internal14FrameInspector13GetExpressionEi@PLT
	movq	(%r15), %rdx
	movq	328(%rdx), %rcx
	cmpq	%rcx, (%rax)
	jne	.L498
	movq	16(%r15), %rdi
.L510:
	call	_ZN2v88internal14FrameInspector16javascript_frameEv@PLT
	movq	%rax, %rdi
	movq	16(%r15), %rax
	movl	8(%rax), %esi
	call	_ZN2v88internal9Accessors20FunctionGetArgumentsEPNS0_15JavaScriptFrameEi@PLT
	movq	%rax, %r8
	movq	(%r15), %rax
	movq	%r8, -160(%rbp)
	addq	$2040, %rax
	cmpq	$0, 16(%rbx)
	movq	%rax, -168(%rbp)
	je	.L511
	leaq	-160(%rbp), %rdx
	leaq	-168(%rbp), %rsi
	movq	%rbx, %rdi
	call	*24(%rbx)
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L549:
	movq	40(%r15), %rax
	movq	(%r15), %r12
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal7Context16extension_objectEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L518
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L519:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$18, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb@PLT
	movq	%rax, -216(%rbp)
	testq	%rax, %rax
	je	.L553
	movq	(%rax), %rax
	movl	11(%rax), %edx
	testl	%edx, %edx
	jle	.L498
	xorl	%r12d, %r12d
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L555:
	movq	-120(%rbp), %rax
	addq	$88, %rax
.L528:
	cmpq	$0, 16(%rbx)
	movq	%rdx, -152(%rbp)
	movq	%rax, -144(%rbp)
	je	.L511
	movq	-208(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	*24(%rbx)
	testb	%al, %al
	jne	.L498
	movq	-216(%rbp), %rax
	addq	$1, %r12
	movq	(%rax), %rax
	cmpl	%r12d, 11(%rax)
	jle	.L498
.L529:
	movq	(%r15), %rcx
	movq	15(%rax,%r12,8), %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L522
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L523:
	movq	(%r14), %rax
	movq	(%rdx), %rcx
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rcx), %rsi
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rsi)
	jne	.L525
	xorl	%eax, %eax
	testb	$1, 11(%rcx)
	sete	%al
	addl	%eax, %eax
.L525:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	%rdi, -120(%rbp)
	movq	(%rdx), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %ecx
	movq	%rdx, %rax
	andl	$-32, %ecx
	cmpl	$32, %ecx
	je	.L554
.L526:
	movq	%r13, %rdi
	movq	%rdx, -200(%rbp)
	movq	%rax, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r14, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r14, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	movq	-200(%rbp), %rdx
	je	.L555
	movq	%r13, %rdi
	movq	%rdx, -200(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-200(%rbp), %rdx
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L504:
	call	_ZNK2v88internal5Scope16HasThisReferenceEv@PLT
	testb	%al, %al
	jne	.L507
	movq	(%r15), %rax
	leaq	3424(%rax), %rdx
	addq	$88, %rax
	cmpq	$0, 16(%rbx)
	movq	%rdx, -184(%rbp)
	movq	%rax, -176(%rbp)
	je	.L511
	leaq	-176(%rbp), %rdx
	leaq	-184(%rbp), %rsi
	movq	%rbx, %rdi
	call	*24(%rbx)
	testb	%al, %al
	je	.L507
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L522:
	movq	41088(%rcx), %rdx
	cmpq	41096(%rcx), %rdx
	je	.L556
.L524:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%rdx)
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L554:
	movq	%rdx, %rsi
	movq	%rdx, -200(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-200(%rbp), %rdx
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L556:
	movq	%rcx, %rdi
	movq	%rsi, -224(%rbp)
	movq	%rcx, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %rsi
	movq	-200(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L551:
	movq	%r13, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L518:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L557
.L520:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L552:
	movzbl	133(%rax), %ecx
	leal	-8(%rcx), %edx
	cmpb	$1, %dl
	ja	.L509
	jmp	.L498
.L553:
	leaq	.LC6(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L557:
	movq	%r12, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L520
.L550:
	call	__stack_chk_fail@PLT
.L511:
	call	_ZSt25__throw_bad_function_callv@PLT
	.cfi_endproc
.LFE20275:
	.size	_ZNK2v88internal13ScopeIterator15VisitLocalScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE, .-_ZNK2v88internal13ScopeIterator15VisitLocalScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE
	.section	.text._ZNK2v88internal13ScopeIterator10VisitScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13ScopeIterator10VisitScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE
	.type	_ZNK2v88internal13ScopeIterator10VisitScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE, @function
_ZNK2v88internal13ScopeIterator10VisitScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE:
.LFB20265:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	testq	%r8, %r8
	je	.L559
	movq	80(%rdi), %rax
	movzbl	128(%rax), %eax
	cmpb	$7, %al
	ja	.L560
	leaq	CSWTCH.1074(%rip), %rcx
	cmpl	$8, (%rcx,%rax,4)
	ja	.L558
	movl	(%rcx,%rax,4), %eax
	leaq	.L563(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal13ScopeIterator10VisitScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE,"a",@progbits
	.align 4
	.align 4
.L563:
	.long	.L560-.L563
	.long	.L577-.L563
	.long	.L560-.L563
	.long	.L577-.L563
	.long	.L577-.L563
	.long	.L577-.L563
	.long	.L565-.L563
	.long	.L577-.L563
	.long	.L562-.L563
	.section	.text._ZNK2v88internal13ScopeIterator10VisitScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE
	.p2align 4,,10
	.p2align 3
.L562:
	testq	%r8, %r8
	je	.L572
.L577:
	jmp	_ZNK2v88internal13ScopeIterator15VisitLocalScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE
	.p2align 4,,10
	.p2align 3
.L578:
	cmpb	$0, 88(%rdi)
	jne	.L560
.L565:
	jmp	_ZNK2v88internal13ScopeIterator16VisitScriptScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEE
.L558:
	ret
	.p2align 4,,10
	.p2align 3
.L572:
	jmp	_ZNK2v88internal13ScopeIterator16VisitModuleScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEE
	.p2align 4,,10
	.p2align 3
.L559:
	movq	40(%rdi), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rcx
	cmpw	$145, 11(%rcx)
	je	.L578
	movq	-1(%rax), %rcx
	cmpw	$143, 11(%rcx)
	je	.L577
	movq	-1(%rax), %rcx
	cmpw	$142, 11(%rcx)
	je	.L577
	movq	-1(%rax), %rcx
	cmpw	$141, 11(%rcx)
	je	.L577
	movq	-1(%rax), %rcx
	cmpw	$140, 11(%rcx)
	je	.L577
	movq	-1(%rax), %rcx
	cmpw	$139, 11(%rcx)
	je	.L577
	movq	-1(%rax), %rdx
	cmpw	$144, 11(%rdx)
	je	.L572
	movq	-1(%rax), %rax
	cmpw	$146, 11(%rax)
	je	.L565
.L560:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20265:
	.size	_ZNK2v88internal13ScopeIterator10VisitScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE, .-_ZNK2v88internal13ScopeIterator10VisitScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE
	.section	.text._ZNK2v88internal13ScopeIterator14DeclaresLocalsENS1_4ModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13ScopeIterator14DeclaresLocalsENS1_4ModeE
	.type	_ZNK2v88internal13ScopeIterator14DeclaresLocalsENS1_4ModeE, @function
_ZNK2v88internal13ScopeIterator14DeclaresLocalsENS1_4ModeE:
.LFB20255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$72, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 32(%rdi)
	je	.L580
	movq	80(%rdi), %rax
	movzbl	128(%rax), %eax
	cmpb	$7, %al
	ja	.L581
	leaq	CSWTCH.1074(%rip), %rcx
	testl	$-3, (%rcx,%rax,4)
	jne	.L582
.L585:
	cmpl	$1, %esi
	sete	%al
.L579:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L592
	addq	$72, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L593:
	.cfi_restore_state
	cmpb	$0, 88(%rdi)
	jne	.L585
	.p2align 4,,10
	.p2align 3
.L582:
	leaq	-65(%rbp), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZNK2v88internal13ScopeIterator14DeclaresLocalsENS3_4ModeEEUlNS2_6HandleINS2_6StringEEENS5_INS2_6ObjectEEEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation(%rip), %rdx
	movb	$0, -65(%rbp)
	movq	%rax, -64(%rbp)
	leaq	_ZNSt17_Function_handlerIFbN2v88internal6HandleINS1_6StringEEENS2_INS1_6ObjectEEEEZNKS1_13ScopeIterator14DeclaresLocalsENS8_4ModeEEUlS4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_OS6_(%rip), %rax
	movq	%rdx, %xmm0
	leaq	-64(%rbp), %r12
	movq	%rax, %xmm1
	movl	%esi, %edx
	movq	%r12, %rsi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZNK2v88internal13ScopeIterator10VisitScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L586
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L586:
	movzbl	-65(%rbp), %eax
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L580:
	movq	40(%rdi), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rcx
	cmpw	$145, 11(%rcx)
	je	.L593
	movq	-1(%rax), %rcx
	cmpw	$143, 11(%rcx)
	je	.L582
	movq	-1(%rax), %rcx
	cmpw	$142, 11(%rcx)
	je	.L582
	movq	-1(%rax), %rcx
	cmpw	$141, 11(%rcx)
	je	.L582
	movq	-1(%rax), %rcx
	cmpw	$140, 11(%rcx)
	je	.L582
	movq	-1(%rax), %rcx
	cmpw	$139, 11(%rcx)
	je	.L582
	movq	-1(%rax), %rcx
	cmpw	$144, 11(%rcx)
	je	.L582
	movq	-1(%rax), %rax
	cmpw	$146, 11(%rax)
	jne	.L585
	jmp	.L582
.L592:
	call	__stack_chk_fail@PLT
.L581:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20255:
	.size	_ZNK2v88internal13ScopeIterator14DeclaresLocalsENS1_4ModeE, .-_ZNK2v88internal13ScopeIterator14DeclaresLocalsENS1_4ModeE
	.section	.text._ZN2v88internal13ScopeIterator11ScopeObjectENS1_4ModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScopeIterator11ScopeObjectENS1_4ModeE
	.type	_ZN2v88internal13ScopeIterator11ScopeObjectENS1_4ModeE, @function
_ZN2v88internal13ScopeIterator11ScopeObjectENS1_4ModeE:
.LFB20263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 32(%rdi)
	je	.L595
	movq	80(%rdi), %rax
	movzbl	128(%rax), %eax
	cmpb	$7, %al
	ja	.L596
	leaq	CSWTCH.1074(%rip), %rdx
	movl	(%rdx,%rax,4), %eax
	testl	%eax, %eax
	je	.L618
	cmpl	$2, %eax
	je	.L605
.L600:
	movq	(%r12), %rdi
	xorl	%esi, %esi
	leaq	-80(%rbp), %r14
	call	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE@PLT
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal13ScopeIterator11ScopeObjectENS3_4ModeEEUlNS2_6HandleINS2_6StringEEENS5_INS2_6ObjectEEEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation(%rip), %rcx
	movq	%rax, %rbx
	leaq	_ZNSt17_Function_handlerIFbN2v88internal6HandleINS1_6StringEEENS2_INS1_6ObjectEEEEZNS1_13ScopeIterator11ScopeObjectENS8_4ModeEEUlS4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_OS6_(%rip), %rax
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movq	%r12, -80(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZNK2v88internal13ScopeIterator10VisitScopeERKSt8functionIFbNS0_6HandleINS0_6StringEEENS3_INS0_6ObjectEEEEENS1_4ModeE
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L606
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L606:
	movq	%rbx, %rax
.L601:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L619
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L595:
	.cfi_restore_state
	movq	40(%rdi), %rax
	movq	(%rax), %rdx
	movq	-1(%rdx), %rcx
	cmpw	$145, 11(%rcx)
	je	.L620
	movq	-1(%rdx), %rax
	cmpw	$143, 11(%rax)
	je	.L600
	movq	-1(%rdx), %rax
	cmpw	$142, 11(%rax)
	je	.L600
	movq	-1(%rdx), %rax
	cmpw	$141, 11(%rax)
	je	.L600
	movq	-1(%rdx), %rax
	cmpw	$140, 11(%rax)
	je	.L600
	movq	-1(%rdx), %rax
	cmpw	$139, 11(%rax)
	je	.L600
	movq	-1(%rdx), %rax
	cmpw	$144, 11(%rax)
	je	.L600
	movq	-1(%rdx), %rax
	cmpw	$146, 11(%rax)
	je	.L600
	.p2align 4,,10
	.p2align 3
.L605:
	movq	%r12, %rdi
	call	_ZN2v88internal13ScopeIterator20WithContextExtensionEv
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L618:
	movq	40(%rdi), %rax
.L597:
	movq	(%r12), %rbx
	movq	(%rax), %rax
	leaq	-88(%rbp), %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal7Context12global_proxyEv@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L602
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L602:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L621
.L604:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L620:
	cmpb	$0, 88(%rdi)
	jne	.L597
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L621:
	movq	%rbx, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	jmp	.L604
.L619:
	call	__stack_chk_fail@PLT
.L596:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20263:
	.size	_ZN2v88internal13ScopeIterator11ScopeObjectENS1_4ModeE, .-_ZN2v88internal13ScopeIterator11ScopeObjectENS1_4ModeE
	.section	.text._ZN2v88internal13ScopeIterator23MaterializeScopeDetailsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScopeIterator23MaterializeScopeDetailsEv
	.type	_ZN2v88internal13ScopeIterator23MaterializeScopeDetailsEv, @function
_ZN2v88internal13ScopeIterator23MaterializeScopeDetailsEv:
.LFB20251:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rbx, %rdi
	movq	(%rax), %r13
	movq	%rax, %r12
	call	_ZNK2v88internal13ScopeIterator4TypeEv
	movl	$1, %esi
	movq	%rbx, %rdi
	salq	$32, %rax
	movq	%rax, 15(%r13)
	call	_ZN2v88internal13ScopeIterator11ScopeObjectENS1_4ModeE
	movq	(%r12), %r14
	movq	(%rax), %r13
	movq	%r13, 23(%r14)
	testb	$1, %r13b
	je	.L640
	movq	%r13, %r15
	leaq	23(%r14), %rsi
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L666
	testb	$24, %al
	je	.L640
.L671:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L667
	.p2align 4,,10
	.p2align 3
.L640:
	movq	%rbx, %rdi
	call	_ZNK2v88internal13ScopeIterator4TypeEv
	testl	%eax, %eax
	je	.L630
	movq	%rbx, %rdi
	call	_ZNK2v88internal13ScopeIterator4TypeEv
	cmpl	$6, %eax
	je	.L630
	cmpq	$0, 32(%rbx)
	je	.L629
	movq	80(%rbx), %rax
	movl	124(%rax), %eax
	testl	%eax, %eax
	jle	.L630
.L629:
	movq	%rbx, %rdi
	call	_ZNK2v88internal13ScopeIterator20GetFunctionDebugNameEv
	movq	(%r12), %r14
	movq	(%rax), %r13
	leaq	31(%r14), %rsi
	movq	%r13, 31(%r14)
	testb	$1, %r13b
	je	.L639
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	je	.L632
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
.L632:
	testb	$24, %al
	je	.L639
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L668
	.p2align 4,,10
	.p2align 3
.L639:
	movq	%rbx, %rdi
	movq	(%r12), %r13
	call	_ZN2v88internal13ScopeIterator14start_positionEv
	movq	%rbx, %rdi
	salq	$32, %rax
	movq	%rax, 39(%r13)
	movq	(%r12), %r13
	call	_ZN2v88internal13ScopeIterator12end_positionEv
	salq	$32, %rax
	movq	%rax, 47(%r13)
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.L630
	movq	(%r12), %r14
	movq	(%rax), %r13
	movq	%r13, 55(%r14)
	leaq	55(%r14), %rsi
	testb	$1, %r13b
	je	.L630
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L669
.L636:
	testb	$24, %al
	je	.L630
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L670
.L630:
	movq	(%r12), %rax
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	xorl	%r8d, %r8d
	movl	$3, %edx
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L666:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L671
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L667:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L669:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L668:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L670:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L630
	.cfi_endproc
.LFE20251:
	.size	_ZN2v88internal13ScopeIterator23MaterializeScopeDetailsEv, .-_ZN2v88internal13ScopeIterator23MaterializeScopeDetailsEv
	.section	.text._ZN2v88internal13ScopeIterator21SetLocalVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScopeIterator21SetLocalVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE
	.type	_ZN2v88internal13ScopeIterator21SetLocalVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE, @function
_ZN2v88internal13ScopeIterator21SetLocalVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE:
.LFB20276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	80(%rdi), %rax
	movq	64(%rax), %r13
	leaq	56(%rax), %rbx
	cmpq	%rbx, %r13
	jne	.L715
	jmp	.L721
	.p2align 4,,10
	.p2align 3
.L677:
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L681
	movq	(%r15), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	je	.L680
.L681:
	movq	%r15, %rdx
	call	_ZN2v88internal6String10SlowEqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testb	%al, %al
	jne	.L676
	movq	(%rbx), %r12
.L680:
	leaq	24(%r12), %rbx
	cmpq	%rbx, %r13
	je	.L721
.L715:
	movq	(%rbx), %r12
	movq	(%r14), %rdi
	movq	8(%r12), %rax
	movq	(%rax), %rsi
	cmpq	%r15, %rsi
	je	.L676
	movq	(%rsi), %rax
	testq	%r15, %r15
	je	.L677
	testq	%rsi, %rsi
	je	.L677
	cmpq	%rax, (%r15)
	jne	.L677
.L676:
	movzwl	40(%r12), %esi
	movl	32(%r12), %r13d
	movl	%esi, %eax
	sarl	$7, %eax
	andl	$7, %eax
	cmpb	$5, %al
	ja	.L682
	leaq	.L684(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal13ScopeIterator21SetLocalVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE,"a",@progbits
	.align 4
	.align 4
.L684:
	.long	.L721-.L684
	.long	.L688-.L684
	.long	.L687-.L684
	.long	.L686-.L684
	.long	.L721-.L684
	.long	.L683-.L684
	.section	.text._ZN2v88internal13ScopeIterator21SetLocalVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE
.L683:
	testl	%r13d, %r13d
	jg	.L757
	.p2align 4,,10
	.p2align 3
.L721:
	xorl	%eax, %eax
.L672:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L758
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L686:
	.cfi_restore_state
	movq	40(%r14), %rax
	movq	(%rax), %r15
	movq	-72(%rbp), %rax
	movq	(%rax), %r12
	leal	16(,%r13,8), %eax
	cltq
	leaq	-1(%r15,%rax), %r13
	movq	%r12, 0(%r13)
	testb	$1, %r12b
	je	.L750
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L759
.L710:
	testb	$24, %al
	je	.L750
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L750
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L750:
	movl	$1, %eax
	jmp	.L672
.L687:
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.L760
	call	_ZN2v88internal14FrameInspector16javascript_frameEv@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	cmpl	$4, %eax
	je	.L721
	movq	-72(%rbp), %rax
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	(%r12), %rax
	call	*144(%rax)
	movq	%rbx, (%rax)
	movl	$1, %eax
	jmp	.L672
.L688:
	sarl	$4, %esi
	andl	$7, %esi
	cmpb	$2, %sil
	je	.L721
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.L761
	call	_ZN2v88internal14FrameInspector16javascript_frameEv@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	cmpl	$4, %eax
	je	.L721
	movq	-72(%rbp), %rax
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	(%rax), %rdx
	call	_ZNK2v88internal15JavaScriptFrame17SetParameterValueEiNS0_6ObjectE@PLT
	movl	$1, %eax
	jmp	.L672
.L757:
	movq	40(%r14), %rax
	movq	(%r14), %rbx
	leaq	-64(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context6moduleEv@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L712
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L713:
	movl	32(%r12), %esi
	movq	-72(%rbp), %rdx
	call	_ZN2v88internal16SourceTextModule13StoreVariableENS0_6HandleIS1_EEiNS2_INS0_6ObjectEEE@PLT
	jmp	.L750
.L759:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L710
.L761:
	movq	(%r14), %r12
	movq	24(%r14), %rax
	movq	41112(%r12), %rdi
	movq	(%rax), %rax
	movq	71(%rax), %rbx
	testq	%rdi, %rdi
	je	.L691
.L755:
	movq	%rbx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rbx
.L704:
	movq	-72(%rbp), %rax
	movq	(%rax), %r12
	leal	16(,%r13,8), %eax
	cltq
	leaq	-1(%rbx,%rax), %r13
	movq	%r12, 0(%r13)
	testb	$1, %r12b
	je	.L750
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	je	.L707
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
.L707:
	testb	$24, %al
	je	.L750
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L750
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L750
.L712:
	movq	41088(%rbx), %rdi
	cmpq	41096(%rbx), %rdi
	je	.L762
.L714:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r13, (%rdi)
	jmp	.L713
.L760:
	movq	32(%r14), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rdx
	movq	15(%rdx), %rax
	testb	$1, %al
	jne	.L699
.L701:
	andq	$-262144, %rdx
	movq	24(%rdx), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal9ScopeInfo5EmptyEPNS0_7IsolateE@PLT
.L700:
	movl	11(%rax), %edx
	testl	%edx, %edx
	jle	.L702
	movq	23(%rax), %rax
	sarq	$32, %rax
	addl	%eax, %r13d
.L702:
	movq	(%r14), %r12
	movq	24(%r14), %rax
	movq	41112(%r12), %rdi
	movq	(%rax), %rax
	movq	71(%rax), %rbx
	testq	%rdi, %rdi
	jne	.L755
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L756
.L705:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rbx, (%rax)
	jmp	.L704
.L691:
	movq	41088(%r12), %rax
	cmpq	%rax, 41096(%r12)
	jne	.L705
.L756:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L705
.L699:
	movq	-1(%rax), %rsi
	cmpw	$136, 11(%rsi)
	jne	.L701
	jmp	.L700
.L762:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rdi
	jmp	.L714
.L758:
	call	__stack_chk_fail@PLT
.L682:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20276:
	.size	_ZN2v88internal13ScopeIterator21SetLocalVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE, .-_ZN2v88internal13ScopeIterator21SetLocalVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE
	.section	.rodata._ZN2v88internal13ScopeIterator24SetContextExtensionValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"Object::SetDataProperty(&it, new_value).ToChecked()"
	.section	.text._ZN2v88internal13ScopeIterator24SetContextExtensionValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScopeIterator24SetContextExtensionValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE
	.type	_ZN2v88internal13ScopeIterator24SetContextExtensionValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE, @function
_ZN2v88internal13ScopeIterator24SetContextExtensionValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE:
.LFB20277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	(%rax), %rdx
	movq	31(%rdx), %rax
	movq	%rax, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	%rax, -37496(%rcx)
	jne	.L783
.L764:
	xorl	%eax, %eax
.L763:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L784
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L783:
	.cfi_restore_state
	movq	(%rdi), %rcx
	leaq	-144(%rbp), %r15
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%r15, %rdi
	movq	%rdx, -144(%rbp)
	movq	%rcx, -152(%rbp)
	call	_ZN2v88internal7Context16extension_objectEv@PLT
	movq	-152(%rbp), %rcx
	movq	%rax, %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L765
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rbx), %rdi
	movq	%rax, %r13
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L768
.L770:
	movl	$-1, %edx
	movq	%r13, %rsi
	movq	%rdi, -152(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-152(%rbp), %rdi
	movq	%rax, %rbx
.L769:
	movq	(%r12), %rdx
	movl	$1, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L771
	movl	11(%rdx), %eax
	notl	%eax
	andl	$1, %eax
.L771:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	(%r12), %rax
	movq	%rdi, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	movq	%r12, %rax
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L785
.L772:
	movq	%r15, %rdi
	movq	%rax, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE@PLT
	testb	%al, %al
	je	.L786
.L773:
	shrw	$8, %ax
	je	.L764
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object15SetDataPropertyEPNS0_14LookupIteratorENS0_6HandleIS1_EE@PLT
	testb	%al, %al
	je	.L787
.L774:
	movzbl	%ah, %eax
	testb	%al, %al
	jne	.L763
	leaq	.LC7(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L765:
	movq	41088(%rcx), %r13
	cmpq	41096(%rcx), %r13
	je	.L788
.L767:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, 0(%r13)
	movq	0(%r13), %rax
	movq	(%rbx), %rdi
	testb	$1, %al
	je	.L770
.L768:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L770
	movq	%r13, %rbx
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L785:
	movq	%r12, %rsi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L788:
	movq	%rcx, %rdi
	movq	%rcx, -152(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rsi
	movq	-152(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L786:
	movl	%eax, -152(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-152(%rbp), %eax
	jmp	.L773
	.p2align 4,,10
	.p2align 3
.L787:
	movl	%eax, -152(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-152(%rbp), %eax
	jmp	.L774
.L784:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20277:
	.size	_ZN2v88internal13ScopeIterator24SetContextExtensionValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE, .-_ZN2v88internal13ScopeIterator24SetContextExtensionValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE
	.section	.text._ZN2v88internal13ScopeIterator23SetContextVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScopeIterator23SetContextVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE
	.type	_ZN2v88internal13ScopeIterator23SetContextVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE, @function
_ZN2v88internal13ScopeIterator23SetContextVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE:
.LFB20278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	leaq	-48(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal7Context10scope_infoEv@PLT
	leaq	-49(%rbp), %r8
	leaq	-50(%rbp), %rcx
	movq	%r13, %rsi
	movq	%rax, %rdi
	leaq	-51(%rbp), %rdx
	call	_ZN2v88internal9ScopeInfo16ContextSlotIndexES1_NS0_6StringEPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE@PLT
	xorl	%r8d, %r8d
	testl	%eax, %eax
	js	.L789
	movq	40(%rbx), %rdx
	leal	16(,%rax,8), %eax
	movq	(%r12), %r12
	cltq
	movq	(%rdx), %r13
	leaq	-1(%r13,%rax), %r14
	movq	%r12, (%r14)
	testb	$1, %r12b
	je	.L794
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L805
.L792:
	testb	$24, %al
	je	.L794
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L806
	.p2align 4,,10
	.p2align 3
.L794:
	movl	$1, %r8d
.L789:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L807
	addq	$32, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L805:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L806:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L794
.L807:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20278:
	.size	_ZN2v88internal13ScopeIterator23SetContextVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE, .-_ZN2v88internal13ScopeIterator23SetContextVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE
	.section	.text._ZN2v88internal13ScopeIterator22SetModuleVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScopeIterator22SetModuleVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE
	.type	_ZN2v88internal13ScopeIterator22SetModuleVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE, @function
_ZN2v88internal13ScopeIterator22SetModuleVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE:
.LFB20279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	leaq	-72(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal7Context10scope_infoEv@PLT
	movq	(%r12), %rsi
	leaq	-73(%rbp), %r8
	movq	%r14, %rdi
	leaq	-74(%rbp), %rcx
	leaq	-75(%rbp), %rdx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal9ScopeInfo11ModuleIndexENS0_6StringEPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE@PLT
	movl	%eax, %edi
	movl	%eax, %r12d
	call	_ZN2v88internal26SourceTextModuleDescriptor16GetCellIndexKindEi@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpl	$1, %r8d
	je	.L817
.L808:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L818
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L817:
	.cfi_restore_state
	movq	40(%rbx), %rax
	movq	(%rbx), %r15
	movq	%r14, %rdi
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context6moduleEv@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L810
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L811:
	movq	%r13, %rdx
	movl	%r12d, %esi
	call	_ZN2v88internal16SourceTextModule13StoreVariableENS0_6HandleIS1_EEiNS2_INS0_6ObjectEEE@PLT
	movl	$1, %eax
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L810:
	movq	41088(%r15), %rdi
	cmpq	41096(%r15), %rdi
	je	.L819
.L812:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rdi)
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L819:
	movq	%r15, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L812
.L818:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20279:
	.size	_ZN2v88internal13ScopeIterator22SetModuleVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE, .-_ZN2v88internal13ScopeIterator22SetModuleVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE
	.section	.text._ZN2v88internal13ScopeIterator22SetScriptVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScopeIterator22SetScriptVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE
	.type	_ZN2v88internal13ScopeIterator22SetScriptVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE, @function
_ZN2v88internal13ScopeIterator22SetScriptVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE:
.LFB20280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-72(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%rdx, -88(%rbp)
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	23(%rax), %rax
	movq	1135(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L821
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L822:
	movq	(%r12), %rdx
	movq	(%r14), %rsi
	movq	%r13, %rcx
	movq	(%rbx), %rdi
	call	_ZN2v88internal18ScriptContextTable6LookupEPNS0_7IsolateES1_NS0_6StringEPNS1_12LookupResultE@PLT
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L843
.L820:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L844
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L821:
	.cfi_restore_state
	movq	41088(%r15), %r14
	cmpq	41096(%r15), %r14
	je	.L845
.L823:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	jmp	.L822
	.p2align 4,,10
	.p2align 3
.L843:
	movl	-72(%rbp), %eax
	movq	(%rbx), %rbx
	movq	(%r14), %rdx
	leal	24(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %r14
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L825
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r14
.L826:
	movq	-88(%rbp), %rax
	movq	(%rax), %r13
	movl	-68(%rbp), %eax
	leal	16(,%rax,8), %eax
	cltq
	leaq	-1(%r14,%rax), %r15
	movq	%r13, (%r15)
	testb	$1, %r13b
	je	.L820
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L829
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L829:
	testb	$24, %al
	je	.L820
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L820
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L825:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L846
.L827:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r14, (%rax)
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L845:
	movq	%r15, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L823
	.p2align 4,,10
	.p2align 3
.L846:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L827
.L844:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20280:
	.size	_ZN2v88internal13ScopeIterator22SetScriptVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE, .-_ZN2v88internal13ScopeIterator22SetScriptVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE
	.section	.text._ZN2v88internal13ScopeIterator16SetVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ScopeIterator16SetVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE
	.type	_ZN2v88internal13ScopeIterator16SetVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE, @function
_ZN2v88internal13ScopeIterator16SetVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE:
.LFB20266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	(%rsi), %rax
	movq	(%rdi), %rdi
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L883
	movq	32(%r12), %rdx
	testq	%rdx, %rdx
	je	.L849
.L884:
	movq	80(%r12), %rax
	movzbl	128(%rax), %eax
	cmpb	$7, %al
	ja	.L850
	leaq	CSWTCH.1074(%rip), %rcx
	cmpl	$8, (%rcx,%rax,4)
	ja	.L871
	movl	(%rcx,%rax,4), %eax
	leaq	.L853(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal13ScopeIterator16SetVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE,"a",@progbits
	.align 4
	.align 4
.L853:
	.long	.L871-.L853
	.long	.L855-.L853
	.long	.L871-.L853
	.long	.L855-.L853
	.long	.L852-.L853
	.long	.L852-.L853
	.long	.L854-.L853
	.long	.L852-.L853
	.long	.L852-.L853
	.section	.text._ZN2v88internal13ScopeIterator16SetVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE
	.p2align 4,,10
	.p2align 3
.L852:
	testq	%rdx, %rdx
	je	.L865
	addq	$16, %rsp
	movq	%r14, %rdx
	movq	%r12, %rdi
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13ScopeIterator21SetLocalVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE
	.p2align 4,,10
	.p2align 3
.L855:
	.cfi_restore_state
	testq	%rdx, %rdx
	je	.L868
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal13ScopeIterator21SetLocalVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE
	testb	%al, %al
	jne	.L869
	movq	80(%r12), %rax
	movq	-24(%rbp), %rsi
	movl	124(%rax), %eax
	testl	%eax, %eax
	jle	.L871
.L870:
	addq	$16, %rsp
	movq	%r14, %rdx
	movq	%r12, %rdi
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13ScopeIterator24SetContextExtensionValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE
	.p2align 4,,10
	.p2align 3
.L885:
	.cfi_restore_state
	cmpb	$0, 88(%r12)
	jne	.L871
.L854:
	addq	$16, %rsp
	movq	%r14, %rdx
	movq	%r12, %rdi
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13ScopeIterator22SetScriptVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE
	.p2align 4,,10
	.p2align 3
.L883:
	.cfi_restore_state
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	32(%r12), %rdx
	movq	%rax, %rsi
	testq	%rdx, %rdx
	jne	.L884
.L849:
	movq	40(%r12), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$145, 11(%rdx)
	je	.L885
	movq	-1(%rax), %rdx
	cmpw	$143, 11(%rdx)
	je	.L868
	movq	-1(%rax), %rdx
	cmpw	$142, 11(%rdx)
	je	.L868
	movq	-1(%rax), %rdx
	cmpw	$141, 11(%rdx)
	je	.L868
	movq	-1(%rax), %rdx
	cmpw	$140, 11(%rdx)
	je	.L865
	movq	-1(%rax), %rdx
	cmpw	$139, 11(%rdx)
	je	.L865
	movq	-1(%rax), %rdx
	cmpw	$144, 11(%rdx)
	je	.L865
	movq	-1(%rax), %rax
	cmpw	$146, 11(%rax)
	je	.L854
	.p2align 4,,10
	.p2align 3
.L871:
	xorl	%eax, %eax
.L847:
	addq	$16, %rsp
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L865:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZNK2v88internal13ScopeIterator4TypeEv
	movq	-24(%rbp), %rsi
	cmpl	$8, %eax
	je	.L866
.L867:
	addq	$16, %rsp
	movq	%r14, %rdx
	movq	%r12, %rdi
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13ScopeIterator23SetContextVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE
	.p2align 4,,10
	.p2align 3
.L868:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal13ScopeIterator23SetContextVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE
	movq	-24(%rbp), %rsi
	testb	%al, %al
	je	.L870
.L869:
	movl	$1, %eax
	jmp	.L847
	.p2align 4,,10
	.p2align 3
.L866:
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal13ScopeIterator22SetModuleVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE
	movq	-24(%rbp), %rsi
	testb	%al, %al
	je	.L867
	jmp	.L869
.L850:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20266:
	.size	_ZN2v88internal13ScopeIterator16SetVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE, .-_ZN2v88internal13ScopeIterator16SetVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateEPNS0_14FrameInspectorENS1_6OptionE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateEPNS0_14FrameInspectorENS1_6OptionE, @function
_GLOBAL__sub_I__ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateEPNS0_14FrameInspectorENS1_6OptionE:
.LFB25160:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25160:
	.size	_GLOBAL__sub_I__ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateEPNS0_14FrameInspectorENS1_6OptionE, .-_GLOBAL__sub_I__ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateEPNS0_14FrameInspectorENS1_6OptionE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal13ScopeIteratorC2EPNS0_7IsolateEPNS0_14FrameInspectorENS1_6OptionE
	.section	.rodata.CSWTCH.1074,"a"
	.align 32
	.type	CSWTCH.1074, @object
	.size	CSWTCH.1074, 32
CSWTCH.1074:
	.long	5
	.long	7
	.long	1
	.long	8
	.long	6
	.long	4
	.long	5
	.long	2
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
