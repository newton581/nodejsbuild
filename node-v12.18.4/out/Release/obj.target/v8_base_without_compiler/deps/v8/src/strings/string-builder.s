	.file	"string-builder.cc"
	.text
	.section	.text._ZN2v88internal25StringBuilderConcatHelperIhEEvNS0_6StringEPT_NS0_10FixedArrayEi,"axG",@progbits,_ZN2v88internal25StringBuilderConcatHelperIhEEvNS0_6StringEPT_NS0_10FixedArrayEi,comdat
	.p2align 4
	.weak	_ZN2v88internal25StringBuilderConcatHelperIhEEvNS0_6StringEPT_NS0_10FixedArrayEi
	.type	_ZN2v88internal25StringBuilderConcatHelperIhEEvNS0_6StringEPT_NS0_10FixedArrayEi, @function
_ZN2v88internal25StringBuilderConcatHelperIhEEvNS0_6StringEPT_NS0_10FixedArrayEi:
.LFB19731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	movl	%ecx, -60(%rbp)
	testl	%ecx, %ecx
	jle	.L1
	movq	%rdi, %r14
	leaq	-1(%rdx), %rbx
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L12:
	sarq	$32, %rdi
	testq	%rdi, %rdi
	jle	.L4
	movl	%edi, %edx
	movl	%edi, %r12d
	shrl	$11, %edx
	andl	$2047, %r12d
	andl	$524287, %edx
.L5:
	leal	(%rdx,%r12), %ecx
	movq	%r14, %rdi
	addl	%r12d, %r15d
	call	_ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii@PLT
	cmpl	%r13d, -60(%rbp)
	jle	.L1
.L8:
	leal	2(%r13), %ecx
	addl	$1, %r13d
	leal	0(,%rcx,8), %edx
	movslq	%edx, %rsi
	movq	(%rsi,%rbx), %rdi
	movslq	%r15d, %rsi
	addq	-56(%rbp), %rsi
	testb	$1, %dil
	je	.L12
	movl	11(%rdi), %ecx
	xorl	%edx, %edx
	movl	%ecx, -64(%rbp)
	call	_ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii@PLT
	movl	-64(%rbp), %ecx
	addl	%ecx, %r15d
	cmpl	%r13d, -60(%rbp)
	jg	.L8
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	addl	$8, %edx
	movl	%edi, %r12d
	movl	%ecx, %r13d
	movslq	%edx, %rdx
	negl	%r12d
	movq	(%rdx,%rbx), %rdx
	shrq	$32, %rdx
	jmp	.L5
	.cfi_endproc
.LFE19731:
	.size	_ZN2v88internal25StringBuilderConcatHelperIhEEvNS0_6StringEPT_NS0_10FixedArrayEi, .-_ZN2v88internal25StringBuilderConcatHelperIhEEvNS0_6StringEPT_NS0_10FixedArrayEi
	.section	.text._ZN2v88internal25StringBuilderConcatHelperItEEvNS0_6StringEPT_NS0_10FixedArrayEi,"axG",@progbits,_ZN2v88internal25StringBuilderConcatHelperItEEvNS0_6StringEPT_NS0_10FixedArrayEi,comdat
	.p2align 4
	.weak	_ZN2v88internal25StringBuilderConcatHelperItEEvNS0_6StringEPT_NS0_10FixedArrayEi
	.type	_ZN2v88internal25StringBuilderConcatHelperItEEvNS0_6StringEPT_NS0_10FixedArrayEi, @function
_ZN2v88internal25StringBuilderConcatHelperItEEvNS0_6StringEPT_NS0_10FixedArrayEi:
.LFB19732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	movl	%ecx, -60(%rbp)
	testl	%ecx, %ecx
	jle	.L13
	movq	%rdi, %r14
	leaq	-1(%rdx), %rbx
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L23:
	sarq	$32, %rdi
	testq	%rdi, %rdi
	jle	.L16
	movl	%edi, %edx
	movl	%edi, %r13d
	shrl	$11, %edx
	andl	$2047, %r13d
	andl	$524287, %edx
.L17:
	leal	(%rdx,%r13), %ecx
	movq	%r14, %rdi
	addl	%r13d, %r15d
	call	_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii@PLT
	cmpl	%r12d, -60(%rbp)
	jle	.L13
.L20:
	leal	2(%r12), %ecx
	addl	$1, %r12d
	leal	0(,%rcx,8), %edx
	movslq	%edx, %rsi
	movq	(%rsi,%rbx), %rdi
	movq	-56(%rbp), %rax
	movslq	%r15d, %rsi
	leaq	(%rax,%rsi,2), %rsi
	testb	$1, %dil
	je	.L23
	movl	11(%rdi), %ecx
	xorl	%edx, %edx
	movl	%ecx, -64(%rbp)
	call	_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii@PLT
	movl	-64(%rbp), %ecx
	addl	%ecx, %r15d
	cmpl	%r12d, -60(%rbp)
	jg	.L20
.L13:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	addl	$8, %edx
	movl	%edi, %r13d
	movl	%ecx, %r12d
	movslq	%edx, %rdx
	negl	%r13d
	movq	(%rdx,%rbx), %rdx
	shrq	$32, %rdx
	jmp	.L17
	.cfi_endproc
.LFE19732:
	.size	_ZN2v88internal25StringBuilderConcatHelperItEEvNS0_6StringEPT_NS0_10FixedArrayEi, .-_ZN2v88internal25StringBuilderConcatHelperItEEvNS0_6StringEPT_NS0_10FixedArrayEi
	.section	.text._ZN2v88internal25StringBuilderConcatLengthEiNS0_10FixedArrayEiPb,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25StringBuilderConcatLengthEiNS0_10FixedArrayEiPb
	.type	_ZN2v88internal25StringBuilderConcatLengthEiNS0_10FixedArrayEiPb, @function
_ZN2v88internal25StringBuilderConcatLengthEiNS0_10FixedArrayEiPb:
.LFB17840:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	jle	.L33
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rsi), %r11
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	$1073741799, %ebx
.L32:
	leal	16(,%rsi,8), %r8d
	movslq	%r8d, %rax
	movq	(%rax,%r11), %rax
	testb	$1, %al
	jne	.L26
	sarq	$32, %rax
	testq	%rax, %rax
	jle	.L27
	movl	%eax, %r8d
	movl	%eax, %r10d
	shrl	$11, %r8d
	andl	$2047, %r10d
	andl	$524287, %r8d
.L28:
	cmpl	%edi, %r8d
	jg	.L30
	movl	%edi, %eax
	subl	%r8d, %eax
	cmpl	%r10d, %eax
	jl	.L30
.L31:
	movl	%ebx, %eax
	subl	%r9d, %eax
	cmpl	%r10d, %eax
	jl	.L34
	addl	$1, %esi
	addl	%r10d, %r9d
	cmpl	%esi, %edx
	jg	.L32
.L24:
	movl	%r9d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	-1(%rax), %r8
	cmpw	$63, 11(%r8)
	ja	.L30
	cmpb	$0, (%rcx)
	movl	11(%rax), %r10d
	je	.L31
	movq	-1(%rax), %rax
	testb	$8, 11(%rax)
	jne	.L31
	movb	$0, (%rcx)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L27:
	movl	%eax, %r10d
	addl	$1, %esi
	negl	%r10d
	cmpl	%esi, %edx
	jle	.L30
	addl	$8, %r8d
	movslq	%r8d, %r8
	movq	(%r8,%r11), %rax
	testb	$1, %al
	jne	.L30
	sarq	$32, %rax
	movl	%eax, %r8d
	jns	.L28
.L30:
	movl	$-1, %r9d
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movl	$2147483647, %r9d
	jmp	.L24
.L33:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	xorl	%r9d, %r9d
	movl	%r9d, %eax
	ret
	.cfi_endproc
.LFE17840:
	.size	_ZN2v88internal25StringBuilderConcatLengthEiNS0_10FixedArrayEiPb, .-_ZN2v88internal25StringBuilderConcatLengthEiNS0_10FixedArrayEiPb
	.section	.text._ZN2v88internal17FixedArrayBuilderC2EPNS0_7IsolateEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17FixedArrayBuilderC2EPNS0_7IsolateEi
	.type	_ZN2v88internal17FixedArrayBuilderC2EPNS0_7IsolateEi, @function
_ZN2v88internal17FixedArrayBuilderC2EPNS0_7IsolateEi:
.LFB17842:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	%edx, %esi
	xorl	%edx, %edx
	subq	$8, %rsp
	call	_ZN2v88internal7Factory22NewFixedArrayWithHolesEiNS0_14AllocationTypeE@PLT
	movl	$0, 8(%rbx)
	movq	%rax, (%rbx)
	movb	$0, 12(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17842:
	.size	_ZN2v88internal17FixedArrayBuilderC2EPNS0_7IsolateEi, .-_ZN2v88internal17FixedArrayBuilderC2EPNS0_7IsolateEi
	.globl	_ZN2v88internal17FixedArrayBuilderC1EPNS0_7IsolateEi
	.set	_ZN2v88internal17FixedArrayBuilderC1EPNS0_7IsolateEi,_ZN2v88internal17FixedArrayBuilderC2EPNS0_7IsolateEi
	.section	.text._ZN2v88internal17FixedArrayBuilderC2ENS0_6HandleINS0_10FixedArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17FixedArrayBuilderC2ENS0_6HandleINS0_10FixedArrayEEE
	.type	_ZN2v88internal17FixedArrayBuilderC2ENS0_6HandleINS0_10FixedArrayEEE, @function
_ZN2v88internal17FixedArrayBuilderC2ENS0_6HandleINS0_10FixedArrayEEE:
.LFB17845:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movl	$0, 8(%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE17845:
	.size	_ZN2v88internal17FixedArrayBuilderC2ENS0_6HandleINS0_10FixedArrayEEE, .-_ZN2v88internal17FixedArrayBuilderC2ENS0_6HandleINS0_10FixedArrayEEE
	.globl	_ZN2v88internal17FixedArrayBuilderC1ENS0_6HandleINS0_10FixedArrayEEE
	.set	_ZN2v88internal17FixedArrayBuilderC1ENS0_6HandleINS0_10FixedArrayEEE,_ZN2v88internal17FixedArrayBuilderC2ENS0_6HandleINS0_10FixedArrayEEE
	.section	.text._ZN2v88internal17FixedArrayBuilder11HasCapacityEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17FixedArrayBuilder11HasCapacityEi
	.type	_ZN2v88internal17FixedArrayBuilder11HasCapacityEi, @function
_ZN2v88internal17FixedArrayBuilder11HasCapacityEi:
.LFB17847:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addl	8(%rdi), %esi
	movq	(%rax), %rax
	cmpl	%esi, 11(%rax)
	setge	%al
	ret
	.cfi_endproc
.LFE17847:
	.size	_ZN2v88internal17FixedArrayBuilder11HasCapacityEi, .-_ZN2v88internal17FixedArrayBuilder11HasCapacityEi
	.section	.text._ZN2v88internal17FixedArrayBuilder14EnsureCapacityEPNS0_7IsolateEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17FixedArrayBuilder14EnsureCapacityEPNS0_7IsolateEi
	.type	_ZN2v88internal17FixedArrayBuilder14EnsureCapacityEPNS0_7IsolateEi, @function
_ZN2v88internal17FixedArrayBuilder14EnsureCapacityEPNS0_7IsolateEi:
.LFB17848:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	addl	8(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	(%rax), %rax
	movslq	11(%rax), %rax
	cmpl	%eax, %edx
	jle	.L47
	movq	%rsi, %rdi
	movl	%eax, %esi
	.p2align 4,,10
	.p2align 3
.L49:
	addl	%esi, %esi
	cmpl	%esi, %edx
	jg	.L49
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory22NewFixedArrayWithHolesEiNS0_14AllocationTypeE@PLT
	movl	8(%rbx), %r8d
	leaq	-32(%rbp), %rdi
	xorl	%ecx, %ecx
	movq	%rax, %r12
	movq	(%rbx), %rax
	xorl	%esi, %esi
	movq	(%rax), %rax
	movq	%rax, -32(%rbp)
	movq	(%r12), %rdx
	call	_ZNK2v88internal10FixedArray6CopyToEiS1_ii@PLT
	movq	%r12, (%rbx)
.L47:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L53
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L53:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17848:
	.size	_ZN2v88internal17FixedArrayBuilder14EnsureCapacityEPNS0_7IsolateEi, .-_ZN2v88internal17FixedArrayBuilder14EnsureCapacityEPNS0_7IsolateEi
	.section	.text._ZN2v88internal17FixedArrayBuilder3AddENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17FixedArrayBuilder3AddENS0_6ObjectE
	.type	_ZN2v88internal17FixedArrayBuilder3AddENS0_6ObjectE, @function
_ZN2v88internal17FixedArrayBuilder3AddENS0_6ObjectE:
.LFB17849:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	(%rax), %r13
	movl	8(%rdi), %eax
	leal	16(,%rax,8), %eax
	cltq
	leaq	-1(%r13,%rax), %r14
	movq	%rsi, (%r14)
	testb	$1, %sil
	je	.L58
	movq	%rsi, %r15
	movq	%rsi, %r12
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L66
	testb	$24, %al
	je	.L58
.L68:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L67
.L58:
	addl	$1, 8(%rbx)
	movb	$1, 12(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movq	%rsi, %rdx
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	testb	$24, %al
	jne	.L68
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L67:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L58
	.cfi_endproc
.LFE17849:
	.size	_ZN2v88internal17FixedArrayBuilder3AddENS0_6ObjectE, .-_ZN2v88internal17FixedArrayBuilder3AddENS0_6ObjectE
	.section	.text._ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE
	.type	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE, @function
_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE:
.LFB17850:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	movq	(%rdi), %rdx
	leal	16(,%rax,8), %eax
	movq	(%rdx), %rdx
	cltq
	movq	%rsi, -1(%rax,%rdx)
	addl	$1, 8(%rdi)
	ret
	.cfi_endproc
.LFE17850:
	.size	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE, .-_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE
	.section	.text._ZN2v88internal17FixedArrayBuilder8capacityEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17FixedArrayBuilder8capacityEv
	.type	_ZN2v88internal17FixedArrayBuilder8capacityEv, @function
_ZN2v88internal17FixedArrayBuilder8capacityEv:
.LFB17851:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax), %rax
	movslq	11(%rax), %rax
	ret
	.cfi_endproc
.LFE17851:
	.size	_ZN2v88internal17FixedArrayBuilder8capacityEv, .-_ZN2v88internal17FixedArrayBuilder8capacityEv
	.section	.text._ZN2v88internal17FixedArrayBuilder9ToJSArrayENS0_6HandleINS0_7JSArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17FixedArrayBuilder9ToJSArrayENS0_6HandleINS0_7JSArrayEEE
	.type	_ZN2v88internal17FixedArrayBuilder9ToJSArrayENS0_6HandleINS0_7JSArrayEEE, @function
_ZN2v88internal17FixedArrayBuilder9ToJSArrayENS0_6HandleINS0_7JSArrayEEE:
.LFB17852:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%r14), %r13
	movq	(%rdi), %r15
	movq	%r13, %rax
	movq	(%r15), %r12
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movslq	11(%r12), %rsi
	movq	-1(%r12), %rdx
	cmpq	%rdx, -37104(%rax)
	je	.L72
	movq	-1(%r13), %rdx
	movzbl	14(%rdx), %edx
	shrl	$3, %edx
	cmpl	$3, %edx
	je	.L79
	movq	-37496(%rax), %r8
	testl	%esi, %esi
	je	.L79
	cmpb	$5, %dl
	leaq	15(%r12), %rax
	setbe	%cl
	subl	$1, %esi
	leaq	23(%r12,%rsi,8), %r9
	andl	%edx, %ecx
	movzbl	%dl, %esi
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L75:
	andl	$1, %edi
	je	.L76
	testb	%cl, %cl
	jne	.L93
	movl	$2, %esi
.L76:
	addq	$8, %rax
	cmpq	%rax, %r9
	je	.L105
.L78:
	movq	(%rax), %rdi
	cmpq	%rdi, %r8
	jne	.L75
	movl	$1, %ecx
	testb	%sil, %sil
	je	.L89
	cmpb	$4, %sil
	je	.L90
	cmpb	$2, %sil
	je	.L91
	cmpb	$6, %sil
	movl	$7, %edi
	cmove	%edi, %esi
	addq	$8, %rax
	cmpq	%rax, %r9
	jne	.L78
	.p2align 4,,10
	.p2align 3
.L105:
	cmpb	%sil, %dl
	jne	.L103
	.p2align 4,,10
	.p2align 3
.L79:
	movq	%r12, 15(%r13)
	leaq	15(%r13), %rsi
	testb	$1, %r12b
	je	.L88
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L106
	testb	$24, %al
	je	.L88
.L108:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L107
.L88:
	movq	(%r15), %rax
	movq	(%r14), %rdx
	movslq	11(%rax), %rax
	salq	$32, %rax
	movq	%rax, 23(%rdx)
	movslq	8(%rbx), %rax
	movq	(%r14), %rdx
	salq	$32, %rax
	movq	%rax, 23(%rdx)
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L108
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L89:
	movl	$1, %esi
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L72:
	movq	-1(%r13), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$1, %eax
	je	.L104
	movq	-1(%r13), %rax
	testb	$-8, 14(%rax)
	jne	.L79
	testl	%esi, %esi
	je	.L82
	subq	$1, %r12
	addl	$2, %esi
	movl	$2, %ecx
	movabsq	$-2251799814209537, %rdi
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L83:
	addl	$1, %ecx
	cmpl	%ecx, %esi
	je	.L82
.L84:
	leal	0(,%rcx,8), %eax
	cltq
	cmpq	%rdi, (%rax,%r12)
	jne	.L83
.L104:
	movl	$5, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	movq	(%r14), %r13
	movq	(%r15), %r12
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L82:
	movl	$4, %esi
.L103:
	movq	%r14, %rdi
	call	_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	movq	(%r14), %r13
	movq	(%r15), %r12
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L90:
	movl	$5, %esi
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L91:
	movl	$3, %esi
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L93:
	movl	$3, %esi
	jmp	.L103
	.cfi_endproc
.LFE17852:
	.size	_ZN2v88internal17FixedArrayBuilder9ToJSArrayENS0_6HandleINS0_7JSArrayEEE, .-_ZN2v88internal17FixedArrayBuilder9ToJSArrayENS0_6HandleINS0_7JSArrayEEE
	.section	.text._ZN2v88internal24ReplacementStringBuilderC2EPNS0_4HeapENS0_6HandleINS0_6StringEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ReplacementStringBuilderC2EPNS0_4HeapENS0_6HandleINS0_6StringEEEi
	.type	_ZN2v88internal24ReplacementStringBuilderC2EPNS0_4HeapENS0_6HandleINS0_6StringEEEi, @function
_ZN2v88internal24ReplacementStringBuilderC2EPNS0_4HeapENS0_6HandleINS0_6StringEEEi:
.LFB17854:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	xorl	%edx, %edx
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, (%rdi)
	leaq	-37592(%rsi), %rdi
	movl	%ecx, %esi
	call	_ZN2v88internal7Factory22NewFixedArrayWithHolesEiNS0_14AllocationTypeE@PLT
	movl	$0, 16(%rbx)
	movq	%rax, 8(%rbx)
	movb	$0, 20(%rbx)
	movq	(%r12), %rax
	movq	%r12, 24(%rbx)
	movl	$0, 32(%rbx)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	shrw	$3, %ax
	andl	$1, %eax
	movb	%al, 36(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17854:
	.size	_ZN2v88internal24ReplacementStringBuilderC2EPNS0_4HeapENS0_6HandleINS0_6StringEEEi, .-_ZN2v88internal24ReplacementStringBuilderC2EPNS0_4HeapENS0_6HandleINS0_6StringEEEi
	.globl	_ZN2v88internal24ReplacementStringBuilderC1EPNS0_4HeapENS0_6HandleINS0_6StringEEEi
	.set	_ZN2v88internal24ReplacementStringBuilderC1EPNS0_4HeapENS0_6HandleINS0_6StringEEEi,_ZN2v88internal24ReplacementStringBuilderC2EPNS0_4HeapENS0_6HandleINS0_6StringEEEi
	.section	.text._ZN2v88internal24ReplacementStringBuilder14EnsureCapacityEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ReplacementStringBuilder14EnsureCapacityEi
	.type	_ZN2v88internal24ReplacementStringBuilder14EnsureCapacityEi, @function
_ZN2v88internal24ReplacementStringBuilder14EnsureCapacityEi:
.LFB17856:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	addl	16(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movslq	11(%rax), %rax
	cmpl	%esi, %eax
	jge	.L111
	movq	%rdi, %rbx
	movl	%eax, %r8d
	.p2align 4,,10
	.p2align 3
.L113:
	addl	%r8d, %r8d
	cmpl	%r8d, %esi
	jg	.L113
	movq	(%rbx), %rax
	movl	%r8d, %esi
	xorl	%edx, %edx
	leaq	-37592(%rax), %rdi
	call	_ZN2v88internal7Factory22NewFixedArrayWithHolesEiNS0_14AllocationTypeE@PLT
	movl	16(%rbx), %r8d
	leaq	-32(%rbp), %rdi
	xorl	%ecx, %ecx
	movq	%rax, %r12
	movq	8(%rbx), %rax
	xorl	%esi, %esi
	movq	(%rax), %rax
	movq	%rax, -32(%rbp)
	movq	(%r12), %rdx
	call	_ZNK2v88internal10FixedArray6CopyToEiS1_ii@PLT
	movq	%r12, 8(%rbx)
.L111:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L117
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L117:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17856:
	.size	_ZN2v88internal24ReplacementStringBuilder14EnsureCapacityEi, .-_ZN2v88internal24ReplacementStringBuilder14EnsureCapacityEi
	.section	.text._ZN2v88internal24ReplacementStringBuilder9AddStringENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ReplacementStringBuilder9AddStringENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal24ReplacementStringBuilder9AddStringENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal24ReplacementStringBuilder9AddStringENS0_6HandleINS0_6StringEEE:
.LFB17857:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movl	11(%r13), %r12d
	movq	(%rax), %r15
	movl	16(%rdi), %eax
	movslq	11(%r15), %rcx
	leal	1(%rax), %edx
	cmpl	%edx, %ecx
	jge	.L119
	movl	%ecx, %esi
	.p2align 4,,10
	.p2align 3
.L120:
	addl	%esi, %esi
	cmpl	%esi, %edx
	jg	.L120
	movq	(%rbx), %rax
	xorl	%edx, %edx
	leaq	-37592(%rax), %rdi
	call	_ZN2v88internal7Factory22NewFixedArrayWithHolesEiNS0_14AllocationTypeE@PLT
	movl	16(%rbx), %r8d
	leaq	-64(%rbp), %rdi
	xorl	%ecx, %ecx
	movq	%rax, %r15
	movq	8(%rbx), %rax
	xorl	%esi, %esi
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	movq	(%r15), %rdx
	call	_ZNK2v88internal10FixedArray6CopyToEiS1_ii@PLT
	movq	%r15, 8(%rbx)
	movl	16(%rbx), %eax
	movq	(%r14), %r13
	movq	(%r15), %r15
.L119:
	leal	16(,%rax,8), %eax
	cltq
	leaq	-1(%r15,%rax), %rsi
	movq	%r13, (%rsi)
	testb	$1, %r13b
	je	.L126
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	jne	.L137
	testb	$24, %al
	je	.L126
.L140:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L138
	.p2align 4,,10
	.p2align 3
.L126:
	addl	$1, 16(%rbx)
	movb	$1, 20(%rbx)
	movq	(%r14), %rax
	movq	-1(%rax), %rax
	testb	$8, 11(%rax)
	jne	.L124
	movb	$0, 36(%rbx)
.L124:
	movl	32(%rbx), %edx
	movl	$1073741799, %eax
	subl	%r12d, %eax
	addl	%edx, %r12d
	cmpl	%eax, %edx
	movl	$2147483647, %eax
	cmovg	%eax, %r12d
	movl	%r12d, 32(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L139
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L140
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L138:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L126
.L139:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17857:
	.size	_ZN2v88internal24ReplacementStringBuilder9AddStringENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal24ReplacementStringBuilder9AddStringENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal24ReplacementStringBuilder8ToStringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ReplacementStringBuilder8ToStringEv
	.type	_ZN2v88internal24ReplacementStringBuilder8ToStringEv, @function
_ZN2v88internal24ReplacementStringBuilder8ToStringEv:
.LFB17858:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	16(%rdi), %edx
	movq	(%rdi), %rax
	testl	%edx, %edx
	jne	.L142
	subq	$37464, %rax
.L143:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	xorl	%edx, %edx
	cmpb	$0, 36(%r12)
	movl	32(%r12), %esi
	leaq	-37592(%rax), %rdi
	jne	.L150
	call	_ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L149
	movq	(%rax), %rax
	movl	16(%r12), %ecx
	leaq	15(%rax), %rsi
	movq	8(%r12), %rax
	movq	(%rax), %rdx
	movq	24(%r12), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal25StringBuilderConcatHelperItEEvNS0_6StringEPT_NS0_10FixedArrayEi
.L146:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	call	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L149
	movq	(%rax), %rax
	movl	16(%r12), %ecx
	leaq	15(%rax), %rsi
	movq	8(%r12), %rax
	movq	(%rax), %rdx
	movq	24(%r12), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal25StringBuilderConcatHelperIhEEvNS0_6StringEPT_NS0_10FixedArrayEi
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L149:
	xorl	%eax, %eax
	jmp	.L143
	.cfi_endproc
.LFE17858:
	.size	_ZN2v88internal24ReplacementStringBuilder8ToStringEv, .-_ZN2v88internal24ReplacementStringBuilder8ToStringEv
	.section	.text._ZN2v88internal24ReplacementStringBuilder10AddElementENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ReplacementStringBuilder10AddElementENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal24ReplacementStringBuilder10AddElementENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal24ReplacementStringBuilder10AddElementENS0_6HandleINS0_6ObjectEEE:
.LFB17862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	(%rax), %r12
	movl	16(%rdi), %eax
	movslq	11(%r12), %rcx
	leal	1(%rax), %edx
	cmpl	%edx, %ecx
	jge	.L152
	movl	%ecx, %esi
	.p2align 4,,10
	.p2align 3
.L153:
	addl	%esi, %esi
	cmpl	%esi, %edx
	jg	.L153
	movq	(%rbx), %rax
	xorl	%edx, %edx
	leaq	-37592(%rax), %rdi
	call	_ZN2v88internal7Factory22NewFixedArrayWithHolesEiNS0_14AllocationTypeE@PLT
	movl	16(%rbx), %r8d
	leaq	-64(%rbp), %rdi
	xorl	%ecx, %ecx
	movq	%rax, %r12
	movq	8(%rbx), %rax
	xorl	%esi, %esi
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	movq	(%r12), %rdx
	call	_ZNK2v88internal10FixedArray6CopyToEiS1_ii@PLT
	movq	%r12, 8(%rbx)
	movl	16(%rbx), %eax
	movq	(%r12), %r12
.L152:
	leal	16(,%rax,8), %eax
	movq	0(%r13), %r13
	cltq
	leaq	-1(%r12,%rax), %r14
	movq	%r13, (%r14)
	testb	$1, %r13b
	je	.L157
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L167
	testb	$24, %al
	je	.L157
.L170:
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L168
	.p2align 4,,10
	.p2align 3
.L157:
	addl	$1, 16(%rbx)
	movb	$1, 20(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L169
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	testb	$24, %al
	jne	.L170
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L168:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L157
.L169:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17862:
	.size	_ZN2v88internal24ReplacementStringBuilder10AddElementENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal24ReplacementStringBuilder10AddElementENS0_6HandleINS0_6ObjectEEE
	.section	.rodata._ZN2v88internal24IncrementalStringBuilderC2EPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"(location_) != nullptr"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal24IncrementalStringBuilderC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24IncrementalStringBuilderC2EPNS0_7IsolateE
	.type	_ZN2v88internal24IncrementalStringBuilderC2EPNS0_7IsolateE, @function
_ZN2v88internal24IncrementalStringBuilderC2EPNS0_7IsolateE:
.LFB17864:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movups	%xmm0, 24(%rdi)
	movq	128(%rsi), %r13
	movq	41088(%rsi), %rax
	movq	%rsi, (%rdi)
	movl	$0, 8(%rdi)
	movb	$0, 12(%rdi)
	movq	$32, 16(%rdi)
	movq	%rsi, %rdi
	movl	$32, %esi
	cmpq	41096(%r12), %rax
	je	.L177
.L172:
	leaq	8(%rax), %rdx
	movq	%rax, 24(%rbx)
	movq	%rdx, 41088(%r12)
	xorl	%edx, %edx
	movq	%r13, (%rax)
	call	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L178
	movq	%rax, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	16(%rbx), %esi
	movq	(%rbx), %rdi
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L178:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17864:
	.size	_ZN2v88internal24IncrementalStringBuilderC2EPNS0_7IsolateE, .-_ZN2v88internal24IncrementalStringBuilderC2EPNS0_7IsolateE
	.globl	_ZN2v88internal24IncrementalStringBuilderC1EPNS0_7IsolateE
	.set	_ZN2v88internal24IncrementalStringBuilderC1EPNS0_7IsolateE,_ZN2v88internal24IncrementalStringBuilderC2EPNS0_7IsolateE
	.section	.text._ZNK2v88internal24IncrementalStringBuilder6LengthEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal24IncrementalStringBuilder6LengthEv
	.type	_ZNK2v88internal24IncrementalStringBuilder6LengthEv, @function
_ZNK2v88internal24IncrementalStringBuilder6LengthEv:
.LFB17866:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	(%rax), %rdx
	movl	20(%rdi), %eax
	addl	11(%rdx), %eax
	ret
	.cfi_endproc
.LFE17866:
	.size	_ZNK2v88internal24IncrementalStringBuilder6LengthEv, .-_ZNK2v88internal24IncrementalStringBuilder6LengthEv
	.section	.text._ZN2v88internal24IncrementalStringBuilder10AccumulateENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24IncrementalStringBuilder10AccumulateENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal24IncrementalStringBuilder10AccumulateENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal24IncrementalStringBuilder10AccumulateENS0_6HandleINS0_6StringEEE:
.LFB17867:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rsi
	movq	(%rdx), %rcx
	movq	(%rdi), %rdi
	movq	(%rsi), %rax
	movl	11(%rax), %eax
	addl	11(%rcx), %eax
	cmpl	$1073741799, %eax
	jle	.L181
	leaq	128(%rdi), %rax
	movb	$1, 12(%rbx)
	movq	(%rax), %rax
	movq	%rax, (%rsi)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	call	_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_@PLT
	testq	%rax, %rax
	je	.L185
	movq	24(%rbx), %rsi
	movq	(%rax), %rax
	movq	%rax, (%rsi)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17867:
	.size	_ZN2v88internal24IncrementalStringBuilder10AccumulateENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal24IncrementalStringBuilder10AccumulateENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal24IncrementalStringBuilder6ExtendEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24IncrementalStringBuilder6ExtendEv
	.type	_ZN2v88internal24IncrementalStringBuilder6ExtendEv, @function
_ZN2v88internal24IncrementalStringBuilder6ExtendEv:
.LFB17868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rdx
	movq	24(%rdi), %rsi
	movq	(%rdi), %rdi
	movq	(%rsi), %rcx
	movq	(%rdx), %rax
	movl	11(%rax), %eax
	addl	11(%rcx), %eax
	cmpl	$1073741799, %eax
	jle	.L187
	movb	$1, 12(%rbx)
	leaq	128(%rdi), %rax
.L188:
	movq	(%rax), %rax
	movq	%rax, (%rsi)
	movl	16(%rbx), %esi
	cmpl	$8192, %esi
	jg	.L190
	addl	%esi, %esi
	movl	%esi, 16(%rbx)
.L190:
	movl	8(%rbx), %eax
	movq	(%rbx), %rdi
	xorl	%edx, %edx
	testl	%eax, %eax
	je	.L201
	call	_ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L192
.L193:
	movq	(%rax), %rdx
	movq	32(%rbx), %rax
	movq	%rdx, (%rax)
	movl	$0, 20(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	call	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	jne	.L193
.L192:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L187:
	call	_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_@PLT
	testq	%rax, %rax
	je	.L192
	movq	24(%rbx), %rsi
	jmp	.L188
	.cfi_endproc
.LFE17868:
	.size	_ZN2v88internal24IncrementalStringBuilder6ExtendEv, .-_ZN2v88internal24IncrementalStringBuilder6ExtendEv
	.section	.text._ZN2v88internal24IncrementalStringBuilder6FinishEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24IncrementalStringBuilder6FinishEv
	.type	_ZN2v88internal24IncrementalStringBuilder6FinishEv, @function
_ZN2v88internal24IncrementalStringBuilder6FinishEv:
.LFB17869:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	20(%rdi), %esi
	movq	32(%rdi), %rdi
	call	_ZN2v88internal9SeqString8TruncateENS0_6HandleIS1_EEi@PLT
	movq	(%rax), %rdx
	movq	32(%rbx), %rax
	movq	%rdx, (%rax)
	movq	32(%rbx), %rdx
	movq	24(%rbx), %rsi
	movq	(%rdx), %rax
	movq	(%rsi), %rcx
	movl	11(%rax), %eax
	addl	11(%rcx), %eax
	cmpl	$1073741799, %eax
	jle	.L203
	movq	(%rbx), %rax
	movb	$1, 12(%rbx)
	subq	$-128, %rax
	movq	(%rax), %rax
	movq	%rax, (%rsi)
	cmpb	$0, 12(%rbx)
	jne	.L209
.L206:
	movq	24(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_@PLT
	testq	%rax, %rax
	je	.L210
	movq	24(%rbx), %rsi
	movq	(%rax), %rax
	movq	%rax, (%rsi)
	cmpb	$0, 12(%rbx)
	je	.L206
.L209:
	movq	(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory27NewInvalidStringLengthErrorEv@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L210:
	.cfi_restore_state
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17869:
	.size	_ZN2v88internal24IncrementalStringBuilder6FinishEv, .-_ZN2v88internal24IncrementalStringBuilder6FinishEv
	.section	.text._ZN2v88internal24IncrementalStringBuilder15CanAppendByCopyENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24IncrementalStringBuilder15CanAppendByCopyENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal24IncrementalStringBuilder15CanAppendByCopyENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal24IncrementalStringBuilder15CanAppendByCopyENS0_6HandleINS0_6StringEEE:
.LFB17870:
	.cfi_startproc
	endbr64
	cmpl	$1, 8(%rdi)
	movq	(%rsi), %rax
	jne	.L224
.L212:
	movl	11(%rax), %eax
	cmpl	$16, %eax
	jg	.L214
	movl	16(%rdi), %edx
	subl	20(%rdi), %edx
	cmpl	%eax, %edx
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L213
.L214:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L225
.L213:
	movq	%rax, %rcx
.L217:
	movq	-1(%rcx), %rdx
	movzwl	11(%rdx), %edx
	andw	$9, %dx
	je	.L214
	cmpw	$8, %dx
	je	.L212
	movq	15(%rcx), %rcx
	jmp	.L217
	.cfi_endproc
.LFE17870:
	.size	_ZN2v88internal24IncrementalStringBuilder15CanAppendByCopyENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal24IncrementalStringBuilder15CanAppendByCopyENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal24IncrementalStringBuilder18AppendStringByCopyENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24IncrementalStringBuilder18AppendStringByCopyENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal24IncrementalStringBuilder18AppendStringByCopyENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal24IncrementalStringBuilder18AppendStringByCopyENS0_6HandleINS0_6StringEEE:
.LFB17871:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	32(%r12), %rax
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	(%rsi), %rdi
	movq	(%rax), %rdx
	movslq	20(%r12), %rax
	movl	11(%rdi), %ecx
	leaq	15(%rdx,%rax), %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii@PLT
	movq	(%rbx), %rdx
	movl	20(%r12), %eax
	addl	11(%rdx), %eax
	movl	%eax, 20(%r12)
	cmpl	16(%r12), %eax
	je	.L229
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L229:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal24IncrementalStringBuilder6ExtendEv
	.cfi_endproc
.LFE17871:
	.size	_ZN2v88internal24IncrementalStringBuilder18AppendStringByCopyENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal24IncrementalStringBuilder18AppendStringByCopyENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE:
.LFB17872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	(%rsi), %rdi
	cmpl	$1, 8(%r12)
	jne	.L252
.L231:
	movl	11(%rdi), %ecx
	movslq	20(%r12), %rsi
	movq	32(%r12), %r8
	cmpl	$16, %ecx
	jg	.L239
	movl	16(%r12), %eax
	subl	%esi, %eax
	cmpl	%eax, %ecx
	jge	.L239
	movq	(%r8), %rax
	xorl	%edx, %edx
	leaq	15(%rax,%rsi), %rsi
	call	_ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii@PLT
	movq	0(%r13), %rdx
	movl	20(%r12), %eax
	addl	11(%rdx), %eax
	movl	%eax, 20(%r12)
	cmpl	16(%r12), %eax
	jne	.L230
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal24IncrementalStringBuilder6ExtendEv
	.p2align 4,,10
	.p2align 3
.L254:
	.cfi_restore_state
	movq	23(%rdi), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	je	.L232
.L233:
	movl	20(%r12), %esi
	movq	32(%r12), %r8
.L239:
	movq	%r8, %rdi
	call	_ZN2v88internal9SeqString8TruncateENS0_6HandleIS1_EEi@PLT
	movq	%r12, %rdi
	movq	(%rax), %rdx
	movq	32(%r12), %rax
	movq	%rdx, (%rax)
	movl	$32, 16(%r12)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv
	movq	24(%r12), %rsi
	movq	0(%r13), %rax
	movq	(%rsi), %rdx
	movl	11(%rax), %eax
	addl	11(%rdx), %eax
	cmpl	$1073741799, %eax
	jle	.L244
	movq	(%r12), %rax
	movb	$1, 12(%r12)
	subq	$-128, %rax
.L245:
	movq	(%rax), %rax
	movq	%rax, (%rsi)
.L230:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	movq	(%r12), %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_@PLT
	testq	%rax, %rax
	je	.L253
	movq	24(%r12), %rsi
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L252:
	movq	-1(%rdi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	je	.L254
.L232:
	movq	%rdi, %rdx
.L236:
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	je	.L233
	cmpw	$8, %ax
	je	.L231
	movq	15(%rdx), %rdx
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L253:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17872:
	.size	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal25StringBuilderConcatLengthEiNS0_10FixedArrayEiPb,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal25StringBuilderConcatLengthEiNS0_10FixedArrayEiPb, @function
_GLOBAL__sub_I__ZN2v88internal25StringBuilderConcatLengthEiNS0_10FixedArrayEiPb:
.LFB21618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21618:
	.size	_GLOBAL__sub_I__ZN2v88internal25StringBuilderConcatLengthEiNS0_10FixedArrayEiPb, .-_GLOBAL__sub_I__ZN2v88internal25StringBuilderConcatLengthEiNS0_10FixedArrayEiPb
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal25StringBuilderConcatLengthEiNS0_10FixedArrayEiPb
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
