	.file	"builtins-call.cc"
	.text
	.section	.rodata._ZN2v88internal8Builtins12CallFunctionENS0_19ConvertReceiverModeE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8Builtins12CallFunctionENS0_19ConvertReceiverModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins12CallFunctionENS0_19ConvertReceiverModeE
	.type	_ZN2v88internal8Builtins12CallFunctionENS0_19ConvertReceiverModeE, @function
_ZN2v88internal8Builtins12CallFunctionENS0_19ConvertReceiverModeE:
.LFB18343:
	.cfi_startproc
	endbr64
	cmpl	$1, %esi
	je	.L2
	cmpl	$2, %esi
	je	.L3
	testl	%esi, %esi
	je	.L8
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movl	$5, %esi
	jmp	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$4, %esi
	jmp	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$6, %esi
	jmp	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	.cfi_endproc
.LFE18343:
	.size	_ZN2v88internal8Builtins12CallFunctionENS0_19ConvertReceiverModeE, .-_ZN2v88internal8Builtins12CallFunctionENS0_19ConvertReceiverModeE
	.section	.text._ZN2v88internal8Builtins4CallENS0_19ConvertReceiverModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins4CallENS0_19ConvertReceiverModeE
	.type	_ZN2v88internal8Builtins4CallENS0_19ConvertReceiverModeE, @function
_ZN2v88internal8Builtins4CallENS0_19ConvertReceiverModeE:
.LFB18344:
	.cfi_startproc
	endbr64
	cmpl	$1, %esi
	je	.L10
	cmpl	$2, %esi
	je	.L11
	testl	%esi, %esi
	je	.L15
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movl	$9, %esi
	jmp	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	.p2align 4,,10
	.p2align 3
.L15:
	movl	$8, %esi
	jmp	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$10, %esi
	jmp	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	.cfi_endproc
.LFE18344:
	.size	_ZN2v88internal8Builtins4CallENS0_19ConvertReceiverModeE, .-_ZN2v88internal8Builtins4CallENS0_19ConvertReceiverModeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins12CallFunctionENS0_19ConvertReceiverModeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins12CallFunctionENS0_19ConvertReceiverModeE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins12CallFunctionENS0_19ConvertReceiverModeE:
.LFB22053:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22053:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins12CallFunctionENS0_19ConvertReceiverModeE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins12CallFunctionENS0_19ConvertReceiverModeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins12CallFunctionENS0_19ConvertReceiverModeE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
