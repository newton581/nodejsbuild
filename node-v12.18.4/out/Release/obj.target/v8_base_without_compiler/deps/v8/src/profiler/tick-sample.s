	.file	"tick-sample.cc"
	.text
	.section	.text._ZN2v88internal10TickSample14GetStackSampleEPNS0_7IsolateEPNS_13RegisterStateENS1_17RecordCEntryFrameEPPvmPNS0_21SampleInfoWithContextEbS8_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10TickSample14GetStackSampleEPNS0_7IsolateEPNS_13RegisterStateENS1_17RecordCEntryFrameEPPvmPNS0_21SampleInfoWithContextEbS8_
	.type	_ZN2v88internal10TickSample14GetStackSampleEPNS0_7IsolateEPNS_13RegisterStateENS1_17RecordCEntryFrameEPPvmPNS0_21SampleInfoWithContextEbS8_, @function
_ZN2v88internal10TickSample14GetStackSampleEPNS0_7IsolateEPNS_13RegisterStateENS1_17RecordCEntryFrameEPPvmPNS0_21SampleInfoWithContextEbS8_:
.LFB20506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$1592, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -1576(%rbp)
	movq	24(%rbp), %r13
	movq	%rsi, -1560(%rbp)
	movl	%edx, -1588(%rbp)
	movq	%r9, -1584(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, (%r9)
	movl	12616(%rdi), %eax
	movups	%xmm0, 16(%r9)
	movl	%eax, 8(%r9)
	cmpl	$1, %eax
	je	.L115
	movq	12600(%rdi), %rax
	movq	%rax, -1600(%rbp)
	testq	%rax, %rax
	je	.L115
	movq	(%rsi), %r15
	movq	%r8, %rbx
	testq	%r15, %r15
	je	.L5
	movq	39640(%rdi), %rax
	movq	%r15, %rdx
	subq	48(%rax), %rdx
	cmpq	%rdx, 56(%rax)
	ja	.L117
.L5:
	movq	-1576(%rbp), %rdx
	movq	12608(%rdx), %rax
	testq	%rax, %rax
	je	.L12
	cmpq	12568(%rdx), %rax
	jb	.L118
.L12:
	movq	-1560(%rbp), %rax
	subq	$8, %rsp
	movq	%r15, %rdx
	movq	-1576(%rbp), %rsi
	leaq	-1552(%rbp), %rdi
	movq	16(%rax), %rcx
	pushq	-1600(%rbp)
	movq	24(%rax), %r9
	movq	%rdi, -1568(%rbp)
	movq	8(%rax), %r8
	call	_ZN2v88internal22SafeStackFrameIteratorC1EPNS0_7IsolateEmmmmm@PLT
	movq	-88(%rbp), %rax
	popq	%rcx
	popq	%rsi
	testq	%rax, %rax
	jne	.L119
	movq	-1584(%rbp), %rax
	movq	$0, 24(%rax)
.L17:
	cmpq	$0, -136(%rbp)
	je	.L115
	movl	-96(%rbp), %eax
	movl	-1588(%rbp), %edx
	cmpl	$21, %eax
	sete	%r15b
	cmpl	$3, %eax
	sete	%al
	orl	%eax, %r15d
	movb	%r15b, -1560(%rbp)
	movl	%r15d, %eax
	xorl	%r15d, %r15d
	testl	%edx, %edx
	je	.L120
.L18:
	movq	-1576(%rbp), %rax
	movq	40960(%rax), %rax
	movq	23240(%rax), %rdx
	movq	%rdx, %r14
	movq	-136(%rbp), %rax
	testq	%rax, %rax
	je	.L19
	cmpq	%rbx, %r15
	jnb	.L19
	movq	%r13, %rdx
	movq	%rbx, %r13
	movq	%r12, %rbx
	movq	%rdx, %r12
	.p2align 4,,10
	.p2align 3
.L41:
	testq	%r14, %r14
	je	.L20
	cmpq	32(%rax), %r14
	jnb	.L20
	cmpq	%r15, %r13
	jbe	.L20
	testq	%r12, %r12
	jne	.L23
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L28:
	movq	-136(%rbp), %rax
	cmpq	32(%rax), %r14
	jnb	.L20
	cmpq	%r15, %r13
	jbe	.L20
.L23:
	movq	$0, (%r12,%r15,8)
	movq	(%r14), %rax
	addq	$1, %r15
	movq	%rax, -8(%rbx,%r15,8)
	movq	8(%r14), %r14
	testq	%r14, %r14
	jne	.L28
.L20:
	cmpq	%r13, %r15
	je	.L19
	movq	-136(%rbp), %rdi
	movq	(%rdi), %rax
	call	*(%rax)
	leaq	0(,%r15,8), %rsi
	testb	%al, %al
	je	.L29
	movq	-136(%rbp), %rax
	movq	%rax, %rdi
	testq	%r12, %r12
	jne	.L30
	cmpb	$0, -1560(%rbp)
	jne	.L30
.L31:
	movq	(%rdi), %rax
	movq	%rsi, -1560(%rbp)
	addq	$1, %r15
	call	*8(%rax)
	movq	-1560(%rbp), %rsi
	movl	%eax, %r8d
	movq	-136(%rbp), %rax
	addq	%rbx, %rsi
	cmpl	$12, %r8d
	je	.L121
.L39:
	movq	40(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, (%rsi)
.L40:
	movq	-1568(%rbp), %rdi
	call	_ZN2v88internal22SafeStackFrameIterator7AdvanceEv@PLT
	movb	$0, -1560(%rbp)
	movq	-136(%rbp), %rax
	cmpq	%r15, %r13
	jbe	.L19
	testq	%rax, %rax
	jne	.L41
.L19:
	movq	-1584(%rbp), %rax
	movq	%r15, (%rax)
.L115:
	movl	$1, %eax
.L1:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L122
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	movq	-136(%rbp), %rax
	cmpq	32(%rax), %r14
	jnb	.L20
	cmpq	%r15, %r13
	jbe	.L20
.L26:
	movq	(%r14), %rax
	addq	$1, %r15
	movq	%rax, -8(%rbx,%r15,8)
	movq	8(%r14), %r14
	testq	%r14, %r14
	jne	.L123
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L29:
	movq	-136(%rbp), %rdi
	testq	%r12, %r12
	je	.L31
	movq	$0, (%r12,%rsi)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L121:
	movq	32(%rax), %rdi
	movq	-24(%rdi), %r8
	movq	%r8, %r9
	andl	$3, %r9d
	cmpq	$1, %r9
	jne	.L39
	movq	-32(%rdi), %rdi
	testb	$1, %dil
	jne	.L39
	sarq	$32, %rdi
	addq	%rdi, %r8
	movq	%r8, (%rsi)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L30:
	movq	32(%rax), %rdi
	movq	-8(%rdi), %rdi
	movq	%rdi, %r8
	andl	$3, %r8d
	cmpq	$1, %r8
	je	.L33
.L35:
	xorl	%r8d, %r8d
.L34:
	movq	%rax, %rdi
	testq	%r12, %r12
	je	.L37
	cmpb	$0, -1560(%rbp)
	movq	%r8, (%r12,%rsi)
	je	.L31
.L37:
	movq	-1584(%rbp), %rax
	movq	%r8, 24(%rax)
	jmp	.L31
.L120:
	testb	%al, %al
	je	.L18
	movq	-1576(%rbp), %rax
	movl	$1, %r15d
	movq	12576(%rax), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L18
	movq	-1584(%rbp), %rax
	movq	24(%rax), %rax
	movq	%rax, 0(%r13)
	jmp	.L18
.L118:
	movq	8(%rax), %rax
	movq	-1584(%rbp), %rcx
	movq	%rax, 16(%rcx)
	movq	-1560(%rbp), %rax
	movq	(%rax), %r15
	jmp	.L12
.L33:
	movq	-1576(%rbp), %rax
	movq	39640(%rax), %r8
	movq	96(%r8), %r9
	movq	-136(%rbp), %rax
	cmpq	%r9, %rdi
	jb	.L35
	movq	104(%r8), %rax
	cmpq	%rax, %rdi
	jnb	.L114
	movq	-1576(%rbp), %rax
	leaq	39(%rdi), %r8
	movq	39640(%rax), %r9
	movq	96(%r9), %r10
	movq	-136(%rbp), %rax
	cmpq	%r10, %r8
	jb	.L35
	movq	104(%r9), %rax
	cmpq	%rax, %r8
	jnb	.L114
	movq	39(%rdi), %r8
	movq	-136(%rbp), %rax
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L119:
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L14
.L16:
	xorl	%eax, %eax
.L15:
	movq	-1584(%rbp), %rcx
	movq	%rax, 24(%rcx)
	jmp	.L17
.L14:
	movq	-1576(%rbp), %rdx
	movq	39640(%rdx), %rdx
	movq	96(%rdx), %rcx
	cmpq	%rcx, %rax
	jb	.L16
	movq	104(%rdx), %rdx
	cmpq	%rdx, %rax
	jnb	.L16
	movq	-1576(%rbp), %rcx
	leaq	39(%rax), %rdx
	movq	39640(%rcx), %rcx
	movq	96(%rcx), %rsi
	cmpq	%rsi, %rdx
	jb	.L16
	movq	104(%rcx), %rcx
	cmpq	%rcx, %rdx
	jnb	.L16
	movq	39(%rax), %rax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L117:
	movl	_ZZN2v88internal12_GLOBAL__N_115IsNoFrameRegionEmE8patterns(%rip), %eax
	testl	%eax, %eax
	je	.L5
	leaq	12+_ZZN2v88internal12_GLOBAL__N_115IsNoFrameRegionEmE8patterns(%rip), %rcx
	movq	%r12, -1608(%rbp)
	movq	%rcx, -1568(%rbp)
	movq	%r8, -1616(%rbp)
	movq	%r13, -1624(%rbp)
	movl	%eax, %r13d
.L11:
	movq	-1568(%rbp), %rcx
	movl	(%rcx), %eax
	movq	%rcx, %r14
	cmpl	$-1, %eax
	je	.L6
	leaq	-8(%rcx), %rbx
	movslq	%r13d, %r12
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L124:
	movq	%r15, %rdx
	subq	%rsi, %rdx
	xorq	%r15, %rdx
	testq	$-4096, %rdx
	je	.L7
	movl	%r13d, %edx
	addq	%rbx, %rsi
	movq	%r15, %rdi
	subl	%eax, %edx
	movslq	%edx, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	je	.L45
.L9:
	movl	4(%r14), %eax
	addq	$4, %r14
	cmpl	$-1, %eax
	je	.L6
.L10:
	movslq	%eax, %rsi
	testl	%eax, %eax
	jne	.L124
.L7:
	movq	%r15, %rdi
	movq	%r12, %rdx
	subq	%rsi, %rdi
	movq	%rbx, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L9
.L45:
	xorl	%eax, %eax
	jmp	.L1
.L6:
	addq	$28, -1568(%rbp)
	movq	-1568(%rbp), %rax
	movl	-12(%rax), %r13d
	testl	%r13d, %r13d
	jne	.L11
	movq	-1608(%rbp), %r12
	movq	-1616(%rbp), %rbx
	movq	-1624(%rbp), %r13
	jmp	.L5
.L114:
	movq	-136(%rbp), %rax
	jmp	.L35
.L122:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20506:
	.size	_ZN2v88internal10TickSample14GetStackSampleEPNS0_7IsolateEPNS_13RegisterStateENS1_17RecordCEntryFrameEPPvmPNS0_21SampleInfoWithContextEbS8_, .-_ZN2v88internal10TickSample14GetStackSampleEPNS0_7IsolateEPNS_13RegisterStateENS1_17RecordCEntryFrameEPPvmPNS0_21SampleInfoWithContextEbS8_
	.section	.text._ZN2v88internal10TickSample4InitEPNS0_7IsolateERKNS_13RegisterStateENS1_17RecordCEntryFrameEbbNS_4base9TimeDeltaE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10TickSample4InitEPNS0_7IsolateERKNS_13RegisterStateENS1_17RecordCEntryFrameEbbNS_4base9TimeDeltaE
	.type	_ZN2v88internal10TickSample4InitEPNS0_7IsolateERKNS_13RegisterStateENS1_17RecordCEntryFrameEbbNS_4base9TimeDeltaE, @function
_ZN2v88internal10TickSample4InitEPNS0_7IsolateERKNS_13RegisterStateENS1_17RecordCEntryFrameEbbNS_4base9TimeDeltaE:
.LFB20504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	andl	$1, %r8d
	movzbl	%r9b, %r9d
	movl	%ecx, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	leaq	24(%rbx), %rcx
	leaq	-64(%rbp), %rsi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leal	(%r8,%r8), %eax
	movzbl	4113(%rbx), %r8d
	andl	$-3, %r8d
	orl	%eax, %r8d
	leaq	2064(%rbx), %rax
	movb	%r8b, 4113(%rbx)
	movdqu	(%rdx), %xmm0
	movl	$255, %r8d
	movdqu	16(%rdx), %xmm1
	pushq	%rax
	movl	%r10d, %edx
	pushq	%r9
	leaq	-96(%rbp), %r9
	movaps	%xmm0, -64(%rbp)
	movaps	%xmm1, -48(%rbp)
	call	_ZN2v88internal10TickSample14GetStackSampleEPNS0_7IsolateEPNS_13RegisterStateENS1_17RecordCEntryFrameEPPvmPNS0_21SampleInfoWithContextEbS8_
	popq	%rdx
	popq	%rcx
	testb	%al, %al
	je	.L132
	movl	-88(%rbp), %eax
	movq	-80(%rbp), %rdx
	movl	%eax, (%rbx)
	movq	-64(%rbp), %rax
	testq	%rdx, %rdx
	setne	%cl
	movq	%rax, 8(%rbx)
	movq	-96(%rbp), %rax
	movb	%al, 4112(%rbx)
	movzbl	4113(%rbx), %eax
	andl	$-2, %eax
	orl	%ecx, %eax
	movb	%al, 4113(%rbx)
	movq	-72(%rbp), %rax
	movq	%rax, 4104(%rbx)
	testq	%rdx, %rdx
	jne	.L133
	movq	$0, 16(%rbx)
.L129:
	movq	16(%rbp), %rax
	movq	%rax, 4128(%rbx)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%rax, 4120(%rbx)
.L125:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L134
	movq	-8(%rbp), %rbx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	movq	%rdx, 16(%rbx)
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L132:
	movq	$0, 8(%rbx)
	jmp	.L125
.L134:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20504:
	.size	_ZN2v88internal10TickSample4InitEPNS0_7IsolateERKNS_13RegisterStateENS1_17RecordCEntryFrameEbbNS_4base9TimeDeltaE, .-_ZN2v88internal10TickSample4InitEPNS0_7IsolateERKNS_13RegisterStateENS1_17RecordCEntryFrameEbbNS_4base9TimeDeltaE
	.section	.text._ZN2v88internal10TickSample14GetStackSampleEPNS0_7IsolateEPNS_13RegisterStateENS1_17RecordCEntryFrameEPPvmPNS_10SampleInfoEbS8_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10TickSample14GetStackSampleEPNS0_7IsolateEPNS_13RegisterStateENS1_17RecordCEntryFrameEPPvmPNS_10SampleInfoEbS8_
	.type	_ZN2v88internal10TickSample14GetStackSampleEPNS0_7IsolateEPNS_13RegisterStateENS1_17RecordCEntryFrameEPPvmPNS_10SampleInfoEbS8_, @function
_ZN2v88internal10TickSample14GetStackSampleEPNS0_7IsolateEPNS_13RegisterStateENS1_17RecordCEntryFrameEPPvmPNS_10SampleInfoEbS8_:
.LFB20505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%r9, %rbx
	leaq	-64(%rbp), %r9
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	16(%rbp), %eax
	pushq	24(%rbp)
	pushq	%rax
	call	_ZN2v88internal10TickSample14GetStackSampleEPNS0_7IsolateEPNS_13RegisterStateENS1_17RecordCEntryFrameEPPvmPNS0_21SampleInfoWithContextEbS8_
	movq	-64(%rbp), %rdx
	movq	%rdx, (%rbx)
	movl	-56(%rbp), %edx
	movl	%edx, 8(%rbx)
	movq	-48(%rbp), %rdx
	movq	%rdx, 16(%rbx)
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L138
	movq	-8(%rbp), %rbx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L138:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20505:
	.size	_ZN2v88internal10TickSample14GetStackSampleEPNS0_7IsolateEPNS_13RegisterStateENS1_17RecordCEntryFrameEPPvmPNS_10SampleInfoEbS8_, .-_ZN2v88internal10TickSample14GetStackSampleEPNS0_7IsolateEPNS_13RegisterStateENS1_17RecordCEntryFrameEPPvmPNS_10SampleInfoEbS8_
	.section	.rodata._ZNK2v88internal10TickSample5printEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"IDLE"
.LC1:
	.string	"JS"
.LC2:
	.string	"PARSER"
.LC3:
	.string	"BYTECODE_COMPILER"
.LC4:
	.string	"COMPILER"
.LC5:
	.string	"OTHER"
.LC6:
	.string	"EXTERNAL"
.LC7:
	.string	"GC"
.LC8:
	.string	"external_callback_entry"
.LC9:
	.string	"tos"
.LC10:
	.string	"TickSample: at %p\n"
.LC11:
	.string	" - state: %s\n"
.LC12:
	.string	" - pc: %p\n"
.LC13:
	.string	" - stack: (%u frames)\n"
.LC14:
	.string	"    %p\n"
.LC15:
	.string	" - has_external_callback: %d\n"
.LC16:
	.string	" - %s: %p\n"
.LC17:
	.string	" - update_stats: %d\n"
.LC18:
	.string	" - sampling_interval: %ld\n"
.LC19:
	.string	"\n"
	.section	.text._ZNK2v88internal10TickSample5printEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal10TickSample5printEv
	.type	_ZNK2v88internal10TickSample5printEv, @function
_ZNK2v88internal10TickSample5printEv:
.LFB20510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	.LC10(%rip), %rdi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	(%r12), %eax
	leaq	.L142(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal10TickSample5printEv,"a",@progbits
	.align 4
	.align 4
.L142:
	.long	.L149-.L142
	.long	.L148-.L142
	.long	.L153-.L142
	.long	.L146-.L142
	.long	.L145-.L142
	.long	.L144-.L142
	.long	.L143-.L142
	.long	.L141-.L142
	.section	.text._ZNK2v88internal10TickSample5printEv
	.p2align 4,,10
	.p2align 3
.L143:
	leaq	.LC6(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L147:
	xorl	%eax, %eax
	leaq	.LC11(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%r12), %rsi
	xorl	%eax, %eax
	leaq	.LC12(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	xorl	%eax, %eax
	leaq	.LC13(%rip), %rdi
	movzbl	4112(%r12), %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, 4112(%r12)
	je	.L150
	xorl	%ebx, %ebx
	leaq	.LC14(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L151:
	movq	24(%r12,%rbx,8), %rsi
	xorl	%eax, %eax
	movq	%r13, %rdi
	addq	$1, %rbx
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	4112(%r12), %eax
	cmpl	%ebx, %eax
	ja	.L151
.L150:
	movzbl	4113(%r12), %esi
	xorl	%eax, %eax
	leaq	.LC15(%rip), %rdi
	andl	$1, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	leaq	.LC9(%rip), %rax
	testb	$1, 4113(%r12)
	leaq	.LC8(%rip), %rsi
	cmove	%rax, %rsi
	movq	16(%r12), %rdx
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	leaq	.LC17(%rip), %rdi
	xorl	%eax, %eax
	movzbl	4113(%r12), %esi
	shrb	%sil
	andl	$1, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	leaq	4128(%r12), %rdi
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	leaq	.LC18(%rip), %rdi
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	addq	$8, %rsp
	leaq	.LC19(%rip), %rdi
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6PrintFEPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	leaq	.LC0(%rip), %rsi
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L149:
	leaq	.LC1(%rip), %rsi
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L148:
	leaq	.LC7(%rip), %rsi
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L153:
	leaq	.LC2(%rip), %rsi
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L146:
	leaq	.LC3(%rip), %rsi
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L145:
	leaq	.LC4(%rip), %rsi
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L144:
	leaq	.LC5(%rip), %rsi
	jmp	.L147
	.cfi_endproc
.LFE20510:
	.size	_ZNK2v88internal10TickSample5printEv, .-_ZNK2v88internal10TickSample5printEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal10TickSample4InitEPNS0_7IsolateERKNS_13RegisterStateENS1_17RecordCEntryFrameEbbNS_4base9TimeDeltaE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal10TickSample4InitEPNS0_7IsolateERKNS_13RegisterStateENS1_17RecordCEntryFrameEbbNS_4base9TimeDeltaE, @function
_GLOBAL__sub_I__ZN2v88internal10TickSample4InitEPNS0_7IsolateERKNS_13RegisterStateENS1_17RecordCEntryFrameEbbNS_4base9TimeDeltaE:
.LFB25632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25632:
	.size	_GLOBAL__sub_I__ZN2v88internal10TickSample4InitEPNS0_7IsolateERKNS_13RegisterStateENS1_17RecordCEntryFrameEbbNS_4base9TimeDeltaE, .-_GLOBAL__sub_I__ZN2v88internal10TickSample4InitEPNS0_7IsolateERKNS_13RegisterStateENS1_17RecordCEntryFrameEbbNS_4base9TimeDeltaE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal10TickSample4InitEPNS0_7IsolateERKNS_13RegisterStateENS1_17RecordCEntryFrameEbbNS_4base9TimeDeltaE
	.section	.data._ZZN2v88internal12_GLOBAL__N_115IsNoFrameRegionEmE8patterns,"aw"
	.align 32
	.type	_ZZN2v88internal12_GLOBAL__N_115IsNoFrameRegionEmE8patterns, @object
	.size	_ZZN2v88internal12_GLOBAL__N_115IsNoFrameRegionEmE8patterns, 112
_ZZN2v88internal12_GLOBAL__N_115IsNoFrameRegionEmE8patterns:
	.long	4
	.string	"UH\211\345"
	.zero	3
	.long	0
	.long	1
	.long	-1
	.zero	4
	.long	2
	.string	"]\302"
	.zero	5
	.long	0
	.long	1
	.long	-1
	.zero	4
	.long	2
	.string	"]\303"
	.zero	5
	.long	0
	.long	1
	.long	-1
	.zero	4
	.long	0
	.string	""
	.zero	7
	.zero	16
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
