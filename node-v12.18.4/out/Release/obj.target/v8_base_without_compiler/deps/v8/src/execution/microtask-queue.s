	.file	"microtask-queue.cc"
	.text
	.section	.text._ZNK2v88internal14MicrotaskQueue19IsRunningMicrotasksEv,"axG",@progbits,_ZNK2v88internal14MicrotaskQueue19IsRunningMicrotasksEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal14MicrotaskQueue19IsRunningMicrotasksEv
	.type	_ZNK2v88internal14MicrotaskQueue19IsRunningMicrotasksEv, @function
_ZNK2v88internal14MicrotaskQueue19IsRunningMicrotasksEv:
.LFB3581:
	.cfi_startproc
	endbr64
	movzbl	76(%rdi), %eax
	ret
	.cfi_endproc
.LFE3581:
	.size	_ZNK2v88internal14MicrotaskQueue19IsRunningMicrotasksEv, .-_ZNK2v88internal14MicrotaskQueue19IsRunningMicrotasksEv
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5081:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5081:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5082:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5082:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v88internal14MicrotaskQueueD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MicrotaskQueueD2Ev
	.type	_ZN2v88internal14MicrotaskQueueD2Ev, @function
_ZN2v88internal14MicrotaskQueueD2Ev:
.LFB18241:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14MicrotaskQueueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rax
	cmpq	%rdi, %rax
	je	.L6
	movq	56(%rdi), %rdx
	movq	%rdx, 56(%rax)
	movq	56(%rdi), %rax
	movq	48(%rdi), %rdx
	movq	%rdx, 48(%rax)
.L6:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L7
	call	_ZdaPv@PLT
.L7:
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18241:
	.size	_ZN2v88internal14MicrotaskQueueD2Ev, .-_ZN2v88internal14MicrotaskQueueD2Ev
	.globl	_ZN2v88internal14MicrotaskQueueD1Ev
	.set	_ZN2v88internal14MicrotaskQueueD1Ev,_ZN2v88internal14MicrotaskQueueD2Ev
	.section	.text._ZN2v88internal14MicrotaskQueueD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MicrotaskQueueD0Ev
	.type	_ZN2v88internal14MicrotaskQueueD0Ev, @function
_ZN2v88internal14MicrotaskQueueD0Ev:
.LFB18243:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14MicrotaskQueueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rax
	cmpq	%rax, %rdi
	je	.L14
	movq	56(%rdi), %rdx
	movq	%rdx, 56(%rax)
	movq	56(%rdi), %rax
	movq	48(%rdi), %rdx
	movq	%rdx, 48(%rax)
.L14:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L15
	call	_ZdaPv@PLT
.L15:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L16
	call	_ZdlPv@PLT
.L16:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE18243:
	.size	_ZN2v88internal14MicrotaskQueueD0Ev, .-_ZN2v88internal14MicrotaskQueueD0Ev
	.section	.text._ZN2v88internal14MicrotaskQueue16EnqueueMicrotaskEPNS_7IsolateENS_5LocalINS_8FunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MicrotaskQueue16EnqueueMicrotaskEPNS_7IsolateENS_5LocalINS_8FunctionEEE
	.type	_ZN2v88internal14MicrotaskQueue16EnqueueMicrotaskEPNS_7IsolateENS_5LocalINS_8FunctionEEE, @function
_ZN2v88internal14MicrotaskQueue16EnqueueMicrotaskEPNS_7IsolateENS_5LocalINS_8FunctionEEE:
.LFB18245:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	12464(%rsi), %rax
	addl	$1, 41104(%rsi)
	movq	41088(%rsi), %r13
	movq	41096(%rsi), %r14
	movq	39(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L25
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L26:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory15NewCallableTaskENS0_6HandleINS0_10JSReceiverEEENS2_INS0_7ContextEEE@PLT
	movq	8(%rbx), %rcx
	movq	16(%rbx), %r15
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
	cmpq	%r15, %rcx
	je	.L44
	movq	32(%rbx), %r8
	addq	24(%rbx), %rcx
.L35:
	movq	%rcx, %rax
	cqto
	idivq	%r15
	movq	-56(%rbp), %rax
	movq	%rax, (%r8,%rdx,8)
	addq	$1, 8(%rbx)
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r14
	je	.L45
	movq	%r14, 41096(%r12)
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movq	%r13, %rdx
	cmpq	41096(%r12), %r13
	je	.L46
.L27:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L44:
	addq	%r15, %r15
	cmpq	$8, %r15
	jg	.L47
	movl	$64, %edi
	movl	$8, %r15d
.L30:
	call	_Znam@PLT
	movq	8(%rbx), %rcx
	movq	32(%rbx), %r9
	movq	%rax, %r8
	testq	%rcx, %rcx
	jle	.L32
	movq	24(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rsi, %rax
	leaq	(%rcx,%rsi), %r10
	negq	%rax
	leaq	(%r8,%rax,8), %r11
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%rsi, %rax
	cqto
	idivq	%rdi
	movq	(%r9,%rdx,8), %rax
	movq	%rax, (%r11,%rsi,8)
	addq	$1, %rsi
	cmpq	%rsi, %r10
	jne	.L33
.L32:
	testq	%r9, %r9
	je	.L34
	movq	%r9, %rdi
	movq	%r8, -64(%rbp)
	call	_ZdaPv@PLT
	movq	8(%rbx), %rcx
	movq	-64(%rbp), %r8
.L34:
	movq	%r8, 32(%rbx)
	movq	%r15, 16(%rbx)
	movq	$0, 24(%rbx)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L47:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %r15
	jle	.L48
	movq	$-1, %rdi
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L46:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L27
.L48:
	leaq	0(,%r15,8), %rdi
	jmp	.L30
	.cfi_endproc
.LFE18245:
	.size	_ZN2v88internal14MicrotaskQueue16EnqueueMicrotaskEPNS_7IsolateENS_5LocalINS_8FunctionEEE, .-_ZN2v88internal14MicrotaskQueue16EnqueueMicrotaskEPNS_7IsolateENS_5LocalINS_8FunctionEEE
	.section	.text._ZN2v88internal14MicrotaskQueue16EnqueueMicrotaskEPNS_7IsolateEPFvPvES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MicrotaskQueue16EnqueueMicrotaskEPNS_7IsolateEPFvPvES4_
	.type	_ZN2v88internal14MicrotaskQueue16EnqueueMicrotaskEPNS_7IsolateEPFvPvES4_, @function
_ZN2v88internal14MicrotaskQueue16EnqueueMicrotaskEPNS_7IsolateEPFvPvES4_:
.LFB18246:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	xorl	%edx, %edx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rcx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	41088(%r12), %rax
	addl	$1, 41104(%r12)
	movq	41096(%r12), %r13
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE@PLT
	movq	%r14, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory15NewCallbackTaskENS0_6HandleINS0_7ForeignEEES4_@PLT
	movq	8(%rbx), %rcx
	movq	16(%rbx), %r15
	movq	(%rax), %r14
	cmpq	%r15, %rcx
	je	.L66
	movq	32(%rbx), %r8
	addq	24(%rbx), %rcx
.L57:
	movq	%rcx, %rax
	cqto
	idivq	%r15
	movq	-56(%rbp), %rax
	movq	%r14, (%r8,%rdx,8)
	addq	$1, 8(%rbx)
	movq	%rax, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L67
	movq	%r13, 41096(%r12)
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	addq	%r15, %r15
	cmpq	$8, %r15
	jg	.L68
	movl	$64, %edi
	movl	$8, %r15d
.L52:
	call	_Znam@PLT
	movq	8(%rbx), %rcx
	movq	32(%rbx), %r9
	movq	%rax, %r8
	testq	%rcx, %rcx
	jle	.L54
	movq	24(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rsi, %rax
	leaq	(%rcx,%rsi), %r10
	negq	%rax
	leaq	(%r8,%rax,8), %r11
	.p2align 4,,10
	.p2align 3
.L55:
	movq	%rsi, %rax
	cqto
	idivq	%rdi
	movq	(%r9,%rdx,8), %rax
	movq	%rax, (%r11,%rsi,8)
	addq	$1, %rsi
	cmpq	%rsi, %r10
	jne	.L55
.L54:
	testq	%r9, %r9
	je	.L56
	movq	%r9, %rdi
	movq	%r8, -64(%rbp)
	call	_ZdaPv@PLT
	movq	8(%rbx), %rcx
	movq	-64(%rbp), %r8
.L56:
	movq	%r8, 32(%rbx)
	movq	%r15, 16(%rbx)
	movq	$0, 24(%rbx)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L68:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %r15
	jle	.L69
	movq	$-1, %rdi
	jmp	.L52
.L69:
	leaq	0(,%r15,8), %rdi
	jmp	.L52
	.cfi_endproc
.LFE18246:
	.size	_ZN2v88internal14MicrotaskQueue16EnqueueMicrotaskEPNS_7IsolateEPFvPvES4_, .-_ZN2v88internal14MicrotaskQueue16EnqueueMicrotaskEPNS_7IsolateEPFvPvES4_
	.section	.text._ZN2v88internal14MicrotaskQueue26SetUpDefaultMicrotaskQueueEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MicrotaskQueue26SetUpDefaultMicrotaskQueueEPNS0_7IsolateE
	.type	_ZN2v88internal14MicrotaskQueue26SetUpDefaultMicrotaskQueueEPNS0_7IsolateE, @function
_ZN2v88internal14MicrotaskQueue26SetUpDefaultMicrotaskQueueEPNS0_7IsolateE:
.LFB18217:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$104, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN2v88internal14MicrotaskQueueE(%rip), %rdx
	movups	%xmm0, 8(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movq	%rdx, (%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 64(%rax)
	movl	$2, 72(%rax)
	movb	$0, 76(%rax)
	movq	$0, 96(%rax)
	movups	%xmm0, 48(%rax)
	movq	%rax, 41752(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18217:
	.size	_ZN2v88internal14MicrotaskQueue26SetUpDefaultMicrotaskQueueEPNS0_7IsolateE, .-_ZN2v88internal14MicrotaskQueue26SetUpDefaultMicrotaskQueueEPNS0_7IsolateE
	.section	.text._ZN2v88internal14MicrotaskQueue3NewEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MicrotaskQueue3NewEPNS0_7IsolateE
	.type	_ZN2v88internal14MicrotaskQueue3NewEPNS0_7IsolateE, @function
_ZN2v88internal14MicrotaskQueue3NewEPNS0_7IsolateE:
.LFB18220:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$104, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN2v88internal14MicrotaskQueueE(%rip), %rcx
	movq	%rax, (%r12)
	movq	41752(%rbx), %rdx
	movups	%xmm0, 48(%rax)
	movq	%rcx, (%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 64(%rax)
	movl	$2, 72(%rax)
	movb	$0, 76(%rax)
	movq	$0, 96(%rax)
	movups	%xmm0, 8(%rax)
	movups	%xmm0, 80(%rax)
	movq	56(%rdx), %rdx
	movq	48(%rdx), %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 48(%rax)
	movq	48(%rdx), %rcx
	movq	%rax, 56(%rcx)
	movq	(%r12), %rax
	movq	%rax, 48(%rdx)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18220:
	.size	_ZN2v88internal14MicrotaskQueue3NewEPNS0_7IsolateE, .-_ZN2v88internal14MicrotaskQueue3NewEPNS0_7IsolateE
	.section	.text._ZN2v88internal14MicrotaskQueueC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MicrotaskQueueC2Ev
	.type	_ZN2v88internal14MicrotaskQueueC2Ev, @function
_ZN2v88internal14MicrotaskQueueC2Ev:
.LFB18238:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN2v88internal14MicrotaskQueueE(%rip), %rax
	movq	$0, 24(%rdi)
	movq	%rax, (%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 64(%rdi)
	movl	$2, 72(%rdi)
	movb	$0, 76(%rdi)
	movq	$0, 96(%rdi)
	movups	%xmm0, 8(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 80(%rdi)
	ret
	.cfi_endproc
.LFE18238:
	.size	_ZN2v88internal14MicrotaskQueueC2Ev, .-_ZN2v88internal14MicrotaskQueueC2Ev
	.globl	_ZN2v88internal14MicrotaskQueueC1Ev
	.set	_ZN2v88internal14MicrotaskQueueC1Ev,_ZN2v88internal14MicrotaskQueueC2Ev
	.section	.text._ZN2v88internal14MicrotaskQueue20CallEnqueueMicrotaskEPNS0_7IsolateElm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MicrotaskQueue20CallEnqueueMicrotaskEPNS0_7IsolateElm
	.type	_ZN2v88internal14MicrotaskQueue20CallEnqueueMicrotaskEPNS0_7IsolateElm, @function
_ZN2v88internal14MicrotaskQueue20CallEnqueueMicrotaskEPNS0_7IsolateElm:
.LFB18244:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	8(%rsi), %rcx
	movq	16(%rsi), %r12
	cmpq	%r12, %rcx
	je	.L76
	movq	32(%rsi), %r15
	addq	24(%rsi), %rcx
.L77:
	movq	%rcx, %rax
	cqto
	idivq	%r12
	movq	%r14, (%r15,%rdx,8)
	addq	$1, 8(%rbx)
	movq	88(%r13), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	addq	%r12, %r12
	cmpq	$8, %r12
	jg	.L90
	movl	$64, %edi
	movl	$8, %r12d
.L79:
	call	_Znam@PLT
	movq	8(%rbx), %rcx
	movq	32(%rbx), %r8
	movq	%rax, %r15
	testq	%rcx, %rcx
	jle	.L81
	movq	24(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rsi, %rax
	leaq	(%rcx,%rsi), %r10
	negq	%rax
	leaq	(%r15,%rax,8), %r9
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%rsi, %rax
	cqto
	idivq	%rdi
	movq	(%r8,%rdx,8), %rax
	movq	%rax, (%r9,%rsi,8)
	addq	$1, %rsi
	cmpq	%r10, %rsi
	jne	.L82
.L81:
	testq	%r8, %r8
	je	.L83
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	movq	8(%rbx), %rcx
.L83:
	movq	%r15, 32(%rbx)
	movq	%r12, 16(%rbx)
	movq	$0, 24(%rbx)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L90:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %r12
	jle	.L91
	movq	$-1, %rdi
	jmp	.L79
.L91:
	leaq	0(,%r12,8), %rdi
	jmp	.L79
	.cfi_endproc
.LFE18244:
	.size	_ZN2v88internal14MicrotaskQueue20CallEnqueueMicrotaskEPNS0_7IsolateElm, .-_ZN2v88internal14MicrotaskQueue20CallEnqueueMicrotaskEPNS0_7IsolateElm
	.section	.text._ZN2v88internal14MicrotaskQueue16EnqueueMicrotaskENS0_9MicrotaskE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MicrotaskQueue16EnqueueMicrotaskENS0_9MicrotaskE
	.type	_ZN2v88internal14MicrotaskQueue16EnqueueMicrotaskENS0_9MicrotaskE, @function
_ZN2v88internal14MicrotaskQueue16EnqueueMicrotaskENS0_9MicrotaskE:
.LFB18247:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %rcx
	movq	%rdi, %rbx
	movq	16(%rdi), %r12
	cmpq	%r12, %rcx
	je	.L93
	movq	32(%rdi), %r14
	addq	24(%rdi), %rcx
.L94:
	movq	%rcx, %rax
	cqto
	idivq	%r12
	movq	%r13, (%r14,%rdx,8)
	addq	$1, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	addq	%r12, %r12
	cmpq	$8, %r12
	jg	.L107
	movl	$64, %edi
	movl	$8, %r12d
.L96:
	call	_Znam@PLT
	movq	8(%rbx), %rcx
	movq	32(%rbx), %r8
	movq	%rax, %r14
	testq	%rcx, %rcx
	jle	.L98
	movq	24(%rbx), %rdi
	movq	16(%rbx), %rsi
	movq	%rdi, %rax
	leaq	(%rcx,%rdi), %r10
	negq	%rax
	leaq	(%r14,%rax,8), %r9
	.p2align 4,,10
	.p2align 3
.L99:
	movq	%rdi, %rax
	cqto
	idivq	%rsi
	movq	(%r8,%rdx,8), %rax
	movq	%rax, (%r9,%rdi,8)
	addq	$1, %rdi
	cmpq	%r10, %rdi
	jne	.L99
.L98:
	testq	%r8, %r8
	je	.L100
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	movq	8(%rbx), %rcx
.L100:
	movq	%r14, 32(%rbx)
	movq	%r12, 16(%rbx)
	movq	$0, 24(%rbx)
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L107:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %r12
	jle	.L108
	movq	$-1, %rdi
	jmp	.L96
.L108:
	leaq	0(,%r12,8), %rdi
	jmp	.L96
	.cfi_endproc
.LFE18247:
	.size	_ZN2v88internal14MicrotaskQueue16EnqueueMicrotaskENS0_9MicrotaskE, .-_ZN2v88internal14MicrotaskQueue16EnqueueMicrotaskENS0_9MicrotaskE
	.section	.text._ZN2v88internal14MicrotaskQueue17IterateMicrotasksEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MicrotaskQueue17IterateMicrotasksEPNS0_11RootVisitorE
	.type	_ZN2v88internal14MicrotaskQueue17IterateMicrotasksEPNS0_11RootVisitorE, @function
_ZN2v88internal14MicrotaskQueue17IterateMicrotasksEPNS0_11RootVisitorE:
.LFB18256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %rax
	movq	16(%rdi), %rdx
	testq	%rax, %rax
	je	.L110
	movq	%rsi, %r13
	movq	(%rsi), %r9
	movq	24(%rdi), %rsi
	movq	32(%rdi), %rcx
	movq	%r13, %rdi
	addq	%rsi, %rax
	cmpq	%rdx, %rax
	leaq	(%rcx,%rsi,8), %r10
	movl	$16, %esi
	cmovg	%rdx, %rax
	xorl	%edx, %edx
	leaq	(%rcx,%rax,8), %r8
	movq	%r10, %rcx
	call	*16(%r9)
	movl	$0, %edx
	movq	0(%r13), %r9
	movq	%r13, %rdi
	movq	8(%r12), %rax
	addq	24(%r12), %rax
	movl	$16, %esi
	subq	16(%r12), %rax
	movq	32(%r12), %rcx
	cmovs	%rdx, %rax
	leaq	(%rcx,%rax,8), %r8
	call	*16(%r9)
	movq	16(%r12), %rdx
.L110:
	cmpq	$8, %rdx
	jle	.L109
	movq	8(%r12), %rax
	addq	%rax, %rax
	cmpq	%rdx, %rax
	jge	.L109
	movq	%rdx, %rbx
	.p2align 4,,10
	.p2align 3
.L112:
	sarq	%rbx
	cmpq	%rax, %rbx
	jg	.L112
	cmpq	$7, %rbx
	jle	.L119
	cmpq	%rdx, %rbx
	jl	.L129
.L109:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	movl	$64, %edi
	movl	$8, %ebx
.L114:
	call	_Znam@PLT
	movq	8(%r12), %rdi
	movq	32(%r12), %r8
	movq	%rax, %r13
	testq	%rdi, %rdi
	jle	.L116
	movq	24(%r12), %rcx
	movq	16(%r12), %rsi
	movq	%rcx, %rax
	addq	%rcx, %rdi
	negq	%rax
	leaq	0(%r13,%rax,8), %r9
	.p2align 4,,10
	.p2align 3
.L117:
	movq	%rcx, %rax
	cqto
	idivq	%rsi
	movq	(%r8,%rdx,8), %rax
	movq	%rax, (%r9,%rcx,8)
	addq	$1, %rcx
	cmpq	%rdi, %rcx
	jne	.L117
.L116:
	testq	%r8, %r8
	je	.L118
	movq	%r8, %rdi
	call	_ZdaPv@PLT
.L118:
	movq	%r13, 32(%r12)
	movq	%rbx, 16(%r12)
	movq	$0, 24(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L129:
	.cfi_restore_state
	movabsq	$1152921504606846975, %rax
	orq	$-1, %rdi
	cmpq	%rax, %rbx
	jg	.L114
	leaq	0(,%rbx,8), %rdi
	jmp	.L114
	.cfi_endproc
.LFE18256:
	.size	_ZN2v88internal14MicrotaskQueue17IterateMicrotasksEPNS0_11RootVisitorE, .-_ZN2v88internal14MicrotaskQueue17IterateMicrotasksEPNS0_11RootVisitorE
	.section	.text._ZNK2v88internal14MicrotaskQueue31FireMicrotasksCompletedCallbackEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14MicrotaskQueue31FireMicrotasksCompletedCallbackEPNS0_7IsolateE
	.type	_ZNK2v88internal14MicrotaskQueue31FireMicrotasksCompletedCallbackEPNS0_7IsolateE, @function
_ZNK2v88internal14MicrotaskQueue31FireMicrotasksCompletedCallbackEPNS0_7IsolateE:
.LFB18273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	88(%rdi), %rcx
	movq	%rdi, %rbx
	movq	80(%rdi), %r8
	movq	%rcx, %rdi
	subq	%r8, %rdi
	movq	%rdi, %rax
	sarq	$4, %rax
	je	.L151
	movabsq	$576460752303423487, %rdx
	cmpq	%rdx, %rax
	ja	.L152
	call	_Znwm@PLT
	movq	88(%rbx), %rcx
	movq	80(%rbx), %r8
	movq	%rax, %r13
	cmpq	%r8, %rcx
	je	.L141
.L132:
	movq	%r8, %rax
	movq	%r13, %rdx
	.p2align 4,,10
	.p2align 3
.L137:
	movq	(%rax), %rdi
	movq	8(%rax), %rsi
	addq	$16, %rax
	addq	$16, %rdx
	movq	%rdi, -16(%rdx)
	movq	%rsi, -8(%rdx)
	cmpq	%rax, %rcx
	jne	.L137
	subq	%r8, %rcx
	movq	%r13, %rbx
	leaq	0(%r13,%rcx), %r14
	cmpq	%r14, %r13
	je	.L141
	.p2align 4,,10
	.p2align 3
.L139:
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	*(%rbx)
	addq	$16, %rbx
	cmpq	%rbx, %r14
	jne	.L139
	testq	%r13, %r13
	jne	.L141
.L130:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	xorl	%r13d, %r13d
	cmpq	%rcx, %r8
	jne	.L132
	jmp	.L130
.L152:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE18273:
	.size	_ZNK2v88internal14MicrotaskQueue31FireMicrotasksCompletedCallbackEPNS0_7IsolateE, .-_ZNK2v88internal14MicrotaskQueue31FireMicrotasksCompletedCallbackEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateE.part.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"v8.execute"
.LC1:
	.string	"RunMicrotasks"
.LC2:
	.string	"v8"
.LC3:
	.string	"V8.RunMicrotasks"
.LC4:
	.string	"microtask_count"
	.section	.text._ZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateE.part.0, @function
_ZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateE.part.0:
.LFB22599:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-144(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	40(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%rsi), %rax
	addl	$1, 41104(%rsi)
	movq	$0, -160(%rbp)
	movq	%rax, -208(%rbp)
	movq	41096(%rsi), %rax
	movb	$1, 76(%rdi)
	movq	%r15, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v87Isolate31SuppressMicrotaskExecutionScopeC1EPS0_@PLT
	movq	41120(%r14), %rbx
	movq	48(%rbx), %rax
	movq	%rax, -192(%rbp)
	movq	_ZZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateEE28trace_event_unique_atomic161(%rip), %rdx
	testq	%rdx, %rdx
	je	.L206
.L155:
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L207
.L157:
	movq	_ZZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateEE28trace_event_unique_atomic163(%rip), %rdx
	testq	%rdx, %rdx
	je	.L208
.L162:
	movq	$0, -120(%rbp)
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L209
.L164:
	leaq	-160(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal9Execution16TryRunMicrotasksEPNS0_7IsolateEPNS0_14MicrotaskQueueEPNS0_11MaybeHandleINS0_6ObjectEEE@PLT
	movq	%rax, -200(%rbp)
	movl	40(%r12), %eax
	subl	%r13d, %eax
	cmpq	$0, -120(%rbp)
	movl	%eax, %r13d
	jne	.L210
.L165:
	movq	_ZZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateEE28trace_event_unique_atomic169(%rip), %rdx
	testq	%rdx, %rdx
	je	.L211
.L167:
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L212
.L169:
	movq	48(%rbx), %rax
	movq	-192(%rbp), %rcx
	cmpq	%rax, %rcx
	jnb	.L173
	movq	%rcx, %rsi
	movq	%rcx, 48(%rbx)
	subq	%rax, %rsi
	addq	%rsi, 72(%rbx)
.L173:
	movq	%r15, %rdi
	call	_ZN2v87Isolate31SuppressMicrotaskExecutionScopeD1Ev@PLT
	cmpq	$0, -200(%rbp)
	movb	$0, 76(%r12)
	je	.L213
.L174:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal14MicrotaskQueue31FireMicrotasksCompletedCallbackEPNS0_7IsolateE
.L177:
	movq	-208(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-184(%rbp), %rax
	cmpq	41096(%r14), %rax
	je	.L153
	movq	%rax, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L153:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L214
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	cmpq	$0, -160(%rbp)
	jne	.L174
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L176
	call	_ZdaPv@PLT
.L176:
	movq	$0, 32(%r12)
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movl	$-1, %r13d
	movq	$0, 24(%r12)
	movups	%xmm0, 8(%r12)
	call	_ZN2v88internal7Isolate32SetTerminationOnExternalTryCatchEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal14MicrotaskQueue31FireMicrotasksCompletedCallbackEPNS0_7IsolateE
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L212:
	leaq	.LC4(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rdx, -216(%rbp)
	movq	%rax, -152(%rbp)
	movslq	%r13d, %rax
	movb	$3, -161(%rbp)
	movq	%rax, -128(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-216(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r11
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r11
	jne	.L215
.L170:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L171
	movq	(%rdi), %rax
	call	*8(%rax)
.L171:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L169
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L211:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L216
.L168:
	movq	%rdx, _ZZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateEE28trace_event_unique_atomic169(%rip)
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L209:
	leaq	-128(%rbp), %rdi
	leaq	.LC3(%rip), %rcx
	movq	%r14, %rsi
	call	_ZN2v88internal7tracing21CallStatsScopedTracer10InitializeEPNS0_7IsolateEPKhPKc@PLT
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L208:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L217
.L163:
	movq	%rdx, _ZZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateEE28trace_event_unique_atomic163(%rip)
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L207:
	pxor	%xmm0, %xmm0
	movq	%rdx, -200(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-200(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r11
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r11
	jne	.L218
.L158:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L159
	movq	(%rdi), %rax
	call	*8(%rax)
.L159:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L157
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L206:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L219
.L156:
	movq	%rdx, _ZZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateEE28trace_event_unique_atomic161(%rip)
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L210:
	movq	-112(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L165
	leaq	-128(%rbp), %rdi
	call	_ZN2v88internal7tracing21CallStatsScopedTracer16AddEndTraceEventEv@PLT
	jmp	.L165
.L219:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L156
.L218:
	subq	$8, %rsp
	leaq	-80(%rbp), %rax
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC1(%rip), %rcx
	movl	$66, %esi
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r11
	addq	$64, %rsp
	jmp	.L158
.L217:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L163
.L216:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L168
.L215:
	subq	$8, %rsp
	leaq	-80(%rbp), %rax
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC1(%rip), %rcx
	movl	$69, %esi
	pushq	%rax
	leaq	-128(%rbp), %rax
	pushq	%rax
	leaq	-161(%rbp), %rax
	pushq	%rax
	leaq	-152(%rbp), %rax
	pushq	%rax
	pushq	$1
	pushq	$0
	call	*%r11
	addq	$64, %rsp
	jmp	.L170
.L214:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22599:
	.size	_ZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateE.part.0, .-_ZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateE.part.0
	.section	.text._ZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateE
	.type	_ZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateE, @function
_ZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateE:
.LFB18255:
	.cfi_startproc
	endbr64
	cmpq	$0, 8(%rdi)
	je	.L224
	jmp	_ZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateE.part.0
	.p2align 4,,10
	.p2align 3
.L224:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal14MicrotaskQueue31FireMicrotasksCompletedCallbackEPNS0_7IsolateE
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18255:
	.size	_ZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateE, .-_ZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateE
	.section	.text._ZN2v88internal14MicrotaskQueue17PerformCheckpointEPNS_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MicrotaskQueue17PerformCheckpointEPNS_7IsolateE
	.type	_ZN2v88internal14MicrotaskQueue17PerformCheckpointEPNS_7IsolateE, @function
_ZN2v88internal14MicrotaskQueue17PerformCheckpointEPNS_7IsolateE:
.LFB18248:
	.cfi_startproc
	endbr64
	cmpb	$0, 76(%rdi)
	jne	.L227
	movl	64(%rdi), %edx
	testl	%edx, %edx
	jne	.L227
	movl	68(%rdi), %eax
	testl	%eax, %eax
	je	.L230
.L227:
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	cmpq	$0, 8(%rdi)
	je	.L231
	jmp	_ZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateE.part.0
	.p2align 4,,10
	.p2align 3
.L231:
	jmp	_ZNK2v88internal14MicrotaskQueue31FireMicrotasksCompletedCallbackEPNS0_7IsolateE
	.cfi_endproc
.LFE18248:
	.size	_ZN2v88internal14MicrotaskQueue17PerformCheckpointEPNS_7IsolateE, .-_ZN2v88internal14MicrotaskQueue17PerformCheckpointEPNS_7IsolateE
	.section	.text._ZNK2v88internal14MicrotaskQueue3getEl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14MicrotaskQueue3getEl
	.type	_ZNK2v88internal14MicrotaskQueue3getEl, @function
_ZNK2v88internal14MicrotaskQueue3getEl:
.LFB18274:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	addq	%rsi, %rax
	cqto
	idivq	16(%rdi)
	movq	32(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	ret
	.cfi_endproc
.LFE18274:
	.size	_ZNK2v88internal14MicrotaskQueue3getEl, .-_ZNK2v88internal14MicrotaskQueue3getEl
	.section	.text._ZN2v88internal14MicrotaskQueue11OnCompletedEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MicrotaskQueue11OnCompletedEPNS0_7IsolateE
	.type	_ZN2v88internal14MicrotaskQueue11OnCompletedEPNS0_7IsolateE, @function
_ZN2v88internal14MicrotaskQueue11OnCompletedEPNS0_7IsolateE:
.LFB18275:
	.cfi_startproc
	endbr64
	jmp	_ZNK2v88internal14MicrotaskQueue31FireMicrotasksCompletedCallbackEPNS0_7IsolateE
	.cfi_endproc
.LFE18275:
	.size	_ZN2v88internal14MicrotaskQueue11OnCompletedEPNS0_7IsolateE, .-_ZN2v88internal14MicrotaskQueue11OnCompletedEPNS0_7IsolateE
	.section	.text._ZN2v88internal14MicrotaskQueue12ResizeBufferEl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MicrotaskQueue12ResizeBufferEl
	.type	_ZN2v88internal14MicrotaskQueue12ResizeBufferEl, @function
_ZN2v88internal14MicrotaskQueue12ResizeBufferEl:
.LFB18276:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	0(,%rsi,8), %rdi
	subq	$8, %rsp
	cmpq	%rax, %rsi
	movq	$-1, %rax
	cmova	%rax, %rdi
	call	_Znam@PLT
	movq	8(%rbx), %r8
	movq	32(%rbx), %r9
	movq	%rax, %r13
	testq	%r8, %r8
	jle	.L237
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdi
	movq	%rcx, %rax
	addq	%rcx, %r8
	negq	%rax
	leaq	0(%r13,%rax,8), %r10
	.p2align 4,,10
	.p2align 3
.L238:
	movq	%rcx, %rax
	cqto
	idivq	%rdi
	movq	(%r9,%rdx,8), %rax
	movq	%rax, (%r10,%rcx,8)
	addq	$1, %rcx
	cmpq	%rcx, %r8
	jne	.L238
.L237:
	testq	%r9, %r9
	je	.L239
	movq	%r9, %rdi
	call	_ZdaPv@PLT
.L239:
	movq	%r13, 32(%rbx)
	movq	%r12, 16(%rbx)
	movq	$0, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18276:
	.size	_ZN2v88internal14MicrotaskQueue12ResizeBufferEl, .-_ZN2v88internal14MicrotaskQueue12ResizeBufferEl
	.section	.rodata._ZNSt6vectorISt4pairIPFvPN2v87IsolateEPvES4_ESaIS7_EE17_M_realloc_insertIJRKS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_.str1.1,"aMS",@progbits,1
.LC5:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorISt4pairIPFvPN2v87IsolateEPvES4_ESaIS7_EE17_M_realloc_insertIJRKS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt4pairIPFvPN2v87IsolateEPvES4_ESaIS7_EE17_M_realloc_insertIJRKS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairIPFvPN2v87IsolateEPvES4_ESaIS7_EE17_M_realloc_insertIJRKS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt4pairIPFvPN2v87IsolateEPvES4_ESaIS7_EE17_M_realloc_insertIJRKS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt4pairIPFvPN2v87IsolateEPvES4_ESaIS7_EE17_M_realloc_insertIJRKS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB21536:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movabsq	$576460752303423487, %rdi
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$4, %rax
	cmpq	%rdi, %rax
	je	.L264
	movq	%rsi, %rcx
	movq	%rsi, %rbx
	subq	%r15, %rcx
	testq	%rax, %rax
	je	.L255
	movabsq	$9223372036854775792, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L265
.L247:
	movq	%rsi, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	%rax, %r14
	movq	-72(%rbp), %rdx
	leaq	(%rax,%rsi), %rax
	leaq	16(%r14), %r8
.L254:
	movq	8(%rdx), %rsi
	movq	(%rdx), %rdx
	addq	%r14, %rcx
	movq	%rdx, (%rcx)
	movq	%rsi, 8(%rcx)
	cmpq	%r15, %rbx
	je	.L249
	movq	%r14, %rcx
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L250:
	movq	(%rdx), %rdi
	movq	8(%rdx), %rsi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	%rdi, -16(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L250
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	16(%r14,%rdx), %r8
.L249:
	cmpq	%r12, %rbx
	je	.L251
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L252:
	movq	8(%rdx), %rsi
	movq	(%rdx), %rdi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	%rdi, -16(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%r12, %rdx
	jne	.L252
	subq	%rbx, %rdx
	addq	%rdx, %r8
.L251:
	testq	%r15, %r15
	je	.L253
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
.L253:
	movq	%r14, %xmm0
	movq	%r8, %xmm1
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L248
	movl	$16, %r8d
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L255:
	movl	$16, %esi
	jmp	.L247
.L248:
	cmpq	%rdi, %r8
	movq	%rdi, %rax
	cmovbe	%r8, %rax
	salq	$4, %rax
	movq	%rax, %rsi
	jmp	.L247
.L264:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE21536:
	.size	_ZNSt6vectorISt4pairIPFvPN2v87IsolateEPvES4_ESaIS7_EE17_M_realloc_insertIJRKS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt4pairIPFvPN2v87IsolateEPvES4_ESaIS7_EE17_M_realloc_insertIJRKS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZN2v88internal14MicrotaskQueue30AddMicrotasksCompletedCallbackEPFvPNS_7IsolateEPvES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MicrotaskQueue30AddMicrotasksCompletedCallbackEPFvPNS_7IsolateEPvES4_
	.type	_ZN2v88internal14MicrotaskQueue30AddMicrotasksCompletedCallbackEPFvPNS_7IsolateEPvES4_, @function
_ZN2v88internal14MicrotaskQueue30AddMicrotasksCompletedCallbackEPFvPNS_7IsolateEPvES4_:
.LFB18258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	88(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -32(%rbp)
	movq	80(%rdi), %rsi
	movq	%r8, %rax
	movq	%rdx, -24(%rbp)
	subq	%rsi, %rax
	movq	%rax, %r9
	sarq	$6, %rax
	sarq	$4, %r9
	testq	%rax, %rax
	jle	.L267
	salq	$6, %rax
	addq	%rsi, %rax
.L273:
	cmpq	(%rsi), %rcx
	je	.L292
.L268:
	cmpq	16(%rsi), %rcx
	je	.L293
.L270:
	cmpq	32(%rsi), %rcx
	je	.L294
.L271:
	cmpq	48(%rsi), %rcx
	je	.L295
.L272:
	addq	$64, %rsi
	cmpq	%rax, %rsi
	jne	.L273
	movq	%r8, %r9
	subq	%rsi, %r9
	sarq	$4, %r9
.L267:
	cmpq	$2, %r9
	je	.L274
	cmpq	$3, %r9
	je	.L275
	cmpq	$1, %r9
	je	.L276
.L277:
	movq	%r8, %rsi
.L280:
	cmpq	%rsi, 96(%rdi)
	je	.L281
	movq	%rcx, (%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 88(%rdi)
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L292:
	cmpq	8(%rsi), %rdx
	jne	.L268
.L269:
	cmpq	%rsi, %r8
	je	.L280
.L266:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L296
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	.cfi_restore_state
	cmpq	24(%rsi), %rdx
	jne	.L270
	addq	$16, %rsi
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L294:
	cmpq	40(%rsi), %rdx
	jne	.L271
	addq	$32, %rsi
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L295:
	cmpq	56(%rsi), %rdx
	jne	.L272
	addq	$48, %rsi
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L281:
	leaq	-32(%rbp), %rdx
	addq	$80, %rdi
	call	_ZNSt6vectorISt4pairIPFvPN2v87IsolateEPvES4_ESaIS7_EE17_M_realloc_insertIJRKS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L275:
	cmpq	(%rsi), %rcx
	je	.L297
.L278:
	addq	$16, %rsi
.L274:
	cmpq	(%rsi), %rcx
	je	.L298
.L279:
	addq	$16, %rsi
.L276:
	cmpq	(%rsi), %rcx
	jne	.L277
	cmpq	8(%rsi), %rdx
	jne	.L277
	jmp	.L269
.L298:
	cmpq	8(%rsi), %rdx
	jne	.L279
	jmp	.L269
.L297:
	cmpq	8(%rsi), %rdx
	jne	.L278
	jmp	.L269
.L296:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18258:
	.size	_ZN2v88internal14MicrotaskQueue30AddMicrotasksCompletedCallbackEPFvPNS_7IsolateEPvES4_, .-_ZN2v88internal14MicrotaskQueue30AddMicrotasksCompletedCallbackEPFvPNS_7IsolateEPvES4_
	.section	.text._ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPSt4pairIPFvPN2v87IsolateEPvES6_ESt6vectorIS9_SaIS9_EEEENS0_5__ops16_Iter_equals_valIKS9_EEET_SJ_SJ_T0_St26random_access_iterator_tag,"axG",@progbits,_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPSt4pairIPFvPN2v87IsolateEPvES6_ESt6vectorIS9_SaIS9_EEEENS0_5__ops16_Iter_equals_valIKS9_EEET_SJ_SJ_T0_St26random_access_iterator_tag,comdat
	.p2align 4
	.weak	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPSt4pairIPFvPN2v87IsolateEPvES6_ESt6vectorIS9_SaIS9_EEEENS0_5__ops16_Iter_equals_valIKS9_EEET_SJ_SJ_T0_St26random_access_iterator_tag
	.type	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPSt4pairIPFvPN2v87IsolateEPvES6_ESt6vectorIS9_SaIS9_EEEENS0_5__ops16_Iter_equals_valIKS9_EEET_SJ_SJ_T0_St26random_access_iterator_tag, @function
_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPSt4pairIPFvPN2v87IsolateEPvES6_ESt6vectorIS9_SaIS9_EEEENS0_5__ops16_Iter_equals_valIKS9_EEET_SJ_SJ_T0_St26random_access_iterator_tag:
.LFB22147:
	.cfi_startproc
	endbr64
	movq	%rsi, %rcx
	subq	%rdi, %rcx
	movq	%rcx, %rax
	sarq	$6, %rcx
	sarq	$4, %rax
	testq	%rcx, %rcx
	jle	.L300
	salq	$6, %rcx
	movq	(%rdx), %rax
	addq	%rdi, %rcx
.L311:
	cmpq	(%rdi), %rax
	je	.L326
.L301:
	cmpq	16(%rdi), %rax
	je	.L327
.L303:
	cmpq	32(%rdi), %rax
	je	.L328
.L306:
	cmpq	48(%rdi), %rax
	je	.L329
.L309:
	addq	$64, %rdi
	cmpq	%rdi, %rcx
	jne	.L311
	movq	%rsi, %rax
	subq	%rdi, %rax
	sarq	$4, %rax
.L300:
	cmpq	$2, %rax
	je	.L312
	cmpq	$3, %rax
	je	.L313
	cmpq	$1, %rax
	je	.L330
.L315:
	movq	%rsi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L326:
	movq	8(%rdx), %r8
	cmpq	%r8, 8(%rdi)
	jne	.L301
.L323:
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	movq	8(%rdx), %r9
	cmpq	%r9, 24(%rdi)
	jne	.L303
	leaq	16(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L328:
	movq	8(%rdx), %r10
	cmpq	%r10, 40(%rdi)
	jne	.L306
	leaq	32(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L329:
	movq	8(%rdx), %r11
	cmpq	%r11, 56(%rdi)
	jne	.L309
	leaq	48(%rdi), %rax
	ret
.L330:
	movq	(%rdx), %rcx
.L321:
	cmpq	%rcx, (%rdi)
	jne	.L315
	movq	8(%rdx), %rax
	cmpq	%rax, 8(%rdi)
	jne	.L315
	jmp	.L323
.L313:
	movq	(%rdx), %rcx
	cmpq	%rcx, (%rdi)
	je	.L331
.L316:
	addq	$16, %rdi
.L318:
	cmpq	%rcx, (%rdi)
	je	.L332
.L319:
	addq	$16, %rdi
	jmp	.L321
.L312:
	movq	(%rdx), %rcx
	jmp	.L318
.L332:
	movq	8(%rdx), %r11
	movq	%rdi, %rax
	cmpq	%r11, 8(%rdi)
	jne	.L319
	ret
.L331:
	movq	8(%rdx), %r11
	movq	%rdi, %rax
	cmpq	%r11, 8(%rdi)
	jne	.L316
	ret
	.cfi_endproc
.LFE22147:
	.size	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPSt4pairIPFvPN2v87IsolateEPvES6_ESt6vectorIS9_SaIS9_EEEENS0_5__ops16_Iter_equals_valIKS9_EEET_SJ_SJ_T0_St26random_access_iterator_tag, .-_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPSt4pairIPFvPN2v87IsolateEPvES6_ESt6vectorIS9_SaIS9_EEEENS0_5__ops16_Iter_equals_valIKS9_EEET_SJ_SJ_T0_St26random_access_iterator_tag
	.section	.text._ZN2v88internal14MicrotaskQueue33RemoveMicrotasksCompletedCallbackEPFvPNS_7IsolateEPvES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MicrotaskQueue33RemoveMicrotasksCompletedCallbackEPFvPNS_7IsolateEPvES4_
	.type	_ZN2v88internal14MicrotaskQueue33RemoveMicrotasksCompletedCallbackEPFvPNS_7IsolateEPvES4_, @function
_ZN2v88internal14MicrotaskQueue33RemoveMicrotasksCompletedCallbackEPFvPNS_7IsolateEPvES4_:
.LFB18272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -48(%rbp)
	movq	88(%rdi), %rsi
	movq	80(%rdi), %rdi
	movq	%rdx, -40(%rbp)
	leaq	-48(%rbp), %rdx
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPSt4pairIPFvPN2v87IsolateEPvES6_ESt6vectorIS9_SaIS9_EEEENS0_5__ops16_Iter_equals_valIKS9_EEET_SJ_SJ_T0_St26random_access_iterator_tag
	movq	88(%rbx), %rcx
	cmpq	%rax, %rcx
	je	.L333
	movq	%rax, %rdx
	addq	$16, %rax
	cmpq	%rax, %rcx
	je	.L336
	movq	%rcx, %rsi
	subq	%rax, %rsi
	movq	%rsi, %rax
	sarq	$4, %rax
	testq	%rsi, %rsi
	jle	.L339
	.p2align 4,,10
	.p2align 3
.L337:
	movq	16(%rdx), %rcx
	addq	$16, %rdx
	movq	%rcx, -16(%rdx)
	movq	8(%rdx), %rcx
	movq	%rcx, -8(%rdx)
	subq	$1, %rax
	jne	.L337
	movq	88(%rbx), %rax
.L336:
	subq	$16, %rax
	movq	%rax, 88(%rbx)
.L333:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L342
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	.cfi_restore_state
	movq	%rcx, %rax
	jmp	.L336
.L342:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18272:
	.size	_ZN2v88internal14MicrotaskQueue33RemoveMicrotasksCompletedCallbackEPFvPNS_7IsolateEPvES4_, .-_ZN2v88internal14MicrotaskQueue33RemoveMicrotasksCompletedCallbackEPFvPNS_7IsolateEPvES4_
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal14MicrotaskQueue17kRingBufferOffsetE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal14MicrotaskQueue17kRingBufferOffsetE, @function
_GLOBAL__sub_I__ZN2v88internal14MicrotaskQueue17kRingBufferOffsetE:
.LFB22550:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22550:
	.size	_GLOBAL__sub_I__ZN2v88internal14MicrotaskQueue17kRingBufferOffsetE, .-_GLOBAL__sub_I__ZN2v88internal14MicrotaskQueue17kRingBufferOffsetE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal14MicrotaskQueue17kRingBufferOffsetE
	.weak	_ZTVN2v88internal14MicrotaskQueueE
	.section	.data.rel.ro.local._ZTVN2v88internal14MicrotaskQueueE,"awG",@progbits,_ZTVN2v88internal14MicrotaskQueueE,comdat
	.align 8
	.type	_ZTVN2v88internal14MicrotaskQueueE, @object
	.size	_ZTVN2v88internal14MicrotaskQueueE, 80
_ZTVN2v88internal14MicrotaskQueueE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14MicrotaskQueueD1Ev
	.quad	_ZN2v88internal14MicrotaskQueueD0Ev
	.quad	_ZN2v88internal14MicrotaskQueue16EnqueueMicrotaskEPNS_7IsolateENS_5LocalINS_8FunctionEEE
	.quad	_ZN2v88internal14MicrotaskQueue16EnqueueMicrotaskEPNS_7IsolateEPFvPvES4_
	.quad	_ZN2v88internal14MicrotaskQueue30AddMicrotasksCompletedCallbackEPFvPNS_7IsolateEPvES4_
	.quad	_ZN2v88internal14MicrotaskQueue33RemoveMicrotasksCompletedCallbackEPFvPNS_7IsolateEPvES4_
	.quad	_ZN2v88internal14MicrotaskQueue17PerformCheckpointEPNS_7IsolateE
	.quad	_ZNK2v88internal14MicrotaskQueue19IsRunningMicrotasksEv
	.section	.bss._ZZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateEE28trace_event_unique_atomic169,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateEE28trace_event_unique_atomic169, @object
	.size	_ZZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateEE28trace_event_unique_atomic169, 8
_ZZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateEE28trace_event_unique_atomic169:
	.zero	8
	.section	.bss._ZZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateEE28trace_event_unique_atomic163,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateEE28trace_event_unique_atomic163, @object
	.size	_ZZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateEE28trace_event_unique_atomic163, 8
_ZZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateEE28trace_event_unique_atomic163:
	.zero	8
	.section	.bss._ZZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateEE28trace_event_unique_atomic161,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateEE28trace_event_unique_atomic161, @object
	.size	_ZZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateEE28trace_event_unique_atomic161, 8
_ZZN2v88internal14MicrotaskQueue13RunMicrotasksEPNS0_7IsolateEE28trace_event_unique_atomic161:
	.zero	8
	.globl	_ZN2v88internal14MicrotaskQueue16kMinimumCapacityE
	.section	.rodata._ZN2v88internal14MicrotaskQueue16kMinimumCapacityE,"a"
	.align 8
	.type	_ZN2v88internal14MicrotaskQueue16kMinimumCapacityE, @object
	.size	_ZN2v88internal14MicrotaskQueue16kMinimumCapacityE, 8
_ZN2v88internal14MicrotaskQueue16kMinimumCapacityE:
	.quad	8
	.globl	_ZN2v88internal14MicrotaskQueue29kFinishedMicrotaskCountOffsetE
	.section	.rodata._ZN2v88internal14MicrotaskQueue29kFinishedMicrotaskCountOffsetE,"a"
	.align 8
	.type	_ZN2v88internal14MicrotaskQueue29kFinishedMicrotaskCountOffsetE, @object
	.size	_ZN2v88internal14MicrotaskQueue29kFinishedMicrotaskCountOffsetE, 8
_ZN2v88internal14MicrotaskQueue29kFinishedMicrotaskCountOffsetE:
	.quad	40
	.globl	_ZN2v88internal14MicrotaskQueue12kStartOffsetE
	.section	.rodata._ZN2v88internal14MicrotaskQueue12kStartOffsetE,"a"
	.align 8
	.type	_ZN2v88internal14MicrotaskQueue12kStartOffsetE, @object
	.size	_ZN2v88internal14MicrotaskQueue12kStartOffsetE, 8
_ZN2v88internal14MicrotaskQueue12kStartOffsetE:
	.quad	24
	.globl	_ZN2v88internal14MicrotaskQueue11kSizeOffsetE
	.section	.rodata._ZN2v88internal14MicrotaskQueue11kSizeOffsetE,"a"
	.align 8
	.type	_ZN2v88internal14MicrotaskQueue11kSizeOffsetE, @object
	.size	_ZN2v88internal14MicrotaskQueue11kSizeOffsetE, 8
_ZN2v88internal14MicrotaskQueue11kSizeOffsetE:
	.quad	8
	.globl	_ZN2v88internal14MicrotaskQueue15kCapacityOffsetE
	.section	.rodata._ZN2v88internal14MicrotaskQueue15kCapacityOffsetE,"a"
	.align 8
	.type	_ZN2v88internal14MicrotaskQueue15kCapacityOffsetE, @object
	.size	_ZN2v88internal14MicrotaskQueue15kCapacityOffsetE, 8
_ZN2v88internal14MicrotaskQueue15kCapacityOffsetE:
	.quad	16
	.globl	_ZN2v88internal14MicrotaskQueue17kRingBufferOffsetE
	.section	.rodata._ZN2v88internal14MicrotaskQueue17kRingBufferOffsetE,"a"
	.align 8
	.type	_ZN2v88internal14MicrotaskQueue17kRingBufferOffsetE, @object
	.size	_ZN2v88internal14MicrotaskQueue17kRingBufferOffsetE, 8
_ZN2v88internal14MicrotaskQueue17kRingBufferOffsetE:
	.quad	32
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
