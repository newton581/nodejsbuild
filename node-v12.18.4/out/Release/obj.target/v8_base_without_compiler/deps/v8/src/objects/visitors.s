	.file	"visitors.cc"
	.text
	.section	.text._ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_,"axG",@progbits,_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.type	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_, @function
_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_:
.LFB6136:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*16(%rax)
	.cfi_endproc
.LFE6136:
	.size	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_, .-_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.section	.text._ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.type	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE, @function
_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE:
.LFB6137:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rdx), %rcx
	movq	16(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE6137:
	.size	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE, .-_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.section	.text._ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE,"axG",@progbits,_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE
	.type	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE, @function
_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE:
.LFB6138:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rdx), %rcx
	movq	24(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE6138:
	.size	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE, .-_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE
	.section	.text._ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE:
.LFB6141:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6141:
	.size	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE:
.LFB6142:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6142:
	.size	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm,"axG",@progbits,_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm
	.type	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm, @function
_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm:
.LFB6143:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6143:
	.size	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm, .-_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm
	.section	.text._ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE:
.LFB6144:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6144:
	.size	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE:
.LFB6145:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6145:
	.size	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal13ObjectVisitor14VisitRelocInfoEPNS0_13RelocIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ObjectVisitor14VisitRelocInfoEPNS0_13RelocIteratorE
	.type	_ZN2v88internal13ObjectVisitor14VisitRelocInfoEPNS0_13RelocIteratorE, @function
_ZN2v88internal13ObjectVisitor14VisitRelocInfoEPNS0_13RelocIteratorE:
.LFB6195:
	.cfi_startproc
	endbr64
	cmpb	$0, 56(%rsi)
	jne	.L24
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	16(%rsi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L12:
	cmpb	$1, %dl
	jle	.L28
	cmpb	$7, %dl
	je	.L29
	leal	-8(%rdx), %ecx
	cmpb	$1, %cl
	ja	.L17
	movq	(%rbx), %rdx
	movq	40(%r15), %rsi
	movq	112(%rdx), %rcx
	cmpq	%r13, %rcx
	jne	.L27
	.p2align 4,,10
	.p2align 3
.L13:
	movq	%r15, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, 56(%r15)
	jne	.L30
.L21:
	movzbl	24(%r15), %edx
	leal	-2(%rdx), %ecx
	cmpb	$1, %cl
	ja	.L12
	movq	(%rbx), %rcx
	movq	40(%r15), %rsi
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	*80(%rcx)
	movq	%r15, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, 56(%r15)
	je	.L21
.L30:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movq	(%rbx), %rcx
	movq	40(%r15), %rsi
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	*72(%rcx)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L29:
	movq	(%rbx), %rdx
	movq	40(%r15), %rsi
	movq	96(%rdx), %rcx
	cmpq	%r14, %rcx
	je	.L13
.L27:
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	*%rcx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L17:
	cmpb	$6, %dl
	je	.L31
	cmpb	$10, %dl
	jne	.L13
	movq	(%rbx), %rdx
	leaq	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE(%rip), %rax
	movq	40(%r15), %rsi
	movq	120(%rdx), %rcx
	cmpq	%rax, %rcx
	je	.L13
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L31:
	movq	(%rbx), %rdx
	leaq	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE(%rip), %rax
	movq	40(%r15), %rsi
	movq	88(%rdx), %rcx
	cmpq	%rax, %rcx
	je	.L13
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE6195:
	.size	_ZN2v88internal13ObjectVisitor14VisitRelocInfoEPNS0_13RelocIteratorE, .-_ZN2v88internal13ObjectVisitor14VisitRelocInfoEPNS0_13RelocIteratorE
	.section	.text._ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.type	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE, @function
_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE:
.LFB6139:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_(%rip), %r9
	leaq	8(%rdx), %rcx
	movq	32(%rax), %r8
	cmpq	%r9, %r8
	jne	.L33
	jmp	*16(%rax)
	.p2align 4,,10
	.p2align 3
.L33:
	jmp	*%r8
	.cfi_endproc
.LFE6139:
	.size	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE, .-_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.section	.text._ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_,"axG",@progbits,_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_
	.type	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_, @function
_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_:
.LFB6140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	leaq	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE(%rip), %rbx
	movq	40(%rax), %rcx
	cmpq	%rbx, %rcx
	jne	.L35
	leaq	8(%rdx), %rcx
	call	*16(%rax)
	movq	(%r12), %rax
	movq	40(%rax), %rcx
	cmpq	%rbx, %rcx
	jne	.L37
.L39:
	popq	%rbx
	leaq	8(%r14), %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	16(%rax), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	call	*%rcx
	movq	(%r12), %rax
	movq	40(%rax), %rcx
	cmpq	%rbx, %rcx
	je	.L39
.L37:
	popq	%rbx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rcx
	.cfi_endproc
.LFE6140:
	.size	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_, .-_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_
	.section	.rodata._ZN2v88internal11RootVisitor8RootNameENS0_4RootE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"(Unknown)"
.LC1:
	.string	"(Internalized strings)"
.LC2:
	.string	"(Read-only roots)"
.LC3:
	.string	"(Strong roots)"
.LC4:
	.string	"(Smi roots)"
.LC5:
	.string	"(Bootstrapper)"
.LC6:
	.string	"(Isolate)"
.LC7:
	.string	"(Relocatable)"
.LC8:
	.string	"(Debugger)"
.LC9:
	.string	"(Compilation cache)"
.LC10:
	.string	"(Handle scope)"
.LC11:
	.string	"(Dispatch table)"
.LC12:
	.string	"(Builtins)"
.LC13:
	.string	"(Global handles)"
.LC14:
	.string	"(Eternal handles)"
.LC15:
	.string	"(Thread manager)"
.LC16:
	.string	"(Extensions)"
.LC17:
	.string	"(Code flusher)"
.LC18:
	.string	"(Partial snapshot cache)"
.LC19:
	.string	"(Read-only object cache)"
.LC20:
	.string	"(Weak collections)"
.LC21:
	.string	"(Wrapper tracing)"
.LC22:
	.string	"(External strings)"
.LC23:
	.string	"unreachable code"
	.section	.text._ZN2v88internal11RootVisitor8RootNameENS0_4RootE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11RootVisitor8RootNameENS0_4RootE
	.type	_ZN2v88internal11RootVisitor8RootNameENS0_4RootE, @function
_ZN2v88internal11RootVisitor8RootNameENS0_4RootE:
.LFB6194:
	.cfi_startproc
	endbr64
	cmpl	$23, %edi
	ja	.L41
	leaq	.L43(%rip), %rdx
	movl	%edi, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11RootVisitor8RootNameENS0_4RootE,"a",@progbits
	.align 4
	.align 4
.L43:
	.long	.L66-.L43
	.long	.L65-.L43
	.long	.L67-.L43
	.long	.L50-.L43
	.long	.L62-.L43
	.long	.L61-.L43
	.long	.L60-.L43
	.long	.L59-.L43
	.long	.L58-.L43
	.long	.L57-.L43
	.long	.L56-.L43
	.long	.L55-.L43
	.long	.L54-.L43
	.long	.L53-.L43
	.long	.L52-.L43
	.long	.L51-.L43
	.long	.L50-.L43
	.long	.L49-.L43
	.long	.L48-.L43
	.long	.L47-.L43
	.long	.L46-.L43
	.long	.L45-.L43
	.long	.L44-.L43
	.long	.L42-.L43
	.section	.text._ZN2v88internal11RootVisitor8RootNameENS0_4RootE
	.p2align 4,,10
	.p2align 3
.L50:
	leaq	.LC3(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	leaq	.LC22(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	leaq	.LC2(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	leaq	.LC18(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	leaq	.LC4(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	leaq	.LC21(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	leaq	.LC8(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	leaq	.LC7(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	leaq	.LC13(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	leaq	.LC20(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	leaq	.LC19(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	leaq	.LC12(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	leaq	.LC17(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	leaq	.LC16(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	leaq	.LC0(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	leaq	.LC6(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	leaq	.LC5(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	leaq	.LC1(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	.LC11(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	leaq	.LC10(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	leaq	.LC9(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	leaq	.LC14(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	leaq	.LC15(%rip), %rax
	ret
.L41:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC23(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE6194:
	.size	_ZN2v88internal11RootVisitor8RootNameENS0_4RootE, .-_ZN2v88internal11RootVisitor8RootNameENS0_4RootE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11RootVisitor8RootNameENS0_4RootE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11RootVisitor8RootNameENS0_4RootE, @function
_GLOBAL__sub_I__ZN2v88internal11RootVisitor8RootNameENS0_4RootE:
.LFB7061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE7061:
	.size	_GLOBAL__sub_I__ZN2v88internal11RootVisitor8RootNameENS0_4RootE, .-_GLOBAL__sub_I__ZN2v88internal11RootVisitor8RootNameENS0_4RootE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11RootVisitor8RootNameENS0_4RootE
	.weak	_ZTVN2v88internal13ObjectVisitorE
	.section	.data.rel.ro._ZTVN2v88internal13ObjectVisitorE,"awG",@progbits,_ZTVN2v88internal13ObjectVisitorE,comdat
	.align 8
	.type	_ZTVN2v88internal13ObjectVisitorE, @object
	.size	_ZTVN2v88internal13ObjectVisitorE, 152
_ZTVN2v88internal13ObjectVisitorE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.quad	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.quad	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE
	.quad	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.quad	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm
	.quad	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor14VisitRelocInfoEPNS0_13RelocIteratorE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
