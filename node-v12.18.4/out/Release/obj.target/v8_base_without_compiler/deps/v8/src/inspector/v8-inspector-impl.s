	.file	"v8-inspector-impl.cc"
	.text
	.section	.text._ZN12v8_inspector17V8InspectorClient13currentTimeMSEv,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv
	.type	_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv, @function
_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv:
.LFB4843:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	ret
	.cfi_endproc
.LFE4843:
	.size	_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv, .-_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv
	.section	.text._ZNSt14_Function_base13_Base_managerIZN12v8_inspector15V8InspectorImpl14contextCreatedERKNS1_13V8ContextInfoEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSA_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector15V8InspectorImpl14contextCreatedERKNS1_13V8ContextInfoEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSA_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN12v8_inspector15V8InspectorImpl14contextCreatedERKNS1_13V8ContextInfoEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSA_St18_Manager_operation:
.LFB11998:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L4
	cmpl	$3, %edx
	je	.L5
	cmpl	$1, %edx
	je	.L9
.L5:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE11998:
	.size	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector15V8InspectorImpl14contextCreatedERKNS1_13V8ContextInfoEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSA_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN12v8_inspector15V8InspectorImpl14contextCreatedERKNS1_13V8ContextInfoEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSA_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN12v8_inspector15V8InspectorImpl16contextCollectedEiiEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector15V8InspectorImpl16contextCollectedEiiEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN12v8_inspector15V8InspectorImpl16contextCollectedEiiEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation:
.LFB12003:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L11
	cmpl	$3, %edx
	je	.L12
	cmpl	$1, %edx
	je	.L16
.L12:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE12003:
	.size	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector15V8InspectorImpl16contextCollectedEiiEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN12v8_inspector15V8InspectorImpl16contextCollectedEiiEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN12v8_inspector15V8InspectorImpl17resetContextGroupEiEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector15V8InspectorImpl17resetContextGroupEiEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN12v8_inspector15V8InspectorImpl17resetContextGroupEiEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation:
.LFB12021:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L18
	cmpl	$3, %edx
	je	.L19
	cmpl	$1, %edx
	je	.L23
.L19:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE12021:
	.size	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector15V8InspectorImpl17resetContextGroupEiEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN12v8_inspector15V8InspectorImpl17resetContextGroupEiEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN12v8_inspector15V8InspectorImpl17resetContextGroupEiEUlPNS1_22V8InspectorSessionImplEE0_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector15V8InspectorImpl17resetContextGroupEiEUlPNS1_22V8InspectorSessionImplEE0_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN12v8_inspector15V8InspectorImpl17resetContextGroupEiEUlPNS1_22V8InspectorSessionImplEE0_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation:
.LFB12025:
	.cfi_startproc
	endbr64
	cmpl	$1, %edx
	je	.L26
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE12025:
	.size	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector15V8InspectorImpl17resetContextGroupEiEUlPNS1_22V8InspectorSessionImplEE0_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN12v8_inspector15V8InspectorImpl17resetContextGroupEiEUlPNS1_22V8InspectorSessionImplEE0_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation
	.section	.text._ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB16853:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE16853:
	.size	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB16870:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE16870:
	.size	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB16855:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE16855:
	.size	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB16869:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE16869:
	.size	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZN12v8_inspector15V8InspectorImpl16createStackTraceEN2v85LocalINS1_10StackTraceEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl16createStackTraceEN2v85LocalINS1_10StackTraceEEE
	.type	_ZN12v8_inspector15V8InspectorImpl16createStackTraceEN2v85LocalINS1_10StackTraceEEE, @function
_ZN12v8_inspector15V8InspectorImpl16createStackTraceEN2v85LocalINS1_10StackTraceEEE:
.LFB8302:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	24(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector10V8Debugger16createStackTraceEN2v85LocalINS1_10StackTraceEEE@PLT
	movq	-32(%rbp), %rax
	movq	%rax, (%r12)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L34
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L34:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8302:
	.size	_ZN12v8_inspector15V8InspectorImpl16createStackTraceEN2v85LocalINS1_10StackTraceEEE, .-_ZN12v8_inspector15V8InspectorImpl16createStackTraceEN2v85LocalINS1_10StackTraceEEE
	.section	.text._ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_15V8InspectorImpl16contextCollectedEiiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_15V8InspectorImpl16contextCollectedEiiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_15V8InspectorImpl16contextCollectedEiiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB12002:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rsi), %rdx
	movq	(%rax), %rsi
	movq	184(%rdx), %rdi
	jmp	_ZN12v8_inspector18V8RuntimeAgentImpl31reportExecutionContextDestroyedEPNS_16InspectedContextE@PLT
	.cfi_endproc
.LFE12002:
	.size	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_15V8InspectorImpl16contextCollectedEiiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_15V8InspectorImpl16contextCollectedEiiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_15V8InspectorImpl17resetContextGroupEiEUlS2_E0_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_15V8InspectorImpl17resetContextGroupEiEUlS2_E0_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_15V8InspectorImpl17resetContextGroupEiEUlS2_E0_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB12024:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rdi
	jmp	_ZN12v8_inspector22V8InspectorSessionImpl5resetEv@PLT
	.cfi_endproc
.LFE12024:
	.size	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_15V8InspectorImpl17resetContextGroupEiEUlS2_E0_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_15V8InspectorImpl17resetContextGroupEiEUlS2_E0_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZN12v8_inspector15V8InspectorImpl11idleStartedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl11idleStartedEv
	.type	_ZN12v8_inspector15V8InspectorImpl11idleStartedEv, @function
_ZN12v8_inspector15V8InspectorImpl11idleStartedEv:
.LFB8365:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movl	$1, %esi
	jmp	_ZN2v87Isolate7SetIdleEb@PLT
	.cfi_endproc
.LFE8365:
	.size	_ZN12v8_inspector15V8InspectorImpl11idleStartedEv, .-_ZN12v8_inspector15V8InspectorImpl11idleStartedEv
	.section	.text._ZN12v8_inspector15V8InspectorImpl12idleFinishedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl12idleFinishedEv
	.type	_ZN12v8_inspector15V8InspectorImpl12idleFinishedEv, @function
_ZN12v8_inspector15V8InspectorImpl12idleFinishedEv:
.LFB8366:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	xorl	%esi, %esi
	jmp	_ZN2v87Isolate7SetIdleEb@PLT
	.cfi_endproc
.LFE8366:
	.size	_ZN12v8_inspector15V8InspectorImpl12idleFinishedEv, .-_ZN12v8_inspector15V8InspectorImpl12idleFinishedEv
	.section	.text._ZN12v8_inspector15V8InspectorImpl17captureStackTraceEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl17captureStackTraceEb
	.type	_ZN12v8_inspector15V8InspectorImpl17captureStackTraceEb, @function
_ZN12v8_inspector15V8InspectorImpl17captureStackTraceEb:
.LFB8373:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%dl, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	24(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector10V8Debugger17captureStackTraceEb@PLT
	movq	-32(%rbp), %rax
	movq	%rax, (%r12)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L42
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L42:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8373:
	.size	_ZN12v8_inspector15V8InspectorImpl17captureStackTraceEb, .-_ZN12v8_inspector15V8InspectorImpl17captureStackTraceEb
	.section	.text._ZN12v8_inspector15V8InspectorImpl22storeCurrentStackTraceERKNS_10StringViewE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl22storeCurrentStackTraceERKNS_10StringViewE
	.type	_ZN12v8_inspector15V8InspectorImpl22storeCurrentStackTraceERKNS_10StringViewE, @function
_ZN12v8_inspector15V8InspectorImpl22storeCurrentStackTraceERKNS_10StringViewE:
.LFB8374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	24(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector10V8Debugger22storeCurrentStackTraceERKNS_10StringViewE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L46
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L46:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8374:
	.size	_ZN12v8_inspector15V8InspectorImpl22storeCurrentStackTraceERKNS_10StringViewE, .-_ZN12v8_inspector15V8InspectorImpl22storeCurrentStackTraceERKNS_10StringViewE
	.section	.text._ZN12v8_inspector15V8InspectorImpl24externalAsyncTaskStartedERKNS_14V8StackTraceIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl24externalAsyncTaskStartedERKNS_14V8StackTraceIdE
	.type	_ZN12v8_inspector15V8InspectorImpl24externalAsyncTaskStartedERKNS_14V8StackTraceIdE, @function
_ZN12v8_inspector15V8InspectorImpl24externalAsyncTaskStartedERKNS_14V8StackTraceIdE:
.LFB8375:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	jmp	_ZN12v8_inspector10V8Debugger24externalAsyncTaskStartedERKNS_14V8StackTraceIdE@PLT
	.cfi_endproc
.LFE8375:
	.size	_ZN12v8_inspector15V8InspectorImpl24externalAsyncTaskStartedERKNS_14V8StackTraceIdE, .-_ZN12v8_inspector15V8InspectorImpl24externalAsyncTaskStartedERKNS_14V8StackTraceIdE
	.section	.text._ZN12v8_inspector15V8InspectorImpl25externalAsyncTaskFinishedERKNS_14V8StackTraceIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl25externalAsyncTaskFinishedERKNS_14V8StackTraceIdE
	.type	_ZN12v8_inspector15V8InspectorImpl25externalAsyncTaskFinishedERKNS_14V8StackTraceIdE, @function
_ZN12v8_inspector15V8InspectorImpl25externalAsyncTaskFinishedERKNS_14V8StackTraceIdE:
.LFB8376:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	jmp	_ZN12v8_inspector10V8Debugger25externalAsyncTaskFinishedERKNS_14V8StackTraceIdE@PLT
	.cfi_endproc
.LFE8376:
	.size	_ZN12v8_inspector15V8InspectorImpl25externalAsyncTaskFinishedERKNS_14V8StackTraceIdE, .-_ZN12v8_inspector15V8InspectorImpl25externalAsyncTaskFinishedERKNS_14V8StackTraceIdE
	.section	.text._ZN12v8_inspector15V8InspectorImpl18asyncTaskScheduledERKNS_10StringViewEPvb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl18asyncTaskScheduledERKNS_10StringViewEPvb
	.type	_ZN12v8_inspector15V8InspectorImpl18asyncTaskScheduledERKNS_10StringViewEPvb, @function
_ZN12v8_inspector15V8InspectorImpl18asyncTaskScheduledERKNS_10StringViewEPvb:
.LFB8377:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L49
	movq	24(%rdi), %rdi
	movzbl	%cl, %ecx
	jmp	_ZN12v8_inspector10V8Debugger18asyncTaskScheduledERKNS_10StringViewEPvb@PLT
	.p2align 4,,10
	.p2align 3
.L49:
	ret
	.cfi_endproc
.LFE8377:
	.size	_ZN12v8_inspector15V8InspectorImpl18asyncTaskScheduledERKNS_10StringViewEPvb, .-_ZN12v8_inspector15V8InspectorImpl18asyncTaskScheduledERKNS_10StringViewEPvb
	.section	.text._ZN12v8_inspector15V8InspectorImpl17asyncTaskCanceledEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl17asyncTaskCanceledEPv
	.type	_ZN12v8_inspector15V8InspectorImpl17asyncTaskCanceledEPv, @function
_ZN12v8_inspector15V8InspectorImpl17asyncTaskCanceledEPv:
.LFB8378:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L51
	movq	24(%rdi), %rdi
	jmp	_ZN12v8_inspector10V8Debugger17asyncTaskCanceledEPv@PLT
	.p2align 4,,10
	.p2align 3
.L51:
	ret
	.cfi_endproc
.LFE8378:
	.size	_ZN12v8_inspector15V8InspectorImpl17asyncTaskCanceledEPv, .-_ZN12v8_inspector15V8InspectorImpl17asyncTaskCanceledEPv
	.section	.text._ZN12v8_inspector15V8InspectorImpl16asyncTaskStartedEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl16asyncTaskStartedEPv
	.type	_ZN12v8_inspector15V8InspectorImpl16asyncTaskStartedEPv, @function
_ZN12v8_inspector15V8InspectorImpl16asyncTaskStartedEPv:
.LFB8379:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L53
	movq	24(%rdi), %rdi
	jmp	_ZN12v8_inspector10V8Debugger16asyncTaskStartedEPv@PLT
	.p2align 4,,10
	.p2align 3
.L53:
	ret
	.cfi_endproc
.LFE8379:
	.size	_ZN12v8_inspector15V8InspectorImpl16asyncTaskStartedEPv, .-_ZN12v8_inspector15V8InspectorImpl16asyncTaskStartedEPv
	.section	.text._ZN12v8_inspector15V8InspectorImpl17asyncTaskFinishedEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl17asyncTaskFinishedEPv
	.type	_ZN12v8_inspector15V8InspectorImpl17asyncTaskFinishedEPv, @function
_ZN12v8_inspector15V8InspectorImpl17asyncTaskFinishedEPv:
.LFB8380:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L55
	movq	24(%rdi), %rdi
	jmp	_ZN12v8_inspector10V8Debugger17asyncTaskFinishedEPv@PLT
	.p2align 4,,10
	.p2align 3
.L55:
	ret
	.cfi_endproc
.LFE8380:
	.size	_ZN12v8_inspector15V8InspectorImpl17asyncTaskFinishedEPv, .-_ZN12v8_inspector15V8InspectorImpl17asyncTaskFinishedEPv
	.section	.text._ZN12v8_inspector15V8InspectorImpl21allAsyncTasksCanceledEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl21allAsyncTasksCanceledEv
	.type	_ZN12v8_inspector15V8InspectorImpl21allAsyncTasksCanceledEv, @function
_ZN12v8_inspector15V8InspectorImpl21allAsyncTasksCanceledEv:
.LFB8381:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	jmp	_ZN12v8_inspector10V8Debugger21allAsyncTasksCanceledEv@PLT
	.cfi_endproc
.LFE8381:
	.size	_ZN12v8_inspector15V8InspectorImpl21allAsyncTasksCanceledEv, .-_ZN12v8_inspector15V8InspectorImpl21allAsyncTasksCanceledEv
	.section	.text._ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB16868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	16(%rdi), %r12
	testq	%r12, %r12
	je	.L58
	movq	%r12, %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE16868:
	.size	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTask3RunEv,"axG",@progbits,_ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTask3RunEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTask3RunEv
	.type	_ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTask3RunEv, @function
_ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTask3RunEv:
.LFB8411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	16(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%rbx), %rax
	cmpb	$0, 40(%rax)
	jne	.L64
	movq	8(%rbx), %rdi
	call	_ZN2v87Isolate18TerminateExecutionEv@PLT
.L64:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE8411:
	.size	_ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTask3RunEv, .-_ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTask3RunEv
	.section	.text._ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_15V8InspectorImpl14contextCreatedERKNS0_13V8ContextInfoEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_15V8InspectorImpl14contextCreatedERKNS0_13V8ContextInfoEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_15V8InspectorImpl14contextCreatedERKNS0_13V8ContextInfoEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB11997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rsi), %r12
	movq	%rdi, %rbx
	movq	(%rdi), %rax
	movq	184(%r12), %rdi
	movq	(%rax), %rsi
	call	_ZN12v8_inspector18V8RuntimeAgentImpl11addBindingsEPNS_16InspectedContextE@PLT
	movq	(%rbx), %rax
	movq	184(%r12), %rdi
	popq	%rbx
	popq	%r12
	movq	(%rax), %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector18V8RuntimeAgentImpl29reportExecutionContextCreatedEPNS_16InspectedContextE@PLT
	.cfi_endproc
.LFE11997:
	.size	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_15V8InspectorImpl14contextCreatedERKNS0_13V8ContextInfoEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_15V8InspectorImpl14contextCreatedERKNS0_13V8ContextInfoEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZNSt8__detail16_Hashtable_allocISaINS_10_Hash_nodeISt4pairIKiSt10unique_ptrISt13unordered_mapIiS4_IN12v8_inspector16InspectedContextESt14default_deleteIS7_EESt4hashIiESt8equal_toIiESaIS2_IS3_SA_EEES8_ISH_EEELb0EEEEE18_M_deallocate_nodeEPSL_.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt8__detail16_Hashtable_allocISaINS_10_Hash_nodeISt4pairIKiSt10unique_ptrISt13unordered_mapIiS4_IN12v8_inspector16InspectedContextESt14default_deleteIS7_EESt4hashIiESt8equal_toIiESaIS2_IS3_SA_EEES8_ISH_EEELb0EEEEE18_M_deallocate_nodeEPSL_.isra.0, @function
_ZNSt8__detail16_Hashtable_allocISaINS_10_Hash_nodeISt4pairIKiSt10unique_ptrISt13unordered_mapIiS4_IN12v8_inspector16InspectedContextESt14default_deleteIS7_EESt4hashIiESt8equal_toIiESaIS2_IS3_SA_EEES8_ISH_EEELb0EEEEE18_M_deallocate_nodeEPSL_.isra.0:
.LFB17137:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	testq	%r14, %r14
	je	.L68
	movq	16(%r14), %r12
	testq	%r12, %r12
	jne	.L72
	.p2align 4,,10
	.p2align 3
.L69:
	movq	8(%r14), %rax
	movq	(%r14), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	(%r14), %rdi
	leaq	48(%r14), %rax
	movq	$0, 24(%r14)
	movq	$0, 16(%r14)
	cmpq	%rax, %rdi
	je	.L73
	call	_ZdlPv@PLT
.L73:
	movl	$56, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L68:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZN12v8_inspector16InspectedContextD1Ev@PLT
	movq	%r15, %rdi
	movl	$272, %esi
	call	_ZdlPvm@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L69
.L71:
	movq	%rbx, %r12
.L72:
	movq	16(%r12), %r15
	movq	(%r12), %rbx
	testq	%r15, %r15
	jne	.L87
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L71
	jmp	.L69
	.cfi_endproc
.LFE17137:
	.size	_ZNSt8__detail16_Hashtable_allocISaINS_10_Hash_nodeISt4pairIKiSt10unique_ptrISt13unordered_mapIiS4_IN12v8_inspector16InspectedContextESt14default_deleteIS7_EESt4hashIiESt8equal_toIiESaIS2_IS3_SA_EEES8_ISH_EEELb0EEEEE18_M_deallocate_nodeEPSL_.isra.0, .-_ZNSt8__detail16_Hashtable_allocISaINS_10_Hash_nodeISt4pairIKiSt10unique_ptrISt13unordered_mapIiS4_IN12v8_inspector16InspectedContextESt14default_deleteIS7_EESt4hashIiESt8equal_toIiESaIS2_IS3_SA_EEES8_ISH_EEELb0EEEEE18_M_deallocate_nodeEPSL_.isra.0
	.section	.text._ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskD2Ev,"axG",@progbits,_ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskD2Ev
	.type	_ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskD2Ev, @function
_ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskD2Ev:
.LFB12105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	24(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L88
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L91
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L97
.L88:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L88
.L97:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L94
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L95:
	cmpl	$1, %eax
	jne	.L88
	movq	(%r12), %rax
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	24(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L95
	.cfi_endproc
.LFE12105:
	.size	_ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskD2Ev, .-_ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskD2Ev
	.weak	_ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskD1Ev
	.set	_ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskD1Ev,_ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskD2Ev
	.section	.text._ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskD0Ev,"axG",@progbits,_ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskD0Ev
	.type	_ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskD0Ev, @function
_ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskD0Ev:
.LFB12107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	24(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L100
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L101
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L107
.L100:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L100
.L107:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L104
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L105:
	cmpl	$1, %eax
	jne	.L100
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L104:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L105
	.cfi_endproc
.LFE12107:
	.size	_ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskD0Ev, .-_ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskD0Ev
	.section	.text._ZN12v8_inspector11V8Inspector6createEPN2v87IsolateEPNS_17V8InspectorClientE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector11V8Inspector6createEPN2v87IsolateEPNS_17V8InspectorClientE
	.type	_ZN12v8_inspector11V8Inspector6createEPN2v87IsolateEPNS_17V8InspectorClientE, @function
_ZN12v8_inspector11V8Inspector6createEPN2v87IsolateEPNS_17V8InspectorClientE:
.LFB8184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movl	$352, %edi
	pushq	%rbx
	.cfi_offset 3, -48
	call	_Znwm@PLT
	movl	$848, %edi
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector15V8InspectorImplE(%rip), %rax
	movq	%r13, 16(%rbx)
	movq	%rax, (%rbx)
	movq	%r14, 8(%rbx)
	call	_Znwm@PLT
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN12v8_inspector10V8DebuggerC1EPN2v87IsolateEPNS_15V8InspectorImplE@PLT
	movq	8(%rbx), %rdi
	movq	%r13, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 48(%rbx)
	call	_ZN2v85debug18GetNextRandomInt64EPNS_7IsolateE@PLT
	movss	.LC1(%rip), %xmm0
	movq	%rbx, %rsi
	movq	$1, 72(%rbx)
	movq	%rax, 56(%rbx)
	leaq	112(%rbx), %rax
	movq	%rax, 64(%rbx)
	leaq	168(%rbx), %rax
	movq	%rax, 120(%rbx)
	leaq	224(%rbx), %rax
	movq	%rax, 176(%rbx)
	leaq	280(%rbx), %rax
	movq	%rax, 232(%rbx)
	leaq	336(%rbx), %rax
	movq	%rax, 288(%rbx)
	movss	%xmm0, 96(%rbx)
	movss	%xmm0, 152(%rbx)
	movss	%xmm0, 208(%rbx)
	movss	%xmm0, 264(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 112(%rbx)
	movq	$1, 128(%rbx)
	movq	$0, 136(%rbx)
	movq	$0, 144(%rbx)
	movq	$0, 160(%rbx)
	movq	$0, 168(%rbx)
	movq	$1, 184(%rbx)
	movq	$0, 192(%rbx)
	movq	$0, 200(%rbx)
	movq	$0, 216(%rbx)
	movq	$0, 224(%rbx)
	movq	$1, 240(%rbx)
	movq	$0, 248(%rbx)
	movq	$0, 256(%rbx)
	movq	$0, 272(%rbx)
	movq	$0, 280(%rbx)
	movq	$1, 296(%rbx)
	movq	$0, 304(%rbx)
	movq	8(%rbx), %rdi
	movss	%xmm0, 320(%rbx)
	pxor	%xmm0, %xmm0
	movq	$0, 312(%rbx)
	movq	$0, 328(%rbx)
	movups	%xmm0, 336(%rbx)
	call	_ZN2v85debug12SetInspectorEPNS_7IsolateEPN12v8_inspector11V8InspectorE@PLT
	movq	344(%rbx), %r13
	testq	%r13, %r13
	je	.L114
.L109:
	movq	8(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v85debug18SetConsoleDelegateEPNS_7IsolateEPNS0_15ConsoleDelegateE@PLT
	movq	%rbx, (%r12)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	movl	$16, %edi
	call	_Znwm@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN12v8_inspector9V8ConsoleC1EPNS_15V8InspectorImplE@PLT
	movq	344(%rbx), %rdi
	movq	%r13, 344(%rbx)
	testq	%rdi, %rdi
	je	.L109
	movq	(%rdi), %rax
	call	*184(%rax)
	movq	344(%rbx), %r13
	jmp	.L109
	.cfi_endproc
.LFE8184:
	.size	_ZN12v8_inspector11V8Inspector6createEPN2v87IsolateEPNS_17V8InspectorClientE, .-_ZN12v8_inspector11V8Inspector6createEPN2v87IsolateEPNS_17V8InspectorClientE
	.section	.text._ZN12v8_inspector15V8InspectorImplC2EPN2v87IsolateEPNS_17V8InspectorClientE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImplC2EPN2v87IsolateEPNS_17V8InspectorClientE
	.type	_ZN12v8_inspector15V8InspectorImplC2EPN2v87IsolateEPNS_17V8InspectorClientE, @function
_ZN12v8_inspector15V8InspectorImplC2EPN2v87IsolateEPNS_17V8InspectorClientE:
.LFB8277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector15V8InspectorImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rsi, 8(%rdi)
	movq	%rdx, 16(%rdi)
	movq	%rax, (%rdi)
	movl	$848, %edi
	call	_Znwm@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN12v8_inspector10V8DebuggerC1EPN2v87IsolateEPNS_15V8InspectorImplE@PLT
	movq	%r12, 24(%rbx)
	movq	8(%rbx), %rdi
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 48(%rbx)
	call	_ZN2v85debug18GetNextRandomInt64EPNS_7IsolateE@PLT
	movss	.LC1(%rip), %xmm0
	movq	%rbx, %rsi
	movq	$1, 72(%rbx)
	movq	%rax, 56(%rbx)
	leaq	112(%rbx), %rax
	movq	%rax, 64(%rbx)
	leaq	168(%rbx), %rax
	movq	%rax, 120(%rbx)
	leaq	224(%rbx), %rax
	movq	%rax, 176(%rbx)
	leaq	280(%rbx), %rax
	movq	%rax, 232(%rbx)
	leaq	336(%rbx), %rax
	movq	%rax, 288(%rbx)
	movss	%xmm0, 96(%rbx)
	movss	%xmm0, 152(%rbx)
	movss	%xmm0, 208(%rbx)
	movss	%xmm0, 264(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 112(%rbx)
	movq	$1, 128(%rbx)
	movq	$0, 136(%rbx)
	movq	$0, 144(%rbx)
	movq	$0, 160(%rbx)
	movq	$0, 168(%rbx)
	movq	$1, 184(%rbx)
	movq	$0, 192(%rbx)
	movq	$0, 200(%rbx)
	movq	$0, 216(%rbx)
	movq	$0, 224(%rbx)
	movq	$1, 240(%rbx)
	movq	$0, 248(%rbx)
	movq	$0, 256(%rbx)
	movq	$0, 272(%rbx)
	movq	$0, 280(%rbx)
	movq	$1, 296(%rbx)
	movq	$0, 304(%rbx)
	movq	8(%rbx), %rdi
	movq	$0, 312(%rbx)
	movq	$0, 328(%rbx)
	movss	%xmm0, 320(%rbx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 336(%rbx)
	call	_ZN2v85debug12SetInspectorEPNS_7IsolateEPN12v8_inspector11V8InspectorE@PLT
	movq	344(%rbx), %r12
	testq	%r12, %r12
	je	.L121
.L116:
	movq	8(%rbx), %rdi
	addq	$8, %rsp
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v85debug18SetConsoleDelegateEPNS_7IsolateEPNS0_15ConsoleDelegateE@PLT
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movl	$16, %edi
	call	_Znwm@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN12v8_inspector9V8ConsoleC1EPNS_15V8InspectorImplE@PLT
	movq	344(%rbx), %rdi
	movq	%r12, 344(%rbx)
	testq	%rdi, %rdi
	je	.L116
	movq	(%rdi), %rax
	call	*184(%rax)
	movq	344(%rbx), %r12
	jmp	.L116
	.cfi_endproc
.LFE8277:
	.size	_ZN12v8_inspector15V8InspectorImplC2EPN2v87IsolateEPNS_17V8InspectorClientE, .-_ZN12v8_inspector15V8InspectorImplC2EPN2v87IsolateEPNS_17V8InspectorClientE
	.globl	_ZN12v8_inspector15V8InspectorImplC1EPN2v87IsolateEPNS_17V8InspectorClientE
	.set	_ZN12v8_inspector15V8InspectorImplC1EPN2v87IsolateEPNS_17V8InspectorClientE,_ZN12v8_inspector15V8InspectorImplC2EPN2v87IsolateEPNS_17V8InspectorClientE
	.section	.text._ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEN2v85LocalINS1_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEN2v85LocalINS1_7ContextEEE
	.type	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEN2v85LocalINS1_7ContextEEE, @function
_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEN2v85LocalINS1_7ContextEEE:
.LFB8283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	296(%rbx), %r9
	xorl	%edx, %edx
	movl	%eax, %esi
	cltq
	divq	%r9
	movq	288(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L127
	movq	(%rax), %rcx
	movq	%rdx, %r8
	movl	8(%rcx), %edi
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L130:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L127
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%r9
	cmpq	%rdx, %r8
	jne	.L127
.L125:
	cmpl	%edi, %esi
	jne	.L130
	movl	12(%rcx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8283:
	.size	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEN2v85LocalINS1_7ContextEEE, .-_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEN2v85LocalINS1_7ContextEEE
	.section	.text._ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi
	.type	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi, @function
_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi:
.LFB8284:
	.cfi_startproc
	endbr64
	movq	296(%rdi), %r8
	movslq	%esi, %rax
	xorl	%edx, %edx
	divq	%r8
	movq	288(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L136
	movq	(%rax), %rcx
	movl	8(%rcx), %edi
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L138:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L136
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L136
.L134:
	cmpl	%esi, %edi
	jne	.L138
	movl	12(%rcx), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8284:
	.size	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi, .-_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi
	.section	.text._ZN12v8_inspector15V8InspectorImpl27compileAndRunInternalScriptEN2v85LocalINS1_7ContextEEENS2_INS1_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl27compileAndRunInternalScriptEN2v85LocalINS1_7ContextEEENS2_INS1_6StringEEE
	.type	_ZN12v8_inspector15V8InspectorImpl27compileAndRunInternalScriptEN2v85LocalINS1_7ContextEEENS2_INS1_6StringEEE, @function
_ZN12v8_inspector15V8InspectorImpl27compileAndRunInternalScriptEN2v85LocalINS1_7ContextEEENS2_INS1_6StringEEE:
.LFB8285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v85debug22CompileInspectorScriptEPNS_7IsolateENS_5LocalINS_6StringEEE@PLT
	testq	%rax, %rax
	je	.L148
	movq	8(%rbx), %rsi
	leaq	-80(%rbp), %r15
	movl	$1, %edx
	movq	%rax, %r13
	movq	%r15, %rdi
	call	_ZN2v815MicrotasksScopeC1EPNS_7IsolateENS0_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	8(%rbx), %rsi
	leaq	-96(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN2v87Isolate23SafeForTerminationScopeC1EPS0_@PLT
	movq	%r13, %rdi
	call	_ZN2v813UnboundScript20BindToCurrentContextEv@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v86Script3RunENS_5LocalINS_7ContextEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	_ZN2v87Isolate23SafeForTerminationScopeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v815MicrotasksScopeD1Ev@PLT
.L142:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L149
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	xorl	%r14d, %r14d
	jmp	.L142
.L149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8285:
	.size	_ZN12v8_inspector15V8InspectorImpl27compileAndRunInternalScriptEN2v85LocalINS1_7ContextEEENS2_INS1_6StringEEE, .-_ZN12v8_inspector15V8InspectorImpl27compileAndRunInternalScriptEN2v85LocalINS1_7ContextEEENS2_INS1_6StringEEE
	.section	.text._ZN12v8_inspector15V8InspectorImpl13compileScriptEN2v85LocalINS1_7ContextEEERKNS_8String16ES7_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl13compileScriptEN2v85LocalINS1_7ContextEEERKNS_8String16ES7_
	.type	_ZN12v8_inspector15V8InspectorImpl13compileScriptEN2v85LocalINS1_7ContextEEERKNS_8String16ES7_, @function
_ZN12v8_inspector15V8InspectorImpl13compileScriptEN2v85LocalINS1_7ContextEEERKNS_8String16ES7_:
.LFB8286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	leaq	-80(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	-96(%rbp), %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%rcx, -176(%rbp)
	movq	8(%rdi), %rdx
	movq	%rdx, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	xorl	%eax, %eax
	movq	%r15, -96(%rbp)
	movq	%rdx, -200(%rbp)
	movq	$0, -88(%rbp)
	movw	%ax, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	8(%rbx), %r12
	xorl	%esi, %esi
	movq	%rax, -168(%rbp)
	movq	%r12, %rdi
	leaq	120(%r12), %rax
	movq	%rax, -208(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, -192(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	-176(%rbp), %r8
	movq	8(%rbx), %rdi
	movq	%rax, -184(%rbp)
	movq	%r8, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-200(%rbp), %rdx
	movq	%rax, -176(%rbp)
	cmpq	$-112, %rdx
	je	.L151
	leaq	112(%rdx), %rdi
	call	_ZNK2v85Value6IsTrueEv@PLT
	testb	%al, %al
	je	.L151
	cmpq	$-120, %r12
	je	.L167
	movq	-208(%rbp), %rdi
	call	_ZNK2v85Value6IsTrueEv@PLT
	movzbl	%al, %eax
	orl	$2, %eax
	movl	%eax, %r12d
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L151:
	cmpq	$-120, %r12
	je	.L168
	movq	-208(%rbp), %rdi
	call	_ZNK2v85Value6IsTrueEv@PLT
	movzbl	%al, %eax
	movl	%eax, %r12d
.L153:
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L154
	call	_ZdlPv@PLT
.L154:
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, -160(%rbp)
	movq	-176(%rbp), %rax
	leaq	-160(%rbp), %rsi
	movl	%r12d, -128(%rbp)
	movq	%rax, -152(%rbp)
	movq	-184(%rbp), %rax
	movq	$0, -112(%rbp)
	movq	%rax, -144(%rbp)
	movq	-192(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	%rax, -136(%rbp)
	movq	-168(%rbp), %rax
	movq	%rax, -120(%rbp)
	call	_ZN2v814ScriptCompiler7CompileENS_5LocalINS_7ContextEEEPNS0_6SourceENS0_14CompileOptionsENS0_13NoCacheReasonE@PLT
	movq	-104(%rbp), %r13
	movq	%rax, %r12
	testq	%r13, %r13
	je	.L155
	movq	%r13, %rdi
	call	_ZN2v814ScriptCompiler10CachedDataD1Ev@PLT
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L155:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L169
	addq	$168, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L167:
	movl	$2, %r12d
	jmp	.L153
.L169:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8286:
	.size	_ZN12v8_inspector15V8InspectorImpl13compileScriptEN2v85LocalINS1_7ContextEEERKNS_8String16ES7_, .-_ZN12v8_inspector15V8InspectorImpl13compileScriptEN2v85LocalINS1_7ContextEEERKNS_8String16ES7_
	.section	.text._ZN12v8_inspector15V8InspectorImpl28enableStackCapturingIfNeededEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl28enableStackCapturingIfNeededEv
	.type	_ZN12v8_inspector15V8InspectorImpl28enableStackCapturingIfNeededEv, @function
_ZN12v8_inspector15V8InspectorImpl28enableStackCapturingIfNeededEv:
.LFB8287:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	40(%rdi), %eax
	testl	%eax, %eax
	je	.L173
	addl	$1, %eax
	movl	%eax, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	movl	$1, %esi
	call	_ZN12v8_inspector16V8StackTraceImpl41setCaptureStackTraceForUncaughtExceptionsEPN2v87IsolateEb@PLT
	movl	40(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8287:
	.size	_ZN12v8_inspector15V8InspectorImpl28enableStackCapturingIfNeededEv, .-_ZN12v8_inspector15V8InspectorImpl28enableStackCapturingIfNeededEv
	.section	.text._ZN12v8_inspector15V8InspectorImpl29disableStackCapturingIfNeededEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl29disableStackCapturingIfNeededEv
	.type	_ZN12v8_inspector15V8InspectorImpl29disableStackCapturingIfNeededEv, @function
_ZN12v8_inspector15V8InspectorImpl29disableStackCapturingIfNeededEv:
.LFB8288:
	.cfi_startproc
	endbr64
	subl	$1, 40(%rdi)
	je	.L176
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	movq	8(%rdi), %rdi
	xorl	%esi, %esi
	jmp	_ZN12v8_inspector16V8StackTraceImpl41setCaptureStackTraceForUncaughtExceptionsEPN2v87IsolateEb@PLT
	.cfi_endproc
.LFE8288:
	.size	_ZN12v8_inspector15V8InspectorImpl29disableStackCapturingIfNeededEv, .-_ZN12v8_inspector15V8InspectorImpl29disableStackCapturingIfNeededEv
	.section	.text._ZN12v8_inspector15V8InspectorImpl24hasConsoleMessageStorageEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl24hasConsoleMessageStorageEi
	.type	_ZN12v8_inspector15V8InspectorImpl24hasConsoleMessageStorageEi, @function
_ZN12v8_inspector15V8InspectorImpl24hasConsoleMessageStorageEi:
.LFB8299:
	.cfi_startproc
	endbr64
	movq	240(%rdi), %r8
	movslq	%esi, %rax
	xorl	%edx, %edx
	divq	%r8
	movq	232(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L182
	movq	(%rax), %rcx
	movl	8(%rcx), %edi
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L184:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L182
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L182
.L179:
	cmpl	%edi, %esi
	jne	.L184
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8299:
	.size	_ZN12v8_inspector15V8InspectorImpl24hasConsoleMessageStorageEi, .-_ZN12v8_inspector15V8InspectorImpl24hasConsoleMessageStorageEi
	.section	.text._ZNK12v8_inspector15V8InspectorImpl10getContextEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector15V8InspectorImpl10getContextEii
	.type	_ZNK12v8_inspector15V8InspectorImpl10getContextEii, @function
_ZNK12v8_inspector15V8InspectorImpl10getContextEii:
.LFB8310:
	.cfi_startproc
	endbr64
	movl	%edx, %ecx
	testl	%esi, %esi
	je	.L193
	testl	%edx, %edx
	je	.L193
	movq	128(%rdi), %r9
	movslq	%esi, %rax
	xorl	%edx, %edx
	divq	%r9
	movq	120(%rdi), %rax
	movq	(%rax,%rdx,8), %r8
	movq	%rdx, %r10
	testq	%r8, %r8
	je	.L185
	movq	(%r8), %r8
	movl	8(%r8), %edi
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L208:
	movq	(%r8), %r8
	testq	%r8, %r8
	je	.L185
	movslq	8(%r8), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%r9
	cmpq	%rdx, %r10
	jne	.L193
.L188:
	cmpl	%edi, %esi
	jne	.L208
	movq	16(%r8), %rdi
	movslq	%ecx, %rax
	xorl	%edx, %edx
	movq	8(%rdi), %rsi
	divq	%rsi
	movq	(%rdi), %rax
	movq	(%rax,%rdx,8), %r8
	movq	%rdx, %r9
	testq	%r8, %r8
	je	.L185
	movq	(%r8), %r8
	movl	8(%r8), %edi
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L209:
	movq	(%r8), %r8
	testq	%r8, %r8
	je	.L185
	movslq	8(%r8), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L193
.L190:
	cmpl	%edi, %ecx
	jne	.L209
	movq	16(%r8), %r8
.L185:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	xorl	%r8d, %r8d
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE8310:
	.size	_ZNK12v8_inspector15V8InspectorImpl10getContextEii, .-_ZNK12v8_inspector15V8InspectorImpl10getContextEii
	.section	.text._ZN12v8_inspector15V8InspectorImpl11contextByIdEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl11contextByIdEi
	.type	_ZN12v8_inspector15V8InspectorImpl11contextByIdEi, @function
_ZN12v8_inspector15V8InspectorImpl11contextByIdEi:
.LFB8314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rax
	movq	296(%rdi), %rsi
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rsi
	movq	288(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L217
	movq	(%rax), %rcx
	movq	%rdx, %r10
	movl	8(%rcx), %r9d
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L222:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L217
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %r9
	divq	%rsi
	cmpq	%rdx, %r10
	jne	.L217
.L213:
	cmpl	%r9d, %r8d
	jne	.L222
	movl	12(%rcx), %esi
.L211:
	movl	%r8d, %edx
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEii
	movq	%rax, %rdi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L218
	call	_ZNK12v8_inspector16InspectedContext7contextEv@PLT
.L218:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L217:
	.cfi_restore_state
	xorl	%esi, %esi
	jmp	.L211
	.cfi_endproc
.LFE8314:
	.size	_ZN12v8_inspector15V8InspectorImpl11contextByIdEi, .-_ZN12v8_inspector15V8InspectorImpl11contextByIdEi
	.section	.text._ZNK12v8_inspector15V8InspectorImpl10getContextEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector15V8InspectorImpl10getContextEi
	.type	_ZNK12v8_inspector15V8InspectorImpl10getContextEi, @function
_ZNK12v8_inspector15V8InspectorImpl10getContextEi:
.LFB8313:
	.cfi_startproc
	endbr64
	movslq	%esi, %rax
	movq	296(%rdi), %rsi
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rsi
	movq	288(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L228
	movq	(%rax), %rcx
	movl	8(%rcx), %r9d
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L230:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L228
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %r9
	divq	%rsi
	cmpq	%rdx, %r10
	jne	.L228
.L226:
	cmpl	%r9d, %r8d
	jne	.L230
	movl	12(%rcx), %esi
	movl	%r8d, %edx
	jmp	_ZNK12v8_inspector15V8InspectorImpl10getContextEii
	.p2align 4,,10
	.p2align 3
.L228:
	xorl	%esi, %esi
	movl	%r8d, %edx
	jmp	_ZNK12v8_inspector15V8InspectorImpl10getContextEii
	.cfi_endproc
.LFE8313:
	.size	_ZNK12v8_inspector15V8InspectorImpl10getContextEi, .-_ZNK12v8_inspector15V8InspectorImpl10getContextEi
	.section	.text._ZN12v8_inspector15V8InspectorImpl12regexContextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl12regexContextEv
	.type	_ZN12v8_inspector15V8InspectorImpl12regexContextEv, @function
_ZN12v8_inspector15V8InspectorImpl12regexContextEv:
.LFB8382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rax
	movq	8(%rdi), %rdi
	testq	%rax, %rax
	je	.L244
.L232:
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
.L235:
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	subq	$8, %rsp
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	pushq	$0
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	call	_ZN2v87Context3NewEPNS_7IsolateEPNS_22ExtensionConfigurationENS_10MaybeLocalINS_14ObjectTemplateEEENS5_INS_5ValueEEENS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE@PLT
	movq	32(%rbx), %rdi
	movq	8(%rbx), %r13
	movq	%rax, %r12
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L233
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 32(%rbx)
	movq	%r12, %rax
	testq	%r12, %r12
	je	.L235
.L234:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	je	.L235
	movq	8(%rbx), %rdi
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L233:
	movq	%r12, %rax
	testq	%r12, %r12
	jne	.L234
	jmp	.L235
	.cfi_endproc
.LFE8382:
	.size	_ZN12v8_inspector15V8InspectorImpl12regexContextEv, .-_ZN12v8_inspector15V8InspectorImpl12regexContextEv
	.section	.text._ZN12v8_inspector15V8InspectorImpl11sessionByIdEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl11sessionByIdEii
	.type	_ZN12v8_inspector15V8InspectorImpl11sessionByIdEii, @function
_ZN12v8_inspector15V8InspectorImpl11sessionByIdEii:
.LFB8384:
	.cfi_startproc
	endbr64
	movq	184(%rdi), %r9
	movl	%edx, %r10d
	movslq	%esi, %rax
	xorl	%edx, %edx
	divq	%r9
	movq	176(%rdi), %rax
	movq	(%rax,%rdx,8), %r8
	movq	%rdx, %r11
	testq	%r8, %r8
	je	.L245
	movq	(%r8), %r8
	movl	8(%r8), %ecx
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L264:
	movq	(%r8), %r8
	testq	%r8, %r8
	je	.L245
	movslq	8(%r8), %rax
	xorl	%edx, %edx
	movq	%rax, %rcx
	divq	%r9
	cmpq	%rdx, %r11
	jne	.L263
.L248:
	cmpl	%ecx, %esi
	jne	.L264
	leaq	24(%r8), %rdx
	movq	32(%r8), %r8
	testq	%r8, %r8
	je	.L245
	movq	%rdx, %rax
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L265:
	movq	%r8, %rax
	movq	16(%r8), %r8
	testq	%r8, %r8
	je	.L249
.L252:
	cmpl	%r10d, 32(%r8)
	jge	.L265
	movq	24(%r8), %r8
	testq	%r8, %r8
	jne	.L252
.L249:
	cmpq	%rax, %rdx
	je	.L245
	cmpl	%r10d, 32(%rax)
	jle	.L266
.L245:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L266:
	movq	40(%rax), %r8
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L263:
	xorl	%r8d, %r8d
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE8384:
	.size	_ZN12v8_inspector15V8InspectorImpl11sessionByIdEii, .-_ZN12v8_inspector15V8InspectorImpl11sessionByIdEii
	.section	.text._ZN12v8_inspector15V8InspectorImpl7consoleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl7consoleEv
	.type	_ZN12v8_inspector15V8InspectorImpl7consoleEv, @function
_ZN12v8_inspector15V8InspectorImpl7consoleEv:
.LFB8385:
	.cfi_startproc
	endbr64
	movq	344(%rdi), %rax
	testq	%rax, %rax
	je	.L276
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$16, %edi
	subq	$24, %rsp
	call	_Znwm@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -24(%rbp)
	call	_ZN12v8_inspector9V8ConsoleC1EPNS_15V8InspectorImplE@PLT
	movq	344(%rbx), %rdi
	movq	-24(%rbp), %rax
	movq	%rax, 344(%rbx)
	testq	%rdi, %rdi
	je	.L267
	movq	(%rdi), %rax
	call	*184(%rax)
	movq	344(%rbx), %rax
.L267:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8385:
	.size	_ZN12v8_inspector15V8InspectorImpl7consoleEv, .-_ZN12v8_inspector15V8InspectorImpl7consoleEv
	.section	.text._ZN12v8_inspector15V8InspectorImpl13EvaluateScopeC2ERKNS_14InjectedScript5ScopeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl13EvaluateScopeC2ERKNS_14InjectedScript5ScopeE
	.type	_ZN12v8_inspector15V8InspectorImpl13EvaluateScopeC2ERKNS_14InjectedScript5ScopeE, @function
_ZN12v8_inspector15V8InspectorImpl13EvaluateScopeC2ERKNS_14InjectedScript5ScopeE:
.LFB8395:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %rax
	movq	%rsi, (%rdi)
	pxor	%xmm0, %xmm0
	addq	$32, %rdi
	movq	8(%rax), %rsi
	movups	%xmm0, -16(%rdi)
	movq	%rsi, -24(%rdi)
	jmp	_ZN2v87Isolate23SafeForTerminationScopeC1EPS0_@PLT
	.cfi_endproc
.LFE8395:
	.size	_ZN12v8_inspector15V8InspectorImpl13EvaluateScopeC2ERKNS_14InjectedScript5ScopeE, .-_ZN12v8_inspector15V8InspectorImpl13EvaluateScopeC2ERKNS_14InjectedScript5ScopeE
	.globl	_ZN12v8_inspector15V8InspectorImpl13EvaluateScopeC1ERKNS_14InjectedScript5ScopeE
	.set	_ZN12v8_inspector15V8InspectorImpl13EvaluateScopeC1ERKNS_14InjectedScript5ScopeE,_ZN12v8_inspector15V8InspectorImpl13EvaluateScopeC2ERKNS_14InjectedScript5ScopeE
	.section	.text._ZN12v8_inspector15V8InspectorImpl13EvaluateScopeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl13EvaluateScopeD2Ev
	.type	_ZN12v8_inspector15V8InspectorImpl13EvaluateScopeD2Ev, @function
_ZN12v8_inspector15V8InspectorImpl13EvaluateScopeD2Ev:
.LFB8398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	leaq	48(%rax), %rdi
	call	_ZNK2v88TryCatch13HasTerminatedEv@PLT
	testb	%al, %al
	jne	.L295
.L279:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L280
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%rbx), %rax
	movb	$1, 40(%rax)
	movq	8(%rbx), %rdi
	call	_ZN2v87Isolate24CancelTerminateExecutionEv@PLT
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L280:
	leaq	32(%rbx), %rdi
	call	_ZN2v87Isolate23SafeForTerminationScopeD1Ev@PLT
	movq	24(%rbx), %r12
	testq	%r12, %r12
	je	.L278
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L283
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L296
.L278:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L278
.L296:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L286
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L287:
	cmpl	$1, %eax
	jne	.L278
	movq	(%r12), %rax
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	24(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L295:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	movq	24(%rax), %rdi
	call	_ZN12v8_inspector10V8Debugger17reportTerminationEv@PLT
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L286:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L287
	.cfi_endproc
.LFE8398:
	.size	_ZN12v8_inspector15V8InspectorImpl13EvaluateScopeD2Ev, .-_ZN12v8_inspector15V8InspectorImpl13EvaluateScopeD2Ev
	.globl	_ZN12v8_inspector15V8InspectorImpl13EvaluateScopeD1Ev
	.set	_ZN12v8_inspector15V8InspectorImpl13EvaluateScopeD1Ev,_ZN12v8_inspector15V8InspectorImpl13EvaluateScopeD2Ev
	.section	.rodata._ZN12v8_inspector15V8InspectorImpl13EvaluateScope10setTimeoutEd.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Execution was terminated"
	.section	.text._ZN12v8_inspector15V8InspectorImpl13EvaluateScope10setTimeoutEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl13EvaluateScope10setTimeoutEd
	.type	_ZN12v8_inspector15V8InspectorImpl13EvaluateScope10setTimeoutEd, @function
_ZN12v8_inspector15V8InspectorImpl13EvaluateScope10setTimeoutEd:
.LFB8412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	8(%rsi), %rdi
	movsd	%xmm0, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate22IsExecutionTerminatingEv@PLT
	testb	%al, %al
	je	.L298
	leaq	-96(%rbp), %r12
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L297
	call	_ZdlPv@PLT
.L297:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L319
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	movl	$48, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %r12
	movups	%xmm0, (%rax)
	movq	%rax, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	call	_ZN2v84base5MutexC1Ev@PLT
	movb	$0, 40(%r12)
	movl	$24, %edi
	call	_Znwm@PLT
	movq	24(%rbx), %r14
	movq	%r12, %xmm0
	movabsq	$4294967297, %rcx
	movq	%rax, %xmm1
	movq	%rcx, 8(%rax)
	leaq	16+_ZTVSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rcx
	punpcklqdq	%xmm1, %xmm0
	movq	%rcx, (%rax)
	movq	%r12, 16(%rax)
	movups	%xmm0, 16(%rbx)
	testq	%r14, %r14
	je	.L302
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r12
	testq	%r12, %r12
	je	.L303
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
.L304:
	cmpl	$1, %eax
	jne	.L302
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%r12, %r12
	je	.L306
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L307:
	cmpl	$1, %eax
	jne	.L302
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L302:
	call	_ZN2v85debug18GetCurrentPlatformEv@PLT
	movq	8(%rbx), %rdx
	movq	16(%rbx), %r15
	movq	%rax, %r12
	movq	24(%rbx), %rbx
	movq	(%rax), %rax
	movq	80(%rax), %r14
	testq	%rbx, %rbx
	je	.L308
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L309
	lock addl	$1, 8(%rbx)
.L308:
	movl	$32, %edi
	movq	%rdx, -128(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rdx
	movq	%r12, %rdi
	leaq	-104(%rbp), %rsi
	leaq	16+_ZTVN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskE(%rip), %rcx
	movq	%r15, 16(%rax)
	movsd	-120(%rbp), %xmm0
	movq	%rcx, (%rax)
	movq	%rdx, 8(%rax)
	movq	%rbx, 24(%rax)
	movq	%rax, -104(%rbp)
	call	*%r14
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L310
	movq	(%rdi), %rax
	call	*8(%rax)
.L310:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L309:
	addl	$1, 8(%rbx)
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L303:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L306:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L307
.L319:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8412:
	.size	_ZN12v8_inspector15V8InspectorImpl13EvaluateScope10setTimeoutEd, .-_ZN12v8_inspector15V8InspectorImpl13EvaluateScope10setTimeoutEd
	.section	.rodata._ZNSt6vectorIiSaIiEE7reserveEm.str1.1,"aMS",@progbits,1
.LC3:
	.string	"vector::reserve"
	.section	.text._ZNSt6vectorIiSaIiEE7reserveEm,"axG",@progbits,_ZNSt6vectorIiSaIiEE7reserveEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIiSaIiEE7reserveEm
	.type	_ZNSt6vectorIiSaIiEE7reserveEm, @function
_ZNSt6vectorIiSaIiEE7reserveEm:
.LFB10546:
	.cfi_startproc
	endbr64
	movabsq	$2305843009213693951, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	%rax, %rsi
	ja	.L332
	movq	(%rdi), %r12
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	subq	%r12, %rax
	sarq	$2, %rax
	cmpq	%rax, %rsi
	ja	.L333
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	.cfi_restore_state
	movq	8(%rdi), %r13
	leaq	0(,%rsi,4), %r14
	subq	%r12, %r13
	testq	%rsi, %rsi
	je	.L327
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	(%rbx), %r12
	movq	8(%rbx), %rdx
	movq	%rax, %r15
	subq	%r12, %rdx
.L323:
	testq	%rdx, %rdx
	jg	.L334
	testq	%r12, %r12
	jne	.L325
.L326:
	addq	%r15, %r13
	addq	%r15, %r14
	movq	%r15, (%rbx)
	movq	%r13, 8(%rbx)
	movq	%r14, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	memmove@PLT
.L325:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L327:
	movq	%r13, %rdx
	xorl	%r15d, %r15d
	jmp	.L323
.L332:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10546:
	.size	_ZNSt6vectorIiSaIiEE7reserveEm, .-_ZNSt6vectorIiSaIiEE7reserveEm
	.section	.rodata._ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_.str1.1,"aMS",@progbits,1
.LC4:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.type	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, @function
_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_:
.LFB12072:
	.cfi_startproc
	endbr64
	movabsq	$2305843009213693951, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$2, %rax
	cmpq	%rcx, %rax
	je	.L349
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L345
	movabsq	$9223372036854775804, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L350
.L337:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L344:
	movl	(%r15), %eax
	subq	%r8, %r13
	leaq	4(%rbx,%rdx), %r15
	movl	%eax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L351
	testq	%r13, %r13
	jg	.L340
	testq	%r9, %r9
	jne	.L343
.L341:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L351:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L340
.L343:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L350:
	testq	%rsi, %rsi
	jne	.L338
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L340:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L341
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L345:
	movl	$4, %r14d
	jmp	.L337
.L349:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L338:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$2, %r14
	jmp	.L337
	.cfi_endproc
.LFE12072:
	.size	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, .-_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.section	.text._ZN12v8_inspector15V8InspectorImpl14forEachSessionEiRKSt8functionIFvPNS_22V8InspectorSessionImplEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl14forEachSessionEiRKSt8functionIFvPNS_22V8InspectorSessionImplEEE
	.type	_ZN12v8_inspector15V8InspectorImpl14forEachSessionEiRKSt8functionIFvPNS_22V8InspectorSessionImplEEE, @function
_ZN12v8_inspector15V8InspectorImpl14forEachSessionEiRKSt8functionIFvPNS_22V8InspectorSessionImplEEE:
.LFB8387:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -112(%rbp)
	xorl	%edx, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	%esi, %rax
	movq	184(%rdi), %rsi
	movq	%rax, -104(%rbp)
	divq	%rsi
	movq	176(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L352
	movq	(%rax), %r13
	movq	%rdx, %rdi
	movl	8(%r13), %ecx
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L402:
	movq	0(%r13), %r13
	testq	%r13, %r13
	je	.L352
	movslq	8(%r13), %rax
	xorl	%edx, %edx
	movq	%rax, %rcx
	divq	%rsi
	cmpq	%rdx, %rdi
	jne	.L352
.L355:
	cmpl	%ecx, %r14d
	jne	.L402
	movq	56(%r13), %rsi
	leaq	-80(%rbp), %r15
	pxor	%xmm0, %xmm0
	addq	$24, %r13
	movq	%r15, %rdi
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZNSt6vectorIiSaIiEE7reserveEm
	movq	16(%r13), %r10
	movq	-72(%rbp), %rbx
	cmpq	%r13, %r10
	je	.L359
	.p2align 4,,10
	.p2align 3
.L360:
	cmpq	-64(%rbp), %rbx
	je	.L356
	movl	32(%r10), %eax
	movq	%r10, %rdi
	movl	%eax, (%rbx)
	movq	-72(%rbp), %rax
	leaq	4(%rax), %rbx
	movq	%rbx, -72(%rbp)
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r10
	cmpq	%rax, %r13
	jne	.L360
	movq	-80(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L376
.L375:
	leaq	-88(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L370:
	movq	184(%r12), %rdi
	movq	-104(%rbp), %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	176(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L362
	movq	(%rax), %rcx
	movl	8(%rcx), %esi
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L403:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L362
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L362
.L364:
	cmpl	%esi, %r14d
	jne	.L403
	movq	32(%rcx), %rax
	leaq	24(%rcx), %rsi
	testq	%rax, %rax
	je	.L362
	movl	(%r15), %edx
	movq	%rsi, %rcx
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L404:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L366
.L365:
	cmpl	%edx, 32(%rax)
	jge	.L404
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L365
.L366:
	cmpq	%rcx, %rsi
	je	.L362
	cmpl	32(%rcx), %edx
	jl	.L362
	movq	40(%rcx), %rax
	movq	%rax, -88(%rbp)
	movq	-112(%rbp), %rax
	cmpq	$0, 16(%rax)
	je	.L405
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	*24(%rax)
.L362:
	addq	$4, %r15
	cmpq	%r15, %rbx
	jne	.L370
	movq	-80(%rbp), %rbx
.L361:
	testq	%rbx, %rbx
	je	.L352
.L376:
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L352:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L406
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
	leaq	32(%r10), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%r10, -120(%rbp)
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movq	-120(%rbp), %r10
	movq	-72(%rbp), %rbx
	movq	%r10, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r10
	cmpq	%rax, %r13
	jne	.L360
.L359:
	movq	-80(%rbp), %r15
	cmpq	%rbx, %r15
	jne	.L375
	jmp	.L361
.L406:
	call	__stack_chk_fail@PLT
.L405:
	call	_ZSt25__throw_bad_function_callv@PLT
	.cfi_endproc
.LFE8387:
	.size	_ZN12v8_inspector15V8InspectorImpl14forEachSessionEiRKSt8functionIFvPNS_22V8InspectorSessionImplEEE, .-_ZN12v8_inspector15V8InspectorImpl14forEachSessionEiRKSt8functionIFvPNS_22V8InspectorSessionImplEEE
	.section	.text._ZN12v8_inspector15V8InspectorImpl14forEachContextEiRKSt8functionIFvPNS_16InspectedContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl14forEachContextEiRKSt8functionIFvPNS_16InspectedContextEEE
	.type	_ZN12v8_inspector15V8InspectorImpl14forEachContextEiRKSt8functionIFvPNS_16InspectedContextEEE, @function
_ZN12v8_inspector15V8InspectorImpl14forEachContextEiRKSt8functionIFvPNS_16InspectedContextEEE:
.LFB8386:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	%esi, %rax
	movq	128(%rdi), %rsi
	movq	%rax, -104(%rbp)
	divq	%rsi
	movq	120(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L407
	movq	(%rax), %r12
	movq	%rdx, %rdi
	movl	8(%r12), %ecx
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L463:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L407
	movslq	8(%r12), %rax
	xorl	%edx, %edx
	movq	%rax, %rcx
	divq	%rsi
	cmpq	%rdx, %rdi
	jne	.L407
.L410:
	cmpl	%ebx, %ecx
	jne	.L463
	movq	$0, -64(%rbp)
	movq	16(%r12), %rax
	leaq	-80(%rbp), %r13
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	movq	24(%rax), %rsi
	movaps	%xmm0, -80(%rbp)
	call	_ZNSt6vectorIiSaIiEE7reserveEm
	movq	16(%r12), %rax
	movq	-72(%rbp), %r14
	movq	-112(%rbp), %r9
	movq	16(%rax), %r12
	testq	%r12, %r12
	je	.L414
	.p2align 4,,10
	.p2align 3
.L415:
	cmpq	%r14, -64(%rbp)
	je	.L411
	movl	8(%r12), %eax
	movl	%eax, (%r14)
	movq	-72(%rbp), %rax
	leaq	4(%rax), %r14
	movq	%r14, -72(%rbp)
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.L415
	movq	-80(%rbp), %r13
	cmpq	%r13, %r14
	je	.L430
.L429:
	leaq	-88(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L422:
	movq	128(%r15), %rsi
	movq	-104(%rbp), %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	120(%r15), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L417
	movq	(%rax), %rcx
	movl	8(%rcx), %edi
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L464:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L417
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%rsi
	cmpq	%rdx, %r10
	jne	.L417
.L419:
	cmpl	%ebx, %edi
	jne	.L464
	movq	16(%rcx), %rcx
	movslq	0(%r13), %rax
	xorl	%edx, %edx
	movq	8(%rcx), %rsi
	movq	%rax, %r11
	divq	%rsi
	movq	(%rcx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L417
	movq	(%rax), %rcx
	movl	8(%rcx), %edi
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L465:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L417
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%rsi
	cmpq	%rdx, %r10
	jne	.L417
.L421:
	cmpl	%edi, %r11d
	jne	.L465
	movq	16(%rcx), %rax
	cmpq	$0, 16(%r9)
	movq	%rax, -88(%rbp)
	je	.L466
	movq	%r9, -112(%rbp)
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	*24(%r9)
	movq	-112(%rbp), %r9
.L417:
	addq	$4, %r13
	cmpq	%r13, %r14
	jne	.L422
	movq	-80(%rbp), %r14
.L416:
	testq	%r14, %r14
	je	.L407
.L430:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L407:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L467
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L411:
	.cfi_restore_state
	leaq	8(%r12), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movq	(%r12), %r12
	movq	-72(%rbp), %r14
	movq	-112(%rbp), %r9
	testq	%r12, %r12
	jne	.L415
.L414:
	movq	-80(%rbp), %r13
	cmpq	%r14, %r13
	jne	.L429
	jmp	.L416
.L466:
	call	_ZSt25__throw_bad_function_callv@PLT
.L467:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8386:
	.size	_ZN12v8_inspector15V8InspectorImpl14forEachContextEiRKSt8functionIFvPNS_16InspectedContextEEE, .-_ZN12v8_inspector15V8InspectorImpl14forEachContextEiRKSt8functionIFvPNS_16InspectedContextEEE
	.section	.text._ZNSt10_HashtableIiSt4pairIKiiESaIS2_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS4_10_Hash_nodeIS2_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiiESaIS2_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS4_10_Hash_nodeIS2_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiiESaIS2_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS4_10_Hash_nodeIS2_Lb0EEEm
	.type	_ZNSt10_HashtableIiSt4pairIKiiESaIS2_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS4_10_Hash_nodeIS2_Lb0EEEm, @function
_ZNSt10_HashtableIiSt4pairIKiiESaIS2_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS4_10_Hash_nodeIS2_Lb0EEEm:
.LFB14132:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L469
	movq	(%rbx), %r8
.L470:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L479
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L480:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L469:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L493
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L494
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L472:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L474
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L476:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L477:
	testq	%rsi, %rsi
	je	.L474
.L475:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movslq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L476
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L482
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L475
	.p2align 4,,10
	.p2align 3
.L474:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L478
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L478:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L479:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L481
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L481:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L482:
	movq	%rdx, %rdi
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L493:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L472
.L494:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE14132:
	.size	_ZNSt10_HashtableIiSt4pairIKiiESaIS2_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS4_10_Hash_nodeIS2_Lb0EEEm, .-_ZNSt10_HashtableIiSt4pairIKiiESaIS2_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS4_10_Hash_nodeIS2_Lb0EEEm
	.section	.text._ZNSt8__detail9_Map_baseIiSt4pairIKiiESaIS3_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_,"axG",@progbits,_ZNSt8__detail9_Map_baseIiSt4pairIKiiESaIS3_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIiSt4pairIKiiESaIS3_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	.type	_ZNSt8__detail9_Map_baseIiSt4pairIKiiESaIS3_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_, @function
_ZNSt8__detail9_Map_baseIiSt4pairIKiiESaIS3_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_:
.LFB11850:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movslq	(%rsi), %r14
	movq	%rsi, %rbx
	movq	8(%rdi), %rdi
	movq	%r14, %rax
	divq	%rdi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r13
	testq	%rax, %rax
	je	.L496
	movq	(%rax), %rcx
	movq	%r14, %r8
	movl	8(%rcx), %esi
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L508:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L496
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%rdi
	cmpq	%rdx, %r13
	jne	.L496
.L498:
	cmpl	%esi, %r8d
	jne	.L508
	popq	%rbx
	leaq	12(%rcx), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L496:
	.cfi_restore_state
	movl	$16, %edi
	call	_Znwm@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	(%rbx), %eax
	movl	$1, %r8d
	movl	$0, 12(%rcx)
	movl	%eax, 8(%rcx)
	call	_ZNSt10_HashtableIiSt4pairIKiiESaIS2_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS4_10_Hash_nodeIS2_Lb0EEEm
	popq	%rbx
	popq	%r12
	addq	$12, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11850:
	.size	_ZNSt8__detail9_Map_baseIiSt4pairIKiiESaIS3_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_, .-_ZNSt8__detail9_Map_baseIiSt4pairIKiiESaIS3_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	.section	.text._ZN12v8_inspector15V8InspectorImpl14muteExceptionsEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl14muteExceptionsEi
	.type	_ZN12v8_inspector15V8InspectorImpl14muteExceptionsEi, @function
_ZN12v8_inspector15V8InspectorImpl14muteExceptionsEi:
.LFB8289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$64, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%esi, -4(%rbp)
	leaq	-4(%rbp), %rsi
	call	_ZNSt8__detail9_Map_baseIiSt4pairIKiiESaIS3_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	addl	$1, (%rax)
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8289:
	.size	_ZN12v8_inspector15V8InspectorImpl14muteExceptionsEi, .-_ZN12v8_inspector15V8InspectorImpl14muteExceptionsEi
	.section	.text._ZN12v8_inspector15V8InspectorImpl16unmuteExceptionsEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl16unmuteExceptionsEi
	.type	_ZN12v8_inspector15V8InspectorImpl16unmuteExceptionsEi, @function
_ZN12v8_inspector15V8InspectorImpl16unmuteExceptionsEi:
.LFB8290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$64, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%esi, -4(%rbp)
	leaq	-4(%rbp), %rsi
	call	_ZNSt8__detail9_Map_baseIiSt4pairIKiiESaIS3_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	subl	$1, (%rax)
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8290:
	.size	_ZN12v8_inspector15V8InspectorImpl16unmuteExceptionsEi, .-_ZN12v8_inspector15V8InspectorImpl16unmuteExceptionsEi
	.section	.text._ZNSt10_HashtableIiSt4pairIKiSt3mapIiPN12v8_inspector22V8InspectorSessionImplESt4lessIiESaIS0_IS1_S5_EEEESaISB_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSD_10_Hash_nodeISB_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiSt3mapIiPN12v8_inspector22V8InspectorSessionImplESt4lessIiESaIS0_IS1_S5_EEEESaISB_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSD_10_Hash_nodeISB_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiSt3mapIiPN12v8_inspector22V8InspectorSessionImplESt4lessIiESaIS0_IS1_S5_EEEESaISB_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSD_10_Hash_nodeISB_Lb0EEEm
	.type	_ZNSt10_HashtableIiSt4pairIKiSt3mapIiPN12v8_inspector22V8InspectorSessionImplESt4lessIiESaIS0_IS1_S5_EEEESaISB_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSD_10_Hash_nodeISB_Lb0EEEm, @function
_ZNSt10_HashtableIiSt4pairIKiSt3mapIiPN12v8_inspector22V8InspectorSessionImplESt4lessIiESaIS0_IS1_S5_EEEESaISB_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSD_10_Hash_nodeISB_Lb0EEEm:
.LFB14244:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L514
	movq	(%rbx), %r8
.L515:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L524
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L525:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L514:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L538
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L539
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L517:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L519
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L521:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L522:
	testq	%rsi, %rsi
	je	.L519
.L520:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movslq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L521
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L527
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L520
	.p2align 4,,10
	.p2align 3
.L519:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L523
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L523:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L524:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L526
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L526:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L527:
	movq	%rdx, %rdi
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L538:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L517
.L539:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE14244:
	.size	_ZNSt10_HashtableIiSt4pairIKiSt3mapIiPN12v8_inspector22V8InspectorSessionImplESt4lessIiESaIS0_IS1_S5_EEEESaISB_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSD_10_Hash_nodeISB_Lb0EEEm, .-_ZNSt10_HashtableIiSt4pairIKiSt3mapIiPN12v8_inspector22V8InspectorSessionImplESt4lessIiESaIS0_IS1_S5_EEEESaISB_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSD_10_Hash_nodeISB_Lb0EEEm
	.section	.text._ZNSt8_Rb_treeIiSt4pairIKiPN12v8_inspector22V8InspectorSessionImplEESt10_Select1stIS5_ESt4lessIiESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERS1_,"axG",@progbits,_ZNSt8_Rb_treeIiSt4pairIKiPN12v8_inspector22V8InspectorSessionImplEESt10_Select1stIS5_ESt4lessIiESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiSt4pairIKiPN12v8_inspector22V8InspectorSessionImplEESt10_Select1stIS5_ESt4lessIiESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERS1_
	.type	_ZNSt8_Rb_treeIiSt4pairIKiPN12v8_inspector22V8InspectorSessionImplEESt10_Select1stIS5_ESt4lessIiESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERS1_, @function
_ZNSt8_Rb_treeIiSt4pairIKiPN12v8_inspector22V8InspectorSessionImplEESt10_Select1stIS5_ESt4lessIiESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERS1_:
.LFB14249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpq	%rsi, %r15
	je	.L590
	movl	(%rdx), %r14d
	cmpl	32(%rsi), %r14d
	jge	.L551
	movq	24(%rdi), %rbx
	movq	%rbx, %rax
	movq	%rbx, %rdx
	cmpq	%rsi, %rbx
	je	.L543
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpl	32(%rax), %r14d
	jle	.L553
	movl	$0, %ebx
	cmpq	$0, 24(%rax)
	movq	%rbx, %rax
	cmovne	%r12, %rdx
	cmovne	%r12, %rax
.L543:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L551:
	.cfi_restore_state
	jle	.L562
	movq	32(%rdi), %rdx
	cmpq	%rsi, %rdx
	je	.L589
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpl	32(%rax), %r14d
	jge	.L564
	movl	$0, %ebx
	cmpq	$0, 24(%r12)
	movq	%rbx, %rax
	cmovne	%rdx, %rax
	cmove	%r12, %rdx
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L590:
	cmpq	$0, 40(%rdi)
	je	.L542
	movq	32(%rdi), %rdx
	movl	(%r14), %eax
	cmpl	%eax, 32(%rdx)
	jl	.L589
.L542:
	movq	16(%r13), %rbx
	testq	%rbx, %rbx
	je	.L573
	movl	(%r14), %esi
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L591:
	movq	16(%rbx), %rax
	movl	$1, %ecx
	testq	%rax, %rax
	je	.L546
.L592:
	movq	%rax, %rbx
.L545:
	movl	32(%rbx), %edx
	cmpl	%edx, %esi
	jl	.L591
	movq	24(%rbx), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	jne	.L592
.L546:
	movq	%rbx, %r12
	testb	%cl, %cl
	jne	.L544
.L549:
	xorl	%eax, %eax
	cmpl	%esi, %edx
	cmovl	%rax, %rbx
	cmovge	%rax, %r12
.L550:
	addq	$8, %rsp
	movq	%rbx, %rax
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L562:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rsi, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L589:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L553:
	.cfi_restore_state
	movq	16(%r13), %r12
	testq	%r12, %r12
	jne	.L556
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L594:
	movq	16(%r12), %rax
	movl	$1, %esi
.L559:
	testq	%rax, %rax
	je	.L557
	movq	%rax, %r12
.L556:
	movl	32(%r12), %ecx
	cmpl	%ecx, %r14d
	jl	.L594
	movq	24(%r12), %rax
	xorl	%esi, %esi
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L573:
	movq	%r12, %rbx
.L544:
	cmpq	%rbx, 24(%r13)
	je	.L575
	movq	%rbx, %rdi
	movq	%rbx, %r12
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	(%r14), %esi
	movl	32(%rax), %edx
	movq	%rax, %rbx
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L557:
	movq	%r12, %rdx
	testb	%sil, %sil
	jne	.L555
.L560:
	xorl	%eax, %eax
	cmpl	%ecx, %r14d
	cmovg	%rax, %r12
	cmovle	%rax, %rdx
.L561:
	movq	%r12, %rax
	jmp	.L543
.L593:
	movq	%r15, %r12
.L555:
	cmpq	%r12, %rbx
	je	.L579
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%r12, %rdx
	movl	32(%rax), %ecx
	movq	%rax, %r12
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L564:
	movq	16(%r13), %rbx
	testq	%rbx, %rbx
	jne	.L567
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L596:
	movq	16(%rbx), %rax
	movl	$1, %esi
.L570:
	testq	%rax, %rax
	je	.L568
	movq	%rax, %rbx
.L567:
	movl	32(%rbx), %ecx
	cmpl	%ecx, %r14d
	jl	.L596
	movq	24(%rbx), %rax
	xorl	%esi, %esi
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L568:
	movq	%rbx, %rdx
	testb	%sil, %sil
	jne	.L566
.L571:
	xorl	%eax, %eax
	cmpl	%ecx, %r14d
	cmovg	%rax, %rbx
	cmovle	%rax, %rdx
.L572:
	movq	%rbx, %rax
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L575:
	movq	%rbx, %r12
	xorl	%ebx, %ebx
	jmp	.L550
.L595:
	movq	%r15, %rbx
.L566:
	cmpq	%rbx, 24(%r13)
	je	.L583
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rbx, %rdx
	movl	32(%rax), %ecx
	movq	%rax, %rbx
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L579:
	movq	%r12, %rdx
	xorl	%r12d, %r12d
	jmp	.L561
.L583:
	movq	%rbx, %rdx
	xorl	%ebx, %ebx
	jmp	.L572
	.cfi_endproc
.LFE14249:
	.size	_ZNSt8_Rb_treeIiSt4pairIKiPN12v8_inspector22V8InspectorSessionImplEESt10_Select1stIS5_ESt4lessIiESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERS1_, .-_ZNSt8_Rb_treeIiSt4pairIKiPN12v8_inspector22V8InspectorSessionImplEESt10_Select1stIS5_ESt4lessIiESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERS1_
	.section	.text._ZN12v8_inspector15V8InspectorImpl7connectEiPNS_11V8Inspector7ChannelERKNS_10StringViewE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl7connectEiPNS_11V8Inspector7ChannelERKNS_10StringViewE
	.type	_ZN12v8_inspector15V8InspectorImpl7connectEiPNS_11V8Inspector7ChannelERKNS_10StringViewE, @function
_ZN12v8_inspector15V8InspectorImpl7connectEiPNS_11V8Inspector7ChannelERKNS_10StringViewE:
.LFB8305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %r9
	movq	%rcx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	-64(%rbp), %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	52(%rsi), %eax
	leal	1(%rax), %r14d
	movl	%r14d, 52(%rsi)
	movl	%r14d, %ecx
	call	_ZN12v8_inspector22V8InspectorSessionImpl6createEPNS_15V8InspectorImplEiiPNS_11V8Inspector7ChannelERKNS_10StringViewE@PLT
	movq	184(%rbx), %rsi
	movslq	%r12d, %r9
	xorl	%edx, %edx
	movq	%r9, %rax
	divq	%rsi
	movq	176(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r15
	testq	%rax, %rax
	je	.L598
	movq	(%rax), %rcx
	movl	8(%rcx), %edi
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L623:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L598
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%rsi
	cmpq	%rdx, %r15
	jne	.L598
.L600:
	cmpl	%edi, %r12d
	jne	.L623
	leaq	16(%rcx), %rbx
.L610:
	movq	16(%rbx), %rax
	leaq	8(%rbx), %r15
	movq	%r15, %r12
	testq	%rax, %rax
	jne	.L602
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L624:
	movq	%rax, %r12
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L603
.L602:
	cmpl	32(%rax), %r14d
	jle	.L624
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L602
.L603:
	cmpq	%r12, %r15
	je	.L601
	cmpl	32(%r12), %r14d
	jl	.L601
.L606:
	movq	-64(%rbp), %rax
	movq	%rax, 40(%r12)
	movq	%rax, 0(%r13)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L625
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L601:
	.cfi_restore_state
	movl	$48, %edi
	movq	%r12, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rsi
	movq	%rbx, %rdi
	movl	%r14d, 32(%rax)
	leaq	32(%rax), %rdx
	movq	%rax, %r12
	movq	$0, 40(%rax)
	call	_ZNSt8_Rb_treeIiSt4pairIKiPN12v8_inspector22V8InspectorSessionImplEESt10_Select1stIS5_ESt4lessIiESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERS1_
	movq	%rax, %r14
	testq	%rdx, %rdx
	je	.L607
	testq	%rax, %rax
	jne	.L613
	cmpq	%rdx, %r15
	jne	.L626
.L613:
	movl	$1, %edi
.L608:
	movq	%r15, %rcx
	movq	%r12, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L607:
	movq	%r12, %rdi
	movq	%r14, %r12
	call	_ZdlPv@PLT
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L626:
	xorl	%edi, %edi
	movl	32(%rdx), %eax
	cmpl	%eax, 32(%r12)
	setl	%dil
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L598:
	movl	$64, %edi
	movq	%r9, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %r9
	leaq	176(%rbx), %rdi
	movq	%r15, %rsi
	movq	$0, (%rax)
	movq	%rax, %rcx
	leaq	24(%rax), %rax
	movl	$1, %r8d
	movl	%r12d, -16(%rax)
	movq	%r9, %rdx
	movl	$0, (%rax)
	movq	$0, 8(%rax)
	movq	%rax, 40(%rcx)
	movq	%rax, 48(%rcx)
	movq	$0, 56(%rcx)
	call	_ZNSt10_HashtableIiSt4pairIKiSt3mapIiPN12v8_inspector22V8InspectorSessionImplESt4lessIiESaIS0_IS1_S5_EEEESaISB_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSD_10_Hash_nodeISB_Lb0EEEm
	leaq	16(%rax), %rbx
	jmp	.L610
.L625:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8305:
	.size	_ZN12v8_inspector15V8InspectorImpl7connectEiPNS_11V8Inspector7ChannelERKNS_10StringViewE, .-_ZN12v8_inspector15V8InspectorImpl7connectEiPNS_11V8Inspector7ChannelERKNS_10StringViewE
	.section	.text._ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector16InspectedContextESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector16InspectedContextESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector16InspectedContextESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	.type	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector16InspectedContextESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm, @function
_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector16InspectedContextESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm:
.LFB14410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L628
	movq	(%rbx), %r8
.L629:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L638
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L639:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L628:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L652
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L653
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L631:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L633
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L635:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L636:
	testq	%rsi, %rsi
	je	.L633
.L634:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movslq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L635
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L641
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L634
	.p2align 4,,10
	.p2align 3
.L633:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L637
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L637:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L638:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L640
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L640:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L641:
	movq	%rdx, %rdi
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L652:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L631
.L653:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE14410:
	.size	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector16InspectedContextESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm, .-_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector16InspectedContextESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	.section	.text._ZNSt10_HashtableIiSt4pairIKiiESaIS2_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiiESaIS2_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiiESaIS2_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	.type	_ZNSt10_HashtableIiSt4pairIKiiESaIS2_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_, @function
_ZNSt10_HashtableIiSt4pairIKiiESaIS2_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_:
.LFB14422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	(%rsi), %rax
	movq	8(%rdi), %rdi
	movq	(%rbx), %r12
	movq	%rax, %r8
	divq	%rdi
	leaq	0(,%rdx,8), %r14
	leaq	(%r12,%r14), %r13
	movq	0(%r13), %r11
	testq	%r11, %r11
	je	.L665
	movq	(%r11), %r15
	movq	%rdx, %r10
	movq	%r11, %r9
	movl	8(%r15), %esi
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L672:
	testq	%rcx, %rcx
	je	.L665
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%r15, %r9
	movq	%rax, %rsi
	divq	%rdi
	cmpq	%r10, %rdx
	jne	.L665
	movq	%rcx, %r15
.L657:
	movq	(%r15), %rcx
	cmpl	%esi, %r8d
	jne	.L672
	cmpq	%r9, %r11
	je	.L673
	testq	%rcx, %rcx
	je	.L659
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%rdi
	cmpq	%rdx, %r10
	je	.L659
	movq	%r9, (%r12,%rdx,8)
	movq	(%r15), %rcx
.L659:
	movq	%rcx, (%r9)
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L665:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L673:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L666
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%rdi
	cmpq	%rdx, %r10
	je	.L659
	movq	%r9, (%r12,%rdx,8)
	addq	(%rbx), %r14
	movq	(%r14), %rax
	movq	%r14, %r13
.L658:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L674
.L660:
	movq	$0, 0(%r13)
	movq	(%r15), %rcx
	jmp	.L659
.L666:
	movq	%r9, %rax
	jmp	.L658
.L674:
	movq	%rcx, 16(%rbx)
	jmp	.L660
	.cfi_endproc
.LFE14422:
	.size	_ZNSt10_HashtableIiSt4pairIKiiESaIS2_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_, .-_ZNSt10_HashtableIiSt4pairIKiiESaIS2_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	.section	.text._ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector23V8ConsoleMessageStorageESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector23V8ConsoleMessageStorageESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector23V8ConsoleMessageStorageESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	.type	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector23V8ConsoleMessageStorageESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_, @function
_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector23V8ConsoleMessageStorageESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_:
.LFB14429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	(%rsi), %rax
	movq	8(%rdi), %rdi
	movq	(%rbx), %r13
	movq	%rax, %r8
	divq	%rdi
	leaq	0(,%rdx,8), %r15
	leaq	0(%r13,%r15), %r14
	movq	(%r14), %r11
	testq	%r11, %r11
	je	.L687
	movq	(%r11), %r12
	movq	%rdx, %r10
	movq	%r11, %r9
	movl	8(%r12), %esi
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L697:
	testq	%rcx, %rcx
	je	.L687
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%r12, %r9
	movq	%rax, %rsi
	divq	%rdi
	cmpq	%r10, %rdx
	jne	.L687
	movq	%rcx, %r12
.L678:
	movq	(%r12), %rcx
	cmpl	%esi, %r8d
	jne	.L697
	cmpq	%r9, %r11
	je	.L698
	testq	%rcx, %rcx
	je	.L680
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%rdi
	cmpq	%rdx, %r10
	je	.L680
	movq	%r9, 0(%r13,%rdx,8)
	movq	(%r12), %rcx
.L680:
	movq	%rcx, (%r9)
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L682
	movq	%r13, %rdi
	call	_ZN12v8_inspector23V8ConsoleMessageStorageD1Ev@PLT
	movl	$144, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L682:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L687:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L698:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L688
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%rdi
	cmpq	%rdx, %r10
	je	.L680
	movq	%r9, 0(%r13,%rdx,8)
	addq	(%rbx), %r15
	movq	(%r15), %rax
	movq	%r15, %r14
.L679:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L699
.L681:
	movq	$0, (%r14)
	movq	(%r12), %rcx
	jmp	.L680
.L688:
	movq	%r9, %rax
	jmp	.L679
.L699:
	movq	%rcx, 16(%rbx)
	jmp	.L681
	.cfi_endproc
.LFE14429:
	.size	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector23V8ConsoleMessageStorageESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_, .-_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector23V8ConsoleMessageStorageESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	.section	.text._ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.type	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, @function
_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_:
.LFB14436:
	.cfi_startproc
	endbr64
	movabsq	$2305843009213693951, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$2, %rax
	cmpq	%rcx, %rax
	je	.L714
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L710
	movabsq	$9223372036854775804, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L715
.L702:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L709:
	movl	(%r15), %eax
	subq	%r8, %r13
	leaq	4(%rbx,%rdx), %r15
	movl	%eax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L716
	testq	%r13, %r13
	jg	.L705
	testq	%r9, %r9
	jne	.L708
.L706:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L716:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L705
.L708:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L715:
	testq	%rsi, %rsi
	jne	.L703
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L705:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L706
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L710:
	movl	$4, %r14d
	jmp	.L702
.L714:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L703:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$2, %r14
	jmp	.L702
	.cfi_endproc
.LFE14436:
	.size	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, .-_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.section	.text._ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_15V8InspectorImpl17resetContextGroupEiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_15V8InspectorImpl17resetContextGroupEiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_15V8InspectorImpl17resetContextGroupEiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB12020:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	8(%rdi), %rsi
	movl	16(%rax), %eax
	movl	%eax, -12(%rbp)
	cmpq	16(%rdi), %rsi
	je	.L718
	movl	%eax, (%rsi)
	addq	$4, 8(%rdi)
.L717:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L722
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L718:
	.cfi_restore_state
	leaq	-12(%rbp), %rdx
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	jmp	.L717
.L722:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12020:
	.size	_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_15V8InspectorImpl17resetContextGroupEiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_15V8InspectorImpl17resetContextGroupEiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS2_IN12v8_inspector16InspectedContextESt14default_deleteIS5_EESt4hashIiESt8equal_toIiESaIS0_IS1_S8_EEES6_ISF_EEESaISI_ENSt8__detail10_Select1stESC_SA_NSK_18_Mod_range_hashingENSK_20_Default_ranged_hashENSK_20_Prime_rehash_policyENSK_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS2_IN12v8_inspector16InspectedContextESt14default_deleteIS5_EESt4hashIiESt8equal_toIiESaIS0_IS1_S8_EEES6_ISF_EEESaISI_ENSt8__detail10_Select1stESC_SA_NSK_18_Mod_range_hashingENSK_20_Default_ranged_hashENSK_20_Prime_rehash_policyENSK_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS2_IN12v8_inspector16InspectedContextESt14default_deleteIS5_EESt4hashIiESt8equal_toIiESaIS0_IS1_S8_EEES6_ISF_EEESaISI_ENSt8__detail10_Select1stESC_SA_NSK_18_Mod_range_hashingENSK_20_Default_ranged_hashENSK_20_Prime_rehash_policyENSK_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	.type	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS2_IN12v8_inspector16InspectedContextESt14default_deleteIS5_EESt4hashIiESt8equal_toIiESaIS0_IS1_S8_EEES6_ISF_EEESaISI_ENSt8__detail10_Select1stESC_SA_NSK_18_Mod_range_hashingENSK_20_Default_ranged_hashENSK_20_Prime_rehash_policyENSK_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_, @function
_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS2_IN12v8_inspector16InspectedContextESt14default_deleteIS5_EESt4hashIiESt8equal_toIiESaIS0_IS1_S8_EEES6_ISF_EEESaISI_ENSt8__detail10_Select1stESC_SA_NSK_18_Mod_range_hashingENSK_20_Default_ranged_hashENSK_20_Prime_rehash_policyENSK_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_:
.LFB14449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	(%rsi), %rax
	movq	8(%rdi), %rdi
	movq	(%rbx), %r12
	movq	%rax, %r8
	divq	%rdi
	leaq	0(,%rdx,8), %r14
	leaq	(%r12,%r14), %r13
	movq	0(%r13), %r11
	testq	%r11, %r11
	je	.L734
	movq	(%r11), %r15
	movq	%rdx, %r10
	movq	%r11, %r9
	movl	8(%r15), %esi
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L741:
	testq	%rcx, %rcx
	je	.L734
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%r15, %r9
	movq	%rax, %rsi
	divq	%rdi
	cmpq	%r10, %rdx
	jne	.L734
	movq	%rcx, %r15
.L726:
	movq	(%r15), %rcx
	cmpl	%esi, %r8d
	jne	.L741
	cmpq	%r9, %r11
	je	.L742
	testq	%rcx, %rcx
	je	.L728
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%rdi
	cmpq	%rdx, %r10
	je	.L728
	movq	%r9, (%r12,%rdx,8)
	movq	(%r15), %rcx
.L728:
	movq	%rcx, (%r9)
	movq	%r15, %rdi
	call	_ZNSt8__detail16_Hashtable_allocISaINS_10_Hash_nodeISt4pairIKiSt10unique_ptrISt13unordered_mapIiS4_IN12v8_inspector16InspectedContextESt14default_deleteIS7_EESt4hashIiESt8equal_toIiESaIS2_IS3_SA_EEES8_ISH_EEELb0EEEEE18_M_deallocate_nodeEPSL_.isra.0
	subq	$1, 24(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L734:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L742:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L735
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%rdi
	cmpq	%rdx, %r10
	je	.L728
	movq	%r9, (%r12,%rdx,8)
	addq	(%rbx), %r14
	movq	(%r14), %rax
	movq	%r14, %r13
.L727:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L743
.L729:
	movq	$0, 0(%r13)
	movq	(%r15), %rcx
	jmp	.L728
.L735:
	movq	%r9, %rax
	jmp	.L727
.L743:
	movq	%rcx, 16(%rbx)
	jmp	.L729
	.cfi_endproc
.LFE14449:
	.size	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS2_IN12v8_inspector16InspectedContextESt14default_deleteIS5_EESt4hashIiESt8equal_toIiESaIS0_IS1_S8_EEES6_ISF_EEESaISI_ENSt8__detail10_Select1stESC_SA_NSK_18_Mod_range_hashingENSK_20_Default_ranged_hashENSK_20_Prime_rehash_policyENSK_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_, .-_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS2_IN12v8_inspector16InspectedContextESt14default_deleteIS5_EESt4hashIiESt8equal_toIiESaIS0_IS1_S8_EEES6_ISF_EEESaISI_ENSt8__detail10_Select1stESC_SA_NSK_18_Mod_range_hashingENSK_20_Default_ranged_hashENSK_20_Prime_rehash_policyENSK_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	.section	.text._ZN12v8_inspector15V8InspectorImpl17resetContextGroupEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl17resetContextGroupEi
	.type	_ZN12v8_inspector15V8InspectorImpl17resetContextGroupEi, @function
_ZN12v8_inspector15V8InspectorImpl17resetContextGroupEi:
.LFB8348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-116(%rbp), %r13
	leaq	-112(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-80(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$232, %rdi
	subq	$96, %rsp
	movl	%esi, -116(%rbp)
	movq	%r13, %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector23V8ConsoleMessageStorageESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	leaq	64(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNSt10_HashtableIiSt4pairIKiiESaIS2_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	pxor	%xmm0, %xmm0
	movl	-116(%rbp), %esi
	movq	%r12, %rdx
	leaq	_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_15V8InspectorImpl17resetContextGroupEiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector15V8InspectorImpl17resetContextGroupEiEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation(%rip), %rcx
	movaps	%xmm0, -112(%rbp)
	movq	%rbx, %rdi
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movq	$0, -96(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%r14, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZN12v8_inspector15V8InspectorImpl14forEachContextEiRKSt8functionIFvPNS_16InspectedContextEEE
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L745
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L745:
	movq	24(%rbx), %rax
	movq	8(%rbx), %rsi
	movq	%r14, %rdx
	leaq	728(%rax), %rdi
	call	_ZN12v8_inspector15WasmTranslation5ClearEPN2v87IsolateERKSt6vectorIiSaIiEE@PLT
	movl	-116(%rbp), %esi
	movq	%r12, %rdx
	movq	%rbx, %rdi
	leaq	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_15V8InspectorImpl17resetContextGroupEiEUlS2_E0_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector15V8InspectorImpl17resetContextGroupEiEUlPNS1_22V8InspectorSessionImplEE0_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation(%rip), %rcx
	movq	%rax, %xmm2
	movq	%rcx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN12v8_inspector15V8InspectorImpl14forEachSessionEiRKSt8functionIFvPNS_22V8InspectorSessionImplEEE
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L746
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L746:
	leaq	120(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS2_IN12v8_inspector16InspectedContextESt14default_deleteIS5_EESt4hashIiESt8equal_toIiESaIS0_IS1_S8_EEES6_ISF_EEESaISI_ENSt8__detail10_Select1stESC_SA_NSK_18_Mod_range_hashingENSK_20_Default_ranged_hashENSK_20_Prime_rehash_policyENSK_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L744
	call	_ZdlPv@PLT
.L744:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L759
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L759:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8348:
	.size	_ZN12v8_inspector15V8InspectorImpl17resetContextGroupEi, .-_ZN12v8_inspector15V8InspectorImpl17resetContextGroupEi
	.section	.text._ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS2_IN12v8_inspector16InspectedContextESt14default_deleteIS5_EESt4hashIiESt8equal_toIiESaIS0_IS1_S8_EEES6_ISF_EEESaISI_ENSt8__detail10_Select1stESC_SA_NSK_18_Mod_range_hashingENSK_20_Default_ranged_hashENSK_20_Prime_rehash_policyENSK_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSK_10_Hash_nodeISI_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS2_IN12v8_inspector16InspectedContextESt14default_deleteIS5_EESt4hashIiESt8equal_toIiESaIS0_IS1_S8_EEES6_ISF_EEESaISI_ENSt8__detail10_Select1stESC_SA_NSK_18_Mod_range_hashingENSK_20_Default_ranged_hashENSK_20_Prime_rehash_policyENSK_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSK_10_Hash_nodeISI_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS2_IN12v8_inspector16InspectedContextESt14default_deleteIS5_EESt4hashIiESt8equal_toIiESaIS0_IS1_S8_EEES6_ISF_EEESaISI_ENSt8__detail10_Select1stESC_SA_NSK_18_Mod_range_hashingENSK_20_Default_ranged_hashENSK_20_Prime_rehash_policyENSK_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSK_10_Hash_nodeISI_Lb0EEEm
	.type	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS2_IN12v8_inspector16InspectedContextESt14default_deleteIS5_EESt4hashIiESt8equal_toIiESaIS0_IS1_S8_EEES6_ISF_EEESaISI_ENSt8__detail10_Select1stESC_SA_NSK_18_Mod_range_hashingENSK_20_Default_ranged_hashENSK_20_Prime_rehash_policyENSK_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSK_10_Hash_nodeISI_Lb0EEEm, @function
_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS2_IN12v8_inspector16InspectedContextESt14default_deleteIS5_EESt4hashIiESt8equal_toIiESaIS0_IS1_S8_EEES6_ISF_EEESaISI_ENSt8__detail10_Select1stESC_SA_NSK_18_Mod_range_hashingENSK_20_Default_ranged_hashENSK_20_Prime_rehash_policyENSK_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSK_10_Hash_nodeISI_Lb0EEEm:
.LFB14512:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L761
	movq	(%rbx), %r8
.L762:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L771
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L772:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L761:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L785
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L786
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L764:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L766
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L768:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L769:
	testq	%rsi, %rsi
	je	.L766
.L767:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movslq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L768
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L774
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L767
	.p2align 4,,10
	.p2align 3
.L766:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L770
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L770:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L771:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L773
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L773:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L774:
	movq	%rdx, %rdi
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L785:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L764
.L786:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE14512:
	.size	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS2_IN12v8_inspector16InspectedContextESt14default_deleteIS5_EESt4hashIiESt8equal_toIiESaIS0_IS1_S8_EEES6_ISF_EEESaISI_ENSt8__detail10_Select1stESC_SA_NSK_18_Mod_range_hashingENSK_20_Default_ranged_hashENSK_20_Prime_rehash_policyENSK_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSK_10_Hash_nodeISI_Lb0EEEm, .-_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS2_IN12v8_inspector16InspectedContextESt14default_deleteIS5_EESt4hashIiESt8equal_toIiESaIS0_IS1_S8_EEES6_ISF_EEESaISI_ENSt8__detail10_Select1stESC_SA_NSK_18_Mod_range_hashingENSK_20_Default_ranged_hashENSK_20_Prime_rehash_policyENSK_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSK_10_Hash_nodeISI_Lb0EEEm
	.section	.text._ZN12v8_inspector15V8InspectorImpl14contextCreatedERKNS_13V8ContextInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl14contextCreatedERKNS_13V8ContextInfoE
	.type	_ZN12v8_inspector15V8InspectorImpl14contextCreatedERKNS_13V8ContextInfoE, @function
_ZN12v8_inspector15V8InspectorImpl14contextCreatedERKNS_13V8ContextInfoE:
.LFB8315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	48(%rdi), %eax
	addl	$1, %eax
	movl	%eax, 48(%rdi)
	movl	$272, %edi
	movl	%eax, -108(%rbp)
	call	_Znwm@PLT
	movl	-108(%rbp), %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector16InspectedContextC1EPNS_15V8InspectorImplERKNS_13V8ContextInfoEi@PLT
	leaq	-108(%rbp), %rsi
	movq	%rbx, -104(%rbp)
	leaq	288(%r12), %rdi
	call	_ZNSt8__detail9_Map_baseIiSt4pairIKiiESaIS3_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	movl	8(%r13), %esi
	xorl	%edx, %edx
	movl	%esi, (%rax)
	movq	128(%r12), %rcx
	movslq	%esi, %rax
	divq	%rcx
	movq	120(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L788
	movq	(%rax), %rbx
	movq	%rdx, %r8
	movl	8(%rbx), %edi
	jmp	.L790
	.p2align 4,,10
	.p2align 3
.L830:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L788
	movslq	8(%rbx), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%rcx
	cmpq	%rdx, %r8
	jne	.L788
.L790:
	cmpl	%edi, %esi
	jne	.L830
.L789:
	movq	16(%rbx), %r14
	movslq	-108(%rbp), %rbx
	xorl	%edx, %edx
	movq	8(%r14), %rsi
	movq	%rbx, %rax
	movq	%rbx, %r8
	divq	%rsi
	movq	(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r15
	testq	%rax, %rax
	je	.L794
	movq	(%rax), %rcx
	movl	8(%rcx), %edi
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L831:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L794
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%rsi
	cmpq	%rdx, %r15
	jne	.L794
.L796:
	cmpl	%r8d, %edi
	jne	.L831
	addq	$16, %rcx
.L799:
	movq	(%rcx), %r14
	movq	-104(%rbp), %rax
	movq	%rax, (%rcx)
	testq	%r14, %r14
	je	.L797
	movq	%r14, %rdi
	call	_ZN12v8_inspector16InspectedContextD1Ev@PLT
	movl	$272, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L797:
	leaq	-104(%rbp), %rax
	movl	8(%r13), %esi
	leaq	-96(%rbp), %r14
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	leaq	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector15V8InspectorImpl14contextCreatedERKNS1_13V8ContextInfoEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSA_St18_Manager_operation(%rip), %rdx
	leaq	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_15V8InspectorImpl14contextCreatedERKNS0_13V8ContextInfoEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%r14, %rdx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN12v8_inspector15V8InspectorImpl14forEachSessionEiRKSt8functionIFvPNS_22V8InspectorSessionImplEEE
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L787
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L787:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L832
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L788:
	.cfi_restore_state
	movl	$56, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movl	8(%r13), %r14d
	movl	$24, %edi
	movq	$0, 48(%rax)
	movq	%rax, %rbx
	leaq	48(%rax), %rax
	movups	%xmm0, -16(%rax)
	movq	%rax, (%rbx)
	movq	$1, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movl	$0x3f800000, 32(%rbx)
	movq	$0, 40(%rbx)
	call	_Znwm@PLT
	movq	128(%r12), %rsi
	movslq	%r14d, %r10
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %rdi
	movl	%r14d, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%r10, %rax
	divq	%rsi
	movq	120(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L791
	movq	(%rax), %rbx
	movl	8(%rbx), %ecx
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L833:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L791
	movslq	8(%rbx), %rax
	xorl	%edx, %edx
	movq	%rax, %rcx
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L791
.L793:
	cmpl	%ecx, %r14d
	jne	.L833
	call	_ZNSt8__detail16_Hashtable_allocISaINS_10_Hash_nodeISt4pairIKiSt10unique_ptrISt13unordered_mapIiS4_IN12v8_inspector16InspectedContextESt14default_deleteIS7_EESt4hashIiESt8equal_toIiESaIS2_IS3_SA_EEES8_ISH_EEELb0EEEEE18_M_deallocate_nodeEPSL_.isra.0
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L794:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	$0, (%rax)
	movl	-108(%rbp), %eax
	movl	$1, %r8d
	movq	$0, 16(%rcx)
	movl	%eax, 8(%rcx)
	call	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector16InspectedContextESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	leaq	16(%rax), %rcx
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L791:
	leaq	120(%r12), %r11
	movq	%rdi, %rcx
	movq	%r10, %rdx
	movq	%r9, %rsi
	movl	$1, %r8d
	movq	%r11, %rdi
	call	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS2_IN12v8_inspector16InspectedContextESt14default_deleteIS5_EESt4hashIiESt8equal_toIiESaIS0_IS1_S8_EEES6_ISF_EEESaISI_ENSt8__detail10_Select1stESC_SA_NSK_18_Mod_range_hashingENSK_20_Default_ranged_hashENSK_20_Prime_rehash_policyENSK_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSK_10_Hash_nodeISI_Lb0EEEm
	movq	%rax, %rbx
	jmp	.L789
.L832:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8315:
	.size	_ZN12v8_inspector15V8InspectorImpl14contextCreatedERKNS_13V8ContextInfoE, .-_ZN12v8_inspector15V8InspectorImpl14contextCreatedERKNS_13V8ContextInfoE
	.section	.text._ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS3_IN12v8_inspector16InspectedContextESt14default_deleteIS6_EESt4hashIiESt8equal_toIiESaIS1_IS2_S9_EEES7_ISG_EEESaISJ_ENS_10_Select1stESD_SB_NS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_,"axG",@progbits,_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS3_IN12v8_inspector16InspectedContextESt14default_deleteIS6_EESt4hashIiESt8equal_toIiESaIS1_IS2_S9_EEES7_ISG_EEESaISJ_ENS_10_Select1stESD_SB_NS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS3_IN12v8_inspector16InspectedContextESt14default_deleteIS6_EESt4hashIiESt8equal_toIiESaIS1_IS2_S9_EEES7_ISG_EEESaISJ_ENS_10_Select1stESD_SB_NS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	.type	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS3_IN12v8_inspector16InspectedContextESt14default_deleteIS6_EESt4hashIiESt8equal_toIiESaIS1_IS2_S9_EEES7_ISG_EEESaISJ_ENS_10_Select1stESD_SB_NS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_, @function
_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS3_IN12v8_inspector16InspectedContextESt14default_deleteIS6_EESt4hashIiESt8equal_toIiESaIS1_IS2_S9_EEES7_ISG_EEESaISJ_ENS_10_Select1stESD_SB_NS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_:
.LFB12049:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movslq	(%rsi), %r14
	movq	%rsi, %rbx
	movq	8(%rdi), %rdi
	movq	%r14, %rax
	divq	%rdi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r13
	testq	%rax, %rax
	je	.L835
	movq	(%rax), %rcx
	movq	%r14, %r8
	movl	8(%rcx), %esi
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L847:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L835
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%rdi
	cmpq	%rdx, %r13
	jne	.L835
.L837:
	cmpl	%esi, %r8d
	jne	.L847
	popq	%rbx
	leaq	16(%rcx), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L835:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	(%rbx), %eax
	movl	$1, %r8d
	movq	$0, 16(%rcx)
	movl	%eax, 8(%rcx)
	call	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS2_IN12v8_inspector16InspectedContextESt14default_deleteIS5_EESt4hashIiESt8equal_toIiESaIS0_IS1_S8_EEES6_ISF_EEESaISI_ENSt8__detail10_Select1stESC_SA_NSK_18_Mod_range_hashingENSK_20_Default_ranged_hashENSK_20_Prime_rehash_policyENSK_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSK_10_Hash_nodeISI_Lb0EEEm
	popq	%rbx
	popq	%r12
	addq	$16, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12049:
	.size	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS3_IN12v8_inspector16InspectedContextESt14default_deleteIS6_EESt4hashIiESt8equal_toIiESaIS1_IS2_S9_EEES7_ISG_EEESaISJ_ENS_10_Select1stESD_SB_NS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_, .-_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS3_IN12v8_inspector16InspectedContextESt14default_deleteIS6_EESt4hashIiESt8equal_toIiESaIS1_IS2_S9_EEES7_ISG_EEESaISJ_ENS_10_Select1stESD_SB_NS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	.section	.text._ZN12v8_inspector15V8InspectorImpl23discardInspectedContextEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl23discardInspectedContextEii
	.type	_ZN12v8_inspector15V8InspectorImpl23discardInspectedContextEii, @function
_ZN12v8_inspector15V8InspectorImpl23discardInspectedContextEii:
.LFB8383:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$40, %rsp
	movl	%esi, -52(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEii
	testq	%rax, %rax
	je	.L848
	addq	$120, %r12
	leaq	-52(%rbp), %r14
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS3_IN12v8_inspector16InspectedContextESt14default_deleteIS6_EESt4hashIiESt8equal_toIiESaIS1_IS2_S9_EEES7_ISG_EEESaISJ_ENS_10_Select1stESD_SB_NS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	xorl	%edx, %edx
	movq	(%rax), %r15
	movslq	%ebx, %rax
	movq	8(%r15), %rcx
	movq	(%r15), %r11
	divq	%rcx
	leaq	0(,%rdx,8), %rax
	movq	%rdx, %r10
	movq	%rax, -72(%rbp)
	addq	%r11, %rax
	movq	(%rax), %r8
	movq	%rax, -64(%rbp)
	testq	%r8, %r8
	je	.L851
	movq	(%r8), %r13
	movq	%r8, %r9
	movl	8(%r13), %edi
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L878:
	testq	%rsi, %rsi
	je	.L851
	movslq	8(%rsi), %rax
	xorl	%edx, %edx
	movq	%r13, %r9
	movq	%rax, %rdi
	divq	%rcx
	cmpq	%rdx, %r10
	jne	.L851
	movq	%rsi, %r13
.L853:
	movq	0(%r13), %rsi
	cmpl	%edi, %ebx
	jne	.L878
	cmpq	%r9, %r8
	je	.L879
	testq	%rsi, %rsi
	je	.L855
	movslq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r10
	je	.L855
	movq	%r9, (%r11,%rdx,8)
	movq	0(%r13), %rsi
.L855:
	movq	%rsi, (%r9)
	movq	16(%r13), %rbx
	testq	%rbx, %rbx
	je	.L857
	movq	%rbx, %rdi
	call	_ZN12v8_inspector16InspectedContextD1Ev@PLT
	movl	$272, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L857:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	subq	$1, 24(%r15)
.L851:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS3_IN12v8_inspector16InspectedContextESt14default_deleteIS6_EESt4hashIiESt8equal_toIiESaIS1_IS2_S9_EEES7_ISG_EEESaISJ_ENS_10_Select1stESD_SB_NS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	movq	(%rax), %rax
	cmpq	$0, 24(%rax)
	je	.L880
.L848:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L880:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrISt13unordered_mapIiS2_IN12v8_inspector16InspectedContextESt14default_deleteIS5_EESt4hashIiESt8equal_toIiESaIS0_IS1_S8_EEES6_ISF_EEESaISI_ENSt8__detail10_Select1stESC_SA_NSK_18_Mod_range_hashingENSK_20_Default_ranged_hashENSK_20_Prime_rehash_policyENSK_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L879:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L861
	movslq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r10
	je	.L855
	movq	%r9, (%r11,%rdx,8)
	movq	-72(%rbp), %rax
	addq	(%r15), %rax
	movq	%rax, -64(%rbp)
	movq	(%rax), %rax
.L854:
	leaq	16(%r15), %rdx
	cmpq	%rdx, %rax
	je	.L881
.L856:
	movq	-64(%rbp), %rax
	movq	$0, (%rax)
	movq	0(%r13), %rsi
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L861:
	movq	%r9, %rax
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L881:
	movq	%rsi, 16(%r15)
	jmp	.L856
	.cfi_endproc
.LFE8383:
	.size	_ZN12v8_inspector15V8InspectorImpl23discardInspectedContextEii, .-_ZN12v8_inspector15V8InspectorImpl23discardInspectedContextEii
	.section	.text._ZN12v8_inspector15V8InspectorImpl16contextCollectedEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl16contextCollectedEii
	.type	_ZN12v8_inspector15V8InspectorImpl16contextCollectedEii, @function
_ZN12v8_inspector15V8InspectorImpl16contextCollectedEii:
.LFB8346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	leaq	-100(%rbp), %rsi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$288, %rdi
	subq	$88, %rsp
	movl	%edx, -100(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNSt10_HashtableIiSt4pairIKiiESaIS2_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	movq	240(%r12), %rsi
	movslq	%r13d, %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	232(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L899
	movq	(%rax), %rcx
	movq	%rdx, %r8
	movl	8(%rcx), %edi
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L900:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L899
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L899
.L887:
	cmpl	%r13d, %edi
	jne	.L900
	movq	16(%rcx), %rdi
	movl	-100(%rbp), %esi
	call	_ZN12v8_inspector23V8ConsoleMessageStorage16contextDestroyedEi@PLT
.L899:
	movl	-100(%rbp), %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEii
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L882
	leaq	-88(%rbp), %rax
	leaq	-80(%rbp), %r14
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	leaq	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector15V8InspectorImpl16contextCollectedEiiEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation(%rip), %rcx
	leaq	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_15V8InspectorImpl16contextCollectedEiiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	movq	%r14, %rdx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN12v8_inspector15V8InspectorImpl14forEachSessionEiRKSt8functionIFvPNS_22V8InspectorSessionImplEEE
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L890
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L890:
	movl	-100(%rbp), %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15V8InspectorImpl23discardInspectedContextEii
.L882:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L901
	addq	$88, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L901:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8346:
	.size	_ZN12v8_inspector15V8InspectorImpl16contextCollectedEii, .-_ZN12v8_inspector15V8InspectorImpl16contextCollectedEii
	.section	.text._ZN12v8_inspector15V8InspectorImpl16contextDestroyedEN2v85LocalINS1_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl16contextDestroyedEN2v85LocalINS1_7ContextEEE
	.type	_ZN12v8_inspector15V8InspectorImpl16contextDestroyedEN2v85LocalINS1_7ContextEEE, @function
_ZN12v8_inspector15V8InspectorImpl16contextDestroyedEN2v85LocalINS1_7ContextEEE:
.LFB8345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	296(%r12), %rdi
	xorl	%edx, %edx
	movl	%eax, %esi
	cltq
	divq	%rdi
	movq	288(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L907
	movq	(%rax), %rcx
	movq	%rdx, %r9
	movl	8(%rcx), %r8d
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L910:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L907
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L907
.L905:
	cmpl	%r8d, %esi
	jne	.L910
	movl	12(%rcx), %esi
.L903:
	addq	$8, %rsp
	movl	%r14d, %edx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector15V8InspectorImpl16contextCollectedEii
	.p2align 4,,10
	.p2align 3
.L907:
	.cfi_restore_state
	xorl	%esi, %esi
	jmp	.L903
	.cfi_endproc
.LFE8345:
	.size	_ZN12v8_inspector15V8InspectorImpl16contextDestroyedEN2v85LocalINS1_7ContextEEE, .-_ZN12v8_inspector15V8InspectorImpl16contextDestroyedEN2v85LocalINS1_7ContextEEE
	.section	.text._ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector23V8ConsoleMessageStorageESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector23V8ConsoleMessageStorageESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector23V8ConsoleMessageStorageESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	.type	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector23V8ConsoleMessageStorageESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm, @function
_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector23V8ConsoleMessageStorageESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm:
.LFB15745:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L912
	movq	(%rbx), %r8
.L913:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L922
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L923:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L912:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L936
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L937
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L915:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L917
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L918
	.p2align 4,,10
	.p2align 3
.L919:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L920:
	testq	%rsi, %rsi
	je	.L917
.L918:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movslq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L919
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L925
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L918
	.p2align 4,,10
	.p2align 3
.L917:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L921
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L921:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L922:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L924
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L924:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L923
	.p2align 4,,10
	.p2align 3
.L925:
	movq	%rdx, %rdi
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L936:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L915
.L937:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE15745:
	.size	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector23V8ConsoleMessageStorageESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm, .-_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector23V8ConsoleMessageStorageESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	.section	.text._ZN12v8_inspector15V8InspectorImpl27ensureConsoleMessageStorageEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl27ensureConsoleMessageStorageEi
	.type	_ZN12v8_inspector15V8InspectorImpl27ensureConsoleMessageStorageEi, @function
_ZN12v8_inspector15V8InspectorImpl27ensureConsoleMessageStorageEi:
.LFB8291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%esi, %r14
	pushq	%r13
	movq	%r14, %rax
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r14, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	240(%rdi), %rcx
	divq	%rcx
	movq	232(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L939
	movq	(%rax), %rbx
	movq	%rdx, %rdi
	movl	8(%rbx), %esi
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L962:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L939
	movslq	8(%rbx), %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%rcx
	cmpq	%rdx, %rdi
	jne	.L939
.L941:
	cmpl	%r12d, %esi
	jne	.L962
.L940:
	movq	16(%rbx), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L939:
	.cfi_restore_state
	movl	$144, %edi
	call	_Znwm@PLT
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN12v8_inspector23V8ConsoleMessageStorageC1EPNS_15V8InspectorImplEi@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	movq	-56(%rbp), %r8
	movq	240(%r13), %rdi
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %r15
	movl	%r12d, 8(%rax)
	movq	%r8, 16(%rax)
	movq	%r14, %rax
	divq	%rdi
	movq	232(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rsi
	testq	%rax, %rax
	je	.L942
	movq	(%rax), %rbx
	movl	8(%rbx), %ecx
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L963:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L942
	movslq	8(%rbx), %rax
	xorl	%edx, %edx
	movq	%rax, %rcx
	divq	%rdi
	cmpq	%rdx, %rsi
	jne	.L942
.L944:
	cmpl	%r12d, %ecx
	jne	.L963
	movq	%r8, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN12v8_inspector23V8ConsoleMessageStorageD1Ev@PLT
	movq	-56(%rbp), %r8
	movl	$144, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L942:
	leaq	232(%r13), %rdi
	movl	$1, %r8d
	movq	%r15, %rcx
	movq	%r14, %rdx
	call	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector23V8ConsoleMessageStorageESt14default_deleteIS4_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	movq	%rax, %rbx
	jmp	.L940
	.cfi_endproc
.LFE8291:
	.size	_ZN12v8_inspector15V8InspectorImpl27ensureConsoleMessageStorageEi, .-_ZN12v8_inspector15V8InspectorImpl27ensureConsoleMessageStorageEi
	.section	.text._ZN12v8_inspector15V8InspectorImpl15exceptionThrownEN2v85LocalINS1_7ContextEEERKNS_10StringViewENS2_INS1_5ValueEEES7_S7_jjSt10unique_ptrINS_12V8StackTraceESt14default_deleteISB_EEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl15exceptionThrownEN2v85LocalINS1_7ContextEEERKNS_10StringViewENS2_INS1_5ValueEEES7_S7_jjSt10unique_ptrINS_12V8StackTraceESt14default_deleteISB_EEi
	.type	_ZN12v8_inspector15V8InspectorImpl15exceptionThrownEN2v85LocalINS1_7ContextEEERKNS_10StringViewENS2_INS1_5ValueEEES7_S7_jjSt10unique_ptrINS_12V8StackTraceESt14default_deleteISB_EEi, @function
_ZN12v8_inspector15V8InspectorImpl15exceptionThrownEN2v85LocalINS1_7ContextEEERKNS_10StringViewENS2_INS1_5ValueEEES7_S7_jjSt10unique_ptrINS_12V8StackTraceESt14default_deleteISB_EEi:
.LFB8367:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -232(%rbp)
	movq	32(%rbp), %rbx
	movq	%rcx, -240(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	296(%r12), %rdi
	xorl	%edx, %edx
	movl	%eax, %esi
	cltq
	divq	%rdi
	movq	288(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L965
	movq	(%rax), %rcx
	movq	%rdx, %r9
	movl	8(%rcx), %r8d
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L999:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L965
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L965
.L967:
	cmpl	%r8d, %esi
	jne	.L999
	movl	12(%rcx), %eax
	movl	%eax, -212(%rbp)
	testl	%eax, %eax
	je	.L965
	leaq	-212(%rbp), %rsi
	leaq	64(%r12), %rdi
	call	_ZNSt8__detail9_Map_baseIiSt4pairIKiiESaIS3_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L1000
.L965:
	xorl	%ebx, %ebx
.L964:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1001
	leaq	-40(%rbp), %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1000:
	.cfi_restore_state
	movl	44(%r12), %eax
	movq	(%rbx), %rdx
	movq	$0, (%rbx)
	movq	%r13, %rdi
	leal	1(%rax), %ebx
	movq	%rdx, -256(%rbp)
	movl	%ebx, 44(%r12)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	-232(%rbp), %rsi
	movl	%eax, -244(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN12v8_inspector10toString16ERKNS_10StringViewE@PLT
	movq	-256(%rbp), %rdx
	movq	8(%r12), %rax
	movq	%r15, %rsi
	leaq	-192(%rbp), %r15
	movq	%rdx, -200(%rbp)
	leaq	-144(%rbp), %rdx
	movq	%rdx, %rdi
	movq	%rax, -232(%rbp)
	movq	%rdx, -256(%rbp)
	call	_ZN12v8_inspector10toString16ERKNS_10StringViewE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector10toString16ERKNS_10StringViewE@PLT
	movq	16(%r12), %rdi
	pxor	%xmm0, %xmm0
	leaq	_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv(%rip), %rsi
	movq	-256(%rbp), %rdx
	movq	-264(%rbp), %rax
	movq	(%rdi), %rcx
	movq	176(%rcx), %rcx
	cmpq	%rsi, %rcx
	jne	.L1002
.L970:
	movl	24(%rbp), %r8d
	movl	16(%rbp), %ecx
	pushq	%rbx
	leaq	-200(%rbp), %r13
	movl	-244(%rbp), %esi
	pushq	-240(%rbp)
	leaq	-208(%rbp), %rdi
	movq	%r13, %r9
	pushq	%rsi
	movq	%r15, %rsi
	pushq	%rax
	movl	40(%rbp), %eax
	pushq	-232(%rbp)
	pushq	%rax
	call	_ZN12v8_inspector16V8ConsoleMessage18createForExceptionEdRKNS_8String16ES3_jjSt10unique_ptrINS_16V8StackTraceImplESt14default_deleteIS5_EEiPN2v87IsolateES3_iNS9_5LocalINS9_5ValueEEEj@PLT
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	addq	$48, %rsp
	cmpq	%rax, %rdi
	je	.L971
	call	_ZdlPv@PLT
.L971:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L972
	call	_ZdlPv@PLT
.L972:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L973
	movq	(%rdi), %rax
	call	*64(%rax)
.L973:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L974
	call	_ZdlPv@PLT
.L974:
	movl	-212(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15V8InspectorImpl27ensureConsoleMessageStorageEi
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	-208(%rbp), %rax
	movq	$0, -208(%rbp)
	movq	%rax, -200(%rbp)
	call	_ZN12v8_inspector23V8ConsoleMessageStorage10addMessageESt10unique_ptrINS_16V8ConsoleMessageESt14default_deleteIS2_EE@PLT
	movq	-200(%rbp), %r12
	testq	%r12, %r12
	je	.L975
	movq	%r12, %rdi
	call	_ZN12v8_inspector16V8ConsoleMessageD1Ev@PLT
	movl	$240, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L975:
	movq	-208(%rbp), %r12
	testq	%r12, %r12
	je	.L964
	movq	%r12, %rdi
	call	_ZN12v8_inspector16V8ConsoleMessageD1Ev@PLT
	movl	$240, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L1002:
	movq	%rdx, -264(%rbp)
	movq	%rax, -256(%rbp)
	call	*%rcx
	movq	-264(%rbp), %rdx
	movq	-256(%rbp), %rax
	jmp	.L970
.L1001:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8367:
	.size	_ZN12v8_inspector15V8InspectorImpl15exceptionThrownEN2v85LocalINS1_7ContextEEERKNS_10StringViewENS2_INS1_5ValueEEES7_S7_jjSt10unique_ptrINS_12V8StackTraceESt14default_deleteISB_EEi, .-_ZN12v8_inspector15V8InspectorImpl15exceptionThrownEN2v85LocalINS1_7ContextEEERKNS_10StringViewENS2_INS1_5ValueEEES7_S7_jjSt10unique_ptrINS_12V8StackTraceESt14default_deleteISB_EEi
	.section	.text._ZN12v8_inspector15V8InspectorImpl16exceptionRevokedEN2v85LocalINS1_7ContextEEEjRKNS_10StringViewE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl16exceptionRevokedEN2v85LocalINS1_7ContextEEEjRKNS_10StringViewE
	.type	_ZN12v8_inspector15V8InspectorImpl16exceptionRevokedEN2v85LocalINS1_7ContextEEEjRKNS_10StringViewE, @function
_ZN12v8_inspector15V8InspectorImpl16exceptionRevokedEN2v85LocalINS1_7ContextEEEjRKNS_10StringViewE:
.LFB8372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	296(%r12), %r8
	xorl	%edx, %edx
	movl	%eax, %edi
	cltq
	divq	%r8
	movq	288(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L1003
	movq	(%rax), %rcx
	movq	%rdx, %r9
	movl	8(%rcx), %esi
	jmp	.L1006
	.p2align 4,,10
	.p2align 3
.L1032:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1003
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L1003
.L1006:
	cmpl	%esi, %edi
	jne	.L1032
	movl	12(%rcx), %r15d
	testl	%r15d, %r15d
	je	.L1003
	leaq	-96(%rbp), %rbx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector10toString16ERKNS_10StringViewE@PLT
	movq	16(%r12), %rdi
	leaq	_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv(%rip), %rdx
	pxor	%xmm0, %xmm0
	movq	(%rdi), %rax
	movq	176(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1033
.L1007:
	leaq	-112(%rbp), %rdi
	movl	%r13d, %edx
	movq	%rbx, %rsi
	call	_ZN12v8_inspector16V8ConsoleMessage25createForRevokedExceptionEdRKNS_8String16Ej@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1008
	call	_ZdlPv@PLT
.L1008:
	movq	%r12, %rdi
	movl	%r15d, %esi
	call	_ZN12v8_inspector15V8InspectorImpl27ensureConsoleMessageStorageEi
	leaq	-104(%rbp), %rsi
	movq	%rax, %rdi
	movq	-112(%rbp), %rax
	movq	$0, -112(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector23V8ConsoleMessageStorage10addMessageESt10unique_ptrINS_16V8ConsoleMessageESt14default_deleteIS2_EE@PLT
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L1009
	movq	%r12, %rdi
	call	_ZN12v8_inspector16V8ConsoleMessageD1Ev@PLT
	movl	$240, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1009:
	movq	-112(%rbp), %r12
	testq	%r12, %r12
	je	.L1003
	movq	%r12, %rdi
	call	_ZN12v8_inspector16V8ConsoleMessageD1Ev@PLT
	movl	$240, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1003:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1034
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1033:
	.cfi_restore_state
	call	*%rax
	jmp	.L1007
.L1034:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8372:
	.size	_ZN12v8_inspector15V8InspectorImpl16exceptionRevokedEN2v85LocalINS1_7ContextEEEjRKNS_10StringViewE, .-_ZN12v8_inspector15V8InspectorImpl16exceptionRevokedEN2v85LocalINS1_7ContextEEEjRKNS_10StringViewE
	.section	.text._ZNSt8_Rb_treeIiSt4pairIKiPN12v8_inspector22V8InspectorSessionImplEESt10_Select1stIS5_ESt4lessIiESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeIiSt4pairIKiPN12v8_inspector22V8InspectorSessionImplEESt10_Select1stIS5_ESt4lessIiESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiSt4pairIKiPN12v8_inspector22V8InspectorSessionImplEESt10_Select1stIS5_ESt4lessIiESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeIiSt4pairIKiPN12v8_inspector22V8InspectorSessionImplEESt10_Select1stIS5_ESt4lessIiESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeIiSt4pairIKiPN12v8_inspector22V8InspectorSessionImplEESt10_Select1stIS5_ESt4lessIiESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB16378:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1043
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L1037:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIiSt4pairIKiPN12v8_inspector22V8InspectorSessionImplEESt10_Select1stIS5_ESt4lessIiESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1037
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1043:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE16378:
	.size	_ZNSt8_Rb_treeIiSt4pairIKiPN12v8_inspector22V8InspectorSessionImplEESt10_Select1stIS5_ESt4lessIiESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeIiSt4pairIKiPN12v8_inspector22V8InspectorSessionImplEESt10_Select1stIS5_ESt4lessIiESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.text._ZN12v8_inspector15V8InspectorImplD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImplD2Ev
	.type	_ZN12v8_inspector15V8InspectorImplD2Ev, @function
_ZN12v8_inspector15V8InspectorImplD2Ev:
.LFB8280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector15V8InspectorImplE(%rip), %rax
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	call	_ZN2v85debug12SetInspectorEPNS_7IsolateEPN12v8_inspector11V8InspectorE@PLT
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v85debug18SetConsoleDelegateEPNS_7IsolateEPNS0_15ConsoleDelegateE@PLT
	movq	344(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1047
	movq	(%rdi), %rax
	call	*184(%rax)
.L1047:
	movq	304(%rbx), %r12
	testq	%r12, %r12
	je	.L1048
	.p2align 4,,10
	.p2align 3
.L1049:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L1049
.L1048:
	movq	296(%rbx), %rax
	movq	288(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	288(%rbx), %rdi
	leaq	336(%rbx), %rax
	movq	$0, 312(%rbx)
	movq	$0, 304(%rbx)
	cmpq	%rax, %rdi
	je	.L1050
	call	_ZdlPv@PLT
.L1050:
	movq	248(%rbx), %r12
	testq	%r12, %r12
	jne	.L1054
	jmp	.L1051
	.p2align 4,,10
	.p2align 3
.L1105:
	movq	%r14, %rdi
	call	_ZN12v8_inspector23V8ConsoleMessageStorageD1Ev@PLT
	movq	%r14, %rdi
	movl	$144, %esi
	call	_ZdlPvm@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L1051
.L1053:
	movq	%r13, %r12
.L1054:
	movq	16(%r12), %r14
	movq	(%r12), %r13
	testq	%r14, %r14
	jne	.L1105
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1053
.L1051:
	movq	240(%rbx), %rax
	movq	232(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	232(%rbx), %rdi
	leaq	280(%rbx), %rax
	movq	$0, 256(%rbx)
	movq	$0, 248(%rbx)
	cmpq	%rax, %rdi
	je	.L1055
	call	_ZdlPv@PLT
.L1055:
	movq	192(%rbx), %r13
	testq	%r13, %r13
	je	.L1056
	.p2align 4,,10
	.p2align 3
.L1059:
	movq	%r13, %r14
	movq	0(%r13), %r13
	movq	32(%r14), %r12
	leaq	16(%r14), %r15
	testq	%r12, %r12
	je	.L1057
.L1058:
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIiSt4pairIKiPN12v8_inspector22V8InspectorSessionImplEESt10_Select1stIS5_ESt4lessIiESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r12, %rdi
	movq	16(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L1058
.L1057:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1059
.L1056:
	movq	184(%rbx), %rax
	movq	176(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	176(%rbx), %rdi
	leaq	224(%rbx), %rax
	movq	$0, 200(%rbx)
	movq	$0, 192(%rbx)
	cmpq	%rax, %rdi
	je	.L1060
	call	_ZdlPv@PLT
.L1060:
	movq	136(%rbx), %r12
	testq	%r12, %r12
	je	.L1061
	.p2align 4,,10
	.p2align 3
.L1062:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZNSt8__detail16_Hashtable_allocISaINS_10_Hash_nodeISt4pairIKiSt10unique_ptrISt13unordered_mapIiS4_IN12v8_inspector16InspectedContextESt14default_deleteIS7_EESt4hashIiESt8equal_toIiESaIS2_IS3_SA_EEES8_ISH_EEELb0EEEEE18_M_deallocate_nodeEPSL_.isra.0
	testq	%r12, %r12
	jne	.L1062
.L1061:
	movq	128(%rbx), %rax
	movq	120(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	120(%rbx), %rdi
	leaq	168(%rbx), %rax
	movq	$0, 144(%rbx)
	movq	$0, 136(%rbx)
	cmpq	%rax, %rdi
	je	.L1063
	call	_ZdlPv@PLT
.L1063:
	movq	80(%rbx), %r12
	testq	%r12, %r12
	je	.L1064
	.p2align 4,,10
	.p2align 3
.L1065:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L1065
.L1064:
	movq	72(%rbx), %rax
	movq	64(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	64(%rbx), %rdi
	leaq	112(%rbx), %rax
	movq	$0, 88(%rbx)
	movq	$0, 80(%rbx)
	cmpq	%rax, %rdi
	je	.L1066
	call	_ZdlPv@PLT
.L1066:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1067
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L1067:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1046
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1046:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8280:
	.size	_ZN12v8_inspector15V8InspectorImplD2Ev, .-_ZN12v8_inspector15V8InspectorImplD2Ev
	.globl	_ZN12v8_inspector15V8InspectorImplD1Ev
	.set	_ZN12v8_inspector15V8InspectorImplD1Ev,_ZN12v8_inspector15V8InspectorImplD2Ev
	.section	.text._ZN12v8_inspector15V8InspectorImplD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImplD0Ev
	.type	_ZN12v8_inspector15V8InspectorImplD0Ev, @function
_ZN12v8_inspector15V8InspectorImplD0Ev:
.LFB8282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN12v8_inspector15V8InspectorImplD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$352, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8282:
	.size	_ZN12v8_inspector15V8InspectorImplD0Ev, .-_ZN12v8_inspector15V8InspectorImplD0Ev
	.section	.text._ZNSt10_HashtableIiSt4pairIKiSt3mapIiPN12v8_inspector22V8InspectorSessionImplESt4lessIiESaIS0_IS1_S5_EEEESaISB_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiSt3mapIiPN12v8_inspector22V8InspectorSessionImplESt4lessIiESaIS0_IS1_S5_EEEESaISB_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiSt3mapIiPN12v8_inspector22V8InspectorSessionImplESt4lessIiESaIS0_IS1_S5_EEEESaISB_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	.type	_ZNSt10_HashtableIiSt4pairIKiSt3mapIiPN12v8_inspector22V8InspectorSessionImplESt4lessIiESaIS0_IS1_S5_EEEESaISB_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_, @function
_ZNSt10_HashtableIiSt4pairIKiSt3mapIiPN12v8_inspector22V8InspectorSessionImplESt4lessIiESaIS0_IS1_S5_EEEESaISB_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_:
.LFB14349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	(%rsi), %rax
	movq	8(%rdi), %rdi
	movq	(%rbx), %r13
	movq	%rax, %r8
	divq	%rdi
	leaq	0(,%rdx,8), %r15
	leaq	0(%r13,%r15), %r14
	movq	(%r14), %r11
	testq	%r11, %r11
	je	.L1121
	movq	(%r11), %r12
	movq	%rdx, %r10
	movq	%r11, %r9
	movl	8(%r12), %esi
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1132:
	testq	%rcx, %rcx
	je	.L1121
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%r12, %r9
	movq	%rax, %rsi
	divq	%rdi
	cmpq	%r10, %rdx
	jne	.L1121
	movq	%rcx, %r12
.L1111:
	movq	(%r12), %rcx
	cmpl	%esi, %r8d
	jne	.L1132
	cmpq	%r9, %r11
	je	.L1133
	testq	%rcx, %rcx
	je	.L1113
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%rdi
	cmpq	%rdx, %r10
	je	.L1113
	movq	%r9, 0(%r13,%rdx,8)
	movq	(%r12), %rcx
.L1113:
	movq	%rcx, (%r9)
	movq	32(%r12), %r13
	leaq	16(%r12), %r14
	testq	%r13, %r13
	je	.L1116
.L1115:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIiSt4pairIKiPN12v8_inspector22V8InspectorSessionImplEESt10_Select1stIS5_ESt4lessIiESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1115
.L1116:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1121:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1133:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L1122
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%rdi
	cmpq	%rdx, %r10
	je	.L1113
	movq	%r9, 0(%r13,%rdx,8)
	addq	(%rbx), %r15
	movq	(%r15), %rax
	movq	%r15, %r14
.L1112:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L1134
.L1114:
	movq	$0, (%r14)
	movq	(%r12), %rcx
	jmp	.L1113
.L1122:
	movq	%r9, %rax
	jmp	.L1112
.L1134:
	movq	%rcx, 16(%rbx)
	jmp	.L1114
	.cfi_endproc
.LFE14349:
	.size	_ZNSt10_HashtableIiSt4pairIKiSt3mapIiPN12v8_inspector22V8InspectorSessionImplESt4lessIiESaIS0_IS1_S5_EEEESaISB_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_, .-_ZNSt10_HashtableIiSt4pairIKiSt3mapIiPN12v8_inspector22V8InspectorSessionImplESt4lessIiESaIS0_IS1_S5_EEEESaISB_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	.section	.text._ZN12v8_inspector15V8InspectorImpl10disconnectEPNS_22V8InspectorSessionImplE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15V8InspectorImpl10disconnectEPNS_22V8InspectorSessionImplE
	.type	_ZN12v8_inspector15V8InspectorImpl10disconnectEPNS_22V8InspectorSessionImplE, @function
_ZN12v8_inspector15V8InspectorImpl10disconnectEPNS_22V8InspectorSessionImplE:
.LFB8309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movslq	16(%rsi), %r14
	movq	184(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	176(%rdi), %rax
	movq	%rax, -80(%rbp)
	movq	%r14, %rax
	movq	%r14, %rbx
	divq	%rsi
	movq	176(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r13
	testq	%rax, %rax
	je	.L1136
	movq	(%rax), %rcx
	movl	8(%rcx), %edi
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1197:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1136
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%rsi
	cmpq	%rdx, %r13
	jne	.L1136
.L1138:
	cmpl	%edi, %ebx
	jne	.L1197
	leaq	16(%rcx), %rbx
.L1162:
	movq	16(%rbx), %r8
	movl	20(%r15), %esi
	leaq	8(%rbx), %r13
	testq	%r8, %r8
	je	.L1139
	movq	%r13, %r12
	movq	%r8, %r14
	jmp	.L1140
	.p2align 4,,10
	.p2align 3
.L1199:
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.L1198
.L1164:
	movq	%rax, %r14
.L1140:
	cmpl	32(%r14), %esi
	jg	.L1199
	movq	16(%r14), %rax
	jge	.L1200
	movq	%r14, %r12
	testq	%rax, %rax
	jne	.L1164
.L1198:
	cmpq	%r12, 24(%rbx)
	jne	.L1158
	cmpq	%r12, %r13
	je	.L1155
.L1158:
	movq	40(%rbx), %rax
	.p2align 4,,10
	.p2align 3
.L1160:
	testq	%rax, %rax
	je	.L1156
.L1135:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1201
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1152:
	.cfi_restore_state
	movq	24(%rax), %rax
.L1153:
	testq	%rax, %rax
	je	.L1148
.L1202:
	cmpl	32(%rax), %esi
	jg	.L1152
	movq	%rax, %r14
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L1202
.L1148:
	cmpq	%r14, 24(%rbx)
	jne	.L1154
	cmpq	%r13, %r12
	jne	.L1154
	.p2align 4,,10
	.p2align 3
.L1155:
	movq	24(%r8), %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	call	_ZNSt8_Rb_treeIiSt4pairIKiPN12v8_inspector22V8InspectorSessionImplEESt10_Select1stIS5_ESt4lessIiESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	-72(%rbp), %r8
	movq	%r8, %rdi
	movq	16(%r8), %r8
	movq	%r8, -72(%rbp)
	movq	%r8, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %r8
	testq	%r8, %r8
	jne	.L1155
.L1157:
	movq	$0, 16(%rbx)
	movq	%r13, 24(%rbx)
	movq	%r13, 32(%rbx)
	movq	$0, 40(%rbx)
	.p2align 4,,10
	.p2align 3
.L1156:
	movl	16(%r15), %eax
	movq	-80(%rbp), %rdi
	leaq	-60(%rbp), %rsi
	movl	%eax, -60(%rbp)
	call	_ZNSt10_HashtableIiSt4pairIKiSt3mapIiPN12v8_inspector22V8InspectorSessionImplESt4lessIiESaIS0_IS1_S5_EEEESaISB_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	jmp	.L1135
	.p2align 4,,10
	.p2align 3
.L1200:
	movq	24(%r14), %rdx
	testq	%rdx, %rdx
	jne	.L1147
	jmp	.L1153
	.p2align 4,,10
	.p2align 3
.L1203:
	movq	%rdx, %r12
	movq	16(%rdx), %rdx
.L1151:
	testq	%rdx, %rdx
	je	.L1153
.L1147:
	cmpl	32(%rdx), %esi
	jl	.L1203
	movq	24(%rdx), %rdx
	jmp	.L1151
	.p2align 4,,10
	.p2align 3
.L1154:
	cmpq	%r12, %r14
	je	.L1158
	.p2align 4,,10
	.p2align 3
.L1159:
	movq	%r14, %rdi
	movq	%r14, -72(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-72(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r14
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	movq	40(%rbx), %rax
	subq	$1, %rax
	movq	%rax, 40(%rbx)
	cmpq	%r12, %r14
	jne	.L1159
	jmp	.L1160
	.p2align 4,,10
	.p2align 3
.L1136:
	movl	$64, %edi
	call	_Znwm@PLT
	movq	-80(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	movl	%ebx, 8(%rax)
	movq	%rax, %rcx
	leaq	24(%rax), %rax
	movl	$1, %r8d
	movq	$0, -24(%rax)
	movl	$0, (%rax)
	movq	$0, 8(%rax)
	movq	%rax, 40(%rcx)
	movq	%rax, 48(%rcx)
	movq	$0, 56(%rcx)
	call	_ZNSt10_HashtableIiSt4pairIKiSt3mapIiPN12v8_inspector22V8InspectorSessionImplESt4lessIiESaIS0_IS1_S5_EEEESaISB_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSD_10_Hash_nodeISB_Lb0EEEm
	leaq	16(%rax), %rbx
	jmp	.L1162
	.p2align 4,,10
	.p2align 3
.L1139:
	cmpq	%r13, 24(%rbx)
	jne	.L1158
	jmp	.L1157
.L1201:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8309:
	.size	_ZN12v8_inspector15V8InspectorImpl10disconnectEPNS_22V8InspectorSessionImplE, .-_ZN12v8_inspector15V8InspectorImpl10disconnectEPNS_22V8InspectorSessionImplE
	.weak	_ZTVN12v8_inspector15V8InspectorImplE
	.section	.data.rel.ro.local._ZTVN12v8_inspector15V8InspectorImplE,"awG",@progbits,_ZTVN12v8_inspector15V8InspectorImplE,comdat
	.align 8
	.type	_ZTVN12v8_inspector15V8InspectorImplE, @object
	.size	_ZTVN12v8_inspector15V8InspectorImplE, 184
_ZTVN12v8_inspector15V8InspectorImplE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector15V8InspectorImplD1Ev
	.quad	_ZN12v8_inspector15V8InspectorImplD0Ev
	.quad	_ZN12v8_inspector15V8InspectorImpl14contextCreatedERKNS_13V8ContextInfoE
	.quad	_ZN12v8_inspector15V8InspectorImpl16contextDestroyedEN2v85LocalINS1_7ContextEEE
	.quad	_ZN12v8_inspector15V8InspectorImpl17resetContextGroupEi
	.quad	_ZN12v8_inspector15V8InspectorImpl11contextByIdEi
	.quad	_ZN12v8_inspector15V8InspectorImpl11idleStartedEv
	.quad	_ZN12v8_inspector15V8InspectorImpl12idleFinishedEv
	.quad	_ZN12v8_inspector15V8InspectorImpl18asyncTaskScheduledERKNS_10StringViewEPvb
	.quad	_ZN12v8_inspector15V8InspectorImpl17asyncTaskCanceledEPv
	.quad	_ZN12v8_inspector15V8InspectorImpl16asyncTaskStartedEPv
	.quad	_ZN12v8_inspector15V8InspectorImpl17asyncTaskFinishedEPv
	.quad	_ZN12v8_inspector15V8InspectorImpl21allAsyncTasksCanceledEv
	.quad	_ZN12v8_inspector15V8InspectorImpl22storeCurrentStackTraceERKNS_10StringViewE
	.quad	_ZN12v8_inspector15V8InspectorImpl24externalAsyncTaskStartedERKNS_14V8StackTraceIdE
	.quad	_ZN12v8_inspector15V8InspectorImpl25externalAsyncTaskFinishedERKNS_14V8StackTraceIdE
	.quad	_ZN12v8_inspector15V8InspectorImpl15exceptionThrownEN2v85LocalINS1_7ContextEEERKNS_10StringViewENS2_INS1_5ValueEEES7_S7_jjSt10unique_ptrINS_12V8StackTraceESt14default_deleteISB_EEi
	.quad	_ZN12v8_inspector15V8InspectorImpl16exceptionRevokedEN2v85LocalINS1_7ContextEEEjRKNS_10StringViewE
	.quad	_ZN12v8_inspector15V8InspectorImpl7connectEiPNS_11V8Inspector7ChannelERKNS_10StringViewE
	.quad	_ZN12v8_inspector15V8InspectorImpl16createStackTraceEN2v85LocalINS1_10StackTraceEEE
	.quad	_ZN12v8_inspector15V8InspectorImpl17captureStackTraceEb
	.weak	_ZTVN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskE
	.section	.data.rel.ro.local._ZTVN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskE,"awG",@progbits,_ZTVN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskE,comdat
	.align 8
	.type	_ZTVN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskE, @object
	.size	_ZTVN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskE, 40
_ZTVN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskD1Ev
	.quad	_ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTaskD0Ev
	.quad	_ZN12v8_inspector15V8InspectorImpl13EvaluateScope13TerminateTask3RunEv
	.weak	_ZTVSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt15_Sp_counted_ptrIPN12v8_inspector15V8InspectorImpl13EvaluateScope11CancelTokenELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	1065353216
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
