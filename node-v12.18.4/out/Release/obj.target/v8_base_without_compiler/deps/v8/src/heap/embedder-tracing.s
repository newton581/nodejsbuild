	.file	"embedder-tracing.cc"
	.text
	.section	.text._ZN2v818EmbedderHeapTracer13TraceEpilogueEv,"axG",@progbits,_ZN2v818EmbedderHeapTracer13TraceEpilogueEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v818EmbedderHeapTracer13TraceEpilogueEv
	.type	_ZN2v818EmbedderHeapTracer13TraceEpilogueEv, @function
_ZN2v818EmbedderHeapTracer13TraceEpilogueEv:
.LFB2547:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2547:
	.size	_ZN2v818EmbedderHeapTracer13TraceEpilogueEv, .-_ZN2v818EmbedderHeapTracer13TraceEpilogueEv
	.section	.text._ZN2v818EmbedderHeapTracer13TraceEpilogueEPNS0_12TraceSummaryE,"axG",@progbits,_ZN2v818EmbedderHeapTracer13TraceEpilogueEPNS0_12TraceSummaryE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v818EmbedderHeapTracer13TraceEpilogueEPNS0_12TraceSummaryE
	.type	_ZN2v818EmbedderHeapTracer13TraceEpilogueEPNS0_12TraceSummaryE, @function
_ZN2v818EmbedderHeapTracer13TraceEpilogueEPNS0_12TraceSummaryE:
.LFB2548:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZN2v818EmbedderHeapTracer13TraceEpilogueEv(%rip), %rdx
	movq	56(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	jmp	*%rax
	.cfi_endproc
.LFE2548:
	.size	_ZN2v818EmbedderHeapTracer13TraceEpilogueEPNS0_12TraceSummaryE, .-_ZN2v818EmbedderHeapTracer13TraceEpilogueEPNS0_12TraceSummaryE
	.section	.text._ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope23FlushWrapperCacheIfFullEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope23FlushWrapperCacheIfFullEv.part.0, @function
_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope23FlushWrapperCacheIfFullEv.part.0:
.LFB22275:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	leaq	8(%rbx), %rsi
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	8(%rbx), %rax
	cmpq	16(%rbx), %rax
	je	.L7
	movq	%rax, 16(%rbx)
.L7:
	movq	24(%rbx), %rsi
	subq	%rax, %rsi
	cmpq	$15999, %rsi
	jbe	.L16
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movl	$16000, %edi
	call	_Znwm@PLT
	movq	16(%rbx), %rcx
	movq	8(%rbx), %rdi
	movq	%rax, %r12
	cmpq	%rdi, %rcx
	je	.L17
	subq	$16, %rcx
	xorl	%edx, %edx
	xorl	%eax, %eax
	subq	%rdi, %rcx
	shrq	$4, %rcx
	addq	$1, %rcx
	.p2align 4,,10
	.p2align 3
.L12:
	movdqu	(%rdi,%rdx), %xmm0
	addq	$1, %rax
	movups	%xmm0, (%r12,%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rcx
	ja	.L12
.L10:
	call	_ZdlPv@PLT
.L11:
	movq	%r12, 8(%rbx)
	movq	%r12, 16(%rbx)
	addq	$16000, %r12
	movq	%r12, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L11
	jmp	.L10
	.cfi_endproc
.LFE22275:
	.size	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope23FlushWrapperCacheIfFullEv.part.0, .-_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope23FlushWrapperCacheIfFullEv.part.0
	.section	.text._ZN2v88internal23LocalEmbedderHeapTracer15SetRemoteTracerEPNS_18EmbedderHeapTracerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23LocalEmbedderHeapTracer15SetRemoteTracerEPNS_18EmbedderHeapTracerE
	.type	_ZN2v88internal23LocalEmbedderHeapTracer15SetRemoteTracerEPNS_18EmbedderHeapTracerE, @function
_ZN2v88internal23LocalEmbedderHeapTracer15SetRemoteTracerEPNS_18EmbedderHeapTracerE:
.LFB18383:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L19
	movq	$0, 8(%rax)
.L19:
	movq	%rsi, 8(%rdi)
	testq	%rsi, %rsi
	je	.L18
	movq	(%rdi), %rax
	movq	%rax, 8(%rsi)
.L18:
	ret
	.cfi_endproc
.LFE18383:
	.size	_ZN2v88internal23LocalEmbedderHeapTracer15SetRemoteTracerEPNS_18EmbedderHeapTracerE, .-_ZN2v88internal23LocalEmbedderHeapTracer15SetRemoteTracerEPNS_18EmbedderHeapTracerE
	.section	.text._ZN2v88internal23LocalEmbedderHeapTracer13TracePrologueENS_18EmbedderHeapTracer10TraceFlagsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23LocalEmbedderHeapTracer13TracePrologueENS_18EmbedderHeapTracer10TraceFlagsE
	.type	_ZN2v88internal23LocalEmbedderHeapTracer13TracePrologueENS_18EmbedderHeapTracer10TraceFlagsE, @function
_ZN2v88internal23LocalEmbedderHeapTracer13TracePrologueENS_18EmbedderHeapTracer10TraceFlagsE:
.LFB18384:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L27
	movq	$0, 16(%rdi)
	movb	$0, 28(%rdi)
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	32(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L27:
	ret
	.cfi_endproc
.LFE18384:
	.size	_ZN2v88internal23LocalEmbedderHeapTracer13TracePrologueENS_18EmbedderHeapTracer10TraceFlagsE, .-_ZN2v88internal23LocalEmbedderHeapTracer13TracePrologueENS_18EmbedderHeapTracer10TraceFlagsE
	.section	.text._ZN2v88internal23LocalEmbedderHeapTracer13TraceEpilogueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23LocalEmbedderHeapTracer13TraceEpilogueEv
	.type	_ZN2v88internal23LocalEmbedderHeapTracer13TraceEpilogueEv, @function
_ZN2v88internal23LocalEmbedderHeapTracer13TraceEpilogueEv:
.LFB18385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L29
	movq	$0x000000000, -48(%rbp)
	leaq	_ZN2v818EmbedderHeapTracer13TraceEpilogueEPNS0_12TraceSummaryE(%rip), %rcx
	movq	$0, -40(%rbp)
	movq	(%rdi), %rax
	movq	64(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L31
	movq	56(%rax), %rax
	leaq	_ZN2v818EmbedderHeapTracer13TraceEpilogueEv(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L39
	movq	$0, 32(%rbx)
	movq	$0, 48(%rbx)
.L29:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L40
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	leaq	-48(%rbp), %rsi
	call	*%rdx
	movq	-40(%rbp), %rsi
	movsd	-48(%rbp), %xmm0
.L33:
	comisd	.LC1(%rip), %xmm0
	movq	%rsi, 32(%rbx)
	movq	$0, 48(%rbx)
	jbe	.L29
	movq	(%rbx), %rax
	movq	39600(%rax), %rdi
	call	_ZN2v88internal8GCTracer19RecordEmbedderSpeedEmd@PLT
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L39:
	call	*%rax
	movq	-40(%rbp), %rsi
	movsd	-48(%rbp), %xmm0
	jmp	.L33
.L40:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18385:
	.size	_ZN2v88internal23LocalEmbedderHeapTracer13TraceEpilogueEv, .-_ZN2v88internal23LocalEmbedderHeapTracer13TraceEpilogueEv
	.section	.text._ZN2v88internal23LocalEmbedderHeapTracer15EnterFinalPauseEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23LocalEmbedderHeapTracer15EnterFinalPauseEv
	.type	_ZN2v88internal23LocalEmbedderHeapTracer15EnterFinalPauseEv, @function
_ZN2v88internal23LocalEmbedderHeapTracer15EnterFinalPauseEv:
.LFB18389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L41
	movq	(%rdi), %rax
	movl	24(%rbx), %esi
	call	*72(%rax)
	movl	$0, 24(%rbx)
.L41:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18389:
	.size	_ZN2v88internal23LocalEmbedderHeapTracer15EnterFinalPauseEv, .-_ZN2v88internal23LocalEmbedderHeapTracer15EnterFinalPauseEv
	.section	.text._ZN2v88internal23LocalEmbedderHeapTracer5TraceEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23LocalEmbedderHeapTracer5TraceEd
	.type	_ZN2v88internal23LocalEmbedderHeapTracer5TraceEd, @function
_ZN2v88internal23LocalEmbedderHeapTracer5TraceEd:
.LFB18390:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L48
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L48:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE18390:
	.size	_ZN2v88internal23LocalEmbedderHeapTracer5TraceEd, .-_ZN2v88internal23LocalEmbedderHeapTracer5TraceEd
	.section	.text._ZN2v88internal23LocalEmbedderHeapTracer19IsRemoteTracingDoneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23LocalEmbedderHeapTracer19IsRemoteTracingDoneEv
	.type	_ZN2v88internal23LocalEmbedderHeapTracer19IsRemoteTracingDoneEv, @function
_ZN2v88internal23LocalEmbedderHeapTracer19IsRemoteTracingDoneEv:
.LFB18391:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L50
	movq	(%rdi), %rax
	jmp	*48(%rax)
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE18391:
	.size	_ZN2v88internal23LocalEmbedderHeapTracer19IsRemoteTracingDoneEv, .-_ZN2v88internal23LocalEmbedderHeapTracer19IsRemoteTracingDoneEv
	.section	.text._ZN2v88internal23LocalEmbedderHeapTracer40SetEmbedderStackStateForNextFinalizationENS_18EmbedderHeapTracer18EmbedderStackStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23LocalEmbedderHeapTracer40SetEmbedderStackStateForNextFinalizationENS_18EmbedderHeapTracer18EmbedderStackStateE
	.type	_ZN2v88internal23LocalEmbedderHeapTracer40SetEmbedderStackStateForNextFinalizationENS_18EmbedderHeapTracer18EmbedderStackStateE, @function
_ZN2v88internal23LocalEmbedderHeapTracer40SetEmbedderStackStateForNextFinalizationENS_18EmbedderHeapTracer18EmbedderStackStateE:
.LFB18392:
	.cfi_startproc
	endbr64
	cmpq	$0, 8(%rdi)
	je	.L51
	movl	%esi, 24(%rdi)
.L51:
	ret
	.cfi_endproc
.LFE18392:
	.size	_ZN2v88internal23LocalEmbedderHeapTracer40SetEmbedderStackStateForNextFinalizationENS_18EmbedderHeapTracer18EmbedderStackStateE, .-_ZN2v88internal23LocalEmbedderHeapTracer40SetEmbedderStackStateForNextFinalizationENS_18EmbedderHeapTracer18EmbedderStackStateE
	.section	.text._ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScopeC2EPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScopeC2EPS1_
	.type	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScopeC2EPS1_, @function
_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScopeC2EPS1_:
.LFB18403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movl	$16000, %edi
	call	_Znwm@PLT
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rdi
	movq	%rax, %r12
	cmpq	%rdi, %rdx
	je	.L54
	subq	$16, %rdx
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	subq	%rdi, %rdx
	shrq	$4, %rdx
	addq	$1, %rdx
.L56:
	movdqu	(%rdi,%rax), %xmm1
	addq	$1, %rcx
	movups	%xmm1, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rcx
	jb	.L56
.L55:
	call	_ZdlPv@PLT
.L57:
	movq	%r12, %xmm0
	addq	$16000, %r12
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, 24(%rbx)
	movups	%xmm0, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L57
	jmp	.L55
	.cfi_endproc
.LFE18403:
	.size	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScopeC2EPS1_, .-_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScopeC2EPS1_
	.globl	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScopeC1EPS1_
	.set	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScopeC1EPS1_,_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScopeC2EPS1_
	.section	.text._ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScopeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScopeD2Ev
	.type	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScopeD2Ev, @function
_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScopeD2Ev:
.LFB18406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	cmpq	%rdi, 8(%rbx)
	je	.L63
	movq	(%rbx), %rax
	leaq	8(%rbx), %rsi
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	8(%rbx), %rdi
.L63:
	testq	%rdi, %rdi
	je	.L62
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18406:
	.size	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScopeD2Ev, .-_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScopeD2Ev
	.globl	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScopeD1Ev
	.set	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScopeD1Ev,_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScopeD2Ev
	.section	.text._ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope23FlushWrapperCacheIfFullEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope23FlushWrapperCacheIfFullEv
	.type	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope23FlushWrapperCacheIfFullEv, @function
_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope23FlushWrapperCacheIfFullEv:
.LFB18417:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	cmpq	%rax, 16(%rdi)
	je	.L68
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	jmp	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope23FlushWrapperCacheIfFullEv.part.0
	.cfi_endproc
.LFE18417:
	.size	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope23FlushWrapperCacheIfFullEv, .-_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope23FlushWrapperCacheIfFullEv
	.section	.text._ZN2v88internal23LocalEmbedderHeapTracer31StartIncrementalMarkingIfNeededEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23LocalEmbedderHeapTracer31StartIncrementalMarkingIfNeededEv
	.type	_ZN2v88internal23LocalEmbedderHeapTracer31StartIncrementalMarkingIfNeededEv, @function
_ZN2v88internal23LocalEmbedderHeapTracer31StartIncrementalMarkingIfNeededEv:
.LFB18419:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal25FLAG_global_gc_schedulingE(%rip)
	jne	.L80
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	addq	$37592, %r12
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap28ShouldOptimizeForMemoryUsageEv@PLT
	movq	%r12, %rdi
	movl	$64, %edx
	movzbl	%al, %esi
	call	_ZN2v88internal4Heap49StartIncrementalMarkingIfAllocationLimitIsReachedEiNS_15GCCallbackFlagsE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap36AllocationLimitOvershotByLargeMarginEv@PLT
	testb	%al, %al
	jne	.L81
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$22, %esi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4Heap36FinalizeIncrementalMarkingAtomicallyENS0_23GarbageCollectionReasonE@PLT
	.cfi_endproc
.LFE18419:
	.size	_ZN2v88internal23LocalEmbedderHeapTracer31StartIncrementalMarkingIfNeededEv, .-_ZN2v88internal23LocalEmbedderHeapTracer31StartIncrementalMarkingIfNeededEv
	.section	.rodata._ZNSt6vectorISt4pairIPvS1_ESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_.str1.1,"aMS",@progbits,1
.LC2:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorISt4pairIPvS1_ESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt4pairIPvS1_ESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairIPvS1_ESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorISt4pairIPvS1_ESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorISt4pairIPvS1_ESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB21351:
	.cfi_startproc
	endbr64
	movabsq	$576460752303423487, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r8
	movq	(%rdi), %r13
	movq	%r8, %rax
	subq	%r13, %rax
	sarq	$4, %rax
	cmpq	%rcx, %rax
	je	.L103
	movq	%rsi, %r9
	movq	%rdi, %r15
	movq	%rsi, %r12
	subq	%r13, %r9
	testq	%rax, %rax
	je	.L94
	movabsq	$9223372036854775792, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L104
.L84:
	movq	%r14, %rdi
	movq	%rdx, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %r9
	movq	%rax, %rbx
	leaq	(%rax,%r14), %rsi
	movq	-56(%rbp), %r8
	leaq	16(%rax), %r14
	movdqu	(%rdx), %xmm3
	movups	%xmm3, (%rbx,%r9)
	cmpq	%r13, %r12
	je	.L86
.L105:
	leaq	-16(%r12), %rdi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	subq	%r13, %rdi
	movq	%rdi, %rax
	shrq	$4, %rax
	addq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L87:
	movdqu	0(%r13,%rdx), %xmm1
	addq	$1, %rcx
	movups	%xmm1, (%rbx,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rax
	ja	.L87
	leaq	32(%rbx,%rdi), %r14
	cmpq	%r8, %r12
	je	.L88
.L89:
	subq	%r12, %r8
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	leaq	-16(%r8), %rdi
	movq	%rdi, %rax
	shrq	$4, %rax
	addq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L91:
	movdqu	(%r12,%rdx), %xmm2
	addq	$1, %rcx
	movups	%xmm2, (%r14,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rax
	ja	.L91
	leaq	16(%r14,%rdi), %r14
.L90:
	testq	%r13, %r13
	jne	.L88
.L92:
	movq	%rbx, %xmm0
	movq	%r14, %xmm4
	movq	%rsi, 16(%r15)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L104:
	testq	%rsi, %rsi
	jne	.L85
	movdqu	(%rdx), %xmm3
	xorl	%ebx, %ebx
	movl	$16, %r14d
	movups	%xmm3, (%rbx,%r9)
	cmpq	%r13, %r12
	jne	.L105
.L86:
	cmpq	%r8, %r12
	jne	.L89
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L94:
	movl	$16, %r14d
	jmp	.L84
.L85:
	cmpq	%rcx, %rsi
	cmova	%rcx, %rsi
	salq	$4, %rsi
	movq	%rsi, %r14
	jmp	.L84
.L103:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE21351:
	.size	_ZNSt6vectorISt4pairIPvS1_ESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorISt4pairIPvS1_ESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.text._ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope24AddWrapperInfoForTestingESt4pairIPvS4_E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope24AddWrapperInfoForTestingESt4pairIPvS4_E
	.type	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope24AddWrapperInfoForTestingESt4pairIPvS4_E, @function
_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope24AddWrapperInfoForTestingESt4pairIPvS4_E:
.LFB18418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%rsi, -32(%rbp)
	movq	16(%rdi), %rsi
	movq	%rdx, -24(%rbp)
	cmpq	24(%rdi), %rsi
	je	.L107
	movdqu	-32(%rbp), %xmm0
	movups	%xmm0, (%rsi)
	movq	16(%rdi), %rax
	addq	$16, %rax
	movq	%rax, 16(%rdi)
.L108:
	cmpq	%rax, 24(%r12)
	jne	.L106
	movq	%r12, %rdi
	call	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope23FlushWrapperCacheIfFullEv.part.0
.L106:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	leaq	8(%rdi), %rdi
	call	_ZNSt6vectorISt4pairIPvS1_ESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	movq	16(%r12), %rax
	jmp	.L108
	.cfi_endproc
.LFE18418:
	.size	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope24AddWrapperInfoForTestingESt4pairIPvS4_E, .-_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope24AddWrapperInfoForTestingESt4pairIPvS4_E
	.section	.text._ZNSt6vectorISt4pairIPvS1_ESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt4pairIPvS1_ESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairIPvS1_ESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorISt4pairIPvS1_ESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorISt4pairIPvS1_ESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB21892:
	.cfi_startproc
	endbr64
	movabsq	$576460752303423487, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r8
	movq	(%rdi), %r13
	movq	%r8, %rax
	subq	%r13, %rax
	sarq	$4, %rax
	cmpq	%rcx, %rax
	je	.L132
	movq	%rsi, %r9
	movq	%rdi, %r15
	movq	%rsi, %r12
	subq	%r13, %r9
	testq	%rax, %rax
	je	.L123
	movabsq	$9223372036854775792, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L133
.L113:
	movq	%r14, %rdi
	movq	%rdx, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %r9
	movq	%rax, %rbx
	leaq	(%rax,%r14), %rsi
	movq	-56(%rbp), %r8
	leaq	16(%rax), %r14
	movdqu	(%rdx), %xmm3
	movups	%xmm3, (%rbx,%r9)
	cmpq	%r13, %r12
	je	.L115
.L134:
	leaq	-16(%r12), %rdi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	subq	%r13, %rdi
	movq	%rdi, %rax
	shrq	$4, %rax
	addq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L116:
	movdqu	0(%r13,%rdx), %xmm1
	addq	$1, %rcx
	movups	%xmm1, (%rbx,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rax
	ja	.L116
	leaq	32(%rbx,%rdi), %r14
	cmpq	%r8, %r12
	je	.L117
.L118:
	subq	%r12, %r8
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	leaq	-16(%r8), %rdi
	movq	%rdi, %rax
	shrq	$4, %rax
	addq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L120:
	movdqu	(%r12,%rdx), %xmm2
	addq	$1, %rcx
	movups	%xmm2, (%r14,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rax
	ja	.L120
	leaq	16(%r14,%rdi), %r14
.L119:
	testq	%r13, %r13
	jne	.L117
.L121:
	movq	%rbx, %xmm0
	movq	%r14, %xmm4
	movq	%rsi, 16(%r15)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L133:
	testq	%rsi, %rsi
	jne	.L114
	movdqu	(%rdx), %xmm3
	xorl	%ebx, %ebx
	movl	$16, %r14d
	movups	%xmm3, (%rbx,%r9)
	cmpq	%r13, %r12
	jne	.L134
.L115:
	cmpq	%r8, %r12
	jne	.L118
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L123:
	movl	$16, %r14d
	jmp	.L113
.L114:
	cmpq	%rcx, %rsi
	cmova	%rcx, %rsi
	salq	$4, %rsi
	movq	%rsi, %r14
	jmp	.L113
.L132:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE21892:
	.size	_ZNSt6vectorISt4pairIPvS1_ESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorISt4pairIPvS1_ESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.text._ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope20TracePossibleWrapperENS0_8JSObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope20TracePossibleWrapperENS0_8JSObjectE
	.type	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope20TracePossibleWrapperENS0_8JSObjectE, @function
_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope20TracePossibleWrapperENS0_8JSObjectE:
.LFB18408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	-1(%rsi), %r14
	movzbl	7(%r14), %eax
	sall	$3, %eax
	jne	.L164
.L135:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L165
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	movq	%rdi, %r13
	movzwl	11(%r14), %edi
	movl	%eax, %ebx
	movq	%rsi, %r12
	movl	$24, %eax
	cmpw	$1057, %di
	je	.L138
	movsbl	13(%r14), %esi
	shrl	$31, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
.L138:
	movzbl	7(%r14), %edx
	subl	%eax, %ebx
	movzbl	8(%r14), %ecx
	movl	%ebx, %eax
	sarl	$3, %eax
	subl	%ecx, %edx
	subl	%edx, %eax
	cmpl	$1, %eax
	jle	.L135
	movq	-1(%r12), %rdx
	movl	$24, %eax
	movzwl	11(%rdx), %edi
	cmpw	$1057, %di
	je	.L140
	movsbl	13(%rdx), %esi
	shrl	$31, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
	cltq
.L140:
	movq	-1(%rax,%r12), %rbx
	testb	$1, %bl
	jne	.L163
	testq	%rbx, %rbx
	jne	.L141
.L163:
	movq	16(%r13), %rsi
.L146:
	movq	24(%r13), %rax
.L143:
	cmpq	%rax, %rsi
	jne	.L135
	movq	%r13, %rdi
	call	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope23FlushWrapperCacheIfFullEv.part.0
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L141:
	movq	-1(%r12), %rdx
	movl	$32, %eax
	movzwl	11(%rdx), %edi
	cmpw	$1057, %di
	je	.L144
	movsbl	13(%rdx), %esi
	shrl	$31, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
	addl	$8, %eax
	cltq
.L144:
	movq	-1(%rax,%r12), %rdx
	movq	16(%r13), %rsi
	movq	24(%r13), %rax
	testb	$1, %dl
	jne	.L143
	movq	%rbx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	cmpq	%rsi, %rax
	je	.L145
	movq	%rbx, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rsi)
	movq	16(%r13), %rax
	leaq	16(%rax), %rsi
	movq	%rsi, 16(%r13)
	jmp	.L146
.L145:
	leaq	-64(%rbp), %rdx
	leaq	8(%r13), %rdi
	call	_ZNSt6vectorISt4pairIPvS1_ESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L163
.L165:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18408:
	.size	_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope20TracePossibleWrapperENS0_8JSObjectE, .-_ZN2v88internal23LocalEmbedderHeapTracer15ProcessingScope20TracePossibleWrapperENS0_8JSObjectE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal23LocalEmbedderHeapTracer15SetRemoteTracerEPNS_18EmbedderHeapTracerE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal23LocalEmbedderHeapTracer15SetRemoteTracerEPNS_18EmbedderHeapTracerE, @function
_GLOBAL__sub_I__ZN2v88internal23LocalEmbedderHeapTracer15SetRemoteTracerEPNS_18EmbedderHeapTracerE:
.LFB22235:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22235:
	.size	_GLOBAL__sub_I__ZN2v88internal23LocalEmbedderHeapTracer15SetRemoteTracerEPNS_18EmbedderHeapTracerE, .-_GLOBAL__sub_I__ZN2v88internal23LocalEmbedderHeapTracer15SetRemoteTracerEPNS_18EmbedderHeapTracerE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal23LocalEmbedderHeapTracer15SetRemoteTracerEPNS_18EmbedderHeapTracerE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1071644672
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
