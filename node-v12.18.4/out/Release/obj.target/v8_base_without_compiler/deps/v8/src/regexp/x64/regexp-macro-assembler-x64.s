	.file	"regexp-macro-assembler-x64.cc"
	.text
	.section	.text._ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv,"axG",@progbits,_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.type	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv, @function
_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv:
.LFB6897:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6897:
	.size	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv, .-_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.section	.text._ZN2v88internal20RegExpMacroAssembler21AbortedCodeGenerationEv,"axG",@progbits,_ZN2v88internal20RegExpMacroAssembler21AbortedCodeGenerationEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20RegExpMacroAssembler21AbortedCodeGenerationEv
	.type	_ZN2v88internal20RegExpMacroAssembler21AbortedCodeGenerationEv, @function
_ZN2v88internal20RegExpMacroAssembler21AbortedCodeGenerationEv:
.LFB9960:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9960:
	.size	_ZN2v88internal20RegExpMacroAssembler21AbortedCodeGenerationEv, .-_ZN2v88internal20RegExpMacroAssembler21AbortedCodeGenerationEv
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6417stack_limit_slackEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6417stack_limit_slackEv
	.type	_ZN2v88internal23RegExpMacroAssemblerX6417stack_limit_slackEv, @function
_ZN2v88internal23RegExpMacroAssemblerX6417stack_limit_slackEv:
.LFB19618:
	.cfi_startproc
	endbr64
	movl	$32, %eax
	ret
	.cfi_endproc
.LFE19618:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6417stack_limit_slackEv, .-_ZN2v88internal23RegExpMacroAssemblerX6417stack_limit_slackEv
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6414ImplementationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6414ImplementationEv
	.type	_ZN2v88internal23RegExpMacroAssemblerX6414ImplementationEv, @function
_ZN2v88internal23RegExpMacroAssemblerX6414ImplementationEv:
.LFB19651:
	.cfi_startproc
	endbr64
	movl	$6, %eax
	ret
	.cfi_endproc
.LFE19651:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6414ImplementationEv, .-_ZN2v88internal23RegExpMacroAssemblerX6414ImplementationEv
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX647SucceedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX647SucceedEv
	.type	_ZN2v88internal23RegExpMacroAssemblerX647SucceedEv, @function
_ZN2v88internal23RegExpMacroAssemblerX647SucceedEv:
.LFB19663:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	652(%rdi), %rsi
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	32(%rdi), %rdi
	subq	$8, %rsp
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movl	12(%rbx), %eax
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19663:
	.size	_ZN2v88internal23RegExpMacroAssemblerX647SucceedEv, .-_ZN2v88internal23RegExpMacroAssemblerX647SucceedEv
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX644BindEPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX644BindEPNS0_5LabelE
	.type	_ZN2v88internal23RegExpMacroAssemblerX644BindEPNS0_5LabelE, @function
_ZN2v88internal23RegExpMacroAssemblerX644BindEPNS0_5LabelE:
.LFB19622:
	.cfi_startproc
	endbr64
	addq	$32, %rdi
	jmp	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	.cfi_endproc
.LFE19622:
	.size	_ZN2v88internal23RegExpMacroAssemblerX644BindEPNS0_5LabelE, .-_ZN2v88internal23RegExpMacroAssemblerX644BindEPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX644FailEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX644FailEv
	.type	_ZN2v88internal23RegExpMacroAssemblerX644FailEv, @function
_ZN2v88internal23RegExpMacroAssemblerX644FailEv:
.LFB19642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	32(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	12(%rdi), %eax
	movq	%rdi, %rbx
	testl	%eax, %eax
	je	.L12
.L10:
	leaq	668(%rbx), %rsi
	movq	%r12, %rdi
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movl	_ZN2v88internalL3raxE(%rip), %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	jmp	.L10
	.cfi_endproc
.LFE19642:
	.size	_ZN2v88internal23RegExpMacroAssemblerX644FailEv, .-_ZN2v88internal23RegExpMacroAssemblerX644FailEv
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6422AdvanceCurrentPositionEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6422AdvanceCurrentPositionEi
	.type	_ZN2v88internal23RegExpMacroAssemblerX6422AdvanceCurrentPositionEi, @function
_ZN2v88internal23RegExpMacroAssemblerX6422AdvanceCurrentPositionEi:
.LFB19619:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jne	.L15
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	imull	624(%rdi), %esi
	movl	$8, %r8d
	addq	$32, %rdi
	movabsq	$81604378624, %rcx
	movl	$7, %edx
	orq	%rsi, %rcx
	xorl	%esi, %esi
	jmp	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	.cfi_endproc
.LFE19619:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6422AdvanceCurrentPositionEi, .-_ZN2v88internal23RegExpMacroAssemblerX6422AdvanceCurrentPositionEi
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6419PushCurrentPositionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6419PushCurrentPositionEv
	.type	_ZN2v88internal23RegExpMacroAssemblerX6419PushCurrentPositionEv, @function
_ZN2v88internal23RegExpMacroAssemblerX6419PushCurrentPositionEv:
.LFB19656:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %r8d
	movl	$1, %edx
	movabsq	$81604378628, %rcx
	movl	$5, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	32(%rdi), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	xorl	%edx, %edx
	leaq	-36(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-36(%rbp), %rsi
	movl	-28(%rbp), %edx
	movq	%r12, %rdi
	movl	$4, %r8d
	movl	$7, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L19
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L19:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19656:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6419PushCurrentPositionEv, .-_ZN2v88internal23RegExpMacroAssemblerX6419PushCurrentPositionEv
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6418PopCurrentPositionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6418PopCurrentPositionEv
	.type	_ZN2v88internal23RegExpMacroAssemblerX6418PopCurrentPositionEv, @function
_ZN2v88internal23RegExpMacroAssemblerX6418PopCurrentPositionEv:
.LFB19653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	32(%rdi), %r12
	leaq	-36(%rbp), %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-28(%rbp), %ecx
	movq	-36(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	call	_ZN2v88internal9Assembler7movsxlqENS0_8RegisterENS0_7OperandE@PLT
	xorl	%esi, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movabsq	$81604378628, %rcx
	movl	$1, %edx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L23
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L23:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19653:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6418PopCurrentPositionEv, .-_ZN2v88internal23RegExpMacroAssemblerX6418PopCurrentPositionEv
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX649BacktrackEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX649BacktrackEv
	.type	_ZN2v88internal23RegExpMacroAssemblerX649BacktrackEv, @function
_ZN2v88internal23RegExpMacroAssemblerX649BacktrackEv:
.LFB19621:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-60(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	32(%rbx), %r12
	subq	$40, %rsp
	movq	544(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -60(%rbp)
	call	_ZN2v88internal17ExternalReference18address_of_jslimitEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8load_raxENS0_17ExternalReferenceE@PLT
	movl	$8, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movl	$4, %edx
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$7, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	leaq	676(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4callEPNS0_5LabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	xorl	%edx, %edx
	leaq	-52(%rbp), %rdi
	movl	$1, %esi
	movl	_ZN2v88internalL3rbxE(%rip), %r13d
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-44(%rbp), %ecx
	movq	-52(%rbp), %rdx
	movq	%r12, %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler7movsxlqENS0_8RegisterENS0_7OperandE@PLT
	xorl	%esi, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movabsq	$81604378628, %rcx
	movl	$1, %edx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	$8, %r8d
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	$3, %edx
	movl	$3, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpENS0_8RegisterE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L27
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L27:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19621:
	.size	_ZN2v88internal23RegExpMacroAssemblerX649BacktrackEv, .-_ZN2v88internal23RegExpMacroAssemblerX649BacktrackEv
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6414ClearRegistersEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6414ClearRegistersEii
	.type	_ZN2v88internal23RegExpMacroAssemblerX6414ClearRegistersEii, @function
_ZN2v88internal23RegExpMacroAssemblerX6414ClearRegistersEii:
.LFB19665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	32(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$72, %rsp
	movq	%rdi, -112(%rbp)
	movq	%r14, %rdi
	movl	_ZN2v88internalL3rbpE(%rip), %r15d
	movl	%edx, -100(%rbp)
	movl	$-72, %edx
	movl	%r15d, %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movl	$8, %r8d
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	-100(%rbp), %r9d
	leal	1(%r9), %eax
	movl	%eax, -100(%rbp)
	cmpl	%r9d, %ebx
	jg	.L28
	.p2align 4,,10
	.p2align 3
.L32:
	movq	-112(%rbp), %rax
	movl	%ebx, %edx
	addl	$1, %ebx
	cmpl	%edx, 628(%rax)
	jg	.L30
	movl	%ebx, 628(%rax)
.L30:
	movl	%ebx, %edx
	movl	%r15d, %esi
	movq	%r14, %rdi
	negl	%edx
	leal	-72(,%rdx,8), %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rsi
	movl	-72(%rbp), %edx
	movl	%r13d, %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	cmpl	-100(%rbp), %ebx
	jne	.L32
.L28:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L36
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L36:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19665:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6414ClearRegistersEii, .-_ZN2v88internal23RegExpMacroAssemblerX6414ClearRegistersEii
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6413PushBacktrackEPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6413PushBacktrackEPNS0_5LabelE
	.type	_ZN2v88internal23RegExpMacroAssemblerX6413PushBacktrackEPNS0_5LabelE, @function
_ZN2v88internal23RegExpMacroAssemblerX6413PushBacktrackEPNS0_5LabelE:
.LFB19655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %r8d
	movl	$1, %edx
	movabsq	$81604378628, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	movl	$5, %esi
	pushq	%r12
	.cfi_offset 12, -40
	leaq	32(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	leaq	-52(%rbp), %rdi
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-52(%rbp), %rsi
	movl	-44(%rbp), %edx
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4movlENS0_7OperandEPNS0_5LabelE@PLT
	movq	616(%rbx), %rax
	movq	64(%rbx), %r14
	subq	48(%rbx), %r14
	testq	%rax, %rax
	je	.L47
.L38:
	movl	4(%rax), %edx
	cmpl	(%rax), %edx
	je	.L48
.L41:
	movl	%r14d, 24(%rax,%rdx,4)
	movq	616(%rbx), %rax
	leaq	-60(%rbp), %r13
	addl	$1, 4(%rax)
	movq	544(%rbx), %rdi
	addq	$1, 600(%rbx)
	movq	$0, -60(%rbp)
	call	_ZN2v88internal17ExternalReference37address_of_regexp_stack_limit_addressEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8load_raxENS0_17ExternalReferenceE@PLT
	movl	$8, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movl	$1, %edx
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$7, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	leaq	684(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4callEPNS0_5LabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L49
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L50
.L42:
	movq	%rax, 616(%rbx)
	movl	4(%rax), %edx
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L47:
	movq	592(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$55, %rdx
	jbe	.L51
	leaq	56(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L40:
	pxor	%xmm0, %xmm0
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 608(%rbx)
	movq	%rax, 616(%rbx)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L50:
	leal	(%rdx,%rdx), %r13d
	movl	$256, %eax
	movq	592(%rbx), %rdi
	cmpl	$256, %r13d
	cmova	%eax, %r13d
	movq	24(%rdi), %rdx
	movl	%r13d, %eax
	leaq	31(,%rax,4), %rsi
	movq	16(%rdi), %rax
	andq	$-8, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L52
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L44:
	pxor	%xmm0, %xmm0
	movl	$0, 4(%rax)
	movups	%xmm0, 8(%rax)
	movl	%r13d, (%rax)
	movq	616(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	616(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	616(%rbx), %rax
	movq	8(%rax), %rax
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L51:
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L52:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L44
.L49:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19655:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6413PushBacktrackEPNS0_5LabelE, .-_ZN2v88internal23RegExpMacroAssemblerX6413PushBacktrackEPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6411SetRegisterEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6411SetRegisterEii
	.type	_ZN2v88internal23RegExpMacroAssemblerX6411SetRegisterEii, @function
_ZN2v88internal23RegExpMacroAssemblerX6411SetRegisterEii:
.LFB19662:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	32(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	628(%rdi), %esi
	jl	.L54
	leal	1(%rsi), %eax
	movl	%eax, 628(%rdi)
.L54:
	movl	$-10, %edx
	leaq	-48(%rbp), %rdi
	subl	%esi, %edx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	sall	$3, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	%ebx, %ecx
	movq	-48(%rbp), %rsi
	movl	-40(%rbp), %edx
	movabsq	$81604378624, %rbx
	movl	$8, %r8d
	movq	%r12, %rdi
	orq	%rbx, %rcx
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_9ImmediateEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L57
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L57:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19662:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6411SetRegisterEii, .-_ZN2v88internal23RegExpMacroAssemblerX6411SetRegisterEii
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6431ReadCurrentPositionFromRegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6431ReadCurrentPositionFromRegisterEi
	.type	_ZN2v88internal23RegExpMacroAssemblerX6431ReadCurrentPositionFromRegisterEi, @function
_ZN2v88internal23RegExpMacroAssemblerX6431ReadCurrentPositionFromRegisterEi:
.LFB19658:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	32(%rdi), %r12
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	628(%rdi), %esi
	jl	.L59
	leal	1(%rsi), %eax
	movl	%eax, 628(%rdi)
.L59:
	movl	$-10, %edx
	leaq	-48(%rbp), %rdi
	subl	%esi, %edx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	sall	$3, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-40(%rbp), %ecx
	movq	-48(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L62
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L62:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19658:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6431ReadCurrentPositionFromRegisterEi, .-_ZN2v88internal23RegExpMacroAssemblerX6431ReadCurrentPositionFromRegisterEi
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6411PopRegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6411PopRegisterEi
	.type	_ZN2v88internal23RegExpMacroAssemblerX6411PopRegisterEi, @function
_ZN2v88internal23RegExpMacroAssemblerX6411PopRegisterEi:
.LFB19654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	32(%rdi), %r12
	leaq	-52(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	movl	$1, %esi
	subq	$48, %rsp
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-44(%rbp), %ecx
	movq	-52(%rbp), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler7movsxlqENS0_8RegisterENS0_7OperandE@PLT
	xorl	%esi, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movabsq	$81604378628, %rcx
	movl	$1, %edx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	cmpl	628(%r14), %ebx
	jl	.L64
	leal	1(%rbx), %eax
	movl	%eax, 628(%r14)
.L64:
	movl	$-10, %edx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	leaq	-64(%rbp), %rdi
	subl	%ebx, %edx
	sall	$3, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-64(%rbp), %rsi
	movl	-56(%rbp), %edx
	movl	%r13d, %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rsi, -52(%rbp)
	movl	%edx, -44(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L67
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L67:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19654:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6411PopRegisterEi, .-_ZN2v88internal23RegExpMacroAssemblerX6411PopRegisterEi
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6416CheckCharacterLTEtPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6416CheckCharacterLTEtPNS0_5LabelE
	.type	_ZN2v88internal23RegExpMacroAssemblerX6416CheckCharacterLTEtPNS0_5LabelE, @function
_ZN2v88internal23RegExpMacroAssemblerX6416CheckCharacterLTEtPNS0_5LabelE:
.LFB19627:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzwl	%si, %ecx
	movl	$4, %r8d
	movabsq	$81604378624, %rsi
	orq	%rsi, %rcx
	movl	$7, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	32(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	movl	$2, %edx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	leaq	660(%rbx), %rdx
	movl	$1, %ecx
	testq	%r12, %r12
	je	.L71
	movl	$1, %ecx
	movq	%r12, %rdx
.L71:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$12, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	.cfi_endproc
.LFE19627:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6416CheckCharacterLTEtPNS0_5LabelE, .-_ZN2v88internal23RegExpMacroAssemblerX6416CheckCharacterLTEtPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6416CheckCharacterGTEtPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6416CheckCharacterGTEtPNS0_5LabelE
	.type	_ZN2v88internal23RegExpMacroAssemblerX6416CheckCharacterGTEtPNS0_5LabelE, @function
_ZN2v88internal23RegExpMacroAssemblerX6416CheckCharacterGTEtPNS0_5LabelE:
.LFB19624:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzwl	%si, %ecx
	movl	$4, %r8d
	movabsq	$81604378624, %rsi
	orq	%rsi, %rcx
	movl	$7, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	32(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	movl	$2, %edx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	leaq	660(%rbx), %rdx
	movl	$1, %ecx
	testq	%r12, %r12
	je	.L75
	movl	$1, %ecx
	movq	%r12, %rdx
.L75:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$15, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	.cfi_endproc
.LFE19624:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6416CheckCharacterGTEtPNS0_5LabelE, .-_ZN2v88internal23RegExpMacroAssemblerX6416CheckCharacterGTEtPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6414CheckCharacterEjPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6414CheckCharacterEjPNS0_5LabelE
	.type	_ZN2v88internal23RegExpMacroAssemblerX6414CheckCharacterEjPNS0_5LabelE, @function
_ZN2v88internal23RegExpMacroAssemblerX6414CheckCharacterEjPNS0_5LabelE:
.LFB19623:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$4, %r8d
	movabsq	$81604378624, %rsi
	orq	%rsi, %rcx
	movl	$7, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	32(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	movl	$2, %edx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	leaq	660(%rbx), %rdx
	movl	$1, %ecx
	testq	%r12, %r12
	je	.L79
	movl	$1, %ecx
	movq	%r12, %rdx
.L79:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$4, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	.cfi_endproc
.LFE19623:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6414CheckCharacterEjPNS0_5LabelE, .-_ZN2v88internal23RegExpMacroAssemblerX6414CheckCharacterEjPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6417CheckNotCharacterEjPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6417CheckNotCharacterEjPNS0_5LabelE
	.type	_ZN2v88internal23RegExpMacroAssemblerX6417CheckNotCharacterEjPNS0_5LabelE, @function
_ZN2v88internal23RegExpMacroAssemblerX6417CheckNotCharacterEjPNS0_5LabelE:
.LFB19634:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$4, %r8d
	movabsq	$81604378624, %rsi
	orq	%rsi, %rcx
	movl	$7, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	32(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	movl	$2, %edx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	leaq	660(%rbx), %rdx
	movl	$1, %ecx
	testq	%r12, %r12
	je	.L83
	movl	$1, %ecx
	movq	%r12, %rdx
.L83:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$5, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	.cfi_endproc
.LFE19634:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6417CheckNotCharacterEjPNS0_5LabelE, .-_ZN2v88internal23RegExpMacroAssemblerX6417CheckNotCharacterEjPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX644GoToEPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX644GoToEPNS0_5LabelE
	.type	_ZN2v88internal23RegExpMacroAssemblerX644GoToEPNS0_5LabelE, @function
_ZN2v88internal23RegExpMacroAssemblerX644GoToEPNS0_5LabelE:
.LFB19647:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L86
	addq	$32, %rdi
	movl	$1, %edx
	jmp	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	.p2align 4,,10
	.p2align 3
.L86:
	movq	(%rdi), %rax
	jmp	*56(%rax)
	.cfi_endproc
.LFE19647:
	.size	_ZN2v88internal23RegExpMacroAssemblerX644GoToEPNS0_5LabelE, .-_ZN2v88internal23RegExpMacroAssemblerX644GoToEPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6428ReadStackPointerFromRegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6428ReadStackPointerFromRegisterEi
	.type	_ZN2v88internal23RegExpMacroAssemblerX6428ReadStackPointerFromRegisterEi, @function
_ZN2v88internal23RegExpMacroAssemblerX6428ReadStackPointerFromRegisterEi:
.LFB19660:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	32(%rdi), %r12
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	628(%rdi), %esi
	jl	.L88
	leal	1(%rsi), %eax
	movl	%eax, 628(%rdi)
.L88:
	movl	_ZN2v88internalL3rbpE(%rip), %r13d
	movl	$-10, %edx
	leaq	-48(%rbp), %rdi
	subl	%esi, %edx
	sall	$3, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-40(%rbp), %ecx
	movq	-48(%rbp), %rdx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$1, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	leaq	-84(%rbp), %rdi
	movl	$16, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-84(%rbp), %rcx
	movl	-76(%rbp), %r8d
	movq	%r12, %rdi
	movl	$8, %r9d
	movl	$1, %edx
	movl	$3, %esi
	movq	%rcx, -48(%rbp)
	movl	%r8d, -40(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L91
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L91:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19660:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6428ReadStackPointerFromRegisterEi, .-_ZN2v88internal23RegExpMacroAssemblerX6428ReadStackPointerFromRegisterEi
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6427WriteStackPointerToRegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6427WriteStackPointerToRegisterEi
	.type	_ZN2v88internal23RegExpMacroAssemblerX6427WriteStackPointerToRegisterEi, @function
_ZN2v88internal23RegExpMacroAssemblerX6427WriteStackPointerToRegisterEi:
.LFB19666:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	32(%rdi), %r13
	pushq	%r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$56, %rsp
	movl	_ZN2v88internalL3raxE(%rip), %r12d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%r12d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %r14d
	leaq	-92(%rbp), %rdi
	movl	$16, %edx
	movl	%r14d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-92(%rbp), %rcx
	movl	-84(%rbp), %r8d
	xorl	%edx, %edx
	movl	$8, %r9d
	movl	$43, %esi
	movq	%r13, %rdi
	movq	%rcx, -80(%rbp)
	movl	%r8d, -72(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	cmpl	628(%r15), %ebx
	jl	.L93
	leal	1(%rbx), %eax
	movl	%eax, 628(%r15)
.L93:
	movl	$-10, %edx
	leaq	-80(%rbp), %rdi
	movl	%r14d, %esi
	subl	%ebx, %edx
	sall	$3, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rsi
	movl	-72(%rbp), %edx
	movl	%r12d, %ecx
	movl	$8, %r8d
	movq	%r13, %rdi
	movq	%rsi, -92(%rbp)
	movl	%edx, -84(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L96
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L96:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19666:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6427WriteStackPointerToRegisterEi, .-_ZN2v88internal23RegExpMacroAssemblerX6427WriteStackPointerToRegisterEi
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6415AdvanceRegisterEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6415AdvanceRegisterEii
	.type	_ZN2v88internal23RegExpMacroAssemblerX6415AdvanceRegisterEii, @function
_ZN2v88internal23RegExpMacroAssemblerX6415AdvanceRegisterEii:
.LFB19620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	%edx, %ebx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%ebx, %ebx
	jne	.L105
.L97:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L106
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	leaq	32(%rdi), %r12
	cmpl	628(%rdi), %esi
	jl	.L99
	leal	1(%rsi), %eax
	movl	%eax, 628(%rdi)
.L99:
	movl	$-10, %edx
	leaq	-48(%rbp), %rdi
	subl	%esi, %edx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	sall	$3, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-40(%rbp), %ecx
	movq	-48(%rbp), %rdx
	xorl	%esi, %esi
	movabsq	$81604378624, %r8
	movl	$8, %r9d
	movq	%r12, %rdi
	orq	%rbx, %r8
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L97
.L106:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19620:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6415AdvanceRegisterEii, .-_ZN2v88internal23RegExpMacroAssemblerX6415AdvanceRegisterEii
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6415IfRegisterEqPosEiPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6415IfRegisterEqPosEiPNS0_5LabelE
	.type	_ZN2v88internal23RegExpMacroAssemblerX6415IfRegisterEqPosEiPNS0_5LabelE, @function
_ZN2v88internal23RegExpMacroAssemblerX6415IfRegisterEqPosEiPNS0_5LabelE:
.LFB19650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	32(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	628(%rdi), %esi
	jl	.L108
	leal	1(%rsi), %eax
	movl	%eax, 628(%rdi)
.L108:
	movl	$-10, %edx
	leaq	-64(%rbp), %rdi
	subl	%esi, %edx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	sall	$3, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-64(%rbp), %rcx
	movl	-56(%rbp), %r8d
	movq	%r13, %rdi
	movl	$8, %r9d
	movl	$7, %edx
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	testq	%r12, %r12
	je	.L113
	movl	$1, %ecx
	movq	%r12, %rdx
	movl	$4, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
.L107:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L114
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	leaq	660(%rbx), %rdx
	movl	$1, %ecx
	movl	$4, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L107
.L114:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19650:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6415IfRegisterEqPosEiPNS0_5LabelE, .-_ZN2v88internal23RegExpMacroAssemblerX6415IfRegisterEqPosEiPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6412IfRegisterLTEiiPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6412IfRegisterLTEiiPNS0_5LabelE
	.type	_ZN2v88internal23RegExpMacroAssemblerX6412IfRegisterLTEiiPNS0_5LabelE, @function
_ZN2v88internal23RegExpMacroAssemblerX6412IfRegisterLTEiiPNS0_5LabelE:
.LFB19649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	32(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	628(%rdi), %esi
	jl	.L116
	leal	1(%rsi), %eax
	movl	%eax, 628(%rdi)
.L116:
	movl	$-10, %edx
	leaq	-64(%rbp), %rdi
	subl	%esi, %edx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	sall	$3, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %rdx
	movq	%r13, %rdi
	movabsq	$81604378624, %r8
	movl	$8, %r9d
	movl	$7, %esi
	orq	%r12, %r8
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	testq	%r14, %r14
	je	.L121
	movl	$1, %ecx
	movq	%r14, %rdx
	movl	$12, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
.L115:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L122
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	leaq	660(%rbx), %rdx
	movl	$1, %ecx
	movl	$12, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L115
.L122:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19649:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6412IfRegisterLTEiiPNS0_5LabelE, .-_ZN2v88internal23RegExpMacroAssemblerX6412IfRegisterLTEiiPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6412IfRegisterGEEiiPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6412IfRegisterGEEiiPNS0_5LabelE
	.type	_ZN2v88internal23RegExpMacroAssemblerX6412IfRegisterGEEiiPNS0_5LabelE, @function
_ZN2v88internal23RegExpMacroAssemblerX6412IfRegisterGEEiiPNS0_5LabelE:
.LFB19648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	32(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	628(%rdi), %esi
	jl	.L124
	leal	1(%rsi), %eax
	movl	%eax, 628(%rdi)
.L124:
	movl	$-10, %edx
	leaq	-64(%rbp), %rdi
	subl	%esi, %edx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	sall	$3, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %rdx
	movq	%r13, %rdi
	movabsq	$81604378624, %r8
	movl	$8, %r9d
	movl	$7, %esi
	orq	%r12, %r8
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	testq	%r14, %r14
	je	.L129
	movl	$1, %ecx
	movq	%r14, %rdx
	movl	$13, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
.L123:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L130
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	leaq	660(%rbx), %rdx
	movl	$1, %ecx
	movl	$13, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L123
.L130:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19648:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6412IfRegisterGEEiiPNS0_5LabelE, .-_ZN2v88internal23RegExpMacroAssemblerX6412IfRegisterGEEiiPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6412PushRegisterEiNS0_20RegExpMacroAssembler14StackCheckFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6412PushRegisterEiNS0_20RegExpMacroAssembler14StackCheckFlagE
	.type	_ZN2v88internal23RegExpMacroAssemblerX6412PushRegisterEiNS0_20RegExpMacroAssembler14StackCheckFlagE, @function
_ZN2v88internal23RegExpMacroAssemblerX6412PushRegisterEiNS0_20RegExpMacroAssembler14StackCheckFlagE:
.LFB19657:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	leaq	32(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	628(%rdi), %esi
	jl	.L132
	leal	1(%rsi), %eax
	movl	%eax, 628(%rdi)
.L132:
	movl	$-10, %edx
	leaq	-64(%rbp), %r14
	subl	%esi, %edx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movq	%r14, %rdi
	sall	$3, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$8, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movabsq	$81604378628, %rcx
	movl	$5, %esi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-64(%rbp), %rsi
	movl	-56(%rbp), %edx
	xorl	%ecx, %ecx
	movl	$4, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	testl	%r13d, %r13d
	jne	.L139
.L131:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L140
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movq	544(%rbx), %rdi
	movq	$0, -96(%rbp)
	leaq	-96(%rbp), %r13
	call	_ZN2v88internal17ExternalReference37address_of_regexp_stack_limit_addressEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8load_raxENS0_17ExternalReferenceE@PLT
	movl	$8, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movl	$1, %edx
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$7, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	leaq	684(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4callEPNS0_5LabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L131
.L140:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19657:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6412PushRegisterEiNS0_20RegExpMacroAssembler14StackCheckFlagE, .-_ZN2v88internal23RegExpMacroAssemblerX6412PushRegisterEiNS0_20RegExpMacroAssembler14StackCheckFlagE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6430WriteCurrentPositionToRegisterEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6430WriteCurrentPositionToRegisterEii
	.type	_ZN2v88internal23RegExpMacroAssemblerX6430WriteCurrentPositionToRegisterEii, @function
_ZN2v88internal23RegExpMacroAssemblerX6430WriteCurrentPositionToRegisterEii:
.LFB19664:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	32(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jne	.L142
	cmpl	628(%rdi), %esi
	jge	.L148
.L143:
	movl	$-10, %edx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	leaq	-52(%rbp), %rdi
	subl	%r12d, %edx
	sall	$3, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-52(%rbp), %rsi
	movl	-44(%rbp), %edx
	movq	%r13, %rdi
	movl	_ZN2v88internalL3rdiE(%rip), %ecx
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
.L141:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L149
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	imull	624(%rdi), %edx
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	leaq	-52(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-52(%rbp), %rdx
	movl	-44(%rbp), %ecx
	movq	%r13, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %r14d
	movl	$8, %r8d
	movq	%rdx, -64(%rbp)
	movl	%r14d, %esi
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	cmpl	628(%rbx), %r12d
	jl	.L145
	leal	1(%r12), %eax
	movl	%eax, 628(%rbx)
.L145:
	movl	$-10, %edx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	leaq	-64(%rbp), %rdi
	subl	%r12d, %edx
	sall	$3, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-64(%rbp), %rsi
	movl	-56(%rbp), %edx
	movl	%r14d, %ecx
	movl	$8, %r8d
	movq	%r13, %rdi
	movq	%rsi, -52(%rbp)
	movl	%edx, -44(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L148:
	leal	1(%rsi), %eax
	movl	%eax, 628(%rdi)
	jmp	.L143
.L149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19664:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6430WriteCurrentPositionToRegisterEii, .-_ZN2v88internal23RegExpMacroAssemblerX6430WriteCurrentPositionToRegisterEii
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6421CheckCharacterInRangeEttPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6421CheckCharacterInRangeEttPNS0_5LabelE
	.type	_ZN2v88internal23RegExpMacroAssemblerX6421CheckCharacterInRangeEttPNS0_5LabelE, @function
_ZN2v88internal23RegExpMacroAssemblerX6421CheckCharacterInRangeEttPNS0_5LabelE:
.LFB19638:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movzwl	%si, %r13d
	movl	$2, %esi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	32(%rdi), %r12
	leaq	-80(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	movl	%r13d, %edx
	negl	%edx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$4, %r8d
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movzwl	%bx, %ecx
	xorl	%edx, %edx
	movl	$4, %r8d
	subl	%r13d, %ecx
	movl	$7, %esi
	movq	%r12, %rdi
	movabsq	$81604378624, %rbx
	orq	%rbx, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	testq	%r14, %r14
	je	.L155
	movl	$1, %ecx
	movq	%r14, %rdx
	movl	$6, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
.L150:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L156
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	leaq	660(%r15), %rdx
	movl	$1, %ecx
	movl	$6, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L150
.L156:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19638:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6421CheckCharacterInRangeEttPNS0_5LabelE, .-_ZN2v88internal23RegExpMacroAssemblerX6421CheckCharacterInRangeEttPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6424CheckCharacterNotInRangeEttPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6424CheckCharacterNotInRangeEttPNS0_5LabelE
	.type	_ZN2v88internal23RegExpMacroAssemblerX6424CheckCharacterNotInRangeEttPNS0_5LabelE, @function
_ZN2v88internal23RegExpMacroAssemblerX6424CheckCharacterNotInRangeEttPNS0_5LabelE:
.LFB19639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movzwl	%si, %r13d
	movl	$2, %esi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	32(%rdi), %r12
	leaq	-80(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	movl	%r13d, %edx
	negl	%edx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$4, %r8d
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movzwl	%bx, %ecx
	xorl	%edx, %edx
	movl	$4, %r8d
	subl	%r13d, %ecx
	movl	$7, %esi
	movq	%r12, %rdi
	movabsq	$81604378624, %rbx
	orq	%rbx, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	testq	%r14, %r14
	je	.L162
	movl	$1, %ecx
	movq	%r14, %rdx
	movl	$7, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
.L157:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L163
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	leaq	660(%r15), %rdx
	movl	$1, %ecx
	movl	$7, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L157
.L163:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19639:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6424CheckCharacterNotInRangeEttPNS0_5LabelE, .-_ZN2v88internal23RegExpMacroAssemblerX6424CheckCharacterNotInRangeEttPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6415CheckNotAtStartEiPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6415CheckNotAtStartEiPNS0_5LabelE
	.type	_ZN2v88internal23RegExpMacroAssemblerX6415CheckNotAtStartEiPNS0_5LabelE, @function
_ZN2v88internal23RegExpMacroAssemblerX6415CheckNotAtStartEiPNS0_5LabelE:
.LFB19626:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	32(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	624(%rdi), %eax
	leaq	-64(%rbp), %rdi
	imull	%eax, %esi
	subl	%eax, %esi
	movl	%esi, %edx
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	leaq	-76(%rbp), %rdi
	movl	$-72, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-76(%rbp), %rcx
	movl	-68(%rbp), %r8d
	xorl	%edx, %edx
	movl	$8, %r9d
	movl	$59, %esi
	movq	%r12, %rdi
	movq	%rcx, -64(%rbp)
	movl	%r8d, -56(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	testq	%r13, %r13
	je	.L169
	movl	$1, %ecx
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
.L164:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L170
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	leaq	660(%rbx), %rdx
	movl	$1, %ecx
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L164
.L170:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19626:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6415CheckNotAtStartEiPNS0_5LabelE, .-_ZN2v88internal23RegExpMacroAssemblerX6415CheckNotAtStartEiPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6412CheckAtStartEiPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6412CheckAtStartEiPNS0_5LabelE
	.type	_ZN2v88internal23RegExpMacroAssemblerX6412CheckAtStartEiPNS0_5LabelE, @function
_ZN2v88internal23RegExpMacroAssemblerX6412CheckAtStartEiPNS0_5LabelE:
.LFB19625:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	32(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	624(%rdi), %eax
	leaq	-64(%rbp), %rdi
	imull	%eax, %esi
	subl	%eax, %esi
	movl	%esi, %edx
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	leaq	-76(%rbp), %rdi
	movl	$-72, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-76(%rbp), %rcx
	movl	-68(%rbp), %r8d
	xorl	%edx, %edx
	movl	$8, %r9d
	movl	$59, %esi
	movq	%r12, %rdi
	movq	%rcx, -64(%rbp)
	movl	%r8d, -56(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	testq	%r13, %r13
	je	.L176
	movl	$1, %ecx
	movq	%r13, %rdx
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
.L171:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L177
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	leaq	660(%rbx), %rdx
	movl	$1, %ecx
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L171
.L177:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19625:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6412CheckAtStartEiPNS0_5LabelE, .-_ZN2v88internal23RegExpMacroAssemblerX6412CheckAtStartEiPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6430CheckNotCharacterAfterMinusAndEtttPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6430CheckNotCharacterAfterMinusAndEtttPNS0_5LabelE
	.type	_ZN2v88internal23RegExpMacroAssemblerX6430CheckNotCharacterAfterMinusAndEtttPNS0_5LabelE, @function
_ZN2v88internal23RegExpMacroAssemblerX6430CheckNotCharacterAfterMinusAndEtttPNS0_5LabelE:
.LFB19637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzwl	%dx, %edx
	negl	%edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	32(%rdi), %r13
	leaq	-80(%rbp), %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	movl	$2, %esi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r13, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$4, %r8d
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movzwl	%r12w, %ecx
	xorl	%edx, %edx
	movl	$4, %esi
	movabsq	$81604378624, %r12
	movl	$4, %r8d
	movq	%r13, %rdi
	orq	%r12, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movzwl	%bx, %ecx
	xorl	%edx, %edx
	movl	$4, %r8d
	orq	%r12, %rcx
	movl	$7, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	testq	%r14, %r14
	je	.L183
	movl	$1, %ecx
	movq	%r14, %rdx
	movl	$5, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
.L178:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L184
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	leaq	660(%r15), %rdx
	movl	$1, %ecx
	movl	$5, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L178
.L184:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19637:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6430CheckNotCharacterAfterMinusAndEtttPNS0_5LabelE, .-_ZN2v88internal23RegExpMacroAssemblerX6430CheckNotCharacterAfterMinusAndEtttPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6425CheckNotCharacterAfterAndEjjPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6425CheckNotCharacterAfterAndEjjPNS0_5LabelE
	.type	_ZN2v88internal23RegExpMacroAssemblerX6425CheckNotCharacterAfterAndEjjPNS0_5LabelE, @function
_ZN2v88internal23RegExpMacroAssemblerX6425CheckNotCharacterAfterAndEjjPNS0_5LabelE:
.LFB19636:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	32(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	testl	%esi, %esi
	jne	.L186
	movl	%edx, %edx
	movl	$4, %ecx
	movl	$2, %esi
	movq	%r14, %rdi
	movabsq	$81604378624, %rax
	orq	%rax, %rdx
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterENS0_9ImmediateEi@PLT
.L187:
	leaq	660(%r12), %rdx
	movl	$1, %ecx
	testq	%r13, %r13
	je	.L190
	movl	$1, %ecx
	movq	%r13, %rdx
.L190:
	addq	$8, %rsp
	movq	%r14, %rdi
	movl	$5, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	movabsq	$-1095216660481, %rax
	movl	%esi, %ebx
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movq	%r14, %rdi
	andl	%eax, %edx
	movl	$4, %ecx
	movabsq	$81604378624, %r15
	orq	%r15, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movl	$8, %r8d
	movl	$2, %ecx
	movl	$35, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	%ebx, %ecx
	movl	$4, %r8d
	xorl	%edx, %edx
	movabsq	$-1095216660481, %rax
	movl	$7, %esi
	movq	%r14, %rdi
	andl	%eax, %ecx
	orq	%r15, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L187
	.cfi_endproc
.LFE19636:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6425CheckNotCharacterAfterAndEjjPNS0_5LabelE, .-_ZN2v88internal23RegExpMacroAssemblerX6425CheckNotCharacterAfterAndEjjPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6422CheckCharacterAfterAndEjjPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6422CheckCharacterAfterAndEjjPNS0_5LabelE
	.type	_ZN2v88internal23RegExpMacroAssemblerX6422CheckCharacterAfterAndEjjPNS0_5LabelE, @function
_ZN2v88internal23RegExpMacroAssemblerX6422CheckCharacterAfterAndEjjPNS0_5LabelE:
.LFB19635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	32(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	testl	%esi, %esi
	jne	.L192
	movl	%edx, %edx
	movl	$4, %ecx
	movl	$2, %esi
	movq	%r14, %rdi
	movabsq	$81604378624, %rax
	orq	%rax, %rdx
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterENS0_9ImmediateEi@PLT
.L193:
	leaq	660(%r12), %rdx
	movl	$1, %ecx
	testq	%r13, %r13
	je	.L196
	movl	$1, %ecx
	movq	%r13, %rdx
.L196:
	addq	$8, %rsp
	movq	%r14, %rdi
	movl	$4, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	movabsq	$-1095216660481, %rax
	movl	%esi, %ebx
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movq	%r14, %rdi
	andl	%eax, %edx
	movl	$4, %ecx
	movabsq	$81604378624, %r15
	orq	%r15, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movl	$8, %r8d
	movl	$2, %ecx
	movl	$35, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	%ebx, %ecx
	movl	$4, %r8d
	xorl	%edx, %edx
	movabsq	$-1095216660481, %rax
	movl	$7, %esi
	movq	%r14, %rdi
	andl	%eax, %ecx
	orq	%r15, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L193
	.cfi_endproc
.LFE19635:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6422CheckCharacterAfterAndEjjPNS0_5LabelE, .-_ZN2v88internal23RegExpMacroAssemblerX6422CheckCharacterAfterAndEjjPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6415CheckBitInTableENS0_6HandleINS0_9ByteArrayEEEPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6415CheckBitInTableENS0_6HandleINS0_9ByteArrayEEEPNS0_5LabelE
	.type	_ZN2v88internal23RegExpMacroAssemblerX6415CheckBitInTableENS0_6HandleINS0_9ByteArrayEEEPNS0_5LabelE, @function
_ZN2v88internal23RegExpMacroAssemblerX6415CheckBitInTableENS0_6HandleINS0_9ByteArrayEEEPNS0_5LabelE:
.LFB19640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movabsq	$81604378624, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movq	%rsi, %rdx
	pushq	%r12
	.cfi_offset 12, -48
	leaq	32(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$56, %rsp
	movl	_ZN2v88internalL3raxE(%rip), %r15d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%r15d, %esi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEENS0_9RelocInfo4ModeE@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	$2, %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	%r14, %rcx
	movl	$3, %edx
	movq	%r12, %rdi
	orq	$127, %rcx
	movl	$8, %r8d
	movl	$4, %esi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	xorl	%ecx, %ecx
	leaq	-80(%rbp), %rdi
	movl	$3, %edx
	movl	$15, %r8d
	movl	%r15d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r14, %r8
	movl	$7, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler25immediate_arithmetic_op_8EhNS0_7OperandENS0_9ImmediateE@PLT
	testq	%r13, %r13
	je	.L202
	movl	$1, %ecx
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
.L197:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L203
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	leaq	660(%rbx), %rdx
	movl	$1, %ecx
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L197
.L203:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19640:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6415CheckBitInTableENS0_6HandleINS0_9ByteArrayEEEPNS0_5LabelE, .-_ZN2v88internal23RegExpMacroAssemblerX6415CheckBitInTableENS0_6HandleINS0_9ByteArrayEEEPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6415CheckGreedyLoopEPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6415CheckGreedyLoopEPNS0_5LabelE
	.type	_ZN2v88internal23RegExpMacroAssemblerX6415CheckGreedyLoopEPNS0_5LabelE, @function
_ZN2v88internal23RegExpMacroAssemblerX6415CheckGreedyLoopEPNS0_5LabelE:
.LFB19628:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$1, %esi
	leaq	-84(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	32(%rdi), %r12
	leaq	-76(%rbp), %rdi
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -84(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-68(%rbp), %r8d
	movq	-76(%rbp), %rcx
	movq	%r12, %rdi
	movl	$4, %r9d
	movl	$7, %edx
	movl	$59, %esi
	movl	%r8d, -56(%rbp)
	movl	%r8d, -44(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rcx, -52(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	movl	$1, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	xorl	%esi, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movabsq	$81604378628, %rcx
	movl	$1, %edx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	testq	%r14, %r14
	je	.L209
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
.L206:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L210
	addq	$64, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*56(%rax)
	jmp	.L206
.L210:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19628:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6415CheckGreedyLoopEPNS0_5LabelE, .-_ZN2v88internal23RegExpMacroAssemblerX6415CheckGreedyLoopEPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6425SetCurrentPositionFromEndEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6425SetCurrentPositionFromEndEi
	.type	_ZN2v88internal23RegExpMacroAssemblerX6425SetCurrentPositionFromEndEi, @function
_ZN2v88internal23RegExpMacroAssemblerX6425SetCurrentPositionFromEndEi:
.LFB19661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %r8d
	movl	$7, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-88(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	32(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	movl	$7, %esi
	negl	%ebx
	movl	%ebx, %ecx
	subq	$56, %rsp
	imull	624(%rdi), %ecx
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -88(%rbp)
	movabsq	$81604378624, %rax
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$13, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%ebx, %edx
	movq	%r12, %rdi
	movl	$8, %ecx
	imull	624(%r13), %edx
	movl	_ZN2v88internalL3rdiE(%rip), %r15d
	movabsq	$81604378624, %rax
	movl	%r15d, %esi
	orq	%rax, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	cmpl	$1, 624(%r13)
	leaq	-68(%rbp), %rdi
	je	.L216
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	movl	$-2, %r8d
	xorl	%ecx, %ecx
	movl	%r15d, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movq	%r12, %rdi
	movl	$4, %r8d
	movl	$2, %esi
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterENS0_7OperandEi@PLT
.L213:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L217
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore_state
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	xorl	%ecx, %ecx
	movl	%r15d, %edx
	movl	$-1, %r8d
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movq	%r12, %rdi
	movl	$4, %r8d
	movl	$2, %esi
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L213
.L217:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19661:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6425SetCurrentPositionFromEndEi, .-_ZN2v88internal23RegExpMacroAssemblerX6425SetCurrentPositionFromEndEi
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6413CheckPositionEiPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6413CheckPositionEiPNS0_5LabelE
	.type	_ZN2v88internal23RegExpMacroAssemblerX6413CheckPositionEiPNS0_5LabelE, @function
_ZN2v88internal23RegExpMacroAssemblerX6413CheckPositionEiPNS0_5LabelE:
.LFB19672:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	32(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movl	624(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	js	.L219
	movl	%esi, %ecx
	movl	$4, %r8d
	movq	%r13, %rdi
	movabsq	$81604378624, %rsi
	negl	%ecx
	imull	%edx, %ecx
	movl	$7, %edx
	orq	%rsi, %rcx
	movl	$7, %esi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	testq	%r12, %r12
	je	.L225
	movl	$1, %ecx
	movq	%r12, %rdx
	movl	$13, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
.L218:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L226
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	imull	%esi, %edx
	leaq	-52(%rbp), %r14
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-44(%rbp), %ecx
	movq	-52(%rbp), %rdx
	movq	%r13, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movl	$-72, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-52(%rbp), %rcx
	movl	-44(%rbp), %r8d
	xorl	%edx, %edx
	movl	$8, %r9d
	movl	$59, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	testq	%r12, %r12
	je	.L227
	movl	$1, %ecx
	movq	%r12, %rdx
	movl	$14, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L225:
	leaq	660(%rbx), %rdx
	movl	$1, %ecx
	movl	$13, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L227:
	leaq	660(%rbx), %rdx
	movl	$1, %ecx
	movl	$14, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L218
.L226:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19672:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6413CheckPositionEiPNS0_5LabelE, .-_ZN2v88internal23RegExpMacroAssemblerX6413CheckPositionEiPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6424LoadCurrentCharacterImplEiPNS0_5LabelEbii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6424LoadCurrentCharacterImplEiPNS0_5LabelEbii
	.type	_ZN2v88internal23RegExpMacroAssemblerX6424LoadCurrentCharacterImplEiPNS0_5LabelEbii, @function
_ZN2v88internal23RegExpMacroAssemblerX6424LoadCurrentCharacterImplEiPNS0_5LabelEbii:
.LFB19652:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testb	%cl, %cl
	je	.L229
	movq	(%rdi), %rax
	movq	192(%rax), %rax
	testl	%esi, %esi
	js	.L230
	leal	-1(%rsi,%r9), %esi
	call	*%rax
.L229:
	cmpl	$1, 624(%r12)
	leaq	32(%r12), %r14
	je	.L239
.L231:
	movl	_ZN2v88internalL3rdiE(%rip), %edx
	leal	(%rbx,%rbx), %r8d
	leaq	-52(%rbp), %rdi
	xorl	%ecx, %ecx
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	cmpl	$2, %r13d
	je	.L238
.L235:
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-44(%rbp), %ecx
	movq	-52(%rbp), %rdx
	movq	%r14, %rdi
	movl	$4, %r8d
	movl	$2, %esi
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterENS0_7OperandEi@PLT
.L228:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L240
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L238:
	.cfi_restore_state
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-44(%rbp), %ecx
	movq	-52(%rbp), %rdx
	movq	%r14, %rdi
	movl	$4, %r8d
	movl	$2, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L230:
	call	*%rax
	leaq	32(%r12), %r14
	cmpl	$1, 624(%r12)
	jne	.L231
.L239:
	movl	_ZN2v88internalL3rdiE(%rip), %edx
	leaq	-52(%rbp), %rdi
	movl	%ebx, %r8d
	xorl	%ecx, %ecx
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	cmpl	$4, %r13d
	je	.L238
	cmpl	$2, %r13d
	je	.L235
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-44(%rbp), %ecx
	movq	-52(%rbp), %rdx
	movq	%r14, %rdi
	movl	$4, %r8d
	movl	$2, %esi
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L228
.L240:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19652:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6424LoadCurrentCharacterImplEiPNS0_5LabelEbii, .-_ZN2v88internal23RegExpMacroAssemblerX6424LoadCurrentCharacterImplEiPNS0_5LabelEbii
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6421CheckNotBackReferenceEibPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6421CheckNotBackReferenceEibPNS0_5LabelE
	.type	_ZN2v88internal23RegExpMacroAssemblerX6421CheckNotBackReferenceEibPNS0_5LabelE, @function
_ZN2v88internal23RegExpMacroAssemblerX6421CheckNotBackReferenceEibPNS0_5LabelE:
.LFB19633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	32(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movl	%esi, -204(%rbp)
	movl	%edx, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	1(%rsi), %eax
	movq	$0, -192(%rbp)
	movl	%eax, -196(%rbp)
	cmpl	628(%rdi), %esi
	jl	.L242
	movl	%eax, 628(%rdi)
.L242:
	movl	-204(%rbp), %r15d
	movl	$-10, %eax
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	leaq	-68(%rbp), %r13
	movq	%r13, %rdi
	subl	%r15d, %eax
	sall	$3, %eax
	movl	%eax, %edx
	movl	%eax, -228(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$2, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	-196(%rbp), %eax
	cmpl	%eax, 628(%rbx)
	jg	.L243
	leal	2(%r15), %eax
	movl	%eax, 628(%rbx)
.L243:
	movl	$-10, %eax
	subl	-196(%rbp), %eax
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movq	%r13, %rdi
	sall	$3, %eax
	movl	%eax, %edx
	movl	%eax, -232(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	xorl	%esi, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	xorl	%edx, %edx
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	$2, %ecx
	movl	$43, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	leaq	-192(%rbp), %rax
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$4, %esi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	cmpb	$0, -200(%rbp)
	je	.L244
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	leaq	-80(%rbp), %r13
	movl	$-72, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rbxE(%rip), %r15d
	movl	$4, %r8d
	movq	%rdx, -68(%rbp)
	movl	%r15d, %esi
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	xorl	%ecx, %ecx
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	$3, %edx
	movl	$3, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	$4, %r8d
	movl	$3, %ecx
	movq	%r12, %rdi
	movl	$7, %edx
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	testq	%r14, %r14
	je	.L260
	movl	$1, %ecx
	movq	%r14, %rdx
	movl	$14, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
.L246:
	movl	_ZN2v88internalL3rdiE(%rip), %eax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	movl	%eax, %edx
	movl	%eax, -208(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movl	%r15d, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	$8, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movl	$3, %edx
	movl	$43, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L244:
	movl	_ZN2v88internalL3rdiE(%rip), %eax
	movl	_ZN2v88internalL3rbxE(%rip), %r15d
	movl	$4, %ecx
	movq	%r12, %rdi
	movl	%eax, %edx
	movl	%r15d, %esi
	movl	%eax, -208(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	xorl	%ecx, %ecx
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	$3, %edx
	movl	$3, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	testq	%r14, %r14
	je	.L261
	movl	$1, %ecx
	movq	%r14, %rdx
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
.L248:
	movl	-208(%rbp), %edx
	leaq	-80(%rbp), %r13
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movl	%r15d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
.L256:
	movl	$8, %r8d
	movl	$6, %ecx
	movl	$2, %edx
	movq	%r12, %rdi
	movl	$3, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	_ZN2v88internalL3raxE(%rip), %edx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	_ZN2v88internalL3rdxE(%rip), %r9d
	movq	%r13, %rdi
	movl	%r9d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL2r9E(%rip), %esi
	movl	$8, %r8d
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	leaq	-184(%rbp), %rax
	movq	%r12, %rdi
	movq	$0, -184(%rbp)
	movq	%rax, %rsi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	_ZN2v88internalL3rdxE(%rip), %r9d
	xorl	%edx, %edx
	movq	%r13, %rdi
	cmpl	$1, 624(%rbx)
	movl	%r9d, %esi
	je	.L262
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$4, %r8d
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterENS0_7OperandEi@PLT
	xorl	%edx, %edx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rcx
	movl	-72(%rbp), %r8d
	movq	%r12, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %edx
	movl	$59, %esi
	movq	%rcx, -68(%rbp)
	movl	%r8d, -60(%rbp)
	call	_ZN2v88internal9Assembler16arithmetic_op_16EhNS0_8RegisterENS0_7OperandE@PLT
	testq	%r14, %r14
	je	.L263
.L251:
	movl	$1, %ecx
	movq	%r14, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
.L252:
	xorl	%esi, %esi
	movl	$8, %r8d
	movl	$3, %edx
	movq	%r12, %rdi
	movabsq	$-1095216660481, %r14
	movabsq	$81604378624, %rax
	movl	%r14d, %ecx
	andl	624(%rbx), %ecx
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	%r14d, %ecx
	andl	624(%rbx), %ecx
	xorl	%esi, %esi
	movl	$8, %r8d
	movl	$2, %edx
	movq	%r12, %rdi
	movabsq	$81604378624, %rax
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	$8, %r8d
	movl	$9, %ecx
	movq	%r12, %rdi
	movl	$2, %edx
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	-224(%rbp), %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$2, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$8, %ecx
	movl	%r15d, %edx
	movq	%r12, %rdi
	movl	-208(%rbp), %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	$8, %r8d
	movl	$6, %ecx
	movq	%r12, %rdi
	movl	$7, %edx
	movl	$43, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	cmpb	$0, -200(%rbp)
	jne	.L264
.L253:
	movq	-216(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L265
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	.cfi_restore_state
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$4, %r8d
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterENS0_7OperandEi@PLT
	xorl	%edx, %edx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rcx
	movl	-72(%rbp), %r8d
	movq	%r12, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %edx
	movl	$58, %esi
	movq	%rcx, -68(%rbp)
	movl	%r8d, -60(%rbp)
	call	_ZN2v88internal9Assembler15arithmetic_op_8EhNS0_8RegisterENS0_7OperandE@PLT
	testq	%r14, %r14
	jne	.L251
.L263:
	leaq	660(%rbx), %rdx
	movl	$1, %ecx
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L264:
	movl	-204(%rbp), %eax
	cmpl	628(%rbx), %eax
	jl	.L254
	movl	-196(%rbp), %eax
	movl	%eax, 628(%rbx)
.L254:
	movl	-228(%rbp), %edx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rcx
	movl	-72(%rbp), %r8d
	movq	%r12, %rdi
	movl	$8, %r9d
	movl	$7, %edx
	movl	$3, %esi
	movq	%rcx, -68(%rbp)
	movl	%r8d, -60(%rbp)
	movq	%rcx, -92(%rbp)
	movl	%r8d, -84(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	movl	-196(%rbp), %eax
	cmpl	%eax, 628(%rbx)
	jg	.L255
	movl	-204(%rbp), %esi
	addl	$2, %esi
	movl	%esi, 628(%rbx)
.L255:
	movl	-232(%rbp), %edx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	leaq	-92(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-92(%rbp), %rcx
	movl	-84(%rbp), %r8d
	movq	%r12, %rdi
	movl	$8, %r9d
	movl	$7, %edx
	movl	$43, %esi
	movq	%rcx, -80(%rbp)
	movl	%r8d, -72(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L260:
	leaq	660(%rbx), %rdx
	movl	$1, %ecx
	movl	$14, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L261:
	leaq	660(%rbx), %rdx
	movl	$1, %ecx
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L248
.L265:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19633:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6421CheckNotBackReferenceEibPNS0_5LabelE, .-_ZN2v88internal23RegExpMacroAssemblerX6421CheckNotBackReferenceEibPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6431CheckNotBackReferenceIgnoreCaseEibbPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6431CheckNotBackReferenceIgnoreCaseEibbPNS0_5LabelE
	.type	_ZN2v88internal23RegExpMacroAssemblerX6431CheckNotBackReferenceIgnoreCaseEibbPNS0_5LabelE, @function
_ZN2v88internal23RegExpMacroAssemblerX6431CheckNotBackReferenceIgnoreCaseEibbPNS0_5LabelE:
.LFB19629:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	32(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$232, %rsp
	movl	%esi, -232(%rbp)
	movl	%edx, -212(%rbp)
	movl	%ecx, -248(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	1(%rsi), %eax
	movq	$0, -200(%rbp)
	movl	%eax, -228(%rbp)
	cmpl	628(%rdi), %esi
	jl	.L267
	movl	%eax, 628(%rdi)
.L267:
	movl	-232(%rbp), %r15d
	movl	$-10, %eax
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	leaq	-68(%rbp), %r13
	movq	%r13, %rdi
	subl	%r15d, %eax
	sall	$3, %eax
	movl	%eax, %edx
	movl	%eax, -252(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$2, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	-228(%rbp), %eax
	cmpl	628(%rbx), %eax
	jl	.L268
	leal	2(%r15), %eax
	movl	%eax, 628(%rbx)
.L268:
	movl	$-10, %eax
	subl	-228(%rbp), %eax
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movq	%r13, %rdi
	sall	$3, %eax
	movl	%eax, %edx
	movl	%eax, -256(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$3, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$8, %r8d
	movl	$2, %ecx
	movq	%r12, %rdi
	movl	$3, %edx
	movl	$43, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	leaq	-200(%rbp), %rax
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$4, %esi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	cmpb	$0, -212(%rbp)
	je	.L269
	leaq	-80(%rbp), %rax
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movl	$-72, %edx
	movq	%rax, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movl	$4, %r8d
	movq	%rdx, -68(%rbp)
	movl	%r13d, %esi
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	xorl	%edx, %edx
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$3, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	xorl	%ecx, %ecx
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	$7, %edx
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	testq	%r14, %r14
	je	.L292
	movl	$1, %ecx
	movq	%r14, %rdx
	movl	$14, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %eax
	cmpl	$1, 624(%rbx)
	movl	%eax, -216(%rbp)
	jne	.L287
.L295:
	movq	$0, -192(%rbp)
.L285:
	movl	_ZN2v88internalL3rsiE(%rip), %r9d
	movl	_ZN2v88internalL3rdxE(%rip), %edx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	-224(%rbp), %rdi
	movl	%r9d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL2r9E(%rip), %r15d
	movl	$8, %r8d
	movq	%rdx, -68(%rbp)
	movl	%r15d, %esi
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	-216(%rbp), %edx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	_ZN2v88internalL3rsiE(%rip), %r9d
	movq	-224(%rbp), %rdi
	movl	%r9d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3r11E(%rip), %esi
	movl	$8, %r8d
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	cmpb	$0, -212(%rbp)
	jne	.L293
.L274:
	movl	$8, %r8d
	movl	$9, %ecx
	movl	$3, %edx
	movq	%r12, %rdi
	movl	$3, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	leaq	-184(%rbp), %r9
	movq	%r12, %rdi
	movq	$0, -184(%rbp)
	movq	%r9, %rsi
	movq	%r9, -264(%rbp)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	%r15d, %esi
	movq	-224(%rbp), %r15
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rdxE(%rip), %esi
	movl	$4, %r8d
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL3r11E(%rip), %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	movabsq	$81604378624, %r15
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movl	%r13d, %esi
	movl	$4, %r8d
	movq	%r12, %rdi
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r13d, %edx
	movl	$58, %esi
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rdxE(%rip), %ecx
	call	_ZN2v88internal9Assembler15arithmetic_op_8EhNS0_8RegisterES2_@PLT
	leaq	-192(%rbp), %r10
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r10, %rdx
	movl	$4, %esi
	movq	%r10, -248(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r15, %rcx
	xorl	%edx, %edx
	movl	$8, %r8d
	orq	$32, %rcx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r15, %rcx
	movl	$2, %edx
	movq	%r12, %rdi
	movl	$8, %r8d
	orq	$32, %rcx
	movl	$1, %esi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	%r13d, %edx
	movl	$58, %esi
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rdxE(%rip), %ecx
	call	_ZN2v88internal9Assembler15arithmetic_op_8EhNS0_8RegisterES2_@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r15, %rcx
	movl	%r13d, %edx
	movl	$5, %esi
	orq	$97, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler25immediate_arithmetic_op_8EhNS0_8RegisterENS0_9ImmediateE@PLT
	movq	%r15, %rcx
	movl	%r13d, %edx
	movl	$7, %esi
	orq	$25, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler25immediate_arithmetic_op_8EhNS0_8RegisterENS0_9ImmediateE@PLT
	movq	-248(%rbp), %r10
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$6, %esi
	movq	%r10, %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r15, %rcx
	movl	%r13d, %edx
	movl	$5, %esi
	orq	$127, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler25immediate_arithmetic_op_8EhNS0_8RegisterENS0_9ImmediateE@PLT
	movq	%r15, %rcx
	movl	%r13d, %edx
	movl	$7, %esi
	orq	$30, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler25immediate_arithmetic_op_8EhNS0_8RegisterENS0_9ImmediateE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$7, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r15, %rcx
	movl	%r13d, %edx
	movl	$7, %esi
	orq	$23, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler25immediate_arithmetic_op_8EhNS0_8RegisterENS0_9ImmediateE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$4, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-248(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	%r15, %rcx
	movl	$11, %edx
	movq	%r12, %rdi
	orq	$1, %rcx
	movl	$8, %r8d
	xorl	%esi, %esi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r15, %rcx
	xorl	%esi, %esi
	movl	$8, %r8d
	orq	$1, %rcx
	movl	$9, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	$8, %r8d
	movl	$3, %ecx
	movq	%r12, %rdi
	movl	$9, %edx
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	-264(%rbp), %r9
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%r9, %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	_ZN2v88internalL3r11E(%rip), %edx
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	-216(%rbp), %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	$8, %r8d
	movl	$6, %ecx
	movq	%r12, %rdi
	movl	$7, %edx
	movl	$43, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	cmpb	$0, -212(%rbp)
	je	.L278
	movl	-232(%rbp), %eax
	cmpl	628(%rbx), %eax
	jl	.L276
	movl	-228(%rbp), %eax
	movl	%eax, 628(%rbx)
.L276:
	movl	-252(%rbp), %edx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movq	-224(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rcx
	movl	-72(%rbp), %r8d
	movq	%r12, %rdi
	movl	$8, %r9d
	movl	$7, %edx
	movl	$3, %esi
	movq	%rcx, -68(%rbp)
	movl	%r8d, -60(%rbp)
	movq	%rcx, -92(%rbp)
	movl	%r8d, -84(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	movl	-228(%rbp), %eax
	cmpl	628(%rbx), %eax
	jl	.L277
	movl	-232(%rbp), %esi
	addl	$2, %esi
	movl	%esi, 628(%rbx)
.L277:
	movl	-256(%rbp), %edx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	leaq	-92(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-92(%rbp), %rcx
	movl	-84(%rbp), %r8d
	movq	%r12, %rdi
	movl	$8, %r9d
	movl	$7, %edx
	movl	$43, %esi
	movq	%rcx, -80(%rbp)
	movl	%r8d, -72(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L269:
	movl	_ZN2v88internalL3rdiE(%rip), %eax
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movl	$4, %ecx
	movq	%r12, %rdi
	movl	%eax, %edx
	movl	%r13d, %esi
	movl	%eax, -216(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	xorl	%edx, %edx
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$3, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	testq	%r14, %r14
	je	.L294
	movl	$1, %ecx
	movq	%r14, %rdx
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	leaq	-80(%rbp), %rax
	cmpl	$1, 624(%rbx)
	movq	%rax, -224(%rbp)
	je	.L295
.L287:
	movl	_ZN2v88internalL3rsiE(%rip), %r15d
	movq	%r12, %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	-216(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	movl	-216(%rbp), %edx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	-224(%rbp), %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movl	%r13d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL3rdxE(%rip), %edx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	-224(%rbp), %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%r12, %rdi
	movl	-216(%rbp), %esi
	movl	$8, %r8d
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	$8, %ecx
	movl	%r13d, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	cmpb	$0, -212(%rbp)
	jne	.L296
.L279:
	movl	_ZN2v88internalL3rbxE(%rip), %edx
	movl	_ZN2v88internalL9arg_reg_3E(%rip), %esi
	movl	$8, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	cmpb	$0, -248(%rbp)
	je	.L280
	movl	_ZN2v88internalL9arg_reg_4E(%rip), %esi
	movl	$8, %ecx
	movq	%r12, %rdi
	movabsq	$81604378624, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
.L281:
	movzbl	568(%rbx), %ecx
	movb	$1, 568(%rbx)
	movq	544(%rbx), %rdi
	movb	%cl, -224(%rbp)
	call	_ZN2v88internal17ExternalReference32re_case_insensitive_compare_uc16EPNS0_7IsolateE@PLT
	movl	$4, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	movzbl	-224(%rbp), %ecx
	movl	$8, %esi
	movq	%r12, %rdi
	movq	552(%rbx), %rdx
	movb	%cl, 568(%rbx)
	movl	$3, %ecx
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEENS0_9RelocInfo4ModeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	-216(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	$8, %ecx
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	testq	%r14, %r14
	je	.L297
	movl	$1, %ecx
	movq	%r14, %rdx
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
.L283:
	movl	$8, %r8d
	movl	$3, %ecx
	movl	$7, %edx
	cmpb	$0, -212(%rbp)
	je	.L284
	movl	$43, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
.L278:
	movq	-240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L298
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	.cfi_restore_state
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L280:
	movq	544(%rbx), %rdi
	call	_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL9arg_reg_4E(%rip), %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal14TurboAssembler11LoadAddressENS0_8RegisterENS0_17ExternalReferenceE@PLT
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L292:
	leaq	660(%rbx), %r15
	movl	$1, %ecx
	movl	$14, %esi
	movq	%r12, %rdi
	movq	%r15, %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %eax
	movl	%eax, -216(%rbp)
.L271:
	cmpl	$1, 624(%rbx)
	jne	.L287
	movq	$0, -192(%rbp)
	movq	%r15, %r14
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L294:
	leaq	660(%rbx), %r15
	movl	$1, %ecx
	movl	$15, %esi
	movq	%r12, %rdi
	movq	%r15, %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	leaq	-80(%rbp), %rax
	movq	%rax, -224(%rbp)
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L293:
	movl	$8, %r8d
	movl	$3, %ecx
	movl	$11, %edx
	movq	%r12, %rdi
	movl	$43, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L296:
	movl	$8, %r8d
	movl	$3, %ecx
	movl	$6, %edx
	movq	%r12, %rdi
	movl	$43, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L297:
	leaq	660(%rbx), %rdx
	movl	$1, %ecx
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L283
.L298:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19629:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6431CheckNotBackReferenceIgnoreCaseEibbPNS0_5LabelE, .-_ZN2v88internal23RegExpMacroAssemblerX6431CheckNotBackReferenceIgnoreCaseEibbPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6426CheckSpecialCharacterClassEtPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6426CheckSpecialCharacterClassEtPNS0_5LabelE
	.type	_ZN2v88internal23RegExpMacroAssemblerX6426CheckSpecialCharacterClassEtPNS0_5LabelE, @function
_ZN2v88internal23RegExpMacroAssemblerX6426CheckSpecialCharacterClassEtPNS0_5LabelE:
.LFB19641:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpw	$119, %si
	ja	.L332
	movq	%rdi, %rbx
	movq	%rdx, %r12
	cmpw	$86, %si
	jbe	.L338
	subl	$87, %esi
	cmpw	$32, %si
	ja	.L332
	leaq	.L305(%rip), %rdx
	movzwl	%si, %esi
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal23RegExpMacroAssemblerX6426CheckSpecialCharacterClassEtPNS0_5LabelE,"a",@progbits
	.align 4
	.align 4
.L305:
	.long	.L309-.L305
	.long	.L332-.L305
	.long	.L332-.L305
	.long	.L332-.L305
	.long	.L332-.L305
	.long	.L332-.L305
	.long	.L332-.L305
	.long	.L332-.L305
	.long	.L332-.L305
	.long	.L332-.L305
	.long	.L332-.L305
	.long	.L332-.L305
	.long	.L332-.L305
	.long	.L308-.L305
	.long	.L332-.L305
	.long	.L332-.L305
	.long	.L332-.L305
	.long	.L332-.L305
	.long	.L332-.L305
	.long	.L332-.L305
	.long	.L332-.L305
	.long	.L332-.L305
	.long	.L332-.L305
	.long	.L307-.L305
	.long	.L332-.L305
	.long	.L332-.L305
	.long	.L332-.L305
	.long	.L332-.L305
	.long	.L306-.L305
	.long	.L332-.L305
	.long	.L332-.L305
	.long	.L332-.L305
	.long	.L304-.L305
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6426CheckSpecialCharacterClassEtPNS0_5LabelE
.L332:
	xorl	%eax, %eax
.L299:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L339
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	.cfi_restore_state
	cmpw	$46, %si
	je	.L302
	cmpw	$68, %si
	jne	.L340
	leaq	32(%rdi), %r13
	movl	$-48, %edx
	leaq	-80(%rbp), %rdi
	movl	$2, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%r13, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$4, %r8d
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	xorl	%edx, %edx
	movl	$4, %r8d
	movq	%r13, %rdi
	movabsq	$81604378633, %rcx
	movl	$7, %esi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	testq	%r12, %r12
	je	.L341
.L337:
	movl	$1, %ecx
	movq	%r12, %rdx
	movl	$6, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L336
.L340:
	cmpw	$42, %si
	sete	%al
	jmp	.L299
.L304:
	cmpl	$1, 624(%rdi)
	leaq	32(%rdi), %r13
	je	.L322
	movl	$4, %r8d
	movl	$2, %edx
	movl	$7, %esi
	movq	%r13, %rdi
	movabsq	$81604378746, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	testq	%r12, %r12
	je	.L342
.L323:
	movl	$1, %ecx
	movq	%r12, %rdx
	movl	$7, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	544(%rbx), %rdi
	call	_ZN2v88internal17ExternalReference21re_word_character_mapEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %r14d
	movq	%r13, %rdi
	movq	%rax, %rdx
	movl	%r14d, %esi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_17ExternalReferenceE@PLT
	leaq	-68(%rbp), %rdi
	xorl	%ecx, %ecx
	movl	$2, %edx
	movl	%r14d, %esi
	xorl	%r8d, %r8d
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%r13, %rdi
	movl	$2, %ecx
	call	_ZN2v88internal9Assembler5testbENS0_7OperandENS0_8RegisterE@PLT
.L325:
	movl	$1, %ecx
	movq	%r12, %rdx
	movl	$4, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$1, %eax
	jmp	.L299
.L309:
	cmpl	$1, 624(%rdi)
	movq	$0, -88(%rbp)
	leaq	32(%rdi), %r13
	je	.L326
	movl	$2, %edx
	movl	$7, %esi
	movq	%r13, %rdi
	movabsq	$81604378746, %rcx
	movl	$4, %r8d
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	leaq	-88(%rbp), %rdx
	movl	$1, %ecx
	movq	%r13, %rdi
	movl	$7, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
.L326:
	movq	544(%rbx), %rdi
	call	_ZN2v88internal17ExternalReference21re_word_character_mapEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %r14d
	movq	%r13, %rdi
	movq	%rax, %rdx
	movl	%r14d, %esi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_17ExternalReferenceE@PLT
	xorl	%ecx, %ecx
	leaq	-68(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$2, %edx
	movl	%r14d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%r13, %rdi
	movl	$2, %ecx
	call	_ZN2v88internal9Assembler5testbENS0_7OperandENS0_8RegisterE@PLT
	testq	%r12, %r12
	je	.L343
	movl	$1, %ecx
	movq	%r12, %rdx
	movl	$5, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
.L328:
	cmpl	$1, 624(%rbx)
	je	.L336
	leaq	-88(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	.p2align 4,,10
	.p2align 3
.L336:
	movl	$1, %eax
	jmp	.L299
.L308:
	leaq	32(%rdi), %r13
	movl	$-48, %edx
	leaq	-80(%rbp), %rdi
	movl	$2, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%r13, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$4, %r8d
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	xorl	%edx, %edx
	movl	$4, %r8d
	movq	%r13, %rdi
	movabsq	$81604378633, %rcx
	movl	$7, %esi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	testq	%r12, %r12
	je	.L335
.L319:
	movl	$1, %ecx
	movq	%r12, %rdx
	movl	$7, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$1, %eax
	jmp	.L299
.L307:
	leaq	32(%rdi), %r13
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$4, %ecx
	movabsq	$81604378624, %r14
	movl	$2, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$4, %r8d
	orq	$1, %rcx
	movl	$6, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$4, %r8d
	orq	$11, %rcx
	movl	$5, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$4, %r8d
	orq	$1, %rcx
	movl	$7, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	cmpl	$1, 624(%rbx)
	je	.L344
	leaq	-88(%rbp), %r15
	movl	$1, %ecx
	movl	$6, %esi
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	$0, -88(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	xorl	%edx, %edx
	movl	$4, %r8d
	movq	%r13, %rdi
	movabsq	$-1095216660481, %rcx
	movl	$5, %esi
	andl	$8221, %ecx
	orq	%r14, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	xorl	%edx, %edx
	movl	$4, %r8d
	movq	%r13, %rdi
	movabsq	$-1095216660481, %rcx
	movl	$7, %esi
	andl	$1, %ecx
	orq	%r14, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	testq	%r12, %r12
	je	.L345
	movl	$1, %ecx
	movq	%r12, %rdx
	movl	$7, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
.L321:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$1, %eax
	jmp	.L299
.L306:
	xorl	%eax, %eax
	cmpl	$1, 624(%rdi)
	jne	.L299
	leaq	32(%rdi), %r13
	movl	$4, %r8d
	movl	$2, %edx
	movabsq	$81604378624, %r14
	movq	%r14, %rcx
	movl	$7, %esi
	leaq	-88(%rbp), %r15
	movq	%r13, %rdi
	orq	$32, %rcx
	movq	$0, -88(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movl	$4, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	leaq	-80(%rbp), %rdi
	movl	$-9, %edx
	movl	$2, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%r13, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$4, %r8d
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movq	%r14, %rcx
	movl	$4, %r8d
	xorl	%edx, %edx
	orq	$4, %rcx
	movl	$7, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movl	$6, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$4, %r8d
	orb	$-105, %cl
	movl	$7, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	testq	%r12, %r12
	je	.L346
	movl	$1, %ecx
	movq	%r12, %rdx
	movl	$5, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L302:
	leaq	32(%rdi), %r13
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$4, %ecx
	movabsq	$81604378624, %r14
	movl	$2, %edx
	movq	%r13, %rdi
	movabsq	$-1095216660481, %r15
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$4, %r8d
	orq	$1, %rcx
	movl	$6, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$4, %r8d
	orq	$11, %rcx
	movl	$5, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$4, %r8d
	orq	$1, %rcx
	movl	$7, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	testq	%r12, %r12
	je	.L347
.L314:
	movl	$1, %ecx
	movq	%r12, %rdx
	movl	$6, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	cmpl	$2, 624(%rbx)
	jne	.L336
	movq	%r15, %rcx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movl	$4, %r8d
	andl	$8221, %ecx
	movl	$5, %esi
	orq	%r14, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r15, %rcx
	movl	$4, %r8d
	xorl	%edx, %edx
	andl	$1, %ecx
	movl	$7, %esi
	movq	%r13, %rdi
	orq	%r14, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L322:
	movq	544(%rdi), %rdi
	call	_ZN2v88internal17ExternalReference21re_word_character_mapEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %r14d
	movq	%r13, %rdi
	movq	%rax, %rdx
	movl	%r14d, %esi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_17ExternalReferenceE@PLT
	xorl	%ecx, %ecx
	leaq	-68(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$2, %edx
	movl	%r14d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%r13, %rdi
	movl	$2, %ecx
	call	_ZN2v88internal9Assembler5testbENS0_7OperandENS0_8RegisterE@PLT
	testq	%r12, %r12
	jne	.L325
	leaq	660(%rbx), %r12
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L341:
	leaq	660(%rbx), %rdx
	movl	$1, %ecx
	movl	$6, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$1, %eax
	jmp	.L299
.L342:
	leaq	660(%rbx), %r12
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L344:
	testq	%r12, %r12
	jne	.L319
.L335:
	leaq	660(%rbx), %rdx
	movl	$1, %ecx
	movl	$7, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$1, %eax
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L343:
	leaq	660(%rbx), %rdx
	movl	$1, %ecx
	movl	$5, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L347:
	leaq	660(%rbx), %r12
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L345:
	leaq	660(%rbx), %rdx
	movl	$1, %ecx
	movl	$7, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L321
.L346:
	leaq	660(%rbx), %rdx
	movl	$1, %ecx
	movl	$5, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L321
.L339:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19641:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6426CheckSpecialCharacterClassEtPNS0_5LabelE, .-_ZN2v88internal23RegExpMacroAssemblerX6426CheckSpecialCharacterClassEtPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6424CallCheckStackGuardStateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6424CallCheckStackGuardStateEv
	.type	_ZN2v88internal23RegExpMacroAssemblerX6424CallCheckStackGuardStateEv, @function
_ZN2v88internal23RegExpMacroAssemblerX6424CallCheckStackGuardStateEv:
.LFB19667:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	32(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %edx
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rdxE(%rip), %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	$8, %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	_ZN2v88internalL3rspE(%rip), %esi
	leaq	-48(%rbp), %rdi
	movl	$-8, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-40(%rbp), %ecx
	movq	-48(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movq	544(%rbx), %rdi
	call	_ZN2v88internal17ExternalReference26re_check_stack_guard_stateEPNS0_7IsolateE@PLT
	movl	$3, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L351
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L351:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19667:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6424CallCheckStackGuardStateEv, .-_ZN2v88internal23RegExpMacroAssemblerX6424CallCheckStackGuardStateEv
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6420CheckStackGuardStateEPmmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6420CheckStackGuardStateEPmmm
	.type	_ZN2v88internal23RegExpMacroAssemblerX6420CheckStackGuardStateEPmmm, @function
_ZN2v88internal23RegExpMacroAssemblerX6420CheckStackGuardStateEPmmm:
.LFB19670:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-32(%rdx), %rax
	movl	24(%rdx), %r10d
	movq	%rdi, %rcx
	movq	%rsi, %r8
	movq	32(%rdx), %rdi
	movl	-16(%rdx), %esi
	leaq	-8(%rdx), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rax
	leaq	-24(%rdx), %rax
	movl	%r10d, %edx
	pushq	%rax
	call	_ZN2v88internal26NativeRegExpMacroAssembler20CheckStackGuardStateEPNS0_7IsolateEiNS0_6RegExp10CallOriginEPmNS0_4CodeES6_PPKhSA_@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19670:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6420CheckStackGuardStateEPmmm, .-_ZN2v88internal23RegExpMacroAssemblerX6420CheckStackGuardStateEPmmm
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6417register_locationEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6417register_locationEi
	.type	_ZN2v88internal23RegExpMacroAssemblerX6417register_locationEi, @function
_ZN2v88internal23RegExpMacroAssemblerX6417register_locationEi:
.LFB19671:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpl	%esi, 628(%rdi)
	jg	.L355
	leal	1(%rsi), %eax
	movl	%eax, 628(%rdi)
.L355:
	movl	$-10, %edx
	leaq	-32(%rbp), %rdi
	subl	%esi, %edx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	sall	$3, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-32(%rbp), %rax
	movl	-24(%rbp), %edx
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L358
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L358:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19671:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6417register_locationEi, .-_ZN2v88internal23RegExpMacroAssemblerX6417register_locationEi
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6417BranchOrBacktrackENS0_9ConditionEPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6417BranchOrBacktrackENS0_9ConditionEPNS0_5LabelE
	.type	_ZN2v88internal23RegExpMacroAssemblerX6417BranchOrBacktrackENS0_9ConditionEPNS0_5LabelE, @function
_ZN2v88internal23RegExpMacroAssemblerX6417BranchOrBacktrackENS0_9ConditionEPNS0_5LabelE:
.LFB19673:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	testl	%esi, %esi
	js	.L363
	leaq	32(%rdi), %r9
	testq	%rdx, %rdx
	je	.L364
	movl	$1, %ecx
	movq	%r9, %rdi
	jmp	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	.p2align 4,,10
	.p2align 3
.L363:
	testq	%rdx, %rdx
	je	.L365
	addq	$32, %rdi
	movl	$1, %edx
	movq	%r8, %rsi
	jmp	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	.p2align 4,,10
	.p2align 3
.L364:
	leaq	660(%rdi), %rdx
	movl	$1, %ecx
	movq	%r9, %rdi
	jmp	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	.p2align 4,,10
	.p2align 3
.L365:
	movq	(%rdi), %rax
	jmp	*56(%rax)
	.cfi_endproc
.LFE19673:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6417BranchOrBacktrackENS0_9ConditionEPNS0_5LabelE, .-_ZN2v88internal23RegExpMacroAssemblerX6417BranchOrBacktrackENS0_9ConditionEPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6426FixupCodeRelativePositionsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6426FixupCodeRelativePositionsEv
	.type	_ZN2v88internal23RegExpMacroAssemblerX6426FixupCodeRelativePositionsEv, @function
_ZN2v88internal23RegExpMacroAssemblerX6426FixupCodeRelativePositionsEv:
.LFB19679:
	.cfi_startproc
	endbr64
	movq	616(%rdi), %r8
	movq	608(%rdi), %rsi
	testq	%r8, %r8
	je	.L372
	movl	4(%r8), %r9d
	testq	%r9, %r9
	sete	%r10b
	cmpl	(%r8), %r9d
	je	.L403
	.p2align 4,,10
	.p2align 3
.L402:
	cmpq	%rsi, %r8
	jne	.L383
	testb	%r10b, %r10b
	jne	.L372
.L383:
	xorl	%ecx, %ecx
.L378:
	movl	24(%rsi,%rcx,4), %edx
	addq	$1, %rcx
	leal	-4(%rdx), %eax
	cltq
	addq	48(%rdi), %rax
	addl	(%rax), %edx
	addl	$63, %edx
	movl	%edx, (%rax)
	movl	(%rsi), %eax
	cmpq	%rcx, %rax
	jbe	.L374
	cmpq	%rsi, %r8
	jne	.L378
	cmpq	%rcx, %r9
	jne	.L378
.L372:
	cmpq	$0, 600(%rdi)
	je	.L404
	movq	608(%rdi), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L379
	.p2align 4,,10
	.p2align 3
.L380:
	movq	8(%rax), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L380
.L379:
	movl	$0, 4(%rax)
	movq	%rax, 616(%rdi)
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L382
	.p2align 4,,10
	.p2align 3
.L381:
	movl	$0, 4(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L381
.L382:
	movq	$0, 600(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L374:
	movq	8(%rsi), %rsi
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L403:
	movq	8(%r8), %r8
	movl	$1, %r10d
	xorl	%r9d, %r9d
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L404:
	ret
	.cfi_endproc
.LFE19679:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6426FixupCodeRelativePositionsEv, .-_ZN2v88internal23RegExpMacroAssemblerX6426FixupCodeRelativePositionsEv
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX647GetCodeENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX647GetCodeENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal23RegExpMacroAssemblerX647GetCodeENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal23RegExpMacroAssemblerX647GetCodeENS0_6HandleINS0_6StringEEE:
.LFB19643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movabsq	$81604378624, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	32(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$392, %rsp
	movq	%rsi, -424(%rbp)
	leaq	636(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -352(%rbp)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movzbl	568(%rbx), %eax
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rbpE(%rip), %r13d
	movb	$1, 568(%rbx)
	movl	%r13d, %esi
	movb	%al, -373(%rbp)
	movb	%al, -292(%rbp)
	movq	%r12, -304(%rbp)
	movl	$24, -296(%rbp)
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	$8, %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rspE(%rip), %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rdxE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rcxE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL2r8E(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL2r9E(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_9ImmediateE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_9ImmediateE@PLT
	movq	544(%rbx), %rdi
	movq	$0, -344(%rbp)
	movq	$0, -336(%rbp)
	call	_ZN2v88internal17ExternalReference18address_of_jslimitEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL3rspE(%rip), %edx
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rcxE(%rip), %esi
	movq	%rax, %r14
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_17ExternalReferenceE@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	leaq	-92(%rbp), %rdi
	xorl	%edx, %edx
	movq	%rdi, -392(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-84(%rbp), %r8d
	movq	-92(%rbp), %rcx
	movq	%r12, %rdi
	movl	$8, %r9d
	movl	$1, %edx
	movl	$43, %esi
	movl	%r8d, -72(%rbp)
	movl	%r8d, -60(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rcx, -68(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	leaq	-344(%rbp), %r9
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r9, %rdx
	movl	$6, %esi
	movq	%r9, -368(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	628(%rbx), %eax
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	$1, %edx
	movl	$7, %esi
	leal	0(,%rax,8), %ecx
	orq	%r15, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	leaq	-336(%rbp), %r8
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r8, %rdx
	movl	$3, %esi
	movq	%r8, -360(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %r14d
	movq	$-1, %rdx
	movq	%r12, %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	leaq	-352(%rbp), %rax
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -384(%rbp)
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	-368(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	552(%rbx), %rdx
	movl	$3, %ecx
	movq	%r12, %rdi
	movl	$8, %esi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEENS0_9RelocInfo4ModeE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpMacroAssemblerX6424CallCheckStackGuardStateEv
	movl	$8, %ecx
	movl	%r14d, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	movq	-384(%rbp), %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-360(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	628(%rbx), %eax
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	$4, %edx
	movl	$5, %esi
	leal	0(,%rax,8), %ecx
	orq	%r15, %rcx
	leaq	-80(%rbp), %r15
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	$-32, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	movq	%r15, -360(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	movl	$8, %r8d
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$-24, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movl	$8, %r8d
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$8, %r8d
	movl	$6, %ecx
	movq	%r12, %rdi
	movl	$7, %edx
	movl	$43, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	$-16, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	movl	$8, %r8d
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	movl	$8, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_negENS0_8RegisterEi@PLT
	movl	624(%rbx), %r8d
	cmpl	$2, %r8d
	je	.L453
	negl	%r8d
	xorl	%ecx, %ecx
.L450:
	movl	_ZN2v88internalL3rbxE(%rip), %edx
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	leaq	-320(%rbp), %r15
	movq	-360(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movl	%r14d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movq	-360(%rbp), %rdi
	movl	$-72, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rsi
	movl	-72(%rbp), %edx
	movl	%r14d, %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rsi, -68(%rbp)
	movl	%edx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movq	552(%rbx), %rdx
	movl	$3, %ecx
	movq	%r12, %rdi
	movl	$8, %esi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEENS0_9RelocInfo4ModeE@PLT
	movq	-392(%rbp), %rdi
	movl	$-16, %edx
	movl	%r13d, %esi
	movq	$0, -328(%rbp)
	movq	$0, -320(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-92(%rbp), %rdx
	movl	-84(%rbp), %ecx
	movq	%r12, %rdi
	movl	$4, %r9d
	movl	$7, %esi
	movabsq	$81604378624, %r8
	movq	%rdx, -80(%rbp)
	movl	%ecx, -72(%rbp)
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	xorl	%ecx, %ecx
	movl	$5, %esi
	movq	%r12, %rdi
	leaq	-328(%rbp), %rax
	movq	%rax, %rdx
	movq	%rax, -408(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$10, %edx
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	-408(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-68(%rbp), %rax
	cmpl	$1, 624(%rbx)
	movq	%rax, -368(%rbp)
	je	.L454
	movl	_ZN2v88internalL3rdiE(%rip), %edx
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	movl	$-2, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-68(%rbp), %rdx
	movl	-60(%rbp), %ecx
	movq	%r12, %rdi
	movl	$4, %r8d
	movl	$2, %esi
	movq	%rdx, -80(%rbp)
	movl	%ecx, -72(%rbp)
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterENS0_7OperandEi@PLT
.L409:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	632(%rbx), %eax
	testl	%eax, %eax
	jle	.L451
	xorl	%r15d, %r15d
	cmpl	$8, %eax
	jg	.L455
	.p2align 4,,10
	.p2align 3
.L412:
	movl	%r15d, %edx
	addl	$1, %r15d
	cmpl	%edx, 628(%rbx)
	jg	.L413
	movl	%r15d, 628(%rbx)
.L413:
	movl	%r15d, %edx
	movq	-368(%rbp), %rdi
	movl	%r13d, %esi
	negl	%edx
	leal	-72(,%rdx,8), %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movl	%r14d, %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rsi, -116(%rbp)
	movl	%edx, -108(%rbp)
	movq	%rsi, -140(%rbp)
	movl	%edx, -132(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	cmpl	%r15d, 632(%rbx)
	jg	.L412
.L451:
	leaq	-224(%rbp), %rax
	movq	%rax, -416(%rbp)
.L411:
	movq	-360(%rbp), %rdi
	movl	$16, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$1, %esi
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	leaq	644(%rbx), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movl	652(%rbx), %r11d
	leaq	668(%rbx), %rax
	movq	%rax, -400(%rbp)
	testl	%r11d, %r11d
	jg	.L456
.L415:
	movq	-400(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	12(%rbx), %edi
	testl	%edi, %edi
	jne	.L457
.L431:
	movq	-384(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-360(%rbp), %rdi
	movl	$-56, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	movl	$8, %r8d
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$8, %ecx
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rspE(%rip), %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3retEi@PLT
	movl	660(%rbx), %esi
	testl	%esi, %esi
	jle	.L432
	movq	%r12, %rdi
	leaq	660(%rbx), %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
.L432:
	movl	676(%rbx), %ecx
	movq	$0, -312(%rbp)
	testl	%ecx, %ecx
	jle	.L433
	leaq	676(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-368(%rbp), %r15
	movl	_ZN2v88internalL3rspE(%rip), %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %r8d
	movq	-68(%rbp), %rcx
	movq	%r12, %rdi
	movl	$8, %r9d
	movl	$8, %edx
	movl	$41, %esi
	movl	%r8d, -84(%rbp)
	movl	%r8d, -72(%rbp)
	movq	%rcx, -92(%rbp)
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpMacroAssemblerX6424CallCheckStackGuardStateEv
	movl	$8, %ecx
	movl	%r14d, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	movq	-384(%rbp), %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	552(%rbx), %rdx
	movl	$3, %ecx
	movq	%r12, %rdi
	movl	$8, %esi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEENS0_9RelocInfo4ModeE@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movq	-360(%rbp), %rdi
	movl	$-32, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	movl	$8, %r8d
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL3rspE(%rip), %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rcx
	movl	-60(%rbp), %r8d
	movq	%r12, %rdi
	movl	$1, %esi
	movl	$8, %r9d
	movl	$8, %edx
	movq	%rcx, -92(%rbp)
	movl	%r8d, -84(%rbp)
	movq	%rcx, -80(%rbp)
	movl	%r8d, -72(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3retEi@PLT
.L433:
	movl	684(%rbx), %edx
	testl	%edx, %edx
	jle	.L434
	leaq	684(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-368(%rbp), %r15
	movl	_ZN2v88internalL3rspE(%rip), %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %r8d
	movq	-68(%rbp), %rcx
	movq	%r12, %rdi
	movl	$8, %r9d
	movl	$8, %edx
	movl	$41, %esi
	movl	%r8d, -84(%rbp)
	movl	%r8d, -72(%rbp)
	movq	%rcx, -92(%rbp)
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-360(%rbp), %rdi
	movl	$16, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	movl	$8, %r8d
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movq	544(%rbx), %rdi
	call	_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL3rdxE(%rip), %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal14TurboAssembler11LoadAddressENS0_8RegisterENS0_17ExternalReferenceE@PLT
	movq	544(%rbx), %rdi
	call	_ZN2v88internal17ExternalReference13re_grow_stackEPNS0_7IsolateE@PLT
	movl	$3, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	movl	$8, %ecx
	movl	%r14d, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	leaq	-312(%rbp), %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$4, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$8, %ecx
	movl	%r14d, %edx
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	552(%rbx), %rdx
	movl	$3, %ecx
	movq	%r12, %rdi
	movl	$8, %esi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEENS0_9RelocInfo4ModeE@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rspE(%rip), %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rcx
	movl	-60(%rbp), %r8d
	movq	%r12, %rdi
	movl	$1, %esi
	movl	$8, %r9d
	movl	$8, %edx
	movq	%rcx, -92(%rbp)
	movl	%r8d, -84(%rbp)
	movq	%rcx, -80(%rbp)
	movl	%r8d, -72(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3retEi@PLT
.L434:
	movl	-312(%rbp), %eax
	testl	%eax, %eax
	jle	.L435
	leaq	-312(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	$-1, %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	movq	-384(%rbp), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
.L435:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpMacroAssemblerX6426FixupCodeRelativePositionsEv
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	544(%rbx), %r13
	movq	-416(%rbp), %r14
	xorl	%ecx, %ecx
	leaq	-288(%rbp), %r12
	movaps	%xmm0, -224(%rbp)
	movq	%r14, %rdx
	movq	%r13, %rsi
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal9Assembler7GetCodeEPNS0_7IsolateEPNS0_8CodeDescEPNS0_21SafepointTableBuilderEi@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movl	$4, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11CodeBuilderC1EPNS0_7IsolateERKNS0_8CodeDescENS0_4Code4KindE@PLT
	movq	552(%rbx), %rax
	movq	%r12, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal7Factory11CodeBuilder5BuildEv@PLT
	movq	41488(%r13), %r12
	movq	-424(%rbp), %rcx
	movq	%rax, -360(%rbp)
	movq	(%rax), %r13
	leaq	56(%r12), %r15
	movq	(%rcx), %r14
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L437
	.p2align 4,,10
	.p2align 3
.L436:
	movq	8(%r12), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*80(%rax)
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.L436
.L437:
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movzbl	-373(%rbp), %eax
	movb	%al, 568(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L458
	movq	-360(%rbp), %rax
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L456:
	.cfi_restore_state
	leaq	652(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	632(%rbx), %r10d
	testl	%r10d, %r10d
	jg	.L416
.L420:
	movl	12(%rbx), %r9d
	testl	%r9d, %r9d
	je	.L459
	movq	-360(%rbp), %rdi
	movl	$-64, %edx
	movl	%r13d, %esi
	movabsq	$81604378624, %r15
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rsi
	movl	-72(%rbp), %edx
	movq	%r12, %rdi
	movl	$8, %ecx
	movq	%rsi, -68(%rbp)
	movl	%edx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_incENS0_7OperandEi@PLT
	movq	-368(%rbp), %rdi
	movl	$-48, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rcxE(%rip), %esi
	call	_ZN2v88internal9Assembler7movsxlqENS0_8RegisterENS0_7OperandE@PLT
	movl	632(%rbx), %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	$1, %edx
	movl	$5, %esi
	andl	$-1, %ecx
	orq	%r15, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	632(%rbx), %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	$1, %edx
	movl	$7, %esi
	andl	$-1, %ecx
	orq	%r15, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	-400(%rbp), %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$12, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-360(%rbp), %rdi
	movl	$-48, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rsi
	movl	-72(%rbp), %edx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rcxE(%rip), %ecx
	movl	$8, %r8d
	movq	%rsi, -68(%rbp)
	movl	%edx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movl	632(%rbx), %eax
	movl	$-40, %edx
	movl	%r13d, %esi
	movq	-392(%rbp), %rdi
	sall	$2, %eax
	movl	%eax, -372(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-84(%rbp), %ecx
	movq	-92(%rbp), %rdx
	xorl	%esi, %esi
	movl	-372(%rbp), %r8d
	movl	$8, %r9d
	movq	%r12, %rdi
	movl	%ecx, -72(%rbp)
	orq	%r15, %r8
	movl	%ecx, -60(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	movq	-360(%rbp), %rdi
	movl	$-72, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movl	%r14d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	12(%rbx), %eax
	subl	$2, %eax
	cmpl	$1, %eax
	jbe	.L460
.L427:
	movq	-408(%rbp), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L454:
	movl	_ZN2v88internalL3rdiE(%rip), %edx
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movl	$-1, %r8d
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-68(%rbp), %rdx
	movl	-60(%rbp), %ecx
	movq	%r12, %rdi
	movl	$4, %r8d
	movl	$2, %esi
	movq	%rdx, -80(%rbp)
	movl	%ecx, -72(%rbp)
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L453:
	movl	$-2, %r8d
	movl	$1, %ecx
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L457:
	movq	-360(%rbp), %rdi
	movl	%r13d, %esi
	movl	$-64, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movl	%r14d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L459:
	movl	$8, %ecx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movabsq	$81604378625, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L455:
	movl	_ZN2v88internalL3rcxE(%rip), %esi
	movq	%r12, %rdi
	movq	$-80, %rdx
	movabsq	$81604378624, %r15
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	leaq	-224(%rbp), %rax
	movq	%r12, %rdi
	movq	$0, -224(%rbp)
	movq	%rax, %rsi
	movq	%rax, -416(%rbp)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	_ZN2v88internalL3rcxE(%rip), %edx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	-360(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-80(%rbp), %rsi
	movl	-72(%rbp), %edx
	movl	%r14d, %ecx
	movq	%r12, %rdi
	movl	$8, %r8d
	movq	%rsi, -68(%rbp)
	movl	%edx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movq	%r15, %rcx
	movq	%r12, %rdi
	movl	$1, %edx
	orq	$8, %rcx
	movl	$8, %r8d
	movl	$5, %esi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	$-10, %ecx
	subl	632(%rbx), %ecx
	movq	%r12, %rdi
	sall	$3, %ecx
	movl	$8, %r8d
	movl	$1, %edx
	movl	$7, %esi
	orq	%r15, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	-416(%rbp), %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$15, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L460:
	movl	$8, %r8d
	movl	$2, %ecx
	movl	$7, %edx
	movq	%r12, %rdi
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	-408(%rbp), %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %edx
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	%edx, %esi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	xorl	%ecx, %ecx
	movl	$4, %esi
	movq	%r12, %rdi
	movq	-400(%rbp), %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-416(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -224(%rbp)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	cmpl	$2, 624(%rbx)
	je	.L461
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movl	$8, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_incENS0_8RegisterEi@PLT
.L429:
	cmpl	$3, 12(%rbx)
	jne	.L427
	movq	-416(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal20RegExpMacroAssembler23CheckNotInSurrogatePairEiPNS0_5LabelE@PLT
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L416:
	movq	-360(%rbp), %r15
	movl	$-16, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rdxE(%rip), %esi
	movl	$8, %r8d
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$-40, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	movl	$8, %r8d
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$-32, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rcxE(%rip), %esi
	movl	$8, %r8d
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-392(%rbp), %rdi
	movl	$-24, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-92(%rbp), %rcx
	movl	-84(%rbp), %r8d
	movq	%r12, %rdi
	movl	$8, %r9d
	movl	$1, %edx
	movl	$43, %esi
	movq	%rcx, -80(%rbp)
	movl	%r8d, -72(%rbp)
	movq	%rcx, -68(%rbp)
	movl	%r8d, -60(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	cmpl	$2, 624(%rbx)
	je	.L462
	movl	$8, %r8d
	movl	$2, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$3, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
.L422:
	movl	632(%rbx), %r8d
	testl	%r8d, %r8d
	jle	.L420
	xorl	%r15d, %r15d
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L425:
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	movq	-360(%rbp), %rdi
	leal	0(,%r15,4), %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rsi
	movl	-72(%rbp), %edx
	movl	%r14d, %ecx
	movl	$4, %r8d
	movq	%r12, %rdi
	movq	%rsi, -68(%rbp)
	movl	%edx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movl	-372(%rbp), %eax
	cmpl	%eax, 632(%rbx)
	jle	.L420
	movl	-372(%rbp), %r15d
.L421:
	leal	1(%r15), %eax
	movl	%eax, -372(%rbp)
	cmpl	%r15d, 628(%rbx)
	jg	.L423
	movl	%eax, 628(%rbx)
.L423:
	movl	%r15d, %edx
	movq	-368(%rbp), %rdi
	movl	%r13d, %esi
	negl	%edx
	leal	-80(,%rdx,8), %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rdx
	movl	-60(%rbp), %ecx
	movl	%r14d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rdx, -104(%rbp)
	movl	%ecx, -96(%rbp)
	movq	%rdx, -128(%rbp)
	movl	%ecx, -120(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	testl	%r15d, %r15d
	jne	.L424
	movl	12(%rbx), %eax
	leal	-2(%rax), %edx
	cmpl	$1, %edx
	jbe	.L463
.L424:
	xorl	%edx, %edx
	movl	$8, %r8d
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$3, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	cmpl	$2, 624(%rbx)
	jne	.L425
	movabsq	$-4294967296, %rdx
	movl	$7, %ecx
	movl	%r14d, %esi
	movq	%r12, %rdi
	andq	-432(%rbp), %rdx
	movl	$8, %r8d
	movabsq	$-1095216660481, %rax
	orq	$1, %rdx
	andq	%rax, %rdx
	movabsq	$81604378624, %rax
	orq	%rax, %rdx
	movq	%rdx, -432(%rbp)
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L463:
	movl	_ZN2v88internalL3rdxE(%rip), %esi
	movl	$8, %ecx
	movl	%r14d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L462:
	movq	-360(%rbp), %rdi
	movl	_ZN2v88internalL3rdxE(%rip), %edx
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	_ZN2v88internalL3rcxE(%rip), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rcxE(%rip), %esi
	movl	$8, %r8d
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L422
.L461:
	movl	$8, %r8d
	movl	$7, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movabsq	$-1095216660481, %rcx
	andl	$2, %ecx
	orq	%r15, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L429
.L458:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19643:
	.size	_ZN2v88internal23RegExpMacroAssemblerX647GetCodeENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal23RegExpMacroAssemblerX647GetCodeENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6415CheckPreemptionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6415CheckPreemptionEv
	.type	_ZN2v88internal23RegExpMacroAssemblerX6415CheckPreemptionEv, @function
_ZN2v88internal23RegExpMacroAssemblerX6415CheckPreemptionEv:
.LFB19683:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	32(%rbx), %r12
	subq	$24, %rsp
	movq	544(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	call	_ZN2v88internal17ExternalReference18address_of_jslimitEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8load_raxENS0_17ExternalReferenceE@PLT
	movl	$8, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movl	$4, %edx
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$7, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	leaq	676(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4callEPNS0_5LabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L467
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L467:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19683:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6415CheckPreemptionEv, .-_ZN2v88internal23RegExpMacroAssemblerX6415CheckPreemptionEv
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6415CheckStackLimitEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6415CheckStackLimitEv
	.type	_ZN2v88internal23RegExpMacroAssemblerX6415CheckStackLimitEv, @function
_ZN2v88internal23RegExpMacroAssemblerX6415CheckStackLimitEv:
.LFB19684:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	32(%rbx), %r12
	subq	$24, %rsp
	movq	544(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	call	_ZN2v88internal17ExternalReference37address_of_regexp_stack_limit_addressEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8load_raxENS0_17ExternalReferenceE@PLT
	movl	$8, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movl	$1, %edx
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$7, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	leaq	684(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4callEPNS0_5LabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L471
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L471:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19684:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6415CheckStackLimitEv, .-_ZN2v88internal23RegExpMacroAssemblerX6415CheckStackLimitEv
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX6429LoadCurrentCharacterUncheckedEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6429LoadCurrentCharacterUncheckedEii
	.type	_ZN2v88internal23RegExpMacroAssemblerX6429LoadCurrentCharacterUncheckedEii, @function
_ZN2v88internal23RegExpMacroAssemblerX6429LoadCurrentCharacterUncheckedEii:
.LFB19685:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	32(%rdi), %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$1, 624(%rdi)
	je	.L481
	xorl	%ecx, %ecx
	leal	(%rsi,%rsi), %r8d
	cmpl	$2, %edx
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	movl	_ZN2v88internalL3rdiE(%rip), %edx
	leaq	-48(%rbp), %rdi
	je	.L480
.L477:
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-40(%rbp), %ecx
	movq	-48(%rbp), %rdx
	movq	%r12, %rdi
	movl	$4, %r8d
	movl	$2, %esi
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterENS0_7OperandEi@PLT
.L472:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L482
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L483:
	.cfi_restore_state
	movl	_ZN2v88internalL3rdiE(%rip), %edx
	movl	_ZN2v88internalL3rsiE(%rip), %esi
.L480:
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-40(%rbp), %ecx
	movq	-48(%rbp), %rdx
	movq	%r12, %rdi
	movl	$4, %r8d
	movl	$2, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L481:
	leaq	-48(%rbp), %rdi
	movl	%esi, %r8d
	xorl	%ecx, %ecx
	cmpl	$4, %edx
	je	.L483
	cmpl	$2, %edx
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	movl	_ZN2v88internalL3rdiE(%rip), %edx
	je	.L477
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-40(%rbp), %ecx
	movq	-48(%rbp), %rdx
	movq	%r12, %rdi
	movl	$4, %r8d
	movl	$2, %esi
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L472
.L482:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19685:
	.size	_ZN2v88internal23RegExpMacroAssemblerX6429LoadCurrentCharacterUncheckedEii, .-_ZN2v88internal23RegExpMacroAssemblerX6429LoadCurrentCharacterUncheckedEii
	.section	.text._ZN2v88internal13ZoneChunkListIiEC2EPNS0_4ZoneENS2_9StartModeE,"axG",@progbits,_ZN2v88internal13ZoneChunkListIiEC5EPNS0_4ZoneENS2_9StartModeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ZoneChunkListIiEC2EPNS0_4ZoneENS2_9StartModeE
	.type	_ZN2v88internal13ZoneChunkListIiEC2EPNS0_4ZoneENS2_9StartModeE, @function
_ZN2v88internal13ZoneChunkListIiEC2EPNS0_4ZoneENS2_9StartModeE:
.LFB20212:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movups	%xmm0, 16(%rdi)
	testl	%edx, %edx
	jne	.L492
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L492:
	.cfi_restore_state
	movq	%rsi, %rdi
	movl	%edx, %eax
	leaq	31(,%rax,4), %rsi
	movq	24(%rdi), %rcx
	movq	16(%rdi), %rax
	andq	$-8, %rsi
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L493
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L487:
	pxor	%xmm0, %xmm0
	movl	$0, 4(%rax)
	movl	%edx, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%rbx)
	movq	%rax, 24(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L493:
	.cfi_restore_state
	movl	%edx, -20(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-20(%rbp), %edx
	jmp	.L487
	.cfi_endproc
.LFE20212:
	.size	_ZN2v88internal13ZoneChunkListIiEC2EPNS0_4ZoneENS2_9StartModeE, .-_ZN2v88internal13ZoneChunkListIiEC2EPNS0_4ZoneENS2_9StartModeE
	.weak	_ZN2v88internal13ZoneChunkListIiEC1EPNS0_4ZoneENS2_9StartModeE
	.set	_ZN2v88internal13ZoneChunkListIiEC1EPNS0_4ZoneENS2_9StartModeE,_ZN2v88internal13ZoneChunkListIiEC2EPNS0_4ZoneENS2_9StartModeE
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX64C2EPNS0_7IsolateEPNS0_4ZoneENS0_26NativeRegExpMacroAssembler4ModeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX64C2EPNS0_7IsolateEPNS0_4ZoneENS0_26NativeRegExpMacroAssembler4ModeEi
	.type	_ZN2v88internal23RegExpMacroAssemblerX64C2EPNS0_7IsolateEPNS0_4ZoneENS0_26NativeRegExpMacroAssembler4ModeEi, @function
_ZN2v88internal23RegExpMacroAssemblerX64C2EPNS0_7IsolateEPNS0_4ZoneENS0_26NativeRegExpMacroAssembler4ModeEi:
.LFB19612:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	32(%rbx), %r12
	subq	$72, %rsp
	movl	%ecx, -100(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal26NativeRegExpMacroAssemblerC2EPNS0_7IsolateEPNS0_4ZoneE@PLT
	leaq	16+_ZTVN2v88internal23RegExpMacroAssemblerX64E(%rip), %rax
	leaq	-96(%rbp), %rdi
	movl	$1024, %esi
	movq	%rax, (%rbx)
	call	_ZN2v88internal18NewAssemblerBufferEi@PLT
	movq	-96(%rbp), %rax
	leaq	-80(%rbp), %r10
	xorl	%edx, %edx
	movq	%r10, %rdi
	movq	%r15, %rsi
	movq	%r10, -112(%rbp)
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN2v88internal16AssemblerOptions7DefaultEPNS0_7IsolateEb@PLT
	movq	-112(%rbp), %r10
	movq	%r12, %rdi
	leaq	-88(%rbp), %r8
	movl	$1, %ecx
	movq	%r15, %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal18TurboAssemblerBaseC1EPNS0_7IsolateERKNS0_16AssemblerOptionsENS0_18CodeObjectRequiredESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS9_EE@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L495
	movq	(%rdi), %rax
	call	*8(%rax)
.L495:
	movq	-96(%rbp), %rdi
	leaq	16+_ZTVN2v88internal14MacroAssemblerE(%rip), %rax
	movq	%rax, 32(%rbx)
	testq	%rdi, %rdi
	je	.L496
	movq	(%rdi), %rax
	call	*8(%rax)
.L496:
	movzbl	560(%rbx), %eax
	movq	%r12, 576(%rbx)
	xorl	%edx, %edx
	leaq	592(%rbx), %rdi
	movb	$0, 560(%rbx)
	movq	%r14, %rsi
	movb	%al, 584(%rbx)
	call	_ZN2v88internal13ZoneChunkListIiEC1EPNS0_4ZoneENS2_9StartModeE
	movl	-100(%rbp), %eax
	movl	$1, %edx
	movq	%r12, %rdi
	movl	%r13d, 628(%rbx)
	leaq	636(%rbx), %rsi
	movl	%eax, 624(%rbx)
	movl	%r13d, 632(%rbx)
	movq	$0, 636(%rbx)
	movq	$0, 644(%rbx)
	movq	$0, 652(%rbx)
	movq	$0, 660(%rbx)
	movq	$0, 668(%rbx)
	movq	$0, 676(%rbx)
	movq	$0, 684(%rbx)
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	leaq	644(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L505
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L505:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19612:
	.size	_ZN2v88internal23RegExpMacroAssemblerX64C2EPNS0_7IsolateEPNS0_4ZoneENS0_26NativeRegExpMacroAssembler4ModeEi, .-_ZN2v88internal23RegExpMacroAssemblerX64C2EPNS0_7IsolateEPNS0_4ZoneENS0_26NativeRegExpMacroAssembler4ModeEi
	.globl	_ZN2v88internal23RegExpMacroAssemblerX64C1EPNS0_7IsolateEPNS0_4ZoneENS0_26NativeRegExpMacroAssembler4ModeEi
	.set	_ZN2v88internal23RegExpMacroAssemblerX64C1EPNS0_7IsolateEPNS0_4ZoneENS0_26NativeRegExpMacroAssembler4ModeEi,_ZN2v88internal23RegExpMacroAssemblerX64C2EPNS0_7IsolateEPNS0_4ZoneENS0_26NativeRegExpMacroAssembler4ModeEi
	.section	.text._ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,"axG",@progbits,_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.type	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, @function
_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E:
.LFB21862:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L514
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L508:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L508
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L514:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE21862:
	.size	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, .-_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.section	.text._ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	.type	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E, @function
_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E:
.LFB22742:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L532
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L521:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r12), %rdi
	movq	16(%r12), %rbx
	testq	%rdi, %rdi
	je	.L519
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L517
.L520:
	movq	%rbx, %r12
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L519:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L520
.L517:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L532:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE22742:
	.size	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E, .-_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	.section	.text._ZN2v88internal9AssemblerD2Ev,"axG",@progbits,_ZN2v88internal9AssemblerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9AssemblerD2Ev
	.type	_ZN2v88internal9AssemblerD2Ev, @function
_ZN2v88internal9AssemblerD2Ev:
.LFB19595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	480(%rdi), %r13
	leaq	464(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L539
.L536:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L536
.L539:
	movq	424(%r12), %r13
	leaq	408(%r12), %r14
	testq	%r13, %r13
	je	.L537
.L538:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L542
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L537
.L543:
	movq	%rbx, %r13
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L542:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L543
.L537:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L541
	movq	400(%r12), %rax
	movq	368(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L544
	.p2align 4,,10
	.p2align 3
.L545:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L545
	movq	328(%r12), %rdi
.L544:
	call	_ZdlPv@PLT
.L541:
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L546
	movq	312(%r12), %rax
	movq	280(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L547
	.p2align 4,,10
	.p2align 3
.L548:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L548
	movq	240(%r12), %rdi
.L547:
	call	_ZdlPv@PLT
.L546:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	.cfi_endproc
.LFE19595:
	.size	_ZN2v88internal9AssemblerD2Ev, .-_ZN2v88internal9AssemblerD2Ev
	.weak	_ZN2v88internal9AssemblerD1Ev
	.set	_ZN2v88internal9AssemblerD1Ev,_ZN2v88internal9AssemblerD2Ev
	.section	.text._ZN2v88internal14MacroAssemblerD2Ev,"axG",@progbits,_ZN2v88internal14MacroAssemblerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14MacroAssemblerD2Ev
	.type	_ZN2v88internal14MacroAssemblerD2Ev, @function
_ZN2v88internal14MacroAssemblerD2Ev:
.LFB23781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	480(%rdi), %r13
	leaq	464(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L573
.L570:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L570
.L573:
	movq	424(%r12), %r13
	leaq	408(%r12), %r14
	testq	%r13, %r13
	je	.L571
.L572:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L576
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L571
.L577:
	movq	%rbx, %r13
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L576:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L577
.L571:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L575
	movq	400(%r12), %rax
	movq	368(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L578
	.p2align 4,,10
	.p2align 3
.L579:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L579
	movq	328(%r12), %rdi
.L578:
	call	_ZdlPv@PLT
.L575:
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L580
	movq	312(%r12), %rax
	movq	280(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L581
	.p2align 4,,10
	.p2align 3
.L582:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L582
	movq	240(%r12), %rdi
.L581:
	call	_ZdlPv@PLT
.L580:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	.cfi_endproc
.LFE23781:
	.size	_ZN2v88internal14MacroAssemblerD2Ev, .-_ZN2v88internal14MacroAssemblerD2Ev
	.weak	_ZN2v88internal14MacroAssemblerD1Ev
	.set	_ZN2v88internal14MacroAssemblerD1Ev,_ZN2v88internal14MacroAssemblerD2Ev
	.section	.text._ZN2v88internal9AssemblerD0Ev,"axG",@progbits,_ZN2v88internal9AssemblerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9AssemblerD0Ev
	.type	_ZN2v88internal9AssemblerD0Ev, @function
_ZN2v88internal9AssemblerD0Ev:
.LFB19597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	480(%rdi), %r13
	leaq	464(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L607
.L604:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L604
.L607:
	movq	424(%r12), %r13
	leaq	408(%r12), %r14
	testq	%r13, %r13
	je	.L605
.L606:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L610
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L605
.L611:
	movq	%rbx, %r13
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L610:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L611
.L605:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L609
	movq	400(%r12), %rax
	movq	368(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L612
	.p2align 4,,10
	.p2align 3
.L613:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L613
	movq	328(%r12), %rdi
.L612:
	call	_ZdlPv@PLT
.L609:
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L614
	movq	312(%r12), %rax
	movq	280(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L615
	.p2align 4,,10
	.p2align 3
.L616:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L616
	movq	240(%r12), %rdi
.L615:
	call	_ZdlPv@PLT
.L614:
	movq	%r12, %rdi
	call	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE19597:
	.size	_ZN2v88internal9AssemblerD0Ev, .-_ZN2v88internal9AssemblerD0Ev
	.section	.text._ZN2v88internal14MacroAssemblerD0Ev,"axG",@progbits,_ZN2v88internal14MacroAssemblerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14MacroAssemblerD0Ev
	.type	_ZN2v88internal14MacroAssemblerD0Ev, @function
_ZN2v88internal14MacroAssemblerD0Ev:
.LFB23783:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	480(%rdi), %r13
	leaq	464(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L641
.L638:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L638
.L641:
	movq	424(%r12), %r13
	leaq	408(%r12), %r14
	testq	%r13, %r13
	je	.L639
.L640:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L644
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L639
.L645:
	movq	%rbx, %r13
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L644:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L645
.L639:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L643
	movq	400(%r12), %rax
	movq	368(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L646
	.p2align 4,,10
	.p2align 3
.L647:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L647
	movq	328(%r12), %rdi
.L646:
	call	_ZdlPv@PLT
.L643:
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L648
	movq	312(%r12), %rax
	movq	280(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L649
	.p2align 4,,10
	.p2align 3
.L650:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L650
	movq	240(%r12), %rdi
.L649:
	call	_ZdlPv@PLT
.L648:
	movq	%r12, %rdi
	call	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE23783:
	.size	_ZN2v88internal14MacroAssemblerD0Ev, .-_ZN2v88internal14MacroAssemblerD0Ev
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX64D0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX64D0Ev
	.type	_ZN2v88internal23RegExpMacroAssemblerX64D0Ev, @function
_ZN2v88internal23RegExpMacroAssemblerX64D0Ev:
.LFB19617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal23RegExpMacroAssemblerX64E(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	32(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	496(%rdi), %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movzbl	584(%rdi), %edx
	movq	576(%rdi), %rax
	movl	$0, 636(%rdi)
	movl	$0, 644(%rdi)
	movl	$0, 652(%rdi)
	movl	$0, 660(%rdi)
	movl	$0, 668(%rdi)
	movl	$0, 676(%rdi)
	movl	$0, 684(%rdi)
	movb	%dl, 528(%rax)
	movq	512(%rdi), %r13
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rax, 32(%rdi)
	testq	%r13, %r13
	je	.L675
.L672:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L672
.L675:
	movq	456(%r12), %r13
	leaq	440(%r12), %r15
	testq	%r13, %r13
	je	.L673
.L674:
	movq	24(%r13), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L678
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L673
.L679:
	movq	%rbx, %r13
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L678:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L679
.L673:
	movq	360(%r12), %rdi
	testq	%rdi, %rdi
	je	.L677
	movq	432(%r12), %rax
	movq	400(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L680
	.p2align 4,,10
	.p2align 3
.L681:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L681
	movq	360(%r12), %rdi
.L680:
	call	_ZdlPv@PLT
.L677:
	movq	272(%r12), %rdi
	testq	%rdi, %rdi
	je	.L682
	movq	344(%r12), %rax
	movq	312(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L683
	.p2align 4,,10
	.p2align 3
.L684:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L684
	movq	272(%r12), %rdi
.L683:
	call	_ZdlPv@PLT
.L682:
	movq	%r14, %rdi
	call	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal26NativeRegExpMacroAssemblerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$696, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE19617:
	.size	_ZN2v88internal23RegExpMacroAssemblerX64D0Ev, .-_ZN2v88internal23RegExpMacroAssemblerX64D0Ev
	.section	.text._ZN2v88internal23RegExpMacroAssemblerX64D2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpMacroAssemblerX64D2Ev
	.type	_ZN2v88internal23RegExpMacroAssemblerX64D2Ev, @function
_ZN2v88internal23RegExpMacroAssemblerX64D2Ev:
.LFB19615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal23RegExpMacroAssemblerX64E(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	32(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	496(%rdi), %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movzbl	584(%rdi), %edx
	movq	576(%rdi), %rax
	movl	$0, 636(%rdi)
	movl	$0, 644(%rdi)
	movl	$0, 652(%rdi)
	movl	$0, 660(%rdi)
	movl	$0, 668(%rdi)
	movl	$0, 676(%rdi)
	movl	$0, 684(%rdi)
	movb	%dl, 528(%rax)
	movq	512(%rdi), %r13
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rax, 32(%rdi)
	testq	%r13, %r13
	je	.L709
.L706:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L706
.L709:
	movq	456(%r12), %r13
	leaq	440(%r12), %r15
	testq	%r13, %r13
	je	.L707
.L708:
	movq	24(%r13), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L712
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L707
.L713:
	movq	%rbx, %r13
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L712:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L713
.L707:
	movq	360(%r12), %rdi
	testq	%rdi, %rdi
	je	.L711
	movq	432(%r12), %rax
	movq	400(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L714
	.p2align 4,,10
	.p2align 3
.L715:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L715
	movq	360(%r12), %rdi
.L714:
	call	_ZdlPv@PLT
.L711:
	movq	272(%r12), %rdi
	testq	%rdi, %rdi
	je	.L716
	movq	344(%r12), %rax
	movq	312(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L717
	.p2align 4,,10
	.p2align 3
.L718:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L718
	movq	272(%r12), %rdi
.L717:
	call	_ZdlPv@PLT
.L716:
	movq	%r14, %rdi
	call	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal26NativeRegExpMacroAssemblerD2Ev@PLT
	.cfi_endproc
.LFE19615:
	.size	_ZN2v88internal23RegExpMacroAssemblerX64D2Ev, .-_ZN2v88internal23RegExpMacroAssemblerX64D2Ev
	.globl	_ZN2v88internal23RegExpMacroAssemblerX64D1Ev
	.set	_ZN2v88internal23RegExpMacroAssemblerX64D1Ev,_ZN2v88internal23RegExpMacroAssemblerX64D2Ev
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal23RegExpMacroAssemblerX6415kRegExpCodeSizeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal23RegExpMacroAssemblerX6415kRegExpCodeSizeE, @function
_GLOBAL__sub_I__ZN2v88internal23RegExpMacroAssemblerX6415kRegExpCodeSizeE:
.LFB23801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23801:
	.size	_GLOBAL__sub_I__ZN2v88internal23RegExpMacroAssemblerX6415kRegExpCodeSizeE, .-_GLOBAL__sub_I__ZN2v88internal23RegExpMacroAssemblerX6415kRegExpCodeSizeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal23RegExpMacroAssemblerX6415kRegExpCodeSizeE
	.weak	_ZTVN2v88internal9AssemblerE
	.section	.data.rel.ro.local._ZTVN2v88internal9AssemblerE,"awG",@progbits,_ZTVN2v88internal9AssemblerE,comdat
	.align 8
	.type	_ZTVN2v88internal9AssemblerE, @object
	.size	_ZTVN2v88internal9AssemblerE, 40
_ZTVN2v88internal9AssemblerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal9AssemblerD1Ev
	.quad	_ZN2v88internal9AssemblerD0Ev
	.quad	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.weak	_ZTVN2v88internal14MacroAssemblerE
	.section	.data.rel.ro._ZTVN2v88internal14MacroAssemblerE,"awG",@progbits,_ZTVN2v88internal14MacroAssemblerE,comdat
	.align 8
	.type	_ZTVN2v88internal14MacroAssemblerE, @object
	.size	_ZTVN2v88internal14MacroAssemblerE, 112
_ZTVN2v88internal14MacroAssemblerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14MacroAssemblerD1Ev
	.quad	_ZN2v88internal14MacroAssemblerD0Ev
	.quad	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.quad	_ZN2v88internal14TurboAssembler4JumpERKNS0_17ExternalReferenceE
	.quad	_ZN2v88internal14TurboAssembler18CallBuiltinByIndexENS0_8RegisterE
	.quad	_ZN2v88internal14TurboAssembler14CallCodeObjectENS0_8RegisterE
	.quad	_ZN2v88internal14TurboAssembler14JumpCodeObjectENS0_8RegisterE
	.quad	_ZN2v88internal14TurboAssembler19LoadCodeObjectEntryENS0_8RegisterES2_
	.quad	_ZN2v88internal14TurboAssembler22LoadFromConstantsTableENS0_8RegisterEi
	.quad	_ZN2v88internal14TurboAssembler22LoadRootRegisterOffsetENS0_8RegisterEl
	.quad	_ZN2v88internal14TurboAssembler16LoadRootRelativeENS0_8RegisterEi
	.quad	_ZN2v88internal14TurboAssembler8LoadRootENS0_8RegisterENS0_9RootIndexE
	.weak	_ZTVN2v88internal23RegExpMacroAssemblerX64E
	.section	.data.rel.ro._ZTVN2v88internal23RegExpMacroAssemblerX64E,"awG",@progbits,_ZTVN2v88internal23RegExpMacroAssemblerX64E,comdat
	.align 8
	.type	_ZTVN2v88internal23RegExpMacroAssemblerX64E, @object
	.size	_ZTVN2v88internal23RegExpMacroAssemblerX64E, 392
_ZTVN2v88internal23RegExpMacroAssemblerX64E:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23RegExpMacroAssemblerX64D1Ev
	.quad	_ZN2v88internal23RegExpMacroAssemblerX64D0Ev
	.quad	_ZN2v88internal20RegExpMacroAssembler21AbortedCodeGenerationEv
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6417stack_limit_slackEv
	.quad	_ZN2v88internal26NativeRegExpMacroAssembler16CanReadUnalignedEv
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6422AdvanceCurrentPositionEi
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6415AdvanceRegisterEii
	.quad	_ZN2v88internal23RegExpMacroAssemblerX649BacktrackEv
	.quad	_ZN2v88internal23RegExpMacroAssemblerX644BindEPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6414CheckCharacterEjPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6422CheckCharacterAfterAndEjjPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6416CheckCharacterGTEtPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6416CheckCharacterLTEtPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6415CheckGreedyLoopEPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6412CheckAtStartEiPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6415CheckNotAtStartEiPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6421CheckNotBackReferenceEibPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6431CheckNotBackReferenceIgnoreCaseEibbPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6417CheckNotCharacterEjPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6425CheckNotCharacterAfterAndEjjPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6430CheckNotCharacterAfterMinusAndEtttPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6421CheckCharacterInRangeEttPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6424CheckCharacterNotInRangeEttPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6415CheckBitInTableENS0_6HandleINS0_9ByteArrayEEEPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6413CheckPositionEiPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6426CheckSpecialCharacterClassEtPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpMacroAssemblerX644FailEv
	.quad	_ZN2v88internal23RegExpMacroAssemblerX647GetCodeENS0_6HandleINS0_6StringEEE
	.quad	_ZN2v88internal23RegExpMacroAssemblerX644GoToEPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6412IfRegisterGEEiiPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6412IfRegisterLTEiiPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6415IfRegisterEqPosEiPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6414ImplementationEv
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6424LoadCurrentCharacterImplEiPNS0_5LabelEbii
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6418PopCurrentPositionEv
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6411PopRegisterEi
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6413PushBacktrackEPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6419PushCurrentPositionEv
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6412PushRegisterEiNS0_20RegExpMacroAssembler14StackCheckFlagE
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6431ReadCurrentPositionFromRegisterEi
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6428ReadStackPointerFromRegisterEi
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6425SetCurrentPositionFromEndEi
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6411SetRegisterEii
	.quad	_ZN2v88internal23RegExpMacroAssemblerX647SucceedEv
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6430WriteCurrentPositionToRegisterEii
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6414ClearRegistersEii
	.quad	_ZN2v88internal23RegExpMacroAssemblerX6427WriteStackPointerToRegisterEi
	.globl	_ZN2v88internal23RegExpMacroAssemblerX6415kRegExpCodeSizeE
	.section	.rodata._ZN2v88internal23RegExpMacroAssemblerX6415kRegExpCodeSizeE,"a"
	.align 4
	.type	_ZN2v88internal23RegExpMacroAssemblerX6415kRegExpCodeSizeE, @object
	.size	_ZN2v88internal23RegExpMacroAssemblerX6415kRegExpCodeSizeE, 4
_ZN2v88internal23RegExpMacroAssemblerX6415kRegExpCodeSizeE:
	.long	1024
	.section	.rodata._ZN2v88internalL16kScratchRegisterE,"a"
	.align 4
	.type	_ZN2v88internalL16kScratchRegisterE, @object
	.size	_ZN2v88internalL16kScratchRegisterE, 4
_ZN2v88internalL16kScratchRegisterE:
	.long	10
	.section	.rodata._ZN2v88internalL3r11E,"a"
	.align 4
	.type	_ZN2v88internalL3r11E, @object
	.size	_ZN2v88internalL3r11E, 4
_ZN2v88internalL3r11E:
	.long	11
	.section	.rodata._ZN2v88internalL2r9E,"a"
	.align 4
	.type	_ZN2v88internalL2r9E, @object
	.size	_ZN2v88internalL2r9E, 4
_ZN2v88internalL2r9E:
	.long	9
	.section	.rodata._ZN2v88internalL2r8E,"a"
	.align 4
	.type	_ZN2v88internalL2r8E, @object
	.size	_ZN2v88internalL2r8E, 4
_ZN2v88internalL2r8E:
	.long	8
	.section	.rodata._ZN2v88internalL3rdiE,"a"
	.align 4
	.type	_ZN2v88internalL3rdiE, @object
	.size	_ZN2v88internalL3rdiE, 4
_ZN2v88internalL3rdiE:
	.long	7
	.section	.rodata._ZN2v88internalL3rsiE,"a"
	.align 4
	.type	_ZN2v88internalL3rsiE, @object
	.size	_ZN2v88internalL3rsiE, 4
_ZN2v88internalL3rsiE:
	.long	6
	.section	.rodata._ZN2v88internalL3rbpE,"a"
	.align 4
	.type	_ZN2v88internalL3rbpE, @object
	.size	_ZN2v88internalL3rbpE, 4
_ZN2v88internalL3rbpE:
	.long	5
	.section	.rodata._ZN2v88internalL3rspE,"a"
	.align 4
	.type	_ZN2v88internalL3rspE, @object
	.size	_ZN2v88internalL3rspE, 4
_ZN2v88internalL3rspE:
	.long	4
	.section	.rodata._ZN2v88internalL3rbxE,"a"
	.align 4
	.type	_ZN2v88internalL3rbxE, @object
	.size	_ZN2v88internalL3rbxE, 4
_ZN2v88internalL3rbxE:
	.long	3
	.section	.rodata._ZN2v88internalL3rdxE,"a"
	.align 4
	.type	_ZN2v88internalL3rdxE, @object
	.size	_ZN2v88internalL3rdxE, 4
_ZN2v88internalL3rdxE:
	.long	2
	.set	_ZN2v88internalL9arg_reg_3E,_ZN2v88internalL3rdxE
	.section	.rodata._ZN2v88internalL3rcxE,"a"
	.align 4
	.type	_ZN2v88internalL3rcxE, @object
	.size	_ZN2v88internalL3rcxE, 4
_ZN2v88internalL3rcxE:
	.long	1
	.set	_ZN2v88internalL9arg_reg_4E,_ZN2v88internalL3rcxE
	.section	.rodata._ZN2v88internalL3raxE,"a"
	.align 4
	.type	_ZN2v88internalL3raxE, @object
	.size	_ZN2v88internalL3raxE, 4
_ZN2v88internalL3raxE:
	.zero	4
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
