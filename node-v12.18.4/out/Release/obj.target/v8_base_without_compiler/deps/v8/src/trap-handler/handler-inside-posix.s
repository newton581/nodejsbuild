	.file	"handler-inside-posix.cc"
	.text
	.section	.text._ZN2v88internal12trap_handler15TryHandleSignalEiP9siginfo_tPv.part.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12trap_handler15TryHandleSignalEiP9siginfo_tPv.part.0, @function
_ZN2v88internal12trap_handler15TryHandleSignalEiP9siginfo_tPv.part.0:
.LFB3973:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-432(%rbp), %r12
	leaq	-304(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$424, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	sigemptyset@PLT
	movq	%r12, %rdi
	movl	$11, %esi
	call	sigaddset@PLT
	movdqa	-432(%rbp), %xmm0
	movq	%r13, %rdx
	movdqa	-416(%rbp), %xmm1
	movdqa	-400(%rbp), %xmm2
	movdqa	-384(%rbp), %xmm3
	leaq	-176(%rbp), %rsi
	movl	$1, %edi
	movdqa	-368(%rbp), %xmm4
	movdqa	-352(%rbp), %xmm5
	movaps	%xmm0, -176(%rbp)
	movdqa	-336(%rbp), %xmm6
	movdqa	-320(%rbp), %xmm7
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm6, -80(%rbp)
	movaps	%xmm7, -64(%rbp)
	call	pthread_sigmask@PLT
	movq	168(%rbx), %rdi
	leaq	-440(%rbp), %rsi
	movq	$0, -440(%rbp)
	call	_ZN2v88internal12trap_handler17TryFindLandingPadEmPm@PLT
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L7
	xorl	%edx, %edx
	movq	%r13, %rsi
	movl	$2, %edi
	call	pthread_sigmask@PLT
.L1:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L8
	addq	$424, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movq	-440(%rbp), %rax
	xorl	%edx, %edx
	movq	%r13, %rsi
	movl	$2, %edi
	movq	%rax, 168(%rbx)
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	$1, %fs:(%rax)
	call	pthread_sigmask@PLT
	jmp	.L1
.L8:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3973:
	.size	_ZN2v88internal12trap_handler15TryHandleSignalEiP9siginfo_tPv.part.0, .-_ZN2v88internal12trap_handler15TryHandleSignalEiP9siginfo_tPv.part.0
	.section	.text._ZN2v88internal12trap_handler23IsKernelGeneratedSignalEP9siginfo_t,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal12trap_handler23IsKernelGeneratedSignalEP9siginfo_t
	.type	_ZN2v88internal12trap_handler23IsKernelGeneratedSignalEP9siginfo_t, @function
_ZN2v88internal12trap_handler23IsKernelGeneratedSignalEP9siginfo_t:
.LFB3447:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	testl	%eax, %eax
	setg	%al
	ret
	.cfi_endproc
.LFE3447:
	.size	_ZN2v88internal12trap_handler23IsKernelGeneratedSignalEP9siginfo_t, .-_ZN2v88internal12trap_handler23IsKernelGeneratedSignalEP9siginfo_t
	.section	.text._ZN2v88internal12trap_handler15TryHandleSignalEiP9siginfo_tPv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal12trap_handler15TryHandleSignalEiP9siginfo_tPv
	.type	_ZN2v88internal12trap_handler15TryHandleSignalEiP9siginfo_tPv, @function
_ZN2v88internal12trap_handler15TryHandleSignalEiP9siginfo_tPv:
.LFB3454:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	%fs:(%rax), %ecx
	testl	%ecx, %ecx
	je	.L12
	movl	$0, %fs:(%rax)
	cmpl	$11, %edi
	je	.L14
.L12:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movl	8(%rsi), %eax
	testl	%eax, %eax
	jle	.L12
	movq	%rdx, %rdi
	jmp	_ZN2v88internal12trap_handler15TryHandleSignalEiP9siginfo_tPv.part.0
	.cfi_endproc
.LFE3454:
	.size	_ZN2v88internal12trap_handler15TryHandleSignalEiP9siginfo_tPv, .-_ZN2v88internal12trap_handler15TryHandleSignalEiP9siginfo_tPv
	.section	.text._ZN2v88internal12trap_handler12HandleSignalEiP9siginfo_tPv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal12trap_handler12HandleSignalEiP9siginfo_tPv
	.type	_ZN2v88internal12trap_handler12HandleSignalEiP9siginfo_tPv, @function
_ZN2v88internal12trap_handler12HandleSignalEiP9siginfo_tPv:
.LFB3455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%fs:(%rax), %ecx
	movq	%rsi, %rbx
	testl	%ecx, %ecx
	je	.L19
	movl	$0, %fs:(%rax)
	cmpl	$11, %edi
	je	.L25
.L19:
	call	_ZN2v88internal12trap_handler17RemoveTrapHandlerEv@PLT
	movl	8(%rbx), %edx
	testl	%edx, %edx
	jle	.L26
.L15:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movl	8(%rsi), %eax
	testl	%eax, %eax
	jle	.L19
	movq	%rdx, %rdi
	call	_ZN2v88internal12trap_handler15TryHandleSignalEiP9siginfo_tPv.part.0
	testb	%al, %al
	je	.L19
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L26:
	popq	%rbx
	movl	%r12d, %edi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	raise@PLT
	.cfi_endproc
.LFE3455:
	.size	_ZN2v88internal12trap_handler12HandleSignalEiP9siginfo_tPv, .-_ZN2v88internal12trap_handler12HandleSignalEiP9siginfo_tPv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
