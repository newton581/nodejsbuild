	.file	"function-compiler.cc"
	.text
	.section	.text._ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB2687:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2687:
	.size	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4581:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4581:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4582:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4582:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB4584:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4584:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZNK2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4View5startEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4View5startEv, @function
_ZNK2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4View5startEv:
.LFB20405:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE20405:
	.size	_ZNK2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4View5startEv, .-_ZNK2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4View5startEv
	.section	.text._ZNK2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4View4sizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4View4sizeEv, @function
_ZNK2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4View4sizeEv:
.LFB20406:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE20406:
	.size	_ZNK2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4View4sizeEv, .-_ZNK2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4View4sizeEv
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB23735:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE23735:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4ViewD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4ViewD2Ev, @function
_ZN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4ViewD2Ev:
.LFB20402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4ViewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	24(%rdi), %rbx
	movq	%rax, (%rdi)
	movq	16(%rbx), %r8
	cmpq	8(%rdi), %r8
	je	.L16
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	$0, 16(%rbx)
	testq	%r8, %r8
	je	.L11
	movq	%r8, %rdi
	call	_ZdaPv@PLT
.L11:
	movq	$0, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20402:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4ViewD2Ev, .-_ZN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4ViewD2Ev
	.set	_ZN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4ViewD1Ev,_ZN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4ViewD2Ev
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4View4GrowEi,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4View4GrowEi, @function
_ZN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4View4GrowEi:
.LFB20407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	24(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	0(%r13), %rax
	movslq	%edx, %rbx
	movq	16(%r13), %rdi
	movq	$0, 0(%r13)
	movq	%rax, 16(%r13)
	testq	%rdi, %rdi
	je	.L18
	call	_ZdaPv@PLT
.L18:
	movq	8(%r13), %rax
	movq	%rax, 24(%r13)
	xorl	%eax, %eax
	testq	%rbx, %rbx
	jne	.L30
.L19:
	movq	24(%r14), %r13
	movq	0(%r13), %rdi
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L20
	call	_ZdaPv@PLT
.L20:
	movq	%rbx, 8(%r13)
	movq	24(%r14), %rbx
	movl	$32, %edi
	movq	8(%rbx), %r13
	movq	(%rbx), %r14
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4ViewE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	%r14, 8(%rax)
	movq	%r13, 16(%rax)
	movq	%rbx, 24(%rax)
	popq	%rbx
	movq	%rax, (%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_Znam@PLT
	jmp	.L19
	.cfi_endproc
.LFE20407:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4View4GrowEi, .-_ZN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4View4GrowEi
	.section	.text._ZN2v88internal6Logger27is_listening_to_code_eventsEv,"axG",@progbits,_ZN2v88internal6Logger27is_listening_to_code_eventsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6Logger27is_listening_to_code_eventsEv
	.type	_ZN2v88internal6Logger27is_listening_to_code_eventsEv, @function
_ZN2v88internal6Logger27is_listening_to_code_eventsEv:
.LFB20289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L31
	cmpq	$0, 80(%rbx)
	setne	%al
.L31:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20289:
	.size	_ZN2v88internal6Logger27is_listening_to_code_eventsEv, .-_ZN2v88internal6Logger27is_listening_to_code_eventsEv
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_129RecordWasmHeapStubCompilationEPNS0_7IsolateENS0_6HandleINS0_4CodeEEEPKcz.constprop.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"NewArray"
.LC1:
	.string	"%s"
.LC2:
	.string	"0 < len"
.LC3:
	.string	"Check failed: %s."
.LC4:
	.string	"(location_) != nullptr"
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_129RecordWasmHeapStubCompilationEPNS0_7IsolateENS0_6HandleINS0_4CodeEEEPKcz.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_129RecordWasmHeapStubCompilationEPNS0_7IsolateENS0_6HandleINS0_4CodeEEEPKcz.constprop.0, @function
_ZN2v88internal4wasm12_GLOBAL__N_129RecordWasmHeapStubCompilationEPNS0_7IsolateENS0_6HandleINS0_4CodeEEEPKcz.constprop.0:
.LFB25069:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$248, %rsp
	movq	%rcx, -200(%rbp)
	movq	%r8, -192(%rbp)
	movq	%r9, -184(%rbp)
	testb	%al, %al
	je	.L35
	movaps	%xmm0, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm6, -80(%rbp)
	movaps	%xmm7, -64(%rbp)
.L35:
	movq	%fs:40, %rax
	movq	%rax, -232(%rbp)
	xorl	%eax, %eax
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L47
.L36:
	leaq	16(%rbp), %rax
	leaq	-256(%rbp), %rcx
	movq	%r12, %rdi
	movl	$128, %esi
	movq	%rax, -248(%rbp)
	leaq	.LC1(%rip), %rdx
	leaq	-224(%rbp), %rax
	movq	$128, -280(%rbp)
	movl	$24, -256(%rbp)
	movl	$48, -252(%rbp)
	movq	%rax, -240(%rbp)
	movq	%r12, -288(%rbp)
	call	_ZN2v88internal9VSNPrintFENS0_6VectorIcEEPKcP13__va_list_tag@PLT
	testl	%eax, %eax
	jle	.L48
	movq	%r12, %rdi
	call	strlen@PLT
	xorl	%edx, %edx
	movq	%rbx, %rdi
	leaq	-272(%rbp), %rsi
	movq	%r12, -272(%rbp)
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L49
	movq	41488(%rbx), %rbx
	movq	0(%r13), %r15
	movq	(%rax), %r14
	leaq	56(%rbx), %r13
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L39
	.p2align 4,,10
	.p2align 3
.L40:
	movq	8(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movl	$18, %esi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L40
.L39:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	%r12, %rdi
	call	_ZdaPv@PLT
	movq	-232(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L50
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L49:
	leaq	.LC4(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L47:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L36
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L50:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25069:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_129RecordWasmHeapStubCompilationEPNS0_7IsolateENS0_6HandleINS0_4CodeEEEPKcz.constprop.0, .-_ZN2v88internal4wasm12_GLOBAL__N_129RecordWasmHeapStubCompilationEPNS0_7IsolateENS0_6HandleINS0_4CodeEEEPKcz.constprop.0
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4ViewD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4ViewD0Ev, @function
_ZN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4ViewD0Ev:
.LFB20404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4ViewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	24(%rdi), %rbx
	movq	%rax, (%rdi)
	movq	16(%rbx), %rdi
	cmpq	%rdi, 8(%r12)
	je	.L58
.L52:
	popq	%rbx
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movq	$0, 16(%rbx)
	testq	%rdi, %rdi
	je	.L53
	call	_ZdaPv@PLT
.L53:
	movq	$0, 24(%rbx)
	jmp	.L52
	.cfi_endproc
.LFE20404:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4ViewD0Ev, .-_ZN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4ViewD0Ev
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB19898:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L65
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L68
.L59:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L59
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE19898:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.text._ZN2v88internal4wasm21WasmInstructionBufferD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm21WasmInstructionBufferD2Ev
	.type	_ZN2v88internal4wasm21WasmInstructionBufferD2Ev, @function
_ZN2v88internal4wasm21WasmInstructionBufferD2Ev:
.LFB20418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L70
	call	_ZdaPv@PLT
.L70:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L69
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20418:
	.size	_ZN2v88internal4wasm21WasmInstructionBufferD2Ev, .-_ZN2v88internal4wasm21WasmInstructionBufferD2Ev
	.globl	_ZN2v88internal4wasm21WasmInstructionBufferD1Ev
	.set	_ZN2v88internal4wasm21WasmInstructionBufferD1Ev,_ZN2v88internal4wasm21WasmInstructionBufferD2Ev
	.section	.text._ZN2v88internal4wasm21WasmInstructionBuffer10CreateViewEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm21WasmInstructionBuffer10CreateViewEv
	.type	_ZN2v88internal4wasm21WasmInstructionBuffer10CreateViewEv, @function
_ZN2v88internal4wasm21WasmInstructionBuffer10CreateViewEv:
.LFB20420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	8(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movl	$32, %edi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4ViewE(%rip), %rdx
	movq	%rdx, (%rax)
	movq	%r14, 8(%rax)
	movq	%r13, 16(%rax)
	movq	%rbx, 24(%rax)
	popq	%rbx
	movq	%rax, (%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20420:
	.size	_ZN2v88internal4wasm21WasmInstructionBuffer10CreateViewEv, .-_ZN2v88internal4wasm21WasmInstructionBuffer10CreateViewEv
	.section	.text._ZN2v88internal4wasm21WasmInstructionBuffer13ReleaseBufferEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm21WasmInstructionBuffer13ReleaseBufferEv
	.type	_ZN2v88internal4wasm21WasmInstructionBuffer13ReleaseBufferEv, @function
_ZN2v88internal4wasm21WasmInstructionBuffer13ReleaseBufferEv:
.LFB20421:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rdx
	movq	$0, 8(%rsi)
	movq	%rdi, %rax
	movq	$0, (%rsi)
	movq	%rdx, (%rdi)
	ret
	.cfi_endproc
.LFE20421:
	.size	_ZN2v88internal4wasm21WasmInstructionBuffer13ReleaseBufferEv, .-_ZN2v88internal4wasm21WasmInstructionBuffer13ReleaseBufferEv
	.section	.text._ZN2v88internal4wasm21WasmInstructionBuffer3NewEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm21WasmInstructionBuffer3NewEv
	.type	_ZN2v88internal4wasm21WasmInstructionBuffer3NewEv, @function
_ZN2v88internal4wasm21WasmInstructionBuffer3NewEv:
.LFB20424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$32, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	call	_Znwm@PLT
	movl	$4096, %edi
	movq	$0, (%rax)
	movq	%rax, %rbx
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	call	_Znam@PLT
	movq	%rbx, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movq	$4096, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20424:
	.size	_ZN2v88internal4wasm21WasmInstructionBuffer3NewEv, .-_ZN2v88internal4wasm21WasmInstructionBuffer3NewEv
	.section	.text._ZN2v88internal4wasm19WasmCompilationUnit23GetDefaultExecutionTierEPKNS1_10WasmModuleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmCompilationUnit23GetDefaultExecutionTierEPKNS1_10WasmModuleE
	.type	_ZN2v88internal4wasm19WasmCompilationUnit23GetDefaultExecutionTierEPKNS1_10WasmModuleE, @function
_ZN2v88internal4wasm19WasmCompilationUnit23GetDefaultExecutionTierEPKNS1_10WasmModuleE:
.LFB20428:
	.cfi_startproc
	endbr64
	cmpb	$0, 392(%rdi)
	movl	$3, %eax
	jne	.L81
	cmpb	$0, _ZN2v88internal23FLAG_wasm_interpret_allE(%rip)
	movl	$1, %eax
	jne	.L81
	cmpb	$0, _ZN2v88internal12FLAG_liftoffE(%rip)
	sete	%al
	addl	$2, %eax
.L81:
	ret
	.cfi_endproc
.LFE20428:
	.size	_ZN2v88internal4wasm19WasmCompilationUnit23GetDefaultExecutionTierEPKNS1_10WasmModuleE, .-_ZN2v88internal4wasm19WasmCompilationUnit23GetDefaultExecutionTierEPKNS1_10WasmModuleE
	.section	.text._ZN2v88internal4wasm19WasmCompilationUnit31ExecuteImportWrapperCompilationEPNS1_10WasmEngineEPNS1_14CompilationEnvE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmCompilationUnit31ExecuteImportWrapperCompilationEPNS1_10WasmEngineEPNS1_14CompilationEnvE
	.type	_ZN2v88internal4wasm19WasmCompilationUnit31ExecuteImportWrapperCompilationEPNS1_10WasmEngineEPNS1_14CompilationEnvE, @function
_ZN2v88internal4wasm19WasmCompilationUnit31ExecuteImportWrapperCompilationEPNS1_10WasmEngineEPNS1_14CompilationEnvE:
.LFB20472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rcx), %rax
	movslq	(%r8), %rcx
	cmpb	$0, 392(%rax)
	setne	%r9b
	salq	$5, %rcx
	addq	136(%rax), %rcx
	movq	(%rcx), %r8
	movl	$5, %ecx
	call	_ZN2v88internal8compiler28CompileWasmImportCallWrapperEPNS0_4wasm10WasmEngineEPNS2_14CompilationEnvENS1_18WasmImportCallKindEPNS0_9SignatureINS2_9ValueTypeEEEb@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L89
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L89:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20472:
	.size	_ZN2v88internal4wasm19WasmCompilationUnit31ExecuteImportWrapperCompilationEPNS1_10WasmEngineEPNS1_14CompilationEnvE, .-_ZN2v88internal4wasm19WasmCompilationUnit31ExecuteImportWrapperCompilationEPNS1_10WasmEngineEPNS1_14CompilationEnvE
	.section	.rodata._ZN2v88internal4wasm19WasmCompilationUnit26ExecuteFunctionCompilationEPNS1_10WasmEngineEPNS1_14CompilationEnvERKSt10shared_ptrINS1_16WireBytesStorageEEPNS0_8CountersEPNS1_12WasmFeaturesE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"unreachable code"
	.section	.text._ZN2v88internal4wasm19WasmCompilationUnit26ExecuteFunctionCompilationEPNS1_10WasmEngineEPNS1_14CompilationEnvERKSt10shared_ptrINS1_16WireBytesStorageEEPNS0_8CountersEPNS1_12WasmFeaturesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmCompilationUnit26ExecuteFunctionCompilationEPNS1_10WasmEngineEPNS1_14CompilationEnvERKSt10shared_ptrINS1_16WireBytesStorageEEPNS0_8CountersEPNS1_12WasmFeaturesE
	.type	_ZN2v88internal4wasm19WasmCompilationUnit26ExecuteFunctionCompilationEPNS1_10WasmEngineEPNS1_14CompilationEnvERKSt10shared_ptrINS1_16WireBytesStorageEEPNS0_8CountersEPNS1_12WasmFeaturesE, @function
_ZN2v88internal4wasm19WasmCompilationUnit26ExecuteFunctionCompilationEPNS1_10WasmEngineEPNS1_14CompilationEnvERKSt10shared_ptrINS1_16WireBytesStorageEEPNS0_8CountersEPNS1_12WasmFeaturesE:
.LFB20473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$248, %rsp
	movq	16(%rbp), %rax
	movslq	(%rsi), %r14
	movq	%rdx, -264(%rbp)
	movq	(%r8), %rdi
	movq	%rax, -272(%rbp)
	salq	$5, %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rcx), %rax
	addq	136(%rax), %r14
	movq	(%rdi), %rax
	movq	16(%r14), %rsi
	call	*16(%rax)
	movq	(%r14), %rcx
	leaq	1208(%rbx), %rdi
	movq	%rdx, %rsi
	movl	16(%r14), %edx
	movq	%rax, %xmm0
	movq	%rcx, -224(%rbp)
	movl	%edx, -216(%rbp)
	leaq	(%rax,%rsi), %rdx
	movq	0(%r13), %rax
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	cmpb	$0, 392(%rax)
	movaps	%xmm0, -208(%rbp)
	je	.L92
	leaq	1168(%rbx), %rdi
.L92:
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movq	0(%r13), %rax
	leaq	4680(%rbx), %rdi
	cmpb	$0, 392(%rax)
	jne	.L94
	leaq	4728(%rbx), %rdi
.L94:
	leaq	-256(%rbp), %r14
	xorl	%edx, %edx
	movq	%rdi, -248(%rbp)
	movq	$0, -256(%rbp)
	movq	%r14, %rsi
	movq	$0, -240(%rbp)
	call	_ZN2v88internal14TimedHistogram5StartEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	pxor	%xmm0, %xmm0
	movq	$0, (%r15)
	movups	%xmm0, 8(%r15)
	movups	%xmm0, 24(%r15)
	movups	%xmm0, 40(%r15)
	pxor	%xmm0, %xmm0
	movq	$0, 56(%r15)
	movl	$0, 64(%r15)
	movq	$0, 88(%r15)
	movq	$0, 96(%r15)
	movq	$0, 104(%r15)
	movq	$0, 112(%r15)
	movq	$0, 120(%r15)
	movl	$-1, 128(%r15)
	movb	$0, 134(%r15)
	movups	%xmm0, 72(%r15)
	movzbl	4(%r12), %eax
	cmpb	$2, %al
	je	.L95
	jg	.L96
	testb	%al, %al
	je	.L97
	cmpb	$1, %al
	jne	.L99
	subq	$8, %rsp
	pushq	-272(%rbp)
	movq	%rbx, %r9
	movq	%r13, %rdx
	movl	(%r12), %r8d
	leaq	-192(%rbp), %rdi
	leaq	-224(%rbp), %rcx
	movq	-264(%rbp), %rsi
	call	_ZN2v88internal8compiler34ExecuteInterpreterEntryCompilationEPNS0_4wasm10WasmEngineEPNS2_14CompilationEnvERKNS2_12FunctionBodyEiPNS0_8CountersEPNS2_12WasmFeaturesE@PLT
	movdqa	-192(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm2
	movdqa	-144(%rbp), %xmm3
	movups	%xmm7, (%r15)
	movdqa	-176(%rbp), %xmm7
	movups	%xmm2, 32(%r15)
	movups	%xmm7, 16(%r15)
	movups	%xmm3, 48(%r15)
	movups	%xmm4, 64(%r15)
.L186:
	movq	-112(%rbp), %rax
	movq	80(%r15), %rdi
	movq	$0, -112(%rbp)
	movq	%rax, 80(%r15)
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L114
	call	_ZdaPv@PLT
.L114:
	movq	-104(%rbp), %rax
	movq	96(%r15), %rdi
	movq	%rax, 88(%r15)
	movq	-96(%rbp), %rax
	movq	$0, -96(%rbp)
	movq	%rax, 96(%r15)
	testq	%rdi, %rdi
	je	.L115
	call	_ZdaPv@PLT
.L115:
	movq	-88(%rbp), %rax
	movq	112(%r15), %rdi
	movq	%rax, 104(%r15)
	movq	-80(%rbp), %rax
	movq	$0, -80(%rbp)
	movq	%rax, 112(%r15)
	testq	%rdi, %rdi
	je	.L110
	call	_ZdaPv@PLT
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rdi
	movq	%rax, 120(%r15)
	movl	-64(%rbp), %eax
	movl	%eax, 128(%r15)
	movzwl	-60(%rbp), %eax
	movw	%ax, 132(%r15)
	movzbl	-58(%rbp), %eax
	movb	%al, 134(%r15)
	testq	%rdi, %rdi
	je	.L117
	call	_ZdaPv@PLT
.L117:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L118
	call	_ZdaPv@PLT
.L118:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L99
	call	_ZdaPv@PLT
.L99:
	movq	-240(%rbp), %rdx
	movq	-248(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L187
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	cmpb	$3, %al
	jne	.L99
	movl	(%r12), %r8d
.L173:
	leaq	-192(%rbp), %rax
	movq	%rax, -280(%rbp)
	leaq	-224(%rbp), %rax
	movq	%rax, -288(%rbp)
	movq	%rax, %rcx
.L100:
	subq	$8, %rsp
	pushq	-272(%rbp)
	movq	%rbx, %r9
	movq	%r13, %rdx
	movq	-264(%rbp), %rsi
	movq	-280(%rbp), %rdi
	call	_ZN2v88internal8compiler30ExecuteTurbofanWasmCompilationEPNS0_4wasm10WasmEngineEPNS2_14CompilationEnvERKNS2_12FunctionBodyEiPNS0_8CountersEPNS2_12WasmFeaturesE@PLT
	movdqa	-192(%rbp), %xmm2
	movdqa	-176(%rbp), %xmm3
	movdqa	-160(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm6
	movups	%xmm2, (%r15)
	movups	%xmm3, 16(%r15)
	movups	%xmm4, 32(%r15)
	movups	%xmm5, 48(%r15)
	movups	%xmm6, 64(%r15)
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L110:
	movq	-72(%rbp), %rax
	movq	%rax, 120(%r15)
	movl	-64(%rbp), %eax
	movl	%eax, 128(%r15)
	movzwl	-60(%rbp), %eax
	movw	%ax, 132(%r15)
	movzbl	-58(%rbp), %eax
	movb	%al, 134(%r15)
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L95:
	movl	_ZN2v88internal31FLAG_wasm_tier_mask_for_testingE(%rip), %eax
	movl	(%r12), %r8d
	testl	%eax, %eax
	jne	.L188
.L101:
	subq	$8, %rsp
	pushq	-272(%rbp)
	movq	-264(%rbp), %rax
	leaq	-192(%rbp), %rdi
	leaq	-224(%rbp), %rcx
	movq	%rbx, %r9
	movq	%r13, %rdx
	movq	%rdi, -280(%rbp)
	leaq	408(%rax), %rsi
	movq	%rcx, -288(%rbp)
	call	_ZN2v88internal4wasm25ExecuteLiftoffCompilationEPNS0_19AccountingAllocatorEPNS1_14CompilationEnvERKNS1_12FunctionBodyEiPNS0_8CountersEPNS1_12WasmFeaturesE@PLT
	movdqa	-192(%rbp), %xmm5
	popq	%rcx
	movdqa	-176(%rbp), %xmm6
	movq	-112(%rbp), %rax
	movq	$0, -112(%rbp)
	movdqa	-160(%rbp), %xmm7
	movq	80(%r15), %rdi
	movups	%xmm5, (%r15)
	movups	%xmm6, 16(%r15)
	movdqa	-144(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm6
	movq	%rax, 80(%r15)
	popq	%rsi
	movups	%xmm7, 32(%r15)
	movups	%xmm5, 48(%r15)
	movups	%xmm6, 64(%r15)
	testq	%rdi, %rdi
	je	.L102
	call	_ZdaPv@PLT
.L102:
	movq	-104(%rbp), %rax
	movq	96(%r15), %rdi
	movq	%rax, 88(%r15)
	movq	-96(%rbp), %rax
	movq	$0, -96(%rbp)
	movq	%rax, 96(%r15)
	testq	%rdi, %rdi
	je	.L103
	call	_ZdaPv@PLT
.L103:
	movq	-88(%rbp), %rax
	movq	112(%r15), %rdi
	movq	%rax, 104(%r15)
	movq	-80(%rbp), %rax
	movq	$0, -80(%rbp)
	movq	%rax, 112(%r15)
	testq	%rdi, %rdi
	je	.L104
	call	_ZdaPv@PLT
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rdi
	movq	%rax, 120(%r15)
	movl	-64(%rbp), %eax
	movl	%eax, 128(%r15)
	movzwl	-60(%rbp), %eax
	movw	%ax, 132(%r15)
	movzbl	-58(%rbp), %eax
	movb	%al, 134(%r15)
	testq	%rdi, %rdi
	je	.L105
	call	_ZdaPv@PLT
.L105:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L106
	call	_ZdaPv@PLT
.L106:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L107
	call	_ZdaPv@PLT
.L107:
	cmpq	$0, (%r15)
	jne	.L99
	movl	(%r12), %r8d
	movq	-288(%rbp), %rcx
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L104:
	movq	-72(%rbp), %rax
	movq	%rax, 120(%r15)
	movl	-64(%rbp), %eax
	movl	%eax, 128(%r15)
	movzwl	-60(%rbp), %eax
	movw	%ax, 132(%r15)
	movzbl	-58(%rbp), %eax
	movb	%al, 134(%r15)
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L188:
	cmpl	$31, %r8d
	jg	.L101
	btl	%r8d, %eax
	jnc	.L101
	jmp	.L173
.L97:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L187:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20473:
	.size	_ZN2v88internal4wasm19WasmCompilationUnit26ExecuteFunctionCompilationEPNS1_10WasmEngineEPNS1_14CompilationEnvERKSt10shared_ptrINS1_16WireBytesStorageEEPNS0_8CountersEPNS1_12WasmFeaturesE, .-_ZN2v88internal4wasm19WasmCompilationUnit26ExecuteFunctionCompilationEPNS1_10WasmEngineEPNS1_14CompilationEnvERKSt10shared_ptrINS1_16WireBytesStorageEEPNS0_8CountersEPNS1_12WasmFeaturesE
	.section	.text._ZN2v88internal4wasm19WasmCompilationUnit18ExecuteCompilationEPNS1_10WasmEngineEPNS1_14CompilationEnvERKSt10shared_ptrINS1_16WireBytesStorageEEPNS0_8CountersEPNS1_12WasmFeaturesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmCompilationUnit18ExecuteCompilationEPNS1_10WasmEngineEPNS1_14CompilationEnvERKSt10shared_ptrINS1_16WireBytesStorageEEPNS0_8CountersEPNS1_12WasmFeaturesE
	.type	_ZN2v88internal4wasm19WasmCompilationUnit18ExecuteCompilationEPNS1_10WasmEngineEPNS1_14CompilationEnvERKSt10shared_ptrINS1_16WireBytesStorageEEPNS0_8CountersEPNS1_12WasmFeaturesE, @function
_ZN2v88internal4wasm19WasmCompilationUnit18ExecuteCompilationEPNS1_10WasmEngineEPNS1_14CompilationEnvERKSt10shared_ptrINS1_16WireBytesStorageEEPNS0_8CountersEPNS1_12WasmFeaturesE:
.LFB20429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	subq	$152, %rsp
	movq	16(%rbp), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, 8(%r12)
	movups	%xmm0, 24(%r12)
	movups	%xmm0, 40(%r12)
	pxor	%xmm0, %xmm0
	movq	$0, (%r12)
	movq	$0, 56(%r12)
	movl	$0, 64(%r12)
	movq	$0, 88(%r12)
	movq	$0, 96(%r12)
	movq	$0, 104(%r12)
	movq	$0, 112(%r12)
	movq	$0, 120(%r12)
	movl	$-1, 128(%r12)
	movb	$0, 134(%r12)
	movups	%xmm0, 72(%r12)
	movq	(%rcx), %rcx
	movslq	(%rbx), %rax
	cmpl	60(%rcx), %eax
	jge	.L190
	xorl	%r9d, %r9d
	cmpb	$0, 392(%rcx)
	leaq	-176(%rbp), %rdi
	setne	%r9b
	salq	$5, %rax
	addq	136(%rcx), %rax
	movl	$5, %ecx
	movq	(%rax), %r8
	call	_ZN2v88internal8compiler28CompileWasmImportCallWrapperEPNS0_4wasm10WasmEngineEPNS2_14CompilationEnvENS1_18WasmImportCallKindEPNS0_9SignatureINS2_9ValueTypeEEEb@PLT
	movq	-96(%rbp), %rax
	movdqa	-176(%rbp), %xmm1
	movq	$0, -96(%rbp)
	movdqa	-160(%rbp), %xmm2
	movdqa	-144(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm5
	movups	%xmm1, (%r12)
	movq	80(%r12), %rdi
	movups	%xmm2, 16(%r12)
	movq	%rax, 80(%r12)
	movups	%xmm3, 32(%r12)
	movups	%xmm4, 48(%r12)
	movups	%xmm5, 64(%r12)
	testq	%rdi, %rdi
	je	.L198
.L248:
	call	_ZdaPv@PLT
.L198:
	movq	-88(%rbp), %rax
	movq	96(%r12), %rdi
	movq	%rax, 88(%r12)
	movq	-80(%rbp), %rax
	movq	$0, -80(%rbp)
	movq	%rax, 96(%r12)
	testq	%rdi, %rdi
	je	.L199
	call	_ZdaPv@PLT
.L199:
	movq	-72(%rbp), %rax
	movq	112(%r12), %rdi
	movq	%rax, 104(%r12)
	movq	-64(%rbp), %rax
	movq	$0, -64(%rbp)
	movq	%rax, 112(%r12)
	testq	%rdi, %rdi
	je	.L200
	call	_ZdaPv@PLT
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %rdi
	movq	%rax, 120(%r12)
	movl	-48(%rbp), %eax
	movl	%eax, 128(%r12)
	movzwl	-44(%rbp), %eax
	movw	%ax, 132(%r12)
	movzbl	-42(%rbp), %eax
	movb	%al, 134(%r12)
	testq	%rdi, %rdi
	je	.L201
	call	_ZdaPv@PLT
.L201:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L202
	call	_ZdaPv@PLT
.L202:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L197
	call	_ZdaPv@PLT
.L197:
	cmpq	$0, (%r12)
	je	.L204
	movl	12(%r12), %esi
	leaq	8136(%r13), %rdi
	call	_ZN2v88internal22StatsCounterThreadSafe9IncrementEi@PLT
	movl	52(%r12), %esi
	leaq	8200(%r13), %rdi
	call	_ZN2v88internal22StatsCounterThreadSafe9IncrementEi@PLT
.L204:
	movl	(%rbx), %eax
	movl	%eax, 128(%r12)
	movzbl	4(%rbx), %eax
	movb	%al, 132(%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L249
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	subq	$8, %rsp
	leaq	-176(%rbp), %r10
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	pushq	%rdi
	movq	%rbx, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal4wasm19WasmCompilationUnit26ExecuteFunctionCompilationEPNS1_10WasmEngineEPNS1_14CompilationEnvERKSt10shared_ptrINS1_16WireBytesStorageEEPNS0_8CountersEPNS1_12WasmFeaturesE
	movq	-96(%rbp), %rax
	movdqa	-176(%rbp), %xmm6
	movq	$0, -96(%rbp)
	movdqa	-160(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm1
	movdqa	-128(%rbp), %xmm2
	movdqa	-112(%rbp), %xmm3
	movups	%xmm6, (%r12)
	movq	80(%r12), %rdi
	movups	%xmm7, 16(%r12)
	movq	%rax, 80(%r12)
	popq	%rax
	movups	%xmm1, 32(%r12)
	popq	%rdx
	movups	%xmm2, 48(%r12)
	movups	%xmm3, 64(%r12)
	testq	%rdi, %rdi
	jne	.L248
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L200:
	movq	-56(%rbp), %rax
	movq	%rax, 120(%r12)
	movl	-48(%rbp), %eax
	movl	%eax, 128(%r12)
	movzwl	-44(%rbp), %eax
	movw	%ax, 132(%r12)
	movzbl	-42(%rbp), %eax
	movb	%al, 134(%r12)
	jmp	.L201
.L249:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20429:
	.size	_ZN2v88internal4wasm19WasmCompilationUnit18ExecuteCompilationEPNS1_10WasmEngineEPNS1_14CompilationEnvERKSt10shared_ptrINS1_16WireBytesStorageEEPNS0_8CountersEPNS1_12WasmFeaturesE, .-_ZN2v88internal4wasm19WasmCompilationUnit18ExecuteCompilationEPNS1_10WasmEngineEPNS1_14CompilationEnvERKSt10shared_ptrINS1_16WireBytesStorageEEPNS0_8CountersEPNS1_12WasmFeaturesE
	.section	.text._ZN2v88internal4wasm19WasmCompilationUnit19CompileWasmFunctionEPNS0_7IsolateEPNS1_12NativeModuleEPNS1_12WasmFeaturesEPKNS1_12WasmFunctionENS1_13ExecutionTierE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmCompilationUnit19CompileWasmFunctionEPNS0_7IsolateEPNS1_12NativeModuleEPNS1_12WasmFeaturesEPKNS1_12WasmFunctionENS1_13ExecutionTierE
	.type	_ZN2v88internal4wasm19WasmCompilationUnit19CompileWasmFunctionEPNS0_7IsolateEPNS1_12NativeModuleEPNS1_12WasmFeaturesEPKNS1_12WasmFunctionENS1_13ExecutionTierE, @function
_ZN2v88internal4wasm19WasmCompilationUnit19CompileWasmFunctionEPNS0_7IsolateEPNS1_12NativeModuleEPNS1_12WasmFeaturesEPKNS1_12WasmFunctionENS1_13ExecutionTierE:
.LFB20476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$440, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	8(%rcx), %eax
	movb	%r8b, -452(%rbp)
	movl	%eax, -456(%rbp)
	call	_ZNK2v88internal4wasm12NativeModule20CreateCompilationEnvEv@PLT
	movq	40960(%rbx), %r9
	movq	520(%r12), %rsi
	movq	%r14, %rdi
	movq	%r9, -472(%rbp)
	call	_ZNK2v88internal4wasm16CompilationState19GetWireBytesStorageEv@PLT
	subq	$8, %rsp
	movq	%r14, %r8
	movq	45752(%rbx), %rdx
	pushq	%r13
	movq	-472(%rbp), %r9
	movq	%r15, %rcx
	leaq	-336(%rbp), %rdi
	leaq	-456(%rbp), %rsi
	call	_ZN2v88internal4wasm19WasmCompilationUnit18ExecuteCompilationEPNS1_10WasmEngineEPNS1_14CompilationEnvERKSt10shared_ptrINS1_16WireBytesStorageEEPNS0_8CountersEPNS1_12WasmFeaturesE
	movq	-184(%rbp), %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L252
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L253
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L288
	.p2align 4,,10
	.p2align 3
.L252:
	cmpq	$0, -336(%rbp)
	je	.L260
	leaq	-400(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeC1Ev@PLT
	movq	-256(%rbp), %rax
	movq	%r12, %rdi
	movdqa	-336(%rbp), %xmm0
	movdqa	-320(%rbp), %xmm1
	movdqa	-304(%rbp), %xmm2
	movq	%r14, %rsi
	movq	$0, -256(%rbp)
	movq	%rax, -112(%rbp)
	movq	-248(%rbp), %rax
	movdqa	-288(%rbp), %xmm3
	movdqa	-272(%rbp), %xmm4
	movaps	%xmm0, -192(%rbp)
	movq	%rax, -104(%rbp)
	movq	-240(%rbp), %rax
	movaps	%xmm1, -176(%rbp)
	movq	%rax, -96(%rbp)
	movq	-232(%rbp), %rax
	movq	$0, -240(%rbp)
	movq	%rax, -88(%rbp)
	movq	-224(%rbp), %rax
	movaps	%xmm2, -160(%rbp)
	movq	%rax, -80(%rbp)
	movq	-216(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -72(%rbp)
	movl	-208(%rbp), %eax
	movaps	%xmm3, -144(%rbp)
	movl	%eax, -64(%rbp)
	movzwl	-204(%rbp), %eax
	movaps	%xmm4, -128(%rbp)
	movw	%ax, -60(%rbp)
	movzbl	-202(%rbp), %eax
	movb	%al, -58(%rbp)
	call	_ZN2v88internal4wasm12NativeModule15AddCompiledCodeENS1_21WasmCompilationResultE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L261
	call	_ZdaPv@PLT
.L261:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L262
	call	_ZdaPv@PLT
.L262:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L263
	call	_ZdaPv@PLT
.L263:
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
.L264:
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L265
	call	_ZdaPv@PLT
.L265:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L266
	call	_ZdaPv@PLT
.L266:
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L250
	call	_ZdaPv@PLT
.L250:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L289
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L252
.L288:
	movq	0(%r13), %rax
	leaq	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L290
.L256:
	testq	%rbx, %rbx
	je	.L257
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L258:
	cmpl	$1, %eax
	jne	.L252
	movq	0(%r13), %rax
	leaq	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L259
	call	*8(%rax)
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L260:
	movq	520(%r12), %rdi
	call	_ZN2v88internal4wasm16CompilationState8SetErrorEv@PLT
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L257:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L259:
	call	*%rdx
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L290:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L256
.L289:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20476:
	.size	_ZN2v88internal4wasm19WasmCompilationUnit19CompileWasmFunctionEPNS0_7IsolateEPNS1_12NativeModuleEPNS1_12WasmFeaturesEPKNS1_12WasmFunctionENS1_13ExecutionTierE, .-_ZN2v88internal4wasm19WasmCompilationUnit19CompileWasmFunctionEPNS0_7IsolateEPNS1_12NativeModuleEPNS1_12WasmFeaturesEPKNS1_12WasmFunctionENS1_13ExecutionTierE
	.section	.text._ZN2v88internal4wasm30JSToWasmWrapperCompilationUnitC2EPNS0_7IsolateEPNS1_10WasmEngineEPNS0_9SignatureINS1_9ValueTypeEEEbRKNS1_12WasmFeaturesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnitC2EPNS0_7IsolateEPNS1_10WasmEngineEPNS0_9SignatureINS1_9ValueTypeEEEbRKNS1_12WasmFeaturesE
	.type	_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnitC2EPNS0_7IsolateEPNS1_10WasmEngineEPNS0_9SignatureINS1_9ValueTypeEEEbRKNS1_12WasmFeaturesE, @function
_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnitC2EPNS0_7IsolateEPNS1_10WasmEngineEPNS0_9SignatureINS1_9ValueTypeEEEbRKNS1_12WasmFeaturesE:
.LFB20485:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movb	%r8b, -16(%rdi)
	movzbl	%r8b, %r8d
	movq	%rcx, -8(%rdi)
	call	_ZN2v88internal8compiler25NewJSToWasmCompilationJobEPNS0_7IsolateEPNS0_4wasm10WasmEngineEPNS0_9SignatureINS4_9ValueTypeEEEbRKNS4_12WasmFeaturesE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L294
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L294:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20485:
	.size	_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnitC2EPNS0_7IsolateEPNS1_10WasmEngineEPNS0_9SignatureINS1_9ValueTypeEEEbRKNS1_12WasmFeaturesE, .-_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnitC2EPNS0_7IsolateEPNS1_10WasmEngineEPNS0_9SignatureINS1_9ValueTypeEEEbRKNS1_12WasmFeaturesE
	.globl	_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnitC1EPNS0_7IsolateEPNS1_10WasmEngineEPNS0_9SignatureINS1_9ValueTypeEEEbRKNS1_12WasmFeaturesE
	.set	_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnitC1EPNS0_7IsolateEPNS1_10WasmEngineEPNS0_9SignatureINS1_9ValueTypeEEEbRKNS1_12WasmFeaturesE,_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnitC2EPNS0_7IsolateEPNS1_10WasmEngineEPNS0_9SignatureINS1_9ValueTypeEEEbRKNS1_12WasmFeaturesE
	.section	.text._ZN2v88internal4wasm30JSToWasmWrapperCompilationUnitD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnitD2Ev
	.type	_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnitD2Ev, @function
_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnitD2Ev:
.LFB20488:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L295
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L295:
	ret
	.cfi_endproc
.LFE20488:
	.size	_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnitD2Ev, .-_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnitD2Ev
	.globl	_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnitD1Ev
	.set	_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnitD1Ev,_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnitD2Ev
	.section	.rodata._ZN2v88internal4wasm30JSToWasmWrapperCompilationUnit7ExecuteEv.str1.1,"aMS",@progbits,1
.LC6:
	.string	"disabled-by-default-v8.wasm"
.LC7:
	.string	"CompileJSToWasmWrapper"
	.section	.rodata._ZN2v88internal4wasm30JSToWasmWrapperCompilationUnit7ExecuteEv.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"status == CompilationJob::SUCCEEDED"
	.section	.text._ZN2v88internal4wasm30JSToWasmWrapperCompilationUnit7ExecuteEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnit7ExecuteEv
	.type	_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnit7ExecuteEv, @function
_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnit7ExecuteEv:
.LFB20490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	_ZZN2v88internal4wasm30JSToWasmWrapperCompilationUnit7ExecuteEvE28trace_event_unique_atomic276(%rip), %rbx
	testq	%rbx, %rbx
	je	.L319
	movq	$0, -96(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L320
.L301:
	movq	16(%r12), %rdi
	call	_ZN2v88internal23OptimizedCompilationJob10ExecuteJobEv@PLT
	testl	%eax, %eax
	jne	.L321
.L305:
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L322
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L323
.L300:
	movq	%rbx, _ZZN2v88internal4wasm30JSToWasmWrapperCompilationUnit7ExecuteEvE28trace_event_unique_atomic276(%rip)
	movq	$0, -96(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	je	.L301
.L320:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L324
.L302:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L303
	movq	(%rdi), %rax
	call	*8(%rax)
.L303:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L304
	movq	(%rdi), %rax
	call	*8(%rax)
.L304:
	leaq	.LC7(%rip), %rax
	movq	16(%r12), %rdi
	movq	%rbx, -88(%rbp)
	movq	%rax, -80(%rbp)
	leaq	-88(%rbp), %rax
	movq	%r13, -72(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal23OptimizedCompilationJob10ExecuteJobEv@PLT
	testl	%eax, %eax
	je	.L305
.L321:
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L324:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC7(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L323:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L300
.L322:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20490:
	.size	_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnit7ExecuteEv, .-_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnit7ExecuteEv
	.section	.text._ZN2v88internal4wasm30JSToWasmWrapperCompilationUnit8FinalizeEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnit8FinalizeEPNS0_7IsolateE
	.type	_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnit8FinalizeEPNS0_7IsolateE, @function
_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnit8FinalizeEPNS0_7IsolateE:
.LFB20491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal23OptimizedCompilationJob11FinalizeJobEPNS0_7IsolateE@PLT
	testl	%eax, %eax
	jne	.L337
	movq	16(%rbx), %rax
	movq	41016(%r12), %r13
	leaq	_ZN2v88internal6Logger27is_listening_to_code_eventsEv(%rip), %rdx
	movq	24(%rax), %rax
	movq	%r13, %rdi
	movq	40(%rax), %r14
	movq	0(%r13), %rax
	movq	136(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L327
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L328
.L331:
	movq	16(%rbx), %rax
	leaq	-48(%rbp), %rdi
	movq	24(%rax), %rsi
	call	_ZNK2v88internal24OptimizedCompilationInfo12GetDebugNameEv@PLT
	movq	-48(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r14, %rsi
	leaq	.LC1(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12_GLOBAL__N_129RecordWasmHeapStubCompilationEPNS0_7IsolateENS0_6HandleINS0_4CodeEEEPKcz.constprop.0
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L332
	call	_ZdaPv@PLT
.L332:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L338
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L328:
	.cfi_restore_state
	cmpq	$0, 80(%r13)
	jne	.L331
	cmpb	$0, 41812(%r12)
	jne	.L331
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L327:
	call	*%rax
	testb	%al, %al
	jne	.L331
	cmpb	$0, 41812(%r12)
	jne	.L331
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L337:
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L338:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20491:
	.size	_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnit8FinalizeEPNS0_7IsolateE, .-_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnit8FinalizeEPNS0_7IsolateE
	.section	.text._ZN2v88internal4wasm30JSToWasmWrapperCompilationUnit22CompileJSToWasmWrapperEPNS0_7IsolateEPNS0_9SignatureINS1_9ValueTypeEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnit22CompileJSToWasmWrapperEPNS0_7IsolateEPNS0_9SignatureINS1_9ValueTypeEEEb
	.type	_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnit22CompileJSToWasmWrapperEPNS0_7IsolateEPNS0_9SignatureINS1_9ValueTypeEEEb, @function
_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnit22CompileJSToWasmWrapperEPNS0_7IsolateEPNS0_9SignatureINS1_9ValueTypeEEEb:
.LFB20492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm23WasmFeaturesFromIsolateEPNS0_7IsolateE@PLT
	movq	%r13, %rcx
	movq	%r12, %rsi
	leaq	-77(%rbp), %r9
	movb	%dl, -69(%rbp)
	movzbl	%bl, %r8d
	leaq	-48(%rbp), %rdi
	movq	%r13, -56(%rbp)
	leaq	-64(%rbp), %r13
	movb	%bl, -64(%rbp)
	movq	%rax, -77(%rbp)
	movzbl	%dh, %eax
	movb	%al, -68(%rbp)
	movq	%rdx, %rax
	shrq	$16, %rax
	movb	%al, -67(%rbp)
	movq	%rdx, %rax
	shrq	$32, %rdx
	movb	%dl, -65(%rbp)
	movq	45752(%r12), %rdx
	shrq	$24, %rax
	movb	%al, -66(%rbp)
	call	_ZN2v88internal8compiler25NewJSToWasmCompilationJobEPNS0_7IsolateEPNS0_4wasm10WasmEngineEPNS0_9SignatureINS4_9ValueTypeEEEbRKNS4_12WasmFeaturesE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnit7ExecuteEv
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnit8FinalizeEPNS0_7IsolateE
	movq	-48(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L340
	movq	(%rdi), %rax
	call	*8(%rax)
.L340:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L346
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L346:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20492:
	.size	_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnit22CompileJSToWasmWrapperEPNS0_7IsolateEPNS0_9SignatureINS1_9ValueTypeEEEb, .-_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnit22CompileJSToWasmWrapperEPNS0_7IsolateEPNS0_9SignatureINS1_9ValueTypeEEEb
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal4wasm21WasmInstructionBufferD2Ev,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal4wasm21WasmInstructionBufferD2Ev, @function
_GLOBAL__sub_I__ZN2v88internal4wasm21WasmInstructionBufferD2Ev:
.LFB25013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25013:
	.size	_GLOBAL__sub_I__ZN2v88internal4wasm21WasmInstructionBufferD2Ev, .-_GLOBAL__sub_I__ZN2v88internal4wasm21WasmInstructionBufferD2Ev
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal4wasm21WasmInstructionBufferD2Ev
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4ViewE,"aw"
	.align 8
	.type	_ZTVN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4ViewE, @object
	.size	_ZTVN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4ViewE, 56
_ZTVN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4ViewE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4ViewD1Ev
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4ViewD0Ev
	.quad	_ZNK2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4View5startEv
	.quad	_ZNK2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4View4sizeEv
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_125WasmInstructionBufferImpl4View4GrowEi
	.section	.bss._ZZN2v88internal4wasm30JSToWasmWrapperCompilationUnit7ExecuteEvE28trace_event_unique_atomic276,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal4wasm30JSToWasmWrapperCompilationUnit7ExecuteEvE28trace_event_unique_atomic276, @object
	.size	_ZZN2v88internal4wasm30JSToWasmWrapperCompilationUnit7ExecuteEvE28trace_event_unique_atomic276, 8
_ZZN2v88internal4wasm30JSToWasmWrapperCompilationUnit7ExecuteEvE28trace_event_unique_atomic276:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
