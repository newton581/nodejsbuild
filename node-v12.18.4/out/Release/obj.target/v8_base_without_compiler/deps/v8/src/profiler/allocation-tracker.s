	.file	"allocation-tracker.cc"
	.text
	.section	.text._ZN2v88internal17AllocationTracker18UnresolvedLocation16HandleWeakScriptERKNS_16WeakCallbackInfoIvEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17AllocationTracker18UnresolvedLocation16HandleWeakScriptERKNS_16WeakCallbackInfoIvEE
	.type	_ZN2v88internal17AllocationTracker18UnresolvedLocation16HandleWeakScriptERKNS_16WeakCallbackInfoIvEE, @function
_ZN2v88internal17AllocationTracker18UnresolvedLocation16HandleWeakScriptERKNS_16WeakCallbackInfoIvEE:
.LFB18366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	8(%rdi), %rbx
	movq	(%rbx), %rdi
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	movq	$0, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18366:
	.size	_ZN2v88internal17AllocationTracker18UnresolvedLocation16HandleWeakScriptERKNS_16WeakCallbackInfoIvEE, .-_ZN2v88internal17AllocationTracker18UnresolvedLocation16HandleWeakScriptERKNS_16WeakCallbackInfoIvEE
	.section	.text._ZN2v88internal19AllocationTraceNodeC2EPNS0_19AllocationTraceTreeEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19AllocationTraceNodeC2EPNS0_19AllocationTraceTreeEj
	.type	_ZN2v88internal19AllocationTraceNodeC2EPNS0_19AllocationTraceTreeEj, @function
_ZN2v88internal19AllocationTraceNodeC2EPNS0_19AllocationTraceTreeEj:
.LFB18267:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	movl	%edx, 8(%rdi)
	pxor	%xmm0, %xmm0
	movq	%rsi, (%rdi)
	leal	1(%rax), %edx
	movq	$0, 12(%rdi)
	movl	%edx, (%rsi)
	movl	%eax, 20(%rdi)
	movq	$0, 40(%rdi)
	movups	%xmm0, 24(%rdi)
	ret
	.cfi_endproc
.LFE18267:
	.size	_ZN2v88internal19AllocationTraceNodeC2EPNS0_19AllocationTraceTreeEj, .-_ZN2v88internal19AllocationTraceNodeC2EPNS0_19AllocationTraceTreeEj
	.globl	_ZN2v88internal19AllocationTraceNodeC1EPNS0_19AllocationTraceTreeEj
	.set	_ZN2v88internal19AllocationTraceNodeC1EPNS0_19AllocationTraceTreeEj,_ZN2v88internal19AllocationTraceNodeC2EPNS0_19AllocationTraceTreeEj
	.section	.text._ZN2v88internal19AllocationTraceNodeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19AllocationTraceNodeD2Ev
	.type	_ZN2v88internal19AllocationTraceNodeD2Ev, @function
_ZN2v88internal19AllocationTraceNodeD2Ev:
.LFB18270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	24(%rdi), %rbx
	movq	32(%rdi), %r13
	cmpq	%r13, %rbx
	je	.L6
	movq	%rdi, %r14
.L8:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L7
	movq	%r12, %rdi
	call	_ZN2v88internal19AllocationTraceNodeD1Ev
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L7:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L8
	movq	24(%r14), %r13
.L6:
	testq	%r13, %r13
	je	.L5
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18270:
	.size	_ZN2v88internal19AllocationTraceNodeD2Ev, .-_ZN2v88internal19AllocationTraceNodeD2Ev
	.globl	_ZN2v88internal19AllocationTraceNodeD1Ev
	.set	_ZN2v88internal19AllocationTraceNodeD1Ev,_ZN2v88internal19AllocationTraceNodeD2Ev
	.section	.text._ZN2v88internal19AllocationTraceNode9FindChildEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19AllocationTraceNode9FindChildEj
	.type	_ZN2v88internal19AllocationTraceNode9FindChildEj, @function
_ZN2v88internal19AllocationTraceNode9FindChildEj:
.LFB18272:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	32(%rdi), %rdx
	cmpq	%rdx, %rax
	jne	.L17
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L20:
	addq	$8, %rax
	cmpq	%rax, %rdx
	je	.L18
.L17:
	movq	(%rax), %r8
	cmpl	8(%r8), %esi
	jne	.L20
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	xorl	%r8d, %r8d
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE18272:
	.size	_ZN2v88internal19AllocationTraceNode9FindChildEj, .-_ZN2v88internal19AllocationTraceNode9FindChildEj
	.section	.text._ZN2v88internal19AllocationTraceNode13AddAllocationEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19AllocationTraceNode13AddAllocationEj
	.type	_ZN2v88internal19AllocationTraceNode13AddAllocationEj, @function
_ZN2v88internal19AllocationTraceNode13AddAllocationEj:
.LFB18274:
	.cfi_startproc
	endbr64
	addl	%esi, 12(%rdi)
	addl	$1, 16(%rdi)
	ret
	.cfi_endproc
.LFE18274:
	.size	_ZN2v88internal19AllocationTraceNode13AddAllocationEj, .-_ZN2v88internal19AllocationTraceNode13AddAllocationEj
	.section	.rodata._ZN2v88internal19AllocationTraceNode5PrintEiPNS0_17AllocationTrackerE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%10u %10u %*c"
.LC1:
	.string	"%s #%u"
.LC2:
	.string	"%u #%u"
.LC3:
	.string	"\n"
	.section	.text._ZN2v88internal19AllocationTraceNode5PrintEiPNS0_17AllocationTrackerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19AllocationTraceNode5PrintEiPNS0_17AllocationTrackerE
	.type	_ZN2v88internal19AllocationTraceNode5PrintEiPNS0_17AllocationTrackerE, @function
_ZN2v88internal19AllocationTraceNode5PrintEiPNS0_17AllocationTrackerE:
.LFB18275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$32, %r8d
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movl	16(%rdi), %edx
	movl	%r12d, %ecx
	movl	12(%rdi), %esi
	leaq	.LC0(%rip), %rdi
	call	_ZN2v84base2OS5PrintEPKcz@PLT
	movl	20(%r13), %edx
	testq	%r14, %r14
	je	.L23
	movl	8(%r13), %ecx
	movq	328(%r14), %rax
	leaq	.LC1(%rip), %rdi
	movq	(%rax,%rcx,8), %rax
	movq	(%rax), %rsi
	xorl	%eax, %eax
	call	_ZN2v84base2OS5PrintEPKcz@PLT
.L24:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	addl	$2, %r12d
	call	_ZN2v84base2OS5PrintEPKcz@PLT
	movq	24(%r13), %rbx
	movq	32(%r13), %r13
	cmpq	%rbx, %r13
	je	.L22
.L26:
	movq	(%rbx), %rdi
	movq	%r14, %rdx
	movl	%r12d, %esi
	addq	$8, %rbx
	call	_ZN2v88internal19AllocationTraceNode5PrintEiPNS0_17AllocationTrackerE
	cmpq	%rbx, %r13
	jne	.L26
.L22:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movl	8(%r13), %esi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v84base2OS5PrintEPKcz@PLT
	jmp	.L24
	.cfi_endproc
.LFE18275:
	.size	_ZN2v88internal19AllocationTraceNode5PrintEiPNS0_17AllocationTrackerE, .-_ZN2v88internal19AllocationTraceNode5PrintEiPNS0_17AllocationTrackerE
	.section	.text._ZN2v88internal19AllocationTraceTreeC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19AllocationTraceTreeC2Ev
	.type	_ZN2v88internal19AllocationTraceTreeC2Ev, @function
_ZN2v88internal19AllocationTraceTreeC2Ev:
.LFB18277:
	.cfi_startproc
	endbr64
	movdqa	.LC4(%rip), %xmm0
	movq	%rdi, 8(%rdi)
	movl	$2, (%rdi)
	movups	%xmm0, 16(%rdi)
	pxor	%xmm0, %xmm0
	movq	$0, 48(%rdi)
	movups	%xmm0, 32(%rdi)
	ret
	.cfi_endproc
.LFE18277:
	.size	_ZN2v88internal19AllocationTraceTreeC2Ev, .-_ZN2v88internal19AllocationTraceTreeC2Ev
	.globl	_ZN2v88internal19AllocationTraceTreeC1Ev
	.set	_ZN2v88internal19AllocationTraceTreeC1Ev,_ZN2v88internal19AllocationTraceTreeC2Ev
	.section	.rodata._ZN2v88internal19AllocationTraceTree5PrintEPNS0_17AllocationTrackerE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"[AllocationTraceTree:]\n"
	.section	.rodata._ZN2v88internal19AllocationTraceTree5PrintEPNS0_17AllocationTrackerE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"Total size | Allocation count | Function id | id\n"
	.section	.text._ZN2v88internal19AllocationTraceTree5PrintEPNS0_17AllocationTrackerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19AllocationTraceTree5PrintEPNS0_17AllocationTrackerE
	.type	_ZN2v88internal19AllocationTraceTree5PrintEPNS0_17AllocationTrackerE, @function
_ZN2v88internal19AllocationTraceTree5PrintEPNS0_17AllocationTrackerE:
.LFB18281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	.LC5(%rip), %rdi
	call	_ZN2v84base2OS5PrintEPKcz@PLT
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v84base2OS5PrintEPKcz@PLT
	leaq	8(%rbx), %rdi
	movq	%r12, %rdx
	popq	%rbx
	xorl	%esi, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal19AllocationTraceNode5PrintEiPNS0_17AllocationTrackerE
	.cfi_endproc
.LFE18281:
	.size	_ZN2v88internal19AllocationTraceTree5PrintEPNS0_17AllocationTrackerE, .-_ZN2v88internal19AllocationTraceTree5PrintEPNS0_17AllocationTrackerE
	.section	.rodata._ZN2v88internal17AllocationTracker12FunctionInfoC2Ev.str1.1,"aMS",@progbits,1
.LC7:
	.string	""
	.section	.text._ZN2v88internal17AllocationTracker12FunctionInfoC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17AllocationTracker12FunctionInfoC2Ev
	.type	_ZN2v88internal17AllocationTracker12FunctionInfoC2Ev, @function
_ZN2v88internal17AllocationTracker12FunctionInfoC2Ev:
.LFB18283:
	.cfi_startproc
	endbr64
	leaq	.LC7(%rip), %rax
	movl	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movq	%rax, 16(%rdi)
	movabsq	$-4294967296, %rax
	movq	%rax, 24(%rdi)
	movl	$-1, 32(%rdi)
	ret
	.cfi_endproc
.LFE18283:
	.size	_ZN2v88internal17AllocationTracker12FunctionInfoC2Ev, .-_ZN2v88internal17AllocationTracker12FunctionInfoC2Ev
	.globl	_ZN2v88internal17AllocationTracker12FunctionInfoC1Ev
	.set	_ZN2v88internal17AllocationTracker12FunctionInfoC1Ev,_ZN2v88internal17AllocationTracker12FunctionInfoC2Ev
	.section	.text._ZN2v88internal17AddressToTraceMap14GetTraceNodeIdEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17AddressToTraceMap14GetTraceNodeIdEm
	.type	_ZN2v88internal17AddressToTraceMap14GetTraceNodeIdEm, @function
_ZN2v88internal17AddressToTraceMap14GetTraceNodeIdEm:
.LFB18298:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	leaq	8(%rdi), %rcx
	testq	%rax, %rax
	je	.L39
	movq	%rcx, %rdx
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L36
.L35:
	cmpq	%rsi, 32(%rax)
	ja	.L42
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L35
.L36:
	xorl	%eax, %eax
	cmpq	%rdx, %rcx
	je	.L33
	cmpq	%rsi, 40(%rdx)
	ja	.L33
	movl	48(%rdx), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE18298:
	.size	_ZN2v88internal17AddressToTraceMap14GetTraceNodeIdEm, .-_ZN2v88internal17AddressToTraceMap14GetTraceNodeIdEm
	.section	.rodata._ZN2v88internal17AddressToTraceMap5PrintEv.str1.1,"aMS",@progbits,1
.LC8:
	.string	"[AddressToTraceMap (%zu): \n"
.LC9:
	.string	"[%p - %p] => %u\n"
.LC10:
	.string	"]\n"
	.section	.text._ZN2v88internal17AddressToTraceMap5PrintEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17AddressToTraceMap5PrintEv
	.type	_ZN2v88internal17AddressToTraceMap5PrintEv, @function
_ZN2v88internal17AddressToTraceMap5PrintEv:
.LFB18301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$8, %rbx
	subq	$8, %rsp
	movq	40(%rdi), %rsi
	leaq	.LC8(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	16(%rbx), %r12
	cmpq	%rbx, %r12
	je	.L44
	leaq	.LC9(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L45:
	movl	48(%r12), %ecx
	movq	32(%r12), %rdx
	movq	%r13, %rdi
	xorl	%eax, %eax
	movq	40(%r12), %rsi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rbx, %rax
	jne	.L45
.L44:
	addq	$8, %rsp
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6PrintFEPKcz@PLT
	.cfi_endproc
.LFE18301:
	.size	_ZN2v88internal17AddressToTraceMap5PrintEv, .-_ZN2v88internal17AddressToTraceMap5PrintEv
	.section	.text._ZN2v88internal17AllocationTracker23PrepareForSerializationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17AllocationTracker23PrepareForSerializationEv
	.type	_ZN2v88internal17AllocationTracker23PrepareForSerializationEv, @function
_ZN2v88internal17AllocationTracker23PrepareForSerializationEv:
.LFB18351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	384(%rdi), %rax
	movq	376(%rdi), %rbx
	movq	%rdi, -72(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rbx, %rax
	je	.L48
	.p2align 4,,10
	.p2align 3
.L53:
	movq	(%rbx), %r13
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L51
	movq	(%rax), %rax
	andq	$-262144, %rax
	movq	24(%rax), %r12
	movq	3496(%r12), %r14
	movq	3504(%r12), %r15
	subq	$37592, %r12
	addl	$1, 41104(%r12)
	movq	16(%r13), %r8
	movl	8(%r13), %esi
	movq	0(%r13), %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal6Script13GetLineNumberENS0_6HandleIS1_EEi@PLT
	movq	-56(%rbp), %r8
	movl	%eax, 28(%r8)
	movq	16(%r13), %r8
	movl	8(%r13), %esi
	movq	0(%r13), %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal6Script15GetColumnNumberENS0_6HandleIS1_EEi@PLT
	movq	-56(%rbp), %r8
	movl	%eax, 32(%r8)
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r15
	je	.L52
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L52:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L51
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
.L51:
	movl	$24, %esi
	movq	%r13, %rdi
	addq	$8, %rbx
	call	_ZdlPvm@PLT
	cmpq	%rbx, -64(%rbp)
	jne	.L53
	movq	-72(%rbp), %rdx
	movq	376(%rdx), %rax
	cmpq	384(%rdx), %rax
	je	.L48
	movq	%rax, 384(%rdx)
.L48:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18351:
	.size	_ZN2v88internal17AllocationTracker23PrepareForSerializationEv, .-_ZN2v88internal17AllocationTracker23PrepareForSerializationEv
	.section	.text._ZN2v88internal17AllocationTracker18UnresolvedLocationC2ENS0_6ScriptEiPNS1_12FunctionInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17AllocationTracker18UnresolvedLocationC2ENS0_6ScriptEiPNS1_12FunctionInfoE
	.type	_ZN2v88internal17AllocationTracker18UnresolvedLocationC2ENS0_6ScriptEiPNS1_12FunctionInfoE, @function
_ZN2v88internal17AllocationTracker18UnresolvedLocationC2ENS0_6ScriptEiPNS1_12FunctionInfoE:
.LFB18360:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	andq	$-262144, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	$0, (%rdi)
	movq	24(%rax), %rax
	movl	%edx, 8(%rdi)
	movq	%rcx, 16(%rdi)
	movq	3560(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	%r12, %rsi
	xorl	%ecx, %ecx
	leaq	_ZN2v88internal17AllocationTracker18UnresolvedLocation16HandleWeakScriptERKNS_16WeakCallbackInfoIvEE(%rip), %rdx
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%rax, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	.cfi_endproc
.LFE18360:
	.size	_ZN2v88internal17AllocationTracker18UnresolvedLocationC2ENS0_6ScriptEiPNS1_12FunctionInfoE, .-_ZN2v88internal17AllocationTracker18UnresolvedLocationC2ENS0_6ScriptEiPNS1_12FunctionInfoE
	.globl	_ZN2v88internal17AllocationTracker18UnresolvedLocationC1ENS0_6ScriptEiPNS1_12FunctionInfoE
	.set	_ZN2v88internal17AllocationTracker18UnresolvedLocationC1ENS0_6ScriptEiPNS1_12FunctionInfoE,_ZN2v88internal17AllocationTracker18UnresolvedLocationC2ENS0_6ScriptEiPNS1_12FunctionInfoE
	.section	.text._ZN2v88internal17AllocationTracker18UnresolvedLocationD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17AllocationTracker18UnresolvedLocationD2Ev
	.type	_ZN2v88internal17AllocationTracker18UnresolvedLocationD2Ev, @function
_ZN2v88internal17AllocationTracker18UnresolvedLocationD2Ev:
.LFB18363:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L61
	jmp	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	.p2align 4,,10
	.p2align 3
.L61:
	ret
	.cfi_endproc
.LFE18363:
	.size	_ZN2v88internal17AllocationTracker18UnresolvedLocationD2Ev, .-_ZN2v88internal17AllocationTracker18UnresolvedLocationD2Ev
	.globl	_ZN2v88internal17AllocationTracker18UnresolvedLocationD1Ev
	.set	_ZN2v88internal17AllocationTracker18UnresolvedLocationD1Ev,_ZN2v88internal17AllocationTracker18UnresolvedLocationD2Ev
	.section	.text._ZN2v88internal17AllocationTracker18UnresolvedLocation7ResolveEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17AllocationTracker18UnresolvedLocation7ResolveEv
	.type	_ZN2v88internal17AllocationTracker18UnresolvedLocation7ResolveEv, @function
_ZN2v88internal17AllocationTracker18UnresolvedLocation7ResolveEv:
.LFB18365:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L67
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rax), %rax
	andq	$-262144, %rax
	movq	24(%rax), %r12
	movq	3496(%r12), %r14
	movq	3504(%r12), %r13
	subq	$37592, %r12
	addl	$1, 41104(%r12)
	movq	16(%rdi), %r15
	movl	8(%rdi), %esi
	movq	(%rdi), %rdi
	call	_ZN2v88internal6Script13GetLineNumberENS0_6HandleIS1_EEi@PLT
	movl	%eax, 28(%r15)
	movl	8(%rbx), %esi
	movq	(%rbx), %rdi
	movq	16(%rbx), %r15
	call	_ZN2v88internal6Script15GetColumnNumberENS0_6HandleIS1_EEi@PLT
	movl	%eax, 32(%r15)
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L63
	movq	%r13, 41096(%r12)
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE18365:
	.size	_ZN2v88internal17AllocationTracker18UnresolvedLocation7ResolveEv, .-_ZN2v88internal17AllocationTracker18UnresolvedLocation7ResolveEv
	.section	.rodata._ZNSt6vectorIPN2v88internal19AllocationTraceNodeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_.str1.1,"aMS",@progbits,1
.LC11:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN2v88internal19AllocationTraceNodeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal19AllocationTraceNodeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal19AllocationTraceNodeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal19AllocationTraceNodeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal19AllocationTraceNodeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB21415:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L84
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L80
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L85
.L72:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L79:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L86
	testq	%r13, %r13
	jg	.L75
	testq	%r9, %r9
	jne	.L78
.L76:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L75
.L78:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L85:
	testq	%rsi, %rsi
	jne	.L73
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L76
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L80:
	movl	$8, %r14d
	jmp	.L72
.L84:
	leaq	.LC11(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L73:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L72
	.cfi_endproc
.LFE21415:
	.size	_ZNSt6vectorIPN2v88internal19AllocationTraceNodeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIPN2v88internal19AllocationTraceNodeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZN2v88internal19AllocationTraceNode14FindOrAddChildEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19AllocationTraceNode14FindOrAddChildEj
	.type	_ZN2v88internal19AllocationTraceNode14FindOrAddChildEj, @function
_ZN2v88internal19AllocationTraceNode14FindOrAddChildEj:
.LFB18273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	subq	$16, %rsp
	movq	32(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	cmpq	%rdx, %rax
	jne	.L90
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L97:
	addq	$8, %rax
	cmpq	%rax, %rdx
	je	.L88
.L90:
	movq	(%rax), %r8
	cmpl	8(%r8), %ebx
	jne	.L97
.L87:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L98
	addq	$16, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	movl	$48, %edi
	movq	$0, -32(%rbp)
	call	_Znwm@PLT
	movq	(%r12), %rdx
	pxor	%xmm0, %xmm0
	movl	%ebx, 8(%rax)
	movl	(%rdx), %ecx
	movq	%rdx, (%rax)
	movq	$0, 12(%rax)
	leal	1(%rcx), %esi
	movl	%ecx, 20(%rax)
	movl	%esi, (%rdx)
	movq	32(%r12), %rsi
	movq	$0, 40(%rax)
	movq	%rax, -32(%rbp)
	movups	%xmm0, 24(%rax)
	cmpq	40(%r12), %rsi
	je	.L99
	movq	%rax, (%rsi)
	movq	-32(%rbp), %r8
	addq	$8, 32(%r12)
	jmp	.L87
.L99:
	leaq	-32(%rbp), %rdx
	leaq	24(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal19AllocationTraceNodeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	movq	-32(%rbp), %r8
	jmp	.L87
.L98:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18273:
	.size	_ZN2v88internal19AllocationTraceNode14FindOrAddChildEj, .-_ZN2v88internal19AllocationTraceNode14FindOrAddChildEj
	.section	.text._ZN2v88internal19AllocationTraceTree14AddPathFromEndERKNS0_6VectorIjEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19AllocationTraceTree14AddPathFromEndERKNS0_6VectorIjEE
	.type	_ZN2v88internal19AllocationTraceTree14AddPathFromEndERKNS0_6VectorIjEE, @function
_ZN2v88internal19AllocationTraceTree14AddPathFromEndERKNS0_6VectorIjEE:
.LFB18279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	8(%rdi), %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movslq	8(%r14), %rax
	leaq	-4(%rsi,%rax,4), %rbx
	leaq	-4(%rsi), %rax
	cmpq	%rax, %rbx
	je	.L100
	.p2align 4,,10
	.p2align 3
.L105:
	movq	24(%r12), %rax
	movq	32(%r12), %rdx
	movl	(%rbx), %r13d
	cmpq	%rdx, %rax
	jne	.L104
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L114:
	addq	$8, %rax
	cmpq	%rax, %rdx
	je	.L102
.L104:
	movq	(%rax), %rcx
	cmpl	8(%rcx), %r13d
	jne	.L114
	movq	%rcx, %r12
.L103:
	subq	$4, %rbx
	leaq	-4(%rsi), %rax
	cmpq	%rax, %rbx
	jne	.L105
.L100:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L115
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movl	$48, %edi
	movq	$0, -48(%rbp)
	call	_Znwm@PLT
	movq	(%r12), %rdx
	pxor	%xmm0, %xmm0
	movl	%r13d, 8(%rax)
	movl	(%rdx), %ecx
	movq	%rdx, (%rax)
	movq	$0, 12(%rax)
	leal	1(%rcx), %esi
	movl	%ecx, 20(%rax)
	movl	%esi, (%rdx)
	movq	$0, 40(%rax)
	movups	%xmm0, 24(%rax)
	movq	32(%r12), %rsi
	movq	%rax, -48(%rbp)
	cmpq	40(%r12), %rsi
	je	.L116
	movq	%rax, (%rsi)
	addq	$8, 32(%r12)
	movq	-48(%rbp), %r12
	movq	(%r14), %rsi
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L116:
	leaq	24(%r12), %rdi
	leaq	-48(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal19AllocationTraceNodeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	movq	-48(%rbp), %r12
	movq	(%r14), %rsi
	jmp	.L103
.L115:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18279:
	.size	_ZN2v88internal19AllocationTraceTree14AddPathFromEndERKNS0_6VectorIjEE, .-_ZN2v88internal19AllocationTraceTree14AddPathFromEndERKNS0_6VectorIjEE
	.section	.text._ZNSt8_Rb_treeImSt4pairIKmN2v88internal17AddressToTraceMap10RangeStackEESt10_Select1stIS6_ESt4lessImESaIS6_EE16_M_insert_uniqueIS6_EES0_ISt17_Rb_tree_iteratorIS6_EbEOT_,"axG",@progbits,_ZNSt8_Rb_treeImSt4pairIKmN2v88internal17AddressToTraceMap10RangeStackEESt10_Select1stIS6_ESt4lessImESaIS6_EE16_M_insert_uniqueIS6_EES0_ISt17_Rb_tree_iteratorIS6_EbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal17AddressToTraceMap10RangeStackEESt10_Select1stIS6_ESt4lessImESaIS6_EE16_M_insert_uniqueIS6_EES0_ISt17_Rb_tree_iteratorIS6_EbEOT_
	.type	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal17AddressToTraceMap10RangeStackEESt10_Select1stIS6_ESt4lessImESaIS6_EE16_M_insert_uniqueIS6_EES0_ISt17_Rb_tree_iteratorIS6_EbEOT_, @function
_ZNSt8_Rb_treeImSt4pairIKmN2v88internal17AddressToTraceMap10RangeStackEESt10_Select1stIS6_ESt4lessImESaIS6_EE16_M_insert_uniqueIS6_EES0_ISt17_Rb_tree_iteratorIS6_EbEOT_:
.LFB21418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r12
	testq	%r12, %r12
	je	.L118
	movq	(%rsi), %r13
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L135:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L120
.L136:
	movq	%rax, %r12
.L119:
	movq	32(%r12), %rcx
	cmpq	%rcx, %r13
	jb	.L135
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L136
.L120:
	testb	%dl, %dl
	jne	.L137
	cmpq	%rcx, %r13
	jbe	.L128
.L127:
	movl	$1, %r8d
	cmpq	%r12, %r15
	jne	.L138
.L125:
	movl	$56, %edi
	movl	%r8d, -52(%rbp)
	call	_Znwm@PLT
	movdqu	(%r14), %xmm0
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%rax, %r13
	movl	-52(%rbp), %r8d
	movups	%xmm0, 32(%rax)
	movq	16(%r14), %rax
	movq	%r13, %rsi
	movl	%r8d, %edi
	movq	%rax, 48(%r13)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	cmpq	24(%rbx), %r12
	je	.L127
.L129:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	cmpq	32(%rax), %r13
	ja	.L127
	movq	%rax, %r12
.L128:
	addq	$24, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	xorl	%r8d, %r8d
	cmpq	32(%r12), %r13
	setb	%r8b
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L118:
	movq	%r15, %r12
	cmpq	24(%rdi), %r15
	je	.L131
	movq	(%rsi), %r13
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L131:
	movl	$1, %r8d
	jmp	.L125
	.cfi_endproc
.LFE21418:
	.size	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal17AddressToTraceMap10RangeStackEESt10_Select1stIS6_ESt4lessImESaIS6_EE16_M_insert_uniqueIS6_EES0_ISt17_Rb_tree_iteratorIS6_EbEOT_, .-_ZNSt8_Rb_treeImSt4pairIKmN2v88internal17AddressToTraceMap10RangeStackEESt10_Select1stIS6_ESt4lessImESaIS6_EE16_M_insert_uniqueIS6_EES0_ISt17_Rb_tree_iteratorIS6_EbEOT_
	.section	.text._ZNSt8_Rb_treeImSt4pairIKmN2v88internal17AddressToTraceMap10RangeStackEESt10_Select1stIS6_ESt4lessImESaIS6_EE8_M_eraseEPSt13_Rb_tree_nodeIS6_E,"axG",@progbits,_ZNSt8_Rb_treeImSt4pairIKmN2v88internal17AddressToTraceMap10RangeStackEESt10_Select1stIS6_ESt4lessImESaIS6_EE8_M_eraseEPSt13_Rb_tree_nodeIS6_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal17AddressToTraceMap10RangeStackEESt10_Select1stIS6_ESt4lessImESaIS6_EE8_M_eraseEPSt13_Rb_tree_nodeIS6_E
	.type	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal17AddressToTraceMap10RangeStackEESt10_Select1stIS6_ESt4lessImESaIS6_EE8_M_eraseEPSt13_Rb_tree_nodeIS6_E, @function
_ZNSt8_Rb_treeImSt4pairIKmN2v88internal17AddressToTraceMap10RangeStackEESt10_Select1stIS6_ESt4lessImESaIS6_EE8_M_eraseEPSt13_Rb_tree_nodeIS6_E:
.LFB21475:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L147
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L141:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal17AddressToTraceMap10RangeStackEESt10_Select1stIS6_ESt4lessImESaIS6_EE8_M_eraseEPSt13_Rb_tree_nodeIS6_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L141
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE21475:
	.size	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal17AddressToTraceMap10RangeStackEESt10_Select1stIS6_ESt4lessImESaIS6_EE8_M_eraseEPSt13_Rb_tree_nodeIS6_E, .-_ZNSt8_Rb_treeImSt4pairIKmN2v88internal17AddressToTraceMap10RangeStackEESt10_Select1stIS6_ESt4lessImESaIS6_EE8_M_eraseEPSt13_Rb_tree_nodeIS6_E
	.section	.text._ZN2v88internal17AddressToTraceMap5ClearEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17AddressToTraceMap5ClearEv
	.type	_ZN2v88internal17AddressToTraceMap5ClearEv, @function
_ZN2v88internal17AddressToTraceMap5ClearEv:
.LFB18300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	16(%rdi), %r12
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L151
.L152:
	movq	24(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal17AddressToTraceMap10RangeStackEESt10_Select1stIS6_ESt4lessImESaIS6_EE8_M_eraseEPSt13_Rb_tree_nodeIS6_E
	movq	%r12, %rdi
	movq	16(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L152
.L151:
	leaq	8(%rbx), %rax
	movq	$0, 16(%rbx)
	movq	%rax, 24(%rbx)
	movq	%rax, 32(%rbx)
	movq	$0, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18300:
	.size	_ZN2v88internal17AddressToTraceMap5ClearEv, .-_ZN2v88internal17AddressToTraceMap5ClearEv
	.section	.text._ZN2v88internal17AllocationTrackerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17AllocationTrackerD2Ev
	.type	_ZN2v88internal17AllocationTrackerD2Ev, @function
_ZN2v88internal17AllocationTrackerD2Ev:
.LFB18349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	384(%rdi), %r14
	movq	376(%rdi), %rbx
	cmpq	%r14, %rbx
	je	.L165
	.p2align 4,,10
	.p2align 3
.L164:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L162
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L163
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
.L163:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L162:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L164
.L165:
	movq	336(%r13), %r12
	movq	328(%r13), %rbx
	cmpq	%rbx, %r12
	je	.L161
	.p2align 4,,10
	.p2align 3
.L169:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L168
	movl	$40, %esi
	call	_ZdlPvm@PLT
.L168:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L169
.L161:
	movq	424(%r13), %rbx
	leaq	408(%r13), %r12
	testq	%rbx, %rbx
	je	.L166
.L167:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal17AddressToTraceMap10RangeStackEESt10_Select1stIS6_ESt4lessImESaIS6_EE8_M_eraseEPSt13_Rb_tree_nodeIS6_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L167
.L166:
	movq	376(%r13), %rdi
	testq	%rdi, %rdi
	je	.L171
	call	_ZdlPv@PLT
.L171:
	movq	352(%r13), %rdi
	call	free@PLT
	movq	328(%r13), %rdi
	testq	%rdi, %rdi
	je	.L172
	call	_ZdlPv@PLT
.L172:
	movq	48(%r13), %rbx
	movq	56(%r13), %r14
	cmpq	%r14, %rbx
	je	.L173
.L175:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L174
	movq	%r12, %rdi
	call	_ZN2v88internal19AllocationTraceNodeD1Ev
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L174:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L175
	movq	48(%r13), %r14
.L173:
	testq	%r14, %r14
	je	.L158
	popq	%rbx
	movq	%r14, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18349:
	.size	_ZN2v88internal17AllocationTrackerD2Ev, .-_ZN2v88internal17AllocationTrackerD2Ev
	.globl	_ZN2v88internal17AllocationTrackerD1Ev
	.set	_ZN2v88internal17AllocationTrackerD1Ev,_ZN2v88internal17AllocationTrackerD2Ev
	.section	.text._ZN2v88internal17AddressToTraceMap11RemoveRangeEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17AddressToTraceMap11RemoveRangeEmm
	.type	_ZN2v88internal17AddressToTraceMap11RemoveRangeEmm, @function
_ZN2v88internal17AddressToTraceMap11RemoveRangeEmm:
.LFB18302:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r13, %rbx
	subq	$72, %rsp
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	jne	.L205
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L248:
	movq	%rax, %rbx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L207
.L205:
	movq	-88(%rbp), %rcx
	cmpq	%rcx, 32(%rax)
	ja	.L248
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L205
.L207:
	cmpq	%rbx, %r13
	je	.L204
	movq	40(%rbx), %rax
	movq	%rax, -96(%rbp)
	cmpq	-88(%rbp), %rax
	jb	.L249
	movl	$0, -100(%rbp)
	movq	$0, -96(%rbp)
.L211:
	movq	%rbx, %r14
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L212:
	movq	%r14, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r14
	cmpq	%rax, %r13
	je	.L213
.L217:
	cmpq	%r15, 32(%r14)
	jbe	.L212
	cmpq	%r15, 40(%r14)
	jnb	.L213
	movq	%r15, 40(%r14)
	cmpq	%rbx, 24(%r12)
	jne	.L247
	cmpq	%r13, %r14
	jne	.L247
	movq	16(%r12), %rbx
	testq	%rbx, %rbx
	jne	.L219
.L218:
	movq	$0, 16(%r12)
	movq	%r13, 24(%r12)
	movq	%r13, 32(%r12)
	movq	$0, 40(%r12)
	.p2align 4,,10
	.p2align 3
.L220:
	cmpq	$0, -96(%rbp)
	je	.L204
	movq	-88(%rbp), %xmm0
	movl	-100(%rbp), %eax
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movhps	-96(%rbp), %xmm0
	movl	%eax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal17AddressToTraceMap10RangeStackEESt10_Select1stIS6_ESt4lessImESaIS6_EE16_M_insert_uniqueIS6_EES0_ISt17_Rb_tree_iteratorIS6_EbEOT_
.L204:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L250
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rbx, %r15
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	subq	$1, 40(%r12)
.L247:
	cmpq	%r14, %rbx
	jne	.L222
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L249:
	movl	48(%rbx), %eax
	movl	%eax, -100(%rbp)
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L213:
	cmpq	%rbx, 24(%r12)
	jne	.L247
	cmpq	%r14, %r13
	jne	.L247
	movq	-112(%rbp), %rbx
.L219:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal17AddressToTraceMap10RangeStackEESt10_Select1stIS6_ESt4lessImESaIS6_EE8_M_eraseEPSt13_Rb_tree_nodeIS6_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L219
	jmp	.L218
.L250:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18302:
	.size	_ZN2v88internal17AddressToTraceMap11RemoveRangeEmm, .-_ZN2v88internal17AddressToTraceMap11RemoveRangeEmm
	.section	.text._ZN2v88internal17AddressToTraceMap8AddRangeEmij,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17AddressToTraceMap8AddRangeEmij
	.type	_ZN2v88internal17AddressToTraceMap8AddRangeEmij, @function
_ZN2v88internal17AddressToTraceMap8AddRangeEmij:
.LFB18285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	(%rdx,%rsi), %rbx
	movq	%rbx, %rdx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17AddressToTraceMap11RemoveRangeEmm
	leaq	-64(%rbp), %rsi
	movq	%r13, %rdi
	movl	%r14d, -48(%rbp)
	movq	%r12, %xmm1
	movq	%rbx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal17AddressToTraceMap10RangeStackEESt10_Select1stIS6_ESt4lessImESaIS6_EE16_M_insert_uniqueIS6_EES0_ISt17_Rb_tree_iteratorIS6_EbEOT_
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L254
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L254:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18285:
	.size	_ZN2v88internal17AddressToTraceMap8AddRangeEmij, .-_ZN2v88internal17AddressToTraceMap8AddRangeEmij
	.section	.text._ZN2v88internal17AddressToTraceMap10MoveObjectEmmi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17AddressToTraceMap10MoveObjectEmmi
	.type	_ZN2v88internal17AddressToTraceMap10MoveObjectEmmi, @function
_ZN2v88internal17AddressToTraceMap10MoveObjectEmmi:
.LFB18299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L255
	addq	$8, %rdi
	movq	%rdx, %r13
	movq	%rdi, %rdx
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L269:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L258
.L257:
	cmpq	32(%rax), %rsi
	jb	.L269
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L257
.L258:
	cmpq	%rdx, %rdi
	je	.L255
	cmpq	40(%rdx), %rsi
	jb	.L255
	movl	48(%rdx), %r14d
	testl	%r14d, %r14d
	je	.L255
	movslq	%ecx, %rbx
	movq	%r12, %rdi
	leaq	(%rbx,%rsi), %rdx
	addq	%r13, %rbx
	call	_ZN2v88internal17AddressToTraceMap11RemoveRangeEmm
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rbx, %rdx
	call	_ZN2v88internal17AddressToTraceMap11RemoveRangeEmm
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movl	%r14d, -48(%rbp)
	movq	%rbx, %xmm0
	movq	%r13, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal17AddressToTraceMap10RangeStackEESt10_Select1stIS6_ESt4lessImESaIS6_EE16_M_insert_uniqueIS6_EES0_ISt17_Rb_tree_iteratorIS6_EbEOT_
.L255:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L270
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L270:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18299:
	.size	_ZN2v88internal17AddressToTraceMap10MoveObjectEmmi, .-_ZN2v88internal17AddressToTraceMap10MoveObjectEmmi
	.section	.text._ZNSt6vectorIPN2v88internal17AllocationTracker12FunctionInfoESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal17AllocationTracker12FunctionInfoESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal17AllocationTracker12FunctionInfoESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal17AllocationTracker12FunctionInfoESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal17AllocationTracker12FunctionInfoESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB21479:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L285
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L281
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L286
.L273:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L280:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L287
	testq	%r13, %r13
	jg	.L276
	testq	%r9, %r9
	jne	.L279
.L277:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L287:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L276
.L279:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L286:
	testq	%rsi, %rsi
	jne	.L274
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L276:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L277
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L281:
	movl	$8, %r14d
	jmp	.L273
.L285:
	leaq	.LC11(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L274:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L273
	.cfi_endproc
.LFE21479:
	.size	_ZNSt6vectorIPN2v88internal17AllocationTracker12FunctionInfoESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal17AllocationTracker12FunctionInfoESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.rodata._ZN2v88internal17AllocationTrackerC2EPNS0_14HeapObjectsMapEPNS0_14StringsStorageE.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"Out of memory: HashMap::Initialize"
	.section	.rodata._ZN2v88internal17AllocationTrackerC2EPNS0_14HeapObjectsMapEPNS0_14StringsStorageE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"(root)"
	.section	.text._ZN2v88internal17AllocationTrackerC2EPNS0_14HeapObjectsMapEPNS0_14StringsStorageE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17AllocationTrackerC2EPNS0_14HeapObjectsMapEPNS0_14StringsStorageE
	.type	_ZN2v88internal17AllocationTrackerC2EPNS0_14HeapObjectsMapEPNS0_14StringsStorageE, @function
_ZN2v88internal17AllocationTrackerC2EPNS0_14HeapObjectsMapEPNS0_14StringsStorageE:
.LFB18346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	leaq	16(%rdi), %rax
	movdqa	.LC4(%rip), %xmm0
	movq	%rax, 24(%rdi)
	movl	$2, 16(%rdi)
	movq	$0, 64(%rdi)
	movq	$0, 344(%rdi)
	movups	%xmm0, 32(%rdi)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 328(%rdi)
	movl	$192, %edi
	call	malloc@PLT
	movq	%rax, 352(%rbx)
	testq	%rax, %rax
	je	.L296
	movl	$8, 360(%rbx)
	movl	$24, %edx
	movq	$0, (%rax)
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L290:
	movq	352(%rbx), %rcx
	addq	$1, %rax
	movq	$0, (%rcx,%rdx)
	movl	360(%rbx), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L290
	leaq	416(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$0, 364(%rbx)
	movl	$40, %edi
	movq	$0, 392(%rbx)
	movl	$0, 400(%rbx)
	movl	$0, 416(%rbx)
	movq	$0, 424(%rbx)
	movq	%rax, 432(%rbx)
	movq	%rax, 440(%rbx)
	movq	$0, 448(%rbx)
	movups	%xmm0, 376(%rbx)
	call	_Znwm@PLT
	leaq	.LC7(%rip), %rcx
	movabsq	$-4294967296, %rdi
	movq	336(%rbx), %rsi
	movq	%rcx, 16(%rax)
	leaq	.LC13(%rip), %rcx
	movl	$0, 8(%rax)
	movq	%rdi, 24(%rax)
	movl	$-1, 32(%rax)
	movq	%rax, -32(%rbp)
	movq	%rcx, (%rax)
	cmpq	344(%rbx), %rsi
	je	.L291
	movq	%rax, (%rsi)
	addq	$8, 336(%rbx)
.L288:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L297
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	leaq	328(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal17AllocationTracker12FunctionInfoESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L288
.L297:
	call	__stack_chk_fail@PLT
.L296:
	leaq	.LC12(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18346:
	.size	_ZN2v88internal17AllocationTrackerC2EPNS0_14HeapObjectsMapEPNS0_14StringsStorageE, .-_ZN2v88internal17AllocationTrackerC2EPNS0_14HeapObjectsMapEPNS0_14StringsStorageE
	.globl	_ZN2v88internal17AllocationTrackerC1EPNS0_14HeapObjectsMapEPNS0_14StringsStorageE
	.set	_ZN2v88internal17AllocationTrackerC1EPNS0_14HeapObjectsMapEPNS0_14StringsStorageE,_ZN2v88internal17AllocationTrackerC2EPNS0_14HeapObjectsMapEPNS0_14StringsStorageE
	.section	.rodata._ZN2v88internal17AllocationTracker27functionInfoIndexForVMStateENS_8StateTagE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"(V8 API)"
	.section	.text._ZN2v88internal17AllocationTracker27functionInfoIndexForVMStateENS_8StateTagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17AllocationTracker27functionInfoIndexForVMStateENS_8StateTagE
	.type	_ZN2v88internal17AllocationTracker27functionInfoIndexForVMStateENS_8StateTagE, @function
_ZN2v88internal17AllocationTracker27functionInfoIndexForVMStateENS_8StateTagE:
.LFB18358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$5, %esi
	jne	.L298
	movl	400(%rdi), %eax
	movq	%rdi, %rbx
	testl	%eax, %eax
	je	.L306
.L298:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L307
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	movl	$40, %edi
	call	_Znwm@PLT
	movq	336(%rbx), %rsi
	movabsq	$-4294967296, %rdx
	leaq	.LC7(%rip), %rcx
	movq	%rdx, 24(%rax)
	movq	%rsi, %rdx
	subq	328(%rbx), %rdx
	movq	%rcx, 16(%rax)
	leaq	.LC14(%rip), %rcx
	sarq	$3, %rdx
	movl	$0, 8(%rax)
	movl	$-1, 32(%rax)
	movq	%rax, -32(%rbp)
	movq	%rcx, (%rax)
	movl	%edx, 400(%rbx)
	cmpq	344(%rbx), %rsi
	je	.L301
	movq	%rax, (%rsi)
	addq	$8, 336(%rbx)
.L302:
	movl	400(%rbx), %eax
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L301:
	leaq	-32(%rbp), %rdx
	leaq	328(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal17AllocationTracker12FunctionInfoESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L302
.L307:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18358:
	.size	_ZN2v88internal17AllocationTracker27functionInfoIndexForVMStateENS_8StateTagE, .-_ZN2v88internal17AllocationTracker27functionInfoIndexForVMStateENS_8StateTagE
	.section	.text._ZNSt6vectorIPN2v88internal17AllocationTracker18UnresolvedLocationESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal17AllocationTracker18UnresolvedLocationESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal17AllocationTracker18UnresolvedLocationESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal17AllocationTracker18UnresolvedLocationESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal17AllocationTracker18UnresolvedLocationESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB22118:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L322
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L318
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L323
.L310:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L317:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L324
	testq	%r13, %r13
	jg	.L313
	testq	%r9, %r9
	jne	.L316
.L314:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L324:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L313
.L316:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L323:
	testq	%rsi, %rsi
	jne	.L311
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L313:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L314
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L318:
	movl	$8, %r14d
	jmp	.L310
.L322:
	leaq	.LC11(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L311:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L310
	.cfi_endproc
.LFE22118:
	.size	_ZNSt6vectorIPN2v88internal17AllocationTracker18UnresolvedLocationESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal17AllocationTracker18UnresolvedLocationESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_,"axG",@progbits,_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_
	.type	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_, @function
_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_:
.LFB22348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	8(%rdi), %eax
	movq	(%rdi), %r12
	movl	12(%rdi), %r14d
	addl	%eax, %eax
	leaq	(%rax,%rax,2), %rdi
	movq	%rax, %rbx
	salq	$3, %rdi
	call	malloc@PLT
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L365
	movl	%ebx, 8(%r13)
	testl	%ebx, %ebx
	je	.L327
	movq	$0, (%rax)
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L328:
	movq	0(%r13), %rcx
	addq	$1, %rax
	movq	$0, (%rcx,%rdx)
	movl	8(%r13), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L328
.L327:
	movl	$0, 12(%r13)
	movq	%r12, %r15
	testl	%r14d, %r14d
	je	.L335
.L329:
	movq	(%r15), %rsi
	testq	%rsi, %rsi
	jne	.L366
.L330:
	movq	24(%r15), %rsi
	addq	$24, %r15
	testq	%rsi, %rsi
	je	.L330
.L366:
	movl	8(%r13), %eax
	movl	16(%r15), %ebx
	movq	0(%r13), %r8
	leal	-1(%rax), %edi
	movl	%ebx, %eax
	andl	%edi, %eax
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L367:
	testq	%rdx, %rdx
	je	.L331
	addq	$1, %rax
	andq	%rdi, %rax
.L364:
	leaq	(%rax,%rax,2), %rdx
	leaq	(%r8,%rdx,8), %rcx
	movq	(%rcx), %rdx
	cmpq	%rdx, %rsi
	jne	.L367
.L331:
	movq	8(%r15), %rax
	movq	%rsi, (%rcx)
	movl	%ebx, 16(%rcx)
	movq	%rax, 8(%rcx)
	movl	12(%r13), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 12(%r13)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	8(%r13), %eax
	jnb	.L334
.L337:
	addq	$24, %r15
	subl	$1, %r14d
	jne	.L329
.L335:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_
	movl	8(%r13), %eax
	movq	0(%r13), %rsi
	leal	-1(%rax), %ecx
	movl	%ebx, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	movq	(%rsi,%rdx,8), %rdx
	testq	%rdx, %rdx
	je	.L337
	movq	(%r15), %rdi
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L368:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	movq	(%rsi,%rdx,8), %rdx
	testq	%rdx, %rdx
	je	.L337
.L338:
	cmpq	%rdx, %rdi
	jne	.L368
	jmp	.L337
.L365:
	leaq	.LC12(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22348:
	.size	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_, .-_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_
	.section	.text._ZN2v88internal17AllocationTracker15AddFunctionInfoENS0_18SharedFunctionInfoEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17AllocationTracker15AddFunctionInfoENS0_18SharedFunctionInfoEj
	.type	_ZN2v88internal17AllocationTracker15AddFunctionInfoENS0_18SharedFunctionInfoEj, @function
_ZN2v88internal17AllocationTracker15AddFunctionInfoENS0_18SharedFunctionInfoEj:
.LFB18357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r15, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%rsi, -88(%rbp)
	movq	352(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%r15d, %eax
	sall	$15, %eax
	subl	%r15d, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%edx, %eax
	andl	$1073741823, %eax
	movl	%eax, %r13d
	movl	360(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%r13d, %eax
	andl	%edx, %eax
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L407:
	cmpq	%rcx, %r15
	je	.L371
	addq	$1, %rax
	andq	%rdx, %rax
.L406:
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rsi,%rcx,8), %r14
	movq	(%r14), %rcx
	testq	%rcx, %rcx
	jne	.L407
	movq	%r15, (%r14)
	movq	$0, 8(%r14)
	movl	%r13d, 16(%r14)
	movl	364(%rbx), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 364(%rbx)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	360(%rbx), %eax
	jnb	.L408
.L371:
	movq	8(%r14), %rax
	testq	%rax, %rax
	je	.L409
.L375:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L410
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L408:
	.cfi_restore_state
	leaq	352(%rbx), %rdi
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_
	movl	360(%rbx), %eax
	movq	352(%rbx), %rsi
	leal	-1(%rax), %ecx
	movl	%r13d, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %r14
	movq	(%r14), %rdx
	testq	%rdx, %rdx
	je	.L371
	cmpq	%rdx, %r15
	jne	.L373
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L411:
	testq	%rdx, %rdx
	je	.L371
.L373:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %r14
	movq	(%r14), %rdx
	cmpq	%rdx, %r15
	jne	.L411
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L409:
	movl	$40, %edi
	leaq	-88(%rbp), %r15
	call	_Znwm@PLT
	leaq	.LC7(%rip), %rdx
	movq	8(%rbx), %r8
	movabsq	$-4294967296, %rdi
	movq	%rdx, (%rax)
	movq	%rdx, 16(%rax)
	movq	%rdi, 24(%rax)
	movq	%r15, %rdi
	movl	$0, 8(%rax)
	movl	$-1, 32(%rax)
	movq	%r8, -96(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	movq	-96(%rbp), %r8
	movq	-72(%rbp), %r13
	movq	%rax, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal14StringsStorage7GetNameENS0_4NameE@PLT
	movq	%rax, 0(%r13)
	movq	-72(%rbp), %r13
	movq	-88(%rbp), %rdx
	movl	%r12d, 8(%r13)
	movq	31(%rdx), %rax
	testb	$1, %al
	jne	.L412
.L377:
	movq	336(%rbx), %rax
	subq	328(%rbx), %rax
	sarq	$3, %rax
	movq	%rax, 8(%r14)
	movq	336(%rbx), %rsi
	cmpq	344(%rbx), %rsi
	je	.L386
	movq	-72(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 336(%rbx)
.L387:
	movq	8(%r14), %rax
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L412:
	movq	-1(%rax), %rsi
	leaq	-1(%rax), %rcx
	cmpw	$86, 11(%rsi)
	je	.L413
.L378:
	movq	(%rcx), %rax
	cmpw	$96, 11(%rax)
	jne	.L377
	movq	31(%rdx), %r8
	testb	$1, %r8b
	jne	.L414
.L380:
	movq	15(%r8), %rsi
	testb	$1, %sil
	jne	.L415
.L382:
	movslq	67(%r8), %rax
	movq	%r15, %rdi
	movq	%r8, -96(%rbp)
	movl	%eax, 24(%r13)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movl	$24, %edi
	movl	%eax, %r13d
	call	_Znwm@PLT
	movq	-96(%rbp), %r8
	movq	%rax, %r12
	movq	$0, (%rax)
	movl	%r13d, 8(%rax)
	movq	-72(%rbp), %rax
	movq	%r8, %rsi
	movq	%rax, 16(%r12)
	movq	%r8, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	3560(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	%r12, %rsi
	xorl	%ecx, %ecx
	leaq	_ZN2v88internal17AllocationTracker18UnresolvedLocation16HandleWeakScriptERKNS_16WeakCallbackInfoIvEE(%rip), %rdx
	movq	%rax, (%r12)
	movq	%rax, %rdi
	call	_ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	%r12, -64(%rbp)
	movq	384(%rbx), %rsi
	cmpq	392(%rbx), %rsi
	je	.L384
	movq	%r12, (%rsi)
	addq	$8, 384(%rbx)
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L413:
	movq	23(%rax), %rcx
	testb	$1, %cl
	je	.L377
	subq	$1, %rcx
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L386:
	leaq	-72(%rbp), %rdx
	leaq	328(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal17AllocationTracker12FunctionInfoESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L387
.L415:
	movq	-1(%rsi), %rax
	cmpw	$64, 11(%rax)
	ja	.L382
	movq	8(%rbx), %rdi
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal14StringsStorage7GetNameENS0_4NameE@PLT
	movq	-96(%rbp), %r8
	movq	%rax, 16(%r13)
	movq	-72(%rbp), %r13
	jmp	.L382
.L414:
	movq	-1(%r8), %rax
	cmpw	$86, 11(%rax)
	jne	.L380
	movq	23(%r8), %r8
	jmp	.L380
.L384:
	leaq	-64(%rbp), %rdx
	leaq	376(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal17AllocationTracker18UnresolvedLocationESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L377
.L410:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18357:
	.size	_ZN2v88internal17AllocationTracker15AddFunctionInfoENS0_18SharedFunctionInfoEj, .-_ZN2v88internal17AllocationTracker15AddFunctionInfoENS0_18SharedFunctionInfoEj
	.section	.text._ZN2v88internal17AllocationTracker15AllocationEventEmi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17AllocationTracker15AllocationEventEmi
	.type	_ZN2v88internal17AllocationTracker15AllocationEventEmi, @function
_ZN2v88internal17AllocationTracker15AllocationEventEmi:
.LFB18352:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-1504(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1512, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -1544(%rbp)
	movl	%edx, -1548(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	80(%rax), %r14
	movq	%r14, %rdi
	subq	$37592, %r14
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -88(%rbp)
	je	.L417
	movq	%r13, %rdi
	movl	$1, %ebx
	leaq	-1536(%rbp), %r15
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L418
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L420:
	addq	$1, %rbx
	cmpq	$65, %rbx
	je	.L430
.L418:
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	%r15, %rdi
	movq	23(%rax), %rax
	movq	(%r12), %r14
	movq	%rax, -1536(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movslq	%ebx, %r14
	movl	%eax, %edx
	movq	-1536(%rbp), %rax
	leaq	-1(%rax), %rsi
	call	_ZN2v88internal14HeapObjectsMap14FindOrAddEntryEmjb@PLT
	movq	-1536(%rbp), %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	call	_ZN2v88internal17AllocationTracker15AddFunctionInfoENS0_18SharedFunctionInfoEj
	movq	%r13, %rdi
	movl	%eax, 68(%r12,%rbx,4)
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L420
	leaq	-4(,%r14,4), %rax
.L419:
	leaq	72(%r12,%rax), %r13
	leaq	68(%r12), %rbx
	leaq	24(%r12), %rdi
	cmpq	%r13, %rbx
	je	.L426
	.p2align 4,,10
	.p2align 3
.L427:
	movl	0(%r13), %esi
	subq	$4, %r13
	call	_ZN2v88internal19AllocationTraceNode14FindOrAddChildEj
	movq	%rax, %rdi
	cmpq	%r13, %rbx
	jne	.L427
.L426:
	addl	$1, 16(%rdi)
	movslq	-1548(%rbp), %rbx
	addq	$408, %r12
	addl	%ebx, 12(%rdi)
	movq	-1544(%rbp), %rsi
	movl	20(%rdi), %r13d
	movq	%r12, %rdi
	addq	%rsi, %rbx
	movq	%rbx, %rdx
	call	_ZN2v88internal17AddressToTraceMap11RemoveRangeEmm
	movq	%rbx, %xmm0
	movq	%r15, %rsi
	movq	%r12, %rdi
	movhps	-1544(%rbp), %xmm0
	movl	%r13d, -1520(%rbp)
	movaps	%xmm0, -1536(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal17AddressToTraceMap10RangeStackEESt10_Select1stIS6_ESt4lessImESaIS6_EE16_M_insert_uniqueIS6_EES0_ISt17_Rb_tree_iteratorIS6_EbEOT_
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L438
	addq	$1512, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L417:
	.cfi_restore_state
	cmpl	$5, 12616(%r14)
	je	.L421
	leaq	-1536(%rbp), %r15
.L425:
	movq	$-4, %rax
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L421:
	movl	400(%r12), %eax
	leaq	-1536(%rbp), %r15
	testl	%eax, %eax
	je	.L439
.L422:
	movl	%eax, 72(%r12)
	xorl	%eax, %eax
	jmp	.L419
.L439:
	movl	$40, %edi
	call	_Znwm@PLT
	movq	336(%r12), %rsi
	leaq	.LC7(%rip), %rcx
	movq	%rcx, 16(%rax)
	movabsq	$-4294967296, %rcx
	movq	%rsi, %rdx
	subq	328(%r12), %rdx
	movq	%rcx, 24(%rax)
	leaq	.LC14(%rip), %rcx
	sarq	$3, %rdx
	movl	$0, 8(%rax)
	movl	$-1, 32(%rax)
	movq	%rax, -1536(%rbp)
	movq	%rcx, (%rax)
	movl	%edx, 400(%r12)
	cmpq	344(%r12), %rsi
	je	.L423
	movq	%rax, (%rsi)
	addq	$8, 336(%r12)
.L424:
	movl	400(%r12), %eax
	testl	%eax, %eax
	je	.L425
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L430:
	movl	$252, %eax
	jmp	.L419
.L423:
	leaq	328(%r12), %rdi
	movq	%r15, %rdx
	call	_ZNSt6vectorIPN2v88internal17AllocationTracker12FunctionInfoESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L424
.L438:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18352:
	.size	_ZN2v88internal17AllocationTracker15AllocationEventEmi, .-_ZN2v88internal17AllocationTracker15AllocationEventEmi
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal19AllocationTraceNodeC2EPNS0_19AllocationTraceTreeEj,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal19AllocationTraceNodeC2EPNS0_19AllocationTraceTreeEj, @function
_GLOBAL__sub_I__ZN2v88internal19AllocationTraceNodeC2EPNS0_19AllocationTraceTreeEj:
.LFB22743:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22743:
	.size	_GLOBAL__sub_I__ZN2v88internal19AllocationTraceNodeC2EPNS0_19AllocationTraceTreeEj, .-_GLOBAL__sub_I__ZN2v88internal19AllocationTraceNodeC2EPNS0_19AllocationTraceTreeEj
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal19AllocationTraceNodeC2EPNS0_19AllocationTraceTreeEj
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.long	0
	.long	0
	.long	0
	.long	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
