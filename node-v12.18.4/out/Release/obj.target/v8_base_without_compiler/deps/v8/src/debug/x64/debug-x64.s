	.file	"debug-x64.cc"
	.text
	.section	.text._ZN2v88internal12DebugCodegen31GenerateHandleDebuggerStatementEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12DebugCodegen31GenerateHandleDebuggerStatementEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal12DebugCodegen31GenerateHandleDebuggerStatementEPNS0_14MacroAssemblerE, @function
_ZN2v88internal12DebugCodegen31GenerateHandleDebuggerStatementEPNS0_14MacroAssemblerE:
.LFB19462:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$17, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movzbl	536(%rdi), %ebx
	movb	$1, 536(%rdi)
	call	_ZN2v88internal14TurboAssembler10EnterFrameENS0_10StackFrame4TypeE@PLT
	movl	$83, %edi
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14MacroAssembler11CallRuntimeEPKNS0_7Runtime8FunctionEiNS0_14SaveFPRegsModeE@PLT
	movl	$17, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE@PLT
	movb	%bl, 536(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler15MaybeDropFramesEv@PLT
	popq	%rbx
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler3retEi@PLT
	.cfi_endproc
.LFE19462:
	.size	_ZN2v88internal12DebugCodegen31GenerateHandleDebuggerStatementEPNS0_14MacroAssemblerE, .-_ZN2v88internal12DebugCodegen31GenerateHandleDebuggerStatementEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal12DebugCodegen30GenerateFrameDropperTrampolineEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12DebugCodegen30GenerateFrameDropperTrampolineEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal12DebugCodegen30GenerateFrameDropperTrampolineEPNS0_14MacroAssemblerE, @function
_ZN2v88internal12DebugCodegen30GenerateFrameDropperTrampolineEPNS0_14MacroAssemblerE:
.LFB19463:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$80, %rsp
	movl	_ZN2v88internalL3rbxE(%rip), %r13d
	movl	_ZN2v88internalL3rbpE(%rip), %r14d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	%r14d, %esi
	movl	%r13d, %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	%r14d, %esi
	movl	$-16, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rdiE(%rip), %r14d
	movl	$8, %r8d
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5leaveEv@PLT
	movl	$23, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	leaq	-76(%rbp), %rdi
	movl	$41, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-76(%rbp), %rdx
	movl	-68(%rbp), %ecx
	movl	%r13d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rdx, -64(%rbp)
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterENS0_7OperandEi@PLT
	leaq	-108(%rbp), %rcx
	xorl	%eax, %eax
	movl	%r14d, %esi
	movl	_ZN2v88internalL6no_regE(%rip), %edx
	movq	%rcx, %r8
	movq	%r12, %rdi
	movw	%ax, -104(%rbp)
	movl	$1, %r9d
	movl	$3, -108(%rbp)
	call	_ZN2v88internal14MacroAssembler14InvokeFunctionENS0_8RegisterES2_RKNS0_14ParameterCountES5_10InvokeFlag@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L7
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L7:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19463:
	.size	_ZN2v88internal12DebugCodegen30GenerateFrameDropperTrampolineEPNS0_14MacroAssemblerE, .-_ZN2v88internal12DebugCodegen30GenerateFrameDropperTrampolineEPNS0_14MacroAssemblerE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal12DebugCodegen31GenerateHandleDebuggerStatementEPNS0_14MacroAssemblerE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal12DebugCodegen31GenerateHandleDebuggerStatementEPNS0_14MacroAssemblerE, @function
_GLOBAL__sub_I__ZN2v88internal12DebugCodegen31GenerateHandleDebuggerStatementEPNS0_14MacroAssemblerE:
.LFB23420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23420:
	.size	_GLOBAL__sub_I__ZN2v88internal12DebugCodegen31GenerateHandleDebuggerStatementEPNS0_14MacroAssemblerE, .-_GLOBAL__sub_I__ZN2v88internal12DebugCodegen31GenerateHandleDebuggerStatementEPNS0_14MacroAssemblerE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal12DebugCodegen31GenerateHandleDebuggerStatementEPNS0_14MacroAssemblerE
	.globl	_ZN2v88internal8LiveEdit22kFrameDropperSupportedE
	.section	.rodata._ZN2v88internal8LiveEdit22kFrameDropperSupportedE,"a"
	.type	_ZN2v88internal8LiveEdit22kFrameDropperSupportedE, @object
	.size	_ZN2v88internal8LiveEdit22kFrameDropperSupportedE, 1
_ZN2v88internal8LiveEdit22kFrameDropperSupportedE:
	.byte	1
	.section	.rodata._ZN2v88internalL6no_regE,"a"
	.align 4
	.type	_ZN2v88internalL6no_regE, @object
	.size	_ZN2v88internalL6no_regE, 4
_ZN2v88internalL6no_regE:
	.long	-1
	.section	.rodata._ZN2v88internalL3rdiE,"a"
	.align 4
	.type	_ZN2v88internalL3rdiE, @object
	.size	_ZN2v88internalL3rdiE, 4
_ZN2v88internalL3rdiE:
	.long	7
	.section	.rodata._ZN2v88internalL3rbpE,"a"
	.align 4
	.type	_ZN2v88internalL3rbpE, @object
	.size	_ZN2v88internalL3rbpE, 4
_ZN2v88internalL3rbpE:
	.long	5
	.section	.rodata._ZN2v88internalL3rbxE,"a"
	.align 4
	.type	_ZN2v88internalL3rbxE, @object
	.size	_ZN2v88internalL3rbxE, 4
_ZN2v88internalL3rbxE:
	.long	3
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
