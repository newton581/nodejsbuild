	.file	"asm-types.cc"
	.text
	.section	.text._ZN2v88internal4wasm15AsmCallableType14AsFunctionTypeEv,"axG",@progbits,_ZN2v88internal4wasm15AsmCallableType14AsFunctionTypeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15AsmCallableType14AsFunctionTypeEv
	.type	_ZN2v88internal4wasm15AsmCallableType14AsFunctionTypeEv, @function
_ZN2v88internal4wasm15AsmCallableType14AsFunctionTypeEv:
.LFB5512:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5512:
	.size	_ZN2v88internal4wasm15AsmCallableType14AsFunctionTypeEv, .-_ZN2v88internal4wasm15AsmCallableType14AsFunctionTypeEv
	.section	.text._ZN2v88internal4wasm15AsmCallableType24AsOverloadedFunctionTypeEv,"axG",@progbits,_ZN2v88internal4wasm15AsmCallableType24AsOverloadedFunctionTypeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15AsmCallableType24AsOverloadedFunctionTypeEv
	.type	_ZN2v88internal4wasm15AsmCallableType24AsOverloadedFunctionTypeEv, @function
_ZN2v88internal4wasm15AsmCallableType24AsOverloadedFunctionTypeEv:
.LFB5513:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5513:
	.size	_ZN2v88internal4wasm15AsmCallableType24AsOverloadedFunctionTypeEv, .-_ZN2v88internal4wasm15AsmCallableType24AsOverloadedFunctionTypeEv
	.section	.text._ZN2v88internal4wasm15AsmFunctionType14AsFunctionTypeEv,"axG",@progbits,_ZN2v88internal4wasm15AsmFunctionType14AsFunctionTypeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15AsmFunctionType14AsFunctionTypeEv
	.type	_ZN2v88internal4wasm15AsmFunctionType14AsFunctionTypeEv, @function
_ZN2v88internal4wasm15AsmFunctionType14AsFunctionTypeEv:
.LFB5514:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE5514:
	.size	_ZN2v88internal4wasm15AsmFunctionType14AsFunctionTypeEv, .-_ZN2v88internal4wasm15AsmFunctionType14AsFunctionTypeEv
	.section	.text._ZN2v88internal4wasm25AsmOverloadedFunctionType24AsOverloadedFunctionTypeEv,"axG",@progbits,_ZN2v88internal4wasm25AsmOverloadedFunctionType24AsOverloadedFunctionTypeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm25AsmOverloadedFunctionType24AsOverloadedFunctionTypeEv
	.type	_ZN2v88internal4wasm25AsmOverloadedFunctionType24AsOverloadedFunctionTypeEv, @function
_ZN2v88internal4wasm25AsmOverloadedFunctionType24AsOverloadedFunctionTypeEv:
.LFB5534:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE5534:
	.size	_ZN2v88internal4wasm25AsmOverloadedFunctionType24AsOverloadedFunctionTypeEv, .-_ZN2v88internal4wasm25AsmOverloadedFunctionType24AsOverloadedFunctionTypeEv
	.section	.text._ZN2v88internal4wasm15AsmCallableType3IsAEPNS1_7AsmTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15AsmCallableType3IsAEPNS1_7AsmTypeE
	.type	_ZN2v88internal4wasm15AsmCallableType3IsAEPNS1_7AsmTypeE, @function
_ZN2v88internal4wasm15AsmCallableType3IsAEPNS1_7AsmTypeE:
.LFB6786:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testb	$1, %sil
	je	.L9
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	cmpq	%rdi, %rsi
	sete	%al
	ret
	.cfi_endproc
.LFE6786:
	.size	_ZN2v88internal4wasm15AsmCallableType3IsAEPNS1_7AsmTypeE, .-_ZN2v88internal4wasm15AsmCallableType3IsAEPNS1_7AsmTypeE
	.section	.text.unlikely._ZN2v88internal4wasm25AsmOverloadedFunctionType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE,"ax",@progbits
	.align 2
.LCOLDB0:
	.section	.text._ZN2v88internal4wasm25AsmOverloadedFunctionType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE,"ax",@progbits
.LHOTB0:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm25AsmOverloadedFunctionType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE
	.type	_ZN2v88internal4wasm25AsmOverloadedFunctionType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE, @function
_ZN2v88internal4wasm25AsmOverloadedFunctionType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE:
.LFB6803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rdi), %rdx
	cmpq	24(%rdi), %rdx
	je	.L11
	movq	%rdi, %r14
	movq	%rsi, %r12
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L14:
	movq	(%rdx,%rbx,8), %rdi
	testb	$1, %dil
	jne	.L12
	movq	(%rdi), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	*8(%rax)
	testb	%al, %al
	jne	.L10
	movq	16(%r14), %rdx
	movq	24(%r14), %rax
	addq	$1, %rbx
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jb	.L14
.L11:
	xorl	%eax, %eax
.L10:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal4wasm25AsmOverloadedFunctionType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE
	.cfi_startproc
	.type	_ZN2v88internal4wasm25AsmOverloadedFunctionType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE.cold, @function
_ZN2v88internal4wasm25AsmOverloadedFunctionType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE.cold:
.LFSB6803:
.L12:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	0, %rax
	ud2
	.cfi_endproc
.LFE6803:
	.section	.text._ZN2v88internal4wasm25AsmOverloadedFunctionType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE
	.size	_ZN2v88internal4wasm25AsmOverloadedFunctionType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE, .-_ZN2v88internal4wasm25AsmOverloadedFunctionType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE
	.section	.text.unlikely._ZN2v88internal4wasm25AsmOverloadedFunctionType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE
	.size	_ZN2v88internal4wasm25AsmOverloadedFunctionType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE.cold, .-_ZN2v88internal4wasm25AsmOverloadedFunctionType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE.cold
.LCOLDE0:
	.section	.text._ZN2v88internal4wasm25AsmOverloadedFunctionType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE
.LHOTE0:
	.section	.text._ZN2v88internal4wasm25AsmOverloadedFunctionTypeD2Ev,"axG",@progbits,_ZN2v88internal4wasm25AsmOverloadedFunctionTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm25AsmOverloadedFunctionTypeD2Ev
	.type	_ZN2v88internal4wasm25AsmOverloadedFunctionTypeD2Ev, @function
_ZN2v88internal4wasm25AsmOverloadedFunctionTypeD2Ev:
.LFB7695:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7695:
	.size	_ZN2v88internal4wasm25AsmOverloadedFunctionTypeD2Ev, .-_ZN2v88internal4wasm25AsmOverloadedFunctionTypeD2Ev
	.weak	_ZN2v88internal4wasm25AsmOverloadedFunctionTypeD1Ev
	.set	_ZN2v88internal4wasm25AsmOverloadedFunctionTypeD1Ev,_ZN2v88internal4wasm25AsmOverloadedFunctionTypeD2Ev
	.section	.text._ZN2v88internal4wasm15AsmFunctionTypeD2Ev,"axG",@progbits,_ZN2v88internal4wasm15AsmFunctionTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15AsmFunctionTypeD2Ev
	.type	_ZN2v88internal4wasm15AsmFunctionTypeD2Ev, @function
_ZN2v88internal4wasm15AsmFunctionTypeD2Ev:
.LFB7699:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7699:
	.size	_ZN2v88internal4wasm15AsmFunctionTypeD2Ev, .-_ZN2v88internal4wasm15AsmFunctionTypeD2Ev
	.weak	_ZN2v88internal4wasm15AsmFunctionTypeD1Ev
	.set	_ZN2v88internal4wasm15AsmFunctionTypeD1Ev,_ZN2v88internal4wasm15AsmFunctionTypeD2Ev
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxTypeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxTypeD2Ev, @function
_ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxTypeD2Ev:
.LFB7703:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7703:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxTypeD2Ev, .-_ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxTypeD2Ev
	.set	_ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxTypeD1Ev,_ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxTypeD2Ev
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_113AsmFroundTypeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_113AsmFroundTypeD2Ev, @function
_ZN2v88internal4wasm12_GLOBAL__N_113AsmFroundTypeD2Ev:
.LFB7707:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7707:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_113AsmFroundTypeD2Ev, .-_ZN2v88internal4wasm12_GLOBAL__N_113AsmFroundTypeD2Ev
	.set	_ZN2v88internal4wasm12_GLOBAL__N_113AsmFroundTypeD1Ev,_ZN2v88internal4wasm12_GLOBAL__N_113AsmFroundTypeD2Ev
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxTypeD0Ev.str1.1,"aMS",@progbits,1
.LC1:
	.string	"unreachable code"
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxTypeD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxTypeD0Ev, @function
_ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxTypeD0Ev:
.LFB7705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE7705:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxTypeD0Ev, .-_ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxTypeD0Ev
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_113AsmFroundTypeD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_113AsmFroundTypeD0Ev, @function
_ZN2v88internal4wasm12_GLOBAL__N_113AsmFroundTypeD0Ev:
.LFB7709:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE7709:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_113AsmFroundTypeD0Ev, .-_ZN2v88internal4wasm12_GLOBAL__N_113AsmFroundTypeD0Ev
	.section	.text._ZN2v88internal4wasm25AsmOverloadedFunctionTypeD0Ev,"axG",@progbits,_ZN2v88internal4wasm25AsmOverloadedFunctionTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm25AsmOverloadedFunctionTypeD0Ev
	.type	_ZN2v88internal4wasm25AsmOverloadedFunctionTypeD0Ev, @function
_ZN2v88internal4wasm25AsmOverloadedFunctionTypeD0Ev:
.LFB7697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE7697:
	.size	_ZN2v88internal4wasm25AsmOverloadedFunctionTypeD0Ev, .-_ZN2v88internal4wasm25AsmOverloadedFunctionTypeD0Ev
	.section	.text._ZN2v88internal4wasm15AsmFunctionTypeD0Ev,"axG",@progbits,_ZN2v88internal4wasm15AsmFunctionTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15AsmFunctionTypeD0Ev
	.type	_ZN2v88internal4wasm15AsmFunctionTypeD0Ev, @function
_ZN2v88internal4wasm15AsmFunctionTypeD0Ev:
.LFB7701:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE7701:
	.size	_ZN2v88internal4wasm15AsmFunctionTypeD0Ev, .-_ZN2v88internal4wasm15AsmFunctionTypeD0Ev
	.section	.text._ZN2v88internal4wasm15AsmFunctionType3IsAEPNS1_7AsmTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15AsmFunctionType3IsAEPNS1_7AsmTypeE
	.type	_ZN2v88internal4wasm15AsmFunctionType3IsAEPNS1_7AsmTypeE, @function
_ZN2v88internal4wasm15AsmFunctionType3IsAEPNS1_7AsmTypeE:
.LFB6800:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	andl	$1, %ebx
	je	.L53
.L30:
	xorl	%eax, %eax
.L29:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rdi, %r12
	movq	%rsi, %rdi
	call	*16(%rax)
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L30
	movq	8(%rax), %rcx
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L54
	testb	$1, %al
	je	.L33
	testq	%rcx, %rcx
	je	.L30
	testb	$1, %cl
	je	.L30
	andl	$-2, %eax
	andl	$-2, %ecx
	cmpl	%ecx, %eax
	sete	%al
.L32:
	testb	%al, %al
	je	.L30
	movq	24(%r12), %rsi
	movq	32(%r12), %r8
	movq	24(%rdx), %rdi
	movq	32(%rdx), %rcx
	subq	%rsi, %r8
	subq	%rdi, %rcx
	cmpq	%r8, %rcx
	jne	.L30
	sarq	$3, %rcx
	jne	.L38
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L56:
	testq	%rdx, %rdx
	je	.L30
	testb	$1, %dl
	je	.L30
	andl	$-2, %eax
	andl	$-2, %edx
	cmpl	%edx, %eax
	sete	%al
.L36:
	testb	%al, %al
	je	.L30
	addq	$1, %rbx
	cmpq	%rcx, %rbx
	je	.L29
.L38:
	movq	(%rsi,%rbx,8), %rax
	movq	(%rdi,%rbx,8), %rdx
	testq	%rax, %rax
	je	.L55
	testb	$1, %al
	jne	.L56
	cmpq	%rax, %rdx
	sete	%al
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L33:
	cmpq	%rax, %rcx
	sete	%al
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L55:
	testq	%rdx, %rdx
	sete	%al
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L54:
	testq	%rcx, %rcx
	sete	%al
	jmp	.L32
	.cfi_endproc
.LFE6800:
	.size	_ZN2v88internal4wasm15AsmFunctionType3IsAEPNS1_7AsmTypeE, .-_ZN2v88internal4wasm15AsmFunctionType3IsAEPNS1_7AsmTypeE
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS5_EE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS5_EE, @function
_ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS5_EE:
.LFB6797:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L80
	testb	$1, %al
	je	.L60
	testb	$1, %sil
	je	.L61
	testq	%rsi, %rsi
	je	.L61
	andl	$-2, %eax
	andl	$-2, %esi
	cmpl	%esi, %eax
	sete	%al
.L59:
	testb	%al, %al
	je	.L61
	movq	8(%r13), %rsi
	movq	16(%r13), %rax
	subq	%rsi, %rax
	cmpq	$15, %rax
	jbe	.L61
	xorl	%ebx, %ebx
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L81:
	testq	%rsi, %rsi
	je	.L61
	testb	$1, %sil
	je	.L61
	andl	$-2, %esi
	andl	%esi, %edi
	cmpl	%esi, %edi
	sete	%al
.L63:
	testb	%al, %al
	je	.L61
	movq	8(%r13), %rsi
	movq	16(%r13), %rcx
	addq	$1, %rbx
	subq	%rsi, %rcx
	sarq	$3, %rcx
	cmpq	%rbx, %rcx
	jbe	.L57
.L64:
	movq	(%rsi,%rbx,8), %rdi
	movq	16(%r12), %rsi
	testb	$1, %dil
	jne	.L81
	movq	(%rdi), %rax
	call	*48(%rax)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L60:
	cmpq	%rsi, %rax
	sete	%al
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L61:
	xorl	%eax, %eax
.L57:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	testq	%rsi, %rsi
	sete	%al
	jmp	.L59
	.cfi_endproc
.LFE6797:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS5_EE, .-_ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS5_EE
	.section	.text._ZN2v88internal4wasm15AsmFunctionType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15AsmFunctionType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE
	.type	_ZN2v88internal4wasm15AsmFunctionType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE, @function
_ZN2v88internal4wasm15AsmFunctionType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE:
.LFB6801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	testq	%rsi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	sete	%r14b
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L84
	testb	$1, %al
	je	.L85
	testb	$1, %sil
	je	.L86
	testq	%rsi, %rsi
	je	.L86
	andl	$-2, %eax
	andl	$-2, %esi
	cmpl	%esi, %eax
	sete	%r14b
.L84:
	testb	%r14b, %r14b
	je	.L86
	movq	24(%r12), %rcx
	movq	32(%r12), %rsi
	movq	8(%r13), %rax
	movq	16(%r13), %rdx
	subq	%rcx, %rsi
	subq	%rax, %rdx
	cmpq	%rsi, %rdx
	jne	.L86
	xorl	%ebx, %ebx
	testq	%rdx, %rdx
	jne	.L87
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L107:
	testq	%rsi, %rsi
	je	.L86
	testb	$1, %sil
	je	.L86
	andl	$-2, %esi
	andl	%esi, %edi
	cmpl	%esi, %edi
	sete	%al
.L90:
	testb	%al, %al
	je	.L86
	movq	24(%r12), %rcx
	movq	32(%r12), %rax
	addq	$1, %rbx
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rbx, %rax
	jbe	.L82
	movq	8(%r13), %rax
.L87:
	movq	(%rax,%rbx,8), %rdi
	movq	(%rcx,%rbx,8), %rsi
	testb	$1, %dil
	jne	.L107
	movq	(%rdi), %rax
	call	*48(%rax)
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L86:
	xorl	%r14d, %r14d
.L82:
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	cmpq	%rsi, %rax
	sete	%r14b
	jmp	.L84
	.cfi_endproc
.LFE6801:
	.size	_ZN2v88internal4wasm15AsmFunctionType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE, .-_ZN2v88internal4wasm15AsmFunctionType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_113AsmFroundType4NameEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_113AsmFroundType4NameEv, @function
_ZN2v88internal4wasm12_GLOBAL__N_113AsmFroundType4NameEv:
.LFB6791:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1970238054, 16(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$25710, %edx
	movw	%dx, 20(%rdi)
	movq	$6, 8(%rdi)
	movb	$0, 22(%rdi)
	ret
	.cfi_endproc
.LFE6791:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_113AsmFroundType4NameEv, .-_ZN2v88internal4wasm12_GLOBAL__N_113AsmFroundType4NameEv
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_113AsmFroundType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS5_EE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_113AsmFroundType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS5_EE, @function
_ZN2v88internal4wasm12_GLOBAL__N_113AsmFroundType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS5_EE:
.LFB6793:
	.cfi_startproc
	endbr64
	movq	8(%rdx), %rcx
	movq	16(%rdx), %rax
	xorl	%r8d, %r8d
	subq	%rcx, %rax
	cmpq	$8, %rax
	je	.L136
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rcx), %r12
	testb	$1, %r12b
	je	.L111
	movl	%r12d, %eax
	andl	$8196, %eax
	cmpl	$8196, %eax
	je	.L113
	movl	%r12d, %eax
	andl	$76, %eax
	cmpl	$76, %eax
	je	.L113
	movl	%r12d, %eax
	andl	$1824, %eax
	cmpl	$1824, %eax
	je	.L113
	andl	$2816, %r12d
	cmpl	$2816, %r12d
	sete	%r8b
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L111:
	movq	(%r12), %rax
	movl	$8197, %esi
	movq	%r12, %rdi
	call	*48(%rax)
	testb	%al, %al
	je	.L137
.L113:
	movl	$1, %r8d
.L109:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movq	(%r12), %rax
	movl	$77, %esi
	movq	%r12, %rdi
	call	*48(%rax)
	testb	%al, %al
	jne	.L113
	movq	(%r12), %rax
	movl	$1825, %esi
	movq	%r12, %rdi
	call	*48(%rax)
	testb	%al, %al
	jne	.L113
	movq	(%r12), %rax
	movq	%r12, %rdi
	movl	$2817, %esi
	movq	48(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE6793:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_113AsmFroundType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS5_EE, .-_ZN2v88internal4wasm12_GLOBAL__N_113AsmFroundType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS5_EE
	.section	.rodata._ZN2v88internal4wasm25AsmOverloadedFunctionType4NameB5cxx11Ev.str1.1,"aMS",@progbits,1
.LC2:
	.string	"basic_string::append"
.LC3:
	.string	" /\\ "
	.section	.text._ZN2v88internal4wasm25AsmOverloadedFunctionType4NameB5cxx11Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm25AsmOverloadedFunctionType4NameB5cxx11Ev
	.type	_ZN2v88internal4wasm25AsmOverloadedFunctionType4NameB5cxx11Ev, @function
_ZN2v88internal4wasm25AsmOverloadedFunctionType4NameB5cxx11Ev:
.LFB6802:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movabsq	$7526475350838045798, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 16(%rdi)
	movq	16(%rsi), %rdx
	cmpq	24(%rsi), %rdx
	je	.L138
	.p2align 4,,10
	.p2align 3
.L139:
	movq	(%rdx,%rbx,8), %rsi
	testb	$1, %sil
	je	.L142
.L196:
	andl	$-2, %esi
	cmpl	$8196, %esi
	je	.L143
	ja	.L144
	cmpl	$236, %esi
	je	.L145
	ja	.L146
	cmpl	$32, %esi
	ja	.L147
	cmpl	$1, %esi
	jbe	.L148
	cmpl	$32, %esi
	ja	.L148
	leaq	.L150(%rip), %rdx
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm25AsmOverloadedFunctionType4NameB5cxx11Ev,"a",@progbits
	.align 4
	.align 4
.L150:
	.long	.L148-.L150
	.long	.L148-.L150
	.long	.L154-.L150
	.long	.L148-.L150
	.long	.L153-.L150
	.long	.L148-.L150
	.long	.L148-.L150
	.long	.L148-.L150
	.long	.L152-.L150
	.long	.L148-.L150
	.long	.L148-.L150
	.long	.L148-.L150
	.long	.L148-.L150
	.long	.L148-.L150
	.long	.L148-.L150
	.long	.L148-.L150
	.long	.L151-.L150
	.long	.L148-.L150
	.long	.L148-.L150
	.long	.L148-.L150
	.long	.L148-.L150
	.long	.L148-.L150
	.long	.L148-.L150
	.long	.L148-.L150
	.long	.L148-.L150
	.long	.L148-.L150
	.long	.L148-.L150
	.long	.L148-.L150
	.long	.L148-.L150
	.long	.L148-.L150
	.long	.L148-.L150
	.long	.L148-.L150
	.long	.L149-.L150
	.section	.text._ZN2v88internal4wasm25AsmOverloadedFunctionType4NameB5cxx11Ev
	.p2align 4,,10
	.p2align 3
.L144:
	cmpl	$524290, %esi
	je	.L162
	jbe	.L192
	cmpl	$4194306, %esi
	je	.L170
	jbe	.L193
	cmpl	$8388610, %esi
	jne	.L194
	movabsq	$4698440184733068358, %rax
	leaq	-80(%rbp), %rcx
	movq	%rcx, -96(%rbp)
.L188:
	movq	%rax, -80(%rbp)
	movq	%rcx, %r14
	movq	%rcx, %rsi
	movl	$12, %edx
	movl	$2036429426, -72(%rbp)
	movq	$12, -88(%rbp)
	movb	$0, -68(%rbp)
	.p2align 4,,10
	.p2align 3
.L176:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L177
	call	_ZdlPv@PLT
.L177:
	movq	16(%r12), %rdx
	movq	24(%r12), %rax
	addq	$1, %rbx
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jnb	.L138
	testq	%rbx, %rbx
	je	.L139
	movabsq	$4611686018427387903, %rax
	subq	8(%r13), %rax
	cmpq	$3, %rax
	jbe	.L195
	movl	$4, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	16(%r12), %rdx
	movq	(%rdx,%rbx,8), %rsi
	testb	$1, %sil
	jne	.L196
.L142:
	movq	(%rsi), %rax
	leaq	-96(%rbp), %rdi
	leaq	-80(%rbp), %r14
	call	*(%rax)
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L147:
	cmpl	$76, %esi
	jne	.L148
	leaq	-80(%rbp), %rcx
	movl	$25964, %eax
	movb	$63, -74(%rbp)
	movl	$7, %edx
	movq	%rcx, -96(%rbp)
	movq	%rcx, %r14
	movq	%rcx, %rsi
	movl	$1651863396, -80(%rbp)
	movw	%ax, -76(%rbp)
	movq	$7, -88(%rbp)
	movb	$0, -73(%rbp)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L146:
	cmpl	$1824, %esi
	je	.L156
	jbe	.L197
	cmpl	$2816, %esi
	jne	.L198
	movabsq	$7234309766870429301, %rax
	leaq	-80(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	%rax, -80(%rbp)
.L189:
	movb	$0, -72(%rbp)
	movq	%rcx, %r14
	movq	%rcx, %rsi
	movl	$8, %edx
	movq	$8, -88(%rbp)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L194:
	cmpl	$-2147483648, %esi
	jne	.L148
	leaq	-80(%rbp), %rcx
	movl	$15973, %eax
	movl	$1852796476, -80(%rbp)
	movq	%rcx, -96(%rbp)
	movw	%ax, -76(%rbp)
	.p2align 4,,10
	.p2align 3
.L187:
	movb	$0, -74(%rbp)
	movq	%rcx, %r14
	movq	%rcx, %rsi
	movl	$6, %edx
	movq	$6, -88(%rbp)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L198:
	cmpl	$7968, %esi
	jne	.L148
	leaq	-80(%rbp), %rcx
	movl	$28021, %r8d
	movl	$1853385062, -80(%rbp)
	movq	%rcx, -96(%rbp)
	movw	%r8w, -76(%rbp)
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L197:
	cmpl	$256, %esi
	jne	.L199
	leaq	-80(%rbp), %rcx
	movl	$26739, %r11d
	movl	$1769238121, -80(%rbp)
	movq	%rcx, -96(%rbp)
	movw	%r11w, -76(%rbp)
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L193:
	cmpl	$1048578, %esi
	jne	.L200
	movabsq	$8232916790388091221, %rax
	leaq	-80(%rbp), %rcx
	movq	%rcx, -96(%rbp)
.L191:
	movl	$24946, %esi
	movq	%rax, -80(%rbp)
	movq	%rcx, %r14
	movl	$11, %edx
	movw	%si, -72(%rbp)
	movq	%rcx, %rsi
	movb	$121, -70(%rbp)
	movq	$11, -88(%rbp)
	movb	$0, -69(%rbp)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L192:
	cmpl	$65538, %esi
	je	.L164
	jbe	.L201
	cmpl	$131074, %esi
	jne	.L202
	leaq	-80(%rbp), %rcx
	movb	$121, -72(%rbp)
	movabsq	$7021800393469619785, %rax
	movl	$9, %edx
	movq	%rcx, -96(%rbp)
	movq	%rcx, %r14
	movq	%rcx, %rsi
	movq	%rax, -80(%rbp)
	movq	$9, -88(%rbp)
	movb	$0, -71(%rbp)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L199:
	cmpl	$768, %esi
	jne	.L148
	leaq	-80(%rbp), %rcx
	movl	$28265, %r10d
	movb	$116, -78(%rbp)
	movl	$3, %edx
	movq	%rcx, -96(%rbp)
	movq	%rcx, %r14
	movq	%rcx, %rsi
	movw	%r10w, -80(%rbp)
	movq	$3, -88(%rbp)
	movb	$0, -77(%rbp)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L200:
	cmpl	$2097154, %esi
	jne	.L148
	movabsq	$8246725551536238153, %rax
	leaq	-80(%rbp), %rcx
	movq	%rcx, -96(%rbp)
.L190:
	movl	$31073, %edx
	movq	%rax, -80(%rbp)
	movq	%rcx, %r14
	movq	%rcx, %rsi
	movw	%dx, -72(%rbp)
	movl	$10, %edx
	movq	$10, -88(%rbp)
	movb	$0, -70(%rbp)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L202:
	cmpl	$262146, %esi
	jne	.L148
	movabsq	$8232921179844667733, %rax
	leaq	-80(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L201:
	cmpl	$24588, %esi
	jne	.L203
	leaq	-80(%rbp), %rcx
	movl	$16244, %edi
	movl	$1634692198, -80(%rbp)
	movq	%rcx, -96(%rbp)
	movw	%di, -76(%rbp)
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L203:
	cmpl	$57356, %esi
	jne	.L148
	leaq	-80(%rbp), %rcx
	movl	$1634692198, -80(%rbp)
	movl	$5, %edx
	movq	%rcx, -96(%rbp)
	movq	%rcx, %r14
	movq	%rcx, %rsi
	movb	$116, -76(%rbp)
	movq	$5, -88(%rbp)
	movb	$0, -75(%rbp)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L138:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L204
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L149:
	.cfi_restore_state
	leaq	-80(%rbp), %rcx
	movl	$28274, %eax
	movl	$1702131813, -80(%rbp)
	movq	%rcx, -96(%rbp)
	movw	%ax, -76(%rbp)
	jmp	.L187
.L154:
	leaq	-80(%rbp), %rcx
	movl	$23899, %eax
	movb	$0, -78(%rbp)
	movl	$2, %edx
	movq	%rcx, -96(%rbp)
	movq	%rcx, %r14
	movq	%rcx, %rsi
	movw	%ax, -80(%rbp)
	movq	$2, -88(%rbp)
	jmp	.L176
.L153:
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	leaq	-96(%rbp), %rdi
	movq	$16, -104(%rbp)
	leaq	-80(%rbp), %r14
	movq	%r14, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	movdqa	.LC4(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movups	%xmm0, (%rax)
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	jmp	.L176
.L152:
	leaq	-80(%rbp), %rcx
	movl	$1818391919, -72(%rbp)
	movabsq	$7240732069988363366, %rax
	movl	$14, %edx
	movq	%rax, -80(%rbp)
	movl	$16229, %eax
	movq	%rcx, %r14
	movq	%rcx, %rsi
	movq	%rcx, -96(%rbp)
	movw	%ax, -68(%rbp)
	movq	$14, -88(%rbp)
	movb	$0, -66(%rbp)
	jmp	.L176
.L151:
	leaq	-80(%rbp), %rcx
	movl	$1684631414, -80(%rbp)
	movl	$4, %edx
	movq	%rcx, -96(%rbp)
	movq	%rcx, %r14
	movq	%rcx, %rsi
	movq	$4, -88(%rbp)
	movb	$0, -76(%rbp)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L156:
	leaq	-80(%rbp), %rcx
	movl	$25701, %r9d
	movl	$1852270963, -80(%rbp)
	movq	%rcx, -96(%rbp)
	movw	%r9w, -76(%rbp)
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L162:
	movabsq	$8246725568682552905, %rax
	leaq	-80(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L170:
	movabsq	$4697873936244763718, %rax
	leaq	-80(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L164:
	movabsq	$8246725578396166485, %rax
	leaq	-80(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L145:
	leaq	-80(%rbp), %rcx
	movl	$25964, %r14d
	movl	$1651863396, -80(%rbp)
	movq	%rcx, -96(%rbp)
	movw	%r14w, -76(%rbp)
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L143:
	leaq	-80(%rbp), %rcx
	movq	%r15, -80(%rbp)
	movq	%rcx, -96(%rbp)
	jmp	.L189
.L148:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L195:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L204:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6802:
	.size	_ZN2v88internal4wasm25AsmOverloadedFunctionType4NameB5cxx11Ev, .-_ZN2v88internal4wasm25AsmOverloadedFunctionType4NameB5cxx11Ev
	.section	.text._ZN2v88internal4wasm7AsmType14AsCallableTypeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm7AsmType14AsCallableTypeEv
	.type	_ZN2v88internal4wasm7AsmType14AsCallableTypeEv, @function
_ZN2v88internal4wasm7AsmType14AsCallableTypeEv:
.LFB6779:
	.cfi_startproc
	endbr64
	testb	$1, %dil
	movl	$0, %eax
	cmove	%rdi, %rax
	ret
	.cfi_endproc
.LFE6779:
	.size	_ZN2v88internal4wasm7AsmType14AsCallableTypeEv, .-_ZN2v88internal4wasm7AsmType14AsCallableTypeEv
	.section	.text._ZN2v88internal4wasm7AsmType4NameB5cxx11Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm7AsmType4NameB5cxx11Ev
	.type	_ZN2v88internal4wasm7AsmType4NameB5cxx11Ev, @function
_ZN2v88internal4wasm7AsmType4NameB5cxx11Ev:
.LFB6780:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testb	$1, %sil
	je	.L209
	andl	$-2, %esi
	cmpl	$8196, %esi
	je	.L210
	ja	.L211
	cmpl	$236, %esi
	je	.L212
	ja	.L213
	cmpl	$32, %esi
	ja	.L214
	cmpl	$1, %esi
	jbe	.L215
	cmpl	$32, %esi
	ja	.L215
	leaq	.L217(%rip), %rdx
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm7AsmType4NameB5cxx11Ev,"a",@progbits
	.align 4
	.align 4
.L217:
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L221-.L217
	.long	.L215-.L217
	.long	.L220-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L219-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L218-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L216-.L217
	.section	.text._ZN2v88internal4wasm7AsmType4NameB5cxx11Ev
	.p2align 4,,10
	.p2align 3
.L213:
	cmpl	$1824, %esi
	je	.L223
	jbe	.L247
	cmpl	$2816, %esi
	je	.L227
	cmpl	$7968, %esi
	jne	.L215
	leaq	16(%rdi), %rax
	movl	$28021, %r9d
	movb	$0, 22(%rdi)
	movq	%rax, (%rdi)
	movl	$1853385062, 16(%rdi)
	movw	%r9w, 20(%rdi)
	movq	$6, 8(%rdi)
	.p2align 4,,10
	.p2align 3
.L208:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L248
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore_state
	cmpl	$524290, %esi
	je	.L229
	jbe	.L249
	cmpl	$4194306, %esi
	je	.L237
	jbe	.L250
	cmpl	$8388610, %esi
	je	.L241
	cmpl	$-2147483648, %esi
	jne	.L215
	leaq	16(%rdi), %rax
	movl	$1852796476, 16(%rdi)
	movq	%rax, (%rdi)
	movl	$15973, %eax
	movw	%ax, 20(%rdi)
	movq	$6, 8(%rdi)
	movb	$0, 22(%rdi)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L249:
	cmpl	$65538, %esi
	je	.L231
	jbe	.L251
	cmpl	$131074, %esi
	je	.L235
	cmpl	$262146, %esi
	jne	.L215
	movabsq	$8232921179844667733, %rcx
	leaq	16(%rdi), %rax
	movq	%rax, (%rdi)
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L214:
	cmpl	$76, %esi
	jne	.L215
	leaq	16(%rdi), %rax
	movl	$1651863396, 16(%rdi)
	movq	%rax, (%rdi)
	movl	$25964, %eax
	movw	%ax, 20(%rdi)
	movb	$63, 22(%rdi)
	movq	$7, 8(%rdi)
	movb	$0, 23(%rdi)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L250:
	cmpl	$1048578, %esi
	je	.L239
	cmpl	$2097154, %esi
	jne	.L215
	leaq	16(%rdi), %rax
	movl	$31073, %edx
	movb	$0, 26(%rdi)
	movabsq	$8246725551536238153, %rcx
	movq	%rax, (%rdi)
	movq	%rcx, 16(%rdi)
	movw	%dx, 24(%rdi)
	movq	$10, 8(%rdi)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L251:
	cmpl	$24588, %esi
	je	.L233
	cmpl	$57356, %esi
	jne	.L215
	leaq	16(%rdi), %rax
	movl	$1634692198, 16(%rdi)
	movq	%rax, (%rdi)
	movb	$116, 20(%rdi)
	movq	$5, 8(%rdi)
	movb	$0, 21(%rdi)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L247:
	cmpl	$256, %esi
	je	.L225
	cmpl	$768, %esi
	jne	.L215
	leaq	16(%rdi), %rax
	movl	$28265, %r11d
	movb	$116, 18(%rdi)
	movq	%rax, (%rdi)
	movw	%r11w, 16(%rdi)
	movq	$3, 8(%rdi)
	movb	$0, 19(%rdi)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L209:
	movq	(%rsi), %rax
	call	*(%rax)
	jmp	.L208
.L216:
	leaq	16(%rdi), %rax
	movl	$1702131813, 16(%rdi)
	movq	%rax, (%rdi)
	movl	$28274, %eax
	movw	%ax, 20(%rdi)
	movq	$6, 8(%rdi)
	movb	$0, 22(%rdi)
	jmp	.L208
.L218:
	leaq	16(%rdi), %rax
	movl	$1684631414, 16(%rdi)
	movq	%rax, (%rdi)
	movq	$4, 8(%rdi)
	movb	$0, 20(%rdi)
	jmp	.L208
.L219:
	leaq	16(%rdi), %rax
	movl	$1818391919, 24(%rdi)
	movabsq	$7240732069988363366, %rcx
	movq	%rax, (%rdi)
	movl	$16229, %eax
	movq	%rcx, 16(%rdi)
	movw	%ax, 28(%rdi)
	movq	$14, 8(%rdi)
	movb	$0, 30(%rdi)
	jmp	.L208
.L220:
	leaq	16(%rdi), %rax
	xorl	%edx, %edx
	leaq	-32(%rbp), %rsi
	movq	$16, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC4(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movups	%xmm0, (%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	jmp	.L208
.L221:
	leaq	16(%rdi), %rax
	movl	$23899, %edx
	movq	$2, 8(%rdi)
	movq	%rax, (%rdi)
	movw	%dx, 16(%rdi)
	movb	$0, 18(%rdi)
	jmp	.L208
.L215:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L227:
	leaq	16(%rdi), %rax
	movq	$8, 8(%rdi)
	movq	%rax, (%rdi)
	movabsq	$7234309766870429301, %rax
	movq	%rax, 16(%rdi)
	movb	$0, 24(%rdi)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L225:
	leaq	16(%rdi), %rax
	movl	$1769238121, 16(%rdi)
	movq	%rax, (%rdi)
	movl	$26739, %eax
	movw	%ax, 20(%rdi)
	movq	$6, 8(%rdi)
	movb	$0, 22(%rdi)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L233:
	leaq	16(%rdi), %rax
	movl	$16244, %r8d
	movb	$0, 22(%rdi)
	movq	%rax, (%rdi)
	movl	$1634692198, 16(%rdi)
	movw	%r8w, 20(%rdi)
	movq	$6, 8(%rdi)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L239:
	movabsq	$8232916790388091221, %rcx
	leaq	16(%rdi), %rax
	movq	%rax, (%rdi)
.L246:
	movq	%rcx, 16(%r12)
	movl	$24946, %ecx
	movw	%cx, 24(%r12)
	movb	$121, 26(%r12)
	movq	$11, 8(%r12)
	movb	$0, 27(%r12)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L235:
	leaq	16(%rdi), %rax
	movb	$121, 24(%rdi)
	movabsq	$7021800393469619785, %rcx
	movq	%rax, (%rdi)
	movq	%rcx, 16(%rdi)
	movq	$9, 8(%rdi)
	movb	$0, 25(%rdi)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L241:
	leaq	16(%rdi), %rax
	movl	$2036429426, 24(%rdi)
	movabsq	$4698440184733068358, %rcx
	movq	%rax, (%rdi)
	movq	%rcx, 16(%rdi)
	movq	$12, 8(%rdi)
	movb	$0, 28(%rdi)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L237:
	leaq	16(%rdi), %rax
	movl	$2036429426, 24(%rdi)
	movabsq	$4697873936244763718, %rcx
	movq	%rax, (%rdi)
	movq	%rcx, 16(%rdi)
	movq	$12, 8(%rdi)
	movb	$0, 28(%rdi)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L223:
	leaq	16(%rdi), %rax
	movl	$25701, %r10d
	movb	$0, 22(%rdi)
	movq	%rax, (%rdi)
	movl	$1852270963, 16(%rdi)
	movw	%r10w, 20(%rdi)
	movq	$6, 8(%rdi)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L231:
	movabsq	$8246725578396166485, %rcx
	leaq	16(%rdi), %rax
	movq	%rax, (%rdi)
	movq	%rcx, 16(%rdi)
	movl	$31073, %edi
	movw	%di, 24(%r12)
	movq	$10, 8(%r12)
	movb	$0, 26(%r12)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L210:
	leaq	16(%rdi), %rax
	movq	$8, 8(%rdi)
	movq	%rax, (%rdi)
	movabsq	$7526475350838045798, %rax
	movq	%rax, 16(%rdi)
	movb	$0, 24(%rdi)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L229:
	leaq	16(%rdi), %rax
	movl	$31073, %esi
	movb	$0, 26(%rdi)
	movabsq	$8246725568682552905, %rcx
	movq	%rax, (%rdi)
	movq	%rcx, 16(%rdi)
	movw	%si, 24(%rdi)
	movq	$10, 8(%rdi)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L212:
	leaq	16(%rdi), %rax
	movl	$1651863396, 16(%rdi)
	movq	%rax, (%rdi)
	movl	$25964, %eax
	movw	%ax, 20(%rdi)
	movq	$6, 8(%rdi)
	movb	$0, 22(%rdi)
	jmp	.L208
.L248:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6780:
	.size	_ZN2v88internal4wasm7AsmType4NameB5cxx11Ev, .-_ZN2v88internal4wasm7AsmType4NameB5cxx11Ev
	.section	.rodata._ZN2v88internal4wasm15AsmFunctionType4NameB5cxx11Ev.str1.1,"aMS",@progbits,1
.LC5:
	.string	"("
.LC6:
	.string	", "
.LC7:
	.string	") -> "
	.section	.text._ZN2v88internal4wasm15AsmFunctionType4NameB5cxx11Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15AsmFunctionType4NameB5cxx11Ev
	.type	_ZN2v88internal4wasm15AsmFunctionType4NameB5cxx11Ev, @function
_ZN2v88internal4wasm15AsmFunctionType4NameB5cxx11Ev:
.LFB6787:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	.LC5(%rip), %rsi
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 16(%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	24(%r14), %rdx
	cmpq	%rdx, 32(%r14)
	je	.L259
	.p2align 4,,10
	.p2align 3
.L253:
	movq	(%rdx,%r15,8), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7AsmType4NameB5cxx11Ev
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L256
	call	_ZdlPv@PLT
.L256:
	movq	24(%r14), %rdx
	movq	32(%r14), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	leaq	-1(%rax), %rsi
	cmpq	%r15, %rsi
	je	.L257
	movabsq	$4611686018427387903, %rax
	subq	8(%r12), %rax
	cmpq	$1, %rax
	jbe	.L254
	movl	$2, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	24(%r14), %rdx
	movq	32(%r14), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
.L257:
	addq	$1, %r15
	cmpq	%rax, %r15
	jb	.L253
.L259:
	movabsq	$4611686018427387903, %rax
	subq	8(%r12), %rax
	cmpq	$4, %rax
	jbe	.L254
	movl	$5, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	8(%r14), %rsi
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal4wasm7AsmType4NameB5cxx11Ev
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L252
	call	_ZdlPv@PLT
.L252:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L268
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L254:
	.cfi_restore_state
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L268:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6787:
	.size	_ZN2v88internal4wasm15AsmFunctionType4NameB5cxx11Ev, .-_ZN2v88internal4wasm15AsmFunctionType4NameB5cxx11Ev
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxType4NameEv.str1.1,"aMS",@progbits,1
.LC8:
	.string	"...) -> "
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxType4NameEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxType4NameEv, @function
_ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxType4NameEv:
.LFB6798:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-288(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$264, %rsp
	movq	8(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7AsmType4NameB5cxx11Ev
	movq	16(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType4NameB5cxx11Ev
	movq	16(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7AsmType4NameB5cxx11Ev
	leaq	.LC5(%rip), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	$1, %r8d
	leaq	-240(%rbp), %r13
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	%r13, -256(%rbp)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L305
	movq	%rcx, -256(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -240(%rbp)
.L271:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -248(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movabsq	$4611686018427387903, %rax
	subq	-248(%rbp), %rax
	cmpq	$1, %rax
	jbe	.L281
	movl	$2, %edx
	leaq	-256(%rbp), %rdi
	leaq	.LC6(%rip), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	leaq	-208(%rbp), %rbx
	movq	%rbx, -224(%rbp)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L306
	movq	%rcx, -224(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -208(%rbp)
.L274:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -216(%rbp)
	movq	%rdx, (%rax)
	movq	-224(%rbp), %r9
	movq	$0, 8(%rax)
	movl	$15, %eax
	movq	-216(%rbp), %r8
	movq	-184(%rbp), %rdx
	cmpq	%rbx, %r9
	movq	%rax, %rdi
	cmovne	-208(%rbp), %rdi
	movq	-192(%rbp), %rsi
	leaq	(%r8,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L276
	leaq	-176(%rbp), %rdi
	cmpq	%rdi, %rsi
	cmovne	-176(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L307
.L276:
	leaq	-224(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L278:
	leaq	-144(%rbp), %r14
	leaq	16(%rax), %rdx
	movq	%r14, -160(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L308
	movq	%rcx, -160(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -144(%rbp)
.L280:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -152(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movabsq	$4611686018427387903, %rax
	subq	-152(%rbp), %rax
	cmpq	$7, %rax
	jbe	.L281
	movl	$8, %edx
	leaq	-160(%rbp), %rdi
	leaq	.LC8(%rip), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	leaq	-112(%rbp), %r9
	movq	%r9, -128(%rbp)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L309
	movq	%rcx, -128(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -112(%rbp)
.L283:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -120(%rbp)
	movq	%rdx, (%rax)
	movq	-128(%rbp), %r10
	movq	$0, 8(%rax)
	movl	$15, %eax
	movq	-120(%rbp), %r8
	movq	-88(%rbp), %rdx
	cmpq	%r9, %r10
	movq	%rax, %rdi
	cmovne	-112(%rbp), %rdi
	movq	-96(%rbp), %rsi
	leaq	(%r8,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L310
	leaq	-80(%rbp), %rdi
	cmpq	%rdi, %rsi
	cmovne	-80(%rbp), %rax
	movq	%rdi, -296(%rbp)
	cmpq	%rax, %rcx
	jbe	.L311
.L285:
	leaq	-128(%rbp), %rdi
	movq	%r9, -304(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-304(%rbp), %r9
.L287:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L312
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L289:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	-128(%rbp), %rdi
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	cmpq	%r9, %rdi
	je	.L290
	call	_ZdlPv@PLT
.L290:
	movq	-160(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L291
	call	_ZdlPv@PLT
.L291:
	movq	-224(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L292
	call	_ZdlPv@PLT
.L292:
	movq	-256(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L293
	call	_ZdlPv@PLT
.L293:
	movq	-288(%rbp), %rdi
	leaq	-272(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L294
	call	_ZdlPv@PLT
.L294:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L295
	call	_ZdlPv@PLT
.L295:
	movq	-96(%rbp), %rdi
	cmpq	-296(%rbp), %rdi
	je	.L269
	call	_ZdlPv@PLT
.L269:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L313
	addq	$264, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L307:
	.cfi_restore_state
	movq	%r9, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L311:
	movq	%r10, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%r9, -304(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-304(%rbp), %r9
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L310:
	leaq	-80(%rbp), %rax
	movq	%rax, -296(%rbp)
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L305:
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, -240(%rbp)
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L308:
	movdqu	16(%rax), %xmm2
	movaps	%xmm2, -144(%rbp)
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L306:
	movdqu	16(%rax), %xmm1
	movaps	%xmm1, -208(%rbp)
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L309:
	movdqu	16(%rax), %xmm3
	movaps	%xmm3, -112(%rbp)
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L312:
	movdqu	16(%rax), %xmm4
	movups	%xmm4, 16(%r12)
	jmp	.L289
.L281:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L313:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6798:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxType4NameEv, .-_ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxType4NameEv
	.section	.text._ZN2v88internal4wasm7AsmType9IsExactlyEPS2_S3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm7AsmType9IsExactlyEPS2_S3_
	.type	_ZN2v88internal4wasm7AsmType9IsExactlyEPS2_S3_, @function
_ZN2v88internal4wasm7AsmType9IsExactlyEPS2_S3_:
.LFB6781:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L319
	testb	$1, %dil
	je	.L317
	testb	$1, %sil
	je	.L318
	testq	%rsi, %rsi
	je	.L318
	andl	$-2, %edi
	andl	$-2, %esi
	cmpl	%esi, %edi
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L317:
	cmpq	%rsi, %rdi
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L318:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	testq	%rsi, %rsi
	sete	%al
	ret
	.cfi_endproc
.LFE6781:
	.size	_ZN2v88internal4wasm7AsmType9IsExactlyEPS2_S3_, .-_ZN2v88internal4wasm7AsmType9IsExactlyEPS2_S3_
	.section	.text._ZN2v88internal4wasm7AsmType3IsAEPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm7AsmType3IsAEPS2_
	.type	_ZN2v88internal4wasm7AsmType3IsAEPS2_, @function
_ZN2v88internal4wasm7AsmType3IsAEPS2_:
.LFB6782:
	.cfi_startproc
	endbr64
	testb	$1, %dil
	je	.L321
	testq	%rsi, %rsi
	setne	%al
	andb	%sil, %al
	jne	.L326
	ret
	.p2align 4,,10
	.p2align 3
.L326:
	andl	$-2, %esi
	andl	%esi, %edi
	cmpl	%esi, %edi
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L321:
	movq	(%rdi), %rax
	jmp	*48(%rax)
	.cfi_endproc
.LFE6782:
	.size	_ZN2v88internal4wasm7AsmType3IsAEPS2_, .-_ZN2v88internal4wasm7AsmType3IsAEPS2_
	.section	.text._ZN2v88internal4wasm7AsmType18ElementSizeInBytesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm7AsmType18ElementSizeInBytesEv
	.type	_ZN2v88internal4wasm7AsmType18ElementSizeInBytesEv, @function
_ZN2v88internal4wasm7AsmType18ElementSizeInBytesEv:
.LFB6783:
	.cfi_startproc
	endbr64
	movl	$-1, %eax
	testb	$1, %dil
	je	.L327
	andl	$-2, %edi
	cmpl	$1048578, %edi
	je	.L332
	ja	.L329
	cmpl	$262146, %edi
	je	.L333
	jbe	.L341
	xorl	%eax, %eax
	cmpl	$524290, %edi
	sete	%al
	leal	-1(%rax,%rax,2), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L341:
	subl	$65538, %edi
	andl	$-65537, %edi
	cmpl	$1, %edi
	sbbl	%eax, %eax
	andl	$2, %eax
	subl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L329:
	movl	$8, %eax
	cmpl	$8388610, %edi
	je	.L327
	movl	$-1, %eax
	jbe	.L342
.L327:
	ret
	.p2align 4,,10
	.p2align 3
.L342:
	subl	$2097154, %edi
	andl	$-2097153, %edi
	cmpl	$1, %edi
	sbbl	%eax, %eax
	andl	$5, %eax
	subl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	movl	$4, %eax
	ret
	.cfi_endproc
.LFE6783:
	.size	_ZN2v88internal4wasm7AsmType18ElementSizeInBytesEv, .-_ZN2v88internal4wasm7AsmType18ElementSizeInBytesEv
	.section	.text._ZN2v88internal4wasm7AsmType8LoadTypeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm7AsmType8LoadTypeEv
	.type	_ZN2v88internal4wasm7AsmType8LoadTypeEv, @function
_ZN2v88internal4wasm7AsmType8LoadTypeEv:
.LFB6784:
	.cfi_startproc
	endbr64
	movl	$2147483649, %eax
	testb	$1, %dil
	je	.L343
	andl	$-2, %edi
	movl	$24589, %eax
	cmpl	$4194306, %edi
	je	.L343
	jbe	.L357
	cmpl	$8388610, %edi
	movl	$77, %edx
	movl	$2147483649, %eax
	cmove	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L357:
	cmpl	$524290, %edi
	je	.L349
	jbe	.L358
	subl	$1048578, %edi
	movl	$257, %edx
	movl	$2147483649, %eax
	andl	$-1048578, %edi
	cmove	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	movl	$257, %eax
	cmpl	$262146, %edi
	jne	.L359
.L343:
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	subl	$65538, %edi
	movl	$2147483649, %edx
	andl	$-65538, %edi
	cmovne	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L349:
	movl	$257, %eax
	ret
	.cfi_endproc
.LFE6784:
	.size	_ZN2v88internal4wasm7AsmType8LoadTypeEv, .-_ZN2v88internal4wasm7AsmType8LoadTypeEv
	.section	.text._ZN2v88internal4wasm7AsmType9StoreTypeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm7AsmType9StoreTypeEv
	.type	_ZN2v88internal4wasm7AsmType9StoreTypeEv, @function
_ZN2v88internal4wasm7AsmType9StoreTypeEv:
.LFB6785:
	.cfi_startproc
	endbr64
	movl	$2147483649, %eax
	testb	$1, %dil
	je	.L360
	andl	$-2, %edi
	movl	$5, %eax
	cmpl	$4194306, %edi
	je	.L360
	jbe	.L374
	cmpl	$8388610, %edi
	movl	$9, %edx
	movl	$2147483649, %eax
	cmove	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L374:
	cmpl	$524290, %edi
	je	.L366
	jbe	.L375
	subl	$1048578, %edi
	movl	$257, %edx
	movl	$2147483649, %eax
	andl	$-1048578, %edi
	cmove	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L375:
	movl	$257, %eax
	cmpl	$262146, %edi
	jne	.L376
.L360:
	ret
	.p2align 4,,10
	.p2align 3
.L376:
	subl	$65538, %edi
	movl	$2147483649, %edx
	andl	$-65538, %edi
	cmovne	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L366:
	movl	$257, %eax
	ret
	.cfi_endproc
.LFE6785:
	.size	_ZN2v88internal4wasm7AsmType9StoreTypeEv, .-_ZN2v88internal4wasm7AsmType9StoreTypeEv
	.section	.text._ZN2v88internal4wasm7AsmType10FroundTypeEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm7AsmType10FroundTypeEPNS0_4ZoneE
	.type	_ZN2v88internal4wasm7AsmType10FroundTypeEPNS0_4ZoneE, @function
_ZN2v88internal4wasm7AsmType10FroundTypeEPNS0_4ZoneE:
.LFB6792:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jbe	.L384
	leaq	8(%rax), %rdx
	leaq	16+_ZTVN2v88internal4wasm12_GLOBAL__N_113AsmFroundTypeE(%rip), %rcx
	movq	%rdx, 16(%rdi)
	movq	%rcx, (%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L384:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	leaq	16+_ZTVN2v88internal4wasm12_GLOBAL__N_113AsmFroundTypeE(%rip), %rcx
	movq	%rcx, (%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6792:
	.size	_ZN2v88internal4wasm7AsmType10FroundTypeEPNS0_4ZoneE, .-_ZN2v88internal4wasm7AsmType10FroundTypeEPNS0_4ZoneE
	.section	.text._ZN2v88internal4wasm7AsmType10MinMaxTypeEPNS0_4ZoneEPS2_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm7AsmType10MinMaxTypeEPNS0_4ZoneEPS2_S5_
	.type	_ZN2v88internal4wasm7AsmType10MinMaxTypeEPNS0_4ZoneEPS2_S5_, @function
_ZN2v88internal4wasm7AsmType10MinMaxTypeEPNS0_4ZoneEPS2_S5_:
.LFB6799:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L389
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L387:
	leaq	16+_ZTVN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxTypeE(%rip), %rcx
	movq	%r12, 8(%rax)
	movq	%rcx, (%rax)
	movq	%rbx, 16(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L389:
	.cfi_restore_state
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L387
	.cfi_endproc
.LFE6799:
	.size	_ZN2v88internal4wasm7AsmType10MinMaxTypeEPNS0_4ZoneEPS2_S5_, .-_ZN2v88internal4wasm7AsmType10MinMaxTypeEPNS0_4ZoneEPS2_S5_
	.section	.rodata._ZN2v88internal4wasm25AsmOverloadedFunctionType11AddOverloadEPNS1_7AsmTypeE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal4wasm25AsmOverloadedFunctionType11AddOverloadEPNS1_7AsmTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm25AsmOverloadedFunctionType11AddOverloadEPNS1_7AsmTypeE
	.type	_ZN2v88internal4wasm25AsmOverloadedFunctionType11AddOverloadEPNS1_7AsmTypeE, @function
_ZN2v88internal4wasm25AsmOverloadedFunctionType11AddOverloadEPNS1_7AsmTypeE:
.LFB6804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %r12
	cmpq	32(%rdi), %r12
	je	.L391
	movq	%rsi, (%r12)
	addq	$8, 24(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L391:
	.cfi_restore_state
	movq	16(%rdi), %r14
	movq	%r12, %rcx
	subq	%r14, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L416
	testq	%rax, %rax
	je	.L403
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L417
	movl	$2147483640, %esi
	movl	$2147483640, %r15d
.L394:
	movq	8(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L418
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L397:
	leaq	(%rax,%r15), %rsi
	leaq	8(%rax), %rdx
.L395:
	movq	%r13, (%rax,%rcx)
	cmpq	%r14, %r12
	je	.L398
	subq	$8, %r12
	leaq	15(%rax), %rdx
	subq	%r14, %r12
	subq	%r14, %rdx
	movq	%r12, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L406
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L406
	leaq	1(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L400:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L400
	movq	%rdi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %r14
	addq	%rax, %rcx
	cmpq	%rdx, %rdi
	je	.L402
	movq	(%r14), %rdx
	movq	%rdx, (%rcx)
.L402:
	leaq	16(%rax,%r12), %rdx
.L398:
	movq	%rax, %xmm0
	movq	%rdx, %xmm2
	movq	%rsi, 32(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 16(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L417:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L419
	movl	$8, %edx
	xorl	%esi, %esi
	xorl	%eax, %eax
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L403:
	movl	$8, %esi
	movl	$8, %r15d
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L406:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L399:
	movq	(%r14,%rdx,8), %rdi
	movq	%rdi, (%rax,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rdi, %rcx
	jne	.L399
	jmp	.L402
.L418:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	jmp	.L397
.L416:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L419:
	movl	$268435455, %r15d
	cmpq	$268435455, %rdx
	cmova	%r15, %rdx
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rsi
	jmp	.L394
	.cfi_endproc
.LFE6804:
	.size	_ZN2v88internal4wasm25AsmOverloadedFunctionType11AddOverloadEPNS1_7AsmTypeE, .-_ZN2v88internal4wasm25AsmOverloadedFunctionType11AddOverloadEPNS1_7AsmTypeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal4wasm7AsmType14AsCallableTypeEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal4wasm7AsmType14AsCallableTypeEv, @function
_GLOBAL__sub_I__ZN2v88internal4wasm7AsmType14AsCallableTypeEv:
.LFB7719:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE7719:
	.size	_GLOBAL__sub_I__ZN2v88internal4wasm7AsmType14AsCallableTypeEv, .-_GLOBAL__sub_I__ZN2v88internal4wasm7AsmType14AsCallableTypeEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal4wasm7AsmType14AsCallableTypeEv
	.weak	_ZTVN2v88internal4wasm15AsmCallableTypeE
	.section	.data.rel.ro._ZTVN2v88internal4wasm15AsmCallableTypeE,"awG",@progbits,_ZTVN2v88internal4wasm15AsmCallableTypeE,comdat
	.align 8
	.type	_ZTVN2v88internal4wasm15AsmCallableTypeE, @object
	.size	_ZTVN2v88internal4wasm15AsmCallableTypeE, 72
_ZTVN2v88internal4wasm15AsmCallableTypeE:
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal4wasm15AsmCallableType14AsFunctionTypeEv
	.quad	_ZN2v88internal4wasm15AsmCallableType24AsOverloadedFunctionTypeEv
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm15AsmCallableType3IsAEPNS1_7AsmTypeE
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm12_GLOBAL__N_113AsmFroundTypeE,"aw"
	.align 8
	.type	_ZTVN2v88internal4wasm12_GLOBAL__N_113AsmFroundTypeE, @object
	.size	_ZTVN2v88internal4wasm12_GLOBAL__N_113AsmFroundTypeE, 72
_ZTVN2v88internal4wasm12_GLOBAL__N_113AsmFroundTypeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_113AsmFroundType4NameEv
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_113AsmFroundType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS5_EE
	.quad	_ZN2v88internal4wasm15AsmCallableType14AsFunctionTypeEv
	.quad	_ZN2v88internal4wasm15AsmCallableType24AsOverloadedFunctionTypeEv
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_113AsmFroundTypeD1Ev
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_113AsmFroundTypeD0Ev
	.quad	_ZN2v88internal4wasm15AsmCallableType3IsAEPNS1_7AsmTypeE
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxTypeE,"aw"
	.align 8
	.type	_ZTVN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxTypeE, @object
	.size	_ZTVN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxTypeE, 72
_ZTVN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxTypeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxType4NameEv
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS5_EE
	.quad	_ZN2v88internal4wasm15AsmCallableType14AsFunctionTypeEv
	.quad	_ZN2v88internal4wasm15AsmCallableType24AsOverloadedFunctionTypeEv
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxTypeD1Ev
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_113AsmMinMaxTypeD0Ev
	.quad	_ZN2v88internal4wasm15AsmCallableType3IsAEPNS1_7AsmTypeE
	.weak	_ZTVN2v88internal4wasm15AsmFunctionTypeE
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm15AsmFunctionTypeE,"awG",@progbits,_ZTVN2v88internal4wasm15AsmFunctionTypeE,comdat
	.align 8
	.type	_ZTVN2v88internal4wasm15AsmFunctionTypeE, @object
	.size	_ZTVN2v88internal4wasm15AsmFunctionTypeE, 72
_ZTVN2v88internal4wasm15AsmFunctionTypeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm15AsmFunctionType4NameB5cxx11Ev
	.quad	_ZN2v88internal4wasm15AsmFunctionType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE
	.quad	_ZN2v88internal4wasm15AsmFunctionType14AsFunctionTypeEv
	.quad	_ZN2v88internal4wasm15AsmCallableType24AsOverloadedFunctionTypeEv
	.quad	_ZN2v88internal4wasm15AsmFunctionTypeD1Ev
	.quad	_ZN2v88internal4wasm15AsmFunctionTypeD0Ev
	.quad	_ZN2v88internal4wasm15AsmFunctionType3IsAEPNS1_7AsmTypeE
	.weak	_ZTVN2v88internal4wasm25AsmOverloadedFunctionTypeE
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm25AsmOverloadedFunctionTypeE,"awG",@progbits,_ZTVN2v88internal4wasm25AsmOverloadedFunctionTypeE,comdat
	.align 8
	.type	_ZTVN2v88internal4wasm25AsmOverloadedFunctionTypeE, @object
	.size	_ZTVN2v88internal4wasm25AsmOverloadedFunctionTypeE, 72
_ZTVN2v88internal4wasm25AsmOverloadedFunctionTypeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm25AsmOverloadedFunctionType4NameB5cxx11Ev
	.quad	_ZN2v88internal4wasm25AsmOverloadedFunctionType16CanBeInvokedWithEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE
	.quad	_ZN2v88internal4wasm15AsmCallableType14AsFunctionTypeEv
	.quad	_ZN2v88internal4wasm25AsmOverloadedFunctionType24AsOverloadedFunctionTypeEv
	.quad	_ZN2v88internal4wasm25AsmOverloadedFunctionTypeD1Ev
	.quad	_ZN2v88internal4wasm25AsmOverloadedFunctionTypeD0Ev
	.quad	_ZN2v88internal4wasm15AsmCallableType3IsAEPNS1_7AsmTypeE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.quad	7526475350838045798
	.quad	4568176567170065532
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
