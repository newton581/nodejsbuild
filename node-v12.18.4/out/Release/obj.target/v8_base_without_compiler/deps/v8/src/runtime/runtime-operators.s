	.file	"runtime-operators.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L14
.L5:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L5
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL25Stats_Runtime_StrictEqualEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC1:
	.string	"V8.Runtime_Runtime_StrictEqual"
	.section	.text._ZN2v88internalL25Stats_Runtime_StrictEqualEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL25Stats_Runtime_StrictEqualEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL25Stats_Runtime_StrictEqualEiPmPNS0_7IsolateE.isra.0:
.LFB24978:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L46
.L16:
	movq	_ZZN2v88internalL25Stats_Runtime_StrictEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic43(%rip), %rbx
	testq	%rbx, %rbx
	je	.L47
.L18:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L48
.L20:
	movq	(%r12), %rax
	movq	-8(%r12), %rsi
	leaq	-152(%rbp), %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal6Object12StrictEqualsES1_@PLT
	testb	%al, %al
	je	.L24
	movq	112(%r13), %r12
.L25:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L49
.L15:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L50
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	120(%r13), %r12
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L48:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L51
.L21:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L22
	movq	(%rdi), %rax
	call	*8(%rax)
.L22:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L23
	movq	(%rdi), %rax
	call	*8(%rax)
.L23:
	leaq	.LC1(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L47:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L52
.L19:
	movq	%rbx, _ZZN2v88internalL25Stats_Runtime_StrictEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic43(%rip)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L46:
	movq	40960(%rsi), %rax
	movl	$472, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L49:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L52:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L51:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC1(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L21
.L50:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24978:
	.size	_ZN2v88internalL25Stats_Runtime_StrictEqualEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL25Stats_Runtime_StrictEqualEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL28Stats_Runtime_StrictNotEqualEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"V8.Runtime_Runtime_StrictNotEqual"
	.section	.text._ZN2v88internalL28Stats_Runtime_StrictNotEqualEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Stats_Runtime_StrictNotEqualEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL28Stats_Runtime_StrictNotEqualEiPmPNS0_7IsolateE.isra.0:
.LFB24979:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L84
.L54:
	movq	_ZZN2v88internalL28Stats_Runtime_StrictNotEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic51(%rip), %rbx
	testq	%rbx, %rbx
	je	.L85
.L56:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L86
.L58:
	movq	(%r12), %rax
	movq	-8(%r12), %rsi
	leaq	-152(%rbp), %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal6Object12StrictEqualsES1_@PLT
	testb	%al, %al
	jne	.L62
	movq	112(%r13), %r12
.L63:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L87
.L53:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L88
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movq	120(%r13), %r12
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L86:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L89
.L59:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L60
	movq	(%rdi), %rax
	call	*8(%rax)
.L60:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L61
	movq	(%rdi), %rax
	call	*8(%rax)
.L61:
	leaq	.LC2(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L85:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L90
.L57:
	movq	%rbx, _ZZN2v88internalL28Stats_Runtime_StrictNotEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic51(%rip)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L84:
	movq	40960(%rsi), %rax
	movl	$473, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L87:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L90:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L89:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC2(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L59
.L88:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24979:
	.size	_ZN2v88internalL28Stats_Runtime_StrictNotEqualEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL28Stats_Runtime_StrictNotEqualEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL17Stats_Runtime_AddEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC3:
	.string	"V8.Runtime_Runtime_Add"
	.section	.text._ZN2v88internalL17Stats_Runtime_AddEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL17Stats_Runtime_AddEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL17Stats_Runtime_AddEiPmPNS0_7IsolateE.isra.0:
.LFB24984:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L124
.L92:
	movq	_ZZN2v88internalL17Stats_Runtime_AddEiPmPNS0_7IsolateEE27trace_event_unique_atomic14(%rip), %rbx
	testq	%rbx, %rbx
	je	.L125
.L94:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L126
.L96:
	addl	$1, 41104(%r12)
	leaq	-8(%r13), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	call	_ZN2v88internal6Object3AddEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testq	%rax, %rax
	je	.L127
	movq	(%rax), %r13
.L101:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L104
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L104:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L128
.L91:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L129
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L130
.L97:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L98
	movq	(%rdi), %rax
	call	*8(%rax)
.L98:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L99
	movq	(%rdi), %rax
	call	*8(%rax)
.L99:
	leaq	.LC3(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L125:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L131
.L95:
	movq	%rbx, _ZZN2v88internalL17Stats_Runtime_AddEiPmPNS0_7IsolateEE27trace_event_unique_atomic14(%rip)
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L127:
	movq	312(%r12), %r13
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L128:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L124:
	movq	40960(%rsi), %rax
	movl	$465, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L131:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L130:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC3(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L97
.L129:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24984:
	.size	_ZN2v88internalL17Stats_Runtime_AddEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL17Stats_Runtime_AddEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL19Stats_Runtime_EqualEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC4:
	.string	"V8.Runtime_Runtime_Equal"
	.section	.text._ZN2v88internalL19Stats_Runtime_EqualEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL19Stats_Runtime_EqualEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL19Stats_Runtime_EqualEiPmPNS0_7IsolateE.isra.0:
.LFB24986:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L166
.L133:
	movq	_ZZN2v88internalL19Stats_Runtime_EqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic23(%rip), %rbx
	testq	%rbx, %rbx
	je	.L167
.L135:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L168
.L137:
	addl	$1, 41104(%r12)
	leaq	-8(%r13), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	call	_ZN2v88internal6Object6EqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testb	%al, %al
	jne	.L141
	movq	312(%r12), %r13
.L142:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L146
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L146:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L169
.L132:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L170
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	shrw	$8, %ax
	je	.L143
	movq	112(%r12), %r13
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L143:
	movq	120(%r12), %r13
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L168:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L171
.L138:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L139
	movq	(%rdi), %rax
	call	*8(%rax)
.L139:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L140
	movq	(%rdi), %rax
	call	*8(%rax)
.L140:
	leaq	.LC4(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L167:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L172
.L136:
	movq	%rbx, _ZZN2v88internalL19Stats_Runtime_EqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic23(%rip)
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L166:
	movq	40960(%rsi), %rax
	movl	$466, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L169:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L172:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L171:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC4(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L138
.L170:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24986:
	.size	_ZN2v88internalL19Stats_Runtime_EqualEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL19Stats_Runtime_EqualEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL22Stats_Runtime_NotEqualEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC5:
	.string	"V8.Runtime_Runtime_NotEqual"
	.section	.text._ZN2v88internalL22Stats_Runtime_NotEqualEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL22Stats_Runtime_NotEqualEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL22Stats_Runtime_NotEqualEiPmPNS0_7IsolateE.isra.0:
.LFB24987:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L207
.L174:
	movq	_ZZN2v88internalL22Stats_Runtime_NotEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic33(%rip), %rbx
	testq	%rbx, %rbx
	je	.L208
.L176:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L209
.L178:
	addl	$1, 41104(%r12)
	leaq	-8(%r13), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	call	_ZN2v88internal6Object6EqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testb	%al, %al
	jne	.L182
	movq	312(%r12), %r13
.L183:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L187
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L187:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L210
.L173:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L211
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	shrw	$8, %ax
	jne	.L184
	movq	112(%r12), %r13
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L184:
	movq	120(%r12), %r13
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L209:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L212
.L179:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L180
	movq	(%rdi), %rax
	call	*8(%rax)
.L180:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L181
	movq	(%rdi), %rax
	call	*8(%rax)
.L181:
	leaq	.LC5(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L208:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L213
.L177:
	movq	%rbx, _ZZN2v88internalL22Stats_Runtime_NotEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic33(%rip)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L207:
	movq	40960(%rsi), %rax
	movl	$471, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L210:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L213:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L212:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC5(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L179
.L211:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24987:
	.size	_ZN2v88internalL22Stats_Runtime_NotEqualEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL22Stats_Runtime_NotEqualEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL22Stats_Runtime_LessThanEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC6:
	.string	"V8.Runtime_Runtime_LessThan"
	.section	.text._ZN2v88internalL22Stats_Runtime_LessThanEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL22Stats_Runtime_LessThanEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL22Stats_Runtime_LessThanEiPmPNS0_7IsolateE.isra.0:
.LFB24988:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L248
.L215:
	movq	_ZZN2v88internalL22Stats_Runtime_LessThanEiPmPNS0_7IsolateEE27trace_event_unique_atomic59(%rip), %rbx
	testq	%rbx, %rbx
	je	.L249
.L217:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L250
.L219:
	addl	$1, 41104(%r12)
	leaq	-8(%r13), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	call	_ZN2v88internal6Object7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testb	%al, %al
	je	.L223
	sarq	$32, %rax
	testl	%eax, %eax
	je	.L224
	subl	$1, %eax
	cmpl	$2, %eax
	ja	.L223
	movq	120(%r12), %r13
.L225:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L228
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L228:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L251
.L214:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L252
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	movq	312(%r12), %r13
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L224:
	movq	112(%r12), %r13
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L250:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L253
.L220:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L221
	movq	(%rdi), %rax
	call	*8(%rax)
.L221:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L222
	movq	(%rdi), %rax
	call	*8(%rax)
.L222:
	leaq	.LC6(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L249:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L254
.L218:
	movq	%rbx, _ZZN2v88internalL22Stats_Runtime_LessThanEiPmPNS0_7IsolateEE27trace_event_unique_atomic59(%rip)
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L248:
	movq	40960(%rsi), %rax
	movl	$469, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L251:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L254:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L253:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC6(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L220
.L252:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24988:
	.size	_ZN2v88internalL22Stats_Runtime_LessThanEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL22Stats_Runtime_LessThanEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL25Stats_Runtime_GreaterThanEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"V8.Runtime_Runtime_GreaterThan"
	.section	.text._ZN2v88internalL25Stats_Runtime_GreaterThanEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL25Stats_Runtime_GreaterThanEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL25Stats_Runtime_GreaterThanEiPmPNS0_7IsolateE.isra.0:
.LFB24989:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L293
.L256:
	movq	_ZZN2v88internalL25Stats_Runtime_GreaterThanEiPmPNS0_7IsolateEE27trace_event_unique_atomic69(%rip), %rbx
	testq	%rbx, %rbx
	je	.L294
.L258:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L295
.L260:
	addl	$1, 41104(%r12)
	leaq	-8(%r13), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	call	_ZN2v88internal6Object7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testb	%al, %al
	je	.L264
	sarq	$32, %rax
	cmpl	$2, %eax
	je	.L265
	jle	.L296
	cmpl	$3, %eax
	je	.L273
.L264:
	movq	312(%r12), %r13
.L269:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L272
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L272:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L297
.L255:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L298
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L296:
	.cfi_restore_state
	cmpl	$1, %eax
	ja	.L264
.L273:
	movq	120(%r12), %r13
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L265:
	movq	112(%r12), %r13
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L295:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L299
.L261:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L262
	movq	(%rdi), %rax
	call	*8(%rax)
.L262:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L263
	movq	(%rdi), %rax
	call	*8(%rax)
.L263:
	leaq	.LC7(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L294:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L300
.L259:
	movq	%rbx, _ZZN2v88internalL25Stats_Runtime_GreaterThanEiPmPNS0_7IsolateEE27trace_event_unique_atomic69(%rip)
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L293:
	movq	40960(%rsi), %rax
	movl	$467, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L297:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L300:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L299:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC7(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L261
.L298:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24989:
	.size	_ZN2v88internalL25Stats_Runtime_GreaterThanEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL25Stats_Runtime_GreaterThanEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL29Stats_Runtime_LessThanOrEqualEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"V8.Runtime_Runtime_LessThanOrEqual"
	.section	.text._ZN2v88internalL29Stats_Runtime_LessThanOrEqualEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Stats_Runtime_LessThanOrEqualEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL29Stats_Runtime_LessThanOrEqualEiPmPNS0_7IsolateE.isra.0:
.LFB24990:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L337
.L302:
	movq	_ZZN2v88internalL29Stats_Runtime_LessThanOrEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic79(%rip), %rbx
	testq	%rbx, %rbx
	je	.L338
.L304:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L339
.L306:
	addl	$1, 41104(%r12)
	leaq	-8(%r13), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	call	_ZN2v88internal6Object7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testb	%al, %al
	je	.L310
	sarq	$32, %rax
	cmpl	$1, %eax
	jle	.L340
	subl	$2, %eax
	cmpl	$1, %eax
	ja	.L310
	movq	120(%r12), %r13
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L340:
	testl	%eax, %eax
	jns	.L341
.L310:
	movq	312(%r12), %r13
.L314:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L317
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L317:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L342
.L301:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L343
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L341:
	.cfi_restore_state
	movq	112(%r12), %r13
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L339:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L344
.L307:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L308
	movq	(%rdi), %rax
	call	*8(%rax)
.L308:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L309
	movq	(%rdi), %rax
	call	*8(%rax)
.L309:
	leaq	.LC8(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L338:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L345
.L305:
	movq	%rbx, _ZZN2v88internalL29Stats_Runtime_LessThanOrEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic79(%rip)
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L337:
	movq	40960(%rsi), %rax
	movl	$470, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L342:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L345:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L344:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC8(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L307
.L343:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24990:
	.size	_ZN2v88internalL29Stats_Runtime_LessThanOrEqualEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL29Stats_Runtime_LessThanOrEqualEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL32Stats_Runtime_GreaterThanOrEqualEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"V8.Runtime_Runtime_GreaterThanOrEqual"
	.section	.text._ZN2v88internalL32Stats_Runtime_GreaterThanOrEqualEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_GreaterThanOrEqualEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL32Stats_Runtime_GreaterThanOrEqualEiPmPNS0_7IsolateE.isra.0:
.LFB24991:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L384
.L347:
	movq	_ZZN2v88internalL32Stats_Runtime_GreaterThanOrEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic89(%rip), %rbx
	testq	%rbx, %rbx
	je	.L385
.L349:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L386
.L351:
	addl	$1, 41104(%r12)
	leaq	-8(%r13), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	call	_ZN2v88internal6Object7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testb	%al, %al
	je	.L355
	sarq	$32, %rax
	cmpl	$2, %eax
	jg	.L356
	testl	%eax, %eax
	jg	.L357
	je	.L364
.L355:
	movq	312(%r12), %r13
.L360:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L363
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L363:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L387
.L346:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L388
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
	cmpl	$3, %eax
	jne	.L355
.L364:
	movq	120(%r12), %r13
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L357:
	movq	112(%r12), %r13
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L386:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L389
.L352:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L353
	movq	(%rdi), %rax
	call	*8(%rax)
.L353:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L354
	movq	(%rdi), %rax
	call	*8(%rax)
.L354:
	leaq	.LC9(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L385:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L390
.L350:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_GreaterThanOrEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic89(%rip)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L384:
	movq	40960(%rsi), %rax
	movl	$468, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L387:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L390:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L389:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC9(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L352
.L388:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24991:
	.size	_ZN2v88internalL32Stats_Runtime_GreaterThanOrEqualEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL32Stats_Runtime_GreaterThanOrEqualEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal11Runtime_AddEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal11Runtime_AddEiPmPNS0_7IsolateE
	.type	_ZN2v88internal11Runtime_AddEiPmPNS0_7IsolateE, @function
_ZN2v88internal11Runtime_AddEiPmPNS0_7IsolateE:
.LFB20029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L398
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	%r12, %rdi
	movq	41096(%rdx), %r13
	leaq	-8(%rsi), %rdx
	call	_ZN2v88internal6Object3AddEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testq	%rax, %rax
	je	.L399
	movq	(%rax), %r14
.L394:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L391
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L391:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L399:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L398:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL17Stats_Runtime_AddEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20029:
	.size	_ZN2v88internal11Runtime_AddEiPmPNS0_7IsolateE, .-_ZN2v88internal11Runtime_AddEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal13Runtime_EqualEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal13Runtime_EqualEiPmPNS0_7IsolateE
	.type	_ZN2v88internal13Runtime_EqualEiPmPNS0_7IsolateE, @function
_ZN2v88internal13Runtime_EqualEiPmPNS0_7IsolateE:
.LFB20032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L408
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	%r12, %rdi
	movq	41096(%rdx), %r13
	leaq	-8(%rsi), %rdx
	call	_ZN2v88internal6Object6EqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testb	%al, %al
	je	.L409
	shrw	$8, %ax
	je	.L404
	movq	112(%r12), %r14
.L403:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L400
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L400:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L409:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L404:
	movq	120(%r12), %r14
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L408:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL19Stats_Runtime_EqualEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20032:
	.size	_ZN2v88internal13Runtime_EqualEiPmPNS0_7IsolateE, .-_ZN2v88internal13Runtime_EqualEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal16Runtime_NotEqualEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal16Runtime_NotEqualEiPmPNS0_7IsolateE
	.type	_ZN2v88internal16Runtime_NotEqualEiPmPNS0_7IsolateE, @function
_ZN2v88internal16Runtime_NotEqualEiPmPNS0_7IsolateE:
.LFB20035:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L418
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	%r12, %rdi
	movq	41096(%rdx), %r13
	leaq	-8(%rsi), %rdx
	call	_ZN2v88internal6Object6EqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testb	%al, %al
	je	.L419
	shrw	$8, %ax
	jne	.L414
	movq	112(%r12), %r14
.L413:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L410
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L410:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L419:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L414:
	movq	120(%r12), %r14
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L418:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL22Stats_Runtime_NotEqualEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20035:
	.size	_ZN2v88internal16Runtime_NotEqualEiPmPNS0_7IsolateE, .-_ZN2v88internal16Runtime_NotEqualEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal19Runtime_StrictEqualEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19Runtime_StrictEqualEiPmPNS0_7IsolateE
	.type	_ZN2v88internal19Runtime_StrictEqualEiPmPNS0_7IsolateE, @function
_ZN2v88internal19Runtime_StrictEqualEiPmPNS0_7IsolateE:
.LFB20038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L427
	movq	(%rsi), %rax
	movq	-8(%rsi), %rsi
	leaq	-32(%rbp), %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal6Object12StrictEqualsES1_@PLT
	testb	%al, %al
	jne	.L428
	movq	120(%r12), %rax
.L420:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L429
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L428:
	.cfi_restore_state
	movq	112(%r12), %rax
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L427:
	movq	%rdx, %rsi
	call	_ZN2v88internalL25Stats_Runtime_StrictEqualEiPmPNS0_7IsolateE.isra.0
	jmp	.L420
.L429:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20038:
	.size	_ZN2v88internal19Runtime_StrictEqualEiPmPNS0_7IsolateE, .-_ZN2v88internal19Runtime_StrictEqualEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Runtime_StrictNotEqualEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Runtime_StrictNotEqualEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Runtime_StrictNotEqualEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Runtime_StrictNotEqualEiPmPNS0_7IsolateE:
.LFB20041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L437
	movq	(%rsi), %rax
	movq	-8(%rsi), %rsi
	leaq	-32(%rbp), %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal6Object12StrictEqualsES1_@PLT
	testb	%al, %al
	je	.L438
	movq	120(%r12), %rax
.L430:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L439
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L438:
	.cfi_restore_state
	movq	112(%r12), %rax
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L437:
	movq	%rdx, %rsi
	call	_ZN2v88internalL28Stats_Runtime_StrictNotEqualEiPmPNS0_7IsolateE.isra.0
	jmp	.L430
.L439:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20041:
	.size	_ZN2v88internal22Runtime_StrictNotEqualEiPmPNS0_7IsolateE, .-_ZN2v88internal22Runtime_StrictNotEqualEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal16Runtime_LessThanEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal16Runtime_LessThanEiPmPNS0_7IsolateE
	.type	_ZN2v88internal16Runtime_LessThanEiPmPNS0_7IsolateE, @function
_ZN2v88internal16Runtime_LessThanEiPmPNS0_7IsolateE:
.LFB20044:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L448
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	%r12, %rdi
	movq	41096(%rdx), %r14
	leaq	-8(%rsi), %rdx
	call	_ZN2v88internal6Object7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testb	%al, %al
	je	.L442
	sarq	$32, %rax
	testl	%eax, %eax
	je	.L443
	subl	$1, %eax
	cmpl	$2, %eax
	ja	.L442
	movq	120(%r12), %r13
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L442:
	movq	312(%r12), %r13
.L444:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L440
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L440:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L443:
	.cfi_restore_state
	movq	112(%r12), %r13
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L448:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL22Stats_Runtime_LessThanEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20044:
	.size	_ZN2v88internal16Runtime_LessThanEiPmPNS0_7IsolateE, .-_ZN2v88internal16Runtime_LessThanEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal19Runtime_GreaterThanEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19Runtime_GreaterThanEiPmPNS0_7IsolateE
	.type	_ZN2v88internal19Runtime_GreaterThanEiPmPNS0_7IsolateE, @function
_ZN2v88internal19Runtime_GreaterThanEiPmPNS0_7IsolateE:
.LFB20047:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L461
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	%r12, %rdi
	movq	41096(%rdx), %r13
	leaq	-8(%rsi), %rdx
	call	_ZN2v88internal6Object7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testb	%al, %al
	je	.L451
	sarq	$32, %rax
	cmpl	$2, %eax
	je	.L452
	jg	.L453
	cmpl	$1, %eax
	ja	.L451
.L459:
	movq	120(%r12), %r14
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L453:
	cmpl	$3, %eax
	je	.L459
.L451:
	movq	312(%r12), %r14
.L456:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L449
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L449:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L452:
	.cfi_restore_state
	movq	112(%r12), %r14
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L461:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL25Stats_Runtime_GreaterThanEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20047:
	.size	_ZN2v88internal19Runtime_GreaterThanEiPmPNS0_7IsolateE, .-_ZN2v88internal19Runtime_GreaterThanEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Runtime_LessThanOrEqualEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Runtime_LessThanOrEqualEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Runtime_LessThanOrEqualEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Runtime_LessThanOrEqualEiPmPNS0_7IsolateE:
.LFB20050:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L472
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	%r12, %rdi
	movq	41096(%rdx), %r13
	leaq	-8(%rsi), %rdx
	call	_ZN2v88internal6Object7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testb	%al, %al
	je	.L464
	sarq	$32, %rax
	cmpl	$1, %eax
	jg	.L465
	testl	%eax, %eax
	jns	.L473
.L464:
	movq	312(%r12), %r14
.L468:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L462
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L462:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L473:
	.cfi_restore_state
	movq	112(%r12), %r14
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L465:
	subl	$2, %eax
	cmpl	$1, %eax
	ja	.L464
	movq	120(%r12), %r14
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L472:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL29Stats_Runtime_LessThanOrEqualEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20050:
	.size	_ZN2v88internal23Runtime_LessThanOrEqualEiPmPNS0_7IsolateE, .-_ZN2v88internal23Runtime_LessThanOrEqualEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_GreaterThanOrEqualEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_GreaterThanOrEqualEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_GreaterThanOrEqualEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_GreaterThanOrEqualEiPmPNS0_7IsolateE:
.LFB20053:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L486
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	%r12, %rdi
	movq	41096(%rdx), %r13
	leaq	-8(%rsi), %rdx
	call	_ZN2v88internal6Object7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testb	%al, %al
	je	.L476
	sarq	$32, %rax
	cmpl	$2, %eax
	jg	.L477
	testl	%eax, %eax
	jg	.L478
	jne	.L476
.L484:
	movq	120(%r12), %r14
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L477:
	cmpl	$3, %eax
	je	.L484
.L476:
	movq	312(%r12), %r14
.L481:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L474
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L474:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L478:
	.cfi_restore_state
	movq	112(%r12), %r14
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L486:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL32Stats_Runtime_GreaterThanOrEqualEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20053:
	.size	_ZN2v88internal26Runtime_GreaterThanOrEqualEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_GreaterThanOrEqualEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11Runtime_AddEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11Runtime_AddEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal11Runtime_AddEiPmPNS0_7IsolateE:
.LFB24973:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24973:
	.size	_GLOBAL__sub_I__ZN2v88internal11Runtime_AddEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal11Runtime_AddEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11Runtime_AddEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL32Stats_Runtime_GreaterThanOrEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic89,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_GreaterThanOrEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic89, @object
	.size	_ZZN2v88internalL32Stats_Runtime_GreaterThanOrEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic89, 8
_ZZN2v88internalL32Stats_Runtime_GreaterThanOrEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic89:
	.zero	8
	.section	.bss._ZZN2v88internalL29Stats_Runtime_LessThanOrEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic79,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Stats_Runtime_LessThanOrEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic79, @object
	.size	_ZZN2v88internalL29Stats_Runtime_LessThanOrEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic79, 8
_ZZN2v88internalL29Stats_Runtime_LessThanOrEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic79:
	.zero	8
	.section	.bss._ZZN2v88internalL25Stats_Runtime_GreaterThanEiPmPNS0_7IsolateEE27trace_event_unique_atomic69,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL25Stats_Runtime_GreaterThanEiPmPNS0_7IsolateEE27trace_event_unique_atomic69, @object
	.size	_ZZN2v88internalL25Stats_Runtime_GreaterThanEiPmPNS0_7IsolateEE27trace_event_unique_atomic69, 8
_ZZN2v88internalL25Stats_Runtime_GreaterThanEiPmPNS0_7IsolateEE27trace_event_unique_atomic69:
	.zero	8
	.section	.bss._ZZN2v88internalL22Stats_Runtime_LessThanEiPmPNS0_7IsolateEE27trace_event_unique_atomic59,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL22Stats_Runtime_LessThanEiPmPNS0_7IsolateEE27trace_event_unique_atomic59, @object
	.size	_ZZN2v88internalL22Stats_Runtime_LessThanEiPmPNS0_7IsolateEE27trace_event_unique_atomic59, 8
_ZZN2v88internalL22Stats_Runtime_LessThanEiPmPNS0_7IsolateEE27trace_event_unique_atomic59:
	.zero	8
	.section	.bss._ZZN2v88internalL28Stats_Runtime_StrictNotEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic51,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Stats_Runtime_StrictNotEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic51, @object
	.size	_ZZN2v88internalL28Stats_Runtime_StrictNotEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic51, 8
_ZZN2v88internalL28Stats_Runtime_StrictNotEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic51:
	.zero	8
	.section	.bss._ZZN2v88internalL25Stats_Runtime_StrictEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic43,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL25Stats_Runtime_StrictEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic43, @object
	.size	_ZZN2v88internalL25Stats_Runtime_StrictEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic43, 8
_ZZN2v88internalL25Stats_Runtime_StrictEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic43:
	.zero	8
	.section	.bss._ZZN2v88internalL22Stats_Runtime_NotEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic33,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL22Stats_Runtime_NotEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic33, @object
	.size	_ZZN2v88internalL22Stats_Runtime_NotEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic33, 8
_ZZN2v88internalL22Stats_Runtime_NotEqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic33:
	.zero	8
	.section	.bss._ZZN2v88internalL19Stats_Runtime_EqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic23,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL19Stats_Runtime_EqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic23, @object
	.size	_ZZN2v88internalL19Stats_Runtime_EqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic23, 8
_ZZN2v88internalL19Stats_Runtime_EqualEiPmPNS0_7IsolateEE27trace_event_unique_atomic23:
	.zero	8
	.section	.bss._ZZN2v88internalL17Stats_Runtime_AddEiPmPNS0_7IsolateEE27trace_event_unique_atomic14,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL17Stats_Runtime_AddEiPmPNS0_7IsolateEE27trace_event_unique_atomic14, @object
	.size	_ZZN2v88internalL17Stats_Runtime_AddEiPmPNS0_7IsolateEE27trace_event_unique_atomic14, 8
_ZZN2v88internalL17Stats_Runtime_AddEiPmPNS0_7IsolateEE27trace_event_unique_atomic14:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
