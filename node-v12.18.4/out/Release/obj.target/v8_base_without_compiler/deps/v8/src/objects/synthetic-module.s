	.file	"synthetic-module.cc"
	.text
	.section	.text._ZN2v88internal15SyntheticModule9SetExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15SyntheticModule9SetExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal15SyntheticModule9SetExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal15SyntheticModule9SetExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_6ObjectEEE:
.LFB18157:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	subq	$32, %rsp
	movq	41112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L3:
	movq	%rsi, -48(%rbp)
	leaq	-48(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE6LookupENS0_6HandleINS0_6ObjectEEE@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L5
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r14
	testb	$1, %r14b
	jne	.L8
.L10:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$273, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
	movb	$0, %ah
.L9:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L24
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L25
.L4:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L5:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L26
.L7:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	movq	(%rax), %r14
	testb	$1, %r14b
	je	.L10
.L8:
	movq	-1(%r14), %rax
	cmpw	$151, 11(%rax)
	jne	.L10
	movq	(%rbx), %r12
	leaq	7(%r14), %r13
	movq	%r12, 7(%r14)
	testb	$1, %r12b
	je	.L14
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L27
.L12:
	testb	$24, %al
	je	.L14
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L14
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$257, %eax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L26:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L12
.L24:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18157:
	.size	_ZN2v88internal15SyntheticModule9SetExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal15SyntheticModule9SetExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_6ObjectEEE
	.section	.rodata._ZN2v88internal15SyntheticModule15SetExportStrictEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_6ObjectEEE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"export_object->IsCell()"
.LC1:
	.string	"Check failed: %s."
.LC2:
	.string	"set_export_result.FromJust()"
	.section	.text._ZN2v88internal15SyntheticModule15SetExportStrictEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15SyntheticModule15SetExportStrictEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal15SyntheticModule15SetExportStrictEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal15SyntheticModule15SetExportStrictEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_6ObjectEEE:
.LFB18158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	41112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L29
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L30:
	movq	%rsi, -48(%rbp)
	leaq	-48(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE6LookupENS0_6HandleINS0_6ObjectEEE@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L32
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L35
.L36:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L41
.L31:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L32:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L42
.L34:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	movq	(%rax), %rax
	testb	$1, %al
	je	.L36
.L35:
	movq	-1(%rax), %rax
	cmpw	$151, 11(%rax)
	jne	.L36
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15SyntheticModule9SetExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_6ObjectEEE
	testb	%al, %al
	je	.L43
.L37:
	shrw	$8, %ax
	je	.L44
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L45
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movl	%eax, -56(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-56(%rbp), %eax
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L44:
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L31
.L45:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18158:
	.size	_ZN2v88internal15SyntheticModule15SetExportStrictEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal15SyntheticModule15SetExportStrictEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal15SyntheticModule13ResolveExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15SyntheticModule13ResolveExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEb
	.type	_ZN2v88internal15SyntheticModule13ResolveExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEb, @function
_ZN2v88internal15SyntheticModule13ResolveExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEb:
.LFB18159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	leaq	-48(%rbp), %rdi
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%r8d, %ebx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rcx, %rsi
	movq	7(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE6LookupENS0_6HandleINS0_6ObjectEEE@PLT
	movq	41112(%r14), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L47
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rdx
	testb	$1, %dl
	jne	.L50
.L53:
	xorl	%eax, %eax
	testb	%bl, %bl
	jne	.L59
.L54:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L60
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L61
.L49:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	movq	(%rax), %rdx
	testb	$1, %dl
	je	.L53
.L50:
	movq	-1(%rdx), %rdx
	cmpw	$151, 11(%rdx)
	je	.L54
	xorl	%eax, %eax
	testb	%bl, %bl
	je	.L54
.L59:
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdx
	movl	$327, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory14NewSyntaxErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	leaq	16(%rbp), %rdx
	movq	%r14, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L61:
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L49
.L60:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18159:
	.size	_ZN2v88internal15SyntheticModule13ResolveExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEb, .-_ZN2v88internal15SyntheticModule13ResolveExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEb
	.section	.rodata._ZN2v88internal15SyntheticModule18PrepareInstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"exports->Lookup(name).IsTheHole(isolate)"
	.section	.text._ZN2v88internal15SyntheticModule18PrepareInstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15SyntheticModule18PrepareInstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE
	.type	_ZN2v88internal15SyntheticModule18PrepareInstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE, @function
_ZN2v88internal15SyntheticModule18PrepareInstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE:
.LFB18163:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%rsi, -104(%rbp)
	movq	(%rsi), %rax
	movq	41112(%rdi), %rdi
	movq	7(%rax), %rsi
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	testq	%rdi, %rdi
	je	.L63
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L64:
	movq	-104(%rbp), %rax
	movq	41112(%rbx), %rdi
	movq	(%rax), %rax
	movq	55(%rax), %rsi
	testq	%rdi, %rdi
	je	.L66
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
.L67:
	movslq	11(%rsi), %rax
	testq	%rax, %rax
	jle	.L69
	subl	$1, %eax
	leaq	88(%rbx), %rcx
	movl	$16, %r14d
	leaq	24(,%rax,8), %rax
	movq	%rcx, -80(%rbp)
	movq	%rax, -96(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, -88(%rbp)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L89:
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L71:
	movq	(%r12), %rax
	movq	-88(%rbp), %rdi
	movq	%rsi, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE6LookupENS0_6HandleINS0_6ObjectEEE@PLT
	cmpq	%rax, 96(%rbx)
	movq	-72(%rbp), %rsi
	jne	.L88
	movq	%r12, %rdi
	movq	%r13, %rdx
	addq	$8, %r14
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE3PutENS0_6HandleIS2_EENS5_INS0_6ObjectEEES8_@PLT
	movq	%rax, %r12
	cmpq	%r14, -96(%rbp)
	je	.L69
.L74:
	movq	-80(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory7NewCellENS0_6HandleINS0_6ObjectEEE@PLT
	movq	%rax, %r13
	movq	(%r15), %rax
	movq	-1(%r14,%rax), %r8
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L89
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L90
.L72:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r8, (%rsi)
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L88:
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L90:
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L63:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L91
.L65:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L66:
	movq	41088(%rbx), %r15
	cmpq	41096(%rbx), %r15
	je	.L92
.L68:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r15)
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L69:
	movq	-104(%rbp), %rax
	movq	(%r12), %r12
	movq	(%rax), %r13
	movq	%r12, 7(%r13)
	leaq	7(%r13), %r14
	testb	$1, %r12b
	je	.L78
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L93
	testb	$24, %al
	je	.L78
.L95:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L78
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L78:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L94
	addq	$72, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L95
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L92:
	movq	%rbx, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%rbx, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L65
.L94:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18163:
	.size	_ZN2v88internal15SyntheticModule18PrepareInstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE, .-_ZN2v88internal15SyntheticModule18PrepareInstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE
	.section	.text._ZN2v88internal15SyntheticModule17FinishInstantiateEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15SyntheticModule17FinishInstantiateEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal15SyntheticModule17FinishInstantiateEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal15SyntheticModule17FinishInstantiateEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18164:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	leaq	-16(%rbp), %rdi
	movl	$3, %esi
	movq	%rax, -16(%rbp)
	call	_ZN2v88internal6Module9SetStatusENS1_6StatusE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L99
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$1, %eax
	ret
.L99:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18164:
	.size	_ZN2v88internal15SyntheticModule17FinishInstantiateEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal15SyntheticModule17FinishInstantiateEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15SyntheticModule8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15SyntheticModule8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal15SyntheticModule8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal15SyntheticModule8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18165:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-48(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	$4, %esi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal6Module9SetStatusENS1_6StatusE@PLT
	movq	(%rbx), %rax
	movq	63(%rax), %rax
	movq	7(%rax), %r13
	movq	12464(%r12), %rax
	movq	39(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L101
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	*%r13
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L111
.L108:
	movq	(%rbx), %rax
	movl	$5, %esi
	movq	%r14, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal6Module9SetStatusENS1_6StatusE@PLT
	movq	%r13, %rax
.L106:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L112
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	movq	41088(%r12), %rdi
	cmpq	41096(%r12), %rdi
	je	.L113
.L103:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdi)
	movq	%rbx, %rsi
	call	*%r13
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L108
.L111:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	(%rbx), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal6Module11RecordErrorEPNS0_7IsolateE@PLT
	xorl	%eax, %eax
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L103
.L112:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18165:
	.size	_ZN2v88internal15SyntheticModule8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal15SyntheticModule8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal15SyntheticModule9SetExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal15SyntheticModule9SetExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_6ObjectEEE, @function
_GLOBAL__sub_I__ZN2v88internal15SyntheticModule9SetExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_6ObjectEEE:
.LFB22403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22403:
	.size	_GLOBAL__sub_I__ZN2v88internal15SyntheticModule9SetExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_6ObjectEEE, .-_GLOBAL__sub_I__ZN2v88internal15SyntheticModule9SetExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_6ObjectEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal15SyntheticModule9SetExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS4_INS0_6ObjectEEE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
