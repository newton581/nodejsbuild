	.file	"code-serializer.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5186:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5186:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5187:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5187:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5189:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5189:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.type	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, @function
_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE:
.LFB7689:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rcx), %r8
	movq	16(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE7689:
	.size	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, .-_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.section	.text._ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE,"axG",@progbits,_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.type	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE, @function
_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE:
.LFB7690:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7690:
	.size	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE, .-_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.section	.text._ZN2v88internal14CodeSerializer11ElideObjectENS0_6ObjectE,"axG",@progbits,_ZN2v88internal14CodeSerializer11ElideObjectENS0_6ObjectE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CodeSerializer11ElideObjectENS0_6ObjectE
	.type	_ZN2v88internal14CodeSerializer11ElideObjectENS0_6ObjectE, @function
_ZN2v88internal14CodeSerializer11ElideObjectENS0_6ObjectE:
.LFB9004:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE9004:
	.size	_ZN2v88internal14CodeSerializer11ElideObjectENS0_6ObjectE, .-_ZN2v88internal14CodeSerializer11ElideObjectENS0_6ObjectE
	.section	.text._ZN2v88internal14SerializedDataD2Ev,"axG",@progbits,_ZN2v88internal14SerializedDataD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14SerializedDataD2Ev
	.type	_ZN2v88internal14SerializedDataD2Ev, @function
_ZN2v88internal14SerializedDataD2Ev:
.LFB8870:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14SerializedDataE(%rip), %rax
	cmpb	$0, 20(%rdi)
	movq	%rax, (%rdi)
	je	.L8
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L8
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	ret
	.cfi_endproc
.LFE8870:
	.size	_ZN2v88internal14SerializedDataD2Ev, .-_ZN2v88internal14SerializedDataD2Ev
	.weak	_ZN2v88internal14SerializedDataD1Ev
	.set	_ZN2v88internal14SerializedDataD1Ev,_ZN2v88internal14SerializedDataD2Ev
	.section	.text._ZN2v88internal14SerializedDataD0Ev,"axG",@progbits,_ZN2v88internal14SerializedDataD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14SerializedDataD0Ev
	.type	_ZN2v88internal14SerializedDataD0Ev, @function
_ZN2v88internal14SerializedDataD0Ev:
.LFB8872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14SerializedDataE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$0, 20(%rdi)
	movq	%rax, (%rdi)
	je	.L14
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L14
	call	_ZdaPv@PLT
.L14:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8872:
	.size	_ZN2v88internal14SerializedDataD0Ev, .-_ZN2v88internal14SerializedDataD0Ev
	.section	.text._ZN2v88internal18SerializedCodeDataD2Ev,"axG",@progbits,_ZN2v88internal18SerializedCodeDataD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18SerializedCodeDataD2Ev
	.type	_ZN2v88internal18SerializedCodeDataD2Ev, @function
_ZN2v88internal18SerializedCodeDataD2Ev:
.LFB27619:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14SerializedDataE(%rip), %rax
	cmpb	$0, 20(%rdi)
	movq	%rax, (%rdi)
	je	.L19
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L19
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L19:
	ret
	.cfi_endproc
.LFE27619:
	.size	_ZN2v88internal18SerializedCodeDataD2Ev, .-_ZN2v88internal18SerializedCodeDataD2Ev
	.weak	_ZN2v88internal18SerializedCodeDataD1Ev
	.set	_ZN2v88internal18SerializedCodeDataD1Ev,_ZN2v88internal18SerializedCodeDataD2Ev
	.section	.text._ZN2v88internal18SerializedCodeDataD0Ev,"axG",@progbits,_ZN2v88internal18SerializedCodeDataD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18SerializedCodeDataD0Ev
	.type	_ZN2v88internal18SerializedCodeDataD0Ev, @function
_ZN2v88internal18SerializedCodeDataD0Ev:
.LFB27621:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14SerializedDataE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$0, 20(%rdi)
	movq	%rax, (%rdi)
	je	.L25
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L25
	call	_ZdaPv@PLT
.L25:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27621:
	.size	_ZN2v88internal18SerializedCodeDataD0Ev, .-_ZN2v88internal18SerializedCodeDataD0Ev
	.section	.rodata._ZN2v88internal14CodeSerializerD2Ev.str1.1,"aMS",@progbits,1
.LC0:
	.string	"CodeSerializer"
	.section	.text._ZN2v88internal14CodeSerializerD2Ev,"axG",@progbits,_ZN2v88internal14CodeSerializerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CodeSerializerD2Ev
	.type	_ZN2v88internal14CodeSerializerD2Ev, @function
_ZN2v88internal14CodeSerializerD2Ev:
.LFB9001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CodeSerializerE(%rip), %rax
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal10Serializer16OutputStatisticsEPKc@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10SerializerD2Ev@PLT
	.cfi_endproc
.LFE9001:
	.size	_ZN2v88internal14CodeSerializerD2Ev, .-_ZN2v88internal14CodeSerializerD2Ev
	.weak	_ZN2v88internal14CodeSerializerD1Ev
	.set	_ZN2v88internal14CodeSerializerD1Ev,_ZN2v88internal14CodeSerializerD2Ev
	.section	.text._ZN2v88internal14CodeSerializerD0Ev,"axG",@progbits,_ZN2v88internal14CodeSerializerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CodeSerializerD0Ev
	.type	_ZN2v88internal14CodeSerializerD0Ev, @function
_ZN2v88internal14CodeSerializerD0Ev:
.LFB9003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CodeSerializerE(%rip), %rax
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal10Serializer16OutputStatisticsEPKc@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal10SerializerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$368, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9003:
	.size	_ZN2v88internal14CodeSerializerD0Ev, .-_ZN2v88internal14CodeSerializerD0Ev
	.section	.text._ZN2v88internal6Logger27is_listening_to_code_eventsEv,"axG",@progbits,_ZN2v88internal6Logger27is_listening_to_code_eventsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6Logger27is_listening_to_code_eventsEv
	.type	_ZN2v88internal6Logger27is_listening_to_code_eventsEv, @function
_ZN2v88internal6Logger27is_listening_to_code_eventsEv:
.LFB8721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L34
	cmpq	$0, 80(%rbx)
	setne	%al
.L34:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8721:
	.size	_ZN2v88internal6Logger27is_listening_to_code_eventsEv, .-_ZN2v88internal6Logger27is_listening_to_code_eventsEv
	.section	.text._ZN2v88internal18SharedFunctionInfo24set_script_or_debug_infoENS0_6ObjectENS0_16WriteBarrierModeE,"axG",@progbits,_ZN2v88internal18SharedFunctionInfo24set_script_or_debug_infoENS0_6ObjectENS0_16WriteBarrierModeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18SharedFunctionInfo24set_script_or_debug_infoENS0_6ObjectENS0_16WriteBarrierModeE
	.type	_ZN2v88internal18SharedFunctionInfo24set_script_or_debug_infoENS0_6ObjectENS0_16WriteBarrierModeE, @function
_ZN2v88internal18SharedFunctionInfo24set_script_or_debug_infoENS0_6ObjectENS0_16WriteBarrierModeE:
.LFB19135:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	%rsi, 31(%rax)
	testl	%edx, %edx
	je	.L53
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%r12, %rax
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	notq	%rax
	leaq	31(%rdi), %rsi
	andl	$1, %eax
	cmpl	$4, %edx
	je	.L56
	testb	%al, %al
	jne	.L37
.L48:
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L37
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L37
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r12, %rdx
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	testb	%al, %al
	jne	.L37
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L48
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	leaq	31(%rdi), %rsi
	jmp	.L48
	.cfi_endproc
.LFE19135:
	.size	_ZN2v88internal18SharedFunctionInfo24set_script_or_debug_infoENS0_6ObjectENS0_16WriteBarrierModeE, .-_ZN2v88internal18SharedFunctionInfo24set_script_or_debug_infoENS0_6ObjectENS0_16WriteBarrierModeE
	.section	.text._ZN2v88internal18SharedFunctionInfo21SetDebugBytecodeArrayENS0_13BytecodeArrayE,"axG",@progbits,_ZN2v88internal18SharedFunctionInfo21SetDebugBytecodeArrayENS0_13BytecodeArrayE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18SharedFunctionInfo21SetDebugBytecodeArrayENS0_13BytecodeArrayE
	.type	_ZN2v88internal18SharedFunctionInfo21SetDebugBytecodeArrayENS0_13BytecodeArrayE, @function
_ZN2v88internal18SharedFunctionInfo21SetDebugBytecodeArrayENS0_13BytecodeArrayE:
.LFB19218:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	7(%rax), %rdx
	movq	%rsi, %rax
	notq	%rax
	andl	$1, %eax
	testb	$1, %dl
	jne	.L80
.L58:
	movq	(%rbx), %rdx
	movq	7(%rdx), %r13
	movq	%r12, 7(%r13)
	leaq	7(%r13), %r14
	testb	%al, %al
	jne	.L57
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L81
	testb	$24, %al
	je	.L57
.L83:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L82
.L57:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L83
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L80:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L58
	movq	(%rdi), %rdx
	movq	%rsi, 7(%rdx)
	testb	%al, %al
	jne	.L57
	movq	%r12, %r13
	movq	(%rdi), %rdi
	andq	$-262144, %r13
	movq	8(%r13), %rax
	leaq	7(%rdi), %rsi
	testl	$262144, %eax
	jne	.L84
.L60:
	testb	$24, %al
	je	.L57
	movq	%rdi, %rax
	movq	%r12, %rdx
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L57
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
.L79:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	movq	8(%r13), %rax
	leaq	7(%rdi), %rsi
	jmp	.L60
	.cfi_endproc
.LFE19218:
	.size	_ZN2v88internal18SharedFunctionInfo21SetDebugBytecodeArrayENS0_13BytecodeArrayE, .-_ZN2v88internal18SharedFunctionInfo21SetDebugBytecodeArrayENS0_13BytecodeArrayE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB21481:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L91
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L94
.L85:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L85
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE21481:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internal10ScriptDataC2EPKhi.str1.1,"aMS",@progbits,1
.LC1:
	.string	"NewArray"
	.section	.text._ZN2v88internal10ScriptDataC2EPKhi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ScriptDataC2EPKhi
	.type	_ZN2v88internal10ScriptDataC2EPKhi, @function
_ZN2v88internal10ScriptDataC2EPKhi:
.LFB22069:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rsi, 8(%rdi)
	andb	$-4, (%rdi)
	movl	%edx, 16(%rdi)
	testb	$7, %sil
	jne	.L106
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	movslq	%edx, %r12
	movq	%rsi, %r13
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L107
.L97:
	testq	%r12, %r12
	jne	.L108
	orb	$1, (%rbx)
	movq	%rcx, 8(%rbx)
.L109:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movq	%rcx, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	orb	$1, (%rbx)
	movq	%rax, %rcx
	movq	%rcx, 8(%rbx)
	jmp	.L109
.L107:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	jne	.L97
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE22069:
	.size	_ZN2v88internal10ScriptDataC2EPKhi, .-_ZN2v88internal10ScriptDataC2EPKhi
	.globl	_ZN2v88internal10ScriptDataC1EPKhi
	.set	_ZN2v88internal10ScriptDataC1EPKhi,_ZN2v88internal10ScriptDataC2EPKhi
	.section	.text._ZN2v88internal14CodeSerializerC2EPNS0_7IsolateEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14CodeSerializerC2EPNS0_7IsolateEj
	.type	_ZN2v88internal14CodeSerializerC2EPNS0_7IsolateEj, @function
_ZN2v88internal14CodeSerializerC2EPNS0_7IsolateEj:
.LFB22072:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN2v88internal10SerializerC2EPNS0_7IsolateE@PLT
	leaq	16+_ZTVN2v88internal14CodeSerializerE(%rip), %rax
	movl	%r12d, 360(%rbx)
	movl	_ZN2v88internal29FLAG_serialization_chunk_sizeE(%rip), %esi
	movq	%rax, (%rbx)
	leaq	216(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal19SerializerAllocator18UseCustomChunkSizeEj@PLT
	.cfi_endproc
.LFE22072:
	.size	_ZN2v88internal14CodeSerializerC2EPNS0_7IsolateEj, .-_ZN2v88internal14CodeSerializerC2EPNS0_7IsolateEj
	.globl	_ZN2v88internal14CodeSerializerC1EPNS0_7IsolateEj
	.set	_ZN2v88internal14CodeSerializerC1EPNS0_7IsolateEj,_ZN2v88internal14CodeSerializerC2EPNS0_7IsolateEj
	.section	.text._ZN2v88internal14CodeSerializer16SerializeGenericENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14CodeSerializer16SerializeGenericENS0_10HeapObjectE
	.type	_ZN2v88internal14CodeSerializer16SerializeGenericENS0_10HeapObjectE, @function
_ZN2v88internal14CodeSerializer16SerializeGenericENS0_10HeapObjectE:
.LFB22078:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -40(%rbp)
	addq	$80, %rdi
	leaq	16+_ZTVN2v88internal10Serializer16ObjectSerializerE(%rip), %rax
	movq	%rdi, -24(%rbp)
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	movq	%rsi, -32(%rbp)
	movl	$0, -16(%rbp)
	call	_ZN2v88internal10Serializer16ObjectSerializer9SerializeEv@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L115
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L115:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22078:
	.size	_ZN2v88internal14CodeSerializer16SerializeGenericENS0_10HeapObjectE, .-_ZN2v88internal14CodeSerializer16SerializeGenericENS0_10HeapObjectE
	.section	.text._ZN2v88internal40CreateInterpreterDataForDeserializedCodeEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEEb,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal40CreateInterpreterDataForDeserializedCodeEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEEb
	.type	_ZN2v88internal40CreateInterpreterDataForDeserializedCodeEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEEb, @function
_ZN2v88internal40CreateInterpreterDataForDeserializedCodeEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEEb:
.LFB22079:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -104(%rbp)
	movb	%dl, -121(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L183
.L117:
	movq	-104(%rbp), %rax
	movq	%rsi, -96(%rbp)
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L118
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L119:
	movq	-104(%rbp), %rax
	movq	128(%rax), %rsi
	movq	-96(%rbp), %rax
	movq	15(%rax), %rax
	testb	$1, %al
	jne	.L184
.L121:
	movq	-104(%rbp), %rax
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L122
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -152(%rbp)
.L123:
	movq	-104(%rbp), %r14
	leaq	-80(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	movq	%r14, %rsi
	call	_ZN2v88internal18SharedFunctionInfo14ScriptIteratorC1EPNS0_7IsolateENS0_6ScriptE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal18SharedFunctionInfo14ScriptIterator4NextEv@PLT
	leaq	4776(%r14), %rsi
	movq	%rax, -88(%rbp)
	movq	%rsi, -136(%rbp)
	testq	%rax, %rax
	jne	.L125
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L129:
	movq	-88(%rbp), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L185
.L147:
	movq	-120(%rbp), %rdi
	call	_ZN2v88internal18SharedFunctionInfo14ScriptIterator4NextEv@PLT
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L116
.L125:
	movq	7(%rax), %rax
	testb	$1, %al
	je	.L129
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L129
.L132:
	movq	-104(%rbp), %rbx
	movq	-136(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory8CopyCodeENS0_6HandleINS0_4CodeEEE@PLT
	movl	$1, %edx
	movq	%rbx, %rdi
	movl	$91, %esi
	movq	%rax, %r12
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE@PLT
	movq	-88(%rbp), %rdx
	movq	(%rax), %r14
	movq	%rax, %rbx
	movq	31(%rdx), %rax
	testb	$1, %al
	jne	.L186
.L130:
	movq	7(%rdx), %rax
	testb	$1, %al
	jne	.L187
.L136:
	movq	-88(%rbp), %rax
	movq	7(%rax), %rax
	movq	7(%rax), %r13
.L135:
	movq	%r13, 7(%r14)
	leaq	7(%r14), %r15
	testb	$1, %r13b
	je	.L152
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -112(%rbp)
	testl	$262144, %eax
	je	.L138
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-112(%rbp), %rcx
	movq	8(%rcx), %rax
.L138:
	testb	$24, %al
	je	.L152
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L152
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L152:
	movq	(%rbx), %r14
	movq	(%r12), %r13
	movq	%r13, 15(%r14)
	leaq	15(%r14), %r15
	testb	$1, %r13b
	je	.L151
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -112(%rbp)
	testl	$262144, %eax
	je	.L141
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-112(%rbp), %rcx
	movq	8(%rcx), %rax
.L141:
	testb	$24, %al
	je	.L151
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L151
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L151:
	movq	(%rbx), %r13
	movq	-88(%rbp), %rax
	movq	%r13, 7(%rax)
	testb	$1, %r13b
	je	.L150
	movq	%r13, %rbx
	movq	-88(%rbp), %rdi
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	leaq	7(%rdi), %rsi
	testl	$262144, %eax
	je	.L144
	movq	%r13, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rdi
	movq	8(%rbx), %rax
	leaq	7(%rdi), %rsi
.L144:
	testb	$24, %al
	je	.L150
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L150
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L150:
	cmpb	$0, -121(%rbp)
	je	.L147
	leaq	-88(%rbp), %r14
	leaq	-96(%rbp), %r13
	movq	%r14, %rdi
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal6Script13GetLineNumberEi@PLT
	movq	%r14, %rdi
	leal	1(%rax), %ebx
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal6Script15GetColumnNumberEi@PLT
	movq	(%r12), %r13
	movq	-88(%rbp), %r12
	addl	$1, %eax
	movl	%eax, -112(%rbp)
	movq	-104(%rbp), %rax
	movq	41488(%rax), %r15
	movq	-152(%rbp), %rax
	movq	(%rax), %r14
	leaq	56(%r15), %rax
	movq	%rax, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%r15), %r15
	testq	%r15, %r15
	je	.L148
	movq	%r13, %rax
	movq	%r12, %r13
	movq	%r14, %r12
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L146:
	movq	8(%r15), %rdi
	movl	-112(%rbp), %edx
	subq	$8, %rsp
	movl	%ebx, %r9d
	movq	%r12, %r8
	movq	%r13, %rcx
	movl	$12, %esi
	movq	(%rdi), %rax
	pushq	%rdx
	movq	%r14, %rdx
	call	*40(%rax)
	movq	(%r15), %r15
	popq	%rax
	popq	%rdx
	testq	%r15, %r15
	jne	.L146
.L148:
	movq	-144(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L185:
	movq	-1(%rax), %rax
	cmpw	$91, 11(%rax)
	jne	.L147
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L186:
	movq	-1(%rax), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L130
	movq	39(%rax), %rcx
	testb	$1, %cl
	je	.L130
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L130
	movq	31(%rax), %r13
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L187:
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L136
	movq	-88(%rbp), %rax
	movq	7(%rax), %r13
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L116:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L188
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	movq	41088(%rax), %rcx
	movq	%rcx, -152(%rbp)
	cmpq	41096(%rax), %rcx
	je	.L189
.L124:
	movq	-152(%rbp), %rdx
	movq	-104(%rbp), %rcx
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%rdx)
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L118:
	movq	%rax, %rdx
	movq	41088(%rax), %rax
	cmpq	41096(%rdx), %rax
	je	.L190
.L120:
	movq	-104(%rbp), %rcx
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rcx)
	movq	%rsi, (%rax)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L184:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	cmovbe	%rax, %rsi
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L183:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L117
	movq	23(%rsi), %rsi
	jmp	.L117
.L190:
	movq	%rdx, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	jmp	.L120
.L189:
	movq	%rax, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	%rax, -152(%rbp)
	jmp	.L124
.L188:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22079:
	.size	_ZN2v88internal40CreateInterpreterDataForDeserializedCodeEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEEb, .-_ZN2v88internal40CreateInterpreterDataForDeserializedCodeEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEEb
	.section	.text._ZN2v88internal18SerializedCodeDataC2EPKSt6vectorIhSaIhEEPKNS0_14CodeSerializerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18SerializedCodeDataC2EPKSt6vectorIhSaIhEEPKNS0_14CodeSerializerE
	.type	_ZN2v88internal18SerializedCodeDataC2EPKSt6vectorIhSaIhEEPKNS0_14CodeSerializerE, @function
_ZN2v88internal18SerializedCodeDataC2EPKSt6vectorIhSaIhEEPKNS0_14CodeSerializerE:
.LFB22088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	leaq	216(%rdx), %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal18SerializedCodeDataE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movl	$0, 16(%rdi)
	movb	$0, 20(%rdi)
	leaq	-80(%rbp), %rdi
	call	_ZNK2v88internal19SerializerAllocator18EncodeReservationsEv@PLT
	movq	-72(%rbp), %r14
	subq	-80(%rbp), %r14
	movq	%rbx, %rdi
	leal	39(%r14), %r13d
	movq	8(%r12), %rsi
	subq	(%r12), %rsi
	andl	$-8, %r13d
	addl	%r13d, %esi
	call	_ZN2v88internal14SerializedData12AllocateDataEj@PLT
	movq	8(%rbx), %rdi
	movq	%r13, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	movq	8(%rbx), %rax
	movl	$-1059191884, (%rax)
	movl	_ZN2v88internal7Version6major_E(%rip), %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movl	_ZN2v88internal7Version6minor_E(%rip), %edi
	movq	%rax, -104(%rbp)
	call	_ZN2v84base10hash_valueEj@PLT
	movl	_ZN2v88internal7Version6build_E(%rip), %edi
	movq	%rax, -96(%rbp)
	call	_ZN2v84base10hash_valueEj@PLT
	movl	_ZN2v88internal7Version6patch_E(%rip), %edi
	movq	%rax, -88(%rbp)
	call	_ZN2v84base10hash_valueEj@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	-88(%rbp), %r10
	movq	%rax, %rdi
	movq	%r10, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	-96(%rbp), %r9
	movq	%rax, %rdi
	movq	%r9, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	-104(%rbp), %r8
	movq	%rax, %rdi
	movq	%r8, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	8(%rbx), %rdx
	movl	%eax, 4(%rdx)
	movl	360(%r15), %edx
	movq	8(%rbx), %rax
	movl	%edx, 8(%rax)
	call	_ZN2v88internal8FlagList4HashEv@PLT
	movq	8(%rbx), %rdx
	movl	%eax, 12(%rdx)
	movq	8(%rbx), %rdx
	movq	-72(%rbp), %rax
	subq	-80(%rbp), %rax
	sarq	$2, %rax
	movl	%eax, 16(%rdx)
	movq	8(%rbx), %rdx
	movq	8(%r12), %rax
	subq	(%r12), %rax
	movl	%eax, 20(%rdx)
	movq	%r14, %rdx
	movq	8(%rbx), %rdi
	andl	$4294967295, %edx
	jne	.L218
	movq	(%r12), %rsi
	movq	8(%r12), %rdx
	subq	%rsi, %rdx
	jne	.L219
.L195:
	movl	16(%rbx), %ecx
	leaq	32(%rdi), %rax
	leal	-25(%rcx), %edx
	subl	$32, %ecx
	cmovns	%ecx, %edx
	sarl	$3, %edx
	movslq	%edx, %rdx
	leaq	(%rax,%rdx,8), %rsi
	cmpq	%rax, %rsi
	jbe	.L202
	xorl	%ecx, %ecx
	movl	$1, %edx
	.p2align 4,,10
	.p2align 3
.L199:
	addq	$8, %rax
	addq	-8(%rax), %rdx
	addq	%rdx, %rcx
	cmpq	%rax, %rsi
	ja	.L199
	movq	%rdx, %rax
	shrq	$32, %rax
	xorl	%eax, %edx
	movq	%rcx, %rax
	shrq	$32, %rax
	xorl	%eax, %ecx
.L198:
	movl	%edx, 24(%rdi)
	movq	8(%rbx), %rax
	movl	%ecx, 28(%rax)
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L191
	call	_ZdlPv@PLT
.L191:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L220
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	movq	-80(%rbp), %rsi
	leaq	32(%rdi), %r8
	testl	$4294967288, %r14d
	jne	.L193
	xorl	%eax, %eax
.L194:
	movzbl	(%rsi,%rax), %ecx
	movb	%cl, 32(%rdi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L194
	movq	8(%rbx), %rdi
.L221:
	movq	(%r12), %rsi
	movq	8(%r12), %rdx
	subq	%rsi, %rdx
	je	.L195
.L219:
	addq	%r13, %rdi
	cmpq	$7, %rdx
	ja	.L196
	xorl	%eax, %eax
.L197:
	movzbl	(%rsi,%rax), %ecx
	movb	%cl, (%rdi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L197
	movq	8(%rbx), %rdi
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L193:
	movq	%r8, %rdi
	call	memcpy@PLT
	movq	8(%rbx), %rdi
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L196:
	call	memcpy@PLT
	movq	8(%rbx), %rdi
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L202:
	xorl	%ecx, %ecx
	movl	$1, %edx
	jmp	.L198
.L220:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22088:
	.size	_ZN2v88internal18SerializedCodeDataC2EPKSt6vectorIhSaIhEEPKNS0_14CodeSerializerE, .-_ZN2v88internal18SerializedCodeDataC2EPKSt6vectorIhSaIhEEPKNS0_14CodeSerializerE
	.globl	_ZN2v88internal18SerializedCodeDataC1EPKSt6vectorIhSaIhEEPKNS0_14CodeSerializerE
	.set	_ZN2v88internal18SerializedCodeDataC1EPKSt6vectorIhSaIhEEPKNS0_14CodeSerializerE,_ZN2v88internal18SerializedCodeDataC2EPKSt6vectorIhSaIhEEPKNS0_14CodeSerializerE
	.section	.text._ZNK2v88internal18SerializedCodeData11SanityCheckEPNS0_7IsolateEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal18SerializedCodeData11SanityCheckEPNS0_7IsolateEj
	.type	_ZNK2v88internal18SerializedCodeData11SanityCheckEPNS0_7IsolateEj, @function
_ZNK2v88internal18SerializedCodeData11SanityCheckEPNS0_7IsolateEj:
.LFB22090:
	.cfi_startproc
	endbr64
	cmpl	$31, 16(%rdi)
	movl	$7, %eax
	jbe	.L241
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	cmpl	$-1059191884, (%rsi)
	je	.L244
.L222:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	movl	8(%rsi), %eax
	movl	12(%rsi), %ecx
	movl	%edx, -56(%rbp)
	movl	_ZN2v88internal7Version6major_E(%rip), %edi
	movl	20(%rsi), %edx
	movl	%eax, -52(%rbp)
	movl	24(%rsi), %eax
	movl	%ecx, -60(%rbp)
	movl	4(%rsi), %ebx
	movl	%eax, -72(%rbp)
	movl	28(%rsi), %eax
	movl	%edx, -64(%rbp)
	movl	%eax, -68(%rbp)
	call	_ZN2v84base10hash_valueEj@PLT
	movl	_ZN2v88internal7Version6minor_E(%rip), %edi
	movq	%rax, %r14
	call	_ZN2v84base10hash_valueEj@PLT
	movl	_ZN2v88internal7Version6build_E(%rip), %edi
	movq	%rax, %r13
	call	_ZN2v84base10hash_valueEj@PLT
	movl	_ZN2v88internal7Version6patch_E(%rip), %edi
	movq	%rax, %r12
	call	_ZN2v84base10hash_valueEj@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%rax, %r8
	movl	$2, %eax
	cmpl	%r8d, %ebx
	jne	.L222
	movl	-56(%rbp), %edx
	movl	$3, %eax
	cmpl	-52(%rbp), %edx
	jne	.L222
	call	_ZN2v88internal8FlagList4HashEv@PLT
	movl	%eax, %r8d
	movl	$5, %eax
	cmpl	-60(%rbp), %r8d
	jne	.L222
	movq	8(%r15), %rdx
	movl	16(%r15), %esi
	movl	16(%rdx), %eax
	movl	%esi, %ecx
	leal	39(,%rax,4), %eax
	andl	$-8, %eax
	subl	%eax, %ecx
	movl	$8, %eax
	cmpl	-64(%rbp), %ecx
	jb	.L222
	leaq	32(%rdx), %rax
	leal	-25(%rsi), %edx
	subl	$32, %esi
	cmovns	%esi, %edx
	sarl	$3, %edx
	movslq	%edx, %rdx
	leaq	(%rax,%rdx,8), %rsi
	cmpq	%rax, %rsi
	jbe	.L233
	xorl	%ecx, %ecx
	movl	$1, %edx
	.p2align 4,,10
	.p2align 3
.L225:
	addq	$8, %rax
	addq	-8(%rax), %rdx
	addq	%rdx, %rcx
	cmpq	%rax, %rsi
	ja	.L225
	movq	%rdx, %rax
	shrq	$32, %rax
	xorl	%eax, %edx
	movq	%rcx, %rax
	shrq	$32, %rax
	xorl	%eax, %ecx
.L224:
	cmpl	%ecx, -68(%rbp)
	jne	.L235
	cmpl	%edx, -72(%rbp)
	je	.L234
.L235:
	movl	$6, %eax
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L241:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
.L234:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	xorl	%eax, %eax
	jmp	.L222
.L233:
	xorl	%ecx, %ecx
	movl	$1, %edx
	jmp	.L224
	.cfi_endproc
.LFE22090:
	.size	_ZNK2v88internal18SerializedCodeData11SanityCheckEPNS0_7IsolateEj, .-_ZNK2v88internal18SerializedCodeData11SanityCheckEPNS0_7IsolateEj
	.section	.rodata._ZN2v88internal14CodeSerializer11DeserializeEPNS0_7IsolateEPNS0_10ScriptDataENS0_6HandleINS0_6StringEEENS_19ScriptOriginOptionsE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"[Cached code failed check]\n"
.LC3:
	.string	"[Deserializing failed]\n"
	.section	.rodata._ZN2v88internal14CodeSerializer11DeserializeEPNS0_7IsolateEPNS0_10ScriptDataENS0_6HandleINS0_6StringEEENS_19ScriptOriginOptionsE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"[Deserializing from %d bytes took %0.3f ms]\n"
	.section	.rodata._ZN2v88internal14CodeSerializer11DeserializeEPNS0_7IsolateEPNS0_10ScriptDataENS0_6HandleINS0_6StringEEENS_19ScriptOriginOptionsE.str1.1
.LC5:
	.string	"deserialize"
	.section	.text._ZN2v88internal14CodeSerializer11DeserializeEPNS0_7IsolateEPNS0_10ScriptDataENS0_6HandleINS0_6StringEEENS_19ScriptOriginOptionsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14CodeSerializer11DeserializeEPNS0_7IsolateEPNS0_10ScriptDataENS0_6HandleINS0_6StringEEENS_19ScriptOriginOptionsE
	.type	_ZN2v88internal14CodeSerializer11DeserializeEPNS0_7IsolateEPNS0_10ScriptDataENS0_6HandleINS0_6StringEEENS_19ScriptOriginOptionsE, @function
_ZN2v88internal14CodeSerializer11DeserializeEPNS0_7IsolateEPNS0_10ScriptDataENS0_6HandleINS0_6StringEEENS_19ScriptOriginOptionsE:
.LFB22080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal28FLAG_profile_deserializationE(%rip)
	jne	.L246
	cmpb	$0, _ZN2v88internal24FLAG_log_function_eventsE(%rip)
	movq	$0, -168(%rbp)
	jne	.L246
.L247:
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	leaq	-80(%rbp), %rbx
	movq	%r15, %rsi
	movq	8(%r12), %rcx
	movq	%rbx, %rdi
	movq	%rax, -160(%rbp)
	movq	41096(%r15), %rax
	movq	%rax, -152(%rbp)
	movq	(%r14), %rax
	movl	11(%rax), %edx
	movq	%rcx, -72(%rbp)
	leaq	16+_ZTVN2v88internal18SerializedCodeDataE(%rip), %rcx
	movq	%rcx, -80(%rbp)
	movl	%edx, %eax
	movb	$0, -60(%rbp)
	orl	$-2147483648, %eax
	andl	$8, %r13d
	cmovne	%eax, %edx
	movl	16(%r12), %eax
	movl	%eax, -64(%rbp)
	call	_ZNK2v88internal18SerializedCodeData11SanityCheckEPNS0_7IsolateEj
	leaq	16+_ZTVN2v88internal18SerializedCodeDataE(%rip), %rcx
	testl	%eax, %eax
	movl	%eax, %r13d
	je	.L249
	orb	$2, (%r12)
	leaq	16+_ZTVN2v88internal14SerializedDataE(%rip), %rbx
	cmpb	$0, -60(%rbp)
	movq	$0, -104(%rbp)
	movl	$0, -96(%rbp)
	movb	$0, -92(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%rbx, -80(%rbp)
	je	.L323
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L323
	call	_ZdaPv@PLT
.L323:
	cmpb	$0, _ZN2v88internal28FLAG_profile_deserializationE(%rip)
	jne	.L364
.L254:
	movq	40960(%r15), %rax
	movl	%r13d, %esi
	xorl	%r12d, %r12d
	leaq	168(%rax), %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
.L255:
	cmpb	$0, -92(%rbp)
	movq	%rbx, -112(%rbp)
	je	.L314
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L314
	call	_ZdaPv@PLT
.L314:
	movq	-160(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-152(%rbp), %rax
	cmpq	%rax, 41096(%r15)
	je	.L316
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L316:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L365
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L246:
	.cfi_restore_state
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%rax, -168(%rbp)
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L249:
	movq	-72(%rbp), %rax
	leaq	-112(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rcx, -112(%rbp)
	movq	%rax, -104(%rbp)
	movl	-64(%rbp), %eax
	movl	%eax, -96(%rbp)
	movzbl	-60(%rbp), %eax
	movb	%al, -92(%rbp)
	call	_ZN2v88internal18ObjectDeserializer29DeserializeSharedFunctionInfoEPNS0_7IsolateEPKNS0_18SerializedCodeDataENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L366
	cmpb	$0, _ZN2v88internal28FLAG_profile_deserializationE(%rip)
	jne	.L367
.L259:
	movq	41016(%r15), %r12
	leaq	_ZN2v88internal6Logger27is_listening_to_code_eventsEv(%rip), %rdx
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	136(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L260
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L368
.L261:
	movzbl	_ZN2v88internal36FLAG_interpreted_frames_native_stackE(%rip), %r12d
	testb	%r12b, %r12b
	je	.L369
	movq	-176(%rbp), %rsi
	movl	$1, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal40CreateInterpreterDataForDeserializedCodeEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEEb
	movq	%r15, %rdi
	call	_ZNK2v88internal7Isolate32NeedsSourcePositionsForProfilingEv@PLT
	movb	%al, -185(%rbp)
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L364:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L369:
	movq	%r15, %rdi
	movl	$1, %r12d
	call	_ZNK2v88internal7Isolate32NeedsSourcePositionsForProfilingEv@PLT
	movb	%al, -185(%rbp)
.L317:
	movq	-176(%rbp), %rax
	movq	(%rax), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L370
.L267:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L268
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -200(%rbp)
	movq	(%rax), %rsi
.L269:
	movq	15(%rsi), %rax
	testb	$1, %al
	jne	.L371
.L271:
	movq	128(%r15), %rsi
.L272:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L273
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -208(%rbp)
.L274:
	cmpb	$0, _ZN2v88internal24FLAG_log_function_eventsE(%rip)
	jne	.L372
.L277:
	testb	%r12b, %r12b
	jne	.L373
.L266:
	movq	-176(%rbp), %rax
	cmpb	$0, -185(%rbp)
	movq	(%rax), %r13
	jne	.L374
.L305:
	movq	-160(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-152(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L310
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L310:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L311
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L312:
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	leaq	16+_ZTVN2v88internal14SerializedDataE(%rip), %rbx
	movq	%rax, -160(%rbp)
	movq	41096(%r15), %rax
	movq	%rax, -152(%rbp)
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L368:
	cmpq	$0, 80(%r12)
	jne	.L261
.L263:
	movzbl	41812(%r15), %r12d
	testb	%r12b, %r12b
	jne	.L261
	movq	41488(%r15), %rax
	movq	16(%rax), %r13
	testq	%r13, %r13
	jne	.L265
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L375:
	movq	0(%r13), %r13
	testq	%r13, %r13
	je	.L264
.L265:
	movq	8(%r13), %rdi
	movq	(%rdi), %rax
	call	*136(%rax)
	testb	%al, %al
	je	.L375
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L367:
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%rbx, %rdi
	subq	-168(%rbp), %rax
	movq	%rax, -80(%rbp)
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	movl	16(%r12), %esi
	movl	$1, %eax
	leaq	.LC4(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L366:
	cmpb	$0, _ZN2v88internal28FLAG_profile_deserializationE(%rip)
	jne	.L376
.L257:
	xorl	%r12d, %r12d
	leaq	16+_ZTVN2v88internal14SerializedDataE(%rip), %rbx
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L311:
	movq	41088(%r15), %r12
	cmpq	41096(%r15), %r12
	je	.L377
.L313:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r15)
	movq	%r13, (%r12)
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L374:
	movq	31(%r13), %rsi
	testb	$1, %sil
	jne	.L378
.L306:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L307
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L308:
	call	_ZN2v88internal6Script12InitLineEndsENS0_6HandleIS1_EE@PLT
	movq	-176(%rbp), %rax
	movq	(%rax), %r13
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L273:
	movq	41088(%r15), %rax
	movq	%rax, -208(%rbp)
	cmpq	41096(%r15), %rax
	je	.L379
.L275:
	movq	-208(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rcx)
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L268:
	movq	41088(%r15), %rax
	movq	%rax, -200(%rbp)
	cmpq	41096(%r15), %rax
	je	.L380
.L270:
	movq	-200(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rcx)
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L373:
	movq	-200(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal6Script12InitLineEndsENS0_6HandleIS1_EE@PLT
	movq	(%r14), %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal18SharedFunctionInfo14ScriptIteratorC1EPNS0_7IsolateENS0_6ScriptE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal18SharedFunctionInfo14ScriptIterator4NextEv@PLT
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L266
	movq	%r15, -184(%rbp)
	movq	%rbx, -216(%rbp)
	.p2align 4,,10
	.p2align 3
.L279:
	movabsq	$287762808832, %rbx
	movq	7(%rax), %rax
	cmpq	%rbx, %rax
	je	.L281
	testb	$1, %al
	jne	.L282
.L286:
	movq	-184(%rbp), %rax
	movq	-136(%rbp), %rsi
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L381
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L287:
	cmpb	$0, -185(%rbp)
	jne	.L382
.L289:
	movq	-200(%rbp), %r15
	leaq	-120(%rbp), %r14
	leaq	-128(%rbp), %r13
	movq	%r14, %rdi
	movq	(%r15), %rax
	movq	%rax, -128(%rbp)
	movq	(%r12), %rax
	movq	%rax, -120(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal6Script13GetLineNumberEi@PLT
	movq	%r14, %rdi
	leal	1(%rax), %ebx
	movq	(%r15), %rax
	movq	%rax, -128(%rbp)
	movq	(%r12), %rax
	movq	%rax, -120(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal6Script15GetColumnNumberEi@PLT
	movq	(%r12), %r13
	addl	$1, %eax
	movl	%eax, -168(%rbp)
	movq	-184(%rbp), %rax
	movq	41488(%rax), %r15
	movq	-208(%rbp), %rax
	movq	(%rax), %r14
	movq	-136(%rbp), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L290
.L293:
	movq	-136(%rbp), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L383
.L291:
	leaq	-136(%rbp), %rdi
	call	_ZNK2v88internal18SharedFunctionInfo7GetCodeEv@PLT
	movq	%rax, %r12
.L301:
	leaq	56(%r15), %rax
	movq	%rax, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%r15), %r15
	movq	%r12, %rax
	movq	%r14, %r12
	movq	%rax, %r14
	testq	%r15, %r15
	je	.L303
	.p2align 4,,10
	.p2align 3
.L302:
	movq	8(%r15), %rdi
	movl	-168(%rbp), %edx
	movl	%ebx, %r9d
	movq	%r12, %r8
	subq	$8, %rsp
	movq	%r13, %rcx
	movl	$17, %esi
	movq	(%rdi), %rax
	pushq	%rdx
	movq	%r14, %rdx
	call	*40(%rax)
	movq	(%r15), %r15
	popq	%rax
	popq	%rdx
	testq	%r15, %r15
	jne	.L302
.L303:
	movq	-224(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L281:
	movq	-216(%rbp), %rdi
	call	_ZN2v88internal18SharedFunctionInfo14ScriptIterator4NextEv@PLT
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	jne	.L279
	movq	-184(%rbp), %r15
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L381:
	movq	41088(%rax), %r12
	cmpq	41096(%rax), %r12
	je	.L384
.L288:
	movq	-184(%rbp), %rbx
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L282:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L281
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L286
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L290:
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L293
.L296:
	movq	-136(%rbp), %rdx
	movq	31(%rdx), %rax
	testb	$1, %al
	jne	.L385
.L294:
	movq	7(%rdx), %rax
	testb	$1, %al
	jne	.L386
.L300:
	movq	-136(%rbp), %rax
	movq	7(%rax), %rax
	movq	7(%rax), %r12
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L382:
	movq	-184(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal18SharedFunctionInfo30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L383:
	movq	-1(%rax), %rax
	cmpw	$91, 11(%rax)
	jne	.L291
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L372:
	movq	41016(%r15), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L277
	movq	-176(%rbp), %r14
	movq	-208(%rbp), %rax
	movq	%rbx, %rdi
	movq	(%rax), %r9
	movq	(%r14), %rax
	movq	%r9, -216(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo11EndPositionEv@PLT
	leaq	-120(%rbp), %rdi
	movl	%eax, -184(%rbp)
	movq	(%r14), %rax
	movq	%rax, -120(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movl	%eax, %r14d
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	leaq	-128(%rbp), %rdi
	subq	-168(%rbp), %rax
	movq	%rax, -128(%rbp)
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	movq	-200(%rbp), %rax
	movl	%r14d, %ecx
	movq	%r13, %rdi
	movq	-216(%rbp), %r9
	movl	-184(%rbp), %r8d
	leaq	.LC5(%rip), %rsi
	movq	(%rax), %rax
	movslq	67(%rax), %rdx
	call	_ZN2v88internal6Logger13FunctionEventEPKcidiiNS0_6StringE@PLT
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L371:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L271
	movq	-200(%rbp), %rax
	movq	(%rax), %rax
	movq	15(%rax), %rsi
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L370:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L267
	movq	23(%rsi), %rsi
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L260:
	call	*%rax
	testb	%al, %al
	je	.L263
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L307:
	movq	41088(%r15), %rdi
	cmpq	41096(%r15), %rdi
	je	.L387
.L309:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rdi)
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L376:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L385:
	movq	-1(%rax), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L294
	movq	39(%rax), %rcx
	testb	$1, %cl
	je	.L294
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L294
	movq	31(%rax), %r12
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L264:
	cmpb	$0, _ZN2v88internal36FLAG_interpreted_frames_native_stackE(%rip)
	jne	.L388
.L363:
	movq	%r15, %rdi
	call	_ZNK2v88internal7Isolate32NeedsSourcePositionsForProfilingEv@PLT
	cmpb	$0, _ZN2v88internal24FLAG_log_function_eventsE(%rip)
	movb	%al, -185(%rbp)
	je	.L266
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L386:
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L300
	movq	-136(%rbp), %rax
	movq	7(%rax), %r12
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L378:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L306
	movq	23(%rsi), %rsi
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L377:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r12
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L384:
	movq	%rax, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L380:
	movq	%r15, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rsi
	movq	%rax, -200(%rbp)
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L379:
	movq	%r15, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rsi
	movq	%rax, -208(%rbp)
	jmp	.L275
.L387:
	movq	%r15, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L309
.L365:
	call	__stack_chk_fail@PLT
.L388:
	movq	-176(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal40CreateInterpreterDataForDeserializedCodeEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEEb
	jmp	.L363
	.cfi_endproc
.LFE22080:
	.size	_ZN2v88internal14CodeSerializer11DeserializeEPNS0_7IsolateEPNS0_10ScriptDataENS0_6HandleINS0_6StringEEENS_19ScriptOriginOptionsE, .-_ZN2v88internal14CodeSerializer11DeserializeEPNS0_7IsolateEPNS0_10ScriptDataENS0_6HandleINS0_6StringEEENS_19ScriptOriginOptionsE
	.section	.text._ZN2v88internal18SerializedCodeData10SourceHashENS0_6HandleINS0_6StringEEENS_19ScriptOriginOptionsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18SerializedCodeData10SourceHashENS0_6HandleINS0_6StringEEENS_19ScriptOriginOptionsE
	.type	_ZN2v88internal18SerializedCodeData10SourceHashENS0_6HandleINS0_6StringEEENS_19ScriptOriginOptionsE, @function
_ZN2v88internal18SerializedCodeData10SourceHashENS0_6HandleINS0_6StringEEENS_19ScriptOriginOptionsE:
.LFB22091:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	11(%rax), %eax
	movl	%eax, %edx
	orl	$-2147483648, %edx
	andl	$8, %esi
	cmovne	%edx, %eax
	ret
	.cfi_endproc
.LFE22091:
	.size	_ZN2v88internal18SerializedCodeData10SourceHashENS0_6HandleINS0_6StringEEENS_19ScriptOriginOptionsE, .-_ZN2v88internal18SerializedCodeData10SourceHashENS0_6HandleINS0_6StringEEENS_19ScriptOriginOptionsE
	.section	.text._ZN2v88internal18SerializedCodeData13GetScriptDataEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18SerializedCodeData13GetScriptDataEv
	.type	_ZN2v88internal18SerializedCodeData13GetScriptDataEv, @function
_ZN2v88internal18SerializedCodeData13GetScriptDataEv:
.LFB22092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	$24, %edi
	call	_Znwm@PLT
	movl	16(%rbx), %edx
	movq	8(%rbx), %r14
	andb	$-4, (%rax)
	movq	%rax, %r12
	movq	%r14, 8(%rax)
	movl	%edx, 16(%rax)
	testb	$7, %r14b
	jne	.L405
.L394:
	movq	%r12, %rax
	orb	$1, (%r12)
	movb	$0, 20(%rbx)
	movq	$0, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L405:
	.cfi_restore_state
	movslq	%edx, %r13
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r13, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L406
.L395:
	testq	%r13, %r13
	jne	.L407
.L397:
	movq	%rcx, 8(%r12)
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L407:
	cmpq	$7, %r13
	ja	.L398
	xorl	%eax, %eax
.L400:
	movzbl	(%r14,%rax), %edx
	movb	%dl, (%rcx,%rax)
	addq	$1, %rax
	cmpq	%rax, %r13
	jne	.L400
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L398:
	movq	%rcx, %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	%rax, %rcx
	jmp	.L397
.L406:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r13, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	jne	.L395
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE22092:
	.size	_ZN2v88internal18SerializedCodeData13GetScriptDataEv, .-_ZN2v88internal18SerializedCodeData13GetScriptDataEv
	.section	.text._ZN2v88internal14CodeSerializer27SerializeSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14CodeSerializer27SerializeSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal14CodeSerializer27SerializeSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal14CodeSerializer27SerializeSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB22075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %rdx
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	24(%rax), %r8
	cmpq	%rdx, %r8
	jne	.L409
	leaq	8(%rsi), %r8
	xorl	%edx, %edx
	movl	$10, %esi
	call	*16(%rax)
.L410:
	movq	%r12, %rdi
	leaq	-48(%rbp), %r13
	call	_ZN2v88internal10Serializer24SerializeDeferredObjectsEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10Serializer3PadEi@PLT
	leaq	80(%r12), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal18SerializedCodeDataC1EPKSt6vectorIhSaIhEEPKNS0_14CodeSerializerE
	movq	%r13, %rdi
	call	_ZN2v88internal18SerializedCodeData13GetScriptDataEv
	leaq	16+_ZTVN2v88internal14SerializedDataE(%rip), %rcx
	cmpb	$0, -28(%rbp)
	movq	%rcx, -48(%rbp)
	je	.L408
	movq	-40(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L408
	movq	%rax, -56(%rbp)
	call	_ZdaPv@PLT
	movq	-56(%rbp), %rax
.L408:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L417
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L409:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$10, %esi
	call	*%r8
	jmp	.L410
.L417:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22075:
	.size	_ZN2v88internal14CodeSerializer27SerializeSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZN2v88internal14CodeSerializer27SerializeSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.text._ZNK2v88internal18SerializedCodeData12ReservationsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal18SerializedCodeData12ReservationsEv
	.type	_ZNK2v88internal18SerializedCodeData12ReservationsEv, @function
_ZNK2v88internal18SerializedCodeData12ReservationsEv:
.LFB22093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rsi), %rax
	movl	16(%rax), %ebx
	movq	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	leaq	0(,%rbx,4), %r14
	testq	%rbx, %rbx
	je	.L419
	movq	%r14, %rdi
	call	_Znwm@PLT
	leaq	(%rax,%r14), %rcx
	movq	%rax, (%r12)
	movq	%rax, %rdi
	leaq	-1(%rbx), %rax
	movq	%rcx, 16(%r12)
	cmpq	$2, %rax
	jbe	.L424
	movq	%rbx, %rax
	movq	%rdi, %rdx
	pxor	%xmm0, %xmm0
	shrq	$2, %rax
	salq	$4, %rax
	addq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L421:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rdx
	jne	.L421
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	andq	$-4, %rdx
	andl	$3, %esi
	leaq	(%rdi,%rdx,4), %rax
	cmpq	%rdx, %rbx
	je	.L423
.L420:
	movl	$0, (%rax)
	cmpq	$1, %rsi
	je	.L423
	movl	$0, 4(%rax)
	cmpq	$2, %rsi
	je	.L423
	movl	$0, 8(%rax)
.L423:
	movq	%rcx, 8(%r12)
	movq	8(%r13), %rsi
	movq	%r14, %rdx
	addq	$32, %rsi
	call	memcpy@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L419:
	.cfi_restore_state
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L424:
	movq	%rdi, %rax
	movq	%rbx, %rsi
	jmp	.L420
	.cfi_endproc
.LFE22093:
	.size	_ZNK2v88internal18SerializedCodeData12ReservationsEv, .-_ZNK2v88internal18SerializedCodeData12ReservationsEv
	.section	.text._ZNK2v88internal18SerializedCodeData7PayloadEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal18SerializedCodeData7PayloadEv
	.type	_ZNK2v88internal18SerializedCodeData7PayloadEv, @function
_ZNK2v88internal18SerializedCodeData7PayloadEv:
.LFB22094:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movl	16(%rax), %edx
	movslq	20(%rax), %r8
	leal	39(,%rdx,4), %edx
	andl	$-8, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rax
	movq	%r8, %rdx
	ret
	.cfi_endproc
.LFE22094:
	.size	_ZNK2v88internal18SerializedCodeData7PayloadEv, .-_ZNK2v88internal18SerializedCodeData7PayloadEv
	.section	.text._ZN2v88internal18SerializedCodeDataC2EPNS0_10ScriptDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18SerializedCodeDataC2EPNS0_10ScriptDataE
	.type	_ZN2v88internal18SerializedCodeDataC2EPNS0_10ScriptDataE, @function
_ZN2v88internal18SerializedCodeDataC2EPNS0_10ScriptDataE:
.LFB22096:
	.cfi_startproc
	endbr64
	movl	16(%rsi), %eax
	movq	8(%rsi), %rdx
	movb	$0, 20(%rdi)
	movl	%eax, 16(%rdi)
	leaq	16+_ZTVN2v88internal18SerializedCodeDataE(%rip), %rax
	movq	%rdx, 8(%rdi)
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE22096:
	.size	_ZN2v88internal18SerializedCodeDataC2EPNS0_10ScriptDataE, .-_ZN2v88internal18SerializedCodeDataC2EPNS0_10ScriptDataE
	.globl	_ZN2v88internal18SerializedCodeDataC1EPNS0_10ScriptDataE
	.set	_ZN2v88internal18SerializedCodeDataC1EPNS0_10ScriptDataE,_ZN2v88internal18SerializedCodeDataC2EPNS0_10ScriptDataE
	.section	.text._ZN2v88internal18SerializedCodeData14FromCachedDataEPNS0_7IsolateEPNS0_10ScriptDataEjPNS1_17SanityCheckResultE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18SerializedCodeData14FromCachedDataEPNS0_7IsolateEPNS0_10ScriptDataEjPNS1_17SanityCheckResultE
	.type	_ZN2v88internal18SerializedCodeData14FromCachedDataEPNS0_7IsolateEPNS0_10ScriptDataEjPNS1_17SanityCheckResultE, @function
_ZN2v88internal18SerializedCodeData14FromCachedDataEPNS0_7IsolateEPNS0_10ScriptDataEjPNS1_17SanityCheckResultE:
.LFB22098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$72, %rsp
	movl	16(%rdx), %r15d
	movq	8(%rdx), %r14
	cmpl	$31, %r15d
	jbe	.L456
	cmpl	$-1059191884, (%r14)
	je	.L457
	movl	$1, (%r8)
.L438:
	leaq	16+_ZTVN2v88internal18SerializedCodeDataE(%rip), %rax
	orb	$2, (%rbx)
	movq	$0, 8(%r12)
	movl	$0, 16(%r12)
	movb	$0, 20(%r12)
	movq	%rax, (%r12)
.L436:
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L457:
	.cfi_restore_state
	movl	8(%r14), %edx
	movl	20(%r14), %edi
	movl	%ecx, -84(%rbp)
	movl	4(%r14), %eax
	movl	12(%r14), %ecx
	movl	%edx, -56(%rbp)
	movl	24(%r14), %edx
	movl	%edi, -92(%rbp)
	movl	_ZN2v88internal7Version6major_E(%rip), %edi
	movl	%edx, -96(%rbp)
	movl	28(%r14), %edx
	movl	%ecx, -88(%rbp)
	movl	%edx, -100(%rbp)
	movl	%eax, -52(%rbp)
	call	_ZN2v84base10hash_valueEj@PLT
	movl	_ZN2v88internal7Version6minor_E(%rip), %edi
	movq	%rax, -80(%rbp)
	call	_ZN2v84base10hash_valueEj@PLT
	movl	_ZN2v88internal7Version6build_E(%rip), %edi
	movq	%rax, -72(%rbp)
	call	_ZN2v84base10hash_valueEj@PLT
	movl	_ZN2v88internal7Version6patch_E(%rip), %edi
	movq	%rax, -64(%rbp)
	call	_ZN2v84base10hash_valueEj@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	-64(%rbp), %r11
	movq	%rax, %rdi
	movq	%r11, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	-72(%rbp), %r10
	movq	%rax, %rdi
	movq	%r10, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	-80(%rbp), %r9
	movq	%rax, %rdi
	movq	%r9, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	cmpl	%eax, -52(%rbp)
	jne	.L440
	movl	-84(%rbp), %ecx
	cmpl	-56(%rbp), %ecx
	jne	.L441
	call	_ZN2v88internal8FlagList4HashEv@PLT
	cmpl	%eax, -88(%rbp)
	jne	.L442
	movl	16(%r14), %eax
	movl	%r15d, %esi
	leal	39(,%rax,4), %eax
	andl	$-8, %eax
	subl	%eax, %esi
	cmpl	%esi, -92(%rbp)
	ja	.L443
	movl	%r15d, %esi
	leal	-25(%r15), %ecx
	leaq	32(%r14), %rax
	subl	$32, %esi
	cmovns	%esi, %ecx
	sarl	$3, %ecx
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,8), %rdi
	cmpq	%rdi, %rax
	jnb	.L449
	xorl	%esi, %esi
	movl	$1, %ecx
	.p2align 4,,10
	.p2align 3
.L445:
	addq	$8, %rax
	addq	-8(%rax), %rcx
	addq	%rcx, %rsi
	cmpq	%rax, %rdi
	ja	.L445
	movq	%rcx, %rax
	shrq	$32, %rax
	xorl	%eax, %ecx
	movq	%rsi, %rax
	shrq	$32, %rax
	xorl	%eax, %esi
.L444:
	cmpl	%ecx, -96(%rbp)
	jne	.L450
	cmpl	%esi, -100(%rbp)
	je	.L446
.L450:
	movl	$6, 0(%r13)
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L456:
	movl	$7, (%r8)
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L440:
	movl	$2, 0(%r13)
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L441:
	movl	$3, 0(%r13)
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L443:
	movl	$8, 0(%r13)
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L446:
	leaq	16+_ZTVN2v88internal18SerializedCodeDataE(%rip), %rax
	movl	$0, 0(%r13)
	movq	%r14, 8(%r12)
	movl	%r15d, 16(%r12)
	movb	$0, 20(%r12)
	movq	%rax, (%r12)
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L442:
	movl	$5, 0(%r13)
	jmp	.L438
.L449:
	xorl	%esi, %esi
	movl	$1, %ecx
	jmp	.L444
	.cfi_endproc
.LFE22098:
	.size	_ZN2v88internal18SerializedCodeData14FromCachedDataEPNS0_7IsolateEPNS0_10ScriptDataEjPNS1_17SanityCheckResultE, .-_ZN2v88internal18SerializedCodeData14FromCachedDataEPNS0_7IsolateEPNS0_10ScriptDataEjPNS1_17SanityCheckResultE
	.section	.rodata._ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"Out of memory: HashMap::Initialize"
	.section	.text._ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_,"axG",@progbits,_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_
	.type	_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_, @function
_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_:
.LFB26707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	8(%rdi), %eax
	movq	(%rdi), %r13
	movl	12(%rdi), %r15d
	addl	%eax, %eax
	leaq	(%rax,%rax,2), %rdi
	movq	%rax, %rbx
	salq	$3, %rdi
	call	malloc@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L483
	movl	%ebx, 8(%r12)
	testl	%ebx, %ebx
	je	.L460
	movb	$0, 20(%rax)
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L461:
	movq	(%r12), %rcx
	addq	$1, %rax
	movb	$0, 20(%rcx,%rdx)
	movl	8(%r12), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L461
.L460:
	movl	$0, 12(%r12)
	movq	%r13, %r14
	testl	%r15d, %r15d
	je	.L467
.L462:
	cmpb	$0, 20(%r14)
	jne	.L484
.L463:
	addq	$24, %r14
	cmpb	$0, 20(%r14)
	je	.L463
.L484:
	movl	8(%r12), %eax
	movl	16(%r14), %ebx
	movq	(%r12), %rdi
	movq	(%r14), %rsi
	leal	-1(%rax), %ecx
	movl	%ebx, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 20(%rdx)
	jne	.L465
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L485:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 20(%rdx)
	je	.L464
.L465:
	cmpq	%rsi, (%rdx)
	jne	.L485
.L464:
	movl	12(%r14), %eax
	movl	8(%r14), %ecx
	movq	%rsi, (%rdx)
	movl	%ebx, 16(%rdx)
	movl	%ecx, 8(%rdx)
	movl	%eax, 12(%rdx)
	movb	$1, 20(%rdx)
	movl	12(%r12), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 12(%r12)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	8(%r12), %eax
	jnb	.L466
.L469:
	addq	$24, %r14
	subl	$1, %r15d
	jne	.L462
.L467:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L466:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_
	movl	8(%r12), %eax
	movq	(%r12), %rsi
	leal	-1(%rax), %ecx
	movl	%ebx, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 20(%rdx)
	je	.L469
	movq	(%r14), %rdi
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L486:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 20(%rdx)
	je	.L469
.L470:
	cmpq	%rdi, (%rdx)
	jne	.L486
	jmp	.L469
.L483:
	leaq	.LC6(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE26707:
	.size	_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_, .-_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_
	.section	.text._ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE14LookupOrInsertIZNS7_14LookupOrInsertERKmjS6_EUlvE_EEPNS0_20TemplateHashMapEntryImS3_EESA_jRKT_S6_,"axG",@progbits,_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE14LookupOrInsertIZNS7_14LookupOrInsertERKmjS6_EUlvE_EEPNS0_20TemplateHashMapEntryImS3_EESA_jRKT_S6_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE14LookupOrInsertIZNS7_14LookupOrInsertERKmjS6_EUlvE_EEPNS0_20TemplateHashMapEntryImS3_EESA_jRKT_S6_
	.type	_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE14LookupOrInsertIZNS7_14LookupOrInsertERKmjS6_EUlvE_EEPNS0_20TemplateHashMapEntryImS3_EESA_jRKT_S6_, @function
_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE14LookupOrInsertIZNS7_14LookupOrInsertERKmjS6_EUlvE_EEPNS0_20TemplateHashMapEntryImS3_EESA_jRKT_S6_:
.LFB24632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	subq	$16, %rsp
	movl	8(%rdi), %eax
	movq	(%rdi), %r10
	movq	(%rsi), %r11
	leal	-1(%rax), %r9d
	movl	%edx, %eax
	andl	%r9d, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%r10,%rdx,8), %r8
	cmpb	$0, 20(%r8)
	jne	.L490
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L497:
	addq	$1, %rax
	andq	%r9, %rax
	leaq	(%rax,%rax,2), %rcx
	leaq	(%r10,%rcx,8), %r8
	cmpb	$0, 20(%r8)
	je	.L488
.L490:
	cmpq	%r11, (%r8)
	jne	.L497
.L487:
	addq	$16, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L488:
	.cfi_restore_state
	movq	%r11, (%r8)
	movq	$6, 8(%r8)
	movl	%ebx, 16(%r8)
	movb	$1, 20(%r8)
	movl	12(%rdi), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 12(%rdi)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	8(%rdi), %eax
	jb	.L487
	movq	%rdi, -24(%rbp)
	call	_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_
	movq	-24(%rbp), %rdi
	movl	%ebx, %edx
	movl	8(%rdi), %eax
	movq	(%rdi), %rsi
	leal	-1(%rax), %ecx
	andl	%ecx, %edx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rsi,%rax,8), %r8
	cmpb	$0, 20(%r8)
	je	.L487
	movq	0(%r13), %rdi
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L498:
	addq	$1, %rdx
	andq	%rcx, %rdx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rsi,%rax,8), %r8
	cmpb	$0, 20(%r8)
	je	.L487
.L492:
	cmpq	%rdi, (%r8)
	jne	.L498
	jmp	.L487
	.cfi_endproc
.LFE24632:
	.size	_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE14LookupOrInsertIZNS7_14LookupOrInsertERKmjS6_EUlvE_EEPNS0_20TemplateHashMapEntryImS3_EESA_jRKT_S6_, .-_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE14LookupOrInsertIZNS7_14LookupOrInsertERKmjS6_EUlvE_EEPNS0_20TemplateHashMapEntryImS3_EESA_jRKT_S6_
	.section	.rodata._ZN2v88internal14CodeSerializer9SerializeENS0_6HandleINS0_18SharedFunctionInfoEEE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"v8"
.LC8:
	.string	"V8.Execute"
	.section	.rodata._ZN2v88internal14CodeSerializer9SerializeENS0_6HandleINS0_18SharedFunctionInfoEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"disabled-by-default-v8.compile"
	.section	.rodata._ZN2v88internal14CodeSerializer9SerializeENS0_6HandleINS0_18SharedFunctionInfoEEE.str1.1
.LC10:
	.string	"V8.CompileSerialize"
.LC11:
	.string	"[Serializing from"
.LC12:
	.string	"]\n"
	.section	.rodata._ZN2v88internal14CodeSerializer9SerializeENS0_6HandleINS0_18SharedFunctionInfoEEE.str1.8
	.align 8
.LC13:
	.string	"[Serializing to %d bytes took %0.3f ms]\n"
	.section	.text._ZN2v88internal14CodeSerializer9SerializeENS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14CodeSerializer9SerializeENS0_6HandleINS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal14CodeSerializer9SerializeENS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal14CodeSerializer9SerializeENS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB22074:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$616, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	andq	$-262144, %rax
	movq	24(%rax), %r12
	movq	_ZZN2v88internal14CodeSerializer9SerializeENS0_6HandleINS0_18SharedFunctionInfoEEEE27trace_event_unique_atomic42(%rip), %rdx
	subq	$37592, %r12
	testq	%rdx, %rdx
	je	.L558
.L501:
	movq	$0, -520(%rbp)
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L559
.L503:
	movq	40960(%r12), %rbx
	movq	2744(%rbx), %rax
	leaq	2712(%rbx), %rdi
	leaq	2760(%rbx), %rsi
	movq	%rdi, -616(%rbp)
	movq	16(%rax), %rdx
	movq	%rsi, -624(%rbp)
	call	_ZN2v88internal14TimedHistogram5StartEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	pxor	%xmm0, %xmm0
	movq	$0, -448(%rbp)
	movaps	%xmm0, -480(%rbp)
	movaps	%xmm0, -464(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L560
.L504:
	movq	_ZZN2v88internal14CodeSerializer9SerializeENS0_6HandleINS0_18SharedFunctionInfoEEEE27trace_event_unique_atomic46(%rip), %r15
	testq	%r15, %r15
	je	.L561
.L506:
	movq	$0, -560(%rbp)
	movzbl	(%r15), %eax
	testb	$5, %al
	jne	.L562
.L508:
	cmpb	$0, _ZN2v88internal28FLAG_profile_deserializationE(%rip)
	movq	$0, -648(%rbp)
	jne	.L563
.L512:
	movq	0(%r13), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L564
.L513:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L514
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	cmpb	$0, _ZN2v88internal21FLAG_trace_serializerE(%rip)
	leaq	-592(%rbp), %r15
	movq	%rax, %r14
	jne	.L565
.L517:
	movq	(%r14), %rax
	movq	%r15, %rdi
	movq	%rax, -592(%rbp)
	call	_ZN2v88internal6Script17ContainsAsmModuleEv@PLT
	testb	%al, %al
	jne	.L532
	movq	(%r14), %rax
	movq	41112(%r12), %rdi
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L519
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L520:
	movl	11(%rsi), %edx
	movq	(%r14), %rsi
	leaq	-432(%rbp), %r14
	movq	%rax, -640(%rbp)
	movq	%r14, %rdi
	movl	%edx, %ecx
	orl	$-2147483648, %ecx
	testb	$32, 99(%rsi)
	movq	%r12, %rsi
	cmovne	%ecx, %edx
	movl	%edx, -632(%rbp)
	call	_ZN2v88internal10SerializerC2EPNS0_7IsolateE@PLT
	movl	-632(%rbp), %edx
	movl	_ZN2v88internal29FLAG_serialization_chunk_sizeE(%rip), %esi
	leaq	16+_ZTVN2v88internal14CodeSerializerE(%rip), %rax
	leaq	-216(%rbp), %rdi
	movq	%rax, -432(%rbp)
	movl	%edx, -72(%rbp)
	call	_ZN2v88internal19SerializerAllocator18UseCustomChunkSizeEj@PLT
	movq	-640(%rbp), %rax
	movq	%r15, %rsi
	movl	-300(%rbp), %r12d
	leaq	-593(%rbp), %rcx
	leaq	-320(%rbp), %rdi
	movq	(%rax), %rdx
	leal	1(%r12), %eax
	movl	%eax, -300(%rbp)
	movq	%rdx, -592(%rbp)
	call	_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE14LookupOrInsertIZNS7_14LookupOrInsertERKmjS6_EUlvE_EEPNS0_20TemplateHashMapEntryImS3_EESA_jRKT_S6_
	leaq	8(%r13), %r8
	movq	%r13, %rcx
	xorl	%edx, %edx
	movl	$22, 8(%rax)
	movl	$10, %esi
	movq	%r14, %rdi
	movl	%r12d, 12(%rax)
	call	_ZN2v88internal10Serializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal10Serializer24SerializeDeferredObjectsEv@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal10Serializer3PadEi@PLT
	leaq	-352(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal18SerializedCodeDataC1EPKSt6vectorIhSaIhEEPKNS0_14CodeSerializerE
	movq	%r15, %rdi
	call	_ZN2v88internal18SerializedCodeData13GetScriptDataEv
	cmpb	$0, -572(%rbp)
	movq	%rax, %r13
	leaq	16+_ZTVN2v88internal14SerializedDataE(%rip), %rax
	movq	%rax, -592(%rbp)
	je	.L523
	movq	-584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L523
	call	_ZdaPv@PLT
.L523:
	cmpb	$0, _ZN2v88internal28FLAG_profile_deserializationE(%rip)
	jne	.L566
.L524:
	movl	16(%r13), %edx
	movq	8(%r13), %r15
	movl	$24, %edi
	movl	%edx, -632(%rbp)
	call	_Znwm@PLT
	movl	-632(%rbp), %edx
	movl	$1, %ecx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v814ScriptCompiler10CachedDataC1EPKhiNS1_12BufferPolicyE@PLT
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	leaq	16+_ZTVN2v88internal14CodeSerializerE(%rip), %rax
	movq	%r14, %rdi
	leaq	.LC0(%rip), %rsi
	movq	%rax, -432(%rbp)
	call	_ZN2v88internal10Serializer16OutputStatisticsEPKc@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal10SerializerD2Ev@PLT
.L518:
	leaq	-560(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L567
.L525:
	movq	2744(%rbx), %rax
	movq	-624(%rbp), %rsi
	movq	-616(%rbp), %rdi
	movq	16(%rax), %rdx
	call	_ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	cmpq	$0, -520(%rbp)
	jne	.L568
.L499:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L569
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L519:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L570
.L521:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L514:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L571
.L516:
	leaq	8(%r14), %rax
	leaq	-592(%rbp), %r15
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	cmpb	$0, _ZN2v88internal21FLAG_trace_serializerE(%rip)
	je	.L517
.L565:
	xorl	%eax, %eax
	leaq	.LC11(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r14), %rax
	movq	stdout(%rip), %rsi
	movq	%r15, %rdi
	movq	15(%rax), %rax
	movq	%rax, -592(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	leaq	.LC12(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L564:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L513
	movq	23(%rsi), %rsi
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L563:
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%rax, -648(%rbp)
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L562:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -432(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L572
.L509:
	movq	-424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L510
	movq	(%rdi), %rax
	call	*8(%rax)
.L510:
	movq	-432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L511
	movq	(%rdi), %rax
	call	*8(%rax)
.L511:
	leaq	.LC10(%rip), %rax
	movq	%r15, -552(%rbp)
	movq	%rax, -544(%rbp)
	leaq	-552(%rbp), %rax
	movq	%r14, -536(%rbp)
	movq	%rax, -560(%rbp)
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L561:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r15
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L573
.L507:
	movq	%r15, _ZZN2v88internal14CodeSerializer9SerializeENS0_6HandleINS0_18SharedFunctionInfoEEEE27trace_event_unique_atomic46(%rip)
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L559:
	leaq	-528(%rbp), %rdi
	leaq	.LC8(%rip), %rcx
	movq	%r12, %rsi
	call	_ZN2v88internal7tracing21CallStatsScopedTracer10InitializeEPNS0_7IsolateEPKhPKc@PLT
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L558:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L574
.L502:
	movq	%rdx, _ZZN2v88internal14CodeSerializer9SerializeENS0_6HandleINS0_18SharedFunctionInfoEEEE27trace_event_unique_atomic42(%rip)
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L566:
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%r15, %rdi
	subq	-648(%rbp), %rax
	movq	%rax, -592(%rbp)
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	movl	16(%r13), %esi
	movl	$1, %eax
	leaq	.LC13(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L532:
	xorl	%r12d, %r12d
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L568:
	movq	-512(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L499
	leaq	-528(%rbp), %rdi
	call	_ZN2v88internal7tracing21CallStatsScopedTracer16AddEndTraceEventEv@PLT
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L567:
	leaq	-472(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L560:
	movq	40960(%r12), %rax
	leaq	-472(%rbp), %rsi
	movl	$138, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -480(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L571:
	movq	%r12, %rdi
	movq	%rsi, -632(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-632(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L570:
	movq	%r12, %rdi
	movq	%rsi, -632(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-632(%rbp), %rsi
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L574:
	leaq	.LC7(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L573:
	leaq	.LC9(%rip), %rsi
	call	*%rax
	movq	%rax, %r15
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L572:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$88, %esi
	leaq	-432(%rbp), %rdx
	pushq	$0
	leaq	.LC10(%rip), %rcx
	pushq	%rdx
	movq	%r15, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L509
.L569:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22074:
	.size	_ZN2v88internal14CodeSerializer9SerializeENS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZN2v88internal14CodeSerializer9SerializeENS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.rodata._ZN2v88internal14CodeSerializer23SerializeReadOnlyObjectENS0_10HeapObjectE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"SerializeBackReference(obj)"
.LC15:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal14CodeSerializer23SerializeReadOnlyObjectENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14CodeSerializer23SerializeReadOnlyObjectENS0_10HeapObjectE
	.type	_ZN2v88internal14CodeSerializer23SerializeReadOnlyObjectENS0_10HeapObjectE, @function
_ZN2v88internal14CodeSerializer23SerializeReadOnlyObjectENS0_10HeapObjectE:
.LFB22076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal12ReadOnlyHeap8ContainsENS0_10HeapObjectE@PLT
	testb	%al, %al
	je	.L575
	movq	104(%r14), %rax
	leaq	-1(%r13), %r15
	movq	%r15, %r12
	movq	37896(%rax), %rax
	andq	$-262144, %r12
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L581
	cmpq	%rax, %r12
	je	.L581
	xorl	%edx, %edx
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L589:
	testq	%rax, %rax
	je	.L582
.L578:
	movq	224(%rax), %rax
	addl	$1, %edx
	cmpq	%rax, %r12
	jne	.L589
.L582:
	movl	%edx, %ebx
	sall	$4, %ebx
.L577:
	leaq	-64(%rbp), %rsi
	leaq	112(%r14), %rdi
	movl	%r13d, %edx
	subq	%r12, %r15
	leaq	-65(%rbp), %rcx
	movq	%r13, -64(%rbp)
	call	_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE14LookupOrInsertIZNS7_14LookupOrInsertERKmjS6_EUlvE_EEPNS0_20TemplateHashMapEntryImS3_EESA_jRKT_S6_
	movq	%r13, %rsi
	movq	%r14, %rdi
	movl	%ebx, 8(%rax)
	movl	%r15d, 12(%rax)
	call	_ZN2v88internal10Serializer22SerializeBackReferenceENS0_10HeapObjectE@PLT
	testb	%al, %al
	je	.L590
.L575:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L591
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L581:
	.cfi_restore_state
	xorl	%ebx, %ebx
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L590:
	leaq	.LC14(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L591:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22076:
	.size	_ZN2v88internal14CodeSerializer23SerializeReadOnlyObjectENS0_10HeapObjectE, .-_ZN2v88internal14CodeSerializer23SerializeReadOnlyObjectENS0_10HeapObjectE
	.section	.rodata._ZN2v88internal14CodeSerializer15SerializeObjectENS0_10HeapObjectE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"!obj.IsCode()"
.LC17:
	.string	"!obj.IsMap()"
	.section	.rodata._ZN2v88internal14CodeSerializer15SerializeObjectENS0_10HeapObjectE.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"!obj.IsJSGlobalProxy() && !obj.IsJSGlobalObject()"
	.align 8
.LC19:
	.string	"obj.NeedsRehashing() implies obj.CanBeRehashed()"
	.align 8
.LC20:
	.string	"!obj.IsJSFunction() && !obj.IsContext()"
	.section	.text._ZN2v88internal14CodeSerializer15SerializeObjectENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14CodeSerializer15SerializeObjectENS0_10HeapObjectE
	.type	_ZN2v88internal14CodeSerializer15SerializeObjectENS0_10HeapObjectE, @function
_ZN2v88internal14CodeSerializer15SerializeObjectENS0_10HeapObjectE:
.LFB22077:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal10Serializer18SerializeHotObjectENS0_10HeapObjectE@PLT
	testb	%al, %al
	je	.L683
.L592:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L684
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L683:
	.cfi_restore_state
	movq	-120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10Serializer13SerializeRootENS0_10HeapObjectE@PLT
	testb	%al, %al
	jne	.L592
	movq	-120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10Serializer22SerializeBackReferenceENS0_10HeapObjectE@PLT
	testb	%al, %al
	jne	.L592
	movq	-120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14CodeSerializer23SerializeReadOnlyObjectENS0_10HeapObjectE
	testb	%al, %al
	jne	.L592
	movq	-120(%rbp), %r13
	movq	-1(%r13), %rax
	cmpw	$69, 11(%rax)
	je	.L685
	movq	(%r12), %rax
	leaq	_ZN2v88internal14CodeSerializer11ElideObjectENS0_6ObjectE(%rip), %rdx
	movq	104(%r12), %rbx
	movq	56(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L686
.L596:
	movq	-1(%r13), %rax
	cmpw	$96, 11(%rax)
	je	.L687
	movq	-1(%r13), %rax
	cmpw	$160, 11(%rax)
	je	.L688
	cmpb	$0, _ZN2v88internal36FLAG_interpreted_frames_native_stackE(%rip)
	leaq	-1(%r13), %rax
	jne	.L689
.L621:
	movq	(%rax), %rax
	cmpw	$72, 11(%rax)
	je	.L690
.L623:
	movq	-120(%rbp), %rax
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	je	.L691
	movq	-1(%rax), %rdx
	cmpw	$1026, 11(%rdx)
	je	.L630
	movq	-1(%rax), %rax
	cmpw	$1025, 11(%rax)
	je	.L630
	leaq	-120(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNK2v88internal10HeapObject14NeedsRehashingEv@PLT
	testb	%al, %al
	jne	.L631
.L634:
	movq	-120(%rbp), %rax
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	je	.L632
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	subw	$138, %dx
	cmpw	$9, %dx
	jbe	.L632
	leaq	16+_ZTVN2v88internal10Serializer16ObjectSerializerE(%rip), %rcx
	movq	%r12, -88(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rax, -80(%rbp)
.L682:
	addq	$80, %r12
	leaq	-96(%rbp), %rdi
	movl	$0, -64(%rbp)
	movq	%r12, -72(%rbp)
	call	_ZN2v88internal10Serializer16ObjectSerializer9SerializeEv@PLT
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L686:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	movq	-120(%rbp), %r13
	testb	%al, %al
	je	.L596
	movq	(%r12), %rax
	movq	88(%rbx), %rsi
	movq	%r12, %rdi
	call	*40(%rax)
	jmp	.L592
.L687:
	movq	39(%r13), %r15
	movq	88(%rbx), %r14
	leaq	39(%r13), %rsi
	movq	%rsi, -128(%rbp)
	cmpq	%r15, %r14
	je	.L599
	cmpq	%r15, 3840(%rbx)
	je	.L599
	movq	%r14, 39(%r13)
	testb	$1, %r14b
	je	.L599
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -136(%rbp)
	testl	$262144, %eax
	je	.L601
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-136(%rbp), %rcx
	movq	8(%rcx), %rax
.L601:
	testb	$24, %al
	je	.L599
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L599
	movq	-128(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L599:
	movq	288(%rbx), %rbx
	leaq	119(%r13), %rsi
	movq	119(%r13), %r14
	movq	%rsi, -136(%rbp)
	movq	%rbx, 119(%r13)
	testb	$1, %bl
	je	.L637
	movq	%rbx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -144(%rbp)
	testl	$262144, %eax
	je	.L604
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-144(%rbp), %rcx
	movq	8(%rcx), %rax
.L604:
	testb	$24, %al
	je	.L637
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L637
	movq	-136(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L637:
	leaq	16+_ZTVN2v88internal10Serializer16ObjectSerializerE(%rip), %rax
	movq	%r12, -88(%rbp)
	leaq	-96(%rbp), %rdi
	addq	$80, %r12
	movq	%rax, -96(%rbp)
	movq	-120(%rbp), %rax
	movq	%r12, -72(%rbp)
	movq	%rax, -80(%rbp)
	movl	$0, -64(%rbp)
	call	_ZN2v88internal10Serializer16ObjectSerializer9SerializeEv@PLT
	movq	%r14, 119(%r13)
	testb	$1, %r14b
	je	.L636
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L607
	movq	-136(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L607:
	testb	$24, %al
	je	.L636
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L636
	movq	-136(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L636:
	movq	%r15, 39(%r13)
	testb	$1, %r15b
	je	.L592
	movq	%r15, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L610
	movq	-128(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L610:
	testb	$24, %al
	je	.L592
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L592
	movq	-128(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L592
.L685:
	leaq	.LC16(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L690:
	movq	-120(%rbp), %r13
	movq	31(%r13), %rax
	leaq	31(%r13), %r15
	testb	$1, %al
	jne	.L692
.L624:
	movq	7(%rax), %r14
	movq	%r14, 31(%r13)
	testb	$1, %r14b
	je	.L623
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L627
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L627:
	testb	$24, %al
	je	.L623
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L623
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L623
.L630:
	leaq	.LC18(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L632:
	leaq	.LC20(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L688:
	movq	%r13, -104(%rbp)
	movq	31(%r13), %r14
	testb	$1, %r14b
	jne	.L693
.L613:
	leaq	16+_ZTVN2v88internal10Serializer16ObjectSerializerE(%rip), %rax
	movq	%r12, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	%r13, -80(%rbp)
	jmp	.L682
.L691:
	leaq	.LC17(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L689:
	movq	-1(%r13), %rdx
	cmpw	$91, 11(%rdx)
	jne	.L621
	movq	7(%r13), %rax
	movq	%rax, -120(%rbp)
	subq	$1, %rax
	jmp	.L621
.L631:
	movq	%r13, %rdi
	call	_ZNK2v88internal10HeapObject13CanBeRehashedEv@PLT
	testb	%al, %al
	jne	.L634
	leaq	.LC19(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L692:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rax
	je	.L623
	movq	-1(%rax), %rcx
	cmpw	$70, 11(%rcx)
	je	.L623
	cmpq	-37280(%rdx), %rax
	je	.L623
	jmp	.L624
.L693:
	movq	-1(%r14), %rax
	cmpw	$86, 11(%rax)
	jne	.L613
	movq	39(%r14), %r13
	testb	$1, %r13b
	jne	.L615
.L617:
	xorl	%r13d, %r13d
	leaq	-104(%rbp), %r15
.L616:
	movq	23(%r14), %rsi
	movq	%r15, %rdi
	movl	$4, %edx
	call	_ZN2v88internal18SharedFunctionInfo24set_script_or_debug_infoENS0_6ObjectENS0_16WriteBarrierModeE
	leaq	16+_ZTVN2v88internal10Serializer16ObjectSerializerE(%rip), %rax
	movq	%r12, -88(%rbp)
	leaq	-96(%rbp), %rdi
	movq	%rax, -96(%rbp)
	movq	-120(%rbp), %rax
	addq	$80, %r12
	movq	%r12, -72(%rbp)
	movq	%rax, -80(%rbp)
	movl	$0, -64(%rbp)
	call	_ZN2v88internal10Serializer16ObjectSerializer9SerializeEv@PLT
	testq	%r14, %r14
	je	.L592
	movl	$4, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal18SharedFunctionInfo24set_script_or_debug_infoENS0_6ObjectENS0_16WriteBarrierModeE
	testq	%r13, %r13
	je	.L592
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal18SharedFunctionInfo21SetDebugBytecodeArrayENS0_13BytecodeArrayE
	jmp	.L592
.L684:
	call	__stack_chk_fail@PLT
.L615:
	movq	-1(%r13), %rax
	cmpw	$72, 11(%rax)
	jne	.L617
	leaq	-104(%rbp), %r15
	movq	31(%r14), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal18SharedFunctionInfo21SetDebugBytecodeArrayENS0_13BytecodeArrayE
	jmp	.L616
	.cfi_endproc
.LFE22077:
	.size	_ZN2v88internal14CodeSerializer15SerializeObjectENS0_10HeapObjectE, .-_ZN2v88internal14CodeSerializer15SerializeObjectENS0_10HeapObjectE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal10ScriptDataC2EPKhi,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal10ScriptDataC2EPKhi, @function
_GLOBAL__sub_I__ZN2v88internal10ScriptDataC2EPKhi:
.LFB27756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE27756:
	.size	_GLOBAL__sub_I__ZN2v88internal10ScriptDataC2EPKhi, .-_GLOBAL__sub_I__ZN2v88internal10ScriptDataC2EPKhi
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal10ScriptDataC2EPKhi
	.weak	_ZTVN2v88internal14SerializedDataE
	.section	.data.rel.ro.local._ZTVN2v88internal14SerializedDataE,"awG",@progbits,_ZTVN2v88internal14SerializedDataE,comdat
	.align 8
	.type	_ZTVN2v88internal14SerializedDataE, @object
	.size	_ZTVN2v88internal14SerializedDataE, 32
_ZTVN2v88internal14SerializedDataE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14SerializedDataD1Ev
	.quad	_ZN2v88internal14SerializedDataD0Ev
	.weak	_ZTVN2v88internal18SerializedCodeDataE
	.section	.data.rel.ro.local._ZTVN2v88internal18SerializedCodeDataE,"awG",@progbits,_ZTVN2v88internal18SerializedCodeDataE,comdat
	.align 8
	.type	_ZTVN2v88internal18SerializedCodeDataE, @object
	.size	_ZTVN2v88internal18SerializedCodeDataE, 32
_ZTVN2v88internal18SerializedCodeDataE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal18SerializedCodeDataD1Ev
	.quad	_ZN2v88internal18SerializedCodeDataD0Ev
	.weak	_ZTVN2v88internal14CodeSerializerE
	.section	.data.rel.ro._ZTVN2v88internal14CodeSerializerE,"awG",@progbits,_ZTVN2v88internal14CodeSerializerE,comdat
	.align 8
	.type	_ZTVN2v88internal14CodeSerializerE, @object
	.size	_ZTVN2v88internal14CodeSerializerE, 80
_ZTVN2v88internal14CodeSerializerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14CodeSerializerD1Ev
	.quad	_ZN2v88internal14CodeSerializerD0Ev
	.quad	_ZN2v88internal10Serializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.quad	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.quad	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.quad	_ZN2v88internal14CodeSerializer15SerializeObjectENS0_10HeapObjectE
	.quad	_ZN2v88internal10Serializer14MustBeDeferredENS0_10HeapObjectE
	.quad	_ZN2v88internal14CodeSerializer11ElideObjectENS0_6ObjectE
	.section	.bss._ZZN2v88internal14CodeSerializer9SerializeENS0_6HandleINS0_18SharedFunctionInfoEEEE27trace_event_unique_atomic46,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal14CodeSerializer9SerializeENS0_6HandleINS0_18SharedFunctionInfoEEEE27trace_event_unique_atomic46, @object
	.size	_ZZN2v88internal14CodeSerializer9SerializeENS0_6HandleINS0_18SharedFunctionInfoEEEE27trace_event_unique_atomic46, 8
_ZZN2v88internal14CodeSerializer9SerializeENS0_6HandleINS0_18SharedFunctionInfoEEEE27trace_event_unique_atomic46:
	.zero	8
	.section	.bss._ZZN2v88internal14CodeSerializer9SerializeENS0_6HandleINS0_18SharedFunctionInfoEEEE27trace_event_unique_atomic42,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal14CodeSerializer9SerializeENS0_6HandleINS0_18SharedFunctionInfoEEEE27trace_event_unique_atomic42, @object
	.size	_ZZN2v88internal14CodeSerializer9SerializeENS0_6HandleINS0_18SharedFunctionInfoEEEE27trace_event_unique_atomic42, 8
_ZZN2v88internal14CodeSerializer9SerializeENS0_6HandleINS0_18SharedFunctionInfoEEEE27trace_event_unique_atomic42:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
