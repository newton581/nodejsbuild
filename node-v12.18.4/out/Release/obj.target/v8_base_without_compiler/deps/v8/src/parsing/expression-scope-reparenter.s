	.file	"expression-scope-reparenter.cc"
	.text
	.section	.rodata._ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE, @function
_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE:
.LFB21315:
	.cfi_startproc
	cmpb	$0, 8(%rdi)
	je	.L148
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	.L7(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
.L3:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jb	.L149
	movl	4(%r12), %edx
	movl	%edx, %eax
	andl	$63, %eax
	cmpb	$57, %al
	ja	.L1
	movzbl	%al, %eax
	movslq	0(%r13,%rax,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE,"a",@progbits
	.align 4
	.align 4
.L7:
	.long	.L1-.L7
	.long	.L69-.L7
	.long	.L52-.L7
	.long	.L51-.L7
	.long	.L50-.L7
	.long	.L48-.L7
	.long	.L48-.L7
	.long	.L47-.L7
	.long	.L46-.L7
	.long	.L42-.L7
	.long	.L1-.L7
	.long	.L118-.L7
	.long	.L43-.L7
	.long	.L1-.L7
	.long	.L1-.L7
	.long	.L42-.L7
	.long	.L41-.L7
	.long	.L40-.L7
	.long	.L39-.L7
	.long	.L1-.L7
	.long	.L38-.L7
	.long	.L1-.L7
	.long	.L37-.L7
	.long	.L36-.L7
	.long	.L19-.L7
	.long	.L8-.L7
	.long	.L19-.L7
	.long	.L32-.L7
	.long	.L31-.L7
	.long	.L30-.L7
	.long	.L29-.L7
	.long	.L28-.L7
	.long	.L19-.L7
	.long	.L19-.L7
	.long	.L16-.L7
	.long	.L8-.L7
	.long	.L23-.L7
	.long	.L1-.L7
	.long	.L22-.L7
	.long	.L1-.L7
	.long	.L8-.L7
	.long	.L1-.L7
	.long	.L1-.L7
	.long	.L8-.L7
	.long	.L19-.L7
	.long	.L15-.L7
	.long	.L17-.L7
	.long	.L16-.L7
	.long	.L15-.L7
	.long	.L8-.L7
	.long	.L13-.L7
	.long	.L1-.L7
	.long	.L8-.L7
	.long	.L8-.L7
	.long	.L10-.L7
	.long	.L8-.L7
	.long	.L8-.L7
	.long	.L6-.L7
	.section	.text._ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L15:
	movl	24(%rbx), %eax
	leal	1(%rax), %edx
	movl	%edx, 24(%rbx)
	movq	8(%r12), %r13
	testb	$1, 5(%r13)
	je	.L150
.L89:
	cmpb	$0, 8(%rbx)
	movl	%eax, 24(%rbx)
	jne	.L1
	movl	%edx, 24(%rbx)
	movq	16(%r12), %r12
	testb	$1, 5(%r12)
	je	.L151
.L91:
	movl	%eax, 24(%rbx)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	addl	$1, 24(%rbx)
	movq	8(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L149:
	movb	$1, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	addl	$1, 24(%rbx)
	movq	8(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movl	24(%rbx), %eax
	cmpb	$0, 8(%rbx)
	leal	-1(%rax), %edx
	movl	%edx, 24(%rbx)
	jne	.L1
	movl	%eax, 24(%rbx)
	movq	16(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L48:
	movq	32(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%rbx)
	jne	.L1
	movq	40(%r12), %rsi
.L138:
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%rbx)
	jne	.L1
.L69:
	movq	24(%r12), %r12
.L54:
	cmpb	$0, 8(%rbx)
	jne	.L1
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L42:
	movq	8(%r12), %r12
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L16:
	addl	$1, 24(%rbx)
	movq	8(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movl	24(%rbx), %eax
	cmpb	$0, 8(%rbx)
	leal	-1(%rax), %edx
	movl	%edx, 24(%rbx)
	jne	.L1
	movl	%eax, 24(%rbx)
	movq	16(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movl	24(%rbx), %eax
	cmpb	$0, 8(%rbx)
	leal	-1(%rax), %edx
	movl	%edx, 24(%rbx)
	jne	.L1
	movl	%eax, 24(%rbx)
	movq	24(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%rbx)
	jmp	.L1
.L39:
	movq	8(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%rbx)
	jne	.L1
.L118:
	movq	16(%r12), %r12
	jmp	.L54
.L52:
	movq	24(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%rbx)
	jne	.L1
	movq	32(%r12), %r12
	jmp	.L54
.L51:
	movq	32(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%rbx)
	jne	.L1
	jmp	.L69
.L40:
	movq	8(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L69
.L136:
	movq	32(%rbx), %rsi
.L134:
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal5Scope17ReplaceOuterScopeEPS1_@PLT
.L38:
	.cfi_restore_state
	movq	8(%r12), %r13
	movl	12(%r13), %r10d
	testl	%r10d, %r10d
	jle	.L1
	xorl	%r12d, %r12d
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L113:
	addq	$1, %r12
	cmpl	%r12d, 12(%r13)
	jle	.L1
.L73:
	movq	0(%r13), %rax
	movq	(%rax,%r12,8), %r14
	movq	(%r14), %rsi
	andq	$-4, %rsi
	movzbl	4(%rsi), %eax
	andl	$63, %eax
	cmpb	$41, %al
	je	.L72
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%rbx)
	jne	.L1
.L72:
	movq	8(%r14), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%rbx)
	je	.L113
	jmp	.L1
.L37:
	movl	36(%r12), %r9d
	testl	%r9d, %r9d
	jle	.L1
	movl	24(%rbx), %eax
	xorl	%r13d, %r13d
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L152:
	movl	%eax, 24(%rbx)
	movq	8(%r14), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movl	24(%rbx), %eax
	subl	$1, %eax
	cmpb	$0, 8(%rbx)
	movl	%eax, 24(%rbx)
	jne	.L1
	addq	$1, %r13
	cmpl	%r13d, 36(%r12)
	jle	.L1
.L74:
	movq	24(%r12), %rdx
	addl	$1, %eax
	movq	%rbx, %rdi
	movq	(%rdx,%r13,8), %r14
	movl	%eax, 24(%rbx)
	movq	(%r14), %rsi
	andq	$-4, %rsi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movl	24(%rbx), %eax
	cmpb	$0, 8(%rbx)
	leal	-1(%rax), %edx
	movl	%edx, 24(%rbx)
	je	.L152
	jmp	.L1
.L36:
	movl	36(%r12), %r8d
	testl	%r8d, %r8d
	jle	.L1
	movl	24(%rbx), %eax
	xorl	%r13d, %r13d
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L153:
	addq	$1, %r13
	cmpl	%r13d, 36(%r12)
	jle	.L1
.L75:
	movq	24(%r12), %rdx
	addl	$1, %eax
	movq	%rbx, %rdi
	movq	(%rdx,%r13,8), %rsi
	movl	%eax, 24(%rbx)
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movl	24(%rbx), %eax
	subl	$1, %eax
	cmpb	$0, 8(%rbx)
	movl	%eax, 24(%rbx)
	je	.L153
	jmp	.L1
.L10:
	andb	$1, %dh
	jne	.L1
.L140:
	movq	32(%rbx), %rax
	movq	%r12, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal5Scope16RemoveUnresolvedEPNS0_13VariableProxyE@PLT
	testb	%al, %al
	je	.L1
	movq	32(%rbx), %rdi
	addq	$8, %rsp
	movq	%r12, %rsi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal5Scope13AddUnresolvedEPNS0_13VariableProxyE@PLT
.L13:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	(%rax), %r12
	movslq	12(%rax), %rax
	leaq	(%r12,%rax,8), %r13
	cmpq	%r13, %r12
	je	.L1
	movl	24(%rbx), %eax
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L154:
	addq	$8, %r12
	cmpq	%r12, %r13
	je	.L1
.L93:
	addl	$1, %eax
	movq	(%r12), %rsi
	movq	%rbx, %rdi
	movl	%eax, 24(%rbx)
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movl	24(%rbx), %eax
	subl	$1, %eax
	cmpb	$0, 8(%rbx)
	movl	%eax, 24(%rbx)
	je	.L154
	jmp	.L1
.L28:
	movq	32(%rbx), %rsi
	movq	16(%r12), %rdi
	jmp	.L134
.L46:
	movq	16(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%rbx)
	jne	.L1
	movl	36(%r12), %edx
	testl	%edx, %edx
	jle	.L1
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L68:
	movq	24(%r12), %rax
	movq	(%rax,%r15,8), %r14
	movq	(%r14), %rsi
	testq	%rsi, %rsi
	je	.L63
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%rbx)
	jne	.L1
	movl	20(%r14), %eax
	testl	%eax, %eax
	jle	.L119
.L94:
	xorl	%r13d, %r13d
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L155:
	addq	$1, %r13
	cmpl	%r13d, 20(%r14)
	jle	.L119
.L66:
	movq	8(%r14), %rax
	movq	%rbx, %rdi
	movq	(%rax,%r13,8), %rsi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%rbx)
	je	.L155
	jmp	.L1
.L47:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L136
	movl	20(%r12), %r13d
	testl	%r13d, %r13d
	jle	.L1
	xorl	%r13d, %r13d
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L156:
	addq	$1, %r13
	cmpl	%r13d, 20(%r12)
	jle	.L1
.L62:
	movq	8(%r12), %rax
	movq	%rbx, %rdi
	movq	(%rax,%r13,8), %rsi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%rbx)
	je	.L156
	jmp	.L1
.L41:
	movq	16(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movq	32(%rbx), %rsi
	movq	8(%r12), %rdi
	jmp	.L134
.L23:
	movq	8(%r12), %r14
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.L80
	movq	32(%rbx), %rsi
	call	_ZN2v88internal5Scope17ReplaceOuterScopeEPS1_@PLT
.L81:
	cmpb	$0, 8(%rbx)
	jne	.L1
.L84:
	movq	16(%r12), %r12
	testb	$1, 5(%r12)
	jne	.L1
	jmp	.L140
.L50:
	movq	32(%r12), %rsi
	testq	%rsi, %rsi
	je	.L58
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%rbx)
	jne	.L1
.L58:
	movq	40(%r12), %rsi
	testq	%rsi, %rsi
	je	.L57
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%rbx)
	jne	.L1
.L57:
	movq	48(%r12), %rsi
	testq	%rsi, %rsi
	jne	.L138
	jmp	.L69
.L30:
	addl	$1, 24(%rbx)
	movq	8(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movl	24(%rbx), %eax
	subl	$1, %eax
	cmpb	$0, 8(%rbx)
	movl	%eax, 24(%rbx)
	jne	.L1
	movl	28(%r12), %esi
	testl	%esi, %esi
	jle	.L1
	xorl	%r13d, %r13d
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L157:
	addq	$1, %r13
	cmpl	%r13d, 28(%r12)
	jle	.L1
.L78:
	movq	16(%r12), %rdx
	addl	$1, %eax
	movq	%rbx, %rdi
	movq	(%rdx,%r13,8), %rsi
	movl	%eax, 24(%rbx)
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movl	24(%rbx), %eax
	subl	$1, %eax
	cmpb	$0, 8(%rbx)
	movl	%eax, 24(%rbx)
	je	.L157
	jmp	.L1
.L29:
	movl	36(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L1
	movl	24(%rbx), %eax
	xorl	%r13d, %r13d
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L158:
	addq	$1, %r13
	cmpl	%r13d, 36(%r12)
	jle	.L1
.L79:
	movq	24(%r12), %rdx
	addl	$1, %eax
	movq	%rbx, %rdi
	movq	(%rdx,%r13,8), %rsi
	movl	%eax, 24(%rbx)
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movl	24(%rbx), %eax
	subl	$1, %eax
	cmpb	$0, 8(%rbx)
	movl	%eax, 24(%rbx)
	je	.L158
	jmp	.L1
.L17:
	addl	$1, 24(%rbx)
	movq	16(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%rbx)
	jmp	.L1
.L22:
	movq	32(%rbx), %rsi
	movq	40(%r12), %rdi
	jmp	.L134
.L32:
	addl	$1, 24(%rbx)
	movq	8(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movl	24(%rbx), %eax
	subl	$1, %eax
	cmpb	$0, 8(%rbx)
	movl	%eax, 24(%rbx)
	jne	.L1
	movq	24(%r12), %rcx
	cmpq	%rcx, 32(%r12)
	je	.L1
	xorl	%r13d, %r13d
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L159:
	movq	32(%r12), %rdx
	subq	24(%r12), %rdx
	addq	$1, %r13
	sarq	$4, %rdx
	cmpq	%r13, %rdx
	jbe	.L1
.L76:
	addl	$1, %eax
	movq	%rbx, %rdi
	movl	%eax, 24(%rbx)
	movq	%r13, %rax
	salq	$4, %rax
	addq	24(%r12), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movl	24(%rbx), %eax
	subl	$1, %eax
	cmpb	$0, 8(%rbx)
	movl	%eax, 24(%rbx)
	je	.L159
	jmp	.L1
.L31:
	addl	$1, 24(%rbx)
	movq	8(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movl	24(%rbx), %eax
	subl	$1, %eax
	cmpb	$0, 8(%rbx)
	movl	%eax, 24(%rbx)
	jne	.L1
	movl	28(%r12), %edi
	testl	%edi, %edi
	jle	.L1
	xorl	%r13d, %r13d
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L160:
	addq	$1, %r13
	cmpl	%r13d, 28(%r12)
	jle	.L1
.L77:
	movq	16(%r12), %rdx
	addl	$1, %eax
	movq	%rbx, %rdi
	movq	(%rdx,%r13,8), %rsi
	movl	%eax, 24(%rbx)
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movl	24(%rbx), %eax
	subl	$1, %eax
	cmpb	$0, 8(%rbx)
	movl	%eax, 24(%rbx)
	je	.L160
	jmp	.L1
.L43:
	movq	8(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%rbx)
	jne	.L1
	movq	16(%r12), %rsi
	jmp	.L138
.L6:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L119:
	movl	36(%r12), %edx
.L67:
	addq	$1, %r15
	cmpl	%r15d, %edx
	jg	.L68
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L63:
	movl	20(%r14), %r11d
	testl	%r11d, %r11d
	jg	.L94
	cmpb	$0, 8(%rbx)
	je	.L67
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L150:
	movq	32(%rbx), %rax
	movq	%r13, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal5Scope16RemoveUnresolvedEPNS0_13VariableProxyE@PLT
	testb	%al, %al
	jne	.L90
.L124:
	movl	24(%rbx), %edx
	leal	-1(%rdx), %eax
	jmp	.L89
.L151:
	movq	32(%rbx), %rax
	movq	%r12, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal5Scope16RemoveUnresolvedEPNS0_13VariableProxyE@PLT
	testb	%al, %al
	jne	.L92
.L125:
	movl	24(%rbx), %eax
	subl	$1, %eax
	jmp	.L91
.L80:
	movl	20(%r14), %edx
	testl	%edx, %edx
	jle	.L81
	xorl	%r13d, %r13d
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L161:
	addq	$1, %r13
	cmpl	%r13d, 20(%r14)
	jle	.L84
.L82:
	movq	8(%r14), %rax
	movq	%rbx, %rdi
	movq	(%rax,%r13,8), %rsi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%rbx)
	je	.L161
	jmp	.L1
.L90:
	movq	32(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal5Scope13AddUnresolvedEPNS0_13VariableProxyE@PLT
	jmp	.L124
.L92:
	movq	32(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal5Scope13AddUnresolvedEPNS0_13VariableProxyE@PLT
	jmp	.L125
	.cfi_endproc
.LFE21315:
	.size	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE, .-_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	.section	.text._ZN2v88internal23ReparentExpressionScopeEmPNS0_10ExpressionEPNS0_5ScopeE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23ReparentExpressionScopeEmPNS0_10ExpressionEPNS0_5ScopeE
	.type	_ZN2v88internal23ReparentExpressionScopeEmPNS0_10ExpressionEPNS0_5ScopeE, @function
_ZN2v88internal23ReparentExpressionScopeEmPNS0_10ExpressionEPNS0_5ScopeE:
.LFB19279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -80(%rbp)
	movl	$0, -72(%rbp)
	movq	%rdi, -96(%rbp)
	movb	$0, -88(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	-96(%rbp), %rax
	jb	.L162
	movl	4(%r12), %ecx
	movl	%ecx, %eax
	andl	$63, %eax
	cmpb	$57, %al
	ja	.L162
	leaq	.L166(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal23ReparentExpressionScopeEmPNS0_10ExpressionEPNS0_5ScopeE,"a",@progbits
	.align 4
	.align 4
.L166:
	.long	.L162-.L166
	.long	.L203-.L166
	.long	.L202-.L166
	.long	.L201-.L166
	.long	.L200-.L166
	.long	.L199-.L166
	.long	.L198-.L166
	.long	.L197-.L166
	.long	.L196-.L166
	.long	.L193-.L166
	.long	.L162-.L166
	.long	.L195-.L166
	.long	.L194-.L166
	.long	.L162-.L166
	.long	.L162-.L166
	.long	.L193-.L166
	.long	.L192-.L166
	.long	.L191-.L166
	.long	.L190-.L166
	.long	.L162-.L166
	.long	.L189-.L166
	.long	.L162-.L166
	.long	.L188-.L166
	.long	.L187-.L166
	.long	.L186-.L166
	.long	.L167-.L166
	.long	.L185-.L166
	.long	.L184-.L166
	.long	.L183-.L166
	.long	.L182-.L166
	.long	.L181-.L166
	.long	.L180-.L166
	.long	.L179-.L166
	.long	.L178-.L166
	.long	.L177-.L166
	.long	.L167-.L166
	.long	.L176-.L166
	.long	.L162-.L166
	.long	.L175-.L166
	.long	.L162-.L166
	.long	.L167-.L166
	.long	.L162-.L166
	.long	.L162-.L166
	.long	.L167-.L166
	.long	.L174-.L166
	.long	.L173-.L166
	.long	.L172-.L166
	.long	.L171-.L166
	.long	.L170-.L166
	.long	.L167-.L166
	.long	.L169-.L166
	.long	.L162-.L166
	.long	.L167-.L166
	.long	.L167-.L166
	.long	.L168-.L166
	.long	.L167-.L166
	.long	.L167-.L166
	.long	.L165-.L166
	.section	.text._ZN2v88internal23ReparentExpressionScopeEmPNS0_10ExpressionEPNS0_5ScopeE
.L200:
	movq	32(%r12), %rsi
	leaq	-96(%rbp), %r13
	testq	%rsi, %rsi
	je	.L207
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	jne	.L162
.L207:
	movq	40(%r12), %rsi
	testq	%rsi, %rsi
	je	.L206
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	jne	.L162
.L206:
	movq	48(%r12), %rsi
	testq	%rsi, %rsi
	je	.L209
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	jne	.L162
.L209:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L162:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L277
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	movq	8(%r12), %rsi
	leaq	-96(%rbp), %rdi
	addl	$1, -72(%rbp)
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L193:
	movq	8(%r12), %rsi
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	jmp	.L162
.L185:
	movq	8(%r12), %rsi
	leaq	-96(%rbp), %r13
	addl	$1, -72(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	jne	.L162
	movq	16(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	jmp	.L162
.L184:
	movq	8(%r12), %rsi
	leaq	-96(%rbp), %r13
	addl	$1, -72(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movl	-72(%rbp), %eax
	subl	$1, %eax
	cmpb	$0, -88(%rbp)
	movl	%eax, -72(%rbp)
	jne	.L162
	movq	24(%r12), %rcx
	cmpq	32(%r12), %rcx
	je	.L162
	xorl	%ebx, %ebx
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L278:
	movq	24(%r12), %rcx
	movq	32(%r12), %rdx
	addq	$1, %rbx
	subq	%rcx, %rdx
	sarq	$4, %rdx
	cmpq	%rbx, %rdx
	jbe	.L162
.L226:
	addl	$1, %eax
	movq	%r13, %rdi
	movl	%eax, -72(%rbp)
	movq	%rbx, %rax
	salq	$4, %rax
	movq	(%rcx,%rax), %rsi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movl	-72(%rbp), %eax
	subl	$1, %eax
	cmpb	$0, -88(%rbp)
	movl	%eax, -72(%rbp)
	je	.L278
	jmp	.L162
.L183:
	movq	8(%r12), %rsi
	leaq	-96(%rbp), %r13
	addl	$1, -72(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movl	-72(%rbp), %eax
	subl	$1, %eax
	cmpb	$0, -88(%rbp)
	movl	%eax, -72(%rbp)
	jne	.L162
	movl	28(%r12), %edi
	testl	%edi, %edi
	jle	.L162
	xorl	%ebx, %ebx
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L279:
	addq	$1, %rbx
	cmpl	%ebx, 28(%r12)
	jle	.L162
.L227:
	movq	16(%r12), %rdx
	addl	$1, %eax
	movq	%r13, %rdi
	movq	(%rdx,%rbx,8), %rsi
	movl	%eax, -72(%rbp)
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movl	-72(%rbp), %eax
	subl	$1, %eax
	cmpb	$0, -88(%rbp)
	movl	%eax, -72(%rbp)
	je	.L279
	jmp	.L162
.L182:
	movq	8(%r12), %rsi
	leaq	-96(%rbp), %r13
	addl	$1, -72(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movl	-72(%rbp), %eax
	subl	$1, %eax
	cmpb	$0, -88(%rbp)
	movl	%eax, -72(%rbp)
	jne	.L162
	movl	28(%r12), %esi
	testl	%esi, %esi
	jle	.L162
	xorl	%ebx, %ebx
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L280:
	addq	$1, %rbx
	cmpl	%ebx, 28(%r12)
	jle	.L162
.L228:
	movq	16(%r12), %rdx
	addl	$1, %eax
	movq	%r13, %rdi
	movq	(%rdx,%rbx,8), %rsi
	movl	%eax, -72(%rbp)
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movl	-72(%rbp), %eax
	subl	$1, %eax
	cmpb	$0, -88(%rbp)
	movl	%eax, -72(%rbp)
	je	.L280
	jmp	.L162
.L181:
	movl	36(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L162
	movl	-72(%rbp), %eax
	xorl	%ebx, %ebx
	leaq	-96(%rbp), %r13
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L281:
	addq	$1, %rbx
	cmpl	%ebx, 36(%r12)
	jle	.L162
.L229:
	movq	24(%r12), %rdx
	addl	$1, %eax
	movq	%r13, %rdi
	movq	(%rdx,%rbx,8), %rsi
	movl	%eax, -72(%rbp)
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movl	-72(%rbp), %eax
	subl	$1, %eax
	cmpb	$0, -88(%rbp)
	movl	%eax, -72(%rbp)
	je	.L281
	jmp	.L162
.L180:
	movq	16(%r12), %rdi
	movq	-64(%rbp), %rsi
	call	_ZN2v88internal5Scope17ReplaceOuterScopeEPS1_@PLT
	jmp	.L162
.L179:
	movq	8(%r12), %rsi
	leaq	-96(%rbp), %r13
	addl	$1, -72(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	jne	.L162
	movq	16(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	jmp	.L162
.L178:
	movq	8(%r12), %rsi
	leaq	-96(%rbp), %r13
	addl	$1, -72(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	jne	.L162
	movq	16(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	jmp	.L162
.L177:
	movq	8(%r12), %rsi
	leaq	-96(%rbp), %r13
	addl	$1, -72(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	jne	.L162
	movq	16(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	jne	.L162
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	jmp	.L162
.L176:
	movq	8(%r12), %r14
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.L230
	movq	-64(%rbp), %rsi
	call	_ZN2v88internal5Scope17ReplaceOuterScopeEPS1_@PLT
.L231:
	cmpb	$0, -88(%rbp)
	jne	.L162
.L234:
	movq	16(%r12), %r12
	testb	$1, 5(%r12)
	jne	.L162
	movq	-64(%rbp), %rax
	movq	%r12, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal5Scope16RemoveUnresolvedEPNS0_13VariableProxyE@PLT
	testb	%al, %al
	je	.L162
	movq	-64(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal5Scope13AddUnresolvedEPNS0_13VariableProxyE@PLT
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L175:
	movq	40(%r12), %rdi
	movq	-64(%rbp), %rsi
	call	_ZN2v88internal5Scope17ReplaceOuterScopeEPS1_@PLT
	jmp	.L162
.L174:
	movq	8(%r12), %rsi
	leaq	-96(%rbp), %r13
	addl	$1, -72(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	jne	.L162
	movq	16(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	jmp	.L162
.L173:
	movq	8(%r12), %r13
	addl	$1, -72(%rbp)
	testb	$1, 5(%r13)
	je	.L282
.L236:
	cmpb	$0, -88(%rbp)
	jne	.L162
	movq	16(%r12), %r12
	testb	$1, 5(%r12)
	jne	.L162
	movq	-64(%rbp), %rax
	movq	%r12, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal5Scope16RemoveUnresolvedEPNS0_13VariableProxyE@PLT
	testb	%al, %al
	je	.L162
	movq	-64(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal5Scope13AddUnresolvedEPNS0_13VariableProxyE@PLT
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L172:
	movq	16(%r12), %rsi
	leaq	-96(%rbp), %rdi
	addl	$1, -72(%rbp)
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	jmp	.L162
.L170:
	movq	8(%r12), %r13
	addl	$1, -72(%rbp)
	testb	$1, 5(%r13)
	je	.L283
.L239:
	cmpb	$0, -88(%rbp)
	jne	.L162
	movq	16(%r12), %r12
	testb	$1, 5(%r12)
	jne	.L162
	movq	-64(%rbp), %rax
	movq	%r12, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal5Scope16RemoveUnresolvedEPNS0_13VariableProxyE@PLT
	testb	%al, %al
	je	.L162
	movq	-64(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal5Scope13AddUnresolvedEPNS0_13VariableProxyE@PLT
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L169:
	movq	16(%r12), %rax
	movq	(%rax), %rbx
	movslq	12(%rax), %rax
	leaq	(%rbx,%rax,8), %r12
	cmpq	%r12, %rbx
	je	.L162
	movl	-72(%rbp), %eax
	leaq	-96(%rbp), %r13
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L284:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	je	.L162
.L241:
	movq	(%rbx), %rsi
	addl	$1, %eax
	movq	%r13, %rdi
	movl	%eax, -72(%rbp)
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movl	-72(%rbp), %eax
	subl	$1, %eax
	cmpb	$0, -88(%rbp)
	movl	%eax, -72(%rbp)
	je	.L284
	jmp	.L162
.L171:
	movq	8(%r12), %rsi
	leaq	-96(%rbp), %r13
	addl	$1, -72(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	jne	.L162
	movq	16(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	jne	.L162
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	jmp	.L162
.L168:
	andb	$1, %ch
	jne	.L162
	movq	-64(%rbp), %rax
	movq	%r12, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal5Scope16RemoveUnresolvedEPNS0_13VariableProxyE@PLT
	testb	%al, %al
	je	.L162
	movq	-64(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal5Scope13AddUnresolvedEPNS0_13VariableProxyE@PLT
	jmp	.L162
.L203:
	movq	24(%r12), %rsi
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	jmp	.L162
.L202:
	movq	24(%r12), %rsi
	leaq	-96(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	jne	.L162
	movq	32(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	jmp	.L162
.L201:
	movq	32(%r12), %rsi
	leaq	-96(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	jne	.L162
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	jmp	.L162
.L199:
	movq	32(%r12), %rsi
	leaq	-96(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	jne	.L162
	movq	40(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	jne	.L162
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	jmp	.L162
.L198:
	movq	32(%r12), %rsi
	leaq	-96(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	jne	.L162
	movq	40(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	jne	.L162
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	jmp	.L162
.L197:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L210
	movq	-64(%rbp), %rsi
	call	_ZN2v88internal5Scope17ReplaceOuterScopeEPS1_@PLT
	jmp	.L162
.L196:
	movq	16(%r12), %rsi
	leaq	-96(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	jne	.L162
	movl	36(%r12), %edx
	testl	%edx, %edx
	jle	.L162
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L218:
	movq	24(%r12), %rax
	movq	(%rax,%r15,8), %r14
	movq	(%r14), %rsi
	testq	%rsi, %rsi
	je	.L285
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	jne	.L162
	movl	20(%r14), %eax
	testl	%eax, %eax
	jle	.L276
.L213:
	xorl	%ebx, %ebx
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L286:
	addq	$1, %rbx
	cmpl	%ebx, 20(%r14)
	jle	.L276
.L216:
	movq	8(%r14), %rax
	movq	%r13, %rdi
	movq	(%rax,%rbx,8), %rsi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	je	.L286
	jmp	.L162
.L192:
	movq	16(%r12), %rsi
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movq	8(%r12), %rdi
	movq	-64(%rbp), %rsi
	call	_ZN2v88internal5Scope17ReplaceOuterScopeEPS1_@PLT
	jmp	.L162
.L187:
	movl	36(%r12), %r8d
	testl	%r8d, %r8d
	jle	.L162
	movl	-72(%rbp), %eax
	xorl	%ebx, %ebx
	leaq	-96(%rbp), %r13
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L287:
	addq	$1, %rbx
	cmpl	%ebx, 36(%r12)
	jle	.L162
.L225:
	movq	24(%r12), %rdx
	addl	$1, %eax
	movq	%r13, %rdi
	movq	(%rdx,%rbx,8), %rsi
	movl	%eax, -72(%rbp)
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movl	-72(%rbp), %eax
	subl	$1, %eax
	cmpb	$0, -88(%rbp)
	movl	%eax, -72(%rbp)
	je	.L287
	jmp	.L162
.L186:
	movq	8(%r12), %rsi
	leaq	-96(%rbp), %r13
	addl	$1, -72(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	jne	.L162
	movq	16(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	jmp	.L162
.L191:
	movq	8(%r12), %rsi
	leaq	-96(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L219
	movq	-64(%rbp), %rsi
	call	_ZN2v88internal5Scope17ReplaceOuterScopeEPS1_@PLT
	jmp	.L162
.L190:
	movq	8(%r12), %rsi
	leaq	-96(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	jne	.L162
	movq	16(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	jmp	.L162
.L188:
	movl	36(%r12), %r9d
	testl	%r9d, %r9d
	jle	.L162
	movl	-72(%rbp), %eax
	xorl	%ebx, %ebx
	leaq	-96(%rbp), %r13
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L288:
	movq	8(%r14), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	movl	-72(%rbp), %eax
	subl	$1, %eax
	cmpb	$0, -88(%rbp)
	movl	%eax, -72(%rbp)
	jne	.L162
	addq	$1, %rbx
	cmpl	%ebx, 36(%r12)
	jle	.L162
.L224:
	movq	24(%r12), %rdx
	addl	$1, %eax
	movq	%r13, %rdi
	movq	(%rdx,%rbx,8), %r14
	movl	%eax, -72(%rbp)
	movq	(%r14), %rsi
	andq	$-4, %rsi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	je	.L288
	jmp	.L162
.L195:
	movq	16(%r12), %rsi
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	jmp	.L162
.L189:
	movq	8(%r12), %r12
	movl	12(%r12), %r10d
	testl	%r10d, %r10d
	jle	.L162
	xorl	%ebx, %ebx
	leaq	-96(%rbp), %r13
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L274:
	addq	$1, %rbx
	cmpl	%ebx, 12(%r12)
	jle	.L162
.L223:
	movq	(%r12), %rax
	movq	(%rax,%rbx,8), %r14
	movq	(%r14), %rsi
	andq	$-4, %rsi
	movzbl	4(%rsi), %eax
	andl	$63, %eax
	cmpb	$41, %al
	je	.L222
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	jne	.L162
.L222:
	movq	8(%r14), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	je	.L274
	jmp	.L162
.L194:
	movq	8(%r12), %rsi
	leaq	-96(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	jne	.L162
	movq	16(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	jne	.L162
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	jmp	.L162
.L165:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L276:
	movl	36(%r12), %edx
.L217:
	addq	$1, %r15
	cmpl	%r15d, %edx
	jg	.L218
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L285:
	movl	20(%r14), %r11d
	testl	%r11d, %r11d
	jg	.L213
	cmpb	$0, -88(%rbp)
	je	.L217
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L282:
	movq	-64(%rbp), %rax
	movq	%r13, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal5Scope16RemoveUnresolvedEPNS0_13VariableProxyE@PLT
	testb	%al, %al
	je	.L236
	movq	-64(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal5Scope13AddUnresolvedEPNS0_13VariableProxyE@PLT
	jmp	.L236
.L283:
	movq	-64(%rbp), %rax
	movq	%r13, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal5Scope16RemoveUnresolvedEPNS0_13VariableProxyE@PLT
	testb	%al, %al
	je	.L239
	movq	-64(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal5Scope13AddUnresolvedEPNS0_13VariableProxyE@PLT
	jmp	.L239
.L219:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	jmp	.L162
.L230:
	movl	20(%r14), %edx
	testl	%edx, %edx
	jle	.L231
	xorl	%ebx, %ebx
	leaq	-96(%rbp), %r13
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L289:
	addq	$1, %rbx
	cmpl	%ebx, 20(%r14)
	jle	.L234
.L232:
	movq	8(%r14), %rax
	movq	%r13, %rdi
	movq	(%rax,%rbx,8), %rsi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	je	.L289
	jmp	.L162
.L210:
	movl	20(%r12), %ebx
	testl	%ebx, %ebx
	jle	.L162
	xorl	%ebx, %ebx
	leaq	-96(%rbp), %r13
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L290:
	addq	$1, %rbx
	cmpl	%ebx, 20(%r12)
	jle	.L162
.L211:
	movq	8(%r12), %rax
	movq	%r13, %rdi
	movq	(%rax,%rbx,8), %rsi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_110ReparenterEE5VisitEPNS0_7AstNodeE
	cmpb	$0, -88(%rbp)
	je	.L290
	jmp	.L162
.L277:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19279:
	.size	_ZN2v88internal23ReparentExpressionScopeEmPNS0_10ExpressionEPNS0_5ScopeE, .-_ZN2v88internal23ReparentExpressionScopeEmPNS0_10ExpressionEPNS0_5ScopeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal23ReparentExpressionScopeEmPNS0_10ExpressionEPNS0_5ScopeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal23ReparentExpressionScopeEmPNS0_10ExpressionEPNS0_5ScopeE, @function
_GLOBAL__sub_I__ZN2v88internal23ReparentExpressionScopeEmPNS0_10ExpressionEPNS0_5ScopeE:
.LFB23584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23584:
	.size	_GLOBAL__sub_I__ZN2v88internal23ReparentExpressionScopeEmPNS0_10ExpressionEPNS0_5ScopeE, .-_GLOBAL__sub_I__ZN2v88internal23ReparentExpressionScopeEmPNS0_10ExpressionEPNS0_5ScopeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal23ReparentExpressionScopeEmPNS0_10ExpressionEPNS0_5ScopeE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
