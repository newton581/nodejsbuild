	.file	"regexp-parser.cc"
	.text
	.section	.text._ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPPN2v88internal13RegExpCaptureESt6vectorIS5_NS3_13ZoneAllocatorIS5_EEEEElS5_NS0_5__ops15_Iter_comp_iterINS3_12_GLOBAL__N_122RegExpCaptureIndexLessEEEEvT_T0_SI_T1_T2_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPPN2v88internal13RegExpCaptureESt6vectorIS5_NS3_13ZoneAllocatorIS5_EEEEElS5_NS0_5__ops15_Iter_comp_iterINS3_12_GLOBAL__N_122RegExpCaptureIndexLessEEEEvT_T0_SI_T1_T2_.isra.0, @function
_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPPN2v88internal13RegExpCaptureESt6vectorIS5_NS3_13ZoneAllocatorIS5_EEEEElS5_NS0_5__ops15_Iter_comp_iterINS3_12_GLOBAL__N_122RegExpCaptureIndexLessEEEEvT_T0_SI_T1_T2_.isra.0:
.LFB22840:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdx), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rax, %r12
	andl	$1, %r13d
	shrq	$63, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	addq	%rax, %r12
	sarq	%r12
	cmpq	%r12, %rsi
	jge	.L2
	movq	%rsi, %r10
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%r14, (%rdi,%r10,8)
	cmpq	%r8, %r12
	jle	.L4
.L5:
	movq	%r8, %r10
.L6:
	leaq	1(%r10), %rax
	leaq	(%rax,%rax), %r8
	salq	$4, %rax
	leaq	-1(%r8), %r9
	addq	%rdi, %rax
	leaq	(%rdi,%r9,8), %rbx
	movq	(%rax), %r14
	movq	(%rbx), %r11
	movl	16(%r14), %r15d
	cmpl	%r15d, 16(%r11)
	jle	.L17
	movq	%r11, (%rdi,%r10,8)
	cmpq	%r9, %r12
	jle	.L11
	movq	%r9, %r8
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L11:
	movq	%rbx, %rax
	movq	%r9, %r8
.L4:
	testq	%r13, %r13
	je	.L10
.L7:
	leaq	-1(%r8), %r9
	movq	%r9, %rdx
	shrq	$63, %rdx
	addq	%r9, %rdx
	sarq	%rdx
	cmpq	%rsi, %r8
	jg	.L9
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L19:
	leaq	-1(%rdx), %r8
	movq	%r9, (%rax)
	movq	%r8, %rax
	shrq	$63, %rax
	addq	%r8, %rax
	movq	%rdx, %r8
	sarq	%rax
	cmpq	%rdx, %rsi
	jge	.L18
	movq	%rax, %rdx
.L9:
	leaq	(%rdi,%rdx,8), %r10
	movl	16(%rcx), %ebx
	leaq	(%rdi,%r8,8), %rax
	movq	(%r10), %r9
	cmpl	%ebx, 16(%r9)
	jl	.L19
.L8:
	movq	%rcx, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	leaq	(%rdi,%rsi,8), %rax
	testq	%r13, %r13
	jne	.L8
	movq	%rsi, %r8
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	-2(%rdx), %r9
	movq	%r9, %rdx
	shrq	$63, %rdx
	addq	%r9, %rdx
	sarq	%rdx
	cmpq	%rdx, %r8
	jne	.L7
	leaq	1(%r8,%r8), %r8
	leaq	(%rdi,%r8,8), %rdx
	movq	(%rdx), %r9
	movq	%r9, (%rax)
	movq	%rdx, %rax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%r10, %rax
	movq	%rcx, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22840:
	.size	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPPN2v88internal13RegExpCaptureESt6vectorIS5_NS3_13ZoneAllocatorIS5_EEEEElS5_NS0_5__ops15_Iter_comp_iterINS3_12_GLOBAL__N_122RegExpCaptureIndexLessEEEEvT_T0_SI_T1_T2_.isra.0, .-_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPPN2v88internal13RegExpCaptureESt6vectorIS5_NS3_13ZoneAllocatorIS5_EEEEElS5_NS0_5__ops15_Iter_comp_iterINS3_12_GLOBAL__N_122RegExpCaptureIndexLessEEEEvT_T0_SI_T1_T2_.isra.0
	.section	.text._ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPPN2v88internal13RegExpCaptureESt6vectorIS5_NS3_13ZoneAllocatorIS5_EEEEElNS0_5__ops15_Iter_comp_iterINS3_12_GLOBAL__N_122RegExpCaptureIndexLessEEEEvT_SH_T0_T1_,"ax",@progbits
	.p2align 4
	.type	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPPN2v88internal13RegExpCaptureESt6vectorIS5_NS3_13ZoneAllocatorIS5_EEEEElNS0_5__ops15_Iter_comp_iterINS3_12_GLOBAL__N_122RegExpCaptureIndexLessEEEEvT_SH_T0_T1_, @function
_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPPN2v88internal13RegExpCaptureESt6vectorIS5_NS3_13ZoneAllocatorIS5_EEEEElNS0_5__ops15_Iter_comp_iterINS3_12_GLOBAL__N_122RegExpCaptureIndexLessEEEEvT_SH_T0_T1_:
.LFB22178:
	.cfi_startproc
	movq	%rsi, %rax
	subq	%rdi, %rax
	cmpq	$128, %rax
	jle	.L49
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L22
	leaq	8(%rdi), %r14
.L24:
	movq	%rsi, %rax
	movq	-8(%rsi), %r11
	movq	(%rbx), %rcx
	subq	$1, %r15
	subq	%rbx, %rax
	movq	%rax, %rdx
	shrq	$63, %rax
	movl	16(%r11), %r8d
	sarq	$3, %rdx
	addq	%rdx, %rax
	sarq	%rax
	leaq	(%rbx,%rax,8), %r9
	movq	8(%rbx), %rax
	movq	(%r9), %r10
	movl	16(%rax), %edi
	movl	16(%r10), %edx
	cmpl	%edx, %edi
	jge	.L29
	cmpl	%r8d, %edx
	jl	.L34
	cmpl	%r8d, %edi
	jge	.L32
.L52:
	movq	%r11, (%rbx)
	movq	%rcx, -8(%rsi)
	movq	(%rbx), %rax
.L31:
	movl	16(%rcx), %r9d
	movl	16(%rax), %edi
	movq	%r14, %r12
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L35:
	movq	(%r12), %r8
	movq	%r12, %r13
	cmpl	%edi, 16(%r8)
	jl	.L36
	leaq	-8(%rax), %rdx
	cmpl	%r9d, %edi
	jge	.L37
	subq	$16, %rax
	.p2align 4,,10
	.p2align 3
.L38:
	movq	(%rax), %rcx
	movq	%rax, %rdx
	subq	$8, %rax
	cmpl	%edi, 16(%rcx)
	jg	.L38
.L37:
	cmpq	%r12, %rdx
	jbe	.L53
	movq	%rcx, (%r12)
	movq	-8(%rdx), %rcx
	movq	%r8, (%rdx)
	movq	(%rbx), %rax
	movl	16(%rcx), %r9d
	movl	16(%rax), %edi
	movq	%rdx, %rax
.L36:
	addq	$8, %r12
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L53:
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPPN2v88internal13RegExpCaptureESt6vectorIS5_NS3_13ZoneAllocatorIS5_EEEEElNS0_5__ops15_Iter_comp_iterINS3_12_GLOBAL__N_122RegExpCaptureIndexLessEEEEvT_SH_T0_T1_
	movq	%r12, %rax
	subq	%rbx, %rax
	cmpq	$128, %rax
	jle	.L20
	testq	%r15, %r15
	je	.L22
	movq	%r12, %rsi
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L29:
	cmpl	%r8d, %edi
	jge	.L33
	movq	%rcx, %xmm1
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	movq	-8(%rsi), %rcx
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L33:
	cmpl	%r8d, %edx
	jl	.L52
.L34:
	movq	%r10, (%rbx)
	movq	%rcx, (%r9)
	movq	(%rbx), %rax
	movq	-8(%rsi), %rcx
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L22:
	sarq	$3, %rax
	leaq	-2(%rax), %rsi
	movq	%rax, %r12
	sarq	%rsi
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L54:
	subq	$1, %rsi
.L26:
	movq	(%rbx,%rsi,8), %rcx
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPPN2v88internal13RegExpCaptureESt6vectorIS5_NS3_13ZoneAllocatorIS5_EEEEElS5_NS0_5__ops15_Iter_comp_iterINS3_12_GLOBAL__N_122RegExpCaptureIndexLessEEEEvT_T0_SI_T1_T2_.isra.0
	testq	%rsi, %rsi
	jne	.L54
	subq	$8, %r13
	.p2align 4,,10
	.p2align 3
.L27:
	movq	(%rbx), %rax
	movq	%r13, %r12
	movq	0(%r13), %rcx
	xorl	%esi, %esi
	subq	%rbx, %r12
	movq	%rbx, %rdi
	subq	$8, %r13
	movq	%rax, 8(%r13)
	movq	%r12, %rdx
	sarq	$3, %rdx
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPPN2v88internal13RegExpCaptureESt6vectorIS5_NS3_13ZoneAllocatorIS5_EEEEElS5_NS0_5__ops15_Iter_comp_iterINS3_12_GLOBAL__N_122RegExpCaptureIndexLessEEEEvT_T0_SI_T1_T2_.isra.0
	cmpq	$8, %r12
	jg	.L27
.L20:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	%rcx, %xmm2
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rbx)
	movq	-8(%rsi), %rcx
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE22178:
	.size	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPPN2v88internal13RegExpCaptureESt6vectorIS5_NS3_13ZoneAllocatorIS5_EEEEElNS0_5__ops15_Iter_comp_iterINS3_12_GLOBAL__N_122RegExpCaptureIndexLessEEEEvT_SH_T0_T1_, .-_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPPN2v88internal13RegExpCaptureESt6vectorIS5_NS3_13ZoneAllocatorIS5_EEEEElNS0_5__ops15_Iter_comp_iterINS3_12_GLOBAL__N_122RegExpCaptureIndexLessEEEEvT_SH_T0_T1_
	.section	.text._ZN2v88internal20RegExpCharacterClassC2EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE,"axG",@progbits,_ZN2v88internal20RegExpCharacterClassC5EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20RegExpCharacterClassC2EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	.type	_ZN2v88internal20RegExpCharacterClassC2EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE, @function
_ZN2v88internal20RegExpCharacterClassC2EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE:
.LFB7873:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal20RegExpCharacterClassE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	movq	%rdx, 8(%rdi)
	movw	%ax, 16(%rdi)
	movl	%ecx, 24(%rdi)
	movl	12(%rdx), %ecx
	movl	%r8d, 28(%rdi)
	testl	%ecx, %ecx
	je	.L63
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movl	8(%rdx), %eax
	movq	%rdx, %r12
	testl	%eax, %eax
	jle	.L57
	movq	(%rdx), %rax
	movl	$1, 12(%rdx)
	movabsq	$4785070309113856, %rcx
	movq	%rcx, (%rax)
.L58:
	xorl	$1, 28(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movq	%rsi, %rdi
	leal	1(%rax,%rax), %r13d
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	movslq	%r13d, %rsi
	salq	$3, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L64
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L60:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L61
	movq	(%r12), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$3, %rdx
.L61:
	addl	$1, %eax
	movq	%rcx, (%r12)
	movl	%eax, 12(%r12)
	movabsq	$4785070309113856, %rax
	movl	%r13d, 8(%r12)
	movq	%rax, (%rcx,%rdx)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L64:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L60
	.cfi_endproc
.LFE7873:
	.size	_ZN2v88internal20RegExpCharacterClassC2EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE, .-_ZN2v88internal20RegExpCharacterClassC2EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	.weak	_ZN2v88internal20RegExpCharacterClassC1EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	.set	_ZN2v88internal20RegExpCharacterClassC1EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE,_ZN2v88internal20RegExpCharacterClassC2EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	.section	.rodata._ZN2v88internal12RegExpParserC2EPNS0_16FlatStringReaderEPNS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEEPNS0_7IsolateEPNS0_4ZoneE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Aborting on stack overflow"
.LC1:
	.string	"(location_) != nullptr"
.LC2:
	.string	"Check failed: %s."
.LC3:
	.string	"Regular expression too large"
	.section	.text._ZN2v88internal12RegExpParserC2EPNS0_16FlatStringReaderEPNS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEEPNS0_7IsolateEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParserC2EPNS0_16FlatStringReaderEPNS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEEPNS0_7IsolateEPNS0_4ZoneE
	.type	_ZN2v88internal12RegExpParserC2EPNS0_16FlatStringReaderEPNS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEEPNS0_7IsolateEPNS0_4ZoneE, @function
_ZN2v88internal12RegExpParserC2EPNS0_16FlatStringReaderEPNS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEEPNS0_7IsolateEPNS0_4ZoneE:
.LFB18359:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	%r9, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movhps	-72(%rbp), %xmm0
	movq	%rdx, 16(%rdi)
	movabsq	$4294967296, %rax
	movq	%rax, 72(%rdi)
	xorl	%eax, %eax
	movw	%ax, 80(%rdi)
	movl	36(%rsi), %eax
	movups	%xmm0, (%rdi)
	pxor	%xmm0, %xmm0
	movq	$0, 24(%rdi)
	movq	%rsi, 48(%rdi)
	movl	$2097152, 56(%rdi)
	movl	%ecx, 60(%rdi)
	movq	$0, 64(%rdi)
	movups	%xmm0, 32(%rdi)
	testl	%eax, %eax
	jg	.L88
	addl	$1, %eax
	movb	$0, 76(%rdi)
	movl	%eax, 64(%rdi)
.L65:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L89
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	movq	%r8, %r12
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r12), %rax
	jnb	.L67
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L90
	movl	$216, %edi
	call	_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE@PLT
	cmpb	$0, 81(%rbx)
	movq	%rax, %r12
	jne	.L65
	movq	%rax, %rdi
	call	strlen@PLT
	movb	$1, 81(%rbx)
	movq	(%rbx), %rdi
	leaq	-64(%rbp), %rsi
	cltq
	movq	%r12, -64(%rbp)
	movq	%rax, -56(%rbp)
.L87:
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%rbx), %rdx
	testq	%rax, %rax
	je	.L91
	movq	%rax, (%rdx)
	movq	48(%rbx), %rax
	movl	$2097152, 56(%rbx)
	movl	36(%rax), %eax
	movl	%eax, 64(%rbx)
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L67:
	movq	8(%rbx), %rax
	cmpq	$268435456, 8(%rax)
	jbe	.L72
	cmpb	$0, 81(%rbx)
	jne	.L65
	leaq	.LC3(%rip), %rax
	movb	$1, 81(%rbx)
	movq	(%rbx), %rdi
	leaq	-48(%rbp), %rsi
	movq	%rax, -48(%rbp)
	movq	$28, -40(%rbp)
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L72:
	movq	48(%rbx), %rax
	movslq	64(%rbx), %rcx
	movzbl	32(%rax), %edi
	movq	40(%rax), %r8
	movq	%rcx, %rdx
	testb	%dil, %dil
	jne	.L92
	movzwl	(%r8,%rcx,2), %esi
.L76:
	leal	1(%rdx), %ecx
	testb	$16, 60(%rbx)
	je	.L77
	cmpl	36(%rax), %ecx
	jge	.L77
	movl	%esi, %eax
	andl	$64512, %eax
	cmpl	$55296, %eax
	jne	.L77
	testb	%dil, %dil
	je	.L93
	.p2align 4,,10
	.p2align 3
.L77:
	movl	%ecx, 64(%rbx)
	movl	%esi, 56(%rbx)
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L92:
	movzbl	(%r8,%rcx), %esi
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L91:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L93:
	movslq	%ecx, %rax
	movzwl	(%r8,%rax,2), %eax
	movl	%eax, %edi
	andw	$-1024, %di
	cmpw	$-9216, %di
	jne	.L77
	sall	$10, %esi
	andl	$1023, %eax
	leal	2(%rdx), %ecx
	andl	$1047552, %esi
	orl	%eax, %esi
	addl	$65536, %esi
	jmp	.L77
.L89:
	call	__stack_chk_fail@PLT
.L90:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18359:
	.size	_ZN2v88internal12RegExpParserC2EPNS0_16FlatStringReaderEPNS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEEPNS0_7IsolateEPNS0_4ZoneE, .-_ZN2v88internal12RegExpParserC2EPNS0_16FlatStringReaderEPNS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEEPNS0_7IsolateEPNS0_4ZoneE
	.globl	_ZN2v88internal12RegExpParserC1EPNS0_16FlatStringReaderEPNS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEEPNS0_7IsolateEPNS0_4ZoneE
	.set	_ZN2v88internal12RegExpParserC1EPNS0_16FlatStringReaderEPNS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEEPNS0_7IsolateEPNS0_4ZoneE,_ZN2v88internal12RegExpParserC2EPNS0_16FlatStringReaderEPNS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEEPNS0_7IsolateEPNS0_4ZoneE
	.section	.text._ZN2v88internal12RegExpParser4NextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser4NextEv
	.type	_ZN2v88internal12RegExpParser4NextEv, @function
_ZN2v88internal12RegExpParser4NextEv:
.LFB18362:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	movl	64(%rdi), %edx
	movl	$2097152, %r8d
	movl	36(%rax), %ecx
	cmpl	%ecx, %edx
	jge	.L94
	cmpb	$0, 32(%rax)
	movq	40(%rax), %r9
	movslq	%edx, %rsi
	jne	.L104
	movzwl	(%r9,%rsi,2), %r8d
	leaq	(%rsi,%rsi), %r10
	movl	%r8d, %eax
	testb	$16, 60(%rdi)
	je	.L94
	addl	$1, %edx
	cmpl	%edx, %ecx
	jg	.L105
.L94:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	movzbl	(%r9,%rsi), %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	andw	$-1024, %ax
	cmpw	$-10240, %ax
	jne	.L94
	movzwl	2(%r9,%r10), %eax
	movl	%eax, %edx
	andw	$-1024, %dx
	cmpw	$-9216, %dx
	jne	.L94
	sall	$10, %r8d
	andl	$1023, %eax
	andl	$1047552, %r8d
	orl	%eax, %r8d
	addl	$65536, %r8d
	jmp	.L94
	.cfi_endproc
.LFE18362:
	.size	_ZN2v88internal12RegExpParser4NextEv, .-_ZN2v88internal12RegExpParser4NextEv
	.section	.text._ZN2v88internal12RegExpParser7AdvanceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser7AdvanceEv
	.type	_ZN2v88internal12RegExpParser7AdvanceEv, @function
_ZN2v88internal12RegExpParser7AdvanceEv:
.LFB18363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	48(%rdi), %rax
	movl	36(%rax), %ecx
	cmpl	%ecx, 64(%rdi)
	jl	.L129
	movl	$2097152, 56(%rdi)
	movl	36(%rax), %eax
	movb	$0, 76(%rdi)
	addl	$1, %eax
	movl	%eax, 64(%rdi)
.L106:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L130
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movq	(%rdi), %r12
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r12), %rax
	jnb	.L108
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L131
	movl	$216, %edi
	call	_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE@PLT
	cmpb	$0, 81(%rbx)
	movq	%rax, %r12
	jne	.L106
	movq	%rax, %rdi
	call	strlen@PLT
	movb	$1, 81(%rbx)
	movq	(%rbx), %rdi
	leaq	-64(%rbp), %rsi
	cltq
	movq	%r12, -64(%rbp)
	movq	%rax, -56(%rbp)
.L128:
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%rbx), %rdx
	testq	%rax, %rax
	je	.L132
	movq	%rax, (%rdx)
	movq	48(%rbx), %rax
	movl	$2097152, 56(%rbx)
	movl	36(%rax), %eax
	movl	%eax, 64(%rbx)
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L108:
	movq	8(%rbx), %rax
	cmpq	$268435456, 8(%rax)
	jbe	.L113
	cmpb	$0, 81(%rbx)
	jne	.L106
	leaq	.LC3(%rip), %rax
	movb	$1, 81(%rbx)
	movq	(%rbx), %rdi
	leaq	-48(%rbp), %rsi
	movq	%rax, -48(%rbp)
	movq	$28, -40(%rbp)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L113:
	movq	48(%rbx), %rax
	movslq	64(%rbx), %rcx
	movzbl	32(%rax), %edi
	movq	40(%rax), %r8
	movq	%rcx, %rdx
	testb	%dil, %dil
	jne	.L133
	movzwl	(%r8,%rcx,2), %esi
.L117:
	leal	1(%rdx), %ecx
	testb	$16, 60(%rbx)
	je	.L118
	cmpl	36(%rax), %ecx
	jge	.L118
	movl	%esi, %eax
	andl	$64512, %eax
	cmpl	$55296, %eax
	jne	.L118
	testb	%dil, %dil
	je	.L134
	.p2align 4,,10
	.p2align 3
.L118:
	movl	%ecx, 64(%rbx)
	movl	%esi, 56(%rbx)
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L133:
	movzbl	(%r8,%rcx), %esi
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L132:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L134:
	movslq	%ecx, %rax
	movzwl	(%r8,%rax,2), %eax
	movl	%eax, %edi
	andw	$-1024, %di
	cmpw	$-9216, %di
	jne	.L118
	sall	$10, %esi
	andl	$1023, %eax
	leal	2(%rdx), %ecx
	andl	$1047552, %esi
	orl	%eax, %esi
	addl	$65536, %esi
	jmp	.L118
.L130:
	call	__stack_chk_fail@PLT
.L131:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18363:
	.size	_ZN2v88internal12RegExpParser7AdvanceEv, .-_ZN2v88internal12RegExpParser7AdvanceEv
	.section	.text._ZN2v88internal12RegExpParser5ResetEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser5ResetEi
	.type	_ZN2v88internal12RegExpParser5ResetEi, @function
_ZN2v88internal12RegExpParser5ResetEi:
.LFB18364:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	48(%rdi), %rax
	movl	%esi, 64(%rdi)
	cmpl	36(%rax), %esi
	setl	76(%rdi)
	cmpl	36(%rax), %esi
	jl	.L158
	movl	$2097152, 56(%rdi)
	movl	36(%rax), %eax
	movb	$0, 76(%rdi)
	addl	$1, %eax
	movl	%eax, 64(%rdi)
.L135:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L159
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	movq	(%rdi), %r12
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r12), %rax
	jnb	.L137
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L160
	movl	$216, %edi
	call	_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE@PLT
	cmpb	$0, 81(%rbx)
	movq	%rax, %r12
	jne	.L135
	movq	%rax, %rdi
	call	strlen@PLT
	movb	$1, 81(%rbx)
	movq	(%rbx), %rdi
	leaq	-64(%rbp), %rsi
	cltq
	movq	%r12, -64(%rbp)
	movq	%rax, -56(%rbp)
.L157:
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%rbx), %rdx
	testq	%rax, %rax
	je	.L161
	movq	%rax, (%rdx)
	movq	48(%rbx), %rax
	movl	$2097152, 56(%rbx)
	movl	36(%rax), %eax
	movl	%eax, 64(%rbx)
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L137:
	movq	8(%rbx), %rax
	cmpq	$268435456, 8(%rax)
	jbe	.L142
	cmpb	$0, 81(%rbx)
	jne	.L135
	leaq	.LC3(%rip), %rax
	movb	$1, 81(%rbx)
	movq	(%rbx), %rdi
	leaq	-48(%rbp), %rsi
	movq	%rax, -48(%rbp)
	movq	$28, -40(%rbp)
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L142:
	movq	48(%rbx), %rax
	movslq	64(%rbx), %rcx
	movzbl	32(%rax), %edi
	movq	40(%rax), %r8
	movq	%rcx, %rdx
	testb	%dil, %dil
	jne	.L162
	movzwl	(%r8,%rcx,2), %esi
.L146:
	leal	1(%rdx), %ecx
	testb	$16, 60(%rbx)
	je	.L147
	cmpl	36(%rax), %ecx
	jge	.L147
	movl	%esi, %eax
	andl	$64512, %eax
	cmpl	$55296, %eax
	jne	.L147
	testb	%dil, %dil
	je	.L163
	.p2align 4,,10
	.p2align 3
.L147:
	movl	%ecx, 64(%rbx)
	movl	%esi, 56(%rbx)
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L162:
	movzbl	(%r8,%rcx), %esi
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L161:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L163:
	movslq	%ecx, %rax
	movzwl	(%r8,%rax,2), %eax
	movl	%eax, %edi
	andw	$-1024, %di
	cmpw	$-9216, %di
	jne	.L147
	sall	$10, %esi
	andl	$1023, %eax
	leal	2(%rdx), %ecx
	andl	$1047552, %esi
	orl	%eax, %esi
	addl	$65536, %esi
	jmp	.L147
.L159:
	call	__stack_chk_fail@PLT
.L160:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18364:
	.size	_ZN2v88internal12RegExpParser5ResetEi, .-_ZN2v88internal12RegExpParser5ResetEi
	.section	.text._ZN2v88internal12RegExpParser7AdvanceEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser7AdvanceEi
	.type	_ZN2v88internal12RegExpParser7AdvanceEi, @function
_ZN2v88internal12RegExpParser7AdvanceEi:
.LFB18365:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	48(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	64(%rdi), %eax
	leal	-1(%rax,%rsi), %eax
	movl	%eax, 64(%rdi)
	cmpl	36(%rdx), %eax
	jl	.L187
	movl	$2097152, 56(%rdi)
	movl	36(%rdx), %eax
	movb	$0, 76(%rdi)
	addl	$1, %eax
	movl	%eax, 64(%rdi)
.L164:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L188
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movq	(%rdi), %r12
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r12), %rax
	jnb	.L166
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L189
	movl	$216, %edi
	call	_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE@PLT
	cmpb	$0, 81(%rbx)
	movq	%rax, %r12
	jne	.L164
	movq	%rax, %rdi
	call	strlen@PLT
	movb	$1, 81(%rbx)
	movq	(%rbx), %rdi
	leaq	-64(%rbp), %rsi
	cltq
	movq	%r12, -64(%rbp)
	movq	%rax, -56(%rbp)
.L186:
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%rbx), %rdx
	testq	%rax, %rax
	je	.L190
	movq	%rax, (%rdx)
	movq	48(%rbx), %rax
	movl	$2097152, 56(%rbx)
	movl	36(%rax), %eax
	movl	%eax, 64(%rbx)
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L166:
	movq	8(%rbx), %rax
	cmpq	$268435456, 8(%rax)
	jbe	.L171
	cmpb	$0, 81(%rbx)
	jne	.L164
	leaq	.LC3(%rip), %rax
	movb	$1, 81(%rbx)
	movq	(%rbx), %rdi
	leaq	-48(%rbp), %rsi
	movq	%rax, -48(%rbp)
	movq	$28, -40(%rbp)
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L171:
	movq	48(%rbx), %rax
	movslq	64(%rbx), %rcx
	movzbl	32(%rax), %edi
	movq	40(%rax), %r8
	movq	%rcx, %rdx
	testb	%dil, %dil
	jne	.L191
	movzwl	(%r8,%rcx,2), %esi
.L175:
	leal	1(%rdx), %ecx
	testb	$16, 60(%rbx)
	je	.L176
	cmpl	36(%rax), %ecx
	jge	.L176
	movl	%esi, %eax
	andl	$64512, %eax
	cmpl	$55296, %eax
	jne	.L176
	testb	%dil, %dil
	je	.L192
	.p2align 4,,10
	.p2align 3
.L176:
	movl	%ecx, 64(%rbx)
	movl	%esi, 56(%rbx)
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L191:
	movzbl	(%r8,%rcx), %esi
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L190:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L192:
	movslq	%ecx, %rax
	movzwl	(%r8,%rax,2), %eax
	movl	%eax, %edi
	andw	$-1024, %di
	cmpw	$-9216, %di
	jne	.L176
	sall	$10, %esi
	andl	$1023, %eax
	leal	2(%rdx), %ecx
	andl	$1047552, %esi
	orl	%eax, %esi
	addl	$65536, %esi
	jmp	.L176
.L188:
	call	__stack_chk_fail@PLT
.L189:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18365:
	.size	_ZN2v88internal12RegExpParser7AdvanceEi, .-_ZN2v88internal12RegExpParser7AdvanceEi
	.section	.text._ZN2v88internal12RegExpParser6simpleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser6simpleEv
	.type	_ZN2v88internal12RegExpParser6simpleEv, @function
_ZN2v88internal12RegExpParser6simpleEv:
.LFB18366:
	.cfi_startproc
	endbr64
	movzbl	77(%rdi), %eax
	ret
	.cfi_endproc
.LFE18366:
	.size	_ZN2v88internal12RegExpParser6simpleEv, .-_ZN2v88internal12RegExpParser6simpleEv
	.section	.text._ZN2v88internal12RegExpParser24IsSyntaxCharacterOrSlashEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser24IsSyntaxCharacterOrSlashEi
	.type	_ZN2v88internal12RegExpParser24IsSyntaxCharacterOrSlashEi, @function
_ZN2v88internal12RegExpParser24IsSyntaxCharacterOrSlashEi:
.LFB18367:
	.cfi_startproc
	endbr64
	cmpl	$94, %edi
	jg	.L195
	xorl	%eax, %eax
	cmpl	$35, %edi
	jle	.L194
	subl	$36, %edi
	cmpl	$58, %edi
	ja	.L200
	leaq	.L198(%rip), %rdx
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal12RegExpParser24IsSyntaxCharacterOrSlashEi,"a",@progbits
	.align 4
	.align 4
.L198:
	.long	.L197-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L197-.L198
	.long	.L197-.L198
	.long	.L197-.L198
	.long	.L197-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L197-.L198
	.long	.L197-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L197-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L200-.L198
	.long	.L197-.L198
	.long	.L197-.L198
	.long	.L197-.L198
	.long	.L197-.L198
	.section	.text._ZN2v88internal12RegExpParser24IsSyntaxCharacterOrSlashEi
.L197:
	movl	$1, %eax
	ret
.L200:
	xorl	%eax, %eax
.L194:
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	subl	$123, %edi
	cmpl	$3, %edi
	setb	%al
	ret
	.cfi_endproc
.LFE18367:
	.size	_ZN2v88internal12RegExpParser24IsSyntaxCharacterOrSlashEi, .-_ZN2v88internal12RegExpParser24IsSyntaxCharacterOrSlashEi
	.section	.text._ZN2v88internal12RegExpParser11ReportErrorENS0_6VectorIKcEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser11ReportErrorENS0_6VectorIKcEE
	.type	_ZN2v88internal12RegExpParser11ReportErrorENS0_6VectorIKcEE, @function
_ZN2v88internal12RegExpParser11ReportErrorENS0_6VectorIKcEE:
.LFB18368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 81(%rdi)
	je	.L207
.L203:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L208
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore_state
	movb	$1, 81(%rdi)
	movq	%rdi, %rbx
	movslq	%edx, %rdx
	movq	(%rdi), %rdi
	movq	%rsi, -48(%rbp)
	leaq	-48(%rbp), %rsi
	movq	%rdx, -40(%rbp)
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%rbx), %rdx
	testq	%rax, %rax
	je	.L209
	movq	%rax, (%rdx)
	movq	48(%rbx), %rax
	movl	$2097152, 56(%rbx)
	movl	36(%rax), %eax
	movl	%eax, 64(%rbx)
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L209:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L208:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18368:
	.size	_ZN2v88internal12RegExpParser11ReportErrorENS0_6VectorIKcEE, .-_ZN2v88internal12RegExpParser11ReportErrorENS0_6VectorIKcEE
	.section	.text._ZN2v88internal12RegExpParser15ScanForCapturesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser15ScanForCapturesEv
	.type	_ZN2v88internal12RegExpParser15ScanForCapturesEv, @function
_ZN2v88internal12RegExpParser15ScanForCapturesEv:
.LFB18385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	.LC3(%rip), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movl	68(%rdi), %r13d
	movl	56(%rdi), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	64(%rdi), %eax
	movl	%eax, -100(%rbp)
	.p2align 4,,10
	.p2align 3
.L216:
	cmpl	$2097152, %ebx
	je	.L212
.L249:
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	cmpl	$91, %ebx
	je	.L232
	cmpl	$92, %ebx
	je	.L214
	cmpl	$40, %ebx
	je	.L215
.L246:
	movl	56(%r12), %ebx
	cmpl	$2097152, %ebx
	jne	.L249
	.p2align 4,,10
	.p2align 3
.L212:
	movl	-100(%rbp), %r14d
	movq	48(%r12), %rax
	movl	%r13d, 72(%r12)
	movq	%r12, %rdi
	movb	$1, 79(%r12)
	subl	$1, %r14d
	movl	%r14d, 64(%r12)
	cmpl	36(%rax), %r14d
	setl	76(%r12)
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L250
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	.p2align 4,,10
	.p2align 3
.L232:
	movl	56(%r12), %ebx
	cmpl	$2097152, %ebx
	je	.L212
	movq	48(%r12), %rax
	movl	36(%rax), %ecx
	cmpl	%ecx, 64(%r12)
	jl	.L251
	movl	$2097152, 56(%r12)
	movl	36(%rax), %eax
	movb	$0, 76(%r12)
	addl	$1, %eax
	movl	%eax, 64(%r12)
.L224:
	cmpl	$92, %ebx
	je	.L252
	cmpl	$93, %ebx
	jne	.L232
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L251:
	movq	(%r12), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r14), %rax
	jnb	.L220
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L253
	movl	$216, %edi
	call	_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE@PLT
	cmpb	$0, 81(%r12)
	movq	%rax, %r14
	jne	.L224
	movq	%rax, %rdi
	call	strlen@PLT
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-96(%rbp), %rsi
	cltq
	movq	%r14, -96(%rbp)
	movq	%rax, -88(%rbp)
.L248:
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r12), %rdx
	testq	%rax, %rax
	je	.L254
	movq	%rax, (%rdx)
	movq	48(%r12), %rax
	movl	$2097152, 56(%r12)
	movl	36(%rax), %eax
	movl	%eax, 64(%r12)
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L220:
	movq	8(%r12), %rax
	cmpq	$268435456, 8(%rax)
	jbe	.L225
	cmpb	$0, 81(%r12)
	jne	.L224
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-80(%rbp), %rsi
	movq	%r15, -80(%rbp)
	movq	$28, -72(%rbp)
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L215:
	movl	56(%r12), %ebx
	cmpl	$63, %ebx
	je	.L255
.L233:
	addl	$1, %r13d
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L214:
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	56(%r12), %ebx
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L225:
	movq	48(%r12), %rax
	movslq	64(%r12), %rcx
	movzbl	32(%rax), %edi
	movq	40(%rax), %r8
	movq	%rcx, %rdx
	testb	%dil, %dil
	je	.L228
	movzbl	(%r8,%rcx), %esi
.L229:
	leal	1(%rdx), %ecx
	testb	$16, 60(%r12)
	je	.L230
	cmpl	36(%rax), %ecx
	jge	.L230
	movl	%esi, %eax
	andl	$64512, %eax
	cmpl	$55296, %eax
	jne	.L230
	testb	%dil, %dil
	je	.L256
	.p2align 4,,10
	.p2align 3
.L230:
	movl	%ecx, 64(%r12)
	movl	%esi, 56(%r12)
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L228:
	movzwl	(%r8,%rcx,2), %esi
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L255:
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	56(%r12), %ebx
	cmpl	$60, %ebx
	jne	.L216
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	56(%r12), %ebx
	cmpl	$33, %ebx
	je	.L216
	cmpl	$61, %ebx
	je	.L216
	movb	$1, 80(%r12)
	jmp	.L233
.L254:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L256:
	movslq	%ecx, %rax
	movzwl	(%r8,%rax,2), %eax
	movl	%eax, %edi
	andw	$-1024, %di
	cmpw	$-9216, %di
	jne	.L230
	sall	$10, %esi
	andl	$1023, %eax
	leal	2(%rdx), %ecx
	andl	$1047552, %esi
	orl	%eax, %esi
	addl	$65536, %esi
	jmp	.L230
.L253:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L250:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18385:
	.size	_ZN2v88internal12RegExpParser15ScanForCapturesEv, .-_ZN2v88internal12RegExpParser15ScanForCapturesEv
	.section	.text._ZN2v88internal12RegExpParser23ParseBackReferenceIndexEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser23ParseBackReferenceIndexEPi
	.type	_ZN2v88internal12RegExpParser23ParseBackReferenceIndexEPi, @function
_ZN2v88internal12RegExpParser23ParseBackReferenceIndexEPi:
.LFB18386:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	$2097104, %ebx
	subq	$56, %rsp
	movq	48(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	64(%rdi), %rax
	movl	36(%rdx), %esi
	leal	-1(%rax), %r14d
	leal	1(%rax), %ecx
	cmpl	%esi, %eax
	jge	.L258
	cmpb	$0, 32(%rdx)
	movq	40(%rdx), %rdi
	jne	.L301
	movzwl	(%rdi,%rax,2), %ebx
	leaq	(%rax,%rax), %rdx
	movl	%ebx, %eax
	testb	$16, 60(%r12)
	je	.L296
	cmpl	%ecx, %esi
	jg	.L260
.L296:
	subl	$48, %ebx
.L258:
	movl	%ecx, 64(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	56(%r12), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L264
	leal	(%rbx,%rbx,4), %edx
	leal	(%rax,%rdx,2), %ebx
	cmpl	$65536, %ebx
	jg	.L299
	.p2align 4,,10
	.p2align 3
.L265:
	movq	48(%r12), %rax
	movl	36(%rax), %ecx
	cmpl	%ecx, 64(%r12)
	jl	.L302
	movl	$2097152, 56(%r12)
	movl	36(%rax), %eax
	movb	$0, 76(%r12)
	addl	$1, %eax
	movl	%eax, 64(%r12)
.L264:
	cmpl	%ebx, 68(%r12)
	jge	.L280
	cmpb	$0, 79(%r12)
	je	.L303
.L281:
	cmpl	%ebx, 72(%r12)
	jl	.L299
.L280:
	movl	%ebx, 0(%r13)
	movl	$1, %eax
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L302:
	movq	(%r12), %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r15), %rax
	jnb	.L269
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L304
	movl	$216, %edi
	call	_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE@PLT
	cmpb	$0, 81(%r12)
	movq	%rax, %r15
	jne	.L297
	movq	%rax, %rdi
	call	strlen@PLT
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-96(%rbp), %rsi
	cltq
	movq	%r15, -96(%rbp)
	movq	%rax, -88(%rbp)
.L300:
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r12), %rdx
	testq	%rax, %rax
	je	.L305
	movq	%rax, (%rdx)
	movq	48(%r12), %rax
	movl	$2097152, 56(%r12)
	movl	36(%rax), %eax
	movl	%eax, 64(%r12)
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L269:
	movq	8(%r12), %rax
	cmpq	$268435456, 8(%rax)
	jbe	.L274
	cmpb	$0, 81(%r12)
	je	.L275
.L297:
	movl	56(%r12), %edx
	leal	-48(%rdx), %eax
.L272:
	cmpl	$9, %eax
	ja	.L264
	leal	(%rbx,%rbx,4), %eax
	leal	-48(%rdx,%rax,2), %ebx
	cmpl	$65536, %ebx
	jle	.L265
.L299:
	movq	48(%r12), %rax
	movl	%r14d, 64(%r12)
	movq	%r12, %rdi
	cmpl	36(%rax), %r14d
	setl	76(%r12)
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	xorl	%eax, %eax
.L257:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L306
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_restore_state
	movq	48(%r12), %rcx
	movslq	64(%r12), %rax
	movzbl	32(%rcx), %edi
	movq	40(%rcx), %r8
	movq	%rax, %rsi
	testb	%dil, %dil
	jne	.L307
	movzwl	(%r8,%rax,2), %edx
.L278:
	leal	1(%rsi), %r9d
	movl	%edx, %eax
	testb	$16, 60(%r12)
	je	.L279
	cmpl	36(%rcx), %r9d
	jge	.L279
	movl	%edx, %ecx
	andl	$64512, %ecx
	cmpl	$55296, %ecx
	jne	.L279
	testb	%dil, %dil
	je	.L308
	.p2align 4,,10
	.p2align 3
.L279:
	movl	%r9d, 64(%r12)
	subl	$48, %eax
	movl	%edx, 56(%r12)
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L301:
	movzbl	(%rdi,%rax), %ebx
	subl	$48, %ebx
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L307:
	movzbl	(%r8,%rax), %edx
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L275:
	leaq	.LC3(%rip), %rax
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-80(%rbp), %rsi
	movq	%rax, -80(%rbp)
	movq	$28, -72(%rbp)
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L260:
	andw	$-1024, %ax
	cmpw	$-10240, %ax
	jne	.L296
	movzwl	2(%rdi,%rdx), %eax
	movl	%eax, %edx
	andw	$-1024, %dx
	cmpw	$-9216, %dx
	jne	.L296
	sall	$10, %ebx
	andl	$1023, %eax
	andl	$1047552, %ebx
	orl	%eax, %ebx
	addl	$65488, %ebx
	jmp	.L258
.L303:
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser15ScanForCapturesEv
	jmp	.L281
.L305:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L308:
	movslq	%r9d, %rcx
	movzwl	(%r8,%rcx,2), %ecx
	movl	%ecx, %edi
	andw	$-1024, %di
	cmpw	$-9216, %di
	jne	.L279
	sall	$10, %eax
	andl	$1023, %ecx
	leal	2(%rsi), %r9d
	andl	$1047552, %eax
	orl	%ecx, %eax
	addl	$65536, %eax
	movl	%eax, %edx
	jmp	.L279
.L304:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L306:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18386:
	.size	_ZN2v88internal12RegExpParser23ParseBackReferenceIndexEPi, .-_ZN2v88internal12RegExpParser23ParseBackReferenceIndexEPi
	.section	.text._ZN2v88internal12RegExpParser10GetCaptureEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser10GetCaptureEi
	.type	_ZN2v88internal12RegExpParser10GetCaptureEi, @function
_ZN2v88internal12RegExpParser10GetCaptureEi:
.LFB18392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %r12
	movl	72(%rdi), %r14d
	movl	%esi, -60(%rbp)
	cmpb	$0, 79(%rdi)
	cmove	68(%rdi), %r14d
	testq	%r12, %r12
	je	.L329
.L311:
	movl	12(%r12), %eax
	leaq	16+_ZTVN2v88internal13RegExpCaptureE(%rip), %r9
	cmpl	%eax, %r14d
	jg	.L317
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L331:
	movq	(%r12), %rdx
	leal	1(%rax), %esi
	movl	%esi, 12(%r12)
	movq	%rbx, (%rdx,%rax,8)
	movq	24(%rcx), %r12
	movl	12(%r12), %eax
	cmpl	%eax, %r14d
	jle	.L318
.L317:
	movq	8(%rcx), %r15
	leal	1(%rax), %r13d
	movq	16(%r15), %rbx
	movq	24(%r15), %rax
	subq	%rbx, %rax
	cmpq	$31, %rax
	jbe	.L330
	leaq	32(%rbx), %rax
	movq	%rax, 16(%r15)
.L320:
	movq	%r9, (%rbx)
	movq	$0, 8(%rbx)
	movl	%r13d, 16(%rbx)
	movq	$0, 24(%rbx)
	movslq	12(%r12), %rax
	movl	8(%r12), %edx
	cmpl	%edx, %eax
	jl	.L331
	movq	16(%r15), %rdi
	movq	24(%r15), %rax
	leal	1(%rdx,%rdx), %r13d
	movslq	%r13d, %rsi
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L332
	addq	%rdi, %rsi
	movq	%rsi, 16(%r15)
.L324:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L325
	movq	(%r12), %rsi
	movq	%rcx, -56(%rbp)
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movq	-56(%rbp), %rcx
	leaq	16+_ZTVN2v88internal13RegExpCaptureE(%rip), %r9
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$3, %rdx
.L325:
	addl	$1, %eax
	movq	%rdi, (%r12)
	movl	%eax, 12(%r12)
	movl	%r13d, 8(%r12)
	movq	%rbx, (%rdi,%rdx)
	movq	24(%rcx), %r12
	movl	12(%r12), %eax
	cmpl	%eax, %r14d
	jg	.L317
.L318:
	movl	-60(%rbp), %r14d
	movq	(%r12), %rax
	subl	$1, %r14d
	movslq	%r14d, %r14
	movq	(%rax,%r14,8), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L330:
	.cfi_restore_state
	movl	$32, %esi
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	leaq	16+_ZTVN2v88internal13RegExpCaptureE(%rip), %r9
	movq	%rax, %rbx
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L332:
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	leaq	16+_ZTVN2v88internal13RegExpCaptureE(%rip), %r9
	movq	%rax, %rdi
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L329:
	movq	8(%rdi), %r13
	movq	16(%r13), %rbx
	movq	24(%r13), %rax
	subq	%rbx, %rax
	cmpq	$15, %rax
	jbe	.L333
	leaq	16(%rbx), %rax
	movq	%rax, 16(%r13)
.L313:
	testl	%r14d, %r14d
	jg	.L334
.L314:
	movq	%r12, (%rbx)
	movq	%rbx, %r12
	movl	%r14d, 8(%rbx)
	movl	$0, 12(%rbx)
	movq	%rbx, 24(%rcx)
	jmp	.L311
.L334:
	movq	16(%r13), %r12
	movq	24(%r13), %rax
	movslq	%r14d, %rsi
	salq	$3, %rsi
	subq	%r12, %rax
	cmpq	%rax, %rsi
	ja	.L335
	addq	%r12, %rsi
	movq	%rsi, 16(%r13)
	jmp	.L314
.L333:
	movq	%rdi, -56(%rbp)
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %rbx
	jmp	.L313
.L335:
	movq	%r13, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r12
	jmp	.L314
	.cfi_endproc
.LFE18392:
	.size	_ZN2v88internal12RegExpParser10GetCaptureEi, .-_ZN2v88internal12RegExpParser10GetCaptureEi
	.section	.rodata._ZN2v88internal12RegExpParser24PatchNamedBackReferencesEv.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"Invalid named capture referenced"
	.section	.text._ZN2v88internal12RegExpParser24PatchNamedBackReferencesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser24PatchNamedBackReferencesEv
	.type	_ZN2v88internal12RegExpParser24PatchNamedBackReferencesEv, @function
_ZN2v88internal12RegExpParser24PatchNamedBackReferencesEv:
.LFB18391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	testq	%rax, %rax
	je	.L336
	cmpq	$0, 32(%rdi)
	movq	%rdi, %r12
	je	.L381
	movl	12(%rax), %edx
	testl	%edx, %edx
	jle	.L336
	xorl	%r13d, %r13d
	leaq	16+_ZTVN2v88internal13RegExpCaptureE(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L363:
	movq	(%rax), %rax
	movq	8(%r12), %rdi
	movq	(%rax,%r13,8), %r14
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L382
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L344:
	movq	$0, 24(%rax)
	movq	%rbx, (%rax)
	movq	$0, 8(%rax)
	movl	$0, 16(%rax)
	movq	16(%r14), %rdx
	movq	%rdx, 24(%rax)
	movq	32(%r12), %rax
	leaq	16(%rax), %r11
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L345
	movq	16(%rdx), %r8
	movq	8(%rdx), %rcx
	movq	%r11, %r10
	movq	%r8, %r9
	subq	%rcx, %r9
	.p2align 4,,10
	.p2align 3
.L346:
	movq	32(%rax), %rdx
	movq	24(%rdx), %rdx
	movq	16(%rdx), %rsi
	movq	8(%rdx), %rdx
	movq	%rsi, %r15
	leaq	(%rdx,%r9), %rdi
	subq	%rdx, %r15
	cmpq	%r9, %r15
	cmovg	%rdi, %rsi
	movq	%rcx, %rdi
	cmpq	%rsi, %rdx
	jne	.L353
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L375:
	ja	.L352
	addq	$2, %rdx
	addq	$2, %rdi
	cmpq	%rdx, %rsi
	je	.L349
.L353:
	movzwl	(%rdi), %r15d
	cmpw	%r15w, (%rdx)
	jnb	.L375
.L350:
	movq	24(%rax), %rax
.L354:
	testq	%rax, %rax
	jne	.L346
	cmpq	%r11, %r10
	je	.L345
	movq	32(%r10), %rdx
	movq	24(%rdx), %rax
	movq	16(%rax), %rdi
	movq	8(%rax), %rax
	movq	%rdi, %rsi
	subq	%rax, %rsi
	leaq	(%rcx,%rsi), %r10
	cmpq	%rsi, %r9
	cmovg	%r10, %r8
	cmpq	%r8, %rcx
	jne	.L359
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L376:
	ja	.L358
	addq	$2, %rcx
	addq	$2, %rax
	cmpq	%rcx, %r8
	je	.L356
.L359:
	movzwl	(%rax), %esi
	cmpw	%si, (%rcx)
	jnb	.L376
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L349:
	cmpq	%rdi, %r8
	jne	.L350
.L352:
	movq	%rax, %r10
	movq	16(%rax), %rax
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L358:
	movl	16(%rdx), %esi
	movq	%r12, %rdi
	addq	$1, %r13
	call	_ZN2v88internal12RegExpParser10GetCaptureEi
	movq	%rax, 8(%r14)
	movq	40(%r12), %rax
	cmpl	%r13d, 12(%rax)
	jg	.L363
.L336:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L383
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
	cmpq	%rax, %rdi
	je	.L358
	.p2align 4,,10
	.p2align 3
.L345:
	cmpb	$0, 81(%r12)
	jne	.L336
	leaq	.LC4(%rip), %rax
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-80(%rbp), %rsi
	movq	%rax, -80(%rbp)
	movq	$32, -72(%rbp)
.L380:
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r12), %rdx
	testq	%rax, %rax
	je	.L384
	movq	%rax, (%rdx)
	movq	48(%r12), %rax
	movl	$2097152, 56(%r12)
	movl	36(%rax), %eax
	movl	%eax, 64(%r12)
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L382:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L381:
	cmpb	$0, 81(%rdi)
	jne	.L336
	leaq	.LC4(%rip), %rax
	movb	$1, 81(%rdi)
	leaq	-96(%rbp), %rsi
	movq	(%rdi), %rdi
	movq	%rax, -96(%rbp)
	movq	$32, -88(%rbp)
	jmp	.L380
.L384:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L383:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18391:
	.size	_ZN2v88internal12RegExpParser24PatchNamedBackReferencesEv, .-_ZN2v88internal12RegExpParser24PatchNamedBackReferencesEv
	.section	.rodata._ZN2v88internal12RegExpParser20CreateCaptureNameMapEv.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN2v88internal12RegExpParser20CreateCaptureNameMapEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser20CreateCaptureNameMapEv
	.type	_ZN2v88internal12RegExpParser20CreateCaptureNameMapEv, @function
_ZN2v88internal12RegExpParser20CreateCaptureNameMapEv:
.LFB18394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	testq	%rax, %rax
	je	.L386
	cmpq	$0, 48(%rax)
	jne	.L387
.L386:
	xorl	%eax, %eax
.L388:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L449
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L387:
	.cfi_restore_state
	movq	32(%rax), %r12
	leaq	16(%rax), %r15
	movq	8(%rdi), %rbx
	movq	%rdi, %r13
	cmpq	%r15, %r12
	je	.L389
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L390:
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	addq	$1, %r14
	movq	%rax, %rdi
	cmpq	%rax, %r15
	jne	.L390
	cmpq	$268435455, %r14
	jg	.L450
	movq	24(%rbx), %rax
	leaq	0(,%r14,8), %rsi
	movq	16(%rbx), %r14
	subq	%r14, %rax
	cmpq	%rax, %rsi
	ja	.L451
	addq	%r14, %rsi
	movq	%rsi, 16(%rbx)
.L392:
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L393:
	movq	32(%r12), %rax
	movq	%r12, %rdi
	addq	$8, %rbx
	movq	%rax, -8(%rbx)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r15
	jne	.L393
	movq	%rbx, %rcx
	subq	%r14, %rcx
	movq	%rcx, %rax
	movq	%rcx, -88(%rbp)
	sarq	$3, %rax
	leal	(%rax,%rax), %r15d
	cmpq	%rbx, %r14
	je	.L394
	bsrq	%rax, %rax
	movl	$63, %edx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	xorq	$63, %rax
	leaq	8(%r14), %r12
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPPN2v88internal13RegExpCaptureESt6vectorIS5_NS3_13ZoneAllocatorIS5_EEEEElNS0_5__ops15_Iter_comp_iterINS3_12_GLOBAL__N_122RegExpCaptureIndexLessEEEEvT_SH_T0_T1_
	movq	-88(%rbp), %rcx
	movq	%r12, %r9
	cmpq	$128, %rcx
	jle	.L395
	leaq	128(%r14), %r8
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L453:
	cmpq	%r9, %r14
	je	.L397
	movq	%r9, %rdx
	movl	$8, %eax
	movq	%r14, %rsi
	movq	%r10, -104(%rbp)
	subq	%r14, %rdx
	leaq	(%r14,%rax), %rdi
	movq	%r8, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	memmove@PLT
	movq	-104(%rbp), %r10
	movq	-96(%rbp), %r8
	movq	-88(%rbp), %r9
.L397:
	movq	%r10, (%r14)
.L398:
	addq	$8, %r9
	cmpq	%r9, %r8
	je	.L452
.L401:
	movq	(%r9), %r10
	movq	(%r14), %rax
	movl	16(%r10), %esi
	cmpl	16(%rax), %esi
	jl	.L453
	movq	-8(%r9), %rdx
	leaq	-8(%r9), %rax
	cmpl	%esi, 16(%rdx)
	jle	.L424
	.p2align 4,,10
	.p2align 3
.L400:
	movq	%rdx, 8(%rax)
	movq	%rax, %rsi
	movq	-8(%rax), %rdx
	subq	$8, %rax
	movl	16(%rdx), %ecx
	cmpl	%ecx, 16(%r10)
	jl	.L400
.L399:
	movq	%r10, (%rsi)
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L395:
	movq	%r12, %r8
	cmpq	%r12, %rbx
	jne	.L413
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L454:
	cmpq	%r14, %r8
	je	.L409
	movq	%r8, %rdx
	movl	$8, %eax
	movq	%r14, %rsi
	movq	%r9, -96(%rbp)
	subq	%r14, %rdx
	leaq	(%r14,%rax), %rdi
	movq	%r8, -88(%rbp)
	call	memmove@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %r8
.L409:
	movq	%r9, (%r14)
.L410:
	addq	$8, %r8
	cmpq	%r8, %rbx
	je	.L403
.L413:
	movq	(%r8), %r9
	movq	(%r14), %rax
	movq	%r8, %rsi
	movl	16(%r9), %edi
	cmpl	16(%rax), %edi
	jl	.L454
	movq	-8(%r8), %rdx
	leaq	-8(%r8), %rax
	cmpl	16(%rdx), %edi
	jge	.L411
	.p2align 4,,10
	.p2align 3
.L412:
	movq	%rdx, 8(%rax)
	movq	%rax, %rsi
	movq	-8(%rax), %rdx
	subq	$8, %rax
	movl	16(%rdx), %ecx
	cmpl	%ecx, 16(%r9)
	jl	.L412
.L411:
	movq	%r9, (%rsi)
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L425:
	movq	%r8, %rdi
	addq	$8, %r8
	movq	%rsi, (%rdi)
	cmpq	%r8, %rbx
	jne	.L402
	.p2align 4,,10
	.p2align 3
.L403:
	movq	0(%r13), %rax
	movl	%r15d, %esi
	xorl	%edx, %edx
	movl	$16, %r15d
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r13
	leaq	-80(%rbp), %rax
	movq	%rax, -96(%rbp)
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L455:
	addq	$8, %r12
.L418:
	movq	(%r14), %rax
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	24(%rax), %rax
	movq	8(%rax), %rdx
	movq	16(%rax), %rax
	subq	%rdx, %rax
	movq	%rdx, -80(%rbp)
	xorl	%edx, %edx
	sarq	%rax
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringItEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	0(%r13), %rdi
	movq	(%rax), %rdx
	leaq	-1(%rdi,%r15), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L419
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %r9
	movq	%rax, -104(%rbp)
	testl	$262144, %r9d
	je	.L416
	movq	%rdx, -128(%rbp)
	movq	%rsi, -120(%rbp)
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	8(%rax), %r9
.L416:
	andl	$24, %r9d
	je	.L419
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L419
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L419:
	movq	(%r14), %rax
	movq	0(%r13), %rdx
	movq	%r12, %r14
	addq	$16, %r15
	movslq	16(%rax), %rax
	salq	$32, %rax
	movq	%rax, -9(%r15,%rdx)
	cmpq	%r12, %rbx
	jne	.L455
.L420:
	movq	%r13, %rax
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L452:
	cmpq	%r8, %rbx
	je	.L403
	.p2align 4,,10
	.p2align 3
.L402:
	movq	-8(%r8), %rdx
	movq	(%r8), %rsi
	leaq	-8(%r8), %rax
	movl	16(%rdx), %ecx
	cmpl	%ecx, 16(%rsi)
	jge	.L425
	.p2align 4,,10
	.p2align 3
.L405:
	movq	%rdx, 8(%rax)
	movq	%rax, %rdi
	movq	-8(%rax), %rdx
	subq	$8, %rax
	movl	16(%rdx), %ecx
	cmpl	%ecx, 16(%rsi)
	jl	.L405
	addq	$8, %r8
	movq	%rsi, (%rdi)
	cmpq	%r8, %rbx
	jne	.L402
	jmp	.L403
.L451:
	movq	%rbx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L389:
	xorl	%r15d, %r15d
.L394:
	movq	0(%r13), %rdi
	xorl	%edx, %edx
	movl	%r15d, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r13
	jmp	.L420
.L424:
	movq	%r9, %rsi
	jmp	.L399
.L449:
	call	__stack_chk_fail@PLT
.L450:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE18394:
	.size	_ZN2v88internal12RegExpParser20CreateCaptureNameMapEv, .-_ZN2v88internal12RegExpParser20CreateCaptureNameMapEv
	.section	.text._ZN2v88internal12RegExpParser16HasNamedCapturesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser16HasNamedCapturesEv
	.type	_ZN2v88internal12RegExpParser16HasNamedCapturesEv, @function
_ZN2v88internal12RegExpParser16HasNamedCapturesEv:
.LFB18398:
	.cfi_startproc
	endbr64
	movzbl	80(%rdi), %eax
	testb	%al, %al
	jne	.L459
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 79(%rdi)
	je	.L462
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L462:
	.cfi_restore_state
	call	_ZN2v88internal12RegExpParser15ScanForCapturesEv
	movzbl	80(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L459:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE18398:
	.size	_ZN2v88internal12RegExpParser16HasNamedCapturesEv, .-_ZN2v88internal12RegExpParser16HasNamedCapturesEv
	.section	.text._ZN2v88internal12RegExpParser17RegExpParserState20IsInsideCaptureGroupEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser17RegExpParserState20IsInsideCaptureGroupEi
	.type	_ZN2v88internal12RegExpParser17RegExpParserState20IsInsideCaptureGroupEi, @function
_ZN2v88internal12RegExpParser17RegExpParserState20IsInsideCaptureGroupEi:
.LFB18399:
	.cfi_startproc
	endbr64
	.p2align 4,,10
	.p2align 3
.L466:
	cmpl	$1, 16(%rdi)
	jne	.L464
	cmpl	24(%rdi), %esi
	je	.L467
	jg	.L468
.L464:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L466
.L468:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L467:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE18399:
	.size	_ZN2v88internal12RegExpParser17RegExpParserState20IsInsideCaptureGroupEi, .-_ZN2v88internal12RegExpParser17RegExpParserState20IsInsideCaptureGroupEi
	.section	.text._ZN2v88internal12RegExpParser17RegExpParserState20IsInsideCaptureGroupEPKNS0_10ZoneVectorItEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser17RegExpParserState20IsInsideCaptureGroupEPKNS0_10ZoneVectorItEE
	.type	_ZN2v88internal12RegExpParser17RegExpParserState20IsInsideCaptureGroupEPKNS0_10ZoneVectorItEE, @function
_ZN2v88internal12RegExpParser17RegExpParserState20IsInsideCaptureGroupEPKNS0_10ZoneVectorItEE:
.LFB18400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L471:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L481
.L473:
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.L471
	movq	8(%rax), %rdi
	movq	16(%rax), %rdx
	movq	8(%r12), %rsi
	movq	16(%r12), %rax
	subq	%rdi, %rdx
	subq	%rsi, %rax
	cmpq	%rax, %rdx
	jne	.L471
	testq	%rdx, %rdx
	jne	.L482
.L475:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L482:
	.cfi_restore_state
	call	memcmp@PLT
	testl	%eax, %eax
	je	.L475
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L473
.L481:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18400:
	.size	_ZN2v88internal12RegExpParser17RegExpParserState20IsInsideCaptureGroupEPKNS0_10ZoneVectorItEE, .-_ZN2v88internal12RegExpParser17RegExpParserState20IsInsideCaptureGroupEPKNS0_10ZoneVectorItEE
	.section	.text._ZN2v88internal12RegExpParser23ParseIntervalQuantifierEPiS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser23ParseIntervalQuantifierEPiS2_
	.type	_ZN2v88internal12RegExpParser23ParseIntervalQuantifierEPiS2_, @function
_ZN2v88internal12RegExpParser23ParseIntervalQuantifierEPiS2_:
.LFB18401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	64(%rdi), %eax
	leal	-1(%rax), %r13d
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	56(%r12), %eax
	leal	-48(%rax), %edx
	cmpl	$9, %edx
	ja	.L484
	xorl	%r15d, %r15d
	movl	$3435973837, %ebx
	.p2align 4,,10
	.p2align 3
.L485:
	leal	(%r15,%r15,4), %eax
	leal	(%rdx,%rax,2), %r15d
	movq	48(%r12), %rax
	movl	36(%rax), %ecx
	cmpl	%ecx, 64(%r12)
	jl	.L531
	movl	$2097152, 56(%r12)
	movb	$0, 76(%r12)
.L504:
	movl	%r13d, 64(%r12)
.L529:
	cmpl	36(%rax), %r13d
	movq	%r12, %rdi
	setl	76(%r12)
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	xorl	%eax, %eax
.L483:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L532
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L531:
	.cfi_restore_state
	movq	(%r12), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r14), %rax
	jnb	.L490
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L533
	movl	$216, %edi
	call	_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE@PLT
	cmpb	$0, 81(%r12)
	movq	%rax, %r14
	je	.L492
.L527:
	movl	56(%r12), %edx
	leal	-48(%rdx), %eax
.L493:
	cmpl	$9, %eax
	ja	.L487
	subl	$48, %edx
	movl	$2147483647, %eax
	subl	%edx, %eax
	imulq	%rbx, %rax
	shrq	$35, %rax
	cmpl	%r15d, %eax
	jge	.L485
	.p2align 4,,10
	.p2align 3
.L488:
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	56(%r12), %edx
	leal	-48(%rdx), %eax
	cmpl	$9, %eax
	jbe	.L488
	movl	$2147483647, %r15d
.L487:
	cmpl	$125, %edx
	je	.L534
	cmpl	$44, %edx
	jne	.L535
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	56(%r12), %edx
	cmpl	$125, %edx
	je	.L536
	subl	$48, %edx
	xorl	%r14d, %r14d
	movl	$3435973837, %ebx
	cmpl	$9, %edx
	ja	.L511
	.p2align 4,,10
	.p2align 3
.L507:
	leal	(%r14,%r14,4), %eax
	movq	%r12, %rdi
	leal	(%rdx,%rax,2), %r14d
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	56(%r12), %eax
	leal	-48(%rax), %edx
	cmpl	$9, %edx
	ja	.L509
	movl	$2147483647, %eax
	subl	%edx, %eax
	imulq	%rbx, %rax
	shrq	$35, %rax
	cmpl	%r14d, %eax
	jge	.L507
	.p2align 4,,10
	.p2align 3
.L508:
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	56(%r12), %eax
	leal	-48(%rax), %edx
	cmpl	$9, %edx
	jbe	.L508
	movl	$2147483647, %r14d
.L509:
	cmpl	$125, %eax
	jne	.L511
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
.L503:
	movq	-104(%rbp), %rax
	movl	%r15d, (%rax)
	movq	-112(%rbp), %rax
	movl	%r14d, (%rax)
	movl	$1, %eax
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L490:
	movq	8(%r12), %rax
	cmpq	$268435456, 8(%rax)
	jbe	.L496
	cmpb	$0, 81(%r12)
	jne	.L527
	leaq	.LC3(%rip), %rax
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-80(%rbp), %rsi
	movq	%rax, -80(%rbp)
	movq	$28, -72(%rbp)
.L530:
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r12), %rdx
	testq	%rax, %rax
	je	.L537
	movq	%rax, (%rdx)
	movq	48(%r12), %rax
	movl	$2097152, 56(%r12)
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L496:
	movq	48(%r12), %rax
	movslq	64(%r12), %rdx
	movzbl	32(%rax), %r9d
	movq	40(%rax), %r10
	movq	%rdx, %rdi
	testb	%r9b, %r9b
	je	.L499
	movzbl	(%r10,%rdx), %edx
.L500:
	leal	1(%rdi), %r8d
	movl	%edx, %esi
	testb	$16, 60(%r12)
	je	.L501
	cmpl	36(%rax), %r8d
	jge	.L501
	movl	%edx, %eax
	andl	$64512, %eax
	cmpl	$55296, %eax
	jne	.L501
	testb	%r9b, %r9b
	je	.L538
	.p2align 4,,10
	.p2align 3
.L501:
	movl	%r8d, 64(%r12)
	leal	-48(%rsi), %eax
	movl	%edx, 56(%r12)
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L492:
	movq	%rax, %rdi
	call	strlen@PLT
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-96(%rbp), %rsi
	cltq
	movq	%r14, -96(%rbp)
	movq	%rax, -88(%rbp)
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L499:
	movzwl	(%r10,%rdx,2), %edx
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L484:
	movq	48(%r12), %rax
	movl	%r13d, 64(%r12)
	movq	%r12, %rdi
	cmpl	%r13d, 36(%rax)
	setg	76(%r12)
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	xorl	%eax, %eax
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L511:
	movl	%r13d, 64(%r12)
	movq	48(%r12), %rax
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L534:
	movq	%r12, %rdi
	movl	%r15d, %r14d
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	jmp	.L503
.L537:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L536:
	movq	%r12, %rdi
	movl	$2147483647, %r14d
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	jmp	.L503
.L538:
	movslq	%r8d, %rax
	movzwl	(%r10,%rax,2), %eax
	movl	%eax, %r9d
	andw	$-1024, %r9w
	cmpw	$-9216, %r9w
	jne	.L501
	sall	$10, %edx
	andl	$1023, %eax
	leal	2(%rdi), %r8d
	movl	%edx, %esi
	andl	$1047552, %esi
	orl	%esi, %eax
	leal	65536(%rax), %esi
	movl	%esi, %edx
	jmp	.L501
.L533:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L532:
	call	__stack_chk_fail@PLT
.L535:
	movq	48(%r12), %rax
	jmp	.L504
	.cfi_endproc
.LFE18401:
	.size	_ZN2v88internal12RegExpParser23ParseIntervalQuantifierEPiS2_, .-_ZN2v88internal12RegExpParser23ParseIntervalQuantifierEPiS2_
	.section	.text._ZN2v88internal12RegExpParser17ParseOctalLiteralEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser17ParseOctalLiteralEv
	.type	_ZN2v88internal12RegExpParser17ParseOctalLiteralEv, @function
_ZN2v88internal12RegExpParser17ParseOctalLiteralEv:
.LFB18402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	56(%rdi), %eax
	leal	-48(%rax), %r13d
	movq	48(%rdi), %rax
	movl	36(%rax), %ecx
	cmpl	%ecx, 64(%rdi)
	jl	.L564
	movl	$2097152, 56(%rdi)
	movl	36(%rax), %eax
	movb	$0, 76(%rdi)
	addl	$1, %eax
	movl	%eax, 64(%rdi)
.L539:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L565
	addq	$56, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L564:
	.cfi_restore_state
	movq	(%rdi), %rbx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%rbx), %rax
	jnb	.L541
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L566
	movl	$216, %edi
	call	_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE@PLT
	cmpb	$0, 81(%r12)
	movq	%rax, %rbx
	jne	.L561
	movq	%rax, %rdi
	call	strlen@PLT
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-80(%rbp), %rsi
	cltq
	movq	%rbx, -80(%rbp)
	movq	%rax, -72(%rbp)
.L563:
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r12), %rdx
	testq	%rax, %rax
	je	.L567
	movq	%rax, (%rdx)
	movq	48(%r12), %rax
	movl	$2097152, 56(%r12)
	movl	36(%rax), %eax
	movl	%eax, 64(%r12)
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L541:
	movq	8(%r12), %rax
	cmpq	$268435456, 8(%rax)
	jbe	.L547
	cmpb	$0, 81(%r12)
	je	.L548
.L561:
	movl	56(%r12), %eax
	leal	-48(%rax), %edx
.L544:
	cmpl	$7, %edx
	ja	.L539
	leal	-48(%rax,%r13,8), %r13d
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	cmpl	$31, %r13d
	jg	.L539
	movl	56(%r12), %eax
	leal	-48(%rax), %edx
	cmpl	$7, %edx
	ja	.L539
	movq	%r12, %rdi
	leal	-48(%rax,%r13,8), %r13d
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L547:
	movq	48(%r12), %rcx
	movslq	64(%r12), %rax
	movzbl	32(%rcx), %r8d
	movq	40(%rcx), %r9
	movq	%rax, %rsi
	testb	%r8b, %r8b
	jne	.L568
	movzwl	(%r9,%rax,2), %eax
.L551:
	leal	1(%rsi), %edi
	movl	%eax, %edx
	testb	$16, 60(%r12)
	je	.L552
	cmpl	36(%rcx), %edi
	jge	.L552
	movl	%eax, %ecx
	andl	$64512, %ecx
	cmpl	$55296, %ecx
	jne	.L552
	testb	%r8b, %r8b
	je	.L569
	.p2align 4,,10
	.p2align 3
.L552:
	movl	%edi, 64(%r12)
	subl	$48, %edx
	movl	%eax, 56(%r12)
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L568:
	movzbl	(%r9,%rax), %eax
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L548:
	leaq	.LC3(%rip), %rax
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-64(%rbp), %rsi
	movq	%rax, -64(%rbp)
	movq	$28, -56(%rbp)
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L567:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L569:
	movslq	%edi, %rcx
	movzwl	(%r9,%rcx,2), %ecx
	movl	%ecx, %r8d
	andw	$-1024, %r8w
	cmpw	$-9216, %r8w
	jne	.L552
	sall	$10, %eax
	andl	$1023, %ecx
	leal	2(%rsi), %edi
	andl	$1047552, %eax
	orl	%ecx, %eax
	leal	65536(%rax), %edx
	movl	%edx, %eax
	jmp	.L552
.L565:
	call	__stack_chk_fail@PLT
.L566:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18402:
	.size	_ZN2v88internal12RegExpParser17ParseOctalLiteralEv, .-_ZN2v88internal12RegExpParser17ParseOctalLiteralEv
	.section	.text._ZN2v88internal12RegExpParser14ParseHexEscapeEiPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser14ParseHexEscapeEiPi
	.type	_ZN2v88internal12RegExpParser14ParseHexEscapeEiPi, @function
_ZN2v88internal12RegExpParser14ParseHexEscapeEiPi:
.LFB18403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	64(%rdi), %eax
	movl	%eax, -100(%rbp)
	testl	%esi, %esi
	jle	.L589
	movq	%rdi, %r15
	movl	%esi, %ebx
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L575:
	movl	$2097152, 56(%r15)
	movl	36(%rax), %eax
	movb	$0, 76(%r15)
	addl	$1, %eax
	movl	%eax, 64(%r15)
.L580:
	addl	$1, %r12d
	cmpl	%r12d, %ebx
	je	.L571
.L587:
	movl	56(%r15), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L572
	orl	$32, %eax
	leal	-49(%rax), %edx
	cmpl	$5, %edx
	ja	.L573
	subl	$39, %eax
	js	.L573
.L572:
	movl	%r14d, %ecx
	sall	$4, %ecx
	leal	(%rcx,%rax), %r14d
	movq	48(%r15), %rax
	movl	36(%rax), %esi
	cmpl	%esi, 64(%r15)
	jge	.L575
	movq	(%r15), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r13), %rax
	jnb	.L576
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L600
	movl	$216, %edi
	call	_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE@PLT
	cmpb	$0, 81(%r15)
	jne	.L580
	movq	%rax, %rdi
	movq	%rax, -112(%rbp)
	call	strlen@PLT
	movq	-112(%rbp), %rdx
	movq	(%r15), %rdi
	leaq	-96(%rbp), %rsi
	movb	$1, 81(%r15)
	cltq
	movq	%rdx, -96(%rbp)
	xorl	%edx, %edx
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r15), %rdx
	testq	%rax, %rax
	je	.L601
.L598:
	movq	%rax, (%rdx)
	movq	48(%r15), %rax
	addl	$1, %r12d
	movl	$2097152, 56(%r15)
	movl	36(%rax), %eax
	movl	%eax, 64(%r15)
	cmpl	%r12d, %ebx
	jne	.L587
	.p2align 4,,10
	.p2align 3
.L571:
	movq	-120(%rbp), %rax
	movl	%r14d, (%rax)
	movl	$1, %eax
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L576:
	movq	8(%r15), %rax
	cmpq	$268435456, 8(%rax)
	jbe	.L581
	cmpb	$0, 81(%r15)
	jne	.L580
	movb	$1, 81(%r15)
	movq	(%r15), %rdi
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	leaq	.LC3(%rip), %rax
	movq	$28, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r15), %rdx
	testq	%rax, %rax
	jne	.L598
.L601:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L581:
	movq	48(%r15), %rax
	movslq	64(%r15), %rsi
	movzbl	32(%rax), %r9d
	movq	40(%rax), %r10
	movq	%rsi, %rdx
	testb	%r9b, %r9b
	jne	.L602
	movzwl	(%r10,%rsi,2), %edi
.L585:
	leal	1(%rdx), %esi
	testb	$16, 60(%r15)
	je	.L586
	cmpl	36(%rax), %esi
	jge	.L586
	movl	%edi, %eax
	andl	$64512, %eax
	cmpl	$55296, %eax
	jne	.L586
	testb	%r9b, %r9b
	je	.L603
	.p2align 4,,10
	.p2align 3
.L586:
	movl	%esi, 64(%r15)
	movl	%edi, 56(%r15)
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L573:
	movl	-100(%rbp), %r13d
	movq	48(%r15), %rax
	movq	%r15, %rdi
	subl	$1, %r13d
	movl	%r13d, 64(%r15)
	cmpl	36(%rax), %r13d
	setl	76(%r15)
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	xorl	%eax, %eax
.L570:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L604
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L602:
	.cfi_restore_state
	movzbl	(%r10,%rsi), %edi
	jmp	.L585
.L603:
	movslq	%esi, %rax
	movzwl	(%r10,%rax,2), %eax
	movl	%eax, %r9d
	andw	$-1024, %r9w
	cmpw	$-9216, %r9w
	jne	.L586
	sall	$10, %edi
	andl	$1023, %eax
	leal	2(%rdx), %esi
	andl	$1047552, %edi
	orl	%edi, %eax
	leal	65536(%rax), %edi
	jmp	.L586
.L589:
	xorl	%r14d, %r14d
	jmp	.L571
.L600:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L604:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18403:
	.size	_ZN2v88internal12RegExpParser14ParseHexEscapeEiPi, .-_ZN2v88internal12RegExpParser14ParseHexEscapeEiPi
	.section	.text._ZN2v88internal12RegExpParser18ParseUnicodeEscapeEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser18ParseUnicodeEscapeEPi
	.type	_ZN2v88internal12RegExpParser18ParseUnicodeEscapeEPi, @function
_ZN2v88internal12RegExpParser18ParseUnicodeEscapeEPi:
.LFB18404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -104(%rbp)
	movl	64(%rdi), %ecx
	leal	-1(%rcx), %r12d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	56(%rdi), %eax
	cmpl	$123, %eax
	jne	.L606
	testb	$16, 60(%rdi)
	jne	.L659
.L606:
	movl	$4, %r13d
	xorl	%ebx, %ebx
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L616:
	movl	$2097152, 56(%r15)
	movl	36(%rax), %eax
	movb	$0, 76(%r15)
	addl	$1, %eax
	movl	%eax, 64(%r15)
.L621:
	subl	$1, %r13d
	je	.L628
.L662:
	movl	56(%r15), %eax
.L629:
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L613
	orl	$32, %eax
	leal	-49(%rax), %edx
	cmpl	$5, %edx
	ja	.L608
	subl	$39, %eax
	js	.L608
.L613:
	sall	$4, %ebx
	addl	%eax, %ebx
	movq	48(%r15), %rax
	movl	36(%rax), %ecx
	cmpl	%ecx, 64(%r15)
	jge	.L616
	movq	(%r15), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r14), %rax
	jnb	.L617
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L660
	movl	$216, %edi
	call	_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE@PLT
	cmpb	$0, 81(%r15)
	jne	.L621
	movq	%rax, %rdi
	movq	%rax, -112(%rbp)
	call	strlen@PLT
	movq	-112(%rbp), %rdx
	movq	(%r15), %rdi
	leaq	-96(%rbp), %rsi
	cltq
	movb	$1, 81(%r15)
	movq	%rdx, -96(%rbp)
	movq	%rax, -88(%rbp)
.L658:
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r15), %rdx
	testq	%rax, %rax
	je	.L661
	movq	%rax, (%rdx)
	movq	48(%r15), %rax
	movl	$2097152, 56(%r15)
	movl	36(%rax), %eax
	movl	%eax, 64(%r15)
	subl	$1, %r13d
	jne	.L662
	.p2align 4,,10
	.p2align 3
.L628:
	movq	-104(%rbp), %rax
	movl	%ebx, (%rax)
	testb	$16, 60(%r15)
	je	.L656
	andl	$64512, %ebx
	movl	$1, %eax
	cmpl	$55296, %ebx
	jne	.L605
	cmpl	$92, 56(%r15)
	jne	.L605
	movq	48(%r15), %rdx
	movl	64(%r15), %eax
	movl	36(%rdx), %ecx
	leal	-1(%rax), %r13d
	cmpl	%ecx, %eax
	jge	.L631
	movslq	%eax, %rdi
	addl	$1, %eax
	cmpb	$0, 32(%rdx)
	movq	40(%rdx), %rsi
	je	.L632
	movzbl	(%rsi,%rdi), %esi
.L633:
	cmpl	$117, %esi
	je	.L663
.L631:
	movl	%r13d, 64(%r15)
	cmpl	36(%rdx), %r13d
	setl	76(%r15)
.L657:
	movq	%r15, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
.L656:
	movl	$1, %eax
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L617:
	movq	8(%r15), %rax
	cmpq	$268435456, 8(%rax)
	jbe	.L622
	cmpb	$0, 81(%r15)
	jne	.L621
	leaq	.LC3(%rip), %rax
	movb	$1, 81(%r15)
	movq	(%r15), %rdi
	leaq	-80(%rbp), %rsi
	movq	%rax, -80(%rbp)
	movq	$28, -72(%rbp)
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L659:
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	56(%r15), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L607
	orl	$32, %eax
	leal	-49(%rax), %edx
	cmpl	$5, %edx
	ja	.L608
	subl	$39, %eax
	js	.L608
.L607:
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L609:
	sall	$4, %ebx
	addl	%eax, %ebx
	cmpl	$1114111, %ebx
	jg	.L608
	movq	%r15, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	56(%r15), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L609
	orl	$32, %eax
	leal	-49(%rax), %edx
	cmpl	$5, %edx
	ja	.L611
	subl	$39, %eax
	jns	.L609
.L611:
	movq	-104(%rbp), %rax
	movl	%ebx, (%rax)
	cmpl	$125, 56(%r15)
	je	.L657
	.p2align 4,,10
	.p2align 3
.L608:
	movq	48(%r15), %rax
	movl	%r12d, 64(%r15)
	movq	%r15, %rdi
	cmpl	%r12d, 36(%rax)
	setg	76(%r15)
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	xorl	%eax, %eax
.L605:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L664
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L622:
	.cfi_restore_state
	movq	48(%r15), %rax
	movslq	64(%r15), %rcx
	movzbl	32(%rax), %edi
	movq	40(%rax), %r9
	movq	%rcx, %rdx
	testb	%dil, %dil
	jne	.L665
	movzwl	(%r9,%rcx,2), %esi
.L626:
	leal	1(%rdx), %ecx
	testb	$16, 60(%r15)
	je	.L627
	cmpl	36(%rax), %ecx
	jge	.L627
	movl	%esi, %eax
	andl	$64512, %eax
	cmpl	$55296, %eax
	jne	.L627
	testb	%dil, %dil
	je	.L666
	.p2align 4,,10
	.p2align 3
.L627:
	movl	%ecx, 64(%r15)
	movl	%esi, 56(%r15)
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L665:
	movzbl	(%r9,%rcx), %esi
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L661:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L666:
	movslq	%ecx, %rax
	movzwl	(%r9,%rax,2), %eax
	movl	%eax, %edi
	andw	$-1024, %di
	cmpw	$-9216, %di
	jne	.L627
	sall	$10, %esi
	andl	$1023, %eax
	leal	2(%rdx), %ecx
	andl	$1047552, %esi
	orl	%eax, %esi
	addl	$65536, %esi
	jmp	.L627
.L632:
	movzwl	(%rsi,%rdi,2), %esi
	movl	%esi, %edi
	cmpl	%eax, %ecx
	jle	.L633
	andw	$-1024, %di
	cmpw	$-10240, %di
	je	.L631
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L663:
	movl	%eax, 64(%r15)
	movq	%r15, %rdi
	movl	$4, %r12d
	xorl	%ebx, %ebx
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	64(%r15), %r14d
	.p2align 4,,10
	.p2align 3
.L636:
	movl	56(%r15), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L634
	orl	$32, %eax
	leal	-49(%rax), %edx
	cmpl	$5, %edx
	ja	.L635
	subl	$39, %eax
	js	.L635
.L634:
	sall	$4, %ebx
	movq	%r15, %rdi
	addl	%eax, %ebx
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	subl	$1, %r12d
	jne	.L636
	movl	%ebx, %eax
	andl	$64512, %eax
	cmpl	$56320, %eax
	je	.L637
	movq	48(%r15), %rdx
	jmp	.L631
.L635:
	movq	48(%r15), %rdx
	leal	-1(%r14), %eax
	movq	%r15, %rdi
	movl	%eax, 64(%r15)
	cmpl	36(%rdx), %eax
	setl	76(%r15)
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movq	48(%r15), %rdx
	jmp	.L631
.L660:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L637:
	movq	-104(%rbp), %rsi
	andl	$1023, %ebx
	movl	(%rsi), %eax
	movl	%eax, -104(%rbp)
	sall	$10, %eax
	andl	$1047552, %eax
	orl	%eax, %ebx
	leal	65536(%rbx), %eax
	movl	%eax, (%rsi)
	movl	$1, %eax
	jmp	.L605
.L664:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18404:
	.size	_ZN2v88internal12RegExpParser18ParseUnicodeEscapeEPi, .-_ZN2v88internal12RegExpParser18ParseUnicodeEscapeEPi
	.section	.text._ZN2v88internal12RegExpParser29ParseUnlimitedLengthHexNumberEiPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser29ParseUnlimitedLengthHexNumberEiPi
	.type	_ZN2v88internal12RegExpParser29ParseUnlimitedLengthHexNumberEiPi, @function
_ZN2v88internal12RegExpParser29ParseUnlimitedLengthHexNumberEiPi:
.LFB18415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	56(%rdi), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L668
	orl	$32, %eax
	leal	-49(%rax), %edx
	cmpl	$5, %edx
	ja	.L671
	subl	$39, %eax
	js	.L671
.L668:
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L673:
	sall	$4, %r13d
	addl	%eax, %r13d
	cmpl	%r14d, %r13d
	jg	.L671
	movq	48(%r12), %rax
	movl	36(%rax), %ecx
	cmpl	%ecx, 64(%r12)
	jl	.L699
	movl	$2097152, 56(%r12)
	movl	36(%rax), %eax
	movb	$0, 76(%r12)
	addl	$1, %eax
	movl	%eax, 64(%r12)
.L672:
	movl	%r13d, (%rbx)
	movl	$1, %eax
.L667:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L700
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L671:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L699:
	movq	(%r12), %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r15), %rax
	jnb	.L675
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L701
	movl	$216, %edi
	call	_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE@PLT
	cmpb	$0, 81(%r12)
	movq	%rax, %r15
	je	.L677
.L696:
	movl	56(%r12), %eax
	subl	$48, %eax
.L678:
	cmpl	$9, %eax
	ja	.L702
	.p2align 4,,10
	.p2align 3
.L687:
	testl	%eax, %eax
	jns	.L673
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L702:
	orl	$32, %eax
	leal	-49(%rax), %edx
	cmpl	$5, %edx
	ja	.L672
	subl	$39, %eax
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L675:
	movq	8(%r12), %rax
	cmpq	$268435456, 8(%rax)
	jbe	.L681
	cmpb	$0, 81(%r12)
	jne	.L696
	leaq	.LC3(%rip), %rax
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-80(%rbp), %rsi
	movq	%rax, -80(%rbp)
	movq	$28, -72(%rbp)
.L698:
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r12), %rdx
	testq	%rax, %rax
	je	.L703
	movq	%rax, (%rdx)
	movq	48(%r12), %rax
	movl	$2097152, 56(%r12)
	movl	36(%rax), %eax
	movl	%eax, 64(%r12)
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L681:
	movq	48(%r12), %rdx
	movslq	64(%r12), %rax
	movzbl	32(%rdx), %edi
	movq	40(%rdx), %r8
	movq	%rax, %rcx
	testb	%dil, %dil
	je	.L684
	movzbl	(%r8,%rax), %eax
.L685:
	leal	1(%rcx), %esi
	testb	$16, 60(%r12)
	je	.L686
	cmpl	36(%rdx), %esi
	jge	.L686
	movl	%eax, %edx
	andl	$64512, %edx
	cmpl	$55296, %edx
	jne	.L686
	testb	%dil, %dil
	je	.L704
	.p2align 4,,10
	.p2align 3
.L686:
	movl	%eax, 56(%r12)
	subl	$48, %eax
	movl	%esi, 64(%r12)
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L677:
	movq	%rax, %rdi
	call	strlen@PLT
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-96(%rbp), %rsi
	cltq
	movq	%r15, -96(%rbp)
	movq	%rax, -88(%rbp)
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L684:
	movzwl	(%r8,%rax,2), %eax
	jmp	.L685
.L703:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L704:
	movslq	%esi, %rdx
	movzwl	(%r8,%rdx,2), %edx
	movl	%edx, %edi
	andw	$-1024, %di
	cmpw	$-9216, %di
	jne	.L686
	sall	$10, %eax
	andl	$1023, %edx
	leal	2(%rcx), %esi
	andl	$1047552, %eax
	orl	%edx, %eax
	addl	$65536, %eax
	jmp	.L686
.L701:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L700:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18415:
	.size	_ZN2v88internal12RegExpParser29ParseUnlimitedLengthHexNumberEiPi, .-_ZN2v88internal12RegExpParser29ParseUnlimitedLengthHexNumberEiPi
	.section	.rodata._ZN2v88internal12RegExpParser25ParseClassCharacterEscapeEv.str1.1,"aMS",@progbits,1
.LC6:
	.string	"Invalid class escape"
.LC7:
	.string	"Invalid escape"
.LC8:
	.string	"Invalid unicode escape"
	.section	.text._ZN2v88internal12RegExpParser25ParseClassCharacterEscapeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser25ParseClassCharacterEscapeEv
	.type	_ZN2v88internal12RegExpParser25ParseClassCharacterEscapeEv, @function
_ZN2v88internal12RegExpParser25ParseClassCharacterEscapeEv:
.LFB18416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	48(%rdi), %rax
	movl	36(%rax), %ecx
	cmpl	%ecx, 64(%rdi)
	jl	.L841
	movl	$2097152, 56(%rdi)
	movl	36(%rax), %eax
	movb	$0, 76(%rdi)
	addl	$1, %eax
	movl	%eax, 64(%rdi)
.L712:
	movl	$2097152, %r15d
	testb	$16, 60(%r12)
	jne	.L764
.L760:
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	.p2align 4,,10
	.p2align 3
.L705:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L842
	addq	$152, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L841:
	.cfi_restore_state
	movq	(%rdi), %rbx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%rbx), %rax
	jnb	.L707
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L843
	movl	$216, %edi
	call	_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE@PLT
	cmpb	$0, 81(%r12)
	movq	%rax, %rbx
	jne	.L829
	movq	%rax, %rdi
	call	strlen@PLT
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-176(%rbp), %rsi
	cltq
	movq	%rbx, -176(%rbp)
	movq	%rax, -168(%rbp)
.L832:
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r12), %rdx
	testq	%rax, %rax
	je	.L715
	movq	%rax, (%rdx)
	movq	48(%r12), %rax
	movl	$2097152, 56(%r12)
	movl	36(%rax), %eax
	movl	%eax, 64(%r12)
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L707:
	movq	8(%r12), %rax
	cmpq	$268435456, 8(%rax)
	jbe	.L713
	cmpb	$0, 81(%r12)
	je	.L714
.L829:
	movl	56(%r12), %r15d
.L710:
	leal	-48(%r15), %ecx
	cmpl	$72, %ecx
	ja	.L719
	leaq	.L721(%rip), %rsi
	movl	%ecx, %edx
	movslq	(%rsi,%rdx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal12RegExpParser25ParseClassCharacterEscapeEv,"a",@progbits
	.align 4
	.align 4
.L721:
	.long	.L731-.L721
	.long	.L730-.L721
	.long	.L730-.L721
	.long	.L730-.L721
	.long	.L730-.L721
	.long	.L730-.L721
	.long	.L730-.L721
	.long	.L730-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L729-.L721
	.long	.L728-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L727-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L726-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L719-.L721
	.long	.L725-.L721
	.long	.L719-.L721
	.long	.L724-.L721
	.long	.L723-.L721
	.long	.L722-.L721
	.long	.L719-.L721
	.long	.L720-.L721
	.section	.text._ZN2v88internal12RegExpParser25ParseClassCharacterEscapeEv
	.p2align 4,,10
	.p2align 3
.L730:
	testb	$16, 60(%r12)
	jne	.L750
.L743:
	movq	%r12, %rdi
	movl	%ecx, %r15d
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	56(%r12), %eax
	leal	-48(%rax), %edx
	cmpl	$7, %edx
	ja	.L705
	leal	-48(%rax,%r15,8), %r15d
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	cmpl	$31, %r15d
	jg	.L705
	movl	56(%r12), %eax
	leal	-48(%rax), %edx
	cmpl	$7, %edx
	ja	.L705
	movq	%r12, %rdi
	leal	-48(%rax,%r15,8), %r15d
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L731:
	testb	$16, 60(%r12)
	je	.L743
	movq	48(%r12), %rdx
	movl	64(%r12), %eax
	movl	36(%rdx), %ecx
	cmpl	%ecx, %eax
	jge	.L744
	movslq	%eax, %rdi
	addl	$1, %eax
	cmpb	$0, 32(%rdx)
	movq	40(%rdx), %rsi
	je	.L745
	cmpb	$47, (%rsi,%rdi)
	jbe	.L744
	movzbl	(%rsi,%rdi), %edx
.L749:
	cmpl	$57, %edx
	jg	.L744
.L750:
	cmpb	$0, 81(%r12)
	jne	.L831
	leaq	.LC6(%rip), %rax
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-128(%rbp), %rsi
	movq	%rax, -128(%rbp)
	movq	$20, -120(%rbp)
.L838:
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r12), %rdx
	testq	%rax, %rax
	je	.L715
	movq	%rax, (%rdx)
	movq	48(%r12), %rax
	movl	$2097152, 56(%r12)
	movl	36(%rax), %eax
	movl	%eax, 64(%r12)
.L831:
	xorl	%r15d, %r15d
	jmp	.L705
.L720:
	movq	%r12, %rdi
	movl	$2, %r13d
	xorl	%ebx, %ebx
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	64(%r12), %r14d
	jmp	.L755
	.p2align 4,,10
	.p2align 3
.L844:
	movl	$1, %r13d
.L755:
	movl	56(%r12), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L752
	orl	$32, %eax
	leal	-49(%rax), %edx
	cmpl	$5, %edx
	ja	.L753
	subl	$39, %eax
	js	.L753
.L752:
	sall	$4, %ebx
	movq	%r12, %rdi
	addl	%eax, %ebx
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	cmpl	$1, %r13d
	jne	.L844
	movl	%ebx, %r15d
	jmp	.L705
.L722:
	movq	%r12, %rdi
	movl	$11, %r15d
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	jmp	.L705
.L723:
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	leaq	-180(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser18ParseUnicodeEscapeEPi
	testb	%al, %al
	jne	.L845
	testb	$16, 60(%r12)
	je	.L705
	cmpb	$0, 81(%r12)
	jne	.L831
	leaq	.LC8(%rip), %rax
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-96(%rbp), %rsi
	movq	%rax, -96(%rbp)
	movq	$22, -88(%rbp)
	jmp	.L838
	.p2align 4,,10
	.p2align 3
.L724:
	movq	%r12, %rdi
	movl	$9, %r15d
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	jmp	.L705
.L725:
	movq	%r12, %rdi
	movl	$13, %r15d
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	jmp	.L705
.L726:
	movq	%r12, %rdi
	movl	$10, %r15d
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	jmp	.L705
.L727:
	movq	%r12, %rdi
	movl	$12, %r15d
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	jmp	.L705
.L728:
	movq	48(%r12), %rcx
	movl	60(%r12), %eax
	movl	64(%r12), %edx
	movl	36(%rcx), %esi
	andl	$16, %eax
	cmpl	%edx, %esi
	jle	.L733
	cmpb	$0, 32(%rcx)
	movq	40(%rcx), %rdi
	movslq	%edx, %r8
	jne	.L846
	movzwl	(%rdi,%r8,2), %ebx
	addl	$1, %edx
	testl	%eax, %eax
	setne	%al
	movl	%ebx, %r8d
	movl	%eax, %ecx
	cmpl	%edx, %esi
	jle	.L737
	testb	%al, %al
	jne	.L847
.L737:
	movl	%ebx, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpl	$25, %eax
	jbe	.L772
	testb	%cl, %cl
	jne	.L738
	leal	-48(%rbx), %eax
	cmpl	$9, %eax
	jbe	.L772
	movl	$92, %r15d
	cmpl	$95, %ebx
	jne	.L705
.L772:
	movl	%edx, 64(%r12)
	movq	%r12, %rdi
	andl	$31, %ebx
	movl	%ebx, %r15d
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	jmp	.L705
.L729:
	movq	%r12, %rdi
	movl	$8, %r15d
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L713:
	movq	48(%r12), %rax
	movslq	64(%r12), %rcx
	movzbl	32(%rax), %esi
	movq	40(%rax), %rdi
	movq	%rcx, %rdx
	testb	%sil, %sil
	jne	.L848
	movzwl	(%rdi,%rcx,2), %r15d
.L717:
	leal	1(%rdx), %ecx
	testb	$16, 60(%r12)
	je	.L718
	cmpl	36(%rax), %ecx
	jge	.L718
	movl	%r15d, %eax
	andl	$64512, %eax
	cmpl	$55296, %eax
	jne	.L718
	testb	%sil, %sil
	je	.L849
	.p2align 4,,10
	.p2align 3
.L718:
	movl	%ecx, 64(%r12)
	movl	%r15d, 56(%r12)
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L753:
	subl	$1, %r14d
	movq	48(%r12), %rax
	movq	%r12, %rdi
	movl	%r14d, 64(%r12)
	cmpl	36(%rax), %r14d
	setl	76(%r12)
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	testb	$16, 60(%r12)
	je	.L705
	cmpb	$0, 81(%r12)
	jne	.L831
	leaq	.LC7(%rip), %rax
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-112(%rbp), %rsi
	movq	%rax, -112(%rbp)
	movq	$14, -104(%rbp)
	jmp	.L838
	.p2align 4,,10
	.p2align 3
.L848:
	movzbl	(%rdi,%rcx), %r15d
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L714:
	leaq	.LC3(%rip), %rax
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-160(%rbp), %rsi
	movq	%rax, -160(%rbp)
	movq	$28, -152(%rbp)
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L846:
	addl	$1, %edx
	testl	%eax, %eax
	movzbl	(%rdi,%r8), %ebx
	setne	%cl
	cmpl	%edx, %esi
	jle	.L737
	testb	%cl, %cl
	je	.L737
.L735:
	movl	%ebx, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpl	$25, %eax
	jbe	.L772
.L738:
	cmpb	$0, 81(%r12)
	jne	.L831
	leaq	.LC6(%rip), %rax
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-144(%rbp), %rsi
	movq	%rax, -144(%rbp)
	movq	$20, -136(%rbp)
	jmp	.L838
.L719:
	testb	$16, 60(%r12)
	je	.L760
	cmpl	$94, %r15d
	jg	.L761
	cmpl	$35, %r15d
	jle	.L762
	leal	-36(%r15), %eax
	cmpl	$58, %eax
	ja	.L762
	leaq	.L763(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal12RegExpParser25ParseClassCharacterEscapeEv
	.align 4
	.align 4
.L763:
	.long	.L760-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L760-.L763
	.long	.L760-.L763
	.long	.L760-.L763
	.long	.L760-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L760-.L763
	.long	.L760-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L760-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L762-.L763
	.long	.L760-.L763
	.long	.L760-.L763
	.long	.L760-.L763
	.long	.L760-.L763
	.section	.text._ZN2v88internal12RegExpParser25ParseClassCharacterEscapeEv
.L762:
	cmpl	$45, %r15d
	je	.L760
	.p2align 4,,10
	.p2align 3
.L764:
	cmpb	$0, 81(%r12)
	jne	.L831
	leaq	.LC7(%rip), %rax
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-80(%rbp), %rsi
	movq	%rax, -80(%rbp)
	movq	$14, -72(%rbp)
	jmp	.L838
.L761:
	leal	-123(%r15), %eax
	cmpl	$2, %eax
	ja	.L764
	jmp	.L760
	.p2align 4,,10
	.p2align 3
.L845:
	movl	-180(%rbp), %r15d
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L745:
	movzwl	(%rsi,%rdi,2), %edx
	cmpl	%eax, %ecx
	jle	.L747
	movl	%edx, %r8d
	andw	$-1024, %r8w
	cmpw	$-10240, %r8w
	je	.L744
.L747:
	cmpw	$47, %dx
	jbe	.L744
	movzwl	(%rsi,%rdi,2), %edx
	movl	%edx, %esi
	cmpl	%eax, %ecx
	jle	.L749
	andw	$-1024, %si
	cmpw	$-10240, %si
	jne	.L749
	.p2align 4,,10
	.p2align 3
.L744:
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L733:
	testl	%eax, %eax
	jne	.L738
	movl	$92, %r15d
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L715:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L847:
	andw	$-1024, %r8w
	cmpw	$-10240, %r8w
	jne	.L735
	movslq	%edx, %rax
	movzwl	(%rdi,%rax,2), %eax
	movl	%eax, %ecx
	andw	$-1024, %cx
	cmpw	$-9216, %cx
	jne	.L738
	sall	$10, %ebx
	andl	$1023, %eax
	andl	$1047552, %ebx
	orl	%ebx, %eax
	leal	65536(%rax), %ebx
	jmp	.L735
.L849:
	movslq	%ecx, %rax
	movzwl	(%rdi,%rax,2), %eax
	movl	%eax, %esi
	andw	$-1024, %si
	cmpw	$-9216, %si
	jne	.L718
	sall	$10, %r15d
	andl	$1023, %eax
	leal	2(%rdx), %ecx
	andl	$1047552, %r15d
	orl	%eax, %r15d
	addl	$65536, %r15d
	jmp	.L718
.L842:
	call	__stack_chk_fail@PLT
.L843:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18416:
	.size	_ZN2v88internal12RegExpParser25ParseClassCharacterEscapeEv, .-_ZN2v88internal12RegExpParser25ParseClassCharacterEscapeEv
	.section	.text._ZN2v88internal13RegExpBuilderC2EPNS0_4ZoneENS_4base5FlagsINS0_8JSRegExp4FlagEiEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpBuilderC2EPNS0_4ZoneENS_4base5FlagsINS0_8JSRegExp4FlagEiEE
	.type	_ZN2v88internal13RegExpBuilderC2EPNS0_4ZoneENS_4base5FlagsINS0_8JSRegExp4FlagEiEE, @function
_ZN2v88internal13RegExpBuilderC2EPNS0_4ZoneENS_4base5FlagsINS0_8JSRegExp4FlagEiEE:
.LFB18424:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%rsi, (%rdi)
	movb	$0, 8(%rdi)
	movl	%edx, 12(%rdi)
	movq	$0, 16(%rdi)
	movw	%ax, 24(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	ret
	.cfi_endproc
.LFE18424:
	.size	_ZN2v88internal13RegExpBuilderC2EPNS0_4ZoneENS_4base5FlagsINS0_8JSRegExp4FlagEiEE, .-_ZN2v88internal13RegExpBuilderC2EPNS0_4ZoneENS_4base5FlagsINS0_8JSRegExp4FlagEiEE
	.globl	_ZN2v88internal13RegExpBuilderC1EPNS0_4ZoneENS_4base5FlagsINS0_8JSRegExp4FlagEiEE
	.set	_ZN2v88internal13RegExpBuilderC1EPNS0_4ZoneENS_4base5FlagsINS0_8JSRegExp4FlagEiEE,_ZN2v88internal13RegExpBuilderC2EPNS0_4ZoneENS_4base5FlagsINS0_8JSRegExp4FlagEiEE
	.section	.text._ZN2v88internal13RegExpBuilder8AddEmptyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpBuilder8AddEmptyEv
	.type	_ZN2v88internal13RegExpBuilder8AddEmptyEv, @function
_ZN2v88internal13RegExpBuilder8AddEmptyEv:
.LFB18434:
	.cfi_startproc
	endbr64
	movb	$1, 8(%rdi)
	ret
	.cfi_endproc
.LFE18434:
	.size	_ZN2v88internal13RegExpBuilder8AddEmptyEv, .-_ZN2v88internal13RegExpBuilder8AddEmptyEv
	.section	.text._ZN2v88internal13RegExpBuilder30AddCharacterClassForDesugaringEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpBuilder30AddCharacterClassForDesugaringEi
	.type	_ZN2v88internal13RegExpBuilder30AddCharacterClassForDesugaringEi, @function
_ZN2v88internal13RegExpBuilder30AddCharacterClassForDesugaringEi:
.LFB18436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r12
	movq	24(%r12), %rcx
	movq	16(%r12), %rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	cmpq	$15, %rax
	jbe	.L870
	leaq	16(%rdx), %rax
	subq	%rax, %rcx
	movq	%rax, 16(%r12)
	cmpq	$7, %rcx
	jbe	.L871
.L855:
	leaq	8(%rax), %rcx
	movq	%rcx, 16(%r12)
.L856:
	movabsq	$4294967297, %rsi
	movq	%rax, (%rdx)
	movq	%rsi, 8(%rdx)
	movl	%r13d, (%rax)
	movl	%r13d, 4(%rax)
	movq	(%rbx), %rdi
	movl	12(%rbx), %ecx
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$31, %rax
	jbe	.L872
	leaq	32(%r13), %rax
	movq	%rax, 16(%rdi)
.L858:
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal20RegExpCharacterClassC1EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	movq	%rbx, %rdi
	call	_ZN2v88internal13RegExpBuilder9FlushTextEv
	cmpq	$0, 40(%rbx)
	je	.L859
	movq	32(%rbx), %r12
	movq	(%rbx), %r14
	testq	%r12, %r12
	je	.L873
.L860:
	movslq	12(%r12), %rax
	movl	8(%r12), %edx
	cmpl	%edx, %eax
	jge	.L865
	leal	1(%rax), %ecx
	movq	(%r12), %rdx
	movl	%ecx, 12(%r12)
	movq	40(%rbx), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L859:
	movq	%r13, 40(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L865:
	.cfi_restore_state
	movq	16(%r14), %rdi
	movq	24(%r14), %rax
	leal	1(%rdx,%rdx), %ecx
	movslq	%ecx, %rsi
	movq	40(%rbx), %r15
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L874
	addq	%rdi, %rsi
	movq	%rsi, 16(%r14)
.L867:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jg	.L875
.L868:
	addl	$1, %eax
	movq	%rdi, (%r12)
	movl	%ecx, 8(%r12)
	movl	%eax, 12(%r12)
	movq	%r15, (%rdi,%rdx)
	jmp	.L859
	.p2align 4,,10
	.p2align 3
.L875:
	movq	(%r12), %rsi
	movl	%ecx, -56(%rbp)
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movl	-56(%rbp), %ecx
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$3, %rdx
	jmp	.L868
	.p2align 4,,10
	.p2align 3
.L873:
	movq	24(%r14), %rdx
	movq	16(%r14), %r15
	movq	%rdx, %rax
	subq	%r15, %rax
	cmpq	$15, %rax
	jbe	.L876
	leaq	16(%r15), %rax
	movq	%rax, 16(%r14)
.L862:
	subq	%rax, %rdx
	movq	%r15, %r12
	cmpq	$15, %rdx
	jbe	.L877
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
.L864:
	movq	%rax, (%r15)
	movq	$2, 8(%r15)
	movq	%r15, 32(%rbx)
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L870:
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r12), %rcx
	movq	%rax, %rdx
	movq	16(%r12), %rax
	subq	%rax, %rcx
	cmpq	$7, %rcx
	ja	.L855
	.p2align 4,,10
	.p2align 3
.L871:
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	jmp	.L856
	.p2align 4,,10
	.p2align 3
.L872:
	movl	$32, %esi
	movl	%ecx, -60(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movl	-60(%rbp), %ecx
	movq	%rax, %r13
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L874:
	movq	%r14, %rdi
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-56(%rbp), %ecx
	movq	%rax, %rdi
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L877:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L864
	.p2align 4,,10
	.p2align 3
.L876:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r14), %rdx
	movq	%rax, %r15
	movq	16(%r14), %rax
	jmp	.L862
	.cfi_endproc
.LFE18436:
	.size	_ZN2v88internal13RegExpBuilder30AddCharacterClassForDesugaringEi, .-_ZN2v88internal13RegExpBuilder30AddCharacterClassForDesugaringEi
	.section	.text._ZN2v88internal13RegExpBuilder21FlushPendingSurrogateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpBuilder21FlushPendingSurrogateEv
	.type	_ZN2v88internal13RegExpBuilder21FlushPendingSurrogateEv, @function
_ZN2v88internal13RegExpBuilder21FlushPendingSurrogateEv:
.LFB18428:
	.cfi_startproc
	endbr64
	movzwl	24(%rdi), %esi
	testw	%si, %si
	jne	.L880
	ret
	.p2align 4,,10
	.p2align 3
.L880:
	xorl	%eax, %eax
	movw	%ax, 24(%rdi)
	jmp	_ZN2v88internal13RegExpBuilder30AddCharacterClassForDesugaringEi
	.cfi_endproc
.LFE18428:
	.size	_ZN2v88internal13RegExpBuilder21FlushPendingSurrogateEv, .-_ZN2v88internal13RegExpBuilder21FlushPendingSurrogateEv
	.section	.text._ZN2v88internal13RegExpBuilder16AddLeadSurrogateEt,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpBuilder16AddLeadSurrogateEt
	.type	_ZN2v88internal13RegExpBuilder16AddLeadSurrogateEt, @function
_ZN2v88internal13RegExpBuilder16AddLeadSurrogateEt:
.LFB18426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	movzwl	24(%rdi), %esi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testw	%si, %si
	jne	.L887
	movw	%r12w, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L887:
	.cfi_restore_state
	xorl	%eax, %eax
	movw	%ax, 24(%rdi)
	call	_ZN2v88internal13RegExpBuilder30AddCharacterClassForDesugaringEi
	movw	%r12w, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18426:
	.size	_ZN2v88internal13RegExpBuilder16AddLeadSurrogateEt, .-_ZN2v88internal13RegExpBuilder16AddLeadSurrogateEt
	.section	.text._ZN2v88internal13RegExpBuilder15FlushCharactersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpBuilder15FlushCharactersEv
	.type	_ZN2v88internal13RegExpBuilder15FlushCharactersEv, @function
_ZN2v88internal13RegExpBuilder15FlushCharactersEv:
.LFB18429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movzwl	24(%rdi), %esi
	testw	%si, %si
	jne	.L910
.L889:
	movq	16(%rbx), %rax
	movb	$0, 8(%rbx)
	testq	%rax, %rax
	je	.L888
	movq	(%rbx), %rdi
	movq	(%rax), %r15
	movslq	12(%rax), %r14
	movl	12(%rbx), %r13d
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$31, %rax
	jbe	.L911
	leaq	32(%r12), %rax
	movq	%rax, 16(%rdi)
.L892:
	leaq	16+_ZTVN2v88internal10RegExpAtomE(%rip), %rax
	movq	%r15, 8(%r12)
	movq	%rax, (%r12)
	movq	%r14, 16(%r12)
	movl	%r13d, 24(%r12)
	cmpq	$0, 56(%rbx)
	movq	$0, 16(%rbx)
	je	.L893
	movq	48(%rbx), %r13
	movq	(%rbx), %r14
	testq	%r13, %r13
	je	.L912
.L894:
	movslq	12(%r13), %rax
	movl	8(%r13), %edx
	cmpl	%edx, %eax
	jge	.L899
	leal	1(%rax), %ecx
	movq	0(%r13), %rdx
	movl	%ecx, 12(%r13)
	movq	56(%rbx), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L893:
	movq	%r12, 56(%rbx)
.L888:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L910:
	.cfi_restore_state
	xorl	%eax, %eax
	movw	%ax, 24(%rdi)
	call	_ZN2v88internal13RegExpBuilder30AddCharacterClassForDesugaringEi
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L911:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L899:
	movq	16(%r14), %rdi
	movq	24(%r14), %rax
	leal	1(%rdx,%rdx), %ecx
	movslq	%ecx, %rsi
	movq	56(%rbx), %r15
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L913
	addq	%rdi, %rsi
	movq	%rsi, 16(%r14)
.L901:
	movslq	12(%r13), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L902
	movq	0(%r13), %rsi
	movl	%ecx, -52(%rbp)
	call	memcpy@PLT
	movslq	12(%r13), %rdx
	movl	-52(%rbp), %ecx
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$3, %rdx
.L902:
	addl	$1, %eax
	movq	%rdi, 0(%r13)
	movl	%ecx, 8(%r13)
	movl	%eax, 12(%r13)
	movq	%r15, (%rdi,%rdx)
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L912:
	movq	24(%r14), %rax
	movq	16(%r14), %r15
	movq	%rax, %rdx
	subq	%r15, %rdx
	cmpq	$15, %rdx
	jbe	.L914
	leaq	16(%r15), %rdx
	movq	%rdx, 16(%r14)
.L896:
	subq	%rdx, %rax
	movq	%r15, %r13
	cmpq	$15, %rax
	jbe	.L915
	leaq	16(%rdx), %rax
	movq	%rax, 16(%r14)
.L898:
	movq	%rdx, (%r15)
	movq	$2, 8(%r15)
	movq	%r15, 48(%rbx)
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L913:
	movq	%r14, %rdi
	movl	%ecx, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %ecx
	movq	%rax, %rdi
	jmp	.L901
.L915:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L898
.L914:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	16(%r14), %rdx
	movq	%rax, %r15
	movq	24(%r14), %rax
	jmp	.L896
	.cfi_endproc
.LFE18429:
	.size	_ZN2v88internal13RegExpBuilder15FlushCharactersEv, .-_ZN2v88internal13RegExpBuilder15FlushCharactersEv
	.section	.text._ZN2v88internal13RegExpBuilder9FlushTextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpBuilder9FlushTextEv
	.type	_ZN2v88internal13RegExpBuilder9FlushTextEv, @function
_ZN2v88internal13RegExpBuilder9FlushTextEv:
.LFB18430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movzwl	24(%rdi), %esi
	testw	%si, %si
	jne	.L977
.L917:
	movq	16(%rbx), %rax
	movb	$0, 8(%rbx)
	testq	%rax, %rax
	je	.L978
	movq	(%rbx), %rdi
	movq	(%rax), %rdx
	movslq	12(%rax), %r15
	movl	12(%rbx), %r13d
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$31, %rax
	jbe	.L979
	leaq	32(%r12), %rax
	movq	%rax, 16(%rdi)
.L922:
	leaq	16+_ZTVN2v88internal10RegExpAtomE(%rip), %rax
	movq	%rdx, 8(%r12)
	movq	%r12, %r14
	movq	%rax, (%r12)
	movq	%r15, 16(%r12)
	movl	%r13d, 24(%r12)
	cmpq	$0, 56(%rbx)
	movq	$0, 16(%rbx)
	je	.L923
	movq	48(%rbx), %r13
	movq	(%rbx), %r15
	testq	%r13, %r13
	je	.L980
.L924:
	movslq	12(%r13), %rax
	movl	8(%r13), %edx
	cmpl	%edx, %eax
	jge	.L929
	leal	1(%rax), %ecx
	movq	0(%r13), %rdx
	movl	%ecx, 12(%r13)
	movq	56(%rbx), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L923:
	movq	48(%rbx), %rax
	movq	%r12, 56(%rbx)
	testq	%rax, %rax
	je	.L933
.L919:
	movl	12(%rax), %r15d
.L920:
	xorl	%eax, %eax
	testq	%r14, %r14
	setne	%al
	addl	%eax, %r15d
	je	.L916
	movq	(%rbx), %r12
	cmpl	$1, %r15d
	je	.L968
	movq	16(%r12), %r14
	movq	24(%r12), %rax
	subq	%r14, %rax
	cmpq	$31, %rax
	jbe	.L981
	leaq	32(%r14), %rax
	movq	%rax, 16(%r12)
.L948:
	leaq	16+_ZTVN2v88internal10RegExpTextE(%rip), %rax
	movq	%rax, (%r14)
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L982
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%r12)
.L950:
	movq	%rax, 8(%r14)
	leal	-1(%r15), %r13d
	xorl	%r12d, %r12d
	movq	$2, 16(%r14)
	movl	$0, 24(%r14)
	testl	%r15d, %r15d
	jg	.L958
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L954:
	cmpl	%r12d, 12(%rax)
	je	.L956
	movq	(%rax), %rax
	movq	(%rax,%r12,8), %rdi
.L955:
	movq	(%rdi), %rax
	movq	(%rbx), %rdx
	movq	%r14, %rsi
	call	*80(%rax)
	leaq	1(%r12), %rax
	cmpq	%r13, %r12
	je	.L957
	movq	%rax, %r12
.L958:
	movq	48(%rbx), %rax
	testq	%rax, %rax
	jne	.L954
.L956:
	movq	56(%rbx), %rdi
	jmp	.L955
	.p2align 4,,10
	.p2align 3
.L957:
	cmpq	$0, 40(%rbx)
	je	.L953
	movq	32(%rbx), %r12
	movq	(%rbx), %r13
	testq	%r12, %r12
	je	.L983
.L959:
	movslq	12(%r12), %rax
	movl	8(%r12), %edx
	cmpl	%edx, %eax
	jge	.L964
	leal	1(%rax), %ecx
	movq	(%r12), %rdx
	movl	%ecx, 12(%r12)
	movq	40(%rbx), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L953:
	movq	%r14, 40(%rbx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
.L916:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L978:
	.cfi_restore_state
	movq	48(%rbx), %rax
	movq	56(%rbx), %r14
	xorl	%r15d, %r15d
	testq	%rax, %rax
	jne	.L919
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L977:
	xorl	%eax, %eax
	movw	%ax, 24(%rdi)
	call	_ZN2v88internal13RegExpBuilder30AddCharacterClassForDesugaringEi
	jmp	.L917
	.p2align 4,,10
	.p2align 3
.L933:
	movq	(%rbx), %r12
.L968:
	cmpq	$0, 40(%rbx)
	je	.L953
	movq	32(%rbx), %r13
	testq	%r13, %r13
	je	.L984
.L937:
	movslq	12(%r13), %rax
	movl	8(%r13), %edx
	cmpl	%edx, %eax
	jge	.L942
	leal	1(%rax), %ecx
	movq	0(%r13), %rdx
	movl	%ecx, 12(%r13)
	movq	40(%rbx), %rcx
	movq	%rcx, (%rdx,%rax,8)
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L979:
	movl	$32, %esi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L929:
	movq	16(%r15), %rdi
	movq	24(%r15), %rax
	leal	1(%rdx,%rdx), %r8d
	movslq	%r8d, %rsi
	movq	56(%rbx), %rcx
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L985
	addq	%rdi, %rsi
	movq	%rsi, 16(%r15)
.L931:
	movslq	12(%r13), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L932
	movq	0(%r13), %rsi
	movl	%r8d, -60(%rbp)
	movq	%rcx, -56(%rbp)
	call	memcpy@PLT
	movslq	12(%r13), %rdx
	movl	-60(%rbp), %r8d
	movq	-56(%rbp), %rcx
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$3, %rdx
.L932:
	addl	$1, %eax
	movq	%rdi, 0(%r13)
	movl	%r8d, 8(%r13)
	movl	%eax, 12(%r13)
	movq	%rcx, (%rdi,%rdx)
	jmp	.L923
	.p2align 4,,10
	.p2align 3
.L964:
	movq	16(%r13), %rdi
	movq	24(%r13), %rax
	leal	1(%rdx,%rdx), %ecx
	movslq	%ecx, %rsi
	movq	40(%rbx), %r15
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L986
	addq	%rdi, %rsi
	movq	%rsi, 16(%r13)
.L966:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L967
	movq	(%r12), %rsi
	movl	%ecx, -56(%rbp)
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movl	-56(%rbp), %ecx
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$3, %rdx
.L967:
	addl	$1, %eax
	movq	%rdi, (%r12)
	movl	%ecx, 8(%r12)
	movl	%eax, 12(%r12)
	movq	%r15, (%rdi,%rdx)
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L980:
	movq	24(%r15), %rax
	movq	16(%r15), %rdx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	cmpq	$15, %rcx
	jbe	.L987
	leaq	16(%rdx), %rcx
	movq	%rcx, 16(%r15)
.L926:
	subq	%rcx, %rax
	movq	%rdx, %r13
	cmpq	$15, %rax
	jbe	.L988
	leaq	16(%rcx), %rax
	movq	%rax, 16(%r15)
.L928:
	movq	%rcx, (%rdx)
	movq	$2, 8(%rdx)
	movq	%rdx, 48(%rbx)
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L983:
	movq	24(%r13), %rax
	movq	16(%r13), %r15
	movq	%rax, %rdx
	subq	%r15, %rdx
	cmpq	$15, %rdx
	jbe	.L989
	leaq	16(%r15), %rdx
	movq	%rdx, 16(%r13)
.L961:
	subq	%rdx, %rax
	movq	%r15, %r12
	cmpq	$15, %rax
	jbe	.L990
	leaq	16(%rdx), %rax
	movq	%rax, 16(%r13)
.L963:
	movq	%rdx, (%r15)
	movq	$2, 8(%r15)
	movq	%r15, 32(%rbx)
	jmp	.L959
	.p2align 4,,10
	.p2align 3
.L942:
	movq	16(%r12), %rdi
	movq	24(%r12), %rax
	leal	1(%rdx,%rdx), %ecx
	movslq	%ecx, %rsi
	movq	40(%rbx), %r15
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L991
	addq	%rdi, %rsi
	movq	%rsi, 16(%r12)
.L944:
	movslq	12(%r13), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L945
	movq	0(%r13), %rsi
	movl	%ecx, -56(%rbp)
	call	memcpy@PLT
	movslq	12(%r13), %rdx
	movl	-56(%rbp), %ecx
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$3, %rdx
.L945:
	addl	$1, %eax
	movq	%rdi, 0(%r13)
	movl	%ecx, 8(%r13)
	movl	%eax, 12(%r13)
	movq	%r15, (%rdi,%rdx)
	jmp	.L953
.L989:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	16(%r13), %rdx
	movq	%rax, %r15
	movq	24(%r13), %rax
	jmp	.L961
.L991:
	movq	%r12, %rdi
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-56(%rbp), %ecx
	movq	%rax, %rdi
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L982:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L981:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L948
.L984:
	movq	24(%r12), %rax
	movq	16(%r12), %r15
	movq	%rax, %rdx
	subq	%r15, %rdx
	cmpq	$15, %rdx
	jbe	.L992
	leaq	16(%r15), %rdx
	movq	%rdx, 16(%r12)
.L939:
	subq	%rdx, %rax
	movq	%r15, %r13
	cmpq	$15, %rax
	jbe	.L993
	leaq	16(%rdx), %rax
	movq	%rax, 16(%r12)
.L941:
	movq	%rdx, (%r15)
	movq	$2, 8(%r15)
	movq	%r15, 32(%rbx)
	jmp	.L937
.L985:
	movq	%r15, %rdi
	movl	%r8d, -60(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movl	-60(%rbp), %r8d
	movq	%rax, %rdi
	jmp	.L931
.L986:
	movq	%r13, %rdi
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-56(%rbp), %ecx
	movq	%rax, %rdi
	jmp	.L966
.L987:
	movl	$16, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	16(%r15), %rcx
	movq	%rax, %rdx
	movq	24(%r15), %rax
	jmp	.L926
.L988:
	movl	$16, %esi
	movq	%r15, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L928
.L990:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L963
.L993:
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L941
.L992:
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	16(%r12), %rdx
	movq	%rax, %r15
	movq	24(%r12), %rax
	jmp	.L939
	.cfi_endproc
.LFE18430:
	.size	_ZN2v88internal13RegExpBuilder9FlushTextEv, .-_ZN2v88internal13RegExpBuilder9FlushTextEv
	.section	.text._ZN2v88internal13RegExpBuilder7AddAtomEPNS0_10RegExpTreeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpBuilder7AddAtomEPNS0_10RegExpTreeE
	.type	_ZN2v88internal13RegExpBuilder7AddAtomEPNS0_10RegExpTreeE, @function
_ZN2v88internal13RegExpBuilder7AddAtomEPNS0_10RegExpTreeE:
.LFB18437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$24, %rsp
	movq	(%rsi), %rax
	call	*256(%rax)
	testb	%al, %al
	jne	.L1037
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*32(%rax)
	testb	%al, %al
	je	.L997
	movzwl	24(%rbx), %esi
	testw	%si, %si
	jne	.L1038
.L998:
	movq	16(%rbx), %rax
	movq	(%rbx), %r13
	movb	$0, 8(%rbx)
	testq	%rax, %rax
	je	.L999
	movq	(%rax), %rcx
	movslq	12(%rax), %rdx
	movq	16(%r13), %r14
	movq	24(%r13), %rax
	movl	12(%rbx), %r15d
	subq	%r14, %rax
	cmpq	$31, %rax
	jbe	.L1039
	leaq	32(%r14), %rax
	movq	%rax, 16(%r13)
.L1001:
	leaq	16+_ZTVN2v88internal10RegExpAtomE(%rip), %rax
	movq	%rcx, 8(%r14)
	movq	%rax, (%r14)
	movq	%rdx, 16(%r14)
	movl	%r15d, 24(%r14)
	cmpq	$0, 56(%rbx)
	movq	$0, 16(%rbx)
	movq	(%rbx), %r13
	je	.L1002
	movq	48(%rbx), %r15
	testq	%r15, %r15
	je	.L1040
.L1003:
	movslq	12(%r15), %rax
	movl	8(%r15), %edx
	cmpl	%edx, %eax
	jge	.L1008
	leal	1(%rax), %ecx
	movq	(%r15), %rdx
	movl	%ecx, 12(%r15)
	movq	56(%rbx), %rcx
	movq	%rcx, (%rdx,%rax,8)
	movq	(%rbx), %r13
.L1002:
	movq	%r14, 56(%rbx)
.L1012:
	movq	48(%rbx), %r14
	testq	%r14, %r14
	je	.L1041
.L1014:
	movslq	12(%r14), %rax
	movl	8(%r14), %edx
	cmpl	%edx, %eax
	jge	.L1019
	leal	1(%rax), %ecx
	movq	(%r14), %rdx
	movl	%ecx, 12(%r14)
	movq	56(%rbx), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L1013:
	movq	%r12, 56(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L997:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal13RegExpBuilder9FlushTextEv
	cmpq	$0, 40(%rbx)
	je	.L1023
	movq	32(%rbx), %r13
	movq	(%rbx), %r14
	testq	%r13, %r13
	je	.L1042
.L1024:
	movslq	12(%r13), %rax
	movl	8(%r13), %edx
	cmpl	%edx, %eax
	jge	.L1029
	leal	1(%rax), %ecx
	movq	0(%r13), %rdx
	movl	%ecx, 12(%r13)
	movq	40(%rbx), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L1023:
	movq	%r12, 40(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1037:
	.cfi_restore_state
	movb	$1, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L999:
	.cfi_restore_state
	cmpq	$0, 56(%rbx)
	je	.L1013
	jmp	.L1012
	.p2align 4,,10
	.p2align 3
.L1029:
	movq	16(%r14), %rdi
	movq	24(%r14), %rax
	leal	1(%rdx,%rdx), %ecx
	movslq	%ecx, %rsi
	movq	40(%rbx), %r15
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1043
	addq	%rdi, %rsi
	movq	%rsi, 16(%r14)
.L1031:
	movslq	12(%r13), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jg	.L1044
.L1032:
	addl	$1, %eax
	movq	%rdi, 0(%r13)
	movl	%ecx, 8(%r13)
	movl	%eax, 12(%r13)
	movq	%r15, (%rdi,%rdx)
	jmp	.L1023
	.p2align 4,,10
	.p2align 3
.L1038:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movw	%ax, 24(%rbx)
	call	_ZN2v88internal13RegExpBuilder30AddCharacterClassForDesugaringEi
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1019:
	movq	16(%r13), %rdi
	movq	24(%r13), %rax
	leal	1(%rdx,%rdx), %ecx
	movslq	%ecx, %rsi
	movq	56(%rbx), %r15
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1045
	addq	%rdi, %rsi
	movq	%rsi, 16(%r13)
.L1021:
	movslq	12(%r14), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jg	.L1046
.L1022:
	addl	$1, %eax
	movq	%rdi, (%r14)
	movl	%ecx, 8(%r14)
	movl	%eax, 12(%r14)
	movq	%r15, (%rdi,%rdx)
	jmp	.L1013
	.p2align 4,,10
	.p2align 3
.L1042:
	movq	24(%r14), %rax
	movq	16(%r14), %r15
	movq	%rax, %rdx
	subq	%r15, %rdx
	cmpq	$15, %rdx
	jbe	.L1047
	leaq	16(%r15), %rdx
	movq	%rdx, 16(%r14)
.L1026:
	subq	%rdx, %rax
	movq	%r15, %r13
	cmpq	$15, %rax
	jbe	.L1048
	leaq	16(%rdx), %rax
	movq	%rax, 16(%r14)
.L1028:
	movq	%rdx, (%r15)
	movq	$2, 8(%r15)
	movq	%r15, 32(%rbx)
	jmp	.L1024
	.p2align 4,,10
	.p2align 3
.L1044:
	movq	0(%r13), %rsi
	movl	%ecx, -56(%rbp)
	call	memcpy@PLT
	movslq	12(%r13), %rdx
	movl	-56(%rbp), %ecx
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$3, %rdx
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1046:
	movq	(%r14), %rsi
	movl	%ecx, -56(%rbp)
	call	memcpy@PLT
	movslq	12(%r14), %rdx
	movl	-56(%rbp), %ecx
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$3, %rdx
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1008:
	movq	16(%r13), %rdi
	movq	24(%r13), %rax
	leal	1(%rdx,%rdx), %r8d
	movslq	%r8d, %rsi
	movq	56(%rbx), %rcx
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1049
	addq	%rdi, %rsi
	movq	%rsi, 16(%r13)
.L1010:
	movslq	12(%r15), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L1011
	movq	(%r15), %rsi
	movl	%r8d, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	memcpy@PLT
	movslq	12(%r15), %rdx
	movl	-64(%rbp), %r8d
	movq	-56(%rbp), %rcx
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$3, %rdx
.L1011:
	addl	$1, %eax
	movq	%rdi, (%r15)
	movl	%r8d, 8(%r15)
	movl	%eax, 12(%r15)
	movq	%rcx, (%rdi,%rdx)
	movq	(%rbx), %r13
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L1041:
	movq	24(%r13), %rax
	movq	16(%r13), %r15
	movq	%rax, %rdx
	subq	%r15, %rdx
	cmpq	$15, %rdx
	jbe	.L1050
	leaq	16(%r15), %rdx
	movq	%rdx, 16(%r13)
.L1016:
	subq	%rdx, %rax
	movq	%r15, %r14
	cmpq	$15, %rax
	jbe	.L1051
	leaq	16(%rdx), %rax
	movq	%rax, 16(%r13)
.L1018:
	movq	%rdx, (%r15)
	movq	$2, 8(%r15)
	movq	%r15, 48(%rbx)
	jmp	.L1014
	.p2align 4,,10
	.p2align 3
.L1040:
	movq	24(%r13), %rax
	movq	16(%r13), %rdx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	cmpq	$15, %rcx
	jbe	.L1052
	leaq	16(%rdx), %rcx
	movq	%rcx, 16(%r13)
.L1005:
	subq	%rcx, %rax
	movq	%rdx, %r15
	cmpq	$15, %rax
	jbe	.L1053
	leaq	16(%rcx), %rax
	movq	%rax, 16(%r13)
.L1007:
	movq	%rcx, (%rdx)
	movq	$2, 8(%rdx)
	movq	%rdx, 48(%rbx)
	jmp	.L1003
	.p2align 4,,10
	.p2align 3
.L1039:
	movl	$32, %esi
	movq	%r13, %rdi
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L1001
.L1043:
	movq	%r14, %rdi
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-56(%rbp), %ecx
	movq	%rax, %rdi
	jmp	.L1031
.L1045:
	movq	%r13, %rdi
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-56(%rbp), %ecx
	movq	%rax, %rdi
	jmp	.L1021
.L1049:
	movq	%r13, %rdi
	movl	%r8d, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movl	-64(%rbp), %r8d
	movq	%rax, %rdi
	jmp	.L1010
.L1051:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L1018
.L1050:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	16(%r13), %rdx
	movq	%rax, %r15
	movq	24(%r13), %rax
	jmp	.L1016
.L1048:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L1028
.L1047:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	16(%r14), %rdx
	movq	%rax, %r15
	movq	24(%r14), %rax
	jmp	.L1026
.L1053:
	movl	$16, %esi
	movq	%r13, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L1007
.L1052:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	16(%r13), %rcx
	movq	%rax, %rdx
	movq	24(%r13), %rax
	jmp	.L1005
	.cfi_endproc
.LFE18437:
	.size	_ZN2v88internal13RegExpBuilder7AddAtomEPNS0_10RegExpTreeE, .-_ZN2v88internal13RegExpBuilder7AddAtomEPNS0_10RegExpTreeE
	.section	.text._ZN2v88internal13RegExpBuilder17AddCharacterClassEPNS0_20RegExpCharacterClassE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpBuilder17AddCharacterClassEPNS0_20RegExpCharacterClassE
	.type	_ZN2v88internal13RegExpBuilder17AddCharacterClassEPNS0_20RegExpCharacterClassE, @function
_ZN2v88internal13RegExpBuilder17AddCharacterClassEPNS0_20RegExpCharacterClassE:
.LFB18435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	12(%rdi), %eax
	testb	$16, %al
	je	.L1055
	testb	$2, %al
	je	.L1080
.L1056:
	movq	%r12, %rdi
	call	_ZN2v88internal13RegExpBuilder9FlushTextEv
	cmpq	$0, 40(%r12)
	je	.L1059
.L1083:
	movq	32(%r12), %rbx
	movq	(%r12), %r14
	testq	%rbx, %rbx
	je	.L1081
.L1060:
	movslq	12(%rbx), %rax
	movl	8(%rbx), %edx
	cmpl	%edx, %eax
	jge	.L1065
	leal	1(%rax), %ecx
	movq	(%rbx), %rdx
	movl	%ecx, 12(%rbx)
	movq	40(%r12), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L1059:
	movq	%r13, 40(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1055:
	.cfi_restore_state
	addq	$24, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13RegExpBuilder7AddAtomEPNS0_10RegExpTreeE
	.p2align 4,,10
	.p2align 3
.L1080:
	.cfi_restore_state
	leaq	8(%rsi), %rdi
	movq	(%r12), %rsi
	call	_ZN2v88internal12CharacterSet6rangesEPNS0_4ZoneE@PLT
	movq	%rax, %rbx
	movq	%rax, %rdi
	call	_ZN2v88internal14CharacterRange12CanonicalizeEPNS0_8ZoneListIS1_EE@PLT
	movl	12(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L1055
	movq	(%rbx), %rsi
	movslq	%ecx, %rdx
	salq	$3, %rdx
	leaq	(%rsi,%rdx), %rax
	leaq	-8(%rsi,%rdx), %rsi
	leal	-1(%rcx), %edx
	salq	$3, %rdx
	subq	%rdx, %rsi
	jmp	.L1058
	.p2align 4,,10
	.p2align 3
.L1082:
	cmpl	$57343, %ecx
	jg	.L1070
	cmpl	$55295, %edx
	jg	.L1056
.L1070:
	subq	$8, %rax
	cmpq	%rsi, %rax
	je	.L1055
.L1058:
	movl	-4(%rax), %edx
	movl	-8(%rax), %ecx
	cmpl	$65535, %edx
	jle	.L1082
	movq	%r12, %rdi
	call	_ZN2v88internal13RegExpBuilder9FlushTextEv
	cmpq	$0, 40(%r12)
	jne	.L1083
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1065:
	movq	16(%r14), %rdi
	movq	24(%r14), %rax
	leal	1(%rdx,%rdx), %ecx
	movslq	%ecx, %rsi
	movq	40(%r12), %r15
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1084
	addq	%rdi, %rsi
	movq	%rsi, 16(%r14)
.L1067:
	movslq	12(%rbx), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L1068
	movq	(%rbx), %rsi
	movl	%ecx, -52(%rbp)
	call	memcpy@PLT
	movslq	12(%rbx), %rdx
	movl	-52(%rbp), %ecx
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$3, %rdx
.L1068:
	addl	$1, %eax
	movq	%rdi, (%rbx)
	movl	%ecx, 8(%rbx)
	movl	%eax, 12(%rbx)
	movq	%r15, (%rdi,%rdx)
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1081:
	movq	24(%r14), %rax
	movq	16(%r14), %r15
	movq	%rax, %rdx
	subq	%r15, %rdx
	cmpq	$15, %rdx
	jbe	.L1085
	leaq	16(%r15), %rdx
	movq	%rdx, 16(%r14)
.L1062:
	subq	%rdx, %rax
	movq	%r15, %rbx
	cmpq	$15, %rax
	jbe	.L1086
	leaq	16(%rdx), %rax
	movq	%rax, 16(%r14)
.L1064:
	movq	%rdx, (%r15)
	movq	$2, 8(%r15)
	movq	%r15, 32(%r12)
	jmp	.L1060
	.p2align 4,,10
	.p2align 3
.L1084:
	movq	%r14, %rdi
	movl	%ecx, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %ecx
	movq	%rax, %rdi
	jmp	.L1067
.L1086:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L1064
.L1085:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	16(%r14), %rdx
	movq	%rax, %r15
	movq	24(%r14), %rax
	jmp	.L1062
	.cfi_endproc
.LFE18435:
	.size	_ZN2v88internal13RegExpBuilder17AddCharacterClassEPNS0_20RegExpCharacterClassE, .-_ZN2v88internal13RegExpBuilder17AddCharacterClassEPNS0_20RegExpCharacterClassE
	.section	.text._ZN2v88internal13RegExpBuilder7AddTermEPNS0_10RegExpTreeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpBuilder7AddTermEPNS0_10RegExpTreeE
	.type	_ZN2v88internal13RegExpBuilder7AddTermEPNS0_10RegExpTreeE, @function
_ZN2v88internal13RegExpBuilder7AddTermEPNS0_10RegExpTreeE:
.LFB18438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	_ZN2v88internal13RegExpBuilder9FlushTextEv
	cmpq	$0, 40(%rbx)
	je	.L1088
	movq	32(%rbx), %r12
	movq	(%rbx), %r14
	testq	%r12, %r12
	je	.L1099
.L1089:
	movslq	12(%r12), %rax
	movl	8(%r12), %edx
	cmpl	%edx, %eax
	jge	.L1094
	leal	1(%rax), %ecx
	movq	(%r12), %rdx
	movl	%ecx, 12(%r12)
	movq	40(%rbx), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L1088:
	movq	%r13, 40(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1094:
	.cfi_restore_state
	movq	16(%r14), %rdi
	movq	24(%r14), %rax
	leal	1(%rdx,%rdx), %ecx
	movslq	%ecx, %rsi
	movq	40(%rbx), %r15
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1100
	addq	%rdi, %rsi
	movq	%rsi, 16(%r14)
.L1096:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jg	.L1101
.L1097:
	addl	$1, %eax
	movq	%rdi, (%r12)
	movl	%ecx, 8(%r12)
	movl	%eax, 12(%r12)
	movq	%r15, (%rdi,%rdx)
	jmp	.L1088
	.p2align 4,,10
	.p2align 3
.L1101:
	movq	(%r12), %rsi
	movl	%ecx, -52(%rbp)
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movl	-52(%rbp), %ecx
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$3, %rdx
	jmp	.L1097
	.p2align 4,,10
	.p2align 3
.L1099:
	movq	24(%r14), %rdx
	movq	16(%r14), %r15
	movq	%rdx, %rax
	subq	%r15, %rax
	cmpq	$15, %rax
	jbe	.L1102
	leaq	16(%r15), %rax
	movq	%rax, 16(%r14)
.L1091:
	subq	%rax, %rdx
	movq	%r15, %r12
	cmpq	$15, %rdx
	jbe	.L1103
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
.L1093:
	movq	%rax, (%r15)
	movq	$2, 8(%r15)
	movq	%r15, 32(%rbx)
	jmp	.L1089
	.p2align 4,,10
	.p2align 3
.L1100:
	movq	%r14, %rdi
	movl	%ecx, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %ecx
	movq	%rax, %rdi
	jmp	.L1096
	.p2align 4,,10
	.p2align 3
.L1103:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1102:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r14), %rdx
	movq	%rax, %r15
	movq	16(%r14), %rax
	jmp	.L1091
	.cfi_endproc
.LFE18438:
	.size	_ZN2v88internal13RegExpBuilder7AddTermEPNS0_10RegExpTreeE, .-_ZN2v88internal13RegExpBuilder7AddTermEPNS0_10RegExpTreeE
	.section	.text._ZN2v88internal13RegExpBuilder12AddAssertionEPNS0_10RegExpTreeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpBuilder12AddAssertionEPNS0_10RegExpTreeE
	.type	_ZN2v88internal13RegExpBuilder12AddAssertionEPNS0_10RegExpTreeE, @function
_ZN2v88internal13RegExpBuilder12AddAssertionEPNS0_10RegExpTreeE:
.LFB22945:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	_ZN2v88internal13RegExpBuilder9FlushTextEv
	cmpq	$0, 40(%rbx)
	je	.L1105
	movq	32(%rbx), %r12
	movq	(%rbx), %r14
	testq	%r12, %r12
	je	.L1116
.L1106:
	movslq	12(%r12), %rax
	movl	8(%r12), %edx
	cmpl	%edx, %eax
	jge	.L1111
	leal	1(%rax), %ecx
	movq	(%r12), %rdx
	movl	%ecx, 12(%r12)
	movq	40(%rbx), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L1105:
	movq	%r13, 40(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1111:
	.cfi_restore_state
	movq	16(%r14), %rdi
	movq	24(%r14), %rax
	leal	1(%rdx,%rdx), %ecx
	movslq	%ecx, %rsi
	movq	40(%rbx), %r15
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1117
	addq	%rdi, %rsi
	movq	%rsi, 16(%r14)
.L1113:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jg	.L1118
.L1114:
	addl	$1, %eax
	movq	%rdi, (%r12)
	movl	%ecx, 8(%r12)
	movl	%eax, 12(%r12)
	movq	%r15, (%rdi,%rdx)
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1118:
	movq	(%r12), %rsi
	movl	%ecx, -52(%rbp)
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movl	-52(%rbp), %ecx
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$3, %rdx
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1116:
	movq	24(%r14), %rdx
	movq	16(%r14), %r15
	movq	%rdx, %rax
	subq	%r15, %rax
	cmpq	$15, %rax
	jbe	.L1119
	leaq	16(%r15), %rax
	movq	%rax, 16(%r14)
.L1108:
	subq	%rax, %rdx
	movq	%r15, %r12
	cmpq	$15, %rdx
	jbe	.L1120
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
.L1110:
	movq	%rax, (%r15)
	movq	$2, 8(%r15)
	movq	%r15, 32(%rbx)
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1117:
	movq	%r14, %rdi
	movl	%ecx, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %ecx
	movq	%rax, %rdi
	jmp	.L1113
	.p2align 4,,10
	.p2align 3
.L1120:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1119:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r14), %rdx
	movq	%rax, %r15
	movq	16(%r14), %rax
	jmp	.L1108
	.cfi_endproc
.LFE22945:
	.size	_ZN2v88internal13RegExpBuilder12AddAssertionEPNS0_10RegExpTreeE, .-_ZN2v88internal13RegExpBuilder12AddAssertionEPNS0_10RegExpTreeE
	.section	.text._ZN2v88internal13RegExpBuilder14NewAlternativeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpBuilder14NewAlternativeEv
	.type	_ZN2v88internal13RegExpBuilder14NewAlternativeEv, @function
_ZN2v88internal13RegExpBuilder14NewAlternativeEv:
.LFB18440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	_ZN2v88internal13RegExpBuilder9FlushTextEv
	movq	32(%rbx), %r12
	movq	40(%rbx), %r14
	xorl	%edx, %edx
	testq	%r12, %r12
	je	.L1122
	movl	12(%r12), %edx
.L1122:
	xorl	%eax, %eax
	testq	%r14, %r14
	movq	(%rbx), %r13
	setne	%al
	addl	%edx, %eax
	je	.L1160
	cmpl	$1, %eax
	jne	.L1161
.L1126:
	cmpq	$0, 72(%rbx)
	je	.L1140
	movq	64(%rbx), %r12
	testq	%r12, %r12
	je	.L1162
.L1141:
	movslq	12(%r12), %rax
	movl	8(%r12), %edx
	cmpl	%edx, %eax
	jge	.L1146
	leal	1(%rax), %ecx
	movq	(%r12), %rdx
	movl	%ecx, 12(%r12)
	movq	72(%rbx), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L1140:
	pxor	%xmm0, %xmm0
	movq	%r14, 72(%rbx)
	movups	%xmm0, 32(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1161:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L1163
.L1127:
	testq	%r14, %r14
	je	.L1132
	movslq	12(%r12), %rax
	movl	8(%r12), %edx
	cmpl	%edx, %eax
	jge	.L1133
	leal	1(%rax), %ecx
	movq	(%r12), %rdx
	movl	%ecx, 12(%r12)
	movq	40(%rbx), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L1134:
	movq	$0, 40(%rbx)
	movq	32(%rbx), %r12
.L1132:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$23, %rax
	jbe	.L1164
	leaq	24(%r14), %rax
	movq	%rax, 16(%rdi)
.L1139:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17RegExpAlternativeC1EPNS0_8ZoneListIPNS0_10RegExpTreeEEE@PLT
	movq	(%rbx), %r13
	jmp	.L1126
	.p2align 4,,10
	.p2align 3
.L1146:
	movq	16(%r13), %rdi
	movq	24(%r13), %rax
	leal	1(%rdx,%rdx), %ecx
	movslq	%ecx, %rsi
	movq	72(%rbx), %r15
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1165
	addq	%rdi, %rsi
	movq	%rsi, 16(%r13)
.L1148:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jg	.L1166
.L1149:
	addl	$1, %eax
	movq	%rdi, (%r12)
	movl	%ecx, 8(%r12)
	movl	%eax, 12(%r12)
	movq	%r15, (%rdi,%rdx)
	jmp	.L1140
	.p2align 4,,10
	.p2align 3
.L1160:
	movq	16(%r13), %r14
	movq	24(%r13), %rax
	subq	%r14, %rax
	cmpq	$7, %rax
	jbe	.L1167
	leaq	8(%r14), %rax
	movq	%rax, 16(%r13)
.L1125:
	leaq	16+_ZTVN2v88internal11RegExpEmptyE(%rip), %rax
	movq	%rax, (%r14)
	movq	(%rbx), %r13
	jmp	.L1126
	.p2align 4,,10
	.p2align 3
.L1166:
	movq	(%r12), %rsi
	movl	%ecx, -52(%rbp)
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movl	-52(%rbp), %ecx
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$3, %rdx
	jmp	.L1149
	.p2align 4,,10
	.p2align 3
.L1162:
	movq	24(%r13), %rdx
	movq	16(%r13), %r15
	movq	%rdx, %rax
	subq	%r15, %rax
	cmpq	$15, %rax
	jbe	.L1168
	leaq	16(%r15), %rax
	movq	%rax, 16(%r13)
.L1143:
	subq	%rax, %rdx
	movq	%r15, %r12
	cmpq	$15, %rdx
	jbe	.L1169
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
.L1145:
	movq	%rax, (%r15)
	movq	$2, 8(%r15)
	movq	%r15, 64(%rbx)
	jmp	.L1141
	.p2align 4,,10
	.p2align 3
.L1133:
	movq	16(%r13), %rcx
	movq	24(%r13), %rax
	leal	1(%rdx,%rdx), %r15d
	movslq	%r15d, %rsi
	salq	$3, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L1170
	addq	%rcx, %rsi
	movq	%rsi, 16(%r13)
.L1136:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jg	.L1171
.L1137:
	addl	$1, %eax
	movq	%rcx, (%r12)
	movl	%r15d, 8(%r12)
	movl	%eax, 12(%r12)
	movq	%r14, (%rcx,%rdx)
	jmp	.L1134
	.p2align 4,,10
	.p2align 3
.L1163:
	movq	24(%r13), %rax
	movq	16(%r13), %r14
	movq	%rax, %rdx
	subq	%r14, %rdx
	cmpq	$15, %rdx
	jbe	.L1172
	leaq	16(%r14), %rdx
	movq	%rdx, 16(%r13)
.L1129:
	subq	%rdx, %rax
	movq	%r14, %r12
	cmpq	$15, %rax
	jbe	.L1173
	leaq	16(%rdx), %rax
	movq	%rax, 16(%r13)
.L1131:
	movq	%rdx, (%r14)
	movq	$2, 8(%r14)
	movq	%r14, 32(%rbx)
	movq	40(%rbx), %r14
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1171:
	movq	(%r12), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$3, %rdx
	jmp	.L1137
	.p2align 4,,10
	.p2align 3
.L1167:
	movl	$8, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L1125
	.p2align 4,,10
	.p2align 3
.L1164:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L1139
	.p2align 4,,10
	.p2align 3
.L1165:
	movq	%r13, %rdi
	movl	%ecx, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %ecx
	movq	%rax, %rdi
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1168:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r13), %rdx
	movq	%rax, %r15
	movq	16(%r13), %rax
	jmp	.L1143
	.p2align 4,,10
	.p2align 3
.L1169:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1145
.L1170:
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L1136
.L1173:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L1131
.L1172:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	16(%r13), %rdx
	movq	%rax, %r14
	movq	24(%r13), %rax
	jmp	.L1129
	.cfi_endproc
.LFE18440:
	.size	_ZN2v88internal13RegExpBuilder14NewAlternativeEv, .-_ZN2v88internal13RegExpBuilder14NewAlternativeEv
	.section	.text._ZN2v88internal13RegExpBuilder10FlushTermsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpBuilder10FlushTermsEv
	.type	_ZN2v88internal13RegExpBuilder10FlushTermsEv, @function
_ZN2v88internal13RegExpBuilder10FlushTermsEv:
.LFB18441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	_ZN2v88internal13RegExpBuilder9FlushTextEv
	movq	32(%rbx), %r12
	movq	40(%rbx), %r14
	xorl	%edx, %edx
	testq	%r12, %r12
	je	.L1175
	movl	12(%r12), %edx
.L1175:
	xorl	%eax, %eax
	testq	%r14, %r14
	movq	(%rbx), %r13
	setne	%al
	addl	%edx, %eax
	je	.L1213
	cmpl	$1, %eax
	jne	.L1214
.L1179:
	cmpq	$0, 72(%rbx)
	je	.L1193
	movq	64(%rbx), %r12
	testq	%r12, %r12
	je	.L1215
.L1194:
	movslq	12(%r12), %rax
	movl	8(%r12), %edx
	cmpl	%edx, %eax
	jge	.L1199
	leal	1(%rax), %ecx
	movq	(%r12), %rdx
	movl	%ecx, 12(%r12)
	movq	72(%rbx), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L1193:
	pxor	%xmm0, %xmm0
	movq	%r14, 72(%rbx)
	movups	%xmm0, 32(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1214:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L1216
.L1180:
	testq	%r14, %r14
	je	.L1185
	movslq	12(%r12), %rax
	movl	8(%r12), %edx
	cmpl	%edx, %eax
	jge	.L1186
	leal	1(%rax), %ecx
	movq	(%r12), %rdx
	movl	%ecx, 12(%r12)
	movq	40(%rbx), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L1187:
	movq	$0, 40(%rbx)
	movq	32(%rbx), %r12
.L1185:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$23, %rax
	jbe	.L1217
	leaq	24(%r14), %rax
	movq	%rax, 16(%rdi)
.L1192:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17RegExpAlternativeC1EPNS0_8ZoneListIPNS0_10RegExpTreeEEE@PLT
	movq	(%rbx), %r13
	jmp	.L1179
	.p2align 4,,10
	.p2align 3
.L1199:
	movq	16(%r13), %rdi
	movq	24(%r13), %rax
	leal	1(%rdx,%rdx), %ecx
	movslq	%ecx, %rsi
	movq	72(%rbx), %r15
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1218
	addq	%rdi, %rsi
	movq	%rsi, 16(%r13)
.L1201:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jg	.L1219
.L1202:
	addl	$1, %eax
	movq	%rdi, (%r12)
	movl	%ecx, 8(%r12)
	movl	%eax, 12(%r12)
	movq	%r15, (%rdi,%rdx)
	jmp	.L1193
	.p2align 4,,10
	.p2align 3
.L1213:
	movq	16(%r13), %r14
	movq	24(%r13), %rax
	subq	%r14, %rax
	cmpq	$7, %rax
	jbe	.L1220
	leaq	8(%r14), %rax
	movq	%rax, 16(%r13)
.L1178:
	leaq	16+_ZTVN2v88internal11RegExpEmptyE(%rip), %rax
	movq	%rax, (%r14)
	movq	(%rbx), %r13
	jmp	.L1179
	.p2align 4,,10
	.p2align 3
.L1219:
	movq	(%r12), %rsi
	movl	%ecx, -52(%rbp)
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movl	-52(%rbp), %ecx
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$3, %rdx
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1215:
	movq	24(%r13), %rdx
	movq	16(%r13), %r15
	movq	%rdx, %rax
	subq	%r15, %rax
	cmpq	$15, %rax
	jbe	.L1221
	leaq	16(%r15), %rax
	movq	%rax, 16(%r13)
.L1196:
	subq	%rax, %rdx
	movq	%r15, %r12
	cmpq	$15, %rdx
	jbe	.L1222
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
.L1198:
	movq	%rax, (%r15)
	movq	$2, 8(%r15)
	movq	%r15, 64(%rbx)
	jmp	.L1194
	.p2align 4,,10
	.p2align 3
.L1186:
	movq	16(%r13), %rcx
	movq	24(%r13), %rax
	leal	1(%rdx,%rdx), %r15d
	movslq	%r15d, %rsi
	salq	$3, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L1223
	addq	%rcx, %rsi
	movq	%rsi, 16(%r13)
.L1189:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jg	.L1224
.L1190:
	addl	$1, %eax
	movq	%rcx, (%r12)
	movl	%r15d, 8(%r12)
	movl	%eax, 12(%r12)
	movq	%r14, (%rcx,%rdx)
	jmp	.L1187
	.p2align 4,,10
	.p2align 3
.L1216:
	movq	24(%r13), %rax
	movq	16(%r13), %r14
	movq	%rax, %rdx
	subq	%r14, %rdx
	cmpq	$15, %rdx
	jbe	.L1225
	leaq	16(%r14), %rdx
	movq	%rdx, 16(%r13)
.L1182:
	subq	%rdx, %rax
	movq	%r14, %r12
	cmpq	$15, %rax
	jbe	.L1226
	leaq	16(%rdx), %rax
	movq	%rax, 16(%r13)
.L1184:
	movq	%rdx, (%r14)
	movq	$2, 8(%r14)
	movq	%r14, 32(%rbx)
	movq	40(%rbx), %r14
	jmp	.L1180
	.p2align 4,,10
	.p2align 3
.L1224:
	movq	(%r12), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$3, %rdx
	jmp	.L1190
	.p2align 4,,10
	.p2align 3
.L1220:
	movl	$8, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L1178
	.p2align 4,,10
	.p2align 3
.L1217:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L1192
	.p2align 4,,10
	.p2align 3
.L1218:
	movq	%r13, %rdi
	movl	%ecx, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %ecx
	movq	%rax, %rdi
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1221:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r13), %rdx
	movq	%rax, %r15
	movq	16(%r13), %rax
	jmp	.L1196
	.p2align 4,,10
	.p2align 3
.L1222:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1198
.L1223:
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L1189
.L1226:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L1184
.L1225:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	16(%r13), %rdx
	movq	%rax, %r14
	movq	24(%r13), %rax
	jmp	.L1182
	.cfi_endproc
.LFE18441:
	.size	_ZN2v88internal13RegExpBuilder10FlushTermsEv, .-_ZN2v88internal13RegExpBuilder10FlushTermsEv
	.section	.text._ZN2v88internal13RegExpBuilder25NeedsDesugaringForUnicodeEPNS0_20RegExpCharacterClassE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpBuilder25NeedsDesugaringForUnicodeEPNS0_20RegExpCharacterClassE
	.type	_ZN2v88internal13RegExpBuilder25NeedsDesugaringForUnicodeEPNS0_20RegExpCharacterClassE, @function
_ZN2v88internal13RegExpBuilder25NeedsDesugaringForUnicodeEPNS0_20RegExpCharacterClassE:
.LFB18445:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %eax
	testb	$16, %al
	je	.L1243
	testb	$2, %al
	je	.L1229
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1229:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rsi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal12CharacterSet6rangesEPNS0_4ZoneE@PLT
	movq	%rax, %rbx
	movq	%rax, %rdi
	call	_ZN2v88internal14CharacterRange12CanonicalizeEPNS0_8ZoneListIS1_EE@PLT
	movl	12(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L1228
	movq	(%rbx), %rsi
	movslq	%ecx, %rdx
	salq	$3, %rdx
	leaq	(%rsi,%rdx), %rax
	leaq	-8(%rsi,%rdx), %rsi
	leal	-1(%rcx), %edx
	salq	$3, %rdx
	subq	%rdx, %rsi
	jmp	.L1233
	.p2align 4,,10
	.p2align 3
.L1247:
	cmpl	$57343, %ecx
	jg	.L1234
	cmpl	$55295, %edx
	jg	.L1231
.L1234:
	subq	$8, %rax
	cmpq	%rsi, %rax
	je	.L1228
.L1233:
	movl	-4(%rax), %edx
	movl	-8(%rax), %ecx
	cmpl	$65535, %edx
	jle	.L1247
.L1231:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1243:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1228:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18445:
	.size	_ZN2v88internal13RegExpBuilder25NeedsDesugaringForUnicodeEPNS0_20RegExpCharacterClassE, .-_ZN2v88internal13RegExpBuilder25NeedsDesugaringForUnicodeEPNS0_20RegExpCharacterClassE
	.section	.text._ZN2v88internal13RegExpBuilder8ToRegExpEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpBuilder8ToRegExpEv
	.type	_ZN2v88internal13RegExpBuilder8ToRegExpEv, @function
_ZN2v88internal13RegExpBuilder8ToRegExpEv:
.LFB18447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	_ZN2v88internal13RegExpBuilder9FlushTextEv
	movq	32(%rbx), %r13
	movq	40(%rbx), %r12
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L1249
	movl	12(%r13), %eax
.L1249:
	xorl	%r15d, %r15d
	testq	%r12, %r12
	movq	(%rbx), %r14
	setne	%r15b
	addl	%r15d, %eax
	je	.L1314
	cmpl	$1, %eax
	jne	.L1315
.L1253:
	cmpq	$0, 72(%rbx)
	movq	64(%rbx), %r13
	je	.L1267
	testq	%r13, %r13
	je	.L1316
.L1268:
	movslq	12(%r13), %rax
	movl	8(%r13), %edx
	cmpl	%edx, %eax
	jge	.L1273
	leal	1(%rax), %ecx
	movq	0(%r13), %rdx
	movl	%ecx, 12(%r13)
	movq	72(%rbx), %rcx
	movq	%rcx, (%rdx,%rax,8)
	movq	64(%rbx), %r13
.L1267:
	pxor	%xmm0, %xmm0
	movq	%r12, 72(%rbx)
	movups	%xmm0, 32(%rbx)
	testq	%r13, %r13
	je	.L1277
	addl	12(%r13), %r15d
.L1277:
	testl	%r15d, %r15d
	je	.L1317
	cmpl	$1, %r15d
	jne	.L1318
.L1248:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1315:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L1319
.L1254:
	testq	%r12, %r12
	je	.L1259
	movslq	12(%r13), %rax
	movl	8(%r13), %edx
	cmpl	%edx, %eax
	jge	.L1260
	leal	1(%rax), %ecx
	movq	0(%r13), %rdx
	movl	%ecx, 12(%r13)
	movq	40(%rbx), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L1261:
	movq	$0, 40(%rbx)
	movq	32(%rbx), %r13
.L1259:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$23, %rax
	jbe	.L1320
	leaq	24(%r12), %rax
	movq	%rax, 16(%rdi)
.L1266:
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	_ZN2v88internal17RegExpAlternativeC1EPNS0_8ZoneListIPNS0_10RegExpTreeEEE@PLT
	testq	%r12, %r12
	movq	(%rbx), %r14
	setne	%r15b
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1318:
	movq	(%rbx), %r14
	testq	%r13, %r13
	je	.L1321
.L1282:
	testq	%r12, %r12
	je	.L1287
	movslq	12(%r13), %rax
	movl	8(%r13), %edx
	cmpl	%edx, %eax
	jge	.L1288
	leal	1(%rax), %ecx
	movq	0(%r13), %rdx
	movl	%ecx, 12(%r13)
	movq	72(%rbx), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L1289:
	movq	$0, 72(%rbx)
	movq	64(%rbx), %r13
.L1287:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$23, %rax
	jbe	.L1322
	leaq	24(%r12), %rax
	movq	%rax, 16(%rdi)
.L1294:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17RegExpDisjunctionC1EPNS0_8ZoneListIPNS0_10RegExpTreeEEE@PLT
	jmp	.L1248
	.p2align 4,,10
	.p2align 3
.L1273:
	movq	16(%r14), %rdi
	movq	24(%r14), %rax
	leal	1(%rdx,%rdx), %r8d
	movslq	%r8d, %rsi
	movq	72(%rbx), %rcx
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1323
	addq	%rdi, %rsi
	movq	%rsi, 16(%r14)
.L1275:
	movslq	12(%r13), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jg	.L1324
.L1276:
	addl	$1, %eax
	movq	%rdi, 0(%r13)
	movl	%r8d, 8(%r13)
	movl	%eax, 12(%r13)
	movq	%rcx, (%rdi,%rdx)
	movq	64(%rbx), %r13
	jmp	.L1267
	.p2align 4,,10
	.p2align 3
.L1314:
	movq	16(%r14), %r12
	movq	24(%r14), %rax
	subq	%r12, %rax
	cmpq	$7, %rax
	jbe	.L1325
	leaq	8(%r12), %rax
	movq	%rax, 16(%r14)
.L1252:
	leaq	16+_ZTVN2v88internal11RegExpEmptyE(%rip), %rax
	movl	$1, %r15d
	movq	%rax, (%r12)
	movq	(%rbx), %r14
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1317:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$7, %rax
	jbe	.L1326
	leaq	8(%r12), %rax
	movq	%rax, 16(%rdi)
.L1280:
	leaq	16+_ZTVN2v88internal11RegExpEmptyE(%rip), %rax
	movq	%rax, (%r12)
	jmp	.L1248
	.p2align 4,,10
	.p2align 3
.L1324:
	movq	0(%r13), %rsi
	movl	%r8d, -60(%rbp)
	movq	%rcx, -56(%rbp)
	call	memcpy@PLT
	movslq	12(%r13), %rdx
	movl	-60(%rbp), %r8d
	movq	%rax, %rdi
	movq	-56(%rbp), %rcx
	movq	%rdx, %rax
	salq	$3, %rdx
	jmp	.L1276
	.p2align 4,,10
	.p2align 3
.L1316:
	movq	24(%r14), %rcx
	movq	16(%r14), %rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	cmpq	$15, %rax
	jbe	.L1327
	leaq	16(%rdx), %rax
	movq	%rax, 16(%r14)
.L1270:
	subq	%rax, %rcx
	movq	%rdx, %r13
	cmpq	$15, %rcx
	jbe	.L1328
	leaq	16(%rax), %rcx
	movq	%rcx, 16(%r14)
.L1272:
	movq	%rax, (%rdx)
	movq	$2, 8(%rdx)
	movq	%rdx, 64(%rbx)
	jmp	.L1268
	.p2align 4,,10
	.p2align 3
.L1288:
	movq	16(%r14), %rcx
	movq	24(%r14), %rax
	leal	1(%rdx,%rdx), %r15d
	movslq	%r15d, %rsi
	salq	$3, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L1329
	addq	%rcx, %rsi
	movq	%rsi, 16(%r14)
.L1291:
	movslq	12(%r13), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jg	.L1330
.L1292:
	addl	$1, %eax
	movq	%rcx, 0(%r13)
	movl	%r15d, 8(%r13)
	movl	%eax, 12(%r13)
	movq	%r12, (%rcx,%rdx)
	jmp	.L1289
	.p2align 4,,10
	.p2align 3
.L1260:
	movq	16(%r14), %rcx
	movq	24(%r14), %rax
	leal	1(%rdx,%rdx), %r15d
	movslq	%r15d, %rsi
	salq	$3, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L1331
	addq	%rcx, %rsi
	movq	%rsi, 16(%r14)
.L1263:
	movslq	12(%r13), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jg	.L1332
.L1264:
	addl	$1, %eax
	movq	%rcx, 0(%r13)
	movl	%r15d, 8(%r13)
	movl	%eax, 12(%r13)
	movq	%r12, (%rcx,%rdx)
	jmp	.L1261
	.p2align 4,,10
	.p2align 3
.L1319:
	movq	24(%r14), %rax
	movq	16(%r14), %r12
	movq	%rax, %rdx
	subq	%r12, %rdx
	cmpq	$15, %rdx
	jbe	.L1333
	leaq	16(%r12), %rdx
	movq	%rdx, 16(%r14)
.L1256:
	subq	%rdx, %rax
	movq	%r12, %r13
	cmpq	$15, %rax
	jbe	.L1334
	leaq	16(%rdx), %rax
	movq	%rax, 16(%r14)
.L1258:
	movq	%rdx, (%r12)
	movq	$2, 8(%r12)
	movq	%r12, 32(%rbx)
	movq	40(%rbx), %r12
	jmp	.L1254
	.p2align 4,,10
	.p2align 3
.L1321:
	movq	24(%r14), %rax
	movq	16(%r14), %r12
	movq	%rax, %rdx
	subq	%r12, %rdx
	cmpq	$15, %rdx
	jbe	.L1335
	leaq	16(%r12), %rdx
	movq	%rdx, 16(%r14)
.L1284:
	subq	%rdx, %rax
	movq	%r12, %r13
	cmpq	$15, %rax
	jbe	.L1336
	leaq	16(%rdx), %rax
	movq	%rax, 16(%r14)
.L1286:
	movq	%rdx, (%r12)
	movq	$2, 8(%r12)
	movq	%r12, 64(%rbx)
	movq	72(%rbx), %r12
	jmp	.L1282
	.p2align 4,,10
	.p2align 3
.L1332:
	movq	0(%r13), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%r13), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$3, %rdx
	jmp	.L1264
	.p2align 4,,10
	.p2align 3
.L1330:
	movq	0(%r13), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%r13), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$3, %rdx
	jmp	.L1292
	.p2align 4,,10
	.p2align 3
.L1325:
	movl	$8, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1326:
	movl	$8, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L1280
	.p2align 4,,10
	.p2align 3
.L1320:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L1266
	.p2align 4,,10
	.p2align 3
.L1322:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L1294
	.p2align 4,,10
	.p2align 3
.L1323:
	movq	%r14, %rdi
	movl	%r8d, -60(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movl	-60(%rbp), %r8d
	movq	%rax, %rdi
	jmp	.L1275
	.p2align 4,,10
	.p2align 3
.L1327:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r14), %rcx
	movq	%rax, %rdx
	movq	16(%r14), %rax
	jmp	.L1270
	.p2align 4,,10
	.p2align 3
.L1328:
	movl	$16, %esi
	movq	%r14, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	jmp	.L1272
.L1331:
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L1263
.L1329:
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L1291
.L1336:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L1286
.L1335:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	16(%r14), %rdx
	movq	%rax, %r12
	movq	24(%r14), %rax
	jmp	.L1284
.L1334:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L1258
.L1333:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	16(%r14), %rdx
	movq	%rax, %r12
	movq	24(%r14), %rax
	jmp	.L1256
	.cfi_endproc
.LFE18447:
	.size	_ZN2v88internal13RegExpBuilder8ToRegExpEv, .-_ZN2v88internal13RegExpBuilder8ToRegExpEv
	.section	.rodata._ZN2v88internal13RegExpBuilder19AddQuantifierToAtomEiiNS0_16RegExpQuantifier14QuantifierTypeE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"unreachable code"
	.section	.text._ZN2v88internal13RegExpBuilder19AddQuantifierToAtomEiiNS0_16RegExpQuantifier14QuantifierTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpBuilder19AddQuantifierToAtomEiiNS0_16RegExpQuantifier14QuantifierTypeE
	.type	_ZN2v88internal13RegExpBuilder19AddQuantifierToAtomEiiNS0_16RegExpQuantifier14QuantifierTypeE, @function
_ZN2v88internal13RegExpBuilder19AddQuantifierToAtomEiiNS0_16RegExpQuantifier14QuantifierTypeE:
.LFB18448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movzwl	24(%rdi), %esi
	movl	%ecx, -56(%rbp)
	testw	%si, %si
	jne	.L1406
.L1338:
	movzbl	8(%rbx), %r8d
	testb	%r8b, %r8b
	jne	.L1407
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L1341
	movslq	12(%rax), %rcx
	movq	(%rax), %r8
	leaq	16+_ZTVN2v88internal10RegExpAtomE(%rip), %r9
	movq	(%rbx), %rdx
	movl	12(%rbx), %r14d
	cmpl	$1, %ecx
	jg	.L1408
.L1343:
	movq	$0, 16(%rbx)
	movq	16(%rdx), %r15
	movq	24(%rdx), %rax
	subq	%r15, %rax
	cmpq	$31, %rax
	jbe	.L1409
	leaq	32(%r15), %rax
	movq	%rax, 16(%rdx)
.L1357:
	movq	%r9, (%r15)
	movq	%rbx, %rdi
	movq	%r8, 8(%r15)
	movq	%rcx, 16(%r15)
	movl	%r14d, 24(%r15)
	call	_ZN2v88internal13RegExpBuilder9FlushTextEv
.L1358:
	movq	(%rbx), %r8
	movq	16(%r8), %r14
	movq	24(%r8), %rax
	subq	%r14, %rax
	cmpq	$39, %rax
	jbe	.L1410
	leaq	40(%r14), %rax
	movq	%rax, 16(%r8)
.L1383:
	leaq	16+_ZTVN2v88internal16RegExpQuantifierE(%rip), %rax
	movq	%r15, 8(%r14)
	movq	%rax, (%r14)
	movl	-56(%rbp), %eax
	movl	%r12d, 16(%r14)
	movl	%eax, 32(%r14)
	movq	(%r15), %rax
	movl	%r13d, 20(%r14)
	movq	56(%rax), %rax
	testl	%r12d, %r12d
	jle	.L1384
	movq	%r8, -56(%rbp)
	movq	%r15, %rdi
	call	*%rax
	movq	-56(%rbp), %r8
	movl	%eax, %r9d
	movl	$2147483647, %eax
	cltd
	idivl	%r12d
	cmpl	%eax, %r9d
	jle	.L1411
	movq	(%r15), %rax
	movl	$2147483647, 24(%r14)
	movq	64(%rax), %rax
	testl	%r13d, %r13d
	jg	.L1412
	.p2align 4,,10
	.p2align 3
.L1387:
	movq	%r8, -56(%rbp)
	movq	%r15, %rdi
	call	*%rax
	movq	-56(%rbp), %r8
	imull	%eax, %r13d
	movl	%r13d, 28(%r14)
.L1389:
	cmpq	$0, 40(%rbx)
	je	.L1390
	movq	32(%rbx), %r12
	testq	%r12, %r12
	je	.L1413
.L1391:
	movslq	12(%r12), %rax
	movl	8(%r12), %edx
	cmpl	%edx, %eax
	jge	.L1396
	leal	1(%rax), %esi
	movq	(%r12), %rdx
	movl	%esi, 12(%r12)
	movq	40(%rbx), %rsi
	movq	%rsi, (%rdx,%rax,8)
.L1390:
	movq	%r14, 40(%rbx)
	movl	$1, %r8d
.L1337:
	addq	$56, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1411:
	.cfi_restore_state
	movq	(%r15), %rax
	movq	56(%rax), %rax
.L1384:
	movq	%r8, -56(%rbp)
	movq	%r15, %rdi
	call	*%rax
	movq	-56(%rbp), %r8
	imull	%eax, %r12d
	movq	(%r15), %rax
	movq	64(%rax), %rax
	movl	%r12d, 24(%r14)
	testl	%r13d, %r13d
	jle	.L1387
.L1412:
	movq	%r8, -56(%rbp)
	movq	%r15, %rdi
	call	*%rax
	movq	-56(%rbp), %r8
	movl	%eax, %r9d
	movl	$2147483647, %eax
	cltd
	idivl	%r13d
	cmpl	%eax, %r9d
	jle	.L1414
	movl	$2147483647, 28(%r14)
	jmp	.L1389
	.p2align 4,,10
	.p2align 3
.L1407:
	movb	$0, 8(%rbx)
	jmp	.L1337
	.p2align 4,,10
	.p2align 3
.L1406:
	xorl	%eax, %eax
	movw	%ax, 24(%rdi)
	call	_ZN2v88internal13RegExpBuilder30AddCharacterClassForDesugaringEi
	jmp	.L1338
	.p2align 4,,10
	.p2align 3
.L1341:
	movq	48(%rbx), %rax
	movq	56(%rbx), %r15
	testq	%rax, %rax
	je	.L1359
	xorl	%edx, %edx
	movl	12(%rax), %ecx
	testq	%r15, %r15
	setne	%dl
	addl	%ecx, %edx
	testl	%edx, %edx
	jle	.L1360
	testl	%ecx, %ecx
	jg	.L1415
.L1361:
	movq	$0, 56(%rbx)
.L1362:
	movq	%rbx, %rdi
	call	_ZN2v88internal13RegExpBuilder9FlushTextEv
	jmp	.L1358
	.p2align 4,,10
	.p2align 3
.L1414:
	movq	(%r15), %rax
	movq	64(%rax), %rax
	jmp	.L1387
	.p2align 4,,10
	.p2align 3
.L1408:
	subl	$1, %ecx
	movq	24(%rdx), %rax
	movslq	%ecx, %r15
	movq	16(%rdx), %rcx
	subq	%rcx, %rax
	cmpq	$31, %rax
	jbe	.L1416
	leaq	32(%rcx), %rax
	movq	%rax, 16(%rdx)
.L1345:
	leaq	16+_ZTVN2v88internal10RegExpAtomE(%rip), %r9
	movq	%r8, 8(%rcx)
	movq	%r9, (%rcx)
	movq	%r15, 16(%rcx)
	movl	%r14d, 24(%rcx)
	cmpq	$0, 56(%rbx)
	je	.L1346
	movq	48(%rbx), %r10
	testq	%r10, %r10
	je	.L1417
.L1347:
	movslq	12(%r10), %rax
	movl	8(%r10), %esi
	cmpl	%esi, %eax
	jge	.L1352
	leal	1(%rax), %esi
	movq	(%r10), %rdx
	movl	%esi, 12(%r10)
	movq	56(%rbx), %rsi
	movq	%rsi, (%rdx,%rax,8)
.L1346:
	movl	12(%rbx), %r14d
	movq	(%rbx), %rdx
	movq	%rcx, 56(%rbx)
	leaq	(%r8,%r15,2), %r8
	movl	$1, %ecx
	jmp	.L1343
	.p2align 4,,10
	.p2align 3
.L1396:
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	leal	1(%rdx,%rdx), %r15d
	movslq	%r15d, %rsi
	movq	40(%rbx), %r13
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1418
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L1398:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L1399
	movq	(%r12), %rsi
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$3, %rdx
.L1399:
	addl	$1, %eax
	movq	%rdi, (%r12)
	movl	%r15d, 8(%r12)
	movl	%eax, 12(%r12)
	movq	%r13, (%rdi,%rdx)
	jmp	.L1390
	.p2align 4,,10
	.p2align 3
.L1359:
	testq	%r15, %r15
	jne	.L1361
.L1360:
	movq	32(%rbx), %rax
	movq	40(%rbx), %r15
	testq	%rax, %rax
	je	.L1363
	xorl	%ecx, %ecx
	movl	12(%rax), %edx
	testq	%r15, %r15
	setne	%cl
	addl	%edx, %ecx
	testl	%ecx, %ecx
	jle	.L1364
	testl	%edx, %edx
	jg	.L1419
.L1365:
	movq	$0, 40(%rbx)
.L1366:
	movq	(%r15), %rax
	movb	%r8b, -64(%rbp)
	movq	%r15, %rdi
	call	*224(%rax)
	movzbl	-64(%rbp), %r8d
	testb	%al, %al
	je	.L1371
	testb	$16, 12(%rbx)
	jne	.L1337
	movq	(%r15), %rax
	movb	%r8b, -64(%rbp)
	movq	%r15, %rdi
	call	*216(%rax)
	movzbl	-64(%rbp), %r8d
	cmpl	$1, 28(%rax)
	je	.L1337
.L1371:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*64(%rax)
	testl	%eax, %eax
	jne	.L1358
	movl	$1, %r8d
	testl	%r12d, %r12d
	je	.L1337
	cmpq	$0, 40(%rbx)
	je	.L1372
	movq	32(%rbx), %r12
	movq	(%rbx), %r13
	testq	%r12, %r12
	je	.L1420
.L1373:
	movslq	12(%r12), %rax
	movl	8(%r12), %edx
	cmpl	%edx, %eax
	jge	.L1378
	leal	1(%rax), %ecx
	movq	(%r12), %rdx
	movl	%ecx, 12(%r12)
	movq	40(%rbx), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L1372:
	movq	%r15, 40(%rbx)
	movl	$1, %r8d
	jmp	.L1337
	.p2align 4,,10
	.p2align 3
.L1413:
	movq	24(%r8), %rax
	movq	16(%r8), %r13
	movq	%rax, %rdx
	subq	%r13, %rdx
	cmpq	$15, %rdx
	jbe	.L1421
	leaq	16(%r13), %rdx
	movq	%rdx, 16(%r8)
.L1393:
	subq	%rdx, %rax
	movq	%r13, %r12
	cmpq	$15, %rax
	jbe	.L1422
	leaq	16(%rdx), %rax
	movq	%rax, 16(%r8)
.L1395:
	movq	%rdx, 0(%r13)
	movq	$2, 8(%r13)
	movq	%r13, 32(%rbx)
	jmp	.L1391
	.p2align 4,,10
	.p2align 3
.L1410:
	movq	%r8, %rdi
	movl	$40, %esi
	movq	%r8, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %r8
	movq	%rax, %r14
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1415:
	movq	(%rax), %rdx
	subl	$1, %ecx
	movslq	%ecx, %rsi
	movq	(%rdx,%rsi,8), %rdx
	movl	%ecx, 12(%rax)
	movq	%rdx, 56(%rbx)
	jmp	.L1362
	.p2align 4,,10
	.p2align 3
.L1419:
	movq	(%rax), %rcx
	subl	$1, %edx
	movslq	%edx, %rsi
	movq	(%rcx,%rsi,8), %rcx
	movl	%edx, 12(%rax)
	movq	%rcx, 40(%rbx)
	jmp	.L1366
	.p2align 4,,10
	.p2align 3
.L1409:
	movl	$32, %esi
	movq	%rdx, %rdi
	movq	%r9, -80(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%r8, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r9
	movq	%rax, %r15
	jmp	.L1357
	.p2align 4,,10
	.p2align 3
.L1352:
	movq	16(%rdx), %rdi
	movq	24(%rdx), %rax
	leal	1(%rsi,%rsi), %r11d
	movslq	%r11d, %rsi
	movq	56(%rbx), %r14
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1423
	addq	%rdi, %rsi
	movq	%rsi, 16(%rdx)
.L1354:
	movslq	12(%r10), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L1355
	movq	(%r10), %rsi
	movq	%r9, -96(%rbp)
	movl	%r11d, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r10
	movq	-96(%rbp), %r9
	movl	-88(%rbp), %r11d
	movq	-80(%rbp), %rcx
	movq	%rax, %rdi
	movslq	12(%r10), %rdx
	movq	-72(%rbp), %r8
	movq	%rdx, %rax
	salq	$3, %rdx
.L1355:
	addl	$1, %eax
	movq	%rdi, (%r10)
	movl	%r11d, 8(%r10)
	movl	%eax, 12(%r10)
	movq	%r14, (%rdi,%rdx)
	jmp	.L1346
	.p2align 4,,10
	.p2align 3
.L1363:
	testq	%r15, %r15
	jne	.L1365
.L1364:
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1417:
	movq	24(%rdx), %rax
	movq	16(%rdx), %r14
	movq	%rax, %rsi
	subq	%r14, %rsi
	cmpq	$15, %rsi
	jbe	.L1424
	leaq	16(%r14), %rsi
	movq	%rsi, 16(%rdx)
.L1349:
	subq	%rsi, %rax
	movq	%r14, %r10
	cmpq	$15, %rax
	jbe	.L1425
	leaq	16(%rsi), %rax
	movq	%rax, 16(%rdx)
.L1351:
	movq	%rsi, (%r14)
	movq	$2, 8(%r14)
	movq	%r14, 48(%rbx)
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1416:
	movq	%rdx, %rdi
	movl	$32, %esi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rcx
	jmp	.L1345
	.p2align 4,,10
	.p2align 3
.L1418:
	movq	%r8, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L1398
.L1422:
	movq	%r8, %rdi
	movl	$16, %esi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L1395
.L1421:
	movq	%r8, %rdi
	movl	$16, %esi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %r13
	movq	16(%r8), %rdx
	movq	24(%r8), %rax
	jmp	.L1393
.L1423:
	movq	%rdx, %rdi
	movq	%r9, -96(%rbp)
	movl	%r11d, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r10, -72(%rbp)
	movq	%r8, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %r10
	movq	-80(%rbp), %rcx
	movl	-88(%rbp), %r11d
	movq	%rax, %rdi
	movq	-96(%rbp), %r9
	jmp	.L1354
.L1378:
	movq	16(%r13), %rcx
	movq	24(%r13), %rax
	leal	1(%rdx,%rdx), %r8d
	movslq	%r8d, %rsi
	movq	40(%rbx), %r14
	salq	$3, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L1426
	addq	%rcx, %rsi
	movq	%rsi, 16(%r13)
.L1380:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L1381
	movq	(%r12), %rsi
	movq	%rcx, %rdi
	movl	%r8d, -56(%rbp)
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movl	-56(%rbp), %r8d
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$3, %rdx
.L1381:
	addl	$1, %eax
	movq	%rcx, (%r12)
	movl	%r8d, 8(%r12)
	movl	%eax, 12(%r12)
	movq	%r14, (%rcx,%rdx)
	jmp	.L1372
.L1425:
	movl	$16, %esi
	movq	%rdx, %rdi
	movq	%r9, -96(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r14, -80(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r10
	movq	-88(%rbp), %rcx
	movq	%rax, %rsi
	movq	-96(%rbp), %r9
	jmp	.L1351
.L1424:
	movl	$16, %esi
	movq	%rdx, %rdi
	movq	%r9, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %r14
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %r9
	movq	24(%rdx), %rax
	movq	16(%rdx), %rsi
	jmp	.L1349
.L1420:
	movq	24(%r13), %rdx
	movq	16(%r13), %r14
	movq	%rdx, %rax
	subq	%r14, %rax
	cmpq	$15, %rax
	jbe	.L1427
	leaq	16(%r14), %rax
	movq	%rax, 16(%r13)
.L1375:
	subq	%rax, %rdx
	movq	%r14, %r12
	cmpq	$15, %rdx
	jbe	.L1428
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
.L1377:
	movq	%rax, (%r14)
	movq	$2, 8(%r14)
	movq	%r14, 32(%rbx)
	jmp	.L1373
.L1426:
	movq	%r13, %rdi
	movl	%r8d, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-56(%rbp), %r8d
	movq	%rax, %rcx
	jmp	.L1380
.L1428:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1377
.L1427:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r13), %rdx
	movq	%rax, %r14
	movq	16(%r13), %rax
	jmp	.L1375
	.cfi_endproc
.LFE18448:
	.size	_ZN2v88internal13RegExpBuilder19AddQuantifierToAtomEiiNS0_16RegExpQuantifier14QuantifierTypeE, .-_ZN2v88internal13RegExpBuilder19AddQuantifierToAtomEiiNS0_16RegExpQuantifier14QuantifierTypeE
	.section	.text._ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE,"axG",@progbits,_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	.type	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE, @function
_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE:
.LFB18948:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	12(%rdi), %rax
	movl	8(%rdi), %ecx
	cmpl	%ecx, %eax
	jge	.L1430
	leal	1(%rax), %ecx
	movq	(%rdi), %rdx
	movl	%ecx, 12(%rdi)
	movq	(%rsi), %rcx
	movq	%rcx, (%rdx,%rax,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1430:
	.cfi_restore_state
	leal	1(%rcx,%rcx), %r12d
	movq	24(%rdx), %rax
	movq	16(%rdx), %rcx
	movq	(%rsi), %r13
	movslq	%r12d, %rsi
	salq	$3, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L1436
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdx)
.L1433:
	movslq	12(%rbx), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jg	.L1437
.L1434:
	addl	$1, %eax
	movl	%r12d, 8(%rbx)
	movq	%rcx, (%rbx)
	movl	%eax, 12(%rbx)
	movq	%r13, (%rcx,%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1437:
	.cfi_restore_state
	movq	(%rbx), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%rbx), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$3, %rdx
	jmp	.L1434
	.p2align 4,,10
	.p2align 3
.L1436:
	movq	%rdx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L1433
	.cfi_endproc
.LFE18948:
	.size	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE, .-_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	.section	.rodata._ZNSt6vectorItN2v88internal13ZoneAllocatorItEEE12emplace_backIJtEEEvDpOT_.str1.1,"aMS",@progbits,1
.LC10:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorItN2v88internal13ZoneAllocatorItEEE12emplace_backIJtEEEvDpOT_,"axG",@progbits,_ZNSt6vectorItN2v88internal13ZoneAllocatorItEEE12emplace_backIJtEEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorItN2v88internal13ZoneAllocatorItEEE12emplace_backIJtEEEvDpOT_
	.type	_ZNSt6vectorItN2v88internal13ZoneAllocatorItEEE12emplace_backIJtEEEvDpOT_, @function
_ZNSt6vectorItN2v88internal13ZoneAllocatorItEEE12emplace_backIJtEEEvDpOT_:
.LFB21481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r13
	cmpq	24(%rdi), %r13
	je	.L1439
	movzwl	(%rsi), %eax
	movw	%ax, 0(%r13)
	addq	$2, 16(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1439:
	.cfi_restore_state
	movq	8(%rdi), %r14
	movq	%r13, %r15
	subq	%r14, %r15
	movq	%r15, %rax
	sarq	%rax
	cmpq	$1073741823, %rax
	je	.L1464
	testq	%rax, %rax
	je	.L1451
	movl	$2147483648, %esi
	movl	$2147483646, %edx
	cmpq	%rax, %r15
	jnb	.L1465
.L1442:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L1466
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1445:
	leaq	(%rax,%rdx), %rdi
	leaq	2(%rax), %rdx
.L1443:
	movzwl	(%r12), %ecx
	movw	%cx, (%rax,%r15)
	cmpq	%r14, %r13
	je	.L1446
	leaq	-2(%r13), %r8
	leaq	15(%rax), %rdx
	subq	%r14, %r8
	subq	%r14, %rdx
	movq	%r8, %rcx
	shrq	%rcx
	cmpq	$30, %rdx
	jbe	.L1454
	cmpq	$12, %r8
	jbe	.L1454
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	$3, %rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L1448:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L1448
	movq	%rsi, %rcx
	andq	$-8, %rcx
	leaq	(%rcx,%rcx), %rdx
	addq	%rdx, %r14
	addq	%rax, %rdx
	cmpq	%rcx, %rsi
	je	.L1450
	movzwl	(%r14), %ecx
	movw	%cx, (%rdx)
	leaq	2(%r14), %rcx
	cmpq	%rcx, %r13
	je	.L1450
	movzwl	2(%r14), %ecx
	movw	%cx, 2(%rdx)
	leaq	4(%r14), %rcx
	cmpq	%rcx, %r13
	je	.L1450
	movzwl	4(%r14), %ecx
	movw	%cx, 4(%rdx)
	leaq	6(%r14), %rcx
	cmpq	%rcx, %r13
	je	.L1450
	movzwl	6(%r14), %ecx
	movw	%cx, 6(%rdx)
	leaq	8(%r14), %rcx
	cmpq	%rcx, %r13
	je	.L1450
	movzwl	8(%r14), %ecx
	movw	%cx, 8(%rdx)
	leaq	10(%r14), %rcx
	cmpq	%rcx, %r13
	je	.L1450
	movzwl	10(%r14), %ecx
	movw	%cx, 10(%rdx)
	leaq	12(%r14), %rcx
	cmpq	%rcx, %r13
	je	.L1450
	movzwl	12(%r14), %ecx
	movw	%cx, 12(%rdx)
.L1450:
	leaq	4(%rax,%r8), %rdx
.L1446:
	movq	%rax, %xmm0
	movq	%rdx, %xmm2
	movq	%rdi, 24(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1465:
	.cfi_restore_state
	testq	%r15, %r15
	jne	.L1467
	movl	$2, %edx
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	.L1443
	.p2align 4,,10
	.p2align 3
.L1451:
	movl	$8, %esi
	movl	$2, %edx
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L1454:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1447:
	movzwl	(%r14,%rdx,2), %esi
	movw	%si, (%rax,%rdx,2)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L1447
	jmp	.L1450
.L1466:
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	jmp	.L1445
.L1464:
	leaq	.LC10(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1467:
	movl	$1073741823, %edx
	cmpq	$1073741823, %r15
	movq	%rdx, %rdi
	cmovbe	%r15, %rdi
	leaq	(%rdi,%rdi), %rdx
	leaq	7(%rdx), %rsi
	andq	$-8, %rsi
	jmp	.L1442
	.cfi_endproc
.LFE21481:
	.size	_ZNSt6vectorItN2v88internal13ZoneAllocatorItEEE12emplace_backIJtEEEvDpOT_, .-_ZNSt6vectorItN2v88internal13ZoneAllocatorItEEE12emplace_backIJtEEEvDpOT_
	.section	.rodata._ZN2v88internal12RegExpParser21ParseCaptureGroupNameEv.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"Invalid Unicode escape sequence"
	.section	.rodata._ZN2v88internal12RegExpParser21ParseCaptureGroupNameEv.str1.1,"aMS",@progbits,1
.LC12:
	.string	"Invalid capture group name"
	.section	.text._ZN2v88internal12RegExpParser21ParseCaptureGroupNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser21ParseCaptureGroupNameEv
	.type	_ZN2v88internal12RegExpParser21ParseCaptureGroupNameEv, @function
_ZN2v88internal12RegExpParser21ParseCaptureGroupNameEv:
.LFB18388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%r12), %rbx
	movq	24(%r12), %rax
	subq	%rbx, %rax
	cmpq	$31, %rax
	jbe	.L1546
	leaq	32(%rbx), %rax
	movq	%rax, 16(%r12)
.L1470:
	leaq	-144(%rbp), %rax
	movq	%r12, (%rbx)
	movl	$1, %r13d
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r12
	movq	%rax, -200(%rbp)
	leaq	-160(%rbp), %rax
	movq	%rbx, -184(%rbp)
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	%rax, -192(%rbp)
	jmp	.L1507
	.p2align 4,,10
	.p2align 3
.L1550:
	cmpl	$127, %r14d
	ja	.L1547
	movslq	%r14d, %r8
	testb	$1, (%r12,%r8)
	je	.L1497
	movl	-164(%rbp), %r13d
.L1509:
	leaq	-80(%rbp), %rsi
	movq	%rbx, %rdi
	movw	%r13w, -80(%rbp)
	call	_ZNSt6vectorItN2v88internal13ZoneAllocatorItEEE12emplace_backIJtEEEvDpOT_
.L1499:
	xorl	%r13d, %r13d
.L1507:
	movl	56(%r15), %r14d
	movq	48(%r15), %rax
	movl	%r14d, -164(%rbp)
	movl	36(%rax), %ecx
	cmpl	%ecx, 64(%r15)
	jl	.L1548
	movl	$2097152, 56(%r15)
	movl	36(%rax), %eax
	movb	$0, 76(%r15)
	addl	$1, %eax
	movl	%eax, 64(%r15)
.L1476:
	cmpl	$92, %r14d
	je	.L1549
.L1483:
	testb	%r13b, %r13b
	jne	.L1550
	cmpl	$62, %r14d
	je	.L1468
	cmpl	$127, %r14d
	ja	.L1551
	movslq	%r14d, %rax
	testb	$2, (%r12,%rax)
	je	.L1503
.L1504:
	leaq	-80(%rbp), %rsi
	movq	%rbx, %rdi
	movw	%r14w, -80(%rbp)
	call	_ZNSt6vectorItN2v88internal13ZoneAllocatorItEEE12emplace_backIJtEEEvDpOT_
	jmp	.L1499
	.p2align 4,,10
	.p2align 3
.L1549:
	cmpl	$117, 56(%r15)
	je	.L1484
.L1490:
	cmpb	$0, 81(%r15)
	jne	.L1489
	leaq	.LC12(%rip), %rax
	movb	$1, 81(%r15)
	movq	(%r15), %rdi
	leaq	-112(%rbp), %rsi
	movq	%rax, -112(%rbp)
	movq	$26, -104(%rbp)
.L1543:
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r15), %rdx
	testq	%rax, %rax
	je	.L1479
	movq	%rax, (%rdx)
	movq	48(%r15), %rax
	movl	$2097152, 56(%r15)
	movl	36(%rax), %eax
	movl	%eax, 64(%r15)
	.p2align 4,,10
	.p2align 3
.L1489:
	movq	$0, -184(%rbp)
.L1468:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1552
	movq	-184(%rbp), %rax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1548:
	.cfi_restore_state
	movq	(%r15), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r14), %rax
	jnb	.L1472
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L1553
	movl	$216, %edi
	call	_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE@PLT
	cmpb	$0, 81(%r15)
	movq	%rax, %r14
	jne	.L1538
	movq	%rax, %rdi
	call	strlen@PLT
	movb	$1, 81(%r15)
	movq	(%r15), %rdi
	xorl	%edx, %edx
	movq	-192(%rbp), %rsi
	cltq
	movq	%r14, -160(%rbp)
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r15), %rdx
	testq	%rax, %rax
	je	.L1479
.L1541:
	movq	%rax, (%rdx)
	movq	48(%r15), %rax
	movl	$2097152, 56(%r15)
	movl	36(%rax), %eax
	movl	%eax, 64(%r15)
.L1538:
	movl	-164(%rbp), %r14d
	jmp	.L1476
	.p2align 4,,10
	.p2align 3
.L1472:
	movq	8(%r15), %rax
	cmpq	$268435456, 8(%rax)
	jbe	.L1477
	cmpb	$0, 81(%r15)
	jne	.L1538
	movb	$1, 81(%r15)
	movq	(%r15), %rdi
	leaq	.LC3(%rip), %rax
	xorl	%edx, %edx
	movq	-200(%rbp), %rsi
	movq	%rax, -144(%rbp)
	movq	$28, -136(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r15), %rdx
	testq	%rax, %rax
	jne	.L1541
.L1479:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1547:
	movl	%r14d, %edi
	call	_ZN2v88internal21IsIdentifierStartSlowEi@PLT
	testb	%al, %al
	je	.L1497
	movl	-164(%rbp), %r13d
	cmpl	$65535, %r13d
	jbe	.L1509
	leal	-65536(%r13), %edx
	leaq	-80(%rbp), %r14
	movq	%rbx, %rdi
	shrl	$10, %edx
	movq	%r14, %rsi
	andw	$1023, %dx
	subw	$10240, %dx
	movw	%dx, -80(%rbp)
	call	_ZNSt6vectorItN2v88internal13ZoneAllocatorItEEE12emplace_backIJtEEEvDpOT_
	movl	%r13d, %eax
	movq	%r14, %rsi
	movq	%rbx, %rdi
	andw	$1023, %ax
	subw	$9216, %ax
	movw	%ax, -80(%rbp)
	call	_ZNSt6vectorItN2v88internal13ZoneAllocatorItEEE12emplace_backIJtEEEvDpOT_
	jmp	.L1499
	.p2align 4,,10
	.p2align 3
.L1551:
	movl	%r14d, %edi
	call	_ZN2v88internal20IsIdentifierPartSlowEi@PLT
	testb	%al, %al
	je	.L1503
	movl	-164(%rbp), %r14d
	cmpl	$65535, %r14d
	jbe	.L1504
	leal	-65536(%r14), %eax
	leaq	-80(%rbp), %r13
	movq	%rbx, %rdi
	shrl	$10, %eax
	movq	%r13, %rsi
	andw	$1023, %ax
	subw	$10240, %ax
	movw	%ax, -80(%rbp)
	call	_ZNSt6vectorItN2v88internal13ZoneAllocatorItEEE12emplace_backIJtEEEvDpOT_
	movl	%r14d, %r8d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	andw	$1023, %r8w
	subw	$9216, %r8w
	movw	%r8w, -80(%rbp)
	call	_ZNSt6vectorItN2v88internal13ZoneAllocatorItEEE12emplace_backIJtEEEvDpOT_
	jmp	.L1499
	.p2align 4,,10
	.p2align 3
.L1497:
	cmpb	$0, 81(%r15)
	jne	.L1489
	leaq	.LC12(%rip), %rax
	movb	$1, 81(%r15)
	movq	(%r15), %rdi
	leaq	-96(%rbp), %rsi
	movq	%rax, -96(%rbp)
	movq	$26, -88(%rbp)
	jmp	.L1543
	.p2align 4,,10
	.p2align 3
.L1503:
	cmpb	$0, 81(%r15)
	jne	.L1489
	leaq	.LC12(%rip), %rax
	movb	$1, 81(%r15)
	movq	(%r15), %rdi
	leaq	-80(%rbp), %rsi
	movq	%rax, -80(%rbp)
	movq	$26, -72(%rbp)
	jmp	.L1543
	.p2align 4,,10
	.p2align 3
.L1477:
	movq	48(%r15), %rax
	movslq	64(%r15), %rsi
	movzbl	32(%rax), %r8d
	movq	40(%rax), %r10
	movq	%rsi, %rdx
	testb	%r8b, %r8b
	je	.L1480
	movzbl	(%r10,%rsi), %edi
.L1481:
	leal	1(%rdx), %esi
	testb	$16, 60(%r15)
	je	.L1482
	cmpl	36(%rax), %esi
	jge	.L1482
	movl	%edi, %eax
	andl	$64512, %eax
	cmpl	$55296, %eax
	jne	.L1482
	testb	%r8b, %r8b
	jne	.L1482
	movslq	%esi, %rax
	movzwl	(%r10,%rax,2), %eax
	movl	%eax, %r8d
	andw	$-1024, %r8w
	cmpw	$-9216, %r8w
	jne	.L1482
	sall	$10, %edi
	andl	$1023, %eax
	leal	2(%rdx), %esi
	andl	$1047552, %edi
	orl	%eax, %edi
	addl	$65536, %edi
	.p2align 4,,10
	.p2align 3
.L1482:
	movl	%esi, 64(%r15)
	movl	%edi, 56(%r15)
	jmp	.L1538
	.p2align 4,,10
	.p2align 3
.L1484:
	movq	%r15, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	leaq	-164(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12RegExpParser18ParseUnicodeEscapeEPi
	testb	%al, %al
	je	.L1554
	movl	-164(%rbp), %r14d
	cmpl	$92, %r14d
	jne	.L1483
	jmp	.L1490
	.p2align 4,,10
	.p2align 3
.L1480:
	movzwl	(%r10,%rsi,2), %edi
	jmp	.L1481
.L1546:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L1470
.L1554:
	cmpb	$0, 81(%r15)
	jne	.L1489
	leaq	.LC11(%rip), %rax
	movb	$1, 81(%r15)
	movq	(%r15), %rdi
	leaq	-128(%rbp), %rsi
	movq	%rax, -128(%rbp)
	movq	$31, -120(%rbp)
	jmp	.L1543
.L1553:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1552:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18388:
	.size	_ZN2v88internal12RegExpParser21ParseCaptureGroupNameEv, .-_ZN2v88internal12RegExpParser21ParseCaptureGroupNameEv
	.section	.rodata._ZN2v88internal12RegExpParser20ParseOpenParenthesisEPNS1_17RegExpParserStateE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"Invalid group"
.LC14:
	.string	"Multiple dashes in flag group"
.LC15:
	.string	"Repeated flag in flag group"
.LC16:
	.string	"Invalid flag group"
.LC17:
	.string	"Too many captures"
	.section	.text._ZN2v88internal12RegExpParser20ParseOpenParenthesisEPNS1_17RegExpParserStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser20ParseOpenParenthesisEPNS1_17RegExpParserStateE
	.type	_ZN2v88internal12RegExpParser20ParseOpenParenthesisEPNS1_17RegExpParserStateE, @function
_ZN2v88internal12RegExpParser20ParseOpenParenthesisEPNS1_17RegExpParserStateE:
.LFB18380:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movl	20(%rsi), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	48(%rdi), %rax
	movl	36(%rax), %edi
	cmpl	%edi, 64(%r12)
	jl	.L1688
	movl	$2097152, 56(%r12)
	movl	36(%rax), %eax
	movb	$0, 76(%r12)
	addl	$1, %eax
	movl	%eax, 64(%r12)
.L1562:
	movl	68(%r12), %eax
	cmpl	$65535, %eax
	jg	.L1611
	addl	$1, %eax
	movl	$-1, %edx
	movl	$1, %r9d
	xorl	%ecx, %ecx
	movl	%eax, 68(%r12)
	xorl	%eax, %eax
.L1581:
	movq	8(%r15), %rsi
	movq	8(%r12), %r14
	orl	12(%rsi), %eax
	movq	16(%r14), %r13
	andl	%eax, %edx
	movq	24(%r14), %rax
	subq	%r13, %rax
	cmpq	$39, %rax
	jbe	.L1689
	leaq	40(%r13), %rax
	movq	%rax, 16(%r14)
.L1614:
	movl	68(%r12), %r12d
	movq	%r15, 0(%r13)
	movq	%r13, %r8
	movq	16(%r14), %rax
	movq	24(%r14), %rsi
	subq	%rax, %rsi
	cmpq	$79, %rsi
	jbe	.L1690
	leaq	80(%rax), %rsi
	movq	%rsi, 16(%r14)
.L1616:
	pxor	%xmm0, %xmm0
	movl	%edx, 12(%rax)
	xorl	%edx, %edx
	movq	%r14, (%rax)
	movb	$0, 8(%rax)
	movq	$0, 16(%rax)
	movw	%dx, 24(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movq	%rax, 8(%r13)
	movl	%r9d, 16(%r13)
	movl	%ebx, 20(%r13)
	movl	%r12d, 24(%r13)
	movq	%rcx, 32(%r13)
.L1555:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1691
	addq	$184, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1611:
	.cfi_restore_state
	cmpb	$0, 81(%r12)
	je	.L1692
.L1625:
	xorl	%r8d, %r8d
	jmp	.L1555
	.p2align 4,,10
	.p2align 3
.L1688:
	movq	(%r12), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r13), %rax
	jnb	.L1557
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L1693
	movl	$216, %edi
	call	_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE@PLT
	cmpb	$0, 81(%r12)
	movq	%rax, %r13
	jne	.L1678
	movq	%rax, %rdi
	call	strlen@PLT
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-192(%rbp), %rsi
	cltq
	movq	%r13, -192(%rbp)
	movq	%rax, -184(%rbp)
.L1680:
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r12), %rdx
	testq	%rax, %rax
	je	.L1565
	movq	%rax, (%rdx)
	movq	48(%r12), %rax
	movl	$2097152, 56(%r12)
	movl	36(%rax), %eax
	movl	%eax, 64(%r12)
	jmp	.L1562
	.p2align 4,,10
	.p2align 3
.L1557:
	movq	8(%r12), %rax
	cmpq	$268435456, 8(%rax)
	jbe	.L1563
	cmpb	$0, 81(%r12)
	je	.L1564
.L1678:
	movl	56(%r12), %eax
.L1560:
	cmpl	$63, %eax
	jne	.L1562
	movq	48(%r12), %rdx
	movslq	64(%r12), %rax
	movl	36(%rdx), %esi
	cmpl	%eax, %esi
	jle	.L1569
	cmpb	$0, 32(%rdx)
	movq	40(%rdx), %rcx
	leal	1(%rax), %edi
	je	.L1570
	movzbl	(%rcx,%rax), %ecx
.L1571:
	cmpl	$61, %ecx
	jg	.L1572
	cmpl	$32, %ecx
	jle	.L1569
	subl	$33, %ecx
	cmpl	$28, %ecx
	ja	.L1569
	leaq	.L1576(%rip), %rdx
	movslq	(%rdx,%rcx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal12RegExpParser20ParseOpenParenthesisEPNS1_17RegExpParserStateE,"a",@progbits
	.align 4
	.align 4
.L1576:
	.long	.L1579-.L1576
	.long	.L1569-.L1576
	.long	.L1569-.L1576
	.long	.L1569-.L1576
	.long	.L1569-.L1576
	.long	.L1569-.L1576
	.long	.L1569-.L1576
	.long	.L1569-.L1576
	.long	.L1569-.L1576
	.long	.L1569-.L1576
	.long	.L1569-.L1576
	.long	.L1569-.L1576
	.long	.L1574-.L1576
	.long	.L1569-.L1576
	.long	.L1569-.L1576
	.long	.L1569-.L1576
	.long	.L1569-.L1576
	.long	.L1569-.L1576
	.long	.L1569-.L1576
	.long	.L1569-.L1576
	.long	.L1569-.L1576
	.long	.L1569-.L1576
	.long	.L1569-.L1576
	.long	.L1569-.L1576
	.long	.L1569-.L1576
	.long	.L1578-.L1576
	.long	.L1569-.L1576
	.long	.L1577-.L1576
	.long	.L1575-.L1576
	.section	.text._ZN2v88internal12RegExpParser20ParseOpenParenthesisEPNS1_17RegExpParserStateE
	.p2align 4,,10
	.p2align 3
.L1570:
	movzwl	(%rcx,%rax,2), %ecx
	movl	%ecx, %eax
	testb	$16, 60(%r12)
	je	.L1571
	cmpl	%edi, %esi
	jle	.L1571
	andw	$-1024, %ax
	cmpw	$-10240, %ax
	jne	.L1571
.L1569:
	cmpb	$0, 81(%r12)
	jne	.L1625
	leaq	.LC13(%rip), %rax
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-96(%rbp), %rsi
	movq	%rax, -96(%rbp)
	movq	$13, -88(%rbp)
.L1683:
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r12), %rdx
	testq	%rax, %rax
	je	.L1565
	movq	%rax, (%rdx)
	movq	48(%r12), %rax
	movl	$2097152, 56(%r12)
	movl	36(%rax), %eax
	movl	%eax, 64(%r12)
	jmp	.L1625
	.p2align 4,,10
	.p2align 3
.L1572:
	subl	$105, %ecx
	cmpl	$10, %ecx
	ja	.L1569
	movl	$1, %eax
	salq	%cl, %rax
	testl	$1041, %eax
	je	.L1569
.L1574:
	movzbl	_ZN2v88internal26FLAG_regexp_mode_modifiersE(%rip), %r14d
	testb	%r14b, %r14b
	je	.L1694
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	xorl	%eax, %eax
	movl	$32, %esi
.L1585:
	movl	56(%r12), %edx
	cmpl	$58, %edx
	je	.L1586
.L1696:
	jg	.L1587
	cmpl	$41, %edx
	je	.L1588
	cmpl	$45, %edx
	jne	.L1590
	testb	%r14b, %r14b
	je	.L1695
	movq	%r12, %rdi
	movl	%eax, -200(%rbp)
	xorl	%r14d, %r14d
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	56(%r12), %edx
	movl	-200(%rbp), %eax
	movl	$32, %esi
	cmpl	$58, %edx
	jne	.L1696
	.p2align 4,,10
	.p2align 3
.L1586:
	movq	%r12, %rdi
	movl	%eax, -200(%rbp)
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	%r13d, %edx
	movl	-200(%rbp), %eax
	xorl	%ecx, %ecx
	notl	%edx
	movl	$4, %r9d
	jmp	.L1581
	.p2align 4,,10
	.p2align 3
.L1563:
	movq	48(%r12), %rdx
	movslq	64(%r12), %rax
	movzbl	32(%rdx), %edi
	movq	40(%rdx), %r8
	movq	%rax, %rcx
	testb	%dil, %dil
	jne	.L1697
	movzwl	(%r8,%rax,2), %eax
.L1567:
	leal	1(%rcx), %esi
	testb	$16, 60(%r12)
	je	.L1568
	cmpl	36(%rdx), %esi
	jge	.L1568
	movl	%eax, %edx
	andl	$64512, %edx
	cmpl	$55296, %edx
	jne	.L1568
	testb	%dil, %dil
	je	.L1698
	.p2align 4,,10
	.p2align 3
.L1568:
	movl	%esi, 64(%r12)
	movl	%eax, 56(%r12)
	jmp	.L1560
	.p2align 4,,10
	.p2align 3
.L1690:
	movl	$80, %esi
	movq	%r14, %rdi
	movl	%r9d, -208(%rbp)
	movl	%edx, -204(%rbp)
	movq	%rcx, -200(%rbp)
	movq	%r13, -216(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-200(%rbp), %rcx
	movl	-204(%rbp), %edx
	movl	-208(%rbp), %r9d
	movq	-216(%rbp), %r8
	jmp	.L1616
	.p2align 4,,10
	.p2align 3
.L1689:
	movl	$40, %esi
	movq	%r14, %rdi
	movl	%r9d, -208(%rbp)
	movl	%edx, -204(%rbp)
	movq	%rcx, -200(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-200(%rbp), %rcx
	movl	-204(%rbp), %edx
	movl	-208(%rbp), %r9d
	movq	%rax, %r13
	jmp	.L1614
.L1579:
	movl	%edi, 64(%r12)
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	$-1, %edx
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	movl	$3, %r9d
	jmp	.L1581
.L1575:
	movl	%edi, 64(%r12)
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	$-1, %edx
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	movl	$2, %r9d
	jmp	.L1581
.L1577:
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movq	48(%r12), %rax
	movl	64(%r12), %edx
	movl	36(%rax), %esi
	cmpl	%esi, %edx
	jge	.L1601
	movl	60(%r12), %ecx
	movslq	%edx, %r8
	addl	$1, %edx
	movq	40(%rax), %rdi
	shrl	$4, %ecx
	cmpl	%edx, %esi
	setg	%sil
	andl	%esi, %ecx
	cmpb	$0, 32(%rax)
	je	.L1602
	cmpb	$61, (%rdi,%r8)
	je	.L1603
	movzbl	(%rdi,%r8), %eax
.L1617:
	cmpl	$33, %eax
	je	.L1699
.L1601:
	movb	$1, 80(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	68(%r12), %eax
	cmpl	$65535, %eax
	jg	.L1611
	addl	$1, %eax
	movq	%r12, %rdi
	movl	%eax, 68(%r12)
	call	_ZN2v88internal12RegExpParser21ParseCaptureGroupNameEv
	cmpb	$0, 81(%r12)
	movq	%rax, %rcx
	jne	.L1625
	movl	$-1, %edx
	xorl	%eax, %eax
	movl	$1, %r9d
	jmp	.L1581
.L1578:
	movl	%edi, 64(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	$-1, %edx
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	movl	$4, %r9d
	jmp	.L1581
	.p2align 4,,10
	.p2align 3
.L1692:
	leaq	.LC17(%rip), %rax
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-80(%rbp), %rsi
	movq	%rax, -80(%rbp)
	movq	$17, -72(%rbp)
	jmp	.L1683
	.p2align 4,,10
	.p2align 3
.L1697:
	movzbl	(%r8,%rax), %eax
	jmp	.L1567
	.p2align 4,,10
	.p2align 3
.L1564:
	leaq	.LC3(%rip), %rax
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-176(%rbp), %rsi
	movq	%rax, -176(%rbp)
	movq	$28, -168(%rbp)
	jmp	.L1680
.L1694:
	cmpb	$0, 81(%r12)
	jne	.L1625
	leaq	.LC13(%rip), %rax
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-160(%rbp), %rsi
	movq	%rax, -160(%rbp)
	movq	$13, -152(%rbp)
	jmp	.L1683
	.p2align 4,,10
	.p2align 3
.L1587:
	movl	$32, %ecx
	cmpl	$115, %edx
	jne	.L1700
.L1591:
	movl	%r13d, %edx
	orl	%eax, %edx
	testl	%ecx, %edx
	jne	.L1701
	testb	%r14b, %r14b
	je	.L1598
	orl	%ecx, %eax
.L1599:
	movq	%r12, %rdi
	movl	%eax, -200(%rbp)
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	-200(%rbp), %eax
	movl	$32, %esi
	jmp	.L1585
	.p2align 4,,10
	.p2align 3
.L1700:
	movl	%edx, %ecx
	andl	$-5, %ecx
	cmpl	$105, %ecx
	jne	.L1590
	movl	$2, %ecx
	cmpl	$105, %edx
	je	.L1591
	cmpl	$109, %edx
	jne	.L1702
	movl	$4, %ecx
	jmp	.L1591
	.p2align 4,,10
	.p2align 3
.L1565:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1602:
	movzwl	(%rdi,%r8,2), %eax
	movl	%eax, %esi
	testb	%cl, %cl
	jne	.L1703
	cmpl	$61, %eax
	je	.L1603
	movzwl	(%rdi,%r8,2), %eax
	jmp	.L1617
	.p2align 4,,10
	.p2align 3
.L1590:
	cmpb	$0, 81(%r12)
	jne	.L1625
	leaq	.LC16(%rip), %rax
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-112(%rbp), %rsi
	movq	%rax, -112(%rbp)
	movq	$18, -104(%rbp)
	jmp	.L1683
	.p2align 4,,10
	.p2align 3
.L1588:
	movq	%r12, %rdi
	movl	%eax, -200(%rbp)
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movq	8(%r15), %rdi
	call	_ZN2v88internal13RegExpBuilder9FlushTextEv
	movq	8(%r15), %rcx
	movl	%r13d, %edx
	movq	%r15, %r8
	movl	-200(%rbp), %eax
	notl	%edx
	orl	12(%rcx), %eax
	andl	%edx, %eax
	movl	%eax, 12(%rcx)
	jmp	.L1555
.L1703:
	andw	$-1024, %si
	cmpw	$-10240, %si
	je	.L1601
	cmpl	$61, %eax
	jne	.L1617
.L1603:
	movl	%edx, 64(%r12)
	movq	%r12, %rdi
	movl	$1, %ebx
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	$-1, %edx
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	movl	$2, %r9d
	jmp	.L1581
.L1699:
	movl	%edx, 64(%r12)
	movq	%r12, %rdi
	movl	$1, %ebx
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	$-1, %edx
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	movl	$3, %r9d
	jmp	.L1581
.L1598:
	orl	%ecx, %r13d
	jmp	.L1599
.L1698:
	movslq	%esi, %rdx
	movzwl	(%r8,%rdx,2), %edx
	movl	%edx, %edi
	andw	$-1024, %di
	cmpw	$-9216, %di
	jne	.L1568
	sall	$10, %eax
	andl	$1023, %edx
	leal	2(%rcx), %esi
	andl	$1047552, %eax
	orl	%edx, %eax
	addl	$65536, %eax
	jmp	.L1568
.L1695:
	cmpb	$0, 81(%r12)
	jne	.L1625
	leaq	.LC14(%rip), %rax
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-144(%rbp), %rsi
	movq	%rax, -144(%rbp)
	movq	$29, -136(%rbp)
	jmp	.L1683
.L1701:
	cmpb	$0, 81(%r12)
	jne	.L1625
	leaq	.LC15(%rip), %rax
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-128(%rbp), %rsi
	movq	%rax, -128(%rbp)
	movq	$27, -120(%rbp)
	jmp	.L1683
.L1691:
	call	__stack_chk_fail@PLT
.L1702:
	cmpl	$115, %edx
	movl	$16, %ecx
	cmove	%esi, %ecx
	jmp	.L1591
.L1693:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18380:
	.size	_ZN2v88internal12RegExpParser20ParseOpenParenthesisEPNS1_17RegExpParserStateE, .-_ZN2v88internal12RegExpParser20ParseOpenParenthesisEPNS1_17RegExpParserStateE
	.section	.rodata._ZN2v88internal12RegExpParser23ParseNamedBackReferenceEPNS0_13RegExpBuilderEPNS1_17RegExpParserStateE.str1.1,"aMS",@progbits,1
.LC18:
	.string	"Invalid named reference"
	.section	.text._ZN2v88internal12RegExpParser23ParseNamedBackReferenceEPNS0_13RegExpBuilderEPNS1_17RegExpParserStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser23ParseNamedBackReferenceEPNS0_13RegExpBuilderEPNS1_17RegExpParserStateE
	.type	_ZN2v88internal12RegExpParser23ParseNamedBackReferenceEPNS0_13RegExpBuilderEPNS1_17RegExpParserStateE, @function
_ZN2v88internal12RegExpParser23ParseNamedBackReferenceEPNS0_13RegExpBuilderEPNS1_17RegExpParserStateE:
.LFB18390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$60, 56(%rdi)
	je	.L1705
	cmpb	$0, 81(%rdi)
	je	.L1758
	xorl	%eax, %eax
.L1704:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1759
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1705:
	.cfi_restore_state
	movq	%rsi, %r13
	movq	%rdx, %rbx
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser21ParseCaptureGroupNameEv
	movq	%rax, %r14
	xorl	%eax, %eax
	testq	%r14, %r14
	je	.L1704
	testq	%rbx, %rbx
	jne	.L1712
	.p2align 4,,10
	.p2align 3
.L1764:
	movq	8(%r12), %rdi
	movl	12(%r13), %r15d
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$31, %rax
	jbe	.L1760
	leaq	32(%rbx), %rax
	movq	%rax, 16(%rdi)
.L1713:
	leaq	16+_ZTVN2v88internal19RegExpBackReferenceE(%rip), %rax
	movq	$0, 8(%rbx)
	movq	%rbx, %rdi
	movq	%rax, (%rbx)
	movl	%r15d, 24(%rbx)
	movq	%r14, 16(%rbx)
	call	_ZN2v88internal10RegExpTree7IsEmptyEv@PLT
	testb	%al, %al
	jne	.L1761
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder9FlushTextEv
	cmpq	$0, 40(%r13)
	je	.L1716
	movq	32(%r13), %r14
	movq	0(%r13), %r15
	testq	%r14, %r14
	je	.L1762
.L1717:
	movslq	12(%r14), %rax
	movl	8(%r14), %edx
	cmpl	%edx, %eax
	jge	.L1722
	leal	1(%rax), %ecx
	movq	(%r14), %rdx
	movl	%ecx, 12(%r14)
	movq	40(%r13), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L1716:
	movq	%rbx, 40(%r13)
.L1715:
	movq	40(%r12), %r13
	movq	8(%r12), %r14
	testq	%r13, %r13
	je	.L1763
.L1726:
	movslq	12(%r13), %rax
	movl	8(%r13), %edx
	cmpl	%edx, %eax
	jge	.L1731
	movq	0(%r13), %rdx
	leal	1(%rax), %ecx
	movl	%ecx, 12(%r13)
	movq	%rbx, (%rdx,%rax,8)
	movl	$1, %eax
	jmp	.L1704
	.p2align 4,,10
	.p2align 3
.L1765:
	call	memcmp@PLT
	testl	%eax, %eax
	je	.L1711
	.p2align 4,,10
	.p2align 3
.L1710:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1764
.L1712:
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.L1710
	movq	8(%rax), %rdi
	movq	16(%rax), %rdx
	movq	8(%r14), %rsi
	movq	16(%r14), %rax
	subq	%rdi, %rdx
	subq	%rsi, %rax
	cmpq	%rax, %rdx
	jne	.L1710
	testq	%rdx, %rdx
	jne	.L1765
.L1711:
	movb	$1, 8(%r13)
	movl	$1, %eax
	jmp	.L1704
	.p2align 4,,10
	.p2align 3
.L1758:
	movb	$1, 81(%rdi)
	movq	(%rdi), %rdi
	leaq	.LC18(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%rax, -80(%rbp)
	movq	$23, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r12), %rdx
	testq	%rax, %rax
	je	.L1766
	movq	%rax, (%rdx)
	movq	48(%r12), %rax
	movl	$2097152, 56(%r12)
	movl	36(%rax), %eax
	movl	%eax, 64(%r12)
	xorl	%eax, %eax
	jmp	.L1704
.L1762:
	movq	24(%r15), %rax
	movq	16(%r15), %rdx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	cmpq	$15, %rcx
	jbe	.L1767
	leaq	16(%rdx), %rcx
	movq	%rcx, 16(%r15)
.L1719:
	subq	%rcx, %rax
	movq	%rdx, %r14
	cmpq	$15, %rax
	jbe	.L1768
	leaq	16(%rcx), %rax
	movq	%rax, 16(%r15)
.L1721:
	movq	%rcx, (%rdx)
	movq	$2, 8(%rdx)
	movq	%rdx, 32(%r13)
	jmp	.L1717
	.p2align 4,,10
	.p2align 3
.L1761:
	movb	$1, 8(%r13)
	jmp	.L1715
	.p2align 4,,10
	.p2align 3
.L1731:
	movq	16(%r14), %rcx
	movq	24(%r14), %rax
	leal	1(%rdx,%rdx), %r12d
	movslq	%r12d, %rsi
	salq	$3, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L1769
	addq	%rcx, %rsi
	movq	%rsi, 16(%r14)
.L1733:
	movslq	12(%r13), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L1734
	movq	0(%r13), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%r13), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$3, %rdx
.L1734:
	addl	$1, %eax
	movq	%rcx, 0(%r13)
	movl	%eax, 12(%r13)
	movl	$1, %eax
	movl	%r12d, 8(%r13)
	movq	%rbx, (%rcx,%rdx)
	jmp	.L1704
	.p2align 4,,10
	.p2align 3
.L1766:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1763:
	movq	24(%r14), %rax
	movq	16(%r14), %r15
	movq	%rax, %rdx
	subq	%r15, %rdx
	cmpq	$15, %rdx
	jbe	.L1770
	leaq	16(%r15), %rdx
	movq	%rdx, 16(%r14)
.L1728:
	subq	%rdx, %rax
	movq	%r15, %r13
	cmpq	$7, %rax
	jbe	.L1771
	leaq	8(%rdx), %rax
	movq	%rax, 16(%r14)
.L1730:
	movq	%rdx, (%r15)
	movq	$1, 8(%r15)
	movq	8(%r12), %r14
	movq	%r15, 40(%r12)
	jmp	.L1726
	.p2align 4,,10
	.p2align 3
.L1760:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L1713
.L1722:
	movq	16(%r15), %rdi
	movq	24(%r15), %rax
	leal	1(%rdx,%rdx), %r8d
	movslq	%r8d, %rsi
	movq	40(%r13), %rcx
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1772
	addq	%rdi, %rsi
	movq	%rsi, 16(%r15)
.L1724:
	movslq	12(%r14), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L1725
	movq	(%r14), %rsi
	movl	%r8d, -92(%rbp)
	movq	%rcx, -88(%rbp)
	call	memcpy@PLT
	movslq	12(%r14), %rdx
	movl	-92(%rbp), %r8d
	movq	-88(%rbp), %rcx
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$3, %rdx
.L1725:
	addl	$1, %eax
	movq	%rdi, (%r14)
	movl	%r8d, 8(%r14)
	movl	%eax, 12(%r14)
	movq	%rcx, (%rdi,%rdx)
	jmp	.L1716
.L1769:
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L1733
.L1771:
	movl	$8, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L1730
.L1770:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	16(%r14), %rdx
	movq	%rax, %r15
	movq	24(%r14), %rax
	jmp	.L1728
.L1772:
	movq	%r15, %rdi
	movl	%r8d, -92(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %rcx
	movl	-92(%rbp), %r8d
	movq	%rax, %rdi
	jmp	.L1724
.L1768:
	movl	$16, %esi
	movq	%r15, %rdi
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L1721
.L1767:
	movl	$16, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	16(%r15), %rcx
	movq	%rax, %rdx
	movq	24(%r15), %rax
	jmp	.L1719
.L1759:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18390:
	.size	_ZN2v88internal12RegExpParser23ParseNamedBackReferenceEPNS0_13RegExpBuilderEPNS1_17RegExpParserStateE, .-_ZN2v88internal12RegExpParser23ParseNamedBackReferenceEPNS0_13RegExpBuilderEPNS1_17RegExpParserStateE
	.section	.text._ZNSt8_Rb_treeIPN2v88internal13RegExpCaptureES3_St9_IdentityIS3_ENS1_12RegExpParser21RegExpCaptureNameLessENS1_13ZoneAllocatorIS3_EEE4findERKS3_,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal13RegExpCaptureES3_St9_IdentityIS3_ENS1_12RegExpParser21RegExpCaptureNameLessENS1_13ZoneAllocatorIS3_EEE4findERKS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal13RegExpCaptureES3_St9_IdentityIS3_ENS1_12RegExpParser21RegExpCaptureNameLessENS1_13ZoneAllocatorIS3_EEE4findERKS3_
	.type	_ZNSt8_Rb_treeIPN2v88internal13RegExpCaptureES3_St9_IdentityIS3_ENS1_12RegExpParser21RegExpCaptureNameLessENS1_13ZoneAllocatorIS3_EEE4findERKS3_, @function
_ZNSt8_Rb_treeIPN2v88internal13RegExpCaptureES3_St9_IdentityIS3_ENS1_12RegExpParser21RegExpCaptureNameLessENS1_13ZoneAllocatorIS3_EEE4findERKS3_:
.LFB21501:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	leaq	16(%rdi), %r11
	testq	%rax, %rax
	je	.L1788
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r11, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	(%rsi), %rdx
	movq	24(%rdx), %rdx
	movq	16(%rdx), %rdi
	movq	8(%rdx), %rdx
	movq	%rdi, %r10
	subq	%rdx, %r10
	.p2align 4,,10
	.p2align 3
.L1775:
	movq	32(%rax), %rcx
	movq	24(%rcx), %rcx
	movq	16(%rcx), %rsi
	movq	8(%rcx), %rcx
	movq	%rsi, %rbx
	leaq	(%rcx,%r10), %r8
	subq	%rcx, %rbx
	cmpq	%r10, %rbx
	cmovg	%r8, %rsi
	movq	%rdx, %r8
	cmpq	%rsi, %rcx
	jne	.L1782
	jmp	.L1778
	.p2align 4,,10
	.p2align 3
.L1793:
	ja	.L1781
	addq	$2, %rcx
	addq	$2, %r8
	cmpq	%rcx, %rsi
	je	.L1778
.L1782:
	movzwl	(%r8), %ebx
	cmpw	%bx, (%rcx)
	jnb	.L1793
.L1779:
	movq	24(%rax), %rax
.L1783:
	testq	%rax, %rax
	jne	.L1775
	cmpq	%r9, %r11
	je	.L1774
	movq	32(%r9), %rax
	movq	24(%rax), %rax
	movq	16(%rax), %rsi
	movq	8(%rax), %rax
	movq	%rsi, %rcx
	subq	%rax, %rcx
	leaq	(%rdx,%rcx), %r8
	cmpq	%rcx, %r10
	cmovg	%r8, %rdi
	cmpq	%rdi, %rdx
	jne	.L1787
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L1794:
	ja	.L1774
	addq	$2, %rdx
	addq	$2, %rax
	cmpq	%rdx, %rdi
	je	.L1785
.L1787:
	movzwl	(%rax), %ebx
	cmpw	%bx, (%rdx)
	jnb	.L1794
	movq	%r11, %r9
.L1774:
	movq	%r9, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1778:
	.cfi_restore_state
	cmpq	%r8, %rdi
	jne	.L1779
.L1781:
	movq	%rax, %r9
	movq	16(%rax), %rax
	jmp	.L1783
	.p2align 4,,10
	.p2align 3
.L1785:
	cmpq	%rax, %rsi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmovne	%r11, %r9
	movq	%r9, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1788:
	.cfi_restore 3
	.cfi_restore 6
	movq	%r11, %rax
	ret
	.cfi_endproc
.LFE21501:
	.size	_ZNSt8_Rb_treeIPN2v88internal13RegExpCaptureES3_St9_IdentityIS3_ENS1_12RegExpParser21RegExpCaptureNameLessENS1_13ZoneAllocatorIS3_EEE4findERKS3_, .-_ZNSt8_Rb_treeIPN2v88internal13RegExpCaptureES3_St9_IdentityIS3_ENS1_12RegExpParser21RegExpCaptureNameLessENS1_13ZoneAllocatorIS3_EEE4findERKS3_
	.section	.text._ZNSt8_Rb_treeIPN2v88internal13RegExpCaptureES3_St9_IdentityIS3_ENS1_12RegExpParser21RegExpCaptureNameLessENS1_13ZoneAllocatorIS3_EEE17_M_emplace_uniqueIJRS3_EEESt4pairISt17_Rb_tree_iteratorIS3_EbEDpOT_,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal13RegExpCaptureES3_St9_IdentityIS3_ENS1_12RegExpParser21RegExpCaptureNameLessENS1_13ZoneAllocatorIS3_EEE17_M_emplace_uniqueIJRS3_EEESt4pairISt17_Rb_tree_iteratorIS3_EbEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal13RegExpCaptureES3_St9_IdentityIS3_ENS1_12RegExpParser21RegExpCaptureNameLessENS1_13ZoneAllocatorIS3_EEE17_M_emplace_uniqueIJRS3_EEESt4pairISt17_Rb_tree_iteratorIS3_EbEDpOT_
	.type	_ZNSt8_Rb_treeIPN2v88internal13RegExpCaptureES3_St9_IdentityIS3_ENS1_12RegExpParser21RegExpCaptureNameLessENS1_13ZoneAllocatorIS3_EEE17_M_emplace_uniqueIJRS3_EEESt4pairISt17_Rb_tree_iteratorIS3_EbEDpOT_, @function
_ZNSt8_Rb_treeIPN2v88internal13RegExpCaptureES3_St9_IdentityIS3_ENS1_12RegExpParser21RegExpCaptureNameLessENS1_13ZoneAllocatorIS3_EEE17_M_emplace_uniqueIJRS3_EEESt4pairISt17_Rb_tree_iteratorIS3_EbEDpOT_:
.LFB21506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$39, %rax
	jbe	.L1846
	leaq	40(%r13), %rax
	movq	%rax, 16(%rdi)
.L1801:
	movq	(%r14), %rax
	leaq	16(%r12), %rcx
	movq	%rcx, -56(%rbp)
	movq	%rax, 32(%r13)
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L1802
	movq	24(%rax), %r14
	movq	16(%r14), %r10
	movq	8(%r14), %rcx
	movq	%r10, %r11
	subq	%rcx, %r11
	.p2align 4,,10
	.p2align 3
.L1803:
	movq	32(%rbx), %rax
	movq	24(%rax), %rax
	movq	16(%rax), %rsi
	movq	8(%rax), %rdx
	movq	%rsi, %r9
	movq	%rdx, %rax
	subq	%rdx, %r9
	leaq	(%rcx,%r9), %r8
	cmpq	%r9, %r11
	cmovle	%r10, %r8
	cmpq	%r8, %rcx
	je	.L1806
	movq	%rcx, %rdi
	jmp	.L1810
	.p2align 4,,10
	.p2align 3
.L1839:
	ja	.L1809
	addq	$2, %rdi
	addq	$2, %rax
	cmpq	%rdi, %r8
	je	.L1806
.L1810:
	movzwl	(%rax), %r15d
	cmpw	%r15w, (%rdi)
	jnb	.L1839
.L1807:
	movq	16(%rbx), %rax
	movl	$1, %edi
	testq	%rax, %rax
	je	.L1804
.L1847:
	movq	%rax, %rbx
	jmp	.L1803
	.p2align 4,,10
	.p2align 3
.L1806:
	cmpq	%rax, %rsi
	jne	.L1807
.L1809:
	movq	24(%rbx), %rax
	xorl	%edi, %edi
	testq	%rax, %rax
	jne	.L1847
.L1804:
	movq	%rbx, %r8
	testb	%dil, %dil
	je	.L1813
	cmpq	%rbx, 32(%r12)
	je	.L1848
.L1827:
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	16(%r14), %r10
	movq	8(%r14), %rcx
	movq	%rbx, %r8
	movq	32(%rax), %rdx
	movq	%rax, %rbx
	movq	%r10, %r11
	movq	24(%rdx), %rdx
	subq	%rcx, %r11
	movq	16(%rdx), %rsi
	movq	8(%rdx), %rdx
	movq	%rsi, %r9
	subq	%rdx, %r9
.L1813:
	leaq	(%rdx,%r11), %rax
	cmpq	%r9, %r11
	cmovl	%rax, %rsi
	cmpq	%rdx, %rsi
	jne	.L1819
	jmp	.L1815
	.p2align 4,,10
	.p2align 3
.L1840:
	ja	.L1818
	addq	$2, %rdx
	addq	$2, %rcx
	cmpq	%rdx, %rsi
	je	.L1815
.L1819:
	movzwl	(%rcx), %eax
	cmpw	%ax, (%rdx)
	jnb	.L1840
.L1816:
	testq	%r8, %r8
	je	.L1849
.L1826:
	movl	$1, %edi
	cmpq	%r8, -56(%rbp)
	jne	.L1850
.L1820:
	movq	-56(%rbp), %rcx
	movq	%r8, %rdx
	movq	%r13, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r12)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1849:
	.cfi_restore_state
	xorl	%ebx, %ebx
.L1818:
	movq	%rbx, %rax
	xorl	%edx, %edx
.L1851:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1815:
	.cfi_restore_state
	cmpq	%rcx, %r10
	jne	.L1816
	movq	%rbx, %rax
	xorl	%edx, %edx
	jmp	.L1851
	.p2align 4,,10
	.p2align 3
.L1848:
	movq	%rbx, %r8
	jmp	.L1826
	.p2align 4,,10
	.p2align 3
.L1802:
	leaq	16(%r12), %rbx
	cmpq	32(%r12), %rbx
	je	.L1835
	movq	24(%rax), %r14
	jmp	.L1827
	.p2align 4,,10
	.p2align 3
.L1846:
	movl	$40, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L1801
	.p2align 4,,10
	.p2align 3
.L1850:
	movq	32(%r8), %rax
	movq	16(%r14), %rcx
	movq	24(%rax), %rax
	movq	%rcx, %r10
	movq	16(%rax), %rdi
	movq	8(%rax), %rdx
	movq	8(%r14), %rax
	movq	%rdi, %rsi
	subq	%rdx, %rsi
	subq	%rax, %r10
	leaq	(%rax,%rsi), %r9
	cmpq	%rsi, %r10
	cmovg	%r9, %rcx
	cmpq	%rcx, %rax
	jne	.L1824
	jmp	.L1822
	.p2align 4,,10
	.p2align 3
.L1841:
	ja	.L1833
	addq	$2, %rax
	addq	$2, %rdx
	cmpq	%rax, %rcx
	je	.L1822
.L1824:
	movzwl	(%rdx), %ebx
	cmpw	%bx, (%rax)
	jnb	.L1841
.L1844:
	movl	$1, %edi
	jmp	.L1820
	.p2align 4,,10
	.p2align 3
.L1835:
	leaq	16(%r12), %r8
	jmp	.L1844
	.p2align 4,,10
	.p2align 3
.L1822:
	cmpq	%rdx, %rdi
	setne	%dil
	movzbl	%dil, %edi
	jmp	.L1820
	.p2align 4,,10
	.p2align 3
.L1833:
	xorl	%edi, %edi
	jmp	.L1820
	.cfi_endproc
.LFE21506:
	.size	_ZNSt8_Rb_treeIPN2v88internal13RegExpCaptureES3_St9_IdentityIS3_ENS1_12RegExpParser21RegExpCaptureNameLessENS1_13ZoneAllocatorIS3_EEE17_M_emplace_uniqueIJRS3_EEESt4pairISt17_Rb_tree_iteratorIS3_EbEDpOT_, .-_ZNSt8_Rb_treeIPN2v88internal13RegExpCaptureES3_St9_IdentityIS3_ENS1_12RegExpParser21RegExpCaptureNameLessENS1_13ZoneAllocatorIS3_EEE17_M_emplace_uniqueIJRS3_EEESt4pairISt17_Rb_tree_iteratorIS3_EbEDpOT_
	.section	.rodata._ZN2v88internal12RegExpParser25CreateNamedCaptureAtIndexEPKNS0_10ZoneVectorItEEi.str1.1,"aMS",@progbits,1
.LC19:
	.string	"Duplicate capture group name"
	.section	.text._ZN2v88internal12RegExpParser25CreateNamedCaptureAtIndexEPKNS0_10ZoneVectorItEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser25CreateNamedCaptureAtIndexEPKNS0_10ZoneVectorItEEi
	.type	_ZN2v88internal12RegExpParser25CreateNamedCaptureAtIndexEPKNS0_10ZoneVectorItEEi, @function
_ZN2v88internal12RegExpParser25CreateNamedCaptureAtIndexEPKNS0_10ZoneVectorItEEi:
.LFB18389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movl	%edx, %esi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal12RegExpParser10GetCaptureEi
	movq	%r12, 24(%rax)
	movq	32(%rbx), %rdi
	movq	%rax, -56(%rbp)
	testq	%rdi, %rdi
	je	.L1862
	leaq	-56(%rbp), %r12
	movq	%r12, %rsi
	call	_ZNSt8_Rb_treeIPN2v88internal13RegExpCaptureES3_St9_IdentityIS3_ENS1_12RegExpParser21RegExpCaptureNameLessENS1_13ZoneAllocatorIS3_EEE4findERKS3_
	movq	32(%rbx), %rdi
	leaq	16(%rdi), %rdx
	cmpq	%rax, %rdx
	jne	.L1863
.L1856:
	movq	%r12, %rsi
	call	_ZNSt8_Rb_treeIPN2v88internal13RegExpCaptureES3_St9_IdentityIS3_ENS1_12RegExpParser21RegExpCaptureNameLessENS1_13ZoneAllocatorIS3_EEE17_M_emplace_uniqueIJRS3_EEESt4pairISt17_Rb_tree_iteratorIS3_EbEDpOT_
	movl	$1, %eax
.L1852:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1864
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1862:
	.cfi_restore_state
	movq	8(%rbx), %r12
	movq	16(%r12), %rdi
	movq	24(%r12), %rax
	subq	%rdi, %rax
	cmpq	$55, %rax
	jbe	.L1865
	leaq	56(%rdi), %rax
	movq	%rax, 16(%r12)
.L1855:
	leaq	16(%rdi), %rax
	movq	%r12, (%rdi)
	leaq	-56(%rbp), %r12
	movl	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	%rax, 32(%rdi)
	movq	%rax, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	%rdi, 32(%rbx)
	jmp	.L1856
	.p2align 4,,10
	.p2align 3
.L1863:
	cmpb	$0, 81(%rbx)
	jne	.L1857
	movb	$1, 81(%rbx)
	movq	(%rbx), %rdi
	leaq	.LC19(%rip), %rax
	xorl	%edx, %edx
	leaq	-48(%rbp), %rsi
	movq	%rax, -48(%rbp)
	movq	$28, -40(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%rbx), %rdx
	testq	%rax, %rax
	je	.L1866
	movq	%rax, (%rdx)
	movq	48(%rbx), %rax
	movl	$2097152, 56(%rbx)
	movl	36(%rax), %eax
	movl	%eax, 64(%rbx)
.L1857:
	xorl	%eax, %eax
	jmp	.L1852
	.p2align 4,,10
	.p2align 3
.L1865:
	movq	%r12, %rdi
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L1855
.L1866:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L1864:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18389:
	.size	_ZN2v88internal12RegExpParser25CreateNamedCaptureAtIndexEPKNS0_10ZoneVectorItEEi, .-_ZN2v88internal12RegExpParser25CreateNamedCaptureAtIndexEPKNS0_10ZoneVectorItEEi
	.section	.text._ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	.type	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_, @function
_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_:
.LFB22183:
	.cfi_startproc
	endbr64
	movabsq	$9223372036854775807, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r15
	movq	%rsi, %rax
	subq	%r15, %rax
	cmpq	%rcx, %rax
	je	.L1881
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L1876
	leaq	(%rax,%rax), %r14
	cmpq	%r14, %rax
	jbe	.L1882
.L1878:
	movq	%rcx, %r14
.L1869:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1875:
	movzbl	0(%r13), %eax
	subq	%r8, %rsi
	leaq	1(%rbx,%rdx), %r10
	movq	%rsi, %r13
	movb	%al, (%rbx,%rdx)
	leaq	(%r10,%rsi), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1883
	testq	%rsi, %rsi
	jg	.L1871
	testq	%r15, %r15
	jne	.L1874
.L1872:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1883:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L1871
.L1874:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L1872
	.p2align 4,,10
	.p2align 3
.L1882:
	testq	%r14, %r14
	js	.L1878
	jne	.L1869
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1875
	.p2align 4,,10
	.p2align 3
.L1871:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L1872
	jmp	.L1874
	.p2align 4,,10
	.p2align 3
.L1876:
	movl	$1, %r14d
	jmp	.L1869
.L1881:
	leaq	.LC10(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22183:
	.size	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_, .-_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	.section	.text._ZN2v88internal12RegExpParser22ParsePropertyClassNameEPSt6vectorIcSaIcEES5_.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12RegExpParser22ParsePropertyClassNameEPSt6vectorIcSaIcEES5_.part.0, @function
_ZN2v88internal12RegExpParser22ParsePropertyClassNameEPSt6vectorIcSaIcEES5_.part.0:
.LFB22921:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	56(%r12), %eax
	cmpl	$125, %eax
	je	.L1886
.L1885:
	cmpl	$61, %eax
	je	.L1938
	leaq	-96(%rbp), %rdi
	movq	%rdi, -104(%rbp)
.L1901:
	movl	%eax, %edx
	movl	%eax, %ecx
	andl	$-33, %edx
	subl	$65, %edx
	cmpb	$25, %dl
	jbe	.L1890
.L1945:
	subl	$48, %eax
	cmpb	$9, %al
	jbe	.L1890
	cmpb	$95, %cl
	jne	.L1892
.L1890:
	movq	48(%r12), %rax
	movl	36(%rax), %eax
	cmpl	%eax, 64(%r12)
	jge	.L1892
.L1946:
	movb	%cl, -80(%rbp)
	movq	8(%r15), %rsi
	cmpq	16(%r15), %rsi
	je	.L1893
	movb	%cl, (%rsi)
	addq	$1, 8(%r15)
.L1894:
	movq	48(%r12), %rax
	movl	36(%rax), %ebx
	cmpl	%ebx, 64(%r12)
	jl	.L1944
	movl	$2097152, 56(%r12)
	movl	36(%rax), %eax
	movb	$0, 76(%r12)
	addl	$1, %eax
	movl	%eax, 64(%r12)
.L1905:
	movl	$2097152, %eax
	movl	%eax, %edx
	movl	%eax, %ecx
	andl	$-33, %edx
	subl	$65, %edx
	cmpb	$25, %dl
	ja	.L1945
	movq	48(%r12), %rax
	movl	36(%rax), %eax
	cmpl	%eax, 64(%r12)
	jl	.L1946
	.p2align 4,,10
	.p2align 3
.L1892:
	xorl	%eax, %eax
	jmp	.L1884
	.p2align 4,,10
	.p2align 3
.L1944:
	movq	(%r12), %rbx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%rbx), %rax
	jnb	.L1896
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L1947
	movl	$216, %edi
	call	_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE@PLT
	cmpb	$0, 81(%r12)
	jne	.L1941
	movq	%rax, %rdi
	movq	%rax, -112(%rbp)
	call	strlen@PLT
	movq	-112(%rbp), %rdx
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	movq	-104(%rbp), %rsi
	cltq
	movq	%rdx, -96(%rbp)
	xorl	%edx, %edx
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r12), %rdx
	testq	%rax, %rax
	je	.L1904
	movq	%rax, (%rdx)
	movq	48(%r12), %rax
	movl	$2097152, 56(%r12)
	movl	36(%rax), %eax
	movl	%eax, 64(%r12)
	movl	$2097152, %eax
	jmp	.L1901
	.p2align 4,,10
	.p2align 3
.L1893:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	jmp	.L1894
	.p2align 4,,10
	.p2align 3
.L1896:
	movq	8(%r12), %rax
	cmpq	$268435456, 8(%rax)
	jbe	.L1902
	cmpb	$0, 81(%r12)
	je	.L1903
.L1941:
	movl	56(%r12), %eax
.L1899:
	cmpl	$125, %eax
	jne	.L1885
.L1886:
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movb	$0, -80(%rbp)
	movq	8(%r15), %rsi
	cmpq	16(%r15), %rsi
	je	.L1917
	movb	$0, (%rsi)
	addq	$1, 8(%r15)
.L1918:
	movl	$1, %eax
.L1884:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L1948
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1902:
	.cfi_restore_state
	movq	48(%r12), %rdx
	movslq	64(%r12), %rax
	movzbl	32(%rdx), %edi
	movq	40(%rdx), %r9
	movq	%rax, %rcx
	testb	%dil, %dil
	jne	.L1949
	movzwl	(%r9,%rax,2), %eax
.L1907:
	leal	1(%rcx), %esi
	testb	$16, 60(%r12)
	je	.L1908
	cmpl	36(%rdx), %esi
	jge	.L1908
	movl	%eax, %edx
	andl	$64512, %edx
	cmpl	$55296, %edx
	jne	.L1908
	testb	%dil, %dil
	je	.L1950
	.p2align 4,,10
	.p2align 3
.L1908:
	movl	%esi, 64(%r12)
	movl	%eax, 56(%r12)
	jmp	.L1899
	.p2align 4,,10
	.p2align 3
.L1949:
	movzbl	(%r9,%rax), %eax
	jmp	.L1907
	.p2align 4,,10
	.p2align 3
.L1903:
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	leaq	.LC3(%rip), %rax
	movq	$28, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r12), %rdx
	testq	%rax, %rax
	je	.L1904
	movq	%rax, (%rdx)
	movq	48(%r12), %rax
	movl	$2097152, 56(%r12)
	movl	36(%rax), %eax
	movl	%eax, 64(%r12)
	jmp	.L1905
	.p2align 4,,10
	.p2align 3
.L1938:
	movq	%r12, %rdi
	leaq	-80(%rbp), %rbx
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	56(%r12), %eax
	cmpl	$125, %eax
	jne	.L1914
	jmp	.L1913
	.p2align 4,,10
	.p2align 3
.L1951:
	movb	%cl, (%rsi)
	addq	$1, 8(%r13)
.L1942:
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	56(%r12), %eax
	cmpl	$125, %eax
	je	.L1913
.L1914:
	movl	%eax, %edx
	movl	%eax, %ecx
	andl	$-33, %edx
	subl	$65, %edx
	cmpb	$25, %dl
	jbe	.L1909
	subl	$48, %eax
	cmpb	$9, %al
	jbe	.L1909
	cmpb	$95, %cl
	jne	.L1892
.L1909:
	movq	48(%r12), %rax
	movl	36(%rax), %eax
	cmpl	%eax, 64(%r12)
	jge	.L1892
	movb	%cl, -80(%rbp)
	movq	8(%r13), %rsi
	cmpq	16(%r13), %rsi
	jne	.L1951
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	jmp	.L1942
.L1904:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1913:
	movb	$0, -80(%rbp)
	movq	8(%r13), %rsi
	cmpq	16(%r13), %rsi
	je	.L1915
	movb	$0, (%rsi)
	addq	$1, 8(%r13)
	jmp	.L1886
.L1917:
	leaq	-80(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	jmp	.L1918
.L1950:
	movslq	%esi, %rdx
	movzwl	(%r9,%rdx,2), %edx
	movl	%edx, %edi
	andw	$-1024, %di
	cmpw	$-9216, %di
	jne	.L1908
	sall	$10, %eax
	andl	$1023, %edx
	leal	2(%rcx), %esi
	andl	$1047552, %eax
	orl	%eax, %edx
	leal	65536(%rdx), %eax
	jmp	.L1908
.L1915:
	leaq	-80(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	jmp	.L1886
.L1947:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1948:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22921:
	.size	_ZN2v88internal12RegExpParser22ParsePropertyClassNameEPSt6vectorIcSaIcEES5_.part.0, .-_ZN2v88internal12RegExpParser22ParsePropertyClassNameEPSt6vectorIcSaIcEES5_.part.0
	.section	.text._ZN2v88internal12RegExpParser22ParsePropertyClassNameEPSt6vectorIcSaIcEES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser22ParsePropertyClassNameEPSt6vectorIcSaIcEES5_
	.type	_ZN2v88internal12RegExpParser22ParsePropertyClassNameEPSt6vectorIcSaIcEES5_, @function
_ZN2v88internal12RegExpParser22ParsePropertyClassNameEPSt6vectorIcSaIcEES5_:
.LFB18412:
	.cfi_startproc
	endbr64
	cmpl	$123, 56(%rdi)
	je	.L1954
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1954:
	jmp	_ZN2v88internal12RegExpParser22ParsePropertyClassNameEPSt6vectorIcSaIcEES5_.part.0
	.cfi_endproc
.LFE18412:
	.size	_ZN2v88internal12RegExpParser22ParsePropertyClassNameEPSt6vectorIcSaIcEES5_, .-_ZN2v88internal12RegExpParser22ParsePropertyClassNameEPSt6vectorIcSaIcEES5_
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal12RegExpParserC2EPNS0_16FlatStringReaderEPNS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEEPNS0_7IsolateEPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal12RegExpParserC2EPNS0_16FlatStringReaderEPNS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEEPNS0_7IsolateEPNS0_4ZoneE, @function
_GLOBAL__sub_I__ZN2v88internal12RegExpParserC2EPNS0_16FlatStringReaderEPNS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEEPNS0_7IsolateEPNS0_4ZoneE:
.LFB22732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22732:
	.size	_GLOBAL__sub_I__ZN2v88internal12RegExpParserC2EPNS0_16FlatStringReaderEPNS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEEPNS0_7IsolateEPNS0_4ZoneE, .-_GLOBAL__sub_I__ZN2v88internal12RegExpParserC2EPNS0_16FlatStringReaderEPNS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEEPNS0_7IsolateEPNS0_4ZoneE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal12RegExpParserC2EPNS0_16FlatStringReaderEPNS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEEPNS0_7IsolateEPNS0_4ZoneE
	.section	.text._ZN2v88internal12_GLOBAL__N_123LookupPropertyValueNameE9UPropertyPKcbPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_123LookupPropertyValueNameE9UPropertyPKcbPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneE, @function
_ZN2v88internal12_GLOBAL__N_123LookupPropertyValueNameE9UPropertyPKcbPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneE:
.LFB18407:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$4106, %ebx
	subq	$264, %rsp
	movl	%edx, -292(%rbp)
	movq	%rcx, -280(%rbp)
	movq	%r8, -288(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$28672, %edi
	cmovne	%edi, %ebx
	movl	%ebx, %edi
	call	u_getPropertyValueEnum_67@PLT
	cmpl	$-1, %eax
	je	.L1963
	movl	%eax, %esi
	xorl	%edx, %edx
	movl	%ebx, %edi
	movl	%eax, %r12d
	call	u_getPropertyValueName_67@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1971
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L1962
.L1971:
	xorl	%r15d, %r15d
	jmp	.L1964
	.p2align 4,,10
	.p2align 3
.L1981:
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L1962
.L1964:
	addl	$1, %r15d
	movl	%r12d, %esi
	movl	%ebx, %edi
	movl	%r15d, %edx
	call	u_getPropertyValueName_67@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L1981
.L1963:
	xorl	%r12d, %r12d
.L1957:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1982
	addq	$264, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1962:
	.cfi_restore_state
	leaq	-256(%rbp), %r13
	movl	$0, -268(%rbp)
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movl	%r12d, %edx
	movl	%r14d, %esi
	movq	%r13, %rdi
	leaq	-268(%rbp), %rcx
	call	_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode@PLT
	movl	-268(%rbp), %ebx
	testl	%ebx, %ebx
	je	.L1983
.L1965:
	xorl	%r12d, %r12d
.L1967:
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	jmp	.L1957
	.p2align 4,,10
	.p2align 3
.L1983:
	movq	%r13, %rdi
	call	_ZNK6icu_6710UnicodeSet7isEmptyEv@PLT
	testb	%al, %al
	jne	.L1965
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet16removeAllStringsEv@PLT
	cmpb	$0, -292(%rbp)
	jne	.L1984
.L1966:
	leaq	-264(%rbp), %r12
	jmp	.L1968
	.p2align 4,,10
	.p2align 3
.L1985:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	movl	%ebx, %esi
	movq	%r13, %rdi
	addl	$1, %ebx
	movl	%eax, %r14d
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movq	-288(%rbp), %rdx
	movq	%r12, %rsi
	movq	-280(%rbp), %rdi
	movl	%eax, -264(%rbp)
	movl	%r14d, -260(%rbp)
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
.L1968:
	movq	%r13, %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	cmpl	%ebx, %eax
	jg	.L1985
	movl	$1, %r12d
	jmp	.L1967
	.p2align 4,,10
	.p2align 3
.L1984:
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet10complementEv@PLT
	jmp	.L1966
.L1982:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18407:
	.size	_ZN2v88internal12_GLOBAL__N_123LookupPropertyValueNameE9UPropertyPKcbPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneE, .-_ZN2v88internal12_GLOBAL__N_123LookupPropertyValueNameE9UPropertyPKcbPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneE
	.section	.rodata._ZN2v88internal12RegExpParser21AddPropertyClassRangeEPNS0_8ZoneListINS0_14CharacterRangeEEEbRKSt6vectorIcSaIcEESA_.str1.1,"aMS",@progbits,1
.LC20:
	.string	"N"
.LC21:
	.string	"Y"
.LC22:
	.string	"Any"
.LC23:
	.string	"ASCII"
.LC24:
	.string	"Assigned"
.LC25:
	.string	"Unassigned"
	.section	.text._ZN2v88internal12RegExpParser21AddPropertyClassRangeEPNS0_8ZoneListINS0_14CharacterRangeEEEbRKSt6vectorIcSaIcEESA_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser21AddPropertyClassRangeEPNS0_8ZoneListINS0_14CharacterRangeEEEbRKSt6vectorIcSaIcEESA_
	.type	_ZN2v88internal12RegExpParser21AddPropertyClassRangeEPNS0_8ZoneListINS0_14CharacterRangeEEEbRKSt6vectorIcSaIcEESA_, @function
_ZN2v88internal12RegExpParser21AddPropertyClassRangeEPNS0_8ZoneListINS0_14CharacterRangeEEEbRKSt6vectorIcSaIcEESA_:
.LFB18413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -68(%rbp)
	movq	(%rcx), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r8), %rax
	movq	%rax, -80(%rbp)
	movq	8(%r8), %rax
	cmpq	%rax, (%r8)
	je	.L2034
	movq	%r13, %rdi
	call	u_getPropertyEnum_67@PLT
	xorl	%esi, %esi
	movl	%eax, %edi
	movl	%eax, %r12d
	call	u_getPropertyName_67@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2016
	movq	%r13, %rdi
	xorl	%ebx, %ebx
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L2009
	jmp	.L2008
	.p2align 4,,10
	.p2align 3
.L2035:
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L2008
.L2009:
	addl	$1, %ebx
	movl	%r12d, %edi
	movl	%ebx, %esi
	call	u_getPropertyName_67@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L2035
.L2007:
	xorl	%r12d, %r12d
.L1986:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2036
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2016:
	.cfi_restore_state
	xorl	%ebx, %ebx
	jmp	.L2009
	.p2align 4,,10
	.p2align 3
.L2008:
	cmpl	$4101, %r12d
	je	.L2037
	cmpl	$4106, %r12d
	je	.L2005
	cmpl	$28672, %r12d
	jne	.L2007
.L2005:
	movzbl	-68(%rbp), %edx
	movq	8(%r14), %r8
	movl	%r12d, %edi
	movq	%r15, %rcx
	movq	-80(%rbp), %rsi
	call	_ZN2v88internal12_GLOBAL__N_123LookupPropertyValueNameE9UPropertyPKcbPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneE
	movl	%eax, %r12d
	jmp	.L1986
	.p2align 4,,10
	.p2align 3
.L2034:
	movq	8(%rdi), %r8
	movzbl	-68(%rbp), %edx
	movq	%rsi, %rcx
	movl	$8192, %edi
	movq	%r13, %rsi
	call	_ZN2v88internal12_GLOBAL__N_123LookupPropertyValueNameE9UPropertyPKcbPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneE
	movl	%eax, %r12d
	testb	%al, %al
	je	.L1988
.L2033:
	movl	$1, %r12d
	jmp	.L1986
	.p2align 4,,10
	.p2align 3
.L1988:
	movl	$4, %ecx
	leaq	.LC22(%rip), %rdi
	movq	%r13, %rsi
	movq	8(%r14), %rdx
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L1990
	cmpb	$0, -68(%rbp)
	jne	.L2033
	leaq	-64(%rbp), %rsi
	movq	%r15, %rdi
	movl	$1, %r12d
	movabsq	$4785070309113856, %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	jmp	.L1986
	.p2align 4,,10
	.p2align 3
.L2037:
	movl	$8192, %r12d
	jmp	.L2005
	.p2align 4,,10
	.p2align 3
.L1990:
	movl	$6, %ecx
	leaq	.LC23(%rip), %rdi
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	movsbl	%al, %eax
	testl	%eax, %eax
	je	.L2038
	movl	$9, %ecx
	leaq	.LC24(%rip), %rdi
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L1997
	movzbl	-68(%rbp), %r9d
	movq	%rdx, %r8
	movq	%r15, %rcx
	movl	$4101, %edi
	leaq	.LC25(%rip), %rsi
	xorl	$1, %r9d
	movzbl	%r9b, %r9d
	movl	%r9d, %edx
	call	_ZN2v88internal12_GLOBAL__N_123LookupPropertyValueNameE9UPropertyPKcbPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneE
	testb	%al, %al
	jne	.L2033
.L1997:
	movq	%r13, %rdi
	call	u_getPropertyEnum_67@PLT
	movl	%eax, %ebx
	cmpl	$64, %eax
	ja	.L1986
	leaq	.L1996(%rip), %rcx
	movl	%eax, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal12RegExpParser21AddPropertyClassRangeEPNS0_8ZoneListINS0_14CharacterRangeEEEbRKSt6vectorIcSaIcEESA_,"a",@progbits
	.align 4
	.align 4
.L1996:
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1986-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1986-.L1996
	.long	.L1995-.L1996
	.long	.L1986-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1986-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1986-.L1996
	.long	.L1986-.L1996
	.long	.L1986-.L1996
	.long	.L1986-.L1996
	.long	.L1986-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1986-.L1996
	.long	.L1986-.L1996
	.long	.L1986-.L1996
	.long	.L1986-.L1996
	.long	.L1986-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1995-.L1996
	.long	.L1986-.L1996
	.long	.L1995-.L1996
	.section	.text._ZN2v88internal12RegExpParser21AddPropertyClassRangeEPNS0_8ZoneListINS0_14CharacterRangeEEEbRKSt6vectorIcSaIcEESA_
	.p2align 4,,10
	.p2align 3
.L1995:
	xorl	%esi, %esi
	movl	%ebx, %edi
	call	u_getPropertyName_67@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2013
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L2003
	jmp	.L2002
	.p2align 4,,10
	.p2align 3
.L2001:
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L2002
.L2003:
	addl	$1, %r12d
	movl	%ebx, %edi
	movl	%r12d, %esi
	call	u_getPropertyName_67@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L2001
	jmp	.L2007
	.p2align 4,,10
	.p2align 3
.L2038:
	movl	-68(%rbp), %ebx
	movl	$128, %esi
	movq	%r15, %rdi
	cmpb	$1, %bl
	sbbl	%ecx, %ecx
	andl	$-1113984, %ecx
	addl	$1114111, %ecx
	testb	%bl, %bl
	cmovne	%esi, %eax
	leaq	-64(%rbp), %rsi
	movl	%ecx, -60(%rbp)
	movl	%eax, -64(%rbp)
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	jmp	.L2033
	.p2align 4,,10
	.p2align 3
.L2002:
	cmpb	$0, -68(%rbp)
	leaq	.LC21(%rip), %rax
	movq	%r15, %rcx
	movl	%ebx, %edi
	leaq	.LC20(%rip), %rsi
	movq	8(%r14), %r8
	cmove	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal12_GLOBAL__N_123LookupPropertyValueNameE9UPropertyPKcbPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneE
	movl	%eax, %r12d
	jmp	.L1986
	.p2align 4,,10
	.p2align 3
.L2013:
	xorl	%r12d, %r12d
	jmp	.L2003
.L2036:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18413:
	.size	_ZN2v88internal12RegExpParser21AddPropertyClassRangeEPNS0_8ZoneListINS0_14CharacterRangeEEEbRKSt6vectorIcSaIcEESA_, .-_ZN2v88internal12RegExpParser21AddPropertyClassRangeEPNS0_8ZoneListINS0_14CharacterRangeEEEbRKSt6vectorIcSaIcEESA_
	.section	.rodata._ZN2v88internal12RegExpParser16ParseClassEscapeEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneEbPiPb.part.0.str1.1,"aMS",@progbits,1
.LC26:
	.string	"\\ at end of pattern"
	.section	.rodata._ZN2v88internal12RegExpParser16ParseClassEscapeEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneEbPiPb.part.0.str1.8,"aMS",@progbits,1
	.align 8
.LC27:
	.string	"Invalid property name in character class"
	.section	.text._ZN2v88internal12RegExpParser16ParseClassEscapeEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneEbPiPb.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12RegExpParser16ParseClassEscapeEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneEbPiPb.part.0, @function
_ZN2v88internal12RegExpParser16ParseClassEscapeEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneEbPiPb.part.0:
.LFB22941:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	48(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	64(%rdi), %eax
	movl	36(%rsi), %r8d
	cmpl	%r8d, %eax
	jge	.L2040
	cmpb	$0, 32(%rsi)
	movq	%r9, %rbx
	movq	40(%rsi), %rdi
	movslq	%eax, %r9
	je	.L2041
	movzbl	(%rdi,%r9), %r14d
	movl	60(%r12), %edi
	addl	$1, %eax
	shrl	$4, %edi
	andl	$1, %edi
	cmpl	%eax, %r8d
	jle	.L2044
	testb	%dil, %dil
	jne	.L2042
.L2044:
	leal	-68(%r14), %esi
	cmpl	$51, %esi
	ja	.L2045
	leaq	.L2047(%rip), %r8
	movslq	(%r8,%rsi,4), %rsi
	addq	%r8, %rsi
	notrack jmp	*%rsi
	.section	.rodata._ZN2v88internal12RegExpParser16ParseClassEscapeEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneEbPiPb.part.0,"a",@progbits
	.align 4
	.align 4
.L2047:
	.long	.L2046-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2048-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2046-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2046-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2046-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2048-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2046-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2045-.L2047
	.long	.L2046-.L2047
	.section	.text._ZN2v88internal12RegExpParser16ParseClassEscapeEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneEbPiPb.part.0
	.p2align 4,,10
	.p2align 3
.L2041:
	movzwl	(%rdi,%r9,2), %r14d
	movl	60(%r12), %edi
	addl	$1, %eax
	shrl	$4, %edi
	movl	%r14d, %esi
	andl	$1, %edi
	cmpl	%eax, %r8d
	jle	.L2044
	testb	%dil, %dil
	je	.L2044
	andw	$-1024, %si
	cmpw	$-10240, %si
	jne	.L2042
.L2045:
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser25ParseClassCharacterEscapeEv
	movl	%eax, (%r15)
	movb	$0, (%rbx)
.L2039:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2092
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2042:
	.cfi_restore_state
	leal	-68(%r14), %esi
	cmpl	$51, %esi
	ja	.L2045
	leaq	.L2061(%rip), %rdi
	movslq	(%rdi,%rsi,4), %rsi
	addq	%rdi, %rsi
	notrack jmp	*%rsi
	.section	.rodata._ZN2v88internal12RegExpParser16ParseClassEscapeEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneEbPiPb.part.0
	.align 4
	.align 4
.L2061:
	.long	.L2046-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2052-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2046-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2046-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2046-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2052-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2046-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2045-.L2061
	.long	.L2046-.L2061
	.section	.text._ZN2v88internal12RegExpParser16ParseClassEscapeEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneEbPiPb.part.0
	.p2align 4,,10
	.p2align 3
.L2040:
	cmpb	$0, 81(%rdi)
	jne	.L2039
	movb	$1, 81(%rdi)
	movq	(%rdi), %rdi
	xorl	%edx, %edx
	leaq	.LC26(%rip), %rax
	leaq	-144(%rbp), %rsi
	movq	%rax, -144(%rbp)
	movq	$19, -136(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r12), %rdx
	testq	%rax, %rax
	je	.L2058
	movq	%rax, (%rdx)
	movq	48(%r12), %rax
	movl	$2097152, 56(%r12)
	movl	36(%rax), %eax
	movl	%eax, 64(%r12)
	jmp	.L2039
.L2046:
	movzbl	%cl, %r8d
	movq	%r13, %rsi
	movq	%rdx, %rcx
	movl	%r14d, %edi
	movl	%r8d, %edx
	call	_ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEbPNS0_4ZoneE@PLT
	addl	$1, 64(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movb	$1, (%rbx)
	jmp	.L2039
.L2048:
	testb	%dil, %dil
	je	.L2045
.L2052:
	movl	%eax, 64(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	pxor	%xmm0, %xmm0
	cmpl	$123, 56(%r12)
	movq	$0, -96(%rbp)
	movq	$0, -64(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -80(%rbp)
	je	.L2053
.L2056:
	cmpb	$0, 81(%r12)
	jne	.L2057
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	xorl	%edx, %edx
	leaq	-128(%rbp), %rsi
	leaq	.LC27(%rip), %rax
	movq	$40, -120(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r12), %rdx
	testq	%rax, %rax
	je	.L2058
	movq	%rax, (%rdx)
	movq	48(%r12), %rax
	movl	$2097152, 56(%r12)
	movl	36(%rax), %eax
	movl	%eax, 64(%r12)
.L2057:
	movq	-80(%rbp), %rdi
	movb	$1, (%rbx)
	testq	%rdi, %rdi
	je	.L2059
	call	_ZdlPv@PLT
.L2059:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2039
	call	_ZdlPv@PLT
	jmp	.L2039
	.p2align 4,,10
	.p2align 3
.L2058:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2053:
	leaq	-80(%rbp), %r8
	leaq	-112(%rbp), %r15
	movq	%r12, %rdi
	movq	%r8, %rdx
	movq	%r15, %rsi
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal12RegExpParser22ParsePropertyClassNameEPSt6vectorIcSaIcEES5_.part.0
	movq	-152(%rbp), %r8
	testb	%al, %al
	je	.L2056
	xorl	%edx, %edx
	cmpl	$80, %r14d
	movq	%r15, %rcx
	movq	%r13, %rsi
	sete	%dl
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser21AddPropertyClassRangeEPNS0_8ZoneListINS0_14CharacterRangeEEEbRKSt6vectorIcSaIcEESA_
	testb	%al, %al
	jne	.L2057
	jmp	.L2056
.L2092:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22941:
	.size	_ZN2v88internal12RegExpParser16ParseClassEscapeEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneEbPiPb.part.0, .-_ZN2v88internal12RegExpParser16ParseClassEscapeEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneEbPiPb.part.0
	.section	.rodata._ZN2v88internal12RegExpParser19ParseCharacterClassEPKNS0_13RegExpBuilderE.str1.1,"aMS",@progbits,1
.LC28:
	.string	"Invalid character class"
	.section	.rodata._ZN2v88internal12RegExpParser19ParseCharacterClassEPKNS0_13RegExpBuilderE.str1.8,"aMS",@progbits,1
	.align 8
.LC29:
	.string	"Range out of order in character class"
	.section	.rodata._ZN2v88internal12RegExpParser19ParseCharacterClassEPKNS0_13RegExpBuilderE.str1.1
.LC30:
	.string	"Unterminated character class"
	.section	.text._ZN2v88internal12RegExpParser19ParseCharacterClassEPKNS0_13RegExpBuilderE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser19ParseCharacterClassEPKNS0_13RegExpBuilderE
	.type	_ZN2v88internal12RegExpParser19ParseCharacterClassEPKNS0_13RegExpBuilderE, @function
_ZN2v88internal12RegExpParser19ParseCharacterClassEPKNS0_13RegExpBuilderE:
.LFB18418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	cmpl	$94, 56(%r15)
	movl	$0, -140(%rbp)
	je	.L2156
.L2094:
	movq	8(%r15), %r13
	movq	24(%r13), %rdx
	movq	16(%r13), %r12
	movq	%rdx, %rax
	subq	%r12, %rax
	cmpq	$15, %rax
	jbe	.L2157
	leaq	16(%r12), %rax
	movq	%rax, 16(%r13)
.L2096:
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L2158
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
.L2098:
	movq	%rax, (%r12)
	xorl	%ebx, %ebx
	movq	$2, 8(%r12)
	testb	$16, 60(%r15)
	je	.L2099
	movq	-136(%rbp), %rax
	movl	12(%rax), %eax
	movl	%eax, %ebx
	movl	%eax, -144(%rbp)
	shrl	%ebx
	andl	$1, %ebx
	.p2align 4,,10
	.p2align 3
.L2099:
	cmpb	$0, 76(%r15)
	je	.L2123
.L2160:
	movl	56(%r15), %r14d
	cmpl	$93, %r14d
	je	.L2101
	movzbl	%bl, %r13d
	cmpl	$92, %r14d
	jne	.L2102
	movq	8(%r15), %rdx
	leaq	-122(%rbp), %r9
	leaq	-120(%rbp), %r8
	movl	%r13d, %ecx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12RegExpParser16ParseClassEscapeEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneEbPiPb.part.0
	cmpb	$0, 81(%r15)
	jne	.L2129
.L2161:
	cmpl	$45, 56(%r15)
	je	.L2159
	cmpb	$0, -122(%rbp)
	jne	.L2099
	movl	-120(%rbp), %eax
	movq	8(%r15), %rdx
	leaq	-80(%rbp), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	movl	%eax, -80(%rbp)
	movl	%eax, -76(%rbp)
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	cmpb	$0, 76(%r15)
	jne	.L2160
.L2123:
	cmpb	$0, 81(%r15)
	jne	.L2129
	leaq	.LC30(%rip), %rax
	movb	$1, 81(%r15)
	movq	(%r15), %rdi
	leaq	-80(%rbp), %rsi
	movq	%rax, -80(%rbp)
	movq	$28, -72(%rbp)
	jmp	.L2154
	.p2align 4,,10
	.p2align 3
.L2102:
	movq	%r15, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	cmpb	$0, 81(%r15)
	movl	%r14d, -120(%rbp)
	movb	$0, -122(%rbp)
	je	.L2161
	.p2align 4,,10
	.p2align 3
.L2129:
	xorl	%eax, %eax
.L2093:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2162
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2159:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	56(%r15), %eax
	cmpl	$2097152, %eax
	je	.L2106
	movq	8(%r15), %rdx
	cmpl	$93, %eax
	je	.L2163
	cmpl	$92, %eax
	jne	.L2109
	leaq	-121(%rbp), %r9
	leaq	-116(%rbp), %r8
	movl	%r13d, %ecx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12RegExpParser16ParseClassEscapeEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneEbPiPb.part.0
.L2110:
	cmpb	$0, 81(%r15)
	jne	.L2129
	cmpb	$0, -122(%rbp)
	jne	.L2111
	cmpb	$0, -121(%rbp)
	jne	.L2112
	movl	-120(%rbp), %edx
	movl	-116(%rbp), %eax
	cmpl	%eax, %edx
	jg	.L2164
	movq	8(%r15), %r8
	leaq	-80(%rbp), %r13
	movl	%edx, -80(%rbp)
	movq	%r12, %rdi
	movq	%r13, %rsi
	movl	%eax, -76(%rbp)
	movq	%r8, %rdx
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	jmp	.L2099
	.p2align 4,,10
	.p2align 3
.L2111:
	testb	$16, 60(%r15)
	jne	.L2126
	leaq	-80(%rbp), %r13
.L2128:
	movq	8(%r15), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$193273528365, %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	cmpb	$0, -121(%rbp)
	jne	.L2099
	movl	-116(%rbp), %eax
	movq	8(%r15), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, -80(%rbp)
	movl	%eax, -76(%rbp)
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	jmp	.L2099
	.p2align 4,,10
	.p2align 3
.L2109:
	movq	%r15, %rdi
	movl	%eax, -144(%rbp)
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	-144(%rbp), %eax
	movb	$0, -121(%rbp)
	movl	%eax, -116(%rbp)
	jmp	.L2110
	.p2align 4,,10
	.p2align 3
.L2112:
	testb	$16, 60(%r15)
	je	.L2165
.L2126:
	leaq	.LC28(%rip), %rax
	movb	$1, 81(%r15)
	movq	(%r15), %rdi
	leaq	-112(%rbp), %rsi
	movq	%rax, -112(%rbp)
	movq	$23, -104(%rbp)
.L2154:
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r15), %rdx
	testq	%rax, %rax
	je	.L2166
	movq	%rax, (%rdx)
	movq	48(%r15), %rax
	movl	$2097152, 56(%r15)
	movl	36(%rax), %eax
	movl	%eax, 64(%r15)
	jmp	.L2129
	.p2align 4,,10
	.p2align 3
.L2156:
	movq	%r15, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	$1, -140(%rbp)
	jmp	.L2094
	.p2align 4,,10
	.p2align 3
.L2165:
	movl	-120(%rbp), %eax
	leaq	-80(%rbp), %r13
	movq	8(%r15), %rdx
	movq	%r12, %rdi
	movq	%r13, %rsi
	movl	%eax, -80(%rbp)
	movl	%eax, -76(%rbp)
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	jmp	.L2128
.L2163:
	cmpb	$0, -122(%rbp)
	leaq	-80(%rbp), %r13
	je	.L2167
.L2108:
	movabsq	$193273528365, %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
.L2106:
	cmpb	$0, 76(%r15)
	je	.L2123
	.p2align 4,,10
	.p2align 3
.L2101:
	movq	%r15, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movq	8(%r15), %r13
	movq	-136(%rbp), %rax
	movq	24(%r13), %rdx
	movl	12(%rax), %ecx
	movq	16(%r13), %rax
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L2168
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%r13)
.L2125:
	movl	-140(%rbp), %r8d
	movq	%rax, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal20RegExpCharacterClassC1EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	movq	-136(%rbp), %rax
	jmp	.L2093
	.p2align 4,,10
	.p2align 3
.L2158:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2098
	.p2align 4,,10
	.p2align 3
.L2157:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r13), %rdx
	movq	%rax, %r12
	movq	16(%r13), %rax
	jmp	.L2096
.L2166:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2168:
	movl	$32, %esi
	movq	%r13, %rdi
	movl	%ecx, -136(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-136(%rbp), %ecx
	jmp	.L2125
.L2167:
	movl	-120(%rbp), %eax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, -80(%rbp)
	movl	%eax, -76(%rbp)
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	movq	8(%r15), %rdx
	jmp	.L2108
.L2164:
	leaq	.LC29(%rip), %rax
	movb	$1, 81(%r15)
	movq	(%r15), %rdi
	leaq	-96(%rbp), %rsi
	movq	%rax, -96(%rbp)
	movq	$37, -88(%rbp)
	jmp	.L2154
.L2162:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18418:
	.size	_ZN2v88internal12RegExpParser19ParseCharacterClassEPKNS0_13RegExpBuilderE, .-_ZN2v88internal12RegExpParser19ParseCharacterClassEPKNS0_13RegExpBuilderE
	.section	.text._ZN2v88internal12RegExpParser16ParseClassEscapeEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneEbPiPb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser16ParseClassEscapeEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneEbPiPb
	.type	_ZN2v88internal12RegExpParser16ParseClassEscapeEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneEbPiPb, @function
_ZN2v88internal12RegExpParser16ParseClassEscapeEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneEbPiPb:
.LFB18417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$152, %rsp
	movl	56(%rdi), %r15d
	movl	64(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	48(%rdi), %rax
	movl	36(%rax), %edi
	cmpl	$92, %r15d
	jne	.L2170
	cmpl	%edi, %esi
	jge	.L2171
	cmpb	$0, 32(%rax)
	movq	40(%rax), %r8
	movslq	%esi, %r9
	je	.L2172
	movzbl	(%r8,%r9), %r15d
	movl	60(%r12), %r8d
	addl	$1, %esi
	shrl	$4, %r8d
	andl	$1, %r8d
	cmpl	%esi, %edi
	jle	.L2175
	testb	%r8b, %r8b
	je	.L2175
.L2173:
	leal	-68(%r15), %eax
	cmpl	$51, %eax
	ja	.L2176
	leaq	.L2202(%rip), %rdi
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal12RegExpParser16ParseClassEscapeEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneEbPiPb,"a",@progbits
	.align 4
	.align 4
.L2202:
	.long	.L2177-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2183-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2177-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2177-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2177-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2183-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2177-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2176-.L2202
	.long	.L2177-.L2202
	.section	.text._ZN2v88internal12RegExpParser16ParseClassEscapeEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneEbPiPb
	.p2align 4,,10
	.p2align 3
.L2171:
	cmpb	$0, 81(%r12)
	je	.L2245
	.p2align 4,,10
	.p2align 3
.L2169:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2246
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2172:
	.cfi_restore_state
	movzwl	(%r8,%r9,2), %r15d
	movl	60(%r12), %r8d
	addl	$1, %esi
	shrl	$4, %r8d
	movl	%r15d, %eax
	andl	$1, %r8d
	cmpl	%edi, %esi
	jl	.L2247
.L2175:
	leal	-68(%r15), %eax
	cmpl	$51, %eax
	ja	.L2176
	leaq	.L2178(%rip), %rdi
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal12RegExpParser16ParseClassEscapeEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneEbPiPb
	.align 4
	.align 4
.L2178:
	.long	.L2177-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2179-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2177-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2177-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2177-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2179-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2177-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2176-.L2178
	.long	.L2177-.L2178
	.section	.text._ZN2v88internal12RegExpParser16ParseClassEscapeEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneEbPiPb
	.p2align 4,,10
	.p2align 3
.L2170:
	cmpl	%edi, %esi
	jl	.L2248
	movl	$2097152, 56(%r12)
	movl	36(%rax), %eax
	movb	$0, 76(%r12)
	addl	$1, %eax
	movl	%eax, 64(%r12)
.L2196:
	movl	%r15d, (%r14)
	movb	$0, (%rbx)
	jmp	.L2169
.L2179:
	testb	%r8b, %r8b
	jne	.L2183
.L2176:
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser25ParseClassCharacterEscapeEv
	movl	%eax, (%r14)
	movb	$0, (%rbx)
	jmp	.L2169
.L2177:
	movzbl	%cl, %r8d
	movq	%r13, %rsi
	movq	%rdx, %rcx
	movl	%r15d, %edi
	movl	%r8d, %edx
	call	_ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEbPNS0_4ZoneE@PLT
	addl	$1, 64(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movb	$1, (%rbx)
	jmp	.L2169
	.p2align 4,,10
	.p2align 3
.L2247:
	testb	%r8b, %r8b
	je	.L2175
	andw	$-1024, %ax
	cmpw	$-10240, %ax
	je	.L2176
	jmp	.L2173
	.p2align 4,,10
	.p2align 3
.L2245:
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	.LC26(%rip), %rax
	xorl	%edx, %edx
	leaq	-176(%rbp), %rsi
	movq	%rax, -176(%rbp)
	movq	$19, -168(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r12), %rdx
	testq	%rax, %rax
	je	.L2189
	movq	%rax, (%rdx)
	movq	48(%r12), %rax
	movl	$2097152, 56(%r12)
	movl	36(%rax), %eax
	movl	%eax, 64(%r12)
	jmp	.L2169
	.p2align 4,,10
	.p2align 3
.L2248:
	movq	(%r12), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r13), %rax
	jnb	.L2193
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L2249
	movl	$216, %edi
	call	_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE@PLT
	cmpb	$0, 81(%r12)
	movq	%rax, %r13
	jne	.L2196
	movq	%rax, %rdi
	call	strlen@PLT
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-144(%rbp), %rsi
	cltq
	movq	%r13, -144(%rbp)
	movq	%rax, -136(%rbp)
	jmp	.L2244
	.p2align 4,,10
	.p2align 3
.L2193:
	movq	8(%r12), %rax
	cmpq	$268435456, 8(%rax)
	ja	.L2250
	movq	48(%r12), %rax
	movslq	64(%r12), %rcx
	movzbl	32(%rax), %edi
	movq	40(%rax), %r8
	movq	%rcx, %rdx
	testb	%dil, %dil
	je	.L2199
	movzbl	(%r8,%rcx), %esi
.L2200:
	leal	1(%rdx), %ecx
	testb	$16, 60(%r12)
	je	.L2201
	cmpl	36(%rax), %ecx
	jge	.L2201
	movl	%esi, %eax
	andl	$64512, %eax
	cmpl	$55296, %eax
	jne	.L2201
	testb	%dil, %dil
	je	.L2251
	.p2align 4,,10
	.p2align 3
.L2201:
	movl	%ecx, 64(%r12)
	movl	%esi, 56(%r12)
	jmp	.L2196
.L2183:
	movl	%esi, 64(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	pxor	%xmm0, %xmm0
	cmpl	$123, 56(%r12)
	movq	$0, -96(%rbp)
	movq	$0, -64(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -80(%rbp)
	je	.L2184
.L2187:
	cmpb	$0, 81(%r12)
	jne	.L2188
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	.LC27(%rip), %rax
	xorl	%edx, %edx
	leaq	-160(%rbp), %rsi
	movq	%rax, -160(%rbp)
	movq	$40, -152(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r12), %rdx
	testq	%rax, %rax
	je	.L2189
	movq	%rax, (%rdx)
	movq	48(%r12), %rax
	movl	$2097152, 56(%r12)
	movl	36(%rax), %eax
	movl	%eax, 64(%r12)
.L2188:
	movq	-80(%rbp), %rdi
	movb	$1, (%rbx)
	testq	%rdi, %rdi
	je	.L2190
	call	_ZdlPv@PLT
.L2190:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2169
	call	_ZdlPv@PLT
	jmp	.L2169
	.p2align 4,,10
	.p2align 3
.L2250:
	cmpb	$0, 81(%r12)
	jne	.L2196
	leaq	.LC3(%rip), %rax
	movb	$1, 81(%r12)
	movq	(%r12), %rdi
	leaq	-128(%rbp), %rsi
	movq	%rax, -128(%rbp)
	movq	$28, -120(%rbp)
.L2244:
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r12), %rdx
	testq	%rax, %rax
	je	.L2189
	movq	%rax, (%rdx)
	movq	48(%r12), %rax
	movl	$2097152, 56(%r12)
	movl	36(%rax), %eax
	movl	%eax, 64(%r12)
	jmp	.L2196
	.p2align 4,,10
	.p2align 3
.L2199:
	movzwl	(%r8,%rcx,2), %esi
	jmp	.L2200
	.p2align 4,,10
	.p2align 3
.L2189:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2184:
	leaq	-80(%rbp), %r8
	leaq	-112(%rbp), %r14
	movq	%r12, %rdi
	movq	%r8, %rdx
	movq	%r14, %rsi
	movq	%r8, -184(%rbp)
	call	_ZN2v88internal12RegExpParser22ParsePropertyClassNameEPSt6vectorIcSaIcEES5_.part.0
	movq	-184(%rbp), %r8
	testb	%al, %al
	je	.L2187
	xorl	%edx, %edx
	cmpl	$80, %r15d
	movq	%r14, %rcx
	movq	%r13, %rsi
	sete	%dl
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser21AddPropertyClassRangeEPNS0_8ZoneListINS0_14CharacterRangeEEEbRKSt6vectorIcSaIcEESA_
	testb	%al, %al
	jne	.L2188
	jmp	.L2187
.L2251:
	movslq	%ecx, %rax
	movzwl	(%r8,%rax,2), %eax
	movl	%eax, %edi
	andw	$-1024, %di
	cmpw	$-9216, %di
	jne	.L2201
	sall	$10, %esi
	andl	$1023, %eax
	leal	2(%rdx), %ecx
	andl	$1047552, %esi
	orl	%eax, %esi
	addl	$65536, %esi
	jmp	.L2201
.L2246:
	call	__stack_chk_fail@PLT
.L2249:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18417:
	.size	_ZN2v88internal12RegExpParser16ParseClassEscapeEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneEbPiPb, .-_ZN2v88internal12RegExpParser16ParseClassEscapeEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneEbPiPb
	.section	.text._ZN2v88internal13RegExpBuilder17AddTrailSurrogateEt,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpBuilder17AddTrailSurrogateEt
	.type	_ZN2v88internal13RegExpBuilder17AddTrailSurrogateEt, @function
_ZN2v88internal13RegExpBuilder17AddTrailSurrogateEt:
.LFB18427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$232, %rsp
	movzwl	24(%rdi), %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testw	%r14w, %r14w
	je	.L2253
	xorl	%eax, %eax
	movw	%ax, 24(%rdi)
	movl	12(%rdi), %eax
	andl	$18, %eax
	cmpl	$18, %eax
	je	.L2274
.L2254:
	movq	(%r12), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$7, %rax
	jbe	.L2275
	leaq	8(%r13), %rax
	movq	%rax, 16(%rdi)
.L2257:
	movw	%r14w, 0(%r13)
	movw	%bx, 2(%r13)
	movq	(%r12), %rdi
	movl	12(%r12), %r14d
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$31, %rax
	jbe	.L2276
	leaq	32(%rbx), %rax
	movq	%rax, 16(%rdi)
.L2259:
	leaq	16+_ZTVN2v88internal10RegExpAtomE(%rip), %rax
	movq	%r13, 8(%rbx)
	movq	%rbx, %rdi
	movq	%rax, (%rbx)
	movq	$2, 16(%rbx)
	movl	%r14d, 24(%rbx)
	call	_ZN2v88internal10RegExpTree7IsEmptyEv@PLT
	testb	%al, %al
	jne	.L2277
	movq	%r12, %rdi
	call	_ZN2v88internal13RegExpBuilder15FlushCharactersEv
	cmpq	$0, 56(%r12)
	je	.L2262
	movq	48(%r12), %r13
	movq	(%r12), %r14
	testq	%r13, %r13
	je	.L2278
.L2263:
	movslq	12(%r13), %rax
	movl	8(%r13), %edx
	cmpl	%edx, %eax
	jge	.L2268
	leal	1(%rax), %ecx
	movq	0(%r13), %rdx
	movl	%ecx, 12(%r13)
	movq	56(%r12), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L2262:
	movq	%rbx, 56(%r12)
	.p2align 4,,10
	.p2align 3
.L2252:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2279
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2253:
	.cfi_restore_state
	testw	%si, %si
	je	.L2252
	movzwl	%si, %esi
	call	_ZN2v88internal13RegExpBuilder30AddCharacterClassForDesugaringEi
	jmp	.L2252
	.p2align 4,,10
	.p2align 3
.L2274:
	movl	%r14d, %r13d
	movl	%esi, %eax
	leaq	-256(%rbp), %r15
	sall	$10, %r13d
	andl	$1023, %eax
	movq	%r15, %rdi
	andl	$1047552, %r13d
	orl	%eax, %r13d
	addl	$65536, %r13d
	movl	%r13d, %edx
	movl	%r13d, %esi
	call	_ZN6icu_6710UnicodeSetC1Eii@PLT
	movl	$2, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet9closeOverEi@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet16removeAllStringsEv@PLT
	movq	%r15, %rdi
	call	_ZNK6icu_6710UnicodeSet4sizeEv@PLT
	movq	%r15, %rdi
	movl	%eax, -260(%rbp)
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	cmpl	$1, -260(%rbp)
	jle	.L2254
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal13RegExpBuilder30AddCharacterClassForDesugaringEi
	jmp	.L2252
	.p2align 4,,10
	.p2align 3
.L2277:
	movb	$1, 8(%r12)
	jmp	.L2252
	.p2align 4,,10
	.p2align 3
.L2278:
	movq	24(%r14), %rax
	movq	16(%r14), %r15
	movq	%rax, %rdx
	subq	%r15, %rdx
	cmpq	$15, %rdx
	jbe	.L2280
	leaq	16(%r15), %rdx
	movq	%rdx, 16(%r14)
.L2265:
	subq	%rdx, %rax
	movq	%r15, %r13
	cmpq	$15, %rax
	jbe	.L2281
	leaq	16(%rdx), %rax
	movq	%rax, 16(%r14)
.L2267:
	movq	%rdx, (%r15)
	movq	$2, 8(%r15)
	movq	%r15, 48(%r12)
	jmp	.L2263
	.p2align 4,,10
	.p2align 3
.L2276:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L2259
	.p2align 4,,10
	.p2align 3
.L2275:
	movl	$8, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L2257
	.p2align 4,,10
	.p2align 3
.L2268:
	movq	16(%r14), %rdi
	movq	24(%r14), %rax
	leal	1(%rdx,%rdx), %ecx
	movslq	%ecx, %rsi
	movq	56(%r12), %r15
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L2282
	addq	%rdi, %rsi
	movq	%rsi, 16(%r14)
.L2270:
	movslq	12(%r13), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L2271
	movq	0(%r13), %rsi
	movl	%ecx, -260(%rbp)
	call	memcpy@PLT
	movslq	12(%r13), %rdx
	movl	-260(%rbp), %ecx
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$3, %rdx
.L2271:
	addl	$1, %eax
	movq	%rdi, 0(%r13)
	movl	%ecx, 8(%r13)
	movl	%eax, 12(%r13)
	movq	%r15, (%rdi,%rdx)
	jmp	.L2262
.L2282:
	movq	%r14, %rdi
	movl	%ecx, -260(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-260(%rbp), %ecx
	movq	%rax, %rdi
	jmp	.L2270
.L2281:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L2267
.L2280:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	16(%r14), %rdx
	movq	%rax, %r15
	movq	24(%r14), %rax
	jmp	.L2265
.L2279:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18427:
	.size	_ZN2v88internal13RegExpBuilder17AddTrailSurrogateEt, .-_ZN2v88internal13RegExpBuilder17AddTrailSurrogateEt
	.section	.text._ZN2v88internal13RegExpBuilder28NeedsDesugaringForIgnoreCaseEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpBuilder28NeedsDesugaringForIgnoreCaseEi
	.type	_ZN2v88internal13RegExpBuilder28NeedsDesugaringForIgnoreCaseEi, @function
_ZN2v88internal13RegExpBuilder28NeedsDesugaringForIgnoreCaseEi:
.LFB18446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	subq	$208, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	12(%rdi), %eax
	andl	$18, %eax
	cmpl	$18, %eax
	je	.L2289
.L2283:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2290
	addq	$208, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2289:
	.cfi_restore_state
	leaq	-224(%rbp), %r13
	movl	%esi, %edx
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetC1Eii@PLT
	movl	$2, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet9closeOverEi@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet16removeAllStringsEv@PLT
	movq	%r13, %rdi
	call	_ZNK6icu_6710UnicodeSet4sizeEv@PLT
	movq	%r13, %rdi
	cmpl	$1, %eax
	setg	%r12b
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	jmp	.L2283
.L2290:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18446:
	.size	_ZN2v88internal13RegExpBuilder28NeedsDesugaringForIgnoreCaseEi, .-_ZN2v88internal13RegExpBuilder28NeedsDesugaringForIgnoreCaseEi
	.section	.text._ZN2v88internal13RegExpBuilder12AddCharacterEt,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpBuilder12AddCharacterEt
	.type	_ZN2v88internal13RegExpBuilder12AddCharacterEt, @function
_ZN2v88internal13RegExpBuilder12AddCharacterEt:
.LFB18431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$216, %rsp
	movzwl	24(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testw	%si, %si
	jne	.L2309
.L2292:
	movl	12(%r12), %eax
	movb	$0, 8(%r12)
	andl	$18, %eax
	cmpl	$18, %eax
	je	.L2310
.L2293:
	movq	16(%r12), %r13
	movq	(%r12), %r14
	testq	%r13, %r13
	je	.L2311
.L2295:
	movslq	12(%r13), %rax
	movl	8(%r13), %edx
	cmpl	%edx, %eax
	jge	.L2300
	movq	0(%r13), %rdx
	leal	1(%rax), %ecx
	movl	%ecx, 12(%r13)
	movw	%bx, (%rdx,%rax,2)
.L2291:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2312
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2309:
	.cfi_restore_state
	xorl	%eax, %eax
	movw	%ax, 24(%rdi)
	call	_ZN2v88internal13RegExpBuilder30AddCharacterClassForDesugaringEi
	jmp	.L2292
	.p2align 4,,10
	.p2align 3
.L2300:
	leal	1(%rdx,%rdx), %r12d
	movq	16(%r14), %rcx
	movslq	%r12d, %rax
	leaq	7(%rax,%rax), %rsi
	movq	24(%r14), %rax
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L2313
	addq	%rcx, %rsi
	movq	%rsi, 16(%r14)
.L2302:
	movslq	12(%r13), %rdx
	movq	%rdx, %rax
	addq	%rdx, %rdx
	testl	%eax, %eax
	jg	.L2314
.L2303:
	addl	$1, %eax
	movq	%rcx, 0(%r13)
	movl	%r12d, 8(%r13)
	movl	%eax, 12(%r13)
	movw	%bx, (%rcx,%rdx)
	jmp	.L2291
	.p2align 4,,10
	.p2align 3
.L2314:
	movq	0(%r13), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%r13), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rax
	addq	%rdx, %rdx
	jmp	.L2303
	.p2align 4,,10
	.p2align 3
.L2310:
	leaq	-256(%rbp), %r13
	movzwl	%bx, %r15d
	movl	%r15d, %edx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetC1Eii@PLT
	movl	$2, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet9closeOverEi@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet16removeAllStringsEv@PLT
	movq	%r13, %rdi
	call	_ZNK6icu_6710UnicodeSet4sizeEv@PLT
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	cmpl	$1, %r14d
	jle	.L2293
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal13RegExpBuilder30AddCharacterClassForDesugaringEi
	jmp	.L2291
	.p2align 4,,10
	.p2align 3
.L2311:
	movq	24(%r14), %rdx
	movq	16(%r14), %r15
	movq	%rdx, %rax
	subq	%r15, %rax
	cmpq	$15, %rax
	jbe	.L2315
	leaq	16(%r15), %rax
	movq	%rax, 16(%r14)
.L2297:
	subq	%rax, %rdx
	movq	%r15, %r13
	cmpq	$7, %rdx
	jbe	.L2316
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%r14)
.L2299:
	movq	%rax, (%r15)
	movq	$4, 8(%r15)
	movq	(%r12), %r14
	movq	%r15, 16(%r12)
	jmp	.L2295
	.p2align 4,,10
	.p2align 3
.L2313:
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L2302
	.p2align 4,,10
	.p2align 3
.L2316:
	movl	$8, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2299
	.p2align 4,,10
	.p2align 3
.L2315:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r14), %rdx
	movq	%rax, %r15
	movq	16(%r14), %rax
	jmp	.L2297
.L2312:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18431:
	.size	_ZN2v88internal13RegExpBuilder12AddCharacterEt, .-_ZN2v88internal13RegExpBuilder12AddCharacterEt
	.section	.text._ZN2v88internal13RegExpBuilder26AddEscapedUnicodeCharacterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpBuilder26AddEscapedUnicodeCharacterEi
	.type	_ZN2v88internal13RegExpBuilder26AddEscapedUnicodeCharacterEi, @function
_ZN2v88internal13RegExpBuilder26AddEscapedUnicodeCharacterEi:
.LFB18433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	movzwl	24(%rdi), %esi
	testw	%si, %si
	jne	.L2335
	cmpl	$65535, %ebx
	jle	.L2319
	leal	-65536(%rbx), %r12d
	shrl	$10, %r12d
	andw	$1023, %r12w
	subw	$10240, %r12w
.L2320:
	movl	%ebx, %esi
	movw	%r12w, 24(%r13)
	movq	%r13, %rdi
	andw	$1023, %si
	subw	$9216, %si
	movzwl	%si, %esi
	call	_ZN2v88internal13RegExpBuilder17AddTrailSurrogateEt
	movzwl	24(%r13), %r12d
.L2321:
	testw	%r12w, %r12w
	jne	.L2336
.L2317:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2335:
	.cfi_restore_state
	xorl	%edi, %edi
	movw	%di, 24(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder30AddCharacterClassForDesugaringEi
	cmpl	$65535, %ebx
	jg	.L2337
.L2319:
	testb	$16, 12(%r13)
	je	.L2325
	movl	%ebx, %eax
	andl	$64512, %eax
	cmpl	$55296, %eax
	je	.L2338
	cmpl	$56320, %eax
	je	.L2339
.L2325:
	movzwl	%bx, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder12AddCharacterEt
	movzwl	24(%r13), %r12d
	testw	%r12w, %r12w
	je	.L2317
.L2336:
	xorl	%eax, %eax
	movzwl	%r12w, %esi
	movq	%r13, %rdi
	movw	%ax, 24(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13RegExpBuilder30AddCharacterClassForDesugaringEi
	.p2align 4,,10
	.p2align 3
.L2337:
	.cfi_restore_state
	leal	-65536(%rbx), %r12d
	movzwl	24(%r13), %esi
	shrl	$10, %r12d
	andw	$1023, %r12w
	subw	$10240, %r12w
	testw	%si, %si
	je	.L2320
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movw	%cx, 24(%r13)
	call	_ZN2v88internal13RegExpBuilder30AddCharacterClassForDesugaringEi
	jmp	.L2320
	.p2align 4,,10
	.p2align 3
.L2338:
	movzwl	24(%r13), %esi
	movl	%ebx, %r12d
	testw	%si, %si
	jne	.L2340
.L2324:
	movw	%bx, 24(%r13)
	jmp	.L2321
	.p2align 4,,10
	.p2align 3
.L2339:
	movzwl	%bx, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder17AddTrailSurrogateEt
	movzwl	24(%r13), %r12d
	jmp	.L2321
	.p2align 4,,10
	.p2align 3
.L2340:
	xorl	%edx, %edx
	movq	%r13, %rdi
	movw	%dx, 24(%r13)
	call	_ZN2v88internal13RegExpBuilder30AddCharacterClassForDesugaringEi
	jmp	.L2324
	.cfi_endproc
.LFE18433:
	.size	_ZN2v88internal13RegExpBuilder26AddEscapedUnicodeCharacterEi, .-_ZN2v88internal13RegExpBuilder26AddEscapedUnicodeCharacterEi
	.section	.rodata._ZN2v88internal12RegExpParser19GetPropertySequenceERKSt6vectorIcSaIcEE.str1.1,"aMS",@progbits,1
.LC31:
	.string	"Emoji_Flag_Sequence"
.LC32:
	.string	"Emoji_Tag_Sequence"
.LC33:
	.string	"Emoji_ZWJ_Sequence"
.LC34:
	.string	"Emoji_Keycap_Sequence"
.LC35:
	.string	"Emoji_Modifier_Sequence"
	.section	.text._ZN2v88internal12RegExpParser19GetPropertySequenceERKSt6vectorIcSaIcEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser19GetPropertySequenceERKSt6vectorIcSaIcEE
	.type	_ZN2v88internal12RegExpParser19GetPropertySequenceERKSt6vectorIcSaIcEE, @function
_ZN2v88internal12RegExpParser19GetPropertySequenceERKSt6vectorIcSaIcEE:
.LFB18414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal28FLAG_harmony_regexp_sequenceE(%rip)
	je	.L2405
	movq	(%rsi), %rdx
	movq	%rdi, %rbx
	movl	$20, %ecx
	leaq	.LC31(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L2428
	leaq	_ZN2v88internal24UnicodePropertySequences19kEmojiFlagSequencesE(%rip), %r12
.L2343:
	movq	8(%rbx), %rax
	xorl	%r8d, %r8d
	movb	$0, -136(%rbp)
	pxor	%xmm0, %xmm0
	movq	$0, -128(%rbp)
	leaq	-144(%rbp), %r13
	leaq	16+_ZTVN2v88internal11RegExpEmptyE(%rip), %r14
	movq	%rax, -144(%rbp)
	movl	$16, -132(%rbp)
	movw	%r8w, -120(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L2346:
	movl	(%r12), %ebx
	addq	$4, %r12
	testl	%ebx, %ebx
	jne	.L2429
.L2347:
	movl	(%r12), %ecx
	movq	%r13, %rdi
	testl	%ecx, %ecx
	je	.L2427
	call	_ZN2v88internal13RegExpBuilder9FlushTextEv
	movq	-112(%rbp), %r8
	movq	-104(%rbp), %r15
	testq	%r8, %r8
	je	.L2358
	movl	12(%r8), %ebx
.L2358:
	xorl	%esi, %esi
	testq	%r15, %r15
	movq	-144(%rbp), %rdi
	setne	%sil
	addl	%ebx, %esi
	je	.L2430
	cmpl	$1, %esi
	je	.L2362
	testq	%r8, %r8
	je	.L2431
.L2363:
	testq	%r15, %r15
	je	.L2368
	movslq	12(%r8), %rax
	movl	8(%r8), %edx
	cmpl	%edx, %eax
	jge	.L2369
	leal	1(%rax), %ecx
	movq	(%r8), %rdx
	movl	%ecx, 12(%r8)
	movq	-104(%rbp), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L2370:
	movq	$0, -104(%rbp)
	movq	-112(%rbp), %r8
.L2368:
	movq	-144(%rbp), %rdi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	$23, %rax
	jbe	.L2432
	leaq	24(%r15), %rax
	movq	%rax, 16(%rdi)
.L2375:
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17RegExpAlternativeC1EPNS0_8ZoneListIPNS0_10RegExpTreeEEE@PLT
	movq	-144(%rbp), %rdi
.L2362:
	cmpq	$0, -72(%rbp)
	je	.L2376
	movq	-80(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L2433
.L2377:
	movslq	12(%rbx), %rax
	movl	8(%rbx), %edx
	cmpl	%edx, %eax
	jge	.L2382
	leal	1(%rax), %ecx
	movq	(%rbx), %rdx
	movl	%ecx, 12(%rbx)
	movq	-72(%rbp), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L2376:
	movl	(%r12), %ebx
	pxor	%xmm0, %xmm0
	movq	%r15, -72(%rbp)
	addq	$4, %r12
	movaps	%xmm0, -112(%rbp)
	testl	%ebx, %ebx
	je	.L2347
.L2429:
	cmpl	$65535, %ebx
	jle	.L2348
	leal	-65536(%rbx), %r15d
	movzwl	-120(%rbp), %esi
	shrl	$10, %r15d
	andw	$1023, %r15w
	subw	$10240, %r15w
	testw	%si, %si
	jne	.L2434
.L2349:
	andw	$1023, %bx
	movq	%r13, %rdi
	movw	%r15w, -120(%rbp)
	leal	-9216(%rbx), %esi
	movzwl	%si, %esi
	call	_ZN2v88internal13RegExpBuilder17AddTrailSurrogateEt
	jmp	.L2346
	.p2align 4,,10
	.p2align 3
.L2428:
	movl	$19, %ecx
	leaq	.LC32(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L2407
	movl	$19, %ecx
	leaq	.LC33(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L2408
	movl	$22, %ecx
	leaq	.LC34(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L2435
	movq	%rdx, %rsi
	movl	$24, %ecx
	leaq	.LC35(%rip), %rdi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	xorl	%eax, %eax
	testb	%dl, %dl
	jne	.L2341
	movq	8(%rbx), %r13
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movb	$0, -136(%rbp)
	movl	$16, -132(%rbp)
	movq	%r13, -144(%rbp)
	movq	$0, -128(%rbp)
	movw	%ax, -120(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	24(%r13), %rax
	movq	16(%r13), %r12
	movq	%rax, %rdx
	subq	%r12, %rdx
	cmpq	$15, %rdx
	jbe	.L2436
	leaq	16(%r12), %rdx
	movq	%rdx, 16(%r13)
.L2393:
	subq	%rdx, %rax
	cmpq	$15, %rax
	jbe	.L2437
	leaq	16(%rdx), %rax
	movq	%rax, 16(%r13)
.L2395:
	movq	%rdx, (%r12)
	movq	%r12, %rcx
	xorl	%edx, %edx
	movl	$60, %edi
	movq	$2, 8(%r12)
	movq	8(%rbx), %r8
	leaq	.LC21(%rip), %rsi
	call	_ZN2v88internal12_GLOBAL__N_123LookupPropertyValueNameE9UPropertyPKcbPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneE
	movq	8(%rbx), %r13
	movq	16(%r13), %r14
	movq	24(%r13), %rax
	subq	%r14, %rax
	cmpq	$31, %rax
	jbe	.L2438
	leaq	32(%r14), %rax
	movq	%rax, 16(%r13)
.L2397:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movl	$16, %ecx
	leaq	-144(%rbp), %r13
	call	_ZN2v88internal20RegExpCharacterClassC1EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder17AddCharacterClassEPNS0_20RegExpCharacterClassE
	movq	8(%rbx), %r14
	movq	24(%r14), %rax
	movq	16(%r14), %r12
	movq	%rax, %rdx
	subq	%r12, %rdx
	cmpq	$15, %rdx
	jbe	.L2439
	leaq	16(%r12), %rdx
	movq	%rdx, 16(%r14)
.L2399:
	subq	%rdx, %rax
	cmpq	$15, %rax
	jbe	.L2440
	leaq	16(%rdx), %rax
	movq	%rax, 16(%r14)
.L2401:
	movq	%rdx, (%r12)
	movq	%r12, %rcx
	xorl	%edx, %edx
	movl	$59, %edi
	movq	$2, 8(%r12)
	movq	8(%rbx), %r8
	leaq	.LC21(%rip), %rsi
	call	_ZN2v88internal12_GLOBAL__N_123LookupPropertyValueNameE9UPropertyPKcbPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneE
	movq	8(%rbx), %r15
	movq	16(%r15), %r14
	movq	24(%r15), %rax
	subq	%r14, %rax
	cmpq	$31, %rax
	jbe	.L2441
	leaq	32(%r14), %rax
	movq	%rax, 16(%r15)
.L2403:
	xorl	%r8d, %r8d
	movl	$16, %ecx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal20RegExpCharacterClassC1EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal13RegExpBuilder17AddCharacterClassEPNS0_20RegExpCharacterClassE
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder8ToRegExpEv
	jmp	.L2341
	.p2align 4,,10
	.p2align 3
.L2430:
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	$7, %rax
	jbe	.L2442
	leaq	8(%r15), %rax
	movq	%rax, 16(%rdi)
.L2361:
	movq	%r14, (%r15)
	movq	-144(%rbp), %rdi
	jmp	.L2362
	.p2align 4,,10
	.p2align 3
.L2348:
	movzwl	%bx, %esi
	testb	$16, -132(%rbp)
	je	.L2352
	movl	%ebx, %eax
	andl	$64512, %eax
	cmpl	$55296, %eax
	je	.L2443
	movzwl	%bx, %esi
	cmpl	$56320, %eax
	je	.L2355
.L2352:
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder12AddCharacterEt
	jmp	.L2346
	.p2align 4,,10
	.p2align 3
.L2382:
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	leal	1(%rdx,%rdx), %r9d
	movslq	%r9d, %rsi
	movq	-72(%rbp), %rcx
	salq	$3, %rsi
	subq	%r8, %rax
	cmpq	%rax, %rsi
	ja	.L2444
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L2384:
	movslq	12(%rbx), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L2385
	movq	(%rbx), %rsi
	movq	%r8, %rdi
	movl	%r9d, -176(%rbp)
	movq	%rcx, -168(%rbp)
	call	memcpy@PLT
	movslq	12(%rbx), %rdx
	movl	-176(%rbp), %r9d
	movq	-168(%rbp), %rcx
	movq	%rax, %r8
	movq	%rdx, %rax
	salq	$3, %rdx
.L2385:
	addl	$1, %eax
	movq	%r8, (%rbx)
	movl	%r9d, 8(%rbx)
	movl	%eax, 12(%rbx)
	movq	%rcx, (%r8,%rdx)
	jmp	.L2376
.L2435:
	movq	8(%rbx), %r13
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movb	$0, -136(%rbp)
	movw	%dx, -120(%rbp)
	movq	%r13, -144(%rbp)
	movl	$16, -132(%rbp)
	movq	$0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	24(%r13), %rax
	movq	16(%r13), %r12
	movq	%rax, %rdx
	subq	%r12, %rdx
	cmpq	$15, %rdx
	jbe	.L2445
	leaq	16(%r12), %rdx
	movq	%rdx, 16(%r13)
.L2387:
	subq	%rdx, %rax
	cmpq	$15, %rax
	jbe	.L2446
	leaq	16(%rdx), %rax
	movq	%rax, 16(%r13)
.L2389:
	movq	%rdx, (%r12)
	movq	%r12, %rdi
	leaq	-152(%rbp), %r13
	movabsq	$244813135920, %rax
	movq	$2, 8(%r12)
	movq	8(%rbx), %rdx
	movq	%r13, %rsi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	movq	8(%rbx), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$150323855395, %rax
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	movq	8(%rbx), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$180388626474, %rax
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	movq	8(%rbx), %r14
	movq	16(%r14), %r13
	movq	24(%r14), %rax
	subq	%r13, %rax
	cmpq	$31, %rax
	jbe	.L2447
	leaq	32(%r13), %rax
	movq	%rax, 16(%r14)
.L2391:
	movq	%r12, %rdx
	xorl	%r8d, %r8d
	movl	$16, %ecx
	movq	%r14, %rsi
	leaq	-144(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN2v88internal20RegExpCharacterClassC1EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13RegExpBuilder17AddCharacterClassEPNS0_20RegExpCharacterClassE
	movl	$65039, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal13RegExpBuilder12AddCharacterEt
	movq	%r12, %rdi
	movl	$8419, %esi
	call	_ZN2v88internal13RegExpBuilder12AddCharacterEt
	movq	%r12, %rdi
	.p2align 4,,10
	.p2align 3
.L2427:
	call	_ZN2v88internal13RegExpBuilder8ToRegExpEv
.L2341:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2448
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2433:
	.cfi_restore_state
	movq	24(%rdi), %rax
	movq	16(%rdi), %rdx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	cmpq	$15, %rcx
	jbe	.L2449
	leaq	16(%rdx), %rcx
	movq	%rcx, 16(%rdi)
.L2379:
	subq	%rcx, %rax
	movq	%rdx, %rbx
	cmpq	$15, %rax
	jbe	.L2450
	leaq	16(%rcx), %rax
	movq	%rax, 16(%rdi)
.L2381:
	movq	%rcx, (%rdx)
	movq	$2, 8(%rdx)
	movq	%rdx, -80(%rbp)
	jmp	.L2377
	.p2align 4,,10
	.p2align 3
.L2434:
	xorl	%edi, %edi
	movw	%di, -120(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder30AddCharacterClassForDesugaringEi
	jmp	.L2349
	.p2align 4,,10
	.p2align 3
.L2369:
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leal	1(%rdx,%rdx), %ebx
	movslq	%ebx, %rsi
	salq	$3, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L2451
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L2372:
	movslq	12(%r8), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jg	.L2452
.L2373:
	addl	$1, %eax
	movq	%rcx, (%r8)
	movl	%ebx, 8(%r8)
	movl	%eax, 12(%r8)
	movq	%r15, (%rcx,%rdx)
	jmp	.L2370
	.p2align 4,,10
	.p2align 3
.L2431:
	movq	24(%rdi), %rax
	movq	16(%rdi), %rbx
	movq	%rax, %rdx
	subq	%rbx, %rdx
	cmpq	$15, %rdx
	jbe	.L2453
	leaq	16(%rbx), %rdx
	movq	%rdx, 16(%rdi)
.L2365:
	subq	%rdx, %rax
	movq	%rbx, %r8
	cmpq	$15, %rax
	jbe	.L2454
	leaq	16(%rdx), %rax
	movq	%rax, 16(%rdi)
.L2367:
	movq	%rdx, (%rbx)
	movq	$2, 8(%rbx)
	movq	-104(%rbp), %r15
	movq	%rbx, -112(%rbp)
	jmp	.L2363
	.p2align 4,,10
	.p2align 3
.L2443:
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	jne	.L2455
.L2354:
	movw	%bx, -120(%rbp)
	jmp	.L2346
	.p2align 4,,10
	.p2align 3
.L2407:
	leaq	_ZN2v88internal24UnicodePropertySequences18kEmojiTagSequencesE(%rip), %r12
	jmp	.L2343
	.p2align 4,,10
	.p2align 3
.L2452:
	movq	(%r8), %rsi
	movq	%rcx, %rdi
	movq	%r8, -168(%rbp)
	call	memcpy@PLT
	movq	-168(%rbp), %r8
	movq	%rax, %rcx
	movslq	12(%r8), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	jmp	.L2373
	.p2align 4,,10
	.p2align 3
.L2408:
	leaq	_ZN2v88internal24UnicodePropertySequences18kEmojiZWJSequencesE(%rip), %r12
	jmp	.L2343
.L2455:
	xorl	%esi, %esi
	movq	%r13, %rdi
	movw	%si, -120(%rbp)
	movzwl	%ax, %esi
	call	_ZN2v88internal13RegExpBuilder30AddCharacterClassForDesugaringEi
	jmp	.L2354
	.p2align 4,,10
	.p2align 3
.L2442:
	movl	$8, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L2361
	.p2align 4,,10
	.p2align 3
.L2432:
	movl	$24, %esi
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-168(%rbp), %r8
	movq	%rax, %r15
	jmp	.L2375
	.p2align 4,,10
	.p2align 3
.L2355:
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder17AddTrailSurrogateEt
	jmp	.L2346
	.p2align 4,,10
	.p2align 3
.L2405:
	xorl	%eax, %eax
	jmp	.L2341
	.p2align 4,,10
	.p2align 3
.L2444:
	movl	%r9d, -176(%rbp)
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-168(%rbp), %rcx
	movl	-176(%rbp), %r9d
	movq	%rax, %r8
	jmp	.L2384
.L2449:
	movl	$16, %esi
	movq	%rdi, -168(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-168(%rbp), %rdi
	movq	%rax, %rdx
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	jmp	.L2379
.L2450:
	movl	$16, %esi
	movq	%rdx, -176(%rbp)
	movq	%rdi, -168(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-168(%rbp), %rdi
	movq	-176(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L2381
.L2453:
	movl	$16, %esi
	movq	%rdi, -168(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-168(%rbp), %rdi
	movq	%rax, %rbx
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	jmp	.L2365
.L2454:
	movl	$16, %esi
	movq	%rdi, -168(%rbp)
	movq	%rbx, -176(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-168(%rbp), %rdi
	movq	-176(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L2367
.L2451:
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-168(%rbp), %r8
	movq	%rax, %rcx
	jmp	.L2372
.L2441:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L2403
.L2440:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L2401
.L2439:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	16(%r14), %rdx
	movq	%rax, %r12
	movq	24(%r14), %rax
	jmp	.L2399
.L2438:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L2397
.L2437:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L2395
.L2436:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	16(%r13), %rdx
	movq	%rax, %r12
	movq	24(%r13), %rax
	jmp	.L2393
.L2447:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L2391
.L2446:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L2389
.L2445:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	16(%r13), %rdx
	movq	%rax, %r12
	movq	24(%r13), %rax
	jmp	.L2387
.L2448:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18414:
	.size	_ZN2v88internal12RegExpParser19GetPropertySequenceERKSt6vectorIcSaIcEE, .-_ZN2v88internal12RegExpParser19GetPropertySequenceERKSt6vectorIcSaIcEE
	.section	.text._ZN2v88internal13RegExpBuilder19AddUnicodeCharacterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpBuilder19AddUnicodeCharacterEi
	.type	_ZN2v88internal13RegExpBuilder19AddUnicodeCharacterEi, @function
_ZN2v88internal13RegExpBuilder19AddUnicodeCharacterEi:
.LFB18432:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$232, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$65535, %esi
	jle	.L2457
	movzwl	24(%rdi), %esi
	testw	%si, %si
	jne	.L2490
.L2458:
	xorl	%eax, %eax
	leal	-65536(%rbx), %r13d
	andw	$1023, %bx
	movw	%ax, 24(%r12)
	movl	12(%r12), %eax
	shrl	$10, %r13d
	leal	-9216(%rbx), %r15d
	andw	$1023, %r13w
	andl	$18, %eax
	subw	$10240, %r13w
	cmpl	$18, %eax
	je	.L2491
.L2459:
	movq	(%r12), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$7, %rax
	jbe	.L2492
	leaq	8(%r14), %rax
	movq	%rax, 16(%rdi)
.L2462:
	movw	%r13w, (%r14)
	movw	%r15w, 2(%r14)
	movq	(%r12), %rdi
	movl	12(%r12), %r13d
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$31, %rax
	jbe	.L2493
	leaq	32(%rbx), %rax
	movq	%rax, 16(%rdi)
.L2464:
	leaq	16+_ZTVN2v88internal10RegExpAtomE(%rip), %rax
	movq	%r14, 8(%rbx)
	movq	%rbx, %rdi
	movq	%rax, (%rbx)
	movq	$2, 16(%rbx)
	movl	%r13d, 24(%rbx)
	call	_ZN2v88internal10RegExpTree7IsEmptyEv@PLT
	testb	%al, %al
	jne	.L2494
	movq	%r12, %rdi
	call	_ZN2v88internal13RegExpBuilder15FlushCharactersEv
	cmpq	$0, 56(%r12)
	je	.L2467
	movq	48(%r12), %r13
	movq	(%r12), %r14
	testq	%r13, %r13
	je	.L2495
.L2468:
	movslq	12(%r13), %rax
	movl	8(%r13), %edx
	cmpl	%edx, %eax
	jge	.L2473
	leal	1(%rax), %ecx
	movq	0(%r13), %rdx
	movl	%ecx, 12(%r13)
	movq	56(%r12), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L2467:
	movq	%rbx, 56(%r12)
	jmp	.L2456
	.p2align 4,,10
	.p2align 3
.L2457:
	testb	$16, 12(%rdi)
	jne	.L2477
.L2480:
	movzwl	%bx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal13RegExpBuilder12AddCharacterEt
.L2456:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2496
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2477:
	.cfi_restore_state
	movl	%esi, %eax
	andl	$64512, %eax
	cmpl	$55296, %eax
	je	.L2497
	cmpl	$56320, %eax
	jne	.L2480
	movzwl	%si, %esi
	call	_ZN2v88internal13RegExpBuilder17AddTrailSurrogateEt
	jmp	.L2456
	.p2align 4,,10
	.p2align 3
.L2490:
	xorl	%ecx, %ecx
	movw	%cx, 24(%rdi)
	call	_ZN2v88internal13RegExpBuilder30AddCharacterClassForDesugaringEi
	jmp	.L2458
	.p2align 4,,10
	.p2align 3
.L2497:
	movzwl	24(%rdi), %esi
	testw	%si, %si
	jne	.L2498
.L2479:
	movw	%bx, 24(%r12)
	jmp	.L2456
	.p2align 4,,10
	.p2align 3
.L2494:
	movb	$1, 8(%r12)
	jmp	.L2456
	.p2align 4,,10
	.p2align 3
.L2473:
	movq	16(%r14), %rdi
	movq	24(%r14), %rax
	leal	1(%rdx,%rdx), %ecx
	movslq	%ecx, %rsi
	movq	56(%r12), %r15
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L2499
	addq	%rdi, %rsi
	movq	%rsi, 16(%r14)
.L2475:
	movslq	12(%r13), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L2476
	movq	0(%r13), %rsi
	movl	%ecx, -260(%rbp)
	call	memcpy@PLT
	movslq	12(%r13), %rdx
	movl	-260(%rbp), %ecx
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$3, %rdx
.L2476:
	addl	$1, %eax
	movq	%rdi, 0(%r13)
	movl	%ecx, 8(%r13)
	movl	%eax, 12(%r13)
	movq	%r15, (%rdi,%rdx)
	jmp	.L2467
.L2495:
	movq	24(%r14), %rax
	movq	16(%r14), %r15
	movq	%rax, %rdx
	subq	%r15, %rdx
	cmpq	$15, %rdx
	jbe	.L2500
	leaq	16(%r15), %rdx
	movq	%rdx, 16(%r14)
.L2470:
	subq	%rdx, %rax
	movq	%r15, %r13
	cmpq	$15, %rax
	jbe	.L2501
	leaq	16(%rdx), %rax
	movq	%rax, 16(%r14)
.L2472:
	movq	%rdx, (%r15)
	movq	$2, 8(%r15)
	movq	%r15, 48(%r12)
	jmp	.L2468
	.p2align 4,,10
	.p2align 3
.L2491:
	movl	%r13d, %r14d
	movzwl	%bx, %ebx
	sall	$10, %r14d
	andl	$1047552, %r14d
	orl	%r14d, %ebx
	leal	65536(%rbx), %r14d
	leaq	-256(%rbp), %rbx
	movl	%r14d, %edx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6710UnicodeSetC1Eii@PLT
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6710UnicodeSet9closeOverEi@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6710UnicodeSet16removeAllStringsEv@PLT
	movq	%rbx, %rdi
	call	_ZNK6icu_6710UnicodeSet4sizeEv@PLT
	movq	%rbx, %rdi
	movl	%eax, -260(%rbp)
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	cmpl	$1, -260(%rbp)
	jle	.L2459
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal13RegExpBuilder30AddCharacterClassForDesugaringEi
	jmp	.L2456
	.p2align 4,,10
	.p2align 3
.L2498:
	xorl	%edx, %edx
	movw	%dx, 24(%rdi)
	call	_ZN2v88internal13RegExpBuilder30AddCharacterClassForDesugaringEi
	jmp	.L2479
	.p2align 4,,10
	.p2align 3
.L2493:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2492:
	movl	$8, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L2462
.L2499:
	movq	%r14, %rdi
	movl	%ecx, -260(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-260(%rbp), %ecx
	movq	%rax, %rdi
	jmp	.L2475
.L2501:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L2472
.L2500:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	16(%r14), %rdx
	movq	%rax, %r15
	movq	24(%r14), %rax
	jmp	.L2470
.L2496:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18432:
	.size	_ZN2v88internal13RegExpBuilder19AddUnicodeCharacterEi, .-_ZN2v88internal13RegExpBuilder19AddUnicodeCharacterEi
	.section	.rodata._ZN2v88internal12RegExpParser16ParseDisjunctionEv.str1.1,"aMS",@progbits,1
.LC36:
	.string	"Unterminated group"
.LC37:
	.string	"Unmatched ')'"
.LC38:
	.string	"Nothing to repeat"
.LC39:
	.string	"Invalid property name"
.LC40:
	.string	"Invalid decimal escape"
.LC41:
	.string	"Invalid Unicode escape"
.LC42:
	.string	"Lone quantifier brackets"
	.section	.rodata._ZN2v88internal12RegExpParser16ParseDisjunctionEv.str1.8,"aMS",@progbits,1
	.align 8
.LC43:
	.string	"numbers out of order in {} quantifier"
	.section	.rodata._ZN2v88internal12RegExpParser16ParseDisjunctionEv.str1.1
.LC44:
	.string	"Incomplete quantifier"
.LC45:
	.string	"Invalid quantifier"
	.section	.text._ZN2v88internal12RegExpParser16ParseDisjunctionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser16ParseDisjunctionEv
	.type	_ZN2v88internal12RegExpParser16ParseDisjunctionEv, @function
_ZN2v88internal12RegExpParser16ParseDisjunctionEv:
.LFB18370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$376, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movl	60(%rdi), %r12d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movq	16(%rbx), %r10
	movq	24(%rbx), %rax
	subq	%r10, %rax
	cmpq	$79, %rax
	jbe	.L2887
	leaq	80(%r10), %rax
	movq	%rax, 16(%rbx)
.L2504:
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	movq	%rbx, (%r10)
	movq	%r10, %r13
	movw	%ax, 24(%r10)
	leaq	-96(%rbp), %rax
	leaq	.L2719(%rip), %rbx
	movl	%r12d, 12(%r10)
	leaq	.L2509(%rip), %r12
	movb	$0, 8(%r10)
	movq	$0, 16(%r10)
	movups	%xmm0, 32(%r10)
	movq	%r10, -88(%rbp)
	movq	$0, -80(%rbp)
	movl	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movq	%rax, -392(%rbp)
	movups	%xmm0, 48(%r10)
	movups	%xmm0, 64(%r10)
	movl	56(%r14), %esi
.L2505:
	cmpl	$125, %esi
	jg	.L2506
.L2891:
	cmpl	$35, %esi
	jle	.L2507
	leal	-36(%rsi), %eax
	cmpl	$89, %eax
	ja	.L2507
	movslq	(%r12,%rax,4), %rax
	addq	%r12, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal12RegExpParser16ParseDisjunctionEv,"a",@progbits
	.align 4
	.align 4
.L2509:
	.long	.L2519-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2518-.L2509
	.long	.L2517-.L2509
	.long	.L2515-.L2509
	.long	.L2515-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2516-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2515-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2514-.L2509
	.long	.L2513-.L2509
	.long	.L2508-.L2509
	.long	.L2512-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2507-.L2509
	.long	.L2511-.L2509
	.long	.L2510-.L2509
	.long	.L2508-.L2509
	.section	.text._ZN2v88internal12RegExpParser16ParseDisjunctionEv
.L2511:
	leaq	-380(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rsi, %rdx
	call	_ZN2v88internal12RegExpParser23ParseIntervalQuantifierEPiS2_
	cmpb	$0, 81(%r14)
	jne	.L2530
	testb	%al, %al
	jne	.L2888
.L2508:
	testb	$16, 60(%r14)
	jne	.L2723
	movl	56(%r14), %esi
.L2507:
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder19AddUnicodeCharacterEi
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
.L2537:
	movl	56(%r14), %esi
	cmpl	$63, %esi
	je	.L2725
.L2897:
	jg	.L2726
	cmpl	$42, %esi
	je	.L2727
	cmpl	$43, %esi
	jne	.L2505
	movl	$1, -376(%rbp)
	movq	%r14, %rdi
	movl	$2147483647, -372(%rbp)
	call	_ZN2v88internal12RegExpParser7AdvanceEv
.L2730:
	xorl	%ecx, %ecx
	cmpl	$63, 56(%r14)
	je	.L2889
.L2735:
	movl	-372(%rbp), %edx
	movl	-376(%rbp), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder19AddQuantifierToAtomEiiNS0_16RegExpQuantifier14QuantifierTypeE
	testb	%al, %al
	je	.L2890
.L2736:
	movl	56(%r14), %esi
	cmpl	$125, %esi
	jle	.L2891
.L2506:
	cmpl	$2097152, %esi
	jne	.L2507
	movq	-392(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L2520
	cmpb	$0, 81(%r14)
	je	.L2892
	.p2align 4,,10
	.p2align 3
.L2530:
	xorl	%r15d, %r15d
.L2502:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2893
	addq	$376, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2726:
	.cfi_restore_state
	cmpl	$123, %esi
	jne	.L2505
	leaq	-372(%rbp), %rdx
	leaq	-376(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser23ParseIntervalQuantifierEPiS2_
	testb	%al, %al
	je	.L2731
	movl	-376(%rbp), %eax
	cmpl	%eax, -372(%rbp)
	jge	.L2730
	cmpb	$0, 81(%r14)
	jne	.L2530
	leaq	.LC43(%rip), %rax
	movb	$1, 81(%r14)
	movq	(%r14), %rdi
	leaq	-208(%rbp), %rsi
	movq	%rax, -208(%rbp)
	movq	$37, -200(%rbp)
	jmp	.L2862
.L2510:
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder10FlushTermsEv
	movl	56(%r14), %esi
	jmp	.L2505
.L2512:
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movq	8(%r14), %rdi
	movl	12(%r13), %edx
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	testb	$4, %dl
	jne	.L2894
	cmpq	$15, %rax
	jbe	.L2895
	leaq	16(%r15), %rax
	movq	%rax, 16(%rdi)
.L2554:
	leaq	16+_ZTVN2v88internal15RegExpAssertionE(%rip), %rax
	movl	$1, 8(%r15)
	movq	%r13, %rdi
	movq	%rax, (%r15)
	movl	%edx, 12(%r15)
	call	_ZN2v88internal13RegExpBuilder9FlushTextEv
	cmpq	$0, 40(%r13)
	je	.L2555
	movq	32(%r13), %rcx
	movq	0(%r13), %rdi
	testq	%rcx, %rcx
	je	.L2896
.L2556:
	movslq	12(%rcx), %rax
	movl	8(%rcx), %edx
	cmpl	%edx, %eax
	jge	.L2561
	leal	1(%rax), %esi
	movq	(%rcx), %rdx
	movl	%esi, 12(%rcx)
	movq	40(%r13), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L2555:
	movq	%r15, 40(%r13)
	movl	56(%r14), %esi
	movb	$1, 78(%r14)
	jmp	.L2505
.L2513:
	movq	48(%r14), %rdx
	movslq	64(%r14), %rax
	movl	36(%rdx), %esi
	cmpl	%eax, %esi
	jle	.L2586
	movl	60(%r14), %ecx
	movq	40(%rdx), %r8
	leal	1(%rax), %edi
	andl	$16, %ecx
	setne	%r9b
	cmpb	$0, 32(%rdx)
	je	.L2587
	movzbl	(%r8,%rax), %r15d
.L2588:
	leal	-48(%r15), %eax
	cmpl	$72, %eax
	ja	.L2590
	leaq	.L2592(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal12RegExpParser16ParseDisjunctionEv
	.align 4
	.align 4
.L2592:
	.long	.L2606-.L2592
	.long	.L2605-.L2592
	.long	.L2605-.L2592
	.long	.L2605-.L2592
	.long	.L2605-.L2592
	.long	.L2605-.L2592
	.long	.L2605-.L2592
	.long	.L2605-.L2592
	.long	.L2605-.L2592
	.long	.L2605-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2604-.L2592
	.long	.L2590-.L2592
	.long	.L2593-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2598-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2593-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2593-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2603-.L2592
	.long	.L2602-.L2592
	.long	.L2593-.L2592
	.long	.L2590-.L2592
	.long	.L2601-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2600-.L2592
	.long	.L2590-.L2592
	.long	.L2590-.L2592
	.long	.L2599-.L2592
	.long	.L2590-.L2592
	.long	.L2598-.L2592
	.long	.L2590-.L2592
	.long	.L2597-.L2592
	.long	.L2593-.L2592
	.long	.L2596-.L2592
	.long	.L2595-.L2592
	.long	.L2594-.L2592
	.long	.L2593-.L2592
	.long	.L2591-.L2592
	.section	.text._ZN2v88internal12RegExpParser16ParseDisjunctionEv
.L2514:
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal12RegExpParser19ParseCharacterClassEPKNS0_13RegExpBuilderE
	cmpb	$0, 81(%r14)
	movq	%rax, %rdi
	jne	.L2530
	movq	(%rax), %rax
	call	*136(%rax)
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal13RegExpBuilder17AddCharacterClassEPNS0_20RegExpCharacterClassE
	movl	56(%r14), %esi
	cmpl	$63, %esi
	jne	.L2897
	.p2align 4,,10
	.p2align 3
.L2725:
	movl	$0, -376(%rbp)
	movq	%r14, %rdi
	movl	$1, -372(%rbp)
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	xorl	%ecx, %ecx
	cmpl	$63, 56(%r14)
	jne	.L2735
.L2889:
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	$1, %ecx
	jmp	.L2735
.L2516:
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movq	8(%r14), %rdi
	movq	24(%rdi), %rax
	movq	16(%rdi), %r15
	movq	%rax, %rdx
	subq	%r15, %rdx
	cmpq	$15, %rdx
	jbe	.L2898
	leaq	16(%r15), %rdx
	movq	%rdx, 16(%rdi)
.L2579:
	subq	%rdx, %rax
	cmpq	$15, %rax
	jbe	.L2899
	leaq	16(%rdx), %rax
	movq	%rax, 16(%rdi)
.L2581:
	movq	%rdx, (%r15)
	movq	%r15, %rsi
	movl	$0, %edx
	movq	$2, 8(%r15)
	testb	$32, 12(%r13)
	movq	8(%r14), %rcx
	jne	.L2900
	movl	$46, %edi
	call	_ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEbPNS0_4ZoneE@PLT
.L2583:
	movq	8(%r14), %r10
	movl	12(%r13), %ecx
	movq	16(%r10), %r9
	movq	24(%r10), %rax
	subq	%r9, %rax
	cmpq	$31, %rax
	jbe	.L2901
	leaq	32(%r9), %rax
	movq	%rax, 16(%r10)
.L2585:
	movq	%r10, %rsi
	movq	%r9, %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rdx
	movq	%r9, -400(%rbp)
	call	_ZN2v88internal20RegExpCharacterClassC1EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	movq	-400(%rbp), %r9
	movq	%r13, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal13RegExpBuilder17AddCharacterClassEPNS0_20RegExpCharacterClassE
	jmp	.L2537
.L2515:
	cmpb	$0, 81(%r14)
	jne	.L2530
	leaq	.LC38(%rip), %rax
	movb	$1, 81(%r14)
	movq	(%r14), %rdi
	leaq	-336(%rbp), %rsi
	movq	%rax, -336(%rbp)
	movq	$17, -328(%rbp)
.L2862:
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r14), %rdx
	testq	%rax, %rax
	je	.L2526
	movq	%rax, (%rdx)
	movq	48(%r14), %rax
	movl	$2097152, 56(%r14)
	movl	36(%rax), %eax
	movl	%eax, 64(%r14)
	jmp	.L2530
.L2517:
	movq	-392(%rbp), %rax
	movq	(%rax), %r15
	testq	%r15, %r15
	je	.L2902
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder8ToRegExpEv
	movq	%rax, %r13
	movq	-392(%rbp), %rax
	movl	24(%rax), %r15d
	movl	16(%rax), %eax
	cmpl	$1, %eax
	je	.L2903
	movq	8(%r14), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %r9
	subq	%rsi, %r9
	cmpl	$4, %eax
	je	.L2904
	cmpl	$2, %eax
	movq	-392(%rbp), %rax
	movl	68(%r14), %edx
	sete	%r8b
	movl	20(%rax), %ecx
	cmpq	$31, %r9
	jbe	.L2905
	leaq	32(%rsi), %rax
	movq	%rax, 16(%rdi)
.L2536:
	leaq	16+_ZTVN2v88internal16RegExpLookaroundE(%rip), %rax
	subl	%r15d, %edx
	movq	%r13, 8(%rsi)
	movq	%rax, (%rsi)
	movb	%r8b, 16(%rsi)
	movl	%edx, 20(%rsi)
	movl	%r15d, 24(%rsi)
	movl	%ecx, 28(%rsi)
.L2529:
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	movq	8(%rax), %r13
	movq	%rax, -392(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder7AddAtomEPNS0_10RegExpTreeE
	jmp	.L2537
.L2518:
	movq	-392(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser20ParseOpenParenthesisEPNS1_17RegExpParserStateE
	cmpb	$0, 81(%r14)
	movq	%rax, -392(%rbp)
	jne	.L2530
	movq	8(%rax), %r13
	movl	56(%r14), %esi
	jmp	.L2505
.L2519:
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movq	8(%r14), %rdi
	movl	12(%r13), %ecx
	xorl	%edx, %edx
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	testb	$4, %cl
	sete	%dl
	subq	%r15, %rax
	addl	$2, %edx
	cmpq	$15, %rax
	jbe	.L2906
	leaq	16(%r15), %rax
	movq	%rax, 16(%rdi)
.L2567:
	leaq	16+_ZTVN2v88internal15RegExpAssertionE(%rip), %rax
	movl	%edx, 8(%r15)
	movq	%r13, %rdi
	movq	%rax, (%r15)
	movl	%ecx, 12(%r15)
	call	_ZN2v88internal13RegExpBuilder9FlushTextEv
	cmpq	$0, 40(%r13)
	je	.L2568
.L2882:
	movq	32(%r13), %rcx
	movq	0(%r13), %rdi
	testq	%rcx, %rcx
	je	.L2907
.L2569:
	movslq	12(%rcx), %rax
	movl	8(%rcx), %edx
	cmpl	%edx, %eax
	jge	.L2574
	leal	1(%rax), %esi
	movq	(%rcx), %rdx
	movl	%esi, 12(%rcx)
	movq	40(%r13), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L2568:
	movq	%r15, 40(%r13)
	movl	56(%r14), %esi
	jmp	.L2505
	.p2align 4,,10
	.p2align 3
.L2731:
	testb	$16, 60(%r14)
	je	.L2736
	cmpb	$0, 81(%r14)
	jne	.L2530
	leaq	.LC44(%rip), %rax
	movb	$1, 81(%r14)
	movq	(%r14), %rdi
	leaq	-192(%rbp), %rsi
	movq	%rax, -192(%rbp)
	movq	$21, -184(%rbp)
	jmp	.L2862
	.p2align 4,,10
	.p2align 3
.L2727:
	movl	$0, -376(%rbp)
	movq	%r14, %rdi
	movl	$2147483647, -372(%rbp)
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	jmp	.L2730
	.p2align 4,,10
	.p2align 3
.L2900:
	movl	$42, %edi
	call	_ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEbPNS0_4ZoneE@PLT
	jmp	.L2583
	.p2align 4,,10
	.p2align 3
.L2894:
	cmpq	$15, %rax
	jbe	.L2908
	leaq	16(%r15), %rax
	movq	%rax, 16(%rdi)
.L2542:
	leaq	16+_ZTVN2v88internal15RegExpAssertionE(%rip), %rax
	movl	$0, 8(%r15)
	movq	%r13, %rdi
	movq	%rax, (%r15)
	movl	%edx, 12(%r15)
	call	_ZN2v88internal13RegExpBuilder9FlushTextEv
	cmpq	$0, 40(%r13)
	jne	.L2882
	jmp	.L2568
	.p2align 4,,10
	.p2align 3
.L2587:
	movzwl	(%r8,%rax,2), %r15d
	movl	%r15d, %eax
	cmpl	%edi, %esi
	jle	.L2588
	testb	%r9b, %r9b
	je	.L2588
	andw	$-1024, %ax
	cmpw	$-10240, %ax
	jne	.L2588
	.p2align 4,,10
	.p2align 3
.L2590:
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	testb	$16, 60(%r14)
	movl	56(%r14), %edx
	je	.L2718
	cmpl	$94, %edx
	jg	.L2716
	cmpl	$35, %edx
	jle	.L2717
	leal	-36(%rdx), %eax
	cmpl	$58, %eax
	ja	.L2717
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal12RegExpParser16ParseDisjunctionEv
	.align 4
	.align 4
.L2719:
	.long	.L2718-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2718-.L2719
	.long	.L2718-.L2719
	.long	.L2718-.L2719
	.long	.L2718-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2718-.L2719
	.long	.L2718-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2718-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2718-.L2719
	.long	.L2718-.L2719
	.long	.L2718-.L2719
	.long	.L2718-.L2719
	.section	.text._ZN2v88internal12RegExpParser16ParseDisjunctionEv
	.p2align 4,,10
	.p2align 3
.L2892:
	leaq	.LC36(%rip), %rax
	movb	$1, 81(%r14)
	movq	(%r14), %rdi
	leaq	-368(%rbp), %rsi
	movq	%rax, -368(%rbp)
	movq	$18, -360(%rbp)
	jmp	.L2862
	.p2align 4,,10
	.p2align 3
.L2520:
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder8ToRegExpEv
	movq	%rax, %r15
	jmp	.L2502
	.p2align 4,,10
	.p2align 3
.L2904:
	cmpq	$15, %r9
	jbe	.L2909
	leaq	16(%rsi), %rax
	movq	%rax, 16(%rdi)
.L2534:
	leaq	16+_ZTVN2v88internal11RegExpGroupE(%rip), %rax
	movq	%r13, 8(%rsi)
	movq	%rax, (%rsi)
	jmp	.L2529
	.p2align 4,,10
	.p2align 3
.L2574:
	movq	16(%rdi), %r10
	movq	24(%rdi), %rax
	leal	1(%rdx,%rdx), %r9d
	movslq	%r9d, %rsi
	movq	40(%r13), %r8
	salq	$3, %rsi
	subq	%r10, %rax
	cmpq	%rax, %rsi
	ja	.L2910
	addq	%r10, %rsi
	movq	%rsi, 16(%rdi)
.L2576:
	movslq	12(%rcx), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L2577
	movl	%r9d, -416(%rbp)
	movq	%r10, %rdi
	movq	%r8, -408(%rbp)
	movq	(%rcx), %rsi
	movq	%rcx, -400(%rbp)
	call	memcpy@PLT
	movq	-400(%rbp), %rcx
	movl	-416(%rbp), %r9d
	movq	-408(%rbp), %r8
	movq	%rax, %r10
	movslq	12(%rcx), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
.L2577:
	addl	$1, %eax
	movq	%r10, (%rcx)
	movl	%r9d, 8(%rcx)
	movl	%eax, 12(%rcx)
	movq	%r8, (%r10,%rdx)
	jmp	.L2568
	.p2align 4,,10
	.p2align 3
.L2903:
	movq	-392(%rbp), %rax
	movq	32(%rax), %rsi
	testq	%rsi, %rsi
	je	.L2531
	movl	%r15d, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser25CreateNamedCaptureAtIndexEPKNS0_10ZoneVectorItEEi
	cmpb	$0, 81(%r14)
	jne	.L2530
.L2531:
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser10GetCaptureEi
	movq	%r13, 8(%rax)
	movq	%rax, %rsi
	jmp	.L2529
	.p2align 4,,10
	.p2align 3
.L2887:
	movl	$80, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r10
	jmp	.L2504
	.p2align 4,,10
	.p2align 3
.L2586:
	cmpb	$0, 81(%r14)
	jne	.L2530
	leaq	.LC26(%rip), %rax
	movb	$1, 81(%r14)
	movq	(%r14), %rdi
	leaq	-320(%rbp), %rsi
	movq	%rax, -320(%rbp)
	movq	$19, -312(%rbp)
	jmp	.L2862
	.p2align 4,,10
	.p2align 3
.L2907:
	movq	24(%rdi), %rax
	movq	16(%rdi), %rdx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	cmpq	$15, %rcx
	jbe	.L2911
	leaq	16(%rdx), %rcx
	movq	%rcx, 16(%rdi)
	movq	%rcx, %rsi
.L2571:
	subq	%rsi, %rax
	movq	%rdx, %rcx
	cmpq	$15, %rax
	jbe	.L2912
	leaq	16(%rsi), %rax
	movq	%rax, 16(%rdi)
.L2573:
	movq	%rsi, (%rdx)
	movq	$2, 8(%rdx)
	movq	%rdx, 32(%r13)
	jmp	.L2569
.L2716:
	leal	-123(%rdx), %eax
	cmpl	$2, %eax
	ja	.L2717
.L2718:
	movq	%r13, %rdi
	movzwl	%dx, %esi
	call	_ZN2v88internal13RegExpBuilder12AddCharacterEt
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	jmp	.L2537
.L2717:
	cmpb	$0, 81(%r14)
	jne	.L2530
	leaq	.LC7(%rip), %rax
	movb	$1, 81(%r14)
	movq	(%r14), %rdi
	leaq	-256(%rbp), %rsi
	movq	%rax, -256(%rbp)
	movq	$14, -248(%rbp)
	jmp	.L2862
.L2605:
	leaq	-128(%rbp), %rsi
	movq	%r14, %rdi
	movl	$0, -128(%rbp)
	call	_ZN2v88internal12RegExpParser23ParseBackReferenceIndexEPi
	cmpb	$0, 81(%r14)
	jne	.L2530
	testb	%al, %al
	jne	.L2913
	testb	$16, 60(%r14)
	jne	.L2847
	movq	48(%r14), %rdx
	movslq	64(%r14), %rax
	cmpl	36(%rdx), %eax
	jge	.L2606
	cmpb	$0, 32(%rdx)
	movq	40(%rdx), %rcx
	je	.L2664
	movzbl	(%rcx,%rax), %esi
.L2665:
	leal	-56(%rsi), %eax
	cmpl	$1, %eax
	jbe	.L2914
.L2606:
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	testb	$16, 60(%r14)
	je	.L2666
	movq	48(%r14), %rdx
	movl	64(%r14), %eax
	movl	36(%rdx), %ecx
	cmpl	%ecx, %eax
	jge	.L2666
	movslq	%eax, %rsi
	addl	$1, %eax
	cmpb	$0, 32(%rdx)
	movq	40(%rdx), %rdi
	je	.L2667
	cmpb	$47, (%rdi,%rsi)
	jbe	.L2666
	movzbl	(%rdi,%rsi), %esi
.L2671:
	cmpl	$57, %esi
	jle	.L2672
.L2666:
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser17ParseOctalLiteralEv
	movq	%r13, %rdi
	movzwl	%ax, %esi
	call	_ZN2v88internal13RegExpBuilder12AddCharacterEt
	jmp	.L2537
.L2593:
	movl	%edi, 64(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movq	8(%r14), %rdi
	movq	24(%rdi), %rax
	movq	16(%rdi), %r9
	movq	%rax, %rdx
	subq	%r9, %rdx
	cmpq	$15, %rdx
	jbe	.L2915
	leaq	16(%r9), %rdx
	movq	%rdx, 16(%rdi)
.L2633:
	subq	%rdx, %rax
	cmpq	$15, %rax
	jbe	.L2916
	leaq	16(%rdx), %rax
	movq	%rax, 16(%rdi)
.L2635:
	movq	%rdx, (%r9)
	movq	$2, 8(%r9)
	movl	60(%r14), %edx
	movq	8(%r14), %rcx
	andl	$16, %edx
	je	.L2636
	movl	12(%r13), %edx
	shrl	%edx
	andl	$1, %edx
.L2636:
	movq	%r9, %rsi
	movl	%r15d, %edi
	movq	%r9, -400(%rbp)
	call	_ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEbPNS0_4ZoneE@PLT
	movq	8(%r14), %r10
	movl	12(%r13), %ecx
	movq	-400(%rbp), %r9
	movq	16(%r10), %r15
	movq	24(%r10), %rax
	subq	%r15, %rax
	cmpq	$31, %rax
	jbe	.L2917
	leaq	32(%r15), %rax
	movq	%rax, 16(%r10)
.L2638:
	movq	%r10, %rsi
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movq	%r9, %rdx
	call	_ZN2v88internal20RegExpCharacterClassC1EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder17AddCharacterClassEPNS0_20RegExpCharacterClassE
	jmp	.L2537
.L2598:
	movl	%edi, 64(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	testb	$16, 60(%r14)
	je	.L2639
	movq	8(%r14), %rdi
	movq	24(%rdi), %rdx
	movq	16(%rdi), %r9
	movq	%rdx, %rax
	subq	%r9, %rax
	cmpq	$15, %rax
	jbe	.L2918
	leaq	16(%r9), %rax
	movq	%rax, 16(%rdi)
.L2641:
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L2919
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L2643:
	movq	%rax, (%r9)
	pxor	%xmm0, %xmm0
	movq	$2, 8(%r9)
	cmpl	$123, 56(%r14)
	movq	$0, -144(%rbp)
	movq	$0, -112(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -128(%rbp)
	je	.L2920
.L2644:
	cmpb	$0, 81(%r14)
	jne	.L2649
	movb	$1, 81(%r14)
	movq	(%r14), %rdi
	leaq	.LC39(%rip), %rax
	xorl	%edx, %edx
	leaq	-304(%rbp), %rsi
	movq	%rax, -304(%rbp)
	movq	$21, -296(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r14), %rdx
	testq	%rax, %rax
	je	.L2526
	movq	%rax, (%rdx)
	movq	48(%r14), %rax
	movl	$2097152, 56(%r14)
	movl	36(%rax), %eax
	movl	%eax, 64(%r14)
.L2649:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2650
	call	_ZdlPv@PLT
.L2650:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2530
	call	_ZdlPv@PLT
	jmp	.L2530
.L2888:
	leaq	.LC38(%rip), %rax
	movb	$1, 81(%r14)
	movq	(%r14), %rdi
	leaq	-240(%rbp), %rsi
	movq	%rax, -240(%rbp)
	movq	$17, -232(%rbp)
	jmp	.L2862
.L2604:
	movl	%edi, 64(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movq	8(%r14), %rdi
	movl	12(%r13), %r15d
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	$15, %rax
	jbe	.L2921
	leaq	16(%rcx), %rax
	movq	%rax, 16(%rdi)
.L2621:
	leaq	16+_ZTVN2v88internal15RegExpAssertionE(%rip), %rax
	movl	$5, 8(%rcx)
	movq	%rax, (%rcx)
.L2886:
	movl	%r15d, 12(%rcx)
	movq	%r13, %rdi
	movq	%rcx, -400(%rbp)
	call	_ZN2v88internal13RegExpBuilder9FlushTextEv
	cmpq	$0, 40(%r13)
	movq	-400(%rbp), %rcx
	je	.L2622
	movq	32(%r13), %r8
	movq	0(%r13), %rdi
	testq	%r8, %r8
	je	.L2922
.L2623:
	movslq	12(%r8), %rax
	movl	8(%r8), %edx
	cmpl	%edx, %eax
	jge	.L2628
	leal	1(%rax), %esi
	movq	(%r8), %rdx
	movl	%esi, 12(%r8)
	movq	40(%r13), %rsi
	movq	%rsi, (%rdx,%rax,8)
.L2622:
	movq	%rcx, 40(%r13)
	movl	56(%r14), %esi
	jmp	.L2505
.L2601:
	movl	%edi, 64(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	$12, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder12AddCharacterEt
	jmp	.L2537
.L2602:
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movq	48(%r14), %rdx
	movl	64(%r14), %eax
	movl	36(%rdx), %ecx
	cmpl	%ecx, %eax
	jge	.L2923
	cmpb	$0, 32(%rdx)
	movslq	%eax, %rsi
	je	.L2675
	movq	40(%rdx), %rdx
	movzbl	(%rdx,%rsi), %r15d
	leal	1(%rax), %esi
	movl	60(%r14), %eax
	andl	$16, %eax
	je	.L2678
	cmpl	%esi, %ecx
	jg	.L2676
.L2678:
	movl	%r15d, %edx
	andl	$-33, %edx
	subl	$65, %edx
	cmpl	$25, %edx
	jbe	.L2680
.L2674:
	testl	%eax, %eax
	jne	.L2679
	movl	$92, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder12AddCharacterEt
	jmp	.L2537
.L2603:
	movl	%edi, 64(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movq	8(%r14), %rdi
	movl	12(%r13), %r15d
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	$15, %rax
	jbe	.L2924
	leaq	16(%rcx), %rax
	movq	%rax, 16(%rdi)
.L2609:
	leaq	16+_ZTVN2v88internal15RegExpAssertionE(%rip), %rax
	movl	$4, 8(%rcx)
	movq	%rax, (%rcx)
	jmp	.L2886
.L2591:
	movl	%edi, 64(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	56(%r14), %eax
	movl	64(%r14), %r15d
	leal	-48(%rax), %ecx
	cmpl	$9, %ecx
	jbe	.L2682
	orl	$32, %ecx
	leal	-49(%rcx), %eax
	cmpl	$5, %eax
	ja	.L2855
	subl	$39, %ecx
	js	.L2855
.L2682:
	movq	48(%r14), %rax
	cmpl	36(%rax), %r15d
	jl	.L2684
	movl	$2097152, 56(%r14)
	movl	36(%rax), %edx
	movb	$0, 76(%r14)
	addl	$1, %edx
	movl	%edx, 64(%r14)
.L2685:
	movl	56(%r14), %edx
	subl	$48, %edx
	cmpl	$9, %edx
	jbe	.L2698
	orl	$32, %edx
	leal	-49(%rdx), %esi
	cmpl	$5, %esi
	ja	.L2683
	subl	$39, %edx
	js	.L2683
.L2698:
	sall	$4, %ecx
	leal	(%rcx,%rdx), %r15d
	movl	36(%rax), %ecx
	cmpl	%ecx, 64(%r14)
	jl	.L2925
	movl	$2097152, 56(%r14)
	movl	36(%rax), %eax
	movb	$0, 76(%r14)
	addl	$1, %eax
	movl	%eax, 64(%r14)
.L2705:
	movzwl	%r15w, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder12AddCharacterEt
	jmp	.L2537
.L2596:
	movl	%edi, 64(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	$9, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder12AddCharacterEt
	jmp	.L2537
.L2594:
	movl	%edi, 64(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	$11, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder12AddCharacterEt
	jmp	.L2537
.L2595:
	movl	%edi, 64(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	leaq	-128(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser18ParseUnicodeEscapeEPi
	testb	%al, %al
	jne	.L2926
	testb	$16, 60(%r14)
	jne	.L2713
	movl	$117, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder12AddCharacterEt
	jmp	.L2537
.L2599:
	movl	%edi, 64(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	$10, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder12AddCharacterEt
	jmp	.L2537
.L2600:
	testl	%ecx, %ecx
	jne	.L2714
	cmpb	$0, 80(%r14)
	jne	.L2714
	cmpb	$0, 79(%r14)
	jne	.L2590
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser15ScanForCapturesEv
	cmpb	$0, 80(%r14)
	je	.L2590
	movl	64(%r14), %eax
	leal	1(%rax), %edi
.L2714:
	movl	%edi, 64(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movq	-392(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser23ParseNamedBackReferenceEPNS0_13RegExpBuilderEPNS1_17RegExpParserStateE
	cmpb	$0, 81(%r14)
	je	.L2537
	jmp	.L2530
	.p2align 4,,10
	.p2align 3
.L2597:
	movl	%edi, 64(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	$13, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder12AddCharacterEt
	jmp	.L2537
.L2526:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2561:
	movq	16(%rdi), %r10
	movq	24(%rdi), %rax
	leal	1(%rdx,%rdx), %r9d
	movslq	%r9d, %rsi
	movq	40(%r13), %r8
	salq	$3, %rsi
	subq	%r10, %rax
	cmpq	%rax, %rsi
	ja	.L2927
	addq	%r10, %rsi
	movq	%rsi, 16(%rdi)
.L2563:
	movslq	12(%rcx), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L2564
	movq	(%rcx), %rsi
	movq	%r10, %rdi
	movl	%r9d, -416(%rbp)
	movq	%r8, -408(%rbp)
	movq	%rcx, -400(%rbp)
	call	memcpy@PLT
	movq	-400(%rbp), %rcx
	movl	-416(%rbp), %r9d
	movq	-408(%rbp), %r8
	movq	%rax, %r10
	movslq	12(%rcx), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
.L2564:
	addl	$1, %eax
	movq	%r10, (%rcx)
	movl	%r9d, 8(%rcx)
	movl	%eax, 12(%rcx)
	movq	%r8, (%r10,%rdx)
	jmp	.L2555
.L2906:
	movl	$16, %esi
	movl	%ecx, -408(%rbp)
	movl	%edx, -400(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-400(%rbp), %edx
	movl	-408(%rbp), %ecx
	movq	%rax, %r15
	jmp	.L2567
.L2898:
	movl	$16, %esi
	movq	%rdi, -400(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-400(%rbp), %rdi
	movq	%rax, %r15
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	jmp	.L2579
.L2899:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L2581
.L2901:
	movq	%r10, %rdi
	movl	$32, %esi
	movl	%ecx, -408(%rbp)
	movq	%r10, -400(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-400(%rbp), %r10
	movl	-408(%rbp), %ecx
	movq	%rax, %r9
	jmp	.L2585
.L2890:
	cmpb	$0, 81(%r14)
	jne	.L2530
	leaq	.LC45(%rip), %rax
	movb	$1, 81(%r14)
	movq	(%r14), %rdi
	leaq	-176(%rbp), %rsi
	movq	%rax, -176(%rbp)
	movq	$18, -168(%rbp)
	jmp	.L2862
.L2896:
	movq	24(%rdi), %rax
	movq	16(%rdi), %rdx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	cmpq	$15, %rcx
	jbe	.L2928
	leaq	16(%rdx), %rcx
	movq	%rcx, 16(%rdi)
	movq	%rcx, %rsi
.L2558:
	subq	%rsi, %rax
	movq	%rdx, %rcx
	cmpq	$15, %rax
	jbe	.L2929
	leaq	16(%rsi), %rax
	movq	%rax, 16(%rdi)
.L2560:
	movq	%rsi, (%rdx)
	movq	$2, 8(%rdx)
	movq	%rdx, 32(%r13)
	jmp	.L2556
.L2895:
	movl	$16, %esi
	movl	%edx, -400(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-400(%rbp), %edx
	movq	%rax, %r15
	jmp	.L2554
.L2908:
	movl	$16, %esi
	movl	%edx, -400(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-400(%rbp), %edx
	movq	%rax, %r15
	jmp	.L2542
.L2905:
	movl	$32, %esi
	movl	%ecx, -416(%rbp)
	movl	%edx, -408(%rbp)
	movb	%r8b, -400(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-400(%rbp), %r8d
	movl	-408(%rbp), %edx
	movl	-416(%rbp), %ecx
	movq	%rax, %rsi
	jmp	.L2536
.L2723:
	cmpb	$0, 81(%r14)
	jne	.L2530
	leaq	.LC42(%rip), %rax
	movb	$1, 81(%r14)
	movq	(%r14), %rdi
	leaq	-224(%rbp), %rsi
	movq	%rax, -224(%rbp)
	movq	$24, -216(%rbp)
	jmp	.L2862
.L2910:
	movl	%r9d, -416(%rbp)
	movq	%r8, -408(%rbp)
	movq	%rcx, -400(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-400(%rbp), %rcx
	movq	-408(%rbp), %r8
	movl	-416(%rbp), %r9d
	movq	%rax, %r10
	jmp	.L2576
.L2855:
	movq	48(%r14), %rax
.L2683:
	subl	$1, %r15d
	movq	%r14, %rdi
	movl	%r15d, 64(%r14)
	cmpl	36(%rax), %r15d
	setl	76(%r14)
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	testb	$16, 60(%r14)
	jne	.L2847
	movl	$120, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder12AddCharacterEt
	jmp	.L2537
.L2639:
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder12AddCharacterEt
	jmp	.L2537
.L2909:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rsi
	jmp	.L2534
.L2628:
	movq	40(%r13), %rax
	leal	1(%rdx,%rdx), %r15d
	movslq	%r15d, %rsi
	movq	%rax, -400(%rbp)
	movq	16(%rdi), %r9
	salq	$3, %rsi
	movq	24(%rdi), %rax
	subq	%r9, %rax
	cmpq	%rax, %rsi
	ja	.L2930
	addq	%r9, %rsi
	movq	%rsi, 16(%rdi)
.L2630:
	movslq	12(%r8), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L2631
	movq	%rcx, -416(%rbp)
	movq	(%r8), %rsi
	movq	%r9, %rdi
	movq	%r8, -408(%rbp)
	call	memcpy@PLT
	movq	-408(%rbp), %r8
	movq	-416(%rbp), %rcx
	movq	%rax, %r9
	movslq	12(%r8), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
.L2631:
	addl	$1, %eax
	movq	%r9, (%r8)
	movl	%r15d, 8(%r8)
	movl	%eax, 12(%r8)
	movq	-400(%rbp), %rax
	movq	%rax, (%r9,%rdx)
	jmp	.L2622
.L2902:
	cmpb	$0, 81(%r14)
	jne	.L2502
	movb	$1, 81(%r14)
	movq	(%r14), %rdi
	leaq	.LC37(%rip), %rax
	xorl	%edx, %edx
	leaq	-352(%rbp), %rsi
	movq	%rax, -352(%rbp)
	movq	$13, -344(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r14), %rdx
	testq	%rax, %rax
	je	.L2526
	movq	%rax, (%rdx)
	movq	48(%r14), %rax
	movl	$2097152, 56(%r14)
	movl	36(%rax), %eax
	movl	%eax, 64(%r14)
	jmp	.L2502
.L2912:
	movl	$16, %esi
	movq	%rdx, -416(%rbp)
	movq	%rdx, -408(%rbp)
	movq	%rdi, -400(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-400(%rbp), %rdi
	movq	-408(%rbp), %rcx
	movq	-416(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L2573
.L2911:
	movl	$16, %esi
	movq	%rdi, -400(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-400(%rbp), %rdi
	movq	%rax, %rdx
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	jmp	.L2571
.L2667:
	movzwl	(%rdi,%rsi,2), %edx
	cmpl	%eax, %ecx
	jle	.L2669
	movl	%edx, %r8d
	andw	$-1024, %r8w
	cmpw	$-10240, %r8w
	jne	.L2669
	movzwl	%dx, %esi
.L2741:
	andw	$-1024, %dx
	cmpw	$-10240, %dx
	je	.L2666
	jmp	.L2671
	.p2align 4,,10
	.p2align 3
.L2675:
	movq	40(%rdx), %rdi
	movzwl	(%rdi,%rsi,2), %r15d
	leal	1(%rax), %esi
	movl	60(%r14), %eax
	movl	%r15d, %edx
	andl	$16, %eax
	cmpl	%esi, %ecx
	jle	.L2678
	testl	%eax, %eax
	je	.L2678
	andw	$-1024, %dx
	cmpw	$-10240, %dx
	je	.L2931
.L2676:
	movl	%r15d, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpl	$25, %eax
	ja	.L2679
.L2680:
	movl	%esi, 64(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movl	%r15d, %esi
	movq	%r13, %rdi
	andl	$31, %esi
	call	_ZN2v88internal13RegExpBuilder12AddCharacterEt
	jmp	.L2537
.L2926:
	movl	-128(%rbp), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder26AddEscapedUnicodeCharacterEi
	jmp	.L2537
.L2684:
	movq	(%r14), %rdx
	movl	%ecx, -408(%rbp)
	movq	%rdx, -400(%rbp)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	-400(%rbp), %rdx
	movl	-408(%rbp), %ecx
	cmpq	%rax, 37528(%rdx)
	ja	.L2686
	movq	8(%r14), %rax
	cmpq	$268435456, 8(%rax)
	ja	.L2687
	movq	48(%r14), %rax
	movslq	64(%r14), %rdx
	movzbl	32(%rax), %r9d
	movq	40(%rax), %rdi
	movq	%rdx, %r8
	testb	%r9b, %r9b
	je	.L2932
	movzbl	(%rdi,%rdx), %edx
.L2692:
	leal	1(%r8), %esi
	testb	$16, 60(%r14)
	je	.L2691
	cmpl	%esi, 36(%rax)
	jle	.L2691
	movl	%edx, %r10d
	andl	$64512, %r10d
	cmpl	$55296, %r10d
	jne	.L2691
	testb	%r9b, %r9b
	je	.L2933
.L2691:
	movl	%esi, 64(%r14)
	movl	%edx, 56(%r14)
	jmp	.L2685
.L2925:
	movq	(%r14), %rdx
	movq	%rdx, -400(%rbp)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	-400(%rbp), %rdx
	cmpq	37528(%rdx), %rax
	jnb	.L2702
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L2695
	movl	$216, %edi
	call	_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE@PLT
	cmpb	$0, 81(%r14)
	jne	.L2705
	movq	%rax, %rdi
	movq	%rax, -400(%rbp)
	call	strlen@PLT
	movq	-400(%rbp), %rdx
	movq	(%r14), %rdi
	movb	$1, 81(%r14)
	cltq
	leaq	-288(%rbp), %rsi
	movq	%rdx, -288(%rbp)
	movq	%rax, -280(%rbp)
.L2860:
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r14), %rdx
	testq	%rax, %rax
	je	.L2526
	movq	%rax, (%rdx)
	movq	48(%r14), %rax
	movl	$2097152, 56(%r14)
	movl	36(%rax), %eax
	movl	%eax, 64(%r14)
	jmp	.L2705
.L2664:
	movzwl	(%rcx,%rax,2), %esi
	jmp	.L2665
.L2922:
	movq	24(%rdi), %rdx
	movq	16(%rdi), %r15
	movq	%rdx, %rax
	subq	%r15, %rax
	cmpq	$15, %rax
	jbe	.L2934
	leaq	16(%r15), %rax
	movq	%rax, 16(%rdi)
.L2625:
	subq	%rax, %rdx
	movq	%r15, %r8
	cmpq	$15, %rdx
	jbe	.L2935
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L2627:
	movq	%rax, (%r15)
	movq	$2, 8(%r15)
	movq	%r15, 32(%r13)
	jmp	.L2623
.L2927:
	movl	%r9d, -416(%rbp)
	movq	%r8, -408(%rbp)
	movq	%rcx, -400(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-400(%rbp), %rcx
	movq	-408(%rbp), %r8
	movl	-416(%rbp), %r9d
	movq	%rax, %r10
	jmp	.L2563
.L2913:
	movl	-128(%rbp), %esi
	movq	-392(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L2659:
	cmpl	$1, 16(%rax)
	jne	.L2656
	cmpl	24(%rax), %esi
	je	.L2657
	jg	.L2658
.L2656:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L2659
.L2658:
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser10GetCaptureEi
	movq	8(%r14), %rdi
	movl	12(%r13), %edx
	movq	%rax, %r15
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$31, %rax
	jbe	.L2936
	leaq	32(%rsi), %rax
	movq	%rax, 16(%rdi)
.L2660:
	leaq	16+_ZTVN2v88internal19RegExpBackReferenceE(%rip), %rax
	movq	%r15, 8(%rsi)
	movq	%r13, %rdi
	movq	%rax, (%rsi)
	movq	$0, 16(%rsi)
	movl	%edx, 24(%rsi)
	call	_ZN2v88internal13RegExpBuilder7AddAtomEPNS0_10RegExpTreeE
	jmp	.L2537
.L2923:
	movl	60(%r14), %eax
	andl	$16, %eax
	jmp	.L2674
.L2657:
	movb	$1, 8(%r13)
	jmp	.L2537
.L2920:
	leaq	-128(%rbp), %r8
	leaq	-160(%rbp), %r10
	movq	%r14, %rdi
	movq	%r9, -416(%rbp)
	movq	%r8, %rdx
	movq	%r10, %rsi
	movq	%r8, -408(%rbp)
	movq	%r10, -400(%rbp)
	call	_ZN2v88internal12RegExpParser22ParsePropertyClassNameEPSt6vectorIcSaIcEES5_.part.0
	testb	%al, %al
	je	.L2644
	movq	-400(%rbp), %r10
	movq	-416(%rbp), %r9
	xorl	%edx, %edx
	cmpl	$80, %r15d
	movq	-408(%rbp), %r8
	sete	%dl
	movq	%r14, %rdi
	movq	%r10, %rcx
	movq	%r9, %rsi
	movq	%r10, -408(%rbp)
	movq	%r9, -400(%rbp)
	call	_ZN2v88internal12RegExpParser21AddPropertyClassRangeEPNS0_8ZoneListINS0_14CharacterRangeEEEbRKSt6vectorIcSaIcEESA_
	movq	-400(%rbp), %r9
	movq	-408(%rbp), %r10
	testb	%al, %al
	jne	.L2937
	cmpl	$112, %r15d
	jne	.L2644
	movq	-128(%rbp), %rax
	cmpq	%rax, -120(%rbp)
	jne	.L2644
	movq	%r10, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser19GetPropertySequenceERKSt6vectorIcSaIcEE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2644
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder7AddAtomEPNS0_10RegExpTreeE
.L2648:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2652
	call	_ZdlPv@PLT
.L2652:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2537
	call	_ZdlPv@PLT
	jmp	.L2537
.L2914:
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder12AddCharacterEt
	addl	$1, 64(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	jmp	.L2537
.L2686:
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	movl	%ecx, -400(%rbp)
	jne	.L2695
	movl	$216, %edi
	call	_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE@PLT
	cmpb	$0, 81(%r14)
	movl	-400(%rbp), %ecx
	jne	.L2854
	movq	%rax, %rdi
	movl	%ecx, -408(%rbp)
	movq	%rax, -400(%rbp)
	call	strlen@PLT
	movq	-400(%rbp), %rdx
	movq	(%r14), %rdi
	movb	$1, 81(%r14)
	cltq
	leaq	-288(%rbp), %rsi
	movq	%rdx, -288(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r14), %rdx
	testq	%rax, %rax
	je	.L2526
	movq	%rax, (%rdx)
	movq	48(%r14), %rax
	movl	$2097152, 56(%r14)
	movl	-408(%rbp), %ecx
	movl	36(%rax), %edx
	movl	%edx, 64(%r14)
	jmp	.L2685
.L2929:
	movl	$16, %esi
	movq	%rdx, -416(%rbp)
	movq	%rdx, -408(%rbp)
	movq	%rdi, -400(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-400(%rbp), %rdi
	movq	-408(%rbp), %rcx
	movq	-416(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L2560
.L2928:
	movl	$16, %esi
	movq	%rdi, -400(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-400(%rbp), %rdi
	movq	%rax, %rdx
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	jmp	.L2558
.L2669:
	cmpw	$47, %dx
	jbe	.L2666
	movzwl	(%rdi,%rsi,2), %esi
	movl	%esi, %edx
	cmpl	%eax, %ecx
	jg	.L2741
	jmp	.L2671
.L2702:
	movq	8(%r14), %rax
	cmpq	$268435456, 8(%rax)
	jbe	.L2706
	cmpb	$0, 81(%r14)
	jne	.L2705
	leaq	.LC3(%rip), %rax
	movb	$1, 81(%r14)
	movq	(%r14), %rdi
	leaq	-272(%rbp), %rsi
	movq	%rax, -272(%rbp)
	movq	$28, -264(%rbp)
	jmp	.L2860
.L2854:
	movq	48(%r14), %rax
	jmp	.L2685
.L2921:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L2621
.L2917:
	movq	%r10, %rdi
	movl	$32, %esi
	movl	%ecx, -416(%rbp)
	movq	%r9, -408(%rbp)
	movq	%r10, -400(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-400(%rbp), %r10
	movq	-408(%rbp), %r9
	movl	-416(%rbp), %ecx
	movq	%rax, %r15
	jmp	.L2638
.L2915:
	movl	$16, %esi
	movq	%rdi, -400(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-400(%rbp), %rdi
	movq	%rax, %r9
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	jmp	.L2633
.L2924:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L2609
.L2916:
	movl	$16, %esi
	movq	%r9, -400(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-400(%rbp), %r9
	movq	%rax, %rdx
	jmp	.L2635
.L2706:
	movq	48(%r14), %rdi
	movslq	64(%r14), %rax
	movzbl	32(%rdi), %r8d
	movq	40(%rdi), %rcx
	movq	%rax, %rsi
	testb	%r8b, %r8b
	je	.L2708
	movzbl	(%rcx,%rax), %eax
.L2709:
	leal	1(%rsi), %edx
	testb	$16, 60(%r14)
	je	.L2710
	cmpl	36(%rdi), %edx
	jge	.L2710
	movl	%eax, %edi
	andl	$64512, %edi
	cmpl	$55296, %edi
	jne	.L2710
	testb	%r8b, %r8b
	je	.L2938
.L2710:
	movl	%edx, 64(%r14)
	movl	%eax, 56(%r14)
	jmp	.L2705
.L2687:
	cmpb	$0, 81(%r14)
	jne	.L2854
	movb	$1, 81(%r14)
	movq	(%r14), %rdi
	leaq	.LC3(%rip), %rax
	xorl	%edx, %edx
	leaq	-272(%rbp), %rsi
	movl	%ecx, -400(%rbp)
	movq	%rax, -272(%rbp)
	movq	$28, -264(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	16(%r14), %rdx
	testq	%rax, %rax
	je	.L2526
	movq	%rax, (%rdx)
	movq	48(%r14), %rax
	movl	$2097152, 56(%r14)
	movl	-400(%rbp), %ecx
	movl	36(%rax), %edx
	movl	%edx, 64(%r14)
	jmp	.L2685
.L2919:
	movl	$16, %esi
	movq	%r9, -400(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-400(%rbp), %r9
	jmp	.L2643
.L2918:
	movl	$16, %esi
	movq	%rdi, -400(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-400(%rbp), %rdi
	movq	%rax, %r9
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	jmp	.L2641
.L2937:
	movq	8(%r14), %r10
	movl	12(%r13), %ecx
	movq	16(%r10), %r15
	movq	24(%r10), %rax
	subq	%r15, %rax
	cmpq	$31, %rax
	jbe	.L2939
	leaq	32(%r15), %rax
	movq	%rax, 16(%r10)
.L2647:
	movq	%r10, %rsi
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movq	%r9, %rdx
	call	_ZN2v88internal20RegExpCharacterClassC1EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13RegExpBuilder17AddCharacterClassEPNS0_20RegExpCharacterClassE
	jmp	.L2648
.L2930:
	movq	%rcx, -416(%rbp)
	movq	%r8, -408(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-408(%rbp), %r8
	movq	-416(%rbp), %rcx
	movq	%rax, %r9
	jmp	.L2630
.L2847:
	leaq	.LC7(%rip), %rsi
	movl	$14, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser11ReportErrorENS0_6VectorIKcEE
	movq	%rax, %r15
	jmp	.L2502
.L2893:
	call	__stack_chk_fail@PLT
.L2932:
	movzwl	(%rdi,%rdx,2), %edx
	jmp	.L2692
.L2708:
	movzwl	(%rcx,%rax,2), %eax
	jmp	.L2709
.L2931:
	movslq	%esi, %rax
	movzwl	(%rdi,%rax,2), %eax
	movl	%eax, %edx
	andw	$-1024, %dx
	cmpw	$-9216, %dx
	jne	.L2679
	sall	$10, %r15d
	andl	$1023, %eax
	andl	$1047552, %r15d
	orl	%eax, %r15d
	addl	$65536, %r15d
	jmp	.L2676
.L2935:
	movl	$16, %esi
	movq	%rcx, -416(%rbp)
	movq	%rdi, -400(%rbp)
	movq	%r15, -408(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-400(%rbp), %rdi
	movq	-408(%rbp), %r8
	movq	-416(%rbp), %rcx
	jmp	.L2627
.L2934:
	movl	$16, %esi
	movq	%rcx, -408(%rbp)
	movq	%rdi, -400(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-400(%rbp), %rdi
	movq	-408(%rbp), %rcx
	movq	%rax, %r15
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	jmp	.L2625
.L2936:
	movl	$32, %esi
	movl	%edx, -400(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-400(%rbp), %edx
	movq	%rax, %rsi
	jmp	.L2660
.L2695:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2938:
	movslq	%edx, %rdi
	movzwl	(%rcx,%rdi,2), %ecx
	movl	%ecx, %edi
	andw	$-1024, %di
	cmpw	$-9216, %di
	jne	.L2710
	sall	$10, %eax
	andl	$1023, %ecx
	leal	2(%rsi), %edx
	andl	$1047552, %eax
	orl	%ecx, %eax
	addl	$65536, %eax
	jmp	.L2710
.L2679:
	leaq	.LC8(%rip), %rsi
	movl	$22, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser11ReportErrorENS0_6VectorIKcEE
	movq	%rax, %r15
	jmp	.L2502
.L2939:
	movq	%r10, %rdi
	movl	$32, %esi
	movl	%ecx, -416(%rbp)
	movq	%r9, -408(%rbp)
	movq	%r10, -400(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-400(%rbp), %r10
	movq	-408(%rbp), %r9
	movl	-416(%rbp), %ecx
	movq	%rax, %r15
	jmp	.L2647
.L2672:
	leaq	.LC40(%rip), %rsi
	movl	$22, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser11ReportErrorENS0_6VectorIKcEE
	movq	%rax, %r15
	jmp	.L2502
.L2713:
	leaq	.LC41(%rip), %rsi
	movl	$22, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal12RegExpParser11ReportErrorENS0_6VectorIKcEE
	movq	%rax, %r15
	jmp	.L2502
.L2933:
	movslq	%esi, %r9
	movzwl	(%rdi,%r9,2), %edi
	movl	%edi, %r9d
	andw	$-1024, %r9w
	cmpw	$-9216, %r9w
	jne	.L2691
	sall	$10, %edx
	andl	$1023, %edi
	leal	2(%r8), %esi
	andl	$1047552, %edx
	orl	%edi, %edx
	addl	$65536, %edx
	jmp	.L2691
	.cfi_endproc
.LFE18370:
	.size	_ZN2v88internal12RegExpParser16ParseDisjunctionEv, .-_ZN2v88internal12RegExpParser16ParseDisjunctionEv
	.section	.text._ZN2v88internal12RegExpParser12ParsePatternEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser12ParsePatternEv
	.type	_ZN2v88internal12RegExpParser12ParsePatternEv, @function
_ZN2v88internal12RegExpParser12ParsePatternEv:
.LFB18369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN2v88internal12RegExpParser16ParseDisjunctionEv
	cmpb	$0, 81(%rbx)
	jne	.L2943
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal12RegExpParser24PatchNamedBackReferencesEv
	cmpb	$0, 81(%rbx)
	jne	.L2943
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*160(%rax)
	testb	%al, %al
	je	.L2940
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*152(%rax)
	movq	48(%rbx), %rdx
	movl	16(%rax), %eax
	cmpl	%eax, 36(%rdx)
	je	.L2948
.L2940:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2943:
	.cfi_restore_state
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2948:
	.cfi_restore_state
	movb	$1, 77(%rbx)
	jmp	.L2940
	.cfi_endproc
.LFE18369:
	.size	_ZN2v88internal12RegExpParser12ParsePatternEv, .-_ZN2v88internal12RegExpParser12ParsePatternEv
	.section	.text._ZN2v88internal12RegExpParser11ParseRegExpEPNS0_7IsolateEPNS0_4ZoneEPNS0_16FlatStringReaderENS_4base5FlagsINS0_8JSRegExp4FlagEiEEPNS0_17RegExpCompileDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12RegExpParser11ParseRegExpEPNS0_7IsolateEPNS0_4ZoneEPNS0_16FlatStringReaderENS_4base5FlagsINS0_8JSRegExp4FlagEiEEPNS0_17RegExpCompileDataE
	.type	_ZN2v88internal12RegExpParser11ParseRegExpEPNS0_7IsolateEPNS0_4ZoneEPNS0_16FlatStringReaderENS_4base5FlagsINS0_8JSRegExp4FlagEiEEPNS0_17RegExpCompileDataE, @function
_ZN2v88internal12RegExpParser11ParseRegExpEPNS0_7IsolateEPNS0_4ZoneEPNS0_16FlatStringReaderENS_4base5FlagsINS0_8JSRegExp4FlagEiEEPNS0_17RegExpCompileDataE:
.LFB18422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm1
	movq	%rdi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-128(%rbp), %r13
	pushq	%r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%r8, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	40(%r8), %rax
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -112(%rbp)
	movabsq	$4294967296, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -80(%rbp)
	movl	%ecx, -68(%rbp)
	movw	%ax, -48(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	$0, -104(%rbp)
	movl	$2097152, -72(%rbp)
	movq	$0, -64(%rbp)
	call	_ZN2v88internal12RegExpParser7AdvanceEv
	movq	%r13, %rdi
	call	_ZN2v88internal12RegExpParser16ParseDisjunctionEv
	cmpb	$0, -47(%rbp)
	jne	.L2950
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal12RegExpParser24PatchNamedBackReferencesEv
	cmpb	$0, -47(%rbp)
	jne	.L2950
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*160(%rax)
	testb	%al, %al
	jne	.L2962
.L2952:
	movzbl	-47(%rbp), %eax
	testb	%al, %al
	jne	.L2954
	movq	%r12, (%rbx)
	movq	(%r12), %rax
	movq	%r12, %rdi
	movl	-60(%rbp), %r14d
	call	*160(%rax)
	testb	%al, %al
	jne	.L2963
.L2955:
	movb	%al, 24(%rbx)
	movzbl	-50(%rbp), %eax
	movq	%r13, %rdi
	movb	%al, 25(%rbx)
	call	_ZN2v88internal12RegExpParser20CreateCaptureNameMapEv
	movl	%r14d, 48(%rbx)
	movq	%rax, 32(%rbx)
	movzbl	-47(%rbp), %eax
.L2954:
	xorl	$1, %eax
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2964
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2962:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*152(%rax)
	movq	-80(%rbp), %rdx
	movl	16(%rax), %eax
	cmpl	%eax, 36(%rdx)
	jne	.L2952
	movb	$1, -51(%rbp)
	jmp	.L2952
	.p2align 4,,10
	.p2align 3
.L2963:
	testl	%r14d, %r14d
	sete	%al
	andb	-51(%rbp), %al
	jmp	.L2955
	.p2align 4,,10
	.p2align 3
.L2950:
	movl	$1, %eax
	jmp	.L2954
.L2964:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18422:
	.size	_ZN2v88internal12RegExpParser11ParseRegExpEPNS0_7IsolateEPNS0_4ZoneEPNS0_16FlatStringReaderENS_4base5FlagsINS0_8JSRegExp4FlagEiEEPNS0_17RegExpCompileDataE, .-_ZN2v88internal12RegExpParser11ParseRegExpEPNS0_7IsolateEPNS0_4ZoneEPNS0_16FlatStringReaderENS_4base5FlagsINS0_8JSRegExp4FlagEiEEPNS0_17RegExpCompileDataE
	.section	.rodata._ZN2v88internalL15kAsciiCharFlagsE,"a"
	.align 32
	.type	_ZN2v88internalL15kAsciiCharFlagsE, @object
	.size	_ZN2v88internalL15kAsciiCharFlagsE, 128
_ZN2v88internalL15kAsciiCharFlagsE:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\f\b\f\f\b"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\f"
	.string	""
	.string	""
	.string	"\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\002\002\002\002\002\002\002\002\002\002"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"
	.string	"\003"
	.string	""
	.string	"\003"
	.string	"\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
