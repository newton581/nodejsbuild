	.file	"handler-outside.cc"
	.text
	.section	.text._ZN2v88internal12trap_handler17CreateHandlerDataEmmmPKNS1_24ProtectedInstructionDataE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal12trap_handler17CreateHandlerDataEmmmPKNS1_24ProtectedInstructionDataE
	.type	_ZN2v88internal12trap_handler17CreateHandlerDataEmmmPKNS1_24ProtectedInstructionDataE, @function
_ZN2v88internal12trap_handler17CreateHandlerDataEmmmPKNS1_24ProtectedInstructionDataE:
.LFB3450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	24(,%rdx,8), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$16, %rsp
	movq	%rdi, -40(%rbp)
	movq	%r13, %rdi
	movq	%rsi, -48(%rbp)
	call	malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1
	movq	%rbx, 16(%rax)
	leaq	24(%rax), %rdi
	leaq	-24(%r13), %rdx
	movq	%r14, %rsi
	movq	-40(%rbp), %xmm0
	movhps	-48(%rbp), %xmm0
	movups	%xmm0, (%rax)
	call	memcpy@PLT
.L1:
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3450:
	.size	_ZN2v88internal12trap_handler17CreateHandlerDataEmmmPKNS1_24ProtectedInstructionDataE, .-_ZN2v88internal12trap_handler17CreateHandlerDataEmmmPKNS1_24ProtectedInstructionDataE
	.section	.text.unlikely._ZN2v88internal12trap_handler19RegisterHandlerDataEmmmPKNS1_24ProtectedInstructionDataE,"ax",@progbits
.LCOLDB0:
	.section	.text._ZN2v88internal12trap_handler19RegisterHandlerDataEmmmPKNS1_24ProtectedInstructionDataE,"ax",@progbits
.LHOTB0:
	.p2align 4
	.globl	_ZN2v88internal12trap_handler19RegisterHandlerDataEmmmPKNS1_24ProtectedInstructionDataE
	.type	_ZN2v88internal12trap_handler19RegisterHandlerDataEmmmPKNS1_24ProtectedInstructionDataE, @function
_ZN2v88internal12trap_handler19RegisterHandlerDataEmmmPKNS1_24ProtectedInstructionDataE:
.LFB3451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	leaq	24(,%rdx,8), %rbx
	subq	$32, %rsp
	movq	%rdi, -56(%rbp)
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	malloc@PLT
	testq	%rax, %rax
	je	.L15
	movq	-56(%rbp), %xmm0
	movq	%r13, 16(%rax)
	movq	%r14, %rsi
	movq	%rax, %r12
	leaq	-24(%rbx), %rdx
	leaq	24(%rax), %rdi
	movhps	-64(%rbp), %xmm0
	leaq	-41(%rbp), %r14
	movups	%xmm0, (%rax)
	call	memcpy@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal12trap_handler12MetadataLockC1Ev@PLT
	movq	_ZN12_GLOBAL__N_115gNextCodeObjectE(%rip), %rbx
	cmpq	%rbx, _ZN2v88internal12trap_handler15gNumCodeObjectsE(%rip)
	movq	_ZN2v88internal12trap_handler12gCodeObjectsE(%rip), %rdx
	je	.L26
.L11:
	movq	%rbx, %rax
	salq	$4, %rax
	addq	%rax, %rdx
	movq	8(%rdx), %rax
	movq	%rax, _ZN12_GLOBAL__N_115gNextCodeObjectE(%rip)
	cmpq	$2147483647, %rbx
	ja	.L19
	movq	%r12, (%rdx)
	movl	%ebx, %r12d
.L14:
	movq	%r14, %rdi
	call	_ZN2v88internal12trap_handler12MetadataLockD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L27
	addq	$32, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	testq	%rbx, %rbx
	jne	.L28
	movl	$16384, %esi
	movl	$1024, %r13d
.L12:
	movq	_ZN2v88internal12trap_handler12gCodeObjectsE(%rip), %rdi
	call	realloc@PLT
	movq	%rax, _ZN2v88internal12trap_handler12gCodeObjectsE(%rip)
	testq	%rax, %rax
	je	.L15
	movq	_ZN2v88internal12trap_handler15gNumCodeObjectsE(%rip), %rdi
	movq	%r13, %rdx
	xorl	%esi, %esi
	subq	%rdi, %rdx
	salq	$4, %rdi
	salq	$4, %rdx
	addq	%rax, %rdi
	call	memset@PLT
	movq	_ZN2v88internal12trap_handler15gNumCodeObjectsE(%rip), %rax
	movq	_ZN2v88internal12trap_handler12gCodeObjectsE(%rip), %rdx
	cmpq	%rax, %r13
	jbe	.L17
	.p2align 4,,10
	.p2align 3
.L18:
	addq	$1, %rax
	movq	%rax, %rcx
	salq	$4, %rcx
	movq	%rax, -8(%rdx,%rcx)
	cmpq	%r13, %rax
	jne	.L18
.L17:
	movq	%r13, _ZN2v88internal12trap_handler15gNumCodeObjectsE(%rip)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L28:
	leaq	(%rbx,%rbx), %r13
	movl	$2147483647, %eax
	cmpq	$2147483647, %r13
	cmova	%rax, %r13
	cmpq	%r13, %rbx
	je	.L19
	movq	%r13, %rsi
	salq	$4, %rsi
	jmp	.L12
.L19:
	movq	%r12, %rdi
	movl	$-1, %r12d
	call	free@PLT
	jmp	.L14
.L27:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal12trap_handler19RegisterHandlerDataEmmmPKNS1_24ProtectedInstructionDataE
	.cfi_startproc
	.type	_ZN2v88internal12trap_handler19RegisterHandlerDataEmmmPKNS1_24ProtectedInstructionDataE.cold, @function
_ZN2v88internal12trap_handler19RegisterHandlerDataEmmmPKNS1_24ProtectedInstructionDataE.cold:
.LFSB3451:
.L15:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	call	abort@PLT
	.cfi_endproc
.LFE3451:
	.section	.text._ZN2v88internal12trap_handler19RegisterHandlerDataEmmmPKNS1_24ProtectedInstructionDataE
	.size	_ZN2v88internal12trap_handler19RegisterHandlerDataEmmmPKNS1_24ProtectedInstructionDataE, .-_ZN2v88internal12trap_handler19RegisterHandlerDataEmmmPKNS1_24ProtectedInstructionDataE
	.section	.text.unlikely._ZN2v88internal12trap_handler19RegisterHandlerDataEmmmPKNS1_24ProtectedInstructionDataE
	.size	_ZN2v88internal12trap_handler19RegisterHandlerDataEmmmPKNS1_24ProtectedInstructionDataE.cold, .-_ZN2v88internal12trap_handler19RegisterHandlerDataEmmmPKNS1_24ProtectedInstructionDataE.cold
.LCOLDE0:
	.section	.text._ZN2v88internal12trap_handler19RegisterHandlerDataEmmmPKNS1_24ProtectedInstructionDataE
.LHOTE0:
	.section	.text._ZN2v88internal12trap_handler18ReleaseHandlerDataEi,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal12trap_handler18ReleaseHandlerDataEi
	.type	_ZN2v88internal12trap_handler18ReleaseHandlerDataEi, @function
_ZN2v88internal12trap_handler18ReleaseHandlerDataEi:
.LFB3452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movslq	%edi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %ebx
	je	.L29
	leaq	-41(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal12trap_handler12MetadataLockC1Ev@PLT
	movq	_ZN2v88internal12trap_handler12gCodeObjectsE(%rip), %rdx
	movq	%rbx, %rax
	movq	%r12, %rdi
	salq	$4, %rax
	movq	_ZN12_GLOBAL__N_115gNextCodeObjectE(%rip), %rcx
	movq	%rbx, _ZN12_GLOBAL__N_115gNextCodeObjectE(%rip)
	addq	%rax, %rdx
	movq	(%rdx), %r13
	movq	$0, (%rdx)
	movq	_ZN2v88internal12trap_handler12gCodeObjectsE(%rip), %rdx
	movq	%rcx, 8(%rdx,%rax)
	call	_ZN2v88internal12trap_handler12MetadataLockD1Ev@PLT
	movq	%r13, %rdi
	call	free@PLT
.L29:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L36
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L36:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3452:
	.size	_ZN2v88internal12trap_handler18ReleaseHandlerDataEi, .-_ZN2v88internal12trap_handler18ReleaseHandlerDataEi
	.section	.text._ZN2v88internal12trap_handler33GetThreadInWasmThreadLocalAddressEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal12trap_handler33GetThreadInWasmThreadLocalAddressEv
	.type	_ZN2v88internal12trap_handler33GetThreadInWasmThreadLocalAddressEv, @function
_ZN2v88internal12trap_handler33GetThreadInWasmThreadLocalAddressEv:
.LFB3453:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	addq	%fs:0, %rax
	ret
	.cfi_endproc
.LFE3453:
	.size	_ZN2v88internal12trap_handler33GetThreadInWasmThreadLocalAddressEv, .-_ZN2v88internal12trap_handler33GetThreadInWasmThreadLocalAddressEv
	.section	.text._ZN2v88internal12trap_handler21GetRecoveredTrapCountEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal12trap_handler21GetRecoveredTrapCountEv
	.type	_ZN2v88internal12trap_handler21GetRecoveredTrapCountEv, @function
_ZN2v88internal12trap_handler21GetRecoveredTrapCountEv:
.LFB3454:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal12trap_handler19gRecoveredTrapCountE(%rip), %rax
	ret
	.cfi_endproc
.LFE3454:
	.size	_ZN2v88internal12trap_handler21GetRecoveredTrapCountEv, .-_ZN2v88internal12trap_handler21GetRecoveredTrapCountEv
	.section	.text._ZN2v88internal12trap_handler17EnableTrapHandlerEb,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal12trap_handler17EnableTrapHandlerEb
	.type	_ZN2v88internal12trap_handler17EnableTrapHandlerEb, @function
_ZN2v88internal12trap_handler17EnableTrapHandlerEb:
.LFB3455:
	.cfi_startproc
	endbr64
	testb	%dil, %dil
	jne	.L46
	movb	$1, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal12trap_handler26RegisterDefaultTrapHandlerEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movb	%al, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	ret
	.cfi_endproc
.LFE3455:
	.size	_ZN2v88internal12trap_handler17EnableTrapHandlerEb, .-_ZN2v88internal12trap_handler17EnableTrapHandlerEb
	.globl	_ZN2v88internal12trap_handler25g_is_trap_handler_enabledE
	.section	.bss._ZN2v88internal12trap_handler25g_is_trap_handler_enabledE,"aw",@nobits
	.type	_ZN2v88internal12trap_handler25g_is_trap_handler_enabledE, @object
	.size	_ZN2v88internal12trap_handler25g_is_trap_handler_enabledE, 1
_ZN2v88internal12trap_handler25g_is_trap_handler_enabledE:
	.zero	1
	.section	.bss._ZN12_GLOBAL__N_115gNextCodeObjectE,"aw",@nobits
	.align 8
	.type	_ZN12_GLOBAL__N_115gNextCodeObjectE, @object
	.size	_ZN12_GLOBAL__N_115gNextCodeObjectE, 8
_ZN12_GLOBAL__N_115gNextCodeObjectE:
	.zero	8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
