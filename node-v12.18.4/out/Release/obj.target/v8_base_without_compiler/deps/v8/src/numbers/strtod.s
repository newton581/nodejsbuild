	.file	"strtod.cc"
	.text
	.section	.rodata._ZN2v88internal6StrtodENS0_6VectorIKcEEi.str1.1,"aMS",@progbits,1
.LC2:
	.string	"unreachable code"
	.section	.text._ZN2v88internal6StrtodENS0_6VectorIKcEEi,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal6StrtodENS0_6VectorIKcEEi
	.type	_ZN2v88internal6StrtodENS0_6VectorIKcEEi, @function
_ZN2v88internal6StrtodENS0_6VectorIKcEEi:
.LFB5059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$2008, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jle	.L235
	movl	%esi, %ecx
	movq	%rdi, %r8
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L3:
	addl	$1, %eax
	addq	$1, %r8
	cmpl	%ecx, %eax
	je	.L235
.L5:
	cmpb	$48, (%r8)
	je	.L3
	subl	%eax, %esi
	movl	%esi, %eax
	subl	$1, %eax
	js	.L235
	cltq
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L6:
	subq	$1, %rax
	testl	%eax, %eax
	js	.L235
.L9:
	cmpb	$48, (%r8,%rax)
	movl	%eax, %r10d
	je	.L6
	leal	1(%rax), %r13d
	subl	%r13d, %esi
	leal	(%rsi,%rdx), %r9d
	cmpl	$780, %r13d
	jle	.L239
	leaq	-848(%rbp), %r11
	movq	%r8, %rsi
	movl	$97, %ecx
	movq	%r11, %rdi
	leal	-779(%r9,%r10), %edx
	rep movsq
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movzbl	2(%rsi), %eax
	movl	$780, %esi
	movb	%al, 2(%rdi)
	movq	%r11, %rdi
	movb	$49, -69(%rbp)
	call	_ZN2v88internal6StrtodENS0_6VectorIKcEEi
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L235:
	pxor	%xmm0, %xmm0
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L240
	addq	$2008, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L239:
	.cfi_restore_state
	leal	(%r9,%r13), %eax
	cmpl	$309, %eax
	jg	.L54
	pxor	%xmm0, %xmm0
	cmpl	$-323, %eax
	jl	.L1
	cmpl	$15, %r13d
	jg	.L20
	cmpl	$-22, %r9d
	jnb	.L241
	cmpl	$22, %r9d
	jbe	.L242
	testl	%r9d, %r9d
	js	.L20
	movl	$15, %ecx
	movl	%r9d, %esi
	subl	%r13d, %ecx
	subl	%ecx, %esi
	cmpl	$22, %esi
	jg	.L20
	movsbl	(%r8), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	testl	%r10d, %r10d
	jle	.L21
	movabsq	$1844674407370955160, %rax
	cmpq	%rax, %rdx
	ja	.L21
	movsbl	1(%r8), %edi
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %edi
	movslq	%edi, %rdi
	leaq	(%rdi,%rdx,2), %rdx
	cmpl	$1, %r10d
	je	.L21
	cmpq	%rax, %rdx
	ja	.L21
	leaq	(%rdx,%rdx,4), %rdi
	movsbl	2(%r8), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rdi,2), %rdx
	cmpl	$2, %r10d
	je	.L21
	cmpq	%rax, %rdx
	ja	.L21
	leaq	(%rdx,%rdx,4), %rdi
	movsbl	3(%r8), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rdi,2), %rdx
	cmpl	$3, %r10d
	je	.L21
	cmpq	%rax, %rdx
	ja	.L21
	leaq	(%rdx,%rdx,4), %rdi
	movsbl	4(%r8), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rdi,2), %rdx
	cmpl	$4, %r10d
	je	.L21
	cmpq	%rax, %rdx
	ja	.L21
	movsbl	5(%r8), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$5, %r10d
	je	.L21
	movabsq	$1844674407370955160, %rax
	cmpq	%rax, %rdx
	ja	.L21
	leaq	(%rdx,%rdx,4), %rdi
	movsbl	6(%r8), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rdi,2), %rdx
	cmpl	$6, %r10d
	je	.L21
	cmpq	%rax, %rdx
	ja	.L21
	leaq	(%rdx,%rdx,4), %rdi
	movsbl	7(%r8), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rdi,2), %rdx
	cmpl	$7, %r10d
	je	.L21
	cmpq	%rax, %rdx
	ja	.L21
	leaq	(%rdx,%rdx,4), %rdi
	movsbl	8(%r8), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rdi,2), %rdx
	cmpl	$8, %r10d
	je	.L21
	cmpq	%rax, %rdx
	ja	.L21
	leaq	(%rdx,%rdx,4), %rdi
	movsbl	9(%r8), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rdi,2), %rdx
	cmpl	$9, %r10d
	je	.L21
	cmpq	%rax, %rdx
	ja	.L21
	movsbl	10(%r8), %eax
	imulq	$10, %rdx, %rdx
	subl	$48, %eax
	cltq
	addq	%rax, %rdx
	cmpl	$10, %r10d
	je	.L21
	movabsq	$1844674407370955160, %rdi
	cmpq	%rdi, %rdx
	ja	.L21
	movsbl	11(%r8), %eax
	imulq	$10, %rdx, %rdx
	subl	$48, %eax
	cltq
	addq	%rax, %rdx
	cmpl	$11, %r10d
	je	.L21
	cmpq	%rdi, %rdx
	ja	.L21
	movsbl	12(%r8), %eax
	imulq	$10, %rdx, %rdx
	subl	$48, %eax
	cltq
	addq	%rax, %rdx
	cmpl	$12, %r10d
	je	.L21
	cmpq	%rdi, %rdx
	ja	.L21
	movsbl	13(%r8), %eax
	imulq	$10, %rdx, %rdx
	subl	$48, %eax
	cltq
	addq	%rax, %rdx
	cmpl	$13, %r10d
	je	.L21
	cmpq	%rdi, %rdx
	ja	.L21
	movsbl	14(%r8), %eax
	imulq	$10, %rdx, %rdx
	subl	$48, %eax
	cltq
	addq	%rax, %rdx
.L21:
	testq	%rdx, %rdx
	js	.L22
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L23:
	leaq	_ZN2v88internalL19exact_powers_of_tenE(%rip), %rax
	movslq	%ecx, %rcx
	movslq	%esi, %rsi
	mulsd	(%rax,%rcx,8), %xmm0
	mulsd	(%rax,%rsi,8), %xmm0
	jmp	.L1
.L243:
	movabsq	$9007199254740992, %r10
	addq	$1, %rdx
	cmpq	%r10, %rdx
	jne	.L53
	movabsq	$4503599627370496, %rdx
	addl	$1, %eax
.L53:
	subq	%rsi, %r11
	cmpq	%rdi, %r11
	setb	%sil
	cmpq	%rcx, %rdi
	setb	%cl
	andl	%ecx, %esi
	cmpl	$971, %eax
	jle	.L76
.L54:
	movsd	.LC1(%rip), %xmm0
	jmp	.L1
.L20:
	movl	$0, -1976(%rbp)
	xorl	%edx, %edx
	xorl	%eax, %eax
	movabsq	$1844674407370955160, %r11
	jmp	.L11
.L25:
	addq	$1, %rdx
	cmpq	%r11, %rax
	ja	.L24
.L11:
	leaq	(%rax,%rax,4), %rcx
	movsbl	(%r8,%rdx), %eax
	movl	%edx, %edi
	leal	1(%rdx), %esi
	subl	$48, %eax
	cltq
	leaq	(%rax,%rcx,2), %rax
	cmpl	%edx, %r10d
	jg	.L25
.L24:
	movl	%r9d, %ebx
	xorl	%r12d, %r12d
	cmpl	%r10d, %edi
	je	.L27
	movslq	%esi, %rsi
	xorl	%edx, %edx
	movl	$4, %r12d
	cmpb	$52, (%r8,%rsi)
	setg	%dl
	subl	%edi, %r10d
	addq	%rdx, %rax
	leal	(%r10,%r9), %ebx
.L27:
	movabsq	$-18014398509481984, %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	testq	%rsi, %rax
	jne	.L32
.L29:
	salq	$10, %rax
	subl	$10, %edx
	testq	%rcx, %rax
	je	.L29
.L32:
	testq	%rax, %rax
	js	.L30
.L31:
	subl	$1, %edx
	addq	%rax, %rax
	jns	.L31
.L30:
	cmpl	_ZN2v88internal16PowersOfTenCache19kMinDecimalExponentE(%rip), %ebx
	movq	%r8, -2016(%rbp)
	pxor	%xmm0, %xmm0
	movl	%r9d, -2008(%rbp)
	movq	%rax, -1984(%rbp)
	movl	%edx, -1976(%rbp)
	jl	.L1
	negl	%edx
	movl	%ebx, %edi
	leaq	-1968(%rbp), %r14
	movq	$0, -1968(%rbp)
	movl	%edx, %ecx
	movq	%r14, %rsi
	leaq	-1988(%rbp), %rdx
	movl	$0, -1960(%rbp)
	salq	%cl, %r12
	leaq	-1984(%rbp), %r15
	call	_ZN2v88internal16PowersOfTenCache32GetCachedPowerForDecimalExponentEiPNS0_5DiyFpEPi@PLT
	movl	-1988(%rbp), %eax
	movl	-2008(%rbp), %r9d
	movq	-2016(%rbp), %r8
	cmpl	%eax, %ebx
	je	.L34
	subl	%eax, %ebx
	cmpl	$7, %ebx
	ja	.L35
	leaq	.L37(%rip), %rdx
	movl	%ebx, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6StrtodENS0_6VectorIKcEEi,"a",@progbits
	.align 4
	.align 4
.L37:
	.long	.L35-.L37
	.long	.L43-.L37
	.long	.L84-.L37
	.long	.L41-.L37
	.long	.L40-.L37
	.long	.L39-.L37
	.long	.L38-.L37
	.long	.L36-.L37
	.section	.text._ZN2v88internal6StrtodENS0_6VectorIKcEEi
.L36:
	movabsq	$-7451627795949551616, %rdx
	movl	$-40, %eax
.L42:
	leaq	-1984(%rbp), %r15
	leaq	-1952(%rbp), %rsi
	movq	%r8, -2016(%rbp)
	movq	%r15, %rdi
	movl	%r9d, -2008(%rbp)
	movq	%rdx, -1952(%rbp)
	movl	%eax, -1944(%rbp)
	call	_ZN2v88internal5DiyFp8MultiplyERKS1_@PLT
	movl	$19, %eax
	leaq	4(%r12), %rdx
	movq	-2016(%rbp), %r8
	subl	%r13d, %eax
	movl	-2008(%rbp), %r9d
	cmpl	%eax, %ebx
	cmovg	%rdx, %r12
.L34:
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%r8, -2016(%rbp)
	movl	%r9d, -2008(%rbp)
	call	_ZN2v88internal5DiyFp8MultiplyERKS1_@PLT
	xorl	%esi, %esi
	testq	%r12, %r12
	movq	-1984(%rbp), %rdx
	setne	%sil
	movl	-1976(%rbp), %ecx
	movabsq	$-18014398509481984, %r10
	movl	-2008(%rbp), %r9d
	addl	$8, %esi
	movq	-2016(%rbp), %r8
	movq	%r10, %rdi
	movslq	%esi, %rsi
	movl	%ecx, %eax
	addq	%r12, %rsi
	testq	%r10, %rdx
	jne	.L48
.L45:
	salq	$10, %rdx
	subl	$10, %eax
	testq	%rdi, %rdx
	je	.L45
.L48:
	testq	%rdx, %rdx
	js	.L46
.L47:
	subl	$1, %eax
	addq	%rdx, %rdx
	jns	.L47
.L46:
	subl	%eax, %ecx
	movl	%eax, -1976(%rbp)
	salq	%cl, %rsi
	leal	64(%rax), %ecx
	cmpl	$-1021, %ecx
	jge	.L86
	cmpl	$-1073, %ecx
	jl	.L87
	leal	1138(%rax), %r11d
	movl	$64, %r10d
	subl	%r11d, %r10d
	cmpl	$60, %r10d
	jg	.L51
	movl	%r10d, %ecx
	movq	$-1, %rdi
	salq	%cl, %rdi
	movl	$63, %ecx
	subl	%r11d, %ecx
	movl	$8, %r11d
	notq	%rdi
	salq	%cl, %r11
.L49:
	andq	%rdx, %rdi
	movl	%r10d, %ecx
	addl	%r10d, %eax
	shrq	%cl, %rdx
	salq	$3, %rdi
	leaq	(%rsi,%r11), %rcx
	cmpq	%rcx, %rdi
	jnb	.L243
	subq	%rsi, %r11
	cmpq	%r11, %rdi
	seta	%sil
	cmpl	$971, %eax
	jg	.L54
.L76:
	movslq	%r13d, %r10
	cmpl	$-1074, %eax
	jl	.L55
	movabsq	$4503599627370496, %rdi
	movq	%rdx, %rcx
	andq	%rdi, %rcx
	cmpl	$-1074, %eax
	je	.L56
	testq	%rcx, %rcx
	je	.L58
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L230:
	testq	%rcx, %rcx
	jne	.L244
.L58:
	addq	%rdx, %rdx
	movl	%eax, %r11d
	subl	$1, %eax
	movq	%rdx, %rcx
	andq	%rdi, %rcx
	cmpl	$-1074, %eax
	jne	.L230
.L56:
	movabsq	$4503599627370496, %rax
	testq	%rcx, %rcx
	cmovne	%rax, %rcx
.L72:
	movabsq	$4503599627370495, %rbx
	andq	%rbx, %rdx
	movq	%rdx, %rbx
	orq	%rcx, %rbx
	movq	%rbx, %xmm0
	testb	%sil, %sil
	je	.L1
	movsd	.LC1(%rip), %xmm1
	ucomisd	%xmm1, %xmm0
	jnp	.L245
.L62:
	movabsq	$4503599627370495, %r12
	movabsq	$9218868437227405312, %rax
	andq	%rbx, %r12
	testq	%rax, %rbx
	je	.L88
	movq	%rbx, %r13
	shrq	$52, %r13
	leal	-1075(%r13), %eax
	subl	$1076, %r13d
	movl	%eax, -2008(%rbp)
	movabsq	$4503599627370496, %rax
	addq	%rax, %r12
.L65:
	leaq	-1936(%rbp), %r14
	leaq	-1392(%rbp), %r15
	movl	%r9d, -2016(%rbp)
	movq	%r14, %rdi
	movsd	%xmm0, -2040(%rbp)
	movq	%r8, -2032(%rbp)
	movq	%r10, -2024(%rbp)
	call	_ZN2v88internal6BignumC1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal6BignumC1Ev@PLT
	movq	-2032(%rbp), %r8
	movq	-2024(%rbp), %r10
	movq	%r14, %rdi
	movq	%r8, %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal6Bignum19AssignDecimalStringENS0_6VectorIKcEE@PLT
	leaq	1(%r12,%r12), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Bignum12AssignUInt64Em@PLT
	movl	-2016(%rbp), %r9d
	movsd	-2040(%rbp), %xmm0
	testl	%r9d, %r9d
	movsd	%xmm0, -2016(%rbp)
	movl	%r9d, %esi
	js	.L66
	movq	%r14, %rdi
	call	_ZN2v88internal6Bignum20MultiplyByPowerOfTenEi@PLT
	movsd	-2016(%rbp), %xmm0
.L67:
	testl	%r13d, %r13d
	jle	.L68
	movl	%r13d, %esi
	movq	%r15, %rdi
	movsd	%xmm0, -2008(%rbp)
	call	_ZN2v88internal6Bignum9ShiftLeftEi@PLT
	movsd	-2008(%rbp), %xmm0
.L69:
	movq	%r15, %rsi
	movq	%r14, %rdi
	movsd	%xmm0, -2008(%rbp)
	call	_ZN2v88internal6Bignum7CompareERKS1_S3_@PLT
	movsd	-2008(%rbp), %xmm0
	testl	%eax, %eax
	js	.L1
	jne	.L237
	andl	$1, %r12d
	je	.L1
.L237:
	movabsq	$9218868437227405312, %rax
	movsd	.LC1(%rip), %xmm1
	cmpq	%rax, %rbx
	je	.L71
	leaq	1(%rbx), %rax
	movq	%rax, %xmm0
	jmp	.L1
.L241:
	movsbl	(%r8), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	testl	%r10d, %r10d
	jle	.L13
	movabsq	$1844674407370955160, %rcx
	cmpq	%rcx, %rdx
	ja	.L13
	movsbl	1(%r8), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$1, %r10d
	je	.L13
	cmpq	%rcx, %rdx
	ja	.L13
	movsbl	2(%r8), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$2, %r10d
	je	.L13
	movq	%rcx, %rax
	cmpq	%rcx, %rdx
	ja	.L13
	leaq	(%rdx,%rdx,4), %rcx
	movsbl	3(%r8), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rcx,2), %rdx
	cmpl	$3, %r10d
	je	.L13
	cmpq	%rax, %rdx
	ja	.L13
	leaq	(%rdx,%rdx,4), %rcx
	movsbl	4(%r8), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rcx,2), %rdx
	cmpl	$4, %r10d
	je	.L13
	cmpq	%rax, %rdx
	ja	.L13
	leaq	(%rdx,%rdx,4), %rcx
	movsbl	5(%r8), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rcx,2), %rdx
	cmpl	$5, %r10d
	je	.L13
	cmpq	%rax, %rdx
	ja	.L13
	leaq	(%rdx,%rdx,4), %rcx
	movsbl	6(%r8), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rcx,2), %rdx
	cmpl	$6, %r10d
	je	.L13
	cmpq	%rax, %rdx
	ja	.L13
	movsbl	7(%r8), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$7, %r10d
	je	.L13
	movabsq	$1844674407370955160, %rax
	cmpq	%rax, %rdx
	ja	.L13
	leaq	(%rdx,%rdx,4), %rcx
	movsbl	8(%r8), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rcx,2), %rdx
	cmpl	$8, %r10d
	je	.L13
	cmpq	%rax, %rdx
	ja	.L13
	leaq	(%rdx,%rdx,4), %rcx
	movsbl	9(%r8), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rcx,2), %rdx
	cmpl	$9, %r10d
	je	.L13
	cmpq	%rax, %rdx
	ja	.L13
	leaq	(%rdx,%rdx,4), %rcx
	movsbl	10(%r8), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rcx,2), %rdx
	cmpl	$10, %r10d
	je	.L13
	cmpq	%rax, %rdx
	ja	.L13
	leaq	(%rdx,%rdx,4), %rcx
	movsbl	11(%r8), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rcx,2), %rdx
	cmpl	$11, %r10d
	je	.L13
	cmpq	%rax, %rdx
	ja	.L13
	movsbl	12(%r8), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$12, %r10d
	je	.L13
	movabsq	$1844674407370955160, %rcx
	cmpq	%rcx, %rdx
	ja	.L13
	movsbl	13(%r8), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$13, %r10d
	je	.L13
	cmpq	%rcx, %rdx
	ja	.L13
	movsbl	14(%r8), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
.L13:
	testq	%rdx, %rdx
	js	.L14
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L15:
	movl	%r9d, %eax
	leaq	_ZN2v88internalL19exact_powers_of_tenE(%rip), %rdx
	negl	%eax
	cltq
	divsd	(%rdx,%rax,8), %xmm0
	jmp	.L1
.L242:
	movsbl	(%r8), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	testl	%r10d, %r10d
	jle	.L17
	movabsq	$1844674407370955160, %rax
	cmpq	%rax, %rdx
	ja	.L17
	movsbl	1(%r8), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$1, %r10d
	je	.L17
	movabsq	$1844674407370955160, %rax
	cmpq	%rax, %rdx
	ja	.L17
	leaq	(%rdx,%rdx,4), %rcx
	movsbl	2(%r8), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rcx,2), %rdx
	cmpl	$2, %r10d
	je	.L17
	cmpq	%rax, %rdx
	ja	.L17
	leaq	(%rdx,%rdx,4), %rcx
	movsbl	3(%r8), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rcx,2), %rdx
	cmpl	$3, %r10d
	je	.L17
	cmpq	%rax, %rdx
	ja	.L17
	leaq	(%rdx,%rdx,4), %rcx
	movsbl	4(%r8), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rcx,2), %rdx
	cmpl	$4, %r10d
	je	.L17
	cmpq	%rax, %rdx
	ja	.L17
	leaq	(%rdx,%rdx,4), %rcx
	movsbl	5(%r8), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rcx,2), %rdx
	cmpl	$5, %r10d
	je	.L17
	cmpq	%rax, %rdx
	ja	.L17
	movsbl	6(%r8), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$6, %r10d
	je	.L17
	movabsq	$1844674407370955160, %rax
	cmpq	%rax, %rdx
	ja	.L17
	leaq	(%rdx,%rdx,4), %rcx
	movsbl	7(%r8), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rcx,2), %rdx
	cmpl	$7, %r10d
	je	.L17
	cmpq	%rax, %rdx
	ja	.L17
	leaq	(%rdx,%rdx,4), %rcx
	movsbl	8(%r8), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rcx,2), %rdx
	cmpl	$8, %r10d
	je	.L17
	cmpq	%rax, %rdx
	ja	.L17
	leaq	(%rdx,%rdx,4), %rcx
	movsbl	9(%r8), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rcx,2), %rdx
	cmpl	$9, %r10d
	je	.L17
	cmpq	%rax, %rdx
	ja	.L17
	leaq	(%rdx,%rdx,4), %rcx
	movsbl	10(%r8), %edx
	subl	$48, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rcx,2), %rdx
	cmpl	$10, %r10d
	je	.L17
	cmpq	%rax, %rdx
	ja	.L17
	movsbl	11(%r8), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$11, %r10d
	je	.L17
	movabsq	$1844674407370955160, %rcx
	cmpq	%rcx, %rdx
	ja	.L17
	movsbl	12(%r8), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$12, %r10d
	je	.L17
	cmpq	%rcx, %rdx
	ja	.L17
	movsbl	13(%r8), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
	cmpl	$13, %r10d
	je	.L17
	cmpq	%rcx, %rdx
	ja	.L17
	movsbl	14(%r8), %eax
	leaq	(%rdx,%rdx,4), %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rdx
.L17:
	testq	%rdx, %rdx
	js	.L18
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L19:
	movslq	%r9d, %r9
	leaq	_ZN2v88internalL19exact_powers_of_tenE(%rip), %rax
	mulsd	(%rax,%r9,8), %xmm0
	jmp	.L1
.L66:
	negl	%esi
	movq	%r15, %rdi
	call	_ZN2v88internal6Bignum20MultiplyByPowerOfTenEi@PLT
	movsd	-2016(%rbp), %xmm0
	jmp	.L67
.L245:
	jne	.L62
.L71:
	movapd	%xmm1, %xmm0
	jmp	.L1
.L55:
	testb	%sil, %sil
	je	.L235
	xorl	%ebx, %ebx
	pxor	%xmm0, %xmm0
	movl	$-1075, %r13d
	xorl	%r12d, %r12d
	movl	$-1074, -2008(%rbp)
	jmp	.L65
.L14:
	movq	%rdx, %rax
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L15
.L68:
	movl	$1, %esi
	movq	%r14, %rdi
	subl	-2008(%rbp), %esi
	movsd	%xmm0, -2016(%rbp)
	call	_ZN2v88internal6Bignum9ShiftLeftEi@PLT
	movsd	-2016(%rbp), %xmm0
	jmp	.L69
.L87:
	movl	$4, %ecx
.L50:
	sarq	%cl, %rsi
	shrq	%cl, %rdx
	addl	%ecx, %eax
	movl	$60, %r10d
	movabsq	$4611686018427387904, %r11
	addq	$9, %rsi
	movabsq	$1152921504606846975, %rdi
	jmp	.L49
.L51:
	movl	$4, %ecx
	subl	%r11d, %ecx
	jmp	.L50
.L86:
	movl	$8192, %r11d
	movl	$2047, %edi
	movl	$11, %r10d
	jmp	.L49
.L244:
	leal	1074(%r11), %ecx
	salq	$52, %rcx
	jmp	.L72
.L18:
	movq	%rdx, %rax
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L19
.L57:
	addl	$1075, %eax
	movq	%rax, %rcx
	salq	$52, %rcx
	jmp	.L72
.L88:
	movl	$-1074, -2008(%rbp)
	movl	$-1075, %r13d
	jmp	.L65
.L240:
	call	__stack_chk_fail@PLT
.L22:
	movq	%rdx, %rax
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L23
.L38:
	movabsq	$-854558029293551616, %rdx
	movl	$-44, %eax
	jmp	.L42
.L39:
	movabsq	$-4372995238176751616, %rdx
	movl	$-47, %eax
	jmp	.L42
.L40:
	movabsq	$-7187745005283311616, %rdx
	movl	$-50, %eax
	jmp	.L42
.L41:
	movabsq	$-432345564227567616, %rdx
	movl	$-54, %eax
	jmp	.L42
.L84:
	movabsq	$-4035225266123964416, %rdx
	movl	$-57, %eax
	jmp	.L42
.L43:
	movabsq	$-6917529027641081856, %rdx
	movl	$-60, %eax
	jmp	.L42
.L35:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5059:
	.size	_ZN2v88internal6StrtodENS0_6VectorIKcEEi, .-_ZN2v88internal6StrtodENS0_6VectorIKcEEi
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal6StrtodENS0_6VectorIKcEEi,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal6StrtodENS0_6VectorIKcEEi, @function
_GLOBAL__sub_I__ZN2v88internal6StrtodENS0_6VectorIKcEEi:
.LFB5830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE5830:
	.size	_GLOBAL__sub_I__ZN2v88internal6StrtodENS0_6VectorIKcEEi, .-_GLOBAL__sub_I__ZN2v88internal6StrtodENS0_6VectorIKcEEi
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal6StrtodENS0_6VectorIKcEEi
	.section	.rodata._ZN2v88internalL19exact_powers_of_tenE,"a"
	.align 32
	.type	_ZN2v88internalL19exact_powers_of_tenE, @object
	.size	_ZN2v88internalL19exact_powers_of_tenE, 184
_ZN2v88internalL19exact_powers_of_tenE:
	.long	0
	.long	1072693248
	.long	0
	.long	1076101120
	.long	0
	.long	1079574528
	.long	0
	.long	1083129856
	.long	0
	.long	1086556160
	.long	0
	.long	1090021888
	.long	0
	.long	1093567616
	.long	0
	.long	1097011920
	.long	0
	.long	1100470148
	.long	0
	.long	1104006501
	.long	536870912
	.long	1107468383
	.long	3892314112
	.long	1110919286
	.long	2717908992
	.long	1114446484
	.long	3846176768
	.long	1117925532
	.long	512753664
	.long	1121369284
	.long	640942080
	.long	1124887541
	.long	937459712
	.long	1128383353
	.long	2245566464
	.long	1131820119
	.long	1733216256
	.long	1135329645
	.long	1620131072
	.long	1138841828
	.long	2025163840
	.long	1142271773
	.long	3605196624
	.long	1145772772
	.long	105764242
	.long	1149300943
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	2146435072
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
