	.file	"lsan-page-allocator.cc"
	.text
	.section	.text._ZN2v813PageAllocator18DiscardSystemPagesEPvm,"axG",@progbits,_ZN2v813PageAllocator18DiscardSystemPagesEPvm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813PageAllocator18DiscardSystemPagesEPvm
	.type	_ZN2v813PageAllocator18DiscardSystemPagesEPvm, @function
_ZN2v813PageAllocator18DiscardSystemPagesEPvm:
.LFB1941:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1941:
	.size	_ZN2v813PageAllocator18DiscardSystemPagesEPvm, .-_ZN2v813PageAllocator18DiscardSystemPagesEPvm
	.section	.text._ZN2v84base17LsanPageAllocator16AllocatePageSizeEv,"axG",@progbits,_ZN2v84base17LsanPageAllocator16AllocatePageSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base17LsanPageAllocator16AllocatePageSizeEv
	.type	_ZN2v84base17LsanPageAllocator16AllocatePageSizeEv, @function
_ZN2v84base17LsanPageAllocator16AllocatePageSizeEv:
.LFB1953:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE1953:
	.size	_ZN2v84base17LsanPageAllocator16AllocatePageSizeEv, .-_ZN2v84base17LsanPageAllocator16AllocatePageSizeEv
	.section	.text._ZN2v84base17LsanPageAllocator14CommitPageSizeEv,"axG",@progbits,_ZN2v84base17LsanPageAllocator14CommitPageSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base17LsanPageAllocator14CommitPageSizeEv
	.type	_ZN2v84base17LsanPageAllocator14CommitPageSizeEv, @function
_ZN2v84base17LsanPageAllocator14CommitPageSizeEv:
.LFB1954:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE1954:
	.size	_ZN2v84base17LsanPageAllocator14CommitPageSizeEv, .-_ZN2v84base17LsanPageAllocator14CommitPageSizeEv
	.section	.text._ZN2v84base17LsanPageAllocator17SetRandomMmapSeedEl,"axG",@progbits,_ZN2v84base17LsanPageAllocator17SetRandomMmapSeedEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base17LsanPageAllocator17SetRandomMmapSeedEl
	.type	_ZN2v84base17LsanPageAllocator17SetRandomMmapSeedEl, @function
_ZN2v84base17LsanPageAllocator17SetRandomMmapSeedEl:
.LFB1955:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.cfi_endproc
.LFE1955:
	.size	_ZN2v84base17LsanPageAllocator17SetRandomMmapSeedEl, .-_ZN2v84base17LsanPageAllocator17SetRandomMmapSeedEl
	.section	.text._ZN2v84base17LsanPageAllocator17GetRandomMmapAddrEv,"axG",@progbits,_ZN2v84base17LsanPageAllocator17GetRandomMmapAddrEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base17LsanPageAllocator17GetRandomMmapAddrEv
	.type	_ZN2v84base17LsanPageAllocator17GetRandomMmapAddrEv, @function
_ZN2v84base17LsanPageAllocator17GetRandomMmapAddrEv:
.LFB1956:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.cfi_endproc
.LFE1956:
	.size	_ZN2v84base17LsanPageAllocator17GetRandomMmapAddrEv, .-_ZN2v84base17LsanPageAllocator17GetRandomMmapAddrEv
	.section	.text._ZN2v84base17LsanPageAllocator14SetPermissionsEPvmNS_13PageAllocator10PermissionE,"axG",@progbits,_ZN2v84base17LsanPageAllocator14SetPermissionsEPvmNS_13PageAllocator10PermissionE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base17LsanPageAllocator14SetPermissionsEPvmNS_13PageAllocator10PermissionE
	.type	_ZN2v84base17LsanPageAllocator14SetPermissionsEPvmNS_13PageAllocator10PermissionE, @function
_ZN2v84base17LsanPageAllocator14SetPermissionsEPvmNS_13PageAllocator10PermissionE:
.LFB1957:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*72(%rax)
	.cfi_endproc
.LFE1957:
	.size	_ZN2v84base17LsanPageAllocator14SetPermissionsEPvmNS_13PageAllocator10PermissionE, .-_ZN2v84base17LsanPageAllocator14SetPermissionsEPvmNS_13PageAllocator10PermissionE
	.section	.text._ZN2v84base17LsanPageAllocator13AllocatePagesEPvmmNS_13PageAllocator10PermissionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base17LsanPageAllocator13AllocatePagesEPvmmNS_13PageAllocator10PermissionE
	.type	_ZN2v84base17LsanPageAllocator13AllocatePagesEPvmmNS_13PageAllocator10PermissionE, @function
_ZN2v84base17LsanPageAllocator13AllocatePagesEPvmmNS_13PageAllocator10PermissionE:
.LFB2598:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*48(%rax)
	.cfi_endproc
.LFE2598:
	.size	_ZN2v84base17LsanPageAllocator13AllocatePagesEPvmmNS_13PageAllocator10PermissionE, .-_ZN2v84base17LsanPageAllocator13AllocatePagesEPvmmNS_13PageAllocator10PermissionE
	.section	.text._ZN2v84base17LsanPageAllocator9FreePagesEPvm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base17LsanPageAllocator9FreePagesEPvm
	.type	_ZN2v84base17LsanPageAllocator9FreePagesEPvm, @function
_ZN2v84base17LsanPageAllocator9FreePagesEPvm:
.LFB2599:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*56(%rax)
	.cfi_endproc
.LFE2599:
	.size	_ZN2v84base17LsanPageAllocator9FreePagesEPvm, .-_ZN2v84base17LsanPageAllocator9FreePagesEPvm
	.section	.text._ZN2v84base17LsanPageAllocator12ReleasePagesEPvmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base17LsanPageAllocator12ReleasePagesEPvmm
	.type	_ZN2v84base17LsanPageAllocator12ReleasePagesEPvmm, @function
_ZN2v84base17LsanPageAllocator12ReleasePagesEPvmm:
.LFB2600:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*64(%rax)
	.cfi_endproc
.LFE2600:
	.size	_ZN2v84base17LsanPageAllocator12ReleasePagesEPvmm, .-_ZN2v84base17LsanPageAllocator12ReleasePagesEPvmm
	.section	.text._ZN2v84base17LsanPageAllocatorD2Ev,"axG",@progbits,_ZN2v84base17LsanPageAllocatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base17LsanPageAllocatorD2Ev
	.type	_ZN2v84base17LsanPageAllocatorD2Ev, @function
_ZN2v84base17LsanPageAllocatorD2Ev:
.LFB3143:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3143:
	.size	_ZN2v84base17LsanPageAllocatorD2Ev, .-_ZN2v84base17LsanPageAllocatorD2Ev
	.weak	_ZN2v84base17LsanPageAllocatorD1Ev
	.set	_ZN2v84base17LsanPageAllocatorD1Ev,_ZN2v84base17LsanPageAllocatorD2Ev
	.section	.text._ZN2v84base17LsanPageAllocatorD0Ev,"axG",@progbits,_ZN2v84base17LsanPageAllocatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base17LsanPageAllocatorD0Ev
	.type	_ZN2v84base17LsanPageAllocatorD0Ev, @function
_ZN2v84base17LsanPageAllocatorD0Ev:
.LFB3145:
	.cfi_startproc
	endbr64
	movl	$32, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE3145:
	.size	_ZN2v84base17LsanPageAllocatorD0Ev, .-_ZN2v84base17LsanPageAllocatorD0Ev
	.section	.text._ZN2v84base17LsanPageAllocatorC2EPNS_13PageAllocatorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v84base17LsanPageAllocatorC2EPNS_13PageAllocatorE
	.type	_ZN2v84base17LsanPageAllocatorC2EPNS_13PageAllocatorE, @function
_ZN2v84base17LsanPageAllocatorC2EPNS_13PageAllocatorE:
.LFB2596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v84base17LsanPageAllocatorE(%rip), %rax
	leaq	_ZN2v84base17LsanPageAllocator16AllocatePageSizeEv(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	(%rsi), %rdx
	movq	%rax, (%rbx)
	movq	%rsi, 8(%rbx)
	movq	16(%rdx), %rax
	cmpq	%rcx, %rax
	jne	.L14
	movq	16(%rsi), %rax
.L15:
	movq	%rax, 16(%rbx)
	movq	24(%rdx), %rax
	leaq	_ZN2v84base17LsanPageAllocator14CommitPageSizeEv(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L16
	movq	24(%rdi), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	call	*%rax
	movq	8(%rbx), %rdi
	movq	(%rdi), %rdx
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L16:
	call	*%rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2596:
	.size	_ZN2v84base17LsanPageAllocatorC2EPNS_13PageAllocatorE, .-_ZN2v84base17LsanPageAllocatorC2EPNS_13PageAllocatorE
	.globl	_ZN2v84base17LsanPageAllocatorC1EPNS_13PageAllocatorE
	.set	_ZN2v84base17LsanPageAllocatorC1EPNS_13PageAllocatorE,_ZN2v84base17LsanPageAllocatorC2EPNS_13PageAllocatorE
	.weak	_ZTVN2v84base17LsanPageAllocatorE
	.section	.data.rel.ro.local._ZTVN2v84base17LsanPageAllocatorE,"awG",@progbits,_ZTVN2v84base17LsanPageAllocatorE,comdat
	.align 8
	.type	_ZTVN2v84base17LsanPageAllocatorE, @object
	.size	_ZTVN2v84base17LsanPageAllocatorE, 104
_ZTVN2v84base17LsanPageAllocatorE:
	.quad	0
	.quad	0
	.quad	_ZN2v84base17LsanPageAllocatorD1Ev
	.quad	_ZN2v84base17LsanPageAllocatorD0Ev
	.quad	_ZN2v84base17LsanPageAllocator16AllocatePageSizeEv
	.quad	_ZN2v84base17LsanPageAllocator14CommitPageSizeEv
	.quad	_ZN2v84base17LsanPageAllocator17SetRandomMmapSeedEl
	.quad	_ZN2v84base17LsanPageAllocator17GetRandomMmapAddrEv
	.quad	_ZN2v84base17LsanPageAllocator13AllocatePagesEPvmmNS_13PageAllocator10PermissionE
	.quad	_ZN2v84base17LsanPageAllocator9FreePagesEPvm
	.quad	_ZN2v84base17LsanPageAllocator12ReleasePagesEPvmm
	.quad	_ZN2v84base17LsanPageAllocator14SetPermissionsEPvmNS_13PageAllocator10PermissionE
	.quad	_ZN2v813PageAllocator18DiscardSystemPagesEPvm
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
