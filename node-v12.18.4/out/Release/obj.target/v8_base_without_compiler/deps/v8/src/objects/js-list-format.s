	.file	"js-list-format.cc"
	.text
	.section	.text._ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,"axG",@progbits,_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.type	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB22968:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	movq	(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE22968:
	.size	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation:
.LFB22969:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L4
	cmpl	$3, %edx
	je	.L5
	cmpl	$1, %edx
	je	.L9
.L5:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22969:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.section	.text._ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB23070:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE23070:
	.size	_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB23098:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L11
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L11:
	ret
	.cfi_endproc
.LFE23098:
	.size	_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB23100:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE23100:
	.size	_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB23072:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23072:
	.size	_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB23099:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23099:
	.size	_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZN2v88internal7ManagedIN6icu_6713ListFormatterEE10DestructorEPv,"axG",@progbits,_ZN2v88internal7ManagedIN6icu_6713ListFormatterEE10DestructorEPv,comdat
	.p2align 4
	.weak	_ZN2v88internal7ManagedIN6icu_6713ListFormatterEE10DestructorEPv
	.type	_ZN2v88internal7ManagedIN6icu_6713ListFormatterEE10DestructorEPv, @function
_ZN2v88internal7ManagedIN6icu_6713ListFormatterEE10DestructorEPv:
.LFB22425:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L16
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	testq	%r13, %r13
	je	.L19
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L20
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L21:
	cmpl	$1, %eax
	je	.L28
.L19:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L16:
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L23
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L24:
	cmpl	$1, %eax
	jne	.L19
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L20:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L23:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L24
	.cfi_endproc
.LFE22425:
	.size	_ZN2v88internal7ManagedIN6icu_6713ListFormatterEE10DestructorEPv, .-_ZN2v88internal7ManagedIN6icu_6713ListFormatterEE10DestructorEPv
	.section	.rodata._ZN2v88internal9get_styleEPKc.str1.1,"aMS",@progbits,1
.LC0:
	.string	"arrow"
.LC1:
	.string	"ong"
.LC2:
	.string	"hort"
.LC3:
	.string	"unreachable code"
	.section	.text._ZN2v88internal9get_styleEPKc,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal9get_styleEPKc
	.type	_ZN2v88internal9get_styleEPKc, @function
_ZN2v88internal9get_styleEPKc:
.LFB18273:
	.cfi_startproc
	endbr64
	movzbl	(%rdi), %eax
	cmpb	$110, %al
	je	.L30
	cmpb	$115, %al
	je	.L31
	cmpb	$108, %al
	je	.L42
.L33:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	leaq	1(%rdi), %rsi
	movl	$6, %ecx
	leaq	.LC0(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L33
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	leaq	1(%rdi), %rsi
	movl	$4, %ecx
	leaq	.LC1(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	movsbl	%al, %eax
	testl	%eax, %eax
	jne	.L33
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	leaq	1(%rdi), %rsi
	movl	$5, %ecx
	leaq	.LC2(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L33
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE18273:
	.size	_ZN2v88internal9get_styleEPKc, .-_ZN2v88internal9get_styleEPKc
	.section	.rodata._ZN2v88internal8get_typeEPKc.str1.1,"aMS",@progbits,1
.LC4:
	.string	"onjunction"
.LC5:
	.string	"isjunction"
.LC6:
	.string	"nit"
	.section	.text._ZN2v88internal8get_typeEPKc,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8get_typeEPKc
	.type	_ZN2v88internal8get_typeEPKc, @function
_ZN2v88internal8get_typeEPKc:
.LFB18274:
	.cfi_startproc
	endbr64
	movzbl	(%rdi), %eax
	cmpb	$100, %al
	je	.L44
	cmpb	$117, %al
	je	.L45
	cmpb	$99, %al
	je	.L55
.L46:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	leaq	1(%rdi), %rsi
	movl	$11, %ecx
	leaq	.LC5(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L46
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	1(%rdi), %rsi
	movl	$11, %ecx
	leaq	.LC4(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	movsbl	%al, %eax
	testl	%eax, %eax
	jne	.L46
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	leaq	1(%rdi), %rsi
	movl	$4, %ecx
	leaq	.LC6(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L46
	movl	$2, %eax
	ret
	.cfi_endproc
.LFE18274:
	.size	_ZN2v88internal8get_typeEPKc, .-_ZN2v88internal8get_typeEPKc
	.section	.text._ZNK2v88internal12JSListFormat13StyleAsStringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12JSListFormat13StyleAsStringEv
	.type	_ZNK2v88internal12JSListFormat13StyleAsStringEv, @function
_ZNK2v88internal12JSListFormat13StyleAsStringEv:
.LFB18328:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movslq	43(%rdx), %rax
	andl	$3, %eax
	cmpl	$1, %eax
	je	.L57
	cmpl	$2, %eax
	je	.L58
	testl	%eax, %eax
	je	.L63
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	subq	$34352, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	subq	$34792, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	subq	$34704, %rax
	ret
	.cfi_endproc
.LFE18328:
	.size	_ZNK2v88internal12JSListFormat13StyleAsStringEv, .-_ZNK2v88internal12JSListFormat13StyleAsStringEv
	.section	.text._ZNK2v88internal12JSListFormat12TypeAsStringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12JSListFormat12TypeAsStringEv
	.type	_ZNK2v88internal12JSListFormat12TypeAsStringEv, @function
_ZNK2v88internal12JSListFormat12TypeAsStringEv:
.LFB18329:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movslq	43(%rdx), %rax
	shrl	$2, %eax
	andl	$3, %eax
	cmpl	$1, %eax
	je	.L65
	cmpl	$2, %eax
	je	.L66
	testl	%eax, %eax
	je	.L71
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	subq	$35240, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	subq	$35304, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	subq	$35632, %rax
	ret
	.cfi_endproc
.LFE18329:
	.size	_ZNK2v88internal12JSListFormat12TypeAsStringEv, .-_ZNK2v88internal12JSListFormat12TypeAsStringEv
	.section	.text._ZN2v88internal12JSListFormat15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12JSListFormat15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal12JSListFormat15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal12JSListFormat15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r13
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L73
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L74:
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L76
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L77:
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	leaq	-48(%rbp), %r14
	movq	%r12, %rdi
	leaq	1592(%r12), %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal12JSListFormat12TypeAsStringEv
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	leaq	1912(%r12), %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal12JSListFormat13StyleAsStringEv
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	leaq	1872(%r12), %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L81
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L82
.L75:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L76:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L83
.L78:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L83:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L78
.L81:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18327:
	.size	_ZN2v88internal12JSListFormat15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal12JSListFormat15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal12JSListFormat19GetAvailableLocalesB5cxx11Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12JSListFormat19GetAvailableLocalesB5cxx11Ev
	.type	_ZN2v88internal12JSListFormat19GetAvailableLocalesB5cxx11Ev, @function
_ZN2v88internal12JSListFormat19GetAvailableLocalesB5cxx11Ev:
.LFB18362:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	_ZZN2v88internal12JSListFormat19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %eax
	cmpb	$2, %al
	jne	.L95
.L85:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L96
	addq	$56, %rsp
	leaq	16+_ZZN2v88internal12JSListFormat19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	leaq	_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_676LocaleENS2_12_GLOBAL__N_116CheckListPatternEEENS0_32StaticallyAllocatedInstanceTraitIS9_EENS0_21DefaultConstructTraitIS9_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS9_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -64(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZZN2v88internal12JSListFormat19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rax
	leaq	-64(%rbp), %r12
	movq	%rax, -56(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r12, %rsi
	leaq	_ZZN2v88internal12JSListFormat19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L85
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L85
.L96:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18362:
	.size	_ZN2v88internal12JSListFormat19GetAvailableLocalesB5cxx11Ev, .-_ZN2v88internal12JSListFormat19GetAvailableLocalesB5cxx11Ev
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB21583:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L112
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L101:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L99
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L97
.L100:
	movq	%rbx, %r12
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L99:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L100
.L97:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE21583:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.rodata._ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_676LocaleENS2_12_GLOBAL__N_116CheckListPatternEEENS0_32StaticallyAllocatedInstanceTraitIS9_EENS0_21DefaultConstructTraitIS9_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS9_EEE12InitInstanceEPv.str1.1,"aMS",@progbits,1
.LC7:
	.string	"listPattern"
	.section	.text._ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_676LocaleENS2_12_GLOBAL__N_116CheckListPatternEEENS0_32StaticallyAllocatedInstanceTraitIS9_EENS0_21DefaultConstructTraitIS9_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS9_EEE12InitInstanceEPv,"ax",@progbits
	.p2align 4
	.type	_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_676LocaleENS2_12_GLOBAL__N_116CheckListPatternEEENS0_32StaticallyAllocatedInstanceTraitIS9_EENS0_21DefaultConstructTraitIS9_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS9_EEE12InitInstanceEPv, @function
_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_676LocaleENS2_12_GLOBAL__N_116CheckListPatternEEENS0_32StaticallyAllocatedInstanceTraitIS9_EENS0_21DefaultConstructTraitIS9_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS9_EEE12InitInstanceEPv:
.LFB22455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	16(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_676LocaleENS0_12_GLOBAL__N_116CheckListPatternEEE(%rip), %rax
	movl	$0, 16(%rdi)
	movq	%rax, (%rdi)
	movq	$0, 24(%rdi)
	movq	%r13, 32(%rdi)
	movq	%r13, 40(%rdi)
	movq	$0, 48(%rdi)
	leaq	-116(%rbp), %rdi
	movl	$0, -116(%rbp)
	call	_ZN6icu_676Locale19getAvailableLocalesERi@PLT
	movl	-116(%rbp), %edx
	leaq	-112(%rbp), %rdi
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	leaq	.LC7(%rip), %r8
	call	_ZN2v88internal4Intl14BuildLocaleSetB5cxx11EPKN6icu_676LocaleEiPKcS7_@PLT
	movq	24(%rbx), %r12
	testq	%r12, %r12
	je	.L116
	leaq	8(%rbx), %r15
.L119:
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %r14
	cmpq	%rax, %rdi
	je	.L117
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L116
.L118:
	movq	%r14, %r12
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L117:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L118
.L116:
	movq	-96(%rbp), %rax
	movq	$0, 24(%rbx)
	movq	%r13, 32(%rbx)
	movq	%r13, 40(%rbx)
	movq	$0, 48(%rbx)
	testq	%rax, %rax
	je	.L115
	movl	-104(%rbp), %edx
	movq	%rax, 24(%rbx)
	movl	%edx, 16(%rbx)
	movq	-88(%rbp), %rdx
	movq	%rdx, 32(%rbx)
	movq	-80(%rbp), %rdx
	movq	%rdx, 40(%rbx)
	movq	%r13, 8(%rax)
	movq	-72(%rbp), %rax
	movq	%rax, 48(%rbx)
.L115:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L135
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L135:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22455:
	.size	_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_676LocaleENS2_12_GLOBAL__N_116CheckListPatternEEENS0_32StaticallyAllocatedInstanceTraitIS9_EENS0_21DefaultConstructTraitIS9_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS9_EEE12InitInstanceEPv, .-_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_676LocaleENS2_12_GLOBAL__N_116CheckListPatternEEENS0_32StaticallyAllocatedInstanceTraitIS9_EENS0_21DefaultConstructTraitIS9_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS9_EEE12InitInstanceEPv
	.section	.text._ZN2v88internal4Intl16AvailableLocalesIN6icu_676LocaleENS0_12_GLOBAL__N_116CheckListPatternEED2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4Intl16AvailableLocalesIN6icu_676LocaleENS0_12_GLOBAL__N_116CheckListPatternEED2Ev, @function
_ZN2v88internal4Intl16AvailableLocalesIN6icu_676LocaleENS0_12_GLOBAL__N_116CheckListPatternEED2Ev:
.LFB23102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_676LocaleENS0_12_GLOBAL__N_116CheckListPatternEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	24(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L136
	leaq	8(%rdi), %r13
.L140:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L138
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L136
.L139:
	movq	%rbx, %r12
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L138:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L139
.L136:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23102:
	.size	_ZN2v88internal4Intl16AvailableLocalesIN6icu_676LocaleENS0_12_GLOBAL__N_116CheckListPatternEED2Ev, .-_ZN2v88internal4Intl16AvailableLocalesIN6icu_676LocaleENS0_12_GLOBAL__N_116CheckListPatternEED2Ev
	.set	_ZN2v88internal4Intl16AvailableLocalesIN6icu_676LocaleENS0_12_GLOBAL__N_116CheckListPatternEED1Ev,_ZN2v88internal4Intl16AvailableLocalesIN6icu_676LocaleENS0_12_GLOBAL__N_116CheckListPatternEED2Ev
	.section	.text._ZN2v88internal4Intl16AvailableLocalesIN6icu_676LocaleENS0_12_GLOBAL__N_116CheckListPatternEED0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4Intl16AvailableLocalesIN6icu_676LocaleENS0_12_GLOBAL__N_116CheckListPatternEED0Ev, @function
_ZN2v88internal4Intl16AvailableLocalesIN6icu_676LocaleENS0_12_GLOBAL__N_116CheckListPatternEED0Ev:
.LFB23104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_676LocaleENS0_12_GLOBAL__N_116CheckListPatternEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	24(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L152
	leaq	8(%rdi), %r14
.L155:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L153
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L152
.L154:
	movq	%rbx, %r12
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L153:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L154
.L152:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movl	$56, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23104:
	.size	_ZN2v88internal4Intl16AvailableLocalesIN6icu_676LocaleENS0_12_GLOBAL__N_116CheckListPatternEED0Ev, .-_ZN2v88internal4Intl16AvailableLocalesIN6icu_676LocaleENS0_12_GLOBAL__N_116CheckListPatternEED0Ev
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E:
.LFB21585:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L182
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L171:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	64(%r12), %rdi
	leaq	80(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L168
	call	_ZdlPv@PLT
.L168:
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L169
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L166
.L170:
	movq	%rbx, %r12
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L169:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L170
.L166:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE21585:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.section	.rodata._ZNSt6vectorIN6icu_6713UnicodeStringESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_.str1.1,"aMS",@progbits,1
.LC8:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN6icu_6713UnicodeStringESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN6icu_6713UnicodeStringESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN6icu_6713UnicodeStringESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.type	_ZNSt6vectorIN6icu_6713UnicodeStringESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, @function
_ZNSt6vectorIN6icu_6713UnicodeStringESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_:
.LFB22445:
	.cfi_startproc
	endbr64
	movabsq	$144115188075855871, %r8
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r13
	movq	%rax, -64(%rbp)
	subq	%r13, %rax
	sarq	$6, %rax
	cmpq	%r8, %rax
	je	.L204
	movq	%rsi, %r12
	movq	%rdi, %r14
	movq	%rdx, %rsi
	movq	%r12, %rcx
	subq	%r13, %rcx
	testq	%rax, %rax
	je	.L195
	movabsq	$9223372036854775744, %rdi
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L205
.L187:
	movq	%rsi, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdi
	movq	-80(%rbp), %rcx
	movq	%rax, %rbx
	movq	-88(%rbp), %rsi
	leaq	(%rax,%rdi), %rax
	movq	%rax, -72(%rbp)
	leaq	64(%rbx), %rax
	movq	%rax, -56(%rbp)
.L194:
	leaq	(%rbx,%rcx), %rdi
	call	_ZN6icu_6713UnicodeStringC1EOS0_@PLT
	cmpq	%r13, %r12
	je	.L189
	movq	%rbx, %rdx
	movq	%r13, %r15
	.p2align 4,,10
	.p2align 3
.L190:
	movq	%rdx, %rdi
	movq	%r15, %rsi
	movq	%rdx, -56(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EOS0_@PLT
	movq	(%r15), %rcx
	movq	%r15, %rdi
	addq	$64, %r15
	call	*(%rcx)
	movq	-56(%rbp), %rdx
	addq	$64, %rdx
	cmpq	%r15, %r12
	jne	.L190
	movq	%r12, %rax
	subq	%r13, %rax
	leaq	64(%rbx,%rax), %rax
	movq	%rax, -56(%rbp)
.L189:
	cmpq	-64(%rbp), %r12
	je	.L191
	movq	-56(%rbp), %rdx
	movq	%r12, %r15
	.p2align 4,,10
	.p2align 3
.L192:
	movq	%rdx, %rdi
	movq	%r15, %rsi
	movq	%rdx, -80(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EOS0_@PLT
	movq	(%r15), %rcx
	movq	%r15, %rdi
	addq	$64, %r15
	call	*(%rcx)
	movq	-80(%rbp), %rdx
	addq	$64, %rdx
	cmpq	-64(%rbp), %r15
	jne	.L192
	movq	%r15, %rax
	subq	%r12, %rax
	addq	%rax, -56(%rbp)
.L191:
	testq	%r13, %r13
	je	.L193
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L193:
	movq	-72(%rbp), %rax
	movq	%rbx, %xmm0
	movhps	-56(%rbp), %xmm0
	movq	%rax, 16(%r14)
	movups	%xmm0, (%r14)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L188
	movq	$64, -56(%rbp)
	xorl	%ebx, %ebx
	movq	$0, -72(%rbp)
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L195:
	movl	$64, %edi
	jmp	.L187
.L204:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L188:
	cmpq	%r8, %rdx
	cmova	%r8, %rdx
	salq	$6, %rdx
	movq	%rdx, %rdi
	jmp	.L187
	.cfi_endproc
.LFE22445:
	.size	_ZNSt6vectorIN6icu_6713UnicodeStringESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, .-_ZNSt6vectorIN6icu_6713UnicodeStringESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal9get_styleEPKc,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal9get_styleEPKc, @function
_GLOBAL__sub_I__ZN2v88internal9get_styleEPKc:
.LFB23105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23105:
	.size	_GLOBAL__sub_I__ZN2v88internal9get_styleEPKc, .-_GLOBAL__sub_I__ZN2v88internal9get_styleEPKc
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal9get_styleEPKc
	.section	.rodata._ZN2v88internal12JSListFormat3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_.str1.1,"aMS",@progbits,1
.LC9:
	.string	"or"
.LC10:
	.string	"or-short"
.LC11:
	.string	"standard-narrow"
.LC12:
	.string	"standard-short"
.LC13:
	.string	"unit-short"
.LC14:
	.string	"standard"
.LC15:
	.string	"unit-narrow"
.LC16:
	.string	"or-narrow"
.LC17:
	.string	"unit"
	.section	.rodata._ZN2v88internal12JSListFormat3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZN2v88internal12JSListFormat3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_.str1.1
.LC19:
	.string	"Intl.ListFormat"
.LC20:
	.string	"(location_) != nullptr"
.LC21:
	.string	"Check failed: %s."
.LC22:
	.string	"conjunction"
.LC23:
	.string	"disjunction"
.LC24:
	.string	"type"
.LC25:
	.string	"long"
.LC26:
	.string	"short"
.LC27:
	.string	"narrow"
.LC28:
	.string	"style"
	.section	.rodata._ZN2v88internal12JSListFormat3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_.str1.8
	.align 8
.LC29:
	.string	"Failed to create ICU list formatter, are ICU data files missing?"
	.section	.rodata._ZN2v88internal12JSListFormat3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_.str1.1
.LC30:
	.string	"(formatter) != nullptr"
	.section	.text._ZN2v88internal12JSListFormat3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12JSListFormat3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_
	.type	_ZN2v88internal12JSListFormat3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_, @function
_ZN2v88internal12JSListFormat3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_:
.LFB18275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-736(%rbp), %rdi
	pushq	%rbx
	subq	$808, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -808(%rbp)
	movq	%r12, %rsi
	movq	%rcx, -792(%rbp)
	xorl	%ecx, %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4Intl22CanonicalizeLocaleListB5cxx11EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEb@PLT
	cmpb	$0, -736(%rbp)
	je	.L210
	movq	-720(%rbp), %rbx
	pxor	%xmm0, %xmm0
	movq	-728(%rbp), %r9
	movq	$0, -752(%rbp)
	movaps	%xmm0, -768(%rbp)
	movq	%rbx, %r13
	subq	%r9, %r13
	movq	%r13, %rax
	sarq	$5, %rax
	je	.L401
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L402
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	-720(%rbp), %rbx
	movq	-728(%rbp), %r9
	movq	%rax, %r14
.L212:
	movq	%r14, %xmm0
	addq	%r14, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, -752(%rbp)
	movaps	%xmm0, -768(%rbp)
	cmpq	%rbx, %r9
	je	.L214
	leaq	-704(%rbp), %r13
	movq	%r12, -816(%rbp)
	movq	%r9, %r12
	movq	%rbx, -800(%rbp)
	movq	%r13, %r15
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L406:
	movzbl	(%rbx), %eax
	movb	%al, 16(%r14)
.L219:
	movq	%r13, 8(%r14)
	addq	$32, %r12
	addq	$32, %r14
	movb	$0, (%rdi,%r13)
	cmpq	%r12, -800(%rbp)
	je	.L403
.L220:
	leaq	16(%r14), %rdi
	movq	%rdi, (%r14)
	movq	(%r12), %rbx
	movq	8(%r12), %r13
	movq	%rbx, %rax
	addq	%r13, %rax
	je	.L215
	testq	%rbx, %rbx
	je	.L404
.L215:
	movq	%r13, -704(%rbp)
	cmpq	$15, %r13
	ja	.L405
	cmpq	$1, %r13
	je	.L406
	testq	%r13, %r13
	je	.L219
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L224:
	xorl	%r14d, %r14d
.L227:
	movq	-760(%rbp), %rbx
	movq	-768(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L281
	.p2align 4,,10
	.p2align 3
.L285:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L282
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L285
.L283:
	movq	-768(%rbp), %r12
.L281:
	testq	%r12, %r12
	je	.L210
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L210:
	movq	-720(%rbp), %rbx
	movq	-728(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L287
	.p2align 4,,10
	.p2align 3
.L291:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L288
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L291
.L289:
	movq	-728(%rbp), %r12
.L287:
	testq	%r12, %r12
	je	.L292
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L292:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L407
	addq	$808, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L291
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L405:
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r14)
	movq	%rax, %rdi
	movq	-704(%rbp), %rax
	movq	%rax, 16(%r14)
.L217:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-704(%rbp), %r13
	movq	(%r14), %rdi
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L403:
	movq	-816(%rbp), %r12
.L214:
	movq	-792(%rbp), %r13
	movq	%r14, -760(%rbp)
	movq	0(%r13), %rax
	cmpq	88(%r12), %rax
	je	.L408
	testb	$1, %al
	jne	.L223
.L226:
	movq	-792(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L224
.L222:
	leaq	.LC19(%rip), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4Intl16GetLocaleMatcherEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKc@PLT
	testb	%al, %al
	je	.L224
	sarq	$32, %rax
	movl	$0, -696(%rbp)
	leaq	-368(%rbp), %r10
	movq	$0, -688(%rbp)
	movq	%rax, %rbx
	leaq	-696(%rbp), %rax
	movq	%rax, -680(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -664(%rbp)
	movzbl	_ZZN2v88internal12JSListFormat19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %eax
	cmpb	$2, %al
	jne	.L409
.L228:
	leaq	-704(%rbp), %r15
	movl	%ebx, %r8d
	movq	%r12, %rsi
	movq	%r10, %rdi
	leaq	-768(%rbp), %rcx
	movq	%r15, %r9
	leaq	16+_ZZN2v88internal12JSListFormat19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rdx
	call	_ZN2v88internal4Intl13ResolveLocaleEPNS0_7IsolateERKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISA_ESaISA_EERKSt6vectorISA_SD_ENS1_13MatcherOptionESG_@PLT
	movq	-688(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L234
.L395:
	movq	24(%rbx), %rsi
	movq	%rbx, %r14
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r14), %rdi
	leaq	48(%r14), %rax
	movq	16(%rbx), %rbx
	cmpq	%rax, %rdi
	je	.L233
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L395
.L234:
	movq	-368(%rbp), %rbx
	movq	%rbx, %rdi
	call	strlen@PLT
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rbx, -704(%rbp)
	movq	%rax, -696(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, -792(%rbp)
	testq	%rax, %rax
	je	.L410
	movabsq	$4294967296, %rax
	movl	$12, %edi
	movl	$2, -640(%rbp)
	movq	%rax, -648(%rbp)
	call	_Znwm@PLT
	leaq	.LC22(%rip), %rcx
	movl	$24, %edi
	movq	%rax, %r14
	movq	-648(%rbp), %rax
	movq	%rcx, %xmm0
	movq	%rax, (%r14)
	movl	-640(%rbp), %eax
	movl	%eax, 8(%r14)
	leaq	.LC23(%rip), %rax
	movq	%rax, %xmm1
	leaq	.LC17(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -608(%rbp)
	movaps	%xmm0, -624(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movdqa	-624(%rbp), %xmm2
	movq	%rax, %rbx
	movaps	%xmm0, -704(%rbp)
	movups	%xmm2, (%rax)
	movq	-608(%rbp), %rax
	movq	$0, -784(%rbp)
	movq	%rax, 16(%rbx)
	movq	$0, -688(%rbp)
	call	_Znwm@PLT
	movq	16(%rbx), %rcx
	movdqu	(%rbx), %xmm3
	movq	%r12, %rdi
	leaq	24(%rax), %rdx
	leaq	-784(%rbp), %r9
	movq	%r13, %rsi
	movq	%rax, -704(%rbp)
	movq	%rcx, 16(%rax)
	leaq	.LC19(%rip), %r8
	movq	%r15, %rcx
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	leaq	.LC24(%rip), %rdx
	movups	%xmm3, (%rax)
	call	_ZN2v88internal4Intl15GetStringOptionEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKcSt6vectorIS8_SaIS8_EES8_PSt10unique_ptrIA_cSt14default_deleteISD_EE@PLT
	movq	-704(%rbp), %rdi
	movl	%eax, %edx
	testq	%rdi, %rdi
	je	.L236
	movl	%eax, -816(%rbp)
	movb	%al, -800(%rbp)
	call	_ZdlPv@PLT
	movl	-816(%rbp), %eax
	movzbl	-800(%rbp), %edx
.L236:
	movq	-784(%rbp), %rdi
	testb	%dl, %dl
	je	.L237
	shrw	$8, %ax
	jne	.L411
	testq	%rdi, %rdi
	je	.L394
	call	_ZdaPv@PLT
.L394:
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	movl	$0, -800(%rbp)
.L241:
	movabsq	$4294967296, %rax
	movl	$12, %edi
	movl	$2, -628(%rbp)
	movq	%rax, -636(%rbp)
	call	_Znwm@PLT
	leaq	.LC25(%rip), %rcx
	movl	$24, %edi
	movq	%rax, %r14
	movq	-636(%rbp), %rax
	movq	%rcx, %xmm0
	movq	%rax, (%r14)
	movl	-628(%rbp), %eax
	movl	%eax, 8(%r14)
	leaq	.LC26(%rip), %rax
	movq	%rax, %xmm5
	leaq	.LC27(%rip), %rax
	punpcklqdq	%xmm5, %xmm0
	movq	%rax, -576(%rbp)
	movaps	%xmm0, -592(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movdqa	-592(%rbp), %xmm6
	movq	%rax, %rbx
	movaps	%xmm0, -704(%rbp)
	movups	%xmm6, (%rax)
	movq	-576(%rbp), %rax
	movq	$0, -776(%rbp)
	movq	%rax, 16(%rbx)
	movq	$0, -688(%rbp)
	call	_Znwm@PLT
	movq	16(%rbx), %rcx
	movdqu	(%rbx), %xmm7
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	movq	%r12, %rdi
	leaq	-776(%rbp), %r9
	movq	%rax, -704(%rbp)
	movq	%rcx, 16(%rax)
	leaq	.LC19(%rip), %r8
	movq	%r15, %rcx
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	leaq	.LC28(%rip), %rdx
	movups	%xmm7, (%rax)
	call	_ZN2v88internal4Intl15GetStringOptionEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKcSt6vectorIS8_SaIS8_EES8_PSt10unique_ptrIA_cSt14default_deleteISD_EE@PLT
	movq	-704(%rbp), %rdi
	movl	%eax, %r13d
	testq	%rdi, %rdi
	je	.L244
	movb	%al, -816(%rbp)
	call	_ZdlPv@PLT
	movzbl	-816(%rbp), %eax
.L244:
	movq	-776(%rbp), %rdi
	testb	%al, %al
	je	.L237
	shrw	$8, %r13w
	je	.L246
	movq	(%rbx), %rsi
	movq	%rdi, -816(%rbp)
	call	strcmp@PLT
	movq	-816(%rbp), %rdi
	testl	%eax, %eax
	je	.L309
	movq	8(%rbx), %rsi
	call	strcmp@PLT
	movq	-816(%rbp), %rdi
	testl	%eax, %eax
	je	.L310
	movq	16(%rbx), %rsi
	call	strcmp@PLT
	movq	-816(%rbp), %rdi
	testl	%eax, %eax
	je	.L412
.L252:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L233:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L395
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L282:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L285
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L401:
	xorl	%r14d, %r14d
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L409:
	leaq	_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_676LocaleENS2_12_GLOBAL__N_116CheckListPatternEEENS0_32StaticallyAllocatedInstanceTraitIS9_EENS0_21DefaultConstructTraitIS9_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS9_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%r10, %rsi
	movq	%r10, -792(%rbp)
	movq	%rax, -368(%rbp)
	leaq	8+_ZZN2v88internal12JSListFormat19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rax
	movq	%rcx, %xmm0
	leaq	_ZZN2v88internal12JSListFormat19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rdi
	movq	%rax, -360(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -352(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-352(%rbp), %rax
	movq	-792(%rbp), %r10
	testq	%rax, %rax
	je	.L228
	movq	%r10, %rsi
	movq	%r10, %rdi
	movl	$3, %edx
	call	*%rax
	movq	-792(%rbp), %r10
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L408:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE@PLT
	movq	%rax, %r13
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L237:
	testq	%rdi, %rdi
	jne	.L413
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L242:
	xorl	%r14d, %r14d
	leaq	-336(%rbp), %r13
.L243:
	movq	-96(%rbp), %r12
	leaq	-112(%rbp), %r15
	testq	%r12, %r12
	je	.L279
.L274:
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	64(%r12), %rdi
	leaq	80(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L277
	call	_ZdlPv@PLT
.L277:
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L278
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L279
.L280:
	movq	%rbx, %r12
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L278:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L280
.L279:
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-368(%rbp), %rdi
	leaq	-352(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L227
	call	_ZdlPv@PLT
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L223:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L226
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L413:
	call	_ZdaPv@PLT
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L411:
	movq	(%rbx), %rsi
	movq	%rdi, -800(%rbp)
	call	strcmp@PLT
	movq	-800(%rbp), %rdi
	testl	%eax, %eax
	je	.L307
	movq	8(%rbx), %rsi
	call	strcmp@PLT
	movq	-800(%rbp), %rdi
	testl	%eax, %eax
	je	.L308
	movq	16(%rbx), %rsi
	call	strcmp@PLT
	movq	-800(%rbp), %rdi
	testl	%eax, %eax
	jne	.L252
	movl	$2, %eax
.L239:
	movl	(%r14,%rax,4), %eax
	movl	%eax, -800(%rbp)
	call	_ZdaPv@PLT
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L410:
	leaq	.LC20(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L246:
	testq	%rdi, %rdi
	jne	.L414
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L250:
	leaq	-336(%rbp), %r13
	leaq	-592(%rbp), %rbx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	cmpl	$1, -800(%rbp)
	movl	$0, -704(%rbp)
	movl	$0, -816(%rbp)
	je	.L316
.L249:
	movl	-800(%rbp), %eax
	cmpl	$2, %eax
	jne	.L415
	movl	-816(%rbp), %eax
	cmpl	$1, %eax
	je	.L317
	leaq	.LC15(%rip), %rsi
	cmpl	$2, %eax
	je	.L253
	leaq	.LC17(%rip), %rsi
	testl	%eax, %eax
	jne	.L252
	.p2align 4,,10
	.p2align 3
.L253:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN6icu_6713ListFormatter14createInstanceERKNS_6LocaleEPKcR10UErrorCode@PLT
	movq	%rax, %r15
	movl	-704(%rbp), %eax
	testl	%eax, %eax
	jg	.L416
	testq	%r15, %r15
	je	.L417
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%rax, %r9
	movabsq	$4294967297, %rax
	movq	%rax, 8(%r9)
	leaq	16+_ZTVSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	movq	%rax, (%r9)
	movq	32(%r12), %rax
	subq	48(%r12), %rax
	movq	%r15, 16(%r9)
	cmpq	$33554432, %rax
	jg	.L418
.L257:
	movq	%r9, %xmm5
	movq	%r15, %xmm0
	movl	$16, %edi
	movq	%r9, -840(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -832(%rbp)
	call	_Znwm@PLT
	movdqa	-832(%rbp), %xmm0
	movq	-840(%rbp), %r9
	movq	%rax, %r15
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movups	%xmm0, (%rax)
	leaq	8(%r9), %rax
	movq	%rax, %r14
	je	.L419
	lock addl	$1, (%r14)
.L258:
	movl	$48, %edi
	movq	%r9, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %r8
	movups	%xmm0, 8(%rax)
	movq	%r15, 24(%rax)
	movq	%r8, %rsi
	movq	$0, (%rax)
	leaq	_ZN2v88internal7ManagedIN6icu_6713ListFormatterEE10DestructorEPv(%rip), %rax
	movq	%rax, 32(%r8)
	movq	$0, 40(%r8)
	movq	%r8, -832(%rbp)
	call	_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE@PLT
	movq	41152(%r12), %rdi
	movq	(%rax), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	-832(%rbp), %r8
	movq	_ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE@GOTPCREL(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movq	%rax, 40(%r8)
	movq	%r8, %rsi
	call	_ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	-832(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal7Isolate28RegisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE@PLT
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	-840(%rbp), %r9
	je	.L259
	movl	$-1, %eax
	lock xaddl	%eax, (%r14)
.L260:
	cmpl	$1, %eax
	je	.L420
.L262:
	movq	-808(%rbp), %rsi
	movq	(%rsi), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	je	.L266
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$2, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory22NewSlowJSObjectFromMapENS0_6HandleINS0_3MapEEEiNS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	movq	%rax, %r14
.L267:
	movq	(%r14), %rax
	movq	$0, 39(%rax)
	movq	(%r14), %rdi
	movq	(%r15), %r12
	movq	%r12, 31(%rdi)
	leaq	31(%rdi), %rsi
	testb	$1, %r12b
	je	.L294
	movq	%r12, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	je	.L269
	movq	%r12, %rdx
	movq	%rsi, -832(%rbp)
	movq	%rdi, -808(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-832(%rbp), %rsi
	movq	-808(%rbp), %rdi
.L269:
	testb	$24, %al
	je	.L294
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L294
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L294:
	movq	-792(%rbp), %rax
	movq	(%r14), %r15
	movq	(%rax), %r12
	leaq	23(%r15), %rsi
	movq	%r12, 23(%r15)
	testb	$1, %r12b
	je	.L293
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -792(%rbp)
	testl	$262144, %eax
	je	.L272
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%rsi, -808(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-792(%rbp), %rcx
	movq	-808(%rbp), %rsi
	movq	8(%rcx), %rax
.L272:
	testb	$24, %al
	je	.L293
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L293
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L293:
	movq	(%r14), %rdx
	movl	-800(%rbp), %ecx
	movq	%rbx, %rdi
	movslq	43(%rdx), %rax
	sall	$2, %ecx
	andl	$-13, %eax
	orl	%ecx, %eax
	salq	$32, %rax
	movq	%rax, 39(%rdx)
	movq	(%r14), %rdx
	movslq	43(%rdx), %rax
	andl	$-4, %eax
	orl	-816(%rbp), %eax
	salq	$32, %rax
	movq	%rax, 39(%rdx)
	call	_ZN6icu_676LocaleD1Ev@PLT
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L415:
	testl	%eax, %eax
	jne	.L252
	movl	-816(%rbp), %eax
	cmpl	$1, %eax
	je	.L311
	cmpl	$2, %eax
	je	.L312
	testl	%eax, %eax
	jne	.L252
	leaq	.LC14(%rip), %rsi
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L266:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	movq	%rax, %r14
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L418:
	movq	%r12, %rdi
	movq	%r9, -832(%rbp)
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	movq	-832(%rbp), %r9
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L419:
	addl	$1, 8(%r9)
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L259:
	movl	8(%r9), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r9)
	jmp	.L260
.L414:
	call	_ZdaPv@PLT
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	jmp	.L250
.L420:
	movq	(%r9), %rax
	movq	%r9, -832(%rbp)
	movq	%r9, %rdi
	call	*16(%rax)
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	-832(%rbp), %r9
	je	.L263
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r9)
.L264:
	cmpl	$1, %eax
	jne	.L262
	movq	(%r9), %rax
	movq	%r9, %rdi
	call	*24(%rax)
	jmp	.L262
.L309:
	xorl	%eax, %eax
.L247:
	movl	(%r14,%rax,4), %eax
	leaq	-336(%rbp), %r13
	movl	%eax, -816(%rbp)
	call	_ZdaPv@PLT
	movq	%rbx, %rdi
	leaq	-592(%rbp), %rbx
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	cmpl	$1, -800(%rbp)
	movl	$0, -704(%rbp)
	jne	.L249
	cmpl	$1, -816(%rbp)
	leaq	.LC10(%rip), %rsi
	je	.L253
	movl	-816(%rbp), %eax
	cmpl	$2, %eax
	je	.L315
	testl	%eax, %eax
	jne	.L252
.L316:
	leaq	.LC9(%rip), %rsi
	jmp	.L253
.L317:
	leaq	.LC13(%rip), %rsi
	jmp	.L253
.L312:
	leaq	.LC11(%rip), %rsi
	jmp	.L253
.L417:
	leaq	.LC30(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L311:
	leaq	.LC12(%rip), %rsi
	jmp	.L253
.L263:
	movl	12(%r9), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r9)
	jmp	.L264
.L307:
	xorl	%eax, %eax
	jmp	.L239
.L308:
	movl	$1, %eax
	jmp	.L239
.L315:
	leaq	.LC16(%rip), %rsi
	jmp	.L253
.L310:
	movl	$1, %eax
	jmp	.L247
.L412:
	movl	$2, %eax
	jmp	.L247
.L404:
	leaq	.LC18(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L407:
	call	__stack_chk_fail@PLT
.L402:
	call	_ZSt17__throw_bad_allocv@PLT
.L416:
	testq	%r15, %r15
	je	.L255
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
.L255:
	leaq	.LC29(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18275:
	.size	_ZN2v88internal12JSListFormat3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_, .-_ZN2v88internal12JSListFormat3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_
	.section	.text._ZN2v88internal12_GLOBAL__N_120ToUnicodeStringArrayEPNS0_7IsolateENS0_6HandleINS0_7JSArrayEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_120ToUnicodeStringArrayEPNS0_7IsolateENS0_6HandleINS0_7JSArrayEEE, @function
_ZN2v88internal12_GLOBAL__N_120ToUnicodeStringArrayEPNS0_7IsolateENS0_6HandleINS0_7JSArrayEEE:
.LFB18330:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$152, %rsp
	movq	(%rdx), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-1(%rsi), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*56(%rax)
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %rcx
	movq	$0, -144(%rbp)
	movl	%eax, -172(%rbp)
	movq	%rcx, -168(%rbp)
	movaps	%xmm0, -160(%rbp)
	testl	%eax, %eax
	je	.L469
.L422:
	movq	(%r14), %rax
	movl	%r15d, %edx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	*40(%rax)
	movq	(%rax), %rdx
	testb	$1, %dl
	jne	.L424
.L428:
	movl	%r15d, %ecx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	xorl	%esi, %esi
	cvtsi2sdq	%rcx, %xmm0
	leaq	3344(%r12), %rbx
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	%rbx, %r8
	movq	%r12, %rdi
	movl	$15, %esi
	leaq	1576(%r12), %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movb	$0, 0(%r13)
	pxor	%xmm0, %xmm0
	movq	-152(%rbp), %rbx
	movq	$0, 24(%r13)
	movq	-160(%rbp), %r12
	movups	%xmm0, 8(%r13)
.L427:
	cmpq	%r12, %rbx
	je	.L454
	.p2align 4,,10
	.p2align 3
.L455:
	movq	(%r12), %rax
	movq	%r12, %rdi
	addq	$64, %r12
	call	*(%rax)
	cmpq	%rbx, %r12
	jne	.L455
	movq	-160(%rbp), %r12
.L454:
	testq	%r12, %r12
	je	.L421
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L421:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L470
	addq	$152, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L424:
	.cfi_restore_state
	movq	-1(%rdx), %rsi
	cmpw	$63, 11(%rsi)
	ja	.L428
	movq	-1(%rdx), %rsi
	movzwl	11(%rsi), %esi
	andl	$7, %esi
	cmpw	$1, %si
	jne	.L430
	movq	23(%rdx), %rsi
	movl	11(%rsi), %edi
	testl	%edi, %edi
	je	.L430
	movq	-1(%rdx), %rdi
	movq	%rdx, %r9
	cmpw	$63, 11(%rdi)
	jbe	.L471
.L432:
	movq	-1(%r9), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L472
	.p2align 4,,10
	.p2align 3
.L430:
	movq	-168(%rbp), %rdi
	movq	%rax, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal4Intl18ToICUUnicodeStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-152(%rbp), %rdi
	cmpq	-144(%rbp), %rdi
	je	.L446
	movq	-168(%rbp), %rsi
	call	_ZN6icu_6713UnicodeStringC1EOS0_@PLT
	addq	$64, -152(%rbp)
.L468:
	movq	-168(%rbp), %rdi
	addl	$1, %r15d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpl	%r15d, -172(%rbp)
	jne	.L422
	movq	-152(%rbp), %r15
	movq	-160(%rbp), %rbx
	pxor	%xmm0, %xmm0
	movb	$1, 0(%r13)
	movq	$0, 24(%r13)
	movq	%r15, %r12
	movups	%xmm0, 8(%r13)
	subq	%rbx, %r12
	movq	%r12, %rdx
	sarq	$6, %rdx
	je	.L423
	movabsq	$144115188075855871, %rax
	cmpq	%rax, %rdx
	ja	.L473
	movq	%r12, %rdi
	call	_Znwm@PLT
	movq	-152(%rbp), %r15
	movq	-160(%rbp), %rbx
	movq	%rax, -168(%rbp)
.L450:
	movq	-168(%rbp), %r14
	movq	%r14, %xmm0
	addq	%r14, %r12
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, 24(%r13)
	movq	%rbx, %r12
	movups	%xmm0, 8(%r13)
	cmpq	%rbx, %r15
	je	.L452
	.p2align 4,,10
	.p2align 3
.L453:
	movq	%r12, %rsi
	movq	%r14, %rdi
	addq	$64, %r12
	addq	$64, %r14
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	cmpq	%r12, %r15
	jne	.L453
	subq	%rbx, %r15
	addq	%r15, -168(%rbp)
	movq	-152(%rbp), %rbx
	movq	-160(%rbp), %r12
.L452:
	movq	-168(%rbp), %rax
	movq	%rax, 16(%r13)
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L446:
	leaq	-160(%rbp), %r9
	movq	-168(%rbp), %rdx
	movq	%rdi, %rsi
	movq	%r9, %rdi
	call	_ZNSt6vectorIN6icu_6713UnicodeStringESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L471:
	movq	-1(%rdx), %rdi
	movzwl	11(%rdi), %edi
	andl	$7, %edi
	cmpw	$1, %di
	jne	.L432
	movq	-1(%rdx), %rdi
	movzwl	11(%rdi), %edi
	andl	$7, %edi
	cmpw	$1, %di
	jne	.L435
	movl	11(%rsi), %ecx
	testl	%ecx, %ecx
	je	.L435
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L472:
	movq	-1(%r9), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$5, %dx
	jne	.L430
	movq	(%rax), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L443
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L469:
	movb	$1, 0(%r13)
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	movups	%xmm0, 8(%r13)
.L423:
	movq	$0, -168(%rbp)
	jmp	.L450
.L435:
	movq	41112(%r12), %rdi
	movq	15(%rdx), %r9
	testq	%rdi, %rdi
	je	.L437
	movq	%r9, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r9
	jmp	.L432
.L443:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L474
.L445:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L430
.L437:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L475
.L439:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%r9, (%rax)
	jmp	.L432
.L474:
	movq	%r12, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rsi
	jmp	.L445
.L475:
	movq	%r12, %rdi
	movq	%r9, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %r9
	jmp	.L439
.L470:
	call	__stack_chk_fail@PLT
.L473:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE18330:
	.size	_ZN2v88internal12_GLOBAL__N_120ToUnicodeStringArrayEPNS0_7IsolateENS0_6HandleINS0_7JSArrayEEE, .-_ZN2v88internal12_GLOBAL__N_120ToUnicodeStringArrayEPNS0_7IsolateENS0_6HandleINS0_7JSArrayEEE
	.section	.text._ZN2v88internal12JSListFormat10FormatListEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_7JSArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12JSListFormat10FormatListEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_7JSArrayEEE
	.type	_ZN2v88internal12JSListFormat10FormatListEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_7JSArrayEEE, @function
_ZN2v88internal12JSListFormat10FormatListEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_7JSArrayEEE:
.LFB18358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-96(%rbp), %rdi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -160(%rbp)
	movq	%r14, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal12_GLOBAL__N_120ToUnicodeStringArrayEPNS0_7IsolateENS0_6HandleINS0_7JSArrayEEE
	cmpb	$0, -96(%rbp)
	je	.L493
	movq	-80(%rbp), %rbx
	movq	-88(%rbp), %r15
	movq	%rbx, %rdi
	subq	%r15, %rdi
	movq	%rdi, %rax
	sarq	$6, %rax
	je	.L494
	movabsq	$144115188075855871, %rdx
	cmpq	%rdx, %rax
	ja	.L504
	call	_Znwm@PLT
	movq	-80(%rbp), %rbx
	movq	-88(%rbp), %r15
	movq	%rax, %r13
.L478:
	cmpq	%r15, %rbx
	je	.L495
	movq	%r15, %r12
	movq	%r13, %rax
	.p2align 4,,10
	.p2align 3
.L481:
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -152(%rbp)
	addq	$64, %r12
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-152(%rbp), %rax
	addq	$64, %rax
	cmpq	%r12, %rbx
	jne	.L481
	subq	%r15, %rbx
	addq	%r13, %rbx
.L480:
	movq	-160(%rbp), %rax
	movq	(%rax), %rax
	movq	31(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L505
	movq	%rbx, %rcx
	leaq	-128(%rbp), %r12
	leaq	-132(%rbp), %r8
	movq	%r13, %rdx
	movl	$0, -132(%rbp)
	subq	%r13, %rcx
	movq	%r12, %rdi
	sarq	$6, %rcx
	call	_ZNK6icu_6713ListFormatter20formatStringsToValueEPKNS_13UnicodeStringEiR10UErrorCode@PLT
	movl	-132(%rbp), %eax
	testl	%eax, %eax
	jg	.L506
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4Intl17FormattedToStringEPNS0_7IsolateERKN6icu_6714FormattedValueE@PLT
	movq	%rax, %r14
.L484:
	movq	%r12, %rdi
	movq	%r13, %r12
	call	_ZN6icu_6713FormattedListD1Ev@PLT
	cmpq	%r13, %rbx
	je	.L488
	.p2align 4,,10
	.p2align 3
.L485:
	movq	(%r12), %rax
	movq	%r12, %rdi
	addq	$64, %r12
	call	*(%rax)
	cmpq	%r12, %rbx
	jne	.L485
.L488:
	testq	%r13, %r13
	je	.L477
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L493:
	xorl	%r14d, %r14d
.L477:
	movq	-80(%rbp), %rbx
	movq	-88(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L489
	.p2align 4,,10
	.p2align 3
.L490:
	movq	(%r12), %rax
	movq	%r12, %rdi
	addq	$64, %r12
	call	*(%rax)
	cmpq	%r12, %rbx
	jne	.L490
	movq	-88(%rbp), %r12
.L489:
	testq	%r12, %r12
	je	.L491
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L491:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L507
	addq	$120, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L494:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L506:
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$10, %esi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	%r14, %rdi
	xorl	%edx, %edx
	xorl	%r14d, %r14d
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L495:
	movq	%r13, %rbx
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L505:
	leaq	.LC30(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L507:
	call	__stack_chk_fail@PLT
.L504:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE18358:
	.size	_ZN2v88internal12JSListFormat10FormatListEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_7JSArrayEEE, .-_ZN2v88internal12JSListFormat10FormatListEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_7JSArrayEEE
	.section	.text._ZN2v88internal12_GLOBAL__N_122FormattedListToJSArrayEPNS0_7IsolateERKN6icu_6714FormattedValueE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_122FormattedListToJSArrayEPNS0_7IsolateERKN6icu_6714FormattedValueE, @function
_ZN2v88internal12_GLOBAL__N_122FormattedListToJSArrayEPNS0_7IsolateERKN6icu_6714FormattedValueE:
.LFB18354:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r14
	leaq	-128(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$3, %esi
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
	movq	%r14, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN6icu_6724ConstrainedFieldPositionC1Ev@PLT
	movl	$3, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6724ConstrainedFieldPosition17constrainCategoryEi@PLT
	leaq	-164(%rbp), %rax
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, -192(%rbp)
	movq	%rax, %rdx
	movq	(%rbx), %rax
	movl	$0, -164(%rbp)
	call	*16(%rax)
	leaq	1584(%r12), %rax
	movq	%rax, -208(%rbp)
.L519:
	movq	(%rbx), %rax
	movq	-192(%rbp), %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L509
	movl	-164(%rbp), %edx
	testl	%edx, %edx
	jg	.L510
	movl	-144(%rbp), %ecx
	movl	-148(%rbp), %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4Intl8ToStringEPNS0_7IsolateERKN6icu_6713UnicodeStringEii@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L525
	movl	-152(%rbp), %eax
	testl	%eax, %eax
	jne	.L526
	movq	-208(%rbp), %rcx
.L518:
	movq	-200(%rbp), %rsi
	leal	1(%r13), %eax
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	%eax, -180(%rbp)
	call	_ZN2v88internal4Intl10AddElementEPNS0_7IsolateENS0_6HandleINS0_7JSArrayEEEiNS4_INS0_6StringEEES8_@PLT
	movl	-180(%rbp), %r13d
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L526:
	cmpl	$1, %eax
	jne	.L527
	leaq	2472(%r12), %rcx
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L510:
	xorl	%edx, %edx
	movl	$10, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	.p2align 4,,10
	.p2align 3
.L525:
	xorl	%r12d, %r12d
.L514:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6724ConstrainedFieldPositionD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L528
	addq	$168, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L509:
	.cfi_restore_state
	movl	-164(%rbp), %eax
	testl	%eax, %eax
	jg	.L510
	movq	-200(%rbp), %r12
	movq	(%r12), %rdi
	call	_ZN2v88internal8JSObject16ValidateElementsES1_@PLT
	jmp	.L514
.L527:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L528:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18354:
	.size	_ZN2v88internal12_GLOBAL__N_122FormattedListToJSArrayEPNS0_7IsolateERKN6icu_6714FormattedValueE, .-_ZN2v88internal12_GLOBAL__N_122FormattedListToJSArrayEPNS0_7IsolateERKN6icu_6714FormattedValueE
	.section	.text._ZN2v88internal12JSListFormat17FormatListToPartsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_7JSArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12JSListFormat17FormatListToPartsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_7JSArrayEEE
	.type	_ZN2v88internal12JSListFormat17FormatListToPartsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_7JSArrayEEE, @function
_ZN2v88internal12JSListFormat17FormatListToPartsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_7JSArrayEEE:
.LFB18359:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-96(%rbp), %rdi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -160(%rbp)
	movq	%r14, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal12_GLOBAL__N_120ToUnicodeStringArrayEPNS0_7IsolateENS0_6HandleINS0_7JSArrayEEE
	cmpb	$0, -96(%rbp)
	je	.L546
	movq	-80(%rbp), %rbx
	movq	-88(%rbp), %r15
	movq	%rbx, %rdi
	subq	%r15, %rdi
	movq	%rdi, %rax
	sarq	$6, %rax
	je	.L547
	movabsq	$144115188075855871, %rdx
	cmpq	%rdx, %rax
	ja	.L557
	call	_Znwm@PLT
	movq	-80(%rbp), %rbx
	movq	-88(%rbp), %r15
	movq	%rax, %r13
.L531:
	cmpq	%r15, %rbx
	je	.L548
	movq	%r15, %r12
	movq	%r13, %rax
	.p2align 4,,10
	.p2align 3
.L534:
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -152(%rbp)
	addq	$64, %r12
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-152(%rbp), %rax
	addq	$64, %rax
	cmpq	%r12, %rbx
	jne	.L534
	subq	%r15, %rbx
	addq	%r13, %rbx
.L533:
	movq	-160(%rbp), %rax
	movq	(%rax), %rax
	movq	31(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L558
	movq	%rbx, %rcx
	leaq	-128(%rbp), %r12
	leaq	-132(%rbp), %r8
	movq	%r13, %rdx
	movl	$0, -132(%rbp)
	subq	%r13, %rcx
	movq	%r12, %rdi
	sarq	$6, %rcx
	call	_ZNK6icu_6713ListFormatter20formatStringsToValueEPKNS_13UnicodeStringEiR10UErrorCode@PLT
	movl	-132(%rbp), %eax
	testl	%eax, %eax
	jg	.L559
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal12_GLOBAL__N_122FormattedListToJSArrayEPNS0_7IsolateERKN6icu_6714FormattedValueE
	movq	%rax, %r14
.L537:
	movq	%r12, %rdi
	movq	%r13, %r12
	call	_ZN6icu_6713FormattedListD1Ev@PLT
	cmpq	%r13, %rbx
	je	.L541
	.p2align 4,,10
	.p2align 3
.L538:
	movq	(%r12), %rax
	movq	%r12, %rdi
	addq	$64, %r12
	call	*(%rax)
	cmpq	%r12, %rbx
	jne	.L538
.L541:
	testq	%r13, %r13
	je	.L530
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L546:
	xorl	%r14d, %r14d
.L530:
	movq	-80(%rbp), %rbx
	movq	-88(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L542
	.p2align 4,,10
	.p2align 3
.L543:
	movq	(%r12), %rax
	movq	%r12, %rdi
	addq	$64, %r12
	call	*(%rax)
	cmpq	%r12, %rbx
	jne	.L543
	movq	-88(%rbp), %r12
.L542:
	testq	%r12, %r12
	je	.L544
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L544:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L560
	addq	$120, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L547:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L559:
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$10, %esi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	%r14, %rdi
	xorl	%edx, %edx
	xorl	%r14d, %r14d
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L548:
	movq	%r13, %rbx
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L558:
	leaq	.LC30(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L560:
	call	__stack_chk_fail@PLT
.L557:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE18359:
	.size	_ZN2v88internal12JSListFormat17FormatListToPartsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_7JSArrayEEE, .-_ZN2v88internal12JSListFormat17FormatListToPartsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_7JSArrayEEE
	.section	.data.rel.ro.local._ZTVN2v88internal4Intl16AvailableLocalesIN6icu_676LocaleENS0_12_GLOBAL__N_116CheckListPatternEEE,"aw"
	.align 8
	.type	_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_676LocaleENS0_12_GLOBAL__N_116CheckListPatternEEE, @object
	.size	_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_676LocaleENS0_12_GLOBAL__N_116CheckListPatternEEE, 32
_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_676LocaleENS0_12_GLOBAL__N_116CheckListPatternEEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4Intl16AvailableLocalesIN6icu_676LocaleENS0_12_GLOBAL__N_116CheckListPatternEED1Ev
	.quad	_ZN2v88internal4Intl16AvailableLocalesIN6icu_676LocaleENS0_12_GLOBAL__N_116CheckListPatternEED0Ev
	.weak	_ZTVSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt15_Sp_counted_ptrIPN6icu_6713ListFormatterELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.bss._ZZN2v88internal12JSListFormat19GetAvailableLocalesB5cxx11EvE17available_locales,"aw",@nobits
	.align 32
	.type	_ZZN2v88internal12JSListFormat19GetAvailableLocalesB5cxx11EvE17available_locales, @object
	.size	_ZZN2v88internal12JSListFormat19GetAvailableLocalesB5cxx11EvE17available_locales, 64
_ZZN2v88internal12JSListFormat19GetAvailableLocalesB5cxx11EvE17available_locales:
	.zero	64
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
