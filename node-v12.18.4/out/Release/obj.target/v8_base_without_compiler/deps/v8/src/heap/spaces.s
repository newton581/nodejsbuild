	.file	"spaces.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4369:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4369:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4370:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4370:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB4372:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4372:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal5Space29StartNextInlineAllocationStepEv,"axG",@progbits,_ZN2v88internal5Space29StartNextInlineAllocationStepEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal5Space29StartNextInlineAllocationStepEv
	.type	_ZN2v88internal5Space29StartNextInlineAllocationStepEv, @function
_ZN2v88internal5Space29StartNextInlineAllocationStepEv:
.LFB7998:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7998:
	.size	_ZN2v88internal5Space29StartNextInlineAllocationStepEv, .-_ZN2v88internal5Space29StartNextInlineAllocationStepEv
	.section	.text._ZN2v88internal5Space15CommittedMemoryEv,"axG",@progbits,_ZN2v88internal5Space15CommittedMemoryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal5Space15CommittedMemoryEv
	.type	_ZN2v88internal5Space15CommittedMemoryEv, @function
_ZN2v88internal5Space15CommittedMemoryEv:
.LFB7999:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	ret
	.cfi_endproc
.LFE7999:
	.size	_ZN2v88internal5Space15CommittedMemoryEv, .-_ZN2v88internal5Space15CommittedMemoryEv
	.section	.text._ZN2v88internal5Space22MaximumCommittedMemoryEv,"axG",@progbits,_ZN2v88internal5Space22MaximumCommittedMemoryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal5Space22MaximumCommittedMemoryEv
	.type	_ZN2v88internal5Space22MaximumCommittedMemoryEv, @function
_ZN2v88internal5Space22MaximumCommittedMemoryEv:
.LFB8000:
	.cfi_startproc
	endbr64
	movq	88(%rdi), %rax
	ret
	.cfi_endproc
.LFE8000:
	.size	_ZN2v88internal5Space22MaximumCommittedMemoryEv, .-_ZN2v88internal5Space22MaximumCommittedMemoryEv
	.section	.text._ZN2v88internal5Space13SizeOfObjectsEv,"axG",@progbits,_ZN2v88internal5Space13SizeOfObjectsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal5Space13SizeOfObjectsEv
	.type	_ZN2v88internal5Space13SizeOfObjectsEv, @function
_ZN2v88internal5Space13SizeOfObjectsEv:
.LFB8001:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*72(%rax)
	.cfi_endproc
.LFE8001:
	.size	_ZN2v88internal5Space13SizeOfObjectsEv, .-_ZN2v88internal5Space13SizeOfObjectsEv
	.section	.text._ZN2v88internal5Space30RoundSizeDownToObjectAlignmentEi,"axG",@progbits,_ZN2v88internal5Space30RoundSizeDownToObjectAlignmentEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal5Space30RoundSizeDownToObjectAlignmentEi
	.type	_ZN2v88internal5Space30RoundSizeDownToObjectAlignmentEi, @function
_ZN2v88internal5Space30RoundSizeDownToObjectAlignmentEi:
.LFB8002:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	andl	$-8, %eax
	cmpl	$3, 72(%rdi)
	je	.L12
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	%esi, %eax
	andl	$-32, %eax
	ret
	.cfi_endproc
.LFE8002:
	.size	_ZN2v88internal5Space30RoundSizeDownToObjectAlignmentEi, .-_ZN2v88internal5Space30RoundSizeDownToObjectAlignmentEi
	.section	.text._ZN2v88internal17FreeListFastAlloc21GuaranteedAllocatableEm,"axG",@progbits,_ZN2v88internal17FreeListFastAlloc21GuaranteedAllocatableEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17FreeListFastAlloc21GuaranteedAllocatableEm
	.type	_ZN2v88internal17FreeListFastAlloc21GuaranteedAllocatableEm, @function
_ZN2v88internal17FreeListFastAlloc21GuaranteedAllocatableEm:
.LFB8148:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$16376, %rsi
	jbe	.L13
	cmpq	$65529, %rsi
	sbbq	%rax, %rax
	andq	$-49152, %rax
	addq	$65528, %rax
.L13:
	ret
	.cfi_endproc
.LFE8148:
	.size	_ZN2v88internal17FreeListFastAlloc21GuaranteedAllocatableEm, .-_ZN2v88internal17FreeListFastAlloc21GuaranteedAllocatableEm
	.section	.text._ZN2v88internal17FreeListFastAlloc26SelectFreeListCategoryTypeEm,"axG",@progbits,_ZN2v88internal17FreeListFastAlloc26SelectFreeListCategoryTypeEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17FreeListFastAlloc26SelectFreeListCategoryTypeEm
	.type	_ZN2v88internal17FreeListFastAlloc26SelectFreeListCategoryTypeEm, @function
_ZN2v88internal17FreeListFastAlloc26SelectFreeListCategoryTypeEm:
.LFB8149:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$16376, %rsi
	jbe	.L17
	xorl	%eax, %eax
	cmpq	$65528, %rsi
	seta	%al
	addl	$1, %eax
.L17:
	ret
	.cfi_endproc
.LFE8149:
	.size	_ZN2v88internal17FreeListFastAlloc26SelectFreeListCategoryTypeEm, .-_ZN2v88internal17FreeListFastAlloc26SelectFreeListCategoryTypeEm
	.section	.text._ZN2v88internal12FreeListMany26SelectFreeListCategoryTypeEm,"axG",@progbits,_ZN2v88internal12FreeListMany26SelectFreeListCategoryTypeEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12FreeListMany26SelectFreeListCategoryTypeEm
	.type	_ZN2v88internal12FreeListMany26SelectFreeListCategoryTypeEm, @function
_ZN2v88internal12FreeListMany26SelectFreeListCategoryTypeEm:
.LFB8150:
	.cfi_startproc
	endbr64
	cmpq	$256, %rsi
	ja	.L22
	movq	%rsi, %rax
	movl	$0, %edx
	shrq	$4, %rax
	subl	$1, %eax
	cmpq	$31, %rsi
	cmovbe	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movl	12(%rdi), %eax
	cmpl	$15, %eax
	jle	.L21
	cmpq	$511, %rsi
	jbe	.L26
	cmpl	$16, %eax
	je	.L21
	cmpq	$1023, %rsi
	jbe	.L27
	cmpl	$17, %eax
	je	.L21
	cmpq	$2047, %rsi
	jbe	.L28
	cmpl	$18, %eax
	je	.L21
	cmpq	$4095, %rsi
	jbe	.L29
	cmpl	$19, %eax
	je	.L21
	cmpq	$8191, %rsi
	jbe	.L30
	cmpl	$20, %eax
	je	.L21
	cmpq	$16383, %rsi
	jbe	.L31
	cmpl	$21, %eax
	je	.L21
	cmpq	$32767, %rsi
	jbe	.L32
	cmpl	$22, %eax
	je	.L21
	xorl	%eax, %eax
	cmpq	$65535, %rsi
	seta	%al
	addl	$22, %eax
	ret
.L32:
	movl	$21, %eax
.L21:
	ret
.L26:
	movl	$15, %eax
	ret
.L27:
	movl	$16, %eax
	ret
.L28:
	movl	$17, %eax
	ret
.L29:
	movl	$18, %eax
	ret
.L30:
	movl	$19, %eax
	ret
.L31:
	movl	$20, %eax
	ret
	.cfi_endproc
.LFE8150:
	.size	_ZN2v88internal12FreeListMany26SelectFreeListCategoryTypeEm, .-_ZN2v88internal12FreeListMany26SelectFreeListCategoryTypeEm
	.section	.text._ZN2v88internal11FreeListMap26SelectFreeListCategoryTypeEm,"axG",@progbits,_ZN2v88internal11FreeListMap26SelectFreeListCategoryTypeEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11FreeListMap26SelectFreeListCategoryTypeEm
	.type	_ZN2v88internal11FreeListMap26SelectFreeListCategoryTypeEm, @function
_ZN2v88internal11FreeListMap26SelectFreeListCategoryTypeEm:
.LFB8155:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8155:
	.size	_ZN2v88internal11FreeListMap26SelectFreeListCategoryTypeEm, .-_ZN2v88internal11FreeListMap26SelectFreeListCategoryTypeEm
	.section	.text._ZN2v88internal10PagedSpace9AvailableEv,"axG",@progbits,_ZN2v88internal10PagedSpace9AvailableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10PagedSpace9AvailableEv
	.type	_ZN2v88internal10PagedSpace9AvailableEv, @function
_ZN2v88internal10PagedSpace9AvailableEv:
.LFB8179:
	.cfi_startproc
	endbr64
	movq	96(%rdi), %rax
	movq	40(%rax), %rax
	ret
	.cfi_endproc
.LFE8179:
	.size	_ZN2v88internal10PagedSpace9AvailableEv, .-_ZN2v88internal10PagedSpace9AvailableEv
	.section	.text._ZN2v88internal10PagedSpace4SizeEv,"axG",@progbits,_ZN2v88internal10PagedSpace4SizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10PagedSpace4SizeEv
	.type	_ZN2v88internal10PagedSpace4SizeEv, @function
_ZN2v88internal10PagedSpace4SizeEv:
.LFB8180:
	.cfi_startproc
	endbr64
	movq	184(%rdi), %rax
	ret
	.cfi_endproc
.LFE8180:
	.size	_ZN2v88internal10PagedSpace4SizeEv, .-_ZN2v88internal10PagedSpace4SizeEv
	.section	.text._ZN2v88internal10PagedSpace8is_localEv,"axG",@progbits,_ZN2v88internal10PagedSpace8is_localEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10PagedSpace8is_localEv
	.type	_ZN2v88internal10PagedSpace8is_localEv, @function
_ZN2v88internal10PagedSpace8is_localEv:
.LFB8191:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8191:
	.size	_ZN2v88internal10PagedSpace8is_localEv, .-_ZN2v88internal10PagedSpace8is_localEv
	.section	.text._ZN2v88internal10PagedSpace12snapshotableEv,"axG",@progbits,_ZN2v88internal10PagedSpace12snapshotableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10PagedSpace12snapshotableEv
	.type	_ZN2v88internal10PagedSpace12snapshotableEv, @function
_ZN2v88internal10PagedSpace12snapshotableEv:
.LFB8198:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE8198:
	.size	_ZN2v88internal10PagedSpace12snapshotableEv, .-_ZN2v88internal10PagedSpace12snapshotableEv
	.section	.text._ZN2v88internal9SemiSpace13SizeOfObjectsEv,"axG",@progbits,_ZN2v88internal9SemiSpace13SizeOfObjectsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9SemiSpace13SizeOfObjectsEv
	.type	_ZN2v88internal9SemiSpace13SizeOfObjectsEv, @function
_ZN2v88internal9SemiSpace13SizeOfObjectsEv:
.LFB8227:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*72(%rax)
	.cfi_endproc
.LFE8227:
	.size	_ZN2v88internal9SemiSpace13SizeOfObjectsEv, .-_ZN2v88internal9SemiSpace13SizeOfObjectsEv
	.section	.text._ZN2v88internal8NewSpace4SizeEv,"axG",@progbits,_ZN2v88internal8NewSpace4SizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8NewSpace4SizeEv
	.type	_ZN2v88internal8NewSpace4SizeEv, @function
_ZN2v88internal8NewSpace4SizeEv:
.LFB8239:
	.cfi_startproc
	endbr64
	movslq	360(%rdi), %rax
	movq	352(%rdi), %rdx
	imulq	$261864, %rax, %rax
	subq	40(%rdx), %rax
	addq	104(%rdi), %rax
	ret
	.cfi_endproc
.LFE8239:
	.size	_ZN2v88internal8NewSpace4SizeEv, .-_ZN2v88internal8NewSpace4SizeEv
	.section	.text._ZN2v88internal8NewSpace15CommittedMemoryEv,"axG",@progbits,_ZN2v88internal8NewSpace15CommittedMemoryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8NewSpace15CommittedMemoryEv
	.type	_ZN2v88internal8NewSpace15CommittedMemoryEv, @function
_ZN2v88internal8NewSpace15CommittedMemoryEv:
.LFB8243:
	.cfi_startproc
	endbr64
	movq	448(%rdi), %rax
	addq	288(%rdi), %rax
	ret
	.cfi_endproc
.LFE8243:
	.size	_ZN2v88internal8NewSpace15CommittedMemoryEv, .-_ZN2v88internal8NewSpace15CommittedMemoryEv
	.section	.text._ZN2v88internal8NewSpace22MaximumCommittedMemoryEv,"axG",@progbits,_ZN2v88internal8NewSpace22MaximumCommittedMemoryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8NewSpace22MaximumCommittedMemoryEv
	.type	_ZN2v88internal8NewSpace22MaximumCommittedMemoryEv, @function
_ZN2v88internal8NewSpace22MaximumCommittedMemoryEv:
.LFB8244:
	.cfi_startproc
	endbr64
	movq	456(%rdi), %rax
	addq	296(%rdi), %rax
	ret
	.cfi_endproc
.LFE8244:
	.size	_ZN2v88internal8NewSpace22MaximumCommittedMemoryEv, .-_ZN2v88internal8NewSpace22MaximumCommittedMemoryEv
	.section	.text._ZN2v88internal8NewSpace24SupportsInlineAllocationEv,"axG",@progbits,_ZN2v88internal8NewSpace24SupportsInlineAllocationEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8NewSpace24SupportsInlineAllocationEv
	.type	_ZN2v88internal8NewSpace24SupportsInlineAllocationEv, @function
_ZN2v88internal8NewSpace24SupportsInlineAllocationEv:
.LFB8268:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE8268:
	.size	_ZN2v88internal8NewSpace24SupportsInlineAllocationEv, .-_ZN2v88internal8NewSpace24SupportsInlineAllocationEv
	.section	.text._ZN2v88internal15CompactionSpace8is_localEv,"axG",@progbits,_ZN2v88internal15CompactionSpace8is_localEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CompactionSpace8is_localEv
	.type	_ZN2v88internal15CompactionSpace8is_localEv, @function
_ZN2v88internal15CompactionSpace8is_localEv:
.LFB8272:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE8272:
	.size	_ZN2v88internal15CompactionSpace8is_localEv, .-_ZN2v88internal15CompactionSpace8is_localEv
	.section	.text._ZN2v88internal15CompactionSpace12snapshotableEv,"axG",@progbits,_ZN2v88internal15CompactionSpace12snapshotableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CompactionSpace12snapshotableEv
	.type	_ZN2v88internal15CompactionSpace12snapshotableEv, @function
_ZN2v88internal15CompactionSpace12snapshotableEv:
.LFB8273:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8273:
	.size	_ZN2v88internal15CompactionSpace12snapshotableEv, .-_ZN2v88internal15CompactionSpace12snapshotableEv
	.section	.text._ZN2v88internal13ReadOnlySpace9AvailableEv,"axG",@progbits,_ZN2v88internal13ReadOnlySpace9AvailableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ReadOnlySpace9AvailableEv
	.type	_ZN2v88internal13ReadOnlySpace9AvailableEv, @function
_ZN2v88internal13ReadOnlySpace9AvailableEv:
.LFB8295:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8295:
	.size	_ZN2v88internal13ReadOnlySpace9AvailableEv, .-_ZN2v88internal13ReadOnlySpace9AvailableEv
	.section	.text._ZN2v88internal16LargeObjectSpace4SizeEv,"axG",@progbits,_ZN2v88internal16LargeObjectSpace4SizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16LargeObjectSpace4SizeEv
	.type	_ZN2v88internal16LargeObjectSpace4SizeEv, @function
_ZN2v88internal16LargeObjectSpace4SizeEv:
.LFB8300:
	.cfi_startproc
	endbr64
	movq	104(%rdi), %rax
	ret
	.cfi_endproc
.LFE8300:
	.size	_ZN2v88internal16LargeObjectSpace4SizeEv, .-_ZN2v88internal16LargeObjectSpace4SizeEv
	.section	.text._ZN2v88internal16LargeObjectSpace13SizeOfObjectsEv,"axG",@progbits,_ZN2v88internal16LargeObjectSpace13SizeOfObjectsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16LargeObjectSpace13SizeOfObjectsEv
	.type	_ZN2v88internal16LargeObjectSpace13SizeOfObjectsEv, @function
_ZN2v88internal16LargeObjectSpace13SizeOfObjectsEv:
.LFB8301:
	.cfi_startproc
	endbr64
	movq	120(%rdi), %rax
	ret
	.cfi_endproc
.LFE8301:
	.size	_ZN2v88internal16LargeObjectSpace13SizeOfObjectsEv, .-_ZN2v88internal16LargeObjectSpace13SizeOfObjectsEv
	.section	.text._ZN2v88internal5Space24PauseAllocationObserversEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Space24PauseAllocationObserversEv
	.type	_ZN2v88internal5Space24PauseAllocationObserversEv, @function
_ZN2v88internal5Space24PauseAllocationObserversEv:
.LFB22928:
	.cfi_startproc
	endbr64
	movb	$1, 56(%rdi)
	ret
	.cfi_endproc
.LFE22928:
	.size	_ZN2v88internal5Space24PauseAllocationObserversEv, .-_ZN2v88internal5Space24PauseAllocationObserversEv
	.section	.text._ZN2v88internal5Space25ResumeAllocationObserversEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Space25ResumeAllocationObserversEv
	.type	_ZN2v88internal5Space25ResumeAllocationObserversEv, @function
_ZN2v88internal5Space25ResumeAllocationObserversEv:
.LFB22929:
	.cfi_startproc
	endbr64
	movb	$0, 56(%rdi)
	ret
	.cfi_endproc
.LFE22929:
	.size	_ZN2v88internal5Space25ResumeAllocationObserversEv, .-_ZN2v88internal5Space25ResumeAllocationObserversEv
	.section	.text._ZN2v88internal16LargeObjectSpace9AvailableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16LargeObjectSpace9AvailableEv
	.type	_ZN2v88internal16LargeObjectSpace9AvailableEv, @function
_ZN2v88internal16LargeObjectSpace9AvailableEv:
.LFB22988:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22988:
	.size	_ZN2v88internal16LargeObjectSpace9AvailableEv, .-_ZN2v88internal16LargeObjectSpace9AvailableEv
	.section	.text._ZN2v88internal18FreeListManyCached11AddCategoryEPNS0_16FreeListCategoryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18FreeListManyCached11AddCategoryEPNS0_16FreeListCategoryE
	.type	_ZN2v88internal18FreeListManyCached11AddCategoryEPNS0_16FreeListCategoryE, @function
_ZN2v88internal18FreeListManyCached11AddCategoryEPNS0_16FreeListCategoryE:
.LFB23070:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$0, 8(%rsi)
	je	.L73
	movslq	(%rsi), %rdx
	movq	32(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L75
	movq	%rsi, 16(%rax)
.L75:
	movq	%rax, 24(%rsi)
	movq	32(%rdi), %rax
	movq	%rsi, (%rax,%rdx,8)
	movl	4(%rsi), %eax
	addq	%rax, 40(%rdi)
	movl	(%rsi), %edx
	testl	%edx, %edx
	js	.L77
	movslq	%edx, %rax
	leaq	-4(%rdi), %rcx
	leaq	(%rdi,%rax,4), %rax
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L83:
	movl	%edx, 48(%rax)
	subq	$4, %rax
	cmpq	%rcx, %rax
	je	.L77
.L78:
	cmpl	48(%rax), %edx
	jl	.L83
.L77:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	ret
	.cfi_endproc
.LFE23070:
	.size	_ZN2v88internal18FreeListManyCached11AddCategoryEPNS0_16FreeListCategoryE, .-_ZN2v88internal18FreeListManyCached11AddCategoryEPNS0_16FreeListCategoryE
	.section	.text._ZN2v88internal11FreeListMap21GuaranteedAllocatableEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11FreeListMap21GuaranteedAllocatableEm
	.type	_ZN2v88internal11FreeListMap21GuaranteedAllocatableEm, @function
_ZN2v88internal11FreeListMap21GuaranteedAllocatableEm:
.LFB23079:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	ret
	.cfi_endproc
.LFE23079:
	.size	_ZN2v88internal11FreeListMap21GuaranteedAllocatableEm, .-_ZN2v88internal11FreeListMap21GuaranteedAllocatableEm
	.section	.text._ZN2v88internal11FreeListMap14GetPageForSizeEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11FreeListMap14GetPageForSizeEm
	.type	_ZN2v88internal11FreeListMap14GetPageForSizeEm, @function
_ZN2v88internal11FreeListMap14GetPageForSizeEm:
.LFB23080:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L85
	movq	8(%rax), %rax
	andq	$-262144, %rax
.L85:
	ret
	.cfi_endproc
.LFE23080:
	.size	_ZN2v88internal11FreeListMap14GetPageForSizeEm, .-_ZN2v88internal11FreeListMap14GetPageForSizeEm
	.section	.text._ZN2v88internal8FreeList11AddCategoryEPNS0_16FreeListCategoryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8FreeList11AddCategoryEPNS0_16FreeListCategoryE
	.type	_ZN2v88internal8FreeList11AddCategoryEPNS0_16FreeListCategoryE, @function
_ZN2v88internal8FreeList11AddCategoryEPNS0_16FreeListCategoryE:
.LFB23092:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$0, 8(%rsi)
	je	.L90
	movslq	(%rsi), %rdx
	movq	32(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L92
	movq	%rsi, 16(%rax)
.L92:
	movq	%rax, 24(%rsi)
	movq	32(%rdi), %rax
	movq	%rsi, (%rax,%rdx,8)
	movl	4(%rsi), %eax
	addq	%rax, 40(%rdi)
	movl	$1, %eax
.L90:
	ret
	.cfi_endproc
.LFE23092:
	.size	_ZN2v88internal8FreeList11AddCategoryEPNS0_16FreeListCategoryE, .-_ZN2v88internal8FreeList11AddCategoryEPNS0_16FreeListCategoryE
	.section	.text._ZN2v88internal30LargeObjectSpaceObjectIterator4NextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30LargeObjectSpaceObjectIterator4NextEv
	.type	_ZN2v88internal30LargeObjectSpaceObjectIterator4NextEv, @function
_ZN2v88internal30LargeObjectSpaceObjectIterator4NextEv:
.LFB23132:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L98
	movq	40(%rdx), %rax
	movq	224(%rdx), %rdx
	movq	%rdx, 8(%rdi)
	addq	$1, %rax
.L98:
	ret
	.cfi_endproc
.LFE23132:
	.size	_ZN2v88internal30LargeObjectSpaceObjectIterator4NextEv, .-_ZN2v88internal30LargeObjectSpaceObjectIterator4NextEv
	.section	.text._ZN2v88internal16LargeObjectSpace7AddPageEPNS0_9LargePageEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16LargeObjectSpace7AddPageEPNS0_9LargePageEm
	.type	_ZN2v88internal16LargeObjectSpace7AddPageEPNS0_9LargePageEm, @function
_ZN2v88internal16LargeObjectSpace7AddPageEPNS0_9LargePageEm:
.LFB23149:
	.cfi_startproc
	endbr64
	movslq	(%rsi), %rax
	addq	%rax, 104(%rdi)
	movq	80(%rdi), %rax
	addq	(%rsi), %rax
	movq	%rax, 80(%rdi)
	cmpq	88(%rdi), %rax
	jbe	.L102
	movq	%rax, 88(%rdi)
.L102:
	movq	40(%rdi), %rax
	addq	%rdx, 120(%rdi)
	addl	$1, 112(%rdi)
	testq	%rax, %rax
	je	.L103
	movq	224(%rax), %rdx
	movq	%rax, 232(%rsi)
	movq	%rdx, 224(%rsi)
	movq	%rsi, 224(%rax)
	testq	%rdx, %rdx
	je	.L104
	movq	%rsi, 232(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	movq	%rsi, 40(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 224(%rsi)
	movq	%rsi, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 32(%rdi)
	ret
	.cfi_endproc
.LFE23149:
	.size	_ZN2v88internal16LargeObjectSpace7AddPageEPNS0_9LargePageEm, .-_ZN2v88internal16LargeObjectSpace7AddPageEPNS0_9LargePageEm
	.section	.text._ZN2v88internal16LargeObjectSpace10RemovePageEPNS0_9LargePageEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16LargeObjectSpace10RemovePageEPNS0_9LargePageEm
	.type	_ZN2v88internal16LargeObjectSpace10RemovePageEPNS0_9LargePageEm, @function
_ZN2v88internal16LargeObjectSpace10RemovePageEPNS0_9LargePageEm:
.LFB23150:
	.cfi_startproc
	endbr64
	movslq	(%rsi), %rax
	subq	%rax, 104(%rdi)
	movq	(%rsi), %rax
	subq	%rdx, 120(%rdi)
	subq	%rax, 80(%rdi)
	subl	$1, 112(%rdi)
	cmpq	40(%rdi), %rsi
	je	.L117
	movq	224(%rsi), %rax
	cmpq	32(%rdi), %rsi
	je	.L118
.L108:
	movq	232(%rsi), %rdx
	testq	%rax, %rax
	je	.L109
	movq	%rdx, 232(%rax)
.L109:
	testq	%rdx, %rdx
	je	.L110
	movq	%rax, 224(%rdx)
.L110:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 224(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	movq	232(%rsi), %rax
	movq	%rax, 40(%rdi)
	movq	224(%rsi), %rax
	cmpq	32(%rdi), %rsi
	jne	.L108
.L118:
	movq	%rax, 32(%rdi)
	movq	224(%rsi), %rax
	jmp	.L108
	.cfi_endproc
.LFE23150:
	.size	_ZN2v88internal16LargeObjectSpace10RemovePageEPNS0_9LargePageEm, .-_ZN2v88internal16LargeObjectSpace10RemovePageEPNS0_9LargePageEm
	.section	.text._ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,"axG",@progbits,_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.type	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB30278:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	movq	(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE30278:
	.size	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation:
.LFB30279:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L121
	cmpl	$3, %edx
	je	.L122
	cmpl	$1, %edx
	je	.L126
.L122:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE30279:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.section	.text._ZN2v88internal30LargeObjectSpaceObjectIteratorD2Ev,"axG",@progbits,_ZN2v88internal30LargeObjectSpaceObjectIteratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal30LargeObjectSpaceObjectIteratorD2Ev
	.type	_ZN2v88internal30LargeObjectSpaceObjectIteratorD2Ev, @function
_ZN2v88internal30LargeObjectSpaceObjectIteratorD2Ev:
.LFB30702:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE30702:
	.size	_ZN2v88internal30LargeObjectSpaceObjectIteratorD2Ev, .-_ZN2v88internal30LargeObjectSpaceObjectIteratorD2Ev
	.weak	_ZN2v88internal30LargeObjectSpaceObjectIteratorD1Ev
	.set	_ZN2v88internal30LargeObjectSpaceObjectIteratorD1Ev,_ZN2v88internal30LargeObjectSpaceObjectIteratorD2Ev
	.section	.text._ZN2v88internal23SemiSpaceObjectIteratorD2Ev,"axG",@progbits,_ZN2v88internal23SemiSpaceObjectIteratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23SemiSpaceObjectIteratorD2Ev
	.type	_ZN2v88internal23SemiSpaceObjectIteratorD2Ev, @function
_ZN2v88internal23SemiSpaceObjectIteratorD2Ev:
.LFB30742:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE30742:
	.size	_ZN2v88internal23SemiSpaceObjectIteratorD2Ev, .-_ZN2v88internal23SemiSpaceObjectIteratorD2Ev
	.weak	_ZN2v88internal23SemiSpaceObjectIteratorD1Ev
	.set	_ZN2v88internal23SemiSpaceObjectIteratorD1Ev,_ZN2v88internal23SemiSpaceObjectIteratorD2Ev
	.section	.text._ZN2v88internal24PagedSpaceObjectIteratorD2Ev,"axG",@progbits,_ZN2v88internal24PagedSpaceObjectIteratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24PagedSpaceObjectIteratorD2Ev
	.type	_ZN2v88internal24PagedSpaceObjectIteratorD2Ev, @function
_ZN2v88internal24PagedSpaceObjectIteratorD2Ev:
.LFB30746:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE30746:
	.size	_ZN2v88internal24PagedSpaceObjectIteratorD2Ev, .-_ZN2v88internal24PagedSpaceObjectIteratorD2Ev
	.weak	_ZN2v88internal24PagedSpaceObjectIteratorD1Ev
	.set	_ZN2v88internal24PagedSpaceObjectIteratorD1Ev,_ZN2v88internal24PagedSpaceObjectIteratorD2Ev
	.section	.text._ZN2v88internal10NoFreeListD2Ev,"axG",@progbits,_ZN2v88internal10NoFreeListD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10NoFreeListD2Ev
	.type	_ZN2v88internal10NoFreeListD2Ev, @function
_ZN2v88internal10NoFreeListD2Ev:
.LFB30750:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE30750:
	.size	_ZN2v88internal10NoFreeListD2Ev, .-_ZN2v88internal10NoFreeListD2Ev
	.weak	_ZN2v88internal10NoFreeListD1Ev
	.set	_ZN2v88internal10NoFreeListD1Ev,_ZN2v88internal10NoFreeListD2Ev
	.section	.text._ZNK2v88internal5Space25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE,"axG",@progbits,_ZNK2v88internal5Space25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal5Space25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE
	.type	_ZNK2v88internal5Space25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE, @function
_ZNK2v88internal5Space25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE:
.LFB8005:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rax
	ret
	.cfi_endproc
.LFE8005:
	.size	_ZNK2v88internal5Space25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE, .-_ZNK2v88internal5Space25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE
	.section	.text._ZNK2v88internal8NewSpace25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE,"axG",@progbits,_ZNK2v88internal8NewSpace25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8NewSpace25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE
	.type	_ZNK2v88internal8NewSpace25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE, @function
_ZNK2v88internal8NewSpace25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE:
.LFB8246:
	.cfi_startproc
	endbr64
	movq	256(%rdi), %rax
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rax
	ret
	.cfi_endproc
.LFE8246:
	.size	_ZNK2v88internal8NewSpace25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE, .-_ZNK2v88internal8NewSpace25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE
	.section	.text._ZN2v88internal10PagedSpace5WasteEv,"axG",@progbits,_ZN2v88internal10PagedSpace5WasteEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10PagedSpace5WasteEv
	.type	_ZN2v88internal10PagedSpace5WasteEv, @function
_ZN2v88internal10PagedSpace5WasteEv:
.LFB8181:
	.cfi_startproc
	endbr64
	movq	96(%rdi), %rax
	movq	24(%rax), %rax
	ret
	.cfi_endproc
.LFE8181:
	.size	_ZN2v88internal10PagedSpace5WasteEv, .-_ZN2v88internal10PagedSpace5WasteEv
	.section	.text._ZN2v88internal24PagedSpaceObjectIteratorD0Ev,"axG",@progbits,_ZN2v88internal24PagedSpaceObjectIteratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24PagedSpaceObjectIteratorD0Ev
	.type	_ZN2v88internal24PagedSpaceObjectIteratorD0Ev, @function
_ZN2v88internal24PagedSpaceObjectIteratorD0Ev:
.LFB30748:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE30748:
	.size	_ZN2v88internal24PagedSpaceObjectIteratorD0Ev, .-_ZN2v88internal24PagedSpaceObjectIteratorD0Ev
	.section	.text._ZN2v88internal23SemiSpaceObjectIteratorD0Ev,"axG",@progbits,_ZN2v88internal23SemiSpaceObjectIteratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23SemiSpaceObjectIteratorD0Ev
	.type	_ZN2v88internal23SemiSpaceObjectIteratorD0Ev, @function
_ZN2v88internal23SemiSpaceObjectIteratorD0Ev:
.LFB30744:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE30744:
	.size	_ZN2v88internal23SemiSpaceObjectIteratorD0Ev, .-_ZN2v88internal23SemiSpaceObjectIteratorD0Ev
	.section	.text._ZN2v88internal30LargeObjectSpaceObjectIteratorD0Ev,"axG",@progbits,_ZN2v88internal30LargeObjectSpaceObjectIteratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal30LargeObjectSpaceObjectIteratorD0Ev
	.type	_ZN2v88internal30LargeObjectSpaceObjectIteratorD0Ev, @function
_ZN2v88internal30LargeObjectSpaceObjectIteratorD0Ev:
.LFB30704:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE30704:
	.size	_ZN2v88internal30LargeObjectSpaceObjectIteratorD0Ev, .-_ZN2v88internal30LargeObjectSpaceObjectIteratorD0Ev
	.section	.text._ZN2v88internal5Space24RemoveAllocationObserverEPNS0_18AllocationObserverE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Space24RemoveAllocationObserverEPNS0_18AllocationObserverE
	.type	_ZN2v88internal5Space24RemoveAllocationObserverEPNS0_18AllocationObserverE, @function
_ZN2v88internal5Space24RemoveAllocationObserverEPNS0_18AllocationObserverE:
.LFB22927:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rdx
	movq	8(%rdi), %rdi
	movq	%rdx, %rax
	subq	%rdi, %rax
	movq	%rax, %rsi
	sarq	$5, %rax
	sarq	$3, %rsi
	testq	%rax, %rax
	jle	.L138
	salq	$5, %rax
	addq	%rdi, %rax
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L160:
	cmpq	8(%rdi), %rcx
	je	.L156
	cmpq	16(%rdi), %rcx
	je	.L157
	cmpq	24(%rdi), %rcx
	je	.L158
	addq	$32, %rdi
	cmpq	%rax, %rdi
	je	.L159
.L143:
	leaq	8(%rdi), %rsi
	cmpq	(%rdi), %rcx
	jne	.L160
.L139:
	cmpq	%rsi, %rdx
	je	.L148
	subq	%rsi, %rdx
	call	memmove@PLT
	movq	16(%r12), %rsi
.L148:
	movq	(%r12), %rax
	subq	$8, %rsi
	movq	%r12, %rdi
	movq	%rsi, 16(%r12)
	movq	48(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	movq	%rdx, %rsi
	subq	%rdi, %rsi
	sarq	$3, %rsi
.L138:
	cmpq	$2, %rsi
	je	.L149
	cmpq	$3, %rsi
	je	.L145
	cmpq	$1, %rsi
	je	.L146
.L147:
	leaq	8(%rdx), %rsi
	movq	%rdx, %rdi
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L149:
	movq	%rdi, %rsi
.L144:
	leaq	8(%rsi), %rdi
	cmpq	(%rsi), %rcx
	je	.L150
.L146:
	leaq	8(%rdi), %rsi
	cmpq	(%rdi), %rcx
	jne	.L147
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L145:
	leaq	8(%rdi), %rsi
	cmpq	(%rdi), %rcx
	jne	.L144
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L156:
	leaq	16(%rdi), %rax
	movq	%rsi, %rdi
	movq	%rax, %rsi
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L157:
	leaq	24(%rdi), %rsi
	addq	$16, %rdi
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L158:
	leaq	32(%rdi), %rsi
	addq	$24, %rdi
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L150:
	movq	%rdi, %rax
	movq	%rsi, %rdi
	movq	%rax, %rsi
	jmp	.L139
	.cfi_endproc
.LFE22927:
	.size	_ZN2v88internal5Space24RemoveAllocationObserverEPNS0_18AllocationObserverE, .-_ZN2v88internal5Space24RemoveAllocationObserverEPNS0_18AllocationObserverE
	.section	.text._ZN2v84base16LazyInstanceImplINS_8internal20CodeRangeAddressHintENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv,"axG",@progbits,_ZN2v84base16LazyInstanceImplINS_8internal20CodeRangeAddressHintENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv,comdat
	.p2align 4
	.weak	_ZN2v84base16LazyInstanceImplINS_8internal20CodeRangeAddressHintENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv
	.type	_ZN2v84base16LazyInstanceImplINS_8internal20CodeRangeAddressHintENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv, @function
_ZN2v84base16LazyInstanceImplINS_8internal20CodeRangeAddressHintENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv:
.LFB28760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movl	$12, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	rep stosq
	movq	%r12, %rdi
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	88(%r12), %rax
	movq	$1, 48(%r12)
	movq	%rax, 40(%r12)
	movq	$0, 56(%r12)
	movq	$0, 64(%r12)
	movl	$0x3f800000, 72(%r12)
	movq	$0, 80(%r12)
	movq	$0, 88(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28760:
	.size	_ZN2v84base16LazyInstanceImplINS_8internal20CodeRangeAddressHintENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv, .-_ZN2v84base16LazyInstanceImplINS_8internal20CodeRangeAddressHintENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv
	.section	.text._ZN2v88internal10NoFreeListD0Ev,"axG",@progbits,_ZN2v88internal10NoFreeListD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10NoFreeListD0Ev
	.type	_ZN2v88internal10NoFreeListD0Ev, @function
_ZN2v88internal10NoFreeListD0Ev:
.LFB30752:
	.cfi_startproc
	endbr64
	movl	$48, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE30752:
	.size	_ZN2v88internal10NoFreeListD0Ev, .-_ZN2v88internal10NoFreeListD0Ev
	.section	.text._ZN2v84base20BoundedPageAllocatorD0Ev,"axG",@progbits,_ZN2v84base20BoundedPageAllocatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base20BoundedPageAllocatorD0Ev
	.type	_ZN2v84base20BoundedPageAllocatorD0Ev, @function
_ZN2v84base20BoundedPageAllocatorD0Ev:
.LFB27569:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v84base20BoundedPageAllocatorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	72(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -72(%rdi)
	call	_ZN2v84base15RegionAllocatorD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$224, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27569:
	.size	_ZN2v84base20BoundedPageAllocatorD0Ev, .-_ZN2v84base20BoundedPageAllocatorD0Ev
	.section	.text._ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD2Ev,"axG",@progbits,_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD2Ev
	.type	_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD2Ev, @function
_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD2Ev:
.LFB27599:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE27599:
	.size	_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD2Ev, .-_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD2Ev
	.weak	_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD1Ev
	.set	_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD1Ev,_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD2Ev
	.section	.text._ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD0Ev,"axG",@progbits,_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD0Ev
	.type	_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD0Ev, @function
_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD0Ev:
.LFB27601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27601:
	.size	_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD0Ev, .-_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD0Ev
	.section	.rodata._ZN2v88internal10NoFreeList14GetPageForSizeEm.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"NoFreeList can't be used as a standard FreeList."
	.section	.text._ZN2v88internal10NoFreeList14GetPageForSizeEm,"axG",@progbits,_ZN2v88internal10NoFreeList14GetPageForSizeEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10NoFreeList14GetPageForSizeEm
	.type	_ZN2v88internal10NoFreeList14GetPageForSizeEm, @function
_ZN2v88internal10NoFreeList14GetPageForSizeEm:
.LFB7974:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE7974:
	.size	_ZN2v88internal10NoFreeList14GetPageForSizeEm, .-_ZN2v88internal10NoFreeList14GetPageForSizeEm
	.section	.text._ZN2v88internal10NoFreeList4FreeEmmNS0_8FreeModeE,"axG",@progbits,_ZN2v88internal10NoFreeList4FreeEmmNS0_8FreeModeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10NoFreeList4FreeEmmNS0_8FreeModeE
	.type	_ZN2v88internal10NoFreeList4FreeEmmNS0_8FreeModeE, @function
_ZN2v88internal10NoFreeList4FreeEmmNS0_8FreeModeE:
.LFB7972:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE7972:
	.size	_ZN2v88internal10NoFreeList4FreeEmmNS0_8FreeModeE, .-_ZN2v88internal10NoFreeList4FreeEmmNS0_8FreeModeE
	.section	.text._ZN2v88internal10NoFreeList8AllocateEmPmNS0_16AllocationOriginE,"axG",@progbits,_ZN2v88internal10NoFreeList8AllocateEmPmNS0_16AllocationOriginE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10NoFreeList8AllocateEmPmNS0_16AllocationOriginE
	.type	_ZN2v88internal10NoFreeList8AllocateEmPmNS0_16AllocationOriginE, @function
_ZN2v88internal10NoFreeList8AllocateEmPmNS0_16AllocationOriginE:
.LFB7973:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE7973:
	.size	_ZN2v88internal10NoFreeList8AllocateEmPmNS0_16AllocationOriginE, .-_ZN2v88internal10NoFreeList8AllocateEmPmNS0_16AllocationOriginE
	.section	.text._ZN2v88internal10NoFreeList26SelectFreeListCategoryTypeEm,"axG",@progbits,_ZN2v88internal10NoFreeList26SelectFreeListCategoryTypeEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10NoFreeList26SelectFreeListCategoryTypeEm
	.type	_ZN2v88internal10NoFreeList26SelectFreeListCategoryTypeEm, @function
_ZN2v88internal10NoFreeList26SelectFreeListCategoryTypeEm:
.LFB7975:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE7975:
	.size	_ZN2v88internal10NoFreeList26SelectFreeListCategoryTypeEm, .-_ZN2v88internal10NoFreeList26SelectFreeListCategoryTypeEm
	.section	.rodata._ZN2v88internal10NoFreeList21GuaranteedAllocatableEm.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"NoFreeList can't be used as a standard FreeList. "
	.section	.text._ZN2v88internal10NoFreeList21GuaranteedAllocatableEm,"axG",@progbits,_ZN2v88internal10NoFreeList21GuaranteedAllocatableEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10NoFreeList21GuaranteedAllocatableEm
	.type	_ZN2v88internal10NoFreeList21GuaranteedAllocatableEm, @function
_ZN2v88internal10NoFreeList21GuaranteedAllocatableEm:
.LFB7971:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE7971:
	.size	_ZN2v88internal10NoFreeList21GuaranteedAllocatableEm, .-_ZN2v88internal10NoFreeList21GuaranteedAllocatableEm
	.section	.rodata._ZN2v88internal9SemiSpace17GetObjectIteratorEv.str1.1,"aMS",@progbits,1
.LC3:
	.string	"unreachable code"
	.section	.text._ZN2v88internal9SemiSpace17GetObjectIteratorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9SemiSpace17GetObjectIteratorEv
	.type	_ZN2v88internal9SemiSpace17GetObjectIteratorEv, @function
_ZN2v88internal9SemiSpace17GetObjectIteratorEv:
.LFB23010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23010:
	.size	_ZN2v88internal9SemiSpace17GetObjectIteratorEv, .-_ZN2v88internal9SemiSpace17GetObjectIteratorEv
	.section	.text._ZN2v88internal9SemiSpace9AvailableEv,"axG",@progbits,_ZN2v88internal9SemiSpace9AvailableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9SemiSpace9AvailableEv
	.type	_ZN2v88internal9SemiSpace9AvailableEv, @function
_ZN2v88internal9SemiSpace9AvailableEv:
.LFB8228:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8228:
	.size	_ZN2v88internal9SemiSpace9AvailableEv, .-_ZN2v88internal9SemiSpace9AvailableEv
	.section	.text._ZN2v88internal9SemiSpace4SizeEv,"axG",@progbits,_ZN2v88internal9SemiSpace4SizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9SemiSpace4SizeEv
	.type	_ZN2v88internal9SemiSpace4SizeEv, @function
_ZN2v88internal9SemiSpace4SizeEv:
.LFB8226:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8226:
	.size	_ZN2v88internal9SemiSpace4SizeEv, .-_ZN2v88internal9SemiSpace4SizeEv
	.section	.text._ZN2v88internalL28AllocateAndInitializeSlotSetEmm,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28AllocateAndInitializeSlotSetEmm, @function
_ZN2v88internalL28AllocateAndInitializeSlotSetEmm:
.LFB22909:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	262143(%rdi), %r13
	shrq	$18, %r13
	pushq	%r12
	leaq	0(%r13,%r13,2), %r14
	pushq	%rbx
	salq	$7, %r14
	addq	$8, %r14
	movq	%r14, %rdi
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	_Znam@PLT
	leaq	8(%rax), %rsi
	movq	%rax, -64(%rbp)
	addq	%rax, %r14
	movq	%r13, (%rax)
	movq	%rsi, %rbx
	movq	%rsi, -56(%rbp)
	testq	%r13, %r13
	je	.L185
	.p2align 4,,10
	.p2align 3
.L191:
	leaq	264(%rbx), %rdi
	call	_ZN2v84base5MutexC1Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	$0, 304(%rbx)
	movq	$8, 312(%rbx)
	movups	%xmm0, 320(%rbx)
	movups	%xmm0, 336(%rbx)
	movups	%xmm0, 352(%rbx)
	movups	%xmm0, 368(%rbx)
	call	_Znwm@PLT
	movq	312(%rbx), %rdx
	movl	$512, %edi
	movq	%rax, 304(%rbx)
	leaq	-4(,%rdx,4), %rdx
	andq	$-8, %rdx
	leaq	(%rax,%rdx), %r12
	call	_Znwm@PLT
	movq	%r12, 344(%rbx)
	leaq	256(%rbx), %rdx
	leaq	512(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rax, (%r12)
	movq	%rcx, 336(%rbx)
	punpcklqdq	%xmm1, %xmm1
	movq	%r12, 376(%rbx)
	movq	%rcx, 368(%rbx)
	movq	%rax, 360(%rbx)
	movq	%rax, 352(%rbx)
	movq	%rbx, %rax
	movups	%xmm1, 320(%rbx)
	.p2align 4,,10
	.p2align 3
.L189:
	addq	$8, %rax
	movq	$0, -8(%rax)
	cmpq	%rdx, %rax
	jne	.L189
	addq	$384, %rbx
	cmpq	%r14, %rbx
	jne	.L191
	testq	%r13, %r13
	je	.L185
	movq	-64(%rbp), %r12
	salq	$18, %r13
	addq	%r15, %r13
	addq	$264, %r12
	.p2align 4,,10
	.p2align 3
.L192:
	movq	%r15, (%r12)
	addq	$262144, %r15
	addq	$384, %r12
	cmpq	%r15, %r13
	jne	.L192
.L185:
	movq	-56(%rbp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22909:
	.size	_ZN2v88internalL28AllocateAndInitializeSlotSetEmm, .-_ZN2v88internalL28AllocateAndInitializeSlotSetEmm
	.section	.text._ZN2v88internal14FreeListLegacyD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FreeListLegacyD2Ev
	.type	_ZN2v88internal14FreeListLegacyD2Ev, @function
_ZN2v88internal14FreeListLegacyD2Ev:
.LFB23044:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14FreeListLegacyE(%rip), %rax
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L202
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L202:
	ret
	.cfi_endproc
.LFE23044:
	.size	_ZN2v88internal14FreeListLegacyD2Ev, .-_ZN2v88internal14FreeListLegacyD2Ev
	.globl	_ZN2v88internal14FreeListLegacyD1Ev
	.set	_ZN2v88internal14FreeListLegacyD1Ev,_ZN2v88internal14FreeListLegacyD2Ev
	.section	.text._ZN2v88internal14FreeListLegacyD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FreeListLegacyD0Ev
	.type	_ZN2v88internal14FreeListLegacyD0Ev, @function
_ZN2v88internal14FreeListLegacyD0Ev:
.LFB23046:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14FreeListLegacyE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L205
	call	_ZdaPv@PLT
.L205:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23046:
	.size	_ZN2v88internal14FreeListLegacyD0Ev, .-_ZN2v88internal14FreeListLegacyD0Ev
	.section	.text._ZN2v88internal17FreeListFastAllocD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17FreeListFastAllocD2Ev
	.type	_ZN2v88internal17FreeListFastAllocD2Ev, @function
_ZN2v88internal17FreeListFastAllocD2Ev:
.LFB23052:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal17FreeListFastAllocE(%rip), %rax
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L210
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L210:
	ret
	.cfi_endproc
.LFE23052:
	.size	_ZN2v88internal17FreeListFastAllocD2Ev, .-_ZN2v88internal17FreeListFastAllocD2Ev
	.globl	_ZN2v88internal17FreeListFastAllocD1Ev
	.set	_ZN2v88internal17FreeListFastAllocD1Ev,_ZN2v88internal17FreeListFastAllocD2Ev
	.section	.text._ZN2v88internal17FreeListFastAllocD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17FreeListFastAllocD0Ev
	.type	_ZN2v88internal17FreeListFastAllocD0Ev, @function
_ZN2v88internal17FreeListFastAllocD0Ev:
.LFB23054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal17FreeListFastAllocE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L213
	call	_ZdaPv@PLT
.L213:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23054:
	.size	_ZN2v88internal17FreeListFastAllocD0Ev, .-_ZN2v88internal17FreeListFastAllocD0Ev
	.section	.text._ZN2v88internal12FreeListManyD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12FreeListManyD2Ev
	.type	_ZN2v88internal12FreeListManyD2Ev, @function
_ZN2v88internal12FreeListManyD2Ev:
.LFB23060:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal12FreeListManyE(%rip), %rax
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L218
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L218:
	ret
	.cfi_endproc
.LFE23060:
	.size	_ZN2v88internal12FreeListManyD2Ev, .-_ZN2v88internal12FreeListManyD2Ev
	.globl	_ZN2v88internal12FreeListManyD1Ev
	.set	_ZN2v88internal12FreeListManyD1Ev,_ZN2v88internal12FreeListManyD2Ev
	.section	.text._ZN2v88internal12FreeListManyD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12FreeListManyD0Ev
	.type	_ZN2v88internal12FreeListManyD0Ev, @function
_ZN2v88internal12FreeListManyD0Ev:
.LFB23062:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal12FreeListManyE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L221
	call	_ZdaPv@PLT
.L221:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23062:
	.size	_ZN2v88internal12FreeListManyD0Ev, .-_ZN2v88internal12FreeListManyD0Ev
	.section	.text._ZN2v88internal18FreeListManyCachedD2Ev,"axG",@progbits,_ZN2v88internal18FreeListManyCachedD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18FreeListManyCachedD2Ev
	.type	_ZN2v88internal18FreeListManyCachedD2Ev, @function
_ZN2v88internal18FreeListManyCachedD2Ev:
.LFB23025:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal12FreeListManyE(%rip), %rax
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L226
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L226:
	ret
	.cfi_endproc
.LFE23025:
	.size	_ZN2v88internal18FreeListManyCachedD2Ev, .-_ZN2v88internal18FreeListManyCachedD2Ev
	.weak	_ZN2v88internal18FreeListManyCachedD1Ev
	.set	_ZN2v88internal18FreeListManyCachedD1Ev,_ZN2v88internal18FreeListManyCachedD2Ev
	.section	.text._ZN2v88internal18FreeListManyCachedD0Ev,"axG",@progbits,_ZN2v88internal18FreeListManyCachedD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18FreeListManyCachedD0Ev
	.type	_ZN2v88internal18FreeListManyCachedD0Ev, @function
_ZN2v88internal18FreeListManyCachedD0Ev:
.LFB23027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal12FreeListManyE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L229
	call	_ZdaPv@PLT
.L229:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$152, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23027:
	.size	_ZN2v88internal18FreeListManyCachedD0Ev, .-_ZN2v88internal18FreeListManyCachedD0Ev
	.section	.text._ZN2v88internal26FreeListManyCachedFastPathD2Ev,"axG",@progbits,_ZN2v88internal26FreeListManyCachedFastPathD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26FreeListManyCachedFastPathD2Ev
	.type	_ZN2v88internal26FreeListManyCachedFastPathD2Ev, @function
_ZN2v88internal26FreeListManyCachedFastPathD2Ev:
.LFB23032:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal12FreeListManyE(%rip), %rax
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L234
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L234:
	ret
	.cfi_endproc
.LFE23032:
	.size	_ZN2v88internal26FreeListManyCachedFastPathD2Ev, .-_ZN2v88internal26FreeListManyCachedFastPathD2Ev
	.weak	_ZN2v88internal26FreeListManyCachedFastPathD1Ev
	.set	_ZN2v88internal26FreeListManyCachedFastPathD1Ev,_ZN2v88internal26FreeListManyCachedFastPathD2Ev
	.section	.text._ZN2v88internal26FreeListManyCachedFastPathD0Ev,"axG",@progbits,_ZN2v88internal26FreeListManyCachedFastPathD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26FreeListManyCachedFastPathD0Ev
	.type	_ZN2v88internal26FreeListManyCachedFastPathD0Ev, @function
_ZN2v88internal26FreeListManyCachedFastPathD0Ev:
.LFB23034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal12FreeListManyE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L237
	call	_ZdaPv@PLT
.L237:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$152, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23034:
	.size	_ZN2v88internal26FreeListManyCachedFastPathD0Ev, .-_ZN2v88internal26FreeListManyCachedFastPathD0Ev
	.section	.text._ZN2v88internal24FreeListManyCachedOriginD2Ev,"axG",@progbits,_ZN2v88internal24FreeListManyCachedOriginD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24FreeListManyCachedOriginD2Ev
	.type	_ZN2v88internal24FreeListManyCachedOriginD2Ev, @function
_ZN2v88internal24FreeListManyCachedOriginD2Ev:
.LFB30710:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal12FreeListManyE(%rip), %rax
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L242
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L242:
	ret
	.cfi_endproc
.LFE30710:
	.size	_ZN2v88internal24FreeListManyCachedOriginD2Ev, .-_ZN2v88internal24FreeListManyCachedOriginD2Ev
	.weak	_ZN2v88internal24FreeListManyCachedOriginD1Ev
	.set	_ZN2v88internal24FreeListManyCachedOriginD1Ev,_ZN2v88internal24FreeListManyCachedOriginD2Ev
	.section	.text._ZN2v88internal24FreeListManyCachedOriginD0Ev,"axG",@progbits,_ZN2v88internal24FreeListManyCachedOriginD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24FreeListManyCachedOriginD0Ev
	.type	_ZN2v88internal24FreeListManyCachedOriginD0Ev, @function
_ZN2v88internal24FreeListManyCachedOriginD0Ev:
.LFB30712:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal12FreeListManyE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L245
	call	_ZdaPv@PLT
.L245:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$152, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE30712:
	.size	_ZN2v88internal24FreeListManyCachedOriginD0Ev, .-_ZN2v88internal24FreeListManyCachedOriginD0Ev
	.section	.text._ZN2v88internal11FreeListMapD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11FreeListMapD2Ev
	.type	_ZN2v88internal11FreeListMapD2Ev, @function
_ZN2v88internal11FreeListMapD2Ev:
.LFB23082:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal11FreeListMapE(%rip), %rax
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L250
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L250:
	ret
	.cfi_endproc
.LFE23082:
	.size	_ZN2v88internal11FreeListMapD2Ev, .-_ZN2v88internal11FreeListMapD2Ev
	.globl	_ZN2v88internal11FreeListMapD1Ev
	.set	_ZN2v88internal11FreeListMapD1Ev,_ZN2v88internal11FreeListMapD2Ev
	.section	.text._ZN2v88internal11FreeListMapD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11FreeListMapD0Ev
	.type	_ZN2v88internal11FreeListMapD0Ev, @function
_ZN2v88internal11FreeListMapD0Ev:
.LFB23084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal11FreeListMapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L253
	call	_ZdaPv@PLT
.L253:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23084:
	.size	_ZN2v88internal11FreeListMapD0Ev, .-_ZN2v88internal11FreeListMapD0Ev
	.section	.text._ZN2v88internal10PagedSpace17GetObjectIteratorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace17GetObjectIteratorEv
	.type	_ZN2v88internal10PagedSpace17GetObjectIteratorEv, @function
_ZN2v88internal10PagedSpace17GetObjectIteratorEv:
.LFB22962:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$56, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZN2v88internal8MallocednwEm@PLT
	movq	32(%rbx), %rdx
	leaq	16+_ZTVN2v88internal24PagedSpaceObjectIteratorE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movq	%rcx, (%rax)
	movq	%rbx, 24(%rax)
	movq	%rdx, 32(%rax)
	movq	$0, 40(%rax)
	movq	%rdx, 48(%rax)
	movups	%xmm0, 8(%rax)
	popq	%rbx
	movq	%rax, (%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22962:
	.size	_ZN2v88internal10PagedSpace17GetObjectIteratorEv, .-_ZN2v88internal10PagedSpace17GetObjectIteratorEv
	.section	.text._ZN2v88internal8NewSpace17GetObjectIteratorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8NewSpace17GetObjectIteratorEv
	.type	_ZN2v88internal8NewSpace17GetObjectIteratorEv, @function
_ZN2v88internal8NewSpace17GetObjectIteratorEv:
.LFB22995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$24, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZN2v88internal8MallocednwEm@PLT
	movq	240(%rbx), %rdx
	leaq	16+_ZTVN2v88internal23SemiSpaceObjectIteratorE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	40(%rdx), %xmm0
	movhps	104(%rbx), %xmm0
	movups	%xmm0, 8(%rax)
	popq	%rbx
	movq	%rax, (%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22995:
	.size	_ZN2v88internal8NewSpace17GetObjectIteratorEv, .-_ZN2v88internal8NewSpace17GetObjectIteratorEv
	.section	.text._ZN2v88internal16LargeObjectSpace17GetObjectIteratorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16LargeObjectSpace17GetObjectIteratorEv
	.type	_ZN2v88internal16LargeObjectSpace17GetObjectIteratorEv, @function
_ZN2v88internal16LargeObjectSpace17GetObjectIteratorEv:
.LFB23154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$16, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZN2v88internal8MallocednwEm@PLT
	movq	32(%rbx), %rdx
	leaq	16+_ZTVN2v88internal30LargeObjectSpaceObjectIteratorE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	%rdx, 8(%rax)
	popq	%rbx
	movq	%rax, (%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23154:
	.size	_ZN2v88internal16LargeObjectSpace17GetObjectIteratorEv, .-_ZN2v88internal16LargeObjectSpace17GetObjectIteratorEv
	.section	.text._ZN2v88internal14FreeListLegacy21GuaranteedAllocatableEm,"axG",@progbits,_ZN2v88internal14FreeListLegacy21GuaranteedAllocatableEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14FreeListLegacy21GuaranteedAllocatableEm
	.type	_ZN2v88internal14FreeListLegacy21GuaranteedAllocatableEm, @function
_ZN2v88internal14FreeListLegacy21GuaranteedAllocatableEm:
.LFB8145:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$80, %rsi
	jbe	.L264
	movl	$80, %eax
	cmpq	$248, %rsi
	jbe	.L264
	movl	$248, %eax
	cmpq	$2040, %rsi
	jbe	.L264
	movl	$2040, %eax
	cmpq	$16376, %rsi
	jbe	.L264
	cmpq	$65528, %rsi
	movl	$16376, %eax
	cmova	%rsi, %rax
.L264:
	ret
	.cfi_endproc
.LFE8145:
	.size	_ZN2v88internal14FreeListLegacy21GuaranteedAllocatableEm, .-_ZN2v88internal14FreeListLegacy21GuaranteedAllocatableEm
	.section	.text._ZN2v88internal14FreeListLegacy26SelectFreeListCategoryTypeEm,"axG",@progbits,_ZN2v88internal14FreeListLegacy26SelectFreeListCategoryTypeEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14FreeListLegacy26SelectFreeListCategoryTypeEm
	.type	_ZN2v88internal14FreeListLegacy26SelectFreeListCategoryTypeEm, @function
_ZN2v88internal14FreeListLegacy26SelectFreeListCategoryTypeEm:
.LFB8146:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$80, %rsi
	jbe	.L271
	movl	$1, %eax
	cmpq	$248, %rsi
	jbe	.L271
	movl	$2, %eax
	cmpq	$2040, %rsi
	jbe	.L271
	movl	$3, %eax
	cmpq	$16376, %rsi
	jbe	.L271
	xorl	%eax, %eax
	cmpq	$65528, %rsi
	seta	%al
	addl	$4, %eax
.L271:
	ret
	.cfi_endproc
.LFE8146:
	.size	_ZN2v88internal14FreeListLegacy26SelectFreeListCategoryTypeEm, .-_ZN2v88internal14FreeListLegacy26SelectFreeListCategoryTypeEm
	.section	.text._ZN2v88internal12FreeListMany21GuaranteedAllocatableEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12FreeListMany21GuaranteedAllocatableEm
	.type	_ZN2v88internal12FreeListMany21GuaranteedAllocatableEm, @function
_ZN2v88internal12FreeListMany21GuaranteedAllocatableEm:
.LFB23063:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$23, %rsi
	jbe	.L278
	movl	12(%rdi), %edi
	testl	%edi, %edi
	jle	.L283
	movl	$1, %eax
	leaq	_ZN2v88internal12FreeListMany14categories_minE(%rip), %r8
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L280:
	addq	$1, %rax
	cmpl	%eax, %edi
	jl	.L283
.L281:
	movl	(%r8,%rax,4), %edx
	movl	%eax, %ecx
	cmpq	%rdx, %rsi
	jnb	.L280
	subl	$1, %ecx
	movslq	%ecx, %rcx
	movl	(%r8,%rcx,4), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	movq	%rsi, %rax
.L278:
	ret
	.cfi_endproc
.LFE23063:
	.size	_ZN2v88internal12FreeListMany21GuaranteedAllocatableEm, .-_ZN2v88internal12FreeListMany21GuaranteedAllocatableEm
	.section	.text._ZN2v88internal10PagedSpace24SupportsInlineAllocationEv,"axG",@progbits,_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv
	.type	_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv, @function
_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv:
.LFB8197:
	.cfi_startproc
	endbr64
	cmpl	$2, 72(%rdi)
	je	.L293
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*152(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	xorl	$1, %eax
	ret
	.cfi_endproc
.LFE8197:
	.size	_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv, .-_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv
	.section	.text._ZN2v88internal19SpaceWithLinearArea12ComputeLimitEmmm.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal19SpaceWithLinearArea12ComputeLimitEmmm.part.0, @function
_ZN2v88internal19SpaceWithLinearArea12ComputeLimitEmmm.part.0:
.LFB31019:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	%rsi, %rbx
	call	*128(%rax)
	testb	%al, %al
	je	.L295
	cmpb	$0, 56(%r12)
	jne	.L295
	movq	16(%r12), %rcx
	movq	8(%r12), %rax
	cmpq	%rax, %rcx
	je	.L295
	xorl	%esi, %esi
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L307:
	cmpq	%rdx, %rsi
	cmovg	%rdx, %rsi
	addq	$8, %rax
	cmpq	%rax, %rcx
	je	.L306
.L297:
	movq	(%rax), %rdx
	movq	16(%rdx), %rdx
	testq	%rsi, %rsi
	jne	.L307
	addq	$8, %rax
	movq	%rdx, %rsi
	cmpq	%rax, %rcx
	jne	.L297
.L306:
	cmpl	$1, 72(%r12)
	leaq	-1(%rsi), %rax
	je	.L299
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*104(%rax)
	cltq
.L299:
	leaq	(%rbx,%r13), %rsi
	addq	%rax, %rsi
	cmpq	%rsi, %r14
	cmova	%rsi, %r14
.L295:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE31019:
	.size	_ZN2v88internal19SpaceWithLinearArea12ComputeLimitEmmm.part.0, .-_ZN2v88internal19SpaceWithLinearArea12ComputeLimitEmmm.part.0
	.section	.text._ZN2v88internal19SpaceWithLinearArea29StartNextInlineAllocationStepEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SpaceWithLinearArea29StartNextInlineAllocationStepEv
	.type	_ZN2v88internal19SpaceWithLinearArea29StartNextInlineAllocationStepEv, @function
_ZN2v88internal19SpaceWithLinearArea29StartNextInlineAllocationStepEv:
.LFB22989:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	cmpb	$0, 432(%rax)
	jne	.L308
	cmpb	$0, 56(%rdi)
	jne	.L308
	movq	8(%rdi), %rax
	cmpq	%rax, 16(%rdi)
	je	.L308
	movq	104(%rdi), %rax
	xorl	%esi, %esi
	movq	%rax, 120(%rdi)
	movq	(%rdi), %rax
	movq	136(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L308:
	ret
	.cfi_endproc
.LFE22989:
	.size	_ZN2v88internal19SpaceWithLinearArea29StartNextInlineAllocationStepEv, .-_ZN2v88internal19SpaceWithLinearArea29StartNextInlineAllocationStepEv
	.section	.text._ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairImPN2v88internal4PageEESt6vectorIS7_SaIS7_EEEElS7_NS0_5__ops15_Iter_comp_iterIZNS4_8MapSpace12SortFreeListEvEUlRKS7_SH_E_EEEvT_T0_SL_T1_T2_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairImPN2v88internal4PageEESt6vectorIS7_SaIS7_EEEElS7_NS0_5__ops15_Iter_comp_iterIZNS4_8MapSpace12SortFreeListEvEUlRKS7_SH_E_EEEvT_T0_SL_T1_T2_.isra.0, @function
_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairImPN2v88internal4PageEESt6vectorIS7_SaIS7_EEEElS7_NS0_5__ops15_Iter_comp_iterIZNS4_8MapSpace12SortFreeListEvEUlRKS7_SH_E_EEEvT_T0_SL_T1_T2_.isra.0:
.LFB31185:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdx), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rax, %r12
	andl	$1, %r13d
	shrq	$63, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	addq	%rax, %r12
	sarq	%r12
	cmpq	%r12, %rsi
	jge	.L311
	movq	%rsi, %r9
	.p2align 4,,10
	.p2align 3
.L315:
	leaq	1(%r9), %rax
	salq	$4, %r9
	leaq	(%rax,%rax), %r10
	salq	$5, %rax
	addq	%rdi, %r9
	leaq	-1(%r10), %rbx
	addq	%rdi, %rax
	movq	%rbx, %r11
	movq	(%rax), %r15
	salq	$4, %r11
	addq	%rdi, %r11
	movq	(%r11), %r14
	cmpq	%r15, %r14
	ja	.L312
	movq	%r15, (%r9)
	movq	8(%rax), %r11
	movq	%r11, 8(%r9)
	cmpq	%r10, %r12
	jle	.L313
	movq	%r10, %r9
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L312:
	movq	%r14, (%r9)
	movq	8(%r11), %rax
	movq	%rax, 8(%r9)
	cmpq	%rbx, %r12
	jle	.L320
	movq	%rbx, %r9
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L320:
	movq	%r11, %rax
	movq	%rbx, %r10
.L313:
	testq	%r13, %r13
	je	.L319
.L316:
	leaq	-1(%r10), %rdx
	movq	%rdx, %r9
	shrq	$63, %r9
	addq	%rdx, %r9
	sarq	%r9
	cmpq	%rsi, %r10
	jg	.L318
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L326:
	movq	%r11, (%rax)
	movq	8(%rdx), %r10
	movq	%r10, 8(%rax)
	leaq	-1(%r9), %r10
	movq	%r10, %rax
	shrq	$63, %rax
	addq	%r10, %rax
	movq	%r9, %r10
	sarq	%rax
	cmpq	%r9, %rsi
	jge	.L325
	movq	%rax, %r9
.L318:
	movq	%r9, %rdx
	salq	$4, %r10
	salq	$4, %rdx
	leaq	(%rdi,%r10), %rax
	addq	%rdi, %rdx
	movq	(%rdx), %r11
	cmpq	%rcx, %r11
	jb	.L326
.L317:
	movq	%rcx, (%rax)
	movq	%r8, 8(%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L311:
	.cfi_restore_state
	movq	%rsi, %rax
	salq	$4, %rax
	addq	%rdi, %rax
	testq	%r13, %r13
	jne	.L317
	movq	%rsi, %r10
	.p2align 4,,10
	.p2align 3
.L319:
	leaq	-2(%rdx), %r9
	movq	%r9, %rdx
	shrq	$63, %rdx
	addq	%r9, %rdx
	sarq	%rdx
	cmpq	%r10, %rdx
	jne	.L316
	leaq	1(%r10,%r10), %r10
	movq	%r10, %rdx
	salq	$4, %rdx
	addq	%rdi, %rdx
	movq	(%rdx), %r9
	movq	%r9, (%rax)
	movq	8(%rdx), %r9
	movq	%r9, 8(%rax)
	movq	%rdx, %rax
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L325:
	movq	%rdx, %rax
	jmp	.L317
	.cfi_endproc
.LFE31185:
	.size	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairImPN2v88internal4PageEESt6vectorIS7_SaIS7_EEEElS7_NS0_5__ops15_Iter_comp_iterIZNS4_8MapSpace12SortFreeListEvEUlRKS7_SH_E_EEEvT_T0_SL_T1_T2_.isra.0, .-_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairImPN2v88internal4PageEESt6vectorIS7_SaIS7_EEEElS7_NS0_5__ops15_Iter_comp_iterIZNS4_8MapSpace12SortFreeListEvEUlRKS7_SH_E_EEEvT_T0_SL_T1_T2_.isra.0
	.section	.text._ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPSt4pairImPN2v88internal4PageEESt6vectorIS7_SaIS7_EEEENS0_5__ops15_Iter_comp_iterIZNS4_8MapSpace12SortFreeListEvEUlRKS7_SH_E_EEEvT_SK_T0_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPSt4pairImPN2v88internal4PageEESt6vectorIS7_SaIS7_EEEENS0_5__ops15_Iter_comp_iterIZNS4_8MapSpace12SortFreeListEvEUlRKS7_SH_E_EEEvT_SK_T0_.isra.0, @function
_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPSt4pairImPN2v88internal4PageEESt6vectorIS7_SaIS7_EEEENS0_5__ops15_Iter_comp_iterIZNS4_8MapSpace12SortFreeListEvEUlRKS7_SH_E_EEEvT_SK_T0_.isra.0:
.LFB31331:
	.cfi_startproc
	cmpq	%rsi, %rdi
	je	.L327
	leaq	16(%rdi), %r9
	.p2align 4,,10
	.p2align 3
.L343:
	cmpq	%r9, %rsi
	je	.L327
.L344:
	movq	(%r9), %r8
	movq	%r9, %rax
	addq	$16, %r9
	movq	-8(%r9), %r10
	cmpq	(%rdi), %r8
	jnb	.L330
	movq	%rax, %rcx
	subq	%rdi, %rcx
	movq	%rcx, %rdx
	sarq	$4, %rdx
	testq	%rcx, %rcx
	jle	.L333
	.p2align 4,,10
	.p2align 3
.L331:
	movq	-16(%rax), %rcx
	subq	$16, %rax
	movq	%rcx, 16(%rax)
	movq	8(%rax), %rcx
	movq	%rcx, 24(%rax)
	subq	$1, %rdx
	jne	.L331
.L333:
	movq	%r8, (%rdi)
	movq	%r10, 8(%rdi)
	cmpq	%r9, %rsi
	jne	.L344
.L327:
	ret
	.p2align 4,,10
	.p2align 3
.L330:
	movq	-32(%r9), %rcx
	leaq	-32(%r9), %rdx
	cmpq	%rcx, %r8
	jnb	.L334
	.p2align 4,,10
	.p2align 3
.L335:
	movq	8(%rdx), %rax
	movq	%rcx, 16(%rdx)
	movq	%rax, 24(%rdx)
	movq	%rdx, %rax
	movq	-16(%rdx), %rcx
	subq	$16, %rdx
	cmpq	%rcx, %r8
	jb	.L335
.L334:
	movq	%r8, (%rax)
	movq	%r10, 8(%rax)
	jmp	.L343
	.cfi_endproc
.LFE31331:
	.size	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPSt4pairImPN2v88internal4PageEESt6vectorIS7_SaIS7_EEEENS0_5__ops15_Iter_comp_iterIZNS4_8MapSpace12SortFreeListEvEUlRKS7_SH_E_EEEvT_SK_T0_.isra.0, .-_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPSt4pairImPN2v88internal4PageEESt6vectorIS7_SaIS7_EEEENS0_5__ops15_Iter_comp_iterIZNS4_8MapSpace12SortFreeListEvEUlRKS7_SH_E_EEEvT_SK_T0_.isra.0
	.section	.rodata._ZN2v88internal10PagedSpace13SizeOfObjectsEv.str1.1,"aMS",@progbits,1
.LC4:
	.string	"limit() >= top()"
.LC5:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal10PagedSpace13SizeOfObjectsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace13SizeOfObjectsEv
	.type	_ZN2v88internal10PagedSpace13SizeOfObjectsEv, @function
_ZN2v88internal10PagedSpace13SizeOfObjectsEv:
.LFB23099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	104(%rdi), %rcx
	movq	112(%rdi), %rdx
	cmpq	%rdx, %rcx
	ja	.L350
	movq	(%rdi), %rax
	leaq	_ZN2v88internal10PagedSpace4SizeEv(%rip), %rsi
	movq	%rdi, %rbx
	movq	72(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L347
	movq	184(%rdi), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addq	%rcx, %rax
	subq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	.cfi_restore_state
	call	*%rax
	movq	104(%rbx), %rcx
	movq	112(%rbx), %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addq	%rcx, %rax
	subq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L350:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23099:
	.size	_ZN2v88internal10PagedSpace13SizeOfObjectsEv, .-_ZN2v88internal10PagedSpace13SizeOfObjectsEv
	.section	.text._ZN2v88internal16LargeObjectSpace23CommittedPhysicalMemoryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16LargeObjectSpace23CommittedPhysicalMemoryEv
	.type	_ZN2v88internal16LargeObjectSpace23CommittedPhysicalMemoryEv, @function
_ZN2v88internal16LargeObjectSpace23CommittedPhysicalMemoryEv:
.LFB23143:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZN2v88internal5Space15CommittedMemoryEv(%rip), %rdx
	movq	56(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L352
	movq	80(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L352:
	jmp	*%rax
	.cfi_endproc
.LFE23143:
	.size	_ZN2v88internal16LargeObjectSpace23CommittedPhysicalMemoryEv, .-_ZN2v88internal16LargeObjectSpace23CommittedPhysicalMemoryEv
	.section	.text._ZN2v88internal19NewLargeObjectSpace9AvailableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19NewLargeObjectSpace9AvailableEv
	.type	_ZN2v88internal19NewLargeObjectSpace9AvailableEv, @function
_ZN2v88internal19NewLargeObjectSpace9AvailableEv:
.LFB23159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal16LargeObjectSpace13SizeOfObjectsEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	136(%rdi), %rbx
	movq	80(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L356
	movq	120(%rdi), %rax
	addq	$8, %rsp
	subq	%rax, %rbx
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
	call	*%rax
	addq	$8, %rsp
	subq	%rax, %rbx
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23159:
	.size	_ZN2v88internal19NewLargeObjectSpace9AvailableEv, .-_ZN2v88internal19NewLargeObjectSpace9AvailableEv
	.section	.text._ZN2v88internal17FreeListFastAlloc14GetPageForSizeEm,"axG",@progbits,_ZN2v88internal17FreeListFastAlloc14GetPageForSizeEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17FreeListFastAlloc14GetPageForSizeEm
	.type	_ZN2v88internal17FreeListFastAlloc14GetPageForSizeEm, @function
_ZN2v88internal17FreeListFastAlloc14GetPageForSizeEm:
.LFB20540:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal17FreeListFastAlloc26SelectFreeListCategoryTypeEm(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L360
	movq	32(%rdi), %rdi
	movq	16(%rdi), %rax
	cmpq	$16376, %rsi
	jbe	.L371
	cmpq	$65528, %rsi
	jbe	.L383
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	testq	%rax, %rax
	je	.L370
.L369:
	movq	8(%rax), %rax
	andq	$-262144, %rax
	sete	%sil
	andl	%esi, %ecx
.L364:
	testb	%cl, %cl
	jne	.L365
	andl	%esi, %edx
.L366:
	testb	%dl, %dl
	je	.L359
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L370
	movq	8(%rax), %rax
	andq	$-262144, %rax
.L359:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L383:
	.cfi_restore_state
	xorl	%edx, %edx
.L361:
	movl	$1, %ecx
	testq	%rax, %rax
	jne	.L369
	.p2align 4,,10
	.p2align 3
.L365:
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L366
	movq	8(%rax), %rax
	andq	$-262144, %rax
	sete	%cl
	andl	%ecx, %edx
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L371:
	movl	$1, %edx
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L360:
	call	*%rax
	movq	32(%rbx), %rdi
	movl	$1, %esi
	movl	%eax, %edx
	cmpl	$1, %edx
	movq	16(%rdi), %rax
	setle	%cl
	testl	%edx, %edx
	setle	%dl
	testq	%rax, %rax
	jne	.L369
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L370:
	xorl	%eax, %eax
	jmp	.L359
	.cfi_endproc
.LFE20540:
	.size	_ZN2v88internal17FreeListFastAlloc14GetPageForSizeEm, .-_ZN2v88internal17FreeListFastAlloc14GetPageForSizeEm
	.section	.text._ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPSt4pairImPN2v88internal4PageEESt6vectorIS7_SaIS7_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_8MapSpace12SortFreeListEvEUlRKS7_SH_E_EEEvT_SK_T0_T1_,"ax",@progbits
	.p2align 4
	.type	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPSt4pairImPN2v88internal4PageEESt6vectorIS7_SaIS7_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_8MapSpace12SortFreeListEvEUlRKS7_SH_E_EEEvT_SK_T0_T1_, @function
_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPSt4pairImPN2v88internal4PageEESt6vectorIS7_SaIS7_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_8MapSpace12SortFreeListEvEUlRKS7_SH_E_EEEvT_SK_T0_T1_:
.LFB28971:
	.cfi_startproc
	movq	%rsi, %rax
	subq	%rdi, %rax
	cmpq	$256, %rax
	jle	.L413
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L386
	leaq	16(%rdi), %r12
.L388:
	movq	%rsi, %rcx
	movq	-16(%rsi), %rdi
	movq	(%rbx), %r8
	subq	$1, %r15
	subq	%rbx, %rcx
	movq	%rcx, %rax
	shrq	$63, %rcx
	sarq	$4, %rax
	addq	%rcx, %rax
	movq	16(%rbx), %rcx
	sarq	%rax
	salq	$4, %rax
	addq	%rbx, %rax
	movq	(%rax), %rdx
	cmpq	%rdx, %rcx
	jnb	.L393
	cmpq	%rdi, %rdx
	jb	.L398
	cmpq	%rdi, %rcx
	jb	.L416
.L417:
	movq	8(%rbx), %rax
	movq	24(%rbx), %rdx
	movq	%rcx, (%rbx)
	movq	%r8, 16(%rbx)
	movq	%rdx, 8(%rbx)
	movq	%rax, 24(%rbx)
.L395:
	movq	-16(%rsi), %rdi
	movq	%r12, %r13
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L399:
	movq	0(%r13), %r8
	movq	%r13, %r14
	cmpq	%rcx, %r8
	jb	.L400
	leaq	-16(%rax), %rdx
	cmpq	%rcx, %rdi
	jbe	.L401
	subq	$32, %rax
	.p2align 4,,10
	.p2align 3
.L402:
	movq	%rax, %rdx
	movq	(%rax), %rdi
	subq	$16, %rax
	cmpq	%rcx, %rdi
	ja	.L402
.L401:
	cmpq	%rdx, %r13
	jnb	.L418
	movq	%rdi, 0(%r13)
	movq	8(%rdx), %rcx
	movq	%r8, (%rdx)
	movq	8(%r13), %rax
	movq	%rcx, 8(%r13)
	movq	-16(%rdx), %rdi
	movq	%rax, 8(%rdx)
	movq	(%rbx), %rcx
	movq	%rdx, %rax
.L400:
	addq	$16, %r13
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L393:
	cmpq	%rdi, %rcx
	jb	.L417
	cmpq	%rdi, %rdx
	jnb	.L398
.L416:
	movq	%rdi, (%rbx)
	movq	-8(%rsi), %rdx
	movq	%r8, -16(%rsi)
	movq	8(%rbx), %rax
	movq	%rdx, 8(%rbx)
	movq	%rax, -8(%rsi)
	movq	(%rbx), %rcx
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L418:
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPSt4pairImPN2v88internal4PageEESt6vectorIS7_SaIS7_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_8MapSpace12SortFreeListEvEUlRKS7_SH_E_EEEvT_SK_T0_T1_
	movq	%r13, %rax
	subq	%rbx, %rax
	cmpq	$256, %rax
	jle	.L384
	testq	%r15, %r15
	je	.L386
	movq	%r13, %rsi
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L398:
	movq	%rdx, (%rbx)
	movq	8(%rax), %rcx
	movq	%r8, (%rax)
	movq	8(%rbx), %rdx
	movq	%rcx, 8(%rbx)
	movq	%rdx, 8(%rax)
	movq	(%rbx), %rcx
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L386:
	sarq	$4, %rax
	leaq	-2(%rax), %rsi
	movq	%rax, %r12
	sarq	%rsi
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L389:
	subq	$1, %rsi
.L391:
	movq	%rsi, %rax
	movq	%r12, %rdx
	movq	%rbx, %rdi
	salq	$4, %rax
	movq	(%rbx,%rax), %rcx
	movq	8(%rbx,%rax), %r8
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairImPN2v88internal4PageEESt6vectorIS7_SaIS7_EEEElS7_NS0_5__ops15_Iter_comp_iterIZNS4_8MapSpace12SortFreeListEvEUlRKS7_SH_E_EEEvT_T0_SL_T1_T2_.isra.0
	testq	%rsi, %rsi
	jne	.L389
	subq	$16, %r14
	.p2align 4,,10
	.p2align 3
.L390:
	movq	(%rbx), %rax
	movq	(%r14), %rcx
	movq	%r14, %r12
	xorl	%esi, %esi
	movq	8(%r14), %r8
	subq	%rbx, %r12
	movq	%rbx, %rdi
	subq	$16, %r14
	movq	%rax, 16(%r14)
	movq	8(%rbx), %rax
	movq	%r12, %rdx
	sarq	$4, %rdx
	movq	%rax, 24(%r14)
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairImPN2v88internal4PageEESt6vectorIS7_SaIS7_EEEElS7_NS0_5__ops15_Iter_comp_iterIZNS4_8MapSpace12SortFreeListEvEUlRKS7_SH_E_EEEvT_T0_SL_T1_T2_.isra.0
	cmpq	$16, %r12
	jg	.L390
.L384:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE28971:
	.size	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPSt4pairImPN2v88internal4PageEESt6vectorIS7_SaIS7_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_8MapSpace12SortFreeListEvEUlRKS7_SH_E_EEEvT_SK_T0_T1_, .-_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPSt4pairImPN2v88internal4PageEESt6vectorIS7_SaIS7_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_8MapSpace12SortFreeListEvEUlRKS7_SH_E_EEEvT_SK_T0_T1_
	.section	.text._ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD2Ev,"axG",@progbits,_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD5Ev,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD1Ev
	.type	_ZThn32_N2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD1Ev, @function
_ZThn32_N2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD1Ev:
.LFB31380:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	subq	$32, %rdi
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE31380:
	.size	_ZThn32_N2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD1Ev, .-_ZThn32_N2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD1Ev
	.section	.text._ZN2v88internal8FreeList14RemoveCategoryEPNS0_16FreeListCategoryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8FreeList14RemoveCategoryEPNS0_16FreeListCategoryE
	.type	_ZN2v88internal8FreeList14RemoveCategoryEPNS0_16FreeListCategoryE, @function
_ZN2v88internal8FreeList14RemoveCategoryEPNS0_16FreeListCategoryE:
.LFB23093:
	.cfi_startproc
	endbr64
	movslq	(%rsi), %rdx
	movq	32(%rdi), %rax
	cmpq	$0, 16(%rsi)
	leaq	(%rax,%rdx,8), %rax
	movq	(%rax), %rdx
	je	.L437
.L421:
	movl	4(%rsi), %ecx
	subq	%rcx, 40(%rdi)
	cmpq	%rdx, %rsi
	je	.L424
.L436:
	movq	16(%rsi), %rdx
	movq	24(%rsi), %rax
	testq	%rdx, %rdx
	je	.L426
	movq	%rax, 24(%rdx)
	movq	24(%rsi), %rax
.L426:
	testq	%rax, %rax
	je	.L422
	movq	16(%rsi), %rdx
	movq	%rdx, 16(%rax)
.L422:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 16(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L437:
	cmpq	$0, 24(%rsi)
	jne	.L421
	cmpq	%rdx, %rsi
	jne	.L422
	movl	4(%rsi), %ecx
	subq	%rcx, 40(%rdi)
	cmpq	%rdx, %rsi
	jne	.L436
	.p2align 4,,10
	.p2align 3
.L424:
	movq	24(%rsi), %rdx
	movq	%rdx, (%rax)
	jmp	.L436
	.cfi_endproc
.LFE23093:
	.size	_ZN2v88internal8FreeList14RemoveCategoryEPNS0_16FreeListCategoryE, .-_ZN2v88internal8FreeList14RemoveCategoryEPNS0_16FreeListCategoryE
	.section	.text._ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD0Ev,"axG",@progbits,_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD5Ev,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD0Ev
	.type	_ZThn32_N2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD0Ev, @function
_ZThn32_N2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD0Ev:
.LFB31381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-32(%rdi), %r12
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	movq	%r12, %rdi
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE31381:
	.size	_ZThn32_N2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD0Ev, .-_ZThn32_N2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD0Ev
	.section	.text._ZN2v88internal14FreeListLegacy14GetPageForSizeEm,"axG",@progbits,_ZN2v88internal14FreeListLegacy14GetPageForSizeEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14FreeListLegacy14GetPageForSizeEm
	.type	_ZN2v88internal14FreeListLegacy14GetPageForSizeEm, @function
_ZN2v88internal14FreeListLegacy14GetPageForSizeEm:
.LFB20539:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal14FreeListLegacy26SelectFreeListCategoryTypeEm(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L441
	movq	32(%rdi), %rcx
	movq	40(%rcx), %rax
	cmpq	$80, %rsi
	jbe	.L457
	cmpq	$248, %rsi
	jbe	.L458
	cmpq	$2040, %rsi
	jbe	.L459
	cmpq	$16376, %rsi
	jbe	.L460
	cmpq	$65528, %rsi
	jbe	.L482
	xorl	%edi, %edi
	xorl	%esi, %esi
	movl	$1, %edx
	testq	%rax, %rax
	je	.L480
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%r10d, %r10d
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L457:
	movl	$1, %edi
	movl	$1, %esi
	movl	$1, %r9d
	movl	$1, %r8d
.L442:
	movl	$1, %r10d
	testq	%rax, %rax
	je	.L446
.L455:
	movq	8(%rax), %rax
	andq	$-262144, %rax
	sete	%dl
	andl	%edx, %r10d
.L445:
	testb	%r10b, %r10b
	je	.L478
.L446:
	movq	32(%rcx), %rax
	testq	%rax, %rax
	je	.L462
	movq	8(%rax), %rax
	andq	$-262144, %rax
	sete	%dl
.L478:
	andl	%edx, %r8d
.L447:
	testb	%r8b, %r8b
	je	.L479
	movq	24(%rcx), %rax
	testq	%rax, %rax
	je	.L463
	movq	8(%rax), %rax
	andq	$-262144, %rax
	sete	%dl
.L479:
	andl	%edx, %r9d
.L449:
	testb	%r9b, %r9b
	je	.L480
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L464
	movq	8(%rax), %rax
	andq	$-262144, %rax
	sete	%dl
.L480:
	andl	%edx, %esi
.L451:
	testb	%sil, %sil
	je	.L481
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L453
	movq	8(%rax), %rax
	andq	$-262144, %rax
	sete	%dl
.L481:
	andl	%edx, %edi
.L453:
	testb	%dil, %dil
	je	.L440
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L440
	movq	8(%rax), %rax
	andq	$-262144, %rax
.L440:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L441:
	.cfi_restore_state
	call	*%rax
	movq	32(%rbx), %rcx
	movl	%eax, %edx
	cmpl	$4, %edx
	movq	40(%rcx), %rax
	setle	%r10b
	cmpl	$3, %edx
	setle	%r8b
	cmpl	$2, %edx
	setle	%r9b
	cmpl	$1, %edx
	setle	%sil
	testl	%edx, %edx
	movl	$1, %edx
	setle	%dil
	testq	%rax, %rax
	jne	.L455
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L458:
	xorl	%edi, %edi
	movl	$1, %esi
	movl	$1, %r9d
	movl	$1, %r8d
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L482:
	xorl	%edi, %edi
	xorl	%esi, %esi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L459:
	xorl	%edi, %edi
	xorl	%esi, %esi
	movl	$1, %r9d
	movl	$1, %r8d
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L460:
	xorl	%edi, %edi
	xorl	%esi, %esi
	xorl	%r9d, %r9d
	movl	$1, %r8d
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L464:
	movl	%r9d, %edx
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L462:
	movl	$1, %edx
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L463:
	movl	%r8d, %edx
	jmp	.L449
	.cfi_endproc
.LFE20539:
	.size	_ZN2v88internal14FreeListLegacy14GetPageForSizeEm, .-_ZN2v88internal14FreeListLegacy14GetPageForSizeEm
	.section	.text._ZN2v88internal8NewSpace13SizeOfObjectsEv,"axG",@progbits,_ZN2v88internal8NewSpace13SizeOfObjectsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8NewSpace13SizeOfObjectsEv
	.type	_ZN2v88internal8NewSpace13SizeOfObjectsEv, @function
_ZN2v88internal8NewSpace13SizeOfObjectsEv:
.LFB8240:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZN2v88internal8NewSpace4SizeEv(%rip), %rdx
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L484
	movslq	360(%rdi), %rax
	movq	352(%rdi), %rdx
	imulq	$261864, %rax, %rax
	subq	40(%rdx), %rax
	addq	104(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L484:
	jmp	*%rax
	.cfi_endproc
.LFE8240:
	.size	_ZN2v88internal8NewSpace13SizeOfObjectsEv, .-_ZN2v88internal8NewSpace13SizeOfObjectsEv
	.section	.text._ZN2v88internal8NewSpace9AvailableEv,"axG",@progbits,_ZN2v88internal8NewSpace9AvailableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8NewSpace9AvailableEv
	.type	_ZN2v88internal8NewSpace9AvailableEv, @function
_ZN2v88internal8NewSpace9AvailableEv:
.LFB8245:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal8NewSpace4SizeEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	312(%rdi), %rbx
	movq	72(%rax), %rax
	shrq	$18, %rbx
	imulq	$261864, %rbx, %rbx
	cmpq	%rdx, %rax
	jne	.L488
	movslq	360(%rdi), %rax
	movq	352(%rdi), %rdx
	imulq	$261864, %rax, %rax
	subq	40(%rdx), %rax
	addq	104(%rdi), %rax
	addq	$8, %rsp
	subq	%rax, %rbx
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L488:
	.cfi_restore_state
	call	*%rax
	addq	$8, %rsp
	subq	%rax, %rbx
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8245:
	.size	_ZN2v88internal8NewSpace9AvailableEv, .-_ZN2v88internal8NewSpace9AvailableEv
	.section	.text._ZN2v88internal8FreeList5ResetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8FreeList5ResetEv
	.type	_ZN2v88internal8FreeList5ResetEv, @function
_ZN2v88internal8FreeList5ResetEv:
.LFB23086:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L501
	xorl	%ecx, %ecx
	pxor	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L499:
	movq	32(%rdi), %rdx
	movq	(%rdx,%rcx,8), %rdx
	testq	%rdx, %rdx
	jne	.L498
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L494:
	cmpq	$0, 8(%rax)
	je	.L497
.L502:
	movl	4(%rax), %esi
	subq	%rsi, 40(%rdi)
.L497:
	movq	$0, 8(%rax)
	movl	$0, 4(%rax)
	movups	%xmm0, 16(%rax)
	testq	%rdx, %rdx
	je	.L513
.L498:
	movq	%rdx, %rax
	movq	24(%rdx), %rdx
	movq	%rdx, %rsi
	orq	16(%rax), %rsi
	jne	.L494
	movslq	(%rax), %r8
	movq	32(%rdi), %rsi
	cmpq	(%rsi,%r8,8), %rax
	je	.L514
	movq	$0, 8(%rax)
	movl	$0, 4(%rax)
	movups	%xmm0, 16(%rax)
	movl	8(%rdi), %eax
.L493:
	addq	$1, %rcx
	cmpl	%ecx, %eax
	jg	.L499
	testl	%eax, %eax
	jle	.L501
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L500:
	movq	32(%rdi), %rdx
	movq	$0, (%rdx,%rax,8)
	addq	$1, %rax
	cmpl	%eax, 8(%rdi)
	jg	.L500
.L501:
	movq	$0, 24(%rdi)
	mfence
	movq	$0, 40(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L513:
	movl	8(%rdi), %eax
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L514:
	cmpq	$0, 8(%rax)
	jne	.L502
	movl	$0, 4(%rax)
	movups	%xmm0, 16(%rax)
	movl	8(%rdi), %eax
	jmp	.L493
	.cfi_endproc
.LFE23086:
	.size	_ZN2v88internal8FreeList5ResetEv, .-_ZN2v88internal8FreeList5ResetEv
	.section	.text._ZN2v88internal18FreeListManyCached5ResetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18FreeListManyCached5ResetEv
	.type	_ZN2v88internal18FreeListManyCached5ResetEv, @function
_ZN2v88internal18FreeListManyCached5ResetEv:
.LFB23069:
	.cfi_startproc
	endbr64
	movdqa	.LC6(%rip), %xmm0
	movl	$24, 144(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	movups	%xmm0, 80(%rdi)
	movups	%xmm0, 96(%rdi)
	movups	%xmm0, 112(%rdi)
	movups	%xmm0, 128(%rdi)
	jmp	_ZN2v88internal8FreeList5ResetEv
	.cfi_endproc
.LFE23069:
	.size	_ZN2v88internal18FreeListManyCached5ResetEv, .-_ZN2v88internal18FreeListManyCached5ResetEv
	.section	.text._ZN2v88internal23SemiSpaceObjectIterator4NextEv,"axG",@progbits,_ZN2v88internal23SemiSpaceObjectIterator4NextEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23SemiSpaceObjectIterator4NextEv
	.type	_ZN2v88internal23SemiSpaceObjectIterator4NextEv, @function
_ZN2v88internal23SemiSpaceObjectIterator4NextEv:
.LFB20501:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	cmpq	%rdx, %rax
	je	.L517
	movq	%rdi, %rbx
	leaq	-32(%rbp), %r12
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L520:
	movq	8(%rbx), %rax
	movq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L517
.L521:
	testl	$262143, %eax
	jne	.L518
	subq	$8, %rax
	andq	$-262144, %rax
	movq	224(%rax), %rax
	movq	40(%rax), %rax
	movq	%rax, 8(%rbx)
	cmpq	%rax, %rdx
	je	.L517
.L518:
	leaq	1(%rax), %rdx
	movq	%r12, %rdi
	movq	%rdx, -32(%rbp)
	movq	(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, 8(%rbx)
	movq	-32(%rbp), %rax
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	cmpw	$76, %dx
	je	.L520
	cmpw	$73, %dx
	je	.L520
.L522:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L525
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L517:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L522
.L525:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20501:
	.size	_ZN2v88internal23SemiSpaceObjectIterator4NextEv, .-_ZN2v88internal23SemiSpaceObjectIterator4NextEv
	.section	.text._ZN2v88internal24PagedSpaceObjectIterator4NextEv,"axG",@progbits,_ZN2v88internal24PagedSpaceObjectIterator4NextEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24PagedSpaceObjectIterator4NextEv
	.type	_ZN2v88internal24PagedSpaceObjectIterator4NextEv, @function
_ZN2v88internal24PagedSpaceObjectIterator4NextEv:
.LFB20502:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-48(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	.p2align 4,,10
	.p2align 3
.L538:
	cmpq	%rax, %rdx
	jne	.L528
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L545:
	movq	112(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L529
	movq	%rdx, 8(%rbx)
	movq	%rdx, %rax
	cmpq	%rax, 16(%rbx)
	je	.L533
.L528:
	movq	24(%rbx), %rdx
	cmpq	%rax, 104(%rdx)
	je	.L545
.L529:
	leaq	1(%rax), %rdx
	movq	%r13, %rdi
	movq	%rdx, -48(%rbp)
	movq	(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, 8(%rbx)
	movq	-48(%rbp), %rax
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	cmpw	$73, %dx
	je	.L531
	cmpw	$76, %dx
	jne	.L546
.L531:
	movq	8(%rbx), %rax
	cmpq	%rax, 16(%rbx)
	jne	.L528
.L533:
	movq	48(%rbx), %r12
	cmpq	%r12, 40(%rbx)
	je	.L547
	movq	224(%r12), %rax
	movq	%r12, %rsi
	movq	%rax, 48(%rbx)
	movq	24(%rbx), %rax
	movq	64(%rax), %r14
	movq	2016(%r14), %rax
	movq	9984(%rax), %rdi
	call	_ZN2v88internal7Sweeper20EnsurePageIsIterableEPNS0_4PageE@PLT
	testb	$2, 10(%r12)
	jne	.L548
.L537:
	movq	40(%r12), %rax
	movq	%rax, 8(%rbx)
	movq	48(%r12), %rdx
	movq	%rdx, 16(%rbx)
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L546:
	movq	-1(%rax), %rdx
	testq	%rax, %rax
	je	.L533
	.p2align 4,,10
	.p2align 3
.L536:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L549
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L547:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L548:
	movq	2024(%r14), %rdi
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal25MinorMarkCompactCollector12MakeIterableEPNS0_4PageENS0_20MarkingTreatmentModeENS0_22FreeSpaceTreatmentModeE@PLT
	jmp	.L537
.L549:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20502:
	.size	_ZN2v88internal24PagedSpaceObjectIterator4NextEv, .-_ZN2v88internal24PagedSpaceObjectIterator4NextEv
	.section	.text._ZN2v88internal19SpaceWithLinearArea25ResumeAllocationObserversEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SpaceWithLinearArea25ResumeAllocationObserversEv
	.type	_ZN2v88internal19SpaceWithLinearArea25ResumeAllocationObserversEv, @function
_ZN2v88internal19SpaceWithLinearArea25ResumeAllocationObserversEv:
.LFB22993:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movb	$0, 56(%rdi)
	leaq	_ZN2v88internal19SpaceWithLinearArea29StartNextInlineAllocationStepEv(%rip), %rcx
	movq	48(%rdx), %rax
	cmpq	%rcx, %rax
	jne	.L551
	movq	64(%rdi), %rax
	cmpb	$0, 432(%rax)
	jne	.L550
	movq	8(%rdi), %rax
	cmpq	%rax, 16(%rdi)
	je	.L550
	movq	104(%rdi), %rax
	xorl	%esi, %esi
	movq	%rax, 120(%rdi)
	movq	136(%rdx), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L550:
	ret
	.p2align 4,,10
	.p2align 3
.L551:
	jmp	*%rax
	.cfi_endproc
.LFE22993:
	.size	_ZN2v88internal19SpaceWithLinearArea25ResumeAllocationObserversEv, .-_ZN2v88internal19SpaceWithLinearArea25ResumeAllocationObserversEv
	.section	.text._ZN2v88internal12FreeListMany14GetPageForSizeEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12FreeListMany14GetPageForSizeEm
	.type	_ZN2v88internal12FreeListMany14GetPageForSizeEm, @function
_ZN2v88internal12FreeListMany14GetPageForSizeEm:
.LFB23064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal12FreeListMany26SelectFreeListCategoryTypeEm(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L554
	movl	12(%rdi), %r8d
	cmpq	$256, %rsi
	jbe	.L598
	cmpl	$15, %r8d
	jle	.L557
	cmpq	$511, %rsi
	jbe	.L566
	cmpl	$16, %r8d
	je	.L557
	cmpq	$1023, %rsi
	jbe	.L567
	cmpl	$17, %r8d
	je	.L557
	cmpq	$2047, %rsi
	jbe	.L568
	cmpl	$18, %r8d
	je	.L557
	cmpq	$4095, %rsi
	jbe	.L569
	cmpl	$19, %r8d
	je	.L557
	cmpq	$8191, %rsi
	jbe	.L570
	cmpl	$20, %r8d
	je	.L557
	cmpq	$16383, %rsi
	jbe	.L571
	cmpl	$21, %r8d
	je	.L557
	cmpq	$32767, %rsi
	jbe	.L572
	cmpl	$22, %r8d
	je	.L557
	cmpq	$65535, %rsi
	jbe	.L573
.L557:
	leal	1(%r8), %edx
	movl	%r8d, %r9d
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L598:
	cmpq	$31, %rsi
	jbe	.L565
	shrq	$4, %rsi
	movl	%esi, %edx
	leal	-1(%rsi), %r9d
.L556:
	movq	32(%rbx), %rdi
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L560:
	movq	8(%rsi), %rax
	andq	$-262144, %rax
	jne	.L553
.L562:
	cmpl	%edx, %r8d
	jl	.L559
	addl	$1, %edx
	movslq	%edx, %rcx
.L561:
	movq	-8(%rdi,%rcx,8), %rsi
	movl	%ecx, %edx
	testq	%rsi, %rsi
	jne	.L560
	addq	$1, %rcx
	leal	-1(%rcx), %eax
	cmpl	%eax, %r8d
	jge	.L561
	.p2align 4,,10
	.p2align 3
.L559:
	movslq	%r9d, %r9
	movq	(%rdi,%r9,8), %rax
	testq	%rax, %rax
	je	.L553
	movq	8(%rax), %rax
	andq	$-262144, %rax
.L553:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L565:
	.cfi_restore_state
	movl	$1, %edx
	xorl	%r9d, %r9d
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L554:
	call	*%rax
	movl	12(%rbx), %r8d
	movl	%eax, %r9d
	leal	1(%rax), %edx
	jmp	.L556
.L573:
	movl	$23, %edx
	movl	$22, %r9d
	jmp	.L556
.L566:
	movl	$16, %edx
	movl	$15, %r9d
	jmp	.L556
.L567:
	movl	$17, %edx
	movl	$16, %r9d
	jmp	.L556
.L568:
	movl	$18, %edx
	movl	$17, %r9d
	jmp	.L556
.L569:
	movl	$19, %edx
	movl	$18, %r9d
	jmp	.L556
.L570:
	movl	$20, %edx
	movl	$19, %r9d
	jmp	.L556
.L571:
	movl	$21, %edx
	movl	$20, %r9d
	jmp	.L556
.L572:
	movl	$22, %edx
	movl	$21, %r9d
	jmp	.L556
	.cfi_endproc
.LFE23064:
	.size	_ZN2v88internal12FreeListMany14GetPageForSizeEm, .-_ZN2v88internal12FreeListMany14GetPageForSizeEm
	.section	.text._ZN2v88internal9SemiSpace23CommittedPhysicalMemoryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9SemiSpace23CommittedPhysicalMemoryEv
	.type	_ZN2v88internal9SemiSpace23CommittedPhysicalMemoryEv, @function
_ZN2v88internal9SemiSpace23CommittedPhysicalMemoryEv:
.LFB23000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	cmpb	$0, 136(%rdi)
	pushq	%rbx
	.cfi_offset 3, -32
	je	.L599
	movq	32(%rdi), %rbx
	testq	%rbx, %rbx
	jne	.L605
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L601:
	testb	$32, 10(%rbx)
	jne	.L603
	movq	80(%rbx), %rax
	cmpl	$5, 72(%rax)
	je	.L604
.L603:
	movq	152(%rbx), %rax
	addq	%rax, %r12
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L599
.L605:
	call	_ZN2v84base2OS14HasLazyCommitsEv@PLT
	testb	%al, %al
	jne	.L601
.L604:
	movq	(%rbx), %rax
	movq	224(%rbx), %rbx
	addq	%rax, %r12
	testq	%rbx, %rbx
	jne	.L605
.L599:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23000:
	.size	_ZN2v88internal9SemiSpace23CommittedPhysicalMemoryEv, .-_ZN2v88internal9SemiSpace23CommittedPhysicalMemoryEv
	.section	.text._ZN2v88internal10PagedSpace23CommittedPhysicalMemoryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace23CommittedPhysicalMemoryEv
	.type	_ZN2v88internal10PagedSpace23CommittedPhysicalMemoryEv, @function
_ZN2v88internal10PagedSpace23CommittedPhysicalMemoryEv:
.LFB22939:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	call	_ZN2v84base2OS14HasLazyCommitsEv@PLT
	testb	%al, %al
	jne	.L611
	movq	(%r12), %rax
	leaq	_ZN2v88internal5Space15CommittedMemoryEv(%rip), %rdx
	movq	56(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L612
	movq	80(%r12), %r12
.L610:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L611:
	.cfi_restore_state
	movq	104(%r12), %rax
	testq	%rax, %rax
	je	.L614
	leaq	-1(%rax), %rcx
	andq	$-262144, %rcx
	subq	%rcx, %rax
	addq	$152, %rcx
	movq	%rax, %rdx
.L615:
	movq	(%rcx), %rax
	cmpq	%rax, %rdx
	jle	.L614
	lock cmpxchgq	%rdx, (%rcx)
	jne	.L615
.L614:
	movq	32(%r12), %rbx
	xorl	%r12d, %r12d
	testq	%rbx, %rbx
	je	.L610
	call	_ZN2v84base2OS14HasLazyCommitsEv@PLT
	testb	%al, %al
	je	.L619
	.p2align 4,,10
	.p2align 3
.L616:
	testb	$32, 10(%rbx)
	jne	.L618
	movq	80(%rbx), %rax
	cmpl	$5, 72(%rax)
	je	.L619
.L618:
	movq	152(%rbx), %rax
.L617:
	movq	224(%rbx), %rbx
	addq	%rax, %r12
	testq	%rbx, %rbx
	je	.L610
	call	_ZN2v84base2OS14HasLazyCommitsEv@PLT
	testb	%al, %al
	jne	.L616
.L619:
	movq	(%rbx), %rax
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L612:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE22939:
	.size	_ZN2v88internal10PagedSpace23CommittedPhysicalMemoryEv, .-_ZN2v88internal10PagedSpace23CommittedPhysicalMemoryEv
	.section	.text._ZN2v88internal18FreeListManyCached14RemoveCategoryEPNS0_16FreeListCategoryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18FreeListManyCached14RemoveCategoryEPNS0_16FreeListCategoryE
	.type	_ZN2v88internal18FreeListManyCached14RemoveCategoryEPNS0_16FreeListCategoryE, @function
_ZN2v88internal18FreeListManyCached14RemoveCategoryEPNS0_16FreeListCategoryE:
.LFB23071:
	.cfi_startproc
	endbr64
	movslq	(%rsi), %rcx
	movq	32(%rdi), %rax
	cmpq	$0, 16(%rsi)
	leaq	(%rax,%rcx,8), %rax
	movq	%rcx, %rdx
	movq	(%rax), %rcx
	je	.L651
.L630:
	movl	4(%rsi), %edx
	subq	%rdx, 40(%rdi)
	cmpq	%rcx, %rsi
	je	.L633
.L648:
	movq	16(%rsi), %rdx
	movq	24(%rsi), %rax
	testq	%rdx, %rdx
	je	.L635
	movq	%rax, 24(%rdx)
	movq	24(%rsi), %rax
.L635:
	testq	%rax, %rax
	je	.L649
	movq	16(%rsi), %rdx
	movq	%rdx, 16(%rax)
.L649:
	movl	(%rsi), %edx
.L631:
	pxor	%xmm0, %xmm0
	movslq	%edx, %rax
	movups	%xmm0, 16(%rsi)
	movq	32(%rdi), %rcx
	cmpq	$0, (%rcx,%rax,8)
	je	.L652
.L629:
	ret
	.p2align 4,,10
	.p2align 3
.L652:
	testl	%edx, %edx
	js	.L629
	leal	1(%rdx), %esi
	leaq	(%rdi,%rax,4), %rax
	movslq	%esi, %rsi
	leaq	-4(%rdi), %r8
	addq	$12, %rsi
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L653:
	movl	(%rdi,%rsi,4), %ecx
	subq	$4, %rax
	movl	%ecx, 52(%rax)
	cmpq	%r8, %rax
	je	.L629
.L638:
	cmpl	48(%rax), %edx
	je	.L653
	ret
	.p2align 4,,10
	.p2align 3
.L633:
	movq	24(%rsi), %rdx
	movq	%rdx, (%rax)
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L651:
	cmpq	$0, 24(%rsi)
	jne	.L630
	cmpq	%rcx, %rsi
	je	.L630
	jmp	.L631
	.cfi_endproc
.LFE23071:
	.size	_ZN2v88internal18FreeListManyCached14RemoveCategoryEPNS0_16FreeListCategoryE, .-_ZN2v88internal18FreeListManyCached14RemoveCategoryEPNS0_16FreeListCategoryE
	.section	.text._ZN2v88internal8NewSpace23CommittedPhysicalMemoryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8NewSpace23CommittedPhysicalMemoryEv
	.type	_ZN2v88internal8NewSpace23CommittedPhysicalMemoryEv, @function
_ZN2v88internal8NewSpace23CommittedPhysicalMemoryEv:
.LFB23015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZN2v84base2OS14HasLazyCommitsEv@PLT
	testb	%al, %al
	jne	.L655
	movq	(%r12), %rax
	leaq	_ZN2v88internal8NewSpace15CommittedMemoryEv(%rip), %rdx
	movq	56(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L656
	movq	288(%r12), %r13
	addq	448(%r12), %r13
.L654:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L655:
	.cfi_restore_state
	movq	104(%r12), %rax
	testq	%rax, %rax
	je	.L661
	leaq	-1(%rax), %rcx
	andq	$-262144, %rcx
	subq	%rcx, %rax
	addq	$152, %rcx
	movq	%rax, %rdx
.L662:
	movq	(%rcx), %rax
	cmpq	%rax, %rdx
	jle	.L661
	lock cmpxchgq	%rdx, (%rcx)
	jne	.L662
.L661:
	xorl	%r13d, %r13d
	cmpb	$0, 344(%r12)
	je	.L660
	movq	240(%r12), %rbx
	xorl	%r13d, %r13d
	testq	%rbx, %rbx
	jne	.L667
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L663:
	testb	$32, 10(%rbx)
	jne	.L665
	movq	80(%rbx), %rax
	cmpl	$5, 72(%rax)
	je	.L666
.L665:
	movq	152(%rbx), %rax
	addq	%rax, %r13
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L660
.L667:
	call	_ZN2v84base2OS14HasLazyCommitsEv@PLT
	testb	%al, %al
	jne	.L663
.L666:
	movq	(%rbx), %rax
	movq	224(%rbx), %rbx
	addq	%rax, %r13
	testq	%rbx, %rbx
	jne	.L667
.L660:
	cmpb	$0, 504(%r12)
	je	.L654
	movq	400(%r12), %rbx
	testq	%rbx, %rbx
	je	.L654
	xorl	%r12d, %r12d
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L669:
	testb	$32, 10(%rbx)
	jne	.L671
	movq	80(%rbx), %rax
	cmpl	$5, 72(%rax)
	je	.L672
.L671:
	movq	152(%rbx), %rax
	addq	%rax, %r12
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L681
.L673:
	call	_ZN2v84base2OS14HasLazyCommitsEv@PLT
	testb	%al, %al
	jne	.L669
.L672:
	movq	(%rbx), %rax
	movq	224(%rbx), %rbx
	addq	%rax, %r12
	testq	%rbx, %rbx
	jne	.L673
.L681:
	addq	$8, %rsp
	addq	%r12, %r13
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L656:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE23015:
	.size	_ZN2v88internal8NewSpace23CommittedPhysicalMemoryEv, .-_ZN2v88internal8NewSpace23CommittedPhysicalMemoryEv
	.section	.text._ZN2v88internal9SemiSpaceD2Ev,"axG",@progbits,_ZN2v88internal9SemiSpaceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9SemiSpaceD2Ev
	.type	_ZN2v88internal9SemiSpaceD2Ev, @function
_ZN2v88internal9SemiSpaceD2Ev:
.LFB30714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L683
	call	_ZdaPv@PLT
.L683:
	movq	96(%rbx), %rdi
	movq	$0, 48(%rbx)
	testq	%rdi, %rdi
	je	.L684
	movq	(%rdi), %rax
	call	*8(%rax)
.L684:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L682
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L682:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE30714:
	.size	_ZN2v88internal9SemiSpaceD2Ev, .-_ZN2v88internal9SemiSpaceD2Ev
	.weak	_ZN2v88internal9SemiSpaceD1Ev
	.set	_ZN2v88internal9SemiSpaceD1Ev,_ZN2v88internal9SemiSpaceD2Ev
	.section	.text._ZN2v88internal9SemiSpaceD0Ev,"axG",@progbits,_ZN2v88internal9SemiSpaceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9SemiSpaceD0Ev
	.type	_ZN2v88internal9SemiSpaceD0Ev, @function
_ZN2v88internal9SemiSpaceD0Ev:
.LFB30716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L694
	call	_ZdaPv@PLT
.L694:
	movq	$0, 48(%r12)
	movq	96(%r12), %rdi
	testq	%rdi, %rdi
	je	.L695
	movq	(%rdi), %rax
	call	*8(%rax)
.L695:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L696
	call	_ZdlPv@PLT
.L696:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE30716:
	.size	_ZN2v88internal9SemiSpaceD0Ev, .-_ZN2v88internal9SemiSpaceD0Ev
	.section	.text._ZN2v88internal8FreeList4FreeEmmNS0_8FreeModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8FreeList4FreeEmmNS0_8FreeModeE
	.type	_ZN2v88internal8FreeList4FreeEmmNS0_8FreeModeE, @function
_ZN2v88internal8FreeList4FreeEmmNS0_8FreeModeE:
.LFB23039:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	andq	$-262144, %r14
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	subq	%rdx, 192(%r14)
	cmpq	%rdx, 16(%rdi)
	ja	.L713
	movq	(%rdi), %rax
	movq	%rsi, %r12
	movl	%ecx, %r15d
	movq	%rdx, %rsi
	addq	$1, %r12
	call	*72(%rax)
	movq	240(%r14), %rdx
	cltq
	movq	(%rdx,%rax,8), %rsi
	movq	8(%rsi), %rax
	movq	%rax, 15(%r12)
	movq	%r12, 8(%rsi)
	xorl	%r12d, %r12d
	addl	%ebx, 4(%rsi)
	testl	%r15d, %r15d
	jne	.L707
	cmpq	$0, 16(%rsi)
	je	.L714
.L710:
	addq	%rbx, 40(%r13)
	xorl	%r12d, %r12d
.L707:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L713:
	.cfi_restore_state
	addq	%rdx, 216(%r14)
	lock addq	%rdx, 24(%rdi)
	movq	%rdx, %r12
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L714:
	cmpq	$0, 24(%rsi)
	jne	.L710
	movslq	(%rsi), %rdx
	movq	32(%r13), %rax
	cmpq	(%rax,%rdx,8), %rsi
	je	.L710
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*56(%rax)
	jmp	.L707
	.cfi_endproc
.LFE23039:
	.size	_ZN2v88internal8FreeList4FreeEmmNS0_8FreeModeE, .-_ZN2v88internal8FreeList4FreeEmmNS0_8FreeModeE
	.section	.text._ZN2v88internal18FreeListManyCached4FreeEmmNS0_8FreeModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18FreeListManyCached4FreeEmmNS0_8FreeModeE
	.type	_ZN2v88internal18FreeListManyCached4FreeEmmNS0_8FreeModeE, @function
_ZN2v88internal18FreeListManyCached4FreeEmmNS0_8FreeModeE:
.LFB23072:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	andq	$-262144, %r15
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	subq	%rdx, 192(%r15)
	cmpq	%rdx, 16(%rdi)
	ja	.L759
	movq	(%rdi), %rax
	leaq	_ZN2v88internal12FreeListMany26SelectFreeListCategoryTypeEm(%rip), %rdx
	movq	%rsi, %r14
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L718
	cmpq	$256, %rbx
	jbe	.L760
	movl	12(%rdi), %r12d
	cmpl	$15, %r12d
	jle	.L722
	cmpq	$511, %rbx
	jbe	.L729
	cmpl	$16, %r12d
	je	.L722
	cmpq	$1023, %rbx
	jbe	.L730
	cmpl	$17, %r12d
	je	.L722
	cmpq	$2047, %rbx
	jbe	.L731
	cmpl	$18, %r12d
	je	.L722
	cmpq	$4095, %rbx
	jbe	.L732
	cmpl	$19, %r12d
	je	.L722
	cmpq	$8191, %rbx
	jbe	.L733
	cmpl	$20, %r12d
	je	.L722
	cmpq	$16383, %rbx
	jbe	.L734
	cmpl	$21, %r12d
	je	.L722
	cmpq	$32767, %rbx
	jbe	.L735
	cmpl	$22, %r12d
	je	.L722
	cmpq	$65535, %rbx
	jbe	.L736
.L722:
	movslq	%r12d, %rax
	salq	$3, %rax
.L720:
	movq	240(%r15), %rdx
	addq	$1, %r14
	movq	(%rdx,%rax), %rsi
	movq	8(%rsi), %rax
	movq	%rax, 15(%r14)
	movq	%r14, 8(%rsi)
	addl	%ebx, 4(%rsi)
	testl	%ecx, %ecx
	jne	.L726
	cmpq	$0, 16(%rsi)
	je	.L761
.L724:
	addq	%rbx, 40(%r13)
.L725:
	testl	%r12d, %r12d
	js	.L726
	movslq	%r12d, %rax
	leaq	0(%r13,%rax,4), %rax
	subq	$4, %r13
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L762:
	movl	%r12d, 48(%rax)
	subq	$4, %rax
	cmpq	%r13, %rax
	je	.L726
.L727:
	cmpl	48(%rax), %r12d
	jl	.L762
.L726:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L760:
	.cfi_restore_state
	cmpq	$31, %rbx
	ja	.L763
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L759:
	addq	%rdx, 216(%r15)
	lock addq	%rdx, 24(%rdi)
	addq	$24, %rsp
	movq	%rdx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L763:
	.cfi_restore_state
	movq	%rbx, %r12
	shrq	$4, %r12
	subl	$1, %r12d
	movslq	%r12d, %rax
	salq	$3, %rax
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L718:
	movl	%ecx, -52(%rbp)
	movq	%rbx, %rsi
	call	*%rax
	movl	-52(%rbp), %ecx
	movl	%eax, %r12d
	cltq
	salq	$3, %rax
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L761:
	cmpq	$0, 24(%rsi)
	jne	.L724
	movslq	(%rsi), %rdx
	movq	32(%r13), %rax
	cmpq	(%rax,%rdx,8), %rsi
	je	.L724
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*56(%rax)
	jmp	.L725
.L736:
	movl	$22, %r12d
	jmp	.L722
.L729:
	movl	$15, %r12d
	jmp	.L722
.L730:
	movl	$16, %r12d
	jmp	.L722
.L731:
	movl	$17, %r12d
	jmp	.L722
.L732:
	movl	$18, %r12d
	jmp	.L722
.L733:
	movl	$19, %r12d
	jmp	.L722
.L734:
	movl	$20, %r12d
	jmp	.L722
.L735:
	movl	$21, %r12d
	jmp	.L722
	.cfi_endproc
.LFE23072:
	.size	_ZN2v88internal18FreeListManyCached4FreeEmmNS0_8FreeModeE, .-_ZN2v88internal18FreeListManyCached4FreeEmmNS0_8FreeModeE
	.section	.text._ZN2v88internal19SpaceWithLinearArea24PauseAllocationObserversEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SpaceWithLinearArea24PauseAllocationObserversEv
	.type	_ZN2v88internal19SpaceWithLinearArea24PauseAllocationObserversEv, @function
_ZN2v88internal19SpaceWithLinearArea24PauseAllocationObserversEv:
.LFB22992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	64(%rdi), %rax
	cmpb	$0, 432(%rax)
	jne	.L765
	movq	120(%rdi), %r13
	testq	%r13, %r13
	je	.L765
	movq	104(%rdi), %rbx
	cmpq	%r13, %rbx
	jnb	.L766
	movq	%rbx, 120(%rdi)
	movq	%rbx, %r13
.L766:
	cmpb	$0, 56(%r12)
	jne	.L767
	movq	8(%r12), %rdx
	cmpq	%rdx, 16(%r12)
	je	.L767
	movb	$1, 432(%rax)
	movq	64(%r12), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$1, %r8d
	movl	$1, %ecx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movl	%ebx, %eax
	movq	16(%r12), %r14
	movq	8(%r12), %rbx
	subl	%r13d, %eax
	movl	%eax, %r13d
	cmpq	%r14, %rbx
	je	.L770
	.p2align 4,,10
	.p2align 3
.L769:
	movq	(%rbx), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r13d, %esi
	addq	$8, %rbx
	call	_ZN2v88internal18AllocationObserver14AllocationStepEimm@PLT
	cmpq	%rbx, %r14
	jne	.L769
.L770:
	movq	64(%r12), %rax
	movb	$0, 432(%rax)
.L767:
	movq	$0, 120(%r12)
.L765:
	movq	(%r12), %rax
	popq	%rbx
	movb	$1, 56(%r12)
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%r12
	popq	%r13
	movq	136(%rax), %rax
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE22992:
	.size	_ZN2v88internal19SpaceWithLinearArea24PauseAllocationObserversEv, .-_ZN2v88internal19SpaceWithLinearArea24PauseAllocationObserversEv
	.section	.text._ZN2v88internal19SpaceWithLinearArea24RemoveAllocationObserverEPNS0_18AllocationObserverE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SpaceWithLinearArea24RemoveAllocationObserverEPNS0_18AllocationObserverE
	.type	_ZN2v88internal19SpaceWithLinearArea24RemoveAllocationObserverEPNS0_18AllocationObserverE, @function
_ZN2v88internal19SpaceWithLinearArea24RemoveAllocationObserverEPNS0_18AllocationObserverE:
.LFB22991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	$0, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	movq	8(%rdi), %rdx
	movq	104(%rdi), %rbx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	cmpq	$8, %rcx
	movq	64(%rdi), %rcx
	cmovne	%rbx, %r14
	cmpb	$0, 432(%rcx)
	jne	.L779
	movq	120(%rdi), %r15
	testq	%r15, %r15
	je	.L779
	cmpq	%rbx, %r15
	ja	.L798
.L780:
	cmpb	$1, 56(%r12)
	je	.L781
	cmpq	%rax, %rdx
	je	.L781
	movb	$1, 432(%rcx)
	movq	64(%r12), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$1, %r8d
	movl	$1, %ecx
	subl	%r15d, %ebx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	16(%r12), %rax
	movq	8(%r12), %r15
	movq	%rax, -56(%rbp)
	cmpq	%rax, %r15
	je	.L784
	.p2align 4,,10
	.p2align 3
.L783:
	movq	(%r15), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%ebx, %esi
	addq	$8, %r15
	call	_ZN2v88internal18AllocationObserver14AllocationStepEimm@PLT
	cmpq	%r15, -56(%rbp)
	jne	.L783
.L784:
	movq	64(%r12), %rax
	movb	$0, 432(%rax)
.L781:
	movq	%r14, 120(%r12)
.L779:
	addq	$24, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal5Space24RemoveAllocationObserverEPNS0_18AllocationObserverE
	.p2align 4,,10
	.p2align 3
.L798:
	.cfi_restore_state
	movq	%rbx, 120(%rdi)
	movq	%rbx, %r15
	jmp	.L780
	.cfi_endproc
.LFE22991:
	.size	_ZN2v88internal19SpaceWithLinearArea24RemoveAllocationObserverEPNS0_18AllocationObserverE, .-_ZN2v88internal19SpaceWithLinearArea24RemoveAllocationObserverEPNS0_18AllocationObserverE
	.section	.text._ZN2v88internal17FreeListFastAlloc8AllocateEmPmNS0_16AllocationOriginE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17FreeListFastAlloc8AllocateEmPmNS0_16AllocationOriginE
	.type	_ZN2v88internal17FreeListFastAlloc8AllocateEmPmNS0_16AllocationOriginE, @function
_ZN2v88internal17FreeListFastAlloc8AllocateEmPmNS0_16AllocationOriginE:
.LFB23055:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	leaq	_ZN2v88internal17FreeListFastAlloc26SelectFreeListCategoryTypeEm(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L800
	xorl	%eax, %eax
	cmpq	$16376, %rsi
	jbe	.L801
	xorl	%eax, %eax
	cmpq	$65528, %rsi
	seta	%al
	addl	$1, %eax
.L801:
	movq	32(%r13), %rdi
	xorl	%ebx, %ebx
	movl	$1, %r8d
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L823:
	movq	$0, (%r14)
	xorl	%r12d, %r12d
.L808:
	cmpq	$0, 8(%rsi)
	jne	.L809
	movq	0(%r13), %rdx
	movl	%eax, -52(%rbp)
	movq	%r13, %rdi
	call	*64(%rdx)
	movl	-52(%rbp), %eax
	movl	$1, %r8d
.L809:
	movl	%r8d, %edx
	subl	%ebx, %edx
	cmpl	%edx, %eax
	jg	.L821
	testq	%r12, %r12
	jne	.L804
	movq	32(%r13), %rdi
.L806:
	addq	$1, %rbx
.L803:
	movq	%rbx, %rcx
	negq	%rcx
	movq	16(%rdi,%rcx,8), %rsi
	testq	%rsi, %rsi
	je	.L822
	movq	8(%rsi), %r12
	movslq	11(%r12), %rdx
	cmpq	%rdx, %r15
	ja	.L823
	movq	15(%r12), %rdx
	movq	%rdx, 8(%rsi)
	movslq	11(%r12), %rdx
	movq	%rdx, (%r14)
	subl	%edx, 4(%rsi)
	testq	%r12, %r12
	je	.L808
	movq	(%r14), %rdx
	subq	%rdx, 40(%r13)
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L822:
	movl	%r8d, %edx
	subl	%ebx, %edx
	cmpl	%edx, %eax
	jle	.L806
	xorl	%r12d, %r12d
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L821:
	testq	%r12, %r12
	je	.L802
	.p2align 4,,10
	.p2align 3
.L804:
	movq	%r12, %rax
	movq	(%r14), %rdx
	andq	$-262144, %rax
	addq	%rdx, 192(%rax)
.L802:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L800:
	.cfi_restore_state
	call	*%rax
	xorl	%r12d, %r12d
	cmpl	$2, %eax
	jg	.L802
	jmp	.L801
	.cfi_endproc
.LFE23055:
	.size	_ZN2v88internal17FreeListFastAlloc8AllocateEmPmNS0_16AllocationOriginE, .-_ZN2v88internal17FreeListFastAlloc8AllocateEmPmNS0_16AllocationOriginE
	.section	.text._ZN2v88internal8NewSpace27UpdateInlineAllocationLimitEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8NewSpace27UpdateInlineAllocationLimitEm
	.type	_ZN2v88internal8NewSpace27UpdateInlineAllocationLimitEm, @function
_ZN2v88internal8NewSpace27UpdateInlineAllocationLimitEm:
.LFB22983:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	64(%rdi), %rax
	movq	%rdi, %rbx
	movq	104(%rdi), %r13
	cmpb	$0, 1520(%rax)
	leaq	(%rsi,%r13), %r14
	jne	.L826
	movq	352(%rdi), %rax
	movq	48(%rax), %r14
	movq	(%rdi), %rax
	call	*128(%rax)
	testb	%al, %al
	je	.L826
	cmpb	$0, 56(%rbx)
	jne	.L826
	movq	16(%rbx), %rcx
	movq	8(%rbx), %rax
	cmpq	%rax, %rcx
	je	.L826
	xorl	%esi, %esi
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L838:
	cmpq	%rdx, %rsi
	cmovg	%rdx, %rsi
	addq	$8, %rax
	cmpq	%rax, %rcx
	je	.L837
.L828:
	movq	(%rax), %rdx
	movq	16(%rdx), %rdx
	testq	%rsi, %rsi
	jne	.L838
	addq	$8, %rax
	movq	%rdx, %rsi
	cmpq	%rax, %rcx
	jne	.L828
.L837:
	cmpl	$1, 72(%rbx)
	leaq	-1(%rsi), %rax
	je	.L830
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*104(%rax)
	cltq
.L830:
	leaq	(%r12,%r13), %rsi
	addq	%rax, %rsi
	cmpq	%rsi, %r14
	cmova	%rsi, %r14
.L826:
	movq	%r14, 112(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22983:
	.size	_ZN2v88internal8NewSpace27UpdateInlineAllocationLimitEm, .-_ZN2v88internal8NewSpace27UpdateInlineAllocationLimitEm
	.section	.text._ZN2v88internal11FreeListMap8AllocateEmPmNS0_16AllocationOriginE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11FreeListMap8AllocateEmPmNS0_16AllocationOriginE
	.type	_ZN2v88internal11FreeListMap8AllocateEmPmNS0_16AllocationOriginE, @function
_ZN2v88internal11FreeListMap8AllocateEmPmNS0_16AllocationOriginE:
.LFB23085:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	%rsi, %rcx
	movq	(%rax), %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L853
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	8(%rsi), %rax
	movslq	11(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L841
	movq	$0, (%rbx)
.L842:
	movq	8(%rsi), %rax
	testq	%rax, %rax
	jne	.L856
	movq	(%rdi), %rdx
	movq	%rax, -24(%rbp)
	call	*64(%rdx)
	movq	-24(%rbp), %rax
	jmp	.L849
	.p2align 4,,10
	.p2align 3
.L841:
	movq	15(%rax), %rdx
	movq	%rdx, 8(%rsi)
	movslq	11(%rax), %rdx
	movq	%rdx, (%rbx)
	subl	%edx, 4(%rsi)
	testq	%rax, %rax
	je	.L842
	movq	(%rbx), %rdx
	subq	%rdx, 40(%rdi)
	cmpq	$0, 8(%rsi)
	je	.L843
.L845:
	movq	%rax, %rdx
	movq	(%rbx), %rcx
	andq	$-262144, %rdx
	addq	%rcx, 192(%rdx)
.L849:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L853:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L843:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdx
	movq	%rax, -24(%rbp)
	call	*64(%rdx)
	movq	-24(%rbp), %rax
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L856:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23085:
	.size	_ZN2v88internal11FreeListMap8AllocateEmPmNS0_16AllocationOriginE, .-_ZN2v88internal11FreeListMap8AllocateEmPmNS0_16AllocationOriginE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB9589:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L863
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L866
.L857:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L863:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L866:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L857
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE9589:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.text._ZN2v88internal24PagedSpaceObjectIteratorC2EPNS0_10PagedSpaceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PagedSpaceObjectIteratorC2EPNS0_10PagedSpaceE
	.type	_ZN2v88internal24PagedSpaceObjectIteratorC2EPNS0_10PagedSpaceE, @function
_ZN2v88internal24PagedSpaceObjectIteratorC2EPNS0_10PagedSpaceE:
.LFB22741:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal24PagedSpaceObjectIteratorE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsi, 24(%rdi)
	movq	%rax, (%rdi)
	movq	32(%rsi), %rax
	movups	%xmm0, 8(%rdi)
	movq	%rax, 32(%rdi)
	movq	$0, 40(%rdi)
	movq	%rax, 48(%rdi)
	ret
	.cfi_endproc
.LFE22741:
	.size	_ZN2v88internal24PagedSpaceObjectIteratorC2EPNS0_10PagedSpaceE, .-_ZN2v88internal24PagedSpaceObjectIteratorC2EPNS0_10PagedSpaceE
	.globl	_ZN2v88internal24PagedSpaceObjectIteratorC1EPNS0_10PagedSpaceE
	.set	_ZN2v88internal24PagedSpaceObjectIteratorC1EPNS0_10PagedSpaceE,_ZN2v88internal24PagedSpaceObjectIteratorC2EPNS0_10PagedSpaceE
	.section	.text._ZN2v88internal24PagedSpaceObjectIteratorC2EPNS0_4PageE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PagedSpaceObjectIteratorC2EPNS0_4PageE
	.type	_ZN2v88internal24PagedSpaceObjectIteratorC2EPNS0_4PageE, @function
_ZN2v88internal24PagedSpaceObjectIteratorC2EPNS0_4PageE:
.LFB22744:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal24PagedSpaceObjectIteratorE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, (%rdi)
	movups	%xmm0, 8(%rdi)
	movq	80(%rsi), %rax
	movq	%rax, 24(%rdi)
	movq	224(%rsi), %rax
	movq	%rsi, 32(%rdi)
	movq	%rsi, 48(%rdi)
	movq	%rax, 40(%rdi)
	ret
	.cfi_endproc
.LFE22744:
	.size	_ZN2v88internal24PagedSpaceObjectIteratorC2EPNS0_4PageE, .-_ZN2v88internal24PagedSpaceObjectIteratorC2EPNS0_4PageE
	.globl	_ZN2v88internal24PagedSpaceObjectIteratorC1EPNS0_4PageE
	.set	_ZN2v88internal24PagedSpaceObjectIteratorC1EPNS0_4PageE,_ZN2v88internal24PagedSpaceObjectIteratorC2EPNS0_4PageE
	.section	.text._ZN2v88internal24PagedSpaceObjectIterator17AdvanceToNextPageEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PagedSpaceObjectIterator17AdvanceToNextPageEv
	.type	_ZN2v88internal24PagedSpaceObjectIterator17AdvanceToNextPageEv, @function
_ZN2v88internal24PagedSpaceObjectIterator17AdvanceToNextPageEv:
.LFB22746:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	48(%rdi), %r12
	cmpq	%r12, 40(%rdi)
	je	.L869
	movq	224(%r12), %rax
	movq	%rdi, %rbx
	movq	%r12, %rsi
	movq	%rax, 48(%rdi)
	movq	24(%rdi), %rax
	movq	64(%rax), %r13
	movq	2016(%r13), %rax
	movq	9984(%rax), %rdi
	call	_ZN2v88internal7Sweeper20EnsurePageIsIterableEPNS0_4PageE@PLT
	testb	$2, 10(%r12)
	jne	.L878
.L871:
	movq	40(%r12), %rax
	movq	%rax, 8(%rbx)
	movq	48(%r12), %rax
	movq	%rax, 16(%rbx)
	movl	$1, %eax
.L869:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L878:
	.cfi_restore_state
	movq	2024(%r13), %rdi
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal25MinorMarkCompactCollector12MakeIterableEPNS0_4PageENS0_20MarkingTreatmentModeENS0_22FreeSpaceTreatmentModeE@PLT
	jmp	.L871
	.cfi_endproc
.LFE22746:
	.size	_ZN2v88internal24PagedSpaceObjectIterator17AdvanceToNextPageEv, .-_ZN2v88internal24PagedSpaceObjectIterator17AdvanceToNextPageEv
	.section	.text._ZN2v88internal20CodeRangeAddressHint14GetAddressHintEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20CodeRangeAddressHint14GetAddressHintEm
	.type	_ZN2v88internal20CodeRangeAddressHint14GetAddressHintEm, @function
_ZN2v88internal20CodeRangeAddressHint14GetAddressHintEm:
.LFB22753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	48(%r12), %rsi
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	40(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L880
	movq	(%rax), %rcx
	movq	%rdx, %r8
	movq	8(%rcx), %rdi
	jmp	.L882
	.p2align 4,,10
	.p2align 3
.L895:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L880
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L880
.L882:
	cmpq	%rdi, %rbx
	jne	.L895
	movq	24(%rcx), %rdx
	cmpq	16(%rcx), %rdx
	je	.L880
	movq	-8(%rdx), %r13
	subq	$8, %rdx
	movq	%rdx, 24(%rcx)
.L883:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L880:
	.cfi_restore_state
	call	_ZN2v88internal17GetRandomMmapAddrEv@PLT
	movq	%rax, %r13
	jmp	.L883
	.cfi_endproc
.LFE22753:
	.size	_ZN2v88internal20CodeRangeAddressHint14GetAddressHintEm, .-_ZN2v88internal20CodeRangeAddressHint14GetAddressHintEm
	.section	.rodata._ZN2v88internal15MemoryAllocator27InitializeCodePageAllocatorEPNS_13PageAllocatorEm.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"CodeRange setup: allocate virtual memory"
	.section	.rodata._ZN2v88internal15MemoryAllocator27InitializeCodePageAllocatorEPNS_13PageAllocatorEm.str1.1,"aMS",@progbits,1
.LC8:
	.string	"CodeRange"
	.section	.text._ZN2v88internal15MemoryAllocator27InitializeCodePageAllocatorEPNS_13PageAllocatorEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocator27InitializeCodePageAllocatorEPNS_13PageAllocatorEm
	.type	_ZN2v88internal15MemoryAllocator27InitializeCodePageAllocatorEPNS_13PageAllocatorEm, @function
_ZN2v88internal15MemoryAllocator27InitializeCodePageAllocatorEPNS_13PageAllocatorEm:
.LFB22785:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	$134217728, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, 40(%rdi)
	testq	%rdx, %rdx
	je	.L897
	cmpq	$3145728, %rdx
	movl	$3145728, %r14d
	cmovnb	%rdx, %r14
.L897:
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	jne	.L898
	call	_ZN2v88internal14CommitPageSizeEv@PLT
.L898:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	movq	%rax, %r13
	movzbl	_ZN2v88internalL23code_range_address_hintE(%rip), %eax
	cmpb	$2, %al
	jne	.L922
.L899:
	movq	%r14, %rsi
	leaq	8+_ZN2v88internalL23code_range_address_hintE(%rip), %rdi
	leaq	-128(%rbp), %r15
	negq	%r13
	call	_ZN2v88internal20CodeRangeAddressHint14GetAddressHintEm
	movq	%r12, %rdi
	andq	%rax, %r13
	movq	(%r12), %rax
	call	*16(%rax)
	movl	$4096, %r8d
	movq	%r14, %rdx
	movq	%r13, %rcx
	cmpq	$4096, %rax
	movq	%r12, %rsi
	movq	%r15, %rdi
	cmovnb	%rax, %r8
	call	_ZN2v88internal13VirtualMemoryC1EPNS_13PageAllocatorEmPvm@PLT
	movq	-120(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L923
	movq	-112(%rbp), %rax
	leaq	262143(%rdx), %r13
	movq	%rdx, 48(%rbx)
	andq	$-262144, %r13
	movq	%rax, 56(%rbx)
	addq	%rdx, %rax
	subq	%r13, %rax
	andq	$-262144, %rax
	movq	%rax, -136(%rbp)
	movq	(%rbx), %rax
	movq	41016(%rax), %rdi
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movq	-144(%rbp), %rdi
	testb	%al, %al
	jne	.L924
.L902:
	movq	-128(%rbp), %rax
	movdqu	-120(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%rax, 8(%rbx)
	movups	%xmm1, 16(%rbx)
	call	_ZN2v88internal13VirtualMemory5ResetEv@PLT
	movl	$224, %edi
	call	_Znwm@PLT
	movq	-136(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r13, %rdx
	movl	$262144, %r8d
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v84base20BoundedPageAllocatorC1EPNS_13PageAllocatorEmmm@PLT
	movq	64(%rbx), %r12
	movq	%r14, 64(%rbx)
	testq	%r12, %r12
	je	.L903
	movq	(%r12), %rax
	leaq	_ZN2v84base20BoundedPageAllocatorD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L904
	leaq	16+_ZTVN2v84base20BoundedPageAllocatorE(%rip), %rax
	leaq	72(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZN2v84base15RegionAllocatorD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movl	$224, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	64(%rbx), %r14
.L903:
	movq	%r14, 40(%rbx)
	movq	%r15, %rdi
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L925
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L922:
	.cfi_restore_state
	leaq	_ZN2v84base16LazyInstanceImplINS_8internal20CodeRangeAddressHintENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -96(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internalL23code_range_address_hintE(%rip), %rax
	leaq	-96(%rbp), %r15
	movq	%rax, -88(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r15, %rsi
	leaq	_ZN2v88internalL23code_range_address_hintE(%rip), %rdi
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L899
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
	jmp	.L899
	.p2align 4,,10
	.p2align 3
.L924:
	movq	-120(%rbp), %rdx
	movq	%r14, %rcx
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal6Logger8NewEventEPKcPvm@PLT
	jmp	.L902
	.p2align 4,,10
	.p2align 3
.L904:
	movq	%r12, %rdi
	call	*%rax
	movq	64(%rbx), %r14
	jmp	.L903
.L925:
	call	__stack_chk_fail@PLT
.L923:
	movq	(%rbx), %rdi
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal2V823FatalProcessOutOfMemoryEPNS0_7IsolateEPKcb@PLT
	.cfi_endproc
.LFE22785:
	.size	_ZN2v88internal15MemoryAllocator27InitializeCodePageAllocatorEPNS_13PageAllocatorEm, .-_ZN2v88internal15MemoryAllocator27InitializeCodePageAllocatorEPNS_13PageAllocatorEm
	.section	.text._ZN2v88internal15MemoryAllocatorC2EPNS0_7IsolateEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocatorC2EPNS0_7IsolateEmm
	.type	_ZN2v88internal15MemoryAllocatorC2EPNS0_7IsolateEmm, @function
_ZN2v88internal15MemoryAllocatorC2EPNS0_7IsolateEmm:
.LFB22783:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	addq	$262143, %r13
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	andq	$-262144, %r13
	addq	$37592, %rbx
	subq	$8, %rsp
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movups	%xmm0, 16(%rdi)
	movq	%rsi, %rdi
	call	_ZN2v88internal7Isolate14page_allocatorEv@PLT
	movq	%r13, 72(%r12)
	pxor	%xmm0, %xmm0
	leaq	152(%r12), %rdi
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movq	$0, 64(%r12)
	movq	$0, 80(%r12)
	movq	$0, 88(%r12)
	movq	$-1, 96(%r12)
	movq	$0, 104(%r12)
	movq	$0, 112(%r12)
	movq	%rbx, 136(%r12)
	movq	%r12, 144(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 120(%r12)
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	296(%r12), %rdi
	xorl	%esi, %esi
	movq	$0, 192(%r12)
	movq	$0, 200(%r12)
	movq	$0, 208(%r12)
	movq	$0, 216(%r12)
	movq	$0, 224(%r12)
	movq	$0, 232(%r12)
	movq	$0, 240(%r12)
	movq	$0, 248(%r12)
	movq	$0, 256(%r12)
	call	_ZN2v84base9SemaphoreC1Ei@PLT
	movq	192(%r12), %rdx
	pxor	%xmm0, %xmm0
	movq	208(%r12), %rax
	movups	%xmm0, 328(%r12)
	subq	%rdx, %rax
	cmpq	$511, %rax
	jbe	.L942
.L927:
	movq	240(%r12), %rdx
	movq	256(%r12), %rax
	subq	%rdx, %rax
	cmpq	$511, %rax
	jbe	.L943
.L931:
	leaq	392(%r12), %rax
	movq	%r14, %rdx
	movq	$1, 352(%r12)
	movq	32(%r12), %rsi
	movq	%rax, 344(%r12)
	movq	%r12, %rdi
	movq	$0, 360(%r12)
	movq	$0, 368(%r12)
	movl	$0x3f800000, 376(%r12)
	movq	$0, 384(%r12)
	movq	$0, 392(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal15MemoryAllocator27InitializeCodePageAllocatorEPNS_13PageAllocatorEm
	.p2align 4,,10
	.p2align 3
.L942:
	.cfi_restore_state
	movq	200(%r12), %r13
	movl	$512, %edi
	subq	%rdx, %r13
	call	_Znwm@PLT
	movq	192(%r12), %r15
	movq	200(%r12), %rdx
	movq	%rax, %rbx
	subq	%r15, %rdx
	testq	%rdx, %rdx
	jg	.L944
	testq	%r15, %r15
	jne	.L929
.L930:
	addq	%rbx, %r13
	movq	%rbx, 192(%r12)
	addq	$512, %rbx
	movq	%r13, 200(%r12)
	movq	%rbx, 208(%r12)
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L943:
	movq	248(%r12), %r13
	movl	$512, %edi
	subq	%rdx, %r13
	call	_Znwm@PLT
	movq	240(%r12), %r15
	movq	248(%r12), %rdx
	movq	%rax, %rbx
	subq	%r15, %rdx
	testq	%rdx, %rdx
	jg	.L945
	testq	%r15, %r15
	jne	.L933
.L934:
	addq	%rbx, %r13
	movq	%rbx, 240(%r12)
	addq	$512, %rbx
	movq	%r13, 248(%r12)
	movq	%rbx, 256(%r12)
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L945:
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	memmove@PLT
.L933:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L934
	.p2align 4,,10
	.p2align 3
.L944:
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	memmove@PLT
.L929:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L930
	.cfi_endproc
.LFE22783:
	.size	_ZN2v88internal15MemoryAllocatorC2EPNS0_7IsolateEmm, .-_ZN2v88internal15MemoryAllocatorC2EPNS0_7IsolateEmm
	.globl	_ZN2v88internal15MemoryAllocatorC1EPNS0_7IsolateEmm
	.set	_ZN2v88internal15MemoryAllocatorC1EPNS0_7IsolateEmm,_ZN2v88internal15MemoryAllocatorC2EPNS0_7IsolateEmm
	.section	.rodata._ZN2v88internal15MemoryAllocator8Unmapper28CancelAndWaitForPendingTasksEv.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"Unmapper::CancelAndWaitForPendingTasks: no tasks remaining\n"
	.section	.text._ZN2v88internal15MemoryAllocator8Unmapper28CancelAndWaitForPendingTasksEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocator8Unmapper28CancelAndWaitForPendingTasksEv
	.type	_ZN2v88internal15MemoryAllocator8Unmapper28CancelAndWaitForPendingTasksEv, @function
_ZN2v88internal15MemoryAllocator8Unmapper28CancelAndWaitForPendingTasksEv:
.LFB22803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	160(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, 192(%rdi)
	jle	.L951
	.p2align 4,,10
	.p2align 3
.L953:
	movq	(%rbx), %rax
	movq	128(%rbx,%r12,8), %rsi
	movq	8048(%rax), %rdi
	call	_ZN2v88internal21CancelableTaskManager8TryAbortEm@PLT
	cmpl	$2, %eax
	je	.L950
	movq	%r13, %rdi
	addq	$1, %r12
	call	_ZN2v84base9Semaphore4WaitEv@PLT
	cmpq	192(%rbx), %r12
	jl	.L953
.L951:
	movq	$0, 192(%rbx)
	movq	$0, 200(%rbx)
	mfence
	cmpb	$0, _ZN2v88internal19FLAG_trace_unmapperE(%rip)
	jne	.L957
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L950:
	.cfi_restore_state
	addq	$1, %r12
	cmpq	%r12, 192(%rbx)
	jg	.L953
	jmp	.L951
	.p2align 4,,10
	.p2align 3
.L957:
	movq	(%rbx), %rdi
	addq	$8, %rsp
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	subq	$37592, %rdi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12PrintIsolateEPvPKcz@PLT
	.cfi_endproc
.LFE22803:
	.size	_ZN2v88internal15MemoryAllocator8Unmapper28CancelAndWaitForPendingTasksEv, .-_ZN2v88internal15MemoryAllocator8Unmapper28CancelAndWaitForPendingTasksEv
	.section	.text._ZN2v88internal15MemoryAllocator8Unmapper19MakeRoomForNewTasksEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocator8Unmapper19MakeRoomForNewTasksEv
	.type	_ZN2v88internal15MemoryAllocator8Unmapper19MakeRoomForNewTasksEv, @function
_ZN2v88internal15MemoryAllocator8Unmapper19MakeRoomForNewTasksEv:
.LFB22806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	200(%rdi), %r12
	movq	192(%rdi), %rax
	testq	%r12, %r12
	jne	.L960
	testq	%rax, %rax
	jg	.L968
.L960:
	cmpq	$4, %rax
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L968:
	.cfi_restore_state
	leaq	160(%rdi), %r13
.L964:
	movq	(%rbx), %rax
	movq	128(%rbx,%r12,8), %rsi
	movq	8048(%rax), %rdi
	call	_ZN2v88internal21CancelableTaskManager8TryAbortEm@PLT
	cmpl	$2, %eax
	je	.L961
	movq	%r13, %rdi
	addq	$1, %r12
	call	_ZN2v84base9Semaphore4WaitEv@PLT
	cmpq	%r12, 192(%rbx)
	jg	.L964
.L962:
	movq	$0, 192(%rbx)
	movq	$0, 200(%rbx)
	mfence
	cmpb	$0, _ZN2v88internal19FLAG_trace_unmapperE(%rip)
	jne	.L965
.L967:
	movq	192(%rbx), %rax
	jmp	.L960
	.p2align 4,,10
	.p2align 3
.L961:
	addq	$1, %r12
	cmpq	%r12, 192(%rbx)
	jg	.L964
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L965:
	movq	(%rbx), %rax
	leaq	.LC9(%rip), %rsi
	leaq	-37592(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal12PrintIsolateEPvPKcz@PLT
	jmp	.L967
	.cfi_endproc
.LFE22806:
	.size	_ZN2v88internal15MemoryAllocator8Unmapper19MakeRoomForNewTasksEv, .-_ZN2v88internal15MemoryAllocator8Unmapper19MakeRoomForNewTasksEv
	.section	.text._ZN2v88internal15MemoryAllocator8Unmapper23NumberOfCommittedChunksEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocator8Unmapper23NumberOfCommittedChunksEv
	.type	_ZN2v88internal15MemoryAllocator8Unmapper23NumberOfCommittedChunksEv, @function
_ZN2v88internal15MemoryAllocator8Unmapper23NumberOfCommittedChunksEv:
.LFB22810:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	16(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	88(%rbx), %rax
	movq	64(%rbx), %r12
	movq	%r13, %rdi
	subq	80(%rbx), %rax
	subq	56(%rbx), %r12
	sarq	$3, %rax
	sarq	$3, %r12
	addq	%rax, %r12
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22810:
	.size	_ZN2v88internal15MemoryAllocator8Unmapper23NumberOfCommittedChunksEv, .-_ZN2v88internal15MemoryAllocator8Unmapper23NumberOfCommittedChunksEv
	.section	.text._ZN2v88internal15MemoryAllocator8Unmapper14NumberOfChunksEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocator8Unmapper14NumberOfChunksEv
	.type	_ZN2v88internal15MemoryAllocator8Unmapper14NumberOfChunksEv, @function
_ZN2v88internal15MemoryAllocator8Unmapper14NumberOfChunksEv:
.LFB22811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	16(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	112(%r12), %rax
	movq	64(%r12), %rbx
	movq	%r13, %rdi
	subq	104(%r12), %rax
	subq	56(%r12), %rbx
	sarq	$3, %rbx
	sarq	$3, %rax
	leaq	(%rax,%rbx), %rax
	movq	88(%r12), %rbx
	subq	80(%r12), %rbx
	sarq	$3, %rbx
	addq	%rax, %rbx
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$8, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22811:
	.size	_ZN2v88internal15MemoryAllocator8Unmapper14NumberOfChunksEv, .-_ZN2v88internal15MemoryAllocator8Unmapper14NumberOfChunksEv
	.section	.text._ZN2v88internal15MemoryAllocator8Unmapper23CommittedBufferedMemoryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocator8Unmapper23CommittedBufferedMemoryEv
	.type	_ZN2v88internal15MemoryAllocator8Unmapper23CommittedBufferedMemoryEv, @function
_ZN2v88internal15MemoryAllocator8Unmapper23CommittedBufferedMemoryEv:
.LFB22812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	16(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	56(%rbx), %rax
	movq	64(%rbx), %rcx
	cmpq	%rcx, %rax
	je	.L974
	.p2align 4,,10
	.p2align 3
.L975:
	movq	(%rax), %rdx
	addq	$8, %rax
	addq	(%rdx), %r12
	cmpq	%rax, %rcx
	jne	.L975
.L974:
	movq	80(%rbx), %rax
	movq	88(%rbx), %rcx
	cmpq	%rax, %rcx
	je	.L976
	.p2align 4,,10
	.p2align 3
.L977:
	movq	(%rax), %rdx
	addq	$8, %rax
	addq	(%rdx), %r12
	cmpq	%rax, %rcx
	jne	.L977
.L976:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22812:
	.size	_ZN2v88internal15MemoryAllocator8Unmapper23CommittedBufferedMemoryEv, .-_ZN2v88internal15MemoryAllocator8Unmapper23CommittedBufferedMemoryEv
	.section	.text._ZN2v88internal15MemoryAllocator12CommitMemoryEPNS0_13VirtualMemoryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocator12CommitMemoryEPNS0_13VirtualMemoryE
	.type	_ZN2v88internal15MemoryAllocator12CommitMemoryEPNS0_13VirtualMemoryE, @function
_ZN2v88internal15MemoryAllocator12CommitMemoryEPNS0_13VirtualMemoryE:
.LFB22813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rsi), %r14
	movq	8(%rsi), %rbx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal13VirtualMemory14SetPermissionsEmmNS_13PageAllocator10PermissionE@PLT
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L1001
.L982:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1001:
	.cfi_restore_state
	leaq	(%rbx,%r14), %rcx
	leaq	96(%r13), %rdx
.L987:
	movq	(%rdx), %rax
	cmpq	%rax, %rbx
	jnb	.L986
	lock cmpxchgq	%rbx, (%rdx)
	jne	.L987
.L986:
	leaq	104(%r13), %rdx
.L985:
	movq	(%rdx), %rax
	cmpq	%rax, %rcx
	jbe	.L991
	lock cmpxchgq	%rcx, (%rdx)
	jne	.L985
.L991:
	movq	0(%r13), %rax
	movq	40960(%rax), %rbx
	cmpb	$0, 5952(%rbx)
	je	.L1002
	movq	5944(%rbx), %rax
.L992:
	testq	%rax, %rax
	je	.L982
	addl	%r14d, (%rax)
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1002:
	.cfi_restore_state
	movb	$1, 5952(%rbx)
	leaq	5928(%rbx), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 5944(%rbx)
	jmp	.L992
	.cfi_endproc
.LFE22813:
	.size	_ZN2v88internal15MemoryAllocator12CommitMemoryEPNS0_13VirtualMemoryE, .-_ZN2v88internal15MemoryAllocator12CommitMemoryEPNS0_13VirtualMemoryE
	.section	.text._ZN2v88internal15MemoryAllocator14UncommitMemoryEPNS0_13VirtualMemoryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocator14UncommitMemoryEPNS0_13VirtualMemoryE
	.type	_ZN2v88internal15MemoryAllocator14UncommitMemoryEPNS0_13VirtualMemoryE, @function
_ZN2v88internal15MemoryAllocator14UncommitMemoryEPNS0_13VirtualMemoryE:
.LFB22814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	16(%rsi), %r13
	movq	8(%rsi), %rsi
	movq	%r13, %rdx
	call	_ZN2v88internal13VirtualMemory14SetPermissionsEmmNS_13PageAllocator10PermissionE@PLT
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L1014
.L1003:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1014:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	40960(%rax), %rbx
	cmpb	$0, 5952(%rbx)
	je	.L1005
	movq	5944(%rbx), %rax
.L1006:
	testq	%rax, %rax
	je	.L1003
	subl	%r13d, (%rax)
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1005:
	.cfi_restore_state
	movb	$1, 5952(%rbx)
	leaq	5928(%rbx), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 5944(%rbx)
	jmp	.L1006
	.cfi_endproc
.LFE22814:
	.size	_ZN2v88internal15MemoryAllocator14UncommitMemoryEPNS0_13VirtualMemoryE, .-_ZN2v88internal15MemoryAllocator14UncommitMemoryEPNS0_13VirtualMemoryE
	.section	.rodata._ZN2v88internal15MemoryAllocator10FreeMemoryEPNS_13PageAllocatorEmm.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"FreePages(page_allocator, reinterpret_cast<void*>(base), size)"
	.section	.text._ZN2v88internal15MemoryAllocator10FreeMemoryEPNS_13PageAllocatorEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocator10FreeMemoryEPNS_13PageAllocatorEmm
	.type	_ZN2v88internal15MemoryAllocator10FreeMemoryEPNS_13PageAllocatorEmm, @function
_ZN2v88internal15MemoryAllocator10FreeMemoryEPNS_13PageAllocatorEmm:
.LFB22815:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal9FreePagesEPNS_13PageAllocatorEPvm@PLT
	testb	%al, %al
	je	.L1018
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1018:
	.cfi_restore_state
	leaq	.LC10(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22815:
	.size	_ZN2v88internal15MemoryAllocator10FreeMemoryEPNS_13PageAllocatorEmm, .-_ZN2v88internal15MemoryAllocator10FreeMemoryEPNS_13PageAllocatorEmm
	.section	.rodata._ZN2v88internal11MemoryChunk19DiscardUnusedMemoryEmm.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"page_allocator->DiscardSystemPages( reinterpret_cast<void*>(memory_area.begin()), memory_area.size())"
	.section	.text._ZN2v88internal11MemoryChunk19DiscardUnusedMemoryEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11MemoryChunk19DiscardUnusedMemoryEmm
	.type	_ZN2v88internal11MemoryChunk19DiscardUnusedMemoryEmm, @function
_ZN2v88internal11MemoryChunk19DiscardUnusedMemoryEmm:
.LFB22817:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	je	.L1020
	sall	$10, %eax
	movslq	%eax, %rcx
	movq	%rcx, %rax
.L1021:
	leaq	24(%rax), %rdx
	cmpq	%rdx, %r12
	jb	.L1019
	negq	%rcx
	leaq	23(%rax,%rbx), %rsi
	addq	%r12, %rbx
	andq	%rcx, %rsi
	andq	%rbx, %rcx
	cmpq	%rcx, %rsi
	jnb	.L1019
	subq	%rsi, %rcx
	movq	%rcx, %rdx
	je	.L1019
	movq	24(%r13), %rax
	movq	2048(%rax), %rax
	movq	32(%rax), %rdi
	testb	$1, 8(%r13)
	jne	.L1031
	movq	(%rdi), %rax
	call	*80(%rax)
	testb	%al, %al
	je	.L1032
.L1019:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1020:
	.cfi_restore_state
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movq	%rax, %rcx
	jmp	.L1021
	.p2align 4,,10
	.p2align 3
.L1031:
	movq	40(%rax), %rdi
	movq	(%rdi), %rax
	call	*80(%rax)
	testb	%al, %al
	jne	.L1019
.L1032:
	leaq	.LC11(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22817:
	.size	_ZN2v88internal11MemoryChunk19DiscardUnusedMemoryEmm, .-_ZN2v88internal11MemoryChunk19DiscardUnusedMemoryEmm
	.section	.text._ZN2v88internal17MemoryChunkLayout24CodePageGuardStartOffsetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17MemoryChunkLayout24CodePageGuardStartOffsetEv
	.type	_ZN2v88internal17MemoryChunkLayout24CodePageGuardStartOffsetEv, @function
_ZN2v88internal17MemoryChunkLayout24CodePageGuardStartOffsetEv:
.LFB22818:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	je	.L1034
	sall	$10, %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	negq	%rdx
	addq	$279, %rax
	andq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1034:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%rax, %rdx
	addq	$279, %rax
	negq	%rdx
	andq	%rdx, %rax
	ret
	.cfi_endproc
.LFE22818:
	.size	_ZN2v88internal17MemoryChunkLayout24CodePageGuardStartOffsetEv, .-_ZN2v88internal17MemoryChunkLayout24CodePageGuardStartOffsetEv
	.section	.text._ZN2v88internal17MemoryChunkLayout17CodePageGuardSizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17MemoryChunkLayout17CodePageGuardSizeEv
	.type	_ZN2v88internal17MemoryChunkLayout17CodePageGuardSizeEv, @function
_ZN2v88internal17MemoryChunkLayout17CodePageGuardSizeEv:
.LFB22819:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	je	.L1040
	sall	$10, %eax
	cltq
	ret
	.p2align 4,,10
	.p2align 3
.L1040:
	jmp	_ZN2v88internal14CommitPageSizeEv@PLT
	.cfi_endproc
.LFE22819:
	.size	_ZN2v88internal17MemoryChunkLayout17CodePageGuardSizeEv, .-_ZN2v88internal17MemoryChunkLayout17CodePageGuardSizeEv
	.section	.text._ZN2v88internal17MemoryChunkLayout27ObjectStartOffsetInCodePageEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17MemoryChunkLayout27ObjectStartOffsetInCodePageEv
	.type	_ZN2v88internal17MemoryChunkLayout27ObjectStartOffsetInCodePageEv, @function
_ZN2v88internal17MemoryChunkLayout27ObjectStartOffsetInCodePageEv:
.LFB22820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	je	.L1044
	sall	$10, %eax
	cltq
	movq	%rax, %rdx
	leaq	279(%rax), %rbx
	negq	%rdx
	andq	%rdx, %rbx
.L1047:
	addq	$8, %rsp
	addq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1044:
	.cfi_restore_state
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %edx
	movq	%rax, %rbx
	leaq	279(%rax), %rax
	negq	%rbx
	andq	%rax, %rbx
	testl	%edx, %edx
	jne	.L1049
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	addq	$8, %rsp
	addq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1049:
	.cfi_restore_state
	sall	$10, %edx
	movslq	%edx, %rax
	jmp	.L1047
	.cfi_endproc
.LFE22820:
	.size	_ZN2v88internal17MemoryChunkLayout27ObjectStartOffsetInCodePageEv, .-_ZN2v88internal17MemoryChunkLayout27ObjectStartOffsetInCodePageEv
	.section	.text._ZN2v88internal17MemoryChunkLayout25ObjectEndOffsetInCodePageEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17MemoryChunkLayout25ObjectEndOffsetInCodePageEv
	.type	_ZN2v88internal17MemoryChunkLayout25ObjectEndOffsetInCodePageEv, @function
_ZN2v88internal17MemoryChunkLayout25ObjectEndOffsetInCodePageEv:
.LFB22821:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	je	.L1051
	sall	$10, %eax
	movl	%eax, %edx
	movl	$262144, %eax
	subl	%edx, %eax
	cltq
	ret
	.p2align 4,,10
	.p2align 3
.L1051:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%eax, %edx
	movl	$262144, %eax
	subl	%edx, %eax
	cltq
	ret
	.cfi_endproc
.LFE22821:
	.size	_ZN2v88internal17MemoryChunkLayout25ObjectEndOffsetInCodePageEv, .-_ZN2v88internal17MemoryChunkLayout25ObjectEndOffsetInCodePageEv
	.section	.text._ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInCodePageEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInCodePageEv
	.type	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInCodePageEv, @function
_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInCodePageEv:
.LFB22822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testl	%eax, %eax
	je	.L1057
	sall	$10, %eax
	movl	$262144, %ebx
	movslq	%eax, %rcx
	subl	%eax, %ebx
	movq	%rcx, %rdx
	movslq	%ebx, %rbx
.L1058:
	movq	%rcx, %rax
	leaq	279(%rcx), %r12
	negq	%rax
	andq	%rax, %r12
.L1060:
	movq	%rdx, %rax
	addq	%r12, %rax
	subq	%rax, %rbx
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1057:
	.cfi_restore_state
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movl	$262144, %ebx
	subl	%eax, %ebx
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	movslq	%ebx, %rbx
	testl	%eax, %eax
	jne	.L1064
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movq	%rax, %r12
	leaq	279(%rax), %rax
	negq	%r12
	andq	%rax, %r12
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	jne	.L1065
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	addq	%r12, %rax
	subq	%rax, %rbx
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1064:
	.cfi_restore_state
	sall	$10, %eax
	movslq	%eax, %rcx
	movq	%rcx, %rdx
	jmp	.L1058
.L1065:
	sall	$10, %eax
	movslq	%eax, %rdx
	jmp	.L1060
	.cfi_endproc
.LFE22822:
	.size	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInCodePageEv, .-_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInCodePageEv
	.section	.text._ZN2v88internal17MemoryChunkLayout27ObjectStartOffsetInDataPageEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17MemoryChunkLayout27ObjectStartOffsetInDataPageEv
	.type	_ZN2v88internal17MemoryChunkLayout27ObjectStartOffsetInDataPageEv, @function
_ZN2v88internal17MemoryChunkLayout27ObjectStartOffsetInDataPageEv:
.LFB22823:
	.cfi_startproc
	endbr64
	movl	$280, %eax
	ret
	.cfi_endproc
.LFE22823:
	.size	_ZN2v88internal17MemoryChunkLayout27ObjectStartOffsetInDataPageEv, .-_ZN2v88internal17MemoryChunkLayout27ObjectStartOffsetInDataPageEv
	.section	.text._ZN2v88internal17MemoryChunkLayout30ObjectStartOffsetInMemoryChunkENS0_15AllocationSpaceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17MemoryChunkLayout30ObjectStartOffsetInMemoryChunkENS0_15AllocationSpaceE
	.type	_ZN2v88internal17MemoryChunkLayout30ObjectStartOffsetInMemoryChunkENS0_15AllocationSpaceE, @function
_ZN2v88internal17MemoryChunkLayout30ObjectStartOffsetInMemoryChunkENS0_15AllocationSpaceE:
.LFB22824:
	.cfi_startproc
	endbr64
	cmpl	$3, %edi
	je	.L1079
	movl	$280, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1079:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	je	.L1069
	sall	$10, %eax
	movslq	%eax, %rdx
	movq	%rdx, %rbx
	leaq	279(%rdx), %rax
	negq	%rbx
	andq	%rax, %rbx
.L1072:
	addq	$8, %rsp
	leaq	(%rbx,%rdx), %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1069:
	.cfi_restore_state
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movq	%rax, %rbx
	leaq	279(%rax), %rax
	negq	%rbx
	andq	%rax, %rbx
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	jne	.L1080
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	addq	$8, %rsp
	movq	%rax, %rdx
	leaq	(%rbx,%rdx), %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1080:
	.cfi_restore_state
	sall	$10, %eax
	movslq	%eax, %rdx
	jmp	.L1072
	.cfi_endproc
.LFE22824:
	.size	_ZN2v88internal17MemoryChunkLayout30ObjectStartOffsetInMemoryChunkENS0_15AllocationSpaceE, .-_ZN2v88internal17MemoryChunkLayout30ObjectStartOffsetInMemoryChunkENS0_15AllocationSpaceE
	.section	.text._ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv
	.type	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv, @function
_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv:
.LFB22825:
	.cfi_startproc
	endbr64
	movl	$261864, %eax
	ret
	.cfi_endproc
.LFE22825:
	.size	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv, .-_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv
	.section	.text._ZN2v88internal17MemoryChunkLayout30AllocatableMemoryInMemoryChunkENS0_15AllocationSpaceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17MemoryChunkLayout30AllocatableMemoryInMemoryChunkENS0_15AllocationSpaceE
	.type	_ZN2v88internal17MemoryChunkLayout30AllocatableMemoryInMemoryChunkENS0_15AllocationSpaceE, @function
_ZN2v88internal17MemoryChunkLayout30AllocatableMemoryInMemoryChunkENS0_15AllocationSpaceE:
.LFB22826:
	.cfi_startproc
	endbr64
	cmpl	$3, %edi
	je	.L1096
	movl	$261864, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1096:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testl	%eax, %eax
	je	.L1084
	sall	$10, %eax
	movl	$262144, %ebx
	movslq	%eax, %rcx
	subl	%eax, %ebx
	movq	%rcx, %rdx
	movslq	%ebx, %rbx
.L1085:
	movq	%rcx, %rax
	leaq	279(%rcx), %r12
	negq	%rax
	andq	%rax, %r12
.L1087:
	movq	%rdx, %rax
	addq	%r12, %rax
	subq	%rax, %rbx
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1084:
	.cfi_restore_state
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movq	%rax, %r8
	movl	$262144, %eax
	subl	%r8d, %eax
	movslq	%eax, %rbx
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	jne	.L1097
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movq	%rax, %r12
	leaq	279(%rax), %rax
	negq	%r12
	andq	%rax, %r12
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	jne	.L1098
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	addq	%r12, %rax
	subq	%rax, %rbx
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1097:
	.cfi_restore_state
	sall	$10, %eax
	movslq	%eax, %rcx
	movq	%rcx, %rdx
	jmp	.L1085
.L1098:
	sall	$10, %eax
	movslq	%eax, %rdx
	jmp	.L1087
	.cfi_endproc
.LFE22826:
	.size	_ZN2v88internal17MemoryChunkLayout30AllocatableMemoryInMemoryChunkENS0_15AllocationSpaceE, .-_ZN2v88internal17MemoryChunkLayout30AllocatableMemoryInMemoryChunkENS0_15AllocationSpaceE
	.section	.text._ZN2v88internal11MemoryChunk25InitializationMemoryFenceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11MemoryChunk25InitializationMemoryFenceEv
	.type	_ZN2v88internal11MemoryChunk25InitializationMemoryFenceEv, @function
_ZN2v88internal11MemoryChunk25InitializationMemoryFenceEv:
.LFB22827:
	.cfi_startproc
	endbr64
	mfence
	ret
	.cfi_endproc
.LFE22827:
	.size	_ZN2v88internal11MemoryChunk25InitializationMemoryFenceEv, .-_ZN2v88internal11MemoryChunk25InitializationMemoryFenceEv
	.section	.rodata._ZN2v88internal11MemoryChunk52DecrementWriteUnprotectCounterAndMaybeSetPermissionsENS_13PageAllocator10PermissionE.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"reservation_.SetPermissions(protect_start, protect_size, permission)"
	.section	.text._ZN2v88internal11MemoryChunk52DecrementWriteUnprotectCounterAndMaybeSetPermissionsENS_13PageAllocator10PermissionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11MemoryChunk52DecrementWriteUnprotectCounterAndMaybeSetPermissionsENS_13PageAllocator10PermissionE
	.type	_ZN2v88internal11MemoryChunk52DecrementWriteUnprotectCounterAndMaybeSetPermissionsENS_13PageAllocator10PermissionE, @function
_ZN2v88internal11MemoryChunk52DecrementWriteUnprotectCounterAndMaybeSetPermissionsENS_13PageAllocator10PermissionE:
.LFB22828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	176(%rdi), %r13
	movq	%rdi, %rbx
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	184(%rbx), %rax
	testq	%rax, %rax
	je	.L1102
	subq	$1, %rax
	movq	%rax, 184(%rbx)
	jne	.L1102
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %esi
	testl	%esi, %esi
	je	.L1103
	sall	$10, %esi
	movslq	%esi, %rsi
	movq	%rsi, %rdi
	leaq	279(%rsi), %r14
	movq	%rsi, %rax
	negq	%rdi
	andq	%rdi, %r14
.L1104:
	addq	%rsi, %r14
	addq	%rbx, %r14
.L1108:
	movq	48(%rbx), %rcx
	movq	%r14, %rsi
	leaq	-1(%rcx), %rdx
	movq	%rdx, %rcx
	subq	40(%rbx), %rcx
	leaq	(%rcx,%rax), %rdx
	movl	%r12d, %ecx
	andq	%rdi, %rdx
	leaq	56(%rbx), %rdi
	call	_ZN2v88internal13VirtualMemory14SetPermissionsEmmNS_13PageAllocator10PermissionE@PLT
	testb	%al, %al
	je	.L1110
.L1102:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L1103:
	.cfi_restore_state
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	leaq	279(%rax), %r14
	negq	%rax
	andq	%rax, %r14
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	jne	.L1111
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	addq	%rbx, %r14
	addq	%rax, %r14
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	jne	.L1112
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movq	%rax, %rdi
	negq	%rdi
	jmp	.L1108
	.p2align 4,,10
	.p2align 3
.L1110:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L1112:
	sall	$10, %eax
	cltq
	movq	%rax, %rdi
	negq	%rdi
	jmp	.L1108
.L1111:
	sall	$10, %eax
	movslq	%eax, %rsi
	movq	%rsi, %rdi
	movq	%rsi, %rax
	negq	%rdi
	jmp	.L1104
	.cfi_endproc
.LFE22828:
	.size	_ZN2v88internal11MemoryChunk52DecrementWriteUnprotectCounterAndMaybeSetPermissionsENS_13PageAllocator10PermissionE, .-_ZN2v88internal11MemoryChunk52DecrementWriteUnprotectCounterAndMaybeSetPermissionsENS_13PageAllocator10PermissionE
	.section	.text._ZN2v88internal11MemoryChunk11SetReadableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11MemoryChunk11SetReadableEv
	.type	_ZN2v88internal11MemoryChunk11SetReadableEv, @function
_ZN2v88internal11MemoryChunk11SetReadableEv:
.LFB22829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	176(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	184(%rbx), %rax
	testq	%rax, %rax
	je	.L1115
	subq	$1, %rax
	movq	%rax, 184(%rbx)
	jne	.L1115
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %esi
	testl	%esi, %esi
	je	.L1116
	sall	$10, %esi
	movslq	%esi, %rsi
	movq	%rsi, %rdi
	leaq	279(%rsi), %r13
	movq	%rsi, %rax
	negq	%rdi
	andq	%rdi, %r13
.L1117:
	addq	%rsi, %r13
	addq	%rbx, %r13
.L1121:
	movq	48(%rbx), %rcx
	movq	%r13, %rsi
	leaq	-1(%rcx), %rdx
	movq	%rdx, %rcx
	subq	40(%rbx), %rcx
	leaq	(%rcx,%rax), %rdx
	movl	$1, %ecx
	andq	%rdi, %rdx
	leaq	56(%rbx), %rdi
	call	_ZN2v88internal13VirtualMemory14SetPermissionsEmmNS_13PageAllocator10PermissionE@PLT
	testb	%al, %al
	je	.L1123
.L1115:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L1116:
	.cfi_restore_state
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	leaq	279(%rax), %r13
	negq	%rax
	andq	%rax, %r13
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	jne	.L1124
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	addq	%rbx, %r13
	addq	%rax, %r13
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	jne	.L1125
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movq	%rax, %rdi
	negq	%rdi
	jmp	.L1121
	.p2align 4,,10
	.p2align 3
.L1123:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L1125:
	sall	$10, %eax
	cltq
	movq	%rax, %rdi
	negq	%rdi
	jmp	.L1121
.L1124:
	sall	$10, %eax
	movslq	%eax, %rsi
	movq	%rsi, %rdi
	movq	%rsi, %rax
	negq	%rdi
	jmp	.L1117
	.cfi_endproc
.LFE22829:
	.size	_ZN2v88internal11MemoryChunk11SetReadableEv, .-_ZN2v88internal11MemoryChunk11SetReadableEv
	.section	.text._ZN2v88internal11MemoryChunk20SetReadAndExecutableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11MemoryChunk20SetReadAndExecutableEv
	.type	_ZN2v88internal11MemoryChunk20SetReadAndExecutableEv, @function
_ZN2v88internal11MemoryChunk20SetReadAndExecutableEv:
.LFB22830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	176(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	184(%rbx), %rax
	testq	%rax, %rax
	je	.L1128
	subq	$1, %rax
	movq	%rax, 184(%rbx)
	jne	.L1128
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %esi
	testl	%esi, %esi
	je	.L1129
	sall	$10, %esi
	movslq	%esi, %rsi
	movq	%rsi, %rdi
	leaq	279(%rsi), %r13
	movq	%rsi, %rax
	negq	%rdi
	andq	%rdi, %r13
.L1130:
	addq	%rsi, %r13
	addq	%rbx, %r13
.L1134:
	movq	48(%rbx), %rcx
	movq	%r13, %rsi
	leaq	-1(%rcx), %rdx
	movq	%rdx, %rcx
	subq	40(%rbx), %rcx
	leaq	(%rcx,%rax), %rdx
	movl	$4, %ecx
	andq	%rdi, %rdx
	leaq	56(%rbx), %rdi
	call	_ZN2v88internal13VirtualMemory14SetPermissionsEmmNS_13PageAllocator10PermissionE@PLT
	testb	%al, %al
	je	.L1136
.L1128:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L1129:
	.cfi_restore_state
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	leaq	279(%rax), %r13
	negq	%rax
	andq	%rax, %r13
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	jne	.L1137
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	addq	%rbx, %r13
	addq	%rax, %r13
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	jne	.L1138
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movq	%rax, %rdi
	negq	%rdi
	jmp	.L1134
	.p2align 4,,10
	.p2align 3
.L1136:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L1138:
	sall	$10, %eax
	cltq
	movq	%rax, %rdi
	negq	%rdi
	jmp	.L1134
.L1137:
	sall	$10, %eax
	movslq	%eax, %rsi
	movq	%rsi, %rdi
	movq	%rsi, %rax
	negq	%rdi
	jmp	.L1130
	.cfi_endproc
.LFE22830:
	.size	_ZN2v88internal11MemoryChunk20SetReadAndExecutableEv, .-_ZN2v88internal11MemoryChunk20SetReadAndExecutableEv
	.section	.rodata._ZN2v88internal11MemoryChunk18SetReadAndWritableEv.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"reservation_.SetPermissions(unprotect_start, unprotect_size, PageAllocator::kReadWrite)"
	.section	.text._ZN2v88internal11MemoryChunk18SetReadAndWritableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11MemoryChunk18SetReadAndWritableEv
	.type	_ZN2v88internal11MemoryChunk18SetReadAndWritableEv, @function
_ZN2v88internal11MemoryChunk18SetReadAndWritableEv:
.LFB22831:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	176(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	184(%rbx), %rax
	addq	$1, %rax
	movq	%rax, 184(%rbx)
	cmpq	$1, %rax
	je	.L1148
.L1140:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L1148:
	.cfi_restore_state
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %esi
	testl	%esi, %esi
	je	.L1141
	sall	$10, %esi
	movslq	%esi, %rsi
	movq	%rsi, %rdi
	leaq	279(%rsi), %r13
	movq	%rsi, %rax
	negq	%rdi
	andq	%rdi, %r13
.L1142:
	addq	%rsi, %r13
	addq	%rbx, %r13
.L1146:
	movq	48(%rbx), %rcx
	movq	%r13, %rsi
	leaq	-1(%rcx), %rdx
	movq	%rdx, %rcx
	subq	40(%rbx), %rcx
	leaq	(%rcx,%rax), %rdx
	movl	$2, %ecx
	andq	%rdi, %rdx
	leaq	56(%rbx), %rdi
	call	_ZN2v88internal13VirtualMemory14SetPermissionsEmmNS_13PageAllocator10PermissionE@PLT
	testb	%al, %al
	jne	.L1140
	leaq	.LC13(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1141:
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	leaq	279(%rax), %r13
	negq	%rax
	andq	%rax, %r13
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	jne	.L1149
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	addq	%rbx, %r13
	addq	%rax, %r13
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	jne	.L1150
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movq	%rax, %rdi
	negq	%rdi
	jmp	.L1146
.L1150:
	sall	$10, %eax
	cltq
	movq	%rax, %rdi
	negq	%rdi
	jmp	.L1146
.L1149:
	sall	$10, %eax
	movslq	%eax, %rsi
	movq	%rsi, %rdi
	movq	%rsi, %rax
	negq	%rdi
	jmp	.L1142
	.cfi_endproc
.LFE22831:
	.size	_ZN2v88internal11MemoryChunk18SetReadAndWritableEv, .-_ZN2v88internal11MemoryChunk18SetReadAndWritableEv
	.section	.text._ZN2v88internal18CodeObjectRegistry32RegisterNewlyAllocatedCodeObjectEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18CodeObjectRegistry32RegisterNewlyAllocatedCodeObjectEm
	.type	_ZN2v88internal18CodeObjectRegistry32RegisterNewlyAllocatedCodeObjectEm, @function
_ZN2v88internal18CodeObjectRegistry32RegisterNewlyAllocatedCodeObjectEm:
.LFB22832:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	32(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	40(%rdi), %r12
	testq	%r12, %r12
	jne	.L1153
	jmp	.L1170
	.p2align 4,,10
	.p2align 3
.L1171:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L1154
.L1172:
	movq	%rax, %r12
.L1153:
	movq	32(%r12), %rcx
	cmpq	%r13, %rcx
	ja	.L1171
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L1172
.L1154:
	testb	%dl, %dl
	jne	.L1173
	cmpq	%r13, %rcx
	jnb	.L1151
.L1161:
	movl	$1, %r15d
	cmpq	%r12, %r14
	jne	.L1174
.L1160:
	movl	$40, %edi
	call	_Znwm@PLT
	movq	%r14, %rcx
	movq	%r12, %rdx
	movl	%r15d, %edi
	movq	%r13, 32(%rax)
	movq	%rax, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 64(%rbx)
.L1151:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1173:
	.cfi_restore_state
	cmpq	%r12, 48(%rbx)
	je	.L1161
.L1162:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	cmpq	%r13, 32(%rax)
	jnb	.L1151
	testq	%r12, %r12
	je	.L1151
	movl	$1, %r15d
	cmpq	%r12, %r14
	je	.L1160
.L1174:
	xorl	%r15d, %r15d
	cmpq	%r13, 32(%r12)
	seta	%r15b
	jmp	.L1160
	.p2align 4,,10
	.p2align 3
.L1170:
	movq	%r14, %r12
	cmpq	48(%rdi), %r14
	jne	.L1162
	movl	$1, %r15d
	jmp	.L1160
	.cfi_endproc
.LFE22832:
	.size	_ZN2v88internal18CodeObjectRegistry32RegisterNewlyAllocatedCodeObjectEm, .-_ZN2v88internal18CodeObjectRegistry32RegisterNewlyAllocatedCodeObjectEm
	.section	.rodata._ZN2v88internal18CodeObjectRegistry33RegisterAlreadyExistingCodeObjectEm.str1.1,"aMS",@progbits,1
.LC14:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal18CodeObjectRegistry33RegisterAlreadyExistingCodeObjectEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18CodeObjectRegistry33RegisterAlreadyExistingCodeObjectEm
	.type	_ZN2v88internal18CodeObjectRegistry33RegisterAlreadyExistingCodeObjectEm, @function
_ZN2v88internal18CodeObjectRegistry33RegisterAlreadyExistingCodeObjectEm:
.LFB22837:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rdx
	cmpq	16(%rdi), %rdx
	je	.L1176
	movq	%rsi, (%rdx)
	addq	$8, 8(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1176:
	.cfi_restore_state
	movq	(%rdi), %r15
	subq	%r15, %rdx
	movq	%rdx, %rax
	movq	%rdx, %r12
	movabsq	$1152921504606846975, %rdx
	sarq	$3, %rax
	cmpq	%rdx, %rax
	je	.L1191
	testq	%rax, %rax
	je	.L1184
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L1192
.L1179:
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	addq	%rax, %r14
.L1180:
	leaq	8(%r13,%r12), %rax
	movq	%rsi, 0(%r13,%r12)
	movq	%rax, -56(%rbp)
	testq	%r12, %r12
	jg	.L1193
	testq	%r15, %r15
	jne	.L1182
.L1183:
	movq	%r13, %xmm0
	movq	%r14, 16(%rbx)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1193:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	memmove@PLT
.L1182:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L1183
	.p2align 4,,10
	.p2align 3
.L1192:
	testq	%rcx, %rcx
	jne	.L1194
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmp	.L1180
	.p2align 4,,10
	.p2align 3
.L1184:
	movl	$8, %r14d
	jmp	.L1179
.L1191:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1194:
	cmpq	%rdx, %rcx
	cmovbe	%rcx, %rdx
	leaq	0(,%rdx,8), %r14
	jmp	.L1179
	.cfi_endproc
.LFE22837:
	.size	_ZN2v88internal18CodeObjectRegistry33RegisterAlreadyExistingCodeObjectEm, .-_ZN2v88internal18CodeObjectRegistry33RegisterAlreadyExistingCodeObjectEm
	.section	.text._ZN2v88internal18CodeObjectRegistry8FinalizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18CodeObjectRegistry8FinalizeEv
	.type	_ZN2v88internal18CodeObjectRegistry8FinalizeEv, @function
_ZN2v88internal18CodeObjectRegistry8FinalizeEv:
.LFB22839:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE22839:
	.size	_ZN2v88internal18CodeObjectRegistry8FinalizeEv, .-_ZN2v88internal18CodeObjectRegistry8FinalizeEv
	.section	.text._ZNK2v88internal18CodeObjectRegistry8ContainsEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal18CodeObjectRegistry8ContainsEm
	.type	_ZNK2v88internal18CodeObjectRegistry8ContainsEm, @function
_ZNK2v88internal18CodeObjectRegistry8ContainsEm:
.LFB22840:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	leaq	32(%rdi), %rcx
	testq	%rax, %rax
	je	.L1197
	movq	%rcx, %rdx
	jmp	.L1198
	.p2align 4,,10
	.p2align 3
.L1213:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1199
.L1198:
	cmpq	%rsi, 32(%rax)
	jnb	.L1213
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1198
.L1199:
	cmpq	%rcx, %rdx
	je	.L1197
	movl	$1, %eax
	cmpq	%rsi, 32(%rdx)
	jbe	.L1196
.L1197:
	movq	8(%rdi), %r8
	movq	(%rdi), %rdi
	movq	%r8, %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	.p2align 4,,10
	.p2align 3
.L1204:
	testq	%rax, %rax
	jle	.L1203
.L1214:
	movq	%rax, %rdx
	sarq	%rdx
	leaq	(%rdi,%rdx,8), %rcx
	cmpq	%rsi, (%rcx)
	jnb	.L1209
	subq	%rdx, %rax
	leaq	8(%rcx), %rdi
	subq	$1, %rax
	testq	%rax, %rax
	jg	.L1214
.L1203:
	cmpq	%rdi, %r8
	je	.L1207
	cmpq	%rsi, (%rdi)
	ja	.L1207
	movl	$1, %eax
.L1196:
	ret
	.p2align 4,,10
	.p2align 3
.L1209:
	movq	%rdx, %rax
	jmp	.L1204
	.p2align 4,,10
	.p2align 3
.L1207:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22840:
	.size	_ZNK2v88internal18CodeObjectRegistry8ContainsEm, .-_ZNK2v88internal18CodeObjectRegistry8ContainsEm
	.section	.text._ZNK2v88internal18CodeObjectRegistry34GetCodeObjectStartFromInnerAddressEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal18CodeObjectRegistry34GetCodeObjectStartFromInnerAddressEm
	.type	_ZNK2v88internal18CodeObjectRegistry34GetCodeObjectStartFromInnerAddressEm, @function
_ZNK2v88internal18CodeObjectRegistry34GetCodeObjectStartFromInnerAddressEm:
.LFB22841:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movq	(%rdi), %r9
	cmpq	%rax, %r9
	je	.L1216
	subq	%r9, %rax
	movq	%r9, %r8
	sarq	$3, %rax
	.p2align 4,,10
	.p2align 3
.L1218:
	testq	%rax, %rax
	jle	.L1217
.L1235:
	movq	%rax, %rdx
	sarq	%rdx
	leaq	(%r8,%rdx,8), %rcx
	cmpq	%rsi, (%rcx)
	ja	.L1226
	subq	%rdx, %rax
	leaq	8(%rcx), %r8
	subq	$1, %rax
	testq	%rax, %rax
	jg	.L1235
.L1217:
	xorl	%r12d, %r12d
	cmpq	%r8, %r9
	je	.L1216
	movq	-8(%r8), %r12
.L1216:
	cmpq	$0, 64(%rdi)
	je	.L1215
	movq	40(%rdi), %rax
	leaq	32(%rdi), %r8
	testq	%rax, %rax
	jne	.L1222
	jmp	.L1221
	.p2align 4,,10
	.p2align 3
.L1236:
	movq	%rax, %r8
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1221
.L1222:
	cmpq	%rsi, 32(%rax)
	ja	.L1236
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1222
.L1221:
	cmpq	%r8, 48(%rdi)
	je	.L1215
	movq	%r8, %rdi
	call	_ZSt18_Rb_tree_decrementPKSt18_Rb_tree_node_base@PLT
	movq	32(%rax), %rax
	cmpq	%rax, %r12
	cmovb	%rax, %r12
.L1215:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1226:
	.cfi_restore_state
	movq	%rdx, %rax
	jmp	.L1218
	.cfi_endproc
.LFE22841:
	.size	_ZNK2v88internal18CodeObjectRegistry34GetCodeObjectStartFromInnerAddressEm, .-_ZNK2v88internal18CodeObjectRegistry34GetCodeObjectStartFromInnerAddressEm
	.section	.rodata._ZN2v88internal11MemoryChunk10InitializeEPNS0_4HeapEmmmmNS0_13ExecutabilityEPNS0_5SpaceENS0_13VirtualMemoryE.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"reservation.SetPermissions(area_start, area_size, DefaultWritableCodePermissions())"
	.section	.text._ZN2v88internal11MemoryChunk10InitializeEPNS0_4HeapEmmmmNS0_13ExecutabilityEPNS0_5SpaceENS0_13VirtualMemoryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11MemoryChunk10InitializeEPNS0_4HeapEmmmmNS0_13ExecutabilityEPNS0_5SpaceENS0_13VirtualMemoryE
	.type	_ZN2v88internal11MemoryChunk10InitializeEPNS0_4HeapEmmmmNS0_13ExecutabilityEPNS0_5SpaceENS0_13VirtualMemoryE, @function
_ZN2v88internal11MemoryChunk10InitializeEPNS0_4HeapEmmmmNS0_13ExecutabilityEPNS0_5SpaceENS0_13VirtualMemoryE:
.LFB22843:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movq	%rdx, %rsi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r15, %r12
	movq	%r13, %rdx
	pushq	%rbx
	andq	$-262144, %r12
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%r8, -72(%rbp)
	movq	24(%rbp), %r14
	movl	%r9d, -56(%rbp)
	call	_ZN2v88internal16BasicMemoryChunkC1Emmm@PLT
	movq	%rbx, 24(%r12)
	leaq	56(%r12), %rdi
	movq	16(%rbp), %rax
	movq	%rax, 80(%r12)
	mfence
	call	_ZN2v88internal13VirtualMemory5ResetEv@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rax
	movl	$40, %edi
	subq	%r15, %rax
	movq	$0, 104(%r12)
	movq	$0, 112(%r12)
	movq	$0, 120(%r12)
	movq	$0, 128(%r12)
	movups	%xmm0, 136(%r12)
	movq	$0, 88(%r12)
	mfence
	movq	%rax, 152(%r12)
	mfence
	movq	$0, 168(%r12)
	mfence
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v84base5MutexC1Ev@PLT
	movq	-64(%rbp), %rax
	movl	$40, %edi
	movq	$0, 184(%r12)
	movq	%rax, 176(%r12)
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v84base5MutexC1Ev@PLT
	movq	-64(%rbp), %rax
	movq	$0, 216(%r12)
	movq	$0, 264(%r12)
	movq	%rax, 160(%r12)
	movq	48(%r12), %rax
	subq	40(%r12), %rax
	movq	$0, 248(%r12)
	movq	%rax, 192(%r12)
	movq	$0, 200(%r12)
	mfence
	movq	$0, 208(%r12)
	mfence
	movq	$0, 240(%r12)
	movq	$0, 96(%r12)
	movq	16(%rbp), %rax
	movq	-72(%rbp), %r8
	movl	72(%rax), %eax
	testl	%eax, %eax
	jne	.L1238
	movq	16(%r12), %rax
	leaq	8(%rax), %rdi
	movq	$-1, (%rax)
	movq	$-1, 4088(%rax)
	andq	$-8, %rdi
	subq	%rdi, %rax
	leal	4096(%rax), %ecx
	movq	$-1, %rax
	shrl	$3, %ecx
	rep stosq
	orq	$2097152, 8(%r12)
.L1238:
	cmpl	$1, -56(%rbp)
	jne	.L1241
	orq	$1, 8(%r12)
	cmpb	$0, 376(%rbx)
	je	.L1240
	movq	384(%rbx), %rax
	movq	%rax, 184(%r12)
.L1241:
	movq	(%r14), %rax
	movdqu	8(%r14), %xmm1
	movq	%r14, %rdi
	movq	%rax, 56(%r12)
	movups	%xmm1, 64(%r12)
	call	_ZN2v88internal13VirtualMemory5ResetEv@PLT
	movq	16(%rbp), %rax
	cmpl	$3, 72(%rax)
	je	.L1249
	movq	$0, 272(%r12)
.L1237:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1240:
	.cfi_restore_state
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	je	.L1242
	sall	$10, %eax
	movslq	%eax, %rcx
	movq	%rcx, %rax
.L1243:
	leaq	-1(%r8), %rdx
	negq	%rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	subq	%r13, %rdx
	leaq	(%rdx,%rax), %rdx
	andq	%rcx, %rdx
	xorl	%ecx, %ecx
	cmpb	$0, _ZN2v88internal12FLAG_jitlessE(%rip)
	sete	%cl
	addl	$2, %ecx
	call	_ZN2v88internal13VirtualMemory14SetPermissionsEmmNS_13PageAllocator10PermissionE@PLT
	testb	%al, %al
	jne	.L1241
	leaq	.LC15(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1242:
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rcx
	jmp	.L1243
	.p2align 4,,10
	.p2align 3
.L1249:
	movl	$72, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movups	%xmm0, 32(%rax)
	leaq	32(%rax), %rdx
	movups	%xmm0, 16(%rax)
	pxor	%xmm0, %xmm0
	movq	$0, 64(%rax)
	movq	$0, 40(%rax)
	movq	%rdx, 48(%rax)
	movq	%rdx, 56(%rax)
	movq	%rax, 272(%r12)
	movups	%xmm0, (%rax)
	jmp	.L1237
	.cfi_endproc
.LFE22843:
	.size	_ZN2v88internal11MemoryChunk10InitializeEPNS0_4HeapEmmmmNS0_13ExecutabilityEPNS0_5SpaceENS0_13VirtualMemoryE, .-_ZN2v88internal11MemoryChunk10InitializeEPNS0_4HeapEmmmmNS0_13ExecutabilityEPNS0_5SpaceENS0_13VirtualMemoryE
	.section	.text._ZN2v88internal9SemiSpace14InitializePageEPNS0_11MemoryChunkE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9SemiSpace14InitializePageEPNS0_11MemoryChunkE
	.type	_ZN2v88internal9SemiSpace14InitializePageEPNS0_11MemoryChunkE, @function
_ZN2v88internal9SemiSpace14InitializePageEPNS0_11MemoryChunkE:
.LFB22869:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	cmpl	$1, 140(%rdi)
	sbbq	%rax, %rax
	andq	$-8, %rax
	addq	$16, %rax
	orq	8(%rsi), %rax
	movq	%rax, 8(%rsi)
	movq	64(%rdi), %rdx
	movq	2064(%rdx), %rdx
	cmpl	$1, 80(%rdx)
	jle	.L1252
	orq	$262150, %rax
.L1253:
	movq	%rax, 8(%r12)
	movl	$64, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	cmpb	$0, _ZN2v88internal13FLAG_minor_mcE(%rip)
	leaq	56(%rax), %rdx
	movq	%r12, (%rax)
	movq	%rdx, 8(%rax)
	movq	$1, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movl	$0x3f800000, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	%rax, 248(%r12)
	movups	%xmm0, 224(%r12)
	jne	.L1257
	mfence
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1252:
	.cfi_restore_state
	andq	$-262149, %rax
	orq	$2, %rax
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1257:
	movl	$4096, %esi
	movl	$1, %edi
	call	calloc@PLT
	movq	%rax, 264(%r12)
	movq	%r12, %rax
	movq	$0, 256(%r12)
	mfence
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22869:
	.size	_ZN2v88internal9SemiSpace14InitializePageEPNS0_11MemoryChunkE, .-_ZN2v88internal9SemiSpace14InitializePageEPNS0_11MemoryChunkE
	.section	.rodata._ZN2v88internal9LargePage10InitializeEPNS0_4HeapEPNS0_11MemoryChunkENS0_13ExecutabilityE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"Code page is too large."
	.section	.text._ZN2v88internal9LargePage10InitializeEPNS0_4HeapEPNS0_11MemoryChunkENS0_13ExecutabilityE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9LargePage10InitializeEPNS0_4HeapEPNS0_11MemoryChunkENS0_13ExecutabilityE
	.type	_ZN2v88internal9LargePage10InitializeEPNS0_4HeapEPNS0_11MemoryChunkENS0_13ExecutabilityE, @function
_ZN2v88internal9LargePage10InitializeEPNS0_4HeapEPNS0_11MemoryChunkENS0_13ExecutabilityE:
.LFB22870:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	testl	%edx, %edx
	je	.L1259
	cmpq	$536870912, (%rsi)
	ja	.L1269
.L1259:
	leaq	262176(%rax), %rdx
	cmpq	48(%rax), %rdx
	jnb	.L1260
	.p2align 4,,10
	.p2align 3
.L1261:
	movq	$0, (%rdx)
	addq	$262144, %rdx
	cmpq	48(%rax), %rdx
	jb	.L1261
.L1260:
	pxor	%xmm0, %xmm0
	orq	$32, 8(%rax)
	movups	%xmm0, 224(%rax)
	ret
.L1269:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22870:
	.size	_ZN2v88internal9LargePage10InitializeEPNS0_4HeapEPNS0_11MemoryChunkENS0_13ExecutabilityE, .-_ZN2v88internal9LargePage10InitializeEPNS0_4HeapEPNS0_11MemoryChunkENS0_13ExecutabilityE
	.section	.text._ZN2v88internal4Page26AllocateFreeListCategoriesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Page26AllocateFreeListCategoriesEv
	.type	_ZN2v88internal4Page26AllocateFreeListCategoriesEv, @function
_ZN2v88internal4Page26AllocateFreeListCategoriesEv:
.LFB22871:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	80(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	80(%rdi), %rax
	movq	$-1, %rdi
	movq	96(%rax), %rax
	movslq	8(%rax), %rbx
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rbx
	leaq	0(,%rbx,8), %r14
	cmovbe	%r14, %rdi
	call	_Znam@PLT
	movq	%rax, %rcx
	movq	%rbx, %rax
	subq	$1, %rax
	js	.L1272
	subq	$2, %rbx
	movl	$8, %edx
	movq	%rcx, %rdi
	cmpq	$-1, %rbx
	cmovge	%r14, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	movq	%rax, %rcx
.L1272:
	movq	%rcx, 240(%r13)
	xorl	%ebx, %ebx
	jmp	.L1274
	.p2align 4,,10
	.p2align 3
.L1277:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	240(%r13), %rdx
	movl	$4294967295, %esi
	pxor	%xmm0, %xmm0
	movq	%rsi, (%rax)
	movq	$0, 8(%rax)
	movups	%xmm0, 16(%rax)
	movq	%rax, (%rdx,%rbx,8)
	addq	$1, %rbx
.L1274:
	movq	(%r12), %rax
	movq	96(%rax), %rax
	cmpl	%ebx, 12(%rax)
	jge	.L1277
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22871:
	.size	_ZN2v88internal4Page26AllocateFreeListCategoriesEv, .-_ZN2v88internal4Page26AllocateFreeListCategoriesEv
	.section	.text._ZN2v88internal10PagedSpace14InitializePageEPNS0_11MemoryChunkE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace14InitializePageEPNS0_11MemoryChunkE
	.type	_ZN2v88internal10PagedSpace14InitializePageEPNS0_11MemoryChunkE, @function
_ZN2v88internal10PagedSpace14InitializePageEPNS0_11MemoryChunkE:
.LFB22868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	48(%rsi), %rax
	subq	40(%rsi), %rax
	movq	$0, 216(%rsi)
	movq	%rax, 192(%rsi)
	movq	64(%rdi), %rdx
	movq	8(%rsi), %rax
	movq	2064(%rdx), %rdx
	cmpl	$1, 80(%rdx)
	jle	.L1279
	orq	$262150, %rax
.L1280:
	movq	%rax, 8(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal4Page26AllocateFreeListCategoriesEv
	leaq	80(%r12), %rcx
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	jmp	.L1282
	.p2align 4,,10
	.p2align 3
.L1284:
	movq	240(%r12), %rdx
	movq	(%rdx,%rax,8), %rdx
	movl	$0, 4(%rdx)
	movl	%eax, (%rdx)
	addq	$1, %rax
	movups	%xmm0, 16(%rdx)
.L1282:
	movq	(%rcx), %rdx
	movq	96(%rdx), %rdx
	cmpl	%eax, 12(%rdx)
	jge	.L1284
	pxor	%xmm0, %xmm0
	movq	%r12, %rax
	movups	%xmm0, 224(%r12)
	mfence
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1279:
	.cfi_restore_state
	andq	$-262147, %rax
	orq	$4, %rax
	jmp	.L1280
	.cfi_endproc
.LFE22868:
	.size	_ZN2v88internal10PagedSpace14InitializePageEPNS0_11MemoryChunkE, .-_ZN2v88internal10PagedSpace14InitializePageEPNS0_11MemoryChunkE
	.section	.text._ZN2v88internal4Page28InitializeFreeListCategoriesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Page28InitializeFreeListCategoriesEv
	.type	_ZN2v88internal4Page28InitializeFreeListCategoriesEv, @function
_ZN2v88internal4Page28InitializeFreeListCategoriesEv:
.LFB22875:
	.cfi_startproc
	endbr64
	leaq	80(%rdi), %rcx
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1288:
	movq	240(%rdi), %rdx
	movq	(%rdx,%rax,8), %rdx
	movl	$0, 4(%rdx)
	movl	%eax, (%rdx)
	addq	$1, %rax
	movups	%xmm0, 16(%rdx)
.L1287:
	movq	(%rcx), %rdx
	movq	96(%rdx), %rdx
	cmpl	%eax, 12(%rdx)
	jge	.L1288
	ret
	.cfi_endproc
.LFE22875:
	.size	_ZN2v88internal4Page28InitializeFreeListCategoriesEv, .-_ZN2v88internal4Page28InitializeFreeListCategoriesEv
	.section	.text._ZN2v88internal4Page25ReleaseFreeListCategoriesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Page25ReleaseFreeListCategoriesEv
	.type	_ZN2v88internal4Page25ReleaseFreeListCategoriesEv, @function
_ZN2v88internal4Page25ReleaseFreeListCategoriesEv:
.LFB22876:
	.cfi_startproc
	endbr64
	cmpq	$0, 240(%rdi)
	je	.L1302
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	80(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	xorl	%ebx, %ebx
	subq	$8, %rsp
	jmp	.L1293
	.p2align 4,,10
	.p2align 3
.L1305:
	movq	240(%r12), %rax
	movq	(%rax,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L1292
	movl	$32, %esi
	call	_ZdlPvm@PLT
	movq	240(%r12), %rax
	movq	$0, (%rax,%rbx,8)
.L1292:
	addq	$1, %rbx
.L1293:
	movq	0(%r13), %rax
	movq	96(%rax), %rax
	cmpl	%ebx, 12(%rax)
	jge	.L1305
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1294
	call	_ZdaPv@PLT
.L1294:
	movq	$0, 240(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1302:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE22876:
	.size	_ZN2v88internal4Page25ReleaseFreeListCategoriesEv, .-_ZN2v88internal4Page25ReleaseFreeListCategoriesEv
	.section	.text._ZN2v88internal11MemoryChunk23CommittedPhysicalMemoryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11MemoryChunk23CommittedPhysicalMemoryEv
	.type	_ZN2v88internal11MemoryChunk23CommittedPhysicalMemoryEv, @function
_ZN2v88internal11MemoryChunk23CommittedPhysicalMemoryEv:
.LFB22878:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v84base2OS14HasLazyCommitsEv@PLT
	testb	%al, %al
	je	.L1310
	testb	$32, 10(%rbx)
	je	.L1312
.L1309:
	movq	152(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1312:
	.cfi_restore_state
	movq	80(%rbx), %rax
	cmpl	$5, 72(%rax)
	jne	.L1309
.L1310:
	movq	(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22878:
	.size	_ZN2v88internal11MemoryChunk23CommittedPhysicalMemoryEv, .-_ZN2v88internal11MemoryChunk23CommittedPhysicalMemoryEv
	.section	.text._ZNK2v88internal11MemoryChunk10InOldSpaceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11MemoryChunk10InOldSpaceEv
	.type	_ZNK2v88internal11MemoryChunk10InOldSpaceEv, @function
_ZNK2v88internal11MemoryChunk10InOldSpaceEv:
.LFB22879:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testb	$32, 10(%rdi)
	je	.L1316
	ret
	.p2align 4,,10
	.p2align 3
.L1316:
	movq	80(%rdi), %rax
	cmpl	$2, 72(%rax)
	sete	%al
	ret
	.cfi_endproc
.LFE22879:
	.size	_ZNK2v88internal11MemoryChunk10InOldSpaceEv, .-_ZNK2v88internal11MemoryChunk10InOldSpaceEv
	.section	.text._ZNK2v88internal11MemoryChunk18InLargeObjectSpaceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11MemoryChunk18InLargeObjectSpaceEv
	.type	_ZNK2v88internal11MemoryChunk18InLargeObjectSpaceEv, @function
_ZNK2v88internal11MemoryChunk18InLargeObjectSpaceEv:
.LFB22880:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testb	$32, 10(%rdi)
	je	.L1320
	ret
	.p2align 4,,10
	.p2align 3
.L1320:
	movq	80(%rdi), %rax
	cmpl	$5, 72(%rax)
	sete	%al
	ret
	.cfi_endproc
.LFE22880:
	.size	_ZNK2v88internal11MemoryChunk18InLargeObjectSpaceEv, .-_ZNK2v88internal11MemoryChunk18InLargeObjectSpaceEv
	.section	.text._ZN2v88internal11MemoryChunk25SetOldGenerationPageFlagsEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11MemoryChunk25SetOldGenerationPageFlagsEb
	.type	_ZN2v88internal11MemoryChunk25SetOldGenerationPageFlagsEb, @function
_ZN2v88internal11MemoryChunk25SetOldGenerationPageFlagsEb:
.LFB22882:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262147, %rax
	orq	$262150, %rdx
	orq	$4, %rax
	testb	%sil, %sil
	cmovne	%rdx, %rax
	movq	%rax, 8(%rdi)
	ret
	.cfi_endproc
.LFE22882:
	.size	_ZN2v88internal11MemoryChunk25SetOldGenerationPageFlagsEb, .-_ZN2v88internal11MemoryChunk25SetOldGenerationPageFlagsEb
	.section	.text._ZN2v88internal11MemoryChunk27SetYoungGenerationPageFlagsEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11MemoryChunk27SetYoungGenerationPageFlagsEb
	.type	_ZN2v88internal11MemoryChunk27SetYoungGenerationPageFlagsEb, @function
_ZN2v88internal11MemoryChunk27SetYoungGenerationPageFlagsEb:
.LFB22883:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262149, %rax
	orq	$262150, %rdx
	orq	$2, %rax
	testb	%sil, %sil
	cmovne	%rdx, %rax
	movq	%rax, 8(%rdi)
	ret
	.cfi_endproc
.LFE22883:
	.size	_ZN2v88internal11MemoryChunk27SetYoungGenerationPageFlagsEb, .-_ZN2v88internal11MemoryChunk27SetYoungGenerationPageFlagsEb
	.section	.text._ZN2v88internal4Page25ResetAllocationStatisticsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Page25ResetAllocationStatisticsEv
	.type	_ZN2v88internal4Page25ResetAllocationStatisticsEv, @function
_ZN2v88internal4Page25ResetAllocationStatisticsEv:
.LFB22884:
	.cfi_startproc
	endbr64
	movq	$0, 216(%rdi)
	movq	48(%rdi), %rax
	subq	40(%rdi), %rax
	movq	%rax, 192(%rdi)
	ret
	.cfi_endproc
.LFE22884:
	.size	_ZN2v88internal4Page25ResetAllocationStatisticsEv, .-_ZN2v88internal4Page25ResetAllocationStatisticsEv
	.section	.text._ZN2v88internal4Page20AllocateLocalTrackerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Page20AllocateLocalTrackerEv
	.type	_ZN2v88internal4Page20AllocateLocalTrackerEv, @function
_ZN2v88internal4Page20AllocateLocalTrackerEv:
.LFB22885:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$64, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	leaq	56(%rax), %rdx
	movq	%rbx, (%rax)
	movq	%rdx, 8(%rax)
	movq	$1, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movl	$0x3f800000, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	%rax, 248(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22885:
	.size	_ZN2v88internal4Page20AllocateLocalTrackerEv, .-_ZN2v88internal4Page20AllocateLocalTrackerEv
	.section	.text._ZN2v88internal4Page22contains_array_buffersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Page22contains_array_buffersEv
	.type	_ZN2v88internal4Page22contains_array_buffersEv, @function
_ZN2v88internal4Page22contains_array_buffersEv:
.LFB22886:
	.cfi_startproc
	endbr64
	movq	248(%rdi), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L1330
	cmpq	$0, 32(%rdx)
	setne	%al
.L1330:
	ret
	.cfi_endproc
.LFE22886:
	.size	_ZN2v88internal4Page22contains_array_buffersEv, .-_ZN2v88internal4Page22contains_array_buffersEv
	.section	.text._ZN2v88internal4Page19AvailableInFreeListEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Page19AvailableInFreeListEv
	.type	_ZN2v88internal4Page19AvailableInFreeListEv, @function
_ZN2v88internal4Page19AvailableInFreeListEv:
.LFB22887:
	.cfi_startproc
	endbr64
	leaq	80(%rdi), %rdx
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	jmp	.L1336
	.p2align 4,,10
	.p2align 3
.L1337:
	movq	240(%rdi), %rcx
	movq	(%rcx,%rax,8), %rcx
	addq	$1, %rax
	movl	4(%rcx), %ecx
	addq	%rcx, %r8
.L1336:
	movq	(%rdx), %rcx
	movq	96(%rcx), %rcx
	cmpl	%eax, 8(%rcx)
	jg	.L1337
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE22887:
	.size	_ZN2v88internal4Page19AvailableInFreeListEv, .-_ZN2v88internal4Page19AvailableInFreeListEv
	.section	.text._ZN2v88internal15MemoryAllocator17PartialFreeMemoryEPNS0_11MemoryChunkEmmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocator17PartialFreeMemoryEPNS0_11MemoryChunkEmmm
	.type	_ZN2v88internal15MemoryAllocator17PartialFreeMemoryEPNS0_11MemoryChunkEmmm, @function
_ZN2v88internal15MemoryAllocator17PartialFreeMemoryEPNS0_11MemoryChunkEmmm:
.LFB22892:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	56(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%r8, 48(%rsi)
	subq	%rcx, (%rsi)
	testb	$1, 8(%rsi)
	je	.L1339
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %edx
	testl	%edx, %edx
	je	.L1340
	sall	$10, %edx
	movslq	%edx, %rdx
.L1341:
	xorl	%ecx, %ecx
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal13VirtualMemory14SetPermissionsEmmNS_13PageAllocator10PermissionE@PLT
.L1339:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal13VirtualMemory7ReleaseEm@PLT
	movq	%rax, %rbx
	lock subq	%rax, 80(%r12)
	movq	(%r12), %rax
	movq	40960(%rax), %r12
	cmpb	$0, 5952(%r12)
	je	.L1342
	movq	5944(%r12), %rax
.L1343:
	testq	%rax, %rax
	je	.L1338
	subl	%ebx, (%rax)
.L1338:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1340:
	.cfi_restore_state
	movq	%rsi, %rbx
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movq	48(%rbx), %r8
	movq	%rax, %rdx
	jmp	.L1341
	.p2align 4,,10
	.p2align 3
.L1342:
	movb	$1, 5952(%r12)
	leaq	5928(%r12), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 5944(%r12)
	jmp	.L1343
	.cfi_endproc
.LFE22892:
	.size	_ZN2v88internal15MemoryAllocator17PartialFreeMemoryEPNS0_11MemoryChunkEmmm, .-_ZN2v88internal15MemoryAllocator17PartialFreeMemoryEPNS0_11MemoryChunkEmmm
	.section	.rodata._ZN2v88internal4Page21ShrinkToHighWaterMarkEv.part.0.str1.1,"aMS",@progbits,1
.LC17:
	.string	"filler.IsFiller()"
	.section	.rodata._ZN2v88internal4Page21ShrinkToHighWaterMarkEv.part.0.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"Shrinking page %p: end %p -> %p\n"
	.align 8
.LC19:
	.string	"filler.address() + filler.Size() == area_end()"
	.section	.text._ZN2v88internal4Page21ShrinkToHighWaterMarkEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4Page21ShrinkToHighWaterMarkEv.part.0, @function
_ZN2v88internal4Page21ShrinkToHighWaterMarkEv.part.0:
.LFB31007:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	152(%rdi), %rax
	addq	%rdi, %rax
	leaq	1(%rax), %rdx
	movq	%rdx, -48(%rbp)
	cmpq	48(%rdi), %rax
	je	.L1353
	movq	(%rax), %rax
	movq	%rdi, %rbx
	movzwl	11(%rax), %eax
	cmpw	$73, %ax
	je	.L1354
	cmpw	$76, %ax
	jne	.L1359
.L1354:
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	je	.L1355
	sall	$10, %eax
	cltq
.L1356:
	movq	48(%rbx), %rcx
	movq	-48(%rbp), %rsi
	negq	%rax
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	leaq	1(%rdx), %r12
	andq	%rax, %r12
	je	.L1353
	cmpb	$0, _ZN2v88internal21FLAG_trace_gc_verboseE(%rip)
	movq	24(%rbx), %rdi
	jne	.L1370
.L1357:
	subq	$1, %rsi
	subl	%r12d, %ecx
	movl	$1, %r8d
	subl	%esi, %ecx
	movl	%ecx, %edx
	movl	$1, %ecx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	24(%rbx), %rax
	movq	(%rbx), %rdx
	movq	%r12, %rcx
	movq	48(%rbx), %r8
	movq	%rbx, %rsi
	movq	2048(%rax), %rdi
	addq	%rbx, %rdx
	subq	%r12, %rdx
	subq	%r12, %r8
	call	_ZN2v88internal15MemoryAllocator17PartialFreeMemoryEPNS0_11MemoryChunkEmmm
	movq	-48(%rbp), %rax
	leaq	-1(%rax), %rdx
	cmpq	48(%rbx), %rdx
	je	.L1352
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	cmpw	$73, %dx
	je	.L1362
	cmpw	$76, %dx
	jne	.L1359
.L1362:
	movq	48(%rbx), %rbx
	movq	-1(%rax), %rsi
	leaq	-48(%rbp), %rdi
	leaq	-1(%rax), %r13
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%r13, %rax
	cmpq	%rax, %rbx
	je	.L1352
	leaq	.LC19(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1353:
	xorl	%r12d, %r12d
.L1352:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1371
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1355:
	.cfi_restore_state
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	jmp	.L1356
	.p2align 4,,10
	.p2align 3
.L1370:
	movq	%rcx, %r8
	subq	$37592, %rdi
	movq	%rbx, %rdx
	xorl	%eax, %eax
	leaq	.LC18(%rip), %rsi
	subq	%r12, %r8
	call	_ZN2v88internal12PrintIsolateEPvPKcz@PLT
	movq	24(%rbx), %rdi
	movq	48(%rbx), %rcx
	movq	-48(%rbp), %rsi
	jmp	.L1357
	.p2align 4,,10
	.p2align 3
.L1359:
	leaq	.LC17(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1371:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE31007:
	.size	_ZN2v88internal4Page21ShrinkToHighWaterMarkEv.part.0, .-_ZN2v88internal4Page21ShrinkToHighWaterMarkEv.part.0
	.section	.text._ZN2v88internal4Page21ShrinkToHighWaterMarkEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Page21ShrinkToHighWaterMarkEv
	.type	_ZN2v88internal4Page21ShrinkToHighWaterMarkEv, @function
_ZN2v88internal4Page21ShrinkToHighWaterMarkEv:
.LFB22889:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	64(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	jne	.L1394
.L1372:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1395
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1394:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	152(%rdi), %rax
	addq	%rdi, %rax
	leaq	1(%rax), %rdx
	movq	%rdx, -48(%rbp)
	cmpq	48(%rdi), %rax
	je	.L1374
	movq	(%rax), %rax
	movzwl	11(%rax), %eax
	cmpw	$73, %ax
	je	.L1375
	cmpw	$76, %ax
	jne	.L1380
.L1375:
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	je	.L1376
	sall	$10, %eax
	cltq
.L1377:
	movq	48(%rbx), %rcx
	movq	-48(%rbp), %rsi
	negq	%rax
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	leaq	1(%rdx), %r12
	andq	%rax, %r12
	je	.L1374
	cmpb	$0, _ZN2v88internal21FLAG_trace_gc_verboseE(%rip)
	movq	24(%rbx), %rdi
	jne	.L1396
.L1378:
	subq	$1, %rsi
	subl	%r12d, %ecx
	movl	$1, %r8d
	subl	%esi, %ecx
	movl	%ecx, %edx
	movl	$1, %ecx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	24(%rbx), %rax
	movq	(%rbx), %rdx
	movq	%r12, %rcx
	movq	48(%rbx), %r8
	movq	%rbx, %rsi
	movq	2048(%rax), %rdi
	addq	%rbx, %rdx
	subq	%r12, %rdx
	subq	%r12, %r8
	call	_ZN2v88internal15MemoryAllocator17PartialFreeMemoryEPNS0_11MemoryChunkEmmm
	movq	-48(%rbp), %rax
	leaq	-1(%rax), %rdx
	cmpq	48(%rbx), %rdx
	je	.L1372
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	cmpw	$73, %dx
	je	.L1383
	cmpw	$76, %dx
	je	.L1383
	.p2align 4,,10
	.p2align 3
.L1380:
	leaq	.LC17(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1374:
	xorl	%r12d, %r12d
	jmp	.L1372
	.p2align 4,,10
	.p2align 3
.L1376:
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	jmp	.L1377
	.p2align 4,,10
	.p2align 3
.L1383:
	movq	48(%rbx), %rbx
	movq	-1(%rax), %rsi
	leaq	-48(%rbp), %rdi
	leaq	-1(%rax), %r13
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%r13, %rax
	cmpq	%rax, %rbx
	je	.L1372
	leaq	.LC19(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1396:
	movq	%rcx, %r8
	subq	$37592, %rdi
	movq	%rbx, %rdx
	xorl	%eax, %eax
	leaq	.LC18(%rip), %rsi
	subq	%r12, %r8
	call	_ZN2v88internal12PrintIsolateEPvPKcz@PLT
	movq	24(%rbx), %rdi
	movq	48(%rbx), %rcx
	movq	-48(%rbp), %rsi
	jmp	.L1378
.L1395:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22889:
	.size	_ZN2v88internal4Page21ShrinkToHighWaterMarkEv, .-_ZN2v88internal4Page21ShrinkToHighWaterMarkEv
	.section	.text._ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE1EEEvPNS0_11MemoryChunkE,"axG",@progbits,_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE1EEEvPNS0_11MemoryChunkE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE1EEEvPNS0_11MemoryChunkE
	.type	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE1EEEvPNS0_11MemoryChunkE, @function
_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE1EEEvPNS0_11MemoryChunkE:
.LFB25878:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	32(%rdi), %rdi
	movl	$262144, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal9FreePagesEPNS_13PageAllocatorEPvm@PLT
	testb	%al, %al
	je	.L1400
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1400:
	.cfi_restore_state
	leaq	.LC10(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25878:
	.size	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE1EEEvPNS0_11MemoryChunkE, .-_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE1EEEvPNS0_11MemoryChunkE
	.section	.text._ZN2v88internal15MemoryAllocator8ZapBlockEmmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocator8ZapBlockEmmm
	.type	_ZN2v88internal15MemoryAllocator8ZapBlockEmmm, @function
_ZN2v88internal15MemoryAllocator8ZapBlockEmmm:
.LFB22900:
	.cfi_startproc
	endbr64
	shrq	$3, %rdx
	movq	%rcx, %rax
	movq	%rsi, %rdi
	movq	%rdx, %rcx
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	ret
	.cfi_endproc
.LFE22900:
	.size	_ZN2v88internal15MemoryAllocator8ZapBlockEmmm, .-_ZN2v88internal15MemoryAllocator8ZapBlockEmmm
	.section	.text._ZN2v88internal15MemoryAllocator17GetCommitPageSizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocator17GetCommitPageSizeEv
	.type	_ZN2v88internal15MemoryAllocator17GetCommitPageSizeEv, @function
_ZN2v88internal15MemoryAllocator17GetCommitPageSizeEv:
.LFB22901:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	je	.L1403
	sall	$10, %eax
	cltq
	ret
	.p2align 4,,10
	.p2align 3
.L1403:
	jmp	_ZN2v88internal14CommitPageSizeEv@PLT
	.cfi_endproc
.LFE22901:
	.size	_ZN2v88internal15MemoryAllocator17GetCommitPageSizeEv, .-_ZN2v88internal15MemoryAllocator17GetCommitPageSizeEv
	.section	.text._ZN2v88internal15MemoryAllocator24ComputeDiscardMemoryAreaEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocator24ComputeDiscardMemoryAreaEmm
	.type	_ZN2v88internal15MemoryAllocator24ComputeDiscardMemoryAreaEmm, @function
_ZN2v88internal15MemoryAllocator24ComputeDiscardMemoryAreaEmm:
.LFB22902:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	testl	%eax, %eax
	je	.L1407
	sall	$10, %eax
	movslq	%eax, %rdx
	movq	%rdx, %rcx
.L1408:
	leaq	24(%rcx), %rax
	cmpq	%rbx, %rax
	ja	.L1411
	movq	%rdx, %rax
	leaq	23(%rcx,%r12), %rcx
	leaq	(%rbx,%r12), %rdx
	negq	%rax
	andq	%rax, %rcx
	andq	%rax, %rdx
	cmpq	%rcx, %rdx
	ja	.L1413
.L1411:
	popq	%rbx
	xorl	%eax, %eax
	xorl	%edx, %edx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1413:
	.cfi_restore_state
	popq	%rbx
	movq	%rcx, %rax
	subq	%rcx, %rdx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1407:
	.cfi_restore_state
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movq	%rax, %rcx
	movq	%rax, %rdx
	jmp	.L1408
	.cfi_endproc
.LFE22902:
	.size	_ZN2v88internal15MemoryAllocator24ComputeDiscardMemoryAreaEmm, .-_ZN2v88internal15MemoryAllocator24ComputeDiscardMemoryAreaEmm
	.section	.text._ZN2v88internal15MemoryAllocator22CommitExecutableMemoryEPNS0_13VirtualMemoryEmmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocator22CommitExecutableMemoryEPNS0_13VirtualMemoryEmmm
	.type	_ZN2v88internal15MemoryAllocator22CommitExecutableMemoryEPNS0_13VirtualMemoryEmmm, @function
_ZN2v88internal15MemoryAllocator22CommitExecutableMemoryEPNS0_13VirtualMemoryEmmm:
.LFB22903:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %edx
	movq	%rdi, -88(%rbp)
	movq	%r8, -80(%rbp)
	testl	%edx, %edx
	je	.L1415
	sall	$10, %edx
	movslq	%edx, %rdx
	movq	%rdx, -56(%rbp)
	movq	%rdx, %rbx
	movq	%rdx, -72(%rbp)
.L1416:
	movq	%rdx, %rax
.L1418:
	movq	%rdx, %r14
	addq	$279, %rax
	negq	%r14
	andq	%rax, %r14
.L1420:
	negq	%rdx
	andq	%rax, %rdx
	movq	%rdx, -64(%rbp)
.L1424:
	movl	$2, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13VirtualMemory14SetPermissionsEmmNS_13PageAllocator10PermissionE@PLT
	movl	%eax, %r8d
	testb	%al, %al
	jne	.L1443
.L1414:
	addq	$56, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1415:
	.cfi_restore_state
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movq	%rax, %rbx
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	jne	.L1444
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movq	%rax, -72(%rbp)
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	jne	.L1445
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	leaq	279(%rax), %r14
	negq	%rax
	andq	%rax, %r14
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	jne	.L1446
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	leaq	279(%rax), %rdx
	negq	%rax
	andq	%rax, %rdx
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	movq	%rdx, -64(%rbp)
	testl	%eax, %eax
	jne	.L1447
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movq	%rax, -56(%rbp)
	jmp	.L1424
	.p2align 4,,10
	.p2align 3
.L1443:
	xorl	%ecx, %ecx
	leaq	0(%r13,%r14), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal13VirtualMemory14SetPermissionsEmmNS_13PageAllocator10PermissionE@PLT
	testb	%al, %al
	je	.L1427
	movq	-64(%rbp), %r9
	addq	-56(%rbp), %r9
	movq	%r15, %rdx
	movl	$2, %ecx
	addq	%r13, %r9
	subq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal13VirtualMemory14SetPermissionsEmmNS_13PageAllocator10PermissionE@PLT
	testb	%al, %al
	je	.L1427
	movq	-80(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	addq	%r13, %rsi
	subq	-72(%rbp), %rsi
	call	_ZN2v88internal13VirtualMemory14SetPermissionsEmmNS_13PageAllocator10PermissionE@PLT
	movq	-56(%rbp), %r9
	testb	%al, %al
	movl	%eax, %r8d
	jne	.L1448
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13VirtualMemory14SetPermissionsEmmNS_13PageAllocator10PermissionE@PLT
	.p2align 4,,10
	.p2align 3
.L1427:
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13VirtualMemory14SetPermissionsEmmNS_13PageAllocator10PermissionE@PLT
	xorl	%r8d, %r8d
	jmp	.L1414
	.p2align 4,,10
	.p2align 3
.L1448:
	movq	-88(%rbp), %rax
	addq	%r9, %r15
	leaq	96(%rax), %rdx
.L1433:
	movq	(%rdx), %rax
	cmpq	%rax, %r13
	jnb	.L1432
	lock cmpxchgq	%r13, (%rdx)
	jne	.L1433
.L1432:
	movq	-88(%rbp), %rax
	leaq	104(%rax), %rdx
.L1431:
	movq	(%rdx), %rax
	cmpq	%rax, %r15
	jbe	.L1414
	lock cmpxchgq	%r15, (%rdx)
	je	.L1414
	jmp	.L1431
.L1444:
	sall	$10, %eax
	movslq	%eax, %rdx
	movq	%rdx, -72(%rbp)
	movq	%rdx, -56(%rbp)
	jmp	.L1416
.L1445:
	sall	$10, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	%rax, %rdx
	jmp	.L1418
.L1446:
	sall	$10, %eax
	movslq	%eax, %rdx
	movq	%rdx, -56(%rbp)
	leaq	279(%rdx), %rax
	jmp	.L1420
.L1447:
	sall	$10, %eax
	cltq
	movq	%rax, -56(%rbp)
	jmp	.L1424
	.cfi_endproc
.LFE22903:
	.size	_ZN2v88internal15MemoryAllocator22CommitExecutableMemoryEPNS0_13VirtualMemoryEmmm, .-_ZN2v88internal15MemoryAllocator22CommitExecutableMemoryEPNS0_13VirtualMemoryEmmm
	.section	.text._ZN2v88internal15MemoryAllocator21AllocateAlignedMemoryEmmmNS0_13ExecutabilityEPvPNS0_13VirtualMemoryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocator21AllocateAlignedMemoryEmmmNS0_13ExecutabilityEPvPNS0_13VirtualMemoryE
	.type	_ZN2v88internal15MemoryAllocator21AllocateAlignedMemoryEmmmNS0_13ExecutabilityEPvPNS0_13VirtualMemoryE, @function
_ZN2v88internal15MemoryAllocator21AllocateAlignedMemoryEmmmNS0_13ExecutabilityEPvPNS0_13VirtualMemoryE:
.LFB22816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rax
	movq	%r9, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	16(%rbp), %rdx
	movq	%rdx, -88(%rbp)
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	cmpl	$1, %r8d
	je	.L1473
	movq	32(%r12), %rsi
	leaq	-80(%rbp), %r15
	movq	%rax, %r8
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal13VirtualMemoryC1EPNS_13PageAllocatorEmPvm@PLT
	movq	-72(%rbp), %r14
	testq	%r14, %r14
	jne	.L1474
.L1472:
	xorl	%r14d, %r14d
.L1452:
	movq	%r15, %rdi
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1475
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1474:
	.cfi_restore_state
	leaq	80(%r12), %rax
	movq	%rax, -96(%rbp)
	movq	%rax, %rdx
	movq	-64(%rbp), %rax
	lock addq	%rax, (%rdx)
	movl	$2, %ecx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal13VirtualMemory14SetPermissionsEmmNS_13PageAllocator10PermissionE@PLT
	testb	%al, %al
	jne	.L1476
.L1458:
	movq	%r15, %rdi
	call	_ZN2v88internal13VirtualMemory4FreeEv@PLT
	movq	-96(%rbp), %rax
	lock subq	%rbx, (%rax)
	jmp	.L1472
	.p2align 4,,10
	.p2align 3
.L1473:
	movq	40(%r12), %rsi
	leaq	-80(%rbp), %r15
	movq	%rax, %r8
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal13VirtualMemoryC1EPNS_13PageAllocatorEmPvm@PLT
	movq	-72(%rbp), %r14
	testq	%r14, %r14
	je	.L1472
	leaq	80(%r12), %rax
	movq	%rax, -96(%rbp)
	movq	%rax, %rsi
	movq	-64(%rbp), %rax
	lock addq	%rax, (%rsi)
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15MemoryAllocator22CommitExecutableMemoryEPNS0_13VirtualMemoryEmmm
	testb	%al, %al
	je	.L1458
.L1455:
	movq	-88(%rbp), %rbx
	movq	-80(%rbp), %rax
	movq	%r15, %rdi
	movdqu	-72(%rbp), %xmm0
	movq	%rax, (%rbx)
	movups	%xmm0, 8(%rbx)
	call	_ZN2v88internal13VirtualMemory5ResetEv@PLT
	jmp	.L1452
	.p2align 4,,10
	.p2align 3
.L1476:
	addq	%r14, %r13
	leaq	96(%r12), %rcx
.L1454:
	movq	(%rcx), %rax
	cmpq	%r14, %rax
	jbe	.L1453
	lock cmpxchgq	%r14, (%rcx)
	jne	.L1454
.L1453:
	leaq	104(%r12), %rcx
.L1456:
	movq	(%rcx), %rax
	cmpq	%rax, %r13
	jbe	.L1455
	lock cmpxchgq	%r13, (%rcx)
	je	.L1455
	jmp	.L1456
.L1475:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22816:
	.size	_ZN2v88internal15MemoryAllocator21AllocateAlignedMemoryEmmmNS0_13ExecutabilityEPvPNS0_13VirtualMemoryE, .-_ZN2v88internal15MemoryAllocator21AllocateAlignedMemoryEmmmNS0_13ExecutabilityEPvPNS0_13VirtualMemoryE
	.section	.text._ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE0EEEPNS0_7SlotSetEv,"axG",@progbits,_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE0EEEPNS0_7SlotSetEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE0EEEPNS0_7SlotSetEv
	.type	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE0EEEPNS0_7SlotSetEv, @function
_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE0EEEPNS0_7SlotSetEv:
.LFB25893:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdi
	call	_ZN2v88internalL28AllocateAndInitializeSlotSetEmm
	movq	%rax, %r13
	xorl	%eax, %eax
	lock cmpxchgq	%r13, 104(%rbx)
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L1477
	testq	%r13, %r13
	je	.L1491
	movq	-8(%r13), %rax
	leaq	(%rax,%rax,2), %rsi
	salq	$7, %rsi
	leaq	0(%r13,%rsi), %rbx
	cmpq	%rbx, %r13
	je	.L1479
	.p2align 4,,10
	.p2align 3
.L1489:
	subq	$384, %rbx
	movq	%rbx, %r12
	leaq	256(%rbx), %r15
	.p2align 4,,10
	.p2align 3
.L1481:
	movq	(%r12), %rdi
	movq	$0, (%r12)
	testq	%rdi, %rdi
	je	.L1480
	call	_ZdaPv@PLT
.L1480:
	addq	$8, %r12
	cmpq	%r15, %r12
	jne	.L1481
	leaq	264(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	.p2align 4,,10
	.p2align 3
.L1484:
	movq	352(%rbx), %rdi
	cmpq	320(%rbx), %rdi
	je	.L1482
.L1505:
	cmpq	360(%rbx), %rdi
	je	.L1504
	movq	-8(%rdi), %r15
	subq	$8, %rdi
	movq	%rdi, 352(%rbx)
.L1490:
	testq	%r15, %r15
	je	.L1484
	movq	%r15, %rdi
	call	_ZdaPv@PLT
	movq	352(%rbx), %rdi
	cmpq	320(%rbx), %rdi
	jne	.L1505
.L1482:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	cmpq	$0, 304(%rbx)
	je	.L1486
	movq	376(%rbx), %rax
	movq	344(%rbx), %r14
	leaq	8(%rax), %r15
	cmpq	%r14, %r15
	jbe	.L1488
	.p2align 4,,10
	.p2align 3
.L1487:
	movq	(%r14), %rdi
	addq	$8, %r14
	call	_ZdlPv@PLT
	cmpq	%r14, %r15
	ja	.L1487
.L1488:
	movq	304(%rbx), %rdi
	call	_ZdlPv@PLT
.L1486:
	movq	%r12, %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	cmpq	%rbx, %r13
	jne	.L1489
	movq	-8(%r13), %rax
	leaq	(%rax,%rax,2), %rsi
	salq	$7, %rsi
.L1479:
	leaq	-8(%r13), %rdi
	addq	$8, %rsi
	call	_ZdaPvm@PLT
	movq	-56(%rbp), %r13
.L1477:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1504:
	.cfi_restore_state
	movq	376(%rbx), %rax
	movq	-8(%rax), %rax
	movq	504(%rax), %r15
	call	_ZdlPv@PLT
	movq	376(%rbx), %rax
	leaq	-8(%rax), %rdx
	movq	%rdx, 376(%rbx)
	movq	-8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 360(%rbx)
	addq	$504, %rax
	movq	%rdx, 368(%rbx)
	movq	%rax, 352(%rbx)
	jmp	.L1490
	.p2align 4,,10
	.p2align 3
.L1491:
	movq	%rax, %r13
	jmp	.L1477
	.cfi_endproc
.LFE25893:
	.size	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE0EEEPNS0_7SlotSetEv, .-_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE0EEEPNS0_7SlotSetEv
	.section	.text._ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv,"axG",@progbits,_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv
	.type	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv, @function
_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv:
.LFB25894:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdi
	call	_ZN2v88internalL28AllocateAndInitializeSlotSetEmm
	movq	%rax, %r13
	xorl	%eax, %eax
	lock cmpxchgq	%r13, 112(%rbx)
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L1506
	testq	%r13, %r13
	je	.L1520
	movq	-8(%r13), %rax
	leaq	(%rax,%rax,2), %rsi
	salq	$7, %rsi
	leaq	0(%r13,%rsi), %rbx
	cmpq	%rbx, %r13
	je	.L1508
	.p2align 4,,10
	.p2align 3
.L1518:
	subq	$384, %rbx
	movq	%rbx, %r12
	leaq	256(%rbx), %r15
	.p2align 4,,10
	.p2align 3
.L1510:
	movq	(%r12), %rdi
	movq	$0, (%r12)
	testq	%rdi, %rdi
	je	.L1509
	call	_ZdaPv@PLT
.L1509:
	addq	$8, %r12
	cmpq	%r15, %r12
	jne	.L1510
	leaq	264(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	.p2align 4,,10
	.p2align 3
.L1513:
	movq	352(%rbx), %rdi
	cmpq	320(%rbx), %rdi
	je	.L1511
.L1534:
	cmpq	360(%rbx), %rdi
	je	.L1533
	movq	-8(%rdi), %r15
	subq	$8, %rdi
	movq	%rdi, 352(%rbx)
.L1519:
	testq	%r15, %r15
	je	.L1513
	movq	%r15, %rdi
	call	_ZdaPv@PLT
	movq	352(%rbx), %rdi
	cmpq	320(%rbx), %rdi
	jne	.L1534
.L1511:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	cmpq	$0, 304(%rbx)
	je	.L1515
	movq	376(%rbx), %rax
	movq	344(%rbx), %r14
	leaq	8(%rax), %r15
	cmpq	%r14, %r15
	jbe	.L1517
	.p2align 4,,10
	.p2align 3
.L1516:
	movq	(%r14), %rdi
	addq	$8, %r14
	call	_ZdlPv@PLT
	cmpq	%r14, %r15
	ja	.L1516
.L1517:
	movq	304(%rbx), %rdi
	call	_ZdlPv@PLT
.L1515:
	movq	%r12, %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	cmpq	%rbx, %r13
	jne	.L1518
	movq	-8(%r13), %rax
	leaq	(%rax,%rax,2), %rsi
	salq	$7, %rsi
.L1508:
	leaq	-8(%r13), %rdi
	addq	$8, %rsi
	call	_ZdaPvm@PLT
	movq	-56(%rbp), %r13
.L1506:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1533:
	.cfi_restore_state
	movq	376(%rbx), %rax
	movq	-8(%rax), %rax
	movq	504(%rax), %r15
	call	_ZdlPv@PLT
	movq	376(%rbx), %rax
	leaq	-8(%rax), %rdx
	movq	%rdx, 376(%rbx)
	movq	-8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 360(%rbx)
	addq	$504, %rax
	movq	%rdx, 368(%rbx)
	movq	%rax, 352(%rbx)
	jmp	.L1519
	.p2align 4,,10
	.p2align 3
.L1520:
	movq	%rax, %r13
	jmp	.L1506
	.cfi_endproc
.LFE25894:
	.size	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv, .-_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv
	.section	.text._ZN2v88internal11MemoryChunk14ReleaseSlotSetILNS0_17RememberedSetTypeE0EEEvv,"axG",@progbits,_ZN2v88internal11MemoryChunk14ReleaseSlotSetILNS0_17RememberedSetTypeE0EEEvv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11MemoryChunk14ReleaseSlotSetILNS0_17RememberedSetTypeE0EEEvv
	.type	_ZN2v88internal11MemoryChunk14ReleaseSlotSetILNS0_17RememberedSetTypeE0EEEvv, @function
_ZN2v88internal11MemoryChunk14ReleaseSlotSetILNS0_17RememberedSetTypeE0EEEvv:
.LFB25884:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	104(%rdi), %r13
	testq	%r13, %r13
	je	.L1535
	movq	$0, 104(%rdi)
	movq	-8(%r13), %rax
	leaq	(%rax,%rax,2), %rsi
	salq	$7, %rsi
	leaq	0(%r13,%rsi), %rbx
	cmpq	%rbx, %r13
	je	.L1537
	.p2align 4,,10
	.p2align 3
.L1547:
	subq	$384, %rbx
	movq	%rbx, %r12
	leaq	256(%rbx), %r14
	.p2align 4,,10
	.p2align 3
.L1539:
	movq	(%r12), %rdi
	movq	$0, (%r12)
	testq	%rdi, %rdi
	je	.L1538
	call	_ZdaPv@PLT
.L1538:
	addq	$8, %r12
	cmpq	%r14, %r12
	jne	.L1539
	leaq	264(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	.p2align 4,,10
	.p2align 3
.L1542:
	movq	352(%rbx), %rdi
	cmpq	320(%rbx), %rdi
	je	.L1540
.L1559:
	cmpq	360(%rbx), %rdi
	je	.L1558
	movq	-8(%rdi), %r14
	subq	$8, %rdi
	movq	%rdi, 352(%rbx)
.L1548:
	testq	%r14, %r14
	je	.L1542
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movq	352(%rbx), %rdi
	cmpq	320(%rbx), %rdi
	jne	.L1559
.L1540:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	cmpq	$0, 304(%rbx)
	je	.L1544
	movq	376(%rbx), %rax
	movq	344(%rbx), %r14
	leaq	8(%rax), %r15
	cmpq	%r14, %r15
	jbe	.L1546
	.p2align 4,,10
	.p2align 3
.L1545:
	movq	(%r14), %rdi
	addq	$8, %r14
	call	_ZdlPv@PLT
	cmpq	%r14, %r15
	ja	.L1545
.L1546:
	movq	304(%rbx), %rdi
	call	_ZdlPv@PLT
.L1544:
	movq	%r12, %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	cmpq	%rbx, %r13
	jne	.L1547
	movq	-8(%r13), %rax
	leaq	(%rax,%rax,2), %rsi
	salq	$7, %rsi
.L1537:
	addq	$8, %rsp
	leaq	-8(%r13), %rdi
	addq	$8, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdaPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1558:
	.cfi_restore_state
	movq	376(%rbx), %rax
	movq	-8(%rax), %rax
	movq	504(%rax), %r14
	call	_ZdlPv@PLT
	movq	376(%rbx), %rax
	leaq	-8(%rax), %rdx
	movq	%rdx, 376(%rbx)
	movq	-8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 360(%rbx)
	addq	$504, %rax
	movq	%rdx, 368(%rbx)
	movq	%rax, 352(%rbx)
	jmp	.L1548
	.p2align 4,,10
	.p2align 3
.L1535:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25884:
	.size	_ZN2v88internal11MemoryChunk14ReleaseSlotSetILNS0_17RememberedSetTypeE0EEEvv, .-_ZN2v88internal11MemoryChunk14ReleaseSlotSetILNS0_17RememberedSetTypeE0EEEvv
	.section	.text._ZN2v88internal11MemoryChunk14ReleaseSlotSetILNS0_17RememberedSetTypeE1EEEvv,"axG",@progbits,_ZN2v88internal11MemoryChunk14ReleaseSlotSetILNS0_17RememberedSetTypeE1EEEvv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11MemoryChunk14ReleaseSlotSetILNS0_17RememberedSetTypeE1EEEvv
	.type	_ZN2v88internal11MemoryChunk14ReleaseSlotSetILNS0_17RememberedSetTypeE1EEEvv, @function
_ZN2v88internal11MemoryChunk14ReleaseSlotSetILNS0_17RememberedSetTypeE1EEEvv:
.LFB25885:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	112(%rdi), %r13
	testq	%r13, %r13
	je	.L1560
	movq	$0, 112(%rdi)
	movq	-8(%r13), %rax
	leaq	(%rax,%rax,2), %rsi
	salq	$7, %rsi
	leaq	0(%r13,%rsi), %rbx
	cmpq	%rbx, %r13
	je	.L1562
	.p2align 4,,10
	.p2align 3
.L1572:
	subq	$384, %rbx
	movq	%rbx, %r12
	leaq	256(%rbx), %r14
	.p2align 4,,10
	.p2align 3
.L1564:
	movq	(%r12), %rdi
	movq	$0, (%r12)
	testq	%rdi, %rdi
	je	.L1563
	call	_ZdaPv@PLT
.L1563:
	addq	$8, %r12
	cmpq	%r14, %r12
	jne	.L1564
	leaq	264(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	.p2align 4,,10
	.p2align 3
.L1567:
	movq	352(%rbx), %rdi
	cmpq	320(%rbx), %rdi
	je	.L1565
.L1584:
	cmpq	360(%rbx), %rdi
	je	.L1583
	movq	-8(%rdi), %r14
	subq	$8, %rdi
	movq	%rdi, 352(%rbx)
.L1573:
	testq	%r14, %r14
	je	.L1567
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movq	352(%rbx), %rdi
	cmpq	320(%rbx), %rdi
	jne	.L1584
.L1565:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	cmpq	$0, 304(%rbx)
	je	.L1569
	movq	376(%rbx), %rax
	movq	344(%rbx), %r14
	leaq	8(%rax), %r15
	cmpq	%r14, %r15
	jbe	.L1571
	.p2align 4,,10
	.p2align 3
.L1570:
	movq	(%r14), %rdi
	addq	$8, %r14
	call	_ZdlPv@PLT
	cmpq	%r14, %r15
	ja	.L1570
.L1571:
	movq	304(%rbx), %rdi
	call	_ZdlPv@PLT
.L1569:
	movq	%r12, %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	cmpq	%rbx, %r13
	jne	.L1572
	movq	-8(%r13), %rax
	leaq	(%rax,%rax,2), %rsi
	salq	$7, %rsi
.L1562:
	addq	$8, %rsp
	leaq	-8(%r13), %rdi
	addq	$8, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdaPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1583:
	.cfi_restore_state
	movq	376(%rbx), %rax
	movq	-8(%rax), %rax
	movq	504(%rax), %r14
	call	_ZdlPv@PLT
	movq	376(%rbx), %rax
	leaq	-8(%rax), %rdx
	movq	%rdx, 376(%rbx)
	movq	-8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 360(%rbx)
	addq	$504, %rax
	movq	%rdx, 368(%rbx)
	movq	%rax, 352(%rbx)
	jmp	.L1573
	.p2align 4,,10
	.p2align 3
.L1560:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25885:
	.size	_ZN2v88internal11MemoryChunk14ReleaseSlotSetILNS0_17RememberedSetTypeE1EEEvv, .-_ZN2v88internal11MemoryChunk14ReleaseSlotSetILNS0_17RememberedSetTypeE1EEEvv
	.section	.text._ZN2v88internal11MemoryChunk19ReleaseTypedSlotSetILNS0_17RememberedSetTypeE0EEEvv,"axG",@progbits,_ZN2v88internal11MemoryChunk19ReleaseTypedSlotSetILNS0_17RememberedSetTypeE0EEEvv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11MemoryChunk19ReleaseTypedSlotSetILNS0_17RememberedSetTypeE0EEEvv
	.type	_ZN2v88internal11MemoryChunk19ReleaseTypedSlotSetILNS0_17RememberedSetTypeE0EEEvv, @function
_ZN2v88internal11MemoryChunk19ReleaseTypedSlotSetILNS0_17RememberedSetTypeE0EEEvv:
.LFB25886:
	.cfi_startproc
	endbr64
	movq	120(%rdi), %r8
	testq	%r8, %r8
	je	.L1585
	movq	$0, 120(%rdi)
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	8(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1585:
	ret
	.cfi_endproc
.LFE25886:
	.size	_ZN2v88internal11MemoryChunk19ReleaseTypedSlotSetILNS0_17RememberedSetTypeE0EEEvv, .-_ZN2v88internal11MemoryChunk19ReleaseTypedSlotSetILNS0_17RememberedSetTypeE0EEEvv
	.section	.text._ZN2v88internal11MemoryChunk19ReleaseTypedSlotSetILNS0_17RememberedSetTypeE1EEEvv,"axG",@progbits,_ZN2v88internal11MemoryChunk19ReleaseTypedSlotSetILNS0_17RememberedSetTypeE1EEEvv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11MemoryChunk19ReleaseTypedSlotSetILNS0_17RememberedSetTypeE1EEEvv
	.type	_ZN2v88internal11MemoryChunk19ReleaseTypedSlotSetILNS0_17RememberedSetTypeE1EEEvv, @function
_ZN2v88internal11MemoryChunk19ReleaseTypedSlotSetILNS0_17RememberedSetTypeE1EEEvv:
.LFB25887:
	.cfi_startproc
	endbr64
	movq	128(%rdi), %r8
	testq	%r8, %r8
	je	.L1587
	movq	$0, 128(%rdi)
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	8(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1587:
	ret
	.cfi_endproc
.LFE25887:
	.size	_ZN2v88internal11MemoryChunk19ReleaseTypedSlotSetILNS0_17RememberedSetTypeE1EEEvv, .-_ZN2v88internal11MemoryChunk19ReleaseTypedSlotSetILNS0_17RememberedSetTypeE1EEEvv
	.section	.text._ZN2v88internal11MemoryChunk24AllocateInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEEv,"axG",@progbits,_ZN2v88internal11MemoryChunk24AllocateInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11MemoryChunk24AllocateInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEEv
	.type	_ZN2v88internal11MemoryChunk24AllocateInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEEv, @function
_ZN2v88internal11MemoryChunk24AllocateInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEEv:
.LFB25897:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$48, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	leaq	8(%rax), %rdx
	movl	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	%rdx, 24(%rax)
	movq	%rdx, 32(%rax)
	movq	$0, 40(%rax)
	movq	%rax, 136(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25897:
	.size	_ZN2v88internal11MemoryChunk24AllocateInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEEv, .-_ZN2v88internal11MemoryChunk24AllocateInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEEv
	.section	.text._ZN2v88internal11MemoryChunk24AllocateInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEEv,"axG",@progbits,_ZN2v88internal11MemoryChunk24AllocateInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11MemoryChunk24AllocateInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEEv
	.type	_ZN2v88internal11MemoryChunk24AllocateInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEEv, @function
_ZN2v88internal11MemoryChunk24AllocateInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEEv:
.LFB25907:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$48, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	leaq	8(%rax), %rdx
	movl	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	%rdx, 24(%rax)
	movq	%rdx, 32(%rax)
	movq	$0, 40(%rax)
	movq	%rax, 144(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25907:
	.size	_ZN2v88internal11MemoryChunk24AllocateInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEEv, .-_ZN2v88internal11MemoryChunk24AllocateInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEEv
	.section	.text._ZN2v88internal11MemoryChunk36RegisteredObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEbNS0_10HeapObjectE,"axG",@progbits,_ZN2v88internal11MemoryChunk36RegisteredObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEbNS0_10HeapObjectE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11MemoryChunk36RegisteredObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEbNS0_10HeapObjectE
	.type	_ZN2v88internal11MemoryChunk36RegisteredObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEbNS0_10HeapObjectE, @function
_ZN2v88internal11MemoryChunk36RegisteredObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEbNS0_10HeapObjectE:
.LFB25926:
	.cfi_startproc
	endbr64
	movq	136(%rdi), %rax
	testq	%rax, %rax
	je	.L1600
	leaq	8(%rax), %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1600
	movq	%rcx, %rdx
	jmp	.L1596
	.p2align 4,,10
	.p2align 3
.L1603:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1597
.L1596:
	cmpq	32(%rax), %rsi
	jbe	.L1603
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1596
.L1597:
	xorl	%eax, %eax
	cmpq	%rcx, %rdx
	je	.L1593
	cmpq	32(%rdx), %rsi
	setnb	%al
	ret
	.p2align 4,,10
	.p2align 3
.L1600:
	xorl	%eax, %eax
.L1593:
	ret
	.cfi_endproc
.LFE25926:
	.size	_ZN2v88internal11MemoryChunk36RegisteredObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEbNS0_10HeapObjectE, .-_ZN2v88internal11MemoryChunk36RegisteredObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEbNS0_10HeapObjectE
	.section	.text._ZN2v88internal11MemoryChunk36RegisteredObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEbNS0_10HeapObjectE,"axG",@progbits,_ZN2v88internal11MemoryChunk36RegisteredObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEbNS0_10HeapObjectE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11MemoryChunk36RegisteredObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEbNS0_10HeapObjectE
	.type	_ZN2v88internal11MemoryChunk36RegisteredObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEbNS0_10HeapObjectE, @function
_ZN2v88internal11MemoryChunk36RegisteredObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEbNS0_10HeapObjectE:
.LFB25927:
	.cfi_startproc
	endbr64
	movq	144(%rdi), %rax
	testq	%rax, %rax
	je	.L1611
	leaq	8(%rax), %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1611
	movq	%rcx, %rdx
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1614:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1608
.L1607:
	cmpq	32(%rax), %rsi
	jbe	.L1614
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1607
.L1608:
	xorl	%eax, %eax
	cmpq	%rcx, %rdx
	je	.L1604
	cmpq	32(%rdx), %rsi
	setnb	%al
	ret
	.p2align 4,,10
	.p2align 3
.L1611:
	xorl	%eax, %eax
.L1604:
	ret
	.cfi_endproc
.LFE25927:
	.size	_ZN2v88internal11MemoryChunk36RegisteredObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEbNS0_10HeapObjectE, .-_ZN2v88internal11MemoryChunk36RegisteredObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEbNS0_10HeapObjectE
	.section	.text._ZN2v88internal11MemoryChunk19ReleaseLocalTrackerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11MemoryChunk19ReleaseLocalTrackerEv
	.type	_ZN2v88internal11MemoryChunk19ReleaseLocalTrackerEv, @function
_ZN2v88internal11MemoryChunk19ReleaseLocalTrackerEv:
.LFB22923:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	248(%rdi), %r12
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L1616
	movq	%r12, %rdi
	call	_ZN2v88internal23LocalArrayBufferTrackerD1Ev@PLT
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1616:
	movq	$0, 248(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22923:
	.size	_ZN2v88internal11MemoryChunk19ReleaseLocalTrackerEv, .-_ZN2v88internal11MemoryChunk19ReleaseLocalTrackerEv
	.section	.text._ZN2v88internal11MemoryChunk29AllocateYoungGenerationBitmapEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11MemoryChunk29AllocateYoungGenerationBitmapEv
	.type	_ZN2v88internal11MemoryChunk29AllocateYoungGenerationBitmapEv, @function
_ZN2v88internal11MemoryChunk29AllocateYoungGenerationBitmapEv:
.LFB22924:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4096, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$1, %edi
	subq	$8, %rsp
	call	calloc@PLT
	movq	%rax, 264(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22924:
	.size	_ZN2v88internal11MemoryChunk29AllocateYoungGenerationBitmapEv, .-_ZN2v88internal11MemoryChunk29AllocateYoungGenerationBitmapEv
	.section	.text._ZN2v88internal11MemoryChunk28ReleaseYoungGenerationBitmapEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11MemoryChunk28ReleaseYoungGenerationBitmapEv
	.type	_ZN2v88internal11MemoryChunk28ReleaseYoungGenerationBitmapEv, @function
_ZN2v88internal11MemoryChunk28ReleaseYoungGenerationBitmapEv:
.LFB22925:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	264(%rdi), %rdi
	call	free@PLT
	movq	$0, 264(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22925:
	.size	_ZN2v88internal11MemoryChunk28ReleaseYoungGenerationBitmapEv, .-_ZN2v88internal11MemoryChunk28ReleaseYoungGenerationBitmapEv
	.section	.text._ZN2v88internal5Space14AllocationStepEimi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Space14AllocationStepEimi
	.type	_ZN2v88internal5Space14AllocationStepEimi, @function
_ZN2v88internal5Space14AllocationStepEimi:
.LFB22930:
	.cfi_startproc
	endbr64
	cmpb	$0, 56(%rdi)
	jne	.L1633
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	cmpq	%rax, 8(%rdi)
	je	.L1625
	movq	64(%rdi), %rax
	movq	%rdx, %r13
	movslq	%ecx, %rbx
	movl	%esi, %r14d
	movl	$1, %r8d
	movl	$1, %ecx
	movl	%ebx, %edx
	movq	%r13, %rsi
	movb	$1, 432(%rax)
	movq	64(%rdi), %rdi
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	16(%r12), %rax
	movq	8(%r12), %r15
	movq	%rax, -56(%rbp)
	cmpq	%rax, %r15
	je	.L1629
	.p2align 4,,10
	.p2align 3
.L1628:
	movq	(%r15), %rdi
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movl	%r14d, %esi
	addq	$8, %r15
	call	_ZN2v88internal18AllocationObserver14AllocationStepEimm@PLT
	cmpq	%r15, -56(%rbp)
	jne	.L1628
.L1629:
	movq	64(%r12), %rax
	movb	$0, 432(%rax)
.L1625:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1633:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE22930:
	.size	_ZN2v88internal5Space14AllocationStepEimi, .-_ZN2v88internal5Space14AllocationStepEimi
	.section	.text._ZN2v88internal5Space31GetNextInlineAllocationStepSizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Space31GetNextInlineAllocationStepSizeEv
	.type	_ZN2v88internal5Space31GetNextInlineAllocationStepSizeEv, @function
_ZN2v88internal5Space31GetNextInlineAllocationStepSizeEv:
.LFB22931:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	16(%rdi), %rcx
	xorl	%r8d, %r8d
	cmpq	%rax, %rcx
	jne	.L1639
	jmp	.L1636
	.p2align 4,,10
	.p2align 3
.L1643:
	cmpq	%rdx, %r8
	cmovg	%rdx, %r8
	addq	$8, %rax
	cmpq	%rax, %rcx
	je	.L1636
.L1639:
	movq	(%rax), %rdx
	movq	16(%rdx), %rdx
	testq	%r8, %r8
	jne	.L1643
	addq	$8, %rax
	movq	%rdx, %r8
	cmpq	%rax, %rcx
	jne	.L1639
.L1636:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE22931:
	.size	_ZN2v88internal5Space31GetNextInlineAllocationStepSizeEv, .-_ZN2v88internal5Space31GetNextInlineAllocationStepSizeEv
	.section	.text._ZN2v88internal10PagedSpaceC2EPNS0_4HeapENS0_15AllocationSpaceENS0_13ExecutabilityEPNS0_8FreeListE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpaceC2EPNS0_4HeapENS0_15AllocationSpaceENS0_13ExecutabilityEPNS0_8FreeListE
	.type	_ZN2v88internal10PagedSpaceC2EPNS0_4HeapENS0_15AllocationSpaceENS0_13ExecutabilityEPNS0_8FreeListE, @function
_ZN2v88internal10PagedSpaceC2EPNS0_4HeapENS0_15AllocationSpaceENS0_13ExecutabilityEPNS0_8FreeListE:
.LFB22933:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rsi, 64(%rdi)
	movl	%edx, 72(%rdi)
	movq	%r8, 96(%rdi)
	movq	%rax, (%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 56(%rdi)
	movups	%xmm0, 80(%rdi)
	movl	$16, %edi
	call	_Znam@PLT
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	leaq	192(%rbx), %rdi
	movq	%rax, 48(%rbx)
	movq	$0, (%rax)
	mfence
	movq	48(%rbx), %rax
	movq	$0, 8(%rax)
	leaq	16+_ZTVN2v88internal10PagedSpaceE(%rip), %rax
	mfence
	movq	%rax, (%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 144(%rbx)
	movl	%r13d, 152(%rbx)
	movups	%xmm0, 104(%rbx)
	movups	%xmm1, 128(%rbx)
	movq	$0, 168(%rbx)
	mfence
	movups	%xmm0, 176(%rbx)
	call	_ZN2v84base5MutexC1Ev@PLT
	movl	$261864, %eax
	cmpl	$3, %r12d
	je	.L1655
.L1645:
	movq	%rax, 160(%rbx)
	pxor	%xmm0, %xmm0
	movq	$0, 168(%rbx)
	mfence
	movups	%xmm0, 176(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1655:
	.cfi_restore_state
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	je	.L1646
	sall	$10, %eax
	movl	$262144, %r12d
	movslq	%eax, %rcx
	subl	%eax, %r12d
	movq	%rcx, %rdx
	movslq	%r12d, %r12
.L1647:
	movq	%rcx, %r13
	leaq	279(%rcx), %rax
	negq	%r13
	andq	%rax, %r13
.L1649:
	movq	%rdx, %rax
.L1651:
	addq	%r13, %rax
	subq	%rax, %r12
	movq	%r12, %rax
	jmp	.L1645
	.p2align 4,,10
	.p2align 3
.L1646:
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movq	%rax, %r8
	movl	$262144, %eax
	subl	%r8d, %eax
	movslq	%eax, %r12
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	jne	.L1656
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movq	%rax, %r13
	leaq	279(%rax), %rax
	negq	%r13
	andq	%rax, %r13
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	jne	.L1657
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	jmp	.L1651
.L1656:
	sall	$10, %eax
	movslq	%eax, %rcx
	movq	%rcx, %rdx
	jmp	.L1647
.L1657:
	sall	$10, %eax
	movslq	%eax, %rdx
	jmp	.L1649
	.cfi_endproc
.LFE22933:
	.size	_ZN2v88internal10PagedSpaceC2EPNS0_4HeapENS0_15AllocationSpaceENS0_13ExecutabilityEPNS0_8FreeListE, .-_ZN2v88internal10PagedSpaceC2EPNS0_4HeapENS0_15AllocationSpaceENS0_13ExecutabilityEPNS0_8FreeListE
	.globl	_ZN2v88internal10PagedSpaceC1EPNS0_4HeapENS0_15AllocationSpaceENS0_13ExecutabilityEPNS0_8FreeListE
	.set	_ZN2v88internal10PagedSpaceC1EPNS0_4HeapENS0_15AllocationSpaceENS0_13ExecutabilityEPNS0_8FreeListE,_ZN2v88internal10PagedSpaceC2EPNS0_4HeapENS0_15AllocationSpaceENS0_13ExecutabilityEPNS0_8FreeListE
	.section	.text._ZN2v88internal10PagedSpace12ContainsSlowEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace12ContainsSlowEm
	.type	_ZN2v88internal10PagedSpace12ContainsSlowEm, @function
_ZN2v88internal10PagedSpace12ContainsSlowEm:
.LFB22940:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	andq	$-262144, %rsi
	testq	%rax, %rax
	jne	.L1661
	jmp	.L1662
	.p2align 4,,10
	.p2align 3
.L1667:
	movq	224(%rax), %rax
	testq	%rax, %rax
	je	.L1666
.L1661:
	cmpq	%rax, %rsi
	jne	.L1667
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1666:
	ret
.L1662:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22940:
	.size	_ZN2v88internal10PagedSpace12ContainsSlowEm, .-_ZN2v88internal10PagedSpace12ContainsSlowEm
	.section	.rodata._ZN2v88internal10PagedSpace33RefineAllocatedBytesAfterSweepingEPNS0_4PageE.str1.1,"aMS",@progbits,1
.LC20:
	.string	"page->SweepingDone()"
	.section	.text._ZN2v88internal10PagedSpace33RefineAllocatedBytesAfterSweepingEPNS0_4PageE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace33RefineAllocatedBytesAfterSweepingEPNS0_4PageE
	.type	_ZN2v88internal10PagedSpace33RefineAllocatedBytesAfterSweepingEPNS0_4PageE, @function
_ZN2v88internal10PagedSpace33RefineAllocatedBytesAfterSweepingEPNS0_4PageE:
.LFB22941:
	.cfi_startproc
	endbr64
	movq	168(%rsi), %rax
	testq	%rax, %rax
	jne	.L1679
	movq	96(%rsi), %rdx
	movq	192(%rsi), %rax
	cmpq	%rax, %rdx
	jbe	.L1671
	movq	64(%rdi), %rcx
	subq	%rdx, %rax
	addq	%rax, 184(%rdi)
	movq	2160(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L1671
	addq	%rdx, %rax
	movq	%rax, 2160(%rcx)
.L1671:
	movq	$0, 96(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L1679:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC20(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22941:
	.size	_ZN2v88internal10PagedSpace33RefineAllocatedBytesAfterSweepingEPNS0_4PageE, .-_ZN2v88internal10PagedSpace33RefineAllocatedBytesAfterSweepingEPNS0_4PageE
	.section	.text._ZN2v88internal10PagedSpace7AddPageEPNS0_4PageE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace7AddPageEPNS0_4PageE
	.type	_ZN2v88internal10PagedSpace7AddPageEPNS0_4PageE, @function
_ZN2v88internal10PagedSpace7AddPageEPNS0_4PageE:
.LFB22943:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	168(%rsi), %r14
	testq	%r14, %r14
	jne	.L1690
	movq	%rdi, %rbx
	movq	%rsi, %r12
	leaq	80(%rsi), %r15
	movq	%rdi, 80(%rsi)
	mfence
	movq	40(%rdi), %rax
	testq	%rax, %rax
	je	.L1682
	movq	224(%rax), %rdx
	movq	%rax, 232(%rsi)
	movq	%rdx, 224(%rsi)
	movq	%rsi, 224(%rax)
	testq	%rdx, %rdx
	je	.L1683
	movq	%rsi, 232(%rdx)
.L1684:
	movq	80(%rbx), %rax
	addq	(%r12), %rax
	movq	%rax, 80(%rbx)
	cmpq	88(%rbx), %rax
	jbe	.L1685
	movq	%rax, 88(%rbx)
.L1685:
	movq	48(%r12), %rax
	leaq	168(%rbx), %rdx
	subq	40(%r12), %rax
	lock addq	%rax, (%rdx)
	movq	168(%rbx), %rax
	cmpq	176(%rbx), %rax
	ja	.L1691
.L1686:
	movq	192(%r12), %rax
	addq	%rax, 184(%rbx)
	movq	200(%r12), %rdx
	movq	48(%rbx), %rax
	lock addq	%rdx, (%rax)
	movq	64(%rbx), %rax
	lock addq	%rdx, 176(%rax)
	movq	208(%r12), %rdx
	movq	48(%rbx), %rax
	lock addq	%rdx, 8(%rax)
	movq	64(%rbx), %rax
	lock addq	%rdx, 176(%rax)
	xorl	%r13d, %r13d
	jmp	.L1688
	.p2align 4,,10
	.p2align 3
.L1692:
	movq	240(%r12), %rax
	movq	96(%rbx), %rdi
	movq	(%rax,%r13,8), %rsi
	addq	$1, %r13
	movl	4(%rsi), %eax
	addq	%rax, %r14
	movq	(%rdi), %rax
	call	*56(%rax)
.L1688:
	movq	(%r15), %rax
	movq	96(%rax), %rax
	cmpl	%r13d, 8(%rax)
	jg	.L1692
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1691:
	.cfi_restore_state
	movq	168(%rbx), %rax
	movq	%rax, 176(%rbx)
	jmp	.L1686
	.p2align 4,,10
	.p2align 3
.L1682:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 224(%rsi)
	movq	%rsi, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 32(%rdi)
	jmp	.L1684
	.p2align 4,,10
	.p2align 3
.L1683:
	movq	%rsi, 40(%rdi)
	jmp	.L1684
	.p2align 4,,10
	.p2align 3
.L1690:
	leaq	.LC20(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22943:
	.size	_ZN2v88internal10PagedSpace7AddPageEPNS0_4PageE, .-_ZN2v88internal10PagedSpace7AddPageEPNS0_4PageE
	.section	.text._ZN2v88internal4Page15ConvertNewToOldEPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Page15ConvertNewToOldEPS1_
	.type	_ZN2v88internal4Page15ConvertNewToOldEPS1_, @function
_ZN2v88internal4Page15ConvertNewToOldEPS1_:
.LFB22877:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	24(%rdi), %rax
	movq	256(%rax), %r13
	movq	%r13, 80(%rdi)
	mfence
	movq	$0, 8(%rdi)
	movq	%r13, %rdi
	call	_ZN2v88internal10PagedSpace14InitializePageEPNS0_11MemoryChunkE
	movq	%r13, %rdi
	movq	%rax, %r12
	movq	%rax, %rsi
	call	_ZN2v88internal10PagedSpace7AddPageEPNS0_4PageE
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22877:
	.size	_ZN2v88internal4Page15ConvertNewToOldEPS1_, .-_ZN2v88internal4Page15ConvertNewToOldEPS1_
	.section	.text._ZN2v88internal10PagedSpace10RemovePageEPNS0_4PageE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace10RemovePageEPNS0_4PageE
	.type	_ZN2v88internal10PagedSpace10RemovePageEPNS0_4PageE, @function
_ZN2v88internal10PagedSpace10RemovePageEPNS0_4PageE:
.LFB22944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	168(%rsi), %r13
	testq	%r13, %r13
	jne	.L1710
	movq	%rdi, %r12
	movq	%rsi, %rbx
	cmpq	40(%rdi), %rsi
	je	.L1711
	movq	224(%rbx), %rax
	cmpq	32(%r12), %rbx
	je	.L1712
.L1698:
	movq	232(%rbx), %rdx
	testq	%rax, %rax
	je	.L1699
	movq	%rdx, 232(%rax)
.L1699:
	testq	%rdx, %rdx
	je	.L1700
	movq	%rax, 224(%rdx)
.L1700:
	pxor	%xmm0, %xmm0
	leaq	80(%rbx), %r14
	movups	%xmm0, 224(%rbx)
	jmp	.L1702
	.p2align 4,,10
	.p2align 3
.L1713:
	movq	240(%rbx), %rax
	movq	96(%r12), %rdi
	movq	(%rax,%r13,8), %rsi
	movq	(%rdi), %rax
	addq	$1, %r13
	call	*64(%rax)
.L1702:
	movq	(%r14), %rax
	movq	96(%rax), %rax
	cmpl	%r13d, 8(%rax)
	jg	.L1713
	movq	192(%rbx), %rax
	subq	%rax, 184(%r12)
	movq	48(%rbx), %rax
	subq	40(%rbx), %rax
	lock subq	%rax, 168(%r12)
	movq	(%rbx), %rax
	subq	%rax, 80(%r12)
	movq	200(%rbx), %rdx
	movq	48(%r12), %rax
	lock subq	%rdx, (%rax)
	movq	64(%r12), %rax
	lock subq	%rdx, 176(%rax)
	movq	208(%rbx), %rdx
	movq	48(%r12), %rax
	lock subq	%rdx, 8(%rax)
	movq	64(%r12), %rax
	lock subq	%rdx, 176(%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1711:
	.cfi_restore_state
	movq	232(%rsi), %rax
	movq	%rax, 40(%rdi)
	movq	224(%rbx), %rax
	cmpq	32(%r12), %rbx
	jne	.L1698
.L1712:
	movq	%rax, 32(%r12)
	movq	224(%rbx), %rax
	jmp	.L1698
	.p2align 4,,10
	.p2align 3
.L1710:
	leaq	.LC20(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22944:
	.size	_ZN2v88internal10PagedSpace10RemovePageEPNS0_4PageE, .-_ZN2v88internal10PagedSpace10RemovePageEPNS0_4PageE
	.section	.text._ZN2v88internal10PagedSpace14RemovePageSafeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace14RemovePageSafeEi
	.type	_ZN2v88internal10PagedSpace14RemovePageSafeEi, @function
_ZN2v88internal10PagedSpace14RemovePageSafeEi:
.LFB22942:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	192(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%esi, %ebx
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	96(%r13), %rdi
	movslq	%ebx, %rsi
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1715
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal10PagedSpace10RemovePageEPNS0_4PageE
.L1715:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22942:
	.size	_ZN2v88internal10PagedSpace14RemovePageSafeEi, .-_ZN2v88internal10PagedSpace14RemovePageSafeEi
	.section	.text._ZN2v88internal10PagedSpace14RefillFreeListEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace14RefillFreeListEv
	.type	_ZN2v88internal10PagedSpace14RefillFreeListEv, @function
_ZN2v88internal10PagedSpace14RefillFreeListEv:
.LFB22936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	72(%rdi), %eax
	leal	-2(%rax), %ecx
	cmpl	$2, %ecx
	jbe	.L1721
	testl	%eax, %eax
	jne	.L1720
.L1721:
	movq	64(%r14), %rax
	movq	$0, -56(%rbp)
	movq	2016(%rax), %rax
	movq	%rax, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L1741:
	movq	-64(%rbp), %rax
	movq	%r14, %rsi
	movq	9984(%rax), %rdi
	call	_ZN2v88internal7Sweeper16GetSweptPageSafeEPNS0_10PagedSpaceE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1720
	testb	$16, 9(%rax)
	jne	.L1765
.L1723:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*152(%rax)
	testb	%al, %al
	je	.L1728
	movq	80(%rbx), %r13
	leaq	192(%r13), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	168(%rbx), %rax
	testq	%rax, %rax
	jne	.L1734
	movq	96(%rbx), %rcx
	movq	192(%rbx), %rax
	cmpq	%rax, %rcx
	jbe	.L1731
	movq	64(%r13), %rsi
	subq	%rcx, %rax
	addq	%rax, 184(%r13)
	movq	2160(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L1731
	addq	%rcx, %rax
	movq	%rax, 2160(%rsi)
.L1731:
	movq	$0, 96(%rbx)
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal10PagedSpace10RemovePageEPNS0_4PageE
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal10PagedSpace7AddPageEPNS0_4PageE
	movq	-56(%rbp), %r12
	movq	%r15, %rdi
	addq	%rax, %r12
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L1733:
	movq	(%r14), %rax
	addq	216(%rbx), %r12
	movq	%r14, %rdi
	movq	%r12, -56(%rbp)
	call	*152(%rax)
	cmpq	$512000, %r12
	jbe	.L1741
	testb	%al, %al
	je	.L1741
.L1720:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1728:
	.cfi_restore_state
	leaq	192(%r14), %rax
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	168(%rbx), %r15
	testq	%r15, %r15
	jne	.L1734
	movq	96(%rbx), %rcx
	movq	192(%rbx), %rax
	cmpq	%rax, %rcx
	jbe	.L1736
	movq	64(%r14), %rsi
	subq	%rcx, %rax
	addq	%rax, 184(%r14)
	movq	2160(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L1736
	addq	%rcx, %rax
	movq	%rax, 2160(%rsi)
.L1736:
	movq	$0, 96(%rbx)
	leaq	80(%rbx), %r13
	xorl	%r12d, %r12d
	jmp	.L1739
	.p2align 4,,10
	.p2align 3
.L1766:
	movq	240(%rbx), %rax
	movq	96(%r14), %rdi
	movq	(%rax,%r12,8), %rsi
	addq	$1, %r12
	movl	4(%rsi), %eax
	addq	%rax, %r15
	movq	(%rdi), %rax
	call	*56(%rax)
.L1739:
	movq	0(%r13), %rax
	movq	96(%rax), %rax
	cmpl	%r12d, 8(%rax)
	jg	.L1766
	movq	-56(%rbp), %r12
	movq	-72(%rbp), %rdi
	addq	%r15, %r12
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1733
	.p2align 4,,10
	.p2align 3
.L1765:
	leaq	80(%rax), %rdi
	xorl	%ecx, %ecx
	jmp	.L1727
	.p2align 4,,10
	.p2align 3
.L1725:
	cmpq	$0, 8(%rax)
	je	.L1726
	movl	4(%rax), %r8d
	subq	%r8, 40(%rsi)
.L1726:
	movq	$0, 8(%rax)
	pxor	%xmm0, %xmm0
	addq	$1, %rcx
	movl	$0, 4(%rax)
	movups	%xmm0, 16(%rax)
.L1727:
	movq	(%rdi), %rax
	movq	96(%rax), %rax
	cmpl	%ecx, 8(%rax)
	jle	.L1723
	movq	240(%rbx), %rax
	movq	96(%r14), %rsi
	movq	(%rax,%rcx,8), %rax
	cmpq	$0, 16(%rax)
	jne	.L1725
	cmpq	$0, 24(%rax)
	jne	.L1725
	movslq	(%rax), %r9
	movq	32(%rsi), %r8
	cmpq	(%r8,%r9,8), %rax
	jne	.L1726
	jmp	.L1725
	.p2align 4,,10
	.p2align 3
.L1734:
	leaq	.LC20(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22936:
	.size	_ZN2v88internal10PagedSpace14RefillFreeListEv, .-_ZN2v88internal10PagedSpace14RefillFreeListEv
	.section	.text._ZN2v88internal10PagedSpace25ShrinkPageToHighWaterMarkEPNS0_4PageE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace25ShrinkPageToHighWaterMarkEPNS0_4PageE
	.type	_ZN2v88internal10PagedSpace25ShrinkPageToHighWaterMarkEPNS0_4PageE, @function
_ZN2v88internal10PagedSpace25ShrinkPageToHighWaterMarkEPNS0_4PageE:
.LFB22945:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	64(%rsi), %rax
	testq	%rax, %rax
	je	.L1768
	movq	%rsi, %rdi
	call	_ZN2v88internal4Page21ShrinkToHighWaterMarkEv.part.0
.L1768:
	lock subq	%rax, 168(%rbx)
	subq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22945:
	.size	_ZN2v88internal10PagedSpace25ShrinkPageToHighWaterMarkEPNS0_4PageE, .-_ZN2v88internal10PagedSpace25ShrinkPageToHighWaterMarkEPNS0_4PageE
	.section	.text._ZN2v88internal10PagedSpace13ResetFreeListEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace13ResetFreeListEv
	.type	_ZN2v88internal10PagedSpace13ResetFreeListEv, @function
_ZN2v88internal10PagedSpace13ResetFreeListEv:
.LFB22946:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	32(%rdi), %r15
	movq	%rdi, -56(%rbp)
	testq	%r15, %r15
	je	.L1773
	.p2align 4,,10
	.p2align 3
.L1774:
	movq	-56(%rbp), %rax
	leaq	80(%r15), %r14
	xorl	%r13d, %r13d
	movq	96(%rax), %r12
	jmp	.L1778
	.p2align 4,,10
	.p2align 3
.L1776:
	cmpq	$0, 8(%rbx)
	je	.L1777
	movl	4(%rbx), %edx
	subq	%rdx, 40(%r12)
.L1777:
	movq	$0, 8(%rbx)
	pxor	%xmm0, %xmm0
	addq	$1, %r13
	movl	$0, 4(%rbx)
	movups	%xmm0, 16(%rbx)
.L1778:
	movq	(%r14), %rdx
	movq	96(%rdx), %rdx
	cmpl	%r13d, 8(%rdx)
	jle	.L1775
	movq	240(%r15), %rdx
	movq	%r12, %rdi
	movq	(%rdx,%r13,8), %rbx
	movq	(%r12), %rdx
	movq	%rbx, %rsi
	call	*64(%rdx)
	cmpq	$0, 16(%rbx)
	jne	.L1776
	cmpq	$0, 24(%rbx)
	jne	.L1776
	movslq	(%rbx), %rcx
	movq	32(%r12), %rdx
	cmpq	(%rdx,%rcx,8), %rbx
	jne	.L1777
	jmp	.L1776
	.p2align 4,,10
	.p2align 3
.L1775:
	movq	224(%r15), %r15
	testq	%r15, %r15
	jne	.L1774
.L1773:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22946:
	.size	_ZN2v88internal10PagedSpace13ResetFreeListEv, .-_ZN2v88internal10PagedSpace13ResetFreeListEv
	.section	.text._ZN2v88internal10PagedSpace15CountTotalPagesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace15CountTotalPagesEv
	.type	_ZN2v88internal10PagedSpace15CountTotalPagesEv, @function
_ZN2v88internal10PagedSpace15CountTotalPagesEv:
.LFB22949:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L1783
	.p2align 4,,10
	.p2align 3
.L1785:
	movq	224(%rax), %rax
	addl	$1, %r8d
	testq	%rax, %rax
	jne	.L1785
.L1783:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE22949:
	.size	_ZN2v88internal10PagedSpace15CountTotalPagesEv, .-_ZN2v88internal10PagedSpace15CountTotalPagesEv
	.section	.text._ZN2v88internal19SpaceWithLinearArea12ComputeLimitEmmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SpaceWithLinearArea12ComputeLimitEmmm
	.type	_ZN2v88internal19SpaceWithLinearArea12ComputeLimitEmmm, @function
_ZN2v88internal19SpaceWithLinearArea12ComputeLimitEmmm:
.LFB22952:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	(%rsi,%rcx), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	64(%rdi), %rax
	movq	%rsi, %rbx
	cmpb	$0, 1520(%rax)
	jne	.L1790
	movq	(%rdi), %rax
	movq	%rdi, %r12
	movq	%rdx, %r14
	call	*128(%rax)
	testb	%al, %al
	je	.L1790
	cmpb	$0, 56(%r12)
	jne	.L1790
	movq	16(%r12), %rcx
	movq	8(%r12), %rax
	cmpq	%rax, %rcx
	je	.L1790
	xorl	%esi, %esi
	jmp	.L1792
	.p2align 4,,10
	.p2align 3
.L1802:
	cmpq	%rdx, %rsi
	cmovg	%rdx, %rsi
	addq	$8, %rax
	cmpq	%rax, %rcx
	je	.L1801
.L1792:
	movq	(%rax), %rdx
	movq	16(%rdx), %rdx
	testq	%rsi, %rsi
	jne	.L1802
	addq	$8, %rax
	movq	%rdx, %rsi
	cmpq	%rax, %rcx
	jne	.L1792
.L1801:
	cmpl	$1, 72(%r12)
	leaq	-1(%rsi), %rax
	je	.L1794
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*104(%rax)
	cltq
.L1794:
	leaq	(%rbx,%r13), %rsi
	addq	%rax, %rsi
	cmpq	%rsi, %r14
	cmova	%rsi, %r14
.L1790:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22952:
	.size	_ZN2v88internal19SpaceWithLinearArea12ComputeLimitEmmm, .-_ZN2v88internal19SpaceWithLinearArea12ComputeLimitEmmm
	.section	.text._ZN2v88internal19SpaceWithLinearArea23UpdateAllocationOriginsENS0_16AllocationOriginE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SpaceWithLinearArea23UpdateAllocationOriginsENS0_16AllocationOriginE
	.type	_ZN2v88internal19SpaceWithLinearArea23UpdateAllocationOriginsENS0_16AllocationOriginE, @function
_ZN2v88internal19SpaceWithLinearArea23UpdateAllocationOriginsENS0_16AllocationOriginE:
.LFB22953:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	addq	$1, 128(%rdi,%rsi,8)
	ret
	.cfi_endproc
.LFE22953:
	.size	_ZN2v88internal19SpaceWithLinearArea23UpdateAllocationOriginsENS0_16AllocationOriginE, .-_ZN2v88internal19SpaceWithLinearArea23UpdateAllocationOriginsENS0_16AllocationOriginE
	.section	.rodata._ZN2v88internal19SpaceWithLinearArea23PrintAllocationsOriginsEv.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"Allocations Origins for %s: GeneratedCode:%zu - Runtime:%zu - GC:%zu\n"
	.section	.text._ZN2v88internal19SpaceWithLinearArea23PrintAllocationsOriginsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SpaceWithLinearArea23PrintAllocationsOriginsEv
	.type	_ZN2v88internal19SpaceWithLinearArea23PrintAllocationsOriginsEv, @function
_ZN2v88internal19SpaceWithLinearArea23PrintAllocationsOriginsEv:
.LFB22954:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	144(%rdi), %r14
	movq	136(%rdi), %r13
	movq	128(%rdi), %r12
	movl	72(%rdi), %edi
	call	_ZN2v88internal4Heap12GetSpaceNameENS0_15AllocationSpaceE@PLT
	movq	64(%rbx), %rdi
	movq	%r14, %r9
	popq	%rbx
	movq	%rax, %rdx
	movq	%r13, %r8
	movq	%r12, %rcx
	xorl	%eax, %eax
	popq	%r12
	subq	$37592, %rdi
	leaq	.LC21(%rip), %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12PrintIsolateEPvPKcz@PLT
	.cfi_endproc
.LFE22954:
	.size	_ZN2v88internal19SpaceWithLinearArea23PrintAllocationsOriginsEv, .-_ZN2v88internal19SpaceWithLinearArea23PrintAllocationsOriginsEv
	.section	.rodata._ZN2v88internal10PagedSpace11SetReadableEv.str1.8,"aMS",@progbits,1
	.align 8
.LC22:
	.string	"heap()->memory_allocator()->IsMemoryChunkExecutable(page)"
	.section	.text._ZN2v88internal10PagedSpace11SetReadableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace11SetReadableEv
	.type	_ZN2v88internal10PagedSpace11SetReadableEv, @function
_ZN2v88internal10PagedSpace11SetReadableEv:
.LFB22959:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	32(%rdi), %r12
	testq	%r12, %r12
	je	.L1806
	movq	%rdi, %rbx
	.p2align 4,,10
	.p2align 3
.L1811:
	movq	64(%rbx), %rax
	xorl	%edx, %edx
	movq	2048(%rax), %rcx
	movq	%r12, %rax
	movq	352(%rcx), %rdi
	divq	%rdi
	movq	344(%rcx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L1808
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L1810
	.p2align 4,,10
	.p2align 3
.L1823:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1808
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r8
	jne	.L1808
.L1810:
	cmpq	%r12, %rsi
	jne	.L1823
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal11MemoryChunk52DecrementWriteUnprotectCounterAndMaybeSetPermissionsENS_13PageAllocator10PermissionE
	movq	224(%r12), %r12
	testq	%r12, %r12
	jne	.L1811
.L1806:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1808:
	.cfi_restore_state
	leaq	.LC22(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22959:
	.size	_ZN2v88internal10PagedSpace11SetReadableEv, .-_ZN2v88internal10PagedSpace11SetReadableEv
	.section	.text._ZN2v88internal10PagedSpace20SetReadAndExecutableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace20SetReadAndExecutableEv
	.type	_ZN2v88internal10PagedSpace20SetReadAndExecutableEv, @function
_ZN2v88internal10PagedSpace20SetReadAndExecutableEv:
.LFB22960:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	32(%rdi), %r12
	testq	%r12, %r12
	je	.L1824
	movq	%rdi, %rbx
	.p2align 4,,10
	.p2align 3
.L1829:
	movq	64(%rbx), %rax
	xorl	%edx, %edx
	movq	2048(%rax), %rcx
	movq	%r12, %rax
	movq	352(%rcx), %rdi
	divq	%rdi
	movq	344(%rcx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L1826
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L1828
	.p2align 4,,10
	.p2align 3
.L1841:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1826
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r8
	jne	.L1826
.L1828:
	cmpq	%r12, %rsi
	jne	.L1841
	movq	%r12, %rdi
	movl	$4, %esi
	call	_ZN2v88internal11MemoryChunk52DecrementWriteUnprotectCounterAndMaybeSetPermissionsENS_13PageAllocator10PermissionE
	movq	224(%r12), %r12
	testq	%r12, %r12
	jne	.L1829
.L1824:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1826:
	.cfi_restore_state
	leaq	.LC22(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22960:
	.size	_ZN2v88internal10PagedSpace20SetReadAndExecutableEv, .-_ZN2v88internal10PagedSpace20SetReadAndExecutableEv
	.section	.text._ZN2v88internal10PagedSpace18SetReadAndWritableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace18SetReadAndWritableEv
	.type	_ZN2v88internal10PagedSpace18SetReadAndWritableEv, @function
_ZN2v88internal10PagedSpace18SetReadAndWritableEv:
.LFB22961:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	32(%rdi), %r12
	testq	%r12, %r12
	je	.L1842
	movq	%rdi, %rbx
	.p2align 4,,10
	.p2align 3
.L1847:
	movq	64(%rbx), %rax
	xorl	%edx, %edx
	movq	2048(%rax), %rcx
	movq	%r12, %rax
	movq	352(%rcx), %rdi
	divq	%rdi
	movq	344(%rcx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L1844
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L1846
	.p2align 4,,10
	.p2align 3
.L1859:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1844
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r8
	jne	.L1844
.L1846:
	cmpq	%r12, %rsi
	jne	.L1859
	movq	%r12, %rdi
	call	_ZN2v88internal11MemoryChunk18SetReadAndWritableEv
	movq	224(%r12), %r12
	testq	%r12, %r12
	jne	.L1847
.L1842:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1844:
	.cfi_restore_state
	leaq	.LC22(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22961:
	.size	_ZN2v88internal10PagedSpace18SetReadAndWritableEv, .-_ZN2v88internal10PagedSpace18SetReadAndWritableEv
	.section	.text._ZN2v88internal8NewSpace4FlipEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8NewSpace4FlipEv
	.type	_ZN2v88internal8NewSpace4FlipEv, @function
_ZN2v88internal8NewSpace4FlipEv:
.LFB22968:
	.cfi_startproc
	endbr64
	movq	352(%rdi), %rax
	movq	312(%rdi), %rsi
	leaq	208(%rdi), %r9
	leaq	368(%rdi), %rcx
	movq	472(%rdi), %rdx
	movdqu	240(%rdi), %xmm1
	movq	8(%rax), %r8
	movq	%rsi, 472(%rdi)
	movq	320(%rdi), %rsi
	movq	%rdx, 312(%rdi)
	movq	480(%rdi), %rdx
	movq	%rsi, 480(%rdi)
	movq	328(%rdi), %rsi
	movq	%rdx, 320(%rdi)
	movq	488(%rdi), %rdx
	movq	%rsi, 488(%rdi)
	movq	336(%rdi), %rsi
	movq	%rdx, 328(%rdi)
	movq	496(%rdi), %rdx
	movq	%rsi, 496(%rdi)
	movzbl	344(%rdi), %esi
	movq	%rdx, 336(%rdi)
	movzbl	504(%rdi), %edx
	movb	%sil, 504(%rdi)
	movq	408(%rdi), %rsi
	movb	%dl, 344(%rdi)
	movq	400(%rdi), %rdx
	movq	%rsi, %xmm2
	movq	512(%rdi), %rsi
	movq	%rax, 512(%rdi)
	movq	%rdx, %xmm0
	movq	416(%rdi), %rax
	movups	%xmm1, 400(%rdi)
	movq	%rsi, 352(%rdi)
	movq	256(%rdi), %rsi
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 240(%rdi)
	movq	%rsi, 416(%rdi)
	movq	%rax, 256(%rdi)
	testq	%rdx, %rdx
	je	.L1866
	.p2align 4,,10
	.p2align 3
.L1867:
	movq	%r8, %rax
	movq	%r9, 80(%rdx)
	mfence
	movq	8(%rdx), %rsi
	xorq	%rsi, %rax
	andl	$262150, %eax
	xorq	%rsi, %rax
	movq	%rax, 8(%rdx)
	cmpl	$1, 348(%rdi)
	je	.L1879
	andq	$-17, %rax
	orq	$8, %rax
	movq	%rax, 8(%rdx)
	movq	224(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1867
.L1866:
	movq	400(%rdi), %rax
	testq	%rax, %rax
	je	.L1880
	.p2align 4,,10
	.p2align 3
.L1870:
	movq	%rcx, 80(%rax)
	mfence
	cmpl	$1, 508(%rdi)
	movq	8(%rax), %rdx
	je	.L1881
	andq	$-17, %rdx
	orq	$8, %rdx
	movq	%rdx, 8(%rax)
	movq	224(%rax), %rax
	testq	%rax, %rax
	jne	.L1870
	ret
	.p2align 4,,10
	.p2align 3
.L1881:
	andq	$-524297, %rdx
	movq	$0, 96(%rax)
	orq	$16, %rdx
	movq	%rdx, 8(%rax)
	movq	224(%rax), %rax
	testq	%rax, %rax
	jne	.L1870
	ret
	.p2align 4,,10
	.p2align 3
.L1879:
	andq	$-524297, %rax
	movq	$0, 96(%rdx)
	orq	$16, %rax
	movq	%rax, 8(%rdx)
	movq	224(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1867
	movq	400(%rdi), %rax
	testq	%rax, %rax
	jne	.L1870
.L1880:
	ret
	.cfi_endproc
.LFE22968:
	.size	_ZN2v88internal8NewSpace4FlipEv, .-_ZN2v88internal8NewSpace4FlipEv
	.section	.text._ZN2v88internal21LocalAllocationBuffer5CloseEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21LocalAllocationBuffer5CloseEv
	.type	_ZN2v88internal21LocalAllocationBuffer5CloseEv, @function
_ZN2v88internal21LocalAllocationBuffer5CloseEv:
.LFB22973:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rsi
	testq	%rsi, %rsi
	jne	.L1891
	xorl	%eax, %eax
	xorl	%edx, %edx
	ret
	.p2align 4,,10
	.p2align 3
.L1891:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	16(%rdi), %edx
	movq	(%rdi), %rdi
	subl	%esi, %edx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	8(%rbx), %rax
	movq	16(%rbx), %rdx
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22973:
	.size	_ZN2v88internal21LocalAllocationBuffer5CloseEv, .-_ZN2v88internal21LocalAllocationBuffer5CloseEv
	.section	.text._ZN2v88internal21LocalAllocationBufferC2EPNS0_4HeapENS0_20LinearAllocationAreaE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21LocalAllocationBufferC2EPNS0_4HeapENS0_20LinearAllocationAreaE
	.type	_ZN2v88internal21LocalAllocationBufferC2EPNS0_4HeapENS0_20LinearAllocationAreaE, @function
_ZN2v88internal21LocalAllocationBufferC2EPNS0_4HeapENS0_20LinearAllocationAreaE:
.LFB22975:
	.cfi_startproc
	endbr64
	movq	%rdx, %rax
	movq	%rsi, (%rdi)
	movq	%rsi, %r9
	movq	%rcx, %rdx
	movq	%rax, 8(%rdi)
	movq	%rax, %rsi
	movq	%rcx, 16(%rdi)
	testq	%rax, %rax
	jne	.L1894
	ret
	.p2align 4,,10
	.p2align 3
.L1894:
	subl	%eax, %edx
	movl	$1, %r8d
	movl	$1, %ecx
	movq	%r9, %rdi
	jmp	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	.cfi_endproc
.LFE22975:
	.size	_ZN2v88internal21LocalAllocationBufferC2EPNS0_4HeapENS0_20LinearAllocationAreaE, .-_ZN2v88internal21LocalAllocationBufferC2EPNS0_4HeapENS0_20LinearAllocationAreaE
	.globl	_ZN2v88internal21LocalAllocationBufferC1EPNS0_4HeapENS0_20LinearAllocationAreaE
	.set	_ZN2v88internal21LocalAllocationBufferC1EPNS0_4HeapENS0_20LinearAllocationAreaE,_ZN2v88internal21LocalAllocationBufferC2EPNS0_4HeapENS0_20LinearAllocationAreaE
	.section	.text._ZN2v88internal21LocalAllocationBufferC2ERKS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21LocalAllocationBufferC2ERKS1_
	.type	_ZN2v88internal21LocalAllocationBufferC2ERKS1_, @function
_ZN2v88internal21LocalAllocationBufferC2ERKS1_:
.LFB22978:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movups	%xmm0, 8(%rdi)
	movdqu	8(%rsi), %xmm1
	movq	(%rsi), %rax
	movups	%xmm0, 8(%rsi)
	movq	%rax, (%rdi)
	movups	%xmm1, 8(%rdi)
	ret
	.cfi_endproc
.LFE22978:
	.size	_ZN2v88internal21LocalAllocationBufferC2ERKS1_, .-_ZN2v88internal21LocalAllocationBufferC2ERKS1_
	.globl	_ZN2v88internal21LocalAllocationBufferC1ERKS1_
	.set	_ZN2v88internal21LocalAllocationBufferC1ERKS1_,_ZN2v88internal21LocalAllocationBufferC2ERKS1_
	.section	.text._ZN2v88internal21LocalAllocationBufferaSERKS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21LocalAllocationBufferaSERKS1_
	.type	_ZN2v88internal21LocalAllocationBufferaSERKS1_, @function
_ZN2v88internal21LocalAllocationBufferaSERKS1_:
.LFB22980:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	8(%rdi), %rsi
	testq	%rsi, %rsi
	jne	.L1902
.L1897:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, (%r12)
	movdqu	8(%rbx), %xmm1
	movq	%r12, %rax
	movups	%xmm1, 8(%r12)
	movups	%xmm0, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1902:
	.cfi_restore_state
	movl	16(%rdi), %edx
	movq	(%rdi), %rdi
	movl	$1, %r8d
	movl	$1, %ecx
	subl	%esi, %edx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	pxor	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	jmp	.L1897
	.cfi_endproc
.LFE22980:
	.size	_ZN2v88internal21LocalAllocationBufferaSERKS1_, .-_ZN2v88internal21LocalAllocationBufferaSERKS1_
	.section	.text._ZN2v88internal8NewSpace26UpdateLinearAllocationAreaEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8NewSpace26UpdateLinearAllocationAreaEv
	.type	_ZN2v88internal8NewSpace26UpdateLinearAllocationAreaEv, @function
_ZN2v88internal8NewSpace26UpdateLinearAllocationAreaEv:
.LFB22981:
	.cfi_startproc
	endbr64
	movq	352(%rdi), %rdx
	movq	104(%rdi), %rax
	movq	40(%rdx), %rsi
	testq	%rax, %rax
	je	.L1904
	leaq	-1(%rax), %rcx
	andq	$-262144, %rcx
	subq	%rcx, %rax
	addq	$152, %rcx
	movq	%rax, %rdx
.L1906:
	movq	(%rcx), %rax
	cmpq	%rax, %rdx
	jle	.L1914
	lock cmpxchgq	%rdx, (%rcx)
	jne	.L1906
.L1914:
	movq	352(%rdi), %rdx
.L1904:
	movq	48(%rdx), %rax
	movq	%rsi, 104(%rdi)
	leaq	_ZN2v88internal19SpaceWithLinearArea29StartNextInlineAllocationStepEv(%rip), %rcx
	movq	%rax, 112(%rdi)
	movq	%rax, 200(%rdi)
	movq	104(%rdi), %rax
	movq	%rax, 192(%rdi)
	movq	(%rdi), %rdx
	movq	48(%rdx), %rax
	cmpq	%rcx, %rax
	jne	.L1907
	movq	64(%rdi), %rax
	cmpb	$0, 432(%rax)
	jne	.L1903
	cmpb	$0, 56(%rdi)
	jne	.L1903
	movq	8(%rdi), %rax
	cmpq	%rax, 16(%rdi)
	je	.L1903
	movq	104(%rdi), %rax
	xorl	%esi, %esi
	movq	%rax, 120(%rdi)
	movq	136(%rdx), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1903:
	ret
	.p2align 4,,10
	.p2align 3
.L1907:
	jmp	*%rax
	.cfi_endproc
.LFE22981:
	.size	_ZN2v88internal8NewSpace26UpdateLinearAllocationAreaEv, .-_ZN2v88internal8NewSpace26UpdateLinearAllocationAreaEv
	.section	.text._ZN2v88internal8NewSpace25ResetLinearAllocationAreaEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8NewSpace25ResetLinearAllocationAreaEv
	.type	_ZN2v88internal8NewSpace25ResetLinearAllocationAreaEv, @function
_ZN2v88internal8NewSpace25ResetLinearAllocationAreaEv:
.LFB22982:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	64(%rdi), %rax
	cmpb	$0, 432(%rax)
	jne	.L1916
	movq	120(%rdi), %rbx
	testq	%rbx, %rbx
	jne	.L1932
.L1916:
	movq	240(%r12), %rax
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	movl	$0, 360(%r12)
	movq	%rax, 352(%r12)
	call	_ZN2v88internal8NewSpace26UpdateLinearAllocationAreaEv
	movq	240(%r12), %r13
	testq	%r13, %r13
	je	.L1915
	.p2align 4,,10
	.p2align 3
.L1922:
	movq	16(%r13), %rax
	movq	%r13, %rsi
	leaq	8(%rax), %rdi
	movq	$0, (%rax)
	movq	$0, 4088(%rax)
	andq	$-8, %rdi
	subq	%rdi, %rax
	leal	4096(%rax), %ecx
	movq	%rbx, %rax
	shrl	$3, %ecx
	rep stosq
	movq	$0, 96(%r13)
	movq	64(%r12), %rax
	movq	2072(%rax), %rdi
	call	_ZN2v88internal17ConcurrentMarking20ClearMemoryChunkDataEPNS0_11MemoryChunkE@PLT
	movq	224(%r13), %r13
	testq	%r13, %r13
	jne	.L1922
.L1915:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1932:
	.cfi_restore_state
	movq	104(%rdi), %r13
	cmpq	%rbx, %r13
	jnb	.L1917
	movq	%r13, 120(%rdi)
	movq	%r13, %rbx
.L1917:
	cmpb	$0, 56(%r12)
	jne	.L1918
	movq	8(%r12), %rdx
	cmpq	%rdx, 16(%r12)
	je	.L1918
	movb	$1, 432(%rax)
	movq	64(%r12), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$1, %r8d
	movl	$1, %ecx
	movl	%r13d, %r15d
	subl	%ebx, %r15d
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	16(%r12), %r14
	movq	8(%r12), %rbx
	cmpq	%r14, %rbx
	je	.L1921
	.p2align 4,,10
	.p2align 3
.L1920:
	movq	(%rbx), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r15d, %esi
	addq	$8, %rbx
	call	_ZN2v88internal18AllocationObserver14AllocationStepEimm@PLT
	cmpq	%rbx, %r14
	jne	.L1920
.L1921:
	movq	64(%r12), %rax
	movb	$0, 432(%rax)
.L1918:
	movq	%r13, 120(%r12)
	jmp	.L1916
	.cfi_endproc
.LFE22982:
	.size	_ZN2v88internal8NewSpace25ResetLinearAllocationAreaEv, .-_ZN2v88internal8NewSpace25ResetLinearAllocationAreaEv
	.section	.text._ZN2v88internal8NewSpace12AddFreshPageEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8NewSpace12AddFreshPageEv
	.type	_ZN2v88internal8NewSpace12AddFreshPageEv, @function
_ZN2v88internal8NewSpace12AddFreshPageEv:
.LFB22985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	64(%rdi), %rax
	movq	104(%rdi), %r13
	cmpb	$0, 432(%rax)
	jne	.L1934
	movq	120(%rdi), %rbx
	testq	%rbx, %rbx
	jne	.L1948
.L1934:
	movq	352(%r12), %rax
	movq	312(%r12), %rdx
	movq	224(%rax), %rcx
	movl	360(%r12), %eax
	shrq	$18, %rdx
	addl	$1, %eax
	cmpl	%edx, %eax
	je	.L1941
	testq	%rcx, %rcx
	je	.L1941
	movl	%eax, 360(%r12)
	leaq	-8(%r13), %rax
	movq	64(%r12), %rdi
	movq	%r13, %rsi
	andq	$-262144, %rax
	movq	%rcx, 352(%r12)
	movl	$1, %r8d
	movl	$1, %ecx
	movq	48(%rax), %rdx
	subl	%r13d, %edx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8NewSpace26UpdateLinearAllocationAreaEv
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1948:
	.cfi_restore_state
	cmpq	%rbx, %r13
	jnb	.L1935
	movq	%r13, 120(%rdi)
	movq	%r13, %rbx
.L1935:
	cmpb	$0, 56(%r12)
	jne	.L1936
	movq	8(%r12), %rsi
	cmpq	%rsi, 16(%r12)
	je	.L1936
	movb	$1, 432(%rax)
	movq	64(%r12), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$1, %r8d
	movl	$1, %ecx
	movl	%r13d, %r15d
	subl	%ebx, %r15d
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	16(%r12), %r14
	movq	8(%r12), %rbx
	cmpq	%r14, %rbx
	je	.L1939
	.p2align 4,,10
	.p2align 3
.L1938:
	movq	(%rbx), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r15d, %esi
	addq	$8, %rbx
	call	_ZN2v88internal18AllocationObserver14AllocationStepEimm@PLT
	cmpq	%rbx, %r14
	jne	.L1938
.L1939:
	movq	64(%r12), %rax
	movb	$0, 432(%rax)
.L1936:
	movq	%r13, 120(%r12)
	jmp	.L1934
	.p2align 4,,10
	.p2align 3
.L1941:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22985:
	.size	_ZN2v88internal8NewSpace12AddFreshPageEv, .-_ZN2v88internal8NewSpace12AddFreshPageEv
	.section	.text._ZN2v88internal8NewSpace24AddFreshPageSynchronizedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8NewSpace24AddFreshPageSynchronizedEv
	.type	_ZN2v88internal8NewSpace24AddFreshPageSynchronizedEv, @function
_ZN2v88internal8NewSpace24AddFreshPageSynchronizedEv:
.LFB22986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	152(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	64(%r12), %rax
	movq	104(%r12), %r13
	cmpb	$0, 432(%rax)
	jne	.L1950
	movq	120(%r12), %r15
	testq	%r15, %r15
	jne	.L1964
.L1950:
	movq	352(%r12), %rax
	movq	312(%r12), %rdx
	movq	224(%rax), %rcx
	movl	360(%r12), %eax
	shrq	$18, %rdx
	addl	$1, %eax
	cmpl	%edx, %eax
	je	.L1957
	testq	%rcx, %rcx
	je	.L1957
	movl	%eax, 360(%r12)
	leaq	-8(%r13), %rax
	movq	64(%r12), %rdi
	movq	%r13, %rsi
	andq	$-262144, %rax
	movq	%rcx, 352(%r12)
	movl	$1, %r8d
	movl	$1, %ecx
	movq	48(%rax), %rdx
	subl	%r13d, %edx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	%r12, %rdi
	movl	$1, %r12d
	call	_ZN2v88internal8NewSpace26UpdateLinearAllocationAreaEv
.L1956:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1964:
	.cfi_restore_state
	cmpq	%r15, %r13
	jnb	.L1951
	movq	%r13, 120(%r12)
	movq	%r13, %r15
.L1951:
	cmpb	$0, 56(%r12)
	jne	.L1952
	movq	8(%r12), %rsi
	cmpq	%rsi, 16(%r12)
	je	.L1952
	movb	$1, 432(%rax)
	movq	64(%r12), %rdi
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	$1, %r8d
	movl	$1, %ecx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	16(%r12), %rax
	movl	%r13d, %esi
	movq	8(%r12), %rbx
	subl	%r15d, %esi
	movq	%rax, -56(%rbp)
	movl	%esi, %r15d
	cmpq	%rax, %rbx
	je	.L1955
	.p2align 4,,10
	.p2align 3
.L1954:
	movq	(%rbx), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r15d, %esi
	addq	$8, %rbx
	call	_ZN2v88internal18AllocationObserver14AllocationStepEimm@PLT
	cmpq	%rbx, -56(%rbp)
	jne	.L1954
.L1955:
	movq	64(%r12), %rax
	movb	$0, 432(%rax)
.L1952:
	movq	%r13, 120(%r12)
	jmp	.L1950
	.p2align 4,,10
	.p2align 3
.L1957:
	xorl	%r12d, %r12d
	jmp	.L1956
	.cfi_endproc
.LFE22986:
	.size	_ZN2v88internal8NewSpace24AddFreshPageSynchronizedEv, .-_ZN2v88internal8NewSpace24AddFreshPageSynchronizedEv
	.section	.text._ZN2v88internal19SpaceWithLinearArea20InlineAllocationStepEmmmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SpaceWithLinearArea20InlineAllocationStepEmmmm
	.type	_ZN2v88internal19SpaceWithLinearArea20InlineAllocationStepEmmmm, @function
_ZN2v88internal19SpaceWithLinearArea20InlineAllocationStepEmmmm:
.LFB22994:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	64(%rdi), %rax
	movq	%rdx, -56(%rbp)
	cmpb	$0, 432(%rax)
	jne	.L1965
	movq	120(%rdi), %r15
	movq	%rdi, %rbx
	testq	%r15, %r15
	je	.L1965
	movq	%rsi, %r12
	movq	%rcx, %r14
	movq	%r8, %r13
	cmpq	%rsi, %r15
	jbe	.L1967
	movq	%rsi, 120(%rdi)
	movq	%rsi, %r15
.L1967:
	cmpb	$0, 56(%rbx)
	jne	.L1968
	movq	8(%rbx), %rdx
	cmpq	%rdx, 16(%rbx)
	je	.L1968
	movb	$1, 432(%rax)
	movl	%r13d, %edx
	movq	%r14, %rsi
	subl	%r15d, %r12d
	movq	64(%rbx), %rdi
	movl	$1, %r8d
	movl	$1, %ecx
	movslq	%r13d, %r13
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	16(%rbx), %rax
	movq	8(%rbx), %r15
	movq	%rax, -64(%rbp)
	cmpq	%rax, %r15
	je	.L1971
	.p2align 4,,10
	.p2align 3
.L1970:
	movq	(%r15), %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movl	%r12d, %esi
	addq	$8, %r15
	call	_ZN2v88internal18AllocationObserver14AllocationStepEimm@PLT
	cmpq	%r15, -64(%rbp)
	jne	.L1970
.L1971:
	movq	64(%rbx), %rax
	movb	$0, 432(%rax)
.L1968:
	movq	-56(%rbp), %rax
	movq	%rax, 120(%rbx)
.L1965:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22994:
	.size	_ZN2v88internal19SpaceWithLinearArea20InlineAllocationStepEmmmm, .-_ZN2v88internal19SpaceWithLinearArea20InlineAllocationStepEmmmm
	.section	.text._ZN2v88internal8NewSpace16EnsureAllocationEiNS0_19AllocationAlignmentE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8NewSpace16EnsureAllocationEiNS0_19AllocationAlignmentE
	.type	_ZN2v88internal8NewSpace16EnsureAllocationEiNS0_19AllocationAlignmentE, @function
_ZN2v88internal8NewSpace16EnsureAllocationEiNS0_19AllocationAlignmentE:
.LFB22987:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	104(%rdi), %r15
	movq	352(%rdi), %rax
	movl	%esi, -52(%rbp)
	movl	%edx, %esi
	movq	%r15, %rdi
	movq	48(%rax), %rbx
	call	_ZN2v88internal4Heap14GetFillToAlignEmNS0_19AllocationAlignmentE@PLT
	addl	%eax, %r13d
	movslq	%r13d, %r13
	leaq	0(%r13,%r15), %rcx
	cmpq	%rbx, %rcx
	ja	.L1989
	movl	$1, %r9d
	cmpq	112(%r12), %rbx
	ja	.L1990
.L1978:
	addq	$24, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1989:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8NewSpace12AddFreshPageEv
	movl	%eax, %r9d
	testb	%al, %al
	je	.L1978
	movq	104(%r12), %r15
	movq	352(%r12), %rax
	movl	%r14d, %esi
	movq	%r15, %rdi
	movq	48(%rax), %rbx
	call	_ZN2v88internal4Heap14GetFillToAlignEmNS0_19AllocationAlignmentE@PLT
	movl	$1, %r9d
	cmpq	112(%r12), %rbx
	jbe	.L1978
	.p2align 4,,10
	.p2align 3
.L1990:
	cltq
	leaq	0(%r13,%r15), %rsi
	movslq	-52(%rbp), %r8
	movq	%r12, %rdi
	leaq	(%rax,%r15), %rcx
	movq	%rsi, %rdx
	movb	%r9b, -53(%rbp)
	call	_ZN2v88internal19SpaceWithLinearArea20InlineAllocationStepEmmmm
	movq	(%r12), %rax
	movzbl	-53(%rbp), %r9d
	leaq	_ZN2v88internal8NewSpace27UpdateInlineAllocationLimitEm(%rip), %rdx
	movq	136(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1981
	movq	64(%r12), %rax
	movq	104(%r12), %rsi
	cmpb	$0, 1520(%rax)
	jne	.L1991
	movq	352(%r12), %rax
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	48(%rax), %rdx
	call	_ZN2v88internal19SpaceWithLinearArea12ComputeLimitEmmm.part.0
.L1983:
	movq	%rax, 112(%r12)
	movl	$1, %r9d
	jmp	.L1978
	.p2align 4,,10
	.p2align 3
.L1991:
	leaq	0(%r13,%rsi), %rax
	jmp	.L1983
	.p2align 4,,10
	.p2align 3
.L1981:
	movb	%r9b, -52(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	movzbl	-52(%rbp), %r9d
	jmp	.L1978
	.cfi_endproc
.LFE22987:
	.size	_ZN2v88internal8NewSpace16EnsureAllocationEiNS0_19AllocationAlignmentE, .-_ZN2v88internal8NewSpace16EnsureAllocationEiNS0_19AllocationAlignmentE
	.section	.text._ZN2v88internal9SemiSpace5SetUpEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9SemiSpace5SetUpEmm
	.type	_ZN2v88internal9SemiSpace5SetUpEmm, @function
_ZN2v88internal9SemiSpace5SetUpEmm:
.LFB22996:
	.cfi_startproc
	endbr64
	andq	$-262144, %rsi
	andq	$-262144, %rdx
	movb	$0, 136(%rdi)
	movq	%rsi, 120(%rdi)
	movq	%rsi, 104(%rdi)
	movq	%rdx, 112(%rdi)
	ret
	.cfi_endproc
.LFE22996:
	.size	_ZN2v88internal9SemiSpace5SetUpEmm, .-_ZN2v88internal9SemiSpace5SetUpEmm
	.section	.text._ZN2v88internal9SemiSpace13FixPagesFlagsEll,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9SemiSpace13FixPagesFlagsEll
	.type	_ZN2v88internal9SemiSpace13FixPagesFlagsEll, @function
_ZN2v88internal9SemiSpace13FixPagesFlagsEll:
.LFB23004:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L1993
	.p2align 4,,10
	.p2align 3
.L1998:
	movq	%rdi, 80(%rcx)
	mfence
	movq	8(%rcx), %r8
	movq	%r8, %rax
	xorq	%rsi, %rax
	andq	%rdx, %rax
	xorq	%r8, %rax
	movq	%rax, 8(%rcx)
	cmpl	$1, 140(%rdi)
	je	.L2003
	andq	$-17, %rax
	orq	$8, %rax
	movq	%rax, 8(%rcx)
	movq	224(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.L1998
.L1993:
	ret
	.p2align 4,,10
	.p2align 3
.L2003:
	andq	$-524297, %rax
	movq	$0, 96(%rcx)
	orq	$16, %rax
	movq	%rax, 8(%rcx)
	movq	224(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.L1998
	ret
	.cfi_endproc
.LFE23004:
	.size	_ZN2v88internal9SemiSpace13FixPagesFlagsEll, .-_ZN2v88internal9SemiSpace13FixPagesFlagsEll
	.section	.text._ZN2v88internal9SemiSpace5ResetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9SemiSpace5ResetEv
	.type	_ZN2v88internal9SemiSpace5ResetEv, @function
_ZN2v88internal9SemiSpace5ResetEv:
.LFB23005:
	.cfi_startproc
	endbr64
	movl	$0, 152(%rdi)
	movq	32(%rdi), %rax
	movq	%rax, 144(%rdi)
	ret
	.cfi_endproc
.LFE23005:
	.size	_ZN2v88internal9SemiSpace5ResetEv, .-_ZN2v88internal9SemiSpace5ResetEv
	.section	.text._ZN2v88internal9SemiSpace10RemovePageEPNS0_4PageE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9SemiSpace10RemovePageEPNS0_4PageE
	.type	_ZN2v88internal9SemiSpace10RemovePageEPNS0_4PageE, @function
_ZN2v88internal9SemiSpace10RemovePageEPNS0_4PageE:
.LFB23006:
	.cfi_startproc
	endbr64
	cmpq	%rsi, 144(%rdi)
	je	.L2020
.L2006:
	cmpq	40(%rdi), %rsi
	je	.L2021
.L2007:
	movq	224(%rsi), %rax
	cmpq	32(%rdi), %rsi
	je	.L2022
.L2008:
	movq	232(%rsi), %rdx
	testq	%rax, %rax
	je	.L2009
	movq	%rdx, 232(%rax)
.L2009:
	testq	%rdx, %rdx
	je	.L2010
	movq	%rax, 224(%rdx)
.L2010:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 224(%rsi)
	movq	200(%rsi), %rdx
	movq	48(%rdi), %rax
	lock subq	%rdx, (%rax)
	movq	64(%rdi), %rax
	lock subq	%rdx, 176(%rax)
	movq	208(%rsi), %rdx
	movq	48(%rdi), %rax
	lock subq	%rdx, 8(%rax)
	movq	64(%rdi), %rax
	lock subq	%rdx, 176(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L2020:
	movq	232(%rsi), %rax
	testq	%rax, %rax
	je	.L2006
	movq	%rax, 144(%rdi)
	cmpq	40(%rdi), %rsi
	jne	.L2007
.L2021:
	movq	232(%rsi), %rax
	movq	%rax, 40(%rdi)
	movq	224(%rsi), %rax
	cmpq	32(%rdi), %rsi
	jne	.L2008
.L2022:
	movq	%rax, 32(%rdi)
	movq	224(%rsi), %rax
	jmp	.L2008
	.cfi_endproc
.LFE23006:
	.size	_ZN2v88internal9SemiSpace10RemovePageEPNS0_4PageE, .-_ZN2v88internal9SemiSpace10RemovePageEPNS0_4PageE
	.section	.text._ZN2v88internal9SemiSpace11PrependPageEPNS0_4PageE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9SemiSpace11PrependPageEPNS0_4PageE
	.type	_ZN2v88internal9SemiSpace11PrependPageEPNS0_4PageE, @function
_ZN2v88internal9SemiSpace11PrependPageEPNS0_4PageE:
.LFB23007:
	.cfi_startproc
	endbr64
	movq	144(%rdi), %rax
	movq	8(%rax), %rax
	movq	%rax, 8(%rsi)
	movq	%rdi, 80(%rsi)
	mfence
	movq	32(%rdi), %rax
	testq	%rax, %rax
	je	.L2024
	movq	232(%rax), %rdx
	movq	%rax, 224(%rsi)
	movq	%rdx, 232(%rsi)
	movq	%rsi, 232(%rax)
	testq	%rdx, %rdx
	je	.L2025
	movq	%rsi, 224(%rdx)
.L2026:
	addl	$1, 152(%rdi)
	movq	200(%rsi), %rdx
	movq	48(%rdi), %rax
	lock addq	%rdx, (%rax)
	movq	64(%rdi), %rax
	lock addq	%rdx, 176(%rax)
	movq	208(%rsi), %rdx
	movq	48(%rdi), %rax
	lock addq	%rdx, 8(%rax)
	movq	64(%rdi), %rax
	lock addq	%rdx, 176(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L2025:
	movq	%rsi, 32(%rdi)
	jmp	.L2026
	.p2align 4,,10
	.p2align 3
.L2024:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 224(%rsi)
	movq	%rsi, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 32(%rdi)
	jmp	.L2026
	.cfi_endproc
.LFE23007:
	.size	_ZN2v88internal9SemiSpace11PrependPageEPNS0_4PageE, .-_ZN2v88internal9SemiSpace11PrependPageEPNS0_4PageE
	.section	.text._ZN2v88internal9SemiSpace4SwapEPS1_S2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9SemiSpace4SwapEPS1_S2_
	.type	_ZN2v88internal9SemiSpace4SwapEPS1_S2_, @function
_ZN2v88internal9SemiSpace4SwapEPS1_S2_:
.LFB23008:
	.cfi_startproc
	endbr64
	movq	144(%rsi), %rax
	movq	104(%rsi), %rdx
	movq	8(%rax), %r8
	movq	104(%rdi), %rax
	movq	%rdx, 104(%rdi)
	movq	112(%rsi), %rdx
	movq	%rax, 104(%rsi)
	movq	112(%rdi), %rax
	movq	%rdx, 112(%rdi)
	movq	120(%rsi), %rdx
	movq	%rax, 112(%rsi)
	movq	120(%rdi), %rax
	movq	%rdx, 120(%rdi)
	movq	128(%rsi), %rdx
	movq	%rax, 120(%rsi)
	movq	128(%rdi), %rax
	movq	%rdx, 128(%rdi)
	movzbl	136(%rsi), %edx
	movq	%rax, 128(%rsi)
	movzbl	136(%rdi), %eax
	movb	%dl, 136(%rdi)
	movdqu	32(%rsi), %xmm1
	movb	%al, 136(%rsi)
	movq	40(%rdi), %rax
	movq	32(%rdi), %xmm0
	movups	%xmm1, 32(%rdi)
	movq	144(%rsi), %rdx
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 32(%rsi)
	movq	144(%rdi), %rax
	movq	%rdx, 144(%rdi)
	movq	%rax, 144(%rsi)
	movq	48(%rdi), %rax
	movq	48(%rsi), %rdx
	movq	%rdx, 48(%rdi)
	movq	32(%rsi), %rdx
	movq	%rax, 48(%rsi)
	testq	%rdx, %rdx
	je	.L2033
	.p2align 4,,10
	.p2align 3
.L2034:
	movq	%r8, %rax
	movq	%rsi, 80(%rdx)
	mfence
	movq	8(%rdx), %rcx
	xorq	%rcx, %rax
	andl	$262150, %eax
	xorq	%rcx, %rax
	movq	%rax, 8(%rdx)
	cmpl	$1, 140(%rsi)
	je	.L2046
	andq	$-17, %rax
	orq	$8, %rax
	movq	%rax, 8(%rdx)
	movq	224(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2034
.L2033:
	movq	32(%rdi), %rax
	testq	%rax, %rax
	je	.L2047
	.p2align 4,,10
	.p2align 3
.L2037:
	movq	%rdi, 80(%rax)
	mfence
	cmpl	$1, 140(%rdi)
	movq	8(%rax), %rdx
	je	.L2048
	andq	$-17, %rdx
	orq	$8, %rdx
	movq	%rdx, 8(%rax)
	movq	224(%rax), %rax
	testq	%rax, %rax
	jne	.L2037
	ret
	.p2align 4,,10
	.p2align 3
.L2048:
	andq	$-524297, %rdx
	movq	$0, 96(%rax)
	orq	$16, %rdx
	movq	%rdx, 8(%rax)
	movq	224(%rax), %rax
	testq	%rax, %rax
	jne	.L2037
	ret
	.p2align 4,,10
	.p2align 3
.L2046:
	andq	$-524297, %rax
	movq	$0, 96(%rdx)
	orq	$16, %rax
	movq	%rax, 8(%rdx)
	movq	224(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2034
	movq	32(%rdi), %rax
	testq	%rax, %rax
	jne	.L2037
.L2047:
	ret
	.cfi_endproc
.LFE23008:
	.size	_ZN2v88internal9SemiSpace4SwapEPS1_S2_, .-_ZN2v88internal9SemiSpace4SwapEPS1_S2_
	.section	.text._ZN2v88internal9SemiSpace12set_age_markEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9SemiSpace12set_age_markEm
	.type	_ZN2v88internal9SemiSpace12set_age_markEm, @function
_ZN2v88internal9SemiSpace12set_age_markEm:
.LFB23009:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	%rsi, 128(%rdi)
	subq	$8, %rsi
	andq	$-262144, %rsi
	movq	40(%rax), %rax
	movq	224(%rsi), %rdx
	andq	$-262144, %rax
	cmpq	%rdx, %rax
	je	.L2049
	.p2align 4,,10
	.p2align 3
.L2051:
	orq	$524288, 8(%rax)
	movq	224(%rax), %rax
	cmpq	%rax, %rdx
	jne	.L2051
.L2049:
	ret
	.cfi_endproc
.LFE23009:
	.size	_ZN2v88internal9SemiSpace12set_age_markEm, .-_ZN2v88internal9SemiSpace12set_age_markEm
	.section	.text._ZN2v88internal23SemiSpaceObjectIteratorC2EPNS0_8NewSpaceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23SemiSpaceObjectIteratorC2EPNS0_8NewSpaceE
	.type	_ZN2v88internal23SemiSpaceObjectIteratorC2EPNS0_8NewSpaceE, @function
_ZN2v88internal23SemiSpaceObjectIteratorC2EPNS0_8NewSpaceE:
.LFB23012:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal23SemiSpaceObjectIteratorE(%rip), %rax
	movq	%rax, (%rdi)
	movq	240(%rsi), %rax
	movq	40(%rax), %xmm0
	movhps	104(%rsi), %xmm0
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE23012:
	.size	_ZN2v88internal23SemiSpaceObjectIteratorC2EPNS0_8NewSpaceE, .-_ZN2v88internal23SemiSpaceObjectIteratorC2EPNS0_8NewSpaceE
	.globl	_ZN2v88internal23SemiSpaceObjectIteratorC1EPNS0_8NewSpaceE
	.set	_ZN2v88internal23SemiSpaceObjectIteratorC1EPNS0_8NewSpaceE,_ZN2v88internal23SemiSpaceObjectIteratorC2EPNS0_8NewSpaceE
	.section	.text._ZN2v88internal23SemiSpaceObjectIterator10InitializeEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23SemiSpaceObjectIterator10InitializeEmm
	.type	_ZN2v88internal23SemiSpaceObjectIterator10InitializeEmm, @function
_ZN2v88internal23SemiSpaceObjectIterator10InitializeEmm:
.LFB23014:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE23014:
	.size	_ZN2v88internal23SemiSpaceObjectIterator10InitializeEmm, .-_ZN2v88internal23SemiSpaceObjectIterator10InitializeEmm
	.section	.text._ZN2v88internal16FreeListCategory5ResetEPNS0_8FreeListE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16FreeListCategory5ResetEPNS0_8FreeListE
	.type	_ZN2v88internal16FreeListCategory5ResetEPNS0_8FreeListE, @function
_ZN2v88internal16FreeListCategory5ResetEPNS0_8FreeListE:
.LFB23016:
	.cfi_startproc
	endbr64
	cmpq	$0, 16(%rdi)
	je	.L2058
.L2056:
	cmpq	$0, 8(%rdi)
	je	.L2057
	movl	4(%rdi), %eax
	subq	%rax, 40(%rsi)
.L2057:
	pxor	%xmm0, %xmm0
	movq	$0, 8(%rdi)
	movl	$0, 4(%rdi)
	movups	%xmm0, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L2058:
	cmpq	$0, 24(%rdi)
	jne	.L2056
	movslq	(%rdi), %rdx
	movq	32(%rsi), %rax
	cmpq	(%rax,%rdx,8), %rdi
	jne	.L2057
	jmp	.L2056
	.cfi_endproc
.LFE23016:
	.size	_ZN2v88internal16FreeListCategory5ResetEPNS0_8FreeListE, .-_ZN2v88internal16FreeListCategory5ResetEPNS0_8FreeListE
	.section	.text._ZN2v88internal16FreeListCategory16PickNodeFromListEmPm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16FreeListCategory16PickNodeFromListEmPm
	.type	_ZN2v88internal16FreeListCategory16PickNodeFromListEmPm, @function
_ZN2v88internal16FreeListCategory16PickNodeFromListEmPm:
.LFB23017:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movslq	11(%rax), %rcx
	cmpq	%rsi, %rcx
	jnb	.L2060
	movq	$0, (%rdx)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2060:
	movq	15(%rax), %rcx
	movq	%rcx, 8(%rdi)
	movslq	11(%rax), %rcx
	movq	%rcx, (%rdx)
	subl	%ecx, 4(%rdi)
	ret
	.cfi_endproc
.LFE23017:
	.size	_ZN2v88internal16FreeListCategory16PickNodeFromListEmPm, .-_ZN2v88internal16FreeListCategory16PickNodeFromListEmPm
	.section	.text._ZN2v88internal16FreeListCategory19SearchForNodeInListEmPm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16FreeListCategory19SearchForNodeInListEmPm
	.type	_ZN2v88internal16FreeListCategory19SearchForNodeInListEmPm, @function
_ZN2v88internal16FreeListCategory19SearchForNodeInListEmPm:
.LFB23018:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L2080
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	jmp	.L2071
	.p2align 4,,10
	.p2align 3
.L2064:
	movq	15(%rax), %rcx
	movq	%rax, %r12
	testq	%rcx, %rcx
	je	.L2084
	movq	%rcx, %rax
.L2071:
	movslq	11(%rax), %rbx
	cmpq	%rsi, %rbx
	jb	.L2064
	subl	%ebx, 4(%rdi)
	cmpq	%rax, %r8
	jne	.L2065
	movq	15(%rax), %rcx
	movq	%rcx, 8(%rdi)
.L2065:
	testq	%r12, %r12
	je	.L2066
	movq	%r12, %rsi
	andq	$-262144, %rsi
	testb	$32, 10(%rsi)
	je	.L2085
.L2068:
	movq	15(%rax), %rcx
	movq	%rcx, 15(%r12)
.L2066:
	movq	%rbx, (%rdx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2084:
	.cfi_restore_state
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2085:
	.cfi_restore_state
	movq	80(%rsi), %rcx
	cmpl	$3, 72(%rcx)
	jne	.L2068
	movq	24(%rsi), %rdi
	movq	%rdx, -32(%rbp)
	movq	%rax, -24(%rbp)
	call	_ZN2v88internal4Heap31UnprotectAndRegisterMemoryChunkEPNS0_11MemoryChunkE@PLT
	movq	-32(%rbp), %rdx
	movq	-24(%rbp), %rax
	jmp	.L2068
.L2080:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE23018:
	.size	_ZN2v88internal16FreeListCategory19SearchForNodeInListEmPm, .-_ZN2v88internal16FreeListCategory19SearchForNodeInListEmPm
	.section	.text._ZN2v88internal12FreeListMany8AllocateEmPmNS0_16AllocationOriginE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12FreeListMany8AllocateEmPmNS0_16AllocationOriginE
	.type	_ZN2v88internal12FreeListMany8AllocateEmPmNS0_16AllocationOriginE, @function
_ZN2v88internal12FreeListMany8AllocateEmPmNS0_16AllocationOriginE:
.LFB23065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal12FreeListMany26SelectFreeListCategoryTypeEm(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2087
	movl	12(%rdi), %edi
	movslq	%edi, %rcx
	cmpq	$256, %rsi
	jbe	.L2140
	cmpl	$15, %edi
	jle	.L2091
	cmpq	$511, %rsi
	jbe	.L2105
	cmpl	$16, %edi
	je	.L2091
	cmpq	$1023, %rsi
	jbe	.L2106
	cmpl	$17, %edi
	je	.L2091
	cmpq	$2047, %rsi
	jbe	.L2107
	cmpl	$18, %edi
	je	.L2091
	cmpq	$4095, %rsi
	jbe	.L2108
	cmpl	$19, %edi
	je	.L2091
	cmpq	$8191, %rsi
	jbe	.L2109
	cmpl	$20, %edi
	je	.L2091
	cmpq	$16383, %rsi
	jbe	.L2110
	cmpl	$21, %edi
	je	.L2091
	cmpq	$32767, %rsi
	jbe	.L2111
	cmpl	$22, %edi
	je	.L2091
	movl	$23, %ecx
	movl	$22, %ebx
	cmpq	$65535, %rsi
	jbe	.L2090
.L2091:
	movq	32(%r13), %rax
	movq	(%rax,%rcx,8), %r14
	jmp	.L2102
	.p2align 4,,10
	.p2align 3
.L2142:
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rdx, -56(%rbp)
	movq	24(%r14), %rbx
	call	_ZN2v88internal16FreeListCategory19SearchForNodeInListEmPm
	movq	-56(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.L2141
	movq	%rbx, %r14
.L2102:
	testq	%r14, %r14
	jne	.L2142
	xorl	%r12d, %r12d
	jmp	.L2101
	.p2align 4,,10
	.p2align 3
.L2140:
	cmpq	$31, %rsi
	ja	.L2089
	xorl	%ebx, %ebx
	testl	%edi, %edi
	jle	.L2091
.L2090:
	movslq	%ebx, %r14
	movq	32(%r13), %rax
	salq	$3, %r14
	jmp	.L2093
	.p2align 4,,10
	.p2align 3
.L2145:
	movq	$0, (%rdx)
	xorl	%r12d, %r12d
.L2098:
	cmpq	$0, 8(%rsi)
	jne	.L2099
	movq	0(%r13), %rax
	movq	%rdx, -56(%rbp)
	movq	%r13, %rdi
	call	*64(%rax)
	movq	-56(%rbp), %rdx
.L2099:
	movslq	12(%r13), %rcx
	movl	%ecx, %edi
	cmpl	%ebx, %ecx
	jle	.L2143
	testq	%r12, %r12
	jne	.L2094
	movq	32(%r13), %rax
.L2096:
	addq	$8, %r14
.L2093:
	movq	(%rax,%r14), %rsi
	addl	$1, %ebx
	testq	%rsi, %rsi
	je	.L2144
	movq	8(%rsi), %r12
	movslq	11(%r12), %rax
	cmpq	%rax, %r15
	ja	.L2145
	movq	15(%r12), %rax
	movq	%rax, 8(%rsi)
	movslq	11(%r12), %rax
	movq	%rax, (%rdx)
	subl	%eax, 4(%rsi)
	testq	%r12, %r12
	je	.L2098
	movq	(%rdx), %rax
	subq	%rax, 40(%r13)
	jmp	.L2098
	.p2align 4,,10
	.p2align 3
.L2144:
	movslq	%edi, %rcx
	cmpl	%ebx, %edi
	jg	.L2096
	jmp	.L2091
	.p2align 4,,10
	.p2align 3
.L2141:
	movq	(%rdx), %rax
	subq	%rax, 40(%r13)
	cmpq	$0, 8(%r14)
	jne	.L2094
	movq	0(%r13), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	*64(%rax)
	movq	-56(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L2094:
	movq	%r12, %rax
	movq	(%rdx), %rdx
	andq	$-262144, %rax
	addq	%rdx, 192(%rax)
.L2101:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2143:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L2094
	jmp	.L2091
	.p2align 4,,10
	.p2align 3
.L2087:
	movq	%rdx, -56(%rbp)
	call	*%rax
	movl	12(%r13), %edi
	movq	-56(%rbp), %rdx
	movl	%eax, %ebx
	cmpl	%edi, %eax
	movslq	%edi, %rcx
	jl	.L2090
	jmp	.L2091
	.p2align 4,,10
	.p2align 3
.L2089:
	movq	%rsi, %rbx
	shrq	$4, %rbx
	subl	$1, %ebx
	cmpl	%edi, %ebx
	jl	.L2090
	jmp	.L2091
.L2105:
	movl	$15, %ebx
	jmp	.L2090
.L2106:
	movl	$16, %ebx
	jmp	.L2090
.L2107:
	movl	$17, %ebx
	jmp	.L2090
.L2108:
	movl	$18, %ebx
	jmp	.L2090
.L2109:
	movl	$19, %ebx
	jmp	.L2090
.L2110:
	movl	$20, %ebx
	jmp	.L2090
.L2111:
	movl	$21, %ebx
	jmp	.L2090
	.cfi_endproc
.LFE23065:
	.size	_ZN2v88internal12FreeListMany8AllocateEmPmNS0_16AllocationOriginE, .-_ZN2v88internal12FreeListMany8AllocateEmPmNS0_16AllocationOriginE
	.section	.text._ZN2v88internal18FreeListManyCached8AllocateEmPmNS0_16AllocationOriginE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18FreeListManyCached8AllocateEmPmNS0_16AllocationOriginE
	.type	_ZN2v88internal18FreeListManyCached8AllocateEmPmNS0_16AllocationOriginE, @function
_ZN2v88internal18FreeListManyCached8AllocateEmPmNS0_16AllocationOriginE:
.LFB23073:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	leaq	_ZN2v88internal12FreeListMany26SelectFreeListCategoryTypeEm(%rip), %rdx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2147
	movl	12(%rdi), %r13d
	cmpq	$256, %rsi
	jbe	.L2198
	cmpl	$15, %r13d
	jle	.L2169
	cmpq	$511, %rsi
	jbe	.L2170
	cmpl	$16, %r13d
	je	.L2171
	cmpq	$1023, %rsi
	jbe	.L2171
	cmpl	$17, %r13d
	je	.L2173
	cmpq	$2047, %rsi
	jbe	.L2173
	cmpl	$18, %r13d
	je	.L2175
	cmpq	$4095, %rsi
	jbe	.L2175
	cmpl	$19, %r13d
	je	.L2177
	cmpq	$8191, %rsi
	jbe	.L2177
	cmpl	$20, %r13d
	je	.L2179
	cmpq	$16383, %rsi
	jbe	.L2179
	cmpl	$21, %r13d
	je	.L2181
	cmpq	$32767, %rsi
	jbe	.L2181
	cmpl	$22, %r13d
	je	.L2183
	cmpq	$65536, %rsi
	movl	$23, %edx
	sbbl	%eax, %eax
	addl	$23, %eax
	cmpq	$65535, %rsi
	cmova	%edx, %r13d
	jmp	.L2149
	.p2align 4,,10
	.p2align 3
.L2198:
	movq	%rsi, %rax
	movl	$0, %edx
	shrq	$4, %rax
	subl	$1, %eax
	cmpq	$31, %rsi
	cmovbe	%edx, %eax
.L2149:
	cltq
	movq	32(%r12), %rdx
	movl	48(%r12,%rax,4), %ebx
	cmpl	%r13d, %ebx
	jl	.L2158
	jmp	.L2151
	.p2align 4,,10
	.p2align 3
.L2200:
	movq	$0, (%r15)
.L2154:
	cmpq	$0, 8(%rsi)
	je	.L2199
.L2197:
	movq	32(%r12), %rdx
	movl	12(%r12), %r13d
.L2152:
	addl	$1, %ebx
	movslq	%ebx, %rbx
	movl	48(%r12,%rbx,4), %ebx
	cmpl	%r13d, %ebx
	jge	.L2151
.L2158:
	movslq	%ebx, %rax
	movq	(%rdx,%rax,8), %rsi
	leaq	0(,%rax,8), %rcx
	testq	%rsi, %rsi
	je	.L2152
	movq	8(%rsi), %rax
	movslq	11(%rax), %rdx
	cmpq	%rdx, %r14
	ja	.L2200
	movq	15(%rax), %rdx
	movq	%rdx, 8(%rsi)
	movslq	11(%rax), %rdx
	movq	%rdx, (%r15)
	subl	%edx, 4(%rsi)
	testq	%rax, %rax
	je	.L2154
	movq	(%r15), %rdx
	subq	%rdx, 40(%r12)
	cmpq	$0, 8(%rsi)
	jne	.L2201
	movq	(%r12), %rdx
	movq	%rax, -64(%rbp)
	movq	%r12, %rdi
	movq	%rcx, -56(%rbp)
	call	*64(%rdx)
	movq	32(%r12), %rdx
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L2157:
	cmpq	$0, (%rdx,%rcx)
	je	.L2202
.L2162:
	movq	%rax, %rdx
	movq	(%r15), %rcx
	andq	$-262144, %rdx
	addq	%rcx, 192(%rdx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2199:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*64(%rax)
	jmp	.L2197
	.p2align 4,,10
	.p2align 3
.L2201:
	movq	32(%r12), %rdx
	cmpq	$0, (%rdx,%rcx)
	jne	.L2162
.L2202:
	testl	%ebx, %ebx
	js	.L2162
	leal	1(%rbx), %esi
	movslq	%ebx, %rdx
	leaq	-4(%r12), %rdi
	movslq	%esi, %rsi
	leaq	(%r12,%rdx,4), %rdx
	addq	$12, %rsi
	jmp	.L2163
	.p2align 4,,10
	.p2align 3
.L2203:
	movl	(%r12,%rsi,4), %ecx
	subq	$4, %rdx
	movl	%ecx, 52(%rdx)
	cmpq	%rdi, %rdx
	je	.L2162
.L2163:
	cmpl	%ebx, 48(%rdx)
	je	.L2203
	jmp	.L2162
	.p2align 4,,10
	.p2align 3
.L2151:
	movslq	%r13d, %rax
	movq	(%rdx,%rax,8), %rbx
	leaq	0(,%rax,8), %rcx
	jmp	.L2160
	.p2align 4,,10
	.p2align 3
.L2205:
	movq	24(%rbx), %rax
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal16FreeListCategory19SearchForNodeInListEmPm
	movq	-56(%rbp), %rcx
	testq	%rax, %rax
	jne	.L2204
	movq	-64(%rbp), %rbx
.L2160:
	movq	%rcx, -56(%rbp)
	testq	%rbx, %rbx
	jne	.L2205
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2147:
	.cfi_restore_state
	call	*%rax
	movl	12(%r12), %r13d
	jmp	.L2149
	.p2align 4,,10
	.p2align 3
.L2204:
	movq	(%r15), %rdx
	subq	%rdx, 40(%r12)
	cmpq	$0, 8(%rbx)
	jne	.L2161
	movq	(%r12), %rdx
	movq	%rax, -64(%rbp)
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	*64(%rdx)
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rcx
.L2161:
	movq	32(%r12), %rdx
	movl	%r13d, %ebx
	jmp	.L2157
.L2181:
	movl	$21, %eax
	jmp	.L2149
.L2171:
	movl	$16, %eax
	jmp	.L2149
.L2173:
	movl	$17, %eax
	jmp	.L2149
.L2175:
	movl	$18, %eax
	jmp	.L2149
.L2177:
	movl	$19, %eax
	jmp	.L2149
.L2179:
	movl	$20, %eax
	jmp	.L2149
.L2169:
	movl	%r13d, %eax
	jmp	.L2149
.L2170:
	movl	$15, %eax
	jmp	.L2149
.L2183:
	movl	$22, %eax
	jmp	.L2149
	.cfi_endproc
.LFE23073:
	.size	_ZN2v88internal18FreeListManyCached8AllocateEmPmNS0_16AllocationOriginE, .-_ZN2v88internal18FreeListManyCached8AllocateEmPmNS0_16AllocationOriginE
	.section	.text._ZN2v88internal16FreeListCategory4FreeEmmNS0_8FreeModeEPNS0_8FreeListE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16FreeListCategory4FreeEmmNS0_8FreeModeEPNS0_8FreeListE
	.type	_ZN2v88internal16FreeListCategory4FreeEmmNS0_8FreeModeEPNS0_8FreeListE, @function
_ZN2v88internal16FreeListCategory4FreeEmmNS0_8FreeModeEPNS0_8FreeListE:
.LFB23019:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	addq	$1, %rsi
	movq	%rax, 15(%rsi)
	movq	%rsi, 8(%rdi)
	addl	%edx, 4(%rdi)
	testl	%ecx, %ecx
	jne	.L2206
	cmpq	$0, 16(%rdi)
	je	.L2209
.L2208:
	addq	%rdx, 40(%r8)
.L2206:
	ret
	.p2align 4,,10
	.p2align 3
.L2209:
	cmpq	$0, 24(%rdi)
	jne	.L2208
	movslq	(%rdi), %rcx
	movq	32(%r8), %rax
	cmpq	(%rax,%rcx,8), %rdi
	je	.L2208
	movq	(%r8), %rax
	movq	%rdi, %rsi
	movq	%r8, %rdi
	movq	56(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE23019:
	.size	_ZN2v88internal16FreeListCategory4FreeEmmNS0_8FreeModeEPNS0_8FreeListE, .-_ZN2v88internal16FreeListCategory4FreeEmmNS0_8FreeModeEPNS0_8FreeListE
	.section	.text._ZN2v88internal16FreeListCategory14RepairFreeListEPNS0_4HeapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16FreeListCategory14RepairFreeListEPNS0_4HeapE
	.type	_ZN2v88internal16FreeListCategory14RepairFreeListEPNS0_4HeapE, @function
_ZN2v88internal16FreeListCategory14RepairFreeListEPNS0_4HeapE:
.LFB23020:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	-37536(%rsi), %rcx
	testq	%rax, %rax
	je	.L2210
	.p2align 4,,10
	.p2align 3
.L2214:
	movq	-1(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L2212
	movq	%rcx, -1(%rax)
	movq	15(%rax), %rax
	testq	%rax, %rax
	jne	.L2214
	ret
	.p2align 4,,10
	.p2align 3
.L2212:
	movq	15(%rax), %rax
	testq	%rax, %rax
	jne	.L2214
.L2210:
	ret
	.cfi_endproc
.LFE23020:
	.size	_ZN2v88internal16FreeListCategory14RepairFreeListEPNS0_4HeapE, .-_ZN2v88internal16FreeListCategory14RepairFreeListEPNS0_4HeapE
	.section	.text._ZN2v88internal16FreeListCategory6RelinkEPNS0_8FreeListE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16FreeListCategory6RelinkEPNS0_8FreeListE
	.type	_ZN2v88internal16FreeListCategory6RelinkEPNS0_8FreeListE, @function
_ZN2v88internal16FreeListCategory6RelinkEPNS0_8FreeListE:
.LFB23021:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%r8, %rsi
	movq	56(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE23021:
	.size	_ZN2v88internal16FreeListCategory6RelinkEPNS0_8FreeListE, .-_ZN2v88internal16FreeListCategory6RelinkEPNS0_8FreeListE
	.section	.rodata._ZN2v88internal8FreeList14CreateFreeListEv.str1.1,"aMS",@progbits,1
.LC25:
	.string	"Invalid FreeList strategy"
	.section	.text._ZN2v88internal8FreeList14CreateFreeListEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8FreeList14CreateFreeListEv
	.type	_ZN2v88internal8FreeList14CreateFreeListEv, @function
_ZN2v88internal8FreeList14CreateFreeListEv:
.LFB23022:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpl	$5, _ZN2v88internal25FLAG_gc_freelist_strategyE(%rip)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	ja	.L2223
	movl	_ZN2v88internal25FLAG_gc_freelist_strategyE(%rip), %eax
	leaq	.L2225(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8FreeList14CreateFreeListEv,"a",@progbits
	.align 4
	.align 4
.L2225:
	.long	.L2230-.L2225
	.long	.L2229-.L2225
	.long	.L2228-.L2225
	.long	.L2227-.L2225
	.long	.L2226-.L2225
	.long	.L2224-.L2225
	.section	.text._ZN2v88internal8FreeList14CreateFreeListEv
	.p2align 4,,10
	.p2align 3
.L2226:
	movl	$152, %edi
	xorl	%ebx, %ebx
	call	_Znwm@PLT
	movl	$19, %ecx
	movdqa	.LC23(%rip), %xmm0
	movq	%rax, %r12
	movq	%rax, %rdi
	movq	%rbx, %rax
	rep stosq
	leaq	16+_ZTVN2v88internal12FreeListManyE(%rip), %rax
	movups	%xmm0, 16(%r12)
	movl	$192, %edi
	movq	%rax, (%r12)
	movabsq	$98784247832, %rax
	movq	%rax, 8(%r12)
	call	_Znam@PLT
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	movq	$0, (%rax)
	movq	%rax, %rdx
	movq	$0, 184(%rax)
	andq	$-8, %rdi
	movq	%rbx, %rax
	subq	%rdi, %rcx
	addl	$192, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	%rdx, 32(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal8FreeList5ResetEv
	movdqa	.LC6(%rip), %xmm0
	leaq	16+_ZTVN2v88internal26FreeListManyCachedFastPathE(%rip), %rax
	movl	$24, 144(%r12)
	movq	%rax, (%r12)
	movq	%r12, %rax
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	movups	%xmm0, 112(%r12)
	movups	%xmm0, 128(%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2224:
	.cfi_restore_state
	movl	$152, %edi
	xorl	%ebx, %ebx
	call	_Znwm@PLT
	movl	$19, %ecx
	movdqa	.LC23(%rip), %xmm0
	movq	%rax, %r12
	movq	%rax, %rdi
	movq	%rbx, %rax
	rep stosq
	leaq	16+_ZTVN2v88internal12FreeListManyE(%rip), %rax
	movups	%xmm0, 16(%r12)
	movl	$192, %edi
	movq	%rax, (%r12)
	movabsq	$98784247832, %rax
	movq	%rax, 8(%r12)
	call	_Znam@PLT
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	movq	$0, (%rax)
	movq	%rax, %rdx
	movq	$0, 184(%rax)
	andq	$-8, %rdi
	movq	%rbx, %rax
	subq	%rdi, %rcx
	addl	$192, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	%rdx, 32(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal8FreeList5ResetEv
	movdqa	.LC6(%rip), %xmm0
	leaq	16+_ZTVN2v88internal24FreeListManyCachedOriginE(%rip), %rax
	movl	$24, 144(%r12)
	movq	%rax, (%r12)
	movq	%r12, %rax
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	movups	%xmm0, 112(%r12)
	movups	%xmm0, 128(%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2230:
	.cfi_restore_state
	movl	$48, %edi
	call	_Znwm@PLT
	movdqa	.LC23(%rip), %xmm0
	movl	$48, %edi
	movq	%rax, %r12
	leaq	16+_ZTVN2v88internal14FreeListLegacyE(%rip), %rax
	movq	%rax, (%r12)
	movabsq	$21474836486, %rax
	movq	%rax, 8(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movups	%xmm0, 16(%r12)
	call	_Znam@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, 32(%r12)
	call	_ZN2v88internal8FreeList5ResetEv
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2229:
	.cfi_restore_state
	movl	$48, %edi
	call	_Znwm@PLT
	movdqa	.LC24(%rip), %xmm0
	movl	$24, %edi
	movq	%rax, %r12
	leaq	16+_ZTVN2v88internal17FreeListFastAllocE(%rip), %rax
	movq	%rax, (%r12)
	movabsq	$8589934595, %rax
	movq	%rax, 8(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movups	%xmm0, 16(%r12)
	call	_Znam@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, 32(%r12)
	call	_ZN2v88internal8FreeList5ResetEv
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2228:
	.cfi_restore_state
	movl	$48, %edi
	call	_Znwm@PLT
	movdqa	.LC23(%rip), %xmm0
	movl	$192, %edi
	movq	%rax, %r12
	leaq	16+_ZTVN2v88internal12FreeListManyE(%rip), %rax
	movq	%rax, (%r12)
	movabsq	$98784247832, %rax
	movq	%rax, 8(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movups	%xmm0, 16(%r12)
	call	_Znam@PLT
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	movq	$0, (%rax)
	movq	%rax, %rdx
	movq	$0, 184(%rax)
	andq	$-8, %rdi
	xorl	%eax, %eax
	subq	%rdi, %rcx
	addl	$192, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	%rdx, 32(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal8FreeList5ResetEv
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2227:
	.cfi_restore_state
	movl	$152, %edi
	call	_Znwm@PLT
	movdqa	.LC23(%rip), %xmm0
	movl	$192, %edi
	movq	%rax, %r12
	leaq	16+_ZTVN2v88internal12FreeListManyE(%rip), %rax
	movq	%rax, (%r12)
	movabsq	$98784247832, %rax
	movq	%rax, 8(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movups	%xmm0, 16(%r12)
	call	_Znam@PLT
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	movq	$0, (%rax)
	movq	%rax, %rdx
	movq	$0, 184(%rax)
	andq	$-8, %rdi
	xorl	%eax, %eax
	subq	%rdi, %rcx
	addl	$192, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	%rdx, 32(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal8FreeList5ResetEv
	movdqa	.LC6(%rip), %xmm0
	leaq	16+_ZTVN2v88internal18FreeListManyCachedE(%rip), %rax
	movl	$24, 144(%r12)
	movq	%rax, (%r12)
	movq	%r12, %rax
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	movups	%xmm0, 112(%r12)
	movups	%xmm0, 128(%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2223:
	.cfi_restore_state
	leaq	.LC25(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23022:
	.size	_ZN2v88internal8FreeList14CreateFreeListEv, .-_ZN2v88internal8FreeList14CreateFreeListEv
	.section	.text._ZN2v88internal8FreeList13TryFindNodeInEimPm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8FreeList13TryFindNodeInEimPm
	.type	_ZN2v88internal8FreeList13TryFindNodeInEimPm, @function
_ZN2v88internal8FreeList13TryFindNodeInEimPm:
.LFB23037:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rsi
	testq	%rsi, %rsi
	je	.L2245
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	8(%rsi), %rbx
	movslq	11(%rbx), %rax
	cmpq	%rax, %rdx
	ja	.L2246
	movq	15(%rbx), %rax
	movq	%rax, 8(%rsi)
	movslq	11(%rbx), %rax
	movq	%rax, (%rcx)
	subl	%eax, 4(%rsi)
	testq	%rbx, %rbx
	je	.L2237
	movq	(%rcx), %rax
	subq	%rax, 40(%rdi)
.L2237:
	cmpq	$0, 8(%rsi)
	jne	.L2238
	movq	(%rdi), %rax
	call	*64(%rax)
.L2238:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2246:
	.cfi_restore_state
	movq	$0, (%rcx)
	xorl	%ebx, %ebx
	jmp	.L2237
	.p2align 4,,10
	.p2align 3
.L2245:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE23037:
	.size	_ZN2v88internal8FreeList13TryFindNodeInEimPm, .-_ZN2v88internal8FreeList13TryFindNodeInEimPm
	.section	.text._ZN2v88internal14FreeListLegacy8AllocateEmPmNS0_16AllocationOriginE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FreeListLegacy8AllocateEmPmNS0_16AllocationOriginE
	.type	_ZN2v88internal14FreeListLegacy8AllocateEmPmNS0_16AllocationOriginE, @function
_ZN2v88internal14FreeListLegacy8AllocateEmPmNS0_16AllocationOriginE:
.LFB23047:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	$248, %rsi
	jbe	.L2268
	cmpq	$2040, %rsi
	jbe	.L2269
	movl	$5, -60(%rbp)
	cmpq	$16376, %rsi
	jbe	.L2287
.L2249:
	movq	32(%r15), %rax
	movq	40(%rax), %r13
	jmp	.L2259
	.p2align 4,,10
	.p2align 3
.L2289:
	movq	%r8, %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	movq	24(%r13), %rbx
	call	_ZN2v88internal16FreeListCategory19SearchForNodeInListEmPm
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.L2288
	movq	%rbx, %r13
.L2259:
	testq	%r13, %r13
	jne	.L2289
	cmpl	$5, -60(%rbp)
	je	.L2267
	movq	(%r15), %rax
	leaq	_ZN2v88internal14FreeListLegacy26SelectFreeListCategoryTypeEm(%rip), %rdx
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2263
	cmpq	$80, %r8
	jbe	.L2266
	movl	$1, %esi
	cmpq	$248, %r8
	jbe	.L2265
	movl	$2, %esi
	cmpq	$2040, %r8
	jbe	.L2265
	movl	$3, %esi
	cmpq	$16376, %r8
	jbe	.L2265
	xorl	%esi, %esi
	cmpq	$65528, %r8
	seta	%sil
	addl	$4, %esi
.L2265:
	movq	%r14, %rcx
	movq	%r8, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8FreeList13TryFindNodeInEimPm
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L2251
.L2267:
	xorl	%r12d, %r12d
	jmp	.L2262
	.p2align 4,,10
	.p2align 3
.L2269:
	movl	$3, -60(%rbp)
.L2248:
	movslq	-60(%rbp), %rbx
	movq	32(%r15), %rax
	movq	%rbx, %r13
	salq	$3, %rbx
	jmp	.L2250
	.p2align 4,,10
	.p2align 3
.L2292:
	movq	$0, (%r14)
	xorl	%r12d, %r12d
.L2255:
	cmpq	$0, 8(%rsi)
	jne	.L2256
	movq	(%r15), %rax
	movq	%r8, -56(%rbp)
	movq	%r15, %rdi
	call	*64(%rax)
	movq	-56(%rbp), %r8
.L2256:
	cmpl	$5, %r13d
	je	.L2290
	testq	%r12, %r12
	jne	.L2251
	movq	32(%r15), %rax
.L2253:
	addq	$8, %rbx
.L2250:
	movq	(%rax,%rbx), %rsi
	addl	$1, %r13d
	testq	%rsi, %rsi
	je	.L2291
	movq	8(%rsi), %r12
	movslq	11(%r12), %rax
	cmpq	%rax, %r8
	ja	.L2292
	movq	15(%r12), %rax
	movq	%rax, 8(%rsi)
	movslq	11(%r12), %rax
	movq	%rax, (%r14)
	subl	%eax, 4(%rsi)
	testq	%r12, %r12
	je	.L2255
	movq	(%r14), %rax
	subq	%rax, 40(%r15)
	jmp	.L2255
	.p2align 4,,10
	.p2align 3
.L2291:
	cmpl	$5, %r13d
	jne	.L2253
	jmp	.L2249
	.p2align 4,,10
	.p2align 3
.L2288:
	movq	(%r14), %rax
	subq	%rax, 40(%r15)
	cmpq	$0, 8(%r13)
	jne	.L2251
	movq	(%r15), %rax
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	*64(%rax)
	.p2align 4,,10
	.p2align 3
.L2251:
	movq	%r12, %rax
	movq	(%r14), %rdx
	andq	$-262144, %rax
	addq	%rdx, 192(%rax)
.L2262:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2268:
	.cfi_restore_state
	movl	$2, -60(%rbp)
	jmp	.L2248
	.p2align 4,,10
	.p2align 3
.L2290:
	testq	%r12, %r12
	jne	.L2251
	jmp	.L2249
.L2263:
	movq	%r8, %rsi
	movq	%r8, -56(%rbp)
	movq	%r15, %rdi
	call	*%rax
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	movl	%eax, %esi
	jne	.L2265
.L2266:
	movq	%r8, %rdx
	movq	%r14, %rcx
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal8FreeList13TryFindNodeInEimPm
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.L2251
	xorl	%esi, %esi
	jmp	.L2265
	.p2align 4,,10
	.p2align 3
.L2287:
	movl	$4, -60(%rbp)
	jmp	.L2248
	.cfi_endproc
.LFE23047:
	.size	_ZN2v88internal14FreeListLegacy8AllocateEmPmNS0_16AllocationOriginE, .-_ZN2v88internal14FreeListLegacy8AllocateEmPmNS0_16AllocationOriginE
	.section	.text._ZN2v88internal26FreeListManyCachedFastPath8AllocateEmPmNS0_16AllocationOriginE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26FreeListManyCachedFastPath8AllocateEmPmNS0_16AllocationOriginE
	.type	_ZN2v88internal26FreeListManyCachedFastPath8AllocateEmPmNS0_16AllocationOriginE, @function
_ZN2v88internal26FreeListManyCachedFastPath8AllocateEmPmNS0_16AllocationOriginE:
.LFB23074:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal12FreeListMany14categories_minE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movslq	12(%rdi), %rdx
	movl	(%rax,%rdx,4), %eax
	movq	%rdx, %rcx
	cmpq	%rax, %rsi
	jnb	.L2327
	leaq	1920(%rsi), %rax
	cmpl	$18, %edx
	jle	.L2327
	cmpq	$2048, %rax
	jbe	.L2328
	cmpl	$19, %edx
	je	.L2329
	cmpq	$4096, %rax
	jbe	.L2330
	cmpl	$20, %edx
	je	.L2331
	cmpq	$8192, %rax
	jbe	.L2332
	cmpl	$21, %edx
	je	.L2333
	cmpq	$16384, %rax
	jbe	.L2334
	cmpl	$22, %edx
	je	.L2335
	cmpq	$32768, %rax
	jbe	.L2336
	cmpl	$23, %edx
	je	.L2337
	movl	%edx, -56(%rbp)
	cmpq	$65536, %rax
	jbe	.L2382
.L2296:
	movslq	-56(%rbp), %rax
	movl	48(%r13,%rax,4), %ebx
	cmpl	%ecx, %ebx
	jle	.L2303
	jmp	.L2304
	.p2align 4,,10
	.p2align 3
.L2384:
	movq	$0, (%r15)
.L2300:
	cmpq	$0, 8(%rsi)
	je	.L2383
.L2324:
	movl	12(%r13), %ecx
.L2298:
	addl	$1, %ebx
	movslq	%ebx, %rbx
	movl	48(%r13,%rbx,4), %ebx
	cmpl	%ecx, %ebx
	jg	.L2297
.L2303:
	movq	32(%r13), %rdx
	movslq	%ebx, %rax
	leaq	0(,%rax,8), %r12
	movq	(%rdx,%rax,8), %rsi
	testq	%rsi, %rsi
	je	.L2298
	movq	8(%rsi), %rax
	movslq	11(%rax), %rdx
	cmpq	%rdx, %r14
	ja	.L2384
	movq	15(%rax), %rdx
	movq	%rdx, 8(%rsi)
	movslq	11(%rax), %rdx
	movq	%rdx, (%r15)
	subl	%edx, 4(%rsi)
	testq	%rax, %rax
	je	.L2300
	movq	(%r15), %rdx
	subq	%rdx, 40(%r13)
	cmpq	$0, 8(%rsi)
	jne	.L2306
	movq	0(%r13), %rdx
	movq	%rax, -56(%rbp)
	movq	%r13, %rdi
	call	*64(%rdx)
	movq	-56(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L2306:
	movq	32(%r13), %rdx
	cmpq	$0, (%rdx,%r12)
	je	.L2385
.L2318:
	movq	%rax, %rdx
	movq	(%r15), %rcx
	andq	$-262144, %rdx
	addq	%rcx, 192(%rdx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2383:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*64(%rax)
	jmp	.L2324
	.p2align 4,,10
	.p2align 3
.L2327:
	movl	%ecx, -56(%rbp)
.L2294:
	movslq	-56(%rbp), %rax
	movl	48(%r13,%rax,4), %ebx
	cmpl	%ecx, %ebx
	jle	.L2303
	.p2align 4,,10
	.p2align 3
.L2297:
	cmpq	$128, %r14
	ja	.L2380
	movl	108(%r13), %ebx
	cmpl	$17, %ebx
	jle	.L2307
	jmp	.L2380
	.p2align 4,,10
	.p2align 3
.L2305:
	addl	$1, %ebx
	movslq	%ebx, %rbx
	movl	48(%r13,%rbx,4), %ebx
	cmpl	$17, %ebx
	jg	.L2386
.L2307:
	movq	%r15, %rcx
	movq	%r14, %rdx
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8FreeList13TryFindNodeInEimPm
	testq	%rax, %rax
	je	.L2305
.L2381:
	movq	32(%r13), %rdx
	movslq	%ebx, %r8
	leaq	0(,%r8,8), %r12
	cmpq	$0, (%rdx,%r12)
	jne	.L2318
.L2385:
	testl	%ebx, %ebx
	js	.L2318
	leal	1(%rbx), %esi
	movslq	%ebx, %rdx
	leaq	-4(%r13), %rdi
	movslq	%esi, %rsi
	leaq	0(%r13,%rdx,4), %rdx
	addq	$12, %rsi
	jmp	.L2319
	.p2align 4,,10
	.p2align 3
.L2387:
	movl	0(%r13,%rsi,4), %ecx
	subq	$4, %rdx
	movl	%ecx, 52(%rdx)
	cmpq	%rdx, %rdi
	je	.L2318
.L2319:
	cmpl	%ebx, 48(%rdx)
	je	.L2387
	jmp	.L2318
	.p2align 4,,10
	.p2align 3
.L2386:
	movslq	12(%r13), %rdx
	movq	%rdx, %rcx
.L2304:
	movq	32(%r13), %rax
	leaq	0(,%rdx,8), %r12
	movq	(%rax,%rdx,8), %rbx
	jmp	.L2309
	.p2align 4,,10
	.p2align 3
.L2389:
	movq	24(%rbx), %rax
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal16FreeListCategory19SearchForNodeInListEmPm
	movl	-64(%rbp), %ecx
	testq	%rax, %rax
	jne	.L2388
	movq	-72(%rbp), %rbx
.L2309:
	movl	%ecx, -64(%rbp)
	testq	%rbx, %rbx
	jne	.L2389
	movq	0(%r13), %rax
	leaq	_ZN2v88internal12FreeListMany26SelectFreeListCategoryTypeEm(%rip), %rdx
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2311
	cmpq	$256, %r14
	ja	.L2312
	movq	%r14, %rax
	movl	$0, %edx
	shrq	$4, %rax
	subl	$1, %eax
	cmpq	$31, %r14
	cmovbe	%edx, %eax
.L2313:
	cltq
	movl	48(%r13,%rax,4), %ebx
	cmpl	%ebx, -56(%rbp)
	jg	.L2317
	jmp	.L2315
	.p2align 4,,10
	.p2align 3
.L2316:
	addl	$1, %ebx
	movslq	%ebx, %rbx
	movl	48(%r13,%rbx,4), %ebx
	cmpl	%ebx, -56(%rbp)
	jle	.L2315
.L2317:
	movq	%r15, %rcx
	movq	%r14, %rdx
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8FreeList13TryFindNodeInEimPm
	testq	%rax, %rax
	je	.L2316
	jmp	.L2381
	.p2align 4,,10
	.p2align 3
.L2315:
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2380:
	.cfi_restore_state
	movslq	%ecx, %rdx
	jmp	.L2304
	.p2align 4,,10
	.p2align 3
.L2312:
	movl	12(%r13), %eax
	cmpl	$15, %eax
	jle	.L2313
	cmpq	$511, %r14
	jbe	.L2341
	cmpl	$16, %eax
	je	.L2313
	cmpq	$1023, %r14
	jbe	.L2342
	cmpl	$17, %eax
	je	.L2313
	cmpq	$2047, %r14
	jbe	.L2343
	cmpl	$18, %eax
	je	.L2313
	cmpq	$4095, %r14
	jbe	.L2344
	cmpl	$19, %eax
	je	.L2313
	cmpq	$8191, %r14
	jbe	.L2345
	cmpl	$20, %eax
	je	.L2313
	cmpq	$16383, %r14
	jbe	.L2346
	cmpl	$21, %eax
	je	.L2313
	cmpq	$32767, %r14
	jbe	.L2347
	cmpl	$22, %eax
	je	.L2313
	xorl	%eax, %eax
	cmpq	$65535, %r14
	seta	%al
	addl	$22, %eax
	jmp	.L2313
	.p2align 4,,10
	.p2align 3
.L2388:
	movq	(%r15), %rdx
	subq	%rdx, 40(%r13)
	cmpq	$0, 8(%rbx)
	jne	.L2310
	movq	0(%r13), %rdx
	movq	%rax, -64(%rbp)
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movl	%ecx, -56(%rbp)
	call	*64(%rdx)
	movq	-64(%rbp), %rax
	movl	-56(%rbp), %ecx
.L2310:
	movl	%ecx, %ebx
	jmp	.L2306
	.p2align 4,,10
	.p2align 3
.L2311:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2313
.L2331:
	movl	$20, -56(%rbp)
	jmp	.L2296
.L2328:
	movl	$18, -56(%rbp)
	jmp	.L2294
.L2329:
	movl	$19, -56(%rbp)
	jmp	.L2296
.L2330:
	movl	$19, -56(%rbp)
	jmp	.L2294
.L2332:
	movl	$20, -56(%rbp)
	jmp	.L2294
.L2333:
	movl	$21, -56(%rbp)
	jmp	.L2296
.L2334:
	movl	$21, -56(%rbp)
	jmp	.L2294
.L2335:
	movl	$22, -56(%rbp)
	jmp	.L2296
.L2336:
	movl	$22, -56(%rbp)
	jmp	.L2294
.L2337:
	movl	$23, -56(%rbp)
	jmp	.L2296
.L2382:
	movl	$23, -56(%rbp)
	jmp	.L2294
.L2341:
	movl	$15, %eax
	jmp	.L2313
.L2342:
	movl	$16, %eax
	jmp	.L2313
.L2343:
	movl	$17, %eax
	jmp	.L2313
.L2344:
	movl	$18, %eax
	jmp	.L2313
.L2345:
	movl	$19, %eax
	jmp	.L2313
.L2347:
	movl	$21, %eax
	jmp	.L2313
.L2346:
	movl	$20, %eax
	jmp	.L2313
	.cfi_endproc
.LFE23074:
	.size	_ZN2v88internal26FreeListManyCachedFastPath8AllocateEmPmNS0_16AllocationOriginE, .-_ZN2v88internal26FreeListManyCachedFastPath8AllocateEmPmNS0_16AllocationOriginE
	.section	.text._ZN2v88internal24FreeListManyCachedOrigin8AllocateEmPmNS0_16AllocationOriginE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24FreeListManyCachedOrigin8AllocateEmPmNS0_16AllocationOriginE
	.type	_ZN2v88internal24FreeListManyCachedOrigin8AllocateEmPmNS0_16AllocationOriginE, @function
_ZN2v88internal24FreeListManyCachedOrigin8AllocateEmPmNS0_16AllocationOriginE:
.LFB23075:
	.cfi_startproc
	endbr64
	cmpl	$2, %ecx
	je	.L2392
	jmp	_ZN2v88internal26FreeListManyCachedFastPath8AllocateEmPmNS0_16AllocationOriginE
	.p2align 4,,10
	.p2align 3
.L2392:
	jmp	_ZN2v88internal18FreeListManyCached8AllocateEmPmNS0_16AllocationOriginE
	.cfi_endproc
.LFE23075:
	.size	_ZN2v88internal24FreeListManyCachedOrigin8AllocateEmPmNS0_16AllocationOriginE, .-_ZN2v88internal24FreeListManyCachedOrigin8AllocateEmPmNS0_16AllocationOriginE
	.section	.text._ZN2v88internal8FreeList19SearchForNodeInListEimPm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8FreeList19SearchForNodeInListEimPm
	.type	_ZN2v88internal8FreeList19SearchForNodeInListEimPm, @function
_ZN2v88internal8FreeList19SearchForNodeInListEimPm:
.LFB23038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	32(%rdi), %rax
	movq	(%rax,%rsi,8), %r15
	jmp	.L2395
	.p2align 4,,10
	.p2align 3
.L2401:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	24(%r15), %rbx
	call	_ZN2v88internal16FreeListCategory19SearchForNodeInListEmPm
	testq	%rax, %rax
	jne	.L2400
	movq	%rbx, %r15
.L2395:
	testq	%r15, %r15
	jne	.L2401
	xorl	%eax, %eax
.L2397:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2400:
	.cfi_restore_state
	movq	(%r12), %rdx
	subq	%rdx, 40(%r14)
	cmpq	$0, 8(%r15)
	jne	.L2397
	movq	(%r14), %rdx
	movq	%rax, -56(%rbp)
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	*64(%rdx)
	movq	-56(%rbp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23038:
	.size	_ZN2v88internal8FreeList19SearchForNodeInListEimPm, .-_ZN2v88internal8FreeList19SearchForNodeInListEimPm
	.section	.text._ZN2v88internal14FreeListLegacyC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14FreeListLegacyC2Ev
	.type	_ZN2v88internal14FreeListLegacyC2Ev, @function
_ZN2v88internal14FreeListLegacyC2Ev:
.LFB23041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14FreeListLegacyE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movabsq	$21474836486, %rax
	movdqa	.LC23(%rip), %xmm0
	movq	%rax, 8(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movups	%xmm0, 16(%rdi)
	movl	$48, %edi
	call	_Znam@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rax, 32(%r12)
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8FreeList5ResetEv
	.cfi_endproc
.LFE23041:
	.size	_ZN2v88internal14FreeListLegacyC2Ev, .-_ZN2v88internal14FreeListLegacyC2Ev
	.globl	_ZN2v88internal14FreeListLegacyC1Ev
	.set	_ZN2v88internal14FreeListLegacyC1Ev,_ZN2v88internal14FreeListLegacyC2Ev
	.section	.text._ZN2v88internal17FreeListFastAllocC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17FreeListFastAllocC2Ev
	.type	_ZN2v88internal17FreeListFastAllocC2Ev, @function
_ZN2v88internal17FreeListFastAllocC2Ev:
.LFB23049:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal17FreeListFastAllocE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movabsq	$8589934595, %rax
	movdqa	.LC24(%rip), %xmm0
	movq	%rax, 8(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movups	%xmm0, 16(%rdi)
	movl	$24, %edi
	call	_Znam@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	$0, 16(%rax)
	movq	%rax, 32(%r12)
	movups	%xmm0, (%rax)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8FreeList5ResetEv
	.cfi_endproc
.LFE23049:
	.size	_ZN2v88internal17FreeListFastAllocC2Ev, .-_ZN2v88internal17FreeListFastAllocC2Ev
	.globl	_ZN2v88internal17FreeListFastAllocC1Ev
	.set	_ZN2v88internal17FreeListFastAllocC1Ev,_ZN2v88internal17FreeListFastAllocC2Ev
	.section	.text._ZN2v88internal12FreeListManyC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12FreeListManyC2Ev
	.type	_ZN2v88internal12FreeListManyC2Ev, @function
_ZN2v88internal12FreeListManyC2Ev:
.LFB23057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal12FreeListManyE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movabsq	$98784247832, %rax
	movdqa	.LC23(%rip), %xmm0
	movq	%rax, 8(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movups	%xmm0, 16(%rdi)
	movl	$192, %edi
	call	_Znam@PLT
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	movq	$0, (%rax)
	movq	%rax, %rdx
	movq	$0, 184(%rax)
	andq	$-8, %rdi
	xorl	%eax, %eax
	subq	%rdi, %rcx
	addl	$192, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	%rdx, 32(%r12)
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8FreeList5ResetEv
	.cfi_endproc
.LFE23057:
	.size	_ZN2v88internal12FreeListManyC2Ev, .-_ZN2v88internal12FreeListManyC2Ev
	.globl	_ZN2v88internal12FreeListManyC1Ev
	.set	_ZN2v88internal12FreeListManyC1Ev,_ZN2v88internal12FreeListManyC2Ev
	.section	.text._ZN2v88internal18FreeListManyCachedC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18FreeListManyCachedC2Ev
	.type	_ZN2v88internal18FreeListManyCachedC2Ev, @function
_ZN2v88internal18FreeListManyCachedC2Ev:
.LFB23067:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal12FreeListManyE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movabsq	$98784247832, %rax
	movdqa	.LC23(%rip), %xmm0
	movq	%rax, 8(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movups	%xmm0, 16(%rdi)
	movl	$192, %edi
	call	_Znam@PLT
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	movq	$0, (%rax)
	movq	%rax, %rdx
	movq	$0, 184(%rax)
	andq	$-8, %rdi
	xorl	%eax, %eax
	subq	%rdi, %rcx
	addl	$192, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	%rdx, 32(%rbx)
	movq	%rbx, %rdi
	call	_ZN2v88internal8FreeList5ResetEv
	movdqa	.LC6(%rip), %xmm0
	leaq	16+_ZTVN2v88internal18FreeListManyCachedE(%rip), %rax
	movl	$24, 144(%rbx)
	movq	%rax, (%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 96(%rbx)
	movups	%xmm0, 112(%rbx)
	movups	%xmm0, 128(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23067:
	.size	_ZN2v88internal18FreeListManyCachedC2Ev, .-_ZN2v88internal18FreeListManyCachedC2Ev
	.globl	_ZN2v88internal18FreeListManyCachedC1Ev
	.set	_ZN2v88internal18FreeListManyCachedC1Ev,_ZN2v88internal18FreeListManyCachedC2Ev
	.section	.text._ZN2v88internal11FreeListMapC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11FreeListMapC2Ev
	.type	_ZN2v88internal11FreeListMapC2Ev, @function
_ZN2v88internal11FreeListMapC2Ev:
.LFB23077:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal11FreeListMapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movdqa	.LC26(%rip), %xmm0
	movq	$1, 8(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movups	%xmm0, 16(%rdi)
	movl	$8, %edi
	call	_Znam@PLT
	movq	%r12, %rdi
	movq	$0, (%rax)
	movq	%rax, 32(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8FreeList5ResetEv
	.cfi_endproc
.LFE23077:
	.size	_ZN2v88internal11FreeListMapC2Ev, .-_ZN2v88internal11FreeListMapC2Ev
	.globl	_ZN2v88internal11FreeListMapC1Ev
	.set	_ZN2v88internal11FreeListMapC1Ev,_ZN2v88internal11FreeListMapC2Ev
	.section	.text._ZN2v88internal8FreeList18EvictFreeListItemsEPNS0_4PageE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8FreeList18EvictFreeListItemsEPNS0_4PageE
	.type	_ZN2v88internal8FreeList18EvictFreeListItemsEPNS0_4PageE, @function
_ZN2v88internal8FreeList18EvictFreeListItemsEPNS0_4PageE:
.LFB23088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	jmp	.L2416
	.p2align 4,,10
	.p2align 3
.L2414:
	cmpq	$0, 8(%rbx)
	je	.L2415
	movl	4(%rbx), %edx
	subq	%rdx, 40(%r12)
.L2415:
	movq	$0, 8(%rbx)
	addq	$1, %r13
	movl	$0, 4(%rbx)
	movups	%xmm0, 16(%rbx)
.L2416:
	movq	(%rax), %rdx
	movq	%rax, -56(%rbp)
	movq	96(%rdx), %rdx
	cmpl	%r13d, 8(%rdx)
	jle	.L2412
	movq	240(%r14), %rdx
	movq	%r12, %rdi
	movq	(%rdx,%r13,8), %rbx
	movl	4(%rbx), %edx
	movq	%rbx, %rsi
	addq	%rdx, %r15
	movq	(%r12), %rdx
	call	*64(%rdx)
	cmpq	$0, 16(%rbx)
	movq	-56(%rbp), %rax
	pxor	%xmm0, %xmm0
	jne	.L2414
	cmpq	$0, 24(%rbx)
	jne	.L2414
	movslq	(%rbx), %rcx
	movq	32(%r12), %rdx
	cmpq	(%rdx,%rcx,8), %rbx
	jne	.L2415
	jmp	.L2414
	.p2align 4,,10
	.p2align 3
.L2412:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23088:
	.size	_ZN2v88internal8FreeList18EvictFreeListItemsEPNS0_4PageE, .-_ZN2v88internal8FreeList18EvictFreeListItemsEPNS0_4PageE
	.section	.text._ZN2v88internal8FreeList11RepairListsEPNS0_4HeapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8FreeList11RepairListsEPNS0_4HeapE
	.type	_ZN2v88internal8FreeList11RepairListsEPNS0_4HeapE, @function
_ZN2v88internal8FreeList11RepairListsEPNS0_4HeapE:
.LFB23090:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	xorl	%ecx, %ecx
	testl	%eax, %eax
	jle	.L2418
	.p2align 4,,10
	.p2align 3
.L2419:
	movq	32(%rdi), %rdx
	movq	(%rdx,%rcx,8), %rdx
	testq	%rdx, %rdx
	je	.L2420
	leaq	-37592(%rsi), %r8
	.p2align 4,,10
	.p2align 3
.L2422:
	movq	%rdx, %rax
	movq	56(%r8), %r9
	movq	24(%rdx), %rdx
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L2425
	.p2align 4,,10
	.p2align 3
.L2421:
	movq	-1(%rax), %r10
	testq	%r10, %r10
	jne	.L2423
	movq	%r9, -1(%rax)
	movq	15(%rax), %rax
	testq	%rax, %rax
	jne	.L2421
.L2425:
	testq	%rdx, %rdx
	jne	.L2422
.L2438:
	movl	8(%rdi), %eax
.L2420:
	addq	$1, %rcx
	cmpl	%ecx, %eax
	jg	.L2419
.L2418:
	ret
	.p2align 4,,10
	.p2align 3
.L2423:
	movq	15(%rax), %rax
	testq	%rax, %rax
	jne	.L2421
	testq	%rdx, %rdx
	jne	.L2422
	jmp	.L2438
	.cfi_endproc
.LFE23090:
	.size	_ZN2v88internal8FreeList11RepairListsEPNS0_4HeapE, .-_ZN2v88internal8FreeList11RepairListsEPNS0_4HeapE
	.section	.rodata._ZN2v88internal8FreeList15PrintCategoriesEi.str1.1,"aMS",@progbits,1
.LC27:
	.string	"FreeList[%p, top=%p, %d] "
.LC28:
	.string	"%p -> "
.LC29:
	.string	"null\n"
	.section	.text._ZN2v88internal8FreeList15PrintCategoriesEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8FreeList15PrintCategoriesEi
	.type	_ZN2v88internal8FreeList15PrintCategoriesEi, @function
_ZN2v88internal8FreeList15PrintCategoriesEi:
.LFB23094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rdx
	movq	%rdi, %rsi
	movq	%rdx, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	32(%rdi), %rax
	leaq	.LC27(%rip), %rdi
	movq	(%rax,%rdx,8), %rbx
	xorl	%eax, %eax
	movq	%rbx, %rdx
	call	_ZN2v88internal6PrintFEPKcz@PLT
	testq	%rbx, %rbx
	je	.L2440
	leaq	.LC28(%rip), %r12
	.p2align 4,,10
	.p2align 3
.L2441:
	movq	%rbx, %rsi
	movq	24(%rbx), %rbx
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	testq	%rbx, %rbx
	jne	.L2441
.L2440:
	popq	%rbx
	leaq	.LC29(%rip), %rdi
	popq	%r12
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6PrintFEPKcz@PLT
	.cfi_endproc
.LFE23094:
	.size	_ZN2v88internal8FreeList15PrintCategoriesEi, .-_ZN2v88internal8FreeList15PrintCategoriesEi
	.section	.text._ZN2v88internal11MemoryChunk15FreeListsLengthEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11MemoryChunk15FreeListsLengthEv
	.type	_ZN2v88internal11MemoryChunk15FreeListsLengthEv, @function
_ZN2v88internal11MemoryChunk15FreeListsLengthEv:
.LFB23095:
	.cfi_startproc
	endbr64
	leaq	80(%rdi), %rsi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	jmp	.L2452
	.p2align 4,,10
	.p2align 3
.L2449:
	addq	$1, %rcx
.L2452:
	movq	(%rsi), %rax
	movq	96(%rax), %rax
	cmpl	%ecx, 12(%rax)
	jl	.L2447
	movq	240(%rdi), %rax
	movq	(%rax,%rcx,8), %rax
	testq	%rax, %rax
	je	.L2449
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L2449
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2451:
	movq	15(%rax), %rax
	addl	$1, %edx
	testq	%rax, %rax
	jne	.L2451
	addl	%edx, %r8d
	jmp	.L2449
	.p2align 4,,10
	.p2align 3
.L2447:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE23095:
	.size	_ZN2v88internal11MemoryChunk15FreeListsLengthEv, .-_ZN2v88internal11MemoryChunk15FreeListsLengthEv
	.section	.text._ZN2v88internal16FreeListCategory11SumFreeListEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16FreeListCategory11SumFreeListEv
	.type	_ZN2v88internal16FreeListCategory11SumFreeListEv, @function
_ZN2v88internal16FreeListCategory11SumFreeListEv:
.LFB23096:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L2460
	.p2align 4,,10
	.p2align 3
.L2462:
	movq	7(%rax), %rdx
	movq	15(%rax), %rax
	sarq	$32, %rdx
	addq	%rdx, %r8
	testq	%rax, %rax
	jne	.L2462
.L2460:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE23096:
	.size	_ZN2v88internal16FreeListCategory11SumFreeListEv, .-_ZN2v88internal16FreeListCategory11SumFreeListEv
	.section	.text._ZN2v88internal16FreeListCategory14FreeListLengthEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16FreeListCategory14FreeListLengthEv
	.type	_ZN2v88internal16FreeListCategory14FreeListLengthEv, @function
_ZN2v88internal16FreeListCategory14FreeListLengthEv:
.LFB23097:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L2465
	.p2align 4,,10
	.p2align 3
.L2467:
	movq	15(%rax), %rax
	addl	$1, %r8d
	testq	%rax, %rax
	jne	.L2467
.L2465:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE23097:
	.size	_ZN2v88internal16FreeListCategory14FreeListLengthEv, .-_ZN2v88internal16FreeListCategory14FreeListLengthEv
	.section	.text._ZN2v88internal13ReadOnlySpaceC2EPNS0_4HeapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ReadOnlySpaceC2EPNS0_4HeapE
	.type	_ZN2v88internal13ReadOnlySpaceC2EPNS0_4HeapE, @function
_ZN2v88internal13ReadOnlySpaceC2EPNS0_4HeapE:
.LFB23119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal8FreeList14CreateFreeListEv
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %rdx
	movq	%r12, 64(%rbx)
	pxor	%xmm0, %xmm0
	movq	%rdx, (%rbx)
	movl	$16, %edi
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movb	$0, 56(%rbx)
	movl	$0, 72(%rbx)
	movq	%rax, 96(%rbx)
	movups	%xmm0, 80(%rbx)
	call	_Znam@PLT
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	leaq	192(%rbx), %rdi
	movq	%rax, 48(%rbx)
	movq	%r13, (%rax)
	mfence
	movq	48(%rbx), %rax
	movq	%r13, 8(%rax)
	leaq	16+_ZTVN2v88internal10PagedSpaceE(%rip), %rax
	mfence
	movq	%rax, (%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 144(%rbx)
	movl	$0, 152(%rbx)
	movups	%xmm0, 104(%rbx)
	movups	%xmm1, 128(%rbx)
	movq	%r13, 168(%rbx)
	mfence
	movups	%xmm0, 176(%rbx)
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	16+_ZTVN2v88internal13ReadOnlySpaceE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	$261864, 160(%rbx)
	movq	%r13, 168(%rbx)
	mfence
	movq	%rax, (%rbx)
	movups	%xmm0, 176(%rbx)
	movzbl	3866(%r12), %eax
	movb	$0, 232(%rbx)
	movb	%al, 233(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23119:
	.size	_ZN2v88internal13ReadOnlySpaceC2EPNS0_4HeapE, .-_ZN2v88internal13ReadOnlySpaceC2EPNS0_4HeapE
	.globl	_ZN2v88internal13ReadOnlySpaceC1EPNS0_4HeapE
	.set	_ZN2v88internal13ReadOnlySpaceC1EPNS0_4HeapE,_ZN2v88internal13ReadOnlySpaceC2EPNS0_4HeapE
	.section	.rodata._ZN2v88internal13ReadOnlySpace22SetPermissionsForPagesEPNS0_15MemoryAllocatorENS_13PageAllocator10PermissionE.str1.8,"aMS",@progbits,1
	.align 8
.LC30:
	.string	"SetPermissions(page_allocator, p->address(), p->size(), access)"
	.section	.text._ZN2v88internal13ReadOnlySpace22SetPermissionsForPagesEPNS0_15MemoryAllocatorENS_13PageAllocator10PermissionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ReadOnlySpace22SetPermissionsForPagesEPNS0_15MemoryAllocatorENS_13PageAllocator10PermissionE
	.type	_ZN2v88internal13ReadOnlySpace22SetPermissionsForPagesEPNS0_15MemoryAllocatorENS_13PageAllocator10PermissionE, @function
_ZN2v88internal13ReadOnlySpace22SetPermissionsForPagesEPNS0_15MemoryAllocatorENS_13PageAllocator10PermissionE:
.LFB23122:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	32(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L2472
	movq	%rsi, %r12
	movl	%edx, %r13d
	jmp	.L2476
	.p2align 4,,10
	.p2align 3
.L2475:
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L2472
.L2476:
	movq	32(%r12), %rdi
	testb	$1, 8(%rbx)
	je	.L2474
	movq	40(%r12), %rdi
.L2474:
	movq	(%rbx), %rdx
	movl	%r13d, %ecx
	movq	%rbx, %rsi
	call	_ZN2v88internal14SetPermissionsEPNS_13PageAllocatorEPvmNS1_10PermissionE@PLT
	testb	%al, %al
	jne	.L2475
	leaq	.LC30(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2472:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23122:
	.size	_ZN2v88internal13ReadOnlySpace22SetPermissionsForPagesEPNS0_15MemoryAllocatorENS_13PageAllocator10PermissionE, .-_ZN2v88internal13ReadOnlySpace22SetPermissionsForPagesEPNS0_15MemoryAllocatorENS_13PageAllocator10PermissionE
	.section	.rodata._ZN2v88internal13ReadOnlySpace35RepairFreeListsAfterDeserializationEv.str1.8,"aMS",@progbits,1
	.align 8
.LC31:
	.string	"size == static_cast<int>(end - start)"
	.section	.text._ZN2v88internal13ReadOnlySpace35RepairFreeListsAfterDeserializationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ReadOnlySpace35RepairFreeListsAfterDeserializationEv
	.type	_ZN2v88internal13ReadOnlySpace35RepairFreeListsAfterDeserializationEv, @function
_ZN2v88internal13ReadOnlySpace35RepairFreeListsAfterDeserializationEv:
.LFB23123:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	96(%rdi), %r9
	movq	64(%rdi), %r10
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	8(%r9), %eax
	testl	%eax, %eax
	jle	.L2495
	.p2align 4,,10
	.p2align 3
.L2486:
	movq	32(%r9), %rdx
	movq	(%rdx,%rcx,8), %rdx
	testq	%rdx, %rdx
	je	.L2489
	leaq	-37592(%r10), %r8
	.p2align 4,,10
	.p2align 3
.L2491:
	movq	%rdx, %rax
	movq	56(%r8), %rsi
	movq	24(%rdx), %rdx
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L2494
	.p2align 4,,10
	.p2align 3
.L2490:
	movq	-1(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L2492
	movq	%rsi, -1(%rax)
	movq	15(%rax), %rax
	testq	%rax, %rax
	jne	.L2490
.L2494:
	testq	%rdx, %rdx
	jne	.L2491
.L2518:
	movl	8(%r9), %eax
.L2489:
	addq	$1, %rcx
	cmpl	%ecx, %eax
	jg	.L2486
.L2495:
	movq	32(%r14), %rbx
	leaq	-64(%rbp), %r15
	testq	%rbx, %rbx
	jne	.L2487
	jmp	.L2485
	.p2align 4,,10
	.p2align 3
.L2497:
	subl	%r12d, %edx
	cmpl	%r13d, %edx
	jne	.L2516
.L2499:
	movq	64(%r14), %rdi
	movl	$1, %r8d
	movl	$1, %ecx
	movq	%r12, %rsi
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
.L2496:
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L2485
.L2487:
	movq	216(%rbx), %r13
	testl	%r13d, %r13d
	je	.L2496
	movslq	%r13d, %rax
	movq	152(%rbx), %r12
	addq	%rbx, %r12
	movq	48(%rbx), %rdx
	movq	%rdx, %rsi
	subq	%rax, %rsi
	cmpq	%r12, %rsi
	jbe	.L2497
	leaq	1(%r12), %rax
	movq	%rax, -64(%rbp)
	movq	(%r12), %rax
	movzwl	11(%rax), %eax
	cmpw	$76, %ax
	je	.L2498
	cmpw	$73, %ax
	jne	.L2517
.L2498:
	movq	-64(%rbp), %rax
	movq	%rdx, -72(%rbp)
	movq	%r15, %rdi
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-72(%rbp), %rdx
	cltq
	addq	%rax, %r12
	subl	%r12d, %edx
	cmpl	%r13d, %edx
	je	.L2499
.L2516:
	leaq	.LC31(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2492:
	movq	15(%rax), %rax
	testq	%rax, %rax
	jne	.L2490
	testq	%rdx, %rdx
	jne	.L2491
	jmp	.L2518
	.p2align 4,,10
	.p2align 3
.L2485:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2519
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2517:
	.cfi_restore_state
	leaq	.LC17(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2519:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23123:
	.size	_ZN2v88internal13ReadOnlySpace35RepairFreeListsAfterDeserializationEv, .-_ZN2v88internal13ReadOnlySpace35RepairFreeListsAfterDeserializationEv
	.section	.text._ZN2v88internal13ReadOnlySpace26ClearStringPaddingIfNeededEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ReadOnlySpace26ClearStringPaddingIfNeededEv
	.type	_ZN2v88internal13ReadOnlySpace26ClearStringPaddingIfNeededEv, @function
_ZN2v88internal13ReadOnlySpace26ClearStringPaddingIfNeededEv:
.LFB23124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 233(%rdi)
	je	.L2537
.L2520:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2538
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2537:
	.cfi_restore_state
	leaq	-64(%rbp), %rbx
	movq	%rdi, %rsi
	movq	%rdi, %r12
	movq	%rbx, %rdi
	call	_ZN2v88internal26ReadOnlyHeapObjectIteratorC1EPNS0_13ReadOnlySpaceE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal26ReadOnlyHeapObjectIterator4NextEv@PLT
	testq	%rax, %rax
	je	.L2523
	leaq	-72(%rbp), %r13
	jmp	.L2530
	.p2align 4,,10
	.p2align 3
.L2524:
	movq	(%rdx), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L2527
	movq	(%rdx), %rcx
	testb	$7, 11(%rcx)
	je	.L2539
.L2527:
	movq	%rbx, %rdi
	call	_ZN2v88internal26ReadOnlyHeapObjectIterator4NextEv@PLT
	testq	%rax, %rax
	je	.L2523
.L2530:
	movq	-1(%rax), %rcx
	leaq	-1(%rax), %rdx
	cmpw	$63, 11(%rcx)
	ja	.L2524
	movq	-1(%rax), %rcx
	testb	$7, 11(%rcx)
	jne	.L2524
	movq	(%rdx), %rcx
	testb	$8, 11(%rcx)
	je	.L2524
	movq	%r13, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal16SeqOneByteString13clear_paddingEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal26ReadOnlyHeapObjectIterator4NextEv@PLT
	testq	%rax, %rax
	jne	.L2530
	.p2align 4,,10
	.p2align 3
.L2523:
	movb	$1, 233(%r12)
	jmp	.L2520
	.p2align 4,,10
	.p2align 3
.L2539:
	movq	(%rdx), %rdx
	testb	$8, 11(%rdx)
	jne	.L2527
	movq	%r13, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal16SeqTwoByteString13clear_paddingEv@PLT
	jmp	.L2527
.L2538:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23124:
	.size	_ZN2v88internal13ReadOnlySpace26ClearStringPaddingIfNeededEv, .-_ZN2v88internal13ReadOnlySpace26ClearStringPaddingIfNeededEv
	.section	.text._ZN2v88internal13ReadOnlySpace6UnsealEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ReadOnlySpace6UnsealEv
	.type	_ZN2v88internal13ReadOnlySpace6UnsealEv, @function
_ZN2v88internal13ReadOnlySpace6UnsealEv:
.LFB23126:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	64(%rdi), %rax
	movq	32(%rdi), %rbx
	movq	2048(%rax), %r13
	testq	%rbx, %rbx
	jne	.L2544
	jmp	.L2541
	.p2align 4,,10
	.p2align 3
.L2543:
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L2541
.L2544:
	movq	32(%r13), %rdi
	testb	$1, 8(%rbx)
	je	.L2542
	movq	40(%r13), %rdi
.L2542:
	movq	(%rbx), %rdx
	movl	$2, %ecx
	movq	%rbx, %rsi
	call	_ZN2v88internal14SetPermissionsEPNS_13PageAllocatorEPvmNS1_10PermissionE@PLT
	testb	%al, %al
	jne	.L2543
	leaq	.LC30(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2541:
	movb	$0, 232(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23126:
	.size	_ZN2v88internal13ReadOnlySpace6UnsealEv, .-_ZN2v88internal13ReadOnlySpace6UnsealEv
	.section	.text._ZN2v88internal9LargePage18GetAddressToShrinkEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9LargePage18GetAddressToShrinkEmm
	.type	_ZN2v88internal9LargePage18GetAddressToShrinkEmm, @function
_ZN2v88internal9LargePage18GetAddressToShrinkEmm:
.LFB23127:
	.cfi_startproc
	endbr64
	testb	$1, 8(%rdi)
	jne	.L2566
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %edi
	testl	%edi, %edi
	je	.L2556
	sall	$10, %edi
	movslq	%edi, %rdi
	movq	%rdi, %rax
.L2557:
	leaq	-1(%r14,%r13), %r12
	negq	%rdi
	movq	%r12, %rdx
	subq	%rbx, %rdx
	leaq	(%rdx,%rax), %r12
	andq	%rdi, %r12
	call	_ZN2v84base2OS14HasLazyCommitsEv@PLT
	testb	%al, %al
	je	.L2561
	testb	$32, 10(%rbx)
	je	.L2567
.L2560:
	movq	152(%rbx), %rax
.L2559:
	cmpq	%rax, %r12
	jnb	.L2562
	leaq	(%rbx,%r12), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2556:
	.cfi_restore_state
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movq	%rax, %rdi
	jmp	.L2557
	.p2align 4,,10
	.p2align 3
.L2562:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2567:
	.cfi_restore_state
	movq	80(%rbx), %rax
	cmpl	$5, 72(%rax)
	jne	.L2560
	.p2align 4,,10
	.p2align 3
.L2561:
	movq	(%rbx), %rax
	jmp	.L2559
	.p2align 4,,10
	.p2align 3
.L2566:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE23127:
	.size	_ZN2v88internal9LargePage18GetAddressToShrinkEmm, .-_ZN2v88internal9LargePage18GetAddressToShrinkEmm
	.section	.text._ZN2v88internal30LargeObjectSpaceObjectIteratorC2EPNS0_16LargeObjectSpaceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30LargeObjectSpaceObjectIteratorC2EPNS0_16LargeObjectSpaceE
	.type	_ZN2v88internal30LargeObjectSpaceObjectIteratorC2EPNS0_16LargeObjectSpaceE, @function
_ZN2v88internal30LargeObjectSpaceObjectIteratorC2EPNS0_16LargeObjectSpaceE:
.LFB23130:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal30LargeObjectSpaceObjectIteratorE(%rip), %rax
	movq	%rax, (%rdi)
	movq	32(%rsi), %rax
	movq	%rax, 8(%rdi)
	ret
	.cfi_endproc
.LFE23130:
	.size	_ZN2v88internal30LargeObjectSpaceObjectIteratorC2EPNS0_16LargeObjectSpaceE, .-_ZN2v88internal30LargeObjectSpaceObjectIteratorC2EPNS0_16LargeObjectSpaceE
	.globl	_ZN2v88internal30LargeObjectSpaceObjectIteratorC1EPNS0_16LargeObjectSpaceE
	.set	_ZN2v88internal30LargeObjectSpaceObjectIteratorC1EPNS0_16LargeObjectSpaceE,_ZN2v88internal30LargeObjectSpaceObjectIteratorC2EPNS0_16LargeObjectSpaceE
	.section	.text._ZN2v88internal16LargeObjectSpaceC2EPNS0_4HeapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16LargeObjectSpaceC2EPNS0_4HeapE
	.type	_ZN2v88internal16LargeObjectSpaceC2EPNS0_4HeapE, @function
_ZN2v88internal16LargeObjectSpaceC2EPNS0_4HeapE:
.LFB23134:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$48, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal10NoFreeListE(%rip), %rcx
	movq	%r12, 64(%rbx)
	pxor	%xmm0, %xmm0
	movq	%rcx, (%rax)
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %rcx
	movl	$16, %edi
	movq	%rcx, (%rbx)
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movb	$0, 56(%rbx)
	movl	$5, 72(%rbx)
	movq	%rax, 96(%rbx)
	movq	$0, 8(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 16(%rax)
	call	_Znam@PLT
	xorl	%edx, %edx
	movq	%rax, 48(%rbx)
	movq	%rdx, (%rax)
	mfence
	movq	48(%rbx), %rax
	movq	%rdx, 8(%rax)
	leaq	16+_ZTVN2v88internal16LargeObjectSpaceE(%rip), %rax
	mfence
	movq	%rax, (%rbx)
	movq	$0, 104(%rbx)
	movl	$0, 112(%rbx)
	movq	$0, 120(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23134:
	.size	_ZN2v88internal16LargeObjectSpaceC2EPNS0_4HeapE, .-_ZN2v88internal16LargeObjectSpaceC2EPNS0_4HeapE
	.globl	_ZN2v88internal16LargeObjectSpaceC1EPNS0_4HeapE
	.set	_ZN2v88internal16LargeObjectSpaceC1EPNS0_4HeapE,_ZN2v88internal16LargeObjectSpaceC2EPNS0_4HeapE
	.section	.text._ZN2v88internal16LargeObjectSpaceC2EPNS0_4HeapENS0_15AllocationSpaceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16LargeObjectSpaceC2EPNS0_4HeapENS0_15AllocationSpaceE
	.type	_ZN2v88internal16LargeObjectSpaceC2EPNS0_4HeapENS0_15AllocationSpaceE, @function
_ZN2v88internal16LargeObjectSpaceC2EPNS0_4HeapENS0_15AllocationSpaceE:
.LFB23137:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$48, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal10NoFreeListE(%rip), %rcx
	movq	%r13, 64(%rbx)
	pxor	%xmm0, %xmm0
	movq	%rcx, (%rax)
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %rcx
	movl	$16, %edi
	movl	%r12d, 72(%rbx)
	movq	%rcx, (%rbx)
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movb	$0, 56(%rbx)
	movq	%rax, 96(%rbx)
	movq	$0, 8(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 16(%rax)
	call	_Znam@PLT
	xorl	%edx, %edx
	movq	%rax, 48(%rbx)
	movq	%rdx, (%rax)
	mfence
	movq	48(%rbx), %rax
	movq	%rdx, 8(%rax)
	leaq	16+_ZTVN2v88internal16LargeObjectSpaceE(%rip), %rax
	mfence
	movq	%rax, (%rbx)
	movq	$0, 104(%rbx)
	movl	$0, 112(%rbx)
	movq	$0, 120(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23137:
	.size	_ZN2v88internal16LargeObjectSpaceC2EPNS0_4HeapENS0_15AllocationSpaceE, .-_ZN2v88internal16LargeObjectSpaceC2EPNS0_4HeapENS0_15AllocationSpaceE
	.globl	_ZN2v88internal16LargeObjectSpaceC1EPNS0_4HeapENS0_15AllocationSpaceE
	.set	_ZN2v88internal16LargeObjectSpaceC1EPNS0_4HeapENS0_15AllocationSpaceE,_ZN2v88internal16LargeObjectSpaceC2EPNS0_4HeapENS0_15AllocationSpaceE
	.section	.rodata._ZN2v88internal20CodeLargeObjectSpace8FindPageEm.str1.1,"aMS",@progbits,1
.LC32:
	.string	"page->Contains(a)"
	.section	.text._ZN2v88internal20CodeLargeObjectSpace8FindPageEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20CodeLargeObjectSpace8FindPageEm
	.type	_ZN2v88internal20CodeLargeObjectSpace8FindPageEm, @function
_ZN2v88internal20CodeLargeObjectSpace8FindPageEm:
.LFB23144:
	.cfi_startproc
	endbr64
	movq	%rsi, %r9
	movq	136(%rdi), %rcx
	xorl	%edx, %edx
	andq	$-262144, %r9
	movq	%r9, %rax
	divq	%rcx
	movq	128(%rdi), %rax
	movq	(%rax,%rdx,8), %r8
	testq	%r8, %r8
	je	.L2573
	movq	(%r8), %r8
	movq	%rdx, %r10
	movq	8(%r8), %rdi
	jmp	.L2576
	.p2align 4,,10
	.p2align 3
.L2589:
	movq	(%r8), %r8
	testq	%r8, %r8
	je	.L2573
	movq	8(%r8), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rcx
	cmpq	%rdx, %r10
	jne	.L2588
.L2576:
	cmpq	%rdi, %r9
	jne	.L2589
	movq	16(%r8), %r8
	cmpq	40(%r8), %rsi
	jb	.L2578
	cmpq	48(%r8), %rsi
	jb	.L2573
.L2578:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC32(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2588:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	xorl	%r8d, %r8d
.L2573:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE23144:
	.size	_ZN2v88internal20CodeLargeObjectSpace8FindPageEm, .-_ZN2v88internal20CodeLargeObjectSpace8FindPageEm
	.section	.text._ZN2v88internal16LargeObjectSpace30ClearMarkingStateOfLiveObjectsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16LargeObjectSpace30ClearMarkingStateOfLiveObjectsEv
	.type	_ZN2v88internal16LargeObjectSpace30ClearMarkingStateOfLiveObjectsEv, @function
_ZN2v88internal16LargeObjectSpace30ClearMarkingStateOfLiveObjectsEv:
.LFB23145:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L2630
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	40(%rdx), %rax
	movq	224(%rdx), %r13
	addq	$1, %rax
	je	.L2590
	.p2align 4,,10
	.p2align 3
.L2613:
	movq	%rax, %rbx
	andl	$262143, %eax
	movl	$1, %edx
	movl	%eax, %ecx
	andq	$-262144, %rbx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movq	16(%rbx), %rcx
	leaq	(%rcx,%rax,4), %rax
	movl	(%rax), %esi
	testl	%edx, %esi
	je	.L2593
	movl	%edx, %ecx
	notl	%ecx
	andl	%esi, %ecx
	movl	%ecx, (%rax)
	addl	%edx, %edx
	je	.L2594
	notl	%edx
	movq	%rax, %rsi
.L2595:
	andl	%ecx, %edx
	movl	%edx, (%rsi)
	movq	104(%rbx), %r15
	testq	%r15, %r15
	je	.L2599
	movq	(%rbx), %rax
	leaq	262143(%rax), %rdx
	shrq	$18, %rdx
	je	.L2599
	leaq	(%rdx,%rdx,2), %r14
	salq	$7, %r14
	leaq	(%r14,%r15), %rax
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L2612:
	movq	%r15, %r14
	leaq	256(%r15), %r12
	jmp	.L2605
	.p2align 4,,10
	.p2align 3
.L2601:
	addq	$8, %r14
	cmpq	%r14, %r12
	je	.L2633
.L2605:
	movq	(%r14), %rcx
	testq	%rcx, %rcx
	je	.L2601
	leaq	128(%rcx), %rsi
.L2604:
	movl	(%rcx), %edi
	testl	%edi, %edi
	jne	.L2601
	addq	$4, %rcx
	cmpq	%rcx, %rsi
	jne	.L2604
	movq	(%r14), %rdi
	movq	$0, (%r14)
	testq	%rdi, %rdi
	je	.L2601
	call	_ZdaPv@PLT
	addq	$8, %r14
	cmpq	%r14, %r12
	jne	.L2605
	.p2align 4,,10
	.p2align 3
.L2633:
	leaq	264(%r15), %r12
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	jmp	.L2611
	.p2align 4,,10
	.p2align 3
.L2607:
	movq	-8(%rdi), %r14
	subq	$8, %rdi
	movq	%rdi, 352(%r15)
	testq	%r14, %r14
	je	.L2610
.L2608:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
.L2611:
	movq	352(%r15), %rdi
.L2610:
	cmpq	%rdi, 320(%r15)
	je	.L2606
.L2634:
	cmpq	%rdi, 360(%r15)
	jne	.L2607
	movq	376(%r15), %rdx
	movq	-8(%rdx), %rdx
	movq	504(%rdx), %r14
	call	_ZdlPv@PLT
	movq	376(%r15), %rdx
	leaq	-8(%rdx), %rcx
	movq	%rcx, 376(%r15)
	movq	-8(%rdx), %rdi
	leaq	512(%rdi), %rdx
	movq	%rdi, 360(%r15)
	addq	$504, %rdi
	movq	%rdx, 368(%r15)
	movq	%rdi, 352(%r15)
	testq	%r14, %r14
	jne	.L2608
	cmpq	%rdi, 320(%r15)
	jne	.L2634
	.p2align 4,,10
	.p2align 3
.L2606:
	movq	%r12, %rdi
	addq	$384, %r15
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	cmpq	%r15, -56(%rbp)
	jne	.L2612
.L2599:
	testb	$1, 9(%rbx)
	jne	.L2635
.L2598:
	movq	$0, 96(%rbx)
.L2593:
	testq	%r13, %r13
	je	.L2590
	movq	40(%r13), %rax
	movq	224(%r13), %r13
	addq	$1, %rax
	jne	.L2613
.L2590:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2594:
	.cfi_restore_state
	movl	4(%rax), %ecx
	leaq	4(%rax), %rsi
	movl	$-2, %edx
	jmp	.L2595
	.p2align 4,,10
	.p2align 3
.L2635:
	movq	$0, 88(%rbx)
	jmp	.L2598
	.p2align 4,,10
	.p2align 3
.L2630:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE23145:
	.size	_ZN2v88internal16LargeObjectSpace30ClearMarkingStateOfLiveObjectsEv, .-_ZN2v88internal16LargeObjectSpace30ClearMarkingStateOfLiveObjectsEv
	.section	.text._ZN2v88internal20CodeLargeObjectSpace21InsertChunkMapEntriesEPNS0_9LargePageE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20CodeLargeObjectSpace21InsertChunkMapEntriesEPNS0_9LargePageE
	.type	_ZN2v88internal20CodeLargeObjectSpace21InsertChunkMapEntriesEPNS0_9LargePageE, @function
_ZN2v88internal20CodeLargeObjectSpace21InsertChunkMapEntriesEPNS0_9LargePageE:
.LFB23146:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	addq	%rsi, %rax
	leaq	144(%rdi), %rsi
	movq	%rsi, -56(%rbp)
	cmpq	%rax, %r14
	jnb	.L2636
	movq	%rdi, %r13
	leaq	160(%rdi), %r15
	.p2align 4,,10
	.p2align 3
.L2637:
	movq	136(%r13), %rsi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	128(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rdi
	leaq	0(,%rdx,8), %r10
	testq	%rax, %rax
	je	.L2638
	movq	(%rax), %rbx
	movq	8(%rbx), %rcx
	jmp	.L2640
	.p2align 4,,10
	.p2align 3
.L2680:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L2638
	movq	8(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %rdi
	jne	.L2638
.L2640:
	cmpq	%rcx, %r12
	jne	.L2680
.L2679:
	movq	%r14, 16(%rbx)
	movq	(%r14), %rax
	addq	$262144, %r12
	addq	$16, %rbx
	addq	%r14, %rax
	cmpq	%r12, %rax
	ja	.L2637
.L2636:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2638:
	.cfi_restore_state
	movl	$24, %edi
	movq	%r10, -64(%rbp)
	call	_Znwm@PLT
	movq	152(%r13), %rdx
	movl	$1, %ecx
	movq	%r15, %rdi
	movq	%r12, 8(%rax)
	movq	136(%r13), %rsi
	movq	%rax, %rbx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r8
	testb	%al, %al
	jne	.L2641
	movq	128(%r13), %r9
	movq	-64(%rbp), %r10
	leaq	(%r9,%r10), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2651
.L2683:
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	(%rax), %rax
	movq	%rbx, (%rax)
.L2652:
	addq	$1, 152(%r13)
	jmp	.L2679
	.p2align 4,,10
	.p2align 3
.L2641:
	cmpq	$1, %rdx
	je	.L2681
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L2682
	leaq	0(,%rdx,8), %rdx
	movq	%r8, -72(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-72(%rbp), %r8
	leaq	176(%r13), %r11
	movq	%rax, %r9
.L2644:
	movq	144(%r13), %rsi
	movq	$0, 144(%r13)
	testq	%rsi, %rsi
	je	.L2646
	xorl	%r10d, %r10d
	jmp	.L2647
	.p2align 4,,10
	.p2align 3
.L2648:
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	(%rdi), %rax
	movq	%rcx, (%rax)
.L2649:
	testq	%rsi, %rsi
	je	.L2646
.L2647:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r8
	leaq	(%r9,%rdx,8), %rdi
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L2648
	movq	144(%r13), %rax
	movq	%rax, (%rcx)
	movq	-56(%rbp), %rax
	movq	%rcx, 144(%r13)
	movq	%rax, (%rdi)
	cmpq	$0, (%rcx)
	je	.L2657
	movq	%rcx, (%r9,%r10,8)
	movq	%rdx, %r10
	testq	%rsi, %rsi
	jne	.L2647
	.p2align 4,,10
	.p2align 3
.L2646:
	movq	128(%r13), %rdi
	cmpq	%rdi, %r11
	je	.L2650
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r9
.L2650:
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%r8, 136(%r13)
	divq	%r8
	movq	%r9, 128(%r13)
	leaq	0(,%rdx,8), %r10
	leaq	(%r9,%r10), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L2683
.L2651:
	movq	144(%r13), %rdx
	movq	%rbx, 144(%r13)
	movq	%rdx, (%rbx)
	testq	%rdx, %rdx
	je	.L2653
	movq	8(%rdx), %rax
	xorl	%edx, %edx
	divq	136(%r13)
	movq	%rbx, (%r9,%rdx,8)
	movq	128(%r13), %rax
	addq	%r10, %rax
.L2653:
	movq	-56(%rbp), %rsi
	movq	%rsi, (%rax)
	jmp	.L2652
	.p2align 4,,10
	.p2align 3
.L2657:
	movq	%rdx, %r10
	jmp	.L2649
.L2681:
	leaq	176(%r13), %r9
	movq	$0, 176(%r13)
	movq	%r9, %r11
	jmp	.L2644
.L2682:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE23146:
	.size	_ZN2v88internal20CodeLargeObjectSpace21InsertChunkMapEntriesEPNS0_9LargePageE, .-_ZN2v88internal20CodeLargeObjectSpace21InsertChunkMapEntriesEPNS0_9LargePageE
	.section	.text._ZN2v88internal20CodeLargeObjectSpace7AddPageEPNS0_9LargePageEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20CodeLargeObjectSpace7AddPageEPNS0_9LargePageEm
	.type	_ZN2v88internal20CodeLargeObjectSpace7AddPageEPNS0_9LargePageEm, @function
_ZN2v88internal20CodeLargeObjectSpace7AddPageEPNS0_9LargePageEm:
.LFB23170:
	.cfi_startproc
	endbr64
	movslq	(%rsi), %rax
	addq	%rax, 104(%rdi)
	movq	80(%rdi), %rax
	addq	(%rsi), %rax
	movq	%rax, 80(%rdi)
	cmpq	88(%rdi), %rax
	jbe	.L2685
	movq	%rax, 88(%rdi)
.L2685:
	movq	40(%rdi), %rax
	addq	%rdx, 120(%rdi)
	addl	$1, 112(%rdi)
	testq	%rax, %rax
	je	.L2686
	movq	224(%rax), %rdx
	movq	%rax, 232(%rsi)
	movq	%rdx, 224(%rsi)
	movq	%rsi, 224(%rax)
	testq	%rdx, %rdx
	je	.L2687
	movq	%rsi, 232(%rdx)
	jmp	_ZN2v88internal20CodeLargeObjectSpace21InsertChunkMapEntriesEPNS0_9LargePageE
	.p2align 4,,10
	.p2align 3
.L2687:
	movq	%rsi, 40(%rdi)
	jmp	_ZN2v88internal20CodeLargeObjectSpace21InsertChunkMapEntriesEPNS0_9LargePageE
	.p2align 4,,10
	.p2align 3
.L2686:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 224(%rsi)
	movq	%rsi, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 32(%rdi)
	jmp	_ZN2v88internal20CodeLargeObjectSpace21InsertChunkMapEntriesEPNS0_9LargePageE
	.cfi_endproc
.LFE23170:
	.size	_ZN2v88internal20CodeLargeObjectSpace7AddPageEPNS0_9LargePageEm, .-_ZN2v88internal20CodeLargeObjectSpace7AddPageEPNS0_9LargePageEm
	.section	.text._ZN2v88internal20CodeLargeObjectSpace21RemoveChunkMapEntriesEPNS0_9LargePageE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20CodeLargeObjectSpace21RemoveChunkMapEntriesEPNS0_9LargePageE
	.type	_ZN2v88internal20CodeLargeObjectSpace21RemoveChunkMapEntriesEPNS0_9LargePageE, @function
_ZN2v88internal20CodeLargeObjectSpace21RemoveChunkMapEntriesEPNS0_9LargePageE:
.LFB23147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	144(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movq	%rsi, -64(%rbp)
	movq	%rdx, -72(%rbp)
	addq	%rsi, %rax
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rsi
	jnb	.L2689
	movq	%rdi, %r12
	movq	%rsi, %rbx
	.p2align 4,,10
	.p2align 3
.L2690:
	movq	136(%r12), %rcx
	movq	%rbx, %rax
	xorl	%edx, %edx
	movq	128(%r12), %r13
	divq	%rcx
	leaq	0(,%rdx,8), %r15
	movq	%rdx, %r11
	leaq	0(%r13,%r15), %r14
	movq	(%r14), %r9
	testq	%r9, %r9
	je	.L2691
	movq	(%r9), %rdi
	movq	%r9, %r10
	movq	8(%rdi), %rsi
	jmp	.L2693
	.p2align 4,,10
	.p2align 3
.L2716:
	movq	(%rdi), %r8
	testq	%r8, %r8
	je	.L2691
	movq	8(%r8), %rsi
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L2691
	movq	%r8, %rdi
.L2693:
	cmpq	%rbx, %rsi
	jne	.L2716
	movq	(%rdi), %rsi
	cmpq	%r10, %r9
	je	.L2717
	testq	%rsi, %rsi
	je	.L2695
	movq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L2695
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%rdi), %rsi
.L2695:
	movq	%rsi, (%r10)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	subq	$1, 152(%r12)
	movq	(%rax), %rdx
	addq	%rax, %rdx
	movq	%rdx, -56(%rbp)
.L2691:
	addq	$262144, %rbx
	cmpq	%rbx, -56(%rbp)
	ja	.L2690
.L2689:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2717:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L2700
	movq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L2695
	movq	%r10, 0(%r13,%rdx,8)
	addq	128(%r12), %r15
	movq	(%r15), %rax
	movq	%r15, %r14
	cmpq	-72(%rbp), %rax
	je	.L2718
.L2696:
	movq	$0, (%r14)
	movq	(%rdi), %rsi
	jmp	.L2695
	.p2align 4,,10
	.p2align 3
.L2700:
	movq	%r10, %rax
	cmpq	-72(%rbp), %rax
	jne	.L2696
.L2718:
	movq	%rsi, 144(%r12)
	jmp	.L2696
	.cfi_endproc
.LFE23147:
	.size	_ZN2v88internal20CodeLargeObjectSpace21RemoveChunkMapEntriesEPNS0_9LargePageE, .-_ZN2v88internal20CodeLargeObjectSpace21RemoveChunkMapEntriesEPNS0_9LargePageE
	.section	.text._ZN2v88internal20CodeLargeObjectSpace10RemovePageEPNS0_9LargePageEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20CodeLargeObjectSpace10RemovePageEPNS0_9LargePageEm
	.type	_ZN2v88internal20CodeLargeObjectSpace10RemovePageEPNS0_9LargePageEm, @function
_ZN2v88internal20CodeLargeObjectSpace10RemovePageEPNS0_9LargePageEm:
.LFB23171:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal20CodeLargeObjectSpace21RemoveChunkMapEntriesEPNS0_9LargePageE
	movslq	(%r12), %rax
	subq	%rax, 104(%rbx)
	movq	(%r12), %rax
	subq	%r13, 120(%rbx)
	subq	%rax, 80(%rbx)
	subl	$1, 112(%rbx)
	cmpq	40(%rbx), %r12
	je	.L2731
	movq	224(%r12), %rax
	cmpq	32(%rbx), %r12
	je	.L2732
.L2721:
	movq	232(%r12), %rdx
	testq	%rax, %rax
	je	.L2722
	movq	%rdx, 232(%rax)
.L2722:
	testq	%rdx, %rdx
	je	.L2723
	movq	%rax, 224(%rdx)
.L2723:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 224(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2731:
	.cfi_restore_state
	movq	232(%r12), %rax
	movq	%rax, 40(%rbx)
	movq	224(%r12), %rax
	cmpq	32(%rbx), %r12
	jne	.L2721
.L2732:
	movq	%rax, 32(%rbx)
	movq	224(%r12), %rax
	jmp	.L2721
	.cfi_endproc
.LFE23171:
	.size	_ZN2v88internal20CodeLargeObjectSpace10RemovePageEPNS0_9LargePageEm, .-_ZN2v88internal20CodeLargeObjectSpace10RemovePageEPNS0_9LargePageEm
	.section	.text._ZN2v88internal16LargeObjectSpace21PromoteNewLargeObjectEPNS0_9LargePageE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16LargeObjectSpace21PromoteNewLargeObjectEPNS0_9LargePageE
	.type	_ZN2v88internal16LargeObjectSpace21PromoteNewLargeObjectEPNS0_9LargePageE, @function
_ZN2v88internal16LargeObjectSpace21PromoteNewLargeObjectEPNS0_9LargePageE:
.LFB23148:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	-48(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	40(%rsi), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, -48(%rbp)
	movq	(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	%rbx, %rsi
	movslq	%eax, %r13
	movq	%r13, %rdx
	movq	80(%rbx), %rdi
	movq	(%rdi), %rax
	call	*136(%rax)
	movq	(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	*128(%rax)
	movq	8(%rbx), %rax
	movq	%rax, %rdx
	andq	$-9, %rdx
	movq	%rdx, 8(%rbx)
	movq	64(%r12), %rcx
	movq	2064(%rcx), %rcx
	cmpl	$1, 80(%rcx)
	jle	.L2734
	movq	%rdx, %rax
	orq	$262150, %rax
.L2735:
	movq	%rax, 8(%rbx)
	movq	%r12, 80(%rbx)
	mfence
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2738
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2734:
	.cfi_restore_state
	andq	$-262155, %rax
	orq	$4, %rax
	jmp	.L2735
.L2738:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23148:
	.size	_ZN2v88internal16LargeObjectSpace21PromoteNewLargeObjectEPNS0_9LargePageE, .-_ZN2v88internal16LargeObjectSpace21PromoteNewLargeObjectEPNS0_9LargePageE
	.section	.text._ZN2v88internal16LargeObjectSpace8ContainsENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16LargeObjectSpace8ContainsENS0_10HeapObjectE
	.type	_ZN2v88internal16LargeObjectSpace8ContainsENS0_10HeapObjectE, @function
_ZN2v88internal16LargeObjectSpace8ContainsENS0_10HeapObjectE:
.LFB23152:
	.cfi_startproc
	endbr64
	andq	$-262144, %rsi
	movq	80(%rsi), %rax
	cmpq	%rax, %rdi
	sete	%al
	ret
	.cfi_endproc
.LFE23152:
	.size	_ZN2v88internal16LargeObjectSpace8ContainsENS0_10HeapObjectE, .-_ZN2v88internal16LargeObjectSpace8ContainsENS0_10HeapObjectE
	.section	.text._ZN2v88internal16LargeObjectSpace12ContainsSlowEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16LargeObjectSpace12ContainsSlowEm
	.type	_ZN2v88internal16LargeObjectSpace12ContainsSlowEm, @function
_ZN2v88internal16LargeObjectSpace12ContainsSlowEm:
.LFB23153:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	testq	%rax, %rax
	je	.L2744
.L2743:
	cmpq	40(%rax), %rsi
	jb	.L2742
	cmpq	48(%rax), %rsi
	jnb	.L2742
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2742:
	movq	224(%rax), %rax
	testq	%rax, %rax
	jne	.L2743
	ret
	.p2align 4,,10
	.p2align 3
.L2744:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE23153:
	.size	_ZN2v88internal16LargeObjectSpace12ContainsSlowEm, .-_ZN2v88internal16LargeObjectSpace12ContainsSlowEm
	.section	.text._ZN2v88internal19NewLargeObjectSpaceC2EPNS0_4HeapEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19NewLargeObjectSpaceC2EPNS0_4HeapEm
	.type	_ZN2v88internal19NewLargeObjectSpaceC2EPNS0_4HeapEm, @function
_ZN2v88internal19NewLargeObjectSpaceC2EPNS0_4HeapEm:
.LFB23156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$48, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal10NoFreeListE(%rip), %rcx
	movq	%r13, 64(%rbx)
	pxor	%xmm0, %xmm0
	movq	%rcx, (%rax)
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %rcx
	movl	$16, %edi
	movq	%rcx, (%rbx)
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movb	$0, 56(%rbx)
	movl	$7, 72(%rbx)
	movq	%rax, 96(%rbx)
	movq	$0, 8(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 16(%rax)
	call	_Znam@PLT
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movq	%rax, 48(%rbx)
	movq	%rdx, (%rax)
	mfence
	movq	48(%rbx), %rax
	movq	%rdx, 8(%rax)
	leaq	16+_ZTVN2v88internal19NewLargeObjectSpaceE(%rip), %rax
	mfence
	movq	%r12, 136(%rbx)
	movq	$0, 104(%rbx)
	movl	$0, 112(%rbx)
	movq	%rax, (%rbx)
	movups	%xmm0, 120(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23156:
	.size	_ZN2v88internal19NewLargeObjectSpaceC2EPNS0_4HeapEm, .-_ZN2v88internal19NewLargeObjectSpaceC2EPNS0_4HeapEm
	.globl	_ZN2v88internal19NewLargeObjectSpaceC1EPNS0_4HeapEm
	.set	_ZN2v88internal19NewLargeObjectSpaceC1EPNS0_4HeapEm,_ZN2v88internal19NewLargeObjectSpaceC2EPNS0_4HeapEm
	.section	.text._ZN2v88internal19NewLargeObjectSpace4FlipEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19NewLargeObjectSpace4FlipEv
	.type	_ZN2v88internal19NewLargeObjectSpace4FlipEv, @function
_ZN2v88internal19NewLargeObjectSpace4FlipEv:
.LFB23160:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L2749
	.p2align 4,,10
	.p2align 3
.L2751:
	movq	8(%rdx), %rax
	andq	$-17, %rax
	orq	$8, %rax
	movq	%rax, 8(%rdx)
	movq	224(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2751
.L2749:
	ret
	.cfi_endproc
.LFE23160:
	.size	_ZN2v88internal19NewLargeObjectSpace4FlipEv, .-_ZN2v88internal19NewLargeObjectSpace4FlipEv
	.section	.text._ZN2v88internal19NewLargeObjectSpace11SetCapacityEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19NewLargeObjectSpace11SetCapacityEm
	.type	_ZN2v88internal19NewLargeObjectSpace11SetCapacityEm, @function
_ZN2v88internal19NewLargeObjectSpace11SetCapacityEm:
.LFB23162:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	leaq	_ZN2v88internal16LargeObjectSpace13SizeOfObjectsEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	80(%rax), %rax
	movq	%rdi, %rbx
	cmpq	%rdx, %rax
	jne	.L2757
	movq	120(%rdi), %rax
.L2758:
	cmpq	%r12, %rax
	cmovb	%r12, %rax
	movq	%rax, 136(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2757:
	.cfi_restore_state
	call	*%rax
	jmp	.L2758
	.cfi_endproc
.LFE23162:
	.size	_ZN2v88internal19NewLargeObjectSpace11SetCapacityEm, .-_ZN2v88internal19NewLargeObjectSpace11SetCapacityEm
	.section	.text._ZN2v88internal20CodeLargeObjectSpaceC2EPNS0_4HeapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20CodeLargeObjectSpaceC2EPNS0_4HeapE
	.type	_ZN2v88internal20CodeLargeObjectSpaceC2EPNS0_4HeapE, @function
_ZN2v88internal20CodeLargeObjectSpaceC2EPNS0_4HeapE:
.LFB23167:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	$48, %edi
	leaq	176(%rbx), %r13
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal10NoFreeListE(%rip), %rcx
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %rdx
	movq	$0, 8(%rbx)
	movq	%rcx, (%rax)
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	$0, 8(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	%rdx, (%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movb	$0, 56(%rbx)
	movq	%r12, 64(%rbx)
	movl	$6, 72(%rbx)
	movq	%rax, 96(%rbx)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 80(%rbx)
	call	_Znam@PLT
	leaq	160(%rbx), %rdi
	movl	$1024, %esi
	movq	%rax, 48(%rbx)
	movq	$0, (%rax)
	mfence
	movq	48(%rbx), %rax
	movq	$0, 8(%rax)
	leaq	16+_ZTVN2v88internal20CodeLargeObjectSpaceE(%rip), %rax
	mfence
	movq	$0, 104(%rbx)
	movl	$0, 112(%rbx)
	movq	$0, 120(%rbx)
	movq	%rax, (%rbx)
	movq	%r13, 128(%rbx)
	movq	$1, 136(%rbx)
	movq	$0, 144(%rbx)
	movq	$0, 152(%rbx)
	movl	$0x3f800000, 160(%rbx)
	movq	$0, 168(%rbx)
	movq	$0, 176(%rbx)
	call	_ZNKSt8__detail20_Prime_rehash_policy11_M_next_bktEm@PLT
	cmpq	136(%rbx), %rax
	jbe	.L2760
	movq	%rax, %r12
	cmpq	$1, %rax
	je	.L2766
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %r12
	ja	.L2767
	leaq	0(,%r12,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	memset@PLT
.L2763:
	movq	%r13, 128(%rbx)
	movq	%r12, 136(%rbx)
.L2760:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2766:
	.cfi_restore_state
	movq	$0, 176(%rbx)
	jmp	.L2763
.L2767:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE23167:
	.size	_ZN2v88internal20CodeLargeObjectSpaceC2EPNS0_4HeapE, .-_ZN2v88internal20CodeLargeObjectSpaceC2EPNS0_4HeapE
	.globl	_ZN2v88internal20CodeLargeObjectSpaceC1EPNS0_4HeapE
	.set	_ZN2v88internal20CodeLargeObjectSpaceC1EPNS0_4HeapE,_ZN2v88internal20CodeLargeObjectSpaceC2EPNS0_4HeapE
	.section	.text._ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE0EE8SetRangeEjj,"axG",@progbits,_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE0EE8SetRangeEjj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE0EE8SetRangeEjj
	.type	_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE0EE8SetRangeEjj, @function
_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE0EE8SetRangeEjj:
.LFB25875:
	.cfi_startproc
	endbr64
	cmpl	%edx, %esi
	jnb	.L2786
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	%esi, %r11d
	movl	$1, %esi
	subl	$1, %edx
	shrl	$5, %r11d
	movl	%edx, %r9d
	movl	%r11d, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%esi, %ebx
	shrl	$5, %r9d
	sall	%cl, %ebx
	movl	%edx, %ecx
	sall	%cl, %esi
	leaq	(%rdi,%rax,4), %rcx
	movl	%esi, %r10d
	cmpl	%r9d, %r11d
	je	.L2770
	movl	%ebx, %r8d
	subl	$1, %ebx
	negl	%r8d
	jmp	.L2774
	.p2align 4,,10
	.p2align 3
.L2771:
	movl	%ebx, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	orl	%r8d, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L2775
.L2774:
	movl	(%rcx), %edx
	movl	%r8d, %eax
	andl	%edx, %eax
	cmpl	%eax, %r8d
	jne	.L2771
.L2775:
	leal	1(%r11), %eax
	cmpl	%r9d, %eax
	jnb	.L2772
	leal	-2(%r9), %ecx
	movl	%eax, %edx
	subl	%r11d, %ecx
	leaq	(%rdi,%rdx,4), %rax
	addq	%rcx, %rdx
	leaq	4(%rdi,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L2777:
	movl	$-1, (%rax)
	addq	$4, %rax
	cmpq	%rdx, %rax
	jne	.L2777
.L2772:
	leal	-1(%r10), %ecx
	leaq	(%rdi,%r9,4), %rsi
	orl	%r10d, %ecx
	jmp	.L2776
	.p2align 4,,10
	.p2align 3
.L2778:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L2779
.L2776:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L2778
.L2779:
	mfence
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2770:
	.cfi_restore_state
	subl	%ebx, %esi
	orl	%r10d, %esi
	jmp	.L2780
	.p2align 4,,10
	.p2align 3
.L2790:
	movl	%esi, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	je	.L2779
.L2780:
	movl	(%rcx), %edx
	movl	%esi, %eax
	andl	%edx, %eax
	cmpl	%eax, %esi
	jne	.L2790
	mfence
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2786:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE25875:
	.size	_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE0EE8SetRangeEjj, .-_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE0EE8SetRangeEjj
	.section	.text._ZN2v88internal4Page15CreateBlackAreaEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Page15CreateBlackAreaEmm
	.type	_ZN2v88internal4Page15CreateBlackAreaEmm, @function
_ZN2v88internal4Page15CreateBlackAreaEmm:
.LFB22890:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	subl	%edi, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	subl	%edi, %edx
	shrl	$3, %esi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	shrl	$3, %edx
	subq	%r13, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE0EE8SetRangeEjj
	addq	%r12, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22890:
	.size	_ZN2v88internal4Page15CreateBlackAreaEmm, .-_ZN2v88internal4Page15CreateBlackAreaEmm
	.section	.text._ZN2v88internal10PagedSpace29MarkLinearAllocationAreaBlackEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace29MarkLinearAllocationAreaBlackEv
	.type	_ZN2v88internal10PagedSpace29MarkLinearAllocationAreaBlackEv, @function
_ZN2v88internal10PagedSpace29MarkLinearAllocationAreaBlackEv:
.LFB22955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	104(%rdi), %rbx
	movq	112(%rdi), %r12
	testq	%rbx, %rbx
	je	.L2793
	cmpq	%r12, %rbx
	jne	.L2802
.L2793:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2802:
	.cfi_restore_state
	leaq	-8(%rbx), %r13
	movl	%r12d, %edx
	movl	%ebx, %esi
	subq	%rbx, %r12
	andq	$-262144, %r13
	movq	16(%r13), %rdi
	subl	%r13d, %edx
	subl	%r13d, %esi
	shrl	$3, %edx
	shrl	$3, %esi
	call	_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE0EE8SetRangeEjj
	addq	%r12, 96(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22955:
	.size	_ZN2v88internal10PagedSpace29MarkLinearAllocationAreaBlackEv, .-_ZN2v88internal10PagedSpace29MarkLinearAllocationAreaBlackEv
	.section	.text._ZN2v88internal10PagedSpace23SetLinearAllocationAreaEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace23SetLinearAllocationAreaEmm
	.type	_ZN2v88internal10PagedSpace23SetLinearAllocationAreaEmm, @function
_ZN2v88internal10PagedSpace23SetLinearAllocationAreaEmm:
.LFB22950:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	104(%rdi), %rax
	testq	%rax, %rax
	je	.L2804
	leaq	-1(%rax), %rcx
	andq	$-262144, %rcx
	subq	%rcx, %rax
	addq	$152, %rcx
	movq	%rax, %rdx
.L2805:
	movq	(%rcx), %rax
	cmpq	%rax, %rdx
	jle	.L2804
	lock cmpxchgq	%rdx, (%rcx)
	jne	.L2805
.L2804:
	movq	%rbx, %xmm0
	movq	%r12, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 104(%rdi)
	testq	%rbx, %rbx
	je	.L2803
	cmpq	%r12, %rbx
	je	.L2803
	movq	64(%rdi), %rax
	movq	2064(%rax), %rax
	cmpb	$0, 87(%rax)
	jne	.L2819
.L2803:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2819:
	.cfi_restore_state
	leaq	-8(%rbx), %r13
	movl	%r12d, %edx
	movl	%ebx, %esi
	subq	%rbx, %r12
	andq	$-262144, %r13
	movq	16(%r13), %rdi
	subl	%r13d, %edx
	subl	%r13d, %esi
	shrl	$3, %edx
	shrl	$3, %esi
	call	_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE0EE8SetRangeEjj
	addq	%r12, 96(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22950:
	.size	_ZN2v88internal10PagedSpace23SetLinearAllocationAreaEmm, .-_ZN2v88internal10PagedSpace23SetLinearAllocationAreaEmm
	.section	.text._ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE0EE10ClearRangeEjj,"axG",@progbits,_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE0EE10ClearRangeEjj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE0EE10ClearRangeEjj
	.type	_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE0EE10ClearRangeEjj, @function
_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE0EE10ClearRangeEjj:
.LFB25876:
	.cfi_startproc
	endbr64
	cmpl	%edx, %esi
	jnb	.L2841
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$1, %edx
	movl	%esi, %r10d
	movl	%esi, %ecx
	shrl	$5, %r10d
	movl	%edx, %r9d
	movl	%r10d, %eax
	shrl	$5, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	$1, %ebx
	movl	%ebx, %r8d
	sall	%cl, %r8d
	movl	%edx, %ecx
	sall	%cl, %ebx
	leaq	(%rdi,%rax,4), %rcx
	cmpl	%r9d, %r10d
	je	.L2822
	movl	%r8d, %r11d
	subl	$1, %r8d
	negl	%r11d
	jmp	.L2826
	.p2align 4,,10
	.p2align 3
.L2823:
	movl	%r8d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L2827
.L2826:
	movl	(%rcx), %edx
	testl	%edx, %r11d
	jne	.L2823
.L2827:
	leal	1(%r10), %eax
	cmpl	%r9d, %eax
	jnb	.L2824
	leal	-2(%r9), %ecx
	movl	%eax, %edx
	subl	%r10d, %ecx
	leaq	(%rdi,%rdx,4), %rax
	addq	%rcx, %rdx
	leaq	4(%rdi,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L2829:
	movl	$0, (%rax)
	addq	$4, %rax
	cmpq	%rdx, %rax
	jne	.L2829
.L2824:
	leal	-1(%rbx), %r8d
	leaq	(%rdi,%r9,4), %rcx
	orl	%ebx, %r8d
	movl	%r8d, %edi
	notl	%edi
	jmp	.L2828
	.p2align 4,,10
	.p2align 3
.L2830:
	movl	%edi, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L2831
.L2828:
	movl	(%rcx), %edx
	testl	%edx, %r8d
	jne	.L2830
.L2831:
	mfence
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2822:
	.cfi_restore_state
	movl	%ebx, %eax
	subl	%r8d, %eax
	movl	%eax, %r8d
	orl	%ebx, %r8d
	movl	%r8d, %edi
	notl	%edi
	jmp	.L2832
	.p2align 4,,10
	.p2align 3
.L2845:
	movl	%edi, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L2831
.L2832:
	movl	(%rcx), %edx
	testl	%edx, %r8d
	jne	.L2845
	mfence
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2841:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE25876:
	.size	_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE0EE10ClearRangeEjj, .-_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE0EE10ClearRangeEjj
	.section	.text._ZN2v88internal4Page16DestroyBlackAreaEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Page16DestroyBlackAreaEmm
	.type	_ZN2v88internal4Page16DestroyBlackAreaEmm, @function
_ZN2v88internal4Page16DestroyBlackAreaEmm:
.LFB22891:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	subl	%edi, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	subl	%edi, %edx
	shrl	$3, %esi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	shrl	$3, %edx
	subq	%r13, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE0EE10ClearRangeEjj
	subq	%r12, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22891:
	.size	_ZN2v88internal4Page16DestroyBlackAreaEmm, .-_ZN2v88internal4Page16DestroyBlackAreaEmm
	.section	.text._ZN2v88internal10PagedSpace24FreeLinearAllocationAreaEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace24FreeLinearAllocationAreaEv
	.type	_ZN2v88internal10PagedSpace24FreeLinearAllocationAreaEv, @function
_ZN2v88internal10PagedSpace24FreeLinearAllocationAreaEv:
.LFB22957:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	104(%rdi), %r12
	testq	%r12, %r12
	je	.L2848
	movq	64(%rdi), %rax
	movq	112(%rdi), %r13
	movq	%rdi, %rbx
	movq	2064(%rax), %rdx
	cmpb	$0, 87(%rdx)
	jne	.L2876
.L2851:
	cmpb	$0, 432(%rax)
	jne	.L2877
.L2852:
	movq	120(%rbx), %r15
	testq	%r15, %r15
	je	.L2853
	cmpq	%r15, %r12
	jnb	.L2855
	movq	%r12, 120(%rbx)
	movq	%r12, %r15
.L2855:
	cmpb	$0, 56(%rbx)
	je	.L2878
.L2856:
	movq	$0, 120(%rbx)
.L2853:
	movq	104(%rbx), %rax
	testq	%rax, %rax
	je	.L2863
.L2860:
	leaq	-1(%rax), %rcx
	andq	$-262144, %rcx
	subq	%rcx, %rax
	addq	$152, %rcx
	movq	%rax, %rdx
.L2864:
	movq	(%rcx), %rax
	cmpq	%rax, %rdx
	jle	.L2863
	lock cmpxchgq	%rdx, (%rcx)
	jne	.L2864
.L2863:
	pxor	%xmm0, %xmm0
	cmpl	$3, 72(%rbx)
	movups	%xmm0, 104(%rbx)
	je	.L2879
.L2862:
	movq	%r13, %r14
	subq	%r12, %r14
	je	.L2848
	movq	64(%rbx), %rdi
	movq	%r12, %rsi
	movl	$1, %r8d
	movl	%r14d, %edx
	movl	$1, %ecx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	96(%rbx), %rdi
	movq	%r12, %rsi
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	subq	%r13, %r12
	movq	(%rdi), %rax
	call	*24(%rax)
	addq	%r12, 184(%rbx)
.L2848:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2876:
	.cfi_restore_state
	cmpq	%r13, %r12
	je	.L2851
	leaq	-8(%r12), %r14
	movl	%r13d, %edx
	movl	%r12d, %esi
	andq	$-262144, %r14
	subl	%r14d, %edx
	subl	%r14d, %esi
	movq	16(%r14), %rdi
	shrl	$3, %edx
	shrl	$3, %esi
	call	_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE0EE10ClearRangeEjj
	movl	%r12d, %eax
	subl	%r13d, %eax
	cltq
	addq	%rax, 96(%r14)
	movq	64(%rbx), %rax
	cmpb	$0, 432(%rax)
	je	.L2852
	jmp	.L2853
	.p2align 4,,10
	.p2align 3
.L2878:
	movq	8(%rbx), %rsi
	cmpq	%rsi, 16(%rbx)
	je	.L2856
	movb	$1, 432(%rax)
	movq	64(%rbx), %rdi
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	$1, %r8d
	movl	$1, %ecx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	16(%rbx), %rax
	movl	%r12d, %esi
	movq	8(%rbx), %r14
	subl	%r15d, %esi
	movq	%rax, -56(%rbp)
	movl	%esi, %r15d
	cmpq	%rax, %r14
	je	.L2859
	.p2align 4,,10
	.p2align 3
.L2858:
	movq	(%r14), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r15d, %esi
	addq	$8, %r14
	call	_ZN2v88internal18AllocationObserver14AllocationStepEimm@PLT
	cmpq	%r14, -56(%rbp)
	jne	.L2858
.L2859:
	movq	64(%rbx), %rax
	movb	$0, 432(%rax)
	jmp	.L2856
	.p2align 4,,10
	.p2align 3
.L2877:
	movq	%r12, %rax
	jmp	.L2860
	.p2align 4,,10
	.p2align 3
.L2879:
	movq	%r12, %rsi
	movq	64(%rbx), %rdi
	andq	$-262144, %rsi
	call	_ZN2v88internal4Heap31UnprotectAndRegisterMemoryChunkEPNS0_11MemoryChunkE@PLT
	jmp	.L2862
	.cfi_endproc
.LFE22957:
	.size	_ZN2v88internal10PagedSpace24FreeLinearAllocationAreaEv, .-_ZN2v88internal10PagedSpace24FreeLinearAllocationAreaEv
	.section	.text._ZN2v88internal10PagedSpace21PrepareForMarkCompactEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace21PrepareForMarkCompactEv
	.type	_ZN2v88internal10PagedSpace21PrepareForMarkCompactEv, @function
_ZN2v88internal10PagedSpace21PrepareForMarkCompactEv:
.LFB23098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal10PagedSpace24FreeLinearAllocationAreaEv
	movq	96(%rbx), %rdi
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE23098:
	.size	_ZN2v88internal10PagedSpace21PrepareForMarkCompactEv, .-_ZN2v88internal10PagedSpace21PrepareForMarkCompactEv
	.section	.text._ZN2v88internal10PagedSpace38RefillLinearAllocationAreaFromFreeListEmNS0_16AllocationOriginE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace38RefillLinearAllocationAreaFromFreeListEmNS0_16AllocationOriginE
	.type	_ZN2v88internal10PagedSpace38RefillLinearAllocationAreaFromFreeListEmNS0_16AllocationOriginE, @function
_ZN2v88internal10PagedSpace38RefillLinearAllocationAreaFromFreeListEmNS0_16AllocationOriginE:
.LFB22963:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal10PagedSpace24FreeLinearAllocationAreaEv
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*152(%rax)
	testb	%al, %al
	je	.L2893
.L2883:
	movq	96(%r12), %rdi
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	$0, -64(%rbp)
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rax, %rbx
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L2882
	movq	-64(%rbp), %r14
	movq	64(%r12), %rax
	leaq	-1(%rbx), %r15
	addq	%r14, 184(%r12)
	addq	%r15, %r14
	cmpb	$0, 1520(%rax)
	jne	.L2894
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19SpaceWithLinearArea12ComputeLimitEmmm.part.0
	movq	%rax, %r13
.L2886:
	cmpq	%r13, %r14
	je	.L2887
	cmpl	$3, 72(%r12)
	movq	64(%r12), %rdi
	je	.L2895
.L2888:
	movq	%r14, %rbx
	movl	$1, %r8d
	movl	$1, %ecx
	movq	%r13, %rsi
	subq	%r13, %rbx
	movl	%ebx, %edx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	96(%r12), %rdi
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	184(%r12), %rax
	subq	%r14, %rax
	addq	%r13, %rax
	movq	%rax, 184(%r12)
.L2887:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10PagedSpace23SetLinearAllocationAreaEmm
	movl	$1, %eax
.L2882:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2896
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2894:
	.cfi_restore_state
	addq	%r15, %r13
	jmp	.L2886
	.p2align 4,,10
	.p2align 3
.L2893:
	movq	64(%r12), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal4Heap28ShouldOptimizeForMemoryUsageEv@PLT
	movl	$64, %edx
	movq	%r15, %rdi
	movzbl	%al, %esi
	call	_ZN2v88internal4Heap49StartIncrementalMarkingIfAllocationLimitIsReachedEiNS_15GCCallbackFlagsE@PLT
	jmp	.L2883
	.p2align 4,,10
	.p2align 3
.L2895:
	andq	$-262144, %rbx
	movq	%rbx, %rsi
	call	_ZN2v88internal4Heap31UnprotectAndRegisterMemoryChunkEPNS0_11MemoryChunkE@PLT
	movq	64(%r12), %rdi
	jmp	.L2888
.L2896:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22963:
	.size	_ZN2v88internal10PagedSpace38RefillLinearAllocationAreaFromFreeListEmNS0_16AllocationOriginE, .-_ZN2v88internal10PagedSpace38RefillLinearAllocationAreaFromFreeListEmNS0_16AllocationOriginE
	.section	.text._ZN2v88internal10PagedSpace23SweepAndRetryAllocationEiNS0_16AllocationOriginE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace23SweepAndRetryAllocationEiNS0_16AllocationOriginE
	.type	_ZN2v88internal10PagedSpace23SweepAndRetryAllocationEiNS0_16AllocationOriginE, @function
_ZN2v88internal10PagedSpace23SweepAndRetryAllocationEiNS0_16AllocationOriginE:
.LFB23100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	64(%rdi), %rax
	movq	2016(%rax), %rdi
	movq	9984(%rdi), %rax
	cmpb	$0, 265(%rax)
	jne	.L2900
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2900:
	.cfi_restore_state
	movl	%esi, %ebx
	movl	%edx, %r13d
	call	_ZN2v88internal20MarkCompactCollector23EnsureSweepingCompletedEv@PLT
	addq	$8, %rsp
	movslq	%ebx, %rsi
	movl	%r13d, %edx
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10PagedSpace38RefillLinearAllocationAreaFromFreeListEmNS0_16AllocationOriginE
	.cfi_endproc
.LFE23100:
	.size	_ZN2v88internal10PagedSpace23SweepAndRetryAllocationEiNS0_16AllocationOriginE, .-_ZN2v88internal10PagedSpace23SweepAndRetryAllocationEiNS0_16AllocationOriginE
	.section	.text._ZN2v88internal15CompactionSpace23SweepAndRetryAllocationEiNS0_16AllocationOriginE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15CompactionSpace23SweepAndRetryAllocationEiNS0_16AllocationOriginE
	.type	_ZN2v88internal15CompactionSpace23SweepAndRetryAllocationEiNS0_16AllocationOriginE, @function
_ZN2v88internal15CompactionSpace23SweepAndRetryAllocationEiNS0_16AllocationOriginE:
.LFB23101:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_sweepingE(%rip)
	jne	.L2913
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2913:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	64(%rdi), %rax
	movq	2016(%rax), %rax
	movq	9984(%rax), %rdi
	cmpb	$0, 265(%rdi)
	jne	.L2914
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2914:
	.cfi_restore_state
	movl	%esi, %ebx
	movl	72(%r12), %esi
	movl	%edx, %r13d
	movl	$1, %r8d
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	call	_ZN2v88internal7Sweeper18ParallelSweepSpaceENS0_15AllocationSpaceEiiNS1_35FreeSpaceMayContainInvalidatedSlotsE@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*160(%rax)
	addq	$8, %rsp
	movslq	%ebx, %rsi
	movl	%r13d, %edx
	popq	%rbx
	.cfi_restore 3
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10PagedSpace38RefillLinearAllocationAreaFromFreeListEmNS0_16AllocationOriginE
	.cfi_endproc
.LFE23101:
	.size	_ZN2v88internal15CompactionSpace23SweepAndRetryAllocationEiNS0_16AllocationOriginE, .-_ZN2v88internal15CompactionSpace23SweepAndRetryAllocationEiNS0_16AllocationOriginE
	.section	.text._ZN2v88internal10PagedSpace28ShrinkImmortalImmovablePagesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace28ShrinkImmortalImmovablePagesEv
	.type	_ZN2v88internal10PagedSpace28ShrinkImmortalImmovablePagesEv, @function
_ZN2v88internal10PagedSpace28ShrinkImmortalImmovablePagesEv:
.LFB22947:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	104(%rdi), %rax
	testq	%rax, %rax
	je	.L2919
	leaq	-1(%rax), %rcx
	andq	$-262144, %rcx
	subq	%rcx, %rax
	addq	$152, %rcx
	movq	%rax, %rdx
.L2920:
	movq	(%rcx), %rax
	cmpq	%rax, %rdx
	jle	.L2919
	lock cmpxchgq	%rdx, (%rcx)
	jne	.L2920
.L2919:
	movq	%r13, %rdi
	leaq	168(%r13), %r12
	call	_ZN2v88internal10PagedSpace24FreeLinearAllocationAreaEv
	movq	%r13, %rdi
	call	_ZN2v88internal10PagedSpace13ResetFreeListEv
	movq	32(%r13), %rbx
	testq	%rbx, %rbx
	je	.L2915
	.p2align 4,,10
	.p2align 3
.L2923:
	cmpq	$0, 64(%rbx)
	je	.L2921
	movq	%rbx, %rdi
	call	_ZN2v88internal4Page21ShrinkToHighWaterMarkEv.part.0
	lock subq	%rax, (%r12)
	subq	%rax, 80(%r13)
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L2923
.L2915:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2921:
	.cfi_restore_state
	lock subq	$0, (%r12)
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L2923
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22947:
	.size	_ZN2v88internal10PagedSpace28ShrinkImmortalImmovablePagesEv, .-_ZN2v88internal10PagedSpace28ShrinkImmortalImmovablePagesEv
	.section	.text._ZN2v88internal10PagedSpace20MergeCompactionSpaceEPNS0_15CompactionSpaceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace20MergeCompactionSpaceEPNS0_15CompactionSpaceE
	.type	_ZN2v88internal10PagedSpace20MergeCompactionSpaceEPNS0_15CompactionSpaceE, @function
_ZN2v88internal10PagedSpace20MergeCompactionSpaceEPNS0_15CompactionSpaceE:
.LFB22938:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	192(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal10PagedSpace24FreeLinearAllocationAreaEv
	leaq	128(%r14), %rdx
	leaq	144(%r12), %rax
	cmpq	%rax, %rdx
	jnb	.L2935
	leaq	144(%r14), %rdx
	leaq	128(%r12), %rax
	cmpq	%rax, %rdx
	ja	.L2930
.L2935:
	movdqu	128(%r14), %xmm1
	movdqu	128(%r12), %xmm0
	paddq	%xmm1, %xmm0
	movups	%xmm0, 128(%r12)
	movq	144(%r14), %rax
	addq	%rax, 144(%r12)
.L2932:
	movq	32(%r14), %rbx
	testq	%rbx, %rbx
	je	.L2933
	.p2align 4,,10
	.p2align 3
.L2934:
	movq	%rbx, %r13
	movq	%r14, %rdi
	movq	224(%rbx), %rbx
	movq	%r13, %rsi
	call	_ZN2v88internal10PagedSpace10RemovePageEPNS0_4PageE
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10PagedSpace7AddPageEPNS0_4PageE
	testq	%rbx, %rbx
	jne	.L2934
.L2933:
	addq	$8, %rsp
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L2930:
	.cfi_restore_state
	movq	128(%r14), %rax
	addq	%rax, 128(%r12)
	movq	136(%r14), %rax
	addq	%rax, 136(%r12)
	movq	144(%r14), %rax
	addq	%rax, 144(%r12)
	jmp	.L2932
	.cfi_endproc
.LFE22938:
	.size	_ZN2v88internal10PagedSpace20MergeCompactionSpaceEPNS0_15CompactionSpaceE, .-_ZN2v88internal10PagedSpace20MergeCompactionSpaceEPNS0_15CompactionSpaceE
	.section	.text._ZN2v88internal10PagedSpace26UnmarkLinearAllocationAreaEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace26UnmarkLinearAllocationAreaEv
	.type	_ZN2v88internal10PagedSpace26UnmarkLinearAllocationAreaEv, @function
_ZN2v88internal10PagedSpace26UnmarkLinearAllocationAreaEv:
.LFB22956:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	104(%rdi), %rbx
	movq	112(%rdi), %r12
	testq	%rbx, %rbx
	je	.L2941
	cmpq	%r12, %rbx
	jne	.L2950
.L2941:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2950:
	.cfi_restore_state
	leaq	-8(%rbx), %r13
	movl	%r12d, %edx
	movl	%ebx, %esi
	subq	%rbx, %r12
	andq	$-262144, %r13
	movq	16(%r13), %rdi
	subl	%r13d, %edx
	subl	%r13d, %esi
	shrl	$3, %edx
	shrl	$3, %esi
	call	_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE0EE10ClearRangeEjj
	subq	%r12, 96(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22956:
	.size	_ZN2v88internal10PagedSpace26UnmarkLinearAllocationAreaEv, .-_ZN2v88internal10PagedSpace26UnmarkLinearAllocationAreaEv
	.section	.text._ZN2v88internal10PagedSpace13DecreaseLimitEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace13DecreaseLimitEm
	.type	_ZN2v88internal10PagedSpace13DecreaseLimitEm, @function
_ZN2v88internal10PagedSpace13DecreaseLimitEm:
.LFB22951:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	112(%rdi), %r14
	cmpq	%r14, %rsi
	je	.L2951
	movq	104(%rdi), %rdx
	movq	%rdi, %rbx
	movq	%rsi, %r12
	testq	%rdx, %rdx
	je	.L2954
	leaq	-1(%rdx), %rax
	movq	%rdx, %rsi
	andq	$-262144, %rax
	subq	%rax, %rsi
	leaq	152(%rax), %rcx
.L2955:
	movq	(%rcx), %rax
	cmpq	%rax, %rsi
	jle	.L2954
	lock cmpxchgq	%rsi, (%rcx)
	jne	.L2955
.L2954:
	movq	%r12, %xmm1
	movq	%rdx, %xmm0
	movq	%r14, %r13
	movq	%r12, %rsi
	punpcklqdq	%xmm1, %xmm0
	subq	%r12, %r13
	movq	64(%rbx), %rdi
	movl	$1, %ecx
	movups	%xmm0, 104(%rbx)
	movl	$1, %r8d
	movl	%r13d, %edx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	96(%rbx), %rdi
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%r12, %rax
	subq	%r14, %rax
	addq	%rax, 184(%rbx)
	movq	64(%rbx), %rax
	movq	2064(%rax), %rax
	cmpb	$0, 87(%rax)
	jne	.L2963
.L2951:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2963:
	.cfi_restore_state
	leaq	-8(%r12), %rbx
	movl	%r14d, %edx
	movl	%r12d, %esi
	andq	$-262144, %rbx
	movq	16(%rbx), %rdi
	subl	%ebx, %edx
	subl	%ebx, %esi
	shrl	$3, %edx
	shrl	$3, %esi
	call	_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE0EE10ClearRangeEjj
	subq	%r13, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22951:
	.size	_ZN2v88internal10PagedSpace13DecreaseLimitEm, .-_ZN2v88internal10PagedSpace13DecreaseLimitEm
	.section	.text._ZN2v88internal10PagedSpace27UpdateInlineAllocationLimitEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace27UpdateInlineAllocationLimitEm
	.type	_ZN2v88internal10PagedSpace27UpdateInlineAllocationLimitEm, @function
_ZN2v88internal10PagedSpace27UpdateInlineAllocationLimitEm:
.LFB22984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	64(%rdi), %rax
	movq	%rdi, %rbx
	movq	104(%rdi), %rsi
	movq	112(%rdi), %r14
	cmpb	$0, 1520(%rax)
	leaq	(%rcx,%rsi), %r12
	jne	.L2966
	movq	%r14, %rdx
	call	_ZN2v88internal19SpaceWithLinearArea12ComputeLimitEmmm.part.0
	movq	112(%rbx), %r14
	movq	%rax, %r12
.L2966:
	cmpq	%r14, %r12
	je	.L2964
	movq	104(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L2972
	leaq	-1(%rdx), %rax
	movq	%rdx, %rsi
	andq	$-262144, %rax
	subq	%rax, %rsi
	leaq	152(%rax), %rcx
.L2973:
	movq	(%rcx), %rax
	cmpq	%rax, %rsi
	jle	.L2972
	lock cmpxchgq	%rsi, (%rcx)
	jne	.L2973
.L2972:
	movq	%r12, %xmm1
	movq	%rdx, %xmm0
	movq	%r14, %r13
	movq	%r12, %rsi
	punpcklqdq	%xmm1, %xmm0
	subq	%r12, %r13
	movq	64(%rbx), %rdi
	movl	$1, %ecx
	movups	%xmm0, 104(%rbx)
	movl	$1, %r8d
	movl	%r13d, %edx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	96(%rbx), %rdi
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%r12, %rax
	subq	%r14, %rax
	addq	%rax, 184(%rbx)
	movq	64(%rbx), %rax
	movq	2064(%rax), %rax
	cmpb	$0, 87(%rax)
	jne	.L2975
.L2964:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2975:
	.cfi_restore_state
	leaq	-8(%r12), %rbx
	movl	%r14d, %edx
	movl	%r12d, %esi
	andq	$-262144, %rbx
	movq	16(%rbx), %rdi
	subl	%ebx, %edx
	subl	%ebx, %esi
	shrl	$3, %edx
	shrl	$3, %esi
	call	_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE0EE10ClearRangeEjj
	subq	%r13, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22984:
	.size	_ZN2v88internal10PagedSpace27UpdateInlineAllocationLimitEm, .-_ZN2v88internal10PagedSpace27UpdateInlineAllocationLimitEm
	.section	.text._ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	.type	_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_, @function
_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_:
.LFB26522:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L2990
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L2986
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L2991
.L2978:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L2985:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L2992
	testq	%r13, %r13
	jg	.L2981
	testq	%r9, %r9
	jne	.L2984
.L2982:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2992:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L2981
.L2984:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L2982
	.p2align 4,,10
	.p2align 3
.L2991:
	testq	%rsi, %rsi
	jne	.L2979
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L2985
	.p2align 4,,10
	.p2align 3
.L2981:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L2982
	jmp	.L2984
	.p2align 4,,10
	.p2align 3
.L2986:
	movl	$8, %r14d
	jmp	.L2978
.L2990:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2979:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L2978
	.cfi_endproc
.LFE26522:
	.size	_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_, .-_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	.section	.text._ZN2v84base20BoundedPageAllocatorD2Ev,"axG",@progbits,_ZN2v84base20BoundedPageAllocatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base20BoundedPageAllocatorD2Ev
	.type	_ZN2v84base20BoundedPageAllocatorD2Ev, @function
_ZN2v84base20BoundedPageAllocatorD2Ev:
.LFB27567:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v84base20BoundedPageAllocatorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	72(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -72(%rdi)
	call	_ZN2v84base15RegionAllocatorD1Ev@PLT
	addq	$8, %rsp
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5MutexD1Ev@PLT
	.cfi_endproc
.LFE27567:
	.size	_ZN2v84base20BoundedPageAllocatorD2Ev, .-_ZN2v84base20BoundedPageAllocatorD2Ev
	.weak	_ZN2v84base20BoundedPageAllocatorD1Ev
	.set	_ZN2v84base20BoundedPageAllocatorD1Ev,_ZN2v84base20BoundedPageAllocatorD2Ev
	.section	.text._ZNSt8_Rb_treeImmSt9_IdentityImESt4lessImESaImEE8_M_eraseEPSt13_Rb_tree_nodeImE,"axG",@progbits,_ZNSt8_Rb_treeImmSt9_IdentityImESt4lessImESaImEE8_M_eraseEPSt13_Rb_tree_nodeImE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeImmSt9_IdentityImESt4lessImESaImEE8_M_eraseEPSt13_Rb_tree_nodeImE
	.type	_ZNSt8_Rb_treeImmSt9_IdentityImESt4lessImESaImEE8_M_eraseEPSt13_Rb_tree_nodeImE, @function
_ZNSt8_Rb_treeImmSt9_IdentityImESt4lessImESaImEE8_M_eraseEPSt13_Rb_tree_nodeImE:
.LFB27670:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L3003
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L2997:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeImmSt9_IdentityImESt4lessImESaImEE8_M_eraseEPSt13_Rb_tree_nodeImE
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2997
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3003:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE27670:
	.size	_ZNSt8_Rb_treeImmSt9_IdentityImESt4lessImESaImEE8_M_eraseEPSt13_Rb_tree_nodeImE, .-_ZNSt8_Rb_treeImmSt9_IdentityImESt4lessImESaImEE8_M_eraseEPSt13_Rb_tree_nodeImE
	.section	.text._ZN2v88internal18CodeObjectRegistry5ClearEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18CodeObjectRegistry5ClearEv
	.type	_ZN2v88internal18CodeObjectRegistry5ClearEv, @function
_ZN2v88internal18CodeObjectRegistry5ClearEv:
.LFB22838:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	cmpq	8(%rdi), %rax
	je	.L3007
	movq	%rax, 8(%rdi)
.L3007:
	movq	40(%rbx), %r12
	leaq	24(%rbx), %r13
	testq	%r12, %r12
	je	.L3008
.L3009:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeImmSt9_IdentityImESt4lessImESaImEE8_M_eraseEPSt13_Rb_tree_nodeImE
	movq	%r12, %rdi
	movq	16(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L3009
.L3008:
	leaq	32(%rbx), %rax
	movq	$0, 40(%rbx)
	movq	%rax, 48(%rbx)
	movq	%rax, 56(%rbx)
	movq	$0, 64(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22838:
	.size	_ZN2v88internal18CodeObjectRegistry5ClearEv, .-_ZN2v88internal18CodeObjectRegistry5ClearEv
	.section	.text._ZNSt6vectorIPN2v88internal18AllocationObserverESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal18AllocationObserverESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal18AllocationObserverESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal18AllocationObserverESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal18AllocationObserverESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB27711:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L3029
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L3025
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L3030
.L3017:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L3024:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L3031
	testq	%r13, %r13
	jg	.L3020
	testq	%r9, %r9
	jne	.L3023
.L3021:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3031:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L3020
.L3023:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L3021
	.p2align 4,,10
	.p2align 3
.L3030:
	testq	%rsi, %rsi
	jne	.L3018
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L3024
	.p2align 4,,10
	.p2align 3
.L3020:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L3021
	jmp	.L3023
	.p2align 4,,10
	.p2align 3
.L3025:
	movl	$8, %r14d
	jmp	.L3017
.L3029:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3018:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L3017
	.cfi_endproc
.LFE27711:
	.size	_ZNSt6vectorIPN2v88internal18AllocationObserverESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIPN2v88internal18AllocationObserverESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZN2v88internal19SpaceWithLinearArea21AddAllocationObserverEPNS0_18AllocationObserverE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SpaceWithLinearArea21AddAllocationObserverEPNS0_18AllocationObserverE
	.type	_ZN2v88internal19SpaceWithLinearArea21AddAllocationObserverEPNS0_18AllocationObserverE, @function
_ZN2v88internal19SpaceWithLinearArea21AddAllocationObserverEPNS0_18AllocationObserverE:
.LFB22990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	64(%rdi), %rax
	cmpb	$0, 432(%rax)
	jne	.L3033
	movq	120(%rdi), %r15
	testq	%r15, %r15
	jne	.L3050
.L3033:
	movq	%rbx, -64(%rbp)
	cmpq	%rsi, 24(%r12)
	je	.L3039
.L3053:
	movq	%rbx, (%rsi)
	addq	$8, 16(%r12)
.L3040:
	movq	(%r12), %rdx
	leaq	_ZN2v88internal19SpaceWithLinearArea29StartNextInlineAllocationStepEv(%rip), %rcx
	movq	48(%rdx), %rax
	cmpq	%rcx, %rax
	jne	.L3041
	movq	64(%r12), %rax
	cmpb	$0, 432(%rax)
	jne	.L3032
	cmpb	$0, 56(%r12)
	je	.L3051
.L3032:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3052
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3050:
	.cfi_restore_state
	movq	104(%rdi), %r13
	cmpq	%r15, %r13
	jnb	.L3034
	movq	%r13, 120(%rdi)
	movq	%r13, %r15
.L3034:
	cmpb	$0, 56(%r12)
	jne	.L3035
	cmpq	%rsi, 8(%r12)
	je	.L3035
	movb	$1, 432(%rax)
	movq	64(%r12), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$1, %ecx
	movl	$1, %r8d
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	16(%r12), %rax
	movl	%r13d, %ecx
	movq	8(%r12), %r14
	subl	%r15d, %ecx
	movq	%rax, -72(%rbp)
	movl	%ecx, %r15d
	cmpq	%rax, %r14
	je	.L3038
	.p2align 4,,10
	.p2align 3
.L3037:
	movq	(%r14), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r15d, %esi
	addq	$8, %r14
	call	_ZN2v88internal18AllocationObserver14AllocationStepEimm@PLT
	cmpq	%r14, -72(%rbp)
	jne	.L3037
.L3038:
	movq	64(%r12), %rax
	movb	$0, 432(%rax)
	movq	16(%r12), %rsi
.L3035:
	movq	%r13, 120(%r12)
	movq	%rbx, -64(%rbp)
	cmpq	%rsi, 24(%r12)
	jne	.L3053
.L3039:
	leaq	-64(%rbp), %rdx
	leaq	8(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal18AllocationObserverESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L3040
	.p2align 4,,10
	.p2align 3
.L3051:
	movq	8(%r12), %rax
	cmpq	%rax, 16(%r12)
	je	.L3032
	movq	104(%r12), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, 120(%r12)
	call	*136(%rdx)
	jmp	.L3032
	.p2align 4,,10
	.p2align 3
.L3041:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3032
.L3052:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22990:
	.size	_ZN2v88internal19SpaceWithLinearArea21AddAllocationObserverEPNS0_18AllocationObserverE, .-_ZN2v88internal19SpaceWithLinearArea21AddAllocationObserverEPNS0_18AllocationObserverE
	.section	.text._ZN2v88internal5Space21AddAllocationObserverEPNS0_18AllocationObserverE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Space21AddAllocationObserverEPNS0_18AllocationObserverE
	.type	_ZN2v88internal5Space21AddAllocationObserverEPNS0_18AllocationObserverE, @function
_ZN2v88internal5Space21AddAllocationObserverEPNS0_18AllocationObserverE:
.LFB22926:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%rsi, -24(%rbp)
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	je	.L3055
	movq	-24(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%rdi)
.L3056:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*48(%rax)
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3055:
	.cfi_restore_state
	leaq	-24(%rbp), %rdx
	leaq	8(%rdi), %rdi
	call	_ZNSt6vectorIPN2v88internal18AllocationObserverESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L3056
	.cfi_endproc
.LFE22926:
	.size	_ZN2v88internal5Space21AddAllocationObserverEPNS0_18AllocationObserverE, .-_ZN2v88internal5Space21AddAllocationObserverEPNS0_18AllocationObserverE
	.section	.text._ZNSt6vectorIPN2v88internal11MemoryChunkESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal11MemoryChunkESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal11MemoryChunkESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal11MemoryChunkESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal11MemoryChunkESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB28012:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L3072
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L3068
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L3073
.L3060:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L3067:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L3074
	testq	%r13, %r13
	jg	.L3063
	testq	%r9, %r9
	jne	.L3066
.L3064:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3074:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L3063
.L3066:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L3064
	.p2align 4,,10
	.p2align 3
.L3073:
	testq	%rsi, %rsi
	jne	.L3061
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L3067
	.p2align 4,,10
	.p2align 3
.L3063:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L3064
	jmp	.L3066
	.p2align 4,,10
	.p2align 3
.L3068:
	movl	$8, %r14d
	jmp	.L3060
.L3072:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3061:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L3060
	.cfi_endproc
.LFE28012:
	.size	_ZNSt6vectorIPN2v88internal11MemoryChunkESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIPN2v88internal11MemoryChunkESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt10_HashtableIPN2v88internal11MemoryChunkES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal11MemoryChunkES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal11MemoryChunkES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_
	.type	_ZNSt10_HashtableIPN2v88internal11MemoryChunkES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_, @function
_ZNSt10_HashtableIPN2v88internal11MemoryChunkES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_:
.LFB28038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %r9
	movq	8(%rdi), %r8
	movq	(%rdi), %r13
	movq	%r9, %rax
	divq	%r8
	leaq	0(,%rdx,8), %r15
	leaq	0(%r13,%r15), %r14
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3086
	movq	%rdi, %rbx
	movq	(%r12), %rdi
	movq	%rdx, %r11
	movq	%r12, %r10
	movq	8(%rdi), %rcx
	jmp	.L3078
	.p2align 4,,10
	.p2align 3
.L3093:
	movq	(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L3086
	movq	8(%rsi), %rcx
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rcx, %rax
	divq	%r8
	cmpq	%r11, %rdx
	jne	.L3086
	movq	%rsi, %rdi
.L3078:
	cmpq	%rcx, %r9
	jne	.L3093
	movq	(%rdi), %rcx
	cmpq	%r10, %r12
	je	.L3094
	testq	%rcx, %rcx
	je	.L3080
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r11
	je	.L3080
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%rdi), %rcx
.L3080:
	movq	%rcx, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3086:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3094:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L3087
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r11
	je	.L3080
	movq	%r10, 0(%r13,%rdx,8)
	addq	(%rbx), %r15
	movq	(%r15), %rax
	movq	%r15, %r14
.L3079:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L3095
.L3081:
	movq	$0, (%r14)
	movq	(%rdi), %rcx
	jmp	.L3080
.L3087:
	movq	%r10, %rax
	jmp	.L3079
.L3095:
	movq	%rcx, 16(%rbx)
	jmp	.L3081
	.cfi_endproc
.LFE28038:
	.size	_ZNSt10_HashtableIPN2v88internal11MemoryChunkES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_, .-_ZNSt10_HashtableIPN2v88internal11MemoryChunkES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_
	.section	.text._ZN2v88internal15MemoryAllocator16UnregisterMemoryEPNS0_11MemoryChunkE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocator16UnregisterMemoryEPNS0_11MemoryChunkE
	.type	_ZN2v88internal15MemoryAllocator16UnregisterMemoryEPNS0_11MemoryChunkE, @function
_ZN2v88internal15MemoryAllocator16UnregisterMemoryEPNS0_11MemoryChunkE:
.LFB22893:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 64(%rsi)
	je	.L3097
	movq	72(%rsi), %r14
.L3098:
	lock subq	%r14, 80(%r12)
	movq	(%r12), %rax
	movq	40960(%rax), %r13
	cmpb	$0, 5952(%r13)
	je	.L3099
	movq	5944(%r13), %rax
.L3100:
	testq	%rax, %rax
	je	.L3101
	subl	%r14d, (%rax)
.L3101:
	movq	8(%rbx), %rax
	testb	$1, %al
	jne	.L3114
.L3102:
	orq	$1048576, %rax
	movq	%rax, 8(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3115
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3097:
	.cfi_restore_state
	movq	(%rsi), %r14
	jmp	.L3098
	.p2align 4,,10
	.p2align 3
.L3099:
	movb	$1, 5952(%r13)
	leaq	5928(%r13), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 5944(%r13)
	jmp	.L3100
	.p2align 4,,10
	.p2align 3
.L3114:
	lock subq	%r14, 88(%r12)
	movq	8(%rbx), %rax
	testb	$1, %al
	je	.L3102
	leaq	-48(%rbp), %rsi
	leaq	344(%r12), %rdi
	movq	%rbx, -48(%rbp)
	call	_ZNSt10_HashtableIPN2v88internal11MemoryChunkES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_
	movq	-48(%rbp), %rsi
	movq	24(%rsi), %rdi
	call	_ZN2v88internal4Heap32UnregisterUnprotectedMemoryChunkEPNS0_11MemoryChunkE@PLT
	movq	8(%rbx), %rax
	jmp	.L3102
.L3115:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22893:
	.size	_ZN2v88internal15MemoryAllocator16UnregisterMemoryEPNS0_11MemoryChunkE, .-_ZN2v88internal15MemoryAllocator16UnregisterMemoryEPNS0_11MemoryChunkE
	.section	.rodata._ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE2EEEvPNS0_11MemoryChunkE.str1.1,"aMS",@progbits,1
.LC33:
	.string	"MemoryChunk"
	.section	.text._ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE2EEEvPNS0_11MemoryChunkE,"axG",@progbits,_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE2EEEvPNS0_11MemoryChunkE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE2EEEvPNS0_11MemoryChunkE
	.type	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE2EEEvPNS0_11MemoryChunkE, @function
_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE2EEEvPNS0_11MemoryChunkE:
.LFB25879:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	41016(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L3129
.L3117:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	152(%rbx), %r13
	call	_ZN2v88internal15MemoryAllocator16UnregisterMemoryEPNS0_11MemoryChunkE
	movq	8(%r12), %rdx
	movq	(%rbx), %rax
	movq	%r12, %rsi
	shrq	$6, %rdx
	leaq	37592(%rax), %rdi
	andl	$1, %edx
	call	_ZN2v88internal4Heap20RememberUnmappedPageEmb@PLT
	movq	8(%r12), %rax
	movq	%r12, -48(%rbp)
	movq	%r13, %rdi
	movq	%rax, %rdx
	orb	$32, %dh
	movq	%rdx, 8(%r12)
	testb	$33, %al
	je	.L3130
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	224(%rbx), %rsi
	cmpq	232(%rbx), %rsi
	je	.L3122
	movq	-48(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 224(%rbx)
.L3123:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3131
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3129:
	.cfi_restore_state
	movq	%r12, %rdx
	leaq	.LC33(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger11DeleteEventEPKcPv@PLT
	jmp	.L3117
	.p2align 4,,10
	.p2align 3
.L3130:
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	200(%rbx), %rsi
	cmpq	208(%rbx), %rsi
	je	.L3119
	movq	-48(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 200(%rbx)
	jmp	.L3123
	.p2align 4,,10
	.p2align 3
.L3122:
	leaq	-48(%rbp), %rdx
	leaq	216(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal11MemoryChunkESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L3123
	.p2align 4,,10
	.p2align 3
.L3119:
	leaq	-48(%rbp), %rdx
	leaq	192(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal11MemoryChunkESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L3123
.L3131:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25879:
	.size	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE2EEEvPNS0_11MemoryChunkE, .-_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE2EEEvPNS0_11MemoryChunkE
	.section	.text._ZN2v88internal10PagedSpace11ReleasePageEPNS0_4PageE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace11ReleasePageEPNS0_4PageE
	.type	_ZN2v88internal10PagedSpace11ReleasePageEPNS0_4PageE, @function
_ZN2v88internal10PagedSpace11ReleasePageEPNS0_4PageE:
.LFB22958:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	96(%rdi), %r14
	jmp	.L3136
	.p2align 4,,10
	.p2align 3
.L3134:
	cmpq	$0, 8(%rbx)
	je	.L3135
	movl	4(%rbx), %edx
	subq	%rdx, 40(%r14)
.L3135:
	movq	$0, 8(%rbx)
	addq	$1, %r15
	movl	$0, 4(%rbx)
	movups	%xmm0, 16(%rbx)
.L3136:
	movq	(%rax), %rdx
	movq	%rax, -56(%rbp)
	movq	96(%rdx), %rdx
	cmpl	%r15d, 8(%rdx)
	jle	.L3133
	movq	240(%r13), %rdx
	movq	%r14, %rdi
	movq	(%rdx,%r15,8), %rbx
	movq	(%r14), %rdx
	movq	%rbx, %rsi
	call	*64(%rdx)
	cmpq	$0, 16(%rbx)
	movq	-56(%rbp), %rax
	pxor	%xmm0, %xmm0
	jne	.L3134
	cmpq	$0, 24(%rbx)
	jne	.L3134
	movslq	(%rbx), %rcx
	movq	32(%r14), %rdx
	cmpq	(%rdx,%rcx,8), %rbx
	jne	.L3135
	jmp	.L3134
	.p2align 4,,10
	.p2align 3
.L3133:
	movq	104(%r12), %rax
	subq	$8, %rax
	andq	$-262144, %rax
	cmpq	%rax, %r13
	je	.L3139
.L3137:
	movq	0(%r13), %rax
	subq	%rax, 80(%r12)
	movq	48(%r13), %rax
	subq	40(%r13), %rax
	lock subq	%rax, 168(%r12)
	movq	64(%r12), %rax
	movq	2048(%rax), %rdi
	addq	$24, %rsp
	movq	%r13, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE2EEEvPNS0_11MemoryChunkE
	.p2align 4,,10
	.p2align 3
.L3139:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movups	%xmm0, 104(%r12)
	jmp	.L3137
	.cfi_endproc
.LFE22958:
	.size	_ZN2v88internal10PagedSpace11ReleasePageEPNS0_4PageE, .-_ZN2v88internal10PagedSpace11ReleasePageEPNS0_4PageE
	.section	.text._ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE3EEEvPNS0_11MemoryChunkE,"axG",@progbits,_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE3EEEvPNS0_11MemoryChunkE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE3EEEvPNS0_11MemoryChunkE
	.type	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE3EEEvPNS0_11MemoryChunkE, @function
_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE3EEEvPNS0_11MemoryChunkE:
.LFB25880:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	orq	$16384, 8(%rsi)
	movq	(%rdi), %rax
	movq	41016(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L3153
.L3141:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	152(%r12), %r13
	call	_ZN2v88internal15MemoryAllocator16UnregisterMemoryEPNS0_11MemoryChunkE
	movq	8(%rbx), %rdx
	movq	(%r12), %rax
	movq	%rbx, %rsi
	shrq	$6, %rdx
	leaq	37592(%rax), %rdi
	andl	$1, %edx
	call	_ZN2v88internal4Heap20RememberUnmappedPageEmb@PLT
	movq	8(%rbx), %rax
	movq	%rbx, -48(%rbp)
	movq	%r13, %rdi
	movq	%rax, %rdx
	orb	$32, %dh
	movq	%rdx, 8(%rbx)
	testb	$33, %al
	je	.L3154
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	224(%r12), %rsi
	cmpq	232(%r12), %rsi
	je	.L3146
	movq	-48(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 224(%r12)
.L3147:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3155
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3153:
	.cfi_restore_state
	movq	%rbx, %rdx
	leaq	.LC33(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger11DeleteEventEPKcPv@PLT
	jmp	.L3141
	.p2align 4,,10
	.p2align 3
.L3154:
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	200(%r12), %rsi
	cmpq	208(%r12), %rsi
	je	.L3143
	movq	-48(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 200(%r12)
	jmp	.L3147
	.p2align 4,,10
	.p2align 3
.L3146:
	leaq	-48(%rbp), %rdx
	leaq	216(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal11MemoryChunkESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L3147
	.p2align 4,,10
	.p2align 3
.L3143:
	leaq	-48(%rbp), %rdx
	leaq	192(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal11MemoryChunkESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L3147
.L3155:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25880:
	.size	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE3EEEvPNS0_11MemoryChunkE, .-_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE3EEEvPNS0_11MemoryChunkE
	.section	.text._ZN2v88internal9SemiSpace11RewindPagesEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9SemiSpace11RewindPagesEi
	.type	_ZN2v88internal9SemiSpace11RewindPagesEi, @function
_ZN2v88internal9SemiSpace11RewindPagesEi:
.LFB23002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testl	%esi, %esi
	jg	.L3162
	jmp	.L3156
	.p2align 4,,10
	.p2align 3
.L3158:
	movq	224(%rsi), %rdx
	movq	232(%rsi), %rax
	testq	%rdx, %rdx
	je	.L3159
	movq	%rax, 232(%rdx)
.L3159:
	testq	%rax, %rax
	je	.L3160
	pxor	%xmm0, %xmm0
	movq	%rdx, 224(%rax)
	movups	%xmm0, 224(%rsi)
.L3174:
	movq	64(%rbx), %rax
	movq	2048(%rax), %rdi
	call	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE3EEEvPNS0_11MemoryChunkE
	subl	$1, %r12d
	je	.L3156
.L3162:
	movq	40(%rbx), %rsi
	movq	232(%rsi), %rax
	movq	%rax, 40(%rbx)
	cmpq	%rsi, 32(%rbx)
	jne	.L3158
	movq	224(%rsi), %rax
	movq	%rax, 32(%rbx)
	jmp	.L3158
	.p2align 4,,10
	.p2align 3
.L3160:
	pxor	%xmm1, %xmm1
	movups	%xmm1, 224(%rsi)
	jmp	.L3174
	.p2align 4,,10
	.p2align 3
.L3156:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23002:
	.size	_ZN2v88internal9SemiSpace11RewindPagesEi, .-_ZN2v88internal9SemiSpace11RewindPagesEi
	.section	.text._ZN2v88internal15MemoryAllocator13PreFreeMemoryEPNS0_11MemoryChunkE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocator13PreFreeMemoryEPNS0_11MemoryChunkE
	.type	_ZN2v88internal15MemoryAllocator13PreFreeMemoryEPNS0_11MemoryChunkE, @function
_ZN2v88internal15MemoryAllocator13PreFreeMemoryEPNS0_11MemoryChunkE:
.LFB22894:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	41016(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L3197
.L3176:
	cmpq	$0, 64(%rbx)
	je	.L3177
	movq	72(%rbx), %r14
.L3178:
	lock subq	%r14, 80(%r12)
	movq	(%r12), %rax
	movq	40960(%rax), %r13
	cmpb	$0, 5952(%r13)
	je	.L3179
	movq	5944(%r13), %rax
.L3180:
	testq	%rax, %rax
	je	.L3181
	subl	%r14d, (%rax)
.L3181:
	movq	8(%rbx), %rdx
	testb	$1, %dl
	jne	.L3198
.L3182:
	movq	%rdx, %rax
	shrq	$6, %rdx
	movq	%rbx, %rsi
	orq	$1048576, %rax
	andl	$1, %edx
	movq	%rax, 8(%rbx)
	movq	(%r12), %rdi
	addq	$37592, %rdi
	call	_ZN2v88internal4Heap20RememberUnmappedPageEmb@PLT
	orq	$8192, 8(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3199
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3177:
	.cfi_restore_state
	movq	(%rbx), %r14
	jmp	.L3178
	.p2align 4,,10
	.p2align 3
.L3179:
	movb	$1, 5952(%r13)
	leaq	5928(%r13), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 5944(%r13)
	jmp	.L3180
	.p2align 4,,10
	.p2align 3
.L3197:
	movq	%rbx, %rdx
	leaq	.LC33(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger11DeleteEventEPKcPv@PLT
	jmp	.L3176
	.p2align 4,,10
	.p2align 3
.L3198:
	lock subq	%r14, 88(%r12)
	movq	8(%rbx), %rdx
	testb	$1, %dl
	je	.L3182
	leaq	-48(%rbp), %rsi
	leaq	344(%r12), %rdi
	movq	%rbx, -48(%rbp)
	call	_ZNSt10_HashtableIPN2v88internal11MemoryChunkES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_
	movq	-48(%rbp), %rsi
	movq	24(%rsi), %rdi
	call	_ZN2v88internal4Heap32UnregisterUnprotectedMemoryChunkEPNS0_11MemoryChunkE@PLT
	movq	8(%rbx), %rdx
	jmp	.L3182
.L3199:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22894:
	.size	_ZN2v88internal15MemoryAllocator13PreFreeMemoryEPNS0_11MemoryChunkE, .-_ZN2v88internal15MemoryAllocator13PreFreeMemoryEPNS0_11MemoryChunkE
	.section	.rodata._ZNSt5dequeIPjSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC34:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIPjSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIPjSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPjSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_
	.type	_ZNSt5dequeIPjSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_, @function
_ZNSt5dequeIPjSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_:
.LFB28142:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	72(%rdi), %r14
	movq	40(%rdi), %rsi
	movq	48(%rbx), %rdx
	subq	56(%rbx), %rdx
	movq	%r14, %r13
	movq	%rdx, %rcx
	subq	%rsi, %r13
	sarq	$3, %rcx
	movq	%r13, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	32(%rbx), %rax
	subq	16(%rbx), %rax
	movabsq	$1152921504606846975, %rcx
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	je	.L3209
	movq	(%rbx), %r8
	movq	8(%rbx), %rdx
	movq	%r14, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L3210
.L3202:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r14)
	movq	(%r12), %rdx
	movq	48(%rbx), %rax
	movq	%rdx, (%rax)
	movq	72(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 72(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 56(%rbx)
	movq	%rdx, 64(%rbx)
	movq	%rax, 48(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3210:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L3211
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	cmpq	%rcx, %r14
	ja	.L3212
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	%rax, %rsi
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rsi,%rax,8), %r15
	movq	72(%rbx), %rax
	movq	40(%rbx), %rsi
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L3207
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L3207:
	movq	(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 8(%rbx)
	movq	%rax, (%rbx)
.L3205:
	movq	%r15, 40(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r13), %r14
	movq	(%r15), %xmm0
	movq	%r14, 72(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	(%r14), %rax
	movq	%rax, 56(%rbx)
	addq	$512, %rax
	movq	%rax, 64(%rbx)
	jmp	.L3202
	.p2align 4,,10
	.p2align 3
.L3211:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L3204
	cmpq	%r14, %rsi
	je	.L3205
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L3205
	.p2align 4,,10
	.p2align 3
.L3204:
	cmpq	%r14, %rsi
	je	.L3205
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L3205
.L3209:
	leaq	.LC34(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3212:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE28142:
	.size	_ZNSt5dequeIPjSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_, .-_ZNSt5dequeIPjSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_
	.section	.rodata._ZN2v88internal7SlotSet11RemoveRangeEiiNS1_15EmptyBucketModeE.str1.8,"aMS",@progbits,1
	.align 8
.LC35:
	.string	"end_offset <= 1 << kPageSizeBits"
	.section	.text._ZN2v88internal7SlotSet11RemoveRangeEiiNS1_15EmptyBucketModeE,"axG",@progbits,_ZN2v88internal7SlotSet11RemoveRangeEiiNS1_15EmptyBucketModeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7SlotSet11RemoveRangeEiiNS1_15EmptyBucketModeE
	.type	_ZN2v88internal7SlotSet11RemoveRangeEiiNS1_15EmptyBucketModeE, @function
_ZN2v88internal7SlotSet11RemoveRangeEiiNS1_15EmptyBucketModeE:
.LFB9672:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$262144, %edx
	jg	.L3272
	movl	%esi, %eax
	movq	%rdi, %r14
	movl	%esi, %edi
	sarl	$3, %esi
	sarl	$8, %eax
	movl	%edx, %r9d
	movl	%ecx, %r12d
	movl	%esi, %ecx
	sarl	$13, %edi
	andl	$31, %eax
	sarl	$13, %r9d
	movl	%eax, %r8d
	movl	%edx, %eax
	sarl	$3, %edx
	movl	%r9d, -68(%rbp)
	sarl	$8, %eax
	andl	$31, %eax
	movl	%eax, -72(%rbp)
	movl	%eax, %r10d
	movl	$1, %eax
	movl	%eax, %ebx
	sall	%cl, %ebx
	movl	%edx, %ecx
	sall	%cl, %eax
	movl	%ebx, %esi
	leal	-1(%rbx), %r11d
	movl	%eax, %r13d
	movl	%eax, -76(%rbp)
	movslq	%edi, %rax
	movq	%rax, -96(%rbp)
	salq	$3, %rax
	negl	%r13d
	movq	%rax, -88(%rbp)
	leaq	(%r14,%rax), %rbx
	cmpl	%edi, %r9d
	jne	.L3215
	cmpl	%r8d, %r10d
	je	.L3273
.L3215:
	movq	(%rbx), %r9
	leal	1(%r8), %r15d
	testq	%r9, %r9
	jne	.L3274
	cmpl	%edi, -68(%rbp)
	jg	.L3225
.L3237:
	movq	(%rbx), %rcx
	cmpl	$32, %edi
	je	.L3213
	testq	%rcx, %rcx
	je	.L3213
	cmpl	%r15d, -72(%rbp)
	jle	.L3240
	movl	-72(%rbp), %ebx
	movslq	%r15d, %rsi
	leaq	(%rcx,%rsi,4), %rax
	leal	-1(%rbx), %edx
	subl	%r15d, %edx
	addq	%rsi, %rdx
	leaq	4(%rcx,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L3241:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L3241
.L3240:
	movl	-76(%rbp), %edi
	movslq	-72(%rbp), %r12
	subl	$1, %edi
	leaq	(%rcx,%r12,4), %rcx
	jmp	.L3239
	.p2align 4,,10
	.p2align 3
.L3275:
	movl	%r13d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L3213
.L3239:
	movl	(%rcx), %edx
	testl	%edx, %edi
	jne	.L3275
.L3213:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3276
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3274:
	.cfi_restore_state
	movslq	%r8d, %rax
	negl	%esi
	movq	%rax, -104(%rbp)
	salq	$2, %rax
	movq	%rax, -112(%rbp)
	leaq	(%r9,%rax), %rcx
	jmp	.L3221
	.p2align 4,,10
	.p2align 3
.L3277:
	movl	%r11d, %r10d
	movl	%edx, %eax
	andl	%edx, %r10d
	lock cmpxchgl	%r10d, (%rcx)
	cmpl	%eax, %edx
	je	.L3220
.L3221:
	movl	(%rcx), %edx
	testl	%edx, %esi
	jne	.L3277
.L3220:
	cmpl	%edi, -68(%rbp)
	jle	.L3237
	cmpl	$32, %r15d
	jne	.L3222
	.p2align 4,,10
	.p2align 3
.L3225:
	leal	1(%rdi), %eax
	cmpl	-68(%rbp), %eax
	jge	.L3278
	movl	-68(%rbp), %edx
	movq	-88(%rbp), %rax
	movl	%r13d, -88(%rbp)
	movl	%r12d, %r13d
	subl	%edi, %edx
	leaq	8(%r14,%rax), %r15
	leaq	264(%r14), %rax
	subl	$2, %edx
	addq	-96(%rbp), %rdx
	movq	%rax, %r12
	leaq	16(%r14,%rdx,8), %rbx
	jmp	.L3236
	.p2align 4,,10
	.p2align 3
.L3281:
	movq	(%r15), %rdi
	movq	$0, (%r15)
	testq	%rdi, %rdi
	je	.L3231
	call	_ZdaPv@PLT
.L3231:
	addq	$8, %r15
	cmpq	%rbx, %r15
	je	.L3279
.L3236:
	cmpl	$1, %r13d
	je	.L3280
	testl	%r13d, %r13d
	je	.L3281
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.L3231
	leaq	128(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L3235:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rcx
	jne	.L3235
	addq	$8, %r15
	cmpq	%rbx, %r15
	jne	.L3236
	.p2align 4,,10
	.p2align 3
.L3279:
	movslq	-68(%rbp), %rax
	movl	-88(%rbp), %r13d
	xorl	%r15d, %r15d
	movq	%rax, %rdi
	leaq	(%r14,%rax,8), %rbx
	jmp	.L3237
	.p2align 4,,10
	.p2align 3
.L3280:
	movq	(%r15), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L3231
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	368(%r14), %rax
	movq	352(%r14), %rcx
	subq	$8, %rax
	cmpq	%rax, %rcx
	je	.L3229
	movq	-64(%rbp), %rax
	movq	%rax, (%rcx)
	addq	$8, 352(%r14)
.L3230:
	movq	%r12, %rdi
	movq	$0, (%r15)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L3231
	.p2align 4,,10
	.p2align 3
.L3222:
	movq	-112(%rbp), %rax
	movl	$30, %edx
	subl	%r8d, %edx
	addq	-104(%rbp), %rdx
	leaq	4(%rax,%r9), %rax
	leaq	8(%r9,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L3226:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rdx, %rax
	jne	.L3226
	jmp	.L3225
.L3273:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L3213
	orl	%r11d, %r13d
	movslq	%r8d, %rdx
	movl	%r13d, %edi
	leaq	(%rax,%rdx,4), %rcx
	notl	%edi
	jmp	.L3218
	.p2align 4,,10
	.p2align 3
.L3282:
	movl	%r13d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L3213
.L3218:
	movl	(%rcx), %edx
	testl	%edx, %edi
	jne	.L3282
	jmp	.L3213
	.p2align 4,,10
	.p2align 3
.L3229:
	leaq	-64(%rbp), %rsi
	leaq	304(%r14), %rdi
	call	_ZNSt5dequeIPjSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_
	jmp	.L3230
	.p2align 4,,10
	.p2align 3
.L3272:
	leaq	.LC35(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L3278:
	movq	-88(%rbp), %rbx
	movl	%eax, %edi
	xorl	%r15d, %r15d
	leaq	8(%r14,%rbx), %rbx
	jmp	.L3237
.L3276:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9672:
	.size	_ZN2v88internal7SlotSet11RemoveRangeEiiNS1_15EmptyBucketModeE, .-_ZN2v88internal7SlotSet11RemoveRangeEiiNS1_15EmptyBucketModeE
	.section	.text._ZNSt10_HashtableImSt4pairIKmSt6vectorImSaImEEESaIS5_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableImSt4pairIKmSt6vectorImSaImEEESaIS5_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableImSt4pairIKmSt6vectorImSaImEEESaIS5_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm
	.type	_ZNSt10_HashtableImSt4pairIKmSt6vectorImSaImEEESaIS5_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm, @function
_ZNSt10_HashtableImSt4pairIKmSt6vectorImSaImEEESaIS5_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm:
.LFB28750:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L3284
	movq	(%rbx), %r8
.L3285:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L3294
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L3295:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3284:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L3308
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L3309
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L3287:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L3289
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L3290
	.p2align 4,,10
	.p2align 3
.L3291:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L3292:
	testq	%rsi, %rsi
	je	.L3289
.L3290:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L3291
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L3297
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L3290
	.p2align 4,,10
	.p2align 3
.L3289:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r10
	je	.L3293
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L3293:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L3285
	.p2align 4,,10
	.p2align 3
.L3294:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L3296
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L3296:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L3295
	.p2align 4,,10
	.p2align 3
.L3297:
	movq	%rdx, %rdi
	jmp	.L3292
	.p2align 4,,10
	.p2align 3
.L3308:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L3287
.L3309:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE28750:
	.size	_ZNSt10_HashtableImSt4pairIKmSt6vectorImSaImEEESaIS5_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm, .-_ZNSt10_HashtableImSt4pairIKmSt6vectorImSaImEEESaIS5_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm
	.section	.text._ZN2v88internal20CodeRangeAddressHint20NotifyFreedCodeRangeEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20CodeRangeAddressHint20NotifyFreedCodeRangeEmm
	.type	_ZN2v88internal20CodeRangeAddressHint20NotifyFreedCodeRangeEmm, @function
_ZN2v88internal20CodeRangeAddressHint20NotifyFreedCodeRangeEmm:
.LFB22754:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%rsi, -40(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	48(%r12), %rcx
	movq	%r13, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	40(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L3311
	movq	(%rax), %rdi
	movq	8(%rdi), %rsi
	jmp	.L3313
	.p2align 4,,10
	.p2align 3
.L3326:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3311
	movq	8(%rdi), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r14
	jne	.L3311
.L3313:
	cmpq	%rsi, %r13
	jne	.L3326
	addq	$16, %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L3314
.L3327:
	movq	-40(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 8(%rdi)
.L3315:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3311:
	.cfi_restore_state
	movl	$40, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	$0, (%rax)
	leaq	40(%r12), %rdi
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	%r13, 8(%rax)
	movq	$0, 32(%rax)
	movups	%xmm0, 16(%rax)
	call	_ZNSt10_HashtableImSt4pairIKmSt6vectorImSaImEEESaIS5_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm
	leaq	16(%rax), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	jne	.L3327
	.p2align 4,,10
	.p2align 3
.L3314:
	leaq	-40(%rbp), %rdx
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L3315
	.cfi_endproc
.LFE22754:
	.size	_ZN2v88internal20CodeRangeAddressHint20NotifyFreedCodeRangeEmm, .-_ZN2v88internal20CodeRangeAddressHint20NotifyFreedCodeRangeEmm
	.section	.text._ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB28869:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L3336
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L3330:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L3330
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3336:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE28869:
	.size	_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.text._ZN2v88internal11MemoryChunk23ReleaseInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEvv,"axG",@progbits,_ZN2v88internal11MemoryChunk23ReleaseInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEvv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11MemoryChunk23ReleaseInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEvv
	.type	_ZN2v88internal11MemoryChunk23ReleaseInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEvv, @function
_ZN2v88internal11MemoryChunk23ReleaseInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEvv:
.LFB25888:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	136(%rdi), %r13
	testq	%r13, %r13
	je	.L3339
	movq	16(%r13), %rbx
	movq	%rdi, %r12
	testq	%rbx, %rbx
	je	.L3341
.L3342:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L3342
.L3341:
	movl	$48, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	movq	$0, 136(%r12)
.L3339:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25888:
	.size	_ZN2v88internal11MemoryChunk23ReleaseInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEvv, .-_ZN2v88internal11MemoryChunk23ReleaseInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEvv
	.section	.text._ZN2v88internal11MemoryChunk23ReleaseInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEvv,"axG",@progbits,_ZN2v88internal11MemoryChunk23ReleaseInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEvv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11MemoryChunk23ReleaseInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEvv
	.type	_ZN2v88internal11MemoryChunk23ReleaseInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEvv, @function
_ZN2v88internal11MemoryChunk23ReleaseInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEvv:
.LFB25892:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	144(%rdi), %r13
	testq	%r13, %r13
	je	.L3351
	movq	16(%r13), %rbx
	movq	%rdi, %r12
	testq	%rbx, %rbx
	je	.L3353
.L3354:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L3354
.L3353:
	movl	$48, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	movq	$0, 144(%r12)
.L3351:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25892:
	.size	_ZN2v88internal11MemoryChunk23ReleaseInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEvv, .-_ZN2v88internal11MemoryChunk23ReleaseInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEvv
	.section	.text._ZN2v88internal11MemoryChunk44ReleaseAllocatedMemoryNeededForWritableChunkEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11MemoryChunk44ReleaseAllocatedMemoryNeededForWritableChunkEv
	.type	_ZN2v88internal11MemoryChunk44ReleaseAllocatedMemoryNeededForWritableChunkEv, @function
_ZN2v88internal11MemoryChunk44ReleaseAllocatedMemoryNeededForWritableChunkEv:
.LFB22904:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	160(%rdi), %r12
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L3364
	movq	%r12, %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	$0, 160(%rbx)
.L3364:
	movq	176(%rbx), %r12
	testq	%r12, %r12
	je	.L3365
	movq	%r12, %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	$0, 176(%rbx)
.L3365:
	movq	272(%rbx), %r13
	testq	%r13, %r13
	je	.L3366
	movq	40(%r13), %r12
	leaq	24(%r13), %r14
	testq	%r12, %r12
	je	.L3367
.L3368:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeImmSt9_IdentityImESt4lessImESaImEE8_M_eraseEPSt13_Rb_tree_nodeImE
	movq	%r12, %rdi
	movq	16(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L3368
.L3367:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3369
	call	_ZdlPv@PLT
.L3369:
	movl	$72, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	movq	$0, 272(%rbx)
.L3366:
	movq	%rbx, %rdi
	call	_ZN2v88internal11MemoryChunk14ReleaseSlotSetILNS0_17RememberedSetTypeE0EEEvv
	movq	%rbx, %rdi
	call	_ZN2v88internal11MemoryChunk14ReleaseSlotSetILNS0_17RememberedSetTypeE1EEEvv
	movq	%rbx, %rdi
	call	_ZN2v88internal11MemoryChunk19ReleaseTypedSlotSetILNS0_17RememberedSetTypeE0EEEvv
	movq	%rbx, %rdi
	call	_ZN2v88internal11MemoryChunk19ReleaseTypedSlotSetILNS0_17RememberedSetTypeE1EEEvv
	movq	%rbx, %rdi
	call	_ZN2v88internal11MemoryChunk23ReleaseInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEvv
	movq	%rbx, %rdi
	call	_ZN2v88internal11MemoryChunk23ReleaseInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEvv
	movq	248(%rbx), %r12
	testq	%r12, %r12
	je	.L3370
	movq	%r12, %rdi
	call	_ZN2v88internal23LocalArrayBufferTrackerD1Ev@PLT
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	$0, 248(%rbx)
.L3370:
	movq	264(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3363
	call	free@PLT
	movq	$0, 264(%rbx)
.L3363:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22904:
	.size	_ZN2v88internal11MemoryChunk44ReleaseAllocatedMemoryNeededForWritableChunkEv, .-_ZN2v88internal11MemoryChunk44ReleaseAllocatedMemoryNeededForWritableChunkEv
	.section	.text._ZN2v88internal11MemoryChunk25ReleaseAllAllocatedMemoryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11MemoryChunk25ReleaseAllAllocatedMemoryEv
	.type	_ZN2v88internal11MemoryChunk25ReleaseAllAllocatedMemoryEv, @function
_ZN2v88internal11MemoryChunk25ReleaseAllAllocatedMemoryEv:
.LFB22908:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %rbx
	andl	$32, %ebx
	jne	.L3396
	cmpq	$0, 240(%rdi)
	je	.L3396
	leaq	80(%rdi), %r13
	jmp	.L3399
	.p2align 4,,10
	.p2align 3
.L3409:
	movq	240(%r12), %rax
	movq	(%rax,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L3398
	movl	$32, %esi
	call	_ZdlPvm@PLT
	movq	240(%r12), %rax
	movq	$0, (%rax,%rbx,8)
.L3398:
	addq	$1, %rbx
.L3399:
	movq	0(%r13), %rax
	movq	96(%rax), %rax
	cmpl	%ebx, 12(%rax)
	jge	.L3409
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3400
	call	_ZdaPv@PLT
.L3400:
	movq	$0, 240(%r12)
.L3396:
	movq	%r12, %rdi
	call	_ZN2v88internal11MemoryChunk44ReleaseAllocatedMemoryNeededForWritableChunkEv
	cmpq	$0, 16(%r12)
	je	.L3395
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal16BasicMemoryChunk20ReleaseMarkingBitmapEv@PLT
	.p2align 4,,10
	.p2align 3
.L3395:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22908:
	.size	_ZN2v88internal11MemoryChunk25ReleaseAllAllocatedMemoryEv, .-_ZN2v88internal11MemoryChunk25ReleaseAllAllocatedMemoryEv
	.section	.text._ZN2v88internal15MemoryAllocator18AllocatePagePooledINS0_9SemiSpaceEEEPNS0_11MemoryChunkEPT_,"axG",@progbits,_ZN2v88internal15MemoryAllocator18AllocatePagePooledINS0_9SemiSpaceEEEPNS0_11MemoryChunkEPT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15MemoryAllocator18AllocatePagePooledINS0_9SemiSpaceEEEPNS0_11MemoryChunkEPT_
	.type	_ZN2v88internal15MemoryAllocator18AllocatePagePooledINS0_9SemiSpaceEEEPNS0_11MemoryChunkEPT_, @function
_ZN2v88internal15MemoryAllocator18AllocatePagePooledINS0_9SemiSpaceEEEPNS0_11MemoryChunkEPT_:
.LFB27674:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	152(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	248(%rbx), %rax
	cmpq	240(%rbx), %rax
	je	.L3434
	movq	-8(%rax), %r12
	subq	$8, %rax
	movq	%r14, %rdi
	movq	%rax, 248(%rbx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	testq	%r12, %r12
	je	.L3412
.L3413:
	cmpl	$3, 72(%r13)
	movl	$280, %r14d
	je	.L3435
.L3421:
	movq	32(%rbx), %rax
	leaq	-112(%rbp), %r15
	movq	%rbx, %rdi
	movq	%r12, -104(%rbp)
	movq	%r15, %rsi
	movq	$262144, -96(%rbp)
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal15MemoryAllocator12CommitMemoryEPNS0_13VirtualMemoryE
	testb	%al, %al
	jne	.L3436
	xorl	%r12d, %r12d
.L3419:
	movq	%r15, %rdi
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
.L3410:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3437
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3436:
	.cfi_restore_state
	movdqu	-104(%rbp), %xmm0
	movq	-112(%rbp), %rax
	movq	%r15, %rdi
	movups	%xmm0, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal13VirtualMemory5ResetEv@PLT
	leaq	(%r12,%r14), %rcx
	leaq	-80(%rbp), %r14
	movq	(%rbx), %rax
	pushq	%r14
	xorl	%r9d, %r9d
	leaq	262144(%r12), %r8
	movq	%r12, %rsi
	pushq	%r13
	movl	$262144, %edx
	leaq	37592(%rax), %rdi
	call	_ZN2v88internal11MemoryChunk10InitializeEPNS0_4HeapEmmmmNS0_13ExecutabilityEPNS0_5SpaceENS0_13VirtualMemoryE
	movq	%r14, %rdi
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
	lock addq	$262144, 80(%rbx)
	popq	%rax
	popq	%rdx
	jmp	.L3419
	.p2align 4,,10
	.p2align 3
.L3435:
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %r14d
	testl	%r14d, %r14d
	je	.L3415
	sall	$10, %r14d
	movslq	%r14d, %rax
	movq	%rax, %r14
	leaq	279(%rax), %rcx
	negq	%r14
	andq	%rcx, %r14
.L3418:
	addq	%rax, %r14
	jmp	.L3421
	.p2align 4,,10
	.p2align 3
.L3415:
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	leaq	279(%rax), %r14
	negq	%rax
	andq	%rax, %r14
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	jne	.L3438
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	jmp	.L3418
	.p2align 4,,10
	.p2align 3
.L3434:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L3412:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	200(%rbx), %rax
	cmpq	192(%rbx), %rax
	je	.L3439
	movq	-8(%rax), %r12
	subq	$8, %rax
	movq	%r14, %rdi
	movq	%rax, 200(%rbx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	testq	%r12, %r12
	je	.L3410
	movq	%r12, %rdi
	call	_ZN2v88internal11MemoryChunk25ReleaseAllAllocatedMemoryEv
	jmp	.L3413
.L3439:
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L3410
.L3437:
	call	__stack_chk_fail@PLT
.L3438:
	sall	$10, %eax
	cltq
	jmp	.L3418
	.cfi_endproc
.LFE27674:
	.size	_ZN2v88internal15MemoryAllocator18AllocatePagePooledINS0_9SemiSpaceEEEPNS0_11MemoryChunkEPT_, .-_ZN2v88internal15MemoryAllocator18AllocatePagePooledINS0_9SemiSpaceEEEPNS0_11MemoryChunkEPT_
	.section	.text._ZN2v88internal12ReadOnlyPage21MakeHeaderRelocatableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12ReadOnlyPage21MakeHeaderRelocatableEv
	.type	_ZN2v88internal12ReadOnlyPage21MakeHeaderRelocatableEv, @function
_ZN2v88internal12ReadOnlyPage21MakeHeaderRelocatableEv:
.LFB23121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal11MemoryChunk44ReleaseAllocatedMemoryNeededForWritableChunkEv
	leaq	80(%rbx), %rcx
	xorl	%eax, %eax
	jmp	.L3442
	.p2align 4,,10
	.p2align 3
.L3444:
	addl	$1, %eax
.L3442:
	movq	(%rcx), %rdx
	movq	96(%rdx), %rdx
	cmpl	8(%rdx), %eax
	jl	.L3444
	movq	$0, 24(%rbx)
	movq	$0, 80(%rbx)
	mfence
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23121:
	.size	_ZN2v88internal12ReadOnlyPage21MakeHeaderRelocatableEv, .-_ZN2v88internal12ReadOnlyPage21MakeHeaderRelocatableEv
	.section	.text._ZN2v88internal13ReadOnlySpace4SealENS1_8SealModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ReadOnlySpace4SealENS1_8SealModeE
	.type	_ZN2v88internal13ReadOnlySpace4SealENS1_8SealModeE, @function
_ZN2v88internal13ReadOnlySpace4SealENS1_8SealModeE:
.LFB23125:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	call	_ZN2v88internal10PagedSpace24FreeLinearAllocationAreaEv
	movq	64(%r13), %rax
	movb	$1, 232(%r13)
	movq	2048(%rax), %r12
	testl	%ebx, %ebx
	jne	.L3452
	movq	32(%r13), %rbx
	movq	$0, 64(%r13)
	testq	%rbx, %rbx
	je	.L3445
	.p2align 4,,10
	.p2align 3
.L3449:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15MemoryAllocator16UnregisterMemoryEPNS0_11MemoryChunkE
	movq	%rbx, %rdi
	call	_ZN2v88internal11MemoryChunk44ReleaseAllocatedMemoryNeededForWritableChunkEv
	leaq	80(%rbx), %rdx
	xorl	%eax, %eax
	jmp	.L3451
	.p2align 4,,10
	.p2align 3
.L3463:
	addl	$1, %eax
.L3451:
	movq	(%rdx), %rcx
	movq	96(%rcx), %rcx
	cmpl	%eax, 8(%rcx)
	jg	.L3463
	movq	$0, 24(%rbx)
	movq	$0, (%rdx)
	mfence
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L3449
.L3452:
	movq	32(%r13), %rbx
	testq	%rbx, %rbx
	jne	.L3447
	jmp	.L3445
	.p2align 4,,10
	.p2align 3
.L3454:
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L3445
.L3447:
	movq	32(%r12), %rdi
	testb	$1, 8(%rbx)
	je	.L3453
	movq	40(%r12), %rdi
.L3453:
	movq	(%rbx), %rdx
	movl	$1, %ecx
	movq	%rbx, %rsi
	call	_ZN2v88internal14SetPermissionsEPNS_13PageAllocatorEPvmNS1_10PermissionE@PLT
	testb	%al, %al
	jne	.L3454
	leaq	.LC30(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3445:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23125:
	.size	_ZN2v88internal13ReadOnlySpace4SealENS1_8SealModeE, .-_ZN2v88internal13ReadOnlySpace4SealENS1_8SealModeE
	.section	.text._ZN2v88internal15MemoryAllocator17PerformFreeMemoryEPNS0_11MemoryChunkE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocator17PerformFreeMemoryEPNS0_11MemoryChunkE
	.type	_ZN2v88internal15MemoryAllocator17PerformFreeMemoryEPNS0_11MemoryChunkE, @function
_ZN2v88internal15MemoryAllocator17PerformFreeMemoryEPNS0_11MemoryChunkE:
.LFB22895:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rsi), %rbx
	andl	$32, %ebx
	jne	.L3465
	cmpq	$0, 240(%rsi)
	je	.L3465
	leaq	80(%rsi), %r14
	jmp	.L3468
	.p2align 4,,10
	.p2align 3
.L3494:
	movq	240(%r12), %rax
	movq	(%rax,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L3467
	movl	$32, %esi
	call	_ZdlPvm@PLT
	movq	240(%r12), %rax
	movq	$0, (%rax,%rbx,8)
.L3467:
	addq	$1, %rbx
.L3468:
	movq	(%r14), %rax
	movq	96(%rax), %rax
	cmpl	%ebx, 12(%rax)
	jge	.L3494
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3469
	call	_ZdaPv@PLT
.L3469:
	movq	$0, 240(%r12)
.L3465:
	movq	%r12, %rdi
	call	_ZN2v88internal11MemoryChunk44ReleaseAllocatedMemoryNeededForWritableChunkEv
	cmpq	$0, 16(%r12)
	je	.L3470
	movq	%r12, %rdi
	call	_ZN2v88internal16BasicMemoryChunk20ReleaseMarkingBitmapEv@PLT
.L3470:
	movq	8(%r12), %rax
	movq	64(%r12), %rsi
	leaq	56(%r12), %rdi
	testb	$64, %ah
	jne	.L3495
	testq	%rsi, %rsi
	jne	.L3496
	movq	(%r12), %rdx
	movq	32(%r13), %rdi
	testb	$1, %al
	je	.L3477
	movq	40(%r13), %rdi
.L3477:
	movq	%r12, %rsi
	call	_ZN2v88internal9FreePagesEPNS_13PageAllocatorEPvm@PLT
	testb	%al, %al
	je	.L3497
.L3464:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3496:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13VirtualMemory4FreeEv@PLT
	.p2align 4,,10
	.p2align 3
.L3495:
	.cfi_restore_state
	movq	72(%r12), %rbx
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	call	_ZN2v88internal13VirtualMemory14SetPermissionsEmmNS_13PageAllocator10PermissionE@PLT
	testb	%al, %al
	je	.L3464
	movq	0(%r13), %rax
	movq	40960(%rax), %r12
	cmpb	$0, 5952(%r12)
	je	.L3473
	movq	5944(%r12), %rax
.L3474:
	testq	%rax, %rax
	je	.L3464
	subl	%ebx, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3473:
	.cfi_restore_state
	movb	$1, 5952(%r12)
	leaq	5928(%r12), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 5944(%r12)
	jmp	.L3474
	.p2align 4,,10
	.p2align 3
.L3497:
	leaq	.LC10(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22895:
	.size	_ZN2v88internal15MemoryAllocator17PerformFreeMemoryEPNS0_11MemoryChunkE, .-_ZN2v88internal15MemoryAllocator17PerformFreeMemoryEPNS0_11MemoryChunkE
	.section	.text._ZN2v88internal15MemoryAllocator8Unmapper41PerformFreeMemoryOnQueuedNonRegularChunksEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocator8Unmapper41PerformFreeMemoryOnQueuedNonRegularChunksEv
	.type	_ZN2v88internal15MemoryAllocator8Unmapper41PerformFreeMemoryOnQueuedNonRegularChunksEv, @function
_ZN2v88internal15MemoryAllocator8Unmapper41PerformFreeMemoryOnQueuedNonRegularChunksEv:
.LFB22807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	16(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	jmp	.L3501
	.p2align 4,,10
	.p2align 3
.L3499:
	movq	-8(%rax), %r12
	subq	$8, %rax
	movq	%r13, %rdi
	movq	%rax, 88(%rbx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	testq	%r12, %r12
	je	.L3498
	movq	8(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal15MemoryAllocator17PerformFreeMemoryEPNS0_11MemoryChunkE
.L3501:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	88(%rbx), %rax
	cmpq	80(%rbx), %rax
	jne	.L3499
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L3498:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22807:
	.size	_ZN2v88internal15MemoryAllocator8Unmapper41PerformFreeMemoryOnQueuedNonRegularChunksEv, .-_ZN2v88internal15MemoryAllocator8Unmapper41PerformFreeMemoryOnQueuedNonRegularChunksEv
	.section	.text._ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE0EEEvPNS0_11MemoryChunkE,"axG",@progbits,_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE0EEEvPNS0_11MemoryChunkE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE0EEEvPNS0_11MemoryChunkE
	.type	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE0EEEvPNS0_11MemoryChunkE, @function
_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE0EEEvPNS0_11MemoryChunkE:
.LFB25877:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	41016(%rax), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L3509
.L3504:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal15MemoryAllocator16UnregisterMemoryEPNS0_11MemoryChunkE
	movq	8(%r12), %rdx
	movq	0(%r13), %rax
	movq	%r12, %rsi
	shrq	$6, %rdx
	leaq	37592(%rax), %rdi
	andl	$1, %edx
	call	_ZN2v88internal4Heap20RememberUnmappedPageEmb@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	orq	$8192, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal15MemoryAllocator17PerformFreeMemoryEPNS0_11MemoryChunkE
	.p2align 4,,10
	.p2align 3
.L3509:
	.cfi_restore_state
	movq	%r12, %rdx
	leaq	.LC33(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6Logger11DeleteEventEPKcPv@PLT
	jmp	.L3504
	.cfi_endproc
.LFE25877:
	.size	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE0EEEvPNS0_11MemoryChunkE, .-_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE0EEEvPNS0_11MemoryChunkE
	.section	.text._ZN2v88internal10PagedSpace8TearDownEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace8TearDownEv
	.type	_ZN2v88internal10PagedSpace8TearDownEv, @function
_ZN2v88internal10PagedSpace8TearDownEv:
.LFB22935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	jmp	.L3515
	.p2align 4,,10
	.p2align 3
.L3511:
	cmpq	%rax, %rsi
	jne	.L3512
	movq	232(%rsi), %rax
	movq	%rax, 40(%rbx)
.L3512:
	movq	224(%rsi), %rax
	movq	%rax, 32(%rbx)
	movq	232(%rsi), %rdx
	testq	%rax, %rax
	je	.L3513
	movq	%rdx, 232(%rax)
.L3513:
	testq	%rdx, %rdx
	je	.L3514
	movq	%rax, 224(%rdx)
.L3514:
	pxor	%xmm1, %xmm1
	movups	%xmm1, 224(%rsi)
	movq	64(%rbx), %rax
	movq	2048(%rax), %rdi
	call	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE0EEEvPNS0_11MemoryChunkE
.L3515:
	movq	32(%rbx), %rsi
	movq	40(%rbx), %rax
	testq	%rsi, %rsi
	jne	.L3511
	testq	%rax, %rax
	jne	.L3512
	pxor	%xmm0, %xmm0
	movq	$0, 168(%rbx)
	mfence
	movups	%xmm0, 176(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22935:
	.size	_ZN2v88internal10PagedSpace8TearDownEv, .-_ZN2v88internal10PagedSpace8TearDownEv
	.section	.rodata._ZN2v88internal16LargeObjectSpace8TearDownEv.str1.1,"aMS",@progbits,1
.LC36:
	.string	"LargeObjectChunk"
	.section	.text._ZN2v88internal16LargeObjectSpace8TearDownEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16LargeObjectSpace8TearDownEv
	.type	_ZN2v88internal16LargeObjectSpace8TearDownEv, @function
_ZN2v88internal16LargeObjectSpace8TearDownEv:
.LFB23139:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	jmp	.L3531
	.p2align 4,,10
	.p2align 3
.L3526:
	cmpq	40(%rbx), %r12
	je	.L3543
.L3527:
	movq	224(%r12), %rax
	cmpq	32(%rbx), %r12
	je	.L3544
.L3528:
	movq	232(%r12), %rdx
	testq	%rax, %rax
	je	.L3529
	movq	%rdx, 232(%rax)
.L3529:
	testq	%rdx, %rdx
	je	.L3530
	movq	%rax, 224(%rdx)
.L3530:
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movups	%xmm0, 224(%r12)
	movq	64(%rbx), %rax
	movq	2048(%rax), %rdi
	call	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE0EEEvPNS0_11MemoryChunkE
.L3531:
	movq	32(%rbx), %r12
	testq	%r12, %r12
	je	.L3545
.L3525:
	movq	64(%rbx), %rax
	movq	3424(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L3526
	movq	%r12, %rdx
	leaq	.LC36(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger11DeleteEventEPKcPv@PLT
	cmpq	40(%rbx), %r12
	jne	.L3527
.L3543:
	movq	232(%r12), %rax
	movq	%rax, 40(%rbx)
	movq	224(%r12), %rax
	cmpq	32(%rbx), %r12
	jne	.L3528
.L3544:
	movq	%rax, 32(%rbx)
	movq	224(%r12), %rax
	jmp	.L3528
	.p2align 4,,10
	.p2align 3
.L3545:
	cmpq	$0, 40(%rbx)
	jne	.L3525
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23139:
	.size	_ZN2v88internal16LargeObjectSpace8TearDownEv, .-_ZN2v88internal16LargeObjectSpace8TearDownEv
	.section	.text._ZN2v88internal16LargeObjectSpaceD2Ev,"axG",@progbits,_ZN2v88internal16LargeObjectSpaceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16LargeObjectSpaceD2Ev
	.type	_ZN2v88internal16LargeObjectSpaceD2Ev, @function
_ZN2v88internal16LargeObjectSpaceD2Ev:
.LFB8297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal16LargeObjectSpaceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal16LargeObjectSpace8TearDownEv
	movq	48(%rbx), %rdi
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L3547
	call	_ZdaPv@PLT
.L3547:
	movq	96(%rbx), %rdi
	movq	$0, 48(%rbx)
	testq	%rdi, %rdi
	je	.L3548
	movq	(%rdi), %rax
	call	*8(%rax)
.L3548:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3546
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3546:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8297:
	.size	_ZN2v88internal16LargeObjectSpaceD2Ev, .-_ZN2v88internal16LargeObjectSpaceD2Ev
	.weak	_ZN2v88internal16LargeObjectSpaceD1Ev
	.set	_ZN2v88internal16LargeObjectSpaceD1Ev,_ZN2v88internal16LargeObjectSpaceD2Ev
	.section	.text._ZN2v88internal16LargeObjectSpaceD0Ev,"axG",@progbits,_ZN2v88internal16LargeObjectSpaceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16LargeObjectSpaceD0Ev
	.type	_ZN2v88internal16LargeObjectSpaceD0Ev, @function
_ZN2v88internal16LargeObjectSpaceD0Ev:
.LFB8299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal16LargeObjectSpaceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal16LargeObjectSpace8TearDownEv
	movq	48(%r12), %rdi
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L3558
	call	_ZdaPv@PLT
.L3558:
	movq	$0, 48(%r12)
	movq	96(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3559
	movq	(%rdi), %rax
	call	*8(%rax)
.L3559:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3560
	call	_ZdlPv@PLT
.L3560:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE8299:
	.size	_ZN2v88internal16LargeObjectSpaceD0Ev, .-_ZN2v88internal16LargeObjectSpaceD0Ev
	.section	.text._ZN2v88internal20CodeLargeObjectSpaceD2Ev,"axG",@progbits,_ZN2v88internal20CodeLargeObjectSpaceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20CodeLargeObjectSpaceD2Ev
	.type	_ZN2v88internal20CodeLargeObjectSpaceD2Ev, @function
_ZN2v88internal20CodeLargeObjectSpaceD2Ev:
.LFB30694:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal20CodeLargeObjectSpaceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	144(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L3572
	.p2align 4,,10
	.p2align 3
.L3573:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L3573
.L3572:
	movq	136(%rbx), %rax
	movq	128(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%rbx), %rdi
	leaq	176(%rbx), %rax
	movq	$0, 152(%rbx)
	movq	$0, 144(%rbx)
	cmpq	%rax, %rdi
	je	.L3574
	call	_ZdlPv@PLT
.L3574:
	leaq	16+_ZTVN2v88internal16LargeObjectSpaceE(%rip), %rax
	movq	%rbx, %rdi
	movq	%rax, (%rbx)
	call	_ZN2v88internal16LargeObjectSpace8TearDownEv
	movq	48(%rbx), %rdi
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L3575
	call	_ZdaPv@PLT
.L3575:
	movq	96(%rbx), %rdi
	movq	$0, 48(%rbx)
	testq	%rdi, %rdi
	je	.L3576
	movq	(%rdi), %rax
	call	*8(%rax)
.L3576:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3571
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3571:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE30694:
	.size	_ZN2v88internal20CodeLargeObjectSpaceD2Ev, .-_ZN2v88internal20CodeLargeObjectSpaceD2Ev
	.weak	_ZN2v88internal20CodeLargeObjectSpaceD1Ev
	.set	_ZN2v88internal20CodeLargeObjectSpaceD1Ev,_ZN2v88internal20CodeLargeObjectSpaceD2Ev
	.section	.text._ZN2v88internal19NewLargeObjectSpaceD2Ev,"axG",@progbits,_ZN2v88internal19NewLargeObjectSpaceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19NewLargeObjectSpaceD2Ev
	.type	_ZN2v88internal19NewLargeObjectSpaceD2Ev, @function
_ZN2v88internal19NewLargeObjectSpaceD2Ev:
.LFB30698:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal16LargeObjectSpaceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal16LargeObjectSpace8TearDownEv
	movq	48(%rbx), %rdi
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L3590
	call	_ZdaPv@PLT
.L3590:
	movq	96(%rbx), %rdi
	movq	$0, 48(%rbx)
	testq	%rdi, %rdi
	je	.L3591
	movq	(%rdi), %rax
	call	*8(%rax)
.L3591:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3589
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3589:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE30698:
	.size	_ZN2v88internal19NewLargeObjectSpaceD2Ev, .-_ZN2v88internal19NewLargeObjectSpaceD2Ev
	.weak	_ZN2v88internal19NewLargeObjectSpaceD1Ev
	.set	_ZN2v88internal19NewLargeObjectSpaceD1Ev,_ZN2v88internal19NewLargeObjectSpaceD2Ev
	.section	.text._ZN2v88internal19NewLargeObjectSpaceD0Ev,"axG",@progbits,_ZN2v88internal19NewLargeObjectSpaceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19NewLargeObjectSpaceD0Ev
	.type	_ZN2v88internal19NewLargeObjectSpaceD0Ev, @function
_ZN2v88internal19NewLargeObjectSpaceD0Ev:
.LFB30700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal16LargeObjectSpaceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal16LargeObjectSpace8TearDownEv
	movq	48(%r12), %rdi
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L3601
	call	_ZdaPv@PLT
.L3601:
	movq	$0, 48(%r12)
	movq	96(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3602
	movq	(%rdi), %rax
	call	*8(%rax)
.L3602:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3603
	call	_ZdlPv@PLT
.L3603:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE30700:
	.size	_ZN2v88internal19NewLargeObjectSpaceD0Ev, .-_ZN2v88internal19NewLargeObjectSpaceD0Ev
	.section	.text._ZN2v88internal20CodeLargeObjectSpaceD0Ev,"axG",@progbits,_ZN2v88internal20CodeLargeObjectSpaceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20CodeLargeObjectSpaceD0Ev
	.type	_ZN2v88internal20CodeLargeObjectSpaceD0Ev, @function
_ZN2v88internal20CodeLargeObjectSpaceD0Ev:
.LFB30696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal20CodeLargeObjectSpaceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	144(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%rbx, %rbx
	je	.L3615
	.p2align 4,,10
	.p2align 3
.L3616:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L3616
.L3615:
	movq	136(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%r12), %rdi
	leaq	176(%r12), %rax
	movq	$0, 152(%r12)
	movq	$0, 144(%r12)
	cmpq	%rax, %rdi
	je	.L3617
	call	_ZdlPv@PLT
.L3617:
	leaq	16+_ZTVN2v88internal16LargeObjectSpaceE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN2v88internal16LargeObjectSpace8TearDownEv
	movq	48(%r12), %rdi
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L3618
	call	_ZdaPv@PLT
.L3618:
	movq	$0, 48(%r12)
	movq	96(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3619
	movq	(%rdi), %rax
	call	*8(%rax)
.L3619:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3620
	call	_ZdlPv@PLT
.L3620:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE30696:
	.size	_ZN2v88internal20CodeLargeObjectSpaceD0Ev, .-_ZN2v88internal20CodeLargeObjectSpaceD0Ev
	.section	.text._ZN2v88internal15CompactionSpaceD2Ev,"axG",@progbits,_ZN2v88internal15CompactionSpaceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CompactionSpaceD2Ev
	.type	_ZN2v88internal15CompactionSpaceD2Ev, @function
_ZN2v88internal15CompactionSpaceD2Ev:
.LFB30706:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal10PagedSpaceE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	jmp	.L3642
	.p2align 4,,10
	.p2align 3
.L3636:
	cmpq	%rsi, %rax
	jne	.L3637
	movq	232(%rsi), %rax
	movq	%rax, 40(%rbx)
.L3637:
	movq	224(%rsi), %rax
	movq	%rax, 32(%rbx)
	movq	232(%rsi), %rdx
	testq	%rax, %rax
	je	.L3640
	movq	%rdx, 232(%rax)
.L3640:
	testq	%rdx, %rdx
	je	.L3641
	movq	%rax, 224(%rdx)
.L3641:
	movups	%xmm0, 224(%rsi)
	movq	64(%rbx), %rax
	movq	2048(%rax), %rdi
	call	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE0EEEvPNS0_11MemoryChunkE
	pxor	%xmm0, %xmm0
.L3642:
	movq	32(%rbx), %rsi
	movq	40(%rbx), %rax
	testq	%rsi, %rsi
	jne	.L3636
	testq	%rax, %rax
	jne	.L3637
	pxor	%xmm0, %xmm0
	leaq	192(%rbx), %rdi
	movq	$0, 168(%rbx)
	mfence
	movups	%xmm0, 176(%rbx)
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	48(%rbx), %rdi
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L3639
	call	_ZdaPv@PLT
.L3639:
	movq	96(%rbx), %rdi
	movq	$0, 48(%rbx)
	testq	%rdi, %rdi
	je	.L3643
	movq	(%rdi), %rax
	call	*8(%rax)
.L3643:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3635
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3635:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE30706:
	.size	_ZN2v88internal15CompactionSpaceD2Ev, .-_ZN2v88internal15CompactionSpaceD2Ev
	.weak	_ZN2v88internal15CompactionSpaceD1Ev
	.set	_ZN2v88internal15CompactionSpaceD1Ev,_ZN2v88internal15CompactionSpaceD2Ev
	.section	.text._ZN2v88internal10PagedSpaceD0Ev,"axG",@progbits,_ZN2v88internal10PagedSpaceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10PagedSpaceD0Ev
	.type	_ZN2v88internal10PagedSpaceD0Ev, @function
_ZN2v88internal10PagedSpaceD0Ev:
.LFB8175:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal10PagedSpaceE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	jmp	.L3662
	.p2align 4,,10
	.p2align 3
.L3656:
	cmpq	%rax, %rsi
	jne	.L3657
	movq	232(%rsi), %rax
	movq	%rax, 40(%r12)
.L3657:
	movq	224(%rsi), %rax
	movq	%rax, 32(%r12)
	movq	232(%rsi), %rdx
	testq	%rax, %rax
	je	.L3660
	movq	%rdx, 232(%rax)
.L3660:
	testq	%rdx, %rdx
	je	.L3661
	movq	%rax, 224(%rdx)
.L3661:
	movups	%xmm0, 224(%rsi)
	movq	64(%r12), %rax
	movq	2048(%rax), %rdi
	call	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE0EEEvPNS0_11MemoryChunkE
	pxor	%xmm0, %xmm0
.L3662:
	movq	32(%r12), %rsi
	movq	40(%r12), %rax
	testq	%rsi, %rsi
	jne	.L3656
	testq	%rax, %rax
	jne	.L3657
	pxor	%xmm0, %xmm0
	leaq	192(%r12), %rdi
	movq	$0, 168(%r12)
	mfence
	movups	%xmm0, 176(%r12)
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	48(%r12), %rdi
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L3659
	call	_ZdaPv@PLT
.L3659:
	movq	$0, 48(%r12)
	movq	96(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3663
	movq	(%rdi), %rax
	call	*8(%rax)
.L3663:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3664
	call	_ZdlPv@PLT
.L3664:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE8175:
	.size	_ZN2v88internal10PagedSpaceD0Ev, .-_ZN2v88internal10PagedSpaceD0Ev
	.section	.text._ZN2v88internal13ReadOnlySpaceD0Ev,"axG",@progbits,_ZN2v88internal13ReadOnlySpaceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ReadOnlySpaceD0Ev
	.type	_ZN2v88internal13ReadOnlySpaceD0Ev, @function
_ZN2v88internal13ReadOnlySpaceD0Ev:
.LFB8293:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal13ReadOnlySpaceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movq	32(%rdi), %r12
	movq	64(%rdi), %rax
	movq	2048(%rax), %rbx
	testq	%r12, %r12
	jne	.L3682
	jmp	.L3679
	.p2align 4,,10
	.p2align 3
.L3681:
	movq	224(%r12), %r12
	testq	%r12, %r12
	je	.L3712
.L3682:
	movq	32(%rbx), %rdi
	testb	$1, 8(%r12)
	je	.L3680
	movq	40(%rbx), %rdi
.L3680:
	movq	(%r12), %rdx
	movl	$2, %ecx
	movq	%r12, %rsi
	call	_ZN2v88internal14SetPermissionsEPNS_13PageAllocatorEPvmNS1_10PermissionE@PLT
	testb	%al, %al
	jne	.L3681
	leaq	.LC30(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3712:
	movq	32(%r13), %r12
.L3679:
	leaq	16+_ZTVN2v88internal10PagedSpaceE(%rip), %rax
	movb	$0, 232(%r13)
	movq	%rax, 0(%r13)
	jmp	.L3689
	.p2align 4,,10
	.p2align 3
.L3683:
	cmpq	%r12, %rax
	jne	.L3684
	movq	232(%r12), %rax
	movq	%rax, 40(%r13)
.L3684:
	movq	224(%r12), %rax
	movq	%rax, 32(%r13)
	movq	232(%r12), %rdx
	testq	%rax, %rax
	je	.L3687
	movq	%rdx, 232(%rax)
.L3687:
	testq	%rdx, %rdx
	je	.L3688
	movq	%rax, 224(%rdx)
.L3688:
	pxor	%xmm1, %xmm1
	movq	%r12, %rsi
	movups	%xmm1, 224(%r12)
	movq	64(%r13), %rax
	movq	2048(%rax), %rdi
	call	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE0EEEvPNS0_11MemoryChunkE
	movq	32(%r13), %r12
.L3689:
	movq	40(%r13), %rax
	testq	%r12, %r12
	jne	.L3683
	testq	%rax, %rax
	jne	.L3684
	pxor	%xmm0, %xmm0
	leaq	192(%r13), %rdi
	movq	$0, 168(%r13)
	mfence
	movups	%xmm0, 176(%r13)
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	48(%r13), %rdi
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L3686
	call	_ZdaPv@PLT
.L3686:
	movq	96(%r13), %rdi
	movq	$0, 48(%r13)
	testq	%rdi, %rdi
	je	.L3690
	movq	(%rdi), %rax
	call	*8(%rax)
.L3690:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3691
	call	_ZdlPv@PLT
.L3691:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE8293:
	.size	_ZN2v88internal13ReadOnlySpaceD0Ev, .-_ZN2v88internal13ReadOnlySpaceD0Ev
	.section	.text._ZN2v88internal15CompactionSpaceD0Ev,"axG",@progbits,_ZN2v88internal15CompactionSpaceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CompactionSpaceD0Ev
	.type	_ZN2v88internal15CompactionSpaceD0Ev, @function
_ZN2v88internal15CompactionSpaceD0Ev:
.LFB30708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal10PagedSpaceE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	jmp	.L3720
	.p2align 4,,10
	.p2align 3
.L3714:
	cmpq	%rsi, %rax
	jne	.L3715
	movq	232(%rsi), %rax
	movq	%rax, 40(%r12)
.L3715:
	movq	224(%rsi), %rax
	movq	%rax, 32(%r12)
	movq	232(%rsi), %rdx
	testq	%rax, %rax
	je	.L3718
	movq	%rdx, 232(%rax)
.L3718:
	testq	%rdx, %rdx
	je	.L3719
	movq	%rax, 224(%rdx)
.L3719:
	movups	%xmm0, 224(%rsi)
	movq	64(%r12), %rax
	movq	2048(%rax), %rdi
	call	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE0EEEvPNS0_11MemoryChunkE
	pxor	%xmm0, %xmm0
.L3720:
	movq	32(%r12), %rsi
	movq	40(%r12), %rax
	testq	%rsi, %rsi
	jne	.L3714
	testq	%rax, %rax
	jne	.L3715
	pxor	%xmm0, %xmm0
	leaq	192(%r12), %rdi
	movq	$0, 168(%r12)
	mfence
	movups	%xmm0, 176(%r12)
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	48(%r12), %rdi
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L3717
	call	_ZdaPv@PLT
.L3717:
	movq	$0, 48(%r12)
	movq	96(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3721
	movq	(%rdi), %rax
	call	*8(%rax)
.L3721:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3722
	call	_ZdlPv@PLT
.L3722:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE30708:
	.size	_ZN2v88internal15CompactionSpaceD0Ev, .-_ZN2v88internal15CompactionSpaceD0Ev
	.section	.text._ZN2v88internal10PagedSpaceD2Ev,"axG",@progbits,_ZN2v88internal10PagedSpaceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10PagedSpaceD2Ev
	.type	_ZN2v88internal10PagedSpaceD2Ev, @function
_ZN2v88internal10PagedSpaceD2Ev:
.LFB8173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal10PagedSpaceE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	jmp	.L3743
	.p2align 4,,10
	.p2align 3
.L3737:
	cmpq	%rsi, %rax
	jne	.L3738
	movq	232(%rsi), %rax
	movq	%rax, 40(%rbx)
.L3738:
	movq	224(%rsi), %rax
	movq	%rax, 32(%rbx)
	movq	232(%rsi), %rdx
	testq	%rax, %rax
	je	.L3741
	movq	%rdx, 232(%rax)
.L3741:
	testq	%rdx, %rdx
	je	.L3742
	movq	%rax, 224(%rdx)
.L3742:
	movups	%xmm0, 224(%rsi)
	movq	64(%rbx), %rax
	movq	2048(%rax), %rdi
	call	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE0EEEvPNS0_11MemoryChunkE
	pxor	%xmm0, %xmm0
.L3743:
	movq	32(%rbx), %rsi
	movq	40(%rbx), %rax
	testq	%rsi, %rsi
	jne	.L3737
	testq	%rax, %rax
	jne	.L3738
	pxor	%xmm0, %xmm0
	leaq	192(%rbx), %rdi
	movq	$0, 168(%rbx)
	mfence
	movups	%xmm0, 176(%rbx)
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	48(%rbx), %rdi
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L3740
	call	_ZdaPv@PLT
.L3740:
	movq	96(%rbx), %rdi
	movq	$0, 48(%rbx)
	testq	%rdi, %rdi
	je	.L3744
	movq	(%rdi), %rax
	call	*8(%rax)
.L3744:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3736
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3736:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8173:
	.size	_ZN2v88internal10PagedSpaceD2Ev, .-_ZN2v88internal10PagedSpaceD2Ev
	.weak	_ZN2v88internal10PagedSpaceD1Ev
	.set	_ZN2v88internal10PagedSpaceD1Ev,_ZN2v88internal10PagedSpaceD2Ev
	.section	.text._ZN2v88internal13ReadOnlySpaceD2Ev,"axG",@progbits,_ZN2v88internal13ReadOnlySpaceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ReadOnlySpaceD2Ev
	.type	_ZN2v88internal13ReadOnlySpaceD2Ev, @function
_ZN2v88internal13ReadOnlySpaceD2Ev:
.LFB8291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal13ReadOnlySpaceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %r12
	movq	64(%rdi), %rax
	movq	2048(%rax), %r13
	testq	%r12, %r12
	jne	.L3760
	jmp	.L3757
	.p2align 4,,10
	.p2align 3
.L3759:
	movq	224(%r12), %r12
	testq	%r12, %r12
	je	.L3787
.L3760:
	movq	32(%r13), %rdi
	testb	$1, 8(%r12)
	je	.L3758
	movq	40(%r13), %rdi
.L3758:
	movq	(%r12), %rdx
	movl	$2, %ecx
	movq	%r12, %rsi
	call	_ZN2v88internal14SetPermissionsEPNS_13PageAllocatorEPvmNS1_10PermissionE@PLT
	testb	%al, %al
	jne	.L3759
	leaq	.LC30(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3787:
	movq	32(%rbx), %r12
.L3757:
	leaq	16+_ZTVN2v88internal10PagedSpaceE(%rip), %rax
	movb	$0, 232(%rbx)
	movq	%rax, (%rbx)
	jmp	.L3767
	.p2align 4,,10
	.p2align 3
.L3761:
	cmpq	%r12, %rax
	jne	.L3762
	movq	232(%r12), %rax
	movq	%rax, 40(%rbx)
.L3762:
	movq	224(%r12), %rax
	movq	%rax, 32(%rbx)
	movq	232(%r12), %rdx
	testq	%rax, %rax
	je	.L3765
	movq	%rdx, 232(%rax)
.L3765:
	testq	%rdx, %rdx
	je	.L3766
	movq	%rax, 224(%rdx)
.L3766:
	pxor	%xmm1, %xmm1
	movq	%r12, %rsi
	movups	%xmm1, 224(%r12)
	movq	64(%rbx), %rax
	movq	2048(%rax), %rdi
	call	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE0EEEvPNS0_11MemoryChunkE
	movq	32(%rbx), %r12
.L3767:
	movq	40(%rbx), %rax
	testq	%r12, %r12
	jne	.L3761
	testq	%rax, %rax
	jne	.L3762
	pxor	%xmm0, %xmm0
	leaq	192(%rbx), %rdi
	movq	$0, 168(%rbx)
	mfence
	movups	%xmm0, 176(%rbx)
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	48(%rbx), %rdi
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L3764
	call	_ZdaPv@PLT
.L3764:
	movq	96(%rbx), %rdi
	movq	$0, 48(%rbx)
	testq	%rdi, %rdi
	je	.L3768
	movq	(%rdi), %rax
	call	*8(%rax)
.L3768:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3756
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3756:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8291:
	.size	_ZN2v88internal13ReadOnlySpaceD2Ev, .-_ZN2v88internal13ReadOnlySpaceD2Ev
	.weak	_ZN2v88internal13ReadOnlySpaceD1Ev
	.set	_ZN2v88internal13ReadOnlySpaceD1Ev,_ZN2v88internal13ReadOnlySpaceD2Ev
	.section	.text._ZN2v88internal15MemoryAllocator8Unmapper12PrepareForGCEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocator8Unmapper12PrepareForGCEv
	.type	_ZN2v88internal15MemoryAllocator8Unmapper12PrepareForGCEv, @function
_ZN2v88internal15MemoryAllocator8Unmapper12PrepareForGCEv:
.LFB22804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	16(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	jmp	.L3791
	.p2align 4,,10
	.p2align 3
.L3789:
	movq	-8(%rax), %r13
	subq	$8, %rax
	movq	%r12, %rdi
	movq	%rax, 88(%rbx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	testq	%r13, %r13
	je	.L3788
	movq	8(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal15MemoryAllocator17PerformFreeMemoryEPNS0_11MemoryChunkE
.L3791:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	88(%rbx), %rax
	cmpq	80(%rbx), %rax
	jne	.L3789
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L3788:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22804:
	.size	_ZN2v88internal15MemoryAllocator8Unmapper12PrepareForGCEv, .-_ZN2v88internal15MemoryAllocator8Unmapper12PrepareForGCEv
	.section	.rodata._ZN2v88internal15MemoryAllocator8Unmapper31PerformFreeMemoryOnQueuedChunksILNS2_8FreeModeE1EEEvv.str1.8,"aMS",@progbits,1
	.align 8
.LC37:
	.string	"Unmapper::PerformFreeMemoryOnQueuedChunks: %d queued chunks\n"
	.section	.text._ZN2v88internal15MemoryAllocator8Unmapper31PerformFreeMemoryOnQueuedChunksILNS2_8FreeModeE1EEEvv,"axG",@progbits,_ZN2v88internal15MemoryAllocator8Unmapper31PerformFreeMemoryOnQueuedChunksILNS2_8FreeModeE1EEEvv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15MemoryAllocator8Unmapper31PerformFreeMemoryOnQueuedChunksILNS2_8FreeModeE1EEEvv
	.type	_ZN2v88internal15MemoryAllocator8Unmapper31PerformFreeMemoryOnQueuedChunksILNS2_8FreeModeE1EEEvv, @function
_ZN2v88internal15MemoryAllocator8Unmapper31PerformFreeMemoryOnQueuedChunksILNS2_8FreeModeE1EEEvv:
.LFB25805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	16(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal19FLAG_trace_unmapperE(%rip)
	jne	.L3820
.L3794:
	leaq	-64(%rbp), %r13
	jmp	.L3798
	.p2align 4,,10
	.p2align 3
.L3795:
	movq	-8(%rax), %r15
	subq	$8, %rax
	movq	%r12, %rdi
	movq	%rax, 64(%rbx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	testq	%r15, %r15
	je	.L3796
	movq	8(%r15), %r14
	movq	8(%rbx), %rdi
	movq	%r15, %rsi
	andl	$16384, %r14d
	call	_ZN2v88internal15MemoryAllocator17PerformFreeMemoryEPNS0_11MemoryChunkE
	testq	%r14, %r14
	jne	.L3821
.L3798:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	64(%rbx), %rax
	cmpq	56(%rbx), %rax
	jne	.L3795
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L3796
	.p2align 4,,10
	.p2align 3
.L3802:
	movq	-8(%rax), %r13
	subq	$8, %rax
	movq	%r12, %rdi
	movq	%rax, 112(%rbx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	testq	%r13, %r13
	je	.L3803
	movq	8(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE1EEEvPNS0_11MemoryChunkE
.L3796:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	112(%rbx), %rax
	cmpq	104(%rbx), %rax
	jne	.L3802
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L3803
	.p2align 4,,10
	.p2align 3
.L3805:
	movq	-8(%rax), %r13
	subq	$8, %rax
	movq	%r12, %rdi
	movq	%rax, 88(%rbx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	testq	%r13, %r13
	je	.L3793
	movq	8(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal15MemoryAllocator17PerformFreeMemoryEPNS0_11MemoryChunkE
.L3803:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	88(%rbx), %rax
	cmpq	80(%rbx), %rax
	jne	.L3805
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L3793:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3822
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3821:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%r15, -64(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	112(%rbx), %rsi
	cmpq	120(%rbx), %rsi
	je	.L3799
	movq	-64(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 112(%rbx)
.L3800:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L3798
	.p2align 4,,10
	.p2align 3
.L3820:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	64(%rbx), %rdx
	subq	56(%rbx), %rdx
	movq	%r12, %rdi
	movq	%rdx, %rax
	movq	88(%rbx), %rdx
	subq	80(%rbx), %rdx
	sarq	$3, %rax
	movq	112(%rbx), %r13
	sarq	$3, %rdx
	subq	104(%rbx), %r13
	addq	%rax, %rdx
	sarq	$3, %r13
	addq	%rdx, %r13
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	(%rbx), %rax
	movl	%r13d, %edx
	leaq	.LC37(%rip), %rsi
	leaq	-37592(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal12PrintIsolateEPvPKcz@PLT
	jmp	.L3794
.L3799:
	leaq	104(%rbx), %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIPN2v88internal11MemoryChunkESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L3800
.L3822:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25805:
	.size	_ZN2v88internal15MemoryAllocator8Unmapper31PerformFreeMemoryOnQueuedChunksILNS2_8FreeModeE1EEEvv, .-_ZN2v88internal15MemoryAllocator8Unmapper31PerformFreeMemoryOnQueuedChunksILNS2_8FreeModeE1EEEvv
	.section	.text._ZN2v88internal15MemoryAllocator8Unmapper24EnsureUnmappingCompletedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocator8Unmapper24EnsureUnmappingCompletedEv
	.type	_ZN2v88internal15MemoryAllocator8Unmapper24EnsureUnmappingCompletedEv, @function
_ZN2v88internal15MemoryAllocator8Unmapper24EnsureUnmappingCompletedEv:
.LFB22805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	160(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	xorl	%ebx, %ebx
	subq	$8, %rsp
	cmpq	$0, 192(%rdi)
	jle	.L3828
	.p2align 4,,10
	.p2align 3
.L3830:
	movq	(%r12), %rax
	movq	128(%r12,%rbx,8), %rsi
	movq	8048(%rax), %rdi
	call	_ZN2v88internal21CancelableTaskManager8TryAbortEm@PLT
	cmpl	$2, %eax
	je	.L3827
	movq	%r13, %rdi
	addq	$1, %rbx
	call	_ZN2v84base9Semaphore4WaitEv@PLT
	cmpq	192(%r12), %rbx
	jl	.L3830
.L3828:
	movq	$0, 192(%r12)
	movq	$0, 200(%r12)
	mfence
	cmpb	$0, _ZN2v88internal19FLAG_trace_unmapperE(%rip)
	jne	.L3833
.L3826:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal15MemoryAllocator8Unmapper31PerformFreeMemoryOnQueuedChunksILNS2_8FreeModeE1EEEvv
	.p2align 4,,10
	.p2align 3
.L3827:
	.cfi_restore_state
	addq	$1, %rbx
	cmpq	%rbx, 192(%r12)
	jg	.L3830
	jmp	.L3828
	.p2align 4,,10
	.p2align 3
.L3833:
	movq	(%r12), %rax
	leaq	.LC9(%rip), %rsi
	leaq	-37592(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal12PrintIsolateEPvPKcz@PLT
	jmp	.L3826
	.cfi_endproc
.LFE22805:
	.size	_ZN2v88internal15MemoryAllocator8Unmapper24EnsureUnmappingCompletedEv, .-_ZN2v88internal15MemoryAllocator8Unmapper24EnsureUnmappingCompletedEv
	.section	.rodata._ZN2v88internal15MemoryAllocator8Unmapper8TearDownEv.str1.1,"aMS",@progbits,1
.LC38:
	.string	"0 == pending_unmapping_tasks_"
	.section	.text._ZN2v88internal15MemoryAllocator8Unmapper8TearDownEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocator8Unmapper8TearDownEv
	.type	_ZN2v88internal15MemoryAllocator8Unmapper8TearDownEv, @function
_ZN2v88internal15MemoryAllocator8Unmapper8TearDownEv:
.LFB22809:
	.cfi_startproc
	endbr64
	cmpq	$0, 192(%rdi)
	jne	.L3839
	jmp	_ZN2v88internal15MemoryAllocator8Unmapper31PerformFreeMemoryOnQueuedChunksILNS2_8FreeModeE1EEEvv
	.p2align 4,,10
	.p2align 3
.L3839:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC38(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22809:
	.size	_ZN2v88internal15MemoryAllocator8Unmapper8TearDownEv, .-_ZN2v88internal15MemoryAllocator8Unmapper8TearDownEv
	.section	.text._ZN2v88internal15MemoryAllocator8TearDownEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocator8TearDownEv
	.type	_ZN2v88internal15MemoryAllocator8TearDownEv, @function
_ZN2v88internal15MemoryAllocator8TearDownEv:
.LFB22786:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 328(%rdi)
	jne	.L3860
	addq	$136, %rdi
	call	_ZN2v88internal15MemoryAllocator8Unmapper31PerformFreeMemoryOnQueuedChunksILNS2_8FreeModeE1EEEvv
	cmpq	$0, 120(%rbx)
	movq	$0, 72(%rbx)
	jne	.L3861
.L3842:
	cmpq	$0, 64(%rbx)
	je	.L3844
	movzbl	_ZN2v88internalL23code_range_address_hintE(%rip), %eax
	cmpb	$2, %al
	je	.L3845
	leaq	_ZN2v84base16LazyInstanceImplINS_8internal20CodeRangeAddressHintENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -64(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internalL23code_range_address_hintE(%rip), %rax
	leaq	-64(%rbp), %r12
	movq	%rax, -56(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r12, %rsi
	leaq	_ZN2v88internalL23code_range_address_hintE(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L3845
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L3845:
	movq	56(%rbx), %rdx
	movq	48(%rbx), %rsi
	leaq	8+_ZN2v88internalL23code_range_address_hintE(%rip), %rdi
	call	_ZN2v88internal20CodeRangeAddressHint20NotifyFreedCodeRangeEmm
	movq	64(%rbx), %r12
	pxor	%xmm0, %xmm0
	movq	$0, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	testq	%r12, %r12
	je	.L3844
	movq	(%r12), %rax
	leaq	_ZN2v84base20BoundedPageAllocatorD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3848
	leaq	16+_ZTVN2v84base20BoundedPageAllocatorE(%rip), %rax
	leaq	72(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZN2v84base15RegionAllocatorD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movl	$224, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3844:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3862
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3861:
	.cfi_restore_state
	leaq	112(%rbx), %rdi
	call	_ZN2v88internal13VirtualMemory4FreeEv@PLT
	jmp	.L3842
	.p2align 4,,10
	.p2align 3
.L3860:
	leaq	.LC38(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3848:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3844
.L3862:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22786:
	.size	_ZN2v88internal15MemoryAllocator8TearDownEv, .-_ZN2v88internal15MemoryAllocator8TearDownEv
	.section	.text._ZN2v88internal15MemoryAllocator8Unmapper31PerformFreeMemoryOnQueuedChunksILNS2_8FreeModeE0EEEvv,"axG",@progbits,_ZN2v88internal15MemoryAllocator8Unmapper31PerformFreeMemoryOnQueuedChunksILNS2_8FreeModeE0EEEvv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15MemoryAllocator8Unmapper31PerformFreeMemoryOnQueuedChunksILNS2_8FreeModeE0EEEvv
	.type	_ZN2v88internal15MemoryAllocator8Unmapper31PerformFreeMemoryOnQueuedChunksILNS2_8FreeModeE0EEEvv, @function
_ZN2v88internal15MemoryAllocator8Unmapper31PerformFreeMemoryOnQueuedChunksILNS2_8FreeModeE0EEEvv:
.LFB25784:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	16(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal19FLAG_trace_unmapperE(%rip)
	jne	.L3884
.L3864:
	leaq	-64(%rbp), %r13
	jmp	.L3868
	.p2align 4,,10
	.p2align 3
.L3865:
	movq	-8(%rax), %r15
	subq	$8, %rax
	movq	%r12, %rdi
	movq	%rax, 64(%rbx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	testq	%r15, %r15
	je	.L3866
	movq	8(%r15), %r14
	movq	8(%rbx), %rdi
	movq	%r15, %rsi
	andl	$16384, %r14d
	call	_ZN2v88internal15MemoryAllocator17PerformFreeMemoryEPNS0_11MemoryChunkE
	testq	%r14, %r14
	jne	.L3885
.L3868:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	64(%rbx), %rax
	cmpq	56(%rbx), %rax
	jne	.L3865
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L3866
	.p2align 4,,10
	.p2align 3
.L3872:
	movq	-8(%rax), %r13
	subq	$8, %rax
	movq	%r12, %rdi
	movq	%rax, 88(%rbx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	testq	%r13, %r13
	je	.L3863
	movq	8(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal15MemoryAllocator17PerformFreeMemoryEPNS0_11MemoryChunkE
.L3866:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	88(%rbx), %rax
	cmpq	80(%rbx), %rax
	jne	.L3872
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L3863:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3886
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3885:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%r15, -64(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	112(%rbx), %rsi
	cmpq	120(%rbx), %rsi
	je	.L3869
	movq	-64(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 112(%rbx)
.L3870:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L3868
	.p2align 4,,10
	.p2align 3
.L3884:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	64(%rbx), %rdx
	subq	56(%rbx), %rdx
	movq	%r12, %rdi
	movq	%rdx, %rax
	movq	88(%rbx), %rdx
	subq	80(%rbx), %rdx
	sarq	$3, %rax
	movq	112(%rbx), %r13
	sarq	$3, %rdx
	subq	104(%rbx), %r13
	addq	%rax, %rdx
	sarq	$3, %r13
	addq	%rdx, %r13
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	(%rbx), %rax
	movl	%r13d, %edx
	leaq	.LC37(%rip), %rsi
	leaq	-37592(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal12PrintIsolateEPvPKcz@PLT
	jmp	.L3864
.L3869:
	leaq	104(%rbx), %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIPN2v88internal11MemoryChunkESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L3870
.L3886:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25784:
	.size	_ZN2v88internal15MemoryAllocator8Unmapper31PerformFreeMemoryOnQueuedChunksILNS2_8FreeModeE0EEEvv, .-_ZN2v88internal15MemoryAllocator8Unmapper31PerformFreeMemoryOnQueuedChunksILNS2_8FreeModeE0EEEvv
	.section	.rodata._ZN2v88internal15MemoryAllocator8Unmapper16FreeQueuedChunksEv.str1.8,"aMS",@progbits,1
	.align 8
.LC39:
	.string	"Unmapper::FreeQueuedChunks: reached task limit (%d)\n"
	.align 8
.LC40:
	.string	"Unmapper::FreeQueuedChunks: new task id=%lu\n"
	.section	.text._ZN2v88internal15MemoryAllocator8Unmapper16FreeQueuedChunksEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocator8Unmapper16FreeQueuedChunksEv
	.type	_ZN2v88internal15MemoryAllocator8Unmapper16FreeQueuedChunksEv, @function
_ZN2v88internal15MemoryAllocator8Unmapper16FreeQueuedChunksEv:
.LFB22799:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	cmpl	$4, 392(%rax)
	je	.L3888
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_sweepingE(%rip)
	jne	.L3909
.L3888:
	movq	%r12, %rdi
	call	_ZN2v88internal15MemoryAllocator8Unmapper31PerformFreeMemoryOnQueuedChunksILNS2_8FreeModeE0EEEvv
.L3887:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3910
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3909:
	.cfi_restore_state
	leaq	200(%rdi), %r14
	movq	200(%rdi), %rbx
	testq	%rbx, %rbx
	jne	.L3908
	cmpq	$0, 192(%rdi)
	jg	.L3911
.L3903:
	movq	(%r12), %rax
	movl	$56, %edi
	leaq	-37592(%rax), %r13
	call	_Znwm@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE@PLT
	leaq	16+_ZTVN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskE(%rip), %rax
	movq	%r12, 40(%rbx)
	movq	%rax, (%rbx)
	addq	$48, %rax
	cmpb	$0, _ZN2v88internal19FLAG_trace_unmapperE(%rip)
	movq	%rax, 32(%rbx)
	movq	39600(%r13), %rax
	movq	%rax, 48(%rbx)
	jne	.L3912
.L3900:
	lock addq	$1, (%r14)
	movq	192(%r12), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 192(%r12)
	movq	24(%rbx), %rdx
	addq	$32, %rbx
	movq	%rdx, 128(%r12,%rax,8)
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	leaq	-48(%rbp), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	%rbx, -48(%rbp)
	call	*56(%rax)
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3887
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L3887
	.p2align 4,,10
	.p2align 3
.L3911:
	leaq	160(%rdi), %r13
.L3895:
	movq	(%r12), %rax
	movq	128(%r12,%rbx,8), %rsi
	movq	8048(%rax), %rdi
	call	_ZN2v88internal21CancelableTaskManager8TryAbortEm@PLT
	cmpl	$2, %eax
	je	.L3892
	movq	%r13, %rdi
	addq	$1, %rbx
	call	_ZN2v84base9Semaphore4WaitEv@PLT
	cmpq	%rbx, 192(%r12)
	jg	.L3895
.L3893:
	movq	$0, 192(%r12)
	movq	$0, 200(%r12)
	mfence
	cmpb	$0, _ZN2v88internal19FLAG_trace_unmapperE(%rip)
	je	.L3896
	movq	(%r12), %rax
	leaq	.LC9(%rip), %rsi
	leaq	-37592(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal12PrintIsolateEPvPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3908:
	movq	192(%r12), %rax
	cmpq	$4, %rax
	jne	.L3903
	cmpb	$0, _ZN2v88internal19FLAG_trace_unmapperE(%rip)
	je	.L3887
	movq	(%r12), %rdi
	movl	$4, %edx
	leaq	.LC39(%rip), %rsi
	xorl	%eax, %eax
	subq	$37592, %rdi
	call	_ZN2v88internal12PrintIsolateEPvPKcz@PLT
	jmp	.L3887
	.p2align 4,,10
	.p2align 3
.L3892:
	addq	$1, %rbx
	cmpq	%rbx, 192(%r12)
	jg	.L3895
	jmp	.L3893
	.p2align 4,,10
	.p2align 3
.L3896:
	cmpq	$4, 192(%r12)
	je	.L3887
	jmp	.L3903
	.p2align 4,,10
	.p2align 3
.L3912:
	movq	(%r12), %rax
	movq	24(%rbx), %rdx
	leaq	.LC40(%rip), %rsi
	leaq	-37592(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal12PrintIsolateEPvPKcz@PLT
	jmp	.L3900
.L3910:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22799:
	.size	_ZN2v88internal15MemoryAllocator8Unmapper16FreeQueuedChunksEv, .-_ZN2v88internal15MemoryAllocator8Unmapper16FreeQueuedChunksEv
	.section	.text._ZN2v88internal19NewLargeObjectSpace15FreeDeadObjectsERKSt8functionIFbNS0_10HeapObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19NewLargeObjectSpace15FreeDeadObjectsERKSt8functionIFbNS0_10HeapObjectEEE
	.type	_ZN2v88internal19NewLargeObjectSpace15FreeDeadObjectsERKSt8functionIFbNS0_10HeapObjectEEE, @function
_ZN2v88internal19NewLargeObjectSpace15FreeDeadObjectsERKSt8functionIFbNS0_10HeapObjectEEE:
.LFB23161:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	32(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	64(%rdi), %rax
	movq	2064(%rax), %rax
	movl	80(%rax), %eax
	movl	%eax, -96(%rbp)
	testq	%rbx, %rbx
	je	.L3914
	leaq	-72(%rbp), %rax
	movb	$0, -89(%rbp)
	movq	%rsi, %r12
	movq	$0, -104(%rbp)
	movq	%rax, -88(%rbp)
	jmp	.L3924
	.p2align 4,,10
	.p2align 3
.L3946:
	movq	(%r14), %rsi
	leaq	_ZN2v88internal16LargeObjectSpace10RemovePageEPNS0_9LargePageEm(%rip), %rcx
	movq	136(%rsi), %r9
	cmpq	%rcx, %r9
	jne	.L3917
	movslq	(%r15), %rsi
	subq	%rsi, 104(%r14)
	movq	(%r15), %rsi
	subq	%r13, 120(%r14)
	subq	%rsi, 80(%r14)
	subl	$1, 112(%r14)
	cmpq	40(%r14), %r15
	je	.L3941
.L3918:
	movq	224(%r15), %rdx
	cmpq	32(%r14), %r15
	je	.L3942
.L3919:
	movq	232(%r15), %rsi
	testq	%rdx, %rdx
	je	.L3920
	movq	%rsi, 232(%rdx)
.L3920:
	testq	%rsi, %rsi
	je	.L3921
	movq	%rdx, 224(%rsi)
.L3921:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 224(%r15)
.L3922:
	movq	64(%r14), %rdx
	movq	%r15, %rsi
	movb	%al, -90(%rbp)
	movq	2048(%rdx), %rdi
	call	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE2EEEvPNS0_11MemoryChunkE
	cmpl	$1, -96(%rbp)
	movzbl	-90(%rbp), %eax
	setg	%r13b
	andb	_ZN2v88internal23FLAG_concurrent_markingE(%rip), %r13b
	movb	%r13b, -89(%rbp)
	jne	.L3943
	movb	%al, -89(%rbp)
.L3923:
	testq	%rbx, %rbx
	je	.L3944
.L3924:
	movq	%rbx, %r15
	movq	224(%rbx), %rbx
	movq	40(%r15), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, -72(%rbp)
	movq	(%rax), %rsi
	movq	-88(%rbp), %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cmpq	$0, 16(%r12)
	movslq	%eax, %r13
	movq	-72(%rbp), %rax
	movq	%rax, -64(%rbp)
	je	.L3945
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	call	*24(%r12)
	testb	%al, %al
	jne	.L3946
	addq	%r13, -104(%rbp)
	testq	%rbx, %rbx
	jne	.L3924
.L3944:
	movq	-104(%rbp), %rax
	cmpb	$0, -89(%rbp)
	movq	%rax, 120(%r14)
	je	.L3913
	movq	64(%r14), %rax
	movq	2048(%rax), %rdi
	addq	$136, %rdi
	call	_ZN2v88internal15MemoryAllocator8Unmapper16FreeQueuedChunksEv
.L3913:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3947
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3942:
	.cfi_restore_state
	movq	%rdx, 32(%r14)
	movq	224(%r15), %rdx
	jmp	.L3919
	.p2align 4,,10
	.p2align 3
.L3941:
	movq	232(%r15), %rdx
	movq	%rdx, 40(%r14)
	jmp	.L3918
	.p2align 4,,10
	.p2align 3
.L3943:
	movq	64(%r14), %rax
	movq	%r15, %rsi
	movq	2072(%rax), %rdi
	call	_ZN2v88internal17ConcurrentMarking20ClearMemoryChunkDataEPNS0_11MemoryChunkE@PLT
	jmp	.L3923
	.p2align 4,,10
	.p2align 3
.L3917:
	movb	%al, -89(%rbp)
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	*%r9
	movzbl	-89(%rbp), %eax
	jmp	.L3922
.L3914:
	movq	$0, 120(%rdi)
	jmp	.L3913
.L3945:
	call	_ZSt25__throw_bad_function_callv@PLT
.L3947:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23161:
	.size	_ZN2v88internal19NewLargeObjectSpace15FreeDeadObjectsERKSt8functionIFbNS0_10HeapObjectEEE, .-_ZN2v88internal19NewLargeObjectSpace15FreeDeadObjectsERKSt8functionIFbNS0_10HeapObjectEEE
	.section	.text._ZN2v88internal9SemiSpace8UncommitEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9SemiSpace8UncommitEv
	.type	_ZN2v88internal9SemiSpace8UncommitEv, @function
_ZN2v88internal9SemiSpace8UncommitEv:
.LFB22999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	jmp	.L3953
	.p2align 4,,10
	.p2align 3
.L3949:
	cmpq	%rax, %rsi
	jne	.L3950
	movq	232(%rsi), %rax
	movq	%rax, 40(%rbx)
.L3950:
	movq	224(%rsi), %rax
	movq	%rax, 32(%rbx)
	movq	232(%rsi), %rdx
	testq	%rax, %rax
	je	.L3951
	movq	%rdx, 232(%rax)
.L3951:
	testq	%rdx, %rdx
	je	.L3952
	movq	%rax, 224(%rdx)
.L3952:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 224(%rsi)
	movq	64(%rbx), %rax
	movq	2048(%rax), %rdi
	call	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE3EEEvPNS0_11MemoryChunkE
.L3953:
	movq	32(%rbx), %rsi
	movq	40(%rbx), %rax
	testq	%rsi, %rsi
	jne	.L3949
	testq	%rax, %rax
	jne	.L3950
	movq	104(%rbx), %rax
	subq	%rax, 80(%rbx)
	movq	64(%rbx), %rax
	movb	$0, 136(%rbx)
	movq	$0, 144(%rbx)
	movq	2048(%rax), %rdi
	addq	$136, %rdi
	call	_ZN2v88internal15MemoryAllocator8Unmapper16FreeQueuedChunksEv
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22999:
	.size	_ZN2v88internal9SemiSpace8UncommitEv, .-_ZN2v88internal9SemiSpace8UncommitEv
	.section	.text._ZN2v88internal9SemiSpace8TearDownEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9SemiSpace8TearDownEv
	.type	_ZN2v88internal9SemiSpace8TearDownEv, @function
_ZN2v88internal9SemiSpace8TearDownEv:
.LFB22997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 136(%rdi)
	jne	.L3966
	pxor	%xmm0, %xmm0
	movups	%xmm0, 104(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3966:
	.cfi_restore_state
	call	_ZN2v88internal9SemiSpace8UncommitEv
	pxor	%xmm0, %xmm0
	movups	%xmm0, 104(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22997:
	.size	_ZN2v88internal9SemiSpace8TearDownEv, .-_ZN2v88internal9SemiSpace8TearDownEv
	.section	.text._ZN2v88internal8NewSpace8TearDownEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8NewSpace8TearDownEv
	.type	_ZN2v88internal8NewSpace8TearDownEv, @function
_ZN2v88internal8NewSpace8TearDownEv:
.LFB22967:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 344(%rdi)
	movups	%xmm0, 104(%rdi)
	jne	.L3971
.L3968:
	pxor	%xmm0, %xmm0
	cmpb	$0, 504(%rbx)
	movups	%xmm0, 312(%rbx)
	jne	.L3972
.L3969:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 472(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3971:
	.cfi_restore_state
	leaq	208(%rdi), %rdi
	call	_ZN2v88internal9SemiSpace8UncommitEv
	jmp	.L3968
	.p2align 4,,10
	.p2align 3
.L3972:
	leaq	368(%rbx), %rdi
	call	_ZN2v88internal9SemiSpace8UncommitEv
	jmp	.L3969
	.cfi_endproc
.LFE22967:
	.size	_ZN2v88internal8NewSpace8TearDownEv, .-_ZN2v88internal8NewSpace8TearDownEv
	.section	.text._ZN2v88internal8NewSpaceD0Ev,"axG",@progbits,_ZN2v88internal8NewSpaceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8NewSpaceD0Ev
	.type	_ZN2v88internal8NewSpaceD0Ev, @function
_ZN2v88internal8NewSpaceD0Ev:
.LFB8238:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal8NewSpaceE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 344(%rdi)
	movq	%rax, (%rdi)
	movups	%xmm0, 104(%rdi)
	jne	.L4013
.L3974:
	cmpb	$0, 504(%r12)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 312(%r12)
	jne	.L4014
.L3975:
	pxor	%xmm0, %xmm0
	leaq	528(%r12), %rdi
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %rbx
	movups	%xmm0, 472(%r12)
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
	movq	416(%r12), %rdi
	movq	%rbx, 368(%r12)
	testq	%rdi, %rdi
	je	.L3976
	call	_ZdaPv@PLT
.L3976:
	movq	464(%r12), %rdi
	movq	$0, 416(%r12)
	testq	%rdi, %rdi
	je	.L3977
	movq	(%rdi), %rax
	call	*8(%rax)
.L3977:
	movq	376(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3978
	call	_ZdlPv@PLT
.L3978:
	movq	256(%r12), %rdi
	movq	%rbx, 208(%r12)
	testq	%rdi, %rdi
	je	.L3979
	call	_ZdaPv@PLT
.L3979:
	movq	304(%r12), %rdi
	movq	$0, 256(%r12)
	testq	%rdi, %rdi
	je	.L3980
	movq	(%rdi), %rax
	call	*8(%rax)
.L3980:
	movq	216(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3981
	call	_ZdlPv@PLT
.L3981:
	leaq	152(%r12), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	48(%r12), %rdi
	movq	%rbx, (%r12)
	testq	%rdi, %rdi
	je	.L3982
	call	_ZdaPv@PLT
.L3982:
	movq	$0, 48(%r12)
	movq	96(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3983
	movq	(%rdi), %rax
	call	*8(%rax)
.L3983:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3984
	call	_ZdlPv@PLT
.L3984:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L4013:
	.cfi_restore_state
	leaq	208(%rdi), %rdi
	call	_ZN2v88internal9SemiSpace8UncommitEv
	jmp	.L3974
	.p2align 4,,10
	.p2align 3
.L4014:
	leaq	368(%r12), %rdi
	call	_ZN2v88internal9SemiSpace8UncommitEv
	jmp	.L3975
	.cfi_endproc
.LFE8238:
	.size	_ZN2v88internal8NewSpaceD0Ev, .-_ZN2v88internal8NewSpaceD0Ev
	.section	.text._ZN2v88internal8NewSpaceD2Ev,"axG",@progbits,_ZN2v88internal8NewSpaceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8NewSpaceD2Ev
	.type	_ZN2v88internal8NewSpaceD2Ev, @function
_ZN2v88internal8NewSpaceD2Ev:
.LFB8236:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal8NewSpaceE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpb	$0, 344(%rdi)
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movups	%xmm0, 104(%rdi)
	jne	.L4052
.L4016:
	pxor	%xmm0, %xmm0
	cmpb	$0, 504(%rbx)
	movups	%xmm0, 312(%rbx)
	jne	.L4053
.L4017:
	pxor	%xmm0, %xmm0
	leaq	528(%rbx), %rdi
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %r12
	movups	%xmm0, 472(%rbx)
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
	movq	416(%rbx), %rdi
	movq	%r12, 368(%rbx)
	testq	%rdi, %rdi
	je	.L4018
	call	_ZdaPv@PLT
.L4018:
	movq	464(%rbx), %rdi
	movq	$0, 416(%rbx)
	testq	%rdi, %rdi
	je	.L4019
	movq	(%rdi), %rax
	call	*8(%rax)
.L4019:
	movq	376(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L4020
	call	_ZdlPv@PLT
.L4020:
	movq	256(%rbx), %rdi
	movq	%r12, 208(%rbx)
	testq	%rdi, %rdi
	je	.L4021
	call	_ZdaPv@PLT
.L4021:
	movq	304(%rbx), %rdi
	movq	$0, 256(%rbx)
	testq	%rdi, %rdi
	je	.L4022
	movq	(%rdi), %rax
	call	*8(%rax)
.L4022:
	movq	216(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L4023
	call	_ZdlPv@PLT
.L4023:
	leaq	152(%rbx), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	48(%rbx), %rdi
	movq	%r12, (%rbx)
	testq	%rdi, %rdi
	je	.L4024
	call	_ZdaPv@PLT
.L4024:
	movq	96(%rbx), %rdi
	movq	$0, 48(%rbx)
	testq	%rdi, %rdi
	je	.L4025
	movq	(%rdi), %rax
	call	*8(%rax)
.L4025:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L4015
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L4015:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4053:
	.cfi_restore_state
	leaq	368(%rbx), %rdi
	call	_ZN2v88internal9SemiSpace8UncommitEv
	jmp	.L4017
	.p2align 4,,10
	.p2align 3
.L4052:
	leaq	208(%rdi), %rdi
	call	_ZN2v88internal9SemiSpace8UncommitEv
	jmp	.L4016
	.cfi_endproc
.LFE8236:
	.size	_ZN2v88internal8NewSpaceD2Ev, .-_ZN2v88internal8NewSpaceD2Ev
	.weak	_ZN2v88internal8NewSpaceD1Ev
	.set	_ZN2v88internal8NewSpaceD1Ev,_ZN2v88internal8NewSpaceD2Ev
	.section	.text._ZN2v88internal9SemiSpace8ShrinkToEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9SemiSpace8ShrinkToEm
	.type	_ZN2v88internal9SemiSpace8ShrinkToEm, @function
_ZN2v88internal9SemiSpace8ShrinkToEm:
.LFB23003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpb	$0, 136(%rdi)
	movq	%rdi, %rbx
	jne	.L4074
.L4055:
	movq	%r13, 104(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4074:
	.cfi_restore_state
	movq	104(%rdi), %r14
	movq	%r14, %rax
	subq	%rsi, %rax
	shrq	$18, %rax
	movl	%eax, %r12d
	testl	%eax, %eax
	jg	.L4061
	jmp	.L4056
	.p2align 4,,10
	.p2align 3
.L4057:
	movq	224(%rsi), %rdx
	movq	232(%rsi), %rax
	testq	%rdx, %rdx
	je	.L4058
	movq	%rax, 232(%rdx)
.L4058:
	testq	%rax, %rax
	je	.L4059
	pxor	%xmm0, %xmm0
	movq	%rdx, 224(%rax)
	movups	%xmm0, 224(%rsi)
.L4073:
	movq	64(%rbx), %rax
	movq	2048(%rax), %rdi
	call	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE3EEEvPNS0_11MemoryChunkE
	subl	$1, %r12d
	je	.L4056
.L4061:
	movq	40(%rbx), %rsi
	movq	232(%rsi), %rax
	movq	%rax, 40(%rbx)
	cmpq	32(%rbx), %rsi
	jne	.L4057
	movq	224(%rsi), %rax
	movq	%rax, 32(%rbx)
	jmp	.L4057
	.p2align 4,,10
	.p2align 3
.L4059:
	pxor	%xmm1, %xmm1
	movups	%xmm1, 224(%rsi)
	jmp	.L4073
	.p2align 4,,10
	.p2align 3
.L4056:
	movq	%r13, %rax
	subq	%r14, %rax
	addq	%rax, 80(%rbx)
	movq	64(%rbx), %rax
	movq	2048(%rax), %rdi
	addq	$136, %rdi
	call	_ZN2v88internal15MemoryAllocator8Unmapper16FreeQueuedChunksEv
	jmp	.L4055
	.cfi_endproc
.LFE23003:
	.size	_ZN2v88internal9SemiSpace8ShrinkToEm, .-_ZN2v88internal9SemiSpace8ShrinkToEm
	.section	.rodata._ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTask11RunInternalEv.str1.1,"aMS",@progbits,1
.LC41:
	.string	"disabled-by-default-v8.gc"
	.section	.rodata._ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTask11RunInternalEv.str1.8,"aMS",@progbits,1
	.align 8
.LC42:
	.string	"UnmapFreeMemoryTask Done: id=%lu\n"
	.section	.text._ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTask11RunInternalEv,"axG",@progbits,_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTask11RunInternalEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTask11RunInternalEv
	.type	_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTask11RunInternalEv, @function
_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTask11RunInternalEv:
.LFB22798:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-144(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-184(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	48(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8GCTracer32worker_thread_runtime_call_statsEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeC1EPNS0_28WorkerThreadRuntimeCallStatsE@PLT
	movq	48(%rbx), %rsi
	movl	$2, %edx
	movq	%r13, %rdi
	movq	-184(%rbp), %rcx
	call	_ZN2v88internal8GCTracer15BackgroundScopeC1EPS1_NS2_7ScopeIdEPNS0_16RuntimeCallStatsE@PLT
	movq	_ZZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTask11RunInternalEvE28trace_event_unique_atomic262(%rip), %rdx
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L4097
.L4077:
	movq	$0, -176(%rbp)
	movzbl	(%r14), %eax
	testb	$5, %al
	jne	.L4098
.L4079:
	movq	40(%rbx), %rdi
	call	_ZN2v88internal15MemoryAllocator8Unmapper31PerformFreeMemoryOnQueuedChunksILNS2_8FreeModeE0EEEvv
	movq	40(%rbx), %rax
	lock subq	$1, 200(%rax)
	movq	40(%rbx), %rax
	leaq	160(%rax), %rdi
	call	_ZN2v84base9Semaphore6SignalEv@PLT
	cmpb	$0, _ZN2v88internal19FLAG_trace_unmapperE(%rip)
	jne	.L4099
.L4083:
	leaq	-176(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	%r13, %rdi
	call	_ZN2v88internal8GCTracer15BackgroundScopeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4100
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4097:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r14
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4101
.L4078:
	movq	%r14, _ZZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTask11RunInternalEvE28trace_event_unique_atomic262(%rip)
	jmp	.L4077
	.p2align 4,,10
	.p2align 3
.L4099:
	movq	40(%rbx), %rax
	movq	24(%rbx), %rdx
	leaq	.LC42(%rip), %rsi
	movq	(%rax), %rdi
	xorl	%eax, %eax
	subq	$37592, %rdi
	call	_ZN2v88internal12PrintIsolateEPvPKcz@PLT
	jmp	.L4083
	.p2align 4,,10
	.p2align 3
.L4098:
	movl	$2, %edi
	xorl	%r15d, %r15d
	call	_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, -200(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4102
.L4080:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4081
	movq	(%rdi), %rax
	call	*8(%rax)
.L4081:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4082
	movq	(%rdi), %rax
	call	*8(%rax)
.L4082:
	movl	$2, %edi
	call	_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE@PLT
	movq	%r14, -168(%rbp)
	movq	%rax, -160(%rbp)
	leaq	-168(%rbp), %rax
	movq	%r15, -152(%rbp)
	movq	%rax, -176(%rbp)
	jmp	.L4079
	.p2align 4,,10
	.p2align 3
.L4101:
	leaq	.LC41(%rip), %rsi
	call	*%rax
	movq	%rax, %r14
	jmp	.L4078
	.p2align 4,,10
	.p2align 3
.L4102:
	subq	$8, %rsp
	leaq	-80(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r14, %rdx
	movl	$88, %esi
	pushq	%rcx
	movq	-200(%rbp), %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L4080
.L4100:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22798:
	.size	_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTask11RunInternalEv, .-_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTask11RunInternalEv
	.section	.text._ZN2v88internal14CancelableTask3RunEv,"axG",@progbits,_ZN2v88internal14CancelableTask3RunEv,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal14CancelableTask3RunEv
	.type	_ZThn32_N2v88internal14CancelableTask3RunEv, @function
_ZThn32_N2v88internal14CancelableTask3RunEv:
.LFB31382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	lock cmpxchgl	%edx, -16(%rdi)
	jne	.L4103
	movq	-32(%rdi), %rax
	leaq	_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTask11RunInternalEv(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L4129
	subq	$32, %rdi
	call	*%rax
.L4103:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4130
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4130:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L4129:
	movq	16(%rdi), %rdi
	leaq	-184(%rbp), %r14
	leaq	-144(%rbp), %r15
	call	_ZN2v88internal8GCTracer32worker_thread_runtime_call_statsEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeC1EPNS0_28WorkerThreadRuntimeCallStatsE@PLT
	movq	16(%rbx), %rsi
	movl	$2, %edx
	movq	%r15, %rdi
	movq	-184(%rbp), %rcx
	call	_ZN2v88internal8GCTracer15BackgroundScopeC1EPS1_NS2_7ScopeIdEPNS0_16RuntimeCallStatsE@PLT
	movq	_ZZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTask11RunInternalEvE28trace_event_unique_atomic262(%rip), %r12
	testq	%r12, %r12
	je	.L4131
.L4106:
	movq	$0, -176(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L4132
.L4108:
	movq	8(%rbx), %rdi
	call	_ZN2v88internal15MemoryAllocator8Unmapper31PerformFreeMemoryOnQueuedChunksILNS2_8FreeModeE0EEEvv
	movq	8(%rbx), %rax
	lock subq	$1, 200(%rax)
	movq	8(%rbx), %rax
	leaq	160(%rax), %rdi
	call	_ZN2v84base9Semaphore6SignalEv@PLT
	cmpb	$0, _ZN2v88internal19FLAG_trace_unmapperE(%rip)
	je	.L4112
	movq	8(%rbx), %rax
	movq	-8(%rbx), %rdx
	leaq	.LC42(%rip), %rsi
	movq	(%rax), %rdi
	xorl	%eax, %eax
	subq	$37592, %rdi
	call	_ZN2v88internal12PrintIsolateEPvPKcz@PLT
.L4112:
	leaq	-176(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	%r15, %rdi
	call	_ZN2v88internal8GCTracer15BackgroundScopeD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeD1Ev@PLT
	jmp	.L4103
.L4132:
	movl	$2, %edi
	xorl	%r13d, %r13d
	call	_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, -200(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L4109
	pushq	%rdx
	leaq	-80(%rbp), %rdx
	movq	-200(%rbp), %rcx
	xorl	%r9d, %r9d
	pushq	$0
	xorl	%r8d, %r8d
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
.L4109:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4110
	movq	(%rdi), %rax
	call	*8(%rax)
.L4110:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4111
	movq	(%rdi), %rax
	call	*8(%rax)
.L4111:
	movl	$2, %edi
	call	_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE@PLT
	movq	%r12, -168(%rbp)
	movq	%rax, -160(%rbp)
	leaq	-168(%rbp), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -176(%rbp)
	jmp	.L4108
.L4131:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	je	.L4107
	leaq	.LC41(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
.L4107:
	movq	%r12, _ZZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTask11RunInternalEvE28trace_event_unique_atomic262(%rip)
	jmp	.L4106
	.cfi_endproc
.LFE31382:
	.size	_ZThn32_N2v88internal14CancelableTask3RunEv, .-_ZThn32_N2v88internal14CancelableTask3RunEv
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CancelableTask3RunEv
	.type	_ZN2v88internal14CancelableTask3RunEv, @function
_ZN2v88internal14CancelableTask3RunEv:
.LFB7944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	lock cmpxchgl	%edx, 16(%rdi)
	jne	.L4133
	movq	(%rdi), %rax
	leaq	_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTask11RunInternalEv(%rip), %rdx
	movq	%rdi, %r12
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4135
	movq	48(%rdi), %rdi
	leaq	-184(%rbp), %r13
	leaq	-144(%rbp), %r14
	call	_ZN2v88internal8GCTracer32worker_thread_runtime_call_statsEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeC1EPNS0_28WorkerThreadRuntimeCallStatsE@PLT
	movq	48(%r12), %rsi
	movl	$2, %edx
	movq	%r14, %rdi
	movq	-184(%rbp), %rcx
	call	_ZN2v88internal8GCTracer15BackgroundScopeC1EPS1_NS2_7ScopeIdEPNS0_16RuntimeCallStatsE@PLT
	movq	_ZZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTask11RunInternalEvE28trace_event_unique_atomic262(%rip), %rdx
	movq	%rdx, %r15
	testq	%rdx, %rdx
	je	.L4160
.L4137:
	movq	$0, -176(%rbp)
	movzbl	(%r15), %eax
	testb	$5, %al
	jne	.L4161
.L4139:
	movq	40(%r12), %rdi
	call	_ZN2v88internal15MemoryAllocator8Unmapper31PerformFreeMemoryOnQueuedChunksILNS2_8FreeModeE0EEEvv
	movq	40(%r12), %rax
	lock subq	$1, 200(%rax)
	movq	40(%r12), %rax
	leaq	160(%rax), %rdi
	call	_ZN2v84base9Semaphore6SignalEv@PLT
	cmpb	$0, _ZN2v88internal19FLAG_trace_unmapperE(%rip)
	jne	.L4162
.L4143:
	leaq	-176(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8GCTracer15BackgroundScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeD1Ev@PLT
.L4133:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4163
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4160:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r15
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4164
.L4138:
	movq	%r15, _ZZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTask11RunInternalEvE28trace_event_unique_atomic262(%rip)
	jmp	.L4137
	.p2align 4,,10
	.p2align 3
.L4162:
	movq	40(%r12), %rax
	movq	24(%r12), %rdx
	leaq	.LC42(%rip), %rsi
	movq	(%rax), %rdi
	xorl	%eax, %eax
	subq	$37592, %rdi
	call	_ZN2v88internal12PrintIsolateEPvPKcz@PLT
	jmp	.L4143
	.p2align 4,,10
	.p2align 3
.L4161:
	movl	$2, %edi
	xorl	%ebx, %ebx
	call	_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, -200(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4165
.L4140:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4141
	movq	(%rdi), %rax
	call	*8(%rax)
.L4141:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4142
	movq	(%rdi), %rax
	call	*8(%rax)
.L4142:
	movl	$2, %edi
	call	_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE@PLT
	movq	%r15, -168(%rbp)
	movq	%rax, -160(%rbp)
	leaq	-168(%rbp), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -176(%rbp)
	jmp	.L4139
	.p2align 4,,10
	.p2align 3
.L4135:
	call	*%rax
	jmp	.L4133
	.p2align 4,,10
	.p2align 3
.L4165:
	subq	$8, %rsp
	leaq	-80(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r15, %rdx
	movl	$88, %esi
	pushq	%rcx
	movq	-200(%rbp), %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %rbx
	addq	$64, %rsp
	jmp	.L4140
	.p2align 4,,10
	.p2align 3
.L4164:
	leaq	.LC41(%rip), %rsi
	call	*%rax
	movq	%rax, %r15
	jmp	.L4138
.L4163:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7944:
	.size	_ZN2v88internal14CancelableTask3RunEv, .-_ZN2v88internal14CancelableTask3RunEv
	.section	.text._ZNSt6vectorISt4pairImPN2v88internal4PageEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt4pairImPN2v88internal4PageEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairImPN2v88internal4PageEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorISt4pairImPN2v88internal4PageEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorISt4pairImPN2v88internal4PageEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB28966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movabsq	$576460752303423487, %rdi
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$4, %rax
	cmpq	%rdi, %rax
	je	.L4185
	movq	%rsi, %rcx
	movq	%rsi, %rbx
	subq	%r15, %rcx
	testq	%rax, %rax
	je	.L4176
	movabsq	$9223372036854775792, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L4186
.L4168:
	movq	%rsi, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	%rax, %r14
	movq	-72(%rbp), %rdx
	leaq	(%rax,%rsi), %rax
	leaq	16(%r14), %r8
.L4175:
	movq	8(%rdx), %rsi
	movq	(%rdx), %rdx
	addq	%r14, %rcx
	movq	%rdx, (%rcx)
	movq	%rsi, 8(%rcx)
	cmpq	%r15, %rbx
	je	.L4170
	movq	%r14, %rcx
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L4171:
	movq	(%rdx), %rdi
	movq	8(%rdx), %rsi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	%rdi, -16(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L4171
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	16(%r14,%rdx), %r8
.L4170:
	cmpq	%r12, %rbx
	je	.L4172
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L4173:
	movq	8(%rdx), %rsi
	movq	(%rdx), %rdi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	%rdi, -16(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%r12, %rdx
	jne	.L4173
	subq	%rbx, %rdx
	addq	%rdx, %r8
.L4172:
	testq	%r15, %r15
	je	.L4174
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
.L4174:
	movq	%r14, %xmm0
	movq	%r8, %xmm1
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4186:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L4169
	movl	$16, %r8d
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L4175
	.p2align 4,,10
	.p2align 3
.L4176:
	movl	$16, %esi
	jmp	.L4168
.L4169:
	cmpq	%rdi, %r8
	movq	%rdi, %rax
	cmovbe	%r8, %rax
	salq	$4, %rax
	movq	%rax, %rsi
	jmp	.L4168
.L4185:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE28966:
	.size	_ZNSt6vectorISt4pairImPN2v88internal4PageEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorISt4pairImPN2v88internal4PageEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text._ZN2v88internal8MapSpace12SortFreeListEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8MapSpace12SortFreeListEv
	.type	_ZN2v88internal8MapSpace12SortFreeListEv, @function
_ZN2v88internal8MapSpace12SortFreeListEv:
.LFB23105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	movq	$0, -48(%rbp)
	movaps	%xmm0, -64(%rbp)
	testq	%rax, %rax
	je	.L4187
	movq	%rdi, %r13
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L4189:
	movq	224(%rax), %rax
	addl	$1, %edx
	testq	%rax, %rax
	jne	.L4189
	movslq	%edx, %rbx
	salq	$4, %rbx
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rdi
	movq	%rax, %r12
	movq	%rax, %rdx
	movq	%r8, %rax
	cmpq	%r8, %rdi
	je	.L4193
.L4190:
	movq	(%rax), %rsi
	movq	8(%rax), %rcx
	addq	$16, %rax
	addq	$16, %rdx
	movq	%rsi, -16(%rdx)
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %rdi
	jne	.L4190
.L4193:
	testq	%r8, %r8
	je	.L4192
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L4192:
	addq	%r12, %rbx
	movq	%r12, %xmm0
	movq	%rbx, -48(%rbp)
	movq	32(%r13), %rbx
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -64(%rbp)
	testq	%rbx, %rbx
	je	.L4206
	leaq	-80(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L4198:
	movq	96(%r13), %rdi
	movq	240(%rbx), %rdx
	movq	(%rdi), %rax
	movq	(%rdx), %rsi
	call	*64(%rax)
	movq	192(%rbx), %rax
	movq	-56(%rbp), %rsi
	movq	%rbx, -72(%rbp)
	movq	%rax, -80(%rbp)
	cmpq	-48(%rbp), %rsi
	je	.L4195
	movq	%rax, (%rsi)
	movq	%rbx, 8(%rsi)
	movq	224(%rbx), %rbx
	addq	$16, -56(%rbp)
	testq	%rbx, %rbx
	jne	.L4198
.L4197:
	movq	-64(%rbp), %r14
	movq	-56(%rbp), %r12
	cmpq	%r12, %r14
	je	.L4206
	movq	%r12, %rbx
	movl	$63, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	subq	%r14, %rbx
	movq	%rbx, %rax
	sarq	$4, %rax
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPSt4pairImPN2v88internal4PageEESt6vectorIS7_SaIS7_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_8MapSpace12SortFreeListEvEUlRKS7_SH_E_EEEvT_SK_T0_T1_
	cmpq	$256, %rbx
	jle	.L4199
	leaq	256(%r14), %rsi
	movq	%r14, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPSt4pairImPN2v88internal4PageEESt6vectorIS7_SaIS7_EEEENS0_5__ops15_Iter_comp_iterIZNS4_8MapSpace12SortFreeListEvEUlRKS7_SH_E_EEEvT_SK_T0_.isra.0
	movq	%rsi, %rcx
	cmpq	%r12, %rsi
	je	.L4201
	.p2align 4,,10
	.p2align 3
.L4204:
	movq	(%rcx), %rdi
	movq	-16(%rcx), %rdx
	movq	%rcx, %rsi
	leaq	-16(%rcx), %rax
	movq	8(%rcx), %r8
	cmpq	%rdi, %rdx
	jbe	.L4202
	.p2align 4,,10
	.p2align 3
.L4203:
	movq	%rdx, 16(%rax)
	movq	8(%rax), %rdx
	movq	%rax, %rsi
	subq	$16, %rax
	movq	%rdx, 40(%rax)
	movq	(%rax), %rdx
	cmpq	%rdx, %rdi
	jb	.L4203
.L4202:
	addq	$16, %rcx
	movq	%rdi, (%rsi)
	movq	%r8, 8(%rsi)
	cmpq	%rcx, %r12
	jne	.L4204
.L4201:
	movq	-56(%rbp), %rbx
	movq	-64(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L4206
	.p2align 4,,10
	.p2align 3
.L4207:
	movq	8(%r12), %rax
	movq	96(%r13), %rdi
	addq	$16, %r12
	movq	240(%rax), %rdx
	movq	(%rdi), %rax
	movq	(%rdx), %rsi
	call	*56(%rax)
	cmpq	%rbx, %r12
	jne	.L4207
	movq	-64(%rbp), %r12
.L4206:
	testq	%r12, %r12
	je	.L4187
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L4187:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4230
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4195:
	.cfi_restore_state
	leaq	-64(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZNSt6vectorISt4pairImPN2v88internal4PageEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	movq	224(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L4198
	jmp	.L4197
	.p2align 4,,10
	.p2align 3
.L4199:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPSt4pairImPN2v88internal4PageEESt6vectorIS7_SaIS7_EEEENS0_5__ops15_Iter_comp_iterIZNS4_8MapSpace12SortFreeListEvEUlRKS7_SH_E_EEEvT_SK_T0_.isra.0
	jmp	.L4201
.L4230:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23105:
	.size	_ZN2v88internal8MapSpace12SortFreeListEv, .-_ZN2v88internal8MapSpace12SortFreeListEv
	.section	.text._ZNSt10_HashtableIPN2v88internal11MemoryChunkES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal11MemoryChunkES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal11MemoryChunkES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	.type	_ZNSt10_HashtableIPN2v88internal11MemoryChunkES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm, @function
_ZNSt10_HashtableIPN2v88internal11MemoryChunkES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm:
.LFB29100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L4232
	movq	(%rbx), %r8
.L4233:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L4242
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L4243:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4232:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L4256
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L4257
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L4235:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L4237
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L4238
	.p2align 4,,10
	.p2align 3
.L4239:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L4240:
	testq	%rsi, %rsi
	je	.L4237
.L4238:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L4239
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L4245
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L4238
	.p2align 4,,10
	.p2align 3
.L4237:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L4241
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L4241:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L4233
	.p2align 4,,10
	.p2align 3
.L4242:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L4244
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L4244:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L4243
	.p2align 4,,10
	.p2align 3
.L4245:
	movq	%rdx, %rdi
	jmp	.L4240
	.p2align 4,,10
	.p2align 3
.L4256:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L4235
.L4257:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE29100:
	.size	_ZNSt10_HashtableIPN2v88internal11MemoryChunkES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm, .-_ZNSt10_HashtableIPN2v88internal11MemoryChunkES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	.section	.rodata._ZN2v88internal15MemoryAllocator13AllocateChunkEmmNS0_13ExecutabilityEPNS0_5SpaceE.str1.1,"aMS",@progbits,1
.LC43:
	.string	"!last_chunk_.IsReserved()"
.LC44:
	.string	"last_chunk_.IsReserved()"
	.section	.text._ZN2v88internal15MemoryAllocator13AllocateChunkEmmNS0_13ExecutabilityEPNS0_5SpaceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocator13AllocateChunkEmmNS0_13ExecutabilityEPNS0_5SpaceE
	.type	_ZN2v88internal15MemoryAllocator13AllocateChunkEmmNS0_13ExecutabilityEPNS0_5SpaceE, @function
_ZN2v88internal15MemoryAllocator13AllocateChunkEmmNS0_13ExecutabilityEPNS0_5SpaceE:
.LFB22881:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$136, %rsp
	movq	%rsi, -136(%rbp)
	movq	%rdx, -120(%rbp)
	movl	%ecx, -124(%rbp)
	movq	%r8, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	$0, -112(%rbp)
	movups	%xmm0, -104(%rbp)
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal17GetRandomMmapAddrEv@PLT
	andq	$-262144, %rax
	movq	%rax, %r14
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	cmpl	$1, %ebx
	je	.L4327
	testl	%eax, %eax
	je	.L4281
	sall	$10, %eax
	movq	-136(%rbp), %rdx
	cltq
	movq	%rax, %rcx
	leaq	279(%rax,%rdx), %r13
	negq	%rcx
	andq	%rcx, %r13
.L4284:
	subq	$8, %rsp
	movq	-120(%rbp), %rsi
	leaq	-112(%rbp), %r15
	movl	-124(%rbp), %r8d
	pushq	%r15
	movq	%r14, %r9
	movq	%r12, %rdi
	leaq	279(%rax,%rsi), %rdx
	movq	%r13, %rsi
	andq	%rcx, %rdx
	movl	$262144, %ecx
	call	_ZN2v88internal15MemoryAllocator21AllocateAlignedMemoryEmmmNS0_13ExecutabilityEPvPNS0_13VirtualMemoryE
	popq	%rcx
	popq	%rsi
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L4302
	movq	-120(%rbp), %rsi
	leaq	280(%rax), %rbx
	leaq	(%rsi,%rbx), %rax
	movq	%rax, -160(%rbp)
.L4280:
	movq	(%r12), %rax
	movq	40960(%rax), %r15
	cmpb	$0, 5952(%r15)
	je	.L4285
	movq	5944(%r15), %rax
.L4286:
	testq	%rax, %rax
	je	.L4287
	addl	%r13d, (%rax)
.L4287:
	movq	(%r12), %rax
	movq	41016(%rax), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L4328
.L4288:
	movq	%r13, %rax
	addq	%r14, %rax
	jne	.L4289
	cmpq	$0, 120(%r12)
	jne	.L4329
	movq	-112(%rbp), %rax
	movdqu	-104(%rbp), %xmm1
	leaq	-112(%rbp), %r15
	movq	%r15, %rdi
	movq	%rax, 112(%r12)
	movups	%xmm1, 120(%r12)
	call	_ZN2v88internal13VirtualMemory5ResetEv@PLT
	movq	128(%r12), %rbx
	movq	120(%r12), %rsi
	xorl	%ecx, %ecx
	leaq	112(%r12), %rdi
	movq	%rbx, %rdx
	call	_ZN2v88internal13VirtualMemory14SetPermissionsEmmNS_13PageAllocator10PermissionE@PLT
	testb	%al, %al
	je	.L4292
	movq	(%r12), %rax
	movq	40960(%rax), %r14
	cmpb	$0, 5952(%r14)
	je	.L4293
	movq	5944(%r14), %rax
.L4294:
	testq	%rax, %rax
	je	.L4292
	subl	%ebx, (%rax)
.L4292:
	lock subq	%r13, 80(%r12)
	cmpl	$1, -124(%rbp)
	je	.L4330
.L4296:
	cmpq	$0, 120(%r12)
	je	.L4331
	movq	-144(%rbp), %r8
	movl	-124(%rbp), %ecx
	movq	%r12, %rdi
	movq	-120(%rbp), %rdx
	movq	-136(%rbp), %rsi
	call	_ZN2v88internal15MemoryAllocator13AllocateChunkEmmNS0_13ExecutabilityEPNS0_5SpaceE
	movq	%rax, %r13
.L4273:
	movq	%r15, %rdi
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4332
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4281:
	.cfi_restore_state
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movq	-136(%rbp), %rbx
	leaq	279(%rax,%rbx), %r13
	negq	%rax
	andq	%rax, %r13
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	jne	.L4333
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movq	%rax, %rcx
	negq	%rcx
	jmp	.L4284
	.p2align 4,,10
	.p2align 3
.L4285:
	movb	$1, 5952(%r15)
	leaq	5928(%r15), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 5944(%r15)
	jmp	.L4286
	.p2align 4,,10
	.p2align 3
.L4327:
	testl	%eax, %eax
	je	.L4260
	sall	$10, %eax
	cltq
	movq	%rax, %rbx
	movq	%rax, %r15
.L4261:
	movq	%rax, %r13
	leaq	279(%rax), %rdx
	negq	%r13
	andq	%rdx, %r13
.L4263:
	addq	-136(%rbp), %r13
	addq	%rax, %r13
.L4265:
	leaq	-1(%r13,%rax), %r13
	addq	%rbx, %r13
	negq	%rbx
	andq	%rbx, %r13
.L4267:
	movq	%r15, -160(%rbp)
	movq	%r15, %rax
.L4269:
	movq	%rax, -168(%rbp)
.L4271:
	movq	40(%r12), %rsi
	leaq	-80(%rbp), %rbx
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	$262144, %r8d
	movq	%rbx, %rdi
	call	_ZN2v88internal13VirtualMemoryC1EPNS_13PageAllocatorEmPvm@PLT
	movq	-72(%rbp), %r14
	testq	%r14, %r14
	je	.L4326
	leaq	80(%r12), %rax
	movq	%rax, -176(%rbp)
	movq	%rax, %rdx
	movq	-64(%rbp), %rax
	lock addq	%rax, (%rdx)
	movq	-168(%rbp), %rcx
	movq	-160(%rbp), %rax
	addq	$279, %rcx
	negq	%rax
	andq	%rax, %rcx
	movq	-120(%rbp), %rax
	movq	%r13, %r8
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	-1(%r15,%rax), %rax
	negq	%r15
	addq	%rax, %rcx
	andq	%r15, %rcx
	call	_ZN2v88internal15MemoryAllocator22CommitExecutableMemoryEPNS0_13VirtualMemoryEmmm
	testb	%al, %al
	je	.L4274
	movq	-80(%rbp), %rax
	movdqu	-72(%rbp), %xmm3
	movq	%rbx, %rdi
	movq	%rax, -112(%rbp)
	movups	%xmm3, -104(%rbp)
	call	_ZN2v88internal13VirtualMemory5ResetEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
	movq	-96(%rbp), %rax
	lock addq	%rax, 88(%r12)
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	je	.L4334
	sall	$10, %eax
	cltq
	movq	%rax, %rdx
	leaq	279(%rax), %r15
	negq	%rdx
	andq	%rdx, %r15
.L4279:
	addq	%r15, %rax
	leaq	(%rax,%r14), %rbx
	movq	-120(%rbp), %rax
	addq	%rbx, %rax
	movq	%rax, -160(%rbp)
	jmp	.L4280
	.p2align 4,,10
	.p2align 3
.L4289:
	movdqu	-104(%rbp), %xmm2
	movq	-112(%rbp), %rax
	leaq	-112(%rbp), %r15
	movq	%r15, %rdi
	movups	%xmm2, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal13VirtualMemory5ResetEv@PLT
	leaq	-80(%rbp), %r11
	movl	-124(%rbp), %r9d
	movq	%r13, %rdx
	pushq	%r11
	movq	-152(%rbp), %rdi
	movq	%rbx, %rcx
	movq	%r14, %rsi
	pushq	-144(%rbp)
	movq	-160(%rbp), %r8
	addq	$37592, %rdi
	movq	%r11, -120(%rbp)
	call	_ZN2v88internal11MemoryChunk10InitializeEPNS0_4HeapEmmmmNS0_13ExecutabilityEPNS0_5SpaceENS0_13VirtualMemoryE
	movq	-120(%rbp), %r11
	movq	%rax, %r13
	movq	%r11, %rdi
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
	popq	%rax
	popq	%rdx
	testb	$1, 8(%r13)
	je	.L4273
	movq	352(%r12), %rdi
	movq	%r13, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	344(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L4298
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L4300
	.p2align 4,,10
	.p2align 3
.L4335:
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r14
	jne	.L4298
.L4300:
	cmpq	%rsi, %r13
	je	.L4273
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.L4335
.L4298:
	movl	$16, %edi
	call	_Znwm@PLT
	leaq	344(%r12), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r13, 8(%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	$0, (%rax)
	call	_ZNSt10_HashtableIPN2v88internal11MemoryChunkES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	jmp	.L4273
	.p2align 4,,10
	.p2align 3
.L4328:
	movq	%r13, %rcx
	movq	%r14, %rdx
	leaq	.LC33(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger8NewEventEPKcPvm@PLT
	jmp	.L4288
	.p2align 4,,10
	.p2align 3
.L4260:
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movq	%rax, %rbx
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	movq	%rbx, %r15
	negq	%r15
	testl	%eax, %eax
	jne	.L4336
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	leaq	279(%rax), %r13
	negq	%rax
	andq	%rax, %r13
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	jne	.L4337
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	addq	-136(%rbp), %r13
	addq	%rax, %r13
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	jne	.L4338
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	leaq	-1(%rbx,%r13), %r13
	addq	%rax, %r13
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	andq	%r15, %r13
	testl	%eax, %eax
	jne	.L4339
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movq	%rax, %r15
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	jne	.L4340
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movq	%rax, -168(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L4271
	.p2align 4,,10
	.p2align 3
.L4293:
	movb	$1, 5952(%r14)
	leaq	5928(%r14), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 5944(%r14)
	jmp	.L4294
	.p2align 4,,10
	.p2align 3
.L4274:
	movq	%rbx, %rdi
	call	_ZN2v88internal13VirtualMemory4FreeEv@PLT
	movq	-176(%rbp), %rax
	lock subq	%r13, (%rax)
.L4326:
	movq	%rbx, %rdi
	xorl	%r13d, %r13d
	leaq	-112(%rbp), %r15
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
	jmp	.L4273
	.p2align 4,,10
	.p2align 3
.L4334:
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %edx
	movq	%rax, %r15
	leaq	279(%rax), %rax
	negq	%r15
	andq	%rax, %r15
	testl	%edx, %edx
	jne	.L4341
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	jmp	.L4279
	.p2align 4,,10
	.p2align 3
.L4302:
	xorl	%r13d, %r13d
	jmp	.L4273
	.p2align 4,,10
	.p2align 3
.L4330:
	lock subq	%r13, 88(%r12)
	jmp	.L4296
	.p2align 4,,10
	.p2align 3
.L4329:
	leaq	.LC43(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4331:
	leaq	.LC44(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4332:
	call	__stack_chk_fail@PLT
.L4333:
	sall	$10, %eax
	cltq
	movq	%rax, %rcx
	negq	%rcx
	jmp	.L4284
.L4340:
	sall	$10, %eax
	cltq
	movq	%rax, -160(%rbp)
	jmp	.L4269
.L4341:
	sall	$10, %edx
	movslq	%edx, %rax
	jmp	.L4279
.L4339:
	sall	$10, %eax
	movslq	%eax, %r15
	jmp	.L4267
.L4338:
	sall	$10, %eax
	cltq
	movq	%rax, %r15
	jmp	.L4265
.L4337:
	sall	$10, %eax
	cltq
	movq	%rax, %r15
	jmp	.L4263
.L4336:
	sall	$10, %eax
	cltq
	movq	%rax, %r15
	jmp	.L4261
	.cfi_endproc
.LFE22881:
	.size	_ZN2v88internal15MemoryAllocator13AllocateChunkEmmNS0_13ExecutabilityEPNS0_5SpaceE, .-_ZN2v88internal15MemoryAllocator13AllocateChunkEmmNS0_13ExecutabilityEPNS0_5SpaceE
	.section	.text._ZN2v88internal15MemoryAllocator12AllocatePageILNS1_14AllocationModeE0ENS0_10PagedSpaceEEEPNS0_4PageEmPT0_NS0_13ExecutabilityE,"axG",@progbits,_ZN2v88internal15MemoryAllocator12AllocatePageILNS1_14AllocationModeE0ENS0_10PagedSpaceEEEPNS0_4PageEmPT0_NS0_13ExecutabilityE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15MemoryAllocator12AllocatePageILNS1_14AllocationModeE0ENS0_10PagedSpaceEEEPNS0_4PageEmPT0_NS0_13ExecutabilityE
	.type	_ZN2v88internal15MemoryAllocator12AllocatePageILNS1_14AllocationModeE0ENS0_10PagedSpaceEEEPNS0_4PageEmPT0_NS0_13ExecutabilityE, @function
_ZN2v88internal15MemoryAllocator12AllocatePageILNS1_14AllocationModeE0ENS0_10PagedSpaceEEEPNS0_4PageEmPT0_NS0_13ExecutabilityE:
.LFB25881:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	movq	%rsi, %rdx
	subq	$8, %rsp
	call	_ZN2v88internal15MemoryAllocator13AllocateChunkEmmNS0_13ExecutabilityEPNS0_5SpaceE
	testq	%rax, %rax
	je	.L4342
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10PagedSpace14InitializePageEPNS0_11MemoryChunkE
	.p2align 4,,10
	.p2align 3
.L4342:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25881:
	.size	_ZN2v88internal15MemoryAllocator12AllocatePageILNS1_14AllocationModeE0ENS0_10PagedSpaceEEEPNS0_4PageEmPT0_NS0_13ExecutabilityE, .-_ZN2v88internal15MemoryAllocator12AllocatePageILNS1_14AllocationModeE0ENS0_10PagedSpaceEEEPNS0_4PageEmPT0_NS0_13ExecutabilityE
	.section	.text._ZN2v88internal10PagedSpace6ExpandEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace6ExpandEv
	.type	_ZN2v88internal10PagedSpace6ExpandEv, @function
_ZN2v88internal10PagedSpace6ExpandEv:
.LFB22948:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	64(%rdi), %rax
	movslq	72(%rdi), %rdx
	movq	312(%rax,%rdx,8), %r13
	addq	$192, %r13
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movslq	160(%rbx), %r14
	movq	64(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal4Heap22CanExpandOldGenerationEm@PLT
	testb	%al, %al
	jne	.L4346
.L4348:
	xorl	%r12d, %r12d
.L4347:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4346:
	.cfi_restore_state
	movl	%eax, %r12d
	movq	64(%rbx), %rax
	movl	152(%rbx), %ecx
	movq	%r14, %rsi
	movq	%rbx, %rdx
	movq	2048(%rax), %rdi
	call	_ZN2v88internal15MemoryAllocator12AllocatePageILNS1_14AllocationModeE0ENS0_10PagedSpaceEEEPNS0_4PageEmPT0_NS0_13ExecutabilityE
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L4348
	movq	64(%rbx), %rax
	cmpb	$0, 2868(%rax)
	jne	.L4349
	orq	$128, 8(%r14)
.L4349:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal10PagedSpace7AddPageEPNS0_4PageE
	movq	48(%r14), %r15
	movq	40(%r14), %r14
	movq	%r15, %r9
	subq	%r14, %r9
	je	.L4350
	movq	64(%rbx), %rdi
	movl	%r9d, %edx
	movq	%r14, %rsi
	movl	$1, %r8d
	movl	$1, %ecx
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	96(%rbx), %rdi
	movq	-56(%rbp), %r9
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	subq	%r15, %r14
	movq	(%rdi), %rax
	movq	%r9, %rdx
	call	*24(%rax)
	addq	%r14, 184(%rbx)
.L4350:
	movq	64(%rbx), %rdi
	call	_ZN2v88internal4Heap28NotifyOldGenerationExpansionEv@PLT
	jmp	.L4347
	.cfi_endproc
.LFE22948:
	.size	_ZN2v88internal10PagedSpace6ExpandEv, .-_ZN2v88internal10PagedSpace6ExpandEv
	.section	.text._ZN2v88internal10PagedSpace33RawSlowRefillLinearAllocationAreaEiNS0_16AllocationOriginE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace33RawSlowRefillLinearAllocationAreaEiNS0_16AllocationOriginE
	.type	_ZN2v88internal10PagedSpace33RawSlowRefillLinearAllocationAreaEiNS0_16AllocationOriginE, @function
_ZN2v88internal10PagedSpace33RawSlowRefillLinearAllocationAreaEiNS0_16AllocationOriginE:
.LFB23104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movslq	%esi, %r15
	pushq	%r14
	movq	%r15, %rsi
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	_ZN2v88internal10PagedSpace38RefillLinearAllocationAreaFromFreeListEmNS0_16AllocationOriginE
	testb	%al, %al
	jne	.L4360
	movq	64(%r12), %rax
	movq	%r15, %r14
	movq	2016(%rax), %rbx
	movq	(%r12), %rax
	movq	9984(%rbx), %rdx
	cmpb	$0, 265(%rdx)
	je	.L4361
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_sweepingE(%rip)
	je	.L4362
	movq	%r12, %rdi
	call	*152(%rax)
	testb	%al, %al
	je	.L4363
.L4377:
	movq	(%r12), %rax
.L4362:
	movq	%r12, %rdi
	call	*160(%rax)
	movl	%r13d, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10PagedSpace38RefillLinearAllocationAreaFromFreeListEmNS0_16AllocationOriginE
	testb	%al, %al
	jne	.L4360
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*152(%rax)
	movq	9984(%rbx), %rdi
	movl	72(%r12), %esi
	movl	%r14d, %edx
	xorl	$1, %eax
	movl	$1, %ecx
	movzbl	%al, %r8d
	call	_ZN2v88internal7Sweeper18ParallelSweepSpaceENS0_15AllocationSpaceEiiNS1_35FreeSpaceMayContainInvalidatedSlotsE@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	movq	(%r12), %rax
	call	*160(%rax)
	cmpl	%ebx, %r14d
	jle	.L4366
.L4378:
	movq	(%r12), %rax
.L4361:
	movq	%r12, %rdi
	call	*152(%rax)
	testb	%al, %al
	je	.L4368
	movq	64(%r12), %rax
	movslq	72(%r12), %rdx
	movq	312(%rax,%rdx,8), %r8
	leaq	192(%r8), %r9
	movq	%r8, -56(%rbp)
	movq	%r9, %rdi
	movq	%r9, -64(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-56(%rbp), %r8
	movq	%r15, %rsi
	movq	96(%r8), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L4379
	movq	%rax, %rsi
	movq	%r8, %rdi
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal10PagedSpace10RemovePageEPNS0_4PageE
	movq	-56(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10PagedSpace7AddPageEPNS0_4PageE
	movl	%r13d, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10PagedSpace38RefillLinearAllocationAreaFromFreeListEmNS0_16AllocationOriginE
	testb	%al, %al
	jne	.L4360
.L4368:
	movq	64(%r12), %rdi
	call	_ZN2v88internal4Heap41ShouldExpandOldGenerationOnSlowAllocationEv@PLT
	testb	%al, %al
	jne	.L4371
.L4372:
	movq	(%r12), %rax
	movl	%r13d, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movq	176(%rax), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L4366:
	.cfi_restore_state
	movl	%r13d, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10PagedSpace38RefillLinearAllocationAreaFromFreeListEmNS0_16AllocationOriginE
	testb	%al, %al
	je	.L4378
.L4360:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4371:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal10PagedSpace6ExpandEv
	testb	%al, %al
	je	.L4372
	addq	$24, %rsp
	movl	%r13d, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10PagedSpace38RefillLinearAllocationAreaFromFreeListEmNS0_16AllocationOriginE
	.p2align 4,,10
	.p2align 3
.L4363:
	.cfi_restore_state
	movq	9984(%rbx), %rdi
	call	_ZN2v88internal7Sweeper22AreSweeperTasksRunningEv@PLT
	testb	%al, %al
	jne	.L4377
	movq	%rbx, %rdi
	call	_ZN2v88internal20MarkCompactCollector23EnsureSweepingCompletedEv@PLT
	jmp	.L4377
	.p2align 4,,10
	.p2align 3
.L4379:
	movq	%r9, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L4368
	.cfi_endproc
.LFE23104:
	.size	_ZN2v88internal10PagedSpace33RawSlowRefillLinearAllocationAreaEiNS0_16AllocationOriginE, .-_ZN2v88internal10PagedSpace33RawSlowRefillLinearAllocationAreaEiNS0_16AllocationOriginE
	.section	.text._ZN2v88internal10PagedSpace30SlowRefillLinearAllocationAreaEiNS0_16AllocationOriginE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10PagedSpace30SlowRefillLinearAllocationAreaEiNS0_16AllocationOriginE
	.type	_ZN2v88internal10PagedSpace30SlowRefillLinearAllocationAreaEiNS0_16AllocationOriginE, @function
_ZN2v88internal10PagedSpace30SlowRefillLinearAllocationAreaEiNS0_16AllocationOriginE:
.LFB23102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	64(%rdi), %rax
	movq	$0, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	leaq	-37592(%rax), %rbx
	movl	12616(%rbx), %r15d
	movaps	%xmm0, -80(%rbp)
	movl	$1, 12616(%rbx)
	movq	64(%rdi), %rdx
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4391
.L4381:
	movq	%r12, %rdi
	movl	%r14d, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal10PagedSpace33RawSlowRefillLinearAllocationAreaEiNS0_16AllocationOriginE
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L4392
.L4382:
	movl	%r15d, 12616(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4393
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4391:
	.cfi_restore_state
	movq	3368(%rdx), %rdi
	leaq	-88(%rbp), %rsi
	movl	$149, %edx
	addq	$23240, %rdi
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L4381
	.p2align 4,,10
	.p2align 3
.L4392:
	leaq	-88(%rbp), %rsi
	movb	%al, -97(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movzbl	-97(%rbp), %eax
	jmp	.L4382
.L4393:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23102:
	.size	_ZN2v88internal10PagedSpace30SlowRefillLinearAllocationAreaEiNS0_16AllocationOriginE, .-_ZN2v88internal10PagedSpace30SlowRefillLinearAllocationAreaEiNS0_16AllocationOriginE
	.section	.text._ZN2v88internal15CompactionSpace30SlowRefillLinearAllocationAreaEiNS0_16AllocationOriginE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15CompactionSpace30SlowRefillLinearAllocationAreaEiNS0_16AllocationOriginE
	.type	_ZN2v88internal15CompactionSpace30SlowRefillLinearAllocationAreaEiNS0_16AllocationOriginE, @function
_ZN2v88internal15CompactionSpace30SlowRefillLinearAllocationAreaEiNS0_16AllocationOriginE:
.LFB23103:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal10PagedSpace33RawSlowRefillLinearAllocationAreaEiNS0_16AllocationOriginE
	.cfi_endproc
.LFE23103:
	.size	_ZN2v88internal15CompactionSpace30SlowRefillLinearAllocationAreaEiNS0_16AllocationOriginE, .-_ZN2v88internal15CompactionSpace30SlowRefillLinearAllocationAreaEiNS0_16AllocationOriginE
	.section	.text._ZN2v88internal15MemoryAllocator12AllocatePageILNS1_14AllocationModeE0ENS0_9SemiSpaceEEEPNS0_4PageEmPT0_NS0_13ExecutabilityE,"axG",@progbits,_ZN2v88internal15MemoryAllocator12AllocatePageILNS1_14AllocationModeE0ENS0_9SemiSpaceEEEPNS0_4PageEmPT0_NS0_13ExecutabilityE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15MemoryAllocator12AllocatePageILNS1_14AllocationModeE0ENS0_9SemiSpaceEEEPNS0_4PageEmPT0_NS0_13ExecutabilityE
	.type	_ZN2v88internal15MemoryAllocator12AllocatePageILNS1_14AllocationModeE0ENS0_9SemiSpaceEEEPNS0_4PageEmPT0_NS0_13ExecutabilityE, @function
_ZN2v88internal15MemoryAllocator12AllocatePageILNS1_14AllocationModeE0ENS0_9SemiSpaceEEEPNS0_4PageEmPT0_NS0_13ExecutabilityE:
.LFB25882:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	movq	%rsi, %rdx
	call	_ZN2v88internal15MemoryAllocator13AllocateChunkEmmNS0_13ExecutabilityEPNS0_5SpaceE
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L4395
	cmpl	$1, 140(%rbx)
	sbbq	%rax, %rax
	andq	$-8, %rax
	addq	$16, %rax
	orq	8(%r12), %rax
	movq	%rax, 8(%r12)
	movq	64(%rbx), %rdx
	movq	2064(%rdx), %rdx
	cmpl	$1, 80(%rdx)
	jg	.L4406
	andq	$-262149, %rax
	orq	$2, %rax
.L4399:
	movq	%rax, 8(%r12)
	movl	$64, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	cmpb	$0, _ZN2v88internal13FLAG_minor_mcE(%rip)
	leaq	56(%rax), %rdx
	movq	%r12, (%rax)
	movq	%rdx, 8(%rax)
	movq	$1, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movl	$0x3f800000, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	%rax, 248(%r12)
	movups	%xmm0, 224(%r12)
	jne	.L4407
.L4400:
	mfence
.L4395:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4406:
	.cfi_restore_state
	orq	$262150, %rax
	jmp	.L4399
	.p2align 4,,10
	.p2align 3
.L4407:
	movl	$1, %edi
	movl	$4096, %esi
	call	calloc@PLT
	movl	$512, %ecx
	movq	%rax, %rdx
	movq	%rax, 264(%r12)
	xorl	%eax, %eax
	movq	%rdx, %rdi
	rep stosq
	movq	$0, 256(%r12)
	jmp	.L4400
	.cfi_endproc
.LFE25882:
	.size	_ZN2v88internal15MemoryAllocator12AllocatePageILNS1_14AllocationModeE0ENS0_9SemiSpaceEEEPNS0_4PageEmPT0_NS0_13ExecutabilityE, .-_ZN2v88internal15MemoryAllocator12AllocatePageILNS1_14AllocationModeE0ENS0_9SemiSpaceEEEPNS0_4PageEmPT0_NS0_13ExecutabilityE
	.section	.text._ZN2v88internal15MemoryAllocator12AllocatePageILNS1_14AllocationModeE1ENS0_9SemiSpaceEEEPNS0_4PageEmPT0_NS0_13ExecutabilityE,"axG",@progbits,_ZN2v88internal15MemoryAllocator12AllocatePageILNS1_14AllocationModeE1ENS0_9SemiSpaceEEEPNS0_4PageEmPT0_NS0_13ExecutabilityE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15MemoryAllocator12AllocatePageILNS1_14AllocationModeE1ENS0_9SemiSpaceEEEPNS0_4PageEmPT0_NS0_13ExecutabilityE
	.type	_ZN2v88internal15MemoryAllocator12AllocatePageILNS1_14AllocationModeE1ENS0_9SemiSpaceEEEPNS0_4PageEmPT0_NS0_13ExecutabilityE, @function
_ZN2v88internal15MemoryAllocator12AllocatePageILNS1_14AllocationModeE1ENS0_9SemiSpaceEEEPNS0_4PageEmPT0_NS0_13ExecutabilityE:
.LFB25883:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdx, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal15MemoryAllocator18AllocatePagePooledINS0_9SemiSpaceEEEPNS0_11MemoryChunkEPT_
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L4421
.L4409:
	cmpl	$1, 140(%rbx)
	sbbq	%rax, %rax
	andq	$-8, %rax
	addq	$16, %rax
	orq	8(%r12), %rax
	movq	%rax, 8(%r12)
	movq	64(%rbx), %rdx
	movq	2064(%rdx), %rdx
	cmpl	$1, 80(%rdx)
	jg	.L4422
	andq	$-262149, %rax
	orq	$2, %rax
.L4413:
	movq	%rax, 8(%r12)
	movl	$64, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	cmpb	$0, _ZN2v88internal13FLAG_minor_mcE(%rip)
	leaq	56(%rax), %rdx
	movq	%r12, (%rax)
	movq	%rdx, 8(%rax)
	movq	$1, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movl	$0x3f800000, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	%rax, 248(%r12)
	movups	%xmm0, 224(%r12)
	jne	.L4423
.L4414:
	mfence
.L4408:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4422:
	.cfi_restore_state
	orq	$262150, %rax
	jmp	.L4413
	.p2align 4,,10
	.p2align 3
.L4423:
	movl	$1, %edi
	movl	$4096, %esi
	call	calloc@PLT
	movl	$512, %ecx
	movq	%rax, %rdx
	movq	%rax, 264(%r12)
	xorl	%eax, %eax
	movq	%rdx, %rdi
	rep stosq
	movq	$0, 256(%r12)
	jmp	.L4414
	.p2align 4,,10
	.p2align 3
.L4421:
	movq	%rbx, %r8
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal15MemoryAllocator13AllocateChunkEmmNS0_13ExecutabilityEPNS0_5SpaceE
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L4408
	jmp	.L4409
	.cfi_endproc
.LFE25883:
	.size	_ZN2v88internal15MemoryAllocator12AllocatePageILNS1_14AllocationModeE1ENS0_9SemiSpaceEEEPNS0_4PageEmPT0_NS0_13ExecutabilityE, .-_ZN2v88internal15MemoryAllocator12AllocatePageILNS1_14AllocationModeE1ENS0_9SemiSpaceEEEPNS0_4PageEmPT0_NS0_13ExecutabilityE
	.section	.text._ZN2v88internal9SemiSpace21EnsureCurrentCapacityEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal9SemiSpace21EnsureCurrentCapacityEv.part.0, @function
_ZN2v88internal9SemiSpace21EnsureCurrentCapacityEv.part.0:
.LFB31364:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	104(%rdi), %r14
	movq	32(%rdi), %rdx
	shrq	$18, %r14
	movl	%r14d, %r12d
	testl	%r14d, %r14d
	jle	.L4444
	movq	%rdx, %rsi
	xorl	%ebx, %ebx
	testq	%rdx, %rdx
	jne	.L4425
	jmp	.L4444
	.p2align 4,,10
	.p2align 3
.L4463:
	cmpl	%ebx, %r12d
	jle	.L4429
.L4425:
	movq	224(%rsi), %rsi
	addl	$1, %ebx
	testq	%rsi, %rsi
	jne	.L4463
.L4427:
	cmpl	%ebx, %r14d
	jg	.L4430
	jmp	.L4440
	.p2align 4,,10
	.p2align 3
.L4464:
	movq	224(%rax), %rcx
	movq	%rax, 232(%rdx)
	movq	%rcx, 224(%rdx)
	movq	%rdx, 224(%rax)
	testq	%rcx, %rcx
	je	.L4438
	movq	%rdx, 232(%rcx)
.L4439:
	movq	16(%rdx), %rax
	movl	$1, %r8d
	leaq	8(%rax), %rdi
	movq	$0, (%rax)
	movq	$0, 4088(%rax)
	andq	$-8, %rdi
	subq	%rdi, %rax
	leal	4096(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	movq	$0, 96(%rdx)
	movl	$1, %ecx
	movq	32(%r13), %rax
	movq	40(%rdx), %rsi
	movq	8(%rax), %rax
	movq	%rax, 8(%rdx)
	movq	48(%rdx), %rdx
	movq	64(%r13), %rdi
	subq	%rsi, %rdx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	cmpl	%ebx, %r12d
	je	.L4440
.L4430:
	movq	64(%r13), %rax
	movq	%r13, %rdx
	xorl	%ecx, %ecx
	movl	$261864, %esi
	addl	$1, %ebx
	movq	2048(%rax), %rdi
	call	_ZN2v88internal15MemoryAllocator12AllocatePageILNS1_14AllocationModeE1ENS0_9SemiSpaceEEEPNS0_4PageEmPT0_NS0_13ExecutabilityE
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4443
	movq	40(%r13), %rax
	testq	%rax, %rax
	jne	.L4464
	movq	%rdx, %xmm0
	pxor	%xmm2, %xmm2
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm2, 224(%rdx)
	movups	%xmm0, 32(%r13)
	jmp	.L4439
	.p2align 4,,10
	.p2align 3
.L4438:
	movq	%rdx, 40(%r13)
	jmp	.L4439
.L4444:
	movq	%rdx, %rsi
	xorl	%ebx, %ebx
.L4429:
	testq	%rsi, %rsi
	jne	.L4428
	jmp	.L4427
	.p2align 4,,10
	.p2align 3
.L4432:
	cmpq	%rsi, %rdx
	je	.L4465
.L4433:
	movq	232(%rsi), %rdx
	testq	%rax, %rax
	je	.L4434
	movq	%rdx, 232(%rax)
.L4434:
	testq	%rdx, %rdx
	je	.L4435
	pxor	%xmm1, %xmm1
	movq	%rax, 224(%rdx)
	movups	%xmm1, 224(%rsi)
.L4462:
	andq	$-25, 8(%rsi)
	movq	64(%r13), %rax
	movq	2048(%rax), %rdi
	call	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE3EEEvPNS0_11MemoryChunkE
	testq	%r15, %r15
	je	.L4427
	movq	32(%r13), %rdx
	movq	%r15, %rsi
.L4428:
	movq	224(%rsi), %r15
	movq	%r15, %rax
	cmpq	%rsi, 40(%r13)
	jne	.L4432
	movq	232(%rsi), %rax
	movq	%rax, 40(%r13)
	movq	224(%rsi), %rax
	cmpq	%rsi, %rdx
	jne	.L4433
.L4465:
	movq	%rax, 32(%r13)
	movq	224(%rsi), %rax
	jmp	.L4433
	.p2align 4,,10
	.p2align 3
.L4435:
	pxor	%xmm3, %xmm3
	movups	%xmm3, 224(%rsi)
	jmp	.L4462
.L4440:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4443:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE31364:
	.size	_ZN2v88internal9SemiSpace21EnsureCurrentCapacityEv.part.0, .-_ZN2v88internal9SemiSpace21EnsureCurrentCapacityEv.part.0
	.section	.text._ZN2v88internal9SemiSpace21EnsureCurrentCapacityEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9SemiSpace21EnsureCurrentCapacityEv
	.type	_ZN2v88internal9SemiSpace21EnsureCurrentCapacityEv, @function
_ZN2v88internal9SemiSpace21EnsureCurrentCapacityEv:
.LFB22972:
	.cfi_startproc
	endbr64
	cmpb	$0, 136(%rdi)
	je	.L4467
	jmp	_ZN2v88internal9SemiSpace21EnsureCurrentCapacityEv.part.0
	.p2align 4,,10
	.p2align 3
.L4467:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE22972:
	.size	_ZN2v88internal9SemiSpace21EnsureCurrentCapacityEv, .-_ZN2v88internal9SemiSpace21EnsureCurrentCapacityEv
	.section	.text._ZN2v88internal8NewSpace9RebalanceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8NewSpace9RebalanceEv
	.type	_ZN2v88internal8NewSpace9RebalanceEv, @function
_ZN2v88internal8NewSpace9RebalanceEv:
.LFB22971:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 344(%rdi)
	je	.L4472
	leaq	208(%rdi), %rdi
	call	_ZN2v88internal9SemiSpace21EnsureCurrentCapacityEv.part.0
	testb	%al, %al
	je	.L4468
.L4472:
	cmpb	$0, 504(%rbx)
	movl	$1, %eax
	jne	.L4474
.L4468:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4474:
	.cfi_restore_state
	addq	$8, %rsp
	leaq	368(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9SemiSpace21EnsureCurrentCapacityEv.part.0
	.cfi_endproc
.LFE22971:
	.size	_ZN2v88internal8NewSpace9RebalanceEv, .-_ZN2v88internal8NewSpace9RebalanceEv
	.section	.text._ZN2v88internal9SemiSpace6CommitEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9SemiSpace6CommitEv
	.type	_ZN2v88internal9SemiSpace6CommitEv, @function
_ZN2v88internal9SemiSpace6CommitEv:
.LFB22998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	104(%rdi), %rax
	movq	%rax, %rdx
	shrq	$18, %rdx
	testl	%edx, %edx
	jle	.L4476
	movl	%edx, %r12d
	xorl	%r13d, %r13d
	jmp	.L4488
	.p2align 4,,10
	.p2align 3
.L4507:
	movq	224(%rdx), %rcx
	movq	%rdx, 232(%rax)
	movq	%rcx, 224(%rax)
	movq	%rax, 224(%rdx)
	testq	%rcx, %rcx
	je	.L4486
	movq	%rax, 232(%rcx)
.L4487:
	addl	$1, %r13d
	cmpl	%r13d, %r12d
	je	.L4505
.L4488:
	movq	64(%rbx), %rax
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movl	$261864, %esi
	movq	2048(%rax), %rdi
	call	_ZN2v88internal15MemoryAllocator12AllocatePageILNS1_14AllocationModeE1ENS0_9SemiSpaceEEEPNS0_4PageEmPT0_NS0_13ExecutabilityE
	testq	%rax, %rax
	je	.L4506
	movq	40(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L4507
	movq	%rax, %xmm0
	pxor	%xmm1, %xmm1
	addl	$1, %r13d
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm1, 224(%rax)
	movups	%xmm0, 32(%rbx)
	cmpl	%r13d, %r12d
	jne	.L4488
.L4505:
	movq	104(%rbx), %rax
.L4476:
	movq	32(%rbx), %rdx
	addq	80(%rbx), %rax
	movl	$0, 152(%rbx)
	movq	%rax, 80(%rbx)
	movq	%rdx, 144(%rbx)
	cmpq	88(%rbx), %rax
	jbe	.L4489
	movq	%rax, 88(%rbx)
.L4489:
	cmpq	$0, 128(%rbx)
	je	.L4508
.L4490:
	movb	$1, 136(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4486:
	.cfi_restore_state
	movq	%rax, 40(%rbx)
	jmp	.L4487
	.p2align 4,,10
	.p2align 3
.L4508:
	movq	40(%rdx), %rax
	movq	%rax, 128(%rbx)
	jmp	.L4490
	.p2align 4,,10
	.p2align 3
.L4506:
	testl	%r13d, %r13d
	jne	.L4478
.L4483:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4510:
	.cfi_restore_state
	pxor	%xmm2, %xmm2
	movq	%rdx, 224(%rax)
	movups	%xmm2, 224(%rsi)
.L4504:
	movq	64(%rbx), %rax
	movq	2048(%rax), %rdi
	call	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE3EEEvPNS0_11MemoryChunkE
	subl	$1, %r13d
	je	.L4483
.L4478:
	movq	40(%rbx), %rsi
	movq	232(%rsi), %rax
	movq	%rax, 40(%rbx)
	cmpq	32(%rbx), %rsi
	je	.L4509
.L4480:
	movq	224(%rsi), %rdx
	movq	232(%rsi), %rax
	testq	%rdx, %rdx
	je	.L4481
	movq	%rax, 232(%rdx)
.L4481:
	testq	%rax, %rax
	jne	.L4510
	pxor	%xmm3, %xmm3
	movups	%xmm3, 224(%rsi)
	jmp	.L4504
	.p2align 4,,10
	.p2align 3
.L4509:
	movq	224(%rsi), %rax
	movq	%rax, 32(%rbx)
	jmp	.L4480
	.cfi_endproc
.LFE22998:
	.size	_ZN2v88internal9SemiSpace6CommitEv, .-_ZN2v88internal9SemiSpace6CommitEv
	.section	.rodata._ZN2v88internal8NewSpaceC2EPNS0_4HeapEPNS_13PageAllocatorEmm.str1.1,"aMS",@progbits,1
.LC46:
	.string	"New space setup"
	.section	.text._ZN2v88internal8NewSpaceC2EPNS0_4HeapEPNS_13PageAllocatorEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8NewSpaceC2EPNS0_4HeapEPNS_13PageAllocatorEmm
	.type	_ZN2v88internal8NewSpaceC2EPNS0_4HeapEPNS_13PageAllocatorEmm, @function
_ZN2v88internal8NewSpaceC2EPNS0_4HeapEPNS_13PageAllocatorEmm:
.LFB22965:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16+_ZTVN2v88internal10NoFreeListE(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	16+_ZTVN2v88internal5SpaceE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	16+_ZTVN2v88internal9SemiSpaceE(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$48, %edi
	subq	$40, %rsp
	movq	%r8, -80(%rbp)
	movq	%rcx, -56(%rbp)
	movhps	-80(%rbp), %xmm1
	movaps	%xmm1, -80(%rbp)
	call	_Znwm@PLT
	movq	%r14, (%rbx)
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	$0, 8(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	%r15, (%rax)
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movb	$0, 56(%rbx)
	movq	%r13, 64(%rbx)
	movl	$1, 72(%rbx)
	movq	%rax, 96(%rbx)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 80(%rbx)
	call	_Znam@PLT
	pxor	%xmm2, %xmm2
	pxor	%xmm0, %xmm0
	leaq	152(%rbx), %rdi
	movq	%rax, 48(%rbx)
	movq	$0, (%rax)
	mfence
	movq	48(%rbx), %rax
	movq	$0, 8(%rax)
	leaq	16+_ZTVN2v88internal8NewSpaceE(%rip), %rax
	mfence
	movq	%rax, (%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 144(%rbx)
	movups	%xmm2, 128(%rbx)
	movups	%xmm0, 104(%rbx)
	call	_ZN2v84base5MutexC1Ev@PLT
	movl	$48, %edi
	call	_Znwm@PLT
	movq	%r14, 208(%rbx)
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	$0, 8(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	%r15, (%rax)
	movq	$0, 216(%rbx)
	movq	$0, 224(%rbx)
	movq	$0, 232(%rbx)
	movq	$0, 240(%rbx)
	movq	$0, 248(%rbx)
	movb	$0, 264(%rbx)
	movq	%r13, 272(%rbx)
	movl	$1, 280(%rbx)
	movq	%rax, 304(%rbx)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 288(%rbx)
	call	_Znam@PLT
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movq	%rax, 256(%rbx)
	movq	$0, (%rax)
	mfence
	movq	256(%rbx), %rax
	movq	$0, 8(%rax)
	mfence
	movq	%r12, 208(%rbx)
	movb	$0, 344(%rbx)
	movq	$1, 348(%rbx)
	movq	$0, 356(%rbx)
	movups	%xmm0, 312(%rbx)
	movups	%xmm0, 328(%rbx)
	call	_Znwm@PLT
	movq	%r14, 368(%rbx)
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	$0, 8(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	%r15, (%rax)
	movq	$0, 376(%rbx)
	movq	$0, 384(%rbx)
	movq	$0, 392(%rbx)
	movq	$0, 400(%rbx)
	movq	$0, 408(%rbx)
	movb	$0, 424(%rbx)
	movq	%r13, 432(%rbx)
	movl	$1, 440(%rbx)
	movq	%rax, 464(%rbx)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 448(%rbx)
	call	_Znam@PLT
	pxor	%xmm0, %xmm0
	leaq	208(%rbx), %rdi
	movq	%rax, 416(%rbx)
	movq	$0, (%rax)
	mfence
	movq	416(%rbx), %rax
	movq	$0, 8(%rax)
	mfence
	movq	%r12, 368(%rbx)
	movq	$0, 496(%rbx)
	movq	-56(%rbp), %r12
	movdqa	-80(%rbp), %xmm1
	movb	$0, 504(%rbx)
	pand	.LC45(%rip), %xmm1
	movq	$0, 508(%rbx)
	andq	$-262144, %r12
	movb	$0, 344(%rbx)
	movq	$0, 516(%rbx)
	movq	$0, 528(%rbx)
	movq	%r12, 328(%rbx)
	movq	%r12, 488(%rbx)
	movups	%xmm0, 536(%rbx)
	movups	%xmm1, 312(%rbx)
	movups	%xmm1, 472(%rbx)
	call	_ZN2v88internal9SemiSpace6CommitEv
	testb	%al, %al
	je	.L4529
	movq	64(%rbx), %rax
	cmpb	$0, 432(%rax)
	jne	.L4513
	movq	120(%rbx), %r12
	testq	%r12, %r12
	jne	.L4530
.L4513:
	movq	240(%rbx), %rax
	movq	%rbx, %rdi
	xorl	%r12d, %r12d
	movl	$0, 360(%rbx)
	movq	%rax, 352(%rbx)
	call	_ZN2v88internal8NewSpace26UpdateLinearAllocationAreaEv
	movq	240(%rbx), %r13
	testq	%r13, %r13
	je	.L4511
	.p2align 4,,10
	.p2align 3
.L4519:
	movq	16(%r13), %rcx
	movq	%r12, %rax
	movq	%r13, %rsi
	leaq	8(%rcx), %rdi
	movq	$0, (%rcx)
	movq	$0, 4088(%rcx)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$4096, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	$0, 96(%r13)
	movq	64(%rbx), %rax
	movq	2072(%rax), %rdi
	call	_ZN2v88internal17ConcurrentMarking20ClearMemoryChunkDataEPNS0_11MemoryChunkE@PLT
	movq	224(%r13), %r13
	testq	%r13, %r13
	jne	.L4519
.L4511:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4530:
	.cfi_restore_state
	movq	104(%rbx), %r13
	cmpq	%r12, %r13
	jnb	.L4514
	movq	%r13, 120(%rbx)
	movq	%r13, %r12
.L4514:
	cmpb	$0, 56(%rbx)
	jne	.L4515
	movq	8(%rbx), %rdx
	cmpq	%rdx, 16(%rbx)
	je	.L4515
	movb	$1, 432(%rax)
	movq	64(%rbx), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$1, %r8d
	movl	$1, %ecx
	movl	%r13d, %r15d
	subl	%r12d, %r15d
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	16(%rbx), %r14
	movq	8(%rbx), %r12
	cmpq	%r14, %r12
	je	.L4518
	.p2align 4,,10
	.p2align 3
.L4517:
	movq	(%r12), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r15d, %esi
	addq	$8, %r12
	call	_ZN2v88internal18AllocationObserver14AllocationStepEimm@PLT
	cmpq	%r12, %r14
	jne	.L4517
.L4518:
	movq	64(%rbx), %rax
	movb	$0, 432(%rax)
.L4515:
	movq	%r13, 120(%rbx)
	jmp	.L4513
.L4529:
	leaq	-37592(%r13), %rdi
	xorl	%edx, %edx
	leaq	.LC46(%rip), %rsi
	call	_ZN2v88internal2V823FatalProcessOutOfMemoryEPNS0_7IsolateEPKcb@PLT
	.cfi_endproc
.LFE22965:
	.size	_ZN2v88internal8NewSpaceC2EPNS0_4HeapEPNS_13PageAllocatorEmm, .-_ZN2v88internal8NewSpaceC2EPNS0_4HeapEPNS_13PageAllocatorEmm
	.globl	_ZN2v88internal8NewSpaceC1EPNS0_4HeapEPNS_13PageAllocatorEmm
	.set	_ZN2v88internal8NewSpaceC1EPNS0_4HeapEPNS_13PageAllocatorEmm,_ZN2v88internal8NewSpaceC2EPNS0_4HeapEPNS_13PageAllocatorEmm
	.section	.text._ZN2v88internal9SemiSpace6GrowToEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9SemiSpace6GrowToEm
	.type	_ZN2v88internal9SemiSpace6GrowToEm, @function
_ZN2v88internal9SemiSpace6GrowToEm:
.LFB23001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpb	$0, 136(%rdi)
	je	.L4532
.L4535:
	movq	%r12, %r13
	subq	104(%r15), %r13
	movq	%r13, %rax
	shrq	$18, %rax
	movl	%eax, %ebx
	testl	%eax, %eax
	jle	.L4533
	xorl	%r14d, %r14d
	jmp	.L4534
	.p2align 4,,10
	.p2align 3
.L4565:
	movq	224(%rax), %rcx
	movq	%rax, 232(%rdx)
	movq	%rcx, 224(%rdx)
	movq	%rdx, 224(%rax)
	testq	%rcx, %rcx
	je	.L4545
	movq	%rdx, 232(%rcx)
.L4546:
	movq	16(%rdx), %rax
	addl	$1, %r14d
	leaq	8(%rax), %rdi
	movq	$0, (%rax)
	movq	$0, 4088(%rax)
	andq	$-8, %rdi
	subq	%rdi, %rax
	leal	4096(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	movq	$0, 96(%rdx)
	movq	40(%r15), %rax
	movq	8(%rdx), %rcx
	movq	8(%rax), %rsi
	xorq	%rcx, %rsi
	movq	%rsi, %rax
	andl	$262150, %eax
	xorq	%rcx, %rax
	movq	%rax, 8(%rdx)
	cmpl	%r14d, %ebx
	je	.L4533
.L4534:
	movq	64(%r15), %rax
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movl	$261864, %esi
	movq	2048(%rax), %rdi
	call	_ZN2v88internal15MemoryAllocator12AllocatePageILNS1_14AllocationModeE1ENS0_9SemiSpaceEEEPNS0_4PageEmPT0_NS0_13ExecutabilityE
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4564
	movq	40(%r15), %rax
	testq	%rax, %rax
	jne	.L4565
	movq	%rdx, %xmm0
	pxor	%xmm1, %xmm1
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm1, 224(%rdx)
	movups	%xmm0, 32(%r15)
	jmp	.L4546
	.p2align 4,,10
	.p2align 3
.L4545:
	movq	%rdx, 40(%r15)
	jmp	.L4546
.L4533:
	addq	80(%r15), %r13
	movq	%r13, 80(%r15)
	cmpq	88(%r15), %r13
	jbe	.L4547
	movq	%r13, 88(%r15)
.L4547:
	movq	%r12, 104(%r15)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4564:
	.cfi_restore_state
	testl	%r14d, %r14d
	jne	.L4543
.L4538:
	xorl	%eax, %eax
.L4567:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4566:
	.cfi_restore_state
	movq	224(%rsi), %rax
	movq	%rax, 32(%r15)
.L4539:
	movq	224(%rsi), %rdx
	movq	232(%rsi), %rax
	testq	%rdx, %rdx
	je	.L4540
	movq	%rax, 232(%rdx)
.L4540:
	testq	%rax, %rax
	je	.L4541
	pxor	%xmm2, %xmm2
	movq	%rdx, 224(%rax)
	movups	%xmm2, 224(%rsi)
.L4563:
	movq	64(%r15), %rax
	movq	2048(%rax), %rdi
	call	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE3EEEvPNS0_11MemoryChunkE
	subl	$1, %r14d
	je	.L4538
.L4543:
	movq	40(%r15), %rsi
	movq	232(%rsi), %rax
	movq	%rax, 40(%r15)
	cmpq	32(%r15), %rsi
	jne	.L4539
	jmp	.L4566
.L4541:
	pxor	%xmm3, %xmm3
	movups	%xmm3, 224(%rsi)
	jmp	.L4563
.L4532:
	call	_ZN2v88internal9SemiSpace6CommitEv
	testb	%al, %al
	jne	.L4535
	xorl	%eax, %eax
	jmp	.L4567
	.cfi_endproc
.LFE23001:
	.size	_ZN2v88internal9SemiSpace6GrowToEm, .-_ZN2v88internal9SemiSpace6GrowToEm
	.section	.text._ZN2v88internal8NewSpace4GrowEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8NewSpace4GrowEv
	.type	_ZN2v88internal8NewSpace4GrowEv, @function
_ZN2v88internal8NewSpace4GrowEv:
.LFB22969:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	_ZN2v88internal29FLAG_semi_space_growth_factorE(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	imulq	312(%rdi), %rsi
	movq	%rdi, %rbx
	movq	320(%rdi), %r12
	cmpq	%r12, %rsi
	cmovbe	%rsi, %r12
	addq	$208, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal9SemiSpace6GrowToEm
	testb	%al, %al
	jne	.L4591
.L4568:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4591:
	.cfi_restore_state
	leaq	368(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal9SemiSpace6GrowToEm
	testb	%al, %al
	jne	.L4568
	cmpb	$0, 344(%rbx)
	movq	472(%rbx), %r13
	jne	.L4592
.L4572:
	movq	%r13, 312(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4592:
	.cfi_restore_state
	movq	312(%rbx), %r14
	movq	%r14, %rax
	subq	%r13, %rax
	shrq	$18, %rax
	movl	%eax, %r12d
	testl	%eax, %eax
	jg	.L4578
	jmp	.L4573
	.p2align 4,,10
	.p2align 3
.L4574:
	movq	224(%rsi), %rdx
	movq	232(%rsi), %rax
	testq	%rdx, %rdx
	je	.L4575
	movq	%rax, 232(%rdx)
.L4575:
	testq	%rax, %rax
	je	.L4576
	pxor	%xmm0, %xmm0
	movq	%rdx, 224(%rax)
	movups	%xmm0, 224(%rsi)
.L4590:
	movq	272(%rbx), %rax
	movq	2048(%rax), %rdi
	call	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE3EEEvPNS0_11MemoryChunkE
	subl	$1, %r12d
	je	.L4573
.L4578:
	movq	248(%rbx), %rsi
	movq	232(%rsi), %rax
	movq	%rax, 248(%rbx)
	cmpq	240(%rbx), %rsi
	jne	.L4574
	movq	224(%rsi), %rax
	movq	%rax, 240(%rbx)
	jmp	.L4574
	.p2align 4,,10
	.p2align 3
.L4576:
	pxor	%xmm1, %xmm1
	movups	%xmm1, 224(%rsi)
	jmp	.L4590
	.p2align 4,,10
	.p2align 3
.L4573:
	movq	%r13, %rax
	subq	%r14, %rax
	addq	%rax, 288(%rbx)
	movq	272(%rbx), %rax
	movq	2048(%rax), %rdi
	addq	$136, %rdi
	call	_ZN2v88internal15MemoryAllocator8Unmapper16FreeQueuedChunksEv
	jmp	.L4572
	.cfi_endproc
.LFE22969:
	.size	_ZN2v88internal8NewSpace4GrowEv, .-_ZN2v88internal8NewSpace4GrowEv
	.section	.rodata._ZN2v88internal8NewSpace6ShrinkEv.str1.1,"aMS",@progbits,1
.LC47:
	.string	"inconsistent state"
	.section	.text._ZN2v88internal8NewSpace6ShrinkEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8NewSpace6ShrinkEv
	.type	_ZN2v88internal8NewSpace6ShrinkEv, @function
_ZN2v88internal8NewSpace6ShrinkEv:
.LFB22970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal8NewSpace4SizeEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4594
	movslq	360(%rdi), %rax
	movq	352(%rdi), %rdx
	imulq	$261864, %rax, %rax
	subq	40(%rdx), %rax
	addq	104(%rdi), %rax
.L4595:
	movq	328(%rbx), %r12
	addq	%rax, %rax
	movq	312(%rbx), %r14
	cmpq	%r12, %rax
	cmovnb	%rax, %r12
	addq	$262143, %r12
	andq	$-262144, %r12
	cmpq	%r12, %r14
	ja	.L4616
.L4593:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4616:
	.cfi_restore_state
	cmpb	$0, 344(%rbx)
	jne	.L4617
.L4597:
	movq	400(%rbx), %rax
	movq	%r12, 312(%rbx)
	movq	%r12, %rsi
	leaq	368(%rbx), %rdi
	movl	$0, 520(%rbx)
	movq	%rax, 512(%rbx)
	call	_ZN2v88internal9SemiSpace8ShrinkToEm
	testb	%al, %al
	jne	.L4593
	movq	472(%rbx), %rsi
	leaq	208(%rbx), %rdi
	call	_ZN2v88internal9SemiSpace6GrowToEm
	testb	%al, %al
	jne	.L4593
	leaq	.LC47(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4594:
	call	*%rax
	jmp	.L4595
	.p2align 4,,10
	.p2align 3
.L4617:
	movq	%r14, %rax
	subq	%r12, %rax
	shrq	$18, %rax
	movl	%eax, %r13d
	testl	%eax, %eax
	jg	.L4603
	jmp	.L4598
	.p2align 4,,10
	.p2align 3
.L4599:
	movq	224(%rsi), %rdx
	movq	232(%rsi), %rax
	testq	%rdx, %rdx
	je	.L4600
	movq	%rax, 232(%rdx)
.L4600:
	testq	%rax, %rax
	je	.L4601
	pxor	%xmm0, %xmm0
	movq	%rdx, 224(%rax)
	movups	%xmm0, 224(%rsi)
.L4615:
	movq	272(%rbx), %rax
	movq	2048(%rax), %rdi
	call	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE3EEEvPNS0_11MemoryChunkE
	subl	$1, %r13d
	je	.L4598
.L4603:
	movq	248(%rbx), %rsi
	movq	232(%rsi), %rax
	movq	%rax, 248(%rbx)
	cmpq	240(%rbx), %rsi
	jne	.L4599
	movq	224(%rsi), %rax
	movq	%rax, 240(%rbx)
	jmp	.L4599
	.p2align 4,,10
	.p2align 3
.L4601:
	pxor	%xmm1, %xmm1
	movups	%xmm1, 224(%rsi)
	jmp	.L4615
	.p2align 4,,10
	.p2align 3
.L4598:
	movq	288(%rbx), %rax
	subq	%r14, %rax
	addq	%r12, %rax
	movq	%rax, 288(%rbx)
	movq	272(%rbx), %rax
	movq	2048(%rax), %rdi
	addq	$136, %rdi
	call	_ZN2v88internal15MemoryAllocator8Unmapper16FreeQueuedChunksEv
	jmp	.L4597
	.cfi_endproc
.LFE22970:
	.size	_ZN2v88internal8NewSpace6ShrinkEv, .-_ZN2v88internal8NewSpace6ShrinkEv
	.section	.text._ZN2v88internal15MemoryAllocator17AllocateLargePageEmPNS0_16LargeObjectSpaceENS0_13ExecutabilityE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MemoryAllocator17AllocateLargePageEmPNS0_16LargeObjectSpaceENS0_13ExecutabilityE
	.type	_ZN2v88internal15MemoryAllocator17AllocateLargePageEmPNS0_16LargeObjectSpaceENS0_13ExecutabilityE, @function
_ZN2v88internal15MemoryAllocator17AllocateLargePageEmPNS0_16LargeObjectSpaceENS0_13ExecutabilityE:
.LFB22898:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%ecx, %ebx
	subq	$8, %rsp
	call	_ZN2v88internal15MemoryAllocator13AllocateChunkEmmNS0_13ExecutabilityEPNS0_5SpaceE
	testq	%rax, %rax
	je	.L4618
	testl	%ebx, %ebx
	jne	.L4631
.L4620:
	leaq	262176(%rax), %rdx
	cmpq	48(%rax), %rdx
	jnb	.L4621
	.p2align 4,,10
	.p2align 3
.L4622:
	movq	$0, (%rdx)
	addq	$262144, %rdx
	cmpq	%rdx, 48(%rax)
	ja	.L4622
.L4621:
	orq	$32, 8(%rax)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 224(%rax)
.L4618:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4631:
	.cfi_restore_state
	cmpq	$536870912, (%rax)
	jbe	.L4620
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22898:
	.size	_ZN2v88internal15MemoryAllocator17AllocateLargePageEmPNS0_16LargeObjectSpaceENS0_13ExecutabilityE, .-_ZN2v88internal15MemoryAllocator17AllocateLargePageEmPNS0_16LargeObjectSpaceENS0_13ExecutabilityE
	.section	.text._ZN2v88internal16LargeObjectSpace17AllocateLargePageEiNS0_13ExecutabilityE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16LargeObjectSpace17AllocateLargePageEiNS0_13ExecutabilityE
	.type	_ZN2v88internal16LargeObjectSpace17AllocateLargePageEiNS0_13ExecutabilityE, @function
_ZN2v88internal16LargeObjectSpace17AllocateLargePageEiNS0_13ExecutabilityE:
.LFB23142:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movslq	%esi, %r14
	movq	%r15, %r8
	pushq	%r13
	movq	%r14, %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	movq	%r14, %rdx
	subq	$8, %rsp
	movq	64(%rdi), %rax
	movq	2048(%rax), %rdi
	call	_ZN2v88internal15MemoryAllocator13AllocateChunkEmmNS0_13ExecutabilityEPNS0_5SpaceE
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L4632
	movq	%r14, %r13
	testl	%ebx, %ebx
	jne	.L4645
.L4634:
	leaq	262176(%r12), %rax
	cmpq	%rax, 48(%r12)
	jbe	.L4635
	.p2align 4,,10
	.p2align 3
.L4636:
	movq	$0, (%rax)
	addq	$262144, %rax
	cmpq	%rax, 48(%r12)
	ja	.L4636
.L4635:
	pxor	%xmm0, %xmm0
	orq	$32, 8(%r12)
	movq	%r14, %rdx
	movq	%r12, %rsi
	movups	%xmm0, 224(%r12)
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*128(%rax)
	movq	40(%r12), %rsi
	movq	64(%r15), %rdi
	movl	$1, %r8d
	movl	$1, %ecx
	movl	%r13d, %edx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
.L4632:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4645:
	.cfi_restore_state
	cmpq	$536870912, (%rax)
	jbe	.L4634
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23142:
	.size	_ZN2v88internal16LargeObjectSpace17AllocateLargePageEiNS0_13ExecutabilityE, .-_ZN2v88internal16LargeObjectSpace17AllocateLargePageEiNS0_13ExecutabilityE
	.section	.rodata._ZN2v88internal16LargeObjectSpace11AllocateRawEiNS0_13ExecutabilityE.part.0.str1.1,"aMS",@progbits,1
.LC48:
	.string	"!object.IsSmi()"
	.section	.text._ZN2v88internal16LargeObjectSpace11AllocateRawEiNS0_13ExecutabilityE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal16LargeObjectSpace11AllocateRawEiNS0_13ExecutabilityE.part.0, @function
_ZN2v88internal16LargeObjectSpace11AllocateRawEiNS0_13ExecutabilityE.part.0:
.LFB31366:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal16LargeObjectSpace17AllocateLargePageEiNS0_13ExecutabilityE
	testq	%rax, %rax
	je	.L4676
	movq	64(%r13), %rcx
	movq	8(%rax), %rdx
	movq	2064(%rcx), %rcx
	cmpl	$1, 80(%rcx)
	jg	.L4677
	andq	$-262147, %rdx
	orq	$4, %rdx
.L4650:
	movq	%rdx, 8(%rax)
	movq	40(%rax), %r12
	movq	64(%r13), %r14
	leaq	1(%r12), %r15
	movq	%r14, %rdi
	movq	%r15, -80(%rbp)
	call	_ZN2v88internal4Heap28ShouldOptimizeForMemoryUsageEv@PLT
	movl	$64, %edx
	movq	%r14, %rdi
	movzbl	%al, %esi
	call	_ZN2v88internal4Heap49StartIncrementalMarkingIfAllocationLimitIsReachedEiNS_15GCCallbackFlagsE@PLT
	movq	64(%r13), %rax
	movq	2064(%rax), %rax
	cmpb	$0, 87(%rax)
	je	.L4652
	movl	%r15d, %eax
	movq	%r15, %r14
	movl	$1, %edx
	andl	$262143, %eax
	andq	$-262144, %r14
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movl	%edx, %ecx
	movq	16(%r14), %rdx
	leaq	(%rdx,%rax,4), %rsi
	jmp	.L4654
	.p2align 4,,10
	.p2align 3
.L4679:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L4678
.L4654:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L4679
.L4652:
	mfence
	movq	64(%r13), %rdi
	call	_ZN2v88internal4Heap28NotifyOldGenerationExpansionEv@PLT
	cmpb	$0, 56(%r13)
	jne	.L4660
	movq	8(%r13), %rax
	cmpq	%rax, 16(%r13)
	je	.L4660
	movq	64(%r13), %rax
	movl	$1, %r8d
	movl	%ebx, %edx
	movq	%r12, %rsi
	movl	$1, %ecx
	movslq	%ebx, %r15
	movb	$1, 432(%rax)
	movq	64(%r13), %rdi
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	16(%r13), %rax
	movq	8(%r13), %r14
	movq	%rax, -72(%rbp)
	cmpq	%rax, %r14
	je	.L4663
	.p2align 4,,10
	.p2align 3
.L4662:
	movq	(%r14), %rdi
	movq	%r15, %rcx
	movq	%r12, %rdx
	movl	%ebx, %esi
	addq	$8, %r14
	call	_ZN2v88internal18AllocationObserver14AllocationStepEimm@PLT
	cmpq	%r14, -72(%rbp)
	jne	.L4662
.L4663:
	movq	64(%r13), %rax
	movb	$0, 432(%rax)
.L4660:
	movq	-80(%rbp), %rbx
	movq	%rbx, %rax
	andl	$1, %ebx
	je	.L4680
.L4648:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L4681
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4677:
	.cfi_restore_state
	orq	$262150, %rdx
	jmp	.L4650
	.p2align 4,,10
	.p2align 3
.L4676:
	movslq	72(%r13), %rax
	salq	$32, %rax
	jmp	.L4648
	.p2align 4,,10
	.p2align 3
.L4678:
	movl	%r12d, %edx
	movq	-80(%rbp), %rax
	subl	%r14d, %edx
	movl	%edx, %ecx
	movq	%rax, -64(%rbp)
	movl	$1, %eax
	shrl	$8, %edx
	shrl	$3, %ecx
	sall	%cl, %eax
	movl	%eax, %ecx
	movq	16(%r14), %rax
	leaq	(%rax,%rdx,4), %rsi
	movl	(%rsi), %eax
	testl	%eax, %ecx
	je	.L4652
	addl	%ecx, %ecx
	jne	.L4658
	addq	$4, %rsi
	movl	$1, %ecx
	jmp	.L4658
	.p2align 4,,10
	.p2align 3
.L4683:
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L4682
.L4658:
	movl	(%rsi), %edx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %ecx
	jne	.L4683
	jmp	.L4652
	.p2align 4,,10
	.p2align 3
.L4680:
	leaq	.LC48(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4682:
	movq	-64(%rbp), %rax
	leaq	-64(%rbp), %rdi
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, 96(%r14)
	jmp	.L4652
.L4681:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE31366:
	.size	_ZN2v88internal16LargeObjectSpace11AllocateRawEiNS0_13ExecutabilityE.part.0, .-_ZN2v88internal16LargeObjectSpace11AllocateRawEiNS0_13ExecutabilityE.part.0
	.section	.text._ZN2v88internal16LargeObjectSpace11AllocateRawEiNS0_13ExecutabilityE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16LargeObjectSpace11AllocateRawEiNS0_13ExecutabilityE
	.type	_ZN2v88internal16LargeObjectSpace11AllocateRawEiNS0_13ExecutabilityE, @function
_ZN2v88internal16LargeObjectSpace11AllocateRawEiNS0_13ExecutabilityE:
.LFB23141:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	64(%rdi), %rdi
	call	_ZN2v88internal4Heap22CanExpandOldGenerationEm@PLT
	testb	%al, %al
	jne	.L4685
.L4687:
	movslq	72(%r12), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	salq	$32, %rax
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4685:
	.cfi_restore_state
	movq	64(%r12), %rdi
	call	_ZN2v88internal4Heap41ShouldExpandOldGenerationOnSlowAllocationEv@PLT
	testb	%al, %al
	je	.L4687
	addq	$8, %rsp
	movl	%r14d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal16LargeObjectSpace11AllocateRawEiNS0_13ExecutabilityE.part.0
	.cfi_endproc
.LFE23141:
	.size	_ZN2v88internal16LargeObjectSpace11AllocateRawEiNS0_13ExecutabilityE, .-_ZN2v88internal16LargeObjectSpace11AllocateRawEiNS0_13ExecutabilityE
	.section	.text._ZN2v88internal16LargeObjectSpace11AllocateRawEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16LargeObjectSpace11AllocateRawEi
	.type	_ZN2v88internal16LargeObjectSpace11AllocateRawEi, @function
_ZN2v88internal16LargeObjectSpace11AllocateRawEi:
.LFB23140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	64(%rdi), %rdi
	call	_ZN2v88internal4Heap22CanExpandOldGenerationEm@PLT
	testb	%al, %al
	jne	.L4694
.L4696:
	movslq	72(%r12), %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	salq	$32, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L4694:
	.cfi_restore_state
	movq	64(%r12), %rdi
	call	_ZN2v88internal4Heap41ShouldExpandOldGenerationOnSlowAllocationEv@PLT
	testb	%al, %al
	je	.L4696
	movl	%r13d, %esi
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal16LargeObjectSpace11AllocateRawEiNS0_13ExecutabilityE.part.0
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23140:
	.size	_ZN2v88internal16LargeObjectSpace11AllocateRawEi, .-_ZN2v88internal16LargeObjectSpace11AllocateRawEi
	.section	.text._ZN2v88internal20CodeLargeObjectSpace11AllocateRawEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20CodeLargeObjectSpace11AllocateRawEi
	.type	_ZN2v88internal20CodeLargeObjectSpace11AllocateRawEi, @function
_ZN2v88internal20CodeLargeObjectSpace11AllocateRawEi:
.LFB23169:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	64(%rdi), %rdi
	call	_ZN2v88internal4Heap22CanExpandOldGenerationEm@PLT
	testb	%al, %al
	jne	.L4702
.L4704:
	movslq	72(%r12), %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	salq	$32, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L4702:
	.cfi_restore_state
	movq	64(%r12), %rdi
	call	_ZN2v88internal4Heap41ShouldExpandOldGenerationOnSlowAllocationEv@PLT
	testb	%al, %al
	je	.L4704
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZN2v88internal16LargeObjectSpace11AllocateRawEiNS0_13ExecutabilityE.part.0
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23169:
	.size	_ZN2v88internal20CodeLargeObjectSpace11AllocateRawEi, .-_ZN2v88internal20CodeLargeObjectSpace11AllocateRawEi
	.section	.text._ZN2v88internal19NewLargeObjectSpace11AllocateRawEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19NewLargeObjectSpace11AllocateRawEi
	.type	_ZN2v88internal19NewLargeObjectSpace11AllocateRawEi, @function
_ZN2v88internal19NewLargeObjectSpace11AllocateRawEi:
.LFB23158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	_ZN2v88internal16LargeObjectSpace13SizeOfObjectsEv(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rax
	movq	64(%rdi), %r14
	movq	80(%rax), %rax
	cmpq	%r13, %rax
	jne	.L4710
	movq	120(%rdi), %rsi
.L4711:
	movq	%r14, %rdi
	call	_ZN2v88internal4Heap22CanExpandOldGenerationEm@PLT
	testb	%al, %al
	je	.L4740
	movq	(%rbx), %rax
	movq	80(%rax), %rax
	cmpq	%r13, %rax
	jne	.L4714
	movq	120(%rbx), %rax
.L4715:
	testq	%rax, %rax
	je	.L4716
	movq	(%rbx), %rax
	leaq	_ZN2v88internal19NewLargeObjectSpace9AvailableEv(%rip), %rcx
	movslq	%r12d, %r15
	movq	96(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L4717
	movq	80(%rax), %rax
	movq	136(%rbx), %r14
	cmpq	%r13, %rax
	jne	.L4718
	movq	120(%rbx), %rax
.L4719:
	subq	%rax, %r14
	movq	%r14, %rax
.L4720:
	cmpq	%rax, %r15
	jbe	.L4716
.L4740:
	movslq	72(%rbx), %rax
	salq	$32, %rax
.L4713:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4716:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal16LargeObjectSpace17AllocateLargePageEiNS0_13ExecutabilityE
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L4740
	movq	(%rbx), %rax
	movq	80(%rax), %rax
	cmpq	%r13, %rax
	jne	.L4722
	movq	120(%rbx), %rax
.L4723:
	cmpq	%rax, 136(%rbx)
	movq	64(%rbx), %rdx
	cmovnb	136(%rbx), %rax
	movq	%rax, 136(%rbx)
	movq	40(%r15), %r13
	movq	2064(%rdx), %rdx
	leaq	1(%r13), %rax
	cmpl	$1, 80(%rdx)
	movq	%rax, -64(%rbp)
	movq	8(%r15), %rax
	jle	.L4724
	orq	$262150, %rax
.L4725:
	orq	$16, %rax
	movq	%rax, 8(%r15)
	movq	%r13, 128(%rbx)
	cmpb	$0, _ZN2v88internal13FLAG_minor_mcE(%rip)
	jne	.L4741
.L4726:
	mfence
	cmpb	$0, 56(%rbx)
	jne	.L4727
	movq	8(%rbx), %rax
	cmpq	%rax, 16(%rbx)
	je	.L4727
	movq	64(%rbx), %rax
	movl	$1, %r8d
	movl	%r12d, %edx
	movq	%r13, %rsi
	movl	$1, %ecx
	movslq	%r12d, %r15
	movb	$1, 432(%rax)
	movq	64(%rbx), %rdi
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	16(%rbx), %rax
	movq	8(%rbx), %r14
	movq	%rax, -56(%rbp)
	cmpq	%rax, %r14
	je	.L4730
	.p2align 4,,10
	.p2align 3
.L4729:
	movq	(%r14), %rdi
	movq	%r15, %rcx
	movq	%r13, %rdx
	movl	%r12d, %esi
	addq	$8, %r14
	call	_ZN2v88internal18AllocationObserver14AllocationStepEimm@PLT
	cmpq	%r14, -56(%rbp)
	jne	.L4729
.L4730:
	movq	64(%rbx), %rax
	movb	$0, 432(%rax)
.L4727:
	movq	-64(%rbp), %rcx
	movq	%rcx, %rax
	andl	$1, %ecx
	jne	.L4713
	leaq	.LC48(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4710:
	call	*%rax
	movq	%rax, %rsi
	jmp	.L4711
	.p2align 4,,10
	.p2align 3
.L4714:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L4715
	.p2align 4,,10
	.p2align 3
.L4724:
	andq	$-262149, %rax
	orq	$2, %rax
	jmp	.L4725
	.p2align 4,,10
	.p2align 3
.L4741:
	movl	$1, %edi
	movl	$4096, %esi
	call	calloc@PLT
	movl	$512, %ecx
	movq	%rax, %rdx
	movq	%rax, 264(%r15)
	xorl	%eax, %eax
	movq	%rdx, %rdi
	rep stosq
	movq	$0, 256(%r15)
	jmp	.L4726
	.p2align 4,,10
	.p2align 3
.L4717:
	movq	%rbx, %rdi
	call	*%rdx
	jmp	.L4720
	.p2align 4,,10
	.p2align 3
.L4718:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L4719
	.p2align 4,,10
	.p2align 3
.L4722:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L4723
	.cfi_endproc
.LFE23158:
	.size	_ZN2v88internal19NewLargeObjectSpace11AllocateRawEi, .-_ZN2v88internal19NewLargeObjectSpace11AllocateRawEi
	.section	.text._ZNSt11_Deque_baseISt10unique_ptrIN2v88internal10TypedSlots5ChunkESt14default_deleteIS4_EESaIS7_EE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseISt10unique_ptrIN2v88internal10TypedSlots5ChunkESt14default_deleteIS4_EESaIS7_EE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseISt10unique_ptrIN2v88internal10TypedSlots5ChunkESt14default_deleteIS4_EESaIS7_EE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseISt10unique_ptrIN2v88internal10TypedSlots5ChunkESt14default_deleteIS4_EESaIS7_EE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseISt10unique_ptrIN2v88internal10TypedSlots5ChunkESt14default_deleteIS4_EESaIS7_EE17_M_initialize_mapEm:
.LFB29179:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	shrq	$6, %rdi
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	1(%rdi), %rbx
	addq	$3, %rdi
	subq	$8, %rsp
	cmpq	$8, %rdi
	ja	.L4743
	movq	$8, 8(%r13)
	movl	$64, %edi
.L4744:
	call	_Znwm@PLT
	movq	8(%r13), %rdx
	movq	%rax, 0(%r13)
	subq	%rbx, %rdx
	shrq	%rdx
	leaq	(%rax,%rdx,8), %r15
	leaq	(%r15,%rbx,8), %r12
	movq	%r15, %rbx
	cmpq	%r12, %r15
	jnb	.L4746
	.p2align 4,,10
	.p2align 3
.L4745:
	movl	$512, %edi
	addq	$8, %rbx
	call	_Znwm@PLT
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r12
	ja	.L4745
.L4746:
	movq	(%r15), %rdx
	andl	$63, %r14d
	subq	$8, %r12
	movq	%r15, 40(%r13)
	leaq	512(%rdx), %rax
	movq	%rdx, %xmm0
	movq	%rax, 32(%r13)
	movq	(%r12), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, %xmm2
	movups	%xmm0, 16(%r13)
	leaq	(%rax,%r14,8), %rcx
	movq	%rax, %xmm1
	addq	$512, %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 48(%r13)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 64(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4743:
	.cfi_restore_state
	movq	%rdi, 8(%r13)
	salq	$3, %rdi
	jmp	.L4744
	.cfi_endproc
.LFE29179:
	.size	_ZNSt11_Deque_baseISt10unique_ptrIN2v88internal10TypedSlots5ChunkESt14default_deleteIS4_EESaIS7_EE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseISt10unique_ptrIN2v88internal10TypedSlots5ChunkESt14default_deleteIS4_EESaIS7_EE17_M_initialize_mapEm
	.section	.text._ZN2v88internal11MemoryChunk20AllocateTypedSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_12TypedSlotSetEv,"axG",@progbits,_ZN2v88internal11MemoryChunk20AllocateTypedSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_12TypedSlotSetEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11MemoryChunk20AllocateTypedSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_12TypedSlotSetEv
	.type	_ZN2v88internal11MemoryChunk20AllocateTypedSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_12TypedSlotSetEv, @function
_ZN2v88internal11MemoryChunk20AllocateTypedSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_12TypedSlotSetEv:
.LFB25896:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$152, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	%rax, %r12
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	leaq	16+_ZTVN2v88internal12TypedSlotSetE(%rip), %rax
	leaq	32(%r12), %rdi
	movq	%rax, (%r12)
	movq	%rbx, 24(%r12)
	call	_ZN2v84base5MutexC1Ev@PLT
	pxor	%xmm0, %xmm0
	leaq	72(%r12), %rdi
	xorl	%esi, %esi
	movq	$0, 72(%r12)
	movq	$0, 80(%r12)
	movups	%xmm0, 88(%r12)
	movups	%xmm0, 104(%r12)
	movups	%xmm0, 120(%r12)
	movups	%xmm0, 136(%r12)
	call	_ZNSt11_Deque_baseISt10unique_ptrIN2v88internal10TypedSlots5ChunkESt14default_deleteIS4_EESaIS7_EE17_M_initialize_mapEm
	xorl	%eax, %eax
	lock cmpxchgq	%r12, 128(%rbx)
	testq	%rax, %rax
	je	.L4751
	movq	%rax, %r13
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	%r13, %r12
	call	*8(%rax)
.L4751:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25896:
	.size	_ZN2v88internal11MemoryChunk20AllocateTypedSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_12TypedSlotSetEv, .-_ZN2v88internal11MemoryChunk20AllocateTypedSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_12TypedSlotSetEv
	.section	.text._ZN2v88internal11MemoryChunk20AllocateTypedSlotSetILNS0_17RememberedSetTypeE0EEEPNS0_12TypedSlotSetEv,"axG",@progbits,_ZN2v88internal11MemoryChunk20AllocateTypedSlotSetILNS0_17RememberedSetTypeE0EEEPNS0_12TypedSlotSetEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11MemoryChunk20AllocateTypedSlotSetILNS0_17RememberedSetTypeE0EEEPNS0_12TypedSlotSetEv
	.type	_ZN2v88internal11MemoryChunk20AllocateTypedSlotSetILNS0_17RememberedSetTypeE0EEEPNS0_12TypedSlotSetEv, @function
_ZN2v88internal11MemoryChunk20AllocateTypedSlotSetILNS0_17RememberedSetTypeE0EEEPNS0_12TypedSlotSetEv:
.LFB25895:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$152, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	%rax, %r12
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	leaq	16+_ZTVN2v88internal12TypedSlotSetE(%rip), %rax
	leaq	32(%r12), %rdi
	movq	%rax, (%r12)
	movq	%rbx, 24(%r12)
	call	_ZN2v84base5MutexC1Ev@PLT
	pxor	%xmm0, %xmm0
	leaq	72(%r12), %rdi
	xorl	%esi, %esi
	movq	$0, 72(%r12)
	movq	$0, 80(%r12)
	movups	%xmm0, 88(%r12)
	movups	%xmm0, 104(%r12)
	movups	%xmm0, 120(%r12)
	movups	%xmm0, 136(%r12)
	call	_ZNSt11_Deque_baseISt10unique_ptrIN2v88internal10TypedSlots5ChunkESt14default_deleteIS4_EESaIS7_EE17_M_initialize_mapEm
	xorl	%eax, %eax
	lock cmpxchgq	%r12, 120(%rbx)
	testq	%rax, %rax
	je	.L4757
	movq	%rax, %r13
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	%r13, %r12
	call	*8(%rax)
.L4757:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25895:
	.size	_ZN2v88internal11MemoryChunk20AllocateTypedSlotSetILNS0_17RememberedSetTypeE0EEEPNS0_12TypedSlotSetEv, .-_ZN2v88internal11MemoryChunk20AllocateTypedSlotSetILNS0_17RememberedSetTypeE0EEEPNS0_12TypedSlotSetEv
	.section	.text._ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERS4_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERS4_
	.type	_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERS4_, @function
_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERS4_:
.LFB29530:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpq	%rsi, %r15
	je	.L4813
	movq	(%rdx), %r14
	cmpq	%r14, 32(%rsi)
	jbe	.L4774
	movq	24(%rdi), %rbx
	movq	%rbx, %rax
	movq	%rbx, %rdx
	cmpq	%rsi, %rbx
	je	.L4766
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpq	%r14, 32(%rax)
	jnb	.L4776
	movl	$0, %ebx
	cmpq	$0, 24(%rax)
	movq	%rbx, %rax
	cmovne	%r12, %rdx
	cmovne	%r12, %rax
.L4766:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4774:
	.cfi_restore_state
	jnb	.L4785
	movq	32(%rdi), %rdx
	cmpq	%rsi, %rdx
	je	.L4812
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpq	%r14, 32(%rax)
	jbe	.L4787
	movl	$0, %ebx
	cmpq	$0, 24(%r12)
	movq	%rbx, %rax
	cmovne	%rdx, %rax
	cmove	%r12, %rdx
	jmp	.L4766
	.p2align 4,,10
	.p2align 3
.L4813:
	cmpq	$0, 40(%rdi)
	je	.L4765
	movq	32(%rdi), %rdx
	movq	(%r14), %rax
	cmpq	%rax, 32(%rdx)
	jb	.L4812
.L4765:
	movq	16(%r13), %rbx
	testq	%rbx, %rbx
	je	.L4796
	movq	(%r14), %rsi
	jmp	.L4768
	.p2align 4,,10
	.p2align 3
.L4814:
	movq	16(%rbx), %rax
	movl	$1, %ecx
	testq	%rax, %rax
	je	.L4769
.L4815:
	movq	%rax, %rbx
.L4768:
	movq	32(%rbx), %rdx
	cmpq	%rdx, %rsi
	jb	.L4814
	movq	24(%rbx), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	jne	.L4815
.L4769:
	movq	%rbx, %r12
	testb	%cl, %cl
	jne	.L4767
.L4772:
	xorl	%eax, %eax
	cmpq	%rsi, %rdx
	cmovb	%rax, %rbx
	cmovnb	%rax, %r12
.L4773:
	addq	$8, %rsp
	movq	%rbx, %rax
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4785:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rsi, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4812:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4776:
	.cfi_restore_state
	movq	16(%r13), %r12
	testq	%r12, %r12
	jne	.L4779
	jmp	.L4816
	.p2align 4,,10
	.p2align 3
.L4817:
	movq	16(%r12), %rax
	movl	$1, %esi
.L4782:
	testq	%rax, %rax
	je	.L4780
	movq	%rax, %r12
.L4779:
	movq	32(%r12), %rcx
	cmpq	%rcx, %r14
	jb	.L4817
	movq	24(%r12), %rax
	xorl	%esi, %esi
	jmp	.L4782
	.p2align 4,,10
	.p2align 3
.L4796:
	movq	%r12, %rbx
.L4767:
	cmpq	%rbx, 24(%r13)
	je	.L4798
	movq	%rbx, %rdi
	movq	%rbx, %r12
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	(%r14), %rsi
	movq	32(%rax), %rdx
	movq	%rax, %rbx
	jmp	.L4772
	.p2align 4,,10
	.p2align 3
.L4780:
	movq	%r12, %rdx
	testb	%sil, %sil
	jne	.L4778
.L4783:
	xorl	%eax, %eax
	cmpq	%rcx, %r14
	cmova	%rax, %r12
	cmovbe	%rax, %rdx
.L4784:
	movq	%r12, %rax
	jmp	.L4766
.L4816:
	movq	%r15, %r12
.L4778:
	cmpq	%r12, %rbx
	je	.L4802
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%r12, %rdx
	movq	32(%rax), %rcx
	movq	%rax, %r12
	jmp	.L4783
	.p2align 4,,10
	.p2align 3
.L4787:
	movq	16(%r13), %rbx
	testq	%rbx, %rbx
	jne	.L4790
	jmp	.L4818
	.p2align 4,,10
	.p2align 3
.L4819:
	movq	16(%rbx), %rax
	movl	$1, %esi
.L4793:
	testq	%rax, %rax
	je	.L4791
	movq	%rax, %rbx
.L4790:
	movq	32(%rbx), %rcx
	cmpq	%rcx, %r14
	jb	.L4819
	movq	24(%rbx), %rax
	xorl	%esi, %esi
	jmp	.L4793
	.p2align 4,,10
	.p2align 3
.L4791:
	movq	%rbx, %rdx
	testb	%sil, %sil
	jne	.L4789
.L4794:
	xorl	%eax, %eax
	cmpq	%rcx, %r14
	cmova	%rax, %rbx
	cmovbe	%rax, %rdx
.L4795:
	movq	%rbx, %rax
	jmp	.L4766
	.p2align 4,,10
	.p2align 3
.L4798:
	movq	%rbx, %r12
	xorl	%ebx, %ebx
	jmp	.L4773
.L4818:
	movq	%r15, %rbx
.L4789:
	cmpq	%rbx, 24(%r13)
	je	.L4806
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rbx, %rdx
	movq	32(%rax), %rcx
	movq	%rax, %rbx
	jmp	.L4794
	.p2align 4,,10
	.p2align 3
.L4802:
	movq	%r12, %rdx
	xorl	%r12d, %r12d
	jmp	.L4784
.L4806:
	movq	%rbx, %rdx
	xorl	%ebx, %ebx
	jmp	.L4795
	.cfi_endproc
.LFE29530:
	.size	_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERS4_, .-_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERS4_
	.section	.rodata._ZN2v88internal11MemoryChunk34RegisterObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEvNS0_10HeapObjectEi.str1.1,"aMS",@progbits,1
.LC49:
	.string	"size <= it->second"
	.section	.text._ZN2v88internal11MemoryChunk34RegisterObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEvNS0_10HeapObjectEi,"axG",@progbits,_ZN2v88internal11MemoryChunk34RegisterObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEvNS0_10HeapObjectEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11MemoryChunk34RegisterObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEvNS0_10HeapObjectEi
	.type	_ZN2v88internal11MemoryChunk34RegisterObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEvNS0_10HeapObjectEi, @function
_ZN2v88internal11MemoryChunk34RegisterObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEvNS0_10HeapObjectEi:
.LFB25908:
	.cfi_startproc
	endbr64
	testb	$24, 8(%rdi)
	jne	.L4838
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	136(%rdi), %r13
	testq	%r13, %r13
	je	.L4841
.L4823:
	movq	16(%r13), %rax
	leaq	8(%r13), %rcx
	movq	%rcx, %r12
	testq	%rax, %rax
	jne	.L4825
	jmp	.L4824
	.p2align 4,,10
	.p2align 3
.L4842:
	movq	%rax, %r12
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L4826
.L4825:
	cmpq	%rbx, 32(%rax)
	jnb	.L4842
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L4825
.L4826:
	cmpq	%rcx, %r12
	je	.L4824
	cmpq	%rbx, 32(%r12)
	je	.L4843
.L4824:
	movl	$48, %edi
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rbx, 32(%rax)
	leaq	32(%rax), %rdx
	movq	%rax, %r14
	movl	%r15d, 40(%rax)
	call	_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERS4_
	movq	%rax, %r12
	testq	%rdx, %rdx
	je	.L4831
	movq	-56(%rbp), %rcx
	cmpq	%rdx, %rcx
	je	.L4836
	testq	%rax, %rax
	je	.L4844
.L4836:
	movl	$1, %edi
.L4832:
	movq	%r14, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%r13)
.L4833:
	cmpq	24(%r13), %r14
	je	.L4820
	movq	%r14, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movslq	40(%rax), %rcx
	movq	32(%rax), %rdx
	leaq	-1(%rdx,%rcx), %rsi
	leaq	-1(%rbx), %rcx
	cmpq	%rcx, %rsi
	jbe	.L4820
	subl	%edx, %ebx
	movl	%ebx, 40(%rax)
.L4820:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4838:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L4843:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpl	%r15d, 40(%r12)
	jge	.L4820
	leaq	.LC49(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4844:
	xorl	%edi, %edi
	movq	32(%rdx), %rax
	cmpq	%rax, 32(%r14)
	setb	%dil
	jmp	.L4832
	.p2align 4,,10
	.p2align 3
.L4831:
	movq	%r14, %rdi
	movq	%r12, %r14
	call	_ZdlPv@PLT
	jmp	.L4833
	.p2align 4,,10
	.p2align 3
.L4841:
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal11MemoryChunk24AllocateInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEEv
	movq	-56(%rbp), %rdi
	movq	136(%rdi), %r13
	jmp	.L4823
	.cfi_endproc
.LFE25908:
	.size	_ZN2v88internal11MemoryChunk34RegisterObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEvNS0_10HeapObjectEi, .-_ZN2v88internal11MemoryChunk34RegisterObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE0EEEvNS0_10HeapObjectEi
	.section	.text._ZN2v88internal11MemoryChunk30MoveObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEvNS0_10HeapObjectES4_,"axG",@progbits,_ZN2v88internal11MemoryChunk30MoveObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEvNS0_10HeapObjectES4_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11MemoryChunk30MoveObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEvNS0_10HeapObjectES4_
	.type	_ZN2v88internal11MemoryChunk30MoveObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEvNS0_10HeapObjectES4_, @function
_ZN2v88internal11MemoryChunk30MoveObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEvNS0_10HeapObjectES4_:
.LFB25928:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L4846
	testb	$-128, %ah
	je	.L4845
.L4846:
	movq	144(%r12), %r15
	testq	%r15, %r15
	je	.L4845
	movq	16(%r15), %rax
	leaq	8(%r15), %r8
	testq	%rax, %rax
	je	.L4845
	movq	%r8, %rdi
	jmp	.L4848
	.p2align 4,,10
	.p2align 3
.L4876:
	movq	%rax, %rdi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L4849
.L4848:
	cmpq	%rsi, 32(%rax)
	jnb	.L4876
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L4848
.L4849:
	cmpq	%rdi, %r8
	je	.L4845
	cmpq	%rsi, 32(%rdi)
	ja	.L4845
	movl	%ebx, %r14d
	movl	40(%rdi), %r13d
	subl	%esi, %r14d
	movq	%r8, %rsi
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	subq	$1, 40(%r15)
	movq	144(%r12), %r15
	movq	16(%r15), %rax
	leaq	8(%r15), %rcx
	movq	%rcx, %r12
	testq	%rax, %rax
	jne	.L4853
	jmp	.L4852
	.p2align 4,,10
	.p2align 3
.L4877:
	movq	%rax, %r12
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L4854
.L4853:
	cmpq	32(%rax), %rbx
	jbe	.L4877
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L4853
.L4854:
	cmpq	%rcx, %r12
	je	.L4852
	cmpq	32(%r12), %rbx
	jnb	.L4857
.L4852:
	movl	$48, %edi
	movq	%rcx, -64(%rbp)
	movq	%r12, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rbx, 32(%rax)
	leaq	32(%rax), %rdx
	movq	%rax, %r12
	movl	$0, 40(%rax)
	call	_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERS4_
	movq	%rax, %rbx
	testq	%rdx, %rdx
	je	.L4858
	testq	%rax, %rax
	movq	-64(%rbp), %rcx
	jne	.L4862
	cmpq	%rdx, %rcx
	jne	.L4878
.L4862:
	movl	$1, %edi
.L4859:
	movq	%r12, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%r15)
.L4857:
	subl	%r14d, %r13d
	movl	%r13d, 40(%r12)
.L4845:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4878:
	.cfi_restore_state
	xorl	%edi, %edi
	movq	32(%rdx), %rax
	cmpq	%rax, 32(%r12)
	setb	%dil
	jmp	.L4859
	.p2align 4,,10
	.p2align 3
.L4858:
	movq	%r12, %rdi
	movq	%rbx, %r12
	call	_ZdlPv@PLT
	jmp	.L4857
	.cfi_endproc
.LFE25928:
	.size	_ZN2v88internal11MemoryChunk30MoveObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEvNS0_10HeapObjectES4_, .-_ZN2v88internal11MemoryChunk30MoveObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEvNS0_10HeapObjectES4_
	.section	.text._ZN2v88internal11MemoryChunk34RegisterObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEvNS0_10HeapObjectEi,"axG",@progbits,_ZN2v88internal11MemoryChunk34RegisterObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEvNS0_10HeapObjectEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11MemoryChunk34RegisterObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEvNS0_10HeapObjectEi
	.type	_ZN2v88internal11MemoryChunk34RegisterObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEvNS0_10HeapObjectEi, @function
_ZN2v88internal11MemoryChunk34RegisterObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEvNS0_10HeapObjectEi:
.LFB25925:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rax
	testb	$88, %al
	je	.L4880
	testb	$-128, %ah
	je	.L4879
.L4880:
	movq	144(%rdi), %r13
	testq	%r13, %r13
	je	.L4902
.L4882:
	movq	16(%r13), %rax
	leaq	8(%r13), %rcx
	movq	%rcx, %r12
	testq	%rax, %rax
	jne	.L4884
	jmp	.L4883
	.p2align 4,,10
	.p2align 3
.L4903:
	movq	%rax, %r12
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L4885
.L4884:
	cmpq	32(%rax), %rbx
	jbe	.L4903
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L4884
.L4885:
	cmpq	%r12, %rcx
	je	.L4883
	cmpq	32(%r12), %rbx
	je	.L4904
.L4883:
	movl	$48, %edi
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rbx, 32(%rax)
	leaq	32(%rax), %rdx
	movq	%rax, %r14
	movl	%r15d, 40(%rax)
	call	_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERS4_
	testq	%rdx, %rdx
	je	.L4889
	testq	%rax, %rax
	movq	-56(%rbp), %rcx
	jne	.L4894
	cmpq	%rdx, %rcx
	jne	.L4905
.L4894:
	movl	$1, %edi
.L4890:
	movq	%r14, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%r13)
.L4879:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4904:
	.cfi_restore_state
	cmpl	%r15d, 40(%r12)
	jge	.L4879
	leaq	.LC49(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4889:
	addq	$24, %rsp
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L4905:
	.cfi_restore_state
	xorl	%edi, %edi
	movq	32(%rdx), %rax
	cmpq	%rax, 32(%r14)
	setb	%dil
	jmp	.L4890
	.p2align 4,,10
	.p2align 3
.L4902:
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal11MemoryChunk24AllocateInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEEv
	movq	-56(%rbp), %rdi
	movq	144(%rdi), %r13
	jmp	.L4882
	.cfi_endproc
.LFE25925:
	.size	_ZN2v88internal11MemoryChunk34RegisterObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEvNS0_10HeapObjectEi, .-_ZN2v88internal11MemoryChunk34RegisterObjectWithInvalidatedSlotsILNS0_17RememberedSetTypeE1EEEvNS0_10HeapObjectEi
	.section	.text._ZNSt5dequeISt10unique_ptrIN2v88internal10TypedSlots5ChunkESt14default_deleteIS4_EESaIS7_EE16_M_push_back_auxIJS7_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeISt10unique_ptrIN2v88internal10TypedSlots5ChunkESt14default_deleteIS4_EESaIS7_EE16_M_push_back_auxIJS7_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeISt10unique_ptrIN2v88internal10TypedSlots5ChunkESt14default_deleteIS4_EESaIS7_EE16_M_push_back_auxIJS7_EEEvDpOT_
	.type	_ZNSt5dequeISt10unique_ptrIN2v88internal10TypedSlots5ChunkESt14default_deleteIS4_EESaIS7_EE16_M_push_back_auxIJS7_EEEvDpOT_, @function
_ZNSt5dequeISt10unique_ptrIN2v88internal10TypedSlots5ChunkESt14default_deleteIS4_EESaIS7_EE16_M_push_back_auxIJS7_EEEvDpOT_:
.LFB30324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	72(%rdi), %r14
	movq	40(%rdi), %rsi
	movq	48(%rbx), %rdx
	subq	56(%rbx), %rdx
	movq	%r14, %r13
	movq	%rdx, %rcx
	subq	%rsi, %r13
	sarq	$3, %rcx
	movq	%r13, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	32(%rbx), %rax
	subq	16(%rbx), %rax
	movabsq	$1152921504606846975, %rcx
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	je	.L4915
	movq	(%rbx), %r8
	movq	8(%rbx), %rdx
	movq	%r14, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L4916
.L4908:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r14)
	movq	(%r12), %rdx
	movq	48(%rbx), %rax
	movq	$0, (%r12)
	movq	%rdx, (%rax)
	movq	72(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 72(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 56(%rbx)
	movq	%rdx, 64(%rbx)
	movq	%rax, 48(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4916:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L4917
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	cmpq	%rcx, %r14
	ja	.L4918
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	%rax, %rsi
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rsi,%rax,8), %r15
	movq	72(%rbx), %rax
	movq	40(%rbx), %rsi
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L4913
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L4913:
	movq	(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 8(%rbx)
	movq	%rax, (%rbx)
.L4911:
	movq	%r15, 40(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r13), %r14
	movq	(%r15), %xmm0
	movq	%r14, 72(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	(%r14), %rax
	movq	%rax, 56(%rbx)
	addq	$512, %rax
	movq	%rax, 64(%rbx)
	jmp	.L4908
	.p2align 4,,10
	.p2align 3
.L4917:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L4910
	cmpq	%r14, %rsi
	je	.L4911
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L4911
	.p2align 4,,10
	.p2align 3
.L4910:
	cmpq	%r14, %rsi
	je	.L4911
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L4911
.L4915:
	leaq	.LC34(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L4918:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE30324:
	.size	_ZNSt5dequeISt10unique_ptrIN2v88internal10TypedSlots5ChunkESt14default_deleteIS4_EESaIS7_EE16_M_push_back_auxIJS7_EEEvDpOT_, .-_ZNSt5dequeISt10unique_ptrIN2v88internal10TypedSlots5ChunkESt14default_deleteIS4_EESaIS7_EE16_M_push_back_auxIJS7_EEEvDpOT_
	.section	.text._ZN2v88internal9LargePage24ClearOutOfLiveRangeSlotsEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9LargePage24ClearOutOfLiveRangeSlotsEm
	.type	_ZN2v88internal9LargePage24ClearOutOfLiveRangeSlotsEm, @function
_ZN2v88internal9LargePage24ClearOutOfLiveRangeSlotsEm:
.LFB23128:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -80(%rbp)
	movq	48(%rdi), %rdx
	movq	%rsi, -72(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	104(%rdi), %r12
	testq	%r12, %r12
	je	.L4921
	subq	%rdi, %rdx
	subq	%rdi, %rsi
	cmpq	$262143, %rdx
	jbe	.L5210
	leaq	-1(%rdx), %rbx
	movq	%rsi, %r15
	movl	%edx, %ecx
	andl	$262143, %esi
	shrq	$18, %rbx
	shrq	$18, %r15
	movslq	%ebx, %rax
	movq	%rax, -104(%rbp)
	movl	%ebx, %eax
	sall	$18, %eax
	subl	%eax, %ecx
	movslq	%r15d, %rax
	leaq	(%rax,%rax,2), %r13
	movl	%ecx, -88(%rbp)
	salq	$7, %r13
	movq	%rax, -96(%rbp)
	addq	%r12, %r13
	cmpl	%ebx, %r15d
	je	.L5211
	movl	%esi, %r9d
	sarl	$13, %r9d
	movslq	%r9d, %r8
	movq	0(%r13,%r8,8), %r10
	leaq	0(,%r8,8), %r14
	testq	%r10, %r10
	jne	.L4924
.L4929:
	cmpl	$31, %r9d
	je	.L4925
	movl	$30, %edx
	leaq	8(%r13,%r14), %r14
	movq	%rbx, -112(%rbp)
	subl	%r9d, %edx
	movq	%r14, %rbx
	addq	%rdx, %r8
	leaq	16(%r13,%r8,8), %rax
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L4934:
	movq	(%rbx), %rdi
	movq	$0, (%rbx)
	testq	%rdi, %rdi
	je	.L4933
	call	_ZdaPv@PLT
.L4933:
	addq	$8, %rbx
	cmpq	%r14, %rbx
	jne	.L4934
	movq	-112(%rbp), %rbx
.L4925:
	movq	256(%r13), %rax
	leal	1(%r15), %eax
	cmpl	%eax, %ebx
	jle	.L4931
	cltq
	subl	%r15d, %ebx
	leaq	(%rax,%rax,2), %rax
	salq	$7, %rax
	leaq	256(%r12,%rax), %r14
	leal	-2(%rbx), %eax
	addq	-96(%rbp), %rax
	xorl	%ebx, %ebx
	leaq	(%rax,%rax,2), %rax
	salq	$7, %rax
	leaq	1024(%r12,%rax), %r13
	.p2align 4,,10
	.p2align 3
.L4945:
	movq	-256(%r14), %rdx
	testq	%rdx, %rdx
	jne	.L4941
.L4943:
	leaq	-248(%r14), %r15
	.p2align 4,,10
	.p2align 3
.L4938:
	movq	(%r15), %rdi
	movq	$0, (%r15)
	testq	%rdi, %rdi
	je	.L4944
	call	_ZdaPv@PLT
.L4944:
	addq	$8, %r15
	cmpq	%r14, %r15
	jne	.L4938
	movq	(%r14), %rax
	addq	$384, %r14
	cmpq	%r13, %r14
	jne	.L4945
.L4931:
	movq	-104(%rbp), %rax
	leaq	(%rax,%rax,2), %rbx
	salq	$7, %rbx
	addq	%r12, %rbx
	cmpl	$262144, -88(%rbp)
	jg	.L4935
	movl	-88(%rbp), %eax
	movl	%eax, %r15d
	movl	%eax, %r13d
	sarl	$3, %eax
	sarl	$8, %r13d
	movl	%eax, %ecx
	movl	$1, %eax
	sarl	$13, %r15d
	sall	%cl, %eax
	movl	%r13d, %esi
	andl	$31, %esi
	movl	%eax, %r14d
	movl	%eax, -96(%rbp)
	movl	%esi, -88(%rbp)
	negl	%r14d
	orl	%r15d, %esi
	je	.L5212
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L5213
	testl	%r15d, %r15d
	jg	.L5050
.L5208:
	xorl	%eax, %eax
	movl	$1, %esi
.L4954:
	movq	(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L4921
	testb	%al, %al
	jne	.L4921
	cmpl	-88(%rbp), %esi
	jge	.L4959
	movl	-88(%rbp), %ebx
	movslq	%esi, %rdi
	leaq	(%rcx,%rdi,4), %rax
	leal	-1(%rbx), %edx
	subl	%esi, %edx
	addq	%rdi, %rdx
	leaq	4(%rcx,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L4960:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L4960
.L4959:
	movl	-96(%rbp), %edi
	movslq	-88(%rbp), %r13
	subl	$1, %edi
	leaq	(%rcx,%r13,4), %rcx
	jmp	.L4958
	.p2align 4,,10
	.p2align 3
.L5214:
	movl	%r14d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L4921
.L4958:
	movl	(%rcx), %edx
	testl	%edx, %edi
	jne	.L5214
	.p2align 4,,10
	.p2align 3
.L4921:
	movq	-80(%rbp), %rcx
	movq	48(%rcx), %rax
	movq	112(%rcx), %r12
	testq	%r12, %r12
	jne	.L5215
.L4962:
	movq	-80(%rbp), %rax
	movq	48(%rax), %rsi
	movq	%rsi, -88(%rbp)
	movq	120(%rax), %r15
	testq	%r15, %r15
	jne	.L5216
.L5018:
	movq	-80(%rbp), %rax
	movq	48(%rax), %rsi
	movq	%rsi, -80(%rbp)
	movq	128(%rax), %r15
	testq	%r15, %r15
	jne	.L5217
.L4919:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5218
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5215:
	.cfi_restore_state
	movq	-72(%rbp), %rsi
	subq	%rcx, %rax
	subq	%rcx, %rsi
	cmpq	$262143, %rax
	jbe	.L5219
	leaq	-1(%rax), %rbx
	movq	%rsi, %r15
	andl	$262143, %esi
	shrq	$18, %rbx
	shrq	$18, %r15
	movl	%ebx, %edx
	movslq	%ebx, %rcx
	sall	$18, %edx
	movq	%rcx, -104(%rbp)
	subl	%edx, %eax
	movl	%eax, -88(%rbp)
	movslq	%r15d, %rax
	leaq	(%rax,%rax,2), %r14
	movq	%rax, -96(%rbp)
	salq	$7, %r14
	addq	%r12, %r14
	cmpl	%ebx, %r15d
	je	.L5220
	movl	%esi, %r9d
	sarl	$13, %r9d
	movslq	%r9d, %r8
	movq	(%r14,%r8,8), %r10
	leaq	0(,%r8,8), %r13
	testq	%r10, %r10
	jne	.L4983
.L4988:
	cmpl	$31, %r9d
	je	.L4984
	movl	$30, %edx
	leaq	8(%r14,%r13), %r13
	movq	%rbx, -112(%rbp)
	subl	%r9d, %edx
	movq	%r13, %rbx
	addq	%rdx, %r8
	leaq	16(%r14,%r8,8), %rax
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L4993:
	movq	(%rbx), %rdi
	movq	$0, (%rbx)
	testq	%rdi, %rdi
	je	.L4992
	call	_ZdaPv@PLT
.L4992:
	addq	$8, %rbx
	cmpq	%r13, %rbx
	jne	.L4993
	movq	-112(%rbp), %rbx
.L4984:
	movq	256(%r14), %rax
	leal	1(%r15), %eax
	cmpl	%eax, %ebx
	jle	.L4990
	cltq
	subl	%r15d, %ebx
	leaq	(%rax,%rax,2), %rax
	salq	$7, %rax
	leaq	256(%r12,%rax), %r13
	leal	-2(%rbx), %eax
	addq	-96(%rbp), %rax
	xorl	%ebx, %ebx
	leaq	(%rax,%rax,2), %rax
	salq	$7, %rax
	leaq	1024(%r12,%rax), %r15
	.p2align 4,,10
	.p2align 3
.L5003:
	movq	-256(%r13), %rdx
	testq	%rdx, %rdx
	jne	.L4999
.L5001:
	leaq	-248(%r13), %r14
	.p2align 4,,10
	.p2align 3
.L4996:
	movq	(%r14), %rdi
	movq	$0, (%r14)
	testq	%rdi, %rdi
	je	.L5002
	call	_ZdaPv@PLT
.L5002:
	addq	$8, %r14
	cmpq	%r14, %r13
	jne	.L4996
	movq	0(%r13), %rax
	addq	$384, %r13
	cmpq	%r13, %r15
	jne	.L5003
.L4990:
	movq	-104(%rbp), %rax
	leaq	(%rax,%rax,2), %rbx
	salq	$7, %rbx
	addq	%r12, %rbx
	cmpl	$262144, -88(%rbp)
	jg	.L4935
	movl	-88(%rbp), %eax
	movl	%eax, %r15d
	movl	%eax, %r13d
	sarl	$3, %eax
	sarl	$8, %r13d
	movl	%eax, %ecx
	movl	$1, %eax
	sarl	$13, %r15d
	sall	%cl, %eax
	movl	%r13d, %esi
	andl	$31, %esi
	movl	%eax, %r14d
	movl	%eax, -96(%rbp)
	movl	%esi, -88(%rbp)
	negl	%r14d
	orl	%r15d, %esi
	je	.L5221
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L5222
	testl	%r15d, %r15d
	jg	.L5048
.L5209:
	xorl	%eax, %eax
	movl	$1, %esi
.L5011:
	movq	(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L4962
	testb	%al, %al
	jne	.L4962
	cmpl	-88(%rbp), %esi
	jge	.L5016
	movl	-88(%rbp), %ebx
	movslq	%esi, %rdi
	leaq	(%rcx,%rdi,4), %rax
	leal	-1(%rbx), %edx
	subl	%esi, %edx
	addq	%rdi, %rdx
	leaq	4(%rcx,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L5017:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L5017
.L5016:
	movl	-96(%rbp), %edi
	movslq	-88(%rbp), %r13
	subl	$1, %edi
	leaq	(%rcx,%r13,4), %rcx
	jmp	.L5015
	.p2align 4,,10
	.p2align 3
.L5223:
	movl	%r14d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L4962
.L5015:
	movl	(%rcx), %edx
	testl	%edx, %edi
	jne	.L5223
	jmp	.L4962
	.p2align 4,,10
	.p2align 3
.L5217:
	movq	8(%r15), %r14
	testq	%r14, %r14
	je	.L4919
	leaq	-64(%rbp), %rax
	leaq	32(%r15), %r13
	xorl	%r12d, %r12d
	movq	%rax, -88(%rbp)
	movq	%r14, %rax
	movq	%r15, %r14
	movq	%rax, %r15
	movl	20(%r15), %edx
	movq	8(%r15), %rax
	testl	%edx, %edx
	jle	.L5035
	.p2align 4,,10
	.p2align 3
.L5226:
	subl	$1, %edx
	movl	$1, %esi
	leaq	4(%rax,%rdx,4), %rdi
	jmp	.L5038
	.p2align 4,,10
	.p2align 3
.L5225:
	movl	$-1610612736, (%rax)
.L5036:
	addq	$4, %rax
	cmpq	%rdi, %rax
	je	.L5224
.L5038:
	movl	(%rax), %edx
	movl	%edx, %r9d
	shrl	$29, %r9d
	cmpl	$5, %r9d
	je	.L5036
	andl	$536870911, %edx
	addq	24(%r14), %rdx
	cmpq	%rdx, -72(%rbp)
	setbe	%r9b
	cmpq	%rdx, -80(%rbp)
	seta	%dl
	testb	%dl, %r9b
	jne	.L5225
	addq	$4, %rax
	xorl	%esi, %esi
	cmpq	%rdi, %rax
	jne	.L5038
.L5224:
	movq	(%r15), %rbx
	testb	%sil, %sil
	jne	.L5039
	movq	%r15, %r12
.L5040:
	testq	%rbx, %rbx
	je	.L4919
	movq	%rbx, %r15
	movl	20(%r15), %edx
	movq	8(%r15), %rax
	testl	%edx, %edx
	jg	.L5226
.L5035:
	movq	(%r15), %rbx
.L5039:
	testq	%r12, %r12
	je	.L5041
	movq	%rbx, (%r12)
.L5042:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	%r15, -64(%rbp)
	movq	136(%r14), %rax
	movq	120(%r14), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L5043
	movq	$0, -64(%rbp)
	movq	%r15, (%rdx)
	addq	$8, 120(%r14)
.L5044:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5045
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L5045:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L5040
	.p2align 4,,10
	.p2align 3
.L5041:
	movq	%rbx, 8(%r14)
	jmp	.L5042
	.p2align 4,,10
	.p2align 3
.L5216:
	movq	8(%r15), %r14
	testq	%r14, %r14
	je	.L5018
	movl	20(%r14), %edx
	leaq	-64(%rbp), %rax
	xorl	%r12d, %r12d
	leaq	32(%r15), %r13
	movq	%rax, -96(%rbp)
	movq	8(%r14), %rax
	testl	%edx, %edx
	jle	.L5021
	.p2align 4,,10
	.p2align 3
.L5229:
	subl	$1, %edx
	movl	$1, %esi
	leaq	4(%rax,%rdx,4), %rdi
	jmp	.L5024
	.p2align 4,,10
	.p2align 3
.L5228:
	movl	$-1610612736, (%rax)
.L5022:
	addq	$4, %rax
	cmpq	%rax, %rdi
	je	.L5227
.L5024:
	movl	(%rax), %edx
	movl	%edx, %r9d
	shrl	$29, %r9d
	cmpl	$5, %r9d
	je	.L5022
	andl	$536870911, %edx
	addq	24(%r15), %rdx
	cmpq	%rdx, -72(%rbp)
	setbe	%r9b
	cmpq	%rdx, -88(%rbp)
	seta	%dl
	testb	%dl, %r9b
	jne	.L5228
	addq	$4, %rax
	xorl	%esi, %esi
	cmpq	%rax, %rdi
	jne	.L5024
.L5227:
	movq	(%r14), %rbx
	testb	%sil, %sil
	jne	.L5025
	movq	%r14, %r12
.L5026:
	testq	%rbx, %rbx
	je	.L5018
	movq	%rbx, %r14
	movl	20(%r14), %edx
	movq	8(%r14), %rax
	testl	%edx, %edx
	jg	.L5229
.L5021:
	movq	(%r14), %rbx
.L5025:
	testq	%r12, %r12
	je	.L5027
	movq	%rbx, (%r12)
.L5028:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	%r14, -64(%rbp)
	movq	136(%r15), %rax
	movq	120(%r15), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L5029
	movq	$0, -64(%rbp)
	movq	%r14, (%rdx)
	addq	$8, 120(%r15)
.L5030:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5031
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L5031:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L5026
	.p2align 4,,10
	.p2align 3
.L5027:
	movq	%rbx, 8(%r15)
	jmp	.L5028
	.p2align 4,,10
	.p2align 3
.L5210:
	movq	%r12, %rdi
	call	_ZN2v88internal7SlotSet11RemoveRangeEiiNS1_15EmptyBucketModeE
	jmp	.L4921
	.p2align 4,,10
	.p2align 3
.L4939:
	movl	%ecx, %eax
	lock cmpxchgl	%ebx, (%rdx)
	cmpl	%eax, %ecx
	je	.L4942
.L4941:
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L4939
.L4942:
	leaq	4(%rdx), %rax
	subq	$-128, %rdx
	.p2align 4,,10
	.p2align 3
.L4940:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L4940
	jmp	.L4943
	.p2align 4,,10
	.p2align 3
.L4997:
	movl	%ecx, %eax
	lock cmpxchgl	%ebx, (%rdx)
	cmpl	%eax, %ecx
	je	.L5000
.L4999:
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L4997
.L5000:
	leaq	4(%rdx), %rax
	subq	$-128, %rdx
	.p2align 4,,10
	.p2align 3
.L4998:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L4998
	jmp	.L5001
	.p2align 4,,10
	.p2align 3
.L5220:
	movl	-88(%rbp), %edx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal7SlotSet11RemoveRangeEiiNS1_15EmptyBucketModeE
	jmp	.L4962
	.p2align 4,,10
	.p2align 3
.L4924:
	movl	%esi, %eax
	sarl	$3, %esi
	movl	$1, %edi
	sarl	$8, %eax
	movl	%eax, %ecx
	andl	$31, %ecx
	movl	%ecx, -112(%rbp)
	movl	%esi, %ecx
	sall	%cl, %edi
	movl	%eax, %ecx
	andl	$31, %ecx
	leal	-1(%rdi), %r11d
	negl	%edi
	leaq	0(,%rcx,4), %rax
	movq	%rcx, -120(%rbp)
	movq	%rax, -128(%rbp)
	leaq	(%r10,%rax), %rcx
	jmp	.L4928
	.p2align 4,,10
	.p2align 3
.L5230:
	movl	%r11d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L4927
.L4928:
	movl	(%rcx), %edx
	testl	%edx, %edi
	jne	.L5230
.L4927:
	cmpl	$31, -112(%rbp)
	je	.L4929
	movq	-128(%rbp), %rax
	movl	$30, %edx
	subl	-112(%rbp), %edx
	addq	-120(%rbp), %rdx
	leaq	4(%r10,%rax), %rax
	leaq	8(%r10,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L4930:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L4930
	jmp	.L4929
	.p2align 4,,10
	.p2align 3
.L5029:
	movq	-96(%rbp), %rsi
	leaq	72(%r15), %rdi
	call	_ZNSt5dequeISt10unique_ptrIN2v88internal10TypedSlots5ChunkESt14default_deleteIS4_EESaIS7_EE16_M_push_back_auxIJS7_EEEvDpOT_
	jmp	.L5030
	.p2align 4,,10
	.p2align 3
.L5043:
	movq	-88(%rbp), %rsi
	leaq	72(%r14), %rdi
	call	_ZNSt5dequeISt10unique_ptrIN2v88internal10TypedSlots5ChunkESt14default_deleteIS4_EESaIS7_EE16_M_push_back_auxIJS7_EEEvDpOT_
	jmp	.L5044
	.p2align 4,,10
	.p2align 3
.L5219:
	movl	%eax, %edx
	movl	%esi, %r14d
	movl	%esi, %edi
	sarl	$3, %esi
	sarl	$8, %edx
	movl	%esi, %ecx
	sarl	$13, %r14d
	movl	%eax, %r8d
	sarl	$8, %edi
	andl	$31, %edx
	sarl	$13, %r8d
	movl	%edx, -96(%rbp)
	movl	%edx, %r11d
	movl	$1, %edx
	andl	$31, %edi
	movl	%edx, %ebx
	movl	%r8d, -88(%rbp)
	sall	%cl, %ebx
	movl	%eax, %ecx
	movslq	%r14d, %rax
	sarl	$3, %ecx
	movl	%ebx, %esi
	leal	-1(%rbx), %r9d
	movq	%rax, -112(%rbp)
	sall	%cl, %edx
	leaq	0(,%rax,8), %rbx
	movl	%edx, %r15d
	movl	%edx, -104(%rbp)
	leaq	(%r12,%rbx), %r10
	negl	%r15d
	cmpl	%r11d, %edi
	jne	.L4964
	cmpl	%r8d, %r14d
	je	.L5231
.L4964:
	movq	(%r10), %r13
	leal	1(%rdi), %r11d
	testq	%r13, %r13
	jne	.L5232
	cmpl	-88(%rbp), %r14d
	jl	.L4973
.L4977:
	movq	(%r10), %rcx
	cmpl	$32, %r14d
	je	.L4962
	testq	%rcx, %rcx
	je	.L4962
	cmpl	-96(%rbp), %r11d
	jge	.L4980
	movl	-96(%rbp), %ebx
	movslq	%r11d, %rsi
	leaq	(%rcx,%rsi,4), %rax
	leal	-1(%rbx), %edx
	subl	%r11d, %edx
	addq	%rsi, %rdx
	leaq	4(%rcx,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L4981:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L4981
.L4980:
	movl	-104(%rbp), %edi
	movslq	-96(%rbp), %rax
	subl	$1, %edi
	leaq	(%rcx,%rax,4), %rcx
	jmp	.L4979
	.p2align 4,,10
	.p2align 3
.L5233:
	movl	%r15d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L4962
.L4979:
	movl	(%rcx), %edx
	testl	%edx, %edi
	jne	.L5233
	jmp	.L4962
	.p2align 4,,10
	.p2align 3
.L5211:
	movl	%ecx, %edx
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	call	_ZN2v88internal7SlotSet11RemoveRangeEiiNS1_15EmptyBucketModeE
	jmp	.L4921
	.p2align 4,,10
	.p2align 3
.L5232:
	movslq	%edi, %rax
	negl	%esi
	movq	%rax, -120(%rbp)
	salq	$2, %rax
	movq	%rax, -128(%rbp)
	leaq	0(%r13,%rax), %rcx
	jmp	.L4969
	.p2align 4,,10
	.p2align 3
.L5234:
	movl	%r9d, %r8d
	movl	%edx, %eax
	andl	%edx, %r8d
	lock cmpxchgl	%r8d, (%rcx)
	cmpl	%eax, %edx
	je	.L4968
.L4969:
	movl	(%rcx), %edx
	testl	%edx, %esi
	jne	.L5234
.L4968:
	cmpl	-88(%rbp), %r14d
	jge	.L4977
	cmpl	$32, %r11d
	je	.L4973
	movq	-128(%rbp), %rax
	movl	$30, %edx
	subl	%edi, %edx
	addq	-120(%rbp), %rdx
	leaq	4(%r13,%rax), %rax
	leaq	8(%r13,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L4974:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L4974
.L4973:
	leal	1(%r14), %eax
	movl	%eax, -120(%rbp)
	cmpl	%eax, -88(%rbp)
	jle	.L5235
	movl	-88(%rbp), %eax
	leaq	8(%r12,%rbx), %r13
	leal	-2(%rax), %edx
	subl	%r14d, %edx
	addq	-112(%rbp), %rdx
	leaq	16(%r12,%rdx,8), %rbx
	.p2align 4,,10
	.p2align 3
.L4976:
	movq	0(%r13), %rdi
	movq	$0, 0(%r13)
	testq	%rdi, %rdi
	je	.L4975
	call	_ZdaPv@PLT
.L4975:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L4976
	movl	-88(%rbp), %eax
	movl	-120(%rbp), %ebx
	xorl	%r11d, %r11d
	leal	-1(%rax,%rbx), %eax
	subl	%r14d, %eax
	movl	%eax, %r14d
	cltq
	leaq	(%r12,%rax,8), %r10
	jmp	.L4977
	.p2align 4,,10
	.p2align 3
.L4983:
	movl	%esi, %eax
	movl	$1, %edi
	sarl	$8, %eax
	movl	%eax, %ecx
	andl	$31, %ecx
	movl	%ecx, -112(%rbp)
	movl	%esi, %ecx
	sarl	$3, %ecx
	sall	%cl, %edi
	movl	%eax, %ecx
	andl	$31, %ecx
	leal	-1(%rdi), %r11d
	negl	%edi
	leaq	0(,%rcx,4), %rax
	movq	%rcx, -120(%rbp)
	movq	%rax, -128(%rbp)
	leaq	(%r10,%rax), %rcx
	jmp	.L4987
	.p2align 4,,10
	.p2align 3
.L5236:
	movl	%r11d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L4986
.L4987:
	movl	(%rcx), %edx
	testl	%edx, %edi
	jne	.L5236
.L4986:
	cmpl	$31, -112(%rbp)
	je	.L4988
	movq	-128(%rbp), %rax
	movl	$30, %edx
	subl	-112(%rbp), %edx
	addq	-120(%rbp), %rdx
	leaq	4(%r10,%rax), %rax
	leaq	8(%r10,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L4989:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L4989
	jmp	.L4988
	.p2align 4,,10
	.p2align 3
.L5213:
	xorl	%esi, %esi
	jmp	.L4951
	.p2align 4,,10
	.p2align 3
.L5237:
	movl	%ecx, %eax
	lock cmpxchgl	%esi, (%rdx)
	cmpl	%eax, %ecx
	je	.L4950
.L4951:
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L5237
.L4950:
	leaq	4(%rdx), %rax
	subq	$-128, %rdx
	testl	%r15d, %r15d
	jle	.L5208
	.p2align 4,,10
	.p2align 3
.L4952:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rdx, %rax
	jne	.L4952
.L5050:
	cmpl	$1, %r15d
	jle	.L5238
	leal	-2(%r15), %edx
	leaq	8(%rbx), %r13
	leaq	16(%rbx,%rdx,8), %r12
	.p2align 4,,10
	.p2align 3
.L4956:
	movq	0(%r13), %rdi
	movq	$0, 0(%r13)
	testq	%rdi, %rdi
	je	.L4955
	call	_ZdaPv@PLT
.L4955:
	addq	$8, %r13
	cmpq	%r13, %r12
	jne	.L4956
	movslq	%r15d, %rax
	cmpl	$32, %r15d
	leaq	(%rbx,%rax,8), %rbx
	sete	%al
	xorl	%esi, %esi
	jmp	.L4954
	.p2align 4,,10
	.p2align 3
.L5222:
	xorl	%esi, %esi
	jmp	.L5008
	.p2align 4,,10
	.p2align 3
.L5239:
	movl	%ecx, %eax
	lock cmpxchgl	%esi, (%rdx)
	cmpl	%eax, %ecx
	je	.L5007
.L5008:
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L5239
.L5007:
	leaq	4(%rdx), %rax
	subq	$-128, %rdx
	testl	%r15d, %r15d
	jle	.L5209
	.p2align 4,,10
	.p2align 3
.L5009:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L5009
.L5048:
	cmpl	$1, %r15d
	jle	.L5240
	leal	-2(%r15), %edx
	leaq	8(%rbx), %r13
	leaq	16(%rbx,%rdx,8), %r12
	.p2align 4,,10
	.p2align 3
.L5013:
	movq	0(%r13), %rdi
	movq	$0, 0(%r13)
	testq	%rdi, %rdi
	je	.L5012
	call	_ZdaPv@PLT
.L5012:
	addq	$8, %r13
	cmpq	%r13, %r12
	jne	.L5013
	movslq	%r15d, %rax
	cmpl	$32, %r15d
	leaq	(%rbx,%rax,8), %rbx
	sete	%al
	xorl	%esi, %esi
	jmp	.L5011
	.p2align 4,,10
	.p2align 3
.L4935:
	leaq	.LC35(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L5231:
	movq	(%r10), %rax
	testq	%rax, %rax
	je	.L4962
	orl	%r9d, %r15d
	movslq	%edi, %rdi
	movl	%r15d, %r8d
	leaq	(%rax,%rdi,4), %rcx
	notl	%r8d
	jmp	.L4966
	.p2align 4,,10
	.p2align 3
.L5241:
	movl	%r15d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L4962
.L4966:
	movl	(%rcx), %edx
	testl	%edx, %r8d
	jne	.L5241
	jmp	.L4962
.L5212:
	movq	(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L4921
	movl	-96(%rbp), %edi
	subl	$1, %edi
	jmp	.L4948
	.p2align 4,,10
	.p2align 3
.L5242:
	movl	%r14d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L4921
.L4948:
	movl	(%rcx), %edx
	testl	%edi, %edx
	jne	.L5242
	jmp	.L4921
.L5221:
	movq	(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L4962
	movl	-96(%rbp), %edi
	subl	$1, %edi
	jmp	.L5005
	.p2align 4,,10
	.p2align 3
.L5243:
	movl	%r14d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L4962
.L5005:
	movl	(%rcx), %edx
	testl	%edi, %edx
	jne	.L5243
	jmp	.L4962
.L5235:
	movl	%eax, %r14d
	leaq	8(%r12,%rbx), %r10
	xorl	%r11d, %r11d
	jmp	.L4977
.L5238:
	addq	$8, %rbx
	xorl	%eax, %eax
	xorl	%esi, %esi
	jmp	.L4954
.L5240:
	addq	$8, %rbx
	xorl	%eax, %eax
	xorl	%esi, %esi
	jmp	.L5011
.L5218:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23128:
	.size	_ZN2v88internal9LargePage24ClearOutOfLiveRangeSlotsEm, .-_ZN2v88internal9LargePage24ClearOutOfLiveRangeSlotsEm
	.section	.text._ZN2v88internal16LargeObjectSpace19FreeUnmarkedObjectsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16LargeObjectSpace19FreeUnmarkedObjectsEv
	.type	_ZN2v88internal16LargeObjectSpace19FreeUnmarkedObjectsEv, @function
_ZN2v88internal16LargeObjectSpace19FreeUnmarkedObjectsEv:
.LFB23151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	32(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	testq	%rbx, %rbx
	je	.L5245
	leaq	-64(%rbp), %r13
	movl	$1, %r14d
	jmp	.L5255
	.p2align 4,,10
	.p2align 3
.L5246:
	movq	(%r12), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	*136(%rax)
	movq	64(%r12), %rax
	movq	%r15, %rsi
	movq	2048(%rax), %rdi
	call	_ZN2v88internal15MemoryAllocator4FreeILNS1_8FreeModeE2EEEvPNS0_11MemoryChunkE
.L5248:
	testq	%rbx, %rbx
	je	.L5245
.L5255:
	movq	%rbx, %r15
	movq	224(%rbx), %rbx
	movq	%r13, %rdi
	movq	40(%r15), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, -64(%rbp)
	movq	(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-64(%rbp), %rsi
	movl	%r14d, %r9d
	movslq	%eax, %rdx
	movq	%rsi, %rdi
	movl	%esi, %eax
	andq	$-262144, %rdi
	andl	$262143, %eax
	movq	16(%rdi), %rdi
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	leaq	(%rdi,%rax,4), %rdi
	sall	%cl, %r9d
	movl	(%rdi), %eax
	movl	%r9d, %ecx
	testl	%r9d, %eax
	je	.L5246
	addl	%ecx, %ecx
	jne	.L5247
	movl	4(%rdi), %eax
	movl	$1, %ecx
.L5247:
	testl	%eax, %ecx
	je	.L5246
	addq	%rdx, -80(%rbp)
	testb	$1, 8(%r15)
	jne	.L5248
	movl	_ZN2v88internal20FLAG_v8_os_page_sizeE(%rip), %eax
	testl	%eax, %eax
	je	.L5249
	sall	$10, %eax
	movslq	%eax, %rcx
	movq	%rcx, %rdi
.L5250:
	leaq	-2(%rsi,%rdx), %rax
	negq	%rcx
	subq	%r15, %rax
	addq	%rdi, %rax
	andq	%rcx, %rax
	movq	%rax, -72(%rbp)
	call	_ZN2v84base2OS14HasLazyCommitsEv@PLT
	testb	%al, %al
	je	.L5254
	testb	$32, 10(%r15)
	je	.L5269
.L5253:
	movq	152(%r15), %rax
	jmp	.L5252
	.p2align 4,,10
	.p2align 3
.L5245:
	movq	-80(%rbp), %rax
	movq	%rax, 120(%r12)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5270
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5269:
	.cfi_restore_state
	movq	80(%r15), %rax
	cmpl	$5, 72(%rax)
	jne	.L5253
	.p2align 4,,10
	.p2align 3
.L5254:
	movq	(%r15), %rax
.L5252:
	movq	-72(%rbp), %rdx
	cmpq	%rax, %rdx
	jnb	.L5248
	addq	%r15, %rdx
	je	.L5248
	movq	%rdx, %rsi
	movq	%r15, %rdi
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal9LargePage24ClearOutOfLiveRangeSlotsEm
	movq	64(%r12), %rax
	movq	(%r15), %r10
	movq	%r13, %rdi
	movq	40(%r15), %r8
	movq	2048(%rax), %r11
	movq	-64(%rbp), %rax
	movq	%r10, %rcx
	movq	%r10, -112(%rbp)
	subq	-72(%rbp), %rcx
	movq	%r8, -88(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%r11, -96(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %r11
	movq	%r15, %rsi
	cltq
	movq	-104(%rbp), %rcx
	movq	-120(%rbp), %rdx
	addq	%rax, %r8
	movq	%r11, %rdi
	call	_ZN2v88internal15MemoryAllocator17PartialFreeMemoryEPNS0_11MemoryChunkEmmm
	movq	-72(%rbp), %rax
	movq	-112(%rbp), %r10
	subq	%r10, %rax
	addq	%rax, 104(%r12)
	addq	%rax, 80(%r12)
	jmp	.L5248
	.p2align 4,,10
	.p2align 3
.L5249:
	movq	%rdx, -88(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movq	-88(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, %rcx
	jmp	.L5250
.L5270:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23151:
	.size	_ZN2v88internal16LargeObjectSpace19FreeUnmarkedObjectsEv, .-_ZN2v88internal16LargeObjectSpace19FreeUnmarkedObjectsEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal24PagedSpaceObjectIteratorC2EPNS0_10PagedSpaceE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal24PagedSpaceObjectIteratorC2EPNS0_10PagedSpaceE, @function
_GLOBAL__sub_I__ZN2v88internal24PagedSpaceObjectIteratorC2EPNS0_10PagedSpaceE:
.LFB30778:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE30778:
	.size	_GLOBAL__sub_I__ZN2v88internal24PagedSpaceObjectIteratorC2EPNS0_10PagedSpaceE, .-_GLOBAL__sub_I__ZN2v88internal24PagedSpaceObjectIteratorC2EPNS0_10PagedSpaceE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal24PagedSpaceObjectIteratorC2EPNS0_10PagedSpaceE
	.section	.text._ZN2v88internal29PauseAllocationObserversScopeC2EPNS0_4HeapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PauseAllocationObserversScopeC2EPNS0_4HeapE
	.type	_ZN2v88internal29PauseAllocationObserversScopeC2EPNS0_4HeapE, @function
_ZN2v88internal29PauseAllocationObserversScopeC2EPNS0_4HeapE:
.LFB22748:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, (%rdi)
	movq	%r12, %rdi
	call	_ZN2v88internal13SpaceIteratorC1EPNS0_4HeapE@PLT
	jmp	.L5275
	.p2align 4,,10
	.p2align 3
.L5278:
	call	_ZN2v88internal13SpaceIterator4NextEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*32(%rax)
.L5275:
	movq	%r12, %rdi
	call	_ZN2v88internal13SpaceIterator7HasNextEv@PLT
	movq	%r12, %rdi
	testb	%al, %al
	jne	.L5278
	call	_ZN2v88internal13SpaceIteratorD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5279
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5279:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22748:
	.size	_ZN2v88internal29PauseAllocationObserversScopeC2EPNS0_4HeapE, .-_ZN2v88internal29PauseAllocationObserversScopeC2EPNS0_4HeapE
	.globl	_ZN2v88internal29PauseAllocationObserversScopeC1EPNS0_4HeapE
	.set	_ZN2v88internal29PauseAllocationObserversScopeC1EPNS0_4HeapE,_ZN2v88internal29PauseAllocationObserversScopeC2EPNS0_4HeapE
	.section	.text._ZN2v88internal29PauseAllocationObserversScopeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PauseAllocationObserversScopeD2Ev
	.type	_ZN2v88internal29PauseAllocationObserversScopeD2Ev, @function
_ZN2v88internal29PauseAllocationObserversScopeD2Ev:
.LFB22751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	subq	$40, %rsp
	movq	(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal13SpaceIteratorC1EPNS0_4HeapE@PLT
	jmp	.L5282
	.p2align 4,,10
	.p2align 3
.L5285:
	call	_ZN2v88internal13SpaceIterator4NextEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*40(%rax)
.L5282:
	movq	%r12, %rdi
	call	_ZN2v88internal13SpaceIterator7HasNextEv@PLT
	movq	%r12, %rdi
	testb	%al, %al
	jne	.L5285
	call	_ZN2v88internal13SpaceIteratorD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5286
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5286:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22751:
	.size	_ZN2v88internal29PauseAllocationObserversScopeD2Ev, .-_ZN2v88internal29PauseAllocationObserversScopeD2Ev
	.globl	_ZN2v88internal29PauseAllocationObserversScopeD1Ev
	.set	_ZN2v88internal29PauseAllocationObserversScopeD1Ev,_ZN2v88internal29PauseAllocationObserversScopeD2Ev
	.weak	_ZTVN2v88internal14CancelableTaskE
	.section	.data.rel.ro._ZTVN2v88internal14CancelableTaskE,"awG",@progbits,_ZTVN2v88internal14CancelableTaskE,comdat
	.align 8
	.type	_ZTVN2v88internal14CancelableTaskE, @object
	.size	_ZTVN2v88internal14CancelableTaskE, 88
_ZTVN2v88internal14CancelableTaskE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	__cxa_pure_virtual
	.quad	-32
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.weak	_ZTVN2v88internal10NoFreeListE
	.section	.data.rel.ro.local._ZTVN2v88internal10NoFreeListE,"awG",@progbits,_ZTVN2v88internal10NoFreeListE,comdat
	.align 8
	.type	_ZTVN2v88internal10NoFreeListE, @object
	.size	_ZTVN2v88internal10NoFreeListE, 96
_ZTVN2v88internal10NoFreeListE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal10NoFreeListD1Ev
	.quad	_ZN2v88internal10NoFreeListD0Ev
	.quad	_ZN2v88internal10NoFreeList21GuaranteedAllocatableEm
	.quad	_ZN2v88internal10NoFreeList4FreeEmmNS0_8FreeModeE
	.quad	_ZN2v88internal10NoFreeList8AllocateEmPmNS0_16AllocationOriginE
	.quad	_ZN2v88internal10NoFreeList14GetPageForSizeEm
	.quad	_ZN2v88internal8FreeList5ResetEv
	.quad	_ZN2v88internal8FreeList11AddCategoryEPNS0_16FreeListCategoryE
	.quad	_ZN2v88internal8FreeList14RemoveCategoryEPNS0_16FreeListCategoryE
	.quad	_ZN2v88internal10NoFreeList26SelectFreeListCategoryTypeEm
	.weak	_ZTVN2v88internal24PagedSpaceObjectIteratorE
	.section	.data.rel.ro.local._ZTVN2v88internal24PagedSpaceObjectIteratorE,"awG",@progbits,_ZTVN2v88internal24PagedSpaceObjectIteratorE,comdat
	.align 8
	.type	_ZTVN2v88internal24PagedSpaceObjectIteratorE, @object
	.size	_ZTVN2v88internal24PagedSpaceObjectIteratorE, 40
_ZTVN2v88internal24PagedSpaceObjectIteratorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal24PagedSpaceObjectIteratorD1Ev
	.quad	_ZN2v88internal24PagedSpaceObjectIteratorD0Ev
	.quad	_ZN2v88internal24PagedSpaceObjectIterator4NextEv
	.weak	_ZTVN2v88internal23SemiSpaceObjectIteratorE
	.section	.data.rel.ro.local._ZTVN2v88internal23SemiSpaceObjectIteratorE,"awG",@progbits,_ZTVN2v88internal23SemiSpaceObjectIteratorE,comdat
	.align 8
	.type	_ZTVN2v88internal23SemiSpaceObjectIteratorE, @object
	.size	_ZTVN2v88internal23SemiSpaceObjectIteratorE, 40
_ZTVN2v88internal23SemiSpaceObjectIteratorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23SemiSpaceObjectIteratorD1Ev
	.quad	_ZN2v88internal23SemiSpaceObjectIteratorD0Ev
	.quad	_ZN2v88internal23SemiSpaceObjectIterator4NextEv
	.weak	_ZTVN2v88internal13ReadOnlySpaceE
	.section	.data.rel.ro.local._ZTVN2v88internal13ReadOnlySpaceE,"awG",@progbits,_ZTVN2v88internal13ReadOnlySpaceE,comdat
	.align 8
	.type	_ZTVN2v88internal13ReadOnlySpaceE, @object
	.size	_ZTVN2v88internal13ReadOnlySpaceE, 208
_ZTVN2v88internal13ReadOnlySpaceE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal13ReadOnlySpaceD1Ev
	.quad	_ZN2v88internal13ReadOnlySpaceD0Ev
	.quad	_ZN2v88internal19SpaceWithLinearArea21AddAllocationObserverEPNS0_18AllocationObserverE
	.quad	_ZN2v88internal19SpaceWithLinearArea24RemoveAllocationObserverEPNS0_18AllocationObserverE
	.quad	_ZN2v88internal19SpaceWithLinearArea24PauseAllocationObserversEv
	.quad	_ZN2v88internal19SpaceWithLinearArea25ResumeAllocationObserversEv
	.quad	_ZN2v88internal19SpaceWithLinearArea29StartNextInlineAllocationStepEv
	.quad	_ZN2v88internal5Space15CommittedMemoryEv
	.quad	_ZN2v88internal5Space22MaximumCommittedMemoryEv
	.quad	_ZN2v88internal10PagedSpace4SizeEv
	.quad	_ZN2v88internal10PagedSpace13SizeOfObjectsEv
	.quad	_ZN2v88internal10PagedSpace23CommittedPhysicalMemoryEv
	.quad	_ZN2v88internal13ReadOnlySpace9AvailableEv
	.quad	_ZN2v88internal5Space30RoundSizeDownToObjectAlignmentEi
	.quad	_ZN2v88internal10PagedSpace17GetObjectIteratorEv
	.quad	_ZNK2v88internal5Space25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE
	.quad	_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv
	.quad	_ZN2v88internal10PagedSpace27UpdateInlineAllocationLimitEm
	.quad	_ZN2v88internal10PagedSpace5WasteEv
	.quad	_ZN2v88internal10PagedSpace8is_localEv
	.quad	_ZN2v88internal10PagedSpace14RefillFreeListEv
	.quad	_ZN2v88internal10PagedSpace12snapshotableEv
	.quad	_ZN2v88internal10PagedSpace23SweepAndRetryAllocationEiNS0_16AllocationOriginE
	.quad	_ZN2v88internal10PagedSpace30SlowRefillLinearAllocationAreaEiNS0_16AllocationOriginE
	.weak	_ZTVN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskE
	.section	.data.rel.ro.local._ZTVN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskE,"awG",@progbits,_ZTVN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskE,comdat
	.align 8
	.type	_ZTVN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskE, @object
	.size	_ZTVN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskE, 88
_ZTVN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD1Ev
	.quad	_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD0Ev
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	_ZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTask11RunInternalEv
	.quad	-32
	.quad	0
	.quad	_ZThn32_N2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD1Ev
	.quad	_ZThn32_N2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTaskD0Ev
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.weak	_ZTVN2v88internal5SpaceE
	.section	.data.rel.ro._ZTVN2v88internal5SpaceE,"awG",@progbits,_ZTVN2v88internal5SpaceE,comdat
	.align 8
	.type	_ZTVN2v88internal5SpaceE, @object
	.size	_ZTVN2v88internal5SpaceE, 144
_ZTVN2v88internal5SpaceE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal5Space21AddAllocationObserverEPNS0_18AllocationObserverE
	.quad	_ZN2v88internal5Space24RemoveAllocationObserverEPNS0_18AllocationObserverE
	.quad	_ZN2v88internal5Space24PauseAllocationObserversEv
	.quad	_ZN2v88internal5Space25ResumeAllocationObserversEv
	.quad	_ZN2v88internal5Space29StartNextInlineAllocationStepEv
	.quad	_ZN2v88internal5Space15CommittedMemoryEv
	.quad	_ZN2v88internal5Space22MaximumCommittedMemoryEv
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal5Space13SizeOfObjectsEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal5Space30RoundSizeDownToObjectAlignmentEi
	.quad	__cxa_pure_virtual
	.quad	_ZNK2v88internal5Space25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE
	.weak	_ZTVN2v88internal10PagedSpaceE
	.section	.data.rel.ro.local._ZTVN2v88internal10PagedSpaceE,"awG",@progbits,_ZTVN2v88internal10PagedSpaceE,comdat
	.align 8
	.type	_ZTVN2v88internal10PagedSpaceE, @object
	.size	_ZTVN2v88internal10PagedSpaceE, 208
_ZTVN2v88internal10PagedSpaceE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal10PagedSpaceD1Ev
	.quad	_ZN2v88internal10PagedSpaceD0Ev
	.quad	_ZN2v88internal19SpaceWithLinearArea21AddAllocationObserverEPNS0_18AllocationObserverE
	.quad	_ZN2v88internal19SpaceWithLinearArea24RemoveAllocationObserverEPNS0_18AllocationObserverE
	.quad	_ZN2v88internal19SpaceWithLinearArea24PauseAllocationObserversEv
	.quad	_ZN2v88internal19SpaceWithLinearArea25ResumeAllocationObserversEv
	.quad	_ZN2v88internal19SpaceWithLinearArea29StartNextInlineAllocationStepEv
	.quad	_ZN2v88internal5Space15CommittedMemoryEv
	.quad	_ZN2v88internal5Space22MaximumCommittedMemoryEv
	.quad	_ZN2v88internal10PagedSpace4SizeEv
	.quad	_ZN2v88internal10PagedSpace13SizeOfObjectsEv
	.quad	_ZN2v88internal10PagedSpace23CommittedPhysicalMemoryEv
	.quad	_ZN2v88internal10PagedSpace9AvailableEv
	.quad	_ZN2v88internal5Space30RoundSizeDownToObjectAlignmentEi
	.quad	_ZN2v88internal10PagedSpace17GetObjectIteratorEv
	.quad	_ZNK2v88internal5Space25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE
	.quad	_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv
	.quad	_ZN2v88internal10PagedSpace27UpdateInlineAllocationLimitEm
	.quad	_ZN2v88internal10PagedSpace5WasteEv
	.quad	_ZN2v88internal10PagedSpace8is_localEv
	.quad	_ZN2v88internal10PagedSpace14RefillFreeListEv
	.quad	_ZN2v88internal10PagedSpace12snapshotableEv
	.quad	_ZN2v88internal10PagedSpace23SweepAndRetryAllocationEiNS0_16AllocationOriginE
	.quad	_ZN2v88internal10PagedSpace30SlowRefillLinearAllocationAreaEiNS0_16AllocationOriginE
	.weak	_ZTVN2v88internal16LargeObjectSpaceE
	.section	.data.rel.ro.local._ZTVN2v88internal16LargeObjectSpaceE,"awG",@progbits,_ZTVN2v88internal16LargeObjectSpaceE,comdat
	.align 8
	.type	_ZTVN2v88internal16LargeObjectSpaceE, @object
	.size	_ZTVN2v88internal16LargeObjectSpaceE, 160
_ZTVN2v88internal16LargeObjectSpaceE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal16LargeObjectSpaceD1Ev
	.quad	_ZN2v88internal16LargeObjectSpaceD0Ev
	.quad	_ZN2v88internal5Space21AddAllocationObserverEPNS0_18AllocationObserverE
	.quad	_ZN2v88internal5Space24RemoveAllocationObserverEPNS0_18AllocationObserverE
	.quad	_ZN2v88internal5Space24PauseAllocationObserversEv
	.quad	_ZN2v88internal5Space25ResumeAllocationObserversEv
	.quad	_ZN2v88internal5Space29StartNextInlineAllocationStepEv
	.quad	_ZN2v88internal5Space15CommittedMemoryEv
	.quad	_ZN2v88internal5Space22MaximumCommittedMemoryEv
	.quad	_ZN2v88internal16LargeObjectSpace4SizeEv
	.quad	_ZN2v88internal16LargeObjectSpace13SizeOfObjectsEv
	.quad	_ZN2v88internal16LargeObjectSpace23CommittedPhysicalMemoryEv
	.quad	_ZN2v88internal16LargeObjectSpace9AvailableEv
	.quad	_ZN2v88internal5Space30RoundSizeDownToObjectAlignmentEi
	.quad	_ZN2v88internal16LargeObjectSpace17GetObjectIteratorEv
	.quad	_ZNK2v88internal5Space25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE
	.quad	_ZN2v88internal16LargeObjectSpace7AddPageEPNS0_9LargePageEm
	.quad	_ZN2v88internal16LargeObjectSpace10RemovePageEPNS0_9LargePageEm
	.weak	_ZTVN2v88internal19SpaceWithLinearAreaE
	.section	.data.rel.ro._ZTVN2v88internal19SpaceWithLinearAreaE,"awG",@progbits,_ZTVN2v88internal19SpaceWithLinearAreaE,comdat
	.align 8
	.type	_ZTVN2v88internal19SpaceWithLinearAreaE, @object
	.size	_ZTVN2v88internal19SpaceWithLinearAreaE, 160
_ZTVN2v88internal19SpaceWithLinearAreaE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal19SpaceWithLinearArea21AddAllocationObserverEPNS0_18AllocationObserverE
	.quad	_ZN2v88internal19SpaceWithLinearArea24RemoveAllocationObserverEPNS0_18AllocationObserverE
	.quad	_ZN2v88internal19SpaceWithLinearArea24PauseAllocationObserversEv
	.quad	_ZN2v88internal19SpaceWithLinearArea25ResumeAllocationObserversEv
	.quad	_ZN2v88internal19SpaceWithLinearArea29StartNextInlineAllocationStepEv
	.quad	_ZN2v88internal5Space15CommittedMemoryEv
	.quad	_ZN2v88internal5Space22MaximumCommittedMemoryEv
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal5Space13SizeOfObjectsEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal5Space30RoundSizeDownToObjectAlignmentEi
	.quad	__cxa_pure_virtual
	.quad	_ZNK2v88internal5Space25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN2v88internal9SemiSpaceE
	.section	.data.rel.ro.local._ZTVN2v88internal9SemiSpaceE,"awG",@progbits,_ZTVN2v88internal9SemiSpaceE,comdat
	.align 8
	.type	_ZTVN2v88internal9SemiSpaceE, @object
	.size	_ZTVN2v88internal9SemiSpaceE, 144
_ZTVN2v88internal9SemiSpaceE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal9SemiSpaceD1Ev
	.quad	_ZN2v88internal9SemiSpaceD0Ev
	.quad	_ZN2v88internal5Space21AddAllocationObserverEPNS0_18AllocationObserverE
	.quad	_ZN2v88internal5Space24RemoveAllocationObserverEPNS0_18AllocationObserverE
	.quad	_ZN2v88internal5Space24PauseAllocationObserversEv
	.quad	_ZN2v88internal5Space25ResumeAllocationObserversEv
	.quad	_ZN2v88internal5Space29StartNextInlineAllocationStepEv
	.quad	_ZN2v88internal5Space15CommittedMemoryEv
	.quad	_ZN2v88internal5Space22MaximumCommittedMemoryEv
	.quad	_ZN2v88internal9SemiSpace4SizeEv
	.quad	_ZN2v88internal9SemiSpace13SizeOfObjectsEv
	.quad	_ZN2v88internal9SemiSpace23CommittedPhysicalMemoryEv
	.quad	_ZN2v88internal9SemiSpace9AvailableEv
	.quad	_ZN2v88internal5Space30RoundSizeDownToObjectAlignmentEi
	.quad	_ZN2v88internal9SemiSpace17GetObjectIteratorEv
	.quad	_ZNK2v88internal5Space25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE
	.weak	_ZTVN2v88internal8NewSpaceE
	.section	.data.rel.ro.local._ZTVN2v88internal8NewSpaceE,"awG",@progbits,_ZTVN2v88internal8NewSpaceE,comdat
	.align 8
	.type	_ZTVN2v88internal8NewSpaceE, @object
	.size	_ZTVN2v88internal8NewSpaceE, 160
_ZTVN2v88internal8NewSpaceE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8NewSpaceD1Ev
	.quad	_ZN2v88internal8NewSpaceD0Ev
	.quad	_ZN2v88internal19SpaceWithLinearArea21AddAllocationObserverEPNS0_18AllocationObserverE
	.quad	_ZN2v88internal19SpaceWithLinearArea24RemoveAllocationObserverEPNS0_18AllocationObserverE
	.quad	_ZN2v88internal19SpaceWithLinearArea24PauseAllocationObserversEv
	.quad	_ZN2v88internal19SpaceWithLinearArea25ResumeAllocationObserversEv
	.quad	_ZN2v88internal19SpaceWithLinearArea29StartNextInlineAllocationStepEv
	.quad	_ZN2v88internal8NewSpace15CommittedMemoryEv
	.quad	_ZN2v88internal8NewSpace22MaximumCommittedMemoryEv
	.quad	_ZN2v88internal8NewSpace4SizeEv
	.quad	_ZN2v88internal8NewSpace13SizeOfObjectsEv
	.quad	_ZN2v88internal8NewSpace23CommittedPhysicalMemoryEv
	.quad	_ZN2v88internal8NewSpace9AvailableEv
	.quad	_ZN2v88internal5Space30RoundSizeDownToObjectAlignmentEi
	.quad	_ZN2v88internal8NewSpace17GetObjectIteratorEv
	.quad	_ZNK2v88internal8NewSpace25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE
	.quad	_ZN2v88internal8NewSpace24SupportsInlineAllocationEv
	.quad	_ZN2v88internal8NewSpace27UpdateInlineAllocationLimitEm
	.weak	_ZTVN2v88internal8FreeListE
	.section	.data.rel.ro._ZTVN2v88internal8FreeListE,"awG",@progbits,_ZTVN2v88internal8FreeListE,comdat
	.align 8
	.type	_ZTVN2v88internal8FreeListE, @object
	.size	_ZTVN2v88internal8FreeListE, 96
_ZTVN2v88internal8FreeListE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal8FreeList4FreeEmmNS0_8FreeModeE
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal8FreeList5ResetEv
	.quad	_ZN2v88internal8FreeList11AddCategoryEPNS0_16FreeListCategoryE
	.quad	_ZN2v88internal8FreeList14RemoveCategoryEPNS0_16FreeListCategoryE
	.quad	__cxa_pure_virtual
	.weak	_ZTVN2v88internal14FreeListLegacyE
	.section	.data.rel.ro.local._ZTVN2v88internal14FreeListLegacyE,"awG",@progbits,_ZTVN2v88internal14FreeListLegacyE,comdat
	.align 8
	.type	_ZTVN2v88internal14FreeListLegacyE, @object
	.size	_ZTVN2v88internal14FreeListLegacyE, 96
_ZTVN2v88internal14FreeListLegacyE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14FreeListLegacyD1Ev
	.quad	_ZN2v88internal14FreeListLegacyD0Ev
	.quad	_ZN2v88internal14FreeListLegacy21GuaranteedAllocatableEm
	.quad	_ZN2v88internal8FreeList4FreeEmmNS0_8FreeModeE
	.quad	_ZN2v88internal14FreeListLegacy8AllocateEmPmNS0_16AllocationOriginE
	.quad	_ZN2v88internal14FreeListLegacy14GetPageForSizeEm
	.quad	_ZN2v88internal8FreeList5ResetEv
	.quad	_ZN2v88internal8FreeList11AddCategoryEPNS0_16FreeListCategoryE
	.quad	_ZN2v88internal8FreeList14RemoveCategoryEPNS0_16FreeListCategoryE
	.quad	_ZN2v88internal14FreeListLegacy26SelectFreeListCategoryTypeEm
	.weak	_ZTVN2v88internal17FreeListFastAllocE
	.section	.data.rel.ro.local._ZTVN2v88internal17FreeListFastAllocE,"awG",@progbits,_ZTVN2v88internal17FreeListFastAllocE,comdat
	.align 8
	.type	_ZTVN2v88internal17FreeListFastAllocE, @object
	.size	_ZTVN2v88internal17FreeListFastAllocE, 96
_ZTVN2v88internal17FreeListFastAllocE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal17FreeListFastAllocD1Ev
	.quad	_ZN2v88internal17FreeListFastAllocD0Ev
	.quad	_ZN2v88internal17FreeListFastAlloc21GuaranteedAllocatableEm
	.quad	_ZN2v88internal8FreeList4FreeEmmNS0_8FreeModeE
	.quad	_ZN2v88internal17FreeListFastAlloc8AllocateEmPmNS0_16AllocationOriginE
	.quad	_ZN2v88internal17FreeListFastAlloc14GetPageForSizeEm
	.quad	_ZN2v88internal8FreeList5ResetEv
	.quad	_ZN2v88internal8FreeList11AddCategoryEPNS0_16FreeListCategoryE
	.quad	_ZN2v88internal8FreeList14RemoveCategoryEPNS0_16FreeListCategoryE
	.quad	_ZN2v88internal17FreeListFastAlloc26SelectFreeListCategoryTypeEm
	.weak	_ZTVN2v88internal12FreeListManyE
	.section	.data.rel.ro.local._ZTVN2v88internal12FreeListManyE,"awG",@progbits,_ZTVN2v88internal12FreeListManyE,comdat
	.align 8
	.type	_ZTVN2v88internal12FreeListManyE, @object
	.size	_ZTVN2v88internal12FreeListManyE, 96
_ZTVN2v88internal12FreeListManyE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12FreeListManyD1Ev
	.quad	_ZN2v88internal12FreeListManyD0Ev
	.quad	_ZN2v88internal12FreeListMany21GuaranteedAllocatableEm
	.quad	_ZN2v88internal8FreeList4FreeEmmNS0_8FreeModeE
	.quad	_ZN2v88internal12FreeListMany8AllocateEmPmNS0_16AllocationOriginE
	.quad	_ZN2v88internal12FreeListMany14GetPageForSizeEm
	.quad	_ZN2v88internal8FreeList5ResetEv
	.quad	_ZN2v88internal8FreeList11AddCategoryEPNS0_16FreeListCategoryE
	.quad	_ZN2v88internal8FreeList14RemoveCategoryEPNS0_16FreeListCategoryE
	.quad	_ZN2v88internal12FreeListMany26SelectFreeListCategoryTypeEm
	.weak	_ZTVN2v88internal18FreeListManyCachedE
	.section	.data.rel.ro.local._ZTVN2v88internal18FreeListManyCachedE,"awG",@progbits,_ZTVN2v88internal18FreeListManyCachedE,comdat
	.align 8
	.type	_ZTVN2v88internal18FreeListManyCachedE, @object
	.size	_ZTVN2v88internal18FreeListManyCachedE, 96
_ZTVN2v88internal18FreeListManyCachedE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal18FreeListManyCachedD1Ev
	.quad	_ZN2v88internal18FreeListManyCachedD0Ev
	.quad	_ZN2v88internal12FreeListMany21GuaranteedAllocatableEm
	.quad	_ZN2v88internal18FreeListManyCached4FreeEmmNS0_8FreeModeE
	.quad	_ZN2v88internal18FreeListManyCached8AllocateEmPmNS0_16AllocationOriginE
	.quad	_ZN2v88internal12FreeListMany14GetPageForSizeEm
	.quad	_ZN2v88internal18FreeListManyCached5ResetEv
	.quad	_ZN2v88internal18FreeListManyCached11AddCategoryEPNS0_16FreeListCategoryE
	.quad	_ZN2v88internal18FreeListManyCached14RemoveCategoryEPNS0_16FreeListCategoryE
	.quad	_ZN2v88internal12FreeListMany26SelectFreeListCategoryTypeEm
	.weak	_ZTVN2v88internal26FreeListManyCachedFastPathE
	.section	.data.rel.ro.local._ZTVN2v88internal26FreeListManyCachedFastPathE,"awG",@progbits,_ZTVN2v88internal26FreeListManyCachedFastPathE,comdat
	.align 8
	.type	_ZTVN2v88internal26FreeListManyCachedFastPathE, @object
	.size	_ZTVN2v88internal26FreeListManyCachedFastPathE, 96
_ZTVN2v88internal26FreeListManyCachedFastPathE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal26FreeListManyCachedFastPathD1Ev
	.quad	_ZN2v88internal26FreeListManyCachedFastPathD0Ev
	.quad	_ZN2v88internal12FreeListMany21GuaranteedAllocatableEm
	.quad	_ZN2v88internal18FreeListManyCached4FreeEmmNS0_8FreeModeE
	.quad	_ZN2v88internal26FreeListManyCachedFastPath8AllocateEmPmNS0_16AllocationOriginE
	.quad	_ZN2v88internal12FreeListMany14GetPageForSizeEm
	.quad	_ZN2v88internal18FreeListManyCached5ResetEv
	.quad	_ZN2v88internal18FreeListManyCached11AddCategoryEPNS0_16FreeListCategoryE
	.quad	_ZN2v88internal18FreeListManyCached14RemoveCategoryEPNS0_16FreeListCategoryE
	.quad	_ZN2v88internal12FreeListMany26SelectFreeListCategoryTypeEm
	.weak	_ZTVN2v88internal24FreeListManyCachedOriginE
	.section	.data.rel.ro.local._ZTVN2v88internal24FreeListManyCachedOriginE,"awG",@progbits,_ZTVN2v88internal24FreeListManyCachedOriginE,comdat
	.align 8
	.type	_ZTVN2v88internal24FreeListManyCachedOriginE, @object
	.size	_ZTVN2v88internal24FreeListManyCachedOriginE, 96
_ZTVN2v88internal24FreeListManyCachedOriginE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal24FreeListManyCachedOriginD1Ev
	.quad	_ZN2v88internal24FreeListManyCachedOriginD0Ev
	.quad	_ZN2v88internal12FreeListMany21GuaranteedAllocatableEm
	.quad	_ZN2v88internal18FreeListManyCached4FreeEmmNS0_8FreeModeE
	.quad	_ZN2v88internal24FreeListManyCachedOrigin8AllocateEmPmNS0_16AllocationOriginE
	.quad	_ZN2v88internal12FreeListMany14GetPageForSizeEm
	.quad	_ZN2v88internal18FreeListManyCached5ResetEv
	.quad	_ZN2v88internal18FreeListManyCached11AddCategoryEPNS0_16FreeListCategoryE
	.quad	_ZN2v88internal18FreeListManyCached14RemoveCategoryEPNS0_16FreeListCategoryE
	.quad	_ZN2v88internal12FreeListMany26SelectFreeListCategoryTypeEm
	.weak	_ZTVN2v88internal11FreeListMapE
	.section	.data.rel.ro.local._ZTVN2v88internal11FreeListMapE,"awG",@progbits,_ZTVN2v88internal11FreeListMapE,comdat
	.align 8
	.type	_ZTVN2v88internal11FreeListMapE, @object
	.size	_ZTVN2v88internal11FreeListMapE, 96
_ZTVN2v88internal11FreeListMapE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11FreeListMapD1Ev
	.quad	_ZN2v88internal11FreeListMapD0Ev
	.quad	_ZN2v88internal11FreeListMap21GuaranteedAllocatableEm
	.quad	_ZN2v88internal8FreeList4FreeEmmNS0_8FreeModeE
	.quad	_ZN2v88internal11FreeListMap8AllocateEmPmNS0_16AllocationOriginE
	.quad	_ZN2v88internal11FreeListMap14GetPageForSizeEm
	.quad	_ZN2v88internal8FreeList5ResetEv
	.quad	_ZN2v88internal8FreeList11AddCategoryEPNS0_16FreeListCategoryE
	.quad	_ZN2v88internal8FreeList14RemoveCategoryEPNS0_16FreeListCategoryE
	.quad	_ZN2v88internal11FreeListMap26SelectFreeListCategoryTypeEm
	.weak	_ZTVN2v88internal15CompactionSpaceE
	.section	.data.rel.ro.local._ZTVN2v88internal15CompactionSpaceE,"awG",@progbits,_ZTVN2v88internal15CompactionSpaceE,comdat
	.align 8
	.type	_ZTVN2v88internal15CompactionSpaceE, @object
	.size	_ZTVN2v88internal15CompactionSpaceE, 208
_ZTVN2v88internal15CompactionSpaceE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal15CompactionSpaceD1Ev
	.quad	_ZN2v88internal15CompactionSpaceD0Ev
	.quad	_ZN2v88internal19SpaceWithLinearArea21AddAllocationObserverEPNS0_18AllocationObserverE
	.quad	_ZN2v88internal19SpaceWithLinearArea24RemoveAllocationObserverEPNS0_18AllocationObserverE
	.quad	_ZN2v88internal19SpaceWithLinearArea24PauseAllocationObserversEv
	.quad	_ZN2v88internal19SpaceWithLinearArea25ResumeAllocationObserversEv
	.quad	_ZN2v88internal19SpaceWithLinearArea29StartNextInlineAllocationStepEv
	.quad	_ZN2v88internal5Space15CommittedMemoryEv
	.quad	_ZN2v88internal5Space22MaximumCommittedMemoryEv
	.quad	_ZN2v88internal10PagedSpace4SizeEv
	.quad	_ZN2v88internal10PagedSpace13SizeOfObjectsEv
	.quad	_ZN2v88internal10PagedSpace23CommittedPhysicalMemoryEv
	.quad	_ZN2v88internal10PagedSpace9AvailableEv
	.quad	_ZN2v88internal5Space30RoundSizeDownToObjectAlignmentEi
	.quad	_ZN2v88internal10PagedSpace17GetObjectIteratorEv
	.quad	_ZNK2v88internal5Space25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE
	.quad	_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv
	.quad	_ZN2v88internal10PagedSpace27UpdateInlineAllocationLimitEm
	.quad	_ZN2v88internal10PagedSpace5WasteEv
	.quad	_ZN2v88internal15CompactionSpace8is_localEv
	.quad	_ZN2v88internal10PagedSpace14RefillFreeListEv
	.quad	_ZN2v88internal15CompactionSpace12snapshotableEv
	.quad	_ZN2v88internal15CompactionSpace23SweepAndRetryAllocationEiNS0_16AllocationOriginE
	.quad	_ZN2v88internal15CompactionSpace30SlowRefillLinearAllocationAreaEiNS0_16AllocationOriginE
	.weak	_ZTVN2v88internal30LargeObjectSpaceObjectIteratorE
	.section	.data.rel.ro.local._ZTVN2v88internal30LargeObjectSpaceObjectIteratorE,"awG",@progbits,_ZTVN2v88internal30LargeObjectSpaceObjectIteratorE,comdat
	.align 8
	.type	_ZTVN2v88internal30LargeObjectSpaceObjectIteratorE, @object
	.size	_ZTVN2v88internal30LargeObjectSpaceObjectIteratorE, 40
_ZTVN2v88internal30LargeObjectSpaceObjectIteratorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal30LargeObjectSpaceObjectIteratorD1Ev
	.quad	_ZN2v88internal30LargeObjectSpaceObjectIteratorD0Ev
	.quad	_ZN2v88internal30LargeObjectSpaceObjectIterator4NextEv
	.weak	_ZTVN2v88internal19NewLargeObjectSpaceE
	.section	.data.rel.ro.local._ZTVN2v88internal19NewLargeObjectSpaceE,"awG",@progbits,_ZTVN2v88internal19NewLargeObjectSpaceE,comdat
	.align 8
	.type	_ZTVN2v88internal19NewLargeObjectSpaceE, @object
	.size	_ZTVN2v88internal19NewLargeObjectSpaceE, 160
_ZTVN2v88internal19NewLargeObjectSpaceE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal19NewLargeObjectSpaceD1Ev
	.quad	_ZN2v88internal19NewLargeObjectSpaceD0Ev
	.quad	_ZN2v88internal5Space21AddAllocationObserverEPNS0_18AllocationObserverE
	.quad	_ZN2v88internal5Space24RemoveAllocationObserverEPNS0_18AllocationObserverE
	.quad	_ZN2v88internal5Space24PauseAllocationObserversEv
	.quad	_ZN2v88internal5Space25ResumeAllocationObserversEv
	.quad	_ZN2v88internal5Space29StartNextInlineAllocationStepEv
	.quad	_ZN2v88internal5Space15CommittedMemoryEv
	.quad	_ZN2v88internal5Space22MaximumCommittedMemoryEv
	.quad	_ZN2v88internal16LargeObjectSpace4SizeEv
	.quad	_ZN2v88internal16LargeObjectSpace13SizeOfObjectsEv
	.quad	_ZN2v88internal16LargeObjectSpace23CommittedPhysicalMemoryEv
	.quad	_ZN2v88internal19NewLargeObjectSpace9AvailableEv
	.quad	_ZN2v88internal5Space30RoundSizeDownToObjectAlignmentEi
	.quad	_ZN2v88internal16LargeObjectSpace17GetObjectIteratorEv
	.quad	_ZNK2v88internal5Space25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE
	.quad	_ZN2v88internal16LargeObjectSpace7AddPageEPNS0_9LargePageEm
	.quad	_ZN2v88internal16LargeObjectSpace10RemovePageEPNS0_9LargePageEm
	.weak	_ZTVN2v88internal20CodeLargeObjectSpaceE
	.section	.data.rel.ro.local._ZTVN2v88internal20CodeLargeObjectSpaceE,"awG",@progbits,_ZTVN2v88internal20CodeLargeObjectSpaceE,comdat
	.align 8
	.type	_ZTVN2v88internal20CodeLargeObjectSpaceE, @object
	.size	_ZTVN2v88internal20CodeLargeObjectSpaceE, 160
_ZTVN2v88internal20CodeLargeObjectSpaceE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal20CodeLargeObjectSpaceD1Ev
	.quad	_ZN2v88internal20CodeLargeObjectSpaceD0Ev
	.quad	_ZN2v88internal5Space21AddAllocationObserverEPNS0_18AllocationObserverE
	.quad	_ZN2v88internal5Space24RemoveAllocationObserverEPNS0_18AllocationObserverE
	.quad	_ZN2v88internal5Space24PauseAllocationObserversEv
	.quad	_ZN2v88internal5Space25ResumeAllocationObserversEv
	.quad	_ZN2v88internal5Space29StartNextInlineAllocationStepEv
	.quad	_ZN2v88internal5Space15CommittedMemoryEv
	.quad	_ZN2v88internal5Space22MaximumCommittedMemoryEv
	.quad	_ZN2v88internal16LargeObjectSpace4SizeEv
	.quad	_ZN2v88internal16LargeObjectSpace13SizeOfObjectsEv
	.quad	_ZN2v88internal16LargeObjectSpace23CommittedPhysicalMemoryEv
	.quad	_ZN2v88internal16LargeObjectSpace9AvailableEv
	.quad	_ZN2v88internal5Space30RoundSizeDownToObjectAlignmentEi
	.quad	_ZN2v88internal16LargeObjectSpace17GetObjectIteratorEv
	.quad	_ZNK2v88internal5Space25ExternalBackingStoreBytesENS0_24ExternalBackingStoreTypeE
	.quad	_ZN2v88internal20CodeLargeObjectSpace7AddPageEPNS0_9LargePageEm
	.quad	_ZN2v88internal20CodeLargeObjectSpace10RemovePageEPNS0_9LargePageEm
	.globl	_ZN2v88internal12FreeListMany14categories_minE
	.section	.rodata._ZN2v88internal12FreeListMany14categories_minE,"a"
	.align 32
	.type	_ZN2v88internal12FreeListMany14categories_minE, @object
	.size	_ZN2v88internal12FreeListMany14categories_minE, 96
_ZN2v88internal12FreeListMany14categories_minE:
	.long	24
	.long	32
	.long	48
	.long	64
	.long	80
	.long	96
	.long	112
	.long	128
	.long	144
	.long	160
	.long	176
	.long	192
	.long	208
	.long	224
	.long	240
	.long	256
	.long	512
	.long	1024
	.long	2048
	.long	4096
	.long	8192
	.long	16384
	.long	32768
	.long	65536
	.weak	_ZZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTask11RunInternalEvE28trace_event_unique_atomic262
	.section	.bss._ZZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTask11RunInternalEvE28trace_event_unique_atomic262,"awG",@nobits,_ZZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTask11RunInternalEvE28trace_event_unique_atomic262,comdat
	.align 8
	.type	_ZZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTask11RunInternalEvE28trace_event_unique_atomic262, @gnu_unique_object
	.size	_ZZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTask11RunInternalEvE28trace_event_unique_atomic262, 8
_ZZN2v88internal15MemoryAllocator8Unmapper19UnmapFreeMemoryTask11RunInternalEvE28trace_event_unique_atomic262:
	.zero	8
	.section	.bss._ZN2v88internalL23code_range_address_hintE,"aw",@nobits
	.align 32
	.type	_ZN2v88internalL23code_range_address_hintE, @object
	.size	_ZN2v88internalL23code_range_address_hintE, 104
_ZN2v88internalL23code_range_address_hintE:
	.zero	104
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC6:
	.long	24
	.long	24
	.long	24
	.long	24
	.align 16
.LC23:
	.quad	24
	.quad	0
	.align 16
.LC24:
	.quad	2040
	.quad	0
	.align 16
.LC26:
	.quad	80
	.quad	0
	.align 16
.LC45:
	.quad	-262144
	.quad	-262144
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
