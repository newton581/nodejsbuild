	.file	"string-comparator.cc"
	.text
	.section	.rodata._ZN2v88internal16StringComparator5State4InitENS0_6StringE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal16StringComparator5State4InitENS0_6StringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16StringComparator5State4InitENS0_6StringE
	.type	_ZN2v88internal16StringComparator5State4InitENS0_6StringE, @function
_ZN2v88internal16StringComparator5State4InitENS0_6StringE:
.LFB17792:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.L5(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	11(%rsi), %r13d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.L2:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L3
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal16StringComparator5State4InitENS0_6StringE,"a",@progbits
	.align 4
	.align 4
.L5:
	.long	.L11-.L5
	.long	.L8-.L5
	.long	.L10-.L5
	.long	.L6-.L5
	.long	.L3-.L5
	.long	.L4-.L5
	.long	.L3-.L5
	.long	.L3-.L5
	.long	.L9-.L5
	.long	.L8-.L5
	.long	.L7-.L5
	.long	.L6-.L5
	.long	.L3-.L5
	.long	.L4-.L5
	.section	.text._ZN2v88internal16StringComparator5State4InitENS0_6StringE
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$0, 264(%rbx)
	testq	%rsi, %rsi
	je	.L1
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal18ConsStringIterator10InitializeENS0_10ConsStringEi@PLT
	movl	264(%rbx), %ecx
	movl	$0, -44(%rbp)
	xorl	%eax, %eax
	movl	$11, %edx
	testl	%ecx, %ecx
	jne	.L36
.L16:
	movl	(%rdx), %edi
	movl	%ecx, %r12d
	leaq	.L19(%rip), %rsi
.L17:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L3
	movzwl	%dx, %edx
	movslq	(%rsi,%rdx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal16StringComparator5State4InitENS0_6StringE
	.align 4
	.align 4
.L19:
	.long	.L25-.L19
	.long	.L1-.L19
	.long	.L24-.L19
	.long	.L20-.L19
	.long	.L3-.L19
	.long	.L18-.L19
	.long	.L3-.L19
	.long	.L3-.L19
	.long	.L23-.L19
	.long	.L1-.L19
	.long	.L21-.L19
	.long	.L20-.L19
	.long	.L3-.L19
	.long	.L18-.L19
	.section	.text._ZN2v88internal16StringComparator5State4InitENS0_6StringE
	.p2align 4,,10
	.p2align 3
.L7:
	movq	15(%rsi), %rdi
	movslq	%r12d, %r12
	movq	(%rdi), %rax
	call	*48(%rax)
	movb	$1, 280(%rbx)
	addq	%r12, %rax
	movl	%r13d, 284(%rbx)
	movq	%rax, 288(%rbx)
.L12:
	movl	$0, 264(%rbx)
.L1:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L37
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	movq	15(%rsi), %rsi
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L6:
	addl	27(%rsi), %r12d
	movq	15(%rsi), %rsi
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L10:
	movq	15(%rsi), %rdi
	movslq	%r12d, %r12
	movq	(%rdi), %rax
	call	*48(%rax)
	movb	$0, 280(%rbx)
	leaq	(%rax,%r12,2), %rax
	movl	%r13d, 284(%rbx)
	movq	%rax, 288(%rbx)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L11:
	movslq	%r12d, %r12
	movb	$0, 280(%rbx)
	leaq	15(%rsi,%r12,2), %rax
	movl	%r13d, 284(%rbx)
	movq	%rax, 288(%rbx)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L9:
	movslq	%r12d, %r12
	movb	$1, 280(%rbx)
	leaq	15(%rsi,%r12), %rax
	movl	%r13d, 284(%rbx)
	movq	%rax, 288(%rbx)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L18:
	movq	15(%rax), %rax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L20:
	addl	27(%rax), %r12d
	movq	15(%rax), %rax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L24:
	subl	%ecx, %edi
	movslq	%r12d, %r12
	movl	%edi, %r13d
	movq	15(%rax), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movb	$0, 280(%rbx)
	leaq	(%rax,%r12,2), %rax
	movl	%r13d, 284(%rbx)
	movq	%rax, 288(%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L25:
	movslq	%r12d, %r12
	subl	%ecx, %edi
	movb	$0, 280(%rbx)
	leaq	15(%rax,%r12,2), %rax
	movl	%edi, 284(%rbx)
	movq	%rax, 288(%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L21:
	subl	%ecx, %edi
	movslq	%r12d, %r12
	movl	%edi, %r13d
	movq	15(%rax), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movb	$1, 280(%rbx)
	addq	%r12, %rax
	movl	%r13d, 284(%rbx)
	movq	%rax, 288(%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L23:
	movslq	%r12d, %r12
	subl	%ecx, %edi
	movb	$1, 280(%rbx)
	leaq	15(%rax,%r12), %rax
	movl	%edi, 284(%rbx)
	movq	%rax, 288(%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	-44(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal18ConsStringIterator8ContinueEPi@PLT
	movl	-44(%rbp), %ecx
	leaq	11(%rax), %rdx
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L3:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L37:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17792:
	.size	_ZN2v88internal16StringComparator5State4InitENS0_6StringE, .-_ZN2v88internal16StringComparator5State4InitENS0_6StringE
	.section	.text._ZN2v88internal16StringComparator5State7AdvanceEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16StringComparator5State7AdvanceEi
	.type	_ZN2v88internal16StringComparator5State7AdvanceEi, @function
_ZN2v88internal16StringComparator5State7AdvanceEi:
.LFB17793:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	284(%rdi), %eax
	cmpl	%esi, %eax
	je	.L39
	cmpb	$0, 280(%rdi)
	movq	288(%rdi), %rdx
	movslq	%esi, %rcx
	jne	.L58
	leaq	(%rdx,%rcx,2), %rdx
	movq	%rdx, 288(%rdi)
.L41:
	subl	%esi, %eax
	movl	%eax, 284(%rbx)
.L38:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L59
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movl	264(%rdi), %ecx
	movl	$0, -44(%rbp)
	movl	$11, %edx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jne	.L60
.L43:
	movl	(%rdx), %r13d
	xorl	%r12d, %r12d
	leaq	.L47(%rip), %rcx
.L44:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L45
	movzwl	%dx, %edx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal16StringComparator5State7AdvanceEi,"a",@progbits
	.align 4
	.align 4
.L47:
	.long	.L53-.L47
	.long	.L38-.L47
	.long	.L52-.L47
	.long	.L48-.L47
	.long	.L45-.L47
	.long	.L46-.L47
	.long	.L45-.L47
	.long	.L45-.L47
	.long	.L51-.L47
	.long	.L38-.L47
	.long	.L49-.L47
	.long	.L48-.L47
	.long	.L45-.L47
	.long	.L46-.L47
	.section	.text._ZN2v88internal16StringComparator5State7AdvanceEi
	.p2align 4,,10
	.p2align 3
.L58:
	addq	%rcx, %rdx
	movq	%rdx, 288(%rdi)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L60:
	leaq	-44(%rbp), %rsi
	call	_ZN2v88internal18ConsStringIterator8ContinueEPi@PLT
	leaq	11(%rax), %rdx
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L48:
	addl	27(%rax), %r12d
	movq	15(%rax), %rax
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L46:
	movq	15(%rax), %rax
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L49:
	movq	15(%rax), %rdi
	movslq	%r12d, %r12
	movq	(%rdi), %rax
	call	*48(%rax)
	movb	$1, 280(%rbx)
	addq	%r12, %rax
	movl	%r13d, 284(%rbx)
	movq	%rax, 288(%rbx)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L53:
	movslq	%r12d, %r12
	movb	$0, 280(%rbx)
	leaq	15(%rax,%r12,2), %rax
	movl	%r13d, 284(%rbx)
	movq	%rax, 288(%rbx)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L52:
	movq	15(%rax), %rdi
	movslq	%r12d, %r12
	movq	(%rdi), %rax
	call	*48(%rax)
	movb	$0, 280(%rbx)
	leaq	(%rax,%r12,2), %rax
	movl	%r13d, 284(%rbx)
	movq	%rax, 288(%rbx)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L51:
	movslq	%r12d, %r12
	movb	$1, 280(%rbx)
	leaq	15(%rax,%r12), %rax
	movl	%r13d, 284(%rbx)
	movq	%rax, 288(%rbx)
	jmp	.L38
.L45:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L59:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17793:
	.size	_ZN2v88internal16StringComparator5State7AdvanceEi, .-_ZN2v88internal16StringComparator5State7AdvanceEi
	.section	.text._ZN2v88internal16StringComparator6EqualsENS0_6StringES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16StringComparator6EqualsENS0_6StringES2_
	.type	_ZN2v88internal16StringComparator6EqualsENS0_6StringES2_, @function
_ZN2v88internal16StringComparator6EqualsENS0_6StringES2_:
.LFB17794:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	leaq	.L65(%rip), %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movl	11(%rsi), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.L62:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L63
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal16StringComparator6EqualsENS0_6StringES2_,"a",@progbits
	.align 4
	.align 4
.L65:
	.long	.L71-.L65
	.long	.L68-.L65
	.long	.L70-.L65
	.long	.L66-.L65
	.long	.L63-.L65
	.long	.L64-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L69-.L65
	.long	.L68-.L65
	.long	.L67-.L65
	.long	.L66-.L65
	.long	.L63-.L65
	.long	.L64-.L65
	.section	.text._ZN2v88internal16StringComparator6EqualsENS0_6StringES2_
.L68:
	movl	$0, 264(%rbx)
	testq	%rsi, %rsi
	je	.L74
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal18ConsStringIterator10InitializeENS0_10ConsStringEi@PLT
	movl	264(%rbx), %ecx
	movl	$0, -68(%rbp)
	xorl	%edx, %edx
	movl	$11, %eax
	testl	%ecx, %ecx
	jne	.L176
.L76:
	movl	(%rax), %r14d
	movl	%ecx, %r15d
	leaq	.L79(%rip), %rsi
.L77:
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L63
	movzwl	%ax, %eax
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal16StringComparator6EqualsENS0_6StringES2_
	.align 4
	.align 4
.L79:
	.long	.L85-.L79
	.long	.L74-.L79
	.long	.L84-.L79
	.long	.L80-.L79
	.long	.L63-.L79
	.long	.L78-.L79
	.long	.L63-.L79
	.long	.L63-.L79
	.long	.L83-.L79
	.long	.L74-.L79
	.long	.L81-.L79
	.long	.L80-.L79
	.long	.L63-.L79
	.long	.L78-.L79
	.section	.text._ZN2v88internal16StringComparator6EqualsENS0_6StringES2_
.L70:
	movq	15(%rsi), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movb	$0, 280(%rbx)
	movq	%rax, %r8
	movslq	%r14d, %rax
	movl	%r13d, 284(%rbx)
	leaq	(%r8,%rax,2), %rax
	movq	%rax, 288(%rbx)
.L72:
	movl	$0, 264(%rbx)
.L74:
	leaq	296(%rbx), %rax
	movl	11(%r12), %r15d
	xorl	%r14d, %r14d
	leaq	.L89(%rip), %rdx
	movq	%rax, -96(%rbp)
.L87:
	movq	-1(%r12), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L63
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal16StringComparator6EqualsENS0_6StringES2_
	.align 4
	.align 4
.L89:
	.long	.L95-.L89
	.long	.L92-.L89
	.long	.L94-.L89
	.long	.L90-.L89
	.long	.L63-.L89
	.long	.L88-.L89
	.long	.L63-.L89
	.long	.L63-.L89
	.long	.L93-.L89
	.long	.L92-.L89
	.long	.L91-.L89
	.long	.L90-.L89
	.long	.L63-.L89
	.long	.L88-.L89
	.section	.text._ZN2v88internal16StringComparator6EqualsENS0_6StringES2_
.L92:
	movl	$0, 560(%rbx)
	testq	%r12, %r12
	je	.L106
	movq	-96(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal18ConsStringIterator10InitializeENS0_10ConsStringEi@PLT
	movl	560(%rbx), %ecx
	movl	$0, -72(%rbp)
	xorl	%edx, %edx
	movl	$11, %eax
	testl	%ecx, %ecx
	jne	.L177
.L100:
	movl	(%rax), %r15d
	movl	%ecx, %r12d
	leaq	.L103(%rip), %rsi
.L101:
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L63
	movzwl	%ax, %eax
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal16StringComparator6EqualsENS0_6StringES2_
	.align 4
	.align 4
.L103:
	.long	.L109-.L103
	.long	.L106-.L103
	.long	.L108-.L103
	.long	.L104-.L103
	.long	.L63-.L103
	.long	.L102-.L103
	.long	.L63-.L103
	.long	.L63-.L103
	.long	.L107-.L103
	.long	.L106-.L103
	.long	.L105-.L103
	.long	.L104-.L103
	.long	.L63-.L103
	.long	.L102-.L103
	.section	.text._ZN2v88internal16StringComparator6EqualsENS0_6StringES2_
.L64:
	movq	15(%rsi), %rsi
	jmp	.L62
.L66:
	addl	27(%rsi), %r14d
	movq	15(%rsi), %rsi
	jmp	.L62
.L88:
	movq	15(%r12), %r12
	jmp	.L87
.L90:
	addl	27(%r12), %r14d
	movq	15(%r12), %r12
	jmp	.L87
.L91:
	movq	15(%r12), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movb	$1, 576(%rbx)
	movl	$1, %r10d
	movq	%rax, %r8
	movslq	%r14d, %rax
	movl	%r15d, 580(%rbx)
	leaq	(%r8,%rax), %rsi
	movq	%rsi, 584(%rbx)
.L96:
	movl	$0, 560(%rbx)
.L99:
	movl	%r13d, %ecx
	.p2align 4,,10
	.p2align 3
.L112:
	movl	284(%rbx), %r13d
	movzbl	280(%rbx), %r8d
	movl	%r15d, %r12d
	movq	288(%rbx), %r14
	cmpl	%r15d, %r13d
	cmovle	%r13d, %r12d
	testb	%r8b, %r8b
	je	.L113
	testb	%r10b, %r10b
	je	.L114
	movslq	%r12d, %rdx
	movq	%r14, %rdi
	movl	%ecx, -88(%rbp)
	movb	%r8b, -84(%rbp)
	call	memcmp@PLT
	movzbl	-84(%rbp), %r8d
	movl	-88(%rbp), %ecx
	testl	%eax, %eax
	jne	.L155
.L115:
	subl	%r12d, %ecx
	je	.L156
	cmpl	%r15d, %r13d
	jle	.L121
	movslq	%r12d, %rax
	testb	%r8b, %r8b
	je	.L122
	addq	%rax, %r14
	movq	%r14, 288(%rbx)
.L123:
	subl	%r12d, %r13d
	movl	%r13d, 284(%rbx)
	cmpl	%r15d, %r12d
	je	.L136
.L181:
	movzbl	576(%rbx), %r10d
	movq	584(%rbx), %rsi
	movslq	%r12d, %rax
	testb	%r10b, %r10b
	je	.L137
	addq	%rax, %rsi
	movq	%rsi, 584(%rbx)
.L138:
	subl	%r12d, %r15d
	movl	%r15d, 580(%rbx)
	jmp	.L112
.L93:
	movslq	%r14d, %rax
	movb	$1, 576(%rbx)
	movl	$1, %r10d
	leaq	15(%r12,%rax), %rsi
	movl	%r15d, 580(%rbx)
	movq	%rsi, 584(%rbx)
	jmp	.L96
.L94:
	movq	15(%r12), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movb	$0, 576(%rbx)
	xorl	%r10d, %r10d
	movq	%rax, %r8
	movslq	%r14d, %rax
	movl	%r15d, 580(%rbx)
	leaq	(%r8,%rax,2), %rsi
	movq	%rsi, 584(%rbx)
	jmp	.L96
.L95:
	movslq	%r14d, %rax
	movb	$0, 576(%rbx)
	xorl	%r10d, %r10d
	leaq	15(%r12,%rax,2), %rsi
	movl	%r15d, 580(%rbx)
	movq	%rsi, 584(%rbx)
	jmp	.L96
.L71:
	movslq	%r14d, %rax
	movb	$0, 280(%rbx)
	leaq	15(%rsi,%rax,2), %rax
	movl	%r13d, 284(%rbx)
	movq	%rax, 288(%rbx)
	jmp	.L72
.L69:
	movslq	%r14d, %rax
	movb	$1, 280(%rbx)
	leaq	15(%rsi,%rax), %rax
	movl	%r13d, 284(%rbx)
	movq	%rax, 288(%rbx)
	jmp	.L72
.L67:
	movq	15(%rsi), %rdi
	movslq	%r14d, %r14
	movq	(%rdi), %rax
	call	*48(%rax)
	movb	$1, 280(%rbx)
	addq	%r14, %rax
	movl	%r13d, 284(%rbx)
	movq	%rax, 288(%rbx)
	jmp	.L72
.L78:
	movq	15(%rdx), %rdx
	jmp	.L77
.L80:
	addl	27(%rdx), %r15d
	movq	15(%rdx), %rdx
	jmp	.L77
.L102:
	movq	15(%rdx), %rdx
	jmp	.L101
.L104:
	addl	27(%rdx), %r12d
	movq	15(%rdx), %rdx
	jmp	.L101
.L106:
	movl	580(%rbx), %r15d
	movzbl	576(%rbx), %r10d
	movq	584(%rbx), %rsi
	jmp	.L99
.L105:
	movq	15(%rdx), %rdi
	subl	%ecx, %r15d
	movslq	%r12d, %r12
	movq	(%rdi), %rax
	call	*48(%rax)
	movb	$1, 576(%rbx)
	movl	$1, %r10d
	leaq	(%rax,%r12), %rsi
	movl	%r15d, 580(%rbx)
	movq	%rsi, 584(%rbx)
	jmp	.L99
.L107:
	movslq	%r12d, %r12
	subl	%ecx, %r15d
	movb	$1, 576(%rbx)
	movl	$1, %r10d
	leaq	15(%rdx,%r12), %rsi
	movl	%r15d, 580(%rbx)
	movq	%rsi, 584(%rbx)
	jmp	.L99
.L84:
	movq	15(%rdx), %rdi
	subl	%ecx, %r14d
	movslq	%r15d, %r15
	movq	(%rdi), %rax
	call	*48(%rax)
	movb	$0, 280(%rbx)
	leaq	(%rax,%r15,2), %rax
	movl	%r14d, 284(%rbx)
	movq	%rax, 288(%rbx)
	jmp	.L74
.L108:
	movq	15(%rdx), %rdi
	subl	%ecx, %r15d
	movslq	%r12d, %r12
	movq	(%rdi), %rax
	call	*48(%rax)
	movb	$0, 576(%rbx)
	xorl	%r10d, %r10d
	leaq	(%rax,%r12,2), %rsi
	movl	%r15d, 580(%rbx)
	movq	%rsi, 584(%rbx)
	jmp	.L99
.L109:
	movslq	%r12d, %r12
	subl	%ecx, %r15d
	movb	$0, 576(%rbx)
	xorl	%r10d, %r10d
	leaq	15(%rdx,%r12,2), %rsi
	movl	%r15d, 580(%rbx)
	movq	%rsi, 584(%rbx)
	jmp	.L99
.L81:
	movq	15(%rdx), %rdi
	subl	%ecx, %r14d
	movslq	%r15d, %r15
	movq	(%rdi), %rax
	call	*48(%rax)
	movb	$1, 280(%rbx)
	addq	%r15, %rax
	movl	%r14d, 284(%rbx)
	movq	%rax, 288(%rbx)
	jmp	.L74
.L83:
	movslq	%r15d, %r15
	subl	%ecx, %r14d
	movb	$1, 280(%rbx)
	leaq	15(%rdx,%r15), %rax
	movl	%r14d, 284(%rbx)
	movq	%rax, 288(%rbx)
	jmp	.L74
.L85:
	movslq	%r15d, %r15
	subl	%ecx, %r14d
	movb	$0, 280(%rbx)
	leaq	15(%rdx,%r15,2), %rax
	movl	%r14d, 284(%rbx)
	movq	%rax, 288(%rbx)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L113:
	testb	%r10b, %r10b
	je	.L118
	testl	%r12d, %r12d
	jle	.L115
	leal	-1(%r12), %edi
	xorl	%eax, %eax
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L178:
	leaq	1(%rax), %rdx
	cmpq	%rax, %rdi
	je	.L115
	movq	%rdx, %rax
.L119:
	movzbl	(%rsi,%rax), %edx
	cmpw	%dx, (%r14,%rax,2)
	je	.L178
.L155:
	xorl	%r10d, %r10d
.L61:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L179
	addq	$56, %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movl	264(%rbx), %edi
	movl	$0, -64(%rbp)
	movl	$11, %edx
	xorl	%eax, %eax
	testl	%edi, %edi
	jne	.L180
.L125:
	movl	(%rdx), %r14d
	xorl	%r13d, %r13d
.L126:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L63
	leaq	.L128(%rip), %rdi
	movzwl	%dx, %edx
	movslq	(%rdi,%rdx,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal16StringComparator6EqualsENS0_6StringES2_
	.align 4
	.align 4
.L128:
	.long	.L134-.L128
	.long	.L131-.L128
	.long	.L133-.L128
	.long	.L129-.L128
	.long	.L63-.L128
	.long	.L127-.L128
	.long	.L63-.L128
	.long	.L63-.L128
	.long	.L132-.L128
	.long	.L131-.L128
	.long	.L130-.L128
	.long	.L129-.L128
	.long	.L63-.L128
	.long	.L127-.L128
	.section	.text._ZN2v88internal16StringComparator6EqualsENS0_6StringES2_
	.p2align 4,,10
	.p2align 3
.L132:
	movslq	%r13d, %r13
	movb	$1, 280(%rbx)
	leaq	15(%rax,%r13), %rax
	movl	%r14d, 284(%rbx)
	movq	%rax, 288(%rbx)
.L131:
	movl	580(%rbx), %r15d
.L183:
	cmpl	%r15d, %r12d
	jne	.L181
.L136:
	movl	560(%rbx), %esi
	movl	$0, -60(%rbp)
	movl	$11, %edx
	xorl	%eax, %eax
	testl	%esi, %esi
	jne	.L182
.L140:
	movl	(%rdx), %r15d
	xorl	%r12d, %r12d
.L141:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L63
	leaq	.L143(%rip), %rsi
	movzwl	%dx, %edx
	movslq	(%rsi,%rdx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal16StringComparator6EqualsENS0_6StringES2_
	.align 4
	.align 4
.L143:
	.long	.L149-.L143
	.long	.L146-.L143
	.long	.L148-.L143
	.long	.L144-.L143
	.long	.L63-.L143
	.long	.L142-.L143
	.long	.L63-.L143
	.long	.L63-.L143
	.long	.L147-.L143
	.long	.L146-.L143
	.long	.L145-.L143
	.long	.L144-.L143
	.long	.L63-.L143
	.long	.L142-.L143
	.section	.text._ZN2v88internal16StringComparator6EqualsENS0_6StringES2_
	.p2align 4,,10
	.p2align 3
.L127:
	movq	15(%rax), %rax
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L129:
	addl	27(%rax), %r13d
	movq	15(%rax), %rax
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L133:
	movq	15(%rax), %rdi
	movl	%ecx, -84(%rbp)
	movslq	%r13d, %r13
	movq	(%rdi), %rax
	call	*48(%rax)
	movb	$0, 280(%rbx)
	movl	-84(%rbp), %ecx
	leaq	(%rax,%r13,2), %rax
	movl	%r14d, 284(%rbx)
	movl	580(%rbx), %r15d
	movq	%rax, 288(%rbx)
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L130:
	movq	15(%rax), %rdi
	movl	%ecx, -84(%rbp)
	movslq	%r13d, %r13
	movq	(%rdi), %rax
	call	*48(%rax)
	movb	$1, 280(%rbx)
	movl	-84(%rbp), %ecx
	addq	%r13, %rax
	movl	%r14d, 284(%rbx)
	movl	580(%rbx), %r15d
	movq	%rax, 288(%rbx)
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L134:
	movslq	%r13d, %r13
	movb	$0, 280(%rbx)
	movl	580(%rbx), %r15d
	leaq	15(%rax,%r13,2), %rax
	movl	%r14d, 284(%rbx)
	movq	%rax, 288(%rbx)
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L142:
	movq	15(%rax), %rax
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L144:
	addl	27(%rax), %r12d
	movq	15(%rax), %rax
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L146:
	movl	580(%rbx), %r15d
	movzbl	576(%rbx), %r10d
	movq	584(%rbx), %rsi
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L148:
	movq	15(%rax), %rdi
	movl	%ecx, -84(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movb	$0, 576(%rbx)
	movl	-84(%rbp), %ecx
	xorl	%r10d, %r10d
	movq	%rax, %r8
	movslq	%r12d, %rax
	movl	%r15d, 580(%rbx)
	leaq	(%r8,%rax,2), %rsi
	movq	%rsi, 584(%rbx)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L149:
	movslq	%r12d, %r12
	movb	$0, 576(%rbx)
	xorl	%r10d, %r10d
	leaq	15(%rax,%r12,2), %rsi
	movl	%r15d, 580(%rbx)
	movq	%rsi, 584(%rbx)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L145:
	movq	15(%rax), %rdi
	movl	%ecx, -84(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movb	$1, 576(%rbx)
	movl	-84(%rbp), %ecx
	movl	$1, %r10d
	movq	%rax, %r8
	movslq	%r12d, %rax
	movl	%r15d, 580(%rbx)
	leaq	(%r8,%rax), %rsi
	movq	%rsi, 584(%rbx)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L147:
	movslq	%r12d, %r12
	movb	$1, 576(%rbx)
	movl	$1, %r10d
	leaq	15(%rax,%r12), %rsi
	movl	%r15d, 580(%rbx)
	movq	%rsi, 584(%rbx)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L118:
	movslq	%r12d, %rax
	leaq	(%r14,%rax,2), %rdx
	cmpq	%r14, %rdx
	jbe	.L115
	movq	%r14, %rax
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L184:
	addq	$2, %rax
	addq	$2, %rsi
	cmpq	%rax, %rdx
	jbe	.L115
.L120:
	movzwl	(%rsi), %edi
	cmpw	%di, (%rax)
	je	.L184
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L122:
	leaq	(%r14,%rax,2), %rax
	movq	%rax, 288(%rbx)
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L137:
	leaq	(%rsi,%rax,2), %rsi
	movq	%rsi, 584(%rbx)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L180:
	leaq	-64(%rbp), %rsi
	movq	%rbx, %rdi
	movl	%ecx, -84(%rbp)
	call	_ZN2v88internal18ConsStringIterator8ContinueEPi@PLT
	movl	-84(%rbp), %ecx
	leaq	11(%rax), %rdx
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L182:
	movq	-96(%rbp), %rdi
	leaq	-60(%rbp), %rsi
	movl	%ecx, -84(%rbp)
	call	_ZN2v88internal18ConsStringIterator8ContinueEPi@PLT
	movl	-84(%rbp), %ecx
	leaq	11(%rax), %rdx
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L114:
	testl	%r12d, %r12d
	jle	.L115
	leal	-1(%r12), %r9d
	xorl	%eax, %eax
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L185:
	leaq	1(%rax), %rdx
	cmpq	%rax, %r9
	je	.L115
	movq	%rdx, %rax
.L117:
	movzbl	(%r14,%rax), %edi
	movzwl	(%rsi,%rax,2), %edx
	cmpl	%edx, %edi
	je	.L185
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L156:
	movl	$1, %r10d
	jmp	.L61
.L177:
	movq	-96(%rbp), %rdi
	leaq	-72(%rbp), %rsi
	call	_ZN2v88internal18ConsStringIterator8ContinueEPi@PLT
	movl	-72(%rbp), %ecx
	movq	%rax, %rdx
	leaq	11(%rax), %rax
	jmp	.L100
.L176:
	leaq	-68(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal18ConsStringIterator8ContinueEPi@PLT
	movl	-68(%rbp), %ecx
	movq	%rax, %rdx
	leaq	11(%rax), %rax
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L63:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L179:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17794:
	.size	_ZN2v88internal16StringComparator6EqualsENS0_6StringES2_, .-_ZN2v88internal16StringComparator6EqualsENS0_6StringES2_
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal16StringComparator5State4InitENS0_6StringE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal16StringComparator5State4InitENS0_6StringE, @function
_GLOBAL__sub_I__ZN2v88internal16StringComparator5State4InitENS0_6StringE:
.LFB21466:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21466:
	.size	_GLOBAL__sub_I__ZN2v88internal16StringComparator5State4InitENS0_6StringE, .-_GLOBAL__sub_I__ZN2v88internal16StringComparator5State4InitENS0_6StringE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal16StringComparator5State4InitENS0_6StringE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
