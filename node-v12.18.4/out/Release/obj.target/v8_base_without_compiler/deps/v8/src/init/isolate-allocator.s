	.file	"isolate-allocator.cc"
	.text
	.section	.text._ZN2v84base20BoundedPageAllocatorD0Ev,"axG",@progbits,_ZN2v84base20BoundedPageAllocatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base20BoundedPageAllocatorD0Ev
	.type	_ZN2v84base20BoundedPageAllocatorD0Ev, @function
_ZN2v84base20BoundedPageAllocatorD0Ev:
.LFB9548:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v84base20BoundedPageAllocatorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	72(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -72(%rdi)
	call	_ZN2v84base15RegionAllocatorD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$224, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9548:
	.size	_ZN2v84base20BoundedPageAllocatorD0Ev, .-_ZN2v84base20BoundedPageAllocatorD0Ev
	.section	.text._ZN2v88internal16IsolateAllocatorD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16IsolateAllocatorD2Ev
	.type	_ZN2v88internal16IsolateAllocatorD2Ev, @function
_ZN2v88internal16IsolateAllocatorD2Ev:
.LFB8613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	24(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	$0, 32(%rdi)
	movq	%rdi, %rbx
	jne	.L19
	movq	(%rdi), %rdi
	call	_ZdlPv@PLT
.L19:
	movq	%r12, %rdi
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L4
	movq	(%r12), %rax
	leaq	_ZN2v84base20BoundedPageAllocatorD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L10
	leaq	16+_ZTVN2v84base20BoundedPageAllocatorE(%rip), %rax
	leaq	72(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZN2v84base15RegionAllocatorD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$224, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8613:
	.size	_ZN2v88internal16IsolateAllocatorD2Ev, .-_ZN2v88internal16IsolateAllocatorD2Ev
	.globl	_ZN2v88internal16IsolateAllocatorD1Ev
	.set	_ZN2v88internal16IsolateAllocatorD1Ev,_ZN2v88internal16IsolateAllocatorD2Ev
	.section	.rodata._ZN2v88internal16IsolateAllocator15InitReservationEv.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"padded_reservation.InVM(address, reservation_size)"
	.section	.rodata._ZN2v88internal16IsolateAllocator15InitReservationEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internal16IsolateAllocator15InitReservationEv.str1.8
	.align 8
.LC2:
	.string	"reservation_.size() == reservation_size"
	.align 8
.LC3:
	.string	"Failed to reserve memory for new V8 Isolate"
	.section	.text._ZN2v88internal16IsolateAllocator15InitReservationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16IsolateAllocator15InitReservationEv
	.type	_ZN2v88internal16IsolateAllocator15InitReservationEv, @function
_ZN2v88internal16IsolateAllocator15InitReservationEv:
.LFB8615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movabsq	$6442450943, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	$4, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movabsq	$-4294967296, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal24GetPlatformPageAllocatorEv@PLT
	movq	%rax, %rbx
.L29:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	movl	$2147483648, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	andq	%r13, %rax
	movl	$1, %r8d
	leaq	(%rax,%rdx), %rcx
	movabsq	$8589934592, %rdx
	call	_ZN2v88internal13VirtualMemoryC1EPNS_13PageAllocatorEmPvm@PLT
	movq	-104(%rbp), %rax
	testq	%rax, %rax
	je	.L21
	leaq	(%rax,%r15), %rcx
	movq	-96(%rbp), %rdx
	andq	%r13, %rcx
	addq	$-2147483648, %rcx
	movq	%rcx, %rsi
	subq	%rax, %rsi
	movq	%rsi, %rax
	cmpq	%rsi, %rdx
	jbe	.L22
	movabsq	$4294967296, %rsi
	addq	%rsi, %rax
	cmpq	%rdx, %rax
	jbe	.L36
.L22:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L36:
	cmpl	$1, %r14d
	je	.L37
	movq	%r12, %rdi
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal13VirtualMemory4FreeEv@PLT
	movq	-120(%rbp), %rcx
	leaq	-80(%rbp), %rdi
	movq	%rbx, %rsi
	movl	$1, %r8d
	movq	%rdi, -120(%rbp)
	movabsq	$4294967296, %rdx
	call	_ZN2v88internal13VirtualMemoryC1EPNS_13PageAllocatorEmPvm@PLT
	movq	-72(%rbp), %rcx
	movq	-120(%rbp), %rdi
	testq	%rcx, %rcx
	je	.L26
	leaq	(%rcx,%r15), %rax
	andq	%r13, %rax
	addq	$-2147483648, %rax
	cmpq	%rcx, %rax
	je	.L38
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
	movq	%r12, %rdi
	subl	$1, %r14d
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
	jmp	.L29
.L26:
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
.L21:
	movq	%r12, %rdi
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
	xorl	%edx, %edx
	leaq	.LC3(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal2V823FatalProcessOutOfMemoryEPNS0_7IsolateEPKcb@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	movq	-128(%rbp), %rbx
	movq	-112(%rbp), %rax
	movq	%r12, %rdi
	movq	%rcx, -120(%rbp)
	movdqu	-104(%rbp), %xmm0
	movq	%rax, 24(%rbx)
	movups	%xmm0, 32(%rbx)
	call	_ZN2v88internal13VirtualMemory5ResetEv@PLT
	movq	-120(%rbp), %rcx
.L25:
	movq	%r12, %rdi
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
	movq	-120(%rbp), %rcx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L39
	addq	$104, %rsp
	movq	%rcx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L38:
	.cfi_restore_state
	movq	-128(%rbp), %rbx
	movq	-80(%rbp), %rax
	movq	%rcx, -136(%rbp)
	movdqu	-72(%rbp), %xmm1
	movq	%rax, 24(%rbx)
	movups	%xmm1, 32(%rbx)
	call	_ZN2v88internal13VirtualMemory5ResetEv@PLT
	movq	-120(%rbp), %rdi
	movq	-136(%rbp), %rcx
	movabsq	$4294967296, %rax
	cmpq	%rax, 40(%rbx)
	jne	.L40
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
	movq	-120(%rbp), %rcx
	jmp	.L25
.L40:
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L39:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8615:
	.size	_ZN2v88internal16IsolateAllocator15InitReservationEv, .-_ZN2v88internal16IsolateAllocator15InitReservationEv
	.section	.rodata._ZN2v88internal16IsolateAllocator21CommitPagesForIsolateEm.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"reservation_.InVM(heap_address, kPtrComprHeapReservationSize)"
	.align 8
.LC5:
	.string	"IsAligned(isolate_root, kPtrComprIsolateRootAlignment)"
	.align 8
.LC6:
	.string	"page_allocator_instance_->AllocatePagesAt( reserved_region_address, reserved_region_size, PageAllocator::Permission::kNoAccess)"
	.align 8
.LC7:
	.string	"reservation_.SetPermissions(committed_region_address, committed_region_size, PageAllocator::kReadWrite)"
	.section	.text._ZN2v88internal16IsolateAllocator21CommitPagesForIsolateEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16IsolateAllocator21CommitPagesForIsolateEm
	.type	_ZN2v88internal16IsolateAllocator21CommitPagesForIsolateEm, @function
_ZN2v88internal16IsolateAllocator21CommitPagesForIsolateEm:
.LFB8616:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	40(%rdi), %rdx
	subq	32(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L42
	movabsq	$4294967296, %r14
	addq	%r14, %rax
	cmpq	%rdx, %rax
	ja	.L42
	movq	%rsi, %rbx
	cmpl	$-2147483648, %esi
	jne	.L54
	movq	%rdi, %r13
	call	_ZN2v88internal24GetPlatformPageAllocatorEv@PLT
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	movl	$224, %edi
	leaq	262143(%rax), %r12
	negq	%rax
	andq	%rax, %r12
	call	_Znwm@PLT
	movq	%r14, %rcx
	movq	%r12, %r8
	movq	%rbx, %rdx
	movq	%rax, %rdi
	movq	%r15, %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v84base20BoundedPageAllocatorC1EPNS_13PageAllocatorEmmm@PLT
	movq	16(%r13), %r14
	movq	-56(%rbp), %rdi
	movq	%rdi, 16(%r13)
	testq	%r14, %r14
	je	.L45
	movq	(%r14), %rax
	leaq	_ZN2v84base20BoundedPageAllocatorD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L46
	leaq	16+_ZTVN2v84base20BoundedPageAllocatorE(%rip), %rax
	leaq	72(%r14), %rdi
	movq	%rax, (%r14)
	call	_ZN2v84base15RegionAllocatorD1Ev@PLT
	leaq	8(%r14), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	%r14, %rdi
	movl	$224, %esi
	call	_ZdlPvm@PLT
	movq	16(%r13), %rdi
.L45:
	movl	$2147529423, %edx
	movq	%r12, %rax
	movq	%rdi, 8(%r13)
	xorl	%ecx, %ecx
	leaq	2147483520(%rbx), %r14
	addq	%rdx, %rbx
	negq	%rax
	movq	%r14, %rsi
	leaq	(%rbx,%r12), %rdx
	andq	%rax, %rsi
	andq	%rax, %rdx
	subq	%rsi, %rdx
	call	_ZN2v84base20BoundedPageAllocator15AllocatePagesAtEmmNS_13PageAllocator10PermissionE@PLT
	testb	%al, %al
	je	.L55
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	movq	%r14, %rsi
	leaq	24(%r13), %rdi
	movq	%rax, %rcx
	leaq	(%rbx,%rax), %rdx
	negq	%rcx
	andq	%rcx, %rsi
	andq	%rcx, %rdx
	movl	$2, %ecx
	subq	%rsi, %rdx
	call	_ZN2v88internal13VirtualMemory14SetPermissionsEmmNS_13PageAllocator10PermissionE@PLT
	testb	%al, %al
	je	.L56
	movq	%r14, 0(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L46:
	movq	%r14, %rdi
	call	*%rax
	movq	16(%r13), %rdi
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L54:
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	.LC6(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L56:
	leaq	.LC7(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8616:
	.size	_ZN2v88internal16IsolateAllocator21CommitPagesForIsolateEm, .-_ZN2v88internal16IsolateAllocator21CommitPagesForIsolateEm
	.section	.rodata._ZN2v88internal16IsolateAllocatorC2ENS0_21IsolateAllocationModeE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"mode == IsolateAllocationMode::kInCppHeap"
	.section	.text._ZN2v88internal16IsolateAllocatorC2ENS0_21IsolateAllocationModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16IsolateAllocatorC2ENS0_21IsolateAllocationModeE
	.type	_ZN2v88internal16IsolateAllocatorC2ENS0_21IsolateAllocationModeE, @function
_ZN2v88internal16IsolateAllocatorC2ENS0_21IsolateAllocationModeE:
.LFB8610:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	cmpl	$1, %esi
	je	.L61
	testl	%esi, %esi
	jne	.L62
	call	_ZN2v88internal24GetPlatformPageAllocatorEv@PLT
	movl	$45904, %edi
	movq	%rax, 8(%r12)
	call	_Znwm@PLT
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	call	_ZN2v88internal16IsolateAllocator15InitReservationEv
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal16IsolateAllocator21CommitPagesForIsolateEm
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8610:
	.size	_ZN2v88internal16IsolateAllocatorC2ENS0_21IsolateAllocationModeE, .-_ZN2v88internal16IsolateAllocatorC2ENS0_21IsolateAllocationModeE
	.globl	_ZN2v88internal16IsolateAllocatorC1ENS0_21IsolateAllocationModeE
	.set	_ZN2v88internal16IsolateAllocatorC1ENS0_21IsolateAllocationModeE,_ZN2v88internal16IsolateAllocatorC2ENS0_21IsolateAllocationModeE
	.section	.text._ZN2v84base20BoundedPageAllocatorD2Ev,"axG",@progbits,_ZN2v84base20BoundedPageAllocatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base20BoundedPageAllocatorD2Ev
	.type	_ZN2v84base20BoundedPageAllocatorD2Ev, @function
_ZN2v84base20BoundedPageAllocatorD2Ev:
.LFB9546:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v84base20BoundedPageAllocatorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	72(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -72(%rdi)
	call	_ZN2v84base15RegionAllocatorD1Ev@PLT
	addq	$8, %rsp
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5MutexD1Ev@PLT
	.cfi_endproc
.LFE9546:
	.size	_ZN2v84base20BoundedPageAllocatorD2Ev, .-_ZN2v84base20BoundedPageAllocatorD2Ev
	.weak	_ZN2v84base20BoundedPageAllocatorD1Ev
	.set	_ZN2v84base20BoundedPageAllocatorD1Ev,_ZN2v84base20BoundedPageAllocatorD2Ev
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal16IsolateAllocatorC2ENS0_21IsolateAllocationModeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal16IsolateAllocatorC2ENS0_21IsolateAllocationModeE, @function
_GLOBAL__sub_I__ZN2v88internal16IsolateAllocatorC2ENS0_21IsolateAllocationModeE:
.LFB9968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE9968:
	.size	_GLOBAL__sub_I__ZN2v88internal16IsolateAllocatorC2ENS0_21IsolateAllocationModeE, .-_GLOBAL__sub_I__ZN2v88internal16IsolateAllocatorC2ENS0_21IsolateAllocationModeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal16IsolateAllocatorC2ENS0_21IsolateAllocationModeE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
