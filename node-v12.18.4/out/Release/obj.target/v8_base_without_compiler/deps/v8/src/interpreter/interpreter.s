	.file	"interpreter.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB2414:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE2414:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4360:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4360:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4361:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4361:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB4363:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4363:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.type	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, @function
_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE:
.LFB7990:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rcx), %r8
	movq	16(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE7990:
	.size	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, .-_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.section	.text._ZN2v88internal14CompilationJobD2Ev,"axG",@progbits,_ZN2v88internal14CompilationJobD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CompilationJobD2Ev
	.type	_ZN2v88internal14CompilationJobD2Ev, @function
_ZN2v88internal14CompilationJobD2Ev:
.LFB10665:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10665:
	.size	_ZN2v88internal14CompilationJobD2Ev, .-_ZN2v88internal14CompilationJobD2Ev
	.weak	_ZN2v88internal14CompilationJobD1Ev
	.set	_ZN2v88internal14CompilationJobD1Ev,_ZN2v88internal14CompilationJobD2Ev
	.section	.text._ZN2v88internal14CompilationJobD0Ev,"axG",@progbits,_ZN2v88internal14CompilationJobD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CompilationJobD0Ev
	.type	_ZN2v88internal14CompilationJobD0Ev, @function
_ZN2v88internal14CompilationJobD0Ev:
.LFB10667:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10667:
	.size	_ZN2v88internal14CompilationJobD0Ev, .-_ZN2v88internal14CompilationJobD0Ev
	.section	.text._ZN2v88internal11interpreter11InterpreterD2Ev,"axG",@progbits,_ZN2v88internal11interpreter11InterpreterD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter11InterpreterD2Ev
	.type	_ZN2v88internal11interpreter11InterpreterD2Ev, @function
_ZN2v88internal11interpreter11InterpreterD2Ev:
.LFB26409:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal11interpreter11InterpreterE(%rip), %rax
	movq	%rax, (%rdi)
	movq	6160(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L9
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	ret
	.cfi_endproc
.LFE26409:
	.size	_ZN2v88internal11interpreter11InterpreterD2Ev, .-_ZN2v88internal11interpreter11InterpreterD2Ev
	.weak	_ZN2v88internal11interpreter11InterpreterD1Ev
	.set	_ZN2v88internal11interpreter11InterpreterD1Ev,_ZN2v88internal11interpreter11InterpreterD2Ev
	.section	.text._ZN2v88internal11interpreter11InterpreterD0Ev,"axG",@progbits,_ZN2v88internal11interpreter11InterpreterD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter11InterpreterD0Ev
	.type	_ZN2v88internal11interpreter11InterpreterD0Ev, @function
_ZN2v88internal11interpreter11InterpreterD0Ev:
.LFB26411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal11interpreter11InterpreterE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	6160(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L12
	call	_ZdaPv@PLT
.L12:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$6176, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26411:
	.size	_ZN2v88internal11interpreter11InterpreterD0Ev, .-_ZN2v88internal11interpreter11InterpreterD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB26398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE26398:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZNSt17_Function_handlerIFvN2v88internal11interpreter8BytecodeENS2_12OperandScaleEEZNS2_11Interpreter10InitializeEvEUlS3_S4_E_E9_M_invokeERKSt9_Any_dataOS3_OS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvN2v88internal11interpreter8BytecodeENS2_12OperandScaleEEZNS2_11Interpreter10InitializeEvEUlS3_S4_E_E9_M_invokeERKSt9_Any_dataOS3_OS4_, @function
_ZNSt17_Function_handlerIFvN2v88internal11interpreter8BytecodeENS2_12OperandScaleEEZNS2_11Interpreter10InitializeEvEUlS3_S4_E_E9_M_invokeERKSt9_Any_dataOS3_OS4_:
.LFB24739:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	(%rsi), %r12d
	movzbl	(%rdx), %ebx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %r14
	movl	%ebx, %esi
	movl	%r12d, %edi
	movq	8(%r14), %r13
	call	_ZN2v88internal11interpreter9Bytecodes18BytecodeHasHandlerENS1_8BytecodeENS1_12OperandScaleE@PLT
	testb	%al, %al
	jne	.L28
.L20:
	movq	%r13, -48(%rbp)
	salq	$7, %rbx
	movl	43(%r13), %edx
	leaq	63(%r13), %rax
	andl	$32512, %ebx
	movq	24(%r14), %r14
	addq	%r12, %rbx
	testl	%edx, %edx
	js	.L29
.L22:
	movq	%rax, 16(%r14,%rbx,8)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L30
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movq	(%r14), %rax
	movq	16(%r14), %rdi
	movl	(%rax), %esi
	leal	1(%rsi), %edx
	movl	%edx, (%rax)
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	%rax, %r13
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	-48(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L22
.L30:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24739:
	.size	_ZNSt17_Function_handlerIFvN2v88internal11interpreter8BytecodeENS2_12OperandScaleEEZNS2_11Interpreter10InitializeEvEUlS3_S4_E_E9_M_invokeERKSt9_Any_dataOS3_OS4_, .-_ZNSt17_Function_handlerIFvN2v88internal11interpreter8BytecodeENS2_12OperandScaleEEZNS2_11Interpreter10InitializeEvEUlS3_S4_E_E9_M_invokeERKSt9_Any_dataOS3_OS4_
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter11Interpreter10InitializeEvEUlNS3_8BytecodeENS3_12OperandScaleEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter11Interpreter10InitializeEvEUlNS3_8BytecodeENS3_12OperandScaleEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter11Interpreter10InitializeEvEUlNS3_8BytecodeENS3_12OperandScaleEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation:
.LFB24740:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L32
	cmpl	$3, %edx
	je	.L33
	cmpl	$1, %edx
	je	.L39
.L34:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$32, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%r12), %xmm1
	movq	%rax, (%rbx)
	movups	%xmm1, 16(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L34
	movl	$32, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24740:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter11Interpreter10InitializeEvEUlNS3_8BytecodeENS3_12OperandScaleEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter11Interpreter10InitializeEvEUlNS3_8BytecodeENS3_12OperandScaleEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB26595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26595:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB26399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26399:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB26596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE26596:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB11272:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L52
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L55
.L46:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L46
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE11272:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internal11interpreter25InterpreterCompilationJob14ExecuteJobImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.compile"
	.section	.rodata._ZN2v88internal11interpreter25InterpreterCompilationJob14ExecuteJobImplEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"V8.CompileIgnition"
	.section	.text._ZN2v88internal11interpreter25InterpreterCompilationJob14ExecuteJobImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25InterpreterCompilationJob14ExecuteJobImplEv
	.type	_ZN2v88internal11interpreter25InterpreterCompilationJob14ExecuteJobImplEv, @function
_ZN2v88internal11interpreter25InterpreterCompilationJob14ExecuteJobImplEv:
.LFB20983:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	32(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	8(%rcx), %eax
	movq	136(%rcx), %rdi
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	andl	$8192, %eax
	movaps	%xmm0, -96(%rbp)
	cmpl	$1, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	sbbl	%edx, %edx
	andl	$12, %edx
	addl	$121, %edx
	testq	%rdi, %rdi
	je	.L58
	testl	%eax, %eax
	jne	.L84
.L58:
	movq	_ZZN2v88internal11interpreter25InterpreterCompilationJob14ExecuteJobImplEvE28trace_event_unique_atomic194(%rip), %r12
	testq	%r12, %r12
	je	.L85
.L60:
	movq	$0, -144(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L86
.L62:
	movq	24(%rbx), %rsi
	leaq	224(%rbx), %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator16GenerateBytecodeEm@PLT
	leaq	-144(%rbp), %rdi
	movzbl	232(%rbx), %r12d
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L87
.L56:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L88
	leaq	-24(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L89
.L63:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L64
	movq	(%rdi), %rax
	call	*8(%rax)
.L64:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L65
	movq	(%rdi), %rax
	call	*8(%rax)
.L65:
	leaq	.LC1(%rip), %rax
	movq	%r12, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L85:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L90
.L61:
	movq	%r12, _ZZN2v88internal11interpreter25InterpreterCompilationJob14ExecuteJobImplEvE28trace_event_unique_atomic194(%rip)
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L87:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L90:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L89:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC1(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L63
.L84:
	leaq	-104(%rbp), %rsi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L58
.L88:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20983:
	.size	_ZN2v88internal11interpreter25InterpreterCompilationJob14ExecuteJobImplEv, .-_ZN2v88internal11interpreter25InterpreterCompilationJob14ExecuteJobImplEv
	.section	.rodata._ZN2v88internal11interpreter25InterpreterCompilationJob15FinalizeJobImplENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"V8.CompileIgnitionFinalization"
	.align 8
.LC3:
	.string	"[generated bytecode for function: "
	.section	.rodata._ZN2v88internal11interpreter25InterpreterCompilationJob15FinalizeJobImplENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	" ("
.LC5:
	.string	")]"
	.section	.text._ZN2v88internal11interpreter25InterpreterCompilationJob15FinalizeJobImplENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25InterpreterCompilationJob15FinalizeJobImplENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE
	.type	_ZN2v88internal11interpreter25InterpreterCompilationJob15FinalizeJobImplENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE, @function
_ZN2v88internal11interpreter25InterpreterCompilationJob15FinalizeJobImplENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE:
.LFB20984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$472, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	movaps	%xmm0, -448(%rbp)
	movq	$0, -416(%rbp)
	movq	136(%rax), %rdi
	movaps	%xmm0, -432(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testq	%rdi, %rdi
	je	.L92
	testl	%eax, %eax
	jne	.L151
.L92:
	movq	_ZZN2v88internal11interpreter25InterpreterCompilationJob15FinalizeJobImplENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateEE28trace_event_unique_atomic232(%rip), %rbx
	testq	%rbx, %rbx
	je	.L152
.L94:
	movq	$0, -480(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L153
	movq	168(%r15), %rbx
	testq	%rbx, %rbx
	je	.L154
.L100:
	movq	40(%r15), %rdi
	call	_ZNK2v88internal26UnoptimizedCompilationInfo27SourcePositionRecordingModeEv@PLT
	cmpl	$2, %eax
	je	.L155
.L102:
	cmpb	$0, _ZN2v88internal19FLAG_print_bytecodeE(%rip)
	jne	.L106
.L150:
	xorl	%r12d, %r12d
.L101:
	leaq	-480(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L156
.L91:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L157
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	movq	(%r12), %rax
	movl	47(%rax), %edx
	andl	$268435456, %edx
	je	.L107
	movq	_ZN2v88internal26FLAG_print_bytecode_filterE(%rip), %r13
	movq	%r13, %rdi
	call	strlen@PLT
	testl	%eax, %eax
	jne	.L108
.L149:
	leaq	-488(%rbp), %r14
.L112:
	leaq	-320(%rbp), %rax
	leaq	-400(%rbp), %r13
	movq	%rax, %rdi
	movq	%rax, -504(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	stdout(%rip), %rdx
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%rax, -320(%rbp)
	movq	%r13, %rdi
	xorl	%eax, %eax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	leaq	-496(%rbp), %rdi
	movq	%rax, -400(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	movq	40(%r15), %rax
	movq	16(%rax), %rsi
	call	_ZNK2v88internal15FunctionLiteral12GetDebugNameEv@PLT
	leaq	.LC3(%rip), %rsi
	movl	$34, %edx
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-496(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L158
	movq	%rsi, %rdi
	movq	%rsi, -512(%rbp)
	call	strlen@PLT
	movq	-512(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L113:
	movl	$2, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -488(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movl	$2, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %rdi
	testq	%rdi, %rdi
	je	.L159
	cmpb	$0, 56(%rdi)
	je	.L115
	movsbl	67(%rdi), %esi
.L116:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	(%rbx), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -488(%rbp)
	call	_ZN2v88internal13BytecodeArray11DisassembleERSo@PLT
	movq	%r13, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-496(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L117
	call	_ZdaPv@PLT
.L117:
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	-504(%rbp), %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L153:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -400(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L160
.L97:
	movq	-392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L98
	movq	(%rdi), %rax
	call	*8(%rax)
.L98:
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L99
	movq	(%rdi), %rax
	call	*8(%rax)
.L99:
	leaq	.LC2(%rip), %rax
	movq	%rbx, -472(%rbp)
	movq	168(%r15), %rbx
	movq	%rax, -464(%rbp)
	leaq	-472(%rbp), %rax
	movq	%r14, -456(%rbp)
	movq	%rax, -480(%rbp)
	testq	%rbx, %rbx
	jne	.L100
.L154:
	movq	32(%r15), %rax
	leaq	224(%r15), %rdi
	movq	%r13, %rsi
	movq	80(%rax), %rdx
	call	_ZN2v88internal11interpreter17BytecodeGenerator16FinalizeBytecodeEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE@PLT
	cmpb	$0, 232(%r15)
	jne	.L123
	movq	%rax, %rbx
	movq	40(%r15), %rax
	movq	%rbx, 40(%rax)
	movq	40(%r15), %rdi
	call	_ZNK2v88internal26UnoptimizedCompilationInfo27SourcePositionRecordingModeEv@PLT
	cmpl	$2, %eax
	jne	.L102
.L155:
	movq	%r13, %rsi
	leaq	224(%r15), %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator27FinalizeSourcePositionTableEPNS0_7IsolateE@PLT
	movq	(%rbx), %r14
	movq	(%rax), %r13
	leaq	31(%r14), %rsi
	movq	%r13, 31(%r14)
	testb	$1, %r13b
	je	.L102
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -504(%rbp)
	testl	$262144, %eax
	je	.L104
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -512(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-504(%rbp), %rcx
	movq	-512(%rbp), %rsi
	movq	8(%rcx), %rax
.L104:
	testb	$24, %al
	je	.L102
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L102
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L152:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L161
.L95:
	movq	%rbx, _ZZN2v88internal11interpreter25InterpreterCompilationJob15FinalizeJobImplENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateEE28trace_event_unique_atomic232(%rip)
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L108:
	cmpl	$1, %eax
	jne	.L150
	cmpb	$42, 0(%r13)
	jne	.L150
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L123:
	movl	$1, %r12d
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L107:
	leaq	-488(%rbp), %r14
	movq	_ZN2v88internal26FLAG_print_bytecode_filterE(%rip), %rsi
	movq	%rax, -488(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal18SharedFunctionInfo12PassesFilterEPKc@PLT
	testb	%al, %al
	jne	.L112
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L115:
	movq	%rdi, -512(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-512(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L116
	call	*%rax
	movsbl	%al, %esi
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L156:
	leaq	-440(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L158:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L161:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L160:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$88, %esi
	leaq	-400(%rbp), %rdx
	pushq	$0
	leaq	.LC2(%rip), %rcx
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L97
.L151:
	leaq	-440(%rbp), %rsi
	movl	$134, %edx
	movq	%rdi, -448(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L92
.L157:
	call	__stack_chk_fail@PLT
.L159:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE20984:
	.size	_ZN2v88internal11interpreter25InterpreterCompilationJob15FinalizeJobImplENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE, .-_ZN2v88internal11interpreter25InterpreterCompilationJob15FinalizeJobImplENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE
	.section	.text._ZN2v88internal11interpreter11InterpreterC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter11InterpreterC2EPNS0_7IsolateE
	.type	_ZN2v88internal11interpreter11InterpreterC2EPNS0_7IsolateE, @function
_ZN2v88internal11interpreter11InterpreterC2EPNS0_7IsolateE:
.LFB20903:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal11interpreter11InterpreterE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	addq	$24, %rdi
	subq	$8, %rsp
	movq	%rax, -24(%rdi)
	movl	%ebx, %eax
	movq	%rsi, -16(%rdi)
	movq	$0, 6136(%rdi)
	movq	$0, 6144(%rdi)
	movq	$0, -8(%rdi)
	movq	$0, 6128(%rdi)
	andq	$-8, %rdi
	subl	%edi, %eax
	leal	6160(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	cmpb	$0, _ZN2v88internal30FLAG_trace_ignition_dispatchesE(%rip)
	jne	.L169
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	movl	$267912, %edi
	call	_Znam@PLT
	movq	6160(%rbx), %r8
	movq	%rax, %rdi
	movq	%rax, 6160(%rbx)
	testq	%r8, %r8
	je	.L164
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	movq	6160(%rbx), %rdi
.L164:
	addq	$8, %rsp
	movl	$267912, %edx
	xorl	%esi, %esi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	memset@PLT
	.cfi_endproc
.LFE20903:
	.size	_ZN2v88internal11interpreter11InterpreterC2EPNS0_7IsolateE, .-_ZN2v88internal11interpreter11InterpreterC2EPNS0_7IsolateE
	.globl	_ZN2v88internal11interpreter11InterpreterC1EPNS0_7IsolateE
	.set	_ZN2v88internal11interpreter11InterpreterC1EPNS0_7IsolateE,_ZN2v88internal11interpreter11InterpreterC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal11interpreter11Interpreter18GetBytecodeHandlerENS1_8BytecodeENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter11Interpreter18GetBytecodeHandlerENS1_8BytecodeENS1_12OperandScaleE
	.type	_ZN2v88internal11interpreter11Interpreter18GetBytecodeHandlerENS1_8BytecodeENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter11Interpreter18GetBytecodeHandlerENS1_8BytecodeENS1_12OperandScaleE:
.LFB20906:
	.cfi_startproc
	endbr64
	movzbl	%dl, %edx
	movzbl	%sil, %esi
	leaq	_ZN2v88internalL26kBytecodeToBuiltinsMappingE(%rip), %rax
	movq	8(%rdi), %rdi
	sarl	%edx
	imull	$183, %edx, %edx
	addl	%esi, %edx
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %eax
	testl	%eax, %eax
	leal	1070(%rax), %esi
	movl	$1252, %eax
	cmovs	%eax, %esi
	addq	$41184, %rdi
	jmp	_ZN2v88internal8Builtins7builtinEi@PLT
	.cfi_endproc
.LFE20906:
	.size	_ZN2v88internal11interpreter11Interpreter18GetBytecodeHandlerENS1_8BytecodeENS1_12OperandScaleE, .-_ZN2v88internal11interpreter11Interpreter18GetBytecodeHandlerENS1_8BytecodeENS1_12OperandScaleE
	.section	.text._ZN2v88internal11interpreter11Interpreter18SetBytecodeHandlerENS1_8BytecodeENS1_12OperandScaleENS0_4CodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter11Interpreter18SetBytecodeHandlerENS1_8BytecodeENS1_12OperandScaleENS0_4CodeE
	.type	_ZN2v88internal11interpreter11Interpreter18SetBytecodeHandlerENS1_8BytecodeENS1_12OperandScaleENS0_4CodeE, @function
_ZN2v88internal11interpreter11Interpreter18SetBytecodeHandlerENS1_8BytecodeENS1_12OperandScaleENS0_4CodeE:
.LFB20907:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	salq	$7, %rdx
	movzbl	%sil, %esi
	andl	$32512, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	(%rdx,%rsi), %rbx
	subq	$16, %rsp
	movq	%rcx, -24(%rbp)
	movl	43(%rcx), %eax
	testl	%eax, %eax
	js	.L178
	leaq	63(%rcx), %rax
	movq	%rax, 16(%r12,%rbx,8)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	leaq	-24(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, 16(%r12,%rbx,8)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20907:
	.size	_ZN2v88internal11interpreter11Interpreter18SetBytecodeHandlerENS1_8BytecodeENS1_12OperandScaleENS0_4CodeE, .-_ZN2v88internal11interpreter11Interpreter18SetBytecodeHandlerENS1_8BytecodeENS1_12OperandScaleENS0_4CodeE
	.section	.text._ZN2v88internal11interpreter11Interpreter21GetDispatchTableIndexENS1_8BytecodeENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter11Interpreter21GetDispatchTableIndexENS1_8BytecodeENS1_12OperandScaleE
	.type	_ZN2v88internal11interpreter11Interpreter21GetDispatchTableIndexENS1_8BytecodeENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter11Interpreter21GetDispatchTableIndexENS1_8BytecodeENS1_12OperandScaleE:
.LFB20908:
	.cfi_startproc
	endbr64
	salq	$7, %rsi
	movzbl	%dil, %eax
	andl	$32512, %esi
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE20908:
	.size	_ZN2v88internal11interpreter11Interpreter21GetDispatchTableIndexENS1_8BytecodeENS1_12OperandScaleE, .-_ZN2v88internal11interpreter11Interpreter21GetDispatchTableIndexENS1_8BytecodeENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter11Interpreter20IterateDispatchTableEPNS0_11RootVisitorE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"address < start || address >= end"
	.section	.rodata._ZN2v88internal11interpreter11Interpreter20IterateDispatchTableEPNS0_11RootVisitorE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal11interpreter11Interpreter20IterateDispatchTableEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter11Interpreter20IterateDispatchTableEPNS0_11RootVisitorE
	.type	_ZN2v88internal11interpreter11Interpreter20IterateDispatchTableEPNS0_11RootVisitorE, @function
_ZN2v88internal11interpreter11Interpreter20IterateDispatchTableEPNS0_11RootVisitorE:
.LFB20909:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 41456(%rdi)
	je	.L203
.L181:
	leaq	16(%r13), %rbx
	leaq	6160(%r13), %r14
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L185:
	movq	(%r12), %rax
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %rdx
	leaq	-64(%rbp), %rcx
	movq	24(%rax), %r8
	cmpq	%rdx, %r8
	jne	.L187
	leaq	-56(%rbp), %r8
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r12, %rdi
	call	*16(%rax)
.L188:
	movq	-64(%rbp), %rax
	cmpq	%r15, %rax
	je	.L189
	addq	$63, %rax
	movq	%rax, (%rbx)
.L189:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L180
	movq	8(%r13), %rdi
.L190:
	movq	(%rbx), %r15
	movq	%r15, %rsi
	call	_ZN2v88internal17InstructionStream11PcIsOffHeapEPNS0_7IsolateEm@PLT
	testb	%al, %al
	jne	.L189
	movq	$0, -64(%rbp)
	testq	%r15, %r15
	je	.L185
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movq	-72(%rbp), %rdx
	movl	%eax, %eax
	addq	%rdx, %rax
	cmpq	%rax, %r15
	jnb	.L186
	cmpq	%r15, %rdx
	jbe	.L204
.L186:
	subq	$63, %r15
	movq	%r15, -64(%rbp)
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L187:
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r12, %rdi
	call	*%r8
	jmp	.L188
.L204:
	leaq	.LC6(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L203:
	call	_ZNK2v88internal7Isolate13embedded_blobEv@PLT
	testq	%rax, %rax
	je	.L205
.L180:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L206
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L205:
	.cfi_restore_state
	movq	8(%r13), %rdi
	jmp	.L181
.L206:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20909:
	.size	_ZN2v88internal11interpreter11Interpreter20IterateDispatchTableEPNS0_11RootVisitorE, .-_ZN2v88internal11interpreter11Interpreter20IterateDispatchTableEPNS0_11RootVisitorE
	.section	.text._ZN2v88internal11interpreter11Interpreter15InterruptBudgetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter11Interpreter15InterruptBudgetEv
	.type	_ZN2v88internal11interpreter11Interpreter15InterruptBudgetEv, @function
_ZN2v88internal11interpreter11Interpreter15InterruptBudgetEv:
.LFB20910:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal21FLAG_interrupt_budgetE(%rip), %eax
	ret
	.cfi_endproc
.LFE20910:
	.size	_ZN2v88internal11interpreter11Interpreter15InterruptBudgetEv, .-_ZN2v88internal11interpreter11Interpreter15InterruptBudgetEv
	.section	.rodata._ZN2v88internal11interpreter25InterpreterCompilationJobC2EPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorEPSt6vectorIS6_SaIS6_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"../deps/v8/src/interpreter/interpreter.cc:182"
	.section	.text._ZN2v88internal11interpreter25InterpreterCompilationJobC2EPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorEPSt6vectorIS6_SaIS6_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25InterpreterCompilationJobC2EPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorEPSt6vectorIS6_SaIS6_EE
	.type	_ZN2v88internal11interpreter25InterpreterCompilationJobC2EPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorEPSt6vectorIS6_SaIS6_EE, @function
_ZN2v88internal11interpreter25InterpreterCompilationJobC2EPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorEPSt6vectorIS6_SaIS6_EE:
.LFB20981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CompilationJobE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	128(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	32(%rsi), %rdx
	movq	%rax, (%rdi)
	movl	$1, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	-56(%rbp), %rdx
	movq	%r13, %xmm1
	movq	%r12, %xmm0
	movq	%rax, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	leaq	64(%rbx), %r8
	movq	-64(%rbp), %rsi
	leaq	16+_ZTVN2v88internal11interpreter25InterpreterCompilationJobE(%rip), %rax
	movq	%rdx, 24(%rbx)
	movq	%r8, %rdi
	leaq	.LC8(%rip), %rdx
	movq	%rax, (%rbx)
	movups	%xmm0, 32(%rbx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	-56(%rbp), %r8
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal26UnoptimizedCompilationInfoC1EPNS0_4ZoneEPNS0_9ParseInfoEPNS0_15FunctionLiteralE@PLT
	movq	120(%r12), %rdx
	movq	%r14, %rcx
	movq	%r13, %rsi
	addq	$24, %rsp
	leaq	224(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGeneratorC1EPNS0_26UnoptimizedCompilationInfoEPKNS0_18AstStringConstantsEPSt6vectorIPNS0_15FunctionLiteralESaISA_EE@PLT
	.cfi_endproc
.LFE20981:
	.size	_ZN2v88internal11interpreter25InterpreterCompilationJobC2EPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorEPSt6vectorIS6_SaIS6_EE, .-_ZN2v88internal11interpreter25InterpreterCompilationJobC2EPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorEPSt6vectorIS6_SaIS6_EE
	.globl	_ZN2v88internal11interpreter25InterpreterCompilationJobC1EPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorEPSt6vectorIS6_SaIS6_EE
	.set	_ZN2v88internal11interpreter25InterpreterCompilationJobC1EPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorEPSt6vectorIS6_SaIS6_EE,_ZN2v88internal11interpreter25InterpreterCompilationJobC2EPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorEPSt6vectorIS6_SaIS6_EE
	.section	.text._ZN2v88internal11interpreter11Interpreter17NewCompilationJobEPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorEPSt6vectorIS6_SaIS6_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter11Interpreter17NewCompilationJobEPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorEPSt6vectorIS6_SaIS6_EE
	.type	_ZN2v88internal11interpreter11Interpreter17NewCompilationJobEPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorEPSt6vectorIS6_SaIS6_EE, @function
_ZN2v88internal11interpreter11Interpreter17NewCompilationJobEPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorEPSt6vectorIS6_SaIS6_EE:
.LFB20985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$1104, %edi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_Znwm@PLT
	movq	32(%r12), %rdx
	movq	%rax, %rbx
	leaq	128(%rax), %r14
	leaq	16+_ZTVN2v88internal14CompilationJobE(%rip), %rax
	movq	%rdx, -56(%rbp)
	movq	%rax, (%rbx)
	movl	$1, 8(%rbx)
	movq	$0, 16(%rbx)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	-56(%rbp), %rdx
	movq	%r14, %xmm1
	movq	%r12, %xmm0
	punpcklqdq	%xmm1, %xmm0
	leaq	64(%rbx), %r8
	movq	-64(%rbp), %rsi
	movq	%rax, 16(%rbx)
	movq	%rdx, 24(%rbx)
	leaq	16+_ZTVN2v88internal11interpreter25InterpreterCompilationJobE(%rip), %rax
	movq	%r8, %rdi
	leaq	.LC8(%rip), %rdx
	movups	%xmm0, 32(%rbx)
	pxor	%xmm0, %xmm0
	movq	%rax, (%rbx)
	movups	%xmm0, 48(%rbx)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	-72(%rbp), %r9
	movq	-56(%rbp), %r8
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r9, %rcx
	movq	%r8, %rsi
	call	_ZN2v88internal26UnoptimizedCompilationInfoC1EPNS0_4ZoneEPNS0_9ParseInfoEPNS0_15FunctionLiteralE@PLT
	movq	120(%r12), %rdx
	movq	%r15, %rcx
	movq	%r14, %rsi
	leaq	224(%rbx), %rdi
	call	_ZN2v88internal11interpreter17BytecodeGeneratorC1EPNS0_26UnoptimizedCompilationInfoEPKNS0_18AstStringConstantsEPSt6vectorIPNS0_15FunctionLiteralESaISA_EE@PLT
	movq	%rbx, 0(%r13)
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20985:
	.size	_ZN2v88internal11interpreter11Interpreter17NewCompilationJobEPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorEPSt6vectorIS6_SaIS6_EE, .-_ZN2v88internal11interpreter11Interpreter17NewCompilationJobEPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorEPSt6vectorIS6_SaIS6_EE
	.section	.text._ZN2v88internal11interpreter11Interpreter30NewSourcePositionCollectionJobEPNS0_9ParseInfoEPNS0_15FunctionLiteralENS0_6HandleINS0_13BytecodeArrayEEEPNS0_19AccountingAllocatorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter11Interpreter30NewSourcePositionCollectionJobEPNS0_9ParseInfoEPNS0_15FunctionLiteralENS0_6HandleINS0_13BytecodeArrayEEEPNS0_19AccountingAllocatorE
	.type	_ZN2v88internal11interpreter11Interpreter30NewSourcePositionCollectionJobEPNS0_9ParseInfoEPNS0_15FunctionLiteralENS0_6HandleINS0_13BytecodeArrayEEEPNS0_19AccountingAllocatorE, @function
_ZN2v88internal11interpreter11Interpreter30NewSourcePositionCollectionJobEPNS0_9ParseInfoEPNS0_15FunctionLiteralENS0_6HandleINS0_13BytecodeArrayEEEPNS0_19AccountingAllocatorE:
.LFB20988:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$1104, %edi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%r8, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_Znwm@PLT
	movq	32(%r12), %rdx
	movq	%rax, %rbx
	leaq	128(%rax), %r15
	leaq	16+_ZTVN2v88internal14CompilationJobE(%rip), %rax
	movq	%rdx, -56(%rbp)
	movq	%rax, (%rbx)
	movl	$1, 8(%rbx)
	movq	$0, 16(%rbx)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	-56(%rbp), %rdx
	movq	%r15, %xmm1
	movq	%r12, %xmm0
	punpcklqdq	%xmm1, %xmm0
	leaq	64(%rbx), %r8
	movq	-64(%rbp), %rsi
	movq	%rax, 16(%rbx)
	movq	%rdx, 24(%rbx)
	leaq	16+_ZTVN2v88internal11interpreter25InterpreterCompilationJobE(%rip), %rax
	movq	%r8, %rdi
	leaq	.LC8(%rip), %rdx
	movups	%xmm0, 32(%rbx)
	pxor	%xmm0, %xmm0
	movq	%rax, (%rbx)
	movups	%xmm0, 48(%rbx)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	-72(%rbp), %r9
	movq	-56(%rbp), %r8
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%r9, %rcx
	movq	%r8, %rsi
	call	_ZN2v88internal26UnoptimizedCompilationInfoC1EPNS0_4ZoneEPNS0_9ParseInfoEPNS0_15FunctionLiteralE@PLT
	movq	120(%r12), %rdx
	movq	%r15, %rsi
	xorl	%ecx, %ecx
	leaq	224(%rbx), %rdi
	call	_ZN2v88internal11interpreter17BytecodeGeneratorC1EPNS0_26UnoptimizedCompilationInfoEPKNS0_18AstStringConstantsEPSt6vectorIPNS0_15FunctionLiteralESaISA_EE@PLT
	movq	40(%rbx), %rax
	movq	%r14, 40(%rax)
	movq	%r13, %rax
	movq	%rbx, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20988:
	.size	_ZN2v88internal11interpreter11Interpreter30NewSourcePositionCollectionJobEPNS0_9ParseInfoEPNS0_15FunctionLiteralENS0_6HandleINS0_13BytecodeArrayEEEPNS0_19AccountingAllocatorE, .-_ZN2v88internal11interpreter11Interpreter30NewSourcePositionCollectionJobEPNS0_9ParseInfoEPNS0_15FunctionLiteralENS0_6HandleINS0_13BytecodeArrayEEEPNS0_19AccountingAllocatorE
	.section	.text._ZN2v88internal11interpreter11Interpreter15ForEachBytecodeERKSt8functionIFvNS1_8BytecodeENS1_12OperandScaleEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter11Interpreter15ForEachBytecodeERKSt8functionIFvNS1_8BytecodeENS1_12OperandScaleEEE
	.type	_ZN2v88internal11interpreter11Interpreter15ForEachBytecodeERKSt8functionIFvNS1_8BytecodeENS1_12OperandScaleEEE, @function
_ZN2v88internal11interpreter11Interpreter15ForEachBytecodeERKSt8functionIFvNS1_8BytecodeENS1_12OperandScaleEEE:
.LFB20989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-59(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-60(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$513, %eax
	movb	$4, -57(%rbp)
	movw	%ax, -59(%rbp)
	leaq	-56(%rbp), %rax
	movq	%rax, -72(%rbp)
.L217:
	movzbl	(%r14), %r12d
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L216:
	cmpq	$0, 16(%r15)
	movb	%bl, -61(%rbp)
	movb	%r12b, -60(%rbp)
	je	.L222
	addl	$1, %ebx
	leaq	-61(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	*24(%r15)
	cmpl	$183, %ebx
	jne	.L216
	addq	$1, %r14
	cmpq	%r14, -72(%rbp)
	jne	.L217
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L223
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L222:
	.cfi_restore_state
	call	_ZSt25__throw_bad_function_callv@PLT
.L223:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20989:
	.size	_ZN2v88internal11interpreter11Interpreter15ForEachBytecodeERKSt8functionIFvNS1_8BytecodeENS1_12OperandScaleEEE, .-_ZN2v88internal11interpreter11Interpreter15ForEachBytecodeERKSt8functionIFvNS1_8BytecodeENS1_12OperandScaleEEE
	.section	.text._ZN2v88internal11interpreter11Interpreter10InitializeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter11Interpreter10InitializeEv
	.type	_ZN2v88internal11interpreter11Interpreter10InitializeEv, @function
_ZN2v88internal11interpreter11Interpreter10InitializeEv:
.LFB20990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$57, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-112(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	leaq	41184(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	(%rax), %rdx
	movq	%rdx, -112(%rbp)
	movl	43(%rdx), %ecx
	leaq	63(%rdx), %rax
	testl	%ecx, %ecx
	js	.L239
.L226:
	leaq	_ZNSt17_Function_handlerIFvN2v88internal11interpreter8BytecodeENS2_12OperandScaleEEZNS2_11Interpreter10InitializeEvEUlS3_S4_E_E9_M_invokeERKSt9_Any_dataOS3_OS4_(%rip), %rdx
	movq	%rax, 6168(%r14)
	movq	%r14, %xmm2
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter11Interpreter10InitializeEvEUlNS3_8BytecodeENS3_12OperandScaleEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rbx
	movq	%rdx, %xmm3
	movq	%r12, %xmm1
	movq	%rbx, %xmm0
	movq	%r12, %rdi
	punpcklqdq	%xmm2, %xmm1
	punpcklqdq	%xmm3, %xmm0
	movl	$1252, %esi
	movaps	%xmm1, -160(%rbp)
	leaq	-114(%rbp), %r14
	movaps	%xmm0, -144(%rbp)
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movl	$32, %edi
	movl	$1070, -112(%rbp)
	movq	%rax, %r12
	movq	$0, -80(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm1
	movb	$4, -97(%rbp)
	movdqa	-144(%rbp), %xmm0
	movq	%r13, (%rax)
	leaq	-96(%rbp), %r13
	movq	%r12, 8(%rax)
	leaq	-99(%rbp), %r12
	movq	%rax, -96(%rbp)
	movups	%xmm1, 16(%rax)
	movl	$513, %eax
	movw	%ax, -99(%rbp)
	movq	%rbx, %rax
	movaps	%xmm0, -80(%rbp)
.L230:
	movzbl	(%r12), %r15d
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L229:
	movb	%bl, -113(%rbp)
	movb	%r15b, -114(%rbp)
	testq	%rax, %rax
	je	.L240
	addl	$1, %ebx
	leaq	-113(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	*-72(%rbp)
	movq	-80(%rbp), %rax
	cmpl	$183, %ebx
	jne	.L229
	addq	$1, %r12
	cmpq	%r13, %r12
	jne	.L230
	testq	%rax, %rax
	je	.L224
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L224:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L241
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L240:
	call	_ZSt25__throw_bad_function_callv@PLT
.L241:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20990:
	.size	_ZN2v88internal11interpreter11Interpreter10InitializeEv, .-_ZN2v88internal11interpreter11Interpreter10InitializeEv
	.section	.text._ZNK2v88internal11interpreter11Interpreter26IsDispatchTableInitializedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter11Interpreter26IsDispatchTableInitializedEv
	.type	_ZNK2v88internal11interpreter11Interpreter26IsDispatchTableInitializedEv, @function
_ZNK2v88internal11interpreter11Interpreter26IsDispatchTableInitializedEv:
.LFB20995:
	.cfi_startproc
	endbr64
	cmpq	$0, 16(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE20995:
	.size	_ZNK2v88internal11interpreter11Interpreter26IsDispatchTableInitializedEv, .-_ZNK2v88internal11interpreter11Interpreter26IsDispatchTableInitializedEv
	.section	.text._ZN2v88internal11interpreter11Interpreter27LookupNameOfBytecodeHandlerENS0_4CodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter11Interpreter27LookupNameOfBytecodeHandlerENS0_4CodeE
	.type	_ZN2v88internal11interpreter11Interpreter27LookupNameOfBytecodeHandlerENS0_4CodeE, @function
_ZN2v88internal11interpreter11Interpreter27LookupNameOfBytecodeHandlerENS0_4CodeE:
.LFB20996:
	.cfi_startproc
	endbr64
	movl	43(%rsi), %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$1, %eax
	je	.L245
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	movl	59(%rsi), %edi
	jmp	_ZN2v88internal8Builtins4nameEi@PLT
	.cfi_endproc
.LFE20996:
	.size	_ZN2v88internal11interpreter11Interpreter27LookupNameOfBytecodeHandlerENS0_4CodeE, .-_ZN2v88internal11interpreter11Interpreter27LookupNameOfBytecodeHandlerENS0_4CodeE
	.section	.text._ZNK2v88internal11interpreter11Interpreter18GetDispatchCounterENS1_8BytecodeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter11Interpreter18GetDispatchCounterENS1_8BytecodeES3_
	.type	_ZNK2v88internal11interpreter11Interpreter18GetDispatchCounterENS1_8BytecodeES3_, @function
_ZNK2v88internal11interpreter11Interpreter18GetDispatchCounterENS1_8BytecodeES3_:
.LFB20997:
	.cfi_startproc
	endbr64
	movzbl	%sil, %esi
	movzbl	%dl, %edx
	movq	6160(%rdi), %rax
	imull	$183, %esi, %esi
	addl	%edx, %esi
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rax
	ret
	.cfi_endproc
.LFE20997:
	.size	_ZNK2v88internal11interpreter11Interpreter18GetDispatchCounterENS1_8BytecodeES3_, .-_ZNK2v88internal11interpreter11Interpreter18GetDispatchCounterENS1_8BytecodeES3_
	.section	.rodata._ZN2v88internal11interpreter11Interpreter25GetDispatchCountersObjectEv.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"basic_string::_M_construct null not valid"
	.align 8
.LC10:
	.string	"counters_row ->DefineOwnProperty(context, to_name_object, counter_object) .IsJust()"
	.align 8
.LC11:
	.string	"counters_map->DefineOwnProperty(context, from_name_object, counters_row) .IsJust()"
	.section	.text._ZN2v88internal11interpreter11Interpreter25GetDispatchCountersObjectEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter11Interpreter25GetDispatchCountersObjectEv
	.type	_ZN2v88internal11interpreter11Interpreter25GetDispatchCountersObjectEv, @function
_ZN2v88internal11interpreter11Interpreter25GetDispatchCountersObjectEv:
.LFB20998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	$0, -200(%rbp)
	movq	%rax, -216(%rbp)
	leaq	-128(%rbp), %rax
	movl	$183, -204(%rbp)
	movq	%rax, -192(%rbp)
.L270:
	movq	%r15, %rdi
	xorl	%ebx, %ebx
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	-200(%rbp), %r12
	movq	%rax, -168(%rbp)
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L248:
	addl	$1, %ebx
	addq	$8, %r12
	cmpl	$183, %ebx
	je	.L288
.L259:
	movq	6160(%r14), %rax
	movq	(%rax,%r12), %r13
	testq	%r13, %r13
	je	.L248
	movl	%ebx, %edi
	call	_ZN2v88internal11interpreter9Bytecodes8ToStringENS1_8BytecodeE@PLT
	movq	%rax, %r10
	leaq	-112(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	%rax, -128(%rbp)
	testq	%r10, %r10
	je	.L260
	movq	%r10, %rdi
	movq	%r10, -160(%rbp)
	call	strlen@PLT
	movq	-160(%rbp), %r10
	cmpq	$15, %rax
	movq	%rax, -136(%rbp)
	movq	%rax, %r9
	ja	.L289
	cmpq	$1, %rax
	jne	.L252
	movzbl	(%r10), %edx
	movb	%dl, -112(%rbp)
	movq	-152(%rbp), %rdx
.L253:
	movq	%rax, -120(%rbp)
	movl	$-1, %ecx
	movq	%r15, %rdi
	movb	$0, (%rdx,%rax)
	movq	-128(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L290
.L254:
	testq	%r13, %r13
	js	.L255
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r13, %xmm0
.L256:
	movq	%r15, %rdi
	movq	%rdx, -160(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-160(%rbp), %rdx
	movq	-176(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	-168(%rbp), %rdi
	movq	%rax, %rcx
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L291
	movq	-128(%rbp), %rdi
	cmpq	-152(%rbp), %rdi
	je	.L248
	call	_ZdlPv@PLT
	addl	$1, %ebx
	addq	$8, %r12
	cmpl	$183, %ebx
	jne	.L259
.L288:
	movl	$-73, %edi
	subb	-204(%rbp), %dil
	leaq	-80(%rbp), %rbx
	call	_ZN2v88internal11interpreter9Bytecodes8ToStringENS1_8BytecodeE@PLT
	movq	%rbx, -96(%rbp)
	movq	%rax, %r12
	leaq	-96(%rbp), %rax
	movq	%rax, -152(%rbp)
	testq	%r12, %r12
	je	.L260
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%rax, -136(%rbp)
	movq	%rax, %r13
	cmpq	$15, %rax
	ja	.L292
	cmpq	$1, %rax
	jne	.L263
	movzbl	(%r12), %edx
	movb	%dl, -80(%rbp)
	movq	%rbx, %rdx
.L264:
	movq	%rax, -88(%rbp)
	movl	$-1, %ecx
	movq	%r15, %rdi
	movb	$0, (%rdx,%rax)
	movq	-96(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L293
.L265:
	movq	-168(%rbp), %rcx
	movq	-176(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	-216(%rbp), %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L294
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L267
	call	_ZdlPv@PLT
	addq	$1464, -200(%rbp)
	subl	$1, -204(%rbp)
	jne	.L270
.L268:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L295
	movq	-216(%rbp), %rax
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	movq	%r13, %rax
	movq	%r13, %r8
	pxor	%xmm0, %xmm0
	shrq	%rax
	andl	$1, %r8d
	orq	%r8, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L252:
	testq	%rax, %rax
	jne	.L296
	movq	-152(%rbp), %rdx
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L289:
	movq	-192(%rbp), %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -184(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-160(%rbp), %r10
	movq	-184(%rbp), %r9
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
.L251:
	movq	%r9, %rdx
	movq	%r10, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L260:
	leaq	.LC9(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L290:
	movq	%rax, -160(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-160(%rbp), %rdx
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L291:
	leaq	.LC10(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L292:
	movq	-152(%rbp), %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -80(%rbp)
.L262:
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L264
.L267:
	addq	$1464, -200(%rbp)
	subl	$1, -204(%rbp)
	jne	.L270
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L263:
	testq	%rax, %rax
	jne	.L297
	movq	%rbx, %rdx
	jmp	.L264
.L294:
	leaq	.LC11(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L293:
	movq	%rax, -152(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-152(%rbp), %rdx
	jmp	.L265
.L295:
	call	__stack_chk_fail@PLT
.L296:
	movq	-152(%rbp), %rdi
	jmp	.L251
.L297:
	movq	%rbx, %rdi
	jmp	.L262
	.cfi_endproc
.LFE20998:
	.size	_ZN2v88internal11interpreter11Interpreter25GetDispatchCountersObjectEv, .-_ZN2v88internal11interpreter11Interpreter25GetDispatchCountersObjectEv
	.section	.text._ZNSt8_Rb_treeIN2v88internal3SmiESt4pairIKS2_jESt10_Select1stIS5_ESt4lessIS2_ENS1_13ZoneAllocatorIS5_EEE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal3SmiESt4pairIKS2_jESt10_Select1stIS5_ESt4lessIS2_ENS1_13ZoneAllocatorIS5_EEE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal3SmiESt4pairIKS2_jESt10_Select1stIS5_ESt4lessIS2_ENS1_13ZoneAllocatorIS5_EEE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeIN2v88internal3SmiESt4pairIKS2_jESt10_Select1stIS5_ESt4lessIS2_ENS1_13ZoneAllocatorIS5_EEE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeIN2v88internal3SmiESt4pairIKS2_jESt10_Select1stIS5_ESt4lessIS2_ENS1_13ZoneAllocatorIS5_EEE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB24633:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L306
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L300:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal3SmiESt4pairIKS2_jESt10_Select1stIS5_ESt4lessIS2_ENS1_13ZoneAllocatorIS5_EEE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L300
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE24633:
	.size	_ZNSt8_Rb_treeIN2v88internal3SmiESt4pairIKS2_jESt10_Select1stIS5_ESt4lessIS2_ENS1_13ZoneAllocatorIS5_EEE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeIN2v88internal3SmiESt4pairIKS2_jESt10_Select1stIS5_ESt4lessIS2_ENS1_13ZoneAllocatorIS5_EEE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.text._ZNSt8_Rb_treeIdSt4pairIKdjESt10_Select1stIS2_ESt4lessIdEN2v88internal13ZoneAllocatorIS2_EEE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,"axG",@progbits,_ZNSt8_Rb_treeIdSt4pairIKdjESt10_Select1stIS2_ESt4lessIdEN2v88internal13ZoneAllocatorIS2_EEE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIdSt4pairIKdjESt10_Select1stIS2_ESt4lessIdEN2v88internal13ZoneAllocatorIS2_EEE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.type	_ZNSt8_Rb_treeIdSt4pairIKdjESt10_Select1stIS2_ESt4lessIdEN2v88internal13ZoneAllocatorIS2_EEE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, @function
_ZNSt8_Rb_treeIdSt4pairIKdjESt10_Select1stIS2_ESt4lessIdEN2v88internal13ZoneAllocatorIS2_EEE8_M_eraseEPSt13_Rb_tree_nodeIS2_E:
.LFB24641:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L317
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L311:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIdSt4pairIKdjESt10_Select1stIS2_ESt4lessIdEN2v88internal13ZoneAllocatorIS2_EEE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L311
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L317:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE24641:
	.size	_ZNSt8_Rb_treeIdSt4pairIKdjESt10_Select1stIS2_ESt4lessIdEN2v88internal13ZoneAllocatorIS2_EEE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, .-_ZNSt8_Rb_treeIdSt4pairIKdjESt10_Select1stIS2_ESt4lessIdEN2v88internal13ZoneAllocatorIS2_EEE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.section	.text._ZN2v88internal11interpreter25InterpreterCompilationJobD2Ev,"axG",@progbits,_ZN2v88internal11interpreter25InterpreterCompilationJobD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter25InterpreterCompilationJobD2Ev
	.type	_ZN2v88internal11interpreter25InterpreterCompilationJobD2Ev, @function
_ZN2v88internal11interpreter25InterpreterCompilationJobD2Ev:
.LFB24705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal11interpreter25InterpreterCompilationJobE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movq	632(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L321
	call	_ZdlPv@PLT
.L321:
	movq	432(%rbx), %r12
	leaq	408(%rbx), %r14
	testq	%r12, %r12
	je	.L322
.L325:
	movq	24(%r12), %r13
	testq	%r13, %r13
	je	.L323
.L324:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIdSt4pairIKdjESt10_Select1stIS2_ESt4lessIdEN2v88internal13ZoneAllocatorIS2_EEE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L324
.L323:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L325
.L322:
	movq	344(%rbx), %r12
	leaq	320(%rbx), %r14
	testq	%r12, %r12
	je	.L326
.L329:
	movq	24(%r12), %r13
	testq	%r13, %r13
	je	.L327
.L328:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal3SmiESt4pairIKS2_jESt10_Select1stIS5_ESt4lessIS2_ENS1_13ZoneAllocatorIS5_EEE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L328
.L327:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L329
.L326:
	leaq	64(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4ZoneD1Ev@PLT
	.cfi_endproc
.LFE24705:
	.size	_ZN2v88internal11interpreter25InterpreterCompilationJobD2Ev, .-_ZN2v88internal11interpreter25InterpreterCompilationJobD2Ev
	.weak	_ZN2v88internal11interpreter25InterpreterCompilationJobD1Ev
	.set	_ZN2v88internal11interpreter25InterpreterCompilationJobD1Ev,_ZN2v88internal11interpreter25InterpreterCompilationJobD2Ev
	.section	.text._ZN2v88internal11interpreter25InterpreterCompilationJobD0Ev,"axG",@progbits,_ZN2v88internal11interpreter25InterpreterCompilationJobD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter25InterpreterCompilationJobD0Ev
	.type	_ZN2v88internal11interpreter25InterpreterCompilationJobD0Ev, @function
_ZN2v88internal11interpreter25InterpreterCompilationJobD0Ev:
.LFB24707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal11interpreter25InterpreterCompilationJobE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rax, (%rdi)
	movq	632(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L351
	call	_ZdlPv@PLT
.L351:
	movq	432(%r12), %rbx
	leaq	408(%r12), %r14
	testq	%rbx, %rbx
	je	.L352
.L355:
	movq	24(%rbx), %r13
	testq	%r13, %r13
	je	.L353
.L354:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIdSt4pairIKdjESt10_Select1stIS2_ESt4lessIdEN2v88internal13ZoneAllocatorIS2_EEE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L354
.L353:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L355
.L352:
	movq	344(%r12), %rbx
	leaq	320(%r12), %r14
	testq	%rbx, %rbx
	je	.L356
.L359:
	movq	24(%rbx), %r13
	testq	%r13, %r13
	je	.L357
.L358:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal3SmiESt4pairIKS2_jESt10_Select1stIS5_ESt4lessIS2_ENS1_13ZoneAllocatorIS5_EEE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L358
.L357:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L359
.L356:
	leaq	64(%r12), %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$1104, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24707:
	.size	_ZN2v88internal11interpreter25InterpreterCompilationJobD0Ev, .-_ZN2v88internal11interpreter25InterpreterCompilationJobD0Ev
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11interpreter11InterpreterC2EPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11interpreter11InterpreterC2EPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal11interpreter11InterpreterC2EPNS0_7IsolateE:
.LFB26441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE26441:
	.size	_GLOBAL__sub_I__ZN2v88internal11interpreter11InterpreterC2EPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal11interpreter11InterpreterC2EPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11interpreter11InterpreterC2EPNS0_7IsolateE
	.weak	_ZTVN2v88internal11interpreter11InterpreterE
	.section	.data.rel.ro.local._ZTVN2v88internal11interpreter11InterpreterE,"awG",@progbits,_ZTVN2v88internal11interpreter11InterpreterE,comdat
	.align 8
	.type	_ZTVN2v88internal11interpreter11InterpreterE, @object
	.size	_ZTVN2v88internal11interpreter11InterpreterE, 32
_ZTVN2v88internal11interpreter11InterpreterE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11interpreter11InterpreterD1Ev
	.quad	_ZN2v88internal11interpreter11InterpreterD0Ev
	.weak	_ZTVN2v88internal14CompilationJobE
	.section	.data.rel.ro.local._ZTVN2v88internal14CompilationJobE,"awG",@progbits,_ZTVN2v88internal14CompilationJobE,comdat
	.align 8
	.type	_ZTVN2v88internal14CompilationJobE, @object
	.size	_ZTVN2v88internal14CompilationJobE, 32
_ZTVN2v88internal14CompilationJobE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14CompilationJobD1Ev
	.quad	_ZN2v88internal14CompilationJobD0Ev
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.weak	_ZTVN2v88internal11interpreter25InterpreterCompilationJobE
	.section	.data.rel.ro.local._ZTVN2v88internal11interpreter25InterpreterCompilationJobE,"awG",@progbits,_ZTVN2v88internal11interpreter25InterpreterCompilationJobE,comdat
	.align 8
	.type	_ZTVN2v88internal11interpreter25InterpreterCompilationJobE, @object
	.size	_ZTVN2v88internal11interpreter25InterpreterCompilationJobE, 48
_ZTVN2v88internal11interpreter25InterpreterCompilationJobE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11interpreter25InterpreterCompilationJobD1Ev
	.quad	_ZN2v88internal11interpreter25InterpreterCompilationJobD0Ev
	.quad	_ZN2v88internal11interpreter25InterpreterCompilationJob14ExecuteJobImplEv
	.quad	_ZN2v88internal11interpreter25InterpreterCompilationJob15FinalizeJobImplENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE
	.section	.bss._ZZN2v88internal11interpreter25InterpreterCompilationJob15FinalizeJobImplENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateEE28trace_event_unique_atomic232,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal11interpreter25InterpreterCompilationJob15FinalizeJobImplENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateEE28trace_event_unique_atomic232, @object
	.size	_ZZN2v88internal11interpreter25InterpreterCompilationJob15FinalizeJobImplENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateEE28trace_event_unique_atomic232, 8
_ZZN2v88internal11interpreter25InterpreterCompilationJob15FinalizeJobImplENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateEE28trace_event_unique_atomic232:
	.zero	8
	.section	.bss._ZZN2v88internal11interpreter25InterpreterCompilationJob14ExecuteJobImplEvE28trace_event_unique_atomic194,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal11interpreter25InterpreterCompilationJob14ExecuteJobImplEvE28trace_event_unique_atomic194, @object
	.size	_ZZN2v88internal11interpreter25InterpreterCompilationJob14ExecuteJobImplEvE28trace_event_unique_atomic194, 8
_ZZN2v88internal11interpreter25InterpreterCompilationJob14ExecuteJobImplEvE28trace_event_unique_atomic194:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata._ZN2v88internalL26kBytecodeToBuiltinsMappingE,"a"
	.align 32
	.type	_ZN2v88internalL26kBytecodeToBuiltinsMappingE, @object
	.size	_ZN2v88internalL26kBytecodeToBuiltinsMappingE, 2196
_ZN2v88internalL26kBytecodeToBuiltinsMappingE:
	.long	0
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	11
	.long	12
	.long	13
	.long	14
	.long	15
	.long	16
	.long	17
	.long	18
	.long	19
	.long	20
	.long	21
	.long	22
	.long	23
	.long	24
	.long	25
	.long	26
	.long	27
	.long	28
	.long	29
	.long	30
	.long	31
	.long	32
	.long	33
	.long	34
	.long	35
	.long	36
	.long	37
	.long	38
	.long	39
	.long	40
	.long	41
	.long	42
	.long	43
	.long	44
	.long	45
	.long	46
	.long	47
	.long	48
	.long	49
	.long	50
	.long	51
	.long	52
	.long	53
	.long	54
	.long	55
	.long	56
	.long	57
	.long	58
	.long	59
	.long	60
	.long	61
	.long	62
	.long	63
	.long	64
	.long	65
	.long	66
	.long	67
	.long	68
	.long	69
	.long	70
	.long	71
	.long	72
	.long	73
	.long	74
	.long	75
	.long	76
	.long	77
	.long	78
	.long	79
	.long	80
	.long	81
	.long	82
	.long	83
	.long	84
	.long	85
	.long	86
	.long	87
	.long	88
	.long	89
	.long	90
	.long	91
	.long	92
	.long	93
	.long	94
	.long	95
	.long	96
	.long	97
	.long	98
	.long	99
	.long	100
	.long	101
	.long	102
	.long	103
	.long	104
	.long	105
	.long	106
	.long	107
	.long	108
	.long	109
	.long	110
	.long	111
	.long	112
	.long	113
	.long	114
	.long	115
	.long	116
	.long	117
	.long	118
	.long	119
	.long	120
	.long	121
	.long	122
	.long	123
	.long	124
	.long	125
	.long	126
	.long	127
	.long	128
	.long	129
	.long	130
	.long	131
	.long	132
	.long	133
	.long	134
	.long	135
	.long	136
	.long	137
	.long	138
	.long	139
	.long	140
	.long	141
	.long	142
	.long	143
	.long	144
	.long	145
	.long	146
	.long	147
	.long	148
	.long	149
	.long	150
	.long	151
	.long	152
	.long	153
	.long	154
	.long	155
	.long	156
	.long	157
	.long	158
	.long	159
	.long	160
	.long	161
	.long	162
	.long	163
	.long	164
	.long	165
	.long	166
	.long	167
	.long	168
	.long	169
	.long	170
	.long	171
	.long	172
	.long	173
	.long	174
	.long	175
	.long	176
	.long	177
	.long	178
	.long	179
	.long	180
	.long	181
	.long	182
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	183
	.long	184
	.long	185
	.long	186
	.long	187
	.long	188
	.long	-1
	.long	189
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	190
	.long	191
	.long	192
	.long	193
	.long	194
	.long	195
	.long	196
	.long	197
	.long	198
	.long	199
	.long	200
	.long	201
	.long	202
	.long	203
	.long	204
	.long	205
	.long	206
	.long	207
	.long	208
	.long	209
	.long	210
	.long	211
	.long	212
	.long	213
	.long	214
	.long	215
	.long	216
	.long	217
	.long	218
	.long	219
	.long	220
	.long	221
	.long	222
	.long	223
	.long	224
	.long	225
	.long	226
	.long	227
	.long	228
	.long	229
	.long	230
	.long	231
	.long	232
	.long	233
	.long	234
	.long	235
	.long	236
	.long	237
	.long	238
	.long	239
	.long	240
	.long	241
	.long	242
	.long	243
	.long	244
	.long	245
	.long	246
	.long	247
	.long	248
	.long	249
	.long	250
	.long	251
	.long	-1
	.long	-1
	.long	-1
	.long	252
	.long	253
	.long	254
	.long	255
	.long	256
	.long	257
	.long	258
	.long	259
	.long	260
	.long	261
	.long	262
	.long	263
	.long	264
	.long	265
	.long	266
	.long	267
	.long	268
	.long	269
	.long	270
	.long	271
	.long	272
	.long	273
	.long	274
	.long	275
	.long	276
	.long	277
	.long	278
	.long	279
	.long	280
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	281
	.long	282
	.long	283
	.long	284
	.long	-1
	.long	285
	.long	286
	.long	-1
	.long	287
	.long	288
	.long	-1
	.long	289
	.long	290
	.long	291
	.long	292
	.long	293
	.long	294
	.long	295
	.long	296
	.long	-1
	.long	-1
	.long	-1
	.long	297
	.long	298
	.long	299
	.long	300
	.long	301
	.long	302
	.long	303
	.long	304
	.long	305
	.long	306
	.long	307
	.long	308
	.long	309
	.long	310
	.long	311
	.long	312
	.long	313
	.long	314
	.long	315
	.long	316
	.long	317
	.long	318
	.long	319
	.long	320
	.long	321
	.long	322
	.long	323
	.long	324
	.long	325
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	326
	.long	-1
	.long	-1
	.long	327
	.long	328
	.long	329
	.long	330
	.long	-1
	.long	331
	.long	332
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	333
	.long	334
	.long	335
	.long	336
	.long	337
	.long	338
	.long	-1
	.long	339
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	340
	.long	341
	.long	342
	.long	343
	.long	344
	.long	345
	.long	346
	.long	347
	.long	348
	.long	349
	.long	350
	.long	351
	.long	352
	.long	353
	.long	354
	.long	355
	.long	356
	.long	357
	.long	358
	.long	359
	.long	360
	.long	361
	.long	362
	.long	363
	.long	364
	.long	365
	.long	366
	.long	367
	.long	368
	.long	369
	.long	370
	.long	371
	.long	372
	.long	373
	.long	374
	.long	375
	.long	376
	.long	377
	.long	378
	.long	379
	.long	380
	.long	381
	.long	382
	.long	383
	.long	384
	.long	385
	.long	386
	.long	387
	.long	388
	.long	389
	.long	390
	.long	391
	.long	392
	.long	393
	.long	394
	.long	395
	.long	396
	.long	397
	.long	398
	.long	399
	.long	400
	.long	401
	.long	-1
	.long	-1
	.long	-1
	.long	402
	.long	403
	.long	404
	.long	405
	.long	406
	.long	407
	.long	408
	.long	409
	.long	410
	.long	411
	.long	412
	.long	413
	.long	414
	.long	415
	.long	416
	.long	417
	.long	418
	.long	419
	.long	420
	.long	421
	.long	422
	.long	423
	.long	424
	.long	425
	.long	426
	.long	427
	.long	428
	.long	429
	.long	430
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	431
	.long	432
	.long	433
	.long	434
	.long	-1
	.long	435
	.long	436
	.long	-1
	.long	437
	.long	438
	.long	-1
	.long	439
	.long	440
	.long	441
	.long	442
	.long	443
	.long	444
	.long	445
	.long	446
	.long	-1
	.long	-1
	.long	-1
	.long	447
	.long	448
	.long	449
	.long	450
	.long	451
	.long	452
	.long	453
	.long	454
	.long	455
	.long	456
	.long	457
	.long	458
	.long	459
	.long	460
	.long	461
	.long	462
	.long	463
	.long	464
	.long	465
	.long	466
	.long	467
	.long	468
	.long	469
	.long	470
	.long	471
	.long	472
	.long	473
	.long	474
	.long	475
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	476
	.long	-1
	.long	-1
	.long	477
	.long	478
	.long	479
	.long	480
	.long	-1
	.long	481
	.long	482
	.long	-1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
