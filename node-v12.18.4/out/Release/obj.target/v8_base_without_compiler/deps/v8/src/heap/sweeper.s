	.file	"sweeper.cc"
	.text
	.section	.text._ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB2827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2827:
	.size	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4177:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4177:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4178:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4178:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB4180:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4180:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal14CancelableTask3RunEv,"axG",@progbits,_ZN2v88internal14CancelableTask3RunEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CancelableTask3RunEv
	.type	_ZN2v88internal14CancelableTask3RunEv, @function
_ZN2v88internal14CancelableTask3RunEv:
.LFB4217:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	movl	$2, %edx
	lock cmpxchgl	%edx, 16(%rdi)
	jne	.L6
	movq	(%rdi), %rax
	jmp	*24(%rax)
.L6:
	ret
	.cfi_endproc
.LFE4217:
	.size	_ZN2v88internal14CancelableTask3RunEv, .-_ZN2v88internal14CancelableTask3RunEv
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB26744:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE26744:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZN2v88internal7Sweeper11SweeperTaskD2Ev,"axG",@progbits,_ZN2v88internal7Sweeper11SweeperTaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Sweeper11SweeperTaskD2Ev
	.type	_ZN2v88internal7Sweeper11SweeperTaskD2Ev, @function
_ZN2v88internal7Sweeper11SweeperTaskD2Ev:
.LFB26537:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE26537:
	.size	_ZN2v88internal7Sweeper11SweeperTaskD2Ev, .-_ZN2v88internal7Sweeper11SweeperTaskD2Ev
	.weak	_ZN2v88internal7Sweeper11SweeperTaskD1Ev
	.set	_ZN2v88internal7Sweeper11SweeperTaskD1Ev,_ZN2v88internal7Sweeper11SweeperTaskD2Ev
	.section	.text._ZN2v88internal7Sweeper11SweeperTaskD0Ev,"axG",@progbits,_ZN2v88internal7Sweeper11SweeperTaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Sweeper11SweeperTaskD0Ev
	.type	_ZN2v88internal7Sweeper11SweeperTaskD0Ev, @function
_ZN2v88internal7Sweeper11SweeperTaskD0Ev:
.LFB26539:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$80, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26539:
	.size	_ZN2v88internal7Sweeper11SweeperTaskD0Ev, .-_ZN2v88internal7Sweeper11SweeperTaskD0Ev
	.section	.text._ZN2v88internal7Sweeper15IterabilityTaskD2Ev,"axG",@progbits,_ZN2v88internal7Sweeper15IterabilityTaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Sweeper15IterabilityTaskD2Ev
	.type	_ZN2v88internal7Sweeper15IterabilityTaskD2Ev, @function
_ZN2v88internal7Sweeper15IterabilityTaskD2Ev:
.LFB26648:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE26648:
	.size	_ZN2v88internal7Sweeper15IterabilityTaskD2Ev, .-_ZN2v88internal7Sweeper15IterabilityTaskD2Ev
	.weak	_ZN2v88internal7Sweeper15IterabilityTaskD1Ev
	.set	_ZN2v88internal7Sweeper15IterabilityTaskD1Ev,_ZN2v88internal7Sweeper15IterabilityTaskD2Ev
	.section	.text._ZN2v88internal7Sweeper15IterabilityTaskD0Ev,"axG",@progbits,_ZN2v88internal7Sweeper15IterabilityTaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Sweeper15IterabilityTaskD0Ev
	.type	_ZN2v88internal7Sweeper15IterabilityTaskD0Ev, @function
_ZN2v88internal7Sweeper15IterabilityTaskD0Ev:
.LFB26650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26650:
	.size	_ZN2v88internal7Sweeper15IterabilityTaskD0Ev, .-_ZN2v88internal7Sweeper15IterabilityTaskD0Ev
	.section	.text._ZN2v88internal7Sweeper22IncrementalSweeperTaskD2Ev,"axG",@progbits,_ZN2v88internal7Sweeper22IncrementalSweeperTaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Sweeper22IncrementalSweeperTaskD2Ev
	.type	_ZN2v88internal7Sweeper22IncrementalSweeperTaskD2Ev, @function
_ZN2v88internal7Sweeper22IncrementalSweeperTaskD2Ev:
.LFB26630:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE26630:
	.size	_ZN2v88internal7Sweeper22IncrementalSweeperTaskD2Ev, .-_ZN2v88internal7Sweeper22IncrementalSweeperTaskD2Ev
	.weak	_ZN2v88internal7Sweeper22IncrementalSweeperTaskD1Ev
	.set	_ZN2v88internal7Sweeper22IncrementalSweeperTaskD1Ev,_ZN2v88internal7Sweeper22IncrementalSweeperTaskD2Ev
	.section	.text._ZN2v88internal7Sweeper22IncrementalSweeperTaskD0Ev,"axG",@progbits,_ZN2v88internal7Sweeper22IncrementalSweeperTaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Sweeper22IncrementalSweeperTaskD0Ev
	.type	_ZN2v88internal7Sweeper22IncrementalSweeperTaskD0Ev, @function
_ZN2v88internal7Sweeper22IncrementalSweeperTaskD0Ev:
.LFB26632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26632:
	.size	_ZN2v88internal7Sweeper22IncrementalSweeperTaskD0Ev, .-_ZN2v88internal7Sweeper22IncrementalSweeperTaskD0Ev
	.section	.text._ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPPN2v88internal4PageESt6vectorIS5_SaIS5_EEEElS5_NS0_5__ops15_Iter_comp_iterIZZNS3_7Sweeper13StartSweepingEvENKUlNS3_15AllocationSpaceEE_clESE_EUlS5_S5_E_EEEvT_T0_SJ_T1_T2_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPPN2v88internal4PageESt6vectorIS5_SaIS5_EEEElS5_NS0_5__ops15_Iter_comp_iterIZZNS3_7Sweeper13StartSweepingEvENKUlNS3_15AllocationSpaceEE_clESE_EUlS5_S5_E_EEEvT_T0_SJ_T1_T2_.isra.0, @function
_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPPN2v88internal4PageESt6vectorIS5_SaIS5_EEEElS5_NS0_5__ops15_Iter_comp_iterIZZNS3_7Sweeper13StartSweepingEvENKUlNS3_15AllocationSpaceEE_clESE_EUlS5_S5_E_EEEvT_T0_SJ_T1_T2_.isra.0:
.LFB29184:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdx), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rax, %r12
	andl	$1, %r13d
	shrq	$63, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	addq	%rax, %r12
	sarq	%r12
	cmpq	%r12, %rsi
	jge	.L19
	movq	%rsi, %r10
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%r14, (%rdi,%r10,8)
	cmpq	%r8, %r12
	jle	.L21
.L22:
	movq	%r8, %r10
.L23:
	leaq	1(%r10), %rax
	leaq	(%rax,%rax), %r8
	salq	$4, %rax
	leaq	-1(%r8), %r9
	addq	%rdi, %rax
	leaq	(%rdi,%r9,8), %rbx
	movq	(%rax), %r14
	movq	(%rbx), %r11
	movq	96(%r14), %r15
	cmpq	%r15, 96(%r11)
	jge	.L33
	movq	%r11, (%rdi,%r10,8)
	cmpq	%r9, %r12
	jle	.L28
	movq	%r9, %r8
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L28:
	movq	%rbx, %rax
	movq	%r9, %r8
.L21:
	testq	%r13, %r13
	je	.L27
.L24:
	leaq	-1(%r8), %r9
	movq	%r9, %rdx
	shrq	$63, %rdx
	addq	%r9, %rdx
	sarq	%rdx
	cmpq	%rsi, %r8
	jg	.L26
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L35:
	leaq	-1(%rdx), %r8
	movq	%r9, (%rax)
	movq	%r8, %rax
	shrq	$63, %rax
	addq	%r8, %rax
	movq	%rdx, %r8
	sarq	%rax
	cmpq	%rdx, %rsi
	jge	.L34
	movq	%rax, %rdx
.L26:
	leaq	(%rdi,%rdx,8), %r10
	movq	96(%rcx), %rbx
	leaq	(%rdi,%r8,8), %rax
	movq	(%r10), %r9
	cmpq	%rbx, 96(%r9)
	jg	.L35
.L25:
	movq	%rcx, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	leaq	(%rdi,%rsi,8), %rax
	testq	%r13, %r13
	jne	.L25
	movq	%rsi, %r8
	.p2align 4,,10
	.p2align 3
.L27:
	leaq	-2(%rdx), %r9
	movq	%r9, %rdx
	shrq	$63, %rdx
	addq	%r9, %rdx
	sarq	%rdx
	cmpq	%rdx, %r8
	jne	.L24
	leaq	1(%r8,%r8), %r8
	leaq	(%rdi,%r8,8), %rdx
	movq	(%rdx), %r9
	movq	%r9, (%rax)
	movq	%rdx, %rax
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%r10, %rax
	movq	%rcx, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE29184:
	.size	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPPN2v88internal4PageESt6vectorIS5_SaIS5_EEEElS5_NS0_5__ops15_Iter_comp_iterIZZNS3_7Sweeper13StartSweepingEvENKUlNS3_15AllocationSpaceEE_clESE_EUlS5_S5_E_EEEvT_T0_SJ_T1_T2_.isra.0, .-_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPPN2v88internal4PageESt6vectorIS5_SaIS5_EEEElS5_NS0_5__ops15_Iter_comp_iterIZZNS3_7Sweeper13StartSweepingEvENKUlNS3_15AllocationSpaceEE_clESE_EUlS5_S5_E_EEEvT_T0_SJ_T1_T2_.isra.0
	.section	.text._ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPPN2v88internal4PageESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZZNS3_7Sweeper13StartSweepingEvENKUlNS3_15AllocationSpaceEE_clESE_EUlS5_S5_E_EEEvT_SI_T0_T1_,"ax",@progbits
	.p2align 4
	.type	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPPN2v88internal4PageESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZZNS3_7Sweeper13StartSweepingEvENKUlNS3_15AllocationSpaceEE_clESE_EUlS5_S5_E_EEEvT_SI_T0_T1_, @function
_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPPN2v88internal4PageESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZZNS3_7Sweeper13StartSweepingEvENKUlNS3_15AllocationSpaceEE_clESE_EUlS5_S5_E_EEEvT_SI_T0_T1_:
.LFB27568:
	.cfi_startproc
	movq	%rsi, %rax
	subq	%rdi, %rax
	cmpq	$128, %rax
	jle	.L65
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	testq	%rdx, %rdx
	je	.L38
	leaq	8(%rdi), %rax
	movq	%rcx, %r14
	movq	%rax, -56(%rbp)
.L40:
	movq	%rsi, %rax
	movq	-8(%rsi), %r12
	subq	$1, %r15
	subq	%rbx, %rax
	movq	%rax, %rcx
	shrq	$63, %rax
	movq	96(%r12), %r9
	sarq	$3, %rcx
	addq	%rcx, %rax
	movq	(%rbx), %rcx
	sarq	%rax
	leaq	(%rbx,%rax,8), %r10
	movq	8(%rbx), %rax
	movq	(%r10), %r11
	movq	96(%rax), %r8
	movq	96(%r11), %rdi
	cmpq	%rdi, %r8
	jle	.L45
	cmpq	%r9, %rdi
	jg	.L50
	cmpq	%r9, %r8
	jle	.L48
.L68:
	movq	%r12, (%rbx)
	movq	%rcx, -8(%rsi)
	movq	(%rbx), %rax
.L47:
	movq	96(%rcx), %r10
	movq	-56(%rbp), %r12
	movq	96(%rax), %r8
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L51:
	movq	(%r12), %r9
	movq	%r12, %r13
	cmpq	%r8, 96(%r9)
	jg	.L52
	leaq	-8(%rax), %rdi
	cmpq	%r10, %r8
	jle	.L53
	subq	$16, %rax
	.p2align 4,,10
	.p2align 3
.L54:
	movq	(%rax), %rcx
	movq	%rax, %rdi
	subq	$8, %rax
	cmpq	%r8, 96(%rcx)
	jl	.L54
.L53:
	cmpq	%r12, %rdi
	jbe	.L69
	movq	%rcx, (%r12)
	movq	-8(%rdi), %rcx
	movq	%r9, (%rdi)
	movq	(%rbx), %rax
	movq	96(%rcx), %r10
	movq	96(%rax), %r8
	movq	%rdi, %rax
.L52:
	addq	$8, %r12
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L69:
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPPN2v88internal4PageESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZZNS3_7Sweeper13StartSweepingEvENKUlNS3_15AllocationSpaceEE_clESE_EUlS5_S5_E_EEEvT_SI_T0_T1_
	movq	%r12, %rax
	subq	%rbx, %rax
	cmpq	$128, %rax
	jle	.L36
	testq	%r15, %r15
	je	.L38
	movq	%r12, %rsi
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L45:
	cmpq	%r9, %r8
	jle	.L49
	movq	%rcx, %xmm1
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	movq	-8(%rsi), %rcx
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L49:
	cmpq	%r9, %rdi
	jg	.L68
.L50:
	movq	%r11, (%rbx)
	movq	%rcx, (%r10)
	movq	(%rbx), %rax
	movq	-8(%rsi), %rcx
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L38:
	sarq	$3, %rax
	leaq	-2(%rax), %rsi
	movq	%rax, %r12
	sarq	%rsi
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L70:
	subq	$1, %rsi
.L42:
	movq	(%rbx,%rsi,8), %rcx
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPPN2v88internal4PageESt6vectorIS5_SaIS5_EEEElS5_NS0_5__ops15_Iter_comp_iterIZZNS3_7Sweeper13StartSweepingEvENKUlNS3_15AllocationSpaceEE_clESE_EUlS5_S5_E_EEEvT_T0_SJ_T1_T2_.isra.0
	testq	%rsi, %rsi
	jne	.L70
	subq	$8, %r13
	.p2align 4,,10
	.p2align 3
.L43:
	movq	(%rbx), %rax
	movq	%r13, %r12
	movq	0(%r13), %rcx
	xorl	%esi, %esi
	subq	%rbx, %r12
	movq	%rbx, %rdi
	subq	$8, %r13
	movq	%rax, 8(%r13)
	movq	%r12, %rdx
	sarq	$3, %rdx
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPPN2v88internal4PageESt6vectorIS5_SaIS5_EEEElS5_NS0_5__ops15_Iter_comp_iterIZZNS3_7Sweeper13StartSweepingEvENKUlNS3_15AllocationSpaceEE_clESE_EUlS5_S5_E_EEEvT_T0_SJ_T1_T2_.isra.0
	cmpq	$8, %r12
	jg	.L43
.L36:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movq	%rcx, %xmm2
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rbx)
	movq	-8(%rsi), %rcx
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE27568:
	.size	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPPN2v88internal4PageESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZZNS3_7Sweeper13StartSweepingEvENKUlNS3_15AllocationSpaceEE_clESE_EUlS5_S5_E_EEEvT_SI_T0_T1_, .-_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPPN2v88internal4PageESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZZNS3_7Sweeper13StartSweepingEvENKUlNS3_15AllocationSpaceEE_clESE_EUlS5_S5_E_EEEvT_SI_T0_T1_
	.section	.text._ZN2v88internal7Sweeper22IncrementalSweeperTaskD2Ev,"axG",@progbits,_ZN2v88internal7Sweeper22IncrementalSweeperTaskD5Ev,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal7Sweeper22IncrementalSweeperTaskD1Ev
	.type	_ZThn32_N2v88internal7Sweeper22IncrementalSweeperTaskD1Ev, @function
_ZThn32_N2v88internal7Sweeper22IncrementalSweeperTaskD1Ev:
.LFB29289:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	subq	$32, %rdi
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE29289:
	.size	_ZThn32_N2v88internal7Sweeper22IncrementalSweeperTaskD1Ev, .-_ZThn32_N2v88internal7Sweeper22IncrementalSweeperTaskD1Ev
	.section	.text._ZN2v88internal7Sweeper11SweeperTaskD2Ev,"axG",@progbits,_ZN2v88internal7Sweeper11SweeperTaskD5Ev,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal7Sweeper11SweeperTaskD1Ev
	.type	_ZThn32_N2v88internal7Sweeper11SweeperTaskD1Ev, @function
_ZThn32_N2v88internal7Sweeper11SweeperTaskD1Ev:
.LFB29290:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	subq	$32, %rdi
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE29290:
	.size	_ZThn32_N2v88internal7Sweeper11SweeperTaskD1Ev, .-_ZThn32_N2v88internal7Sweeper11SweeperTaskD1Ev
	.section	.text._ZN2v88internal7Sweeper15IterabilityTaskD2Ev,"axG",@progbits,_ZN2v88internal7Sweeper15IterabilityTaskD5Ev,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal7Sweeper15IterabilityTaskD1Ev
	.type	_ZThn32_N2v88internal7Sweeper15IterabilityTaskD1Ev, @function
_ZThn32_N2v88internal7Sweeper15IterabilityTaskD1Ev:
.LFB29291:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	subq	$32, %rdi
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE29291:
	.size	_ZThn32_N2v88internal7Sweeper15IterabilityTaskD1Ev, .-_ZThn32_N2v88internal7Sweeper15IterabilityTaskD1Ev
	.section	.text._ZN2v88internal7Sweeper22IncrementalSweeperTaskD0Ev,"axG",@progbits,_ZN2v88internal7Sweeper22IncrementalSweeperTaskD5Ev,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal7Sweeper22IncrementalSweeperTaskD0Ev
	.type	_ZThn32_N2v88internal7Sweeper22IncrementalSweeperTaskD0Ev, @function
_ZThn32_N2v88internal7Sweeper22IncrementalSweeperTaskD0Ev:
.LFB29292:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-32(%rdi), %r12
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	movq	%r12, %rdi
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE29292:
	.size	_ZThn32_N2v88internal7Sweeper22IncrementalSweeperTaskD0Ev, .-_ZThn32_N2v88internal7Sweeper22IncrementalSweeperTaskD0Ev
	.section	.text._ZN2v88internal7Sweeper15IterabilityTaskD0Ev,"axG",@progbits,_ZN2v88internal7Sweeper15IterabilityTaskD5Ev,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal7Sweeper15IterabilityTaskD0Ev
	.type	_ZThn32_N2v88internal7Sweeper15IterabilityTaskD0Ev, @function
_ZThn32_N2v88internal7Sweeper15IterabilityTaskD0Ev:
.LFB29293:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-32(%rdi), %r12
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	movq	%r12, %rdi
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE29293:
	.size	_ZThn32_N2v88internal7Sweeper15IterabilityTaskD0Ev, .-_ZThn32_N2v88internal7Sweeper15IterabilityTaskD0Ev
	.section	.text._ZN2v88internal7Sweeper11SweeperTaskD0Ev,"axG",@progbits,_ZN2v88internal7Sweeper11SweeperTaskD5Ev,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal7Sweeper11SweeperTaskD0Ev
	.type	_ZThn32_N2v88internal7Sweeper11SweeperTaskD0Ev, @function
_ZThn32_N2v88internal7Sweeper11SweeperTaskD0Ev:
.LFB29294:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-32(%rdi), %r12
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	movq	%r12, %rdi
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$80, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE29294:
	.size	_ZThn32_N2v88internal7Sweeper11SweeperTaskD0Ev, .-_ZThn32_N2v88internal7Sweeper11SweeperTaskD0Ev
	.section	.text._ZN2v88internal14CancelableTask3RunEv,"axG",@progbits,_ZN2v88internal14CancelableTask3RunEv,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal14CancelableTask3RunEv
	.type	_ZThn32_N2v88internal14CancelableTask3RunEv, @function
_ZThn32_N2v88internal14CancelableTask3RunEv:
.LFB29295:
	.cfi_startproc
	endbr64
	leaq	-32(%rdi), %r8
	xorl	%eax, %eax
	movl	$2, %edx
	lock cmpxchgl	%edx, -16(%rdi)
	jne	.L80
	movq	-32(%rdi), %rax
	movq	%r8, %rdi
	movq	24(%rax), %rax
	jmp	*%rax
.L80:
	ret
	.cfi_endproc
.LFE29295:
	.size	_ZThn32_N2v88internal14CancelableTask3RunEv, .-_ZThn32_N2v88internal14CancelableTask3RunEv
	.section	.text._ZN2v88internal7Sweeper31ScheduleIncrementalSweepingTaskEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal7Sweeper31ScheduleIncrementalSweepingTaskEv.part.0, @function
_ZN2v88internal7Sweeper31ScheduleIncrementalSweepingTaskEv.part.0:
.LFB29208:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$1, 264(%rdi)
	movq	(%rdi), %rax
	leaq	-37592(%rax), %r13
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%r13, %rdx
	leaq	-80(%rbp), %rdi
	movq	%rax, %rsi
	movq	(%rax), %rax
	call	*48(%rax)
	movq	-80(%rbp), %r14
	movl	$56, %edi
	movq	(%r14), %rax
	movq	(%rax), %r15
	movq	(%r12), %rax
	leaq	-37592(%rax), %r13
	call	_Znwm@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE@PLT
	addq	$32, %rbx
	movq	%r13, 8(%rbx)
	movq	%r14, %rdi
	leaq	16+_ZTVN2v88internal7Sweeper22IncrementalSweeperTaskE(%rip), %rax
	movq	%r12, 16(%rbx)
	leaq	-88(%rbp), %rsi
	movq	%rax, -32(%rbx)
	addq	$48, %rax
	movq	%rax, (%rbx)
	movq	%rbx, -88(%rbp)
	call	*%r15
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L83
	movq	(%rdi), %rax
	call	*8(%rax)
.L83:
	movq	-72(%rbp), %r12
	testq	%r12, %r12
	je	.L82
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L86
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L98
	.p2align 4,,10
	.p2align 3
.L82:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L99
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L82
.L98:
	movq	(%r12), %rax
	leaq	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L100
.L89:
	testq	%rbx, %rbx
	je	.L90
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L91:
	cmpl	$1, %eax
	jne	.L82
	movq	(%r12), %rax
	leaq	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv(%rip), %rcx
	movq	%r12, %rdi
	movq	24(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L92
	call	*8(%rax)
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L90:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L92:
	call	*%rdx
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L100:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L89
.L99:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29208:
	.size	_ZN2v88internal7Sweeper31ScheduleIncrementalSweepingTaskEv.part.0, .-_ZN2v88internal7Sweeper31ScheduleIncrementalSweepingTaskEv.part.0
	.section	.rodata._ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE11RemoveRangeEPNS0_11MemoryChunkEmmNS0_7SlotSet15EmptyBucketModeE.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"end_offset <= 1 << kPageSizeBits"
	.section	.rodata._ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE11RemoveRangeEPNS0_11MemoryChunkEmmNS0_7SlotSet15EmptyBucketModeE.constprop.0.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE11RemoveRangeEPNS0_11MemoryChunkEmmNS0_7SlotSet15EmptyBucketModeE.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE11RemoveRangeEPNS0_11MemoryChunkEmmNS0_7SlotSet15EmptyBucketModeE.constprop.0, @function
_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE11RemoveRangeEPNS0_11MemoryChunkEmmNS0_7SlotSet15EmptyBucketModeE.constprop.0:
.LFB29302:
	.cfi_startproc
	movq	112(%rdi), %r8
	testq	%r8, %r8
	jne	.L276
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	subq	%rdi, %rdx
	subq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	$262143, %rdx
	jbe	.L277
	leaq	-1(%rdx), %rdi
	movq	%rcx, %r9
	andl	$262143, %ecx
	shrq	$18, %rdi
	shrq	$18, %r9
	movslq	%edi, %rax
	movslq	%r9d, %r10
	movq	%rax, -56(%rbp)
	movl	%edi, %eax
	sall	$18, %eax
	subl	%eax, %edx
	leaq	(%r10,%r10,2), %rax
	salq	$7, %rax
	leaq	(%r8,%rax), %r15
	cmpl	%edi, %r9d
	je	.L278
	movl	%ecx, %r13d
	sarl	$13, %r13d
	movslq	%r13d, %r12
	leaq	0(,%r12,8), %rax
	movq	%rax, -64(%rbp)
	movq	(%r15,%r12,8), %r14
	testq	%r14, %r14
	jne	.L146
.L151:
	cmpl	$31, %r13d
	je	.L147
	movq	-64(%rbp), %rax
	leaq	8(%r15,%rax), %rcx
	movl	$30, %eax
	subl	%r13d, %eax
	addq	%rax, %r12
	leaq	16(%r15,%r12,8), %r11
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L279:
	addq	$8, %rcx
	cmpq	%r11, %rcx
	je	.L147
.L156:
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L279
	leaq	128(%rax), %rsi
	.p2align 4,,10
	.p2align 3
.L158:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rsi
	jne	.L158
	addq	$8, %rcx
	cmpq	%r11, %rcx
	jne	.L156
.L147:
	movq	256(%r15), %rax
	leal	1(%r9), %eax
	cmpl	%eax, %edi
	jle	.L153
	cltq
	subl	%r9d, %edi
	leaq	(%rax,%rax,2), %rax
	salq	$7, %rax
	leaq	256(%r8,%rax), %r11
	leal	-2(%rdi), %eax
	xorl	%edi, %edi
	addq	%rax, %r10
	leaq	(%r10,%r10,2), %rax
	salq	$7, %rax
	leaq	1024(%r8,%rax), %r9
	.p2align 4,,10
	.p2align 3
.L172:
	movq	-256(%r11), %rcx
	testq	%rcx, %rcx
	jne	.L165
.L167:
	leaq	-248(%r11), %rcx
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L281:
	addq	$8, %rcx
	cmpq	%r11, %rcx
	je	.L280
.L162:
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L281
	leaq	128(%rax), %rsi
	.p2align 4,,10
	.p2align 3
.L171:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rsi
	jne	.L171
	addq	$8, %rcx
	cmpq	%r11, %rcx
	jne	.L162
.L280:
	movq	(%r11), %rax
	addq	$384, %r11
	cmpq	%r11, %r9
	jne	.L172
.L153:
	movq	-56(%rbp), %rax
	leaq	(%rax,%rax,2), %rax
	salq	$7, %rax
	addq	%rax, %r8
	cmpl	$262144, %edx
	jg	.L159
	movl	%edx, %r10d
	movl	%edx, %edi
	sarl	$3, %edx
	movl	$1, %esi
	sarl	$13, %r10d
	movl	%edx, %ecx
	sarl	$8, %edi
	sall	%cl, %esi
	andl	$31, %edi
	movl	%r10d, %eax
	movl	%esi, %r9d
	negl	%r9d
	orl	%edi, %eax
	je	.L282
	movq	(%r8), %rdx
	testq	%rdx, %rdx
	jne	.L283
	testl	%r10d, %r10d
	jg	.L193
.L275:
	xorl	%eax, %eax
	movl	$1, %r10d
.L180:
	movq	(%r8), %rcx
	testq	%rcx, %rcx
	je	.L101
	testb	%al, %al
	jne	.L101
	cmpl	%edi, %r10d
	jge	.L188
	leal	-1(%rdi), %edx
	movslq	%r10d, %r8
	subl	%r10d, %edx
	leaq	(%rcx,%r8,4), %rax
	addq	%r8, %rdx
	leaq	4(%rcx,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L189:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L189
.L188:
	movslq	%edi, %rdi
	subl	$1, %esi
	leaq	(%rcx,%rdi,4), %rcx
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L284:
	movl	%r9d, %edi
	movl	%edx, %eax
	andl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	je	.L101
.L187:
	movl	(%rcx), %edx
	testl	%edx, %esi
	jne	.L284
.L101:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L277:
	.cfi_restore_state
	movl	%ecx, %edi
	movl	$1, %esi
	movl	%ecx, %r11d
	movl	%edx, %ebx
	sarl	$3, %ecx
	movl	%edx, %r9d
	movl	%esi, %eax
	sarl	$3, %edx
	sarl	$13, %edi
	sall	%cl, %eax
	movl	%edx, %ecx
	sall	%cl, %esi
	movslq	%edi, %rdx
	sarl	$8, %r11d
	leal	-1(%rax), %r12d
	sarl	$8, %r9d
	sarl	$13, %ebx
	movl	%esi, -56(%rbp)
	andl	$31, %r11d
	leaq	0(,%rdx,8), %r15
	movq	%rdx, -64(%rbp)
	andl	$31, %r9d
	negl	%esi
	leaq	(%r8,%r15), %r13
	cmpl	%ebx, %edi
	jne	.L104
	cmpl	%r9d, %r11d
	je	.L285
.L104:
	movq	0(%r13), %rdx
	leal	1(%r11), %r14d
	movq	%rdx, -72(%rbp)
	testq	%rdx, %rdx
	jne	.L286
	cmpl	%ebx, %edi
	jl	.L113
.L117:
	movq	0(%r13), %rcx
	cmpl	$32, %edi
	je	.L101
	testq	%rcx, %rcx
	je	.L101
	cmpl	%r14d, %r9d
	jle	.L122
	leal	-1(%r9), %edx
	movslq	%r14d, %rdi
	subl	%r14d, %edx
	leaq	(%rcx,%rdi,4), %rax
	addq	%rdi, %rdx
	leaq	4(%rcx,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L123:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L123
.L122:
	movl	-56(%rbp), %r8d
	movslq	%r9d, %r9
	leaq	(%rcx,%r9,4), %rcx
	subl	$1, %r8d
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L287:
	movl	%esi, %edi
	movl	%edx, %eax
	andl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	je	.L101
.L121:
	movl	(%rcx), %edx
	testl	%edx, %r8d
	jne	.L287
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L146:
	movl	%ecx, %eax
	sarl	$3, %ecx
	movq	%r8, -80(%rbp)
	sarl	$8, %eax
	movl	%eax, %ebx
	movl	%eax, %esi
	andl	$31, %ebx
	andl	$31, %esi
	movl	%ebx, -72(%rbp)
	movl	$1, %ebx
	leaq	0(,%rsi,4), %rax
	sall	%cl, %ebx
	movq	%rsi, -88(%rbp)
	leaq	(%r14,%rax), %rsi
	leal	-1(%rbx), %r11d
	movq	%rax, -96(%rbp)
	negl	%ebx
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L288:
	movl	%r11d, %r8d
	movl	%ecx, %eax
	andl	%ecx, %r8d
	lock cmpxchgl	%r8d, (%rsi)
	cmpl	%eax, %ecx
	je	.L268
.L150:
	movl	(%rsi), %ecx
	testl	%ecx, %ebx
	jne	.L288
.L268:
	cmpl	$31, -72(%rbp)
	movq	-80(%rbp), %r8
	je	.L151
	movq	-96(%rbp), %rax
	movl	$30, %ecx
	subl	-72(%rbp), %ecx
	addq	-88(%rbp), %rcx
	leaq	4(%r14,%rax), %rax
	leaq	8(%r14,%rcx,4), %rcx
	.p2align 4,,10
	.p2align 3
.L152:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rcx, %rax
	jne	.L152
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L163:
	movl	%esi, %eax
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %esi
	je	.L166
.L165:
	movl	(%rcx), %esi
	testl	%esi, %esi
	jne	.L163
.L166:
	leaq	4(%rcx), %rax
	subq	$-128, %rcx
	.p2align 4,,10
	.p2align 3
.L164:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rcx, %rax
	jne	.L164
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L278:
	cmpl	$262144, %edx
	jg	.L159
	movl	$1, %eax
	movl	%ecx, %r11d
	movl	%ecx, %r8d
	movl	%edx, %ebx
	sarl	$3, %ecx
	movl	%edx, %edi
	movl	%eax, %r9d
	sarl	$3, %edx
	sall	%cl, %r9d
	movl	%edx, %ecx
	sarl	$8, %r8d
	sall	%cl, %eax
	sarl	$8, %edi
	andl	$31, %r8d
	leal	-1(%r9), %r12d
	movl	%eax, %esi
	andl	$31, %edi
	sarl	$13, %r11d
	movl	%eax, -64(%rbp)
	sarl	$13, %ebx
	negl	%esi
	cmpl	%edi, %r8d
	movslq	%r11d, %rcx
	sete	%dl
	cmpl	%ebx, %r11d
	movq	%rcx, -88(%rbp)
	sete	%al
	salq	$3, %rcx
	leaq	(%r15,%rcx), %r13
	andb	%al, %dl
	movq	%rcx, -72(%rbp)
	movb	%dl, -56(%rbp)
	movq	0(%r13), %rax
	jne	.L289
	movq	%rax, -80(%rbp)
	leal	1(%r8), %r14d
	testq	%rax, %rax
	jne	.L290
	cmpl	%ebx, %r11d
	jl	.L134
.L141:
	movq	0(%r13), %rcx
	testq	%rcx, %rcx
	je	.L101
	cmpb	$0, -56(%rbp)
	jne	.L101
	cmpl	%r14d, %edi
	jle	.L144
	leal	-1(%rdi), %edx
	movslq	%r14d, %r8
	subl	%r14d, %edx
	leaq	(%rcx,%r8,4), %rax
	addq	%r8, %rdx
	leaq	4(%rcx,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L145:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L145
.L144:
	movl	-64(%rbp), %r8d
	movslq	%edi, %rdi
	leaq	(%rcx,%rdi,4), %rcx
	subl	$1, %r8d
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L291:
	movl	%esi, %edi
	movl	%edx, %eax
	andl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	je	.L101
.L143:
	movl	(%rcx), %edx
	testl	%edx, %r8d
	jne	.L291
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L286:
	negl	%eax
	movq	-72(%rbp), %rcx
	movq	%r8, -80(%rbp)
	movl	%eax, %r10d
	movslq	%r11d, %rax
	movq	%rax, -88(%rbp)
	salq	$2, %rax
	movq	%rax, -96(%rbp)
	addq	%rax, %rcx
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L292:
	movl	%r12d, %r8d
	movl	%edx, %eax
	andl	%edx, %r8d
	lock cmpxchgl	%r8d, (%rcx)
	cmpl	%eax, %edx
	je	.L266
.L109:
	movl	(%rcx), %edx
	testl	%edx, %r10d
	jne	.L292
.L266:
	movq	-80(%rbp), %r8
	cmpl	%ebx, %edi
	jge	.L117
	cmpl	$32, %r14d
	je	.L113
	movq	-72(%rbp), %rcx
	movq	-96(%rbp), %rax
	movl	$30, %edx
	subl	%r11d, %edx
	addq	-88(%rbp), %rdx
	leaq	4(%rcx,%rax), %rax
	leaq	8(%rcx,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L114:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L114
.L113:
	leal	1(%rdi), %r11d
	cmpl	%r11d, %ebx
	jle	.L293
	leal	-2(%rbx), %eax
	leaq	8(%r8,%r15), %rdx
	subl	%edi, %eax
	addq	-64(%rbp), %rax
	leaq	16(%r8,%rax,8), %r10
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L118:
	addq	$8, %rdx
	cmpq	%rdx, %r10
	je	.L294
.L116:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L118
	leaq	128(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L119:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rcx
	jne	.L119
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L294:
	leal	-1(%rbx,%r11), %eax
	xorl	%r14d, %r14d
	subl	%edi, %eax
	movl	%eax, %edi
	cltq
	leaq	(%r8,%rax,8), %r13
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L290:
	movslq	%r8d, %rax
	movq	-80(%rbp), %rcx
	negl	%r9d
	movq	%rax, -96(%rbp)
	salq	$2, %rax
	movq	%rax, -104(%rbp)
	addq	%rax, %rcx
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L295:
	movl	%r12d, %r10d
	movl	%edx, %eax
	andl	%edx, %r10d
	lock cmpxchgl	%r10d, (%rcx)
	cmpl	%eax, %edx
	je	.L129
.L130:
	movl	(%rcx), %edx
	testl	%edx, %r9d
	jne	.L295
.L129:
	cmpl	%ebx, %r11d
	jge	.L141
	cmpl	$32, %r14d
	je	.L134
	movq	-80(%rbp), %rcx
	movq	-104(%rbp), %rax
	movl	$30, %edx
	subl	%r8d, %edx
	addq	-96(%rbp), %rdx
	leaq	4(%rcx,%rax), %rax
	leaq	8(%rcx,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L135:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rdx, %rax
	jne	.L135
.L134:
	leal	1(%r11), %eax
	cmpl	%eax, %ebx
	jle	.L296
	movq	-72(%rbp), %rax
	leaq	8(%r15,%rax), %rdx
	movl	%ebx, %eax
	subl	%r11d, %eax
	subl	$2, %eax
	addq	-88(%rbp), %rax
	leaq	16(%r15,%rax,8), %r8
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L139:
	addq	$8, %rdx
	cmpq	%rdx, %r8
	je	.L297
.L138:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L139
	leaq	128(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L140:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rcx
	jne	.L140
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L283:
	xorl	%r11d, %r11d
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L298:
	movl	%ecx, %eax
	lock cmpxchgl	%r11d, (%rdx)
	cmpl	%eax, %ecx
	je	.L176
.L177:
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L298
.L176:
	leaq	4(%rdx), %rax
	subq	$-128, %rdx
	testl	%r10d, %r10d
	jle	.L275
	.p2align 4,,10
	.p2align 3
.L178:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L178
.L193:
	cmpl	$1, %r10d
	jle	.L299
	leal	-2(%r10), %eax
	leaq	8(%r8), %rdx
	leaq	16(%r8,%rax,8), %r11
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L184:
	addq	$8, %rdx
	cmpq	%rdx, %r11
	je	.L300
.L183:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L184
	leaq	128(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L185:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rcx, %rax
	jne	.L185
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L297:
	cmpl	$32, %ebx
	movslq	%ebx, %rax
	sete	-56(%rbp)
	leaq	(%r15,%rax,8), %r13
	xorl	%r14d, %r14d
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L300:
	movslq	%r10d, %rax
	cmpl	$32, %r10d
	leaq	(%r8,%rax,8), %r8
	sete	%al
	xorl	%r10d, %r10d
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L159:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L285:
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L101
	orl	%r12d, %esi
	movslq	%r11d, %r11
	movl	%esi, %r8d
	leaq	(%rax,%r11,4), %rcx
	notl	%r8d
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L301:
	movl	%esi, %edi
	movl	%edx, %eax
	andl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	je	.L101
.L106:
	movl	(%rcx), %edx
	testl	%edx, %r8d
	jne	.L301
	jmp	.L101
.L289:
	testq	%rax, %rax
	je	.L101
	orl	%r12d, %esi
	movslq	%r8d, %r8
	movl	%esi, %r9d
	leaq	(%rax,%r8,4), %rcx
	notl	%r9d
	jmp	.L127
.L302:
	movl	%esi, %edi
	movl	%edx, %eax
	andl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	je	.L101
.L127:
	movl	(%rcx), %edx
	testl	%edx, %r9d
	jne	.L302
	jmp	.L101
.L282:
	movq	(%r8), %rcx
	testq	%rcx, %rcx
	je	.L101
	subl	$1, %esi
	jmp	.L174
.L303:
	movl	%r9d, %edi
	movl	%edx, %eax
	andl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	je	.L101
.L174:
	movl	(%rcx), %edx
	testl	%esi, %edx
	jne	.L303
	jmp	.L101
.L293:
	leaq	8(%r8,%r15), %r13
	movl	%r11d, %edi
	xorl	%r14d, %r14d
	jmp	.L117
.L296:
	movq	-72(%rbp), %rbx
	cmpl	$32, %eax
	sete	-56(%rbp)
	xorl	%r14d, %r14d
	leaq	8(%r15,%rbx), %r13
	jmp	.L141
.L299:
	addq	$8, %r8
	xorl	%eax, %eax
	xorl	%r10d, %r10d
	jmp	.L180
	.cfi_endproc
.LFE29302:
	.size	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE11RemoveRangeEPNS0_11MemoryChunkEmmNS0_7SlotSet15EmptyBucketModeE.constprop.0, .-_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE11RemoveRangeEPNS0_11MemoryChunkEmmNS0_7SlotSet15EmptyBucketModeE.constprop.0
	.section	.text._ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE0EE11RemoveRangeEPNS0_11MemoryChunkEmmNS0_7SlotSet15EmptyBucketModeE.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE0EE11RemoveRangeEPNS0_11MemoryChunkEmmNS0_7SlotSet15EmptyBucketModeE.constprop.0, @function
_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE0EE11RemoveRangeEPNS0_11MemoryChunkEmmNS0_7SlotSet15EmptyBucketModeE.constprop.0:
.LFB29303:
	.cfi_startproc
	movq	104(%rdi), %r8
	testq	%r8, %r8
	jne	.L479
	ret
	.p2align 4,,10
	.p2align 3
.L479:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	subq	%rdi, %rdx
	subq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	$262143, %rdx
	jbe	.L480
	leaq	-1(%rdx), %rdi
	movq	%rcx, %r9
	andl	$262143, %ecx
	shrq	$18, %rdi
	shrq	$18, %r9
	movslq	%edi, %rax
	movslq	%r9d, %r10
	movq	%rax, -56(%rbp)
	movl	%edi, %eax
	sall	$18, %eax
	subl	%eax, %edx
	leaq	(%r10,%r10,2), %rax
	salq	$7, %rax
	leaq	(%r8,%rax), %r15
	cmpl	%edi, %r9d
	je	.L481
	movl	%ecx, %r13d
	sarl	$13, %r13d
	movslq	%r13d, %r12
	leaq	0(,%r12,8), %rax
	movq	%rax, -64(%rbp)
	movq	(%r15,%r12,8), %r14
	testq	%r14, %r14
	jne	.L349
.L354:
	cmpl	$31, %r13d
	je	.L350
	movq	-64(%rbp), %rax
	leaq	8(%r15,%rax), %rcx
	movl	$30, %eax
	subl	%r13d, %eax
	addq	%rax, %r12
	leaq	16(%r15,%r12,8), %r11
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L482:
	addq	$8, %rcx
	cmpq	%r11, %rcx
	je	.L350
.L359:
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L482
	leaq	128(%rax), %rsi
	.p2align 4,,10
	.p2align 3
.L361:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rsi
	jne	.L361
	addq	$8, %rcx
	cmpq	%r11, %rcx
	jne	.L359
.L350:
	movq	256(%r15), %rax
	leal	1(%r9), %eax
	cmpl	%eax, %edi
	jle	.L356
	cltq
	subl	%r9d, %edi
	leaq	(%rax,%rax,2), %rax
	salq	$7, %rax
	leaq	256(%r8,%rax), %r11
	leal	-2(%rdi), %eax
	xorl	%edi, %edi
	addq	%rax, %r10
	leaq	(%r10,%r10,2), %rax
	salq	$7, %rax
	leaq	1024(%r8,%rax), %r9
	.p2align 4,,10
	.p2align 3
.L375:
	movq	-256(%r11), %rcx
	testq	%rcx, %rcx
	jne	.L368
.L370:
	leaq	-248(%r11), %rcx
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L484:
	addq	$8, %rcx
	cmpq	%r11, %rcx
	je	.L483
.L365:
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L484
	leaq	128(%rax), %rsi
	.p2align 4,,10
	.p2align 3
.L374:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rsi
	jne	.L374
	addq	$8, %rcx
	cmpq	%r11, %rcx
	jne	.L365
.L483:
	movq	(%r11), %rax
	addq	$384, %r11
	cmpq	%r11, %r9
	jne	.L375
.L356:
	movq	-56(%rbp), %rax
	leaq	(%rax,%rax,2), %rax
	salq	$7, %rax
	addq	%rax, %r8
	cmpl	$262144, %edx
	jg	.L362
	movl	%edx, %r10d
	movl	%edx, %edi
	sarl	$3, %edx
	movl	$1, %esi
	sarl	$13, %r10d
	movl	%edx, %ecx
	sarl	$8, %edi
	sall	%cl, %esi
	andl	$31, %edi
	movl	%r10d, %eax
	movl	%esi, %r9d
	negl	%r9d
	orl	%edi, %eax
	je	.L485
	movq	(%r8), %rdx
	testq	%rdx, %rdx
	jne	.L486
	testl	%r10d, %r10d
	jg	.L396
.L478:
	xorl	%eax, %eax
	movl	$1, %r10d
.L383:
	movq	(%r8), %rcx
	testq	%rcx, %rcx
	je	.L304
	testb	%al, %al
	jne	.L304
	cmpl	%edi, %r10d
	jge	.L391
	leal	-1(%rdi), %edx
	movslq	%r10d, %r8
	subl	%r10d, %edx
	leaq	(%rcx,%r8,4), %rax
	addq	%r8, %rdx
	leaq	4(%rcx,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L392:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L392
.L391:
	movslq	%edi, %rdi
	subl	$1, %esi
	leaq	(%rcx,%rdi,4), %rcx
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L487:
	movl	%r9d, %edi
	movl	%edx, %eax
	andl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	je	.L304
.L390:
	movl	(%rcx), %edx
	testl	%edx, %esi
	jne	.L487
.L304:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L480:
	.cfi_restore_state
	movl	%ecx, %edi
	movl	$1, %esi
	movl	%ecx, %r11d
	movl	%edx, %ebx
	sarl	$3, %ecx
	movl	%edx, %r9d
	movl	%esi, %eax
	sarl	$3, %edx
	sarl	$13, %edi
	sall	%cl, %eax
	movl	%edx, %ecx
	sall	%cl, %esi
	movslq	%edi, %rdx
	sarl	$8, %r11d
	leal	-1(%rax), %r12d
	sarl	$8, %r9d
	sarl	$13, %ebx
	movl	%esi, -56(%rbp)
	andl	$31, %r11d
	leaq	0(,%rdx,8), %r15
	movq	%rdx, -64(%rbp)
	andl	$31, %r9d
	negl	%esi
	leaq	(%r8,%r15), %r13
	cmpl	%ebx, %edi
	jne	.L307
	cmpl	%r9d, %r11d
	je	.L488
.L307:
	movq	0(%r13), %rdx
	leal	1(%r11), %r14d
	movq	%rdx, -72(%rbp)
	testq	%rdx, %rdx
	jne	.L489
	cmpl	%ebx, %edi
	jl	.L316
.L320:
	movq	0(%r13), %rcx
	cmpl	$32, %edi
	je	.L304
	testq	%rcx, %rcx
	je	.L304
	cmpl	%r14d, %r9d
	jle	.L325
	leal	-1(%r9), %edx
	movslq	%r14d, %rdi
	subl	%r14d, %edx
	leaq	(%rcx,%rdi,4), %rax
	addq	%rdi, %rdx
	leaq	4(%rcx,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L326:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L326
.L325:
	movl	-56(%rbp), %r8d
	movslq	%r9d, %r9
	leaq	(%rcx,%r9,4), %rcx
	subl	$1, %r8d
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L490:
	movl	%esi, %edi
	movl	%edx, %eax
	andl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	je	.L304
.L324:
	movl	(%rcx), %edx
	testl	%edx, %r8d
	jne	.L490
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L349:
	movl	%ecx, %eax
	sarl	$3, %ecx
	movq	%r8, -80(%rbp)
	sarl	$8, %eax
	movl	%eax, %ebx
	movl	%eax, %esi
	andl	$31, %ebx
	andl	$31, %esi
	movl	%ebx, -72(%rbp)
	movl	$1, %ebx
	leaq	0(,%rsi,4), %rax
	sall	%cl, %ebx
	movq	%rsi, -88(%rbp)
	leaq	(%r14,%rax), %rsi
	leal	-1(%rbx), %r11d
	movq	%rax, -96(%rbp)
	negl	%ebx
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L491:
	movl	%r11d, %r8d
	movl	%ecx, %eax
	andl	%ecx, %r8d
	lock cmpxchgl	%r8d, (%rsi)
	cmpl	%eax, %ecx
	je	.L471
.L353:
	movl	(%rsi), %ecx
	testl	%ecx, %ebx
	jne	.L491
.L471:
	cmpl	$31, -72(%rbp)
	movq	-80(%rbp), %r8
	je	.L354
	movq	-96(%rbp), %rax
	movl	$30, %ecx
	subl	-72(%rbp), %ecx
	addq	-88(%rbp), %rcx
	leaq	4(%r14,%rax), %rax
	leaq	8(%r14,%rcx,4), %rcx
	.p2align 4,,10
	.p2align 3
.L355:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rcx, %rax
	jne	.L355
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L366:
	movl	%esi, %eax
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %esi
	je	.L369
.L368:
	movl	(%rcx), %esi
	testl	%esi, %esi
	jne	.L366
.L369:
	leaq	4(%rcx), %rax
	subq	$-128, %rcx
	.p2align 4,,10
	.p2align 3
.L367:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rcx, %rax
	jne	.L367
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L481:
	cmpl	$262144, %edx
	jg	.L362
	movl	$1, %eax
	movl	%ecx, %r11d
	movl	%ecx, %r8d
	movl	%edx, %ebx
	sarl	$3, %ecx
	movl	%edx, %edi
	movl	%eax, %r9d
	sarl	$3, %edx
	sall	%cl, %r9d
	movl	%edx, %ecx
	sarl	$8, %r8d
	sall	%cl, %eax
	sarl	$8, %edi
	andl	$31, %r8d
	leal	-1(%r9), %r12d
	movl	%eax, %esi
	andl	$31, %edi
	sarl	$13, %r11d
	movl	%eax, -64(%rbp)
	sarl	$13, %ebx
	negl	%esi
	cmpl	%edi, %r8d
	movslq	%r11d, %rcx
	sete	%dl
	cmpl	%ebx, %r11d
	movq	%rcx, -88(%rbp)
	sete	%al
	salq	$3, %rcx
	leaq	(%r15,%rcx), %r13
	andb	%al, %dl
	movq	%rcx, -72(%rbp)
	movb	%dl, -56(%rbp)
	movq	0(%r13), %rax
	jne	.L492
	movq	%rax, -80(%rbp)
	leal	1(%r8), %r14d
	testq	%rax, %rax
	jne	.L493
	cmpl	%ebx, %r11d
	jl	.L337
.L344:
	movq	0(%r13), %rcx
	testq	%rcx, %rcx
	je	.L304
	cmpb	$0, -56(%rbp)
	jne	.L304
	cmpl	%r14d, %edi
	jle	.L347
	leal	-1(%rdi), %edx
	movslq	%r14d, %r8
	subl	%r14d, %edx
	leaq	(%rcx,%r8,4), %rax
	addq	%r8, %rdx
	leaq	4(%rcx,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L348:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L348
.L347:
	movl	-64(%rbp), %r8d
	movslq	%edi, %rdi
	leaq	(%rcx,%rdi,4), %rcx
	subl	$1, %r8d
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L494:
	movl	%esi, %edi
	movl	%edx, %eax
	andl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	je	.L304
.L346:
	movl	(%rcx), %edx
	testl	%edx, %r8d
	jne	.L494
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L489:
	negl	%eax
	movq	-72(%rbp), %rcx
	movq	%r8, -80(%rbp)
	movl	%eax, %r10d
	movslq	%r11d, %rax
	movq	%rax, -88(%rbp)
	salq	$2, %rax
	movq	%rax, -96(%rbp)
	addq	%rax, %rcx
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L495:
	movl	%r12d, %r8d
	movl	%edx, %eax
	andl	%edx, %r8d
	lock cmpxchgl	%r8d, (%rcx)
	cmpl	%eax, %edx
	je	.L469
.L312:
	movl	(%rcx), %edx
	testl	%edx, %r10d
	jne	.L495
.L469:
	movq	-80(%rbp), %r8
	cmpl	%ebx, %edi
	jge	.L320
	cmpl	$32, %r14d
	je	.L316
	movq	-72(%rbp), %rcx
	movq	-96(%rbp), %rax
	movl	$30, %edx
	subl	%r11d, %edx
	addq	-88(%rbp), %rdx
	leaq	4(%rcx,%rax), %rax
	leaq	8(%rcx,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L317:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L317
.L316:
	leal	1(%rdi), %r11d
	cmpl	%r11d, %ebx
	jle	.L496
	leal	-2(%rbx), %eax
	leaq	8(%r8,%r15), %rdx
	subl	%edi, %eax
	addq	-64(%rbp), %rax
	leaq	16(%r8,%rax,8), %r10
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L321:
	addq	$8, %rdx
	cmpq	%rdx, %r10
	je	.L497
.L319:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L321
	leaq	128(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L322:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rcx
	jne	.L322
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L497:
	leal	-1(%rbx,%r11), %eax
	xorl	%r14d, %r14d
	subl	%edi, %eax
	movl	%eax, %edi
	cltq
	leaq	(%r8,%rax,8), %r13
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L493:
	movslq	%r8d, %rax
	movq	-80(%rbp), %rcx
	negl	%r9d
	movq	%rax, -96(%rbp)
	salq	$2, %rax
	movq	%rax, -104(%rbp)
	addq	%rax, %rcx
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L498:
	movl	%r12d, %r10d
	movl	%edx, %eax
	andl	%edx, %r10d
	lock cmpxchgl	%r10d, (%rcx)
	cmpl	%eax, %edx
	je	.L332
.L333:
	movl	(%rcx), %edx
	testl	%edx, %r9d
	jne	.L498
.L332:
	cmpl	%ebx, %r11d
	jge	.L344
	cmpl	$32, %r14d
	je	.L337
	movq	-80(%rbp), %rcx
	movq	-104(%rbp), %rax
	movl	$30, %edx
	subl	%r8d, %edx
	addq	-96(%rbp), %rdx
	leaq	4(%rcx,%rax), %rax
	leaq	8(%rcx,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L338:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rdx, %rax
	jne	.L338
.L337:
	leal	1(%r11), %eax
	cmpl	%eax, %ebx
	jle	.L499
	movq	-72(%rbp), %rax
	leaq	8(%r15,%rax), %rdx
	movl	%ebx, %eax
	subl	%r11d, %eax
	subl	$2, %eax
	addq	-88(%rbp), %rax
	leaq	16(%r15,%rax,8), %r8
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L342:
	addq	$8, %rdx
	cmpq	%rdx, %r8
	je	.L500
.L341:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L342
	leaq	128(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L343:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rcx
	jne	.L343
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L486:
	xorl	%r11d, %r11d
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L501:
	movl	%ecx, %eax
	lock cmpxchgl	%r11d, (%rdx)
	cmpl	%eax, %ecx
	je	.L379
.L380:
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L501
.L379:
	leaq	4(%rdx), %rax
	subq	$-128, %rdx
	testl	%r10d, %r10d
	jle	.L478
	.p2align 4,,10
	.p2align 3
.L381:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L381
.L396:
	cmpl	$1, %r10d
	jle	.L502
	leal	-2(%r10), %eax
	leaq	8(%r8), %rdx
	leaq	16(%r8,%rax,8), %r11
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L387:
	addq	$8, %rdx
	cmpq	%rdx, %r11
	je	.L503
.L386:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L387
	leaq	128(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L388:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rcx, %rax
	jne	.L388
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L500:
	cmpl	$32, %ebx
	movslq	%ebx, %rax
	sete	-56(%rbp)
	leaq	(%r15,%rax,8), %r13
	xorl	%r14d, %r14d
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L503:
	movslq	%r10d, %rax
	cmpl	$32, %r10d
	leaq	(%r8,%rax,8), %r8
	sete	%al
	xorl	%r10d, %r10d
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L362:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L488:
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L304
	orl	%r12d, %esi
	movslq	%r11d, %r11
	movl	%esi, %r8d
	leaq	(%rax,%r11,4), %rcx
	notl	%r8d
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L504:
	movl	%esi, %edi
	movl	%edx, %eax
	andl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	je	.L304
.L309:
	movl	(%rcx), %edx
	testl	%edx, %r8d
	jne	.L504
	jmp	.L304
.L492:
	testq	%rax, %rax
	je	.L304
	orl	%r12d, %esi
	movslq	%r8d, %r8
	movl	%esi, %r9d
	leaq	(%rax,%r8,4), %rcx
	notl	%r9d
	jmp	.L330
.L505:
	movl	%esi, %edi
	movl	%edx, %eax
	andl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	je	.L304
.L330:
	movl	(%rcx), %edx
	testl	%edx, %r9d
	jne	.L505
	jmp	.L304
.L485:
	movq	(%r8), %rcx
	testq	%rcx, %rcx
	je	.L304
	subl	$1, %esi
	jmp	.L377
.L506:
	movl	%r9d, %edi
	movl	%edx, %eax
	andl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	je	.L304
.L377:
	movl	(%rcx), %edx
	testl	%esi, %edx
	jne	.L506
	jmp	.L304
.L496:
	leaq	8(%r8,%r15), %r13
	movl	%r11d, %edi
	xorl	%r14d, %r14d
	jmp	.L320
.L499:
	movq	-72(%rbp), %rbx
	cmpl	$32, %eax
	sete	-56(%rbp)
	xorl	%r14d, %r14d
	leaq	8(%r15,%rbx), %r13
	jmp	.L344
.L502:
	addq	$8, %r8
	xorl	%eax, %eax
	xorl	%r10d, %r10d
	jmp	.L383
	.cfi_endproc
.LFE29303:
	.size	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE0EE11RemoveRangeEPNS0_11MemoryChunkEmmNS0_7SlotSet15EmptyBucketModeE.constprop.0, .-_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE0EE11RemoveRangeEPNS0_11MemoryChunkEmmNS0_7SlotSet15EmptyBucketModeE.constprop.0
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8947:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L513
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L516
.L507:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L513:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L516:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L507
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8947:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.text._ZN2v88internal7SweeperC2EPNS0_4HeapEPNS0_26MajorNonAtomicMarkingStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7SweeperC2EPNS0_4HeapEPNS0_26MajorNonAtomicMarkingStateE
	.type	_ZN2v88internal7SweeperC2EPNS0_4HeapEPNS0_26MajorNonAtomicMarkingStateE, @function
_ZN2v88internal7SweeperC2EPNS0_4HeapEPNS0_26MajorNonAtomicMarkingStateE:
.LFB22319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	xorl	%esi, %esi
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	48(%rdi), %rdi
	subq	$8, %rsp
	movl	$0, -32(%rdi)
	movups	%xmm0, -48(%rdi)
	call	_ZN2v84base9SemaphoreC1Ei@PLT
	leaq	80(%rbx), %rdi
	call	_ZN2v84base5MutexC1Ev@PLT
	xorl	%eax, %eax
	movb	$0, 280(%rbx)
	xorl	%esi, %esi
	movq	$0, 120(%rbx)
	pxor	%xmm0, %xmm0
	leaq	320(%rbx), %rdi
	movq	$0, 128(%rbx)
	movq	$0, 136(%rbx)
	movq	$0, 144(%rbx)
	movq	$0, 152(%rbx)
	movq	$0, 160(%rbx)
	movq	$0, 168(%rbx)
	movq	$0, 176(%rbx)
	movq	$0, 184(%rbx)
	movq	$0, 192(%rbx)
	movq	$0, 200(%rbx)
	movq	$0, 208(%rbx)
	movq	$0, 216(%rbx)
	movq	$0, 224(%rbx)
	movq	$0, 232(%rbx)
	movq	$0, 240(%rbx)
	movq	$0, 248(%rbx)
	movq	$0, 256(%rbx)
	movw	%ax, 264(%rbx)
	movq	$0, 272(%rbx)
	movq	$0, 304(%rbx)
	movups	%xmm0, 288(%rbx)
	call	_ZN2v84base9SemaphoreC1Ei@PLT
	xorl	%edx, %edx
	movb	$0, 354(%rbx)
	movw	%dx, 352(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22319:
	.size	_ZN2v88internal7SweeperC2EPNS0_4HeapEPNS0_26MajorNonAtomicMarkingStateE, .-_ZN2v88internal7SweeperC2EPNS0_4HeapEPNS0_26MajorNonAtomicMarkingStateE
	.globl	_ZN2v88internal7SweeperC1EPNS0_4HeapEPNS0_26MajorNonAtomicMarkingStateE
	.set	_ZN2v88internal7SweeperC1EPNS0_4HeapEPNS0_26MajorNonAtomicMarkingStateE,_ZN2v88internal7SweeperC2EPNS0_4HeapEPNS0_26MajorNonAtomicMarkingStateE
	.section	.text._ZN2v88internal7Sweeper20PauseOrCompleteScopeC2EPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Sweeper20PauseOrCompleteScopeC2EPS1_
	.type	_ZN2v88internal7Sweeper20PauseOrCompleteScopeC2EPS1_, @function
_ZN2v88internal7Sweeper20PauseOrCompleteScopeC2EPS1_:
.LFB22322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, (%rdi)
	movb	$1, 280(%rsi)
	mfence
	movq	(%rdi), %rbx
	cmpb	$0, 265(%rbx)
	je	.L519
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_sweepingE(%rip)
	je	.L522
	movl	16(%rbx), %eax
	movq	%rdi, %r12
	xorl	%r13d, %r13d
	leaq	48(%rbx), %r14
	testl	%eax, %eax
	jle	.L525
.L527:
	movq	(%rbx), %rax
	movq	24(%rbx,%r13,8), %rsi
	movq	8048(%rax), %rdi
	call	_ZN2v88internal21CancelableTaskManager8TryAbortEm@PLT
	cmpl	$2, %eax
	je	.L524
	movq	%r14, %rdi
	addq	$1, %r13
	call	_ZN2v84base9Semaphore4WaitEv@PLT
	cmpl	%r13d, 16(%rbx)
	jg	.L527
.L525:
	movl	$0, 16(%rbx)
	movq	(%r12), %rbx
.L522:
	movq	216(%rbx), %rax
	cmpq	%rax, 224(%rbx)
	je	.L537
.L519:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L524:
	.cfi_restore_state
	lock subq	$1, 272(%rbx)
	addq	$1, %r13
	cmpl	%r13d, 16(%rbx)
	jg	.L527
	movl	$0, 16(%rbx)
	movq	(%r12), %rbx
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L537:
	movq	192(%rbx), %rax
	cmpq	%rax, 200(%rbx)
	jne	.L519
	movq	240(%rbx), %rax
	cmpq	%rax, 248(%rbx)
	jne	.L519
	movq	(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	movq	2016(%rax), %rdi
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal20MarkCompactCollector23EnsureSweepingCompletedEv@PLT
	.cfi_endproc
.LFE22322:
	.size	_ZN2v88internal7Sweeper20PauseOrCompleteScopeC2EPS1_, .-_ZN2v88internal7Sweeper20PauseOrCompleteScopeC2EPS1_
	.globl	_ZN2v88internal7Sweeper20PauseOrCompleteScopeC1EPS1_
	.set	_ZN2v88internal7Sweeper20PauseOrCompleteScopeC1EPS1_,_ZN2v88internal7Sweeper20PauseOrCompleteScopeC2EPS1_
	.section	.text._ZN2v88internal7Sweeper20PauseOrCompleteScopeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Sweeper20PauseOrCompleteScopeD2Ev
	.type	_ZN2v88internal7Sweeper20PauseOrCompleteScopeD2Ev, @function
_ZN2v88internal7Sweeper20PauseOrCompleteScopeD2Ev:
.LFB22325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movb	$0, 280(%rax)
	mfence
	movq	(%rdi), %r13
	cmpb	$0, 265(%r13)
	jne	.L557
.L538:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L558
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L557:
	.cfi_restore_state
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_sweepingE(%rip)
	je	.L538
	movq	0(%r13), %rax
	cmpb	$0, 2871(%rax)
	jne	.L538
	leaq	272(%r13), %rbx
	lock addq	$1, (%rbx)
	movl	$80, %edi
	leaq	16+_ZTVN2v88internal7Sweeper11SweeperTaskE(%rip), %r15
	leaq	48(%r15), %r14
	movq	0(%r13), %rsi
	leaq	-37592(%rsi), %rdx
	movq	%rdx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rax, %r12
	movq	%rdx, %rsi
	addq	$32, %r12
	call	_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rdx
	leaq	48(%r13), %rax
	movq	%r15, -32(%r12)
	movq	%rax, 16(%r12)
	movq	39600(%rdx), %rax
	movq	%r14, (%r12)
	movq	%r13, 8(%r12)
	movq	%rbx, 24(%r12)
	movl	$2, 32(%r12)
	movq	%rax, 40(%r12)
	movslq	16(%r13), %rax
	leal	1(%rax), %edx
	movl	%edx, 16(%r13)
	movq	-8(%r12), %rdx
	movq	%rdx, 24(%r13,%rax,8)
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	56(%rax), %rax
	movq	%r12, -64(%rbp)
	leaq	-64(%rbp), %r12
	movq	%r12, %rsi
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L543
	movq	(%rdi), %rax
	call	*8(%rax)
.L543:
	lock addq	$1, (%rbx)
	movl	$80, %edi
	movq	0(%r13), %rax
	leaq	-37592(%rax), %rcx
	movq	%rcx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rcx
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	movq	%rcx, %rsi
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rcx
	leaq	48(%r13), %rax
	movq	%rax, 48(%rdx)
	movq	39600(%rcx), %rax
	movq	%r15, (%rdx)
	movq	%r14, 32(%rdx)
	movq	%r13, 40(%rdx)
	movq	%rbx, 56(%rdx)
	movl	$3, 64(%rdx)
	movq	%rax, 72(%rdx)
	movslq	16(%r13), %rax
	leal	1(%rax), %ecx
	movl	%ecx, 16(%r13)
	movq	24(%rdx), %rcx
	movq	%rcx, 24(%r13,%rax,8)
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	-72(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	addq	$32, %rdx
	movq	56(%rax), %rax
	movq	%rdx, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L544
	movq	(%rdi), %rax
	call	*8(%rax)
.L544:
	lock addq	$1, (%rbx)
	movl	$80, %edi
	movq	0(%r13), %rax
	leaq	-37592(%rax), %rcx
	movq	%rcx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rcx
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	movq	%rcx, %rsi
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rcx
	leaq	48(%r13), %rax
	movq	%rax, 48(%rdx)
	movq	39600(%rcx), %rax
	movq	%r15, (%rdx)
	movq	%r14, 32(%rdx)
	movq	%r13, 40(%rdx)
	movq	%rbx, 56(%rdx)
	movl	$4, 64(%rdx)
	movq	%rax, 72(%rdx)
	movslq	16(%r13), %rax
	leal	1(%rax), %ecx
	movl	%ecx, 16(%r13)
	movq	24(%rdx), %rcx
	movq	%rcx, 24(%r13,%rax,8)
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	-72(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	addq	$32, %rdx
	movq	56(%rax), %rax
	movq	%rdx, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L545
	movq	(%rdi), %rax
	call	*8(%rax)
.L545:
	cmpb	$0, 264(%r13)
	jne	.L538
	movq	%r13, %rdi
	call	_ZN2v88internal7Sweeper31ScheduleIncrementalSweepingTaskEv.part.0
	jmp	.L538
.L558:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22325:
	.size	_ZN2v88internal7Sweeper20PauseOrCompleteScopeD2Ev, .-_ZN2v88internal7Sweeper20PauseOrCompleteScopeD2Ev
	.globl	_ZN2v88internal7Sweeper20PauseOrCompleteScopeD1Ev
	.set	_ZN2v88internal7Sweeper20PauseOrCompleteScopeD1Ev,_ZN2v88internal7Sweeper20PauseOrCompleteScopeD2Ev
	.section	.text._ZN2v88internal7Sweeper24FilterSweepingPagesScopeC2EPS1_RKNS1_20PauseOrCompleteScopeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Sweeper24FilterSweepingPagesScopeC2EPS1_RKNS1_20PauseOrCompleteScopeE
	.type	_ZN2v88internal7Sweeper24FilterSweepingPagesScopeC2EPS1_RKNS1_20PauseOrCompleteScopeE, @function
_ZN2v88internal7Sweeper24FilterSweepingPagesScopeC2EPS1_RKNS1_20PauseOrCompleteScopeE:
.LFB22331:
	.cfi_startproc
	endbr64
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movzbl	265(%rsi), %eax
	movq	%rsi, (%rdi)
	movq	%rdx, 32(%rdi)
	movb	%al, 40(%rdi)
	testb	%al, %al
	je	.L559
	movq	208(%rsi), %rax
	movdqu	192(%rsi), %xmm1
	pxor	%xmm0, %xmm0
	movq	$0, 208(%rsi)
	movups	%xmm0, 192(%rsi)
	movq	%rax, 24(%rdi)
	movups	%xmm1, 8(%rdi)
.L559:
	ret
	.cfi_endproc
.LFE22331:
	.size	_ZN2v88internal7Sweeper24FilterSweepingPagesScopeC2EPS1_RKNS1_20PauseOrCompleteScopeE, .-_ZN2v88internal7Sweeper24FilterSweepingPagesScopeC2EPS1_RKNS1_20PauseOrCompleteScopeE
	.globl	_ZN2v88internal7Sweeper24FilterSweepingPagesScopeC1EPS1_RKNS1_20PauseOrCompleteScopeE
	.set	_ZN2v88internal7Sweeper24FilterSweepingPagesScopeC1EPS1_RKNS1_20PauseOrCompleteScopeE,_ZN2v88internal7Sweeper24FilterSweepingPagesScopeC2EPS1_RKNS1_20PauseOrCompleteScopeE
	.section	.text._ZN2v88internal7Sweeper24FilterSweepingPagesScopeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Sweeper24FilterSweepingPagesScopeD2Ev
	.type	_ZN2v88internal7Sweeper24FilterSweepingPagesScopeD2Ev, @function
_ZN2v88internal7Sweeper24FilterSweepingPagesScopeD2Ev:
.LFB22334:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 40(%rbx)
	movq	8(%rdi), %rdi
	je	.L578
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	192(%rax), %r8
	movq	%rdi, 192(%rax)
	movq	16(%rbx), %rdx
	movq	%rdx, 200(%rax)
	movq	24(%rbx), %rdx
	movq	%rdx, 208(%rax)
	movq	$0, 24(%rbx)
	movups	%xmm0, 8(%rbx)
	testq	%r8, %r8
	je	.L564
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	movq	8(%rbx), %rdi
.L578:
	testq	%rdi, %rdi
	je	.L564
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L564:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22334:
	.size	_ZN2v88internal7Sweeper24FilterSweepingPagesScopeD2Ev, .-_ZN2v88internal7Sweeper24FilterSweepingPagesScopeD2Ev
	.globl	_ZN2v88internal7Sweeper24FilterSweepingPagesScopeD1Ev
	.set	_ZN2v88internal7Sweeper24FilterSweepingPagesScopeD1Ev,_ZN2v88internal7Sweeper24FilterSweepingPagesScopeD2Ev
	.section	.rodata._ZN2v88internal7Sweeper13StartSweepingEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	"!stop_sweeper_tasks_"
	.section	.text._ZN2v88internal7Sweeper13StartSweepingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Sweeper13StartSweepingEv
	.type	_ZN2v88internal7Sweeper13StartSweepingEv, @function
_ZN2v88internal7Sweeper13StartSweepingEv:
.LFB22352:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	280(%rdi), %eax
	testb	%al, %al
	jne	.L644
	movq	(%rdi), %rax
	movb	$1, 265(%rdi)
	movq	%rdi, %r13
	movb	$1, 352(%rdi)
	movq	192(%rdi), %r12
	movl	2756(%rax), %edx
	movb	%dl, 354(%rdi)
	andb	$1, 354(%rdi)
	movq	2016(%rax), %r14
	leaq	9993(%r14), %rcx
	movq	200(%rdi), %r14
	movq	%rcx, -56(%rbp)
	cmpq	%r12, %r14
	je	.L581
	movq	%r14, %r15
	movl	$63, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	subq	%r12, %r15
	leaq	8(%r12), %rbx
	movq	%r15, %rax
	sarq	$3, %rax
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPPN2v88internal4PageESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZZNS3_7Sweeper13StartSweepingEvENKUlNS3_15AllocationSpaceEE_clESE_EUlS5_S5_E_EEEvT_SI_T0_T1_
	cmpq	$128, %r15
	jle	.L643
	leaq	128(%r12), %r15
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L646:
	cmpq	%rbx, %r12
	je	.L584
	movl	$8, %eax
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r9, -64(%rbp)
	subq	%r12, %rdx
	leaq	(%r12,%rax), %rdi
	call	memmove@PLT
	movq	-64(%rbp), %r9
.L584:
	addq	$8, %rbx
	movq	%r9, (%r12)
	cmpq	%rbx, %r15
	je	.L645
.L588:
	movq	(%rbx), %r9
	movq	(%r12), %rax
	movq	96(%r9), %rsi
	cmpq	96(%rax), %rsi
	jg	.L646
	movq	-8(%rbx), %rdx
	leaq	-8(%rbx), %rax
	cmpq	96(%rdx), %rsi
	jle	.L624
	.p2align 4,,10
	.p2align 3
.L587:
	movq	%rdx, 8(%rax)
	movq	%rax, %rsi
	movq	-8(%rax), %rdx
	subq	$8, %rax
	movq	96(%rdx), %rcx
	cmpq	%rcx, 96(%r9)
	jg	.L587
.L586:
	addq	$8, %rbx
	movq	%r9, (%rsi)
	cmpq	%rbx, %r15
	jne	.L588
.L645:
	cmpq	%r15, %r14
	je	.L581
	.p2align 4,,10
	.p2align 3
.L589:
	movq	-8(%r15), %rdx
	movq	(%r15), %rsi
	leaq	-8(%r15), %rax
	movq	96(%rdx), %rcx
	cmpq	%rcx, 96(%rsi)
	jle	.L625
	.p2align 4,,10
	.p2align 3
.L592:
	movq	%rdx, 8(%rax)
	movq	%rax, %rdi
	movq	-8(%rax), %rdx
	subq	$8, %rax
	movq	96(%rdx), %rcx
	cmpq	%rcx, 96(%rsi)
	jg	.L592
	addq	$8, %r15
	movq	%rsi, (%rdi)
	cmpq	%r15, %r14
	jne	.L589
.L581:
	movq	224(%r13), %r15
	movq	216(%r13), %r13
	cmpq	%r13, %r15
	je	.L579
	movq	%r15, %r12
	movl	$63, %edx
	movq	-56(%rbp), %rcx
	movq	%r15, %rsi
	subq	%r13, %r12
	movq	%r13, %rdi
	leaq	8(%r13), %rbx
	movq	%r12, %rax
	sarq	$3, %rax
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPPN2v88internal4PageESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZZNS3_7Sweeper13StartSweepingEvENKUlNS3_15AllocationSpaceEE_clESE_EUlS5_S5_E_EEEvT_SI_T0_T1_
	cmpq	$128, %r12
	jle	.L603
	leaq	128(%r13), %r12
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L648:
	cmpq	%rbx, %r13
	je	.L605
	movq	%rbx, %rdx
	movl	$8, %eax
	movq	%r13, %rsi
	subq	%r13, %rdx
	leaq	0(%r13,%rax), %rdi
	call	memmove@PLT
.L605:
	addq	$8, %rbx
	movq	%r14, 0(%r13)
	cmpq	%rbx, %r12
	je	.L647
.L609:
	movq	(%rbx), %r14
	movq	0(%r13), %rax
	movq	%rbx, %rsi
	movq	96(%r14), %rdi
	cmpq	96(%rax), %rdi
	jg	.L648
	movq	-8(%rbx), %rdx
	leaq	-8(%rbx), %rax
	cmpq	96(%rdx), %rdi
	jle	.L607
	.p2align 4,,10
	.p2align 3
.L608:
	movq	%rdx, 8(%rax)
	movq	%rax, %rsi
	movq	-8(%rax), %rdx
	subq	$8, %rax
	movq	96(%rdx), %rcx
	cmpq	%rcx, 96(%r14)
	jg	.L608
.L607:
	addq	$8, %rbx
	movq	%r14, (%rsi)
	cmpq	%rbx, %r12
	jne	.L609
.L647:
	movq	%r12, %rax
	cmpq	%r12, %r15
	je	.L579
	.p2align 4,,10
	.p2align 3
.L614:
	movq	(%rax), %rsi
	movq	-8(%rax), %rcx
	movq	%rax, %rdi
	leaq	-8(%rax), %rdx
	movq	96(%rsi), %rbx
	cmpq	%rbx, 96(%rcx)
	jge	.L612
	.p2align 4,,10
	.p2align 3
.L613:
	movq	%rcx, 8(%rdx)
	movq	%rdx, %rdi
	movq	-8(%rdx), %rcx
	subq	$8, %rdx
	movq	96(%rcx), %rbx
	cmpq	%rbx, 96(%rsi)
	jg	.L613
.L612:
	addq	$8, %rax
	movq	%rsi, (%rdi)
	cmpq	%rax, %r15
	jne	.L614
.L579:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L649:
	.cfi_restore_state
	cmpq	%rbx, %r12
	je	.L596
	movq	%rbx, %rdx
	movl	$8, %eax
	movq	%r12, %rsi
	subq	%r12, %rdx
	leaq	(%r12,%rax), %rdi
	call	memmove@PLT
.L596:
	movq	%r15, (%r12)
.L597:
	addq	$8, %rbx
.L643:
	cmpq	%rbx, %r14
	je	.L581
	movq	(%rbx), %r15
	movq	(%r12), %rax
	movq	%rbx, %rsi
	movq	96(%r15), %rdi
	cmpq	96(%rax), %rdi
	jg	.L649
	movq	-8(%rbx), %rdx
	leaq	-8(%rbx), %rax
	cmpq	96(%rdx), %rdi
	jle	.L598
	.p2align 4,,10
	.p2align 3
.L599:
	movq	%rdx, 8(%rax)
	movq	%rax, %rsi
	movq	-8(%rax), %rdx
	subq	$8, %rax
	movq	96(%rdx), %rcx
	cmpq	%rcx, 96(%r15)
	jg	.L599
.L598:
	movq	%r15, (%rsi)
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L603:
	movl	$8, %r12d
	cmpq	%rbx, %r15
	jne	.L622
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L650:
	cmpq	%rbx, %r13
	je	.L618
	movq	%rbx, %rdx
	leaq	0(%r13,%r12), %rdi
	movq	%r13, %rsi
	subq	%r13, %rdx
	call	memmove@PLT
.L618:
	movq	%r14, 0(%r13)
.L619:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L579
.L622:
	movq	(%rbx), %r14
	movq	0(%r13), %rax
	movq	%rbx, %rcx
	movq	96(%r14), %rsi
	cmpq	96(%rax), %rsi
	jg	.L650
	movq	-8(%rbx), %rdx
	leaq	-8(%rbx), %rax
	cmpq	96(%rdx), %rsi
	jle	.L620
	.p2align 4,,10
	.p2align 3
.L621:
	movq	%rdx, 8(%rax)
	movq	%rax, %rcx
	movq	-8(%rax), %rdx
	subq	$8, %rax
	movq	96(%rdx), %rsi
	cmpq	%rsi, 96(%r14)
	jg	.L621
.L620:
	movq	%r14, (%rcx)
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L625:
	movq	%r15, %rdi
	addq	$8, %r15
	movq	%rsi, (%rdi)
	cmpq	%r15, %r14
	jne	.L589
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L644:
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L624:
	movq	%rbx, %rsi
	jmp	.L586
	.cfi_endproc
.LFE22352:
	.size	_ZN2v88internal7Sweeper13StartSweepingEv, .-_ZN2v88internal7Sweeper13StartSweepingEv
	.section	.text._ZN2v88internal7Sweeper17StartSweeperTasksEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Sweeper17StartSweeperTasksEv
	.type	_ZN2v88internal7Sweeper17StartSweeperTasksEv, @function
_ZN2v88internal7Sweeper17StartSweeperTasksEv:
.LFB22355:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_sweepingE(%rip)
	je	.L651
	cmpb	$0, 265(%rdi)
	movq	%rdi, %r13
	jne	.L669
.L651:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L670
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L669:
	.cfi_restore_state
	movq	(%rdi), %rax
	cmpb	$0, 2871(%rax)
	jne	.L651
	leaq	272(%rdi), %rbx
	lock addq	$1, (%rbx)
	leaq	16+_ZTVN2v88internal7Sweeper11SweeperTaskE(%rip), %r15
	leaq	48(%r15), %r14
	movq	(%rdi), %rsi
	movl	$80, %edi
	leaq	-37592(%rsi), %rdx
	movq	%rdx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rax, %r12
	movq	%rdx, %rsi
	addq	$32, %r12
	call	_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rdx
	leaq	48(%r13), %rax
	movq	%r15, -32(%r12)
	movq	%rax, 16(%r12)
	movq	39600(%rdx), %rax
	movq	%r14, (%r12)
	movq	%r13, 8(%r12)
	movq	%rax, 40(%r12)
	movslq	16(%r13), %rax
	movq	%rbx, 24(%r12)
	movl	$2, 32(%r12)
	leal	1(%rax), %edx
	movl	%edx, 16(%r13)
	movq	-8(%r12), %rdx
	movq	%rdx, 24(%r13,%rax,8)
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	56(%rax), %rax
	movq	%r12, -64(%rbp)
	leaq	-64(%rbp), %r12
	movq	%r12, %rsi
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L655
	movq	(%rdi), %rax
	call	*8(%rax)
.L655:
	lock addq	$1, (%rbx)
	movl	$80, %edi
	movq	0(%r13), %rax
	leaq	-37592(%rax), %rcx
	movq	%rcx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rcx
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	movq	%rcx, %rsi
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rcx
	leaq	48(%r13), %rax
	movq	%rax, 48(%rdx)
	movq	39600(%rcx), %rax
	movq	%r15, (%rdx)
	movq	%rax, 72(%rdx)
	movslq	16(%r13), %rax
	movq	%r14, 32(%rdx)
	leal	1(%rax), %ecx
	movq	%r13, 40(%rdx)
	movl	%ecx, 16(%r13)
	movq	24(%rdx), %rcx
	movq	%rbx, 56(%rdx)
	movl	$3, 64(%rdx)
	movq	%rcx, 24(%r13,%rax,8)
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	-72(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	addq	$32, %rdx
	movq	56(%rax), %rax
	movq	%rdx, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L656
	movq	(%rdi), %rax
	call	*8(%rax)
.L656:
	lock addq	$1, (%rbx)
	movl	$80, %edi
	movq	0(%r13), %rax
	leaq	-37592(%rax), %rcx
	movq	%rcx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rcx
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	movq	%rcx, %rsi
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rcx
	leaq	48(%r13), %rax
	movq	%rax, 48(%rdx)
	movq	39600(%rcx), %rax
	movq	%r15, (%rdx)
	movq	%rax, 72(%rdx)
	movslq	16(%r13), %rax
	movq	%r14, 32(%rdx)
	leal	1(%rax), %ecx
	movq	%r13, 40(%rdx)
	movl	%ecx, 16(%r13)
	movq	24(%rdx), %rcx
	movq	%rbx, 56(%rdx)
	movl	$4, 64(%rdx)
	movq	%rcx, 24(%r13,%rax,8)
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	-72(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	addq	$32, %rdx
	movq	56(%rax), %rax
	movq	%rdx, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L657
	movq	(%rdi), %rax
	call	*8(%rax)
.L657:
	cmpb	$0, 264(%r13)
	jne	.L651
	movq	%r13, %rdi
	call	_ZN2v88internal7Sweeper31ScheduleIncrementalSweepingTaskEv.part.0
	jmp	.L651
.L670:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22355:
	.size	_ZN2v88internal7Sweeper17StartSweeperTasksEv, .-_ZN2v88internal7Sweeper17StartSweeperTasksEv
	.section	.text._ZN2v88internal7Sweeper16GetSweptPageSafeEPNS0_10PagedSpaceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Sweeper16GetSweptPageSafeEPNS0_10PagedSpaceE
	.type	_ZN2v88internal7Sweeper16GetSweptPageSafeEPNS0_10PagedSpaceE, @function
_ZN2v88internal7Sweeper16GetSweptPageSafeEPNS0_10PagedSpaceE:
.LFB22361:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	80(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	72(%r12), %eax
	xorl	%r12d, %r12d
	subl	$2, %eax
	cltq
	leaq	(%rax,%rax,2), %rax
	leaq	120(%rbx,%rax,8), %rdx
	movq	8(%rdx), %rax
	cmpq	%rax, (%rdx)
	je	.L672
	movq	-8(%rax), %r12
	subq	$8, %rax
	movq	%rax, 8(%rdx)
.L672:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22361:
	.size	_ZN2v88internal7Sweeper16GetSweptPageSafeEPNS0_10PagedSpaceE, .-_ZN2v88internal7Sweeper16GetSweptPageSafeEPNS0_10PagedSpaceE
	.section	.text._ZN2v88internal7Sweeper20AbortAndWaitForTasksEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Sweeper20AbortAndWaitForTasksEv
	.type	_ZN2v88internal7Sweeper20AbortAndWaitForTasksEv, @function
_ZN2v88internal7Sweeper20AbortAndWaitForTasksEv:
.LFB22362:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_sweepingE(%rip)
	je	.L685
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	48(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	16(%rdi), %eax
	testl	%eax, %eax
	jle	.L680
.L682:
	movq	(%rbx), %rax
	movq	24(%rbx,%r12,8), %rsi
	movq	8048(%rax), %rdi
	call	_ZN2v88internal21CancelableTaskManager8TryAbortEm@PLT
	cmpl	$2, %eax
	je	.L679
	movq	%r13, %rdi
	addq	$1, %r12
	call	_ZN2v84base9Semaphore4WaitEv@PLT
	cmpl	%r12d, 16(%rbx)
	jg	.L682
.L680:
	movl	$0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L679:
	.cfi_restore_state
	lock subq	$1, 272(%rbx)
	addq	$1, %r12
	cmpl	%r12d, 16(%rbx)
	jg	.L682
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L685:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE22362:
	.size	_ZN2v88internal7Sweeper20AbortAndWaitForTasksEv, .-_ZN2v88internal7Sweeper20AbortAndWaitForTasksEv
	.section	.text._ZN2v88internal7Sweeper22AreSweeperTasksRunningEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Sweeper22AreSweeperTasksRunningEv
	.type	_ZN2v88internal7Sweeper22AreSweeperTasksRunningEv, @function
_ZN2v88internal7Sweeper22AreSweeperTasksRunningEv:
.LFB22366:
	.cfi_startproc
	endbr64
	movq	272(%rdi), %rax
	testq	%rax, %rax
	setne	%al
	ret
	.cfi_endproc
.LFE22366:
	.size	_ZN2v88internal7Sweeper22AreSweeperTasksRunningEv, .-_ZN2v88internal7Sweeper22AreSweeperTasksRunningEv
	.section	.text._ZN2v88internal7Sweeper31ScheduleIncrementalSweepingTaskEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Sweeper31ScheduleIncrementalSweepingTaskEv
	.type	_ZN2v88internal7Sweeper31ScheduleIncrementalSweepingTaskEv, @function
_ZN2v88internal7Sweeper31ScheduleIncrementalSweepingTaskEv:
.LFB22426:
	.cfi_startproc
	endbr64
	cmpb	$0, 264(%rdi)
	je	.L691
	ret
	.p2align 4,,10
	.p2align 3
.L691:
	jmp	_ZN2v88internal7Sweeper31ScheduleIncrementalSweepingTaskEv.part.0
	.cfi_endproc
.LFE22426:
	.size	_ZN2v88internal7Sweeper31ScheduleIncrementalSweepingTaskEv, .-_ZN2v88internal7Sweeper31ScheduleIncrementalSweepingTaskEv
	.section	.text._ZN2v88internal7Sweeper20PrepareToBeSweptPageENS0_15AllocationSpaceEPNS0_4PageE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Sweeper20PrepareToBeSweptPageENS0_15AllocationSpaceEPNS0_4PageE
	.type	_ZN2v88internal7Sweeper20PrepareToBeSweptPageENS0_15AllocationSpaceEPNS0_4PageE, @function
_ZN2v88internal7Sweeper20PrepareToBeSweptPageENS0_15AllocationSpaceEPNS0_4PageE:
.LFB22436:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	movq	$1, 168(%rdx)
	mfence
	movq	(%rdi), %rax
	movq	96(%rdx), %rdx
	movq	312(%rax,%rsi,8), %rax
	addq	%rdx, 184(%rax)
	ret
	.cfi_endproc
.LFE22436:
	.size	_ZN2v88internal7Sweeper20PrepareToBeSweptPageENS0_15AllocationSpaceEPNS0_4PageE, .-_ZN2v88internal7Sweeper20PrepareToBeSweptPageENS0_15AllocationSpaceEPNS0_4PageE
	.section	.text._ZN2v88internal7Sweeper19GetSweepingPageSafeENS0_15AllocationSpaceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Sweeper19GetSweepingPageSafeENS0_15AllocationSpaceE
	.type	_ZN2v88internal7Sweeper19GetSweepingPageSafeENS0_15AllocationSpaceE, @function
_ZN2v88internal7Sweeper19GetSweepingPageSafeENS0_15AllocationSpaceE:
.LFB22437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	80(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	leal	-2(%rbx), %esi
	movslq	%esi, %rsi
	leaq	(%rsi,%rsi,2), %rax
	leaq	192(%r12,%rax,8), %rdx
	movq	8(%rdx), %rax
	cmpq	%rax, (%rdx)
	je	.L695
	movq	-8(%rax), %r12
	subq	$8, %rax
	movq	%rax, 8(%rdx)
.L694:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L695:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L694
	.cfi_endproc
.LFE22437:
	.size	_ZN2v88internal7Sweeper19GetSweepingPageSafeENS0_15AllocationSpaceE, .-_ZN2v88internal7Sweeper19GetSweepingPageSafeENS0_15AllocationSpaceE
	.section	.text._ZN2v88internal7Sweeper21StartIterabilityTasksEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Sweeper21StartIterabilityTasksEv
	.type	_ZN2v88internal7Sweeper21StartIterabilityTasksEv, @function
_ZN2v88internal7Sweeper21StartIterabilityTasksEv:
.LFB22444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 352(%rdi)
	je	.L697
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_sweepingE(%rip)
	jne	.L705
.L697:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L706
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L705:
	.cfi_restore_state
	movq	296(%rdi), %rax
	movq	%rdi, %rbx
	cmpq	%rax, 288(%rdi)
	je	.L697
	movq	(%rdi), %rax
	movl	$64, %edi
	leaq	-37592(%rax), %r13
	call	_Znwm@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE@PLT
	leaq	16+_ZTVN2v88internal7Sweeper15IterabilityTaskE(%rip), %rax
	movq	%rbx, 40(%r12)
	addq	$32, %r12
	movq	%rax, -32(%r12)
	addq	$48, %rax
	movq	%rax, (%r12)
	leaq	320(%rbx), %rax
	movq	%rax, 16(%r12)
	movq	39600(%r13), %rax
	movb	$1, 353(%rbx)
	movq	%rax, 24(%r12)
	movq	-8(%r12), %rax
	movq	%rax, 312(%rbx)
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	leaq	-48(%rbp), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	%r12, -48(%rbp)
	call	*56(%rax)
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L697
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L697
.L706:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22444:
	.size	_ZN2v88internal7Sweeper21StartIterabilityTasksEv, .-_ZN2v88internal7Sweeper21StartIterabilityTasksEv
	.section	.text._ZNSt8_Rb_treeIjSt4pairIKjjESt10_Select1stIS2_ESt4lessIjESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,"axG",@progbits,_ZNSt8_Rb_treeIjSt4pairIKjjESt10_Select1stIS2_ESt4lessIjESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIjSt4pairIKjjESt10_Select1stIS2_ESt4lessIjESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.type	_ZNSt8_Rb_treeIjSt4pairIKjjESt10_Select1stIS2_ESt4lessIjESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, @function
_ZNSt8_Rb_treeIjSt4pairIKjjESt10_Select1stIS2_ESt4lessIjESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E:
.LFB26565:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L715
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L709:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIjSt4pairIKjjESt10_Select1stIS2_ESt4lessIjESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L709
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L715:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE26565:
	.size	_ZNSt8_Rb_treeIjSt4pairIKjjESt10_Select1stIS2_ESt4lessIjESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, .-_ZNSt8_Rb_treeIjSt4pairIKjjESt10_Select1stIS2_ESt4lessIjESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.section	.text._ZN2v88internal23LocalArrayBufferTracker4FreeIZNS0_18ArrayBufferTracker8FreeDeadINS0_26MajorNonAtomicMarkingStateEEEvPNS0_4PageEPT_EUlNS0_13JSArrayBufferEE_EEvS8_,"axG",@progbits,_ZN2v88internal23LocalArrayBufferTracker4FreeIZNS0_18ArrayBufferTracker8FreeDeadINS0_26MajorNonAtomicMarkingStateEEEvPNS0_4PageEPT_EUlNS0_13JSArrayBufferEE_EEvS8_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23LocalArrayBufferTracker4FreeIZNS0_18ArrayBufferTracker8FreeDeadINS0_26MajorNonAtomicMarkingStateEEEvPNS0_4PageEPT_EUlNS0_13JSArrayBufferEE_EEvS8_
	.type	_ZN2v88internal23LocalArrayBufferTracker4FreeIZNS0_18ArrayBufferTracker8FreeDeadINS0_26MajorNonAtomicMarkingStateEEEvPNS0_4PageEPT_EUlNS0_13JSArrayBufferEE_EEvS8_, @function
_ZN2v88internal23LocalArrayBufferTracker4FreeIZNS0_18ArrayBufferTracker8FreeDeadINS0_26MajorNonAtomicMarkingStateEEEvPNS0_4PageEPT_EUlNS0_13JSArrayBufferEE_EEvS8_:
.LFB26568:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	$1, %ebx
	subq	$24, %rsp
	movq	(%rdi), %rax
	movq	24(%rdi), %r14
	movq	24(%rax), %r13
	leaq	-37592(%r13), %rax
	movq	%rax, -64(%rbp)
	testq	%r14, %r14
	jne	.L719
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L722:
	movq	(%r14), %r14
	testq	%r14, %r14
	je	.L721
.L719:
	movq	8(%r14), %rcx
	movl	%ebx, %esi
	movq	%rcx, %rdx
	andl	$262143, %ecx
	andq	$-262144, %rdx
	movl	%ecx, %eax
	shrl	$3, %ecx
	movq	16(%rdx), %rdx
	shrl	$8, %eax
	sall	%cl, %esi
	testl	%esi, (%rdx,%rax,4)
	jne	.L722
	movq	24(%r14), %rax
	movq	-64(%rbp), %rdi
	pushq	40(%r14)
	pushq	32(%r14)
	pushq	24(%r14)
	pushq	16(%r14)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal13JSArrayBuffer16FreeBackingStoreEPNS0_7IsolateENS1_10AllocationE@PLT
	movq	16(%r15), %rdi
	movq	48(%r14), %rax
	xorl	%edx, %edx
	movq	8(%r15), %r10
	addq	$32, %rsp
	divq	%rdi
	leaq	0(,%rdx,8), %r11
	movq	%rdx, %r9
	leaq	(%r10,%r11), %rdx
	movq	(%rdx), %rax
	movq	%rax, %r8
	.p2align 4,,10
	.p2align 3
.L723:
	movq	%r8, %rcx
	movq	(%r8), %r8
	cmpq	%r8, %r14
	jne	.L723
	movq	(%r14), %r13
	cmpq	%rcx, %rax
	je	.L742
	testq	%r13, %r13
	je	.L726
	movq	48(%r13), %rax
	xorl	%edx, %edx
	divq	%rdi
	cmpq	%rdx, %r9
	je	.L726
	movq	%rcx, (%r10,%rdx,8)
	movq	(%r14), %r13
.L726:
	movq	%r13, (%rcx)
	movq	%r14, %rdi
	movq	%r13, %r14
	call	_ZdlPv@PLT
	subq	$1, 32(%r15)
	addq	-56(%rbp), %r12
	testq	%r14, %r14
	jne	.L719
.L721:
	testq	%r12, %r12
	jne	.L743
.L718:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L742:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L731
	movq	48(%r13), %rax
	xorl	%edx, %edx
	divq	%rdi
	cmpq	%rdx, %r9
	je	.L726
	movq	%rcx, (%r10,%rdx,8)
	movq	8(%r15), %rdx
	leaq	24(%r15), %rdi
	addq	%r11, %rdx
	movq	(%rdx), %rax
	cmpq	%rdi, %rax
	je	.L744
.L727:
	movq	$0, (%rdx)
	movq	(%r14), %r13
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L743:
	movq	(%r15), %rax
	lock subq	%r12, 200(%rax)
	movq	80(%rax), %rax
	movq	48(%rax), %rdx
	lock subq	%r12, (%rdx)
	movq	64(%rax), %rax
	lock subq	%r12, 176(%rax)
	movq	(%r15), %rax
	movq	24(%rax), %rax
	lock addq	%r12, 56(%rax)
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L731:
	.cfi_restore_state
	movq	%rcx, %rax
	leaq	24(%r15), %rdi
	cmpq	%rdi, %rax
	jne	.L727
.L744:
	movq	%r13, 24(%r15)
	jmp	.L727
	.cfi_endproc
.LFE26568:
	.size	_ZN2v88internal23LocalArrayBufferTracker4FreeIZNS0_18ArrayBufferTracker8FreeDeadINS0_26MajorNonAtomicMarkingStateEEEvPNS0_4PageEPT_EUlNS0_13JSArrayBufferEE_EEvS8_, .-_ZN2v88internal23LocalArrayBufferTracker4FreeIZNS0_18ArrayBufferTracker8FreeDeadINS0_26MajorNonAtomicMarkingStateEEEvPNS0_4PageEPT_EUlNS0_13JSArrayBufferEE_EEvS8_
	.section	.text._ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB26569:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L753
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L747:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L747
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L753:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE26569:
	.size	_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.text._ZNSt8_Rb_treeIjSt4pairIKjjESt10_Select1stIS2_ESt4lessIjESaIS2_EE17_M_emplace_uniqueIJS0_IjjEEEES0_ISt17_Rb_tree_iteratorIS2_EbEDpOT_,"axG",@progbits,_ZNSt8_Rb_treeIjSt4pairIKjjESt10_Select1stIS2_ESt4lessIjESaIS2_EE17_M_emplace_uniqueIJS0_IjjEEEES0_ISt17_Rb_tree_iteratorIS2_EbEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIjSt4pairIKjjESt10_Select1stIS2_ESt4lessIjESaIS2_EE17_M_emplace_uniqueIJS0_IjjEEEES0_ISt17_Rb_tree_iteratorIS2_EbEDpOT_
	.type	_ZNSt8_Rb_treeIjSt4pairIKjjESt10_Select1stIS2_ESt4lessIjESaIS2_EE17_M_emplace_uniqueIJS0_IjjEEEES0_ISt17_Rb_tree_iteratorIS2_EbEDpOT_, @function
_ZNSt8_Rb_treeIjSt4pairIKjjESt10_Select1stIS2_ESt4lessIjESaIS2_EE17_M_emplace_uniqueIJS0_IjjEEEES0_ISt17_Rb_tree_iteratorIS2_EbEDpOT_:
.LFB26592:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$40, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	16(%rbx), %r12
	movl	(%r14), %r15d
	movq	%rax, %r13
	movq	(%r14), %rax
	leaq	8(%rbx), %r14
	movq	%rax, 32(%r13)
	testq	%r12, %r12
	jne	.L758
	jmp	.L778
	.p2align 4,,10
	.p2align 3
.L779:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L759
.L780:
	movq	%rax, %r12
.L758:
	movl	32(%r12), %ecx
	cmpl	%ecx, %r15d
	jb	.L779
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L780
.L759:
	testb	%dl, %dl
	jne	.L781
	cmpl	%ecx, %r15d
	jbe	.L764
.L767:
	movl	$1, %edi
	cmpq	%r12, %r14
	jne	.L782
.L765:
	movq	%r12, %rdx
	movq	%r14, %rcx
	movq	%r13, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L781:
	.cfi_restore_state
	cmpq	24(%rbx), %r12
	je	.L767
.L768:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	cmpl	32(%rax), %r15d
	ja	.L783
	movq	%rax, %r12
.L764:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L783:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L764
	movl	$1, %edi
	cmpq	%r12, %r14
	je	.L765
.L782:
	xorl	%edi, %edi
	cmpl	32(%r12), %r15d
	setb	%dil
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L778:
	movq	%r14, %r12
	cmpq	24(%rbx), %r14
	jne	.L768
	movl	$1, %edi
	jmp	.L765
	.cfi_endproc
.LFE26592:
	.size	_ZNSt8_Rb_treeIjSt4pairIKjjESt10_Select1stIS2_ESt4lessIjESaIS2_EE17_M_emplace_uniqueIJS0_IjjEEEES0_ISt17_Rb_tree_iteratorIS2_EbEDpOT_, .-_ZNSt8_Rb_treeIjSt4pairIKjjESt10_Select1stIS2_ESt4lessIjESaIS2_EE17_M_emplace_uniqueIJS0_IjjEEEES0_ISt17_Rb_tree_iteratorIS2_EbEDpOT_
	.section	.rodata._ZN2v88internal7Sweeper8RawSweepEPNS0_4PageENS1_22FreeListRebuildingModeENS0_22FreeSpaceTreatmentModeENS1_35FreeSpaceMayContainInvalidatedSlotsE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"free_end > free_start"
.LC4:
	.string	"p->area_end() > free_start"
	.section	.text._ZN2v88internal7Sweeper8RawSweepEPNS0_4PageENS1_22FreeListRebuildingModeENS0_22FreeSpaceTreatmentModeENS1_35FreeSpaceMayContainInvalidatedSlotsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Sweeper8RawSweepEPNS0_4PageENS1_22FreeListRebuildingModeENS0_22FreeSpaceTreatmentModeENS1_35FreeSpaceMayContainInvalidatedSlotsE
	.type	_ZN2v88internal7Sweeper8RawSweepEPNS0_4PageENS1_22FreeListRebuildingModeENS0_22FreeSpaceTreatmentModeENS1_35FreeSpaceMayContainInvalidatedSlotsE, @function
_ZN2v88internal7Sweeper8RawSweepEPNS0_4PageENS1_22FreeListRebuildingModeENS0_22FreeSpaceTreatmentModeENS1_35FreeSpaceMayContainInvalidatedSlotsE:
.LFB22367:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$568, %rsp
	movq	%rdi, -472(%rbp)
	movq	%rsi, -400(%rbp)
	movl	%edx, -476(%rbp)
	movl	%ecx, -480(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	80(%rsi), %rax
	movq	%rax, -488(%rbp)
	movq	272(%rsi), %rax
	movq	%rax, -456(%rbp)
	movq	120(%rsi), %rax
	movb	$1, -489(%rbp)
	testq	%rax, %rax
	je	.L1337
.L785:
	leaq	-296(%rbp), %rax
	movl	$0, -296(%rbp)
	movq	%rax, -280(%rbp)
	movq	%rax, -272(%rbp)
	movq	-400(%rbp), %rax
	movq	$0, -288(%rbp)
	movq	248(%rax), %r12
	movq	$0, -264(%rbp)
	testq	%r12, %r12
	je	.L787
	movq	-472(%rbp), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	call	_ZN2v88internal23LocalArrayBufferTracker4FreeIZNS0_18ArrayBufferTracker8FreeDeadINS0_26MajorNonAtomicMarkingStateEEEvPNS0_4PageEPT_EUlNS0_13JSArrayBufferEE_EEvS8_
	cmpq	$0, 32(%r12)
	je	.L1338
.L787:
	movq	-400(%rbp), %rsi
	leaq	-256(%rbp), %rdi
	movq	40(%rsi), %rax
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal23InvalidatedSlotsCleanup9NoCleanupEPNS0_11MemoryChunkE@PLT
	testl	%ebx, %ebx
	je	.L1339
.L789:
	movq	-400(%rbp), %rdi
	call	_ZN2v88internal4Page25ResetAllocationStatisticsEv@PLT
	movq	-456(%rbp), %rax
	testq	%rax, %rax
	je	.L795
	movq	%rax, %rdi
	call	_ZN2v88internal18CodeObjectRegistry5ClearEv@PLT
.L795:
	movq	-400(%rbp), %rbx
	movq	16(%rbx), %rax
	movl	%ebx, -368(%rbp)
	movq	%rax, -336(%rbp)
	movq	24(%rbx), %rax
	movq	-37528(%rax), %rdi
	subq	$37592, %rax
	movq	%rdi, -376(%rbp)
	movq	72(%rax), %rdi
	movq	56(%rax), %rax
	movq	%rdi, -384(%rbp)
	movq	%rax, -392(%rbp)
	movq	48(%rbx), %rax
	movq	%rax, -360(%rbp)
	movl	-360(%rbp), %eax
	subl	%ebx, %eax
	shrl	$8, %eax
	movl	%eax, -364(%rbp)
	movq	40(%rbx), %rax
	movq	%rax, -360(%rbp)
	movl	-360(%rbp), %eax
	movq	%rbx, -360(%rbp)
	subl	%ebx, %eax
	shrl	$8, %eax
	movl	%eax, -464(%rbp)
	je	.L796
	sall	$8, %eax
	addq	%rbx, %rax
	movq	%rax, -360(%rbp)
.L796:
	movl	-464(%rbp), %ebx
	cmpl	%ebx, -364(%rbp)
	ja	.L1340
	movl	-364(%rbp), %eax
	testl	%eax, %eax
	jne	.L1106
	movq	$0, -424(%rbp)
	movq	$0, -440(%rbp)
.L822:
	movq	-400(%rbp), %rax
	movq	48(%rax), %rax
	cmpq	-352(%rbp), %rax
	je	.L1032
	jbe	.L1341
	subq	-352(%rbp), %rax
	cmpl	$1, -480(%rbp)
	movq	%rax, %r12
	je	.L1342
.L1034:
	movl	-476(%rbp), %edx
	testl	%edx, %edx
	je	.L1343
	movq	-400(%rbp), %rax
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	%r12d, %edx
	movq	-352(%rbp), %rsi
	movq	24(%rax), %rdi
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
.L1035:
	movq	-472(%rbp), %rax
	cmpb	$0, 354(%rax)
	jne	.L1344
.L1036:
	movq	-400(%rbp), %rbx
	movq	-352(%rbp), %r15
	movq	48(%rbx), %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE0EE11RemoveRangeEPNS0_11MemoryChunkEmmNS0_7SlotSet15EmptyBucketModeE.constprop.0
	movq	48(%rbx), %rdx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE11RemoveRangeEPNS0_11MemoryChunkEmmNS0_7SlotSet15EmptyBucketModeE.constprop.0
	cmpb	$0, -489(%rbp)
	jne	.L1345
.L1037:
	movq	-256(%rbp), %r14
	movq	-248(%rbp), %rbx
	cmpq	%rbx, %r14
	je	.L1032
	movq	-400(%rbp), %rax
	movq	-176(%rbp), %r15
	movq	48(%rax), %r12
	movq	-352(%rbp), %rax
	cmpq	-168(%rbp), %rax
	jb	.L1043
	movq	-184(%rbp), %r13
	movq	%r14, %rdi
	movq	%r13, %xmm5
	punpcklqdq	%xmm5, %xmm5
	movaps	%xmm5, -336(%rbp)
	.p2align 4,,10
	.p2align 3
.L1040:
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, -256(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rbx
	je	.L1042
	movq	32(%rax), %rax
	leaq	-1(%rax), %r15
	movq	%r15, -176(%rbp)
	movslq	40(%rdi), %rax
	addq	%r15, %rax
	movq	%rax, -168(%rbp)
	cmpq	%rax, -352(%rbp)
	jnb	.L1040
	movq	%rdi, %r14
.L1043:
	movq	-352(%rbp), %r13
	cmpq	%r15, %r12
	ja	.L1041
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1045:
	movq	%rax, -256(%rbp)
	cmpq	%rbx, -248(%rbp)
	je	.L1047
.L1346:
	movq	32(%rbx), %rax
	leaq	-1(%rax), %r15
	movq	%r15, -176(%rbp)
	movslq	40(%rbx), %rax
	addq	%r15, %rax
	movq	%rax, -168(%rbp)
	cmpq	%r15, %r12
	jbe	.L1032
.L1048:
	movq	%rbx, %r14
.L1041:
	movq	%r14, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rbx
	cmpq	%r13, %r15
	jb	.L1045
	movq	-240(%rbp), %r15
	movq	%r14, %rdi
	leaq	8(%r15), %rsi
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	subq	$1, 40(%r15)
	movq	%rbx, -256(%rbp)
	cmpq	%rbx, -248(%rbp)
	jne	.L1346
.L1047:
	movq	-184(%rbp), %r15
	movq	%r15, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -176(%rbp)
	cmpq	%r15, %r12
	ja	.L1048
	.p2align 4,,10
	.p2align 3
.L1032:
	cmpq	$0, -264(%rbp)
	jne	.L1347
.L1050:
	movq	-400(%rbp), %rax
	movq	16(%rax), %rax
	leaq	8(%rax), %rdi
	movq	$0, (%rax)
	movq	$0, 4088(%rax)
	andq	$-8, %rdi
	subq	%rdi, %rax
	leal	4096(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	cmpl	$1, -476(%rbp)
	je	.L1348
	movq	-400(%rbp), %rax
	movq	$0, 168(%rax)
	mfence
	cmpq	$0, -456(%rbp)
	je	.L1073
	movq	-456(%rbp), %rdi
	call	_ZN2v88internal18CodeObjectRegistry8FinalizeEv@PLT
.L1073:
	movq	-400(%rbp), %rax
	movq	80(%rax), %rax
	movq	96(%rax), %rdi
	movq	-424(%rbp), %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movl	%eax, %r13d
.L1055:
	movq	-216(%rbp), %r12
	leaq	-232(%rbp), %rbx
	testq	%r12, %r12
	je	.L1059
.L1056:
	movq	24(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r12, %rdi
	movq	16(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L1056
.L1059:
	movq	-288(%rbp), %rbx
	leaq	-304(%rbp), %r12
	testq	%rbx, %rbx
	je	.L784
.L1058:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIjSt4pairIKjjESt10_Select1stIS2_ESt4lessIjESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1058
.L784:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1349
	addq	$568, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1338:
	.cfi_restore_state
	movq	-400(%rbp), %rdi
	call	_ZN2v88internal11MemoryChunk19ReleaseLocalTrackerEv@PLT
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L1337:
	movq	-400(%rbp), %rax
	movq	128(%rax), %rax
	testq	%rax, %rax
	setne	-489(%rbp)
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L1340:
	movl	%ebx, %eax
	movq	-336(%rbp), %rbx
	movq	-360(%rbp), %rdx
	movq	%rax, %r15
	movl	(%rbx,%rax,4), %r13d
	movl	$1, %ebx
	.p2align 4,,10
	.p2align 3
.L805:
	testl	%r13d, %r13d
	je	.L801
.L1352:
	xorl	%r12d, %r12d
	movl	%ebx, %eax
	rep bsfl	%r13d, %r12d
	movl	%r12d, %ecx
	sall	%cl, %eax
	movl	%eax, %ecx
	notl	%ecx
	andl	%ecx, %r13d
	cmpl	$31, %r12d
	je	.L1350
	leal	1(%r12), %ecx
	movl	%ebx, %eax
	movq	%rdx, %r8
	sall	%cl, %eax
	movl	%eax, %ecx
	testl	%r13d, %ecx
	jne	.L1351
.L1096:
	movq	%r8, %rdx
	testl	%r13d, %r13d
	jne	.L1352
.L801:
	addq	$256, -360(%rbp)
	addl	$1, %r15d
	cmpl	%r15d, -364(%rbp)
	je	.L1336
	movq	-336(%rbp), %rdi
	movl	%r15d, %eax
	movl	(%rdi,%rax,4), %r13d
	jbe	.L1353
	movq	-360(%rbp), %rdx
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L1336:
	movq	%rdx, -344(%rbp)
	movl	%r15d, -464(%rbp)
.L1335:
	movq	-400(%rbp), %rbx
	xorl	%r12d, %r12d
	movq	24(%rbx), %rax
	movq	-37528(%rax), %rdi
	subq	$37592, %rax
	movq	%rdi, -424(%rbp)
	movq	72(%rax), %rdi
	movq	%rdi, -432(%rbp)
	movq	56(%rax), %rax
	movq	%rax, -448(%rbp)
	movq	48(%rbx), %rax
	movq	%rax, -416(%rbp)
	movl	-416(%rbp), %eax
	subl	%ebx, %eax
	shrl	$8, %eax
	movl	%eax, -440(%rbp)
.L799:
	movl	-364(%rbp), %eax
	movl	%eax, %r10d
	sall	$8, %r10d
	leaq	(%rbx,%r10), %r15
	movl	-440(%rbp), %ebx
	cmpl	%ebx, %eax
	jnb	.L814
	movq	-336(%rbp), %rbx
	movq	%r12, -512(%rbp)
	movl	%eax, %eax
	movl	%r13d, -504(%rbp)
	movq	%rax, %r14
	movq	%r15, %r13
	movl	(%rbx,%rax,4), %ebx
	movq	%r15, %rax
	.p2align 4,,10
	.p2align 3
.L816:
	testl	%ebx, %ebx
	je	.L812
.L1356:
	xorl	%edx, %edx
	rep bsfl	%ebx, %edx
	cmpl	$31, %edx
	je	.L1354
	movl	%edx, %ecx
	movl	$1, %esi
	movq	%rax, %r12
	sall	%cl, %esi
	movl	%esi, %ecx
	movl	$1, %esi
	notl	%ecx
	andl	%ecx, %ebx
	leal	1(%rdx), %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	testl	%ebx, %ecx
	jne	.L1355
.L1099:
	movq	%r12, %rax
	testl	%ebx, %ebx
	jne	.L1356
.L812:
	addq	$256, %r13
	addl	$1, %r14d
	cmpl	%r14d, -440(%rbp)
	je	.L1297
	movq	-336(%rbp), %rbx
	movl	%r14d, %eax
	movl	(%rbx,%rax,4), %ebx
	jbe	.L1297
	movq	%r13, %rax
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L1350:
	addq	$256, -360(%rbp)
	addl	$1, %r15d
	cmpl	%r15d, -364(%rbp)
	je	.L1336
	movq	-336(%rbp), %rax
	movl	%r15d, %ecx
	movq	-360(%rbp), %r8
	movl	(%rax,%rcx,4), %r13d
	movl	$1, %ecx
	testl	%r13d, %ecx
	je	.L1096
.L1351:
	sall	$3, %r12d
	movq	%r8, -344(%rbp)
	leaq	-160(%rbp), %rdi
	addq	%rdx, %r12
	leaq	1(%r12), %rax
	movq	%rax, -160(%rbp)
	movq	(%r12), %r14
	movq	%r14, %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-344(%rbp), %r8
	cltq
	leaq	-8(%r12,%rax), %rcx
	movq	%r8, %rdx
	cmpq	%rcx, %r12
	je	.L806
	subl	-368(%rbp), %ecx
	movl	%ecx, %eax
	shrl	$3, %ecx
	shrl	$8, %eax
	andl	$31, %ecx
	cmpl	%r15d, %eax
	je	.L807
	movl	%eax, %edx
	movq	-336(%rbp), %rsi
	subl	%r15d, %edx
	sall	$8, %edx
	addq	%rdx, -360(%rbp)
	movl	%eax, %edx
	movq	-360(%rbp), %rdi
	movl	(%rsi,%rdx,4), %r13d
	movq	%rdi, %rdx
.L807:
	movl	$-2, %edi
	movl	%eax, %r15d
	sall	%cl, %edi
	andl	%edi, %r13d
.L806:
	movq	-160(%rbp), %r12
	testq	%r12, %r12
	je	.L805
	cmpq	%r14, -376(%rbp)
	sete	%cl
	cmpq	%r14, -384(%rbp)
	sete	%al
	orb	%al, %cl
	jne	.L805
	cmpq	%r14, -392(%rbp)
	je	.L805
	movq	%rdx, -344(%rbp)
	movl	%r15d, -464(%rbp)
	testl	%r13d, %r13d
	jne	.L1079
	addl	$1, -464(%rbp)
	movl	-464(%rbp), %eax
	addq	$256, -360(%rbp)
	cmpl	%eax, -364(%rbp)
	je	.L1079
	movl	-464(%rbp), %eax
	movq	-336(%rbp), %rbx
	movl	(%rbx,%rax,4), %r13d
	movq	-360(%rbp), %rax
	movq	%rax, -344(%rbp)
.L1079:
	movq	-400(%rbp), %rbx
	movq	24(%rbx), %rax
	movq	-37528(%rax), %rdi
	subq	$37592, %rax
	movq	%rdi, -424(%rbp)
	movq	72(%rax), %rdi
	movq	56(%rax), %rax
	movq	%rdi, -432(%rbp)
	movq	%rax, -448(%rbp)
	movq	48(%rbx), %rax
	movq	%rax, -416(%rbp)
	movl	-416(%rbp), %eax
	subl	%ebx, %eax
	shrl	$8, %eax
	movl	%eax, -440(%rbp)
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L1354:
	addq	$256, %r13
	addl	$1, %r14d
	cmpl	-440(%rbp), %r14d
	je	.L1297
	movq	-336(%rbp), %rbx
	movl	%r14d, %ecx
	movq	%r13, %r12
	movl	(%rbx,%rcx,4), %ebx
	movl	$1, %ecx
	testl	%ebx, %ecx
	je	.L1099
.L1355:
	sall	$3, %edx
	leaq	-160(%rbp), %rdi
	leaq	(%rdx,%rax), %r15
	leaq	1(%r15), %rax
	movq	%rax, -160(%rbp)
	movq	(%r15), %rdx
	movq	%rdx, %rsi
	movq	%rdx, -416(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-416(%rbp), %rdx
	cltq
	leaq	-8(%r15,%rax), %rcx
	movq	%r12, %rax
	cmpq	%rcx, %r15
	je	.L817
	subl	-368(%rbp), %ecx
	movl	%ecx, %esi
	shrl	$3, %ecx
	shrl	$8, %esi
	andl	$31, %ecx
	cmpl	%esi, %r14d
	je	.L818
	movl	%esi, %eax
	movq	-336(%rbp), %rbx
	subl	%r14d, %eax
	sall	$8, %eax
	addq	%rax, %r13
	movl	%esi, %eax
	movl	(%rbx,%rax,4), %ebx
	movq	%r13, %r12
.L818:
	movl	$-2, %eax
	movl	%esi, %r14d
	sall	%cl, %eax
	andl	%eax, %ebx
	movq	%r12, %rax
.L817:
	movq	-160(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L816
	cmpq	-432(%rbp), %rdx
	sete	%dil
	cmpq	-424(%rbp), %rdx
	sete	%sil
	orb	%sil, %dil
	jne	.L816
	cmpq	-448(%rbp), %rdx
	je	.L816
	movq	%rcx, -432(%rbp)
	movl	-504(%rbp), %r13d
	movq	-512(%rbp), %r12
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L1106:
	xorl	%r12d, %r12d
.L814:
	movq	$0, -432(%rbp)
.L810:
	cmpq	%r12, -432(%rbp)
	movq	$0, -424(%rbp)
	movq	$0, -440(%rbp)
	je	.L822
	movq	-360(%rbp), %r14
	movl	-464(%rbp), %ebx
.L821:
	movq	-456(%rbp), %rax
	movq	%r12, -312(%rbp)
	leaq	-1(%r12), %r15
	testq	%rax, %rax
	je	.L823
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal18CodeObjectRegistry33RegisterAlreadyExistingCodeObjectEm@PLT
	movq	-312(%rbp), %rax
	leaq	-1(%rax), %r15
.L823:
	cmpq	-352(%rbp), %r15
	je	.L824
	jbe	.L1357
	movq	%r15, %r12
	subq	-352(%rbp), %r12
	cmpl	$1, -480(%rbp)
	je	.L1358
.L826:
	movl	-476(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L827
	movq	-488(%rbp), %rax
	movq	-352(%rbp), %rsi
	movl	$1, %ecx
	movl	%r12d, %edx
	movl	$1, %r8d
	movq	64(%rax), %rdi
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	-488(%rbp), %rax
	movl	$1, %ecx
	movq	%r12, %rdx
	movq	-352(%rbp), %rsi
	movq	96(%rax), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%r12, %rdi
	subq	%rax, %rdi
	movq	%rdi, %rax
	movq	-424(%rbp), %rdi
	cmpq	%rax, %rdi
	cmovge	%rdi, %rax
	movq	%rax, -424(%rbp)
	movq	-472(%rbp), %rax
	cmpb	$0, 354(%rax)
	jne	.L1359
.L829:
	movq	-400(%rbp), %rax
	movq	104(%rax), %r11
	testq	%r11, %r11
	je	.L830
	movq	-352(%rbp), %rcx
	movq	%r15, %rdi
	subq	%rax, %rdi
	subq	%rax, %rcx
	movq	%rdi, %rax
	cmpq	$262143, %rdi
	jbe	.L1360
	movq	%rcx, %rdi
	leaq	-1(%rax), %rdx
	movl	%ecx, %esi
	movl	%eax, %r12d
	shrq	$18, %rdi
	shrq	$18, %rdx
	andl	$262143, %esi
	movslq	%edi, %r9
	movl	%edx, %ecx
	movl	%esi, -360(%rbp)
	movslq	%edx, %rsi
	leaq	(%r9,%r9,2), %rax
	sall	$18, %ecx
	movl	%edi, -416(%rbp)
	salq	$7, %rax
	movl	%edx, -448(%rbp)
	subl	%ecx, %r12d
	movq	%rsi, -504(%rbp)
	leaq	(%r11,%rax), %r10
	movq	%rax, -464(%rbp)
	cmpl	%edx, %edi
	je	.L1361
	movl	-360(%rbp), %eax
	sarl	$13, %eax
	movl	%eax, -512(%rbp)
	cltq
	leaq	0(,%rax,8), %rsi
	movq	%rax, -520(%rbp)
	movq	%rsi, -528(%rbp)
	movq	(%r10,%rax,8), %rax
	movq	%rax, -536(%rbp)
	testq	%rax, %rax
	jne	.L875
.L880:
	cmpl	$31, -512(%rbp)
	je	.L876
	movq	-528(%rbp), %rax
	leaq	8(%r10,%rax), %rcx
	movl	$30, %eax
	subl	-512(%rbp), %eax
	addq	-520(%rbp), %rax
	leaq	16(%r10,%rax,8), %r8
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L1362:
	addq	$8, %rcx
	cmpq	%rcx, %r8
	je	.L876
.L885:
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L1362
	leaq	128(%rax), %rsi
	.p2align 4,,10
	.p2align 3
.L887:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rsi
	jne	.L887
	addq	$8, %rcx
	cmpq	%rcx, %r8
	jne	.L885
.L876:
	movq	256(%r10), %rax
	leal	1(%rdi), %eax
	cmpl	%eax, %edx
	jle	.L882
	cltq
	subl	%edi, %edx
	leaq	(%rax,%rax,2), %rax
	salq	$7, %rax
	leaq	256(%r11,%rax), %rcx
	leal	-2(%rdx), %eax
	addq	%r9, %rax
	leaq	(%rax,%rax,2), %rax
	salq	$7, %rax
	leaq	1024(%r11,%rax), %rsi
	.p2align 4,,10
	.p2align 3
.L901:
	movq	-256(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.L894
.L896:
	leaq	-248(%rcx), %rdx
	jmp	.L891
	.p2align 4,,10
	.p2align 3
.L1364:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	je	.L1363
.L891:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L1364
	leaq	128(%rax), %rdi
	.p2align 4,,10
	.p2align 3
.L900:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdi
	jne	.L900
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	jne	.L891
.L1363:
	movq	(%rcx), %rax
	addq	$384, %rcx
	cmpq	%rcx, %rsi
	jne	.L901
.L882:
	movq	-504(%rbp), %rax
	leaq	(%rax,%rax,2), %rsi
	salq	$7, %rsi
	addq	%r11, %rsi
	cmpl	$262144, %r12d
	jg	.L888
	movl	%r12d, %ecx
	movl	%r12d, %edi
	movl	%r12d, %r10d
	movl	$1, %r11d
	sarl	$3, %ecx
	sarl	$13, %edi
	sarl	$8, %r10d
	sall	%cl, %r11d
	movl	%edi, %eax
	andl	$31, %r10d
	movl	%r11d, %r8d
	negl	%r8d
	orl	%r10d, %eax
	je	.L1365
	movq	(%rsi), %rdx
	testq	%rdx, %rdx
	jne	.L906
	testl	%edi, %edi
	jg	.L1069
.L1331:
	xorl	%eax, %eax
	movl	$1, %edi
.L909:
	movq	(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L855
	testb	%al, %al
	jne	.L855
	cmpl	%edi, %r10d
	jle	.L917
	leal	-1(%r10), %ecx
	movslq	%edi, %rsi
	subl	%edi, %ecx
	leaq	(%rdx,%rsi,4), %rax
	addq	%rsi, %rcx
	leaq	4(%rdx,%rcx,4), %rcx
	.p2align 4,,10
	.p2align 3
.L918:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rcx, %rax
	jne	.L918
.L917:
	movslq	%r10d, %r10
	leal	-1(%r11), %ecx
	leaq	(%rdx,%r10,4), %rsi
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L1366:
	movl	%r8d, %edi
	movl	%edx, %eax
	andl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L855
.L916:
	movl	(%rsi), %edx
	testl	%edx, %ecx
	jne	.L1366
	.p2align 4,,10
	.p2align 3
.L855:
	movq	-400(%rbp), %rax
	movq	112(%rax), %rax
	movq	%rax, %r11
	testq	%rax, %rax
	jne	.L1081
	.p2align 4,,10
	.p2align 3
.L924:
	cmpb	$0, -489(%rbp)
	jne	.L1367
.L921:
	movq	-256(%rbp), %rdi
	movq	-248(%rbp), %r12
	cmpq	%r12, %rdi
	je	.L1018
	movq	-352(%rbp), %rax
	movq	-176(%rbp), %rdx
	cmpq	%rax, -168(%rbp)
	ja	.L1013
	movq	-184(%rbp), %rax
	movq	%rax, %xmm2
	movq	%rax, -360(%rbp)
	punpcklqdq	%xmm2, %xmm2
	movaps	%xmm2, -416(%rbp)
	.p2align 4,,10
	.p2align 3
.L1010:
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, -256(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %r12
	je	.L1012
	movq	32(%rax), %rax
	leaq	-1(%rax), %rdx
	movq	%rdx, -176(%rbp)
	movslq	40(%rdi), %rax
	addq	%rdx, %rax
	movq	%rax, -168(%rbp)
	cmpq	%rax, -352(%rbp)
	jnb	.L1010
.L1013:
	cmpq	%rdx, %r15
	jbe	.L1018
	movl	%r13d, -360(%rbp)
	movq	-352(%rbp), %r12
	movq	%rdi, %r13
	movl	%ebx, -416(%rbp)
	movq	%rdx, %rbx
	movq	%r14, -352(%rbp)
	jmp	.L1011
	.p2align 4,,10
	.p2align 3
.L1015:
	movq	%rax, -256(%rbp)
	cmpq	%r14, -248(%rbp)
	je	.L1017
.L1368:
	movq	32(%r14), %rax
	leaq	-1(%rax), %rbx
	movq	%rbx, -176(%rbp)
	movslq	40(%r14), %rax
	addq	%rbx, %rax
	movq	%rax, -168(%rbp)
	cmpq	%rbx, %r15
	jbe	.L1333
.L1019:
	movq	%r14, %r13
.L1011:
	movq	%r13, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r14
	cmpq	%rbx, %r12
	ja	.L1015
	movq	-240(%rbp), %rbx
	movq	%r13, %rdi
	leaq	8(%rbx), %rsi
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	subq	$1, 40(%rbx)
	movq	%r14, -256(%rbp)
	cmpq	%r14, -248(%rbp)
	jne	.L1368
.L1017:
	movq	-184(%rbp), %rbx
	movq	%rbx, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -176(%rbp)
	cmpq	%rbx, %r15
	ja	.L1019
.L1333:
	movl	-360(%rbp), %r13d
	movq	-352(%rbp), %r14
	movl	-416(%rbp), %ebx
.L1018:
	movq	-312(%rbp), %rax
	subq	$1, %rax
	movq	%rax, -352(%rbp)
.L824:
	movq	-352(%rbp), %rax
	leaq	-312(%rbp), %rdi
	movq	(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, -440(%rbp)
	addq	%r15, %rax
	movq	%rax, -352(%rbp)
	cmpl	%ebx, -364(%rbp)
	jbe	.L1103
	.p2align 4,,10
	.p2align 3
.L1104:
	movq	-344(%rbp), %r12
	testl	%r13d, %r13d
	je	.L1023
	xorl	%eax, %eax
	movl	$1, %edx
	rep bsfl	%r13d, %eax
	movl	%eax, %ecx
	sall	%cl, %edx
	leal	1(%rax), %ecx
	notl	%edx
	andl	%edx, %r13d
	movl	$1, %edx
	sall	%cl, %edx
	cmpl	$31, %eax
	je	.L1369
.L1026:
	testl	%edx, %r13d
	je	.L1104
	sall	$3, %eax
	leaq	-160(%rbp), %rdi
	addq	%rax, %r12
	leaq	1(%r12), %rax
	movq	%rax, -160(%rbp)
	movq	(%r12), %r15
	movq	%r15, %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	leaq	-8(%r12,%rax), %rcx
	cmpq	%rcx, %r12
	je	.L1028
	subl	-368(%rbp), %ecx
	movl	%ecx, %esi
	shrl	$3, %ecx
	shrl	$8, %esi
	andl	$31, %ecx
	cmpl	%ebx, %esi
	je	.L1029
	movl	%esi, %eax
	subl	%ebx, %eax
	movq	-336(%rbp), %rbx
	sall	$8, %eax
	addq	%rax, %r14
	movl	%esi, %eax
	movq	%r14, -344(%rbp)
	movl	(%rbx,%rax,4), %r13d
.L1029:
	movl	$-2, %eax
	movl	%esi, %ebx
	sall	%cl, %eax
	andl	%eax, %r13d
.L1028:
	movq	-160(%rbp), %r12
	testq	%r12, %r12
	je	.L1104
	cmpq	%r15, -384(%rbp)
	sete	%cl
	cmpq	%r15, -376(%rbp)
	sete	%al
	orb	%al, %cl
	jne	.L1104
	cmpq	%r15, -392(%rbp)
	je	.L1104
	testl	%r13d, %r13d
	jne	.L1025
	addq	$256, %r14
	addl	$1, %ebx
	cmpl	%ebx, -364(%rbp)
	je	.L1025
	movq	-336(%rbp), %rdi
	movl	%ebx, %eax
	movq	%r14, -344(%rbp)
	movl	(%rdi,%rax,4), %r13d
	.p2align 4,,10
	.p2align 3
.L1025:
	cmpq	%r12, -432(%rbp)
	jne	.L821
	jmp	.L822
	.p2align 4,,10
	.p2align 3
.L1369:
	addq	$256, %r14
	addl	$1, %ebx
	cmpl	%ebx, -364(%rbp)
	je	.L1103
	movq	-336(%rbp), %rdi
	movl	%ebx, %edx
	movq	%r14, -344(%rbp)
	movl	(%rdi,%rdx,4), %r13d
	movl	$1, %edx
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1023:
	addq	$256, %r14
	addl	$1, %ebx
	cmpl	%ebx, -364(%rbp)
	je	.L1083
	movq	-336(%rbp), %rdi
	movl	%ebx, %eax
	movl	(%rdi,%rax,4), %r13d
	jbe	.L1370
	movq	%r14, -344(%rbp)
	jmp	.L1104
.L1370:
	movq	%r14, %r12
	.p2align 4,,10
	.p2align 3
.L1083:
	movq	%r12, -344(%rbp)
	.p2align 4,,10
	.p2align 3
.L1103:
	xorl	%r12d, %r12d
	cmpq	%r12, -432(%rbp)
	jne	.L821
	jmp	.L822
	.p2align 4,,10
	.p2align 3
.L830:
	movq	-400(%rbp), %rax
	movq	112(%rax), %rax
	testq	%rax, %rax
	je	.L924
	movq	-400(%rbp), %rdi
	movq	-352(%rbp), %rcx
	movq	%rax, %r11
	movq	%r15, %rax
	subq	%rdi, %rax
	subq	%rdi, %rcx
	cmpq	$262143, %rax
	jbe	.L1371
	movq	%rcx, %rdx
	leaq	-1(%rax), %rsi
	movl	%ecx, %edi
	movl	%eax, %r12d
	shrq	$18, %rdx
	shrq	$18, %rsi
	andl	$262143, %edi
	movl	%edx, -416(%rbp)
	movslq	%edx, %rdx
	leaq	(%rdx,%rdx,2), %rax
	movl	%edi, -360(%rbp)
	movslq	%esi, %rdi
	salq	$7, %rax
	movq	%rdi, -504(%rbp)
	movq	%rax, -464(%rbp)
	movl	%esi, -448(%rbp)
	sall	$18, %esi
	subl	%esi, %r12d
.L1081:
	movq	-464(%rbp), %r10
	movl	-448(%rbp), %edi
	addq	%r11, %r10
	cmpl	%edi, -416(%rbp)
	je	.L1372
	movl	-360(%rbp), %r9d
	sarl	$13, %r9d
	movslq	%r9d, %rdi
	leaq	0(,%rdi,8), %rax
	movq	%rax, -464(%rbp)
	movq	(%r10,%rdi,8), %rax
	movq	%rax, -512(%rbp)
	testq	%rax, %rax
	jne	.L964
.L969:
	cmpl	$31, %r9d
	je	.L965
	movq	-464(%rbp), %rax
	leaq	8(%r10,%rax), %rdx
	movl	$30, %eax
	subl	%r9d, %eax
	addq	%rax, %rdi
	leaq	16(%r10,%rdi,8), %rsi
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L1373:
	addq	$8, %rdx
	cmpq	%rdx, %rsi
	je	.L965
.L974:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L1373
	leaq	128(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L976:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rcx
	jne	.L976
	addq	$8, %rdx
	cmpq	%rdx, %rsi
	jne	.L974
.L965:
	movq	256(%r10), %rax
	movl	-416(%rbp), %eax
	addl	$1, %eax
	cmpl	-448(%rbp), %eax
	jge	.L971
	cltq
	movslq	-416(%rbp), %rcx
	leaq	(%rax,%rax,2), %rax
	salq	$7, %rax
	leaq	256(%r11,%rax), %rdx
	movl	-448(%rbp), %eax
	subl	$2, %eax
	subl	%ecx, %eax
	addq	%rcx, %rax
	leaq	(%rax,%rax,2), %rax
	salq	$7, %rax
	leaq	1024(%r11,%rax), %rsi
	.p2align 4,,10
	.p2align 3
.L989:
	movq	-256(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.L982
.L984:
	leaq	-248(%rdx), %rcx
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L1375:
	addq	$8, %rcx
	cmpq	%rcx, %rdx
	je	.L1374
.L979:
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L1375
	leaq	128(%rax), %rdi
	.p2align 4,,10
	.p2align 3
.L988:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdi
	jne	.L988
	addq	$8, %rcx
	cmpq	%rcx, %rdx
	jne	.L979
.L1374:
	movq	(%rdx), %rax
	addq	$384, %rdx
	cmpq	%rdx, %rsi
	jne	.L989
.L971:
	movq	-504(%rbp), %rax
	leaq	(%rax,%rax,2), %rsi
	salq	$7, %rsi
	addq	%r11, %rsi
	cmpl	$262144, %r12d
	jg	.L888
	movl	%r12d, %ecx
	movl	%r12d, %r8d
	movl	%r12d, %edi
	movl	$1, %r11d
	sarl	$3, %ecx
	sarl	$13, %r8d
	sarl	$8, %edi
	sall	%cl, %r11d
	movl	%r8d, %eax
	andl	$31, %edi
	movl	%r11d, %r9d
	negl	%r9d
	orl	%edi, %eax
	je	.L1376
	movq	(%rsi), %rdx
	testq	%rdx, %rdx
	jne	.L994
	testl	%r8d, %r8d
	jg	.L1064
.L1332:
	xorl	%eax, %eax
	movl	$1, %r8d
.L997:
	movq	(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L924
	testb	%al, %al
	jne	.L924
	cmpl	%r8d, %edi
	jle	.L1005
	leal	-1(%rdi), %edx
	movslq	%r8d, %rsi
	subl	%r8d, %edx
	leaq	(%rcx,%rsi,4), %rax
	addq	%rsi, %rdx
	leaq	4(%rcx,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L1006:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L1006
.L1005:
	movslq	%edi, %rdi
	subl	$1, %r11d
	leaq	(%rcx,%rdi,4), %rcx
	jmp	.L1004
	.p2align 4,,10
	.p2align 3
.L1377:
	movl	%r9d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L924
.L1004:
	movl	(%rcx), %edx
	testl	%edx, %r11d
	jne	.L1377
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L1012:
	movdqa	-416(%rbp), %xmm1
	movq	-360(%rbp), %rsi
	movaps	%xmm1, -176(%rbp)
	cmpq	%rsi, -352(%rbp)
	jnb	.L1010
	movq	%rsi, %rdx
	jmp	.L1013
	.p2align 4,,10
	.p2align 3
.L827:
	movq	-400(%rbp), %rax
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	%r12d, %edx
	movq	-352(%rbp), %rsi
	movq	24(%rax), %rdi
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	-472(%rbp), %rax
	cmpb	$0, 354(%rax)
	je	.L829
.L1359:
	movq	-352(%rbp), %rsi
	movq	-400(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal11MemoryChunk19DiscardUnusedMemoryEmm@PLT
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L1367:
	movl	-368(%rbp), %edi
	movl	-352(%rbp), %eax
	leaq	-160(%rbp), %rsi
	subl	%edi, %eax
	movl	%eax, -160(%rbp)
	movl	%r15d, %eax
	subl	%edi, %eax
	leaq	-304(%rbp), %rdi
	movl	%eax, -156(%rbp)
	call	_ZNSt8_Rb_treeIjSt4pairIKjjESt10_Select1stIS2_ESt4lessIjESaIS2_EE17_M_emplace_uniqueIJS0_IjjEEEES0_ISt17_Rb_tree_iteratorIS2_EbEDpOT_
	jmp	.L921
	.p2align 4,,10
	.p2align 3
.L1358:
	movq	-352(%rbp), %rdi
	movq	%r12, %rdx
	movl	$204, %esi
	call	memset@PLT
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L892:
	movl	%edi, %eax
	xorl	%r9d, %r9d
	lock cmpxchgl	%r9d, (%rdx)
	cmpl	%eax, %edi
	je	.L895
.L894:
	movl	(%rdx), %edi
	testl	%edi, %edi
	jne	.L892
.L895:
	leaq	4(%rdx), %rax
	subq	$-128, %rdx
	.p2align 4,,10
	.p2align 3
.L893:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L893
	jmp	.L896
	.p2align 4,,10
	.p2align 3
.L980:
	movl	%edi, %eax
	xorl	%r10d, %r10d
	lock cmpxchgl	%r10d, (%rcx)
	cmpl	%eax, %edi
	je	.L983
.L982:
	movl	(%rcx), %edi
	testl	%edi, %edi
	jne	.L980
.L983:
	leaq	4(%rcx), %rax
	subq	$-128, %rcx
	.p2align 4,,10
	.p2align 3
.L981:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rcx
	jne	.L981
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1360:
	movl	%ecx, %edx
	movl	%ecx, %r10d
	sarl	$3, %ecx
	movl	%eax, %r9d
	sarl	$8, %edx
	sarl	$13, %r10d
	movl	%edx, %edi
	movl	%eax, %edx
	sarl	$13, %r9d
	sarl	$8, %edx
	andl	$31, %edi
	movl	%edx, %esi
	movl	$1, %edx
	movl	%edi, -416(%rbp)
	sall	%cl, %edx
	movl	%eax, %ecx
	movl	$1, %eax
	andl	$31, %esi
	sarl	$3, %ecx
	movl	%edx, -528(%rbp)
	leal	-1(%rdx), %r8d
	sall	%cl, %eax
	movl	%esi, -360(%rbp)
	movl	%eax, %r12d
	movl	%eax, -504(%rbp)
	negl	%r12d
	cmpl	%r9d, %r10d
	sete	%dl
	cmpl	%esi, %edi
	movslq	%r10d, %rdi
	sete	%al
	movq	%rdi, -520(%rbp)
	salq	$3, %rdi
	movq	%rdi, -448(%rbp)
	addq	%r11, %rdi
	andb	%al, %dl
	movb	%dl, -512(%rbp)
	movq	(%rdi), %rax
	jne	.L1378
	movq	%rax, -536(%rbp)
	movl	-416(%rbp), %esi
	addl	$1, %esi
	movl	%esi, -464(%rbp)
	testq	%rax, %rax
	jne	.L1379
	cmpl	%r9d, %r10d
	jl	.L841
.L1330:
	movl	%r10d, %eax
.L845:
	movq	(%rdi), %rdx
	cmpl	$32, %eax
	je	.L833
	testq	%rdx, %rdx
	je	.L833
	movl	-464(%rbp), %edi
	cmpl	%edi, -360(%rbp)
	jle	.L850
	movl	-360(%rbp), %ecx
	movslq	%edi, %rsi
	leaq	(%rdx,%rsi,4), %rax
	subl	$1, %ecx
	subl	%edi, %ecx
	addq	%rsi, %rcx
	leaq	4(%rdx,%rcx,4), %rcx
	.p2align 4,,10
	.p2align 3
.L851:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rcx
	jne	.L851
.L850:
	movl	-504(%rbp), %eax
	leal	-1(%rax), %edi
	movslq	-360(%rbp), %rax
	leaq	(%rdx,%rax,4), %rcx
	jmp	.L849
	.p2align 4,,10
	.p2align 3
.L1380:
	movl	%r12d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L833
.L849:
	movl	(%rcx), %edx
	testl	%edx, %edi
	jne	.L1380
.L833:
	movq	-400(%rbp), %rax
	movq	112(%rax), %rax
	movq	%rax, %r11
	testq	%rax, %rax
	je	.L924
.L1080:
	movq	-448(%rbp), %rdi
	addq	%r11, %rdi
	cmpb	$0, -512(%rbp)
	movq	(%rdi), %rax
	jne	.L1381
	movq	%rax, -464(%rbp)
	movl	-416(%rbp), %esi
	addl	$1, %esi
	movl	%esi, -448(%rbp)
	testq	%rax, %rax
	jne	.L1382
	cmpl	%r9d, %r10d
	jl	.L932
.L936:
	movq	(%rdi), %rdx
	cmpl	$32, %r10d
	je	.L924
	testq	%rdx, %rdx
	je	.L924
	movl	-448(%rbp), %edi
	cmpl	%edi, -360(%rbp)
	jle	.L941
	movl	-360(%rbp), %ecx
	movslq	%edi, %rsi
	leaq	(%rdx,%rsi,4), %rax
	subl	$1, %ecx
	subl	%edi, %ecx
	addq	%rsi, %rcx
	leaq	4(%rdx,%rcx,4), %rcx
	.p2align 4,,10
	.p2align 3
.L942:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rcx
	jne	.L942
.L941:
	movl	-504(%rbp), %edi
	movslq	-360(%rbp), %rax
	subl	$1, %edi
	leaq	(%rdx,%rax,4), %rcx
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L1383:
	movl	%r12d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L924
.L940:
	movl	(%rcx), %edx
	testl	%edx, %edi
	jne	.L1383
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L1339:
	movq	-400(%rbp), %rsi
	leaq	-160(%rbp), %rdi
	leaq	-232(%rbp), %rbx
	call	_ZN2v88internal23InvalidatedSlotsCleanup8OldToNewEPNS0_11MemoryChunkE@PLT
	movq	-160(%rbp), %rax
	movq	-216(%rbp), %r12
	movq	%rax, -256(%rbp)
	movq	-152(%rbp), %rax
	movq	%rax, -248(%rbp)
	movq	-144(%rbp), %rax
	movq	%rax, -240(%rbp)
	testq	%r12, %r12
	je	.L793
.L790:
	movq	24(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r12, %rdi
	movq	16(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L790
.L793:
	movq	-120(%rbp), %rdx
	leaq	-224(%rbp), %rax
	movq	$0, -216(%rbp)
	movq	%rax, -208(%rbp)
	movq	%rax, -200(%rbp)
	movq	$0, -192(%rbp)
	testq	%rdx, %rdx
	je	.L1384
	movl	-128(%rbp), %ecx
	movq	%rdx, -216(%rbp)
	movl	%ecx, -224(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rcx, -208(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -200(%rbp)
	movq	%rax, 8(%rdx)
	movq	-96(%rbp), %rax
	movdqu	-88(%rbp), %xmm3
	movq	%rax, -192(%rbp)
	movq	-72(%rbp), %rax
	movups	%xmm3, -184(%rbp)
	movq	%rax, -168(%rbp)
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L1297:
	movl	-504(%rbp), %r13d
	movq	-512(%rbp), %r12
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L1361:
	cmpl	$262144, %r12d
	jg	.L888
	movl	-360(%rbp), %edi
	movl	%r12d, %r9d
	sarl	$13, %r9d
	movl	%edi, %eax
	movl	%edi, %r8d
	sarl	$3, %edi
	sarl	$8, %eax
	movl	%edi, %ecx
	movl	$1, %edi
	sarl	$13, %r8d
	andl	$31, %eax
	sall	%cl, %edi
	movl	%r12d, %ecx
	movl	%eax, %esi
	movl	%eax, -536(%rbp)
	movl	%r12d, %eax
	sarl	$3, %ecx
	sarl	$8, %eax
	leal	-1(%rdi), %edx
	andl	$31, %eax
	movl	%edx, -496(%rbp)
	movl	%eax, -512(%rbp)
	movl	$1, %eax
	sall	%cl, %eax
	movl	%eax, -552(%rbp)
	movl	%eax, %r11d
	movl	-512(%rbp), %eax
	negl	%r11d
	cmpl	%r9d, %r8d
	sete	%dl
	cmpl	%eax, %esi
	movslq	%r8d, %rsi
	sete	%al
	movq	%rsi, -576(%rbp)
	salq	$3, %rsi
	movq	%rsi, -560(%rbp)
	addq	%r10, %rsi
	andb	%al, %dl
	movq	%rsi, -520(%rbp)
	movb	%dl, -544(%rbp)
	movq	(%rsi), %rax
	jne	.L1385
	movq	%rax, -568(%rbp)
	movl	-536(%rbp), %esi
	addl	$1, %esi
	movl	%esi, -528(%rbp)
	testq	%rax, %rax
	jne	.L1386
	cmpl	%r9d, %r8d
	jl	.L863
.L870:
	movq	-520(%rbp), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L855
	cmpb	$0, -544(%rbp)
	jne	.L855
	movl	-528(%rbp), %edi
	cmpl	%edi, -512(%rbp)
	jle	.L873
	movl	-512(%rbp), %ecx
	movslq	%edi, %rsi
	leaq	(%rdx,%rsi,4), %rax
	subl	$1, %ecx
	subl	%edi, %ecx
	addq	%rsi, %rcx
	leaq	4(%rdx,%rcx,4), %rcx
	.p2align 4,,10
	.p2align 3
.L874:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rcx
	jne	.L874
.L873:
	movl	-552(%rbp), %ecx
	movslq	-512(%rbp), %rax
	subl	$1, %ecx
	leaq	(%rdx,%rax,4), %rsi
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L1387:
	movl	%r11d, %edi
	movl	%edx, %eax
	andl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L855
.L872:
	movl	(%rsi), %edx
	testl	%edx, %ecx
	jne	.L1387
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L1357:
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1372:
	cmpl	$262144, %r12d
	jg	.L888
	movl	-360(%rbp), %edi
	movl	%r12d, %esi
	movl	%r12d, %r9d
	sarl	$8, %esi
	sarl	$13, %r9d
	andl	$31, %esi
	movl	%edi, %r11d
	movl	%edi, %eax
	sarl	$3, %edi
	movl	%esi, -360(%rbp)
	movl	%edi, %ecx
	sarl	$8, %eax
	movl	$1, %esi
	sall	%cl, %esi
	andl	$31, %eax
	movl	%r12d, %ecx
	sarl	$13, %r11d
	movl	%eax, -416(%rbp)
	sarl	$3, %ecx
	movl	$1, %eax
	leal	-1(%rsi), %r8d
	sall	%cl, %eax
	movl	-360(%rbp), %edi
	movl	%eax, %r12d
	movl	%eax, -512(%rbp)
	negl	%r12d
	cmpl	%r9d, %r11d
	sete	%dl
	cmpl	%edi, -416(%rbp)
	movslq	%r11d, %rdi
	sete	%al
	movq	%rdi, -536(%rbp)
	salq	$3, %rdi
	movq	%rdi, -520(%rbp)
	addq	%r10, %rdi
	andb	%al, %dl
	movq	%rdi, -448(%rbp)
	movb	%dl, -504(%rbp)
	movq	(%rdi), %rax
	jne	.L1388
	movq	%rax, -528(%rbp)
	movl	-416(%rbp), %edi
	addl	$1, %edi
	movl	%edi, -464(%rbp)
	testq	%rax, %rax
	jne	.L1389
	cmpl	%r9d, %r11d
	jl	.L952
.L959:
	movq	-448(%rbp), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L924
	cmpb	$0, -504(%rbp)
	jne	.L924
	movl	-360(%rbp), %edi
	cmpl	%edi, -464(%rbp)
	jge	.L962
	movslq	-464(%rbp), %rsi
	movl	%edi, %ecx
	subl	$1, %ecx
	subl	%esi, %ecx
	leaq	(%rdx,%rsi,4), %rax
	addq	%rsi, %rcx
	leaq	4(%rdx,%rcx,4), %rcx
	.p2align 4,,10
	.p2align 3
.L963:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rcx
	jne	.L963
.L962:
	movl	-512(%rbp), %edi
	movslq	-360(%rbp), %rax
	subl	$1, %edi
	leaq	(%rdx,%rax,4), %rcx
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L1390:
	movl	%r12d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L924
.L961:
	movl	(%rcx), %edx
	testl	%edx, %edi
	jne	.L1390
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L1391:
	movl	%ecx, %eax
	xorl	%r9d, %r9d
	lock cmpxchgl	%r9d, (%rdx)
	cmpl	%eax, %ecx
	je	.L905
.L906:
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L1391
.L905:
	leaq	4(%rdx), %rax
	subq	$-128, %rdx
	testl	%edi, %edi
	jle	.L1331
	.p2align 4,,10
	.p2align 3
.L907:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L907
.L1069:
	cmpl	$1, %edi
	jle	.L1392
	leal	-2(%rdi), %eax
	leaq	8(%rsi), %rdx
	leaq	16(%rsi,%rax,8), %r9
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L913:
	addq	$8, %rdx
	cmpq	%rdx, %r9
	je	.L1393
.L912:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L913
	leaq	128(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L914:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rcx
	jne	.L914
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L1394:
	movl	%ecx, %eax
	xorl	%r10d, %r10d
	lock cmpxchgl	%r10d, (%rdx)
	cmpl	%eax, %ecx
	je	.L993
.L994:
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L1394
.L993:
	leaq	4(%rdx), %rax
	subq	$-128, %rdx
	testl	%r8d, %r8d
	jle	.L1332
	.p2align 4,,10
	.p2align 3
.L995:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L995
.L1064:
	cmpl	$1, %r8d
	jle	.L1395
	leal	-2(%r8), %eax
	leaq	8(%rsi), %rdx
	leaq	16(%rsi,%rax,8), %r10
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L1001:
	addq	$8, %rdx
	cmpq	%rdx, %r10
	je	.L1396
.L1000:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L1001
	leaq	128(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L1002:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rcx
	jne	.L1002
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1379:
	movslq	-416(%rbp), %rsi
	movl	-528(%rbp), %eax
	movl	%r10d, -544(%rbp)
	movq	-536(%rbp), %rcx
	movq	%rsi, -552(%rbp)
	negl	%eax
	salq	$2, %rsi
	movq	%rsi, -560(%rbp)
	addq	%rsi, %rcx
	movl	%eax, %r10d
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L1397:
	movl	%r8d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L1299
.L837:
	movl	(%rcx), %edx
	testl	%edx, %r10d
	jne	.L1397
.L1299:
	movl	-544(%rbp), %r10d
	cmpl	%r9d, %r10d
	jge	.L1330
	cmpl	$32, -464(%rbp)
	je	.L841
	movq	-536(%rbp), %rdi
	movq	-560(%rbp), %rax
	movl	$30, %edx
	subl	-416(%rbp), %edx
	addq	-552(%rbp), %rdx
	leaq	4(%rdi,%rax), %rax
	leaq	8(%rdi,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L842:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L842
.L841:
	leal	1(%r10), %eax
	cmpl	%eax, %r9d
	jle	.L1398
	movq	-448(%rbp), %rax
	leaq	8(%r11,%rax), %rdx
	movl	%r9d, %eax
	subl	%r10d, %eax
	subl	$2, %eax
	addq	-520(%rbp), %rax
	leaq	16(%r11,%rax,8), %rsi
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L1400:
	addq	$8, %rdx
	cmpq	%rdx, %rsi
	je	.L1399
.L844:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L1400
	leaq	128(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L847:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rcx
	jne	.L847
	addq	$8, %rdx
	cmpq	%rdx, %rsi
	jne	.L844
.L1399:
	movl	$0, -464(%rbp)
	movslq	%r9d, %rax
	leaq	(%r11,%rax,8), %rdi
	movl	%r9d, %eax
	jmp	.L845
.L1386:
	movslq	-536(%rbp), %rax
	movq	-568(%rbp), %rcx
	movl	%r13d, -580(%rbp)
	negl	%edi
	movl	-496(%rbp), %r13d
	movq	%rax, -592(%rbp)
	salq	$2, %rax
	movq	%rax, -600(%rbp)
	addq	%rax, %rcx
	jmp	.L859
	.p2align 4,,10
	.p2align 3
.L1401:
	movl	%r13d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L1302
.L859:
	movl	(%rcx), %edx
	testl	%edx, %edi
	jne	.L1401
.L1302:
	movl	-580(%rbp), %r13d
	cmpl	%r9d, %r8d
	jge	.L870
	cmpl	$32, -528(%rbp)
	je	.L863
	movq	-568(%rbp), %rdi
	movq	-600(%rbp), %rax
	movl	$30, %edx
	subl	-536(%rbp), %edx
	addq	-592(%rbp), %rdx
	leaq	4(%rdi,%rax), %rax
	leaq	8(%rdi,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L864:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L864
.L863:
	leal	1(%r8), %eax
	cmpl	%eax, %r9d
	jle	.L1402
	movq	-560(%rbp), %rax
	leaq	8(%r10,%rax), %rdx
	movl	%r9d, %eax
	subl	%r8d, %eax
	subl	$2, %eax
	addq	-576(%rbp), %rax
	leaq	16(%r10,%rax,8), %rsi
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L868:
	addq	$8, %rdx
	cmpq	%rdx, %rsi
	je	.L1403
.L867:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L868
	leaq	128(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L869:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rcx
	jne	.L869
	jmp	.L868
	.p2align 4,,10
	.p2align 3
.L888:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1042:
	movdqa	-336(%rbp), %xmm4
	movaps	%xmm4, -176(%rbp)
	cmpq	%r13, -352(%rbp)
	jnb	.L1040
	movq	%rdi, %r14
	movq	%r13, %r15
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1371:
	movl	%ecx, %edx
	movl	%ecx, %r10d
	sarl	$3, %ecx
	movl	%eax, %r9d
	sarl	$8, %edx
	sarl	$13, %r10d
	movl	%edx, %edi
	movl	%eax, %edx
	sarl	$13, %r9d
	sarl	$8, %edx
	andl	$31, %edi
	movl	%edx, %esi
	movl	$1, %edx
	movl	%edi, -416(%rbp)
	sall	%cl, %edx
	movl	%eax, %ecx
	movl	$1, %eax
	andl	$31, %esi
	sarl	$3, %ecx
	movl	%edx, -528(%rbp)
	leal	-1(%rdx), %r8d
	sall	%cl, %eax
	movl	%esi, -360(%rbp)
	movl	%eax, %r12d
	movl	%eax, -504(%rbp)
	negl	%r12d
	cmpl	%r9d, %r10d
	sete	%dl
	cmpl	%esi, %edi
	sete	%al
	andl	%eax, %edx
	movslq	%r10d, %rax
	movq	%rax, -520(%rbp)
	salq	$3, %rax
	movb	%dl, -512(%rbp)
	movq	%rax, -448(%rbp)
	jmp	.L1080
	.p2align 4,,10
	.p2align 3
.L875:
	movl	-360(%rbp), %esi
	movl	%r13d, -552(%rbp)
	movq	%r11, -560(%rbp)
	movl	%esi, %eax
	sarl	$3, %esi
	sarl	$8, %eax
	movl	%eax, %ecx
	andl	$31, %ecx
	movl	%ecx, -544(%rbp)
	movl	%esi, %ecx
	movl	$1, %esi
	sall	%cl, %esi
	movl	%esi, %ecx
	leal	-1(%rsi), %r8d
	movl	%eax, %esi
	andl	$31, %esi
	negl	%ecx
	movl	%r8d, %r13d
	leaq	0(,%rsi,4), %rax
	movq	%rsi, -568(%rbp)
	movl	%ecx, %r11d
	movq	-536(%rbp), %rsi
	movq	%rax, -576(%rbp)
	addq	%rax, %rsi
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L1404:
	movl	%r13d, %r8d
	movl	%ecx, %eax
	andl	%ecx, %r8d
	lock cmpxchgl	%r8d, (%rsi)
	cmpl	%eax, %ecx
	je	.L1304
.L879:
	movl	(%rsi), %ecx
	testl	%ecx, %r11d
	jne	.L1404
.L1304:
	cmpl	$31, -544(%rbp)
	movl	-552(%rbp), %r13d
	movq	-560(%rbp), %r11
	je	.L880
	movq	-536(%rbp), %rsi
	movq	-576(%rbp), %rax
	movl	$30, %ecx
	subl	-544(%rbp), %ecx
	addq	-568(%rbp), %rcx
	leaq	4(%rsi,%rax), %rax
	leaq	8(%rsi,%rcx,4), %rcx
	.p2align 4,,10
	.p2align 3
.L881:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rcx
	jne	.L881
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L1382:
	movl	-528(%rbp), %eax
	movslq	-416(%rbp), %rsi
	movl	%r10d, -512(%rbp)
	movq	-464(%rbp), %rcx
	movq	%rsi, -528(%rbp)
	negl	%eax
	salq	$2, %rsi
	movq	%rsi, -536(%rbp)
	addq	%rsi, %rcx
	movl	%eax, %r10d
	jmp	.L928
	.p2align 4,,10
	.p2align 3
.L1405:
	movl	%edx, %esi
	movl	%edx, %eax
	andl	%r8d, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L1310
.L928:
	movl	(%rcx), %edx
	testl	%edx, %r10d
	jne	.L1405
.L1310:
	movl	-512(%rbp), %r10d
	cmpl	%r9d, %r10d
	jge	.L936
	cmpl	$32, -448(%rbp)
	je	.L932
	movq	-464(%rbp), %rdi
	movq	-536(%rbp), %rax
	movl	$30, %edx
	subl	-416(%rbp), %edx
	addq	-528(%rbp), %rdx
	leaq	4(%rdi,%rax), %rax
	leaq	8(%rdi,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L933:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L933
.L932:
	leal	1(%r10), %esi
	cmpl	%esi, %r9d
	jle	.L1406
	movslq	%esi, %rax
	leaq	(%r11,%rax,8), %rdx
	leal	-2(%r9), %eax
	subl	%r10d, %eax
	addq	-520(%rbp), %rax
	leaq	16(%r11,%rax,8), %rdi
	jmp	.L935
	.p2align 4,,10
	.p2align 3
.L1408:
	addq	$8, %rdx
	cmpq	%rdx, %rdi
	je	.L1407
.L935:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L1408
	leaq	128(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L938:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rcx, %rax
	jne	.L938
	addq	$8, %rdx
	cmpq	%rdx, %rdi
	jne	.L935
.L1407:
	movl	$0, -448(%rbp)
	leal	-1(%r9,%rsi), %eax
	subl	%r10d, %eax
	movl	%eax, %r10d
	cltq
	leaq	(%r11,%rax,8), %rdi
	jmp	.L936
	.p2align 4,,10
	.p2align 3
.L964:
	movl	-360(%rbp), %esi
	movl	$1, %r8d
	movl	%r13d, -520(%rbp)
	movl	%esi, %eax
	sarl	$3, %esi
	sarl	$8, %eax
	movl	%esi, %ecx
	movl	%eax, %edx
	movl	%eax, %esi
	sall	%cl, %r8d
	movq	-512(%rbp), %rcx
	andl	$31, %edx
	andl	$31, %esi
	leaq	0(,%rsi,4), %rax
	movl	%edx, -360(%rbp)
	leal	-1(%r8), %edx
	negl	%r8d
	movq	%rsi, -528(%rbp)
	addq	%rax, %rcx
	movl	%edx, %r13d
	movq	%rax, -536(%rbp)
	jmp	.L968
	.p2align 4,,10
	.p2align 3
.L1409:
	movl	%r13d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L1315
.L968:
	movl	(%rcx), %edx
	testl	%edx, %r8d
	jne	.L1409
.L1315:
	cmpl	$31, -360(%rbp)
	movl	-520(%rbp), %r13d
	je	.L969
	movq	-512(%rbp), %rsi
	movq	-536(%rbp), %rax
	movl	$30, %edx
	subl	-360(%rbp), %edx
	addq	-528(%rbp), %rdx
	leaq	4(%rsi,%rax), %rax
	leaq	8(%rsi,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L970:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L970
	jmp	.L969
	.p2align 4,,10
	.p2align 3
.L1384:
	movdqu	-88(%rbp), %xmm6
	movq	-72(%rbp), %rax
	movups	%xmm6, -184(%rbp)
	movq	%rax, -168(%rbp)
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L1403:
	movslq	%r9d, %rax
	cmpl	$32, %r9d
	movl	$0, -528(%rbp)
	leaq	(%r10,%rax,8), %rax
	sete	-544(%rbp)
	movq	%rax, -520(%rbp)
	jmp	.L870
	.p2align 4,,10
	.p2align 3
.L1393:
	movslq	%edi, %rax
	cmpl	$32, %edi
	leaq	(%rsi,%rax,8), %rsi
	sete	%al
	xorl	%edi, %edi
	jmp	.L909
.L1389:
	movslq	-416(%rbp), %rax
	movq	-528(%rbp), %rcx
	negl	%esi
	movq	%rax, -544(%rbp)
	salq	$2, %rax
	movq	%rax, -552(%rbp)
	addq	%rax, %rcx
	jmp	.L948
	.p2align 4,,10
	.p2align 3
.L1410:
	movl	%r8d, %edi
	movl	%edx, %eax
	andl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	je	.L947
.L948:
	movl	(%rcx), %edx
	testl	%edx, %esi
	jne	.L1410
.L947:
	cmpl	%r9d, %r11d
	jge	.L959
	cmpl	$32, -464(%rbp)
	je	.L952
	movq	-528(%rbp), %rdi
	movq	-552(%rbp), %rax
	movl	$30, %edx
	subl	-416(%rbp), %edx
	addq	-544(%rbp), %rdx
	leaq	4(%rdi,%rax), %rax
	leaq	8(%rdi,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L953:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L953
.L952:
	leal	1(%r11), %edx
	cmpl	%edx, %r9d
	jle	.L1411
	movq	-520(%rbp), %rax
	leaq	8(%r10,%rax), %rcx
	leal	-2(%r9), %eax
	subl	%r11d, %eax
	addq	-536(%rbp), %rax
	leaq	16(%r10,%rax,8), %rdi
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L957:
	addq	$8, %rcx
	cmpq	%rcx, %rdi
	je	.L1412
.L956:
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L957
	leaq	128(%rax), %rsi
	.p2align 4,,10
	.p2align 3
.L958:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rsi
	jne	.L958
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L1412:
	movl	$0, -464(%rbp)
	leal	-1(%r9,%rdx), %eax
	subl	%r11d, %eax
	movslq	%eax, %rdx
	cmpl	$32, %eax
	leaq	(%r10,%rdx,8), %rdi
	sete	-504(%rbp)
	movq	%rdi, -448(%rbp)
	jmp	.L959
	.p2align 4,,10
	.p2align 3
.L1396:
	movslq	%r8d, %rax
	cmpl	$32, %r8d
	leaq	(%rsi,%rax,8), %rsi
	sete	%al
	xorl	%r8d, %r8d
	jmp	.L997
.L1348:
	movq	-400(%rbp), %rbx
	movq	40(%rbx), %rax
	addq	192(%rbx), %rax
	movq	$0, 96(%rbx)
	subq	48(%rbx), %rax
	addq	-440(%rbp), %rax
	movq	%rax, 192(%rbx)
	movq	$0, 168(%rbx)
	mfence
	cmpq	$0, -456(%rbp)
	je	.L1334
	movq	-456(%rbp), %rdi
	call	_ZN2v88internal18CodeObjectRegistry8FinalizeEv@PLT
.L1334:
	xorl	%r13d, %r13d
	jmp	.L1055
.L1347:
	movq	-400(%rbp), %rax
	movq	120(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L1413
.L1051:
	movq	-400(%rbp), %rax
	movq	128(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1050
	leaq	-304(%rbp), %r12
	movq	%r12, %rsi
	call	_ZN2v88internal12TypedSlotSet17ClearInvalidSlotsERKSt3mapIjjSt4lessIjESaISt4pairIKjjEEE@PLT
	jmp	.L1050
.L1343:
	movq	-488(%rbp), %rbx
	movq	-352(%rbp), %r15
	movl	$1, %ecx
	movl	%r12d, %edx
	movl	$1, %r8d
	movq	64(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	96(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	movl	$1, %ecx
	movq	%r12, %rbx
	movq	(%rdi), %rax
	call	*24(%rax)
	subq	%rax, %rbx
	movq	%rbx, %rax
	movq	-424(%rbp), %rbx
	cmpq	%rax, %rbx
	cmovge	%rbx, %rax
	movq	%rax, -424(%rbp)
	jmp	.L1035
.L1413:
	leaq	-304(%rbp), %r12
	movq	%r12, %rsi
	call	_ZN2v88internal12TypedSlotSet17ClearInvalidSlotsERKSt3mapIjjSt4lessIjESaISt4pairIKjjEEE@PLT
	jmp	.L1051
.L1365:
	movq	(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L855
	subl	$1, %r11d
	jmp	.L903
.L1414:
	movl	%r8d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L855
.L903:
	movl	(%rcx), %edx
	testl	%r11d, %edx
	jne	.L1414
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L1345:
	movq	-400(%rbp), %rbx
	movl	-352(%rbp), %edx
	leaq	-304(%rbp), %r12
	leaq	-160(%rbp), %rsi
	movq	%r12, %rdi
	movq	48(%rbx), %rax
	subl	%ebx, %edx
	movl	%edx, -160(%rbp)
	movq	%rax, -336(%rbp)
	movl	-336(%rbp), %eax
	subl	%ebx, %eax
	movl	%eax, -156(%rbp)
	call	_ZNSt8_Rb_treeIjSt4pairIKjjESt10_Select1stIS2_ESt4lessIjESaIS2_EE17_M_emplace_uniqueIJS0_IjjEEEES0_ISt17_Rb_tree_iteratorIS2_EbEDpOT_
	jmp	.L1037
.L1344:
	movq	-352(%rbp), %rsi
	movq	-400(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal11MemoryChunk19DiscardUnusedMemoryEmm@PLT
	jmp	.L1036
.L1342:
	movq	-352(%rbp), %rdi
	movq	%rax, %rdx
	movl	$204, %esi
	call	memset@PLT
	jmp	.L1034
.L1378:
	testq	%rax, %rax
	je	.L833
	movl	%r8d, %edi
	movslq	-416(%rbp), %rdx
	orl	%r12d, %edi
	movl	%edi, %r11d
	leaq	(%rax,%rdx,4), %rcx
	notl	%r11d
	jmp	.L834
.L1415:
	movl	%edi, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L833
.L834:
	movl	(%rcx), %edx
	testl	%edx, %r11d
	jne	.L1415
	jmp	.L833
.L1381:
	testq	%rax, %rax
	je	.L924
	movslq	-416(%rbp), %rdx
	orl	%r8d, %r12d
	movl	%r12d, %edi
	notl	%edi
	leaq	(%rax,%rdx,4), %rcx
	jmp	.L925
.L1416:
	movl	%r12d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L924
.L925:
	movl	(%rcx), %edx
	testl	%edx, %edi
	jne	.L1416
	jmp	.L924
.L1385:
	testq	%rax, %rax
	je	.L855
	movl	-496(%rbp), %r8d
	movslq	-536(%rbp), %rdx
	orl	%r11d, %r8d
	leaq	(%rax,%rdx,4), %rcx
	movl	%r8d, %edi
	notl	%edi
	jmp	.L856
.L1417:
	movl	%r8d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L855
.L856:
	movl	(%rcx), %edx
	testl	%edx, %edi
	jne	.L1417
	jmp	.L855
.L1341:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1376:
	movq	(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L924
	subl	$1, %r11d
	jmp	.L991
.L1418:
	movl	%r9d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L924
.L991:
	movl	(%rcx), %edx
	testl	%edx, %r11d
	jne	.L1418
	jmp	.L924
.L1388:
	testq	%rax, %rax
	je	.L924
	orl	%r12d, %r8d
	movslq	-416(%rbp), %r12
	movl	%r8d, %edi
	notl	%edi
	leaq	(%rax,%r12,4), %rcx
	jmp	.L945
.L1419:
	movl	%r8d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L924
.L945:
	movl	(%rcx), %edx
	testl	%edx, %edi
	jne	.L1419
	jmp	.L924
.L1398:
	movq	-448(%rbp), %rdi
	movl	$0, -464(%rbp)
	leaq	8(%r11,%rdi), %rdi
	jmp	.L845
.L1406:
	movl	$0, -448(%rbp)
	movslq	%esi, %rax
	movl	%esi, %r10d
	leaq	(%r11,%rax,8), %rdi
	jmp	.L936
.L1402:
	movq	-560(%rbp), %rdi
	cmpl	$32, %eax
	movl	$0, -528(%rbp)
	sete	-544(%rbp)
	leaq	8(%r10,%rdi), %rdi
	movq	%rdi, -520(%rbp)
	jmp	.L870
.L1392:
	addq	$8, %rsi
	xorl	%eax, %eax
	xorl	%edi, %edi
	jmp	.L909
.L1411:
	movq	-520(%rbp), %rax
	cmpl	$32, %edx
	movl	$0, -464(%rbp)
	sete	-504(%rbp)
	leaq	8(%r10,%rax), %rax
	movq	%rax, -448(%rbp)
	jmp	.L959
.L1395:
	addq	$8, %rsi
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	jmp	.L997
.L1349:
	call	__stack_chk_fail@PLT
.L1353:
	movq	-360(%rbp), %rax
	movl	%r15d, -464(%rbp)
	movq	%rax, -344(%rbp)
	jmp	.L1335
	.cfi_endproc
.LFE22367:
	.size	_ZN2v88internal7Sweeper8RawSweepEPNS0_4PageENS1_22FreeListRebuildingModeENS0_22FreeSpaceTreatmentModeENS1_35FreeSpaceMayContainInvalidatedSlotsE, .-_ZN2v88internal7Sweeper8RawSweepEPNS0_4PageENS1_22FreeListRebuildingModeENS0_22FreeSpaceTreatmentModeENS1_35FreeSpaceMayContainInvalidatedSlotsE
	.section	.text._ZN2v88internal7Sweeper8RawSweepEPNS0_4PageENS1_22FreeListRebuildingModeENS0_22FreeSpaceTreatmentModeENS1_35FreeSpaceMayContainInvalidatedSlotsE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal7Sweeper8RawSweepEPNS0_4PageENS1_22FreeListRebuildingModeENS0_22FreeSpaceTreatmentModeENS1_35FreeSpaceMayContainInvalidatedSlotsE.constprop.0, @function
_ZN2v88internal7Sweeper8RawSweepEPNS0_4PageENS1_22FreeListRebuildingModeENS0_22FreeSpaceTreatmentModeENS1_35FreeSpaceMayContainInvalidatedSlotsE.constprop.0:
.LFB29304:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$440, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -392(%rbp)
	movq	%rsi, -304(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	80(%rsi), %rax
	movq	272(%rsi), %rbx
	movq	%rbx, -376(%rbp)
	movq	120(%rsi), %rax
	movb	$1, -393(%rbp)
	testq	%rax, %rax
	je	.L1952
.L1421:
	leaq	-200(%rbp), %rax
	movl	$0, -200(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	movq	-304(%rbp), %rax
	movq	$0, -192(%rbp)
	movq	248(%rax), %rbx
	movq	$0, -168(%rbp)
	testq	%rbx, %rbx
	je	.L1423
	movq	-392(%rbp), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %rsi
	call	_ZN2v88internal23LocalArrayBufferTracker4FreeIZNS0_18ArrayBufferTracker8FreeDeadINS0_26MajorNonAtomicMarkingStateEEEvPNS0_4PageEPT_EUlNS0_13JSArrayBufferEE_EEvS8_
	cmpq	$0, 32(%rbx)
	je	.L1953
.L1423:
	movq	-304(%rbp), %rax
	leaq	-160(%rbp), %rdi
	movq	40(%rax), %rbx
	movq	%rax, %rsi
	movq	%rbx, -256(%rbp)
	movq	%rax, %rbx
	call	_ZN2v88internal23InvalidatedSlotsCleanup9NoCleanupEPNS0_11MemoryChunkE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal4Page25ResetAllocationStatisticsEv@PLT
	movq	-376(%rbp), %rax
	testq	%rax, %rax
	je	.L1425
	movq	%rax, %rdi
	call	_ZN2v88internal18CodeObjectRegistry5ClearEv@PLT
.L1425:
	movq	-304(%rbp), %rsi
	movq	16(%rsi), %rax
	movl	%esi, -272(%rbp)
	movq	%rax, -240(%rbp)
	movq	24(%rsi), %rax
	movq	-37528(%rax), %rbx
	subq	$37592, %rax
	movq	%rbx, -280(%rbp)
	movq	72(%rax), %rbx
	movq	56(%rax), %rax
	movq	%rbx, -288(%rbp)
	movq	%rax, -296(%rbp)
	movq	48(%rsi), %rax
	movq	%rax, -264(%rbp)
	movl	-264(%rbp), %eax
	subl	%esi, %eax
	shrl	$8, %eax
	movl	%eax, -268(%rbp)
	movq	40(%rsi), %rax
	movq	%rax, -264(%rbp)
	movl	-264(%rbp), %eax
	movq	%rsi, -264(%rbp)
	subl	%esi, %eax
	shrl	$8, %eax
	movl	%eax, -384(%rbp)
	je	.L1426
	sall	$8, %eax
	addq	%rsi, %rax
	movq	%rax, -264(%rbp)
.L1426:
	movl	-384(%rbp), %ebx
	cmpl	%ebx, -268(%rbp)
	ja	.L1954
	movl	-268(%rbp), %eax
	testl	%eax, %eax
	jne	.L1726
	movq	$0, -328(%rbp)
.L1452:
	movq	-304(%rbp), %rax
	movq	48(%rax), %rax
	cmpq	-256(%rbp), %rax
	je	.L1659
	jbe	.L1955
	movq	-256(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	subq	%rsi, %rax
	movq	%rax, %r12
	movq	-304(%rbp), %rax
	movl	%r12d, %edx
	movq	24(%rax), %rdi
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	-392(%rbp), %rax
	cmpb	$0, 354(%rax)
	jne	.L1956
.L1661:
	movq	-304(%rbp), %rbx
	movq	-256(%rbp), %r14
	movq	48(%rbx), %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE0EE11RemoveRangeEPNS0_11MemoryChunkEmmNS0_7SlotSet15EmptyBucketModeE.constprop.0
	movq	48(%rbx), %rdx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal13RememberedSetILNS0_17RememberedSetTypeE1EE11RemoveRangeEPNS0_11MemoryChunkEmmNS0_7SlotSet15EmptyBucketModeE.constprop.0
	cmpb	$0, -393(%rbp)
	jne	.L1957
.L1662:
	movq	-160(%rbp), %r14
	movq	-152(%rbp), %rbx
	cmpq	%rbx, %r14
	je	.L1659
	movq	-304(%rbp), %rax
	movq	-80(%rbp), %r15
	movq	48(%rax), %r12
	movq	-256(%rbp), %rax
	cmpq	-72(%rbp), %rax
	jb	.L1668
	movq	-88(%rbp), %r13
	movq	%r14, %rdi
	movq	%r13, %xmm4
	punpcklqdq	%xmm4, %xmm4
	movaps	%xmm4, -240(%rbp)
	.p2align 4,,10
	.p2align 3
.L1665:
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, -160(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rbx
	je	.L1667
	movq	32(%rax), %rax
	leaq	-1(%rax), %r15
	movq	%r15, -80(%rbp)
	movslq	40(%rdi), %rax
	addq	%r15, %rax
	movq	%rax, -72(%rbp)
	cmpq	%rax, -256(%rbp)
	jnb	.L1665
	movq	%rdi, %r14
.L1668:
	movq	-256(%rbp), %r13
	cmpq	%r15, %r12
	ja	.L1666
	jmp	.L1659
	.p2align 4,,10
	.p2align 3
.L1670:
	movq	%rax, -160(%rbp)
	cmpq	-152(%rbp), %rbx
	je	.L1672
.L1958:
	movq	32(%rbx), %rax
	leaq	-1(%rax), %r15
	movq	%r15, -80(%rbp)
	movslq	40(%rbx), %rax
	addq	%r15, %rax
	movq	%rax, -72(%rbp)
	cmpq	%r15, %r12
	jbe	.L1659
.L1673:
	movq	%rbx, %r14
.L1666:
	movq	%r14, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rbx
	cmpq	%r13, %r15
	jb	.L1670
	movq	-144(%rbp), %r15
	movq	%r14, %rdi
	leaq	8(%r15), %rsi
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	subq	$1, 40(%r15)
	movq	%rbx, -160(%rbp)
	cmpq	-152(%rbp), %rbx
	jne	.L1958
.L1672:
	movq	-88(%rbp), %r15
	movq	%r15, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	cmpq	%r15, %r12
	ja	.L1673
	.p2align 4,,10
	.p2align 3
.L1659:
	cmpq	$0, -168(%rbp)
	jne	.L1959
.L1675:
	movq	-304(%rbp), %rbx
	movq	16(%rbx), %rax
	leaq	8(%rax), %rdi
	movq	$0, (%rax)
	movq	$0, 4088(%rax)
	andq	$-8, %rdi
	subq	%rdi, %rax
	leal	4096(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	movq	$0, 96(%rbx)
	movq	192(%rbx), %rax
	addq	40(%rbx), %rax
	subq	48(%rbx), %rax
	addq	-328(%rbp), %rax
	movq	%rax, 192(%rbx)
	movq	$0, 168(%rbx)
	mfence
	movq	-376(%rbp), %rax
	testq	%rax, %rax
	je	.L1678
	movq	%rax, %rdi
	call	_ZN2v88internal18CodeObjectRegistry8FinalizeEv@PLT
.L1678:
	movq	-120(%rbp), %rbx
	leaq	-136(%rbp), %r12
	testq	%rbx, %rbx
	je	.L1682
.L1679:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal10HeapObjectESt4pairIKS2_iESt10_Select1stIS5_ENS1_6Object8ComparerESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1679
.L1682:
	movq	-192(%rbp), %rbx
	leaq	-208(%rbp), %r12
	testq	%rbx, %rbx
	je	.L1680
.L1681:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIjSt4pairIKjjESt10_Select1stIS2_ESt4lessIjESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1681
.L1680:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1960
	addq	$440, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1953:
	.cfi_restore_state
	movq	-304(%rbp), %rdi
	call	_ZN2v88internal11MemoryChunk19ReleaseLocalTrackerEv@PLT
	jmp	.L1423
	.p2align 4,,10
	.p2align 3
.L1952:
	movq	-304(%rbp), %rax
	movq	128(%rax), %rax
	testq	%rax, %rax
	setne	-393(%rbp)
	jmp	.L1421
	.p2align 4,,10
	.p2align 3
.L1954:
	movl	%ebx, %eax
	movq	-240(%rbp), %rbx
	movq	-264(%rbp), %rsi
	movq	%rax, %r14
	movl	(%rbx,%rax,4), %r15d
	movl	$1, %ebx
	.p2align 4,,10
	.p2align 3
.L1435:
	testl	%r15d, %r15d
	je	.L1431
.L1963:
	xorl	%r12d, %r12d
	movl	%ebx, %edx
	rep bsfl	%r15d, %r12d
	movl	%r12d, %ecx
	sall	%cl, %edx
	notl	%edx
	andl	%edx, %r15d
	cmpl	$31, %r12d
	je	.L1961
	leal	1(%r12), %ecx
	movl	%ebx, %eax
	movq	%rsi, %rdx
	sall	%cl, %eax
	movl	%eax, %ecx
	testl	%r15d, %ecx
	jne	.L1962
.L1715:
	movq	%rdx, %rsi
	testl	%r15d, %r15d
	jne	.L1963
.L1431:
	addq	$256, -264(%rbp)
	addl	$1, %r14d
	cmpl	%r14d, -268(%rbp)
	je	.L1951
	movq	-240(%rbp), %rdi
	movl	%r14d, %eax
	movl	(%rdi,%rax,4), %r15d
	jbe	.L1964
	movq	-264(%rbp), %rsi
	jmp	.L1435
	.p2align 4,,10
	.p2align 3
.L1951:
	movl	%r14d, -384(%rbp)
	movq	%rsi, -248(%rbp)
.L1950:
	movq	-304(%rbp), %rbx
	xorl	%edx, %edx
	movq	24(%rbx), %rax
	movq	-37528(%rax), %rdi
	subq	$37592, %rax
	movq	%rdi, -344(%rbp)
	movq	72(%rax), %rdi
	movq	%rdi, -352(%rbp)
	movq	56(%rax), %rax
	movq	%rax, -368(%rbp)
	movq	48(%rbx), %rax
	movq	%rax, -320(%rbp)
	movl	-320(%rbp), %r14d
	subl	%ebx, %r14d
	shrl	$8, %r14d
	movl	%r14d, -360(%rbp)
.L1429:
	movl	-268(%rbp), %eax
	movl	%eax, %r10d
	sall	$8, %r10d
	leaq	(%rbx,%r10), %r13
	movl	-360(%rbp), %ebx
	cmpl	%ebx, %eax
	jnb	.L1444
	movq	-240(%rbp), %rbx
	movl	%eax, %eax
	movq	%rax, %r14
	movl	(%rbx,%rax,4), %r12d
	movq	%r13, %rax
	.p2align 4,,10
	.p2align 3
.L1446:
	testl	%r12d, %r12d
	je	.L1442
.L1967:
	xorl	%ebx, %ebx
	rep bsfl	%r12d, %ebx
	cmpl	$31, %ebx
	je	.L1965
	movl	%ebx, %ecx
	movl	$1, %esi
	movq	%rax, %r8
	sall	%cl, %esi
	movl	%esi, %ecx
	movl	$1, %esi
	notl	%ecx
	andl	%ecx, %r12d
	leal	1(%rbx), %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	testl	%r12d, %ecx
	jne	.L1966
.L1718:
	movq	%r8, %rax
	testl	%r12d, %r12d
	jne	.L1967
.L1442:
	addq	$256, %r13
	addl	$1, %r14d
	cmpl	%r14d, -360(%rbp)
	je	.L1444
	movq	-240(%rbp), %rbx
	movl	%r14d, %eax
	movl	(%rbx,%rax,4), %r12d
	jbe	.L1444
	movq	%r13, %rax
	jmp	.L1446
	.p2align 4,,10
	.p2align 3
.L1961:
	addq	$256, -264(%rbp)
	addl	$1, %r14d
	cmpl	%r14d, -268(%rbp)
	je	.L1951
	movq	-240(%rbp), %rax
	movl	%r14d, %edx
	movl	$1, %ecx
	movl	(%rax,%rdx,4), %r15d
	movq	-264(%rbp), %rdx
	testl	%r15d, %ecx
	je	.L1715
.L1962:
	sall	$3, %r12d
	movq	%rdx, -248(%rbp)
	leaq	-216(%rbp), %rdi
	addq	%rsi, %r12
	leaq	1(%r12), %rax
	movq	%rax, -216(%rbp)
	movq	(%r12), %r13
	movq	%r13, %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-248(%rbp), %rdx
	cltq
	leaq	-8(%r12,%rax), %rcx
	movq	%rdx, %rsi
	cmpq	%rcx, %r12
	je	.L1436
	subl	-272(%rbp), %ecx
	movl	%ecx, %eax
	shrl	$3, %ecx
	shrl	$8, %eax
	andl	$31, %ecx
	cmpl	%r14d, %eax
	je	.L1437
	movl	%eax, %edx
	movq	-240(%rbp), %rdi
	subl	%r14d, %edx
	sall	$8, %edx
	addq	%rdx, -264(%rbp)
	movl	%eax, %edx
	movq	-264(%rbp), %rsi
	movl	(%rdi,%rdx,4), %r15d
.L1437:
	movl	$-2, %edx
	movl	%eax, %r14d
	sall	%cl, %edx
	andl	%edx, %r15d
.L1436:
	movq	-216(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1435
	cmpq	%r13, -280(%rbp)
	sete	%cl
	cmpq	%r13, -288(%rbp)
	sete	%al
	orb	%al, %cl
	jne	.L1435
	cmpq	%r13, -296(%rbp)
	je	.L1435
	movl	%r14d, -384(%rbp)
	movq	%rsi, -248(%rbp)
	testl	%r15d, %r15d
	jne	.L1698
	addl	$1, -384(%rbp)
	movl	-384(%rbp), %eax
	addq	$256, -264(%rbp)
	cmpl	%eax, -268(%rbp)
	je	.L1698
	movl	-384(%rbp), %eax
	movq	-240(%rbp), %rbx
	movl	(%rbx,%rax,4), %r15d
	movq	-264(%rbp), %rax
	movq	%rax, -248(%rbp)
.L1698:
	movq	-304(%rbp), %rbx
	movq	24(%rbx), %rax
	movq	-37528(%rax), %rdi
	subq	$37592, %rax
	movq	%rdi, -344(%rbp)
	movq	72(%rax), %rdi
	movq	56(%rax), %rax
	movq	%rdi, -352(%rbp)
	movq	%rax, -368(%rbp)
	movq	48(%rbx), %rax
	movq	%rax, -320(%rbp)
	movl	-320(%rbp), %r14d
	subl	%ebx, %r14d
	shrl	$8, %r14d
	movl	%r14d, -360(%rbp)
	jmp	.L1429
	.p2align 4,,10
	.p2align 3
.L1965:
	leaq	256(%r13), %r8
	addl	$1, %r14d
	cmpl	-360(%rbp), %r14d
	je	.L1444
	movq	-240(%rbp), %rsi
	movl	%r14d, %ecx
	movq	%r8, %r13
	movl	(%rsi,%rcx,4), %r12d
	movl	$1, %ecx
	testl	%r12d, %ecx
	je	.L1718
.L1966:
	sall	$3, %ebx
	movq	%rdx, -336(%rbp)
	leaq	-216(%rbp), %rdi
	addq	%rax, %rbx
	movq	%r8, -328(%rbp)
	leaq	1(%rbx), %rax
	movq	%rax, -216(%rbp)
	movq	(%rbx), %rsi
	movq	%rsi, -320(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-328(%rbp), %r8
	movq	-320(%rbp), %rsi
	cltq
	movq	-336(%rbp), %rdx
	leaq	-8(%rbx,%rax), %rcx
	movq	%r8, %rax
	cmpq	%rcx, %rbx
	je	.L1447
	subl	-272(%rbp), %ecx
	movl	%ecx, %edi
	shrl	$3, %ecx
	shrl	$8, %edi
	andl	$31, %ecx
	cmpl	%r14d, %edi
	je	.L1448
	movl	%edi, %eax
	movq	-240(%rbp), %rbx
	subl	%r14d, %eax
	sall	$8, %eax
	addq	%rax, %r13
	movl	%edi, %eax
	movl	(%rbx,%rax,4), %r12d
	movq	%r13, %rax
.L1448:
	movl	$-2, %r8d
	movl	%edi, %r14d
	sall	%cl, %r8d
	andl	%r8d, %r12d
.L1447:
	movq	-216(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L1446
	cmpq	-344(%rbp), %rsi
	sete	%r8b
	cmpq	-352(%rbp), %rsi
	sete	%dil
	orb	%dil, %r8b
	jne	.L1446
	cmpq	-368(%rbp), %rsi
	je	.L1446
	movq	%rcx, -336(%rbp)
	jmp	.L1440
	.p2align 4,,10
	.p2align 3
.L1726:
	xorl	%edx, %edx
.L1444:
	movq	$0, -336(%rbp)
.L1440:
	cmpq	-336(%rbp), %rdx
	movq	$0, -328(%rbp)
	je	.L1452
	movl	-384(%rbp), %ebx
	movq	-264(%rbp), %r12
.L1451:
	movq	-376(%rbp), %rax
	movq	%rdx, -224(%rbp)
	leaq	-1(%rdx), %r14
	testq	%rax, %rax
	je	.L1453
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal18CodeObjectRegistry33RegisterAlreadyExistingCodeObjectEm@PLT
	movq	-224(%rbp), %rax
	leaq	-1(%rax), %r14
.L1453:
	cmpq	-256(%rbp), %r14
	je	.L1454
	jbe	.L1968
	movq	-256(%rbp), %rsi
	movq	-304(%rbp), %rax
	movq	%r14, %r13
	xorl	%r8d, %r8d
	movl	$1, %ecx
	subq	%rsi, %r13
	movq	24(%rax), %rdi
	movl	%r13d, %edx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	-392(%rbp), %rax
	cmpb	$0, 354(%rax)
	jne	.L1969
.L1456:
	movq	-304(%rbp), %rax
	movq	104(%rax), %r11
	testq	%r11, %r11
	je	.L1457
	movq	-256(%rbp), %rcx
	movq	%r14, %rdi
	subq	%rax, %rdi
	subq	%rax, %rcx
	movq	%rdi, %rax
	cmpq	$262143, %rdi
	jbe	.L1970
	movq	%rcx, %rdi
	leaq	-1(%rax), %rdx
	movl	%ecx, %esi
	movl	%eax, %r13d
	shrq	$18, %rdi
	shrq	$18, %rdx
	andl	$262143, %esi
	movslq	%edi, %rax
	movl	%edx, %ecx
	movl	%esi, -264(%rbp)
	movslq	%edx, %rsi
	movq	%rax, -384(%rbp)
	leaq	(%rax,%rax,2), %rax
	sall	$18, %ecx
	salq	$7, %rax
	movl	%edi, -344(%rbp)
	subl	%ecx, %r13d
	movl	%edx, -320(%rbp)
	leaq	(%r11,%rax), %r9
	movq	%rsi, -360(%rbp)
	movq	%rax, -352(%rbp)
	cmpl	%edx, %edi
	je	.L1971
	movl	-264(%rbp), %eax
	sarl	$13, %eax
	movslq	%eax, %r10
	movl	%eax, -368(%rbp)
	leaq	0(,%r10,8), %rax
	movq	%rax, -408(%rbp)
	movq	(%r9,%r10,8), %rax
	movq	%rax, -416(%rbp)
	testq	%rax, %rax
	jne	.L1502
.L1507:
	cmpl	$31, -368(%rbp)
	je	.L1503
	movq	-408(%rbp), %rax
	leaq	8(%r9,%rax), %rcx
	movl	$30, %eax
	subl	-368(%rbp), %eax
	addq	%rax, %r10
	leaq	16(%r9,%r10,8), %r8
	jmp	.L1512
	.p2align 4,,10
	.p2align 3
.L1972:
	addq	$8, %rcx
	cmpq	%rcx, %r8
	je	.L1503
.L1512:
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L1972
	leaq	128(%rax), %rsi
	.p2align 4,,10
	.p2align 3
.L1514:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rsi
	jne	.L1514
	addq	$8, %rcx
	cmpq	%rcx, %r8
	jne	.L1512
.L1503:
	movq	256(%r9), %rax
	leal	1(%rdi), %eax
	cmpl	%eax, %edx
	jle	.L1509
	cltq
	subl	%edi, %edx
	xorl	%esi, %esi
	leaq	(%rax,%rax,2), %rax
	salq	$7, %rax
	leaq	256(%r11,%rax), %rcx
	leal	-2(%rdx), %eax
	addq	-384(%rbp), %rax
	leaq	(%rax,%rax,2), %rax
	salq	$7, %rax
	leaq	1024(%r11,%rax), %rdi
	.p2align 4,,10
	.p2align 3
.L1528:
	movq	-256(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.L1521
.L1523:
	leaq	-248(%rcx), %rdx
	jmp	.L1518
	.p2align 4,,10
	.p2align 3
.L1974:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	je	.L1973
.L1518:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L1974
	leaq	128(%rax), %r8
	.p2align 4,,10
	.p2align 3
.L1527:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %r8
	jne	.L1527
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	jne	.L1518
.L1973:
	movq	(%rcx), %rax
	addq	$384, %rcx
	cmpq	%rcx, %rdi
	jne	.L1528
.L1509:
	movq	-360(%rbp), %rax
	leaq	(%rax,%rax,2), %rsi
	salq	$7, %rsi
	addq	%r11, %rsi
	cmpl	$262144, %r13d
	jg	.L1515
	movl	%r13d, %ecx
	movl	%r13d, %edi
	movl	%r13d, %r10d
	movl	$1, %r11d
	sarl	$3, %ecx
	sarl	$13, %edi
	sarl	$8, %r10d
	sall	%cl, %r11d
	movl	%edi, %eax
	andl	$31, %r10d
	movl	%r11d, %r8d
	negl	%r8d
	orl	%r10d, %eax
	je	.L1975
	movq	(%rsi), %rdx
	testq	%rdx, %rdx
	jne	.L1976
	testl	%edi, %edi
	jg	.L1693
.L1947:
	xorl	%eax, %eax
	movl	$1, %edi
.L1536:
	movq	(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L1482
	testb	%al, %al
	jne	.L1482
	cmpl	%edi, %r10d
	jle	.L1544
	leal	-1(%r10), %ecx
	movslq	%edi, %rsi
	subl	%edi, %ecx
	leaq	(%rdx,%rsi,4), %rax
	addq	%rsi, %rcx
	leaq	4(%rdx,%rcx,4), %rcx
	.p2align 4,,10
	.p2align 3
.L1545:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rcx, %rax
	jne	.L1545
.L1544:
	movslq	%r10d, %r10
	leal	-1(%r11), %ecx
	leaq	(%rdx,%r10,4), %rsi
	jmp	.L1543
	.p2align 4,,10
	.p2align 3
.L1977:
	movl	%r8d, %edi
	movl	%edx, %eax
	andl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L1482
.L1543:
	movl	(%rsi), %edx
	testl	%edx, %ecx
	jne	.L1977
	.p2align 4,,10
	.p2align 3
.L1482:
	movq	-304(%rbp), %rax
	movq	112(%rax), %rax
	movq	%rax, %r11
	testq	%rax, %rax
	jne	.L1700
	.p2align 4,,10
	.p2align 3
.L1551:
	cmpb	$0, -393(%rbp)
	jne	.L1978
.L1548:
	movq	-160(%rbp), %rdi
	movq	-152(%rbp), %r13
	cmpq	%r13, %rdi
	je	.L1645
	movq	-256(%rbp), %rax
	movq	-80(%rbp), %rdx
	cmpq	-72(%rbp), %rax
	jb	.L1640
	movq	-88(%rbp), %rax
	movq	%rax, %xmm2
	movq	%rax, -264(%rbp)
	punpcklqdq	%xmm2, %xmm2
	movaps	%xmm2, -320(%rbp)
	.p2align 4,,10
	.p2align 3
.L1637:
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, -160(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %r13
	je	.L1639
	movq	32(%rax), %rax
	leaq	-1(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movslq	40(%rdi), %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	cmpq	%rax, -256(%rbp)
	jnb	.L1637
.L1640:
	cmpq	%rdx, %r14
	jbe	.L1645
	movq	%r12, -320(%rbp)
	movq	-256(%rbp), %r13
	movq	%rdi, %r12
	movl	%ebx, -256(%rbp)
	movq	%rdx, %rbx
	movl	%r15d, -264(%rbp)
	jmp	.L1638
	.p2align 4,,10
	.p2align 3
.L1642:
	movq	%rax, -160(%rbp)
	cmpq	%r15, -152(%rbp)
	je	.L1644
.L1979:
	movq	32(%r15), %rax
	leaq	-1(%rax), %rbx
	movq	%rbx, -80(%rbp)
	movslq	40(%r15), %rax
	addq	%rbx, %rax
	movq	%rax, -72(%rbp)
	cmpq	%rbx, %r14
	jbe	.L1949
.L1646:
	movq	%r15, %r12
.L1638:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%rbx, %r13
	ja	.L1642
	movq	-144(%rbp), %rbx
	movq	%r12, %rdi
	leaq	8(%rbx), %rsi
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	subq	$1, 40(%rbx)
	movq	%r15, -160(%rbp)
	cmpq	%r15, -152(%rbp)
	jne	.L1979
.L1644:
	movq	-88(%rbp), %rbx
	movq	%rbx, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	cmpq	%rbx, %r14
	ja	.L1646
.L1949:
	movl	-264(%rbp), %r15d
	movl	-256(%rbp), %ebx
	movq	-320(%rbp), %r12
.L1645:
	movq	-224(%rbp), %rax
	subq	$1, %rax
	movq	%rax, -256(%rbp)
.L1454:
	movq	-256(%rbp), %rax
	leaq	-224(%rbp), %rdi
	movq	(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, -328(%rbp)
	addq	%r14, %rax
	movq	%rax, -256(%rbp)
	cmpl	%ebx, -268(%rbp)
	jbe	.L1723
	.p2align 4,,10
	.p2align 3
.L1724:
	movq	-248(%rbp), %r14
	testl	%r15d, %r15d
	je	.L1650
	xorl	%eax, %eax
	movl	$1, %edx
	rep bsfl	%r15d, %eax
	movl	%eax, %ecx
	sall	%cl, %edx
	leal	1(%rax), %ecx
	notl	%edx
	andl	%edx, %r15d
	movl	$1, %edx
	sall	%cl, %edx
	cmpl	$31, %eax
	je	.L1980
.L1653:
	testl	%r15d, %edx
	je	.L1724
	sall	$3, %eax
	leaq	-216(%rbp), %rdi
	addq	%rax, %r14
	leaq	1(%r14), %rax
	movq	%rax, -216(%rbp)
	movq	(%r14), %r13
	movq	%r13, %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	leaq	-8(%r14,%rax), %rcx
	cmpq	%rcx, %r14
	je	.L1655
	subl	-272(%rbp), %ecx
	movl	%ecx, %edx
	shrl	$3, %ecx
	shrl	$8, %edx
	andl	$31, %ecx
	cmpl	%edx, %ebx
	je	.L1656
	movl	%edx, %eax
	movq	-240(%rbp), %rsi
	subl	%ebx, %eax
	sall	$8, %eax
	addq	%rax, %r12
	movl	%edx, %eax
	movq	%r12, -248(%rbp)
	movl	(%rsi,%rax,4), %r15d
.L1656:
	movl	$-2, %eax
	movl	%edx, %ebx
	sall	%cl, %eax
	andl	%eax, %r15d
.L1655:
	movq	-216(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1724
	cmpq	%r13, -280(%rbp)
	sete	%cl
	cmpq	%r13, -288(%rbp)
	sete	%al
	orb	%al, %cl
	jne	.L1724
	cmpq	%r13, -296(%rbp)
	je	.L1724
	testl	%r15d, %r15d
	jne	.L1652
	addq	$256, %r12
	addl	$1, %ebx
	cmpl	%ebx, -268(%rbp)
	je	.L1652
	movq	-240(%rbp), %rdi
	movl	%ebx, %eax
	movq	%r12, -248(%rbp)
	movl	(%rdi,%rax,4), %r15d
	.p2align 4,,10
	.p2align 3
.L1652:
	cmpq	-336(%rbp), %rdx
	jne	.L1451
	jmp	.L1452
	.p2align 4,,10
	.p2align 3
.L1980:
	addq	$256, %r12
	addl	$1, %ebx
	cmpl	%ebx, -268(%rbp)
	je	.L1723
	movq	-240(%rbp), %rsi
	movl	%ebx, %edx
	movq	%r12, -248(%rbp)
	movl	(%rsi,%rdx,4), %r15d
	movl	$1, %edx
	jmp	.L1653
	.p2align 4,,10
	.p2align 3
.L1650:
	addq	$256, %r12
	addl	$1, %ebx
	cmpl	%ebx, -268(%rbp)
	je	.L1703
	movq	-240(%rbp), %rsi
	movl	%ebx, %eax
	movl	(%rsi,%rax,4), %r15d
	jbe	.L1981
	movq	%r12, -248(%rbp)
	jmp	.L1724
.L1981:
	movq	%r12, %r14
	.p2align 4,,10
	.p2align 3
.L1703:
	movq	%r14, -248(%rbp)
	.p2align 4,,10
	.p2align 3
.L1723:
	xorl	%edx, %edx
	cmpq	-336(%rbp), %rdx
	jne	.L1451
	jmp	.L1452
	.p2align 4,,10
	.p2align 3
.L1457:
	movq	-304(%rbp), %rax
	movq	112(%rax), %rax
	testq	%rax, %rax
	je	.L1551
	movq	-304(%rbp), %rdi
	movq	-256(%rbp), %rcx
	movq	%rax, %r11
	movq	%r14, %rax
	subq	%rdi, %rax
	subq	%rdi, %rcx
	cmpq	$262143, %rax
	jbe	.L1982
	movq	%rcx, %rdx
	leaq	-1(%rax), %rsi
	movl	%ecx, %edi
	movl	%eax, %r13d
	shrq	$18, %rdx
	shrq	$18, %rsi
	andl	$262143, %edi
	movl	%edx, -344(%rbp)
	movslq	%edx, %rdx
	leaq	(%rdx,%rdx,2), %rax
	movl	%edi, -264(%rbp)
	movslq	%esi, %rdi
	salq	$7, %rax
	movq	%rdi, -360(%rbp)
	movq	%rax, -352(%rbp)
	movl	%esi, -320(%rbp)
	sall	$18, %esi
	subl	%esi, %r13d
.L1700:
	movq	-352(%rbp), %r10
	movl	-344(%rbp), %edi
	addq	%r11, %r10
	cmpl	%edi, -320(%rbp)
	je	.L1983
	movl	-264(%rbp), %r9d
	sarl	$13, %r9d
	movslq	%r9d, %r8
	leaq	0(,%r8,8), %rax
	movq	%rax, -352(%rbp)
	movq	(%r10,%r8,8), %rax
	movq	%rax, -368(%rbp)
	testq	%rax, %rax
	jne	.L1591
.L1596:
	cmpl	$31, %r9d
	je	.L1592
	movq	-352(%rbp), %rax
	leaq	8(%r10,%rax), %rdx
	movl	$30, %eax
	subl	%r9d, %eax
	addq	%rax, %r8
	leaq	16(%r10,%r8,8), %rsi
	jmp	.L1601
	.p2align 4,,10
	.p2align 3
.L1984:
	addq	$8, %rdx
	cmpq	%rdx, %rsi
	je	.L1592
.L1601:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L1984
	leaq	128(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L1603:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rcx
	jne	.L1603
	addq	$8, %rdx
	cmpq	%rdx, %rsi
	jne	.L1601
.L1592:
	movq	256(%r10), %rax
	movl	-344(%rbp), %eax
	addl	$1, %eax
	cmpl	%eax, -320(%rbp)
	jle	.L1598
	cltq
	movslq	-344(%rbp), %rdx
	xorl	%esi, %esi
	leaq	(%rax,%rax,2), %rax
	salq	$7, %rax
	leaq	256(%r11,%rax), %rcx
	movl	-320(%rbp), %eax
	subl	$2, %eax
	subl	%edx, %eax
	addq	%rdx, %rax
	leaq	(%rax,%rax,2), %rax
	salq	$7, %rax
	leaq	1024(%r11,%rax), %rdi
	.p2align 4,,10
	.p2align 3
.L1616:
	movq	-256(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.L1609
.L1611:
	leaq	-248(%rcx), %rdx
	jmp	.L1606
	.p2align 4,,10
	.p2align 3
.L1986:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	je	.L1985
.L1606:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L1986
	leaq	128(%rax), %r8
	.p2align 4,,10
	.p2align 3
.L1615:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %r8
	jne	.L1615
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	jne	.L1606
.L1985:
	movq	(%rcx), %rax
	addq	$384, %rcx
	cmpq	%rcx, %rdi
	jne	.L1616
.L1598:
	movq	-360(%rbp), %rax
	leaq	(%rax,%rax,2), %rsi
	salq	$7, %rsi
	addq	%r11, %rsi
	cmpl	$262144, %r13d
	jg	.L1515
	movl	%r13d, %ecx
	movl	%r13d, %r8d
	movl	%r13d, %edi
	movl	$1, %r11d
	sarl	$3, %ecx
	sarl	$13, %r8d
	sarl	$8, %edi
	sall	%cl, %r11d
	movl	%r8d, %eax
	andl	$31, %edi
	movl	%r11d, %r9d
	negl	%r9d
	orl	%edi, %eax
	je	.L1987
	movq	(%rsi), %rdx
	testq	%rdx, %rdx
	jne	.L1988
	testl	%r8d, %r8d
	jg	.L1688
.L1948:
	xorl	%eax, %eax
	movl	$1, %r8d
.L1624:
	movq	(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L1551
	testb	%al, %al
	jne	.L1551
	cmpl	%r8d, %edi
	jle	.L1632
	leal	-1(%rdi), %edx
	movslq	%r8d, %rsi
	subl	%r8d, %edx
	leaq	(%rcx,%rsi,4), %rax
	addq	%rsi, %rdx
	leaq	4(%rcx,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L1633:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L1633
.L1632:
	movslq	%edi, %rdi
	subl	$1, %r11d
	leaq	(%rcx,%rdi,4), %rcx
	jmp	.L1631
	.p2align 4,,10
	.p2align 3
.L1989:
	movl	%r9d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L1551
.L1631:
	movl	(%rcx), %edx
	testl	%edx, %r11d
	jne	.L1989
	jmp	.L1551
	.p2align 4,,10
	.p2align 3
.L1639:
	movdqa	-320(%rbp), %xmm1
	movq	-264(%rbp), %rsi
	movaps	%xmm1, -80(%rbp)
	cmpq	%rsi, -256(%rbp)
	jnb	.L1637
	movq	%rsi, %rdx
	jmp	.L1640
	.p2align 4,,10
	.p2align 3
.L1969:
	movq	-256(%rbp), %rsi
	movq	-304(%rbp), %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal11MemoryChunk19DiscardUnusedMemoryEmm@PLT
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1978:
	movl	-272(%rbp), %edi
	movl	-256(%rbp), %eax
	leaq	-216(%rbp), %rsi
	subl	%edi, %eax
	movl	%eax, -216(%rbp)
	movl	%r14d, %eax
	subl	%edi, %eax
	leaq	-208(%rbp), %rdi
	movl	%eax, -212(%rbp)
	call	_ZNSt8_Rb_treeIjSt4pairIKjjESt10_Select1stIS2_ESt4lessIjESaIS2_EE17_M_emplace_uniqueIJS0_IjjEEEES0_ISt17_Rb_tree_iteratorIS2_EbEDpOT_
	jmp	.L1548
	.p2align 4,,10
	.p2align 3
.L1519:
	movl	%r8d, %eax
	lock cmpxchgl	%esi, (%rdx)
	cmpl	%eax, %r8d
	je	.L1522
.L1521:
	movl	(%rdx), %r8d
	testl	%r8d, %r8d
	jne	.L1519
.L1522:
	leaq	4(%rdx), %rax
	subq	$-128, %rdx
	.p2align 4,,10
	.p2align 3
.L1520:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L1520
	jmp	.L1523
	.p2align 4,,10
	.p2align 3
.L1607:
	movl	%r8d, %eax
	lock cmpxchgl	%esi, (%rdx)
	cmpl	%eax, %r8d
	je	.L1610
.L1609:
	movl	(%rdx), %r8d
	testl	%r8d, %r8d
	jne	.L1607
.L1610:
	leaq	4(%rdx), %rax
	subq	$-128, %rdx
	.p2align 4,,10
	.p2align 3
.L1608:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L1608
	jmp	.L1611
	.p2align 4,,10
	.p2align 3
.L1970:
	movl	%ecx, %edx
	movl	%ecx, %r10d
	sarl	$3, %ecx
	movl	%eax, %r9d
	sarl	$8, %edx
	sarl	$13, %r10d
	movl	%edx, %edi
	movl	%eax, %edx
	sarl	$13, %r9d
	sarl	$8, %edx
	andl	$31, %edi
	movl	%edx, %esi
	movl	$1, %edx
	movl	%edi, -320(%rbp)
	movl	%edx, %r8d
	andl	$31, %esi
	sall	%cl, %r8d
	movl	%eax, %ecx
	movl	%esi, -264(%rbp)
	sarl	$3, %ecx
	movl	%r8d, -408(%rbp)
	subl	$1, %r8d
	sall	%cl, %edx
	movl	%edx, %r13d
	movl	%edx, -360(%rbp)
	negl	%r13d
	cmpl	%esi, %edi
	movslq	%r10d, %rdi
	sete	%dl
	cmpl	%r9d, %r10d
	movq	%rdi, -384(%rbp)
	sete	%al
	salq	$3, %rdi
	movq	%rdi, -344(%rbp)
	addq	%r11, %rdi
	andb	%al, %dl
	movb	%dl, -368(%rbp)
	movq	(%rdi), %rax
	jne	.L1990
	movq	%rax, -416(%rbp)
	movl	-320(%rbp), %esi
	addl	$1, %esi
	movl	%esi, -352(%rbp)
	testq	%rax, %rax
	jne	.L1991
	cmpl	%r9d, %r10d
	jl	.L1468
.L1946:
	movl	%r10d, %eax
.L1472:
	movq	(%rdi), %rdx
	cmpl	$32, %eax
	je	.L1460
	testq	%rdx, %rdx
	je	.L1460
	movl	-352(%rbp), %edi
	cmpl	%edi, -264(%rbp)
	jle	.L1477
	movl	-264(%rbp), %ecx
	movslq	%edi, %rsi
	leaq	(%rdx,%rsi,4), %rax
	subl	$1, %ecx
	subl	%edi, %ecx
	addq	%rsi, %rcx
	leaq	4(%rdx,%rcx,4), %rcx
	.p2align 4,,10
	.p2align 3
.L1478:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rcx
	jne	.L1478
.L1477:
	movl	-360(%rbp), %eax
	leal	-1(%rax), %edi
	movslq	-264(%rbp), %rax
	leaq	(%rdx,%rax,4), %rcx
	jmp	.L1476
	.p2align 4,,10
	.p2align 3
.L1992:
	movl	%r13d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L1460
.L1476:
	movl	(%rcx), %edx
	testl	%edx, %edi
	jne	.L1992
.L1460:
	movq	-304(%rbp), %rax
	movq	112(%rax), %rax
	movq	%rax, %r11
	testq	%rax, %rax
	je	.L1551
.L1699:
	movq	-344(%rbp), %rdi
	addq	%r11, %rdi
	cmpb	$0, -368(%rbp)
	movq	(%rdi), %rax
	jne	.L1993
	movq	%rax, -352(%rbp)
	movl	-320(%rbp), %esi
	addl	$1, %esi
	movl	%esi, -344(%rbp)
	testq	%rax, %rax
	jne	.L1994
	cmpl	%r9d, %r10d
	jl	.L1559
.L1563:
	movq	(%rdi), %rcx
	cmpl	$32, %r10d
	je	.L1551
	testq	%rcx, %rcx
	je	.L1551
	movl	-264(%rbp), %edi
	cmpl	%edi, -344(%rbp)
	jge	.L1568
	movslq	-344(%rbp), %rsi
	movl	%edi, %edx
	subl	$1, %edx
	subl	%esi, %edx
	leaq	(%rcx,%rsi,4), %rax
	addq	%rsi, %rdx
	leaq	4(%rcx,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L1569:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L1569
.L1568:
	movl	-360(%rbp), %edx
	movslq	-264(%rbp), %rax
	subl	$1, %edx
	leaq	(%rcx,%rax,4), %rsi
	jmp	.L1567
	.p2align 4,,10
	.p2align 3
.L1995:
	movl	%r13d, %edi
	movl	%ecx, %eax
	andl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	je	.L1551
.L1567:
	movl	(%rsi), %ecx
	testl	%ecx, %edx
	jne	.L1995
	jmp	.L1551
	.p2align 4,,10
	.p2align 3
.L1971:
	cmpl	$262144, %r13d
	jg	.L1515
	movl	-264(%rbp), %edi
	movl	%r13d, %r11d
	sarl	$13, %r11d
	movl	%edi, %eax
	movl	%edi, %r10d
	sarl	$3, %edi
	sarl	$8, %eax
	movl	%edi, %ecx
	sarl	$13, %r10d
	andl	$31, %eax
	movl	%eax, %esi
	movl	%eax, -416(%rbp)
	movl	%r13d, %eax
	sarl	$8, %eax
	andl	$31, %eax
	movl	%eax, -368(%rbp)
	movl	$1, %eax
	movl	%eax, %edi
	sall	%cl, %edi
	leal	-1(%rdi), %ecx
	movl	%ecx, -400(%rbp)
	movl	%r13d, %ecx
	sarl	$3, %ecx
	sall	%cl, %eax
	movl	%eax, -432(%rbp)
	movl	%eax, %r8d
	movl	-368(%rbp), %eax
	negl	%r8d
	cmpl	%r11d, %r10d
	sete	%dl
	cmpl	%eax, %esi
	movslq	%r10d, %rsi
	sete	%al
	movq	%rsi, -456(%rbp)
	salq	$3, %rsi
	movq	%rsi, -440(%rbp)
	addq	%r9, %rsi
	andb	%al, %dl
	movq	%rsi, -384(%rbp)
	movb	%dl, -424(%rbp)
	movq	(%rsi), %rax
	jne	.L1996
	movq	%rax, -448(%rbp)
	movl	-416(%rbp), %esi
	addl	$1, %esi
	movl	%esi, -408(%rbp)
	testq	%rax, %rax
	jne	.L1997
	cmpl	%r11d, %r10d
	jl	.L1490
.L1497:
	movq	-384(%rbp), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1482
	cmpb	$0, -424(%rbp)
	jne	.L1482
	movl	-408(%rbp), %edi
	cmpl	%edi, -368(%rbp)
	jle	.L1500
	movl	-368(%rbp), %ecx
	movslq	%edi, %rsi
	leaq	(%rdx,%rsi,4), %rax
	subl	$1, %ecx
	subl	%edi, %ecx
	addq	%rsi, %rcx
	leaq	4(%rdx,%rcx,4), %rcx
	.p2align 4,,10
	.p2align 3
.L1501:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rcx
	jne	.L1501
.L1500:
	movl	-432(%rbp), %ecx
	movslq	-368(%rbp), %rax
	subl	$1, %ecx
	leaq	(%rdx,%rax,4), %rsi
	jmp	.L1499
	.p2align 4,,10
	.p2align 3
.L1998:
	movl	%r8d, %edi
	movl	%edx, %eax
	andl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L1482
.L1499:
	movl	(%rsi), %edx
	testl	%edx, %ecx
	jne	.L1998
	jmp	.L1482
	.p2align 4,,10
	.p2align 3
.L1968:
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1983:
	cmpl	$262144, %r13d
	jg	.L1515
	movl	-264(%rbp), %edi
	movl	%r13d, %esi
	movl	%r13d, %r11d
	sarl	$8, %esi
	sarl	$13, %r11d
	movl	%edi, %eax
	andl	$31, %esi
	movl	%edi, %r9d
	sarl	$3, %edi
	sarl	$8, %eax
	movl	%esi, -264(%rbp)
	movl	%edi, %ecx
	sarl	$13, %r9d
	andl	$31, %eax
	movl	-264(%rbp), %edi
	movl	%eax, -320(%rbp)
	movl	$1, %eax
	movl	%eax, %esi
	sall	%cl, %esi
	movl	%r13d, %ecx
	sarl	$3, %ecx
	leal	-1(%rsi), %r8d
	sall	%cl, %eax
	movl	%eax, %r13d
	movl	%eax, -368(%rbp)
	negl	%r13d
	cmpl	%r11d, %r9d
	sete	%dl
	cmpl	%edi, -320(%rbp)
	movslq	%r9d, %rdi
	sete	%al
	movq	%rdi, -408(%rbp)
	salq	$3, %rdi
	movq	%rdi, -384(%rbp)
	addq	%r10, %rdi
	andb	%al, %dl
	movq	%rdi, -344(%rbp)
	movb	%dl, -360(%rbp)
	movq	(%rdi), %rax
	jne	.L1999
	movq	%rax, -416(%rbp)
	movl	-320(%rbp), %edi
	addl	$1, %edi
	movl	%edi, -352(%rbp)
	testq	%rax, %rax
	jne	.L2000
	cmpl	%r11d, %r9d
	jl	.L1579
.L1586:
	movq	-344(%rbp), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1551
	cmpb	$0, -360(%rbp)
	jne	.L1551
	movl	-352(%rbp), %edi
	cmpl	%edi, -264(%rbp)
	jle	.L1589
	movl	-264(%rbp), %ecx
	movslq	%edi, %rsi
	leaq	(%rdx,%rsi,4), %rax
	subl	$1, %ecx
	subl	%edi, %ecx
	addq	%rsi, %rcx
	leaq	4(%rdx,%rcx,4), %rcx
	.p2align 4,,10
	.p2align 3
.L1590:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rcx
	jne	.L1590
.L1589:
	movl	-368(%rbp), %edi
	movslq	-264(%rbp), %rax
	subl	$1, %edi
	leaq	(%rdx,%rax,4), %rcx
	jmp	.L1588
	.p2align 4,,10
	.p2align 3
.L2001:
	movl	%r13d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L1551
.L1588:
	movl	(%rcx), %edx
	testl	%edx, %edi
	jne	.L2001
	jmp	.L1551
	.p2align 4,,10
	.p2align 3
.L1991:
	movslq	-320(%rbp), %rsi
	movl	-408(%rbp), %eax
	movl	%r15d, -424(%rbp)
	movq	-416(%rbp), %rcx
	movq	%rsi, -432(%rbp)
	negl	%eax
	salq	$2, %rsi
	movq	%rsi, -440(%rbp)
	addq	%rsi, %rcx
	movl	%eax, %r15d
	jmp	.L1464
	.p2align 4,,10
	.p2align 3
.L2002:
	movl	%r8d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L1915
.L1464:
	movl	(%rcx), %edx
	testl	%edx, %r15d
	jne	.L2002
.L1915:
	movl	-424(%rbp), %r15d
	cmpl	%r9d, %r10d
	jge	.L1946
	cmpl	$32, -352(%rbp)
	je	.L1468
	movq	-416(%rbp), %rdi
	movq	-440(%rbp), %rax
	movl	$30, %edx
	subl	-320(%rbp), %edx
	addq	-432(%rbp), %rdx
	leaq	4(%rdi,%rax), %rax
	leaq	8(%rdi,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L1469:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L1469
.L1468:
	leal	1(%r10), %eax
	cmpl	%eax, %r9d
	jle	.L2003
	movq	-344(%rbp), %rax
	leaq	8(%r11,%rax), %rdx
	movl	%r9d, %eax
	subl	%r10d, %eax
	subl	$2, %eax
	addq	-384(%rbp), %rax
	leaq	16(%r11,%rax,8), %rsi
	jmp	.L1471
	.p2align 4,,10
	.p2align 3
.L2005:
	addq	$8, %rdx
	cmpq	%rdx, %rsi
	je	.L2004
.L1471:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L2005
	leaq	128(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L1474:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rcx
	jne	.L1474
	addq	$8, %rdx
	cmpq	%rdx, %rsi
	jne	.L1471
.L2004:
	movl	$0, -352(%rbp)
	movslq	%r9d, %rax
	leaq	(%r11,%rax,8), %rdi
	movl	%r9d, %eax
	jmp	.L1472
.L1997:
	movslq	-416(%rbp), %rax
	movq	-448(%rbp), %rcx
	movl	%r15d, -460(%rbp)
	negl	%edi
	movl	-400(%rbp), %r15d
	movq	%rax, -472(%rbp)
	salq	$2, %rax
	movq	%rax, -480(%rbp)
	addq	%rax, %rcx
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L2006:
	movl	%r15d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L1918
.L1486:
	movl	(%rcx), %edx
	testl	%edx, %edi
	jne	.L2006
.L1918:
	movl	-460(%rbp), %r15d
	cmpl	%r11d, %r10d
	jge	.L1497
	cmpl	$32, -408(%rbp)
	je	.L1490
	movq	-448(%rbp), %rdi
	movq	-480(%rbp), %rax
	movl	$30, %edx
	subl	-416(%rbp), %edx
	addq	-472(%rbp), %rdx
	leaq	4(%rdi,%rax), %rax
	leaq	8(%rdi,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L1491:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L1491
.L1490:
	leal	1(%r10), %eax
	cmpl	%eax, %r11d
	jle	.L2007
	movq	-440(%rbp), %rax
	leaq	8(%r9,%rax), %rdx
	movl	%r11d, %eax
	subl	%r10d, %eax
	subl	$2, %eax
	addq	-456(%rbp), %rax
	leaq	16(%r9,%rax,8), %rsi
	jmp	.L1494
	.p2align 4,,10
	.p2align 3
.L1495:
	addq	$8, %rdx
	cmpq	%rdx, %rsi
	je	.L2008
.L1494:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L1495
	leaq	128(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L1496:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rcx
	jne	.L1496
	jmp	.L1495
	.p2align 4,,10
	.p2align 3
.L1515:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1667:
	movdqa	-240(%rbp), %xmm3
	movaps	%xmm3, -80(%rbp)
	cmpq	%r13, -256(%rbp)
	jnb	.L1665
	movq	%rdi, %r14
	movq	%r13, %r15
	jmp	.L1668
	.p2align 4,,10
	.p2align 3
.L1982:
	movl	%ecx, %edx
	movl	%ecx, %r10d
	sarl	$3, %ecx
	movl	%eax, %r9d
	sarl	$8, %edx
	sarl	$13, %r10d
	movl	%edx, %edi
	movl	%eax, %edx
	sarl	$13, %r9d
	sarl	$8, %edx
	andl	$31, %edi
	movl	%edx, %esi
	movl	$1, %edx
	movl	%edi, -320(%rbp)
	movl	%edx, %r8d
	andl	$31, %esi
	sall	%cl, %r8d
	movl	%eax, %ecx
	movl	%esi, -264(%rbp)
	sarl	$3, %ecx
	movl	%r8d, -408(%rbp)
	subl	$1, %r8d
	sall	%cl, %edx
	movl	%edx, %r13d
	movl	%edx, -360(%rbp)
	negl	%r13d
	cmpl	%r9d, %r10d
	sete	%dl
	cmpl	%edi, %esi
	sete	%al
	andl	%eax, %edx
	movslq	%r10d, %rax
	movq	%rax, -384(%rbp)
	salq	$3, %rax
	movb	%dl, -368(%rbp)
	movq	%rax, -344(%rbp)
	jmp	.L1699
	.p2align 4,,10
	.p2align 3
.L1502:
	movl	-264(%rbp), %esi
	movl	%r15d, -432(%rbp)
	movq	%r14, -440(%rbp)
	movl	%esi, %eax
	sarl	$3, %esi
	sarl	$8, %eax
	movl	%eax, %ecx
	andl	$31, %ecx
	movl	%ecx, -424(%rbp)
	movl	%esi, %ecx
	movl	$1, %esi
	sall	%cl, %esi
	movl	%esi, %ecx
	leal	-1(%rsi), %r8d
	movl	%eax, %esi
	andl	$31, %esi
	negl	%ecx
	movl	%r8d, %r15d
	leaq	0(,%rsi,4), %rax
	movq	%rsi, -448(%rbp)
	movl	%ecx, %r14d
	movq	-416(%rbp), %rsi
	movq	%rax, -456(%rbp)
	addq	%rax, %rsi
	jmp	.L1506
	.p2align 4,,10
	.p2align 3
.L2009:
	movl	%r15d, %r8d
	movl	%ecx, %eax
	andl	%ecx, %r8d
	lock cmpxchgl	%r8d, (%rsi)
	cmpl	%eax, %ecx
	je	.L1920
.L1506:
	movl	(%rsi), %ecx
	testl	%ecx, %r14d
	jne	.L2009
.L1920:
	cmpl	$31, -424(%rbp)
	movl	-432(%rbp), %r15d
	movq	-440(%rbp), %r14
	je	.L1507
	movq	-416(%rbp), %rsi
	movq	-456(%rbp), %rax
	movl	$30, %ecx
	subl	-424(%rbp), %ecx
	addq	-448(%rbp), %rcx
	leaq	4(%rsi,%rax), %rax
	leaq	8(%rsi,%rcx,4), %rcx
	.p2align 4,,10
	.p2align 3
.L1508:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rcx
	jne	.L1508
	jmp	.L1507
	.p2align 4,,10
	.p2align 3
.L1994:
	movl	-408(%rbp), %eax
	movslq	-320(%rbp), %rsi
	movl	%r15d, -368(%rbp)
	movq	-352(%rbp), %rcx
	movq	%rsi, -408(%rbp)
	negl	%eax
	salq	$2, %rsi
	movq	%rsi, -416(%rbp)
	addq	%rsi, %rcx
	movl	%eax, %r15d
	jmp	.L1555
	.p2align 4,,10
	.p2align 3
.L2010:
	movl	%r8d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L1926
.L1555:
	movl	(%rcx), %edx
	testl	%edx, %r15d
	jne	.L2010
.L1926:
	movl	-368(%rbp), %r15d
	cmpl	%r9d, %r10d
	jge	.L1563
	cmpl	$32, -344(%rbp)
	je	.L1559
	movq	-352(%rbp), %rdi
	movq	-416(%rbp), %rax
	movl	$30, %edx
	subl	-320(%rbp), %edx
	addq	-408(%rbp), %rdx
	leaq	4(%rdi,%rax), %rax
	leaq	8(%rdi,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L1560:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L1560
.L1559:
	leal	1(%r10), %esi
	cmpl	%esi, %r9d
	jle	.L2011
	movslq	%esi, %rax
	leaq	(%r11,%rax,8), %rdx
	leal	-2(%r9), %eax
	subl	%r10d, %eax
	addq	-384(%rbp), %rax
	leaq	16(%r11,%rax,8), %rdi
	jmp	.L1562
	.p2align 4,,10
	.p2align 3
.L2013:
	addq	$8, %rdx
	cmpq	%rdi, %rdx
	je	.L2012
.L1562:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L2013
	leaq	128(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L1565:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rcx, %rax
	jne	.L1565
	addq	$8, %rdx
	cmpq	%rdi, %rdx
	jne	.L1562
.L2012:
	movl	$0, -344(%rbp)
	leal	-1(%r9,%rsi), %eax
	subl	%r10d, %eax
	movl	%eax, %r10d
	cltq
	leaq	(%r11,%rax,8), %rdi
	jmp	.L1563
	.p2align 4,,10
	.p2align 3
.L1591:
	movl	-264(%rbp), %edi
	movl	%r15d, -384(%rbp)
	movl	%edi, %eax
	sarl	$3, %edi
	sarl	$8, %eax
	movl	%edi, %ecx
	movl	$1, %edi
	movl	%eax, %esi
	sall	%cl, %edi
	movq	-368(%rbp), %rcx
	andl	$31, %esi
	leal	-1(%rdi), %edx
	negl	%edi
	movl	%esi, -264(%rbp)
	movl	%eax, %esi
	movl	%edx, %r15d
	andl	$31, %esi
	leaq	0(,%rsi,4), %rax
	movq	%rsi, -408(%rbp)
	movq	%rax, -416(%rbp)
	addq	%rax, %rcx
	jmp	.L1595
	.p2align 4,,10
	.p2align 3
.L2014:
	movl	%r15d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L1931
.L1595:
	movl	(%rcx), %edx
	testl	%edx, %edi
	jne	.L2014
.L1931:
	cmpl	$31, -264(%rbp)
	movl	-384(%rbp), %r15d
	je	.L1596
	movq	-368(%rbp), %rdi
	movq	-416(%rbp), %rax
	movl	$30, %edx
	subl	-264(%rbp), %edx
	addq	-408(%rbp), %rdx
	leaq	4(%rdi,%rax), %rax
	leaq	8(%rdi,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L1597:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L1597
	jmp	.L1596
.L1976:
	xorl	%r9d, %r9d
	jmp	.L1533
	.p2align 4,,10
	.p2align 3
.L2015:
	movl	%ecx, %eax
	lock cmpxchgl	%r9d, (%rdx)
	cmpl	%eax, %ecx
	je	.L1532
.L1533:
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L2015
.L1532:
	leaq	4(%rdx), %rax
	subq	$-128, %rdx
	testl	%edi, %edi
	jle	.L1947
	.p2align 4,,10
	.p2align 3
.L1534:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L1534
.L1693:
	cmpl	$1, %edi
	jle	.L2016
	leal	-2(%rdi), %eax
	leaq	8(%rsi), %rdx
	leaq	16(%rsi,%rax,8), %r9
	jmp	.L1539
	.p2align 4,,10
	.p2align 3
.L1540:
	addq	$8, %rdx
	cmpq	%rdx, %r9
	je	.L2017
.L1539:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L1540
	leaq	128(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L1541:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rcx
	jne	.L1541
	jmp	.L1540
	.p2align 4,,10
	.p2align 3
.L2008:
	movslq	%r11d, %rax
	cmpl	$32, %r11d
	movl	$0, -408(%rbp)
	leaq	(%r9,%rax,8), %rax
	sete	-424(%rbp)
	movq	%rax, -384(%rbp)
	jmp	.L1497
	.p2align 4,,10
	.p2align 3
.L2017:
	movslq	%edi, %rax
	cmpl	$32, %edi
	leaq	(%rsi,%rax,8), %rsi
	sete	%al
	xorl	%edi, %edi
	jmp	.L1536
.L2000:
	movslq	-320(%rbp), %rax
	movq	-416(%rbp), %rcx
	negl	%esi
	movq	%rax, -424(%rbp)
	salq	$2, %rax
	movq	%rax, -432(%rbp)
	addq	%rax, %rcx
	jmp	.L1575
	.p2align 4,,10
	.p2align 3
.L2018:
	movl	%r8d, %edi
	movl	%edx, %eax
	andl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	je	.L1574
.L1575:
	movl	(%rcx), %edx
	testl	%edx, %esi
	jne	.L2018
.L1574:
	cmpl	%r11d, %r9d
	jge	.L1586
	cmpl	$32, -352(%rbp)
	je	.L1579
	movq	-416(%rbp), %rdi
	movq	-432(%rbp), %rax
	movl	$30, %edx
	subl	-320(%rbp), %edx
	addq	-424(%rbp), %rdx
	leaq	4(%rdi,%rax), %rax
	leaq	8(%rdi,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L1580:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L1580
.L1579:
	leal	1(%r9), %edx
	cmpl	%edx, %r11d
	jle	.L2019
	movq	-384(%rbp), %rax
	leaq	8(%r10,%rax), %rcx
	leal	-2(%r11), %eax
	subl	%r9d, %eax
	addq	-408(%rbp), %rax
	leaq	16(%r10,%rax,8), %rdi
	jmp	.L1583
	.p2align 4,,10
	.p2align 3
.L1584:
	addq	$8, %rcx
	cmpq	%rcx, %rdi
	je	.L2020
.L1583:
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L1584
	leaq	128(%rax), %rsi
	.p2align 4,,10
	.p2align 3
.L1585:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rsi
	jne	.L1585
	jmp	.L1584
.L1988:
	xorl	%r10d, %r10d
	jmp	.L1621
	.p2align 4,,10
	.p2align 3
.L2021:
	movl	%ecx, %eax
	lock cmpxchgl	%r10d, (%rdx)
	cmpl	%eax, %ecx
	je	.L1620
.L1621:
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L2021
.L1620:
	leaq	4(%rdx), %rax
	subq	$-128, %rdx
	testl	%r8d, %r8d
	jle	.L1948
	.p2align 4,,10
	.p2align 3
.L1622:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L1622
.L1688:
	cmpl	$1, %r8d
	jle	.L2022
	leal	-2(%r8), %eax
	leaq	8(%rsi), %rdx
	leaq	16(%rsi,%rax,8), %r10
	jmp	.L1627
	.p2align 4,,10
	.p2align 3
.L1628:
	addq	$8, %rdx
	cmpq	%rdx, %r10
	je	.L2023
.L1627:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L1628
	leaq	128(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L1629:
	addq	$4, %rax
	movl	$0, -4(%rax)
	cmpq	%rax, %rcx
	jne	.L1629
	jmp	.L1628
	.p2align 4,,10
	.p2align 3
.L2020:
	movl	$0, -352(%rbp)
	leal	-1(%r11,%rdx), %eax
	subl	%r9d, %eax
	movslq	%eax, %rdx
	cmpl	$32, %eax
	leaq	(%r10,%rdx,8), %rdi
	sete	-360(%rbp)
	movq	%rdi, -344(%rbp)
	jmp	.L1586
	.p2align 4,,10
	.p2align 3
.L2023:
	movslq	%r8d, %rax
	cmpl	$32, %r8d
	leaq	(%rsi,%rax,8), %rsi
	sete	%al
	xorl	%r8d, %r8d
	jmp	.L1624
.L1959:
	movq	-304(%rbp), %rax
	movq	120(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L2024
.L1676:
	movq	-304(%rbp), %rax
	movq	128(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1675
	leaq	-208(%rbp), %r12
	movq	%r12, %rsi
	call	_ZN2v88internal12TypedSlotSet17ClearInvalidSlotsERKSt3mapIjjSt4lessIjESaISt4pairIKjjEEE@PLT
	jmp	.L1675
.L2024:
	leaq	-208(%rbp), %r12
	movq	%r12, %rsi
	call	_ZN2v88internal12TypedSlotSet17ClearInvalidSlotsERKSt3mapIjjSt4lessIjESaISt4pairIKjjEEE@PLT
	jmp	.L1676
.L1975:
	movq	(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L1482
	subl	$1, %r11d
	jmp	.L1530
.L2025:
	movl	%r8d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L1482
.L1530:
	movl	(%rcx), %edx
	testl	%r11d, %edx
	jne	.L2025
	jmp	.L1482
	.p2align 4,,10
	.p2align 3
.L1957:
	movq	-304(%rbp), %rbx
	movl	-256(%rbp), %edx
	leaq	-208(%rbp), %r12
	leaq	-216(%rbp), %rsi
	movq	%r12, %rdi
	movq	48(%rbx), %rax
	subl	%ebx, %edx
	movl	%edx, -216(%rbp)
	movq	%rax, -240(%rbp)
	movl	-240(%rbp), %eax
	subl	%ebx, %eax
	movl	%eax, -212(%rbp)
	call	_ZNSt8_Rb_treeIjSt4pairIKjjESt10_Select1stIS2_ESt4lessIjESaIS2_EE17_M_emplace_uniqueIJS0_IjjEEEES0_ISt17_Rb_tree_iteratorIS2_EbEDpOT_
	jmp	.L1662
.L1956:
	movq	-256(%rbp), %rsi
	movq	-304(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal11MemoryChunk19DiscardUnusedMemoryEmm@PLT
	jmp	.L1661
.L1990:
	testq	%rax, %rax
	je	.L1460
	movl	%r8d, %edi
	movslq	-320(%rbp), %rdx
	orl	%r13d, %edi
	movl	%edi, %r11d
	leaq	(%rax,%rdx,4), %rcx
	notl	%r11d
	jmp	.L1461
.L2026:
	movl	%edi, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L1460
.L1461:
	movl	(%rcx), %edx
	testl	%edx, %r11d
	jne	.L2026
	jmp	.L1460
.L1993:
	testq	%rax, %rax
	je	.L1551
	movslq	-320(%rbp), %rdx
	orl	%r8d, %r13d
	movl	%r13d, %edi
	notl	%edi
	leaq	(%rax,%rdx,4), %rcx
	jmp	.L1552
.L2027:
	movl	%r13d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L1551
.L1552:
	movl	(%rcx), %edx
	testl	%edx, %edi
	jne	.L2027
	jmp	.L1551
.L1996:
	testq	%rax, %rax
	je	.L1482
	movslq	-416(%rbp), %rdx
	orl	-400(%rbp), %r8d
	movl	%r8d, %edi
	notl	%edi
	leaq	(%rax,%rdx,4), %rcx
	jmp	.L1483
.L2028:
	movl	%r8d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L1482
.L1483:
	movl	(%rcx), %edx
	testl	%edx, %edi
	jne	.L2028
	jmp	.L1482
.L1955:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1999:
	testq	%rax, %rax
	je	.L1551
	orl	%r13d, %r8d
	movslq	-320(%rbp), %r13
	movl	%r8d, %edi
	notl	%edi
	leaq	(%rax,%r13,4), %rcx
	jmp	.L1572
.L2029:
	movl	%r8d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L1551
.L1572:
	movl	(%rcx), %edx
	testl	%edx, %edi
	jne	.L2029
	jmp	.L1551
.L1987:
	movq	(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L1551
	subl	$1, %r11d
	jmp	.L1618
.L2030:
	movl	%r9d, %esi
	movl	%edx, %eax
	andl	%edx, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L1551
.L1618:
	movl	(%rcx), %edx
	testl	%edx, %r11d
	jne	.L2030
	jmp	.L1551
.L2003:
	movq	-344(%rbp), %rdi
	movl	$0, -352(%rbp)
	leaq	8(%r11,%rdi), %rdi
	jmp	.L1472
.L2011:
	movl	$0, -344(%rbp)
	movslq	%esi, %rax
	movl	%esi, %r10d
	leaq	(%r11,%rax,8), %rdi
	jmp	.L1563
.L2007:
	movq	-440(%rbp), %rdi
	cmpl	$32, %eax
	movl	$0, -408(%rbp)
	sete	-424(%rbp)
	leaq	8(%r9,%rdi), %rdi
	movq	%rdi, -384(%rbp)
	jmp	.L1497
.L2016:
	addq	$8, %rsi
	xorl	%eax, %eax
	xorl	%edi, %edi
	jmp	.L1536
.L2019:
	movq	-384(%rbp), %rax
	cmpl	$32, %edx
	movl	$0, -352(%rbp)
	sete	-360(%rbp)
	leaq	8(%r10,%rax), %rax
	movq	%rax, -344(%rbp)
	jmp	.L1586
.L2022:
	addq	$8, %rsi
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	jmp	.L1624
.L1960:
	call	__stack_chk_fail@PLT
.L1964:
	movq	-264(%rbp), %rax
	movl	%r14d, -384(%rbp)
	movq	%rax, -248(%rbp)
	jmp	.L1950
	.cfi_endproc
.LFE29304:
	.size	_ZN2v88internal7Sweeper8RawSweepEPNS0_4PageENS1_22FreeListRebuildingModeENS0_22FreeSpaceTreatmentModeENS1_35FreeSpaceMayContainInvalidatedSlotsE.constprop.0, .-_ZN2v88internal7Sweeper8RawSweepEPNS0_4PageENS1_22FreeListRebuildingModeENS0_22FreeSpaceTreatmentModeENS1_35FreeSpaceMayContainInvalidatedSlotsE.constprop.0
	.section	.text._ZN2v88internal7Sweeper26EnsureIterabilityCompletedEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal7Sweeper26EnsureIterabilityCompletedEv.part.0, @function
_ZN2v88internal7Sweeper26EnsureIterabilityCompletedEv.part.0:
.LFB29288:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_sweepingE(%rip)
	je	.L2032
	cmpb	$0, 353(%rdi)
	jne	.L2041
.L2032:
	movq	288(%r12), %rbx
	movq	296(%r12), %r13
	cmpq	%r13, %rbx
	je	.L2034
	.p2align 4,,10
	.p2align 3
.L2035:
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal7Sweeper8RawSweepEPNS0_4PageENS1_22FreeListRebuildingModeENS0_22FreeSpaceTreatmentModeENS1_35FreeSpaceMayContainInvalidatedSlotsE.constprop.0
	cmpq	%rbx, %r13
	jne	.L2035
	movq	288(%r12), %rax
	cmpq	296(%r12), %rax
	je	.L2034
	movq	%rax, 296(%r12)
.L2034:
	movb	$0, 352(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2041:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	312(%rdi), %rsi
	movq	8048(%rax), %rdi
	call	_ZN2v88internal21CancelableTaskManager8TryAbortEm@PLT
	cmpl	$2, %eax
	je	.L2033
	leaq	320(%r12), %rdi
	call	_ZN2v84base9Semaphore4WaitEv@PLT
.L2033:
	movb	$0, 353(%r12)
	jmp	.L2032
	.cfi_endproc
.LFE29288:
	.size	_ZN2v88internal7Sweeper26EnsureIterabilityCompletedEv.part.0, .-_ZN2v88internal7Sweeper26EnsureIterabilityCompletedEv.part.0
	.section	.text._ZN2v88internal7Sweeper26EnsureIterabilityCompletedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Sweeper26EnsureIterabilityCompletedEv
	.type	_ZN2v88internal7Sweeper26EnsureIterabilityCompletedEv, @function
_ZN2v88internal7Sweeper26EnsureIterabilityCompletedEv:
.LFB22439:
	.cfi_startproc
	endbr64
	cmpb	$0, 352(%rdi)
	je	.L2042
	jmp	_ZN2v88internal7Sweeper26EnsureIterabilityCompletedEv.part.0
	.p2align 4,,10
	.p2align 3
.L2042:
	ret
	.cfi_endproc
.LFE22439:
	.size	_ZN2v88internal7Sweeper26EnsureIterabilityCompletedEv, .-_ZN2v88internal7Sweeper26EnsureIterabilityCompletedEv
	.section	.rodata._ZN2v88internal7Sweeper15IterabilityTask11RunInternalEv.str1.1,"aMS",@progbits,1
.LC5:
	.string	"disabled-by-default-v8.gc"
	.section	.text._ZN2v88internal7Sweeper15IterabilityTask11RunInternalEv,"axG",@progbits,_ZN2v88internal7Sweeper15IterabilityTask11RunInternalEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Sweeper15IterabilityTask11RunInternalEv
	.type	_ZN2v88internal7Sweeper15IterabilityTask11RunInternalEv, @function
_ZN2v88internal7Sweeper15IterabilityTask11RunInternalEv:
.LFB22443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-144(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	56(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8GCTracer32worker_thread_runtime_call_statsEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeC1EPNS0_28WorkerThreadRuntimeCallStatsE@PLT
	movq	56(%r12), %rsi
	movl	$6, %edx
	movq	%r15, %rdi
	movq	-184(%rbp), %rcx
	call	_ZN2v88internal8GCTracer15BackgroundScopeC1EPS1_NS2_7ScopeIdEPNS0_16RuntimeCallStatsE@PLT
	movq	_ZZN2v88internal7Sweeper15IterabilityTask11RunInternalEvE28trace_event_unique_atomic578(%rip), %rdx
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L2069
.L2046:
	movq	$0, -176(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L2070
.L2048:
	movq	40(%r12), %rdi
	movq	288(%rdi), %rbx
	movq	296(%rdi), %r13
	cmpq	%rbx, %r13
	jne	.L2055
	jmp	.L2052
	.p2align 4,,10
	.p2align 3
.L2053:
	movq	40(%r12), %rdi
.L2055:
	movq	(%rbx), %rsi
	addq	$8, %rbx
	call	_ZN2v88internal7Sweeper8RawSweepEPNS0_4PageENS1_22FreeListRebuildingModeENS0_22FreeSpaceTreatmentModeENS1_35FreeSpaceMayContainInvalidatedSlotsE.constprop.0
	cmpq	%rbx, %r13
	jne	.L2053
	movq	40(%r12), %rax
	movq	288(%rax), %rdx
	cmpq	%rdx, 296(%rax)
	je	.L2052
	movq	%rdx, 296(%rax)
.L2052:
	movq	48(%r12), %rdi
	call	_ZN2v84base9Semaphore6SignalEv@PLT
	leaq	-176(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	%r15, %rdi
	call	_ZN2v88internal8GCTracer15BackgroundScopeD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2071
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2069:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2072
.L2047:
	movq	%r13, _ZZN2v88internal7Sweeper15IterabilityTask11RunInternalEvE28trace_event_unique_atomic578(%rip)
	jmp	.L2046
	.p2align 4,,10
	.p2align 3
.L2070:
	movl	$6, %edi
	xorl	%ebx, %ebx
	call	_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, -200(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2073
.L2049:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2050
	movq	(%rdi), %rax
	call	*8(%rax)
.L2050:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2051
	movq	(%rdi), %rax
	call	*8(%rax)
.L2051:
	movl	$6, %edi
	call	_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE@PLT
	movq	%r13, -168(%rbp)
	movq	%rax, -160(%rbp)
	leaq	-168(%rbp), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -176(%rbp)
	jmp	.L2048
.L2072:
	leaq	.LC5(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L2047
.L2073:
	subq	$8, %rsp
	leaq	-80(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r13, %rdx
	movl	$88, %esi
	pushq	%rcx
	movq	-200(%rbp), %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %rbx
	addq	$64, %rsp
	jmp	.L2049
.L2071:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22443:
	.size	_ZN2v88internal7Sweeper15IterabilityTask11RunInternalEv, .-_ZN2v88internal7Sweeper15IterabilityTask11RunInternalEv
	.section	.text._ZN2v88internal7Sweeper12MakeIterableEPNS0_4PageE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Sweeper12MakeIterableEPNS0_4PageE
	.type	_ZN2v88internal7Sweeper12MakeIterableEPNS0_4PageE, @function
_ZN2v88internal7Sweeper12MakeIterableEPNS0_4PageE:
.LFB22449:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal7Sweeper8RawSweepEPNS0_4PageENS1_22FreeListRebuildingModeENS0_22FreeSpaceTreatmentModeENS1_35FreeSpaceMayContainInvalidatedSlotsE.constprop.0
	.cfi_endproc
.LFE22449:
	.size	_ZN2v88internal7Sweeper12MakeIterableEPNS0_4PageE, .-_ZN2v88internal7Sweeper12MakeIterableEPNS0_4PageE
	.section	.rodata._ZNSt6vectorIPN2v88internal4PageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_.str1.1,"aMS",@progbits,1
.LC6:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN2v88internal4PageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal4PageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal4PageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal4PageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal4PageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB26609:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L2089
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L2085
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L2090
.L2077:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L2084:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L2091
	testq	%r13, %r13
	jg	.L2080
	testq	%r9, %r9
	jne	.L2083
.L2081:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2091:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L2080
.L2083:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L2081
	.p2align 4,,10
	.p2align 3
.L2090:
	testq	%rsi, %rsi
	jne	.L2078
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L2084
	.p2align 4,,10
	.p2align 3
.L2080:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L2081
	jmp	.L2083
	.p2align 4,,10
	.p2align 3
.L2085:
	movl	$8, %r14d
	jmp	.L2077
.L2089:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2078:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L2077
	.cfi_endproc
.LFE26609:
	.size	_ZNSt6vectorIPN2v88internal4PageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIPN2v88internal4PageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZN2v88internal7Sweeper17ParallelSweepPageEPNS0_4PageENS0_15AllocationSpaceENS1_35FreeSpaceMayContainInvalidatedSlotsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Sweeper17ParallelSweepPageEPNS0_4PageENS0_15AllocationSpaceENS1_35FreeSpaceMayContainInvalidatedSlotsE
	.type	_ZN2v88internal7Sweeper17ParallelSweepPageEPNS0_4PageENS0_15AllocationSpaceENS1_35FreeSpaceMayContainInvalidatedSlotsE, @function
_ZN2v88internal7Sweeper17ParallelSweepPageEPNS0_4PageENS0_15AllocationSpaceENS1_35FreeSpaceMayContainInvalidatedSlotsE:
.LFB22425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	movl	%edx, -60(%rbp)
	movq	168(%rsi), %rax
	testq	%rax, %rax
	jne	.L2124
.L2092:
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2124:
	.cfi_restore_state
	movq	-56(%rbp), %rax
	movq	%rdi, %rbx
	movl	%ecx, %r13d
	movq	160(%rax), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-56(%rbp), %rax
	movq	168(%rax), %rax
	testq	%rax, %rax
	je	.L2125
	movq	-56(%rbp), %r14
	movq	24(%r14), %rax
	movzbl	376(%rax), %eax
	movl	%eax, %esi
	movb	%al, -61(%rbp)
	movq	%r14, %rax
	testb	%sil, %sil
	jne	.L2126
.L2095:
	movq	%rbx, %rdi
	movl	%r13d, %r8d
	xorl	%ecx, %ecx
	movq	$2, 168(%rax)
	xorl	%edx, %edx
	mfence
	movq	-56(%rbp), %rsi
	call	_ZN2v88internal7Sweeper8RawSweepEPNS0_4PageENS1_22FreeListRebuildingModeENS0_22FreeSpaceTreatmentModeENS1_35FreeSpaceMayContainInvalidatedSlotsE
	movl	%eax, %r12d
	movq	-56(%rbp), %rax
	movq	120(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L2127
.L2096:
	movq	-56(%rbp), %rax
	movq	104(%rax), %r13
	testq	%r13, %r13
	jne	.L2128
.L2097:
	cmpb	$0, -61(%rbp)
	jne	.L2129
.L2104:
	movq	%r15, %rdi
	leaq	80(%rbx), %r13
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	-60(%rbp), %edx
	subl	$2, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rdx,2), %rdx
	salq	$3, %rdx
	leaq	(%rbx,%rdx), %rax
	movq	128(%rax), %rsi
	cmpq	136(%rax), %rsi
	je	.L2106
	movq	-56(%rbp), %rdx
	movq	%rdx, (%rsi)
	addq	$8, 128(%rax)
.L2107:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L2092
	.p2align 4,,10
	.p2align 3
.L2126:
	movzbl	8(%r14), %ecx
	andl	$1, %ecx
	movb	%cl, -61(%rbp)
	je	.L2095
	movq	%r14, %rdi
	call	_ZN2v88internal11MemoryChunk18SetReadAndWritableEv@PLT
	movq	-56(%rbp), %rax
	jmp	.L2095
	.p2align 4,,10
	.p2align 3
.L2129:
	cmpb	$0, _ZN2v88internal12FLAG_jitlessE(%rip)
	movq	%r14, %rdi
	je	.L2105
	call	_ZN2v88internal11MemoryChunk11SetReadableEv@PLT
	jmp	.L2104
	.p2align 4,,10
	.p2align 3
.L2128:
	leaq	264(%r13), %rax
	movq	%rax, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	jmp	.L2103
	.p2align 4,,10
	.p2align 3
.L2099:
	movq	-8(%rdi), %r8
	subq	$8, %rdi
	movq	%rdi, 352(%r13)
	testq	%r8, %r8
	je	.L2102
.L2100:
	movq	%r8, %rdi
	call	_ZdaPv@PLT
.L2103:
	movq	352(%r13), %rdi
.L2102:
	cmpq	%rdi, 320(%r13)
	je	.L2098
.L2130:
	cmpq	%rdi, 360(%r13)
	jne	.L2099
	movq	376(%r13), %rdx
	movq	-8(%rdx), %rdx
	movq	504(%rdx), %r8
	movq	%r8, -72(%rbp)
	call	_ZdlPv@PLT
	movq	376(%r13), %rdx
	movq	-72(%rbp), %r8
	leaq	-8(%rdx), %rcx
	movq	%rcx, 376(%r13)
	movq	-8(%rdx), %rdi
	leaq	512(%rdi), %rdx
	movq	%rdi, 360(%r13)
	addq	$504, %rdi
	movq	%rdx, 368(%r13)
	movq	%rdi, 352(%r13)
	testq	%r8, %r8
	jne	.L2100
	cmpq	%rdi, 320(%r13)
	jne	.L2130
	.p2align 4,,10
	.p2align 3
.L2098:
	movq	-80(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L2097
	.p2align 4,,10
	.p2align 3
.L2127:
	call	_ZN2v88internal12TypedSlotSet19FreeToBeFreedChunksEv@PLT
	jmp	.L2096
	.p2align 4,,10
	.p2align 3
.L2125:
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L2092
	.p2align 4,,10
	.p2align 3
.L2106:
	leaq	-56(%rbp), %r8
	leaq	120(%rbx,%rdx), %rdi
	movq	%r8, %rdx
	call	_ZNSt6vectorIPN2v88internal4PageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L2107
	.p2align 4,,10
	.p2align 3
.L2105:
	call	_ZN2v88internal11MemoryChunk20SetReadAndExecutableEv@PLT
	jmp	.L2104
	.cfi_endproc
.LFE22425:
	.size	_ZN2v88internal7Sweeper17ParallelSweepPageEPNS0_4PageENS0_15AllocationSpaceENS1_35FreeSpaceMayContainInvalidatedSlotsE, .-_ZN2v88internal7Sweeper17ParallelSweepPageEPNS0_4PageENS0_15AllocationSpaceENS1_35FreeSpaceMayContainInvalidatedSlotsE
	.section	.text._ZN2v88internal7Sweeper33SweepOrWaitUntilSweepingCompletedEPNS0_4PageE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Sweeper33SweepOrWaitUntilSweepingCompletedEPNS0_4PageE
	.type	_ZN2v88internal7Sweeper33SweepOrWaitUntilSweepingCompletedEPNS0_4PageE, @function
_ZN2v88internal7Sweeper33SweepOrWaitUntilSweepingCompletedEPNS0_4PageE:
.LFB22360:
	.cfi_startproc
	endbr64
	movq	168(%rsi), %rax
	testq	%rax, %rax
	jne	.L2144
	ret
	.p2align 4,,10
	.p2align 3
.L2144:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	testb	$32, 10(%rsi)
	je	.L2145
.L2134:
	movl	$1, %ecx
	movq	%rbx, %rsi
	call	_ZN2v88internal7Sweeper17ParallelSweepPageEPNS0_4PageENS0_15AllocationSpaceENS1_35FreeSpaceMayContainInvalidatedSlotsE
	movq	168(%rbx), %rax
	testq	%rax, %rax
	jne	.L2146
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2145:
	.cfi_restore_state
	movq	80(%rsi), %rax
	movl	72(%rax), %edx
	jmp	.L2134
	.p2align 4,,10
	.p2align 3
.L2146:
	movq	160(%rbx), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	160(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE22360:
	.size	_ZN2v88internal7Sweeper33SweepOrWaitUntilSweepingCompletedEPNS0_4PageE, .-_ZN2v88internal7Sweeper33SweepOrWaitUntilSweepingCompletedEPNS0_4PageE
	.section	.text._ZN2v88internal7Sweeper18SweepSpaceFromTaskENS0_15AllocationSpaceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Sweeper18SweepSpaceFromTaskENS0_15AllocationSpaceE
	.type	_ZN2v88internal7Sweeper18SweepSpaceFromTaskENS0_15AllocationSpaceE, @function
_ZN2v88internal7Sweeper18SweepSpaceFromTaskENS0_15AllocationSpaceE:
.LFB22422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-2(%rsi), %eax
	cltq
	leaq	(%rax,%rax,2), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	80(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	192(%rdi,%rax,8), %rbx
	leaq	280(%rdi), %rax
	subq	$24, %rsp
	movq	%rax, -56(%rbp)
	jmp	.L2150
	.p2align 4,,10
	.p2align 3
.L2149:
	movq	-8(%rax), %r15
	subq	$8, %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rbx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	testq	%r15, %r15
	je	.L2147
	movl	$1, %ecx
	movl	%r14d, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Sweeper17ParallelSweepPageEPNS0_4PageENS0_15AllocationSpaceENS1_35FreeSpaceMayContainInvalidatedSlotsE
.L2150:
	movq	-56(%rbp), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L2147
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	8(%rbx), %rax
	cmpq	(%rbx), %rax
	jne	.L2149
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
.L2147:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22422:
	.size	_ZN2v88internal7Sweeper18SweepSpaceFromTaskENS0_15AllocationSpaceE, .-_ZN2v88internal7Sweeper18SweepSpaceFromTaskENS0_15AllocationSpaceE
	.section	.text._ZN2v88internal7Sweeper31SweepSpaceIncrementallyFromTaskENS0_15AllocationSpaceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Sweeper31SweepSpaceIncrementallyFromTaskENS0_15AllocationSpaceE
	.type	_ZN2v88internal7Sweeper31SweepSpaceIncrementallyFromTaskENS0_15AllocationSpaceE, @function
_ZN2v88internal7Sweeper31SweepSpaceIncrementallyFromTaskENS0_15AllocationSpaceE:
.LFB22423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	80(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_ZN2v84base5Mutex4LockEv@PLT
	leal	-2(%r13), %eax
	cltq
	leaq	(%rax,%rax,2), %rax
	leaq	192(%r12,%rax,8), %rbx
	movq	8(%rbx), %rax
	cmpq	(%rbx), %rax
	je	.L2162
	movq	-8(%rax), %r15
	subq	$8, %rax
	movq	%r14, %rdi
	movq	%rax, 8(%rbx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	testq	%r15, %r15
	je	.L2157
	movl	$1, %ecx
	movl	%r13d, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Sweeper17ParallelSweepPageEPNS0_4PageENS0_15AllocationSpaceENS1_35FreeSpaceMayContainInvalidatedSlotsE
.L2157:
	movq	8(%rbx), %rax
	cmpq	%rax, (%rbx)
	sete	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2162:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L2157
	.cfi_endproc
.LFE22423:
	.size	_ZN2v88internal7Sweeper31SweepSpaceIncrementallyFromTaskENS0_15AllocationSpaceE, .-_ZN2v88internal7Sweeper31SweepSpaceIncrementallyFromTaskENS0_15AllocationSpaceE
	.section	.text._ZN2v88internal7Sweeper18ParallelSweepSpaceENS0_15AllocationSpaceEiiNS1_35FreeSpaceMayContainInvalidatedSlotsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Sweeper18ParallelSweepSpaceENS0_15AllocationSpaceEiiNS1_35FreeSpaceMayContainInvalidatedSlotsE
	.type	_ZN2v88internal7Sweeper18ParallelSweepSpaceENS0_15AllocationSpaceEiiNS1_35FreeSpaceMayContainInvalidatedSlotsE, @function
_ZN2v88internal7Sweeper18ParallelSweepSpaceENS0_15AllocationSpaceEiiNS1_35FreeSpaceMayContainInvalidatedSlotsE:
.LFB22424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-2(%rsi), %eax
	cltq
	leaq	(%rax,%rax,2), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	80(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	192(%rdi,%rax,8), %rbx
	subq	$24, %rsp
	movl	%esi, -52(%rbp)
	movl	%edx, -60(%rbp)
	movl	%r8d, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L2164:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	8(%rbx), %rax
	cmpq	(%rbx), %rax
	je	.L2178
	movq	-8(%rax), %r15
	subq	$8, %rax
	movq	%r13, %rdi
	movq	%rax, 8(%rbx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	testq	%r15, %r15
	je	.L2163
	movl	-56(%rbp), %ecx
	movl	-52(%rbp), %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Sweeper17ParallelSweepPageEPNS0_4PageENS0_15AllocationSpaceENS1_35FreeSpaceMayContainInvalidatedSlotsE
	testb	$16, 9(%r15)
	jne	.L2164
	cmpl	%eax, %r12d
	cmovl	%eax, %r12d
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jle	.L2164
	cmpl	%r12d, %eax
	jg	.L2164
.L2163:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2178:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L2163
	.cfi_endproc
.LFE22424:
	.size	_ZN2v88internal7Sweeper18ParallelSweepSpaceENS0_15AllocationSpaceEiiNS1_35FreeSpaceMayContainInvalidatedSlotsE, .-_ZN2v88internal7Sweeper18ParallelSweepSpaceENS0_15AllocationSpaceEiiNS1_35FreeSpaceMayContainInvalidatedSlotsE
	.section	.text._ZN2v88internal7Sweeper11SweeperTask11RunInternalEv,"axG",@progbits,_ZN2v88internal7Sweeper11SweeperTask11RunInternalEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Sweeper11SweeperTask11RunInternalEv
	.type	_ZN2v88internal7Sweeper11SweeperTask11RunInternalEv, @function
_ZN2v88internal7Sweeper11SweeperTask11RunInternalEv:
.LFB22347:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	72(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8GCTracer32worker_thread_runtime_call_statsEv@PLT
	movq	%rax, %rsi
	leaq	-184(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeC1EPNS0_28WorkerThreadRuntimeCallStatsE@PLT
	leaq	-144(%rbp), %rax
	movq	72(%r15), %rsi
	movq	-184(%rbp), %rcx
	movl	$6, %edx
	movq	%rax, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8GCTracer15BackgroundScopeC1EPS1_NS2_7ScopeIdEPNS0_16RuntimeCallStatsE@PLT
	movq	_ZZN2v88internal7Sweeper11SweeperTask11RunInternalEvE27trace_event_unique_atomic98(%rip), %r13
	testq	%r13, %r13
	je	.L2212
.L2181:
	movq	$0, -176(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L2213
.L2183:
	movl	64(%r15), %eax
	movl	%eax, -208(%rbp)
	leal	-2(%rax), %ebx
.L2191:
	movslq	%ebx, %rax
	movl	%ebx, %edx
	movl	%ebx, %ecx
	imulq	$1431655766, %rax, %rax
	sarl	$31, %edx
	shrq	$32, %rax
	subl	%edx, %eax
	leal	(%rax,%rax,2), %eax
	subl	%eax, %ecx
	movslq	%ecx, %rax
	leal	2(%rax), %ecx
	movl	%ecx, -204(%rbp)
	cmpl	$1, %eax
	je	.L2189
	movq	40(%r15), %r14
	leaq	(%rax,%rax,2), %rax
	leaq	192(%r14,%rax,8), %r12
	leaq	280(%r14), %rax
	movq	%rax, -216(%rbp)
	leaq	80(%r14), %r13
	jmp	.L2190
	.p2align 4,,10
	.p2align 3
.L2188:
	movq	-8(%rax), %rsi
	subq	$8, %rax
	movq	%r13, %rdi
	movq	%rax, 8(%r12)
	movq	%rsi, -200(%rbp)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-200(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L2189
	movl	-204(%rbp), %edx
	movl	$1, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal7Sweeper17ParallelSweepPageEPNS0_4PageENS0_15AllocationSpaceENS1_35FreeSpaceMayContainInvalidatedSlotsE
.L2190:
	movq	-216(%rbp), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L2189
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	8(%r12), %rax
	cmpq	(%r12), %rax
	jne	.L2188
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L2189:
	leal	1(%rbx), %eax
	cmpl	%ebx, -208(%rbp)
	je	.L2214
	movl	%eax, %ebx
	jmp	.L2191
	.p2align 4,,10
	.p2align 3
.L2214:
	movq	56(%r15), %rax
	lock subq	$1, (%rax)
	movq	48(%r15), %rdi
	call	_ZN2v84base9Semaphore6SignalEv@PLT
	leaq	-176(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-232(%rbp), %rdi
	call	_ZN2v88internal8GCTracer15BackgroundScopeD1Ev@PLT
	movq	-224(%rbp), %rdi
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2215
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2212:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2216
.L2182:
	movq	%r13, _ZZN2v88internal7Sweeper11SweeperTask11RunInternalEvE27trace_event_unique_atomic98(%rip)
	jmp	.L2181
	.p2align 4,,10
	.p2align 3
.L2213:
	movl	$6, %edi
	xorl	%ebx, %ebx
	call	_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %r12
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2217
.L2184:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2185
	movq	(%rdi), %rax
	call	*8(%rax)
.L2185:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2186
	movq	(%rdi), %rax
	call	*8(%rax)
.L2186:
	movl	$6, %edi
	call	_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE@PLT
	movq	%r13, -168(%rbp)
	movq	%rax, -160(%rbp)
	leaq	-168(%rbp), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -176(%rbp)
	jmp	.L2183
	.p2align 4,,10
	.p2align 3
.L2216:
	leaq	.LC5(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L2182
	.p2align 4,,10
	.p2align 3
.L2217:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r12, %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %rbx
	addq	$64, %rsp
	jmp	.L2184
.L2215:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22347:
	.size	_ZN2v88internal7Sweeper11SweeperTask11RunInternalEv, .-_ZN2v88internal7Sweeper11SweeperTask11RunInternalEv
	.section	.rodata._ZN2v88internal7Sweeper22IncrementalSweeperTask11RunInternalEv.str1.1,"aMS",@progbits,1
.LC7:
	.string	"v8"
.LC8:
	.string	"V8.Task"
	.section	.text._ZN2v88internal7Sweeper22IncrementalSweeperTask11RunInternalEv,"axG",@progbits,_ZN2v88internal7Sweeper22IncrementalSweeperTask11RunInternalEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Sweeper22IncrementalSweeperTask11RunInternalEv
	.type	_ZN2v88internal7Sweeper22IncrementalSweeperTask11RunInternalEv, @function
_ZN2v88internal7Sweeper22IncrementalSweeperTask11RunInternalEv:
.LFB22351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	40(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	12616(%r14), %r12d
	movl	$1, 12616(%r14)
	movq	_ZZN2v88internal7Sweeper22IncrementalSweeperTask11RunInternalEvE28trace_event_unique_atomic134(%rip), %rdx
	testq	%rdx, %rdx
	je	.L2238
	movq	$0, -88(%rbp)
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L2239
.L2222:
	movq	48(%rbx), %rax
	movb	$0, 264(%rax)
	movq	48(%rbx), %r13
	cmpb	$0, 265(%r13)
	jne	.L2240
.L2224:
	cmpq	$0, -88(%rbp)
	jne	.L2241
.L2228:
	movl	%r12d, 12616(%r14)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2242
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2238:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2243
.L2221:
	movq	%rdx, _ZZN2v88internal7Sweeper22IncrementalSweeperTask11RunInternalEvE28trace_event_unique_atomic134(%rip)
	movq	$0, -88(%rbp)
	movzbl	(%rdx), %eax
	testb	$5, %al
	je	.L2222
.L2239:
	movq	40(%rbx), %rsi
	leaq	-96(%rbp), %rdi
	leaq	.LC8(%rip), %rcx
	call	_ZN2v88internal7tracing21CallStatsScopedTracer10InitializeEPNS0_7IsolateEPKhPKc@PLT
	jmp	.L2222
	.p2align 4,,10
	.p2align 3
.L2240:
	leaq	80(%r13), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	224(%r13), %rax
	cmpq	216(%r13), %rax
	je	.L2244
	movq	-8(%rax), %rsi
	subq	$8, %rax
	movq	%r15, %rdi
	movq	%rax, 224(%r13)
	movq	%rsi, -104(%rbp)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-104(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L2226
	movl	$1, %ecx
	movl	$3, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal7Sweeper17ParallelSweepPageEPNS0_4PageENS0_15AllocationSpaceENS1_35FreeSpaceMayContainInvalidatedSlotsE
.L2226:
	movq	216(%r13), %rax
	cmpq	%rax, 224(%r13)
	je	.L2224
	movq	48(%rbx), %rdi
	cmpb	$0, 264(%rdi)
	jne	.L2224
	call	_ZN2v88internal7Sweeper31ScheduleIncrementalSweepingTaskEv.part.0
	jmp	.L2224
	.p2align 4,,10
	.p2align 3
.L2241:
	movq	-80(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L2228
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal7tracing21CallStatsScopedTracer16AddEndTraceEventEv@PLT
	jmp	.L2228
	.p2align 4,,10
	.p2align 3
.L2243:
	leaq	.LC7(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L2221
	.p2align 4,,10
	.p2align 3
.L2244:
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L2226
.L2242:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22351:
	.size	_ZN2v88internal7Sweeper22IncrementalSweeperTask11RunInternalEv, .-_ZN2v88internal7Sweeper22IncrementalSweeperTask11RunInternalEv
	.section	.rodata._ZN2v88internal7Sweeper15EnsureCompletedEv.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"sweeping_list_[GetSweepSpaceIndex(space)].empty()"
	.section	.text._ZN2v88internal7Sweeper15EnsureCompletedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Sweeper15EnsureCompletedEv
	.type	_ZN2v88internal7Sweeper15EnsureCompletedEv, @function
_ZN2v88internal7Sweeper15EnsureCompletedEv:
.LFB22363:
	.cfi_startproc
	endbr64
	cmpb	$0, 265(%rdi)
	je	.L2276
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 352(%rdi)
	je	.L2247
	call	_ZN2v88internal7Sweeper26EnsureIterabilityCompletedEv.part.0
.L2247:
	leaq	80(%rbx), %r12
	jmp	.L2251
	.p2align 4,,10
	.p2align 3
.L2248:
	movq	-8(%rax), %r13
	subq	$8, %rax
	movq	%r12, %rdi
	movq	%rax, 200(%rbx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	testq	%r13, %r13
	je	.L2249
	movl	$1, %ecx
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Sweeper17ParallelSweepPageEPNS0_4PageENS0_15AllocationSpaceENS1_35FreeSpaceMayContainInvalidatedSlotsE
.L2251:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	200(%rbx), %rax
	cmpq	192(%rbx), %rax
	jne	.L2248
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L2249
	.p2align 4,,10
	.p2align 3
.L2252:
	movq	-8(%rax), %r13
	subq	$8, %rax
	movq	%r12, %rdi
	movq	%rax, 224(%rbx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	testq	%r13, %r13
	je	.L2253
	movl	$1, %ecx
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Sweeper17ParallelSweepPageEPNS0_4PageENS0_15AllocationSpaceENS1_35FreeSpaceMayContainInvalidatedSlotsE
.L2249:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	224(%rbx), %rax
	cmpq	216(%rbx), %rax
	jne	.L2252
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L2253
	.p2align 4,,10
	.p2align 3
.L2255:
	movq	-8(%rax), %r13
	subq	$8, %rax
	movq	%r12, %rdi
	movq	%rax, 248(%rbx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	testq	%r13, %r13
	je	.L2256
	movl	$1, %ecx
	movl	$4, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Sweeper17ParallelSweepPageEPNS0_4PageENS0_15AllocationSpaceENS1_35FreeSpaceMayContainInvalidatedSlotsE
.L2253:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	248(%rbx), %rax
	cmpq	240(%rbx), %rax
	jne	.L2255
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L2256:
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_sweepingE(%rip)
	je	.L2257
	movl	16(%rbx), %eax
	xorl	%r12d, %r12d
	leaq	48(%rbx), %r13
	testl	%eax, %eax
	jle	.L2260
.L2262:
	movq	(%rbx), %rax
	movq	24(%rbx,%r12,8), %rsi
	movq	8048(%rax), %rdi
	call	_ZN2v88internal21CancelableTaskManager8TryAbortEm@PLT
	cmpl	$2, %eax
	je	.L2259
	movq	%r13, %rdi
	addq	$1, %r12
	call	_ZN2v84base9Semaphore4WaitEv@PLT
	cmpl	%r12d, 16(%rbx)
	jg	.L2262
.L2260:
	movl	$0, 16(%rbx)
.L2257:
	movq	192(%rbx), %rax
	cmpq	%rax, 200(%rbx)
	jne	.L2264
	movq	216(%rbx), %rax
	cmpq	%rax, 224(%rbx)
	jne	.L2264
	movq	240(%rbx), %rax
	cmpq	%rax, 248(%rbx)
	jne	.L2264
	movb	$0, 265(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2259:
	.cfi_restore_state
	lock subq	$1, 272(%rbx)
	addq	$1, %r12
	cmpl	%r12d, 16(%rbx)
	jg	.L2262
	movl	$0, 16(%rbx)
	jmp	.L2257
	.p2align 4,,10
	.p2align 3
.L2276:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L2264:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	leaq	.LC9(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22363:
	.size	_ZN2v88internal7Sweeper15EnsureCompletedEv, .-_ZN2v88internal7Sweeper15EnsureCompletedEv
	.section	.text._ZN2v88internal7Sweeper20EnsurePageIsIterableEPNS0_4PageE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Sweeper20EnsurePageIsIterableEPNS0_4PageE
	.type	_ZN2v88internal7Sweeper20EnsurePageIsIterableEPNS0_4PageE, @function
_ZN2v88internal7Sweeper20EnsurePageIsIterableEPNS0_4PageE:
.LFB22438:
	.cfi_startproc
	endbr64
	testb	$32, 10(%rsi)
	je	.L2295
	cmpb	$0, 352(%rdi)
	je	.L2291
	jmp	_ZN2v88internal7Sweeper26EnsureIterabilityCompletedEv.part.0
	.p2align 4,,10
	.p2align 3
.L2291:
	ret
	.p2align 4,,10
	.p2align 3
.L2295:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	80(%rsi), %rax
	movl	72(%rax), %eax
	subl	$2, %eax
	cmpl	$2, %eax
	ja	.L2280
	movq	168(%rsi), %rax
	testq	%rax, %rax
	je	.L2279
	xorl	%edx, %edx
	testb	$32, 10(%rsi)
	jne	.L2283
	movq	80(%rsi), %rax
	movl	72(%rax), %edx
.L2283:
	movl	$1, %ecx
	movq	%rbx, %rsi
	call	_ZN2v88internal7Sweeper17ParallelSweepPageEPNS0_4PageENS0_15AllocationSpaceENS1_35FreeSpaceMayContainInvalidatedSlotsE
	movq	168(%rbx), %rax
	testq	%rax, %rax
	je	.L2279
	movq	160(%rbx), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	160(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L2279:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2280:
	.cfi_restore_state
	cmpb	$0, 352(%rdi)
	je	.L2279
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Sweeper26EnsureIterabilityCompletedEv.part.0
	.cfi_endproc
.LFE22438:
	.size	_ZN2v88internal7Sweeper20EnsurePageIsIterableEPNS0_4PageE, .-_ZN2v88internal7Sweeper20EnsurePageIsIterableEPNS0_4PageE
	.section	.text._ZN2v88internal7Sweeper21AddPageForIterabilityEPNS0_4PageE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Sweeper21AddPageForIterabilityEPNS0_4PageE
	.type	_ZN2v88internal7Sweeper21AddPageForIterabilityEPNS0_4PageE, @function
_ZN2v88internal7Sweeper21AddPageForIterabilityEPNS0_4PageE:
.LFB22448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rsi, -8(%rbp)
	movq	296(%rdi), %rsi
	cmpq	304(%rdi), %rsi
	je	.L2297
	movq	-8(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 296(%rdi)
.L2298:
	movq	-8(%rbp), %rax
	movq	$1, 168(%rax)
	mfence
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2297:
	.cfi_restore_state
	leaq	-8(%rbp), %rdx
	addq	$288, %rdi
	call	_ZNSt6vectorIPN2v88internal4PageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L2298
	.cfi_endproc
.LFE22448:
	.size	_ZN2v88internal7Sweeper21AddPageForIterabilityEPNS0_4PageE, .-_ZN2v88internal7Sweeper21AddPageForIterabilityEPNS0_4PageE
	.section	.text._ZN2v88internal7Sweeper7AddPageENS0_15AllocationSpaceEPNS0_4PageENS1_11AddPageModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Sweeper7AddPageENS0_15AllocationSpaceEPNS0_4PageENS1_11AddPageModeE
	.type	_ZN2v88internal7Sweeper7AddPageENS0_15AllocationSpaceEPNS0_4PageENS1_11AddPageModeE, @function
_ZN2v88internal7Sweeper7AddPageENS0_15AllocationSpaceEPNS0_4PageENS1_11AddPageModeE:
.LFB22435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	80(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	subq	$16, %rsp
	movq	%rdx, -40(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	testl	%r13d, %r13d
	je	.L2305
.L2301:
	leal	-2(%rbx), %esi
	movslq	%esi, %rsi
	leaq	(%rsi,%rsi,2), %rdx
	salq	$3, %rdx
	leaq	(%r12,%rdx), %rax
	movq	200(%rax), %rsi
	cmpq	208(%rax), %rsi
	je	.L2302
	movq	-40(%rbp), %rdx
	movq	%rdx, (%rsi)
	addq	$8, 200(%rax)
.L2303:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2305:
	.cfi_restore_state
	movq	-40(%rbp), %rdx
	movslq	%ebx, %rcx
	movq	$1, 168(%rdx)
	mfence
	movq	(%r12), %rax
	movq	96(%rdx), %rdx
	movq	312(%rax,%rcx,8), %rax
	addq	%rdx, 184(%rax)
	jmp	.L2301
	.p2align 4,,10
	.p2align 3
.L2302:
	leaq	-40(%rbp), %r8
	leaq	192(%r12,%rdx), %rdi
	movq	%r8, %rdx
	call	_ZNSt6vectorIPN2v88internal4PageESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L2303
	.cfi_endproc
.LFE22435:
	.size	_ZN2v88internal7Sweeper7AddPageENS0_15AllocationSpaceEPNS0_4PageENS1_11AddPageModeE, .-_ZN2v88internal7Sweeper7AddPageENS0_15AllocationSpaceEPNS0_4PageENS1_11AddPageModeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal7SweeperC2EPNS0_4HeapEPNS0_26MajorNonAtomicMarkingStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal7SweeperC2EPNS0_4HeapEPNS0_26MajorNonAtomicMarkingStateE, @function
_GLOBAL__sub_I__ZN2v88internal7SweeperC2EPNS0_4HeapEPNS0_26MajorNonAtomicMarkingStateE:
.LFB29064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29064:
	.size	_GLOBAL__sub_I__ZN2v88internal7SweeperC2EPNS0_4HeapEPNS0_26MajorNonAtomicMarkingStateE, .-_GLOBAL__sub_I__ZN2v88internal7SweeperC2EPNS0_4HeapEPNS0_26MajorNonAtomicMarkingStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal7SweeperC2EPNS0_4HeapEPNS0_26MajorNonAtomicMarkingStateE
	.weak	_ZTVN2v88internal14CancelableTaskE
	.section	.data.rel.ro._ZTVN2v88internal14CancelableTaskE,"awG",@progbits,_ZTVN2v88internal14CancelableTaskE,comdat
	.align 8
	.type	_ZTVN2v88internal14CancelableTaskE, @object
	.size	_ZTVN2v88internal14CancelableTaskE, 88
_ZTVN2v88internal14CancelableTaskE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	__cxa_pure_virtual
	.quad	-32
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.weak	_ZTVN2v88internal7Sweeper11SweeperTaskE
	.section	.data.rel.ro.local._ZTVN2v88internal7Sweeper11SweeperTaskE,"awG",@progbits,_ZTVN2v88internal7Sweeper11SweeperTaskE,comdat
	.align 8
	.type	_ZTVN2v88internal7Sweeper11SweeperTaskE, @object
	.size	_ZTVN2v88internal7Sweeper11SweeperTaskE, 88
_ZTVN2v88internal7Sweeper11SweeperTaskE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal7Sweeper11SweeperTaskD1Ev
	.quad	_ZN2v88internal7Sweeper11SweeperTaskD0Ev
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	_ZN2v88internal7Sweeper11SweeperTask11RunInternalEv
	.quad	-32
	.quad	0
	.quad	_ZThn32_N2v88internal7Sweeper11SweeperTaskD1Ev
	.quad	_ZThn32_N2v88internal7Sweeper11SweeperTaskD0Ev
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.weak	_ZTVN2v88internal7Sweeper22IncrementalSweeperTaskE
	.section	.data.rel.ro.local._ZTVN2v88internal7Sweeper22IncrementalSweeperTaskE,"awG",@progbits,_ZTVN2v88internal7Sweeper22IncrementalSweeperTaskE,comdat
	.align 8
	.type	_ZTVN2v88internal7Sweeper22IncrementalSweeperTaskE, @object
	.size	_ZTVN2v88internal7Sweeper22IncrementalSweeperTaskE, 88
_ZTVN2v88internal7Sweeper22IncrementalSweeperTaskE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal7Sweeper22IncrementalSweeperTaskD1Ev
	.quad	_ZN2v88internal7Sweeper22IncrementalSweeperTaskD0Ev
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	_ZN2v88internal7Sweeper22IncrementalSweeperTask11RunInternalEv
	.quad	-32
	.quad	0
	.quad	_ZThn32_N2v88internal7Sweeper22IncrementalSweeperTaskD1Ev
	.quad	_ZThn32_N2v88internal7Sweeper22IncrementalSweeperTaskD0Ev
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.weak	_ZTVN2v88internal7Sweeper15IterabilityTaskE
	.section	.data.rel.ro.local._ZTVN2v88internal7Sweeper15IterabilityTaskE,"awG",@progbits,_ZTVN2v88internal7Sweeper15IterabilityTaskE,comdat
	.align 8
	.type	_ZTVN2v88internal7Sweeper15IterabilityTaskE, @object
	.size	_ZTVN2v88internal7Sweeper15IterabilityTaskE, 88
_ZTVN2v88internal7Sweeper15IterabilityTaskE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal7Sweeper15IterabilityTaskD1Ev
	.quad	_ZN2v88internal7Sweeper15IterabilityTaskD0Ev
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	_ZN2v88internal7Sweeper15IterabilityTask11RunInternalEv
	.quad	-32
	.quad	0
	.quad	_ZThn32_N2v88internal7Sweeper15IterabilityTaskD1Ev
	.quad	_ZThn32_N2v88internal7Sweeper15IterabilityTaskD0Ev
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.weak	_ZZN2v88internal7Sweeper15IterabilityTask11RunInternalEvE28trace_event_unique_atomic578
	.section	.bss._ZZN2v88internal7Sweeper15IterabilityTask11RunInternalEvE28trace_event_unique_atomic578,"awG",@nobits,_ZZN2v88internal7Sweeper15IterabilityTask11RunInternalEvE28trace_event_unique_atomic578,comdat
	.align 8
	.type	_ZZN2v88internal7Sweeper15IterabilityTask11RunInternalEvE28trace_event_unique_atomic578, @gnu_unique_object
	.size	_ZZN2v88internal7Sweeper15IterabilityTask11RunInternalEvE28trace_event_unique_atomic578, 8
_ZZN2v88internal7Sweeper15IterabilityTask11RunInternalEvE28trace_event_unique_atomic578:
	.zero	8
	.weak	_ZZN2v88internal7Sweeper22IncrementalSweeperTask11RunInternalEvE28trace_event_unique_atomic134
	.section	.bss._ZZN2v88internal7Sweeper22IncrementalSweeperTask11RunInternalEvE28trace_event_unique_atomic134,"awG",@nobits,_ZZN2v88internal7Sweeper22IncrementalSweeperTask11RunInternalEvE28trace_event_unique_atomic134,comdat
	.align 8
	.type	_ZZN2v88internal7Sweeper22IncrementalSweeperTask11RunInternalEvE28trace_event_unique_atomic134, @gnu_unique_object
	.size	_ZZN2v88internal7Sweeper22IncrementalSweeperTask11RunInternalEvE28trace_event_unique_atomic134, 8
_ZZN2v88internal7Sweeper22IncrementalSweeperTask11RunInternalEvE28trace_event_unique_atomic134:
	.zero	8
	.weak	_ZZN2v88internal7Sweeper11SweeperTask11RunInternalEvE27trace_event_unique_atomic98
	.section	.bss._ZZN2v88internal7Sweeper11SweeperTask11RunInternalEvE27trace_event_unique_atomic98,"awG",@nobits,_ZZN2v88internal7Sweeper11SweeperTask11RunInternalEvE27trace_event_unique_atomic98,comdat
	.align 8
	.type	_ZZN2v88internal7Sweeper11SweeperTask11RunInternalEvE27trace_event_unique_atomic98, @gnu_unique_object
	.size	_ZZN2v88internal7Sweeper11SweeperTask11RunInternalEvE27trace_event_unique_atomic98, 8
_ZZN2v88internal7Sweeper11SweeperTask11RunInternalEvE27trace_event_unique_atomic98:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
