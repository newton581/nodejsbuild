	.file	"transitions.cc"
	.text
	.section	.rodata._ZN2v88internal19TransitionsAccessor21HasSimpleTransitionToENS0_3MapE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal19TransitionsAccessor21HasSimpleTransitionToENS0_3MapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19TransitionsAccessor21HasSimpleTransitionToENS0_3MapE
	.type	_ZN2v88internal19TransitionsAccessor21HasSimpleTransitionToENS0_3MapE, @function
_ZN2v88internal19TransitionsAccessor21HasSimpleTransitionToENS0_3MapE:
.LFB17783:
	.cfi_startproc
	endbr64
	movl	32(%rdi), %eax
	cmpl	$3, %eax
	je	.L2
	jbe	.L6
	cmpl	$4, %eax
	jne	.L12
.L6:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	24(%rdi), %rax
	andq	$-3, %rax
	cmpq	%rsi, %rax
	sete	%al
	ret
.L12:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17783:
	.size	_ZN2v88internal19TransitionsAccessor21HasSimpleTransitionToENS0_3MapE, .-_ZN2v88internal19TransitionsAccessor21HasSimpleTransitionToENS0_3MapE
	.section	.text._ZN2v88internal19TransitionsAccessor19IsSpecialTransitionENS0_13ReadOnlyRootsENS0_4NameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19TransitionsAccessor19IsSpecialTransitionENS0_13ReadOnlyRootsENS0_4NameE
	.type	_ZN2v88internal19TransitionsAccessor19IsSpecialTransitionENS0_13ReadOnlyRootsENS0_4NameE, @function
_ZN2v88internal19TransitionsAccessor19IsSpecialTransitionENS0_13ReadOnlyRootsENS0_4NameE:
.LFB17787:
	.cfi_startproc
	endbr64
	movq	-1(%rsi), %rdx
	xorl	%eax, %eax
	cmpw	$64, 11(%rdx)
	jne	.L13
	movl	$1, %eax
	cmpq	%rsi, 3696(%rdi)
	je	.L13
	cmpq	%rsi, 3744(%rdi)
	je	.L13
	cmpq	%rsi, 3648(%rdi)
	je	.L13
	cmpq	%rsi, 3616(%rdi)
	je	.L13
	cmpq	%rsi, 3760(%rdi)
	sete	%al
.L13:
	ret
	.cfi_endproc
.LFE17787:
	.size	_ZN2v88internal19TransitionsAccessor19IsSpecialTransitionENS0_13ReadOnlyRootsENS0_4NameE, .-_ZN2v88internal19TransitionsAccessor19IsSpecialTransitionENS0_13ReadOnlyRootsENS0_4NameE
	.section	.text._ZN2v88internal19TransitionsAccessor22CanHaveMoreTransitionsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19TransitionsAccessor22CanHaveMoreTransitionsEv
	.type	_ZN2v88internal19TransitionsAccessor22CanHaveMoreTransitionsEv, @function
_ZN2v88internal19TransitionsAccessor22CanHaveMoreTransitionsEv:
.LFB17792:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movl	15(%rax), %edx
	xorl	%eax, %eax
	andl	$2097152, %edx
	jne	.L20
	cmpl	$4, 32(%rdi)
	movl	$1, %eax
	je	.L26
.L20:
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movq	24(%rdi), %rdx
	cmpl	$1, 11(%rdx)
	jle	.L20
	movq	23(%rdx), %rax
	sarq	$32, %rax
	cmpl	$1535, %eax
	setle	%al
	ret
	.cfi_endproc
.LFE17792:
	.size	_ZN2v88internal19TransitionsAccessor22CanHaveMoreTransitionsEv, .-_ZN2v88internal19TransitionsAccessor22CanHaveMoreTransitionsEv
	.section	.text._ZN2v88internal19TransitionsAccessor13IsMatchingMapENS0_3MapENS0_4NameENS0_12PropertyKindENS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19TransitionsAccessor13IsMatchingMapENS0_3MapENS0_4NameENS0_12PropertyKindENS0_18PropertyAttributesE
	.type	_ZN2v88internal19TransitionsAccessor13IsMatchingMapENS0_3MapENS0_4NameENS0_12PropertyKindENS0_18PropertyAttributesE, @function
_ZN2v88internal19TransitionsAccessor13IsMatchingMapENS0_3MapENS0_4NameENS0_12PropertyKindENS0_18PropertyAttributesE:
.LFB17793:
	.cfi_startproc
	endbr64
	movl	15(%rdi), %eax
	movq	39(%rdi), %rdi
	movq	%rsi, %r9
	movl	%edx, %esi
	shrl	$10, %eax
	andl	$1023, %eax
	leal	(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	leaq	-1(%rdi,%rax), %rdi
	xorl	%eax, %eax
	movq	(%rdi), %r8
	cmpq	%r8, %r9
	je	.L30
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movq	8(%rdi), %rax
	leal	0(,%rcx,8), %edx
	orl	%esi, %edx
	sarq	$32, %rax
	andl	$57, %eax
	cmpl	%edx, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE17793:
	.size	_ZN2v88internal19TransitionsAccessor13IsMatchingMapENS0_3MapENS0_4NameENS0_12PropertyKindENS0_18PropertyAttributesE, .-_ZN2v88internal19TransitionsAccessor13IsMatchingMapENS0_3MapENS0_4NameENS0_12PropertyKindENS0_18PropertyAttributesE
	.section	.text._ZN2v88internal15TransitionArray31CompactPrototypeTransitionArrayEPNS0_7IsolateENS0_14WeakFixedArrayE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TransitionArray31CompactPrototypeTransitionArrayEPNS0_7IsolateENS0_14WeakFixedArrayE
	.type	_ZN2v88internal15TransitionArray31CompactPrototypeTransitionArrayEPNS0_7IsolateENS0_14WeakFixedArrayE, @function
_ZN2v88internal15TransitionArray31CompactPrototypeTransitionArrayEPNS0_7IsolateENS0_14WeakFixedArrayE:
.LFB17794:
	.cfi_startproc
	endbr64
	movl	11(%rsi), %eax
	testl	%eax, %eax
	jne	.L32
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	15(%rsi), %r8
	sarq	$32, %r8
	testq	%r8, %r8
	je	.L34
	jle	.L55
	movq	%rdi, %r10
	movq	%rsi, %rdi
	leal	-1(%r8), %ecx
	xorl	%ebx, %ebx
	leaq	-1(%rsi), %r11
	leaq	23(%rsi), %r12
	movq	%rcx, %r9
	xorl	%r14d, %r14d
	andq	$-262144, %rdi
	movq	%rdi, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L36:
	movq	(%r12), %r13
	cmpl	$3, %r13d
	je	.L38
	cmpl	%ebx, %r14d
	je	.L39
	leal	24(,%r14,8), %r15d
	movslq	%r15d, %r15
	addq	%r11, %r15
	movq	%r13, (%r15)
	testb	$1, %r13b
	je	.L39
	movq	%r13, %rdx
	andq	$-262144, %r13
	movq	8(%r13), %rsi
	andq	$-3, %rdx
	testl	$262144, %esi
	je	.L41
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%r10, -112(%rbp)
	movq	%r11, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movl	%r9d, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r13), %rsi
	movq	-112(%rbp), %r10
	movq	-104(%rbp), %r11
	movq	-96(%rbp), %rcx
	movl	-88(%rbp), %r9d
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rax
.L41:
	andl	$24, %esi
	je	.L39
	movq	-56(%rbp), %rdi
	testb	$24, 8(%rdi)
	je	.L69
	.p2align 4,,10
	.p2align 3
.L39:
	addl	$1, %r14d
.L38:
	leaq	1(%rbx), %rdx
	addq	$8, %r12
	cmpq	%rbx, %rcx
	je	.L37
	movq	%rdx, %rbx
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L37:
	movq	88(%r10), %rbx
	cmpl	%r14d, %r8d
	jle	.L35
	testb	$1, %bl
	je	.L43
	cmpl	$3, %ebx
	je	.L70
	movslq	%r14d, %r15
	subl	%r14d, %r9d
	movq	%r8, -64(%rbp)
	movq	%rax, %r13
	addq	%r15, %r9
	leal	24(,%r14,8), %r12d
	movl	%r14d, -72(%rbp)
	movq	%rbx, %r14
	leaq	31(%rax,%r9,8), %rcx
	movslq	%r12d, %r12
	movq	%rbx, %r9
	movq	%r15, -80(%rbp)
	addq	%r11, %r12
	andq	$-3, %r9
	movq	%rcx, %r15
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%r14, (%r12)
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rdx
	testl	$262144, %edx
	je	.L51
	movq	%r9, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rdx
	movq	-56(%rbp), %r9
.L51:
	andl	$24, %edx
	je	.L50
	movq	%r13, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L50
	movq	%r9, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %r9
.L50:
	addq	$8, %r12
	cmpq	%r12, %r15
	jne	.L49
	movq	-64(%rbp), %r8
	movl	-72(%rbp), %r14d
	movq	%r13, %rax
	movq	-80(%rbp), %r15
.L45:
	salq	$32, %r15
	movq	%r15, 15(%rax)
	jmp	.L53
.L55:
	xorl	%r14d, %r14d
.L35:
	cmpl	%r8d, %r14d
	jne	.L71
.L53:
	cmpl	%r14d, %r8d
	setg	%al
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	leal	24(,%r14,8), %edx
	subl	%r14d, %r9d
	movslq	%r14d, %r15
	movslq	%edx, %rdx
	addq	%r15, %r9
	addq	%rdx, %r11
	leaq	31(%rax,%r9,8), %rdx
	.p2align 4,,10
	.p2align 3
.L52:
	movq	%rbx, (%r11)
	addq	$8, %r11
	cmpq	%r11, %rdx
	jne	.L52
	jmp	.L45
.L34:
	addq	$72, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	movq	%rax, %rdi
	movq	%r15, %rsi
	movq	%r10, -104(%rbp)
	movq	%r11, -96(%rbp)
	movq	%rcx, -88(%rbp)
	movl	%r9d, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %r10
	movq	-96(%rbp), %r11
	movq	-88(%rbp), %rcx
	movl	-80(%rbp), %r9d
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rax
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L70:
	leal	24(,%r14,8), %edx
	subl	%r14d, %r9d
	movslq	%r14d, %r15
	movslq	%edx, %rdx
	addq	%r15, %r9
	addq	%rdx, %r11
	leaq	31(%rax,%r9,8), %rdx
	.p2align 4,,10
	.p2align 3
.L46:
	movq	%rbx, (%r11)
	addq	$8, %r11
	cmpq	%rdx, %r11
	jne	.L46
	jmp	.L45
.L71:
	movslq	%r14d, %r15
	jmp	.L45
	.cfi_endproc
.LFE17794:
	.size	_ZN2v88internal15TransitionArray31CompactPrototypeTransitionArrayEPNS0_7IsolateENS0_14WeakFixedArrayE, .-_ZN2v88internal15TransitionArray31CompactPrototypeTransitionArrayEPNS0_7IsolateENS0_14WeakFixedArrayE
	.section	.text._ZN2v88internal15TransitionArray28GrowPrototypeTransitionArrayENS0_6HandleINS0_14WeakFixedArrayEEEiPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TransitionArray28GrowPrototypeTransitionArrayENS0_6HandleINS0_14WeakFixedArrayEEEiPNS0_7IsolateE
	.type	_ZN2v88internal15TransitionArray28GrowPrototypeTransitionArrayENS0_6HandleINS0_14WeakFixedArrayEEEiPNS0_7IsolateE, @function
_ZN2v88internal15TransitionArray28GrowPrototypeTransitionArrayENS0_6HandleINS0_14WeakFixedArrayEEEiPNS0_7IsolateE:
.LFB17795:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rdx, %rdi
	movl	$256, %edx
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%r8), %rax
	movslq	11(%rax), %rbx
	subl	$1, %ebx
	cmpl	$256, %esi
	cmovle	%esi, %edx
	movq	%r8, %rsi
	subl	%ebx, %edx
	call	_ZN2v88internal7Factory25CopyWeakFixedArrayAndGrowENS0_6HandleINS0_14WeakFixedArrayEEEiNS0_14AllocationTypeE@PLT
	testl	%ebx, %ebx
	js	.L75
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	movq	(%rax), %rdx
	movq	$0, 15(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17795:
	.size	_ZN2v88internal15TransitionArray28GrowPrototypeTransitionArrayENS0_6HandleINS0_14WeakFixedArrayEEEiPNS0_7IsolateE, .-_ZN2v88internal15TransitionArray28GrowPrototypeTransitionArrayENS0_6HandleINS0_14WeakFixedArrayEEEiPNS0_7IsolateE
	.section	.text._ZN2v88internal19TransitionsAccessor22GetPrototypeTransitionENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19TransitionsAccessor22GetPrototypeTransitionENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal19TransitionsAccessor22GetPrototypeTransitionENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal19TransitionsAccessor22GetPrototypeTransitionENS0_6HandleINS0_6ObjectEEE:
.LFB17798:
	.cfi_startproc
	endbr64
	cmpl	$4, 32(%rdi)
	movq	%rsi, %r8
	je	.L77
.L79:
	movq	(%rdi), %rax
	movq	1072(%rax), %rdx
.L78:
	movl	11(%rdx), %eax
	testl	%eax, %eax
	jne	.L101
.L94:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	movq	15(%rdx), %rcx
	sarq	$32, %rcx
	testq	%rcx, %rcx
	jle	.L94
	subl	$1, %ecx
	leaq	23(%rdx), %rax
	leaq	31(%rdx,%rcx,8), %rcx
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L97:
	addq	$8, %rax
	cmpq	%rax, %rcx
	je	.L94
.L95:
	movq	(%rax), %rsi
	movq	%rsi, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	jne	.L97
	cmpl	$3, %esi
	je	.L97
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	andq	$-3, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	23(%rsi), %rbx
	cmpq	%rbx, (%r8)
	je	.L102
	.p2align 4,,10
	.p2align 3
.L82:
	addq	$8, %rax
	cmpq	%rax, %rcx
	je	.L80
	movq	(%rax), %rsi
	movq	%rsi, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	jne	.L82
	cmpl	$3, %esi
	je	.L82
	andq	$-3, %rsi
	movq	23(%rsi), %rbx
	cmpq	%rbx, (%r8)
	jne	.L82
.L102:
	movq	(%rdi), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L83
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L80:
	xorl	%eax, %eax
.L86:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore 3
	.cfi_restore 6
	movq	24(%rdi), %rax
	movq	15(%rax), %rdx
	testq	%rdx, %rdx
	je	.L79
	movq	15(%rax), %rdx
	jmp	.L78
.L83:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L103
.L85:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L86
.L103:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L85
	.cfi_endproc
.LFE17798:
	.size	_ZN2v88internal19TransitionsAccessor22GetPrototypeTransitionENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal19TransitionsAccessor22GetPrototypeTransitionENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal19TransitionsAccessor23GetPrototypeTransitionsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19TransitionsAccessor23GetPrototypeTransitionsEv
	.type	_ZN2v88internal19TransitionsAccessor23GetPrototypeTransitionsEv, @function
_ZN2v88internal19TransitionsAccessor23GetPrototypeTransitionsEv:
.LFB17799:
	.cfi_startproc
	endbr64
	cmpl	$4, 32(%rdi)
	je	.L105
.L107:
	movq	(%rdi), %rax
	movq	1072(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	movq	24(%rdi), %rax
	movq	15(%rax), %rdx
	testq	%rdx, %rdx
	je	.L107
	movq	15(%rax), %rax
	ret
	.cfi_endproc
.LFE17799:
	.size	_ZN2v88internal19TransitionsAccessor23GetPrototypeTransitionsEv, .-_ZN2v88internal19TransitionsAccessor23GetPrototypeTransitionsEv
	.section	.text._ZN2v88internal15TransitionArray31SetNumberOfPrototypeTransitionsENS0_14WeakFixedArrayEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TransitionArray31SetNumberOfPrototypeTransitionsENS0_14WeakFixedArrayEi
	.type	_ZN2v88internal15TransitionArray31SetNumberOfPrototypeTransitionsENS0_14WeakFixedArrayEi, @function
_ZN2v88internal15TransitionArray31SetNumberOfPrototypeTransitionsENS0_14WeakFixedArrayEi:
.LFB17800:
	.cfi_startproc
	endbr64
	salq	$32, %rsi
	movq	%rsi, 15(%rdi)
	ret
	.cfi_endproc
.LFE17800:
	.size	_ZN2v88internal15TransitionArray31SetNumberOfPrototypeTransitionsENS0_14WeakFixedArrayEi, .-_ZN2v88internal15TransitionArray31SetNumberOfPrototypeTransitionsENS0_14WeakFixedArrayEi
	.section	.text._ZN2v88internal19TransitionsAccessor19NumberOfTransitionsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19TransitionsAccessor19NumberOfTransitionsEv
	.type	_ZN2v88internal19TransitionsAccessor19NumberOfTransitionsEv, @function
_ZN2v88internal19TransitionsAccessor19NumberOfTransitionsEv:
.LFB17801:
	.cfi_startproc
	endbr64
	movl	32(%rdi), %eax
	cmpl	$3, %eax
	je	.L116
	ja	.L121
	xorl	%eax, %eax
.L112:
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	cmpl	$4, %eax
	jne	.L114
	movq	24(%rdi), %rdx
	xorl	%eax, %eax
	cmpl	$1, 11(%rdx)
	jle	.L112
	movq	23(%rdx), %rax
	shrq	$32, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	movl	$1, %eax
	ret
.L114:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17801:
	.size	_ZN2v88internal19TransitionsAccessor19NumberOfTransitionsEv, .-_ZN2v88internal19TransitionsAccessor19NumberOfTransitionsEv
	.section	.text._ZN2v88internal19TransitionsAccessor18SetMigrationTargetENS0_3MapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19TransitionsAccessor18SetMigrationTargetENS0_3MapE
	.type	_ZN2v88internal19TransitionsAccessor18SetMigrationTargetENS0_3MapE, @function
_ZN2v88internal19TransitionsAccessor18SetMigrationTargetENS0_3MapE:
.LFB17802:
	.cfi_startproc
	endbr64
	cmpl	$1, 32(%rdi)
	je	.L138
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	%rsi, 71(%rax)
	testb	$1, %sil
	je	.L122
	cmpl	$3, %esi
	je	.L122
	movq	%rsi, %r13
	andq	$-262144, %rsi
	movq	16(%rdi), %rdi
	movq	8(%rsi), %rax
	andq	$-3, %r13
	movq	%rsi, %r12
	leaq	71(%rdi), %r8
	testl	$262144, %eax
	je	.L125
	movq	%r8, %rsi
	movq	%r13, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	16(%rbx), %rdi
	movq	8(%r12), %rax
	leaq	71(%rdi), %r8
.L125:
	testb	$24, %al
	je	.L122
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L139
.L122:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r8, %rsi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE17802:
	.size	_ZN2v88internal19TransitionsAccessor18SetMigrationTargetENS0_3MapE, .-_ZN2v88internal19TransitionsAccessor18SetMigrationTargetENS0_3MapE
	.section	.text._ZN2v88internal19TransitionsAccessor18GetMigrationTargetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19TransitionsAccessor18GetMigrationTargetEv
	.type	_ZN2v88internal19TransitionsAccessor18GetMigrationTargetEv, @function
_ZN2v88internal19TransitionsAccessor18GetMigrationTargetEv:
.LFB17803:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$2, 32(%rdi)
	je	.L143
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	movq	16(%rdi), %rax
	movq	71(%rax), %rax
	ret
	.cfi_endproc
.LFE17803:
	.size	_ZN2v88internal19TransitionsAccessor18GetMigrationTargetEv, .-_ZN2v88internal19TransitionsAccessor18GetMigrationTargetEv
	.section	.text._ZN2v88internal15TransitionArray3ZapEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TransitionArray3ZapEPNS0_7IsolateE
	.type	_ZN2v88internal15TransitionArray3ZapEPNS0_7IsolateE, @function
_ZN2v88internal15TransitionArray3ZapEPNS0_7IsolateE:
.LFB17804:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movq	(%rdi), %rdi
	movq	96(%rsi), %rax
	movslq	11(%rdi), %rcx
	addq	$15, %rdi
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	movq	(%rdx), %rax
	movq	$0, 23(%rax)
	ret
	.cfi_endproc
.LFE17804:
	.size	_ZN2v88internal15TransitionArray3ZapEPNS0_7IsolateE, .-_ZN2v88internal15TransitionArray3ZapEPNS0_7IsolateE
	.section	.text._ZN2v88internal19TransitionsAccessor18ReplaceTransitionsENS0_11MaybeObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19TransitionsAccessor18ReplaceTransitionsENS0_11MaybeObjectE
	.type	_ZN2v88internal19TransitionsAccessor18ReplaceTransitionsENS0_11MaybeObjectE, @function
_ZN2v88internal19TransitionsAccessor18ReplaceTransitionsENS0_11MaybeObjectE:
.LFB17805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpl	$4, 32(%rdi)
	je	.L158
.L146:
	movq	16(%r12), %rax
	movq	%rsi, 71(%rax)
	testb	$1, %sil
	je	.L145
	cmpl	$3, %esi
	je	.L145
	movq	%rsi, %rbx
	movq	16(%r12), %rdi
	movq	%rsi, %r13
	andq	$-262144, %rbx
	andq	$-3, %r13
	movq	8(%rbx), %rax
	leaq	71(%rdi), %r8
	testl	$262144, %eax
	jne	.L159
.L148:
	testb	$24, %al
	je	.L145
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L160
.L145:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	movq	24(%rdi), %rdx
	movq	(%rdi), %rax
	movslq	11(%rdx), %rcx
	movq	96(%rax), %rax
	leaq	15(%rdx), %rdi
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	movq	$0, 23(%rdx)
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L159:
	movq	%r8, %rsi
	movq	%r13, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	16(%r12), %rdi
	movq	8(%rbx), %rax
	leaq	71(%rdi), %r8
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L160:
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r8, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE17805:
	.size	_ZN2v88internal19TransitionsAccessor18ReplaceTransitionsENS0_11MaybeObjectE, .-_ZN2v88internal19TransitionsAccessor18ReplaceTransitionsENS0_11MaybeObjectE
	.section	.text._ZN2v88internal19TransitionsAccessor28EnsureHasFullTransitionArrayEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19TransitionsAccessor28EnsureHasFullTransitionArrayEv
	.type	_ZN2v88internal19TransitionsAccessor28EnsureHasFullTransitionArrayEv, @function
_ZN2v88internal19TransitionsAccessor28EnsureHasFullTransitionArrayEv:
.LFB17807:
	.cfi_startproc
	endbr64
	movl	32(%rdi), %eax
	cmpl	$4, %eax
	je	.L207
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leal	-1(%rax), %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	cmpl	$1, %r12d
	movq	(%rdi), %rdi
	seta	%sil
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory18NewTransitionArrayEii@PLT
	movq	%rax, %r13
	movq	8(%rbx), %rax
	movq	(%rax), %rax
	movq	%rax, 16(%rbx)
	movq	71(%rax), %rsi
	movq	%rsi, 24(%rbx)
	testb	$1, %sil
	je	.L163
	cmpl	$3, %esi
	je	.L163
	movq	%rsi, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	je	.L210
	cmpq	$1, %rax
	je	.L211
.L169:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L163:
	movl	$1, 32(%rbx)
	cmpl	$1, %r12d
	jbe	.L166
	movq	0(%r13), %rax
	movq	$0, 23(%rax)
	.p2align 4,,10
	.p2align 3
.L166:
	movq	0(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19TransitionsAccessor18ReplaceTransitionsENS0_11MaybeObjectE
	movq	8(%rbx), %rax
	movq	(%rax), %rax
	movq	%rax, 16(%rbx)
	movq	71(%rax), %rax
	movq	%rax, 24(%rbx)
	testb	$1, %al
	jne	.L212
.L183:
	movl	$1, 32(%rbx)
.L161:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
	cmpl	$3, %eax
	je	.L183
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L213
	cmpq	$1, %rdx
	jne	.L169
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L186
	movl	$4, 32(%rbx)
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L210:
	movl	$3, 32(%rbx)
	cmpl	$1, %r12d
	jbe	.L166
	movq	(%rbx), %r12
	andq	$-3, %rsi
.L168:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L174
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L175:
	movl	15(%rsi), %edx
	movq	39(%rsi), %rcx
	shrl	$10, %edx
	andl	$1023, %edx
	leal	(%rdx,%rdx,2), %edx
	sall	$3, %edx
	movslq	%edx, %rdx
	movq	-1(%rdx,%rcx), %rdx
	movq	(%rax), %rax
	movq	0(%r13), %r12
	movq	%rax, %r15
	movq	%rdx, 31(%r12)
	orq	$2, %r15
	leaq	31(%r12), %rsi
	testb	$1, %dl
	je	.L189
	cmpl	$3, %edx
	je	.L189
	movq	%rdx, %r8
	andq	$-262144, %rdx
	movq	%rdx, %r14
	movq	8(%rdx), %rdx
	andq	$-3, %r8
	testl	$262144, %edx
	jne	.L214
.L178:
	andl	$24, %edx
	je	.L189
	movq	%r12, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L215
	.p2align 4,,10
	.p2align 3
.L189:
	movq	%r15, 39(%r12)
	leaq	39(%r12), %rsi
	testb	$1, %r15b
	je	.L166
	cmpl	$3, %r15d
	je	.L166
	movq	%rax, %r15
	andq	$-262144, %rax
	movq	%rax, %r14
	movq	8(%rax), %rax
	andq	$-3, %r15
	testl	$262144, %eax
	jne	.L216
.L181:
	testb	$24, %al
	je	.L166
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L166
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L213:
	movl	$3, 32(%rbx)
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L217
.L176:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L216:
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	movq	-56(%rbp), %rsi
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L214:
	movq	%r8, %rdx
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rdx
	movq	-72(%rbp), %rax
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rsi
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L215:
	movq	%r8, %rdx
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rax
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L211:
	movq	-1(%rsi), %rax
	cmpw	$149, 11(%rax)
	jne	.L218
	movl	$4, 32(%rbx)
	cmpl	$1, %r12d
	jbe	.L166
.L172:
	movq	(%rbx), %r12
	xorl	%esi, %esi
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L217:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L218:
	movq	-1(%rsi), %rax
	cmpw	$95, 11(%rax)
	je	.L219
	movl	$2, 32(%rbx)
	cmpl	$1, %r12d
	ja	.L172
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L186:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, 32(%rbx)
	jmp	.L161
.L219:
	movl	$0, 32(%rbx)
	cmpl	$1, %r12d
	jbe	.L166
	jmp	.L172
	.cfi_endproc
.LFE17807:
	.size	_ZN2v88internal19TransitionsAccessor28EnsureHasFullTransitionArrayEv, .-_ZN2v88internal19TransitionsAccessor28EnsureHasFullTransitionArrayEv
	.section	.text._ZN2v88internal19TransitionsAccessor23SetPrototypeTransitionsENS0_6HandleINS0_14WeakFixedArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19TransitionsAccessor23SetPrototypeTransitionsENS0_6HandleINS0_14WeakFixedArrayEEE
	.type	_ZN2v88internal19TransitionsAccessor23SetPrototypeTransitionsENS0_6HandleINS0_14WeakFixedArrayEEE, @function
_ZN2v88internal19TransitionsAccessor23SetPrototypeTransitionsENS0_6HandleINS0_14WeakFixedArrayEEE:
.LFB17806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	_ZN2v88internal19TransitionsAccessor28EnsureHasFullTransitionArrayEv
	movq	24(%rbx), %r13
	movq	(%r12), %rax
	movq	%rax, 15(%r13)
	testb	$1, %al
	je	.L220
	cmpl	$3, %eax
	je	.L220
	movq	%rax, %r14
	andq	$-262144, %rax
	leaq	15(%r13), %r12
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %r14
	testl	$262144, %eax
	jne	.L232
.L222:
	testb	$24, %al
	je	.L220
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L233
.L220:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L233:
	popq	%rbx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE17806:
	.size	_ZN2v88internal19TransitionsAccessor23SetPrototypeTransitionsENS0_6HandleINS0_14WeakFixedArrayEEE, .-_ZN2v88internal19TransitionsAccessor23SetPrototypeTransitionsENS0_6HandleINS0_14WeakFixedArrayEEE
	.section	.text._ZN2v88internal19TransitionsAccessor22PutPrototypeTransitionENS0_6HandleINS0_6ObjectEEENS2_INS0_3MapEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19TransitionsAccessor22PutPrototypeTransitionENS0_6HandleINS0_6ObjectEEENS2_INS0_3MapEEE
	.type	_ZN2v88internal19TransitionsAccessor22PutPrototypeTransitionENS0_6HandleINS0_6ObjectEEENS2_INS0_3MapEEE, @function
_ZN2v88internal19TransitionsAccessor22PutPrototypeTransitionENS0_6HandleINS0_6ObjectEEENS2_INS0_3MapEEE:
.LFB17797:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	movl	15(%rax), %edx
	andl	$1048576, %edx
	je	.L286
.L234:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L286:
	.cfi_restore_state
	movl	15(%rax), %eax
	testl	$2097152, %eax
	jne	.L234
	cmpb	$0, _ZN2v88internal32FLAG_cache_prototype_transitionsE(%rip)
	je	.L234
	cmpl	$4, 32(%rdi)
	movq	(%rdi), %r14
	movq	%rdi, %rbx
	je	.L287
.L238:
	movq	1072(%r14), %rsi
.L240:
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L241
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L242:
	movslq	11(%rsi), %rax
	movl	$1, %r14d
	leal	-1(%rax), %r15d
	testq	%rax, %rax
	je	.L264
	movq	15(%rsi), %r14
	sarq	$32, %r14
	addl	$1, %r14d
	cmpl	%r14d, %r15d
	jl	.L288
.L245:
	movq	0(%r13), %r14
	movl	$24, %eax
	movabsq	$4294967296, %r15
	movl	11(%r14), %edx
	testl	%edx, %edx
	je	.L258
	movq	15(%r14), %rax
	sarq	$32, %rax
	leal	1(%rax), %r15d
	leal	24(,%rax,8), %eax
	salq	$32, %r15
	cltq
.L258:
	movq	(%r12), %rcx
	addq	%r14, %rax
	leaq	-1(%rax), %r12
	movq	%rcx, %rdx
	orq	$2, %rdx
	movq	%rdx, -1(%rax)
	testb	$1, %dl
	je	.L262
	cmpl	$3, %edx
	je	.L262
	movq	%rcx, %rdx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	andq	$-3, %rdx
	movq	%rcx, %rbx
	testl	$262144, %eax
	je	.L260
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-56(%rbp), %rdx
.L260:
	testb	$24, %al
	je	.L262
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L262
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L262:
	movq	0(%r13), %rax
	movq	%r15, 15(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L288:
	.cfi_restore_state
	movq	0(%r13), %rsi
	.p2align 4,,10
	.p2align 3
.L264:
	movq	(%rbx), %rdi
	call	_ZN2v88internal15TransitionArray31CompactPrototypeTransitionArrayEPNS0_7IsolateENS0_14WeakFixedArrayE
	testb	%al, %al
	jne	.L245
	cmpl	$256, %r15d
	je	.L234
	movq	0(%r13), %rax
	leal	(%r14,%r14), %edx
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	movl	$1, %ecx
	movslq	11(%rax), %r15
	movl	$256, %eax
	subl	$1, %r15d
	cmpl	$256, %edx
	cmovg	%eax, %edx
	subl	%r15d, %edx
	call	_ZN2v88internal7Factory25CopyWeakFixedArrayAndGrowENS0_6HandleINS0_14WeakFixedArrayEEEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r13
	testl	%r15d, %r15d
	js	.L289
.L246:
	movq	8(%rbx), %rax
	movq	(%rax), %rax
	movq	%rax, 16(%rbx)
	movq	71(%rax), %rax
	movq	%rax, 24(%rbx)
	testb	$1, %al
	je	.L247
	cmpl	$3, %eax
	je	.L247
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L290
	cmpq	$1, %rdx
	je	.L291
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L241:
	movq	41088(%r14), %r13
	cmpq	41096(%r14), %r13
	je	.L292
.L243:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, 0(%r13)
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L287:
	movq	24(%rdi), %rax
	movq	15(%rax), %rdx
	testq	%rdx, %rdx
	je	.L238
	movq	15(%rax), %rsi
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L247:
	movl	$1, 32(%rbx)
.L249:
	movq	%rbx, %rdi
	call	_ZN2v88internal19TransitionsAccessor28EnsureHasFullTransitionArrayEv
	movq	24(%rbx), %r14
	movq	0(%r13), %rax
	movq	%rax, 15(%r14)
	leaq	15(%r14), %r15
	testb	$1, %al
	je	.L245
	cmpl	$3, %eax
	je	.L245
	movq	%rax, %rdx
	andq	$-262144, %rax
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %rdx
	testl	$262144, %eax
	je	.L256
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-56(%rbp), %rdx
.L256:
	testb	$24, %al
	je	.L245
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L245
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L292:
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L243
.L289:
	movq	(%rax), %rax
	movq	$0, 15(%rax)
	jmp	.L246
.L290:
	movl	$3, 32(%rbx)
	jmp	.L249
.L291:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L293
	movl	$4, 32(%rbx)
	jmp	.L249
.L293:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	je	.L294
	movl	$2, 32(%rbx)
	jmp	.L249
.L294:
	movl	$0, 32(%rbx)
	jmp	.L249
	.cfi_endproc
.LFE17797:
	.size	_ZN2v88internal19TransitionsAccessor22PutPrototypeTransitionENS0_6HandleINS0_6ObjectEEENS2_INS0_3MapEEE, .-_ZN2v88internal19TransitionsAccessor22PutPrototypeTransitionENS0_6HandleINS0_6ObjectEEENS2_INS0_3MapEEE
	.section	.text._ZN2v88internal19TransitionsAccessor30TraverseTransitionTreeInternalEPFvNS0_3MapEPvES3_PNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19TransitionsAccessor30TraverseTransitionTreeInternalEPFvNS0_3MapEPvES3_PNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	.type	_ZN2v88internal19TransitionsAccessor30TraverseTransitionTreeInternalEPFvNS0_3MapEPvES3_PNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE, @function
_ZN2v88internal19TransitionsAccessor30TraverseTransitionTreeInternalEPFvNS0_3MapEPvES3_PNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE:
.LFB17808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	32(%rdi), %eax
	cmpl	$3, %eax
	je	.L296
	cmpl	$4, %eax
	jne	.L298
	movq	24(%rdi), %rax
	movq	15(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L333
.L308:
	cmpl	$1, 11(%rax)
	jle	.L298
	leaq	-96(%rbp), %rcx
	xorl	%r15d, %r15d
	movq	%rcx, -200(%rbp)
.L314:
	movq	23(%rax), %rdx
	sarq	$32, %rdx
	cmpl	%r15d, %edx
	jle	.L298
	movq	%r15, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movq	39(%rax), %rax
	movq	(%rbx), %rdx
	movq	$0, -72(%rbp)
	movq	$0, -88(%rbp)
	andq	$-3, %rax
	movq	%rdx, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -72(%rbp)
	testb	$1, %al
	je	.L322
	cmpl	$3, %eax
	je	.L322
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L334
	cmpq	$1, %rdx
	je	.L335
.L303:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L296:
	movq	24(%rdi), %rax
	movq	(%rdi), %rdx
	movq	$0, -168(%rbp)
	movq	$0, -184(%rbp)
	andq	$-3, %rax
	movq	%rdx, -192(%rbp)
	movq	%rax, -176(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -168(%rbp)
	testb	$1, %al
	jne	.L336
.L299:
	movl	$1, -160(%rbp)
.L301:
	leaq	-192(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal19TransitionsAccessor30TraverseTransitionTreeInternalEPFvNS0_3MapEPvES3_PNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
.L298:
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	*%r12
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L337
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	.cfi_restore_state
	cmpl	$3, %eax
	je	.L299
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L338
	cmpq	$1, %rdx
	jne	.L303
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L339
	movl	$4, -160(%rbp)
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L333:
	movq	15(%rax), %rdx
	movl	11(%rdx), %ecx
	testl	%ecx, %ecx
	je	.L308
	movq	15(%rdx), %rcx
	sarq	$32, %rcx
	testq	%rcx, %rcx
	jle	.L308
	leal	-1(%rcx), %eax
	leaq	23(%rdx), %r15
	leaq	31(%rdx,%rax,8), %r8
	leaq	-144(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L312:
	movq	(%r15), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L340
.L315:
	addq	$8, %r15
	cmpq	%r15, %r8
	jne	.L312
	movq	24(%rbx), %rax
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L322:
	movl	$1, -64(%rbp)
.L324:
	movq	-200(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	addq	$1, %r15
	call	_ZN2v88internal19TransitionsAccessor30TraverseTransitionTreeInternalEPFvNS0_3MapEPvES3_PNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	movq	24(%rbx), %rax
	cmpl	$1, 11(%rax)
	jg	.L314
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L340:
	cmpl	$3, %eax
	je	.L315
	movq	(%rbx), %rdx
	andq	$-3, %rax
	movq	$0, -120(%rbp)
	movq	$0, -136(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%rax, -128(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -120(%rbp)
	testb	$1, %al
	je	.L316
	cmpl	$3, %eax
	je	.L316
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L341
	cmpq	$1, %rdx
	jne	.L303
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L320
	movl	$4, -112(%rbp)
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L316:
	movl	$1, -112(%rbp)
.L318:
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r8, -208(%rbp)
	movq	%rdi, -200(%rbp)
	call	_ZN2v88internal19TransitionsAccessor30TraverseTransitionTreeInternalEPFvNS0_3MapEPvES3_PNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	movq	-200(%rbp), %rdi
	movq	-208(%rbp), %r8
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L334:
	movl	$3, -64(%rbp)
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L338:
	movl	$3, -160(%rbp)
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L335:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L326
	movl	$4, -64(%rbp)
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L326:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -64(%rbp)
	jmp	.L324
.L341:
	movl	$3, -112(%rbp)
	jmp	.L318
.L339:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -160(%rbp)
	jmp	.L301
.L320:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -112(%rbp)
	jmp	.L318
.L337:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17808:
	.size	_ZN2v88internal19TransitionsAccessor30TraverseTransitionTreeInternalEPFvNS0_3MapEPvES3_PNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE, .-_ZN2v88internal19TransitionsAccessor30TraverseTransitionTreeInternalEPFvNS0_3MapEPvES3_PNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	.section	.text._ZN2v88internal15TransitionArray13SearchDetailsEiNS0_12PropertyKindENS0_18PropertyAttributesEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TransitionArray13SearchDetailsEiNS0_12PropertyKindENS0_18PropertyAttributesEPi
	.type	_ZN2v88internal15TransitionArray13SearchDetailsEiNS0_12PropertyKindENS0_18PropertyAttributesEPi, @function
_ZN2v88internal15TransitionArray13SearchDetailsEiNS0_12PropertyKindENS0_18PropertyAttributesEPi:
.LFB17809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r11d, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r9
	cmpl	$1, 11(%r9)
	jle	.L343
	movq	23(%r9), %r11
	shrq	$32, %r11
.L343:
	leal	2(%rsi), %edi
	sall	$4, %edi
	movslq	%edi, %rdi
	movq	-1(%r9,%rdi), %rbx
	cmpl	%r11d, %esi
	jge	.L344
.L350:
	movq	-1(%rdi,%r9), %rax
	cmpq	%rax, %rbx
	jne	.L344
	movq	7(%rdi,%r9), %r12
	leal	1(%rsi), %r10d
	andq	$-3, %r12
	movl	15(%r12), %eax
	movq	39(%r12), %r12
	shrl	$10, %eax
	andl	$1023, %eax
	leal	(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	movq	7(%rax,%r12), %rax
	sarq	$32, %rax
	movl	%eax, %r12d
	andl	$1, %r12d
	cmpl	%r12d, %edx
	jne	.L348
	shrl	$3, %eax
	andl	$7, %eax
	cmpl	%eax, %ecx
	jne	.L348
	movl	%esi, %eax
.L342:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L348:
	.cfi_restore_state
	jge	.L358
.L344:
	movl	$-1, %eax
	testq	%r8, %r8
	je	.L342
	popq	%rbx
	popq	%r12
	movl	%esi, (%r8)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	.cfi_restore_state
	addq	$16, %rdi
	cmpl	%r10d, %r11d
	je	.L359
	movl	%r10d, %esi
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L359:
	movl	%r11d, %esi
	jmp	.L344
	.cfi_endproc
.LFE17809:
	.size	_ZN2v88internal15TransitionArray13SearchDetailsEiNS0_12PropertyKindENS0_18PropertyAttributesEPi, .-_ZN2v88internal15TransitionArray13SearchDetailsEiNS0_12PropertyKindENS0_18PropertyAttributesEPi
	.section	.text._ZN2v88internal15TransitionArray25SearchDetailsAndGetTargetEiNS0_12PropertyKindENS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TransitionArray25SearchDetailsAndGetTargetEiNS0_12PropertyKindENS0_18PropertyAttributesE
	.type	_ZN2v88internal15TransitionArray25SearchDetailsAndGetTargetEiNS0_12PropertyKindENS0_18PropertyAttributesE, @function
_ZN2v88internal15TransitionArray25SearchDetailsAndGetTargetEiNS0_12PropertyKindENS0_18PropertyAttributesE:
.LFB17810:
	.cfi_startproc
	endbr64
	movq	(%rdi), %r8
	xorl	%eax, %eax
	cmpl	$1, 11(%r8)
	jle	.L361
	movq	23(%r8), %rax
	shrq	$32, %rax
.L361:
	leal	2(%rsi), %edi
	sall	$4, %edi
	movslq	%edi, %rdi
	movq	-1(%r8,%rdi), %r10
	cmpl	%eax, %esi
	jge	.L362
	movl	%esi, %r9d
	movslq	%esi, %rsi
	notl	%r9d
	addl	%r9d, %eax
	leaq	3(%rax,%rsi), %r9
	salq	$4, %r9
.L363:
	movq	-1(%rdi,%r8), %rax
	cmpq	%rax, %r10
	jne	.L362
	movq	7(%rdi,%r8), %rax
	andq	$-3, %rax
	movl	15(%rax), %esi
	movq	39(%rax), %r11
	shrl	$10, %esi
	andl	$1023, %esi
	leal	(%rsi,%rsi,2), %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	movq	7(%rsi,%r11), %rsi
	sarq	$32, %rsi
	movl	%esi, %r11d
	andl	$1, %r11d
	cmpl	%r11d, %edx
	jne	.L369
	shrl	$3, %esi
	andl	$7, %esi
	cmpl	%esi, %ecx
	jne	.L369
	ret
	.p2align 4,,10
	.p2align 3
.L369:
	jge	.L370
.L362:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L370:
	addq	$16, %rdi
	cmpq	%rdi, %r9
	jne	.L363
	jmp	.L362
	.cfi_endproc
.LFE17810:
	.size	_ZN2v88internal15TransitionArray25SearchDetailsAndGetTargetEiNS0_12PropertyKindENS0_18PropertyAttributesE, .-_ZN2v88internal15TransitionArray25SearchDetailsAndGetTargetEiNS0_12PropertyKindENS0_18PropertyAttributesE
	.section	.text._ZN2v88internal15TransitionArray4SortEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TransitionArray4SortEv
	.type	_ZN2v88internal15TransitionArray4SortEv, @function
_ZN2v88internal15TransitionArray4SortEv:
.LFB17813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	cmpl	$1, 11(%rax)
	jle	.L371
	movq	%rax, %rcx
	movq	23(%rax), %rdx
	movq	%rdi, %r14
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	sarq	$32, %rdx
	movl	%edx, -140(%rbp)
	leaq	-37592(%rcx), %rdi
	movq	%rdi, -136(%rbp)
	cmpq	$1, %rdx
	jle	.L371
	movq	$48, -112(%rbp)
	movl	$1, -104(%rbp)
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L436:
	movl	-116(%rbp), %eax
	cmpl	%r12d, %eax
	je	.L387
	cmpl	%eax, %r12d
	jge	.L389
.L388:
	movl	-72(%rbp), %r12d
	movq	%r13, %r14
	movq	-80(%rbp), %r15
	movq	(%r14), %rax
	addl	%r12d, %r12d
	leal	24(,%r12,8), %r13d
	leal	16(,%r12,8), %ebx
	subq	$1, %rax
	movslq	%r13d, %r13
	movslq	%ebx, %rbx
.L396:
	movq	%r15, (%rbx,%rax)
	movq	(%r14), %rdi
	leaq	-1(%rdi), %rax
	testb	$1, %r15b
	je	.L406
	cmpl	$3, %r15d
	je	.L406
	movq	%r15, %rdx
	andq	$-262144, %r15
	leaq	(%rbx,%rax), %rsi
	movq	8(%r15), %rcx
	andq	$-3, %rdx
	testl	$262144, %ecx
	je	.L399
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%r14), %rdi
	movq	8(%r15), %rcx
	movq	-72(%rbp), %rdx
	leaq	-1(%rdi), %rax
	leaq	(%rbx,%rax), %rsi
.L399:
	andl	$24, %ecx
	je	.L406
	movq	%rdi, %rcx
	andq	$-262144, %rcx
	testb	$24, 8(%rcx)
	jne	.L406
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%r14), %rax
	subq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L406:
	movq	-128(%rbp), %rbx
	movq	%rbx, 0(%r13,%rax)
	movq	%rbx, %rax
	testb	$1, %al
	je	.L405
	cmpl	$3, %ebx
	je	.L405
	andq	$-262144, %rax
	movq	%rbx, %r12
	movq	(%r14), %rdi
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %r12
	leaq	-1(%r13,%rdi), %rsi
	testl	$262144, %eax
	je	.L402
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%r14), %rdi
	movq	8(%rbx), %rax
	leaq	-1(%r13,%rdi), %rsi
.L402:
	testb	$24, %al
	je	.L405
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L405
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L405:
	addq	$16, -112(%rbp)
	movl	-104(%rbp), %edi
	cmpl	%edi, -140(%rbp)
	je	.L371
	movq	(%r14), %rax
.L404:
	movq	-112(%rbp), %rcx
	movq	-1(%rax,%rcx), %r15
	addl	$1, -104(%rbp)
	movq	7(%rcx,%rax), %rdi
	movq	%rdi, -128(%rbp)
	movq	-1(%r15), %rdx
	cmpw	$64, 11(%rdx)
	jne	.L378
	movq	-136(%rbp), %rcx
	cmpq	3752(%rcx), %r15
	je	.L377
	cmpq	3800(%rcx), %r15
	je	.L377
	cmpq	3704(%rcx), %r15
	je	.L377
	cmpq	3672(%rcx), %r15
	je	.L377
	cmpq	3816(%rcx), %r15
	je	.L377
	.p2align 4,,10
	.p2align 3
.L378:
	movq	-128(%rbp), %rcx
	andq	$-3, %rcx
	movl	15(%rcx), %edx
	movq	39(%rcx), %rcx
	shrl	$10, %edx
	andl	$1023, %edx
	leal	(%rdx,%rdx,2), %edx
	sall	$3, %edx
	movslq	%edx, %rdx
	movq	7(%rdx,%rcx), %rdx
	sarq	$32, %rdx
	movl	%edx, %esi
	shrl	$3, %edx
	andl	$1, %esi
	andl	$7, %edx
	movl	%esi, -116(%rbp)
	movl	%edx, -120(%rbp)
.L376:
	movl	-104(%rbp), %ecx
	movq	-112(%rbp), %rbx
	leaq	7(%r15), %rsi
	movq	%r15, -80(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%r14, %r13
	movq	%rax, %rdi
	movl	%ecx, -72(%rbp)
	leaq	-16(%rbx), %r12
	movq	%r12, %r14
.L397:
	movq	-1(%rdi,%r14), %rdx
	movq	7(%r14,%rax), %r15
	movq	-1(%rdx), %rax
	cmpw	$64, 11(%rax)
	jne	.L379
	movq	-136(%rbp), %rax
	cmpq	3752(%rax), %rdx
	je	.L380
	cmpq	3800(%rax), %rdx
	je	.L380
	cmpq	3704(%rax), %rdx
	je	.L380
	cmpq	3672(%rax), %rdx
	je	.L380
	cmpq	3816(%rax), %rdx
	je	.L380
	.p2align 4,,10
	.p2align 3
.L379:
	movq	%r15, %rsi
	andq	$-3, %rsi
	movl	15(%rsi), %eax
	movq	39(%rsi), %rsi
	shrl	$10, %eax
	andl	$1023, %eax
	leal	(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	movq	7(%rax,%rsi), %rbx
	sarq	$32, %rbx
	movl	%ebx, %r12d
	shrl	$3, %ebx
	andl	$1, %r12d
	andl	$7, %ebx
.L381:
	movq	-88(%rbp), %rax
	movl	(%rax), %esi
	testb	$1, %sil
	jne	.L382
	shrl	$2, %esi
.L383:
	movl	7(%rdx), %eax
	testb	$1, %al
	jne	.L384
	shrl	$2, %eax
.L385:
	cmpq	%rdx, -80(%rbp)
	je	.L436
	cmpl	%esi, %eax
	jbe	.L388
.L389:
	movq	0(%r13), %rax
	leaq	16(%r14), %r12
	movq	%rdx, 15(%rax,%r14)
	movq	0(%r13), %rdi
	leaq	-1(%rdi), %rax
	testb	$1, %dl
	je	.L408
	cmpl	$3, %edx
	je	.L408
	movq	%rdx, %rbx
	movq	%rdx, %r10
	leaq	(%rax,%r12), %rsi
	andq	$-262144, %rbx
	andq	$-3, %r10
	movq	8(%rbx), %rdx
	testl	$262144, %edx
	je	.L391
	movq	%r10, %rdx
	movq	%r10, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	0(%r13), %rdi
	movq	8(%rbx), %rdx
	movq	-96(%rbp), %r10
	leaq	-1(%rdi), %rax
	leaq	(%r12,%rax), %rsi
.L391:
	andl	$24, %edx
	je	.L408
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L408
	movq	%r10, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	0(%r13), %rax
	subq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L408:
	movq	%r15, 24(%rax,%r14)
	leaq	24(%r14), %rbx
	movq	0(%r13), %rdi
	leaq	-1(%rdi), %rax
	testb	$1, %r15b
	je	.L407
	cmpl	$3, %r15d
	je	.L407
	movq	%r15, %rdx
	andq	$-262144, %r15
	leaq	(%rax,%rbx), %rsi
	movq	8(%r15), %r8
	andq	$-3, %rdx
	testl	$262144, %r8d
	je	.L394
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	0(%r13), %rdi
	movq	8(%r15), %r8
	movq	-96(%rbp), %rdx
	leaq	-1(%rdi), %rax
	leaq	(%rax,%rbx), %rsi
.L394:
	andl	$24, %r8d
	je	.L407
	movq	%rdi, %r8
	andq	$-262144, %r8
	testb	$24, 8(%r8)
	jne	.L407
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	0(%r13), %rdi
	leaq	-1(%rdi), %rax
	.p2align 4,,10
	.p2align 3
.L407:
	subl	$1, -72(%rbp)
	movl	-72(%rbp), %ecx
	subq	$16, %r14
	cmpl	$1, %ecx
	je	.L410
	movq	0(%r13), %rax
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L384:
	leaq	-64(%rbp), %rdi
	movl	%esi, -100(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movl	-100(%rbp), %esi
	movq	-96(%rbp), %rdx
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L382:
	movq	-80(%rbp), %rax
	leaq	-64(%rbp), %rdi
	movq	%rdx, -96(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-96(%rbp), %rdx
	movl	%eax, %esi
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L380:
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L387:
	movl	-120(%rbp), %eax
	cmpl	%ebx, %eax
	je	.L388
	cmpl	%eax, %ebx
	jl	.L388
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L377:
	movl	$0, -120(%rbp)
	movl	$0, -116(%rbp)
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L410:
	movq	%r13, %r14
	movq	-80(%rbp), %r15
	movl	$32, %ebx
	movl	$40, %r13d
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L371:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L437
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L437:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17813:
	.size	_ZN2v88internal15TransitionArray4SortEv, .-_ZN2v88internal15TransitionArray4SortEv
	.section	.text._ZN2v88internal12LinearSearchILNS0_10SearchModeE0ENS0_15TransitionArrayEEEiPT0_NS0_4NameEiPi,"axG",@progbits,_ZN2v88internal12LinearSearchILNS0_10SearchModeE0ENS0_15TransitionArrayEEEiPT0_NS0_4NameEiPi,comdat
	.p2align 4
	.weak	_ZN2v88internal12LinearSearchILNS0_10SearchModeE0ENS0_15TransitionArrayEEEiPT0_NS0_4NameEiPi
	.type	_ZN2v88internal12LinearSearchILNS0_10SearchModeE0ENS0_15TransitionArrayEEEiPT0_NS0_4NameEiPi, @function
_ZN2v88internal12LinearSearchILNS0_10SearchModeE0ENS0_15TransitionArrayEEEiPT0_NS0_4NameEiPi:
.LFB20592:
	.cfi_startproc
	endbr64
	testq	%rcx, %rcx
	je	.L439
	movq	(%rdi), %rdi
	cmpl	$1, 11(%rdi)
	jle	.L447
	movl	7(%rsi), %r9d
	movq	23(%rdi), %rax
	sarq	$32, %rax
	movl	%eax, %r11d
	testq	%rax, %rax
	jle	.L440
	leal	-1(%rax), %r10d
	xorl	%eax, %eax
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L441:
	cmpq	%rdx, %rsi
	je	.L438
	leaq	1(%rax), %rdx
	cmpq	%rax, %r10
	je	.L440
	movq	%rdx, %rax
.L443:
	movq	%rax, %rdx
	movl	%eax, %r8d
	salq	$4, %rdx
	addq	%rdi, %rdx
	movq	31(%rdx), %rdx
	cmpl	7(%rdx), %r9d
	jnb	.L441
	movl	%eax, (%rcx)
	movl	$-1, %r8d
.L438:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L447:
	xorl	%r11d, %r11d
.L440:
	movl	$-1, %r8d
	movl	%r11d, (%rcx)
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L439:
	testl	%edx, %edx
	jle	.L445
	movq	(%rdi), %rdi
	leal	-1(%rdx), %ecx
	xorl	%eax, %eax
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L449:
	leaq	1(%rax), %rdx
	cmpq	%rcx, %rax
	je	.L445
	movq	%rdx, %rax
.L446:
	movq	%rax, %rdx
	movl	%eax, %r8d
	salq	$4, %rdx
	addq	%rdi, %rdx
	movq	31(%rdx), %rdx
	cmpq	%rdx, %rsi
	jne	.L449
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L445:
	movl	$-1, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE20592:
	.size	_ZN2v88internal12LinearSearchILNS0_10SearchModeE0ENS0_15TransitionArrayEEEiPT0_NS0_4NameEiPi, .-_ZN2v88internal12LinearSearchILNS0_10SearchModeE0ENS0_15TransitionArrayEEEiPT0_NS0_4NameEiPi
	.section	.text._ZN2v88internal12BinarySearchILNS0_10SearchModeE0ENS0_15TransitionArrayEEEiPT0_NS0_4NameEiPi,"axG",@progbits,_ZN2v88internal12BinarySearchILNS0_10SearchModeE0ENS0_15TransitionArrayEEEiPT0_NS0_4NameEiPi,comdat
	.p2align 4
	.weak	_ZN2v88internal12BinarySearchILNS0_10SearchModeE0ENS0_15TransitionArrayEEEiPT0_NS0_4NameEiPi
	.type	_ZN2v88internal12BinarySearchILNS0_10SearchModeE0ENS0_15TransitionArrayEEEiPT0_NS0_4NameEiPi, @function
_ZN2v88internal12BinarySearchILNS0_10SearchModeE0ENS0_15TransitionArrayEEEiPT0_NS0_4NameEiPi:
.LFB20593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	(%rdi), %rdi
	cmpl	$1, 11(%rdi)
	jle	.L451
	movq	23(%rdi), %r11
	movl	7(%rsi), %r9d
	sarq	$32, %r11
	movl	%r11d, %ebx
	subl	$1, %r11d
	je	.L473
.L464:
	movl	%r11d, %r10d
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L455:
	movl	%r10d, %edx
	subl	%r8d, %edx
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	addl	%r8d, %eax
	leal	2(%rax), %edx
	sall	$4, %edx
	movslq	%edx, %rdx
	movq	-1(%rdi,%rdx), %rdx
	cmpl	%r9d, 7(%rdx)
	jnb	.L457
	leal	1(%rax), %r8d
	cmpl	%r10d, %r8d
	jne	.L455
.L454:
	cmpl	%r8d, %r11d
	jl	.L456
.L452:
	leal	2(%r8), %eax
	sall	$4, %eax
	cltq
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L459:
	cmpq	%rsi, %rdx
	je	.L450
	addl	$1, %r8d
	addq	$16, %rax
	cmpl	%r11d, %r8d
	jg	.L456
.L462:
	movq	-1(%rax,%rdi), %rdx
	movl	7(%rdx), %r10d
	cmpl	%r9d, %r10d
	je	.L459
	testq	%rcx, %rcx
	je	.L472
	xorl	%eax, %eax
	cmpl	%r9d, %r10d
	setbe	%al
	addl	%r8d, %eax
	movl	%eax, (%rcx)
.L472:
	movl	$-1, %r8d
.L450:
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L457:
	.cfi_restore_state
	cmpl	%r8d, %eax
	je	.L454
	movl	%eax, %r10d
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L451:
	movl	7(%rsi), %r9d
	xorl	%ebx, %ebx
	movl	$-1, %r11d
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L456:
	testq	%rcx, %rcx
	je	.L472
	movl	$-1, %r8d
	movl	%ebx, (%rcx)
	popq	%rbx
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L473:
	.cfi_restore_state
	xorl	%r8d, %r8d
	movl	$1, %ebx
	jmp	.L452
	.cfi_endproc
.LFE20593:
	.size	_ZN2v88internal12BinarySearchILNS0_10SearchModeE0ENS0_15TransitionArrayEEEiPT0_NS0_4NameEiPi, .-_ZN2v88internal12BinarySearchILNS0_10SearchModeE0ENS0_15TransitionArrayEEEiPT0_NS0_4NameEiPi
	.section	.text._ZN2v88internal19TransitionsAccessor13SearchSpecialENS0_6SymbolE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19TransitionsAccessor13SearchSpecialENS0_6SymbolE
	.type	_ZN2v88internal19TransitionsAccessor13SearchSpecialENS0_6SymbolE, @function
_ZN2v88internal19TransitionsAccessor13SearchSpecialENS0_6SymbolE:
.LFB17786:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$4, 32(%rdi)
	je	.L475
.L478:
	xorl	%eax, %eax
.L476:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L492
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L475:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	24(%rdi), %rdi
	movq	%rdi, -32(%rbp)
	cmpl	$1, 11(%rdi)
	jle	.L478
	movq	23(%rdi), %rdx
	sarq	$32, %rdx
	movl	%edx, %r9d
	je	.L478
	cmpq	$8, %rdx
	jg	.L480
	testl	%edx, %edx
	jle	.L478
	movl	$32, %edx
	xorl	%eax, %eax
	movq	%rdi, %r8
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L493:
	addq	$16, %rdx
	cmpl	%eax, %r9d
	je	.L478
.L482:
	movq	-1(%rdx,%r8), %rcx
	addl	$1, %eax
	cmpq	%rsi, %rcx
	jne	.L493
.L483:
	leal	3(%rax,%rax), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rax,%rdi), %rax
	andq	$-3, %rax
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L480:
	xorl	%ecx, %ecx
	leaq	-32(%rbp), %rdi
	call	_ZN2v88internal12BinarySearchILNS0_10SearchModeE0ENS0_15TransitionArrayEEEiPT0_NS0_4NameEiPi
	cmpl	$-1, %eax
	je	.L478
	movq	24(%rbx), %rdi
	addl	$1, %eax
	jmp	.L483
.L492:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17786:
	.size	_ZN2v88internal19TransitionsAccessor13SearchSpecialENS0_6SymbolE, .-_ZN2v88internal19TransitionsAccessor13SearchSpecialENS0_6SymbolE
	.section	.text._ZN2v88internal15TransitionArray18SearchAndGetTargetENS0_12PropertyKindENS0_4NameENS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TransitionArray18SearchAndGetTargetENS0_12PropertyKindENS0_4NameENS0_18PropertyAttributesE
	.type	_ZN2v88internal15TransitionArray18SearchAndGetTargetENS0_12PropertyKindENS0_4NameENS0_18PropertyAttributesE, @function
_ZN2v88internal15TransitionArray18SearchAndGetTargetENS0_12PropertyKindENS0_4NameENS0_18PropertyAttributesE:
.LFB17812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	cmpl	$1, 11(%rdi)
	jle	.L503
	movl	%esi, %r13d
	movq	%rdx, %rsi
	movq	23(%rdi), %rdx
	sarq	$32, %rdx
	movl	%edx, %r9d
	je	.L503
	movl	%ecx, %r14d
	cmpq	$8, %rdx
	jg	.L498
	testl	%edx, %edx
	jle	.L503
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L500:
	movq	%rdx, %rcx
	movl	%edx, %r8d
	salq	$4, %rcx
	addq	%rdi, %rcx
	movq	31(%rcx), %rax
	cmpq	%rsi, %rax
	je	.L499
	addq	$1, %rdx
	cmpl	%edx, %r9d
	jg	.L500
.L503:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L498:
	.cfi_restore_state
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12BinarySearchILNS0_10SearchModeE0ENS0_15TransitionArrayEEEiPT0_NS0_4NameEiPi
	movl	%eax, %r8d
	cmpl	$-1, %eax
	je	.L503
.L499:
	addq	$8, %rsp
	movl	%r14d, %ecx
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	%r8d, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal15TransitionArray25SearchDetailsAndGetTargetEiNS0_12PropertyKindENS0_18PropertyAttributesE
	.cfi_endproc
.LFE17812:
	.size	_ZN2v88internal15TransitionArray18SearchAndGetTargetENS0_12PropertyKindENS0_4NameENS0_18PropertyAttributesE, .-_ZN2v88internal15TransitionArray18SearchAndGetTargetENS0_12PropertyKindENS0_4NameENS0_18PropertyAttributesE
	.section	.text._ZN2v88internal19TransitionsAccessor16SearchTransitionENS0_4NameENS0_12PropertyKindENS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19TransitionsAccessor16SearchTransitionENS0_4NameENS0_12PropertyKindENS0_18PropertyAttributesE
	.type	_ZN2v88internal19TransitionsAccessor16SearchTransitionENS0_4NameENS0_12PropertyKindENS0_18PropertyAttributesE, @function
_ZN2v88internal19TransitionsAccessor16SearchTransitionENS0_4NameENS0_12PropertyKindENS0_18PropertyAttributesE:
.LFB17785:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movl	%edx, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	32(%rdi), %eax
	cmpl	$3, %eax
	je	.L510
	jbe	.L516
	cmpl	$4, %eax
	jne	.L521
	movq	24(%rdi), %rax
	movq	%r8, %rdx
	leaq	-16(%rbp), %rdi
	movq	%rax, -16(%rbp)
	call	_ZN2v88internal15TransitionArray18SearchAndGetTargetENS0_12PropertyKindENS0_4NameENS0_18PropertyAttributesE
.L514:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L522
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L510:
	.cfi_restore_state
	movq	24(%rdi), %rax
	andq	$-3, %rax
	movl	15(%rax), %edx
	movq	39(%rax), %rdi
	shrl	$10, %edx
	andl	$1023, %edx
	leal	(%rdx,%rdx,2), %edx
	sall	$3, %edx
	movslq	%edx, %rdx
	leaq	-1(%rdi,%rdx), %rdx
	movq	(%rdx), %rdi
	cmpq	%rdi, %r8
	je	.L523
.L516:
	xorl	%eax, %eax
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L523:
	movq	8(%rdx), %rdi
	leal	0(,%rcx,8), %edx
	orl	%esi, %edx
	sarq	$32, %rdi
	andl	$57, %edi
	cmpl	%edx, %edi
	je	.L514
	xorl	%eax, %eax
	jmp	.L514
.L521:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L522:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17785:
	.size	_ZN2v88internal19TransitionsAccessor16SearchTransitionENS0_4NameENS0_12PropertyKindENS0_18PropertyAttributesE, .-_ZN2v88internal19TransitionsAccessor16SearchTransitionENS0_4NameENS0_12PropertyKindENS0_18PropertyAttributesE
	.section	.text._ZN2v88internal19TransitionsAccessor28FindTransitionToDataPropertyENS0_6HandleINS0_4NameEEENS1_17RequestedLocationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19TransitionsAccessor28FindTransitionToDataPropertyENS0_6HandleINS0_4NameEEENS1_17RequestedLocationE
	.type	_ZN2v88internal19TransitionsAccessor28FindTransitionToDataPropertyENS0_6HandleINS0_4NameEEENS1_17RequestedLocationE, @function
_ZN2v88internal19TransitionsAccessor28FindTransitionToDataPropertyENS0_6HandleINS0_4NameEEENS1_17RequestedLocationE:
.LFB17788:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	-1(%rsi), %rax
	cmpw	$64, 11(%rax)
	jne	.L525
	xorl	%ecx, %ecx
	testb	$1, 11(%rsi)
	setne	%cl
	addl	%ecx, %ecx
.L525:
	movl	32(%rbx), %eax
	cmpl	$3, %eax
	je	.L526
	ja	.L544
.L534:
	xorl	%eax, %eax
.L532:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L545
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L544:
	.cfi_restore_state
	cmpl	$4, %eax
	jne	.L546
	movq	24(%rbx), %rax
	movq	%rsi, %rdx
	leaq	-48(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal15TransitionArray18SearchAndGetTargetENS0_12PropertyKindENS0_4NameENS0_18PropertyAttributesE
	movq	%rax, %r12
.L530:
	testq	%r12, %r12
	je	.L534
	movq	39(%r12), %rdx
	movl	15(%r12), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	leal	(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	movq	7(%rdx,%rax), %rax
	cmpl	$1, %r13d
	je	.L547
.L533:
	movq	(%rbx), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L535
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L526:
	movq	24(%rbx), %r12
	xorl	%edx, %edx
	andq	$-3, %r12
	movq	%r12, %rdi
	call	_ZN2v88internal19TransitionsAccessor13IsMatchingMapENS0_3MapENS0_4NameENS0_12PropertyKindENS0_18PropertyAttributesE
	testb	%al, %al
	jne	.L530
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L535:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L548
.L537:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r12, (%rax)
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L547:
	btq	$33, %rax
	jc	.L534
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L548:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L537
.L546:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L545:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17788:
	.size	_ZN2v88internal19TransitionsAccessor28FindTransitionToDataPropertyENS0_6HandleINS0_4NameEEENS1_17RequestedLocationE, .-_ZN2v88internal19TransitionsAccessor28FindTransitionToDataPropertyENS0_6HandleINS0_4NameEEENS1_17RequestedLocationE
	.section	.text._ZN2v88internal15TransitionArray6SearchENS0_12PropertyKindENS0_4NameENS0_18PropertyAttributesEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TransitionArray6SearchENS0_12PropertyKindENS0_4NameENS0_18PropertyAttributesEPi
	.type	_ZN2v88internal15TransitionArray6SearchENS0_12PropertyKindENS0_4NameENS0_18PropertyAttributesEPi, @function
_ZN2v88internal15TransitionArray6SearchENS0_12PropertyKindENS0_4NameENS0_18PropertyAttributesEPi:
.LFB17811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	%r8, %rbx
	cmpl	$1, 11(%rax)
	jle	.L553
	movl	%esi, %r13d
	movq	%rdx, %rsi
	movq	23(%rax), %rdx
	sarq	$32, %rdx
	je	.L553
	movl	%ecx, %r14d
	movq	%rdi, %r12
	movq	%r8, %rcx
	cmpq	$8, %rdx
	jle	.L566
	call	_ZN2v88internal12BinarySearchILNS0_10SearchModeE0ENS0_15TransitionArrayEEEiPT0_NS0_4NameEiPi
	movl	%eax, %esi
.L557:
	cmpl	$-1, %esi
	je	.L549
	movq	%rbx, %r8
	movl	%r14d, %ecx
	popq	%rbx
	movl	%r13d, %edx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal15TransitionArray13SearchDetailsEiNS0_12PropertyKindENS0_18PropertyAttributesEPi
	.p2align 4,,10
	.p2align 3
.L566:
	.cfi_restore_state
	call	_ZN2v88internal12LinearSearchILNS0_10SearchModeE0ENS0_15TransitionArrayEEEiPT0_NS0_4NameEiPi
	movl	%eax, %esi
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L553:
	testq	%rbx, %rbx
	je	.L549
	movl	$0, (%rbx)
.L549:
	popq	%rbx
	movl	$-1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17811:
	.size	_ZN2v88internal15TransitionArray6SearchENS0_12PropertyKindENS0_4NameENS0_18PropertyAttributesEPi, .-_ZN2v88internal15TransitionArray6SearchENS0_12PropertyKindENS0_4NameENS0_18PropertyAttributesEPi
	.section	.rodata._ZN2v88internal19TransitionsAccessor6InsertENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEENS0_20SimpleTransitionFlagE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"instance_type() >= FIRST_JS_RECEIVER_TYPE"
	.section	.rodata._ZN2v88internal19TransitionsAccessor6InsertENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEENS0_20SimpleTransitionFlagE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Check failed: %s."
.LC3:
	.string	"value.IsMap()"
	.section	.rodata._ZN2v88internal19TransitionsAccessor6InsertENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEENS0_20SimpleTransitionFlagE.str1.8
	.align 8
.LC4:
	.string	"GetBackPointer().IsUndefined()"
	.align 8
.LC5:
	.string	"value.IsMap() implies Map::cast(value).GetConstructor() == constructor_or_backpointer()"
	.align 8
.LC6:
	.string	"new_nof <= kMaxNumberOfTransitions"
	.section	.rodata._ZN2v88internal19TransitionsAccessor6InsertENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEENS0_20SimpleTransitionFlagE.str1.1
.LC7:
	.string	"0 <= max_slack"
	.section	.text._ZN2v88internal19TransitionsAccessor6InsertENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEENS0_20SimpleTransitionFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19TransitionsAccessor6InsertENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEENS0_20SimpleTransitionFlagE
	.type	_ZN2v88internal19TransitionsAccessor6InsertENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEENS0_20SimpleTransitionFlagE, @function
_ZN2v88internal19TransitionsAccessor6InsertENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEENS0_20SimpleTransitionFlagE:
.LFB17784:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdx), %r13
	movq	16(%rdi), %r14
	movq	%rsi, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpw	$1023, 11(%r13)
	jbe	.L854
	movq	-1(%r14), %rax
	cmpw	$68, 11(%rax)
	jne	.L855
	movq	%r13, %r15
	movl	%ecx, %ebx
	movq	31(%r13), %rcx
	movq	%rdi, %r12
	andq	$-262144, %r15
	leaq	31(%r13), %rsi
	movq	24(%r15), %rax
	movq	%rcx, %rdx
	subq	$37592, %rax
	testb	$1, %cl
	jne	.L856
.L570:
	movq	88(%rax), %rdx
.L571:
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	-37504(%rax), %rdx
	jne	.L857
	movq	-1(%r14), %rax
	cmpw	$68, 11(%rax)
	je	.L858
.L573:
	movq	%r14, 31(%r13)
	testb	$1, %r14b
	je	.L721
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -104(%rbp)
	testl	$262144, %eax
	je	.L577
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %rcx
	movq	-112(%rbp), %rsi
	movq	8(%rcx), %rax
.L577:
	testb	$24, %al
	je	.L721
	testb	$24, 8(%r15)
	je	.L859
.L721:
	movl	32(%r12), %eax
	leal	-1(%rax), %edx
	cmpl	$1, %edx
	ja	.L579
.L879:
	testl	%ebx, %ebx
	je	.L598
	movq	(%r12), %rdi
	movl	$1, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory18NewTransitionArrayEii@PLT
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal19TransitionsAccessor18ReplaceTransitionsENS0_11MaybeObjectE
	movq	8(%r12), %rax
	movq	(%rax), %rax
	movq	%rax, 16(%r12)
	movq	71(%rax), %rax
	movq	%rax, 24(%r12)
	testb	$1, %al
	jne	.L860
.L617:
	movl	$1, 32(%r12)
.L849:
	movq	24(%r12), %rax
.L584:
	movl	$-1, -68(%rbp)
	cmpl	$2, %ebx
	je	.L623
	movq	-88(%rbp), %rcx
	xorl	%r15d, %r15d
	movq	(%rcx), %rcx
	movl	15(%rcx), %edx
	movq	39(%rcx), %rcx
	shrl	$10, %edx
	andl	$1023, %edx
	leal	(%rdx,%rdx,2), %edx
	sall	$3, %edx
	movslq	%edx, %rdx
	movq	7(%rdx,%rcx), %r13
	movq	%rax, -64(%rbp)
	sarq	$32, %r13
	cmpl	$1, 11(%rax)
	jle	.L624
	movq	23(%rax), %r15
	shrq	$32, %r15
.L624:
	movq	-96(%rbp), %rax
	movl	%r13d, %r14d
	andl	$1, %r13d
	leaq	-64(%rbp), %rdi
	shrl	$3, %r14d
	movl	%r13d, %esi
	leaq	-68(%rbp), %r8
	movq	(%rax), %rdx
	andl	$7, %r14d
	movl	%r14d, %ecx
	call	_ZN2v88internal15TransitionArray6SearchENS0_12PropertyKindENS0_4NameENS0_18PropertyAttributesEPi
	leal	1(%r15), %esi
	cmpl	$-1, %eax
	jne	.L861
.L627:
	cmpl	$1536, %esi
	jg	.L862
	movq	-64(%rbp), %rax
	xorl	%edx, %edx
	movslq	11(%rax), %rcx
	cmpq	$2, %rcx
	jle	.L637
.L718:
	leal	-2(%rcx), %edx
	sarl	%edx
.L637:
	cmpl	%esi, %edx
	jge	.L863
	movl	$1536, %eax
	movq	(%r12), %rdi
	subl	%r15d, %eax
	js	.L864
	movl	$1, %edx
	cmpl	$3, %r15d
	jle	.L655
	movl	%r15d, %edx
	sarl	$2, %edx
	cmpl	%eax, %edx
	cmovg	%eax, %edx
.L655:
	call	_ZN2v88internal7Factory18NewTransitionArrayEii@PLT
	movq	%rax, %r9
	movq	8(%r12), %rax
	movq	(%rax), %rax
	movq	%rax, 16(%r12)
	movq	71(%rax), %rax
	movq	%rax, 24(%r12)
	testb	$1, %al
	jne	.L865
.L656:
	movl	$1, 32(%r12)
.L658:
	movq	%rax, -64(%rbp)
	cmpl	$1, 11(%rax)
	jle	.L662
	movq	23(%rax), %rdx
	sarq	$32, %rdx
	cmpl	%r15d, %edx
	jne	.L866
.L664:
	movq	15(%rax), %rax
	testq	%rax, %rax
	jne	.L867
.L676:
	movl	-68(%rbp), %eax
	testl	%eax, %eax
	jle	.L680
	xorl	%ecx, %ecx
	movl	%r15d, -140(%rbp)
	movl	$32, %r13d
	movq	%r12, -152(%rbp)
	movl	%ecx, %r15d
	movq	%r9, %r12
	.p2align 4,,10
	.p2align 3
.L687:
	movq	(%r12), %rdi
	movq	-64(%rbp), %rdx
	leaq	8(%r13), %r8
	addl	$1, %r15d
	movq	-1(%r8,%rdx), %rax
	leaq	-1(%rdi), %rbx
	movq	-1(%r13,%rdx), %r14
	leaq	(%rbx,%r13), %rsi
	movq	%r14, (%rsi)
	testb	$1, %r14b
	je	.L713
	cmpl	$3, %r14d
	je	.L713
	movq	%r14, %rdx
	andq	$-262144, %r14
	movq	8(%r14), %r10
	andq	$-3, %rdx
	testl	$262144, %r10d
	je	.L682
	movq	%r8, -136(%rbp)
	movq	%rax, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %r10
	movq	-136(%rbp), %r8
	movq	-128(%rbp), %rax
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdi
.L682:
	andl	$24, %r10d
	je	.L713
	movq	%rdi, %r10
	andq	$-262144, %r10
	testb	$24, 8(%r10)
	jne	.L713
	movq	%r8, -120(%rbp)
	movq	%rax, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-120(%rbp), %r8
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L713:
	leaq	(%rbx,%r8), %r14
	movq	%rax, (%r14)
	testb	$1, %al
	je	.L712
	cmpl	$3, %eax
	je	.L712
	movq	%rax, %rdx
	andq	$-262144, %rax
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %rdx
	testl	$262144, %eax
	je	.L685
	movq	%r14, %rsi
	movq	%rdx, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rdi
.L685:
	testb	$24, %al
	je	.L712
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L712
	movq	%r14, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L712:
	movl	-68(%rbp), %eax
	addq	$16, %r13
	cmpl	%r15d, %eax
	jg	.L687
	movq	%r12, %r9
	movl	-140(%rbp), %r15d
	movq	-152(%rbp), %r12
.L680:
	movq	-96(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	(%r9), %rdi
	movq	(%rcx), %rdx
	leal	2(%rax,%rax), %ecx
	movq	(%rsi), %r14
	leal	16(,%rcx,8), %r13d
	leaq	-1(%rdi), %rax
	movslq	%r13d, %r13
	movq	%r14, %rbx
	addq	%rax, %r13
	orq	$2, %rbx
	movq	%rdx, 0(%r13)
	testb	$1, %dl
	je	.L711
	cmpl	$3, %edx
	je	.L711
	movq	%rdx, %r10
	andq	$-262144, %rdx
	movq	%rdx, %r8
	movq	8(%rdx), %rdx
	andq	$-3, %r10
	movq	%r8, -88(%rbp)
	testl	$262144, %edx
	jne	.L868
.L689:
	andl	$24, %edx
	je	.L711
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L711
	movq	%r10, %rdx
	movq	%r13, %rsi
	movq	%rax, -112(%rbp)
	movl	%ecx, -104(%rbp)
	movq	%r9, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-112(%rbp), %rax
	movl	-104(%rbp), %ecx
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L711:
	leal	24(,%rcx,8), %r13d
	movslq	%r13d, %r13
	addq	%rax, %r13
	movq	%rbx, 0(%r13)
	testb	$1, %bl
	je	.L710
	cmpl	$3, %ebx
	je	.L710
	movq	%r14, %rbx
	andq	$-262144, %r14
	movq	8(%r14), %rax
	andq	$-3, %rbx
	testl	$262144, %eax
	jne	.L869
.L692:
	testb	$24, %al
	je	.L710
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L710
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L710:
	movslq	-68(%rbp), %rax
	cmpl	%eax, %r15d
	jle	.L702
	movl	%eax, %edx
	leal	2(%rax), %ebx
	movq	%r12, -128(%rbp)
	movq	%r9, %r12
	notl	%edx
	sall	$4, %ebx
	addl	%r15d, %edx
	movslq	%ebx, %rbx
	leaq	3(%rdx,%rax), %r14
	salq	$4, %r14
	.p2align 4,,10
	.p2align 3
.L703:
	movq	(%r12), %rdi
	movq	-64(%rbp), %rax
	movq	%rbx, %r8
	addq	$16, %rbx
	movq	-9(%rbx,%rax), %r13
	leaq	-1(%rdi), %rcx
	movq	-17(%rbx,%rax), %rax
	leaq	(%rbx,%rcx), %rsi
	movq	%rax, (%rsi)
	testb	$1, %al
	je	.L709
	cmpl	$3, %eax
	je	.L709
	movq	%rax, %rdx
	andq	$-262144, %rax
	movq	%rax, %r15
	movq	8(%rax), %rax
	andq	$-3, %rdx
	testl	$262144, %eax
	je	.L697
	movq	%rcx, -120(%rbp)
	movq	%r8, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-120(%rbp), %rcx
	movq	-112(%rbp), %r8
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdi
.L697:
	testb	$24, %al
	je	.L709
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L709
	movq	%rcx, -104(%rbp)
	movq	%r8, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %rcx
	movq	-96(%rbp), %r8
	movq	-88(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L709:
	leaq	24(%r8,%rcx), %r15
	movq	%r13, (%r15)
	testb	$1, %r13b
	je	.L708
	cmpl	$3, %r13d
	je	.L708
	movq	%r13, %rdx
	andq	$-262144, %r13
	movq	8(%r13), %rax
	andq	$-3, %rdx
	testl	$262144, %eax
	je	.L700
	movq	%r15, %rsi
	movq	%rdx, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r13), %rax
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rdi
.L700:
	testb	$24, %al
	je	.L708
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L708
	movq	%r15, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L708:
	cmpq	%r14, %rbx
	jne	.L703
	movq	%r12, %r9
	movq	-128(%rbp), %r12
.L702:
	movq	(%r9), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19TransitionsAccessor18ReplaceTransitionsENS0_11MaybeObjectE
.L567:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L870
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L856:
	.cfi_restore_state
	movq	-1(%rcx), %rdi
	cmpq	%rdi, 136(%rax)
	jne	.L570
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L579:
	cmpl	$3, %eax
	jne	.L849
.L586:
	movq	24(%r12), %rax
	movq	%rax, %r13
	andq	$-3, %r13
	je	.L584
	movl	15(%r13), %eax
	movq	39(%r13), %rcx
	shrl	$10, %eax
	andl	$1023, %eax
	leal	(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rax,%rcx), %rdx
	movl	15(%r13), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	leal	(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	movq	7(%rcx,%rax), %r14
	cmpl	$2, %ebx
	je	.L592
	movq	-88(%rbp), %rax
	movq	(%rax), %rcx
	movl	15(%rcx), %eax
	movq	39(%rcx), %rcx
	shrl	$10, %eax
	andl	$1023, %eax
	leal	(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	movq	7(%rax,%rcx), %r15
	testl	%ebx, %ebx
	jne	.L592
	movq	-96(%rbp), %rax
	movq	(%rax), %rsi
	cmpq	%rdx, %rsi
	je	.L595
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L596
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	je	.L592
.L596:
	movq	-1(%rdx), %rax
	cmpw	$64, 11(%rax)
	je	.L592
	movq	-1(%rsi), %rax
	cmpw	$64, 11(%rax)
	je	.L592
	leaq	-64(%rbp), %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal6String10SlowEqualsES1_@PLT
	testb	%al, %al
	je	.L592
.L595:
	sarq	$32, %r14
	sarq	$32, %r15
	movl	%r15d, %edx
	movl	%r14d, %eax
	andl	$1, %edx
	andl	$1, %eax
	cmpl	%eax, %edx
	jne	.L592
	shrl	$3, %r15d
	shrl	$3, %r14d
	andl	$7, %r15d
	andl	$7, %r14d
	cmpl	%r14d, %r15d
	je	.L598
	.p2align 4,,10
	.p2align 3
.L592:
	movq	(%r12), %r14
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L871
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L601:
	movq	(%r12), %rdi
	movl	$1, %edx
	movl	$1, %esi
	call	_ZN2v88internal7Factory18NewTransitionArrayEii@PLT
	movq	%rax, %r14
	movq	8(%r12), %rax
	movq	(%rax), %rax
	movq	%rax, 16(%r12)
	movq	71(%rax), %r13
	movq	%r13, 24(%r12)
	testb	$1, %r13b
	je	.L603
	cmpl	$3, %r13d
	je	.L603
	movq	%r13, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	je	.L872
	cmpq	$1, %rax
	jne	.L587
	movq	-1(%r13), %rax
	cmpw	$149, 11(%rax)
	jne	.L608
	movl	$4, 32(%r12)
	movq	(%r14), %r15
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L623:
	movq	%rax, -64(%rbp)
	cmpl	$1, 11(%rax)
	jle	.L873
	movq	23(%rax), %rsi
	movq	-96(%rbp), %rcx
	movq	(%rcx), %r8
	movq	23(%rax), %rdx
	sarq	$32, %rsi
	movl	%esi, %r15d
	sarq	$32, %rdx
	je	.L874
	leaq	-68(%rbp), %rcx
	leaq	-64(%rbp), %rdi
	movq	%r8, %rsi
	cmpq	$8, %rdx
	jle	.L875
	call	_ZN2v88internal12BinarySearchILNS0_10SearchModeE0ENS0_15TransitionArrayEEEiPT0_NS0_4NameEiPi
.L629:
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	leal	1(%r15), %esi
	cmpl	$-1, %eax
	je	.L627
.L861:
	movq	-88(%rbp), %rsi
	leal	5(%rax,%rax), %ebx
	movq	-64(%rbp), %rax
	sall	$3, %ebx
	movq	(%rsi), %rcx
	movslq	%ebx, %rbx
	movq	%rcx, %rdx
	orq	$2, %rdx
	movq	%rdx, -1(%rbx,%rax)
	testb	$1, %dl
	je	.L567
	cmpl	$3, %edx
	je	.L567
	movq	%rcx, %r13
	andq	$-262144, %rcx
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
	andq	$-3, %r13
	movq	%rcx, %r12
	leaq	-1(%rbx,%rdi), %rsi
	testl	$262144, %eax
	jne	.L876
.L633:
	testb	$24, %al
	je	.L567
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L567
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L860:
	cmpl	$3, %eax
	je	.L617
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L877
.L620:
	cmpq	$1, %rdx
	je	.L878
.L587:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L859:
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movl	32(%r12), %eax
	leal	-1(%rax), %edx
	cmpl	$1, %edx
	jbe	.L879
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L865:
	cmpl	$3, %eax
	je	.L656
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L880
	cmpq	$1, %rdx
	jne	.L587
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L660
	movl	$4, 32(%r12)
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L598:
	movq	-88(%rbp), %rax
	movq	%r12, %rdi
	movq	(%rax), %rsi
	orq	$2, %rsi
	call	_ZN2v88internal19TransitionsAccessor18ReplaceTransitionsENS0_11MaybeObjectE
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L603:
	movl	$1, 32(%r12)
	movq	(%r14), %r15
.L605:
	movq	$0, 23(%r15)
.L616:
	movq	(%r14), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19TransitionsAccessor18ReplaceTransitionsENS0_11MaybeObjectE
	movq	8(%r12), %rax
	movq	(%rax), %rax
	movq	%rax, 16(%r12)
	movq	71(%rax), %rax
	movq	%rax, 24(%r12)
	testb	$1, %al
	je	.L617
	cmpl	$3, %eax
	je	.L617
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	jne	.L620
	movl	$3, 32(%r12)
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L854:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L855:
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L871:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L881
.L602:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%r13, (%rax)
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L857:
	leaq	.LC4(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L875:
	call	_ZN2v88internal12LinearSearchILNS0_10SearchModeE0ENS0_15TransitionArrayEEEiPT0_NS0_4NameEiPi
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L858:
	movq	31(%r14), %rax
	testb	$1, %al
	jne	.L575
.L574:
	cmpq	%rax, %rcx
	je	.L573
	leaq	.LC5(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L662:
	testl	%r15d, %r15d
	je	.L664
	xorl	%r15d, %r15d
.L666:
	movq	-96(%rbp), %rax
	movl	$-1, -68(%rbp)
	movq	(%rax), %r10
	cmpl	$2, %ebx
	je	.L882
	leaq	-64(%rbp), %rdi
	leaq	-68(%rbp), %r8
	movl	%r14d, %ecx
	movq	%r10, %rdx
	movl	%r13d, %esi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal15TransitionArray6SearchENS0_12PropertyKindENS0_4NameENS0_18PropertyAttributesEPi
	movq	-104(%rbp), %r9
.L673:
	cmpl	$-1, %eax
	je	.L669
	movl	%eax, -68(%rbp)
	movl	%r15d, %eax
.L675:
	movq	(%r9), %rdx
	salq	$32, %rax
	movq	%rax, 23(%rdx)
	movq	-64(%rbp), %rax
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L863:
	movq	%rsi, %rdx
	salq	$32, %rdx
	movq	%rdx, 23(%rax)
	movq	-64(%rbp), %rdi
	cmpl	%r15d, -68(%rbp)
	jge	.L639
	leal	1(%r15), %ebx
	sall	$4, %ebx
	movslq	%ebx, %rbx
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L841:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L850
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rdx
	leaq	-1(%rdx), %rcx
	.p2align 4,,10
	.p2align 3
.L707:
	movq	8(%rcx,%rbx), %rax
	leaq	8(%rbx), %r14
	leaq	24(%rbx), %r13
	movq	%rax, 23(%rdx,%rbx)
	movq	-64(%rbp), %rdi
	leaq	-1(%rdi), %rdx
	testb	$1, %al
	je	.L706
	cmpl	$3, %eax
	je	.L706
	movq	%rax, %r8
	andq	$-262144, %rax
	leaq	0(%r13,%rdx), %rsi
	movq	%rax, %r12
	movq	8(%rax), %rax
	andq	$-3, %r8
	testl	$262144, %eax
	je	.L644
	movq	%r8, %rdx
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rdi
	movq	8(%r12), %rax
	movq	-104(%rbp), %r8
	leaq	-1(%rdi), %rdx
	leaq	0(%r13,%rdx), %rsi
.L644:
	testb	$24, %al
	je	.L706
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L706
	movq	%r8, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rdi
	leaq	-1(%rdi), %rdx
	.p2align 4,,10
	.p2align 3
.L706:
	leaq	-16(%rbx), %rax
	cmpl	%r15d, -68(%rbp)
	jge	.L646
	movq	%rax, %rbx
.L647:
	movq	-1(%rbx,%rdi), %rax
	movq	-64(%rbp), %rdx
	subl	$1, %r15d
	leaq	16(%rbx), %r13
	movq	%rax, 15(%rdx,%rbx)
	movq	-64(%rbp), %rdi
	leaq	-1(%rdi), %rcx
	movq	%rdi, %rdx
	testb	$1, %al
	je	.L707
	cmpl	$3, %eax
	je	.L707
	movq	%rax, %r14
	andq	$-262144, %rax
	leaq	(%rcx,%r13), %rsi
	movq	%rax, %r12
	movq	8(%rax), %rax
	andq	$-3, %r14
	testl	$262144, %eax
	je	.L641
	movq	%r14, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rdi
	movq	8(%r12), %rax
	leaq	-1(%rdi), %rcx
	leaq	0(%r13,%rcx), %rsi
.L641:
	testb	$24, %al
	jne	.L841
.L850:
	movq	-64(%rbp), %rdx
	jmp	.L707
.L639:
	addl	%esi, %esi
	leaq	-1(%rdi), %rdx
	leal	24(,%rsi,8), %r14d
	leal	16(,%rsi,8), %ebx
	movslq	%r14d, %r14
	movslq	%ebx, %rbx
	.p2align 4,,10
	.p2align 3
.L646:
	movq	-96(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx,%rbx)
	movq	-64(%rbp), %rdi
	leaq	-1(%rdi), %rcx
	testb	$1, %al
	je	.L705
	cmpl	$3, %eax
	je	.L705
	movq	%rax, %r13
	andq	$-262144, %rax
	leaq	(%rcx,%rbx), %rsi
	movq	%rax, %r12
	movq	8(%rax), %rax
	andq	$-3, %r13
	testl	$262144, %eax
	je	.L649
	movq	%r13, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rdi
	movq	8(%r12), %rax
	leaq	-1(%rdi), %rcx
	leaq	(%rbx,%rcx), %rsi
.L649:
	testb	$24, %al
	je	.L705
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L705
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rax
	leaq	-1(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L705:
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	orq	$2, %rdx
	movq	%rdx, (%r14,%rcx)
	testb	$1, %dl
	je	.L567
	cmpl	$3, %edx
	je	.L567
	movq	%rax, %r12
	andq	$-262144, %rax
	movq	-64(%rbp), %rdi
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %r12
	leaq	-1(%rdi,%r14), %rsi
	testl	$262144, %eax
	je	.L652
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rdi
	movq	8(%rbx), %rax
	leaq	-1(%rdi,%r14), %rsi
.L652:
	testb	$24, %al
	je	.L567
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L567
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L867:
	movq	-64(%rbp), %rax
	movq	(%r9), %r13
	movq	15(%rax), %rax
	leaq	15(%r13), %r14
	movq	%rax, 15(%r13)
	testb	$1, %al
	je	.L676
	cmpl	$3, %eax
	je	.L676
	movq	%rax, %rdx
	andq	$-262144, %rax
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %rdx
	testl	$262144, %eax
	je	.L678
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-112(%rbp), %r9
	movq	-104(%rbp), %rdx
.L678:
	testb	$24, %al
	je	.L676
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L676
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %r9
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L873:
	movslq	11(%rax), %rcx
	movl	$0, -68(%rbp)
	cmpq	$2, %rcx
	jle	.L717
	xorl	%r15d, %r15d
	movl	$1, %esi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L869:
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r9, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdi
	jmp	.L692
.L868:
	movq	%r10, %rdx
	movq	%r13, %rsi
	movq	%rax, -128(%rbp)
	movl	%ecx, -120(%rbp)
	movq	%r9, -112(%rbp)
	movq	%r10, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r8
	movq	-128(%rbp), %rax
	movl	-120(%rbp), %ecx
	movq	-112(%rbp), %r9
	movq	8(%r8), %rdx
	movq	-104(%rbp), %r10
	movq	-96(%rbp), %rdi
	jmp	.L689
.L876:
	movq	%r13, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rdi
	movq	8(%r12), %rax
	leaq	-1(%rbx,%rdi), %rsi
	jmp	.L633
.L575:
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L574
	movq	31(%rax), %rax
	testb	$1, %al
	je	.L574
	jmp	.L575
.L880:
	movl	$3, 32(%r12)
	jmp	.L658
.L872:
	movl	$3, 32(%r12)
	movq	%r13, %r8
	movq	(%r14), %r15
	andq	$-3, %r8
	je	.L605
	movl	15(%r8), %eax
	movq	39(%r8), %rdx
	movq	%r8, %rcx
	leaq	31(%r15), %rsi
	orq	$2, %rcx
	shrl	$10, %eax
	andl	$1023, %eax
	leal	(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rax,%rdx), %rax
	movq	%rax, 31(%r15)
	testb	$1, %al
	je	.L720
	cmpl	$3, %eax
	je	.L720
	movq	%rax, %rdx
	andq	$-262144, %rax
	movq	8(%rax), %rdi
	movq	%rax, -104(%rbp)
	andq	$-3, %rdx
	testl	$262144, %edi
	je	.L611
	movq	%r15, %rdi
	movq	%r8, -136(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %rax
	movq	-136(%rbp), %r8
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %rdx
	movq	8(%rax), %rdi
	movq	-112(%rbp), %rsi
.L611:
	andl	$24, %edi
	je	.L720
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L720
	movq	%r15, %rdi
	movq	%r8, -112(%rbp)
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-112(%rbp), %r8
	movq	-104(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L720:
	movq	%rcx, 39(%r15)
	leaq	39(%r15), %rsi
	testb	$1, %cl
	je	.L616
	cmpl	$3, %ecx
	je	.L616
	andq	$-262144, %r13
	movq	8(%r13), %rax
	testl	$262144, %eax
	je	.L614
	movq	%r8, %rdx
	movq	%r15, %rdi
	movq	%r8, -112(%rbp)
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r13), %rax
	movq	-112(%rbp), %r8
	movq	-104(%rbp), %rsi
.L614:
	testb	$24, %al
	je	.L616
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L616
	movq	%r8, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L866:
	movq	23(%rax), %r15
	shrq	$32, %r15
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L862:
	leaq	.LC6(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L877:
	movl	$3, 32(%r12)
	jmp	.L586
.L670:
	movl	$0, -68(%rbp)
.L669:
	leal	1(%r15), %eax
	jmp	.L675
.L882:
	movq	-64(%rbp), %rax
	cmpl	$1, 11(%rax)
	jle	.L670
	movq	23(%rax), %rdx
	sarq	$32, %rdx
	je	.L670
	movq	%r9, -104(%rbp)
	leaq	-68(%rbp), %rcx
	leaq	-64(%rbp), %rdi
	movq	%r10, %rsi
	cmpq	$8, %rdx
	jle	.L883
	call	_ZN2v88internal12BinarySearchILNS0_10SearchModeE0ENS0_15TransitionArrayEEEiPT0_NS0_4NameEiPi
	movq	-104(%rbp), %r9
	jmp	.L673
.L864:
	leaq	.LC7(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L878:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L621
	movl	$4, 32(%r12)
	jmp	.L584
.L881:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L602
.L883:
	call	_ZN2v88internal12LinearSearchILNS0_10SearchModeE0ENS0_15TransitionArrayEEEiPT0_NS0_4NameEiPi
	movq	-104(%rbp), %r9
	jmp	.L673
.L621:
	movq	-1(%rax), %rdx
	cmpw	$95, 11(%rdx)
	je	.L884
	movl	$2, 32(%r12)
	jmp	.L584
.L660:
	movq	-1(%rax), %rdx
	cmpw	$95, 11(%rdx)
	setne	%dl
	movzbl	%dl, %edx
	addl	%edx, %edx
	movl	%edx, 32(%r12)
	jmp	.L658
.L608:
	movq	-1(%r13), %rax
	cmpw	$95, 11(%rax)
	je	.L885
	movl	$2, 32(%r12)
	movq	(%r14), %r15
	jmp	.L605
.L884:
	movl	$0, 32(%r12)
	jmp	.L584
.L885:
	movl	$0, 32(%r12)
	movq	(%r14), %r15
	jmp	.L605
.L874:
	movl	$0, -68(%rbp)
	addl	$1, %esi
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	jmp	.L627
.L717:
	movq	(%r12), %rdi
	movl	$1, %esi
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movl	$1, %edx
	jmp	.L655
.L870:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17784:
	.size	_ZN2v88internal19TransitionsAccessor6InsertENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEENS0_20SimpleTransitionFlagE, .-_ZN2v88internal19TransitionsAccessor6InsertENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEENS0_20SimpleTransitionFlagE
	.section	.text._ZN2v88internal19TransitionsAccessor29HasIntegrityLevelTransitionToENS0_3MapEPNS0_6SymbolEPNS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19TransitionsAccessor29HasIntegrityLevelTransitionToENS0_3MapEPNS0_6SymbolEPNS0_18PropertyAttributesE
	.type	_ZN2v88internal19TransitionsAccessor29HasIntegrityLevelTransitionToENS0_3MapEPNS0_6SymbolEPNS0_18PropertyAttributesE, @function
_ZN2v88internal19TransitionsAccessor29HasIntegrityLevelTransitionToENS0_3MapEPNS0_6SymbolEPNS0_18PropertyAttributesE:
.LFB17814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$4, 32(%rdi)
	je	.L887
.L890:
	xorl	%eax, %eax
.L888:
	cmpq	%rax, %rbx
	jne	.L897
	testq	%r14, %r14
	je	.L898
	movl	$5, (%r14)
.L898:
	testq	%r13, %r13
	je	.L930
	movq	3704(%r15), %rax
	movq	%rax, 0(%r13)
	movl	$1, %eax
.L886:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L931
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L887:
	.cfi_restore_state
	movq	24(%rdi), %rsi
	movq	3704(%r15), %r8
	movq	%rsi, -64(%rbp)
	cmpl	$1, 11(%rsi)
	jle	.L890
	movq	23(%rsi), %rdx
	sarq	$32, %rdx
	movl	%edx, %edi
	je	.L890
	cmpq	$8, %rdx
	jg	.L892
	testl	%edx, %edx
	jle	.L890
	xorl	%edx, %edx
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L932:
	addq	$1, %rdx
	cmpl	%edx, %edi
	jle	.L890
.L895:
	movq	%rdx, %rcx
	movl	%edx, %eax
	salq	$4, %rcx
	addq	%rsi, %rcx
	movq	31(%rcx), %rcx
	cmpq	%rcx, %r8
	jne	.L932
.L896:
	cmpl	$-1, %eax
	je	.L890
	leal	5(%rax,%rax), %eax
	movq	24(%r12), %rdx
	sall	$3, %eax
	cltq
	movq	-1(%rax,%rdx), %rax
	andq	$-3, %rax
	jmp	.L888
	.p2align 4,,10
	.p2align 3
.L901:
	movq	3752(%r15), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19TransitionsAccessor13SearchSpecialENS0_6SymbolE
	movq	%rax, %r8
	xorl	%eax, %eax
	cmpq	%rbx, %r8
	jne	.L886
	testq	%r14, %r14
	je	.L904
	movl	$0, (%r14)
.L904:
	testq	%r13, %r13
	je	.L930
	movq	3752(%r15), %rax
	movq	%rax, 0(%r13)
.L930:
	movl	$1, %eax
	jmp	.L886
	.p2align 4,,10
	.p2align 3
.L897:
	movq	3800(%r15), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19TransitionsAccessor13SearchSpecialENS0_6SymbolE
	cmpq	%rbx, %rax
	jne	.L901
	testq	%r14, %r14
	je	.L902
	movl	$4, (%r14)
.L902:
	testq	%r13, %r13
	je	.L930
	movq	3800(%r15), %rax
	movq	%rax, 0(%r13)
	movl	$1, %eax
	jmp	.L886
	.p2align 4,,10
	.p2align 3
.L892:
	leaq	-64(%rbp), %rdi
	xorl	%ecx, %ecx
	movq	%r8, %rsi
	call	_ZN2v88internal12BinarySearchILNS0_10SearchModeE0ENS0_15TransitionArrayEEEiPT0_NS0_4NameEiPi
	jmp	.L896
.L931:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17814:
	.size	_ZN2v88internal19TransitionsAccessor29HasIntegrityLevelTransitionToENS0_3MapEPNS0_6SymbolEPNS0_18PropertyAttributesE, .-_ZN2v88internal19TransitionsAccessor29HasIntegrityLevelTransitionToENS0_3MapEPNS0_6SymbolEPNS0_18PropertyAttributesE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal19TransitionsAccessor21HasSimpleTransitionToENS0_3MapE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal19TransitionsAccessor21HasSimpleTransitionToENS0_3MapE, @function
_GLOBAL__sub_I__ZN2v88internal19TransitionsAccessor21HasSimpleTransitionToENS0_3MapE:
.LFB21492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21492:
	.size	_GLOBAL__sub_I__ZN2v88internal19TransitionsAccessor21HasSimpleTransitionToENS0_3MapE, .-_GLOBAL__sub_I__ZN2v88internal19TransitionsAccessor21HasSimpleTransitionToENS0_3MapE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal19TransitionsAccessor21HasSimpleTransitionToENS0_3MapE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
