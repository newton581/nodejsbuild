	.file	"pending-optimization-table.cc"
	.text
	.section	.text._ZN2v88internal24PendingOptimizationTable23PreparedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PendingOptimizationTable23PreparedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEb
	.type	_ZN2v88internal24PendingOptimizationTable23PreparedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEb, @function
_ZN2v88internal24PendingOptimizationTable23PreparedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEb:
.LFB19472:
	.cfi_startproc
	endbr64
	movabsq	$21474836480, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	testb	%dl, %dl
	movq	4784(%rdi), %rsi
	movabsq	$4294967296, %rdx
	cmove	%rdx, %rax
	movq	%rax, %r15
	testb	$1, %sil
	jne	.L22
.L3:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L4:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L7
.L26:
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L8:
	movq	(%r12), %rax
	movq	23(%rax), %rax
	movq	31(%rax), %rdx
	testb	$1, %dl
	jne	.L23
.L10:
	movq	7(%rax), %rdx
	testb	$1, %dl
	jne	.L24
.L12:
	movq	7(%rax), %rax
	movq	7(%rax), %r15
.L11:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L13
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L14:
	movq	%r14, %rdx
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	call	_ZN2v88internal7Factory9NewTuple2ENS0_6HandleINS0_6ObjectEEES4_NS0_14AllocationTypeE@PLT
	movq	%rax, %r14
	movq	(%r12), %rax
	movq	23(%rax), %r12
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L16
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L17:
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE3PutENS0_6HandleIS2_EENS5_INS0_6ObjectEEES8_@PLT
	movq	(%rax), %rax
	movq	%rax, 4784(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L25
.L6:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L26
.L7:
	movq	41088(%rbx), %r14
	cmpq	41096(%rbx), %r14
	je	.L27
.L9:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%r15, (%r14)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L16:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L28
.L18:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r12, (%rsi)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L13:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L29
.L15:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r15, (%rsi)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L23:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L10
	movq	39(%rdx), %rcx
	testb	$1, %cl
	je	.L10
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L10
	movq	31(%rdx), %r15
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L22:
	movq	%rsi, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	%rsi, -37504(%rax)
	jne	.L3
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v88internal9HashTableINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	%rax, %r13
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L24:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L12
	movq	7(%rax), %r15
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r14
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L28:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L6
	.cfi_endproc
.LFE19472:
	.size	_ZN2v88internal24PendingOptimizationTable23PreparedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEb, .-_ZN2v88internal24PendingOptimizationTable23PreparedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEb
	.section	.text._ZN2v88internal24PendingOptimizationTable30IsHeuristicOptimizationAllowedEPNS0_7IsolateENS0_10JSFunctionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PendingOptimizationTable30IsHeuristicOptimizationAllowedEPNS0_7IsolateENS0_10JSFunctionE
	.type	_ZN2v88internal24PendingOptimizationTable30IsHeuristicOptimizationAllowedEPNS0_7IsolateENS0_10JSFunctionE, @function
_ZN2v88internal24PendingOptimizationTable30IsHeuristicOptimizationAllowedEPNS0_7IsolateENS0_10JSFunctionE:
.LFB19477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	4784(%rdi), %rsi
	movq	41112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L31
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	testb	$1, %sil
	jne	.L51
.L34:
	movq	%rsi, -32(%rbp)
	movq	23(%r12), %r12
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L38
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L39:
	leaq	-32(%rbp), %rdi
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE6LookupENS0_6HandleINS0_6ObjectEEE@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L41
.L48:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rdx
	testb	$1, %dl
	jne	.L52
.L43:
	movslq	19(%rdx), %rax
	shrl	$2, %eax
	andl	$1, %eax
.L30:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L53
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L54
.L40:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r12, (%rsi)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L31:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L55
.L33:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	testb	$1, %sil
	je	.L34
.L51:
	movq	%rsi, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	%rsi, -37504(%rax)
	jne	.L34
	movq	41112(%rbx), %rdi
	movq	96(%rbx), %rsi
	testq	%rdi, %rdi
	jne	.L48
	.p2align 4,,10
	.p2align 3
.L41:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L56
.L42:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	movq	(%rax), %rdx
	testb	$1, %dl
	je	.L43
.L52:
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rcx
	movl	$1, %eax
	cmpq	%rdx, -37496(%rcx)
	je	.L30
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L56:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L55:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L54:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L40
.L53:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19477:
	.size	_ZN2v88internal24PendingOptimizationTable30IsHeuristicOptimizationAllowedEPNS0_7IsolateENS0_10JSFunctionE, .-_ZN2v88internal24PendingOptimizationTable30IsHeuristicOptimizationAllowedEPNS0_7IsolateENS0_10JSFunctionE
	.section	.rodata._ZN2v88internal24PendingOptimizationTable21MarkedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Error: Function "
	.section	.rodata._ZN2v88internal24PendingOptimizationTable21MarkedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	" should be prepared for optimization with %%PrepareFunctionForOptimize before  %%OptimizeFunctionOnNextCall / %%OptimizeOSR "
	.section	.rodata._ZN2v88internal24PendingOptimizationTable21MarkedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE.str1.1
.LC2:
	.string	"unreachable code"
	.section	.text._ZN2v88internal24PendingOptimizationTable21MarkedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PendingOptimizationTable21MarkedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	.type	_ZN2v88internal24PendingOptimizationTable21MarkedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE, @function
_ZN2v88internal24PendingOptimizationTable21MarkedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE:
.LFB19483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	4784(%rdi), %rsi
	movq	41112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L58
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
	testb	$1, %sil
	jne	.L82
.L61:
	movq	0(%r13), %rax
	movq	%rsi, -48(%rbp)
	movq	23(%rax), %r14
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L65
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L66:
	leaq	-48(%rbp), %rdi
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE6LookupENS0_6HandleINS0_6ObjectEEE@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L68
.L79:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
	movq	(%r14), %rax
	testb	$1, %al
	jne	.L83
.L70:
	movslq	19(%rax), %rcx
	andl	$-2, %ecx
	orl	$2, %ecx
	salq	$32, %rcx
	movq	%rcx, 15(%rax)
	movq	0(%r13), %rax
	movq	23(%rax), %r13
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L84
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L74:
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE3PutENS0_6HandleIS2_EENS5_INS0_6ObjectEEES8_@PLT
	movq	(%rax), %rax
	movq	%rax, 4784(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L86
.L67:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r14, (%rsi)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L58:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L87
.L60:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	testb	$1, %sil
	je	.L61
.L82:
	movq	%rsi, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	%rsi, -37504(%rax)
	jne	.L61
	movq	41112(%rbx), %rdi
	movq	96(%rbx), %rsi
	testq	%rdi, %rdi
	jne	.L79
	.p2align 4,,10
	.p2align 3
.L68:
	movq	41088(%rbx), %r14
	cmpq	41096(%rbx), %r14
	je	.L88
.L69:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r14)
	movq	(%r14), %rax
	testb	$1, %al
	je	.L70
.L83:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37496(%rdx)
	jne	.L70
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	0(%r13), %rax
	movq	stdout(%rip), %rsi
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L84:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L89
.L75:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r13, (%rsi)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L88:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L87:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L89:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L86:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L67
.L85:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19483:
	.size	_ZN2v88internal24PendingOptimizationTable21MarkedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE, .-_ZN2v88internal24PendingOptimizationTable21MarkedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	.section	.text._ZN2v88internal24PendingOptimizationTable20FunctionWasOptimizedEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PendingOptimizationTable20FunctionWasOptimizedEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	.type	_ZN2v88internal24PendingOptimizationTable20FunctionWasOptimizedEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE, @function
_ZN2v88internal24PendingOptimizationTable20FunctionWasOptimizedEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE:
.LFB19484:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	4784(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testb	$1, %sil
	jne	.L91
.L95:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L115
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L96:
	movq	(%r12), %rax
	movq	%rsi, -48(%rbp)
	movq	23(%rax), %r14
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L98
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L99:
	leaq	-48(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE6LookupENS0_6HandleINS0_6ObjectEEE@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L101
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L104
.L107:
	cmpl	$2, 19(%rax)
	je	.L116
.L90:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L117
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L118
.L97:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L101:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L119
.L103:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	movq	(%rax), %rax
	testb	$1, %al
	je	.L107
.L104:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37496(%rdx)
	je	.L90
	cmpl	$2, 19(%rax)
	jne	.L90
	.p2align 4,,10
	.p2align 3
.L116:
	movq	(%r12), %rax
	movq	23(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L108
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L109:
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE6RemoveEPNS0_7IsolateENS0_6HandleIS2_EENS7_INS0_6ObjectEEEPb@PLT
	movq	(%rax), %rax
	movq	%rax, 4784(%rbx)
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L98:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L120
.L100:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r14, (%rsi)
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%rsi, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	%rsi, -37504(%rax)
	je	.L90
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L120:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L118:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L119:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L108:
	movq	41088(%rbx), %rdx
	cmpq	41096(%rbx), %rdx
	je	.L121
.L110:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdx)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L121:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L110
.L117:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19484:
	.size	_ZN2v88internal24PendingOptimizationTable20FunctionWasOptimizedEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE, .-_ZN2v88internal24PendingOptimizationTable20FunctionWasOptimizedEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal24PendingOptimizationTable23PreparedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEb,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal24PendingOptimizationTable23PreparedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEb, @function
_GLOBAL__sub_I__ZN2v88internal24PendingOptimizationTable23PreparedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEb:
.LFB24409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24409:
	.size	_GLOBAL__sub_I__ZN2v88internal24PendingOptimizationTable23PreparedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEb, .-_GLOBAL__sub_I__ZN2v88internal24PendingOptimizationTable23PreparedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEb
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal24PendingOptimizationTable23PreparedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEb
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
