	.file	"compiler-dispatcher.cc"
	.text
	.section	.text._ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB1885:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE1885:
	.size	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5352:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5352:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5353:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5353:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5355:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5355:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS1_4base9LockGuardINS4_5MutexELNS4_12NullBehaviorE0EEEEUldE_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS1_4base9LockGuardINS4_5MutexELNS4_12NullBehaviorE0EEEEUldE_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS1_4base9LockGuardINS4_5MutexELNS4_12NullBehaviorE0EEEEUldE_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation:
.LFB25258:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L7
	cmpl	$3, %edx
	je	.L8
	cmpl	$1, %edx
	je	.L12
.L8:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE25258:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS1_4base9LockGuardINS4_5MutexELNS4_12NullBehaviorE0EEEEUldE_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS1_4base9LockGuardINS4_5MutexELNS4_12NullBehaviorE0EEEEUldE_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal18CompilerDispatcher31ScheduleMoreWorkerTasksIfNeededEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal18CompilerDispatcher31ScheduleMoreWorkerTasksIfNeededEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal18CompilerDispatcher31ScheduleMoreWorkerTasksIfNeededEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation:
.LFB25292:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L14
	cmpl	$3, %edx
	je	.L15
	cmpl	$1, %edx
	je	.L19
.L15:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE25292:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal18CompilerDispatcher31ScheduleMoreWorkerTasksIfNeededEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal18CompilerDispatcher31ScheduleMoreWorkerTasksIfNeededEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB25392:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE25392:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEE11DeleteArrayEPv,"axG",@progbits,_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEE11DeleteArrayEPv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEE11DeleteArrayEPv
	.type	_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEE11DeleteArrayEPv, @function
_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEE11DeleteArrayEPv:
.LFB28138:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE28138:
	.size	_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEE11DeleteArrayEPv, .-_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEE11DeleteArrayEPv
	.section	.text._ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEE15NewPointerArrayEm,"axG",@progbits,_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEE15NewPointerArrayEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEE15NewPointerArrayEm
	.type	_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEE15NewPointerArrayEm, @function
_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEE15NewPointerArrayEm:
.LFB28137:
	.cfi_startproc
	endbr64
	leaq	0(,%rsi,8), %rdi
	jmp	_ZN2v88internal8MallocednwEm@PLT
	.cfi_endproc
.LFE28137:
	.size	_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEE15NewPointerArrayEm, .-_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEE15NewPointerArrayEm
	.section	.text._ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEED2Ev,"axG",@progbits,_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEED2Ev
	.type	_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEED2Ev, @function
_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEED2Ev:
.LFB23620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal15IdentityMapBase5ClearEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal15IdentityMapBaseD2Ev@PLT
	.cfi_endproc
.LFE23620:
	.size	_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEED2Ev, .-_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEED2Ev
	.weak	_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEED1Ev
	.set	_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEED1Ev,_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEED2Ev
	.section	.text._ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEED0Ev,"axG",@progbits,_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEED0Ev
	.type	_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEED0Ev, @function
_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEED0Ev:
.LFB23622:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal15IdentityMapBase5ClearEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal15IdentityMapBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23622:
	.size	_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEED0Ev, .-_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEED0Ev
	.section	.text._ZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE.part.0, @function
_ZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE.part.0:
.LFB28215:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS1_4base9LockGuardINS4_5MutexELNS4_12NullBehaviorE0EEEEUldE_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation(%rip), %rcx
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-96(%rbp), %r8
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-80(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdx
	subq	$72, %rsp
	.cfi_offset 3, -40
	movq	32(%rdi), %r13
	movq	72(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$1, 240(%rdi)
	movq	0(%r13), %rax
	movq	%rdi, -80(%rbp)
	movq	%r8, %rdi
	movq	32(%rax), %rbx
	leaq	_ZNSt17_Function_handlerIFvdEZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS1_4base9LockGuardINS4_5MutexELNS4_12NullBehaviorE0EEEEUldE_E9_M_invokeERKSt9_Any_dataOd(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal22MakeCancelableIdleTaskEPNS0_21CancelableTaskManagerESt8functionIFvdEE@PLT
	movq	-96(%rbp), %rax
	movq	%r13, %rdi
	leaq	-88(%rbp), %rsi
	movq	$0, -96(%rbp)
	leaq	32(%rax), %rdx
	testq	%rax, %rax
	cmovne	%rdx, %rax
	movq	%rax, -88(%rbp)
	call	*%rbx
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L29
	movq	(%rdi), %rax
	call	*8(%rax)
.L29:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L30
	movq	(%rdi), %rax
	call	*8(%rax)
.L30:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L27
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L27:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L45
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L45:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28215:
	.size	_ZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE.part.0, .-_ZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE.part.0
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB19818:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L52
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L55
.L46:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L46
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE19818:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.text._ZN2v88internal18CompilerDispatcher3JobC2EPNS0_21BackgroundCompileTaskE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18CompilerDispatcher3JobC2EPNS0_21BackgroundCompileTaskE
	.type	_ZN2v88internal18CompilerDispatcher3JobC2EPNS0_21BackgroundCompileTaskE, @function
_ZN2v88internal18CompilerDispatcher3JobC2EPNS0_21BackgroundCompileTaskE:
.LFB21157:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movw	%ax, 16(%rdi)
	ret
	.cfi_endproc
.LFE21157:
	.size	_ZN2v88internal18CompilerDispatcher3JobC2EPNS0_21BackgroundCompileTaskE, .-_ZN2v88internal18CompilerDispatcher3JobC2EPNS0_21BackgroundCompileTaskE
	.globl	_ZN2v88internal18CompilerDispatcher3JobC1EPNS0_21BackgroundCompileTaskE
	.set	_ZN2v88internal18CompilerDispatcher3JobC1EPNS0_21BackgroundCompileTaskE,_ZN2v88internal18CompilerDispatcher3JobC2EPNS0_21BackgroundCompileTaskE
	.section	.text._ZN2v88internal18CompilerDispatcher3JobD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18CompilerDispatcher3JobD2Ev
	.type	_ZN2v88internal18CompilerDispatcher3JobD2Ev, @function
_ZN2v88internal18CompilerDispatcher3JobD2Ev:
.LFB21160:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	testq	%r12, %r12
	je	.L57
	movq	%r12, %rdi
	call	_ZN2v88internal21BackgroundCompileTaskD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21160:
	.size	_ZN2v88internal18CompilerDispatcher3JobD2Ev, .-_ZN2v88internal18CompilerDispatcher3JobD2Ev
	.globl	_ZN2v88internal18CompilerDispatcher3JobD1Ev
	.set	_ZN2v88internal18CompilerDispatcher3JobD1Ev,_ZN2v88internal18CompilerDispatcher3JobD2Ev
	.section	.rodata._ZN2v88internal18CompilerDispatcherC2EPNS0_7IsolateEPNS_8PlatformEm.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"CompilerDispatcher: dispatcher is disabled\n"
	.section	.text._ZN2v88internal18CompilerDispatcherC2EPNS0_7IsolateEPNS_8PlatformEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18CompilerDispatcherC2EPNS0_7IsolateEPNS_8PlatformEm
	.type	_ZN2v88internal18CompilerDispatcherC2EPNS0_7IsolateEPNS_8PlatformEm, @function
_ZN2v88internal18CompilerDispatcherC2EPNS0_7IsolateEPNS_8PlatformEm:
.LFB21202:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$32, %rdi
	addq	$37592, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	41136(%rsi), %rax
	movq	%rsi, -32(%rdi)
	movq	%rax, -24(%rdi)
	movq	40960(%rsi), %rax
	leaq	50888(%rax), %rdx
	addq	$5400, %rax
	movq	%rax, -8(%rdi)
	movq	0(%r13), %rax
	movq	%rdx, -16(%rdi)
	movq	%rsi, %rdx
	movq	%r13, %rsi
	call	*48(%rax)
	movzbl	_ZN2v88internal30FLAG_trace_compiler_dispatcherE(%rip), %eax
	movq	%r13, 48(%rbx)
	movl	$160, %edi
	movq	%r14, 56(%rbx)
	movb	%al, 64(%rbx)
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal21CancelableTaskManagerC1Ev@PLT
	leaq	96(%rbx), %rax
	movdqa	.LC0(%rip), %xmm0
	movq	%r13, 72(%rbx)
	movq	%rax, 112(%rbx)
	leaq	200(%rbx), %rdi
	movq	%rax, 120(%rbx)
	leaq	16+_ZTVN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEEE(%rip), %rax
	movq	%rax, 136(%rbx)
	movq	$0, 80(%rbx)
	movl	$0, 96(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 128(%rbx)
	movq	%r12, 152(%rbx)
	movb	$0, 192(%rbx)
	movups	%xmm0, 160(%rbx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 176(%rbx)
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	296(%rbx), %rax
	movb	$0, 240(%rbx)
	movss	.LC1(%rip), %xmm1
	movq	%rax, 248(%rbx)
	pxor	%xmm0, %xmm0
	leaq	352(%rbx), %rax
	leaq	368(%rbx), %rdi
	movq	%rax, 304(%rbx)
	movl	$0, 244(%rbx)
	movq	$1, 256(%rbx)
	movq	$0, 264(%rbx)
	movq	$0, 272(%rbx)
	movq	$0, 288(%rbx)
	movq	$0, 296(%rbx)
	movq	$1, 312(%rbx)
	movq	$0, 320(%rbx)
	movq	$0, 328(%rbx)
	movq	$0, 344(%rbx)
	movss	%xmm1, 280(%rbx)
	movss	%xmm1, 336(%rbx)
	movups	%xmm0, 352(%rbx)
	call	_ZN2v84base17ConditionVariableC1Ev@PLT
	xorl	%esi, %esi
	leaq	424(%rbx), %rdi
	movq	$0, 416(%rbx)
	call	_ZN2v84base9SemaphoreC1Ei@PLT
	cmpb	$0, 64(%rbx)
	je	.L60
	cmpb	$0, _ZN2v88internal24FLAG_compiler_dispatcherE(%rip)
	je	.L66
.L60:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	addq	$16, %rsp
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6PrintFEPKcz@PLT
.L65:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21202:
	.size	_ZN2v88internal18CompilerDispatcherC2EPNS0_7IsolateEPNS_8PlatformEm, .-_ZN2v88internal18CompilerDispatcherC2EPNS0_7IsolateEPNS_8PlatformEm
	.globl	_ZN2v88internal18CompilerDispatcherC1EPNS0_7IsolateEPNS_8PlatformEm
	.set	_ZN2v88internal18CompilerDispatcherC1EPNS0_7IsolateEPNS_8PlatformEm,_ZN2v88internal18CompilerDispatcherC2EPNS0_7IsolateEPNS_8PlatformEm
	.section	.text._ZNK2v88internal18CompilerDispatcher9IsEnabledEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal18CompilerDispatcher9IsEnabledEv
	.type	_ZNK2v88internal18CompilerDispatcher9IsEnabledEv, @function
_ZNK2v88internal18CompilerDispatcher9IsEnabledEv:
.LFB21225:
	.cfi_startproc
	endbr64
	movzbl	_ZN2v88internal24FLAG_compiler_dispatcherE(%rip), %eax
	ret
	.cfi_endproc
.LFE21225:
	.size	_ZNK2v88internal18CompilerDispatcher9IsEnabledEv, .-_ZNK2v88internal18CompilerDispatcher9IsEnabledEv
	.section	.text._ZNK2v88internal18CompilerDispatcher10IsEnqueuedENS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal18CompilerDispatcher10IsEnqueuedENS0_6HandleINS0_18SharedFunctionInfoEEE
	.type	_ZNK2v88internal18CompilerDispatcher10IsEnqueuedENS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZNK2v88internal18CompilerDispatcher10IsEnqueuedENS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB21226:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpq	$0, 128(%rdi)
	jne	.L81
.L68:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movq	(%rsi), %rsi
	movq	%rdi, %rbx
	leaq	96(%rdi), %r13
	leaq	136(%rdi), %rdi
	call	_ZNK2v88internal15IdentityMapBase9FindEntryEm@PLT
	testq	%rax, %rax
	je	.L68
	movq	104(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L68
	movq	(%rax), %rcx
	movq	%r13, %rax
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%rdx, %rax
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L72
.L71:
	cmpq	%rcx, 32(%rdx)
	jnb	.L82
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L71
.L72:
	xorl	%r12d, %r12d
	cmpq	%rax, %r13
	je	.L68
	cmpq	32(%rax), %rcx
	setnb	%r12b
	addq	$8, %rsp
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21226:
	.size	_ZNK2v88internal18CompilerDispatcher10IsEnqueuedENS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZNK2v88internal18CompilerDispatcher10IsEnqueuedENS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.text._ZNK2v88internal18CompilerDispatcher10IsEnqueuedEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal18CompilerDispatcher10IsEnqueuedEm
	.type	_ZNK2v88internal18CompilerDispatcher10IsEnqueuedEm, @function
_ZNK2v88internal18CompilerDispatcher10IsEnqueuedEm:
.LFB21227:
	.cfi_startproc
	endbr64
	movq	104(%rdi), %rax
	leaq	96(%rdi), %rcx
	testq	%rax, %rax
	je	.L89
	movq	%rcx, %rdx
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L86
.L85:
	cmpq	%rsi, 32(%rax)
	jnb	.L91
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L85
.L86:
	xorl	%eax, %eax
	cmpq	%rcx, %rdx
	je	.L83
	cmpq	%rsi, 32(%rdx)
	setbe	%al
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	xorl	%eax, %eax
.L83:
	ret
	.cfi_endproc
.LFE21227:
	.size	_ZNK2v88internal18CompilerDispatcher10IsEnqueuedEm, .-_ZNK2v88internal18CompilerDispatcher10IsEnqueuedEm
	.section	.rodata._ZN2v88internal18CompilerDispatcher26RegisterSharedFunctionInfoEmNS0_18SharedFunctionInfoE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"CompilerDispatcher: registering "
	.section	.rodata._ZN2v88internal18CompilerDispatcher26RegisterSharedFunctionInfoEmNS0_18SharedFunctionInfoE.str1.1,"aMS",@progbits,1
.LC4:
	.string	" with job id %zu\n"
	.section	.text._ZN2v88internal18CompilerDispatcher26RegisterSharedFunctionInfoEmNS0_18SharedFunctionInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18CompilerDispatcher26RegisterSharedFunctionInfoEmNS0_18SharedFunctionInfoE
	.type	_ZN2v88internal18CompilerDispatcher26RegisterSharedFunctionInfoEmNS0_18SharedFunctionInfoE, @function
_ZN2v88internal18CompilerDispatcher26RegisterSharedFunctionInfoEmNS0_18SharedFunctionInfoE:
.LFB21228:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	cmpb	$0, 64(%rdi)
	movq	%rdx, -56(%rbp)
	jne	.L109
.L93:
	movq	(%r12), %rax
	movq	-56(%rbp), %rsi
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	leaq	96(%r12), %rcx
	movq	%rax, %r13
	movq	104(%r12), %rax
	testq	%rax, %rax
	je	.L94
	movq	%rcx, %rdx
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L110:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L96
.L95:
	cmpq	%rbx, 32(%rax)
	jnb	.L110
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L95
.L96:
	cmpq	%rdx, %rcx
	je	.L94
	cmpq	%rbx, 32(%rdx)
	cmovbe	%rdx, %rcx
.L94:
	movq	0(%r13), %rsi
	leaq	136(%r12), %rdi
	movq	40(%rcx), %r15
	leaq	200(%r12), %r14
	call	_ZN2v88internal15IdentityMapBase8GetEntryEm@PLT
	movq	%r14, %rdi
	movq	%rbx, (%rax)
	call	_ZN2v84base5Mutex4LockEv@PLT
	cmpb	$0, 16(%r15)
	movq	%r13, 8(%r15)
	jne	.L111
.L100:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	stdout(%rip), %rsi
	leaq	-56(%rbp), %rdi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	movq	%rbx, %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L111:
	movq	32(%r12), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	testb	%al, %al
	je	.L100
	cmpb	$0, 240(%r12)
	jne	.L100
	movq	%r12, %rdi
	call	_ZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE.part.0
	jmp	.L100
	.cfi_endproc
.LFE21228:
	.size	_ZN2v88internal18CompilerDispatcher26RegisterSharedFunctionInfoEmNS0_18SharedFunctionInfoE, .-_ZN2v88internal18CompilerDispatcher26RegisterSharedFunctionInfoEmNS0_18SharedFunctionInfoE
	.section	.text._ZNK2v88internal18CompilerDispatcher9GetJobForENS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal18CompilerDispatcher9GetJobForENS0_6HandleINS0_18SharedFunctionInfoEEE
	.type	_ZNK2v88internal18CompilerDispatcher9GetJobForENS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZNK2v88internal18CompilerDispatcher9GetJobForENS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB21233:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	addq	$136, %rdi
	subq	$8, %rsp
	movq	(%rsi), %rsi
	call	_ZNK2v88internal15IdentityMapBase9FindEntryEm@PLT
	leaq	96(%rbx), %r8
	testq	%rax, %rax
	je	.L113
	movq	104(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L113
	movq	(%rax), %rax
	movq	%r8, %rcx
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L126:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L115
.L114:
	cmpq	%rax, 32(%rdx)
	jnb	.L126
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L114
.L115:
	cmpq	%rcx, %r8
	je	.L113
	cmpq	32(%rcx), %rax
	cmovnb	%rcx, %r8
.L113:
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21233:
	.size	_ZNK2v88internal18CompilerDispatcher9GetJobForENS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZNK2v88internal18CompilerDispatcher9GetJobForENS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.text._ZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE
	.type	_ZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE, @function
_ZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE:
.LFB21234:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	testb	%al, %al
	je	.L127
	cmpb	$0, 240(%r12)
	je	.L133
.L127:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE.part.0
	.cfi_endproc
.LFE21234:
	.size	_ZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE, .-_ZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE
	.section	.rodata._ZN2v88internal18CompilerDispatcher31ScheduleMoreWorkerTasksIfNeededEv.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"disabled-by-default-v8.compile"
	.align 8
.LC6:
	.string	"V8.CompilerDispatcherScheduleMoreWorkerTasksIfNeeded"
	.section	.text._ZN2v88internal18CompilerDispatcher31ScheduleMoreWorkerTasksIfNeededEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18CompilerDispatcher31ScheduleMoreWorkerTasksIfNeededEv
	.type	_ZN2v88internal18CompilerDispatcher31ScheduleMoreWorkerTasksIfNeededEv, @function
_ZN2v88internal18CompilerDispatcher31ScheduleMoreWorkerTasksIfNeededEv:
.LFB21243:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	_ZZN2v88internal18CompilerDispatcher31ScheduleMoreWorkerTasksIfNeededEvE28trace_event_unique_atomic242(%rip), %r12
	testq	%r12, %r12
	je	.L173
.L136:
	movq	$0, -112(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L174
.L138:
	leaq	200(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	cmpq	$0, 272(%rbx)
	je	.L142
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	movl	%eax, %r8d
	movl	244(%rbx), %eax
	cmpl	%eax, %r8d
	jg	.L175
.L142:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L172:
	leaq	-112(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L176
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	addl	$1, %eax
	movq	%r12, %rdi
	leaq	-80(%rbp), %r12
	movl	%eax, 244(%rbx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	48(%rbx), %r13
	movq	72(%rbx), %rsi
	movq	%r12, %rdx
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal18CompilerDispatcher31ScheduleMoreWorkerTasksIfNeededEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation(%rip), %rcx
	leaq	-128(%rbp), %rdi
	movq	0(%r13), %rax
	movq	%rcx, %xmm0
	movq	56(%rax), %r14
	leaq	_ZNSt17_Function_handlerIFvvEZN2v88internal18CompilerDispatcher31ScheduleMoreWorkerTasksIfNeededEvEUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%rbx, -80(%rbp)
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal18MakeCancelableTaskEPNS0_21CancelableTaskManagerESt8functionIFvvEE@PLT
	movq	-128(%rbp), %rax
	movq	%r13, %rdi
	leaq	-120(%rbp), %rsi
	movq	$0, -128(%rbp)
	leaq	32(%rax), %rdx
	testq	%rax, %rax
	cmovne	%rdx, %rax
	movq	%rax, -120(%rbp)
	call	*%r14
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L144
	movq	(%rdi), %rax
	call	*8(%rax)
.L144:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L145
	movq	(%rdi), %rax
	call	*8(%rax)
.L145:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L172
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L174:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L177
.L139:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L140
	movq	(%rdi), %rax
	call	*8(%rax)
.L140:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L141
	movq	(%rdi), %rax
	call	*8(%rax)
.L141:
	leaq	.LC6(%rip), %rax
	movq	%r12, -104(%rbp)
	movq	%rax, -96(%rbp)
	leaq	-104(%rbp), %rax
	movq	%r13, -88(%rbp)
	movq	%rax, -112(%rbp)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L173:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L178
.L137:
	movq	%r12, _ZZN2v88internal18CompilerDispatcher31ScheduleMoreWorkerTasksIfNeededEvE28trace_event_unique_atomic242(%rip)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L177:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC6(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L178:
	leaq	.LC5(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L137
.L176:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21243:
	.size	_ZN2v88internal18CompilerDispatcher31ScheduleMoreWorkerTasksIfNeededEv, .-_ZN2v88internal18CompilerDispatcher31ScheduleMoreWorkerTasksIfNeededEv
	.section	.text._ZN2v88internal18CompilerDispatcher9RemoveJobESt23_Rb_tree_const_iteratorISt4pairIKmSt10unique_ptrINS1_3JobESt14default_deleteIS6_EEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18CompilerDispatcher9RemoveJobESt23_Rb_tree_const_iteratorISt4pairIKmSt10unique_ptrINS1_3JobESt14default_deleteIS6_EEEE
	.type	_ZN2v88internal18CompilerDispatcher9RemoveJobESt23_Rb_tree_const_iteratorISt4pairIKmSt10unique_ptrINS1_3JobESt14default_deleteIS6_EEEE, @function
_ZN2v88internal18CompilerDispatcher9RemoveJobESt23_Rb_tree_const_iteratorISt4pairIKmSt10unique_ptrINS1_3JobESt14default_deleteIS6_EEEE:
.LFB21265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	40(%rsi), %rax
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L180
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
.L180:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%r12, %rdi
	leaq	96(%rbx), %rsi
	movq	%rax, %r13
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	40(%rax), %r14
	movq	%rax, %r12
	testq	%r14, %r14
	je	.L181
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L182
	movq	%r15, %rdi
	call	_ZN2v88internal21BackgroundCompileTaskD1Ev@PLT
	movl	$64, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L182:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L181:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 128(%rbx)
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21265:
	.size	_ZN2v88internal18CompilerDispatcher9RemoveJobESt23_Rb_tree_const_iteratorISt4pairIKmSt10unique_ptrINS1_3JobESt14default_deleteIS6_EEEE, .-_ZN2v88internal18CompilerDispatcher9RemoveJobESt23_Rb_tree_const_iteratorISt4pairIKmSt10unique_ptrINS1_3JobESt14default_deleteIS6_EEEE
	.section	.rodata._ZN2v88internal18CompilerDispatcher10DoIdleWorkEd.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"V8.CompilerDispatcherDoIdleWork"
	.align 8
.LC9:
	.string	"CompilerDispatcher: received %0.1lfms of idle time\n"
	.section	.rodata._ZN2v88internal18CompilerDispatcher10DoIdleWorkEd.str1.1,"aMS",@progbits,1
.LC10:
	.string	"(location_) != nullptr"
.LC11:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal18CompilerDispatcher10DoIdleWorkEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18CompilerDispatcher10DoIdleWorkEd
	.type	_ZN2v88internal18CompilerDispatcher10DoIdleWorkEd, @function
_ZN2v88internal18CompilerDispatcher10DoIdleWorkEd:
.LFB21248:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movsd	%xmm0, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	_ZZN2v88internal18CompilerDispatcher10DoIdleWorkEdE28trace_event_unique_atomic310(%rip), %rbx
	testq	%rbx, %rbx
	je	.L234
.L195:
	movq	$0, -96(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L235
.L197:
	leaq	200(%r14), %r13
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movb	$0, 240(%r14)
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	cmpb	$0, 64(%r14)
	je	.L201
	movq	48(%r14), %rdi
	movq	(%rdi), %rax
	call	*120(%rax)
	movsd	-104(%rbp), %xmm2
	leaq	.LC9(%rip), %rdi
	movl	$1, %eax
	subsd	%xmm0, %xmm2
	movsd	.LC8(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L201:
	leaq	96(%r14), %rbx
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L238:
	cmpq	$0, 8(%rax)
	je	.L236
.L206:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	40(%r12), %rax
	cmpb	$0, 17(%rax)
	jne	.L212
	movq	8(%rax), %rsi
	movq	(%r14), %rdx
	testq	%rsi, %rsi
	je	.L237
	movq	(%rax), %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8Compiler29FinalizeBackgroundCompileTaskEPNS0_21BackgroundCompileTaskENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_18ClearExceptionFlagE@PLT
.L212:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18CompilerDispatcher9RemoveJobESt23_Rb_tree_const_iteratorISt4pairIKmSt10unique_ptrINS1_3JobESt14default_deleteIS6_EEEE
.L209:
	movq	48(%r14), %rdi
	movq	(%rdi), %rax
	call	*120(%rax)
	movsd	-104(%rbp), %xmm1
	movq	%r13, %rdi
	comisd	%xmm0, %xmm1
	jbe	.L231
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	112(%r14), %r12
	cmpq	%rbx, %r12
	je	.L204
	.p2align 4,,10
	.p2align 3
.L207:
	movq	40(%r12), %rax
	cmpb	$0, 16(%rax)
	jne	.L238
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rbx, %rax
	jne	.L207
.L204:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L239
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore_state
	cmpb	$0, 17(%rax)
	jne	.L206
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rbx, %rax
	jne	.L207
	jmp	.L204
.L237:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L235:
	pxor	%xmm0, %xmm0
	xorl	%r12d, %r12d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L240
.L198:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L199
	movq	(%rdi), %rax
	call	*8(%rax)
.L199:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L200
	movq	(%rdi), %rax
	call	*8(%rax)
.L200:
	leaq	.LC7(%rip), %rax
	movq	%rbx, -88(%rbp)
	movq	%rax, -80(%rbp)
	leaq	-88(%rbp), %rax
	movq	%r12, -72(%rbp)
	movq	%rax, -96(%rbp)
	jmp	.L197
.L234:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L241
.L196:
	movq	%rbx, _ZZN2v88internal18CompilerDispatcher10DoIdleWorkEdE28trace_event_unique_atomic310(%rip)
	jmp	.L195
.L231:
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	32(%r14), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	testb	%al, %al
	je	.L204
	cmpb	$0, 240(%r14)
	jne	.L204
	movq	%r14, %rdi
	call	_ZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE.part.0
	jmp	.L204
.L241:
	leaq	.LC5(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L196
.L240:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC7(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r12
	addq	$64, %rsp
	jmp	.L198
.L239:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21248:
	.size	_ZN2v88internal18CompilerDispatcher10DoIdleWorkEd, .-_ZN2v88internal18CompilerDispatcher10DoIdleWorkEd
	.section	.text._ZNSt17_Function_handlerIFvdEZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS1_4base9LockGuardINS4_5MutexELNS4_12NullBehaviorE0EEEEUldE_E9_M_invokeERKSt9_Any_dataOd,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvdEZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS1_4base9LockGuardINS4_5MutexELNS4_12NullBehaviorE0EEEEUldE_E9_M_invokeERKSt9_Any_dataOd, @function
_ZNSt17_Function_handlerIFvdEZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS1_4base9LockGuardINS4_5MutexELNS4_12NullBehaviorE0EEEEUldE_E9_M_invokeERKSt9_Any_dataOd:
.LFB25256:
	.cfi_startproc
	endbr64
	movsd	(%rsi), %xmm0
	movq	(%rdi), %rdi
	jmp	_ZN2v88internal18CompilerDispatcher10DoIdleWorkEd
	.cfi_endproc
.LFE25256:
	.size	_ZNSt17_Function_handlerIFvdEZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS1_4base9LockGuardINS4_5MutexELNS4_12NullBehaviorE0EEEEUldE_E9_M_invokeERKSt9_Any_dataOd, .-_ZNSt17_Function_handlerIFvdEZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS1_4base9LockGuardINS4_5MutexELNS4_12NullBehaviorE0EEEEUldE_E9_M_invokeERKSt9_Any_dataOd
	.section	.text._ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal18CompilerDispatcher3JobESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E,"axG",@progbits,_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal18CompilerDispatcher3JobESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal18CompilerDispatcher3JobESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	.type	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal18CompilerDispatcher3JobESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E, @function
_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal18CompilerDispatcher3JobESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E:
.LFB25203:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L259
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
.L247:
	movq	24(%rbx), %rsi
	movq	%rbx, %r12
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal18CompilerDispatcher3JobESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	40(%r12), %r13
	movq	16(%rbx), %rbx
	testq	%r13, %r13
	je	.L245
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L246
	movq	%r15, %rdi
	call	_ZN2v88internal21BackgroundCompileTaskD1Ev@PLT
	movl	$64, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L246:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L245:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L247
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L259:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE25203:
	.size	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal18CompilerDispatcher3JobESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E, .-_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal18CompilerDispatcher3JobESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	.section	.rodata._ZN2v88internal18CompilerDispatcherD2Ev.str1.1,"aMS",@progbits,1
.LC12:
	.string	"task_manager_->canceled()"
	.section	.text._ZN2v88internal18CompilerDispatcherD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18CompilerDispatcherD2Ev
	.type	_ZN2v88internal18CompilerDispatcherD2Ev, @function
_ZN2v88internal18CompilerDispatcherD2Ev:
.LFB21205:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	72(%rdi), %rax
	cmpb	$0, 152(%rax)
	je	.L306
	movq	%rdi, %rbx
	leaq	424(%rdi), %rdi
	call	_ZN2v84base9SemaphoreD1Ev@PLT
	leaq	368(%rbx), %rdi
	call	_ZN2v84base17ConditionVariableD1Ev@PLT
	movq	320(%rbx), %r12
	testq	%r12, %r12
	je	.L264
	.p2align 4,,10
	.p2align 3
.L265:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L265
.L264:
	movq	312(%rbx), %rax
	movq	304(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	304(%rbx), %rdi
	leaq	352(%rbx), %rax
	movq	$0, 328(%rbx)
	movq	$0, 320(%rbx)
	cmpq	%rax, %rdi
	je	.L266
	call	_ZdlPv@PLT
.L266:
	movq	264(%rbx), %r12
	testq	%r12, %r12
	je	.L267
	.p2align 4,,10
	.p2align 3
.L268:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L268
.L267:
	movq	256(%rbx), %rax
	movq	248(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	248(%rbx), %rdi
	leaq	296(%rbx), %rax
	movq	$0, 272(%rbx)
	movq	$0, 264(%rbx)
	cmpq	%rax, %rdi
	je	.L269
	call	_ZdlPv@PLT
.L269:
	leaq	200(%rbx), %rdi
	leaq	136(%rbx), %r12
	call	_ZN2v84base5MutexD1Ev@PLT
	leaq	16+_ZTVN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEEE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, 136(%rbx)
	call	_ZN2v88internal15IdentityMapBase5ClearEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal15IdentityMapBaseD2Ev@PLT
	movq	104(%rbx), %r12
	leaq	88(%rbx), %rax
	movq	%rax, -56(%rbp)
	testq	%r12, %r12
	je	.L270
.L273:
	movq	24(%r12), %rsi
	movq	-56(%rbp), %rdi
	movq	%r12, %r15
	call	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal18CompilerDispatcher3JobESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	40(%r15), %r13
	movq	16(%r12), %r12
	testq	%r13, %r13
	je	.L271
	movq	0(%r13), %r14
	testq	%r14, %r14
	je	.L272
	movq	%r14, %rdi
	call	_ZN2v88internal21BackgroundCompileTaskD1Ev@PLT
	movl	$64, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L272:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L271:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L273
.L270:
	movq	72(%rbx), %r12
	testq	%r12, %r12
	je	.L274
	movq	%r12, %rdi
	call	_ZN2v88internal21CancelableTaskManagerD1Ev@PLT
	movl	$160, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L274:
	movq	40(%rbx), %r12
	testq	%r12, %r12
	je	.L262
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L277
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L307
.L262:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L277:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L262
.L307:
	movq	(%r12), %rax
	leaq	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L308
.L280:
	testq	%rbx, %rbx
	je	.L281
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L282:
	cmpl	$1, %eax
	jne	.L262
	movq	(%r12), %rax
	leaq	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv(%rip), %rcx
	movq	24(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L283
	movq	8(%rax), %rax
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	leaq	.LC12(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L281:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L283:
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L280
	.cfi_endproc
.LFE21205:
	.size	_ZN2v88internal18CompilerDispatcherD2Ev, .-_ZN2v88internal18CompilerDispatcherD2Ev
	.globl	_ZN2v88internal18CompilerDispatcherD1Ev
	.set	_ZN2v88internal18CompilerDispatcherD1Ev,_ZN2v88internal18CompilerDispatcherD2Ev
	.section	.text._ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal18CompilerDispatcher3JobESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE17_M_emplace_uniqueIJS0_ImS9_EEEES0_ISt17_Rb_tree_iteratorISA_EbEDpOT_,"axG",@progbits,_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal18CompilerDispatcher3JobESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE17_M_emplace_uniqueIJS0_ImS9_EEEES0_ISt17_Rb_tree_iteratorISA_EbEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal18CompilerDispatcher3JobESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE17_M_emplace_uniqueIJS0_ImS9_EEEES0_ISt17_Rb_tree_iteratorISA_EbEDpOT_
	.type	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal18CompilerDispatcher3JobESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE17_M_emplace_uniqueIJS0_ImS9_EEEES0_ISt17_Rb_tree_iteratorISA_EbEDpOT_, @function
_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal18CompilerDispatcher3JobESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE17_M_emplace_uniqueIJS0_ImS9_EEEES0_ISt17_Rb_tree_iteratorISA_EbEDpOT_:
.LFB25327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$48, %edi
	subq	$24, %rsp
	call	_Znwm@PLT
	movq	8(%r14), %r8
	movq	(%r14), %r15
	movq	$0, 8(%r14)
	movq	16(%rbx), %r12
	movq	%rax, %r13
	leaq	8(%rbx), %r14
	movq	%r15, 32(%rax)
	movq	%r8, 40(%rax)
	testq	%r12, %r12
	jne	.L311
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L340:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L312
.L341:
	movq	%rax, %r12
.L311:
	movq	32(%r12), %rcx
	cmpq	%rcx, %r15
	jb	.L340
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L341
.L312:
	testb	%dl, %dl
	jne	.L342
	cmpq	%rcx, %r15
	jbe	.L317
.L322:
	movl	$1, %edi
	cmpq	%r12, %r14
	jne	.L343
.L318:
	movq	%r12, %rdx
	movq	%r14, %rcx
	movq	%r13, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L342:
	.cfi_restore_state
	cmpq	24(%rbx), %r12
	je	.L322
.L323:
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %r8
	cmpq	%r15, 32(%rax)
	jb	.L344
	movq	%rax, %r12
.L317:
	testq	%r8, %r8
	je	.L320
	movq	(%r8), %r14
	testq	%r14, %r14
	je	.L321
	movq	%r14, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal21BackgroundCompileTaskD1Ev@PLT
	movl	$64, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	movq	-56(%rbp), %r8
.L321:
	movl	$24, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L320:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L344:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L317
	movl	$1, %edi
	cmpq	%r12, %r14
	je	.L318
.L343:
	xorl	%edi, %edi
	cmpq	32(%r12), %r15
	setb	%dil
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L339:
	movq	%r14, %r12
	cmpq	%r14, 24(%rbx)
	jne	.L323
	movl	$1, %edi
	jmp	.L318
	.cfi_endproc
.LFE25327:
	.size	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal18CompilerDispatcher3JobESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE17_M_emplace_uniqueIJS0_ImS9_EEEES0_ISt17_Rb_tree_iteratorISA_EbEDpOT_, .-_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal18CompilerDispatcher3JobESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE17_M_emplace_uniqueIJS0_ImS9_EEEES0_ISt17_Rb_tree_iteratorISA_EbEDpOT_
	.section	.text._ZN2v88internal18CompilerDispatcher9InsertJobESt10unique_ptrINS1_3JobESt14default_deleteIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18CompilerDispatcher9InsertJobESt10unique_ptrINS1_3JobESt14default_deleteIS3_EE
	.type	_ZN2v88internal18CompilerDispatcher9InsertJobESt10unique_ptrINS1_3JobESt14default_deleteIS3_EE, @function
_ZN2v88internal18CompilerDispatcher9InsertJobESt10unique_ptrINS1_3JobESt14default_deleteIS3_EE:
.LFB21249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$88, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	-8(%rdi), %rax
	leaq	1(%rax), %rdx
	movq	%rax, -64(%rbp)
	movq	%rdx, -8(%rdi)
	movq	(%rsi), %rax
	movq	$0, (%rsi)
	leaq	-64(%rbp), %rsi
	movq	%rax, -56(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal18CompilerDispatcher3JobESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE17_M_emplace_uniqueIJS0_ImS9_EEEES0_ISt17_Rb_tree_iteratorISA_EbEDpOT_
	movq	-56(%rbp), %r13
	movq	%rax, %r12
	testq	%r13, %r13
	je	.L346
	movq	0(%r13), %r14
	testq	%r14, %r14
	je	.L347
	movq	%r14, %rdi
	call	_ZN2v88internal21BackgroundCompileTaskD1Ev@PLT
	movl	$64, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L347:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L346:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L356
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L356:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21249:
	.size	_ZN2v88internal18CompilerDispatcher9InsertJobESt10unique_ptrINS1_3JobESt14default_deleteIS3_EE, .-_ZN2v88internal18CompilerDispatcher9InsertJobESt10unique_ptrINS1_3JobESt14default_deleteIS3_EE
	.section	.text._ZNSt10_HashtableIPN2v88internal18CompilerDispatcher3JobES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal18CompilerDispatcher3JobES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal18CompilerDispatcher3JobES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm
	.type	_ZNSt10_HashtableIPN2v88internal18CompilerDispatcher3JobES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm, @function
_ZNSt10_HashtableIPN2v88internal18CompilerDispatcher3JobES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm:
.LFB26892:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L358
	movq	(%rbx), %r8
.L359:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L368
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L369:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L382
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L383
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L361:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L363
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L365:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L366:
	testq	%rsi, %rsi
	je	.L363
.L364:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L365
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L371
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L364
	.p2align 4,,10
	.p2align 3
.L363:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L367
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L367:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L368:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L370
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L370:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L371:
	movq	%rdx, %rdi
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L382:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L361
.L383:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE26892:
	.size	_ZNSt10_HashtableIPN2v88internal18CompilerDispatcher3JobES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm, .-_ZNSt10_HashtableIPN2v88internal18CompilerDispatcher3JobES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm
	.section	.rodata._ZN2v88internal18CompilerDispatcher7EnqueueEPKNS0_9ParseInfoEPKNS0_12AstRawStringEPKNS0_15FunctionLiteralE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"V8.CompilerDispatcherEnqueue"
	.section	.rodata._ZN2v88internal18CompilerDispatcher7EnqueueEPKNS0_9ParseInfoEPKNS0_12AstRawStringEPKNS0_15FunctionLiteralE.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"CompilerDispatcher: enqueued job %zu for function literal id %d\n"
	.section	.text._ZN2v88internal18CompilerDispatcher7EnqueueEPKNS0_9ParseInfoEPKNS0_12AstRawStringEPKNS0_15FunctionLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18CompilerDispatcher7EnqueueEPKNS0_9ParseInfoEPKNS0_12AstRawStringEPKNS0_15FunctionLiteralE
	.type	_ZN2v88internal18CompilerDispatcher7EnqueueEPKNS0_9ParseInfoEPKNS0_12AstRawStringEPKNS0_15FunctionLiteralE, @function
_ZN2v88internal18CompilerDispatcher7EnqueueEPKNS0_9ParseInfoEPKNS0_12AstRawStringEPKNS0_15FunctionLiteralE:
.LFB21207:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	_ZZN2v88internal18CompilerDispatcher7EnqueueEPKNS0_9ParseInfoEPKNS0_12AstRawStringEPKNS0_15FunctionLiteralEE27trace_event_unique_atomic62(%rip), %rdx
	movq	%rdx, %r15
	testq	%rdx, %rdx
	je	.L434
.L386:
	movq	$0, -160(%rbp)
	movzbl	(%r15), %eax
	testb	$5, %al
	jne	.L435
.L388:
	pxor	%xmm0, %xmm0
	movq	$0, -96(%rbp)
	movq	(%r12), %rdx
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L436
.L392:
	cmpb	$0, _ZN2v88internal24FLAG_compiler_dispatcherE(%rip)
	jne	.L393
	movq	-128(%rbp), %rdi
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	testq	%rdi, %rdi
	jne	.L437
.L401:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L438
	leaq	-40(%rbp), %rsp
	movl	%ebx, %eax
	movq	%r13, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L434:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r15
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L439
.L387:
	movq	%r15, _ZZN2v88internal18CompilerDispatcher7EnqueueEPKNS0_9ParseInfoEPKNS0_12AstRawStringEPKNS0_15FunctionLiteralEE27trace_event_unique_atomic62(%rip)
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L393:
	movl	$64, %edi
	call	_Znwm@PLT
	movq	8(%r12), %rsi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%rax, %r15
	movq	56(%r12), %rax
	movq	16(%r12), %r9
	movq	%rbx, %r8
	movq	%r15, %rdi
	pushq	%rax
	pushq	24(%r12)
	call	_ZN2v88internal21BackgroundCompileTaskC1EPNS0_19AccountingAllocatorEPKNS0_9ParseInfoEPKNS0_12AstRawStringEPKNS0_15FunctionLiteralEPNS0_28WorkerThreadRuntimeCallStatsEPNS0_14TimedHistogramEi@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	leaq	88(%r12), %rdi
	leaq	-176(%rbp), %rsi
	movw	%dx, 16(%rax)
	movq	80(%r12), %rdx
	movq	%r15, (%rax)
	leaq	1(%rdx), %rcx
	movq	$0, 8(%rax)
	movq	%rcx, 80(%r12)
	movq	%rdx, -176(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal18CompilerDispatcher3JobESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE17_M_emplace_uniqueIJS0_ImS9_EEEES0_ISt17_Rb_tree_iteratorISA_EbEDpOT_
	movq	-168(%rbp), %r13
	popq	%rcx
	movq	%rax, %r14
	popq	%rsi
	testq	%r13, %r13
	je	.L395
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L396
	movq	%r15, %rdi
	call	_ZN2v88internal21BackgroundCompileTaskD1Ev@PLT
	movl	$64, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L396:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L395:
	cmpb	$0, 64(%r12)
	movq	32(%r14), %r13
	jne	.L440
.L397:
	leaq	200(%r12), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	40(%r14), %r14
	movq	256(%r12), %rdi
	xorl	%edx, %edx
	movq	%r14, %rax
	divq	%rdi
	movq	248(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rbx
	testq	%rax, %rax
	je	.L398
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L441:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L398
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %rbx
	jne	.L398
.L400:
	cmpq	%rsi, %r14
	jne	.L441
.L399:
	movq	%r15, %rdi
	movl	$1, %ebx
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal18CompilerDispatcher31ScheduleMoreWorkerTasksIfNeededEv
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L401
.L437:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L435:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	$0, -184(%rbp)
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L442
.L389:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L390
	movq	(%rdi), %rax
	call	*8(%rax)
.L390:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L391
	movq	(%rdi), %rax
	call	*8(%rax)
.L391:
	leaq	.LC13(%rip), %rax
	movq	%r15, -152(%rbp)
	movq	%rax, -144(%rbp)
	movq	-184(%rbp), %rax
	movq	%rax, -136(%rbp)
	leaq	-152(%rbp), %rax
	movq	%rax, -160(%rbp)
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L440:
	movl	28(%rbx), %edx
	movq	%r13, %rsi
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L436:
	movq	40960(%rdx), %rdi
	leaq	-120(%rbp), %rsi
	movl	$127, %edx
	addq	$23240, %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L442:
	subq	$8, %rsp
	leaq	-80(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r15, %rdx
	movl	$88, %esi
	pushq	%rcx
	leaq	.LC13(%rip), %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, -184(%rbp)
	addq	$64, %rsp
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L439:
	leaq	.LC5(%rip), %rsi
	call	*%rax
	movq	%rax, %r15
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L398:
	movl	$16, %edi
	call	_Znwm@PLT
	leaq	248(%r12), %rdi
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r14, 8(%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	$0, (%rax)
	call	_ZNSt10_HashtableIPN2v88internal18CompilerDispatcher3JobES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm
	jmp	.L399
.L438:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21207:
	.size	_ZN2v88internal18CompilerDispatcher7EnqueueEPKNS0_9ParseInfoEPKNS0_12AstRawStringEPKNS0_15FunctionLiteralE, .-_ZN2v88internal18CompilerDispatcher7EnqueueEPKNS0_9ParseInfoEPKNS0_12AstRawStringEPKNS0_15FunctionLiteralE
	.section	.text._ZNSt10_HashtableIPN2v88internal18CompilerDispatcher3JobES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS6_15_Hash_node_baseEPNS6_10_Hash_nodeIS4_Lb0EEE,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal18CompilerDispatcher3JobES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS6_15_Hash_node_baseEPNS6_10_Hash_nodeIS4_Lb0EEE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal18CompilerDispatcher3JobES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS6_15_Hash_node_baseEPNS6_10_Hash_nodeIS4_Lb0EEE
	.type	_ZNSt10_HashtableIPN2v88internal18CompilerDispatcher3JobES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS6_15_Hash_node_baseEPNS6_10_Hash_nodeIS4_Lb0EEE, @function
_ZNSt10_HashtableIPN2v88internal18CompilerDispatcher3JobES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS6_15_Hash_node_baseEPNS6_10_Hash_nodeIS4_Lb0EEE:
.LFB26906:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	0(,%rsi,8), %r9
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rcx, %rdi
	movq	(%rbx), %rcx
	movq	(%rdi), %r12
	leaq	(%rcx,%r9), %rax
	cmpq	%rdx, (%rax)
	je	.L453
	testq	%r12, %r12
	je	.L446
	movq	8(%r12), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	cmpq	%rdx, %rsi
	je	.L446
	movq	%r8, (%rcx,%rdx,8)
	movq	(%rdi), %r12
.L446:
	movq	%r12, (%r8)
	call	_ZdlPv@PLT
	movq	%r12, %rax
	subq	$1, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L453:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L445
	movq	8(%r12), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	cmpq	%rdx, %rsi
	je	.L446
	movq	%r8, (%rcx,%rdx,8)
	movq	(%rbx), %rax
	addq	%r9, %rax
	movq	(%rax), %rdx
.L445:
	leaq	16(%rbx), %rcx
	cmpq	%rcx, %rdx
	je	.L454
.L447:
	movq	$0, (%rax)
	movq	(%rdi), %r12
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L454:
	movq	%r12, 16(%rbx)
	jmp	.L447
	.cfi_endproc
.LFE26906:
	.size	_ZNSt10_HashtableIPN2v88internal18CompilerDispatcher3JobES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS6_15_Hash_node_baseEPNS6_10_Hash_nodeIS4_Lb0EEE, .-_ZNSt10_HashtableIPN2v88internal18CompilerDispatcher3JobES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS6_15_Hash_node_baseEPNS6_10_Hash_nodeIS4_Lb0EEE
	.section	.rodata._ZN2v88internal18CompilerDispatcher31WaitForJobIfRunningOnBackgroundEPNS1_3JobE.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"V8.CompilerDispatcherWaitForBackgroundJob"
	.section	.text._ZN2v88internal18CompilerDispatcher31WaitForJobIfRunningOnBackgroundEPNS1_3JobE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18CompilerDispatcher31WaitForJobIfRunningOnBackgroundEPNS1_3JobE
	.type	_ZN2v88internal18CompilerDispatcher31WaitForJobIfRunningOnBackgroundEPNS1_3JobE, @function
_ZN2v88internal18CompilerDispatcher31WaitForJobIfRunningOnBackgroundEPNS1_3JobE:
.LFB21229:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	_ZZN2v88internal18CompilerDispatcher31WaitForJobIfRunningOnBackgroundEPNS1_3JobEE28trace_event_unique_atomic132(%rip), %r13
	testq	%r13, %r13
	je	.L513
.L457:
	movq	$0, -144(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L514
.L459:
	pxor	%xmm0, %xmm0
	movq	$0, -80(%rbp)
	movq	(%rbx), %rdx
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L515
.L463:
	leaq	200(%rbx), %r13
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	312(%rbx), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	304(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L464
	movq	(%rax), %rsi
	movq	8(%rsi), %rcx
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L516:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L464
	movq	8(%rsi), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rdi
	cmpq	%rdx, %r8
	jne	.L464
.L466:
	cmpq	%r12, %rcx
	jne	.L516
	movq	%rcx, 360(%rbx)
	leaq	368(%rbx), %r12
	testq	%rcx, %rcx
	je	.L469
	.p2align 4,,10
	.p2align 3
.L475:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v84base17ConditionVariable4WaitEPNS0_5MutexE@PLT
	cmpq	$0, 360(%rbx)
	jne	.L475
.L469:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L517
.L473:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L518
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L513:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L519
.L458:
	movq	%r13, _ZZN2v88internal18CompilerDispatcher31WaitForJobIfRunningOnBackgroundEPNS1_3JobEE28trace_event_unique_atomic132(%rip)
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L514:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L520
.L460:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L461
	movq	(%rdi), %rax
	call	*8(%rax)
.L461:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L462
	movq	(%rdi), %rax
	call	*8(%rax)
.L462:
	leaq	.LC15(%rip), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L515:
	movq	40960(%rdx), %rdi
	leaq	-104(%rbp), %rsi
	movl	$139, %edx
	addq	$23240, %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L517:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L464:
	movq	256(%rbx), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	248(%rbx), %rax
	movq	(%rax,%rdx,8), %r9
	movq	%rdx, %r10
	testq	%r9, %r9
	je	.L469
	movq	(%r9), %r8
	movq	8(%r8), %rcx
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L521:
	movq	(%r8), %rsi
	testq	%rsi, %rsi
	je	.L469
	movq	8(%rsi), %rcx
	xorl	%edx, %edx
	movq	%r8, %r9
	movq	%rcx, %rax
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L469
	movq	%rsi, %r8
.L471:
	cmpq	%rcx, %r12
	jne	.L521
	leaq	248(%rbx), %rdi
	movq	%r8, %rcx
	movq	%r9, %rdx
	movq	%r10, %rsi
	call	_ZNSt10_HashtableIPN2v88internal18CompilerDispatcher3JobES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS6_15_Hash_node_baseEPNS6_10_Hash_nodeIS4_Lb0EEE
	jmp	.L469
.L520:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC15(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L460
.L519:
	leaq	.LC5(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L458
.L518:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21229:
	.size	_ZN2v88internal18CompilerDispatcher31WaitForJobIfRunningOnBackgroundEPNS1_3JobE, .-_ZN2v88internal18CompilerDispatcher31WaitForJobIfRunningOnBackgroundEPNS1_3JobE
	.section	.rodata._ZN2v88internal18CompilerDispatcher9FinishNowENS0_6HandleINS0_18SharedFunctionInfoEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"V8.CompilerDispatcherFinishNow"
	.align 8
.LC17:
	.string	"CompilerDispatcher: finishing "
	.section	.rodata._ZN2v88internal18CompilerDispatcher9FinishNowENS0_6HandleINS0_18SharedFunctionInfoEEE.str1.1,"aMS",@progbits,1
.LC18:
	.string	" now\n"
.LC19:
	.string	"it != jobs_.end()"
	.section	.text._ZN2v88internal18CompilerDispatcher9FinishNowENS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18CompilerDispatcher9FinishNowENS0_6HandleINS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal18CompilerDispatcher9FinishNowENS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal18CompilerDispatcher9FinishNowENS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB21230:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	_ZZN2v88internal18CompilerDispatcher9FinishNowENS0_6HandleINS0_18SharedFunctionInfoEEEE28trace_event_unique_atomic152(%rip), %rbx
	testq	%rbx, %rbx
	je	.L566
.L524:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L567
.L526:
	pxor	%xmm0, %xmm0
	movq	$0, -80(%rbp)
	movq	(%r12), %rdx
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L568
.L530:
	cmpb	$0, 64(%r12)
	jne	.L569
.L531:
	movq	0(%r13), %rsi
	leaq	136(%r12), %rdi
	call	_ZNK2v88internal15IdentityMapBase9FindEntryEm@PLT
	testq	%rax, %rax
	je	.L532
	movq	104(%r12), %rdx
	testq	%rdx, %rdx
	je	.L532
	leaq	96(%r12), %rcx
	movq	(%rax), %rax
	movq	%rcx, %r14
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L570:
	movq	%rdx, %r14
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L534
.L533:
	cmpq	%rax, 32(%rdx)
	jnb	.L570
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L533
.L534:
	cmpq	%r14, %rcx
	je	.L532
	cmpq	32(%r14), %rax
	jnb	.L571
.L532:
	leaq	.LC19(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L571:
	movq	40(%r14), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal18CompilerDispatcher31WaitForJobIfRunningOnBackgroundEPNS1_3JobE
	cmpb	$0, 16(%rbx)
	je	.L572
.L537:
	movq	(%r12), %rdx
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	call	_ZN2v88internal8Compiler29FinalizeBackgroundCompileTaskEPNS0_21BackgroundCompileTaskENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_18ClearExceptionFlagE@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movl	%eax, %r13d
	call	_ZN2v88internal18CompilerDispatcher9RemoveJobESt23_Rb_tree_const_iteratorISt4pairIKmSt10unique_ptrINS1_3JobESt14default_deleteIS6_EEEE
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L573
.L539:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L574
	leaq	-32(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L569:
	.cfi_restore_state
	xorl	%eax, %eax
	leaq	.LC17(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	0(%r13), %rax
	movq	stdout(%rip), %rsi
	leaq	-152(%rbp), %rdi
	movq	%rax, -152(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	leaq	.LC18(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L567:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L575
.L527:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L528
	movq	(%rdi), %rax
	call	*8(%rax)
.L528:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L529
	movq	(%rdi), %rax
	call	*8(%rax)
.L529:
	leaq	.LC16(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L566:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L576
.L525:
	movq	%rbx, _ZZN2v88internal18CompilerDispatcher9FinishNowENS0_6HandleINS0_18SharedFunctionInfoEEEE28trace_event_unique_atomic152(%rip)
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L572:
	movq	(%rbx), %rdi
	call	_ZN2v88internal21BackgroundCompileTask3RunEv@PLT
	movb	$1, 16(%rbx)
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L568:
	movq	40960(%rdx), %rdi
	leaq	-104(%rbp), %rsi
	movl	$130, %edx
	addq	$23240, %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L573:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L576:
	leaq	.LC5(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L575:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC16(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L527
.L574:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21230:
	.size	_ZN2v88internal18CompilerDispatcher9FinishNowENS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZN2v88internal18CompilerDispatcher9FinishNowENS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.rodata._ZN2v88internal18CompilerDispatcher8AbortAllEv.str1.8,"aMS",@progbits,1
	.align 8
.LC20:
	.string	"CompilerDispatcher: aborted job %zu\n"
	.section	.text._ZN2v88internal18CompilerDispatcher8AbortAllEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18CompilerDispatcher8AbortAllEv
	.type	_ZN2v88internal18CompilerDispatcher8AbortAllEv, @function
_ZN2v88internal18CompilerDispatcher8AbortAllEv:
.LFB21232:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	96(%rbx), %r13
	subq	$24, %rsp
	movq	72(%rdi), %rdi
	call	_ZN2v88internal21CancelableTaskManager11TryAbortAllEv@PLT
	movq	112(%rbx), %r12
	cmpq	%r12, %r13
	je	.L578
	leaq	.LC20(%rip), %r14
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L579:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r13
	je	.L578
.L581:
	movq	40(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal18CompilerDispatcher31WaitForJobIfRunningOnBackgroundEPNS1_3JobE
	cmpb	$0, 64(%rbx)
	je	.L579
	movq	32(%r12), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r13
	jne	.L581
.L578:
	movq	104(%rbx), %r12
	leaq	88(%rbx), %r15
	testq	%r12, %r12
	je	.L582
.L585:
	movq	24(%r12), %rsi
	movq	%r12, %r14
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal18CompilerDispatcher3JobESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	40(%r14), %r8
	movq	16(%r12), %r12
	testq	%r8, %r8
	je	.L583
	movq	(%r8), %rdi
	testq	%rdi, %rdi
	je	.L584
	movq	%r8, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal21BackgroundCompileTaskD1Ev@PLT
	movq	-56(%rbp), %rdi
	movl	$64, %esi
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %r8
.L584:
	movl	$24, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L583:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L585
.L582:
	movq	%r13, 112(%rbx)
	leaq	200(%rbx), %r12
	leaq	136(%rbx), %rdi
	movq	%r13, 120(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 128(%rbx)
	call	_ZN2v88internal15IdentityMapBase5ClearEv@PLT
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	72(%rbx), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21CancelableTaskManager13CancelAndWaitEv@PLT
	.cfi_endproc
.LFE21232:
	.size	_ZN2v88internal18CompilerDispatcher8AbortAllEv, .-_ZN2v88internal18CompilerDispatcher8AbortAllEv
	.section	.rodata._ZN2v88internal18CompilerDispatcher16DoBackgroundWorkEv.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"V8.CompilerDispatcherDoBackgroundWork"
	.align 8
.LC22:
	.string	"CompilerDispatcher: doing background work\n"
	.section	.text._ZN2v88internal18CompilerDispatcher16DoBackgroundWorkEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18CompilerDispatcher16DoBackgroundWorkEv
	.type	_ZN2v88internal18CompilerDispatcher16DoBackgroundWorkEv, @function
_ZN2v88internal18CompilerDispatcher16DoBackgroundWorkEv:
.LFB21247:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	_ZZN2v88internal18CompilerDispatcher16DoBackgroundWorkEvE28trace_event_unique_atomic257(%rip), %r12
	testq	%r12, %r12
	je	.L660
.L599:
	movq	$0, -112(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L661
.L601:
	leaq	200(%rbx), %r13
	leaq	248(%rbx), %r15
	.p2align 4,,10
	.p2align 3
.L620:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	cmpq	$0, 272(%rbx)
	jne	.L605
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L606:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	subl	$1, 244(%rbx)
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	leaq	-112(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L662
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L605:
	.cfi_restore_state
	movq	264(%rbx), %rcx
	xorl	%edx, %edx
	movq	8(%rcx), %r12
	movq	%r12, %rax
	divq	256(%rbx)
	movq	248(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L607:
	movq	%rax, %rdx
	movq	(%rax), %rax
	cmpq	%rax, %rcx
	jne	.L607
	movq	%r15, %rdi
	leaq	304(%rbx), %r14
	call	_ZNSt10_HashtableIPN2v88internal18CompilerDispatcher3JobES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS6_15_Hash_node_baseEPNS6_10_Hash_nodeIS4_Lb0EEE
	movq	312(%rbx), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	304(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L608
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L663:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L608
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L608
.L610:
	cmpq	%rsi, %r12
	jne	.L663
.L609:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	testq	%r12, %r12
	je	.L606
	movq	416(%rbx), %rax
	testq	%rax, %rax
	jne	.L664
.L611:
	cmpb	$0, 64(%rbx)
	jne	.L665
.L612:
	movq	(%r12), %rdi
	call	_ZN2v88internal21BackgroundCompileTask3RunEv@PLT
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	312(%rbx), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	304(%rbx), %rax
	movq	(%rax,%rdx,8), %r8
	movq	%rdx, %r10
	testq	%r8, %r8
	je	.L613
	movq	(%r8), %rcx
	movq	8(%rcx), %rsi
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L666:
	movq	(%rcx), %r9
	testq	%r9, %r9
	je	.L613
	movq	8(%r9), %rsi
	xorl	%edx, %edx
	movq	%rcx, %r8
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L613
	movq	%r9, %rcx
.L615:
	cmpq	%rsi, %r12
	jne	.L666
	movq	%r8, %rdx
	movq	%r10, %rsi
	movq	%r14, %rdi
	call	_ZNSt10_HashtableIPN2v88internal18CompilerDispatcher3JobES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS6_15_Hash_node_baseEPNS6_10_Hash_nodeIS4_Lb0EEE
.L613:
	cmpq	$0, 8(%r12)
	movb	$1, 16(%r12)
	je	.L667
.L616:
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	testb	%al, %al
	je	.L617
	cmpb	$0, 240(%rbx)
	jne	.L617
	movq	%rbx, %rdi
	call	_ZN2v88internal18CompilerDispatcher29ScheduleIdleTaskFromAnyThreadERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE.part.0
.L617:
	cmpq	360(%rbx), %r12
	je	.L668
.L619:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L665:
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L667:
	cmpb	$0, 17(%r12)
	jne	.L616
	cmpq	360(%rbx), %r12
	jne	.L619
	.p2align 4,,10
	.p2align 3
.L668:
	movq	$0, 360(%rbx)
	leaq	368(%rbx), %rdi
	call	_ZN2v84base17ConditionVariable9NotifyOneEv@PLT
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L664:
	leaq	424(%rbx), %rdi
	movq	$0, 416(%rbx)
	call	_ZN2v84base9Semaphore4WaitEv@PLT
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L608:
	movl	$16, %edi
	movq	%r9, -120(%rbp)
	call	_Znwm@PLT
	movq	-120(%rbp), %r9
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r12, 8(%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	$0, (%rax)
	movq	%r9, %rsi
	call	_ZNSt10_HashtableIPN2v88internal18CompilerDispatcher3JobES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L660:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L669
.L600:
	movq	%r12, _ZZN2v88internal18CompilerDispatcher16DoBackgroundWorkEvE28trace_event_unique_atomic257(%rip)
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L661:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L670
.L602:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L603
	movq	(%rdi), %rax
	call	*8(%rax)
.L603:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L604
	movq	(%rdi), %rax
	call	*8(%rax)
.L604:
	leaq	.LC21(%rip), %rax
	movq	%r12, -104(%rbp)
	movq	%rax, -96(%rbp)
	leaq	-104(%rbp), %rax
	movq	%r13, -88(%rbp)
	movq	%rax, -112(%rbp)
	jmp	.L601
.L669:
	leaq	.LC5(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L600
.L670:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC21(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L602
.L662:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21247:
	.size	_ZN2v88internal18CompilerDispatcher16DoBackgroundWorkEv, .-_ZN2v88internal18CompilerDispatcher16DoBackgroundWorkEv
	.section	.text._ZNSt17_Function_handlerIFvvEZN2v88internal18CompilerDispatcher31ScheduleMoreWorkerTasksIfNeededEvEUlvE_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvvEZN2v88internal18CompilerDispatcher31ScheduleMoreWorkerTasksIfNeededEvEUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEZN2v88internal18CompilerDispatcher31ScheduleMoreWorkerTasksIfNeededEvEUlvE_E9_M_invokeERKSt9_Any_data:
.LFB25291:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	jmp	_ZN2v88internal18CompilerDispatcher16DoBackgroundWorkEv
	.cfi_endproc
.LFE25291:
	.size	_ZNSt17_Function_handlerIFvvEZN2v88internal18CompilerDispatcher31ScheduleMoreWorkerTasksIfNeededEvEUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEZN2v88internal18CompilerDispatcher31ScheduleMoreWorkerTasksIfNeededEvEUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZN2v88internal18CompilerDispatcher8AbortJobEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18CompilerDispatcher8AbortJobEm
	.type	_ZN2v88internal18CompilerDispatcher8AbortJobEm, @function
_ZN2v88internal18CompilerDispatcher8AbortJobEm:
.LFB21231:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 64(%rdi)
	movq	%rsi, %rbx
	jne	.L705
.L673:
	movq	104(%r12), %rax
	leaq	96(%r12), %r13
	testq	%rax, %rax
	je	.L674
	movq	%r13, %rdx
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L706:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L676
.L675:
	cmpq	%rbx, 32(%rax)
	jnb	.L706
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L675
.L676:
	cmpq	%rdx, %r13
	je	.L674
	cmpq	%rbx, 32(%rdx)
	cmovbe	%rdx, %r13
.L674:
	leaq	200(%r12), %r14
	movq	40(%r13), %rbx
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	256(%r12), %rdi
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	248(%r12), %rax
	movq	(%rax,%rdx,8), %r9
	movq	%rdx, %r10
	testq	%r9, %r9
	je	.L679
	movq	(%r9), %r8
	movq	8(%r8), %rcx
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L707:
	movq	(%r8), %rsi
	testq	%rsi, %rsi
	je	.L679
	movq	8(%rsi), %rcx
	xorl	%edx, %edx
	movq	%r8, %r9
	movq	%rcx, %rax
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L679
	movq	%rsi, %r8
.L681:
	cmpq	%rcx, %rbx
	jne	.L707
	leaq	248(%r12), %rdi
	movq	%r8, %rcx
	movq	%r9, %rdx
	movq	%r10, %rsi
	call	_ZNSt10_HashtableIPN2v88internal18CompilerDispatcher3JobES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS6_15_Hash_node_baseEPNS6_10_Hash_nodeIS4_Lb0EEE
.L679:
	movq	312(%r12), %rdi
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	304(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L682
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L708:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L682
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r8
	jne	.L682
.L684:
	cmpq	%rsi, %rbx
	jne	.L708
	movb	$1, 17(%rbx)
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L705:
	.cfi_restore_state
	leaq	.LC20(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L682:
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal18CompilerDispatcher9RemoveJobESt23_Rb_tree_const_iteratorISt4pairIKmSt10unique_ptrINS1_3JobESt14default_deleteIS6_EEEE
	popq	%rbx
	movq	%r14, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE21231:
	.size	_ZN2v88internal18CompilerDispatcher8AbortJobEm, .-_ZN2v88internal18CompilerDispatcher8AbortJobEm
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal18CompilerDispatcher3JobC2EPNS0_21BackgroundCompileTaskE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal18CompilerDispatcher3JobC2EPNS0_21BackgroundCompileTaskE, @function
_GLOBAL__sub_I__ZN2v88internal18CompilerDispatcher3JobC2EPNS0_21BackgroundCompileTaskE:
.LFB28139:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28139:
	.size	_GLOBAL__sub_I__ZN2v88internal18CompilerDispatcher3JobC2EPNS0_21BackgroundCompileTaskE, .-_GLOBAL__sub_I__ZN2v88internal18CompilerDispatcher3JobC2EPNS0_21BackgroundCompileTaskE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal18CompilerDispatcher3JobC2EPNS0_21BackgroundCompileTaskE
	.weak	_ZTVN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEEE
	.section	.data.rel.ro.local._ZTVN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEEE,"awG",@progbits,_ZTVN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEEE,comdat
	.align 8
	.type	_ZTVN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEEE, @object
	.size	_ZTVN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEEE, 48
_ZTVN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEED1Ev
	.quad	_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEED0Ev
	.quad	_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEE15NewPointerArrayEm
	.quad	_ZN2v88internal11IdentityMapImNS0_25FreeStoreAllocationPolicyEE11DeleteArrayEPv
	.section	.bss._ZZN2v88internal18CompilerDispatcher10DoIdleWorkEdE28trace_event_unique_atomic310,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal18CompilerDispatcher10DoIdleWorkEdE28trace_event_unique_atomic310, @object
	.size	_ZZN2v88internal18CompilerDispatcher10DoIdleWorkEdE28trace_event_unique_atomic310, 8
_ZZN2v88internal18CompilerDispatcher10DoIdleWorkEdE28trace_event_unique_atomic310:
	.zero	8
	.section	.bss._ZZN2v88internal18CompilerDispatcher16DoBackgroundWorkEvE28trace_event_unique_atomic257,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal18CompilerDispatcher16DoBackgroundWorkEvE28trace_event_unique_atomic257, @object
	.size	_ZZN2v88internal18CompilerDispatcher16DoBackgroundWorkEvE28trace_event_unique_atomic257, 8
_ZZN2v88internal18CompilerDispatcher16DoBackgroundWorkEvE28trace_event_unique_atomic257:
	.zero	8
	.section	.bss._ZZN2v88internal18CompilerDispatcher31ScheduleMoreWorkerTasksIfNeededEvE28trace_event_unique_atomic242,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal18CompilerDispatcher31ScheduleMoreWorkerTasksIfNeededEvE28trace_event_unique_atomic242, @object
	.size	_ZZN2v88internal18CompilerDispatcher31ScheduleMoreWorkerTasksIfNeededEvE28trace_event_unique_atomic242, 8
_ZZN2v88internal18CompilerDispatcher31ScheduleMoreWorkerTasksIfNeededEvE28trace_event_unique_atomic242:
	.zero	8
	.section	.bss._ZZN2v88internal18CompilerDispatcher9FinishNowENS0_6HandleINS0_18SharedFunctionInfoEEEE28trace_event_unique_atomic152,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal18CompilerDispatcher9FinishNowENS0_6HandleINS0_18SharedFunctionInfoEEEE28trace_event_unique_atomic152, @object
	.size	_ZZN2v88internal18CompilerDispatcher9FinishNowENS0_6HandleINS0_18SharedFunctionInfoEEEE28trace_event_unique_atomic152, 8
_ZZN2v88internal18CompilerDispatcher9FinishNowENS0_6HandleINS0_18SharedFunctionInfoEEEE28trace_event_unique_atomic152:
	.zero	8
	.section	.bss._ZZN2v88internal18CompilerDispatcher31WaitForJobIfRunningOnBackgroundEPNS1_3JobEE28trace_event_unique_atomic132,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal18CompilerDispatcher31WaitForJobIfRunningOnBackgroundEPNS1_3JobEE28trace_event_unique_atomic132, @object
	.size	_ZZN2v88internal18CompilerDispatcher31WaitForJobIfRunningOnBackgroundEPNS1_3JobEE28trace_event_unique_atomic132, 8
_ZZN2v88internal18CompilerDispatcher31WaitForJobIfRunningOnBackgroundEPNS1_3JobEE28trace_event_unique_atomic132:
	.zero	8
	.section	.bss._ZZN2v88internal18CompilerDispatcher7EnqueueEPKNS0_9ParseInfoEPKNS0_12AstRawStringEPKNS0_15FunctionLiteralEE27trace_event_unique_atomic62,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal18CompilerDispatcher7EnqueueEPKNS0_9ParseInfoEPKNS0_12AstRawStringEPKNS0_15FunctionLiteralEE27trace_event_unique_atomic62, @object
	.size	_ZZN2v88internal18CompilerDispatcher7EnqueueEPKNS0_9ParseInfoEPKNS0_12AstRawStringEPKNS0_15FunctionLiteralEE27trace_event_unique_atomic62, 8
_ZZN2v88internal18CompilerDispatcher7EnqueueEPKNS0_9ParseInfoEPKNS0_12AstRawStringEPKNS0_15FunctionLiteralEE27trace_event_unique_atomic62:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	-1
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	1065353216
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC8:
	.long	0
	.long	1083129856
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
