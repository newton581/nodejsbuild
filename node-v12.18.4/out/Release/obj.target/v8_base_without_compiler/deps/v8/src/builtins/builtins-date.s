	.file	"builtins-date.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal29SmallStringOptimizedAllocatorILm128EED2Ev,"axG",@progbits,_ZN2v88internal29SmallStringOptimizedAllocatorILm128EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal29SmallStringOptimizedAllocatorILm128EED2Ev
	.type	_ZN2v88internal29SmallStringOptimizedAllocatorILm128EED2Ev, @function
_ZN2v88internal29SmallStringOptimizedAllocatorILm128EED2Ev:
.LFB23353:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE23353:
	.size	_ZN2v88internal29SmallStringOptimizedAllocatorILm128EED2Ev, .-_ZN2v88internal29SmallStringOptimizedAllocatorILm128EED2Ev
	.weak	_ZN2v88internal29SmallStringOptimizedAllocatorILm128EED1Ev
	.set	_ZN2v88internal29SmallStringOptimizedAllocatorILm128EED1Ev,_ZN2v88internal29SmallStringOptimizedAllocatorILm128EED2Ev
	.section	.text._ZN2v88internal29SmallStringOptimizedAllocatorILm128EED0Ev,"axG",@progbits,_ZN2v88internal29SmallStringOptimizedAllocatorILm128EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal29SmallStringOptimizedAllocatorILm128EED0Ev
	.type	_ZN2v88internal29SmallStringOptimizedAllocatorILm128EED0Ev, @function
_ZN2v88internal29SmallStringOptimizedAllocatorILm128EED0Ev:
.LFB23355:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23355:
	.size	_ZN2v88internal29SmallStringOptimizedAllocatorILm128EED0Ev, .-_ZN2v88internal29SmallStringOptimizedAllocatorILm128EED0Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_17MakeDayEddd.part.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_17MakeDayEddd.part.0, @function
_ZN2v88internal12_GLOBAL__N_17MakeDayEddd.part.0:
.LFB26158:
	.cfi_startproc
	movq	.LC2(%rip), %xmm3
	movsd	.LC3(%rip), %xmm5
	movapd	%xmm2, %xmm4
	andpd	%xmm3, %xmm4
	ucomisd	%xmm4, %xmm5
	jb	.L19
	cvttsd2sil	%xmm1, %edx
	cvttsd2sil	%xmm0, %eax
	movslq	%edx, %rcx
	movl	%edx, %esi
	imulq	$715827883, %rcx, %rcx
	sarl	$31, %esi
	sarq	$33, %rcx
	subl	%esi, %ecx
	addl	%ecx, %eax
	leal	(%rcx,%rcx,2), %ecx
	sall	$2, %ecx
	subl	%ecx, %edx
	js	.L25
.L9:
	leal	399999(%rax), %esi
	leal	400002(%rax), %edi
	imull	$365, %esi, %ecx
	testl	%esi, %esi
	cmovns	%esi, %edi
	sarl	$2, %edi
	addl	%ecx, %edi
	movslq	%esi, %rcx
	sarl	$31, %esi
	imulq	$1374389535, %rcx, %rcx
	movl	%esi, %r10d
	movq	%rcx, %r9
	sarq	$39, %rcx
	sarq	$37, %r9
	subl	%esi, %ecx
	subl	%r9d, %r10d
	addl	%r10d, %edi
	leal	-146816162(%rdi,%rcx), %ecx
	testb	$3, %al
	je	.L26
.L10:
	leaq	_ZZN2v88internal12_GLOBAL__N_17MakeDayEdddE13kDayFromMonth(%rip), %rax
	movslq	%edx, %rdx
	addl	(%rax,%rdx,4), %ecx
	movl	%ecx, %eax
.L12:
	subl	$1, %eax
	ucomisd	%xmm2, %xmm2
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	jp	.L20
	pxor	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm2
	jnp	.L27
.L21:
	comisd	%xmm1, %xmm2
	movapd	%xmm2, %xmm4
	movsd	.LC4(%rip), %xmm5
	movapd	%xmm2, %xmm1
	andpd	%xmm3, %xmm4
	jb	.L23
	ucomisd	%xmm4, %xmm5
	jbe	.L18
	cvttsd2siq	%xmm2, %rax
	pxor	%xmm4, %xmm4
	movsd	.LC5(%rip), %xmm5
	andnpd	%xmm2, %xmm3
	cvtsi2sdq	%rax, %xmm4
	movapd	%xmm4, %xmm6
	cmpnlesd	%xmm2, %xmm6
	movapd	%xmm6, %xmm1
	andpd	%xmm5, %xmm1
	subsd	%xmm1, %xmm4
	movapd	%xmm4, %xmm1
	orpd	%xmm3, %xmm1
.L18:
	movapd	%xmm1, %xmm2
.L13:
	addsd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movsd	.LC1(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	imull	$-1030792151, %eax, %eax
	addl	$85899344, %eax
	movl	%eax, %esi
	rorl	$2, %esi
	cmpl	$42949672, %esi
	ja	.L11
	rorl	$4, %eax
	cmpl	$10737418, %eax
	ja	.L10
.L11:
	leaq	_ZZN2v88internal12_GLOBAL__N_17MakeDayEdddE13kDayFromMonth_0(%rip), %rax
	movslq	%edx, %rdx
	addl	(%rax,%rdx,4), %ecx
	movl	%ecx, %eax
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L25:
	addl	$12, %edx
	subl	$1, %eax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L23:
	ucomisd	%xmm4, %xmm5
	jbe	.L18
	cvttsd2siq	%xmm2, %rax
	pxor	%xmm4, %xmm4
	movsd	.LC5(%rip), %xmm5
	andnpd	%xmm2, %xmm3
	cvtsi2sdq	%rax, %xmm4
	cmpnlesd	%xmm4, %xmm1
	andpd	%xmm5, %xmm1
	addsd	%xmm4, %xmm1
	orpd	%xmm3, %xmm1
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L27:
	jne	.L21
	addsd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	pxor	%xmm2, %xmm2
	jmp	.L13
	.cfi_endproc
.LFE26158:
	.size	_ZN2v88internal12_GLOBAL__N_17MakeDayEddd.part.0, .-_ZN2v88internal12_GLOBAL__N_17MakeDayEddd.part.0
	.section	.text._ZN2v88internal12_GLOBAL__N_18MakeTimeEdddd,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_18MakeTimeEdddd, @function
_ZN2v88internal12_GLOBAL__N_18MakeTimeEdddd:
.LFB21338:
	.cfi_startproc
	movq	.LC2(%rip), %xmm4
	movsd	.LC3(%rip), %xmm5
	movapd	%xmm0, %xmm7
	movsd	.LC1(%rip), %xmm6
	andpd	%xmm4, %xmm7
	ucomisd	%xmm7, %xmm5
	jb	.L28
	movapd	%xmm1, %xmm6
	andpd	%xmm4, %xmm6
	ucomisd	%xmm6, %xmm5
	movsd	.LC1(%rip), %xmm6
	jb	.L28
	movapd	%xmm2, %xmm7
	andpd	%xmm4, %xmm7
	ucomisd	%xmm7, %xmm5
	jb	.L28
	movapd	%xmm3, %xmm7
	andpd	%xmm4, %xmm7
	ucomisd	%xmm7, %xmm5
	jnb	.L78
.L28:
	movapd	%xmm6, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	ucomisd	%xmm0, %xmm0
	pxor	%xmm5, %xmm5
	jp	.L61
	ucomisd	%xmm5, %xmm0
	jnp	.L79
.L31:
	comisd	%xmm5, %xmm0
	movapd	%xmm0, %xmm7
	movapd	%xmm0, %xmm6
	movsd	.LC4(%rip), %xmm9
	movapd	%xmm4, %xmm8
	andpd	%xmm4, %xmm7
	jb	.L73
	ucomisd	%xmm7, %xmm9
	jbe	.L36
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm7, %xmm7
	andnpd	%xmm0, %xmm8
	movsd	.LC5(%rip), %xmm9
	cvtsi2sdq	%rax, %xmm7
	movapd	%xmm7, %xmm10
	cmpnlesd	%xmm0, %xmm10
	movapd	%xmm10, %xmm6
	andpd	%xmm9, %xmm6
	subsd	%xmm6, %xmm7
	movapd	%xmm7, %xmm6
	orpd	%xmm8, %xmm6
.L36:
	mulsd	.LC6(%rip), %xmm6
	movapd	%xmm6, %xmm0
.L30:
	ucomisd	%xmm1, %xmm1
	jp	.L62
	ucomisd	%xmm5, %xmm1
	jnp	.L80
.L38:
	comisd	%xmm5, %xmm1
	movapd	%xmm1, %xmm7
	movapd	%xmm1, %xmm6
	movsd	.LC4(%rip), %xmm9
	movapd	%xmm4, %xmm8
	andpd	%xmm4, %xmm7
	jb	.L74
	ucomisd	%xmm7, %xmm9
	jbe	.L43
	cvttsd2siq	%xmm1, %rax
	pxor	%xmm7, %xmm7
	andnpd	%xmm1, %xmm8
	movsd	.LC5(%rip), %xmm9
	cvtsi2sdq	%rax, %xmm7
	movapd	%xmm7, %xmm11
	cmpnlesd	%xmm1, %xmm11
	movapd	%xmm11, %xmm6
	andpd	%xmm9, %xmm6
	subsd	%xmm6, %xmm7
	movapd	%xmm7, %xmm6
	orpd	%xmm8, %xmm6
.L43:
	mulsd	.LC7(%rip), %xmm6
.L37:
	ucomisd	%xmm2, %xmm2
	jp	.L63
	ucomisd	%xmm5, %xmm2
	jnp	.L81
.L45:
	comisd	%xmm5, %xmm2
	movapd	%xmm2, %xmm1
	movapd	%xmm2, %xmm7
	movsd	.LC4(%rip), %xmm9
	movapd	%xmm4, %xmm8
	andpd	%xmm4, %xmm1
	jb	.L75
	ucomisd	%xmm1, %xmm9
	jbe	.L49
	cvttsd2siq	%xmm2, %rax
	pxor	%xmm1, %xmm1
	andnpd	%xmm2, %xmm8
	movsd	.LC5(%rip), %xmm9
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm1, %xmm12
	cmpnlesd	%xmm2, %xmm12
	movapd	%xmm12, %xmm7
	andpd	%xmm9, %xmm7
	subsd	%xmm7, %xmm1
	movapd	%xmm1, %xmm7
	orpd	%xmm8, %xmm7
.L49:
	movapd	%xmm7, %xmm2
.L77:
	mulsd	.LC8(%rip), %xmm2
.L44:
	ucomisd	%xmm3, %xmm3
	jp	.L64
	ucomisd	%xmm5, %xmm3
	jnp	.L82
.L71:
	comisd	%xmm5, %xmm3
	movapd	%xmm3, %xmm1
	movsd	.LC4(%rip), %xmm7
	movapd	%xmm3, %xmm5
	andpd	%xmm4, %xmm1
	jb	.L76
	ucomisd	%xmm1, %xmm7
	jbe	.L64
	cvttsd2siq	%xmm3, %rax
	pxor	%xmm1, %xmm1
	andnpd	%xmm3, %xmm4
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm1, %xmm7
	cmpnlesd	%xmm3, %xmm7
	movapd	%xmm7, %xmm5
	movsd	.LC5(%rip), %xmm7
	andpd	%xmm7, %xmm5
	subsd	%xmm5, %xmm1
	movapd	%xmm1, %xmm5
	orpd	%xmm4, %xmm5
.L64:
	movapd	%xmm5, %xmm3
.L51:
	addsd	%xmm0, %xmm6
	addsd	%xmm2, %xmm6
	addsd	%xmm3, %xmm6
	jmp	.L28
.L82:
	je	.L51
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L81:
	je	.L77
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L80:
	jne	.L38
	mulsd	.LC7(%rip), %xmm1
	movapd	%xmm1, %xmm6
	jmp	.L37
.L79:
	jne	.L31
	mulsd	.LC6(%rip), %xmm0
	jmp	.L30
.L73:
	ucomisd	%xmm7, %xmm9
	jbe	.L36
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm7, %xmm7
	andnpd	%xmm0, %xmm8
	movsd	.LC5(%rip), %xmm9
	cvtsi2sdq	%rax, %xmm7
	cmpnlesd	%xmm7, %xmm6
	andpd	%xmm9, %xmm6
	addsd	%xmm7, %xmm6
	orpd	%xmm8, %xmm6
	jmp	.L36
.L74:
	ucomisd	%xmm7, %xmm9
	jbe	.L43
	cvttsd2siq	%xmm1, %rax
	pxor	%xmm7, %xmm7
	andnpd	%xmm1, %xmm8
	movsd	.LC5(%rip), %xmm9
	cvtsi2sdq	%rax, %xmm7
	cmpnlesd	%xmm7, %xmm6
	andpd	%xmm9, %xmm6
	addsd	%xmm7, %xmm6
	orpd	%xmm8, %xmm6
	jmp	.L43
.L75:
	ucomisd	%xmm1, %xmm9
	jbe	.L49
	cvttsd2siq	%xmm2, %rax
	pxor	%xmm1, %xmm1
	andnpd	%xmm2, %xmm8
	movsd	.LC5(%rip), %xmm9
	cvtsi2sdq	%rax, %xmm1
	cmpnlesd	%xmm1, %xmm7
	andpd	%xmm9, %xmm7
	addsd	%xmm1, %xmm7
	orpd	%xmm8, %xmm7
	jmp	.L49
.L76:
	ucomisd	%xmm1, %xmm7
	jbe	.L64
	cvttsd2siq	%xmm3, %rax
	pxor	%xmm1, %xmm1
	movsd	.LC5(%rip), %xmm7
	andnpd	%xmm3, %xmm4
	cvtsi2sdq	%rax, %xmm1
	cmpnlesd	%xmm1, %xmm5
	andpd	%xmm7, %xmm5
	addsd	%xmm1, %xmm5
	orpd	%xmm4, %xmm5
	jmp	.L64
.L63:
	movapd	%xmm5, %xmm2
	jmp	.L44
.L61:
	movapd	%xmm5, %xmm0
	jmp	.L30
.L62:
	movapd	%xmm5, %xmm6
	jmp	.L37
	.cfi_endproc
.LFE21338:
	.size	_ZN2v88internal12_GLOBAL__N_18MakeTimeEdddd, .-_ZN2v88internal12_GLOBAL__N_18MakeTimeEdddd
	.section	.rodata._ZN2v88internalL40Builtin_Impl_DatePrototypeToLocaleStringENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"Date.prototype.toLocaleString"
.LC10:
	.string	"(location_) != nullptr"
.LC11:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL40Builtin_Impl_DatePrototypeToLocaleStringENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Builtin_Impl_DatePrototypeToLocaleStringENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL40Builtin_Impl_DatePrototypeToLocaleStringENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21422:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rdx, %rdi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$66, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L84
.L87:
	leaq	.LC9(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$29, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L102
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L90:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L96
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L96:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L103
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1066, 11(%rax)
	jne	.L87
	cmpl	$6, %r15d
	jg	.L104
	leaq	88(%r12), %rdx
	movq	%rdx, %rcx
	je	.L93
.L91:
	movl	$2, %r9d
	movl	$2, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal16JSDateTimeFormat16ToLocaleDateTimeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_NS1_14RequiredOptionENS1_14DefaultsOptionE@PLT
	testq	%rax, %rax
	je	.L105
	movq	(%rax), %r13
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L102:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L104:
	leaq	-16(%r13), %rcx
.L93:
	leaq	-8(%r13), %rdx
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L105:
	movq	312(%r12), %r13
	jmp	.L90
.L103:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21422:
	.size	_ZN2v88internalL40Builtin_Impl_DatePrototypeToLocaleStringENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL40Builtin_Impl_DatePrototypeToLocaleStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL44Builtin_Impl_DatePrototypeToLocaleDateStringENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"Date.prototype.toLocaleDateString"
	.section	.text._ZN2v88internalL44Builtin_Impl_DatePrototypeToLocaleDateStringENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL44Builtin_Impl_DatePrototypeToLocaleDateStringENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL44Builtin_Impl_DatePrototypeToLocaleDateStringENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21419:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rdx, %rdi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$67, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L107
.L110:
	leaq	.LC12(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$33, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L125
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L113:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L119
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L119:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L126
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1066, 11(%rax)
	jne	.L110
	cmpl	$6, %r15d
	jg	.L127
	leaq	88(%r12), %rdx
	movq	%rdx, %rcx
	je	.L116
.L114:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal16JSDateTimeFormat16ToLocaleDateTimeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_NS1_14RequiredOptionENS1_14DefaultsOptionE@PLT
	testq	%rax, %rax
	je	.L128
	movq	(%rax), %r13
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L125:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L127:
	leaq	-16(%r13), %rcx
.L116:
	leaq	-8(%r13), %rdx
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L128:
	movq	312(%r12), %r13
	jmp	.L113
.L126:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21419:
	.size	_ZN2v88internalL44Builtin_Impl_DatePrototypeToLocaleDateStringENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL44Builtin_Impl_DatePrototypeToLocaleDateStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL44Builtin_Impl_DatePrototypeToLocaleTimeStringENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"Date.prototype.toLocaleTimeString"
	.section	.text._ZN2v88internalL44Builtin_Impl_DatePrototypeToLocaleTimeStringENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL44Builtin_Impl_DatePrototypeToLocaleTimeStringENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL44Builtin_Impl_DatePrototypeToLocaleTimeStringENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21425:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rdx, %rdi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$68, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L130
.L133:
	leaq	.LC13(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$33, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L148
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L136:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L142
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L142:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L149
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1066, 11(%rax)
	jne	.L133
	cmpl	$6, %r15d
	jg	.L150
	leaq	88(%r12), %rdx
	movq	%rdx, %rcx
	je	.L139
.L137:
	movl	$1, %r9d
	movl	$1, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal16JSDateTimeFormat16ToLocaleDateTimeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_NS1_14RequiredOptionENS1_14DefaultsOptionE@PLT
	testq	%rax, %rax
	je	.L151
	movq	(%rax), %r13
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L148:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L150:
	leaq	-16(%r13), %rcx
.L139:
	leaq	-8(%r13), %rdx
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L151:
	movq	312(%r12), %r13
	jmp	.L136
.L149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21425:
	.size	_ZN2v88internalL44Builtin_Impl_DatePrototypeToLocaleTimeStringENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL44Builtin_Impl_DatePrototypeToLocaleTimeStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL33Builtin_Impl_DatePrototypeGetYearENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"Date.prototype.getYear"
	.section	.text._ZN2v88internalL33Builtin_Impl_DatePrototypeGetYearENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Builtin_Impl_DatePrototypeGetYearENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL33Builtin_Impl_DatePrototypeGetYearENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21431:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L153
.L156:
	leaq	.LC14(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$22, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L170
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L159:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L164
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L164:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L171
	addq	$48, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1066, 11(%rdx)
	jne	.L156
	movq	23(%rax), %r13
	testb	$1, %r13b
	jne	.L172
	movq	%r13, %rax
	pxor	%xmm0, %xmm0
	sarq	$32, %rax
	cvtsi2sdl	%eax, %xmm0
.L160:
	ucomisd	%xmm0, %xmm0
	jp	.L159
	cvttsd2siq	%xmm0, %r13
	movq	41240(%r12), %rdi
	movl	$1, %edx
	movq	(%rdi), %rax
	movq	%r13, %rsi
	call	*24(%rax)
	leaq	-68(%rbp), %r10
	leaq	-72(%rbp), %r9
	movabsq	$7164004856975580295, %rdx
	movslq	%eax, %rcx
	movq	41240(%r12), %rdi
	leaq	-64(%rbp), %r8
	addq	%r13, %rcx
	leaq	-86399999(%rcx), %rax
	cmovs	%rax, %rcx
	movq	%rcx, %rax
	sarq	$63, %rcx
	imulq	%rdx
	sarq	$25, %rdx
	movq	%rdx, %rsi
	movq	%r9, %rdx
	subq	%rcx, %rsi
	movq	%r10, %rcx
	call	_ZN2v88internal9DateCache20YearMonthDayFromDaysEiPiS2_S2_@PLT
	movl	-72(%rbp), %eax
	leal	-1900(%rax), %r13d
	salq	$32, %r13
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L170:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L172:
	movsd	7(%r13), %xmm0
	jmp	.L160
.L171:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21431:
	.size	_ZN2v88internalL33Builtin_Impl_DatePrototypeGetYearENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL33Builtin_Impl_DatePrototypeGetYearENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL32Builtin_Impl_DatePrototypeToJsonENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"toISOString"
	.section	.text._ZN2v88internalL32Builtin_Impl_DatePrototypeToJsonENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Builtin_Impl_DatePrototypeToJsonENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL32Builtin_Impl_DatePrototypeToJsonENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21437:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	88(%rdx), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	cmpl	$4, %edi
	cmovg	%rsi, %r13
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L176
.L179:
	movq	%r13, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L213
.L178:
	movq	0(%r13), %rdx
	testb	$1, %dl
	jne	.L215
.L182:
	sarq	$32, %rdx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
.L190:
	andpd	.LC2(%rip), %xmm0
	movsd	.LC3(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jnb	.L192
	movq	104(%r12), %r13
.L180:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L205
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L205:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L216
	addq	$120, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	movq	-1(%rdx), %rax
	cmpw	$67, 11(%rax)
	ja	.L183
	xorl	%ecx, %ecx
	movq	%r13, %rax
.L184:
	testb	%cl, %cl
	jne	.L182
	movq	-1(%rdx), %rdx
	cmpw	$65, 11(%rdx)
	je	.L186
.L192:
	leaq	-144(%rbp), %r15
	leaq	.LC15(%rip), %rax
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rax, -144(%rbp)
	movq	$11, -136(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L217
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L193
.L195:
	movl	$-1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-152(%rbp), %r9
	movq	%rax, %rcx
.L194:
	movq	(%r9), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L196
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L196:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	%r12, -120(%rbp)
	movq	(%r9), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	movq	%r9, %rax
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L218
.L197:
	movq	%r15, %rdi
	movq	%r9, -152(%rbp)
	movq	%rax, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	movq	-152(%rbp), %r9
	jne	.L198
	movq	-120(%rbp), %rax
	xorl	%r8d, %r8d
	leaq	88(%rax), %rsi
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L219
.L202:
	movq	%r9, %rdx
	xorl	%ecx, %ecx
	movl	$25, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L186:
	movq	(%rax), %rdx
	testb	$1, %dl
	je	.L182
	movsd	7(%rdx), %xmm0
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L183:
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver11ToPrimitiveENS0_6HandleIS1_EENS0_15ToPrimitiveHintE@PLT
	testq	%rax, %rax
	jne	.L185
.L213:
	movq	312(%r12), %r13
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L176:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L179
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L198:
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	-152(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L213
	movq	(%rsi), %rax
	xorl	%r8d, %r8d
	testb	$1, %al
	je	.L202
.L219:
	movq	-1(%rax), %rax
	testb	$2, 13(%rax)
	je	.L202
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	testq	%rax, %rax
	je	.L213
	movq	(%rax), %r13
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L193:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L195
	movq	%r13, %rcx
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L218:
	movq	%r9, %rsi
	movq	%r12, %rdi
	movq	%rcx, -160(%rbp)
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-160(%rbp), %rcx
	movq	-152(%rbp), %r9
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L217:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L216:
	call	__stack_chk_fail@PLT
.L185:
	movq	(%rax), %rdx
	movq	%rdx, %rcx
	notq	%rcx
	andl	$1, %ecx
	jmp	.L184
	.cfi_endproc
.LFE21437:
	.size	_ZN2v88internalL32Builtin_Impl_DatePrototypeToJsonENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL32Builtin_Impl_DatePrototypeToJsonENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL37Builtin_Impl_DatePrototypeToUTCStringENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"%s, %02d %s %05d %02d:%02d:%02d GMT"
	.align 8
.LC17:
	.string	"%s, %02d %s %04d %02d:%02d:%02d GMT"
	.section	.rodata._ZN2v88internalL37Builtin_Impl_DatePrototypeToUTCStringENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC18:
	.string	"Date.prototype.toUTCString"
.LC19:
	.string	"Invalid Date"
	.section	.text._ZN2v88internalL37Builtin_Impl_DatePrototypeToUTCStringENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Builtin_Impl_DatePrototypeToUTCStringENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL37Builtin_Impl_DatePrototypeToUTCStringENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21428:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$224, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L221
.L224:
	leaq	.LC18(%rip), %rax
	xorl	%edx, %edx
	leaq	-224(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	movq	$26, -216(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L222
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L227:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L234
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L234:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L255
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1066, 11(%rdx)
	jne	.L224
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L256
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L228:
	ucomisd	%xmm0, %xmm0
	jp	.L257
	leaq	-228(%rbp), %rax
	movq	41240(%r12), %rdi
	leaq	-252(%rbp), %rcx
	pushq	%rax
	leaq	-232(%rbp), %rax
	leaq	-256(%rbp), %rdx
	pushq	%rax
	leaq	-236(%rbp), %rax
	leaq	-244(%rbp), %r9
	pushq	%rax
	cvttsd2siq	%xmm0, %rsi
	leaq	-240(%rbp), %rax
	pushq	%rax
	leaq	-248(%rbp), %r8
	leaq	-176(%rbp), %r13
	call	_ZN2v88internal9DateCache13BreakDownTimeElPiS2_S2_S2_S2_S2_S2_S2_@PLT
	movl	-256(%rbp), %eax
	movslq	-244(%rbp), %rcx
	leaq	_ZN2v88internal12_GLOBAL__N_114kShortWeekDaysE(%rip), %rdx
	addq	$32, %rsp
	leaq	.LC16(%rip), %rdi
	movslq	-252(%rbp), %rsi
	movl	-248(%rbp), %r8d
	testl	%eax, %eax
	movq	(%rdx,%rcx,8), %rcx
	leaq	.LC17(%rip), %rdx
	cmovs	%rdi, %rdx
	movl	-232(%rbp), %edi
	pushq	%rdi
	movl	-236(%rbp), %edi
	pushq	%rdi
	movl	-240(%rbp), %edi
	pushq	%rdi
	movq	%r13, %rdi
	pushq	%rax
	leaq	_ZN2v88internal12_GLOBAL__N_112kShortMonthsE(%rip), %rax
	movq	(%rax,%rsi,8), %r9
	movl	$128, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	%r13, %rdx
.L231:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L231
	movl	%eax, %ecx
	movq	%r13, -192(%rbp)
	leaq	-192(%rbp), %rsi
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %ecx
	addb	%al, %cl
	sbbq	$3, %rdx
	addq	$32, %rsp
	subq	%r13, %rdx
	movq	%rdx, -184(%rbp)
.L254:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L222
	movq	(%rax), %r13
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L222:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L256:
	movsd	7(%rax), %xmm0
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L257:
	leaq	.LC19(%rip), %rax
	leaq	-208(%rbp), %rsi
	movq	$12, -200(%rbp)
	movq	%rax, -208(%rbp)
	jmp	.L254
.L255:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21428:
	.size	_ZN2v88internalL37Builtin_Impl_DatePrototypeToUTCStringENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL37Builtin_Impl_DatePrototypeToUTCStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL37Builtin_Impl_DatePrototypeToISOStringENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC20:
	.string	"Date.prototype.toISOString"
	.section	.rodata._ZN2v88internalL37Builtin_Impl_DatePrototypeToISOStringENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"%04d-%02d-%02dT%02d:%02d:%02d.%03dZ"
	.align 8
.LC22:
	.string	"-%06d-%02d-%02dT%02d:%02d:%02d.%03dZ"
	.align 8
.LC23:
	.string	"+%06d-%02d-%02dT%02d:%02d:%02d.%03dZ"
	.section	.text._ZN2v88internalL37Builtin_Impl_DatePrototypeToISOStringENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Builtin_Impl_DatePrototypeToISOStringENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL37Builtin_Impl_DatePrototypeToISOStringENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21406:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$208, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L259
.L262:
	leaq	.LC20(%rip), %rax
	xorl	%edx, %edx
	leaq	-208(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -208(%rbp)
	movq	$26, -200(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L260
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
.L289:
	movq	(%rax), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L265:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L274
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L274:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L290
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L259:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1066, 11(%rdx)
	jne	.L262
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L291
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L266:
	ucomisd	%xmm0, %xmm0
	jp	.L292
	leaq	-212(%rbp), %rax
	movq	41240(%r12), %rdi
	leaq	-236(%rbp), %rcx
	pushq	%rax
	leaq	-216(%rbp), %rax
	leaq	-240(%rbp), %rdx
	pushq	%rax
	leaq	-220(%rbp), %rax
	leaq	-228(%rbp), %r9
	pushq	%rax
	cvttsd2siq	%xmm0, %rsi
	leaq	-224(%rbp), %rax
	pushq	%rax
	leaq	-232(%rbp), %r8
	call	_ZN2v88internal9DateCache13BreakDownTimeElPiS2_S2_S2_S2_S2_S2_S2_@PLT
	movl	-240(%rbp), %ecx
	addq	$32, %rsp
	cmpl	$9999, %ecx
	jbe	.L293
	movl	-236(%rbp), %r10d
	movl	-212(%rbp), %edi
	leaq	-176(%rbp), %r13
	movl	-216(%rbp), %esi
	movl	-220(%rbp), %edx
	movl	-224(%rbp), %eax
	movl	-232(%rbp), %r9d
	leal	1(%r10), %r8d
	testl	%ecx, %ecx
	js	.L294
	pushq	%rdi
	pushq	%rsi
	pushq	%rdx
	leaq	.LC23(%rip), %rdx
	pushq	%rax
.L288:
	movq	%r13, %rdi
	movl	$128, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	addq	$32, %rsp
	movq	%r13, %rdx
.L271:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L271
	movl	%eax, %ecx
	leaq	-192(%rbp), %rsi
	movq	%r13, -192(%rbp)
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %edi
	addb	%al, %dil
	movq	%r12, %rdi
	sbbq	$3, %rdx
	subq	%r13, %rdx
	movq	%rdx, -184(%rbp)
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L260
	movq	(%rax), %r13
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L260:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L291:
	movsd	7(%rax), %xmm0
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L294:
	pushq	%rdi
	negl	%ecx
	pushq	%rsi
	pushq	%rdx
	leaq	.LC22(%rip), %rdx
	pushq	%rax
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L293:
	movl	-212(%rbp), %eax
	movl	-232(%rbp), %r9d
	leaq	-176(%rbp), %r13
	leaq	.LC21(%rip), %rdx
	pushq	%rax
	movl	-216(%rbp), %eax
	pushq	%rax
	movl	-220(%rbp), %eax
	pushq	%rax
	movl	-224(%rbp), %eax
	pushq	%rax
	movl	-236(%rbp), %eax
	leal	1(%rax), %r8d
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L292:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$202, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	jmp	.L289
.L290:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21406:
	.size	_ZN2v88internalL37Builtin_Impl_DatePrototypeToISOStringENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL37Builtin_Impl_DatePrototypeToISOStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL33Builtin_Impl_DatePrototypeSetTimeENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC24:
	.string	"Date.prototype.setTime"
	.section	.text._ZN2v88internalL33Builtin_Impl_DatePrototypeSetTimeENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Builtin_Impl_DatePrototypeSetTimeENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL33Builtin_Impl_DatePrototypeSetTimeENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21379:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L296
.L299:
	leaq	.LC24(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$22, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L317
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L302:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L312
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L312:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L318
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L296:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1066, 11(%rax)
	jne	.L299
	leaq	-8(%rsi), %rax
	cmpl	$6, %edi
	leaq	88(%rdx), %rsi
	cmovge	%rax, %rsi
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L319
.L305:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L310:
	call	_ZN2v88internal9DateCache8TimeClipEd@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal6JSDate8SetValueENS0_6HandleIS1_EEd@PLT
	movq	(%rax), %r13
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L317:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L319:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L306
	xorl	%edx, %edx
.L307:
	testb	%dl, %dl
	jne	.L305
	movsd	7(%rax), %xmm0
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L306:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	testq	%rax, %rax
	jne	.L308
	movq	312(%r12), %r13
	jmp	.L302
.L318:
	call	__stack_chk_fail@PLT
.L308:
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L307
	.cfi_endproc
.LFE21379:
	.size	_ZN2v88internalL33Builtin_Impl_DatePrototypeSetTimeENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL33Builtin_Impl_DatePrototypeSetTimeENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL44Builtin_Impl_DatePrototypeSetUTCMillisecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC25:
	.string	"Date.prototype.setUTCMilliseconds"
	.section	.text._ZN2v88internalL44Builtin_Impl_DatePrototypeSetUTCMillisecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL44Builtin_Impl_DatePrototypeSetUTCMillisecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL44Builtin_Impl_DatePrototypeSetUTCMillisecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21391:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L321
.L324:
	leaq	.LC25(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$33, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L350
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L327:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L340
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L340:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L351
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L321:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1066, 11(%rdx)
	jne	.L324
	leaq	88(%r12), %rdx
	leaq	-8(%rsi), %rsi
	cmpl	$6, %edi
	cmovl	%rdx, %rsi
	movq	(%rsi), %rdx
	testb	$1, %dl
	je	.L332
	movq	-1(%rdx), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L352
.L332:
	movq	23(%rax), %rax
	testb	$1, %al
	je	.L353
	movsd	7(%rax), %xmm13
.L334:
	ucomisd	%xmm13, %xmm13
	jp	.L335
	movabsq	$7164004856975580295, %rdx
	cvttsd2siq	%xmm13, %rdi
	testq	%rdi, %rdi
	leaq	-86399999(%rdi), %rcx
	cmovns	%rdi, %rcx
	movq	%rcx, %rax
	sarq	$63, %rcx
	imulq	%rdx
	movq	(%rsi), %rax
	sarq	$25, %rdx
	subq	%rcx, %rdx
	testb	$1, %al
	jne	.L337
	sarq	$32, %rax
	pxor	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm3
.L338:
	pxor	%xmm14, %xmm14
	movq	.LC2(%rip), %xmm15
	movsd	.LC3(%rip), %xmm4
	movsd	.LC1(%rip), %xmm13
	cvtsi2sdl	%edx, %xmm14
	movapd	%xmm14, %xmm0
	andpd	%xmm15, %xmm0
	ucomisd	%xmm0, %xmm4
	jb	.L335
	imull	$-86400000, %edx, %edx
	pxor	%xmm0, %xmm0
	pxor	%xmm2, %xmm2
	pxor	%xmm1, %xmm1
	addl	%edi, %edx
	movslq	%edx, %rcx
	sarl	$31, %edx
	imulq	$274877907, %rcx, %rdi
	sarq	$38, %rdi
	subl	%edx, %edi
	movslq	%edi, %rax
	movl	%edi, %esi
	imulq	$-2004318071, %rax, %rax
	sarl	$31, %esi
	shrq	$32, %rax
	addl	%edi, %eax
	sarl	$5, %eax
	subl	%esi, %eax
	imulq	$1172812403, %rcx, %rsi
	imull	$60, %eax, %eax
	imulq	$1250999897, %rcx, %rcx
	sarq	$46, %rsi
	subl	%edx, %esi
	subl	%eax, %edi
	movslq	%esi, %rax
	sarq	$52, %rcx
	cvtsi2sdl	%edi, %xmm2
	movl	%esi, %r8d
	imulq	$-2004318071, %rax, %rax
	sarl	$31, %r8d
	subl	%edx, %ecx
	cvtsi2sdl	%ecx, %xmm0
	shrq	$32, %rax
	addl	%esi, %eax
	sarl	$5, %eax
	subl	%r8d, %eax
	imull	$60, %eax, %eax
	subl	%eax, %esi
	cvtsi2sdl	%esi, %xmm1
	call	_ZN2v88internal12_GLOBAL__N_18MakeTimeEdddd
	movsd	.LC3(%rip), %xmm5
	andpd	%xmm0, %xmm15
	ucomisd	%xmm15, %xmm5
	jb	.L335
	mulsd	.LC26(%rip), %xmm14
	movapd	%xmm14, %xmm13
	addsd	%xmm0, %xmm13
	.p2align 4,,10
	.p2align 3
.L335:
	movapd	%xmm13, %xmm0
	call	_ZN2v88internal9DateCache8TimeClipEd@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal6JSDate8SetValueENS0_6HandleIS1_EEd@PLT
	movq	(%rax), %r13
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L350:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L353:
	sarq	$32, %rax
	pxor	%xmm13, %xmm13
	cvtsi2sdl	%eax, %xmm13
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L352:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L354
	movq	312(%r12), %r13
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L337:
	movsd	7(%rax), %xmm3
	jmp	.L338
.L351:
	call	__stack_chk_fail@PLT
.L354:
	movq	0(%r13), %rax
	jmp	.L332
	.cfi_endproc
.LFE21391:
	.size	_ZN2v88internalL44Builtin_Impl_DatePrototypeSetUTCMillisecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL44Builtin_Impl_DatePrototypeSetUTCMillisecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL36Builtin_Impl_DatePrototypeSetUTCDateENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC27:
	.string	"Date.prototype.setUTCDate"
	.section	.text._ZN2v88internalL36Builtin_Impl_DatePrototypeSetUTCDateENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Builtin_Impl_DatePrototypeSetUTCDateENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL36Builtin_Impl_DatePrototypeSetUTCDateENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21382:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L356
.L359:
	leaq	.LC27(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$25, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L393
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L362:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L376
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L376:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L394
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1066, 11(%rdx)
	jne	.L359
	leaq	-8(%rsi), %rdx
	leaq	88(%r12), %r9
	cmpl	$6, %edi
	cmovge	%rdx, %r9
	movq	(%r9), %rdx
	testb	$1, %dl
	je	.L367
	movq	-1(%rdx), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L395
.L367:
	movq	23(%rax), %rax
	testb	$1, %al
	je	.L396
	movsd	7(%rax), %xmm0
.L369:
	ucomisd	%xmm0, %xmm0
	jp	.L397
	cvttsd2siq	%xmm0, %r10
	movq	41240(%r12), %rdi
	leaq	-80(%rbp), %r8
	movq	%r9, -112(%rbp)
	movabsq	$7164004856975580295, %rdx
	testq	%r10, %r10
	leaq	-86399999(%r10), %rcx
	movq	%r10, -104(%rbp)
	cmovns	%r10, %rcx
	movq	%rcx, %rax
	sarq	$63, %rcx
	imulq	%rdx
	sarq	$25, %rdx
	subq	%rcx, %rdx
	leaq	-84(%rbp), %rcx
	movq	%rdx, %r15
	leaq	-88(%rbp), %rdx
	movl	%r15d, %esi
	call	_ZN2v88internal9DateCache20YearMonthDayFromDaysEiPiS2_S2_@PLT
	movq	-112(%rbp), %r9
	movq	-104(%rbp), %r10
	movq	(%r9), %rax
	testb	$1, %al
	jne	.L372
	sarq	$32, %rax
	pxor	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
.L373:
	movl	-88(%rbp), %eax
	movl	-84(%rbp), %edx
	leal	1000000(%rax), %ecx
	cmpl	$2000000, %ecx
	ja	.L380
	leal	10000000(%rdx), %ecx
	cmpl	$20000000, %ecx
	jbe	.L398
.L380:
	movsd	.LC1(%rip), %xmm0
.L374:
	call	_ZN2v88internal9DateCache8TimeClipEd@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal6JSDate8SetValueENS0_6HandleIS1_EEd@PLT
	movq	(%rax), %r13
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L393:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L396:
	movq	%rax, %rdx
	pxor	%xmm0, %xmm0
	sarq	$32, %rdx
	cvtsi2sdl	%edx, %xmm0
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L395:
	movq	%r9, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	jne	.L399
	movq	312(%r12), %r13
	jmp	.L362
.L398:
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movq	%r10, -104(%rbp)
	cvtsi2sdl	%edx, %xmm1
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal12_GLOBAL__N_17MakeDayEddd.part.0
	movsd	.LC3(%rip), %xmm3
	movapd	%xmm0, %xmm2
	movq	.LC2(%rip), %xmm0
	movapd	%xmm2, %xmm1
	andpd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm3
	jb	.L380
	imull	$-86400000, %r15d, %r15d
	movq	-104(%rbp), %r10
	pxor	%xmm1, %xmm1
	addl	%r15d, %r10d
	cvtsi2sdl	%r10d, %xmm1
	andpd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm3
	movsd	.LC1(%rip), %xmm0
	jb	.L374
	mulsd	.LC26(%rip), %xmm2
	movapd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L372:
	movsd	7(%rax), %xmm2
	jmp	.L373
.L397:
	movq	%rax, %r13
	jmp	.L362
.L394:
	call	__stack_chk_fail@PLT
.L399:
	movq	0(%r13), %rax
	jmp	.L367
	.cfi_endproc
.LFE21382:
	.size	_ZN2v88internalL36Builtin_Impl_DatePrototypeSetUTCDateENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL36Builtin_Impl_DatePrototypeSetUTCDateENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL41Builtin_Impl_DatePrototypeSetMillisecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC28:
	.string	"Date.prototype.setMilliseconds"
	.section	.text._ZN2v88internalL41Builtin_Impl_DatePrototypeSetMillisecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Builtin_Impl_DatePrototypeSetMillisecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL41Builtin_Impl_DatePrototypeSetMillisecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21367:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L401
.L404:
	leaq	.LC28(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$30, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L439
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L407:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L423
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L423:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L440
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L401:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1066, 11(%rdx)
	jne	.L404
	leaq	-8(%rsi), %rdx
	leaq	88(%r12), %r8
	cmpl	$6, %edi
	cmovge	%rdx, %r8
	movq	(%r8), %rdx
	testb	$1, %dl
	je	.L412
	movq	-1(%rdx), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L441
.L412:
	movq	23(%rax), %rax
	testb	$1, %al
	je	.L442
	movsd	7(%rax), %xmm13
.L414:
	ucomisd	%xmm13, %xmm13
	movq	%r8, -88(%rbp)
	jp	.L415
	cvttsd2siq	%xmm13, %r15
	movq	41240(%r12), %rdi
	movl	$1, %edx
	movq	(%rdi), %rax
	movq	%r15, %rsi
	call	*24(%rax)
	movq	-88(%rbp), %r8
	movslq	%eax, %rdx
	addq	%rdx, %r15
	movabsq	$7164004856975580295, %rdx
	leaq	-86399999(%r15), %rcx
	cmovns	%r15, %rcx
	movq	%rcx, %rax
	sarq	$63, %rcx
	imulq	%rdx
	movq	(%r8), %rax
	sarq	$25, %rdx
	subq	%rcx, %rdx
	testb	$1, %al
	jne	.L417
	sarq	$32, %rax
	pxor	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm3
.L418:
	pxor	%xmm14, %xmm14
	movq	.LC2(%rip), %xmm15
	movsd	.LC3(%rip), %xmm4
	movsd	.LC1(%rip), %xmm13
	cvtsi2sdl	%edx, %xmm14
	movapd	%xmm14, %xmm0
	andpd	%xmm15, %xmm0
	ucomisd	%xmm0, %xmm4
	jb	.L415
	imull	$-86400000, %edx, %edx
	pxor	%xmm0, %xmm0
	pxor	%xmm2, %xmm2
	pxor	%xmm1, %xmm1
	addl	%r15d, %edx
	movslq	%edx, %rcx
	sarl	$31, %edx
	imulq	$274877907, %rcx, %rdi
	sarq	$38, %rdi
	subl	%edx, %edi
	movslq	%edi, %rax
	movl	%edi, %esi
	imulq	$-2004318071, %rax, %rax
	sarl	$31, %esi
	shrq	$32, %rax
	addl	%edi, %eax
	sarl	$5, %eax
	subl	%esi, %eax
	imulq	$1172812403, %rcx, %rsi
	imull	$60, %eax, %eax
	imulq	$1250999897, %rcx, %rcx
	sarq	$46, %rsi
	subl	%edx, %esi
	subl	%eax, %edi
	movslq	%esi, %rax
	sarq	$52, %rcx
	cvtsi2sdl	%edi, %xmm2
	movl	%esi, %r8d
	imulq	$-2004318071, %rax, %rax
	sarl	$31, %r8d
	subl	%edx, %ecx
	cvtsi2sdl	%ecx, %xmm0
	shrq	$32, %rax
	addl	%esi, %eax
	sarl	$5, %eax
	subl	%r8d, %eax
	imull	$60, %eax, %eax
	subl	%eax, %esi
	cvtsi2sdl	%esi, %xmm1
	call	_ZN2v88internal12_GLOBAL__N_18MakeTimeEdddd
	movsd	.LC3(%rip), %xmm5
	andpd	%xmm0, %xmm15
	ucomisd	%xmm15, %xmm5
	jb	.L415
	mulsd	.LC26(%rip), %xmm14
	movapd	%xmm14, %xmm13
	addsd	%xmm0, %xmm13
	.p2align 4,,10
	.p2align 3
.L415:
	comisd	.LC29(%rip), %xmm13
	jb	.L436
	movsd	.LC30(%rip), %xmm0
	comisd	%xmm13, %xmm0
	jb	.L436
	cvttsd2siq	%xmm13, %r15
	movq	41240(%r12), %rdi
	xorl	%edx, %edx
	movq	(%rdi), %rax
	movq	%r15, %rsi
	call	*24(%rax)
	pxor	%xmm0, %xmm0
	cltq
	subq	%rax, %r15
	cvtsi2sdq	%r15, %xmm0
.L419:
	call	_ZN2v88internal9DateCache8TimeClipEd@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal6JSDate8SetValueENS0_6HandleIS1_EEd@PLT
	movq	(%rax), %r13
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L439:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L442:
	sarq	$32, %rax
	pxor	%xmm13, %xmm13
	cvtsi2sdl	%eax, %xmm13
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L441:
	movq	%r8, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	jne	.L443
	movq	312(%r12), %r13
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L436:
	movsd	.LC1(%rip), %xmm0
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L417:
	movsd	7(%rax), %xmm3
	jmp	.L418
.L440:
	call	__stack_chk_fail@PLT
.L443:
	movq	0(%r13), %rax
	jmp	.L412
	.cfi_endproc
.LFE21367:
	.size	_ZN2v88internalL41Builtin_Impl_DatePrototypeSetMillisecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL41Builtin_Impl_DatePrototypeSetMillisecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL33Builtin_Impl_DatePrototypeSetDateENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC31:
	.string	"Date.prototype.setDate"
	.section	.text._ZN2v88internalL33Builtin_Impl_DatePrototypeSetDateENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Builtin_Impl_DatePrototypeSetDateENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL33Builtin_Impl_DatePrototypeSetDateENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21358:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L445
.L448:
	leaq	.LC31(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$22, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L491
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L451:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L468
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L468:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L492
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L445:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1066, 11(%rdx)
	jne	.L448
	leaq	-8(%rsi), %rdx
	leaq	88(%r12), %r9
	cmpl	$6, %edi
	cmovge	%rdx, %r9
	movq	(%r9), %rdx
	testb	$1, %dl
	je	.L456
	movq	-1(%rdx), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L493
.L456:
	movq	23(%rax), %rax
	testb	$1, %al
	je	.L494
	movsd	7(%rax), %xmm1
.L458:
	ucomisd	%xmm1, %xmm1
	movq	%r9, -104(%rbp)
	jp	.L459
	cvttsd2siq	%xmm1, %r15
	movq	41240(%r12), %rdi
	movl	$1, %edx
	movq	(%rdi), %rax
	movq	%r15, %rsi
	call	*24(%rax)
	movq	41240(%r12), %rdi
	leaq	-80(%rbp), %r8
	movabsq	$7164004856975580295, %rdx
	movslq	%eax, %r10
	addq	%r15, %r10
	leaq	-86399999(%r10), %rcx
	movq	%r10, -112(%rbp)
	cmovns	%r10, %rcx
	movq	%rcx, %rax
	sarq	$63, %rcx
	imulq	%rdx
	sarq	$25, %rdx
	subq	%rcx, %rdx
	leaq	-84(%rbp), %rcx
	movq	%rdx, %r15
	leaq	-88(%rbp), %rdx
	movl	%r15d, %esi
	call	_ZN2v88internal9DateCache20YearMonthDayFromDaysEiPiS2_S2_@PLT
	movq	-104(%rbp), %r9
	movq	-112(%rbp), %r10
	movq	(%r9), %rax
	testb	$1, %al
	jne	.L461
	sarq	$32, %rax
	pxor	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
.L462:
	movl	-88(%rbp), %eax
	movl	-84(%rbp), %edx
	leal	1000000(%rax), %ecx
	cmpl	$2000000, %ecx
	ja	.L472
	leal	10000000(%rdx), %ecx
	cmpl	$20000000, %ecx
	jbe	.L495
.L472:
	movsd	.LC1(%rip), %xmm1
.L459:
	comisd	.LC29(%rip), %xmm1
	jb	.L488
	movsd	.LC30(%rip), %xmm0
	comisd	%xmm1, %xmm0
	jb	.L488
	cvttsd2siq	%xmm1, %r15
	movq	41240(%r12), %rdi
	xorl	%edx, %edx
	movq	(%rdi), %rax
	movq	%r15, %rsi
	call	*24(%rax)
	pxor	%xmm0, %xmm0
	cltq
	subq	%rax, %r15
	cvtsi2sdq	%r15, %xmm0
.L464:
	call	_ZN2v88internal9DateCache8TimeClipEd@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal6JSDate8SetValueENS0_6HandleIS1_EEd@PLT
	movq	(%rax), %r13
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L491:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L494:
	sarq	$32, %rax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L493:
	movq	%r9, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	jne	.L496
	movq	312(%r12), %r13
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L488:
	movsd	.LC1(%rip), %xmm0
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L461:
	movsd	7(%rax), %xmm2
	jmp	.L462
.L495:
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movq	%r10, -104(%rbp)
	cvtsi2sdl	%edx, %xmm1
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal12_GLOBAL__N_17MakeDayEddd.part.0
	movq	.LC2(%rip), %xmm1
	movsd	.LC3(%rip), %xmm3
	movapd	%xmm0, %xmm2
	andpd	%xmm1, %xmm2
	ucomisd	%xmm2, %xmm3
	jb	.L472
	imull	$-86400000, %r15d, %r15d
	movq	-104(%rbp), %r10
	pxor	%xmm2, %xmm2
	addl	%r10d, %r15d
	cvtsi2sdl	%r15d, %xmm2
	andpd	%xmm2, %xmm1
	ucomisd	%xmm1, %xmm3
	movsd	.LC1(%rip), %xmm1
	jb	.L459
	mulsd	.LC26(%rip), %xmm0
	movapd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	jmp	.L459
.L492:
	call	__stack_chk_fail@PLT
.L496:
	movq	0(%r13), %rax
	jmp	.L456
	.cfi_endproc
.LFE21358:
	.size	_ZN2v88internalL33Builtin_Impl_DatePrototypeSetDateENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL33Builtin_Impl_DatePrototypeSetDateENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL33Builtin_Impl_DatePrototypeSetYearENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC32:
	.string	"Date.prototype.setYear"
	.section	.text._ZN2v88internalL33Builtin_Impl_DatePrototypeSetYearENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Builtin_Impl_DatePrototypeSetYearENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL33Builtin_Impl_DatePrototypeSetYearENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21434:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L498
.L501:
	leaq	.LC32(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$22, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L569
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L504:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L536
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L536:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L570
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L498:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1066, 11(%rdx)
	jne	.L501
	leaq	-8(%rsi), %rdx
	cmpl	$6, %edi
	leaq	88(%r12), %rsi
	cmovge	%rdx, %rsi
	movq	(%rsi), %rdx
	testb	$1, %dl
	jne	.L571
.L507:
	sarq	$32, %rdx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
.L512:
	ucomisd	%xmm0, %xmm0
	jp	.L513
	movq	.LC2(%rip), %xmm2
	movsd	.LC3(%rip), %xmm3
	movapd	%xmm0, %xmm1
	andpd	%xmm2, %xmm1
	ucomisd	%xmm1, %xmm3
	jb	.L538
	pxor	%xmm3, %xmm3
	movapd	%xmm0, %xmm1
	ucomisd	%xmm3, %xmm0
	jnp	.L572
.L550:
	comisd	%xmm3, %xmm0
	movapd	%xmm0, %xmm4
	movsd	.LC4(%rip), %xmm5
	andpd	%xmm2, %xmm4
	jb	.L560
	ucomisd	%xmm4, %xmm5
	jbe	.L514
	cvttsd2siq	%xmm0, %rdx
	pxor	%xmm4, %xmm4
	movsd	.LC5(%rip), %xmm5
	andnpd	%xmm0, %xmm2
	cvtsi2sdq	%rdx, %xmm4
	movapd	%xmm4, %xmm1
	cmpnlesd	%xmm0, %xmm1
	andpd	%xmm5, %xmm1
	subsd	%xmm1, %xmm4
	movapd	%xmm4, %xmm1
	orpd	%xmm2, %xmm1
	.p2align 4,,10
	.p2align 3
.L514:
	comisd	%xmm3, %xmm1
	jb	.L513
.L515:
	movsd	.LC33(%rip), %xmm2
	comisd	%xmm1, %xmm2
	jb	.L513
	addsd	.LC34(%rip), %xmm1
	movapd	%xmm1, %xmm0
.L513:
	movq	23(%rax), %rax
	testb	$1, %al
	je	.L573
	movsd	7(%rax), %xmm1
.L524:
	ucomisd	%xmm1, %xmm1
	jp	.L540
	cvttsd2siq	%xmm1, %r15
	movsd	%xmm0, -112(%rbp)
	movl	$1, %edx
	movq	41240(%r12), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	call	*24(%rax)
	movq	41240(%r12), %rdi
	leaq	-80(%rbp), %r8
	movabsq	$7164004856975580295, %rdx
	movslq	%eax, %r9
	addq	%r15, %r9
	leaq	-86399999(%r9), %rcx
	movq	%r9, -104(%rbp)
	cmovns	%r9, %rcx
	movq	%rcx, %rax
	sarq	$63, %rcx
	imulq	%rdx
	sarq	$25, %rdx
	subq	%rcx, %rdx
	leaq	-84(%rbp), %rcx
	movq	%rdx, %r15
	leaq	-88(%rbp), %rdx
	movl	%r15d, %esi
	imull	$-86400000, %r15d, %r15d
	call	_ZN2v88internal9DateCache20YearMonthDayFromDaysEiPiS2_S2_@PLT
	movq	-104(%rbp), %r9
	pxor	%xmm1, %xmm1
	pxor	%xmm2, %xmm2
	pxor	%xmm7, %xmm7
	cvtsi2sdl	-84(%rbp), %xmm1
	movsd	-112(%rbp), %xmm0
	addl	%r15d, %r9d
	cvtsi2sdl	-80(%rbp), %xmm2
	cvtsi2sdl	%r9d, %xmm7
.L525:
	comisd	.LC35(%rip), %xmm0
	jb	.L546
	movsd	.LC36(%rip), %xmm3
	comisd	%xmm0, %xmm3
	jb	.L546
	comisd	.LC37(%rip), %xmm1
	jb	.L546
	movsd	.LC38(%rip), %xmm3
	comisd	%xmm1, %xmm3
	jb	.L546
	call	_ZN2v88internal12_GLOBAL__N_17MakeDayEddd.part.0
	movq	.LC2(%rip), %xmm2
	movsd	.LC3(%rip), %xmm1
	movapd	%xmm0, %xmm3
	andpd	%xmm2, %xmm3
	ucomisd	%xmm3, %xmm1
	jb	.L546
	andpd	%xmm7, %xmm2
	ucomisd	%xmm2, %xmm1
	movsd	.LC1(%rip), %xmm1
	jb	.L527
	mulsd	.LC26(%rip), %xmm0
	movapd	%xmm0, %xmm1
	addsd	%xmm7, %xmm1
	.p2align 4,,10
	.p2align 3
.L527:
	comisd	.LC29(%rip), %xmm1
	jb	.L566
	movsd	.LC30(%rip), %xmm0
	comisd	%xmm1, %xmm0
	jb	.L566
	cvttsd2siq	%xmm1, %r15
	movq	41240(%r12), %rdi
	xorl	%edx, %edx
	movq	(%rdi), %rax
	movq	%r15, %rsi
	call	*24(%rax)
	pxor	%xmm0, %xmm0
	cltq
	subq	%rax, %r15
	cvtsi2sdq	%r15, %xmm0
.L532:
	call	_ZN2v88internal9DateCache8TimeClipEd@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal6JSDate8SetValueENS0_6HandleIS1_EEd@PLT
	movq	(%rax), %r13
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L569:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L571:
	movq	-1(%rdx), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L508
	xorl	%ecx, %ecx
.L509:
	testb	%cl, %cl
	jne	.L507
	movsd	7(%rdx), %xmm0
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L573:
	sarq	$32, %rax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L508:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	testq	%rax, %rax
	jne	.L510
	movq	312(%r12), %r13
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L572:
	je	.L515
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L546:
	movsd	.LC1(%rip), %xmm1
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L566:
	movsd	.LC1(%rip), %xmm0
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L538:
	movapd	%xmm0, %xmm1
	pxor	%xmm3, %xmm3
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L560:
	ucomisd	%xmm4, %xmm5
	jbe	.L514
	cvttsd2siq	%xmm0, %rdx
	pxor	%xmm4, %xmm4
	movsd	.LC5(%rip), %xmm5
	andnpd	%xmm0, %xmm2
	cvtsi2sdq	%rdx, %xmm4
	cmpnlesd	%xmm4, %xmm1
	andpd	%xmm5, %xmm1
	addsd	%xmm4, %xmm1
	orpd	%xmm2, %xmm1
	jmp	.L514
.L540:
	pxor	%xmm7, %xmm7
	movsd	.LC5(%rip), %xmm2
	movapd	%xmm7, %xmm1
	jmp	.L525
.L570:
	call	__stack_chk_fail@PLT
.L510:
	movq	(%rax), %rdx
	movq	0(%r13), %rax
	movq	%rdx, %rcx
	notq	%rcx
	andl	$1, %ecx
	jmp	.L509
	.cfi_endproc
.LFE21434:
	.size	_ZN2v88internalL33Builtin_Impl_DatePrototypeSetYearENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL33Builtin_Impl_DatePrototypeSetYearENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL39Builtin_Impl_DatePrototypeSetUTCSecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC39:
	.string	"Date.prototype.setUTCSeconds"
	.section	.text._ZN2v88internalL39Builtin_Impl_DatePrototypeSetUTCSecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Builtin_Impl_DatePrototypeSetUTCSecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL39Builtin_Impl_DatePrototypeSetUTCSecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21400:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L575
.L578:
	leaq	.LC39(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$28, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L613
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L581:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L602
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L602:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L614
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L575:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1066, 11(%rdx)
	jne	.L578
	leaq	88(%r12), %rdx
	leaq	-8(%rsi), %rsi
	cmpl	$6, %edi
	movq	%rdi, %r15
	cmovl	%rdx, %rsi
	movq	(%rsi), %rdx
	testb	$1, %dl
	je	.L586
	movq	-1(%rdx), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L615
.L586:
	movq	23(%rax), %rax
	testb	$1, %al
	je	.L616
	movsd	7(%rax), %xmm13
.L588:
	ucomisd	%xmm13, %xmm13
	jp	.L589
	movabsq	$7164004856975580295, %rdx
	cvttsd2siq	%xmm13, %rcx
	testq	%rcx, %rcx
	leaq	-86399999(%rcx), %rdi
	cmovns	%rcx, %rdi
	movq	%rdi, %rax
	sarq	$63, %rdi
	imulq	%rdx
	sarq	$25, %rdx
	subq	%rdi, %rdx
	imull	$-86400000, %edx, %eax
	movq	%rdx, %r8
	addl	%eax, %ecx
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L591
	sarq	$32, %rax
	pxor	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
.L592:
	cmpl	$6, %r15d
	jg	.L593
	movslq	%ecx, %rax
	movl	%ecx, %edx
	pxor	%xmm3, %xmm3
	movl	%ecx, %edi
	imulq	$274877907, %rax, %rax
	sarl	$31, %edx
	sarq	$38, %rax
	subl	%edx, %eax
	imull	$1000, %eax, %eax
	subl	%eax, %edi
	cvtsi2sdl	%edi, %xmm3
.L594:
	pxor	%xmm15, %xmm15
	movq	.LC2(%rip), %xmm14
	movsd	.LC3(%rip), %xmm5
	movsd	.LC1(%rip), %xmm13
	cvtsi2sdl	%r8d, %xmm15
	movapd	%xmm15, %xmm0
	andpd	%xmm14, %xmm0
	ucomisd	%xmm0, %xmm5
	jb	.L589
	movslq	%ecx, %rdx
	sarl	$31, %ecx
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	imulq	$1172812403, %rdx, %rsi
	imulq	$1250999897, %rdx, %rdx
	sarq	$46, %rsi
	subl	%ecx, %esi
	sarq	$52, %rdx
	movslq	%esi, %rax
	movl	%esi, %edi
	subl	%ecx, %edx
	imulq	$-2004318071, %rax, %rax
	sarl	$31, %edi
	cvtsi2sdl	%edx, %xmm0
	shrq	$32, %rax
	addl	%esi, %eax
	sarl	$5, %eax
	subl	%edi, %eax
	imull	$60, %eax, %eax
	subl	%eax, %esi
	cvtsi2sdl	%esi, %xmm1
	call	_ZN2v88internal12_GLOBAL__N_18MakeTimeEdddd
	movsd	.LC3(%rip), %xmm6
	andpd	%xmm0, %xmm14
	ucomisd	%xmm14, %xmm6
	jb	.L589
	mulsd	.LC26(%rip), %xmm15
	movapd	%xmm15, %xmm13
	addsd	%xmm0, %xmm13
	.p2align 4,,10
	.p2align 3
.L589:
	movapd	%xmm13, %xmm0
	call	_ZN2v88internal9DateCache8TimeClipEd@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal6JSDate8SetValueENS0_6HandleIS1_EEd@PLT
	movq	(%rax), %r13
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L613:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L616:
	sarq	$32, %rax
	pxor	%xmm13, %xmm13
	cvtsi2sdl	%eax, %xmm13
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L615:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L617
.L612:
	movq	312(%r12), %r13
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L591:
	movsd	7(%rax), %xmm2
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L593:
	movq	-16(%r13), %rax
	leaq	-16(%r13), %rsi
	testb	$1, %al
	jne	.L618
.L596:
	sarq	$32, %rax
	pxor	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm3
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L618:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L597
	xorl	%edx, %edx
.L598:
	testb	%dl, %dl
	jne	.L596
	movsd	7(%rax), %xmm3
	jmp	.L594
.L597:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r8, -96(%rbp)
	movl	%ecx, -84(%rbp)
	movsd	%xmm2, -104(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movl	-84(%rbp), %ecx
	movq	-96(%rbp), %r8
	testq	%rax, %rax
	movsd	-104(%rbp), %xmm2
	je	.L612
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L598
.L614:
	call	__stack_chk_fail@PLT
.L617:
	movq	0(%r13), %rax
	jmp	.L586
	.cfi_endproc
.LFE21400:
	.size	_ZN2v88internalL39Builtin_Impl_DatePrototypeSetUTCSecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL39Builtin_Impl_DatePrototypeSetUTCSecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL37Builtin_Impl_DatePrototypeSetUTCMonthENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC40:
	.string	"Date.prototype.setUTCMonth"
	.section	.text._ZN2v88internalL37Builtin_Impl_DatePrototypeSetUTCMonthENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Builtin_Impl_DatePrototypeSetUTCMonthENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL37Builtin_Impl_DatePrototypeSetUTCMonthENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21397:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L620
.L623:
	leaq	.LC40(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$26, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L666
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L626:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L650
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L650:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L667
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L620:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1066, 11(%rdx)
	jne	.L623
	leaq	-8(%rsi), %rdx
	leaq	88(%r12), %r10
	cmpl	$6, %edi
	movq	%rdi, %r9
	cmovge	%rdx, %r10
	movq	(%r10), %rdx
	testb	$1, %dl
	je	.L631
	movq	-1(%rdx), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L668
.L631:
	movq	23(%rax), %rax
	testb	$1, %al
	je	.L669
	movsd	7(%rax), %xmm7
.L633:
	ucomisd	%xmm7, %xmm7
	movq	%r9, -112(%rbp)
	movq	%r10, -104(%rbp)
	jp	.L634
	cvttsd2siq	%xmm7, %r15
	movq	41240(%r12), %rdi
	leaq	-80(%rbp), %r8
	movabsq	$7164004856975580295, %rdx
	testq	%r15, %r15
	leaq	-86399999(%r15), %rcx
	cmovns	%r15, %rcx
	movq	%rcx, %rax
	sarq	$63, %rcx
	imulq	%rdx
	sarq	$25, %rdx
	subq	%rcx, %rdx
	leaq	-84(%rbp), %rcx
	movq	%rdx, %rax
	movq	%rdx, -120(%rbp)
	leaq	-88(%rbp), %rdx
	movl	%eax, %esi
	call	_ZN2v88internal9DateCache20YearMonthDayFromDaysEiPiS2_S2_@PLT
	movq	-104(%rbp), %r10
	movq	-112(%rbp), %r9
	movq	(%r10), %rax
	testb	$1, %al
	jne	.L636
	sarq	$32, %rax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
.L637:
	cmpl	$6, %r9d
	jg	.L638
	pxor	%xmm2, %xmm2
	cvtsi2sdl	-80(%rbp), %xmm2
.L639:
	movl	-88(%rbp), %eax
	movsd	.LC1(%rip), %xmm7
	leal	1000000(%rax), %edx
	cmpl	$2000000, %edx
	jbe	.L670
.L634:
	movapd	%xmm7, %xmm0
	call	_ZN2v88internal9DateCache8TimeClipEd@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal6JSDate8SetValueENS0_6HandleIS1_EEd@PLT
	movq	(%rax), %r13
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L666:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L669:
	sarq	$32, %rax
	pxor	%xmm7, %xmm7
	cvtsi2sdl	%eax, %xmm7
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L668:
	movq	%rdi, -104(%rbp)
	xorl	%edx, %edx
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movq	-104(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r10
	jne	.L671
.L665:
	movq	312(%r12), %r13
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L636:
	movsd	7(%rax), %xmm1
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L638:
	movq	-16(%r13), %rax
	leaq	-16(%r13), %rsi
	testb	$1, %al
	jne	.L672
.L641:
	sarq	$32, %rax
	pxor	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L670:
	comisd	.LC37(%rip), %xmm1
	jb	.L634
	movsd	.LC38(%rip), %xmm0
	comisd	%xmm1, %xmm0
	jb	.L634
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal12_GLOBAL__N_17MakeDayEddd.part.0
	movq	.LC2(%rip), %xmm2
	movsd	.LC3(%rip), %xmm3
	movapd	%xmm0, %xmm1
	andpd	%xmm2, %xmm1
	ucomisd	%xmm1, %xmm3
	jb	.L634
	imull	$-86400000, -120(%rbp), %eax
	pxor	%xmm1, %xmm1
	leal	(%rax,%r15), %r11d
	cvtsi2sdl	%r11d, %xmm1
	andpd	%xmm1, %xmm2
	ucomisd	%xmm2, %xmm3
	jb	.L634
	mulsd	.LC26(%rip), %xmm0
	movapd	%xmm0, %xmm7
	addsd	%xmm1, %xmm7
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L672:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L642
	xorl	%edx, %edx
.L643:
	testb	%dl, %dl
	jne	.L641
	movsd	7(%rax), %xmm2
	jmp	.L639
.L642:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movsd	%xmm1, -104(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-104(%rbp), %xmm1
	testq	%rax, %rax
	je	.L665
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L643
.L667:
	call	__stack_chk_fail@PLT
.L671:
	movq	0(%r13), %rax
	jmp	.L631
	.cfi_endproc
.LFE21397:
	.size	_ZN2v88internalL37Builtin_Impl_DatePrototypeSetUTCMonthENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL37Builtin_Impl_DatePrototypeSetUTCMonthENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL36Builtin_Impl_DatePrototypeSetSecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC41:
	.string	"Date.prototype.setSeconds"
	.section	.text._ZN2v88internalL36Builtin_Impl_DatePrototypeSetSecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Builtin_Impl_DatePrototypeSetSecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL36Builtin_Impl_DatePrototypeSetSecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21376:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L674
.L677:
	leaq	.LC41(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$25, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L721
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L680:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L704
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L704:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L722
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L674:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1066, 11(%rdx)
	jne	.L677
	leaq	-8(%rsi), %rdx
	leaq	88(%r12), %r8
	cmpl	$6, %edi
	movq	%rdi, %r15
	cmovge	%rdx, %r8
	movq	(%r8), %rdx
	testb	$1, %dl
	je	.L685
	movq	-1(%rdx), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L723
.L685:
	movq	23(%rax), %rax
	testb	$1, %al
	je	.L724
	movsd	7(%rax), %xmm13
.L687:
	ucomisd	%xmm13, %xmm13
	movq	%r8, -88(%rbp)
	jp	.L688
	cvttsd2siq	%xmm13, %rcx
	movq	41240(%r12), %rdi
	movl	$1, %edx
	movq	(%rdi), %rax
	movq	%rcx, %rsi
	movq	%rcx, -96(%rbp)
	call	*24(%rax)
	movq	-96(%rbp), %rcx
	movq	-88(%rbp), %r8
	movabsq	$7164004856975580295, %rdx
	cltq
	addq	%rax, %rcx
	leaq	-86399999(%rcx), %rsi
	cmovns	%rcx, %rsi
	movq	%rsi, %rax
	sarq	$63, %rsi
	imulq	%rdx
	sarq	$25, %rdx
	subq	%rsi, %rdx
	imull	$-86400000, %edx, %eax
	movq	%rdx, %r9
	addl	%eax, %ecx
	movq	(%r8), %rax
	testb	$1, %al
	jne	.L690
	sarq	$32, %rax
	pxor	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
.L691:
	cmpl	$6, %r15d
	jg	.L692
	movslq	%ecx, %rax
	movl	%ecx, %edx
	pxor	%xmm3, %xmm3
	movl	%ecx, %edi
	imulq	$274877907, %rax, %rax
	sarl	$31, %edx
	sarq	$38, %rax
	subl	%edx, %eax
	imull	$1000, %eax, %eax
	subl	%eax, %edi
	cvtsi2sdl	%edi, %xmm3
.L693:
	pxor	%xmm15, %xmm15
	movq	.LC2(%rip), %xmm14
	movsd	.LC3(%rip), %xmm5
	movsd	.LC1(%rip), %xmm13
	cvtsi2sdl	%r9d, %xmm15
	movapd	%xmm15, %xmm0
	andpd	%xmm14, %xmm0
	ucomisd	%xmm0, %xmm5
	jb	.L688
	movslq	%ecx, %rdx
	sarl	$31, %ecx
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	imulq	$1172812403, %rdx, %rsi
	imulq	$1250999897, %rdx, %rdx
	sarq	$46, %rsi
	subl	%ecx, %esi
	sarq	$52, %rdx
	movslq	%esi, %rax
	movl	%esi, %edi
	subl	%ecx, %edx
	imulq	$-2004318071, %rax, %rax
	sarl	$31, %edi
	cvtsi2sdl	%edx, %xmm0
	shrq	$32, %rax
	addl	%esi, %eax
	sarl	$5, %eax
	subl	%edi, %eax
	imull	$60, %eax, %eax
	subl	%eax, %esi
	cvtsi2sdl	%esi, %xmm1
	call	_ZN2v88internal12_GLOBAL__N_18MakeTimeEdddd
	movsd	.LC3(%rip), %xmm6
	andpd	%xmm0, %xmm14
	ucomisd	%xmm14, %xmm6
	jb	.L688
	mulsd	.LC26(%rip), %xmm15
	movapd	%xmm15, %xmm13
	addsd	%xmm0, %xmm13
	.p2align 4,,10
	.p2align 3
.L688:
	comisd	.LC29(%rip), %xmm13
	jb	.L717
	movsd	.LC30(%rip), %xmm0
	comisd	%xmm13, %xmm0
	jb	.L717
	cvttsd2siq	%xmm13, %r15
	movq	41240(%r12), %rdi
	xorl	%edx, %edx
	movq	(%rdi), %rax
	movq	%r15, %rsi
	call	*24(%rax)
	pxor	%xmm0, %xmm0
	cltq
	subq	%rax, %r15
	cvtsi2sdq	%r15, %xmm0
.L700:
	call	_ZN2v88internal9DateCache8TimeClipEd@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal6JSDate8SetValueENS0_6HandleIS1_EEd@PLT
	movq	(%rax), %r13
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L721:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L724:
	sarq	$32, %rax
	pxor	%xmm13, %xmm13
	cvtsi2sdl	%eax, %xmm13
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L723:
	movq	%r8, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	jne	.L725
.L720:
	movq	312(%r12), %r13
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L717:
	movsd	.LC1(%rip), %xmm0
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L690:
	movsd	7(%rax), %xmm2
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L692:
	movq	-16(%r13), %rax
	leaq	-16(%r13), %rsi
	testb	$1, %al
	jne	.L726
.L695:
	sarq	$32, %rax
	pxor	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm3
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L726:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L696
	xorl	%edx, %edx
.L697:
	testb	%dl, %dl
	jne	.L695
	movsd	7(%rax), %xmm3
	jmp	.L693
.L696:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r9, -96(%rbp)
	movl	%ecx, -88(%rbp)
	movsd	%xmm2, -104(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movl	-88(%rbp), %ecx
	movq	-96(%rbp), %r9
	testq	%rax, %rax
	movsd	-104(%rbp), %xmm2
	je	.L720
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L697
.L722:
	call	__stack_chk_fail@PLT
.L725:
	movq	0(%r13), %rax
	jmp	.L685
	.cfi_endproc
.LFE21376:
	.size	_ZN2v88internalL36Builtin_Impl_DatePrototypeSetSecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL36Builtin_Impl_DatePrototypeSetSecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL34Builtin_Impl_DatePrototypeSetMonthENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC42:
	.string	"Date.prototype.setMonth"
	.section	.text._ZN2v88internalL34Builtin_Impl_DatePrototypeSetMonthENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Builtin_Impl_DatePrototypeSetMonthENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL34Builtin_Impl_DatePrototypeSetMonthENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21373:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L728
.L731:
	leaq	.LC42(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$23, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L783
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L734:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L761
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L761:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L784
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L728:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1066, 11(%rdx)
	jne	.L731
	leaq	-8(%rsi), %rdx
	leaq	88(%r12), %r10
	cmpl	$6, %edi
	movq	%rdi, %r9
	cmovge	%rdx, %r10
	movq	(%r10), %rdx
	testb	$1, %dl
	je	.L739
	movq	-1(%rdx), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L785
.L739:
	movq	23(%rax), %rax
	testb	$1, %al
	je	.L786
	movsd	7(%rax), %xmm7
.L741:
	ucomisd	%xmm7, %xmm7
	movq	%r9, -112(%rbp)
	movq	%r10, -104(%rbp)
	jp	.L742
	cvttsd2siq	%xmm7, %r15
	movq	41240(%r12), %rdi
	movl	$1, %edx
	movq	(%rdi), %rax
	movq	%r15, %rsi
	call	*24(%rax)
	movq	41240(%r12), %rdi
	leaq	-80(%rbp), %r8
	movabsq	$7164004856975580295, %rdx
	movslq	%eax, %r11
	addq	%r11, %r15
	leaq	-86399999(%r15), %rcx
	cmovns	%r15, %rcx
	movq	%rcx, %rax
	sarq	$63, %rcx
	imulq	%rdx
	sarq	$25, %rdx
	subq	%rcx, %rdx
	leaq	-84(%rbp), %rcx
	movq	%rdx, %rax
	movq	%rdx, -120(%rbp)
	leaq	-88(%rbp), %rdx
	movl	%eax, %esi
	call	_ZN2v88internal9DateCache20YearMonthDayFromDaysEiPiS2_S2_@PLT
	movq	-104(%rbp), %r10
	movq	-112(%rbp), %r9
	movq	(%r10), %rax
	testb	$1, %al
	jne	.L744
	sarq	$32, %rax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
.L745:
	cmpl	$6, %r9d
	jg	.L746
	pxor	%xmm2, %xmm2
	cvtsi2sdl	-80(%rbp), %xmm2
.L747:
	movl	-88(%rbp), %eax
	movsd	.LC1(%rip), %xmm7
	leal	1000000(%rax), %edx
	cmpl	$2000000, %edx
	jbe	.L787
.L742:
	comisd	.LC29(%rip), %xmm7
	jb	.L779
	movsd	.LC30(%rip), %xmm0
	comisd	%xmm7, %xmm0
	jb	.L779
	cvttsd2siq	%xmm7, %r15
	movq	41240(%r12), %rdi
	xorl	%edx, %edx
	movq	(%rdi), %rax
	movq	%r15, %rsi
	call	*24(%rax)
	pxor	%xmm0, %xmm0
	cltq
	subq	%rax, %r15
	cvtsi2sdq	%r15, %xmm0
.L757:
	call	_ZN2v88internal9DateCache8TimeClipEd@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal6JSDate8SetValueENS0_6HandleIS1_EEd@PLT
	movq	(%rax), %r13
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L783:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L786:
	sarq	$32, %rax
	pxor	%xmm7, %xmm7
	cvtsi2sdl	%eax, %xmm7
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L785:
	movq	%rdi, -104(%rbp)
	xorl	%edx, %edx
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movq	-104(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r10
	jne	.L788
.L782:
	movq	312(%r12), %r13
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L779:
	movsd	.LC1(%rip), %xmm0
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L744:
	movsd	7(%rax), %xmm1
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L746:
	movq	-16(%r13), %rax
	leaq	-16(%r13), %rsi
	testb	$1, %al
	jne	.L789
.L749:
	sarq	$32, %rax
	pxor	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L787:
	comisd	.LC37(%rip), %xmm1
	jb	.L742
	movsd	.LC38(%rip), %xmm0
	comisd	%xmm1, %xmm0
	jb	.L742
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal12_GLOBAL__N_17MakeDayEddd.part.0
	movq	.LC2(%rip), %xmm2
	movsd	.LC3(%rip), %xmm3
	movapd	%xmm0, %xmm1
	andpd	%xmm2, %xmm1
	ucomisd	%xmm1, %xmm3
	jb	.L742
	imull	$-86400000, -120(%rbp), %eax
	pxor	%xmm1, %xmm1
	addl	%eax, %r15d
	cvtsi2sdl	%r15d, %xmm1
	andpd	%xmm1, %xmm2
	ucomisd	%xmm2, %xmm3
	jb	.L742
	mulsd	.LC26(%rip), %xmm0
	movapd	%xmm0, %xmm7
	addsd	%xmm1, %xmm7
	jmp	.L742
	.p2align 4,,10
	.p2align 3
.L789:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L750
	xorl	%edx, %edx
.L751:
	testb	%dl, %dl
	jne	.L749
	movsd	7(%rax), %xmm2
	jmp	.L747
.L750:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movsd	%xmm1, -104(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-104(%rbp), %xmm1
	testq	%rax, %rax
	je	.L782
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L751
.L784:
	call	__stack_chk_fail@PLT
.L788:
	movq	0(%r13), %rax
	jmp	.L739
	.cfi_endproc
.LFE21373:
	.size	_ZN2v88internalL34Builtin_Impl_DatePrototypeSetMonthENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL34Builtin_Impl_DatePrototypeSetMonthENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL40Builtin_Impl_DatePrototypeSetUTCFullYearENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC43:
	.string	"Date.prototype.setUTCFullYear"
	.section	.text._ZN2v88internalL40Builtin_Impl_DatePrototypeSetUTCFullYearENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Builtin_Impl_DatePrototypeSetUTCFullYearENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL40Builtin_Impl_DatePrototypeSetUTCFullYearENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21385:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L791
.L794:
	leaq	.LC43(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$29, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L855
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r14
.L797:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L830
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L830:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L856
	addq	$88, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L791:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1066, 11(%rdx)
	jne	.L794
	leaq	-8(%rsi), %rdx
	cmpl	$6, %edi
	leaq	88(%r12), %rsi
	cmovge	%rdx, %rsi
	leal	-5(%rdi), %ecx
	movl	%ecx, -104(%rbp)
	movq	(%rsi), %rdx
	testb	$1, %dl
	jne	.L857
.L800:
	sarq	$32, %rdx
	pxor	%xmm3, %xmm3
	cvtsi2sdl	%edx, %xmm3
.L805:
	movq	23(%rax), %rax
	testb	$1, %al
	je	.L858
	movsd	7(%rax), %xmm0
.L807:
	ucomisd	%xmm0, %xmm0
	jp	.L832
	cvttsd2siq	%xmm0, %rcx
	movq	41240(%r12), %rdi
	movsd	%xmm3, -112(%rbp)
	movabsq	$7164004856975580295, %rdx
	testq	%rcx, %rcx
	leaq	-86399999(%rcx), %r8
	cmovns	%rcx, %r8
	movq	%r8, %rax
	sarq	$63, %r8
	imulq	%rdx
	movq	%rdx, %rsi
	leaq	-88(%rbp), %rdx
	sarq	$25, %rsi
	subq	%r8, %rsi
	leaq	-80(%rbp), %r8
	imull	$-86400000, %esi, %r14d
	addl	%ecx, %r14d
	leaq	-84(%rbp), %rcx
	call	_ZN2v88internal9DateCache20YearMonthDayFromDaysEiPiS2_S2_@PLT
	pxor	%xmm1, %xmm1
	pxor	%xmm2, %xmm2
	movsd	-112(%rbp), %xmm3
	cvtsi2sdl	-84(%rbp), %xmm1
	cvtsi2sdl	-80(%rbp), %xmm2
.L808:
	cmpl	$1, -104(%rbp)
	jg	.L859
.L810:
	comisd	.LC35(%rip), %xmm3
	jb	.L838
	movsd	.LC36(%rip), %xmm0
	comisd	%xmm3, %xmm0
	jb	.L838
	comisd	.LC37(%rip), %xmm1
	jb	.L838
	movsd	.LC38(%rip), %xmm0
	comisd	%xmm1, %xmm0
	jb	.L838
	movapd	%xmm3, %xmm0
	call	_ZN2v88internal12_GLOBAL__N_17MakeDayEddd.part.0
	movsd	.LC3(%rip), %xmm3
	movapd	%xmm0, %xmm2
	movq	.LC2(%rip), %xmm0
	movapd	%xmm2, %xmm1
	andpd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm3
	jb	.L838
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%r14d, %xmm1
	andpd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm3
	movsd	.LC1(%rip), %xmm0
	jb	.L824
	mulsd	.LC26(%rip), %xmm2
	movapd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	.p2align 4,,10
	.p2align 3
.L824:
	call	_ZN2v88internal9DateCache8TimeClipEd@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal6JSDate8SetValueENS0_6HandleIS1_EEd@PLT
	movq	(%rax), %r14
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L855:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L857:
	movq	-1(%rdx), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L801
	xorl	%ecx, %ecx
.L802:
	testb	%cl, %cl
	jne	.L800
	movsd	7(%rdx), %xmm3
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L858:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L801:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	testq	%rax, %rax
	jne	.L803
.L854:
	movq	312(%r12), %r14
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L838:
	movsd	.LC1(%rip), %xmm0
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L859:
	movq	-16(%r15), %rax
	leaq	-16(%r15), %rsi
	testb	$1, %al
	jne	.L860
.L812:
	sarq	$32, %rax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
.L817:
	cmpl	$2, -104(%rbp)
	je	.L810
	movq	-24(%r15), %rax
	leaq	-24(%r15), %rsi
	testb	$1, %al
	jne	.L861
.L819:
	sarq	$32, %rax
	pxor	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L860:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L813
	xorl	%edx, %edx
.L814:
	testb	%dl, %dl
	jne	.L812
	movsd	7(%rax), %xmm1
	jmp	.L817
.L832:
	movsd	.LC5(%rip), %xmm2
	xorl	%r14d, %r14d
	pxor	%xmm1, %xmm1
	jmp	.L808
.L861:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L820
	xorl	%edx, %edx
.L821:
	testb	%dl, %dl
	jne	.L819
	movsd	7(%rax), %xmm2
	jmp	.L810
.L813:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movsd	%xmm3, -120(%rbp)
	movsd	%xmm2, -112(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-112(%rbp), %xmm2
	movsd	-120(%rbp), %xmm3
	testq	%rax, %rax
	je	.L854
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L820:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movsd	%xmm3, -112(%rbp)
	movsd	%xmm1, -104(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-104(%rbp), %xmm1
	movsd	-112(%rbp), %xmm3
	testq	%rax, %rax
	je	.L854
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L821
.L856:
	call	__stack_chk_fail@PLT
.L803:
	movq	(%rax), %rdx
	movq	(%r15), %rax
	movq	%rdx, %rcx
	notq	%rcx
	andl	$1, %ecx
	jmp	.L802
	.cfi_endproc
.LFE21385:
	.size	_ZN2v88internalL40Builtin_Impl_DatePrototypeSetUTCFullYearENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL40Builtin_Impl_DatePrototypeSetUTCFullYearENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL37Builtin_Impl_DatePrototypeSetFullYearENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC44:
	.string	"Date.prototype.setFullYear"
	.section	.text._ZN2v88internalL37Builtin_Impl_DatePrototypeSetFullYearENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Builtin_Impl_DatePrototypeSetFullYearENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL37Builtin_Impl_DatePrototypeSetFullYearENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21361:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L863
.L866:
	leaq	.LC44(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$26, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L936
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r14
.L869:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L905
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L905:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L937
	addq	$88, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L863:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1066, 11(%rdx)
	jne	.L866
	leaq	-8(%rsi), %rdx
	cmpl	$6, %edi
	leaq	88(%r12), %rsi
	cmovge	%rdx, %rsi
	leal	-5(%rdi), %ecx
	movl	%ecx, -104(%rbp)
	movq	(%rsi), %rdx
	testb	$1, %dl
	jne	.L938
.L872:
	sarq	$32, %rdx
	pxor	%xmm3, %xmm3
	cvtsi2sdl	%edx, %xmm3
.L877:
	movq	23(%rax), %rax
	testb	$1, %al
	je	.L939
	movsd	7(%rax), %xmm0
.L879:
	ucomisd	%xmm0, %xmm0
	jp	.L907
	cvttsd2siq	%xmm0, %r14
	movsd	%xmm3, -112(%rbp)
	movl	$1, %edx
	movq	41240(%r12), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	call	*24(%rax)
	movq	41240(%r12), %rdi
	leaq	-80(%rbp), %r8
	movabsq	$7164004856975580295, %rdx
	cltq
	addq	%rax, %r14
	leaq	-86399999(%r14), %rcx
	cmovns	%r14, %rcx
	movq	%rcx, %rax
	sarq	$63, %rcx
	imulq	%rdx
	movq	%rdx, %rsi
	leaq	-88(%rbp), %rdx
	sarq	$25, %rsi
	subq	%rcx, %rsi
	leaq	-84(%rbp), %rcx
	imull	$-86400000, %esi, %eax
	addl	%eax, %r14d
	call	_ZN2v88internal9DateCache20YearMonthDayFromDaysEiPiS2_S2_@PLT
	pxor	%xmm1, %xmm1
	pxor	%xmm2, %xmm2
	movsd	-112(%rbp), %xmm3
	cvtsi2sdl	-84(%rbp), %xmm1
	cvtsi2sdl	-80(%rbp), %xmm2
.L880:
	cmpl	$1, -104(%rbp)
	jg	.L940
.L882:
	comisd	.LC35(%rip), %xmm3
	jb	.L913
	movsd	.LC36(%rip), %xmm0
	comisd	%xmm3, %xmm0
	jb	.L913
	comisd	.LC37(%rip), %xmm1
	jb	.L913
	movsd	.LC38(%rip), %xmm0
	comisd	%xmm1, %xmm0
	jb	.L913
	movapd	%xmm3, %xmm0
	call	_ZN2v88internal12_GLOBAL__N_17MakeDayEddd.part.0
	movq	.LC2(%rip), %xmm1
	movsd	.LC3(%rip), %xmm3
	movapd	%xmm0, %xmm2
	andpd	%xmm1, %xmm2
	ucomisd	%xmm2, %xmm3
	jb	.L913
	pxor	%xmm2, %xmm2
	cvtsi2sdl	%r14d, %xmm2
	andpd	%xmm2, %xmm1
	ucomisd	%xmm1, %xmm3
	movsd	.LC1(%rip), %xmm1
	jb	.L896
	mulsd	.LC26(%rip), %xmm0
	movapd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	.p2align 4,,10
	.p2align 3
.L896:
	comisd	.LC29(%rip), %xmm1
	jb	.L932
	movsd	.LC30(%rip), %xmm0
	comisd	%xmm1, %xmm0
	jb	.L932
	cvttsd2siq	%xmm1, %r14
	movq	41240(%r12), %rdi
	xorl	%edx, %edx
	movq	(%rdi), %rax
	movq	%r14, %rsi
	call	*24(%rax)
	pxor	%xmm0, %xmm0
	cltq
	subq	%rax, %r14
	cvtsi2sdq	%r14, %xmm0
.L901:
	call	_ZN2v88internal9DateCache8TimeClipEd@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal6JSDate8SetValueENS0_6HandleIS1_EEd@PLT
	movq	(%rax), %r14
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L936:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L938:
	movq	-1(%rdx), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L873
	xorl	%ecx, %ecx
.L874:
	testb	%cl, %cl
	jne	.L872
	movsd	7(%rdx), %xmm3
	jmp	.L877
	.p2align 4,,10
	.p2align 3
.L939:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L873:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	testq	%rax, %rax
	jne	.L875
.L935:
	movq	312(%r12), %r14
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L913:
	movsd	.LC1(%rip), %xmm1
	jmp	.L896
	.p2align 4,,10
	.p2align 3
.L932:
	movsd	.LC1(%rip), %xmm0
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L940:
	movq	-16(%r15), %rax
	leaq	-16(%r15), %rsi
	testb	$1, %al
	jne	.L941
.L884:
	sarq	$32, %rax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
.L889:
	cmpl	$2, -104(%rbp)
	je	.L882
	movq	-24(%r15), %rax
	leaq	-24(%r15), %rsi
	testb	$1, %al
	jne	.L942
.L891:
	sarq	$32, %rax
	pxor	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	jmp	.L882
	.p2align 4,,10
	.p2align 3
.L941:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L885
	xorl	%edx, %edx
.L886:
	testb	%dl, %dl
	jne	.L884
	movsd	7(%rax), %xmm1
	jmp	.L889
.L907:
	movsd	.LC5(%rip), %xmm2
	xorl	%r14d, %r14d
	pxor	%xmm1, %xmm1
	jmp	.L880
.L942:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L892
	xorl	%edx, %edx
.L893:
	testb	%dl, %dl
	jne	.L891
	movsd	7(%rax), %xmm2
	jmp	.L882
.L885:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movsd	%xmm3, -120(%rbp)
	movsd	%xmm2, -112(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-112(%rbp), %xmm2
	movsd	-120(%rbp), %xmm3
	testq	%rax, %rax
	je	.L935
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L886
	.p2align 4,,10
	.p2align 3
.L892:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movsd	%xmm3, -112(%rbp)
	movsd	%xmm1, -104(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-104(%rbp), %xmm1
	movsd	-112(%rbp), %xmm3
	testq	%rax, %rax
	je	.L935
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L893
.L937:
	call	__stack_chk_fail@PLT
.L875:
	movq	(%rax), %rdx
	movq	(%r15), %rax
	movq	%rdx, %rcx
	notq	%rcx
	andl	$1, %ecx
	jmp	.L874
	.cfi_endproc
.LFE21361:
	.size	_ZN2v88internalL37Builtin_Impl_DatePrototypeSetFullYearENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL37Builtin_Impl_DatePrototypeSetFullYearENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL39Builtin_Impl_DatePrototypeSetUTCMinutesENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC45:
	.string	"Date.prototype.setUTCMinutes"
	.section	.text._ZN2v88internalL39Builtin_Impl_DatePrototypeSetUTCMinutesENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Builtin_Impl_DatePrototypeSetUTCMinutesENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL39Builtin_Impl_DatePrototypeSetUTCMinutesENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21394:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L944
.L947:
	leaq	.LC45(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$28, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L992
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L950:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L978
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L978:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L993
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L944:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1066, 11(%rdx)
	jne	.L947
	leaq	88(%r12), %rdx
	leaq	-8(%rsi), %rsi
	cmpl	$6, %edi
	cmovl	%rdx, %rsi
	leal	-5(%rdi), %r15d
	movq	(%rsi), %rdx
	testb	$1, %dl
	je	.L955
	movq	-1(%rdx), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L994
.L955:
	movq	23(%rax), %rax
	testb	$1, %al
	je	.L995
	movsd	7(%rax), %xmm13
.L957:
	ucomisd	%xmm13, %xmm13
	jp	.L958
	movabsq	$7164004856975580295, %rdx
	cvttsd2siq	%xmm13, %rcx
	testq	%rcx, %rcx
	leaq	-86399999(%rcx), %rdi
	cmovns	%rcx, %rdi
	movq	%rdi, %rax
	sarq	$63, %rdi
	imulq	%rdx
	sarq	$25, %rdx
	subq	%rdi, %rdx
	imull	$-86400000, %edx, %eax
	movq	%rdx, %r8
	addl	%eax, %ecx
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L960
	sarq	$32, %rax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
.L961:
	movslq	%ecx, %r9
	movl	%ecx, %edx
	pxor	%xmm3, %xmm3
	movl	%ecx, %edi
	imulq	$274877907, %r9, %rax
	sarl	$31, %edx
	sarq	$38, %rax
	subl	%edx, %eax
	imull	$1000, %eax, %edx
	subl	%edx, %edi
	cvtsi2sdl	%edi, %xmm3
	cmpl	$1, %r15d
	jg	.L962
	movslq	%eax, %rdx
	movl	%eax, %esi
	pxor	%xmm2, %xmm2
	imulq	$-2004318071, %rdx, %rdx
	sarl	$31, %esi
	shrq	$32, %rdx
	addl	%eax, %edx
	sarl	$5, %edx
	subl	%esi, %edx
	imull	$60, %edx, %edx
	subl	%edx, %eax
	cvtsi2sdl	%eax, %xmm2
.L963:
	pxor	%xmm15, %xmm15
	movq	.LC2(%rip), %xmm14
	movsd	.LC3(%rip), %xmm5
	movsd	.LC1(%rip), %xmm13
	cvtsi2sdl	%r8d, %xmm15
	movapd	%xmm15, %xmm0
	andpd	%xmm14, %xmm0
	ucomisd	%xmm0, %xmm5
	jb	.L958
	imulq	$1250999897, %r9, %r9
	sarl	$31, %ecx
	pxor	%xmm0, %xmm0
	sarq	$52, %r9
	subl	%ecx, %r9d
	cvtsi2sdl	%r9d, %xmm0
	call	_ZN2v88internal12_GLOBAL__N_18MakeTimeEdddd
	movsd	.LC3(%rip), %xmm6
	andpd	%xmm0, %xmm14
	ucomisd	%xmm14, %xmm6
	jb	.L958
	mulsd	.LC26(%rip), %xmm15
	movapd	%xmm15, %xmm13
	addsd	%xmm0, %xmm13
	.p2align 4,,10
	.p2align 3
.L958:
	movapd	%xmm13, %xmm0
	call	_ZN2v88internal9DateCache8TimeClipEd@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal6JSDate8SetValueENS0_6HandleIS1_EEd@PLT
	movq	(%rax), %r13
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L992:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L995:
	sarq	$32, %rax
	pxor	%xmm13, %xmm13
	cvtsi2sdl	%eax, %xmm13
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L994:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L996
.L991:
	movq	312(%r12), %r13
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L960:
	movsd	7(%rax), %xmm1
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L962:
	movq	-16(%r13), %rax
	leaq	-16(%r13), %rsi
	testb	$1, %al
	jne	.L997
.L965:
	sarq	$32, %rax
	pxor	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
.L970:
	cmpl	$2, %r15d
	je	.L963
	movq	-24(%r13), %rax
	leaq	-24(%r13), %rsi
	testb	$1, %al
	jne	.L998
.L972:
	sarq	$32, %rax
	pxor	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm3
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L997:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L966
	xorl	%edx, %edx
.L967:
	testb	%dl, %dl
	jne	.L965
	movsd	7(%rax), %xmm2
	jmp	.L970
.L998:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L973
	xorl	%edx, %edx
.L974:
	testb	%dl, %dl
	jne	.L972
	movsd	7(%rax), %xmm3
	jmp	.L963
.L966:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r9, -120(%rbp)
	movq	%r8, -104(%rbp)
	movl	%ecx, -92(%rbp)
	movsd	%xmm1, -112(%rbp)
	movsd	%xmm3, -88(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-88(%rbp), %xmm3
	movl	-92(%rbp), %ecx
	testq	%rax, %rax
	movq	-104(%rbp), %r8
	movsd	-112(%rbp), %xmm1
	movq	-120(%rbp), %r9
	je	.L991
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L973:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r9, -120(%rbp)
	movq	%r8, -104(%rbp)
	movl	%ecx, -92(%rbp)
	movsd	%xmm1, -112(%rbp)
	movsd	%xmm2, -88(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-88(%rbp), %xmm2
	movl	-92(%rbp), %ecx
	testq	%rax, %rax
	movq	-104(%rbp), %r8
	movsd	-112(%rbp), %xmm1
	movq	-120(%rbp), %r9
	je	.L991
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L974
.L993:
	call	__stack_chk_fail@PLT
.L996:
	movq	0(%r13), %rax
	jmp	.L955
	.cfi_endproc
.LFE21394:
	.size	_ZN2v88internalL39Builtin_Impl_DatePrototypeSetUTCMinutesENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL39Builtin_Impl_DatePrototypeSetUTCMinutesENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL36Builtin_Impl_DatePrototypeSetMinutesENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC46:
	.string	"Date.prototype.setMinutes"
	.section	.text._ZN2v88internalL36Builtin_Impl_DatePrototypeSetMinutesENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Builtin_Impl_DatePrototypeSetMinutesENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL36Builtin_Impl_DatePrototypeSetMinutesENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21370:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L1000
.L1003:
	leaq	.LC46(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$25, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1057
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L1006:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1037
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1037:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1058
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1000:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1066, 11(%rdx)
	jne	.L1003
	leaq	-8(%rsi), %rdx
	leaq	88(%r12), %r8
	cmpl	$6, %edi
	cmovge	%rdx, %r8
	leal	-5(%rdi), %r15d
	movq	(%r8), %rdx
	testb	$1, %dl
	je	.L1011
	movq	-1(%rdx), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1059
.L1011:
	movq	23(%rax), %rax
	testb	$1, %al
	je	.L1060
	movsd	7(%rax), %xmm13
.L1013:
	ucomisd	%xmm13, %xmm13
	movq	%r8, -88(%rbp)
	jp	.L1014
	cvttsd2siq	%xmm13, %rcx
	movq	41240(%r12), %rdi
	movl	$1, %edx
	movq	(%rdi), %rax
	movq	%rcx, %rsi
	movq	%rcx, -96(%rbp)
	call	*24(%rax)
	movq	-96(%rbp), %rcx
	movq	-88(%rbp), %r8
	movabsq	$7164004856975580295, %rdx
	cltq
	addq	%rax, %rcx
	leaq	-86399999(%rcx), %rsi
	cmovns	%rcx, %rsi
	movq	%rsi, %rax
	sarq	$63, %rsi
	imulq	%rdx
	sarq	$25, %rdx
	subq	%rsi, %rdx
	imull	$-86400000, %edx, %eax
	movq	%rdx, %r9
	addl	%eax, %ecx
	movq	(%r8), %rax
	testb	$1, %al
	jne	.L1016
	sarq	$32, %rax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
.L1017:
	movslq	%ecx, %r8
	movl	%ecx, %edx
	pxor	%xmm3, %xmm3
	movl	%ecx, %edi
	imulq	$274877907, %r8, %rax
	sarl	$31, %edx
	sarq	$38, %rax
	subl	%edx, %eax
	imull	$1000, %eax, %edx
	subl	%edx, %edi
	cvtsi2sdl	%edi, %xmm3
	cmpl	$1, %r15d
	jg	.L1018
	movslq	%eax, %rdx
	movl	%eax, %esi
	pxor	%xmm2, %xmm2
	imulq	$-2004318071, %rdx, %rdx
	sarl	$31, %esi
	shrq	$32, %rdx
	addl	%eax, %edx
	sarl	$5, %edx
	subl	%esi, %edx
	imull	$60, %edx, %edx
	subl	%edx, %eax
	cvtsi2sdl	%eax, %xmm2
.L1019:
	pxor	%xmm15, %xmm15
	movq	.LC2(%rip), %xmm14
	movsd	.LC3(%rip), %xmm5
	movsd	.LC1(%rip), %xmm13
	cvtsi2sdl	%r9d, %xmm15
	movapd	%xmm15, %xmm0
	andpd	%xmm14, %xmm0
	ucomisd	%xmm0, %xmm5
	jb	.L1014
	imulq	$1250999897, %r8, %r8
	sarl	$31, %ecx
	pxor	%xmm0, %xmm0
	sarq	$52, %r8
	subl	%ecx, %r8d
	cvtsi2sdl	%r8d, %xmm0
	call	_ZN2v88internal12_GLOBAL__N_18MakeTimeEdddd
	movsd	.LC3(%rip), %xmm6
	andpd	%xmm0, %xmm14
	ucomisd	%xmm14, %xmm6
	jb	.L1014
	mulsd	.LC26(%rip), %xmm15
	movapd	%xmm15, %xmm13
	addsd	%xmm0, %xmm13
	.p2align 4,,10
	.p2align 3
.L1014:
	comisd	.LC29(%rip), %xmm13
	jb	.L1053
	movsd	.LC30(%rip), %xmm0
	comisd	%xmm13, %xmm0
	jb	.L1053
	cvttsd2siq	%xmm13, %r15
	movq	41240(%r12), %rdi
	xorl	%edx, %edx
	movq	(%rdi), %rax
	movq	%r15, %rsi
	call	*24(%rax)
	pxor	%xmm0, %xmm0
	cltq
	subq	%rax, %r15
	cvtsi2sdq	%r15, %xmm0
.L1033:
	call	_ZN2v88internal9DateCache8TimeClipEd@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal6JSDate8SetValueENS0_6HandleIS1_EEd@PLT
	movq	(%rax), %r13
	jmp	.L1006
	.p2align 4,,10
	.p2align 3
.L1057:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1060:
	sarq	$32, %rax
	pxor	%xmm13, %xmm13
	cvtsi2sdl	%eax, %xmm13
	jmp	.L1013
	.p2align 4,,10
	.p2align 3
.L1059:
	movq	%r8, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	jne	.L1061
.L1056:
	movq	312(%r12), %r13
	jmp	.L1006
	.p2align 4,,10
	.p2align 3
.L1053:
	movsd	.LC1(%rip), %xmm0
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1016:
	movsd	7(%rax), %xmm1
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1018:
	movq	-16(%r13), %rax
	leaq	-16(%r13), %rsi
	testb	$1, %al
	jne	.L1062
.L1021:
	sarq	$32, %rax
	pxor	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
.L1026:
	cmpl	$2, %r15d
	je	.L1019
	movq	-24(%r13), %rax
	leaq	-24(%r13), %rsi
	testb	$1, %al
	jne	.L1063
.L1028:
	sarq	$32, %rax
	pxor	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm3
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1062:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1022
	xorl	%edx, %edx
.L1023:
	testb	%dl, %dl
	jne	.L1021
	movsd	7(%rax), %xmm2
	jmp	.L1026
.L1063:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1029
	xorl	%edx, %edx
.L1030:
	testb	%dl, %dl
	jne	.L1028
	movsd	7(%rax), %xmm3
	jmp	.L1019
.L1022:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r8, -120(%rbp)
	movq	%r9, -104(%rbp)
	movl	%ecx, -96(%rbp)
	movsd	%xmm1, -112(%rbp)
	movsd	%xmm3, -88(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-88(%rbp), %xmm3
	movl	-96(%rbp), %ecx
	testq	%rax, %rax
	movq	-104(%rbp), %r9
	movsd	-112(%rbp), %xmm1
	movq	-120(%rbp), %r8
	je	.L1056
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L1023
	.p2align 4,,10
	.p2align 3
.L1029:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r8, -120(%rbp)
	movq	%r9, -104(%rbp)
	movl	%ecx, -96(%rbp)
	movsd	%xmm1, -112(%rbp)
	movsd	%xmm2, -88(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-88(%rbp), %xmm2
	movl	-96(%rbp), %ecx
	testq	%rax, %rax
	movq	-104(%rbp), %r9
	movsd	-112(%rbp), %xmm1
	movq	-120(%rbp), %r8
	je	.L1056
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L1030
.L1058:
	call	__stack_chk_fail@PLT
.L1061:
	movq	0(%r13), %rax
	jmp	.L1011
	.cfi_endproc
.LFE21370:
	.size	_ZN2v88internalL36Builtin_Impl_DatePrototypeSetMinutesENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL36Builtin_Impl_DatePrototypeSetMinutesENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL37Builtin_Impl_DatePrototypeSetUTCHoursENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC47:
	.string	"Date.prototype.setUTCHours"
	.section	.text._ZN2v88internalL37Builtin_Impl_DatePrototypeSetUTCHoursENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Builtin_Impl_DatePrototypeSetUTCHoursENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL37Builtin_Impl_DatePrototypeSetUTCHoursENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21388:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L1065
.L1068:
	leaq	.LC47(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$26, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1122
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L1071:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1107
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1107:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1123
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1065:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1066, 11(%rdx)
	jne	.L1068
	leaq	-8(%rsi), %rdx
	cmpl	$6, %edi
	leaq	88(%r12), %rsi
	cmovge	%rdx, %rsi
	leal	-5(%rdi), %r15d
	movq	(%rsi), %rdx
	testb	$1, %dl
	jne	.L1124
.L1074:
	sarq	$32, %rdx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
.L1079:
	movq	23(%rax), %rax
	testb	$1, %al
	je	.L1125
	movsd	7(%rax), %xmm13
.L1081:
	ucomisd	%xmm13, %xmm13
	jp	.L1082
	cvttsd2siq	%xmm13, %rcx
	pxor	%xmm2, %xmm2
	pxor	%xmm3, %xmm3
	movabsq	$7164004856975580295, %rdx
	testq	%rcx, %rcx
	leaq	-86399999(%rcx), %rsi
	cmovns	%rcx, %rsi
	movq	%rsi, %rax
	sarq	$63, %rsi
	imulq	%rdx
	sarq	$25, %rdx
	subq	%rsi, %rdx
	imull	$-86400000, %edx, %eax
	movq	%rdx, %r8
	addl	%eax, %ecx
	movslq	%ecx, %rdx
	movl	%ecx, %edi
	imulq	$274877907, %rdx, %rsi
	sarl	$31, %edi
	sarq	$38, %rsi
	subl	%edi, %esi
	movslq	%esi, %rax
	movl	%esi, %r9d
	movl	%esi, %r10d
	imulq	$-2004318071, %rax, %rax
	sarl	$31, %r9d
	shrq	$32, %rax
	addl	%esi, %eax
	imull	$1000, %esi, %esi
	sarl	$5, %eax
	subl	%r9d, %eax
	imull	$60, %eax, %eax
	subl	%esi, %ecx
	cvtsi2sdl	%ecx, %xmm3
	subl	%eax, %r10d
	cvtsi2sdl	%r10d, %xmm2
	cmpl	$1, %r15d
	jg	.L1084
	imulq	$1172812403, %rdx, %rdx
	pxor	%xmm1, %xmm1
	sarq	$46, %rdx
	subl	%edi, %edx
	movslq	%edx, %rax
	movl	%edx, %ecx
	imulq	$-2004318071, %rax, %rax
	sarl	$31, %ecx
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$5, %eax
	subl	%ecx, %eax
	imull	$60, %eax, %eax
	subl	%eax, %edx
	cvtsi2sdl	%edx, %xmm1
.L1085:
	pxor	%xmm15, %xmm15
	movq	.LC2(%rip), %xmm14
	movsd	.LC3(%rip), %xmm6
	movsd	.LC1(%rip), %xmm13
	cvtsi2sdl	%r8d, %xmm15
	movapd	%xmm15, %xmm5
	andpd	%xmm14, %xmm5
	ucomisd	%xmm5, %xmm6
	jb	.L1082
	call	_ZN2v88internal12_GLOBAL__N_18MakeTimeEdddd
	movsd	.LC3(%rip), %xmm7
	andpd	%xmm0, %xmm14
	ucomisd	%xmm14, %xmm7
	jb	.L1082
	mulsd	.LC26(%rip), %xmm15
	movapd	%xmm15, %xmm13
	addsd	%xmm0, %xmm13
	.p2align 4,,10
	.p2align 3
.L1082:
	movapd	%xmm13, %xmm0
	call	_ZN2v88internal9DateCache8TimeClipEd@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal6JSDate8SetValueENS0_6HandleIS1_EEd@PLT
	movq	(%rax), %r13
	jmp	.L1071
	.p2align 4,,10
	.p2align 3
.L1122:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1124:
	movq	-1(%rdx), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L1075
	xorl	%ecx, %ecx
.L1076:
	testb	%cl, %cl
	jne	.L1074
	movsd	7(%rdx), %xmm0
	jmp	.L1079
	.p2align 4,,10
	.p2align 3
.L1125:
	sarq	$32, %rax
	pxor	%xmm13, %xmm13
	cvtsi2sdl	%eax, %xmm13
	jmp	.L1081
	.p2align 4,,10
	.p2align 3
.L1075:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	testq	%rax, %rax
	jne	.L1077
.L1121:
	movq	312(%r12), %r13
	jmp	.L1071
	.p2align 4,,10
	.p2align 3
.L1084:
	movq	-16(%r13), %rax
	leaq	-16(%r13), %rsi
	testb	$1, %al
	jne	.L1126
.L1087:
	sarq	$32, %rax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
.L1092:
	cmpl	$2, %r15d
	je	.L1085
	movq	-24(%r13), %rax
	leaq	-24(%r13), %rsi
	testb	$1, %al
	jne	.L1127
.L1094:
	sarq	$32, %rax
	pxor	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
.L1099:
	cmpl	$3, %r15d
	je	.L1085
	movq	-32(%r13), %rax
	leaq	-32(%r13), %rsi
	testb	$1, %al
	jne	.L1128
.L1101:
	sarq	$32, %rax
	pxor	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm3
	jmp	.L1085
	.p2align 4,,10
	.p2align 3
.L1126:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1088
	xorl	%edx, %edx
.L1089:
	testb	%dl, %dl
	jne	.L1087
	movsd	7(%rax), %xmm1
	jmp	.L1092
.L1127:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1095
	xorl	%edx, %edx
.L1096:
	testb	%dl, %dl
	jne	.L1094
	movsd	7(%rax), %xmm2
	jmp	.L1099
.L1088:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r8, -112(%rbp)
	movsd	%xmm0, -104(%rbp)
	movsd	%xmm3, -96(%rbp)
	movsd	%xmm2, -88(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-88(%rbp), %xmm2
	movsd	-96(%rbp), %xmm3
	testq	%rax, %rax
	movsd	-104(%rbp), %xmm0
	movq	-112(%rbp), %r8
	je	.L1121
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L1089
	.p2align 4,,10
	.p2align 3
.L1128:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1102
	xorl	%edx, %edx
.L1103:
	testb	%dl, %dl
	jne	.L1101
	movsd	7(%rax), %xmm3
	jmp	.L1085
.L1095:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r8, -112(%rbp)
	movsd	%xmm0, -104(%rbp)
	movsd	%xmm3, -96(%rbp)
	movsd	%xmm1, -88(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-88(%rbp), %xmm1
	movsd	-96(%rbp), %xmm3
	testq	%rax, %rax
	movsd	-104(%rbp), %xmm0
	movq	-112(%rbp), %r8
	je	.L1121
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L1096
	.p2align 4,,10
	.p2align 3
.L1102:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r8, -112(%rbp)
	movsd	%xmm0, -104(%rbp)
	movsd	%xmm2, -96(%rbp)
	movsd	%xmm1, -88(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-88(%rbp), %xmm1
	movsd	-96(%rbp), %xmm2
	testq	%rax, %rax
	movsd	-104(%rbp), %xmm0
	movq	-112(%rbp), %r8
	je	.L1121
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L1103
.L1123:
	call	__stack_chk_fail@PLT
.L1077:
	movq	(%rax), %rdx
	movq	0(%r13), %rax
	movq	%rdx, %rcx
	notq	%rcx
	andl	$1, %ecx
	jmp	.L1076
	.cfi_endproc
.LFE21388:
	.size	_ZN2v88internalL37Builtin_Impl_DatePrototypeSetUTCHoursENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL37Builtin_Impl_DatePrototypeSetUTCHoursENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL34Builtin_Impl_DatePrototypeSetHoursENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC48:
	.string	"Date.prototype.setHours"
	.section	.text._ZN2v88internalL34Builtin_Impl_DatePrototypeSetHoursENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Builtin_Impl_DatePrototypeSetHoursENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL34Builtin_Impl_DatePrototypeSetHoursENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21364:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L1130
.L1133:
	leaq	.LC48(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$23, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1196
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L1136:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1175
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1175:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1197
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1130:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1066, 11(%rdx)
	jne	.L1133
	leaq	-8(%rsi), %rdx
	cmpl	$6, %edi
	leaq	88(%r12), %rsi
	cmovge	%rdx, %rsi
	leal	-5(%rdi), %r15d
	movq	(%rsi), %rdx
	testb	$1, %dl
	jne	.L1198
.L1139:
	sarq	$32, %rdx
	pxor	%xmm6, %xmm6
	cvtsi2sdl	%edx, %xmm6
	movsd	%xmm6, -96(%rbp)
.L1144:
	movq	23(%rax), %rax
	testb	$1, %al
	je	.L1199
	movsd	7(%rax), %xmm13
.L1146:
	ucomisd	%xmm13, %xmm13
	jp	.L1147
	cvttsd2siq	%xmm13, %rcx
	movq	41240(%r12), %rdi
	movl	$1, %edx
	movq	(%rdi), %rax
	movq	%rcx, %rsi
	movq	%rcx, -88(%rbp)
	call	*24(%rax)
	movq	-88(%rbp), %rcx
	pxor	%xmm2, %xmm2
	pxor	%xmm3, %xmm3
	movabsq	$7164004856975580295, %rdx
	cltq
	addq	%rax, %rcx
	leaq	-86399999(%rcx), %rsi
	cmovns	%rcx, %rsi
	movq	%rsi, %rax
	sarq	$63, %rsi
	imulq	%rdx
	sarq	$25, %rdx
	subq	%rsi, %rdx
	imull	$-86400000, %edx, %eax
	movq	%rdx, %r8
	addl	%eax, %ecx
	movslq	%ecx, %rdx
	movl	%ecx, %edi
	imulq	$274877907, %rdx, %rsi
	sarl	$31, %edi
	sarq	$38, %rsi
	subl	%edi, %esi
	movslq	%esi, %rax
	movl	%esi, %r9d
	movl	%esi, %r10d
	imulq	$-2004318071, %rax, %rax
	sarl	$31, %r9d
	shrq	$32, %rax
	addl	%esi, %eax
	imull	$1000, %esi, %esi
	sarl	$5, %eax
	subl	%r9d, %eax
	imull	$60, %eax, %eax
	subl	%esi, %ecx
	cvtsi2sdl	%ecx, %xmm3
	subl	%eax, %r10d
	cvtsi2sdl	%r10d, %xmm2
	cmpl	$1, %r15d
	jg	.L1149
	imulq	$1172812403, %rdx, %rdx
	pxor	%xmm1, %xmm1
	sarq	$46, %rdx
	subl	%edi, %edx
	movslq	%edx, %rax
	movl	%edx, %ecx
	imulq	$-2004318071, %rax, %rax
	sarl	$31, %ecx
	shrq	$32, %rax
	addl	%edx, %eax
	sarl	$5, %eax
	subl	%ecx, %eax
	imull	$60, %eax, %eax
	subl	%eax, %edx
	cvtsi2sdl	%edx, %xmm1
.L1150:
	pxor	%xmm15, %xmm15
	movq	.LC2(%rip), %xmm14
	movsd	.LC3(%rip), %xmm5
	movsd	.LC1(%rip), %xmm13
	cvtsi2sdl	%r8d, %xmm15
	movapd	%xmm15, %xmm0
	andpd	%xmm14, %xmm0
	ucomisd	%xmm0, %xmm5
	jb	.L1147
	movsd	-96(%rbp), %xmm0
	call	_ZN2v88internal12_GLOBAL__N_18MakeTimeEdddd
	movsd	.LC3(%rip), %xmm6
	andpd	%xmm0, %xmm14
	ucomisd	%xmm14, %xmm6
	jb	.L1147
	mulsd	.LC26(%rip), %xmm15
	movapd	%xmm15, %xmm13
	addsd	%xmm0, %xmm13
	.p2align 4,,10
	.p2align 3
.L1147:
	comisd	.LC29(%rip), %xmm13
	jb	.L1192
	movsd	.LC30(%rip), %xmm0
	comisd	%xmm13, %xmm0
	jb	.L1192
	cvttsd2siq	%xmm13, %r15
	movq	41240(%r12), %rdi
	xorl	%edx, %edx
	movq	(%rdi), %rax
	movq	%r15, %rsi
	call	*24(%rax)
	pxor	%xmm0, %xmm0
	cltq
	subq	%rax, %r15
	cvtsi2sdq	%r15, %xmm0
.L1171:
	call	_ZN2v88internal9DateCache8TimeClipEd@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal6JSDate8SetValueENS0_6HandleIS1_EEd@PLT
	movq	(%rax), %r13
	jmp	.L1136
	.p2align 4,,10
	.p2align 3
.L1196:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1198:
	movq	-1(%rdx), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L1140
	xorl	%ecx, %ecx
.L1141:
	testb	%cl, %cl
	jne	.L1139
	movsd	7(%rdx), %xmm7
	movsd	%xmm7, -96(%rbp)
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1199:
	sarq	$32, %rax
	pxor	%xmm13, %xmm13
	cvtsi2sdl	%eax, %xmm13
	jmp	.L1146
	.p2align 4,,10
	.p2align 3
.L1140:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	testq	%rax, %rax
	jne	.L1142
.L1195:
	movq	312(%r12), %r13
	jmp	.L1136
	.p2align 4,,10
	.p2align 3
.L1192:
	movsd	.LC1(%rip), %xmm0
	jmp	.L1171
	.p2align 4,,10
	.p2align 3
.L1149:
	movq	-16(%r13), %rax
	leaq	-16(%r13), %rsi
	testb	$1, %al
	jne	.L1200
.L1152:
	sarq	$32, %rax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
.L1157:
	cmpl	$2, %r15d
	je	.L1150
	movq	-24(%r13), %rax
	leaq	-24(%r13), %rsi
	testb	$1, %al
	jne	.L1201
.L1159:
	sarq	$32, %rax
	pxor	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
.L1164:
	cmpl	$3, %r15d
	je	.L1150
	movq	-32(%r13), %rax
	leaq	-32(%r13), %rsi
	testb	$1, %al
	jne	.L1202
.L1166:
	sarq	$32, %rax
	pxor	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm3
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1200:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1153
	xorl	%edx, %edx
.L1154:
	testb	%dl, %dl
	jne	.L1152
	movsd	7(%rax), %xmm1
	jmp	.L1157
.L1201:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1160
	xorl	%edx, %edx
.L1161:
	testb	%dl, %dl
	jne	.L1159
	movsd	7(%rax), %xmm2
	jmp	.L1164
.L1153:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r8, -112(%rbp)
	movsd	%xmm3, -104(%rbp)
	movsd	%xmm2, -88(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-88(%rbp), %xmm2
	movsd	-104(%rbp), %xmm3
	testq	%rax, %rax
	movq	-112(%rbp), %r8
	je	.L1195
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L1154
	.p2align 4,,10
	.p2align 3
.L1202:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1167
	xorl	%edx, %edx
.L1168:
	testb	%dl, %dl
	jne	.L1166
	movsd	7(%rax), %xmm3
	jmp	.L1150
.L1160:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r8, -112(%rbp)
	movsd	%xmm3, -104(%rbp)
	movsd	%xmm1, -88(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-88(%rbp), %xmm1
	movsd	-104(%rbp), %xmm3
	testq	%rax, %rax
	movq	-112(%rbp), %r8
	je	.L1195
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L1161
	.p2align 4,,10
	.p2align 3
.L1167:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r8, -112(%rbp)
	movsd	%xmm2, -104(%rbp)
	movsd	%xmm1, -88(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-88(%rbp), %xmm1
	movsd	-104(%rbp), %xmm2
	testq	%rax, %rax
	movq	-112(%rbp), %r8
	je	.L1195
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L1168
.L1197:
	call	__stack_chk_fail@PLT
.L1142:
	movq	(%rax), %rdx
	movq	0(%r13), %rax
	movq	%rdx, %rcx
	notq	%rcx
	andl	$1, %ecx
	jmp	.L1141
	.cfi_endproc
.LFE21364:
	.size	_ZN2v88internalL34Builtin_Impl_DatePrototypeSetHoursENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL34Builtin_Impl_DatePrototypeSetHoursENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internalL20Builtin_Impl_DateUTCENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL20Builtin_Impl_DateUTCENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL20Builtin_Impl_DateUTCENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21355:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leal	-5(%rdi), %ebx
	subq	$56, %rsp
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %r13
	testl	%ebx, %ebx
	jg	.L1302
	pxor	%xmm7, %xmm7
	movsd	.LC5(%rip), %xmm2
	movsd	.LC1(%rip), %xmm0
	movapd	%xmm7, %xmm8
	movapd	%xmm7, %xmm6
	movapd	%xmm7, %xmm5
	movapd	%xmm7, %xmm1
.L1204:
	comisd	.LC35(%rip), %xmm0
	movsd	%xmm8, -72(%rbp)
	movsd	%xmm6, -64(%rbp)
	movsd	%xmm5, -56(%rbp)
	jnb	.L1303
.L1283:
	movsd	.LC1(%rip), %xmm0
.L1263:
	call	_ZN2v88internal9DateCache8TimeClipEd@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rax), %r15
.L1210:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L1298
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1298:
	addq	$56, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1303:
	.cfi_restore_state
	movsd	.LC36(%rip), %xmm3
	comisd	%xmm0, %xmm3
	jb	.L1283
	comisd	.LC37(%rip), %xmm1
	jb	.L1283
	movsd	.LC38(%rip), %xmm3
	comisd	%xmm1, %xmm3
	jb	.L1283
	call	_ZN2v88internal12_GLOBAL__N_17MakeDayEddd.part.0
	movsd	-72(%rbp), %xmm8
	movsd	-64(%rbp), %xmm6
	movsd	-56(%rbp), %xmm5
	movapd	%xmm0, %xmm13
	movapd	%xmm7, %xmm3
	movapd	%xmm8, %xmm2
	movapd	%xmm6, %xmm1
	movapd	%xmm5, %xmm0
	call	_ZN2v88internal12_GLOBAL__N_18MakeTimeEdddd
	movq	.LC2(%rip), %xmm1
	movapd	%xmm0, %xmm2
	movapd	%xmm13, %xmm3
	movsd	.LC3(%rip), %xmm0
	andpd	%xmm1, %xmm3
	ucomisd	%xmm3, %xmm0
	jb	.L1283
	andpd	%xmm2, %xmm1
	ucomisd	%xmm1, %xmm0
	movsd	.LC1(%rip), %xmm0
	jb	.L1263
	movsd	.LC26(%rip), %xmm0
	mulsd	%xmm13, %xmm0
	addsd	%xmm2, %xmm0
	jmp	.L1263
	.p2align 4,,10
	.p2align 3
.L1302:
	movq	%rsi, %r15
	leaq	-8(%rsi), %rsi
	movq	-8(%r15), %rax
	testb	$1, %al
	jne	.L1304
.L1206:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	cmpl	$1, %ebx
	jne	.L1305
.L1271:
	pxor	%xmm7, %xmm7
	movsd	.LC5(%rip), %xmm2
	movapd	%xmm7, %xmm8
	movapd	%xmm7, %xmm6
	movapd	%xmm7, %xmm5
	movapd	%xmm7, %xmm1
.L1213:
	ucomisd	%xmm0, %xmm0
	jp	.L1204
	movq	.LC2(%rip), %xmm4
	movapd	%xmm0, %xmm3
	movsd	.LC3(%rip), %xmm9
	andpd	%xmm4, %xmm3
	ucomisd	%xmm3, %xmm9
	jb	.L1277
	pxor	%xmm9, %xmm9
	movapd	%xmm0, %xmm3
	ucomisd	%xmm9, %xmm0
	jnp	.L1306
.L1285:
	comisd	%xmm9, %xmm0
	movapd	%xmm0, %xmm10
	movsd	.LC4(%rip), %xmm11
	andpd	%xmm4, %xmm10
	jb	.L1293
	ucomisd	%xmm10, %xmm11
	jbe	.L1254
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm10, %xmm10
	andnpd	%xmm0, %xmm4
	movsd	.LC5(%rip), %xmm11
	cvtsi2sdq	%rax, %xmm10
	movapd	%xmm10, %xmm3
	cmpnlesd	%xmm0, %xmm3
	andpd	%xmm11, %xmm3
	subsd	%xmm3, %xmm10
	movapd	%xmm10, %xmm3
	orpd	%xmm4, %xmm3
	.p2align 4,,10
	.p2align 3
.L1254:
	comisd	%xmm9, %xmm3
	jb	.L1204
.L1255:
	movsd	.LC33(%rip), %xmm4
	comisd	%xmm3, %xmm4
	jb	.L1204
	addsd	.LC34(%rip), %xmm3
	movapd	%xmm3, %xmm0
	jmp	.L1204
	.p2align 4,,10
	.p2align 3
.L1304:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1207
	xorl	%edx, %edx
.L1208:
	testb	%dl, %dl
	jne	.L1206
	movsd	7(%rax), %xmm0
	cmpl	$1, %ebx
	je	.L1271
.L1305:
	movq	-16(%r15), %rax
	leaq	-16(%r15), %rsi
	testb	$1, %al
	jne	.L1307
.L1215:
	sarq	$32, %rax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
.L1220:
	cmpl	$2, %ebx
	je	.L1272
	movq	-24(%r15), %rax
	leaq	-24(%r15), %rsi
	testb	$1, %al
	jne	.L1308
.L1222:
	sarq	$32, %rax
	pxor	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
.L1227:
	cmpl	$3, %ebx
	je	.L1273
	movq	-32(%r15), %rax
	leaq	-32(%r15), %rsi
	testb	$1, %al
	jne	.L1309
.L1229:
	sarq	$32, %rax
	pxor	%xmm5, %xmm5
	cvtsi2sdl	%eax, %xmm5
.L1234:
	cmpl	$4, %ebx
	je	.L1274
	movq	-40(%r15), %rax
	leaq	-40(%r15), %rsi
	testb	$1, %al
	jne	.L1310
.L1236:
	sarq	$32, %rax
	pxor	%xmm6, %xmm6
	cvtsi2sdl	%eax, %xmm6
.L1241:
	cmpl	$5, %ebx
	je	.L1275
	movq	-48(%r15), %rax
	leaq	-48(%r15), %rsi
	testb	$1, %al
	jne	.L1311
.L1243:
	sarq	$32, %rax
	pxor	%xmm8, %xmm8
	cvtsi2sdl	%eax, %xmm8
.L1248:
	pxor	%xmm7, %xmm7
	cmpl	$6, %ebx
	je	.L1213
	movq	-56(%r15), %rax
	leaq	-56(%r15), %rsi
	testb	$1, %al
	je	.L1252
	movq	-1(%rax), %rax
	cmpw	$65, 11(%rax)
	jne	.L1312
.L1252:
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L1253
	sarq	$32, %rax
	pxor	%xmm7, %xmm7
	cvtsi2sdl	%eax, %xmm7
	jmp	.L1213
	.p2align 4,,10
	.p2align 3
.L1207:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	testq	%rax, %rax
	jne	.L1209
.L1301:
	movq	312(%r12), %r15
	jmp	.L1210
	.p2align 4,,10
	.p2align 3
.L1277:
	movapd	%xmm0, %xmm3
	pxor	%xmm9, %xmm9
	jmp	.L1254
	.p2align 4,,10
	.p2align 3
.L1306:
	je	.L1255
	jmp	.L1285
	.p2align 4,,10
	.p2align 3
.L1293:
	ucomisd	%xmm10, %xmm11
	jbe	.L1254
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm10, %xmm10
	andnpd	%xmm0, %xmm4
	movsd	.LC5(%rip), %xmm11
	cvtsi2sdq	%rax, %xmm10
	cmpnlesd	%xmm10, %xmm3
	andpd	%xmm11, %xmm3
	addsd	%xmm10, %xmm3
	orpd	%xmm4, %xmm3
	jmp	.L1254
	.p2align 4,,10
	.p2align 3
.L1307:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1216
	xorl	%edx, %edx
.L1217:
	testb	%dl, %dl
	jne	.L1215
	movsd	7(%rax), %xmm1
	jmp	.L1220
	.p2align 4,,10
	.p2align 3
.L1272:
	pxor	%xmm7, %xmm7
	movsd	.LC5(%rip), %xmm2
	movapd	%xmm7, %xmm8
	movapd	%xmm7, %xmm6
	movapd	%xmm7, %xmm5
	jmp	.L1213
	.p2align 4,,10
	.p2align 3
.L1308:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1223
	xorl	%edx, %edx
.L1224:
	testb	%dl, %dl
	jne	.L1222
	movsd	7(%rax), %xmm2
	jmp	.L1227
	.p2align 4,,10
	.p2align 3
.L1216:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movsd	%xmm0, -56(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-56(%rbp), %xmm0
	testq	%rax, %rax
	je	.L1301
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L1217
	.p2align 4,,10
	.p2align 3
.L1273:
	pxor	%xmm7, %xmm7
	movapd	%xmm7, %xmm8
	movapd	%xmm7, %xmm6
	movapd	%xmm7, %xmm5
	jmp	.L1213
	.p2align 4,,10
	.p2align 3
.L1309:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1230
	xorl	%edx, %edx
.L1231:
	testb	%dl, %dl
	jne	.L1229
	movsd	7(%rax), %xmm5
	jmp	.L1234
	.p2align 4,,10
	.p2align 3
.L1223:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movsd	%xmm1, -64(%rbp)
	movsd	%xmm0, -56(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-56(%rbp), %xmm0
	movsd	-64(%rbp), %xmm1
	testq	%rax, %rax
	je	.L1301
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L1224
	.p2align 4,,10
	.p2align 3
.L1274:
	pxor	%xmm7, %xmm7
	movapd	%xmm7, %xmm8
	movapd	%xmm7, %xmm6
	jmp	.L1213
.L1310:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1237
	xorl	%edx, %edx
.L1238:
	testb	%dl, %dl
	jne	.L1236
	movsd	7(%rax), %xmm6
	jmp	.L1241
.L1230:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movsd	%xmm2, -72(%rbp)
	movsd	%xmm1, -64(%rbp)
	movsd	%xmm0, -56(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-56(%rbp), %xmm0
	movsd	-64(%rbp), %xmm1
	testq	%rax, %rax
	movsd	-72(%rbp), %xmm2
	je	.L1301
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1275:
	pxor	%xmm7, %xmm7
	movapd	%xmm7, %xmm8
	jmp	.L1213
.L1311:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1244
	xorl	%edx, %edx
.L1245:
	testb	%dl, %dl
	jne	.L1243
	movsd	7(%rax), %xmm8
	jmp	.L1248
.L1237:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movsd	%xmm5, -80(%rbp)
	movsd	%xmm2, -72(%rbp)
	movsd	%xmm1, -64(%rbp)
	movsd	%xmm0, -56(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-56(%rbp), %xmm0
	movsd	-64(%rbp), %xmm1
	testq	%rax, %rax
	movsd	-72(%rbp), %xmm2
	movsd	-80(%rbp), %xmm5
	je	.L1301
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L1238
.L1244:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movsd	%xmm6, -88(%rbp)
	movsd	%xmm5, -80(%rbp)
	movsd	%xmm2, -72(%rbp)
	movsd	%xmm1, -64(%rbp)
	movsd	%xmm0, -56(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-56(%rbp), %xmm0
	movsd	-64(%rbp), %xmm1
	testq	%rax, %rax
	movsd	-72(%rbp), %xmm2
	movsd	-80(%rbp), %xmm5
	movsd	-88(%rbp), %xmm6
	je	.L1301
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L1245
.L1253:
	movsd	7(%rax), %xmm7
	jmp	.L1213
.L1312:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movsd	%xmm8, -96(%rbp)
	movsd	%xmm6, -88(%rbp)
	movsd	%xmm5, -80(%rbp)
	movsd	%xmm2, -72(%rbp)
	movsd	%xmm1, -64(%rbp)
	movsd	%xmm0, -56(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-56(%rbp), %xmm0
	movsd	-64(%rbp), %xmm1
	testq	%rax, %rax
	movsd	-72(%rbp), %xmm2
	movsd	-80(%rbp), %xmm5
	movq	%rax, %rsi
	movsd	-88(%rbp), %xmm6
	movsd	-96(%rbp), %xmm8
	je	.L1301
	jmp	.L1252
.L1209:
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L1208
	.cfi_endproc
.LFE21355:
	.size	_ZN2v88internalL20Builtin_Impl_DateUTCENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL20Builtin_Impl_DateUTCENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L1319
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L1322
.L1313:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1319:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L1322:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1313
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL26Builtin_Impl_Stats_DateNowEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC49:
	.string	"disabled-by-default-v8.runtime"
	.section	.rodata._ZN2v88internalL26Builtin_Impl_Stats_DateNowEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC50:
	.string	"V8.Builtin_DateNow"
	.section	.text._ZN2v88internalL26Builtin_Impl_Stats_DateNowEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL26Builtin_Impl_Stats_DateNowEiPmPNS0_7IsolateE, @function
_ZN2v88internalL26Builtin_Impl_Stats_DateNowEiPmPNS0_7IsolateE:
.LFB21347:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1353
.L1324:
	movq	_ZZN2v88internalL26Builtin_Impl_Stats_DateNowEiPmPNS0_7IsolateEE28trace_event_unique_atomic294(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1354
.L1326:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1355
.L1328:
	addl	$1, 41104(%r12)
	movq	%r12, %rdi
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	call	_ZN2v88internal6JSDate16CurrentTimeValueEPNS0_7IsolateE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rax), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1332
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1332:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1356
.L1323:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1357
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1354:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1358
.L1327:
	movq	%rbx, _ZZN2v88internalL26Builtin_Impl_Stats_DateNowEiPmPNS0_7IsolateEE28trace_event_unique_atomic294(%rip)
	jmp	.L1326
	.p2align 4,,10
	.p2align 3
.L1355:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1359
.L1329:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1330
	movq	(%rdi), %rax
	call	*8(%rax)
.L1330:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1331
	movq	(%rdi), %rax
	call	*8(%rax)
.L1331:
	leaq	.LC50(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1328
	.p2align 4,,10
	.p2align 3
.L1356:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1323
	.p2align 4,,10
	.p2align 3
.L1353:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$739, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1324
	.p2align 4,,10
	.p2align 3
.L1359:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC50(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L1329
	.p2align 4,,10
	.p2align 3
.L1358:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1327
.L1357:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21347:
	.size	_ZN2v88internalL26Builtin_Impl_Stats_DateNowEiPmPNS0_7IsolateE, .-_ZN2v88internalL26Builtin_Impl_Stats_DateNowEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL26Builtin_Impl_Stats_DateUTCEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC51:
	.string	"V8.Builtin_DateUTC"
	.section	.text._ZN2v88internalL26Builtin_Impl_Stats_DateUTCEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL26Builtin_Impl_Stats_DateUTCEiPmPNS0_7IsolateE, @function
_ZN2v88internalL26Builtin_Impl_Stats_DateUTCEiPmPNS0_7IsolateE:
.LFB21353:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1389
.L1361:
	movq	_ZZN2v88internalL26Builtin_Impl_Stats_DateUTCEiPmPNS0_7IsolateEE28trace_event_unique_atomic310(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1390
.L1363:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1391
.L1365:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL20Builtin_Impl_DateUTCENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1392
.L1369:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1393
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1390:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1394
.L1364:
	movq	%rbx, _ZZN2v88internalL26Builtin_Impl_Stats_DateUTCEiPmPNS0_7IsolateEE28trace_event_unique_atomic310(%rip)
	jmp	.L1363
	.p2align 4,,10
	.p2align 3
.L1391:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1395
.L1366:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1367
	movq	(%rdi), %rax
	call	*8(%rax)
.L1367:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1368
	movq	(%rdi), %rax
	call	*8(%rax)
.L1368:
	leaq	.LC51(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1365
	.p2align 4,,10
	.p2align 3
.L1392:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1369
	.p2align 4,,10
	.p2align 3
.L1389:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$762, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1361
	.p2align 4,,10
	.p2align 3
.L1395:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC51(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1366
	.p2align 4,,10
	.p2align 3
.L1394:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1364
.L1393:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21353:
	.size	_ZN2v88internalL26Builtin_Impl_Stats_DateUTCEiPmPNS0_7IsolateE, .-_ZN2v88internalL26Builtin_Impl_Stats_DateUTCEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetTimeEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC52:
	.string	"V8.Builtin_DatePrototypeSetTime"
	.section	.text._ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetTimeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetTimeEiPmPNS0_7IsolateE, @function
_ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetTimeEiPmPNS0_7IsolateE:
.LFB21377:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1425
.L1397:
	movq	_ZZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetTimeEiPmPNS0_7IsolateEE28trace_event_unique_atomic581(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1426
.L1399:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1427
.L1401:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL33Builtin_Impl_DatePrototypeSetTimeENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1428
.L1405:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1429
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1426:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1430
.L1400:
	movq	%rbx, _ZZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetTimeEiPmPNS0_7IsolateEE28trace_event_unique_atomic581(%rip)
	jmp	.L1399
	.p2align 4,,10
	.p2align 3
.L1427:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1431
.L1402:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1403
	movq	(%rdi), %rax
	call	*8(%rax)
.L1403:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1404
	movq	(%rdi), %rax
	call	*8(%rax)
.L1404:
	leaq	.LC52(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1401
	.p2align 4,,10
	.p2align 3
.L1428:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1405
	.p2align 4,,10
	.p2align 3
.L1425:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$748, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1397
	.p2align 4,,10
	.p2align 3
.L1431:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC52(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1402
	.p2align 4,,10
	.p2align 3
.L1430:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1400
.L1429:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21377:
	.size	_ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetTimeEiPmPNS0_7IsolateE, .-_ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetTimeEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL50Builtin_Impl_Stats_DatePrototypeToLocaleDateStringEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC53:
	.string	"V8.Builtin_DatePrototypeToLocaleDateString"
	.section	.text._ZN2v88internalL50Builtin_Impl_Stats_DatePrototypeToLocaleDateStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL50Builtin_Impl_Stats_DatePrototypeToLocaleDateStringEiPmPNS0_7IsolateE, @function
_ZN2v88internalL50Builtin_Impl_Stats_DatePrototypeToLocaleDateStringEiPmPNS0_7IsolateE:
.LFB21417:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1461
.L1433:
	movq	_ZZN2v88internalL50Builtin_Impl_Stats_DatePrototypeToLocaleDateStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic852(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1462
.L1435:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1463
.L1437:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL44Builtin_Impl_DatePrototypeToLocaleDateStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1464
.L1441:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1465
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1462:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1466
.L1436:
	movq	%rbx, _ZZN2v88internalL50Builtin_Impl_Stats_DatePrototypeToLocaleDateStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic852(%rip)
	jmp	.L1435
	.p2align 4,,10
	.p2align 3
.L1463:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1467
.L1438:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1439
	movq	(%rdi), %rax
	call	*8(%rax)
.L1439:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1440
	movq	(%rdi), %rax
	call	*8(%rax)
.L1440:
	leaq	.LC53(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1437
	.p2align 4,,10
	.p2align 3
.L1464:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1441
	.p2align 4,,10
	.p2align 3
.L1461:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$865, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L1467:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC53(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1438
	.p2align 4,,10
	.p2align 3
.L1466:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1436
.L1465:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21417:
	.size	_ZN2v88internalL50Builtin_Impl_Stats_DatePrototypeToLocaleDateStringEiPmPNS0_7IsolateE, .-_ZN2v88internalL50Builtin_Impl_Stats_DatePrototypeToLocaleDateStringEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL46Builtin_Impl_Stats_DatePrototypeToLocaleStringEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC54:
	.string	"V8.Builtin_DatePrototypeToLocaleString"
	.section	.text._ZN2v88internalL46Builtin_Impl_Stats_DatePrototypeToLocaleStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL46Builtin_Impl_Stats_DatePrototypeToLocaleStringEiPmPNS0_7IsolateE, @function
_ZN2v88internalL46Builtin_Impl_Stats_DatePrototypeToLocaleStringEiPmPNS0_7IsolateE:
.LFB21420:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1497
.L1469:
	movq	_ZZN2v88internalL46Builtin_Impl_Stats_DatePrototypeToLocaleStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic870(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1498
.L1471:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1499
.L1473:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL40Builtin_Impl_DatePrototypeToLocaleStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1500
.L1477:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1501
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1498:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1502
.L1472:
	movq	%rbx, _ZZN2v88internalL46Builtin_Impl_Stats_DatePrototypeToLocaleStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic870(%rip)
	jmp	.L1471
	.p2align 4,,10
	.p2align 3
.L1499:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1503
.L1474:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1475
	movq	(%rdi), %rax
	call	*8(%rax)
.L1475:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1476
	movq	(%rdi), %rax
	call	*8(%rax)
.L1476:
	leaq	.LC54(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1473
	.p2align 4,,10
	.p2align 3
.L1500:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1477
	.p2align 4,,10
	.p2align 3
.L1497:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$866, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1503:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC54(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1474
	.p2align 4,,10
	.p2align 3
.L1502:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1472
.L1501:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21420:
	.size	_ZN2v88internalL46Builtin_Impl_Stats_DatePrototypeToLocaleStringEiPmPNS0_7IsolateE, .-_ZN2v88internalL46Builtin_Impl_Stats_DatePrototypeToLocaleStringEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL50Builtin_Impl_Stats_DatePrototypeToLocaleTimeStringEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC55:
	.string	"V8.Builtin_DatePrototypeToLocaleTimeString"
	.section	.text._ZN2v88internalL50Builtin_Impl_Stats_DatePrototypeToLocaleTimeStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL50Builtin_Impl_Stats_DatePrototypeToLocaleTimeStringEiPmPNS0_7IsolateE, @function
_ZN2v88internalL50Builtin_Impl_Stats_DatePrototypeToLocaleTimeStringEiPmPNS0_7IsolateE:
.LFB21423:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1533
.L1505:
	movq	_ZZN2v88internalL50Builtin_Impl_Stats_DatePrototypeToLocaleTimeStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic888(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1534
.L1507:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1535
.L1509:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL44Builtin_Impl_DatePrototypeToLocaleTimeStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1536
.L1513:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1537
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1534:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1538
.L1508:
	movq	%rbx, _ZZN2v88internalL50Builtin_Impl_Stats_DatePrototypeToLocaleTimeStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic888(%rip)
	jmp	.L1507
	.p2align 4,,10
	.p2align 3
.L1535:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1539
.L1510:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1511
	movq	(%rdi), %rax
	call	*8(%rax)
.L1511:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1512
	movq	(%rdi), %rax
	call	*8(%rax)
.L1512:
	leaq	.LC55(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1509
	.p2align 4,,10
	.p2align 3
.L1536:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1513
	.p2align 4,,10
	.p2align 3
.L1533:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$867, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1505
	.p2align 4,,10
	.p2align 3
.L1539:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC55(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1510
	.p2align 4,,10
	.p2align 3
.L1538:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1508
.L1537:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21423:
	.size	_ZN2v88internalL50Builtin_Impl_Stats_DatePrototypeToLocaleTimeStringEiPmPNS0_7IsolateE, .-_ZN2v88internalL50Builtin_Impl_Stats_DatePrototypeToLocaleTimeStringEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL38Builtin_Impl_Stats_DatePrototypeToJsonEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC56:
	.string	"V8.Builtin_DatePrototypeToJson"
	.section	.text._ZN2v88internalL38Builtin_Impl_Stats_DatePrototypeToJsonEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Builtin_Impl_Stats_DatePrototypeToJsonEiPmPNS0_7IsolateE, @function
_ZN2v88internalL38Builtin_Impl_Stats_DatePrototypeToJsonEiPmPNS0_7IsolateE:
.LFB21435:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1569
.L1541:
	movq	_ZZN2v88internalL38Builtin_Impl_Stats_DatePrototypeToJsonEiPmPNS0_7IsolateEE28trace_event_unique_atomic971(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1570
.L1543:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1571
.L1545:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL32Builtin_Impl_DatePrototypeToJsonENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1572
.L1549:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1573
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1570:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1574
.L1544:
	movq	%rbx, _ZZN2v88internalL38Builtin_Impl_Stats_DatePrototypeToJsonEiPmPNS0_7IsolateEE28trace_event_unique_atomic971(%rip)
	jmp	.L1543
	.p2align 4,,10
	.p2align 3
.L1571:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1575
.L1546:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1547
	movq	(%rdi), %rax
	call	*8(%rax)
.L1547:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1548
	movq	(%rdi), %rax
	call	*8(%rax)
.L1548:
	leaq	.LC56(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1545
	.p2align 4,,10
	.p2align 3
.L1572:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1549
	.p2align 4,,10
	.p2align 3
.L1569:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$761, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1541
	.p2align 4,,10
	.p2align 3
.L1575:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC56(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1546
	.p2align 4,,10
	.p2align 3
.L1574:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1544
.L1573:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21435:
	.size	_ZN2v88internalL38Builtin_Impl_Stats_DatePrototypeToJsonEiPmPNS0_7IsolateE, .-_ZN2v88internalL38Builtin_Impl_Stats_DatePrototypeToJsonEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetDateEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC57:
	.string	"V8.Builtin_DatePrototypeSetDate"
	.section	.text._ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetDateEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetDateEiPmPNS0_7IsolateE, @function
_ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetDateEiPmPNS0_7IsolateE:
.LFB21356:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1605
.L1577:
	movq	_ZZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetDateEiPmPNS0_7IsolateEE28trace_event_unique_atomic370(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1606
.L1579:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1607
.L1581:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL33Builtin_Impl_DatePrototypeSetDateENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1608
.L1585:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1609
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1606:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1610
.L1580:
	movq	%rbx, _ZZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetDateEiPmPNS0_7IsolateEE28trace_event_unique_atomic370(%rip)
	jmp	.L1579
	.p2align 4,,10
	.p2align 3
.L1607:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1611
.L1582:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1583
	movq	(%rdi), %rax
	call	*8(%rax)
.L1583:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1584
	movq	(%rdi), %rax
	call	*8(%rax)
.L1584:
	leaq	.LC57(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1581
	.p2align 4,,10
	.p2align 3
.L1608:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1585
	.p2align 4,,10
	.p2align 3
.L1605:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$741, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1577
	.p2align 4,,10
	.p2align 3
.L1611:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC57(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1582
	.p2align 4,,10
	.p2align 3
.L1610:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1580
.L1609:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21356:
	.size	_ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetDateEiPmPNS0_7IsolateE, .-_ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetDateEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetFullYearEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC58:
	.string	"V8.Builtin_DatePrototypeSetFullYear"
	.section	.text._ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetFullYearEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetFullYearEiPmPNS0_7IsolateE, @function
_ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetFullYearEiPmPNS0_7IsolateE:
.LFB21359:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1641
.L1613:
	movq	_ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetFullYearEiPmPNS0_7IsolateEE28trace_event_unique_atomic390(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1642
.L1615:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1643
.L1617:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL37Builtin_Impl_DatePrototypeSetFullYearENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1644
.L1621:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1645
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1642:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1646
.L1616:
	movq	%rbx, _ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetFullYearEiPmPNS0_7IsolateEE28trace_event_unique_atomic390(%rip)
	jmp	.L1615
	.p2align 4,,10
	.p2align 3
.L1643:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1647
.L1618:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1619
	movq	(%rdi), %rax
	call	*8(%rax)
.L1619:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1620
	movq	(%rdi), %rax
	call	*8(%rax)
.L1620:
	leaq	.LC58(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1617
	.p2align 4,,10
	.p2align 3
.L1644:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1621
	.p2align 4,,10
	.p2align 3
.L1641:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$742, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1613
	.p2align 4,,10
	.p2align 3
.L1647:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC58(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1618
	.p2align 4,,10
	.p2align 3
.L1646:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1616
.L1645:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21359:
	.size	_ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetFullYearEiPmPNS0_7IsolateE, .-_ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetFullYearEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL40Builtin_Impl_Stats_DatePrototypeSetHoursEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC59:
	.string	"V8.Builtin_DatePrototypeSetHours"
	.section	.text._ZN2v88internalL40Builtin_Impl_Stats_DatePrototypeSetHoursEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Builtin_Impl_Stats_DatePrototypeSetHoursEiPmPNS0_7IsolateE, @function
_ZN2v88internalL40Builtin_Impl_Stats_DatePrototypeSetHoursEiPmPNS0_7IsolateE:
.LFB21362:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1677
.L1649:
	movq	_ZZN2v88internalL40Builtin_Impl_Stats_DatePrototypeSetHoursEiPmPNS0_7IsolateEE28trace_event_unique_atomic426(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1678
.L1651:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1679
.L1653:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL34Builtin_Impl_DatePrototypeSetHoursENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1680
.L1657:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1681
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1678:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1682
.L1652:
	movq	%rbx, _ZZN2v88internalL40Builtin_Impl_Stats_DatePrototypeSetHoursEiPmPNS0_7IsolateEE28trace_event_unique_atomic426(%rip)
	jmp	.L1651
	.p2align 4,,10
	.p2align 3
.L1679:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1683
.L1654:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1655
	movq	(%rdi), %rax
	call	*8(%rax)
.L1655:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1656
	movq	(%rdi), %rax
	call	*8(%rax)
.L1656:
	leaq	.LC59(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1653
	.p2align 4,,10
	.p2align 3
.L1680:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1657
	.p2align 4,,10
	.p2align 3
.L1677:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$743, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1649
	.p2align 4,,10
	.p2align 3
.L1683:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC59(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1654
	.p2align 4,,10
	.p2align 3
.L1682:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1652
.L1681:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21362:
	.size	_ZN2v88internalL40Builtin_Impl_Stats_DatePrototypeSetHoursEiPmPNS0_7IsolateE, .-_ZN2v88internalL40Builtin_Impl_Stats_DatePrototypeSetHoursEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL47Builtin_Impl_Stats_DatePrototypeSetMillisecondsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC60:
	.string	"V8.Builtin_DatePrototypeSetMilliseconds"
	.section	.text._ZN2v88internalL47Builtin_Impl_Stats_DatePrototypeSetMillisecondsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL47Builtin_Impl_Stats_DatePrototypeSetMillisecondsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL47Builtin_Impl_Stats_DatePrototypeSetMillisecondsEiPmPNS0_7IsolateE:
.LFB21365:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1713
.L1685:
	movq	_ZZN2v88internalL47Builtin_Impl_Stats_DatePrototypeSetMillisecondsEiPmPNS0_7IsolateEE28trace_event_unique_atomic467(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1714
.L1687:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1715
.L1689:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL41Builtin_Impl_DatePrototypeSetMillisecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1716
.L1693:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1717
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1714:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1718
.L1688:
	movq	%rbx, _ZZN2v88internalL47Builtin_Impl_Stats_DatePrototypeSetMillisecondsEiPmPNS0_7IsolateEE28trace_event_unique_atomic467(%rip)
	jmp	.L1687
	.p2align 4,,10
	.p2align 3
.L1715:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1719
.L1690:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1691
	movq	(%rdi), %rax
	call	*8(%rax)
.L1691:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1692
	movq	(%rdi), %rax
	call	*8(%rax)
.L1692:
	leaq	.LC60(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1689
	.p2align 4,,10
	.p2align 3
.L1716:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1693
	.p2align 4,,10
	.p2align 3
.L1713:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$744, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1685
	.p2align 4,,10
	.p2align 3
.L1719:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC60(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1690
	.p2align 4,,10
	.p2align 3
.L1718:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1688
.L1717:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21365:
	.size	_ZN2v88internalL47Builtin_Impl_Stats_DatePrototypeSetMillisecondsEiPmPNS0_7IsolateE, .-_ZN2v88internalL47Builtin_Impl_Stats_DatePrototypeSetMillisecondsEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetMinutesEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC61:
	.string	"V8.Builtin_DatePrototypeSetMinutes"
	.section	.text._ZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetMinutesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetMinutesEiPmPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetMinutesEiPmPNS0_7IsolateE:
.LFB21368:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1749
.L1721:
	movq	_ZZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetMinutesEiPmPNS0_7IsolateEE28trace_event_unique_atomic488(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1750
.L1723:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1751
.L1725:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL36Builtin_Impl_DatePrototypeSetMinutesENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1752
.L1729:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1753
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1750:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1754
.L1724:
	movq	%rbx, _ZZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetMinutesEiPmPNS0_7IsolateEE28trace_event_unique_atomic488(%rip)
	jmp	.L1723
	.p2align 4,,10
	.p2align 3
.L1751:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1755
.L1726:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1727
	movq	(%rdi), %rax
	call	*8(%rax)
.L1727:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1728
	movq	(%rdi), %rax
	call	*8(%rax)
.L1728:
	leaq	.LC61(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1725
	.p2align 4,,10
	.p2align 3
.L1752:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1729
	.p2align 4,,10
	.p2align 3
.L1749:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$745, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1721
	.p2align 4,,10
	.p2align 3
.L1755:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC61(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1726
	.p2align 4,,10
	.p2align 3
.L1754:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1724
.L1753:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21368:
	.size	_ZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetMinutesEiPmPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetMinutesEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL40Builtin_Impl_Stats_DatePrototypeSetMonthEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC62:
	.string	"V8.Builtin_DatePrototypeSetMonth"
	.section	.text._ZN2v88internalL40Builtin_Impl_Stats_DatePrototypeSetMonthEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Builtin_Impl_Stats_DatePrototypeSetMonthEiPmPNS0_7IsolateE, @function
_ZN2v88internalL40Builtin_Impl_Stats_DatePrototypeSetMonthEiPmPNS0_7IsolateE:
.LFB21371:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1785
.L1757:
	movq	_ZZN2v88internalL40Builtin_Impl_Stats_DatePrototypeSetMonthEiPmPNS0_7IsolateEE28trace_event_unique_atomic523(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1786
.L1759:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1787
.L1761:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL34Builtin_Impl_DatePrototypeSetMonthENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1788
.L1765:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1789
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1786:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1790
.L1760:
	movq	%rbx, _ZZN2v88internalL40Builtin_Impl_Stats_DatePrototypeSetMonthEiPmPNS0_7IsolateEE28trace_event_unique_atomic523(%rip)
	jmp	.L1759
	.p2align 4,,10
	.p2align 3
.L1787:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1791
.L1762:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1763
	movq	(%rdi), %rax
	call	*8(%rax)
.L1763:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1764
	movq	(%rdi), %rax
	call	*8(%rax)
.L1764:
	leaq	.LC62(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1761
	.p2align 4,,10
	.p2align 3
.L1788:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1765
	.p2align 4,,10
	.p2align 3
.L1785:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$746, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1757
	.p2align 4,,10
	.p2align 3
.L1791:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC62(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1762
	.p2align 4,,10
	.p2align 3
.L1790:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1760
.L1789:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21371:
	.size	_ZN2v88internalL40Builtin_Impl_Stats_DatePrototypeSetMonthEiPmPNS0_7IsolateE, .-_ZN2v88internalL40Builtin_Impl_Stats_DatePrototypeSetMonthEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetSecondsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC63:
	.string	"V8.Builtin_DatePrototypeSetSeconds"
	.section	.text._ZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetSecondsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetSecondsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetSecondsEiPmPNS0_7IsolateE:
.LFB21374:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1821
.L1793:
	movq	_ZZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetSecondsEiPmPNS0_7IsolateEE28trace_event_unique_atomic552(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1822
.L1795:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1823
.L1797:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL36Builtin_Impl_DatePrototypeSetSecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1824
.L1801:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1825
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1822:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1826
.L1796:
	movq	%rbx, _ZZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetSecondsEiPmPNS0_7IsolateEE28trace_event_unique_atomic552(%rip)
	jmp	.L1795
	.p2align 4,,10
	.p2align 3
.L1823:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1827
.L1798:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1799
	movq	(%rdi), %rax
	call	*8(%rax)
.L1799:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1800
	movq	(%rdi), %rax
	call	*8(%rax)
.L1800:
	leaq	.LC63(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1797
	.p2align 4,,10
	.p2align 3
.L1824:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1801
	.p2align 4,,10
	.p2align 3
.L1821:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$747, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1793
	.p2align 4,,10
	.p2align 3
.L1827:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC63(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1798
	.p2align 4,,10
	.p2align 3
.L1826:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1796
.L1825:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21374:
	.size	_ZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetSecondsEiPmPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetSecondsEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetUTCDateEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC64:
	.string	"V8.Builtin_DatePrototypeSetUTCDate"
	.section	.text._ZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetUTCDateEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetUTCDateEiPmPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetUTCDateEiPmPNS0_7IsolateE:
.LFB21380:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1857
.L1829:
	movq	_ZZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetUTCDateEiPmPNS0_7IsolateEE28trace_event_unique_atomic591(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1858
.L1831:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1859
.L1833:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL36Builtin_Impl_DatePrototypeSetUTCDateENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1860
.L1837:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1861
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1858:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1862
.L1832:
	movq	%rbx, _ZZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetUTCDateEiPmPNS0_7IsolateEE28trace_event_unique_atomic591(%rip)
	jmp	.L1831
	.p2align 4,,10
	.p2align 3
.L1859:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1863
.L1834:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1835
	movq	(%rdi), %rax
	call	*8(%rax)
.L1835:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1836
	movq	(%rdi), %rax
	call	*8(%rax)
.L1836:
	leaq	.LC64(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1833
	.p2align 4,,10
	.p2align 3
.L1860:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1837
	.p2align 4,,10
	.p2align 3
.L1857:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$749, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1829
	.p2align 4,,10
	.p2align 3
.L1863:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC64(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1834
	.p2align 4,,10
	.p2align 3
.L1862:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1832
.L1861:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21380:
	.size	_ZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetUTCDateEiPmPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetUTCDateEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL46Builtin_Impl_Stats_DatePrototypeSetUTCFullYearEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC65:
	.string	"V8.Builtin_DatePrototypeSetUTCFullYear"
	.section	.text._ZN2v88internalL46Builtin_Impl_Stats_DatePrototypeSetUTCFullYearEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL46Builtin_Impl_Stats_DatePrototypeSetUTCFullYearEiPmPNS0_7IsolateE, @function
_ZN2v88internalL46Builtin_Impl_Stats_DatePrototypeSetUTCFullYearEiPmPNS0_7IsolateE:
.LFB21383:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1893
.L1865:
	movq	_ZZN2v88internalL46Builtin_Impl_Stats_DatePrototypeSetUTCFullYearEiPmPNS0_7IsolateEE28trace_event_unique_atomic609(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1894
.L1867:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1895
.L1869:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL40Builtin_Impl_DatePrototypeSetUTCFullYearENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1896
.L1873:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1897
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1894:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1898
.L1868:
	movq	%rbx, _ZZN2v88internalL46Builtin_Impl_Stats_DatePrototypeSetUTCFullYearEiPmPNS0_7IsolateEE28trace_event_unique_atomic609(%rip)
	jmp	.L1867
	.p2align 4,,10
	.p2align 3
.L1895:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1899
.L1870:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1871
	movq	(%rdi), %rax
	call	*8(%rax)
.L1871:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1872
	movq	(%rdi), %rax
	call	*8(%rax)
.L1872:
	leaq	.LC65(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1869
	.p2align 4,,10
	.p2align 3
.L1896:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1873
	.p2align 4,,10
	.p2align 3
.L1893:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$750, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1865
	.p2align 4,,10
	.p2align 3
.L1899:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC65(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1870
	.p2align 4,,10
	.p2align 3
.L1898:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1868
.L1897:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21383:
	.size	_ZN2v88internalL46Builtin_Impl_Stats_DatePrototypeSetUTCFullYearEiPmPNS0_7IsolateE, .-_ZN2v88internalL46Builtin_Impl_Stats_DatePrototypeSetUTCFullYearEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetUTCHoursEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC66:
	.string	"V8.Builtin_DatePrototypeSetUTCHours"
	.section	.text._ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetUTCHoursEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetUTCHoursEiPmPNS0_7IsolateE, @function
_ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetUTCHoursEiPmPNS0_7IsolateE:
.LFB21386:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1929
.L1901:
	movq	_ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetUTCHoursEiPmPNS0_7IsolateEE28trace_event_unique_atomic644(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1930
.L1903:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1931
.L1905:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL37Builtin_Impl_DatePrototypeSetUTCHoursENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1932
.L1909:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1933
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1930:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1934
.L1904:
	movq	%rbx, _ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetUTCHoursEiPmPNS0_7IsolateEE28trace_event_unique_atomic644(%rip)
	jmp	.L1903
	.p2align 4,,10
	.p2align 3
.L1931:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1935
.L1906:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1907
	movq	(%rdi), %rax
	call	*8(%rax)
.L1907:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1908
	movq	(%rdi), %rax
	call	*8(%rax)
.L1908:
	leaq	.LC66(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1905
	.p2align 4,,10
	.p2align 3
.L1932:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1909
	.p2align 4,,10
	.p2align 3
.L1929:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$751, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1901
	.p2align 4,,10
	.p2align 3
.L1935:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC66(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1906
	.p2align 4,,10
	.p2align 3
.L1934:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1904
.L1933:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21386:
	.size	_ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetUTCHoursEiPmPNS0_7IsolateE, .-_ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetUTCHoursEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL50Builtin_Impl_Stats_DatePrototypeSetUTCMillisecondsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC67:
	.string	"V8.Builtin_DatePrototypeSetUTCMilliseconds"
	.section	.text._ZN2v88internalL50Builtin_Impl_Stats_DatePrototypeSetUTCMillisecondsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL50Builtin_Impl_Stats_DatePrototypeSetUTCMillisecondsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL50Builtin_Impl_Stats_DatePrototypeSetUTCMillisecondsEiPmPNS0_7IsolateE:
.LFB21389:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1965
.L1937:
	movq	_ZZN2v88internalL50Builtin_Impl_Stats_DatePrototypeSetUTCMillisecondsEiPmPNS0_7IsolateEE28trace_event_unique_atomic684(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1966
.L1939:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1967
.L1941:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL44Builtin_Impl_DatePrototypeSetUTCMillisecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1968
.L1945:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1969
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1966:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1970
.L1940:
	movq	%rbx, _ZZN2v88internalL50Builtin_Impl_Stats_DatePrototypeSetUTCMillisecondsEiPmPNS0_7IsolateEE28trace_event_unique_atomic684(%rip)
	jmp	.L1939
	.p2align 4,,10
	.p2align 3
.L1967:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1971
.L1942:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1943
	movq	(%rdi), %rax
	call	*8(%rax)
.L1943:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1944
	movq	(%rdi), %rax
	call	*8(%rax)
.L1944:
	leaq	.LC67(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1941
	.p2align 4,,10
	.p2align 3
.L1968:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1945
	.p2align 4,,10
	.p2align 3
.L1965:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$752, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1937
	.p2align 4,,10
	.p2align 3
.L1971:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC67(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1942
	.p2align 4,,10
	.p2align 3
.L1970:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1940
.L1969:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21389:
	.size	_ZN2v88internalL50Builtin_Impl_Stats_DatePrototypeSetUTCMillisecondsEiPmPNS0_7IsolateE, .-_ZN2v88internalL50Builtin_Impl_Stats_DatePrototypeSetUTCMillisecondsEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL45Builtin_Impl_Stats_DatePrototypeSetUTCMinutesEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC68:
	.string	"V8.Builtin_DatePrototypeSetUTCMinutes"
	.section	.text._ZN2v88internalL45Builtin_Impl_Stats_DatePrototypeSetUTCMinutesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL45Builtin_Impl_Stats_DatePrototypeSetUTCMinutesEiPmPNS0_7IsolateE, @function
_ZN2v88internalL45Builtin_Impl_Stats_DatePrototypeSetUTCMinutesEiPmPNS0_7IsolateE:
.LFB21392:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2001
.L1973:
	movq	_ZZN2v88internalL45Builtin_Impl_Stats_DatePrototypeSetUTCMinutesEiPmPNS0_7IsolateEE28trace_event_unique_atomic704(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2002
.L1975:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2003
.L1977:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL39Builtin_Impl_DatePrototypeSetUTCMinutesENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2004
.L1981:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2005
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2002:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2006
.L1976:
	movq	%rbx, _ZZN2v88internalL45Builtin_Impl_Stats_DatePrototypeSetUTCMinutesEiPmPNS0_7IsolateEE28trace_event_unique_atomic704(%rip)
	jmp	.L1975
	.p2align 4,,10
	.p2align 3
.L2003:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2007
.L1978:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1979
	movq	(%rdi), %rax
	call	*8(%rax)
.L1979:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1980
	movq	(%rdi), %rax
	call	*8(%rax)
.L1980:
	leaq	.LC68(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1977
	.p2align 4,,10
	.p2align 3
.L2004:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1981
	.p2align 4,,10
	.p2align 3
.L2001:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$753, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L2007:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC68(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1978
	.p2align 4,,10
	.p2align 3
.L2006:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1976
.L2005:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21392:
	.size	_ZN2v88internalL45Builtin_Impl_Stats_DatePrototypeSetUTCMinutesEiPmPNS0_7IsolateE, .-_ZN2v88internalL45Builtin_Impl_Stats_DatePrototypeSetUTCMinutesEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetUTCMonthEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC69:
	.string	"V8.Builtin_DatePrototypeSetUTCMonth"
	.section	.text._ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetUTCMonthEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetUTCMonthEiPmPNS0_7IsolateE, @function
_ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetUTCMonthEiPmPNS0_7IsolateE:
.LFB21395:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2037
.L2009:
	movq	_ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetUTCMonthEiPmPNS0_7IsolateEE28trace_event_unique_atomic738(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2038
.L2011:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2039
.L2013:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL37Builtin_Impl_DatePrototypeSetUTCMonthENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2040
.L2017:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2041
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2038:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2042
.L2012:
	movq	%rbx, _ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetUTCMonthEiPmPNS0_7IsolateEE28trace_event_unique_atomic738(%rip)
	jmp	.L2011
	.p2align 4,,10
	.p2align 3
.L2039:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2043
.L2014:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2015
	movq	(%rdi), %rax
	call	*8(%rax)
.L2015:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2016
	movq	(%rdi), %rax
	call	*8(%rax)
.L2016:
	leaq	.LC69(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2013
	.p2align 4,,10
	.p2align 3
.L2040:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2017
	.p2align 4,,10
	.p2align 3
.L2037:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$754, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2009
	.p2align 4,,10
	.p2align 3
.L2043:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC69(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L2014
	.p2align 4,,10
	.p2align 3
.L2042:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2012
.L2041:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21395:
	.size	_ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetUTCMonthEiPmPNS0_7IsolateE, .-_ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetUTCMonthEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL45Builtin_Impl_Stats_DatePrototypeSetUTCSecondsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC70:
	.string	"V8.Builtin_DatePrototypeSetUTCSeconds"
	.section	.text._ZN2v88internalL45Builtin_Impl_Stats_DatePrototypeSetUTCSecondsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL45Builtin_Impl_Stats_DatePrototypeSetUTCSecondsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL45Builtin_Impl_Stats_DatePrototypeSetUTCSecondsEiPmPNS0_7IsolateE:
.LFB21398:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2073
.L2045:
	movq	_ZZN2v88internalL45Builtin_Impl_Stats_DatePrototypeSetUTCSecondsEiPmPNS0_7IsolateEE28trace_event_unique_atomic766(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2074
.L2047:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2075
.L2049:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL39Builtin_Impl_DatePrototypeSetUTCSecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2076
.L2053:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2077
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2074:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2078
.L2048:
	movq	%rbx, _ZZN2v88internalL45Builtin_Impl_Stats_DatePrototypeSetUTCSecondsEiPmPNS0_7IsolateEE28trace_event_unique_atomic766(%rip)
	jmp	.L2047
	.p2align 4,,10
	.p2align 3
.L2075:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2079
.L2050:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2051
	movq	(%rdi), %rax
	call	*8(%rax)
.L2051:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2052
	movq	(%rdi), %rax
	call	*8(%rax)
.L2052:
	leaq	.LC70(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2049
	.p2align 4,,10
	.p2align 3
.L2076:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2053
	.p2align 4,,10
	.p2align 3
.L2073:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$755, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2045
	.p2align 4,,10
	.p2align 3
.L2079:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC70(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L2050
	.p2align 4,,10
	.p2align 3
.L2078:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2048
.L2077:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21398:
	.size	_ZN2v88internalL45Builtin_Impl_Stats_DatePrototypeSetUTCSecondsEiPmPNS0_7IsolateE, .-_ZN2v88internalL45Builtin_Impl_Stats_DatePrototypeSetUTCSecondsEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeToISOStringEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC71:
	.string	"V8.Builtin_DatePrototypeToISOString"
	.section	.text._ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeToISOStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeToISOStringEiPmPNS0_7IsolateE, @function
_ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeToISOStringEiPmPNS0_7IsolateE:
.LFB21404:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2109
.L2081:
	movq	_ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeToISOStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic804(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2110
.L2083:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2111
.L2085:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL37Builtin_Impl_DatePrototypeToISOStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2112
.L2089:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2113
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2110:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2114
.L2084:
	movq	%rbx, _ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeToISOStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic804(%rip)
	jmp	.L2083
	.p2align 4,,10
	.p2align 3
.L2111:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2115
.L2086:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2087
	movq	(%rdi), %rax
	call	*8(%rax)
.L2087:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2088
	movq	(%rdi), %rax
	call	*8(%rax)
.L2088:
	leaq	.LC71(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2085
	.p2align 4,,10
	.p2align 3
.L2112:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2089
	.p2align 4,,10
	.p2align 3
.L2109:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$757, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2081
	.p2align 4,,10
	.p2align 3
.L2115:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC71(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L2086
	.p2align 4,,10
	.p2align 3
.L2114:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2084
.L2113:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21404:
	.size	_ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeToISOStringEiPmPNS0_7IsolateE, .-_ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeToISOStringEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeToUTCStringEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC72:
	.string	"V8.Builtin_DatePrototypeToUTCString"
	.section	.text._ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeToUTCStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeToUTCStringEiPmPNS0_7IsolateE, @function
_ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeToUTCStringEiPmPNS0_7IsolateE:
.LFB21426:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2145
.L2117:
	movq	_ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeToUTCStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic907(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2146
.L2119:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2147
.L2121:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL37Builtin_Impl_DatePrototypeToUTCStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2148
.L2125:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2149
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2146:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2150
.L2120:
	movq	%rbx, _ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeToUTCStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic907(%rip)
	jmp	.L2119
	.p2align 4,,10
	.p2align 3
.L2147:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2151
.L2122:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2123
	movq	(%rdi), %rax
	call	*8(%rax)
.L2123:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2124
	movq	(%rdi), %rax
	call	*8(%rax)
.L2124:
	leaq	.LC72(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2121
	.p2align 4,,10
	.p2align 3
.L2148:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2125
	.p2align 4,,10
	.p2align 3
.L2145:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$758, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2117
	.p2align 4,,10
	.p2align 3
.L2151:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC72(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L2122
	.p2align 4,,10
	.p2align 3
.L2150:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2120
.L2149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21426:
	.size	_ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeToUTCStringEiPmPNS0_7IsolateE, .-_ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeToUTCStringEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeGetYearEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC73:
	.string	"V8.Builtin_DatePrototypeGetYear"
	.section	.text._ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeGetYearEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeGetYearEiPmPNS0_7IsolateE, @function
_ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeGetYearEiPmPNS0_7IsolateE:
.LFB21429:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2181
.L2153:
	movq	_ZZN2v88internalL39Builtin_Impl_Stats_DatePrototypeGetYearEiPmPNS0_7IsolateEE28trace_event_unique_atomic928(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2182
.L2155:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2183
.L2157:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL33Builtin_Impl_DatePrototypeGetYearENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2184
.L2161:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2185
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2182:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2186
.L2156:
	movq	%rbx, _ZZN2v88internalL39Builtin_Impl_Stats_DatePrototypeGetYearEiPmPNS0_7IsolateEE28trace_event_unique_atomic928(%rip)
	jmp	.L2155
	.p2align 4,,10
	.p2align 3
.L2183:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2187
.L2158:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2159
	movq	(%rdi), %rax
	call	*8(%rax)
.L2159:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2160
	movq	(%rdi), %rax
	call	*8(%rax)
.L2160:
	leaq	.LC73(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2157
	.p2align 4,,10
	.p2align 3
.L2184:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2161
	.p2align 4,,10
	.p2align 3
.L2181:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$737, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2153
	.p2align 4,,10
	.p2align 3
.L2187:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC73(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L2158
	.p2align 4,,10
	.p2align 3
.L2186:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2156
.L2185:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21429:
	.size	_ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeGetYearEiPmPNS0_7IsolateE, .-_ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeGetYearEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetYearEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC74:
	.string	"V8.Builtin_DatePrototypeSetYear"
	.section	.text._ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetYearEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetYearEiPmPNS0_7IsolateE, @function
_ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetYearEiPmPNS0_7IsolateE:
.LFB21432:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2217
.L2189:
	movq	_ZZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetYearEiPmPNS0_7IsolateEE28trace_event_unique_atomic942(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2218
.L2191:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2219
.L2193:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL33Builtin_Impl_DatePrototypeSetYearENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2220
.L2197:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2221
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2218:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2222
.L2192:
	movq	%rbx, _ZZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetYearEiPmPNS0_7IsolateEE28trace_event_unique_atomic942(%rip)
	jmp	.L2191
	.p2align 4,,10
	.p2align 3
.L2219:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2223
.L2194:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2195
	movq	(%rdi), %rax
	call	*8(%rax)
.L2195:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2196
	movq	(%rdi), %rax
	call	*8(%rax)
.L2196:
	leaq	.LC74(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2193
	.p2align 4,,10
	.p2align 3
.L2220:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2197
	.p2align 4,,10
	.p2align 3
.L2217:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$738, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2189
	.p2align 4,,10
	.p2align 3
.L2223:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC74(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L2194
	.p2align 4,,10
	.p2align 3
.L2222:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2192
.L2221:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21432:
	.size	_ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetYearEiPmPNS0_7IsolateE, .-_ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetYearEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal15Builtin_DateNowEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal15Builtin_DateNowEiPmPNS0_7IsolateE
	.type	_ZN2v88internal15Builtin_DateNowEiPmPNS0_7IsolateE, @function
_ZN2v88internal15Builtin_DateNowEiPmPNS0_7IsolateE:
.LFB21348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2228
	addl	$1, 41104(%rdx)
	movq	%rdx, %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	call	_ZN2v88internal6JSDate16CurrentTimeValueEPNS0_7IsolateE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2224
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2224:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2228:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL26Builtin_Impl_Stats_DateNowEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21348:
	.size	_ZN2v88internal15Builtin_DateNowEiPmPNS0_7IsolateE, .-_ZN2v88internal15Builtin_DateNowEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal15Builtin_DateUTCEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal15Builtin_DateUTCEiPmPNS0_7IsolateE
	.type	_ZN2v88internal15Builtin_DateUTCEiPmPNS0_7IsolateE, @function
_ZN2v88internal15Builtin_DateUTCEiPmPNS0_7IsolateE:
.LFB21354:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2233
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL20Builtin_Impl_DateUTCENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2233:
	.cfi_restore 6
	jmp	_ZN2v88internalL26Builtin_Impl_Stats_DateUTCEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21354:
	.size	_ZN2v88internal15Builtin_DateUTCEiPmPNS0_7IsolateE, .-_ZN2v88internal15Builtin_DateUTCEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Builtin_DatePrototypeSetDateEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Builtin_DatePrototypeSetDateEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Builtin_DatePrototypeSetDateEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Builtin_DatePrototypeSetDateEiPmPNS0_7IsolateE:
.LFB21357:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2238
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL33Builtin_Impl_DatePrototypeSetDateENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2238:
	.cfi_restore 6
	jmp	_ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetDateEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21357:
	.size	_ZN2v88internal28Builtin_DatePrototypeSetDateEiPmPNS0_7IsolateE, .-_ZN2v88internal28Builtin_DatePrototypeSetDateEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Builtin_DatePrototypeSetFullYearEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Builtin_DatePrototypeSetFullYearEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Builtin_DatePrototypeSetFullYearEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Builtin_DatePrototypeSetFullYearEiPmPNS0_7IsolateE:
.LFB21360:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2243
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL37Builtin_Impl_DatePrototypeSetFullYearENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2243:
	.cfi_restore 6
	jmp	_ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetFullYearEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21360:
	.size	_ZN2v88internal32Builtin_DatePrototypeSetFullYearEiPmPNS0_7IsolateE, .-_ZN2v88internal32Builtin_DatePrototypeSetFullYearEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Builtin_DatePrototypeSetHoursEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Builtin_DatePrototypeSetHoursEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Builtin_DatePrototypeSetHoursEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Builtin_DatePrototypeSetHoursEiPmPNS0_7IsolateE:
.LFB21363:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2248
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL34Builtin_Impl_DatePrototypeSetHoursENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2248:
	.cfi_restore 6
	jmp	_ZN2v88internalL40Builtin_Impl_Stats_DatePrototypeSetHoursEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21363:
	.size	_ZN2v88internal29Builtin_DatePrototypeSetHoursEiPmPNS0_7IsolateE, .-_ZN2v88internal29Builtin_DatePrototypeSetHoursEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal36Builtin_DatePrototypeSetMillisecondsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal36Builtin_DatePrototypeSetMillisecondsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal36Builtin_DatePrototypeSetMillisecondsEiPmPNS0_7IsolateE, @function
_ZN2v88internal36Builtin_DatePrototypeSetMillisecondsEiPmPNS0_7IsolateE:
.LFB21366:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2253
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL41Builtin_Impl_DatePrototypeSetMillisecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2253:
	.cfi_restore 6
	jmp	_ZN2v88internalL47Builtin_Impl_Stats_DatePrototypeSetMillisecondsEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21366:
	.size	_ZN2v88internal36Builtin_DatePrototypeSetMillisecondsEiPmPNS0_7IsolateE, .-_ZN2v88internal36Builtin_DatePrototypeSetMillisecondsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Builtin_DatePrototypeSetMinutesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Builtin_DatePrototypeSetMinutesEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Builtin_DatePrototypeSetMinutesEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Builtin_DatePrototypeSetMinutesEiPmPNS0_7IsolateE:
.LFB21369:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2258
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL36Builtin_Impl_DatePrototypeSetMinutesENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2258:
	.cfi_restore 6
	jmp	_ZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetMinutesEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21369:
	.size	_ZN2v88internal31Builtin_DatePrototypeSetMinutesEiPmPNS0_7IsolateE, .-_ZN2v88internal31Builtin_DatePrototypeSetMinutesEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Builtin_DatePrototypeSetMonthEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Builtin_DatePrototypeSetMonthEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Builtin_DatePrototypeSetMonthEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Builtin_DatePrototypeSetMonthEiPmPNS0_7IsolateE:
.LFB21372:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2263
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL34Builtin_Impl_DatePrototypeSetMonthENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2263:
	.cfi_restore 6
	jmp	_ZN2v88internalL40Builtin_Impl_Stats_DatePrototypeSetMonthEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21372:
	.size	_ZN2v88internal29Builtin_DatePrototypeSetMonthEiPmPNS0_7IsolateE, .-_ZN2v88internal29Builtin_DatePrototypeSetMonthEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Builtin_DatePrototypeSetSecondsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Builtin_DatePrototypeSetSecondsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Builtin_DatePrototypeSetSecondsEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Builtin_DatePrototypeSetSecondsEiPmPNS0_7IsolateE:
.LFB21375:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2268
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL36Builtin_Impl_DatePrototypeSetSecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2268:
	.cfi_restore 6
	jmp	_ZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetSecondsEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21375:
	.size	_ZN2v88internal31Builtin_DatePrototypeSetSecondsEiPmPNS0_7IsolateE, .-_ZN2v88internal31Builtin_DatePrototypeSetSecondsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Builtin_DatePrototypeSetTimeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Builtin_DatePrototypeSetTimeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Builtin_DatePrototypeSetTimeEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Builtin_DatePrototypeSetTimeEiPmPNS0_7IsolateE:
.LFB21378:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2273
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL33Builtin_Impl_DatePrototypeSetTimeENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2273:
	.cfi_restore 6
	jmp	_ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetTimeEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21378:
	.size	_ZN2v88internal28Builtin_DatePrototypeSetTimeEiPmPNS0_7IsolateE, .-_ZN2v88internal28Builtin_DatePrototypeSetTimeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Builtin_DatePrototypeSetUTCDateEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Builtin_DatePrototypeSetUTCDateEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Builtin_DatePrototypeSetUTCDateEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Builtin_DatePrototypeSetUTCDateEiPmPNS0_7IsolateE:
.LFB21381:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2278
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL36Builtin_Impl_DatePrototypeSetUTCDateENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2278:
	.cfi_restore 6
	jmp	_ZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetUTCDateEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21381:
	.size	_ZN2v88internal31Builtin_DatePrototypeSetUTCDateEiPmPNS0_7IsolateE, .-_ZN2v88internal31Builtin_DatePrototypeSetUTCDateEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal35Builtin_DatePrototypeSetUTCFullYearEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal35Builtin_DatePrototypeSetUTCFullYearEiPmPNS0_7IsolateE
	.type	_ZN2v88internal35Builtin_DatePrototypeSetUTCFullYearEiPmPNS0_7IsolateE, @function
_ZN2v88internal35Builtin_DatePrototypeSetUTCFullYearEiPmPNS0_7IsolateE:
.LFB21384:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2283
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL40Builtin_Impl_DatePrototypeSetUTCFullYearENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2283:
	.cfi_restore 6
	jmp	_ZN2v88internalL46Builtin_Impl_Stats_DatePrototypeSetUTCFullYearEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21384:
	.size	_ZN2v88internal35Builtin_DatePrototypeSetUTCFullYearEiPmPNS0_7IsolateE, .-_ZN2v88internal35Builtin_DatePrototypeSetUTCFullYearEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Builtin_DatePrototypeSetUTCHoursEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Builtin_DatePrototypeSetUTCHoursEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Builtin_DatePrototypeSetUTCHoursEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Builtin_DatePrototypeSetUTCHoursEiPmPNS0_7IsolateE:
.LFB21387:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2288
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL37Builtin_Impl_DatePrototypeSetUTCHoursENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2288:
	.cfi_restore 6
	jmp	_ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetUTCHoursEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21387:
	.size	_ZN2v88internal32Builtin_DatePrototypeSetUTCHoursEiPmPNS0_7IsolateE, .-_ZN2v88internal32Builtin_DatePrototypeSetUTCHoursEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal39Builtin_DatePrototypeSetUTCMillisecondsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal39Builtin_DatePrototypeSetUTCMillisecondsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal39Builtin_DatePrototypeSetUTCMillisecondsEiPmPNS0_7IsolateE, @function
_ZN2v88internal39Builtin_DatePrototypeSetUTCMillisecondsEiPmPNS0_7IsolateE:
.LFB21390:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2293
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL44Builtin_Impl_DatePrototypeSetUTCMillisecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2293:
	.cfi_restore 6
	jmp	_ZN2v88internalL50Builtin_Impl_Stats_DatePrototypeSetUTCMillisecondsEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21390:
	.size	_ZN2v88internal39Builtin_DatePrototypeSetUTCMillisecondsEiPmPNS0_7IsolateE, .-_ZN2v88internal39Builtin_DatePrototypeSetUTCMillisecondsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Builtin_DatePrototypeSetUTCMinutesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Builtin_DatePrototypeSetUTCMinutesEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Builtin_DatePrototypeSetUTCMinutesEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Builtin_DatePrototypeSetUTCMinutesEiPmPNS0_7IsolateE:
.LFB21393:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2298
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL39Builtin_Impl_DatePrototypeSetUTCMinutesENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2298:
	.cfi_restore 6
	jmp	_ZN2v88internalL45Builtin_Impl_Stats_DatePrototypeSetUTCMinutesEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21393:
	.size	_ZN2v88internal34Builtin_DatePrototypeSetUTCMinutesEiPmPNS0_7IsolateE, .-_ZN2v88internal34Builtin_DatePrototypeSetUTCMinutesEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Builtin_DatePrototypeSetUTCMonthEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Builtin_DatePrototypeSetUTCMonthEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Builtin_DatePrototypeSetUTCMonthEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Builtin_DatePrototypeSetUTCMonthEiPmPNS0_7IsolateE:
.LFB21396:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2303
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL37Builtin_Impl_DatePrototypeSetUTCMonthENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2303:
	.cfi_restore 6
	jmp	_ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetUTCMonthEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21396:
	.size	_ZN2v88internal32Builtin_DatePrototypeSetUTCMonthEiPmPNS0_7IsolateE, .-_ZN2v88internal32Builtin_DatePrototypeSetUTCMonthEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Builtin_DatePrototypeSetUTCSecondsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Builtin_DatePrototypeSetUTCSecondsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Builtin_DatePrototypeSetUTCSecondsEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Builtin_DatePrototypeSetUTCSecondsEiPmPNS0_7IsolateE:
.LFB21399:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2308
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL39Builtin_Impl_DatePrototypeSetUTCSecondsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2308:
	.cfi_restore 6
	jmp	_ZN2v88internalL45Builtin_Impl_Stats_DatePrototypeSetUTCSecondsEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21399:
	.size	_ZN2v88internal34Builtin_DatePrototypeSetUTCSecondsEiPmPNS0_7IsolateE, .-_ZN2v88internal34Builtin_DatePrototypeSetUTCSecondsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Builtin_DatePrototypeToISOStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Builtin_DatePrototypeToISOStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Builtin_DatePrototypeToISOStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Builtin_DatePrototypeToISOStringEiPmPNS0_7IsolateE:
.LFB21405:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2313
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL37Builtin_Impl_DatePrototypeToISOStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2313:
	.cfi_restore 6
	jmp	_ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeToISOStringEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21405:
	.size	_ZN2v88internal32Builtin_DatePrototypeToISOStringEiPmPNS0_7IsolateE, .-_ZN2v88internal32Builtin_DatePrototypeToISOStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal39Builtin_DatePrototypeToLocaleDateStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal39Builtin_DatePrototypeToLocaleDateStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal39Builtin_DatePrototypeToLocaleDateStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal39Builtin_DatePrototypeToLocaleDateStringEiPmPNS0_7IsolateE:
.LFB21418:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2318
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL44Builtin_Impl_DatePrototypeToLocaleDateStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2318:
	.cfi_restore 6
	jmp	_ZN2v88internalL50Builtin_Impl_Stats_DatePrototypeToLocaleDateStringEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21418:
	.size	_ZN2v88internal39Builtin_DatePrototypeToLocaleDateStringEiPmPNS0_7IsolateE, .-_ZN2v88internal39Builtin_DatePrototypeToLocaleDateStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal35Builtin_DatePrototypeToLocaleStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal35Builtin_DatePrototypeToLocaleStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal35Builtin_DatePrototypeToLocaleStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal35Builtin_DatePrototypeToLocaleStringEiPmPNS0_7IsolateE:
.LFB21421:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2323
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL40Builtin_Impl_DatePrototypeToLocaleStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2323:
	.cfi_restore 6
	jmp	_ZN2v88internalL46Builtin_Impl_Stats_DatePrototypeToLocaleStringEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21421:
	.size	_ZN2v88internal35Builtin_DatePrototypeToLocaleStringEiPmPNS0_7IsolateE, .-_ZN2v88internal35Builtin_DatePrototypeToLocaleStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal39Builtin_DatePrototypeToLocaleTimeStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal39Builtin_DatePrototypeToLocaleTimeStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal39Builtin_DatePrototypeToLocaleTimeStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal39Builtin_DatePrototypeToLocaleTimeStringEiPmPNS0_7IsolateE:
.LFB21424:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2328
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL44Builtin_Impl_DatePrototypeToLocaleTimeStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2328:
	.cfi_restore 6
	jmp	_ZN2v88internalL50Builtin_Impl_Stats_DatePrototypeToLocaleTimeStringEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21424:
	.size	_ZN2v88internal39Builtin_DatePrototypeToLocaleTimeStringEiPmPNS0_7IsolateE, .-_ZN2v88internal39Builtin_DatePrototypeToLocaleTimeStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Builtin_DatePrototypeToUTCStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Builtin_DatePrototypeToUTCStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Builtin_DatePrototypeToUTCStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Builtin_DatePrototypeToUTCStringEiPmPNS0_7IsolateE:
.LFB21427:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2333
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL37Builtin_Impl_DatePrototypeToUTCStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2333:
	.cfi_restore 6
	jmp	_ZN2v88internalL43Builtin_Impl_Stats_DatePrototypeToUTCStringEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21427:
	.size	_ZN2v88internal32Builtin_DatePrototypeToUTCStringEiPmPNS0_7IsolateE, .-_ZN2v88internal32Builtin_DatePrototypeToUTCStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Builtin_DatePrototypeGetYearEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Builtin_DatePrototypeGetYearEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Builtin_DatePrototypeGetYearEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Builtin_DatePrototypeGetYearEiPmPNS0_7IsolateE:
.LFB21430:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2338
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL33Builtin_Impl_DatePrototypeGetYearENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2338:
	.cfi_restore 6
	jmp	_ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeGetYearEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21430:
	.size	_ZN2v88internal28Builtin_DatePrototypeGetYearEiPmPNS0_7IsolateE, .-_ZN2v88internal28Builtin_DatePrototypeGetYearEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Builtin_DatePrototypeSetYearEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Builtin_DatePrototypeSetYearEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Builtin_DatePrototypeSetYearEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Builtin_DatePrototypeSetYearEiPmPNS0_7IsolateE:
.LFB21433:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2343
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL33Builtin_Impl_DatePrototypeSetYearENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2343:
	.cfi_restore 6
	jmp	_ZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetYearEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21433:
	.size	_ZN2v88internal28Builtin_DatePrototypeSetYearEiPmPNS0_7IsolateE, .-_ZN2v88internal28Builtin_DatePrototypeSetYearEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Builtin_DatePrototypeToJsonEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Builtin_DatePrototypeToJsonEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Builtin_DatePrototypeToJsonEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Builtin_DatePrototypeToJsonEiPmPNS0_7IsolateE:
.LFB21436:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2348
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL32Builtin_Impl_DatePrototypeToJsonENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2348:
	.cfi_restore 6
	jmp	_ZN2v88internalL38Builtin_Impl_Stats_DatePrototypeToJsonEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21436:
	.size	_ZN2v88internal27Builtin_DatePrototypeToJsonEiPmPNS0_7IsolateE, .-_ZN2v88internal27Builtin_DatePrototypeToJsonEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv,"axG",@progbits,_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	.type	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv, @function
_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv:
.LFB24991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	24(%rax), %edx
	testl	%edx, %edx
	jne	.L2350
	movl	$4294967295, %eax
	movl	$-1, -40(%rbp)
	movq	%rax, -48(%rbp)
.L2351:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	movq	-48(%rbp), %rax
	movl	-40(%rbp), %edx
	jne	.L2410
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2350:
	.cfi_restore_state
	leal	-48(%rdx), %ecx
	movslq	(%rax), %r11
	cmpl	$9, %ecx
	ja	.L2352
	movl	16(%rax), %r8d
	movl	%r11d, %ecx
	xorl	%esi, %esi
	xorl	%r10d, %r10d
	movslq	%r11d, %rbx
.L2355:
	cmpq	$8, %rsi
	ja	.L2353
	leal	(%r10,%r10,4), %r9d
	leal	-48(%rdx,%r9,2), %r10d
.L2353:
	cmpl	%ecx, %r8d
	jle	.L2354
	movq	8(%rax), %rdx
	addl	$1, %ecx
	addq	%rsi, %rdx
	addq	$1, %rsi
	movzbl	(%rdx,%rbx), %edx
	movl	%ecx, (%rax)
	leal	-48(%rdx), %r9d
	movl	%edx, 24(%rax)
	cmpl	$9, %r9d
	jbe	.L2355
	.p2align 4,,10
	.p2align 3
.L2356:
	movq	(%rdi), %rax
	movl	$-3, -48(%rbp)
	movl	%r10d, -40(%rbp)
	movl	(%rax), %eax
	subl	%r11d, %eax
	movl	%eax, -44(%rbp)
	jmp	.L2351
	.p2align 4,,10
	.p2align 3
.L2352:
	cmpl	$58, %edx
	je	.L2411
	cmpl	$45, %edx
	je	.L2412
	cmpl	$43, %edx
	je	.L2413
	cmpl	$46, %edx
	je	.L2414
	cmpl	$41, %edx
	je	.L2415
	cmpl	$64, %edx
	jbe	.L2367
	movq	$0, -36(%rbp)
	xorl	%esi, %esi
	leaq	-36(%rbp), %r9
	movl	$0, -28(%rbp)
	movl	16(%rax), %edi
.L2368:
	cmpq	$2, %rsi
	ja	.L2370
	orl	$32, %edx
	movl	%edx, (%r9,%rsi,4)
.L2370:
	movl	(%rax), %ecx
	cmpl	%edi, %ecx
	jge	.L2371
	movq	8(%rax), %r8
	movslq	%ecx, %rdx
	addl	$1, %ecx
	leal	1(%rsi), %ebx
	addq	$1, %rsi
	movzbl	(%r8,%rdx), %edx
	movl	%ecx, (%rax)
	movl	%edx, 24(%rax)
	cmpl	$64, %edx
	ja	.L2368
	.p2align 4,,10
	.p2align 3
.L2372:
	cmpl	$2, %ebx
	jg	.L2379
	movl	$2, %eax
	movslq	%ebx, %rdx
	xorl	%edi, %edi
	subl	%ebx, %eax
	leaq	(%r9,%rdx,4), %rcx
	leaq	4(,%rax,4), %rax
	cmpl	$8, %eax
	jb	.L2416
	leaq	8(%rcx), %rsi
	movl	%eax, %edx
	movq	$0, (%rcx)
	movq	$0, -8(%rcx,%rdx)
	andq	$-8, %rsi
	subq	%rsi, %rcx
	addl	%ecx, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L2379
	andl	$-8, %eax
	xorl	%edx, %edx
.L2377:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	%rdi, (%rsi,%rcx)
	cmpl	%eax, %edx
	jb	.L2377
.L2379:
	movl	%ebx, %esi
	movq	%r9, %rdi
	call	_ZN2v88internal10DateParser12KeywordTable6LookupEPKji@PLT
	movl	%ebx, -44(%rbp)
	cltq
	leaq	(%rax,%rax,4), %rdx
	leaq	_ZN2v88internal10DateParser12KeywordTable5arrayE(%rip), %rax
	addq	%rdx, %rax
	movsbl	4(%rax), %edx
	movsbl	3(%rax), %eax
	movl	%edx, -40(%rbp)
	movl	%eax, -48(%rbp)
	jmp	.L2351
	.p2align 4,,10
	.p2align 3
.L2354:
	addl	$1, %ecx
	movl	$0, 24(%rax)
	movl	%ecx, (%rax)
	jmp	.L2356
	.p2align 4,,10
	.p2align 3
.L2411:
	xorl	%edx, %edx
	cmpl	16(%rax), %r11d
	jge	.L2358
	movq	8(%rax), %rcx
	movslq	%r11d, %rdx
	movzbl	(%rcx,%rdx), %edx
.L2358:
	movl	%edx, 24(%rax)
	leal	1(%r11), %edx
	movl	%edx, (%rax)
	movabsq	$8589934590, %rax
	movq	%rax, -48(%rbp)
	movl	$58, -40(%rbp)
	jmp	.L2351
	.p2align 4,,10
	.p2align 3
.L2371:
	addl	$1, %ecx
	movl	$0, 24(%rax)
	leal	1(%rsi), %ebx
	movl	%ecx, (%rax)
	jmp	.L2372
	.p2align 4,,10
	.p2align 3
.L2367:
	movq	16(%rax), %rsi
	movslq	%edx, %rcx
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r9
	movl	%esi, %r8d
	testb	$8, (%r9,%rcx)
	jne	.L2417
	cmpl	$40, %edx
	jne	.L2382
	xorl	%edi, %edi
	movl	$1, %r10d
	xorl	%r9d, %r9d
.L2391:
	movl	%r11d, %ecx
	leal	(%r10,%r11), %esi
	cmpl	$41, %edx
	je	.L2418
	cmpl	$40, %edx
	je	.L2419
.L2384:
	addl	%r9d, %ecx
	cmpl	%ecx, %r8d
	jle	.L2386
	movq	8(%rax), %rdx
	movzbl	(%rdx,%r11), %edx
	movl	%esi, (%rax)
	movl	%edx, 24(%rax)
	testl	%edi, %edi
	jle	.L2409
.L2389:
	addq	$1, %r11
	testl	%edx, %edx
	jne	.L2391
	jmp	.L2409
	.p2align 4,,10
	.p2align 3
.L2412:
	xorl	%edx, %edx
	cmpl	16(%rax), %r11d
	jge	.L2360
	movq	8(%rax), %rcx
	movslq	%r11d, %rdx
	movzbl	(%rcx,%rdx), %edx
.L2360:
	movl	%edx, 24(%rax)
	leal	1(%r11), %edx
	movl	%edx, (%rax)
	movabsq	$8589934590, %rax
	movq	%rax, -48(%rbp)
	movl	$45, -40(%rbp)
	jmp	.L2351
	.p2align 4,,10
	.p2align 3
.L2413:
	xorl	%edx, %edx
	cmpl	16(%rax), %r11d
	jge	.L2362
	movq	8(%rax), %rcx
	movslq	%r11d, %rdx
	movzbl	(%rcx,%rdx), %edx
.L2362:
	movl	%edx, 24(%rax)
	leal	1(%r11), %edx
	movl	%edx, (%rax)
	movabsq	$8589934590, %rax
	movq	%rax, -48(%rbp)
	movl	$43, -40(%rbp)
	jmp	.L2351
	.p2align 4,,10
	.p2align 3
.L2416:
	testb	$4, %al
	jne	.L2420
	testl	%eax, %eax
	je	.L2379
	movb	$0, (%rcx)
	jmp	.L2379
.L2382:
	xorl	%edx, %edx
	cmpl	%esi, %r11d
	jge	.L2392
	movq	8(%rax), %rcx
	movslq	%r11d, %rdx
	movzbl	(%rcx,%rdx), %edx
.L2392:
	movl	%edx, 24(%rax)
	leal	1(%r11), %edx
	movl	%edx, (%rax)
.L2409:
	movabsq	$8589934587, %rax
	movl	$-1, -40(%rbp)
	movq	%rax, -48(%rbp)
	jmp	.L2351
	.p2align 4,,10
	.p2align 3
.L2386:
	movl	$0, 24(%rax)
	movl	%esi, (%rax)
	jmp	.L2409
	.p2align 4,,10
	.p2align 3
.L2418:
	subl	$1, %edi
	jmp	.L2384
	.p2align 4,,10
	.p2align 3
.L2419:
	addl	%r9d, %ecx
	cmpl	%ecx, %r8d
	jle	.L2386
	movq	8(%rax), %rdx
	addl	$1, %edi
	movzbl	(%rdx,%r11), %edx
	movl	%esi, (%rax)
	movl	%edx, 24(%rax)
	jmp	.L2389
	.p2align 4,,10
	.p2align 3
.L2414:
	xorl	%edx, %edx
	cmpl	16(%rax), %r11d
	jge	.L2364
	movq	8(%rax), %rcx
	movslq	%r11d, %rdx
	movzbl	(%rcx,%rdx), %edx
.L2364:
	movl	%edx, 24(%rax)
	leal	1(%r11), %edx
	movl	%edx, (%rax)
	movabsq	$8589934590, %rax
	movq	%rax, -48(%rbp)
	movl	$46, -40(%rbp)
	jmp	.L2351
	.p2align 4,,10
	.p2align 3
.L2415:
	xorl	%edx, %edx
	cmpl	16(%rax), %r11d
	jge	.L2366
	movq	8(%rax), %rcx
	movslq	%r11d, %rdx
	movzbl	(%rcx,%rdx), %edx
.L2366:
	movl	%edx, 24(%rax)
	leal	1(%r11), %edx
	movl	%edx, (%rax)
	movabsq	$8589934590, %rax
	movq	%rax, -48(%rbp)
	movl	$41, -40(%rbp)
	jmp	.L2351
.L2417:
	xorl	%edx, %edx
	cmpl	%esi, %r11d
	jge	.L2381
	movq	8(%rax), %rcx
	movslq	%r11d, %rdx
	movzbl	(%rcx,%rdx), %edx
.L2381:
	movl	%edx, 24(%rax)
	leal	1(%r11), %edx
	movl	%edx, (%rax)
	movq	(%rdi), %rax
	movl	$-4, -48(%rbp)
	movl	(%rax), %eax
	movl	$-1, -40(%rbp)
	subl	%r11d, %eax
	movl	%eax, -44(%rbp)
	jmp	.L2351
.L2420:
	movl	%eax, %edx
	movl	$0, (%rcx)
	movl	$0, -4(%rcx,%rdx)
	jmp	.L2379
.L2410:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24991:
	.size	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv, .-_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	.section	.text._ZN2v88internal10DateParser16ParseES5DateTimeIKhEENS1_9DateTokenEPNS1_19DateStringTokenizerIT_EEPNS1_11DayComposerEPNS1_12TimeComposerEPNS1_16TimeZoneComposerE,"axG",@progbits,_ZN2v88internal10DateParser16ParseES5DateTimeIKhEENS1_9DateTokenEPNS1_19DateStringTokenizerIT_EEPNS1_11DayComposerEPNS1_12TimeComposerEPNS1_16TimeZoneComposerE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10DateParser16ParseES5DateTimeIKhEENS1_9DateTokenEPNS1_19DateStringTokenizerIT_EEPNS1_11DayComposerEPNS1_12TimeComposerEPNS1_16TimeZoneComposerE
	.type	_ZN2v88internal10DateParser16ParseES5DateTimeIKhEENS1_9DateTokenEPNS1_19DateStringTokenizerIT_EEPNS1_11DayComposerEPNS1_12TimeComposerEPNS1_16TimeZoneComposerE, @function
_ZN2v88internal10DateParser16ParseES5DateTimeIKhEENS1_9DateTokenEPNS1_19DateStringTokenizerIT_EEPNS1_11DayComposerEPNS1_12TimeComposerEPNS1_16TimeZoneComposerE:
.LFB24406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movl	8(%rdi), %r12d
	movl	12(%rdi), %r14d
	movl	16(%rdi), %r15d
	cmpl	$-2, %r12d
	je	.L2534
	cmpl	$-3, %r12d
	jne	.L2423
	cmpl	$4, %r14d
	je	.L2430
.L2423:
	movq	%rbx, %rdi
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movl	%r12d, -72(%rbp)
	movq	%rax, 8(%rbx)
	movl	%edx, 16(%rbx)
	movl	%r14d, -68(%rbp)
	movl	%r15d, -64(%rbp)
.L2426:
	movq	-72(%rbp), %rax
	movl	-64(%rbp), %edx
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2534:
	.cfi_restore_state
	leal	-43(%r15), %eax
	movq	%rcx, -112(%rbp)
	andl	$-3, %eax
	movq	%rdx, -104(%rbp)
	jne	.L2423
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	%rax, %rcx
	movq	%rax, 8(%rbx)
	movq	%rdx, %rsi
	shrq	$32, %rcx
	movl	%edx, 16(%rbx)
	cmpl	$6, %ecx
	jne	.L2533
	cmpl	$-3, 8(%rbx)
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	je	.L2424
.L2533:
	movl	$-2, -72(%rbp)
	movl	%r14d, -68(%rbp)
	movl	%r15d, -64(%rbp)
	jmp	.L2426
	.p2align 4,,10
	.p2align 3
.L2430:
	movq	%rcx, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	movq	%rax, 8(%rbx)
	movl	%edx, 16(%rbx)
	movl	12(%r13), %eax
	cmpl	$2, %eax
	jg	.L2429
	movslq	%eax, %rdx
	addl	$1, %eax
	movl	%r15d, 0(%r13,%rdx,4)
	movl	%eax, 12(%r13)
.L2429:
	movl	8(%rbx), %r12d
	movl	16(%rbx), %r15d
	cmpl	$-2, %r12d
	je	.L2535
.L2433:
	cmpl	$3, %r12d
	je	.L2440
	cmpl	$-1, %r12d
	jne	.L2464
.L2441:
	cmpl	$2147483647, 4(%r9)
	je	.L2536
.L2461:
	movl	$4294967295, %eax
	movb	$1, 20(%r13)
	movq	%rax, -72(%rbp)
	movl	$-1, -64(%rbp)
	jmp	.L2426
	.p2align 4,,10
	.p2align 3
.L2464:
	movl	12(%rbx), %r13d
	movq	%rbx, %rdi
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movl	%r12d, -72(%rbp)
	movq	%rax, 8(%rbx)
	movl	%edx, 16(%rbx)
	movl	%r13d, -68(%rbp)
	movl	%r15d, -64(%rbp)
	jmp	.L2426
	.p2align 4,,10
	.p2align 3
.L2535:
	cmpb	$45, %r15b
	jne	.L2464
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	%rax, 8(%rbx)
	movl	%eax, %esi
	shrq	$32, %rax
	movq	%rdx, %rcx
	movl	%edx, 16(%rbx)
	movq	%rax, %r15
	cmpl	$-3, %esi
	jne	.L2435
	cmpl	$2, %eax
	je	.L2537
.L2435:
	movq	%rbx, %rdi
	movq	%rcx, -112(%rbp)
	movl	%esi, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movl	-104(%rbp), %esi
	movq	-112(%rbp), %rcx
	movl	%r15d, -68(%rbp)
	movq	%rax, 8(%rbx)
	movl	%edx, 16(%rbx)
	movl	%esi, -72(%rbp)
	movl	%ecx, -64(%rbp)
	jmp	.L2426
	.p2align 4,,10
	.p2align 3
.L2424:
	movl	$44, %r12d
	movq	%rbx, %rdi
	movq	%rsi, -120(%rbp)
	subl	%r15d, %r12d
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	testl	%r12d, %r12d
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	movq	%rax, 8(%rbx)
	movq	-120(%rbp), %rsi
	movl	%edx, 16(%rbx)
	jns	.L2427
	testl	%esi, %esi
	je	.L2533
.L2427:
	movl	12(%r13), %eax
	cmpl	$2, %eax
	jg	.L2429
	imull	%esi, %r12d
	movslq	%eax, %rdx
	addl	$1, %eax
	movl	%r12d, 0(%r13,%rdx,4)
	movl	%eax, 12(%r13)
	jmp	.L2429
	.p2align 4,,10
	.p2align 3
.L2440:
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	%rax, 8(%rbx)
	movq	%rdx, %r12
	movl	%edx, 16(%rbx)
	cmpl	$-3, %eax
	jne	.L2442
	cmpl	$2, 12(%rbx)
	je	.L2538
.L2442:
	movl	$4294967290, %eax
	movl	$-1, -64(%rbp)
	movq	%rax, -72(%rbp)
	jmp	.L2426
	.p2align 4,,10
	.p2align 3
.L2536:
	movl	16(%r8), %eax
	testl	%eax, %eax
	jne	.L2461
	movq	$1, (%r9)
	movl	$0, 8(%r9)
	jmp	.L2461
.L2538:
	cmpl	$24, %edx
	ja	.L2442
	movq	%rbx, %rdi
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	movq	%rax, 8(%rbx)
	movl	%edx, 16(%rbx)
	movslq	16(%r8), %rax
	cmpl	$3, %eax
	jg	.L2444
	leal	1(%rax), %edx
	movl	%edx, 16(%r8)
	movl	%r12d, (%r8,%rax,4)
.L2444:
	cmpl	$-2, 8(%rbx)
	jne	.L2442
	cmpb	$58, 16(%rbx)
	jne	.L2442
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	%rax, 8(%rbx)
	movq	%rdx, %r15
	movl	%edx, 16(%rbx)
	cmpl	$-3, %eax
	jne	.L2442
	cmpl	$2, 12(%rbx)
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	jne	.L2442
	cmpl	$59, %edx
	ja	.L2442
	cmpl	$24, %r12d
	sete	%r12b
	testl	%edx, %edx
	je	.L2467
	testb	%r12b, %r12b
	jne	.L2442
.L2467:
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	movq	%rax, 8(%rbx)
	movl	%edx, 16(%rbx)
	movslq	16(%r8), %rax
	cmpl	$3, %eax
	jg	.L2446
	leal	1(%rax), %edx
	movl	%edx, 16(%r8)
	movl	%r15d, (%r8,%rax,4)
.L2446:
	cmpl	$-2, 8(%rbx)
	je	.L2539
.L2448:
	movl	8(%rbx), %edx
	movl	16(%rbx), %eax
	cmpl	$2, %edx
	je	.L2540
	cmpl	$-2, %edx
	jne	.L2457
	cmpb	$43, %al
	je	.L2458
	cmpb	$45, %al
	jne	.L2442
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	movl	%edx, 16(%rbx)
	movq	%rax, 8(%rbx)
	orl	$-1, %eax
.L2463:
	movl	%eax, (%r9)
	cmpl	$-3, 8(%rbx)
	jne	.L2442
	movl	12(%rbx), %eax
	movl	16(%rbx), %r12d
	cmpl	$4, %eax
	je	.L2541
	cmpl	$2, %eax
	jne	.L2442
	cmpl	$23, %r12d
	ja	.L2442
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	-112(%rbp), %r9
	movq	%rax, 8(%rbx)
	movl	%edx, 16(%rbx)
	movl	%r12d, 4(%r9)
	cmpl	$-2, 8(%rbx)
	jne	.L2442
	cmpb	$58, 16(%rbx)
	jne	.L2442
	movq	%rbx, %rdi
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	%rax, 8(%rbx)
	movq	%rdx, %r12
	movl	%edx, 16(%rbx)
	cmpl	$-3, %eax
	jne	.L2442
	cmpl	$2, 12(%rbx)
	jne	.L2442
	cmpl	$59, %edx
	ja	.L2442
	movq	%rbx, %rdi
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	-112(%rbp), %r9
	movq	-104(%rbp), %r8
	movq	%rax, 8(%rbx)
	movl	%edx, 16(%rbx)
	movl	%r12d, 8(%r9)
.L2457:
	cmpl	$-1, 8(%rbx)
	je	.L2441
	jmp	.L2442
	.p2align 4,,10
	.p2align 3
.L2537:
	leal	-1(%rdx), %eax
	cmpl	$11, %eax
	ja	.L2435
	movq	%rbx, %rdi
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	movq	%rax, 8(%rbx)
	movq	-120(%rbp), %rcx
	movl	%edx, 16(%rbx)
	movl	12(%r13), %eax
	cmpl	$2, %eax
	jg	.L2437
	movslq	%eax, %rdx
	addl	$1, %eax
	movl	%ecx, 0(%r13,%rdx,4)
	movl	%eax, 12(%r13)
.L2437:
	movl	8(%rbx), %eax
	movl	16(%rbx), %r15d
	cmpl	$-2, %eax
	je	.L2542
	movl	%eax, %r12d
	jmp	.L2433
.L2542:
	cmpb	$45, %r15b
	jne	.L2464
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	%rax, 8(%rbx)
	movl	12(%rbx), %r15d
	movq	%rax, %r12
	movq	%rdx, %rcx
	movl	%edx, 16(%rbx)
	cmpl	$2, %r15d
	jne	.L2438
	cmpl	$-3, %eax
	je	.L2543
.L2438:
	movq	%rbx, %rdi
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	-104(%rbp), %rcx
	movl	%r12d, -72(%rbp)
	movq	%rax, 8(%rbx)
	movl	%edx, 16(%rbx)
	movl	%r15d, -68(%rbp)
	movl	%ecx, -64(%rbp)
	jmp	.L2426
.L2543:
	leal	-1(%rdx), %eax
	cmpl	$30, %eax
	ja	.L2438
	movq	%rbx, %rdi
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	movq	%rax, 8(%rbx)
	movq	-120(%rbp), %rcx
	movq	%rdx, %r15
	movl	%edx, 16(%rbx)
	movl	12(%r13), %eax
	cmpl	$2, %eax
	jg	.L2532
	movslq	%eax, %rdx
	addl	$1, %eax
	movl	%ecx, 0(%r13,%rdx,4)
	movl	%eax, 12(%r13)
	movl	16(%rbx), %r15d
.L2532:
	movl	8(%rbx), %r12d
	jmp	.L2433
.L2540:
	cmpl	$1, 12(%rbx)
	jne	.L2442
	testl	%eax, %eax
	jne	.L2442
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	-112(%rbp), %r9
	movq	-104(%rbp), %r8
	movq	%rax, 8(%rbx)
	movl	%edx, 16(%rbx)
	movq	$1, (%r9)
	movl	$0, 8(%r9)
	jmp	.L2457
.L2539:
	cmpb	$58, 16(%rbx)
	jne	.L2448
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	%rax, 8(%rbx)
	cmpl	$2, 12(%rbx)
	movq	%rdx, %r15
	movl	%edx, 16(%rbx)
	jne	.L2442
	cmpl	$-3, 8(%rbx)
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	jne	.L2442
	cmpl	$59, %edx
	ja	.L2442
	testl	%edx, %edx
	je	.L2468
	testb	%r12b, %r12b
	jne	.L2442
.L2468:
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	movq	%rax, 8(%rbx)
	movl	%edx, 16(%rbx)
	movslq	16(%r8), %rax
	cmpl	$3, %eax
	jg	.L2451
	leal	1(%rax), %edx
	movl	%edx, 16(%r8)
	movl	%r15d, (%r8,%rax,4)
.L2451:
	cmpl	$-2, 8(%rbx)
	jne	.L2448
	cmpb	$46, 16(%rbx)
	jne	.L2448
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	cmpl	$-3, %eax
	movq	%rax, 8(%rbx)
	movl	%edx, 16(%rbx)
	jne	.L2442
	testl	%edx, %edx
	jle	.L2469
	testb	%r12b, %r12b
	jne	.L2442
.L2469:
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	12(%rbx), %r12
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	%r12, -56(%rbp)
	movl	-52(%rbp), %esi
	movl	%edx, 16(%rbx)
	movq	%rax, 8(%rbx)
	movl	$-3, -60(%rbp)
	movq	-60(%rbp), %rdi
	call	_ZN2v88internal10DateParser16ReadMillisecondsENS1_9DateTokenE@PLT
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	movl	%eax, %edx
	movslq	16(%r8), %rax
	cmpl	$3, %eax
	jg	.L2448
	leal	1(%rax), %ecx
	movl	%ecx, 16(%r8)
	movl	%edx, (%r8,%rax,4)
	jmp	.L2448
.L2541:
	movq	%rbx, %rdi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movl	$100, %ecx
	movq	%rax, 8(%rbx)
	movl	%r12d, %eax
	movl	%edx, 16(%rbx)
	cltd
	idivl	%ecx
	cmpl	$23, %eax
	ja	.L2442
	cmpl	$59, %edx
	ja	.L2442
	movq	-104(%rbp), %r9
	movl	%eax, 4(%r9)
	movl	%edx, 8(%r9)
	cmpl	$-1, 8(%rbx)
	je	.L2461
	jmp	.L2442
.L2458:
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	movq	%rax, 8(%rbx)
	movl	$1, %eax
	movl	%edx, 16(%rbx)
	jmp	.L2463
	.cfi_endproc
.LFE24406:
	.size	_ZN2v88internal10DateParser16ParseES5DateTimeIKhEENS1_9DateTokenEPNS1_19DateStringTokenizerIT_EEPNS1_11DayComposerEPNS1_12TimeComposerEPNS1_16TimeZoneComposerE, .-_ZN2v88internal10DateParser16ParseES5DateTimeIKhEENS1_9DateTokenEPNS1_19DateStringTokenizerIT_EEPNS1_11DayComposerEPNS1_12TimeComposerEPNS1_16TimeZoneComposerE
	.section	.text._ZN2v88internal10DateParser5ParseIKhEEbPNS0_7IsolateENS0_6VectorIT_EEPd,"axG",@progbits,_ZN2v88internal10DateParser5ParseIKhEEbPNS0_7IsolateENS0_6VectorIT_EEPd,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10DateParser5ParseIKhEEbPNS0_7IsolateENS0_6VectorIT_EEPd
	.type	_ZN2v88internal10DateParser5ParseIKhEEbPNS0_7IsolateENS0_6VectorIT_EEPd, @function
_ZN2v88internal10DateParser5ParseIKhEEbPNS0_7IsolateENS0_6VectorIT_EEPd:
.LFB23346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -256(%rbp)
	movq	%rcx, -240(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -136(%rbp)
	movq	%rdx, -128(%rbp)
	testl	%edx, %edx
	jle	.L2545
	movzbl	(%rsi), %eax
.L2545:
	leaq	-176(%rbp), %rbx
	movl	%eax, -120(%rbp)
	leaq	-144(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, -176(%rbp)
	movl	$1, -144(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	leaq	-212(%rbp), %rcx
	movq	%rbx, %rdi
	movb	$0, -60(%rbp)
	movq	%rax, -168(%rbp)
	movabsq	$9223372034707292159, %rax
	movq	%rax, -212(%rbp)
	subq	$2147483647, %rax
	movq	%rax, -96(%rbp)
	movq	%rax, -68(%rbp)
	leaq	-80(%rbp), %rax
	movl	%edx, -160(%rbp)
	movq	%rax, %rsi
	leaq	-112(%rbp), %rdx
	movl	$2147483647, -204(%rbp)
	movq	%rcx, -264(%rbp)
	movq	%rdx, -232(%rbp)
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal10DateParser16ParseES5DateTimeIKhEENS1_9DateTokenEPNS1_19DateStringTokenizerIT_EEPNS1_11DayComposerEPNS1_12TimeComposerEPNS1_16TimeZoneComposerE
	movl	%eax, %r14d
	cmpl	$-6, %eax
	je	.L2554
	movl	-68(%rbp), %esi
	movl	%edx, %r15d
	testl	%esi, %esi
	setne	%r13b
	xorl	%r12d, %r12d
	cmpl	$-1, %eax
	je	.L2548
	.p2align 4,,10
	.p2align 3
.L2588:
	cmpl	$-3, %r14d
	je	.L2657
	testl	%r14d, %r14d
	js	.L2573
	cmpl	$4, %r14d
	je	.L2658
	cmpl	$1, %r14d
	je	.L2659
	cmpl	$2, %r14d
	sete	%r12b
	andb	%r13b, %r12b
	je	.L2576
	movl	$1, %eax
	testl	%r15d, %r15d
	js	.L2660
.L2579:
	movl	%eax, -212(%rbp)
	movl	-168(%rbp), %r14d
	movl	%r12d, %r13d
	movl	$0, -204(%rbp)
	movl	%r15d, -208(%rbp)
	movl	-160(%rbp), %r15d
	.p2align 4,,10
	.p2align 3
.L2555:
	movq	%rbx, %rdi
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	%rax, -168(%rbp)
	movl	%edx, -160(%rbp)
	cmpl	$-1, %r14d
	jne	.L2588
.L2548:
	movq	-240(%rbp), %rsi
	movq	-248(%rbp), %rdi
	call	_ZN2v88internal10DateParser11DayComposer5WriteEPd@PLT
	testb	%al, %al
	jne	.L2661
	.p2align 4,,10
	.p2align 3
.L2554:
	xorl	%r12d, %r12d
.L2544:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2662
	addq	$232, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2657:
	.cfi_restore_state
	cmpl	$-2, -168(%rbp)
	je	.L2663
.L2550:
	cmpl	$2147483647, -208(%rbp)
	je	.L2564
.L2563:
	cmpl	$2147483647, -204(%rbp)
	je	.L2664
.L2564:
	movl	-96(%rbp), %eax
	cmpl	$1, %eax
	je	.L2602
.L2567:
	cmpl	$2, %eax
	je	.L2665
	cmpl	$3, %eax
	jne	.L2569
	cmpl	$999, %r15d
	ja	.L2569
	movl	$4, -96(%rbp)
	movl	%r15d, -100(%rbp)
	jmp	.L2595
	.p2align 4,,10
	.p2align 3
.L2573:
	cmpl	$-2, %r14d
	je	.L2580
.L2653:
	movl	-168(%rbp), %r14d
	movl	-160(%rbp), %r15d
	jmp	.L2555
	.p2align 4,,10
	.p2align 3
.L2667:
	movl	-208(%rbp), %eax
	orl	-204(%rbp), %eax
	je	.L2582
	movl	-96(%rbp), %eax
	testl	%eax, %eax
	je	.L2583
.L2582:
	movl	$44, %eax
	movl	-160(%rbp), %r12d
	subl	%r15d, %eax
	sarl	$31, %eax
	orl	$1, %eax
	movl	%eax, -212(%rbp)
	movl	-168(%rbp), %eax
	cmpl	$-3, %eax
	je	.L2666
	cmpl	$-2, %eax
	jne	.L2554
	cmpb	$58, %r12b
	jne	.L2554
	xorl	%eax, %eax
.L2594:
	movq	%rbx, %rdi
	movl	%eax, -208(%rbp)
	movl	%r12d, %r15d
	movl	$1, %r13d
	movl	$2147483647, -204(%rbp)
	movl	$1, %r12d
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	%rax, -168(%rbp)
	movl	%edx, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L2580:
	leal	-43(%r15), %eax
	andl	$-3, %eax
	je	.L2667
	cmpb	$41, %r15b
	jne	.L2653
.L2583:
	testb	%r13b, %r13b
	je	.L2653
	jmp	.L2554
	.p2align 4,,10
	.p2align 3
.L2658:
	movl	-96(%rbp), %edx
	testl	%edx, %edx
	jne	.L2575
.L2576:
	testb	%r13b, %r13b
	jne	.L2554
	movl	-168(%rbp), %r14d
	cmpl	$-3, %r14d
	je	.L2554
	movl	-160(%rbp), %r15d
	movl	$1, %r12d
	jmp	.L2555
	.p2align 4,,10
	.p2align 3
.L2663:
	movl	-160(%rbp), %eax
	cmpb	$58, %al
	je	.L2668
	cmpb	$46, %al
	jne	.L2550
	movq	%rbx, %rdi
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	%rax, -168(%rbp)
	movslq	-96(%rbp), %rax
	movl	%edx, -160(%rbp)
	cmpl	$1, %eax
	je	.L2669
	cmpl	$2, %eax
	je	.L2670
	cmpl	$3, %eax
	jne	.L2561
	cmpl	$999, %r15d
	ja	.L2561
.L2557:
	leal	1(%rax), %edx
	cmpl	$-3, -168(%rbp)
	movl	%r15d, -112(%rbp,%rax,4)
	movl	%edx, -96(%rbp)
	movq	-164(%rbp), %r12
	jne	.L2554
	movq	%rbx, %rdi
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	%r12, -184(%rbp)
	movl	-180(%rbp), %esi
	movl	$-3, -188(%rbp)
	movq	-188(%rbp), %rdi
	movq	%rax, -168(%rbp)
	movl	%edx, -160(%rbp)
	call	_ZN2v88internal10DateParser16ReadMillisecondsENS1_9DateTokenE@PLT
	testl	%eax, %eax
	js	.L2554
	movl	-96(%rbp), %edx
	cmpl	$3, %edx
	jg	.L2654
	leal	1(%rdx), %esi
	movslq	%edx, %rcx
	movl	-168(%rbp), %r14d
	movl	-160(%rbp), %r15d
	movl	%esi, -96(%rbp)
	movl	%eax, -112(%rbp,%rcx,4)
	cmpl	$4, %esi
	je	.L2608
	movl	$2, %eax
	movslq	%esi, %rsi
	movl	$1, %r12d
	movl	$1, %r13d
	subl	%edx, %eax
	leaq	4(,%rax,4), %rdx
	movq	-232(%rbp), %rax
	leaq	(%rax,%rsi,4), %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movl	$4, -96(%rbp)
	jmp	.L2555
	.p2align 4,,10
	.p2align 3
.L2659:
	movl	-168(%rbp), %r14d
	movl	%r15d, -64(%rbp)
	movl	$1, %r12d
	movl	-160(%rbp), %r15d
	cmpl	$-2, %r14d
	jne	.L2555
.L2656:
	cmpb	$45, %r15b
	je	.L2671
.L2572:
	movq	%rbx, %rdi
	movl	$1, %r12d
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	%rax, -168(%rbp)
	movl	%edx, -160(%rbp)
	jmp	.L2588
	.p2align 4,,10
	.p2align 3
.L2575:
	movl	%r15d, -92(%rbp)
	movl	-168(%rbp), %r14d
	movl	$1, %r12d
	movl	-160(%rbp), %r15d
	jmp	.L2555
	.p2align 4,,10
	.p2align 3
.L2665:
	cmpl	$59, %r15d
	ja	.L2569
.L2568:
	movslq	%eax, %rdx
	movq	-232(%rbp), %rsi
	xorl	%edi, %edi
	movl	%r15d, -112(%rbp,%rdx,4)
	movl	$2, %edx
	subl	%eax, %edx
	addl	$1, %eax
	cltq
	leaq	4(,%rdx,4), %rdx
	leaq	(%rsi,%rax,4), %rcx
	cmpl	$8, %edx
	jnb	.L2596
	testb	$4, %dl
	jne	.L2672
	testl	%edx, %edx
	jne	.L2673
.L2597:
	movl	$4, -96(%rbp)
.L2595:
	movl	-168(%rbp), %r14d
	movl	-160(%rbp), %r15d
	cmpl	$-1, %r14d
	setne	%r13b
	cmpl	$-4, %r14d
	setne	%al
	andb	%al, %r13b
	jne	.L2674
.L2608:
	movl	$1, %r12d
	movl	$1, %r13d
	jmp	.L2555
	.p2align 4,,10
	.p2align 3
.L2660:
	negl	%r15d
	movl	$-1, %eax
	jmp	.L2579
	.p2align 4,,10
	.p2align 3
.L2668:
	movq	%rbx, %rdi
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	%rax, -168(%rbp)
	movl	%eax, %r14d
	movl	%edx, -160(%rbp)
	cmpl	$-2, %eax
	je	.L2675
	movslq	-96(%rbp), %rax
	cmpl	$3, %eax
	jg	.L2554
	leal	1(%rax), %esi
	movl	%r15d, -112(%rbp,%rax,4)
	movl	%edx, %r15d
	movl	$1, %r12d
	movl	%esi, -96(%rbp)
	movl	$1, %r13d
	jmp	.L2555
	.p2align 4,,10
	.p2align 3
.L2669:
	cmpl	$59, %r15d
	jbe	.L2557
	cmpl	$2147483647, -208(%rbp)
	jne	.L2563
	.p2align 4,,10
	.p2align 3
.L2602:
	movl	$1, %eax
	cmpl	$59, %r15d
	jbe	.L2568
	.p2align 4,,10
	.p2align 3
.L2569:
	movl	-68(%rbp), %eax
	cmpl	$2, %eax
	jg	.L2554
	movl	-168(%rbp), %r14d
	movslq	%eax, %rdx
	addl	$1, %eax
	movl	$1, %r12d
	movl	%r15d, -80(%rbp,%rdx,4)
	movl	$1, %r13d
	movl	-160(%rbp), %r15d
	movl	%eax, -68(%rbp)
	cmpl	$-2, %r14d
	jne	.L2555
	jmp	.L2656
	.p2align 4,,10
	.p2align 3
.L2664:
	cmpl	$59, %r15d
	ja	.L2566
	movl	%r15d, -204(%rbp)
.L2654:
	movl	-168(%rbp), %r14d
	movl	-160(%rbp), %r15d
	movl	$1, %r12d
	movl	$1, %r13d
	jmp	.L2555
	.p2align 4,,10
	.p2align 3
.L2566:
	movl	-96(%rbp), %eax
	cmpl	$1, %eax
	jne	.L2567
	jmp	.L2569
	.p2align 4,,10
	.p2align 3
.L2671:
	movq	%rbx, %rdi
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	%rax, -168(%rbp)
	movl	%eax, %r14d
	movl	%edx, %r15d
	movl	%edx, -160(%rbp)
	jmp	.L2555
	.p2align 4,,10
	.p2align 3
.L2675:
	cmpb	$58, %dl
	je	.L2676
	movslq	-96(%rbp), %rax
	cmpl	$3, %eax
	jg	.L2554
	leal	1(%rax), %esi
	movl	%r15d, -112(%rbp,%rax,4)
	movl	%esi, -96(%rbp)
	cmpb	$46, %dl
	je	.L2589
	movl	%edx, %r15d
	movl	$1, %r13d
	jmp	.L2572
.L2670:
	cmpl	$59, %r15d
	jbe	.L2557
.L2561:
	cmpl	$2147483647, -208(%rbp)
	jne	.L2563
	jmp	.L2569
	.p2align 4,,10
	.p2align 3
.L2674:
	cmpl	$1, -164(%rbp)
	sete	%al
	cmpl	$2, %r14d
	sete	%dl
	andl	%edx, %eax
	testl	%r15d, %r15d
	sete	%dl
	andb	%dl, %al
	jne	.L2609
	cmpl	$-2, %r14d
	jne	.L2554
	leal	-43(%r15), %eax
	andl	$-3, %eax
	je	.L2572
	jmp	.L2554
	.p2align 4,,10
	.p2align 3
.L2596:
	leaq	8(%rcx), %rsi
	movl	%edx, %eax
	movq	$0, (%rcx)
	movq	$0, -8(%rcx,%rax)
	andq	$-8, %rsi
	subq	%rsi, %rcx
	addl	%ecx, %edx
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L2597
	andl	$-8, %edx
	xorl	%eax, %eax
.L2600:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	%rdi, (%rsi,%rcx)
	cmpl	%edx, %eax
	jb	.L2600
	jmp	.L2597
.L2673:
	movb	$0, (%rcx)
	jmp	.L2597
.L2661:
	movq	-240(%rbp), %rsi
	movq	-232(%rbp), %rdi
	call	_ZN2v88internal10DateParser12TimeComposer5WriteEPd@PLT
	testb	%al, %al
	je	.L2554
	movq	-240(%rbp), %rsi
	movq	-264(%rbp), %rdi
	call	_ZN2v88internal10DateParser16TimeZoneComposer5WriteEPd@PLT
	testb	%al, %al
	je	.L2554
	testb	%r12b, %r12b
	je	.L2615
	movq	-256(%rbp), %rdi
	movl	$33, %esi
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	jmp	.L2544
.L2676:
	movq	%rbx, %rdi
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movl	-96(%rbp), %ecx
	movq	%rax, -168(%rbp)
	movl	%edx, -160(%rbp)
	testl	%ecx, %ecx
	jne	.L2554
	movl	%r15d, -112(%rbp)
	movl	$1, %r12d
	movl	%edx, %r15d
	movl	-168(%rbp), %r14d
	movl	$2, -96(%rbp)
	movl	$1, %r13d
	movl	$0, -108(%rbp)
	jmp	.L2555
.L2666:
	movq	%rbx, %rdi
	movl	-164(%rbp), %r13d
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	%rax, -168(%rbp)
	movq	%rax, %rcx
	movl	%edx, %r15d
	movl	%edx, -160(%rbp)
	cmpl	$-2, %eax
	je	.L2677
.L2586:
	leal	-1(%r13), %eax
	cmpl	$1, %eax
	ja	.L2587
	movl	%r12d, -208(%rbp)
	movl	%ecx, %r14d
	movl	$1, %r12d
	movl	$1, %r13d
	movl	$0, -204(%rbp)
	jmp	.L2555
.L2587:
	subl	$3, %r13d
	cmpl	$1, %r13d
	ja	.L2554
	movslq	%r12d, %rax
	movl	%r12d, %edx
	movl	-168(%rbp), %r14d
	movl	-160(%rbp), %r15d
	imulq	$1374389535, %rax, %rax
	sarl	$31, %edx
	movl	$1, %r13d
	sarq	$37, %rax
	subl	%edx, %eax
	movl	%eax, -208(%rbp)
	imull	$100, %eax, %eax
	subl	%eax, %r12d
	movl	%r12d, -204(%rbp)
	movl	$1, %r12d
	jmp	.L2555
.L2589:
	movq	%rbx, %rdi
	movl	$1, %r12d
	movl	$1, %r13d
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKhE4ScanEv
	movq	%rax, -168(%rbp)
	movl	%eax, %r14d
	movl	%edx, %r15d
	movl	%edx, -160(%rbp)
	jmp	.L2555
.L2609:
	movl	%eax, %r13d
	movl	$2, %r14d
	xorl	%r15d, %r15d
	jmp	.L2572
.L2672:
	movl	%edx, %eax
	movl	$0, (%rcx)
	movl	$0, -4(%rcx,%rax)
	jmp	.L2597
.L2677:
	cmpb	$58, %dl
	jne	.L2586
	movl	%r12d, %eax
	movl	%edx, %r12d
	jmp	.L2594
.L2662:
	call	__stack_chk_fail@PLT
.L2615:
	movl	%eax, %r12d
	jmp	.L2544
	.cfi_endproc
.LFE23346:
	.size	_ZN2v88internal10DateParser5ParseIKhEEbPNS0_7IsolateENS0_6VectorIT_EEPd, .-_ZN2v88internal10DateParser5ParseIKhEEbPNS0_7IsolateENS0_6VectorIT_EEPd
	.section	.text._ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv,"axG",@progbits,_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	.type	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv, @function
_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv:
.LFB24993:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	24(%rax), %edx
	testl	%edx, %edx
	jne	.L2679
	movl	$4294967295, %eax
	movl	$-1, -40(%rbp)
	movq	%rax, -48(%rbp)
.L2680:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	movq	-48(%rbp), %rax
	movl	-40(%rbp), %edx
	jne	.L2739
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2679:
	.cfi_restore_state
	leal	-48(%rdx), %ecx
	movl	(%rax), %r8d
	cmpl	$9, %ecx
	jbe	.L2740
	cmpl	$58, %edx
	je	.L2741
	cmpl	$45, %edx
	je	.L2742
	cmpl	$43, %edx
	je	.L2743
	cmpl	$46, %edx
	je	.L2744
	cmpl	$41, %edx
	je	.L2745
	cmpl	$64, %edx
	jbe	.L2696
	movq	$0, -36(%rbp)
	xorl	%esi, %esi
	leaq	-36(%rbp), %r9
	movl	$0, -28(%rbp)
	movl	16(%rax), %edi
.L2697:
	cmpq	$2, %rsi
	ja	.L2699
	orl	$32, %edx
	movl	%edx, (%r9,%rsi,4)
.L2699:
	movl	(%rax), %ecx
	cmpl	%edi, %ecx
	jge	.L2700
	movq	8(%rax), %rdx
	movslq	%ecx, %r8
	addl	$1, %ecx
	leal	1(%rsi), %ebx
	addq	$1, %rsi
	movzwl	(%rdx,%r8,2), %edx
	movl	%ecx, (%rax)
	movl	%edx, 24(%rax)
	cmpl	$64, %edx
	ja	.L2697
	.p2align 4,,10
	.p2align 3
.L2701:
	cmpl	$2, %ebx
	jg	.L2708
	movl	$2, %eax
	movslq	%ebx, %rdx
	xorl	%edi, %edi
	subl	%ebx, %eax
	leaq	(%r9,%rdx,4), %rcx
	leaq	4(,%rax,4), %rax
	cmpl	$8, %eax
	jb	.L2746
	leaq	8(%rcx), %rsi
	movl	%eax, %edx
	movq	$0, (%rcx)
	movq	$0, -8(%rcx,%rdx)
	andq	$-8, %rsi
	subq	%rsi, %rcx
	addl	%ecx, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L2708
	andl	$-8, %eax
	xorl	%edx, %edx
.L2706:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	%rdi, (%rsi,%rcx)
	cmpl	%eax, %edx
	jb	.L2706
.L2708:
	movl	%ebx, %esi
	movq	%r9, %rdi
	call	_ZN2v88internal10DateParser12KeywordTable6LookupEPKji@PLT
	movl	%ebx, -44(%rbp)
	cltq
	leaq	(%rax,%rax,4), %rdx
	leaq	_ZN2v88internal10DateParser12KeywordTable5arrayE(%rip), %rax
	addq	%rdx, %rax
	movsbl	4(%rax), %edx
	movsbl	3(%rax), %eax
	movl	%edx, -40(%rbp)
	movl	%eax, -48(%rbp)
	jmp	.L2680
	.p2align 4,,10
	.p2align 3
.L2744:
	xorl	%edx, %edx
	cmpl	16(%rax), %r8d
	jge	.L2693
	movq	8(%rax), %rcx
	movslq	%r8d, %rdx
	movzwl	(%rcx,%rdx,2), %edx
.L2693:
	addl	$1, %r8d
	movl	%edx, 24(%rax)
	movl	%r8d, (%rax)
	movabsq	$8589934590, %rax
	movq	%rax, -48(%rbp)
	movl	$46, -40(%rbp)
	jmp	.L2680
	.p2align 4,,10
	.p2align 3
.L2700:
	addl	$1, %ecx
	movl	$0, 24(%rax)
	leal	1(%rsi), %ebx
	movl	%ecx, (%rax)
	jmp	.L2701
	.p2align 4,,10
	.p2align 3
.L2740:
	movl	16(%rax), %r11d
	movslq	%r8d, %rbx
	movl	%r8d, %ecx
	xorl	%esi, %esi
	xorl	%r9d, %r9d
.L2684:
	cmpq	$8, %rsi
	ja	.L2682
	leal	(%r9,%r9,4), %r9d
	leal	-48(%rdx,%r9,2), %r9d
.L2682:
	cmpl	%ecx, %r11d
	jle	.L2683
	movq	8(%rax), %rdx
	leaq	(%rbx,%rsi), %r10
	addl	$1, %ecx
	addq	$1, %rsi
	movzwl	(%rdx,%r10,2), %edx
	movl	%ecx, (%rax)
	leal	-48(%rdx), %r10d
	movl	%edx, 24(%rax)
	cmpl	$9, %r10d
	jbe	.L2684
	.p2align 4,,10
	.p2align 3
.L2685:
	movq	(%rdi), %rax
	movl	$-3, -48(%rbp)
	movl	%r9d, -40(%rbp)
	movl	(%rax), %eax
	subl	%r8d, %eax
	movl	%eax, -44(%rbp)
	jmp	.L2680
	.p2align 4,,10
	.p2align 3
.L2683:
	addl	$1, %ecx
	movl	$0, 24(%rax)
	movl	%ecx, (%rax)
	jmp	.L2685
	.p2align 4,,10
	.p2align 3
.L2696:
	movq	16(%rax), %rsi
	movslq	%edx, %rcx
	leaq	_ZN2v88internalL15kAsciiCharFlagsE(%rip), %r10
	movl	%esi, %r9d
	testb	$8, (%r10,%rcx)
	jne	.L2747
	cmpl	$40, %edx
	jne	.L2711
	movslq	%r8d, %rsi
	xorl	%edi, %edi
	leal	1(%r8), %ecx
	addq	%rsi, %rsi
	cmpl	$41, %edx
	je	.L2748
.L2712:
	cmpl	$40, %edx
	je	.L2749
.L2713:
	cmpl	%r8d, %r9d
	jle	.L2715
	movq	8(%rax), %rdx
	movzwl	(%rdx,%rsi), %edx
	movl	%ecx, (%rax)
	movl	%edx, 24(%rax)
	testl	%edi, %edi
	jle	.L2738
.L2718:
	addq	$2, %rsi
	testl	%edx, %edx
	je	.L2738
	movl	%ecx, %r8d
	leal	1(%r8), %ecx
	cmpl	$41, %edx
	jne	.L2712
.L2748:
	subl	$1, %edi
	jmp	.L2713
	.p2align 4,,10
	.p2align 3
.L2741:
	xorl	%edx, %edx
	cmpl	16(%rax), %r8d
	jge	.L2687
	movq	8(%rax), %rcx
	movslq	%r8d, %rdx
	movzwl	(%rcx,%rdx,2), %edx
.L2687:
	addl	$1, %r8d
	movl	%edx, 24(%rax)
	movl	%r8d, (%rax)
	movabsq	$8589934590, %rax
	movq	%rax, -48(%rbp)
	movl	$58, -40(%rbp)
	jmp	.L2680
	.p2align 4,,10
	.p2align 3
.L2742:
	xorl	%edx, %edx
	cmpl	16(%rax), %r8d
	jge	.L2689
	movq	8(%rax), %rcx
	movslq	%r8d, %rdx
	movzwl	(%rcx,%rdx,2), %edx
.L2689:
	addl	$1, %r8d
	movl	%edx, 24(%rax)
	movl	%r8d, (%rax)
	movabsq	$8589934590, %rax
	movq	%rax, -48(%rbp)
	movl	$45, -40(%rbp)
	jmp	.L2680
	.p2align 4,,10
	.p2align 3
.L2743:
	xorl	%edx, %edx
	cmpl	16(%rax), %r8d
	jge	.L2691
	movq	8(%rax), %rcx
	movslq	%r8d, %rdx
	movzwl	(%rcx,%rdx,2), %edx
.L2691:
	addl	$1, %r8d
	movl	%edx, 24(%rax)
	movl	%r8d, (%rax)
	movabsq	$8589934590, %rax
	movq	%rax, -48(%rbp)
	movl	$43, -40(%rbp)
	jmp	.L2680
	.p2align 4,,10
	.p2align 3
.L2745:
	xorl	%edx, %edx
	cmpl	16(%rax), %r8d
	jge	.L2695
	movq	8(%rax), %rcx
	movslq	%r8d, %rdx
	movzwl	(%rcx,%rdx,2), %edx
.L2695:
	addl	$1, %r8d
	movl	%edx, 24(%rax)
	movl	%r8d, (%rax)
	movabsq	$8589934590, %rax
	movq	%rax, -48(%rbp)
	movl	$41, -40(%rbp)
	jmp	.L2680
	.p2align 4,,10
	.p2align 3
.L2746:
	testb	$4, %al
	jne	.L2750
	testl	%eax, %eax
	je	.L2708
	movb	$0, (%rcx)
	jmp	.L2708
	.p2align 4,,10
	.p2align 3
.L2711:
	xorl	%edx, %edx
	cmpl	%esi, %r8d
	jge	.L2721
	movq	8(%rax), %rcx
	movslq	%r8d, %rdx
	movzwl	(%rcx,%rdx,2), %edx
.L2721:
	addl	$1, %r8d
	movl	%edx, 24(%rax)
	movl	%r8d, (%rax)
.L2738:
	movabsq	$8589934587, %rax
	movl	$-1, -40(%rbp)
	movq	%rax, -48(%rbp)
	jmp	.L2680
	.p2align 4,,10
	.p2align 3
.L2715:
	movl	$0, 24(%rax)
	movl	%ecx, (%rax)
	jmp	.L2738
	.p2align 4,,10
	.p2align 3
.L2749:
	cmpl	%r8d, %r9d
	jle	.L2715
	movq	8(%rax), %rdx
	addl	$1, %edi
	movzwl	(%rdx,%rsi), %edx
	movl	%ecx, (%rax)
	movl	%edx, 24(%rax)
	jmp	.L2718
.L2747:
	xorl	%edx, %edx
	cmpl	%esi, %r8d
	jge	.L2710
	movq	8(%rax), %rcx
	movslq	%r8d, %rdx
	movzwl	(%rcx,%rdx,2), %edx
.L2710:
	movl	%edx, 24(%rax)
	leal	1(%r8), %edx
	movl	%edx, (%rax)
	movq	(%rdi), %rax
	movl	$-4, -48(%rbp)
	movl	(%rax), %eax
	movl	$-1, -40(%rbp)
	subl	%r8d, %eax
	movl	%eax, -44(%rbp)
	jmp	.L2680
.L2750:
	movl	%eax, %edx
	movl	$0, (%rcx)
	movl	$0, -4(%rcx,%rdx)
	jmp	.L2708
.L2739:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24993:
	.size	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv, .-_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	.section	.text._ZN2v88internal10DateParser16ParseES5DateTimeIKtEENS1_9DateTokenEPNS1_19DateStringTokenizerIT_EEPNS1_11DayComposerEPNS1_12TimeComposerEPNS1_16TimeZoneComposerE,"axG",@progbits,_ZN2v88internal10DateParser16ParseES5DateTimeIKtEENS1_9DateTokenEPNS1_19DateStringTokenizerIT_EEPNS1_11DayComposerEPNS1_12TimeComposerEPNS1_16TimeZoneComposerE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10DateParser16ParseES5DateTimeIKtEENS1_9DateTokenEPNS1_19DateStringTokenizerIT_EEPNS1_11DayComposerEPNS1_12TimeComposerEPNS1_16TimeZoneComposerE
	.type	_ZN2v88internal10DateParser16ParseES5DateTimeIKtEENS1_9DateTokenEPNS1_19DateStringTokenizerIT_EEPNS1_11DayComposerEPNS1_12TimeComposerEPNS1_16TimeZoneComposerE, @function
_ZN2v88internal10DateParser16ParseES5DateTimeIKtEENS1_9DateTokenEPNS1_19DateStringTokenizerIT_EEPNS1_11DayComposerEPNS1_12TimeComposerEPNS1_16TimeZoneComposerE:
.LFB24416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movl	8(%rdi), %r12d
	movl	12(%rdi), %r14d
	movl	16(%rdi), %r15d
	cmpl	$-2, %r12d
	je	.L2864
	cmpl	$-3, %r12d
	jne	.L2753
	cmpl	$4, %r14d
	je	.L2760
.L2753:
	movq	%rbx, %rdi
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movl	%r12d, -72(%rbp)
	movq	%rax, 8(%rbx)
	movl	%edx, 16(%rbx)
	movl	%r14d, -68(%rbp)
	movl	%r15d, -64(%rbp)
.L2756:
	movq	-72(%rbp), %rax
	movl	-64(%rbp), %edx
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2864:
	.cfi_restore_state
	leal	-43(%r15), %eax
	movq	%rcx, -112(%rbp)
	andl	$-3, %eax
	movq	%rdx, -104(%rbp)
	jne	.L2753
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	%rax, %rcx
	movq	%rax, 8(%rbx)
	movq	%rdx, %rsi
	shrq	$32, %rcx
	movl	%edx, 16(%rbx)
	cmpl	$6, %ecx
	jne	.L2863
	cmpl	$-3, 8(%rbx)
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	je	.L2754
.L2863:
	movl	$-2, -72(%rbp)
	movl	%r14d, -68(%rbp)
	movl	%r15d, -64(%rbp)
	jmp	.L2756
	.p2align 4,,10
	.p2align 3
.L2760:
	movq	%rcx, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	movq	%rax, 8(%rbx)
	movl	%edx, 16(%rbx)
	movl	12(%r13), %eax
	cmpl	$2, %eax
	jg	.L2759
	movslq	%eax, %rdx
	addl	$1, %eax
	movl	%r15d, 0(%r13,%rdx,4)
	movl	%eax, 12(%r13)
.L2759:
	movl	8(%rbx), %r12d
	movl	16(%rbx), %r15d
	cmpl	$-2, %r12d
	je	.L2865
.L2763:
	cmpl	$3, %r12d
	je	.L2770
	cmpl	$-1, %r12d
	jne	.L2794
.L2771:
	cmpl	$2147483647, 4(%r9)
	je	.L2866
.L2791:
	movl	$4294967295, %eax
	movb	$1, 20(%r13)
	movq	%rax, -72(%rbp)
	movl	$-1, -64(%rbp)
	jmp	.L2756
	.p2align 4,,10
	.p2align 3
.L2794:
	movl	12(%rbx), %r13d
	movq	%rbx, %rdi
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movl	%r12d, -72(%rbp)
	movq	%rax, 8(%rbx)
	movl	%edx, 16(%rbx)
	movl	%r13d, -68(%rbp)
	movl	%r15d, -64(%rbp)
	jmp	.L2756
	.p2align 4,,10
	.p2align 3
.L2865:
	cmpb	$45, %r15b
	jne	.L2794
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	%rax, 8(%rbx)
	movl	%eax, %esi
	shrq	$32, %rax
	movq	%rdx, %rcx
	movl	%edx, 16(%rbx)
	movq	%rax, %r15
	cmpl	$-3, %esi
	jne	.L2765
	cmpl	$2, %eax
	je	.L2867
.L2765:
	movq	%rbx, %rdi
	movq	%rcx, -112(%rbp)
	movl	%esi, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movl	-104(%rbp), %esi
	movq	-112(%rbp), %rcx
	movl	%r15d, -68(%rbp)
	movq	%rax, 8(%rbx)
	movl	%edx, 16(%rbx)
	movl	%esi, -72(%rbp)
	movl	%ecx, -64(%rbp)
	jmp	.L2756
	.p2align 4,,10
	.p2align 3
.L2754:
	movl	$44, %r12d
	movq	%rbx, %rdi
	movq	%rsi, -120(%rbp)
	subl	%r15d, %r12d
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	testl	%r12d, %r12d
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	movq	%rax, 8(%rbx)
	movq	-120(%rbp), %rsi
	movl	%edx, 16(%rbx)
	jns	.L2757
	testl	%esi, %esi
	je	.L2863
.L2757:
	movl	12(%r13), %eax
	cmpl	$2, %eax
	jg	.L2759
	imull	%esi, %r12d
	movslq	%eax, %rdx
	addl	$1, %eax
	movl	%r12d, 0(%r13,%rdx,4)
	movl	%eax, 12(%r13)
	jmp	.L2759
	.p2align 4,,10
	.p2align 3
.L2770:
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	%rax, 8(%rbx)
	movq	%rdx, %r12
	movl	%edx, 16(%rbx)
	cmpl	$-3, %eax
	jne	.L2772
	cmpl	$2, 12(%rbx)
	je	.L2868
.L2772:
	movl	$4294967290, %eax
	movl	$-1, -64(%rbp)
	movq	%rax, -72(%rbp)
	jmp	.L2756
	.p2align 4,,10
	.p2align 3
.L2866:
	movl	16(%r8), %eax
	testl	%eax, %eax
	jne	.L2791
	movq	$1, (%r9)
	movl	$0, 8(%r9)
	jmp	.L2791
.L2868:
	cmpl	$24, %edx
	ja	.L2772
	movq	%rbx, %rdi
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	movq	%rax, 8(%rbx)
	movl	%edx, 16(%rbx)
	movslq	16(%r8), %rax
	cmpl	$3, %eax
	jg	.L2774
	leal	1(%rax), %edx
	movl	%edx, 16(%r8)
	movl	%r12d, (%r8,%rax,4)
.L2774:
	cmpl	$-2, 8(%rbx)
	jne	.L2772
	cmpb	$58, 16(%rbx)
	jne	.L2772
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	%rax, 8(%rbx)
	movq	%rdx, %r15
	movl	%edx, 16(%rbx)
	cmpl	$-3, %eax
	jne	.L2772
	cmpl	$2, 12(%rbx)
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	jne	.L2772
	cmpl	$59, %edx
	ja	.L2772
	cmpl	$24, %r12d
	sete	%r12b
	testl	%edx, %edx
	je	.L2797
	testb	%r12b, %r12b
	jne	.L2772
.L2797:
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	movq	%rax, 8(%rbx)
	movl	%edx, 16(%rbx)
	movslq	16(%r8), %rax
	cmpl	$3, %eax
	jg	.L2776
	leal	1(%rax), %edx
	movl	%edx, 16(%r8)
	movl	%r15d, (%r8,%rax,4)
.L2776:
	cmpl	$-2, 8(%rbx)
	je	.L2869
.L2778:
	movl	8(%rbx), %edx
	movl	16(%rbx), %eax
	cmpl	$2, %edx
	je	.L2870
	cmpl	$-2, %edx
	jne	.L2787
	cmpb	$43, %al
	je	.L2788
	cmpb	$45, %al
	jne	.L2772
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	movl	%edx, 16(%rbx)
	movq	%rax, 8(%rbx)
	orl	$-1, %eax
.L2793:
	movl	%eax, (%r9)
	cmpl	$-3, 8(%rbx)
	jne	.L2772
	movl	12(%rbx), %eax
	movl	16(%rbx), %r12d
	cmpl	$4, %eax
	je	.L2871
	cmpl	$2, %eax
	jne	.L2772
	cmpl	$23, %r12d
	ja	.L2772
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	-112(%rbp), %r9
	movq	%rax, 8(%rbx)
	movl	%edx, 16(%rbx)
	movl	%r12d, 4(%r9)
	cmpl	$-2, 8(%rbx)
	jne	.L2772
	cmpb	$58, 16(%rbx)
	jne	.L2772
	movq	%rbx, %rdi
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	%rax, 8(%rbx)
	movq	%rdx, %r12
	movl	%edx, 16(%rbx)
	cmpl	$-3, %eax
	jne	.L2772
	cmpl	$2, 12(%rbx)
	jne	.L2772
	cmpl	$59, %edx
	ja	.L2772
	movq	%rbx, %rdi
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	-112(%rbp), %r9
	movq	-104(%rbp), %r8
	movq	%rax, 8(%rbx)
	movl	%edx, 16(%rbx)
	movl	%r12d, 8(%r9)
.L2787:
	cmpl	$-1, 8(%rbx)
	je	.L2771
	jmp	.L2772
	.p2align 4,,10
	.p2align 3
.L2867:
	leal	-1(%rdx), %eax
	cmpl	$11, %eax
	ja	.L2765
	movq	%rbx, %rdi
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	movq	%rax, 8(%rbx)
	movq	-120(%rbp), %rcx
	movl	%edx, 16(%rbx)
	movl	12(%r13), %eax
	cmpl	$2, %eax
	jg	.L2767
	movslq	%eax, %rdx
	addl	$1, %eax
	movl	%ecx, 0(%r13,%rdx,4)
	movl	%eax, 12(%r13)
.L2767:
	movl	8(%rbx), %eax
	movl	16(%rbx), %r15d
	cmpl	$-2, %eax
	je	.L2872
	movl	%eax, %r12d
	jmp	.L2763
.L2872:
	cmpb	$45, %r15b
	jne	.L2794
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	%rax, 8(%rbx)
	movl	12(%rbx), %r15d
	movq	%rax, %r12
	movq	%rdx, %rcx
	movl	%edx, 16(%rbx)
	cmpl	$2, %r15d
	jne	.L2768
	cmpl	$-3, %eax
	je	.L2873
.L2768:
	movq	%rbx, %rdi
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	-104(%rbp), %rcx
	movl	%r12d, -72(%rbp)
	movq	%rax, 8(%rbx)
	movl	%edx, 16(%rbx)
	movl	%r15d, -68(%rbp)
	movl	%ecx, -64(%rbp)
	jmp	.L2756
.L2873:
	leal	-1(%rdx), %eax
	cmpl	$30, %eax
	ja	.L2768
	movq	%rbx, %rdi
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	movq	%rax, 8(%rbx)
	movq	-120(%rbp), %rcx
	movq	%rdx, %r15
	movl	%edx, 16(%rbx)
	movl	12(%r13), %eax
	cmpl	$2, %eax
	jg	.L2862
	movslq	%eax, %rdx
	addl	$1, %eax
	movl	%ecx, 0(%r13,%rdx,4)
	movl	%eax, 12(%r13)
	movl	16(%rbx), %r15d
.L2862:
	movl	8(%rbx), %r12d
	jmp	.L2763
.L2870:
	cmpl	$1, 12(%rbx)
	jne	.L2772
	testl	%eax, %eax
	jne	.L2772
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	-112(%rbp), %r9
	movq	-104(%rbp), %r8
	movq	%rax, 8(%rbx)
	movl	%edx, 16(%rbx)
	movq	$1, (%r9)
	movl	$0, 8(%r9)
	jmp	.L2787
.L2869:
	cmpb	$58, 16(%rbx)
	jne	.L2778
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	%rax, 8(%rbx)
	cmpl	$2, 12(%rbx)
	movq	%rdx, %r15
	movl	%edx, 16(%rbx)
	jne	.L2772
	cmpl	$-3, 8(%rbx)
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	jne	.L2772
	cmpl	$59, %edx
	ja	.L2772
	testl	%edx, %edx
	je	.L2798
	testb	%r12b, %r12b
	jne	.L2772
.L2798:
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	movq	%rax, 8(%rbx)
	movl	%edx, 16(%rbx)
	movslq	16(%r8), %rax
	cmpl	$3, %eax
	jg	.L2781
	leal	1(%rax), %edx
	movl	%edx, 16(%r8)
	movl	%r15d, (%r8,%rax,4)
.L2781:
	cmpl	$-2, 8(%rbx)
	jne	.L2778
	cmpb	$46, 16(%rbx)
	jne	.L2778
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	cmpl	$-3, %eax
	movq	%rax, 8(%rbx)
	movl	%edx, 16(%rbx)
	jne	.L2772
	testl	%edx, %edx
	jle	.L2799
	testb	%r12b, %r12b
	jne	.L2772
.L2799:
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	12(%rbx), %r12
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	%r12, -56(%rbp)
	movl	-52(%rbp), %esi
	movl	%edx, 16(%rbx)
	movq	%rax, 8(%rbx)
	movl	$-3, -60(%rbp)
	movq	-60(%rbp), %rdi
	call	_ZN2v88internal10DateParser16ReadMillisecondsENS1_9DateTokenE@PLT
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	movl	%eax, %edx
	movslq	16(%r8), %rax
	cmpl	$3, %eax
	jg	.L2778
	leal	1(%rax), %ecx
	movl	%ecx, 16(%r8)
	movl	%edx, (%r8,%rax,4)
	jmp	.L2778
.L2871:
	movq	%rbx, %rdi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movl	$100, %ecx
	movq	%rax, 8(%rbx)
	movl	%r12d, %eax
	movl	%edx, 16(%rbx)
	cltd
	idivl	%ecx
	cmpl	$23, %eax
	ja	.L2772
	cmpl	$59, %edx
	ja	.L2772
	movq	-104(%rbp), %r9
	movl	%eax, 4(%r9)
	movl	%edx, 8(%r9)
	cmpl	$-1, 8(%rbx)
	je	.L2791
	jmp	.L2772
.L2788:
	movq	%rbx, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	movq	%rax, 8(%rbx)
	movl	$1, %eax
	movl	%edx, 16(%rbx)
	jmp	.L2793
	.cfi_endproc
.LFE24416:
	.size	_ZN2v88internal10DateParser16ParseES5DateTimeIKtEENS1_9DateTokenEPNS1_19DateStringTokenizerIT_EEPNS1_11DayComposerEPNS1_12TimeComposerEPNS1_16TimeZoneComposerE, .-_ZN2v88internal10DateParser16ParseES5DateTimeIKtEENS1_9DateTokenEPNS1_19DateStringTokenizerIT_EEPNS1_11DayComposerEPNS1_12TimeComposerEPNS1_16TimeZoneComposerE
	.section	.text._ZN2v88internal10DateParser5ParseIKtEEbPNS0_7IsolateENS0_6VectorIT_EEPd,"axG",@progbits,_ZN2v88internal10DateParser5ParseIKtEEbPNS0_7IsolateENS0_6VectorIT_EEPd,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10DateParser5ParseIKtEEbPNS0_7IsolateENS0_6VectorIT_EEPd
	.type	_ZN2v88internal10DateParser5ParseIKtEEbPNS0_7IsolateENS0_6VectorIT_EEPd, @function
_ZN2v88internal10DateParser5ParseIKtEEbPNS0_7IsolateENS0_6VectorIT_EEPd:
.LFB23347:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -256(%rbp)
	movq	%rcx, -240(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -136(%rbp)
	movq	%rdx, -128(%rbp)
	testl	%edx, %edx
	jle	.L2875
	movzwl	(%rsi), %eax
.L2875:
	leaq	-176(%rbp), %rbx
	movl	%eax, -120(%rbp)
	leaq	-144(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, -176(%rbp)
	movl	$1, -144(%rbp)
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	leaq	-212(%rbp), %rcx
	movq	%rbx, %rdi
	movb	$0, -60(%rbp)
	movq	%rax, -168(%rbp)
	movabsq	$9223372034707292159, %rax
	movq	%rax, -212(%rbp)
	subq	$2147483647, %rax
	movq	%rax, -96(%rbp)
	movq	%rax, -68(%rbp)
	leaq	-80(%rbp), %rax
	movl	%edx, -160(%rbp)
	movq	%rax, %rsi
	leaq	-112(%rbp), %rdx
	movl	$2147483647, -204(%rbp)
	movq	%rcx, -264(%rbp)
	movq	%rdx, -232(%rbp)
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal10DateParser16ParseES5DateTimeIKtEENS1_9DateTokenEPNS1_19DateStringTokenizerIT_EEPNS1_11DayComposerEPNS1_12TimeComposerEPNS1_16TimeZoneComposerE
	movl	%eax, %r14d
	cmpl	$-6, %eax
	je	.L2884
	movl	-68(%rbp), %esi
	movl	%edx, %r15d
	testl	%esi, %esi
	setne	%r13b
	xorl	%r12d, %r12d
	cmpl	$-1, %eax
	je	.L2878
	.p2align 4,,10
	.p2align 3
.L2918:
	cmpl	$-3, %r14d
	je	.L2987
	testl	%r14d, %r14d
	js	.L2903
	cmpl	$4, %r14d
	je	.L2988
	cmpl	$1, %r14d
	je	.L2989
	cmpl	$2, %r14d
	sete	%r12b
	andb	%r13b, %r12b
	je	.L2906
	movl	$1, %eax
	testl	%r15d, %r15d
	js	.L2990
.L2909:
	movl	%eax, -212(%rbp)
	movl	-168(%rbp), %r14d
	movl	%r12d, %r13d
	movl	$0, -204(%rbp)
	movl	%r15d, -208(%rbp)
	movl	-160(%rbp), %r15d
	.p2align 4,,10
	.p2align 3
.L2885:
	movq	%rbx, %rdi
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	%rax, -168(%rbp)
	movl	%edx, -160(%rbp)
	cmpl	$-1, %r14d
	jne	.L2918
.L2878:
	movq	-240(%rbp), %rsi
	movq	-248(%rbp), %rdi
	call	_ZN2v88internal10DateParser11DayComposer5WriteEPd@PLT
	testb	%al, %al
	jne	.L2991
	.p2align 4,,10
	.p2align 3
.L2884:
	xorl	%r12d, %r12d
.L2874:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2992
	addq	$232, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2987:
	.cfi_restore_state
	cmpl	$-2, -168(%rbp)
	je	.L2993
.L2880:
	cmpl	$2147483647, -208(%rbp)
	je	.L2894
.L2893:
	cmpl	$2147483647, -204(%rbp)
	je	.L2994
.L2894:
	movl	-96(%rbp), %eax
	cmpl	$1, %eax
	je	.L2932
.L2897:
	cmpl	$2, %eax
	je	.L2995
	cmpl	$3, %eax
	jne	.L2899
	cmpl	$999, %r15d
	ja	.L2899
	movl	$4, -96(%rbp)
	movl	%r15d, -100(%rbp)
	jmp	.L2925
	.p2align 4,,10
	.p2align 3
.L2903:
	cmpl	$-2, %r14d
	je	.L2910
.L2983:
	movl	-168(%rbp), %r14d
	movl	-160(%rbp), %r15d
	jmp	.L2885
	.p2align 4,,10
	.p2align 3
.L2997:
	movl	-208(%rbp), %eax
	orl	-204(%rbp), %eax
	je	.L2912
	movl	-96(%rbp), %eax
	testl	%eax, %eax
	je	.L2913
.L2912:
	movl	$44, %eax
	movl	-160(%rbp), %r12d
	subl	%r15d, %eax
	sarl	$31, %eax
	orl	$1, %eax
	movl	%eax, -212(%rbp)
	movl	-168(%rbp), %eax
	cmpl	$-3, %eax
	je	.L2996
	cmpl	$-2, %eax
	jne	.L2884
	cmpb	$58, %r12b
	jne	.L2884
	xorl	%eax, %eax
.L2924:
	movq	%rbx, %rdi
	movl	%eax, -208(%rbp)
	movl	%r12d, %r15d
	movl	$1, %r13d
	movl	$2147483647, -204(%rbp)
	movl	$1, %r12d
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	%rax, -168(%rbp)
	movl	%edx, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L2910:
	leal	-43(%r15), %eax
	andl	$-3, %eax
	je	.L2997
	cmpb	$41, %r15b
	jne	.L2983
.L2913:
	testb	%r13b, %r13b
	je	.L2983
	jmp	.L2884
	.p2align 4,,10
	.p2align 3
.L2988:
	movl	-96(%rbp), %edx
	testl	%edx, %edx
	jne	.L2905
.L2906:
	testb	%r13b, %r13b
	jne	.L2884
	movl	-168(%rbp), %r14d
	cmpl	$-3, %r14d
	je	.L2884
	movl	-160(%rbp), %r15d
	movl	$1, %r12d
	jmp	.L2885
	.p2align 4,,10
	.p2align 3
.L2993:
	movl	-160(%rbp), %eax
	cmpb	$58, %al
	je	.L2998
	cmpb	$46, %al
	jne	.L2880
	movq	%rbx, %rdi
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	%rax, -168(%rbp)
	movslq	-96(%rbp), %rax
	movl	%edx, -160(%rbp)
	cmpl	$1, %eax
	je	.L2999
	cmpl	$2, %eax
	je	.L3000
	cmpl	$3, %eax
	jne	.L2891
	cmpl	$999, %r15d
	ja	.L2891
.L2887:
	leal	1(%rax), %edx
	cmpl	$-3, -168(%rbp)
	movl	%r15d, -112(%rbp,%rax,4)
	movl	%edx, -96(%rbp)
	movq	-164(%rbp), %r12
	jne	.L2884
	movq	%rbx, %rdi
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	%r12, -184(%rbp)
	movl	-180(%rbp), %esi
	movl	$-3, -188(%rbp)
	movq	-188(%rbp), %rdi
	movq	%rax, -168(%rbp)
	movl	%edx, -160(%rbp)
	call	_ZN2v88internal10DateParser16ReadMillisecondsENS1_9DateTokenE@PLT
	testl	%eax, %eax
	js	.L2884
	movl	-96(%rbp), %edx
	cmpl	$3, %edx
	jg	.L2984
	leal	1(%rdx), %esi
	movslq	%edx, %rcx
	movl	-168(%rbp), %r14d
	movl	-160(%rbp), %r15d
	movl	%esi, -96(%rbp)
	movl	%eax, -112(%rbp,%rcx,4)
	cmpl	$4, %esi
	je	.L2938
	movl	$2, %eax
	movslq	%esi, %rsi
	movl	$1, %r12d
	movl	$1, %r13d
	subl	%edx, %eax
	leaq	4(,%rax,4), %rdx
	movq	-232(%rbp), %rax
	leaq	(%rax,%rsi,4), %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movl	$4, -96(%rbp)
	jmp	.L2885
	.p2align 4,,10
	.p2align 3
.L2989:
	movl	-168(%rbp), %r14d
	movl	%r15d, -64(%rbp)
	movl	$1, %r12d
	movl	-160(%rbp), %r15d
	cmpl	$-2, %r14d
	jne	.L2885
.L2986:
	cmpb	$45, %r15b
	je	.L3001
.L2902:
	movq	%rbx, %rdi
	movl	$1, %r12d
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	%rax, -168(%rbp)
	movl	%edx, -160(%rbp)
	jmp	.L2918
	.p2align 4,,10
	.p2align 3
.L2905:
	movl	%r15d, -92(%rbp)
	movl	-168(%rbp), %r14d
	movl	$1, %r12d
	movl	-160(%rbp), %r15d
	jmp	.L2885
	.p2align 4,,10
	.p2align 3
.L2995:
	cmpl	$59, %r15d
	ja	.L2899
.L2898:
	movslq	%eax, %rdx
	movq	-232(%rbp), %rsi
	xorl	%edi, %edi
	movl	%r15d, -112(%rbp,%rdx,4)
	movl	$2, %edx
	subl	%eax, %edx
	addl	$1, %eax
	cltq
	leaq	4(,%rdx,4), %rdx
	leaq	(%rsi,%rax,4), %rcx
	cmpl	$8, %edx
	jnb	.L2926
	testb	$4, %dl
	jne	.L3002
	testl	%edx, %edx
	jne	.L3003
.L2927:
	movl	$4, -96(%rbp)
.L2925:
	movl	-168(%rbp), %r14d
	movl	-160(%rbp), %r15d
	cmpl	$-1, %r14d
	setne	%r13b
	cmpl	$-4, %r14d
	setne	%al
	andb	%al, %r13b
	jne	.L3004
.L2938:
	movl	$1, %r12d
	movl	$1, %r13d
	jmp	.L2885
	.p2align 4,,10
	.p2align 3
.L2990:
	negl	%r15d
	movl	$-1, %eax
	jmp	.L2909
	.p2align 4,,10
	.p2align 3
.L2998:
	movq	%rbx, %rdi
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	%rax, -168(%rbp)
	movl	%eax, %r14d
	movl	%edx, -160(%rbp)
	cmpl	$-2, %eax
	je	.L3005
	movslq	-96(%rbp), %rax
	cmpl	$3, %eax
	jg	.L2884
	leal	1(%rax), %esi
	movl	%r15d, -112(%rbp,%rax,4)
	movl	%edx, %r15d
	movl	$1, %r12d
	movl	%esi, -96(%rbp)
	movl	$1, %r13d
	jmp	.L2885
	.p2align 4,,10
	.p2align 3
.L2999:
	cmpl	$59, %r15d
	jbe	.L2887
	cmpl	$2147483647, -208(%rbp)
	jne	.L2893
	.p2align 4,,10
	.p2align 3
.L2932:
	movl	$1, %eax
	cmpl	$59, %r15d
	jbe	.L2898
	.p2align 4,,10
	.p2align 3
.L2899:
	movl	-68(%rbp), %eax
	cmpl	$2, %eax
	jg	.L2884
	movl	-168(%rbp), %r14d
	movslq	%eax, %rdx
	addl	$1, %eax
	movl	$1, %r12d
	movl	%r15d, -80(%rbp,%rdx,4)
	movl	$1, %r13d
	movl	-160(%rbp), %r15d
	movl	%eax, -68(%rbp)
	cmpl	$-2, %r14d
	jne	.L2885
	jmp	.L2986
	.p2align 4,,10
	.p2align 3
.L2994:
	cmpl	$59, %r15d
	ja	.L2896
	movl	%r15d, -204(%rbp)
.L2984:
	movl	-168(%rbp), %r14d
	movl	-160(%rbp), %r15d
	movl	$1, %r12d
	movl	$1, %r13d
	jmp	.L2885
	.p2align 4,,10
	.p2align 3
.L2896:
	movl	-96(%rbp), %eax
	cmpl	$1, %eax
	jne	.L2897
	jmp	.L2899
	.p2align 4,,10
	.p2align 3
.L3001:
	movq	%rbx, %rdi
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	%rax, -168(%rbp)
	movl	%eax, %r14d
	movl	%edx, %r15d
	movl	%edx, -160(%rbp)
	jmp	.L2885
	.p2align 4,,10
	.p2align 3
.L3005:
	cmpb	$58, %dl
	je	.L3006
	movslq	-96(%rbp), %rax
	cmpl	$3, %eax
	jg	.L2884
	leal	1(%rax), %esi
	movl	%r15d, -112(%rbp,%rax,4)
	movl	%esi, -96(%rbp)
	cmpb	$46, %dl
	je	.L2919
	movl	%edx, %r15d
	movl	$1, %r13d
	jmp	.L2902
.L3000:
	cmpl	$59, %r15d
	jbe	.L2887
.L2891:
	cmpl	$2147483647, -208(%rbp)
	jne	.L2893
	jmp	.L2899
	.p2align 4,,10
	.p2align 3
.L3004:
	cmpl	$1, -164(%rbp)
	sete	%al
	cmpl	$2, %r14d
	sete	%dl
	andl	%edx, %eax
	testl	%r15d, %r15d
	sete	%dl
	andb	%dl, %al
	jne	.L2939
	cmpl	$-2, %r14d
	jne	.L2884
	leal	-43(%r15), %eax
	andl	$-3, %eax
	je	.L2902
	jmp	.L2884
	.p2align 4,,10
	.p2align 3
.L2926:
	leaq	8(%rcx), %rsi
	movl	%edx, %eax
	movq	$0, (%rcx)
	movq	$0, -8(%rcx,%rax)
	andq	$-8, %rsi
	subq	%rsi, %rcx
	addl	%ecx, %edx
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L2927
	andl	$-8, %edx
	xorl	%eax, %eax
.L2930:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	%rdi, (%rsi,%rcx)
	cmpl	%edx, %eax
	jb	.L2930
	jmp	.L2927
.L3003:
	movb	$0, (%rcx)
	jmp	.L2927
.L2991:
	movq	-240(%rbp), %rsi
	movq	-232(%rbp), %rdi
	call	_ZN2v88internal10DateParser12TimeComposer5WriteEPd@PLT
	testb	%al, %al
	je	.L2884
	movq	-240(%rbp), %rsi
	movq	-264(%rbp), %rdi
	call	_ZN2v88internal10DateParser16TimeZoneComposer5WriteEPd@PLT
	testb	%al, %al
	je	.L2884
	testb	%r12b, %r12b
	je	.L2945
	movq	-256(%rbp), %rdi
	movl	$33, %esi
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	jmp	.L2874
.L3006:
	movq	%rbx, %rdi
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movl	-96(%rbp), %ecx
	movq	%rax, -168(%rbp)
	movl	%edx, -160(%rbp)
	testl	%ecx, %ecx
	jne	.L2884
	movl	%r15d, -112(%rbp)
	movl	$1, %r12d
	movl	%edx, %r15d
	movl	-168(%rbp), %r14d
	movl	$2, -96(%rbp)
	movl	$1, %r13d
	movl	$0, -108(%rbp)
	jmp	.L2885
.L2996:
	movq	%rbx, %rdi
	movl	-164(%rbp), %r13d
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	%rax, -168(%rbp)
	movq	%rax, %rcx
	movl	%edx, %r15d
	movl	%edx, -160(%rbp)
	cmpl	$-2, %eax
	je	.L3007
.L2916:
	leal	-1(%r13), %eax
	cmpl	$1, %eax
	ja	.L2917
	movl	%r12d, -208(%rbp)
	movl	%ecx, %r14d
	movl	$1, %r12d
	movl	$1, %r13d
	movl	$0, -204(%rbp)
	jmp	.L2885
.L2917:
	subl	$3, %r13d
	cmpl	$1, %r13d
	ja	.L2884
	movslq	%r12d, %rax
	movl	%r12d, %edx
	movl	-168(%rbp), %r14d
	movl	-160(%rbp), %r15d
	imulq	$1374389535, %rax, %rax
	sarl	$31, %edx
	movl	$1, %r13d
	sarq	$37, %rax
	subl	%edx, %eax
	movl	%eax, -208(%rbp)
	imull	$100, %eax, %eax
	subl	%eax, %r12d
	movl	%r12d, -204(%rbp)
	movl	$1, %r12d
	jmp	.L2885
.L2919:
	movq	%rbx, %rdi
	movl	$1, %r12d
	movl	$1, %r13d
	call	_ZN2v88internal10DateParser19DateStringTokenizerIKtE4ScanEv
	movq	%rax, -168(%rbp)
	movl	%eax, %r14d
	movl	%edx, %r15d
	movl	%edx, -160(%rbp)
	jmp	.L2885
.L2939:
	movl	%eax, %r13d
	movl	$2, %r14d
	xorl	%r15d, %r15d
	jmp	.L2902
.L3002:
	movl	%edx, %eax
	movl	$0, (%rcx)
	movl	$0, -4(%rcx,%rax)
	jmp	.L2927
.L3007:
	cmpb	$58, %dl
	jne	.L2916
	movl	%r12d, %eax
	movl	%edx, %r12d
	jmp	.L2924
.L2992:
	call	__stack_chk_fail@PLT
.L2945:
	movl	%eax, %r12d
	jmp	.L2874
	.cfi_endproc
.LFE23347:
	.size	_ZN2v88internal10DateParser5ParseIKtEEbPNS0_7IsolateENS0_6VectorIT_EEPd, .-_ZN2v88internal10DateParser5ParseIKtEEbPNS0_7IsolateENS0_6VectorIT_EEPd
	.section	.text._ZN2v88internal12_GLOBAL__N_119ParseDateTimeStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_119ParseDateTimeStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal12_GLOBAL__N_119ParseDateTimeStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE:
.LFB21339:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$112, %rsp
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	movq	%rdx, %r12
	movq	-1(%rdx), %rcx
	cmpw	$63, 11(%rcx)
	jbe	.L3062
.L3010:
	movq	-1(%r12), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L3019
.L3061:
	movq	(%rax), %rsi
.L3018:
	movq	%rsi, -104(%rbp)
	leaq	-104(%rbp), %rdi
	leaq	-105(%rbp), %rsi
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	leaq	-96(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rdx, %rax
	movslq	%edx, %rdx
	shrq	$32, %rax
	cmpl	$1, %eax
	je	.L3063
	call	_ZN2v88internal10DateParser5ParseIKtEEbPNS0_7IsolateENS0_6VectorIT_EEPd
.L3026:
	movsd	.LC1(%rip), %xmm13
	testb	%al, %al
	jne	.L3064
.L3027:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3065
	addq	$112, %rsp
	movapd	%xmm13, %xmm0
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3064:
	.cfi_restore_state
	movsd	-96(%rbp), %xmm0
	comisd	.LC35(%rip), %xmm0
	jb	.L3028
	movsd	.LC36(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L3066
	.p2align 4,,10
	.p2align 3
.L3028:
	movsd	-40(%rbp), %xmm1
	ucomisd	%xmm1, %xmm1
	jp	.L3067
	mulsd	.LC8(%rip), %xmm1
	movapd	%xmm13, %xmm0
	subsd	%xmm1, %xmm0
.L3036:
	call	_ZN2v88internal9DateCache8TimeClipEd@PLT
	movapd	%xmm0, %xmm13
	jmp	.L3027
	.p2align 4,,10
	.p2align 3
.L3019:
	movq	-1(%r12), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$5, %dx
	jne	.L3061
	movq	(%rax), %rax
	movq	41112(%rbx), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L3022
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L3061
	.p2align 4,,10
	.p2align 3
.L3062:
	movq	-1(%rdx), %rcx
	movzwl	11(%rcx), %ecx
	andl	$7, %ecx
	cmpw	$1, %cx
	jne	.L3010
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L3013
	movq	23(%rdx), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	je	.L3013
	xorl	%edx, %edx
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %rsi
	jmp	.L3018
	.p2align 4,,10
	.p2align 3
.L3063:
	call	_ZN2v88internal10DateParser5ParseIKhEEbPNS0_7IsolateENS0_6VectorIT_EEPd
	jmp	.L3026
	.p2align 4,,10
	.p2align 3
.L3066:
	movsd	-88(%rbp), %xmm1
	comisd	.LC37(%rip), %xmm1
	jb	.L3028
	movsd	.LC38(%rip), %xmm2
	comisd	%xmm1, %xmm2
	jb	.L3028
	movsd	-80(%rbp), %xmm2
	call	_ZN2v88internal12_GLOBAL__N_17MakeDayEddd.part.0
	movsd	-48(%rbp), %xmm3
	movsd	-56(%rbp), %xmm2
	movapd	%xmm0, %xmm14
	movsd	-64(%rbp), %xmm1
	movsd	-72(%rbp), %xmm0
	call	_ZN2v88internal12_GLOBAL__N_18MakeTimeEdddd
	movq	.LC2(%rip), %xmm1
	movsd	.LC3(%rip), %xmm2
	movapd	%xmm14, %xmm3
	andpd	%xmm1, %xmm3
	ucomisd	%xmm3, %xmm2
	jb	.L3028
	andpd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm2
	jb	.L3028
	mulsd	.LC26(%rip), %xmm14
	movapd	%xmm14, %xmm13
	addsd	%xmm0, %xmm13
	jmp	.L3028
	.p2align 4,,10
	.p2align 3
.L3022:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L3068
.L3024:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L3018
	.p2align 4,,10
	.p2align 3
.L3013:
	movq	41112(%rbx), %rdi
	movq	15(%rdx), %r12
	testq	%rdi, %rdi
	je	.L3015
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
	jmp	.L3010
	.p2align 4,,10
	.p2align 3
.L3015:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L3069
.L3017:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r12, (%rax)
	jmp	.L3010
	.p2align 4,,10
	.p2align 3
.L3067:
	comisd	.LC29(%rip), %xmm13
	jb	.L3059
	movsd	.LC30(%rip), %xmm0
	comisd	%xmm13, %xmm0
	jb	.L3059
	movq	41240(%rbx), %rdi
	cvttsd2siq	%xmm13, %rbx
	xorl	%edx, %edx
	movq	(%rdi), %rax
	movq	%rbx, %rsi
	call	*24(%rax)
	pxor	%xmm0, %xmm0
	cltq
	subq	%rax, %rbx
	cvtsi2sdq	%rbx, %xmm0
	jmp	.L3036
	.p2align 4,,10
	.p2align 3
.L3059:
	movsd	.LC1(%rip), %xmm13
	jmp	.L3027
.L3069:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L3017
	.p2align 4,,10
	.p2align 3
.L3068:
	movq	%rbx, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	jmp	.L3024
.L3065:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21339:
	.size	_ZN2v88internal12_GLOBAL__N_119ParseDateTimeStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal12_GLOBAL__N_119ParseDateTimeStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.section	.rodata._ZN2v88internalL28Builtin_Impl_Stats_DateParseEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC75:
	.string	"V8.Builtin_DateParse"
	.section	.text._ZN2v88internalL28Builtin_Impl_Stats_DateParseEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Builtin_Impl_Stats_DateParseEiPmPNS0_7IsolateE, @function
_ZN2v88internalL28Builtin_Impl_Stats_DateParseEiPmPNS0_7IsolateE:
.LFB21350:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3109
.L3071:
	movq	_ZZN2v88internalL28Builtin_Impl_Stats_DateParseEiPmPNS0_7IsolateEE28trace_event_unique_atomic300(%rip), %r13
	testq	%r13, %r13
	je	.L3110
.L3073:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L3111
.L3075:
	addl	$1, 41104(%r12)
	leaq	-8(%rbx), %rsi
	leaq	88(%r12), %rax
	cmpl	$5, %r15d
	cmovle	%rax, %rsi
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L3081
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3112
.L3083:
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_119ParseDateTimeStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rax), %r15
.L3085:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L3088
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3088:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3113
.L3070:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3114
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3081:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L3083
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L3083
	.p2align 4,,10
	.p2align 3
.L3112:
	movq	312(%r12), %r15
	jmp	.L3085
	.p2align 4,,10
	.p2align 3
.L3111:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3115
.L3076:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3077
	movq	(%rdi), %rax
	call	*8(%rax)
.L3077:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3078
	movq	(%rdi), %rax
	call	*8(%rax)
.L3078:
	leaq	.LC75(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L3075
	.p2align 4,,10
	.p2align 3
.L3110:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3116
.L3074:
	movq	%r13, _ZZN2v88internalL28Builtin_Impl_Stats_DateParseEiPmPNS0_7IsolateEE28trace_event_unique_atomic300(%rip)
	jmp	.L3073
	.p2align 4,,10
	.p2align 3
.L3113:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3070
	.p2align 4,,10
	.p2align 3
.L3109:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$740, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3071
	.p2align 4,,10
	.p2align 3
.L3116:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L3074
	.p2align 4,,10
	.p2align 3
.L3115:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC75(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L3076
.L3114:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21350:
	.size	_ZN2v88internalL28Builtin_Impl_Stats_DateParseEiPmPNS0_7IsolateE, .-_ZN2v88internalL28Builtin_Impl_Stats_DateParseEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal17Builtin_DateParseEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal17Builtin_DateParseEiPmPNS0_7IsolateE
	.type	_ZN2v88internal17Builtin_DateParseEiPmPNS0_7IsolateE, @function
_ZN2v88internal17Builtin_DateParseEiPmPNS0_7IsolateE:
.LFB21351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3130
	addl	$1, 41104(%rdx)
	leaq	88(%rdx), %rax
	subq	$8, %rsi
	cmpl	$5, %edi
	cmovle	%rax, %rsi
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L3121
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3131
.L3123:
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_119ParseDateTimeStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rax), %r14
.L3125:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3117
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3117:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3121:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L3123
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L3123
	.p2align 4,,10
	.p2align 3
.L3131:
	movq	312(%r12), %r14
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3130:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL28Builtin_Impl_Stats_DateParseEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21351:
	.size	_ZN2v88internal17Builtin_DateParseEiPmPNS0_7IsolateE, .-_ZN2v88internal17Builtin_DateParseEiPmPNS0_7IsolateE
	.section	.text._ZN2v84base11SmallVectorIcLm128EE4GrowEm,"axG",@progbits,_ZN2v84base11SmallVectorIcLm128EE4GrowEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base11SmallVectorIcLm128EE4GrowEm
	.type	_ZN2v84base11SmallVectorIcLm128EE4GrowEm, @function
_ZN2v84base11SmallVectorIcLm128EE4GrowEm:
.LFB24995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	8(%rdi), %r13
	movq	16(%rdi), %rdi
	subq	%rax, %r13
	subq	%rax, %rdi
	addq	%rdi, %rdi
	cmpq	%rsi, %rdi
	cmovb	%rsi, %rdi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo64Em@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	malloc@PLT
	movq	(%rbx), %r15
	movq	%r13, %rdx
	movq	%rax, %rdi
	movq	%rax, %r12
	movq	%r15, %rsi
	call	memcpy@PLT
	leaq	24(%rbx), %rax
	cmpq	%rax, %r15
	je	.L3133
	movq	%r15, %rdi
	call	free@PLT
.L3133:
	leaq	(%r12,%r13), %rax
	movq	%r12, (%rbx)
	addq	%r14, %r12
	movq	%r12, 16(%rbx)
	movq	%rax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24995:
	.size	_ZN2v84base11SmallVectorIcLm128EE4GrowEm, .-_ZN2v84base11SmallVectorIcLm128EE4GrowEm
	.section	.text._ZN2v88internal29SmallStringOptimizedAllocatorILm128EE4growEPj,"axG",@progbits,_ZN2v88internal29SmallStringOptimizedAllocatorILm128EE4growEPj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal29SmallStringOptimizedAllocatorILm128EE4growEPj
	.type	_ZN2v88internal29SmallStringOptimizedAllocatorILm128EE4growEPj, @function
_ZN2v88internal29SmallStringOptimizedAllocatorILm128EE4growEPj:
.LFB26144:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %edx
	movq	8(%rdi), %r13
	leal	(%rdx,%rdx), %r14d
	movq	0(%r13), %rax
	cmpl	%r14d, %edx
	jnb	.L3135
	movq	16(%r13), %rdx
	movl	%r14d, %r15d
	movq	%rdi, %rbx
	movq	%rsi, %r12
	subq	%rax, %rdx
	cmpq	%rdx, %r15
	ja	.L3139
.L3137:
	addq	%r15, %rax
	movq	%rax, 8(%r13)
	movl	%r14d, (%r12)
	movq	8(%rbx), %rax
	movq	(%rax), %rax
.L3135:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3139:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v84base11SmallVectorIcLm128EE4GrowEm
	movq	0(%r13), %rax
	jmp	.L3137
	.cfi_endproc
.LFE26144:
	.size	_ZN2v88internal29SmallStringOptimizedAllocatorILm128EE4growEPj, .-_ZN2v88internal29SmallStringOptimizedAllocatorILm128EE4growEPj
	.section	.text._ZN2v88internal29SmallStringOptimizedAllocatorILm128EE8allocateEj,"axG",@progbits,_ZN2v88internal29SmallStringOptimizedAllocatorILm128EE8allocateEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal29SmallStringOptimizedAllocatorILm128EE8allocateEj
	.type	_ZN2v88internal29SmallStringOptimizedAllocatorILm128EE8allocateEj, @function
_ZN2v88internal29SmallStringOptimizedAllocatorILm128EE8allocateEj:
.LFB26143:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	movq	8(%rdi), %r13
	movq	0(%r13), %rax
	movq	16(%r13), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rbx
	ja	.L3143
.L3141:
	addq	%rbx, %rax
	movq	%rax, 8(%r13)
	movq	8(%r12), %rax
	movq	(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3143:
	.cfi_restore_state
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v84base11SmallVectorIcLm128EE4GrowEm
	movq	0(%r13), %rax
	jmp	.L3141
	.cfi_endproc
.LFE26143:
	.size	_ZN2v88internal29SmallStringOptimizedAllocatorILm128EE8allocateEj, .-_ZN2v88internal29SmallStringOptimizedAllocatorILm128EE8allocateEj
	.section	.rodata._ZN2v88internal12_GLOBAL__N_112ToDateStringEdPNS0_9DateCacheENS1_16ToDateStringModeE.str1.1,"aMS",@progbits,1
.LC76:
	.string	"%s %s %02d %05d"
.LC77:
	.string	"%s %s %02d %04d"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_112ToDateStringEdPNS0_9DateCacheENS1_16ToDateStringModeE.str1.8,"aMS",@progbits,1
	.align 8
.LC78:
	.string	"%s %s %02d %05d %02d:%02d:%02d GMT%c%02d%02d (%s)"
	.align 8
.LC79:
	.string	"%s %s %02d %04d %02d:%02d:%02d GMT%c%02d%02d (%s)"
	.align 8
.LC80:
	.string	"%02d:%02d:%02d GMT%c%02d%02d (%s)"
	.section	.text._ZN2v88internal12_GLOBAL__N_112ToDateStringEdPNS0_9DateCacheENS1_16ToDateStringModeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_112ToDateStringEdPNS0_9DateCacheENS1_16ToDateStringModeE, @function
_ZN2v88internal12_GLOBAL__N_112ToDateStringEdPNS0_9DateCacheENS1_16ToDateStringModeE:
.LFB21341:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	152(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -272(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	24(%rdi), %rax
	movq	%r13, %rdi
	subq	%rax, %rdi
	ucomisd	%xmm0, %xmm0
	movq	%rax, -248(%rbp)
	movq	%rdi, -264(%rbp)
	jp	.L3189
	cvttsd2siq	%xmm0, %r12
	movq	(%rsi), %rax
	movq	%rsi, %rbx
	movl	$1, %edx
	movq	%rbx, %rdi
	leaq	-176(%rbp), %r14
	movq	%r12, %rsi
	call	*24(%rax)
	leaq	-224(%rbp), %rcx
	movq	%rbx, %rdi
	leaq	-228(%rbp), %rdx
	movslq	%eax, %rsi
	leaq	-200(%rbp), %rax
	leaq	-216(%rbp), %r9
	pushq	%rax
	leaq	-204(%rbp), %rax
	addq	%r12, %rsi
	leaq	-220(%rbp), %r8
	pushq	%rax
	leaq	-208(%rbp), %rax
	pushq	%rax
	leaq	-212(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal9DateCache13BreakDownTimeElPiS2_S2_S2_S2_S2_S2_S2_@PLT
	movq	(%rbx), %rax
	movl	$1, %edx
	movq	%r12, %rsi
	addq	$32, %rsp
	movq	%rbx, %rdi
	call	*24(%rax)
	movabsq	$5037190915060954895, %rdx
	movslq	%eax, %rcx
	movl	%eax, -284(%rbp)
	negq	%rcx
	movq	%rcx, %rax
	sarq	$63, %rcx
	imulq	%rdx
	sarq	$14, %rdx
	subq	%rcx, %rdx
	movl	$2290649225, %ecx
	movl	%edx, %eax
	sarl	$31, %eax
	xorl	%eax, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	imulq	%rcx, %rax
	shrq	$37, %rax
	movl	%eax, -276(%rbp)
	imull	$60, %eax, %eax
	subl	%eax, %edx
	leaq	-192(%rbp), %rax
	movq	%rax, -256(%rbp)
	movabsq	$2147483647000, %rax
	movl	%edx, -280(%rbp)
	cmpq	%rax, %r12
	ja	.L3190
.L3149:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9DateCache25DaylightSavingsOffsetInMsEl@PLT
	testl	%eax, %eax
	jne	.L3191
	movq	576(%rbx), %rdx
	leaq	576(%rbx), %rcx
	testq	%rdx, %rdx
	je	.L3192
.L3155:
	movl	-272(%rbp), %eax
	cmpl	$1, %eax
	je	.L3156
.L3196:
	cmpl	$2, %eax
	je	.L3157
	movslq	-224(%rbp), %rsi
	leaq	_ZN2v88internal12_GLOBAL__N_112kShortMonthsE(%rip), %rax
	movq	%r13, 16(%r15)
	leaq	.LC76(%rip), %r12
	movl	-228(%rbp), %ebx
	movl	-220(%rbp), %ecx
	movq	%r15, -184(%rbp)
	movq	-248(%rbp), %xmm1
	movq	(%rax,%rsi,8), %rax
	movl	$0, -160(%rbp)
	movslq	-216(%rbp), %rsi
	testl	%ebx, %ebx
	movq	%rax, -272(%rbp)
	leaq	_ZN2v88internal12_GLOBAL__N_114kShortWeekDaysE(%rip), %rax
	punpcklqdq	%xmm1, %xmm1
	movq	(%rax,%rsi,8), %xmm0
	leaq	.LC77(%rip), %rax
	movups	%xmm1, (%r15)
	cmovns	%rax, %r12
	leaq	16+_ZTVN2v88internal29SmallStringOptimizedAllocatorILm128EEE(%rip), %rax
	cmpq	$15, -264(%rbp)
	movq	%rax, -192(%rbp)
	movq	-256(%rbp), %rax
	movq	%rax, -176(%rbp)
	movabsq	$68719476737, %rax
	movq	%rax, -168(%rbp)
	movq	%r15, %rax
	jbe	.L3193
.L3159:
	movq	-248(%rbp), %rsi
	movl	$4, %r8d
	movl	$15, %edx
	movhps	-272(%rbp), %xmm0
	addq	$16, %rsi
	movq	%rsi, 8(%r15)
	movq	(%rax), %rax
	movq	%r12, %rsi
	movq	%rax, -152(%rbp)
	movb	$0, (%rax)
	movl	%ebx, -120(%rbp)
	movl	%ecx, -128(%rbp)
	leaq	-144(%rbp), %rcx
	movaps	%xmm0, -144(%rbp)
.L3185:
	movq	%r14, %rdi
.L3188:
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movq	(%r15), %rax
	movq	16(%r15), %rdx
	movslq	-160(%rbp), %rbx
	subq	%rax, %rdx
	cmpq	%rdx, %rbx
	ja	.L3194
.L3167:
	addq	%rbx, %rax
	movq	%rax, 8(%r15)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3195
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3191:
	.cfi_restore_state
	movq	584(%rbx), %rdx
	leaq	584(%rbx), %rcx
	testq	%rdx, %rdx
	jne	.L3155
.L3192:
	movq	592(%rbx), %rdi
	pxor	%xmm0, %xmm0
	movq	%rcx, -296(%rbp)
	cvtsi2sdq	%r12, %xmm0
	movq	(%rdi), %rax
	call	*(%rax)
	movq	-296(%rbp), %rcx
	movq	%rax, %rdx
	movq	%rax, (%rcx)
	movl	-272(%rbp), %eax
	cmpl	$1, %eax
	jne	.L3196
.L3156:
	leaq	16+_ZTVN2v88internal29SmallStringOptimizedAllocatorILm128EEE(%rip), %rax
	xorl	%ebx, %ebx
	movq	%r13, 16(%r15)
	movl	-208(%rbp), %ecx
	movq	%rax, -192(%rbp)
	movq	-256(%rbp), %rax
	movq	-248(%rbp), %xmm0
	cmpl	$-59999, -284(%rbp)
	movq	%r15, -184(%rbp)
	setl	%bl
	movq	%rax, -176(%rbp)
	cmpq	$15, -264(%rbp)
	movabsq	$68719476737, %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -168(%rbp)
	leal	43(%rbx,%rbx), %ebx
	movl	-204(%rbp), %r12d
	movl	-212(%rbp), %r8d
	movups	%xmm0, (%r15)
	movq	%r15, %rax
	movl	$0, -160(%rbp)
	jbe	.L3197
.L3162:
	movq	-248(%rbp), %rsi
	addq	$16, %rsi
	movq	%rsi, 8(%r15)
	movq	(%rax), %rax
	leaq	.LC80(%rip), %rsi
	movq	%rax, -152(%rbp)
	movb	$0, (%rax)
	movl	-276(%rbp), %eax
	movl	%r8d, -144(%rbp)
	movl	$7, %r8d
	movl	%eax, -112(%rbp)
	movl	-280(%rbp), %eax
	movl	%ecx, -136(%rbp)
	leaq	-144(%rbp), %rcx
	movq	%rdx, -96(%rbp)
	movl	$33, %edx
	movl	%r12d, -128(%rbp)
	movl	%ebx, -120(%rbp)
	movl	%eax, -104(%rbp)
	jmp	.L3185
	.p2align 4,,10
	.p2align 3
.L3194:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v84base11SmallVectorIcLm128EE4GrowEm
	movq	(%r15), %rax
	jmp	.L3167
	.p2align 4,,10
	.p2align 3
.L3157:
	movslq	-224(%rbp), %rsi
	leaq	_ZN2v88internal12_GLOBAL__N_112kShortMonthsE(%rip), %rax
	xorl	%ebx, %ebx
	movq	%r13, 16(%r15)
	movl	-228(%rbp), %ecx
	leaq	.LC78(%rip), %r12
	cmpl	$-59999, -284(%rbp)
	movq	%r15, -184(%rbp)
	movq	(%rax,%rsi,8), %rax
	setl	%bl
	movslq	-216(%rbp), %rsi
	movl	$0, -160(%rbp)
	testl	%ecx, %ecx
	movl	-204(%rbp), %r11d
	movq	-248(%rbp), %xmm1
	leal	43(%rbx,%rbx), %ebx
	movq	%rax, -272(%rbp)
	leaq	_ZN2v88internal12_GLOBAL__N_114kShortWeekDaysE(%rip), %rax
	movl	-208(%rbp), %r10d
	movq	(%rax,%rsi,8), %xmm0
	leaq	.LC79(%rip), %rax
	punpcklqdq	%xmm1, %xmm1
	movl	-212(%rbp), %r9d
	cmovns	%rax, %r12
	leaq	16+_ZTVN2v88internal29SmallStringOptimizedAllocatorILm128EEE(%rip), %rax
	cmpq	$15, -264(%rbp)
	movups	%xmm1, (%r15)
	movq	%rax, -192(%rbp)
	movq	-256(%rbp), %rax
	movl	-220(%rbp), %r8d
	movq	%rax, -176(%rbp)
	movabsq	$68719476737, %rax
	movq	%rax, -168(%rbp)
	movq	%r15, %rax
	jbe	.L3198
.L3166:
	movq	-248(%rbp), %rsi
	movhps	-272(%rbp), %xmm0
	addq	$16, %rsi
	movq	%rsi, 8(%r15)
	movq	(%rax), %rax
	movq	%r12, %rsi
	movq	%rax, -152(%rbp)
	movb	$0, (%rax)
	movl	-276(%rbp), %eax
	movl	%r8d, -128(%rbp)
	movl	$11, %r8d
	movl	%eax, -80(%rbp)
	movl	-280(%rbp), %eax
	movl	%ecx, -120(%rbp)
	leaq	-144(%rbp), %rcx
	movq	%rdx, -64(%rbp)
	movl	$49, %edx
	movl	%r9d, -112(%rbp)
	movl	%r10d, -104(%rbp)
	movl	%r11d, -96(%rbp)
	movl	%ebx, -88(%rbp)
	movl	%eax, -72(%rbp)
	movaps	%xmm0, -144(%rbp)
	jmp	.L3185
	.p2align 4,,10
	.p2align 3
.L3190:
	testq	%r12, %r12
	leaq	-86399999(%r12), %rcx
	movq	%r14, %r8
	movq	%rbx, %rdi
	movabsq	$7164004856975580295, %rdx
	cmovns	%r12, %rcx
	movq	%rcx, %rax
	sarq	$63, %rcx
	imulq	%rdx
	movq	%rdx, %rsi
	leaq	-196(%rbp), %rdx
	sarq	$25, %rsi
	subq	%rcx, %rsi
	movq	-256(%rbp), %rcx
	imull	$-86400000, %esi, %eax
	addl	%r12d, %eax
	movl	%eax, -296(%rbp)
	call	_ZN2v88internal9DateCache20YearMonthDayFromDaysEiPiS2_S2_@PLT
	movl	-196(%rbp), %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	-192(%rbp), %r12d
	movl	%ecx, %esi
	movl	%ecx, -288(%rbp)
	call	_ZN2v88internal9DateCache17DaysFromYearMonthEii@PLT
	movl	-288(%rbp), %ecx
	addl	$4, %eax
	movslq	%eax, %rdx
	movl	%eax, %esi
	imulq	$-1840700269, %rdx, %rdx
	sarl	$31, %esi
	shrq	$32, %rdx
	addl	%eax, %edx
	sarl	$2, %edx
	subl	%esi, %edx
	leal	0(,%rdx,8), %esi
	subl	%edx, %esi
	subl	%esi, %eax
	movl	$1967, %esi
	movl	%eax, %edx
	leal	7(%rax), %eax
	cmovs	%eax, %edx
	testb	$3, %cl
	jne	.L3152
	imull	$-1030792151, %ecx, %ecx
	movl	$1956, %esi
	addl	$85899344, %ecx
	movl	%ecx, %eax
	rorl	$2, %eax
	cmpl	$42949672, %eax
	ja	.L3152
	rorl	$4, %ecx
	cmpl	$10737419, %ecx
	sbbl	%esi, %esi
	andl	$-11, %esi
	addl	$1967, %esi
.L3152:
	leal	(%rdx,%rdx,2), %edx
	movq	%rbx, %rdi
	movl	%edx, %eax
	leal	(%rsi,%rdx,4), %esi
	movl	%r12d, %edx
	andl	$1073741823, %eax
	imulq	$613566757, %rax, %rax
	shrq	$32, %rax
	imull	$28, %eax, %eax
	subl	%eax, %esi
	subl	$1924, %esi
	movl	%esi, %eax
	shrl	$2, %eax
	imulq	$613566757, %rax, %rax
	shrq	$32, %rax
	imull	$28, %eax, %eax
	subl	%eax, %esi
	addl	$2008, %esi
	call	_ZN2v88internal9DateCache17DaysFromYearMonthEii@PLT
	addl	-176(%rbp), %eax
	movl	%eax, %r10d
	subl	$1, %r10d
	movslq	%r10d, %r10
	imulq	$86400000, %r10, %rax
	movslq	-296(%rbp), %r10
	leaq	(%rax,%r10), %r12
	jmp	.L3149
	.p2align 4,,10
	.p2align 3
.L3197:
	movl	$16, %esi
	movq	%r15, %rdi
	movq	%rdx, -272(%rbp)
	movl	%r8d, -264(%rbp)
	movl	%ecx, -256(%rbp)
	call	_ZN2v84base11SmallVectorIcLm128EE4GrowEm
	movq	(%r15), %rax
	movq	-272(%rbp), %rdx
	movl	-264(%rbp), %r8d
	movl	-256(%rbp), %ecx
	movq	%rax, -248(%rbp)
	movq	-184(%rbp), %rax
	jmp	.L3162
	.p2align 4,,10
	.p2align 3
.L3198:
	movl	$16, %esi
	movq	%r15, %rdi
	movq	%rdx, -312(%rbp)
	movl	%r8d, -288(%rbp)
	movl	%ecx, -296(%rbp)
	movl	%r9d, -284(%rbp)
	movl	%r10d, -264(%rbp)
	movl	%r11d, -256(%rbp)
	movq	%xmm0, -304(%rbp)
	call	_ZN2v84base11SmallVectorIcLm128EE4GrowEm
	movq	(%r15), %rax
	movq	-312(%rbp), %rdx
	movq	-304(%rbp), %xmm0
	movl	-288(%rbp), %r8d
	movq	%rax, -248(%rbp)
	movl	-296(%rbp), %ecx
	movq	-184(%rbp), %rax
	movl	-284(%rbp), %r9d
	movl	-264(%rbp), %r10d
	movl	-256(%rbp), %r11d
	jmp	.L3166
	.p2align 4,,10
	.p2align 3
.L3193:
	movl	$16, %esi
	movq	%r15, %rdi
	movl	%ecx, -256(%rbp)
	movq	%xmm0, -264(%rbp)
	call	_ZN2v84base11SmallVectorIcLm128EE4GrowEm
	movq	(%r15), %rax
	movl	-256(%rbp), %ecx
	movq	-264(%rbp), %xmm0
	movq	%rax, -248(%rbp)
	movq	-184(%rbp), %rax
	jmp	.L3159
	.p2align 4,,10
	.p2align 3
.L3189:
	movq	%rax, %xmm0
	movq	%r13, 16(%r15)
	leaq	16+_ZTVN2v88internal29SmallStringOptimizedAllocatorILm128EEE(%rip), %rax
	movq	%rax, -192(%rbp)
	punpcklqdq	%xmm0, %xmm0
	leaq	-192(%rbp), %rax
	movq	%rax, -176(%rbp)
	movabsq	$68719476737, %rax
	movq	%rax, -168(%rbp)
	movq	%r15, %rax
	movq	%r15, -184(%rbp)
	movl	$0, -160(%rbp)
	movups	%xmm0, (%r15)
	cmpq	$15, %rdi
	jbe	.L3199
.L3146:
	movq	-248(%rbp), %rdx
	leaq	-176(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC19(%rip), %rsi
	addq	$16, %rdx
	movq	%rdx, 8(%r15)
	movq	(%rax), %rax
	movl	$12, %edx
	movq	%rax, -152(%rbp)
	movb	$0, (%rax)
	jmp	.L3188
	.p2align 4,,10
	.p2align 3
.L3199:
	movl	$16, %esi
	movq	%r15, %rdi
	call	_ZN2v84base11SmallVectorIcLm128EE4GrowEm
	movq	(%r15), %rax
	movq	%rax, -248(%rbp)
	movq	-184(%rbp), %rax
	jmp	.L3146
.L3195:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21341:
	.size	_ZN2v88internal12_GLOBAL__N_112ToDateStringEdPNS0_9DateCacheENS1_16ToDateStringModeE, .-_ZN2v88internal12_GLOBAL__N_112ToDateStringEdPNS0_9DateCacheENS1_16ToDateStringModeE
	.section	.text._ZN2v88internalL28Builtin_Impl_DateConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Builtin_Impl_DateConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL28Builtin_Impl_DateConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21345:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	-8(,%rdi,8), %eax
	addl	$1, 41104(%rdx)
	movq	88(%r12), %rcx
	movslq	%eax, %rdx
	subq	%rdx, %r8
	cmpq	%rcx, (%r8)
	je	.L3335
	subl	$8, %eax
	movq	%rsi, %r9
	movl	%edi, %r14d
	movq	%rsi, %r15
	cltq
	subq	%rax, %r9
	subl	$5, %r14d
	je	.L3336
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %r10
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	cmpl	$1, %r14d
	jne	.L3208
	testb	%dl, %dl
	je	.L3337
.L3222:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	jmp	.L3207
	.p2align 4,,10
	.p2align 3
.L3208:
	testb	%dl, %dl
	je	.L3338
.L3230:
	movq	-16(%r15), %rdx
	leaq	-16(%r15), %rsi
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	je	.L3339
.L3234:
	movq	(%r10), %rcx
	testb	$1, %cl
	jne	.L3236
	sarq	$32, %rcx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
.L3237:
	testb	%al, %al
	je	.L3238
	sarq	$32, %rdx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
.L3239:
	cmpl	$2, %r14d
	jg	.L3340
	pxor	%xmm7, %xmm7
	movsd	.LC5(%rip), %xmm2
	movapd	%xmm7, %xmm8
	movapd	%xmm7, %xmm6
	movapd	%xmm7, %xmm5
.L3240:
	ucomisd	%xmm0, %xmm0
	jp	.L3274
	movq	.LC2(%rip), %xmm4
	movapd	%xmm0, %xmm3
	movsd	.LC3(%rip), %xmm9
	andpd	%xmm4, %xmm3
	ucomisd	%xmm3, %xmm9
	jb	.L3300
	pxor	%xmm9, %xmm9
	movapd	%xmm0, %xmm3
	ucomisd	%xmm9, %xmm0
	jnp	.L3341
.L3310:
	comisd	%xmm9, %xmm0
	movapd	%xmm0, %xmm10
	movsd	.LC4(%rip), %xmm11
	andpd	%xmm4, %xmm10
	jb	.L3323
	ucomisd	%xmm10, %xmm11
	jbe	.L3275
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm10, %xmm10
	andnpd	%xmm0, %xmm4
	movsd	.LC5(%rip), %xmm11
	cvtsi2sdq	%rax, %xmm10
	movapd	%xmm10, %xmm3
	cmpnlesd	%xmm0, %xmm3
	andpd	%xmm11, %xmm3
	subsd	%xmm3, %xmm10
	movapd	%xmm10, %xmm3
	orpd	%xmm4, %xmm3
	.p2align 4,,10
	.p2align 3
.L3275:
	comisd	%xmm9, %xmm3
	jb	.L3274
.L3276:
	movsd	.LC33(%rip), %xmm4
	comisd	%xmm3, %xmm4
	jb	.L3274
	addsd	.LC34(%rip), %xmm3
	movapd	%xmm3, %xmm0
.L3274:
	comisd	.LC35(%rip), %xmm0
	movsd	%xmm6, -240(%rbp)
	movsd	%xmm8, -248(%rbp)
	movsd	%xmm5, -232(%rbp)
	jb	.L3306
	movsd	.LC36(%rip), %xmm3
	comisd	%xmm0, %xmm3
	jb	.L3306
	comisd	.LC37(%rip), %xmm1
	jb	.L3306
	movsd	.LC38(%rip), %xmm3
	comisd	%xmm1, %xmm3
	jb	.L3306
	movq	%r9, -264(%rbp)
	movq	%r8, -256(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_17MakeDayEddd.part.0
	movsd	-240(%rbp), %xmm6
	movsd	-248(%rbp), %xmm8
	movsd	-232(%rbp), %xmm5
	movapd	%xmm0, %xmm13
	movapd	%xmm7, %xmm3
	movapd	%xmm8, %xmm2
	movapd	%xmm6, %xmm1
	movapd	%xmm5, %xmm0
	call	_ZN2v88internal12_GLOBAL__N_18MakeTimeEdddd
	movq	.LC2(%rip), %xmm1
	movsd	.LC3(%rip), %xmm2
	movapd	%xmm13, %xmm3
	movq	-256(%rbp), %r8
	movq	-264(%rbp), %r9
	andpd	%xmm1, %xmm3
	ucomisd	%xmm3, %xmm2
	jb	.L3306
	andpd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm2
	movsd	.LC1(%rip), %xmm1
	jb	.L3284
	movsd	.LC26(%rip), %xmm1
	mulsd	%xmm13, %xmm1
	addsd	%xmm0, %xmm1
	jmp	.L3284
	.p2align 4,,10
	.p2align 3
.L3336:
	movq	%r12, %rdi
	movq	%r9, -240(%rbp)
	movq	%r8, -232(%rbp)
	call	_ZN2v88internal6JSDate16CurrentTimeValueEPNS0_7IsolateE@PLT
	movq	-232(%rbp), %r8
	movq	-240(%rbp), %r9
.L3207:
	movq	%r8, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal6JSDate3NewENS0_6HandleINS0_10JSFunctionEEENS2_INS0_10JSReceiverEEEd@PLT
	testq	%rax, %rax
	je	.L3334
	movq	(%rax), %r14
.L3205:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3293
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3293:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3342
	addq	$248, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3349:
	.cfi_restore_state
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r9, -288(%rbp)
	movq	%r8, -272(%rbp)
	movsd	%xmm1, -280(%rbp)
	movsd	%xmm8, -264(%rbp)
	movsd	%xmm6, -256(%rbp)
	movsd	%xmm5, -248(%rbp)
	movsd	%xmm2, -240(%rbp)
	movsd	%xmm0, -232(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-232(%rbp), %xmm0
	movsd	-240(%rbp), %xmm2
	testq	%rax, %rax
	movq	-272(%rbp), %r8
	movq	%rax, %rsi
	movsd	-248(%rbp), %xmm5
	movsd	-256(%rbp), %xmm6
	movsd	-280(%rbp), %xmm1
	movsd	-264(%rbp), %xmm8
	movq	-288(%rbp), %r9
	jne	.L3272
	.p2align 4,,10
	.p2align 3
.L3334:
	movq	312(%r12), %r14
	jmp	.L3205
	.p2align 4,,10
	.p2align 3
.L3335:
	movq	%r12, %rdi
	call	_ZN2v88internal6JSDate16CurrentTimeValueEPNS0_7IsolateE@PLT
	movq	41240(%r12), %rsi
	movl	$2, %edx
	leaq	-208(%rbp), %rdi
	call	_ZN2v88internal12_GLOBAL__N_112ToDateStringEdPNS0_9DateCacheENS1_16ToDateStringModeE
	movq	-208(%rbp), %rdx
	movq	-200(%rbp), %rax
	movq	%r12, %rdi
	leaq	-224(%rbp), %rsi
	subq	%rdx, %rax
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	cltq
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L3343
	movq	(%rax), %r14
.L3203:
	movq	-208(%rbp), %rdi
	leaq	-184(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3205
	call	free@PLT
	jmp	.L3205
	.p2align 4,,10
	.p2align 3
.L3338:
	movq	-1(%rax), %rax
	cmpw	$65, 11(%rax)
	je	.L3230
	xorl	%edx, %edx
	movq	%r10, %rsi
	movq	%r12, %rdi
	movq	%r9, -240(%rbp)
	movq	%r8, -232(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movq	-232(%rbp), %r8
	movq	-240(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L3334
	jmp	.L3230
	.p2align 4,,10
	.p2align 3
.L3339:
	movq	-1(%rdx), %rcx
	cmpw	$65, 11(%rcx)
	je	.L3234
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r9, -248(%rbp)
	movq	%r10, -240(%rbp)
	movq	%r8, -232(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movq	-232(%rbp), %r8
	movq	-240(%rbp), %r10
	testq	%rax, %rax
	movq	-248(%rbp), %r9
	je	.L3334
	movq	(%rax), %rdx
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	jmp	.L3234
	.p2align 4,,10
	.p2align 3
.L3337:
	movq	-1(%rax), %rdx
	cmpw	$1066, 11(%rdx)
	je	.L3210
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	ja	.L3344
	xorl	%edx, %edx
.L3217:
	testb	%dl, %dl
	jne	.L3222
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L3219
	movq	(%r10), %rax
	testb	$1, %al
	je	.L3222
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L3223
	xorl	%edx, %edx
.L3224:
	testb	%dl, %dl
	jne	.L3222
.L3226:
	movsd	7(%rax), %xmm0
	jmp	.L3207
	.p2align 4,,10
	.p2align 3
.L3306:
	movsd	.LC1(%rip), %xmm1
.L3284:
	comisd	.LC29(%rip), %xmm1
	jb	.L3329
	movsd	.LC30(%rip), %xmm0
	comisd	%xmm1, %xmm0
	jb	.L3329
	cvttsd2siq	%xmm1, %r14
	movq	%r9, -240(%rbp)
	xorl	%edx, %edx
	movq	41240(%r12), %rdi
	movq	%r8, -232(%rbp)
	movq	(%rdi), %rax
	movq	%r14, %rsi
	call	*24(%rax)
	pxor	%xmm0, %xmm0
	movq	-232(%rbp), %r8
	movq	-240(%rbp), %r9
	cltq
	subq	%rax, %r14
	cvtsi2sdq	%r14, %xmm0
	jmp	.L3207
	.p2align 4,,10
	.p2align 3
.L3329:
	movsd	.LC1(%rip), %xmm0
	jmp	.L3207
	.p2align 4,,10
	.p2align 3
.L3238:
	movsd	7(%rdx), %xmm1
	jmp	.L3239
	.p2align 4,,10
	.p2align 3
.L3236:
	movsd	7(%rcx), %xmm0
	jmp	.L3237
	.p2align 4,,10
	.p2align 3
.L3340:
	movq	-24(%r15), %rax
	leaq	-24(%r15), %rsi
	testb	$1, %al
	jne	.L3345
.L3242:
	sarq	$32, %rax
	pxor	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
.L3247:
	cmpl	$3, %r14d
	je	.L3296
	movq	-32(%r15), %rax
	leaq	-32(%r15), %rsi
	testb	$1, %al
	jne	.L3346
.L3249:
	sarq	$32, %rax
	pxor	%xmm5, %xmm5
	cvtsi2sdl	%eax, %xmm5
.L3254:
	cmpl	$4, %r14d
	je	.L3297
	movq	-40(%r15), %rax
	leaq	-40(%r15), %rsi
	testb	$1, %al
	jne	.L3347
.L3256:
	sarq	$32, %rax
	pxor	%xmm6, %xmm6
	cvtsi2sdl	%eax, %xmm6
.L3261:
	cmpl	$5, %r14d
	je	.L3298
	movq	-48(%r15), %rax
	leaq	-48(%r15), %rsi
	testb	$1, %al
	jne	.L3348
.L3263:
	sarq	$32, %rax
	pxor	%xmm8, %xmm8
	cvtsi2sdl	%eax, %xmm8
.L3268:
	pxor	%xmm7, %xmm7
	cmpl	$6, %r14d
	je	.L3240
	movq	-56(%r15), %rax
	leaq	-56(%r15), %rsi
	testb	$1, %al
	je	.L3272
	movq	-1(%rax), %rax
	cmpw	$65, 11(%rax)
	jne	.L3349
.L3272:
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L3273
	sarq	$32, %rax
	pxor	%xmm7, %xmm7
	cvtsi2sdl	%eax, %xmm7
	jmp	.L3240
	.p2align 4,,10
	.p2align 3
.L3343:
	movq	312(%r12), %r14
	jmp	.L3203
	.p2align 4,,10
	.p2align 3
.L3219:
	movq	%r10, %rsi
	movq	%r12, %rdi
	movq	%r9, -240(%rbp)
	movq	%r8, -232(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_119ParseDateTimeStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	movq	-232(%rbp), %r8
	movq	-240(%rbp), %r9
	jmp	.L3207
	.p2align 4,,10
	.p2align 3
.L3345:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L3243
	xorl	%edx, %edx
.L3244:
	testb	%dl, %dl
	jne	.L3242
	movsd	7(%rax), %xmm2
	jmp	.L3247
	.p2align 4,,10
	.p2align 3
.L3323:
	ucomisd	%xmm10, %xmm11
	jbe	.L3275
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm10, %xmm10
	andnpd	%xmm0, %xmm4
	movsd	.LC5(%rip), %xmm11
	cvtsi2sdq	%rax, %xmm10
	cmpnlesd	%xmm10, %xmm3
	andpd	%xmm11, %xmm3
	addsd	%xmm10, %xmm3
	orpd	%xmm4, %xmm3
	jmp	.L3275
	.p2align 4,,10
	.p2align 3
.L3300:
	movapd	%xmm0, %xmm3
	pxor	%xmm9, %xmm9
	jmp	.L3275
	.p2align 4,,10
	.p2align 3
.L3344:
	xorl	%esi, %esi
	movq	%r10, %rdi
	movq	%r9, -240(%rbp)
	movq	%r8, -232(%rbp)
	call	_ZN2v88internal10JSReceiver11ToPrimitiveENS0_6HandleIS1_EENS0_15ToPrimitiveHintE@PLT
	movq	-232(%rbp), %r8
	movq	-240(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L3334
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L3217
	.p2align 4,,10
	.p2align 3
.L3341:
	je	.L3276
	jmp	.L3310
	.p2align 4,,10
	.p2align 3
.L3210:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L3226
	jmp	.L3222
	.p2align 4,,10
	.p2align 3
.L3223:
	xorl	%edx, %edx
	movq	%r10, %rsi
	movq	%r12, %rdi
	movq	%r9, -240(%rbp)
	movq	%r8, -232(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movq	-232(%rbp), %r8
	movq	-240(%rbp), %r9
	testq	%rax, %rax
	je	.L3334
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L3224
	.p2align 4,,10
	.p2align 3
.L3296:
	pxor	%xmm7, %xmm7
	movapd	%xmm7, %xmm8
	movapd	%xmm7, %xmm6
	movapd	%xmm7, %xmm5
	jmp	.L3240
	.p2align 4,,10
	.p2align 3
.L3346:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L3250
	xorl	%edx, %edx
.L3251:
	testb	%dl, %dl
	jne	.L3249
	movsd	7(%rax), %xmm5
	jmp	.L3254
.L3243:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r9, -256(%rbp)
	movq	%r8, -240(%rbp)
	movsd	%xmm1, -248(%rbp)
	movsd	%xmm0, -232(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-232(%rbp), %xmm0
	movq	-240(%rbp), %r8
	testq	%rax, %rax
	movsd	-248(%rbp), %xmm1
	movq	-256(%rbp), %r9
	je	.L3334
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L3244
	.p2align 4,,10
	.p2align 3
.L3297:
	pxor	%xmm7, %xmm7
	movapd	%xmm7, %xmm8
	movapd	%xmm7, %xmm6
	jmp	.L3240
.L3347:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L3257
	xorl	%edx, %edx
.L3258:
	testb	%dl, %dl
	jne	.L3256
	movsd	7(%rax), %xmm6
	jmp	.L3261
.L3250:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r9, -264(%rbp)
	movq	%r8, -248(%rbp)
	movsd	%xmm1, -256(%rbp)
	movsd	%xmm2, -240(%rbp)
	movsd	%xmm0, -232(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-232(%rbp), %xmm0
	movsd	-240(%rbp), %xmm2
	testq	%rax, %rax
	movq	-248(%rbp), %r8
	movsd	-256(%rbp), %xmm1
	movq	-264(%rbp), %r9
	je	.L3334
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L3251
	.p2align 4,,10
	.p2align 3
.L3298:
	pxor	%xmm7, %xmm7
	movapd	%xmm7, %xmm8
	jmp	.L3240
.L3348:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L3264
	xorl	%edx, %edx
.L3265:
	testb	%dl, %dl
	jne	.L3263
	movsd	7(%rax), %xmm8
	jmp	.L3268
.L3257:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r9, -272(%rbp)
	movq	%r8, -256(%rbp)
	movsd	%xmm1, -264(%rbp)
	movsd	%xmm5, -248(%rbp)
	movsd	%xmm2, -240(%rbp)
	movsd	%xmm0, -232(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-232(%rbp), %xmm0
	movsd	-240(%rbp), %xmm2
	testq	%rax, %rax
	movsd	-248(%rbp), %xmm5
	movq	-256(%rbp), %r8
	movsd	-264(%rbp), %xmm1
	movq	-272(%rbp), %r9
	je	.L3334
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L3258
.L3264:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r9, -280(%rbp)
	movq	%r8, -264(%rbp)
	movsd	%xmm1, -272(%rbp)
	movsd	%xmm6, -256(%rbp)
	movsd	%xmm5, -248(%rbp)
	movsd	%xmm2, -240(%rbp)
	movsd	%xmm0, -232(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-232(%rbp), %xmm0
	movsd	-240(%rbp), %xmm2
	testq	%rax, %rax
	movsd	-248(%rbp), %xmm5
	movsd	-256(%rbp), %xmm6
	movq	-264(%rbp), %r8
	movsd	-272(%rbp), %xmm1
	movq	-280(%rbp), %r9
	je	.L3334
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L3265
.L3273:
	movsd	7(%rax), %xmm7
	jmp	.L3240
.L3342:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21345:
	.size	_ZN2v88internalL28Builtin_Impl_DateConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL28Builtin_Impl_DateConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL34Builtin_Impl_Stats_DateConstructorEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC81:
	.string	"V8.Builtin_DateConstructor"
	.section	.text._ZN2v88internalL34Builtin_Impl_Stats_DateConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Builtin_Impl_Stats_DateConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL34Builtin_Impl_Stats_DateConstructorEiPmPNS0_7IsolateE:
.LFB21343:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3379
.L3351:
	movq	_ZZN2v88internalL34Builtin_Impl_Stats_DateConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic206(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3380
.L3353:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3381
.L3355:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL28Builtin_Impl_DateConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3382
.L3359:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3383
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3380:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3384
.L3354:
	movq	%rbx, _ZZN2v88internalL34Builtin_Impl_Stats_DateConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic206(%rip)
	jmp	.L3353
	.p2align 4,,10
	.p2align 3
.L3381:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3385
.L3356:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3357
	movq	(%rdi), %rax
	call	*8(%rax)
.L3357:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3358
	movq	(%rdi), %rax
	call	*8(%rax)
.L3358:
	leaq	.LC81(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L3355
	.p2align 4,,10
	.p2align 3
.L3382:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3359
	.p2align 4,,10
	.p2align 3
.L3379:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$736, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3351
	.p2align 4,,10
	.p2align 3
.L3385:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC81(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L3356
	.p2align 4,,10
	.p2align 3
.L3384:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3354
.L3383:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21343:
	.size	_ZN2v88internalL34Builtin_Impl_Stats_DateConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internalL34Builtin_Impl_Stats_DateConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Builtin_DateConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Builtin_DateConstructorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Builtin_DateConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Builtin_DateConstructorEiPmPNS0_7IsolateE:
.LFB21344:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3390
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL28Builtin_Impl_DateConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3390:
	.cfi_restore 6
	jmp	_ZN2v88internalL34Builtin_Impl_Stats_DateConstructorEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21344:
	.size	_ZN2v88internal23Builtin_DateConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internal23Builtin_DateConstructorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL38Builtin_Impl_DatePrototypeToDateStringENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC82:
	.string	"Date.prototype.toDateString"
	.section	.text._ZN2v88internalL38Builtin_Impl_DatePrototypeToDateStringENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Builtin_Impl_DatePrototypeToDateStringENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL38Builtin_Impl_DatePrototypeToDateStringENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21403:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$176, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L3392
.L3395:
	leaq	.LC82(%rip), %rax
	xorl	%edx, %edx
	leaq	-208(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -208(%rbp)
	movq	$27, -200(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3410
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L3398:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3404
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3404:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3411
	addq	$176, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3392:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1066, 11(%rdx)
	jne	.L3395
	movq	23(%rax), %rax
	movq	41240(%r12), %rsi
	testb	$1, %al
	jne	.L3412
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L3399:
	xorl	%edx, %edx
	leaq	-192(%rbp), %rdi
	call	_ZN2v88internal12_GLOBAL__N_112ToDateStringEdPNS0_9DateCacheENS1_16ToDateStringModeE
	movq	-192(%rbp), %rdx
	movq	-184(%rbp), %rax
	movq	%r12, %rdi
	leaq	-208(%rbp), %rsi
	subq	%rdx, %rax
	movq	%rdx, -208(%rbp)
	xorl	%edx, %edx
	cltq
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L3413
	movq	(%rax), %r13
.L3401:
	movq	-192(%rbp), %rdi
	leaq	-168(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3398
	call	free@PLT
	jmp	.L3398
	.p2align 4,,10
	.p2align 3
.L3410:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3412:
	movsd	7(%rax), %xmm0
	jmp	.L3399
	.p2align 4,,10
	.p2align 3
.L3413:
	movq	312(%r12), %r13
	jmp	.L3401
.L3411:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21403:
	.size	_ZN2v88internalL38Builtin_Impl_DatePrototypeToDateStringENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL38Builtin_Impl_DatePrototypeToDateStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL44Builtin_Impl_Stats_DatePrototypeToDateStringEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC83:
	.string	"V8.Builtin_DatePrototypeToDateString"
	.section	.text._ZN2v88internalL44Builtin_Impl_Stats_DatePrototypeToDateStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL44Builtin_Impl_Stats_DatePrototypeToDateStringEiPmPNS0_7IsolateE, @function
_ZN2v88internalL44Builtin_Impl_Stats_DatePrototypeToDateStringEiPmPNS0_7IsolateE:
.LFB21401:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3443
.L3415:
	movq	_ZZN2v88internalL44Builtin_Impl_Stats_DatePrototypeToDateStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic794(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3444
.L3417:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3445
.L3419:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL38Builtin_Impl_DatePrototypeToDateStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3446
.L3423:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3447
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3444:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3448
.L3418:
	movq	%rbx, _ZZN2v88internalL44Builtin_Impl_Stats_DatePrototypeToDateStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic794(%rip)
	jmp	.L3417
	.p2align 4,,10
	.p2align 3
.L3445:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3449
.L3420:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3421
	movq	(%rdi), %rax
	call	*8(%rax)
.L3421:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3422
	movq	(%rdi), %rax
	call	*8(%rax)
.L3422:
	leaq	.LC83(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L3419
	.p2align 4,,10
	.p2align 3
.L3446:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3423
	.p2align 4,,10
	.p2align 3
.L3443:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$756, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3415
	.p2align 4,,10
	.p2align 3
.L3449:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC83(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L3420
	.p2align 4,,10
	.p2align 3
.L3448:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3418
.L3447:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21401:
	.size	_ZN2v88internalL44Builtin_Impl_Stats_DatePrototypeToDateStringEiPmPNS0_7IsolateE, .-_ZN2v88internalL44Builtin_Impl_Stats_DatePrototypeToDateStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal33Builtin_DatePrototypeToDateStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Builtin_DatePrototypeToDateStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal33Builtin_DatePrototypeToDateStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal33Builtin_DatePrototypeToDateStringEiPmPNS0_7IsolateE:
.LFB21402:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3454
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL38Builtin_Impl_DatePrototypeToDateStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3454:
	.cfi_restore 6
	jmp	_ZN2v88internalL44Builtin_Impl_Stats_DatePrototypeToDateStringEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21402:
	.size	_ZN2v88internal33Builtin_DatePrototypeToDateStringEiPmPNS0_7IsolateE, .-_ZN2v88internal33Builtin_DatePrototypeToDateStringEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL34Builtin_Impl_DatePrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC84:
	.string	"Date.prototype.toString"
	.section	.text._ZN2v88internalL34Builtin_Impl_DatePrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Builtin_Impl_DatePrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL34Builtin_Impl_DatePrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21413:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$176, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L3456
.L3459:
	leaq	.LC84(%rip), %rax
	xorl	%edx, %edx
	leaq	-208(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -208(%rbp)
	movq	$23, -200(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3474
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L3462:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3468
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3468:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3475
	addq	$176, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3456:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1066, 11(%rdx)
	jne	.L3459
	movq	23(%rax), %rax
	movq	41240(%r12), %rsi
	testb	$1, %al
	jne	.L3476
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L3463:
	leaq	-192(%rbp), %rdi
	movl	$2, %edx
	call	_ZN2v88internal12_GLOBAL__N_112ToDateStringEdPNS0_9DateCacheENS1_16ToDateStringModeE
	movq	-192(%rbp), %rdx
	movq	-184(%rbp), %rax
	movq	%r12, %rdi
	leaq	-208(%rbp), %rsi
	subq	%rdx, %rax
	movq	%rdx, -208(%rbp)
	xorl	%edx, %edx
	cltq
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L3477
	movq	(%rax), %r13
.L3465:
	movq	-192(%rbp), %rdi
	leaq	-168(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3462
	call	free@PLT
	jmp	.L3462
	.p2align 4,,10
	.p2align 3
.L3474:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3476:
	movsd	7(%rax), %xmm0
	jmp	.L3463
	.p2align 4,,10
	.p2align 3
.L3477:
	movq	312(%r12), %r13
	jmp	.L3465
.L3475:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21413:
	.size	_ZN2v88internalL34Builtin_Impl_DatePrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL34Builtin_Impl_DatePrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL40Builtin_Impl_Stats_DatePrototypeToStringEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC85:
	.string	"V8.Builtin_DatePrototypeToString"
	.section	.text._ZN2v88internalL40Builtin_Impl_Stats_DatePrototypeToStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Builtin_Impl_Stats_DatePrototypeToStringEiPmPNS0_7IsolateE, @function
_ZN2v88internalL40Builtin_Impl_Stats_DatePrototypeToStringEiPmPNS0_7IsolateE:
.LFB21411:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3507
.L3479:
	movq	_ZZN2v88internalL40Builtin_Impl_Stats_DatePrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic831(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3508
.L3481:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3509
.L3483:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL34Builtin_Impl_DatePrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3510
.L3487:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3511
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3508:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3512
.L3482:
	movq	%rbx, _ZZN2v88internalL40Builtin_Impl_Stats_DatePrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic831(%rip)
	jmp	.L3481
	.p2align 4,,10
	.p2align 3
.L3509:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3513
.L3484:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3485
	movq	(%rdi), %rax
	call	*8(%rax)
.L3485:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3486
	movq	(%rdi), %rax
	call	*8(%rax)
.L3486:
	leaq	.LC85(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L3483
	.p2align 4,,10
	.p2align 3
.L3510:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3487
	.p2align 4,,10
	.p2align 3
.L3507:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$759, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3479
	.p2align 4,,10
	.p2align 3
.L3513:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC85(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L3484
	.p2align 4,,10
	.p2align 3
.L3512:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3482
.L3511:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21411:
	.size	_ZN2v88internalL40Builtin_Impl_Stats_DatePrototypeToStringEiPmPNS0_7IsolateE, .-_ZN2v88internalL40Builtin_Impl_Stats_DatePrototypeToStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Builtin_DatePrototypeToStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Builtin_DatePrototypeToStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Builtin_DatePrototypeToStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Builtin_DatePrototypeToStringEiPmPNS0_7IsolateE:
.LFB21412:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3518
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL34Builtin_Impl_DatePrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3518:
	.cfi_restore 6
	jmp	_ZN2v88internalL40Builtin_Impl_Stats_DatePrototypeToStringEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21412:
	.size	_ZN2v88internal29Builtin_DatePrototypeToStringEiPmPNS0_7IsolateE, .-_ZN2v88internal29Builtin_DatePrototypeToStringEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL38Builtin_Impl_DatePrototypeToTimeStringENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC86:
	.string	"Date.prototype.toTimeString"
	.section	.text._ZN2v88internalL38Builtin_Impl_DatePrototypeToTimeStringENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Builtin_Impl_DatePrototypeToTimeStringENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL38Builtin_Impl_DatePrototypeToTimeStringENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB21416:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$176, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L3520
.L3523:
	leaq	.LC86(%rip), %rax
	xorl	%edx, %edx
	leaq	-208(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -208(%rbp)
	movq	$27, -200(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3538
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L3526:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3532
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3532:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3539
	addq	$176, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3520:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1066, 11(%rdx)
	jne	.L3523
	movq	23(%rax), %rax
	movq	41240(%r12), %rsi
	testb	$1, %al
	jne	.L3540
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L3527:
	leaq	-192(%rbp), %rdi
	movl	$1, %edx
	call	_ZN2v88internal12_GLOBAL__N_112ToDateStringEdPNS0_9DateCacheENS1_16ToDateStringModeE
	movq	-192(%rbp), %rdx
	movq	-184(%rbp), %rax
	movq	%r12, %rdi
	leaq	-208(%rbp), %rsi
	subq	%rdx, %rax
	movq	%rdx, -208(%rbp)
	xorl	%edx, %edx
	cltq
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L3541
	movq	(%rax), %r13
.L3529:
	movq	-192(%rbp), %rdi
	leaq	-168(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3526
	call	free@PLT
	jmp	.L3526
	.p2align 4,,10
	.p2align 3
.L3538:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3540:
	movsd	7(%rax), %xmm0
	jmp	.L3527
	.p2align 4,,10
	.p2align 3
.L3541:
	movq	312(%r12), %r13
	jmp	.L3529
.L3539:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21416:
	.size	_ZN2v88internalL38Builtin_Impl_DatePrototypeToTimeStringENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL38Builtin_Impl_DatePrototypeToTimeStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL44Builtin_Impl_Stats_DatePrototypeToTimeStringEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC87:
	.string	"V8.Builtin_DatePrototypeToTimeString"
	.section	.text._ZN2v88internalL44Builtin_Impl_Stats_DatePrototypeToTimeStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL44Builtin_Impl_Stats_DatePrototypeToTimeStringEiPmPNS0_7IsolateE, @function
_ZN2v88internalL44Builtin_Impl_Stats_DatePrototypeToTimeStringEiPmPNS0_7IsolateE:
.LFB21414:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3571
.L3543:
	movq	_ZZN2v88internalL44Builtin_Impl_Stats_DatePrototypeToTimeStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic841(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3572
.L3545:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3573
.L3547:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL38Builtin_Impl_DatePrototypeToTimeStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3574
.L3551:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3575
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3572:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3576
.L3546:
	movq	%rbx, _ZZN2v88internalL44Builtin_Impl_Stats_DatePrototypeToTimeStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic841(%rip)
	jmp	.L3545
	.p2align 4,,10
	.p2align 3
.L3573:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3577
.L3548:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3549
	movq	(%rdi), %rax
	call	*8(%rax)
.L3549:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3550
	movq	(%rdi), %rax
	call	*8(%rax)
.L3550:
	leaq	.LC87(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L3547
	.p2align 4,,10
	.p2align 3
.L3574:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3551
	.p2align 4,,10
	.p2align 3
.L3571:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$760, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3543
	.p2align 4,,10
	.p2align 3
.L3577:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC87(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L3548
	.p2align 4,,10
	.p2align 3
.L3576:
	leaq	.LC49(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3546
.L3575:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21414:
	.size	_ZN2v88internalL44Builtin_Impl_Stats_DatePrototypeToTimeStringEiPmPNS0_7IsolateE, .-_ZN2v88internalL44Builtin_Impl_Stats_DatePrototypeToTimeStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal33Builtin_DatePrototypeToTimeStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Builtin_DatePrototypeToTimeStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal33Builtin_DatePrototypeToTimeStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal33Builtin_DatePrototypeToTimeStringEiPmPNS0_7IsolateE:
.LFB21415:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3582
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL38Builtin_Impl_DatePrototypeToTimeStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3582:
	.cfi_restore 6
	jmp	_ZN2v88internalL44Builtin_Impl_Stats_DatePrototypeToTimeStringEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21415:
	.size	_ZN2v88internal33Builtin_DatePrototypeToTimeStringEiPmPNS0_7IsolateE, .-_ZN2v88internal33Builtin_DatePrototypeToTimeStringEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal23Builtin_DateConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal23Builtin_DateConstructorEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal23Builtin_DateConstructorEiPmPNS0_7IsolateE:
.LFB26145:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE26145:
	.size	_GLOBAL__sub_I__ZN2v88internal23Builtin_DateConstructorEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal23Builtin_DateConstructorEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal23Builtin_DateConstructorEiPmPNS0_7IsolateE
	.weak	_ZTVN2v88internal29SmallStringOptimizedAllocatorILm128EEE
	.section	.data.rel.ro.local._ZTVN2v88internal29SmallStringOptimizedAllocatorILm128EEE,"awG",@progbits,_ZTVN2v88internal29SmallStringOptimizedAllocatorILm128EEE,comdat
	.align 8
	.type	_ZTVN2v88internal29SmallStringOptimizedAllocatorILm128EEE, @object
	.size	_ZTVN2v88internal29SmallStringOptimizedAllocatorILm128EEE, 48
_ZTVN2v88internal29SmallStringOptimizedAllocatorILm128EEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal29SmallStringOptimizedAllocatorILm128EED1Ev
	.quad	_ZN2v88internal29SmallStringOptimizedAllocatorILm128EED0Ev
	.quad	_ZN2v88internal29SmallStringOptimizedAllocatorILm128EE8allocateEj
	.quad	_ZN2v88internal29SmallStringOptimizedAllocatorILm128EE4growEPj
	.section	.bss._ZZN2v88internalL38Builtin_Impl_Stats_DatePrototypeToJsonEiPmPNS0_7IsolateEE28trace_event_unique_atomic971,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL38Builtin_Impl_Stats_DatePrototypeToJsonEiPmPNS0_7IsolateEE28trace_event_unique_atomic971, @object
	.size	_ZZN2v88internalL38Builtin_Impl_Stats_DatePrototypeToJsonEiPmPNS0_7IsolateEE28trace_event_unique_atomic971, 8
_ZZN2v88internalL38Builtin_Impl_Stats_DatePrototypeToJsonEiPmPNS0_7IsolateEE28trace_event_unique_atomic971:
	.zero	8
	.section	.bss._ZZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetYearEiPmPNS0_7IsolateEE28trace_event_unique_atomic942,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetYearEiPmPNS0_7IsolateEE28trace_event_unique_atomic942, @object
	.size	_ZZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetYearEiPmPNS0_7IsolateEE28trace_event_unique_atomic942, 8
_ZZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetYearEiPmPNS0_7IsolateEE28trace_event_unique_atomic942:
	.zero	8
	.section	.bss._ZZN2v88internalL39Builtin_Impl_Stats_DatePrototypeGetYearEiPmPNS0_7IsolateEE28trace_event_unique_atomic928,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Builtin_Impl_Stats_DatePrototypeGetYearEiPmPNS0_7IsolateEE28trace_event_unique_atomic928, @object
	.size	_ZZN2v88internalL39Builtin_Impl_Stats_DatePrototypeGetYearEiPmPNS0_7IsolateEE28trace_event_unique_atomic928, 8
_ZZN2v88internalL39Builtin_Impl_Stats_DatePrototypeGetYearEiPmPNS0_7IsolateEE28trace_event_unique_atomic928:
	.zero	8
	.section	.bss._ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeToUTCStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic907,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeToUTCStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic907, @object
	.size	_ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeToUTCStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic907, 8
_ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeToUTCStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic907:
	.zero	8
	.section	.bss._ZZN2v88internalL50Builtin_Impl_Stats_DatePrototypeToLocaleTimeStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic888,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL50Builtin_Impl_Stats_DatePrototypeToLocaleTimeStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic888, @object
	.size	_ZZN2v88internalL50Builtin_Impl_Stats_DatePrototypeToLocaleTimeStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic888, 8
_ZZN2v88internalL50Builtin_Impl_Stats_DatePrototypeToLocaleTimeStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic888:
	.zero	8
	.section	.bss._ZZN2v88internalL46Builtin_Impl_Stats_DatePrototypeToLocaleStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic870,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL46Builtin_Impl_Stats_DatePrototypeToLocaleStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic870, @object
	.size	_ZZN2v88internalL46Builtin_Impl_Stats_DatePrototypeToLocaleStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic870, 8
_ZZN2v88internalL46Builtin_Impl_Stats_DatePrototypeToLocaleStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic870:
	.zero	8
	.section	.bss._ZZN2v88internalL50Builtin_Impl_Stats_DatePrototypeToLocaleDateStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic852,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL50Builtin_Impl_Stats_DatePrototypeToLocaleDateStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic852, @object
	.size	_ZZN2v88internalL50Builtin_Impl_Stats_DatePrototypeToLocaleDateStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic852, 8
_ZZN2v88internalL50Builtin_Impl_Stats_DatePrototypeToLocaleDateStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic852:
	.zero	8
	.section	.bss._ZZN2v88internalL44Builtin_Impl_Stats_DatePrototypeToTimeStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic841,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL44Builtin_Impl_Stats_DatePrototypeToTimeStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic841, @object
	.size	_ZZN2v88internalL44Builtin_Impl_Stats_DatePrototypeToTimeStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic841, 8
_ZZN2v88internalL44Builtin_Impl_Stats_DatePrototypeToTimeStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic841:
	.zero	8
	.section	.bss._ZZN2v88internalL40Builtin_Impl_Stats_DatePrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic831,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Builtin_Impl_Stats_DatePrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic831, @object
	.size	_ZZN2v88internalL40Builtin_Impl_Stats_DatePrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic831, 8
_ZZN2v88internalL40Builtin_Impl_Stats_DatePrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic831:
	.zero	8
	.section	.bss._ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeToISOStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic804,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeToISOStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic804, @object
	.size	_ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeToISOStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic804, 8
_ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeToISOStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic804:
	.zero	8
	.section	.bss._ZZN2v88internalL44Builtin_Impl_Stats_DatePrototypeToDateStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic794,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL44Builtin_Impl_Stats_DatePrototypeToDateStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic794, @object
	.size	_ZZN2v88internalL44Builtin_Impl_Stats_DatePrototypeToDateStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic794, 8
_ZZN2v88internalL44Builtin_Impl_Stats_DatePrototypeToDateStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic794:
	.zero	8
	.section	.bss._ZZN2v88internalL45Builtin_Impl_Stats_DatePrototypeSetUTCSecondsEiPmPNS0_7IsolateEE28trace_event_unique_atomic766,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL45Builtin_Impl_Stats_DatePrototypeSetUTCSecondsEiPmPNS0_7IsolateEE28trace_event_unique_atomic766, @object
	.size	_ZZN2v88internalL45Builtin_Impl_Stats_DatePrototypeSetUTCSecondsEiPmPNS0_7IsolateEE28trace_event_unique_atomic766, 8
_ZZN2v88internalL45Builtin_Impl_Stats_DatePrototypeSetUTCSecondsEiPmPNS0_7IsolateEE28trace_event_unique_atomic766:
	.zero	8
	.section	.bss._ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetUTCMonthEiPmPNS0_7IsolateEE28trace_event_unique_atomic738,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetUTCMonthEiPmPNS0_7IsolateEE28trace_event_unique_atomic738, @object
	.size	_ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetUTCMonthEiPmPNS0_7IsolateEE28trace_event_unique_atomic738, 8
_ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetUTCMonthEiPmPNS0_7IsolateEE28trace_event_unique_atomic738:
	.zero	8
	.section	.bss._ZZN2v88internalL45Builtin_Impl_Stats_DatePrototypeSetUTCMinutesEiPmPNS0_7IsolateEE28trace_event_unique_atomic704,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL45Builtin_Impl_Stats_DatePrototypeSetUTCMinutesEiPmPNS0_7IsolateEE28trace_event_unique_atomic704, @object
	.size	_ZZN2v88internalL45Builtin_Impl_Stats_DatePrototypeSetUTCMinutesEiPmPNS0_7IsolateEE28trace_event_unique_atomic704, 8
_ZZN2v88internalL45Builtin_Impl_Stats_DatePrototypeSetUTCMinutesEiPmPNS0_7IsolateEE28trace_event_unique_atomic704:
	.zero	8
	.section	.bss._ZZN2v88internalL50Builtin_Impl_Stats_DatePrototypeSetUTCMillisecondsEiPmPNS0_7IsolateEE28trace_event_unique_atomic684,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL50Builtin_Impl_Stats_DatePrototypeSetUTCMillisecondsEiPmPNS0_7IsolateEE28trace_event_unique_atomic684, @object
	.size	_ZZN2v88internalL50Builtin_Impl_Stats_DatePrototypeSetUTCMillisecondsEiPmPNS0_7IsolateEE28trace_event_unique_atomic684, 8
_ZZN2v88internalL50Builtin_Impl_Stats_DatePrototypeSetUTCMillisecondsEiPmPNS0_7IsolateEE28trace_event_unique_atomic684:
	.zero	8
	.section	.bss._ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetUTCHoursEiPmPNS0_7IsolateEE28trace_event_unique_atomic644,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetUTCHoursEiPmPNS0_7IsolateEE28trace_event_unique_atomic644, @object
	.size	_ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetUTCHoursEiPmPNS0_7IsolateEE28trace_event_unique_atomic644, 8
_ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetUTCHoursEiPmPNS0_7IsolateEE28trace_event_unique_atomic644:
	.zero	8
	.section	.bss._ZZN2v88internalL46Builtin_Impl_Stats_DatePrototypeSetUTCFullYearEiPmPNS0_7IsolateEE28trace_event_unique_atomic609,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL46Builtin_Impl_Stats_DatePrototypeSetUTCFullYearEiPmPNS0_7IsolateEE28trace_event_unique_atomic609, @object
	.size	_ZZN2v88internalL46Builtin_Impl_Stats_DatePrototypeSetUTCFullYearEiPmPNS0_7IsolateEE28trace_event_unique_atomic609, 8
_ZZN2v88internalL46Builtin_Impl_Stats_DatePrototypeSetUTCFullYearEiPmPNS0_7IsolateEE28trace_event_unique_atomic609:
	.zero	8
	.section	.bss._ZZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetUTCDateEiPmPNS0_7IsolateEE28trace_event_unique_atomic591,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetUTCDateEiPmPNS0_7IsolateEE28trace_event_unique_atomic591, @object
	.size	_ZZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetUTCDateEiPmPNS0_7IsolateEE28trace_event_unique_atomic591, 8
_ZZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetUTCDateEiPmPNS0_7IsolateEE28trace_event_unique_atomic591:
	.zero	8
	.section	.bss._ZZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetTimeEiPmPNS0_7IsolateEE28trace_event_unique_atomic581,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetTimeEiPmPNS0_7IsolateEE28trace_event_unique_atomic581, @object
	.size	_ZZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetTimeEiPmPNS0_7IsolateEE28trace_event_unique_atomic581, 8
_ZZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetTimeEiPmPNS0_7IsolateEE28trace_event_unique_atomic581:
	.zero	8
	.section	.bss._ZZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetSecondsEiPmPNS0_7IsolateEE28trace_event_unique_atomic552,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetSecondsEiPmPNS0_7IsolateEE28trace_event_unique_atomic552, @object
	.size	_ZZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetSecondsEiPmPNS0_7IsolateEE28trace_event_unique_atomic552, 8
_ZZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetSecondsEiPmPNS0_7IsolateEE28trace_event_unique_atomic552:
	.zero	8
	.section	.bss._ZZN2v88internalL40Builtin_Impl_Stats_DatePrototypeSetMonthEiPmPNS0_7IsolateEE28trace_event_unique_atomic523,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Builtin_Impl_Stats_DatePrototypeSetMonthEiPmPNS0_7IsolateEE28trace_event_unique_atomic523, @object
	.size	_ZZN2v88internalL40Builtin_Impl_Stats_DatePrototypeSetMonthEiPmPNS0_7IsolateEE28trace_event_unique_atomic523, 8
_ZZN2v88internalL40Builtin_Impl_Stats_DatePrototypeSetMonthEiPmPNS0_7IsolateEE28trace_event_unique_atomic523:
	.zero	8
	.section	.bss._ZZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetMinutesEiPmPNS0_7IsolateEE28trace_event_unique_atomic488,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetMinutesEiPmPNS0_7IsolateEE28trace_event_unique_atomic488, @object
	.size	_ZZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetMinutesEiPmPNS0_7IsolateEE28trace_event_unique_atomic488, 8
_ZZN2v88internalL42Builtin_Impl_Stats_DatePrototypeSetMinutesEiPmPNS0_7IsolateEE28trace_event_unique_atomic488:
	.zero	8
	.section	.bss._ZZN2v88internalL47Builtin_Impl_Stats_DatePrototypeSetMillisecondsEiPmPNS0_7IsolateEE28trace_event_unique_atomic467,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL47Builtin_Impl_Stats_DatePrototypeSetMillisecondsEiPmPNS0_7IsolateEE28trace_event_unique_atomic467, @object
	.size	_ZZN2v88internalL47Builtin_Impl_Stats_DatePrototypeSetMillisecondsEiPmPNS0_7IsolateEE28trace_event_unique_atomic467, 8
_ZZN2v88internalL47Builtin_Impl_Stats_DatePrototypeSetMillisecondsEiPmPNS0_7IsolateEE28trace_event_unique_atomic467:
	.zero	8
	.section	.bss._ZZN2v88internalL40Builtin_Impl_Stats_DatePrototypeSetHoursEiPmPNS0_7IsolateEE28trace_event_unique_atomic426,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Builtin_Impl_Stats_DatePrototypeSetHoursEiPmPNS0_7IsolateEE28trace_event_unique_atomic426, @object
	.size	_ZZN2v88internalL40Builtin_Impl_Stats_DatePrototypeSetHoursEiPmPNS0_7IsolateEE28trace_event_unique_atomic426, 8
_ZZN2v88internalL40Builtin_Impl_Stats_DatePrototypeSetHoursEiPmPNS0_7IsolateEE28trace_event_unique_atomic426:
	.zero	8
	.section	.bss._ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetFullYearEiPmPNS0_7IsolateEE28trace_event_unique_atomic390,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetFullYearEiPmPNS0_7IsolateEE28trace_event_unique_atomic390, @object
	.size	_ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetFullYearEiPmPNS0_7IsolateEE28trace_event_unique_atomic390, 8
_ZZN2v88internalL43Builtin_Impl_Stats_DatePrototypeSetFullYearEiPmPNS0_7IsolateEE28trace_event_unique_atomic390:
	.zero	8
	.section	.bss._ZZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetDateEiPmPNS0_7IsolateEE28trace_event_unique_atomic370,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetDateEiPmPNS0_7IsolateEE28trace_event_unique_atomic370, @object
	.size	_ZZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetDateEiPmPNS0_7IsolateEE28trace_event_unique_atomic370, 8
_ZZN2v88internalL39Builtin_Impl_Stats_DatePrototypeSetDateEiPmPNS0_7IsolateEE28trace_event_unique_atomic370:
	.zero	8
	.section	.bss._ZZN2v88internalL26Builtin_Impl_Stats_DateUTCEiPmPNS0_7IsolateEE28trace_event_unique_atomic310,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL26Builtin_Impl_Stats_DateUTCEiPmPNS0_7IsolateEE28trace_event_unique_atomic310, @object
	.size	_ZZN2v88internalL26Builtin_Impl_Stats_DateUTCEiPmPNS0_7IsolateEE28trace_event_unique_atomic310, 8
_ZZN2v88internalL26Builtin_Impl_Stats_DateUTCEiPmPNS0_7IsolateEE28trace_event_unique_atomic310:
	.zero	8
	.section	.bss._ZZN2v88internalL28Builtin_Impl_Stats_DateParseEiPmPNS0_7IsolateEE28trace_event_unique_atomic300,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Builtin_Impl_Stats_DateParseEiPmPNS0_7IsolateEE28trace_event_unique_atomic300, @object
	.size	_ZZN2v88internalL28Builtin_Impl_Stats_DateParseEiPmPNS0_7IsolateEE28trace_event_unique_atomic300, 8
_ZZN2v88internalL28Builtin_Impl_Stats_DateParseEiPmPNS0_7IsolateEE28trace_event_unique_atomic300:
	.zero	8
	.section	.bss._ZZN2v88internalL26Builtin_Impl_Stats_DateNowEiPmPNS0_7IsolateEE28trace_event_unique_atomic294,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL26Builtin_Impl_Stats_DateNowEiPmPNS0_7IsolateEE28trace_event_unique_atomic294, @object
	.size	_ZZN2v88internalL26Builtin_Impl_Stats_DateNowEiPmPNS0_7IsolateEE28trace_event_unique_atomic294, 8
_ZZN2v88internalL26Builtin_Impl_Stats_DateNowEiPmPNS0_7IsolateEE28trace_event_unique_atomic294:
	.zero	8
	.section	.bss._ZZN2v88internalL34Builtin_Impl_Stats_DateConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic206,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL34Builtin_Impl_Stats_DateConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic206, @object
	.size	_ZZN2v88internalL34Builtin_Impl_Stats_DateConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic206, 8
_ZZN2v88internalL34Builtin_Impl_Stats_DateConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic206:
	.zero	8
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC88:
	.string	"Jan"
.LC89:
	.string	"Feb"
.LC90:
	.string	"Mar"
.LC91:
	.string	"Apr"
.LC92:
	.string	"May"
.LC93:
	.string	"Jun"
.LC94:
	.string	"Jul"
.LC95:
	.string	"Aug"
.LC96:
	.string	"Sep"
.LC97:
	.string	"Oct"
.LC98:
	.string	"Nov"
.LC99:
	.string	"Dec"
	.section	.data.rel.ro.local._ZN2v88internal12_GLOBAL__N_112kShortMonthsE,"aw"
	.align 32
	.type	_ZN2v88internal12_GLOBAL__N_112kShortMonthsE, @object
	.size	_ZN2v88internal12_GLOBAL__N_112kShortMonthsE, 96
_ZN2v88internal12_GLOBAL__N_112kShortMonthsE:
	.quad	.LC88
	.quad	.LC89
	.quad	.LC90
	.quad	.LC91
	.quad	.LC92
	.quad	.LC93
	.quad	.LC94
	.quad	.LC95
	.quad	.LC96
	.quad	.LC97
	.quad	.LC98
	.quad	.LC99
	.section	.rodata.str1.1
.LC100:
	.string	"Sun"
.LC101:
	.string	"Mon"
.LC102:
	.string	"Tue"
.LC103:
	.string	"Wed"
.LC104:
	.string	"Thu"
.LC105:
	.string	"Fri"
.LC106:
	.string	"Sat"
	.section	.data.rel.ro.local._ZN2v88internal12_GLOBAL__N_114kShortWeekDaysE,"aw"
	.align 32
	.type	_ZN2v88internal12_GLOBAL__N_114kShortWeekDaysE, @object
	.size	_ZN2v88internal12_GLOBAL__N_114kShortWeekDaysE, 56
_ZN2v88internal12_GLOBAL__N_114kShortWeekDaysE:
	.quad	.LC100
	.quad	.LC101
	.quad	.LC102
	.quad	.LC103
	.quad	.LC104
	.quad	.LC105
	.quad	.LC106
	.section	.rodata._ZZN2v88internal12_GLOBAL__N_17MakeDayEdddE13kDayFromMonth_0,"a"
	.align 32
	.type	_ZZN2v88internal12_GLOBAL__N_17MakeDayEdddE13kDayFromMonth_0, @object
	.size	_ZZN2v88internal12_GLOBAL__N_17MakeDayEdddE13kDayFromMonth_0, 48
_ZZN2v88internal12_GLOBAL__N_17MakeDayEdddE13kDayFromMonth_0:
	.long	0
	.long	31
	.long	60
	.long	91
	.long	121
	.long	152
	.long	182
	.long	213
	.long	244
	.long	274
	.long	305
	.long	335
	.section	.rodata._ZZN2v88internal12_GLOBAL__N_17MakeDayEdddE13kDayFromMonth,"a"
	.align 32
	.type	_ZZN2v88internal12_GLOBAL__N_17MakeDayEdddE13kDayFromMonth, @object
	.size	_ZZN2v88internal12_GLOBAL__N_17MakeDayEdddE13kDayFromMonth, 48
_ZZN2v88internal12_GLOBAL__N_17MakeDayEdddE13kDayFromMonth:
	.long	0
	.long	31
	.long	59
	.long	90
	.long	120
	.long	151
	.long	181
	.long	212
	.long	243
	.long	273
	.long	304
	.long	334
	.section	.rodata._ZN2v88internalL15kAsciiCharFlagsE,"a"
	.align 32
	.type	_ZN2v88internalL15kAsciiCharFlagsE, @object
	.size	_ZN2v88internalL15kAsciiCharFlagsE, 128
_ZN2v88internalL15kAsciiCharFlagsE:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\f\b\f\f\b"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\f"
	.string	""
	.string	""
	.string	"\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\002\002\002\002\002\002\002\002\002\002"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"
	.string	"\003"
	.string	""
	.string	"\003"
	.string	"\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	2146959360
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC3:
	.long	4294967295
	.long	2146435071
	.align 8
.LC4:
	.long	0
	.long	1127219200
	.align 8
.LC5:
	.long	0
	.long	1072693248
	.align 8
.LC6:
	.long	0
	.long	1095464768
	.align 8
.LC7:
	.long	0
	.long	1089293312
	.align 8
.LC8:
	.long	0
	.long	1083129856
	.align 8
.LC26:
	.long	0
	.long	1100257648
	.align 8
.LC29:
	.long	1566230528
	.long	-1019301367
	.align 8
.LC30:
	.long	1566230528
	.long	1128182281
	.align 8
.LC33:
	.long	0
	.long	1079558144
	.align 8
.LC34:
	.long	0
	.long	1084076032
	.align 8
.LC35:
	.long	0
	.long	-1053916032
	.align 8
.LC36:
	.long	0
	.long	1093567616
	.align 8
.LC37:
	.long	0
	.long	-1050471728
	.align 8
.LC38:
	.long	0
	.long	1097011920
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
