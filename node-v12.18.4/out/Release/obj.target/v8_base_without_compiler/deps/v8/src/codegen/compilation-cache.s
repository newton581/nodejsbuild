	.file	"compilation-cache.cc"
	.text
	.section	.rodata._ZN2v88internal8NewArrayINS0_6ObjectEEEPT_m.constprop.1.str1.1,"aMS",@progbits,1
.LC0:
	.string	"NewArray"
	.section	.text._ZN2v88internal8NewArrayINS0_6ObjectEEEPT_m.constprop.1,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8NewArrayINS0_6ObjectEEEPT_m.constprop.1, @function
_ZN2v88internal8NewArrayINS0_6ObjectEEEPT_m.constprop.1:
.LFB22432:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$8, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L2
.L4:
	movq	$0, (%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2:
	.cfi_restore_state
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$8, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	jne	.L4
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE22432:
	.size	_ZN2v88internal8NewArrayINS0_6ObjectEEEPT_m.constprop.1, .-_ZN2v88internal8NewArrayINS0_6ObjectEEEPT_m.constprop.1
	.section	.text._ZN2v88internal16CompilationCacheC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16CompilationCacheC2EPNS0_7IsolateE
	.type	_ZN2v88internal16CompilationCacheC2EPNS0_7IsolateE, @function
_ZN2v88internal16CompilationCacheC2EPNS0_7IsolateE:
.LFB18458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	$1, 16(%rdi)
	movups	%xmm0, (%rdi)
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal8NewArrayINS0_6ObjectEEEPT_m.constprop.1
	movl	$1, 40(%rbx)
	movq	%rax, %xmm0
	movhps	-24(%rbp), %xmm0
	movups	%xmm0, 24(%rbx)
	call	_ZN2v88internal8NewArrayINS0_6ObjectEEEPT_m.constprop.1
	movl	$1, 64(%rbx)
	movq	%rax, %xmm0
	movhps	-24(%rbp), %xmm0
	movups	%xmm0, 48(%rbx)
	call	_ZN2v88internal8NewArrayINS0_6ObjectEEEPT_m.constprop.1
	movl	$2, 88(%rbx)
	movl	$16, %edi
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%rax, %xmm0
	movhps	-24(%rbp), %xmm0
	movups	%xmm0, 72(%rbx)
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L10
.L12:
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	%rax, 96(%rbx)
	leaq	8(%rbx), %rax
	movq	%rax, 104(%rbx)
	leaq	32(%rbx), %rax
	movq	%rax, 112(%rbx)
	leaq	56(%rbx), %rax
	movq	%rax, 120(%rbx)
	leaq	80(%rbx), %rax
	movb	$1, 136(%rbx)
	movq	%rax, 128(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L10:
	.cfi_restore_state
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$16, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	jne	.L12
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE18458:
	.size	_ZN2v88internal16CompilationCacheC2EPNS0_7IsolateE, .-_ZN2v88internal16CompilationCacheC2EPNS0_7IsolateE
	.globl	_ZN2v88internal16CompilationCacheC1EPNS0_7IsolateE
	.set	_ZN2v88internal16CompilationCacheC1EPNS0_7IsolateE,_ZN2v88internal16CompilationCacheC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal19CompilationSubCache8GetTableEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19CompilationSubCache8GetTableEi
	.type	_ZN2v88internal19CompilationSubCache8GetTableEi, @function
_ZN2v88internal19CompilationSubCache8GetTableEi:
.LFB18460:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movslq	%esi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	16(%rdi), %rax
	movq	(%rdi), %r13
	movq	(%rax,%r12,8), %rsi
	cmpq	%rsi, 88(%r13)
	je	.L23
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L20
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L24
.L21:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movq	%rdi, %rbx
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movl	$64, %esi
	call	_ZN2v88internal9HashTableINS0_21CompilationCacheTableENS0_21CompilationCacheShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	16(%rbx), %rdx
	movq	(%rax), %rcx
	movq	%rcx, (%rdx,%r12,8)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	%r13, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L21
	.cfi_endproc
.LFE18460:
	.size	_ZN2v88internal19CompilationSubCache8GetTableEi, .-_ZN2v88internal19CompilationSubCache8GetTableEi
	.section	.text._ZN2v88internal19CompilationSubCache3AgeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19CompilationSubCache3AgeEv
	.type	_ZN2v88internal19CompilationSubCache3AgeEv, @function
_ZN2v88internal19CompilationSubCache3AgeEv:
.LFB18461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	8(%rdi), %edx
	movq	16(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %edx
	je	.L34
	leal	-1(%rdx), %eax
	testl	%eax, %eax
	jle	.L28
	cltq
	movq	-8(%rcx,%rax,8), %rsi
	movq	%rsi, (%rcx,%rax,8)
	cmpl	$2, %edx
	je	.L30
	leal	-2(%rdx), %eax
	movslq	%edx, %rcx
	subl	$3, %edx
	cltq
	subq	%rdx, %rcx
	salq	$3, %rax
	leaq	-24(,%rcx,8), %rsi
	.p2align 4,,10
	.p2align 3
.L31:
	movq	16(%rdi), %rdx
	movq	-8(%rdx,%rax), %rcx
	movq	%rcx, (%rdx,%rax)
	subq	$8, %rax
	cmpq	%rsi, %rax
	jne	.L31
.L30:
	movq	16(%rdi), %rcx
.L28:
	movq	(%rdi), %rax
	movq	88(%rax), %rax
	movq	%rax, (%rcx)
.L25:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L35
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movq	(%rdi), %rdx
	movq	(%rcx), %rax
	cmpq	%rax, 88(%rdx)
	je	.L25
	leaq	-16(%rbp), %rdi
	movq	%rax, -16(%rbp)
	call	_ZN2v88internal21CompilationCacheTable3AgeEv@PLT
	jmp	.L25
.L35:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18461:
	.size	_ZN2v88internal19CompilationSubCache3AgeEv, .-_ZN2v88internal19CompilationSubCache3AgeEv
	.section	.text._ZN2v88internal19CompilationSubCache7IterateEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19CompilationSubCache7IterateEPNS0_11RootVisitorE
	.type	_ZN2v88internal19CompilationSubCache7IterateEPNS0_11RootVisitorE, @function
_ZN2v88internal19CompilationSubCache7IterateEPNS0_11RootVisitorE:
.LFB18462:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	movq	(%rsi), %rdx
	movq	%rsi, %rdi
	movl	$9, %esi
	movq	16(%rax), %rcx
	movslq	8(%rax), %rax
	movq	16(%rdx), %r9
	xorl	%edx, %edx
	leaq	(%rcx,%rax,8), %r8
	jmp	*%r9
	.cfi_endproc
.LFE18462:
	.size	_ZN2v88internal19CompilationSubCache7IterateEPNS0_11RootVisitorE, .-_ZN2v88internal19CompilationSubCache7IterateEPNS0_11RootVisitorE
	.section	.text._ZN2v88internal19CompilationSubCache5ClearEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19CompilationSubCache5ClearEv
	.type	_ZN2v88internal19CompilationSubCache5ClearEv, @function
_ZN2v88internal19CompilationSubCache5ClearEv:
.LFB18463:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movslq	8(%rdi), %rcx
	movq	16(%rdi), %rdi
	movq	88(%rax), %rax
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	ret
	.cfi_endproc
.LFE18463:
	.size	_ZN2v88internal19CompilationSubCache5ClearEv, .-_ZN2v88internal19CompilationSubCache5ClearEv
	.section	.text._ZN2v88internal19CompilationSubCache6RemoveENS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19CompilationSubCache6RemoveENS0_6HandleINS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal19CompilationSubCache6RemoveENS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal19CompilationSubCache6RemoveENS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB18464:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%r12), %rax
	movq	%rax, -72(%rbp)
	movq	41096(%r12), %rax
	movq	%rax, -80(%rbp)
	movl	41104(%r12), %eax
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movl	8(%rdi), %edx
	testl	%edx, %edx
	jle	.L39
	movq	%rdi, %r14
	movq	%rsi, %rbx
	leaq	-64(%rbp), %r13
	xorl	%r15d, %r15d
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L51:
	movq	%r10, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L41:
	movq	%rsi, -64(%rbp)
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	addq	$1, %r15
	call	_ZN2v88internal21CompilationCacheTable6RemoveENS0_6ObjectE@PLT
	cmpl	%r15d, 8(%r14)
	jle	.L49
.L44:
	movq	16(%r14), %rax
	movq	(%r14), %rdi
	movq	(%rax,%r15,8), %rsi
	cmpq	%rsi, 88(%rdi)
	je	.L50
	movq	41112(%rdi), %r10
	testq	%r10, %r10
	jne	.L51
	movq	41088(%rdi), %rax
	cmpq	41096(%rdi), %rax
	je	.L52
.L43:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rdi)
	movq	%rsi, (%rax)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L50:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$64, %esi
	call	_ZN2v88internal9HashTableINS0_21CompilationCacheTableENS0_21CompilationCacheShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	16(%r14), %rdx
	movq	(%rax), %rcx
	movq	%rcx, (%rdx,%r15,8)
	movq	(%rax), %rsi
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L49:
	subl	$1, 41104(%r12)
	movq	-72(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-80(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L38
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L38:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L53
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	movq	%rsi, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdi
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L39:
	movl	%eax, 41104(%r12)
	jmp	.L38
.L53:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18464:
	.size	_ZN2v88internal19CompilationSubCache6RemoveENS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZN2v88internal19CompilationSubCache6RemoveENS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.text._ZN2v88internal22CompilationCacheScriptC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22CompilationCacheScriptC2EPNS0_7IsolateE
	.type	_ZN2v88internal22CompilationCacheScriptC2EPNS0_7IsolateE, @function
_ZN2v88internal22CompilationCacheScriptC2EPNS0_7IsolateE:
.LFB18466:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rsi, (%rdi)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$1, 8(%rdi)
	movl	$8, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L55
.L57:
	movq	$0, (%rax)
	movq	%rax, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L55:
	.cfi_restore_state
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$8, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	jne	.L57
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE18466:
	.size	_ZN2v88internal22CompilationCacheScriptC2EPNS0_7IsolateE, .-_ZN2v88internal22CompilationCacheScriptC2EPNS0_7IsolateE
	.globl	_ZN2v88internal22CompilationCacheScriptC1EPNS0_7IsolateE
	.set	_ZN2v88internal22CompilationCacheScriptC1EPNS0_7IsolateE,_ZN2v88internal22CompilationCacheScriptC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal22CompilationCacheScript9HasOriginENS0_6HandleINS0_18SharedFunctionInfoEEENS0_11MaybeHandleINS0_6ObjectEEEiiNS_19ScriptOriginOptionsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22CompilationCacheScript9HasOriginENS0_6HandleINS0_18SharedFunctionInfoEEENS0_11MaybeHandleINS0_6ObjectEEEiiNS_19ScriptOriginOptionsE
	.type	_ZN2v88internal22CompilationCacheScript9HasOriginENS0_6HandleINS0_18SharedFunctionInfoEEENS0_11MaybeHandleINS0_6ObjectEEEiiNS_19ScriptOriginOptionsE, @function
_ZN2v88internal22CompilationCacheScript9HasOriginENS0_6HandleINS0_18SharedFunctionInfoEEENS0_11MaybeHandleINS0_6ObjectEEEiiNS_19ScriptOriginOptionsE:
.LFB18468:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$24, %rsp
	movq	(%rsi), %rax
	movq	(%rdi), %r15
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L87
.L63:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L64
	movl	%r8d, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-56(%rbp), %r8d
	movq	(%rax), %rsi
	testq	%r12, %r12
	je	.L88
.L67:
	cmpl	%r14d, 27(%rsi)
	je	.L69
.L71:
	xorl	%r14d, %r14d
.L62:
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	cmpl	%r8d, 35(%rsi)
	jne	.L71
	movq	(%r12), %rdx
	testb	$1, %dl
	je	.L71
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L71
	movq	(%rax), %rax
	movq	15(%rax), %r15
	movq	%r15, %r14
	notq	%r14
	andl	$1, %r14d
	jne	.L71
	movq	-1(%r15), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L71
	movslq	99(%rax), %rax
	sarl	$2, %eax
	andl	$15, %eax
	cmpl	%eax, %ebx
	jne	.L71
	movq	0(%r13), %rbx
	movq	%r15, %rcx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L73
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	0(%r13), %rdi
	movq	%rax, %rdx
	movq	(%r12), %rax
	cmpq	%rdx, %r12
	je	.L77
	testq	%rdx, %rdx
	je	.L79
	movq	(%rdx), %rcx
.L78:
	cmpq	%rax, %rcx
	je	.L77
.L79:
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L81
	movq	(%rdx), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	je	.L62
.L81:
	addq	$24, %rsp
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6String10SlowEqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movq	41088(%r15), %rax
	cmpq	%rax, 41096(%r15)
	je	.L89
.L66:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	testq	%r12, %r12
	jne	.L67
.L88:
	movq	0(%r13), %rax
	movq	15(%rsi), %rbx
	cmpq	%rbx, 88(%rax)
	sete	%r14b
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L87:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L63
	movq	23(%rsi), %rsi
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L89:
	movq	%r15, %rdi
	movl	%r8d, -60(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-60(%rbp), %r8d
	movq	-56(%rbp), %rsi
	jmp	.L66
.L73:
	movq	41088(%rbx), %rdx
	cmpq	41096(%rbx), %rdx
	je	.L90
.L76:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rbx)
	movq	%r15, (%rdx)
	movq	(%r12), %rax
	movq	0(%r13), %rdi
	cmpq	%rdx, %r12
	jne	.L78
.L77:
	movl	$1, %r14d
	jmp	.L62
.L90:
	movq	%rbx, %rdi
	movq	%r15, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L76
	.cfi_endproc
.LFE18468:
	.size	_ZN2v88internal22CompilationCacheScript9HasOriginENS0_6HandleINS0_18SharedFunctionInfoEEENS0_11MaybeHandleINS0_6ObjectEEEiiNS_19ScriptOriginOptionsE, .-_ZN2v88internal22CompilationCacheScript9HasOriginENS0_6HandleINS0_18SharedFunctionInfoEEENS0_11MaybeHandleINS0_6ObjectEEEiiNS_19ScriptOriginOptionsE
	.section	.rodata._ZN2v88internal22CompilationCacheScript6LookupENS0_6HandleINS0_6StringEEENS0_11MaybeHandleINS0_6ObjectEEEiiNS_19ScriptOriginOptionsENS2_INS0_7ContextEEENS0_12LanguageModeE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"script"
.LC2:
	.string	"hit"
	.section	.text._ZN2v88internal22CompilationCacheScript6LookupENS0_6HandleINS0_6StringEEENS0_11MaybeHandleINS0_6ObjectEEEiiNS_19ScriptOriginOptionsENS2_INS0_7ContextEEENS0_12LanguageModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22CompilationCacheScript6LookupENS0_6HandleINS0_6StringEEENS0_11MaybeHandleINS0_6ObjectEEEiiNS_19ScriptOriginOptionsENS2_INS0_7ContextEEENS0_12LanguageModeE
	.type	_ZN2v88internal22CompilationCacheScript6LookupENS0_6HandleINS0_6StringEEENS0_11MaybeHandleINS0_6ObjectEEEiiNS_19ScriptOriginOptionsENS2_INS0_7ContextEEENS0_12LanguageModeE, @function
_ZN2v88internal22CompilationCacheScript6LookupENS0_6HandleINS0_6StringEEENS0_11MaybeHandleINS0_6ObjectEEEiiNS_19ScriptOriginOptionsENS2_INS0_7ContextEEENS0_12LanguageModeE:
.LFB18469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rdi), %r12
	movl	%ecx, -68(%rbp)
	movl	24(%rbp), %ecx
	movl	%r8d, -64(%rbp)
	movq	41088(%r12), %rax
	movl	%r9d, -60(%rbp)
	movl	%ecx, -56(%rbp)
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal19CompilationSubCache8GetTableEi
	movl	-56(%rbp), %ecx
	movq	16(%rbp), %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal21CompilationCacheTable12LookupScriptENS0_6HandleIS1_EENS2_INS0_6StringEEENS2_INS0_7ContextEEENS0_12LanguageModeE@PLT
	testq	%rax, %rax
	je	.L92
	movl	-68(%rbp), %r11d
	movl	-60(%rbp), %r9d
	movq	%r15, %rdx
	movq	%rax, %rsi
	movl	-64(%rbp), %r8d
	movq	%rbx, %rdi
	movq	%rax, %r13
	movl	%r11d, %ecx
	call	_ZN2v88internal22CompilationCacheScript9HasOriginENS0_6HandleINS0_18SharedFunctionInfoEEENS0_11MaybeHandleINS0_6ObjectEEEiiNS_19ScriptOriginOptionsE
	testb	%al, %al
	jne	.L93
.L92:
	movl	41104(%r12), %eax
	movq	-80(%rbp), %rsi
	movq	41096(%r12), %rdx
	subl	$1, %eax
	movq	%rsi, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %r14
	je	.L101
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L101:
	movq	(%rbx), %rax
	xorl	%r13d, %r13d
	movq	40960(%rax), %r12
.L99:
	cmpb	$0, 6368(%r12)
	je	.L102
	movq	6360(%r12), %rax
.L103:
	testq	%rax, %rax
	je	.L114
	addl	$1, (%rax)
.L114:
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	movq	-80(%rbp), %rax
	movq	0(%r13), %rsi
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L95
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
.L95:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L96
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L97:
	movq	(%rbx), %rax
	movq	40960(%rax), %r12
	testq	%r13, %r13
	je	.L99
	cmpb	$0, 6336(%r12)
	je	.L106
	movq	6328(%r12), %rax
.L107:
	testq	%rax, %rax
	je	.L108
	addl	$1, (%rax)
.L108:
	movq	(%rbx), %rax
	movq	41016(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L114
	movq	0(%r13), %rcx
	leaq	.LC1(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger21CompilationCacheEventEPKcS3_NS0_18SharedFunctionInfoE@PLT
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L96:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L123
.L98:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L106:
	movb	$1, 6336(%r12)
	leaq	6312(%r12), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 6328(%r12)
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L102:
	movb	$1, 6368(%r12)
	leaq	6344(%r12), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 6360(%r12)
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L123:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L98
	.cfi_endproc
.LFE18469:
	.size	_ZN2v88internal22CompilationCacheScript6LookupENS0_6HandleINS0_6StringEEENS0_11MaybeHandleINS0_6ObjectEEEiiNS_19ScriptOriginOptionsENS2_INS0_7ContextEEENS0_12LanguageModeE, .-_ZN2v88internal22CompilationCacheScript6LookupENS0_6HandleINS0_6StringEEENS0_11MaybeHandleINS0_6ObjectEEEiiNS_19ScriptOriginOptionsENS2_INS0_7ContextEEENS0_12LanguageModeE
	.section	.text._ZN2v88internal22CompilationCacheScript3PutENS0_6HandleINS0_6StringEEENS2_INS0_7ContextEEENS0_12LanguageModeENS2_INS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22CompilationCacheScript3PutENS0_6HandleINS0_6StringEEENS2_INS0_7ContextEEENS0_12LanguageModeENS2_INS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal22CompilationCacheScript3PutENS0_6HandleINS0_6StringEEENS2_INS0_7ContextEEENS0_12LanguageModeENS2_INS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal22CompilationCacheScript3PutENS0_6HandleINS0_6StringEEENS2_INS0_7ContextEEENS0_12LanguageModeENS2_INS0_18SharedFunctionInfoEEE:
.LFB18473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movl	%ecx, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	(%rdi), %r12
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	16(%rdi), %rax
	movq	(%rdi), %r11
	movq	(%rax), %rsi
	cmpq	%rsi, 88(%r11)
	je	.L132
	movq	41112(%r11), %rdi
	testq	%rdi, %rdi
	je	.L127
	movq	%r8, -72(%rbp)
	movl	%ecx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %r9
	movl	-64(%rbp), %r10d
	movq	-72(%rbp), %r8
	movq	%rax, %rdi
.L126:
	movq	%r9, %rdx
	movl	%r10d, %ecx
	movq	%r15, %rsi
	call	_ZN2v88internal21CompilationCacheTable9PutScriptENS0_6HandleIS1_EENS2_INS0_6StringEEENS2_INS0_7ContextEEENS0_12LanguageModeENS2_INS0_18SharedFunctionInfoEEE@PLT
	movq	%rax, %r8
	movq	16(%rbx), %rax
	movq	(%r8), %rdx
	movq	%rdx, (%rax)
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L133
	movq	%r13, 41096(%r12)
	addq	$56, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movq	41088(%r11), %rdi
	cmpq	41096(%r11), %rdi
	je	.L134
.L128:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r11)
	movq	%rsi, (%rdi)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L133:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	movl	%ecx, -64(%rbp)
	movq	%r11, %rdi
	xorl	%ecx, %ecx
	movl	$64, %esi
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal9HashTableINS0_21CompilationCacheTableENS0_21CompilationCacheShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	-56(%rbp), %r9
	movl	-64(%rbp), %r10d
	movq	(%rax), %rdx
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	movq	-72(%rbp), %r8
	movq	%rdx, (%rax)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L134:
	movq	%r11, %rdi
	movq	%r8, -88(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%r11, -56(%rbp)
	movl	%ecx, -76(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r8
	movl	-76(%rbp), %r10d
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %rsi
	movq	%rax, %rdi
	movq	-56(%rbp), %r11
	jmp	.L128
	.cfi_endproc
.LFE18473:
	.size	_ZN2v88internal22CompilationCacheScript3PutENS0_6HandleINS0_6StringEEENS2_INS0_7ContextEEENS0_12LanguageModeENS2_INS0_18SharedFunctionInfoEEE, .-_ZN2v88internal22CompilationCacheScript3PutENS0_6HandleINS0_6StringEEENS2_INS0_7ContextEEENS0_12LanguageModeENS2_INS0_18SharedFunctionInfoEEE
	.section	.text._ZN2v88internal20CompilationCacheEval6LookupENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20CompilationCacheEval6LookupENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeEi
	.type	_ZN2v88internal20CompilationCacheEval6LookupENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeEi, @function
_ZN2v88internal20CompilationCacheEval6LookupENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeEi:
.LFB18474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%r12), %rax
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	(%rsi), %rdi
	movq	%rax, -72(%rbp)
	movq	16(%rsi), %rax
	movq	(%rax), %r11
	cmpq	%r11, 88(%rdi)
	je	.L160
	movq	41112(%rdi), %rax
	testq	%rax, %rax
	je	.L138
	movq	%r11, %rsi
	movq	%rax, %rdi
	movl	%r9d, -96(%rbp)
	movq	%r8, -88(%rbp)
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-80(%rbp), %r10
	movq	-88(%rbp), %r8
	movl	-96(%rbp), %r9d
	movq	%rax, %rsi
.L137:
	movl	16(%rbp), %eax
	subq	$8, %rsp
	movq	%r15, %rdx
	movq	%r10, %rcx
	movq	%r13, %rdi
	pushq	%rax
	call	_ZN2v88internal21CompilationCacheTable10LookupEvalENS0_6HandleIS1_EENS2_INS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeEi@PLT
	popq	%rax
	movq	(%rbx), %rax
	cmpq	$0, 16(%r13)
	popq	%rdx
	movq	40960(%rax), %rbx
	je	.L141
	cmpb	$0, 8(%r13)
	je	.L141
	cmpb	$0, 6336(%rbx)
	je	.L142
	movq	6328(%rbx), %rax
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L138:
	movq	41088(%rdi), %rsi
	cmpq	41096(%rdi), %rsi
	je	.L161
.L139:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdi)
	movq	%r11, (%rsi)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L141:
	cmpb	$0, 6368(%rbx)
	je	.L146
	movq	6360(%rbx), %rax
.L147:
	testq	%rax, %rax
	je	.L145
	addl	$1, (%rax)
.L145:
	subl	$1, 41104(%r12)
	movq	-72(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L135
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L135:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L162
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	movb	$1, 6368(%rbx)
	leaq	6344(%rbx), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 6360(%rbx)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L160:
	movq	%rcx, -80(%rbp)
	xorl	%edx, %edx
	movl	$64, %esi
	xorl	%ecx, %ecx
	movl	%r9d, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal9HashTableINS0_21CompilationCacheTableENS0_21CompilationCacheShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	-80(%rbp), %r10
	movq	-88(%rbp), %r8
	movq	(%rax), %rdx
	movq	%rax, %rsi
	movq	16(%rbx), %rax
	movl	-96(%rbp), %r9d
	movq	%rdx, (%rax)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L142:
	movb	$1, 6336(%rbx)
	leaq	6312(%rbx), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 6328(%rbx)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L161:
	movl	%r9d, -108(%rbp)
	movq	%r8, -104(%rbp)
	movq	%r11, -88(%rbp)
	movq	%rdi, -80(%rbp)
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-108(%rbp), %r9d
	movq	-104(%rbp), %r8
	movq	-96(%rbp), %r10
	movq	-88(%rbp), %r11
	movq	%rax, %rsi
	movq	-80(%rbp), %rdi
	jmp	.L139
.L162:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18474:
	.size	_ZN2v88internal20CompilationCacheEval6LookupENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeEi, .-_ZN2v88internal20CompilationCacheEval6LookupENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeEi
	.section	.text._ZN2v88internal20CompilationCacheEval3PutENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEES6_NS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20CompilationCacheEval3PutENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEES6_NS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEEi
	.type	_ZN2v88internal20CompilationCacheEval3PutENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEES6_NS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEEi, @function
_ZN2v88internal20CompilationCacheEval3PutENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEES6_NS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEEi:
.LFB18475:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	(%rdi), %r12
	movl	16(%rbp), %eax
	movl	%eax, -60(%rbp)
	movq	41088(%r12), %rax
	addl	$1, 41104(%r12)
	movq	(%rdi), %r11
	movq	%rax, -56(%rbp)
	movq	16(%rdi), %rax
	movq	41096(%r12), %r14
	movq	(%rax), %rsi
	cmpq	%rsi, 88(%r11)
	je	.L171
	movq	41112(%r11), %rdi
	testq	%rdi, %rdi
	je	.L166
	movq	%r9, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %r10
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %r9
	movq	%rax, %rdi
.L165:
	movl	-60(%rbp), %eax
	subq	$8, %rsp
	movq	%r15, %rdx
	movq	%r10, %rcx
	movq	%r13, %rsi
	pushq	%rax
	call	_ZN2v88internal21CompilationCacheTable7PutEvalENS0_6HandleIS1_EENS2_INS0_6StringEEENS2_INS0_18SharedFunctionInfoEEES7_NS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEEi@PLT
	movq	%rax, %r8
	movq	16(%rbx), %rax
	movq	(%r8), %rdx
	movq	%rdx, (%rax)
	movq	-56(%rbp), %rax
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	popq	%rax
	popq	%rdx
	cmpq	41096(%r12), %r14
	je	.L172
	movq	%r14, 41096(%r12)
	leaq	-40(%rbp), %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	movq	41088(%r11), %rdi
	cmpq	41096(%r11), %rdi
	je	.L173
.L167:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r11)
	movq	%rsi, (%rdi)
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L172:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	movq	%rcx, -72(%rbp)
	xorl	%edx, %edx
	movq	%r11, %rdi
	xorl	%ecx, %ecx
	movl	$64, %esi
	movq	%r9, -88(%rbp)
	movq	%r8, -80(%rbp)
	call	_ZN2v88internal9HashTableINS0_21CompilationCacheTableENS0_21CompilationCacheShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	-72(%rbp), %r10
	movq	-80(%rbp), %r8
	movq	(%rax), %rdx
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	movq	-88(%rbp), %r9
	movq	%rdx, (%rax)
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L173:
	movq	%r11, %rdi
	movq	%r9, -104(%rbp)
	movq	%r8, -96(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%r11, -72(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %r9
	movq	-96(%rbp), %r8
	movq	-88(%rbp), %r10
	movq	-80(%rbp), %rsi
	movq	%rax, %rdi
	movq	-72(%rbp), %r11
	jmp	.L167
	.cfi_endproc
.LFE18475:
	.size	_ZN2v88internal20CompilationCacheEval3PutENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEES6_NS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEEi, .-_ZN2v88internal20CompilationCacheEval3PutENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEES6_NS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEEi
	.section	.text._ZN2v88internal22CompilationCacheRegExp3PutENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS2_INS0_10FixedArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22CompilationCacheRegExp3PutENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS2_INS0_10FixedArrayEEE
	.type	_ZN2v88internal22CompilationCacheRegExp3PutENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS2_INS0_10FixedArrayEEE, @function
_ZN2v88internal22CompilationCacheRegExp3PutENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS2_INS0_10FixedArrayEEE:
.LFB18480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movl	%edx, %r10d
	movq	%rcx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rdi), %r12
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	16(%rdi), %rax
	movq	(%rdi), %r15
	movq	(%rax), %r11
	cmpq	%r11, 88(%r15)
	je	.L182
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L177
	movq	%rsi, -56(%rbp)
	movq	%r11, %rsi
	movq	%rcx, -72(%rbp)
	movl	%edx, -64(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %r9
	movl	-64(%rbp), %r10d
	movq	-72(%rbp), %r8
	movq	%rax, %rsi
.L176:
	movq	(%rbx), %rdi
	movq	%r9, %rdx
	movl	%r10d, %ecx
	call	_ZN2v88internal21CompilationCacheTable9PutRegExpEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS4_INS0_10FixedArrayEEE@PLT
	movq	%rax, %r8
	movq	16(%rbx), %rax
	movq	(%r8), %rdx
	movq	%rdx, (%rax)
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L183
	movq	%r13, 41096(%r12)
	addq	$40, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L184
.L178:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r11, (%rsi)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L183:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	movq	%rcx, -72(%rbp)
	movq	%r15, %rdi
	xorl	%ecx, %ecx
	movl	%edx, -64(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -56(%rbp)
	movl	$64, %esi
	call	_ZN2v88internal9HashTableINS0_21CompilationCacheTableENS0_21CompilationCacheShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	-56(%rbp), %r9
	movl	-64(%rbp), %r10d
	movq	(%rax), %rdx
	movq	%rax, %rsi
	movq	16(%rbx), %rax
	movq	-72(%rbp), %r8
	movq	%rdx, (%rax)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L184:
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	movq	%r11, -56(%rbp)
	movq	%rcx, -80(%rbp)
	movl	%edx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %r8
	movl	-72(%rbp), %r10d
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %r11
	movq	%rax, %rsi
	jmp	.L178
	.cfi_endproc
.LFE18480:
	.size	_ZN2v88internal22CompilationCacheRegExp3PutENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS2_INS0_10FixedArrayEEE, .-_ZN2v88internal22CompilationCacheRegExp3PutENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS2_INS0_10FixedArrayEEE
	.section	.text._ZN2v88internal22CompilationCacheRegExp6LookupENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22CompilationCacheRegExp6LookupENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEE
	.type	_ZN2v88internal22CompilationCacheRegExp6LookupENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEE, @function
_ZN2v88internal22CompilationCacheRegExp6LookupENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEE:
.LFB18476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%r12), %rax
	addl	$1, 41104(%r12)
	movl	8(%rbx), %edx
	movq	(%rdi), %rdi
	movq	%rax, -88(%rbp)
	movq	41096(%r12), %rax
	movq	%rax, -96(%rbp)
	testl	%edx, %edx
	jle	.L186
	leaq	-64(%rbp), %rax
	xorl	%r15d, %r15d
	movq	%rax, -80(%rbp)
.L195:
	movq	16(%rbx), %rax
	movl	%r15d, -72(%rbp)
	movq	(%rax,%r15,8), %rsi
	cmpq	%rsi, 88(%rdi)
	je	.L223
	movq	41112(%rdi), %r10
	testq	%r10, %r10
	je	.L189
	movq	%r10, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L188:
	movq	-80(%rbp), %rdi
	movq	%rsi, -64(%rbp)
	movl	%r14d, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal21CompilationCacheTable12LookupRegExpENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEE@PLT
	movq	%rax, %rcx
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L191
.L194:
	movl	%r15d, %eax
	addq	$1, %r15
	addl	$1, %eax
	movl	%eax, -72(%rbp)
	cmpl	%r15d, 8(%rbx)
	jle	.L193
	movq	(%rbx), %rdi
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L189:
	movq	41088(%rdi), %rax
	cmpq	41096(%rdi), %rax
	je	.L224
.L190:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rdi)
	movq	%rsi, (%rax)
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L191:
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	ja	.L194
.L193:
	movq	(%rcx), %rax
	testb	$1, %al
	jne	.L225
.L196:
	movq	(%rbx), %rax
	movq	40960(%rax), %rbx
	cmpb	$0, 6368(%rbx)
	je	.L226
	movq	6360(%rbx), %rax
.L209:
	testq	%rax, %rax
	je	.L210
	addl	$1, (%rax)
.L210:
	subl	$1, 41104(%r12)
	movq	-88(%rbp), %rax
	xorl	%r13d, %r13d
	movq	%rax, 41088(%r12)
	movq	-96(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L208
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L208:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L227
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$64, %esi
	call	_ZN2v88internal9HashTableINS0_21CompilationCacheTableENS0_21CompilationCacheShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	16(%rbx), %rdx
	movq	(%rax), %rcx
	movq	%rcx, (%rdx,%r15,8)
	movq	(%rax), %rsi
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L226:
	movb	$1, 6368(%rbx)
	leaq	6344(%rbx), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 6360(%rbx)
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L225:
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	ja	.L196
	movl	-72(%rbp), %eax
	testl	%eax, %eax
	jne	.L228
.L200:
	movq	(%rbx), %rax
	movq	40960(%rax), %rbx
	cmpb	$0, 6336(%rbx)
	je	.L201
	movq	6328(%rbx), %rax
.L202:
	testq	%rax, %rax
	je	.L203
	addl	$1, (%rax)
.L203:
	movq	-88(%rbp), %rax
	movq	(%rcx), %r14
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	movq	-96(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L204
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L204:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L205
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L224:
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdi
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L201:
	movb	$1, 6336(%rbx)
	leaq	6312(%rbx), %rdi
	movq	%rcx, -72(%rbp)
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	-72(%rbp), %rcx
	movq	%rax, 6328(%rbx)
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L205:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L229
.L207:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, 0(%r13)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L186:
	movl	$0, -72(%rbp)
	leaq	88(%rdi), %rcx
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L228:
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal22CompilationCacheRegExp3PutENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS2_INS0_10FixedArrayEEE
	movq	-72(%rbp), %rcx
	jmp	.L200
.L229:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r13
	jmp	.L207
.L227:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18476:
	.size	_ZN2v88internal22CompilationCacheRegExp6LookupENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEE, .-_ZN2v88internal22CompilationCacheRegExp6LookupENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEE
	.section	.text._ZN2v88internal16CompilationCache6RemoveENS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16CompilationCache6RemoveENS0_6HandleINS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal16CompilationCache6RemoveENS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal16CompilationCache6RemoveENS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB18481:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal22FLAG_compilation_cacheE(%rip)
	jne	.L236
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpb	$0, 136(%rdi)
	movq	%rdi, %rbx
	je	.L230
	movq	%rsi, %r12
	leaq	32(%rdi), %rdi
	call	_ZN2v88internal19CompilationSubCache6RemoveENS0_6HandleINS0_18SharedFunctionInfoEEE
	leaq	56(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal19CompilationSubCache6RemoveENS0_6HandleINS0_18SharedFunctionInfoEEE
	leaq	8(%rbx), %rdi
	movq	%r12, %rsi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal19CompilationSubCache6RemoveENS0_6HandleINS0_18SharedFunctionInfoEEE
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18481:
	.size	_ZN2v88internal16CompilationCache6RemoveENS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZN2v88internal16CompilationCache6RemoveENS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.text._ZN2v88internal16CompilationCache12LookupScriptENS0_6HandleINS0_6StringEEENS0_11MaybeHandleINS0_6ObjectEEEiiNS_19ScriptOriginOptionsENS2_INS0_7ContextEEENS0_12LanguageModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16CompilationCache12LookupScriptENS0_6HandleINS0_6StringEEENS0_11MaybeHandleINS0_6ObjectEEEiiNS_19ScriptOriginOptionsENS2_INS0_7ContextEEENS0_12LanguageModeE
	.type	_ZN2v88internal16CompilationCache12LookupScriptENS0_6HandleINS0_6StringEEENS0_11MaybeHandleINS0_6ObjectEEEiiNS_19ScriptOriginOptionsENS2_INS0_7ContextEEENS0_12LanguageModeE, @function
_ZN2v88internal16CompilationCache12LookupScriptENS0_6HandleINS0_6StringEEENS0_11MaybeHandleINS0_6ObjectEEEiiNS_19ScriptOriginOptionsENS2_INS0_7ContextEEENS0_12LanguageModeE:
.LFB18482:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpb	$0, _ZN2v88internal22FLAG_compilation_cacheE(%rip)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	24(%rbp), %eax
	je	.L238
	cmpb	$0, 136(%rdi)
	je	.L238
	movb	%al, 24(%rbp)
	addq	$8, %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal22CompilationCacheScript6LookupENS0_6HandleINS0_6StringEEENS0_11MaybeHandleINS0_6ObjectEEEiiNS_19ScriptOriginOptionsENS2_INS0_7ContextEEENS0_12LanguageModeE
	.p2align 4,,10
	.p2align 3
.L238:
	.cfi_restore_state
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18482:
	.size	_ZN2v88internal16CompilationCache12LookupScriptENS0_6HandleINS0_6StringEEENS0_11MaybeHandleINS0_6ObjectEEEiiNS_19ScriptOriginOptionsENS2_INS0_7ContextEEENS0_12LanguageModeE, .-_ZN2v88internal16CompilationCache12LookupScriptENS0_6HandleINS0_6StringEEENS0_11MaybeHandleINS0_6ObjectEEEiiNS_19ScriptOriginOptionsENS2_INS0_7ContextEEENS0_12LanguageModeE
	.section	.rodata._ZN2v88internal16CompilationCache10LookupEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeEi.str1.1,"aMS",@progbits,1
.LC3:
	.string	"eval-global"
.LC4:
	.string	"eval-contextual"
	.section	.text._ZN2v88internal16CompilationCache10LookupEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16CompilationCache10LookupEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeEi
	.type	_ZN2v88internal16CompilationCache10LookupEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeEi, @function
_ZN2v88internal16CompilationCache10LookupEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeEi:
.LFB18483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal22FLAG_compilation_cacheE(%rip)
	movq	$0, (%rdi)
	movb	$0, 8(%rdi)
	movups	%xmm0, 16(%rdi)
	je	.L240
	cmpb	$0, 136(%rsi)
	movq	%rsi, %rbx
	jne	.L257
.L240:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L258
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_restore_state
	movq	(%r8), %rax
	movq	-1(%rax), %rsi
	cmpw	$145, 11(%rsi)
	je	.L259
	movq	(%rbx), %r13
	movq	39(%rax), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L246
	movl	%r9d, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movl	-88(%rbp), %r9d
	movq	%rax, %r8
.L247:
	movl	16(%rbp), %eax
	subq	$8, %rsp
	leaq	56(%rbx), %rsi
	movq	%r12, %rdi
	leaq	.LC4(%rip), %r14
	pushq	%rax
	call	_ZN2v88internal20CompilationCacheEval6LookupENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeEi
	popq	%rax
	popq	%rdx
.L245:
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L240
	cmpb	$0, 8(%r12)
	je	.L240
	movq	(%rbx), %rax
	movq	41016(%rax), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L240
	movq	%r13, %rcx
	movq	%r14, %rdx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger21CompilationCacheEventEPKcS3_NS0_18SharedFunctionInfoE@PLT
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L259:
	movl	16(%rbp), %eax
	subq	$8, %rsp
	leaq	32(%rbx), %rsi
	leaq	.LC3(%rip), %r14
	pushq	%rax
	call	_ZN2v88internal20CompilationCacheEval6LookupENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeEi
	popq	%rcx
	popq	%rsi
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L246:
	movq	41088(%r13), %r8
	cmpq	41096(%r13), %r8
	je	.L260
.L248:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r8)
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L260:
	movq	%r13, %rdi
	movl	%r9d, -92(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-92(%rbp), %r9d
	movq	-88(%rbp), %rcx
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L248
.L258:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18483:
	.size	_ZN2v88internal16CompilationCache10LookupEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeEi, .-_ZN2v88internal16CompilationCache10LookupEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeEi
	.section	.text._ZN2v88internal16CompilationCache12LookupRegExpENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16CompilationCache12LookupRegExpENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEE
	.type	_ZN2v88internal16CompilationCache12LookupRegExpENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEE, @function
_ZN2v88internal16CompilationCache12LookupRegExpENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEE:
.LFB18484:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal22FLAG_compilation_cacheE(%rip)
	je	.L262
	cmpb	$0, 136(%rdi)
	je	.L262
	addq	$80, %rdi
	jmp	_ZN2v88internal22CompilationCacheRegExp6LookupENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEE
	.p2align 4,,10
	.p2align 3
.L262:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE18484:
	.size	_ZN2v88internal16CompilationCache12LookupRegExpENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEE, .-_ZN2v88internal16CompilationCache12LookupRegExpENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEE
	.section	.rodata._ZN2v88internal16CompilationCache9PutScriptENS0_6HandleINS0_6StringEEENS2_INS0_7ContextEEENS0_12LanguageModeENS2_INS0_18SharedFunctionInfoEEE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"put"
	.section	.text._ZN2v88internal16CompilationCache9PutScriptENS0_6HandleINS0_6StringEEENS2_INS0_7ContextEEENS0_12LanguageModeENS2_INS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16CompilationCache9PutScriptENS0_6HandleINS0_6StringEEENS2_INS0_7ContextEEENS0_12LanguageModeENS2_INS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal16CompilationCache9PutScriptENS0_6HandleINS0_6StringEEENS2_INS0_7ContextEEENS0_12LanguageModeENS2_INS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal16CompilationCache9PutScriptENS0_6HandleINS0_6StringEEENS2_INS0_7ContextEEENS0_12LanguageModeENS2_INS0_18SharedFunctionInfoEEE:
.LFB18485:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpb	$0, _ZN2v88internal22FLAG_compilation_cacheE(%rip)
	movq	%rdx, -56(%rbp)
	je	.L263
	cmpb	$0, 136(%rdi)
	movq	%rdi, %rbx
	je	.L263
	movq	(%rdi), %rax
	movq	%rsi, %r13
	movl	%ecx, %r15d
	movq	%r8, %r12
	movq	41016(%rax), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movq	-56(%rbp), %r9
	testb	%al, %al
	jne	.L271
.L265:
	movq	8(%rbx), %r14
	leaq	8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r9, -72(%rbp)
	addl	$1, 41104(%r14)
	movq	41088(%r14), %r11
	movq	41096(%r14), %r10
	movq	%r11, -64(%rbp)
	movq	%r10, -56(%rbp)
	call	_ZN2v88internal19CompilationSubCache8GetTableEi
	movq	-72(%rbp), %r9
	movq	%r12, %r8
	movl	%r15d, %ecx
	movq	%rax, %rdi
	movq	%r13, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal21CompilationCacheTable9PutScriptENS0_6HandleIS1_EENS2_INS0_6StringEEENS2_INS0_7ContextEEENS0_12LanguageModeENS2_INS0_18SharedFunctionInfoEEE@PLT
	movq	-64(%rbp), %r11
	movq	-56(%rbp), %r10
	movq	%rax, %r8
	movq	24(%rbx), %rax
	movq	(%r8), %rdx
	movq	%rdx, (%rax)
	movq	%r11, 41088(%r14)
	subl	$1, 41104(%r14)
	cmpq	41096(%r14), %r10
	je	.L263
	movq	%r10, 41096(%r14)
	addq	$40, %rsp
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L263:
	.cfi_restore_state
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	.cfi_restore_state
	movq	(%r12), %rcx
	leaq	.LC1(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6Logger21CompilationCacheEventEPKcS3_NS0_18SharedFunctionInfoE@PLT
	movq	-56(%rbp), %r9
	jmp	.L265
	.cfi_endproc
.LFE18485:
	.size	_ZN2v88internal16CompilationCache9PutScriptENS0_6HandleINS0_6StringEEENS2_INS0_7ContextEEENS0_12LanguageModeENS2_INS0_18SharedFunctionInfoEEE, .-_ZN2v88internal16CompilationCache9PutScriptENS0_6HandleINS0_6StringEEENS2_INS0_7ContextEEENS0_12LanguageModeENS2_INS0_18SharedFunctionInfoEEE
	.section	.text._ZN2v88internal16CompilationCache7PutEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEES6_NS2_INS0_12FeedbackCellEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16CompilationCache7PutEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEES6_NS2_INS0_12FeedbackCellEEEi
	.type	_ZN2v88internal16CompilationCache7PutEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEES6_NS2_INS0_12FeedbackCellEEEi, @function
_ZN2v88internal16CompilationCache7PutEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEES6_NS2_INS0_12FeedbackCellEEEi:
.LFB18486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpb	$0, _ZN2v88internal22FLAG_compilation_cacheE(%rip)
	movl	16(%rbp), %r11d
	je	.L272
	cmpb	$0, 136(%rdi)
	movq	%rdi, %rbx
	je	.L272
	movq	(%rdi), %r15
	movq	%rsi, %r13
	movq	%r8, %r12
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	movq	41096(%r15), %r14
	movq	%rax, -56(%rbp)
	movq	(%rcx), %rax
	movq	-1(%rax), %rsi
	cmpw	$145, 11(%rsi)
	je	.L287
	movq	(%rdi), %rcx
	movq	39(%rax), %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L277
	movl	%r11d, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r9
	movl	-80(%rbp), %r11d
	movq	%rax, %r8
.L278:
	subq	$8, %rsp
	movq	%r13, %rsi
	leaq	56(%rbx), %rdi
	movq	%r12, %rcx
	pushq	%r11
	leaq	.LC4(%rip), %r13
	call	_ZN2v88internal20CompilationCacheEval3PutENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEES6_NS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEEi
	popq	%rax
	popq	%rdx
.L276:
	movq	(%rbx), %rax
	movq	41016(%rax), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L288
.L280:
	movq	-56(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	cmpq	41096(%r15), %r14
	je	.L272
	movq	%r14, 41096(%r15)
	leaq	-40(%rbp), %rsp
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L272:
	.cfi_restore_state
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L287:
	.cfi_restore_state
	subq	$8, %rsp
	movq	%r13, %rsi
	leaq	32(%rdi), %rdi
	movq	%rcx, %r8
	pushq	%r11
	movq	%r12, %rcx
	leaq	.LC3(%rip), %r13
	call	_ZN2v88internal20CompilationCacheEval3PutENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEES6_NS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEEi
	popq	%rcx
	popq	%rsi
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L288:
	movq	(%r12), %rcx
	movq	%r13, %rdx
	leaq	.LC5(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Logger21CompilationCacheEventEPKcS3_NS0_18SharedFunctionInfoE@PLT
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L277:
	movq	41088(%rcx), %r8
	cmpq	41096(%rcx), %r8
	je	.L289
.L279:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%r8)
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L289:
	movq	%rcx, %rdi
	movl	%r11d, -92(%rbp)
	movq	%r9, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-92(%rbp), %r11d
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	%rax, %r8
	movq	-64(%rbp), %rcx
	jmp	.L279
	.cfi_endproc
.LFE18486:
	.size	_ZN2v88internal16CompilationCache7PutEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEES6_NS2_INS0_12FeedbackCellEEEi, .-_ZN2v88internal16CompilationCache7PutEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEES6_NS2_INS0_12FeedbackCellEEEi
	.section	.text._ZN2v88internal16CompilationCache9PutRegExpENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS2_INS0_10FixedArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16CompilationCache9PutRegExpENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS2_INS0_10FixedArrayEEE
	.type	_ZN2v88internal16CompilationCache9PutRegExpENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS2_INS0_10FixedArrayEEE, @function
_ZN2v88internal16CompilationCache9PutRegExpENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS2_INS0_10FixedArrayEEE:
.LFB18487:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal22FLAG_compilation_cacheE(%rip)
	jne	.L302
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	cmpb	$0, 136(%rdi)
	je	.L290
	movq	80(%rdi), %r15
	movq	%rsi, %r12
	movl	%edx, %r10d
	movq	%rcx, %r13
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	movq	80(%rdi), %rdi
	movq	41096(%r15), %r14
	movq	%rax, -56(%rbp)
	movq	96(%rbx), %rax
	movq	(%rax), %r8
	cmpq	%r8, 88(%rdi)
	je	.L303
	movq	41112(%rdi), %r11
	testq	%r11, %r11
	je	.L294
	movq	%r8, %rsi
	movq	%r11, %rdi
	movl	%edx, -64(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-64(%rbp), %r10d
	movq	%rax, %rsi
.L293:
	movq	80(%rbx), %rdi
	movq	%r13, %r8
	movq	%r12, %rdx
	movl	%r10d, %ecx
	call	_ZN2v88internal21CompilationCacheTable9PutRegExpEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS4_INS0_10FixedArrayEEE@PLT
	movq	%rax, %r8
	movq	96(%rbx), %rax
	movq	(%r8), %rdx
	movq	%rdx, (%rax)
	movq	-56(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	cmpq	41096(%r15), %r14
	je	.L290
	movq	%r14, 41096(%r15)
	addq	$40, %rsp
	movq	%r15, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L290:
	.cfi_restore_state
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L303:
	.cfi_restore_state
	movl	%edx, -64(%rbp)
	movl	$64, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	call	_ZN2v88internal9HashTableINS0_21CompilationCacheTableENS0_21CompilationCacheShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movl	-64(%rbp), %r10d
	movq	(%rax), %rdx
	movq	%rax, %rsi
	movq	96(%rbx), %rax
	movq	%rdx, (%rax)
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L294:
	movq	41088(%rdi), %rsi
	cmpq	41096(%rdi), %rsi
	je	.L304
.L295:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdi)
	movq	%r8, (%rsi)
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L304:
	movq	%r8, -72(%rbp)
	movq	%rdi, -64(%rbp)
	movl	%edx, -76(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-76(%rbp), %r10d
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rdi
	movq	%rax, %rsi
	jmp	.L295
	.cfi_endproc
.LFE18487:
	.size	_ZN2v88internal16CompilationCache9PutRegExpENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS2_INS0_10FixedArrayEEE, .-_ZN2v88internal16CompilationCache9PutRegExpENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS2_INS0_10FixedArrayEEE
	.section	.text._ZN2v88internal16CompilationCache5ClearEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16CompilationCache5ClearEv
	.type	_ZN2v88internal16CompilationCache5ClearEv, @function
_ZN2v88internal16CompilationCache5ClearEv:
.LFB18488:
	.cfi_startproc
	endbr64
	movq	104(%rdi), %rsi
	movq	%rdi, %rdx
	movq	(%rsi), %rax
	movslq	8(%rsi), %rcx
	movq	16(%rsi), %rdi
	movq	88(%rax), %rax
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	movq	112(%rdx), %rsi
	movq	(%rsi), %rax
	movslq	8(%rsi), %rcx
	movq	16(%rsi), %rdi
	movq	88(%rax), %rax
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	movq	120(%rdx), %rsi
	movq	(%rsi), %rax
	movslq	8(%rsi), %rcx
	movq	16(%rsi), %rdi
	movq	88(%rax), %rax
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	movq	128(%rdx), %rdx
	movq	(%rdx), %rax
	movslq	8(%rdx), %rcx
	movq	16(%rdx), %rdi
	movq	88(%rax), %rax
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	ret
	.cfi_endproc
.LFE18488:
	.size	_ZN2v88internal16CompilationCache5ClearEv, .-_ZN2v88internal16CompilationCache5ClearEv
	.section	.text._ZN2v88internal16CompilationCache7IterateEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16CompilationCache7IterateEPNS0_11RootVisitorE
	.type	_ZN2v88internal16CompilationCache7IterateEPNS0_11RootVisitorE, @function
_ZN2v88internal16CompilationCache7IterateEPNS0_11RootVisitorE:
.LFB18489:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	136(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	104(%rdi), %rbx
	subq	$8, %rsp
.L307:
	movq	(%rbx), %rax
	movq	(%r12), %r9
	addq	$8, %rbx
	xorl	%edx, %edx
	movl	$9, %esi
	movq	%r12, %rdi
	movq	16(%rax), %rcx
	movslq	8(%rax), %rax
	leaq	(%rcx,%rax,8), %r8
	call	*16(%r9)
	cmpq	%rbx, %r13
	jne	.L307
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18489:
	.size	_ZN2v88internal16CompilationCache7IterateEPNS0_11RootVisitorE, .-_ZN2v88internal16CompilationCache7IterateEPNS0_11RootVisitorE
	.section	.text._ZN2v88internal16CompilationCache19MarkCompactPrologueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16CompilationCache19MarkCompactPrologueEv
	.type	_ZN2v88internal16CompilationCache19MarkCompactPrologueEv, @function
_ZN2v88internal16CompilationCache19MarkCompactPrologueEv:
.LFB18490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	leaq	136(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	104(%rdi), %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.L317:
	movq	(%rbx), %rdx
	movl	8(%rdx), %ecx
	movq	16(%rdx), %rsi
	cmpl	$1, %ecx
	je	.L321
	leal	-1(%rcx), %eax
	testl	%eax, %eax
	jle	.L313
	cltq
	movq	-8(%rsi,%rax,8), %rdi
	movq	%rdi, (%rsi,%rax,8)
	cmpl	$2, %ecx
	je	.L315
	leal	-2(%rcx), %eax
	movslq	%ecx, %rsi
	subl	$3, %ecx
	cltq
	subq	%rcx, %rsi
	salq	$3, %rax
	leaq	-24(,%rsi,8), %rdi
	.p2align 4,,10
	.p2align 3
.L316:
	movq	16(%rdx), %rcx
	movq	-8(%rcx,%rax), %rsi
	movq	%rsi, (%rcx,%rax)
	subq	$8, %rax
	cmpq	%rax, %rdi
	jne	.L316
.L315:
	movq	16(%rdx), %rsi
.L313:
	movq	(%rdx), %rax
	movq	88(%rax), %rax
	movq	%rax, (%rsi)
.L312:
	addq	$8, %rbx
	cmpq	%r12, %rbx
	jne	.L317
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L322
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L321:
	.cfi_restore_state
	movq	(%rdx), %rdx
	movq	(%rsi), %rax
	cmpq	%rax, 88(%rdx)
	je	.L312
	movq	%r13, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal21CompilationCacheTable3AgeEv@PLT
	jmp	.L312
.L322:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18490:
	.size	_ZN2v88internal16CompilationCache19MarkCompactPrologueEv, .-_ZN2v88internal16CompilationCache19MarkCompactPrologueEv
	.section	.text._ZN2v88internal16CompilationCache6EnableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16CompilationCache6EnableEv
	.type	_ZN2v88internal16CompilationCache6EnableEv, @function
_ZN2v88internal16CompilationCache6EnableEv:
.LFB18491:
	.cfi_startproc
	endbr64
	movb	$1, 136(%rdi)
	ret
	.cfi_endproc
.LFE18491:
	.size	_ZN2v88internal16CompilationCache6EnableEv, .-_ZN2v88internal16CompilationCache6EnableEv
	.section	.text._ZN2v88internal16CompilationCache7DisableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16CompilationCache7DisableEv
	.type	_ZN2v88internal16CompilationCache7DisableEv, @function
_ZN2v88internal16CompilationCache7DisableEv:
.LFB18492:
	.cfi_startproc
	endbr64
	movq	104(%rdi), %rsi
	movb	$0, 136(%rdi)
	movq	%rdi, %rdx
	movq	(%rsi), %rax
	movslq	8(%rsi), %rcx
	movq	16(%rsi), %rdi
	movq	88(%rax), %rax
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	movq	112(%rdx), %rsi
	movq	(%rsi), %rax
	movslq	8(%rsi), %rcx
	movq	16(%rsi), %rdi
	movq	88(%rax), %rax
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	movq	120(%rdx), %rsi
	movq	(%rsi), %rax
	movslq	8(%rsi), %rcx
	movq	16(%rsi), %rdi
	movq	88(%rax), %rax
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	movq	128(%rdx), %rdx
	movq	(%rdx), %rax
	movslq	8(%rdx), %rcx
	movq	16(%rdx), %rdi
	movq	88(%rax), %rax
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	ret
	.cfi_endproc
.LFE18492:
	.size	_ZN2v88internal16CompilationCache7DisableEv, .-_ZN2v88internal16CompilationCache7DisableEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal16CompilationCacheC2EPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal16CompilationCacheC2EPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal16CompilationCacheC2EPNS0_7IsolateE:
.LFB22394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22394:
	.size	_GLOBAL__sub_I__ZN2v88internal16CompilationCacheC2EPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal16CompilationCacheC2EPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal16CompilationCacheC2EPNS0_7IsolateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
