	.file	"json-stringifier.cc"
	.text
	.section	.rodata._ZN2v88internal10FieldIndex13ForDescriptorEPNS0_7IsolateENS0_3MapEi.isra.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal10FieldIndex13ForDescriptorEPNS0_7IsolateENS0_3MapEi.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal10FieldIndex13ForDescriptorEPNS0_7IsolateENS0_3MapEi.isra.0, @function
_ZN2v88internal10FieldIndex13ForDescriptorEPNS0_7IsolateENS0_3MapEi.isra.0:
.LFB22027:
	.cfi_startproc
	leal	3(%rsi,%rsi,2), %eax
	movq	39(%rdi), %rdx
	sall	$3, %eax
	cltq
	movq	7(%rax,%rdx), %rsi
	movzbl	7(%rdi), %ecx
	movzbl	8(%rdi), %edx
	movq	%rsi, %rax
	shrq	$38, %rsi
	shrq	$51, %rax
	subl	%edx, %ecx
	andl	$7, %esi
	movq	%rax, %r8
	andl	$1023, %r8d
	cmpl	%ecx, %r8d
	setl	%dl
	jl	.L12
	subl	%ecx, %r8d
	movl	$16, %r9d
	leal	16(,%r8,8), %r8d
.L3:
	cmpl	$2, %esi
	je	.L7
	cmpb	$2, %sil
	jg	.L5
	je	.L6
.L8:
	xorl	%esi, %esi
.L4:
	movzbl	%dl, %eax
	movslq	%ecx, %rcx
	movslq	%r8d, %r8
	movslq	%r9d, %r9
	salq	$14, %rax
	salq	$17, %rcx
	orq	%rcx, %rax
	salq	$27, %r9
	orq	%r8, %rax
	orq	%r9, %rax
	orq	%rsi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	subl	$3, %esi
	cmpb	$1, %sil
	jbe	.L8
.L6:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movl	$32768, %esi
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L12:
	movzbl	8(%rdi), %r9d
	movzbl	8(%rdi), %eax
	addl	%eax, %r8d
	sall	$3, %r9d
	sall	$3, %r8d
	jmp	.L3
	.cfi_endproc
.LFE22027:
	.size	_ZN2v88internal10FieldIndex13ForDescriptorEPNS0_7IsolateENS0_3MapEi.isra.0, .-_ZN2v88internal10FieldIndex13ForDescriptorEPNS0_7IsolateENS0_3MapEi.isra.0
	.section	.text._ZN2v88internal16SeqTwoByteString19SeqTwoByteStringSetEit,"axG",@progbits,_ZN2v88internal16SeqTwoByteString19SeqTwoByteStringSetEit,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16SeqTwoByteString19SeqTwoByteStringSetEit
	.type	_ZN2v88internal16SeqTwoByteString19SeqTwoByteStringSetEit, @function
_ZN2v88internal16SeqTwoByteString19SeqTwoByteStringSetEit:
.LFB16233:
	.cfi_startproc
	endbr64
	movl	%edx, %r8d
	leal	16(%rsi,%rsi), %eax
	movq	(%rdi), %rdx
	cltq
	movw	%r8w, -1(%rax,%rdx)
	ret
	.cfi_endproc
.LFE16233:
	.size	_ZN2v88internal16SeqTwoByteString19SeqTwoByteStringSetEit, .-_ZN2v88internal16SeqTwoByteString19SeqTwoByteStringSetEit
	.section	.text._ZN2v88internal6Object20GetPropertyOrElementEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE,"axG",@progbits,_ZN2v88internal6Object20GetPropertyOrElementEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6Object20GetPropertyOrElementEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE
	.type	_ZN2v88internal6Object20GetPropertyOrElementEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE, @function
_ZN2v88internal6Object20GetPropertyOrElementEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE:
.LFB17747:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L41
.L16:
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L23
.L25:
	movl	$-1, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %r14
.L24:
	movq	(%r12), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L26
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L26:
	movl	%eax, -224(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -212(%rbp)
	movq	(%r12), %rax
	movq	%r13, -200(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L42
.L27:
	leaq	-224(%rbp), %r12
	movq	%r15, -192(%rbp)
	movq	%r12, %rdi
	movq	$0, -184(%rbp)
	movq	%rbx, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%r14, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L22:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L43
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movq	%rax, -144(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L17
	testb	$2, %al
	jne	.L16
.L17:
	leaq	-144(%rbp), %r8
	leaq	-228(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r8, -248(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	testb	%al, %al
	je	.L16
	movq	(%rbx), %rax
	movl	-228(%rbp), %r14d
	movq	-248(%rbp), %r8
	testb	$1, %al
	jne	.L19
.L21:
	movl	%r14d, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r8, -248(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-248(%rbp), %r8
.L20:
	movabsq	$824633720832, %rcx
	movq	%r8, %rdi
	movq	%r13, -120(%rbp)
	movl	$3, -144(%rbp)
	movq	%rcx, -132(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	%r14d, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r12, -112(%rbp)
	movdqa	-128(%rbp), %xmm1
	leaq	-224(%rbp), %r12
	movdqa	-144(%rbp), %xmm0
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L23:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L25
	movq	%rbx, %r14
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L19:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L21
	movq	%rbx, %rax
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r15
	jmp	.L27
.L43:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17747:
	.size	_ZN2v88internal6Object20GetPropertyOrElementEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE, .-_ZN2v88internal6Object20GetPropertyOrElementEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE
	.section	.text._ZN2v88internal15JsonStringifierC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JsonStringifierC2EPNS0_7IsolateE
	.type	_ZN2v88internal15JsonStringifierC2EPNS0_7IsolateE, @function
_ZN2v88internal15JsonStringifierC2EPNS0_7IsolateE:
.LFB17964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	movq	%rsi, -8(%rdi)
	call	_ZN2v88internal24IncrementalStringBuilderC1EPNS0_7IsolateE@PLT
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 56(%rbx)
	movq	$0, 64(%rbx)
	addq	$3448, %rax
	movq	$0, 72(%rbx)
	movl	$0, 80(%rbx)
	movq	$0, 104(%rbx)
	movq	%rax, 48(%rbx)
	movups	%xmm0, 88(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17964:
	.size	_ZN2v88internal15JsonStringifierC2EPNS0_7IsolateE, .-_ZN2v88internal15JsonStringifierC2EPNS0_7IsolateE
	.globl	_ZN2v88internal15JsonStringifierC1EPNS0_7IsolateE
	.set	_ZN2v88internal15JsonStringifierC1EPNS0_7IsolateE,_ZN2v88internal15JsonStringifierC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal15JsonStringifier18InitializeReplacerENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JsonStringifier18InitializeReplacerENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal15JsonStringifier18InitializeReplacerENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal15JsonStringifier18InitializeReplacerENS0_6HandleINS0_6ObjectEEE:
.LFB17967:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L139
.L136:
	movl	$1, %eax
.L46:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L140
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	movq	%rdi, %r12
	movq	%rsi, %rbx
	cmpw	$1061, 11(%rdx)
	je	.L54
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	je	.L51
	xorl	%edx, %edx
.L52:
	testb	%dl, %dl
	jne	.L136
	movq	-1(%rax), %rax
	testb	$2, 13(%rax)
	je	.L136
	movq	%rbx, 64(%r12)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L51:
	movq	%rsi, %rdi
	call	_ZN2v88internal7JSProxy7IsArrayENS0_6HandleIS1_EE@PLT
	movl	%eax, %edx
	testb	%al, %al
	je	.L46
	shrw	$8, %dx
	je	.L141
.L54:
	movq	(%r12), %r13
	addl	$1, 41104(%r13)
	movq	41088(%r13), %rax
	movq	(%r12), %rdi
	movq	%rax, -176(%rbp)
	movq	41096(%r13), %rax
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal7Factory17NewOrderedHashSetEv@PLT
	movq	(%r12), %rdi
	movq	%rbx, %rsi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal6Object22GetLengthFromArrayLikeEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE@PLT
	testq	%rax, %rax
	je	.L56
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L57
	sarq	$32, %rax
	movl	%eax, -156(%rbp)
	js	.L107
.L60:
	movl	-156(%rbp), %eax
	testl	%eax, %eax
	je	.L95
.L59:
	movq	%r13, -184(%rbp)
	xorl	%r14d, %r14d
	leaq	-144(%rbp), %r15
.L96:
	movq	(%rbx), %rax
	movq	(%r12), %r13
	testb	$1, %al
	jne	.L68
.L70:
	movl	%r14d, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
.L69:
	movabsq	$824633720832, %rcx
	movq	%r15, %rdi
	movq	%r13, -120(%rbp)
	movl	$3, -144(%rbp)
	movq	%rcx, -132(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	%r14d, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L71
	movq	-120(%rbp), %rax
	leaq	88(%rax), %rdx
.L72:
	movq	(%rdx), %rax
	testb	$1, %al
	je	.L74
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	je	.L135
	movq	-1(%rax), %rcx
	cmpw	$63, 11(%rcx)
	jbe	.L135
	movq	-1(%rax), %rcx
	cmpw	$1041, 11(%rcx)
	jne	.L94
	movq	(%r12), %rcx
	movq	23(%rax), %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L86
	movq	%rdx, -192(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-192(%rbp), %rdx
	movq	(%rax), %rsi
.L87:
	testb	$1, %sil
	je	.L89
	movq	-1(%rsi), %rcx
	cmpw	$65, 11(%rcx)
	je	.L89
	movq	(%rax), %rax
	testb	$1, %al
	je	.L94
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L94
.L89:
	movq	(%rdx), %rax
	movq	(%r12), %rdi
	testb	$1, %al
	jne	.L92
	.p2align 4,,10
	.p2align 3
.L93:
	movq	%rdx, %rsi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L133
.L82:
	movq	(%rdx), %rax
	movq	(%r12), %rdi
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L142
.L104:
	movq	-152(%rbp), %rsi
	movq	(%r12), %rdi
	call	_ZN2v88internal14OrderedHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L133
	movq	%rax, -152(%rbp)
.L94:
	addl	$1, %r14d
	cmpl	-156(%rbp), %r14d
	jb	.L96
	movq	-184(%rbp), %r13
.L95:
	movq	-152(%rbp), %rsi
	movq	(%r12), %rdi
	movl	$1, %edx
	call	_ZN2v88internal14OrderedHashSet18ConvertToKeysArrayEPNS0_7IsolateENS0_6HandleIS1_EENS0_17GetKeysConversionE@PLT
	movq	%rax, 56(%r12)
	movq	(%rax), %r14
	movq	-176(%rbp), %rax
	subl	$1, 41104(%r13)
	movq	%rax, 41088(%r13)
	movq	-168(%rbp), %rax
	cmpq	41096(%r13), %rax
	je	.L67
	movq	%rax, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L67:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L97
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L98:
	movq	41088(%r13), %rcx
	addl	$1, 41104(%r13)
	movq	41096(%r13), %rdx
	movq	%rax, 56(%r12)
	movq	%rcx, 41088(%r13)
	subl	$1, 41104(%r13)
	cmpq	41096(%r13), %rdx
	je	.L136
	movq	%rdx, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L72
.L133:
	movq	-184(%rbp), %r13
.L56:
	movq	-176(%rbp), %rax
	subl	$1, 41104(%r13)
	movq	%rax, 41088(%r13)
	movq	-168(%rbp), %rax
	cmpq	41096(%r13), %rax
	je	.L100
	movq	%rax, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L100:
	xorl	%eax, %eax
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L135:
	xorl	%ecx, %ecx
	movq	(%r12), %rdi
	testb	%cl, %cl
	jne	.L93
.L92:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L93
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L74:
	movq	(%r12), %rdi
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L68:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L70
	movq	%rbx, %rax
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L57:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L61
.L107:
	movl	$-1, -156(%rbp)
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L142:
	movq	%rdx, %rsi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rdx
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L61:
	movsd	7(%rax), %xmm1
	movsd	.LC1(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rdx
	movq	%xmm2, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L107
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm2, -156(%rbp)
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L107
	je	.L60
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L97:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L143
.L99:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%r14, (%rax)
	jmp	.L98
.L86:
	movq	41088(%rcx), %rax
	cmpq	41096(%rcx), %rax
	je	.L144
.L88:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rcx)
	movq	%rsi, (%rax)
	jmp	.L87
.L143:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L99
.L144:
	movq	%rcx, %rdi
	movq	%rdx, -208(%rbp)
	movq	%rsi, -200(%rbp)
	movq	%rcx, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-208(%rbp), %rdx
	movq	-200(%rbp), %rsi
	movq	-192(%rbp), %rcx
	jmp	.L88
.L140:
	call	__stack_chk_fail@PLT
.L141:
	movq	(%rbx), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L52
	.cfi_endproc
.LFE17967:
	.size	_ZN2v88internal15JsonStringifier18InitializeReplacerENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal15JsonStringifier18InitializeReplacerENS0_6HandleINS0_6ObjectEEE
	.section	.rodata._ZN2v88internal15JsonStringifier13InitializeGapENS0_6HandleINS0_6ObjectEEE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"NewArray"
	.section	.text._ZN2v88internal15JsonStringifier13InitializeGapENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JsonStringifier13InitializeGapENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal15JsonStringifier13InitializeGapENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal15JsonStringifier13InitializeGapENS0_6HandleINS0_6ObjectEEE:
.LFB17968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movq	41088(%r12), %r15
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	(%rsi), %rdx
	movq	%rdx, %rcx
	notq	%rcx
	movl	%ecx, %ebx
	andl	$1, %ebx
	je	.L264
.L188:
	sarq	$32, %rdx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
.L174:
	movsd	.LC4(%rip), %xmm2
	movapd	%xmm0, %xmm1
	andpd	.LC3(%rip), %xmm1
	ucomisd	%xmm1, %xmm2
	jnb	.L265
.L175:
	movabsq	$9218868437227405312, %rdx
	movq	%xmm0, %rax
	testq	%rdx, %rax
	je	.L168
	movq	%xmm0, %rsi
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %ecx
	subl	$1075, %ecx
	js	.L266
	cmpl	$31, %ecx
	jg	.L168
	movabsq	$4503599627370495, %rbx
	movabsq	$4503599627370496, %rdx
	andq	%rax, %rbx
	addq	%rdx, %rbx
	salq	%cl, %rbx
	movl	%ebx, %edx
.L181:
	sarq	$63, %rax
	movq	%rax, %rbx
	orl	$1, %ebx
	imull	%edx, %ebx
.L178:
	testl	%ebx, %ebx
	jle	.L168
	cmpl	$10, %ebx
	movl	$10, %edx
	leaq	_ZSt7nothrow(%rip), %rsi
	cmovle	%ebx, %edx
	addl	$1, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movq	-56(%rbp), %rdx
	testq	%rax, %rax
	je	.L267
.L183:
	movl	$32, %r8d
	movq	%rax, 72(%r13)
	movw	%r8w, (%rax)
	cmpl	$1, %ebx
	je	.L185
	movl	$32, %edi
	movw	%di, 2(%rax)
	cmpl	$2, %ebx
	je	.L185
	movl	$32, %esi
	movw	%si, 4(%rax)
	cmpl	$3, %ebx
	je	.L185
	movq	72(%r13), %rax
	movl	$32, %ecx
	movw	%cx, 6(%rax)
	cmpl	$4, %ebx
	je	.L185
	movq	72(%r13), %rax
	movl	$32, %r11d
	movw	%r11w, 8(%rax)
	cmpl	$5, %ebx
	je	.L185
	movq	72(%r13), %rax
	movl	$32, %r10d
	movw	%r10w, 10(%rax)
	cmpl	$6, %ebx
	je	.L185
	movq	72(%r13), %rax
	movl	$32, %r9d
	movw	%r9w, 12(%rax)
	cmpl	$7, %ebx
	je	.L185
	movq	72(%r13), %rax
	movl	$32, %r8d
	movw	%r8w, 14(%rax)
	cmpl	$8, %ebx
	je	.L185
	movq	72(%r13), %rax
	movl	$32, %edi
	movw	%di, 16(%rax)
	cmpl	$9, %ebx
	je	.L185
	movq	72(%r13), %rax
	movl	$32, %esi
	movw	%si, 18(%rax)
.L185:
	movq	72(%r13), %rax
	xorl	%ecx, %ecx
	movw	%cx, -2(%rax,%rdx)
.L168:
	movl	$1, %ebx
.L172:
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L145
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L145:
	addq	$40, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	.cfi_restore_state
	movsd	.LC5(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L175
	comisd	.LC6(%rip), %xmm0
	jb	.L175
	cvttsd2sil	%xmm0, %ebx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L175
	je	.L178
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L266:
	cmpl	$-52, %ecx
	jl	.L168
	movabsq	$4503599627370495, %rbx
	movl	$1075, %ecx
	movabsq	$4503599627370496, %rdx
	andq	%rax, %rbx
	subl	%esi, %ecx
	addq	%rdx, %rbx
	movq	%rbx, %rdx
	shrq	%cl, %rdx
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L264:
	movq	-1(%rdx), %rax
	movq	%rsi, %r8
	cmpw	$1041, 11(%rax)
	je	.L148
.L262:
	xorl	%eax, %eax
.L149:
	testb	%al, %al
	jne	.L188
	movq	-1(%rdx), %rax
	cmpw	$63, 11(%rax)
	ja	.L268
	movq	(%r8), %rax
	movq	%r8, -56(%rbp)
	movl	11(%rax), %r9d
	testl	%r9d, %r9d
	jle	.L168
	cmpl	$10, %r9d
	movl	$10, %ecx
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	%r9d, -72(%rbp)
	cmovle	%r9d, %ecx
	leal	1(%rcx), %ebx
	movl	%ecx, -64(%rbp)
	movslq	%ebx, %rbx
	addq	%rbx, %rbx
	movq	%rbx, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-64(%rbp), %ecx
	movl	-72(%rbp), %r9d
	testq	%rax, %rax
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	je	.L269
.L169:
	movq	%rsi, 72(%r13)
	movq	(%r8), %rdi
	xorl	%edx, %edx
	movl	%r9d, -56(%rbp)
	call	_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii@PLT
	movq	72(%r13), %rax
	cmpw	$255, (%rax)
	ja	.L170
	movl	-56(%rbp), %r9d
	cmpl	$1, %r9d
	je	.L171
	cmpw	$255, 2(%rax)
	ja	.L170
	cmpl	$2, %r9d
	je	.L171
	cmpw	$255, 4(%rax)
	ja	.L170
	cmpl	$3, %r9d
	je	.L171
	cmpw	$255, 6(%rax)
	ja	.L170
	cmpl	$4, %r9d
	je	.L171
	cmpw	$255, 8(%rax)
	ja	.L170
	cmpl	$5, %r9d
	je	.L171
	cmpw	$255, 10(%rax)
	ja	.L170
	cmpl	$6, %r9d
	je	.L171
	cmpw	$255, 12(%rax)
	ja	.L170
	cmpl	$7, %r9d
	je	.L171
	cmpw	$255, 14(%rax)
	ja	.L170
	cmpl	$8, %r9d
	je	.L171
	cmpw	$255, 16(%rax)
	ja	.L170
	cmpl	$9, %r9d
	je	.L171
	cmpw	$255, 18(%rax)
	jbe	.L171
.L170:
	movl	28(%r13), %esi
	movq	40(%r13), %rdi
	call	_ZN2v88internal9SeqString8TruncateENS0_6HandleIS1_EEi@PLT
	leaq	8(%r13), %rdi
	movq	(%rax), %rdx
	movq	40(%r13), %rax
	movq	%rdx, (%rax)
	movl	$1, 16(%r13)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	72(%r13), %rax
.L171:
	xorl	%r9d, %r9d
	movw	%r9w, -2(%rax,%rbx)
	movl	$1, %ebx
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L268:
	movq	(%r8), %rdx
	testb	$1, %dl
	je	.L188
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L168
	movsd	7(%rdx), %xmm0
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L148:
	movq	(%rdi), %rcx
	movq	23(%rdx), %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L150
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %r8
	movq	(%rax), %rsi
.L151:
	testb	$1, %sil
	jne	.L270
.L161:
	movq	(%r8), %rdx
	movq	0(%r13), %rdi
	testb	$1, %dl
	je	.L188
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	je	.L262
	movq	%r8, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L172
	.p2align 4,,10
	.p2align 3
.L263:
	movq	(%r8), %rdx
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L150:
	movq	41088(%rcx), %rax
	cmpq	41096(%rcx), %rax
	je	.L271
.L152:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rcx)
	movq	%rsi, (%rax)
	jmp	.L151
.L271:
	movq	%rcx, %rdi
	movq	%rsi, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L270:
	movq	-1(%rsi), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L272
	movq	(%r8), %rdx
	movq	0(%r13), %rdi
	testb	$1, %dl
	jne	.L157
.L160:
	movq	%r8, %rsi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	jne	.L263
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L272:
	movq	(%rax), %rax
	testb	$1, %al
	je	.L161
	movq	-1(%rax), %rax
	cmpw	$65, 11(%rax)
	je	.L161
	jmp	.L263
.L157:
	movq	-1(%rdx), %rax
	cmpw	$63, 11(%rax)
	ja	.L160
	jmp	.L262
.L269:
	movl	%ecx, -72(%rbp)
	movl	%r9d, -64(%rbp)
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	-56(%rbp), %r8
	movl	-64(%rbp), %r9d
	testq	%rax, %rax
	movl	-72(%rbp), %ecx
	movq	%rax, %rsi
	jne	.L169
.L184:
	leaq	.LC2(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L267:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	movq	-56(%rbp), %rdx
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%rdx, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	-56(%rbp), %rdx
	testq	%rax, %rax
	jne	.L183
	jmp	.L184
	.cfi_endproc
.LFE17968:
	.size	_ZN2v88internal15JsonStringifier13InitializeGapENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal15JsonStringifier13InitializeGapENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal15JsonStringifier13CurrentHolderENS0_6HandleINS0_6ObjectEEES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JsonStringifier13CurrentHolderENS0_6HandleINS0_6ObjectEEES4_
	.type	_ZN2v88internal15JsonStringifier13CurrentHolderENS0_6HandleINS0_6ObjectEEES4_, @function
_ZN2v88internal15JsonStringifier13CurrentHolderENS0_6HandleINS0_6ObjectEEES4_:
.LFB17972:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	96(%rdi), %rax
	movq	(%rdi), %r13
	cmpq	88(%rdi), %rax
	je	.L283
	movq	-8(%rax), %rax
	movq	41112(%r13), %rdi
	movq	(%rax), %rsi
	testq	%rdi, %rdi
	je	.L279
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L279:
	.cfi_restore_state
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L284
.L281:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	.cfi_restore_state
	movq	12464(%r13), %rax
	movq	%rdi, %r12
	movq	%rdx, %r14
	movq	39(%rax), %rax
	movq	879(%rax), %r15
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L275
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L276:
	movq	%r13, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	(%r12), %rdi
	movq	%r14, %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%rax, %rbx
	leaq	128(%rdi), %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L275:
	.cfi_restore_state
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L285
.L277:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r15, (%rsi)
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L284:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L285:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L277
	.cfi_endproc
.LFE17972:
	.size	_ZN2v88internal15JsonStringifier13CurrentHolderENS0_6HandleINS0_6ObjectEEES4_, .-_ZN2v88internal15JsonStringifier13CurrentHolderENS0_6HandleINS0_6ObjectEEES4_
	.section	.text._ZN2v88internal15JsonStringifier8StackPopEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JsonStringifier8StackPopEv
	.type	_ZN2v88internal15JsonStringifier8StackPopEv, @function
_ZN2v88internal15JsonStringifier8StackPopEv:
.LFB17974:
	.cfi_startproc
	endbr64
	subq	$16, 96(%rdi)
	ret
	.cfi_endproc
.LFE17974:
	.size	_ZN2v88internal15JsonStringifier8StackPopEv, .-_ZN2v88internal15JsonStringifier8StackPopEv
	.section	.rodata._ZN2v88internal31CircularStructureMessageBuilder9AppendKeyENS0_6HandleINS0_6ObjectEEE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"index "
.LC8:
	.string	"<anonymous>"
.LC9:
	.string	"property '"
.LC10:
	.string	"key->IsString()"
.LC11:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal31CircularStructureMessageBuilder9AppendKeyENS0_6HandleINS0_6ObjectEEE,"axG",@progbits,_ZN2v88internal31CircularStructureMessageBuilder9AppendKeyENS0_6HandleINS0_6ObjectEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal31CircularStructureMessageBuilder9AppendKeyENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal31CircularStructureMessageBuilder9AppendKeyENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal31CircularStructureMessageBuilder9AppendKeyENS0_6HandleINS0_6ObjectEEE:
.LFB17984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L288
	movl	8(%rdi), %r9d
	testl	%r9d, %r9d
	jne	.L385
	movl	$105, %eax
	leaq	.LC7(%rip), %rbx
.L375:
	movl	20(%r12), %edx
.L289:
	movq	32(%r12), %rcx
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r12)
	movb	%al, -1(%rcx,%rdx)
	movl	20(%r12), %edx
	cmpl	16(%r12), %edx
	je	.L386
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L289
.L291:
	movq	0(%r13), %rdi
	leaq	-144(%rbp), %rsi
	movl	$100, %edx
	sarq	$32, %rdi
	call	_ZN2v88internal12IntToCStringEiNS0_6VectorIcEE@PLT
	movl	8(%r12), %r8d
	movq	%rax, %rbx
	testl	%r8d, %r8d
	jne	.L381
.L378:
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L287
	movl	20(%r12), %edx
.L299:
	movq	32(%r12), %rcx
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r12)
	movb	%al, -1(%rcx,%rdx)
	movl	20(%r12), %edx
	cmpl	16(%r12), %edx
	je	.L387
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L299
	.p2align 4,,10
	.p2align 3
.L287:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L388
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L385:
	.cfi_restore_state
	movl	20(%rdi), %edx
	movl	$105, %eax
	leaq	.LC7(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L290:
	movq	32(%r12), %rcx
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r12)
	movw	%ax, -1(%rcx,%rdx)
	movl	20(%r12), %edx
	cmpl	16(%r12), %edx
	je	.L389
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L290
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L390:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
.L381:
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L287
	movl	20(%r12), %edx
.L298:
	movq	32(%r12), %rcx
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r12)
	movw	%ax, -1(%rcx,%rdx)
	movl	20(%r12), %edx
	cmpl	16(%r12), %edx
	je	.L390
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L298
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L288:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L305
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L305:
	movl	8(%rdi), %edx
	movl	11(%rax), %edi
	testl	%edi, %edi
	jne	.L306
	testl	%edx, %edx
	jne	.L391
	movl	$60, %eax
	leaq	.LC8(%rip), %rbx
.L382:
	movl	20(%r12), %edx
.L307:
	movq	32(%r12), %rcx
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r12)
	movb	%al, -1(%rcx,%rdx)
	movl	20(%r12), %edx
	cmpl	16(%r12), %edx
	je	.L392
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L307
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L389:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L291
	movl	20(%r12), %edx
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L387:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L386:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L375
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L306:
	testl	%edx, %edx
	jne	.L393
	movl	$112, %eax
	leaq	.LC9(%rip), %rbx
.L383:
	movl	20(%r12), %edx
.L314:
	movq	32(%r12), %rcx
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r12)
	movb	%al, -1(%rcx,%rdx)
	movl	20(%r12), %edx
	cmpl	16(%r12), %edx
	je	.L394
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L314
.L316:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	32(%r12), %rax
	movl	8(%r12), %esi
	movq	(%rax), %rdx
	movl	20(%r12), %eax
	testl	%esi, %esi
	leal	1(%rax), %ecx
	movl	%ecx, 20(%r12)
	jne	.L321
	addl	$16, %eax
	cltq
	movb	$39, -1(%rdx,%rax)
	movl	16(%r12), %eax
	cmpl	%eax, 20(%r12)
	jne	.L287
.L384:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L393:
	movl	20(%r12), %edx
	movl	$112, %eax
	leaq	.LC9(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L315:
	movq	32(%r12), %rcx
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r12)
	movw	%ax, -1(%rcx,%rdx)
	movl	20(%r12), %edx
	cmpl	16(%r12), %edx
	je	.L395
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L315
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L391:
	movl	20(%r12), %edx
	movl	$60, %eax
	leaq	.LC8(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L308:
	movq	32(%r12), %rcx
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r12)
	movw	%ax, -1(%rcx,%rdx)
	movl	20(%r12), %edx
	cmpl	16(%r12), %edx
	je	.L396
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L308
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L394:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L383
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L396:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L287
	movl	20(%r12), %edx
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L395:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L316
	movl	20(%r12), %edx
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L392:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L382
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L321:
	leal	16(%rax,%rax), %eax
	movl	$39, %ecx
	cltq
	movw	%cx, -1(%rdx,%rax)
	movl	16(%r12), %eax
	cmpl	%eax, 20(%r12)
	jne	.L287
	jmp	.L384
.L388:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17984:
	.size	_ZN2v88internal31CircularStructureMessageBuilder9AppendKeyENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal31CircularStructureMessageBuilder9AppendKeyENS0_6HandleINS0_6ObjectEEE
	.section	.rodata._ZN2v88internal15JsonStringifier38ConstructCircularStructureErrorMessageENS0_6HandleINS0_6ObjectEEEm.str1.1,"aMS",@progbits,1
.LC12:
	.string	"\n    --> "
	.section	.rodata._ZN2v88internal15JsonStringifier38ConstructCircularStructureErrorMessageENS0_6HandleINS0_6ObjectEEEm.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"starting at object with constructor "
	.section	.rodata._ZN2v88internal15JsonStringifier38ConstructCircularStructureErrorMessageENS0_6HandleINS0_6ObjectEEEm.str1.1
.LC14:
	.string	"\n    |     "
.LC15:
	.string	" -> object with constructor "
.LC16:
	.string	"..."
.LC17:
	.string	"\n    --- "
.LC18:
	.string	" closes the circle"
	.section	.text._ZN2v88internal15JsonStringifier38ConstructCircularStructureErrorMessageENS0_6HandleINS0_6ObjectEEEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JsonStringifier38ConstructCircularStructureErrorMessageENS0_6HandleINS0_6ObjectEEEm
	.type	_ZN2v88internal15JsonStringifier38ConstructCircularStructureErrorMessageENS0_6HandleINS0_6ObjectEEEm, @function
_ZN2v88internal15JsonStringifier38ConstructCircularStructureErrorMessageENS0_6HandleINS0_6ObjectEEEm:
.LFB17989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%r14), %rbx
	subq	$88, %rsp
	movq	%rsi, -128(%rbp)
	movq	(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal24IncrementalStringBuilderC1EPNS0_7IsolateE@PLT
	movq	88(%r13), %rdx
	movq	96(%r13), %rax
	leaq	.LC12(%rip), %rcx
	movl	-88(%rbp), %r8d
	subq	%rdx, %rax
	sarq	$4, %rax
	movq	%rax, -112(%rbp)
	movq	%r14, %rax
	salq	$4, %rax
	movq	8(%rdx,%rax), %r15
	movl	-76(%rbp), %eax
	movl	$10, %edx
	testl	%r8d, %r8d
	jne	.L399
	movl	$10, %ecx
	leaq	.LC12(%rip), %rdx
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L622:
	movzbl	(%rdx), %ecx
	testb	%cl, %cl
	je	.L405
.L400:
	movq	-64(%rbp), %rsi
	leal	1(%rax), %edi
	addl	$16, %eax
	addq	$1, %rdx
	cltq
	movq	(%rsi), %rsi
	movl	%edi, -76(%rbp)
	movb	%cl, -1(%rsi,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	jne	.L622
	movq	%r12, %rdi
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-104(%rbp), %rdx
	movl	-76(%rbp), %eax
	movzbl	(%rdx), %ecx
	testb	%cl, %cl
	jne	.L400
	.p2align 4,,10
	.p2align 3
.L405:
	movl	-88(%rbp), %edi
	movl	$115, %edx
	leaq	.LC13(%rip), %rcx
	testl	%edi, %edi
	jne	.L409
	movl	$115, %ecx
	leaq	.LC13(%rip), %rdx
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L625:
	movzbl	(%rdx), %ecx
	testb	%cl, %cl
	je	.L414
.L408:
	movq	-64(%rbp), %rsi
	leal	1(%rax), %edi
	addl	$16, %eax
	addq	$1, %rdx
	cltq
	movq	(%rsi), %rsi
	movl	%edi, -76(%rbp)
	movb	%cl, -1(%rsi,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	jne	.L625
	movq	%r12, %rdi
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-104(%rbp), %rdx
	movl	-76(%rbp), %eax
	movzbl	(%rdx), %ecx
	testb	%cl, %cl
	jne	.L408
	.p2align 4,,10
	.p2align 3
.L414:
	movl	-88(%rbp), %esi
	movq	-64(%rbp), %rdx
	leal	1(%rax), %ecx
	testl	%esi, %esi
	movq	(%rdx), %rdx
	movl	%ecx, -76(%rbp)
	jne	.L417
	addl	$16, %eax
	cltq
	movb	$39, -1(%rdx,%rax)
	movl	-80(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	je	.L418
.L419:
	movq	%r15, %rdi
	call	_ZN2v88internal10JSReceiver18GetConstructorNameENS0_6HandleIS1_EE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	-64(%rbp), %rax
	movl	-88(%rbp), %r10d
	movq	(%rax), %rdx
	movl	-76(%rbp), %eax
	testl	%r10d, %r10d
	leal	1(%rax), %ecx
	movl	%ecx, -76(%rbp)
	jne	.L421
	addl	$16, %eax
	cltq
	movb	$39, -1(%rdx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L626
.L422:
	movq	-112(%rbp), %rdi
	addq	$3, %r14
	cmpq	%rdi, %r14
	cmova	%rdi, %r14
	movq	%r14, -120(%rbp)
	cmpq	%r14, %rbx
	jnb	.L508
	.p2align 4,,10
	.p2align 3
.L447:
	movq	%rbx, %rdx
	movl	-88(%rbp), %r8d
	leaq	.LC14(%rip), %rcx
	salq	$4, %rdx
	addq	88(%r13), %rdx
	movq	(%rdx), %r15
	movq	8(%rdx), %r14
	movl	$10, %edx
	testl	%r8d, %r8d
	jne	.L425
	movl	$10, %edx
	leaq	.LC14(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L424:
	movq	-64(%rbp), %rsi
	leal	1(%rax), %edi
	addl	$16, %eax
	addq	$1, %rcx
	cltq
	movq	(%rsi), %rsi
	movl	%edi, -76(%rbp)
	movb	%dl, -1(%rsi,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L638
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	jne	.L424
.L426:
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	.LC15(%rip), %r15
	call	_ZN2v88internal31CircularStructureMessageBuilder9AppendKeyENS0_6HandleINS0_6ObjectEEE
	movl	-88(%rbp), %edi
	movl	-76(%rbp), %eax
	movl	$32, %edx
	testl	%edi, %edi
	jne	.L432
	movl	$32, %edx
	leaq	.LC15(%rip), %r15
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L628:
	movzbl	(%r15), %edx
	testb	%dl, %dl
	je	.L438
.L433:
	movq	-64(%rbp), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$1, %r15
	cltq
	movq	(%rcx), %rcx
	movl	%esi, -76(%rbp)
	movb	%dl, -1(%rcx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	jne	.L628
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r15), %edx
	movl	-76(%rbp), %eax
	testb	%dl, %dl
	jne	.L433
	.p2align 4,,10
	.p2align 3
.L438:
	movl	-88(%rbp), %esi
	movq	-64(%rbp), %rdx
	leal	1(%rax), %ecx
	testl	%esi, %esi
	movq	(%rdx), %rdx
	movl	%ecx, -76(%rbp)
	jne	.L441
	addl	$16, %eax
	cltq
	movb	$39, -1(%rdx,%rax)
	movl	-80(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	je	.L442
.L443:
	movq	%r14, %rdi
	call	_ZN2v88internal10JSReceiver18GetConstructorNameENS0_6HandleIS1_EE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	-64(%rbp), %rax
	movl	-88(%rbp), %r14d
	movq	(%rax), %rdx
	movl	-76(%rbp), %eax
	testl	%r14d, %r14d
	leal	1(%rax), %ecx
	movl	%ecx, -76(%rbp)
	jne	.L445
	addl	$16, %eax
	cltq
	movb	$39, -1(%rdx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L629
.L446:
	addq	$1, %rbx
	cmpq	%rbx, -120(%rbp)
	jne	.L447
.L423:
	movq	-120(%rbp), %rbx
	movl	-88(%rbp), %ecx
	leaq	1(%rbx), %rdx
	cmpq	-112(%rbp), %rdx
	jb	.L639
.L448:
	movq	-112(%rbp), %rdi
	movq	-120(%rbp), %rsi
	leaq	-1(%rdi), %rbx
	cmpq	%rsi, %rbx
	cmovb	%rsi, %rbx
	cmpq	%rbx, %rdi
	jbe	.L466
	.p2align 4,,10
	.p2align 3
.L490:
	movq	%rbx, %rdx
	salq	$4, %rdx
	addq	88(%r13), %rdx
	movq	(%rdx), %r15
	movq	8(%rdx), %r14
	testl	%ecx, %ecx
	je	.L512
	movl	$10, %edx
	leaq	.LC14(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L468:
	movq	-64(%rbp), %rsi
	leal	1(%rax), %edi
	leal	16(%rax,%rax), %eax
	addq	$1, %rcx
	cltq
	movq	(%rsi), %rsi
	movl	%edi, -76(%rbp)
	movw	%dx, -1(%rsi,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L640
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	jne	.L468
.L469:
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	.LC15(%rip), %r15
	call	_ZN2v88internal31CircularStructureMessageBuilder9AppendKeyENS0_6HandleINS0_6ObjectEEE
	movl	-88(%rbp), %r9d
	movl	-76(%rbp), %eax
	movl	$32, %edx
	testl	%r9d, %r9d
	jne	.L475
	movl	$32, %edx
	leaq	.LC15(%rip), %r15
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L634:
	movzbl	(%r15), %edx
	testb	%dl, %dl
	je	.L481
.L476:
	movq	-64(%rbp), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$1, %r15
	cltq
	movq	(%rcx), %rcx
	movl	%esi, -76(%rbp)
	movb	%dl, -1(%rcx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	jne	.L634
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r15), %edx
	movl	-76(%rbp), %eax
	testb	%dl, %dl
	jne	.L476
	.p2align 4,,10
	.p2align 3
.L481:
	movl	-88(%rbp), %r8d
	movq	-64(%rbp), %rdx
	leal	1(%rax), %ecx
	testl	%r8d, %r8d
	movq	(%rdx), %rdx
	movl	%ecx, -76(%rbp)
	jne	.L484
	addl	$16, %eax
	cltq
	movb	$39, -1(%rdx,%rax)
	movl	-80(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	je	.L485
.L486:
	movq	%r14, %rdi
	call	_ZN2v88internal10JSReceiver18GetConstructorNameENS0_6HandleIS1_EE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	-64(%rbp), %rax
	movl	-88(%rbp), %esi
	movq	(%rax), %rdx
	movl	-76(%rbp), %eax
	testl	%esi, %esi
	leal	1(%rax), %ecx
	movl	%ecx, -76(%rbp)
	jne	.L488
	addl	$16, %eax
	cltq
	movb	$39, -1(%rdx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L635
.L489:
	movl	-88(%rbp), %ecx
	addq	$1, %rbx
	cmpq	%rbx, -112(%rbp)
	jne	.L490
.L466:
	movl	$10, %edx
	leaq	.LC17(%rip), %rbx
	testl	%ecx, %ecx
	jne	.L492
	movl	$10, %edx
	leaq	.LC17(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L491:
	movq	-64(%rbp), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, -76(%rbp)
	movb	%dl, -1(%rcx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L641
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L491
.L493:
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal31CircularStructureMessageBuilder9AppendKeyENS0_6HandleINS0_6ObjectEEE
	movl	-88(%rbp), %eax
	testl	%eax, %eax
	je	.L514
	movl	-76(%rbp), %eax
	movl	$32, %edx
	leaq	.LC18(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L499:
	movq	-64(%rbp), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, -76(%rbp)
	movw	%dx, -1(%rcx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L642
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L499
.L500:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	testq	%rax, %rax
	je	.L643
.L505:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L644
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L645:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	-76(%rbp), %eax
	movq	-104(%rbp), %rcx
.L621:
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	je	.L405
.L399:
	movq	-64(%rbp), %rsi
	leal	1(%rax), %edi
	leal	16(%rax,%rax), %eax
	addq	$1, %rcx
	cltq
	movq	(%rsi), %rsi
	movl	%edi, -76(%rbp)
	movw	%dx, -1(%rsi,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	jne	.L621
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L646:
	movq	%r12, %rdi
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	-76(%rbp), %eax
	movq	-104(%rbp), %rcx
.L623:
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	je	.L414
.L409:
	movq	-64(%rbp), %rsi
	leal	1(%rax), %edi
	leal	16(%rax,%rax), %eax
	addq	$1, %rcx
	cltq
	movq	(%rsi), %rsi
	movl	%edi, -76(%rbp)
	movw	%dx, -1(%rsi,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	jne	.L623
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L647:
	movq	%r12, %rdi
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-104(%rbp), %rcx
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	je	.L426
	movl	-76(%rbp), %eax
	.p2align 4,,10
	.p2align 3
.L425:
	movq	-64(%rbp), %rsi
	leal	1(%rax), %edi
	leal	16(%rax,%rax), %eax
	addq	$1, %rcx
	cltq
	movq	(%rsi), %rsi
	movl	%edi, -76(%rbp)
	movw	%dx, -1(%rsi,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L647
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	jne	.L425
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L648:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	-76(%rbp), %eax
.L627:
	movzbl	(%r15), %edx
	testb	%dl, %dl
	je	.L438
.L432:
	movq	-64(%rbp), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$1, %r15
	cltq
	movq	(%rcx), %rcx
	movl	%esi, -76(%rbp)
	movw	%dx, -1(%rcx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	jne	.L627
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L649:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	-76(%rbp), %eax
.L633:
	movzbl	(%r15), %edx
	testb	%dl, %dl
	je	.L481
.L475:
	movq	-64(%rbp), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$1, %r15
	cltq
	movq	(%rcx), %rcx
	movl	%esi, -76(%rbp)
	movw	%dx, -1(%rcx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	jne	.L633
	jmp	.L649
	.p2align 4,,10
	.p2align 3
.L650:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	je	.L493
	movl	-76(%rbp), %eax
	.p2align 4,,10
	.p2align 3
.L492:
	movq	-64(%rbp), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, -76(%rbp)
	movw	%dx, -1(%rcx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L650
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L492
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L638:
	movq	%r12, %rdi
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-104(%rbp), %rcx
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	je	.L426
	movl	-76(%rbp), %eax
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L640:
	movq	%r12, %rdi
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-104(%rbp), %rcx
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	je	.L469
	movl	-76(%rbp), %eax
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L512:
	movl	$10, %edx
	leaq	.LC14(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L467:
	movq	-64(%rbp), %rsi
	leal	1(%rax), %edi
	addl	$16, %eax
	addq	$1, %rcx
	cltq
	movq	(%rsi), %rsi
	movl	%edi, -76(%rbp)
	movb	%dl, -1(%rsi,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L651
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	jne	.L467
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L651:
	movq	%r12, %rdi
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-104(%rbp), %rcx
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	je	.L469
	movl	-76(%rbp), %eax
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L484:
	leal	16(%rax,%rax), %eax
	movl	$39, %edi
	cltq
	movw	%di, -1(%rdx,%rax)
	movl	-80(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	jne	.L486
.L485:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L488:
	leal	16(%rax,%rax), %eax
	movl	$39, %ecx
	cltq
	movw	%cx, -1(%rdx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	jne	.L489
.L635:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	-76(%rbp), %eax
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L445:
	leal	16(%rax,%rax), %eax
	movl	$39, %r11d
	cltq
	movw	%r11w, -1(%rdx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	jne	.L446
.L629:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	-76(%rbp), %eax
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L441:
	leal	16(%rax,%rax), %eax
	movl	$39, %r15d
	cltq
	movw	%r15w, -1(%rdx,%rax)
	movl	-80(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	jne	.L443
.L442:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L641:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	je	.L493
	movl	-76(%rbp), %eax
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L642:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	je	.L500
	movl	-76(%rbp), %eax
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L514:
	movl	$32, %edx
	leaq	.LC18(%rip), %rbx
.L636:
	movl	-76(%rbp), %eax
.L498:
	movq	-64(%rbp), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, -76(%rbp)
	movb	%dl, -1(%rcx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L652
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L498
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L652:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L636
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L421:
	leal	16(%rax,%rax), %eax
	movl	$39, %r9d
	cltq
	movw	%r9w, -1(%rdx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	jne	.L422
.L626:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	-76(%rbp), %eax
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L417:
	leal	16(%rax,%rax), %eax
	movl	$39, %r11d
	cltq
	movw	%r11w, -1(%rdx,%rax)
	movl	-80(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	jne	.L419
.L418:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L639:
	movl	$10, %edx
	leaq	.LC14(%rip), %rbx
	testl	%ecx, %ecx
	jne	.L450
	movl	$10, %edx
	leaq	.LC14(%rip), %rbx
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L632:
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	je	.L455
.L449:
	movq	-64(%rbp), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, -76(%rbp)
	movb	%dl, -1(%rcx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	jne	.L632
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	movl	-76(%rbp), %eax
	testb	%dl, %dl
	jne	.L449
	.p2align 4,,10
	.p2align 3
.L455:
	movl	-88(%rbp), %r10d
	movl	$46, %edx
	leaq	.LC16(%rip), %rbx
	testl	%r10d, %r10d
	jne	.L459
	movl	$46, %edx
	leaq	.LC16(%rip), %rbx
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L637:
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	je	.L463
.L458:
	movq	-64(%rbp), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, -76(%rbp)
	movb	%dl, -1(%rcx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	jne	.L637
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	movl	-76(%rbp), %eax
	testb	%dl, %dl
	jne	.L458
	.p2align 4,,10
	.p2align 3
.L463:
	movl	-88(%rbp), %ecx
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L653:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	-76(%rbp), %eax
.L630:
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	je	.L455
.L450:
	movq	-64(%rbp), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, -76(%rbp)
	movw	%dx, -1(%rcx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	jne	.L630
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L654:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	movl	-76(%rbp), %eax
	testb	%dl, %dl
	je	.L463
	.p2align 4,,10
	.p2align 3
.L459:
	movq	-64(%rbp), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, -76(%rbp)
	movw	%dx, -1(%rcx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L654
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L459
	movl	-88(%rbp), %ecx
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L643:
	movq	0(%r13), %rax
	subq	$-128, %rax
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L508:
	movq	%rbx, -120(%rbp)
	jmp	.L423
.L644:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17989:
	.size	_ZN2v88internal15JsonStringifier38ConstructCircularStructureErrorMessageENS0_6HandleINS0_6ObjectEEEm, .-_ZN2v88internal15JsonStringifier38ConstructCircularStructureErrorMessageENS0_6HandleINS0_6ObjectEEEm
	.section	.text._ZN2v88internal15JsonStringifier12SerializeSmiENS0_3SmiE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JsonStringifier12SerializeSmiENS0_3SmiE
	.type	_ZN2v88internal15JsonStringifier12SerializeSmiENS0_3SmiE, @function
_ZN2v88internal15JsonStringifier12SerializeSmiENS0_3SmiE:
.LFB17993:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$100, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-144(%rbp), %r8
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%r8, %rsi
	sarq	$32, %rdi
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal12IntToCStringEiNS0_6VectorIcEE@PLT
	movq	%rax, %r13
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jne	.L687
.L684:
	movzbl	0(%r13), %eax
	testb	%al, %al
	je	.L657
	movl	28(%rbx), %edx
.L659:
	movq	40(%rbx), %rcx
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %r13
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 28(%rbx)
	movb	%al, -1(%rcx,%rdx)
	movl	28(%rbx), %edx
	cmpl	24(%rbx), %edx
	je	.L688
	movzbl	0(%r13), %eax
	testb	%al, %al
	jne	.L659
.L657:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L689
	addq	$120, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L690:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
.L687:
	movzbl	0(%r13), %eax
	testb	%al, %al
	je	.L657
	movl	28(%rbx), %edx
.L658:
	movq	40(%rbx), %rcx
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %r13
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 28(%rbx)
	movw	%ax, -1(%rcx,%rdx)
	movl	28(%rbx), %edx
	cmpl	24(%rbx), %edx
	je	.L690
	movzbl	0(%r13), %eax
	testb	%al, %al
	jne	.L658
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L688:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L684
.L689:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17993:
	.size	_ZN2v88internal15JsonStringifier12SerializeSmiENS0_3SmiE, .-_ZN2v88internal15JsonStringifier12SerializeSmiENS0_3SmiE
	.section	.rodata._ZN2v88internal15JsonStringifier15SerializeDoubleEd.str1.1,"aMS",@progbits,1
.LC19:
	.string	"null"
	.section	.text._ZN2v88internal15JsonStringifier15SerializeDoubleEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JsonStringifier15SerializeDoubleEd
	.type	_ZN2v88internal15JsonStringifier15SerializeDoubleEd, @function
_ZN2v88internal15JsonStringifier15SerializeDoubleEd:
.LFB17994:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movapd	%xmm0, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	8(%rdi), %rbx
	subq	$120, %rsp
	andpd	.LC3(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	ucomisd	.LC4(%rip), %xmm1
	ja	.L710
	ucomisd	%xmm0, %xmm0
	jp	.L710
	leaq	-144(%rbp), %rdi
	movl	$100, %esi
	call	_ZN2v88internal15DoubleToCStringEdNS0_6VectorIcEE@PLT
	movq	%rax, %r13
	movl	16(%r12), %eax
	testl	%eax, %eax
	jne	.L746
.L743:
	movzbl	0(%r13), %ecx
	testb	%cl, %cl
	je	.L697
	movl	28(%r12), %edx
.L699:
	movq	40(%r12), %rax
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %r13
	movslq	%edx, %rdx
	movq	(%rax), %rax
	movl	%esi, 28(%r12)
	movb	%cl, -1(%rax,%rdx)
	movl	28(%r12), %edx
	cmpl	24(%r12), %edx
	je	.L747
	movzbl	0(%r13), %ecx
	testb	%cl, %cl
	jne	.L699
.L697:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L748
	addq	$120, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L749:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
.L746:
	movzbl	0(%r13), %ecx
	testb	%cl, %cl
	je	.L697
	movl	28(%r12), %edx
.L698:
	movq	40(%r12), %rax
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %r13
	movslq	%edx, %rdx
	movq	(%rax), %rax
	movl	%esi, 28(%r12)
	movw	%cx, -1(%rax,%rdx)
	movl	28(%r12), %edx
	cmpl	24(%r12), %edx
	je	.L749
	movzbl	0(%r13), %ecx
	testb	%cl, %cl
	jne	.L698
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L747:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L710:
	movl	16(%r12), %edx
	testl	%edx, %edx
	je	.L709
	movl	28(%r12), %edx
	movl	$110, %eax
	leaq	.LC19(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L695:
	movq	40(%r12), %rcx
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %r13
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 28(%r12)
	movw	%ax, -1(%rcx,%rdx)
	movl	28(%r12), %edx
	cmpl	24(%r12), %edx
	je	.L750
	movzbl	0(%r13), %eax
	testb	%al, %al
	jne	.L695
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L750:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	0(%r13), %eax
	testb	%al, %al
	je	.L697
	movl	28(%r12), %edx
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L709:
	movl	$110, %eax
	leaq	.LC19(%rip), %r13
.L740:
	movl	28(%r12), %edx
.L694:
	movq	40(%r12), %rcx
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %r13
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 28(%r12)
	movb	%al, -1(%rcx,%rdx)
	movl	28(%r12), %edx
	cmpl	24(%r12), %edx
	je	.L751
	movzbl	0(%r13), %eax
	testb	%al, %al
	jne	.L694
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L751:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	0(%r13), %eax
	testb	%al, %al
	jne	.L740
	jmp	.L697
.L748:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17994:
	.size	_ZN2v88internal15JsonStringifier15SerializeDoubleEd, .-_ZN2v88internal15JsonStringifier15SerializeDoubleEd
	.section	.text._ZN2v88internal15JsonStringifier11DoNotEscapeIhEEbT_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JsonStringifier11DoNotEscapeIhEEbT_
	.type	_ZN2v88internal15JsonStringifier11DoNotEscapeIhEEbT_, @function
_ZN2v88internal15JsonStringifier11DoNotEscapeIhEEbT_:
.LFB18002:
	.cfi_startproc
	endbr64
	leal	-35(%rdi), %eax
	cmpb	$91, %al
	setbe	%al
	cmpb	$92, %dil
	setne	%dl
	andl	%edx, %eax
	ret
	.cfi_endproc
.LFE18002:
	.size	_ZN2v88internal15JsonStringifier11DoNotEscapeIhEEbT_, .-_ZN2v88internal15JsonStringifier11DoNotEscapeIhEEbT_
	.section	.text._ZN2v88internal15JsonStringifier11DoNotEscapeItEEbT_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JsonStringifier11DoNotEscapeItEEbT_
	.type	_ZN2v88internal15JsonStringifier11DoNotEscapeItEEbT_, @function
_ZN2v88internal15JsonStringifier11DoNotEscapeItEEbT_:
.LFB18003:
	.cfi_startproc
	endbr64
	cmpw	$34, %di
	seta	%al
	cmpw	$92, %di
	setne	%dl
	andl	%edx, %eax
	cmpw	$127, %di
	setne	%dl
	andb	%dl, %al
	jne	.L758
	ret
	.p2align 4,,10
	.p2align 3
.L758:
	addw	$10240, %di
	cmpw	$2047, %di
	seta	%al
	ret
	.cfi_endproc
.LFE18003:
	.size	_ZN2v88internal15JsonStringifier11DoNotEscapeItEEbT_, .-_ZN2v88internal15JsonStringifier11DoNotEscapeItEEbT_
	.section	.rodata._ZN2v88internal15JsonStringifier15SerializeStringENS0_6HandleINS0_6StringEEE.str1.1,"aMS",@progbits,1
.LC20:
	.string	"\\u"
	.section	.rodata
	.align 8
.LC21:
	.string	"\\u0000"
	.string	" \\u0001"
	.string	" \\u0002"
	.string	" \\u0003"
	.string	" \\u0004"
	.string	" \\u0005"
	.string	" \\u0006"
	.string	" \\u0007"
	.string	" \\b"
	.string	"     \\t"
	.string	"     \\n"
	.string	"     \\u000b"
	.string	" \\f"
	.string	"     \\r"
	.string	"     \\u000e"
	.string	" \\u000f"
	.string	" \\u0010"
	.string	" \\u0011"
	.string	" \\u0012"
	.string	" \\u0013"
	.string	" \\u0014"
	.string	" \\u0015"
	.string	" \\u0016"
	.string	" \\u0017"
	.string	" \\u0018"
	.string	" \\u0019"
	.string	" \\u001a"
	.string	" \\u001b"
	.string	" \\u001c"
	.string	" \\u001d"
	.string	" \\u001e"
	.string	" \\u001f"
	.string	"  "
	.string	"      !"
	.string	"      \\\""
	.string	"     #"
	.string	"      $"
	.string	"      %"
	.string	"      &"
	.string	"      '"
	.string	"      ("
	.string	"      )"
	.string	"      *"
	.string	"      +"
	.string	"      ,"
	.string	"      -"
	.string	"      ."
	.string	"      /"
	.string	"      0"
	.string	"      1"
	.string	"      2"
	.string	"      3"
	.string	"      4"
	.string	"      5"
	.string	"      6"
	.string	"      7"
	.string	"      8"
	.string	"      9"
	.string	"      :"
	.string	"      ;"
	.string	"      <"
	.string	"      ="
	.string	"      >"
	.string	"      ?"
	.string	"      @"
	.string	"      A"
	.string	"      B"
	.string	"      C"
	.string	"      D"
	.string	"      E"
	.string	"      F"
	.string	"      G"
	.string	"      H"
	.string	"      I"
	.string	"      J"
	.string	"      K"
	.string	"      L"
	.string	"      M"
	.string	"      N"
	.string	"      O"
	.string	"      P"
	.string	"      Q"
	.string	"      R"
	.string	"      S"
	.string	"      T"
	.string	"      U"
	.string	"      V"
	.string	"      W"
	.string	"      X"
	.string	"      Y"
	.string	"      Z"
	.string	"      ["
	.string	"      \\\\"
	.string	"     ]"
	.string	"      ^"
	.string	"      _"
	.string	"      `"
	.string	"      a"
	.string	"      b"
	.string	"      c"
	.string	"      d"
	.string	"      e"
	.string	"      f"
	.string	"      g"
	.string	"      h"
	.string	"      i"
	.string	"      j"
	.string	"      k"
	.string	"      l"
	.string	"      m"
	.string	"      n"
	.string	"      o"
	.string	"      p"
	.string	"      q"
	.string	"      r"
	.string	"      s"
	.string	"      t"
	.string	"      u"
	.string	"      v"
	.string	"      w"
	.string	"      x"
	.string	"      y"
	.string	"      z"
	.string	"      {"
	.string	"      |"
	.string	"      }"
	.string	"      ~"
	.string	"      \177"
	.string	"      \200"
	.string	"      \201"
	.string	"      \202"
	.string	"      \203"
	.string	"      \204"
	.string	"      \205"
	.string	"      \206"
	.string	"      \207"
	.string	"      \210"
	.string	"      \211"
	.string	"      \212"
	.string	"      \213"
	.string	"      \214"
	.string	"      \215"
	.string	"      \216"
	.string	"      \217"
	.string	"      \220"
	.string	"      \221"
	.string	"      \222"
	.string	"      \223"
	.string	"      \224"
	.string	"      \225"
	.string	"      \226"
	.string	"      \227"
	.string	"      \230"
	.string	"      \231"
	.string	"      \232"
	.string	"      \233"
	.string	"      \234"
	.string	"      \235"
	.string	"      \236"
	.string	"      \237"
	.string	"      \240"
	.string	"      \241"
	.string	"      \242"
	.string	"      \243"
	.string	"      \244"
	.string	"      \245"
	.string	"      \246"
	.string	"      \247"
	.string	"      \250"
	.string	"      \251"
	.string	"      \252"
	.string	"      \253"
	.string	"      \254"
	.string	"      \255"
	.string	"      \256"
	.string	"      \257"
	.string	"      \260"
	.string	"      \261"
	.string	"      \262"
	.string	"      \263"
	.string	"      \264"
	.string	"      \265"
	.string	"      \266"
	.string	"      \267"
	.string	"      \270"
	.string	"      \271"
	.string	"      \272"
	.string	"      \273"
	.string	"      \274"
	.string	"      \275"
	.string	"      \276"
	.string	"      \277"
	.string	"      \300"
	.string	"      \301"
	.string	"      \302"
	.string	"      \303"
	.string	"      \304"
	.string	"      \305"
	.string	"      \306"
	.string	"      \307"
	.string	"      \310"
	.string	"      \311"
	.string	"      \312"
	.string	"      \313"
	.string	"      \314"
	.string	"      \315"
	.string	"      \316"
	.string	"      \317"
	.string	"      \320"
	.string	"      \321"
	.string	"      \322"
	.string	"      \323"
	.string	"      \324"
	.string	"      \325"
	.string	"      \326"
	.string	"      \327"
	.string	"      \330"
	.string	"      \331"
	.string	"      \332"
	.string	"      \333"
	.string	"      \334"
	.string	"      \335"
	.string	"      \336"
	.string	"      \337"
	.string	"      \340"
	.string	"      \341"
	.string	"      \342"
	.string	"      \343"
	.string	"      \344"
	.string	"      \345"
	.string	"      \346"
	.string	"      \347"
	.string	"      \350"
	.string	"      \351"
	.string	"      \352"
	.string	"      \353"
	.string	"      \354"
	.string	"      \355"
	.string	"      \356"
	.string	"      \357"
	.string	"      \360"
	.string	"      \361"
	.string	"      \362"
	.string	"      \363"
	.string	"      \364"
	.string	"      \365"
	.string	"      \366"
	.string	"      \367"
	.string	"      \370"
	.string	"      \371"
	.ascii	"      "
	.string	"\372"
	.string	"      \373"
	.string	"      \374"
	.string	"      \375"
	.string	"      \376"
	.string	"      \377"
	.string	"      "
	.section	.text._ZN2v88internal15JsonStringifier15SerializeStringENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JsonStringifier15SerializeStringENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal15JsonStringifier15SerializeStringENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal15JsonStringifier15SerializeStringENS0_6HandleINS0_6StringEEE:
.LFB18007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, %rsi
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L1164
.L761:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	jbe	.L770
.L1150:
	movq	0(%r13), %rsi
.L769:
	movl	16(%r14), %r9d
	testl	%r9d, %r9d
	jne	.L776
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	je	.L777
.L1174:
	cmpw	$8, %ax
	jne	.L1165
	movq	0(%r13), %rax
	leaq	8(%r14), %r12
	movl	11(%rax), %ebx
	movq	40(%r14), %rax
	movq	(%rax), %rdx
	movl	28(%r14), %eax
	leal	1(%rax), %ecx
	addl	$16, %eax
	cltq
	movl	%ecx, 28(%r14)
	movb	$34, -1(%rdx,%rax)
	movl	24(%r14), %eax
	cmpl	%eax, 28(%r14)
	je	.L1166
.L781:
	cmpl	$16384, %ebx
	jg	.L782
	movl	24(%r14), %eax
	sall	$3, %ebx
	subl	28(%r14), %eax
	cmpl	%eax, %ebx
	jge	.L782
	testl	%ebx, %ebx
	jne	.L1167
.L782:
	movq	(%r14), %rsi
	movq	%r13, %rdx
	leaq	-112(%rbp), %rdi
	xorl	%r15d, %r15d
	leaq	.LC21(%rip), %r13
	call	_ZN2v88internal16FlatStringReaderC1EPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movl	-76(%rbp), %edx
	testl	%edx, %edx
	jg	.L789
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L1169:
	cmpb	$92, %bl
	je	.L790
	movq	40(%r14), %rax
	movq	(%rax), %rdx
	movl	28(%r14), %eax
	leal	1(%rax), %ecx
	addl	$16, %eax
	cltq
	movl	%ecx, 28(%r14)
	movb	%bl, -1(%rdx,%rax)
	movl	-76(%rbp), %edx
	movl	24(%r14), %eax
	cmpl	%eax, 28(%r14)
	je	.L1168
.L792:
	addq	$1, %r15
	cmpl	%r15d, %edx
	jle	.L802
.L789:
	movq	-72(%rbp), %rax
	movzbl	(%rax,%r15), %ebx
	leal	-35(%rbx), %eax
	cmpb	$91, %al
	jbe	.L1169
.L790:
	salq	$3, %rbx
	movl	16(%r14), %r8d
	andl	$2040, %ebx
	addq	%r13, %rbx
	movzbl	(%rbx), %eax
	testl	%r8d, %r8d
	je	.L793
	testb	%al, %al
	je	.L792
.L1152:
	movl	28(%r14), %edx
.L794:
	movq	40(%r14), %rcx
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 28(%r14)
	movw	%ax, -1(%rcx,%rdx)
	movl	28(%r14), %edx
	cmpl	24(%r14), %edx
	je	.L1170
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L794
.L799:
	movl	-76(%rbp), %edx
.L1185:
	addq	$1, %r15
	cmpl	%r15d, %edx
	jg	.L789
.L802:
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rdx, 41704(%rax)
	movl	28(%r14), %eax
	movq	40(%r14), %rdx
.L788:
	movq	(%rdx), %rdx
	leal	1(%rax), %ecx
	addl	$16, %eax
	cltq
	movl	%ecx, 28(%r14)
	movb	$34, -1(%rdx,%rax)
	movl	24(%r14), %eax
	cmpl	%eax, 28(%r14)
	jne	.L759
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L1171:
	cmpw	$8, %ax
	je	.L805
	movq	15(%rsi), %rsi
.L776:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	jne	.L1171
	movq	0(%r13), %rax
	movl	$34, %edi
	leaq	8(%r14), %r12
	movl	11(%rax), %ebx
	movq	40(%r14), %rax
	movq	(%rax), %rdx
	movl	28(%r14), %eax
	leal	1(%rax), %ecx
	leal	16(%rax,%rax), %eax
	cltq
	movl	%ecx, 28(%r14)
	movw	%di, -1(%rdx,%rax)
	movl	24(%r14), %eax
	cmpl	%eax, 28(%r14)
	je	.L1172
.L807:
	cmpl	$16384, %ebx
	jg	.L831
	movl	24(%r14), %eax
	sall	$3, %ebx
	subl	28(%r14), %eax
	cmpl	%eax, %ebx
	jge	.L831
	testl	%ebx, %ebx
	jne	.L1173
.L831:
	movq	(%r14), %rsi
	movq	%r13, %rdx
	leaq	-112(%rbp), %rdi
	leaq	.LC21(%rip), %r13
	call	_ZN2v88internal16FlatStringReaderC1EPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movl	-76(%rbp), %esi
	xorl	%eax, %eax
	testl	%esi, %esi
	jg	.L853
	.p2align 4,,10
	.p2align 3
.L919:
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rdx, 41704(%rax)
	movl	28(%r14), %eax
	movq	40(%r14), %rdx
.L852:
	leal	1(%rax), %ecx
	movq	(%rdx), %rdx
	leal	16(%rax,%rax), %eax
	movl	%ecx, 28(%r14)
	cltq
	movl	$34, %ecx
	movw	%cx, -1(%rdx,%rax)
	movl	24(%r14), %eax
	cmpl	%eax, 28(%r14)
	jne	.L759
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L770:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L1150
	movq	0(%r13), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L773
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1165:
	movq	15(%rsi), %rsi
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	jne	.L1174
	jmp	.L777
	.p2align 4,,10
	.p2align 3
.L805:
	movq	0(%r13), %rax
	movl	$34, %esi
	leaq	8(%r14), %r12
	movl	11(%rax), %ebx
	movq	40(%r14), %rax
	movq	(%rax), %rdx
	movl	28(%r14), %eax
	leal	1(%rax), %ecx
	leal	16(%rax,%rax), %eax
	cltq
	movl	%ecx, 28(%r14)
	movw	%si, -1(%rdx,%rax)
	movl	24(%r14), %eax
	cmpl	%eax, 28(%r14)
	je	.L1175
.L808:
	cmpl	$16384, %ebx
	jg	.L809
	movl	24(%r14), %eax
	sall	$3, %ebx
	subl	28(%r14), %eax
	cmpl	%eax, %ebx
	jge	.L809
	testl	%ebx, %ebx
	jne	.L1176
.L809:
	movq	(%r14), %rsi
	movq	%r13, %rdx
	leaq	-112(%rbp), %rdi
	xorl	%r15d, %r15d
	leaq	.LC21(%rip), %rbx
	call	_ZN2v88internal16FlatStringReaderC1EPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movl	-76(%rbp), %edx
	testl	%edx, %edx
	jg	.L816
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L777:
	movl	28(%r14), %esi
	movq	40(%r14), %rdi
	call	_ZN2v88internal9SeqString8TruncateENS0_6HandleIS1_EEi@PLT
	leaq	8(%r14), %rdi
	movq	(%rax), %rdx
	movq	40(%r14), %rax
	movq	%rdx, (%rax)
	movl	$1, 16(%r14)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal15JsonStringifier15SerializeStringENS0_6HandleINS0_6StringEEE
.L759:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1177
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1179:
	.cfi_restore_state
	cmpw	$127, %r15w
	je	.L854
	movq	40(%r14), %rdx
	movq	(%rdx), %rcx
	movl	28(%r14), %edx
	leaq	-1(%rcx), %r11
	leal	1(%rdx), %edi
	cmpw	$2047, %r10w
	jbe	.L856
	leal	16(%rdx,%rdx), %eax
	movl	%edi, 28(%r14)
	cltq
	movw	%r15w, -1(%rcx,%rax)
	movl	24(%r14), %eax
	cmpl	%eax, 28(%r14)
	je	.L1178
	.p2align 4,,10
	.p2align 3
.L1157:
	movl	-76(%rbp), %esi
	movl	%ebx, %eax
.L859:
	cmpl	%esi, %eax
	jge	.L919
.L853:
	movq	-72(%rbp), %r8
	movslq	%eax, %rdx
	leal	1(%rax), %ebx
	leaq	(%rdx,%rdx), %r9
	movzwl	(%r8,%rdx,2), %r15d
	cmpw	$34, %r15w
	leal	10240(%r15), %r10d
	seta	%cl
	cmpw	$92, %r15w
	setne	%dl
	testb	%dl, %cl
	jne	.L1179
.L854:
	cmpw	$2047, %r10w
	jbe	.L1180
	salq	$3, %r15
	movl	16(%r14), %edi
	andl	$524280, %r15d
	addq	%r13, %r15
	movzbl	(%r15), %eax
	testl	%edi, %edi
	je	.L910
	testb	%al, %al
	je	.L929
.L1156:
	movl	28(%r14), %edx
.L911:
	movq	40(%r14), %rcx
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %r15
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 28(%r14)
	movw	%ax, -1(%rcx,%rdx)
	movl	28(%r14), %edx
	cmpl	24(%r14), %edx
	je	.L1181
	movzbl	(%r15), %eax
	testb	%al, %al
	jne	.L911
	jmp	.L1157
	.p2align 4,,10
	.p2align 3
.L1183:
	cmpb	$92, %r13b
	je	.L817
	movq	40(%r14), %rax
	movq	(%rax), %rdx
	movl	28(%r14), %eax
	leal	1(%rax), %ecx
	leal	16(%rax,%rax), %eax
	cltq
	movl	%ecx, 28(%r14)
	movw	%r13w, -1(%rdx,%rax)
	movl	-76(%rbp), %edx
	movl	24(%r14), %eax
	cmpl	%eax, 28(%r14)
	je	.L1182
.L819:
	addq	$1, %r15
	cmpl	%r15d, %edx
	jle	.L919
.L816:
	movq	-72(%rbp), %rax
	movzbl	(%rax,%r15), %r13d
	leal	-35(%r13), %eax
	cmpb	$91, %al
	jbe	.L1183
.L817:
	salq	$3, %r13
	movl	16(%r14), %ecx
	andl	$2040, %r13d
	addq	%rbx, %r13
	movzbl	0(%r13), %eax
	testl	%ecx, %ecx
	je	.L820
	testb	%al, %al
	je	.L819
.L1154:
	movl	28(%r14), %edx
.L821:
	movq	40(%r14), %rcx
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %r13
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 28(%r14)
	movw	%ax, -1(%rcx,%rdx)
	movl	28(%r14), %edx
	cmpl	24(%r14), %edx
	je	.L1184
	movzbl	0(%r13), %eax
	testb	%al, %al
	jne	.L821
.L826:
	movl	-76(%rbp), %edx
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L1170:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L1152
	movl	-76(%rbp), %edx
	jmp	.L1185
	.p2align 4,,10
	.p2align 3
.L1184:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	0(%r13), %eax
	testb	%al, %al
	jne	.L1154
	movl	-76(%rbp), %edx
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L793:
	testb	%al, %al
	je	.L792
.L1151:
	movl	28(%r14), %edx
.L795:
	movq	40(%r14), %rcx
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 28(%r14)
	movb	%al, -1(%rcx,%rdx)
	movl	28(%r14), %edx
	cmpl	24(%r14), %edx
	je	.L1186
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L795
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L1186:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L1151
	movl	-76(%rbp), %edx
	jmp	.L1185
	.p2align 4,,10
	.p2align 3
.L1180:
	movq	40(%r14), %rdx
	movq	(%rdx), %rcx
	movl	28(%r14), %edx
	leaq	-1(%rcx), %r11
	leal	1(%rdx), %edi
.L856:
	cmpw	$-9217, %r15w
	ja	.L860
	cmpl	%ebx, %esi
	jle	.L861
	movzwl	2(%r8,%r9), %esi
	leal	9216(%rsi), %r8d
	cmpw	$1023, %r8w
	jbe	.L1187
	movl	16(%r14), %eax
	testl	%eax, %eax
	je	.L925
	movl	$92, %eax
	leaq	.LC20(%rip), %rsi
	jmp	.L866
	.p2align 4,,10
	.p2align 3
.L870:
	movzbl	(%rsi), %eax
	testb	%al, %al
	je	.L867
.L871:
	movq	40(%r14), %rcx
	leal	1(%rdx), %edi
	movq	(%rcx), %rcx
.L866:
	leal	16(%rdx,%rdx), %edx
	movl	%edi, 28(%r14)
	addq	$1, %rsi
	movslq	%edx, %rdx
	movw	%ax, -1(%rcx,%rdx)
	movl	28(%r14), %edx
	cmpl	24(%r14), %edx
	jne	.L870
	movq	%r12, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-136(%rbp), %rsi
	movzbl	(%rsi), %eax
	testb	%al, %al
	je	.L867
	movl	28(%r14), %edx
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L1181:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r15), %eax
	testb	%al, %al
	jne	.L1156
	jmp	.L1157
	.p2align 4,,10
	.p2align 3
.L820:
	testb	%al, %al
	je	.L819
.L1153:
	movl	28(%r14), %edx
.L822:
	movq	40(%r14), %rcx
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %r13
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 28(%r14)
	movb	%al, -1(%rcx,%rdx)
	movl	28(%r14), %edx
	cmpl	24(%r14), %edx
	je	.L1188
	movzbl	0(%r13), %eax
	testb	%al, %al
	jne	.L822
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L1188:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	0(%r13), %eax
	testb	%al, %al
	jne	.L1153
	movl	-76(%rbp), %edx
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L1164:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L761
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L764
	movq	23(%rax), %rdx
	movl	11(%rdx), %r10d
	testl	%r10d, %r10d
	je	.L764
	movq	%r13, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L910:
	testb	%al, %al
	je	.L929
.L1155:
	movl	28(%r14), %edx
.L912:
	movq	40(%r14), %rcx
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %r15
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 28(%r14)
	movb	%al, -1(%rcx,%rdx)
	movl	28(%r14), %edx
	cmpl	24(%r14), %edx
	je	.L1189
	movzbl	(%r15), %eax
	testb	%al, %al
	jne	.L912
	jmp	.L1157
	.p2align 4,,10
	.p2align 3
.L1189:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r15), %eax
	testb	%al, %al
	jne	.L1155
	jmp	.L1157
	.p2align 4,,10
	.p2align 3
.L860:
	movl	16(%r14), %r9d
	movl	$92, %eax
	leaq	.LC20(%rip), %rsi
	testl	%r9d, %r9d
	jne	.L896
	movl	$92, %esi
	leaq	.LC20(%rip), %rax
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L898:
	movzbl	(%rax), %esi
	testb	%sil, %sil
	je	.L897
.L899:
	movq	40(%r14), %rcx
	leal	1(%rdx), %edi
	movq	(%rcx), %rcx
.L895:
	addl	$16, %edx
	movl	%edi, 28(%r14)
	addq	$1, %rax
	movslq	%edx, %rdx
	movb	%sil, -1(%rcx,%rdx)
	movl	28(%r14), %edx
	cmpl	24(%r14), %edx
	jne	.L898
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-136(%rbp), %rax
	movzbl	(%rax), %esi
	testb	%sil, %sil
	je	.L897
	movl	28(%r14), %edx
	jmp	.L899
	.p2align 4,,10
	.p2align 3
.L900:
	movzbl	(%rsi), %eax
	testb	%al, %al
	je	.L897
.L901:
	movq	40(%r14), %rcx
	leal	1(%rdx), %edi
	movq	(%rcx), %rcx
.L896:
	leal	16(%rdx,%rdx), %edx
	movl	%edi, 28(%r14)
	addq	$1, %rsi
	movslq	%edx, %rdx
	movw	%ax, -1(%rcx,%rdx)
	movl	28(%r14), %edx
	cmpl	24(%r14), %edx
	jne	.L900
	movq	%r12, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-136(%rbp), %rsi
	movzbl	(%rsi), %eax
	testb	%al, %al
	je	.L897
	movl	28(%r14), %edx
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L897:
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	cvtsi2sdl	%r15d, %xmm0
	call	_ZN2v88internal20DoubleToRadixCStringEdi@PLT
	movl	16(%r14), %r8d
	movq	%rax, %r15
	testl	%r8d, %r8d
	jne	.L1190
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	je	.L873
	movl	28(%r14), %edx
	.p2align 4,,10
	.p2align 3
.L905:
	movq	40(%r14), %rsi
	leal	1(%rdx), %edi
	addl	$16, %edx
	addq	$1, %rax
	movslq	%edx, %rdx
	movq	(%rsi), %rsi
	movl	%edi, 28(%r14)
	movb	%cl, -1(%rsi,%rdx)
	movl	28(%r14), %edx
	cmpl	24(%r14), %edx
	je	.L1191
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	jne	.L905
	.p2align 4,,10
	.p2align 3
.L873:
	movq	%r15, %rdi
	call	_ZdaPv@PLT
	jmp	.L1157
	.p2align 4,,10
	.p2align 3
.L1190:
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	je	.L873
	movl	28(%r14), %edx
	.p2align 4,,10
	.p2align 3
.L904:
	movq	40(%r14), %rsi
	leal	1(%rdx), %edi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %rax
	movslq	%edx, %rdx
	movq	(%rsi), %rsi
	movl	%edi, 28(%r14)
	movw	%cx, -1(%rsi,%rdx)
	movl	28(%r14), %edx
	cmpl	24(%r14), %edx
	je	.L1192
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	jne	.L904
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L861:
	movl	16(%r14), %r11d
	movl	$92, %eax
	leaq	.LC20(%rip), %rsi
	testl	%r11d, %r11d
	jne	.L881
	movl	$92, %esi
	leaq	.LC20(%rip), %rax
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L883:
	movzbl	(%rax), %esi
	testb	%sil, %sil
	je	.L882
.L884:
	movq	40(%r14), %rcx
	leal	1(%rdx), %edi
	movq	(%rcx), %rcx
.L880:
	addl	$16, %edx
	movl	%edi, 28(%r14)
	addq	$1, %rax
	movslq	%edx, %rdx
	movb	%sil, -1(%rcx,%rdx)
	movl	28(%r14), %edx
	cmpl	24(%r14), %edx
	jne	.L883
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-136(%rbp), %rax
	movzbl	(%rax), %esi
	testb	%sil, %sil
	je	.L882
	movl	28(%r14), %edx
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L885:
	movzbl	(%rsi), %eax
	testb	%al, %al
	je	.L882
.L886:
	movq	40(%r14), %rcx
	leal	1(%rdx), %edi
	movq	(%rcx), %rcx
.L881:
	leal	16(%rdx,%rdx), %edx
	movl	%edi, 28(%r14)
	addq	$1, %rsi
	movslq	%edx, %rdx
	movw	%ax, -1(%rcx,%rdx)
	movl	28(%r14), %edx
	cmpl	24(%r14), %edx
	jne	.L885
	movq	%r12, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-136(%rbp), %rsi
	movzbl	(%rsi), %eax
	testb	%al, %al
	je	.L882
	movl	28(%r14), %edx
	jmp	.L886
	.p2align 4,,10
	.p2align 3
.L773:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L1193
.L775:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L769
.L1175:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L808
.L1172:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L1191:
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-136(%rbp), %rax
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	je	.L873
	movl	28(%r14), %edx
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L1192:
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-136(%rbp), %rax
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	je	.L873
	movl	28(%r14), %edx
	jmp	.L904
	.p2align 4,,10
	.p2align 3
.L882:
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	cvtsi2sdl	%r15d, %xmm0
	call	_ZN2v88internal20DoubleToRadixCStringEdi@PLT
	movl	16(%r14), %r10d
	movq	%rax, %r15
	testl	%r10d, %r10d
	je	.L887
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	je	.L873
	movl	28(%r14), %edx
	.p2align 4,,10
	.p2align 3
.L889:
	movq	40(%r14), %rsi
	leal	1(%rdx), %edi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %rax
	movslq	%edx, %rdx
	movq	(%rsi), %rsi
	movl	%edi, 28(%r14)
	movw	%cx, -1(%rsi,%rdx)
	movl	28(%r14), %edx
	cmpl	24(%r14), %edx
	je	.L1194
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	jne	.L889
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L1168:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	-76(%rbp), %edx
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L1194:
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-136(%rbp), %rax
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	je	.L873
	movl	28(%r14), %edx
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L867:
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	cvtsi2sdl	%r15d, %xmm0
	call	_ZN2v88internal20DoubleToRadixCStringEdi@PLT
	movq	%rax, %r15
	movl	16(%r14), %eax
	testl	%eax, %eax
	je	.L872
	movzbl	(%r15), %ecx
	testb	%cl, %cl
	je	.L873
	movl	28(%r14), %edx
	movq	%r15, %rax
	.p2align 4,,10
	.p2align 3
.L874:
	movq	40(%r14), %rsi
	leal	1(%rdx), %edi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %rax
	movslq	%edx, %rdx
	movq	(%rsi), %rsi
	movl	%edi, 28(%r14)
	movw	%cx, -1(%rsi,%rdx)
	movl	28(%r14), %edx
	cmpl	24(%r14), %edx
	je	.L1195
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	jne	.L874
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L1182:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	-76(%rbp), %edx
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L764:
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L766
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
	jmp	.L761
	.p2align 4,,10
	.p2align 3
.L1195:
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-136(%rbp), %rax
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	je	.L873
	movl	28(%r14), %edx
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L887:
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	je	.L873
	movl	28(%r14), %edx
	.p2align 4,,10
	.p2align 3
.L890:
	movq	40(%r14), %rsi
	leal	1(%rdx), %edi
	addl	$16, %edx
	addq	$1, %rax
	movslq	%edx, %rdx
	movq	(%rsi), %rsi
	movl	%edi, 28(%r14)
	movb	%cl, -1(%rsi,%rdx)
	movl	28(%r14), %edx
	cmpl	24(%r14), %edx
	je	.L1196
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	jne	.L890
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L1196:
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-136(%rbp), %rax
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	je	.L873
	movl	28(%r14), %edx
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L929:
	movl	%ebx, %eax
	jmp	.L859
.L1166:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L781
.L872:
	movzbl	(%r15), %ecx
	testb	%cl, %cl
	je	.L873
	movl	28(%r14), %edx
	movq	%r15, %rax
	.p2align 4,,10
	.p2align 3
.L875:
	movq	40(%r14), %rsi
	leal	1(%rdx), %edi
	addl	$16, %edx
	addq	$1, %rax
	movslq	%edx, %rdx
	movq	(%rsi), %rsi
	movl	%edi, 28(%r14)
	movb	%cl, -1(%rsi,%rdx)
	movl	28(%r14), %edx
	cmpl	24(%r14), %edx
	je	.L1197
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	jne	.L875
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L1197:
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-136(%rbp), %rax
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	je	.L873
	movl	28(%r14), %edx
	jmp	.L875
.L925:
	movl	$92, %esi
	leaq	.LC20(%rip), %rax
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L868:
	movzbl	(%rax), %esi
	testb	%sil, %sil
	je	.L867
.L869:
	movq	40(%r14), %rcx
	leal	1(%rdx), %edi
	movq	(%rcx), %rcx
.L865:
	addl	$16, %edx
	movl	%edi, 28(%r14)
	addq	$1, %rax
	movslq	%edx, %rdx
	movb	%sil, -1(%rcx,%rdx)
	movl	28(%r14), %edx
	cmpl	24(%r14), %edx
	jne	.L868
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-136(%rbp), %rax
	movzbl	(%rax), %esi
	testb	%sil, %sil
	je	.L867
	movl	28(%r14), %edx
	jmp	.L869
.L1187:
	leal	16(%rdx,%rdx), %edx
	movl	%edi, 28(%r14)
	movslq	%edx, %rdx
	movw	%r15w, (%rdx,%r11)
	movl	28(%r14), %edx
	cmpl	24(%r14), %edx
	je	.L1198
.L863:
	movq	40(%r14), %rcx
	leal	1(%rdx), %edi
	leal	16(%rdx,%rdx), %edx
	addl	$2, %eax
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%edi, 28(%r14)
	movw	%si, -1(%rcx,%rdx)
	movl	24(%r14), %edi
	cmpl	%edi, 28(%r14)
	je	.L864
	movl	-76(%rbp), %esi
	jmp	.L859
.L1178:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	-76(%rbp), %esi
	movl	%ebx, %eax
	jmp	.L859
.L766:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L1199
.L768:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L761
.L1167:
	movq	0(%r13), %rax
	leaq	-113(%rbp), %rsi
	leaq	-112(%rbp), %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movslq	28(%r14), %rsi
	movq	%rdx, %rcx
	movq	40(%r14), %rdx
	movq	%rax, %rdi
	movq	%rsi, %rax
	movq	(%rdx), %r8
	leaq	15(%r8,%rsi), %r8
	testl	%ecx, %ecx
	jle	.L783
	leal	-1(%rcx), %eax
	leaq	.LC21(%rip), %r10
	leaq	1(%rdi,%rax), %r9
	movq	%r8, %rax
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L1201:
	cmpb	$92, %dl
	je	.L784
	movb	%dl, (%rax)
	addq	$1, %rax
.L785:
	addq	$1, %rdi
	cmpq	%rdi, %r9
	je	.L1200
.L787:
	movzbl	(%rdi), %edx
	leal	-35(%rdx), %ecx
	cmpb	$91, %cl
	jbe	.L1201
.L784:
	salq	$3, %rdx
	andl	$2040, %edx
	addq	%r10, %rdx
	movzbl	(%rdx), %esi
	testb	%sil, %sil
	je	.L785
	leaq	1(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L786:
	addq	$1, %rdx
	movb	%sil, -1(%rcx)
	movq	%rcx, %rax
	addq	$1, %rcx
	movzbl	(%rdx), %esi
	testb	%sil, %sil
	jne	.L786
	jmp	.L785
.L1173:
	movq	0(%r13), %rax
	leaq	-112(%rbp), %rdi
	leaq	-113(%rbp), %rsi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movslq	28(%r14), %r9
	movq	%rdx, %rdi
	movl	%edx, %r15d
	movq	40(%r14), %rdx
	movq	%rax, %r13
	movq	%r9, %rax
	movq	(%rdx), %r8
	leaq	15(%r8,%r9,2), %rbx
	movq	%rbx, -144(%rbp)
	testl	%edi, %edi
	jle	.L832
	xorl	%edx, %edx
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L1203:
	cmpw	$127, %ax
	je	.L833
	cmpw	$2047, %di
	jbe	.L834
	movw	%ax, (%rbx)
	movl	%r8d, %edx
	addq	$2, %rbx
.L835:
	cmpl	%edx, %r15d
	jle	.L1202
.L851:
	movslq	%edx, %rax
	leal	1(%rdx), %r8d
	leaq	(%rax,%rax), %r10
	movzwl	0(%r13,%rax,2), %eax
	cmpw	$34, %ax
	leal	10240(%rax), %edi
	seta	%r11b
	cmpw	$92, %ax
	setne	%r9b
	testb	%r9b, %r11b
	jne	.L1203
.L833:
	cmpw	$2047, %di
	ja	.L836
.L834:
	cmpw	$-9217, %ax
	ja	.L837
	cmpl	%r8d, %r15d
	jle	.L838
	movzwl	2(%r13,%r10), %edi
	leal	9216(%rdi), %r9d
	cmpw	$1023, %r9w
	ja	.L839
	movw	%ax, (%rbx)
	addl	$2, %edx
	addq	$4, %rbx
	movw	%di, -2(%rbx)
	jmp	.L835
.L1176:
	movq	0(%r13), %rax
	leaq	-113(%rbp), %rsi
	leaq	-112(%rbp), %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movslq	28(%r14), %r8
	movq	%rdx, %rcx
	movq	40(%r14), %rdx
	movq	%rax, %rsi
	movq	%r8, %rax
	movq	(%rdx), %rdi
	leaq	15(%rdi,%r8,2), %r9
	testl	%ecx, %ecx
	jle	.L832
	leal	-1(%rcx), %eax
	leaq	.LC21(%rip), %r8
	movq	%r9, %rcx
	leaq	1(%rsi,%rax), %rdi
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L1205:
	cmpb	$92, %al
	je	.L811
	movw	%ax, (%rcx)
	addq	$2, %rcx
.L812:
	addq	$1, %rsi
	cmpq	%rsi, %rdi
	je	.L1204
.L814:
	movzbl	(%rsi), %eax
	leal	-35(%rax), %edx
	cmpb	$91, %dl
	jbe	.L1205
.L811:
	salq	$3, %rax
	andl	$2040, %eax
	addq	%r8, %rax
	movzbl	(%rax), %edx
	testb	%dl, %dl
	je	.L812
	leaq	2(%rcx), %r10
	.p2align 4,,10
	.p2align 3
.L813:
	addq	$1, %rax
	movw	%dx, -2(%r10)
	movq	%r10, %rcx
	addq	$2, %r10
	movzbl	(%rax), %edx
	testb	%dl, %dl
	jne	.L813
	jmp	.L812
.L864:
	movq	%r12, %rdi
	movl	%eax, -136(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	-76(%rbp), %esi
	movl	-136(%rbp), %eax
	jmp	.L859
.L1198:
	movq	%r12, %rdi
	movl	%eax, -144(%rbp)
	movl	%esi, -136(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	28(%r14), %edx
	movl	-144(%rbp), %eax
	movl	-136(%rbp), %esi
	jmp	.L863
.L1199:
	movq	%r12, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L768
.L837:
	leaq	2(%rbx), %rdi
	movl	$92, %edx
	leaq	.LC20(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L847:
	addq	$1, %r9
	movw	%dx, -2(%rdi)
	movq	%rdi, %rbx
	addq	$2, %rdi
	movzbl	(%r9), %edx
	testb	%dl, %dl
	jne	.L847
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movl	%r8d, -136(%rbp)
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal20DoubleToRadixCStringEdi@PLT
	movl	-136(%rbp), %r8d
	movq	%rax, %r9
	movzbl	(%rax), %eax
	testb	%al, %al
	je	.L848
	leaq	2(%rbx), %rdx
	movq	%r9, %rdi
	.p2align 4,,10
	.p2align 3
.L849:
	addq	$1, %rdi
	movw	%ax, -2(%rdx)
	movq	%rdx, %rbx
	addq	$2, %rdx
	movzbl	(%rdi), %eax
	testb	%al, %al
	jne	.L849
.L848:
	movq	%r9, %rdi
	movl	%r8d, -136(%rbp)
	call	_ZdaPv@PLT
	movl	-136(%rbp), %r8d
	movl	%r8d, %edx
	jmp	.L835
.L1202:
	movq	%rbx, %rax
	movq	40(%r14), %rdx
	subq	-144(%rbp), %rax
	sarq	%rax
	addl	28(%r14), %eax
.L832:
	movl	%eax, 28(%r14)
	jmp	.L852
.L836:
	salq	$3, %rax
	leaq	.LC21(%rip), %rdi
	andl	$524280, %eax
	leaq	(%rax,%rdi), %rdx
	movzbl	(%rdx), %eax
	testb	%al, %al
	je	.L923
	leaq	2(%rbx), %rdi
	.p2align 4,,10
	.p2align 3
.L850:
	addq	$1, %rdx
	movw	%ax, -2(%rdi)
	movq	%rdi, %rbx
	leaq	2(%rdi), %rdi
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L850
.L923:
	movl	%r8d, %edx
	jmp	.L835
.L1200:
	movq	40(%r14), %rdx
	subq	%r8, %rax
	addl	28(%r14), %eax
.L783:
	movl	%eax, 28(%r14)
	jmp	.L788
.L838:
	leaq	2(%rbx), %rcx
	movl	$92, %edx
	leaq	.LC20(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L844:
	addq	$1, %rsi
	movw	%dx, -2(%rcx)
	movq	%rcx, %rbx
	addq	$2, %rcx
	movzbl	(%rsi), %edx
	testb	%dl, %dl
	jne	.L844
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal20DoubleToRadixCStringEdi@PLT
	movq	%rax, %rdi
	movzbl	(%rax), %eax
	testb	%al, %al
	je	.L845
	leaq	2(%rbx), %rdx
	movq	%rdi, %rcx
	.p2align 4,,10
	.p2align 3
.L846:
	addq	$1, %rcx
	movw	%ax, -2(%rdx)
	movq	%rdx, %rbx
	addq	$2, %rdx
	movzbl	(%rcx), %eax
	testb	%al, %al
	jne	.L846
.L845:
	call	_ZdaPv@PLT
	movl	28(%r14), %eax
	subq	-144(%rbp), %rbx
	sarq	%rbx
	movq	40(%r14), %rdx
	addl	%ebx, %eax
	jmp	.L832
.L1204:
	movq	%rcx, %rax
	movq	40(%r14), %rdx
	subq	%r9, %rax
	sarq	%rax
	addl	28(%r14), %eax
	jmp	.L832
.L1193:
	movq	%r12, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L775
.L839:
	leaq	2(%rbx), %rdi
	movl	$92, %edx
	leaq	.LC20(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L840:
	addq	$1, %r9
	movw	%dx, -2(%rdi)
	movq	%rdi, %rbx
	addq	$2, %rdi
	movzbl	(%r9), %edx
	testb	%dl, %dl
	jne	.L840
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movl	%r8d, -136(%rbp)
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal20DoubleToRadixCStringEdi@PLT
	movl	-136(%rbp), %r8d
	movq	%rax, %r9
	movzbl	(%rax), %eax
	testb	%al, %al
	je	.L841
	leaq	2(%rbx), %rdx
	movq	%r9, %rdi
	.p2align 4,,10
	.p2align 3
.L842:
	addq	$1, %rdi
	movw	%ax, -2(%rdx)
	movq	%rdx, %rbx
	addq	$2, %rdx
	movzbl	(%rdi), %eax
	testb	%al, %al
	jne	.L842
.L841:
	movq	%r9, %rdi
	movl	%r8d, -136(%rbp)
	call	_ZdaPv@PLT
	movl	-136(%rbp), %r8d
	movl	%r8d, %edx
	jmp	.L851
.L1177:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18007:
	.size	_ZN2v88internal15JsonStringifier15SerializeStringENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal15JsonStringifier15SerializeStringENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal11HandleScope14CloseAndEscapeINS0_6ObjectEEENS0_6HandleIT_EES6_,"axG",@progbits,_ZN2v88internal11HandleScope14CloseAndEscapeINS0_6ObjectEEENS0_6HandleIT_EES6_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11HandleScope14CloseAndEscapeINS0_6ObjectEEENS0_6HandleIT_EES6_
	.type	_ZN2v88internal11HandleScope14CloseAndEscapeINS0_6ObjectEEENS0_6HandleIT_EES6_, @function
_ZN2v88internal11HandleScope14CloseAndEscapeINS0_6ObjectEEENS0_6HandleIT_EES6_:
.LFB19896:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rbx
	movq	8(%rdi), %rdx
	movq	(%rsi), %r14
	movq	16(%rdi), %rax
	subl	$1, 41104(%rbx)
	movq	%rdx, 41088(%rbx)
	cmpq	41096(%rbx), %rax
	je	.L1207
	movq	%rax, 41096(%rbx)
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1207:
	movq	(%r12), %r13
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1208
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1209:
	movq	41088(%rbx), %rdx
	movq	%rdx, 8(%r12)
	movq	41096(%rbx), %rdx
	movq	%rdx, 16(%r12)
	addl	$1, 41104(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1208:
	.cfi_restore_state
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L1212
.L1210:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%r14, (%rax)
	jmp	.L1209
	.p2align 4,,10
	.p2align 3
.L1212:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1210
	.cfi_endproc
.LFE19896:
	.size	_ZN2v88internal11HandleScope14CloseAndEscapeINS0_6ObjectEEENS0_6HandleIT_EES6_, .-_ZN2v88internal11HandleScope14CloseAndEscapeINS0_6ObjectEEENS0_6HandleIT_EES6_
	.section	.text._ZN2v88internal15JsonStringifier19ApplyToJsonFunctionENS0_6HandleINS0_6ObjectEEES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JsonStringifier19ApplyToJsonFunctionENS0_6HandleINS0_6ObjectEEES4_
	.type	_ZN2v88internal15JsonStringifier19ApplyToJsonFunctionENS0_6HandleINS0_6ObjectEEES4_, @function
_ZN2v88internal15JsonStringifier19ApplyToJsonFunctionENS0_6HandleINS0_6ObjectEEES4_:
.LFB17970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rdx, -184(%rbp)
	movq	41088(%rax), %rdx
	addl	$1, 41104(%rax)
	movq	48(%rdi), %r15
	movq	(%rdi), %r14
	movq	%rax, -176(%rbp)
	movq	%rdx, -168(%rbp)
	movq	41096(%rax), %rdx
	movq	%rdx, -160(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L1214
.L1216:
	movl	$-1, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %rcx
.L1215:
	movq	(%r15), %rdx
	movl	$2, %eax
	movq	-1(%rdx), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L1217
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L1217:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	%r14, -120(%rbp)
	movq	(%r15), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L1237
.L1218:
	movq	%r15, -112(%rbp)
	leaq	-144(%rbp), %r15
	movq	%r15, %rdi
	movq	%rcx, -80(%rbp)
	movq	$0, -104(%rbp)
	movq	%r12, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movq	%r15, %rdi
	movl	$1, %esi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1236
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L1238
.L1220:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1225
	movq	-168(%rbp), %rdx
	subl	$1, 41104(%rdi)
	movq	-160(%rbp), %rax
	movq	%rdx, 41088(%rdi)
	cmpq	41096(%rdi), %rax
	je	.L1225
	movq	%rax, 41096(%rdi)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1225:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1239
	addq	$152, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1214:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1216
	movq	%rsi, %rcx
	jmp	.L1215
	.p2align 4,,10
	.p2align 3
.L1238:
	movq	-1(%rax), %rax
	testb	$2, 13(%rax)
	je	.L1220
	movq	(%rbx), %rdi
	testb	$1, 0(%r13)
	je	.L1240
.L1223:
	movq	%r15, %rsi
	leaq	-64(%rbp), %r8
	movl	$1, %ecx
	movq	%r12, %rdx
	movq	-184(%rbp), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1236
	leaq	-176(%rbp), %rdi
	call	_ZN2v88internal11HandleScope14CloseAndEscapeINS0_6ObjectEEENS0_6HandleIT_EES6_
	movq	%rax, %r12
	jmp	.L1220
	.p2align 4,,10
	.p2align 3
.L1237:
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rcx, -192(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-192(%rbp), %rcx
	movq	%rax, %r15
	jmp	.L1218
	.p2align 4,,10
	.p2align 3
.L1236:
	xorl	%r12d, %r12d
	jmp	.L1220
	.p2align 4,,10
	.p2align 3
.L1240:
	movl	$1, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	(%rbx), %rdi
	movq	%rax, -184(%rbp)
	jmp	.L1223
.L1239:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17970:
	.size	_ZN2v88internal15JsonStringifier19ApplyToJsonFunctionENS0_6HandleINS0_6ObjectEEES4_, .-_ZN2v88internal15JsonStringifier19ApplyToJsonFunctionENS0_6HandleINS0_6ObjectEEES4_
	.section	.text._ZN2v88internal15JsonStringifier21ApplyReplacerFunctionENS0_6HandleINS0_6ObjectEEES4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JsonStringifier21ApplyReplacerFunctionENS0_6HandleINS0_6ObjectEEES4_S4_
	.type	_ZN2v88internal15JsonStringifier21ApplyReplacerFunctionENS0_6HandleINS0_6ObjectEEES4_S4_, @function
_ZN2v88internal15JsonStringifier21ApplyReplacerFunctionENS0_6HandleINS0_6ObjectEEES4_S4_:
.LFB17971:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	41088(%rax), %rdx
	addl	$1, 41104(%rax)
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	41096(%rax), %rdx
	movq	%rdx, -80(%rbp)
	testb	$1, (%rsi)
	je	.L1253
.L1242:
	movq	%r12, %xmm1
	movq	%rsi, %xmm0
	movq	%r13, %rdx
	movq	%r12, %rsi
	punpcklqdq	%xmm1, %xmm0
	movq	%rbx, %rdi
	xorl	%r12d, %r12d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal15JsonStringifier13CurrentHolderENS0_6HandleINS0_6ObjectEEES4_
	movq	64(%rbx), %rsi
	movq	(%rbx), %rdi
	leaq	-64(%rbp), %r8
	movq	%rax, %rdx
	movl	$2, %ecx
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1244
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal11HandleScope14CloseAndEscapeINS0_6ObjectEEENS0_6HandleIT_EES6_
	movq	%rax, %r12
.L1244:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1246
	movq	-88(%rbp), %rdx
	subl	$1, 41104(%rdi)
	movq	-80(%rbp), %rax
	movq	%rdx, 41088(%rdi)
	cmpq	41096(%rdi), %rax
	je	.L1246
	movq	%rax, 41096(%rdi)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1246:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1254
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1253:
	.cfi_restore_state
	movq	(%rdi), %rdi
	movl	$1, %edx
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%rax, %rsi
	jmp	.L1242
.L1254:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17971:
	.size	_ZN2v88internal15JsonStringifier21ApplyReplacerFunctionENS0_6HandleINS0_6ObjectEEES4_S4_, .-_ZN2v88internal15JsonStringifier21ApplyReplacerFunctionENS0_6HandleINS0_6ObjectEEES4_S4_
	.section	.rodata._ZNSt6vectorISt4pairIN2v88internal6HandleINS2_6ObjectEEES5_ESaIS6_EE17_M_realloc_insertIJRS5_SA_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_.str1.1,"aMS",@progbits,1
.LC22:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorISt4pairIN2v88internal6HandleINS2_6ObjectEEES5_ESaIS6_EE17_M_realloc_insertIJRS5_SA_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt4pairIN2v88internal6HandleINS2_6ObjectEEES5_ESaIS6_EE17_M_realloc_insertIJRS5_SA_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairIN2v88internal6HandleINS2_6ObjectEEES5_ESaIS6_EE17_M_realloc_insertIJRS5_SA_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	.type	_ZNSt6vectorISt4pairIN2v88internal6HandleINS2_6ObjectEEES5_ESaIS6_EE17_M_realloc_insertIJRS5_SA_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_, @function
_ZNSt6vectorISt4pairIN2v88internal6HandleINS2_6ObjectEEES5_ESaIS6_EE17_M_realloc_insertIJRS5_SA_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_:
.LFB20985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movabsq	$576460752303423487, %rsi
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %r8
	movq	(%rdi), %r13
	movq	%r8, %rax
	subq	%r13, %rax
	sarq	$4, %rax
	cmpq	%rsi, %rax
	je	.L1276
	movq	%r12, %r9
	movq	%rdi, %r15
	subq	%r13, %r9
	testq	%rax, %rax
	je	.L1267
	movabsq	$9223372036854775792, %r14
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L1277
.L1257:
	movq	%r14, %rdi
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rcx
	leaq	(%rax,%r14), %rsi
	movq	%rax, %rbx
	leaq	16(%rax), %r14
.L1266:
	movq	(%rdx), %xmm0
	movhps	(%rcx), %xmm0
	movups	%xmm0, (%rbx,%r9)
	cmpq	%r13, %r12
	je	.L1259
	leaq	-16(%r12), %rdi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	subq	%r13, %rdi
	movq	%rdi, %rax
	shrq	$4, %rax
	addq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L1260:
	movdqu	0(%r13,%rdx), %xmm1
	addq	$1, %rcx
	movups	%xmm1, (%rbx,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rax
	ja	.L1260
	leaq	32(%rbx,%rdi), %r14
	cmpq	%r8, %r12
	je	.L1261
.L1262:
	subq	%r12, %r8
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	leaq	-16(%r8), %rdi
	movq	%rdi, %rax
	shrq	$4, %rax
	addq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L1264:
	movdqu	(%r12,%rdx), %xmm2
	addq	$1, %rcx
	movups	%xmm2, (%r14,%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rcx
	jb	.L1264
	leaq	16(%r14,%rdi), %r14
.L1263:
	testq	%r13, %r13
	jne	.L1261
.L1265:
	movq	%rbx, %xmm0
	movq	%r14, %xmm3
	movq	%rsi, 16(%r15)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1261:
	.cfi_restore_state
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1277:
	testq	%rdi, %rdi
	jne	.L1258
	movl	$16, %r14d
	xorl	%esi, %esi
	xorl	%ebx, %ebx
	jmp	.L1266
	.p2align 4,,10
	.p2align 3
.L1267:
	movl	$16, %r14d
	jmp	.L1257
	.p2align 4,,10
	.p2align 3
.L1259:
	cmpq	%r8, %r12
	jne	.L1262
	jmp	.L1263
.L1258:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	salq	$4, %rsi
	movq	%rsi, %r14
	jmp	.L1257
.L1276:
	leaq	.LC22(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE20985:
	.size	_ZNSt6vectorISt4pairIN2v88internal6HandleINS2_6ObjectEEES5_ESaIS6_EE17_M_realloc_insertIJRS5_SA_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_, .-_ZNSt6vectorISt4pairIN2v88internal6HandleINS2_6ObjectEEES5_ESaIS6_EE17_M_realloc_insertIJRS5_SA_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	.section	.text._ZN2v88internal15JsonStringifier9StackPushENS0_6HandleINS0_6ObjectEEES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JsonStringifier9StackPushENS0_6HandleINS0_6ObjectEEES4_
	.type	_ZN2v88internal15JsonStringifier9StackPushENS0_6HandleINS0_6ObjectEEES4_, @function
_ZN2v88internal15JsonStringifier9StackPushENS0_6HandleINS0_6ObjectEEES4_:
.LFB17973:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %r12
	movq	%rsi, -24(%rbp)
	movq	%rdx, -32(%rbp)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r12), %rax
	jb	.L1290
	movq	96(%rbx), %r8
	movq	88(%rbx), %rsi
	movq	%r8, %rcx
	subq	%rsi, %rcx
	sarq	$4, %rcx
	je	.L1281
	movq	-24(%rbp), %rax
	xorl	%edx, %edx
	movq	(%rax), %rdi
	jmp	.L1283
	.p2align 4,,10
	.p2align 3
.L1282:
	addq	$1, %rdx
	cmpq	%rdx, %rcx
	je	.L1281
.L1283:
	movq	%rdx, %rax
	salq	$4, %rax
	movq	8(%rsi,%rax), %rax
	cmpq	(%rax), %rdi
	jne	.L1282
	movq	-32(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal15JsonStringifier38ConstructCircularStructureErrorMessageENS0_6HandleINS0_6ObjectEEEm
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	movl	$35, %esi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	(%rbx), %rdi
	xorl	%edx, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movl	$2, %eax
.L1278:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1281:
	.cfi_restore_state
	cmpq	%r8, 104(%rbx)
	je	.L1284
	movq	-32(%rbp), %xmm0
	movl	$1, %eax
	movhps	-24(%rbp), %xmm0
	movups	%xmm0, (%r8)
	addq	$16, 96(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1290:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	addq	$16, %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1284:
	.cfi_restore_state
	leaq	-24(%rbp), %rcx
	leaq	-32(%rbp), %rdx
	movq	%r8, %rsi
	leaq	88(%rbx), %rdi
	call	_ZNSt6vectorISt4pairIN2v88internal6HandleINS2_6ObjectEEES5_ESaIS6_EE17_M_realloc_insertIJRS5_SA_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	movl	$1, %eax
	jmp	.L1278
	.cfi_endproc
.LFE17973:
	.size	_ZN2v88internal15JsonStringifier9StackPushENS0_6HandleINS0_6ObjectEEES4_, .-_ZN2v88internal15JsonStringifier9StackPushENS0_6HandleINS0_6ObjectEEES4_
	.section	.rodata._ZN2v88internal15JsonStringifier10Serialize_ILb1EEENS1_6ResultENS0_6HandleINS0_6ObjectEEEbS6_.str1.1,"aMS",@progbits,1
.LC23:
	.string	"false"
.LC24:
	.string	"true"
	.section	.rodata._ZN2v88internal15JsonStringifier10Serialize_ILb1EEENS1_6ResultENS0_6HandleINS0_6ObjectEEEbS6_.str1.8,"aMS",@progbits,1
	.align 8
.LC25:
	.string	"object->length().ToArrayLength(&length)"
	.section	.text._ZN2v88internal15JsonStringifier10Serialize_ILb1EEENS1_6ResultENS0_6HandleINS0_6ObjectEEEbS6_,"axG",@progbits,_ZN2v88internal15JsonStringifier10Serialize_ILb1EEENS1_6ResultENS0_6HandleINS0_6ObjectEEEbS6_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15JsonStringifier10Serialize_ILb1EEENS1_6ResultENS0_6HandleINS0_6ObjectEEEbS6_
	.type	_ZN2v88internal15JsonStringifier10Serialize_ILb1EEENS1_6ResultENS0_6HandleINS0_6ObjectEEEbS6_, @function
_ZN2v88internal15JsonStringifier10Serialize_ILb1EEENS1_6ResultENS0_6HandleINS0_6ObjectEEEbS6_:
.LFB19873:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -72(%rbp)
	movq	(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	%rax, %r9
	movq	37544(%rbx), %rax
	cmpq	%rax, %r9
	jb	.L2230
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L2231
.L1300:
	cmpq	$0, 64(%r15)
	je	.L1302
	movq	-72(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rcx
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier21ApplyReplacerFunctionENS0_6HandleINS0_6ObjectEEES4_S4_
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2221
.L1302:
	movq	(%r12), %rax
	movq	%rax, %rbx
	notq	%rbx
	movl	%ebx, %edi
	andl	$1, %edi
	movb	%dil, -80(%rbp)
	je	.L1304
	leaq	8(%r15), %r13
	testb	%r14b, %r14b
	je	.L1306
	movl	28(%r15), %eax
	movl	16(%r15), %r10d
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%r10d, %r10d
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L1307
	addl	$16, %eax
	cltq
	movb	$44, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2140
	.p2align 4,,10
	.p2align 3
.L1306:
	cmpq	$0, 72(%r15)
	je	.L1315
	movl	28(%r15), %eax
	movl	16(%r15), %edi
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%edi, %edi
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L1312
	addl	$16, %eax
	cltq
	movb	$10, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2141
.L1314:
	movl	80(%r15), %eax
	testl	%eax, %eax
	jle	.L1315
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1316:
	movq	72(%r15), %r14
	movl	16(%r15), %ecx
	movzwl	(%r14), %edx
	testl	%ecx, %ecx
	je	.L1318
	testw	%dx, %dx
	je	.L1319
.L2143:
	movl	28(%r15), %eax
.L1320:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$2, %r14
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L2232
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L1320
.L1325:
	movl	80(%r15), %eax
.L1319:
	addl	$1, %ebx
	cmpl	%eax, %ebx
	jl	.L1316
.L1315:
	movq	-72(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier15SerializeStringENS0_6HandleINS0_6StringEEE
	movl	16(%r15), %r8d
	testl	%r8d, %r8d
	je	.L1310
	movq	40(%r15), %rax
	movl	$58, %r14d
	movq	(%rax), %rdx
	movl	28(%r15), %eax
	leal	1(%rax), %ecx
	leal	16(%rax,%rax), %eax
	cltq
	movl	%ecx, 28(%r15)
	movw	%r14w, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2144
.L1329:
	cmpq	$0, 72(%r15)
	je	.L1332
	movl	28(%r15), %eax
	movl	16(%r15), %ebx
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%ebx, %ebx
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L1333
	addl	$16, %eax
	cltq
	movb	$32, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2145
.L1332:
	movq	(%r12), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier12SerializeSmiENS0_3SmiE
	movl	%eax, %ebx
	jmp	.L1291
	.p2align 4,,10
	.p2align 3
.L1310:
	movq	40(%r15), %rax
	movq	(%rax), %rdx
	movl	28(%r15), %eax
	leal	1(%rax), %ecx
	addl	$16, %eax
	cltq
	movl	%ecx, 28(%r15)
	movb	$58, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1329
.L2144:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1329
	.p2align 4,,10
	.p2align 3
.L1304:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	cmpw	$67, %dx
	je	.L1335
	ja	.L1336
	cmpw	$65, %dx
	je	.L1337
	cmpw	$66, %dx
	jne	.L2233
	movq	(%r15), %r12
	xorl	%edx, %edx
	movl	$22, %esi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	.p2align 4,,10
	.p2align 3
.L2221:
	movl	$2, %ebx
.L1291:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2234
	addq	$104, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2232:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L2143
	jmp	.L1325
	.p2align 4,,10
	.p2align 3
.L1318:
	testw	%dx, %dx
	je	.L1319
.L2142:
	movl	28(%r15), %eax
.L1321:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$2, %r14
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L2235
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L1321
	jmp	.L1325
	.p2align 4,,10
	.p2align 3
.L2235:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L2142
	jmp	.L1325
	.p2align 4,,10
	.p2align 3
.L2230:
	movq	(%r15), %rax
	leaq	37512(%rax), %rdi
	call	_ZN2v88internal10StackGuard16HandleInterruptsEv@PLT
	movq	(%r15), %rdx
	cmpq	312(%rdx), %rax
	je	.L2221
	movq	0(%r13), %rax
	testb	$1, %al
	je	.L1300
.L2231:
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L2236
.L1298:
	movq	-72(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier19ApplyToJsonFunctionENS0_6HandleINS0_6ObjectEEES4_
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L1300
	jmp	.L2221
.L2236:
	movq	-1(%rax), %rax
	cmpw	$66, 11(%rax)
	jne	.L1300
	jmp	.L1298
.L1336:
	cmpw	$1041, %dx
	je	.L1341
	cmpw	$1061, %dx
	jne	.L1340
	leaq	8(%r15), %r13
	testb	%r14b, %r14b
	jne	.L2237
.L1479:
	cmpq	$0, 72(%r15)
	je	.L1488
	movl	28(%r15), %eax
	movl	16(%r15), %ebx
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%ebx, %ebx
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L1485
	addl	$16, %eax
	cltq
	movb	$10, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2171
.L1487:
	movl	80(%r15), %eax
	testl	%eax, %eax
	jle	.L1488
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1489:
	movq	72(%r15), %r14
	movl	16(%r15), %r10d
	movzwl	(%r14), %edx
	testl	%r10d, %r10d
	je	.L1491
	testw	%dx, %dx
	je	.L1492
.L2173:
	movl	28(%r15), %eax
.L1493:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$2, %r14
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L2238
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L1493
.L1498:
	movl	80(%r15), %eax
.L1492:
	addl	$1, %ebx
	cmpl	%eax, %ebx
	jl	.L1489
.L1488:
	movq	-72(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier15SerializeStringENS0_6HandleINS0_6StringEEE
	movl	16(%r15), %r14d
	testl	%r14d, %r14d
	jne	.L2239
	movq	40(%r15), %rax
	movq	(%rax), %rdx
	movl	28(%r15), %eax
	leal	1(%rax), %ecx
	addl	$16, %eax
	cltq
	movl	%ecx, 28(%r15)
	movb	$58, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2174
.L1502:
	cmpq	$0, 72(%r15)
	je	.L1505
	movl	28(%r15), %eax
	movl	16(%r15), %r8d
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%r8d, %r8d
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L1506
	addl	$16, %eax
	cltq
	movb	$32, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2175
.L1505:
	movq	(%r15), %r14
	movq	-72(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	41088(%r14), %rax
	addl	$1, 41104(%r14)
	movq	%rax, -112(%rbp)
	movq	41096(%r14), %rax
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal15JsonStringifier9StackPushENS0_6HandleINS0_6ObjectEEES4_
	movl	%eax, %ebx
	cmpl	$1, %eax
	je	.L2240
.L1508:
	movq	-112(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-96(%rbp), %rax
	cmpq	41096(%r14), %rax
	jne	.L2223
	jmp	.L1291
	.p2align 4,,10
	.p2align 3
.L2233:
	cmpw	$64, %dx
	jne	.L1340
.L1339:
	xorl	%ebx, %ebx
	jmp	.L1291
	.p2align 4,,10
	.p2align 3
.L1312:
	leal	16(%rax,%rax), %eax
	movl	$10, %esi
	cltq
	movw	%si, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1314
.L2141:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1314
	.p2align 4,,10
	.p2align 3
.L1333:
	leal	16(%rax,%rax), %eax
	movl	$32, %r11d
	cltq
	movw	%r11w, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1332
.L2145:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1332
.L1307:
	leal	16(%rax,%rax), %eax
	movl	$44, %r9d
	cltq
	movw	%r9w, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1306
.L2140:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1306
.L1341:
	leaq	8(%r15), %r13
	testb	%r14b, %r14b
	je	.L1644
	movl	28(%r15), %eax
	movl	16(%r15), %r14d
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%r14d, %r14d
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L1645
	addl	$16, %eax
	cltq
	movb	$44, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2196
.L1644:
	cmpq	$0, 72(%r15)
	je	.L1653
	movl	28(%r15), %eax
	movl	16(%r15), %r10d
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%r10d, %r10d
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L1650
	addl	$16, %eax
	cltq
	movb	$10, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2197
.L1652:
	movl	80(%r15), %eax
	testl	%eax, %eax
	jle	.L1653
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1654:
	movq	72(%r15), %r14
	movl	16(%r15), %r8d
	movzwl	(%r14), %edx
	testl	%r8d, %r8d
	je	.L1656
	testw	%dx, %dx
	je	.L1657
.L2199:
	movl	28(%r15), %eax
.L1658:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$2, %r14
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L2241
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L1658
.L1663:
	movl	80(%r15), %eax
.L1657:
	addl	$1, %ebx
	cmpl	%eax, %ebx
	jl	.L1654
.L1653:
	movq	-72(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier15SerializeStringENS0_6HandleINS0_6StringEEE
	movl	16(%r15), %r11d
	testl	%r11d, %r11d
	jne	.L2242
	movq	40(%r15), %rax
	movq	(%rax), %rdx
	movl	28(%r15), %eax
	leal	1(%rax), %ecx
	addl	$16, %eax
	cltq
	movl	%ecx, 28(%r15)
	movb	$58, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2200
.L1667:
	cmpq	$0, 72(%r15)
	je	.L1670
	movl	28(%r15), %eax
	movl	16(%r15), %esi
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%esi, %esi
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L1671
	addl	$16, %eax
	cltq
	movb	$32, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2201
.L1670:
	movq	-72(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier27SerializeJSPrimitiveWrapperENS0_6HandleINS0_18JSPrimitiveWrapperEEENS2_INS0_6ObjectEEE
	movl	%eax, %ebx
	jmp	.L1291
.L1340:
	testb	$1, %al
	jne	.L2243
.L1675:
	leaq	8(%r15), %r13
	testb	%r14b, %r14b
	je	.L1710
	movl	28(%r15), %eax
	movl	16(%r15), %r14d
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%r14d, %r14d
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L1711
	addl	$16, %eax
	cltq
	movb	$44, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2208
.L1710:
	cmpq	$0, 72(%r15)
	je	.L1718
	movl	28(%r15), %eax
	movl	16(%r15), %r10d
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%r10d, %r10d
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L1715
	addl	$16, %eax
	cltq
	movb	$10, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2209
.L1717:
	movl	80(%r15), %eax
	testl	%eax, %eax
	jle	.L1718
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L1719:
	movq	72(%r15), %rbx
	movl	16(%r15), %r8d
	movzwl	(%rbx), %edx
	testl	%r8d, %r8d
	je	.L1721
	testw	%dx, %dx
	je	.L1722
.L2211:
	movl	28(%r15), %eax
.L1723:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$2, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L2244
	movzwl	(%rbx), %edx
	testw	%dx, %dx
	jne	.L1723
.L1728:
	movl	80(%r15), %eax
.L1722:
	addl	$1, %r14d
	cmpl	%eax, %r14d
	jl	.L1719
.L1718:
	movq	-72(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier15SerializeStringENS0_6HandleINS0_6StringEEE
	movl	16(%r15), %r11d
	testl	%r11d, %r11d
	jne	.L2245
	movq	40(%r15), %rax
	movq	(%rax), %rdx
	movl	28(%r15), %eax
	leal	1(%rax), %ecx
	addl	$16, %eax
	cltq
	movl	%ecx, 28(%r15)
	movb	$58, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2212
.L1732:
	cmpq	$0, 72(%r15)
	je	.L1735
	movl	28(%r15), %eax
	movl	16(%r15), %esi
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%esi, %esi
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L1736
	addl	$16, %eax
	cltq
	movb	$32, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2213
.L1735:
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L2246
.L1738:
	movq	(%r15), %r14
	movq	-72(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	41088(%r14), %rax
	addl	$1, 41104(%r14)
	movq	%rax, -104(%rbp)
	movq	41096(%r14), %rax
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal15JsonStringifier9StackPushENS0_6HandleINS0_6ObjectEEES4_
	movl	%eax, %ebx
	cmpl	$1, %eax
	jne	.L1740
	cmpq	$0, 56(%r15)
	je	.L2247
.L1742:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier23SerializeJSReceiverSlowENS0_6HandleINS0_10JSReceiverEEE
	cmpl	$1, %eax
	je	.L1746
	movl	%eax, %ebx
.L1740:
	movq	-104(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-112(%rbp), %rax
	cmpq	41096(%r14), %rax
	je	.L1291
.L2223:
	movq	%rax, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L1291
.L1337:
	leaq	8(%r15), %r13
	testb	%r14b, %r14b
	je	.L1344
	movl	28(%r15), %eax
	movl	16(%r15), %r10d
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%r10d, %r10d
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L1345
	addl	$16, %eax
	cltq
	movb	$44, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2146
.L1344:
	cmpq	$0, 72(%r15)
	je	.L1353
	movl	28(%r15), %eax
	movl	16(%r15), %edi
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%edi, %edi
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L1350
	addl	$16, %eax
	cltq
	movb	$10, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2147
.L1352:
	movl	80(%r15), %eax
	testl	%eax, %eax
	jle	.L1353
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1354:
	movq	72(%r15), %r14
	movl	16(%r15), %ecx
	movzwl	(%r14), %edx
	testl	%ecx, %ecx
	je	.L1356
	testw	%dx, %dx
	je	.L1357
.L2149:
	movl	28(%r15), %eax
.L1358:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$2, %r14
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L2248
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L1358
.L1363:
	movl	80(%r15), %eax
.L1357:
	addl	$1, %ebx
	cmpl	%eax, %ebx
	jl	.L1354
.L1353:
	movq	-72(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier15SerializeStringENS0_6HandleINS0_6StringEEE
	movl	16(%r15), %r8d
	testl	%r8d, %r8d
	jne	.L2249
	movq	40(%r15), %rax
	movq	(%rax), %rdx
	movl	28(%r15), %eax
	leal	1(%rax), %ecx
	addl	$16, %eax
	cltq
	movl	%ecx, 28(%r15)
	movb	$58, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2150
.L1367:
	cmpq	$0, 72(%r15)
	je	.L1370
	movl	28(%r15), %eax
	movl	16(%r15), %r11d
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%r11d, %r11d
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L1371
	addl	$16, %eax
	cltq
	movb	$32, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2151
.L1370:
	movq	(%r12), %rax
	movq	%r15, %rdi
	movq	7(%rax), %xmm0
	call	_ZN2v88internal15JsonStringifier15SerializeDoubleEd
	movl	%eax, %ebx
	jmp	.L1291
.L2237:
	movl	28(%r15), %eax
	movl	16(%r15), %esi
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%esi, %esi
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L1480
	addl	$16, %eax
	cltq
	movb	$44, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1479
.L2170:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1479
	.p2align 4,,10
	.p2align 3
.L2238:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L2173
	jmp	.L1498
	.p2align 4,,10
	.p2align 3
.L2248:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L2149
	jmp	.L1363
	.p2align 4,,10
	.p2align 3
.L2241:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L2199
	jmp	.L1663
.L2244:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%rbx), %edx
	testw	%dx, %dx
	jne	.L2211
	jmp	.L1728
.L1491:
	testw	%dx, %dx
	je	.L1492
.L2172:
	movl	28(%r15), %eax
.L1494:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$2, %r14
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L2250
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L1494
	jmp	.L1498
	.p2align 4,,10
	.p2align 3
.L2250:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L2172
	jmp	.L1498
.L1656:
	testw	%dx, %dx
	je	.L1657
.L2198:
	movl	28(%r15), %eax
.L1659:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$2, %r14
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L2251
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L1659
	jmp	.L1663
	.p2align 4,,10
	.p2align 3
.L2251:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L2198
	jmp	.L1663
.L1356:
	testw	%dx, %dx
	je	.L1357
.L2148:
	movl	28(%r15), %eax
.L1359:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$2, %r14
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L2252
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L1359
	jmp	.L1363
	.p2align 4,,10
	.p2align 3
.L2252:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L2148
	jmp	.L1363
.L1721:
	testw	%dx, %dx
	je	.L1722
.L2210:
	movl	28(%r15), %eax
.L1724:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$2, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L2253
	movzwl	(%rbx), %edx
	testw	%dx, %dx
	jne	.L1724
	jmp	.L1728
.L2253:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%rbx), %edx
	testw	%dx, %dx
	jne	.L2210
	jmp	.L1728
.L1335:
	movslq	43(%rax), %rax
	cmpb	$1, %al
	je	.L1373
	cmpb	$3, %al
	je	.L1374
	testb	%al, %al
	jne	.L1339
	leaq	8(%r15), %rbx
	testb	%r14b, %r14b
	je	.L1376
	movl	28(%r15), %eax
	movl	16(%r15), %r9d
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%r9d, %r9d
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L1377
	addl	$16, %eax
	cltq
	movb	$44, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2152
.L1376:
	cmpq	$0, 72(%r15)
	je	.L1385
	movl	28(%r15), %eax
	movl	16(%r15), %esi
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%esi, %esi
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L1382
	addl	$16, %eax
	cltq
	movb	$10, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2153
.L1384:
	movl	80(%r15), %eax
	testl	%eax, %eax
	jle	.L1385
	xorl	%r12d, %r12d
.L1386:
	movq	72(%r15), %r13
	movl	16(%r15), %r14d
	movzwl	0(%r13), %edx
	testl	%r14d, %r14d
	je	.L1388
	testw	%dx, %dx
	je	.L1389
.L2155:
	movl	28(%r15), %eax
.L1390:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$2, %r13
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L2254
	movzwl	0(%r13), %edx
	testw	%dx, %dx
	jne	.L1390
.L1395:
	movl	80(%r15), %eax
.L1389:
	addl	$1, %r12d
	cmpl	%eax, %r12d
	jl	.L1386
.L1385:
	movq	-72(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier15SerializeStringENS0_6HandleINS0_6StringEEE
	movl	16(%r15), %edi
	testl	%edi, %edi
	jne	.L2255
	movq	40(%r15), %rax
	movq	(%rax), %rdx
	movl	28(%r15), %eax
	leal	1(%rax), %ecx
	addl	$16, %eax
	cltq
	movl	%ecx, 28(%r15)
	movb	$58, -1(%rdx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L2156
.L1398:
	movq	40(%r15), %rcx
	cmpq	$0, 72(%r15)
	movl	16(%r15), %edx
	movq	(%rcx), %rcx
	je	.L1399
	leal	1(%rax), %esi
	movl	%esi, 28(%r15)
	testl	%edx, %edx
	jne	.L1400
	addl	$16, %eax
	cltq
	movb	$32, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L1402
.L2157:
	movq	40(%r15), %rcx
	movl	16(%r15), %edx
	movq	(%rcx), %rcx
.L1399:
	testl	%edx, %edx
	je	.L1788
	movl	$102, %edx
	leaq	.LC23(%rip), %r12
	jmp	.L1404
.L1408:
	movzbl	(%r12), %edx
	testb	%dl, %dl
	je	.L2222
.L1409:
	movq	40(%r15), %rcx
	movq	(%rcx), %rcx
.L1404:
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$1, %r12
	cltq
	movl	%esi, 28(%r15)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	jne	.L1408
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r12), %edx
	testb	%dl, %dl
	je	.L2222
	movl	28(%r15), %eax
	jmp	.L1409
.L2254:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	0(%r13), %edx
	testw	%dx, %dx
	jne	.L2155
	jmp	.L1395
.L2243:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L2256
	leaq	8(%r15), %r13
	testb	%r14b, %r14b
	je	.L1678
	movl	28(%r15), %eax
	movl	16(%r15), %r14d
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%r14d, %r14d
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L1679
	addl	$16, %eax
	cltq
	movb	$44, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2202
.L1678:
	cmpq	$0, 72(%r15)
	je	.L1687
	movl	28(%r15), %eax
	movl	16(%r15), %r10d
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%r10d, %r10d
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L1684
	addl	$16, %eax
	cltq
	movb	$10, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2203
.L1686:
	movl	80(%r15), %eax
	testl	%eax, %eax
	jle	.L1687
	xorl	%ebx, %ebx
.L1688:
	movq	72(%r15), %r14
	movl	16(%r15), %r8d
	movzwl	(%r14), %edx
	testl	%r8d, %r8d
	je	.L1690
	testw	%dx, %dx
	je	.L1691
.L2205:
	movl	28(%r15), %eax
.L1692:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$2, %r14
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L2257
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L1692
.L1697:
	movl	80(%r15), %eax
.L1691:
	addl	$1, %ebx
	cmpl	%eax, %ebx
	jl	.L1688
.L1687:
	movq	-72(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier15SerializeStringENS0_6HandleINS0_6StringEEE
	movl	16(%r15), %r11d
	testl	%r11d, %r11d
	jne	.L2258
	movq	40(%r15), %rax
	movq	(%rax), %rdx
	movl	28(%r15), %eax
	leal	1(%rax), %ecx
	addl	$16, %eax
	cltq
	movl	%ecx, 28(%r15)
	movb	$58, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2206
.L1701:
	cmpq	$0, 72(%r15)
	je	.L1704
	movl	28(%r15), %eax
	movl	16(%r15), %esi
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%esi, %esi
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L1705
	addl	$16, %eax
	cltq
	movb	$32, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2207
.L1704:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier15SerializeStringENS0_6HandleINS0_6StringEEE
.L2222:
	movl	$1, %ebx
	jmp	.L1291
.L1388:
	testw	%dx, %dx
	je	.L1389
.L2154:
	movl	28(%r15), %eax
.L1391:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$2, %r13
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L2259
	movzwl	0(%r13), %edx
	testw	%dx, %dx
	jne	.L1391
	jmp	.L1395
.L2259:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	0(%r13), %edx
	testw	%dx, %dx
	jne	.L2154
	jmp	.L1395
.L2242:
	movq	40(%r15), %rax
	movl	$58, %edi
	movq	(%rax), %rdx
	movl	28(%r15), %eax
	leal	1(%rax), %ecx
	leal	16(%rax,%rax), %eax
	cltq
	movl	%ecx, 28(%r15)
	movw	%di, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1667
.L2200:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1667
.L2249:
	movq	40(%r15), %rax
	movl	$58, %ebx
	movq	(%rax), %rdx
	movl	28(%r15), %eax
	leal	1(%rax), %ecx
	leal	16(%rax,%rax), %eax
	cltq
	movl	%ecx, 28(%r15)
	movw	%bx, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1367
.L2150:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1367
.L2239:
	movq	40(%r15), %rax
	movl	$58, %r9d
	movq	(%rax), %rdx
	movl	28(%r15), %eax
	leal	1(%rax), %ecx
	leal	16(%rax,%rax), %eax
	cltq
	movl	%ecx, 28(%r15)
	movw	%r9w, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1502
.L2174:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1502
.L1650:
	leal	16(%rax,%rax), %eax
	movl	$10, %r9d
	cltq
	movw	%r9w, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1652
.L2197:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1652
.L1485:
	leal	16(%rax,%rax), %eax
	movl	$10, %r11d
	cltq
	movw	%r11w, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1487
.L2171:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1487
.L1350:
	leal	16(%rax,%rax), %eax
	movl	$10, %esi
	cltq
	movw	%si, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1352
.L2147:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1352
.L2240:
	movq	(%r12), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L1509
	sarq	$32, %rax
	movl	%eax, -72(%rbp)
	js	.L1511
.L1512:
	movl	28(%r15), %eax
	movl	16(%r15), %esi
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%esi, %esi
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L1518
	addl	$16, %eax
	cltq
	movb	$91, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2176
.L1520:
	addl	$1, 80(%r15)
	cmpq	$0, 64(%r15)
	je	.L2260
.L1522:
	movl	-72(%rbp), %esi
	testl	%esi, %esi
	jne	.L2261
.L1623:
	subl	$1, 80(%r15)
	movl	28(%r15), %eax
	movl	16(%r15), %esi
	movq	40(%r15), %rcx
	leal	1(%rax), %edx
.L1625:
	movq	(%rcx), %rcx
	movl	%edx, 28(%r15)
	testl	%esi, %esi
	jne	.L1639
	addl	$16, %eax
	cltq
	movb	$93, -1(%rcx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2195
.L1641:
	subq	$16, 96(%r15)
	jmp	.L1508
.L2257:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L2205
	jmp	.L1697
.L1690:
	testw	%dx, %dx
	je	.L1691
.L2204:
	movl	28(%r15), %eax
.L1693:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$2, %r14
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L2262
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L1693
	jmp	.L1697
.L2262:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L2204
	jmp	.L1697
.L1506:
	leal	16(%rax,%rax), %eax
	movl	$32, %edi
	cltq
	movw	%di, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1505
.L2175:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1505
.L1371:
	leal	16(%rax,%rax), %eax
	movl	$32, %r10d
	cltq
	movw	%r10w, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1370
.L2151:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1370
.L1671:
	leal	16(%rax,%rax), %eax
	movl	$32, %ecx
	cltq
	movw	%cx, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1670
.L2201:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1670
.L2245:
	movq	40(%r15), %rax
	movl	$58, %edi
	movq	(%rax), %rdx
	movl	28(%r15), %eax
	leal	1(%rax), %ecx
	leal	16(%rax,%rax), %eax
	cltq
	movl	%ecx, 28(%r15)
	movw	%di, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1732
.L2212:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1732
.L1715:
	leal	16(%rax,%rax), %eax
	movl	$10, %r9d
	cltq
	movw	%r9w, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1717
.L2209:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1717
.L1480:
	leal	16(%rax,%rax), %eax
	movl	$44, %ecx
	cltq
	movw	%cx, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1479
	jmp	.L2170
.L1736:
	leal	16(%rax,%rax), %eax
	movl	$32, %ecx
	cltq
	movw	%cx, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1735
.L2213:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1735
.L1345:
	leal	16(%rax,%rax), %eax
	movl	$44, %r9d
	cltq
	movw	%r9w, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1344
.L2146:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1344
.L2256:
	movq	-1(%rax), %rax
	xorl	%ebx, %ebx
	testb	$2, 13(%rax)
	jne	.L1291
	jmp	.L1675
.L1373:
	leaq	8(%r15), %rbx
	testb	%r14b, %r14b
	je	.L1411
	movl	28(%r15), %eax
	movl	16(%r15), %r11d
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%r11d, %r11d
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L1412
	addl	$16, %eax
	cltq
	movb	$44, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2158
.L1411:
	cmpq	$0, 72(%r15)
	je	.L1420
	movl	28(%r15), %eax
	movl	16(%r15), %r8d
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%r8d, %r8d
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L1417
	addl	$16, %eax
	cltq
	movb	$10, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2159
.L1419:
	movl	80(%r15), %eax
	testl	%eax, %eax
	jle	.L1420
	xorl	%r12d, %r12d
.L1421:
	movq	72(%r15), %r13
	movl	16(%r15), %esi
	movzwl	0(%r13), %edx
	testl	%esi, %esi
	je	.L1423
	testw	%dx, %dx
	je	.L1424
.L2161:
	movl	28(%r15), %eax
.L1425:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$2, %r13
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L2263
	movzwl	0(%r13), %edx
	testw	%dx, %dx
	jne	.L1425
.L1430:
	movl	80(%r15), %eax
.L1424:
	addl	$1, %r12d
	cmpl	%eax, %r12d
	jl	.L1421
.L1420:
	movq	-72(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier15SerializeStringENS0_6HandleINS0_6StringEEE
	movl	16(%r15), %r9d
	testl	%r9d, %r9d
	jne	.L2264
	movq	40(%r15), %rax
	movq	(%rax), %rdx
	movl	28(%r15), %eax
	leal	1(%rax), %ecx
	addl	$16, %eax
	cltq
	movl	%ecx, 28(%r15)
	movb	$58, -1(%rdx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L2162
.L1433:
	movq	40(%r15), %rcx
	cmpq	$0, 72(%r15)
	movl	16(%r15), %edx
	movq	(%rcx), %rcx
	je	.L1434
	leal	1(%rax), %esi
	movl	%esi, 28(%r15)
	testl	%edx, %edx
	jne	.L1435
	addl	$16, %eax
	cltq
	movb	$32, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L1437
.L2163:
	movq	40(%r15), %rcx
	movl	16(%r15), %edx
	movq	(%rcx), %rcx
.L1434:
	testl	%edx, %edx
	je	.L1789
	movl	$116, %edx
	leaq	.LC24(%rip), %r12
	jmp	.L1439
.L1442:
	movzbl	(%r12), %edx
	testb	%dl, %dl
	je	.L2222
.L1443:
	movq	40(%r15), %rcx
	movq	(%rcx), %rcx
.L1439:
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$1, %r12
	cltq
	movl	%esi, 28(%r15)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	jne	.L1442
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r12), %edx
	testb	%dl, %dl
	je	.L2222
	movl	28(%r15), %eax
	jmp	.L1443
.L2263:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	0(%r13), %edx
	testw	%dx, %dx
	jne	.L2161
	jmp	.L1430
.L1423:
	testw	%dx, %dx
	je	.L1424
.L2160:
	movl	28(%r15), %eax
.L1426:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$2, %r13
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L2265
	movzwl	0(%r13), %edx
	testw	%dx, %dx
	jne	.L1426
	jmp	.L1430
.L2265:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	0(%r13), %edx
	testw	%dx, %dx
	jne	.L2160
	jmp	.L1430
.L1645:
	leal	16(%rax,%rax), %eax
	movl	$44, %ebx
	cltq
	movw	%bx, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1644
.L2196:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1644
.L1374:
	leaq	8(%r15), %rbx
	testb	%r14b, %r14b
	je	.L1445
	movl	28(%r15), %eax
	movl	16(%r15), %r14d
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%r14d, %r14d
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L1446
	addl	$16, %eax
	cltq
	movb	$44, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2164
.L1445:
	cmpq	$0, 72(%r15)
	je	.L1454
	movl	28(%r15), %eax
	movl	16(%r15), %r11d
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%r11d, %r11d
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L1451
	addl	$16, %eax
	cltq
	movb	$10, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2165
.L1453:
	movl	80(%r15), %eax
	testl	%eax, %eax
	jle	.L1454
	xorl	%r12d, %r12d
.L1455:
	movq	72(%r15), %r13
	movl	16(%r15), %r9d
	movzwl	0(%r13), %edx
	testl	%r9d, %r9d
	je	.L1457
	testw	%dx, %dx
	je	.L1458
.L2167:
	movl	28(%r15), %eax
.L1459:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$2, %r13
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L2266
	movzwl	0(%r13), %edx
	testw	%dx, %dx
	jne	.L1459
.L1464:
	movl	80(%r15), %eax
.L1458:
	addl	$1, %r12d
	cmpl	%eax, %r12d
	jl	.L1455
.L1454:
	movq	-72(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier15SerializeStringENS0_6HandleINS0_6StringEEE
	movl	16(%r15), %r12d
	testl	%r12d, %r12d
	jne	.L2267
	movq	40(%r15), %rax
	movq	(%rax), %rdx
	movl	28(%r15), %eax
	leal	1(%rax), %ecx
	addl	$16, %eax
	cltq
	movl	%ecx, 28(%r15)
	movb	$58, -1(%rdx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L2168
.L1467:
	movq	40(%r15), %rcx
	cmpq	$0, 72(%r15)
	movl	16(%r15), %edx
	movq	(%rcx), %rcx
	je	.L1468
	leal	1(%rax), %esi
	movl	%esi, 28(%r15)
	testl	%edx, %edx
	jne	.L1469
	addl	$16, %eax
	cltq
	movb	$32, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L1471
.L2169:
	movq	40(%r15), %rcx
	movl	16(%r15), %edx
	movq	(%rcx), %rcx
.L1468:
	testl	%edx, %edx
	je	.L1790
	movl	$110, %edx
	leaq	.LC19(%rip), %r12
	jmp	.L1473
.L1476:
	movzbl	(%r12), %edx
	testb	%dl, %dl
	je	.L2222
.L1477:
	movq	40(%r15), %rcx
	movq	(%rcx), %rcx
.L1473:
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$1, %r12
	cltq
	movl	%esi, 28(%r15)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	jne	.L1476
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r12), %edx
	testb	%dl, %dl
	je	.L2222
	movl	28(%r15), %eax
	jmp	.L1477
.L2266:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	0(%r13), %edx
	testw	%dx, %dx
	jne	.L2167
	jmp	.L1464
.L1457:
	testw	%dx, %dx
	je	.L1458
.L2166:
	movl	28(%r15), %eax
.L1460:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$2, %r13
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L2268
	movzwl	0(%r13), %edx
	testw	%dx, %dx
	jne	.L1460
	jmp	.L1464
.L2268:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	0(%r13), %edx
	testw	%dx, %dx
	jne	.L2166
	jmp	.L1464
.L2246:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jne	.L1738
	movq	-72(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier16SerializeJSProxyENS0_6HandleINS0_7JSProxyEEENS2_INS0_6ObjectEEE
	movl	%eax, %ebx
	jmp	.L1291
.L1711:
	leal	16(%rax,%rax), %eax
	movl	$44, %ebx
	cltq
	movw	%bx, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1710
.L2208:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1710
.L1754:
	subl	$1, 80(%r15)
	cmpb	$0, -80(%rbp)
	movq	-96(%rbp), %r14
	movq	-128(%rbp), %r13
	jne	.L2128
	movl	28(%r15), %esi
.L1767:
	movq	40(%r15), %rax
	cmpl	$0, 16(%r15)
	movq	(%rax), %rdx
	leal	1(%rsi), %eax
	jne	.L1781
	movl	%eax, 28(%r15)
	leal	16(%rsi), %eax
	cltq
	movb	$125, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1746
.L2218:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
.L1746:
	subq	$16, 96(%r15)
	jmp	.L1740
.L1639:
	leal	16(%rax,%rax), %eax
	movl	$93, %edx
	cltq
	movw	%dx, -1(%rcx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1641
.L2195:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1641
.L1518:
	leal	16(%rax,%rax), %eax
	movl	$91, %ecx
	cltq
	movw	%cx, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1520
.L2176:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1520
.L1509:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L2269
.L1511:
	leaq	.LC25(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1790:
	movl	$110, %edx
	leaq	.LC19(%rip), %r12
	jmp	.L1472
.L1474:
	movzbl	(%r12), %edx
	testb	%dl, %dl
	je	.L2222
.L1475:
	movq	40(%r15), %rcx
	movq	(%rcx), %rcx
.L1472:
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$1, %r12
	cltq
	movl	%esi, 28(%r15)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	jne	.L1474
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r12), %edx
	testb	%dl, %dl
	je	.L2222
	movl	28(%r15), %eax
	jmp	.L1475
.L2267:
	movq	40(%r15), %rax
	movl	$58, %r8d
	movq	(%rax), %rdx
	movl	28(%r15), %eax
	leal	1(%rax), %ecx
	leal	16(%rax,%rax), %eax
	cltq
	movl	%ecx, 28(%r15)
	movw	%r8w, -1(%rdx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	jne	.L1467
.L2168:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	28(%r15), %eax
	jmp	.L1467
.L2255:
	movq	40(%r15), %rax
	movl	$58, %r13d
	movq	(%rax), %rdx
	movl	28(%r15), %eax
	leal	1(%rax), %ecx
	leal	16(%rax,%rax), %eax
	cltq
	movl	%ecx, 28(%r15)
	movw	%r13w, -1(%rdx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	jne	.L1398
.L2156:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	28(%r15), %eax
	jmp	.L1398
.L1788:
	movl	$102, %edx
	leaq	.LC23(%rip), %r12
	jmp	.L1403
.L1406:
	movzbl	(%r12), %edx
	testb	%dl, %dl
	je	.L2222
.L1405:
	movq	40(%r15), %rcx
	movq	(%rcx), %rcx
.L1403:
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$1, %r12
	cltq
	movl	%esi, 28(%r15)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	jne	.L1406
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r12), %edx
	testb	%dl, %dl
	je	.L2222
	movl	28(%r15), %eax
	jmp	.L1405
.L1789:
	movl	$116, %edx
	leaq	.LC24(%rip), %r12
	jmp	.L1438
.L1440:
	movzbl	(%r12), %edx
	testb	%dl, %dl
	je	.L2222
.L1441:
	movq	40(%r15), %rcx
	movq	(%rcx), %rcx
.L1438:
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$1, %r12
	cltq
	movl	%esi, 28(%r15)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	jne	.L1440
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r12), %edx
	testb	%dl, %dl
	je	.L2222
	movl	28(%r15), %eax
	jmp	.L1441
.L2264:
	movq	40(%r15), %rax
	movq	(%rax), %rdx
	movl	28(%r15), %eax
	leal	1(%rax), %ecx
	leal	16(%rax,%rax), %eax
	movl	%ecx, 28(%r15)
	cltq
	movl	$58, %ecx
	movw	%cx, -1(%rdx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	jne	.L1433
.L2162:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	28(%r15), %eax
	jmp	.L1433
.L1382:
	leal	16(%rax,%rax), %eax
	movl	$10, %ecx
	cltq
	movw	%cx, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1384
.L2153:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1384
.L1435:
	leal	16(%rax,%rax), %eax
	movl	$32, %edx
	cltq
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	jne	.L2163
.L1437:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	40(%r15), %rcx
	movl	16(%r15), %edx
	movl	28(%r15), %eax
	movq	(%rcx), %rcx
	jmp	.L1434
.L2258:
	movq	40(%r15), %rax
	movl	$58, %edi
	movq	(%rax), %rdx
	movl	28(%r15), %eax
	leal	1(%rax), %ecx
	leal	16(%rax,%rax), %eax
	cltq
	movl	%ecx, 28(%r15)
	movw	%di, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1701
.L2206:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1701
.L1684:
	leal	16(%rax,%rax), %eax
	movl	$10, %r9d
	cltq
	movw	%r9w, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1686
.L2203:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1686
.L1417:
	leal	16(%rax,%rax), %eax
	movl	$10, %edi
	cltq
	movw	%di, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1419
.L2159:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1419
.L1400:
	leal	16(%rax,%rax), %eax
	movl	$32, %r12d
	cltq
	movw	%r12w, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	jne	.L2157
.L1402:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	40(%r15), %rcx
	movl	16(%r15), %edx
	movl	28(%r15), %eax
	movq	(%rcx), %rcx
	jmp	.L1399
.L2247:
	movq	(%r12), %rax
	movq	-1(%rax), %rdx
	cmpw	$1041, 11(%rdx)
	jbe	.L1742
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$2097152, %edx
	jne	.L1742
	movq	(%r15), %rcx
	movq	15(%rax), %rdx
	cmpq	%rdx, 288(%rcx)
	je	.L1743
	cmpq	%rdx, 1016(%rcx)
	jne	.L1742
.L1743:
	movq	-1(%rax), %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L2270
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -72(%rbp)
.L1747:
	movq	40(%r15), %rax
	movl	28(%r15), %esi
	cmpl	$0, 16(%r15)
	movq	(%rax), %rdx
	leal	1(%rsi), %eax
	jne	.L1749
	movl	%eax, 28(%r15)
	leal	16(%rsi), %eax
	cltq
	movb	$123, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2214
.L1751:
	movq	-72(%rbp), %rax
	xorl	%r10d, %r10d
	movq	%r13, -128(%rbp)
	addl	$1, 80(%r15)
	movl	%r10d, %r13d
	movq	$24, -88(%rbp)
	movq	(%rax), %rdi
	movq	%r14, -96(%rbp)
	jmp	.L1753
.L2272:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L1756:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L2215
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %rsi
	movq	(%rax), %rdi
	movq	39(%rdi), %rax
	movq	7(%rsi,%rax), %rax
	movq	%rax, %rdx
	sarq	$32, %rdx
	btq	$36, %rax
	jc	.L1766
	testb	$2, %dl
	jne	.L1762
	movq	(%r12), %rax
	movq	-1(%rax), %rax
	cmpq	%rax, %rdi
	je	.L2271
.L1762:
	movq	(%r15), %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	call	_ZN2v88internal6Object20GetPropertyOrElementEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2137
.L1763:
	movzbl	-80(%rbp), %eax
	movq	%r14, %rcx
	movq	%r15, %rdi
	movl	%eax, %edx
	movl	%eax, %r13d
	call	_ZN2v88internal15JsonStringifier10Serialize_ILb1EEENS1_6ResultENS0_6HandleINS0_6ObjectEEEbS6_
	movl	%eax, %edx
	movl	%r13d, %eax
	xorl	$1, %eax
	cmpl	$1, %edx
	sete	%cl
	andb	%cl, %al
	jne	.L1765
	cmpl	$2, %edx
	je	.L2137
	movl	%r13d, %eax
.L1765:
	movb	%al, -80(%rbp)
.L2215:
	movq	-72(%rbp), %rax
	movq	(%rax), %rdi
.L1766:
	addq	$24, -88(%rbp)
	movl	-120(%rbp), %r13d
.L1753:
	movl	15(%rdi), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	cmpl	%eax, %r13d
	jge	.L1754
	leal	1(%r13), %eax
	movq	(%r15), %rdx
	movl	%eax, -120(%rbp)
	movq	39(%rdi), %rax
	movq	-88(%rbp), %rdi
	movq	-1(%rdi,%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	jne	.L2272
	movq	41088(%rdx), %r14
	cmpq	41096(%rdx), %r14
	je	.L2273
.L1757:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r14)
	jmp	.L1756
.L1451:
	leal	16(%rax,%rax), %eax
	movl	$10, %r10d
	cltq
	movw	%r10w, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1453
.L2165:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1453
.L1469:
	leal	16(%rax,%rax), %eax
	movl	$32, %edi
	cltq
	movw	%di, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	jne	.L2169
.L1471:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	40(%r15), %rcx
	movl	16(%r15), %edx
	movl	28(%r15), %eax
	movq	(%rcx), %rcx
	jmp	.L1468
.L2261:
	movl	$0, -80(%rbp)
.L1586:
	movl	-72(%rbp), %ecx
	movl	-80(%rbp), %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier22SerializeArrayLikeSlowENS0_6HandleINS0_10JSReceiverEEEjj
	cmpl	$1, %eax
	je	.L2118
	movl	%eax, %ebx
	jmp	.L1508
.L2260:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$2, %eax
	je	.L1523
	cmpb	$4, %al
	je	.L1524
	testb	%al, %al
	jne	.L1522
	movq	(%r15), %r12
	movq	15(%rdx), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1525
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -80(%rbp)
.L1526:
	cmpl	$0, -72(%rbp)
	movq	(%r15), %rax
	je	.L1623
	addq	$37544, %rax
	movq	%r14, -88(%rbp)
	movq	%r15, %r14
	movq	%rax, -104(%rbp)
	xorl	%eax, %eax
	movl	%ebx, -120(%rbp)
	movl	%eax, %ebx
.L1536:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	%rax, %r8
	movq	-104(%rbp), %rax
	movq	(%rax), %rax
	cmpq	%rax, %r8
	jb	.L2274
.L1529:
	testl	%ebx, %ebx
	je	.L1531
	movl	28(%r14), %eax
	movl	16(%r14), %r12d
	movq	40(%r14), %rcx
	leal	1(%rax), %esi
	testl	%r12d, %r12d
	movq	(%rcx), %rcx
	movl	%esi, 28(%r14)
	jne	.L1532
	addl	$16, %eax
	cltq
	movb	$44, -1(%rcx,%rax)
	movl	24(%r14), %eax
	cmpl	%eax, 28(%r14)
	je	.L2177
.L1531:
	cmpq	$0, 72(%r14)
	je	.L1540
	movl	28(%r14), %eax
	movl	16(%r14), %r10d
	movq	40(%r14), %rcx
	leal	1(%rax), %esi
	testl	%r10d, %r10d
	movq	(%rcx), %rcx
	movl	%esi, 28(%r14)
	jne	.L1537
	addl	$16, %eax
	cltq
	movb	$10, -1(%rcx,%rax)
	movl	24(%r14), %eax
	cmpl	%eax, 28(%r14)
	je	.L2178
.L1539:
	movl	80(%r14), %eax
	testl	%eax, %eax
	jle	.L1540
	xorl	%r12d, %r12d
.L1541:
	movq	72(%r14), %r15
	movl	16(%r14), %r8d
	movzwl	(%r15), %edx
	testl	%r8d, %r8d
	je	.L1543
	testw	%dx, %dx
	je	.L1544
.L2180:
	movl	28(%r14), %eax
.L1545:
	movq	40(%r14), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$2, %r15
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r14)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r14), %eax
	cmpl	24(%r14), %eax
	je	.L2275
	movzwl	(%r15), %edx
	testw	%dx, %dx
	jne	.L1545
.L1550:
	movl	80(%r14), %eax
.L1544:
	addl	$1, %r12d
	cmpl	%eax, %r12d
	jl	.L1541
.L1540:
	movq	-80(%rbp), %rdi
	leal	16(,%rbx,8), %eax
	addl	$1, %ebx
	cltq
	movq	(%rdi), %rcx
	movq	%r14, %rdi
	movq	-1(%rax,%rcx), %rsi
	call	_ZN2v88internal15JsonStringifier12SerializeSmiENS0_3SmiE
	cmpl	-72(%rbp), %ebx
	jne	.L1536
	movq	%r14, %r15
	movl	-120(%rbp), %ebx
	movq	-88(%rbp), %r14
.L2118:
	subl	$1, 80(%r15)
	movl	16(%r15), %esi
	movl	28(%r15), %eax
.L1786:
	movq	40(%r15), %rcx
	cmpq	$0, 72(%r15)
	leal	1(%rax), %edx
	movq	(%rcx), %rdi
	je	.L1625
	movl	%edx, 28(%r15)
	testl	%esi, %esi
	jne	.L1626
	addl	$16, %eax
	cltq
	movb	$10, -1(%rdi,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L2193
.L1627:
	movl	80(%r15), %ecx
	movl	16(%r15), %esi
	testl	%ecx, %ecx
	jle	.L2194
	movl	$0, -72(%rbp)
	movq	%r13, %r12
.L1628:
	movq	72(%r15), %r13
	movzwl	0(%r13), %edx
	testl	%esi, %esi
	je	.L1629
	testw	%dx, %dx
	je	.L1630
.L1631:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$2, %r13
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L2276
	movzwl	0(%r13), %edx
	testw	%dx, %dx
	jne	.L1631
.L1636:
	movl	16(%r15), %esi
	movl	80(%r15), %ecx
.L1630:
	addl	$1, -72(%rbp)
	movl	-72(%rbp), %edi
	cmpl	%ecx, %edi
	jl	.L1628
	movq	%r12, %r13
.L2194:
	movq	40(%r15), %rcx
	leal	1(%rax), %edx
	jmp	.L1625
.L2276:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	0(%r13), %edx
	movl	28(%r15), %eax
	testw	%dx, %dx
	jne	.L1631
	jmp	.L1636
.L1629:
	testw	%dx, %dx
	jne	.L1632
	jmp	.L1630
	.p2align 4,,10
	.p2align 3
.L2224:
	movzwl	0(%r13), %edx
	testw	%dx, %dx
	je	.L1636
.L1632:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$2, %r13
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	jne	.L2224
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	28(%r15), %eax
	jmp	.L2224
.L1626:
	leal	16(%rax,%rax), %eax
	movl	$10, %ecx
	cltq
	movw	%cx, -1(%rdi,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	jne	.L1627
.L2193:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	28(%r15), %eax
	jmp	.L1627
	.p2align 4,,10
	.p2align 3
.L2275:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%r15), %edx
	testw	%dx, %dx
	jne	.L2180
	jmp	.L1550
.L1543:
	testw	%dx, %dx
	je	.L1544
.L2179:
	movl	28(%r14), %eax
.L1546:
	movq	40(%r14), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$2, %r15
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r14)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r14), %eax
	cmpl	24(%r14), %eax
	je	.L2277
	movzwl	(%r15), %edx
	testw	%dx, %dx
	jne	.L1546
	jmp	.L1550
.L2277:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%r15), %edx
	testw	%dx, %dx
	jne	.L2179
	jmp	.L1550
.L1537:
	leal	16(%rax,%rax), %eax
	movl	$10, %r9d
	cltq
	movw	%r9w, -1(%rcx,%rax)
	movl	24(%r14), %eax
	cmpl	%eax, 28(%r14)
	jne	.L1539
.L2178:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1539
.L2274:
	movq	(%r14), %rax
	leaq	37512(%rax), %rdi
	call	_ZN2v88internal10StackGuard16HandleInterruptsEv@PLT
	movq	(%r14), %rcx
	cmpq	%rax, 312(%rcx)
	jne	.L1529
	movq	-88(%rbp), %r14
	movl	$2, %ebx
	jmp	.L1508
	.p2align 4,,10
	.p2align 3
.L2269:
	movsd	7(%rax), %xmm1
	movsd	.LC1(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rdx
	movq	%xmm2, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L1511
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm2, -72(%rbp)
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L1511
	je	.L1512
	jmp	.L1511
	.p2align 4,,10
	.p2align 3
.L1532:
	leal	16(%rax,%rax), %eax
	movl	$44, %r11d
	cltq
	movw	%r11w, -1(%rcx,%rax)
	movl	24(%r14), %eax
	cmpl	%eax, 28(%r14)
	jne	.L1531
.L2177:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1531
.L1446:
	leal	16(%rax,%rax), %eax
	movl	$44, %r13d
	cltq
	movw	%r13w, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1445
.L2164:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1445
.L1412:
	leal	16(%rax,%rax), %eax
	movl	$44, %r10d
	cltq
	movw	%r10w, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1411
.L2158:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1411
.L1705:
	leal	16(%rax,%rax), %eax
	movl	$32, %ecx
	cltq
	movw	%cx, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1704
.L2207:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1704
.L1679:
	leal	16(%rax,%rax), %eax
	movl	$44, %ebx
	cltq
	movw	%bx, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1678
.L2202:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1678
.L1377:
	leal	16(%rax,%rax), %eax
	movl	$44, %r8d
	cltq
	movw	%r8w, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1376
.L2152:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1376
.L2137:
	movq	-96(%rbp), %r14
	movl	$2, %ebx
	jmp	.L1740
.L2234:
	call	__stack_chk_fail@PLT
.L2271:
	movl	%r13d, %esi
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal10FieldIndex13ForDescriptorEPNS0_7IsolateENS0_3MapEi.isra.0
	movq	-136(%rbp), %rdx
	movq	%r12, %rdi
	shrl	$6, %edx
	movl	%edx, %esi
	movq	%rax, %rdx
	andl	$7, %esi
	call	_ZN2v88internal8JSObject14FastPropertyAtENS0_6HandleIS1_EENS0_14RepresentationENS0_10FieldIndexE@PLT
	movq	%rax, %rsi
	jmp	.L1763
.L1525:
	movq	41088(%r12), %rax
	movq	%rax, -80(%rbp)
	cmpq	41096(%r12), %rax
	je	.L2278
.L1527:
	movq	-80(%rbp), %rdi
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdi)
	jmp	.L1526
.L1524:
	cmpl	$0, -72(%rbp)
	je	.L1623
	movq	(%r15), %r12
	movq	15(%rdx), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1553
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -80(%rbp)
.L1554:
	movq	(%r15), %rax
	movl	%ebx, -128(%rbp)
	xorl	%r12d, %r12d
	movq	%r14, -104(%rbp)
	movq	%r15, %r14
	addq	$37544, %rax
	movq	%rax, -120(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, -88(%rbp)
.L1565:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	%rax, %r8
	movq	-120(%rbp), %rax
	movq	(%rax), %rax
	cmpq	%rax, %r8
	jb	.L1556
.L1560:
	testl	%r12d, %r12d
	je	.L1561
	movq	40(%r14), %rax
	movl	28(%r14), %esi
	movl	16(%r14), %edi
	movq	(%rax), %rdx
	leal	1(%rsi), %eax
	testl	%edi, %edi
	jne	.L1562
	movl	%eax, 28(%r14)
	leal	16(%rsi), %eax
	cltq
	movb	$44, -1(%rdx,%rax)
	movl	24(%r14), %eax
	cmpl	%eax, 28(%r14)
	je	.L2181
.L1561:
	cmpq	$0, 72(%r14)
	je	.L1569
	movq	40(%r14), %rax
	movl	28(%r14), %esi
	movl	16(%r14), %ecx
	movq	(%rax), %rdx
	leal	1(%rsi), %eax
	testl	%ecx, %ecx
	jne	.L1566
	movl	%eax, 28(%r14)
	leal	16(%rsi), %eax
	cltq
	movb	$10, -1(%rdx,%rax)
	movl	24(%r14), %eax
	cmpl	%eax, 28(%r14)
	je	.L2182
.L1568:
	movl	80(%r14), %eax
	testl	%eax, %eax
	jle	.L1569
	xorl	%ebx, %ebx
.L1570:
	movq	72(%r14), %r15
	movl	16(%r14), %eax
	movzwl	(%r15), %edx
	testl	%eax, %eax
	jne	.L2188
.L2185:
	testw	%dx, %dx
	je	.L1573
	movl	28(%r14), %eax
.L1575:
	movq	40(%r14), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$2, %r15
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r14)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r14), %eax
	cmpl	24(%r14), %eax
	je	.L2279
	movzwl	(%r15), %edx
	testw	%dx, %dx
	jne	.L1575
.L1573:
	addl	$1, %ebx
	cmpl	%ebx, 80(%r14)
	jg	.L1570
.L1569:
	movq	-80(%rbp), %rdi
	leal	16(,%r12,8), %eax
	addl	$1, %r12d
	cltq
	movq	(%rdi), %rdx
	movq	%r14, %rdi
	movq	-1(%rax,%rdx), %xmm0
	call	_ZN2v88internal15JsonStringifier15SerializeDoubleEd
	cmpl	-72(%rbp), %r12d
	jne	.L1565
	movq	%r14, %r15
	movl	-128(%rbp), %ebx
	movq	-104(%rbp), %r14
	subl	$1, 80(%r15)
	movl	16(%r15), %esi
	movl	28(%r15), %eax
	jmp	.L1786
	.p2align 4,,10
	.p2align 3
.L2280:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%r15), %edx
.L2188:
	testw	%dx, %dx
	je	.L1573
	movl	28(%r14), %eax
.L1574:
	movq	40(%r14), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$2, %r15
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r14)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r14), %eax
	cmpl	24(%r14), %eax
	je	.L2280
	movzwl	(%r15), %edx
	testw	%dx, %dx
	jne	.L1574
	jmp	.L1573
.L2279:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%r15), %edx
	jmp	.L2185
.L1566:
	movl	%eax, 28(%r14)
	movq	-88(%rbp), %rdi
	movq	%rdx, -64(%rbp)
	movl	$10, %edx
	call	_ZN2v88internal16SeqTwoByteString19SeqTwoByteStringSetEit
	movl	24(%r14), %eax
	cmpl	%eax, 28(%r14)
	jne	.L1568
.L2182:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1568
.L1556:
	movq	(%r14), %rax
	leaq	37512(%rax), %rdi
	call	_ZN2v88internal10StackGuard16HandleInterruptsEv@PLT
	movq	(%r14), %rdx
	cmpq	%rax, 312(%rdx)
	jne	.L1560
	movq	-104(%rbp), %r14
	movl	$2, %ebx
	jmp	.L1508
.L1553:
	movq	41088(%r12), %rax
	movq	%rax, -80(%rbp)
	cmpq	41096(%r12), %rax
	je	.L2281
.L1555:
	movq	-80(%rbp), %rdi
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdi)
	jmp	.L1554
.L2278:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, -80(%rbp)
	jmp	.L1527
.L2281:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, -80(%rbp)
	jmp	.L1555
.L1749:
	movl	%eax, 28(%r15)
	leaq	-64(%rbp), %rdi
	movq	%rdx, -64(%rbp)
	movl	$123, %edx
	call	_ZN2v88internal16SeqTwoByteString19SeqTwoByteStringSetEit
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1751
.L2214:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1751
.L1781:
	movl	%eax, 28(%r15)
	leaq	-64(%rbp), %rdi
	movq	%rdx, -64(%rbp)
	movl	$125, %edx
	call	_ZN2v88internal16SeqTwoByteString19SeqTwoByteStringSetEit
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L1746
	jmp	.L2218
.L2128:
	cmpq	$0, 72(%r15)
	movl	28(%r15), %esi
	je	.L1767
	movq	40(%r15), %rdx
	cmpl	$0, 16(%r15)
	leal	1(%rsi), %eax
	movq	(%rdx), %rdx
	jne	.L1768
	movl	%eax, 28(%r15)
	leal	16(%rsi), %eax
	cltq
	movb	$10, -1(%rdx,%rax)
	movl	28(%r15), %esi
	cmpl	24(%r15), %esi
	je	.L2216
.L1770:
	movl	$0, -72(%rbp)
	movl	16(%r15), %eax
	movq	%r13, %r12
.L1771:
	movl	-72(%rbp), %edi
	cmpl	%edi, 80(%r15)
	jle	.L2138
	movq	72(%r15), %r13
	movzwl	0(%r13), %edx
	testl	%eax, %eax
	je	.L1773
	testw	%dx, %dx
	jne	.L1775
	jmp	.L1774
	.p2align 4,,10
	.p2align 3
.L2226:
	movzwl	0(%r13), %edx
	testw	%dx, %dx
	je	.L2217
.L1775:
	movq	40(%r15), %rax
	addq	$2, %r13
	movq	(%rax), %rdi
	leal	1(%rsi), %eax
	movl	%eax, 28(%r15)
	leal	16(%rsi,%rsi), %eax
	cltq
	movw	%dx, -1(%rdi,%rax)
	movl	28(%r15), %esi
	cmpl	24(%r15), %esi
	jne	.L2226
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	28(%r15), %esi
	jmp	.L2226
.L2270:
	movq	41088(%rcx), %rax
	movq	%rax, -72(%rbp)
	cmpq	41096(%rcx), %rax
	je	.L2282
.L1748:
	movq	-72(%rbp), %rdi
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%rdi)
	jmp	.L1747
.L2217:
	movl	16(%r15), %eax
.L1774:
	addl	$1, -72(%rbp)
	jmp	.L1771
.L2282:
	movq	%rcx, %rdi
	movq	%rsi, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rcx
	movq	%rax, -72(%rbp)
	jmp	.L1748
.L1773:
	testw	%dx, %dx
	jne	.L1776
	jmp	.L1774
	.p2align 4,,10
	.p2align 3
.L2228:
	movzwl	0(%r13), %edx
	testw	%dx, %dx
	je	.L2217
.L1776:
	movq	40(%r15), %rax
	addq	$2, %r13
	movq	(%rax), %rdi
	leal	1(%rsi), %eax
	movl	%eax, 28(%r15)
	leal	16(%rsi), %eax
	cltq
	movb	%dl, -1(%rdi,%rax)
	movl	28(%r15), %esi
	cmpl	24(%r15), %esi
	jne	.L2228
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	28(%r15), %esi
	jmp	.L2228
.L2138:
	movq	%r12, %r13
	jmp	.L1767
.L1768:
	movl	%eax, 28(%r15)
	leaq	-64(%rbp), %rdi
	movq	%rdx, -64(%rbp)
	movl	$10, %edx
	call	_ZN2v88internal16SeqTwoByteString19SeqTwoByteStringSetEit
	movl	28(%r15), %esi
	cmpl	24(%r15), %esi
	jne	.L1770
.L2216:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	28(%r15), %esi
	jmp	.L1770
.L1523:
	movq	(%r15), %rcx
	movq	23(%rdx), %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L1580
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -88(%rbp)
.L1581:
	cmpl	$0, -72(%rbp)
	je	.L1623
	movl	$0, -80(%rbp)
	movl	%ebx, -120(%rbp)
	movq	%r14, -104(%rbp)
	movq	%r15, %r14
.L1618:
	movq	(%r12), %rax
	movq	-88(%rbp), %rdi
	movq	23(%rax), %rbx
	cmpq	%rbx, (%rdi)
	jne	.L1583
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$2, %eax
	jne	.L1583
	movl	-80(%rbp), %r15d
	testl	%r15d, %r15d
	je	.L1585
	movl	28(%r14), %eax
	movl	16(%r14), %ebx
	movq	40(%r14), %rdx
	leal	1(%rax), %ecx
	testl	%ebx, %ebx
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r14)
	jne	.L1588
	addl	$16, %eax
	cltq
	movb	$44, -1(%rdx,%rax)
	movl	24(%r14), %eax
	cmpl	%eax, 28(%r14)
	je	.L2189
.L1585:
	cmpq	$0, 72(%r14)
	je	.L1596
	movl	28(%r14), %eax
	movl	16(%r14), %r10d
	movq	40(%r14), %rdx
	leal	1(%rax), %ecx
	testl	%r10d, %r10d
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r14)
	jne	.L1593
	addl	$16, %eax
	cltq
	movb	$10, -1(%rdx,%rax)
	movl	24(%r14), %eax
	cmpl	%eax, 28(%r14)
	je	.L2190
.L1595:
	movl	80(%r14), %eax
	testl	%eax, %eax
	jle	.L1596
	xorl	%r15d, %r15d
.L1597:
	movq	72(%r14), %rbx
	movl	16(%r14), %r8d
	movzwl	(%rbx), %edx
	testl	%r8d, %r8d
	je	.L1599
	testw	%dx, %dx
	je	.L1600
.L2192:
	movl	28(%r14), %eax
.L1601:
	movq	40(%r14), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$2, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r14)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r14), %eax
	cmpl	24(%r14), %eax
	je	.L2283
	movzwl	(%rbx), %edx
	testw	%dx, %dx
	jne	.L1601
.L1606:
	movl	80(%r14), %eax
.L1600:
	addl	$1, %r15d
	cmpl	%eax, %r15d
	jl	.L1597
.L1596:
	movq	(%r12), %rcx
	movl	-80(%rbp), %eax
	movq	(%r14), %rdx
	movq	15(%rcx), %rcx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2284
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L1609:
	movq	(%r14), %rdx
	movq	-80(%rbp), %rsi
	movq	41112(%rdx), %rdi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L1611
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L1612:
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal15JsonStringifier10Serialize_ILb0EEENS1_6ResultENS0_6HandleINS0_6ObjectEEEbS6_
	testl	%eax, %eax
	jne	.L1614
	movl	16(%r14), %edi
	testl	%edi, %edi
	je	.L1791
	leaq	.LC19(%rip), %rcx
	movl	28(%r14), %eax
	movl	$110, %edx
	movq	%rcx, %rbx
.L1616:
	movq	40(%r14), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r14)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r14), %eax
	cmpl	24(%r14), %eax
	je	.L2285
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L1616
.L1617:
	addl	$1, -80(%rbp)
	movl	-80(%rbp), %eax
	cmpl	-72(%rbp), %eax
	jne	.L1618
	movq	%r14, %r15
	movl	-120(%rbp), %ebx
	movq	-104(%rbp), %r14
	jmp	.L2118
	.p2align 4,,10
	.p2align 3
.L2283:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%rbx), %edx
	testw	%dx, %dx
	jne	.L2192
	jmp	.L1606
.L1599:
	testw	%dx, %dx
	je	.L1600
.L2191:
	movl	28(%r14), %eax
.L1602:
	movq	40(%r14), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$2, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r14)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r14), %eax
	cmpl	24(%r14), %eax
	je	.L2286
	movzwl	(%rbx), %edx
	testw	%dx, %dx
	jne	.L1602
	jmp	.L1606
.L2286:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%rbx), %edx
	testw	%dx, %dx
	jne	.L2191
	jmp	.L1606
.L1593:
	leal	16(%rax,%rax), %eax
	movl	$10, %r9d
	cltq
	movw	%r9w, -1(%rdx,%rax)
	movl	24(%r14), %eax
	cmpl	%eax, 28(%r14)
	jne	.L1595
.L2190:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1595
.L1588:
	leal	16(%rax,%rax), %eax
	movl	$44, %r11d
	cltq
	movw	%r11w, -1(%rdx,%rax)
	movl	24(%r14), %eax
	cmpl	%eax, 28(%r14)
	jne	.L1585
.L2189:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1585
.L2285:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	je	.L1617
	movl	28(%r14), %eax
	jmp	.L1616
.L1791:
	leaq	.LC19(%rip), %rcx
	movl	28(%r14), %eax
	movl	$110, %edx
	movq	%rcx, %rbx
.L1615:
	movq	40(%r14), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r14)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r14), %eax
	cmpl	24(%r14), %eax
	je	.L2287
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L1615
	jmp	.L1617
.L1614:
	cmpl	$1, %eax
	je	.L1617
	movq	-104(%rbp), %r14
	movl	%eax, %ebx
	jmp	.L1508
	.p2align 4,,10
	.p2align 3
.L2284:
	movq	41088(%rdx), %r15
	cmpq	41096(%rdx), %r15
	je	.L2288
.L1610:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r15)
	jmp	.L1609
.L1611:
	movq	41088(%rdx), %rcx
	cmpq	41096(%rdx), %rcx
	je	.L2289
.L1613:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rcx)
	jmp	.L1612
.L2287:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	je	.L1617
	movl	28(%r14), %eax
	jmp	.L1615
.L1583:
	movq	%r14, %r15
	movl	-120(%rbp), %ebx
	movq	-104(%rbp), %r14
	movl	-80(%rbp), %edi
	cmpl	%edi, -72(%rbp)
	jbe	.L2118
	jmp	.L1586
.L1580:
	movq	41088(%rcx), %rax
	movq	%rax, -88(%rbp)
	cmpq	41096(%rcx), %rax
	je	.L2290
.L1582:
	movq	-88(%rbp), %rdi
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%rdi)
	jmp	.L1581
.L2273:
	movq	%rdx, %rdi
	movq	%rsi, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L1757
.L2290:
	movq	%rcx, %rdi
	movq	%rsi, -104(%rbp)
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	-80(%rbp), %rcx
	movq	%rax, -88(%rbp)
	jmp	.L1582
.L1562:
	movl	%eax, 28(%r14)
	movq	-88(%rbp), %rdi
	movq	%rdx, -64(%rbp)
	movl	$44, %edx
	call	_ZN2v88internal16SeqTwoByteString19SeqTwoByteStringSetEit
	movl	24(%r14), %eax
	cmpl	%eax, 28(%r14)
	jne	.L1561
.L2181:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1561
.L2288:
	movq	%rdx, %rdi
	movq	%rsi, -136(%rbp)
	movq	%rdx, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	-128(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L1610
.L2289:
	movq	%rdx, %rdi
	movq	%rsi, -136(%rbp)
	movq	%rdx, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	-128(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L1613
	.cfi_endproc
.LFE19873:
	.size	_ZN2v88internal15JsonStringifier10Serialize_ILb1EEENS1_6ResultENS0_6HandleINS0_6ObjectEEEbS6_, .-_ZN2v88internal15JsonStringifier10Serialize_ILb1EEENS1_6ResultENS0_6HandleINS0_6ObjectEEEbS6_
	.section	.text._ZN2v88internal15JsonStringifier23SerializeJSReceiverSlowENS0_6HandleINS0_10JSReceiverEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JsonStringifier23SerializeJSReceiverSlowENS0_6HandleINS0_10JSReceiverEEE
	.type	_ZN2v88internal15JsonStringifier23SerializeJSReceiverSlowENS0_6HandleINS0_10JSReceiverEEE, @function
_ZN2v88internal15JsonStringifier23SerializeJSReceiverSlowENS0_6HandleINS0_10JSReceiverEEE:
.LFB17998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$248, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	56(%rdi), %rax
	movq	%rax, -256(%rbp)
	testq	%rax, %rax
	je	.L2387
.L2292:
	leaq	8(%r15), %rax
	movl	16(%r15), %r9d
	movq	40(%r15), %rdx
	movq	%rax, -288(%rbp)
	movl	28(%r15), %eax
	testl	%r9d, %r9d
	movq	(%rdx), %rdx
	leal	1(%rax), %ecx
	movl	%ecx, 28(%r15)
	je	.L2388
	leal	16(%rax,%rax), %eax
	movl	$123, %r8d
	cltq
	movw	%r8w, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2381
.L2297:
	movl	80(%r15), %edx
	leal	1(%rdx), %eax
	movl	%eax, 80(%r15)
	movq	-256(%rbp), %rax
	movq	(%rax), %rax
	movl	11(%rax), %edi
	testl	%edi, %edi
	jle	.L2299
	movb	$0, -241(%rbp)
	xorl	%r12d, %r12d
	leaq	-224(%rbp), %r14
.L2319:
	movq	(%r15), %rdx
	movq	15(%rax,%r12,8), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2300
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%r15), %r8
	movq	(%rax), %rsi
	movq	%rax, %r13
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	jbe	.L2389
.L2304:
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L2311
.L2313:
	movq	%r8, %rdi
	movl	$-1, %edx
	movq	%rbx, %rsi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-264(%rbp), %r8
	movq	%rax, %r9
.L2312:
	movq	0(%r13), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L2314
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L2314:
	movl	%eax, -224(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -212(%rbp)
	movq	%r8, -200(%rbp)
	movq	0(%r13), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	movq	%r13, %rax
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L2390
.L2315:
	movq	%r14, %rdi
	movq	%rax, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%rbx, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%r9, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L2310:
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2318
	movzbl	-241(%rbp), %edx
	movq	%r13, %rcx
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier10Serialize_ILb1EEENS1_6ResultENS0_6HandleINS0_6ObjectEEEbS6_
	movzbl	-241(%rbp), %edx
	xorl	$1, %edx
	cmpl	$1, %eax
	sete	%cl
	andb	%cl, %dl
	jne	.L2343
	cmpl	$2, %eax
	je	.L2318
.L2317:
	movq	-256(%rbp), %rax
	addq	$1, %r12
	movq	(%rax), %rax
	cmpl	%r12d, 11(%rax)
	jg	.L2319
	movl	28(%r15), %eax
	subl	$1, 80(%r15)
	cmpb	$0, -241(%rbp)
	movl	16(%r15), %esi
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	je	.L2322
	movq	(%rdx), %rdi
	cmpq	$0, 72(%r15)
	leaq	-1(%rdi), %rdx
	je	.L2321
	movl	%ecx, 28(%r15)
	testl	%esi, %esi
	je	.L2391
	leal	16(%rax,%rax), %eax
	movl	$10, %esi
	cltq
	movw	%si, -1(%rdi,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L2382
.L2324:
	movq	40(%r15), %rdx
	movl	80(%r15), %edi
	xorl	%ebx, %ebx
	movl	16(%r15), %esi
	movq	-288(%rbp), %r12
	movq	(%rdx), %rdx
	testl	%edi, %edi
	jle	.L2392
	.p2align 4,,10
	.p2align 3
.L2325:
	movq	72(%r15), %r13
	movzwl	0(%r13), %ecx
	testl	%esi, %esi
	je	.L2326
	testw	%cx, %cx
	jne	.L2328
	jmp	.L2327
	.p2align 4,,10
	.p2align 3
.L2334:
	movq	40(%r15), %rdx
	movzwl	0(%r13), %ecx
	movq	(%rdx), %rdx
	testw	%cx, %cx
	je	.L2333
.L2328:
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$2, %r13
	cltq
	movl	%esi, 28(%r15)
	movw	%cx, -1(%rdx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	jne	.L2334
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	28(%r15), %eax
	jmp	.L2334
	.p2align 4,,10
	.p2align 3
.L2388:
	addl	$16, %eax
	cltq
	movb	$123, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L2297
.L2381:
	leaq	8(%r15), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L2297
	.p2align 4,,10
	.p2align 3
.L2300:
	movq	41088(%rdx), %r13
	cmpq	41096(%rdx), %r13
	je	.L2393
.L2302:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, 0(%r13)
	movq	(%r15), %r8
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L2304
.L2389:
	movq	%rsi, -144(%rbp)
	movl	7(%rsi), %eax
	testb	$1, %al
	jne	.L2305
	testb	$2, %al
	jne	.L2304
.L2305:
	leaq	-144(%rbp), %r9
	leaq	-228(%rbp), %rsi
	movq	%r8, -272(%rbp)
	movq	%r9, %rdi
	movq	%r9, -264(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-272(%rbp), %r8
	testb	%al, %al
	je	.L2304
	movq	(%rbx), %rax
	movl	-228(%rbp), %edx
	movq	-264(%rbp), %r9
	testb	$1, %al
	jne	.L2307
.L2309:
	movq	%r8, %rdi
	movq	%rbx, %rsi
	movq	%r9, -280(%rbp)
	movl	%edx, -272(%rbp)
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-264(%rbp), %r8
	movl	-272(%rbp), %edx
	movq	-280(%rbp), %r9
.L2308:
	movabsq	$824633720832, %rcx
	movq	%r9, %rdi
	movq	%r8, -120(%rbp)
	movl	$3, -144(%rbp)
	movq	%rcx, -132(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	%edx, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r13, -112(%rbp)
	movdqa	-128(%rbp), %xmm1
	movdqa	-144(%rbp), %xmm0
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	jmp	.L2310
	.p2align 4,,10
	.p2align 3
.L2343:
	movb	%dl, -241(%rbp)
	jmp	.L2317
	.p2align 4,,10
	.p2align 3
.L2387:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$18, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb@PLT
	movq	%rax, -256(%rbp)
	testq	%rax, %rax
	jne	.L2292
	.p2align 4,,10
	.p2align 3
.L2318:
	movl	$2, %eax
.L2291:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2394
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2311:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L2313
	movq	%rbx, %r9
	jmp	.L2312
	.p2align 4,,10
	.p2align 3
.L2307:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L2309
	movq	%rbx, %rax
	jmp	.L2308
	.p2align 4,,10
	.p2align 3
.L2390:
	movq	%r13, %rsi
	movq	%r8, %rdi
	movq	%r9, -264(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-264(%rbp), %r9
	jmp	.L2315
	.p2align 4,,10
	.p2align 3
.L2393:
	movq	%rdx, %rdi
	movq	%rsi, -272(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %rsi
	movq	-264(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L2302
	.p2align 4,,10
	.p2align 3
.L2333:
	movl	16(%r15), %esi
	movl	80(%r15), %edi
.L2327:
	addl	$1, %ebx
	cmpl	%edi, %ebx
	jl	.L2325
	leal	1(%rax), %ecx
	subq	$1, %rdx
.L2321:
	movl	%ecx, 28(%r15)
	testl	%esi, %esi
	jne	.L2336
	addl	$16, %eax
	cltq
	movb	$125, (%rax,%rdx)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L2386
.L2385:
	movl	$1, %eax
	jmp	.L2291
	.p2align 4,,10
	.p2align 3
.L2336:
	leal	16(%rax,%rax), %eax
	movl	$125, %ecx
	cltq
	movw	%cx, (%rax,%rdx)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L2385
.L2386:
	movq	-288(%rbp), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L2385
.L2391:
	addl	$16, %eax
	cltq
	movb	$10, -1(%rdi,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	jne	.L2324
.L2382:
	movq	-288(%rbp), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	28(%r15), %eax
	jmp	.L2324
	.p2align 4,,10
	.p2align 3
.L2326:
	testw	%cx, %cx
	jne	.L2329
	jmp	.L2327
	.p2align 4,,10
	.p2align 3
.L2331:
	movq	40(%r15), %rdx
	movzwl	0(%r13), %ecx
	movq	(%rdx), %rdx
	testw	%cx, %cx
	je	.L2333
.L2329:
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$2, %r13
	cltq
	movl	%esi, 28(%r15)
	movb	%cl, -1(%rdx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	jne	.L2331
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	28(%r15), %eax
	jmp	.L2331
.L2299:
	movl	28(%r15), %eax
	movl	16(%r15), %esi
	movl	%edx, 80(%r15)
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	movq	(%rdx), %rdx
	subq	$1, %rdx
	jmp	.L2321
.L2322:
	movq	(%rdx), %rdx
	subq	$1, %rdx
	jmp	.L2321
.L2392:
	subq	$1, %rdx
	leal	1(%rax), %ecx
	jmp	.L2321
.L2394:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17998:
	.size	_ZN2v88internal15JsonStringifier23SerializeJSReceiverSlowENS0_6HandleINS0_10JSReceiverEEE, .-_ZN2v88internal15JsonStringifier23SerializeJSReceiverSlowENS0_6HandleINS0_10JSReceiverEEE
	.section	.text._ZN2v88internal15JsonStringifier27SerializeJSPrimitiveWrapperENS0_6HandleINS0_18JSPrimitiveWrapperEEENS2_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JsonStringifier27SerializeJSPrimitiveWrapperENS0_6HandleINS0_18JSPrimitiveWrapperEEENS2_INS0_6ObjectEEE
	.type	_ZN2v88internal15JsonStringifier27SerializeJSPrimitiveWrapperENS0_6HandleINS0_18JSPrimitiveWrapperEEENS2_INS0_6ObjectEEE, @function
_ZN2v88internal15JsonStringifier27SerializeJSPrimitiveWrapperENS0_6HandleINS0_18JSPrimitiveWrapperEEENS2_INS0_6ObjectEEE:
.LFB17992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	23(%rcx), %rdi
	movq	%rcx, %rsi
	movq	%rdi, %rbx
	notq	%rbx
	andl	$1, %ebx
	je	.L2555
.L2396:
	movq	0(%r13), %rdi
	testb	$1, %cl
	jne	.L2556
.L2404:
	movq	%r13, %rdi
	call	_ZN2v88internal15JsonStringifier12SerializeSmiENS0_3SmiE
	movl	%eax, %r15d
.L2395:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2557
	addq	$280, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2556:
	.cfi_restore_state
	movq	-1(%rcx), %rax
	cmpw	$65, 11(%rax)
	jne	.L2405
	movq	%rcx, %rsi
	xorl	%eax, %eax
.L2406:
	testb	%al, %al
	jne	.L2404
	movq	7(%rsi), %xmm0
	movq	%r13, %rdi
	movl	$1, %r15d
	call	_ZN2v88internal15JsonStringifier15SerializeDoubleEd
	jmp	.L2395
	.p2align 4,,10
	.p2align 3
.L2555:
	movq	-1(%rdi), %rax
	cmpw	$63, 11(%rax)
	ja	.L2397
	movq	0(%r13), %rdi
	testb	$1, %cl
	jne	.L2398
.L2401:
	movq	%r12, %rsi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L2553
.L2400:
	movq	%r8, %rsi
	movq	%r13, %rdi
	movl	$1, %r15d
	call	_ZN2v88internal15JsonStringifier15SerializeStringENS0_6HandleINS0_6StringEEE
	jmp	.L2395
	.p2align 4,,10
	.p2align 3
.L2397:
	movq	-1(%rdi), %rax
	cmpw	$65, 11(%rax)
	je	.L2396
	movq	-1(%rdi), %rax
	cmpw	$66, 11(%rax)
	je	.L2408
	movq	-1(%rdi), %rax
	cmpw	$67, 11(%rax)
	je	.L2558
	movq	0(%r13), %r14
.L2411:
	movq	41088(%r14), %rax
	addl	$1, 41104(%r14)
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -248(%rbp)
	movq	41096(%r14), %rax
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal15JsonStringifier9StackPushENS0_6HandleINS0_6ObjectEEES4_
	movl	%eax, %r15d
	cmpl	$1, %eax
	jne	.L2420
	cmpq	$0, 56(%r13)
	je	.L2559
.L2421:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal15JsonStringifier23SerializeJSReceiverSlowENS0_6HandleINS0_10JSReceiverEEE
	cmpl	$1, %eax
	je	.L2425
	movl	%eax, %r15d
.L2420:
	movq	-248(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-256(%rbp), %rax
	cmpq	41096(%r14), %rax
	je	.L2395
	movq	%rax, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L2395
	.p2align 4,,10
	.p2align 3
.L2408:
	movq	0(%r13), %r12
	xorl	%edx, %edx
	movl	$22, %esi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L2553:
	movl	$2, %r15d
	jmp	.L2395
	.p2align 4,,10
	.p2align 3
.L2405:
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	testq	%rax, %rax
	je	.L2553
	movq	(%rax), %rsi
	movq	%rsi, %rax
	notq	%rax
	andl	$1, %eax
	jmp	.L2406
	.p2align 4,,10
	.p2align 3
.L2398:
	movq	-1(%rcx), %rax
	cmpw	$63, 11(%rax)
	ja	.L2401
	jmp	.L2400
.L2433:
	movzbl	-281(%rbp), %ebx
	subl	$1, 80(%r13)
	movq	-296(%rbp), %r14
	movl	-288(%rbp), %r15d
	movq	40(%r13), %rdx
	testb	%bl, %bl
	je	.L2547
	cmpq	$0, 72(%r13)
	movq	(%rdx), %rcx
	je	.L2547
	movl	28(%r13), %eax
	cmpl	$0, 16(%r13)
	leal	1(%rax), %edx
	movl	%edx, 28(%r13)
	jne	.L2461
	addl	$16, %eax
	cltq
	movb	$10, -1(%rcx,%rax)
	movl	28(%r13), %eax
	cmpl	24(%r13), %eax
	je	.L2548
.L2463:
	xorl	%ebx, %ebx
	cmpl	$0, 80(%r13)
	movl	%ebx, %r12d
	jle	.L2546
.L2464:
	movq	72(%r13), %rbx
	movl	16(%r13), %ecx
	movzwl	(%rbx), %edx
	testl	%ecx, %ecx
	je	.L2549
	jmp	.L2551
.L2470:
	movzwl	(%rbx), %edx
.L2549:
	testw	%dx, %dx
	je	.L2467
.L2469:
	movq	40(%r13), %rcx
	leal	1(%rax), %edi
	addl	$16, %eax
	addq	$2, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%edi, 28(%r13)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r13), %eax
	cmpl	24(%r13), %eax
	jne	.L2470
	movq	-280(%rbp), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%rbx), %edx
	movl	28(%r13), %eax
	testw	%dx, %dx
	jne	.L2469
.L2467:
	addl	$1, %r12d
	cmpl	%r12d, 80(%r13)
	jg	.L2464
.L2546:
	movq	40(%r13), %rdx
.L2460:
	cmpl	$0, 16(%r13)
	movq	(%rdx), %rdx
	leal	1(%rax), %ecx
	jne	.L2560
	addl	$16, %eax
	movl	%ecx, 28(%r13)
	cltq
	movb	$125, -1(%rdx,%rax)
	movl	24(%r13), %eax
	cmpl	%eax, 28(%r13)
	jne	.L2425
.L2552:
	movq	-280(%rbp), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	.p2align 4,,10
	.p2align 3
.L2425:
	subq	$16, 96(%r13)
	jmp	.L2420
	.p2align 4,,10
	.p2align 3
.L2558:
	movq	0(%r13), %r14
	testb	$-2, 43(%rdi)
	jne	.L2411
	leaq	8(%r13), %r12
	cmpq	%rdi, 112(%r14)
	je	.L2412
	movl	16(%r13), %esi
	movl	$102, %edx
	leaq	.LC23(%rip), %rbx
	testl	%esi, %esi
	je	.L2561
.L2543:
	movl	28(%r13), %eax
.L2414:
	movq	40(%r13), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r13)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r13), %eax
	cmpl	24(%r13), %eax
	je	.L2562
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L2414
.L2415:
	movl	$1, %r15d
	jmp	.L2395
	.p2align 4,,10
	.p2align 3
.L2559:
	movq	(%r12), %rax
	movq	-1(%rax), %rdx
	cmpw	$1041, 11(%rdx)
	jbe	.L2421
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$2097152, %edx
	jne	.L2421
	movq	0(%r13), %rcx
	movq	15(%rax), %rdx
	cmpq	%rdx, 288(%rcx)
	je	.L2422
	cmpq	%rdx, 1016(%rcx)
	jne	.L2421
.L2422:
	movq	-1(%rax), %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L2563
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -264(%rbp)
.L2426:
	leaq	8(%r13), %rax
	movq	40(%r13), %rdx
	cmpl	$0, 16(%r13)
	movq	%rax, -280(%rbp)
	movl	28(%r13), %eax
	movq	(%rdx), %rdx
	leal	1(%rax), %ecx
	movl	%ecx, 28(%r13)
	jne	.L2428
	addl	$16, %eax
	cltq
	movb	$123, -1(%rdx,%rax)
	movl	24(%r13), %eax
	cmpl	%eax, 28(%r13)
	je	.L2544
.L2430:
	movq	-264(%rbp), %rax
	movl	$24, %edx
	xorl	%r10d, %r10d
	addl	$1, 80(%r13)
	movb	%bl, -281(%rbp)
	movl	%r10d, %ebx
	movq	%r14, -296(%rbp)
	movq	(%rax), %rdi
	movq	%rdx, %r14
	movl	%r15d, -288(%rbp)
	movq	%r12, -272(%rbp)
	jmp	.L2432
.L2567:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L2435:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L2545
	movq	-264(%rbp), %rax
	movq	(%rax), %rdi
	movq	39(%rdi), %rax
	movq	7(%r14,%rax), %rax
	movq	%rax, %rdx
	sarq	$32, %rdx
	btq	$36, %rax
	jc	.L2437
	testb	$2, %dl
	jne	.L2442
	movq	-272(%rbp), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	cmpq	%rax, %rdi
	je	.L2564
.L2442:
	movq	(%r12), %rax
	movq	0(%r13), %rbx
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L2565
.L2443:
	movq	-272(%rbp), %rax
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L2450
.L2452:
	movq	-272(%rbp), %rsi
	movl	$-1, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %rdx
.L2451:
	movq	(%r12), %rsi
	movl	$3, %eax
	movq	-1(%rsi), %rdi
	cmpw	$64, 11(%rdi)
	jne	.L2453
	movl	11(%rsi), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L2453:
	movl	%eax, -224(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -212(%rbp)
	movq	%rbx, -200(%rbp)
	movq	(%r12), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %esi
	movq	%r12, %rax
	andl	$-32, %esi
	cmpl	$32, %esi
	je	.L2566
.L2454:
	movq	%rax, -192(%rbp)
	movq	-272(%rbp), %rax
	leaq	-224(%rbp), %rdi
	movq	%rdi, -304(%rbp)
	movq	$0, -184(%rbp)
	movq	%rax, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%rdx, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movq	-304(%rbp), %rdi
.L2449:
	movl	$1, %esi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2456
.L2455:
	movzbl	-281(%rbp), %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	movl	%edx, %ebx
	call	_ZN2v88internal15JsonStringifier10Serialize_ILb1EEENS1_6ResultENS0_6HandleINS0_6ObjectEEEbS6_
	movl	%ebx, %edx
	xorl	$1, %edx
	cmpl	$1, %eax
	sete	%cl
	andb	%cl, %dl
	jne	.L2485
	cmpl	$2, %eax
	je	.L2456
.L2545:
	movq	-264(%rbp), %rax
	movq	(%rax), %rdi
.L2437:
	addq	$24, %r14
	movl	%r15d, %ebx
.L2432:
	movl	15(%rdi), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	cmpl	%eax, %ebx
	jge	.L2433
	movq	0(%r13), %rdx
	movq	39(%rdi), %rax
	leal	1(%rbx), %r15d
	movq	-1(%r14,%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	jne	.L2567
	movq	41088(%rdx), %r12
	cmpq	41096(%rdx), %r12
	je	.L2568
.L2436:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r12)
	jmp	.L2435
.L2562:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L2543
	jmp	.L2415
.L2412:
	movl	16(%r13), %eax
	movl	$116, %edx
	leaq	.LC24(%rip), %rbx
	testl	%eax, %eax
	jne	.L2543
	.p2align 4,,10
	.p2align 3
.L2413:
	movl	28(%r13), %eax
.L2478:
	movq	40(%r13), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r13)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r13), %eax
	cmpl	24(%r13), %eax
	je	.L2569
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L2478
	jmp	.L2415
.L2569:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L2413
	jmp	.L2415
.L2561:
	movl	$102, %edx
	leaq	.LC23(%rip), %rbx
	jmp	.L2413
.L2485:
	movb	%dl, -281(%rbp)
	jmp	.L2545
.L2565:
	movq	%rax, -144(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L2444
	testb	$2, %al
	jne	.L2443
.L2444:
	leaq	-144(%rbp), %r9
	leaq	-228(%rbp), %rsi
	movq	%r9, %rdi
	movq	%r9, -304(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	testb	%al, %al
	je	.L2443
	movq	-272(%rbp), %rax
	movl	-228(%rbp), %edx
	movq	-304(%rbp), %r9
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L2446
.L2448:
	movq	-272(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%r9, -312(%rbp)
	movl	%edx, -304(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movl	-304(%rbp), %edx
	movq	-312(%rbp), %r9
.L2447:
	movabsq	$824633720832, %rcx
	movq	%r9, %rdi
	movq	%rbx, -120(%rbp)
	movq	%rcx, -132(%rbp)
	movq	-272(%rbp), %rcx
	movl	$3, -144(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	%edx, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r12, -112(%rbp)
	movdqa	-128(%rbp), %xmm2
	leaq	-224(%rbp), %rdi
	movdqa	-144(%rbp), %xmm1
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm2, -208(%rbp)
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	jmp	.L2449
.L2568:
	movq	%rdx, %rdi
	movq	%rsi, -312(%rbp)
	movq	%rdx, -304(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-312(%rbp), %rsi
	movq	-304(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L2436
.L2456:
	movq	-296(%rbp), %r14
	movl	$2, %r15d
	jmp	.L2420
.L2564:
	movl	%ebx, %esi
	movq	%rdx, -304(%rbp)
	call	_ZN2v88internal10FieldIndex13ForDescriptorEPNS0_7IsolateENS0_3MapEi.isra.0
	movq	-304(%rbp), %rdx
	movq	-272(%rbp), %rdi
	shrl	$6, %edx
	andl	$7, %edx
	movl	%edx, %esi
	movq	%rax, %rdx
	call	_ZN2v88internal8JSObject14FastPropertyAtENS0_6HandleIS1_EENS0_14RepresentationENS0_10FieldIndexE@PLT
	movq	%rax, %rsi
	jmp	.L2455
.L2446:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L2448
	movq	-272(%rbp), %rax
	jmp	.L2447
.L2428:
	leal	16(%rax,%rax), %eax
	cltq
	movw	$123, -1(%rdx,%rax)
	movl	24(%r13), %eax
	cmpl	%eax, 28(%r13)
	jne	.L2430
.L2544:
	leaq	8(%r13), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L2430
.L2563:
	movq	41088(%rcx), %rax
	movq	%rax, -264(%rbp)
	cmpq	41096(%rcx), %rax
	je	.L2570
.L2427:
	movq	-264(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%rdx)
	jmp	.L2426
.L2450:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L2452
	movq	-272(%rbp), %rdx
	jmp	.L2451
.L2571:
	movq	-280(%rbp), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	28(%r13), %eax
.L2554:
	movzwl	(%rbx), %edx
.L2551:
	testw	%dx, %dx
	je	.L2467
	movq	40(%r13), %rcx
	leal	1(%rax), %edi
	leal	16(%rax,%rax), %eax
	addq	$2, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%edi, 28(%r13)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r13), %eax
	cmpl	24(%r13), %eax
	jne	.L2554
	jmp	.L2571
.L2557:
	call	__stack_chk_fail@PLT
.L2566:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -304(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-304(%rbp), %rdx
	jmp	.L2454
.L2547:
	movl	28(%r13), %eax
	jmp	.L2460
.L2560:
	leal	16(%rax,%rax), %eax
	movl	%ecx, 28(%r13)
	cltq
	movw	$125, -1(%rdx,%rax)
	movl	24(%r13), %eax
	cmpl	%eax, 28(%r13)
	jne	.L2425
	jmp	.L2552
.L2461:
	leal	16(%rax,%rax), %eax
	cltq
	movw	$10, -1(%rcx,%rax)
	movl	28(%r13), %eax
	cmpl	24(%r13), %eax
	jne	.L2463
.L2548:
	movq	-280(%rbp), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	28(%r13), %eax
	jmp	.L2463
.L2570:
	movq	%rcx, %rdi
	movq	%rsi, -280(%rbp)
	movq	%rcx, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-280(%rbp), %rsi
	movq	-272(%rbp), %rcx
	movq	%rax, -264(%rbp)
	jmp	.L2427
	.cfi_endproc
.LFE17992:
	.size	_ZN2v88internal15JsonStringifier27SerializeJSPrimitiveWrapperENS0_6HandleINS0_18JSPrimitiveWrapperEEENS2_INS0_6ObjectEEE, .-_ZN2v88internal15JsonStringifier27SerializeJSPrimitiveWrapperENS0_6HandleINS0_18JSPrimitiveWrapperEEENS2_INS0_6ObjectEEE
	.section	.text._ZN2v88internal15JsonStringifier10Serialize_ILb0EEENS1_6ResultENS0_6HandleINS0_6ObjectEEEbS6_,"axG",@progbits,_ZN2v88internal15JsonStringifier10Serialize_ILb0EEENS1_6ResultENS0_6HandleINS0_6ObjectEEEbS6_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15JsonStringifier10Serialize_ILb0EEENS1_6ResultENS0_6HandleINS0_6ObjectEEEbS6_
	.type	_ZN2v88internal15JsonStringifier10Serialize_ILb0EEENS1_6ResultENS0_6HandleINS0_6ObjectEEEbS6_, @function
_ZN2v88internal15JsonStringifier10Serialize_ILb0EEENS1_6ResultENS0_6HandleINS0_6ObjectEEEbS6_:
.LFB19872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$280, %rsp
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	%rax, %r10
	movq	37544(%r14), %rax
	cmpq	%rax, %r10
	jb	.L3047
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L3048
.L2576:
	cmpq	$0, 64(%r15)
	je	.L2580
	movq	%rbx, %rsi
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier21ApplyReplacerFunctionENS0_6HandleINS0_6ObjectEEES4_S4_
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L3039
.L2580:
	movq	(%rbx), %rsi
	movq	%rsi, %r14
	notq	%r14
	andl	$1, %r14d
	jne	.L3049
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	cmpw	$67, %ax
	je	.L2583
	ja	.L2584
	cmpw	$65, %ax
	je	.L2585
	cmpw	$66, %ax
	jne	.L3050
	movq	(%r15), %r12
	xorl	%edx, %edx
	movl	$22, %esi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L3039:
	movl	$2, %r12d
	.p2align 4,,10
	.p2align 3
.L2572:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3051
	addq	$280, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3049:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier12SerializeSmiENS0_3SmiE
	movl	%eax, %r12d
	jmp	.L2572
	.p2align 4,,10
	.p2align 3
.L3050:
	xorl	%r12d, %r12d
	cmpw	$64, %ax
	je	.L2572
.L2587:
	testb	$1, %sil
	jne	.L3052
.L2753:
	movq	(%r15), %r9
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	41088(%r9), %rax
	addl	$1, 41104(%r9)
	movq	%r9, -264(%rbp)
	movq	%rax, -248(%rbp)
	movq	41096(%r9), %rax
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal15JsonStringifier9StackPushENS0_6HandleINS0_6ObjectEEES4_
	movq	-264(%rbp), %r9
	cmpl	$1, %eax
	movl	%eax, %r12d
	jne	.L2756
	cmpq	$0, 56(%r15)
	je	.L3053
.L2758:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%r9, -264(%rbp)
	call	_ZN2v88internal15JsonStringifier23SerializeJSReceiverSlowENS0_6HandleINS0_10JSReceiverEEE
	movq	-264(%rbp), %r9
	cmpl	$1, %eax
	je	.L2762
	movl	%eax, %r12d
.L2756:
	movq	-248(%rbp), %rax
	subl	$1, 41104(%r9)
	movq	%rax, 41088(%r9)
	movq	-256(%rbp), %rax
	cmpq	41096(%r9), %rax
	je	.L2572
	movq	%rax, 41096(%r9)
	movq	%r9, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L2572
	.p2align 4,,10
	.p2align 3
.L2584:
	cmpw	$1041, %ax
	je	.L2588
	cmpw	$1061, %ax
	jne	.L2587
	movq	(%r15), %r14
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	41088(%r14), %rax
	addl	$1, 41104(%r14)
	movq	%rax, -256(%rbp)
	movq	41096(%r14), %rax
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal15JsonStringifier9StackPushENS0_6HandleINS0_6ObjectEEES4_
	movl	%eax, %r12d
	cmpl	$1, %eax
	je	.L3054
.L2611:
	movq	-256(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-248(%rbp), %rax
	cmpq	41096(%r14), %rax
	je	.L2572
	movq	%rax, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L2572
	.p2align 4,,10
	.p2align 3
.L3047:
	movq	(%r15), %rax
	leaq	37512(%rax), %rdi
	call	_ZN2v88internal10StackGuard16HandleInterruptsEv@PLT
	movq	(%r15), %rdx
	cmpq	312(%rdx), %rax
	je	.L3039
	movq	(%r12), %rax
	testb	$1, %al
	je	.L2576
.L3048:
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L3055
.L2577:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier19ApplyToJsonFunctionENS0_6HandleINS0_6ObjectEEES4_
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L2576
	jmp	.L3039
	.p2align 4,,10
	.p2align 3
.L3055:
	movq	-1(%rax), %rax
	cmpw	$66, 11(%rax)
	jne	.L2576
	jmp	.L2577
	.p2align 4,,10
	.p2align 3
.L2588:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier27SerializeJSPrimitiveWrapperENS0_6HandleINS0_18JSPrimitiveWrapperEEENS2_INS0_6ObjectEEE
	movl	%eax, %r12d
	jmp	.L2572
	.p2align 4,,10
	.p2align 3
.L2583:
	movslq	43(%rsi), %rax
	cmpb	$1, %al
	je	.L2590
	cmpb	$3, %al
	je	.L2591
	xorl	%r12d, %r12d
	testb	%al, %al
	jne	.L2572
	movl	16(%r15), %r9d
	leaq	8(%r15), %r12
	testl	%r9d, %r9d
	je	.L2817
	movl	28(%r15), %eax
	movl	$102, %edx
	leaq	.LC23(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L2593:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L3056
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L2593
	jmp	.L3040
	.p2align 4,,10
	.p2align 3
.L2585:
	movq	7(%rsi), %xmm0
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier15SerializeDoubleEd
	movl	%eax, %r12d
	jmp	.L2572
	.p2align 4,,10
	.p2align 3
.L2591:
	movl	16(%r15), %edi
	leaq	8(%r15), %r12
	testl	%edi, %edi
	je	.L2819
	movl	28(%r15), %eax
	movl	$110, %edx
	leaq	.LC19(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L2606:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L3057
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L2606
	jmp	.L3040
	.p2align 4,,10
	.p2align 3
.L3054:
	movq	(%rbx), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L2612
	sarq	$32, %rax
	movl	%eax, -264(%rbp)
	js	.L2614
.L2615:
	movl	28(%r15), %eax
	movl	16(%r15), %esi
	leaq	8(%r15), %r13
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%esi, %esi
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L2621
	addl	$16, %eax
	cltq
	movb	$91, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L3012
.L2623:
	addl	$1, 80(%r15)
	cmpq	$0, 64(%r15)
	je	.L3058
.L2625:
	movl	-264(%rbp), %esi
	testl	%esi, %esi
	jne	.L3059
.L2728:
	movq	40(%r15), %rcx
	subl	$1, 80(%r15)
	movl	28(%r15), %eax
	movl	16(%r15), %edi
	movq	(%rcx), %rcx
	leal	1(%rax), %edx
	subq	$1, %rcx
.L2730:
	movl	%edx, 28(%r15)
	testl	%edi, %edi
	jne	.L2744
	addl	$16, %eax
	cltq
	movb	$93, (%rax,%rcx)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L3030
.L2746:
	subq	$16, 96(%r15)
	jmp	.L2611
	.p2align 4,,10
	.p2align 3
.L3052:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L3060
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier15SerializeStringENS0_6HandleINS0_6StringEEE
	.p2align 4,,10
	.p2align 3
.L3040:
	movl	$1, %r12d
	jmp	.L2572
	.p2align 4,,10
	.p2align 3
.L2590:
	movl	16(%r15), %r8d
	leaq	8(%r15), %r12
	testl	%r8d, %r8d
	je	.L2818
	movl	28(%r15), %eax
	movl	$116, %edx
	leaq	.LC24(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L2600:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L3061
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L2600
	jmp	.L3040
.L3060:
	movq	-1(%rsi), %rax
	xorl	%r12d, %r12d
	testb	$2, 13(%rax)
	jne	.L2572
	movq	-1(%rsi), %rax
	cmpw	$1024, 11(%rax)
	jne	.L2753
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier16SerializeJSProxyENS0_6HandleINS0_7JSProxyEEENS2_INS0_6ObjectEEE
	movl	%eax, %r12d
	jmp	.L2572
	.p2align 4,,10
	.p2align 3
.L3056:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	je	.L3040
	movl	28(%r15), %eax
	jmp	.L2593
	.p2align 4,,10
	.p2align 3
.L3057:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	je	.L3040
	movl	28(%r15), %eax
	jmp	.L2606
	.p2align 4,,10
	.p2align 3
.L3061:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	je	.L3040
	movl	28(%r15), %eax
	jmp	.L2600
.L2770:
	movzbl	-288(%rbp), %r14d
	subl	$1, 80(%r15)
	movq	-304(%rbp), %r9
	movl	-312(%rbp), %r12d
	movq	40(%r15), %rdx
	testb	%r14b, %r14b
	jne	.L3001
	movl	28(%r15), %eax
.L2794:
	leal	1(%rax), %ecx
	cmpl	$0, 16(%r15)
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L2808
	addl	$16, %eax
	cltq
	movb	$125, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L2762
.L3035:
	movq	-280(%rbp), %rdi
	movq	%r9, -264(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-264(%rbp), %r9
.L2762:
	subq	$16, 96(%r15)
	jmp	.L2756
.L2621:
	leal	16(%rax,%rax), %eax
	movl	$91, %ecx
	cltq
	movw	%cx, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L2623
.L3012:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L2623
.L2817:
	movl	$102, %edx
	leaq	.LC23(%rip), %rbx
.L3009:
	movl	28(%r15), %eax
.L2592:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L3062
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L2592
	jmp	.L3040
	.p2align 4,,10
	.p2align 3
.L3062:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L3009
	jmp	.L3040
.L2819:
	movl	$110, %edx
	leaq	.LC19(%rip), %rbx
.L3011:
	movl	28(%r15), %eax
.L2605:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L3063
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L2605
	jmp	.L3040
	.p2align 4,,10
	.p2align 3
.L3063:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L3011
	jmp	.L3040
.L2818:
	movl	$116, %edx
	leaq	.LC24(%rip), %rbx
.L3010:
	movl	28(%r15), %eax
.L2599:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L3064
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L2599
	jmp	.L3040
	.p2align 4,,10
	.p2align 3
.L3064:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L3010
	jmp	.L3040
.L2744:
	leal	16(%rax,%rax), %eax
	movl	$93, %edx
	cltq
	movw	%dx, (%rax,%rcx)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L2746
.L3030:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L2746
.L2612:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L3065
.L2614:
	leaq	.LC25(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3059:
	xorl	%edx, %edx
.L2691:
	movl	-264(%rbp), %ecx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier22SerializeArrayLikeSlowENS0_6HandleINS0_10JSReceiverEEEjj
	cmpl	$1, %eax
	je	.L2994
	movl	%eax, %r12d
	jmp	.L2611
.L3053:
	movq	(%rbx), %rax
	movq	-1(%rax), %rdx
	cmpw	$1041, 11(%rdx)
	jbe	.L2758
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$2097152, %edx
	jne	.L2758
	movq	(%r15), %rcx
	movq	15(%rax), %rdx
	cmpq	%rdx, 288(%rcx)
	je	.L2759
	cmpq	%rdx, 1016(%rcx)
	jne	.L2758
.L2759:
	movq	-1(%rax), %r13
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L3066
	movq	%r13, %rsi
	movq	%r9, -272(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-272(%rbp), %r9
	movq	%rax, -264(%rbp)
.L2763:
	leaq	8(%r15), %rax
	movq	40(%r15), %rdx
	cmpl	$0, 16(%r15)
	movq	%rax, -280(%rbp)
	movl	28(%r15), %eax
	movq	(%rdx), %rdx
	leal	1(%rax), %ecx
	movl	%ecx, 28(%r15)
	jne	.L2765
	addl	$16, %eax
	cltq
	movb	$123, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L3031
.L2767:
	movq	-264(%rbp), %rax
	movl	$24, %edx
	xorl	%r13d, %r13d
	addl	$1, 80(%r15)
	movb	%r14b, -288(%rbp)
	movl	%r13d, %r14d
	movl	%r12d, -312(%rbp)
	movq	(%rax), %rdi
	movq	%rdx, %r12
	movq	%r9, -304(%rbp)
	jmp	.L2769
.L3070:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L2772:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L3032
	movq	-264(%rbp), %rax
	movq	(%rax), %rdi
	movq	39(%rdi), %rax
	movq	7(%r12,%rax), %rax
	movq	%rax, %rdx
	sarq	$32, %rdx
	btq	$36, %rax
	jc	.L2774
	testb	$2, %dl
	jne	.L2779
	movq	(%rbx), %rax
	movq	-1(%rax), %rax
	cmpq	%rax, %rdi
	je	.L3067
.L2779:
	movq	0(%r13), %rax
	movq	(%r15), %r14
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L3068
.L2780:
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L2787
.L2789:
	orl	$-1, %edx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %rdx
.L2788:
	movq	0(%r13), %rcx
	movl	$3, %eax
	movq	-1(%rcx), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L2790
	movl	11(%rcx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L2790:
	movl	%eax, -224(%rbp)
	movl	$3, %eax
	salq	$38, %rax
	movq	%r14, -200(%rbp)
	movq	%rax, -212(%rbp)
	movq	0(%r13), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %ecx
	movq	%r13, %rax
	andl	$-32, %ecx
	cmpl	$32, %ecx
	je	.L3069
.L2791:
	leaq	-224(%rbp), %r14
	movq	%rax, -192(%rbp)
	movq	%r14, %rdi
	movq	$0, -184(%rbp)
	movq	%rbx, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%rdx, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L2786:
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2793
.L2792:
	movzbl	-288(%rbp), %edx
	movq	%r13, %rcx
	movq	%r15, %rdi
	movl	%edx, %r14d
	call	_ZN2v88internal15JsonStringifier10Serialize_ILb1EEENS1_6ResultENS0_6HandleINS0_6ObjectEEEbS6_
	movl	%r14d, %edx
	xorl	$1, %edx
	cmpl	$1, %eax
	sete	%cl
	andb	%cl, %dl
	jne	.L2828
	cmpl	$2, %eax
	je	.L2793
.L3032:
	movq	-264(%rbp), %rax
	movq	(%rax), %rdi
.L2774:
	movl	-272(%rbp), %r14d
	addq	$24, %r12
.L2769:
	movl	15(%rdi), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	cmpl	%eax, %r14d
	jge	.L2770
	leal	1(%r14), %eax
	movq	(%r15), %rdx
	movl	%eax, -272(%rbp)
	movq	39(%rdi), %rax
	movq	-1(%r12,%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	jne	.L3070
	movq	41088(%rdx), %r13
	cmpq	41096(%rdx), %r13
	je	.L3071
.L2773:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, 0(%r13)
	jmp	.L2772
.L2627:
	cmpl	$0, -264(%rbp)
	je	.L2728
	movq	(%r15), %rbx
	movq	15(%rdx), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2656
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -272(%rbp)
.L2657:
	movq	(%r15), %rax
	movq	%r14, -288(%rbp)
	movl	%r12d, -296(%rbp)
	addq	$37544, %rax
	movq	%rax, -280(%rbp)
	xorl	%eax, %eax
	movl	%eax, %r12d
.L2668:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	%rax, %r8
	movq	-280(%rbp), %rax
	movq	(%rax), %rax
	cmpq	%rax, %r8
	jb	.L2659
.L2663:
	testl	%r12d, %r12d
	je	.L2664
	movl	28(%r15), %eax
	movl	16(%r15), %r8d
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	testl	%r8d, %r8d
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	jne	.L2665
	addl	$16, %eax
	cltq
	movb	$44, -1(%rcx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L3017
.L2664:
	cmpq	$0, 72(%r15)
	je	.L2672
	movl	28(%r15), %eax
	movl	16(%r15), %edi
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	testl	%edi, %edi
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	jne	.L2669
	addl	$16, %eax
	cltq
	movb	$10, -1(%rcx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L3018
.L2671:
	movl	80(%r15), %eax
	testl	%eax, %eax
	jle	.L2672
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L2673:
	movq	72(%r15), %r14
	movl	16(%r15), %ecx
	movzwl	(%r14), %edx
	testl	%ecx, %ecx
	je	.L2675
	testw	%dx, %dx
	je	.L2676
.L3020:
	movl	28(%r15), %eax
.L2677:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$2, %r14
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L3072
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L2677
.L2682:
	movl	80(%r15), %eax
.L2676:
	addl	$1, %ebx
	cmpl	%eax, %ebx
	jl	.L2673
.L2672:
	movq	-272(%rbp), %rbx
	leal	16(,%r12,8), %eax
	movq	%r15, %rdi
	addl	$1, %r12d
	cltq
	movq	(%rbx), %rcx
	movq	-1(%rax,%rcx), %xmm0
	call	_ZN2v88internal15JsonStringifier15SerializeDoubleEd
	cmpl	-264(%rbp), %r12d
	jne	.L2668
.L3045:
	movq	-288(%rbp), %r14
	movl	-296(%rbp), %r12d
.L2994:
	movq	40(%r15), %rcx
	subl	$1, 80(%r15)
	movl	28(%r15), %eax
	cmpq	$0, 72(%r15)
	movq	(%rcx), %rsi
	movl	16(%r15), %edi
	leal	1(%rax), %edx
	leaq	-1(%rsi), %rcx
	je	.L2730
	movl	%edx, 28(%r15)
	testl	%edi, %edi
	jne	.L2731
	addl	$16, %eax
	cltq
	movb	$10, -1(%rsi,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L3026
.L2732:
	movq	40(%r15), %rdx
	movl	80(%r15), %esi
	xorl	%ebx, %ebx
	movl	16(%r15), %edi
	movq	(%rdx), %rcx
	testl	%esi, %esi
	jle	.L3029
	movl	%r12d, -264(%rbp)
	.p2align 4,,10
	.p2align 3
.L2733:
	movq	72(%r15), %r12
	movzwl	(%r12), %r8d
	testl	%edi, %edi
	je	.L2734
	testw	%r8w, %r8w
	jne	.L2736
	jmp	.L2735
	.p2align 4,,10
	.p2align 3
.L2742:
	movq	40(%r15), %rcx
	movzwl	(%r12), %r8d
	movq	(%rcx), %rcx
	testw	%r8w, %r8w
	je	.L2741
.L2736:
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$2, %r12
	cltq
	movl	%esi, 28(%r15)
	movw	%r8w, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	jne	.L2742
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	28(%r15), %eax
	jmp	.L2742
	.p2align 4,,10
	.p2align 3
.L2741:
	movl	16(%r15), %edi
	movl	80(%r15), %esi
.L2735:
	addl	$1, %ebx
	cmpl	%esi, %ebx
	jl	.L2733
	movl	-264(%rbp), %r12d
.L3029:
	leal	1(%rax), %edx
	subq	$1, %rcx
	jmp	.L2730
	.p2align 4,,10
	.p2align 3
.L2734:
	testw	%r8w, %r8w
	jne	.L2737
	jmp	.L2735
	.p2align 4,,10
	.p2align 3
.L2739:
	movq	40(%r15), %rcx
	movzwl	(%r12), %r8d
	movq	(%rcx), %rcx
	testw	%r8w, %r8w
	je	.L2741
.L2737:
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$2, %r12
	cltq
	movl	%esi, 28(%r15)
	movb	%r8b, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	jne	.L2739
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	28(%r15), %eax
	jmp	.L2739
.L3058:
	movq	(%rbx), %rdx
	movq	-1(%rdx), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$2, %eax
	je	.L2626
	cmpb	$4, %al
	je	.L2627
	testb	%al, %al
	jne	.L2625
	movq	(%r15), %rbx
	movq	15(%rdx), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2628
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -272(%rbp)
.L2629:
	cmpl	$0, -264(%rbp)
	movq	(%r15), %rax
	je	.L2728
	addq	$37544, %rax
	movq	%r14, -288(%rbp)
	movq	%r15, %r14
	movq	%rax, -280(%rbp)
	xorl	%eax, %eax
	movl	%r12d, -296(%rbp)
	movl	%eax, %r12d
	.p2align 4,,10
	.p2align 3
.L2639:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	%rax, %r8
	movq	-280(%rbp), %rax
	movq	(%rax), %rax
	cmpq	%rax, %r8
	jb	.L3073
.L2632:
	testl	%r12d, %r12d
	je	.L2634
	movl	28(%r14), %eax
	movl	16(%r14), %r15d
	movq	40(%r14), %rcx
	leal	1(%rax), %esi
	testl	%r15d, %r15d
	movq	(%rcx), %rcx
	movl	%esi, 28(%r14)
	jne	.L2635
	addl	$16, %eax
	cltq
	movb	$44, -1(%rcx,%rax)
	movl	24(%r14), %eax
	cmpl	%eax, 28(%r14)
	je	.L3013
.L2634:
	cmpq	$0, 72(%r14)
	je	.L2643
	movl	28(%r14), %eax
	movl	16(%r14), %r11d
	movq	40(%r14), %rcx
	leal	1(%rax), %esi
	testl	%r11d, %r11d
	movq	(%rcx), %rcx
	movl	%esi, 28(%r14)
	jne	.L2640
	addl	$16, %eax
	cltq
	movb	$10, -1(%rcx,%rax)
	movl	24(%r14), %eax
	cmpl	%eax, 28(%r14)
	je	.L3014
.L2642:
	movl	80(%r14), %eax
	testl	%eax, %eax
	jle	.L2643
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L2644:
	movq	72(%r14), %r15
	movl	16(%r14), %r9d
	movzwl	(%r15), %edx
	testl	%r9d, %r9d
	je	.L2646
	testw	%dx, %dx
	je	.L2647
.L3016:
	movl	28(%r14), %eax
.L2648:
	movq	40(%r14), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$2, %r15
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r14)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r14), %eax
	cmpl	24(%r14), %eax
	je	.L3074
	movzwl	(%r15), %edx
	testw	%dx, %dx
	jne	.L2648
.L2653:
	movl	80(%r14), %eax
.L2647:
	addl	$1, %ebx
	cmpl	%eax, %ebx
	jl	.L2644
.L2643:
	movq	-272(%rbp), %rbx
	leal	16(,%r12,8), %eax
	movq	%r14, %rdi
	addl	$1, %r12d
	cltq
	movq	(%rbx), %rcx
	movq	-1(%rax,%rcx), %rsi
	call	_ZN2v88internal15JsonStringifier12SerializeSmiENS0_3SmiE
	cmpl	-264(%rbp), %r12d
	jne	.L2639
	movq	%r14, %r15
	movl	-296(%rbp), %r12d
	movq	-288(%rbp), %r14
	jmp	.L2994
	.p2align 4,,10
	.p2align 3
.L3074:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%r15), %edx
	testw	%dx, %dx
	jne	.L3016
	jmp	.L2653
	.p2align 4,,10
	.p2align 3
.L2646:
	testw	%dx, %dx
	je	.L2647
.L3015:
	movl	28(%r14), %eax
.L2649:
	movq	40(%r14), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$2, %r15
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r14)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r14), %eax
	cmpl	24(%r14), %eax
	je	.L3075
	movzwl	(%r15), %edx
	testw	%dx, %dx
	jne	.L2649
	jmp	.L2653
	.p2align 4,,10
	.p2align 3
.L3075:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%r15), %edx
	testw	%dx, %dx
	jne	.L3015
	jmp	.L2653
.L2640:
	leal	16(%rax,%rax), %eax
	movl	$10, %r10d
	cltq
	movw	%r10w, -1(%rcx,%rax)
	movl	24(%r14), %eax
	cmpl	%eax, 28(%r14)
	jne	.L2642
.L3014:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L2642
.L3073:
	movq	(%r14), %rax
	leaq	37512(%rax), %rdi
	call	_ZN2v88internal10StackGuard16HandleInterruptsEv@PLT
	movq	(%r14), %rcx
	cmpq	%rax, 312(%rcx)
	jne	.L2632
.L3036:
	movq	-288(%rbp), %r14
	movl	$2, %r12d
	jmp	.L2611
	.p2align 4,,10
	.p2align 3
.L2731:
	leal	16(%rax,%rax), %eax
	movl	$10, %ecx
	cltq
	movw	%cx, -1(%rsi,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	jne	.L2732
.L3026:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	28(%r15), %eax
	jmp	.L2732
.L2635:
	leal	16(%rax,%rax), %eax
	movl	$44, %ebx
	cltq
	movw	%bx, -1(%rcx,%rax)
	movl	24(%r14), %eax
	cmpl	%eax, 28(%r14)
	jne	.L2634
.L3013:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L2634
.L3065:
	movsd	7(%rax), %xmm1
	movsd	.LC1(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rdx
	movq	%xmm2, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L2614
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm2, -264(%rbp)
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L2614
	je	.L2615
	jmp	.L2614
	.p2align 4,,10
	.p2align 3
.L3072:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L3020
	jmp	.L2682
	.p2align 4,,10
	.p2align 3
.L2675:
	testw	%dx, %dx
	je	.L2676
.L3019:
	movl	28(%r15), %eax
.L2678:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$2, %r14
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L3076
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L2678
	jmp	.L2682
	.p2align 4,,10
	.p2align 3
.L3076:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L3019
	jmp	.L2682
.L2669:
	leal	16(%rax,%rax), %eax
	movl	$10, %esi
	cltq
	movw	%si, -1(%rcx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L2671
.L3018:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L2671
.L2659:
	movq	(%r15), %rax
	leaq	37512(%rax), %rdi
	call	_ZN2v88internal10StackGuard16HandleInterruptsEv@PLT
	movq	(%r15), %rcx
	cmpq	%rax, 312(%rcx)
	jne	.L2663
	jmp	.L3036
.L2626:
	movq	(%r15), %rcx
	movq	23(%rdx), %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L2685
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -280(%rbp)
.L2686:
	cmpl	$0, -264(%rbp)
	je	.L2728
	movq	%r14, -288(%rbp)
	xorl	%eax, %eax
	movl	%r12d, -296(%rbp)
	movq	%rbx, -272(%rbp)
	movl	%eax, %ebx
.L2723:
	movq	-272(%rbp), %rax
	movq	-280(%rbp), %rdi
	movq	(%rax), %rax
	movq	23(%rax), %rdx
	cmpq	%rdx, (%rdi)
	jne	.L2688
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$2, %eax
	jne	.L2688
	testl	%ebx, %ebx
	je	.L2690
	movl	28(%r15), %eax
	movl	16(%r15), %r12d
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%r12d, %r12d
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L2693
	addl	$16, %eax
	cltq
	movb	$44, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L3021
.L2690:
	cmpq	$0, 72(%r15)
	je	.L2701
	movl	28(%r15), %eax
	movl	16(%r15), %r10d
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%r10d, %r10d
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L2698
	addl	$16, %eax
	cltq
	movb	$10, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L3022
.L2700:
	movl	80(%r15), %eax
	testl	%eax, %eax
	jle	.L2701
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L2702:
	movq	72(%r15), %r12
	movl	16(%r15), %r8d
	movzwl	(%r12), %edx
	testl	%r8d, %r8d
	je	.L2704
	testw	%dx, %dx
	je	.L2705
.L3024:
	movl	28(%r15), %eax
.L2706:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$2, %r12
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L3077
	movzwl	(%r12), %edx
	testw	%dx, %dx
	jne	.L2706
.L2711:
	movl	80(%r15), %eax
.L2705:
	addl	$1, %r14d
	cmpl	%eax, %r14d
	jl	.L2702
.L2701:
	movq	-272(%rbp), %rax
	movq	(%r15), %rdx
	movq	(%rax), %rcx
	leal	16(,%rbx,8), %eax
	cltq
	movq	15(%rcx), %rcx
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L3078
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L2714:
	movq	(%r15), %rdx
	movq	%rbx, %rsi
	salq	$32, %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2716
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L2717:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier10Serialize_ILb0EEENS1_6ResultENS0_6HandleINS0_6ObjectEEEbS6_
	testl	%eax, %eax
	jne	.L2719
	movl	16(%r15), %edi
	testl	%edi, %edi
	je	.L2820
	movl	28(%r15), %eax
	movl	$110, %edx
	leaq	.LC19(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L2721:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$1, %r14
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L3079
	movzbl	(%r14), %edx
	testb	%dl, %dl
	jne	.L2721
.L2722:
	addl	$1, %ebx
	cmpl	-264(%rbp), %ebx
	jne	.L2723
	jmp	.L3045
	.p2align 4,,10
	.p2align 3
.L3077:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%r12), %edx
	testw	%dx, %dx
	jne	.L3024
	jmp	.L2711
	.p2align 4,,10
	.p2align 3
.L2704:
	testw	%dx, %dx
	je	.L2705
.L3023:
	movl	28(%r15), %eax
.L2707:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$2, %r12
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L3080
	movzwl	(%r12), %edx
	testw	%dx, %dx
	jne	.L2707
	jmp	.L2711
	.p2align 4,,10
	.p2align 3
.L3080:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%r12), %edx
	testw	%dx, %dx
	jne	.L3023
	jmp	.L2711
.L2719:
	cmpl	$1, %eax
	je	.L2722
	movq	-288(%rbp), %r14
	movl	%eax, %r12d
	jmp	.L2611
	.p2align 4,,10
	.p2align 3
.L2716:
	movq	41088(%rdx), %rcx
	cmpq	41096(%rdx), %rcx
	je	.L3081
.L2718:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rcx)
	jmp	.L2717
.L3078:
	movq	41088(%rdx), %r14
	cmpq	41096(%rdx), %r14
	je	.L3082
.L2715:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r14)
	jmp	.L2714
.L3079:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r14), %edx
	testb	%dl, %dl
	je	.L2722
	movl	28(%r15), %eax
	jmp	.L2721
.L2698:
	leal	16(%rax,%rax), %eax
	movl	$10, %r9d
	cltq
	movw	%r9w, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L2700
.L3022:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L2700
.L2820:
	movl	$110, %edx
	leaq	.LC19(%rip), %r14
.L3025:
	movl	28(%r15), %eax
.L2720:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$1, %r14
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L3083
	movzbl	(%r14), %edx
	testb	%dl, %dl
	jne	.L2720
	jmp	.L2722
.L3083:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r14), %edx
	testb	%dl, %dl
	jne	.L3025
	jmp	.L2722
.L2693:
	leal	16(%rax,%rax), %eax
	movl	$44, %r11d
	cltq
	movw	%r11w, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L2690
.L3021:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L2690
.L2665:
	leal	16(%rax,%rax), %eax
	cltq
	movw	$44, -1(%rcx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L2664
.L3017:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L2664
.L2828:
	movb	%dl, -288(%rbp)
	jmp	.L3032
.L3081:
	movq	%rdx, %rdi
	movq	%rsi, -312(%rbp)
	movq	%rdx, -304(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-312(%rbp), %rsi
	movq	-304(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L2718
.L3082:
	movq	%rdx, %rdi
	movq	%rsi, -312(%rbp)
	movq	%rdx, -304(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-312(%rbp), %rsi
	movq	-304(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L2715
.L2688:
	movl	%ebx, %eax
	movl	%ebx, %edx
	movq	-288(%rbp), %r14
	movq	-272(%rbp), %rbx
	movl	-296(%rbp), %r12d
	cmpl	%eax, -264(%rbp)
	jbe	.L2994
	jmp	.L2691
.L2685:
	movq	41088(%rcx), %rax
	movq	%rax, -280(%rbp)
	cmpq	41096(%rcx), %rax
	je	.L3084
.L2687:
	movq	-280(%rbp), %rdi
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%rdi)
	jmp	.L2686
.L3068:
	movq	%rax, -144(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L2781
	testb	$2, %al
	jne	.L2780
.L2781:
	leaq	-144(%rbp), %rax
	leaq	-228(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -296(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	testb	%al, %al
	je	.L2780
	movq	(%rbx), %rax
	movl	-228(%rbp), %edx
	testb	$1, %al
	jne	.L2783
.L2785:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movl	%edx, -320(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movl	-320(%rbp), %edx
.L2784:
	movl	$3, %edi
	movq	%r14, -120(%rbp)
	leaq	-224(%rbp), %r14
	salq	$38, %rdi
	movq	$0, -112(%rbp)
	movq	%rdi, -132(%rbp)
	movq	-296(%rbp), %rdi
	movl	$3, -144(%rbp)
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	%edx, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r13, -112(%rbp)
	movl	$20, %ecx
	movq	%r14, %rdi
	movq	-296(%rbp), %rsi
	rep movsl
	jmp	.L2786
.L3071:
	movq	%rdx, %rdi
	movq	%rsi, -320(%rbp)
	movq	%rdx, -296(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-320(%rbp), %rsi
	movq	-296(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L2773
.L3051:
	call	__stack_chk_fail@PLT
.L2628:
	movq	41088(%rbx), %rax
	movq	%rax, -272(%rbp)
	cmpq	41096(%rbx), %rax
	je	.L3085
.L2630:
	movq	-272(%rbp), %rdi
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdi)
	jmp	.L2629
.L3067:
	movl	%r14d, %esi
	movq	%rdx, -296(%rbp)
	call	_ZN2v88internal10FieldIndex13ForDescriptorEPNS0_7IsolateENS0_3MapEi.isra.0
	movq	-296(%rbp), %rdx
	movq	%rbx, %rdi
	shrl	$6, %edx
	andl	$7, %edx
	movl	%edx, %esi
	movq	%rax, %rdx
	call	_ZN2v88internal8JSObject14FastPropertyAtENS0_6HandleIS1_EENS0_14RepresentationENS0_10FieldIndexE@PLT
	movq	%rax, %rsi
	jmp	.L2792
.L2793:
	movq	-304(%rbp), %r9
	movl	$2, %r12d
	jmp	.L2756
.L2783:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L2785
	movq	%rbx, %rax
	jmp	.L2784
.L2787:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L2789
	movq	%rbx, %rdx
	jmp	.L2788
.L2765:
	leal	16(%rax,%rax), %eax
	cltq
	movw	$123, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L2767
.L3031:
	leaq	8(%r15), %rdi
	movq	%r9, -272(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-272(%rbp), %r9
	jmp	.L2767
.L3066:
	movq	41088(%rcx), %rax
	movq	%rax, -264(%rbp)
	cmpq	41096(%rcx), %rax
	je	.L3086
.L2764:
	movq	-264(%rbp), %rdi
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rcx)
	movq	%r13, (%rdi)
	jmp	.L2763
.L3084:
	movq	%rcx, %rdi
	movq	%rsi, -288(%rbp)
	movq	%rcx, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-288(%rbp), %rsi
	movq	-272(%rbp), %rcx
	movq	%rax, -280(%rbp)
	jmp	.L2687
.L3085:
	movq	%rbx, %rdi
	movq	%rsi, -280(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-280(%rbp), %rsi
	movq	%rax, -272(%rbp)
	jmp	.L2630
.L2656:
	movq	41088(%rbx), %rax
	movq	%rax, -272(%rbp)
	cmpq	41096(%rbx), %rax
	je	.L3087
.L2658:
	movq	-272(%rbp), %rdi
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdi)
	jmp	.L2657
.L3069:
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rdx, -296(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-296(%rbp), %rdx
	jmp	.L2791
.L2808:
	leal	16(%rax,%rax), %eax
	cltq
	movw	$125, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L2762
	jmp	.L3035
.L3001:
	cmpq	$0, 72(%r15)
	movq	(%rdx), %rcx
	movl	28(%r15), %eax
	je	.L2794
	leal	1(%rax), %edx
	cmpl	$0, 16(%r15)
	movl	%edx, 28(%r15)
	jne	.L2795
	addl	$16, %eax
	cltq
	movb	$10, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L3033
.L2797:
	xorl	%ebx, %ebx
	movl	16(%r15), %ecx
	movq	%r9, %r14
	movl	%ebx, %r13d
.L2798:
	cmpl	%r13d, 80(%r15)
	jle	.L3088
	movq	72(%r15), %rbx
	movzwl	(%rbx), %edx
	testl	%ecx, %ecx
	je	.L2800
	testw	%dx, %dx
	jne	.L2802
	jmp	.L2801
	.p2align 4,,10
	.p2align 3
.L3042:
	movzwl	(%rbx), %edx
	testw	%dx, %dx
	je	.L3034
.L2802:
	movq	40(%r15), %rcx
	leal	1(%rax), %edi
	leal	16(%rax,%rax), %eax
	addq	$2, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%edi, 28(%r15)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	jne	.L3042
	movq	-280(%rbp), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	28(%r15), %eax
	jmp	.L3042
.L3034:
	movl	16(%r15), %ecx
.L2801:
	addl	$1, %r13d
	jmp	.L2798
.L2800:
	testw	%dx, %dx
	jne	.L2803
	jmp	.L2801
	.p2align 4,,10
	.p2align 3
.L3044:
	movzwl	(%rbx), %edx
	testw	%dx, %dx
	je	.L3034
.L2803:
	movq	40(%r15), %rcx
	leal	1(%rax), %edi
	addl	$16, %eax
	addq	$2, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%edi, 28(%r15)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	jne	.L3044
	movq	-280(%rbp), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	28(%r15), %eax
	jmp	.L3044
.L3088:
	movq	40(%r15), %rdx
	movq	%r14, %r9
	jmp	.L2794
.L2795:
	leal	16(%rax,%rax), %eax
	cltq
	movw	$10, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	jne	.L2797
.L3033:
	movq	-280(%rbp), %rdi
	movq	%r9, -264(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	28(%r15), %eax
	movq	-264(%rbp), %r9
	jmp	.L2797
.L3086:
	movq	%rcx, %rdi
	movq	%r9, -280(%rbp)
	movq	%rcx, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-280(%rbp), %r9
	movq	-272(%rbp), %rcx
	movq	%rax, -264(%rbp)
	jmp	.L2764
.L3087:
	movq	%rbx, %rdi
	movq	%rsi, -280(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-280(%rbp), %rsi
	movq	%rax, -272(%rbp)
	jmp	.L2658
	.cfi_endproc
.LFE19872:
	.size	_ZN2v88internal15JsonStringifier10Serialize_ILb0EEENS1_6ResultENS0_6HandleINS0_6ObjectEEEbS6_, .-_ZN2v88internal15JsonStringifier10Serialize_ILb0EEENS1_6ResultENS0_6HandleINS0_6ObjectEEEbS6_
	.section	.text._ZN2v88internal15JsonStringifier22SerializeArrayLikeSlowENS0_6HandleINS0_10JSReceiverEEEjj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JsonStringifier22SerializeArrayLikeSlowENS0_6HandleINS0_10JSReceiverEEEjj
	.type	_ZN2v88internal15JsonStringifier22SerializeArrayLikeSlowENS0_6HandleINS0_10JSReceiverEEEjj, @function
_ZN2v88internal15JsonStringifier22SerializeArrayLikeSlowENS0_6HandleINS0_10JSReceiverEEEjj:
.LFB17996:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -168(%rbp)
	movl	%ecx, -172(%rbp)
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	cmpl	$536870899, %ecx
	ja	.L3176
	movl	%edx, %r13d
	leaq	8(%r14), %r12
	movq	%r14, %r15
	movq	%r13, %rax
	salq	$32, %rax
	movq	%rax, -152(%rbp)
	leaq	-144(%rbp), %rax
	movq	%rax, -160(%rbp)
	cmpl	%edx, -172(%rbp)
	jbe	.L3128
.L3129:
	testl	%r13d, %r13d
	je	.L3094
	movl	28(%r15), %eax
	movl	16(%r15), %r9d
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%r9d, %r9d
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L3095
	addl	$16, %eax
	cltq
	movb	$44, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L3171
	.p2align 4,,10
	.p2align 3
.L3094:
	cmpq	$0, 72(%r15)
	je	.L3103
	movl	28(%r15), %eax
	movl	16(%r15), %edi
	movq	40(%r15), %rdx
	leal	1(%rax), %ecx
	testl	%edi, %edi
	movq	(%rdx), %rdx
	movl	%ecx, 28(%r15)
	jne	.L3100
	addl	$16, %eax
	cltq
	movb	$10, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	je	.L3172
.L3102:
	movl	80(%r15), %eax
	testl	%eax, %eax
	jle	.L3103
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L3104:
	movq	72(%r15), %r14
	movl	16(%r15), %ecx
	movzwl	(%r14), %edx
	testl	%ecx, %ecx
	je	.L3106
	testw	%dx, %dx
	je	.L3107
.L3174:
	movl	28(%r15), %eax
.L3108:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$2, %r14
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L3177
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L3108
.L3113:
	movl	80(%r15), %eax
.L3107:
	addl	$1, %ebx
	cmpl	%eax, %ebx
	jl	.L3104
.L3103:
	movq	(%r15), %rax
	movq	$0, -112(%rbp)
	movabsq	$824633720832, %rdi
	movq	%rdi, -132(%rbp)
	movq	-160(%rbp), %rdi
	movq	%rax, -120(%rbp)
	movq	-168(%rbp), %rax
	movl	$3, -144(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	%r13d, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L3178
	movq	-120(%rbp), %rax
	leaq	88(%rax), %rbx
.L3116:
	movq	(%r15), %rdx
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L3117
	movq	-152(%rbp), %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L3118:
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15JsonStringifier10Serialize_ILb0EEENS1_6ResultENS0_6HandleINS0_6ObjectEEEbS6_
	cmpl	$1, %eax
	je	.L3120
	testl	%eax, %eax
	jne	.L3089
	cmpb	$0, 20(%r15)
	jne	.L3121
	movl	16(%r15), %eax
	testl	%eax, %eax
	je	.L3131
	movl	28(%r15), %eax
	movl	$110, %edx
	leaq	.LC19(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L3123:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movw	%dx, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L3179
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L3123
.L3120:
	movabsq	$4294967296, %rdi
	addl	$1, %r13d
	addq	%rdi, -152(%rbp)
	cmpl	%r13d, -172(%rbp)
	jne	.L3129
.L3128:
	movl	$1, %eax
	jmp	.L3089
	.p2align 4,,10
	.p2align 3
.L3177:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L3174
	jmp	.L3113
	.p2align 4,,10
	.p2align 3
.L3106:
	testw	%dx, %dx
	je	.L3107
.L3173:
	movl	28(%r15), %eax
.L3109:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$2, %r14
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L3180
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L3109
	jmp	.L3113
	.p2align 4,,10
	.p2align 3
.L3180:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzwl	(%r14), %edx
	testw	%dx, %dx
	jne	.L3173
	jmp	.L3113
	.p2align 4,,10
	.p2align 3
.L3179:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	je	.L3120
	movl	28(%r15), %eax
	jmp	.L3123
	.p2align 4,,10
	.p2align 3
.L3178:
	movq	-160(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L3116
.L3121:
	movl	$2, %eax
.L3089:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L3181
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3100:
	.cfi_restore_state
	leal	16(%rax,%rax), %eax
	movl	$10, %esi
	cltq
	movw	%si, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L3102
.L3172:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L3102
	.p2align 4,,10
	.p2align 3
.L3117:
	movq	41088(%rdx), %rcx
	cmpq	41096(%rdx), %rcx
	je	.L3182
.L3119:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	-152(%rbp), %rax
	movq	%rax, (%rcx)
	jmp	.L3118
	.p2align 4,,10
	.p2align 3
.L3131:
	movl	$110, %edx
	leaq	.LC19(%rip), %rbx
.L3175:
	movl	28(%r15), %eax
.L3122:
	movq	40(%r15), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 28(%r15)
	movb	%dl, -1(%rcx,%rax)
	movl	28(%r15), %eax
	cmpl	24(%r15), %eax
	je	.L3183
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L3122
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L3183:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L3175
	jmp	.L3120
.L3095:
	leal	16(%rax,%rax), %eax
	movl	$44, %r8d
	cltq
	movw	%r8w, -1(%rdx,%rax)
	movl	24(%r15), %eax
	cmpl	%eax, 28(%r15)
	jne	.L3094
.L3171:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L3094
.L3182:
	movq	%rdx, %rdi
	movq	%rdx, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L3119
.L3176:
	movq	(%r14), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory27NewInvalidStringLengthErrorEv@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movl	$2, %eax
	jmp	.L3089
.L3181:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17996:
	.size	_ZN2v88internal15JsonStringifier22SerializeArrayLikeSlowENS0_6HandleINS0_10JSReceiverEEEjj, .-_ZN2v88internal15JsonStringifier22SerializeArrayLikeSlowENS0_6HandleINS0_10JSReceiverEEEjj
	.section	.text._ZN2v88internal15JsonStringifier16SerializeJSProxyENS0_6HandleINS0_7JSProxyEEENS2_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JsonStringifier16SerializeJSProxyENS0_6HandleINS0_7JSProxyEEENS2_INS0_6ObjectEEE
	.type	_ZN2v88internal15JsonStringifier16SerializeJSProxyENS0_6HandleINS0_7JSProxyEEENS2_INS0_6ObjectEEE, @function
_ZN2v88internal15JsonStringifier16SerializeJSProxyENS0_6HandleINS0_7JSProxyEEENS2_INS0_6ObjectEEE:
.LFB17999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rdi), %r13
	addl	$1, 41104(%r13)
	movq	41088(%r13), %rax
	movq	41096(%r13), %r12
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal15JsonStringifier9StackPushENS0_6HandleINS0_6ObjectEEES4_
	movl	%eax, %r15d
	cmpl	$1, %eax
	jne	.L3185
	movq	(%r14), %rax
	testb	$1, %al
	jne	.L3257
.L3191:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal15JsonStringifier23SerializeJSReceiverSlowENS0_6HandleINS0_10JSReceiverEEE
	cmpl	$1, %eax
	je	.L3222
.L3228:
	movl	%eax, %r15d
	.p2align 4,,10
	.p2align 3
.L3185:
	movq	-56(%rbp), %rax
	subl	$1, 41104(%r13)
	movq	%rax, 41088(%r13)
	cmpq	41096(%r13), %r12
	je	.L3184
	movq	%r12, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3184:
	addq	$40, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3257:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1061, 11(%rdx)
	je	.L3190
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jne	.L3191
	movq	%r14, %rdi
	call	_ZN2v88internal7JSProxy7IsArrayENS0_6HandleIS1_EE@PLT
	movzbl	%ah, %edx
	testb	%al, %al
	jne	.L3258
.L3256:
	movl	$2, %r15d
	jmp	.L3185
.L3258:
	testb	%dl, %dl
	je	.L3191
.L3190:
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal6Object22GetLengthFromArrayLikeEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE@PLT
	testq	%rax, %rax
	je	.L3256
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L3192
	sarq	$32, %rax
	movl	%eax, %ecx
	js	.L3194
.L3195:
	leaq	8(%rbx), %rax
	movl	16(%rbx), %r8d
	movq	40(%rbx), %rdx
	movq	%rax, -72(%rbp)
	movl	28(%rbx), %eax
	testl	%r8d, %r8d
	movq	(%rdx), %rdx
	leal	1(%rax), %esi
	movl	%esi, 28(%rbx)
	jne	.L3201
	addl	$16, %eax
	cltq
	movb	$91, -1(%rdx,%rax)
	movl	24(%rbx), %eax
	cmpl	%eax, 28(%rbx)
	je	.L3251
.L3203:
	addl	$1, 80(%rbx)
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal15JsonStringifier22SerializeArrayLikeSlowENS0_6HandleINS0_10JSReceiverEEEjj
	cmpl	$1, %eax
	jne	.L3228
	movl	28(%rbx), %eax
	movl	-60(%rbp), %ecx
	subl	$1, 80(%rbx)
	movl	16(%rbx), %edi
	movq	40(%rbx), %rsi
	leal	1(%rax), %edx
	testl	%ecx, %ecx
	je	.L3205
	movq	(%rsi), %rcx
	cmpq	$0, 72(%rbx)
	leaq	-1(%rcx), %rsi
	je	.L3206
	movl	%edx, 28(%rbx)
	testl	%edi, %edi
	jne	.L3207
	addl	$16, %eax
	cltq
	movb	$10, -1(%rcx,%rax)
	movl	28(%rbx), %eax
	cmpl	24(%rbx), %eax
	je	.L3252
.L3208:
	movq	40(%rbx), %rdx
	movl	80(%rbx), %r10d
	movl	$0, -60(%rbp)
	movl	16(%rbx), %edi
	movq	(%rdx), %rsi
	testl	%r10d, %r10d
	jle	.L3259
	.p2align 4,,10
	.p2align 3
.L3209:
	movq	72(%rbx), %r14
	movzwl	(%r14), %ecx
	testl	%edi, %edi
	je	.L3210
	testw	%cx, %cx
	jne	.L3212
	jmp	.L3211
	.p2align 4,,10
	.p2align 3
.L3218:
	movq	40(%rbx), %rsi
	movzwl	(%r14), %ecx
	movq	(%rsi), %rsi
	testw	%cx, %cx
	je	.L3217
.L3212:
	leal	1(%rax), %edi
	leal	16(%rax,%rax), %eax
	addq	$2, %r14
	cltq
	movl	%edi, 28(%rbx)
	movw	%cx, -1(%rsi,%rax)
	movl	28(%rbx), %eax
	cmpl	24(%rbx), %eax
	jne	.L3218
	movq	-72(%rbp), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	28(%rbx), %eax
	jmp	.L3218
	.p2align 4,,10
	.p2align 3
.L3205:
	movq	(%rsi), %rsi
	subq	$1, %rsi
.L3206:
	movl	%edx, 28(%rbx)
	testl	%edi, %edi
	jne	.L3220
	addl	$16, %eax
	cltq
	movb	$93, (%rax,%rsi)
	movl	24(%rbx), %eax
	cmpl	%eax, 28(%rbx)
	jne	.L3222
.L3255:
	movq	-72(%rbp), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	.p2align 4,,10
	.p2align 3
.L3222:
	subq	$16, 96(%rbx)
	jmp	.L3185
	.p2align 4,,10
	.p2align 3
.L3201:
	leal	16(%rax,%rax), %eax
	movl	$91, %edi
	cltq
	movw	%di, -1(%rdx,%rax)
	movl	24(%rbx), %eax
	cmpl	%eax, 28(%rbx)
	jne	.L3203
.L3251:
	leaq	8(%rbx), %rdi
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	-60(%rbp), %ecx
	jmp	.L3203
	.p2align 4,,10
	.p2align 3
.L3192:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L3260
.L3194:
	movq	(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory27NewInvalidStringLengthErrorEv@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L3256
	.p2align 4,,10
	.p2align 3
.L3260:
	movsd	7(%rax), %xmm1
	movsd	.LC1(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rdx
	movq	%xmm2, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L3194
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm2, %ecx
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L3194
	je	.L3195
	jmp	.L3194
	.p2align 4,,10
	.p2align 3
.L3217:
	movl	16(%rbx), %edi
	movl	80(%rbx), %r10d
.L3211:
	addl	$1, -60(%rbp)
	movl	-60(%rbp), %ecx
	cmpl	%r10d, %ecx
	jl	.L3209
	leal	1(%rax), %edx
	subq	$1, %rsi
	jmp	.L3206
	.p2align 4,,10
	.p2align 3
.L3210:
	testw	%cx, %cx
	jne	.L3213
	jmp	.L3211
	.p2align 4,,10
	.p2align 3
.L3215:
	movq	40(%rbx), %rsi
	movzwl	(%r14), %ecx
	movq	(%rsi), %rsi
	testw	%cx, %cx
	je	.L3217
.L3213:
	leal	1(%rax), %edi
	addl	$16, %eax
	addq	$2, %r14
	cltq
	movl	%edi, 28(%rbx)
	movb	%cl, -1(%rsi,%rax)
	movl	28(%rbx), %eax
	cmpl	24(%rbx), %eax
	jne	.L3215
	movq	-72(%rbp), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	28(%rbx), %eax
	jmp	.L3215
	.p2align 4,,10
	.p2align 3
.L3220:
	leal	16(%rax,%rax), %eax
	movl	$93, %edx
	cltq
	movw	%dx, (%rax,%rsi)
	movl	24(%rbx), %eax
	cmpl	%eax, 28(%rbx)
	jne	.L3222
	jmp	.L3255
	.p2align 4,,10
	.p2align 3
.L3207:
	leal	16(%rax,%rax), %eax
	movl	$10, %esi
	cltq
	movw	%si, -1(%rcx,%rax)
	movl	28(%rbx), %eax
	cmpl	24(%rbx), %eax
	jne	.L3208
.L3252:
	leaq	8(%rbx), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	28(%rbx), %eax
	jmp	.L3208
.L3259:
	subq	$1, %rsi
	leal	1(%rax), %edx
	jmp	.L3206
	.cfi_endproc
.LFE17999:
	.size	_ZN2v88internal15JsonStringifier16SerializeJSProxyENS0_6HandleINS0_7JSProxyEEENS2_INS0_6ObjectEEE, .-_ZN2v88internal15JsonStringifier16SerializeJSProxyENS0_6HandleINS0_7JSProxyEEENS2_INS0_6ObjectEEE
	.section	.text._ZN2v88internal15JsonStringifier9StringifyENS0_6HandleINS0_6ObjectEEES4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JsonStringifier9StringifyENS0_6HandleINS0_6ObjectEEES4_S4_
	.type	_ZN2v88internal15JsonStringifier9StringifyENS0_6HandleINS0_6ObjectEEES4_S4_, @function
_ZN2v88internal15JsonStringifier9StringifyENS0_6HandleINS0_6ObjectEEES4_S4_:
.LFB17966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal15JsonStringifier18InitializeReplacerENS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	je	.L3265
	movq	(%rbx), %rcx
	movq	0(%r13), %rax
	cmpq	%rax, 88(%rcx)
	je	.L3264
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal15JsonStringifier13InitializeGapENS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	jne	.L3271
.L3265:
	xorl	%eax, %eax
.L3263:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3271:
	.cfi_restore_state
	movq	(%rbx), %rcx
.L3264:
	subq	$-128, %rcx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal15JsonStringifier10Serialize_ILb0EEENS1_6ResultENS0_6HandleINS0_6ObjectEEEbS6_
	testl	%eax, %eax
	jne	.L3266
	movq	(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	addq	$88, %rax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3266:
	.cfi_restore_state
	cmpl	$1, %eax
	jne	.L3265
	leaq	8(%rbx), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	jmp	.L3263
	.cfi_endproc
.LFE17966:
	.size	_ZN2v88internal15JsonStringifier9StringifyENS0_6HandleINS0_6ObjectEEES4_S4_, .-_ZN2v88internal15JsonStringifier9StringifyENS0_6HandleINS0_6ObjectEEES4_S4_
	.section	.text._ZN2v88internal13JsonStringifyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES5_S5_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal13JsonStringifyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES5_S5_
	.type	_ZN2v88internal13JsonStringifyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES5_S5_, @function
_ZN2v88internal13JsonStringifyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES5_S5_:
.LFB17953:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-168(%rbp), %r15
	leaq	-176(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -176(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilderC1EPNS0_7IsolateE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-176(%rbp), %rax
	movq	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	addq	$3448, %rax
	movq	$0, -104(%rbp)
	movl	$0, -96(%rbp)
	movq	$0, -72(%rbp)
	movq	%rax, -128(%rbp)
	movups	%xmm0, -88(%rbp)
	call	_ZN2v88internal15JsonStringifier18InitializeReplacerENS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	je	.L3276
	movq	-176(%rbp), %rcx
	movq	88(%rcx), %rax
	cmpq	%rax, (%rbx)
	je	.L3275
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal15JsonStringifier13InitializeGapENS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	jne	.L3291
.L3276:
	xorl	%r12d, %r12d
.L3274:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3278
	call	_ZdaPv@PLT
.L3278:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3279
	call	_ZdlPv@PLT
.L3279:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3292
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3291:
	.cfi_restore_state
	movq	-176(%rbp), %rcx
.L3275:
	subq	$-128, %rcx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal15JsonStringifier10Serialize_ILb0EEENS1_6ResultENS0_6HandleINS0_6ObjectEEEbS6_
	testl	%eax, %eax
	jne	.L3277
	movq	-176(%rbp), %rax
	leaq	88(%rax), %r12
	jmp	.L3274
	.p2align 4,,10
	.p2align 3
.L3277:
	cmpl	$1, %eax
	jne	.L3276
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	movq	%rax, %r12
	jmp	.L3274
.L3292:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17953:
	.size	_ZN2v88internal13JsonStringifyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES5_S5_, .-_ZN2v88internal13JsonStringifyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES5_S5_
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal13JsonStringifyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES5_S5_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal13JsonStringifyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES5_S5_, @function
_GLOBAL__sub_I__ZN2v88internal13JsonStringifyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES5_S5_:
.LFB21954:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21954:
	.size	_GLOBAL__sub_I__ZN2v88internal13JsonStringifyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES5_S5_, .-_GLOBAL__sub_I__ZN2v88internal13JsonStringifyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES5_S5_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal13JsonStringifyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES5_S5_
	.globl	_ZN2v88internal15JsonStringifier15JsonEscapeTableE
	.section	.data.rel.ro.local._ZN2v88internal15JsonStringifier15JsonEscapeTableE,"aw"
	.align 8
	.type	_ZN2v88internal15JsonStringifier15JsonEscapeTableE, @object
	.size	_ZN2v88internal15JsonStringifier15JsonEscapeTableE, 8
_ZN2v88internal15JsonStringifier15JsonEscapeTableE:
	.quad	.LC21
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1127219200
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC4:
	.long	4294967295
	.long	2146435071
	.align 8
.LC5:
	.long	4290772992
	.long	1105199103
	.align 8
.LC6:
	.long	0
	.long	-1042284544
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
