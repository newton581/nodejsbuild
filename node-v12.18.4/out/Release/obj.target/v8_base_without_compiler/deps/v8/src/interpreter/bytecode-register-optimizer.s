	.file	"bytecode-register-optimizer.cc"
	.text
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer21RegisterListFreeEventENS1_12RegisterListE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21RegisterListFreeEventENS1_12RegisterListE
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21RegisterListFreeEventENS1_12RegisterListE, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21RegisterListFreeEventENS1_12RegisterListE:
.LFB6894:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	sarq	$32, %rdx
	testl	%edx, %edx
	je	.L1
	jle	.L1
	movl	%esi, %eax
	addl	%edx, %esi
	.p2align 4,,10
	.p2align 3
.L3:
	movl	64(%rdi), %edx
	movq	40(%rdi), %rcx
	addl	%eax, %edx
	addl	$1, %eax
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rdx
	movb	$0, 9(%rdx)
	cmpl	%eax, %esi
	jne	.L3
.L1:
	ret
	.cfi_endproc
.LFE6894:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21RegisterListFreeEventENS1_12RegisterListE, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21RegisterListFreeEventENS1_12RegisterListE
	.section	.rodata._ZN2v88internal11interpreter25BytecodeRegisterOptimizerD0Ev.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizerD0Ev,"axG",@progbits,_ZN2v88internal11interpreter25BytecodeRegisterOptimizerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter25BytecodeRegisterOptimizerD0Ev
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizerD0Ev, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizerD0Ev:
.LFB7861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal11interpreter25BytecodeRegisterOptimizerE(%rip), %rax
	cmpq	$0, 88(%rdi)
	movq	%rax, (%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	je	.L7
	movq	160(%rdi), %rax
	movq	128(%rdi), %rdx
	leaq	8(%rax), %rcx
	cmpq	%rdx, %rcx
	jbe	.L13
	movq	80(%rdi), %rax
	.p2align 4,,10
	.p2align 3
.L14:
	testq	%rax, %rax
	je	.L11
	cmpq	$64, 8(%rax)
	ja	.L12
.L11:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	80(%rdi), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 80(%rdi)
.L12:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L14
.L13:
	movq	96(%rdi), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L7
	movq	88(%rdi), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L7:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE7861:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizerD0Ev, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizerD0Ev
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizerD2Ev,"axG",@progbits,_ZN2v88internal11interpreter25BytecodeRegisterOptimizerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter25BytecodeRegisterOptimizerD2Ev
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizerD2Ev, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizerD2Ev:
.LFB7859:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal11interpreter25BytecodeRegisterOptimizerE(%rip), %rax
	movq	%rax, (%rdi)
	movq	88(%rdi), %rax
	testq	%rax, %rax
	je	.L20
	movq	160(%rdi), %rsi
	movq	128(%rdi), %rdx
	leaq	8(%rsi), %rcx
	cmpq	%rdx, %rcx
	jbe	.L22
	movq	80(%rdi), %rax
	.p2align 4,,10
	.p2align 3
.L25:
	testq	%rax, %rax
	je	.L23
	cmpq	$64, 8(%rax)
	ja	.L24
.L23:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	80(%rdi), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 80(%rdi)
.L24:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L25
	movq	88(%rdi), %rax
.L22:
	movq	96(%rdi), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L20
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L20:
	ret
	.cfi_endproc
.LFE7859:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizerD2Ev, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizerD2Ev
	.weak	_ZN2v88internal11interpreter25BytecodeRegisterOptimizerD1Ev
	.set	_ZN2v88internal11interpreter25BytecodeRegisterOptimizerD1Ev,_ZN2v88internal11interpreter25BytecodeRegisterOptimizerD2Ev
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo21AddToEquivalenceSetOfEPS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo21AddToEquivalenceSetOfEPS3_
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo21AddToEquivalenceSetOfEPS3_, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo21AddToEquivalenceSetOfEPS3_:
.LFB6849:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	movq	%rax, 24(%rdx)
	movq	16(%rdi), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rsi), %rax
	movq	%rsi, 24(%rdi)
	movq	%rax, 16(%rdi)
	movq	%rdi, 16(%rsi)
	movq	16(%rdi), %rax
	movq	%rdi, 24(%rax)
	movl	4(%rsi), %eax
	movb	$0, 8(%rdi)
	movl	%eax, 4(%rdi)
	ret
	.cfi_endproc
.LFE6849:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo21AddToEquivalenceSetOfEPS3_, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo21AddToEquivalenceSetOfEPS3_
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo23MoveToNewEquivalenceSetEjb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo23MoveToNewEquivalenceSetEjb
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo23MoveToNewEquivalenceSetEjb, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo23MoveToNewEquivalenceSetEjb:
.LFB6850:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	movq	%rdi, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 24(%rcx)
	movq	16(%rdi), %rcx
	movq	%rcx, 16(%rax)
	movl	%esi, 4(%rdi)
	movb	%dl, 8(%rdi)
	movups	%xmm0, 16(%rdi)
	ret
	.cfi_endproc
.LFE6850:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo23MoveToNewEquivalenceSetEjb, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo23MoveToNewEquivalenceSetEjb
	.section	.text._ZNK2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo28IsOnlyMemberOfEquivalenceSetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo28IsOnlyMemberOfEquivalenceSetEv
	.type	_ZNK2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo28IsOnlyMemberOfEquivalenceSetEv, @function
_ZNK2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo28IsOnlyMemberOfEquivalenceSetEv:
.LFB6851:
	.cfi_startproc
	endbr64
	cmpq	%rdi, 16(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE6851:
	.size	_ZNK2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo28IsOnlyMemberOfEquivalenceSetEv, .-_ZNK2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo28IsOnlyMemberOfEquivalenceSetEv
	.section	.text._ZNK2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo40IsOnlyMaterializedMemberOfEquivalenceSetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo40IsOnlyMaterializedMemberOfEquivalenceSetEv
	.type	_ZNK2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo40IsOnlyMaterializedMemberOfEquivalenceSetEv, @function
_ZNK2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo40IsOnlyMaterializedMemberOfEquivalenceSetEv:
.LFB6852:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	cmpq	%rax, %rdi
	jne	.L39
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L43:
	movq	16(%rax), %rax
	cmpq	%rax, %rdi
	je	.L40
.L39:
	cmpb	$0, 8(%rax)
	je	.L43
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE6852:
	.size	_ZNK2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo40IsOnlyMaterializedMemberOfEquivalenceSetEv, .-_ZNK2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo40IsOnlyMaterializedMemberOfEquivalenceSetEv
	.section	.text._ZNK2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo22IsInSameEquivalenceSetEPS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo22IsInSameEquivalenceSetEPS3_
	.type	_ZNK2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo22IsInSameEquivalenceSetEPS3_, @function
_ZNK2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo22IsInSameEquivalenceSetEPS3_:
.LFB6853:
	.cfi_startproc
	endbr64
	movl	4(%rsi), %eax
	cmpl	%eax, 4(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE6853:
	.size	_ZNK2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo22IsInSameEquivalenceSetEPS3_, .-_ZNK2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo22IsInSameEquivalenceSetEPS3_
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo22GetAllocatedEquivalentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo22GetAllocatedEquivalentEv
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo22GetAllocatedEquivalentEv, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo22GetAllocatedEquivalentEv:
.LFB6854:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L50:
	movq	16(%rax), %rax
	cmpq	%rax, %rdi
	je	.L49
.L47:
	cmpb	$0, 9(%rax)
	je	.L50
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6854:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo22GetAllocatedEquivalentEv, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo22GetAllocatedEquivalentEv
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo25GetMaterializedEquivalentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo25GetMaterializedEquivalentEv
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo25GetMaterializedEquivalentEv, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo25GetMaterializedEquivalentEv:
.LFB6855:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L56:
	movq	16(%rax), %rax
	cmpq	%rax, %rdi
	je	.L55
.L53:
	cmpb	$0, 8(%rax)
	je	.L56
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6855:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo25GetMaterializedEquivalentEv, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo25GetMaterializedEquivalentEv
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo34GetMaterializedEquivalentOtherThanENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo34GetMaterializedEquivalentOtherThanENS1_8RegisterE
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo34GetMaterializedEquivalentOtherThanENS1_8RegisterE, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo34GetMaterializedEquivalentOtherThanENS1_8RegisterE:
.LFB6856:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L60:
	cmpb	$0, 8(%rax)
	je	.L58
	cmpl	%esi, (%rax)
	jne	.L57
.L58:
	movq	16(%rax), %rax
	cmpq	%rax, %rdi
	jne	.L60
	xorl	%eax, %eax
.L57:
	ret
	.cfi_endproc
.LFE6856:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo34GetMaterializedEquivalentOtherThanENS1_8RegisterE, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo34GetMaterializedEquivalentOtherThanENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo26GetEquivalentToMaterializeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo26GetEquivalentToMaterializeEv
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo26GetEquivalentToMaterializeEv, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo26GetEquivalentToMaterializeEv:
.LFB6857:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	xorl	%r8d, %r8d
	cmpq	%rax, %rdi
	jne	.L65
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L70:
	movl	(%r8), %edx
	cmpl	%edx, (%rax)
	cmovl	%rax, %r8
.L64:
	movq	16(%rax), %rax
	cmpq	%rax, %rdi
	je	.L62
.L65:
	cmpb	$0, 8(%rax)
	jne	.L67
	cmpb	$0, 9(%rax)
	je	.L64
	testq	%r8, %r8
	jne	.L70
	movq	%rax, %r8
	movq	16(%rax), %rax
	cmpq	%rax, %rdi
	jne	.L65
.L62:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	xorl	%r8d, %r8d
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE6857:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo26GetEquivalentToMaterializeEv, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo26GetEquivalentToMaterializeEv
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo31MarkTemporariesAsUnmaterializedENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo31MarkTemporariesAsUnmaterializedENS1_8RegisterE
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo31MarkTemporariesAsUnmaterializedENS1_8RegisterE, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo31MarkTemporariesAsUnmaterializedENS1_8RegisterE:
.LFB6858:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	cmpq	%rax, %rdi
	je	.L71
	.p2align 4,,10
	.p2align 3
.L75:
	cmpl	(%rax), %esi
	jg	.L73
	movb	$0, 8(%rax)
	movq	16(%rax), %rax
	cmpq	%rdi, %rax
	jne	.L75
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	movq	16(%rax), %rax
	cmpq	%rax, %rdi
	jne	.L75
.L71:
	ret
	.cfi_endproc
.LFE6858:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo31MarkTemporariesAsUnmaterializedENS1_8RegisterE, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo31MarkTemporariesAsUnmaterializedENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo13GetEquivalentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo13GetEquivalentEv
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo13GetEquivalentEv, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo13GetEquivalentEv:
.LFB6859:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE6859:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo13GetEquivalentEv, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfo13GetEquivalentEv
	.section	.text._ZNK2v88internal11interpreter25BytecodeRegisterOptimizer28EnsureAllRegistersAreFlushedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter25BytecodeRegisterOptimizer28EnsureAllRegistersAreFlushedEv
	.type	_ZNK2v88internal11interpreter25BytecodeRegisterOptimizer28EnsureAllRegistersAreFlushedEv, @function
_ZNK2v88internal11interpreter25BytecodeRegisterOptimizer28EnsureAllRegistersAreFlushedEv:
.LFB6877:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdx
	movq	48(%rdi), %rcx
	cmpq	%rcx, %rdx
	jne	.L80
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L87:
	cmpq	16(%rax), %rax
	jne	.L77
	cmpb	$0, 9(%rax)
	je	.L79
	movzbl	8(%rax), %r8d
	testb	%r8b, %r8b
	je	.L77
.L79:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	je	.L81
.L80:
	movq	(%rdx), %rax
	movzbl	10(%rax), %r8d
	testb	%r8b, %r8b
	je	.L87
	xorl	%r8d, %r8d
.L77:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	movl	$1, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE6877:
	.size	_ZNK2v88internal11interpreter25BytecodeRegisterOptimizer28EnsureAllRegistersAreFlushedEv, .-_ZNK2v88internal11interpreter25BytecodeRegisterOptimizer28EnsureAllRegistersAreFlushedEv
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer22OutputRegisterTransferEPNS2_12RegisterInfoES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer22OutputRegisterTransferEPNS2_12RegisterInfoES4_
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer22OutputRegisterTransferEPNS2_12RegisterInfoES4_, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer22OutputRegisterTransferEPNS2_12RegisterInfoES4_:
.LFB6879:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	8(%rdi), %eax
	movl	(%rsi), %esi
	movq	176(%rdi), %rdi
	movl	(%rdx), %r12d
	movq	(%rdi), %rcx
	cmpl	%esi, %eax
	je	.L94
	cmpl	%eax, %r12d
	je	.L95
	movl	%r12d, %edx
	call	*32(%rcx)
.L90:
	cmpl	8(%rbx), %r12d
	je	.L92
	cmpl	%r12d, 28(%rbx)
	cmovge	28(%rbx), %r12d
	movl	%r12d, 28(%rbx)
.L92:
	movb	$1, 8(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	call	*16(%rcx)
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L94:
	movl	%r12d, %esi
	call	*24(%rcx)
	jmp	.L90
	.cfi_endproc
.LFE6879:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer22OutputRegisterTransferEPNS2_12RegisterInfoES4_, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer22OutputRegisterTransferEPNS2_12RegisterInfoES4_
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv:
.LFB6878:
	.cfi_startproc
	endbr64
	cmpb	$0, 184(%rdi)
	je	.L124
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	104(%rdi), %r14
	movq	120(%rdi), %r15
	movq	128(%rdi), %r8
	movq	136(%rdi), %rcx
	.p2align 4,,10
	.p2align 3
.L108:
	cmpq	%r14, %rcx
	je	.L98
.L130:
	movq	(%r14), %r12
	cmpb	$0, 10(%r12)
	je	.L100
	cmpb	$0, 8(%r12)
	movb	$0, 10(%r12)
	movq	%r12, %rax
	jne	.L128
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L105:
	movl	168(%r13), %eax
	movq	%rbx, %xmm0
	punpcklqdq	%xmm0, %xmm0
	addl	$1, %eax
	movl	%eax, 168(%r13)
	movq	16(%rbx), %rsi
	movq	24(%rbx), %rdx
	movq	%rdx, 24(%rsi)
	movq	16(%rbx), %rsi
	movq	%rsi, 16(%rdx)
	movl	%eax, 4(%rbx)
	movb	$1, 8(%rbx)
	movb	$0, 10(%rbx)
	movups	%xmm0, 16(%rbx)
.L128:
	movq	16(%r12), %rbx
	cmpq	%r12, %rbx
	je	.L100
.L106:
	cmpb	$0, 9(%rbx)
	je	.L105
	cmpb	$0, 8(%rbx)
	jne	.L105
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer22OutputRegisterTransferEPNS2_12RegisterInfoES4_
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L102:
	movq	16(%rax), %rax
	cmpq	%rax, %r12
	je	.L129
	cmpb	$0, 8(%rax)
	je	.L102
	movq	%rax, %r12
	movq	16(%r12), %rbx
	cmpq	%r12, %rbx
	jne	.L106
	.p2align 4,,10
	.p2align 3
.L100:
	addq	$8, %r14
	cmpq	%r14, %r15
	jne	.L108
.L131:
	movq	8(%r8), %r14
	addq	$8, %r8
	leaq	512(%r14), %r15
	cmpq	%r14, %rcx
	jne	.L130
.L98:
	movq	128(%r13), %r8
	movq	160(%r13), %rax
	movq	104(%r13), %xmm0
	movq	112(%r13), %r9
	leaq	8(%rax), %rcx
	leaq	8(%r8), %rdx
	movq	120(%r13), %rdi
	cmpq	%rdx, %rcx
	jbe	.L113
	movq	80(%r13), %rax
	.p2align 4,,10
	.p2align 3
.L114:
	testq	%rax, %rax
	je	.L111
	cmpq	$64, 8(%rax)
	ja	.L112
.L111:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	80(%r13), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 80(%r13)
.L112:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L114
.L113:
	movq	%r9, %xmm1
	movq	%r8, %xmm2
	movb	$0, 184(%r13)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 136(%r13)
	movq	%rdi, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 152(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movl	168(%r13), %eax
	movq	%r12, %xmm0
	addq	$8, %r14
	punpcklqdq	%xmm0, %xmm0
	addl	$1, %eax
	movl	%eax, 168(%r13)
	movq	16(%r12), %rsi
	movq	24(%r12), %rdx
	movq	%rdx, 24(%rsi)
	movq	16(%r12), %rsi
	movq	%rsi, 16(%rdx)
	movl	%eax, 4(%r12)
	movb	$0, 8(%r12)
	movups	%xmm0, 16(%r12)
	cmpq	%r14, %r15
	jne	.L108
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE6878:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer28CreateMaterializedEquivalentEPNS2_12RegisterInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer28CreateMaterializedEquivalentEPNS2_12RegisterInfoE
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer28CreateMaterializedEquivalentEPNS2_12RegisterInfoE, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer28CreateMaterializedEquivalentEPNS2_12RegisterInfoE:
.LFB6880:
	.cfi_startproc
	endbr64
	movq	16(%rsi), %rax
	cmpq	%rax, %rsi
	je	.L132
	xorl	%edx, %edx
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L142:
	movl	(%rax), %ecx
	cmpl	%ecx, (%rdx)
	cmovg	%rax, %rdx
.L134:
	movq	16(%rax), %rax
	cmpq	%rax, %rsi
	je	.L141
.L135:
	cmpb	$0, 8(%rax)
	jne	.L132
	cmpb	$0, 9(%rax)
	je	.L134
	testq	%rdx, %rdx
	jne	.L142
	movq	%rax, %rdx
	movq	16(%rax), %rax
	cmpq	%rax, %rsi
	jne	.L135
	.p2align 4,,10
	.p2align 3
.L141:
	testq	%rdx, %rdx
	je	.L132
	jmp	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer22OutputRegisterTransferEPNS2_12RegisterInfoES4_
	.p2align 4,,10
	.p2align 3
.L132:
	ret
	.cfi_endproc
.LFE6880:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer28CreateMaterializedEquivalentEPNS2_12RegisterInfoE, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer28CreateMaterializedEquivalentEPNS2_12RegisterInfoE
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer25GetMaterializedEquivalentEPNS2_12RegisterInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer25GetMaterializedEquivalentEPNS2_12RegisterInfoE
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer25GetMaterializedEquivalentEPNS2_12RegisterInfoE, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer25GetMaterializedEquivalentEPNS2_12RegisterInfoE:
.LFB6881:
	.cfi_startproc
	endbr64
	cmpb	$0, 8(%rsi)
	movq	%rsi, %rax
	jne	.L143
	.p2align 4,,10
	.p2align 3
.L145:
	movq	16(%rax), %rax
	cmpq	%rax, %rsi
	je	.L148
	cmpb	$0, 8(%rax)
	je	.L145
.L143:
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6881:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer25GetMaterializedEquivalentEPNS2_12RegisterInfoE, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer25GetMaterializedEquivalentEPNS2_12RegisterInfoE
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer39GetMaterializedEquivalentNotAccumulatorEPNS2_12RegisterInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer39GetMaterializedEquivalentNotAccumulatorEPNS2_12RegisterInfoE
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer39GetMaterializedEquivalentNotAccumulatorEPNS2_12RegisterInfoE, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer39GetMaterializedEquivalentNotAccumulatorEPNS2_12RegisterInfoE:
.LFB6882:
	.cfi_startproc
	endbr64
	cmpb	$0, 8(%rsi)
	movq	%rsi, %rax
	jne	.L162
	movl	8(%rdi), %ecx
	.p2align 4,,10
	.p2align 3
.L151:
	movq	16(%rax), %rax
	movzbl	8(%rax), %edx
	cmpq	%rax, %rsi
	je	.L165
	testb	%dl, %dl
	je	.L151
	cmpl	(%rax), %ecx
	je	.L151
.L162:
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rax, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L166:
	movq	16(%rsi), %rsi
	cmpq	%rsi, %rax
	je	.L157
	movzbl	8(%rsi), %edx
.L155:
	testb	%dl, %dl
	je	.L166
.L154:
	movq	%rax, %rdx
	movq	%rax, -8(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer22OutputRegisterTransferEPNS2_12RegisterInfoES4_
	movq	-8(%rbp), %rax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L157:
	.cfi_restore_state
	xorl	%esi, %esi
	jmp	.L154
	.cfi_endproc
.LFE6882:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer39GetMaterializedEquivalentNotAccumulatorEPNS2_12RegisterInfoE, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer39GetMaterializedEquivalentNotAccumulatorEPNS2_12RegisterInfoE
	.section	.text.unlikely._ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE,"ax",@progbits
	.align 2
.LCOLDB1:
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE,"ax",@progbits
.LHOTB1:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE:
.LFB6883:
	.cfi_startproc
	endbr64
	cmpb	$0, 8(%rsi)
	jne	.L179
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L169:
	movq	16(%rax), %rax
	cmpq	%rax, %rbx
	je	.L182
	cmpb	$0, 8(%rax)
	je	.L169
	movl	(%rax), %esi
	movq	176(%r12), %rdi
	movl	8(%r12), %eax
	movl	(%rbx), %r13d
	movq	(%rdi), %rcx
	cmpl	%eax, %esi
	je	.L183
	cmpl	%eax, %r13d
	je	.L184
	movl	%r13d, %edx
	call	*32(%rcx)
.L174:
	cmpl	8(%r12), %r13d
	je	.L176
	cmpl	%r13d, 28(%r12)
	cmovge	28(%r12), %r13d
	movl	%r13d, 28(%r12)
.L176:
	movb	$1, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	%r13d, %esi
	call	*24(%rcx)
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L184:
	call	*16(%rcx)
	jmp	.L174
.L182:
	jmp	.L171
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE
	.cfi_startproc
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE.cold, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE.cold:
.LFSB6883:
.L171:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	0, %eax
	ud2
	.cfi_endproc
.LFE6883:
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE
	.section	.text.unlikely._ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE.cold, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE.cold
.LCOLDE1:
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE
.LHOTE1:
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE:
.LFB6886:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	addl	64(%rdi), %esi
	movq	40(%rdi), %rax
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rbx
	cmpb	$0, 8(%rbx)
	jne	.L195
.L186:
	movl	168(%r12), %eax
	movq	%rbx, %xmm0
	punpcklqdq	%xmm0, %xmm0
	addl	$1, %eax
	movl	%eax, 168(%r12)
	movq	16(%rbx), %rcx
	movq	24(%rbx), %rdx
	movq	%rdx, 24(%rcx)
	movq	16(%rbx), %rcx
	movq	%rcx, 16(%rdx)
	movl	%eax, 4(%rbx)
	movb	$1, 8(%rbx)
	movups	%xmm0, 16(%rbx)
	movl	28(%r12), %eax
	cmpl	%eax, (%rbx)
	cmovge	(%rbx), %eax
	popq	%rbx
	movl	%eax, 28(%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	movq	16(%rbx), %rax
	cmpq	%rax, %rbx
	je	.L186
	xorl	%edx, %edx
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L197:
	movl	(%rax), %edi
	cmpl	%edi, (%rdx)
	cmovg	%rax, %rdx
.L187:
	movq	16(%rax), %rax
	cmpq	%rax, %rbx
	je	.L196
.L188:
	cmpb	$0, 8(%rax)
	jne	.L186
	cmpb	$0, 9(%rax)
	je	.L187
	testq	%rdx, %rdx
	jne	.L197
	movq	%rax, %rdx
	movq	16(%rax), %rax
	cmpq	%rax, %rbx
	jne	.L188
	.p2align 4,,10
	.p2align 3
.L196:
	testq	%rdx, %rdx
	je	.L186
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer22OutputRegisterTransferEPNS2_12RegisterInfoES4_
	jmp	.L186
	.cfi_endproc
.LFE6886:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer25PrepareOutputRegisterListENS1_12RegisterListE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer25PrepareOutputRegisterListENS1_12RegisterListE
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer25PrepareOutputRegisterListENS1_12RegisterListE, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer25PrepareOutputRegisterListENS1_12RegisterListE:
.LFB6887:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	sarq	$32, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%r14d, %r14d
	je	.L198
	jle	.L198
	movq	%rdi, %r12
	movl	%esi, %r13d
	addl	%esi, %r14d
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L200:
	movl	168(%r12), %eax
	movq	%rbx, %xmm0
	punpcklqdq	%xmm0, %xmm0
	addl	$1, %eax
	movl	%eax, 168(%r12)
	movq	16(%rbx), %rcx
	movq	24(%rbx), %rdx
	movq	%rdx, 24(%rcx)
	movq	16(%rbx), %rcx
	movq	%rcx, 16(%rdx)
	movl	%eax, 4(%rbx)
	movb	$1, 8(%rbx)
	movups	%xmm0, 16(%rbx)
	movl	28(%r12), %eax
	cmpl	%eax, (%rbx)
	cmovge	(%rbx), %eax
	addl	$1, %r13d
	movl	%eax, 28(%r12)
	cmpl	%r14d, %r13d
	je	.L198
.L203:
	movl	64(%r12), %eax
	movq	40(%r12), %rdx
	addl	%r13d, %eax
	cltq
	movq	(%rdx,%rax,8), %rbx
	cmpb	$0, 8(%rbx)
	je	.L200
	movq	16(%rbx), %rax
	cmpq	%rax, %rbx
	je	.L200
	xorl	%edx, %edx
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L212:
	movl	(%rax), %edi
	cmpl	%edi, (%rdx)
	cmovg	%rax, %rdx
.L201:
	movq	16(%rax), %rax
	cmpq	%rax, %rbx
	je	.L211
.L202:
	cmpb	$0, 8(%rax)
	jne	.L200
	cmpb	$0, 9(%rax)
	je	.L201
	testq	%rdx, %rdx
	jne	.L212
	movq	%rax, %rdx
	movq	16(%rax), %rax
	cmpq	%rax, %rbx
	jne	.L202
	.p2align 4,,10
	.p2align 3
.L211:
	testq	%rdx, %rdx
	je	.L200
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer22OutputRegisterTransferEPNS2_12RegisterInfoES4_
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L198:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6887:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer25PrepareOutputRegisterListENS1_12RegisterListE, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer25PrepareOutputRegisterListENS1_12RegisterListE
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE:
.LFB6888:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	64(%rdi), %eax
	movq	40(%rdi), %rdx
	addl	%esi, %eax
	cltq
	movq	(%rdx,%rax,8), %rbx
	cmpb	$0, 8(%rbx)
	je	.L214
.L217:
	addq	$8, %rsp
	movl	%esi, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	movl	8(%rdi), %edx
	movq	%rbx, %rax
	.p2align 4,,10
	.p2align 3
.L223:
	movq	16(%rax), %rax
	cmpq	%rax, %rbx
	je	.L226
.L224:
	cmpb	$0, 8(%rax)
	je	.L223
	movl	(%rax), %esi
	cmpl	%esi, %edx
	jne	.L217
	movq	16(%rax), %rax
	cmpq	%rax, %rbx
	jne	.L224
.L226:
	movq	%rbx, %rsi
.L218:
	movq	16(%rsi), %rsi
	cmpq	%rsi, %rbx
	je	.L227
	cmpb	$0, 8(%rsi)
	je	.L218
.L220:
	movq	%rbx, %rdx
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer22OutputRegisterTransferEPNS2_12RegisterInfoES4_
	movl	(%rbx), %esi
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L227:
	xorl	%esi, %esi
	jmp	.L220
	.cfi_endproc
.LFE6888:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer20GetInputRegisterListENS1_12RegisterListE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer20GetInputRegisterListENS1_12RegisterListE
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer20GetInputRegisterListENS1_12RegisterListE, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer20GetInputRegisterListENS1_12RegisterListE:
.LFB6889:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	sarq	$32, %r13
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	cmpl	$1, %r13d
	je	.L239
	testl	%r13d, %r13d
	jle	.L231
	movl	%esi, %r14d
	leal	0(%r13,%rsi), %r12d
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L240:
	addl	$1, %r14d
	cmpl	%r14d, %r12d
	je	.L231
.L236:
	movl	64(%r15), %eax
	movq	40(%r15), %rdx
	addl	%r14d, %eax
	cltq
	movq	(%rdx,%rax,8), %rdx
	cmpb	$0, 8(%rdx)
	movq	%rdx, %rsi
	jne	.L240
	.p2align 4,,10
	.p2align 3
.L233:
	movq	16(%rsi), %rsi
	cmpq	%rsi, %rdx
	je	.L241
	cmpb	$0, 8(%rsi)
	je	.L233
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer22OutputRegisterTransferEPNS2_12RegisterInfoES4_
.L242:
	addl	$1, %r14d
	cmpl	%r14d, %r12d
	jne	.L236
.L231:
	addq	$8, %rsp
	salq	$32, %r13
	movl	%ebx, %eax
	orq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer22OutputRegisterTransferEPNS2_12RegisterInfoES4_
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L239:
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE
	addq	$8, %rsp
	movl	%eax, %eax
	popq	%rbx
	popq	%r12
	btsq	$32, %rax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6889:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer20GetInputRegisterListENS1_12RegisterListE, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer20GetInputRegisterListENS1_12RegisterListE
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer16AllocateRegisterEPNS2_12RegisterInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16AllocateRegisterEPNS2_12RegisterInfoE
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16AllocateRegisterEPNS2_12RegisterInfoE, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16AllocateRegisterEPNS2_12RegisterInfoE:
.LFB6891:
	.cfi_startproc
	endbr64
	cmpb	$0, 8(%rsi)
	movb	$1, 9(%rsi)
	jne	.L243
	movl	168(%rdi), %eax
	movq	%rsi, %xmm0
	punpcklqdq	%xmm0, %xmm0
	addl	$1, %eax
	movl	%eax, 168(%rdi)
	movq	16(%rsi), %rcx
	movq	24(%rsi), %rdx
	movq	%rdx, 24(%rcx)
	movq	16(%rsi), %rcx
	movq	%rcx, 16(%rdx)
	movl	%eax, 4(%rsi)
	movb	$1, 8(%rsi)
	movups	%xmm0, 16(%rsi)
.L243:
	ret
	.cfi_endproc
.LFE6891:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16AllocateRegisterEPNS2_12RegisterInfoE, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16AllocateRegisterEPNS2_12RegisterInfoE
	.section	.rodata._ZNSt6vectorIPN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfoENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC2:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIPN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfoENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIPN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfoENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfoENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	.type	_ZNSt6vectorIPN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfoENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm, @function
_ZNSt6vectorIPN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfoENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm:
.LFB7529:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L273
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$268435455, %esi
	subq	$24, %rsp
	movq	16(%rdi), %rcx
	movq	24(%r13), %rax
	movq	%rcx, %rdx
	subq	8(%rdi), %rdx
	subq	%rcx, %rax
	movq	%rsi, %rdi
	movq	%rdx, %r14
	sarq	$3, %rax
	sarq	$3, %r14
	subq	%r14, %rdi
	cmpq	%rbx, %rax
	jb	.L247
	salq	$3, %rbx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	addq	%rbx, %rcx
	movq	%rcx, 16(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rdi
	jb	.L276
	cmpq	%r14, %rbx
	movq	%r14, %r15
	movq	0(%r13), %rdi
	cmovnb	%rbx, %r15
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	addq	%r14, %r15
	cmpq	$268435455, %r15
	cmova	%rsi, %r15
	subq	%r12, %rax
	salq	$3, %r15
	movq	%r15, %rsi
	cmpq	%r15, %rax
	jb	.L277
	addq	%r12, %rsi
	movq	%rsi, 16(%rdi)
.L251:
	leaq	0(,%rbx,8), %r8
	leaq	(%r12,%rdx), %rdi
	xorl	%esi, %esi
	movq	%r8, %rdx
	call	memset@PLT
	movq	16(%r13), %rax
	movq	8(%r13), %rdx
	cmpq	%rdx, %rax
	je	.L256
	subq	$8, %rax
	leaq	15(%rdx), %rcx
	subq	%rdx, %rax
	subq	%r12, %rcx
	movq	%rax, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rcx
	jbe	.L253
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rsi
	je	.L253
	addq	$1, %rsi
	movq	%rdx, %rdi
	movq	%r12, %rax
	movq	%rsi, %rcx
	subq	%r12, %rdi
	shrq	%rcx
	salq	$4, %rcx
	addq	%r12, %rcx
	.p2align 4,,10
	.p2align 3
.L254:
	movdqu	(%rax,%rdi), %xmm0
	addq	$16, %rax
	movups	%xmm0, -16(%rax)
	cmpq	%rax, %rcx
	jne	.L254
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	addq	%rcx, %rdx
	addq	%r12, %rcx
	cmpq	%rax, %rsi
	je	.L256
	movq	(%rdx), %rax
	movq	%rax, (%rcx)
.L256:
	addq	%r14, %rbx
	movq	%r12, 8(%r13)
	leaq	(%r12,%rbx,8), %rax
	addq	%r15, %r12
	movq	%rax, 16(%r13)
	movq	%r12, 24(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L253:
	.cfi_restore_state
	leaq	8(%rax,%r12), %rsi
	subq	%r12, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L258:
	movq	(%rax,%rdx), %rcx
	addq	$8, %rax
	movq	%rcx, -8(%rax)
	cmpq	%rax, %rsi
	jne	.L258
	jmp	.L256
.L277:
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L251
.L276:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7529:
	.size	_ZNSt6vectorIPN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfoENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm, .-_ZNSt6vectorIPN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfoENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer15GrowRegisterMapENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer15GrowRegisterMapENS1_8RegisterE
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer15GrowRegisterMapENS1_8RegisterE, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer15GrowRegisterMapENS1_8RegisterE:
.LFB6890:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	48(%rdi), %rdx
	movq	40(%rdi), %rcx
	addl	64(%rdi), %esi
	movq	%rdx, %rax
	movslq	%esi, %r14
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%r14, %rax
	jbe	.L286
.L278:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L286:
	.cfi_restore_state
	leaq	1(%r14), %rsi
	movq	%rdi, %rbx
	leaq	32(%rdi), %rdi
	movq	%rax, %r12
	cmpq	%rsi, %rax
	jb	.L287
	cmpq	%rsi, %rax
	jbe	.L278
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdx
	je	.L278
	movq	%rax, 48(%rbx)
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L287:
	subq	%rax, %rsi
	call	_ZNSt6vectorIPN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfoENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L282:
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L283:
	movl	$1, %edx
	movl	%r15d, (%rax)
	movl	%r13d, 4(%rax)
	movw	%dx, 8(%rax)
	movb	$0, 10(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 24(%rax)
	movq	40(%rbx), %rdx
	movq	%rax, (%rdx,%r12,8)
	leaq	1(%r12), %rax
	cmpq	%r12, %r14
	je	.L278
	movq	%rax, %r12
.L281:
	movl	168(%rbx), %eax
	movq	192(%rbx), %rdi
	movl	%r12d, %r15d
	subl	64(%rbx), %r15d
	leal	1(%rax), %r13d
	movl	%r13d, 168(%rbx)
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	ja	.L282
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L283
	.cfi_endproc
.LFE6890:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer15GrowRegisterMapENS1_8RegisterE, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer15GrowRegisterMapENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer21RegisterAllocateEventENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21RegisterAllocateEventENS1_8RegisterE
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21RegisterAllocateEventENS1_8RegisterE, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21RegisterAllocateEventENS1_8RegisterE:
.LFB6892:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdx
	movq	48(%rdi), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	64(%rdi), %r12d
	sarq	$3, %rax
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addl	%esi, %r12d
	movslq	%r12d, %r12
	cmpq	%rax, %r12
	jnb	.L289
	movq	(%rdx,%r12,8), %rax
.L290:
	cmpb	$0, 8(%rax)
	movb	$1, 9(%rax)
	jne	.L288
	movl	168(%rbx), %ecx
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	leal	1(%rcx), %edx
	movl	%edx, 168(%rbx)
	movq	16(%rax), %rsi
	movq	24(%rax), %rcx
	movq	%rcx, 24(%rsi)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movl	%edx, 4(%rax)
	movb	$1, 8(%rax)
	movups	%xmm0, 16(%rax)
.L288:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer15GrowRegisterMapENS1_8RegisterE
	movq	40(%rbx), %rax
	movq	(%rax,%r12,8), %rax
	jmp	.L290
	.cfi_endproc
.LFE6892:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21RegisterAllocateEventENS1_8RegisterE, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21RegisterAllocateEventENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer25RegisterListAllocateEventENS1_12RegisterListE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer25RegisterListAllocateEventENS1_12RegisterListE
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer25RegisterListAllocateEventENS1_12RegisterListE, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer25RegisterListAllocateEventENS1_12RegisterListE:
.LFB6893:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	sarq	$32, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%r14d, %r14d
	jne	.L299
.L293:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L299:
	.cfi_restore_state
	leal	(%r14,%rsi), %r13d
	movq	%rsi, %rbx
	movq	%rdi, %r12
	leal	-1(%r13), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer15GrowRegisterMapENS1_8RegisterE
	testl	%r14d, %r14d
	jle	.L293
	movl	%ebx, %esi
	.p2align 4,,10
	.p2align 3
.L296:
	movl	64(%r12), %eax
	movq	40(%r12), %rdx
	addl	%esi, %eax
	cltq
	movq	(%rdx,%rax,8), %rax
	cmpb	$0, 8(%rax)
	movb	$1, 9(%rax)
	jne	.L295
	movl	168(%r12), %ebx
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	leal	1(%rbx), %edx
	movl	%edx, 168(%r12)
	movq	16(%rax), %rdi
	movq	24(%rax), %rcx
	movq	%rcx, 24(%rdi)
	movq	16(%rax), %rdi
	movq	%rdi, 16(%rcx)
	movl	%edx, 4(%rax)
	movb	$1, 8(%rax)
	movups	%xmm0, 16(%rax)
.L295:
	addl	$1, %esi
	cmpl	%r13d, %esi
	jne	.L296
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6893:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer25RegisterListAllocateEventENS1_12RegisterListE, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer25RegisterListAllocateEventENS1_12RegisterListE
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizerC2EPNS0_4ZoneEPNS1_25BytecodeRegisterAllocatorEiiPNS2_14BytecodeWriterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizerC2EPNS0_4ZoneEPNS1_25BytecodeRegisterAllocatorEiiPNS2_14BytecodeWriterE
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizerC2EPNS0_4ZoneEPNS1_25BytecodeRegisterAllocatorEiiPNS2_14BytecodeWriterE, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizerC2EPNS0_4ZoneEPNS1_25BytecodeRegisterAllocatorEiiPNS2_14BytecodeWriterE:
.LFB6874:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal11interpreter25BytecodeRegisterOptimizerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r8d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rax, (%rdi)
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal11interpreter8Register19virtual_accumulatorEv@PLT
	movl	-56(%rbp), %ecx
	pxor	%xmm0, %xmm0
	movq	%r13, 32(%rbx)
	movl	%eax, 8(%rbx)
	movl	%ecx, 24(%rbx)
	subl	$1, %ecx
	movq	$0, 40(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 56(%rbx)
	movq	%r13, 72(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 88(%rbx)
	movups	%xmm0, 104(%rbx)
	movups	%xmm0, 120(%rbx)
	movups	%xmm0, 136(%rbx)
	movups	%xmm0, 152(%rbx)
	movq	16(%r13), %rdx
	movq	24(%r13), %rax
	movl	%ecx, 28(%rbx)
	subq	%rdx, %rax
	movq	$8, 96(%rbx)
	cmpq	$63, %rax
	jbe	.L320
	leaq	64(%rdx), %rax
	movq	%rdx, 88(%rbx)
	addq	$24, %rdx
	movq	%rax, 16(%r13)
.L303:
	movq	72(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	$511, %rcx
	jbe	.L304
	leaq	512(%rax), %rcx
	movq	%rcx, 16(%rdi)
.L306:
	movq	%rax, (%rdx)
	movl	%r12d, %esi
	xorl	%edi, %edi
	movq	%rdx, 128(%rbx)
	movq	(%rdx), %rcx
	movq	%rdx, 160(%rbx)
	leaq	512(%rcx), %rax
	movq	%rcx, 112(%rbx)
	movq	%rax, 120(%rbx)
	movq	(%rdx), %rax
	movq	%rcx, 104(%rbx)
	leaq	512(%rax), %rdx
	movq	%rbx, 8(%r14)
	movq	%rdx, 152(%rbx)
	movq	%rax, 144(%rbx)
	movq	%rax, 136(%rbx)
	movl	$0, 168(%rbx)
	movq	%r15, 176(%rbx)
	movb	$0, 184(%rbx)
	movq	%r13, 192(%rbx)
	call	_ZN2v88internal11interpreter8Register18FromParameterIndexEii@PLT
	movslq	24(%rbx), %rsi
	movq	40(%rbx), %rdi
	negl	%eax
	movslq	%eax, %rdx
	movl	%eax, 64(%rbx)
	addq	%rdx, %rsi
	movq	48(%rbx), %rdx
	movq	%rdx, %rcx
	subq	%rdi, %rcx
	sarq	$3, %rcx
	cmpq	%rcx, %rsi
	ja	.L321
	jb	.L322
.L311:
	xorl	%r12d, %r12d
	cmpq	%rdx, %rdi
	je	.L310
	.p2align 4,,10
	.p2align 3
.L309:
	movl	%r12d, %r15d
	movq	24(%r13), %rdx
	subl	%eax, %r15d
	movl	168(%rbx), %eax
	leal	1(%rax), %r14d
	movq	16(%r13), %rax
	movl	%r14d, 168(%rbx)
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L323
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%r13)
.L313:
	movl	$257, %edx
	movl	%r15d, (%rax)
	movl	%r14d, 4(%rax)
	movw	%dx, 8(%rax)
	movb	$0, 10(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 24(%rax)
	movq	40(%rbx), %rdx
	movq	%rax, (%rdx,%r12,8)
	movq	40(%rbx), %rdx
	addq	$1, %r12
	movq	48(%rbx), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %r12
	movl	64(%rbx), %eax
	jb	.L309
.L310:
	addl	8(%rbx), %eax
	cltq
	movq	(%rdx,%rax,8), %rax
	movq	%rax, 16(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L323:
	.cfi_restore_state
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L322:
	leaq	(%rdi,%rsi,8), %rcx
	cmpq	%rcx, %rdx
	je	.L311
	movq	%rcx, 48(%rbx)
	movq	%rcx, %rdx
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L320:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	movq	96(%rbx), %rax
	movq	%rcx, 88(%rbx)
	leaq	-4(,%rax,4), %rdx
	movq	80(%rbx), %rax
	andq	$-8, %rdx
	addq	%rcx, %rdx
	testq	%rax, %rax
	je	.L303
	cmpq	$63, 8(%rax)
	jbe	.L303
	movq	(%rax), %rcx
	movq	%rcx, 80(%rbx)
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L321:
	leaq	32(%rbx), %rdi
	subq	%rcx, %rsi
	call	_ZNSt6vectorIPN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfoENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	movq	48(%rbx), %rdx
	movq	40(%rbx), %rdi
	movl	64(%rbx), %eax
	jmp	.L311
.L304:
	movl	$512, %esi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	jmp	.L306
	.cfi_endproc
.LFE6874:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizerC2EPNS0_4ZoneEPNS1_25BytecodeRegisterAllocatorEiiPNS2_14BytecodeWriterE, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizerC2EPNS0_4ZoneEPNS1_25BytecodeRegisterAllocatorEiiPNS2_14BytecodeWriterE
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizerC1EPNS0_4ZoneEPNS1_25BytecodeRegisterAllocatorEiiPNS2_14BytecodeWriterE
	.set	_ZN2v88internal11interpreter25BytecodeRegisterOptimizerC1EPNS0_4ZoneEPNS1_25BytecodeRegisterAllocatorEiiPNS2_14BytecodeWriterE,_ZN2v88internal11interpreter25BytecodeRegisterOptimizerC2EPNS0_4ZoneEPNS1_25BytecodeRegisterAllocatorEiiPNS2_14BytecodeWriterE
	.section	.rodata._ZNSt5dequeIPN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfoENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIPN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfoENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIPN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfoENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfoENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_
	.type	_ZNSt5dequeIPN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfoENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_, @function
_ZNSt5dequeIPN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfoENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_:
.LFB7535:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	88(%rdi), %r13
	movq	56(%rdi), %rsi
	movq	64(%rbx), %rdx
	subq	72(%rbx), %rdx
	movq	%r13, %r14
	movq	%rdx, %rcx
	subq	%rsi, %r14
	sarq	$3, %rcx
	movq	%r14, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	48(%rbx), %rax
	subq	32(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$268435455, %rax
	je	.L342
	movq	16(%rbx), %rcx
	movq	24(%rbx), %rdx
	movq	%r13, %rax
	subq	%rcx, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L343
.L326:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L334
	cmpq	$63, 8(%rax)
	ja	.L344
.L334:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L345
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L335:
	movq	%rax, 8(%r13)
	movq	(%r12), %rdx
	movq	64(%rbx), %rax
	movq	%rdx, (%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L343:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L346
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r13
	leaq	2(%rdx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L347
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L331:
	movq	%rcx, %rax
	movq	56(%rbx), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r15
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L332
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L332:
	movq	24(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L333
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L333:
	movq	%r13, 16(%rbx)
	movq	%rcx, 24(%rbx)
.L329:
	movq	%r15, 56(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r14), %r13
	movq	(%r15), %xmm0
	movq	%r13, 88(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 72(%rbx)
	addq	$512, %rax
	movq	%rax, 80(%rbx)
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L344:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L346:
	subq	%r15, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%rcx,%rdx,8), %r15
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L328
	cmpq	%r13, %rsi
	je	.L329
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L328:
	cmpq	%r13, %rsi
	je	.L329
	leaq	8(%r14), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L345:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L347:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L331
.L342:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7535:
	.size	_ZNSt5dequeIPN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfoENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_, .-_ZNSt5dequeIPN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfoENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer19AddToEquivalenceSetEPNS2_12RegisterInfoES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer19AddToEquivalenceSetEPNS2_12RegisterInfoES4_
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer19AddToEquivalenceSetEPNS2_12RegisterInfoES4_, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer19AddToEquivalenceSetEPNS2_12RegisterInfoES4_:
.LFB6884:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 10(%rdx)
	movq	%rdx, -48(%rbp)
	je	.L353
.L349:
	movq	16(%rbx), %rdx
	movq	24(%rbx), %rax
	movq	%rax, 24(%rdx)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%r12), %rax
	movq	%r12, 24(%rbx)
	movq	%rax, 16(%rbx)
	movq	%rbx, 16(%r12)
	movq	16(%rbx), %rax
	movq	%rbx, 24(%rax)
	movl	4(%r12), %eax
	movb	$0, 8(%rbx)
	movl	%eax, 4(%rbx)
	movb	$1, 184(%r13)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L354
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L353:
	.cfi_restore_state
	movb	$1, 10(%rdx)
	movq	152(%rdi), %rax
	movq	136(%rdi), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L350
	movq	%rbx, (%rdx)
	addq	$8, 136(%rdi)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L350:
	leaq	-48(%rbp), %rsi
	leaq	72(%rdi), %rdi
	call	_ZNSt5dequeIPN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfoENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_
	jmp	.L349
.L354:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6884:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer19AddToEquivalenceSetEPNS2_12RegisterInfoES4_, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer19AddToEquivalenceSetEPNS2_12RegisterInfoES4_
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer27PushToRegistersNeedingFlushEPNS2_12RegisterInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer27PushToRegistersNeedingFlushEPNS2_12RegisterInfoE
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer27PushToRegistersNeedingFlushEPNS2_12RegisterInfoE, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer27PushToRegistersNeedingFlushEPNS2_12RegisterInfoE:
.LFB6876:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	cmpb	$0, 10(%rsi)
	movq	%rsi, -8(%rbp)
	je	.L359
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_restore_state
	movb	$1, 10(%rsi)
	movq	152(%rdi), %rdx
	movq	136(%rdi), %rcx
	subq	$8, %rdx
	cmpq	%rdx, %rcx
	je	.L357
	movq	%rsi, (%rcx)
	addq	$8, 136(%rdi)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L357:
	.cfi_restore_state
	leaq	-8(%rbp), %rsi
	addq	$72, %rdi
	call	_ZNSt5dequeIPN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfoENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6876:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer27PushToRegistersNeedingFlushEPNS2_12RegisterInfoE, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer27PushToRegistersNeedingFlushEPNS2_12RegisterInfoE
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterOptimizer16RegisterTransferEPNS2_12RegisterInfoES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16RegisterTransferEPNS2_12RegisterInfoES4_
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16RegisterTransferEPNS2_12RegisterInfoES4_, @function
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16RegisterTransferEPNS2_12RegisterInfoES4_:
.LFB6885:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movl	4(%rsi), %r13d
	movl	4(%rdx), %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rdx), %eax
	cmpl	%eax, 8(%rdi)
	je	.L390
	cmpl	%eax, 24(%rdi)
	jle	.L390
	movzbl	8(%rdx), %eax
	movl	$1, %ecx
	cmpl	%r14d, %r13d
	jne	.L365
	testb	%al, %al
	jne	.L360
.L373:
	movb	$0, 8(%r12)
	movq	%rbx, %rsi
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L393:
	movq	16(%rsi), %rsi
	cmpq	%rsi, %rbx
	je	.L392
.L366:
	cmpb	$0, 8(%rsi)
	je	.L393
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer22OutputRegisterTransferEPNS2_12RegisterInfoES4_
.L401:
	movl	(%rbx), %eax
	cmpl	8(%r15), %eax
	jne	.L394
	.p2align 4,,10
	.p2align 3
.L360:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L395
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L390:
	.cfi_restore_state
	cmpl	%r14d, %r13d
	je	.L360
	movzbl	8(%r12), %eax
	xorl	%ecx, %ecx
.L365:
	testb	%al, %al
	jne	.L396
.L380:
	cmpb	$0, 10(%r12)
	movq	%r12, -64(%rbp)
	je	.L397
.L371:
	movq	16(%r12), %rdx
	movq	24(%r12), %rax
	movq	%rax, 24(%rdx)
	movq	16(%r12), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbx), %rax
	movq	%rbx, 24(%r12)
	movq	%rax, 16(%r12)
	movq	%r12, 16(%rbx)
	movq	16(%r12), %rax
	movq	%r12, 24(%rax)
	movl	4(%rbx), %eax
	movb	$0, 8(%r12)
	movl	%eax, 4(%r12)
	movb	$1, 184(%r15)
.L370:
	testb	%cl, %cl
	jne	.L373
	movl	(%rbx), %eax
	cmpl	8(%r15), %eax
	je	.L360
.L394:
	movl	24(%r15), %edx
	cmpl	%edx, %eax
	jge	.L360
	movq	16(%rbx), %rax
	cmpq	%rax, %rbx
	je	.L360
	.p2align 4,,10
	.p2align 3
.L378:
	cmpl	(%rax), %edx
	jg	.L376
.L398:
	movb	$0, 8(%rax)
	movq	16(%rax), %rax
	cmpq	%rax, %rbx
	je	.L360
	cmpl	(%rax), %edx
	jle	.L398
.L376:
	movq	16(%rax), %rax
	cmpq	%rax, %rbx
	jne	.L378
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L396:
	movq	16(%r12), %rax
	cmpq	%rax, %r12
	je	.L367
	xorl	%edx, %edx
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L400:
	movl	(%rax), %edi
	cmpl	%edi, (%rdx)
	cmovg	%rax, %rdx
.L368:
	movq	16(%rax), %rax
	cmpq	%rax, %r12
	je	.L399
.L369:
	cmpb	$0, 8(%rax)
	jne	.L367
	cmpb	$0, 9(%rax)
	je	.L368
	testq	%rdx, %rdx
	jne	.L400
	movq	%rax, %rdx
	movq	16(%rax), %rax
	cmpq	%rax, %r12
	jne	.L369
	.p2align 4,,10
	.p2align 3
.L399:
	testq	%rdx, %rdx
	je	.L367
	movq	%r12, %rsi
	movq	%r15, %rdi
	movb	%cl, -65(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer22OutputRegisterTransferEPNS2_12RegisterInfoES4_
	movzbl	-65(%rbp), %ecx
.L367:
	cmpl	%r14d, %r13d
	je	.L370
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L392:
	xorl	%esi, %esi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer22OutputRegisterTransferEPNS2_12RegisterInfoES4_
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L397:
	movb	$1, 10(%r12)
	movq	152(%r15), %rax
	movq	136(%r15), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L372
	movq	%r12, (%rdx)
	addq	$8, 136(%r15)
	jmp	.L371
.L372:
	leaq	-64(%rbp), %rsi
	leaq	72(%r15), %rdi
	movb	%cl, -65(%rbp)
	call	_ZNSt5dequeIPN2v88internal11interpreter25BytecodeRegisterOptimizer12RegisterInfoENS1_22RecyclingZoneAllocatorIS5_EEE16_M_push_back_auxIJRKS5_EEEvDpOT_
	movzbl	-65(%rbp), %ecx
	jmp	.L371
.L395:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6885:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16RegisterTransferEPNS2_12RegisterInfoES4_, .-_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16RegisterTransferEPNS2_12RegisterInfoES4_
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11interpreter25BytecodeRegisterOptimizer21kInvalidEquivalenceIdE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11interpreter25BytecodeRegisterOptimizer21kInvalidEquivalenceIdE, @function
_GLOBAL__sub_I__ZN2v88internal11interpreter25BytecodeRegisterOptimizer21kInvalidEquivalenceIdE:
.LFB7871:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE7871:
	.size	_GLOBAL__sub_I__ZN2v88internal11interpreter25BytecodeRegisterOptimizer21kInvalidEquivalenceIdE, .-_GLOBAL__sub_I__ZN2v88internal11interpreter25BytecodeRegisterOptimizer21kInvalidEquivalenceIdE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11interpreter25BytecodeRegisterOptimizer21kInvalidEquivalenceIdE
	.weak	_ZTVN2v88internal11interpreter25BytecodeRegisterOptimizerE
	.section	.data.rel.ro.local._ZTVN2v88internal11interpreter25BytecodeRegisterOptimizerE,"awG",@progbits,_ZTVN2v88internal11interpreter25BytecodeRegisterOptimizerE,comdat
	.align 8
	.type	_ZTVN2v88internal11interpreter25BytecodeRegisterOptimizerE, @object
	.size	_ZTVN2v88internal11interpreter25BytecodeRegisterOptimizerE, 56
_ZTVN2v88internal11interpreter25BytecodeRegisterOptimizerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11interpreter25BytecodeRegisterOptimizerD1Ev
	.quad	_ZN2v88internal11interpreter25BytecodeRegisterOptimizerD0Ev
	.quad	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21RegisterAllocateEventENS1_8RegisterE
	.quad	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer25RegisterListAllocateEventENS1_12RegisterListE
	.quad	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21RegisterListFreeEventENS1_12RegisterListE
	.globl	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21kInvalidEquivalenceIdE
	.section	.rodata._ZN2v88internal11interpreter25BytecodeRegisterOptimizer21kInvalidEquivalenceIdE,"a"
	.align 4
	.type	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21kInvalidEquivalenceIdE, @object
	.size	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21kInvalidEquivalenceIdE, 4
_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21kInvalidEquivalenceIdE:
	.long	-1
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
