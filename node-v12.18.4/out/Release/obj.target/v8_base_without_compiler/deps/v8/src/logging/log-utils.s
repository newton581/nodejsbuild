	.file	"log-utils.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB1391:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1391:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.text._ZN2v88internal3Log18CreateOutputHandleEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Log18CreateOutputHandleEPKc
	.type	_ZN2v88internal3Log18CreateOutputHandleEPKc, @function
_ZN2v88internal3Log18CreateOutputHandleEPKc:
.LFB17908:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal8FLAG_logE(%rip)
	jne	.L4
	cmpb	$0, _ZN2v88internal12FLAG_log_apiE(%rip)
	je	.L11
.L4:
	movzbl	(%rdi), %eax
	cmpl	$45, %eax
	jne	.L7
	cmpb	$0, 1(%rdi)
	jne	.L7
	movq	stdout(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	cmpl	$38, %eax
	jne	.L9
	cmpb	$0, 1(%rdi)
	jne	.L9
	jmp	_ZN2v84base2OS17OpenTemporaryFileEv@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	movq	_ZN2v84base2OS15LogFileOpenModeE(%rip), %rsi
	jmp	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	cmpb	$0, _ZN2v88internal13FLAG_log_codeE(%rip)
	jne	.L4
	cmpb	$0, _ZN2v88internal16FLAG_log_handlesE(%rip)
	jne	.L4
	cmpb	$0, _ZN2v88internal16FLAG_log_suspectE(%rip)
	jne	.L4
	cmpb	$0, _ZN2v88internal12FLAG_ll_profE(%rip)
	jne	.L4
	cmpb	$0, _ZN2v88internal20FLAG_perf_basic_profE(%rip)
	jne	.L4
	cmpb	$0, _ZN2v88internal14FLAG_perf_profE(%rip)
	jne	.L4
	cmpb	$0, _ZN2v88internal20FLAG_log_source_codeE(%rip)
	jne	.L4
	cmpb	$0, _ZN2v88internal30FLAG_log_internal_timer_eventsE(%rip)
	jne	.L4
	cmpb	$0, _ZN2v88internal13FLAG_prof_cppE(%rip)
	jne	.L4
	cmpb	$0, _ZN2v88internal13FLAG_trace_icE(%rip)
	jne	.L4
	cmpb	$0, _ZN2v88internal24FLAG_log_function_eventsE(%rip)
	jne	.L4
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE17908:
	.size	_ZN2v88internal3Log18CreateOutputHandleEPKc, .-_ZN2v88internal3Log18CreateOutputHandleEPKc
	.section	.text._ZN2v88internal3Log5CloseEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Log5CloseEv
	.type	_ZN2v88internal3Log5CloseEv, @function
_ZN2v88internal3Log5CloseEv:
.LFB17915:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %r12
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L13
	movq	_ZN2v88internal12FLAG_logfileE(%rip), %rax
	cmpb	$38, (%rax)
	je	.L27
.L16:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	fclose@PLT
.L13:
	movq	400(%rbx), %rdi
	movq	$0, 8(%rbx)
	testq	%rdi, %rdi
	je	.L15
	call	_ZdaPv@PLT
.L15:
	movq	%r12, %rax
	movb	$0, (%rbx)
	movq	$0, 400(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	cmpb	$0, 1(%rax)
	je	.L13
	jmp	.L16
	.cfi_endproc
.LFE17915:
	.size	_ZN2v88internal3Log5CloseEv, .-_ZN2v88internal3Log5CloseEv
	.section	.text._ZN2v88internal3Log14MessageBuilderC2EPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Log14MessageBuilderC2EPS1_
	.type	_ZN2v88internal3Log14MessageBuilderC2EPS1_, @function
_ZN2v88internal3Log14MessageBuilderC2EPS1_:
.LFB17917:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movq	%rdi, %rax
	leaq	360(%rsi), %rdi
	movq	%rdi, 8(%rax)
	jmp	_ZN2v84base5Mutex4LockEv@PLT
	.cfi_endproc
.LFE17917:
	.size	_ZN2v88internal3Log14MessageBuilderC2EPS1_, .-_ZN2v88internal3Log14MessageBuilderC2EPS1_
	.globl	_ZN2v88internal3Log14MessageBuilderC1EPS1_
	.set	_ZN2v88internal3Log14MessageBuilderC1EPS1_,_ZN2v88internal3Log14MessageBuilderC2EPS1_
	.section	.text._ZN2v88internal3Log14MessageBuilder22FormatStringIntoBufferEPKcP13__va_list_tag,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Log14MessageBuilder22FormatStringIntoBufferEPKcP13__va_list_tag
	.type	_ZN2v88internal3Log14MessageBuilder22FormatStringIntoBufferEPKcP13__va_list_tag, @function
_ZN2v88internal3Log14MessageBuilder22FormatStringIntoBufferEPKcP13__va_list_tag:
.LFB17929:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movl	$2048, %esi
	movq	400(%rax), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal9VSNPrintFENS0_6VectorIcEEPKcP13__va_list_tag@PLT
	movl	$2048, %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmpl	$-1, %eax
	cmove	%edx, %eax
	ret
	.cfi_endproc
.LFE17929:
	.size	_ZN2v88internal3Log14MessageBuilder22FormatStringIntoBufferEPKcP13__va_list_tag, .-_ZN2v88internal3Log14MessageBuilder22FormatStringIntoBufferEPKcP13__va_list_tag
	.section	.text._ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	.type	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz, @function
_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz:
.LFB17930:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$224, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdx, -192(%rbp)
	movq	%rcx, -184(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L33
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L33:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	movq	%r10, %rdx
	leaq	-240(%rbp), %rcx
	movq	%rax, -232(%rbp)
	movl	$2048, %esi
	leaq	-208(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	(%r14), %rax
	movl	$16, -240(%rbp)
	movq	400(%rax), %rdi
	movl	$48, -236(%rbp)
	call	_ZN2v88internal9VSNPrintFENS0_6VectorIcEEPKcP13__va_list_tag@PLT
	movl	%eax, %r12d
	cmpl	$-1, %eax
	je	.L41
	testl	%eax, %eax
	jle	.L32
.L35:
	xorl	%ebx, %ebx
	leaq	-241(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L37:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movq	400(%rdi), %rax
	addq	$16, %rdi
	movzbl	(%rax,%rbx), %eax
	addq	$1, %rbx
	movb	%al, -241(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, %r12d
	jg	.L37
.L32:
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L42
	addq	$224, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movl	$2048, %r12d
	jmp	.L35
.L42:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17930:
	.size	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz, .-_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	.section	.rodata._ZN2v88internal3Log14MessageBuilder15AppendCharacterEc.str1.1,"aMS",@progbits,1
.LC0:
	.string	"\\x2C"
.LC1:
	.string	"\\\\"
.LC2:
	.string	"\\n"
.LC3:
	.string	"\\x%02x"
	.section	.text._ZN2v88internal3Log14MessageBuilder15AppendCharacterEc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Log14MessageBuilder15AppendCharacterEc
	.type	_ZN2v88internal3Log14MessageBuilder15AppendCharacterEc, @function
_ZN2v88internal3Log14MessageBuilder15AppendCharacterEc:
.LFB17926:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leal	-32(%rsi), %eax
	cmpb	$94, %al
	ja	.L44
	cmpb	$44, %sil
	je	.L51
	cmpb	$92, %sil
	je	.L52
	movq	(%rdi), %rdi
	movb	%sil, -9(%rbp)
	movl	$1, %edx
	leaq	-9(%rbp), %rsi
	addq	$16, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L43:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L53
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	cmpb	$10, %sil
	je	.L54
	movzbl	%sil, %edx
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L51:
	leaq	.LC0(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L54:
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L52:
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L43
.L53:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17926:
	.size	_ZN2v88internal3Log14MessageBuilder15AppendCharacterEc, .-_ZN2v88internal3Log14MessageBuilder15AppendCharacterEc
	.section	.text._ZN2v88internal3Log14MessageBuilder12AppendStringENS0_6VectorIKcEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Log14MessageBuilder12AppendStringENS0_6VectorIKcEE
	.type	_ZN2v88internal3Log14MessageBuilder12AppendStringENS0_6VectorIKcEE, @function
_ZN2v88internal3Log14MessageBuilder12AppendStringENS0_6VectorIKcEE:
.LFB17921:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	leaq	(%rsi,%rdx), %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	%rbx, %rsi
	jnb	.L55
	movq	%rdi, %r12
	movq	%rsi, %r14
	leaq	-41(%rbp), %r13
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L68:
	cmpb	$44, %dl
	je	.L66
	cmpb	$92, %dl
	je	.L67
	movq	(%r12), %rax
	movb	%dl, -41(%rbp)
	movq	%r13, %rsi
	movl	$1, %edx
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L59:
	addq	$1, %r14
	cmpq	%rbx, %r14
	je	.L55
.L62:
	movzbl	(%r14), %edx
	leal	-32(%rdx), %eax
	cmpb	$94, %al
	jbe	.L68
	cmpb	$10, %dl
	je	.L69
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	addq	$1, %r14
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	cmpq	%rbx, %r14
	jne	.L62
.L55:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L70
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L67:
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L66:
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L59
.L70:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17921:
	.size	_ZN2v88internal3Log14MessageBuilder12AppendStringENS0_6VectorIKcEE, .-_ZN2v88internal3Log14MessageBuilder12AppendStringENS0_6VectorIKcEE
	.section	.text._ZN2v88internal3Log14MessageBuilder12AppendStringEPKcm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Log14MessageBuilder12AppendStringEPKcm
	.type	_ZN2v88internal3Log14MessageBuilder12AppendStringEPKcm, @function
_ZN2v88internal3Log14MessageBuilder12AppendStringEPKcm:
.LFB17924:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L71
	testq	%rdx, %rdx
	je	.L71
	movq	%rdi, %r12
	leaq	(%rsi,%rdx), %r13
	leaq	-41(%rbp), %r14
	movq	%rsi, %rbx
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L90:
	cmpb	$44, %dl
	je	.L88
	cmpb	$92, %dl
	je	.L89
	movq	(%r12), %rax
	movb	%dl, -41(%rbp)
	movq	%r14, %rsi
	movl	$1, %edx
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L75:
	addq	$1, %rbx
	cmpq	%rbx, %r13
	je	.L71
.L78:
	movzbl	(%rbx), %edx
	leal	-32(%rdx), %eax
	cmpb	$94, %al
	jbe	.L90
	cmpb	$10, %dl
	je	.L91
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	addq	$1, %rbx
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	cmpq	%rbx, %r13
	jne	.L78
.L71:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L92
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L89:
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L88:
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L75
.L92:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17924:
	.size	_ZN2v88internal3Log14MessageBuilder12AppendStringEPKcm, .-_ZN2v88internal3Log14MessageBuilder12AppendStringEPKcm
	.section	.text._ZN2v88internal3Log14MessageBuilder12AppendStringEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Log14MessageBuilder12AppendStringEPKc
	.type	_ZN2v88internal3Log14MessageBuilder12AppendStringEPKc, @function
_ZN2v88internal3Log14MessageBuilder12AppendStringEPKc:
.LFB17923:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L93
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movq	%rsi, %rbx
	call	strlen@PLT
	testq	%rax, %rax
	je	.L93
	leaq	(%rbx,%rax), %r13
	leaq	-41(%rbp), %r14
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L112:
	cmpb	$44, %dl
	je	.L110
	cmpb	$92, %dl
	je	.L111
	movq	(%r12), %rax
	movb	%dl, -41(%rbp)
	movq	%r14, %rsi
	movl	$1, %edx
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L97:
	addq	$1, %rbx
	cmpq	%rbx, %r13
	je	.L93
.L100:
	movzbl	(%rbx), %edx
	leal	-32(%rdx), %eax
	cmpb	$94, %al
	jbe	.L112
	cmpb	$10, %dl
	je	.L113
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	addq	$1, %rbx
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	cmpq	%rbx, %r13
	jne	.L100
.L93:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L114
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L111:
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L110:
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L97
.L114:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17923:
	.size	_ZN2v88internal3Log14MessageBuilder12AppendStringEPKc, .-_ZN2v88internal3Log14MessageBuilder12AppendStringEPKc
	.section	.text._ZN2v88internal3Log14MessageBuilder18AppendFormatStringEPKcz,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Log14MessageBuilder18AppendFormatStringEPKcz
	.type	_ZN2v88internal3Log14MessageBuilder18AppendFormatStringEPKcz, @function
_ZN2v88internal3Log14MessageBuilder18AppendFormatStringEPKcz:
.LFB17925:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$224, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdx, -192(%rbp)
	movq	%rcx, -184(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L116
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L116:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	movq	%r10, %rdx
	leaq	-240(%rbp), %rcx
	movq	%rax, -232(%rbp)
	movl	$2048, %esi
	leaq	-208(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	(%r14), %rax
	movl	$16, -240(%rbp)
	movq	400(%rax), %rdi
	movl	$48, -236(%rbp)
	call	_ZN2v88internal9VSNPrintFENS0_6VectorIcEEPKcP13__va_list_tag@PLT
	movl	%eax, %r12d
	cmpl	$-1, %eax
	je	.L129
	testl	%eax, %eax
	jle	.L115
.L118:
	xorl	%ebx, %ebx
	leaq	-241(%rbp), %r13
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L132:
	cmpb	$44, %dl
	je	.L130
	cmpb	$92, %dl
	je	.L131
	movb	%dl, -241(%rbp)
	addq	$16, %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L122:
	addq	$1, %rbx
	cmpl	%ebx, %r12d
	jle	.L115
.L125:
	movq	(%r14), %rdi
	movq	400(%rdi), %rax
	movzbl	(%rax,%rbx), %edx
	leal	-32(%rdx), %eax
	cmpb	$94, %al
	jbe	.L132
	cmpb	$10, %dl
	je	.L133
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	addq	$1, %rbx
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	cmpl	%ebx, %r12d
	jg	.L125
.L115:
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L134
	addq	$224, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L131:
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L130:
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L129:
	movl	$2048, %r12d
	jmp	.L118
.L134:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17925:
	.size	_ZN2v88internal3Log14MessageBuilder18AppendFormatStringEPKcz, .-_ZN2v88internal3Log14MessageBuilder18AppendFormatStringEPKcz
	.section	.rodata._ZN2v88internal3Log14MessageBuilder12AppendStringENS0_6StringENS_4base8OptionalIiEE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"unreachable code"
.LC5:
	.string	"\\u%04x"
	.section	.text._ZN2v88internal3Log14MessageBuilder12AppendStringENS0_6StringENS_4base8OptionalIiEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Log14MessageBuilder12AppendStringENS0_6StringENS_4base8OptionalIiEE
	.type	_ZN2v88internal3Log14MessageBuilder12AppendStringENS0_6StringENS_4base8OptionalIiEE, @function
_ZN2v88internal3Log14MessageBuilder12AppendStringENS0_6StringENS_4base8OptionalIiEE:
.LFB17919:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L135
	movl	11(%rsi), %eax
	movq	%rdi, %r13
	movq	%rsi, %r12
	testb	%dl, %dl
	jne	.L164
.L137:
	testl	%eax, %eax
	jle	.L135
	subl	$1, %eax
	leaq	-1(%r12), %r14
	xorl	%r15d, %r15d
	movq	%rax, -72(%rbp)
	leaq	.L141(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L157:
	movq	(%r14), %rax
	movl	%r15d, %edx
	movl	%r15d, %esi
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L139
	movzwl	%ax, %eax
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal3Log14MessageBuilder12AppendStringENS0_6StringENS_4base8OptionalIiEE,"a",@progbits
	.align 4
	.align 4
.L141:
	.long	.L147-.L141
	.long	.L144-.L141
	.long	.L146-.L141
	.long	.L142-.L141
	.long	.L139-.L141
	.long	.L140-.L141
	.long	.L139-.L141
	.long	.L139-.L141
	.long	.L145-.L141
	.long	.L144-.L141
	.long	.L143-.L141
	.long	.L142-.L141
	.long	.L139-.L141
	.long	.L140-.L141
	.section	.text._ZN2v88internal3Log14MessageBuilder12AppendStringENS0_6StringENS_4base8OptionalIiEE
	.p2align 4,,10
	.p2align 3
.L140:
	leaq	-64(%rbp), %rdi
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal10ThinString3GetEi@PLT
.L149:
	cmpw	$255, %ax
	jbe	.L165
.L150:
	movzwl	%ax, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
.L153:
	leaq	1(%r15), %rax
	cmpq	-72(%rbp), %r15
	je	.L135
.L168:
	movq	%rax, %r15
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L142:
	leaq	-64(%rbp), %rdi
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal12SlicedString3GetEi@PLT
	cmpw	$255, %ax
	ja	.L150
.L165:
	movl	%eax, %esi
	movzbl	%al, %edx
	subl	$32, %esi
	cmpb	$94, %sil
	jbe	.L166
.L151:
	cmpb	$10, %dl
	je	.L167
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	leaq	1(%r15), %rax
	cmpq	-72(%rbp), %r15
	jne	.L168
	.p2align 4,,10
	.p2align 3
.L135:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L169
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	leaq	-64(%rbp), %rdi
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal10ConsString3GetEi@PLT
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L146:
	movq	15(%r12), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzwl	(%rax,%r15,2), %eax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L147:
	leal	16(%r15,%r15), %eax
	cltq
	movzwl	(%rax,%r14), %eax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L143:
	movq	15(%r12), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzbl	(%rax,%r15), %eax
	movl	%eax, %esi
.L158:
	subl	$32, %esi
	movzbl	%al, %edx
	cmpb	$94, %sil
	ja	.L151
.L166:
	cmpb	$44, %dl
	je	.L170
	cmpb	$92, %dl
	je	.L171
	movq	0(%r13), %rcx
	leaq	-64(%rbp), %rsi
	movl	$1, %edx
	movb	%al, -64(%rbp)
	leaq	16(%rcx), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L145:
	addl	$16, %edx
	movslq	%edx, %rdx
	movzbl	(%rdx,%r14), %eax
	movl	%eax, %esi
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L164:
	sarq	$32, %rdx
	cmpl	%edx, %eax
	cmovg	%edx, %eax
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L167:
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L171:
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L170:
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L139:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L169:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17919:
	.size	_ZN2v88internal3Log14MessageBuilder12AppendStringENS0_6StringENS_4base8OptionalIiEE, .-_ZN2v88internal3Log14MessageBuilder12AppendStringENS0_6StringENS_4base8OptionalIiEE
	.section	.text._ZN2v88internal3Log14MessageBuilder23AppendSymbolNameDetailsENS0_6StringEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Log14MessageBuilder23AppendSymbolNameDetailsENS0_6StringEb
	.type	_ZN2v88internal3Log14MessageBuilder23AppendSymbolNameDetailsENS0_6StringEb, @function
_ZN2v88internal3Log14MessageBuilder23AppendSymbolNameDetailsENS0_6StringEb:
.LFB17928:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L172
	cmpl	$4096, 11(%rsi)
	movl	$4096, %ebx
	movq	%rdi, %r13
	cmovle	11(%rsi), %ebx
	movq	%rsi, %r12
	testb	%dl, %dl
	jne	.L184
.L174:
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	salq	$32, %rdx
	orq	$1, %rdx
	call	_ZN2v88internal3Log14MessageBuilder12AppendStringENS0_6StringENS_4base8OptionalIiEE
.L172:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L185
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	movq	(%rdi), %rax
	leaq	-57(%rbp), %r15
	movl	$1, %edx
	leaq	16(%rax), %r14
	movq	-1(%rsi), %rax
	movq	%r15, %rsi
	movq	%r14, %rdi
	movzwl	11(%rax), %eax
	andl	$8, %eax
	cmpw	$1, %ax
	sbbl	%eax, %eax
	andl	$-47, %eax
	addl	$97, %eax
	movb	%al, -57(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1(%r12), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$2, %ax
	je	.L186
.L176:
	movq	-1(%r12), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	je	.L187
.L177:
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movb	$58, -57(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	11(%r12), %esi
	movq	%rax, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	movq	%r15, %rsi
	movb	$58, -57(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L187:
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movb	$35, -57(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L186:
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movb	$101, -57(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L176
.L185:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17928:
	.size	_ZN2v88internal3Log14MessageBuilder23AppendSymbolNameDetailsENS0_6StringEb, .-_ZN2v88internal3Log14MessageBuilder23AppendSymbolNameDetailsENS0_6StringEb
	.section	.rodata._ZN2v88internal3Log14MessageBuilder16AppendSymbolNameENS0_6SymbolE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"symbol("
.LC7:
	.string	"\""
.LC8:
	.string	"\" "
.LC9:
	.string	"hash "
.LC10:
	.string	")"
	.section	.text._ZN2v88internal3Log14MessageBuilder16AppendSymbolNameENS0_6SymbolE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Log14MessageBuilder16AppendSymbolNameENS0_6SymbolE
	.type	_ZN2v88internal3Log14MessageBuilder16AppendSymbolNameENS0_6SymbolE, @function
_ZN2v88internal3Log14MessageBuilder16AppendSymbolNameENS0_6SymbolE:
.LFB17927:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$7, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	leaq	.LC6(%rip), %rsi
	subq	$16, %rsp
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16(%r14), %r12
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	15(%rbx), %rax
	testb	$1, %al
	jne	.L189
.L191:
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	15(%rbx), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilder23AppendSymbolNameDetailsENS0_6StringEb
	movl	$2, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L190:
	movl	$5, %edx
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	16(%r14), %rax
	movq	-24(%rax), %rdx
	addq	%r12, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$8, %eax
	movl	%eax, 24(%rdx)
	movl	7(%rbx), %eax
	testb	$1, %al
	jne	.L192
	shrl	$2, %eax
.L193:
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	leaq	.LC10(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	-24(%rax), %rdx
	addq	%rdi, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$2, %eax
	movl	%eax, 24(%rdx)
	movl	$1, %edx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L196
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	leaq	-48(%rbp), %rdi
	movq	%rbx, -48(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L189:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37504(%rdx)
	jne	.L191
	jmp	.L190
.L196:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17927:
	.size	_ZN2v88internal3Log14MessageBuilder16AppendSymbolNameENS0_6SymbolE, .-_ZN2v88internal3Log14MessageBuilder16AppendSymbolNameENS0_6SymbolE
	.section	.rodata._ZN2v88internal3LogC2EPNS0_6LoggerEPKc.str1.1,"aMS",@progbits,1
.LC11:
	.string	"NewArray"
.LC12:
	.string	"v8-version"
	.section	.text._ZN2v88internal3LogC2EPNS0_6LoggerEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3LogC2EPNS0_6LoggerEPKc
	.type	_ZN2v88internal3LogC2EPNS0_6LoggerEPKc, @function
_ZN2v88internal3LogC2EPNS0_6LoggerEPKc:
.LFB17913:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	leaq	360(%rbx), %r12
	subq	$64, %rsp
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$0, (%rbx)
	call	_ZN2v88internal3Log18CreateOutputHandleEPKc
	leaq	16(%rbx), %rdi
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	movq	%rax, %rsi
	cmove	stdout(%rip), %rsi
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movq	%r12, %rdi
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$2048, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L233
.L199:
	movq	%rax, %xmm0
	cmpb	$0, _ZN2v88internal12FLAG_log_allE(%rip)
	movhps	-88(%rbp), %xmm0
	movups	%xmm0, 400(%rbx)
	je	.L200
	movb	$1, _ZN2v88internal12FLAG_log_apiE(%rip)
	movb	$1, _ZN2v88internal13FLAG_log_codeE(%rip)
	movb	$1, _ZN2v88internal16FLAG_log_suspectE(%rip)
	movb	$1, _ZN2v88internal16FLAG_log_handlesE(%rip)
	movb	$1, _ZN2v88internal30FLAG_log_internal_timer_eventsE(%rip)
	movb	$1, _ZN2v88internal24FLAG_log_function_eventsE(%rip)
.L200:
	cmpb	$0, _ZN2v88internal9FLAG_profE(%rip)
	jne	.L234
.L201:
	cmpq	$0, 8(%rbx)
	je	.L197
	movq	%r12, %xmm1
	movq	%rbx, %xmm0
	leaq	-64(%rbp), %r13
	movq	%r12, %rdi
	punpcklqdq	%xmm1, %xmm0
	leaq	.LC12(%rip), %rbx
	leaq	-65(%rbp), %r12
	movaps	%xmm0, -64(%rbp)
	leaq	10(%rbx), %r14
	call	_ZN2v84base5Mutex4LockEv@PLT
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L238:
	cmpb	$44, %dl
	je	.L235
	cmpb	$92, %dl
	je	.L236
	movq	-64(%rbp), %rax
	movb	%dl, -65(%rbp)
	movq	%r12, %rsi
	movl	$1, %edx
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L206:
	addq	$1, %rbx
	cmpq	%rbx, %r14
	je	.L237
.L209:
	movzbl	(%rbx), %edx
	leal	-32(%rdx), %eax
	cmpb	$94, %al
	jbe	.L238
	cmpb	$10, %dl
	je	.L239
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	addq	$1, %rbx
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	cmpq	%rbx, %r14
	jne	.L209
.L237:
	movq	-64(%rbp), %rax
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -65(%rbp)
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-64(%rbp), %rax
	movl	_ZN2v88internal7Version6major_E(%rip), %esi
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	movq	-64(%rbp), %rax
	movq	%r12, %rsi
	movb	$44, -65(%rbp)
	movl	$1, %edx
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-64(%rbp), %rax
	movl	_ZN2v88internal7Version6minor_E(%rip), %esi
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	movq	-64(%rbp), %rax
	movq	%r12, %rsi
	movb	$44, -65(%rbp)
	movl	$1, %edx
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-64(%rbp), %rax
	movl	_ZN2v88internal7Version6build_E(%rip), %esi
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	movq	-64(%rbp), %rax
	movq	%r12, %rsi
	movb	$44, -65(%rbp)
	movl	$1, %edx
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-64(%rbp), %rax
	movl	_ZN2v88internal7Version6patch_E(%rip), %esi
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	movq	_ZN2v88internal7Version9embedder_E(%rip), %rax
	cmpb	$0, (%rax)
	jne	.L210
.L213:
	movq	-64(%rbp), %rax
	movq	%r12, %rsi
	movl	$1, %edx
	movb	$44, -65(%rbp)
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-64(%rbp), %rax
	movzbl	_ZN2v88internal7Version10candidate_E(%rip), %esi
	leaq	16(%rax), %rdi
	call	_ZNSo9_M_insertIbEERSoT_@PLT
	movq	-64(%rbp), %rax
	leaq	16(%rax), %r13
	movq	16(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %r12
	testq	%r12, %r12
	je	.L240
	cmpb	$0, 56(%r12)
	je	.L220
	movsbl	67(%r12), %esi
.L221:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L197:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L241
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L236:
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L235:
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L234:
	movb	$1, _ZN2v88internal13FLAG_log_codeE(%rip)
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L220:
	movq	%r12, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r12), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L221
	movq	%r12, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L210:
	movq	-64(%rbp), %rax
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -65(%rbp)
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	_ZN2v88internal7Version9embedder_E(%rip), %r14
	testq	%r14, %r14
	je	.L213
	movq	%r14, %rdi
	call	strlen@PLT
	testq	%rax, %rax
	je	.L213
	leaq	(%r14,%rax), %rbx
	leaq	-64(%rbp), %r13
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L244:
	cmpb	$44, %dl
	je	.L242
	cmpb	$92, %dl
	je	.L243
	movq	-64(%rbp), %rax
	movb	%dl, -65(%rbp)
	movq	%r12, %rsi
	movl	$1, %edx
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L216:
	addq	$1, %r14
	cmpq	%rbx, %r14
	je	.L213
.L219:
	movzbl	(%r14), %edx
	leal	-32(%rdx), %eax
	cmpb	$94, %al
	jbe	.L244
	cmpb	$10, %dl
	je	.L245
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L245:
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L243:
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L242:
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L216
.L233:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$2048, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	jne	.L199
	leaq	.LC11(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L241:
	call	__stack_chk_fail@PLT
.L240:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE17913:
	.size	_ZN2v88internal3LogC2EPNS0_6LoggerEPKc, .-_ZN2v88internal3LogC2EPNS0_6LoggerEPKc
	.globl	_ZN2v88internal3LogC1EPNS0_6LoggerEPKc
	.set	_ZN2v88internal3LogC1EPNS0_6LoggerEPKc,_ZN2v88internal3LogC2EPNS0_6LoggerEPKc
	.section	.text._ZN2v88internal3Log14MessageBuilder18AppendRawCharacterEc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Log14MessageBuilder18AppendRawCharacterEc
	.type	_ZN2v88internal3Log14MessageBuilder18AppendRawCharacterEc, @function
_ZN2v88internal3Log14MessageBuilder18AppendRawCharacterEc:
.LFB17931:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movb	%sil, -9(%rbp)
	leaq	-9(%rbp), %rsi
	addq	$16, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L249
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L249:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17931:
	.size	_ZN2v88internal3Log14MessageBuilder18AppendRawCharacterEc, .-_ZN2v88internal3Log14MessageBuilder18AppendRawCharacterEc
	.section	.text._ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv
	.type	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv, @function
_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv:
.LFB17932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	(%rdi), %rax
	leaq	16(%rax), %r13
	movq	16(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %r12
	testq	%r12, %r12
	je	.L256
	cmpb	$0, 56(%r12)
	je	.L252
	movsbl	67(%r12), %esi
.L253:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	popq	%r12
	popq	%r13
	movq	%rax, %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSo5flushEv@PLT
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r12), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L253
	movq	%r12, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L253
.L256:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE17932:
	.size	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv, .-_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv
	.section	.text._ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_
	.type	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_, @function
_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_:
.LFB17933:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L258
	movq	%rsi, %rdi
	movq	%rsi, %rbx
	call	strlen@PLT
	testq	%rax, %rax
	je	.L258
	leaq	(%rbx,%rax), %r12
	leaq	-41(%rbp), %r13
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L276:
	cmpb	$44, %dl
	je	.L274
	cmpb	$92, %dl
	je	.L275
	movq	(%r14), %rax
	movb	%dl, -41(%rbp)
	movq	%r13, %rsi
	movl	$1, %edx
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L261:
	addq	$1, %rbx
	cmpq	%rbx, %r12
	je	.L258
.L264:
	movzbl	(%rbx), %edx
	leal	-32(%rdx), %eax
	cmpb	$94, %al
	jbe	.L276
	cmpb	$10, %dl
	je	.L277
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	addq	$1, %rbx
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	cmpq	%rbx, %r12
	jne	.L264
.L258:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L278
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L277:
	.cfi_restore_state
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L275:
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L274:
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L261
.L278:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17933:
	.size	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_, .-_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_
	.section	.rodata._ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_.str1.1,"aMS",@progbits,1
.LC13:
	.string	"0x"
	.section	.text._ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_
	.type	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_, @function
_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_:
.LFB17934:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	leaq	.LC13(%rip), %rsi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rbx
	leaq	16(%rbx), %r13
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	16(%rbx), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	-24(%rax), %rdx
	addq	%r13, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$8, %eax
	movl	%eax, 24(%rdx)
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	movq	(%rax), %rdx
	addq	-24(%rdx), %rax
	movl	24(%rax), %edx
	andl	$-75, %edx
	orl	$2, %edx
	movl	%edx, 24(%rax)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17934:
	.size	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_, .-_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_
	.section	.text._ZN2v88internal3Log14MessageBuilderlsIcEERS2_T_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Log14MessageBuilderlsIcEERS2_T_
	.type	_ZN2v88internal3Log14MessageBuilderlsIcEERS2_T_, @function
_ZN2v88internal3Log14MessageBuilderlsIcEERS2_T_:
.LFB17935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leal	-32(%rsi), %eax
	cmpb	$94, %al
	ja	.L282
	cmpb	$44, %sil
	je	.L289
	cmpb	$92, %sil
	je	.L290
	movq	(%rdi), %rax
	movb	%sil, -25(%rbp)
	movl	$1, %edx
	leaq	-25(%rbp), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L284:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L291
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	cmpb	$10, %sil
	je	.L292
	movzbl	%sil, %edx
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L292:
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L289:
	leaq	.LC0(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L290:
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilder21AppendRawFormatStringEPKcz
	jmp	.L284
.L291:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17935:
	.size	_ZN2v88internal3Log14MessageBuilderlsIcEERS2_T_, .-_ZN2v88internal3Log14MessageBuilderlsIcEERS2_T_
	.section	.text._ZN2v88internal3Log14MessageBuilderlsINS0_6StringEEERS2_T_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Log14MessageBuilderlsINS0_6StringEEERS2_T_
	.type	_ZN2v88internal3Log14MessageBuilderlsINS0_6StringEEERS2_T_, @function
_ZN2v88internal3Log14MessageBuilderlsINS0_6StringEEERS2_T_:
.LFB17936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal3Log14MessageBuilder12AppendStringENS0_6StringENS_4base8OptionalIiEE
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17936:
	.size	_ZN2v88internal3Log14MessageBuilderlsINS0_6StringEEERS2_T_, .-_ZN2v88internal3Log14MessageBuilderlsINS0_6StringEEERS2_T_
	.section	.text._ZN2v88internal3Log14MessageBuilderlsINS0_6SymbolEEERS2_T_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Log14MessageBuilderlsINS0_6SymbolEEERS2_T_
	.type	_ZN2v88internal3Log14MessageBuilderlsINS0_6SymbolEEERS2_T_, @function
_ZN2v88internal3Log14MessageBuilderlsINS0_6SymbolEEERS2_T_:
.LFB17937:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal3Log14MessageBuilder16AppendSymbolNameENS0_6SymbolE
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17937:
	.size	_ZN2v88internal3Log14MessageBuilderlsINS0_6SymbolEEERS2_T_, .-_ZN2v88internal3Log14MessageBuilderlsINS0_6SymbolEEERS2_T_
	.section	.text._ZN2v88internal3Log14MessageBuilderlsINS0_4NameEEERS2_T_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Log14MessageBuilderlsINS0_4NameEEERS2_T_
	.type	_ZN2v88internal3Log14MessageBuilderlsINS0_4NameEEERS2_T_, @function
_ZN2v88internal3Log14MessageBuilderlsINS0_4NameEEERS2_T_:
.LFB17938:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L298
	xorl	%edx, %edx
	call	_ZN2v88internal3Log14MessageBuilder12AppendStringENS0_6StringENS_4base8OptionalIiEE
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	call	_ZN2v88internal3Log14MessageBuilder16AppendSymbolNameENS0_6SymbolE
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17938:
	.size	_ZN2v88internal3Log14MessageBuilderlsINS0_4NameEEERS2_T_, .-_ZN2v88internal3Log14MessageBuilderlsINS0_4NameEEERS2_T_
	.section	.text._ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_
	.type	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_, @function
_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_:
.LFB17939:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-25(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movb	$44, -25(%rbp)
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L304
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L304:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17939:
	.size	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_, .-_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal3Log19kLogToTemporaryFileE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal3Log19kLogToTemporaryFileE, @function
_GLOBAL__sub_I__ZN2v88internal3Log19kLogToTemporaryFileE:
.LFB21675:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21675:
	.size	_GLOBAL__sub_I__ZN2v88internal3Log19kLogToTemporaryFileE, .-_GLOBAL__sub_I__ZN2v88internal3Log19kLogToTemporaryFileE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal3Log19kLogToTemporaryFileE
	.globl	_ZN2v88internal3Log13kLogToConsoleE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC14:
	.string	"-"
	.section	.data.rel.ro.local._ZN2v88internal3Log13kLogToConsoleE,"aw"
	.align 8
	.type	_ZN2v88internal3Log13kLogToConsoleE, @object
	.size	_ZN2v88internal3Log13kLogToConsoleE, 8
_ZN2v88internal3Log13kLogToConsoleE:
	.quad	.LC14
	.globl	_ZN2v88internal3Log19kLogToTemporaryFileE
	.section	.rodata.str1.1
.LC15:
	.string	"&"
	.section	.data.rel.ro.local._ZN2v88internal3Log19kLogToTemporaryFileE,"aw"
	.align 8
	.type	_ZN2v88internal3Log19kLogToTemporaryFileE, @object
	.size	_ZN2v88internal3Log19kLogToTemporaryFileE, 8
_ZN2v88internal3Log19kLogToTemporaryFileE:
	.quad	.LC15
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
