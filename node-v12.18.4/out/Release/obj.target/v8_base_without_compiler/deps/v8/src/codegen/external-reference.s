	.file	"external-reference.cc"
	.text
	.section	.text._ZN2v88internalL28atomic_pair_compare_exchangeEliiii,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28atomic_pair_compare_exchangeEliiii, @function
_ZN2v88internalL28atomic_pair_compare_exchangeEliiii:
.LFB25400:
	.cfi_startproc
	endbr64
	salq	$32, %rdx
	movl	%esi, %eax
	salq	$32, %r8
	movl	%ecx, %ecx
	orq	%rdx, %rax
	orq	%rcx, %r8
	lock cmpxchgq	%r8, (%rdi)
	ret
	.cfi_endproc
.LFE25400:
	.size	_ZN2v88internalL28atomic_pair_compare_exchangeEliiii, .-_ZN2v88internalL28atomic_pair_compare_exchangeEliiii
	.section	.text._ZN2v88internal15SearchStringRawIKhS2_EElPNS0_7IsolateEPKT_iPKT0_ii,"axG",@progbits,_ZN2v88internal15SearchStringRawIKhS2_EElPNS0_7IsolateEPKT_iPKT0_ii,comdat
	.p2align 4
	.weak	_ZN2v88internal15SearchStringRawIKhS2_EElPNS0_7IsolateEPKT_iPKT0_ii
	.type	_ZN2v88internal15SearchStringRawIKhS2_EElPNS0_7IsolateEPKT_iPKT0_ii, @function
_ZN2v88internal15SearchStringRawIKhS2_EElPNS0_7IsolateEPKT_iPKT0_ii:
.LFB29183:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movq	%rdi, %xmm0
	movl	$0, %edi
	movq	%r10, %xmm1
	movl	%r9d, %ecx
	movslq	%edx, %rdx
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movslq	%r8d, %rax
	movaps	%xmm0, -48(%rbp)
	movq	%rax, -32(%rbp)
	leal	-250(%r8), %eax
	testl	%eax, %eax
	cmovs	%edi, %eax
	movl	%eax, -16(%rbp)
	cmpl	$6, %r8d
	jg	.L4
	cmpl	$1, %r8d
	je	.L9
	leaq	_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -24(%rbp)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L4:
	leaq	_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -24(%rbp)
.L6:
	leaq	-48(%rbp), %rdi
	call	*%rax
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	cltq
	jne	.L10
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	leaq	_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -24(%rbp)
	jmp	.L6
.L10:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29183:
	.size	_ZN2v88internal15SearchStringRawIKhS2_EElPNS0_7IsolateEPKT_iPKT0_ii, .-_ZN2v88internal15SearchStringRawIKhS2_EElPNS0_7IsolateEPKT_iPKT0_ii
	.section	.text._ZN2v88internal15SearchStringRawIKtKhEElPNS0_7IsolateEPKT_iPKT0_ii,"axG",@progbits,_ZN2v88internal15SearchStringRawIKtKhEElPNS0_7IsolateEPKT_iPKT0_ii,comdat
	.p2align 4
	.weak	_ZN2v88internal15SearchStringRawIKtKhEElPNS0_7IsolateEPKT_iPKT0_ii
	.type	_ZN2v88internal15SearchStringRawIKtKhEElPNS0_7IsolateEPKT_iPKT0_ii, @function
_ZN2v88internal15SearchStringRawIKtKhEElPNS0_7IsolateEPKT_iPKT0_ii:
.LFB29185:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movq	%rdi, %xmm0
	movl	$0, %edi
	movq	%r10, %xmm1
	movl	%r9d, %ecx
	movslq	%edx, %rdx
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movslq	%r8d, %rax
	movaps	%xmm0, -48(%rbp)
	movq	%rax, -32(%rbp)
	leal	-250(%r8), %eax
	testl	%eax, %eax
	cmovs	%edi, %eax
	movl	%eax, -16(%rbp)
	cmpl	$6, %r8d
	jg	.L12
	cmpl	$1, %r8d
	je	.L17
	leaq	_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -24(%rbp)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L12:
	leaq	_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -24(%rbp)
.L14:
	leaq	-48(%rbp), %rdi
	call	*%rax
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	cltq
	jne	.L18
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	leaq	_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -24(%rbp)
	jmp	.L14
.L18:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29185:
	.size	_ZN2v88internal15SearchStringRawIKtKhEElPNS0_7IsolateEPKT_iPKT0_ii, .-_ZN2v88internal15SearchStringRawIKtKhEElPNS0_7IsolateEPKT_iPKT0_ii
	.section	.text._ZN2v88internal15SearchStringRawIKtS2_EElPNS0_7IsolateEPKT_iPKT0_ii,"axG",@progbits,_ZN2v88internal15SearchStringRawIKtS2_EElPNS0_7IsolateEPKT_iPKT0_ii,comdat
	.p2align 4
	.weak	_ZN2v88internal15SearchStringRawIKtS2_EElPNS0_7IsolateEPKT_iPKT0_ii
	.type	_ZN2v88internal15SearchStringRawIKtS2_EElPNS0_7IsolateEPKT_iPKT0_ii, @function
_ZN2v88internal15SearchStringRawIKtS2_EElPNS0_7IsolateEPKT_iPKT0_ii:
.LFB29186:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movq	%rdi, %xmm0
	movl	$0, %edi
	movq	%r10, %xmm1
	movl	%r9d, %ecx
	movslq	%edx, %rdx
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movslq	%r8d, %rax
	movaps	%xmm0, -48(%rbp)
	movq	%rax, -32(%rbp)
	leal	-250(%r8), %eax
	testl	%eax, %eax
	cmovs	%edi, %eax
	movl	%eax, -16(%rbp)
	cmpl	$6, %r8d
	jg	.L20
	cmpl	$1, %r8d
	je	.L25
	leaq	_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -24(%rbp)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L20:
	leaq	_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -24(%rbp)
.L22:
	leaq	-48(%rbp), %rdi
	call	*%rax
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	cltq
	jne	.L26
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	leaq	_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -24(%rbp)
	jmp	.L22
.L26:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29186:
	.size	_ZN2v88internal15SearchStringRawIKtS2_EElPNS0_7IsolateEPKT_iPKT0_ii, .-_ZN2v88internal15SearchStringRawIKtS2_EElPNS0_7IsolateEPKT_iPKT0_ii
	.section	.text._ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi,comdat
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi:
.LFB30859:
	.cfi_startproc
	endbr64
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE30859:
	.size	_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi:
.LFB31129:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r11
	movq	(%rdi), %r14
	movq	8(%rdi), %r13
	movslq	32(%rdi), %r8
	leal	-1(%r11), %edi
	subl	%r11d, %edx
	leaq	42372(%r14), %r10
	addq	$43396, %r14
	movslq	%edi, %r9
	movzbl	0(%r13,%r9), %r9d
	cmpl	%edx, %ecx
	jg	.L41
	movl	%r11d, %ecx
	movzbl	%r9b, %r11d
	movq	%r8, %r15
	leaq	(%r10,%r11,4), %rbx
	subl	$2, %ecx
	movq	%rbx, -56(%rbp)
	movslq	%ecx, %rbx
	movl	$1, %ecx
	subq	%r8, %rcx
	movq	%rbx, -48(%rbp)
	movq	%rcx, -64(%rbp)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L45:
	movl	%edi, %ebx
	subl	(%r10,%rcx,4), %ebx
	addl	%ebx, %eax
	cmpl	%eax, %edx
	jl	.L41
.L32:
	leal	(%rdi,%rax), %ecx
	movslq	%ecx, %rcx
	movzbl	(%rsi,%rcx), %ecx
	cmpb	%r9b, %cl
	jne	.L45
	testl	%edi, %edi
	js	.L28
	movslq	%eax, %r12
	movq	-48(%rbp), %rcx
	addq	%rsi, %r12
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L46:
	movq	%r8, %rcx
.L33:
	movl	%ecx, %ebx
	testl	%ecx, %ecx
	js	.L28
	movzbl	(%rcx,%r12), %r11d
	leaq	-1(%rcx), %r8
	cmpb	%r11b, 0(%r13,%rcx)
	je	.L46
	cmpl	%ebx, %r15d
	jle	.L47
	movq	-56(%rbp), %rbx
	movl	%edi, %ecx
	subl	(%rbx), %ecx
	addl	%ecx, %eax
	cmpl	%eax, %edx
	jge	.L32
	.p2align 4,,10
	.p2align 3
.L41:
	movl	$-1, %eax
.L28:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	addq	-64(%rbp), %rcx
	subl	(%r10,%r11,4), %ebx
	cmpl	%ebx, (%r14,%rcx,4)
	cmovge	(%r14,%rcx,4), %ebx
	addl	%ebx, %eax
	cmpl	%eax, %edx
	jge	.L32
	jmp	.L41
	.cfi_endproc
.LFE31129:
	.size	_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi:
.LFB31134:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r11
	movq	8(%rdi), %r14
	movslq	32(%rdi), %r10
	movq	(%rdi), %rdi
	subl	%r11d, %edx
	leaq	42372(%rdi), %r9
	addq	$43396, %rdi
	movq	%rdi, -56(%rbp)
	leal	-1(%r11), %edi
	movslq	%edi, %r8
	movzwl	(%r14,%r8,2), %r8d
	cmpl	%edx, %ecx
	jg	.L61
	movl	%r11d, %ecx
	movzwl	%r8w, %r11d
	movq	%r10, %r15
	leaq	(%r9,%r11,4), %rbx
	subl	$2, %ecx
	movq	%rbx, -64(%rbp)
	movslq	%ecx, %rbx
	movl	$1, %ecx
	subq	%r10, %rcx
	movq	%rbx, -48(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L65:
	movl	%edi, %ebx
	subl	(%r9,%r10,4), %ebx
	addl	%ebx, %eax
	cmpl	%eax, %edx
	jl	.L61
.L52:
	leal	(%rdi,%rax), %ecx
	movslq	%ecx, %rcx
	movzbl	(%rsi,%rcx), %r10d
	cmpw	%r10w, %r8w
	jne	.L65
	testl	%edi, %edi
	js	.L48
	movslq	%eax, %r13
	movq	-48(%rbp), %rcx
	addq	%rsi, %r13
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%rbx, %rcx
.L53:
	movl	%ecx, %r12d
	testl	%ecx, %ecx
	js	.L48
	movzbl	0(%r13,%rcx), %r11d
	leaq	-1(%rcx), %rbx
	movq	%r11, %r10
	cmpw	%r11w, (%r14,%rcx,2)
	je	.L66
	cmpl	%r12d, %r15d
	jle	.L67
	movq	-64(%rbp), %rbx
	movl	%edi, %ecx
	subl	(%rbx), %ecx
	addl	%ecx, %eax
	cmpl	%eax, %edx
	jge	.L52
	.p2align 4,,10
	.p2align 3
.L61:
	movl	$-1, %eax
.L48:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movq	-56(%rbp), %rbx
	addq	-72(%rbp), %rcx
	subl	(%r9,%r10,4), %r12d
	cmpl	%r12d, (%rbx,%rcx,4)
	cmovge	(%rbx,%rcx,4), %r12d
	addl	%r12d, %eax
	cmpl	%eax, %edx
	jge	.L52
	jmp	.L61
	.cfi_endproc
.LFE31134:
	.size	_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi:
.LFB31140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rcx
	movq	16(%rdi), %r10
	movq	8(%rdi), %r15
	movl	32(%rdi), %ebx
	leaq	43396(%rcx), %rdi
	subl	%r10d, %edx
	movl	%r10d, -84(%rbp)
	movq	%rdi, -64(%rbp)
	leal	-1(%r10), %edi
	movslq	%edi, %r8
	movl	%ebx, -44(%rbp)
	movzbl	(%r15,%r8), %r8d
	cmpl	%edx, %eax
	jg	.L85
	movl	%r10d, %r14d
	leaq	42372(%rcx), %r9
	movzbl	%r8b, %r10d
	movslq	%ebx, %rcx
	leaq	(%r9,%r10,4), %rbx
	leal	-2(%r14), %r10d
	movq	%rbx, -72(%rbp)
	movslq	%r10d, %rbx
	movl	$1, %r10d
	subq	%rcx, %r10
	movq	%rbx, -56(%rbp)
	movq	%r10, -80(%rbp)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L90:
	cmpw	$255, %cx
	ja	.L72
	movl	%edi, %ebx
	subl	(%r9,%rcx,4), %ebx
	addl	%ebx, %eax
	cmpl	%eax, %edx
	jl	.L85
.L74:
	leal	(%rdi,%rax), %ecx
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %ecx
	cmpw	%r8w, %cx
	jne	.L90
	testl	%edi, %edi
	js	.L68
	movslq	%eax, %r10
	movl	%edi, -48(%rbp)
	movq	-56(%rbp), %rcx
	leaq	(%rsi,%r10,2), %r12
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%rbx, %rcx
.L75:
	leal	1(%rcx), %r14d
	movl	%ecx, %r13d
	testl	%ecx, %ecx
	js	.L68
	movzwl	(%r12,%rcx,2), %edi
	movzbl	(%r15,%rcx), %r11d
	leaq	-1(%rcx), %rbx
	movq	%rdi, %r10
	cmpl	%edi, %r11d
	je	.L91
	movl	-48(%rbp), %edi
	cmpl	%r13d, -44(%rbp)
	jg	.L80
	movq	-64(%rbp), %rbx
	addq	-80(%rbp), %rcx
	movl	(%rbx,%rcx,4), %ecx
	cmpw	$255, %r10w
	ja	.L79
	movl	%r13d, %r14d
	subl	(%r9,%r10,4), %r14d
.L79:
	cmpl	%r14d, %ecx
	cmovl	%r14d, %ecx
	addl	%ecx, %eax
.L89:
	cmpl	%edx, %eax
	jle	.L74
	.p2align 4,,10
	.p2align 3
.L85:
	movl	$-1, %eax
.L68:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movq	-72(%rbp), %rbx
	movl	%edi, %ecx
	subl	(%rbx), %ecx
	addl	%ecx, %eax
	jmp	.L89
.L72:
	addl	-84(%rbp), %eax
	jmp	.L89
	.cfi_endproc
.LFE31140:
	.size	_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi:
.LFB31145:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r11
	movq	(%rdi), %r14
	movq	8(%rdi), %r12
	movslq	32(%rdi), %r8
	leal	-1(%r11), %edi
	subl	%r11d, %edx
	leaq	42372(%r14), %r10
	addq	$43396, %r14
	movslq	%edi, %r9
	movzwl	(%r12,%r9,2), %r9d
	cmpl	%edx, %ecx
	jg	.L105
	movl	%r11d, %ecx
	movzbl	%r9b, %r11d
	movq	%r8, %r15
	leaq	(%r10,%r11,4), %rbx
	subl	$2, %ecx
	movq	%rbx, -56(%rbp)
	movslq	%ecx, %rbx
	movl	$1, %ecx
	subq	%r8, %rcx
	movq	%rbx, -48(%rbp)
	movq	%rcx, -64(%rbp)
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L109:
	movzbl	%cl, %ecx
	movl	%edi, %ebx
	subl	(%r10,%rcx,4), %ebx
	addl	%ebx, %eax
	cmpl	%eax, %edx
	jl	.L105
.L96:
	leal	(%rdi,%rax), %ecx
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %ecx
	cmpw	%r9w, %cx
	jne	.L109
	testl	%edi, %edi
	js	.L92
	movslq	%eax, %r8
	movq	-48(%rbp), %rcx
	leaq	(%rsi,%r8,2), %r13
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L110:
	movq	%r8, %rcx
.L97:
	movl	%ecx, %ebx
	testl	%ecx, %ecx
	js	.L92
	leaq	-1(%rcx), %r8
	movzwl	2(%r13,%r8,2), %r11d
	cmpw	%r11w, (%r12,%rcx,2)
	je	.L110
	cmpl	%ebx, %r15d
	jle	.L111
	movq	-56(%rbp), %rbx
	movl	%edi, %ecx
	subl	(%rbx), %ecx
	addl	%ecx, %eax
	cmpl	%eax, %edx
	jge	.L96
	.p2align 4,,10
	.p2align 3
.L105:
	movl	$-1, %eax
.L92:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	addq	-64(%rbp), %rcx
	movzbl	%r11b, %r11d
	subl	(%r10,%r11,4), %ebx
	cmpl	%ebx, (%r14,%rcx,4)
	cmovge	(%r14,%rcx,4), %ebx
	addl	%ebx, %eax
	cmpl	%eax, %edx
	jge	.L96
	jmp	.L105
	.cfi_endproc
.LFE31145:
	.size	_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal11libc_memcpyEPvPKvm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal11libc_memcpyEPvPKvm
	.type	_ZN2v88internal11libc_memcpyEPvPKvm, @function
_ZN2v88internal11libc_memcpyEPvPKvm:
.LFB25307:
	.cfi_startproc
	endbr64
	jmp	memcpy@PLT
	.cfi_endproc
.LFE25307:
	.size	_ZN2v88internal11libc_memcpyEPvPKvm, .-_ZN2v88internal11libc_memcpyEPvPKvm
	.section	.text._ZN2v88internalL16f64_acos_wrapperEm,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL16f64_acos_wrapperEm, @function
_ZN2v88internalL16f64_acos_wrapperEm:
.LFB25209:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movsd	(%rdi), %xmm0
	call	_ZN2v84base7ieee7544acosEd@PLT
	movq	%xmm0, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25209:
	.size	_ZN2v88internalL16f64_acos_wrapperEm, .-_ZN2v88internalL16f64_acos_wrapperEm
	.section	.text._ZN2v88internalL16f64_asin_wrapperEm,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL16f64_asin_wrapperEm, @function
_ZN2v88internalL16f64_asin_wrapperEm:
.LFB25211:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movsd	(%rdi), %xmm0
	call	_ZN2v84base7ieee7544asinEd@PLT
	movq	%xmm0, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25211:
	.size	_ZN2v88internalL16f64_asin_wrapperEm, .-_ZN2v88internalL16f64_asin_wrapperEm
	.section	.text._ZN2v88internalL15f64_mod_wrapperEm,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL15f64_mod_wrapperEm, @function
_ZN2v88internalL15f64_mod_wrapperEm:
.LFB25214:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movsd	8(%rdi), %xmm1
	movsd	(%rdi), %xmm0
	call	fmod@PLT
	movq	%xmm0, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25214:
	.size	_ZN2v88internalL15f64_mod_wrapperEm, .-_ZN2v88internalL15f64_mod_wrapperEm
	.section	.text._ZN2v88internal20modulo_double_doubleEdd,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20modulo_double_doubleEdd
	.type	_ZN2v88internal20modulo_double_doubleEdd, @function
_ZN2v88internal20modulo_double_doubleEdd:
.LFB25373:
	.cfi_startproc
	endbr64
	jmp	fmod@PLT
	.cfi_endproc
.LFE25373:
	.size	_ZN2v88internal20modulo_double_doubleEdd, .-_ZN2v88internal20modulo_double_doubleEdd
	.section	.text._ZN2v88internal11libc_memchrEPvim,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal11libc_memchrEPvim
	.type	_ZN2v88internal11libc_memchrEPvim, @function
_ZN2v88internal11libc_memchrEPvim:
.LFB25302:
	.cfi_startproc
	endbr64
	jmp	memchr@PLT
	.cfi_endproc
.LFE25302:
	.size	_ZN2v88internal11libc_memchrEPvim, .-_ZN2v88internal11libc_memchrEPvim
	.section	.text._ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi:
.LFB30856:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	subl	16(%rdi), %r13d
	leal	1(%r13), %ebx
	movzbl	(%rax), %r15d
	movl	%r15d, %r14d
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L127:
	subq	%r12, %rax
	movslq	%eax, %rdx
	movl	%eax, %r8d
	cmpb	(%r12,%rdx), %r14b
	je	.L121
	leal	1(%rax), %ecx
	cmpl	%eax, %r13d
	jle	.L124
.L123:
	movl	%ebx, %edx
	movl	%r15d, %esi
	subl	%ecx, %edx
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	leaq	(%r12,%rcx), %rdi
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L127
.L124:
	movl	$-1, %r8d
.L121:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE30856:
	.size	_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi:
.LFB30860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movzwl	(%rax), %r15d
	cmpw	$255, %r15w
	ja	.L132
	movl	%edx, %r14d
	subl	16(%rdi), %r14d
	movq	%rsi, %r13
	movl	%r15d, %ebx
	leal	1(%r14), %r12d
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L136:
	subq	%r13, %rax
	movslq	%eax, %rdx
	movl	%eax, %r8d
	cmpb	0(%r13,%rdx), %bl
	je	.L128
	leal	1(%rax), %ecx
	cmpl	%eax, %r14d
	jle	.L132
.L131:
	movl	%r12d, %edx
	movl	%r15d, %esi
	subl	%ecx, %edx
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	leaq	0(%r13,%rcx), %rdi
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L136
.L132:
	movl	$-1, %r8d
.L128:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE30860:
	.size	_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi:
.LFB30864:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	subl	16(%rdi), %r14d
	leal	1(%r14), %ebx
	movzbl	(%rax), %r15d
	movl	%r15d, %r13d
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L143:
	andq	$-2, %rax
	subq	%r12, %rax
	sarq	%rax
	movslq	%eax, %rdx
	movl	%eax, %r8d
	cmpw	(%r12,%rdx,2), %r13w
	je	.L137
	leal	1(%rax), %ecx
	cmpl	%eax, %r14d
	jle	.L140
.L139:
	movl	%ebx, %edx
	movl	%r15d, %esi
	subl	%ecx, %edx
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	leaq	(%r12,%rcx,2), %rdi
	addq	%rdx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L143
.L140:
	movl	$-1, %r8d
.L137:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE30864:
	.size	_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi:
.LFB30868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	subl	16(%rdi), %r14d
	leal	1(%r14), %ebx
	movzwl	(%rax), %r13d
	movl	%r13d, %eax
	movzbl	%ah, %eax
	cmpb	%al, %r13b
	movl	%eax, %r15d
	cmova	%r13d, %r15d
	movzbl	%r15b, %r15d
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L150:
	andq	$-2, %rax
	subq	%r12, %rax
	sarq	%rax
	movslq	%eax, %rdx
	movl	%eax, %r8d
	cmpw	(%r12,%rdx,2), %r13w
	je	.L144
	leal	1(%rax), %ecx
	cmpl	%eax, %r14d
	jle	.L147
.L146:
	movl	%ebx, %edx
	movl	%r15d, %esi
	subl	%ecx, %edx
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	leaq	(%r12,%rcx,2), %rdi
	addq	%rdx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L150
.L147:
	movl	$-1, %r8d
.L144:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE30868:
	.size	_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12libc_memmoveEPvPKvm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal12libc_memmoveEPvPKvm
	.type	_ZN2v88internal12libc_memmoveEPvPKvm, @function
_ZN2v88internal12libc_memmoveEPvPKvm:
.LFB25313:
	.cfi_startproc
	endbr64
	jmp	memmove@PLT
	.cfi_endproc
.LFE25313:
	.size	_ZN2v88internal12libc_memmoveEPvPKvm, .-_ZN2v88internal12libc_memmoveEPvPKvm
	.section	.text._ZN2v88internal11libc_memsetEPvim,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal11libc_memsetEPvim
	.type	_ZN2v88internal11libc_memsetEPvim, @function
_ZN2v88internal11libc_memsetEPvim:
.LFB25315:
	.cfi_startproc
	endbr64
	jmp	memset@PLT
	.cfi_endproc
.LFE25315:
	.size	_ZN2v88internal11libc_memsetEPvim, .-_ZN2v88internal11libc_memsetEPvim
	.section	.text._ZN2v88internal15GetOrCreateHashEPNS0_7IsolateEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal15GetOrCreateHashEPNS0_7IsolateEm
	.type	_ZN2v88internal15GetOrCreateHashEPNS0_7IsolateEm, @function
_ZN2v88internal15GetOrCreateHashEPNS0_7IsolateEm:
.LFB25332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-16(%rbp), %rdi
	movq	%rsi, -16(%rbp)
	movq	%r8, %rsi
	call	_ZN2v88internal6Object15GetOrCreateHashEPNS0_7IsolateE@PLT
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L156
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L156:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25332:
	.size	_ZN2v88internal15GetOrCreateHashEPNS0_7IsolateEm, .-_ZN2v88internal15GetOrCreateHashEPNS0_7IsolateEm
	.section	.text._ZN2v88internalL28JSReceiverCreateIdentityHashEPNS0_7IsolateEm,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28JSReceiverCreateIdentityHashEPNS0_7IsolateEm, @function
_ZN2v88internalL28JSReceiverCreateIdentityHashEPNS0_7IsolateEm:
.LFB25334:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal10JSReceiver18CreateIdentityHashEPNS0_7IsolateES1_@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25334:
	.size	_ZN2v88internalL28JSReceiverCreateIdentityHashEPNS0_7IsolateEm, .-_ZN2v88internalL28JSReceiverCreateIdentityHashEPNS0_7IsolateEm
	.section	.text._ZN2v88internalL27LexicographicCompareWrapperEPNS0_7IsolateEmm,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL27LexicographicCompareWrapperEPNS0_7IsolateEmm, @function
_ZN2v88internalL27LexicographicCompareWrapperEPNS0_7IsolateEmm:
.LFB25348:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal3Smi20LexicographicCompareEPNS0_7IsolateES1_S1_@PLT
	.cfi_endproc
.LFE25348:
	.size	_ZN2v88internalL27LexicographicCompareWrapperEPNS0_7IsolateEmm, .-_ZN2v88internalL27LexicographicCompareWrapperEPNS0_7IsolateEmm
	.section	.text._ZN2v88internalL32InvalidatePrototypeChainsWrapperEm,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32InvalidatePrototypeChainsWrapperEm, @function
_ZN2v88internalL32InvalidatePrototypeChainsWrapperEm:
.LFB25371:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8JSObject25InvalidatePrototypeChainsENS0_3MapE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25371:
	.size	_ZN2v88internalL32InvalidatePrototypeChainsWrapperEm, .-_ZN2v88internalL32InvalidatePrototypeChainsWrapperEm
	.section	.text._ZN2v88internalL16atomic_pair_loadEl,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL16atomic_pair_loadEl, @function
_ZN2v88internalL16atomic_pair_loadEl:
.LFB25384:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE25384:
	.size	_ZN2v88internalL16atomic_pair_loadEl, .-_ZN2v88internalL16atomic_pair_loadEl
	.section	.text._ZN2v88internalL17atomic_pair_storeElii,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL17atomic_pair_storeElii, @function
_ZN2v88internalL17atomic_pair_storeElii:
.LFB25386:
	.cfi_startproc
	endbr64
	salq	$32, %rdx
	movl	%esi, %esi
	orq	%rsi, %rdx
	movq	%rdx, (%rdi)
	mfence
	ret
	.cfi_endproc
.LFE25386:
	.size	_ZN2v88internalL17atomic_pair_storeElii, .-_ZN2v88internalL17atomic_pair_storeElii
	.section	.text._ZN2v88internalL15atomic_pair_addElii,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL15atomic_pair_addElii, @function
_ZN2v88internalL15atomic_pair_addElii:
.LFB25388:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	salq	$32, %rdx
	orq	%rax, %rdx
	movq	%rdx, %rax
	lock xaddq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE25388:
	.size	_ZN2v88internalL15atomic_pair_addElii, .-_ZN2v88internalL15atomic_pair_addElii
	.section	.text._ZN2v88internalL15atomic_pair_subElii,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL15atomic_pair_subElii, @function
_ZN2v88internalL15atomic_pair_subElii:
.LFB25390:
	.cfi_startproc
	endbr64
	salq	$32, %rdx
	movl	%esi, %eax
	orq	%rdx, %rax
	negq	%rax
	lock xaddq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE25390:
	.size	_ZN2v88internalL15atomic_pair_subElii, .-_ZN2v88internalL15atomic_pair_subElii
	.section	.text._ZN2v88internalL15atomic_pair_andElii,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL15atomic_pair_andElii, @function
_ZN2v88internalL15atomic_pair_andElii:
.LFB25392:
	.cfi_startproc
	endbr64
	salq	$32, %rdx
	movl	%esi, %esi
	movq	(%rdi), %rax
	orq	%rsi, %rdx
.L167:
	movq	%rax, %rcx
	movq	%rax, %r8
	andq	%rdx, %rcx
	lock cmpxchgq	%rcx, (%rdi)
	jne	.L167
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE25392:
	.size	_ZN2v88internalL15atomic_pair_andElii, .-_ZN2v88internalL15atomic_pair_andElii
	.section	.text._ZN2v88internalL14atomic_pair_orElii,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL14atomic_pair_orElii, @function
_ZN2v88internalL14atomic_pair_orElii:
.LFB25394:
	.cfi_startproc
	endbr64
	salq	$32, %rdx
	movl	%esi, %esi
	movq	(%rdi), %rax
	orq	%rsi, %rdx
.L171:
	movq	%rax, %rcx
	movq	%rax, %r8
	orq	%rdx, %rcx
	lock cmpxchgq	%rcx, (%rdi)
	jne	.L171
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE25394:
	.size	_ZN2v88internalL14atomic_pair_orElii, .-_ZN2v88internalL14atomic_pair_orElii
	.section	.text._ZN2v88internalL15atomic_pair_xorElii,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL15atomic_pair_xorElii, @function
_ZN2v88internalL15atomic_pair_xorElii:
.LFB25396:
	.cfi_startproc
	endbr64
	salq	$32, %rdx
	movl	%esi, %esi
	movq	(%rdi), %rax
	orq	%rsi, %rdx
.L175:
	movq	%rax, %rcx
	movq	%rax, %r8
	xorq	%rdx, %rcx
	lock cmpxchgq	%rcx, (%rdi)
	jne	.L175
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE25396:
	.size	_ZN2v88internalL15atomic_pair_xorElii, .-_ZN2v88internalL15atomic_pair_xorElii
	.section	.text._ZN2v88internalL20atomic_pair_exchangeElii,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL20atomic_pair_exchangeElii, @function
_ZN2v88internalL20atomic_pair_exchangeElii:
.LFB25398:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	salq	$32, %rdx
	orq	%rax, %rdx
	movq	%rdx, %rax
	xchgq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE25398:
	.size	_ZN2v88internalL20atomic_pair_exchangeElii, .-_ZN2v88internalL20atomic_pair_exchangeElii
	.section	.rodata._ZN2v88internal17abort_with_reasonEi.str1.1,"aMS",@progbits,1
.LC0:
	.string	"abort: %s\n"
.LC1:
	.string	"abort: <unknown reason: %d>\n"
	.section	.text._ZN2v88internal17abort_with_reasonEi,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal17abort_with_reasonEi
	.type	_ZN2v88internal17abort_with_reasonEi, @function
_ZN2v88internal17abort_with_reasonEi:
.LFB25416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edi, %r12d
	subq	$8, %rsp
	call	_ZN2v88internal18IsValidAbortReasonEi@PLT
	testb	%al, %al
	je	.L180
	movl	%r12d, %edi
	call	_ZN2v88internal14GetAbortReasonENS0_11AbortReasonE@PLT
	leaq	.LC0(%rip), %rdi
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
.L181:
	call	_ZN2v84base2OS5AbortEv@PLT
.L180:
	movl	%r12d, %esi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	jmp	.L181
	.cfi_endproc
.LFE25416:
	.size	_ZN2v88internal17abort_with_reasonEi, .-_ZN2v88internal17abort_with_reasonEi
	.section	.text._ZN2v88internalL24ComputeSeededIntegerHashEPNS0_7IsolateEj,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL24ComputeSeededIntegerHashEPNS0_7IsolateEj, @function
_ZN2v88internalL24ComputeSeededIntegerHashEPNS0_7IsolateEj:
.LFB25336:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movl	%esi, %edi
	movq	1176(%r8), %rax
	movq	15(%rax), %rsi
	jmp	_Z11halfsiphashjm@PLT
	.cfi_endproc
.LFE25336:
	.size	_ZN2v88internalL24ComputeSeededIntegerHashEPNS0_7IsolateEj, .-_ZN2v88internalL24ComputeSeededIntegerHashEPNS0_7IsolateEj
	.section	.text._ZN2v88internalL28EnterMicrotaskContextWrapperEPNS0_22HandleScopeImplementerEm,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28EnterMicrotaskContextWrapperEPNS0_22HandleScopeImplementerEm, @function
_ZN2v88internalL28EnterMicrotaskContextWrapperEPNS0_22HandleScopeImplementerEm:
.LFB25407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	48(%rdi), %rdx
	cmpq	40(%rdi), %rdx
	je	.L185
	movq	32(%rdi), %r12
	salq	$3, %rdx
.L186:
	movq	%r13, (%r12,%rdx)
	movq	72(%rbx), %rdx
	addq	$1, 48(%rbx)
	cmpq	64(%rbx), %rdx
	je	.L198
	movq	56(%rbx), %r12
.L199:
	movb	$1, (%r12,%rdx)
	xorl	%eax, %eax
	addq	$1, 72(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	movq	_ZN2v88internal20DetachableVectorBase16kMinimumCapacityE(%rip), %r14
	addq	%rdx, %rdx
	movabsq	$1152921504606846975, %rax
	cmpq	%r14, %rdx
	cmovnb	%rdx, %r14
	cmpq	%rax, %r14
	leaq	0(,%r14,8), %rdi
	movq	$-1, %rax
	cmova	%rax, %rdi
	call	_Znam@PLT
	movq	%rax, %r12
	movq	%r14, %rax
	subq	$1, %rax
	js	.L194
	leaq	-2(%r14), %rax
	movl	$1, %ecx
	cmpq	$-1, %rax
	cmovge	%r14, %rcx
	cmpq	$1, %r14
	je	.L204
	cmpq	$-1, %rax
	jl	.L204
	movq	%rcx, %rsi
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	shrq	%rsi
	.p2align 4,,10
	.p2align 3
.L193:
	movq	%rax, %rdx
	addq	$1, %rax
	salq	$4, %rdx
	movups	%xmm0, (%r12,%rdx)
	cmpq	%rax, %rsi
	jne	.L193
	movq	%rcx, %rdx
	andq	$-2, %rdx
	leaq	(%r12,%rdx,8), %rax
	cmpq	%rdx, %rcx
	je	.L194
.L191:
	movq	$0, (%rax)
.L194:
	movq	48(%rbx), %rdx
	movq	32(%rbx), %r15
	salq	$3, %rdx
	je	.L213
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	memmove@PLT
.L196:
	movq	%r15, %rdi
	call	_ZdaPv@PLT
	movq	48(%rbx), %rax
	leaq	0(,%rax,8), %rdx
.L197:
	movq	%r12, 32(%rbx)
	movq	%r14, 40(%rbx)
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L198:
	movq	_ZN2v88internal20DetachableVectorBase16kMinimumCapacityE(%rip), %r13
	addq	%rdx, %rdx
	cmpq	%r13, %rdx
	cmovnb	%rdx, %r13
	movq	%r13, %rdi
	call	_Znam@PLT
	movq	72(%rbx), %rdx
	movq	56(%rbx), %r14
	movq	%rax, %r12
	testq	%rdx, %rdx
	jne	.L214
	testq	%r14, %r14
	jne	.L201
	movq	%r12, 56(%rbx)
	movq	%r13, 64(%rbx)
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L213:
	testq	%r15, %r15
	je	.L197
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L214:
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	memmove@PLT
.L201:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movq	%r12, 56(%rbx)
	movq	72(%rbx), %rdx
	movq	%r13, 64(%rbx)
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L204:
	movq	%r12, %rax
	jmp	.L191
	.cfi_endproc
.LFE25407:
	.size	_ZN2v88internalL28EnterMicrotaskContextWrapperEPNS0_22HandleScopeImplementerEm, .-_ZN2v88internalL28EnterMicrotaskContextWrapperEPNS0_22HandleScopeImplementerEm
	.section	.text._ZN2v88internalL21ConvertOneByteToLowerEmm,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL21ConvertOneByteToLowerEmm, @function
_ZN2v88internalL21ConvertOneByteToLowerEmm:
.LFB25358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal4Intl21ConvertOneByteToLowerENS0_6StringES2_@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25358:
	.size	_ZN2v88internalL21ConvertOneByteToLowerEmm, .-_ZN2v88internalL21ConvertOneByteToLowerEmm
	.section	.text._ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi:
.LFB30857:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movl	%ecx, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	8(%rax), %r12
	movq	16(%rax), %rax
	subl	%eax, %ebx
	cmpl	%ebx, %ecx
	jg	.L222
	movl	%eax, %ecx
	movzbl	(%r12), %r14d
	leal	1(%rbx), %eax
	movq	%rsi, %r15
	movl	%eax, -52(%rbp)
	leal	-1(%rcx), %eax
	movl	%eax, -56(%rbp)
	movl	%r14d, %r13d
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L236:
	subq	%r15, %rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	addq	%r15, %rdx
	cmpb	(%rdx), %r13b
	je	.L223
	leal	1(%rax), %edi
	cmpl	%eax, %ebx
	jle	.L222
.L224:
	movl	-52(%rbp), %edx
	movl	%r14d, %esi
	subl	%edi, %edx
	movslq	%edi, %rdi
	addq	%r15, %rdi
	movslq	%edx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L236
.L222:
	movl	$-1, %r9d
.L217:
	addq	$24, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L222
	leal	1(%rax), %edi
	xorl	%eax, %eax
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L237:
	addq	$1, %rax
	cmpl	%eax, -56(%rbp)
	jle	.L217
.L226:
	movzbl	1(%rdx,%rax), %ecx
	cmpb	%cl, 1(%r12,%rax)
	je	.L237
	cmpl	%edi, %ebx
	jge	.L224
	jmp	.L222
	.cfi_endproc
.LFE30857:
	.size	_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi:
.LFB30861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movl	%ecx, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	8(%rax), %r13
	movq	16(%rax), %rax
	subl	%eax, %ebx
	cmpl	%ebx, %ecx
	jg	.L243
	movl	%eax, %ecx
	movzwl	0(%r13), %eax
	movq	%rsi, %r15
	leal	1(%rbx), %esi
	movl	%esi, -52(%rbp)
	movzbl	%ah, %edx
	movl	%eax, %r14d
	cmpb	%dl, %al
	cmovbe	%edx, %eax
	movzbl	%al, %r12d
	leal	-1(%rcx), %eax
	movl	%eax, -56(%rbp)
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L257:
	subq	%r15, %rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	addq	%r15, %rdx
	cmpb	%r14b, (%rdx)
	je	.L244
	leal	1(%rax), %edi
	cmpl	%eax, %ebx
	jle	.L243
.L245:
	movl	-52(%rbp), %edx
	movl	%r12d, %esi
	subl	%edi, %edx
	movslq	%edi, %rdi
	addq	%r15, %rdi
	movslq	%edx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L257
.L243:
	movl	$-1, %r9d
.L238:
	addq	$24, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L243
	leal	1(%rax), %edi
	xorl	%eax, %eax
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L258:
	addq	$1, %rax
	cmpl	%eax, -56(%rbp)
	jle	.L238
.L247:
	movzbl	1(%rdx,%rax), %ecx
	cmpw	%cx, 2(%r13,%rax,2)
	je	.L258
	cmpl	%edi, %ebx
	jge	.L245
	jmp	.L243
	.cfi_endproc
.LFE30861:
	.size	_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi:
.LFB30865:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	16(%rdi), %rax
	movq	8(%rdi), %r13
	subl	%eax, %ebx
	cmpl	%ebx, %ecx
	jg	.L264
	movzbl	0(%r13), %r14d
	movq	%rsi, %r15
	movl	%eax, %esi
	leal	1(%rbx), %eax
	movl	%eax, -52(%rbp)
	leal	-1(%rsi), %eax
	movl	%eax, -56(%rbp)
	movl	%r14d, %r12d
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L278:
	andq	$-2, %rax
	subq	%r15, %rax
	sarq	%rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	leaq	(%r15,%rdx,2), %rdx
	cmpw	%r12w, (%rdx)
	je	.L265
	leal	1(%rax), %ecx
	cmpl	%eax, %ebx
	jle	.L264
.L266:
	movl	-52(%rbp), %edx
	movl	%r14d, %esi
	subl	%ecx, %edx
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	leaq	(%r15,%rcx,2), %rdi
	addq	%rdx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L278
.L264:
	movl	$-1, %r9d
.L259:
	addq	$24, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L264
	leal	1(%rax), %ecx
	xorl	%eax, %eax
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L279:
	addq	$1, %rax
	cmpl	%eax, -56(%rbp)
	jle	.L259
.L268:
	movzbl	1(%r13,%rax), %edi
	movzwl	2(%rdx,%rax,2), %esi
	cmpl	%esi, %edi
	je	.L279
	cmpl	%ecx, %ebx
	jge	.L266
	jmp	.L264
	.cfi_endproc
.LFE30865:
	.size	_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi:
.LFB30869:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	16(%rdi), %rax
	movq	8(%rdi), %r14
	subl	%eax, %ebx
	cmpl	%ebx, %ecx
	jg	.L285
	movzwl	(%r14), %r13d
	movl	%eax, %edi
	leal	1(%rbx), %eax
	movq	%rsi, %r15
	movl	%eax, -52(%rbp)
	movslq	%ecx, %rsi
	movl	%r13d, %eax
	movzbl	%ah, %eax
	cmpb	%al, %r13b
	cmova	%r13d, %eax
	movzbl	%al, %r12d
	leal	-1(%rdi), %eax
	movl	%eax, -56(%rbp)
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L299:
	andq	$-2, %rax
	subq	%r15, %rax
	sarq	%rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	cmpw	%r13w, (%r15,%rdx,2)
	je	.L286
	leal	1(%rax), %ecx
	cmpl	%eax, %ebx
	jle	.L285
	movslq	%ecx, %rsi
.L287:
	movl	-52(%rbp), %edx
	leaq	(%r15,%rsi,2), %rdi
	movl	%r12d, %esi
	subl	%ecx, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L299
.L285:
	movl	$-1, %r9d
.L280:
	addq	$24, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L286:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L285
	leal	1(%rax), %ecx
	xorl	%eax, %eax
	movslq	%ecx, %rsi
	leaq	(%r15,%rsi,2), %rdx
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L300:
	addq	$1, %rax
	cmpl	%eax, -56(%rbp)
	jle	.L280
.L289:
	movzwl	(%rdx,%rax,2), %edi
	cmpw	%di, 2(%r14,%rax,2)
	je	.L300
	cmpl	%ecx, %ebx
	jge	.L287
	jmp	.L285
	.cfi_endproc
.LFE30869:
	.size	_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal17ExternalReference6CreateEPNS_11ApiFunctionENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference6CreateEPNS_11ApiFunctionENS1_4TypeE
	.type	_ZN2v88internal17ExternalReference6CreateEPNS_11ApiFunctionENS1_4TypeE, @function
_ZN2v88internal17ExternalReference6CreateEPNS_11ApiFunctionENS1_4TypeE:
.LFB25123:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE25123:
	.size	_ZN2v88internal17ExternalReference6CreateEPNS_11ApiFunctionENS1_4TypeE, .-_ZN2v88internal17ExternalReference6CreateEPNS_11ApiFunctionENS1_4TypeE
	.section	.rodata._ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"unreachable code"
	.section	.text._ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE
	.type	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE, @function
_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE:
.LFB25124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	movzbl	25(%rax), %ecx
	leal	-1(%rcx), %edx
	cmpb	$1, %dl
	ja	.L305
	movq	16(%rax), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L305:
	.cfi_restore_state
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25124:
	.size	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE, .-_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE
	.section	.text._ZN2v88internal17ExternalReference6CreateEPKNS0_7Runtime8FunctionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference6CreateEPKNS0_7Runtime8FunctionE
	.type	_ZN2v88internal17ExternalReference6CreateEPKNS0_7Runtime8FunctionE, @function
_ZN2v88internal17ExternalReference6CreateEPKNS0_7Runtime8FunctionE:
.LFB25125:
	.cfi_startproc
	endbr64
	movzbl	25(%rdi), %eax
	subl	$1, %eax
	cmpb	$1, %al
	ja	.L311
	movq	16(%rdi), %rax
	ret
.L311:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25125:
	.size	_ZN2v88internal17ExternalReference6CreateEPKNS0_7Runtime8FunctionE, .-_ZN2v88internal17ExternalReference6CreateEPKNS0_7Runtime8FunctionE
	.section	.text._ZN2v88internal17ExternalReference6CreateEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference6CreateEm
	.type	_ZN2v88internal17ExternalReference6CreateEm, @function
_ZN2v88internal17ExternalReference6CreateEm:
.LFB25126:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE25126:
	.size	_ZN2v88internal17ExternalReference6CreateEm, .-_ZN2v88internal17ExternalReference6CreateEm
	.section	.text._ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE:
.LFB25127:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE25127:
	.size	_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference16builtins_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference16builtins_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference16builtins_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference16builtins_addressEPNS0_7IsolateE:
.LFB25128:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$37592, %rdi
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal4Heap15builtin_addressEi@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25128:
	.size	_ZN2v88internal17ExternalReference16builtins_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference16builtins_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference32handle_scope_implementer_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference32handle_scope_implementer_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference32handle_scope_implementer_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference32handle_scope_implementer_addressEPNS0_7IsolateE:
.LFB25129:
	.cfi_startproc
	endbr64
	leaq	41120(%rdi), %rax
	ret
	.cfi_endproc
.LFE25129:
	.size	_ZN2v88internal17ExternalReference32handle_scope_implementer_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference32handle_scope_implementer_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference34interpreter_dispatch_table_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference34interpreter_dispatch_table_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference34interpreter_dispatch_table_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference34interpreter_dispatch_table_addressEPNS0_7IsolateE:
.LFB25130:
	.cfi_startproc
	endbr64
	movq	41504(%rdi), %rax
	addq	$16, %rax
	ret
	.cfi_endproc
.LFE25130:
	.size	_ZN2v88internal17ExternalReference34interpreter_dispatch_table_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference34interpreter_dispatch_table_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference29interpreter_dispatch_countersEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference29interpreter_dispatch_countersEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference29interpreter_dispatch_countersEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference29interpreter_dispatch_countersEPNS0_7IsolateE:
.LFB25131:
	.cfi_startproc
	endbr64
	movq	41504(%rdi), %rax
	movq	6160(%rax), %rax
	ret
	.cfi_endproc
.LFE25131:
	.size	_ZN2v88internal17ExternalReference29interpreter_dispatch_countersEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference29interpreter_dispatch_countersEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference57address_of_interpreter_entry_trampoline_instruction_startEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference57address_of_interpreter_entry_trampoline_instruction_startEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference57address_of_interpreter_entry_trampoline_instruction_startEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference57address_of_interpreter_entry_trampoline_instruction_startEPNS0_7IsolateE:
.LFB25132:
	.cfi_startproc
	endbr64
	movq	41504(%rdi), %rax
	addq	$6168, %rax
	ret
	.cfi_endproc
.LFE25132:
	.size	_ZN2v88internal17ExternalReference57address_of_interpreter_entry_trampoline_instruction_startEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference57address_of_interpreter_entry_trampoline_instruction_startEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference27bytecode_size_table_addressEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference27bytecode_size_table_addressEv
	.type	_ZN2v88internal17ExternalReference27bytecode_size_table_addressEv, @function
_ZN2v88internal17ExternalReference27bytecode_size_table_addressEv:
.LFB25133:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internal11interpreter9Bytecodes14kBytecodeSizesE(%rip), %rax
	ret
	.cfi_endproc
.LFE25133:
	.size	_ZN2v88internal17ExternalReference27bytecode_size_table_addressEv, .-_ZN2v88internal17ExternalReference27bytecode_size_table_addressEv
	.section	.text._ZN2v88internal17ExternalReference6CreateEPNS0_12StatsCounterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference6CreateEPNS0_12StatsCounterE
	.type	_ZN2v88internal17ExternalReference6CreateEPNS0_12StatsCounterE, @function
_ZN2v88internal17ExternalReference6CreateEPNS0_12StatsCounterE:
.LFB25134:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 24(%rdi)
	je	.L322
	movq	16(%rdi), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_restore_state
	movb	$1, 24(%rdi)
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25134:
	.size	_ZN2v88internal17ExternalReference6CreateEPNS0_12StatsCounterE, .-_ZN2v88internal17ExternalReference6CreateEPNS0_12StatsCounterE
	.section	.text._ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE:
.LFB25135:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edi, %r8d
	movq	%rsi, %rdi
	movl	%r8d, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal7Isolate19get_address_from_idENS0_16IsolateAddressIdE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25135:
	.size	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference6CreateERKNS0_16SCTableReferenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference6CreateERKNS0_16SCTableReferenceE
	.type	_ZN2v88internal17ExternalReference6CreateERKNS0_16SCTableReferenceE, @function
_ZN2v88internal17ExternalReference6CreateERKNS0_16SCTableReferenceE:
.LFB25136:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE25136:
	.size	_ZN2v88internal17ExternalReference6CreateERKNS0_16SCTableReferenceE, .-_ZN2v88internal17ExternalReference6CreateERKNS0_16SCTableReferenceE
	.section	.text._ZN2v88internal17ExternalReference41incremental_marking_record_write_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference41incremental_marking_record_write_functionEv
	.type	_ZN2v88internal17ExternalReference41incremental_marking_record_write_functionEv, @function
_ZN2v88internal17ExternalReference41incremental_marking_record_write_functionEv:
.LFB25139:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal18IncrementalMarking19RecordWriteFromCodeEmmPNS0_7IsolateE@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25139:
	.size	_ZN2v88internal17ExternalReference41incremental_marking_record_write_functionEv, .-_ZN2v88internal17ExternalReference41incremental_marking_record_write_functionEv
	.section	.text._ZN2v88internal17ExternalReference30store_buffer_overflow_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference30store_buffer_overflow_functionEv
	.type	_ZN2v88internal17ExternalReference30store_buffer_overflow_functionEv, @function
_ZN2v88internal17ExternalReference30store_buffer_overflow_functionEv:
.LFB25146:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal4Heap38store_buffer_overflow_function_addressEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25146:
	.size	_ZN2v88internal17ExternalReference30store_buffer_overflow_functionEv, .-_ZN2v88internal17ExternalReference30store_buffer_overflow_functionEv
	.section	.text._ZN2v88internal17ExternalReference30delete_handle_scope_extensionsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference30delete_handle_scope_extensionsEv
	.type	_ZN2v88internal17ExternalReference30delete_handle_scope_extensionsEv, @function
_ZN2v88internal17ExternalReference30delete_handle_scope_extensionsEv:
.LFB25147:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25147:
	.size	_ZN2v88internal17ExternalReference30delete_handle_scope_extensionsEv, .-_ZN2v88internal17ExternalReference30delete_handle_scope_extensionsEv
	.section	.text._ZN2v88internal17ExternalReference36ephemeron_key_write_barrier_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference36ephemeron_key_write_barrier_functionEv
	.type	_ZN2v88internal17ExternalReference36ephemeron_key_write_barrier_functionEv, @function
_ZN2v88internal17ExternalReference36ephemeron_key_write_barrier_functionEv:
.LFB25150:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4Heap32EphemeronKeyWriteBarrierFromCodeEmmPNS0_7IsolateE@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25150:
	.size	_ZN2v88internal17ExternalReference36ephemeron_key_write_barrier_functionEv, .-_ZN2v88internal17ExternalReference36ephemeron_key_write_barrier_functionEv
	.section	.text._ZN2v88internal17ExternalReference23get_date_field_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference23get_date_field_functionEv
	.type	_ZN2v88internal17ExternalReference23get_date_field_functionEv, @function
_ZN2v88internal17ExternalReference23get_date_field_functionEv:
.LFB25152:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal6JSDate8GetFieldEmm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25152:
	.size	_ZN2v88internal17ExternalReference23get_date_field_functionEv, .-_ZN2v88internal17ExternalReference23get_date_field_functionEv
	.section	.text._ZN2v88internal17ExternalReference16date_cache_stampEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference16date_cache_stampEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference16date_cache_stampEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference16date_cache_stampEPNS0_7IsolateE:
.LFB25155:
	.cfi_startproc
	endbr64
	movq	41240(%rdi), %rax
	addq	$8, %rax
	ret
	.cfi_endproc
.LFE25155:
	.size	_ZN2v88internal17ExternalReference16date_cache_stampEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference16date_cache_stampEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference44runtime_function_table_address_for_unittestsEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference44runtime_function_table_address_for_unittestsEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference44runtime_function_table_address_for_unittestsEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference44runtime_function_table_address_for_unittestsEPNS0_7IsolateE:
.LFB25156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal7Runtime20RuntimeFunctionTableEPNS0_7IsolateE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25156:
	.size	_ZN2v88internal17ExternalReference44runtime_function_table_address_for_unittestsEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference44runtime_function_table_address_for_unittestsEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference8RedirectEmNS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference8RedirectEmNS1_4TypeE
	.type	_ZN2v88internal17ExternalReference8RedirectEmNS1_4TypeE, @function
_ZN2v88internal17ExternalReference8RedirectEmNS1_4TypeE:
.LFB25157:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE25157:
	.size	_ZN2v88internal17ExternalReference8RedirectEmNS1_4TypeE, .-_ZN2v88internal17ExternalReference8RedirectEmNS1_4TypeE
	.section	.text._ZN2v88internal17ExternalReference18stress_deopt_countEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference18stress_deopt_countEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference18stress_deopt_countEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference18stress_deopt_countEPNS0_7IsolateE:
.LFB25158:
	.cfi_startproc
	endbr64
	leaq	45424(%rdi), %rax
	ret
	.cfi_endproc
.LFE25158:
	.size	_ZN2v88internal17ExternalReference18stress_deopt_countEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference18stress_deopt_countEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference15force_slow_pathEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference15force_slow_pathEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference15force_slow_pathEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference15force_slow_pathEPNS0_7IsolateE:
.LFB25159:
	.cfi_startproc
	endbr64
	leaq	45428(%rdi), %rax
	ret
	.cfi_endproc
.LFE25159:
	.size	_ZN2v88internal17ExternalReference15force_slow_pathEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference15force_slow_pathEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference24new_deoptimizer_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference24new_deoptimizer_functionEv
	.type	_ZN2v88internal17ExternalReference24new_deoptimizer_functionEv, @function
_ZN2v88internal17ExternalReference24new_deoptimizer_functionEv:
.LFB25160:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal11Deoptimizer3NewEmNS0_14DeoptimizeKindEjmiPNS0_7IsolateE@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25160:
	.size	_ZN2v88internal17ExternalReference24new_deoptimizer_functionEv, .-_ZN2v88internal17ExternalReference24new_deoptimizer_functionEv
	.section	.text._ZN2v88internal17ExternalReference30compute_output_frames_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference30compute_output_frames_functionEv
	.type	_ZN2v88internal17ExternalReference30compute_output_frames_functionEv, @function
_ZN2v88internal17ExternalReference30compute_output_frames_functionEv:
.LFB25170:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal11Deoptimizer19ComputeOutputFramesEPS1_@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25170:
	.size	_ZN2v88internal17ExternalReference30compute_output_frames_functionEv, .-_ZN2v88internal17ExternalReference30compute_output_frames_functionEv
	.section	.text._ZN2v88internal17ExternalReference14wasm_f32_truncEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference14wasm_f32_truncEv
	.type	_ZN2v88internal17ExternalReference14wasm_f32_truncEv, @function
_ZN2v88internal17ExternalReference14wasm_f32_truncEv:
.LFB25172:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm17f32_trunc_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25172:
	.size	_ZN2v88internal17ExternalReference14wasm_f32_truncEv, .-_ZN2v88internal17ExternalReference14wasm_f32_truncEv
	.section	.text._ZN2v88internal17ExternalReference14wasm_f32_floorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference14wasm_f32_floorEv
	.type	_ZN2v88internal17ExternalReference14wasm_f32_floorEv, @function
_ZN2v88internal17ExternalReference14wasm_f32_floorEv:
.LFB25174:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm17f32_floor_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25174:
	.size	_ZN2v88internal17ExternalReference14wasm_f32_floorEv, .-_ZN2v88internal17ExternalReference14wasm_f32_floorEv
	.section	.text._ZN2v88internal17ExternalReference13wasm_f32_ceilEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference13wasm_f32_ceilEv
	.type	_ZN2v88internal17ExternalReference13wasm_f32_ceilEv, @function
_ZN2v88internal17ExternalReference13wasm_f32_ceilEv:
.LFB25175:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm16f32_ceil_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25175:
	.size	_ZN2v88internal17ExternalReference13wasm_f32_ceilEv, .-_ZN2v88internal17ExternalReference13wasm_f32_ceilEv
	.section	.text._ZN2v88internal17ExternalReference20wasm_f32_nearest_intEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference20wasm_f32_nearest_intEv
	.type	_ZN2v88internal17ExternalReference20wasm_f32_nearest_intEv, @function
_ZN2v88internal17ExternalReference20wasm_f32_nearest_intEv:
.LFB25176:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm23f32_nearest_int_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25176:
	.size	_ZN2v88internal17ExternalReference20wasm_f32_nearest_intEv, .-_ZN2v88internal17ExternalReference20wasm_f32_nearest_intEv
	.section	.text._ZN2v88internal17ExternalReference14wasm_f64_truncEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference14wasm_f64_truncEv
	.type	_ZN2v88internal17ExternalReference14wasm_f64_truncEv, @function
_ZN2v88internal17ExternalReference14wasm_f64_truncEv:
.LFB25177:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm17f64_trunc_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25177:
	.size	_ZN2v88internal17ExternalReference14wasm_f64_truncEv, .-_ZN2v88internal17ExternalReference14wasm_f64_truncEv
	.section	.text._ZN2v88internal17ExternalReference14wasm_f64_floorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference14wasm_f64_floorEv
	.type	_ZN2v88internal17ExternalReference14wasm_f64_floorEv, @function
_ZN2v88internal17ExternalReference14wasm_f64_floorEv:
.LFB25178:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm17f64_floor_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25178:
	.size	_ZN2v88internal17ExternalReference14wasm_f64_floorEv, .-_ZN2v88internal17ExternalReference14wasm_f64_floorEv
	.section	.text._ZN2v88internal17ExternalReference13wasm_f64_ceilEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference13wasm_f64_ceilEv
	.type	_ZN2v88internal17ExternalReference13wasm_f64_ceilEv, @function
_ZN2v88internal17ExternalReference13wasm_f64_ceilEv:
.LFB25179:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm16f64_ceil_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25179:
	.size	_ZN2v88internal17ExternalReference13wasm_f64_ceilEv, .-_ZN2v88internal17ExternalReference13wasm_f64_ceilEv
	.section	.text._ZN2v88internal17ExternalReference20wasm_f64_nearest_intEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference20wasm_f64_nearest_intEv
	.type	_ZN2v88internal17ExternalReference20wasm_f64_nearest_intEv, @function
_ZN2v88internal17ExternalReference20wasm_f64_nearest_intEv:
.LFB25180:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm23f64_nearest_int_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25180:
	.size	_ZN2v88internal17ExternalReference20wasm_f64_nearest_intEv, .-_ZN2v88internal17ExternalReference20wasm_f64_nearest_intEv
	.section	.text._ZN2v88internal17ExternalReference21wasm_int64_to_float32Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference21wasm_int64_to_float32Ev
	.type	_ZN2v88internal17ExternalReference21wasm_int64_to_float32Ev, @function
_ZN2v88internal17ExternalReference21wasm_int64_to_float32Ev:
.LFB25181:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm24int64_to_float32_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25181:
	.size	_ZN2v88internal17ExternalReference21wasm_int64_to_float32Ev, .-_ZN2v88internal17ExternalReference21wasm_int64_to_float32Ev
	.section	.text._ZN2v88internal17ExternalReference22wasm_uint64_to_float32Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference22wasm_uint64_to_float32Ev
	.type	_ZN2v88internal17ExternalReference22wasm_uint64_to_float32Ev, @function
_ZN2v88internal17ExternalReference22wasm_uint64_to_float32Ev:
.LFB25182:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm25uint64_to_float32_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25182:
	.size	_ZN2v88internal17ExternalReference22wasm_uint64_to_float32Ev, .-_ZN2v88internal17ExternalReference22wasm_uint64_to_float32Ev
	.section	.text._ZN2v88internal17ExternalReference21wasm_int64_to_float64Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference21wasm_int64_to_float64Ev
	.type	_ZN2v88internal17ExternalReference21wasm_int64_to_float64Ev, @function
_ZN2v88internal17ExternalReference21wasm_int64_to_float64Ev:
.LFB25183:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm24int64_to_float64_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25183:
	.size	_ZN2v88internal17ExternalReference21wasm_int64_to_float64Ev, .-_ZN2v88internal17ExternalReference21wasm_int64_to_float64Ev
	.section	.text._ZN2v88internal17ExternalReference22wasm_uint64_to_float64Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference22wasm_uint64_to_float64Ev
	.type	_ZN2v88internal17ExternalReference22wasm_uint64_to_float64Ev, @function
_ZN2v88internal17ExternalReference22wasm_uint64_to_float64Ev:
.LFB25184:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm25uint64_to_float64_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25184:
	.size	_ZN2v88internal17ExternalReference22wasm_uint64_to_float64Ev, .-_ZN2v88internal17ExternalReference22wasm_uint64_to_float64Ev
	.section	.text._ZN2v88internal17ExternalReference21wasm_float32_to_int64Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference21wasm_float32_to_int64Ev
	.type	_ZN2v88internal17ExternalReference21wasm_float32_to_int64Ev, @function
_ZN2v88internal17ExternalReference21wasm_float32_to_int64Ev:
.LFB25185:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm24float32_to_int64_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25185:
	.size	_ZN2v88internal17ExternalReference21wasm_float32_to_int64Ev, .-_ZN2v88internal17ExternalReference21wasm_float32_to_int64Ev
	.section	.text._ZN2v88internal17ExternalReference22wasm_float32_to_uint64Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference22wasm_float32_to_uint64Ev
	.type	_ZN2v88internal17ExternalReference22wasm_float32_to_uint64Ev, @function
_ZN2v88internal17ExternalReference22wasm_float32_to_uint64Ev:
.LFB25187:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm25float32_to_uint64_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25187:
	.size	_ZN2v88internal17ExternalReference22wasm_float32_to_uint64Ev, .-_ZN2v88internal17ExternalReference22wasm_float32_to_uint64Ev
	.section	.text._ZN2v88internal17ExternalReference21wasm_float64_to_int64Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference21wasm_float64_to_int64Ev
	.type	_ZN2v88internal17ExternalReference21wasm_float64_to_int64Ev, @function
_ZN2v88internal17ExternalReference21wasm_float64_to_int64Ev:
.LFB25188:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm24float64_to_int64_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25188:
	.size	_ZN2v88internal17ExternalReference21wasm_float64_to_int64Ev, .-_ZN2v88internal17ExternalReference21wasm_float64_to_int64Ev
	.section	.text._ZN2v88internal17ExternalReference22wasm_float64_to_uint64Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference22wasm_float64_to_uint64Ev
	.type	_ZN2v88internal17ExternalReference22wasm_float64_to_uint64Ev, @function
_ZN2v88internal17ExternalReference22wasm_float64_to_uint64Ev:
.LFB25189:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm25float64_to_uint64_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25189:
	.size	_ZN2v88internal17ExternalReference22wasm_float64_to_uint64Ev, .-_ZN2v88internal17ExternalReference22wasm_float64_to_uint64Ev
	.section	.text._ZN2v88internal17ExternalReference14wasm_int64_divEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference14wasm_int64_divEv
	.type	_ZN2v88internal17ExternalReference14wasm_int64_divEv, @function
_ZN2v88internal17ExternalReference14wasm_int64_divEv:
.LFB25190:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm17int64_div_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25190:
	.size	_ZN2v88internal17ExternalReference14wasm_int64_divEv, .-_ZN2v88internal17ExternalReference14wasm_int64_divEv
	.section	.text._ZN2v88internal17ExternalReference14wasm_int64_modEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference14wasm_int64_modEv
	.type	_ZN2v88internal17ExternalReference14wasm_int64_modEv, @function
_ZN2v88internal17ExternalReference14wasm_int64_modEv:
.LFB25191:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm17int64_mod_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25191:
	.size	_ZN2v88internal17ExternalReference14wasm_int64_modEv, .-_ZN2v88internal17ExternalReference14wasm_int64_modEv
	.section	.text._ZN2v88internal17ExternalReference15wasm_uint64_divEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference15wasm_uint64_divEv
	.type	_ZN2v88internal17ExternalReference15wasm_uint64_divEv, @function
_ZN2v88internal17ExternalReference15wasm_uint64_divEv:
.LFB25192:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm18uint64_div_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25192:
	.size	_ZN2v88internal17ExternalReference15wasm_uint64_divEv, .-_ZN2v88internal17ExternalReference15wasm_uint64_divEv
	.section	.text._ZN2v88internal17ExternalReference15wasm_uint64_modEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference15wasm_uint64_modEv
	.type	_ZN2v88internal17ExternalReference15wasm_uint64_modEv, @function
_ZN2v88internal17ExternalReference15wasm_uint64_modEv:
.LFB25193:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm18uint64_mod_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25193:
	.size	_ZN2v88internal17ExternalReference15wasm_uint64_modEv, .-_ZN2v88internal17ExternalReference15wasm_uint64_modEv
	.section	.text._ZN2v88internal17ExternalReference15wasm_word32_ctzEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference15wasm_word32_ctzEv
	.type	_ZN2v88internal17ExternalReference15wasm_word32_ctzEv, @function
_ZN2v88internal17ExternalReference15wasm_word32_ctzEv:
.LFB25194:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm18word32_ctz_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25194:
	.size	_ZN2v88internal17ExternalReference15wasm_word32_ctzEv, .-_ZN2v88internal17ExternalReference15wasm_word32_ctzEv
	.section	.text._ZN2v88internal17ExternalReference15wasm_word64_ctzEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference15wasm_word64_ctzEv
	.type	_ZN2v88internal17ExternalReference15wasm_word64_ctzEv, @function
_ZN2v88internal17ExternalReference15wasm_word64_ctzEv:
.LFB25196:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm18word64_ctz_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25196:
	.size	_ZN2v88internal17ExternalReference15wasm_word64_ctzEv, .-_ZN2v88internal17ExternalReference15wasm_word64_ctzEv
	.section	.text._ZN2v88internal17ExternalReference18wasm_word32_popcntEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference18wasm_word32_popcntEv
	.type	_ZN2v88internal17ExternalReference18wasm_word32_popcntEv, @function
_ZN2v88internal17ExternalReference18wasm_word32_popcntEv:
.LFB25197:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm21word32_popcnt_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25197:
	.size	_ZN2v88internal17ExternalReference18wasm_word32_popcntEv, .-_ZN2v88internal17ExternalReference18wasm_word32_popcntEv
	.section	.text._ZN2v88internal17ExternalReference18wasm_word64_popcntEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference18wasm_word64_popcntEv
	.type	_ZN2v88internal17ExternalReference18wasm_word64_popcntEv, @function
_ZN2v88internal17ExternalReference18wasm_word64_popcntEv:
.LFB25198:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm21word64_popcnt_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25198:
	.size	_ZN2v88internal17ExternalReference18wasm_word64_popcntEv, .-_ZN2v88internal17ExternalReference18wasm_word64_popcntEv
	.section	.text._ZN2v88internal17ExternalReference15wasm_word32_rolEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference15wasm_word32_rolEv
	.type	_ZN2v88internal17ExternalReference15wasm_word32_rolEv, @function
_ZN2v88internal17ExternalReference15wasm_word32_rolEv:
.LFB25199:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm18word32_rol_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25199:
	.size	_ZN2v88internal17ExternalReference15wasm_word32_rolEv, .-_ZN2v88internal17ExternalReference15wasm_word32_rolEv
	.section	.text._ZN2v88internal17ExternalReference15wasm_word32_rorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference15wasm_word32_rorEv
	.type	_ZN2v88internal17ExternalReference15wasm_word32_rorEv, @function
_ZN2v88internal17ExternalReference15wasm_word32_rorEv:
.LFB25200:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm18word32_ror_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25200:
	.size	_ZN2v88internal17ExternalReference15wasm_word32_rorEv, .-_ZN2v88internal17ExternalReference15wasm_word32_rorEv
	.section	.text._ZN2v88internal17ExternalReference16wasm_memory_copyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference16wasm_memory_copyEv
	.type	_ZN2v88internal17ExternalReference16wasm_memory_copyEv, @function
_ZN2v88internal17ExternalReference16wasm_memory_copyEv:
.LFB25201:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm19memory_copy_wrapperEmmj@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25201:
	.size	_ZN2v88internal17ExternalReference16wasm_memory_copyEv, .-_ZN2v88internal17ExternalReference16wasm_memory_copyEv
	.section	.text._ZN2v88internal17ExternalReference16wasm_memory_fillEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference16wasm_memory_fillEv
	.type	_ZN2v88internal17ExternalReference16wasm_memory_fillEv, @function
_ZN2v88internal17ExternalReference16wasm_memory_fillEv:
.LFB25205:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm19memory_fill_wrapperEmjj@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25205:
	.size	_ZN2v88internal17ExternalReference16wasm_memory_fillEv, .-_ZN2v88internal17ExternalReference16wasm_memory_fillEv
	.section	.text._ZN2v88internal17ExternalReference25f64_acos_wrapper_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference25f64_acos_wrapper_functionEv
	.type	_ZN2v88internal17ExternalReference25f64_acos_wrapper_functionEv, @function
_ZN2v88internal17ExternalReference25f64_acos_wrapper_functionEv:
.LFB25210:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL16f64_acos_wrapperEm(%rip), %rax
	ret
	.cfi_endproc
.LFE25210:
	.size	_ZN2v88internal17ExternalReference25f64_acos_wrapper_functionEv, .-_ZN2v88internal17ExternalReference25f64_acos_wrapper_functionEv
	.section	.text._ZN2v88internal17ExternalReference25f64_asin_wrapper_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference25f64_asin_wrapper_functionEv
	.type	_ZN2v88internal17ExternalReference25f64_asin_wrapper_functionEv, @function
_ZN2v88internal17ExternalReference25f64_asin_wrapper_functionEv:
.LFB25212:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL16f64_asin_wrapperEm(%rip), %rax
	ret
	.cfi_endproc
.LFE25212:
	.size	_ZN2v88internal17ExternalReference25f64_asin_wrapper_functionEv, .-_ZN2v88internal17ExternalReference25f64_asin_wrapper_functionEv
	.section	.text._ZN2v88internal17ExternalReference16wasm_float64_powEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference16wasm_float64_powEv
	.type	_ZN2v88internal17ExternalReference16wasm_float64_powEv, @function
_ZN2v88internal17ExternalReference16wasm_float64_powEv:
.LFB25213:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm19float64_pow_wrapperEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25213:
	.size	_ZN2v88internal17ExternalReference16wasm_float64_powEv, .-_ZN2v88internal17ExternalReference16wasm_float64_powEv
	.section	.text._ZN2v88internal17ExternalReference24f64_mod_wrapper_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference24f64_mod_wrapper_functionEv
	.type	_ZN2v88internal17ExternalReference24f64_mod_wrapper_functionEv, @function
_ZN2v88internal17ExternalReference24f64_mod_wrapper_functionEv:
.LFB25215:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL15f64_mod_wrapperEm(%rip), %rax
	ret
	.cfi_endproc
.LFE25215:
	.size	_ZN2v88internal17ExternalReference24f64_mod_wrapper_functionEv, .-_ZN2v88internal17ExternalReference24f64_mod_wrapper_functionEv
	.section	.text._ZN2v88internal17ExternalReference35wasm_call_trap_callback_for_testingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference35wasm_call_trap_callback_for_testingEv
	.type	_ZN2v88internal17ExternalReference35wasm_call_trap_callback_for_testingEv, @function
_ZN2v88internal17ExternalReference35wasm_call_trap_callback_for_testingEv:
.LFB25216:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasm30call_trap_callback_for_testingEv@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25216:
	.size	_ZN2v88internal17ExternalReference35wasm_call_trap_callback_for_testingEv, .-_ZN2v88internal17ExternalReference35wasm_call_trap_callback_for_testingEv
	.section	.text._ZN2v88internal17ExternalReference12isolate_rootEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference12isolate_rootEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference12isolate_rootEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference12isolate_rootEPNS0_7IsolateE:
.LFB25217:
	.cfi_startproc
	endbr64
	leaq	128(%rdi), %rax
	ret
	.cfi_endproc
.LFE25217:
	.size	_ZN2v88internal17ExternalReference12isolate_rootEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference12isolate_rootEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference29allocation_sites_list_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference29allocation_sites_list_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference29allocation_sites_list_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference29allocation_sites_list_addressEPNS0_7IsolateE:
.LFB25218:
	.cfi_startproc
	endbr64
	leaq	39128(%rdi), %rax
	ret
	.cfi_endproc
.LFE25218:
	.size	_ZN2v88internal17ExternalReference29allocation_sites_list_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference29allocation_sites_list_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference18address_of_jslimitEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference18address_of_jslimitEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference18address_of_jslimitEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference18address_of_jslimitEPNS0_7IsolateE:
.LFB25219:
	.cfi_startproc
	endbr64
	leaq	37536(%rdi), %rax
	ret
	.cfi_endproc
.LFE25219:
	.size	_ZN2v88internal17ExternalReference18address_of_jslimitEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference18address_of_jslimitEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference23address_of_real_jslimitEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference23address_of_real_jslimitEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference23address_of_real_jslimitEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference23address_of_real_jslimitEPNS0_7IsolateE:
.LFB25220:
	.cfi_startproc
	endbr64
	leaq	37520(%rdi), %rax
	ret
	.cfi_endproc
.LFE25220:
	.size	_ZN2v88internal17ExternalReference23address_of_real_jslimitEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference23address_of_real_jslimitEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference16store_buffer_topEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference16store_buffer_topEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference16store_buffer_topEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference16store_buffer_topEPNS0_7IsolateE:
.LFB25221:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$37592, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal4Heap24store_buffer_top_addressEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25221:
	.size	_ZN2v88internal17ExternalReference16store_buffer_topEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference16store_buffer_topEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference28heap_is_marking_flag_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference28heap_is_marking_flag_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference28heap_is_marking_flag_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference28heap_is_marking_flag_addressEPNS0_7IsolateE:
.LFB25222:
	.cfi_startproc
	endbr64
	leaq	40328(%rdi), %rax
	ret
	.cfi_endproc
.LFE25222:
	.size	_ZN2v88internal17ExternalReference28heap_is_marking_flag_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference28heap_is_marking_flag_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference32new_space_allocation_top_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference32new_space_allocation_top_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference32new_space_allocation_top_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference32new_space_allocation_top_addressEPNS0_7IsolateE:
.LFB25223:
	.cfi_startproc
	endbr64
	movq	37840(%rdi), %rax
	addq	$104, %rax
	ret
	.cfi_endproc
.LFE25223:
	.size	_ZN2v88internal17ExternalReference32new_space_allocation_top_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference32new_space_allocation_top_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference34new_space_allocation_limit_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference34new_space_allocation_limit_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference34new_space_allocation_limit_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference34new_space_allocation_limit_addressEPNS0_7IsolateE:
.LFB25224:
	.cfi_startproc
	endbr64
	movq	37840(%rdi), %rax
	addq	$112, %rax
	ret
	.cfi_endproc
.LFE25224:
	.size	_ZN2v88internal17ExternalReference34new_space_allocation_limit_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference34new_space_allocation_limit_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference32old_space_allocation_top_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference32old_space_allocation_top_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference32old_space_allocation_top_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference32old_space_allocation_top_addressEPNS0_7IsolateE:
.LFB25225:
	.cfi_startproc
	endbr64
	movq	37848(%rdi), %rax
	addq	$104, %rax
	ret
	.cfi_endproc
.LFE25225:
	.size	_ZN2v88internal17ExternalReference32old_space_allocation_top_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference32old_space_allocation_top_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference34old_space_allocation_limit_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference34old_space_allocation_limit_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference34old_space_allocation_limit_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference34old_space_allocation_limit_addressEPNS0_7IsolateE:
.LFB25226:
	.cfi_startproc
	endbr64
	movq	37848(%rdi), %rax
	addq	$112, %rax
	ret
	.cfi_endproc
.LFE25226:
	.size	_ZN2v88internal17ExternalReference34old_space_allocation_limit_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference34old_space_allocation_limit_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference26handle_scope_level_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference26handle_scope_level_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference26handle_scope_level_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference26handle_scope_level_addressEPNS0_7IsolateE:
.LFB25227:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal11HandleScope21current_level_addressEPNS0_7IsolateE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25227:
	.size	_ZN2v88internal17ExternalReference26handle_scope_level_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference26handle_scope_level_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference25handle_scope_next_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference25handle_scope_next_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference25handle_scope_next_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference25handle_scope_next_addressEPNS0_7IsolateE:
.LFB25228:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal11HandleScope20current_next_addressEPNS0_7IsolateE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25228:
	.size	_ZN2v88internal17ExternalReference25handle_scope_next_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference25handle_scope_next_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference26handle_scope_limit_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference26handle_scope_limit_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference26handle_scope_limit_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference26handle_scope_limit_addressEPNS0_7IsolateE:
.LFB25229:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal11HandleScope21current_limit_addressEPNS0_7IsolateE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25229:
	.size	_ZN2v88internal17ExternalReference26handle_scope_limit_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference26handle_scope_limit_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference27scheduled_exception_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference27scheduled_exception_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference27scheduled_exception_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference27scheduled_exception_addressEPNS0_7IsolateE:
.LFB25230:
	.cfi_startproc
	endbr64
	leaq	12552(%rdi), %rax
	ret
	.cfi_endproc
.LFE25230:
	.size	_ZN2v88internal17ExternalReference27scheduled_exception_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference27scheduled_exception_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference30address_of_pending_message_objEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference30address_of_pending_message_objEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference30address_of_pending_message_objEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference30address_of_pending_message_objEPNS0_7IsolateE:
.LFB25231:
	.cfi_startproc
	endbr64
	leaq	12536(%rdi), %rax
	ret
	.cfi_endproc
.LFE25231:
	.size	_ZN2v88internal17ExternalReference30address_of_pending_message_objEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference30address_of_pending_message_objEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference17abort_with_reasonEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference17abort_with_reasonEv
	.type	_ZN2v88internal17ExternalReference17abort_with_reasonEv, @function
_ZN2v88internal17ExternalReference17abort_with_reasonEv:
.LFB25232:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internal17abort_with_reasonEi(%rip), %rax
	ret
	.cfi_endproc
.LFE25232:
	.size	_ZN2v88internal17ExternalReference17abort_with_reasonEv, .-_ZN2v88internal17ExternalReference17abort_with_reasonEv
	.section	.text._ZN2v88internal17ExternalReference18address_of_min_intEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference18address_of_min_intEv
	.type	_ZN2v88internal17ExternalReference18address_of_min_intEv, @function
_ZN2v88internal17ExternalReference18address_of_min_intEv:
.LFB25234:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL23double_min_int_constantE(%rip), %rax
	ret
	.cfi_endproc
.LFE25234:
	.size	_ZN2v88internal17ExternalReference18address_of_min_intEv, .-_ZN2v88internal17ExternalReference18address_of_min_intEv
	.section	.text._ZN2v88internal17ExternalReference42address_of_mock_arraybuffer_allocator_flagEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference42address_of_mock_arraybuffer_allocator_flagEv
	.type	_ZN2v88internal17ExternalReference42address_of_mock_arraybuffer_allocator_flagEv, @function
_ZN2v88internal17ExternalReference42address_of_mock_arraybuffer_allocator_flagEv:
.LFB25235:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internal31FLAG_mock_arraybuffer_allocatorE(%rip), %rax
	ret
	.cfi_endproc
.LFE25235:
	.size	_ZN2v88internal17ExternalReference42address_of_mock_arraybuffer_allocator_flagEv, .-_ZN2v88internal17ExternalReference42address_of_mock_arraybuffer_allocator_flagEv
	.section	.text._ZN2v88internal17ExternalReference29address_of_runtime_stats_flagEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference29address_of_runtime_stats_flagEv
	.type	_ZN2v88internal17ExternalReference29address_of_runtime_stats_flagEv, @function
_ZN2v88internal17ExternalReference29address_of_runtime_stats_flagEv:
.LFB25236:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %rax
	ret
	.cfi_endproc
.LFE25236:
	.size	_ZN2v88internal17ExternalReference29address_of_runtime_stats_flagEv, .-_ZN2v88internal17ExternalReference29address_of_runtime_stats_flagEv
	.section	.text._ZN2v88internal17ExternalReference19address_of_one_halfEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference19address_of_one_halfEv
	.type	_ZN2v88internal17ExternalReference19address_of_one_halfEv, @function
_ZN2v88internal17ExternalReference19address_of_one_halfEv:
.LFB25237:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL24double_one_half_constantE(%rip), %rax
	ret
	.cfi_endproc
.LFE25237:
	.size	_ZN2v88internal17ExternalReference19address_of_one_halfEv, .-_ZN2v88internal17ExternalReference19address_of_one_halfEv
	.section	.text._ZN2v88internal17ExternalReference23address_of_the_hole_nanEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference23address_of_the_hole_nanEv
	.type	_ZN2v88internal17ExternalReference23address_of_the_hole_nanEv, @function
_ZN2v88internal17ExternalReference23address_of_the_hole_nanEv:
.LFB25238:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL28double_the_hole_nan_constantE(%rip), %rax
	ret
	.cfi_endproc
.LFE25238:
	.size	_ZN2v88internal17ExternalReference23address_of_the_hole_nanEv, .-_ZN2v88internal17ExternalReference23address_of_the_hole_nanEv
	.section	.text._ZN2v88internal17ExternalReference22address_of_uint32_biasEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference22address_of_uint32_biasEv
	.type	_ZN2v88internal17ExternalReference22address_of_uint32_biasEv, @function
_ZN2v88internal17ExternalReference22address_of_uint32_biasEv:
.LFB25239:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL27double_uint32_bias_constantE(%rip), %rax
	ret
	.cfi_endproc
.LFE25239:
	.size	_ZN2v88internal17ExternalReference22address_of_uint32_biasEv, .-_ZN2v88internal17ExternalReference22address_of_uint32_biasEv
	.section	.text._ZN2v88internal17ExternalReference29address_of_float_abs_constantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference29address_of_float_abs_constantEv
	.type	_ZN2v88internal17ExternalReference29address_of_float_abs_constantEv, @function
_ZN2v88internal17ExternalReference29address_of_float_abs_constantEv:
.LFB25240:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL23float_absolute_constantE(%rip), %rax
	ret
	.cfi_endproc
.LFE25240:
	.size	_ZN2v88internal17ExternalReference29address_of_float_abs_constantEv, .-_ZN2v88internal17ExternalReference29address_of_float_abs_constantEv
	.section	.text._ZN2v88internal17ExternalReference29address_of_float_neg_constantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference29address_of_float_neg_constantEv
	.type	_ZN2v88internal17ExternalReference29address_of_float_neg_constantEv, @function
_ZN2v88internal17ExternalReference29address_of_float_neg_constantEv:
.LFB25241:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL21float_negate_constantE(%rip), %rax
	ret
	.cfi_endproc
.LFE25241:
	.size	_ZN2v88internal17ExternalReference29address_of_float_neg_constantEv, .-_ZN2v88internal17ExternalReference29address_of_float_neg_constantEv
	.section	.text._ZN2v88internal17ExternalReference30address_of_double_abs_constantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference30address_of_double_abs_constantEv
	.type	_ZN2v88internal17ExternalReference30address_of_double_abs_constantEv, @function
_ZN2v88internal17ExternalReference30address_of_double_abs_constantEv:
.LFB25242:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL24double_absolute_constantE(%rip), %rax
	ret
	.cfi_endproc
.LFE25242:
	.size	_ZN2v88internal17ExternalReference30address_of_double_abs_constantEv, .-_ZN2v88internal17ExternalReference30address_of_double_abs_constantEv
	.section	.text._ZN2v88internal17ExternalReference30address_of_double_neg_constantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference30address_of_double_neg_constantEv
	.type	_ZN2v88internal17ExternalReference30address_of_double_neg_constantEv, @function
_ZN2v88internal17ExternalReference30address_of_double_neg_constantEv:
.LFB25243:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL22double_negate_constantE(%rip), %rax
	ret
	.cfi_endproc
.LFE25243:
	.size	_ZN2v88internal17ExternalReference30address_of_double_neg_constantEv, .-_ZN2v88internal17ExternalReference30address_of_double_neg_constantEv
	.section	.text._ZN2v88internal17ExternalReference20is_profiling_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference20is_profiling_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference20is_profiling_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference20is_profiling_addressEPNS0_7IsolateE:
.LFB25244:
	.cfi_startproc
	endbr64
	leaq	41812(%rdi), %rax
	ret
	.cfi_endproc
.LFE25244:
	.size	_ZN2v88internal17ExternalReference20is_profiling_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference20is_profiling_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference24invoke_function_callbackEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference24invoke_function_callbackEv
	.type	_ZN2v88internal17ExternalReference24invoke_function_callbackEv, @function
_ZN2v88internal17ExternalReference24invoke_function_callbackEv:
.LFB25245:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal22InvokeFunctionCallbackERKNS_20FunctionCallbackInfoINS_5ValueEEEPFvS5_E@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25245:
	.size	_ZN2v88internal17ExternalReference24invoke_function_callbackEv, .-_ZN2v88internal17ExternalReference24invoke_function_callbackEv
	.section	.text._ZN2v88internal17ExternalReference31invoke_accessor_getter_callbackEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference31invoke_accessor_getter_callbackEv
	.type	_ZN2v88internal17ExternalReference31invoke_accessor_getter_callbackEv, @function
_ZN2v88internal17ExternalReference31invoke_accessor_getter_callbackEv:
.LFB25246:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal28InvokeAccessorGetterCallbackENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEEPFvS3_S8_E@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25246:
	.size	_ZN2v88internal17ExternalReference31invoke_accessor_getter_callbackEv, .-_ZN2v88internal17ExternalReference31invoke_accessor_getter_callbackEv
	.section	.text._ZN2v88internal17ExternalReference26re_check_stack_guard_stateEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference26re_check_stack_guard_stateEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference26re_check_stack_guard_stateEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference26re_check_stack_guard_stateEPNS0_7IsolateE:
.LFB25247:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal23RegExpMacroAssemblerX6420CheckStackGuardStateEPmmm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25247:
	.size	_ZN2v88internal17ExternalReference26re_check_stack_guard_stateEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference26re_check_stack_guard_stateEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference13re_grow_stackEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference13re_grow_stackEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference13re_grow_stackEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference13re_grow_stackEPNS0_7IsolateE:
.LFB25251:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal26NativeRegExpMacroAssembler9GrowStackEmPmPNS0_7IsolateE@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25251:
	.size	_ZN2v88internal17ExternalReference13re_grow_stackEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference13re_grow_stackEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference25re_match_for_call_from_jsEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference25re_match_for_call_from_jsEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference25re_match_for_call_from_jsEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference25re_match_for_call_from_jsEPNS0_7IsolateE:
.LFB25255:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal19IrregexpInterpreter18MatchForCallFromJsEmimmPiimNS0_6RegExp10CallOriginEPNS0_7IsolateEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25255:
	.size	_ZN2v88internal17ExternalReference25re_match_for_call_from_jsEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference25re_match_for_call_from_jsEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference32re_case_insensitive_compare_uc16EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference32re_case_insensitive_compare_uc16EPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference32re_case_insensitive_compare_uc16EPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference32re_case_insensitive_compare_uc16EPNS0_7IsolateE:
.LFB25269:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal20RegExpMacroAssembler26CaseInsensitiveCompareUC16EmmmPNS0_7IsolateE@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25269:
	.size	_ZN2v88internal17ExternalReference32re_case_insensitive_compare_uc16EPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference32re_case_insensitive_compare_uc16EPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference21re_word_character_mapEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference21re_word_character_mapEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference21re_word_character_mapEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference21re_word_character_mapEPNS0_7IsolateE:
.LFB25272:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internal26NativeRegExpMacroAssembler18word_character_mapE(%rip), %rax
	ret
	.cfi_endproc
.LFE25272:
	.size	_ZN2v88internal17ExternalReference21re_word_character_mapEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference21re_word_character_mapEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference32address_of_static_offsets_vectorEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference32address_of_static_offsets_vectorEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference32address_of_static_offsets_vectorEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference32address_of_static_offsets_vectorEPNS0_7IsolateE:
.LFB25273:
	.cfi_startproc
	endbr64
	leaq	41860(%rdi), %rax
	ret
	.cfi_endproc
.LFE25273:
	.size	_ZN2v88internal17ExternalReference32address_of_static_offsets_vectorEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference32address_of_static_offsets_vectorEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference37address_of_regexp_stack_limit_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference37address_of_regexp_stack_limit_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference37address_of_regexp_stack_limit_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference37address_of_regexp_stack_limit_addressEPNS0_7IsolateE:
.LFB25274:
	.cfi_startproc
	endbr64
	movq	41208(%rdi), %rax
	addq	$24, %rax
	ret
	.cfi_endproc
.LFE25274:
	.size	_ZN2v88internal17ExternalReference37address_of_regexp_stack_limit_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference37address_of_regexp_stack_limit_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference38address_of_regexp_stack_memory_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference38address_of_regexp_stack_memory_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference38address_of_regexp_stack_memory_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference38address_of_regexp_stack_memory_addressEPNS0_7IsolateE:
.LFB25275:
	.cfi_startproc
	endbr64
	movq	41208(%rdi), %rax
	ret
	.cfi_endproc
.LFE25275:
	.size	_ZN2v88internal17ExternalReference38address_of_regexp_stack_memory_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference38address_of_regexp_stack_memory_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference35address_of_regexp_stack_memory_sizeEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference35address_of_regexp_stack_memory_sizeEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference35address_of_regexp_stack_memory_sizeEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference35address_of_regexp_stack_memory_sizeEPNS0_7IsolateE:
.LFB25276:
	.cfi_startproc
	endbr64
	movq	41208(%rdi), %rax
	addq	$16, %rax
	ret
	.cfi_endproc
.LFE25276:
	.size	_ZN2v88internal17ExternalReference35address_of_regexp_stack_memory_sizeEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference35address_of_regexp_stack_memory_sizeEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference42address_of_regexp_stack_memory_top_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference42address_of_regexp_stack_memory_top_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference42address_of_regexp_stack_memory_top_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference42address_of_regexp_stack_memory_top_addressEPNS0_7IsolateE:
.LFB25277:
	.cfi_startproc
	endbr64
	movq	41208(%rdi), %rax
	addq	$8, %rax
	ret
	.cfi_endproc
.LFE25277:
	.size	_ZN2v88internal17ExternalReference42address_of_regexp_stack_memory_top_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference42address_of_regexp_stack_memory_top_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference21ieee754_acos_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference21ieee754_acos_functionEv
	.type	_ZN2v88internal17ExternalReference21ieee754_acos_functionEv, @function
_ZN2v88internal17ExternalReference21ieee754_acos_functionEv:
.LFB25278:
	.cfi_startproc
	endbr64
	movq	_ZN2v84base7ieee7544acosEd@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25278:
	.size	_ZN2v88internal17ExternalReference21ieee754_acos_functionEv, .-_ZN2v88internal17ExternalReference21ieee754_acos_functionEv
	.section	.text._ZN2v88internal17ExternalReference22ieee754_acosh_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference22ieee754_acosh_functionEv
	.type	_ZN2v88internal17ExternalReference22ieee754_acosh_functionEv, @function
_ZN2v88internal17ExternalReference22ieee754_acosh_functionEv:
.LFB25281:
	.cfi_startproc
	endbr64
	movq	_ZN2v84base7ieee7545acoshEd@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25281:
	.size	_ZN2v88internal17ExternalReference22ieee754_acosh_functionEv, .-_ZN2v88internal17ExternalReference22ieee754_acosh_functionEv
	.section	.text._ZN2v88internal17ExternalReference21ieee754_asin_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference21ieee754_asin_functionEv
	.type	_ZN2v88internal17ExternalReference21ieee754_asin_functionEv, @function
_ZN2v88internal17ExternalReference21ieee754_asin_functionEv:
.LFB25282:
	.cfi_startproc
	endbr64
	movq	_ZN2v84base7ieee7544asinEd@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25282:
	.size	_ZN2v88internal17ExternalReference21ieee754_asin_functionEv, .-_ZN2v88internal17ExternalReference21ieee754_asin_functionEv
	.section	.text._ZN2v88internal17ExternalReference22ieee754_asinh_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference22ieee754_asinh_functionEv
	.type	_ZN2v88internal17ExternalReference22ieee754_asinh_functionEv, @function
_ZN2v88internal17ExternalReference22ieee754_asinh_functionEv:
.LFB25283:
	.cfi_startproc
	endbr64
	movq	_ZN2v84base7ieee7545asinhEd@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25283:
	.size	_ZN2v88internal17ExternalReference22ieee754_asinh_functionEv, .-_ZN2v88internal17ExternalReference22ieee754_asinh_functionEv
	.section	.text._ZN2v88internal17ExternalReference21ieee754_atan_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference21ieee754_atan_functionEv
	.type	_ZN2v88internal17ExternalReference21ieee754_atan_functionEv, @function
_ZN2v88internal17ExternalReference21ieee754_atan_functionEv:
.LFB25284:
	.cfi_startproc
	endbr64
	movq	_ZN2v84base7ieee7544atanEd@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25284:
	.size	_ZN2v88internal17ExternalReference21ieee754_atan_functionEv, .-_ZN2v88internal17ExternalReference21ieee754_atan_functionEv
	.section	.text._ZN2v88internal17ExternalReference22ieee754_atanh_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference22ieee754_atanh_functionEv
	.type	_ZN2v88internal17ExternalReference22ieee754_atanh_functionEv, @function
_ZN2v88internal17ExternalReference22ieee754_atanh_functionEv:
.LFB25285:
	.cfi_startproc
	endbr64
	movq	_ZN2v84base7ieee7545atanhEd@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25285:
	.size	_ZN2v88internal17ExternalReference22ieee754_atanh_functionEv, .-_ZN2v88internal17ExternalReference22ieee754_atanh_functionEv
	.section	.text._ZN2v88internal17ExternalReference22ieee754_atan2_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference22ieee754_atan2_functionEv
	.type	_ZN2v88internal17ExternalReference22ieee754_atan2_functionEv, @function
_ZN2v88internal17ExternalReference22ieee754_atan2_functionEv:
.LFB25286:
	.cfi_startproc
	endbr64
	movq	_ZN2v84base7ieee7545atan2Edd@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25286:
	.size	_ZN2v88internal17ExternalReference22ieee754_atan2_functionEv, .-_ZN2v88internal17ExternalReference22ieee754_atan2_functionEv
	.section	.text._ZN2v88internal17ExternalReference21ieee754_cbrt_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference21ieee754_cbrt_functionEv
	.type	_ZN2v88internal17ExternalReference21ieee754_cbrt_functionEv, @function
_ZN2v88internal17ExternalReference21ieee754_cbrt_functionEv:
.LFB25288:
	.cfi_startproc
	endbr64
	movq	_ZN2v84base7ieee7544cbrtEd@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25288:
	.size	_ZN2v88internal17ExternalReference21ieee754_cbrt_functionEv, .-_ZN2v88internal17ExternalReference21ieee754_cbrt_functionEv
	.section	.text._ZN2v88internal17ExternalReference20ieee754_cos_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference20ieee754_cos_functionEv
	.type	_ZN2v88internal17ExternalReference20ieee754_cos_functionEv, @function
_ZN2v88internal17ExternalReference20ieee754_cos_functionEv:
.LFB25289:
	.cfi_startproc
	endbr64
	movq	_ZN2v84base7ieee7543cosEd@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25289:
	.size	_ZN2v88internal17ExternalReference20ieee754_cos_functionEv, .-_ZN2v88internal17ExternalReference20ieee754_cos_functionEv
	.section	.text._ZN2v88internal17ExternalReference21ieee754_cosh_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference21ieee754_cosh_functionEv
	.type	_ZN2v88internal17ExternalReference21ieee754_cosh_functionEv, @function
_ZN2v88internal17ExternalReference21ieee754_cosh_functionEv:
.LFB25290:
	.cfi_startproc
	endbr64
	movq	_ZN2v84base7ieee7544coshEd@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25290:
	.size	_ZN2v88internal17ExternalReference21ieee754_cosh_functionEv, .-_ZN2v88internal17ExternalReference21ieee754_cosh_functionEv
	.section	.text._ZN2v88internal17ExternalReference20ieee754_exp_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference20ieee754_exp_functionEv
	.type	_ZN2v88internal17ExternalReference20ieee754_exp_functionEv, @function
_ZN2v88internal17ExternalReference20ieee754_exp_functionEv:
.LFB25291:
	.cfi_startproc
	endbr64
	movq	_ZN2v84base7ieee7543expEd@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25291:
	.size	_ZN2v88internal17ExternalReference20ieee754_exp_functionEv, .-_ZN2v88internal17ExternalReference20ieee754_exp_functionEv
	.section	.text._ZN2v88internal17ExternalReference22ieee754_expm1_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference22ieee754_expm1_functionEv
	.type	_ZN2v88internal17ExternalReference22ieee754_expm1_functionEv, @function
_ZN2v88internal17ExternalReference22ieee754_expm1_functionEv:
.LFB25292:
	.cfi_startproc
	endbr64
	movq	_ZN2v84base7ieee7545expm1Ed@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25292:
	.size	_ZN2v88internal17ExternalReference22ieee754_expm1_functionEv, .-_ZN2v88internal17ExternalReference22ieee754_expm1_functionEv
	.section	.text._ZN2v88internal17ExternalReference20ieee754_log_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference20ieee754_log_functionEv
	.type	_ZN2v88internal17ExternalReference20ieee754_log_functionEv, @function
_ZN2v88internal17ExternalReference20ieee754_log_functionEv:
.LFB25293:
	.cfi_startproc
	endbr64
	movq	_ZN2v84base7ieee7543logEd@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25293:
	.size	_ZN2v88internal17ExternalReference20ieee754_log_functionEv, .-_ZN2v88internal17ExternalReference20ieee754_log_functionEv
	.section	.text._ZN2v88internal17ExternalReference22ieee754_log1p_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference22ieee754_log1p_functionEv
	.type	_ZN2v88internal17ExternalReference22ieee754_log1p_functionEv, @function
_ZN2v88internal17ExternalReference22ieee754_log1p_functionEv:
.LFB25294:
	.cfi_startproc
	endbr64
	movq	_ZN2v84base7ieee7545log1pEd@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25294:
	.size	_ZN2v88internal17ExternalReference22ieee754_log1p_functionEv, .-_ZN2v88internal17ExternalReference22ieee754_log1p_functionEv
	.section	.text._ZN2v88internal17ExternalReference22ieee754_log10_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference22ieee754_log10_functionEv
	.type	_ZN2v88internal17ExternalReference22ieee754_log10_functionEv, @function
_ZN2v88internal17ExternalReference22ieee754_log10_functionEv:
.LFB25295:
	.cfi_startproc
	endbr64
	movq	_ZN2v84base7ieee7545log10Ed@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25295:
	.size	_ZN2v88internal17ExternalReference22ieee754_log10_functionEv, .-_ZN2v88internal17ExternalReference22ieee754_log10_functionEv
	.section	.text._ZN2v88internal17ExternalReference21ieee754_log2_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference21ieee754_log2_functionEv
	.type	_ZN2v88internal17ExternalReference21ieee754_log2_functionEv, @function
_ZN2v88internal17ExternalReference21ieee754_log2_functionEv:
.LFB25296:
	.cfi_startproc
	endbr64
	movq	_ZN2v84base7ieee7544log2Ed@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25296:
	.size	_ZN2v88internal17ExternalReference21ieee754_log2_functionEv, .-_ZN2v88internal17ExternalReference21ieee754_log2_functionEv
	.section	.text._ZN2v88internal17ExternalReference20ieee754_sin_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference20ieee754_sin_functionEv
	.type	_ZN2v88internal17ExternalReference20ieee754_sin_functionEv, @function
_ZN2v88internal17ExternalReference20ieee754_sin_functionEv:
.LFB25297:
	.cfi_startproc
	endbr64
	movq	_ZN2v84base7ieee7543sinEd@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25297:
	.size	_ZN2v88internal17ExternalReference20ieee754_sin_functionEv, .-_ZN2v88internal17ExternalReference20ieee754_sin_functionEv
	.section	.text._ZN2v88internal17ExternalReference21ieee754_sinh_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference21ieee754_sinh_functionEv
	.type	_ZN2v88internal17ExternalReference21ieee754_sinh_functionEv, @function
_ZN2v88internal17ExternalReference21ieee754_sinh_functionEv:
.LFB25298:
	.cfi_startproc
	endbr64
	movq	_ZN2v84base7ieee7544sinhEd@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25298:
	.size	_ZN2v88internal17ExternalReference21ieee754_sinh_functionEv, .-_ZN2v88internal17ExternalReference21ieee754_sinh_functionEv
	.section	.text._ZN2v88internal17ExternalReference20ieee754_tan_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference20ieee754_tan_functionEv
	.type	_ZN2v88internal17ExternalReference20ieee754_tan_functionEv, @function
_ZN2v88internal17ExternalReference20ieee754_tan_functionEv:
.LFB25299:
	.cfi_startproc
	endbr64
	movq	_ZN2v84base7ieee7543tanEd@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25299:
	.size	_ZN2v88internal17ExternalReference20ieee754_tan_functionEv, .-_ZN2v88internal17ExternalReference20ieee754_tan_functionEv
	.section	.text._ZN2v88internal17ExternalReference21ieee754_tanh_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference21ieee754_tanh_functionEv
	.type	_ZN2v88internal17ExternalReference21ieee754_tanh_functionEv, @function
_ZN2v88internal17ExternalReference21ieee754_tanh_functionEv:
.LFB25300:
	.cfi_startproc
	endbr64
	movq	_ZN2v84base7ieee7544tanhEd@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25300:
	.size	_ZN2v88internal17ExternalReference21ieee754_tanh_functionEv, .-_ZN2v88internal17ExternalReference21ieee754_tanh_functionEv
	.section	.text._ZN2v88internal17ExternalReference20ieee754_pow_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference20ieee754_pow_functionEv
	.type	_ZN2v88internal17ExternalReference20ieee754_pow_functionEv, @function
_ZN2v88internal17ExternalReference20ieee754_pow_functionEv:
.LFB25301:
	.cfi_startproc
	endbr64
	movq	_ZN2v84base7ieee7543powEdd@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25301:
	.size	_ZN2v88internal17ExternalReference20ieee754_pow_functionEv, .-_ZN2v88internal17ExternalReference20ieee754_pow_functionEv
	.section	.text._ZN2v88internal17ExternalReference20libc_memchr_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference20libc_memchr_functionEv
	.type	_ZN2v88internal17ExternalReference20libc_memchr_functionEv, @function
_ZN2v88internal17ExternalReference20libc_memchr_functionEv:
.LFB25303:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internal11libc_memchrEPvim(%rip), %rax
	ret
	.cfi_endproc
.LFE25303:
	.size	_ZN2v88internal17ExternalReference20libc_memchr_functionEv, .-_ZN2v88internal17ExternalReference20libc_memchr_functionEv
	.section	.text._ZN2v88internal17ExternalReference20libc_memcpy_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference20libc_memcpy_functionEv
	.type	_ZN2v88internal17ExternalReference20libc_memcpy_functionEv, @function
_ZN2v88internal17ExternalReference20libc_memcpy_functionEv:
.LFB25308:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internal11libc_memcpyEPvPKvm(%rip), %rax
	ret
	.cfi_endproc
.LFE25308:
	.size	_ZN2v88internal17ExternalReference20libc_memcpy_functionEv, .-_ZN2v88internal17ExternalReference20libc_memcpy_functionEv
	.section	.text._ZN2v88internal17ExternalReference21libc_memmove_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference21libc_memmove_functionEv
	.type	_ZN2v88internal17ExternalReference21libc_memmove_functionEv, @function
_ZN2v88internal17ExternalReference21libc_memmove_functionEv:
.LFB25314:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internal12libc_memmoveEPvPKvm(%rip), %rax
	ret
	.cfi_endproc
.LFE25314:
	.size	_ZN2v88internal17ExternalReference21libc_memmove_functionEv, .-_ZN2v88internal17ExternalReference21libc_memmove_functionEv
	.section	.text._ZN2v88internal17ExternalReference20libc_memset_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference20libc_memset_functionEv
	.type	_ZN2v88internal17ExternalReference20libc_memset_functionEv, @function
_ZN2v88internal17ExternalReference20libc_memset_functionEv:
.LFB25316:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internal11libc_memsetEPvim(%rip), %rax
	ret
	.cfi_endproc
.LFE25316:
	.size	_ZN2v88internal17ExternalReference20libc_memset_functionEv, .-_ZN2v88internal17ExternalReference20libc_memset_functionEv
	.section	.text._ZN2v88internal17ExternalReference15printf_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference15printf_functionEv
	.type	_ZN2v88internal17ExternalReference15printf_functionEv, @function
_ZN2v88internal17ExternalReference15printf_functionEv:
.LFB25317:
	.cfi_startproc
	endbr64
	movq	printf@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25317:
	.size	_ZN2v88internal17ExternalReference15printf_functionEv, .-_ZN2v88internal17ExternalReference15printf_functionEv
	.section	.text._ZN2v88internal17ExternalReference18refill_math_randomEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference18refill_math_randomEv
	.type	_ZN2v88internal17ExternalReference18refill_math_randomEv, @function
_ZN2v88internal17ExternalReference18refill_math_randomEv:
.LFB25318:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal10MathRandom11RefillCacheEPNS0_7IsolateEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25318:
	.size	_ZN2v88internal17ExternalReference18refill_math_randomEv, .-_ZN2v88internal17ExternalReference18refill_math_randomEv
	.section	.text._ZN2v88internal17ExternalReference46jsarray_array_join_concat_to_sequential_stringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference46jsarray_array_join_concat_to_sequential_stringEv
	.type	_ZN2v88internal17ExternalReference46jsarray_array_join_concat_to_sequential_stringEv, @function
_ZN2v88internal17ExternalReference46jsarray_array_join_concat_to_sequential_stringEv:
.LFB25321:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal7JSArray33ArrayJoinConcatToSequentialStringEPNS0_7IsolateEmlmm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25321:
	.size	_ZN2v88internal17ExternalReference46jsarray_array_join_concat_to_sequential_stringEv, .-_ZN2v88internal17ExternalReference46jsarray_array_join_concat_to_sequential_stringEv
	.section	.text._ZN2v88internal17ExternalReference26orderedhashmap_gethash_rawEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference26orderedhashmap_gethash_rawEv
	.type	_ZN2v88internal17ExternalReference26orderedhashmap_gethash_rawEv, @function
_ZN2v88internal17ExternalReference26orderedhashmap_gethash_rawEv:
.LFB25331:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal14OrderedHashMap7GetHashEPNS0_7IsolateEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25331:
	.size	_ZN2v88internal17ExternalReference26orderedhashmap_gethash_rawEv, .-_ZN2v88internal17ExternalReference26orderedhashmap_gethash_rawEv
	.section	.text._ZN2v88internal17ExternalReference22get_or_create_hash_rawEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference22get_or_create_hash_rawEv
	.type	_ZN2v88internal17ExternalReference22get_or_create_hash_rawEv, @function
_ZN2v88internal17ExternalReference22get_or_create_hash_rawEv:
.LFB25333:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internal15GetOrCreateHashEPNS0_7IsolateEm(%rip), %rax
	ret
	.cfi_endproc
.LFE25333:
	.size	_ZN2v88internal17ExternalReference22get_or_create_hash_rawEv, .-_ZN2v88internal17ExternalReference22get_or_create_hash_rawEv
	.section	.text._ZN2v88internal17ExternalReference31jsreceiver_create_identity_hashEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference31jsreceiver_create_identity_hashEv
	.type	_ZN2v88internal17ExternalReference31jsreceiver_create_identity_hashEv, @function
_ZN2v88internal17ExternalReference31jsreceiver_create_identity_hashEv:
.LFB25335:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL28JSReceiverCreateIdentityHashEPNS0_7IsolateEm(%rip), %rax
	ret
	.cfi_endproc
.LFE25335:
	.size	_ZN2v88internal17ExternalReference31jsreceiver_create_identity_hashEv, .-_ZN2v88internal17ExternalReference31jsreceiver_create_identity_hashEv
	.section	.text._ZN2v88internal17ExternalReference20compute_integer_hashEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference20compute_integer_hashEv
	.type	_ZN2v88internal17ExternalReference20compute_integer_hashEv, @function
_ZN2v88internal17ExternalReference20compute_integer_hashEv:
.LFB25337:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL24ComputeSeededIntegerHashEPNS0_7IsolateEj(%rip), %rax
	ret
	.cfi_endproc
.LFE25337:
	.size	_ZN2v88internal17ExternalReference20compute_integer_hashEv, .-_ZN2v88internal17ExternalReference20compute_integer_hashEv
	.section	.text._ZN2v88internal17ExternalReference48copy_fast_number_jsarray_elements_to_typed_arrayEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference48copy_fast_number_jsarray_elements_to_typed_arrayEv
	.type	_ZN2v88internal17ExternalReference48copy_fast_number_jsarray_elements_to_typed_arrayEv, @function
_ZN2v88internal17ExternalReference48copy_fast_number_jsarray_elements_to_typed_arrayEv:
.LFB25340:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal41CopyFastNumberJSArrayElementsToTypedArrayEmmmmm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25340:
	.size	_ZN2v88internal17ExternalReference48copy_fast_number_jsarray_elements_to_typed_arrayEv, .-_ZN2v88internal17ExternalReference48copy_fast_number_jsarray_elements_to_typed_arrayEv
	.section	.text._ZN2v88internal17ExternalReference40copy_typed_array_elements_to_typed_arrayEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference40copy_typed_array_elements_to_typed_arrayEv
	.type	_ZN2v88internal17ExternalReference40copy_typed_array_elements_to_typed_arrayEv, @function
_ZN2v88internal17ExternalReference40copy_typed_array_elements_to_typed_arrayEv:
.LFB25344:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal34CopyTypedArrayElementsToTypedArrayEmmmm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25344:
	.size	_ZN2v88internal17ExternalReference40copy_typed_array_elements_to_typed_arrayEv, .-_ZN2v88internal17ExternalReference40copy_typed_array_elements_to_typed_arrayEv
	.section	.text._ZN2v88internal17ExternalReference31copy_typed_array_elements_sliceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference31copy_typed_array_elements_sliceEv
	.type	_ZN2v88internal17ExternalReference31copy_typed_array_elements_sliceEv, @function
_ZN2v88internal17ExternalReference31copy_typed_array_elements_sliceEv:
.LFB25346:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal27CopyTypedArrayElementsSliceEmmmm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25346:
	.size	_ZN2v88internal17ExternalReference31copy_typed_array_elements_sliceEv, .-_ZN2v88internal17ExternalReference31copy_typed_array_elements_sliceEv
	.section	.text._ZN2v88internal17ExternalReference31try_internalize_string_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference31try_internalize_string_functionEv
	.type	_ZN2v88internal17ExternalReference31try_internalize_string_functionEv, @function
_ZN2v88internal17ExternalReference31try_internalize_string_functionEv:
.LFB25347:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal11StringTable31LookupStringIfExists_NoAllocateEPNS0_7IsolateEm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25347:
	.size	_ZN2v88internal17ExternalReference31try_internalize_string_functionEv, .-_ZN2v88internal17ExternalReference31try_internalize_string_functionEv
	.section	.text._ZN2v88internal17ExternalReference34smi_lexicographic_compare_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference34smi_lexicographic_compare_functionEv
	.type	_ZN2v88internal17ExternalReference34smi_lexicographic_compare_functionEv, @function
_ZN2v88internal17ExternalReference34smi_lexicographic_compare_functionEv:
.LFB25349:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL27LexicographicCompareWrapperEPNS0_7IsolateEmm(%rip), %rax
	ret
	.cfi_endproc
.LFE25349:
	.size	_ZN2v88internal17ExternalReference34smi_lexicographic_compare_functionEv, .-_ZN2v88internal17ExternalReference34smi_lexicographic_compare_functionEv
	.section	.text._ZN2v88internal17ExternalReference54mutable_big_int_absolute_add_and_canonicalize_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference54mutable_big_int_absolute_add_and_canonicalize_functionEv
	.type	_ZN2v88internal17ExternalReference54mutable_big_int_absolute_add_and_canonicalize_functionEv, @function
_ZN2v88internal17ExternalReference54mutable_big_int_absolute_add_and_canonicalize_functionEv:
.LFB25352:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal40MutableBigInt_AbsoluteAddAndCanonicalizeEmmm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25352:
	.size	_ZN2v88internal17ExternalReference54mutable_big_int_absolute_add_and_canonicalize_functionEv, .-_ZN2v88internal17ExternalReference54mutable_big_int_absolute_add_and_canonicalize_functionEv
	.section	.text._ZN2v88internal17ExternalReference41mutable_big_int_absolute_compare_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference41mutable_big_int_absolute_compare_functionEv
	.type	_ZN2v88internal17ExternalReference41mutable_big_int_absolute_compare_functionEv, @function
_ZN2v88internal17ExternalReference41mutable_big_int_absolute_compare_functionEv:
.LFB25354:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal29MutableBigInt_AbsoluteCompareEmm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25354:
	.size	_ZN2v88internal17ExternalReference41mutable_big_int_absolute_compare_functionEv, .-_ZN2v88internal17ExternalReference41mutable_big_int_absolute_compare_functionEv
	.section	.text._ZN2v88internal17ExternalReference54mutable_big_int_absolute_sub_and_canonicalize_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference54mutable_big_int_absolute_sub_and_canonicalize_functionEv
	.type	_ZN2v88internal17ExternalReference54mutable_big_int_absolute_sub_and_canonicalize_functionEv, @function
_ZN2v88internal17ExternalReference54mutable_big_int_absolute_sub_and_canonicalize_functionEv:
.LFB25356:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal40MutableBigInt_AbsoluteSubAndCanonicalizeEmmm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25356:
	.size	_ZN2v88internal17ExternalReference54mutable_big_int_absolute_sub_and_canonicalize_functionEv, .-_ZN2v88internal17ExternalReference54mutable_big_int_absolute_sub_and_canonicalize_functionEv
	.section	.text._ZN2v88internal17ExternalReference17check_object_typeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference17check_object_typeEv
	.type	_ZN2v88internal17ExternalReference17check_object_typeEv, @function
_ZN2v88internal17ExternalReference17check_object_typeEv:
.LFB25357:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal15CheckObjectTypeEmmm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25357:
	.size	_ZN2v88internal17ExternalReference17check_object_typeEv, .-_ZN2v88internal17ExternalReference17check_object_typeEv
	.section	.text._ZN2v88internal17ExternalReference30intl_convert_one_byte_to_lowerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference30intl_convert_one_byte_to_lowerEv
	.type	_ZN2v88internal17ExternalReference30intl_convert_one_byte_to_lowerEv, @function
_ZN2v88internal17ExternalReference30intl_convert_one_byte_to_lowerEv:
.LFB25359:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL21ConvertOneByteToLowerEmm(%rip), %rax
	ret
	.cfi_endproc
.LFE25359:
	.size	_ZN2v88internal17ExternalReference30intl_convert_one_byte_to_lowerEv, .-_ZN2v88internal17ExternalReference30intl_convert_one_byte_to_lowerEv
	.section	.text._ZN2v88internal17ExternalReference26intl_to_latin1_lower_tableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference26intl_to_latin1_lower_tableEv
	.type	_ZN2v88internal17ExternalReference26intl_to_latin1_lower_tableEv, @function
_ZN2v88internal17ExternalReference26intl_to_latin1_lower_tableEv:
.LFB25360:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal4Intl18ToLatin1LowerTableEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25360:
	.size	_ZN2v88internal17ExternalReference26intl_to_latin1_lower_tableEv, .-_ZN2v88internal17ExternalReference26intl_to_latin1_lower_tableEv
	.section	.text._ZN2v88internal17ExternalReference17search_string_rawIKhS3_EES1_v,"axG",@progbits,_ZN2v88internal17ExternalReference17search_string_rawIKhS3_EES1_v,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17ExternalReference17search_string_rawIKhS3_EES1_v
	.type	_ZN2v88internal17ExternalReference17search_string_rawIKhS3_EES1_v, @function
_ZN2v88internal17ExternalReference17search_string_rawIKhS3_EES1_v:
.LFB27796:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internal15SearchStringRawIKhS2_EElPNS0_7IsolateEPKT_iPKT0_ii(%rip), %rax
	ret
	.cfi_endproc
.LFE27796:
	.size	_ZN2v88internal17ExternalReference17search_string_rawIKhS3_EES1_v, .-_ZN2v88internal17ExternalReference17search_string_rawIKhS3_EES1_v
	.section	.text._ZN2v88internal17ExternalReference25search_string_raw_one_oneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference25search_string_raw_one_oneEv
	.type	_ZN2v88internal17ExternalReference25search_string_raw_one_oneEv, @function
_ZN2v88internal17ExternalReference25search_string_raw_one_oneEv:
.LFB25327:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal17ExternalReference17search_string_rawIKhS3_EES1_v
	.cfi_endproc
.LFE25327:
	.size	_ZN2v88internal17ExternalReference25search_string_raw_one_oneEv, .-_ZN2v88internal17ExternalReference25search_string_raw_one_oneEv
	.section	.text._ZN2v88internal17ExternalReference17search_string_rawIKhKtEES1_v,"axG",@progbits,_ZN2v88internal17ExternalReference17search_string_rawIKhKtEES1_v,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17ExternalReference17search_string_rawIKhKtEES1_v
	.type	_ZN2v88internal17ExternalReference17search_string_rawIKhKtEES1_v, @function
_ZN2v88internal17ExternalReference17search_string_rawIKhKtEES1_v:
.LFB27797:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internal15SearchStringRawIKhKtEElPNS0_7IsolateEPKT_iPKT0_ii(%rip), %rax
	ret
	.cfi_endproc
.LFE27797:
	.size	_ZN2v88internal17ExternalReference17search_string_rawIKhKtEES1_v, .-_ZN2v88internal17ExternalReference17search_string_rawIKhKtEES1_v
	.section	.text._ZN2v88internal17ExternalReference25search_string_raw_one_twoEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference25search_string_raw_one_twoEv
	.type	_ZN2v88internal17ExternalReference25search_string_raw_one_twoEv, @function
_ZN2v88internal17ExternalReference25search_string_raw_one_twoEv:
.LFB25328:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal17ExternalReference17search_string_rawIKhKtEES1_v
	.cfi_endproc
.LFE25328:
	.size	_ZN2v88internal17ExternalReference25search_string_raw_one_twoEv, .-_ZN2v88internal17ExternalReference25search_string_raw_one_twoEv
	.section	.text._ZN2v88internal17ExternalReference17search_string_rawIKtKhEES1_v,"axG",@progbits,_ZN2v88internal17ExternalReference17search_string_rawIKtKhEES1_v,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17ExternalReference17search_string_rawIKtKhEES1_v
	.type	_ZN2v88internal17ExternalReference17search_string_rawIKtKhEES1_v, @function
_ZN2v88internal17ExternalReference17search_string_rawIKtKhEES1_v:
.LFB27798:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internal15SearchStringRawIKtKhEElPNS0_7IsolateEPKT_iPKT0_ii(%rip), %rax
	ret
	.cfi_endproc
.LFE27798:
	.size	_ZN2v88internal17ExternalReference17search_string_rawIKtKhEES1_v, .-_ZN2v88internal17ExternalReference17search_string_rawIKtKhEES1_v
	.section	.text._ZN2v88internal17ExternalReference25search_string_raw_two_oneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference25search_string_raw_two_oneEv
	.type	_ZN2v88internal17ExternalReference25search_string_raw_two_oneEv, @function
_ZN2v88internal17ExternalReference25search_string_raw_two_oneEv:
.LFB25329:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal17ExternalReference17search_string_rawIKtKhEES1_v
	.cfi_endproc
.LFE25329:
	.size	_ZN2v88internal17ExternalReference25search_string_raw_two_oneEv, .-_ZN2v88internal17ExternalReference25search_string_raw_two_oneEv
	.section	.text._ZN2v88internal17ExternalReference17search_string_rawIKtS3_EES1_v,"axG",@progbits,_ZN2v88internal17ExternalReference17search_string_rawIKtS3_EES1_v,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17ExternalReference17search_string_rawIKtS3_EES1_v
	.type	_ZN2v88internal17ExternalReference17search_string_rawIKtS3_EES1_v, @function
_ZN2v88internal17ExternalReference17search_string_rawIKtS3_EES1_v:
.LFB27799:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internal15SearchStringRawIKtS2_EElPNS0_7IsolateEPKT_iPKT0_ii(%rip), %rax
	ret
	.cfi_endproc
.LFE27799:
	.size	_ZN2v88internal17ExternalReference17search_string_rawIKtS3_EES1_v, .-_ZN2v88internal17ExternalReference17search_string_rawIKtS3_EES1_v
	.section	.text._ZN2v88internal17ExternalReference25search_string_raw_two_twoEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference25search_string_raw_two_twoEv
	.type	_ZN2v88internal17ExternalReference25search_string_raw_two_twoEv, @function
_ZN2v88internal17ExternalReference25search_string_raw_two_twoEv:
.LFB25330:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal17ExternalReference17search_string_rawIKtS3_EES1_v
	.cfi_endproc
.LFE25330:
	.size	_ZN2v88internal17ExternalReference25search_string_raw_two_twoEv, .-_ZN2v88internal17ExternalReference25search_string_raw_two_twoEv
	.section	.text._ZN2v88internal17ExternalReference14FromRawAddressEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference14FromRawAddressEm
	.type	_ZN2v88internal17ExternalReference14FromRawAddressEm, @function
_ZN2v88internal17ExternalReference14FromRawAddressEm:
.LFB32224:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE32224:
	.size	_ZN2v88internal17ExternalReference14FromRawAddressEm, .-_ZN2v88internal17ExternalReference14FromRawAddressEm
	.section	.text._ZN2v88internal17ExternalReference12cpu_featuresEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference12cpu_featuresEv
	.type	_ZN2v88internal17ExternalReference12cpu_featuresEv, @function
_ZN2v88internal17ExternalReference12cpu_featuresEv:
.LFB25362:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internal11CpuFeatures10supported_E(%rip), %rax
	ret
	.cfi_endproc
.LFE25362:
	.size	_ZN2v88internal17ExternalReference12cpu_featuresEv, .-_ZN2v88internal17ExternalReference12cpu_featuresEv
	.section	.text._ZN2v88internal17ExternalReference20promise_hook_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference20promise_hook_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference20promise_hook_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference20promise_hook_addressEPNS0_7IsolateE:
.LFB25363:
	.cfi_startproc
	endbr64
	leaq	41288(%rdi), %rax
	ret
	.cfi_endproc
.LFE25363:
	.size	_ZN2v88internal17ExternalReference20promise_hook_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference20promise_hook_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference28async_event_delegate_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference28async_event_delegate_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference28async_event_delegate_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference28async_event_delegate_addressEPNS0_7IsolateE:
.LFB25364:
	.cfi_startproc
	endbr64
	leaq	45656(%rdi), %rax
	ret
	.cfi_endproc
.LFE25364:
	.size	_ZN2v88internal17ExternalReference28async_event_delegate_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference28async_event_delegate_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference44promise_hook_or_async_event_delegate_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference44promise_hook_or_async_event_delegate_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference44promise_hook_or_async_event_delegate_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference44promise_hook_or_async_event_delegate_addressEPNS0_7IsolateE:
.LFB25365:
	.cfi_startproc
	endbr64
	leaq	45664(%rdi), %rax
	ret
	.cfi_endproc
.LFE25365:
	.size	_ZN2v88internal17ExternalReference44promise_hook_or_async_event_delegate_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference44promise_hook_or_async_event_delegate_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference63promise_hook_or_debug_is_active_or_async_event_delegate_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference63promise_hook_or_debug_is_active_or_async_event_delegate_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference63promise_hook_or_debug_is_active_or_async_event_delegate_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference63promise_hook_or_debug_is_active_or_async_event_delegate_addressEPNS0_7IsolateE:
.LFB25366:
	.cfi_startproc
	endbr64
	leaq	45665(%rdi), %rax
	ret
	.cfi_endproc
.LFE25366:
	.size	_ZN2v88internal17ExternalReference63promise_hook_or_debug_is_active_or_async_event_delegate_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference63promise_hook_or_debug_is_active_or_async_event_delegate_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference28debug_execution_mode_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference28debug_execution_mode_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference28debug_execution_mode_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference28debug_execution_mode_addressEPNS0_7IsolateE:
.LFB25367:
	.cfi_startproc
	endbr64
	leaq	41828(%rdi), %rax
	ret
	.cfi_endproc
.LFE25367:
	.size	_ZN2v88internal17ExternalReference28debug_execution_mode_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference28debug_execution_mode_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference23debug_is_active_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference23debug_is_active_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference23debug_is_active_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference23debug_is_active_addressEPNS0_7IsolateE:
.LFB25368:
	.cfi_startproc
	endbr64
	movq	41472(%rdi), %rax
	addq	$8, %rax
	ret
	.cfi_endproc
.LFE25368:
	.size	_ZN2v88internal17ExternalReference23debug_is_active_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference23debug_is_active_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference35debug_hook_on_function_call_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference35debug_hook_on_function_call_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference35debug_hook_on_function_call_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference35debug_hook_on_function_call_addressEPNS0_7IsolateE:
.LFB25369:
	.cfi_startproc
	endbr64
	movq	41472(%rdi), %rax
	addq	$9, %rax
	ret
	.cfi_endproc
.LFE25369:
	.size	_ZN2v88internal17ExternalReference35debug_hook_on_function_call_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference35debug_hook_on_function_call_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference30runtime_function_table_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference30runtime_function_table_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference30runtime_function_table_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference30runtime_function_table_addressEPNS0_7IsolateE:
.LFB25370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal7Runtime20RuntimeFunctionTableEPNS0_7IsolateE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25370:
	.size	_ZN2v88internal17ExternalReference30runtime_function_table_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference30runtime_function_table_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference36invalidate_prototype_chains_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference36invalidate_prototype_chains_functionEv
	.type	_ZN2v88internal17ExternalReference36invalidate_prototype_chains_functionEv, @function
_ZN2v88internal17ExternalReference36invalidate_prototype_chains_functionEv:
.LFB25372:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL32InvalidatePrototypeChainsWrapperEm(%rip), %rax
	ret
	.cfi_endproc
.LFE25372:
	.size	_ZN2v88internal17ExternalReference36invalidate_prototype_chains_functionEv, .-_ZN2v88internal17ExternalReference36invalidate_prototype_chains_functionEv
	.section	.text._ZN2v88internal17ExternalReference25mod_two_doubles_operationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference25mod_two_doubles_operationEv
	.type	_ZN2v88internal17ExternalReference25mod_two_doubles_operationEv, @function
_ZN2v88internal17ExternalReference25mod_two_doubles_operationEv:
.LFB25374:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internal20modulo_double_doubleEdd(%rip), %rax
	ret
	.cfi_endproc
.LFE25374:
	.size	_ZN2v88internal17ExternalReference25mod_two_doubles_operationEv, .-_ZN2v88internal17ExternalReference25mod_two_doubles_operationEv
	.section	.text._ZN2v88internal17ExternalReference33debug_suspended_generator_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference33debug_suspended_generator_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference33debug_suspended_generator_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference33debug_suspended_generator_addressEPNS0_7IsolateE:
.LFB25375:
	.cfi_startproc
	endbr64
	movq	41472(%rdi), %rax
	addq	$112, %rax
	ret
	.cfi_endproc
.LFE25375:
	.size	_ZN2v88internal17ExternalReference33debug_suspended_generator_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference33debug_suspended_generator_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference24debug_restart_fp_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference24debug_restart_fp_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference24debug_restart_fp_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference24debug_restart_fp_addressEPNS0_7IsolateE:
.LFB25376:
	.cfi_startproc
	endbr64
	movq	41472(%rdi), %rax
	addq	$120, %rax
	ret
	.cfi_endproc
.LFE25376:
	.size	_ZN2v88internal17ExternalReference24debug_restart_fp_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference24debug_restart_fp_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference29fast_c_call_caller_fp_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference29fast_c_call_caller_fp_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference29fast_c_call_caller_fp_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference29fast_c_call_caller_fp_addressEPNS0_7IsolateE:
.LFB25377:
	.cfi_startproc
	endbr64
	leaq	37496(%rdi), %rax
	ret
	.cfi_endproc
.LFE25377:
	.size	_ZN2v88internal17ExternalReference29fast_c_call_caller_fp_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference29fast_c_call_caller_fp_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference29fast_c_call_caller_pc_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference29fast_c_call_caller_pc_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference29fast_c_call_caller_pc_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference29fast_c_call_caller_pc_addressEPNS0_7IsolateE:
.LFB25378:
	.cfi_startproc
	endbr64
	leaq	37504(%rdi), %rax
	ret
	.cfi_endproc
.LFE25378:
	.size	_ZN2v88internal17ExternalReference29fast_c_call_caller_pc_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference29fast_c_call_caller_pc_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference25stack_is_iterable_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference25stack_is_iterable_addressEPNS0_7IsolateE
	.type	_ZN2v88internal17ExternalReference25stack_is_iterable_addressEPNS0_7IsolateE, @function
_ZN2v88internal17ExternalReference25stack_is_iterable_addressEPNS0_7IsolateE:
.LFB25379:
	.cfi_startproc
	endbr64
	leaq	37568(%rdi), %rax
	ret
	.cfi_endproc
.LFE25379:
	.size	_ZN2v88internal17ExternalReference25stack_is_iterable_addressEPNS0_7IsolateE, .-_ZN2v88internal17ExternalReference25stack_is_iterable_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal17ExternalReference31call_enqueue_microtask_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference31call_enqueue_microtask_functionEv
	.type	_ZN2v88internal17ExternalReference31call_enqueue_microtask_functionEv, @function
_ZN2v88internal17ExternalReference31call_enqueue_microtask_functionEv:
.LFB25380:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal14MicrotaskQueue20CallEnqueueMicrotaskEPNS0_7IsolateElm@GOTPCREL(%rip), %rax
	ret
	.cfi_endproc
.LFE25380:
	.size	_ZN2v88internal17ExternalReference31call_enqueue_microtask_functionEv, .-_ZN2v88internal17ExternalReference31call_enqueue_microtask_functionEv
	.section	.text._ZN2v88internal17ExternalReference25atomic_pair_load_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference25atomic_pair_load_functionEv
	.type	_ZN2v88internal17ExternalReference25atomic_pair_load_functionEv, @function
_ZN2v88internal17ExternalReference25atomic_pair_load_functionEv:
.LFB25385:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL16atomic_pair_loadEl(%rip), %rax
	ret
	.cfi_endproc
.LFE25385:
	.size	_ZN2v88internal17ExternalReference25atomic_pair_load_functionEv, .-_ZN2v88internal17ExternalReference25atomic_pair_load_functionEv
	.section	.text._ZN2v88internal17ExternalReference26atomic_pair_store_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference26atomic_pair_store_functionEv
	.type	_ZN2v88internal17ExternalReference26atomic_pair_store_functionEv, @function
_ZN2v88internal17ExternalReference26atomic_pair_store_functionEv:
.LFB25387:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL17atomic_pair_storeElii(%rip), %rax
	ret
	.cfi_endproc
.LFE25387:
	.size	_ZN2v88internal17ExternalReference26atomic_pair_store_functionEv, .-_ZN2v88internal17ExternalReference26atomic_pair_store_functionEv
	.section	.text._ZN2v88internal17ExternalReference24atomic_pair_add_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference24atomic_pair_add_functionEv
	.type	_ZN2v88internal17ExternalReference24atomic_pair_add_functionEv, @function
_ZN2v88internal17ExternalReference24atomic_pair_add_functionEv:
.LFB25389:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL15atomic_pair_addElii(%rip), %rax
	ret
	.cfi_endproc
.LFE25389:
	.size	_ZN2v88internal17ExternalReference24atomic_pair_add_functionEv, .-_ZN2v88internal17ExternalReference24atomic_pair_add_functionEv
	.section	.text._ZN2v88internal17ExternalReference24atomic_pair_sub_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference24atomic_pair_sub_functionEv
	.type	_ZN2v88internal17ExternalReference24atomic_pair_sub_functionEv, @function
_ZN2v88internal17ExternalReference24atomic_pair_sub_functionEv:
.LFB25391:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL15atomic_pair_subElii(%rip), %rax
	ret
	.cfi_endproc
.LFE25391:
	.size	_ZN2v88internal17ExternalReference24atomic_pair_sub_functionEv, .-_ZN2v88internal17ExternalReference24atomic_pair_sub_functionEv
	.section	.text._ZN2v88internal17ExternalReference24atomic_pair_and_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference24atomic_pair_and_functionEv
	.type	_ZN2v88internal17ExternalReference24atomic_pair_and_functionEv, @function
_ZN2v88internal17ExternalReference24atomic_pair_and_functionEv:
.LFB25393:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL15atomic_pair_andElii(%rip), %rax
	ret
	.cfi_endproc
.LFE25393:
	.size	_ZN2v88internal17ExternalReference24atomic_pair_and_functionEv, .-_ZN2v88internal17ExternalReference24atomic_pair_and_functionEv
	.section	.text._ZN2v88internal17ExternalReference23atomic_pair_or_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference23atomic_pair_or_functionEv
	.type	_ZN2v88internal17ExternalReference23atomic_pair_or_functionEv, @function
_ZN2v88internal17ExternalReference23atomic_pair_or_functionEv:
.LFB25395:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL14atomic_pair_orElii(%rip), %rax
	ret
	.cfi_endproc
.LFE25395:
	.size	_ZN2v88internal17ExternalReference23atomic_pair_or_functionEv, .-_ZN2v88internal17ExternalReference23atomic_pair_or_functionEv
	.section	.text._ZN2v88internal17ExternalReference24atomic_pair_xor_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference24atomic_pair_xor_functionEv
	.type	_ZN2v88internal17ExternalReference24atomic_pair_xor_functionEv, @function
_ZN2v88internal17ExternalReference24atomic_pair_xor_functionEv:
.LFB25397:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL15atomic_pair_xorElii(%rip), %rax
	ret
	.cfi_endproc
.LFE25397:
	.size	_ZN2v88internal17ExternalReference24atomic_pair_xor_functionEv, .-_ZN2v88internal17ExternalReference24atomic_pair_xor_functionEv
	.section	.text._ZN2v88internal17ExternalReference29atomic_pair_exchange_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference29atomic_pair_exchange_functionEv
	.type	_ZN2v88internal17ExternalReference29atomic_pair_exchange_functionEv, @function
_ZN2v88internal17ExternalReference29atomic_pair_exchange_functionEv:
.LFB25399:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL20atomic_pair_exchangeElii(%rip), %rax
	ret
	.cfi_endproc
.LFE25399:
	.size	_ZN2v88internal17ExternalReference29atomic_pair_exchange_functionEv, .-_ZN2v88internal17ExternalReference29atomic_pair_exchange_functionEv
	.section	.text._ZN2v88internal17ExternalReference37atomic_pair_compare_exchange_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference37atomic_pair_compare_exchange_functionEv
	.type	_ZN2v88internal17ExternalReference37atomic_pair_compare_exchange_functionEv, @function
_ZN2v88internal17ExternalReference37atomic_pair_compare_exchange_functionEv:
.LFB25401:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL28atomic_pair_compare_exchangeEliiii(%rip), %rax
	ret
	.cfi_endproc
.LFE25401:
	.size	_ZN2v88internal17ExternalReference37atomic_pair_compare_exchange_functionEv, .-_ZN2v88internal17ExternalReference37atomic_pair_compare_exchange_functionEv
	.section	.text._ZN2v88internal17ExternalReference27call_enter_context_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ExternalReference27call_enter_context_functionEv
	.type	_ZN2v88internal17ExternalReference27call_enter_context_functionEv, @function
_ZN2v88internal17ExternalReference27call_enter_context_functionEv:
.LFB25408:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internalL28EnterMicrotaskContextWrapperEPNS0_22HandleScopeImplementerEm(%rip), %rax
	ret
	.cfi_endproc
.LFE25408:
	.size	_ZN2v88internal17ExternalReference27call_enter_context_functionEv, .-_ZN2v88internal17ExternalReference27call_enter_context_functionEv
	.section	.text._ZN2v88internaleqENS0_17ExternalReferenceES1_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internaleqENS0_17ExternalReferenceES1_
	.type	_ZN2v88internaleqENS0_17ExternalReferenceES1_, @function
_ZN2v88internaleqENS0_17ExternalReferenceES1_:
.LFB25412:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	sete	%al
	ret
	.cfi_endproc
.LFE25412:
	.size	_ZN2v88internaleqENS0_17ExternalReferenceES1_, .-_ZN2v88internaleqENS0_17ExternalReferenceES1_
	.section	.text._ZN2v88internalneENS0_17ExternalReferenceES1_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internalneENS0_17ExternalReferenceES1_
	.type	_ZN2v88internalneENS0_17ExternalReferenceES1_, @function
_ZN2v88internalneENS0_17ExternalReferenceES1_:
.LFB25413:
	.cfi_startproc
	endbr64
	cmpq	%rdi, %rsi
	setne	%al
	ret
	.cfi_endproc
.LFE25413:
	.size	_ZN2v88internalneENS0_17ExternalReferenceES1_, .-_ZN2v88internalneENS0_17ExternalReferenceES1_
	.section	.text._ZN2v88internal10hash_valueENS0_17ExternalReferenceE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal10hash_valueENS0_17ExternalReferenceE
	.type	_ZN2v88internal10hash_valueENS0_17ExternalReferenceE, @function
_ZN2v88internal10hash_valueENS0_17ExternalReferenceE:
.LFB25414:
	.cfi_startproc
	endbr64
	jmp	_ZN2v84base10hash_valueEm@PLT
	.cfi_endproc
.LFE25414:
	.size	_ZN2v88internal10hash_valueENS0_17ExternalReferenceE, .-_ZN2v88internal10hash_valueENS0_17ExternalReferenceE
	.section	.rodata._ZN2v88internallsERSoNS0_17ExternalReferenceE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"<"
.LC4:
	.string	".entry>"
	.section	.text._ZN2v88internallsERSoNS0_17ExternalReferenceE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoNS0_17ExternalReferenceE
	.type	_ZN2v88internallsERSoNS0_17ExternalReferenceE, @function
_ZN2v88internallsERSoNS0_17ExternalReferenceE:
.LFB25415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal7Runtime16FunctionForEntryEm@PLT
	testq	%rax, %rax
	je	.L503
	movq	%rax, %r13
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %r13
	testq	%r13, %r13
	je	.L510
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L505:
	movl	$7, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L503:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L510:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L505
	.cfi_endproc
.LFE25415:
	.size	_ZN2v88internallsERSoNS0_17ExternalReferenceE, .-_ZN2v88internallsERSoNS0_17ExternalReferenceE
	.section	.text._ZN2v88internal12SearchStringIhtEEiPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEi,"axG",@progbits,_ZN2v88internal12SearchStringIhtEEiPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEi,comdat
	.p2align 4
	.weak	_ZN2v88internal12SearchStringIhtEEiPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEi
	.type	_ZN2v88internal12SearchStringIhtEEiPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEi, @function
_ZN2v88internal12SearchStringIhtEEiPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEi:
.LFB30141:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leal	-250(%r8), %eax
	movq	%rdi, -48(%rbp)
	movl	$0, %edi
	testl	%eax, %eax
	movq	%rcx, -40(%rbp)
	cmovs	%edi, %eax
	movq	%r8, -32(%rbp)
	movl	%eax, -16(%rbp)
	movslq	%r8d, %rax
	cmpq	$7, %rax
	leaq	(%rcx,%rax,2), %rdi
	movq	%rcx, %rax
	jbe	.L537
	testb	$7, %cl
	jne	.L513
	.p2align 4,,10
	.p2align 3
.L517:
	leaq	16(%rax), %r10
	cmpq	%r10, %rdi
	jb	.L537
	leaq	-16(%rdi), %r10
	subq	%rax, %r10
	shrq	$3, %r10
	leaq	8(%rax,%r10,8), %r11
	movabsq	$-71777214294589696, %r10
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L518:
	addq	$8, %rax
	cmpq	%rax, %r11
	je	.L537
.L521:
	testq	%r10, (%rax)
	je	.L518
	cmpq	%rax, %rdi
	jbe	.L519
	.p2align 4,,10
	.p2align 3
.L520:
	cmpw	$255, (%rax)
	ja	.L519
	addq	$2, %rax
.L537:
	cmpq	%rax, %rdi
	ja	.L520
.L519:
	subq	%rcx, %rax
	sarq	%rax
	cmpl	%eax, %r8d
	jg	.L538
.L523:
	cmpl	$6, %r8d
	jle	.L539
	leaq	_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -24(%rbp)
.L524:
	leaq	-48(%rbp), %rdi
	movl	%r9d, %ecx
	call	*%rax
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L540
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L515:
	.cfi_restore_state
	addq	$2, %rax
	testb	$7, %al
	je	.L517
.L513:
	cmpw	$255, (%rax)
	jbe	.L515
	subq	%rcx, %rax
	sarq	%rax
	cmpl	%eax, %r8d
	jle	.L523
.L538:
	leaq	_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -24(%rbp)
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L539:
	cmpl	$1, %r8d
	je	.L541
	leaq	_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -24(%rbp)
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L541:
	leaq	_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -24(%rbp)
	jmp	.L524
.L540:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30141:
	.size	_ZN2v88internal12SearchStringIhtEEiPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEi, .-_ZN2v88internal12SearchStringIhtEEiPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEi
	.section	.text._ZN2v88internal15SearchStringRawIKhKtEElPNS0_7IsolateEPKT_iPKT0_ii,"axG",@progbits,_ZN2v88internal15SearchStringRawIKhKtEElPNS0_7IsolateEPKT_iPKT0_ii,comdat
	.p2align 4
	.weak	_ZN2v88internal15SearchStringRawIKhKtEElPNS0_7IsolateEPKT_iPKT0_ii
	.type	_ZN2v88internal15SearchStringRawIKhKtEElPNS0_7IsolateEPKT_iPKT0_ii, @function
_ZN2v88internal15SearchStringRawIKhKtEElPNS0_7IsolateEPKT_iPKT0_ii:
.LFB29184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movslq	%r8d, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal12SearchStringIhtEEiPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEi
	popq	%rbp
	.cfi_def_cfa 7, 8
	cltq
	ret
	.cfi_endproc
.LFE29184:
	.size	_ZN2v88internal15SearchStringRawIKhKtEElPNS0_7IsolateEPKT_iPKT0_ii, .-_ZN2v88internal15SearchStringRawIKhKtEElPNS0_7IsolateEPKT_iPKT0_ii
	.section	.text._ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv,"axG",@progbits,_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv
	.type	_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv, @function
_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv:
.LFB31128:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rsi
	movslq	32(%rdi), %r13
	movq	8(%rdi), %r9
	movq	(%rdi), %rdi
	movq	%rsi, %rcx
	movslq	%esi, %r11
	movl	%esi, %r10d
	movq	%rsi, -64(%rbp)
	leaq	43396(%rdi), %rdx
	addq	$44400, %rdi
	movl	%esi, %r8d
	subl	%r13d, %r10d
	leaq	0(,%r13,4), %rax
	movq	%rdx, %rbx
	leaq	0(,%r11,4), %rsi
	subq	%rax, %rbx
	subq	%rax, %rdi
	movl	%ecx, %eax
	leaq	(%rbx,%rsi), %r14
	addl	$1, %eax
	addq	%rdi, %rsi
	cmpl	%ecx, %r13d
	jge	.L545
	movl	%ecx, -52(%rbp)
	subl	$1, %ecx
	movq	%r13, %r12
	subl	%r13d, %ecx
	cmpl	$2, %ecx
	jbe	.L569
	movl	%r10d, %ecx
	movd	%r10d, %xmm1
	shrl	$2, %ecx
	pshufd	$0, %xmm1, %xmm0
	salq	$4, %rcx
	addq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L547:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L547
	movl	%r10d, %ecx
	andl	$-4, %ecx
	leal	(%rcx,%r12), %edx
	cmpl	%r10d, %ecx
	je	.L548
.L546:
	movslq	%edx, %rcx
	movq	-64(%rbp), %r15
	movl	%r10d, (%rbx,%rcx,4)
	leal	1(%rdx), %ecx
	cmpl	%ecx, %r15d
	jle	.L548
	movslq	%ecx, %rcx
	addl	$2, %edx
	movl	%r10d, (%rbx,%rcx,4)
	cmpl	%r15d, %edx
	jge	.L548
	movslq	%edx, %rdx
	movl	%r10d, (%rbx,%rdx,4)
.L548:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	movzbl	-1(%r9,%r11), %r15d
	movl	-64(%rbp), %esi
	movl	%r15d, %r11d
	.p2align 4,,10
	.p2align 3
.L568:
	cmpl	%eax, %r8d
	jl	.L550
	.p2align 4,,10
	.p2align 3
.L554:
	movslq	%eax, %rdx
	cmpb	%r11b, -1(%r9,%rdx)
	je	.L550
	leaq	(%rbx,%rdx,4), %rcx
	cmpl	%r10d, (%rcx)
	je	.L575
	movl	(%rdi,%rdx,4), %eax
	cmpl	%r8d, %eax
	jle	.L554
.L550:
	subl	$1, %esi
	leal	-1(%rax), %edx
	movslq	%esi, %rcx
	movl	%edx, (%rdi,%rcx,4)
	cmpl	%r8d, %edx
	je	.L576
.L555:
	cmpl	%esi, %r12d
	jge	.L549
.L578:
	movslq	%esi, %rax
	movzbl	-1(%r9,%rax), %r11d
	movl	%edx, %eax
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L575:
	subl	%esi, %eax
	movl	%eax, (%rcx)
	movl	(%rdi,%rdx,4), %eax
	cmpl	%r8d, %eax
	jle	.L554
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L576:
	cmpl	%esi, %r12d
	jl	.L561
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L577:
	cmpl	%r10d, (%r14)
	jne	.L558
	movl	-52(%rbp), %edx
	subl	%ecx, %edx
	movl	%edx, (%r14)
.L558:
	movl	%r8d, -4(%rdi,%rcx,4)
	subq	$1, %rcx
	subl	$1, %esi
	cmpl	%ecx, %r12d
	jge	.L556
.L561:
	movl	%ecx, %esi
	cmpb	%r15b, -1(%r9,%rcx)
	jne	.L577
	cmpl	%r12d, %ecx
	jle	.L544
	subl	$1, %esi
	leal	-2(%rax), %edx
	movslq	%esi, %rax
	movl	%edx, (%rdi,%rax,4)
	cmpl	%esi, %r12d
	jl	.L578
	.p2align 4,,10
	.p2align 3
.L549:
	cmpl	-64(%rbp), %edx
	jge	.L544
	.p2align 4,,10
	.p2align 3
.L563:
	cmpl	%r10d, (%rbx,%r13,4)
	jne	.L565
	movl	%edx, %eax
	subl	%r12d, %eax
	movl	%eax, (%rbx,%r13,4)
.L565:
	cmpl	%r13d, %edx
	je	.L579
	addq	$1, %r13
	cmpl	%r13d, %r8d
	jge	.L563
.L544:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L579:
	.cfi_restore_state
	movslq	%edx, %rdx
	addq	$1, %r13
	movl	(%rdi,%rdx,4), %edx
	cmpl	%r13d, %r8d
	jge	.L563
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L556:
	.cfi_restore_state
	movl	%r8d, %edx
	jmp	.L555
.L545:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L569:
	.cfi_restore_state
	movl	%r13d, %edx
	jmp	.L546
	.cfi_endproc
.LFE31128:
	.size	_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv, .-_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv
	.section	.text._ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi:
.LFB31027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	movq	8(%rdi), %r13
	movq	%rdx, -72(%rbp)
	movq	(%rdi), %r10
	leal	-1(%rax), %edx
	movl	%eax, %r8d
	movl	%eax, -60(%rbp)
	leal	-2(%rax), %r15d
	movslq	%edx, %r9
	leaq	42372(%r10), %rdi
	movl	%edx, %ebx
	negl	%r8d
	movzbl	0(%r13,%r9), %r11d
	subl	42372(%r10,%r11,4), %ebx
	movl	%r14d, %r10d
	movq	%r11, %r9
	movl	$1, %r11d
	movl	%ebx, -56(%rbp)
	subl	%eax, %r10d
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L595:
	movl	%edx, %ebx
	subl	(%rdi,%rax,4), %ebx
	movl	%ebx, %eax
	addl	%ebx, %ecx
	movl	%r11d, %ebx
	subl	%eax, %ebx
	addl	%ebx, %r8d
.L593:
	cmpl	%r10d, %ecx
	jg	.L594
	leal	(%rdx,%rcx), %eax
	cltq
	movzbl	(%rsi,%rax), %eax
	cmpb	%r9b, %al
	jne	.L595
	testl	%r15d, %r15d
	js	.L590
	movslq	%ecx, %rbx
	movl	%edx, -64(%rbp)
	movslq	%r15d, %rax
	addq	%rsi, %rbx
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L596:
	subq	$1, %rax
	testl	%eax, %eax
	js	.L590
.L585:
	movzbl	(%rbx,%rax), %edx
	movl	%eax, %r14d
	cmpb	%dl, 0(%r13,%rax)
	je	.L596
	movl	-60(%rbp), %eax
	movl	-56(%rbp), %ebx
	movl	-64(%rbp), %edx
	subl	%r14d, %eax
	addl	%ebx, %ecx
	subl	%ebx, %eax
	addl	%eax, %r8d
	testl	%r8d, %r8d
	jle	.L593
	movq	%r12, %rdi
	movl	%ecx, -60(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv
	movl	-60(%rbp), %ecx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	leaq	_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	-72(%rbp), %rdx
	movq	%rax, 24(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi
	.p2align 4,,10
	.p2align 3
.L594:
	.cfi_restore_state
	addq	$40, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L590:
	.cfi_restore_state
	addq	$40, %rsp
	movl	%ecx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE31027:
	.size	_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi:
.LFB30858:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	16(%rdi), %rsi
	movq	8(%rdi), %r13
	movq	%rdi, -72(%rbp)
	movq	%rdx, -88(%rbp)
	movl	%esi, %eax
	subl	%esi, %ebx
	movq	%rsi, -80(%rbp)
	movl	%esi, -56(%rbp)
	sall	$2, %eax
	cmpl	%ebx, %ecx
	jg	.L602
	movl	$-9, %edx
	movl	%ecx, %r9d
	subl	%eax, %edx
	movl	%edx, -60(%rbp)
	testl	%edx, %edx
	jg	.L600
	movzbl	0(%r13), %r15d
	leal	1(%rbx), %eax
	movl	%eax, -52(%rbp)
	movl	%r15d, %r14d
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L628:
	subq	%r12, %rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	addq	%r12, %rdx
	cmpb	%r14b, (%rdx)
	je	.L603
	leal	1(%rax), %r9d
	cmpl	%eax, %ebx
	jle	.L602
.L604:
	movl	-52(%rbp), %edx
	movslq	%r9d, %rdi
	movl	%r15d, %esi
	addq	%r12, %rdi
	subl	%r9d, %edx
	movslq	%edx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L628
.L602:
	movl	$-1, %r9d
.L597:
	addq	$56, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L603:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L602
	movl	-56(%rbp), %esi
	movl	$1, %eax
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L629:
	leal	1(%rax), %ecx
	addq	$1, %rax
	cmpl	%eax, %esi
	jle	.L606
.L607:
	movzbl	(%rdx,%rax), %edi
	movl	%eax, %ecx
	cmpb	%dil, 0(%r13,%rax)
	je	.L629
.L606:
	cmpl	-56(%rbp), %ecx
	je	.L597
	addl	$1, %r9d
	addl	-60(%rbp), %ecx
	cmpl	%r9d, %ebx
	jl	.L602
	leal	1(%rcx), %eax
	movl	%eax, -60(%rbp)
	testl	%eax, %eax
	jle	.L604
.L600:
	movq	-72(%rbp), %rax
	movq	(%rax), %rdx
	movl	32(%rax), %r10d
	leaq	42372(%rdx), %rsi
	testl	%r10d, %r10d
	je	.L630
	leal	-1(%r10), %eax
	addq	$43396, %rdx
	movd	%eax, %xmm0
	movq	%rsi, %rax
	pshufd	$0, %xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L612:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L612
.L611:
	movl	-80(%rbp), %eax
	subl	$1, %eax
	cmpl	%r10d, %eax
	jle	.L613
	movl	-80(%rbp), %edx
	movslq	%r10d, %rcx
	movq	-72(%rbp), %r8
	leaq	1(%rcx), %rax
	subl	$2, %edx
	subl	%r10d, %edx
	addq	%rax, %rdx
	jmp	.L614
	.p2align 4,,10
	.p2align 3
.L631:
	addq	$1, %rax
.L614:
	movq	8(%r8), %rdi
	movzbl	(%rdi,%rcx), %edi
	movl	%ecx, (%rsi,%rdi,4)
	movq	%rax, %rcx
	cmpq	%rax, %rdx
	jne	.L631
.L613:
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %rdx
	movq	%r12, %rsi
	movl	%r9d, %ecx
	leaq	_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi(%rip), %rbx
	movq	%rbx, 24(%rax)
	addq	$56, %rsp
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi
.L630:
	.cfi_restore_state
	leaq	42380(%rdx), %rdi
	movq	%rsi, %rcx
	movq	$-1, %rax
	movq	$-1, 42372(%rdx)
	movq	$-1, 43388(%rdx)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	jmp	.L611
	.cfi_endproc
.LFE30858:
	.size	_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv,"axG",@progbits,_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv
	.type	_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv, @function
_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv:
.LFB31133:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r15
	movslq	32(%rdi), %r13
	movq	8(%rdi), %r9
	movq	(%rdi), %rdi
	leaq	0(,%r13,4), %rax
	movslq	%r15d, %r11
	movl	%r15d, %r10d
	leaq	43396(%rdi), %rdx
	addq	$44400, %rdi
	leaq	0(,%r11,4), %rsi
	subl	%r13d, %r10d
	movq	%rdx, %rbx
	subq	%rax, %rdi
	subq	%rax, %rbx
	leal	1(%r15), %eax
	leaq	(%rbx,%rsi), %r14
	addq	%rdi, %rsi
	cmpl	%r15d, %r13d
	jge	.L633
	leal	-1(%r15), %ecx
	movl	%r15d, -52(%rbp)
	movl	%r15d, %r8d
	movq	%r13, %r12
	subl	%r13d, %ecx
	cmpl	$2, %ecx
	jbe	.L657
	movl	%r10d, %ecx
	movd	%r10d, %xmm1
	shrl	$2, %ecx
	pshufd	$0, %xmm1, %xmm0
	salq	$4, %rcx
	addq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L635:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L635
	movl	%r10d, %ecx
	andl	$-4, %ecx
	leal	(%rcx,%r12), %edx
	cmpl	%r10d, %ecx
	je	.L636
.L634:
	movslq	%edx, %rcx
	movl	%r10d, (%rbx,%rcx,4)
	leal	1(%rdx), %ecx
	cmpl	%ecx, %r15d
	jle	.L636
	movslq	%ecx, %rcx
	addl	$2, %edx
	movl	%r10d, (%rbx,%rcx,4)
	cmpl	%r15d, %edx
	jge	.L636
	movslq	%edx, %rdx
	movl	%r10d, (%rbx,%rdx,4)
.L636:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	movzwl	-2(%r9,%r11,2), %ecx
	movl	%r15d, %esi
	movq	%r13, -64(%rbp)
	movl	%ecx, %r11d
	movl	%ecx, %r13d
	.p2align 4,,10
	.p2align 3
.L662:
	cmpl	%eax, %r8d
	jl	.L638
.L642:
	movslq	%eax, %rdx
	cmpw	%r11w, -2(%r9,%rdx,2)
	je	.L638
	leaq	(%rbx,%rdx,4), %rcx
	cmpl	%r10d, (%rcx)
	je	.L664
	movl	(%rdi,%rdx,4), %eax
	cmpl	%r8d, %eax
	jle	.L642
.L638:
	subl	$1, %esi
	leal	-1(%rax), %edx
	movslq	%esi, %rcx
	movl	%edx, (%rdi,%rcx,4)
	cmpl	%r8d, %edx
	je	.L665
.L643:
	cmpl	%esi, %r12d
	jge	.L637
.L667:
	movslq	%esi, %rax
	movzwl	-2(%r9,%rax,2), %r11d
	movl	%edx, %eax
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L664:
	subl	%esi, %eax
	movl	%eax, (%rcx)
	movl	(%rdi,%rdx,4), %eax
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L665:
	cmpl	%esi, %r12d
	jl	.L649
	jmp	.L644
	.p2align 4,,10
	.p2align 3
.L666:
	cmpl	%r10d, (%r14)
	jne	.L646
	movl	-52(%rbp), %edx
	subl	%ecx, %edx
	movl	%edx, (%r14)
.L646:
	movl	%r8d, -4(%rdi,%rcx,4)
	subq	$1, %rcx
	subl	$1, %esi
	cmpl	%ecx, %r12d
	jge	.L644
.L649:
	movl	%ecx, %esi
	cmpw	%r13w, -2(%r9,%rcx,2)
	jne	.L666
	cmpl	%r12d, %ecx
	jle	.L632
	subl	$1, %esi
	leal	-2(%rax), %edx
	movslq	%esi, %rax
	movl	%edx, (%rdi,%rax,4)
	cmpl	%esi, %r12d
	jl	.L667
	.p2align 4,,10
	.p2align 3
.L637:
	movq	-64(%rbp), %r13
	cmpl	%r15d, %edx
	jge	.L632
	.p2align 4,,10
	.p2align 3
.L651:
	cmpl	%r10d, (%rbx,%r13,4)
	jne	.L653
	movl	%edx, %eax
	subl	%r12d, %eax
	movl	%eax, (%rbx,%r13,4)
.L653:
	cmpl	%r13d, %edx
	je	.L668
	addq	$1, %r13
	cmpl	%r13d, %r8d
	jge	.L651
.L632:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L668:
	.cfi_restore_state
	movslq	%edx, %rdx
	addq	$1, %r13
	movl	(%rdi,%rdx,4), %edx
	cmpl	%r13d, %r8d
	jge	.L651
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L644:
	.cfi_restore_state
	movl	%r8d, %edx
	jmp	.L643
.L633:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L657:
	.cfi_restore_state
	movl	%r13d, %edx
	jmp	.L634
	.cfi_endproc
.LFE31133:
	.size	_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv, .-_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv
	.section	.text._ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi:
.LFB31033:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	movq	8(%rdi), %r14
	movq	%rdx, -72(%rbp)
	movq	(%rdi), %r10
	leal	-1(%rax), %edx
	movl	%eax, %r8d
	movl	%eax, -64(%rbp)
	movslq	%edx, %r9
	movl	%edx, %ebx
	leaq	42372(%r10), %rdi
	negl	%r8d
	movzwl	(%r14,%r9,2), %r9d
	movzbl	%r9b, %r11d
	subl	42372(%r10,%r11,4), %ebx
	movl	%r15d, %r10d
	movl	$1, %r11d
	movl	%ebx, -60(%rbp)
	leal	-2(%rax), %ebx
	subl	%eax, %r10d
	movl	%ebx, -56(%rbp)
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L684:
	movl	%edx, %ebx
	subl	(%rdi,%rax,4), %ebx
	movl	%ebx, %eax
	addl	%ebx, %ecx
	movl	%r11d, %ebx
	subl	%eax, %ebx
	addl	%ebx, %r8d
.L682:
	cmpl	%r10d, %ecx
	jg	.L683
	leal	(%rdx,%rcx), %eax
	cltq
	movzbl	(%rsi,%rax), %ebx
	movq	%rbx, %rax
	cmpw	%bx, %r9w
	jne	.L684
	movslq	-56(%rbp), %rax
	testl	%eax, %eax
	js	.L679
	movslq	%ecx, %r13
	addq	%rsi, %r13
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L685:
	subq	$1, %rax
	testl	%eax, %eax
	js	.L679
.L674:
	movzbl	0(%r13,%rax), %ebx
	movl	%eax, %r15d
	cmpw	%bx, (%r14,%rax,2)
	je	.L685
	movl	-64(%rbp), %eax
	movl	-60(%rbp), %ebx
	subl	%r15d, %eax
	addl	%ebx, %ecx
	subl	%ebx, %eax
	addl	%eax, %r8d
	testl	%r8d, %r8d
	jle	.L682
	movq	%r12, %rdi
	movl	%ecx, -60(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv
	movl	-60(%rbp), %ecx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	leaq	_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	-72(%rbp), %rdx
	movq	%rax, 24(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi
	.p2align 4,,10
	.p2align 3
.L683:
	.cfi_restore_state
	addq	$40, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L679:
	.cfi_restore_state
	addq	$40, %rsp
	movl	%ecx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE31033:
	.size	_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi:
.LFB30862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	16(%rdi), %rcx
	movq	8(%rdi), %r14
	movq	%rdi, -72(%rbp)
	movq	%rdx, -88(%rbp)
	movl	%ecx, %eax
	subl	%ecx, %ebx
	movq	%rcx, -80(%rbp)
	movl	%ecx, -56(%rbp)
	sall	$2, %eax
	cmpl	%ebx, %r9d
	jg	.L691
	movl	$-9, %edx
	movq	%rsi, %r13
	subl	%eax, %edx
	movl	%edx, -60(%rbp)
	testl	%edx, %edx
	jg	.L689
	movzwl	(%r14), %eax
	leal	1(%rbx), %ecx
	movl	%ecx, -52(%rbp)
	movzbl	%ah, %edx
	movl	%eax, %r15d
	cmpb	%dl, %al
	cmovbe	%edx, %eax
	movzbl	%al, %r12d
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L718:
	subq	%r13, %rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	addq	%r13, %rdx
	cmpb	%r15b, (%rdx)
	je	.L692
	leal	1(%rax), %r9d
	cmpl	%eax, %ebx
	jle	.L691
.L693:
	movl	-52(%rbp), %edx
	movslq	%r9d, %rdi
	movl	%r12d, %esi
	addq	%r13, %rdi
	subl	%r9d, %edx
	movslq	%edx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L718
.L691:
	movl	$-1, %r9d
.L686:
	addq	$56, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L692:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L691
	movl	-56(%rbp), %edi
	movl	$1, %eax
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L719:
	leal	1(%rax), %ecx
	addq	$1, %rax
	cmpl	%eax, %edi
	jle	.L695
.L696:
	movzbl	(%rdx,%rax), %esi
	movl	%eax, %ecx
	cmpw	%si, (%r14,%rax,2)
	je	.L719
.L695:
	cmpl	-56(%rbp), %ecx
	je	.L686
	addl	$1, %r9d
	addl	-60(%rbp), %ecx
	cmpl	%r9d, %ebx
	jl	.L691
	leal	1(%rcx), %eax
	movl	%eax, -60(%rbp)
	testl	%eax, %eax
	jle	.L693
.L689:
	movq	-72(%rbp), %rax
	movq	(%rax), %rcx
	movl	32(%rax), %edx
	leaq	42372(%rcx), %rsi
	testl	%edx, %edx
	je	.L720
	leal	-1(%rdx), %eax
	addq	$43396, %rcx
	movd	%eax, %xmm0
	movq	%rsi, %rax
	pshufd	$0, %xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L701:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L701
.L700:
	movl	-80(%rbp), %edi
	movslq	%edx, %rax
	movq	-72(%rbp), %r8
	addq	%rax, %rax
	subl	$1, %edi
	cmpl	%edx, %edi
	jle	.L702
	.p2align 4,,10
	.p2align 3
.L703:
	movq	8(%r8), %rcx
	movzbl	(%rcx,%rax), %ecx
	addq	$2, %rax
	movl	%edx, (%rsi,%rcx,4)
	addl	$1, %edx
	cmpl	%edi, %edx
	jne	.L703
.L702:
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %rdx
	movq	%r13, %rsi
	movl	%r9d, %ecx
	leaq	_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi(%rip), %rbx
	movq	%rbx, 24(%rax)
	addq	$56, %rsp
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi
.L720:
	.cfi_restore_state
	leaq	42380(%rcx), %rdi
	movq	$-1, %rax
	movq	$-1, 42372(%rcx)
	movq	$-1, 43388(%rcx)
	andq	$-8, %rdi
	movq	%rsi, %rcx
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	jmp	.L700
	.cfi_endproc
.LFE30862:
	.size	_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv,"axG",@progbits,_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv
	.type	_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv, @function
_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv:
.LFB31139:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rsi
	movslq	32(%rdi), %r13
	movq	8(%rdi), %r9
	movq	(%rdi), %rdi
	movq	%rsi, %rcx
	movslq	%esi, %r11
	movl	%esi, %r10d
	movq	%rsi, -64(%rbp)
	leaq	43396(%rdi), %rdx
	addq	$44400, %rdi
	movl	%esi, %r8d
	subl	%r13d, %r10d
	leaq	0(,%r13,4), %rax
	movq	%rdx, %rbx
	leaq	0(,%r11,4), %rsi
	subq	%rax, %rbx
	subq	%rax, %rdi
	movl	%ecx, %eax
	leaq	(%rbx,%rsi), %r14
	addl	$1, %eax
	addq	%rdi, %rsi
	cmpl	%ecx, %r13d
	jge	.L722
	movl	%ecx, -52(%rbp)
	subl	$1, %ecx
	movq	%r13, %r12
	subl	%r13d, %ecx
	cmpl	$2, %ecx
	jbe	.L746
	movl	%r10d, %ecx
	movd	%r10d, %xmm1
	shrl	$2, %ecx
	pshufd	$0, %xmm1, %xmm0
	salq	$4, %rcx
	addq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L724:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L724
	movl	%r10d, %ecx
	andl	$-4, %ecx
	leal	(%rcx,%r12), %edx
	cmpl	%r10d, %ecx
	je	.L725
.L723:
	movslq	%edx, %rcx
	movq	-64(%rbp), %r15
	movl	%r10d, (%rbx,%rcx,4)
	leal	1(%rdx), %ecx
	cmpl	%ecx, %r15d
	jle	.L725
	movslq	%ecx, %rcx
	addl	$2, %edx
	movl	%r10d, (%rbx,%rcx,4)
	cmpl	%r15d, %edx
	jge	.L725
	movslq	%edx, %rdx
	movl	%r10d, (%rbx,%rdx,4)
.L725:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	movzbl	-1(%r9,%r11), %r15d
	movl	-64(%rbp), %esi
	movl	%r15d, %r11d
	.p2align 4,,10
	.p2align 3
.L745:
	cmpl	%eax, %r8d
	jl	.L727
	.p2align 4,,10
	.p2align 3
.L731:
	movslq	%eax, %rdx
	cmpb	%r11b, -1(%r9,%rdx)
	je	.L727
	leaq	(%rbx,%rdx,4), %rcx
	cmpl	%r10d, (%rcx)
	je	.L752
	movl	(%rdi,%rdx,4), %eax
	cmpl	%r8d, %eax
	jle	.L731
.L727:
	subl	$1, %esi
	leal	-1(%rax), %edx
	movslq	%esi, %rcx
	movl	%edx, (%rdi,%rcx,4)
	cmpl	%r8d, %edx
	je	.L753
.L732:
	cmpl	%esi, %r12d
	jge	.L726
.L755:
	movslq	%esi, %rax
	movzbl	-1(%r9,%rax), %r11d
	movl	%edx, %eax
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L752:
	subl	%esi, %eax
	movl	%eax, (%rcx)
	movl	(%rdi,%rdx,4), %eax
	cmpl	%r8d, %eax
	jle	.L731
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L753:
	cmpl	%esi, %r12d
	jl	.L738
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L754:
	cmpl	%r10d, (%r14)
	jne	.L735
	movl	-52(%rbp), %edx
	subl	%ecx, %edx
	movl	%edx, (%r14)
.L735:
	movl	%r8d, -4(%rdi,%rcx,4)
	subq	$1, %rcx
	subl	$1, %esi
	cmpl	%ecx, %r12d
	jge	.L733
.L738:
	movl	%ecx, %esi
	cmpb	%r15b, -1(%r9,%rcx)
	jne	.L754
	cmpl	%r12d, %ecx
	jle	.L721
	subl	$1, %esi
	leal	-2(%rax), %edx
	movslq	%esi, %rax
	movl	%edx, (%rdi,%rax,4)
	cmpl	%esi, %r12d
	jl	.L755
	.p2align 4,,10
	.p2align 3
.L726:
	cmpl	-64(%rbp), %edx
	jge	.L721
	.p2align 4,,10
	.p2align 3
.L740:
	cmpl	%r10d, (%rbx,%r13,4)
	jne	.L742
	movl	%edx, %eax
	subl	%r12d, %eax
	movl	%eax, (%rbx,%r13,4)
.L742:
	cmpl	%r13d, %edx
	je	.L756
	addq	$1, %r13
	cmpl	%r13d, %r8d
	jge	.L740
.L721:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L756:
	.cfi_restore_state
	movslq	%edx, %rdx
	addq	$1, %r13
	movl	(%rdi,%rdx,4), %edx
	cmpl	%r13d, %r8d
	jge	.L740
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L733:
	.cfi_restore_state
	movl	%r8d, %edx
	jmp	.L732
.L722:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L746:
	.cfi_restore_state
	movl	%r13d, %edx
	jmp	.L723
	.cfi_endproc
.LFE31139:
	.size	_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv, .-_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv
	.section	.text._ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi:
.LFB31038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	movq	8(%rdi), %r13
	movq	%rdx, -88(%rbp)
	movq	(%rdi), %r10
	movq	%rdi, -80(%rbp)
	leal	-1(%rax), %edx
	movl	%eax, %r15d
	movl	%eax, %edi
	movl	%eax, -56(%rbp)
	movslq	%edx, %r8
	leaq	42372(%r10), %r11
	movl	%edx, %ebx
	negl	%edi
	movzbl	0(%r13,%r8), %r9d
	movl	%r14d, %r8d
	subl	%eax, %r8d
	subl	42372(%r10,%r9,4), %ebx
	movl	$1, %r10d
	movl	%ebx, -64(%rbp)
	leal	-2(%rax), %ebx
	movl	%r10d, %eax
	subl	%r15d, %eax
	movl	%ebx, -60(%rbp)
	movl	%eax, -68(%rbp)
.L773:
	cmpl	%r8d, %ecx
	jle	.L762
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L774:
	cmpw	$255, %bx
	ja	.L760
	movl	%edx, %ebx
	subl	(%r11,%rax,4), %ebx
	movl	%ebx, %eax
	addl	%ebx, %ecx
	movl	%r10d, %ebx
	subl	%eax, %ebx
	addl	%ebx, %edi
	cmpl	%r8d, %ecx
	jg	.L769
.L762:
	leal	(%rdx,%rcx), %eax
	cltq
	movzwl	(%rsi,%rax,2), %ebx
	movq	%rbx, %rax
	cmpl	%ebx, %r9d
	jne	.L774
	movslq	-60(%rbp), %rax
	testl	%eax, %eax
	js	.L770
	movslq	%ecx, %rbx
	leaq	(%rsi,%rbx,2), %r15
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L775:
	subq	$1, %rax
	testl	%eax, %eax
	js	.L770
.L764:
	movzbl	0(%r13,%rax), %r12d
	movzwl	(%r15,%rax,2), %ebx
	movl	%eax, %r14d
	cmpl	%ebx, %r12d
	je	.L775
	movl	-56(%rbp), %eax
	movl	-64(%rbp), %ebx
	subl	%r14d, %eax
	addl	%ebx, %ecx
	subl	%ebx, %eax
	addl	%eax, %edi
	testl	%edi, %edi
	jle	.L773
	movq	-80(%rbp), %rbx
	movl	%ecx, -60(%rbp)
	movq	%rsi, -56(%rbp)
	movq	%rbx, %rdi
	call	_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv
	movl	-60(%rbp), %ecx
	movq	-56(%rbp), %rsi
	movq	%rbx, %rdi
	leaq	_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	-88(%rbp), %rdx
	movq	%rax, 24(%rbx)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi
	.p2align 4,,10
	.p2align 3
.L769:
	.cfi_restore_state
	addq	$56, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L770:
	.cfi_restore_state
	addq	$56, %rsp
	movl	%ecx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L760:
	.cfi_restore_state
	addl	-56(%rbp), %ecx
	addl	-68(%rbp), %edi
	jmp	.L773
	.cfi_endproc
.LFE31038:
	.size	_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi:
.LFB30866:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	16(%rdi), %rcx
	movq	8(%rdi), %r14
	movq	%rdi, -72(%rbp)
	movq	%rdx, -88(%rbp)
	movl	%ecx, %eax
	subl	%ecx, %ebx
	movq	%rcx, -80(%rbp)
	movl	%ecx, -56(%rbp)
	sall	$2, %eax
	cmpl	%ebx, %r9d
	jg	.L781
	movl	$-9, %edx
	movq	%rsi, %r12
	subl	%eax, %edx
	movl	%edx, -60(%rbp)
	testl	%edx, %edx
	jg	.L779
	movzbl	(%r14), %r15d
	leal	1(%rbx), %eax
	movl	%eax, -52(%rbp)
	movl	%r15d, %r13d
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L807:
	andq	$-2, %rax
	subq	%r12, %rax
	sarq	%rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	leaq	(%r12,%rdx,2), %rdx
	cmpw	%r13w, (%rdx)
	je	.L782
	leal	1(%rax), %r9d
	cmpl	%eax, %ebx
	jle	.L781
.L783:
	movl	-52(%rbp), %edx
	movl	%r15d, %esi
	subl	%r9d, %edx
	movslq	%r9d, %r9
	movslq	%edx, %rdx
	leaq	(%r12,%r9,2), %rdi
	addq	%rdx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L807
.L781:
	movl	$-1, %r9d
.L776:
	addq	$56, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L782:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L781
	movl	-56(%rbp), %r8d
	movl	$1, %eax
	jmp	.L786
	.p2align 4,,10
	.p2align 3
.L808:
	leal	1(%rax), %ecx
	addq	$1, %rax
	cmpl	%eax, %r8d
	jle	.L785
.L786:
	movzbl	(%r14,%rax), %edi
	movzwl	(%rdx,%rax,2), %esi
	movl	%eax, %ecx
	cmpl	%esi, %edi
	je	.L808
.L785:
	cmpl	-56(%rbp), %ecx
	je	.L776
	addl	$1, %r9d
	addl	-60(%rbp), %ecx
	cmpl	%r9d, %ebx
	jl	.L781
	leal	1(%rcx), %eax
	movl	%eax, -60(%rbp)
	testl	%eax, %eax
	jle	.L783
.L779:
	movq	-72(%rbp), %rax
	movq	(%rax), %rdx
	movl	32(%rax), %r10d
	leaq	42372(%rdx), %rsi
	testl	%r10d, %r10d
	je	.L809
	leal	-1(%r10), %eax
	addq	$43396, %rdx
	movd	%eax, %xmm0
	movq	%rsi, %rax
	pshufd	$0, %xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L791:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L791
.L790:
	movl	-80(%rbp), %eax
	subl	$1, %eax
	cmpl	%r10d, %eax
	jle	.L792
	movl	-80(%rbp), %edx
	movslq	%r10d, %rcx
	movq	-72(%rbp), %r8
	leaq	1(%rcx), %rax
	subl	$2, %edx
	subl	%r10d, %edx
	addq	%rax, %rdx
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L810:
	addq	$1, %rax
.L793:
	movq	8(%r8), %rdi
	movzbl	(%rdi,%rcx), %edi
	movl	%ecx, (%rsi,%rdi,4)
	movq	%rax, %rcx
	cmpq	%rdx, %rax
	jne	.L810
.L792:
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %rdx
	movq	%r12, %rsi
	movl	%r9d, %ecx
	leaq	_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi(%rip), %rbx
	movq	%rbx, 24(%rax)
	addq	$56, %rsp
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi
.L809:
	.cfi_restore_state
	leaq	42380(%rdx), %rdi
	movq	%rsi, %rcx
	movq	$-1, %rax
	movq	$-1, 42372(%rdx)
	movq	$-1, 43388(%rdx)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	jmp	.L790
	.cfi_endproc
.LFE30866:
	.size	_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv,"axG",@progbits,_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv
	.type	_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv, @function
_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv:
.LFB31144:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r15
	movslq	32(%rdi), %r13
	movq	8(%rdi), %r9
	movq	(%rdi), %rdi
	leaq	0(,%r13,4), %rax
	movslq	%r15d, %r11
	movl	%r15d, %r10d
	leaq	43396(%rdi), %rdx
	addq	$44400, %rdi
	leaq	0(,%r11,4), %rsi
	subl	%r13d, %r10d
	movq	%rdx, %rbx
	subq	%rax, %rdi
	subq	%rax, %rbx
	leal	1(%r15), %eax
	leaq	(%rbx,%rsi), %r14
	addq	%rdi, %rsi
	cmpl	%r15d, %r13d
	jge	.L812
	leal	-1(%r15), %ecx
	movl	%r15d, -52(%rbp)
	movl	%r15d, %r8d
	movq	%r13, %r12
	subl	%r13d, %ecx
	cmpl	$2, %ecx
	jbe	.L836
	movl	%r10d, %ecx
	movd	%r10d, %xmm1
	shrl	$2, %ecx
	pshufd	$0, %xmm1, %xmm0
	salq	$4, %rcx
	addq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L814:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L814
	movl	%r10d, %ecx
	andl	$-4, %ecx
	leal	(%rcx,%r12), %edx
	cmpl	%r10d, %ecx
	je	.L815
.L813:
	movslq	%edx, %rcx
	movl	%r10d, (%rbx,%rcx,4)
	leal	1(%rdx), %ecx
	cmpl	%ecx, %r15d
	jle	.L815
	movslq	%ecx, %rcx
	addl	$2, %edx
	movl	%r10d, (%rbx,%rcx,4)
	cmpl	%r15d, %edx
	jge	.L815
	movslq	%edx, %rdx
	movl	%r10d, (%rbx,%rdx,4)
.L815:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	movzwl	-2(%r9,%r11,2), %ecx
	movl	%r15d, %esi
	movq	%r13, -64(%rbp)
	movl	%ecx, %r11d
	movl	%ecx, %r13d
	.p2align 4,,10
	.p2align 3
.L841:
	cmpl	%eax, %r8d
	jl	.L817
.L821:
	movslq	%eax, %rdx
	cmpw	%r11w, -2(%r9,%rdx,2)
	je	.L817
	leaq	(%rbx,%rdx,4), %rcx
	cmpl	%r10d, (%rcx)
	je	.L843
	movl	(%rdi,%rdx,4), %eax
	cmpl	%r8d, %eax
	jle	.L821
.L817:
	subl	$1, %esi
	leal	-1(%rax), %edx
	movslq	%esi, %rcx
	movl	%edx, (%rdi,%rcx,4)
	cmpl	%r8d, %edx
	je	.L844
.L822:
	cmpl	%esi, %r12d
	jge	.L816
.L846:
	movslq	%esi, %rax
	movzwl	-2(%r9,%rax,2), %r11d
	movl	%edx, %eax
	jmp	.L841
	.p2align 4,,10
	.p2align 3
.L843:
	subl	%esi, %eax
	movl	%eax, (%rcx)
	movl	(%rdi,%rdx,4), %eax
	jmp	.L841
	.p2align 4,,10
	.p2align 3
.L844:
	cmpl	%esi, %r12d
	jl	.L828
	jmp	.L823
	.p2align 4,,10
	.p2align 3
.L845:
	cmpl	%r10d, (%r14)
	jne	.L825
	movl	-52(%rbp), %edx
	subl	%ecx, %edx
	movl	%edx, (%r14)
.L825:
	movl	%r8d, -4(%rdi,%rcx,4)
	subq	$1, %rcx
	subl	$1, %esi
	cmpl	%ecx, %r12d
	jge	.L823
.L828:
	movl	%ecx, %esi
	cmpw	%r13w, -2(%r9,%rcx,2)
	jne	.L845
	cmpl	%r12d, %ecx
	jle	.L811
	subl	$1, %esi
	leal	-2(%rax), %edx
	movslq	%esi, %rax
	movl	%edx, (%rdi,%rax,4)
	cmpl	%esi, %r12d
	jl	.L846
	.p2align 4,,10
	.p2align 3
.L816:
	movq	-64(%rbp), %r13
	cmpl	%r15d, %edx
	jge	.L811
	.p2align 4,,10
	.p2align 3
.L830:
	cmpl	%r10d, (%rbx,%r13,4)
	jne	.L832
	movl	%edx, %eax
	subl	%r12d, %eax
	movl	%eax, (%rbx,%r13,4)
.L832:
	cmpl	%r13d, %edx
	je	.L847
	addq	$1, %r13
	cmpl	%r13d, %r8d
	jge	.L830
.L811:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L847:
	.cfi_restore_state
	movslq	%edx, %rdx
	addq	$1, %r13
	movl	(%rdi,%rdx,4), %edx
	cmpl	%r13d, %r8d
	jge	.L830
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L823:
	.cfi_restore_state
	movl	%r8d, %edx
	jmp	.L822
.L812:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L836:
	.cfi_restore_state
	movl	%r13d, %edx
	jmp	.L813
	.cfi_endproc
.LFE31144:
	.size	_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv, .-_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv
	.section	.text._ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi:
.LFB31043:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	movq	8(%rdi), %rbx
	movq	%rdx, -72(%rbp)
	movq	(%rdi), %r10
	leal	-1(%rax), %edx
	movl	%eax, %r8d
	movl	%eax, -60(%rbp)
	movslq	%edx, %r9
	leaq	42372(%r10), %rdi
	movl	%edx, %r15d
	negl	%r8d
	movzwl	(%rbx,%r9,2), %r9d
	movzbl	%r9b, %r11d
	subl	42372(%r10,%r11,4), %r15d
	movl	%r14d, %r10d
	movl	$1, %r11d
	movl	%r15d, -56(%rbp)
	subl	%eax, %r10d
	leal	-2(%rax), %r15d
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L863:
	movzbl	%al, %eax
	movl	%edx, %r14d
	subl	(%rdi,%rax,4), %r14d
	movl	%r14d, %eax
	addl	%r14d, %ecx
	movl	%r11d, %r14d
	subl	%eax, %r14d
	addl	%r14d, %r8d
.L861:
	cmpl	%r10d, %ecx
	jg	.L862
	leal	(%rdx,%rcx), %eax
	cltq
	movzwl	(%rsi,%rax,2), %eax
	cmpw	%r9w, %ax
	jne	.L863
	testl	%r15d, %r15d
	js	.L858
	movslq	%ecx, %r13
	movl	%edx, -64(%rbp)
	movslq	%r15d, %rax
	leaq	(%rsi,%r13,2), %r14
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L864:
	subq	$1, %rax
	testl	%eax, %eax
	js	.L858
.L853:
	movzwl	(%r14,%rax,2), %edx
	movl	%eax, %r13d
	cmpw	%dx, (%rbx,%rax,2)
	je	.L864
	movl	-60(%rbp), %eax
	movl	-56(%rbp), %r14d
	movl	-64(%rbp), %edx
	subl	%r13d, %eax
	addl	%r14d, %ecx
	subl	%r14d, %eax
	addl	%eax, %r8d
	testl	%r8d, %r8d
	jle	.L861
	movq	%r12, %rdi
	movl	%ecx, -60(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv
	movl	-60(%rbp), %ecx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	leaq	_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	-72(%rbp), %rdx
	movq	%rax, 24(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi
	.p2align 4,,10
	.p2align 3
.L862:
	.cfi_restore_state
	addq	$40, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L858:
	.cfi_restore_state
	addq	$40, %rsp
	movl	%ecx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE31043:
	.size	_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi:
.LFB30870:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	16(%rdi), %rsi
	movq	8(%rdi), %r15
	movq	%rdi, -72(%rbp)
	movq	%rdx, -88(%rbp)
	movl	%esi, %eax
	subl	%esi, %ebx
	movq	%rsi, -80(%rbp)
	movl	%esi, -56(%rbp)
	sall	$2, %eax
	cmpl	%ebx, %ecx
	jg	.L870
	movl	$-9, %edx
	movl	%ecx, %r9d
	subl	%eax, %edx
	movl	%edx, -60(%rbp)
	testl	%edx, %edx
	jg	.L868
	movzwl	(%r15), %r14d
	leal	1(%rbx), %eax
	movl	%eax, -52(%rbp)
	movl	%r14d, %eax
	movzbl	%ah, %eax
	cmpb	%al, %r14b
	cmova	%r14d, %eax
	movzbl	%al, %r12d
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L897:
	andq	$-2, %rax
	subq	%r13, %rax
	sarq	%rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	leaq	0(%r13,%rdx,2), %rdx
	cmpw	%r14w, (%rdx)
	je	.L871
	leal	1(%rax), %r9d
	cmpl	%eax, %ebx
	jle	.L870
.L872:
	movl	-52(%rbp), %edx
	movl	%r12d, %esi
	subl	%r9d, %edx
	movslq	%r9d, %r9
	movslq	%edx, %rdx
	leaq	0(%r13,%r9,2), %rdi
	addq	%rdx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L897
.L870:
	movl	$-1, %r9d
.L865:
	addq	$56, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L871:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L870
	movl	-56(%rbp), %esi
	movl	$1, %eax
	jmp	.L875
	.p2align 4,,10
	.p2align 3
.L898:
	leal	1(%rax), %ecx
	addq	$1, %rax
	cmpl	%eax, %esi
	jle	.L874
.L875:
	movzwl	(%rdx,%rax,2), %edi
	movl	%eax, %ecx
	cmpw	%di, (%r15,%rax,2)
	je	.L898
.L874:
	cmpl	-56(%rbp), %ecx
	je	.L865
	addl	$1, %r9d
	addl	-60(%rbp), %ecx
	cmpl	%r9d, %ebx
	jl	.L870
	leal	1(%rcx), %eax
	movl	%eax, -60(%rbp)
	testl	%eax, %eax
	jle	.L872
.L868:
	movq	-72(%rbp), %rax
	movq	(%rax), %rcx
	movl	32(%rax), %edx
	leaq	42372(%rcx), %rsi
	testl	%edx, %edx
	je	.L899
	leal	-1(%rdx), %eax
	addq	$43396, %rcx
	movd	%eax, %xmm0
	movq	%rsi, %rax
	pshufd	$0, %xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L880:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L880
.L879:
	movl	-80(%rbp), %edi
	movslq	%edx, %rax
	movq	-72(%rbp), %r8
	addq	%rax, %rax
	subl	$1, %edi
	cmpl	%edx, %edi
	jle	.L881
	.p2align 4,,10
	.p2align 3
.L882:
	movq	8(%r8), %rcx
	movzbl	(%rcx,%rax), %ecx
	addq	$2, %rax
	movl	%edx, (%rsi,%rcx,4)
	addl	$1, %edx
	cmpl	%edi, %edx
	jne	.L882
.L881:
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %rdx
	movq	%r13, %rsi
	movl	%r9d, %ecx
	leaq	_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi(%rip), %rbx
	movq	%rbx, 24(%rax)
	addq	$56, %rsp
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi
.L899:
	.cfi_restore_state
	leaq	42380(%rcx), %rdi
	movq	$-1, %rax
	movq	$-1, 42372(%rcx)
	movq	$-1, 43388(%rcx)
	andq	$-8, %rdi
	movq	%rsi, %rcx
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	jmp	.L879
	.cfi_endproc
.LFE30870:
	.size	_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal17ExternalReference6CreateEPNS_11ApiFunctionENS1_4TypeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal17ExternalReference6CreateEPNS_11ApiFunctionENS1_4TypeE, @function
_GLOBAL__sub_I__ZN2v88internal17ExternalReference6CreateEPNS_11ApiFunctionENS1_4TypeE:
.LFB32184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE32184:
	.size	_GLOBAL__sub_I__ZN2v88internal17ExternalReference6CreateEPNS_11ApiFunctionENS1_4TypeE, .-_GLOBAL__sub_I__ZN2v88internal17ExternalReference6CreateEPNS_11ApiFunctionENS1_4TypeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal17ExternalReference6CreateEPNS_11ApiFunctionENS1_4TypeE
	.section	.rodata._ZN2v88internalL22double_negate_constantE,"a"
	.align 16
	.type	_ZN2v88internalL22double_negate_constantE, @object
	.size	_ZN2v88internalL22double_negate_constantE, 16
_ZN2v88internalL22double_negate_constantE:
	.quad	-9223372036854775808
	.quad	-9223372036854775808
	.section	.rodata._ZN2v88internalL24double_absolute_constantE,"a"
	.align 16
	.type	_ZN2v88internalL24double_absolute_constantE, @object
	.size	_ZN2v88internalL24double_absolute_constantE, 16
_ZN2v88internalL24double_absolute_constantE:
	.quad	9223372036854775807
	.quad	9223372036854775807
	.section	.rodata._ZN2v88internalL21float_negate_constantE,"a"
	.align 16
	.type	_ZN2v88internalL21float_negate_constantE, @object
	.size	_ZN2v88internalL21float_negate_constantE, 16
_ZN2v88internalL21float_negate_constantE:
	.long	-2147483648
	.long	-2147483648
	.long	-2147483648
	.long	-2147483648
	.section	.rodata._ZN2v88internalL23float_absolute_constantE,"a"
	.align 16
	.type	_ZN2v88internalL23float_absolute_constantE, @object
	.size	_ZN2v88internalL23float_absolute_constantE, 16
_ZN2v88internalL23float_absolute_constantE:
	.long	2147483647
	.long	2147483647
	.long	2147483647
	.long	2147483647
	.section	.rodata._ZN2v88internalL27double_uint32_bias_constantE,"a"
	.align 8
	.type	_ZN2v88internalL27double_uint32_bias_constantE, @object
	.size	_ZN2v88internalL27double_uint32_bias_constantE, 8
_ZN2v88internalL27double_uint32_bias_constantE:
	.long	0
	.long	1106247680
	.section	.rodata._ZN2v88internalL28double_the_hole_nan_constantE,"a"
	.align 8
	.type	_ZN2v88internalL28double_the_hole_nan_constantE, @object
	.size	_ZN2v88internalL28double_the_hole_nan_constantE, 8
_ZN2v88internalL28double_the_hole_nan_constantE:
	.quad	-2251799814209537
	.section	.rodata._ZN2v88internalL24double_one_half_constantE,"a"
	.align 8
	.type	_ZN2v88internalL24double_one_half_constantE, @object
	.size	_ZN2v88internalL24double_one_half_constantE, 8
_ZN2v88internalL24double_one_half_constantE:
	.long	0
	.long	1071644672
	.section	.rodata._ZN2v88internalL23double_min_int_constantE,"a"
	.align 8
	.type	_ZN2v88internalL23double_min_int_constantE, @object
	.size	_ZN2v88internalL23double_min_int_constantE, 8
_ZN2v88internalL23double_min_int_constantE:
	.long	0
	.long	-1042284544
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
