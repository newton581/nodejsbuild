	.file	"module-instantiate.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5068:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5068:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5069:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5069:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5071:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5071:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskD2Ev, @function
_ZN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskD2Ev:
.LFB21998:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE21998:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskD2Ev, .-_ZN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskD2Ev
	.set	_ZN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskD1Ev,_ZN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskD2Ev
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskD0Ev, @function
_ZN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskD0Ev:
.LFB22000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$80, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE22000:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskD0Ev, .-_ZN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskD0Ev
	.section	.text._ZThn32_N2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskD1Ev,"ax",@progbits
	.p2align 4
	.type	_ZThn32_N2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskD1Ev, @function
_ZThn32_N2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskD1Ev:
.LFB23474:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	subq	$32, %rdi
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE23474:
	.size	_ZThn32_N2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskD1Ev, .-_ZThn32_N2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskD1Ev
	.section	.text._ZThn32_N2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskD0Ev,"ax",@progbits
	.p2align 4
	.type	_ZThn32_N2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskD0Ev, @function
_ZThn32_N2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskD0Ev:
.LFB23475:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-32(%rdi), %r12
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	movq	%r12, %rdi
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$80, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23475:
	.size	_ZThn32_N2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskD0Ev, .-_ZThn32_N2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskD0Ev
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L17
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L20
.L11:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L11
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilderC2EPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENSA_INS0_13JSArrayBufferEEE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vector::reserve"
	.section	.text._ZN2v88internal4wasm15InstanceBuilderC2EPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENSA_INS0_13JSArrayBufferEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15InstanceBuilderC2EPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENSA_INS0_13JSArrayBufferEEE
	.type	_ZN2v88internal4wasm15InstanceBuilderC2EPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENSA_INS0_13JSArrayBufferEEE, @function
_ZN2v88internal4wasm15InstanceBuilderC2EPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENSA_INS0_13JSArrayBufferEEE:
.LFB18583:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r10
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rcx), %rax
	movq	%rsi, (%rdi)
	movq	%r10, 32(%rdi)
	movq	23(%rax), %rax
	movq	%rcx, 40(%rdi)
	movq	%r8, 48(%rdi)
	movq	7(%rax), %rax
	movq	%r9, 56(%rdi)
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	192(%rax), %rdx
	movq	%rdx, 8(%rdi)
	movl	200(%rax), %edx
	movl	%edx, 16(%rdi)
	movzbl	204(%rax), %edx
	movb	%dl, 20(%rdi)
	movq	208(%rax), %rdx
	movq	$0, 128(%rdi)
	movups	%xmm0, 64(%rdi)
	movups	%xmm0, 80(%rdi)
	movups	%xmm0, 96(%rdi)
	movups	%xmm0, 112(%rdi)
	movq	216(%rdx), %r12
	subq	208(%rdx), %r12
	movq	%r12, %rax
	movq	%rdx, 24(%rdi)
	movabsq	$-6148914691236517205, %rdx
	sarq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$384307168202282325, %rdx
	cmpq	%rdx, %rax
	ja	.L35
	testq	%rax, %rax
	jne	.L36
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_Znwm@PLT
	movq	120(%rbx), %rsi
	movq	112(%rbx), %rdi
	movq	%rax, %r13
	cmpq	%rdi, %rsi
	je	.L24
	movq	%rax, %rcx
	movq	%rdi, %rdx
	.p2align 4,,10
	.p2align 3
.L25:
	movdqu	(%rdx), %xmm1
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm1, -24(%rcx)
	movq	-8(%rdx), %rax
	movq	%rax, -8(%rcx)
	cmpq	%rdx, %rsi
	jne	.L25
.L24:
	testq	%rdi, %rdi
	je	.L26
	call	_ZdlPv@PLT
.L26:
	movq	%r13, %xmm0
	addq	%r13, %r12
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, 128(%rbx)
	movups	%xmm0, 112(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L35:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE18583:
	.size	_ZN2v88internal4wasm15InstanceBuilderC2EPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENSA_INS0_13JSArrayBufferEEE, .-_ZN2v88internal4wasm15InstanceBuilderC2EPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENSA_INS0_13JSArrayBufferEEE
	.globl	_ZN2v88internal4wasm15InstanceBuilderC1EPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENSA_INS0_13JSArrayBufferEEE
	.set	_ZN2v88internal4wasm15InstanceBuilderC1EPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENSA_INS0_13JSArrayBufferEEE,_ZN2v88internal4wasm15InstanceBuilderC2EPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENSA_INS0_13JSArrayBufferEEE
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder20ExecuteStartFunctionEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"disabled-by-default-v8.wasm"
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder20ExecuteStartFunctionEv.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"InstanceBuilder::ExecuteStartFunction"
	.section	.text._ZN2v88internal4wasm15InstanceBuilder20ExecuteStartFunctionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15InstanceBuilder20ExecuteStartFunctionEv
	.type	_ZN2v88internal4wasm15InstanceBuilder20ExecuteStartFunctionEv, @function
_ZN2v88internal4wasm15InstanceBuilder20ExecuteStartFunctionEv:
.LFB18586:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	_ZZN2v88internal4wasm15InstanceBuilder20ExecuteStartFunctionEvE28trace_event_unique_atomic564(%rip), %r12
	testq	%r12, %r12
	je	.L61
.L39:
	movq	$0, -112(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L62
.L41:
	cmpq	$0, 104(%rbx)
	movl	$1, %r13d
	je	.L45
	movq	(%rbx), %r12
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	41088(%r12), %r15
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	(%rbx), %rdi
	movq	104(%rbx), %rsi
	leaq	88(%rdi), %rdx
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	movq	%r15, 41088(%r12)
	testq	%rax, %rax
	setne	%r13b
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r14
	je	.L45
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L45:
	leaq	-112(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L63
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L64
.L42:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L43
	movq	(%rdi), %rax
	call	*8(%rax)
.L43:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L44
	movq	(%rdi), %rax
	call	*8(%rax)
.L44:
	leaq	.LC2(%rip), %rax
	movq	%r12, -104(%rbp)
	movq	%rax, -96(%rbp)
	leaq	-104(%rbp), %rax
	movq	%r13, -88(%rbp)
	movq	%rax, -112(%rbp)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L61:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L65
.L40:
	movq	%r12, _ZZN2v88internal4wasm15InstanceBuilder20ExecuteStartFunctionEvE28trace_event_unique_atomic564(%rip)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L64:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC2(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L65:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L40
.L63:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18586:
	.size	_ZN2v88internal4wasm15InstanceBuilder20ExecuteStartFunctionEv, .-_ZN2v88internal4wasm15InstanceBuilder20ExecuteStartFunctionEv
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder12LookupImportEjNS0_6HandleINS0_6StringEEES5_.str1.1,"aMS",@progbits,1
.LC3:
	.string	"(location_) != nullptr"
.LC4:
	.string	"Check failed: %s."
.LC5:
	.string	"module not found"
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder12LookupImportEjNS0_6HandleINS0_6StringEEES5_.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"Import #%d module=\"%s\" error: %s"
	.align 8
.LC7:
	.string	"module is not an object or function"
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder12LookupImportEjNS0_6HandleINS0_6StringEEES5_.str1.1
.LC8:
	.string	"import not found"
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder12LookupImportEjNS0_6HandleINS0_6StringEEES5_.str1.8
	.align 8
.LC9:
	.string	"Import #%d module=\"%s\" function=\"%s\" error: %s"
	.section	.text._ZN2v88internal4wasm15InstanceBuilder12LookupImportEjNS0_6HandleINS0_6StringEEES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15InstanceBuilder12LookupImportEjNS0_6HandleINS0_6StringEEES5_
	.type	_ZN2v88internal4wasm15InstanceBuilder12LookupImportEjNS0_6HandleINS0_6StringEEES5_, @function
_ZN2v88internal4wasm15InstanceBuilder12LookupImportEjNS0_6HandleINS0_6StringEEES5_:
.LFB18587:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -248(%rbp)
	movq	48(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L145
	movq	(%rdx), %rax
	movq	(%rdi), %r15
	movq	%rdx, %rbx
	movq	%rdi, %r12
	movl	%esi, %r14d
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L146
.L69:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L76
.L78:
	movl	$-1, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %rcx
.L77:
	movq	(%rbx), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L79
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L79:
	movl	%eax, -224(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -212(%rbp)
	movq	(%rbx), %rax
	movq	%r15, -200(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	movq	%rbx, %rax
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L147
.L80:
	leaq	-224(%rbp), %r15
	movq	%rax, -192(%rbp)
	movq	%r15, %rdi
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%rcx, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L75:
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L148
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L84
.L87:
	movq	(%rbx), %rax
	xorl	%r8d, %r8d
	leaq	-144(%rbp), %rdi
	movq	%r15, %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movq	32(%r12), %r12
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	leaq	.LC7(%rip), %r8
.L144:
	movq	-144(%rbp), %rcx
	movq	%r12, %rdi
	movl	%r14d, %edx
	xorl	%eax, %eax
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal4wasm12ErrorThrower9TypeErrorEPKcz@PLT
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L104
.L142:
	call	_ZdaPv@PLT
.L104:
	xorl	%eax, %eax
.L83:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L149
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	movq	%rax, -144(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L70
	testb	$2, %al
	jne	.L69
.L70:
	leaq	-144(%rbp), %r10
	leaq	-232(%rbp), %rsi
	movq	%r10, %rdi
	movq	%r10, -256(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	testb	%al, %al
	je	.L69
	movq	0(%r13), %rax
	movl	-232(%rbp), %edx
	movq	-256(%rbp), %r10
	testb	$1, %al
	jne	.L72
.L74:
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r10, -264(%rbp)
	movl	%edx, -256(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movl	-256(%rbp), %edx
	movq	-264(%rbp), %r10
.L73:
	movabsq	$824633720832, %rcx
	movq	%r10, %rdi
	movq	%r15, -120(%rbp)
	leaq	-224(%rbp), %r15
	movl	$3, -144(%rbp)
	movq	%rcx, -132(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	%edx, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%rbx, -112(%rbp)
	movdqa	-128(%rbp), %xmm1
	movdqa	-144(%rbp), %xmm0
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L84:
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L87
	movq	-248(%rbp), %rcx
	movq	(%r12), %r8
	movq	(%rcx), %rdx
	movq	-1(%rdx), %rcx
	cmpw	$63, 11(%rcx)
	jbe	.L150
.L89:
	testb	$1, %al
	jne	.L96
.L98:
	movq	%r8, %rdi
	movl	$-1, %edx
	movq	%r13, %rsi
	movq	%r8, -256(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-256(%rbp), %r8
	movq	%rax, %rcx
.L97:
	movq	-248(%rbp), %rax
	movq	(%rax), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rsi
	cmpw	$64, 11(%rsi)
	je	.L151
.L99:
	movq	-248(%rbp), %rsi
	movl	%eax, -224(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -212(%rbp)
	movq	(%rsi), %rax
	movq	%r8, -200(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	movq	%rsi, %rax
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L152
.L100:
	movq	%r15, %rdi
	movq	%rax, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%rcx, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L95:
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	jne	.L83
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r15, %rsi
	movq	-248(%rbp), %rax
	leaq	-144(%rbp), %rdi
	movq	32(%r12), %r12
	movq	(%rax), %rax
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	(%rbx), %rax
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	-144(%rbp), %r13
	leaq	-232(%rbp), %rdi
	movl	$1, %edx
	leaq	-240(%rbp), %rsi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	%r12, %rdi
	movq	%r13, %r8
	movl	%r14d, %edx
	movq	-232(%rbp), %rcx
	leaq	.LC8(%rip), %r9
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	movq	-232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L103
	call	_ZdaPv@PLT
.L103:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L142
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L76:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L78
	movq	%r13, %rcx
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L148:
	movq	(%rbx), %rax
	xorl	%r8d, %r8d
	leaq	-144(%rbp), %rdi
	movq	%r15, %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movq	32(%r12), %r12
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	leaq	.LC5(%rip), %r8
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L72:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L74
	movq	%r13, %rax
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L145:
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L147:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rcx, -256(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-256(%rbp), %rcx
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L151:
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L96:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L98
	movq	%r13, %rcx
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L152:
	movq	%r8, %rdi
	movq	%rcx, -256(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-256(%rbp), %rcx
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L150:
	movq	%rdx, -144(%rbp)
	movl	7(%rdx), %eax
	testb	$1, %al
	jne	.L90
	testb	$2, %al
	jne	.L141
.L90:
	leaq	-144(%rbp), %r10
	leaq	-232(%rbp), %rsi
	movq	%r8, -264(%rbp)
	movq	%r10, %rdi
	movq	%r10, -256(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-256(%rbp), %r10
	movq	-264(%rbp), %r8
	testb	%al, %al
	je	.L141
	movq	0(%r13), %rax
	movl	-232(%rbp), %edx
	testb	$1, %al
	jne	.L92
.L94:
	movq	%r8, %rdi
	movq	%r13, %rsi
	movq	%r10, -272(%rbp)
	movl	%edx, -264(%rbp)
	movq	%r8, -256(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-256(%rbp), %r8
	movl	-264(%rbp), %edx
	movq	-272(%rbp), %r10
.L93:
	movabsq	$824633720832, %rcx
	movq	%r10, %rdi
	movq	%rax, -80(%rbp)
	movl	$3, -144(%rbp)
	movq	%rcx, -132(%rbp)
	movq	%r8, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movl	%edx, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movdqa	-144(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm6
	movq	-248(%rbp), %rax
	movaps	%xmm5, -224(%rbp)
	movdqa	-96(%rbp), %xmm5
	movq	%rax, -112(%rbp)
	movdqa	-112(%rbp), %xmm7
	movaps	%xmm6, -208(%rbp)
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L92:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L94
	movq	%r13, %rax
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L141:
	movq	0(%r13), %rax
	jmp	.L89
.L149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18587:
	.size	_ZN2v88internal4wasm15InstanceBuilder12LookupImportEjNS0_6HandleINS0_6StringEEES5_, .-_ZN2v88internal4wasm15InstanceBuilder12LookupImportEjNS0_6HandleINS0_6StringEEES5_
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder15LookupImportAsmEjNS0_6HandleINS0_6StringEEE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"missing imports object"
.LC11:
	.string	"not a data property"
	.section	.text.unlikely._ZN2v88internal4wasm15InstanceBuilder15LookupImportAsmEjNS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
.LCOLDB12:
	.section	.text._ZN2v88internal4wasm15InstanceBuilder15LookupImportAsmEjNS0_6HandleINS0_6StringEEE,"ax",@progbits
.LHOTB12:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15InstanceBuilder15LookupImportAsmEjNS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal4wasm15InstanceBuilder15LookupImportAsmEjNS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal4wasm15InstanceBuilder15LookupImportAsmEjNS0_6HandleINS0_6StringEEE:
.LFB18591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	48(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	testq	%r12, %r12
	je	.L197
	movq	(%rdi), %r14
	movq	%rdx, %rbx
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L198
.L158:
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L165
.L167:
	movl	$-1, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %rcx
.L166:
	movq	(%rbx), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L168
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L168:
	movl	%eax, -224(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -212(%rbp)
	movq	(%rbx), %rax
	movq	%r14, -200(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	movq	%rbx, %rax
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L199
.L169:
	leaq	-224(%rbp), %rdi
	movq	%rax, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r12, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%rcx, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L164:
	cmpl	$7, -220(%rbp)
	ja	.L180
	movl	-220(%rbp), %eax
	leaq	.L172(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder15LookupImportAsmEjNS0_6HandleINS0_6StringEEE,"a",@progbits
	.align 4
	.align 4
.L172:
	.long	.L171-.L172
	.long	.L171-.L172
	.long	.L171-.L172
	.long	.L171-.L172
	.long	.L174-.L172
	.long	.L171-.L172
	.long	.L173-.L172
	.long	.L171-.L172
	.section	.text._ZN2v88internal4wasm15InstanceBuilder15LookupImportAsmEjNS0_6HandleINS0_6StringEEE
	.p2align 4,,10
	.p2align 3
.L198:
	movq	%rax, -144(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L159
	testb	$2, %al
	jne	.L158
.L159:
	leaq	-144(%rbp), %r8
	leaq	-232(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r8, -248(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	testb	%al, %al
	je	.L158
	movq	(%r12), %rax
	movl	-232(%rbp), %edx
	movq	-248(%rbp), %r8
	testb	$1, %al
	jne	.L161
.L163:
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r8, -256(%rbp)
	movl	%edx, -248(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movl	-248(%rbp), %edx
	movq	-256(%rbp), %r8
.L162:
	movabsq	$824633720832, %rcx
	movq	%r8, %rdi
	movq	%r14, -120(%rbp)
	movl	$3, -144(%rbp)
	movq	%rcx, -132(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r12, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	%edx, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%rbx, -112(%rbp)
	movdqa	-128(%rbp), %xmm1
	movdqa	-144(%rbp), %xmm0
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L197:
	movq	32(%rdi), %r12
	leaq	-224(%rbp), %rsi
	xorl	%r8d, %r8d
	leaq	-144(%rbp), %rdi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	%r12, %rdi
	movl	%r13d, %edx
	xorl	%eax, %eax
	movq	-144(%rbp), %rcx
	leaq	.LC10(%rip), %r8
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L155
	call	_ZdaPv@PLT
.L155:
	xorl	%eax, %eax
.L156:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L200
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	32(%r15), %r12
	xorl	%r8d, %r8d
	movl	$1, %ecx
	leaq	-144(%rbp), %rdi
	leaq	-232(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	%r12, %rdi
	movl	%r13d, %edx
	xorl	%eax, %eax
	movq	-144(%rbp), %rcx
	leaq	.LC11(%rip), %r8
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L175
	call	_ZdaPv@PLT
.L175:
	xorl	%eax, %eax
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L173:
	leaq	-224(%rbp), %rdi
	call	_ZNK2v88internal14LookupIterator12GetDataValueEv@PLT
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L174:
	movq	(%r15), %rax
	addq	$88, %rax
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L161:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L163
	movq	%r12, %rax
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L165:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L167
	movq	%r12, %rcx
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L199:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rcx, -248(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-248(%rbp), %rcx
	jmp	.L169
.L200:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal4wasm15InstanceBuilder15LookupImportAsmEjNS0_6HandleINS0_6StringEEE
	.cfi_startproc
	.type	_ZN2v88internal4wasm15InstanceBuilder15LookupImportAsmEjNS0_6HandleINS0_6StringEEE.cold, @function
_ZN2v88internal4wasm15InstanceBuilder15LookupImportAsmEjNS0_6HandleINS0_6StringEEE.cold:
.LFSB18591:
.L180:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	xorl	%eax, %eax
	jmp	.L156
	.cfi_endproc
.LFE18591:
	.section	.text._ZN2v88internal4wasm15InstanceBuilder15LookupImportAsmEjNS0_6HandleINS0_6StringEEE
	.size	_ZN2v88internal4wasm15InstanceBuilder15LookupImportAsmEjNS0_6HandleINS0_6StringEEE, .-_ZN2v88internal4wasm15InstanceBuilder15LookupImportAsmEjNS0_6HandleINS0_6StringEEE
	.section	.text.unlikely._ZN2v88internal4wasm15InstanceBuilder15LookupImportAsmEjNS0_6HandleINS0_6StringEEE
	.size	_ZN2v88internal4wasm15InstanceBuilder15LookupImportAsmEjNS0_6HandleINS0_6StringEEE.cold, .-_ZN2v88internal4wasm15InstanceBuilder15LookupImportAsmEjNS0_6HandleINS0_6StringEEE.cold
.LCOLDE12:
	.section	.text._ZN2v88internal4wasm15InstanceBuilder15LookupImportAsmEjNS0_6HandleINS0_6StringEEE
.LHOTE12:
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder16LoadDataSegmentsENS0_6HandleINS0_18WasmInstanceObjectEEE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"unreachable code"
.LC14:
	.string	"data segment is out of bounds"
	.section	.text._ZN2v88internal4wasm15InstanceBuilder16LoadDataSegmentsENS0_6HandleINS0_18WasmInstanceObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15InstanceBuilder16LoadDataSegmentsENS0_6HandleINS0_18WasmInstanceObjectEEE
	.type	_ZN2v88internal4wasm15InstanceBuilder16LoadDataSegmentsENS0_6HandleINS0_18WasmInstanceObjectEEE, @function
_ZN2v88internal4wasm15InstanceBuilder16LoadDataSegmentsENS0_6HandleINS0_18WasmInstanceObjectEEE:
.LFB18592:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	232(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, -72(%rbp)
	movq	24(%rdi), %rax
	movq	168(%rax), %rbx
	movq	160(%rax), %r14
	leaq	-64(%rbp), %rax
	movq	%rax, -88(%rbp)
	cmpq	%rbx, %r14
	je	.L201
	movq	%rdi, %r15
	movq	%rsi, %r12
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L225:
	testl	%r8d, %r8d
	je	.L206
	cmpb	$0, 24(%r14)
	je	.L206
	movl	(%r14), %eax
	cmpl	$1, %eax
	jne	.L224
	movq	(%r12), %rax
	movq	-88(%rbp), %rdi
	movl	%r8d, -76(%rbp)
	movl	8(%r14), %r13d
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal18WasmInstanceObject6moduleEv@PLT
	movq	(%r12), %rdx
	salq	$5, %r13
	movl	-76(%rbp), %r8d
	addq	24(%rax), %r13
	movq	167(%rdx), %rsi
	movl	24(%r13), %eax
	movq	31(%rsi), %rsi
	movl	(%rsi,%rax), %eax
.L210:
	movq	31(%rdx), %r13
	movl	16(%r14), %esi
	movl	%eax, %edi
	addq	-72(%rbp), %rsi
	addq	23(%rdx), %rdi
	cmpl	%eax, %r13d
	jb	.L211
	subl	%eax, %r13d
	movl	%r8d, -76(%rbp)
	cmpl	%r13d, %r8d
	movl	%r13d, %edx
	cmovbe	%r8d, %edx
	call	_ZN2v88internal4wasm19memory_copy_wrapperEmmj@PLT
	movl	-76(%rbp), %r8d
	cmpl	%r13d, %r8d
	ja	.L216
.L206:
	addq	$32, %r14
	cmpq	%r14, %rbx
	je	.L201
.L214:
	cmpb	$0, 17(%r15)
	movl	20(%r14), %r8d
	jne	.L225
	testl	%r8d, %r8d
	je	.L206
	movl	(%r14), %eax
	cmpl	$1, %eax
	jne	.L226
	movq	(%r12), %rax
	movq	-88(%rbp), %rdi
	movl	%r8d, -76(%rbp)
	movl	8(%r14), %r13d
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal18WasmInstanceObject6moduleEv@PLT
	salq	$5, %r13
	movl	-76(%rbp), %r8d
	addq	24(%rax), %r13
	movq	(%r12), %rax
	movl	24(%r13), %edx
	movq	167(%rax), %rsi
	movq	31(%rsi), %rsi
	movl	(%rsi,%rdx), %edi
.L213:
	movl	16(%r14), %esi
	addq	23(%rax), %rdi
	movl	%r8d, %edx
	addq	$32, %r14
	addq	-72(%rbp), %rsi
	call	memcpy@PLT
	cmpq	%r14, %rbx
	jne	.L214
.L201:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L227
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_restore_state
	cmpl	$2, %eax
	jne	.L209
	movl	8(%r14), %edi
	movq	(%r12), %rax
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L224:
	cmpl	$2, %eax
	jne	.L209
	movl	8(%r14), %eax
	movq	(%r12), %rdx
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L211:
	xorl	%edx, %edx
	call	_ZN2v88internal4wasm19memory_copy_wrapperEmmj@PLT
.L216:
	movq	32(%r15), %rdi
	leaq	.LC14(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	jmp	.L201
.L209:
	leaq	.LC13(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L227:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18592:
	.size	_ZN2v88internal4wasm15InstanceBuilder16LoadDataSegmentsENS0_6HandleINS0_18WasmInstanceObjectEEE, .-_ZN2v88internal4wasm15InstanceBuilder16LoadDataSegmentsENS0_6HandleINS0_18WasmInstanceObjectEEE
	.section	.text._ZN2v88internal4wasm15InstanceBuilder16WriteGlobalValueERKNS1_10WasmGlobalEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15InstanceBuilder16WriteGlobalValueERKNS1_10WasmGlobalEd
	.type	_ZN2v88internal4wasm15InstanceBuilder16WriteGlobalValueERKNS1_10WasmGlobalEd, @function
_ZN2v88internal4wasm15InstanceBuilder16WriteGlobalValueERKNS1_10WasmGlobalEd:
.LFB18593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	(%rsi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpb	$3, %al
	je	.L229
	cmpb	$4, %al
	je	.L230
	cmpb	$1, %al
	je	.L267
	leaq	.LC13(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L229:
	comisd	.LC23(%rip), %xmm0
	jbe	.L264
	movsd	.LC24(%rip), %xmm1
	movl	.LC15(%rip), %ecx
	comisd	%xmm0, %xmm1
	jnb	.L244
	movl	.LC16(%rip), %ecx
.L244:
	movq	64(%rdi), %rdx
	movslq	24(%rsi), %rax
	testq	%rdx, %rdx
	je	.L247
	movq	(%rdx), %rdx
	movq	31(%rdx), %rdx
	movl	%ecx, (%rdx,%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	.cfi_restore_state
	movsd	.LC20(%rip), %xmm2
	movapd	%xmm0, %xmm1
	andpd	.LC19(%rip), %xmm1
	ucomisd	%xmm1, %xmm2
	jb	.L232
	movsd	.LC21(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L232
	comisd	.LC22(%rip), %xmm0
	jb	.L232
	cvttsd2sil	%xmm0, %edx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L232
	je	.L235
	.p2align 4,,10
	.p2align 3
.L232:
	movabsq	$9218868437227405312, %rdx
	movq	%xmm0, %rax
	testq	%rdx, %rax
	je	.L248
	movq	%xmm0, %r8
	xorl	%edx, %edx
	shrq	$52, %r8
	andl	$2047, %r8d
	movl	%r8d, %ecx
	subl	$1075, %ecx
	js	.L268
	cmpl	$31, %ecx
	jg	.L235
	movabsq	$4503599627370495, %rdx
	movabsq	$4503599627370496, %r8
	andq	%rax, %rdx
	addq	%r8, %rdx
	salq	%cl, %rdx
	movl	%edx, %edx
.L238:
	sarq	$63, %rax
	orl	$1, %eax
	imull	%eax, %edx
.L235:
	movq	64(%rdi), %rcx
	movslq	24(%rsi), %rax
	testq	%rcx, %rcx
	je	.L247
	movq	(%rcx), %rcx
	movq	31(%rcx), %rcx
	movl	%edx, (%rcx,%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	movq	64(%rdi), %rdx
	movslq	24(%rsi), %rax
	testq	%rdx, %rdx
	je	.L247
	movq	(%rdx), %rdx
	movq	31(%rdx), %rdx
	movq	%xmm0, (%rdx,%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L264:
	.cfi_restore_state
	movsd	.LC25(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L265
	comisd	.LC26(%rip), %xmm0
	movl	.LC17(%rip), %ecx
	jnb	.L244
	movl	.LC18(%rip), %ecx
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L268:
	cmpl	$-52, %ecx
	jl	.L235
	movabsq	$4503599627370495, %rdx
	movabsq	$4503599627370496, %rcx
	andq	%rax, %rdx
	addq	%rcx, %rdx
	movl	$1075, %ecx
	subl	%r8d, %ecx
	shrq	%cl, %rdx
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L247:
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L265:
	pxor	%xmm3, %xmm3
	cvtsd2ss	%xmm0, %xmm3
	movd	%xmm3, %ecx
	jmp	.L244
.L248:
	xorl	%edx, %edx
	jmp	.L235
	.cfi_endproc
.LFE18593:
	.size	_ZN2v88internal4wasm15InstanceBuilder16WriteGlobalValueERKNS1_10WasmGlobalEd, .-_ZN2v88internal4wasm15InstanceBuilder16WriteGlobalValueERKNS1_10WasmGlobalEd
	.section	.text._ZN2v88internal4wasm15InstanceBuilder16WriteGlobalValueERKNS1_10WasmGlobalEl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15InstanceBuilder16WriteGlobalValueERKNS1_10WasmGlobalEl
	.type	_ZN2v88internal4wasm15InstanceBuilder16WriteGlobalValueERKNS1_10WasmGlobalEl, @function
_ZN2v88internal4wasm15InstanceBuilder16WriteGlobalValueERKNS1_10WasmGlobalEl:
.LFB18594:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rcx
	movslq	24(%rsi), %rax
	testq	%rcx, %rcx
	je	.L274
	movq	(%rcx), %rcx
	movq	31(%rcx), %rcx
	movq	%rdx, (%rcx,%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18594:
	.size	_ZN2v88internal4wasm15InstanceBuilder16WriteGlobalValueERKNS1_10WasmGlobalEl, .-_ZN2v88internal4wasm15InstanceBuilder16WriteGlobalValueERKNS1_10WasmGlobalEl
	.section	.text._ZN2v88internal4wasm15InstanceBuilder16WriteGlobalValueERKNS1_10WasmGlobalENS0_6HandleINS0_16WasmGlobalObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15InstanceBuilder16WriteGlobalValueERKNS1_10WasmGlobalENS0_6HandleINS0_16WasmGlobalObjectEEE
	.type	_ZN2v88internal4wasm15InstanceBuilder16WriteGlobalValueERKNS1_10WasmGlobalENS0_6HandleINS0_16WasmGlobalObjectEEE, @function
_ZN2v88internal4wasm15InstanceBuilder16WriteGlobalValueERKNS1_10WasmGlobalENS0_6HandleINS0_16WasmGlobalObjectEEE:
.LFB18595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpb	$9, (%rsi)
	ja	.L276
	movzbl	(%rsi), %eax
	leaq	.L278(%rip), %rcx
	movq	%rsi, %rbx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder16WriteGlobalValueERKNS1_10WasmGlobalENS0_6HandleINS0_16WasmGlobalObjectEEE,"a",@progbits
	.align 4
	.align 4
.L278:
	.long	.L276-.L278
	.long	.L282-.L278
	.long	.L279-.L278
	.long	.L282-.L278
	.long	.L279-.L278
	.long	.L276-.L278
	.long	.L277-.L278
	.long	.L277-.L278
	.long	.L276-.L278
	.long	.L277-.L278
	.section	.text._ZN2v88internal4wasm15InstanceBuilder16WriteGlobalValueERKNS1_10WasmGlobalENS0_6HandleINS0_16WasmGlobalObjectEEE
	.p2align 4,,10
	.p2align 3
.L282:
	movq	(%rdx), %rax
	movq	23(%rax), %rdx
	movslq	43(%rax), %rax
	movq	31(%rdx), %rdx
	movl	(%rdx,%rax), %ecx
	movq	64(%rdi), %rdx
	movslq	24(%rsi), %rax
	testq	%rdx, %rdx
	je	.L285
	movq	(%rdx), %rdx
	movq	31(%rdx), %rdx
	movl	%ecx, (%rdx,%rax)
.L275:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L279:
	.cfi_restore_state
	movq	(%rdx), %rax
	movq	23(%rax), %rdx
	movslq	43(%rax), %rax
	movq	31(%rdx), %rdx
	movq	(%rdx,%rax), %rcx
	movq	64(%rdi), %rdx
	movslq	24(%rsi), %rax
	testq	%rdx, %rdx
	je	.L285
	movq	(%rdx), %rdx
	movq	31(%rdx), %rdx
	movq	%rcx, (%rdx,%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L277:
	.cfi_restore_state
	movq	72(%rdi), %rax
	movq	(%rdx), %rdx
	movq	(%rax), %r14
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %r13
	movslq	43(%rdx), %rax
	movq	31(%rdx), %rdx
	leal	16(,%rax,8), %eax
	subq	$37592, %r13
	cltq
	movq	-1(%rax,%rdx), %r12
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L286
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
.L287:
	movl	24(%rbx), %eax
	leal	16(,%rax,8), %eax
	cltq
	leaq	-1(%r14,%rax), %r13
	movq	%r12, 0(%r13)
	testb	$1, %r12b
	je	.L275
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L290
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L290:
	testb	$24, %al
	je	.L275
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L275
	popq	%rbx
	movq	%r12, %rdx
	movq	%r13, %rsi
	popq	%r12
	movq	%r14, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L286:
	.cfi_restore_state
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L312
.L288:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%r12, (%rax)
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L285:
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L276:
	leaq	.LC13(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L312:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L288
	.cfi_endproc
.LFE18595:
	.size	_ZN2v88internal4wasm15InstanceBuilder16WriteGlobalValueERKNS1_10WasmGlobalENS0_6HandleINS0_16WasmGlobalObjectEEE, .-_ZN2v88internal4wasm15InstanceBuilder16WriteGlobalValueERKNS1_10WasmGlobalENS0_6HandleINS0_16WasmGlobalObjectEEE
	.section	.text._ZN2v88internal4wasm15InstanceBuilder17WriteGlobalAnyRefERKNS1_10WasmGlobalENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15InstanceBuilder17WriteGlobalAnyRefERKNS1_10WasmGlobalENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal4wasm15InstanceBuilder17WriteGlobalAnyRefERKNS1_10WasmGlobalENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal4wasm15InstanceBuilder17WriteGlobalAnyRefERKNS1_10WasmGlobalENS0_6HandleINS0_6ObjectEEE:
.LFB18596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	72(%rdi), %rax
	movq	(%rdx), %r12
	movq	(%rax), %r13
	movl	24(%rsi), %eax
	leal	16(,%rax,8), %eax
	cltq
	leaq	-1(%r13,%rax), %r14
	movq	%r12, (%r14)
	testb	$1, %r12b
	je	.L313
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L325
	testb	$24, %al
	je	.L313
.L327:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L326
.L313:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L325:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L327
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L326:
	popq	%rbx
	movq	%r12, %rdx
	movq	%r14, %rsi
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE18596:
	.size	_ZN2v88internal4wasm15InstanceBuilder17WriteGlobalAnyRefERKNS1_10WasmGlobalENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal4wasm15InstanceBuilder17WriteGlobalAnyRefERKNS1_10WasmGlobalENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZNK2v88internal4wasm15InstanceBuilder24FindImportedMemoryBufferEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm15InstanceBuilder24FindImportedMemoryBufferEv
	.type	_ZNK2v88internal4wasm15InstanceBuilder24FindImportedMemoryBufferEv, @function
_ZNK2v88internal4wasm15InstanceBuilder24FindImportedMemoryBufferEv:
.LFB18598:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	208(%rax), %rsi
	movq	216(%rax), %rcx
	movabsq	$-6148914691236517205, %rax
	subq	%rsi, %rcx
	sarq	$3, %rcx
	imulq	%rax, %rcx
	testq	%rcx, %rcx
	je	.L329
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L330:
	addq	$1, %rdx
	addq	$24, %rax
	cmpq	%rcx, %rdx
	je	.L329
.L338:
	cmpb	$2, 16(%rsi,%rax)
	jne	.L330
	addq	112(%rdi), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L348
.L329:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L348:
	movq	-1(%rax), %rdx
	cmpw	$1101, 11(%rdx)
	jne	.L329
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rbx
	movq	23(%rax), %r12
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L349
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L335:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L349:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L350
.L337:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r12, (%rax)
	jmp	.L335
.L350:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L337
	.cfi_endproc
.LFE18598:
	.size	_ZNK2v88internal4wasm15InstanceBuilder24FindImportedMemoryBufferEv, .-_ZNK2v88internal4wasm15InstanceBuilder24FindImportedMemoryBufferEv
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder23ProcessImportedFunctionENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC27:
	.string	"function import requires a callable"
	.align 8
.LC28:
	.string	"imported function does not match the expected type"
	.section	.text._ZN2v88internal4wasm15InstanceBuilder23ProcessImportedFunctionENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15InstanceBuilder23ProcessImportedFunctionENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE
	.type	_ZN2v88internal4wasm15InstanceBuilder23ProcessImportedFunctionENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE, @function
_ZN2v88internal4wasm15InstanceBuilder23ProcessImportedFunctionENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE:
.LFB18602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rbp), %rax
	movq	(%rax), %rdi
	movq	%rdi, %r12
	notq	%r12
	andl	$1, %r12d
	je	.L352
.L355:
	movq	(%r9), %rax
	leaq	-128(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	leaq	-144(%rbp), %rsi
	movl	$1, %edx
	movq	32(%rbx), %r12
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	(%r14), %rax
	movq	-128(%rbp), %rbx
	xorl	%r8d, %r8d
	leaq	-152(%rbp), %rdi
	movl	$1, %ecx
	movl	$1, %edx
	leaq	-160(%rbp), %rsi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	%r12, %rdi
	movq	%rbx, %r8
	movl	%r13d, %edx
	movq	-152(%rbp), %rcx
	leaq	.LC27(%rip), %r9
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L354
	call	_ZdaPv@PLT
.L354:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L358
	call	_ZdaPv@PLT
.L358:
	xorl	%r12d, %r12d
.L359:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L384
	addq	$152, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L352:
	.cfi_restore_state
	movl	%ecx, -168(%rbp)
	movq	-1(%rdi), %rax
	testb	$2, 13(%rax)
	je	.L355
	movq	%r9, -176(%rbp)
	movq	%rsi, %r15
	call	_ZN2v88internal20WasmExternalFunction22IsWasmExternalFunctionENS0_6ObjectE@PLT
	movq	-176(%rbp), %r9
	movl	-168(%rbp), %r10d
	testb	%al, %al
	jne	.L385
.L357:
	movq	24(%rbx), %rdx
	movslq	%r10d, %rax
	movq	16(%rbp), %rdi
	movl	%r10d, -176(%rbp)
	salq	$5, %rax
	movq	%r9, -184(%rbp)
	addq	136(%rdx), %rax
	leaq	8(%rbx), %rdx
	movq	(%rax), %r8
	movq	%r8, %rsi
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal8compiler21ResolveWasmImportCallENS0_6HandleINS0_10JSReceiverEEEPNS0_9SignatureINS0_4wasm9ValueTypeEEERKNS6_12WasmFeaturesE@PLT
	movq	-168(%rbp), %r8
	movl	-176(%rbp), %r10d
	cmpb	$2, %al
	movl	%eax, %esi
	je	.L360
	cmpb	$3, %al
	je	.L361
	testb	%al, %al
	movq	-184(%rbp), %r9
	je	.L386
	movq	(%r15), %rax
	movq	%rdx, -176(%rbp)
	movq	%r8, %rdx
	movl	%r10d, -168(%rbp)
	movq	135(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	528(%rax), %rdi
	call	_ZNK2v88internal4wasm22WasmImportWrapperCache3GetENS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEE@PLT
	movl	-168(%rbp), %r10d
	movq	%r15, -128(%rbp)
	movq	-176(%rbp), %r11
	movl	%r10d, -120(%rbp)
	cmpl	$2, 60(%rax)
	je	.L387
	movq	(%rax), %rdx
	movq	(%r15), %rsi
	leaq	-128(%rbp), %rdi
	call	_ZN2v88internal21ImportedFunctionEntry13SetWasmToWasmENS0_18WasmInstanceObjectEm@PLT
.L369:
	movl	$1, %r12d
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L385:
	movq	16(%rbp), %rcx
	movq	(%rbx), %rdi
	movl	%r10d, %edx
	movq	%r15, %rsi
	movq	%r9, -176(%rbp)
	movl	%r10d, -168(%rbp)
	call	_ZN2v88internal18WasmInstanceObject23SetWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_20WasmExternalFunctionEEE@PLT
	movq	-176(%rbp), %r9
	movl	-168(%rbp), %r10d
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L387:
	movq	(%rbx), %rsi
	leaq	-128(%rbp), %rdi
	movq	%rax, %rcx
	movq	%r11, %rdx
	call	_ZN2v88internal21ImportedFunctionEntry11SetWasmToJsEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKNS0_4wasm8WasmCodeE@PLT
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L360:
	movq	(%r15), %rax
	leaq	-128(%rbp), %r13
	movl	%r10d, -184(%rbp)
	movq	%r13, %rdi
	movq	%r8, -176(%rbp)
	movq	135(%rax), %rax
	movq	%rdx, -168(%rbp)
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %r14
	movq	(%rdx), %rax
	movq	%rax, -128(%rbp)
	call	_ZNK2v88internal16WasmCapiFunction17GetHostCallTargetEv@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal4wasm16WasmCodeRefScopeC1Ev@PLT
	movq	(%rbx), %rax
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	-176(%rbp), %r8
	movq	45752(%rax), %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler26CompileWasmCapiCallWrapperEPNS0_4wasm10WasmEngineEPNS2_12NativeModuleEPNS0_9SignatureINS2_9ValueTypeEEEm@PLT
	movq	%rax, %r12
	movq	(%rbx), %rax
	movl	8(%r12), %esi
	movq	40960(%rax), %rdi
	addq	$8136, %rdi
	call	_ZN2v88internal22StatsCounterThreadSafe9IncrementEi@PLT
	movq	(%rbx), %rax
	movl	24(%r12), %esi
	movq	40960(%rax), %rdi
	addq	$8200, %rdi
	call	_ZN2v88internal22StatsCounterThreadSafe9IncrementEi@PLT
	movq	-168(%rbp), %r11
	movq	(%rbx), %rsi
	movq	%r12, %rcx
	movl	-184(%rbp), %r10d
	leaq	-144(%rbp), %rdi
	movq	%r15, -144(%rbp)
	movl	$1, %r12d
	movq	%r11, %rdx
	movl	%r10d, -136(%rbp)
	call	_ZN2v88internal21ImportedFunctionEntry11SetWasmToJsEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKNS0_4wasm8WasmCodeE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L361:
	movq	(%rbx), %r13
	movq	(%rdx), %rax
	leaq	-128(%rbp), %r12
	movl	%r10d, -176(%rbp)
	movq	%r12, %rdi
	movq	%rdx, -168(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal20WasmExportedFunction8instanceEv@PLT
	movq	41112(%r13), %rdi
	movq	-168(%rbp), %r11
	movl	-176(%rbp), %r10d
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L365
	movq	%r11, -176(%rbp)
	movl	%r10d, -168(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-168(%rbp), %r10d
	movq	-176(%rbp), %r11
	movq	%rax, %rbx
.L366:
	movq	(%r11), %rax
	movq	%r12, %rdi
	movl	%r10d, -168(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal20WasmExportedFunction17GetWasmCallTargetEv@PLT
	movl	-168(%rbp), %r10d
	movq	%r15, -128(%rbp)
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$1, %r12d
	movl	%r10d, -120(%rbp)
	movq	(%rbx), %rsi
	call	_ZN2v88internal21ImportedFunctionEntry13SetWasmToWasmENS0_18WasmInstanceObjectEm@PLT
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L386:
	movq	(%r9), %rax
	leaq	-128(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	leaq	-144(%rbp), %rsi
	movl	$1, %edx
	movq	32(%rbx), %r15
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	(%r14), %rax
	movq	-128(%rbp), %rbx
	xorl	%r8d, %r8d
	leaq	-152(%rbp), %rdi
	movl	$1, %ecx
	movl	$1, %edx
	leaq	-160(%rbp), %rsi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	%r15, %rdi
	movq	%rbx, %r8
	movl	%r13d, %edx
	movq	-152(%rbp), %rcx
	leaq	.LC28(%rip), %r9
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L363
	call	_ZdaPv@PLT
.L363:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L359
	call	_ZdaPv@PLT
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L365:
	movq	41088(%r13), %rbx
	cmpq	41096(%r13), %rbx
	je	.L388
.L367:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rbx)
	jmp	.L366
.L388:
	movq	%r13, %rdi
	movq	%r11, -184(%rbp)
	movl	%r10d, -176(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %r11
	movl	-176(%rbp), %r10d
	movq	-168(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L367
.L384:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18602:
	.size	_ZN2v88internal4wasm15InstanceBuilder23ProcessImportedFunctionENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE, .-_ZN2v88internal4wasm15InstanceBuilder23ProcessImportedFunctionENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder39InitializeImportedIndirectFunctionTableENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_15WasmTableObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC29:
	.string	"table import %d[%d] is not a wasm function"
	.section	.text._ZN2v88internal4wasm15InstanceBuilder39InitializeImportedIndirectFunctionTableENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_15WasmTableObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15InstanceBuilder39InitializeImportedIndirectFunctionTableENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_15WasmTableObjectEEE
	.type	_ZN2v88internal4wasm15InstanceBuilder39InitializeImportedIndirectFunctionTableENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_15WasmTableObjectEEE, @function
_ZN2v88internal4wasm15InstanceBuilder39InitializeImportedIndirectFunctionTableENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_15WasmTableObjectEEE:
.LFB18607:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$168, %rsp
	movq	%rsi, -152(%rbp)
	movl	%edx, %esi
	movl	%edx, -144(%rbp)
	movl	%ecx, -188(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r8), %rax
	movq	23(%rax), %rax
	movslq	11(%rax), %r13
	movl	%r13d, %edx
	movl	%r13d, -140(%rbp)
	call	_ZN2v88internal18WasmInstanceObject42EnsureIndirectFunctionTableWithMinimumSizeENS0_6HandleIS1_EEij@PLT
	testq	%r13, %r13
	jle	.L403
	movl	-144(%rbp), %eax
	leaq	-96(%rbp), %r15
	xorl	%r13d, %r13d
	leaq	-101(%rbp), %r14
	leal	16(,%rax,8), %eax
	cltq
	movq	%rax, -184(%rbp)
	leaq	-102(%rbp), %rax
	movq	%rax, -120(%rbp)
	leaq	-88(%rbp), %rax
	movq	%rax, -136(%rbp)
	leaq	-100(%rbp), %rax
	movq	%rax, -128(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	%r15, %rax
	movq	%r14, %r15
	movl	%r13d, %r14d
	movq	%rax, %r13
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L394:
	movl	-144(%rbp), %edx
	movq	-152(%rbp), %rsi
	movl	%r14d, %ecx
	movq	(%rbx), %rdi
	call	_ZN2v88internal18WasmInstanceObject29ImportWasmJSFunctionIntoTableEPNS0_7IsolateENS0_6HandleIS1_EEiiNS4_INS0_14WasmJSFunctionEEE@PLT
.L402:
	addl	$1, %r14d
	cmpl	-140(%rbp), %r14d
	je	.L403
.L404:
	pushq	-136(%rbp)
	movq	-120(%rbp), %rcx
	movq	%r15, %r8
	movl	%r14d, %edx
	movq	(%rbx), %rdi
	pushq	-128(%rbp)
	movq	%r13, %r9
	movq	%r12, %rsi
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	call	_ZN2v88internal15WasmTableObject21GetFunctionTableEntryEPNS0_7IsolateENS0_6HandleIS1_EEiPbS6_PNS0_11MaybeHandleINS0_18WasmInstanceObjectEEEPiPNS7_INS0_14WasmJSFunctionEEE@PLT
	movzbl	-102(%rbp), %r8d
	popq	%rdx
	popq	%rcx
	testb	%r8b, %r8b
	je	.L408
	cmpb	$0, -101(%rbp)
	jne	.L402
	movq	-88(%rbp), %r8
	testq	%r8, %r8
	jne	.L394
	movq	-96(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L409
	movq	(%rdx), %rax
	movl	-100(%rbp), %ecx
	movq	135(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rsi
	movslq	%ecx, %rax
	salq	$5, %rax
	addq	136(%rsi), %rax
	movq	(%rax), %r8
	movl	-144(%rbp), %eax
	testl	%eax, %eax
	je	.L410
	movq	-152(%rbp), %rax
	movq	$0, -80(%rbp)
	movq	-184(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, %rcx
	movq	207(%rax), %rax
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	movq	-1(%rdi,%rax), %rsi
	movq	3520(%rcx), %rdi
	subq	$37592, %rcx
	testq	%rdi, %rdi
	je	.L399
	movq	%rdx, -168(%rbp)
	movq	%r8, -160(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-160(%rbp), %r8
	movq	-168(%rbp), %rdx
.L400:
	movq	%rax, -72(%rbp)
	movl	-100(%rbp), %ecx
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L410:
	movq	-152(%rbp), %rax
	movq	$0, -72(%rbp)
	movq	%rax, -80(%rbp)
.L398:
	movq	24(%rbx), %rax
	movq	%r8, %rsi
	movl	%ecx, -160(%rbp)
	movq	%rdx, -168(%rbp)
	leaq	328(%rax), %rdi
	movl	%r14d, -64(%rbp)
	addl	$1, %r14d
	call	_ZNK2v88internal4wasm12SignatureMap4FindERKNS0_9SignatureINS1_9ValueTypeEEE@PLT
	movl	-160(%rbp), %ecx
	movq	-168(%rbp), %rdx
	movq	-176(%rbp), %rdi
	movl	%eax, %esi
	call	_ZN2v88internal26IndirectFunctionTableEntry3SetEiNS0_6HandleINS0_18WasmInstanceObjectEEEi@PLT
	cmpl	-140(%rbp), %r14d
	jne	.L404
	.p2align 4,,10
	.p2align 3
.L403:
	movl	$1, %r8d
.L389:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L411
	leaq	-40(%rbp), %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L408:
	.cfi_restore_state
	movq	32(%rbx), %rdi
	movl	-188(%rbp), %edx
	movl	%r14d, %ecx
	xorl	%eax, %eax
	leaq	.LC29(%rip), %rsi
	movb	%r8b, -120(%rbp)
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	movzbl	-120(%rbp), %r8d
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L399:
	movq	41088(%rcx), %rax
	cmpq	41096(%rcx), %rax
	je	.L412
.L401:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rcx)
	movq	%rsi, (%rax)
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L409:
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L412:
	movq	%rcx, %rdi
	movq	%rsi, -208(%rbp)
	movq	%rdx, -200(%rbp)
	movq	%r8, -168(%rbp)
	movq	%rcx, -160(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-208(%rbp), %rsi
	movq	-200(%rbp), %rdx
	movq	-168(%rbp), %r8
	movq	-160(%rbp), %rcx
	jmp	.L401
.L411:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18607:
	.size	_ZN2v88internal4wasm15InstanceBuilder39InitializeImportedIndirectFunctionTableENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_15WasmTableObjectEEE, .-_ZN2v88internal4wasm15InstanceBuilder39InitializeImportedIndirectFunctionTableENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_15WasmTableObjectEEE
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder20ProcessImportedTableENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC30:
	.string	"table import requires a WebAssembly.Table"
	.align 8
.LC31:
	.string	"table import %d is smaller than initial %d, got %u"
	.align 8
.LC32:
	.string	"table import %d has no maximum length, expected %d"
	.align 8
.LC33:
	.string	"table import %d has a larger maximum size %lx than the module's declared maximum %u"
	.align 8
.LC34:
	.string	"imported table does not match the expected type"
	.section	.text._ZN2v88internal4wasm15InstanceBuilder20ProcessImportedTableENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15InstanceBuilder20ProcessImportedTableENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE
	.type	_ZN2v88internal4wasm15InstanceBuilder20ProcessImportedTableENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE, @function
_ZN2v88internal4wasm15InstanceBuilder20ProcessImportedTableENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE:
.LFB18611:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r15), %rdx
	movq	%rdx, %rax
	notq	%rax
	movl	%eax, %r12d
	andl	$1, %r12d
	je	.L414
.L417:
	movq	(%r9), %rax
	movq	32(%rdi), %r12
	leaq	-72(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	(%rbx), %rax
	movq	-64(%rbp), %r14
	leaq	-80(%rbp), %rdi
	leaq	-88(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-80(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r14, %r8
	leaq	.LC30(%rip), %r9
	movl	%r13d, %edx
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L416
	call	_ZdaPv@PLT
.L416:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L420
	call	_ZdaPv@PLT
.L420:
	xorl	%r12d, %r12d
.L413:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L457
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L414:
	.cfi_restore_state
	movq	-1(%rdx), %rax
	cmpw	$1103, 11(%rax)
	jne	.L417
	movl	%ecx, %r14d
	movq	24(%rdi), %rcx
	movq	%rdx, %r10
	movslq	%r14d, %rax
	salq	$4, %rax
	addq	184(%rcx), %rax
	movq	23(%rdx), %rcx
	movl	4(%rax), %r11d
	movslq	11(%rcx), %r8
	cmpl	%r11d, 11(%rcx)
	jl	.L458
	cmpb	$0, 12(%rax)
	je	.L422
	movq	(%rdi), %rcx
	movq	31(%rdx), %r11
	movl	8(%rax), %r8d
	cmpq	%r11, 88(%rcx)
	je	.L456
	testb	$1, %r11b
	jne	.L424
	sarq	$32, %r11
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r11d, %xmm0
.L425:
	cvttsd2siq	%xmm0, %rcx
	testq	%rcx, %rcx
	js	.L456
	movl	%r8d, %r11d
	cmpq	%rcx, %r11
	jge	.L422
	movq	32(%rdi), %rdi
	movl	%r13d, %edx
	leaq	.LC33(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L422:
	movzbl	(%rax), %eax
	cmpb	%al, 51(%rdx)
	jne	.L459
	cmpb	$7, %al
	je	.L460
.L430:
	movq	(%rsi), %rax
	movq	199(%rax), %r12
	leal	16(,%r14,8), %eax
	cltq
	leaq	-1(%r12,%rax), %r13
	movq	%r10, 0(%r13)
	testb	$1, %r10b
	je	.L434
	movq	%r10, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L432
	movq	%r10, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r10, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-104(%rbp), %r10
.L432:
	testb	$24, %al
	je	.L434
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L434
	movq	%r10, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L434:
	movl	$1, %r12d
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L459:
	movq	(%r9), %rax
	movq	32(%rdi), %r14
	leaq	-72(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	(%rbx), %rax
	movq	-64(%rbp), %r15
	leaq	-80(%rbp), %rdi
	leaq	-88(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-80(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r15, %r8
	leaq	.LC34(%rip), %r9
	movl	%r13d, %edx
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L428
	call	_ZdaPv@PLT
.L428:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L413
	call	_ZdaPv@PLT
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L458:
	movq	32(%rdi), %rdi
	movl	%r11d, %ecx
	movl	%r13d, %edx
	xorl	%eax, %eax
	leaq	.LC31(%rip), %rsi
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L424:
	movsd	7(%r11), %xmm0
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L460:
	movq	%r15, %r8
	movl	%r13d, %ecx
	movl	%r14d, %edx
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal4wasm15InstanceBuilder39InitializeImportedIndirectFunctionTableENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_15WasmTableObjectEEE
	movl	%eax, %r12d
	testb	%al, %al
	je	.L413
	movq	(%r15), %r10
	movq	-104(%rbp), %rsi
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L456:
	movq	32(%rdi), %rdi
	movl	%r8d, %ecx
	movl	%r13d, %edx
	xorl	%eax, %eax
	leaq	.LC32(%rip), %rsi
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	jmp	.L413
.L457:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18611:
	.size	_ZN2v88internal4wasm15InstanceBuilder20ProcessImportedTableENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE, .-_ZN2v88internal4wasm15InstanceBuilder20ProcessImportedTableENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder21ProcessImportedMemoryENS0_6HandleINS0_18WasmInstanceObjectEEEiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC35:
	.string	"memory import must be a WebAssembly.Memory object"
	.align 8
.LC36:
	.string	"memory import %d is smaller than initial %u, got %u"
	.align 8
.LC37:
	.string	"memory import %d has no maximum limit, expected at most %u"
	.align 8
.LC38:
	.string	"memory import %d has a larger maximum size %u than the module's declared maximum %u"
	.align 8
.LC39:
	.string	"mismatch in shared state of memory, declared = %d, imported = %d"
	.section	.text._ZN2v88internal4wasm15InstanceBuilder21ProcessImportedMemoryENS0_6HandleINS0_18WasmInstanceObjectEEEiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15InstanceBuilder21ProcessImportedMemoryENS0_6HandleINS0_18WasmInstanceObjectEEEiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE
	.type	_ZN2v88internal4wasm15InstanceBuilder21ProcessImportedMemoryENS0_6HandleINS0_18WasmInstanceObjectEEEiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE, @function
_ZN2v88internal4wasm15InstanceBuilder21ProcessImportedMemoryENS0_6HandleINS0_18WasmInstanceObjectEEEiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE:
.LFB18612:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%r9), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rax
	notq	%rax
	movl	%eax, %r12d
	andl	$1, %r12d
	je	.L462
.L465:
	movq	(%r8), %rax
	leaq	-64(%rbp), %rdi
	leaq	-72(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	32(%r13), %r12
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	(%r15), %rax
	movq	-64(%rbp), %rbx
	leaq	-80(%rbp), %rdi
	leaq	-88(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-80(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rbx, %r8
	leaq	.LC35(%rip), %r9
	movl	%r14d, %edx
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L464
	call	_ZdaPv@PLT
.L464:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L471
	call	_ZdaPv@PLT
.L471:
	xorl	%r12d, %r12d
.L461:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L492
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L462:
	.cfi_restore_state
	movq	-1(%rdx), %rax
	cmpw	$1101, 11(%rax)
	jne	.L465
	movq	(%rsi), %r15
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	%rdx, 159(%r15)
	leaq	159(%r15), %rbx
	movq	%rcx, -104(%rbp)
	movq	8(%rcx), %rax
	testl	$262144, %eax
	je	.L467
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%r9, -120(%rbp)
	movq	%rdx, -112(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %rcx
	movq	-120(%rbp), %r9
	movq	-112(%rbp), %rdx
	movq	8(%rcx), %rax
.L467:
	testb	$24, %al
	je	.L478
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L478
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %r9
.L478:
	movq	0(%r13), %r15
	movq	(%r9), %rax
	movq	41112(%r15), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L493
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-104(%rbp), %r9
	movq	(%rax), %rsi
.L473:
	movq	24(%r13), %rdx
	movq	23(%rsi), %r8
	movl	8(%rdx), %ecx
	shrq	$16, %r8
	cmpl	%r8d, %ecx
	ja	.L494
	cmpb	$0, 17(%rdx)
	je	.L476
	movq	(%r9), %rcx
	movslq	35(%rcx), %rcx
	testq	%rcx, %rcx
	js	.L495
	movl	12(%rdx), %r8d
	cmpl	%ecx, %r8d
	jb	.L496
.L476:
	movl	39(%rsi), %ecx
	shrl	$3, %ecx
	andl	$1, %ecx
	cmpb	%cl, 16(%rdx)
	jne	.L497
	movl	$1, %r12d
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L493:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L498
.L474:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L497:
	movq	(%rax), %rax
	movq	32(%r13), %rdi
	leaq	.LC39(%rip), %rsi
	movl	39(%rax), %ecx
	movzbl	16(%rdx), %edx
	xorl	%eax, %eax
	shrl	$3, %ecx
	andl	$1, %ecx
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L496:
	movq	32(%r13), %rdi
	movl	%r14d, %edx
	leaq	.LC38(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	jmp	.L461
.L498:
	movq	%r15, %rdi
	movq	%r9, -112(%rbp)
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %r9
	movq	-104(%rbp), %rsi
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L494:
	movq	32(%r13), %rdi
	movl	%r14d, %edx
	leaq	.LC36(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	jmp	.L461
.L495:
	movq	32(%r13), %rdi
	movl	%r14d, %edx
	leaq	.LC37(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	jmp	.L461
.L492:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18612:
	.size	_ZN2v88internal4wasm15InstanceBuilder21ProcessImportedMemoryENS0_6HandleINS0_18WasmInstanceObjectEEEiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE, .-_ZN2v88internal4wasm15InstanceBuilder21ProcessImportedMemoryENS0_6HandleINS0_18WasmInstanceObjectEEEiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder31ProcessImportedWasmGlobalObjectENS0_6HandleINS0_18WasmInstanceObjectEEEiNS3_INS0_6StringEEES7_RKNS1_10WasmGlobalENS3_INS0_16WasmGlobalObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC40:
	.string	"imported global does not match the expected mutability"
	.align 8
.LC41:
	.string	"imported global does not match the expected type"
	.section	.text._ZN2v88internal4wasm15InstanceBuilder31ProcessImportedWasmGlobalObjectENS0_6HandleINS0_18WasmInstanceObjectEEEiNS3_INS0_6StringEEES7_RKNS1_10WasmGlobalENS3_INS0_16WasmGlobalObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15InstanceBuilder31ProcessImportedWasmGlobalObjectENS0_6HandleINS0_18WasmInstanceObjectEEEiNS3_INS0_6StringEEES7_RKNS1_10WasmGlobalENS3_INS0_16WasmGlobalObjectEEE
	.type	_ZN2v88internal4wasm15InstanceBuilder31ProcessImportedWasmGlobalObjectENS0_6HandleINS0_18WasmInstanceObjectEEEiNS3_INS0_6StringEEES7_RKNS1_10WasmGlobalENS3_INS0_16WasmGlobalObjectEEE, @function
_ZN2v88internal4wasm15InstanceBuilder31ProcessImportedWasmGlobalObjectENS0_6HandleINS0_18WasmInstanceObjectEEEiNS3_INS0_6StringEEES7_RKNS1_10WasmGlobalENS3_INS0_16WasmGlobalObjectEEE:
.LFB18613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %r9
	movzbl	1(%r12), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r9), %rcx
	movslq	51(%rcx), %rdx
	movl	%edx, %eax
	shrl	$8, %eax
	andl	$1, %eax
	cmpb	%al, %r13b
	jne	.L583
	movzbl	(%r12), %eax
	cmpb	%dl, %al
	je	.L504
	cmpb	$6, %al
	sete	%cl
	cmpb	$8, %dl
	sete	%sil
	testb	%cl, %cl
	je	.L531
	testb	%sil, %sil
	je	.L531
.L507:
	testb	%r13b, %r13b
	je	.L511
.L510:
	movq	(%r8), %rax
	movq	32(%rdi), %r12
	leaq	-72(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	(%r15), %rax
	leaq	-80(%rbp), %rdi
	leaq	-88(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	-64(%rbp), %rbx
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	leaq	.LC41(%rip), %r9
.L582:
	movq	-80(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rbx, %r8
	movl	%r14d, %edx
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L513
	call	_ZdaPv@PLT
.L513:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L514
	call	_ZdaPv@PLT
.L514:
	xorl	%r13d, %r13d
.L499:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L584
	addq	$88, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L504:
	.cfi_restore_state
	testb	%r13b, %r13b
	je	.L511
	movq	(%rdi), %r15
	leal	-6(%rax), %edx
	movq	%rsi, %rbx
	movq	41112(%r15), %rdi
	cmpb	$1, %dl
	jbe	.L533
	cmpb	$9, %al
	je	.L533
	movq	23(%rcx), %r14
	testq	%rdi, %rdi
	je	.L521
	movq	%r14, %rsi
	movq	%r9, 16(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	16(%rbp), %r9
	movq	(%r9), %rdx
	movslq	43(%rdx), %r15
	testq	%rax, %rax
	je	.L585
	movq	(%rax), %r14
.L525:
	addq	31(%r14), %r15
.L520:
	movq	(%rbx), %rax
	movq	183(%rax), %rdi
	movl	24(%r12), %eax
	leal	16(,%rax,8), %eax
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%r14, (%rsi)
	testb	$1, %r14b
	je	.L529
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -104(%rbp)
	testl	$262144, %eax
	je	.L527
	movq	%r14, %rdx
	movq	%rsi, -120(%rbp)
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %rcx
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	8(%rcx), %rax
.L527:
	testb	$24, %al
	je	.L529
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L586
	.p2align 4,,10
	.p2align 3
.L529:
	movq	(%rbx), %rax
	movl	24(%r12), %edx
	movq	111(%rax), %rax
	movq	%r15, (%rax,%rdx,8)
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L583:
	movq	(%r8), %rax
	movq	32(%rdi), %r12
	leaq	-72(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	(%r15), %rax
	leaq	-80(%rbp), %rdi
	leaq	-88(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	-64(%rbp), %rbx
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	leaq	.LC40(%rip), %r9
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L511:
	movq	%r9, %rdx
	movq	%r12, %rsi
	movl	$1, %r13d
	call	_ZN2v88internal4wasm15InstanceBuilder16WriteGlobalValueERKNS1_10WasmGlobalENS0_6HandleINS0_16WasmGlobalObjectEEE
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L531:
	cmpb	$7, %dl
	jne	.L532
	testb	%cl, %cl
	jne	.L507
.L532:
	cmpb	$9, %dl
	jne	.L509
	testb	%cl, %cl
	jne	.L507
.L509:
	cmpb	$7, %al
	jne	.L512
	testb	%sil, %sil
	jne	.L507
.L512:
	cmpb	$9, %al
	sete	%al
	testb	%sil, %al
	jne	.L507
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L533:
	movq	31(%rcx), %r14
	testq	%rdi, %rdi
	je	.L517
	movq	%r14, %rsi
	movq	%r9, 16(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	16(%rbp), %r9
	movq	(%rax), %r14
.L518:
	movq	(%r9), %rax
	movslq	43(%rax), %r15
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L517:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L587
.L519:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%r14, (%rax)
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L521:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L588
.L524:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%r14, (%rax)
	movq	(%r9), %rax
	movslq	43(%rax), %r15
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L586:
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L585:
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L588:
	movq	%r15, %rdi
	movq	%r9, 16(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	16(%rbp), %r9
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L587:
	movq	%r15, %rdi
	movq	%r9, 16(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	16(%rbp), %r9
	jmp	.L519
.L584:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18613:
	.size	_ZN2v88internal4wasm15InstanceBuilder31ProcessImportedWasmGlobalObjectENS0_6HandleINS0_18WasmInstanceObjectEEEiNS3_INS0_6StringEEES7_RKNS1_10WasmGlobalENS3_INS0_16WasmGlobalObjectEEE, .-_ZN2v88internal4wasm15InstanceBuilder31ProcessImportedWasmGlobalObjectENS0_6HandleINS0_18WasmInstanceObjectEEEiNS3_INS0_6StringEEES7_RKNS1_10WasmGlobalENS3_INS0_16WasmGlobalObjectEEE
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder21ProcessImportedGlobalENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC42:
	.string	"global import cannot have type i64"
	.align 8
.LC43:
	.string	"imported mutable global must be a WebAssembly.Global object"
	.align 8
.LC44:
	.string	"imported funcref global must be null or an exported function"
	.align 8
.LC45:
	.string	"global import must be a number or WebAssembly.Global object"
	.section	.text._ZN2v88internal4wasm15InstanceBuilder21ProcessImportedGlobalENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15InstanceBuilder21ProcessImportedGlobalENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE
	.type	_ZN2v88internal4wasm15InstanceBuilder21ProcessImportedGlobalENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE, @function
_ZN2v88internal4wasm15InstanceBuilder21ProcessImportedGlobalENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE:
.LFB18614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%ecx, %rcx
	salq	$5, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	movq	%r9, %r8
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	24(%rdi), %rdx
	movq	16(%rbp), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addq	24(%rdx), %rcx
	movq	%rcx, %r12
	movq	(%rsi), %rcx
	movq	%rcx, %rax
	notq	%rax
	andl	$1, %eax
	cmpb	$2, (%r12)
	je	.L689
.L590:
	cmpb	$0, 392(%rdx)
	je	.L596
	testb	%al, %al
	je	.L690
.L608:
	cmpb	$0, 1(%r12)
	jne	.L691
	movzbl	(%r12), %edx
	movq	(%rsi), %rbx
	leal	-6(%rdx), %eax
	cmpb	$1, %al
	setbe	%r10b
	cmpb	$9, %dl
	sete	%al
	orb	%al, %r10b
	je	.L617
	movq	%r8, -104(%rbp)
	movq	%rsi, -112(%rbp)
	cmpb	$7, %dl
	jne	.L618
	movq	0(%r13), %rax
	cmpq	%rbx, 104(%rax)
	je	.L618
	movq	%rbx, %rdi
	movb	%r10b, -113(%rbp)
	call	_ZN2v88internal20WasmExportedFunction22IsWasmExportedFunctionENS0_6ObjectE@PLT
	movq	-104(%rbp), %r8
	testb	%al, %al
	je	.L619
	movq	-112(%rbp), %rsi
	movzbl	-113(%rbp), %r10d
	movq	(%rsi), %rbx
.L618:
	movq	72(%r13), %rax
	movq	(%rax), %r13
	movl	24(%r12), %eax
	leal	16(,%rax,8), %eax
	cltq
	leaq	-1(%r13,%rax), %r12
	movq	%rbx, (%r12)
	testb	$1, %bl
	je	.L589
	movq	%rbx, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	je	.L623
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movb	%r10b, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	movzbl	-104(%rbp), %r10d
.L623:
	testb	$24, %al
	je	.L589
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L692
	.p2align 4,,10
	.p2align 3
.L589:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L693
	leaq	-40(%rbp), %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L690:
	.cfi_restore_state
	movq	-1(%rcx), %rax
	cmpw	$1105, 11(%rax)
	jne	.L601
	movq	0(%r13), %rax
	leaq	1088(%rax), %rsi
.L601:
	movq	(%rsi), %rcx
	testb	$1, %cl
	je	.L608
	movq	-1(%rcx), %rax
	cmpw	$67, 11(%rax)
	ja	.L681
	movq	-1(%rcx), %rax
	cmpw	$64, 11(%rax)
	je	.L681
	cmpb	$1, (%r12)
	movq	0(%r13), %rdi
	jne	.L678
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal6Object14ConvertToInt32EPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L609
	movq	(%rax), %rcx
	movq	-104(%rbp), %r8
	movq	%rcx, %rax
	notq	%rax
	andl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L596:
	testb	%al, %al
	jne	.L608
.L696:
	movq	-1(%rcx), %rax
	cmpw	$1099, 11(%rax)
	jne	.L608
	subq	$8, %rsp
	movl	%r14d, %edx
	movq	%r12, %r9
	movq	%r15, %rcx
	pushq	%rsi
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal4wasm15InstanceBuilder31ProcessImportedWasmGlobalObjectENS0_6HandleINS0_18WasmInstanceObjectEEEiNS3_INS0_6StringEEES7_RKNS1_10WasmGlobalENS3_INS0_16WasmGlobalObjectEEE
	movl	%eax, %r10d
	popq	%rax
	popq	%rdx
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L617:
	testb	$1, %bl
	jne	.L694
	cmpb	$2, %dl
	jne	.L695
.L628:
	cmpb	$0, 12(%r13)
	jne	.L637
.L630:
	movq	(%r8), %rax
	leaq	-64(%rbp), %rdi
	xorl	%r8d, %r8d
	leaq	-72(%rbp), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movb	%r10b, -104(%rbp)
	movq	32(%r13), %r12
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	(%r15), %rax
	xorl	%r8d, %r8d
	movq	-64(%rbp), %rbx
	leaq	-80(%rbp), %rdi
	leaq	-88(%rbp), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-80(%rbp), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC45(%rip), %r9
	movq	%rbx, %r8
	movl	%r14d, %edx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	movq	-80(%rbp), %rdi
	movzbl	-104(%rbp), %r10d
	testq	%rdi, %rdi
	je	.L634
	call	_ZdaPv@PLT
	movzbl	-104(%rbp), %r10d
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L689:
	movzbl	12(%rdi), %r10d
	testb	%r10b, %r10b
	jne	.L590
	testb	%al, %al
	je	.L591
.L594:
	movq	(%r8), %rax
	leaq	-64(%rbp), %rdi
	xorl	%r8d, %r8d
	leaq	-72(%rbp), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movb	%r10b, -104(%rbp)
	movq	32(%r13), %r12
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	(%r15), %rax
	xorl	%r8d, %r8d
	movq	-64(%rbp), %rbx
	leaq	-80(%rbp), %rdi
	leaq	-88(%rbp), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-80(%rbp), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC42(%rip), %r9
	movq	%rbx, %r8
	movl	%r14d, %edx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	movq	-80(%rbp), %rdi
	movzbl	-104(%rbp), %r10d
	testq	%rdi, %rdi
	je	.L634
	movb	%r10b, -104(%rbp)
	call	_ZdaPv@PLT
	movzbl	-104(%rbp), %r10d
.L634:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L589
	movb	%r10b, -104(%rbp)
	call	_ZdaPv@PLT
	movzbl	-104(%rbp), %r10d
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L694:
	movq	-1(%rbx), %rax
	cmpw	$65, 11(%rax)
	jne	.L626
	cmpb	$2, (%r12)
	je	.L628
	movsd	7(%rbx), %xmm0
.L632:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm15InstanceBuilder16WriteGlobalValueERKNS1_10WasmGlobalEd
	movl	$1, %r10d
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L678:
	movq	-1(%rcx), %rax
	cmpw	$65, 11(%rax)
	jne	.L611
	xorl	%eax, %eax
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L611:
	xorl	%edx, %edx
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movq	-104(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L609
	movq	(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L681:
	movq	%rcx, %rax
	notq	%rax
	andl	$1, %eax
	testb	%al, %al
	jne	.L608
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L691:
	movq	(%r8), %rax
	leaq	-64(%rbp), %rdi
	leaq	-72(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	32(%r13), %r12
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	(%r15), %rax
	leaq	-80(%rbp), %rdi
	leaq	-88(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	-64(%rbp), %rbx
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	leaq	.LC43(%rip), %r9
.L688:
	movq	-80(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rbx, %r8
	movl	%r14d, %edx
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L620
	call	_ZdaPv@PLT
.L620:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L621
	call	_ZdaPv@PLT
.L621:
	xorl	%r10d, %r10d
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L626:
	cmpb	$0, 12(%r13)
	je	.L630
	cmpb	$2, (%r12)
	jne	.L630
	.p2align 4,,10
	.p2align 3
.L637:
	movq	0(%r13), %rdi
	movb	%r10b, -104(%rbp)
	call	_ZN2v88internal6BigInt10FromObjectEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movzbl	-104(%rbp), %r10d
	testq	%rax, %rax
	je	.L589
	movq	(%rax), %rax
	leaq	-64(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6BigInt7AsInt64EPb@PLT
	movq	64(%r13), %rcx
	movslq	24(%r12), %rdx
	testq	%rcx, %rcx
	je	.L609
	movq	(%rcx), %rcx
	movl	$1, %r10d
	movq	31(%rcx), %rcx
	movq	%rax, (%rcx,%rdx)
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L591:
	movq	-1(%rcx), %rdi
	cmpw	$1099, 11(%rdi)
	jne	.L594
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L695:
	sarq	$32, %rbx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L692:
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movb	%r10b, -104(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movzbl	-104(%rbp), %r10d
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L619:
	movq	(%r8), %rax
	leaq	-64(%rbp), %rdi
	leaq	-72(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	32(%r13), %r12
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	(%r15), %rax
	leaq	-80(%rbp), %rdi
	leaq	-88(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	-64(%rbp), %rbx
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	leaq	.LC44(%rip), %r9
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L609:
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L693:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18614:
	.size	_ZN2v88internal4wasm15InstanceBuilder21ProcessImportedGlobalENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE, .-_ZN2v88internal4wasm15InstanceBuilder21ProcessImportedGlobalENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE
	.section	.text._ZN2v88internal4wasm15InstanceBuilder11InitGlobalsENS0_6HandleINS0_18WasmInstanceObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15InstanceBuilder11InitGlobalsENS0_6HandleINS0_18WasmInstanceObjectEEE
	.type	_ZN2v88internal4wasm15InstanceBuilder11InitGlobalsENS0_6HandleINS0_18WasmInstanceObjectEEE, @function
_ZN2v88internal4wasm15InstanceBuilder11InitGlobalsENS0_6HandleINS0_18WasmInstanceObjectEEE:
.LFB18664:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %rax
	movq	%rsi, -88(%rbp)
	movq	32(%rax), %r12
	movq	24(%rax), %rbx
	cmpq	%r12, %rbx
	je	.L697
	movq	%rdi, %r15
	leaq	.L703(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L727:
	movdqu	(%rbx), %xmm1
	movdqu	16(%rbx), %xmm2
	cmpb	$0, 1(%rbx)
	movl	8(%rbx), %eax
	movslq	24(%rbx), %r14
	movzbl	28(%rbx), %edx
	movaps	%xmm1, -80(%rbp)
	movaps	%xmm2, -64(%rbp)
	je	.L699
	testb	%dl, %dl
	jne	.L700
.L699:
	cmpl	$7, %eax
	ja	.L701
	movslq	0(%r13,%rax,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder11InitGlobalsENS0_6HandleINS0_18WasmInstanceObjectEEE,"a",@progbits
	.align 4
	.align 4
.L703:
	.long	.L700-.L703
	.long	.L709-.L703
	.long	.L708-.L703
	.long	.L707-.L703
	.long	.L706-.L703
	.long	.L705-.L703
	.long	.L704-.L703
	.long	.L702-.L703
	.section	.text._ZN2v88internal4wasm15InstanceBuilder11InitGlobalsENS0_6HandleINS0_18WasmInstanceObjectEEE
.L707:
	movq	64(%r15), %rax
	movq	-64(%rbp), %rdx
	testq	%rax, %rax
	je	.L711
	movq	(%rax), %rax
	movq	31(%rax), %rax
	movq	%rdx, (%rax,%r14)
.L700:
	addq	$32, %rbx
	cmpq	%rbx, %r12
	jne	.L727
.L697:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L704:
	.cfi_restore_state
	testb	%dl, %dl
	jne	.L700
	movq	72(%r15), %rdx
	movq	(%r15), %rsi
	leal	16(,%r14,8), %eax
	addq	$32, %rbx
	cltq
	movq	(%rdx), %rdx
	movq	104(%rsi), %rsi
	movq	%rsi, -1(%rax,%rdx)
	cmpq	%rbx, %r12
	jne	.L727
	jmp	.L697
.L705:
	movq	64(%r15), %rax
	movsd	-64(%rbp), %xmm0
	testq	%rax, %rax
	je	.L711
	movq	(%rax), %rax
	addq	$32, %rbx
	movq	31(%rax), %rax
	movq	%xmm0, (%rax,%r14)
	cmpq	%rbx, %r12
	jne	.L727
	jmp	.L697
.L706:
	movq	64(%r15), %rax
	movl	-64(%rbp), %edx
	testq	%rax, %rax
	je	.L711
.L767:
	movq	(%rax), %rax
	addq	$32, %rbx
	movq	31(%rax), %rax
	movl	%edx, (%rax,%r14)
	cmpq	%rbx, %r12
	jne	.L727
	jmp	.L697
.L708:
	movq	64(%r15), %rax
	movl	-64(%rbp), %edx
	testq	%rax, %rax
	jne	.L767
.L711:
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L709:
	movq	24(%r15), %rsi
	movl	-64(%rbp), %edx
	movzbl	(%rbx), %eax
	salq	$5, %rdx
	addq	24(%rsi), %rdx
	movl	24(%rdx), %edi
	leal	-6(%rax), %edx
	cmpb	$1, %dl
	jbe	.L732
	cmpb	$9, %al
	je	.L732
	subl	$2, %eax
	andl	$-3, %eax
	cmpb	$1, %al
	movq	64(%r15), %rax
	sbbq	%rdx, %rdx
	andl	$4, %edx
	addq	$4, %rdx
	testq	%rax, %rax
	je	.L711
	movq	(%rax), %rax
	movq	31(%rax), %rsi
	movslq	%edi, %rax
	addq	%rsi, %r14
	addq	%rax, %rsi
	movl	%edx, %eax
	cmpl	$8, %edx
	jb	.L768
	movq	(%rsi), %rax
	leaq	8(%r14), %r8
	andq	$-8, %r8
	movq	%rax, (%r14)
	movl	%edx, %eax
	movq	-8(%rsi,%rax), %rdi
	movq	%rdi, -8(%r14,%rax)
	subq	%r8, %r14
	leal	(%rdx,%r14), %eax
	subq	%r14, %rsi
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L700
	andl	$-8, %eax
	xorl	%edx, %edx
.L725:
	movl	%edx, %edi
	addl	$8, %edx
	movq	(%rsi,%rdi), %r9
	movq	%r9, (%r8,%rdi)
	cmpl	%eax, %edx
	jb	.L725
	addq	$32, %rbx
	cmpq	%rbx, %r12
	jne	.L727
	jmp	.L697
.L702:
	movl	-64(%rbp), %edx
	movq	(%r15), %rdi
	movq	-88(%rbp), %rsi
	call	_ZN2v88internal18WasmInstanceObject31GetOrCreateWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	72(%r15), %rdx
	movq	(%rdx), %rdi
	movq	(%rax), %rdx
	leal	16(,%r14,8), %eax
	cltq
	leaq	-1(%rdi,%rax), %r14
	movq	%rdx, (%r14)
	testb	$1, %dl
	je	.L700
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -96(%rbp)
	testl	$262144, %eax
	jne	.L769
	testb	$24, %al
	je	.L700
.L770:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L700
	movq	%r14, %rsi
	addq	$32, %rbx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	cmpq	%rbx, %r12
	jne	.L727
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L769:
	movq	%r14, %rsi
	movq	%rdx, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %r8
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rdi
	movq	8(%r8), %rax
	testb	$24, %al
	jne	.L770
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L768:
	andl	$4, %edx
	jne	.L771
	testl	%eax, %eax
	je	.L700
	movzbl	(%rsi), %edx
	movb	%dl, (%r14)
	testb	$2, %al
	je	.L700
	movzwl	-2(%rsi,%rax), %edx
	movw	%dx, -2(%r14,%rax)
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L732:
	movq	72(%r15), %rax
	movq	(%rax), %r8
	leal	16(,%rdi,8), %eax
	cltq
	movq	-1(%r8,%rax), %rdx
	leal	16(,%r14,8), %eax
	cltq
	leaq	-1(%r8,%rax), %r14
	movq	%rdx, (%r14)
	testb	$1, %dl
	je	.L700
	movq	%rdx, %r9
	andq	$-262144, %r9
	movq	8(%r9), %rax
	movq	%r9, -96(%rbp)
	testl	$262144, %eax
	je	.L718
	movq	%r8, %rdi
	movq	%r14, %rsi
	movq	%rdx, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %r9
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %r8
	movq	8(%r9), %rax
.L718:
	testb	$24, %al
	je	.L700
	movq	%r8, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L700
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L700
.L771:
	movl	(%rsi), %edx
	movl	%edx, (%r14)
	movl	-4(%rsi,%rax), %edx
	movl	%edx, -4(%r14,%rax)
	jmp	.L700
.L701:
	leaq	.LC13(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18664:
	.size	_ZN2v88internal4wasm15InstanceBuilder11InitGlobalsENS0_6HandleINS0_18WasmInstanceObjectEEE, .-_ZN2v88internal4wasm15InstanceBuilder11InitGlobalsENS0_6HandleINS0_18WasmInstanceObjectEEE
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder14AllocateMemoryEjj.str1.8,"aMS",@progbits,1
	.align 8
.LC46:
	.string	"Out of memory: wasm memory too large"
	.align 8
.LC47:
	.string	"Out of memory: wasm shared memory"
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder14AllocateMemoryEjj.str1.1,"aMS",@progbits,1
.LC48:
	.string	"Out of memory: wasm memory"
	.section	.text._ZN2v88internal4wasm15InstanceBuilder14AllocateMemoryEjj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15InstanceBuilder14AllocateMemoryEjj
	.type	_ZN2v88internal4wasm15InstanceBuilder14AllocateMemoryEjj, @function
_ZN2v88internal4wasm15InstanceBuilder14AllocateMemoryEjj:
.LFB18665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	call	_ZN2v88internal4wasm13max_mem_pagesEv@PLT
	cmpl	%ebx, %eax
	jb	.L782
	movq	24(%r12), %rax
	movl	%ebx, %esi
	movq	(%r12), %rdi
	salq	$16, %rsi
	cmpb	$0, 16(%rax)
	je	.L775
	cmpb	$0, 10(%r12)
	jne	.L783
.L775:
	call	_ZN2v88internal4wasm14NewArrayBufferEPNS0_7IsolateEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L784
.L780:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L782:
	.cfi_restore_state
	movq	32(%r12), %rdi
	xorl	%eax, %eax
	leaq	.LC46(%rip), %rsi
	call	_ZN2v88internal4wasm12ErrorThrower10RangeErrorEPKcz@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L783:
	.cfi_restore_state
	movl	%r13d, %edx
	salq	$16, %rdx
	call	_ZN2v88internal4wasm20NewSharedArrayBufferEPNS0_7IsolateEmm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L780
	movq	32(%r12), %rdi
	leaq	.LC47(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12ErrorThrower10RangeErrorEPKcz@PLT
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L784:
	movq	32(%r12), %rdi
	leaq	.LC48(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12ErrorThrower10RangeErrorEPKcz@PLT
	jmp	.L780
	.cfi_endproc
.LFE18665:
	.size	_ZN2v88internal4wasm15InstanceBuilder14AllocateMemoryEjj, .-_ZN2v88internal4wasm15InstanceBuilder14AllocateMemoryEjj
	.section	.text._ZNK2v88internal4wasm15InstanceBuilder13NeedsWrappersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm15InstanceBuilder13NeedsWrappersEv
	.type	_ZNK2v88internal4wasm15InstanceBuilder13NeedsWrappersEv, @function
_ZNK2v88internal4wasm15InstanceBuilder13NeedsWrappersEv:
.LFB18666:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	$1, %eax
	movl	72(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L785
	movq	184(%rdx), %rax
	movq	192(%rdx), %rdx
	cmpq	%rdx, %rax
	jne	.L787
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L792:
	addq	$16, %rax
	cmpq	%rax, %rdx
	je	.L789
.L787:
	cmpb	$7, (%rax)
	jne	.L792
	movl	$1, %eax
.L785:
	ret
	.p2align 4,,10
	.p2align 3
.L789:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE18666:
	.size	_ZNK2v88internal4wasm15InstanceBuilder13NeedsWrappersEv, .-_ZNK2v88internal4wasm15InstanceBuilder13NeedsWrappersEv
	.section	.text._ZN2v88internal4wasm15InstanceBuilder32InitializeIndirectFunctionTablesENS0_6HandleINS0_18WasmInstanceObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15InstanceBuilder32InitializeIndirectFunctionTablesENS0_6HandleINS0_18WasmInstanceObjectEEE
	.type	_ZN2v88internal4wasm15InstanceBuilder32InitializeIndirectFunctionTablesENS0_6HandleINS0_18WasmInstanceObjectEEE, @function
_ZN2v88internal4wasm15InstanceBuilder32InitializeIndirectFunctionTablesENS0_6HandleINS0_18WasmInstanceObjectEEE:
.LFB18671:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	xorl	%ebx, %ebx
	subq	$8, %rsp
	movq	24(%rdi), %rax
	movq	192(%rax), %rcx
	movq	184(%rax), %rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$4, %rax
	testl	%eax, %eax
	jle	.L793
	.p2align 4,,10
	.p2align 3
.L794:
	movq	%rbx, %rax
	salq	$4, %rax
	addq	%rdx, %rax
	cmpb	$7, (%rax)
	je	.L801
	movq	%rcx, %rax
	addq	$1, %rbx
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpl	%ebx, %eax
	jg	.L794
.L793:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L801:
	.cfi_restore_state
	movl	4(%rax), %edx
	movl	%ebx, %esi
	movq	%r13, %rdi
	addq	$1, %rbx
	call	_ZN2v88internal18WasmInstanceObject42EnsureIndirectFunctionTableWithMinimumSizeENS0_6HandleIS1_EEij@PLT
	movq	24(%r12), %rax
	movq	192(%rax), %rcx
	movq	184(%rax), %rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpl	%eax, %ebx
	jl	.L794
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18671:
	.size	_ZN2v88internal4wasm15InstanceBuilder32InitializeIndirectFunctionTablesENS0_6HandleINS0_18WasmInstanceObjectEEE, .-_ZN2v88internal4wasm15InstanceBuilder32InitializeIndirectFunctionTablesENS0_6HandleINS0_18WasmInstanceObjectEEE
	.section	.text._ZN2v88internal4wasm19LoadElemSegmentImplEPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEENS4_INS0_15WasmTableObjectEEEjRKNS1_15WasmElemSegmentEjjm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm19LoadElemSegmentImplEPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEENS4_INS0_15WasmTableObjectEEEjRKNS1_15WasmElemSegmentEjjm
	.type	_ZN2v88internal4wasm19LoadElemSegmentImplEPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEENS4_INS0_15WasmTableObjectEEEjRKNS1_15WasmElemSegmentEjjm, @function
_ZN2v88internal4wasm19LoadElemSegmentImplEPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEENS4_INS0_15WasmTableObjectEEEjRKNS1_15WasmElemSegmentEjjm:
.LFB18672:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, -104(%rbp)
	movq	24(%rbp), %rdx
	movl	%ecx, -132(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$1, -133(%rbp)
	testq	%rdx, %rdx
	je	.L802
	movq	0(%r13), %rax
	movl	%r9d, %ecx
	movq	%rdi, %r14
	movq	%rsi, %r15
	movl	%r9d, %r12d
	movq	23(%rax), %rax
	movslq	11(%rax), %rax
	cmpq	%rcx, %rax
	jb	.L830
	subq	%rcx, %rax
	movq	%rdx, %rcx
	movq	%r8, %rbx
	cmpq	%rdx, %rax
	cmovbe	%rax, %rcx
	setnb	%sil
.L804:
	movq	32(%rbx), %rax
	subq	24(%rbx), %rax
	movl	16(%rbp), %ebx
	sarq	$2, %rax
	movq	(%r15), %rdx
	cmpq	%rax, %rbx
	ja	.L805
	subq	%rbx, %rax
	movq	%rcx, %rdi
	movq	%rdx, -80(%rbp)
	cmpq	%rcx, %rax
	cmovbe	%rax, %rdi
	setnb	%al
	andl	%esi, %eax
	movb	%al, -133(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rdi, -112(%rbp)
	movq	%rax, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal18WasmInstanceObject6moduleEv@PLT
	cmpq	$0, -112(%rbp)
	movq	%rax, -120(%rbp)
	je	.L802
	movl	-132(%rbp), %eax
	movq	$0, -88(%rbp)
	leal	16(,%rax,8), %eax
	cltq
	movq	%rax, -192(%rbp)
	leaq	0(,%rbx,4), %rax
	movq	%rax, -96(%rbp)
	leaq	104(%r14), %rax
	movq	%rax, -144(%rbp)
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L820:
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	%r9d, -128(%rbp)
	call	_ZN2v88internal18WasmInstanceObject23GetWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movl	-128(%rbp), %r9d
	testq	%rax, %rax
	je	.L843
	movq	0(%r13), %rdx
	movq	23(%rdx), %rdi
	movq	(%rax), %rdx
	leal	16(,%r12,8), %eax
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L822
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rcx
	movq	%rax, -128(%rbp)
	testl	$262144, %ecx
	je	.L824
	movl	%r9d, -180(%rbp)
	movq	%rdx, -176(%rbp)
	movq	%rsi, -168(%rbp)
	movq	%rdi, -160(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-128(%rbp), %rax
	movl	-180(%rbp), %r9d
	movq	-176(%rbp), %rdx
	movq	-168(%rbp), %rsi
	movq	8(%rax), %rcx
	movq	-160(%rbp), %rdi
.L824:
	andl	$24, %ecx
	je	.L822
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L822
	movl	%r9d, -128(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movl	-128(%rbp), %r9d
	.p2align 4,,10
	.p2align 3
.L822:
	movq	(%rbx), %rcx
	movq	%r15, %r8
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal15WasmTableObject20UpdateDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEiPNS0_9SignatureINS0_4wasm9ValueTypeEEENS4_INS0_18WasmInstanceObjectEEEi@PLT
.L813:
	addq	$1, -88(%rbp)
	addl	$1, %r12d
	movq	-88(%rbp), %rax
	addq	$4, -96(%rbp)
	cmpq	-112(%rbp), %rax
	je	.L802
.L826:
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	24(%rax), %rax
	movl	(%rax,%rdi), %r9d
	movq	0(%r13), %rax
	movslq	51(%rax), %rax
	movl	%eax, %edx
	cmpl	$-1, %r9d
	je	.L844
	movq	-120(%rbp), %rdi
	movl	%r9d, %ebx
	salq	$5, %rbx
	addq	136(%rdi), %rbx
	cmpb	$7, %al
	je	.L845
.L814:
	cmpb	$6, %dl
	movl	%r9d, %edx
	jne	.L820
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18WasmInstanceObject31GetOrCreateWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal15WasmTableObject3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE@PLT
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L805:
	leaq	-80(%rbp), %rdi
	movq	%rdx, -80(%rbp)
	call	_ZN2v88internal18WasmInstanceObject6moduleEv@PLT
	movb	$0, -133(%rbp)
	.p2align 4,,10
	.p2align 3
.L802:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L846
	movzbl	-133(%rbp), %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L830:
	.cfi_restore_state
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%r8, %rbx
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L844:
	cmpb	$7, %al
	je	.L847
.L807:
	movq	-144(%rbp), %rcx
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal15WasmTableObject3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE@PLT
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L845:
	movq	112(%rdi), %rax
	movl	12(%rbx), %edx
	movl	(%rax,%rdx,4), %r8d
	movl	-132(%rbp), %eax
	testl	%eax, %eax
	je	.L848
	movq	(%r15), %rax
	movq	$0, -80(%rbp)
	movq	-192(%rbp), %rcx
	movq	%rax, %rdx
	movq	207(%rax), %rax
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	movq	-1(%rcx,%rax), %rsi
	movq	3520(%rdx), %rdi
	subq	$37592, %rdx
	testq	%rdi, %rdi
	je	.L817
	movl	%r8d, -160(%rbp)
	movl	%r9d, -128(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-128(%rbp), %r9d
	movl	-160(%rbp), %r8d
.L818:
	movq	%rax, -72(%rbp)
.L816:
	movq	-152(%rbp), %rdi
	movl	%r9d, %ecx
	movq	%r15, %rdx
	movl	%r8d, %esi
	movl	%r9d, -128(%rbp)
	movl	%r12d, -64(%rbp)
	call	_ZN2v88internal26IndirectFunctionTableEntry3SetEiNS0_6HandleINS0_18WasmInstanceObjectEEEi@PLT
	movq	0(%r13), %rax
	movl	-128(%rbp), %r9d
	movzbl	51(%rax), %edx
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L843:
	movl	%r9d, %r8d
	movq	%r15, %rcx
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal15WasmTableObject27SetFunctionTablePlaceholderEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_18WasmInstanceObjectEEEi@PLT
	movl	-128(%rbp), %r9d
	jmp	.L822
	.p2align 4,,10
	.p2align 3
.L848:
	movq	%r15, -80(%rbp)
	movq	$0, -72(%rbp)
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L847:
	movl	-132(%rbp), %edx
	testl	%edx, %edx
	je	.L849
	movq	(%r15), %rax
	movq	$0, -80(%rbp)
	movq	-192(%rbp), %rsi
	movq	%rax, %rdx
	movq	207(%rax), %rax
	andq	$-262144, %rdx
	movq	24(%rdx), %rbx
	movq	-1(%rsi,%rax), %rsi
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L810
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L811:
	movq	%rax, -72(%rbp)
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L849:
	movq	%r15, -80(%rbp)
	movq	$0, -72(%rbp)
.L809:
	movq	-152(%rbp), %rdi
	movl	%r12d, -64(%rbp)
	call	_ZN2v88internal26IndirectFunctionTableEntry5clearEv@PLT
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L817:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L850
.L819:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L818
.L810:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L851
.L812:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L811
.L850:
	movq	%rdx, %rdi
	movq	%rsi, -176(%rbp)
	movl	%r8d, -168(%rbp)
	movl	%r9d, -160(%rbp)
	movq	%rdx, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	movl	-168(%rbp), %r8d
	movl	-160(%rbp), %r9d
	movq	-128(%rbp), %rdx
	jmp	.L819
.L851:
	movq	%rbx, %rdi
	movq	%rsi, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	jmp	.L812
.L846:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18672:
	.size	_ZN2v88internal4wasm19LoadElemSegmentImplEPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEENS4_INS0_15WasmTableObjectEEEjRKNS1_15WasmElemSegmentEjjm, .-_ZN2v88internal4wasm19LoadElemSegmentImplEPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEENS4_INS0_15WasmTableObjectEEEjRKNS1_15WasmElemSegmentEjjm
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder17LoadTableSegmentsENS0_6HandleINS0_18WasmInstanceObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC49:
	.string	"table initializer is out of bounds"
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder17LoadTableSegmentsENS0_6HandleINS0_18WasmInstanceObjectEEE.str1.1,"aMS",@progbits,1
.LC50:
	.string	"success"
	.section	.text._ZN2v88internal4wasm15InstanceBuilder17LoadTableSegmentsENS0_6HandleINS0_18WasmInstanceObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15InstanceBuilder17LoadTableSegmentsENS0_6HandleINS0_18WasmInstanceObjectEEE
	.type	_ZN2v88internal4wasm15InstanceBuilder17LoadTableSegmentsENS0_6HandleINS0_18WasmInstanceObjectEEE, @function
_ZN2v88internal4wasm15InstanceBuilder17LoadTableSegmentsENS0_6HandleINS0_18WasmInstanceObjectEEE:
.LFB18673:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	280(%rdx), %rbx
	movq	288(%rdx), %r14
	cmpq	%r14, %rbx
	je	.L853
	leaq	-64(%rbp), %rax
	movq	%rax, -88(%rbp)
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L855:
	movq	(%r15), %rax
	movq	-88(%rbp), %rdi
	movl	%ecx, -72(%rbp)
	movl	16(%rbx), %r12d
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal18WasmInstanceObject6moduleEv@PLT
	salq	$5, %r12
	movl	-72(%rbp), %ecx
	addq	24(%rax), %r12
	movq	(%r15), %rax
	movq	167(%rax), %rdx
	movl	24(%r12), %eax
	movq	31(%rdx), %rdx
	movl	(%rdx,%rax), %r9d
.L857:
	movq	32(%rbx), %r12
	subq	24(%rbx), %r12
	sarq	$2, %r12
	cmpb	$0, 17(%r13)
	je	.L871
	testq	%r12, %r12
	je	.L854
.L871:
	movq	(%r15), %rdx
	movl	(%rbx), %eax
	movq	0(%r13), %rsi
	movq	199(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %r8
	movq	41112(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L859
	movq	%r8, %rsi
	movl	%r9d, -80(%rbp)
	movl	%ecx, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-72(%rbp), %ecx
	movl	-80(%rbp), %r9d
	movq	%rax, %rdx
.L860:
	pushq	%r12
	movq	0(%r13), %rdi
	movq	%rbx, %r8
	movq	%r15, %rsi
	pushq	$0
	call	_ZN2v88internal4wasm19LoadElemSegmentImplEPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEENS4_INS0_15WasmTableObjectEEEjRKNS1_15WasmElemSegmentEjjm
	cmpb	$0, 17(%r13)
	popq	%rdx
	popq	%rcx
	je	.L862
	testb	%al, %al
	je	.L874
	.p2align 4,,10
	.p2align 3
.L854:
	addq	$56, %rbx
	cmpq	%rbx, %r14
	je	.L875
.L863:
	cmpb	$0, 48(%rbx)
	je	.L854
	movl	8(%rbx), %eax
	movl	(%rbx), %ecx
	cmpl	$1, %eax
	je	.L855
	cmpl	$2, %eax
	jne	.L856
	movl	16(%rbx), %r9d
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L862:
	testb	%al, %al
	jne	.L854
	leaq	.LC50(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L859:
	movq	41088(%rsi), %rdx
	cmpq	%rdx, 41096(%rsi)
	je	.L876
.L861:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rsi)
	movq	%r8, (%rdx)
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L875:
	movq	24(%r13), %rdx
.L853:
	movq	184(%rdx), %rax
	movq	192(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$4, %rdx
	testl	%edx, %edx
	jle	.L852
	leal	-1(%rdx), %r14d
	xorl	%ebx, %ebx
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L865:
	leaq	1(%rbx), %rdx
	cmpq	%r14, %rbx
	je	.L852
.L877:
	movq	24(%r13), %rax
	movq	%rdx, %rbx
	movq	184(%rax), %rax
.L869:
	movq	%rbx, %rdx
	movl	%ebx, %r12d
	salq	$4, %rdx
	cmpb	$7, (%rax,%rdx)
	jne	.L865
	movq	(%r15), %rdx
	movq	0(%r13), %rcx
	leal	16(,%rbx,8), %eax
	cltq
	movq	199(%rdx), %rdx
	movq	-1(%rax,%rdx), %r8
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L866
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L867:
	movq	0(%r13), %rdi
	movq	%r15, %rdx
	movl	%r12d, %ecx
	call	_ZN2v88internal15WasmTableObject16AddDispatchTableEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_18WasmInstanceObjectEEEi@PLT
	leaq	1(%rbx), %rdx
	cmpq	%r14, %rbx
	jne	.L877
	.p2align 4,,10
	.p2align 3
.L852:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L878
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L866:
	.cfi_restore_state
	movq	41088(%rcx), %rsi
	cmpq	41096(%rcx), %rsi
	je	.L879
.L868:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rcx)
	movq	%r8, (%rsi)
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L876:
	movq	%rsi, %rdi
	movl	%r9d, -100(%rbp)
	movq	%r8, -96(%rbp)
	movl	%ecx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-100(%rbp), %r9d
	movq	-96(%rbp), %r8
	movl	-80(%rbp), %ecx
	movq	-72(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L861
.L879:
	movq	%rcx, %rdi
	movq	%r8, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L868
.L874:
	movq	32(%r13), %rdi
	leaq	.LC49(%rip), %rsi
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	movq	24(%r13), %rdx
	jmp	.L853
.L856:
	leaq	.LC13(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L878:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18673:
	.size	_ZN2v88internal4wasm15InstanceBuilder17LoadTableSegmentsENS0_6HandleINS0_18WasmInstanceObjectEEE, .-_ZN2v88internal4wasm15InstanceBuilder17LoadTableSegmentsENS0_6HandleINS0_18WasmInstanceObjectEEE
	.section	.text._ZN2v88internal4wasm15InstanceBuilder20InitializeExceptionsENS0_6HandleINS0_18WasmInstanceObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15InstanceBuilder20InitializeExceptionsENS0_6HandleINS0_18WasmInstanceObjectEEE
	.type	_ZN2v88internal4wasm15InstanceBuilder20InitializeExceptionsENS0_6HandleINS0_18WasmInstanceObjectEEE, @function
_ZN2v88internal4wasm15InstanceBuilder20InitializeExceptionsENS0_6HandleINS0_18WasmInstanceObjectEEE:
.LFB18674:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	(%rsi), %rax
	movq	41112(%rbx), %rdi
	movq	223(%rax), %rsi
	testq	%rdi, %rdi
	je	.L881
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L882:
	movl	11(%rsi), %eax
	movl	$15, %ebx
	xorl	%r12d, %r12d
	testl	%eax, %eax
	jg	.L884
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L886:
	movq	0(%r13), %rsi
	addl	$1, %r12d
	addq	$8, %rbx
	cmpl	%r12d, 11(%rsi)
	jle	.L880
.L884:
	movq	(%rsi,%rbx), %rax
	movq	(%r14), %rdi
	leaq	1(%rbx), %rdx
	cmpq	%rax, 88(%rdi)
	jne	.L886
	movl	%r12d, %esi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal16WasmExceptionTag3NewEPNS0_7IsolateEi@PLT
	movq	0(%r13), %rdi
	movq	-56(%rbp), %rdx
	movq	(%rax), %r15
	leaq	-1(%rdi,%rdx), %rsi
	movq	%r15, (%rsi)
	testb	$1, %r15b
	je	.L886
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L888
	movq	%r15, %rdx
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
.L888:
	testb	$24, %al
	je	.L886
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L886
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L886
	.p2align 4,,10
	.p2align 3
.L880:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L881:
	.cfi_restore_state
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L902
.L883:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	jmp	.L882
.L902:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L883
	.cfi_endproc
.LFE18674:
	.size	_ZN2v88internal4wasm15InstanceBuilder20InitializeExceptionsENS0_6HandleINS0_18WasmInstanceObjectEEE, .-_ZN2v88internal4wasm15InstanceBuilder20InitializeExceptionsENS0_6HandleINS0_18WasmInstanceObjectEEE
	.section	.text._ZN2v88internal4wasm15LoadElemSegmentEPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEjjjjj,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm15LoadElemSegmentEPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEjjjjj
	.type	_ZN2v88internal4wasm15LoadElemSegmentEPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEjjjjj, @function
_ZN2v88internal4wasm15LoadElemSegmentEPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEjjjjj:
.LFB18675:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-64(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$56, %rsp
	movl	%r9d, -68(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal18WasmInstanceObject6moduleEv@PLT
	movl	16(%rbp), %ecx
	movq	%rax, %r8
	leaq	0(,%rbx,8), %rax
	movq	280(%r8), %rdx
	subq	%rbx, %rax
	leaq	(%rdx,%rax,8), %rbx
	movq	0(%r13), %rdx
	leal	16(,%r15,8), %eax
	cltq
	movq	199(%rdx), %rdx
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L904
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-80(%rbp), %rcx
	movq	%rax, %rdx
.L905:
	movl	-68(%rbp), %eax
	pushq	%rcx
	movq	%r13, %rsi
	movl	%r14d, %r9d
	movq	%rbx, %r8
	movl	%r15d, %ecx
	movq	%r12, %rdi
	pushq	%rax
	call	_ZN2v88internal4wasm19LoadElemSegmentImplEPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEENS4_INS0_15WasmTableObjectEEEjRKNS1_15WasmElemSegmentEjjm
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L909
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L904:
	.cfi_restore_state
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L910
.L906:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L910:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L906
.L909:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18675:
	.size	_ZN2v88internal4wasm15LoadElemSegmentEPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEjjjjj, .-_ZN2v88internal4wasm15LoadElemSegmentEPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEjjjjj
	.section	.rodata._ZNSt6vectorIN2v88internal6HandleINS1_19WasmExceptionObjectEEESaIS4_EE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC51:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIN2v88internal6HandleINS1_19WasmExceptionObjectEEESaIS4_EE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIN2v88internal6HandleINS1_19WasmExceptionObjectEEESaIS4_EE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6HandleINS1_19WasmExceptionObjectEEESaIS4_EE17_M_default_appendEm
	.type	_ZNSt6vectorIN2v88internal6HandleINS1_19WasmExceptionObjectEEESaIS4_EE17_M_default_appendEm, @function
_ZNSt6vectorIN2v88internal6HandleINS1_19WasmExceptionObjectEEESaIS4_EE17_M_default_appendEm:
.LFB21915:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L950
	movabsq	$1152921504606846975, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$24, %rsp
	movq	8(%rdi), %rcx
	movq	16(%rdi), %rax
	movq	%rcx, %r13
	subq	(%rdi), %r13
	subq	%rcx, %rax
	movq	%r13, %r15
	sarq	$3, %rax
	sarq	$3, %r15
	subq	%r15, %rsi
	cmpq	%rbx, %rax
	jb	.L913
	cmpq	$1, %rbx
	je	.L932
	leaq	-2(%rbx), %rdx
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	addq	$1, %rdx
	.p2align 4,,10
	.p2align 3
.L915:
	movq	%rax, %rsi
	addq	$1, %rax
	salq	$4, %rsi
	movups	%xmm0, (%rcx,%rsi)
	cmpq	%rax, %rdx
	ja	.L915
	leaq	(%rdx,%rdx), %rax
	salq	$4, %rdx
	addq	%rcx, %rdx
	cmpq	%rax, %rbx
	je	.L916
.L914:
	movq	$0, (%rdx)
.L916:
	leaq	(%rcx,%rbx,8), %rax
	movq	%rax, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L950:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L913:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rsi
	jb	.L953
	cmpq	%rbx, %r15
	movq	%rbx, %rsi
	cmovnb	%r15, %rsi
	addq	%r15, %rsi
	cmpq	%rdx, %rsi
	cmova	%rdx, %rsi
	salq	$3, %rsi
	movq	%rsi, %rdi
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	cmpq	$1, %rbx
	movq	-56(%rbp), %rsi
	movq	%rax, %r14
	leaq	(%rax,%r13), %rcx
	je	.L931
	leaq	-2(%rbx), %rax
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	shrq	%rax
	addq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L922:
	movq	%rdx, %rdi
	addq	$1, %rdx
	salq	$4, %rdi
	movups	%xmm0, (%rcx,%rdi)
	cmpq	%rdx, %rax
	ja	.L922
	leaq	(%rax,%rax), %rdx
	salq	$4, %rax
	addq	%rax, %rcx
	cmpq	%rdx, %rbx
	je	.L920
.L931:
	movq	$0, (%rcx)
.L920:
	movq	8(%r12), %rcx
	movq	(%r12), %rdi
	cmpq	%rdi, %rcx
	je	.L930
	leaq	-8(%rcx), %rax
	leaq	15(%rdi), %rdx
	subq	%rdi, %rax
	subq	%r14, %rdx
	shrq	$3, %rax
	cmpq	$30, %rdx
	jbe	.L934
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rax
	je	.L934
	addq	$1, %rax
	xorl	%edx, %edx
	movq	%rax, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L928:
	movdqu	(%rdi,%rdx), %xmm1
	movups	%xmm1, (%r14,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L928
	movq	%rax, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rdx
	leaq	(%rdi,%rdx), %rcx
	addq	%r14, %rdx
	cmpq	%r8, %rax
	je	.L924
	movq	(%rcx), %rax
	movq	%rax, (%rdx)
.L924:
	movq	%rsi, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rsi
.L925:
	addq	%r15, %rbx
	movq	%r14, (%r12)
	leaq	(%r14,%rbx,8), %rax
	addq	%rsi, %r14
	movq	%rax, 8(%r12)
	movq	%r14, 16(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L934:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L926:
	movq	(%rax), %r8
	addq	$8, %rax
	addq	$8, %rdx
	movq	%r8, -8(%rdx)
	cmpq	%rax, %rcx
	jne	.L926
.L930:
	testq	%rdi, %rdi
	je	.L925
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L932:
	movq	%rcx, %rdx
	jmp	.L914
.L953:
	leaq	.LC51(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE21915:
	.size	_ZNSt6vectorIN2v88internal6HandleINS1_19WasmExceptionObjectEEESaIS4_EE17_M_default_appendEm, .-_ZNSt6vectorIN2v88internal6HandleINS1_19WasmExceptionObjectEEESaIS4_EE17_M_default_appendEm
	.section	.rodata._ZNSt6vectorIN2v88internal4wasm15InstanceBuilder15SanitizedImportESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_.str1.1,"aMS",@progbits,1
.LC52:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal4wasm15InstanceBuilder15SanitizedImportESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm15InstanceBuilder15SanitizedImportESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm15InstanceBuilder15SanitizedImportESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm15InstanceBuilder15SanitizedImportESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm15InstanceBuilder15SanitizedImportESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB22615:
	.cfi_startproc
	endbr64
	movabsq	$-6148914691236517205, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r14
	movq	%rsi, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	movabsq	$384307168202282325, %rcx
	cmpq	%rcx, %rax
	je	.L971
	movq	%r12, %r8
	movq	%rdi, %r15
	subq	%r14, %r8
	testq	%rax, %rax
	je	.L963
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L972
.L956:
	movq	%rbx, %rdi
	movq	%rdx, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %r8
	movq	%rax, %r13
	leaq	(%rax,%rbx), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	24(%r13), %rbx
.L962:
	movdqu	(%rdx), %xmm2
	movq	16(%rdx), %rax
	movups	%xmm2, 0(%r13,%r8)
	movq	%rax, 16(%r13,%r8)
	cmpq	%r14, %r12
	je	.L958
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L959:
	movdqu	(%rax), %xmm1
	addq	$24, %rax
	addq	$24, %rdx
	movups	%xmm1, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L959
	leaq	-24(%r12), %rax
	subq	%r14, %rax
	shrq	$3, %rax
	leaq	48(%r13,%rax,8), %rbx
.L958:
	cmpq	%rsi, %r12
	je	.L960
	subq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	-24(%rsi), %rax
	movq	%r12, %rsi
	shrq	$3, %rax
	leaq	24(,%rax,8), %rdx
	movq	%rdx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rdx
	addq	%rdx, %rbx
.L960:
	testq	%r14, %r14
	je	.L961
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L961:
	movq	-56(%rbp), %rax
	movq	%r13, %xmm0
	movq	%rbx, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L972:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L957
	movq	$0, -56(%rbp)
	movl	$24, %ebx
	xorl	%r13d, %r13d
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L963:
	movl	$24, %ebx
	jmp	.L956
.L957:
	cmpq	%rcx, %rdi
	movq	%rcx, %rbx
	cmovbe	%rdi, %rbx
	imulq	$24, %rbx, %rbx
	jmp	.L956
.L971:
	leaq	.LC52(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22615:
	.size	_ZNSt6vectorIN2v88internal4wasm15InstanceBuilder15SanitizedImportESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm15InstanceBuilder15SanitizedImportESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder15SanitizeImportsEv.str1.8,"aMS",@progbits,1
	.align 8
.LC53:
	.string	"Could not resolve module name for import %zu"
	.align 8
.LC54:
	.string	"Could not resolve import name for import %zu"
	.align 8
.LC55:
	.string	"Could not find value for import %zu"
	.section	.text._ZN2v88internal4wasm15InstanceBuilder15SanitizeImportsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15InstanceBuilder15SanitizeImportsEv
	.type	_ZN2v88internal4wasm15InstanceBuilder15SanitizeImportsEv, @function
_ZN2v88internal4wasm15InstanceBuilder15SanitizeImportsEv:
.LFB18597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	232(%rax), %rax
	movq	8(%rax), %r13
	movq	(%rax), %r14
	movq	24(%rdi), %rax
	movq	208(%rax), %rdx
	cmpq	%rdx, 216(%rax)
	je	.L973
	movq	%rdi, %r15
	xorl	%r12d, %r12d
.L984:
	leaq	(%r12,%r12,2), %rax
	movq	(%r15), %rdi
	movq	%r14, %rsi
	leaq	(%rdx,%rax,8), %r9
	movq	%r13, %rdx
	movq	(%r9), %rcx
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal16WasmModuleObject32ExtractUtf8StringFromModuleBytesEPNS0_7IsolateENS0_6VectorIKhEENS0_4wasm12WireBytesRefE@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L988
	movq	8(%r9), %rcx
	movq	(%r15), %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	call	_ZN2v88internal16WasmModuleObject32ExtractUtf8StringFromModuleBytesEPNS0_7IsolateENS0_6VectorIKhEENS0_4wasm12WireBytesRefE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L989
	movq	24(%r15), %rax
	cmpb	$0, 392(%rax)
	je	.L978
	movq	%rcx, %rdx
	movl	%r12d, %esi
	movq	%r15, %rdi
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal4wasm15InstanceBuilder15LookupImportAsmEjNS0_6HandleINS0_6StringEEE
	movq	-88(%rbp), %rcx
.L979:
	movq	32(%r15), %rdi
	movl	16(%rdi), %edx
	testl	%edx, %edx
	jne	.L990
	testq	%rax, %rax
	je	.L991
	movq	%rbx, %xmm0
	movq	%rcx, %xmm1
	movq	%rax, -64(%rbp)
	movq	120(%r15), %rsi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	cmpq	128(%r15), %rsi
	je	.L982
	movdqa	-80(%rbp), %xmm2
	addq	$1, %r12
	movabsq	$-6148914691236517205, %rbx
	movups	%xmm2, (%rsi)
	movq	-64(%rbp), %rax
	movq	%rax, 16(%rsi)
	movq	24(%r15), %rax
	addq	$24, 120(%r15)
	movq	208(%rax), %rdx
	movq	216(%rax), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	imulq	%rbx, %rax
	cmpq	%r12, %rax
	ja	.L984
	.p2align 4,,10
	.p2align 3
.L973:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L992
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L978:
	.cfi_restore_state
	movq	%rbx, %rdx
	movl	%r12d, %esi
	movq	%r15, %rdi
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal4wasm15InstanceBuilder12LookupImportEjNS0_6HandleINS0_6StringEEES5_
	movq	-88(%rbp), %rcx
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L988:
	movq	32(%r15), %rdi
	movq	%r12, %rdx
	leaq	.LC53(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L989:
	movq	32(%r15), %rdi
	movq	%r12, %rdx
	leaq	.LC54(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L982:
	leaq	-80(%rbp), %rdx
	leaq	112(%r15), %rdi
	addq	$1, %r12
	movabsq	$-6148914691236517205, %rbx
	call	_ZNSt6vectorIN2v88internal4wasm15InstanceBuilder15SanitizedImportESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	24(%r15), %rax
	movq	208(%rax), %rdx
	movq	216(%rax), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	imulq	%rbx, %rax
	cmpq	%rax, %r12
	jb	.L984
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L991:
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L990:
	movq	%r12, %rdx
	leaq	.LC55(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	jmp	.L973
.L992:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18597:
	.size	_ZN2v88internal4wasm15InstanceBuilder15SanitizeImportsEv, .-_ZN2v88internal4wasm15InstanceBuilder15SanitizeImportsEv
	.section	.text._ZN2v88internal4wasm19TruncatedUserStringILi50EEC2EPKcm,"axG",@progbits,_ZN2v88internal4wasm19TruncatedUserStringILi50EEC5EPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm19TruncatedUserStringILi50EEC2EPKcm
	.type	_ZN2v88internal4wasm19TruncatedUserStringILi50EEC2EPKcm, @function
_ZN2v88internal4wasm19TruncatedUserStringILi50EEC2EPKcm:
.LFB22659:
	.cfi_startproc
	endbr64
	cmpl	$50, %edx
	movl	$50, %eax
	movq	%rsi, (%rdi)
	cmovl	%edx, %eax
	movl	%eax, 8(%rdi)
	cmpq	$50, %rdx
	ja	.L996
	ret
	.p2align 4,,10
	.p2align 3
.L996:
	movdqu	(%rsi), %xmm0
	leaq	12(%rdi), %rax
	movups	%xmm0, 12(%rdi)
	movdqu	16(%rsi), %xmm1
	movups	%xmm1, 28(%rdi)
	movq	32(%rsi), %rdx
	movq	%rdx, 44(%rdi)
	movl	40(%rsi), %edx
	movl	%edx, 52(%rdi)
	movzwl	44(%rsi), %edx
	movw	%dx, 56(%rdi)
	movzbl	46(%rsi), %edx
	movb	$46, 61(%rdi)
	movb	%dl, 58(%rdi)
	movl	$11822, %edx
	movw	%dx, 59(%rdi)
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE22659:
	.size	_ZN2v88internal4wasm19TruncatedUserStringILi50EEC2EPKcm, .-_ZN2v88internal4wasm19TruncatedUserStringILi50EEC2EPKcm
	.weak	_ZN2v88internal4wasm19TruncatedUserStringILi50EEC1EPKcm
	.set	_ZN2v88internal4wasm19TruncatedUserStringILi50EEC1EPKcm,_ZN2v88internal4wasm19TruncatedUserStringILi50EEC2EPKcm
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder14ProcessExportsENS0_6HandleINS0_18WasmInstanceObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC56:
	.string	"global_addr >= backing_store && global_addr < backing_store + buffer_size"
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder14ProcessExportsENS0_6HandleINS0_18WasmInstanceObjectEEE.str1.1,"aMS",@progbits,1
.LC57:
	.string	"export of %.*s failed."
	.section	.text._ZN2v88internal4wasm15InstanceBuilder14ProcessExportsENS0_6HandleINS0_18WasmInstanceObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15InstanceBuilder14ProcessExportsENS0_6HandleINS0_18WasmInstanceObjectEEE
	.type	_ZN2v88internal4wasm15InstanceBuilder14ProcessExportsENS0_6HandleINS0_18WasmInstanceObjectEEE, @function
_ZN2v88internal4wasm15InstanceBuilder14ProcessExportsENS0_6HandleINS0_18WasmInstanceObjectEEE:
.LFB18667:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -200(%rbp)
	movq	24(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	72(%rdx), %eax
	testl	%eax, %eax
	jne	.L998
	movq	184(%rdx), %rax
	movq	192(%rdx), %rcx
	cmpq	%rcx, %rax
	jne	.L1000
	jmp	.L999
	.p2align 4,,10
	.p2align 3
.L1100:
	addq	$16, %rax
	cmpq	%rax, %rcx
	je	.L999
.L1000:
	cmpb	$7, (%rax)
	jne	.L1100
.L998:
	movq	208(%rdx), %rax
	movq	216(%rdx), %rcx
	movabsq	$-6148914691236517205, %rsi
	subq	%rax, %rcx
	sarq	$3, %rcx
	imulq	%rsi, %rcx
	testl	%ecx, %ecx
	jle	.L999
	subl	$1, %ecx
	movq	%r12, -208(%rbp)
	xorl	%ebx, %ebx
	movq	%rax, %r13
	leaq	3(%rcx,%rcx,2), %r15
	movq	%r14, %r12
	salq	$3, %r15
	jmp	.L1003
	.p2align 4,,10
	.p2align 3
.L1001:
	addq	$24, %rbx
	cmpq	%rbx, %r15
	je	.L1093
.L1101:
	movq	208(%rdx), %r13
.L1003:
	addq	%rbx, %r13
	cmpb	$0, 16(%r13)
	jne	.L1001
	movq	112(%r12), %rax
	movq	16(%rax,%rbx), %r14
	movq	(%r14), %rdi
	call	_ZN2v88internal20WasmExternalFunction22IsWasmExternalFunctionENS0_6ObjectE@PLT
	testb	%al, %al
	jne	.L1002
.L1095:
	addq	$24, %rbx
	movq	24(%r12), %rdx
	cmpq	%rbx, %r15
	jne	.L1101
.L1093:
	movq	%r12, %r14
	movq	-208(%rbp), %r12
.L999:
	movzbl	392(%rdx), %eax
	testb	%al, %al
	je	.L1004
	subl	$1, %eax
	cmpb	$1, %al
	ja	.L1005
	movq	(%r14), %rbx
	movq	12464(%rbx), %rax
	movq	39(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1008
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1009:
	movq	879(%rsi), %r13
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1011
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1012:
	movq	(%r14), %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	_ZN2v88internal5AsmJs19kSingleFunctionNameE(%rip), %rbx
	movq	(%r14), %r13
	movq	%rax, -208(%rbp)
	movq	%rbx, %rdi
	call	strlen@PLT
	leaq	-176(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rbx, -176(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movb	$1, -217(%rbp)
	movq	%rax, -232(%rbp)
.L1007:
	movq	-200(%rbp), %rax
	movq	(%rax), %r15
	movq	-208(%rbp), %rax
	movq	(%rax), %r13
	leaq	143(%r15), %rbx
	movq	%r13, 143(%r15)
	testb	$1, %r13b
	je	.L1071
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -216(%rbp)
	testl	$262144, %eax
	je	.L1015
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %rcx
	movq	8(%rcx), %rax
.L1015:
	testb	$24, %al
	je	.L1071
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1102
	.p2align 4,,10
	.p2align 3
.L1071:
	movzbl	-217(%rbp), %ecx
	pxor	%xmm0, %xmm0
	movups	%xmm0, -168(%rbp)
	leal	0(,%rcx,4), %eax
	movl	%ecx, %edx
	movups	%xmm0, -152(%rbp)
	sall	$4, %edx
	orl	$43, %eax
	orl	%edx, %eax
	movzbl	-176(%rbp), %edx
	andl	$-64, %edx
	orl	%edx, %eax
	movb	%al, -176(%rbp)
	movq	24(%r14), %rax
	movq	240(%rax), %rdi
	movq	232(%rax), %rbx
	movq	%rdi, -216(%rbp)
	cmpq	%rdi, %rbx
	je	.L1017
	movq	%r14, %rax
	leaq	.L1020(%rip), %r13
	movq	%r12, %r14
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L1069:
	movq	40(%r12), %rsi
	movq	(%rbx), %rdx
	movq	(%r12), %rdi
	call	_ZN2v88internal16WasmModuleObject32ExtractUtf8StringFromModuleBytesEPNS0_7IsolateENS0_6HandleIS1_EENS0_4wasm12WireBytesRefE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1027
	cmpb	$4, 8(%rbx)
	ja	.L1005
	movzbl	8(%rbx), %eax
	movslq	0(%r13,%rax,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder14ProcessExportsENS0_6HandleINS0_18WasmInstanceObjectEEE,"a",@progbits
	.align 4
	.align 4
.L1020:
	.long	.L1024-.L1020
	.long	.L1023-.L1020
	.long	.L1022-.L1020
	.long	.L1021-.L1020
	.long	.L1019-.L1020
	.section	.text._ZN2v88internal4wasm15InstanceBuilder14ProcessExportsENS0_6HandleINS0_18WasmInstanceObjectEEE
	.p2align 4,,10
	.p2align 3
.L1002:
	movl	20(%r13), %edx
	movq	(%r12), %rdi
	movq	%r14, %rcx
	movq	-200(%rbp), %rsi
	call	_ZN2v88internal18WasmInstanceObject23SetWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_20WasmExternalFunctionEEE@PLT
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1008:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1103
.L1010:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1021:
	movq	24(%r12), %rax
	movl	12(%rbx), %ecx
	movq	(%r12), %rdx
	salq	$5, %rcx
	addq	24(%rax), %rcx
	movq	-200(%rbp), %rax
	cmpb	$0, 1(%rcx)
	movq	41112(%rdx), %rdi
	movq	(%rax), %rsi
	je	.L1042
	cmpb	$0, 28(%rcx)
	je	.L1042
	movq	183(%rsi), %rsi
	testq	%rdi, %rdi
	je	.L1043
	movq	%rcx, -240(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-240(%rbp), %rcx
	movq	(%rax), %rsi
.L1044:
	movl	24(%rcx), %eax
	movzbl	(%rcx), %edi
	movq	(%r12), %rdx
	leal	16(,%rax,8), %eax
	cltq
	leaq	-1(%rsi,%rax), %rsi
	leal	-6(%rdi), %eax
	cmpb	$1, %al
	jbe	.L1073
	cmpb	$9, %dil
	je	.L1073
	movq	(%rsi), %r9
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1052
	movq	%r9, %rsi
	movq	%rcx, -240(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-240(%rbp), %rcx
	movq	(%rax), %r9
	movq	%rax, %rsi
.L1053:
	movq	-200(%rbp), %rax
	movl	24(%rcx), %edx
	movq	(%rax), %rax
	movq	111(%rax), %rax
	movq	(%rax,%rdx,8), %r8
	movq	31(%r9), %rax
	movq	23(%r9), %rdx
	cmpq	%rax, %r8
	jb	.L1055
	addq	%rax, %rdx
	cmpq	%r8, %rdx
	jbe	.L1055
	subl	%eax, %r8d
	xorl	%r10d, %r10d
	jmp	.L1051
	.p2align 4,,10
	.p2align 3
.L1022:
	movq	(%r12), %rdx
	movq	-200(%rbp), %rax
	movq	41112(%rdx), %rdi
	movq	(%rax), %rax
	movq	159(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1039
.L1098:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1037:
	movq	%rax, -168(%rbp)
.L1096:
	movq	(%r12), %rdi
	movq	-208(%rbp), %rsi
.L1035:
	movb	$1, %r14b
	leaq	-176(%rbp), %rcx
	movq	%r15, %rdx
	movl	%r14d, %r14d
	movq	%r14, %r8
	call	_ZN2v88internal10JSReceiver17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	je	.L1104
	addq	$16, %rbx
	cmpq	%rbx, -216(%rbp)
	jne	.L1069
	movq	24(%r12), %rax
.L1017:
	cmpb	$0, 392(%rax)
	je	.L1105
.L997:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1106
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1024:
	.cfi_restore_state
	movl	12(%rbx), %edx
	movq	(%r12), %rdi
	movq	-200(%rbp), %rsi
	call	_ZN2v88internal18WasmInstanceObject31GetOrCreateWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	cmpb	$0, -217(%rbp)
	movq	%rax, -168(%rbp)
	je	.L1096
	movq	-232(%rbp), %rax
	testq	%rax, %rax
	je	.L1027
	movq	(%r12), %rdi
	cmpq	%r15, %rax
	je	.L1097
	movq	%rax, %rdx
	movq	-200(%rbp), %rsi
	movq	(%r15), %rax
	cmpq	(%rdx), %rax
	je	.L1035
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L1034
	movq	(%rdx), %rax
	movq	-1(%rax), %rax
	movq	-208(%rbp), %rsi
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	je	.L1035
.L1034:
	movq	-232(%rbp), %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal6String10SlowEqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testb	%al, %al
	je	.L1096
	movq	(%r12), %rdi
.L1097:
	movq	-200(%rbp), %rsi
	jmp	.L1035
	.p2align 4,,10
	.p2align 3
.L1023:
	movq	-200(%rbp), %rax
	movq	(%r12), %rdx
	movq	(%rax), %rcx
	movl	12(%rbx), %eax
	movq	199(%rcx), %rcx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	jne	.L1098
	movq	41088(%rdx), %rax
	cmpq	%rax, 41096(%rdx)
	je	.L1099
.L1038:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1037
	.p2align 4,,10
	.p2align 3
.L1019:
	movl	12(%rbx), %ecx
	movq	80(%r12), %rax
	movq	(%r12), %rdi
	movq	(%rax,%rcx,8), %rax
	testq	%rax, %rax
	je	.L1107
.L1063:
	movq	%rax, -168(%rbp)
	movq	-208(%rbp), %rsi
	jmp	.L1035
	.p2align 4,,10
	.p2align 3
.L1042:
	movzbl	(%rcx), %eax
	leal	-6(%rax), %r8d
	cmpb	$1, %r8b
	jbe	.L1074
	cmpb	$9, %al
	je	.L1074
	movq	167(%rsi), %r8
	testq	%rdi, %rdi
	je	.L1061
	movq	%r8, %rsi
	movq	%rcx, -240(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-240(%rbp), %rcx
	xorl	%r10d, %r10d
	movq	%rax, %rsi
.L1059:
	movl	24(%rcx), %r8d
.L1051:
	movzbl	(%rcx), %r11d
	movzbl	1(%rcx), %r9d
	movq	%r10, %rdx
	movq	(%r12), %rdi
	movl	%r11d, %ecx
	call	_ZN2v88internal16WasmGlobalObject3NewEPNS0_7IsolateENS0_11MaybeHandleINS0_13JSArrayBufferEEENS4_INS0_10FixedArrayEEENS0_4wasm9ValueTypeEib@PLT
	testq	%rax, %rax
	jne	.L1037
	.p2align 4,,10
	.p2align 3
.L1027:
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1004:
	movq	(%r14), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE@PLT
	movb	$0, -217(%rbp)
	movq	%rax, -208(%rbp)
	movq	$0, -232(%rbp)
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L1039:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	jne	.L1038
.L1099:
	movq	%rdx, %rdi
	movq	%rsi, -248(%rbp)
	movq	%rdx, -240(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-248(%rbp), %rsi
	movq	-240(%rbp), %rdx
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1074:
	movq	175(%rsi), %rsi
	testq	%rdi, %rdi
	je	.L1058
	movq	%rcx, -240(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-240(%rbp), %rcx
	xorl	%esi, %esi
	movq	%rax, %r10
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1011:
	movq	41088(%rbx), %rsi
	cmpq	%rsi, 41096(%rbx)
	je	.L1108
.L1013:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r13, (%rsi)
	jmp	.L1012
	.p2align 4,,10
	.p2align 3
.L1102:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1071
	.p2align 4,,10
	.p2align 3
.L1043:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1109
.L1045:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1058:
	movq	41088(%rdx), %r10
	cmpq	41096(%rdx), %r10
	je	.L1110
.L1060:
	leaq	8(%r10), %rdi
	movq	%rdi, 41088(%rdx)
	movq	%rsi, (%r10)
	xorl	%esi, %esi
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1061:
	movq	41088(%rdx), %rsi
	cmpq	41096(%rdx), %rsi
	je	.L1111
.L1062:
	leaq	8(%rsi), %rax
	xorl	%r10d, %r10d
	movq	%rax, 41088(%rdx)
	movq	%r8, (%rsi)
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1107:
	movq	24(%r12), %rax
	movq	256(%rax), %r8
	movq	-200(%rbp), %rax
	movq	(%rax), %rsi
	leal	16(,%rcx,8), %eax
	cltq
	movq	223(%rsi), %rdx
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%rdi), %r9
	testq	%r9, %r9
	je	.L1064
	movq	%r9, %rdi
	movq	%rcx, -248(%rbp)
	movq	%r8, -240(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-240(%rbp), %r8
	movq	-248(%rbp), %rcx
	movq	%rax, %rdx
.L1065:
	movq	(%r8,%rcx,8), %rsi
	movq	(%r12), %rdi
	call	_ZN2v88internal19WasmExceptionObject3NewEPNS0_7IsolateEPKNS0_9SignatureINS0_4wasm9ValueTypeEEENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	12(%rbx), %ecx
	movq	80(%r12), %rdx
	movq	%rax, (%rdx,%rcx,8)
	movq	(%r12), %rdi
	jmp	.L1063
	.p2align 4,,10
	.p2align 3
.L1073:
	movq	(%rsi), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1048
	movq	%rcx, -240(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-240(%rbp), %rcx
	movq	%rax, %r10
.L1049:
	movq	-200(%rbp), %rax
	movl	24(%rcx), %esi
	movq	(%rax), %rdx
	movq	111(%rdx), %rdx
	movl	(%rdx,%rsi,8), %r8d
	xorl	%esi, %esi
	jmp	.L1051
	.p2align 4,,10
	.p2align 3
.L1104:
	movq	(%r15), %rax
	leaq	-185(%rbp), %rsi
	leaq	-184(%rbp), %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	leaq	-128(%rbp), %rdi
	movq	%rax, %rsi
	movslq	%edx, %rdx
	call	_ZN2v88internal4wasm19TruncatedUserStringILi50EEC1EPKcm
	movq	32(%r12), %rdi
	movq	-128(%rbp), %rcx
	xorl	%eax, %eax
	movl	-120(%rbp), %edx
	leaq	.LC57(%rip), %rsi
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	jmp	.L997
	.p2align 4,,10
	.p2align 3
.L1064:
	movq	41088(%rdi), %rdx
	cmpq	41096(%rdi), %rdx
	je	.L1112
.L1066:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rdi)
	movq	%rsi, (%rdx)
	jmp	.L1065
	.p2align 4,,10
	.p2align 3
.L1052:
	movq	41088(%rdx), %rsi
	cmpq	41096(%rdx), %rsi
	je	.L1113
.L1054:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdx)
	movq	%r9, (%rsi)
	jmp	.L1053
	.p2align 4,,10
	.p2align 3
.L1048:
	movq	41088(%rdx), %r10
	cmpq	41096(%rdx), %r10
	je	.L1114
.L1050:
	leaq	8(%r10), %rdi
	movq	%rdi, 41088(%rdx)
	movq	%rsi, (%r10)
	jmp	.L1049
.L1108:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1013
.L1103:
	movq	%rbx, %rdi
	movq	%rsi, -208(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-208(%rbp), %rsi
	jmp	.L1010
.L1105:
	movq	-208(%rbp), %rdi
	movl	$1, %edx
	movl	$5, %esi
	call	_ZN2v88internal10JSReceiver17SetIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesENS0_11ShouldThrowE@PLT
	jmp	.L997
.L1111:
	movq	%rdx, %rdi
	movq	%r8, -256(%rbp)
	movq	%rcx, -248(%rbp)
	movq	%rdx, -240(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-256(%rbp), %r8
	movq	-248(%rbp), %rcx
	movq	-240(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L1062
.L1110:
	movq	%rdx, %rdi
	movq	%rsi, -256(%rbp)
	movq	%rcx, -248(%rbp)
	movq	%rdx, -240(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-256(%rbp), %rsi
	movq	-248(%rbp), %rcx
	movq	-240(%rbp), %rdx
	movq	%rax, %r10
	jmp	.L1060
.L1055:
	leaq	.LC56(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1109:
	movq	%rdx, %rdi
	movq	%rsi, -256(%rbp)
	movq	%rcx, -248(%rbp)
	movq	%rdx, -240(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-256(%rbp), %rsi
	movq	-248(%rbp), %rcx
	movq	-240(%rbp), %rdx
	jmp	.L1045
.L1112:
	movq	%rcx, -264(%rbp)
	movq	%rsi, -256(%rbp)
	movq	%r8, -248(%rbp)
	movq	%rdi, -240(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %rcx
	movq	-256(%rbp), %rsi
	movq	-248(%rbp), %r8
	movq	-240(%rbp), %rdi
	movq	%rax, %rdx
	jmp	.L1066
.L1113:
	movq	%rdx, %rdi
	movq	%r9, -256(%rbp)
	movq	%rcx, -248(%rbp)
	movq	%rdx, -240(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-256(%rbp), %r9
	movq	-248(%rbp), %rcx
	movq	-240(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L1054
.L1114:
	movq	%rdx, %rdi
	movq	%rsi, -256(%rbp)
	movq	%rcx, -248(%rbp)
	movq	%rdx, -240(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-256(%rbp), %rsi
	movq	-248(%rbp), %rcx
	movq	-240(%rbp), %rdx
	movq	%rax, %r10
	jmp	.L1050
.L1005:
	leaq	.LC13(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1106:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18667:
	.size	_ZN2v88internal4wasm15InstanceBuilder14ProcessExportsENS0_6HandleINS0_18WasmInstanceObjectEEE, .-_ZN2v88internal4wasm15InstanceBuilder14ProcessExportsENS0_6HandleINS0_18WasmInstanceObjectEEE
	.section	.text._ZNSt10_HashtableISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS2_9SignatureINS2_4wasm9ValueTypeEEEESA_SaISA_ENSt8__detail9_IdentityESt8equal_toISA_ENS6_22WasmImportWrapperCache12CacheKeyHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb1ELb1EEEE5eraseENSC_20_Node_const_iteratorISA_Lb1ELb1EEE,"axG",@progbits,_ZNSt10_HashtableISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS2_9SignatureINS2_4wasm9ValueTypeEEEESA_SaISA_ENSt8__detail9_IdentityESt8equal_toISA_ENS6_22WasmImportWrapperCache12CacheKeyHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb1ELb1EEEE5eraseENSC_20_Node_const_iteratorISA_Lb1ELb1EEE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS2_9SignatureINS2_4wasm9ValueTypeEEEESA_SaISA_ENSt8__detail9_IdentityESt8equal_toISA_ENS6_22WasmImportWrapperCache12CacheKeyHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb1ELb1EEEE5eraseENSC_20_Node_const_iteratorISA_Lb1ELb1EEE
	.type	_ZNSt10_HashtableISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS2_9SignatureINS2_4wasm9ValueTypeEEEESA_SaISA_ENSt8__detail9_IdentityESt8equal_toISA_ENS6_22WasmImportWrapperCache12CacheKeyHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb1ELb1EEEE5eraseENSC_20_Node_const_iteratorISA_Lb1ELb1EEE, @function
_ZNSt10_HashtableISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS2_9SignatureINS2_4wasm9ValueTypeEEEESA_SaISA_ENSt8__detail9_IdentityESt8equal_toISA_ENS6_22WasmImportWrapperCache12CacheKeyHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb1ELb1EEEE5eraseENSC_20_Node_const_iteratorISA_Lb1ELb1EEE:
.LFB22822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	24(%rdi), %rax
	movq	8(%rbx), %rsi
	movq	(%rbx), %r8
	divq	%rsi
	leaq	0(,%rdx,8), %r10
	movq	%rdx, %r11
	leaq	(%r8,%r10), %r9
	movq	(%r9), %rax
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L1116:
	movq	%rdx, %rcx
	movq	(%rdx), %rdx
	cmpq	%rdx, %rdi
	jne	.L1116
	movq	(%rdi), %r12
	cmpq	%rcx, %rax
	je	.L1127
	testq	%r12, %r12
	je	.L1119
	movq	24(%r12), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %r11
	je	.L1119
	movq	%rcx, (%r8,%rdx,8)
	movq	(%rdi), %r12
.L1119:
	movq	%r12, (%rcx)
	call	_ZdlPv@PLT
	movq	%r12, %rax
	subq	$1, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1127:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L1121
	movq	24(%r12), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %r11
	je	.L1119
	movq	%rcx, (%r8,%rdx,8)
	addq	(%rbx), %r10
	movq	(%r10), %rax
	movq	%r10, %r9
.L1118:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L1128
.L1120:
	movq	$0, (%r9)
	movq	(%rdi), %r12
	jmp	.L1119
.L1121:
	movq	%rcx, %rax
	jmp	.L1118
.L1128:
	movq	%r12, 16(%rbx)
	jmp	.L1120
	.cfi_endproc
.LFE22822:
	.size	_ZNSt10_HashtableISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS2_9SignatureINS2_4wasm9ValueTypeEEEESA_SaISA_ENSt8__detail9_IdentityESt8equal_toISA_ENS6_22WasmImportWrapperCache12CacheKeyHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb1ELb1EEEE5eraseENSC_20_Node_const_iteratorISA_Lb1ELb1EEE, .-_ZNSt10_HashtableISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS2_9SignatureINS2_4wasm9ValueTypeEEEESA_SaISA_ENSt8__detail9_IdentityESt8equal_toISA_ENS6_22WasmImportWrapperCache12CacheKeyHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb1ELb1EEEE5eraseENSC_20_Node_const_iteratorISA_Lb1ELb1EEE
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTask11RunInternalEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTask11RunInternalEv, @function
_ZN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTask11RunInternalEv:
.LFB18549:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	jmp	.L1131
	.p2align 4,,10
	.p2align 3
.L1133:
	leaq	40(%r12), %rdi
	movzbl	8(%rsi), %r13d
	movq	16(%rsi), %r14
	call	_ZNSt10_HashtableISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS2_9SignatureINS2_4wasm9ValueTypeEEEESA_SaISA_ENSt8__detail9_IdentityESt8equal_toISA_ENS6_22WasmImportWrapperCache12CacheKeyHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb1ELb1EEEE5eraseENSC_20_Node_const_iteratorISA_Lb1ELb1EEE
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	48(%rbx), %rdx
	movq	56(%rbx), %rsi
	movq	%r14, %r8
	movq	40(%rbx), %rdi
	movq	72(%rbx), %r9
	movl	%r13d, %ecx
	call	_ZN2v88internal4wasm20CompileImportWrapperEPNS1_10WasmEngineEPNS1_12NativeModuleEPNS0_8CountersENS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEEPNS1_22WasmImportWrapperCache17ModificationScopeE@PLT
.L1131:
	movq	64(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	56(%r12), %rsi
	testq	%rsi, %rsi
	jne	.L1133
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE18549:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTask11RunInternalEv, .-_ZN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTask11RunInternalEv
	.section	.text._ZN2v88internal14CancelableTask3RunEv,"axG",@progbits,_ZN2v88internal14CancelableTask3RunEv,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal14CancelableTask3RunEv
	.type	_ZThn32_N2v88internal14CancelableTask3RunEv, @function
_ZThn32_N2v88internal14CancelableTask3RunEv:
.LFB23476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	lock cmpxchgl	%edx, -16(%rdi)
	jne	.L1134
	movq	-32(%rdi), %rax
	leaq	_ZN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTask11RunInternalEv(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1138
	popq	%rbx
	subq	$32, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
.L1134:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1138:
	.cfi_restore_state
	movq	32(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	56(%r12), %rsi
	testq	%rsi, %rsi
	je	.L1137
	leaq	40(%r12), %rdi
	movzbl	8(%rsi), %r13d
	movq	16(%rsi), %r14
	call	_ZNSt10_HashtableISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS2_9SignatureINS2_4wasm9ValueTypeEEEESA_SaISA_ENSt8__detail9_IdentityESt8equal_toISA_ENS6_22WasmImportWrapperCache12CacheKeyHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb1ELb1EEEE5eraseENSC_20_Node_const_iteratorISA_Lb1ELb1EEE
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	16(%rbx), %rdx
	movq	24(%rbx), %rsi
	movq	%r14, %r8
	movq	8(%rbx), %rdi
	movq	40(%rbx), %r9
	movl	%r13d, %ecx
	call	_ZN2v88internal4wasm20CompileImportWrapperEPNS1_10WasmEngineEPNS1_12NativeModuleEPNS0_8CountersENS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEEPNS1_22WasmImportWrapperCache17ModificationScopeE@PLT
	jmp	.L1138
.L1137:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE23476:
	.size	_ZThn32_N2v88internal14CancelableTask3RunEv, .-_ZThn32_N2v88internal14CancelableTask3RunEv
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CancelableTask3RunEv
	.type	_ZN2v88internal14CancelableTask3RunEv, @function
_ZN2v88internal14CancelableTask3RunEv:
.LFB15077:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	movl	$2, %edx
	lock cmpxchgl	%edx, 16(%rdi)
	jne	.L1140
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTask11RunInternalEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1144
	jmp	.L1142
	.p2align 4,,10
	.p2align 3
.L1148:
	leaq	40(%r13), %rdi
	movzbl	8(%rsi), %r14d
	movq	16(%rsi), %rbx
	call	_ZNSt10_HashtableISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS2_9SignatureINS2_4wasm9ValueTypeEEEESA_SaISA_ENSt8__detail9_IdentityESt8equal_toISA_ENS6_22WasmImportWrapperCache12CacheKeyHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb1ELb1EEEE5eraseENSC_20_Node_const_iteratorISA_Lb1ELb1EEE
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	48(%r12), %rdx
	movq	%rbx, %r8
	movl	%r14d, %ecx
	movq	56(%r12), %rsi
	movq	40(%r12), %rdi
	movq	72(%r12), %r9
	call	_ZN2v88internal4wasm20CompileImportWrapperEPNS1_10WasmEngineEPNS1_12NativeModuleEPNS0_8CountersENS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEEPNS1_22WasmImportWrapperCache17ModificationScopeE@PLT
.L1144:
	movq	64(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	56(%r13), %rsi
	testq	%rsi, %rsi
	jne	.L1148
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r13, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L1142:
	.cfi_restore_state
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
.L1140:
	ret
	.cfi_endproc
.LFE15077:
	.size	_ZN2v88internal14CancelableTask3RunEv, .-_ZN2v88internal14CancelableTask3RunEv
	.section	.text._ZNSt10_HashtableISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS2_9SignatureINS2_4wasm9ValueTypeEEEESA_SaISA_ENSt8__detail9_IdentityESt8equal_toISA_ENS6_22WasmImportWrapperCache12CacheKeyHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKSA_NSC_10_AllocNodeISaINSC_10_Hash_nodeISA_Lb1EEEEEEEES0_INSC_14_Node_iteratorISA_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm,"axG",@progbits,_ZNSt10_HashtableISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS2_9SignatureINS2_4wasm9ValueTypeEEEESA_SaISA_ENSt8__detail9_IdentityESt8equal_toISA_ENS6_22WasmImportWrapperCache12CacheKeyHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKSA_NSC_10_AllocNodeISaINSC_10_Hash_nodeISA_Lb1EEEEEEEES0_INSC_14_Node_iteratorISA_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS2_9SignatureINS2_4wasm9ValueTypeEEEESA_SaISA_ENSt8__detail9_IdentityESt8equal_toISA_ENS6_22WasmImportWrapperCache12CacheKeyHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKSA_NSC_10_AllocNodeISaINSC_10_Hash_nodeISA_Lb1EEEEEEEES0_INSC_14_Node_iteratorISA_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	.type	_ZNSt10_HashtableISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS2_9SignatureINS2_4wasm9ValueTypeEEEESA_SaISA_ENSt8__detail9_IdentityESt8equal_toISA_ENS6_22WasmImportWrapperCache12CacheKeyHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKSA_NSC_10_AllocNodeISaINSC_10_Hash_nodeISA_Lb1EEEEEEEES0_INSC_14_Node_iteratorISA_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm, @function
_ZNSt10_HashtableISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS2_9SignatureINS2_4wasm9ValueTypeEEEESA_SaISA_ENSt8__detail9_IdentityESt8equal_toISA_ENS6_22WasmImportWrapperCache12CacheKeyHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKSA_NSC_10_AllocNodeISaINSC_10_Hash_nodeISA_Lb1EEEEEEEES0_INSC_14_Node_iteratorISA_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm:
.LFB22860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %rbx
	movzbl	(%rsi), %r15d
	movq	%rcx, -64(%rbp)
	movq	8(%rbx), %rdi
	movq	(%rbx), %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	16(%rbx), %rcx
	movq	8(%rbx), %rdx
	addq	%rcx, %rdx
	addq	(%rbx), %rdx
	cmpq	%rdx, %rcx
	je	.L1150
	movq	%rdx, %r14
	movq	%rcx, %rbx
	.p2align 4,,10
	.p2align 3
.L1151:
	movq	%rax, %rdi
	addq	$1, %rbx
	call	_ZN2v84base10hash_valueEm@PLT
	movzbl	-1(%rbx), %esi
	xorl	%edi, %edi
	movq	%rax, -56(%rbp)
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rdi
	movq	%r8, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	cmpq	%r14, %rbx
	jne	.L1151
.L1150:
	movq	%rax, %rsi
	xorl	%edi, %edi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	8(%r12), %rsi
	xorl	%edx, %edx
	movq	%rax, %r15
	divq	%rsi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	leaq	0(,%rdx,8), %r8
	testq	%rax, %rax
	je	.L1152
	movq	(%rax), %rcx
	movq	24(%rcx), %rdi
	jmp	.L1155
	.p2align 4,,10
	.p2align 3
.L1153:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1152
	movq	24(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L1152
.L1155:
	cmpq	%rdi, %r15
	jne	.L1153
	movzbl	8(%rcx), %eax
	cmpb	%al, 0(%r13)
	jne	.L1153
	movq	16(%rcx), %rax
	cmpq	%rax, 8(%r13)
	jne	.L1153
	addq	$24, %rsp
	movq	%rcx, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1152:
	.cfi_restore_state
	movl	$32, %edi
	movq	%r8, -56(%rbp)
	call	_Znwm@PLT
	movzbl	0(%r13), %edx
	movq	8(%r12), %rsi
	leaq	32(%r12), %rdi
	movq	$0, (%rax)
	movq	%rax, %rbx
	movq	8(%r13), %rax
	movb	%dl, 8(%rbx)
	movq	-64(%rbp), %rcx
	movq	%rax, 16(%rbx)
	movq	24(%r12), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r13
	testb	%al, %al
	jne	.L1156
	movq	(%r12), %r14
	movq	-56(%rbp), %r8
	movq	%r15, 24(%rbx)
	leaq	(%r14,%r8), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1166
.L1192:
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	(%rax), %rax
	movq	%rbx, (%rax)
.L1167:
	addq	$1, 24(%r12)
	addq	$24, %rsp
	movq	%rbx, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1156:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L1190
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1191
	leaq	0(,%rdx,8), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	memset@PLT
	leaq	48(%r12), %r9
.L1159:
	movq	16(%r12), %rsi
	movq	$0, 16(%r12)
	testq	%rsi, %rsi
	je	.L1161
	xorl	%edi, %edi
	leaq	16(%r12), %r8
	jmp	.L1162
	.p2align 4,,10
	.p2align 3
.L1163:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1164:
	testq	%rsi, %rsi
	je	.L1161
.L1162:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	24(%rcx), %rax
	divq	%r13
	leaq	(%r14,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L1163
	movq	16(%r12), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%r12)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L1170
	movq	%rcx, (%r14,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L1162
	.p2align 4,,10
	.p2align 3
.L1161:
	movq	(%r12), %rdi
	cmpq	%r9, %rdi
	je	.L1165
	call	_ZdlPv@PLT
.L1165:
	movq	%r15, %rax
	xorl	%edx, %edx
	movq	%r13, 8(%r12)
	divq	%r13
	movq	%r14, (%r12)
	movq	%r15, 24(%rbx)
	leaq	0(,%rdx,8), %r8
	leaq	(%r14,%r8), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L1192
.L1166:
	movq	16(%r12), %rdx
	movq	%rbx, 16(%r12)
	movq	%rdx, (%rbx)
	testq	%rdx, %rdx
	je	.L1168
	movq	24(%rdx), %rax
	xorl	%edx, %edx
	divq	8(%r12)
	movq	%rbx, (%r14,%rdx,8)
	movq	(%r12), %rax
	addq	%r8, %rax
.L1168:
	leaq	16(%r12), %rdx
	movq	%rdx, (%rax)
	jmp	.L1167
	.p2align 4,,10
	.p2align 3
.L1170:
	movq	%rdx, %rdi
	jmp	.L1164
	.p2align 4,,10
	.p2align 3
.L1190:
	movq	$0, 48(%r12)
	leaq	48(%r12), %r14
	movq	%r14, %r9
	jmp	.L1159
.L1191:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE22860:
	.size	_ZNSt10_HashtableISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS2_9SignatureINS2_4wasm9ValueTypeEEEESA_SaISA_ENSt8__detail9_IdentityESt8equal_toISA_ENS6_22WasmImportWrapperCache12CacheKeyHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKSA_NSC_10_AllocNodeISaINSC_10_Hash_nodeISA_Lb1EEEEEEEES0_INSC_14_Node_iteratorISA_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm, .-_ZNSt10_HashtableISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS2_9SignatureINS2_4wasm9ValueTypeEEEESA_SaISA_ENSt8__detail9_IdentityESt8equal_toISA_ENS6_22WasmImportWrapperCache12CacheKeyHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKSA_NSC_10_AllocNodeISaINSC_10_Hash_nodeISA_Lb1EEEEEEEES0_INSC_14_Node_iteratorISA_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	.section	.text._ZN2v88internal4wasm15InstanceBuilder21CompileImportWrappersENS0_6HandleINS0_18WasmInstanceObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15InstanceBuilder21CompileImportWrappersENS0_6HandleINS0_18WasmInstanceObjectEEE
	.type	_ZN2v88internal4wasm15InstanceBuilder21CompileImportWrappersENS0_6HandleINS0_18WasmInstanceObjectEEE, @function
_ZN2v88internal4wasm15InstanceBuilder21CompileImportWrappersENS0_6HandleINS0_18WasmInstanceObjectEEE:
.LFB18615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-320(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$392, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	216(%rax), %r13
	subq	208(%rax), %r13
	movabsq	$-6148914691236517205, %rax
	sarq	$3, %r13
	imulq	%rax, %r13
	movq	(%rsi), %rax
	movq	135(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %r14
	movq	528(%r14), %xmm0
	movq	528(%r14), %rdi
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -352(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	%r12, %rdi
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	-232(%rbp), %rax
	movq	$1, -272(%rbp)
	movq	%rax, -424(%rbp)
	movq	%rax, -280(%rbp)
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movl	$0x3f800000, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	testl	%r13d, %r13d
	jle	.L1194
	leal	-1(%r13), %eax
	movq	%r12, -384(%rbp)
	xorl	%r15d, %r15d
	leaq	3(%rax,%rax,2), %r8
	leaq	8(%rbx), %rax
	movq	%rax, -376(%rbp)
	leaq	0(,%r8,8), %r13
	jmp	.L1200
	.p2align 4,,10
	.p2align 3
.L1199:
	addq	$24, %r15
	cmpq	%r13, %r15
	je	.L1223
.L1200:
	movq	24(%rbx), %rax
	movq	208(%rax), %rdx
	cmpb	$0, 16(%rdx,%r15)
	jne	.L1199
	movq	112(%rbx), %rcx
	movq	16(%rcx,%r15), %rdi
	movq	(%rdi), %rcx
	testb	$1, %cl
	je	.L1199
	movq	-1(%rcx), %rcx
	testb	$2, 13(%rcx)
	je	.L1199
	movl	20(%rdx,%r15), %edx
	salq	$5, %rdx
	addq	136(%rax), %rdx
	movq	(%rdx), %r12
	movq	-376(%rbp), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler21ResolveWasmImportCallENS0_6HandleINS0_10JSReceiverEEEPNS0_9SignatureINS0_4wasm9ValueTypeEEERKNS6_12WasmFeaturesE@PLT
	leal	-2(%rax), %edx
	cmpb	$1, %dl
	jbe	.L1199
	testb	%al, %al
	je	.L1199
	leaq	-336(%rbp), %rsi
	leaq	-352(%rbp), %rdi
	movb	%al, -336(%rbp)
	movq	%rsi, -392(%rbp)
	movq	%r12, -328(%rbp)
	call	_ZN2v88internal4wasm22WasmImportWrapperCache17ModificationScopeixERKSt4pairINS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEEE@PLT
	movq	-392(%rbp), %rsi
	cmpq	$0, (%rax)
	jne	.L1199
	leaq	-280(%rbp), %rdi
	leaq	-360(%rbp), %rdx
	movl	$1, %ecx
	addq	$24, %r15
	movq	%rdi, -360(%rbp)
	call	_ZNSt10_HashtableISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS2_9SignatureINS2_4wasm9ValueTypeEEEESA_SaISA_ENSt8__detail9_IdentityESt8equal_toISA_ENS6_22WasmImportWrapperCache12CacheKeyHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKSA_NSC_10_AllocNodeISaINSC_10_Hash_nodeISA_Lb1EEEEEEEES0_INSC_14_Node_iteratorISA_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	cmpq	%r13, %r15
	jne	.L1200
	.p2align 4,,10
	.p2align 3
.L1223:
	movq	-384(%rbp), %r12
.L1194:
	leaq	-224(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal21CancelableTaskManagerC1Ev@PLT
	call	_ZN2v88internal4wasm21GetMaxBackgroundTasksEv@PLT
	movl	%eax, -396(%rbp)
	testl	%eax, %eax
	jle	.L1204
	leaq	-336(%rbp), %rax
	movl	$0, -376(%rbp)
	movq	%rax, -416(%rbp)
	leaq	-352(%rbp), %rax
	movq	%rax, -408(%rbp)
	.p2align 4,,10
	.p2align 3
.L1201:
	movq	(%rbx), %rax
	movl	$80, %edi
	movq	40960(%rax), %rcx
	movq	45752(%rax), %r8
	movq	%rcx, -392(%rbp)
	movq	%r8, -384(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal14CancelableTaskC2EPNS0_21CancelableTaskManagerE@PLT
	leaq	16+_ZTVN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskE(%rip), %rax
	movq	-384(%rbp), %r8
	movq	-392(%rbp), %rcx
	movq	%rax, 0(%r13)
	addq	$48, %rax
	movq	%rax, 32(%r13)
	movq	-408(%rbp), %rax
	movq	%r8, 40(%r13)
	movq	%rcx, 48(%r13)
	movq	%r14, 56(%r13)
	movq	%r12, 64(%r13)
	movq	%rax, 72(%r13)
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	leaq	32(%r13), %rdx
	movq	-416(%rbp), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	56(%rax), %rax
	movq	%rdx, -336(%rbp)
	call	*%rax
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1203
	movq	(%rdi), %rax
	call	*8(%rax)
	addl	$1, -376(%rbp)
	movl	-376(%rbp), %eax
	cmpl	%eax, -396(%rbp)
	jne	.L1201
.L1204:
	movq	%r15, -384(%rbp)
	leaq	-280(%rbp), %r13
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1224:
	movq	16(%rsi), %r8
	movq	%r13, %rdi
	movzbl	8(%rsi), %r15d
	movq	%r8, -376(%rbp)
	call	_ZNSt10_HashtableISt4pairIN2v88internal8compiler18WasmImportCallKindEPNS2_9SignatureINS2_4wasm9ValueTypeEEEESA_SaISA_ENSt8__detail9_IdentityESt8equal_toISA_ENS6_22WasmImportWrapperCache12CacheKeyHashENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb1ELb1EEEE5eraseENSC_20_Node_const_iteratorISA_Lb1ELb1EEE
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	(%rbx), %rax
	movl	%r15d, %ecx
	movq	%r14, %rsi
	movq	-376(%rbp), %r8
	leaq	-352(%rbp), %r9
	movq	40960(%rax), %rdx
	movq	45752(%rax), %rdi
	call	_ZN2v88internal4wasm20CompileImportWrapperEPNS1_10WasmEngineEPNS1_12NativeModuleEPNS0_8CountersENS0_8compiler18WasmImportCallKindEPNS0_9SignatureINS1_9ValueTypeEEEPNS1_22WasmImportWrapperCache17ModificationScopeE@PLT
.L1202:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-264(%rbp), %rsi
	testq	%rsi, %rsi
	jne	.L1224
	movq	-384(%rbp), %r15
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal21CancelableTaskManager13CancelAndWaitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal21CancelableTaskManagerD1Ev@PLT
	movq	-264(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1209
	.p2align 4,,10
	.p2align 3
.L1207:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1207
.L1209:
	movq	-272(%rbp), %rax
	movq	-280(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-280(%rbp), %rdi
	movq	$0, -256(%rbp)
	movq	$0, -264(%rbp)
	cmpq	-424(%rbp), %rdi
	je	.L1208
	call	_ZdlPv@PLT
.L1208:
	movq	%r12, %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	-344(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1225
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1203:
	.cfi_restore_state
	addl	$1, -376(%rbp)
	movl	-376(%rbp), %eax
	cmpl	%eax, -396(%rbp)
	jne	.L1201
	jmp	.L1204
.L1225:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18615:
	.size	_ZN2v88internal4wasm15InstanceBuilder21CompileImportWrappersENS0_6HandleINS0_18WasmInstanceObjectEEE, .-_ZN2v88internal4wasm15InstanceBuilder21CompileImportWrappersENS0_6HandleINS0_18WasmInstanceObjectEEE
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder14ProcessImportsENS0_6HandleINS0_18WasmInstanceObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC59:
	.string	"exception import requires a WebAssembly.Exception"
	.align 8
.LC60:
	.string	"imported exception does not match the expected type"
	.section	.text._ZN2v88internal4wasm15InstanceBuilder14ProcessImportsENS0_6HandleINS0_18WasmInstanceObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15InstanceBuilder14ProcessImportsENS0_6HandleINS0_18WasmInstanceObjectEEE
	.type	_ZN2v88internal4wasm15InstanceBuilder14ProcessImportsENS0_6HandleINS0_18WasmInstanceObjectEEE, @function
_ZN2v88internal4wasm15InstanceBuilder14ProcessImportsENS0_6HandleINS0_18WasmInstanceObjectEEE:
.LFB18662:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm15InstanceBuilder21CompileImportWrappersENS0_6HandleINS0_18WasmInstanceObjectEEE
	movq	24(%r12), %rsi
	movabsq	$-6148914691236517205, %rdx
	movq	208(%rsi), %rcx
	movq	216(%rsi), %rax
	subq	%rcx, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	testl	%eax, %eax
	jle	.L1254
	subl	$1, %eax
	xorl	%ebx, %ebx
	leaq	.L1230(%rip), %r14
	xorl	%r15d, %r15d
	movq	%rax, -104(%rbp)
	movq	%r12, %rax
	movq	%rbx, %r12
	movl	%r15d, %ebx
	movq	%rax, %r15
.L1251:
	leaq	(%r12,%r12,2), %rax
	movl	%r12d, %edx
	salq	$3, %rax
	leaq	(%rcx,%rax), %r11
	addq	112(%r15), %rax
	cmpb	$4, 16(%r11)
	movq	(%rax), %rcx
	movq	8(%rax), %r8
	movq	16(%rax), %r9
	ja	.L1228
	movzbl	16(%r11), %eax
	movslq	(%r14,%rax,4), %rax
	addq	%r14, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder14ProcessImportsENS0_6HandleINS0_18WasmInstanceObjectEEE,"a",@progbits
	.align 4
	.align 4
.L1230:
	.long	.L1234-.L1230
	.long	.L1233-.L1230
	.long	.L1232-.L1230
	.long	.L1231-.L1230
	.long	.L1229-.L1230
	.section	.text._ZN2v88internal4wasm15InstanceBuilder14ProcessImportsENS0_6HandleINS0_18WasmInstanceObjectEEE
	.p2align 4,,10
	.p2align 3
.L1231:
	subq	$8, %rsp
	movl	20(%r11), %r11d
	movq	%r13, %rsi
	movq	%r15, %rdi
	pushq	%r9
	movq	%r8, %r9
	movq	%rcx, %r8
	movl	%r11d, %ecx
	call	_ZN2v88internal4wasm15InstanceBuilder21ProcessImportedGlobalENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE
	popq	%rdx
	popq	%rcx
	testb	%al, %al
	je	.L1250
.L1236:
	leaq	1(%r12), %rax
	cmpq	%r12, -104(%rbp)
	je	.L1274
	movq	24(%r15), %rsi
	movq	%rax, %r12
	movq	208(%rsi), %rcx
	jmp	.L1251
	.p2align 4,,10
	.p2align 3
.L1232:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm15InstanceBuilder21ProcessImportedMemoryENS0_6HandleINS0_18WasmInstanceObjectEEEiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE
	testb	%al, %al
	jne	.L1236
	.p2align 4,,10
	.p2align 3
.L1250:
	movl	$-1, %r15d
.L1226:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1281
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1234:
	.cfi_restore_state
	subq	$8, %rsp
	movl	20(%r11), %r11d
	movq	%r13, %rsi
	movq	%r15, %rdi
	pushq	%r9
	movq	%r8, %r9
	movq	%rcx, %r8
	movl	%r11d, %ecx
	call	_ZN2v88internal4wasm15InstanceBuilder23ProcessImportedFunctionENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE
	popq	%r8
	popq	%r9
	testb	%al, %al
	je	.L1250
	addl	$1, %ebx
	jmp	.L1236
	.p2align 4,,10
	.p2align 3
.L1229:
	movq	(%r9), %rax
	testb	$1, %al
	jne	.L1237
.L1240:
	movq	(%r8), %rax
	movq	%rcx, -104(%rbp)
	leaq	-64(%rbp), %rdi
	leaq	-72(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rbx
	movq	%rax, -72(%rbp)
	movq	32(%r15), %r12
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-104(%rbp), %r11
	leaq	-80(%rbp), %rdi
	xorl	%r8d, %r8d
	leaq	-88(%rbp), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movq	-64(%rbp), %r13
	movq	(%r11), %rax
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	leaq	.LC59(%rip), %r9
.L1280:
	movq	-80(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r13, %r8
	movl	%ebx, %edx
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1245
	call	_ZdaPv@PLT
.L1245:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1250
	call	_ZdaPv@PLT
	jmp	.L1250
	.p2align 4,,10
	.p2align 3
.L1233:
	subq	$8, %rsp
	movl	20(%r11), %r11d
	movq	%r13, %rsi
	movq	%r15, %rdi
	pushq	%r9
	movq	%r8, %r9
	movq	%rcx, %r8
	movl	%r11d, %ecx
	call	_ZN2v88internal4wasm15InstanceBuilder20ProcessImportedTableENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS3_INS0_6StringEEES7_NS3_INS0_6ObjectEEE
	popq	%rsi
	popq	%rdi
	testb	%al, %al
	jne	.L1236
	jmp	.L1250
	.p2align 4,,10
	.p2align 3
.L1237:
	movq	-1(%rax), %rdx
	cmpw	$1098, 11(%rdx)
	jne	.L1240
	movq	%rax, -64(%rbp)
	movl	20(%r11), %edx
	leaq	-64(%rbp), %rdi
	movq	256(%rsi), %rax
	movq	%r8, -144(%rbp)
	movq	%rcx, -136(%rbp)
	movq	(%rax,%rdx,8), %rsi
	movq	%r9, -128(%rbp)
	movq	%r11, -120(%rbp)
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal19WasmExceptionObject16IsSignatureEqualEPKNS0_9SignatureINS0_4wasm9ValueTypeEEE@PLT
	movq	-112(%rbp), %rdi
	movq	-120(%rbp), %r11
	testb	%al, %al
	movq	-128(%rbp), %r9
	movq	-136(%rbp), %rcx
	movq	-144(%rbp), %r8
	je	.L1282
	movq	(%r9), %rax
	movq	31(%rax), %rdx
	movq	0(%r13), %rax
	movq	223(%rax), %rdi
	movl	20(%r11), %eax
	leal	16(,%rax,8), %eax
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L1252
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rcx
	movq	%rax, -112(%rbp)
	testl	$262144, %ecx
	jne	.L1283
.L1248:
	andl	$24, %ecx
	je	.L1252
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1252
	movq	%r9, -120(%rbp)
	movq	%r11, -112(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-120(%rbp), %r9
	movq	-112(%rbp), %r11
	.p2align 4,,10
	.p2align 3
.L1252:
	movl	20(%r11), %edx
	movq	80(%r15), %rax
	movq	%r9, (%rax,%rdx,8)
	jmp	.L1236
	.p2align 4,,10
	.p2align 3
.L1274:
	movl	%ebx, %r15d
	jmp	.L1226
.L1283:
	movq	%r9, -152(%rbp)
	movq	%r11, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdi, -120(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-112(%rbp), %rax
	movq	-128(%rbp), %rsi
	movq	-152(%rbp), %r9
	movq	-144(%rbp), %r11
	movq	8(%rax), %rcx
	movq	-136(%rbp), %rdx
	movq	-120(%rbp), %rdi
	jmp	.L1248
.L1254:
	xorl	%r15d, %r15d
	jmp	.L1226
.L1228:
	leaq	.LC13(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1282:
	movq	(%r8), %rax
	movq	%rcx, -104(%rbp)
	leaq	-72(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rbx
	movq	32(%r15), %r12
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-104(%rbp), %r11
	leaq	-80(%rbp), %rdi
	xorl	%r8d, %r8d
	leaq	-88(%rbp), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movq	-64(%rbp), %r13
	movq	(%r11), %rax
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	leaq	.LC60(%rip), %r9
	jmp	.L1280
.L1281:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18662:
	.size	_ZN2v88internal4wasm15InstanceBuilder14ProcessImportsENS0_6HandleINS0_18WasmInstanceObjectEEE, .-_ZN2v88internal4wasm15InstanceBuilder14ProcessImportsENS0_6HandleINS0_18WasmInstanceObjectEEE
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder5BuildEv.str1.1,"aMS",@progbits,1
.LC61:
	.string	"InstanceBuilder::Build"
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder5BuildEv.str1.8,"aMS",@progbits,1
	.align 8
.LC62:
	.string	"Imports argument must be present and must be an object"
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder5BuildEv.str1.1
.LC63:
	.string	"Out of memory: wasm globals"
	.section	.rodata._ZN2v88internal4wasm15InstanceBuilder5BuildEv.str1.8
	.align 8
.LC64:
	.string	"instance->memory_size() == memory->byte_length()"
	.align 8
.LC65:
	.string	"instance->memory_start() == memory->backing_store()"
	.section	.text._ZN2v88internal4wasm15InstanceBuilder5BuildEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15InstanceBuilder5BuildEv
	.type	_ZN2v88internal4wasm15InstanceBuilder5BuildEv, @function
_ZN2v88internal4wasm15InstanceBuilder5BuildEv:
.LFB18585:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	_ZZN2v88internal4wasm15InstanceBuilder5BuildEvE28trace_event_unique_atomic272(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1486
	movq	$0, -112(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1487
.L1288:
	movq	24(%r15), %rax
	movq	208(%rax), %rbx
	cmpq	%rbx, 216(%rax)
	je	.L1292
	cmpq	$0, 48(%r15)
	je	.L1488
.L1292:
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	_ZN2v88internal4wasm15InstanceBuilder15SanitizeImportsEv
	movq	32(%r15), %rax
	movl	16(%rax), %r11d
	testl	%r11d, %r11d
	je	.L1489
.L1293:
	leaq	-112(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1490
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1486:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1491
.L1287:
	movq	%rbx, _ZZN2v88internal4wasm15InstanceBuilder5BuildEvE28trace_event_unique_atomic272(%rip)
	movq	$0, -112(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	je	.L1288
.L1487:
	pxor	%xmm0, %xmm0
	xorl	%r12d, %r12d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1492
.L1289:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1290
	movq	(%rdi), %rax
	call	*8(%rax)
.L1290:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1291
	movq	(%rdi), %rax
	call	*8(%rax)
.L1291:
	leaq	.LC61(%rip), %rax
	movq	%rbx, -104(%rbp)
	movq	%rax, -96(%rbp)
	leaq	-104(%rbp), %rax
	movq	%r12, -88(%rbp)
	movq	%rax, -112(%rbp)
	jmp	.L1288
	.p2align 4,,10
	.p2align 3
.L1489:
	leaq	-160(%rbp), %rax
	movq	(%r15), %rsi
	movq	%rax, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EEC1EPNS0_7IsolateE@PLT
	movq	24(%r15), %rax
	cmpb	$0, 392(%rax)
	movq	(%r15), %rax
	movq	40960(%rax), %rdi
	je	.L1493
	addq	$4872, %rdi
.L1296:
	leaq	-144(%rbp), %rax
	xorl	%edx, %edx
	movq	%rdi, -136(%rbp)
	movq	%rax, %rsi
	movq	%rax, -216(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -128(%rbp)
	call	_ZN2v88internal14TimedHistogram5StartEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	movq	40(%r15), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %r12
	movq	24(%r15), %rax
	movl	8(%rax), %r13d
	cmpb	$0, 392(%rax)
	movq	(%r15), %rax
	movq	40960(%rax), %rdi
	jne	.L1297
	addq	$1368, %rdi
.L1298:
	movl	%r13d, %esi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movq	24(%r15), %rax
	cmpb	$0, 17(%rax)
	jne	.L1494
.L1299:
	movq	56(%r15), %rax
	testq	%rax, %rax
	je	.L1495
.L1300:
	movq	(%rax), %rdx
	movl	39(%rdx), %eax
	andl	$-3, %eax
	movl	%eax, 39(%rdx)
.L1395:
	movq	40(%r15), %rsi
	movq	(%r15), %rdi
	call	_ZN2v88internal18WasmInstanceObject3NewEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE@PLT
	movq	%r12, %rsi
	movq	%rax, %rbx
	leaq	-176(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal4wasm29NativeModuleModificationScopeC1EPNS1_12NativeModuleE@PLT
	movq	24(%r15), %rax
	movl	48(%rax), %r8d
	testl	%r8d, %r8d
	je	.L1304
	movq	(%r15), %rax
	movl	%r8d, %r12d
	movq	%r12, %rsi
	movq	45544(%rax), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1496
	movq	(%r15), %rdi
	movl	$1, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory16NewJSArrayBufferENS0_10SharedFlagENS0_14AllocationTypeE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movq	%r12, %r8
	movq	%rax, 64(%r15)
	movq	%rax, %rdi
	movq	%r13, %rcx
	xorl	%edx, %edx
	pushq	$0
	movq	(%r15), %rsi
	call	_ZN2v88internal13JSArrayBuffer5SetupENS0_6HandleIS1_EEPNS0_7IsolateEbPvmNS0_10SharedFlagEb@PLT
	movq	64(%r15), %rax
	popq	%r9
	popq	%r10
	testq	%rax, %rax
	je	.L1497
	movq	(%rax), %rax
	movq	(%rbx), %rdx
	movq	31(%rax), %rax
	movq	%rax, 103(%rdx)
	movq	64(%r15), %rax
	movq	(%rbx), %r13
	movq	(%rax), %r12
	leaq	167(%r13), %rsi
	movq	%r12, 167(%r13)
	testb	$1, %r12b
	je	.L1394
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	je	.L1309
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	movq	-184(%rbp), %rsi
.L1309:
	testb	$24, %al
	je	.L1394
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1394
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1394:
	movq	24(%r15), %rax
.L1304:
	movl	52(%rax), %esi
	movq	(%r15), %rdi
	testl	%esi, %esi
	jne	.L1498
.L1311:
	movl	56(%rax), %esi
	testl	%esi, %esi
	jne	.L1499
.L1315:
	movq	264(%rax), %rdx
	subq	256(%rax), %rdx
	sarq	$3, %rdx
	movq	%rdx, -240(%rbp)
	movq	%rdx, %rcx
	testl	%edx, %edx
	jg	.L1500
.L1319:
	movq	192(%rax), %r13
	subq	184(%rax), %r13
	xorl	%edx, %edx
	sarq	$4, %r13
	movl	%r13d, %esi
	movq	%r13, -232(%rbp)
	movq	%r13, %r14
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	movq	24(%r15), %rax
	movslq	64(%rax), %rdx
	cmpl	%edx, %r13d
	jle	.L1325
	movl	%edx, %ecx
	leal	16(,%rdx,8), %r11d
	movq	%rbx, -248(%rbp)
	notl	%ecx
	movslq	%r11d, %r13
	addl	%r14d, %ecx
	leaq	3(%rcx,%rdx), %r14
	salq	$3, %r14
	jmp	.L1329
	.p2align 4,,10
	.p2align 3
.L1501:
	movq	24(%r15), %rax
.L1329:
	movq	184(%rax), %rax
	movq	(%r15), %rdi
	xorl	%r9d, %r9d
	leaq	-32(%rax,%r13,2), %rax
	movl	4(%rax), %edx
	movzbl	(%rax), %esi
	movzbl	12(%rax), %ecx
	movl	8(%rax), %r8d
	call	_ZN2v88internal15WasmTableObject3NewEPNS0_7IsolateENS0_4wasm9ValueTypeEjbjPNS0_6HandleINS0_10FixedArrayEEE@PLT
	movq	(%r12), %rbx
	movq	(%rax), %rdx
	leaq	-1(%rbx,%r13), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L1390
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -184(%rbp)
	testl	$262144, %eax
	je	.L1327
	movq	%rbx, %rdi
	movq	%rdx, -200(%rbp)
	movq	%rsi, -192(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-184(%rbp), %rcx
	movq	-200(%rbp), %rdx
	movq	-192(%rbp), %rsi
	movq	8(%rcx), %rax
.L1327:
	testb	$24, %al
	je	.L1390
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1390
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1390:
	addq	$8, %r13
	cmpq	%r13, %r14
	jne	.L1501
	movq	-248(%rbp), %rbx
.L1325:
	movq	(%rbx), %r14
	movq	(%r12), %r12
	movq	%r12, 199(%r14)
	leaq	199(%r14), %rsi
	testb	$1, %r12b
	je	.L1389
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -184(%rbp)
	testl	$262144, %eax
	je	.L1331
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rsi, -192(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-184(%rbp), %rcx
	movq	-192(%rbp), %rsi
	movq	8(%rcx), %rax
.L1331:
	testb	$24, %al
	je	.L1389
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1502
	.p2align 4,,10
	.p2align 3
.L1389:
	movq	-232(%rbp), %r14
	movq	(%r15), %rdi
	xorl	%edx, %edx
	movl	%r14d, %esi
	leal	-2(%r14), %r12d
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, -184(%rbp)
	movq	%r14, %rax
	xorl	%r14d, %r14d
	cmpl	$1, %eax
	jg	.L1341
	jmp	.L1340
	.p2align 4,,10
	.p2align 3
.L1336:
	cmpq	%r14, %r12
	je	.L1340
	movq	%r13, %r14
.L1341:
	movq	24(%r15), %rdx
	leaq	1(%r14), %r13
	movq	%r13, %rax
	salq	$4, %rax
	addq	184(%rdx), %rax
	cmpb	$7, (%rax)
	jne	.L1336
	movl	4(%rax), %esi
	movq	(%r15), %rdi
	call	_ZN2v88internal25WasmIndirectFunctionTable3NewEPNS0_7IsolateEj@PLT
	movq	-184(%rbp), %rcx
	movq	(%rax), %rdx
	leal	24(,%r14,8), %eax
	movq	(%rcx), %rdi
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L1336
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -192(%rbp)
	testl	$262144, %eax
	je	.L1338
	movq	%rdx, -256(%rbp)
	movq	%rsi, -248(%rbp)
	movq	%rdi, -200(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-192(%rbp), %rcx
	movq	-256(%rbp), %rdx
	movq	-248(%rbp), %rsi
	movq	-200(%rbp), %rdi
	movq	8(%rcx), %rax
.L1338:
	testb	$24, %al
	je	.L1336
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1336
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1336
	.p2align 4,,10
	.p2align 3
.L1493:
	addq	$4824, %rdi
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1340:
	movq	-184(%rbp), %rax
	movq	(%rbx), %r14
	movq	(%rax), %r12
	leaq	207(%r14), %rsi
	movq	%r12, 207(%r14)
	testb	$1, %r12b
	je	.L1387
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -184(%rbp)
	testl	$262144, %eax
	je	.L1342
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rsi, -192(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-184(%rbp), %rcx
	movq	-192(%rbp), %rsi
	movq	8(%rcx), %rax
.L1342:
	testb	$24, %al
	je	.L1387
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1503
	.p2align 4,,10
	.p2align 3
.L1387:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm15InstanceBuilder14ProcessImportsENS0_6HandleINS0_18WasmInstanceObjectEEE
	testl	%eax, %eax
	jns	.L1344
.L1485:
	xorl	%r12d, %r12d
.L1306:
	movq	-224(%rbp), %rdi
	call	_ZN2v88internal4wasm29NativeModuleModificationScopeD1Ev@PLT
.L1303:
	movq	-136(%rbp), %rdi
	movq	-128(%rbp), %rdx
	movq	-216(%rbp), %rsi
	call	_ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	movq	-208(%rbp), %rdi
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED1Ev@PLT
	jmp	.L1293
	.p2align 4,,10
	.p2align 3
.L1488:
	movq	32(%r15), %rdi
	leaq	.LC62(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	call	_ZN2v88internal4wasm12ErrorThrower9TypeErrorEPKcz@PLT
	jmp	.L1293
	.p2align 4,,10
	.p2align 3
.L1297:
	addq	$1328, %rdi
	jmp	.L1298
	.p2align 4,,10
	.p2align 3
.L1495:
	movq	%r15, %rdi
	call	_ZNK2v88internal4wasm15InstanceBuilder24FindImportedMemoryBufferEv
	movq	%rax, 56(%r15)
	testq	%rax, %rax
	jne	.L1300
	testl	%r13d, %r13d
	jne	.L1301
	cmpb	$0, 676(%r12)
	je	.L1395
.L1301:
	movq	24(%r15), %rax
	movl	%r13d, %esi
	movq	%r15, %rdi
	movl	12(%rax), %edx
	call	_ZN2v88internal4wasm15InstanceBuilder14AllocateMemoryEjj
	movq	%rax, 56(%r15)
	testq	%rax, %rax
	jne	.L1395
	xorl	%r12d, %r12d
	jmp	.L1303
	.p2align 4,,10
	.p2align 3
.L1494:
	movl	12(%rax), %esi
	movq	(%r15), %rax
	movq	40960(%rax), %rdi
	addq	$1408, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	jmp	.L1299
	.p2align 4,,10
	.p2align 3
.L1500:
	movl	%ecx, %esi
	movl	$1, %edx
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%rbx), %r13
	movq	(%rax), %r12
	leaq	223(%r13), %rsi
	movq	%r12, 223(%r13)
	testb	$1, %r12b
	je	.L1391
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	jne	.L1504
.L1321:
	testb	$24, %al
	je	.L1391
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1391
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1391:
	movq	88(%r15), %rdx
	movq	80(%r15), %rcx
	movslq	-240(%rbp), %rsi
	movq	%rdx, %rax
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rsi
	ja	.L1505
	jnb	.L1324
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdx
	je	.L1324
	movq	%rax, 88(%r15)
.L1324:
	movq	24(%r15), %rax
	movq	(%r15), %rdi
	jmp	.L1319
	.p2align 4,,10
	.p2align 3
.L1344:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm15InstanceBuilder11InitGlobalsENS0_6HandleINS0_18WasmInstanceObjectEEE
	movl	-232(%rbp), %r8d
	testl	%r8d, %r8d
	jg	.L1506
.L1345:
	movl	-240(%rbp), %edi
	testl	%edi, %edi
	jg	.L1507
.L1350:
	movq	24(%r15), %rax
	cmpb	$0, 18(%rax)
	jne	.L1508
.L1351:
	cmpb	$0, 17(%r15)
	jne	.L1374
	movq	24(%r15), %rax
	movq	280(%rax), %rdx
	movq	288(%rax), %rcx
	cmpq	%rcx, %rdx
	je	.L1364
	leaq	-168(%rbp), %rax
	movq	%r15, %r13
	movq	%rdx, %r14
	movq	%rcx, %r12
	movq	%rax, -184(%rbp)
	movq	%rbx, %r15
.L1373:
	cmpb	$0, 48(%r14)
	je	.L1365
	movl	8(%r14), %eax
	cmpl	$1, %eax
	jne	.L1509
	movq	(%r15), %rax
	movq	-184(%rbp), %rdi
	movl	16(%r14), %ebx
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal18WasmInstanceObject6moduleEv@PLT
	movq	(%r15), %rsi
	salq	$5, %rbx
	addq	24(%rax), %rbx
	movq	167(%rsi), %rcx
	movl	24(%rbx), %eax
	movq	31(%rcx), %rcx
	movl	(%rcx,%rax), %ebx
.L1368:
	movl	(%r14), %eax
	movq	0(%r13), %rcx
	movq	199(%rsi), %rsi
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rsi), %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L1369
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1370:
	movq	23(%rsi), %rax
	movslq	11(%rax), %rcx
	movq	32(%r14), %rax
	subq	24(%r14), %rax
	sarq	$2, %rax
	cmpq	%rax, %rcx
	jb	.L1372
	subq	%rax, %rcx
	cmpq	%rcx, %rbx
	jbe	.L1365
.L1372:
	movq	32(%r13), %rdi
	leaq	.LC49(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	jmp	.L1485
	.p2align 4,,10
	.p2align 3
.L1492:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC61(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r12
	addq	$64, %rsp
	jmp	.L1289
	.p2align 4,,10
	.p2align 3
.L1491:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1498:
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, 72(%r15)
	movq	(%rbx), %r13
	movq	(%rax), %r12
	leaq	175(%r13), %rsi
	movq	%r12, 175(%r13)
	testb	$1, %r12b
	je	.L1393
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	je	.L1313
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	movq	-184(%rbp), %rsi
.L1313:
	testb	$24, %al
	je	.L1393
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1393
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1393:
	movq	24(%r15), %rax
	movq	(%r15), %rdi
	jmp	.L1311
	.p2align 4,,10
	.p2align 3
.L1499:
	movl	$1, %edx
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%rbx), %r13
	movq	(%rax), %r12
	leaq	183(%r13), %rsi
	movq	%r12, 183(%r13)
	testb	$1, %r12b
	je	.L1392
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	je	.L1317
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	movq	-184(%rbp), %rsi
.L1317:
	testb	$24, %al
	je	.L1392
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1392
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1392:
	movq	24(%r15), %rax
	movq	(%r15), %rdi
	jmp	.L1315
	.p2align 4,,10
	.p2align 3
.L1506:
	movq	24(%r15), %rax
	xorl	%r12d, %r12d
	movq	192(%rax), %rcx
	movq	184(%rax), %rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$4, %rax
	testl	%eax, %eax
	jg	.L1346
	jmp	.L1345
	.p2align 4,,10
	.p2align 3
.L1347:
	movq	%rcx, %rax
	addq	$1, %r12
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpl	%r12d, %eax
	jle	.L1345
.L1346:
	movq	%r12, %rax
	salq	$4, %rax
	addq	%rdx, %rax
	cmpb	$7, (%rax)
	jne	.L1347
	movl	4(%rax), %edx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal18WasmInstanceObject42EnsureIndirectFunctionTableWithMinimumSizeENS0_6HandleIS1_EEij@PLT
	movq	24(%r15), %rax
	movq	184(%rax), %rdx
	movq	192(%rax), %rcx
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1507:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm15InstanceBuilder20InitializeExceptionsENS0_6HandleINS0_18WasmInstanceObjectEEE
	jmp	.L1350
	.p2align 4,,10
	.p2align 3
.L1502:
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1389
	.p2align 4,,10
	.p2align 3
.L1503:
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1387
	.p2align 4,,10
	.p2align 3
.L1504:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	movq	-184(%rbp), %rsi
	jmp	.L1321
.L1375:
	addq	$32, %r14
	cmpq	%r14, %rdx
	jne	.L1379
	.p2align 4,,10
	.p2align 3
.L1374:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm15InstanceBuilder14ProcessExportsENS0_6HandleINS0_18WasmInstanceObjectEEE
	movq	32(%r15), %rax
	movl	16(%rax), %esi
	testl	%esi, %esi
	jne	.L1485
	movl	-232(%rbp), %ecx
	testl	%ecx, %ecx
	jg	.L1510
.L1380:
	movq	24(%r15), %rax
	movq	168(%rax), %rcx
	cmpq	%rcx, 160(%rax)
	je	.L1381
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm15InstanceBuilder16LoadDataSegmentsENS0_6HandleINS0_18WasmInstanceObjectEEE
	movq	32(%r15), %rax
	movl	16(%rax), %eax
	testl	%eax, %eax
	jne	.L1485
.L1381:
	movq	40(%r15), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal16WasmModuleObject27SetBreakpointsOnNewInstanceENS0_6HandleIS1_EENS2_INS0_18WasmInstanceObjectEEE@PLT
	movq	24(%r15), %rax
	movl	20(%rax), %r12d
	testl	%r12d, %r12d
	js	.L1382
	movslq	%r12d, %r13
	movq	(%r15), %rdi
	salq	$5, %r13
	addq	136(%rax), %r13
	movzbl	24(%r13), %edx
	movq	0(%r13), %rsi
	call	_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnit22CompileJSToWasmWrapperEPNS0_7IsolateEPNS0_9SignatureINS1_9ValueTypeEEEb@PLT
	movq	(%r15), %rdi
	movl	%r12d, %edx
	movq	%rbx, %rsi
	movq	%rax, %r8
	movq	0(%r13), %rax
	movq	8(%rax), %rcx
	call	_ZN2v88internal20WasmExportedFunction3NewEPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS4_INS0_4CodeEEE@PLT
	movq	%rax, 104(%r15)
.L1382:
	movq	%rbx, %r12
	jmp	.L1306
	.p2align 4,,10
	.p2align 3
.L1365:
	addq	$56, %r14
	cmpq	%r14, %r12
	jne	.L1373
	movq	24(%r13), %rax
	movq	%r15, %rbx
	movq	%r13, %r15
.L1364:
	movq	160(%rax), %r14
	movq	168(%rax), %rdx
	cmpq	%rdx, %r14
	je	.L1374
	leaq	-168(%rbp), %r12
.L1379:
	cmpb	$0, 24(%r14)
	je	.L1375
	movl	(%r14), %eax
	cmpl	$1, %eax
	jne	.L1511
	movq	(%rbx), %rax
	movl	8(%r14), %r13d
	movq	%r12, %rdi
	movq	%rdx, -184(%rbp)
	movq	%rax, -168(%rbp)
	salq	$5, %r13
	call	_ZN2v88internal18WasmInstanceObject6moduleEv@PLT
	movq	-184(%rbp), %rdx
	addq	24(%rax), %r13
	movq	(%rbx), %rax
	movl	24(%r13), %ecx
	movq	167(%rax), %rsi
	movq	31(%rsi), %rsi
	movl	(%rsi,%rcx), %esi
.L1377:
	movq	31(%rax), %rax
	movl	20(%r14), %ecx
	cmpq	%rax, %rcx
	ja	.L1378
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	jbe	.L1375
.L1378:
	movq	32(%r15), %rdi
	leaq	.LC14(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	call	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz@PLT
	jmp	.L1306
	.p2align 4,,10
	.p2align 3
.L1509:
	cmpl	$2, %eax
	jne	.L1367
	movl	16(%r14), %ebx
	movq	(%r15), %rsi
	jmp	.L1368
	.p2align 4,,10
	.p2align 3
.L1496:
	movq	32(%r15), %rdi
	leaq	.LC63(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	call	_ZN2v88internal4wasm12ErrorThrower10RangeErrorEPKcz@PLT
	jmp	.L1306
	.p2align 4,,10
	.p2align 3
.L1369:
	movq	41088(%rcx), %rax
	cmpq	41096(%rcx), %rax
	je	.L1512
.L1371:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rcx)
	movq	%rsi, (%rax)
	jmp	.L1370
	.p2align 4,,10
	.p2align 3
.L1497:
	movq	32(%r15), %rdi
	leaq	.LC63(%rip), %rsi
	xorl	%r12d, %r12d
	call	_ZN2v88internal4wasm12ErrorThrower10RangeErrorEPKcz@PLT
	jmp	.L1306
	.p2align 4,,10
	.p2align 3
.L1505:
	subq	%rax, %rsi
	leaq	80(%r15), %rdi
	call	_ZNSt6vectorIN2v88internal6HandleINS1_19WasmExceptionObjectEEESaIS4_EE17_M_default_appendEm
	jmp	.L1324
	.p2align 4,,10
	.p2align 3
.L1508:
	movq	(%rbx), %rdx
	movq	159(%rdx), %r12
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %r12
	je	.L1513
.L1352:
	movq	(%r15), %r14
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1357
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1358:
	movq	(%r15), %rdi
	movq	%rbx, %rdx
	call	_ZN2v88internal16WasmMemoryObject11AddInstanceEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_18WasmInstanceObjectEEE@PLT
	movq	56(%r15), %rax
	testq	%rax, %rax
	je	.L1351
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	31(%rax), %rcx
	cmpq	%rcx, 23(%rdx)
	jne	.L1514
	movq	31(%rdx), %rcx
	cmpq	%rcx, 23(%rax)
	je	.L1351
	leaq	.LC65(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1357:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L1515
.L1359:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r12, (%rsi)
	jmp	.L1358
.L1513:
	movl	12(%rax), %edx
	movl	$-1, %eax
	movq	56(%r15), %rsi
	movq	(%r15), %rdi
	testl	%edx, %edx
	cmove	%eax, %edx
	call	_ZN2v88internal16WasmMemoryObject3NewEPNS0_7IsolateENS0_11MaybeHandleINS0_13JSArrayBufferEEEj@PLT
	movq	(%rbx), %r14
	movq	(%rax), %r12
	leaq	159(%r14), %rsi
	movq	%r12, 159(%r14)
	testb	$1, %r12b
	je	.L1386
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -184(%rbp)
	testl	$262144, %eax
	je	.L1355
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rsi, -192(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-184(%rbp), %rcx
	movq	-192(%rbp), %rsi
	movq	8(%rcx), %rax
.L1355:
	testb	$24, %al
	je	.L1386
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1386
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L1386:
	movq	(%rbx), %rax
	movq	159(%rax), %r12
	jmp	.L1352
.L1511:
	cmpl	$2, %eax
	jne	.L1367
	movl	8(%r14), %esi
	movq	(%rbx), %rax
	jmp	.L1377
.L1510:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm15InstanceBuilder17LoadTableSegmentsENS0_6HandleINS0_18WasmInstanceObjectEEE
	movq	32(%r15), %rax
	movl	16(%rax), %edx
	testl	%edx, %edx
	jne	.L1485
	jmp	.L1380
.L1512:
	movq	%rcx, %rdi
	movq	%rsi, -200(%rbp)
	movq	%rcx, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	movq	-192(%rbp), %rcx
	jmp	.L1371
.L1515:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1359
.L1514:
	leaq	.LC64(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1490:
	call	__stack_chk_fail@PLT
.L1367:
	leaq	.LC13(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18585:
	.size	_ZN2v88internal4wasm15InstanceBuilder5BuildEv, .-_ZN2v88internal4wasm15InstanceBuilder5BuildEv
	.section	.text._ZN2v88internal4wasm27InstantiateToInstanceObjectEPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENS9_INS0_13JSArrayBufferEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm27InstantiateToInstanceObjectEPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENS9_INS0_13JSArrayBufferEEE
	.type	_ZN2v88internal4wasm27InstantiateToInstanceObjectEPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENS9_INS0_13JSArrayBufferEEE, @function
_ZN2v88internal4wasm27InstantiateToInstanceObjectEPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENS9_INS0_13JSArrayBufferEEE:
.LFB18557:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r9
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$144, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdx), %rcx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -160(%rbp)
	movq	23(%rcx), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	192(%rax), %rdi
	movq	%rdi, -152(%rbp)
	movl	200(%rax), %edi
	movzbl	204(%rax), %eax
	movl	%edi, -144(%rbp)
	movb	%al, -140(%rbp)
	movq	23(%rcx), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	%rdx, -120(%rbp)
	movabsq	$-6148914691236517205, %rdx
	movq	%rsi, -128(%rbp)
	movq	208(%rax), %rax
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	%rax, -136(%rbp)
	movq	$0, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movups	%xmm0, -56(%rbp)
	movups	%xmm0, -40(%rbp)
	movq	216(%rax), %rbx
	subq	208(%rax), %rbx
	movq	%rbx, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$384307168202282325, %rdx
	cmpq	%rdx, %rax
	ja	.L1547
	testq	%rax, %rax
	jne	.L1548
.L1518:
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15InstanceBuilder5BuildEv
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1522
	movq	%r12, %rdi
	movq	%rbx, %r12
	call	_ZN2v88internal4wasm15InstanceBuilder20ExecuteStartFunctionEv
	testb	%al, %al
	je	.L1522
.L1523:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1524
	call	_ZdlPv@PLT
.L1524:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1525
	call	_ZdlPv@PLT
.L1525:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1549
	addq	$144, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1522:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L1523
	.p2align 4,,10
	.p2align 3
.L1548:
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	-40(%rbp), %rsi
	movq	-48(%rbp), %rdi
	movq	%rax, %r12
	cmpq	%rdi, %rsi
	je	.L1519
	movq	%rax, %rcx
	movq	%rdi, %rdx
	.p2align 4,,10
	.p2align 3
.L1520:
	movdqu	(%rdx), %xmm1
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm1, -24(%rcx)
	movq	-8(%rdx), %rax
	movq	%rax, -8(%rcx)
	cmpq	%rdx, %rsi
	jne	.L1520
.L1519:
	testq	%rdi, %rdi
	je	.L1521
	call	_ZdlPv@PLT
.L1521:
	movq	%r12, %xmm0
	addq	%r12, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -32(%rbp)
	movaps	%xmm0, -48(%rbp)
	jmp	.L1518
.L1549:
	call	__stack_chk_fail@PLT
.L1547:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE18557:
	.size	_ZN2v88internal4wasm27InstantiateToInstanceObjectEPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENS9_INS0_13JSArrayBufferEEE, .-_ZN2v88internal4wasm27InstantiateToInstanceObjectEPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENS9_INS0_13JSArrayBufferEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal4wasm27InstantiateToInstanceObjectEPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENS9_INS0_13JSArrayBufferEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal4wasm27InstantiateToInstanceObjectEPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENS9_INS0_13JSArrayBufferEEE, @function
_GLOBAL__sub_I__ZN2v88internal4wasm27InstantiateToInstanceObjectEPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENS9_INS0_13JSArrayBufferEEE:
.LFB23214:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23214:
	.size	_GLOBAL__sub_I__ZN2v88internal4wasm27InstantiateToInstanceObjectEPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENS9_INS0_13JSArrayBufferEEE, .-_GLOBAL__sub_I__ZN2v88internal4wasm27InstantiateToInstanceObjectEPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENS9_INS0_13JSArrayBufferEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal4wasm27InstantiateToInstanceObjectEPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENS9_INS0_13JSArrayBufferEEE
	.weak	_ZTVN2v88internal14CancelableTaskE
	.section	.data.rel.ro._ZTVN2v88internal14CancelableTaskE,"awG",@progbits,_ZTVN2v88internal14CancelableTaskE,comdat
	.align 8
	.type	_ZTVN2v88internal14CancelableTaskE, @object
	.size	_ZTVN2v88internal14CancelableTaskE, 88
_ZTVN2v88internal14CancelableTaskE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	__cxa_pure_virtual
	.quad	-32
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskE,"aw"
	.align 8
	.type	_ZTVN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskE, @object
	.size	_ZTVN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskE, 88
_ZTVN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskD1Ev
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskD0Ev
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTask11RunInternalEv
	.quad	-32
	.quad	0
	.quad	_ZThn32_N2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskD1Ev
	.quad	_ZThn32_N2v88internal4wasm12_GLOBAL__N_124CompileImportWrapperTaskD0Ev
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.section	.bss._ZZN2v88internal4wasm15InstanceBuilder20ExecuteStartFunctionEvE28trace_event_unique_atomic564,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal4wasm15InstanceBuilder20ExecuteStartFunctionEvE28trace_event_unique_atomic564, @object
	.size	_ZZN2v88internal4wasm15InstanceBuilder20ExecuteStartFunctionEvE28trace_event_unique_atomic564, 8
_ZZN2v88internal4wasm15InstanceBuilder20ExecuteStartFunctionEvE28trace_event_unique_atomic564:
	.zero	8
	.section	.bss._ZZN2v88internal4wasm15InstanceBuilder5BuildEvE28trace_event_unique_atomic272,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal4wasm15InstanceBuilder5BuildEvE28trace_event_unique_atomic272, @object
	.size	_ZZN2v88internal4wasm15InstanceBuilder5BuildEvE28trace_event_unique_atomic272, 8
_ZZN2v88internal4wasm15InstanceBuilder5BuildEvE28trace_event_unique_atomic272:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC15:
	.long	2139095039
	.align 4
.LC16:
	.long	2139095040
	.align 4
.LC17:
	.long	4286578687
	.align 4
.LC18:
	.long	4286578688
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC19:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC20:
	.long	4294967295
	.long	2146435071
	.align 8
.LC21:
	.long	4290772992
	.long	1105199103
	.align 8
.LC22:
	.long	0
	.long	-1042284544
	.align 8
.LC23:
	.long	3758096384
	.long	1206910975
	.align 8
.LC24:
	.long	4026531839
	.long	1206910975
	.align 8
.LC25:
	.long	3758096384
	.long	-940572673
	.align 8
.LC26:
	.long	4026531839
	.long	-940572673
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
