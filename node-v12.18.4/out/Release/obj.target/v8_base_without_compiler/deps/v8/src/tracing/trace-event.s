	.file	"trace-event.cc"
	.text
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB1936:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1936:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv
	.type	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv, @function
_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv:
.LFB9150:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	144(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE9150:
	.size	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv, .-_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv
	.section	.rodata._ZN2v88internal7tracing21CallStatsScopedTracer16AddEndTraceEventEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"runtime-call-stats"
	.section	.text._ZN2v88internal7tracing21CallStatsScopedTracer16AddEndTraceEventEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7tracing21CallStatsScopedTracer16AddEndTraceEventEv
	.type	_ZN2v88internal7tracing21CallStatsScopedTracer16AddEndTraceEventEv, @function
_ZN2v88internal7tracing21CallStatsScopedTracer16AddEndTraceEventEv:
.LFB9151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rdi)
	movq	8(%rdi), %rax
	jne	.L6
	cmpq	$0, 16(%rax)
	je	.L6
	movq	%rdi, %rbx
	leaq	-88(%rbp), %rdi
	call	_ZN2v87tracing11TracedValue6CreateEv@PLT
	movq	8(%rbx), %rax
	movq	-88(%rbp), %rsi
	movq	16(%rax), %rax
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats4DumpEPNS_7tracing11TracedValueE@PLT
	movq	8(%rbx), %rax
	movq	(%rax), %r12
	movq	8(%rax), %r13
	leaq	.LC0(%rip), %rax
	movb	$8, -89(%rbp)
	movq	%rax, -80(%rbp)
	movq	-88(%rbp), %rax
	movq	$0, -56(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*144(%rax)
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L33
.L7:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L8
	movq	(%rdi), %rax
	call	*8(%rax)
.L8:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L9
	movq	(%rdi), %rax
	call	*8(%rax)
.L9:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5
.L32:
	movq	(%rdi), %rax
	call	*8(%rax)
.L5:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L34
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movq	8(%rax), %r12
	movq	(%rax), %r13
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*144(%rax)
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L35
.L12:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L13
	movq	(%rdi), %rax
	call	*8(%rax)
.L13:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L32
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L35:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r12, %rcx
	movl	$69, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L33:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r13, %rcx
	movl	$69, %esi
	pushq	%rdx
	leaq	-72(%rbp), %rdx
	pushq	%rdx
	leaq	-89(%rbp), %rdx
	pushq	%rdx
	leaq	-80(%rbp), %rdx
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$1
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L7
.L34:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9151:
	.size	_ZN2v88internal7tracing21CallStatsScopedTracer16AddEndTraceEventEv, .-_ZN2v88internal7tracing21CallStatsScopedTracer16AddEndTraceEventEv
	.section	.text._ZN2v88internal7tracing21CallStatsScopedTracer10InitializeEPNS0_7IsolateEPKhPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7tracing21CallStatsScopedTracer10InitializeEPNS0_7IsolateEPKhPKc
	.type	_ZN2v88internal7tracing21CallStatsScopedTracer10InitializeEPNS0_7IsolateEPKhPKc, @function
_ZN2v88internal7tracing21CallStatsScopedTracer10InitializeEPNS0_7IsolateEPKhPKc:
.LFB9153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	%rsi, 32(%rdi)
	movq	%rdx, 16(%rdi)
	movq	%rcx, 24(%rdi)
	movq	%rax, 8(%rdi)
	movq	40960(%rsi), %rdx
	movzbl	23256(%rdx), %eax
	movb	%al, (%rdi)
	testb	%al, %al
	je	.L49
.L37:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*144(%rax)
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L50
.L38:
	movq	-40(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L39
	movq	(%rdi), %rax
	call	*8(%rax)
.L39:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L36
	movq	(%rdi), %rax
	call	*8(%rax)
.L36:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	leaq	23240(%rdx), %rdi
	call	_ZN2v88internal16RuntimeCallStats5ResetEv@PLT
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L50:
	subq	$8, %rsp
	leaq	-48(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r13, %rcx
	movl	$66, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L38
.L51:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9153:
	.size	_ZN2v88internal7tracing21CallStatsScopedTracer10InitializeEPNS0_7IsolateEPKhPKc, .-_ZN2v88internal7tracing21CallStatsScopedTracer10InitializeEPNS0_7IsolateEPKhPKc
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv, @function
_GLOBAL__sub_I__ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv:
.LFB10507:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE10507:
	.size	_GLOBAL__sub_I__ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv, .-_GLOBAL__sub_I__ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
