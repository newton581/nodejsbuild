	.file	"literal-buffer.cc"
	.text
	.section	.text._ZNK2v88internal13LiteralBuffer11InternalizeEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13LiteralBuffer11InternalizeEPNS0_7IsolateE
	.type	_ZNK2v88internal13LiteralBuffer11InternalizeEPNS0_7IsolateE, @function
_ZNK2v88internal13LiteralBuffer11InternalizeEPNS0_7IsolateE:
.LFB8633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 20(%rdi)
	movslq	16(%rdi), %rax
	movq	%rdx, -32(%rbp)
	je	.L2
	leaq	-32(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, %rdi
	movq	%rax, -24(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
.L3:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L7
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	sarl	%eax
	leaq	-32(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, %rdi
	cltq
	movq	%rax, -24(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringItEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	jmp	.L3
.L7:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8633:
	.size	_ZNK2v88internal13LiteralBuffer11InternalizeEPNS0_7IsolateE, .-_ZNK2v88internal13LiteralBuffer11InternalizeEPNS0_7IsolateE
	.section	.text._ZN2v88internal13LiteralBuffer11NewCapacityEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13LiteralBuffer11NewCapacityEi
	.type	_ZN2v88internal13LiteralBuffer11NewCapacityEi, @function
_ZN2v88internal13LiteralBuffer11NewCapacityEi:
.LFB8634:
	.cfi_startproc
	endbr64
	leal	0(,%rsi,4), %edx
	leal	1048576(%rsi), %eax
	cmpl	$349524, %esi
	cmovle	%edx, %eax
	ret
	.cfi_endproc
.LFE8634:
	.size	_ZN2v88internal13LiteralBuffer11NewCapacityEi, .-_ZN2v88internal13LiteralBuffer11NewCapacityEi
	.section	.rodata._ZN2v88internal13LiteralBuffer12ExpandBufferEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"NewArray"
	.section	.text._ZN2v88internal13LiteralBuffer12ExpandBufferEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13LiteralBuffer12ExpandBufferEv
	.type	_ZN2v88internal13LiteralBuffer12ExpandBufferEv, @function
_ZN2v88internal13LiteralBuffer12ExpandBufferEv:
.LFB8635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %eax
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rdi), %rdx
	cmpl	$16, %edx
	cmovge	%edx, %eax
	cmpl	$349524, %edx
	leal	0(,%rax,4), %ecx
	leal	1048576(%rax), %ebx
	cmovle	%ecx, %ebx
	movslq	%ebx, %rbx
	movq	%rbx, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L23
	movq	%rax, %r13
.L14:
	movslq	16(%r12), %rdx
	movq	(%r12), %r14
	testl	%edx, %edx
	jg	.L24
	testq	%r14, %r14
	jne	.L16
.L17:
	movq	%r13, (%r12)
	movq	%rbx, 8(%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	memcpy@PLT
.L16:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	jmp	.L17
.L23:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L14
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE8635:
	.size	_ZN2v88internal13LiteralBuffer12ExpandBufferEv, .-_ZN2v88internal13LiteralBuffer12ExpandBufferEv
	.section	.text._ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv
	.type	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv, @function
_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv:
.LFB8637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	16(%rdi), %edx
	movq	8(%rdi), %r14
	leal	(%rdx,%rdx), %r13d
	cmpl	%r14d, %r13d
	jl	.L26
	sall	$3, %edx
	leal	1048576(%r13), %r14d
	cmpl	$349524, %r13d
	cmovg	%r14d, %edx
	leaq	_ZSt7nothrow(%rip), %rsi
	movslq	%edx, %r14
	movq	%r14, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L29
.L42:
	movl	16(%r12), %edx
	movq	(%r12), %rdi
	subl	$1, %edx
	js	.L34
.L33:
	movslq	%edx, %rdx
	.p2align 4,,10
	.p2align 3
.L35:
	movzbl	(%rdi,%rdx), %ecx
	movw	%cx, (%rbx,%rdx,2)
	subq	$1, %rdx
	testl	%edx, %edx
	jns	.L35
	movq	(%r12), %rdi
.L34:
	cmpq	%rdi, %rbx
	je	.L32
	testq	%rdi, %rdi
	je	.L36
	call	_ZdaPv@PLT
.L36:
	movq	%rbx, (%r12)
	movq	%r14, 8(%r12)
.L32:
	popq	%rbx
	movl	%r13d, 16(%r12)
	movb	$0, 20(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	(%rdi), %rdi
	subl	$1, %edx
	js	.L32
	movq	%rdi, %rbx
	jmp	.L33
.L29:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r14, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L42
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE8637:
	.size	_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv, .-_ZN2v88internal13LiteralBuffer16ConvertToTwoByteEv
	.section	.text._ZN2v88internal13LiteralBuffer14AddTwoByteCharEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi
	.type	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi, @function
_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi:
.LFB8641:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movslq	16(%rdi), %rax
	cmpl	8(%rdi), %eax
	jge	.L49
.L44:
	addq	(%rbx), %rax
	cmpl	$65535, %r12d
	jg	.L45
	movw	%r12w, (%rax)
	addl	$2, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	leal	-65536(%r12), %edx
	shrl	$10, %edx
	andw	$1023, %dx
	subw	$10240, %dx
	movw	%dx, (%rax)
	movl	16(%rbx), %eax
	addl	$2, %eax
	movl	%eax, 16(%rbx)
	cmpl	8(%rbx), %eax
	jge	.L50
.L47:
	movq	(%rbx), %rdx
	andw	$1023, %r12w
	cltq
	subw	$9216, %r12w
	movw	%r12w, (%rdx,%rax)
	addl	$2, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv
	movslq	16(%rbx), %rax
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%rbx, %rdi
	call	_ZN2v88internal13LiteralBuffer12ExpandBufferEv
	movl	16(%rbx), %eax
	jmp	.L47
	.cfi_endproc
.LFE8641:
	.size	_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi, .-_ZN2v88internal13LiteralBuffer14AddTwoByteCharEi
	.section	.text.startup._GLOBAL__sub_I__ZNK2v88internal13LiteralBuffer11InternalizeEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZNK2v88internal13LiteralBuffer11InternalizeEPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZNK2v88internal13LiteralBuffer11InternalizeEPNS0_7IsolateE:
.LFB9906:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE9906:
	.size	_GLOBAL__sub_I__ZNK2v88internal13LiteralBuffer11InternalizeEPNS0_7IsolateE, .-_GLOBAL__sub_I__ZNK2v88internal13LiteralBuffer11InternalizeEPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZNK2v88internal13LiteralBuffer11InternalizeEPNS0_7IsolateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
