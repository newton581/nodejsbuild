	.file	"traced-value.cc"
	.text
	.section	.text._ZN2v87tracing11TracedValueD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87tracing11TracedValueD2Ev
	.type	_ZN2v87tracing11TracedValueD2Ev, @function
_ZN2v87tracing11TracedValueD2Ev:
.LFB3975:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	leaq	16+_ZTVN2v87tracing11TracedValueE(%rip), %rax
	addq	$24, %rdi
	movq	%rax, -24(%rdi)
	cmpq	%rdi, %r8
	je	.L1
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1:
	ret
	.cfi_endproc
.LFE3975:
	.size	_ZN2v87tracing11TracedValueD2Ev, .-_ZN2v87tracing11TracedValueD2Ev
	.globl	_ZN2v87tracing11TracedValueD1Ev
	.set	_ZN2v87tracing11TracedValueD1Ev,_ZN2v87tracing11TracedValueD2Ev
	.section	.text._ZN2v87tracing11TracedValueD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87tracing11TracedValueD0Ev
	.type	_ZN2v87tracing11TracedValueD0Ev, @function
_ZN2v87tracing11TracedValueD0Ev:
.LFB3977:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v87tracing11TracedValueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L5
	call	_ZdlPv@PLT
.L5:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE3977:
	.size	_ZN2v87tracing11TracedValueD0Ev, .-_ZN2v87tracing11TracedValueD0Ev
	.section	.rodata._ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%d"
	.section	.text._ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0:
.LFB4699:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$208, %rsp
	.cfi_offset 3, -32
	movq	%r8, -160(%rbp)
	movq	%r9, -152(%rbp)
	testb	%al, %al
	je	.L8
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm5, -64(%rbp)
	movaps	%xmm6, -48(%rbp)
	movaps	%xmm7, -32(%rbp)
.L8:
	movq	%fs:40, %rax
	movq	%rax, -200(%rbp)
	xorl	%eax, %eax
	movq	%rsp, %rax
	cmpq	%rax, %rsp
	je	.L10
.L29:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L29
.L10:
	subq	$32, %rsp
	orq	$0, 24(%rsp)
	movl	$16, %ecx
	movl	$16, %esi
	movl	$1, %edx
	leaq	.LC0(%rip), %r8
	leaq	15(%rsp), %rbx
	leaq	16(%rbp), %rax
	movl	$32, -224(%rbp)
	andq	$-16, %rbx
	movq	%rax, -216(%rbp)
	leaq	-224(%rbp), %r9
	leaq	-192(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, -208(%rbp)
	movl	$48, -220(%rbp)
	call	__vsnprintf_chk@PLT
	leaq	16(%r12), %rcx
	movq	%rcx, (%r12)
	movslq	%eax, %rsi
	cmpl	$1, %eax
	jne	.L12
	movzbl	(%rbx), %eax
	movl	$1, %esi
	movb	%al, 16(%r12)
.L13:
	movq	%rsi, 8(%r12)
	movb	$0, (%rcx,%rsi)
	movq	-200(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L30
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	cmpl	$8, %eax
	jnb	.L14
	testb	$4, %al
	jne	.L31
	testl	%eax, %eax
	je	.L15
	movzbl	(%rbx), %edx
	movb	%dl, 16(%r12)
	testb	$2, %al
	jne	.L32
.L15:
	movq	(%r12), %rcx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L14:
	movq	(%rbx), %rdx
	movq	%rdx, 16(%r12)
	movl	%eax, %edx
	movq	-8(%rbx,%rdx), %rdi
	movq	%rdi, -8(%rcx,%rdx)
	leaq	24(%r12), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	%ecx, %eax
	subq	%rcx, %rbx
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L15
	andl	$-8, %eax
	xorl	%edx, %edx
.L18:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	(%rbx,%rcx), %r8
	movq	%r8, (%rdi,%rcx)
	cmpl	%eax, %edx
	jb	.L18
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L31:
	movl	(%rbx), %edx
	movl	%edx, 16(%r12)
	movl	%eax, %edx
	movl	-4(%rbx,%rdx), %eax
	movl	%eax, -4(%rcx,%rdx)
	jmp	.L15
.L32:
	movl	%eax, %edx
	movzwl	-2(%rbx,%rdx), %eax
	movw	%ax, -2(%rcx,%rdx)
	jmp	.L15
.L30:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4699:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	.section	.text._ZNK2v87tracing11TracedValue19AppendAsTraceFormatEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v87tracing11TracedValue19AppendAsTraceFormatEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZNK2v87tracing11TracedValue19AppendAsTraceFormatEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZNK2v87tracing11TracedValue19AppendAsTraceFormatEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB3995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16(%rsi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	8(%rsi), %r13
	movq	(%rsi), %rax
	leaq	1(%r13), %r15
	cmpq	%r14, %rax
	je	.L38
	movq	16(%rsi), %rdx
.L34:
	cmpq	%rdx, %r15
	ja	.L41
.L35:
	movb	$123, (%rax,%r13)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r15, 8(%rbx)
	movb	$0, 1(%rax,%r13)
	movq	16(%r12), %rdx
	movq	8(%r12), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	8(%rbx), %r12
	movq	(%rbx), %rax
	leaq	1(%r12), %r13
	cmpq	%r14, %rax
	je	.L39
	movq	16(%rbx), %rdx
.L36:
	cmpq	%rdx, %r13
	ja	.L42
.L37:
	movb	$125, (%rax,%r12)
	movq	(%rbx), %rax
	movq	%r13, 8(%rbx)
	movb	$0, 1(%rax,%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L42:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L38:
	movl	$15, %edx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$15, %edx
	jmp	.L36
	.cfi_endproc
.LFE3995:
	.size	_ZNK2v87tracing11TracedValue19AppendAsTraceFormatEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZNK2v87tracing11TracedValue19AppendAsTraceFormatEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata._ZN2v87tracing12_GLOBAL__N_121EscapeAndAppendStringEPKcPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"basic_string::append"
.LC2:
	.string	"\\b"
.LC3:
	.string	"\\f"
.LC4:
	.string	"\\n"
.LC5:
	.string	"\\r"
.LC6:
	.string	"\\t"
.LC7:
	.string	"\\\""
.LC8:
	.string	"\\\\"
.LC9:
	.string	"\\u%04X"
	.section	.text._ZN2v87tracing12_GLOBAL__N_121EscapeAndAppendStringEPKcPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v87tracing12_GLOBAL__N_121EscapeAndAppendStringEPKcPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v87tracing12_GLOBAL__N_121EscapeAndAppendStringEPKcPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB3962:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	addq	$16, %rsi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	-8(%rsi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-16(%rsi), %rax
	movq	%rsi, -72(%rbp)
	leaq	1(%rbx), %r12
	cmpq	%rsi, %rax
	je	.L70
	movq	16(%r15), %rdx
.L44:
	cmpq	%rdx, %r12
	ja	.L83
.L45:
	movb	$34, (%rax,%rbx)
	movq	(%r15), %rax
	movq	%r12, 8(%r15)
	movb	$0, 1(%rax,%rbx)
	movzbl	(%r14), %ebx
	testb	%bl, %bl
	je	.L46
	leaq	-64(%rbp), %rax
	leaq	.L51(%rip), %r13
	movabsq	$4611686018427387903, %r12
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L47:
	addq	$1, %r14
	cmpb	$34, %bl
	jg	.L48
	cmpb	$7, %bl
	jle	.L49
	leal	-8(%rbx), %eax
	cmpb	$26, %al
	ja	.L49
	movzbl	%al, %eax
	movslq	0(%r13,%rax,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v87tracing12_GLOBAL__N_121EscapeAndAppendStringEPKcPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"a",@progbits
	.align 4
	.align 4
.L51:
	.long	.L56-.L51
	.long	.L55-.L51
	.long	.L54-.L51
	.long	.L49-.L51
	.long	.L53-.L51
	.long	.L52-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L49-.L51
	.long	.L50-.L51
	.section	.text._ZN2v87tracing12_GLOBAL__N_121EscapeAndAppendStringEPKcPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.L50:
	movq	%r12, %rax
	subq	8(%r15), %rax
	cmpq	$1, %rax
	jbe	.L60
	movl	$2, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	.p2align 4,,10
	.p2align 3
.L59:
	movzbl	(%r14), %ebx
	testb	%bl, %bl
	jne	.L47
.L46:
	movq	8(%r15), %rbx
	movq	(%r15), %rax
	leaq	1(%rbx), %r12
	cmpq	%rax, -72(%rbp)
	je	.L72
	movq	16(%r15), %rdx
.L67:
	cmpq	%rdx, %r12
	ja	.L84
.L68:
	movb	$34, (%rax,%rbx)
	movq	(%r15), %rax
	movq	%r12, 8(%r15)
	movb	$0, 1(%rax,%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L52:
	.cfi_restore_state
	movq	%r12, %rax
	subq	8(%r15), %rax
	cmpq	$1, %rax
	jbe	.L60
	movl	$2, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	jmp	.L59
.L56:
	movq	%r12, %rax
	subq	8(%r15), %rax
	cmpq	$1, %rax
	jbe	.L60
	movl	$2, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	jmp	.L59
.L49:
	cmpb	$31, %bl
	jbe	.L73
	cmpb	$127, %bl
	je	.L73
	movq	8(%r15), %rax
	movq	(%r15), %rdx
	leaq	1(%rax), %r11
	cmpq	%rdx, -72(%rbp)
	je	.L71
	movq	16(%r15), %rcx
.L65:
	cmpq	%rcx, %r11
	ja	.L86
.L66:
	movb	%bl, (%rdx,%rax)
	movq	(%r15), %rdx
	movq	%r11, 8(%r15)
	movb	$0, 1(%rdx,%rax)
	jmp	.L59
.L54:
	movq	%r12, %rax
	subq	8(%r15), %rax
	cmpq	$1, %rax
	jbe	.L60
	movl	$2, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	jmp	.L59
.L53:
	movq	%r12, %rax
	subq	8(%r15), %rax
	cmpq	$1, %rax
	jbe	.L60
	movl	$2, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	jmp	.L59
.L55:
	movq	%r12, %rax
	subq	8(%r15), %rax
	cmpq	$1, %rax
	jbe	.L60
	movl	$2, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L48:
	cmpb	$92, %bl
	jne	.L49
	movq	%r12, %rax
	subq	8(%r15), %rax
	cmpq	$1, %rax
	jbe	.L60
	movl	$2, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L73:
	movzbl	%bl, %ecx
	movq	-80(%rbp), %rbx
	leaq	.LC9(%rip), %rdx
	xorl	%eax, %eax
	movl	$8, %esi
	movq	%rbx, %rdi
	call	_ZN2v84base2OS8SNPrintFEPciPKcz@PLT
	movq	%rbx, %rdx
.L63:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L63
	movl	%eax, %ecx
	movq	-80(%rbp), %rsi
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %edi
	addb	%al, %dil
	movq	%r12, %rax
	sbbq	$3, %rdx
	subq	8(%r15), %rax
	subq	%rsi, %rdx
	cmpq	%rax, %rdx
	ja	.L60
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L83:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r15), %rax
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L84:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r15), %rax
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$15, %ecx
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L70:
	movl	$15, %edx
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L72:
	movl	$15, %edx
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L86:
	xorl	%edx, %edx
	movq	%rax, %rsi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%r11, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r15), %rdx
	movq	-96(%rbp), %r11
	movq	-88(%rbp), %rax
	jmp	.L66
.L60:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L85:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3962:
	.size	_ZN2v87tracing12_GLOBAL__N_121EscapeAndAppendStringEPKcPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v87tracing12_GLOBAL__N_121EscapeAndAppendStringEPKcPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v87tracing11TracedValue6CreateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87tracing11TracedValue6CreateEv
	.type	_ZN2v87tracing11TracedValue6CreateEv, @function
_ZN2v87tracing11TracedValue6CreateEv:
.LFB3963:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$48, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	leaq	16+_ZTVN2v87tracing11TracedValueE(%rip), %rcx
	leaq	24(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%rdx, 8(%rax)
	movq	$0, 16(%rax)
	movb	$0, 24(%rax)
	movb	$1, 40(%rax)
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3963:
	.size	_ZN2v87tracing11TracedValue6CreateEv, .-_ZN2v87tracing11TracedValue6CreateEv
	.section	.text._ZN2v87tracing11TracedValueC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87tracing11TracedValueC2Ev
	.type	_ZN2v87tracing11TracedValueC2Ev, @function
_ZN2v87tracing11TracedValueC2Ev:
.LFB3972:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v87tracing11TracedValueE(%rip), %rax
	movq	$0, 16(%rdi)
	movq	%rax, (%rdi)
	leaq	24(%rdi), %rax
	movq	%rax, 8(%rdi)
	movb	$0, 24(%rdi)
	movb	$1, 40(%rdi)
	ret
	.cfi_endproc
.LFE3972:
	.size	_ZN2v87tracing11TracedValueC2Ev, .-_ZN2v87tracing11TracedValueC2Ev
	.globl	_ZN2v87tracing11TracedValueC1Ev
	.set	_ZN2v87tracing11TracedValueC1Ev,_ZN2v87tracing11TracedValueC2Ev
	.section	.text._ZN2v87tracing11TracedValue13AppendIntegerEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87tracing11TracedValue13AppendIntegerEi
	.type	_ZN2v87tracing11TracedValue13AppendIntegerEi, @function
_ZN2v87tracing11TracedValue13AppendIntegerEi:
.LFB3985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 40(%rdi)
	je	.L91
	movb	$0, 40(%rdi)
.L92:
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-96(%rbp), %rdi
	movl	%r12d, %r8d
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	movl	$16, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L90
	call	_ZdlPv@PLT
.L90:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L99
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	movq	16(%rdi), %r14
	movq	8(%rdi), %rax
	leaq	24(%rdi), %rdx
	leaq	1(%r14), %r15
	cmpq	%rdx, %rax
	je	.L97
	movq	24(%rdi), %rdx
.L93:
	cmpq	%rdx, %r15
	ja	.L100
.L94:
	movb	$44, (%rax,%r14)
	movq	8(%rbx), %rax
	movq	%r15, 16(%rbx)
	movb	$0, 1(%rax,%r14)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L100:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L97:
	movl	$15, %edx
	jmp	.L93
.L99:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3985:
	.size	_ZN2v87tracing11TracedValue13AppendIntegerEi, .-_ZN2v87tracing11TracedValue13AppendIntegerEi
	.section	.text._ZN2v87tracing11TracedValue12AppendDoubleEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87tracing11TracedValue12AppendDoubleEd
	.type	_ZN2v87tracing11TracedValue12AppendDoubleEd, @function
_ZN2v87tracing11TracedValue12AppendDoubleEd:
.LFB3986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	8(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$144, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 40(%rdi)
	je	.L102
	movb	$0, 40(%rdi)
.L103:
	leaq	-144(%rbp), %rdi
	movl	$100, %esi
	movq	$100, -152(%rbp)
	movq	%rdi, -160(%rbp)
	call	_ZN2v88internal15DoubleToCStringEdNS0_6VectorIcEE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	16(%rbx), %rax
	cmpq	%rax, %rdx
	ja	.L110
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L111
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movq	16(%rdi), %r12
	movq	8(%rdi), %rax
	leaq	24(%rdi), %rdx
	leaq	1(%r12), %r14
	cmpq	%rdx, %rax
	je	.L108
	movq	24(%rdi), %rdx
.L104:
	cmpq	%rdx, %r14
	ja	.L112
.L105:
	movb	$44, (%rax,%r12)
	movq	8(%rbx), %rax
	movq	%r14, 16(%rbx)
	movb	$0, 1(%rax,%r12)
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L112:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movsd	%xmm0, -168(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	movsd	-168(%rbp), %xmm0
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L108:
	movl	$15, %edx
	jmp	.L104
.L110:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L111:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3986:
	.size	_ZN2v87tracing11TracedValue12AppendDoubleEd, .-_ZN2v87tracing11TracedValue12AppendDoubleEd
	.section	.rodata._ZN2v87tracing11TracedValue13AppendBooleanEb.str1.1,"aMS",@progbits,1
.LC10:
	.string	"true"
.LC11:
	.string	"false"
	.section	.text._ZN2v87tracing11TracedValue13AppendBooleanEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87tracing11TracedValue13AppendBooleanEb
	.type	_ZN2v87tracing11TracedValue13AppendBooleanEb, @function
_ZN2v87tracing11TracedValue13AppendBooleanEb:
.LFB3987:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$8, %rdi
	subq	$16, %rsp
	cmpb	$0, 40(%rbx)
	je	.L114
	movb	$0, 40(%rbx)
.L115:
	cmpb	$1, %r12b
	leaq	.LC11(%rip), %rax
	leaq	.LC10(%rip), %rsi
	sbbq	%rdx, %rdx
	notq	%rdx
	addq	$5, %rdx
	testb	%r12b, %r12b
	cmove	%rax, %rsi
	movabsq	$4611686018427387903, %rax
	subq	16(%rbx), %rax
	cmpq	%rdx, %rax
	jb	.L123
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	movq	16(%rbx), %r13
	movq	8(%rbx), %rax
	leaq	24(%rbx), %rdx
	leaq	1(%r13), %r14
	cmpq	%rdx, %rax
	je	.L120
	movq	24(%rbx), %rdx
.L116:
	cmpq	%rdx, %r14
	ja	.L124
.L117:
	movb	$44, (%rax,%r13)
	movq	8(%rbx), %rax
	movq	%r14, 16(%rbx)
	movb	$0, 1(%rax,%r13)
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L124:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rdi, -40(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	movq	-40(%rbp), %rdi
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L120:
	movl	$15, %edx
	jmp	.L116
.L123:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE3987:
	.size	_ZN2v87tracing11TracedValue13AppendBooleanEb, .-_ZN2v87tracing11TracedValue13AppendBooleanEb
	.section	.text._ZN2v87tracing11TracedValue12AppendStringEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87tracing11TracedValue12AppendStringEPKc
	.type	_ZN2v87tracing11TracedValue12AppendStringEPKc, @function
_ZN2v87tracing11TracedValue12AppendStringEPKc:
.LFB3988:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 40(%rdi)
	je	.L126
	movb	$0, 40(%rdi)
.L127:
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87tracing12_GLOBAL__N_121EscapeAndAppendStringEPKcPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	movq	16(%rdi), %r14
	movq	8(%rdi), %rax
	leaq	24(%rdi), %rdx
	leaq	1(%r14), %r15
	cmpq	%rdx, %rax
	je	.L130
	movq	24(%rdi), %rdx
.L128:
	cmpq	%rdx, %r15
	ja	.L132
.L129:
	movb	$44, (%rax,%r14)
	movq	8(%rbx), %rax
	movq	%r15, 16(%rbx)
	movb	$0, 1(%rax,%r14)
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L132:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L130:
	movl	$15, %edx
	jmp	.L128
	.cfi_endproc
.LFE3988:
	.size	_ZN2v87tracing11TracedValue12AppendStringEPKc, .-_ZN2v87tracing11TracedValue12AppendStringEPKc
	.section	.text._ZN2v87tracing11TracedValue15BeginDictionaryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87tracing11TracedValue15BeginDictionaryEv
	.type	_ZN2v87tracing11TracedValue15BeginDictionaryEv, @function
_ZN2v87tracing11TracedValue15BeginDictionaryEv:
.LFB3989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	24(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %r12
	cmpb	$0, 40(%rdi)
	movq	8(%rdi), %rax
	leaq	1(%r12), %r14
	je	.L134
	movb	$0, 40(%rdi)
	cmpq	%rax, %r13
	je	.L141
.L145:
	movq	24(%rbx), %rdx
	cmpq	%r14, %rdx
	jb	.L143
.L139:
	movb	$123, (%rax,%r12)
	movq	8(%rbx), %rax
	movq	%r14, 16(%rbx)
	movb	$0, (%rax,%r14)
	movb	$1, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	cmpq	%r13, %rax
	je	.L140
	movq	24(%rdi), %rdx
.L136:
	cmpq	%r14, %rdx
	jb	.L144
.L137:
	movb	$44, (%rax,%r12)
	movq	8(%rbx), %rax
	movq	%r14, 16(%rbx)
	movb	$0, (%rax,%r14)
	movq	16(%rbx), %r12
	movq	8(%rbx), %rax
	leaq	1(%r12), %r14
	cmpq	%rax, %r13
	jne	.L145
.L141:
	movl	$15, %edx
	cmpq	%r14, %rdx
	jnb	.L139
	.p2align 4,,10
	.p2align 3
.L143:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L144:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L140:
	movl	$15, %edx
	jmp	.L136
	.cfi_endproc
.LFE3989:
	.size	_ZN2v87tracing11TracedValue15BeginDictionaryEv, .-_ZN2v87tracing11TracedValue15BeginDictionaryEv
	.section	.text._ZN2v87tracing11TracedValue10BeginArrayEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87tracing11TracedValue10BeginArrayEv
	.type	_ZN2v87tracing11TracedValue10BeginArrayEv, @function
_ZN2v87tracing11TracedValue10BeginArrayEv:
.LFB3990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	24(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %r12
	cmpb	$0, 40(%rdi)
	movq	8(%rdi), %rax
	leaq	1(%r12), %r14
	je	.L147
	movb	$0, 40(%rdi)
	cmpq	%rax, %r13
	je	.L154
.L158:
	movq	24(%rbx), %rdx
	cmpq	%r14, %rdx
	jb	.L156
.L152:
	movb	$91, (%rax,%r12)
	movq	8(%rbx), %rax
	movq	%r14, 16(%rbx)
	movb	$0, (%rax,%r14)
	movb	$1, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	cmpq	%r13, %rax
	je	.L153
	movq	24(%rdi), %rdx
.L149:
	cmpq	%r14, %rdx
	jb	.L157
.L150:
	movb	$44, (%rax,%r12)
	movq	8(%rbx), %rax
	movq	%r14, 16(%rbx)
	movb	$0, (%rax,%r14)
	movq	16(%rbx), %r12
	movq	8(%rbx), %rax
	leaq	1(%r12), %r14
	cmpq	%rax, %r13
	jne	.L158
.L154:
	movl	$15, %edx
	cmpq	%r14, %rdx
	jnb	.L152
	.p2align 4,,10
	.p2align 3
.L156:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L157:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L153:
	movl	$15, %edx
	jmp	.L149
	.cfi_endproc
.LFE3990:
	.size	_ZN2v87tracing11TracedValue10BeginArrayEv, .-_ZN2v87tracing11TracedValue10BeginArrayEv
	.section	.text._ZN2v87tracing11TracedValue13EndDictionaryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87tracing11TracedValue13EndDictionaryEv
	.type	_ZN2v87tracing11TracedValue13EndDictionaryEv, @function
_ZN2v87tracing11TracedValue13EndDictionaryEv:
.LFB3991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %r12
	movq	8(%rdi), %rax
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L162
	movq	24(%rdi), %rdx
.L160:
	cmpq	%rdx, %r13
	ja	.L164
.L161:
	movb	$125, (%rax,%r12)
	movq	8(%rbx), %rax
	movq	%r13, 16(%rbx)
	movb	$0, 1(%rax,%r12)
	movb	$0, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	leaq	8(%rbx), %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L162:
	movl	$15, %edx
	jmp	.L160
	.cfi_endproc
.LFE3991:
	.size	_ZN2v87tracing11TracedValue13EndDictionaryEv, .-_ZN2v87tracing11TracedValue13EndDictionaryEv
	.section	.text._ZN2v87tracing11TracedValue8EndArrayEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87tracing11TracedValue8EndArrayEv
	.type	_ZN2v87tracing11TracedValue8EndArrayEv, @function
_ZN2v87tracing11TracedValue8EndArrayEv:
.LFB3992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %r12
	movq	8(%rdi), %rax
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L168
	movq	24(%rdi), %rdx
.L166:
	cmpq	%rdx, %r13
	ja	.L170
.L167:
	movb	$93, (%rax,%r12)
	movq	8(%rbx), %rax
	movq	%r13, 16(%rbx)
	movb	$0, 1(%rax,%r12)
	movb	$0, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	leaq	8(%rbx), %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L168:
	movl	$15, %edx
	jmp	.L166
	.cfi_endproc
.LFE3992:
	.size	_ZN2v87tracing11TracedValue8EndArrayEv, .-_ZN2v87tracing11TracedValue8EndArrayEv
	.section	.text._ZN2v87tracing11TracedValue10WriteCommaEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87tracing11TracedValue10WriteCommaEv
	.type	_ZN2v87tracing11TracedValue10WriteCommaEv, @function
_ZN2v87tracing11TracedValue10WriteCommaEv:
.LFB3993:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 40(%rdi)
	je	.L172
	movb	$0, 40(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	movq	16(%rdi), %r12
	movq	8(%rdi), %rax
	leaq	24(%rdi), %rdx
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L176
	movq	24(%rdi), %rdx
.L174:
	cmpq	%rdx, %r13
	ja	.L178
.L175:
	movb	$44, (%rax,%r12)
	movq	8(%rbx), %rax
	movq	%r13, 16(%rbx)
	movb	$0, 1(%rax,%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	leaq	8(%rbx), %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L176:
	movl	$15, %edx
	jmp	.L174
	.cfi_endproc
.LFE3993:
	.size	_ZN2v87tracing11TracedValue10WriteCommaEv, .-_ZN2v87tracing11TracedValue10WriteCommaEv
	.section	.rodata._ZN2v87tracing11TracedValue9WriteNameEPKc.str1.1,"aMS",@progbits,1
.LC12:
	.string	"\":"
	.section	.text._ZN2v87tracing11TracedValue9WriteNameEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87tracing11TracedValue9WriteNameEPKc
	.type	_ZN2v87tracing11TracedValue9WriteNameEPKc, @function
_ZN2v87tracing11TracedValue9WriteNameEPKc:
.LFB3994:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24(%rdi), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	8(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r15
	cmpb	$0, 40(%rdi)
	movq	8(%rdi), %rax
	leaq	1(%r15), %r12
	je	.L180
	movb	$0, 40(%rdi)
	cmpq	%rax, %r9
	je	.L189
.L193:
	movq	24(%rbx), %rdx
	cmpq	%r12, %rdx
	jb	.L191
.L185:
	movb	$34, (%rax,%r15)
	movq	8(%rbx), %rax
	movq	%r13, %rdi
	movq	%r12, 16(%rbx)
	movb	$0, (%rax,%r12)
	movabsq	$4611686018427387903, %r12
	call	strlen@PLT
	movq	%rax, %rdx
	movq	%r12, %rax
	subq	16(%rbx), %rax
	cmpq	%rax, %rdx
	ja	.L187
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	subq	16(%rbx), %r12
	cmpq	$1, %r12
	jbe	.L187
	addq	$24, %rsp
	movq	%r14, %rdi
	movl	$2, %edx
	popq	%rbx
	leaq	.LC12(%rip), %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	cmpq	%r9, %rax
	je	.L188
	movq	24(%rdi), %rdx
.L182:
	cmpq	%rdx, %r12
	ja	.L192
.L183:
	movb	$44, (%rax,%r15)
	movq	8(%rbx), %rax
	movq	%r12, 16(%rbx)
	movb	$0, (%rax,%r12)
	movq	16(%rbx), %r15
	movq	8(%rbx), %rax
	leaq	1(%r15), %r12
	cmpq	%rax, %r9
	jne	.L193
.L189:
	movl	$15, %edx
	cmpq	%r12, %rdx
	jnb	.L185
	.p2align 4,,10
	.p2align 3
.L191:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L192:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L188:
	movl	$15, %edx
	jmp	.L182
.L187:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE3994:
	.size	_ZN2v87tracing11TracedValue9WriteNameEPKc, .-_ZN2v87tracing11TracedValue9WriteNameEPKc
	.section	.text._ZN2v87tracing11TracedValue10SetIntegerEPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87tracing11TracedValue10SetIntegerEPKci
	.type	_ZN2v87tracing11TracedValue10SetIntegerEPKci, @function
_ZN2v87tracing11TracedValue10SetIntegerEPKci:
.LFB3978:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87tracing11TracedValue9WriteNameEPKc
	leaq	-64(%rbp), %rdi
	movl	%r12d, %r8d
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC0(%rip), %rcx
	movl	$16, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	leaq	8(%rbx), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L194
	call	_ZdlPv@PLT
.L194:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L198
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L198:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3978:
	.size	_ZN2v87tracing11TracedValue10SetIntegerEPKci, .-_ZN2v87tracing11TracedValue10SetIntegerEPKci
	.section	.text._ZN2v87tracing11TracedValue9SetStringEPKcS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87tracing11TracedValue9SetStringEPKcS3_
	.type	_ZN2v87tracing11TracedValue9SetStringEPKcS3_, @function
_ZN2v87tracing11TracedValue9SetStringEPKcS3_:
.LFB3981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN2v87tracing11TracedValue9WriteNameEPKc
	leaq	8(%rbx), %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87tracing12_GLOBAL__N_121EscapeAndAppendStringEPKcPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
.LFE3981:
	.size	_ZN2v87tracing11TracedValue9SetStringEPKcS3_, .-_ZN2v87tracing11TracedValue9SetStringEPKcS3_
	.section	.text._ZN2v87tracing11TracedValue8SetValueEPKcPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87tracing11TracedValue8SetValueEPKcPS1_
	.type	_ZN2v87tracing11TracedValue8SetValueEPKcPS1_, @function
_ZN2v87tracing11TracedValue8SetValueEPKcPS1_:
.LFB3982:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87tracing11TracedValue9WriteNameEPKc
	movq	(%r12), %rax
	movb	$0, -80(%rbp)
	leaq	_ZNK2v87tracing11TracedValue19AppendAsTraceFormatEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r13, -96(%rbp)
	movq	16(%rax), %rax
	movq	$0, -88(%rbp)
	cmpq	%rdx, %rax
	jne	.L202
	movq	16(%r12), %rdx
	movq	8(%r12), %rsi
	movl	$123, %eax
	movq	%r15, %rdi
	movq	$1, -88(%rbp)
	movw	%ax, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %r12
	movl	$15, %eax
	cmpq	%r13, %rdx
	cmovne	-80(%rbp), %rax
	leaq	1(%r12), %r14
	cmpq	%rax, %r14
	ja	.L210
.L204:
	movb	$125, (%rdx,%r12)
	movq	-96(%rbp), %rax
	movq	%r14, -88(%rbp)
	movb	$0, 1(%rax,%r12)
.L205:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	leaq	8(%rbx), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L201
	call	_ZdlPv@PLT
.L201:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L211
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L210:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-96(%rbp), %rdx
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L202:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L205
.L211:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3982:
	.size	_ZN2v87tracing11TracedValue8SetValueEPKcPS1_, .-_ZN2v87tracing11TracedValue8SetValueEPKcPS1_
	.section	.text._ZN2v87tracing11TracedValue15BeginDictionaryEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87tracing11TracedValue15BeginDictionaryEPKc
	.type	_ZN2v87tracing11TracedValue15BeginDictionaryEPKc, @function
_ZN2v87tracing11TracedValue15BeginDictionaryEPKc:
.LFB3983:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v87tracing11TracedValue9WriteNameEPKc
	movq	16(%rbx), %r12
	movq	8(%rbx), %rax
	leaq	24(%rbx), %rdx
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L215
	movq	24(%rbx), %rdx
.L213:
	cmpq	%rdx, %r13
	ja	.L217
.L214:
	movb	$123, (%rax,%r12)
	movq	8(%rbx), %rax
	movq	%r13, 16(%rbx)
	movb	$0, 1(%rax,%r12)
	movb	$1, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L217:
	.cfi_restore_state
	leaq	8(%rbx), %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L215:
	movl	$15, %edx
	jmp	.L213
	.cfi_endproc
.LFE3983:
	.size	_ZN2v87tracing11TracedValue15BeginDictionaryEPKc, .-_ZN2v87tracing11TracedValue15BeginDictionaryEPKc
	.section	.text._ZN2v87tracing11TracedValue10BeginArrayEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87tracing11TracedValue10BeginArrayEPKc
	.type	_ZN2v87tracing11TracedValue10BeginArrayEPKc, @function
_ZN2v87tracing11TracedValue10BeginArrayEPKc:
.LFB3984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v87tracing11TracedValue9WriteNameEPKc
	movq	16(%rbx), %r12
	movq	8(%rbx), %rax
	leaq	24(%rbx), %rdx
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L221
	movq	24(%rbx), %rdx
.L219:
	cmpq	%rdx, %r13
	ja	.L223
.L220:
	movb	$91, (%rax,%r12)
	movq	8(%rbx), %rax
	movq	%r13, 16(%rbx)
	movb	$0, 1(%rax,%r12)
	movb	$1, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	leaq	8(%rbx), %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L221:
	movl	$15, %edx
	jmp	.L219
	.cfi_endproc
.LFE3984:
	.size	_ZN2v87tracing11TracedValue10BeginArrayEPKc, .-_ZN2v87tracing11TracedValue10BeginArrayEPKc
	.section	.text._ZN2v87tracing11TracedValue9SetDoubleEPKcd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87tracing11TracedValue9SetDoubleEPKcd
	.type	_ZN2v87tracing11TracedValue9SetDoubleEPKcd, @function
_ZN2v87tracing11TracedValue9SetDoubleEPKcd:
.LFB3979:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$152, %rsp
	movsd	%xmm0, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87tracing11TracedValue9WriteNameEPKc
	movsd	-168(%rbp), %xmm0
	movl	$100, %esi
	leaq	-144(%rbp), %rdi
	movq	%rdi, -160(%rbp)
	movq	$100, -152(%rbp)
	call	_ZN2v88internal15DoubleToCStringEdNS0_6VectorIcEE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	16(%rbx), %rax
	cmpq	%rax, %rdx
	ja	.L228
	leaq	8(%rbx), %r13
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L229
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L228:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L229:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3979:
	.size	_ZN2v87tracing11TracedValue9SetDoubleEPKcd, .-_ZN2v87tracing11TracedValue9SetDoubleEPKcd
	.section	.text._ZN2v87tracing11TracedValue10SetBooleanEPKcb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87tracing11TracedValue10SetBooleanEPKcb
	.type	_ZN2v87tracing11TracedValue10SetBooleanEPKcb, @function
_ZN2v87tracing11TracedValue10SetBooleanEPKcb:
.LFB3980:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN2v87tracing11TracedValue9WriteNameEPKc
	cmpb	$1, %r12b
	leaq	.LC11(%rip), %rax
	leaq	.LC10(%rip), %rsi
	sbbq	%rdx, %rdx
	notq	%rdx
	addq	$5, %rdx
	testb	%r12b, %r12b
	cmove	%rax, %rsi
	movabsq	$4611686018427387903, %rax
	subq	16(%rbx), %rax
	cmpq	%rdx, %rax
	jb	.L235
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L235:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE3980:
	.size	_ZN2v87tracing11TracedValue10SetBooleanEPKcb, .-_ZN2v87tracing11TracedValue10SetBooleanEPKcb
	.section	.text.startup._GLOBAL__sub_I__ZN2v87tracing11TracedValue6CreateEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v87tracing11TracedValue6CreateEv, @function
_GLOBAL__sub_I__ZN2v87tracing11TracedValue6CreateEv:
.LFB4678:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE4678:
	.size	_GLOBAL__sub_I__ZN2v87tracing11TracedValue6CreateEv, .-_GLOBAL__sub_I__ZN2v87tracing11TracedValue6CreateEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v87tracing11TracedValue6CreateEv
	.weak	_ZTVN2v87tracing11TracedValueE
	.section	.data.rel.ro.local._ZTVN2v87tracing11TracedValueE,"awG",@progbits,_ZTVN2v87tracing11TracedValueE,comdat
	.align 8
	.type	_ZTVN2v87tracing11TracedValueE, @object
	.size	_ZTVN2v87tracing11TracedValueE, 40
_ZTVN2v87tracing11TracedValueE:
	.quad	0
	.quad	0
	.quad	_ZN2v87tracing11TracedValueD1Ev
	.quad	_ZN2v87tracing11TracedValueD0Ev
	.quad	_ZNK2v87tracing11TracedValue19AppendAsTraceFormatEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
