	.file	"v8-inspector-protocol-encoding.cc"
	.text
	.section	.text._ZN12v8_inspector12_GLOBAL__N_18PlatformD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_18PlatformD2Ev, @function
_ZN12v8_inspector12_GLOBAL__N_18PlatformD2Ev:
.LFB4910:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4910:
	.size	_ZN12v8_inspector12_GLOBAL__N_18PlatformD2Ev, .-_ZN12v8_inspector12_GLOBAL__N_18PlatformD2Ev
	.set	_ZN12v8_inspector12_GLOBAL__N_18PlatformD1Ev,_ZN12v8_inspector12_GLOBAL__N_18PlatformD2Ev
	.section	.rodata._ZNK12v8_inspector12_GLOBAL__N_18Platform6DToStrEd.str1.1,"aMS",@progbits,1
.LC0:
	.string	"NewArray"
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_18Platform6DToStrEd,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_18Platform6DToStrEd, @function
_ZNK12v8_inspector12_GLOBAL__N_18Platform6DToStrEd:
.LFB4208:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$100, %edi
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movsd	%xmm0, -56(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movsd	-56(%rbp), %xmm0
	testq	%rax, %rax
	je	.L9
	movq	%rax, %r12
.L4:
	movq	%r12, %rdi
	movl	$100, %esi
	call	_ZN2v88internal15DoubleToCStringEdNS0_6VectorIcEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L10
	movq	%rax, %rdi
	call	strlen@PLT
	leaq	1(%rax), %rdi
	call	_Znam@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	leaq	1(%rax), %rdx
	call	memcpy@PLT
	movq	%rbx, 0(%r13)
.L6:
	movq	%r12, %rdi
	call	_ZdaPv@PLT
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movq	$0, 0(%r13)
	jmp	.L6
.L9:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$100, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movsd	-56(%rbp), %xmm0
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.L4
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE4208:
	.size	_ZNK12v8_inspector12_GLOBAL__N_18Platform6DToStrEd, .-_ZNK12v8_inspector12_GLOBAL__N_18Platform6DToStrEd
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_18Platform6StrToDEPKcPd,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_18Platform6StrToDEPKcPd, @function
_ZNK12v8_inspector12_GLOBAL__N_18Platform6StrToDEPKcPd:
.LFB4205:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal14StringToDoubleEPKcid@PLT
	ucomisd	%xmm0, %xmm0
	movsd	%xmm0, (%rbx)
	setnp	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4205:
	.size	_ZNK12v8_inspector12_GLOBAL__N_18Platform6StrToDEPKcPd, .-_ZNK12v8_inspector12_GLOBAL__N_18Platform6StrToDEPKcPd
	.section	.text._ZN12v8_inspector12_GLOBAL__N_18PlatformD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_18PlatformD0Ev, @function
_ZN12v8_inspector12_GLOBAL__N_18PlatformD0Ev:
.LFB4912:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4912:
	.size	_ZN12v8_inspector12_GLOBAL__N_18PlatformD0Ev, .-_ZN12v8_inspector12_GLOBAL__N_18PlatformD0Ev
	.section	.text._ZN12v8_inspector17ConvertCBORToJSONEN30v8_inspector_protocol_encoding4spanIhEEPSt6vectorIhSaIhEE,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector17ConvertCBORToJSONEN30v8_inspector_protocol_encoding4spanIhEEPSt6vectorIhSaIhEE
	.type	_ZN12v8_inspector17ConvertCBORToJSONEN30v8_inspector_protocol_encoding4spanIhEEPSt6vectorIhSaIhEE, @function
_ZN12v8_inspector17ConvertCBORToJSONEN30v8_inspector_protocol_encoding4spanIhEEPSt6vectorIhSaIhEE:
.LFB4209:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	movq	%rsi, %r8
	movq	%rdx, %rcx
	movq	%r9, %rsi
	movq	%r8, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-16(%rbp), %rdi
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_18PlatformE(%rip), %rax
	movq	%rax, -16(%rbp)
	call	_ZN30v8_inspector_protocol_encoding4json17ConvertCBORToJSONERKNS0_8PlatformENS_4spanIhEEPSt6vectorIhSaIhEE@PLT
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L17
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L17:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4209:
	.size	_ZN12v8_inspector17ConvertCBORToJSONEN30v8_inspector_protocol_encoding4spanIhEEPSt6vectorIhSaIhEE, .-_ZN12v8_inspector17ConvertCBORToJSONEN30v8_inspector_protocol_encoding4spanIhEEPSt6vectorIhSaIhEE
	.section	.text._ZN12v8_inspector17ConvertJSONToCBOREN30v8_inspector_protocol_encoding4spanIhEEPSt6vectorIhSaIhEE,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector17ConvertJSONToCBOREN30v8_inspector_protocol_encoding4spanIhEEPSt6vectorIhSaIhEE
	.type	_ZN12v8_inspector17ConvertJSONToCBOREN30v8_inspector_protocol_encoding4spanIhEEPSt6vectorIhSaIhEE, @function
_ZN12v8_inspector17ConvertJSONToCBOREN30v8_inspector_protocol_encoding4spanIhEEPSt6vectorIhSaIhEE:
.LFB4220:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	movq	%rsi, %r8
	movq	%rdx, %rcx
	movq	%r9, %rsi
	movq	%r8, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-16(%rbp), %rdi
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_18PlatformE(%rip), %rax
	movq	%rax, -16(%rbp)
	call	_ZN30v8_inspector_protocol_encoding4json17ConvertJSONToCBORERKNS0_8PlatformENS_4spanIhEEPSt6vectorIhSaIhEE@PLT
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L21
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4220:
	.size	_ZN12v8_inspector17ConvertJSONToCBOREN30v8_inspector_protocol_encoding4spanIhEEPSt6vectorIhSaIhEE, .-_ZN12v8_inspector17ConvertJSONToCBOREN30v8_inspector_protocol_encoding4spanIhEEPSt6vectorIhSaIhEE
	.section	.text._ZN12v8_inspector17ConvertJSONToCBOREN30v8_inspector_protocol_encoding4spanItEEPSt6vectorIhSaIhEE,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector17ConvertJSONToCBOREN30v8_inspector_protocol_encoding4spanItEEPSt6vectorIhSaIhEE
	.type	_ZN12v8_inspector17ConvertJSONToCBOREN30v8_inspector_protocol_encoding4spanItEEPSt6vectorIhSaIhEE, @function
_ZN12v8_inspector17ConvertJSONToCBOREN30v8_inspector_protocol_encoding4spanItEEPSt6vectorIhSaIhEE:
.LFB4221:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	movq	%rsi, %r8
	movq	%rdx, %rcx
	movq	%r9, %rsi
	movq	%r8, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-16(%rbp), %rdi
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_18PlatformE(%rip), %rax
	movq	%rax, -16(%rbp)
	call	_ZN30v8_inspector_protocol_encoding4json17ConvertJSONToCBORERKNS0_8PlatformENS_4spanItEEPSt6vectorIhSaIhEE@PLT
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L25
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L25:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4221:
	.size	_ZN12v8_inspector17ConvertJSONToCBOREN30v8_inspector_protocol_encoding4spanItEEPSt6vectorIhSaIhEE, .-_ZN12v8_inspector17ConvertJSONToCBOREN30v8_inspector_protocol_encoding4spanItEEPSt6vectorIhSaIhEE
	.section	.text.startup._GLOBAL__sub_I__ZN12v8_inspector17ConvertCBORToJSONEN30v8_inspector_protocol_encoding4spanIhEEPSt6vectorIhSaIhEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN12v8_inspector17ConvertCBORToJSONEN30v8_inspector_protocol_encoding4spanIhEEPSt6vectorIhSaIhEE, @function
_GLOBAL__sub_I__ZN12v8_inspector17ConvertCBORToJSONEN30v8_inspector_protocol_encoding4spanIhEEPSt6vectorIhSaIhEE:
.LFB4914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE4914:
	.size	_GLOBAL__sub_I__ZN12v8_inspector17ConvertCBORToJSONEN30v8_inspector_protocol_encoding4spanIhEEPSt6vectorIhSaIhEE, .-_GLOBAL__sub_I__ZN12v8_inspector17ConvertCBORToJSONEN30v8_inspector_protocol_encoding4spanIhEEPSt6vectorIhSaIhEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN12v8_inspector17ConvertCBORToJSONEN30v8_inspector_protocol_encoding4spanIhEEPSt6vectorIhSaIhEE
	.section	.data.rel.ro.local._ZTVN12v8_inspector12_GLOBAL__N_18PlatformE,"aw"
	.align 8
	.type	_ZTVN12v8_inspector12_GLOBAL__N_18PlatformE, @object
	.size	_ZTVN12v8_inspector12_GLOBAL__N_18PlatformE, 48
_ZTVN12v8_inspector12_GLOBAL__N_18PlatformE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector12_GLOBAL__N_18PlatformD1Ev
	.quad	_ZN12v8_inspector12_GLOBAL__N_18PlatformD0Ev
	.quad	_ZNK12v8_inspector12_GLOBAL__N_18Platform6StrToDEPKcPd
	.quad	_ZNK12v8_inspector12_GLOBAL__N_18Platform6DToStrEd
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
