	.file	"preparser.cc"
	.text
	.section	.text._ZNK2v88internal25PreParserFormalParameters17ValidateDuplicateEPNS0_9PreParserE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal25PreParserFormalParameters17ValidateDuplicateEPNS0_9PreParserE.part.0, @function
_ZNK2v88internal25PreParserFormalParameters17ValidateDuplicateEPNS0_9PreParserE.part.0:
.LFB26748:
	.cfi_startproc
	movq	128(%rdi), %rax
	movb	$1, (%rax)
	movb	$1, 2(%rax)
	movq	200(%rdi), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L1
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L1:
	ret
	.cfi_endproc
.LFE26748:
	.size	_ZNK2v88internal25PreParserFormalParameters17ValidateDuplicateEPNS0_9PreParserE.part.0, .-_ZNK2v88internal25PreParserFormalParameters17ValidateDuplicateEPNS0_9PreParserE.part.0
	.set	_ZNK2v88internal25PreParserFormalParameters18ValidateStrictModeEPNS0_9PreParserE.part.0,_ZNK2v88internal25PreParserFormalParameters17ValidateDuplicateEPNS0_9PreParserE.part.0
	.section	.text._ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE31RecordAsyncArrowParametersErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE.part.0,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParsePrimaryExpressionEv,comdat
	.align 2
	.p2align 4
	.type	_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE31RecordAsyncArrowParametersErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE.part.0, @function
_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE31RecordAsyncArrowParametersErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE.part.0:
.LFB26889:
	.cfi_startproc
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L8:
	cmpb	$0, 72(%rdi)
	je	.L4
	cmpb	$2, 16(%rax)
	je	.L15
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L8
.L4:
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movq	(%rsi), %rcx
	movl	%edx, 84(%rax)
	movq	%rcx, 76(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L8
	ret
	.cfi_endproc
.LFE26889:
	.size	_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE31RecordAsyncArrowParametersErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE.part.0, .-_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE31RecordAsyncArrowParametersErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE.part.0
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0:
.LFB26966:
	.cfi_startproc
	movq	128(%rdi), %rax
	movb	$1, (%rax)
	movb	$1, 2(%rax)
	movq	200(%rdi), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L16
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L16:
	ret
	.cfi_endproc
.LFE26966:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	.section	.text._ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0, @function
_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0:
.LFB26965:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %ecx
	movq	%rsi, %rdx
	xorl	%r8d, %r8d
	sarq	$32, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	128(%rdi), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%rbx), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L18
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L18:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26965:
	.size	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0, .-_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	.section	.rodata._ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%d"
	.section	.text._ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseClassLiteralENS0_19PreParserIdentifierENS0_7Scanner8LocationEbi,comdat
	.p2align 4
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0:
.LFB26970:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$208, %rsp
	.cfi_offset 3, -32
	movq	%r8, -160(%rbp)
	movq	%r9, -152(%rbp)
	testb	%al, %al
	je	.L22
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm5, -64(%rbp)
	movaps	%xmm6, -48(%rbp)
	movaps	%xmm7, -32(%rbp)
.L22:
	movq	%fs:40, %rax
	movq	%rax, -200(%rbp)
	xorl	%eax, %eax
	movq	%rsp, %rax
	cmpq	%rax, %rsp
	je	.L24
.L43:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L43
.L24:
	subq	$32, %rsp
	orq	$0, 24(%rsp)
	movl	$16, %ecx
	movl	$16, %esi
	movl	$1, %edx
	leaq	.LC0(%rip), %r8
	leaq	15(%rsp), %rbx
	leaq	16(%rbp), %rax
	movl	$32, -224(%rbp)
	andq	$-16, %rbx
	movq	%rax, -216(%rbp)
	leaq	-224(%rbp), %r9
	leaq	-192(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, -208(%rbp)
	movl	$48, -220(%rbp)
	call	__vsnprintf_chk@PLT
	leaq	16(%r12), %rcx
	movq	%rcx, (%r12)
	movslq	%eax, %rsi
	cmpl	$1, %eax
	jne	.L26
	movzbl	(%rbx), %eax
	movl	$1, %esi
	movb	%al, 16(%r12)
.L27:
	movq	%rsi, 8(%r12)
	movb	$0, (%rcx,%rsi)
	movq	-200(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L44
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	cmpl	$8, %eax
	jnb	.L28
	testb	$4, %al
	jne	.L45
	testl	%eax, %eax
	je	.L29
	movzbl	(%rbx), %edx
	movb	%dl, 16(%r12)
	testb	$2, %al
	jne	.L46
.L29:
	movq	(%r12), %rcx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L28:
	movq	(%rbx), %rdx
	movq	%rdx, 16(%r12)
	movl	%eax, %edx
	movq	-8(%rbx,%rdx), %rdi
	movq	%rdi, -8(%rcx,%rdx)
	leaq	24(%r12), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	%ecx, %eax
	subq	%rcx, %rbx
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L29
	andl	$-8, %eax
	xorl	%edx, %edx
.L32:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	(%rbx,%rcx), %r8
	movq	%r8, (%rdi,%rcx)
	cmpl	%eax, %edx
	jb	.L32
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L45:
	movl	(%rbx), %edx
	movl	%edx, 16(%r12)
	movl	%eax, %edx
	movl	-4(%rbx,%rdx), %eax
	movl	%eax, -4(%rcx,%rdx)
	jmp	.L29
.L46:
	movl	%eax, %edx
	movzwl	-2(%rbx,%rdx), %eax
	movw	%ax, -2(%rcx,%rdx)
	jmp	.L29
.L44:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26970:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	.section	.text._ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPS3_NS0_15ExpressionScopeIS4_E9ScopeTypeE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPS3_NS0_15ExpressionScopeIS4_E9ScopeTypeE.constprop.0, @function
_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPS3_NS0_15ExpressionScopeIS4_E9ScopeTypeE.constprop.0:
.LFB26967:
	.cfi_startproc
	movq	144(%rsi), %rax
	movq	%rsi, (%rdi)
	movb	$0, 16(%rdi)
	movq	%rax, 8(%rdi)
	testq	%rax, %rax
	je	.L48
	cmpb	$0, 17(%rax)
	setne	17(%rdi)
	movzbl	18(%rax), %eax
.L53:
	movb	%al, 18(%rdi)
	leaq	176(%rsi), %rax
	xorl	%edx, %edx
	movq	%rdi, 144(%rsi)
	movq	%rax, 24(%rdi)
	movq	184(%rsi), %rax
	subq	176(%rsi), %rax
	sarq	$4, %rax
	movq	%rax, %xmm0
	movq	8(%rdi), %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 32(%rdi)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 56(%rdi)
	testq	%rax, %rax
	je	.L51
	cmpb	$2, 16(%rax)
	ja	.L51
	movzbl	72(%rax), %edx
.L51:
	movb	%dl, 72(%rdi)
	movq	$0, 48(%rdi)
	movl	$-1, 56(%rdi)
	movl	$-1, 64(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	movb	$0, 17(%rdi)
	xorl	%eax, %eax
	jmp	.L53
	.cfi_endproc
.LFE26967:
	.size	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPS3_NS0_15ExpressionScopeIS4_E9ScopeTypeE.constprop.0, .-_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPS3_NS0_15ExpressionScopeIS4_E9ScopeTypeE.constprop.0
	.section	.text._ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE8ValidateEi.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE8ValidateEi.part.0, @function
_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE8ValidateEi.part.0:
.LFB26857:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	xorl	%r8d, %r8d
	leaq	56(%rdi,%rsi,8), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rbx
	movl	48(%rdi,%rsi,4), %ecx
	movl	4(%rax), %edx
	movl	(%rax), %esi
	movq	128(%rbx), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%rbx), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L58
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L58:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26857:
	.size	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE8ValidateEi.part.0, .-_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE8ValidateEi.part.0
	.section	.text._ZN2v88internal8Variable16SetMaybeAssignedEv,"axG",@progbits,_ZN2v88internal8Variable16SetMaybeAssignedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8Variable16SetMaybeAssignedEv
	.type	_ZN2v88internal8Variable16SetMaybeAssignedEv, @function
_ZN2v88internal8Variable16SetMaybeAssignedEv:
.LFB18195:
	.cfi_startproc
	endbr64
	movzwl	40(%rdi), %eax
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$1, %dl
	je	.L71
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L63
	testb	$64, %ah
	je	.L74
.L63:
	orb	$64, %ah
	movw	%ax, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movzwl	40(%rbx), %eax
	orb	$64, %ah
	movw	%ax, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18195:
	.size	_ZN2v88internal8Variable16SetMaybeAssignedEv, .-_ZN2v88internal8Variable16SetMaybeAssignedEv
	.section	.text._ZN2v88internal5Scope14RecordEvalCallEv,"axG",@progbits,_ZN2v88internal5Scope14RecordEvalCallEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal5Scope14RecordEvalCallEv
	.type	_ZN2v88internal5Scope14RecordEvalCallEv, @function
_ZN2v88internal5Scope14RecordEvalCallEv:
.LFB19300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	orb	$2, 129(%rdi)
	call	_ZN2v88internal5Scope19GetDeclarationScopeEv@PLT
	movzbl	129(%rax), %ecx
	movl	%ecx, %edx
	orl	$2, %edx
	movb	%dl, 129(%rax)
	andl	$1, %edx
	jne	.L76
	movzbl	128(%rax), %edx
	cmpb	$1, %dl
	je	.L76
	cmpb	$4, %dl
	je	.L76
	orl	$6, %ecx
	movb	%cl, 129(%rax)
.L76:
	movq	8(%rbx), %rax
	orb	$64, 129(%rbx)
	testq	%rax, %rax
	jne	.L78
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L84:
	orl	$64, %edx
	movb	%dl, 129(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L75
.L78:
	movzbl	129(%rax), %edx
	testb	$64, %dl
	je	.L84
.L75:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19300:
	.size	_ZN2v88internal5Scope14RecordEvalCallEv, .-_ZN2v88internal5Scope14RecordEvalCallEv
	.section	.text._ZN2v88internal9PreParser19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPNS0_5ScopeEPbiNS0_12VariableKindE,"axG",@progbits,_ZN2v88internal9PreParser19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPNS0_5ScopeEPbiNS0_12VariableKindE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9PreParser19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPNS0_5ScopeEPbiNS0_12VariableKindE
	.type	_ZN2v88internal9PreParser19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPNS0_5ScopeEPbiNS0_12VariableKindE, @function
_ZN2v88internal9PreParser19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPNS0_5ScopeEPbiNS0_12VariableKindE:
.LFB20637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movzbl	16(%rbp), %r8d
	movl	%r9d, -52(%rbp)
	call	_ZN2v88internal5Scope19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPbNS0_12VariableKindE@PLT
	testq	%rax, %rax
	je	.L93
	movq	%rax, %r13
	cmpq	(%rax), %r12
	je	.L85
	movq	48(%r15), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L94
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L91:
	movl	-52(%rbp), %ebx
	movq	$0, 16(%rax)
	movl	$64, 4(%rax)
	movl	%ebx, (%rax)
	movq	%r12, 24(%rax)
	movq	%r13, 8(%rax)
	movq	0(%r13), %rdx
	movq	96(%rdx), %rcx
	movq	%rax, (%rcx)
	addq	$16, %rax
	movq	%rax, 96(%rdx)
.L85:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	movq	128(%r15), %rax
	movb	$1, (%rax)
	movb	$1, 2(%rax)
	movq	200(%r15), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L87
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L87:
	cmpb	$1, %bl
	ja	.L95
.L88:
	addq	$24, %rsp
	leaq	32(%r12), %rdi
	movq	%r14, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11VariableMap6LookupEPKNS0_12AstRawStringE@PLT
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal5Scope19GetDeclarationScopeEv@PLT
	movq	%rax, %r12
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L94:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L91
	.cfi_endproc
.LFE20637:
	.size	_ZN2v88internal9PreParser19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPNS0_5ScopeEPbiNS0_12VariableKindE, .-_ZN2v88internal9PreParser19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPNS0_5ScopeEPbiNS0_12VariableKindE
	.section	.text._ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc,"axG",@progbits,_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc
	.type	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc, @function
_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc:
.LFB20693:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r9d
	movq	%rsi, %rdx
	movq	%rcx, %r8
	sarq	$32, %rdx
	movl	%r9d, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	128(%rdi), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%rbx), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L96
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L96:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20693:
	.size	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc, .-_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc
	.section	.text._ZNK2v88internal9PreParser13GetIdentifierEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9PreParser13GetIdentifierEv
	.type	_ZNK2v88internal9PreParser13GetIdentifierEv, @function
_ZNK2v88internal9PreParser13GetIdentifierEv:
.LFB20766:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	40(%rdi), %rsi
	movq	200(%rdi), %rdi
	call	_ZNK2v88internal7Scanner13CurrentSymbolEPNS0_15AstValueFactoryE@PLT
	movq	200(%rbx), %rdx
	movq	(%rdx), %rsi
	movl	$5, %edx
	movzbl	56(%rsi), %ecx
	cmpb	$96, %cl
	je	.L100
	movl	$8, %edx
	cmpb	$108, %cl
	je	.L100
	movl	$6, %edx
	cmpb	$95, %cl
	je	.L100
	movq	40(%rbx), %rdx
	movq	56(%rdx), %r8
	movl	$4, %edx
	cmpq	176(%r8), %rax
	je	.L100
	movl	$7, %edx
	cmpq	344(%r8), %rax
	je	.L100
	movl	4(%rsi), %edi
	subl	(%rsi), %edi
	cmpb	$90, %cl
	leal	-2(%rdi), %edx
	movl	24(%rsi), %ecx
	cmove	%edx, %edi
	cmpb	$0, 28(%rsi)
	jne	.L102
	sarl	%ecx
.L102:
	movl	$1, %edx
	cmpl	%ecx, %edi
	je	.L115
.L100:
	xorl	%r8d, %r8d
	addq	$8, %rsp
	movb	%dl, %r8b
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%r8, %rdx
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	movl	$2, %edx
	cmpq	280(%r8), %rax
	je	.L100
	cmpq	112(%r8), %rax
	sete	%dl
	leal	1(%rdx,%rdx), %edx
	jmp	.L100
	.cfi_endproc
.LFE20766:
	.size	_ZNK2v88internal9PreParser13GetIdentifierEv, .-_ZNK2v88internal9PreParser13GetIdentifierEv
	.section	.text._ZNK2v88internal25PreParserFormalParameters17ValidateDuplicateEPNS0_9PreParserE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal25PreParserFormalParameters17ValidateDuplicateEPNS0_9PreParserE
	.type	_ZNK2v88internal25PreParserFormalParameters17ValidateDuplicateEPNS0_9PreParserE, @function
_ZNK2v88internal25PreParserFormalParameters17ValidateDuplicateEPNS0_9PreParserE:
.LFB20768:
	.cfi_startproc
	endbr64
	cmpb	$0, 20(%rdi)
	jne	.L120
.L116:
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	movq	128(%rsi), %rax
	movb	$1, (%rax)
	movb	$1, 2(%rax)
	movq	200(%rsi), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L116
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	ret
	.cfi_endproc
.LFE20768:
	.size	_ZNK2v88internal25PreParserFormalParameters17ValidateDuplicateEPNS0_9PreParserE, .-_ZNK2v88internal25PreParserFormalParameters17ValidateDuplicateEPNS0_9PreParserE
	.section	.text._ZNK2v88internal25PreParserFormalParameters18ValidateStrictModeEPNS0_9PreParserE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal25PreParserFormalParameters18ValidateStrictModeEPNS0_9PreParserE
	.type	_ZNK2v88internal25PreParserFormalParameters18ValidateStrictModeEPNS0_9PreParserE, @function
_ZNK2v88internal25PreParserFormalParameters18ValidateStrictModeEPNS0_9PreParserE:
.LFB20769:
	.cfi_startproc
	endbr64
	cmpb	$0, 21(%rdi)
	jne	.L125
.L121:
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	movq	128(%rsi), %rax
	movb	$1, (%rax)
	movb	$1, 2(%rax)
	movq	200(%rsi), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L121
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	ret
	.cfi_endproc
.LFE20769:
	.size	_ZNK2v88internal25PreParserFormalParameters18ValidateStrictModeEPNS0_9PreParserE, .-_ZNK2v88internal25PreParserFormalParameters18ValidateStrictModeEPNS0_9PreParserE
	.section	.text._ZN2v88internal9PreParser33BuildParameterInitializationBlockERKNS0_25PreParserFormalParametersE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9PreParser33BuildParameterInitializationBlockERKNS0_25PreParserFormalParametersE
	.type	_ZN2v88internal9PreParser33BuildParameterInitializationBlockERKNS0_25PreParserFormalParametersE, @function
_ZN2v88internal9PreParser33BuildParameterInitializationBlockERKNS0_25PreParserFormalParametersE:
.LFB20776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	testb	$4, 129(%rax)
	je	.L127
	movq	288(%rbx), %rax
	testq	%rax, %rax
	je	.L127
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L128
	orb	$1, 76(%rdx)
.L127:
	addq	$8, %rsp
	movl	$2, %eax
	xorl	%edx, %edx
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	orb	$1, 76(%rax)
	addq	$8, %rsp
	movl	$2, %eax
	xorl	%edx, %edx
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20776:
	.size	_ZN2v88internal9PreParser33BuildParameterInitializationBlockERKNS0_25PreParserFormalParametersE, .-_ZN2v88internal9PreParser33BuildParameterInitializationBlockERKNS0_25PreParserFormalParametersE
	.section	.text._ZN2v88internal9PreParser16IdentifierEqualsERKNS0_19PreParserIdentifierEPKNS0_12AstRawStringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9PreParser16IdentifierEqualsERKNS0_19PreParserIdentifierEPKNS0_12AstRawStringE
	.type	_ZN2v88internal9PreParser16IdentifierEqualsERKNS0_19PreParserIdentifierEPKNS0_12AstRawStringE, @function
_ZN2v88internal9PreParser16IdentifierEqualsERKNS0_19PreParserIdentifierEPKNS0_12AstRawStringE:
.LFB20777:
	.cfi_startproc
	endbr64
	cmpq	%rdx, (%rsi)
	sete	%al
	ret
	.cfi_endproc
.LFE20777:
	.size	_ZN2v88internal9PreParser16IdentifierEqualsERKNS0_19PreParserIdentifierEPKNS0_12AstRawStringE, .-_ZN2v88internal9PreParser16IdentifierEqualsERKNS0_19PreParserIdentifierEPKNS0_12AstRawStringE
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE6ExpectENS0_5Token5ValueE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE6ExpectENS0_5Token5ValueE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE6ExpectENS0_5Token5ValueE
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE6ExpectENS0_5Token5ValueE, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE6ExpectENS0_5Token5ValueE:
.LFB23021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	200(%rdi), %rdi
	movl	%esi, %ebx
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	%bl, %al
	jne	.L140
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	.cfi_endproc
.LFE23021:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE6ExpectENS0_5Token5ValueE, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE6ExpectENS0_5Token5ValueE
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE:
.LFB24338:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	200(%rdi), %rax
	movq	128(%rdi), %rdi
	movq	(%rax), %rax
	movl	4(%rax), %edx
	movl	(%rax), %esi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%rbx), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L141
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L141:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24338:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	.section	.text._ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE30RecordStrictModeParameterErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE,"axG",@progbits,_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE30RecordStrictModeParameterErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE30RecordStrictModeParameterErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE
	.type	_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE30RecordStrictModeParameterErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE, @function
_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE30RecordStrictModeParameterErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE:
.LFB25557:
	.cfi_startproc
	endbr64
	movzbl	16(%rdi), %eax
	leal	-1(%rax), %ecx
	cmpb	$2, %cl
	ja	.L150
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rbx
	cmpb	$3, %al
	je	.L153
	movq	(%rsi), %rax
	movq	%rax, 232(%rbx)
	movq	(%rdi), %rax
	movl	%edx, 240(%rax)
.L144:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	movq	(%rbx), %rax
	movl	(%rsi), %r10d
	movl	4(%rsi), %r9d
	testb	$1, 129(%rax)
	je	.L147
	movq	128(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	%edx, %ecx
	movl	%r10d, %esi
	movl	%r9d, %edx
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%rbx), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L144
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	224(%rbx), %rax
	cmpl	%r9d, %r10d
	setbe	21(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25557:
	.size	_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE30RecordStrictModeParameterErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE, .-_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE30RecordStrictModeParameterErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE
	.section	.text._ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE18RecordPatternErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE,"axG",@progbits,_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE18RecordPatternErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE18RecordPatternErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE
	.type	_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE18RecordPatternErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE, @function
_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE18RecordPatternErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE:
.LFB25571:
	.cfi_startproc
	endbr64
	movzbl	16(%rdi), %eax
	subl	$3, %eax
	cmpb	$2, %al
	jbe	.L162
	movl	68(%rdi), %eax
	cmpl	%eax, 64(%rdi)
	jbe	.L159
	movl	%edx, 52(%rdi)
	movq	(%rsi), %rax
	movq	%rax, 64(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movl	%edx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rbx
	movl	4(%rsi), %r9d
	movl	(%rsi), %esi
	movq	128(%rbx), %rdi
	movl	%r9d, %edx
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%rbx), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L154
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L154:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25571:
	.size	_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE18RecordPatternErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE, .-_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE18RecordPatternErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateE
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateE, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateE:
.LFB25841:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %ecx
	movq	%rsi, %rdx
	xorl	%r8d, %r8d
	sarq	$32, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	128(%rdi), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%rbx), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L163
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L163:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25841:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateE, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateE
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE15ExpectSemicolonEv,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE15ExpectSemicolonEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE15ExpectSemicolonEv
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE15ExpectSemicolonEv, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE15ExpectSemicolonEv:
.LFB25529:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	200(%rdi), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %edx
	cmpb	$12, %dl
	jne	.L167
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Scanner4NextEv@PLT
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	cmpb	$0, 76(%rax)
	je	.L174
.L166:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L174:
	.cfi_restore_state
	subl	$12, %edx
	cmpb	$2, %dl
	jbe	.L166
	movq	(%rdi), %rax
	cmpb	$96, 56(%rax)
	je	.L169
.L170:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	popq	%rdx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
.L169:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	48(%rax), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$9, %eax
	cmpb	$4, %al
	ja	.L172
	movq	200(%r12), %rdi
	jmp	.L170
.L172:
	movq	200(%r12), %rax
	movq	%r12, %rdi
	movl	$16, %edx
	movq	(%rax), %rax
	movq	(%rax), %rsi
	popq	%rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10ParserBaseINS0_9PreParserEE15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateE
	.cfi_endproc
.LFE25529:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE15ExpectSemicolonEv, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE15ExpectSemicolonEv
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE33RewriteInvalidReferenceExpressionENS0_19PreParserExpressionEiiNS0_15MessageTemplateE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE33RewriteInvalidReferenceExpressionENS0_19PreParserExpressionEiiNS0_15MessageTemplateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE33RewriteInvalidReferenceExpressionENS0_19PreParserExpressionEiiNS0_15MessageTemplateE
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE33RewriteInvalidReferenceExpressionENS0_19PreParserExpressionEiiNS0_15MessageTemplateE, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE33RewriteInvalidReferenceExpressionENS0_19PreParserExpressionEiiNS0_15MessageTemplateE:
.LFB25569:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	andl	$7, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpl	$3, %eax
	je	.L191
	cmpl	$2, %eax
	je	.L192
.L178:
	salq	$32, %rcx
	movl	%edx, %edx
	movq	%r12, %rdi
	orq	%rcx, %rdx
	movq	%rdx, %rsi
	movl	%r8d, %edx
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateE
	movl	$1, %eax
.L177:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	shrl	$4, %esi
	movl	%esi, %ebx
	andl	$15, %ebx
	leal	-5(%rbx), %eax
	cmpl	$1, %eax
	ja	.L178
	movq	144(%rdi), %rax
	movzbl	16(%rax), %edi
	leal	-3(%rdi), %esi
	cmpb	$2, %sil
	jbe	.L193
	movl	68(%rax), %edi
	cmpl	%edi, 64(%rax)
	jbe	.L181
	movl	$252, 52(%rax)
	movl	%edx, 64(%rax)
	movl	%ecx, 68(%rax)
.L181:
	movq	(%r12), %rax
	movq	264(%r12), %rdx
	movzbl	129(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbq	%rax, %rax
	andq	$-4, %rax
	addq	$148, %rax
	testq	%rdx, %rdx
	je	.L183
	addl	$1, (%rdx,%rax)
.L183:
	cmpl	$1, %ebx
	sbbl	%eax, %eax
	andl	$-32, %eax
	addl	$50, %eax
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L191:
	movl	%edx, %edx
	salq	$32, %rcx
	movq	%rdx, %rsi
	movl	$295, %edx
	orq	%rcx, %rsi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateE
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	movl	%edx, %edx
	salq	$32, %rcx
	movq	(%rax), %rdi
	movq	%rdx, %rsi
	movl	$252, %edx
	orq	%rcx, %rsi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	jmp	.L181
	.cfi_endproc
.LFE25569:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE33RewriteInvalidReferenceExpressionENS0_19PreParserExpressionEiiNS0_15MessageTemplateE, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE33RewriteInvalidReferenceExpressionENS0_19PreParserExpressionEiiNS0_15MessageTemplateE
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseContinueStatementEv,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseContinueStatementEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseContinueStatementEv
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseContinueStatementEv, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseContinueStatementEv:
.LFB25539:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	200(%rdi), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	cmpb	$0, 76(%rax)
	movzbl	56(%rax), %edx
	jne	.L196
	leal	-12(%rdx), %eax
	cmpb	$2, %al
	ja	.L228
.L196:
	cmpb	$12, %dl
	jne	.L215
.L210:
	call	_ZN2v88internal7Scanner4NextEv@PLT
.L215:
	popq	%rbx
	movl	$3, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	48(%rax), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movq	200(%r12), %rdi
	movzbl	133(%rax), %ebx
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$0, 113(%r12)
	je	.L197
	leal	-92(%rax), %edx
	cmpb	$3, %dl
	ja	.L229
.L198:
	movq	%r12, %rdi
	call	_ZNK2v88internal9PreParser13GetIdentifierEv
.L203:
	movq	200(%r12), %r8
	movq	8(%r8), %rax
	movq	%r8, %rdi
	movzbl	56(%rax), %edx
	cmpb	$12, %dl
	je	.L210
	cmpb	$0, 76(%rax)
	jne	.L215
	subl	$12, %edx
	cmpb	$2, %dl
	jbe	.L215
	movq	(%r8), %rax
	cmpb	$96, 56(%rax)
	je	.L208
.L209:
	movq	%r8, %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L197:
	leal	-92(%rax), %edx
	leal	-9(%rbx), %ecx
	cmpb	$3, %dl
	jbe	.L198
	movq	(%r12), %rdx
	movzbl	129(%rdx), %edx
	andl	$1, %edx
	cmpb	$96, %al
	je	.L230
.L200:
	cmpb	$97, %al
	jne	.L201
	subl	$12, %ebx
	cmpb	$3, %bl
	jbe	.L199
.L227:
	testb	%dl, %dl
	je	.L198
.L199:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L203
.L229:
	movq	(%r12), %rdx
	movzbl	129(%rdx), %edx
	andl	$1, %edx
	cmpb	$96, %al
	jne	.L200
	jmp	.L199
.L201:
	subl	$97, %eax
	cmpb	$4, %al
	jbe	.L227
	jmp	.L199
.L230:
	cmpb	$4, %cl
	ja	.L198
	jmp	.L199
.L208:
	movq	16(%r12), %rax
	movq	48(%rax), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$9, %eax
	cmpb	$4, %al
	ja	.L225
	movq	200(%r12), %r8
	jmp	.L209
.L225:
	movq	200(%r12), %rax
	movl	$16, %edx
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateE
	jmp	.L215
	.cfi_endproc
.LFE25539:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseContinueStatementEv, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseContinueStatementEv
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseBreakStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseBreakStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseBreakStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseBreakStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseBreakStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE:
.LFB25540:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	200(%rdi), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	cmpb	$0, 76(%rax)
	movzbl	56(%rax), %edx
	jne	.L233
	leal	-12(%rdx), %eax
	cmpb	$2, %al
	ja	.L265
.L233:
	cmpb	$12, %dl
	jne	.L252
.L247:
	call	_ZN2v88internal7Scanner4NextEv@PLT
.L252:
	popq	%rbx
	movl	$3, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	48(%rax), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movq	200(%r12), %rdi
	movzbl	133(%rax), %ebx
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$0, 113(%r12)
	je	.L234
	leal	-92(%rax), %edx
	cmpb	$3, %dl
	ja	.L266
.L235:
	movq	%r12, %rdi
	call	_ZNK2v88internal9PreParser13GetIdentifierEv
.L240:
	movq	200(%r12), %r8
	movq	8(%r8), %rax
	movq	%r8, %rdi
	movzbl	56(%rax), %edx
	cmpb	$12, %dl
	je	.L247
	cmpb	$0, 76(%rax)
	jne	.L252
	subl	$12, %edx
	cmpb	$2, %dl
	jbe	.L252
	movq	(%r8), %rax
	cmpb	$96, 56(%rax)
	je	.L245
.L246:
	movq	%r8, %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L234:
	leal	-92(%rax), %edx
	leal	-9(%rbx), %ecx
	cmpb	$3, %dl
	jbe	.L235
	movq	(%r12), %rdx
	movzbl	129(%rdx), %edx
	andl	$1, %edx
	cmpb	$96, %al
	je	.L267
.L237:
	cmpb	$97, %al
	jne	.L238
	subl	$12, %ebx
	cmpb	$3, %bl
	jbe	.L236
.L264:
	testb	%dl, %dl
	je	.L235
.L236:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L240
.L266:
	movq	(%r12), %rdx
	movzbl	129(%rdx), %edx
	andl	$1, %edx
	cmpb	$96, %al
	jne	.L237
	jmp	.L236
.L238:
	subl	$97, %eax
	cmpb	$4, %al
	jbe	.L264
	jmp	.L236
.L267:
	cmpb	$4, %cl
	ja	.L235
	jmp	.L236
.L245:
	movq	16(%r12), %rax
	movq	48(%rax), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$9, %eax
	cmpb	$4, %al
	ja	.L262
	movq	200(%r12), %r8
	jmp	.L246
.L262:
	movq	200(%r12), %rax
	movl	$16, %edx
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateE
	jmp	.L252
	.cfi_endproc
.LFE25540:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseBreakStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseBreakStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE10BlockStateC2EPNS0_4ZoneEPPNS0_5ScopeE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE10BlockStateC5EPNS0_4ZoneEPPNS0_5ScopeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE10BlockStateC2EPNS0_4ZoneEPPNS0_5ScopeE
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE10BlockStateC2EPNS0_4ZoneEPPNS0_5ScopeE, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE10BlockStateC2EPNS0_4ZoneEPPNS0_5ScopeE:
.LFB25843:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	16(%rsi), %r13
	movq	%rdx, %rbx
	movq	24(%rsi), %rax
	subq	%r13, %rax
	cmpq	$135, %rax
	jbe	.L272
	leaq	136(%r13), %rax
	movq	%rax, 16(%rsi)
.L270:
	movq	(%rbx), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$6, %ecx
	call	_ZN2v88internal5ScopeC1EPNS0_4ZoneEPS1_NS0_9ScopeTypeE@PLT
	movq	(%rbx), %rax
	movq	%rbx, (%r14)
	movq	%r13, (%rbx)
	popq	%rbx
	movq	%rax, 8(%r14)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	.cfi_restore_state
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L270
	.cfi_endproc
.LFE25843:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE10BlockStateC2EPNS0_4ZoneEPPNS0_5ScopeE, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE10BlockStateC2EPNS0_4ZoneEPPNS0_5ScopeE
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE10BlockStateC1EPNS0_4ZoneEPPNS0_5ScopeE
	.set	_ZN2v88internal10ParserBaseINS0_9PreParserEE10BlockStateC1EPNS0_4ZoneEPPNS0_5ScopeE,_ZN2v88internal10ParserBaseINS0_9PreParserEE10BlockStateC2EPNS0_4ZoneEPPNS0_5ScopeE
	.section	.text._ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE15ValidatePatternENS0_19PreParserExpressionEii,"axG",@progbits,_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE15ValidatePatternENS0_19PreParserExpressionEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE15ValidatePatternENS0_19PreParserExpressionEii
	.type	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE15ValidatePatternENS0_19PreParserExpressionEii, @function
_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE15ValidatePatternENS0_19PreParserExpressionEii:
.LFB25868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	64(%rdi), %esi
	movl	68(%rdi), %edx
	cmpl	%edx, %esi
	jbe	.L325
.L274:
	andl	$8, %r12d
	jne	.L326
.L276:
	movq	24(%rbx), %rax
	movq	40(%rbx), %r12
	movq	(%rax), %rdx
	movq	32(%rbx), %rax
	salq	$4, %r12
	salq	$4, %rax
	addq	%rdx, %r12
	leaq	(%rdx,%rax), %rbx
	cmpq	%rbx, %r12
	jne	.L286
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L279:
	addq	$16, %rbx
	cmpq	%rbx, %r12
	je	.L273
.L286:
	movq	(%rbx), %rax
	movl	4(%rax), %edx
	movl	%edx, %ecx
	orb	$-128, %cl
	movl	%ecx, 4(%rax)
	andb	$1, %dh
	je	.L279
	movq	8(%rax), %r13
	movzwl	40(%r13), %eax
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$1, %dl
	je	.L279
	movq	16(%r13), %r14
	testq	%r14, %r14
	je	.L280
	testb	$64, %ah
	je	.L327
.L280:
	orb	$64, %ah
	addq	$16, %rbx
	movw	%ax, 40(%r13)
	cmpq	%rbx, %r12
	jne	.L286
.L273:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	.cfi_restore_state
	movzwl	40(%r14), %edx
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L280
	movq	16(%r14), %r15
	testq	%r15, %r15
	je	.L281
	testb	$64, %dh
	je	.L328
.L281:
	orb	$64, %dh
	movw	%dx, 40(%r14)
	movzwl	40(%r13), %eax
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L326:
	movq	(%rbx), %r12
	xorl	%r8d, %r8d
	movl	%r13d, %edx
	movl	$252, %ecx
	movl	%r14d, %esi
	movq	128(%r12), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r12), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L276
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L325:
	movq	(%rdi), %r15
	movl	52(%rdi), %ecx
	xorl	%r8d, %r8d
	movq	128(%r15), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r15), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L274
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L274
.L328:
	movzwl	40(%r15), %eax
	movl	%eax, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L281
	movq	16(%r15), %rdx
	testq	%rdx, %rdx
	je	.L282
	testb	$64, %ah
	jne	.L282
	movzwl	40(%rdx), %ecx
	movl	%ecx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L282
	movq	16(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L283
	testb	$64, %ch
	je	.L329
.L283:
	orb	$64, %ch
	movw	%cx, 40(%rdx)
	movzwl	40(%r15), %eax
.L282:
	orb	$64, %ah
	movw	%ax, 40(%r15)
	movzwl	40(%r14), %edx
	jmp	.L281
.L329:
	movzwl	40(%rsi), %eax
	movl	%eax, %edi
	andl	$15, %edi
	cmpb	$1, %dil
	je	.L283
	movq	16(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L284
	testb	$64, %ah
	je	.L330
.L284:
	orb	$64, %ah
	movw	%ax, 40(%rsi)
	movzwl	40(%rdx), %ecx
	jmp	.L283
.L330:
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdx
	movzwl	40(%rsi), %eax
	jmp	.L284
	.cfi_endproc
.LFE25868:
	.size	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE15ValidatePatternENS0_19PreParserExpressionEii, .-_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE15ValidatePatternENS0_19PreParserExpressionEii
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseNonRestrictedIdentifierEv,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseNonRestrictedIdentifierEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseNonRestrictedIdentifierEv
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseNonRestrictedIdentifierEv, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseNonRestrictedIdentifierEv:
.LFB25894:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	16(%rdi), %rax
	movq	48(%rax), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movq	200(%r12), %rdi
	movzbl	133(%rax), %ebx
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$0, 113(%r12)
	je	.L332
	leal	-92(%rax), %edx
	cmpb	$3, %dl
	ja	.L350
.L333:
	movq	%r12, %rdi
	call	_ZNK2v88internal9PreParser13GetIdentifierEv
	movq	(%r12), %rcx
	movl	%edx, %ebx
	testb	$1, 129(%rcx)
	je	.L338
	subl	$2, %edx
	cmpb	$1, %dl
	jbe	.L351
.L338:
	addq	$16, %rsp
	xorl	%edx, %edx
	movb	%bl, %dl
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore_state
	leal	-92(%rax), %edx
	leal	-9(%rbx), %ecx
	cmpb	$3, %dl
	jbe	.L333
	movq	(%r12), %rdx
	movzbl	129(%rdx), %edx
	andl	$1, %edx
	cmpb	$96, %al
	jne	.L335
	cmpb	$4, %cl
	ja	.L333
	.p2align 4,,10
	.p2align 3
.L334:
	movq	%r12, %rdi
	movl	$1, %ebx
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	movq	40(%r12), %rax
	movq	56(%rax), %rax
	movq	272(%rax), %rax
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L350:
	movq	(%r12), %rdx
	movzbl	129(%rdx), %edx
	andl	$1, %edx
	cmpb	$96, %al
	je	.L334
.L335:
	cmpb	$97, %al
	jne	.L336
	subl	$12, %ebx
	cmpb	$3, %bl
	jbe	.L334
.L349:
	testb	%dl, %dl
	jne	.L334
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L351:
	movq	200(%r12), %rdx
	movq	%r12, %rdi
	movq	%rax, -24(%rbp)
	movq	(%rdx), %rcx
	movl	$295, %edx
	movq	(%rcx), %rsi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	movq	-24(%rbp), %rax
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L336:
	subl	$97, %eax
	cmpb	$4, %al
	jbe	.L349
	jmp	.L334
	.cfi_endproc
.LFE25894:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseNonRestrictedIdentifierEv, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseNonRestrictedIdentifierEv
	.section	.rodata._ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC1:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB25904:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movabsq	$576460752303423487, %rdi
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$4, %rax
	cmpq	%rdi, %rax
	je	.L371
	movq	%rsi, %rcx
	movq	%rsi, %rbx
	subq	%r15, %rcx
	testq	%rax, %rax
	je	.L362
	movabsq	$9223372036854775792, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L372
.L354:
	movq	%rsi, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	%rax, %r14
	movq	-72(%rbp), %rdx
	leaq	(%rax,%rsi), %rax
	leaq	16(%r14), %r8
.L361:
	movl	8(%rdx), %esi
	movq	(%rdx), %rdx
	addq	%r14, %rcx
	movq	%rdx, (%rcx)
	movl	%esi, 8(%rcx)
	cmpq	%r15, %rbx
	je	.L356
	movq	%r14, %rcx
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L357:
	movq	(%rdx), %rdi
	movl	8(%rdx), %esi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	%rdi, -16(%rcx)
	movl	%esi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L357
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	16(%r14,%rdx), %r8
.L356:
	cmpq	%r12, %rbx
	je	.L358
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L359:
	movl	8(%rdx), %esi
	movq	(%rdx), %rdi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	%rdi, -16(%rcx)
	movl	%esi, -8(%rcx)
	cmpq	%r12, %rdx
	jne	.L359
	subq	%rbx, %rdx
	addq	%rdx, %r8
.L358:
	testq	%r15, %r15
	je	.L360
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
.L360:
	movq	%r14, %xmm0
	movq	%r8, %xmm1
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L372:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L355
	movl	$16, %r8d
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L362:
	movl	$16, %esi
	jmp	.L354
.L355:
	cmpq	%rdi, %r8
	movq	%rdi, %rax
	cmovbe	%r8, %rax
	salq	$4, %rax
	movq	%rax, %rsi
	jmp	.L354
.L371:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE25904:
	.size	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text._ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE11NewVariableEPKNS0_12AstRawStringEi,"axG",@progbits,_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE11NewVariableEPKNS0_12AstRawStringEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE11NewVariableEPKNS0_12AstRawStringEi
	.type	_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE11NewVariableEPKNS0_12AstRawStringEi, @function
_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE11NewVariableEPKNS0_12AstRawStringEi:
.LFB23001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	48(%rax), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$23, %rax
	jbe	.L435
	leaq	24(%r12), %rax
	movq	%rax, 16(%rdi)
.L375:
	movl	%r14d, (%r12)
	movq	%r13, 8(%r12)
	movq	$0, 16(%r12)
	movl	$2102, 4(%r12)
	movzbl	16(%rbx), %eax
	cmpb	$2, %al
	ja	.L376
	subl	$1, %eax
	cmpb	$4, %al
	ja	.L436
.L377:
	movq	24(%rbx), %rdi
	movq	%r12, -80(%rbp)
	movl	$-1, -72(%rbp)
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L378
	movq	%r12, (%rsi)
	movl	$-1, 8(%rsi)
	addq	$16, 8(%rdi)
.L379:
	addq	$1, 40(%rbx)
.L373:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L437
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L376:
	.cfi_restore_state
	movq	(%rbx), %r10
	movq	(%r10), %r9
	cmpb	$3, %al
	je	.L438
	movzbl	19(%rbx), %edx
	xorl	%r8d, %r8d
	movq	%r9, %rdi
	leaq	-80(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r10, -104(%rbp)
	movb	%dl, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal5Scope19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPbNS0_12VariableKindE@PLT
	movq	-88(%rbp), %r9
	movzbl	-96(%rbp), %edx
	testq	%rax, %rax
	movq	-104(%rbp), %r10
	movq	%rax, %r15
	je	.L439
	cmpq	%r9, (%rax)
	je	.L393
	movq	48(%r10), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L440
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L395:
	movl	%r14d, (%rax)
	movq	$0, 16(%rax)
	movl	$64, 4(%rax)
	movq	%r9, 24(%rax)
	movq	%r15, 8(%rax)
	movq	(%r15), %rdx
	movq	96(%rdx), %rcx
	movq	%rax, (%rcx)
	addq	$16, %rax
	movq	%rax, 96(%rdx)
.L393:
	cmpb	$0, -80(%rbp)
	movq	(%rbx), %r9
	je	.L396
	movq	(%r9), %rax
	cmpl	$8388607, 44(%rax)
	jle	.L396
	movq	%r9, %rdi
	movl	$309, %esi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	movq	(%rbx), %r9
	.p2align 4,,10
	.p2align 3
.L396:
	movq	24(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L397
	movslq	12(%rcx), %rax
	movl	8(%rcx), %edx
	cmpl	%edx, %eax
	jge	.L398
	movq	(%rcx), %rdx
	leal	1(%rax), %esi
	movl	%esi, 12(%rcx)
	movq	%r13, (%rdx,%rax,8)
	movq	(%rbx), %r9
.L397:
	movzbl	16(%rbx), %eax
	cmpb	$5, %al
	je	.L441
	movq	16(%r9), %rdx
	movl	24(%rdx), %edx
	testl	%edx, %edx
	jg	.L442
.L389:
	cmpb	$4, %al
	jne	.L404
	movq	(%rbx), %rax
	movq	(%rax), %rdi
	testb	$1, 130(%rdi)
	je	.L443
.L404:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13VariableProxy6BindToEPNS0_8VariableE@PLT
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L436:
	movq	(%rbx), %rax
	movq	%r12, %rsi
	movq	(%rax), %rdi
	call	_ZN2v88internal5Scope13AddUnresolvedEPNS0_13VariableProxyE@PLT
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L442:
	movzwl	40(%r15), %edx
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L389
	movq	16(%r15), %r13
	testq	%r13, %r13
	je	.L408
	testb	$64, %dh
	jne	.L408
	movzwl	40(%r13), %eax
	movl	%eax, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L408
	movq	16(%r13), %r14
	testq	%r14, %r14
	je	.L409
	testb	$64, %ah
	je	.L444
.L409:
	orb	$64, %ah
	movw	%ax, 40(%r13)
	movzwl	40(%r15), %edx
	.p2align 4,,10
	.p2align 3
.L408:
	orb	$64, %dh
	movw	%dx, 40(%r15)
.L434:
	movzbl	16(%rbx), %eax
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L443:
	movq	%r12, %rsi
	call	_ZN2v88internal5Scope13AddUnresolvedEPNS0_13VariableProxyE@PLT
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L438:
	movq	%r9, %rdi
	leaq	-80(%rbp), %rcx
	movl	$1, %r8d
	movq	%r13, %rsi
	movl	$2, %edx
	movq	%r10, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal5Scope19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPbNS0_12VariableKindE@PLT
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L445
	cmpq	%r9, (%rax)
	je	.L384
	movq	48(%r10), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L446
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L386:
	movl	%r14d, (%rax)
	movq	$0, 16(%rax)
	movl	$64, 4(%rax)
	movq	%r9, 24(%rax)
	movq	%r15, 8(%rax)
	movq	(%r15), %rdx
	movq	96(%rdx), %rcx
	movq	%rax, (%rcx)
	addq	$16, %rax
	movq	%rax, 96(%rdx)
.L384:
	movl	24(%rbx), %eax
	cmpl	%eax, 20(%rbx)
	jbe	.L434
	cmpb	$0, -80(%rbp)
	jne	.L434
	movq	16(%r13), %rdx
	cmpb	$0, 28(%r13)
	movl	%edx, %eax
	jne	.L388
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
.L388:
	movl	%r14d, 20(%rbx)
	addl	%eax, %r14d
	movl	%r14d, 24(%rbx)
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L441:
	movq	40(%r9), %rax
	movq	56(%rax), %rax
	cmpq	328(%rax), %r13
	jne	.L404
	movq	16(%r13), %rax
	cmpb	$0, 28(%r13)
	movl	%eax, %edx
	jne	.L405
	shrl	$31, %edx
	addl	%eax, %edx
	sarl	%edx
.L405:
	movq	128(%r9), %rdi
	addl	%r14d, %edx
	xorl	%r8d, %r8d
	movl	%r14d, %esi
	movl	$207, %ecx
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	-88(%rbp), %r9
	movq	200(%r9), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L434
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L435:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L439:
	movq	128(%r10), %rax
	movb	$1, (%rax)
	movb	$1, 2(%rax)
	movq	200(%r10), %rax
	movq	24(%rax), %rcx
	cmpb	$0, 48(%rcx)
	je	.L447
.L391:
	cmpb	$1, %dl
	jbe	.L392
	movq	%r9, %rdi
	call	_ZN2v88internal5Scope19GetDeclarationScopeEv@PLT
	movq	%rax, %r9
.L392:
	leaq	32(%r9), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal11VariableMap6LookupEPKNS0_12AstRawStringE@PLT
	movq	%rax, %r15
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L378:
	leaq	-80(%rbp), %rdx
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L398:
	movq	136(%r9), %r9
	leal	1(%rdx,%rdx), %r8d
	movslq	%r8d, %rsi
	movq	16(%r9), %rdi
	movq	24(%r9), %rax
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L448
	addq	%rdi, %rsi
	movq	%rsi, 16(%r9)
.L400:
	movslq	12(%rcx), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jg	.L449
.L401:
	addl	$1, %eax
	movq	%rdi, (%rcx)
	movl	%r8d, 8(%rcx)
	movl	%eax, 12(%rcx)
	movq	%r13, (%rdi,%rdx)
	movq	(%rbx), %r9
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L445:
	movq	128(%r10), %rax
	movb	$1, (%rax)
	movb	$1, 2(%rax)
	movq	200(%r10), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L383
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L383:
	movq	%r9, %rdi
	call	_ZN2v88internal5Scope19GetDeclarationScopeEv@PLT
	movq	%r13, %rsi
	leaq	32(%rax), %rdi
	call	_ZN2v88internal11VariableMap6LookupEPKNS0_12AstRawStringE@PLT
	movq	%rax, %r15
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L447:
	movl	$-1, 32(%rax)
	movq	24(%rcx), %rsi
	movb	$1, 48(%rcx)
	movq	%rsi, 16(%rcx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L449:
	movq	(%rcx), %rsi
	movl	%r8d, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	memcpy@PLT
	movq	-88(%rbp), %rcx
	movl	-96(%rbp), %r8d
	movq	%rax, %rdi
	movslq	12(%rcx), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L440:
	movl	$32, %esi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %r9
	jmp	.L395
.L446:
	movl	$32, %esi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %r9
	jmp	.L386
.L448:
	movq	%r9, %rdi
	movl	%r8d, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %rcx
	movl	-96(%rbp), %r8d
	movq	%rax, %rdi
	jmp	.L400
.L444:
	movzwl	40(%r14), %edx
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L409
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.L410
	testb	$64, %dh
	je	.L450
.L410:
	orb	$64, %dh
	movw	%dx, 40(%r14)
	movzwl	40(%r13), %eax
	jmp	.L409
.L450:
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movzwl	40(%r14), %edx
	jmp	.L410
.L437:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23001:
	.size	_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE11NewVariableEPKNS0_12AstRawStringEi, .-_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE11NewVariableEPKNS0_12AstRawStringEi
	.section	.text._ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPNS0_15ExpressionScopeIS4_EE,"axG",@progbits,_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEEC5EPNS0_15ExpressionScopeIS4_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPNS0_15ExpressionScopeIS4_EE
	.type	_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPNS0_15ExpressionScopeIS4_EE, @function
_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPNS0_15ExpressionScopeIS4_EE:
.LFB25909:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	$0, (%rdi)
	movups	%xmm0, 16(%rdi)
	cmpb	$2, 16(%rsi)
	ja	.L451
	movq	%rsi, (%rdi)
	movl	60(%rsi), %eax
	cmpl	%eax, 56(%rsi)
	jbe	.L453
	movl	48(%rsi), %eax
	movl	%eax, 8(%rdi)
	movq	56(%rsi), %rax
	movq	%rax, 16(%rdi)
	movl	68(%rsi), %eax
	cmpl	%eax, 64(%rsi)
	jbe	.L453
	movl	52(%rsi), %eax
	movl	%eax, 12(%rdi)
	movq	64(%rsi), %rax
	movq	%rax, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L453:
	movq	$0, (%rdi)
.L451:
	ret
	.cfi_endproc
.LFE25909:
	.size	_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPNS0_15ExpressionScopeIS4_EE, .-_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPNS0_15ExpressionScopeIS4_EE
	.weak	_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEEC1EPNS0_15ExpressionScopeIS4_EE
	.set	_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEEC1EPNS0_15ExpressionScopeIS4_EE,_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPNS0_15ExpressionScopeIS4_EE
	.section	.text._ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEED2Ev,"axG",@progbits,_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEED2Ev
	.type	_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEED2Ev, @function
_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEED2Ev:
.LFB25912:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L454
	movl	16(%rdi), %ecx
	cmpl	%ecx, 20(%rdi)
	jnb	.L456
	movl	48(%rax), %edx
	movl	%edx, 8(%rdi)
	movq	56(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L456:
	movl	$4294967295, %esi
	movl	$0, 48(%rax)
	movq	%rsi, 56(%rax)
	movq	(%rdi), %rax
	movl	28(%rdi), %ecx
	cmpl	%ecx, 24(%rdi)
	jbe	.L458
	movl	52(%rax), %edx
	movl	%edx, 12(%rdi)
	movq	64(%rax), %rdx
	movq	%rdx, 24(%rdi)
.L458:
	movl	$4294967295, %esi
	movl	$0, 52(%rax)
	movq	%rsi, 64(%rax)
	movl	20(%rdi), %eax
	cmpl	%eax, 16(%rdi)
	ja	.L459
	movq	(%rdi), %rax
	movl	8(%rdi), %edx
	movl	%edx, 48(%rax)
	movq	(%rdi), %rax
	movq	16(%rdi), %rdx
	movq	%rdx, 56(%rax)
.L459:
	movl	28(%rdi), %eax
	cmpl	%eax, 24(%rdi)
	ja	.L454
	movq	(%rdi), %rax
	movl	12(%rdi), %edx
	movl	%edx, 52(%rax)
	movq	(%rdi), %rax
	movq	24(%rdi), %rdx
	movq	%rdx, 64(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L454:
	ret
	.cfi_endproc
.LFE25912:
	.size	_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEED2Ev, .-_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEED2Ev
	.weak	_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEED1Ev
	.set	_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEED1Ev,_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEED2Ev
	.section	.text._ZNSt6vectorIN2v88internal10ParserBaseINS1_9PreParserEE24DeclarationParsingResult11DeclarationESaIS6_EE17_M_realloc_insertIJRKS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal10ParserBaseINS1_9PreParserEE24DeclarationParsingResult11DeclarationESaIS6_EE17_M_realloc_insertIJRKS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal10ParserBaseINS1_9PreParserEE24DeclarationParsingResult11DeclarationESaIS6_EE17_M_realloc_insertIJRKS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal10ParserBaseINS1_9PreParserEE24DeclarationParsingResult11DeclarationESaIS6_EE17_M_realloc_insertIJRKS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_, @function
_ZNSt6vectorIN2v88internal10ParserBaseINS1_9PreParserEE24DeclarationParsingResult11DeclarationESaIS6_EE17_M_realloc_insertIJRKS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_:
.LFB26073:
	.cfi_startproc
	endbr64
	movabsq	$-6148914691236517205, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r14
	movq	%rsi, %rax
	subq	%r14, %rax
	sarq	$2, %rax
	imulq	%rcx, %rax
	movabsq	$768614336404564650, %rcx
	cmpq	%rcx, %rax
	je	.L482
	movq	%r12, %r8
	movq	%rdi, %r15
	subq	%r14, %r8
	testq	%rax, %rax
	je	.L474
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L483
.L467:
	movq	%rbx, %rdi
	movq	%rdx, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %r8
	movq	%rax, %r13
	leaq	(%rax,%rbx), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	12(%r13), %rbx
.L473:
	movq	(%rdx), %rax
	movq	%rax, 0(%r13,%r8)
	movl	8(%rdx), %eax
	movl	%eax, 8(%r13,%r8)
	cmpq	%r14, %r12
	je	.L469
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L470:
	movq	(%rax), %rcx
	addq	$12, %rax
	addq	$12, %rdx
	movq	%rcx, -12(%rdx)
	movl	-4(%rax), %ecx
	movl	%ecx, -4(%rdx)
	cmpq	%rax, %r12
	jne	.L470
	leaq	-12(%r12), %rax
	subq	%r14, %rax
	shrq	$2, %rax
	leaq	24(%r13,%rax,4), %rbx
.L469:
	cmpq	%rsi, %r12
	je	.L471
	subq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	-12(%rsi), %rax
	movq	%r12, %rsi
	shrq	$2, %rax
	leaq	12(,%rax,4), %rdx
	movq	%rdx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rdx
	addq	%rdx, %rbx
.L471:
	testq	%r14, %r14
	je	.L472
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L472:
	movq	-56(%rbp), %rax
	movq	%r13, %xmm0
	movq	%rbx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L483:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L468
	movq	$0, -56(%rbp)
	movl	$12, %ebx
	xorl	%r13d, %r13d
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L474:
	movl	$12, %ebx
	jmp	.L467
.L468:
	cmpq	%rcx, %rdi
	movq	%rcx, %rbx
	cmovbe	%rdi, %rbx
	imulq	$12, %rbx, %rbx
	jmp	.L467
.L482:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE26073:
	.size	_ZNSt6vectorIN2v88internal10ParserBaseINS1_9PreParserEE24DeclarationParsingResult11DeclarationESaIS6_EE17_M_realloc_insertIJRKS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_, .-_ZNSt6vectorIN2v88internal10ParserBaseINS1_9PreParserEE24DeclarationParsingResult11DeclarationESaIS6_EE17_M_realloc_insertIJRKS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	.section	.text._ZNK2v88internal10ParserBaseINS0_9PreParserEE18NewScopeWithParentEPNS0_5ScopeENS0_9ScopeTypeE,"axG",@progbits,_ZNK2v88internal10ParserBaseINS0_9PreParserEE18NewScopeWithParentEPNS0_5ScopeENS0_9ScopeTypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10ParserBaseINS0_9PreParserEE18NewScopeWithParentEPNS0_5ScopeENS0_9ScopeTypeE
	.type	_ZNK2v88internal10ParserBaseINS0_9PreParserEE18NewScopeWithParentEPNS0_5ScopeENS0_9ScopeTypeE, @function
_ZNK2v88internal10ParserBaseINS0_9PreParserEE18NewScopeWithParentEPNS0_5ScopeENS0_9ScopeTypeE:
.LFB26083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	136(%rdi), %r13
	movq	16(%r13), %r12
	movq	24(%r13), %rax
	subq	%r12, %rax
	cmpq	$135, %rax
	jbe	.L488
	leaq	136(%r12), %rax
	movq	%rax, 16(%r13)
.L486:
	movzbl	%dl, %ecx
	movq	%r13, %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal5ScopeC1EPNS0_4ZoneEPS1_NS0_9ScopeTypeE@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L488:
	.cfi_restore_state
	movl	$136, %esi
	movq	%r13, %rdi
	movl	%edx, -36(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-36(%rbp), %edx
	movq	%rax, %r12
	jmp	.L486
	.cfi_endproc
.LFE26083:
	.size	_ZNK2v88internal10ParserBaseINS0_9PreParserEE18NewScopeWithParentEPNS0_5ScopeENS0_9ScopeTypeE, .-_ZNK2v88internal10ParserBaseINS0_9PreParserEE18NewScopeWithParentEPNS0_5ScopeENS0_9ScopeTypeE
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE22ClassifyArrowParameterEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEEiNS0_19PreParserExpressionE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE22ClassifyArrowParameterEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEEiNS0_19PreParserExpressionE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ClassifyArrowParameterEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEEiNS0_19PreParserExpressionE
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ClassifyArrowParameterEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEEiNS0_19PreParserExpressionE, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE22ClassifyArrowParameterEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEEiNS0_19PreParserExpressionE:
.LFB26095:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rsi), %rax
	testq	%rax, %rax
	je	.L490
	movl	16(%rsi), %ebx
	cmpl	%ebx, 20(%rsi)
	jnb	.L491
	movl	48(%rax), %edx
	movl	%edx, 8(%rsi)
	movq	56(%rax), %rdx
	movq	%rdx, 16(%rsi)
.L491:
	movl	$4294967295, %ebx
	movl	$0, 48(%rax)
	movq	%rbx, 56(%rax)
	movq	(%rsi), %rax
	movl	28(%rsi), %ebx
	cmpl	%ebx, 24(%rsi)
	ja	.L510
.L492:
	movl	$4294967295, %ebx
	movl	$0, 52(%rax)
	movq	%rbx, 64(%rax)
.L490:
	testb	$8, %cl
	jne	.L493
	movl	%ecx, %eax
	andl	$7, %eax
	cmpl	$3, %eax
	je	.L494
	leal	-6(%rax), %edx
	cmpl	$1, %edx
	jbe	.L495
	cmpl	$2, %eax
	je	.L511
.L493:
	movq	200(%rdi), %rax
	movq	144(%rdi), %rdx
	movq	(%rax), %rax
	movl	4(%rax), %r10d
	movzbl	16(%rdx), %eax
	leal	-1(%rax), %ecx
	cmpb	$4, %cl
	ja	.L489
	subl	$3, %eax
	cmpb	$2, %al
	jbe	.L512
	movl	%r9d, 76(%rdx)
	movl	%r10d, 80(%rdx)
	movl	$252, 84(%rdx)
.L489:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L510:
	.cfi_restore_state
	movl	52(%rax), %edx
	movl	%edx, 12(%rsi)
	movq	64(%rax), %rdx
	movq	%rdx, 24(%rsi)
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L511:
	shrl	$4, %ecx
	andl	$15, %ecx
	cmpl	$9, %ecx
	jne	.L493
	.p2align 4,,10
	.p2align 3
.L495:
	movq	144(%rdi), %rdx
	movzbl	16(%rdx), %eax
	subl	$1, %eax
	cmpb	$1, %al
	ja	.L489
	movb	$0, 88(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L494:
	.cfi_restore_state
	shrl	$4, %ecx
	subl	$2, %ecx
	cmpb	$1, %cl
	ja	.L489
	movq	144(%rdi), %rax
	movzbl	16(%rax), %edx
	leal	-1(%rdx), %ecx
	cmpb	$2, %cl
	ja	.L489
	movq	200(%rdi), %rcx
	movq	(%rax), %rdi
	movq	(%rcx), %rcx
	movl	4(%rcx), %ecx
	cmpb	$3, %dl
	je	.L513
	movl	%r9d, 232(%rdi)
	movl	%ecx, 236(%rdi)
	movq	(%rax), %rax
	movl	$295, 240(%rax)
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L512:
	movq	(%rdx), %rbx
	xorl	%r8d, %r8d
	movl	%r10d, %edx
	movl	$252, %ecx
	movl	%r9d, %esi
	movq	128(%rbx), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%rbx), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L489
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L489
.L513:
	movq	(%rdi), %rax
	testb	$1, 129(%rax)
	je	.L502
	addq	$8, %rsp
	salq	$32, %rcx
	movl	%r9d, %esi
	movl	$295, %edx
	popq	%rbx
	orq	%rcx, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
.L502:
	.cfi_restore_state
	movq	224(%rdi), %rax
	cmpl	%ecx, %r9d
	setbe	21(%rax)
	jmp	.L489
	.cfi_endproc
.LFE26095:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ClassifyArrowParameterEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEEiNS0_19PreParserExpressionE, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE22ClassifyArrowParameterEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEEiNS0_19PreParserExpressionE
	.section	.rodata._ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC2:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm
	.type	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm, @function
_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm:
.LFB26128:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L535
	movabsq	$576460752303423487, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rcx
	movq	16(%rdi), %rax
	movq	%rcx, %rbx
	subq	(%rdi), %rbx
	subq	%rcx, %rax
	movq	%rbx, %r14
	sarq	$4, %rax
	sarq	$4, %r14
	subq	%r14, %rsi
	cmpq	%r12, %rax
	jb	.L516
	movq	%rcx, %rax
	movq	%r12, %rdx
	.p2align 4,,10
	.p2align 3
.L517:
	movq	$0, (%rax)
	addq	$16, %rax
	movl	$0, -8(%rax)
	subq	$1, %rdx
	jne	.L517
	salq	$4, %r12
	addq	%r12, %rcx
	movq	%rcx, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L535:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L516:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%r12, %rsi
	jb	.L538
	cmpq	%r12, %r14
	movq	%r12, %r15
	cmovnb	%r14, %r15
	addq	%r14, %r15
	cmpq	%rdx, %r15
	cmova	%rdx, %r15
	salq	$4, %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r12, %rcx
	leaq	(%rax,%rbx), %rdx
	.p2align 4,,10
	.p2align 3
.L521:
	movq	$0, (%rdx)
	addq	$16, %rdx
	movl	$0, -8(%rdx)
	subq	$1, %rcx
	jne	.L521
	movq	0(%r13), %rdi
	movq	8(%r13), %r8
	movq	%rax, %rcx
	movq	%rdi, %rdx
	cmpq	%rdi, %r8
	je	.L525
	.p2align 4,,10
	.p2align 3
.L522:
	movq	(%rdx), %r9
	movl	8(%rdx), %esi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	%r9, -16(%rcx)
	movl	%esi, -8(%rcx)
	cmpq	%rdx, %r8
	jne	.L522
.L525:
	testq	%rdi, %rdi
	je	.L524
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
.L524:
	addq	%r14, %r12
	movq	%rax, 0(%r13)
	salq	$4, %r12
	leaq	(%rax,%r12), %rdx
	addq	%r15, %rax
	movq	%rdx, 8(%r13)
	movq	%rax, 16(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L538:
	.cfi_restore_state
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE26128:
	.size	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm, .-_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm
	.section	.text._ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE6resizeEm,"axG",@progbits,_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE6resizeEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE6resizeEm
	.type	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE6resizeEm, @function
_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE6resizeEm:
.LFB25934:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movq	(%rdi), %rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	ja	.L542
	jnb	.L539
	salq	$4, %rsi
	addq	%rdx, %rsi
	cmpq	%rcx, %rsi
	je	.L539
	movq	%rsi, 8(%rdi)
.L539:
	ret
	.p2align 4,,10
	.p2align 3
.L542:
	subq	%rax, %rsi
	jmp	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm
	.cfi_endproc
.LFE25934:
	.size	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE6resizeEm, .-_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE6resizeEm
	.section	.text._ZN2v88internal21ArrowHeadParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE22ValidateAndCreateScopeEv,"axG",@progbits,_ZN2v88internal21ArrowHeadParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE22ValidateAndCreateScopeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21ArrowHeadParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE22ValidateAndCreateScopeEv
	.type	_ZN2v88internal21ArrowHeadParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE22ValidateAndCreateScopeEv, @function
_ZN2v88internal21ArrowHeadParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE22ValidateAndCreateScopeEv:
.LFB26295:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movl	76(%rdi), %esi
	movl	80(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %r12
	cmpl	%esi, %edx
	jnb	.L584
	movl	64(%rbx), %esi
	movl	68(%rbx), %edx
	cmpl	%edx, %esi
	jbe	.L585
.L546:
	movq	136(%r12), %r13
	xorl	%r8d, %r8d
	cmpb	$2, 16(%rbx)
	sete	%r8b
	movq	(%r12), %rdx
	movq	16(%r13), %r15
	movq	24(%r13), %rax
	addl	$8, %r8d
	subq	%r15, %rax
	cmpq	$223, %rax
	jbe	.L586
	leaq	224(%r15), %rax
	movq	%rax, 16(%r13)
.L550:
	movl	$2, %ecx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal16DeclarationScopeC1EPNS0_4ZoneEPNS0_5ScopeENS0_9ScopeTypeENS0_12FunctionKindE@PLT
	movq	16(%r12), %rax
	movb	$1, 59(%rax)
	cmpb	$0, 88(%rbx)
	jne	.L568
	andb	$-2, 131(%r15)
	cmpb	$0, 88(%rbx)
	jne	.L568
	movb	$0, -81(%rbp)
.L551:
	movq	24(%rbx), %rax
	movq	40(%rbx), %rdx
	movq	(%rax), %rcx
	movq	32(%rbx), %rax
	salq	$4, %rdx
	salq	$4, %rax
	leaq	(%rcx,%rdx), %rsi
	leaq	(%rcx,%rax), %r13
	leaq	-57(%rbp), %rax
	movq	%rsi, -96(%rbp)
	movq	%rax, -104(%rbp)
	cmpq	%r13, %rsi
	je	.L566
	movq	%rbx, -72(%rbp)
	movq	%r13, %rbx
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L556:
	cmpq	(%rax), %r15
	je	.L559
	movq	-80(%rbp), %rax
	movq	48(%rax), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	$31, %rcx
	jbe	.L587
	leaq	32(%rax), %rcx
	movq	%rcx, 16(%rdi)
.L561:
	movl	-88(%rbp), %ecx
	movq	$0, 16(%rax)
	movl	$64, 4(%rax)
	movl	%ecx, (%rax)
	movq	%r15, 24(%rax)
	movq	%rdx, 8(%rax)
	movq	(%rdx), %rcx
	movq	96(%rcx), %rsi
	movq	%rax, (%rsi)
	addq	$16, %rax
	movq	%rax, 96(%rcx)
.L559:
	cmpb	$0, -57(%rbp)
	movl	%r12d, 36(%rdx)
	jne	.L562
	movl	0(%r13), %esi
	movq	8(%r13), %rax
	testb	$1, 5(%r13)
	je	.L563
	movq	8(%rax), %rax
.L563:
	movq	16(%rax), %rcx
	cmpb	$0, 28(%rax)
	movl	%ecx, %edx
	jne	.L564
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
.L564:
	movq	-72(%rbp), %rax
	addl	%esi, %edx
	xorl	%r8d, %r8d
	movl	$284, %ecx
	movq	(%rax), %r12
	movq	128(%r12), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r12), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L562
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L562:
	addq	$16, %rbx
	cmpq	%rbx, -96(%rbp)
	je	.L588
.L567:
	movq	(%rbx), %r13
	movq	-72(%rbp), %rsi
	movl	8(%rbx), %r12d
	movl	4(%r13), %eax
	movq	8(%r13), %r14
	movl	%eax, %edx
	andb	$127, %dl
	movl	%edx, 4(%r13)
	movq	(%rsi), %rdi
	movl	0(%r13), %esi
	movq	%rdi, -80(%rbp)
	movl	%esi, -88(%rbp)
	testb	$1, %ah
	je	.L555
	movq	8(%r14), %r14
.L555:
	movzbl	-81(%rbp), %edx
	movq	-104(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	$1, %r8d
	call	_ZN2v88internal5Scope19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPbNS0_12VariableKindE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L556
	movq	-80(%rbp), %rdi
	movq	128(%rdi), %rax
	movb	$1, (%rax)
	movb	$1, 2(%rax)
	movq	200(%rdi), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L557
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L557:
	cmpb	$2, -81(%rbp)
	movq	%r15, %rax
	jne	.L558
	movq	%r15, %rdi
	call	_ZN2v88internal5Scope19GetDeclarationScopeEv@PLT
.L558:
	leaq	32(%rax), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal11VariableMap6LookupEPKNS0_12AstRawStringE@PLT
	movq	%rax, %rdx
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L588:
	movq	-72(%rbp), %rbx
.L566:
	cmpb	$0, 89(%rbx)
	jne	.L589
.L543:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L590
	addq	$72, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L568:
	.cfi_restore_state
	movb	$2, -81(%rbp)
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L584:
	movl	84(%rdi), %ecx
	movq	128(%r12), %rdi
	xorl	%r8d, %r8d
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r12), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L545
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L545:
	movl	64(%rbx), %esi
	movl	68(%rbx), %edx
	movq	(%rbx), %r12
	cmpl	%edx, %esi
	ja	.L546
.L585:
	movl	52(%rbx), %ecx
	movq	128(%r12), %rdi
	xorl	%r8d, %r8d
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r12), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L547
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L547:
	movq	(%rbx), %r12
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L589:
	orb	$8, 132(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal5Scope16GetReceiverScopeEv@PLT
	movq	176(%rax), %rax
	orw	$1024, 40(%rax)
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L587:
	movl	$32, %esi
	movq	%rdx, -80(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-80(%rbp), %rdx
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L586:
	movl	$224, %esi
	movq	%r13, %rdi
	movl	%r8d, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	movl	-80(%rbp), %r8d
	movq	%rax, %r15
	jmp	.L550
.L590:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26295:
	.size	_ZN2v88internal21ArrowHeadParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE22ValidateAndCreateScopeEv, .-_ZN2v88internal21ArrowHeadParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE22ValidateAndCreateScopeEv
	.section	.text._ZN2v88internal21ArrowHeadParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE18ValidateExpressionEv,"axG",@progbits,_ZN2v88internal21ArrowHeadParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE18ValidateExpressionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21ArrowHeadParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE18ValidateExpressionEv
	.type	_ZN2v88internal21ArrowHeadParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE18ValidateExpressionEv, @function
_ZN2v88internal21ArrowHeadParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE18ValidateExpressionEv:
.LFB26296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4294967295, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rax
	movq	%rcx, 232(%rax)
	movl	$0, 240(%rax)
	movl	56(%rdi), %esi
	movl	60(%rdi), %edx
	cmpl	%edx, %esi
	jbe	.L598
.L592:
	movq	24(%r12), %rax
	movq	32(%r12), %rbx
	movq	(%rax), %r13
	movq	40(%r12), %rax
	salq	$4, %rbx
	salq	$4, %rax
	addq	%r13, %rbx
	addq	%rax, %r13
	cmpq	%rbx, %r13
	je	.L591
	.p2align 4,,10
	.p2align 3
.L595:
	movq	(%r12), %rax
	movq	(%rbx), %rsi
	addq	$16, %rbx
	movq	(%rax), %rdi
	call	_ZN2v88internal5Scope13AddUnresolvedEPNS0_13VariableProxyE@PLT
	cmpq	%rbx, %r13
	jne	.L595
.L591:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L598:
	.cfi_restore_state
	movq	(%rdi), %rbx
	movl	48(%rdi), %ecx
	xorl	%r8d, %r8d
	movq	128(%rbx), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%rbx), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L592
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L592
	.cfi_endproc
.LFE26296:
	.size	_ZN2v88internal21ArrowHeadParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE18ValidateExpressionEv, .-_ZN2v88internal21ArrowHeadParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE18ValidateExpressionEv
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE34ParsePropertyOrPrivatePropertyNameEv,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE34ParsePropertyOrPrivatePropertyNameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE34ParsePropertyOrPrivatePropertyNameEv
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE34ParsePropertyOrPrivatePropertyNameEv, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE34ParsePropertyOrPrivatePropertyNameEv:
.LFB26297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	200(%rdi), %rdi
	movq	(%rdi), %rax
	movl	(%rax), %r13d
	call	_ZN2v88internal7Scanner4NextEv@PLT
	leaq	_ZN2v88internal5Token11token_flagsE(%rip), %rsi
	movzbl	%al, %ecx
	movl	$2, %eax
	testb	$2, (%rsi,%rcx)
	je	.L610
.L604:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L610:
	.cfi_restore_state
	cmpb	$108, %cl
	je	.L611
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
.L609:
	movl	$1, %eax
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L611:
	movq	(%r12), %rdi
	call	_ZN2v88internal5Scope13GetClassScopeEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal9PreParser13GetIdentifierEv
	movq	%rax, %r8
	movq	%rdx, %rbx
	testq	%r14, %r14
	je	.L612
	movq	48(%r12), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$23, %rax
	jbe	.L613
	leaq	24(%rsi), %rax
	movq	%rax, 16(%rdi)
.L606:
	movl	%r13d, (%rsi)
	movq	%r14, %rdi
	movq	%r8, 8(%rsi)
	movq	$0, 16(%rsi)
	movl	$2102, 4(%rsi)
	call	_ZN2v88internal10ClassScope24AddUnresolvedPrivateNameEPNS0_13VariableProxyE@PLT
	movzbl	%bl, %eax
	sall	$4, %eax
	orl	$3, %eax
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L612:
	movq	128(%r12), %rdi
	leal	1(%r13), %edx
	movl	$258, %ecx
	movl	%r13d, %esi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKNS0_12AstRawStringE@PLT
	movq	200(%r12), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L609
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L609
.L613:
	movl	$24, %esi
	movq	%r8, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L606
	.cfi_endproc
.LFE26297:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE34ParsePropertyOrPrivatePropertyNameEv, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE34ParsePropertyOrPrivatePropertyNameEv
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseSuperExpressionEb,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseSuperExpressionEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseSuperExpressionEb
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseSuperExpressionEb, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseSuperExpressionEb:
.LFB26416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	200(%rdi), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rax
	movq	(%r12), %rdi
	movq	(%rax), %rax
	movl	(%rax), %r14d
	call	_ZN2v88internal5Scope16GetReceiverScopeEv@PLT
	movzbl	133(%rax), %edx
	cmpb	$17, %dl
	ja	.L666
	movq	%rax, %rbx
	movl	$235772, %eax
	movq	200(%r12), %rdi
	btq	%rdx, %rax
	jnc	.L616
	movq	8(%rdi), %rax
	movzbl	56(%rax), %eax
	leal	-2(%rax), %ecx
	cmpb	$1, %cl
	jbe	.L667
	cmpb	$5, %al
	jne	.L616
	cmpb	$1, %r13b
	jne	.L668
.L616:
	movq	(%rdi), %rax
	movq	128(%r12), %rdi
	xorl	%r8d, %r8d
	movl	$316, %ecx
	movl	4(%rax), %edx
	movl	(%rax), %esi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r12), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	je	.L669
.L642:
	movl	$1, %eax
.L621:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L669:
	.cfi_restore_state
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L667:
	cmpb	$2, %al
	je	.L670
.L618:
	orb	$32, 131(%rbx)
	movq	(%r12), %rdi
	call	_ZN2v88internal5Scope15GetClosureScopeEv@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal5Scope16GetReceiverScopeEv@PLT
	movq	176(%rax), %rdx
	orw	$2048, 40(%rdx)
	cmpq	%rax, %rbx
	je	.L671
	orb	$8, 132(%rbx)
	orw	$1024, 40(%rdx)
.L624:
	movq	40(%r12), %rax
	movq	48(%r12), %rdi
	movq	(%r12), %r13
	movq	56(%rax), %rax
	movq	16(%rdi), %rsi
	movq	480(%rax), %rbx
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$23, %rax
	jbe	.L672
	leaq	24(%rsi), %rax
	movq	%rax, 16(%rdi)
.L628:
	movl	%r14d, (%rsi)
	movq	%r13, %rdi
	movq	%rbx, 8(%rsi)
	movq	$0, 16(%rsi)
	movl	$2102, 4(%rsi)
	call	_ZN2v88internal5Scope13AddUnresolvedEPNS0_13VariableProxyE@PLT
	popq	%rbx
	movl	$2, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L668:
	.cfi_restore_state
	subl	$4, %edx
	cmpb	$1, %dl
	ja	.L616
	movq	144(%r12), %rax
	.p2align 4,,10
	.p2align 3
.L632:
	movzbl	16(%rax), %ebx
	leal	-1(%rbx), %edx
	cmpb	$1, %dl
	jbe	.L673
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L632
.L630:
	movq	(%r12), %rdi
	call	_ZN2v88internal5Scope15GetClosureScopeEv@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal5Scope16GetReceiverScopeEv@PLT
	movq	176(%rax), %rdx
	orw	$2048, 40(%rdx)
	cmpq	%rax, %rbx
	je	.L674
	orb	$8, 132(%rbx)
	orw	$1024, 40(%rdx)
.L635:
	movq	40(%r12), %rax
	movq	48(%r12), %rdi
	movq	(%r12), %r13
	movq	56(%rax), %rax
	movq	16(%rdi), %rsi
	movq	480(%rax), %rbx
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$23, %rax
	jbe	.L675
	leaq	24(%rsi), %rax
	movq	%rax, 16(%rdi)
.L639:
	movq	%rbx, 8(%rsi)
	movq	%r13, %rdi
	movl	%r14d, (%rsi)
	movq	$0, 16(%rsi)
	movl	$2102, 4(%rsi)
	call	_ZN2v88internal5Scope13AddUnresolvedEPNS0_13VariableProxyE@PLT
	movq	40(%r12), %rax
	movq	48(%r12), %rdi
	movq	(%r12), %r13
	movq	56(%rax), %rax
	movq	16(%rdi), %rsi
	movq	360(%rax), %rbx
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$23, %rax
	jbe	.L676
	leaq	24(%rsi), %rax
	movq	%rax, 16(%rdi)
.L641:
	movl	%r14d, (%rsi)
	movq	%r13, %rdi
	movq	%rbx, 8(%rsi)
	movq	$0, 16(%rsi)
	movl	$2102, 4(%rsi)
	call	_ZN2v88internal5Scope13AddUnresolvedEPNS0_13VariableProxyE@PLT
	movl	$130, %eax
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L666:
	movq	200(%r12), %rdi
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L670:
	call	_ZN2v88internal7Scanner9PeekAheadEv@PLT
	movq	200(%r12), %rdi
	cmpb	$108, %al
	je	.L619
	movq	8(%rdi), %rax
	cmpb	$4, 56(%rax)
	jne	.L618
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	%r12, %rdi
	movl	$375, %esi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L671:
	.cfi_restore_state
	movq	144(%r12), %rax
.L626:
	movzbl	16(%rax), %ecx
	leal	-1(%rcx), %edx
	cmpb	$1, %dl
	jbe	.L677
.L623:
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L624
	movzbl	16(%rax), %ecx
	leal	-1(%rcx), %edx
	cmpb	$1, %dl
	ja	.L623
.L677:
	movb	$1, 89(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L626
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L673:
	movb	$1, 89(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L632
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L672:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rsi
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L619:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movl	$313, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	movl	$1, %eax
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L674:
	movq	144(%r12), %rax
	.p2align 4,,10
	.p2align 3
.L637:
	movzbl	16(%rax), %ecx
	leal	-1(%rcx), %edx
	cmpb	$1, %dl
	jbe	.L678
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L637
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L678:
	movb	$1, 89(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L637
	jmp	.L635
.L676:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rsi
	jmp	.L641
.L675:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rsi
	jmp	.L639
	.cfi_endproc
.LFE26416:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseSuperExpressionEb, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseSuperExpressionEb
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE24ParsePostfixContinuationENS0_19PreParserExpressionEi,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE24ParsePostfixContinuationENS0_19PreParserExpressionEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE24ParsePostfixContinuationENS0_19PreParserExpressionEi
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE24ParsePostfixContinuationENS0_19PreParserExpressionEi, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE24ParsePostfixContinuationENS0_19PreParserExpressionEi:
.LFB26479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	andl	$7, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	cmpl	$3, %eax
	jne	.L680
	movq	(%rdi), %rax
	testb	$1, 129(%rax)
	jne	.L681
.L685:
	movq	144(%rbx), %rdx
	cmpb	$2, 16(%rdx)
	ja	.L686
	movq	40(%rdx), %rsi
	movq	32(%rdx), %rcx
	movl	%esi, %eax
	subl	%ecx, %eax
	cmpl	%ecx, %esi
	je	.L686
	subl	$1, %eax
	movq	24(%rdx), %rdx
	cltq
	addq	%rcx, %rax
	salq	$4, %rax
	addq	(%rdx), %rax
	movq	(%rax), %rax
	movl	4(%rax), %edx
	movl	%edx, %ecx
	orb	$-128, %cl
	movl	%ecx, 4(%rax)
	andb	$1, %dh
	je	.L686
	movq	8(%rax), %r12
	movzwl	40(%r12), %eax
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$1, %dl
	je	.L686
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L687
	testb	$64, %ah
	je	.L716
.L687:
	orb	$64, %ah
	movw	%ax, 40(%r12)
	jmp	.L686
	.p2align 4,,10
	.p2align 3
.L680:
	cmpl	$2, %eax
	je	.L717
.L684:
	movq	200(%rbx), %rax
	movl	$254, %r8d
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movl	4(%rax), %ecx
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE33RewriteInvalidReferenceExpressionENS0_19PreParserExpressionEiiNS0_15MessageTemplateE
	andl	$7, %eax
	cmpl	$3, %eax
	je	.L685
.L686:
	movq	200(%rbx), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	popq	%rbx
	movl	$2, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L681:
	.cfi_restore_state
	movl	%esi, %eax
	shrl	$4, %eax
	subl	$2, %eax
	cmpb	$1, %al
	ja	.L685
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L717:
	movl	%esi, %eax
	shrl	$4, %eax
	andl	$15, %eax
	subl	$1, %eax
	cmpl	$3, %eax
	ja	.L684
	jmp	.L686
.L716:
	movzwl	40(%r13), %edx
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L687
	movq	16(%r13), %r14
	testq	%r14, %r14
	je	.L688
	testb	$64, %dh
	je	.L718
.L688:
	orb	$64, %dh
	movw	%dx, 40(%r13)
	movzwl	40(%r12), %eax
	jmp	.L687
.L718:
	movzwl	40(%r14), %eax
	movl	%eax, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L688
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.L689
	testb	$64, %ah
	je	.L719
.L689:
	orb	$64, %ah
	movw	%ax, 40(%r14)
	movzwl	40(%r13), %edx
	jmp	.L688
.L719:
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movzwl	40(%r14), %eax
	jmp	.L689
	.cfi_endproc
.LFE26479:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE24ParsePostfixContinuationENS0_19PreParserExpressionEi, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE24ParsePostfixContinuationENS0_19PreParserExpressionEi
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseAwaitExpressionEv,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseAwaitExpressionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseAwaitExpressionEv
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseAwaitExpressionEv, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseAwaitExpressionEv:
.LFB26441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	200(%rdi), %rax
	movq	%rdi, %rbx
	movq	144(%rdi), %rcx
	movq	8(%rax), %rsi
	movq	%rcx, %rax
	.p2align 4,,10
	.p2align 3
.L725:
	movzbl	16(%rax), %edx
	cmpb	$3, %dl
	je	.L721
.L752:
	cmpb	$0, 17(%rcx)
	je	.L722
	subl	$1, %edx
	cmpb	$2, %dl
	ja	.L723
	movq	(%rsi), %rdx
	movl	$304, 84(%rax)
	movq	%rdx, 76(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L725
	.p2align 4,,10
	.p2align 3
.L722:
	movq	200(%rbx), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%rbx), %rax
	movq	(%rax), %rax
	movl	4(%rax), %edx
	subl	(%rax), %edx
	cmpb	$90, 56(%rax)
	jne	.L726
	subl	$2, %edx
.L726:
	cmpb	$0, 28(%rax)
	movl	24(%rax), %ecx
	jne	.L727
	sarl	%ecx
.L727:
	cmpl	%ecx, %edx
	jne	.L750
.L728:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	120(%rbx), %rax
	jnb	.L729
	movq	200(%rbx), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	je	.L751
.L730:
	movq	128(%rbx), %rax
	movl	$257, %edx
	movw	%dx, (%rax)
.L729:
	movq	200(%rbx), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %r12d
	leal	-44(%r12), %eax
	cmpb	$8, %al
	ja	.L731
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseUnaryOrPrefixExpressionEv
.L732:
	movq	16(%rbx), %rax
	addl	$1, 20(%rax)
	movl	$2, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L723:
	.cfi_restore_state
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L722
	movzbl	16(%rax), %edx
	cmpb	$3, %dl
	jne	.L752
.L721:
	movq	(%rcx), %r12
	movl	4(%rsi), %edx
	xorl	%r8d, %r8d
	movl	$304, %ecx
	movl	(%rsi), %esi
	movq	128(%r12), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r12), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L722
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L731:
	movq	16(%rbx), %rax
	movq	48(%rax), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$9, %eax
	cmpb	$4, %al
	ja	.L733
	cmpb	$96, %r12b
	je	.L753
.L733:
	movq	200(%rbx), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %rax
	movl	(%rax), %r12d
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParsePrimaryExpressionEv
	movl	%eax, %esi
	movq	200(%rbx), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %edx
	cmpb	$3, %dl
	jbe	.L754
.L734:
	cmpb	$5, %dl
	jbe	.L755
.L735:
	subl	$51, %edx
	cmpb	$1, %dl
	ja	.L732
	cmpb	$0, 76(%rax)
	jne	.L732
	movl	%r12d, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE24ParsePostfixContinuationENS0_19PreParserExpressionEi
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L751:
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L755:
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseLeftHandSideContinuationENS0_19PreParserExpressionE
	movl	%eax, %esi
	movq	200(%rbx), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %edx
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L754:
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE35DoParseMemberExpressionContinuationENS0_19PreParserExpressionE
	movl	%eax, %esi
	movq	200(%rbx), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %edx
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L750:
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L753:
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseAwaitExpressionEv
	jmp	.L732
	.cfi_endproc
.LFE26441:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseAwaitExpressionEv, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseAwaitExpressionEv
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseUnaryOrPrefixExpressionEv,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseUnaryOrPrefixExpressionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseUnaryOrPrefixExpressionEv
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseUnaryOrPrefixExpressionEv, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseUnaryOrPrefixExpressionEv:
.LFB26440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	200(%rdi), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movl	%eax, %ebx
	movq	200(%r12), %rax
	movq	(%rax), %rdx
	movl	(%rdx), %r13d
	cmpb	$46, %bl
	je	.L819
.L757:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	120(%r12), %rax
	jnb	.L758
	movq	200(%r12), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	je	.L820
.L759:
	movq	128(%r12), %rax
	movl	$257, %edx
	movw	%dx, (%rax)
.L758:
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %r15d
	movl	(%rax), %r14d
	leal	-44(%r15), %eax
	cmpb	$8, %al
	ja	.L760
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseUnaryOrPrefixExpressionEv
	movl	%eax, %esi
.L761:
	leal	-44(%rbx), %eax
	cmpb	$6, %al
	ja	.L765
	cmpb	$48, %bl
	je	.L821
.L766:
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	cmpb	$43, 56(%rax)
	je	.L822
.L769:
	movl	$2, %eax
.L768:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L760:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	48(%rax), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$9, %eax
	cmpb	$4, %al
	ja	.L762
	cmpb	$96, %r15b
	je	.L823
.L762:
	movq	200(%r12), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rax
	movl	(%rax), %r15d
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParsePrimaryExpressionEv
	movl	%eax, %esi
	movq	200(%r12), %rax
	movq	8(%rax), %rdx
	movzbl	56(%rdx), %eax
	cmpb	$3, %al
	jbe	.L824
.L763:
	cmpb	$5, %al
	jbe	.L825
.L764:
	subl	$51, %eax
	cmpb	$1, %al
	ja	.L761
	cmpb	$0, 76(%rdx)
	jne	.L761
	movl	%r15d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE24ParsePostfixContinuationENS0_19PreParserExpressionEi
	movl	%eax, %esi
	jmp	.L761
	.p2align 4,,10
	.p2align 3
.L765:
	movl	%esi, %eax
	andl	$7, %eax
	cmpl	$3, %eax
	je	.L826
	cmpl	$2, %eax
	je	.L827
.L774:
	movq	200(%r12), %rax
	movl	$255, %r8d
	movl	%r14d, %edx
	movq	%r12, %rdi
	movq	(%rax), %rax
	movl	4(%rax), %ecx
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE33RewriteInvalidReferenceExpressionENS0_19PreParserExpressionEiiNS0_15MessageTemplateE
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L826:
	movq	(%r12), %rax
	testb	$1, 129(%rax)
	je	.L775
	movl	%esi, %eax
	shrl	$4, %eax
	subl	$2, %eax
	cmpb	$1, %al
	jbe	.L774
.L775:
	movq	144(%r12), %rdx
	cmpb	$2, 16(%rdx)
	ja	.L769
	movq	40(%rdx), %rsi
	movq	32(%rdx), %rcx
	movl	%esi, %eax
	subl	%ecx, %eax
	cmpl	%ecx, %esi
	je	.L769
	subl	$1, %eax
	movq	24(%rdx), %rdx
	cltq
	addq	%rcx, %rax
	salq	$4, %rax
	addq	(%rdx), %rax
	movq	(%rax), %rax
	movl	4(%rax), %edx
	movl	%edx, %ecx
	orb	$-128, %cl
	movl	%ecx, 4(%rax)
	andb	$1, %dh
	je	.L769
	movq	8(%rax), %rbx
	movzwl	40(%rbx), %eax
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$1, %dl
	je	.L769
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L777
	testb	$64, %ah
	je	.L828
.L777:
	orb	$64, %ah
	movw	%ax, 40(%rbx)
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L820:
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L819:
	movq	8(%rax), %rax
	cmpb	$73, 56(%rax)
	jne	.L757
	movzbl	_ZN2v88internal13FLAG_max_lazyE(%rip), %eax
	movq	16(%r12), %rdx
	xorl	$1, %eax
	movb	%al, 57(%rdx)
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L821:
	movl	%esi, %eax
	andl	$7, %eax
	cmpl	$3, %eax
	je	.L829
	cmpl	$2, %eax
	jne	.L766
	movl	%esi, %eax
	shrl	$4, %eax
	andl	$15, %eax
	subl	$2, %eax
	andl	$-3, %eax
	jne	.L766
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	movl	$1, %eax
	jmp	.L768
	.p2align 4,,10
	.p2align 3
.L827:
	movl	%esi, %eax
	shrl	$4, %eax
	andl	$15, %eax
	subl	$1, %eax
	cmpl	$3, %eax
	ja	.L774
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L825:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseLeftHandSideContinuationENS0_19PreParserExpressionE
	movl	%eax, %esi
	movq	200(%r12), %rax
	movq	8(%rax), %rdx
	movzbl	56(%rdx), %eax
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L824:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE35DoParseMemberExpressionContinuationENS0_19PreParserExpressionE
	movl	%eax, %esi
	movq	200(%r12), %rax
	movq	8(%rax), %rdx
	movzbl	56(%rdx), %eax
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L822:
	movl	4(%rax), %esi
	movl	$320, %edx
	movq	%r12, %rdi
	movq	%rsi, %rax
	movl	%r13d, %esi
	salq	$32, %rax
	orq	%rax, %rsi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	movl	$1, %eax
	jmp	.L768
	.p2align 4,,10
	.p2align 3
.L823:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseAwaitExpressionEv
	movl	%eax, %esi
	jmp	.L761
	.p2align 4,,10
	.p2align 3
.L829:
	movq	(%r12), %rax
	testb	$1, 129(%rax)
	je	.L766
	movl	$294, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	movl	$1, %eax
	jmp	.L768
.L828:
	movzwl	40(%r12), %edx
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L777
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L778
	testb	$64, %dh
	je	.L830
.L778:
	orb	$64, %dh
	movw	%dx, 40(%r12)
	movzwl	40(%rbx), %eax
	jmp	.L777
.L830:
	movzwl	40(%r13), %eax
	movl	%eax, %ecx
	andl	$15, %ecx
	subb	$1, %cl
	je	.L778
	movq	16(%r13), %r14
	testq	%r14, %r14
	je	.L779
	testb	$64, %ah
	je	.L831
.L779:
	orb	$64, %ah
	movw	%ax, 40(%r13)
	movzwl	40(%r12), %edx
	jmp	.L778
.L831:
	movzwl	40(%r14), %edx
	movl	%edx, %ecx
	andl	$15, %ecx
	subb	$1, %cl
	je	.L779
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.L780
	testb	$64, %dh
	je	.L832
.L780:
	orb	$64, %dh
	movw	%dx, 40(%r14)
	movzwl	40(%r13), %eax
	jmp	.L779
.L832:
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movzwl	40(%r14), %edx
	jmp	.L780
	.cfi_endproc
.LFE26440:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseUnaryOrPrefixExpressionEv, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseUnaryOrPrefixExpressionEv
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseLogicalExpressionEv,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseLogicalExpressionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseLogicalExpressionEv
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseLogicalExpressionEv, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseLogicalExpressionEv:
.LFB25921:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	200(%rdi), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %ebx
	leal	-44(%rbx), %eax
	cmpb	$8, %al
	ja	.L834
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseUnaryOrPrefixExpressionEv
	movl	%eax, %esi
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %eax
.L835:
	movzbl	256(%r12), %edx
	leaq	_ZN2v88internal5Token11precedence_E(%rip), %rbx
	movzbl	%al, %ecx
	imulq	$114, %rdx, %rdx
	addq	%rbx, %rdx
	movsbl	(%rdx,%rcx), %ecx
	cmpl	$5, %ecx
	jg	.L843
	leal	-32(%rax), %edx
	cmpb	$1, %dl
	jbe	.L852
.L841:
	cmpb	$31, %al
	je	.L853
.L846:
	addq	$8, %rsp
	movl	%esi, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L834:
	.cfi_restore_state
	movq	16(%rdi), %rax
	movq	48(%rax), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$9, %eax
	cmpb	$4, %al
	ja	.L836
	cmpb	$96, %bl
	je	.L854
.L836:
	movq	200(%r12), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rax
	movl	(%rax), %r13d
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParsePrimaryExpressionEv
	movl	%eax, %esi
	movq	200(%r12), %rax
	movq	8(%rax), %rdx
	movzbl	56(%rdx), %eax
	cmpb	$3, %al
	jbe	.L855
.L837:
	cmpb	$5, %al
	jbe	.L856
.L838:
	leal	-51(%rax), %ecx
	cmpb	$1, %cl
	ja	.L835
	cmpb	$0, 76(%rdx)
	je	.L857
	movzbl	256(%r12), %edx
	leaq	_ZN2v88internal5Token11precedence_E(%rip), %rbx
	imulq	$114, %rdx, %rdx
	addq	%rbx, %rdx
	movsbl	(%rdx,%rax), %ecx
	cmpl	$5, %ecx
	jle	.L846
	.p2align 4,,10
	.p2align 3
.L843:
	movl	$6, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseBinaryContinuationENS0_19PreParserExpressionEii
	movl	%eax, %esi
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %eax
	leal	-32(%rax), %edx
	cmpb	$1, %dl
	ja	.L841
.L852:
	movzbl	256(%r12), %edx
	movq	%r12, %rdi
	imulq	$114, %rdx, %rdx
	addq	%rdx, %rbx
	movl	$4, %edx
	movsbl	(%rbx,%rax), %ecx
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseBinaryContinuationENS0_19PreParserExpressionEii
	addq	$8, %rsp
	movl	%eax, %esi
	popq	%rbx
	popq	%r12
	movl	%esi, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L853:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseCoalesceExpressionENS0_19PreParserExpressionE
	movl	%eax, %esi
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L856:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseLeftHandSideContinuationENS0_19PreParserExpressionE
	movl	%eax, %esi
	movq	200(%r12), %rax
	movq	8(%rax), %rdx
	movzbl	56(%rdx), %eax
	jmp	.L838
	.p2align 4,,10
	.p2align 3
.L855:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE35DoParseMemberExpressionContinuationENS0_19PreParserExpressionE
	movl	%eax, %esi
	movq	200(%r12), %rax
	movq	8(%rax), %rdx
	movzbl	56(%rdx), %eax
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L854:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseAwaitExpressionEv
	movl	%eax, %esi
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %eax
	jmp	.L835
.L857:
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE24ParsePostfixContinuationENS0_19PreParserExpressionEi
	movl	%eax, %esi
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %eax
	jmp	.L835
	.cfi_endproc
.LFE25921:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseLogicalExpressionEv, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseLogicalExpressionEv
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseAssignmentExpressionCoverGrammarEv,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseAssignmentExpressionCoverGrammarEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseAssignmentExpressionCoverGrammarEv
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseAssignmentExpressionCoverGrammarEv, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseAssignmentExpressionCoverGrammarEv:
.LFB25168:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	200(%rdi), %rax
	movq	8(%rax), %rax
	movl	(%rax), %r14d
	cmpb	$97, 56(%rax)
	movl	%r14d, %r15d
	je	.L940
.L859:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseLogicalExpressionEv
	movq	200(%r12), %rdi
	movl	%eax, %r13d
	movq	8(%rdi), %rdx
	movzbl	56(%rdx), %ebx
	cmpb	$11, %bl
	je	.L941
.L863:
	leal	-15(%rbx), %eax
	cmpb	$14, %al
	movl	%r13d, %eax
	jbe	.L864
.L861:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L942
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L864:
	.cfi_restore_state
	andl	$7, %eax
	cmpb	$15, %bl
	je	.L943
	cmpl	$3, %eax
	jne	.L869
	movq	(%r12), %rax
	testb	$1, 129(%rax)
	je	.L870
	movl	%r13d, %eax
	shrl	$4, %eax
	subl	$2, %eax
	cmpb	$1, %al
	jbe	.L944
.L870:
	movq	144(%r12), %rdx
	movzbl	16(%rdx), %eax
	testb	$8, %r13b
	jne	.L945
.L872:
	cmpb	$2, %al
	ja	.L939
	movq	40(%rdx), %rsi
	movq	32(%rdx), %rcx
	movl	%esi, %eax
	subl	%ecx, %eax
	cmpl	%ecx, %esi
	je	.L939
	subl	$1, %eax
	movq	24(%rdx), %rdx
	cltq
	addq	%rcx, %rax
	salq	$4, %rax
	addq	(%rdx), %rax
	movq	(%rax), %rax
	movl	4(%rax), %edx
	movl	%edx, %ecx
	orb	$-128, %cl
	movl	%ecx, 4(%rax)
	andb	$1, %dh
	jne	.L878
.L939:
	movq	200(%r12), %rdi
.L876:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	%r12, %rsi
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPS3_NS0_15ExpressionScopeIS4_E9ScopeTypeE.constprop.0
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseAssignmentExpressionCoverGrammarEv
	movl	-88(%rbp), %esi
	movl	-84(%rbp), %edx
	cmpl	%edx, %esi
	jbe	.L946
.L895:
	movq	-120(%rbp), %rdi
	movq	-112(%rbp), %rsi
	movq	8(%rdi), %rcx
	movq	(%rdi), %rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	ja	.L947
	jnb	.L898
	salq	$4, %rsi
	addq	%rdx, %rsi
	cmpq	%rsi, %rcx
	je	.L898
	movq	%rsi, 8(%rdi)
.L898:
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	%rdx, 144(%rax)
	cmpb	$17, %bl
	jne	.L899
	movl	%r13d, %eax
	andl	$7, %eax
	cmpl	$2, %eax
	je	.L948
.L901:
	movl	$146, %eax
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L940:
	movq	16(%rdi), %rax
	movq	48(%rax), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$12, %eax
	cmpb	$3, %al
	jbe	.L860
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	movl	(%rax), %r15d
	jmp	.L859
	.p2align 4,,10
	.p2align 3
.L899:
	movq	144(%r12), %rax
	movq	200(%r12), %rdx
	movzbl	16(%rax), %ebx
	movq	(%rdx), %rdx
	movl	4(%rdx), %r9d
	leal	-3(%rbx), %edx
	cmpb	$2, %dl
	jbe	.L949
	movl	68(%rax), %ecx
	cmpl	%ecx, 64(%rax)
	jbe	.L901
	movl	$252, 52(%rax)
	movl	%r14d, 64(%rax)
	movl	%r9d, 68(%rax)
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L941:
	movq	%r12, %rdi
	movl	%r15d, %edx
	movl	%eax, %esi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseConditionalContinuationENS0_19PreParserExpressionEi
	movq	200(%r12), %rdi
	movl	%eax, %r13d
	movq	8(%rdi), %rax
	movzbl	56(%rax), %ebx
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L947:
	subq	%rax, %rsi
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm
	jmp	.L898
	.p2align 4,,10
	.p2align 3
.L946:
	movq	-144(%rbp), %r15
	movl	-96(%rbp), %ecx
	xorl	%r8d, %r8d
	movq	128(%r15), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r15), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L895
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L945:
	leal	-1(%rax), %ecx
	cmpb	$4, %cl
	ja	.L872
	movq	(%rdi), %rcx
	subl	$3, %eax
	movl	4(%rcx), %esi
	cmpb	$2, %al
	jbe	.L950
	movl	%r14d, 76(%rdx)
	movl	%esi, 80(%rdx)
	movl	$252, 84(%rdx)
	movq	144(%r12), %rdx
	movzbl	16(%rdx), %eax
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L860:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseYieldExpressionEv
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L948:
	shrl	$4, %r13d
	andl	$15, %r13d
	subl	$1, %r13d
	cmpl	$1, %r13d
	ja	.L901
	movq	16(%r12), %rax
	addl	$1, 16(%rax)
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L949:
	movq	(%rax), %rbx
	xorl	%r8d, %r8d
	movl	%r9d, %edx
	movl	$252, %ecx
	movl	%r14d, %esi
	movq	128(%rbx), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%rbx), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L901
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L869:
	movq	(%rdi), %rdx
	movl	4(%rdx), %r9d
	cmpl	$2, %eax
	jne	.L883
	movl	%r13d, %eax
	shrl	$4, %eax
	andl	$15, %eax
	subl	$1, %eax
	cmpl	$3, %eax
	ja	.L871
	movq	144(%r12), %r15
	movzbl	16(%r15), %eax
	leal	-1(%rax), %edx
	cmpb	$4, %dl
	ja	.L884
	subl	$3, %eax
	cmpb	$2, %al
	jbe	.L951
	movl	%r14d, 76(%r15)
	movl	%r9d, 80(%r15)
	movl	$246, 84(%r15)
.L938:
	movq	144(%r12), %r15
	movzbl	16(%r15), %eax
.L884:
	cmpb	$2, %al
	ja	.L939
	movl	60(%r15), %eax
	cmpl	%eax, 56(%r15)
	jbe	.L952
.L888:
	movl	$4294967295, %eax
	movl	$0, 52(%r15)
	movq	%rax, 64(%r15)
	movq	200(%r12), %rdi
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L878:
	movq	8(%rax), %r15
	movzwl	40(%r15), %eax
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$1, %dl
	je	.L939
	movq	16(%r15), %rdx
	testq	%rdx, %rdx
	je	.L880
	testb	$64, %ah
	je	.L953
.L880:
	orb	$64, %ah
	movw	%ax, 40(%r15)
	movq	200(%r12), %rdi
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L943:
	cmpl	$3, %eax
	je	.L866
	andl	$8, %r13d
	je	.L867
.L866:
	movq	248(%r12), %rax
	leaq	-176(%rbp), %rsi
	movq	%r12, %rdi
	movb	$0, -168(%rbp)
	movq	$0, -164(%rbp)
	movl	%r14d, 112(%rax)
	movl	236(%r12), %ebx
	movq	%rax, -176(%rbp)
	movzbl	131(%rax), %eax
	cmpl	%ebx, 232(%r12)
	movb	$0, -156(%rbp)
	setbe	-155(%rbp)
	andl	$1, %eax
	movb	%al, -167(%rbp)
	movl	$4294967295, %eax
	movq	$0, 248(%r12)
	movq	%rax, 232(%r12)
	movl	$0, 240(%r12)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseArrowFunctionLiteralERKNS0_25PreParserFormalParametersE
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L944:
	movq	(%rdi), %rax
	movl	4(%rax), %r9d
.L871:
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$250, %r8d
	movl	%r9d, %ecx
	movl	%r14d, %edx
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE33RewriteInvalidReferenceExpressionENS0_19PreParserExpressionEiiNS0_15MessageTemplateE
	movq	200(%r12), %rdi
	movl	%eax, %r13d
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L950:
	movl	%r14d, %eax
	movq	(%rdx), %rdi
	salq	$32, %rsi
	movl	$252, %edx
	orq	%rax, %rsi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	movq	144(%r12), %rdx
	movzbl	16(%rdx), %eax
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L867:
	movq	(%rdi), %rax
	movl	$270, %edx
	movq	%r12, %rdi
	movl	(%rax), %esi
	movq	%rsi, %rax
	movl	$4294967295, %esi
	salq	$32, %rax
	orq	%rax, %rsi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	movl	$1, %eax
	jmp	.L861
.L953:
	movzwl	40(%rdx), %ecx
	movl	%ecx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L880
	movq	16(%rdx), %rax
	testq	%rax, %rax
	je	.L881
	testb	$64, %ch
	je	.L954
.L881:
	orb	$64, %ch
	movw	%cx, 40(%rdx)
	movzwl	40(%r15), %eax
	jmp	.L880
.L883:
	subl	$6, %eax
	cmpl	$1, %eax
	ja	.L871
	cmpb	$17, %bl
	jne	.L871
	movq	144(%r12), %r15
	movzbl	16(%r15), %eax
	testb	$8, %r13b
	je	.L891
	salq	$32, %r9
	movl	%r14d, %esi
	subl	$3, %eax
	orq	%r9, %rsi
	cmpb	$2, %al
	ja	.L892
	movl	$252, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
.L893:
	movq	200(%r12), %rdi
	movq	144(%r12), %r15
	movq	(%rdi), %rdx
	movzbl	16(%r15), %eax
	movl	4(%rdx), %r9d
.L891:
	cmpb	$2, %al
	ja	.L876
	movl	%r9d, %ecx
	movl	%r14d, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE15ValidatePatternENS0_19PreParserExpressionEii
	movl	$4294967295, %eax
	movl	$0, 48(%r15)
	movq	%rax, 56(%r15)
	jmp	.L939
.L952:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE8ValidateEi.part.0
	jmp	.L888
.L951:
	movq	(%r15), %r15
	xorl	%r8d, %r8d
	movl	%r9d, %edx
	movl	$246, %ecx
	movl	%r14d, %esi
	movq	128(%r15), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r15), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L938
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L938
.L954:
	movzwl	40(%rax), %esi
	movl	%esi, %edi
	andl	$15, %edi
	cmpb	$1, %dil
	je	.L881
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.L882
	testw	$16384, %si
	je	.L955
.L882:
	orw	$16384, %si
	movw	%si, 40(%rax)
	movzwl	40(%rdx), %ecx
	jmp	.L881
.L892:
	movl	$250, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	jmp	.L893
.L942:
	call	__stack_chk_fail@PLT
.L955:
	movq	%rax, -192(%rbp)
	movq	%rdx, -184(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %rdx
	movzwl	40(%rax), %esi
	jmp	.L882
	.cfi_endproc
.LFE25168:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseAssignmentExpressionCoverGrammarEv, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseAssignmentExpressionCoverGrammarEv
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseMemberInitializerEPNS3_9ClassInfoEib,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseMemberInitializerEPNS3_9ClassInfoEib,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseMemberInitializerEPNS3_9ClassInfoEib
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseMemberInitializerEPNS3_9ClassInfoEib, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseMemberInitializerEPNS3_9ClassInfoEib:
.LFB26054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	40(%rsi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	%cl, %cl
	cmovne	32(%rsi), %rbx
	testq	%rbx, %rbx
	je	.L974
.L958:
	movq	200(%r13), %rdi
	movl	$2, %eax
	movq	8(%rdi), %rdx
	cmpb	$17, 56(%rdx)
	je	.L975
.L961:
	movq	(%rdi), %rdx
	movl	4(%rdx), %edx
	movl	%edx, 116(%rbx)
	testb	%r14b, %r14b
	je	.L966
	movq	%rbx, 32(%r12)
	movb	$1, 23(%r12)
.L967:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L976
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L966:
	.cfi_restore_state
	movq	%rbx, 40(%r12)
	movb	$1, 24(%r12)
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L974:
	movq	136(%rdi), %r9
	movl	%edx, %r15d
	movq	(%rdi), %rdx
	movq	16(%r9), %rbx
	movq	24(%r9), %rax
	subq	%rbx, %rax
	cmpq	$223, %rax
	jbe	.L977
	leaq	224(%rbx), %rax
	movq	%rax, 16(%r9)
.L960:
	movq	%r9, %rsi
	movl	$17, %r8d
	movl	$2, %ecx
	movq	%rbx, %rdi
	call	_ZN2v88internal16DeclarationScopeC1EPNS0_4ZoneEPNS0_5ScopeENS0_9ScopeTypeENS0_12FunctionKindE@PLT
	movq	16(%r13), %rax
	movq	%rbx, %rdi
	movb	$1, 59(%rax)
	movq	40(%r13), %rsi
	call	_ZN2v88internal16DeclarationScope31DeclareDefaultFunctionVariablesEPNS0_15AstValueFactoryE@PLT
	orb	$1, 129(%rbx)
	movl	%r15d, 112(%rbx)
	jmp	.L958
	.p2align 4,,10
	.p2align 3
.L975:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	0(%r13), %rax
	movq	%rbx, 0(%r13)
	leaq	-208(%rbp), %rdx
	movq	%r13, -208(%rbp)
	movq	%rax, -200(%rbp)
	leaq	16(%r13), %rax
	movq	%rax, -176(%rbp)
	movq	16(%r13), %rax
	movq	$0, -192(%rbp)
	movl	$0, -184(%rbp)
	movq	%rax, -168(%rbp)
	movq	%rbx, -160(%rbp)
	movl	$0, -152(%rbp)
	movq	%rdx, 16(%r13)
	testq	%rax, %rax
	je	.L962
	movzbl	57(%rax), %edx
	movb	$0, 57(%rax)
	movb	%dl, 58(%rax)
.L962:
	movzbl	256(%r13), %r15d
	movb	$1, 256(%r13)
	leaq	-144(%rbp), %r8
	movq	%r13, %rsi
	movq	%r8, %rdi
	movq	%r8, -216(%rbp)
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPS3_NS0_15ExpressionScopeIS4_E9ScopeTypeE.constprop.0
	movq	%r13, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseAssignmentExpressionCoverGrammarEv
	movl	-84(%rbp), %esi
	cmpl	%esi, -88(%rbp)
	movq	-216(%rbp), %r8
	jbe	.L978
.L963:
	movq	-120(%rbp), %rdi
	movq	-112(%rbp), %rsi
	movq	8(%rdi), %r8
	movq	(%rdi), %rcx
	movq	%r8, %rdx
	subq	%rcx, %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	ja	.L979
	jnb	.L965
	salq	$4, %rsi
	addq	%rcx, %rsi
	cmpq	%rsi, %r8
	je	.L965
	movq	%rsi, 8(%rdi)
.L965:
	movq	-144(%rbp), %rdx
	movq	-136(%rbp), %rcx
	movq	%rcx, 144(%rdx)
	movq	-176(%rbp), %rdx
	movq	-168(%rbp), %rcx
	movb	%r15b, 256(%r13)
	movq	%rcx, (%rdx)
	movq	-208(%rbp), %rdx
	movq	-200(%rbp), %rcx
	movq	%rcx, (%rdx)
	movq	200(%r13), %rdi
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L979:
	subq	%rdx, %rsi
	movl	%eax, -216(%rbp)
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm
	movl	-216(%rbp), %eax
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L978:
	xorl	%esi, %esi
	movq	%r8, %rdi
	movl	%eax, -216(%rbp)
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE8ValidateEi.part.0
	movl	-216(%rbp), %eax
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L977:
	movq	%r9, %rdi
	movl	$224, %esi
	movq	%rdx, -224(%rbp)
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-216(%rbp), %r9
	movq	-224(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L960
.L976:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26054:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseMemberInitializerEPNS3_9ClassInfoEib, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseMemberInitializerEPNS3_9ClassInfoEib
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseConditionalContinuationENS0_19PreParserExpressionEi,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseConditionalContinuationENS0_19PreParserExpressionEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseConditionalContinuationENS0_19PreParserExpressionEi
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseConditionalContinuationENS0_19PreParserExpressionEi, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseConditionalContinuationENS0_19PreParserExpressionEi:
.LFB25922:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	200(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	144(%r12), %rdx
	movq	%r12, -144(%rbp)
	movzbl	256(%r12), %r14d
	movb	$0, -128(%rbp)
	movb	$1, 256(%r12)
	movq	%rdx, -136(%rbp)
	testq	%rdx, %rdx
	je	.L981
	cmpb	$0, 17(%rdx)
	setne	-127(%rbp)
	movzbl	18(%rdx), %eax
.L1001:
	movb	%al, -126(%rbp)
	movq	184(%r12), %rax
	leaq	-144(%rbp), %r13
	leaq	176(%r12), %rbx
	subq	176(%r12), %rax
	movq	%r13, 144(%r12)
	sarq	$4, %rax
	movq	%rbx, -120(%rbp)
	movq	%rax, %xmm0
	xorl	%eax, %eax
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movups	%xmm0, -88(%rbp)
	testq	%rdx, %rdx
	je	.L984
	cmpb	$2, 16(%rdx)
	jbe	.L1014
.L984:
	movq	%r12, %rdi
	movb	%al, -72(%rbp)
	movq	$0, -96(%rbp)
	movl	$-1, -88(%rbp)
	movl	$-1, -80(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseAssignmentExpressionCoverGrammarEv
	movl	-88(%rbp), %esi
	movl	-84(%rbp), %edx
	cmpl	%edx, %esi
	jbe	.L1015
.L985:
	movq	-120(%rbp), %rdi
	movq	-112(%rbp), %rsi
	movq	8(%rdi), %rcx
	movq	(%rdi), %rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	ja	.L1016
	jb	.L1017
.L988:
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	%rdx, 144(%rax)
	movq	200(%r12), %rdi
	movb	%r14b, 256(%r12)
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$9, %al
	jne	.L1018
.L989:
	movq	144(%r12), %rdx
	movq	%r12, -144(%rbp)
	movb	$0, -128(%rbp)
	movq	%rdx, -136(%rbp)
	testq	%rdx, %rdx
	je	.L990
	cmpb	$0, 17(%rdx)
	setne	-127(%rbp)
	movzbl	18(%rdx), %eax
.L1000:
	movb	%al, -126(%rbp)
	movq	184(%r12), %rax
	subq	176(%r12), %rax
	movq	%r13, 144(%r12)
	sarq	$4, %rax
	movq	%rbx, -120(%rbp)
	movq	%rax, %xmm0
	xorl	%eax, %eax
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movups	%xmm0, -88(%rbp)
	testq	%rdx, %rdx
	je	.L993
	cmpb	$2, 16(%rdx)
	jbe	.L1019
.L993:
	movq	%r12, %rdi
	movb	%al, -72(%rbp)
	movq	$0, -96(%rbp)
	movl	$-1, -88(%rbp)
	movl	$-1, -80(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseAssignmentExpressionCoverGrammarEv
	movl	-88(%rbp), %esi
	movl	-84(%rbp), %edx
	cmpl	%edx, %esi
	jbe	.L1020
.L994:
	movq	-120(%rbp), %rdi
	movq	-112(%rbp), %rsi
	movq	8(%rdi), %rcx
	movq	(%rdi), %rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	ja	.L1021
	jnb	.L997
	salq	$4, %rsi
	addq	%rdx, %rsi
	cmpq	%rsi, %rcx
	je	.L997
	movq	%rsi, 8(%rdi)
.L997:
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	%rdx, 144(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1022
	addq	$104, %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1019:
	.cfi_restore_state
	movzbl	72(%rdx), %eax
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L1014:
	movzbl	72(%rdx), %eax
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1017:
	salq	$4, %rsi
	addq	%rdx, %rsi
	cmpq	%rsi, %rcx
	je	.L988
	movq	%rsi, 8(%rdi)
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L1021:
	subq	%rax, %rsi
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm
	jmp	.L997
	.p2align 4,,10
	.p2align 3
.L1020:
	movq	-144(%rbp), %rbx
	movl	-96(%rbp), %ecx
	xorl	%r8d, %r8d
	movq	128(%rbx), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%rbx), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L994
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L994
	.p2align 4,,10
	.p2align 3
.L1016:
	subq	%rax, %rsi
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L1015:
	movq	-144(%rbp), %r15
	movl	-96(%rbp), %ecx
	xorl	%r8d, %r8d
	movq	128(%r15), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r15), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L985
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L985
	.p2align 4,,10
	.p2align 3
.L990:
	movb	$0, -127(%rbp)
	xorl	%eax, %eax
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L981:
	movb	$0, -127(%rbp)
	xorl	%eax, %eax
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1018:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L989
.L1022:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25922:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseConditionalContinuationENS0_19PreParserExpressionEi, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseConditionalContinuationENS0_19PreParserExpressionEi
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseArgumentsEPNS0_23PreParserExpressionListEPbNS0_20ParsingArrowHeadFlagE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseArgumentsEPNS0_23PreParserExpressionListEPbNS0_20ParsingArrowHeadFlagE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseArgumentsEPNS0_23PreParserExpressionListEPbNS0_20ParsingArrowHeadFlagE
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseArgumentsEPNS0_23PreParserExpressionListEPbNS0_20ParsingArrowHeadFlagE, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseArgumentsEPNS0_23PreParserExpressionListEPbNS0_20ParsingArrowHeadFlagE:
.LFB26294:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movl	%ecx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, (%rdx)
	movq	200(%rdi), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	144(%r13), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEEC1EPNS0_15ExpressionScopeIS4_EE
	movq	200(%r13), %rdi
	movq	8(%rdi), %rdx
	movzbl	56(%rdx), %eax
	cmpb	$6, %al
	je	.L1024
	xorl	%r15d, %r15d
.L1044:
	cmpb	$10, %al
	je	.L1077
	movl	(%rdx), %eax
	movq	%r13, %rdi
	movl	%eax, -108(%rbp)
	movzbl	256(%r13), %eax
	movb	$1, 256(%r13)
	movb	%al, -97(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseAssignmentExpressionCoverGrammarEv
	cmpl	$1, -104(%rbp)
	je	.L1078
.L1028:
	addl	$1, (%rbx)
	movq	144(%r13), %rdx
	movq	200(%r13), %rdi
	cmpb	$2, 16(%rdx)
	movq	8(%rdi), %rcx
	ja	.L1039
	movq	32(%rdx), %rax
	movl	40(%rdx), %esi
	subl	%eax, %esi
	je	.L1056
	subl	$1, %esi
	cmpl	%r15d, %esi
	jl	.L1057
	movl	(%rcx), %edi
	movslq	%esi, %rcx
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L1040:
	subq	$1, %rcx
	movl	%edi, 8(%rax)
	cmpl	%ecx, %r15d
	jg	.L1076
	movq	32(%rdx), %rax
.L1042:
	movq	24(%rdx), %r10
	addq	%rcx, %rax
	salq	$4, %rax
	addq	(%r10), %rax
	cmpl	$-1, 8(%rax)
	je	.L1040
.L1076:
	movq	200(%r13), %rdi
	movl	%esi, %r15d
	movq	8(%rdi), %rcx
.L1039:
	cmpb	$30, 56(%rcx)
	je	.L1079
	movzbl	-97(%rbp), %eax
	movb	%al, 256(%r13)
	cmpl	$65534, (%rbx)
	jg	.L1053
	movq	(%rdi), %rax
	movl	(%rax), %esi
	movl	4(%rax), %edx
	movq	8(%rdi), %rax
	cmpb	$6, 56(%rax)
	je	.L1054
	movq	128(%r13), %rdi
	xorl	%r8d, %r8d
	movl	$328, %ecx
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r13), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	je	.L1080
.L1048:
	movq	%r14, %rdi
	call	_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEED1Ev
.L1023:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1081
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1080:
	.cfi_restore_state
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L1048
	.p2align 4,,10
	.p2align 3
.L1079:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movzbl	-97(%rbp), %eax
	movq	200(%r13), %rdi
	movb	%al, 256(%r13)
	movq	8(%rdi), %rdx
	movzbl	56(%rdx), %eax
	cmpb	$6, %al
	jne	.L1044
	.p2align 4,,10
	.p2align 3
.L1024:
	cmpl	$65534, (%rbx)
	jle	.L1054
	.p2align 4,,10
	.p2align 3
.L1053:
	movq	%r13, %rdi
	movl	$305, %esi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	movq	%r14, %rdi
	call	_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEED1Ev
	jmp	.L1023
	.p2align 4,,10
	.p2align 3
.L1078:
	movl	-108(%rbp), %edx
	movl	%eax, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ClassifyArrowParameterEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEEiNS0_19PreParserExpressionE
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L1056:
	xorl	%r15d, %r15d
	jmp	.L1039
	.p2align 4,,10
	.p2align 3
.L1077:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r13), %rax
	movq	%r13, %rdi
	movq	8(%rax), %rax
	movl	(%rax), %eax
	movl	%eax, -108(%rbp)
	movzbl	256(%r13), %eax
	movb	$1, 256(%r13)
	movb	%al, -97(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseAssignmentExpressionCoverGrammarEv
	cmpl	$1, -104(%rbp)
	je	.L1082
.L1027:
	movb	$1, (%r12)
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L1054:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	jmp	.L1048
	.p2align 4,,10
	.p2align 3
.L1082:
	movl	-108(%rbp), %edx
	movl	%eax, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, -112(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ClassifyArrowParameterEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEEiNS0_19PreParserExpressionE
	movq	144(%r13), %rcx
	movzbl	16(%rcx), %eax
	leal	-1(%rax), %edx
	movl	-112(%rbp), %eax
	cmpb	$1, %dl
	ja	.L1051
	movb	$0, 88(%rcx)
.L1051:
	movl	%eax, %ecx
	movq	200(%r13), %rdx
	andl	$7, %ecx
	cmpl	$2, %ecx
	je	.L1083
.L1029:
	movq	8(%rdx), %rcx
	cmpb	$30, 56(%rcx)
	jne	.L1027
	movq	144(%r13), %rdx
	cmpb	$2, 16(%rdx)
	ja	.L1027
	movq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L1038:
	cmpb	$0, 72(%rdx)
	je	.L1027
	cmpb	$2, 16(%rax)
	je	.L1084
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L1038
	jmp	.L1027
	.p2align 4,,10
	.p2align 3
.L1083:
	shrl	$4, %eax
	andl	$15, %eax
	cmpl	$9, %eax
	jne	.L1029
	movq	144(%r13), %rcx
	cmpb	$2, 16(%rcx)
	ja	.L1027
	movq	(%rdx), %rsi
	movq	%rcx, %rax
	.p2align 4,,10
	.p2align 3
.L1034:
	cmpb	$0, 72(%rcx)
	je	.L1032
	cmpb	$2, 16(%rax)
	je	.L1085
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L1034
.L1032:
	movq	200(%r13), %rdx
	jmp	.L1029
	.p2align 4,,10
	.p2align 3
.L1084:
	movq	(%rcx), %rsi
	movl	$279, 84(%rax)
	movq	%rsi, 76(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L1038
	jmp	.L1027
	.p2align 4,,10
	.p2align 3
.L1057:
	movl	%esi, %r15d
	jmp	.L1039
	.p2align 4,,10
	.p2align 3
.L1085:
	movq	(%rsi), %rdx
	movl	$288, 84(%rax)
	movq	%rdx, 76(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L1034
	jmp	.L1032
.L1081:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26294:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseArgumentsEPNS0_23PreParserExpressionListEPbNS0_20ParsingArrowHeadFlagE, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseArgumentsEPNS0_23PreParserExpressionListEPbNS0_20ParsingArrowHeadFlagE
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE16ParseV8IntrinsicEv,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE16ParseV8IntrinsicEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE16ParseV8IntrinsicEv
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE16ParseV8IntrinsicEv, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE16ParseV8IntrinsicEv:
.LFB26420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	200(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movl	(%rax), %ebx
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	16(%r12), %rax
	movq	48(%rax), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movq	200(%r12), %rdi
	movzbl	133(%rax), %r13d
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$0, 113(%r12)
	je	.L1087
	leal	-92(%rax), %edx
	cmpb	$3, %dl
	ja	.L1106
.L1088:
	movq	%r12, %rdi
	call	_ZNK2v88internal9PreParser13GetIdentifierEv
.L1093:
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	cmpb	$5, 56(%rax)
	je	.L1094
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	movl	$1, %eax
.L1095:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1107
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1087:
	.cfi_restore_state
	leal	-92(%rax), %edx
	leal	-9(%r13), %ecx
	cmpb	$3, %dl
	jbe	.L1088
	movq	(%r12), %rdx
	movzbl	129(%rdx), %edx
	andl	$1, %edx
	cmpb	$96, %al
	jne	.L1090
	cmpb	$4, %cl
	ja	.L1088
	.p2align 4,,10
	.p2align 3
.L1089:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1094:
	xorl	%ecx, %ecx
	leaq	-45(%rbp), %rdx
	leaq	-44(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -44(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseArgumentsEPNS0_23PreParserExpressionListEPbNS0_20ParsingArrowHeadFlagE
	cmpb	$0, -45(%rbp)
	movl	$2, %eax
	je	.L1095
	movq	200(%r12), %rax
	movl	$244, %edx
	movq	%r12, %rdi
	movq	(%rax), %rax
	movl	(%rax), %esi
	movq	%rsi, %rax
	movl	%ebx, %esi
	salq	$32, %rax
	orq	%rax, %rsi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateE
	movl	$1, %eax
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1106:
	movq	(%r12), %rdx
	movzbl	129(%rdx), %edx
	andl	$1, %edx
	cmpb	$96, %al
	je	.L1089
.L1090:
	cmpb	$97, %al
	jne	.L1091
	subl	$12, %r13d
	cmpb	$3, %r13b
	jbe	.L1089
.L1105:
	testb	%dl, %dl
	jne	.L1089
	jmp	.L1088
	.p2align 4,,10
	.p2align 3
.L1091:
	subl	$97, %eax
	cmpb	$4, %al
	jbe	.L1105
	jmp	.L1089
.L1107:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26420:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE16ParseV8IntrinsicEv, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE16ParseV8IntrinsicEv
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseYieldExpressionEv,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseYieldExpressionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseYieldExpressionEv
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseYieldExpressionEv, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseYieldExpressionEv:
.LFB25559:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	200(%rdi), %rax
	movq	%rdi, %rbx
	movq	144(%rdi), %rcx
	movq	8(%rax), %rsi
	movq	%rcx, %rax
	.p2align 4,,10
	.p2align 3
.L1113:
	movzbl	16(%rax), %edx
	cmpb	$3, %dl
	je	.L1109
.L1137:
	cmpb	$0, 17(%rcx)
	je	.L1110
	subl	$1, %edx
	cmpb	$2, %dl
	ja	.L1111
	movq	(%rsi), %rdx
	movl	$336, 84(%rax)
	movq	%rdx, 76(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L1113
	.p2align 4,,10
	.p2align 3
.L1110:
	movq	200(%rbx), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%rbx), %rax
	movq	(%rax), %rax
	movl	4(%rax), %edx
	subl	(%rax), %edx
	cmpb	$90, 56(%rax)
	jne	.L1114
	subl	$2, %edx
.L1114:
	cmpb	$0, 28(%rax)
	movl	24(%rax), %ecx
	jne	.L1115
	sarl	%ecx
.L1115:
	cmpl	%ecx, %edx
	jne	.L1134
.L1116:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	120(%rbx), %rax
	jnb	.L1117
	movq	200(%rbx), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	je	.L1135
.L1118:
	movq	128(%rbx), %rax
	movl	$257, %edx
	movw	%dx, (%rax)
.L1117:
	movq	200(%rbx), %rdi
	movq	8(%rdi), %rax
	cmpb	$0, 76(%rax)
	jne	.L1119
	movzbl	56(%rax), %eax
	cmpb	$40, %al
	je	.L1136
	cmpb	$62, %al
	ja	.L1124
	movabsq	$4611686019501159104, %rdx
	btq	%rax, %rdx
	jc	.L1119
.L1124:
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseAssignmentExpressionCoverGrammarEv
.L1119:
	movq	16(%rbx), %rax
	addl	$1, 20(%rax)
.L1131:
	popq	%rbx
	movl	$2, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1111:
	.cfi_restore_state
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1110
	movzbl	16(%rax), %edx
	cmpb	$3, %dl
	jne	.L1137
.L1109:
	movq	(%rcx), %r12
	movl	4(%rsi), %edx
	xorl	%r8d, %r8d
	movl	$336, %ecx
	movl	(%rsi), %esi
	movq	128(%r12), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r12), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L1110
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1135:
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1136:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseAssignmentExpressionCoverGrammarEv
	movq	16(%rbx), %rax
	addl	$1, 20(%rax)
	movq	16(%rbx), %rax
	movq	48(%rax), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$12, %eax
	cmpb	$1, %al
	ja	.L1131
	movq	16(%rbx), %rax
	addl	$1, 20(%rax)
	movq	16(%rbx), %rax
	addl	$1, 20(%rax)
	movq	16(%rbx), %rax
	addl	$1, 20(%rax)
	jmp	.L1131
	.p2align 4,,10
	.p2align 3
.L1134:
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L1116
	.cfi_endproc
.LFE25559:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseYieldExpressionEv, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseYieldExpressionEv
	.section	.rodata._ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseImportExpressionsEv.str1.1,"aMS",@progbits,1
.LC3:
	.string	"import.meta"
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseImportExpressionsEv,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseImportExpressionsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseImportExpressionsEv
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseImportExpressionsEv, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseImportExpressionsEv:
.LFB26418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	200(%rdi), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rdi
	cmpb	$0, 259(%r12)
	movq	8(%rdi), %rax
	movq	(%rdi), %rcx
	movzbl	56(%rax), %eax
	je	.L1140
	cmpb	$2, %al
	je	.L1166
.L1140:
	cmpb	$5, %al
	jne	.L1167
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rax
	movq	8(%rax), %rdx
	cmpb	$6, 56(%rdx)
	je	.L1168
	movzbl	256(%r12), %ebx
	movq	%r12, %rdi
	movb	$1, 256(%r12)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseAssignmentExpressionCoverGrammarEv
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$6, %al
	jne	.L1169
.L1156:
	movb	%bl, 256(%r12)
	movl	$2, %eax
.L1152:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1168:
	.cfi_restore_state
	movq	(%rax), %rax
	movq	128(%r12), %rdi
	xorl	%r8d, %r8d
	movl	$61, %ecx
	movl	4(%rax), %edx
	movl	(%rax), %esi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r12), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L1154
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L1154:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1166:
	.cfi_restore_state
	movl	(%rcx), %r14d
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	40(%r12), %rax
	movq	200(%r12), %rdi
	movq	56(%rax), %rax
	movq	336(%rax), %r13
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$92, %al
	jne	.L1170
.L1141:
	movq	40(%r12), %rsi
	movq	200(%r12), %rdi
	call	_ZNK2v88internal7Scanner13CurrentSymbolEPNS0_15AstValueFactoryE@PLT
	cmpq	%rax, %r13
	jne	.L1171
.L1142:
	movq	200(%r12), %rax
	movq	(%rax), %rax
	movl	4(%rax), %r9d
	movl	(%rax), %esi
	movl	%r9d, %ecx
	subl	%esi, %ecx
	cmpb	$90, 56(%rax)
	je	.L1172
.L1143:
	cmpb	$0, 28(%rax)
	movl	24(%rax), %edx
	je	.L1173
.L1144:
	cmpl	%edx, %ecx
	jne	.L1174
.L1145:
	cmpb	$0, 113(%r12)
	movl	$2, %eax
	jne	.L1152
	movq	200(%r12), %rax
	movl	$60, %edx
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	movl	$1, %eax
	jmp	.L1152
	.p2align 4,,10
	.p2align 3
.L1167:
	cmpb	$0, 113(%r12)
	jne	.L1153
	movq	(%rcx), %rsi
	movl	$59, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	jmp	.L1154
	.p2align 4,,10
	.p2align 3
.L1169:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L1156
	.p2align 4,,10
	.p2align 3
.L1153:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L1154
	.p2align 4,,10
	.p2align 3
.L1173:
	sarl	%edx
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1172:
	subl	$2, %ecx
	jmp	.L1143
.L1174:
	cmpl	$-1, %r14d
	movl	%r9d, %edx
	movq	128(%r12), %rdi
	leaq	.LC3(%rip), %r8
	cmove	%esi, %r14d
	movl	$249, %ecx
	movl	%r14d, %esi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r12), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L1145
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L1145
.L1171:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L1142
.L1170:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L1141
	.cfi_endproc
.LFE26418:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseImportExpressionsEv, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseImportExpressionsEv
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE36ParsePossibleDestructuringSubPatternEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE36ParsePossibleDestructuringSubPatternEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE36ParsePossibleDestructuringSubPatternEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEE
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE36ParsePossibleDestructuringSubPatternEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEE, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE36ParsePossibleDestructuringSubPatternEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEE:
.LFB25914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	testq	%rsi, %rsi
	je	.L1176
	movq	(%rsi), %rax
	testq	%rax, %rax
	je	.L1176
	movl	16(%rsi), %esi
	cmpl	%esi, 20(%rbx)
	jnb	.L1177
	movl	48(%rax), %edx
	movl	%edx, 8(%rbx)
	movq	56(%rax), %rdx
	movq	%rdx, 16(%rbx)
.L1177:
	movl	$4294967295, %edi
	movl	$0, 48(%rax)
	movq	%rdi, 56(%rax)
	movq	(%rbx), %rax
	movl	28(%rbx), %ecx
	cmpl	%ecx, 24(%rbx)
	ja	.L1229
.L1178:
	movl	$4294967295, %esi
	movl	$0, 52(%rax)
	movq	%rsi, 64(%rax)
.L1176:
	movq	200(%r12), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rax
	movl	(%rax), %r13d
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseAssignmentExpressionCoverGrammarEv
	movl	%eax, %edx
	andl	$7, %edx
	cmpl	$3, %edx
	jne	.L1179
	movl	%eax, %edx
	movl	%eax, %ecx
	shrl	$4, %edx
	andl	$8, %ecx
	leal	-2(%rdx), %ebx
	movq	(%r12), %rdx
	testb	$1, 129(%rdx)
	je	.L1230
	cmpb	$1, %bl
	jbe	.L1186
	testl	%ecx, %ecx
	je	.L1200
	movq	200(%r12), %rdx
	movq	(%rdx), %rdx
	movl	4(%rdx), %esi
.L1181:
	movq	144(%r12), %rcx
	movzbl	16(%rcx), %edx
	leal	-1(%rdx), %edi
	cmpb	$4, %dil
	ja	.L1182
	subl	$3, %edx
	cmpb	$2, %dl
	jbe	.L1231
	movl	%r13d, 76(%rcx)
	movl	%esi, 80(%rcx)
	movl	$252, 84(%rcx)
	movq	200(%r12), %rdx
	movq	(%rdx), %rdx
	movl	4(%rdx), %esi
	.p2align 4,,10
	.p2align 3
.L1182:
	cmpb	$1, %bl
	ja	.L1200
	movq	144(%r12), %rdx
	movzbl	16(%rdx), %ecx
	leal	-1(%rcx), %edi
	cmpb	$2, %dil
	ja	.L1200
	movq	(%rdx), %rdi
	cmpb	$3, %cl
	je	.L1232
	movl	%r13d, 232(%rdi)
	movl	%esi, 236(%rdi)
	movq	(%rdx), %rdx
	movl	$295, 240(%rdx)
.L1200:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1229:
	.cfi_restore_state
	movl	52(%rax), %edx
	movl	%edx, 12(%rbx)
	movq	64(%rax), %rdx
	movq	%rdx, 24(%rbx)
	jmp	.L1178
	.p2align 4,,10
	.p2align 3
.L1230:
	movq	200(%r12), %rdx
	movq	(%rdx), %rdx
	movl	4(%rdx), %esi
	testl	%ecx, %ecx
	je	.L1182
	jmp	.L1181
	.p2align 4,,10
	.p2align 3
.L1185:
	movl	%eax, %edx
	shrl	$4, %edx
	andl	$15, %edx
	leal	-1(%rdx), %ecx
	cmpl	$3, %ecx
	jbe	.L1233
	testb	$8, %al
	jne	.L1186
	cmpl	$9, %edx
	je	.L1200
	.p2align 4,,10
	.p2align 3
.L1186:
	movq	144(%r12), %rdx
	movq	200(%r12), %rcx
	movzbl	16(%rdx), %edi
	movq	(%rcx), %rcx
	movl	4(%rcx), %r9d
	leal	-3(%rdi), %ecx
	cmpb	$2, %cl
	jbe	.L1234
	movl	68(%rdx), %edi
	cmpl	%edi, 64(%rdx)
	jbe	.L1200
	movl	$252, 52(%rdx)
	movl	%r13d, 64(%rdx)
	movl	%r9d, 68(%rdx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1179:
	.cfi_restore_state
	cmpl	$2, %edx
	je	.L1185
	testb	$8, %al
	jne	.L1186
	subl	$6, %edx
	cmpl	$1, %edx
	ja	.L1186
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1234:
	.cfi_restore_state
	movq	(%rdx), %rbx
	xorl	%r8d, %r8d
	movl	$252, %ecx
	movl	%r9d, %edx
	movl	%r13d, %esi
	movl	%eax, -36(%rbp)
	movq	128(%rbx), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%rbx), %rdx
	movl	-36(%rbp), %eax
	movq	24(%rdx), %rcx
	cmpb	$0, 48(%rcx)
	jne	.L1200
	movl	$-1, 32(%rdx)
	movq	24(%rcx), %rsi
	movb	$1, 48(%rcx)
	movq	%rsi, 16(%rcx)
	movb	$109, 96(%rdx)
	movb	$109, 176(%rdx)
	movb	$109, 256(%rdx)
	jmp	.L1200
	.p2align 4,,10
	.p2align 3
.L1233:
	movq	144(%r12), %rcx
	movzbl	16(%rcx), %edx
	leal	-1(%rdx), %esi
	cmpb	$4, %sil
	ja	.L1189
	movq	200(%r12), %rsi
	subl	$3, %edx
	movq	(%rsi), %rsi
	movl	4(%rsi), %esi
	cmpb	$2, %dl
	jbe	.L1235
	movl	%r13d, 76(%rcx)
	movl	%esi, 80(%rcx)
	movl	$246, 84(%rcx)
.L1189:
	testq	%rbx, %rbx
	je	.L1200
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1200
	movl	60(%rdi), %ecx
	cmpl	%ecx, 56(%rdi)
	jbe	.L1236
.L1202:
	movl	$4294967295, %esi
	movl	$0, 52(%rdi)
	movq	%rsi, 64(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1235:
	.cfi_restore_state
	movq	(%rcx), %rdi
	salq	$32, %rsi
	movl	$246, %edx
	movl	%eax, -36(%rbp)
	orq	%r13, %rsi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	movl	-36(%rbp), %eax
	jmp	.L1189
	.p2align 4,,10
	.p2align 3
.L1231:
	movq	%rsi, %rdx
	movq	(%rcx), %rdi
	movl	%r13d, %esi
	movl	%eax, -36(%rbp)
	salq	$32, %rdx
	orq	%rdx, %rsi
	movl	$252, %edx
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	movq	200(%r12), %rdx
	movl	-36(%rbp), %eax
	movq	(%rdx), %rdx
	movl	4(%rdx), %esi
	jmp	.L1182
	.p2align 4,,10
	.p2align 3
.L1236:
	xorl	%esi, %esi
	movl	%eax, -36(%rbp)
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE8ValidateEi.part.0
	movq	(%rbx), %rdi
	movl	-36(%rbp), %eax
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1232:
	movq	(%rdi), %rdx
	testb	$1, 129(%rdx)
	je	.L1199
	salq	$32, %rsi
	movl	$295, %edx
	movl	%eax, -36(%rbp)
	orq	%r13, %rsi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	movl	-36(%rbp), %eax
	jmp	.L1200
.L1199:
	movq	224(%rdi), %rdx
	cmpl	%esi, %r13d
	setbe	21(%rdx)
	jmp	.L1200
	.cfi_endproc
.LFE25914:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE36ParsePossibleDestructuringSubPatternEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEE, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE36ParsePossibleDestructuringSubPatternEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEE
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseArrayLiteralEv,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseArrayLiteralEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseArrayLiteralEv
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseArrayLiteralEv, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseArrayLiteralEv:
.LFB25555:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	200(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	144(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEEC1EPNS0_15ExpressionScopeIS4_EE
.L1254:
	movq	200(%rbx), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %eax
	cmpb	$7, %al
	je	.L1238
	cmpb	$30, %al
	je	.L1267
	cmpb	$10, %al
	je	.L1268
	movzbl	256(%rbx), %r14d
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movb	$1, 256(%rbx)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE36ParsePossibleDestructuringSubPatternEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEE
	movb	%r14b, 256(%rbx)
	movq	200(%rbx), %rdi
	movl	%eax, %r13d
.L1245:
	movq	8(%rdi), %rax
	cmpb	$7, 56(%rax)
	jne	.L1269
.L1238:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movl	$7, %r13d
.L1241:
	movq	%r12, %rdi
	call	_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEED1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1270
	addq	$56, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1267:
	.cfi_restore_state
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$30, %al
	je	.L1254
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L1254
	.p2align 4,,10
	.p2align 3
.L1269:
	movl	%r13d, %r14d
	andl	$7, %r14d
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$30, %al
	jne	.L1271
.L1252:
	cmpl	$1, %r14d
	jne	.L1254
	jmp	.L1241
	.p2align 4,,10
	.p2align 3
.L1268:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movzbl	256(%rbx), %r13d
	movq	(%rax), %rax
	movl	(%rax), %r14d
	movb	$1, 256(%rbx)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE36ParsePossibleDestructuringSubPatternEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEE
	movq	200(%rbx), %rdi
	movl	%eax, %edx
	andl	$7, %edx
	cmpl	$2, %edx
	je	.L1243
.L1244:
	movq	8(%rdi), %rax
	cmpb	$30, 56(%rax)
	je	.L1272
.L1249:
	movb	%r13b, 256(%rbx)
	movl	$5, %r13d
	jmp	.L1245
	.p2align 4,,10
	.p2align 3
.L1243:
	shrl	$4, %eax
	andl	$15, %eax
	cmpl	$9, %eax
	jne	.L1244
	movq	144(%rbx), %rax
	movq	(%rdi), %rdx
	movzbl	16(%rax), %ecx
	movl	4(%rdx), %r9d
	leal	-3(%rcx), %edx
	cmpb	$2, %dl
	jbe	.L1273
	movl	68(%rax), %ecx
	cmpl	%ecx, 64(%rax)
	jbe	.L1244
	movl	$252, 52(%rax)
	movl	%r14d, 64(%rax)
	movl	%r9d, 68(%rax)
.L1266:
	movq	200(%rbx), %rdi
	jmp	.L1244
	.p2align 4,,10
	.p2align 3
.L1272:
	movq	(%rdi), %rdx
	movq	144(%rbx), %rax
	movl	4(%rdx), %ecx
	movzbl	16(%rax), %edx
	subl	$3, %edx
	cmpb	$2, %dl
	jbe	.L1274
	movl	68(%rax), %edx
	cmpl	%edx, 64(%rax)
	jbe	.L1249
	movl	$282, 52(%rax)
	movl	%r14d, 64(%rax)
	movl	%ecx, 68(%rax)
	movq	200(%rbx), %rdi
	jmp	.L1249
	.p2align 4,,10
	.p2align 3
.L1271:
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L1252
.L1273:
	movq	(%rax), %r15
	xorl	%r8d, %r8d
	movl	%r9d, %edx
	movl	$252, %ecx
	movl	%r14d, %esi
	movq	128(%r15), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r15), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L1266
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L1266
.L1274:
	movq	(%rax), %rdi
	salq	$32, %rcx
	movl	%r14d, %esi
	movl	$282, %edx
	orq	%rcx, %rsi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	movq	200(%rbx), %rdi
	jmp	.L1249
.L1270:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25555:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseArrayLiteralEv, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseArrayLiteralEv
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE:
.LFB26052:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$168, %rsp
	movq	200(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movzbl	56(%rax), %eax
	cmpb	$95, %al
	je	.L1387
	cmpb	$40, %al
	je	.L1292
	cmpb	$9, 32(%rsi)
	je	.L1388
.L1293:
	cmpb	$88, %al
	je	.L1310
	ja	.L1311
	cmpb	$10, %al
	je	.L1312
	cmpb	$87, %al
	jne	.L1389
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movb	$1, 16(%rbx)
	movzbl	32(%rbx), %eax
	movq	$0, 8(%rbx)
.L1326:
	cmpb	$9, %al
	je	.L1390
	.p2align 4,,10
	.p2align 3
.L1330:
	movl	$2, %eax
.L1288:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1391
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1389:
	.cfi_restore_state
	cmpb	$3, %al
	jne	.L1315
	movq	$0, 8(%rbx)
	leaq	-144(%rbp), %r15
	movb	$0, 16(%rbx)
	movb	$1, 33(%rbx)
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movzbl	256(%r12), %r14d
	movb	$1, 256(%r12)
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPS3_NS0_15ExpressionScopeIS4_E9ScopeTypeE.constprop.0
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseAssignmentExpressionCoverGrammarEv
	movl	%eax, %r13d
	movl	-84(%rbp), %eax
	cmpl	%eax, -88(%rbp)
	jbe	.L1392
.L1331:
	movq	-120(%rbp), %rdi
	movq	-112(%rbp), %rsi
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE6resizeEm
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	%rdx, 144(%rax)
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$7, %al
	jne	.L1393
.L1332:
	cmpb	$9, 32(%rbx)
	je	.L1394
.L1347:
	movb	%r14b, 256(%r12)
	movl	%r13d, %eax
	jmp	.L1288
	.p2align 4,,10
	.p2align 3
.L1311:
	cmpb	$90, %al
	je	.L1316
	cmpb	$108, %al
	jne	.L1315
	movb	$1, 34(%rbx)
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$9, 32(%rbx)
	je	.L1395
.L1317:
	movq	%r12, %rdi
	call	_ZNK2v88internal9PreParser13GetIdentifierEv
	movq	%rax, -208(%rbp)
	movq	%rdx, -200(%rbp)
	movq	%rax, 8(%rbx)
	movzbl	-200(%rbp), %eax
	movb	%al, 16(%rbx)
	movl	24(%rbx), %eax
	testl	%eax, %eax
	je	.L1385
	cmpb	$0, 260(%r12)
	movzbl	32(%rbx), %eax
	jne	.L1326
	cmpb	$1, %al
	jbe	.L1365
	cmpb	$5, %al
	jne	.L1326
.L1365:
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	.p2align 4,,10
	.p2align 3
.L1385:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	movb	$9, 32(%rbx)
	movl	$1, %eax
	jmp	.L1288
	.p2align 4,,10
	.p2align 3
.L1388:
	leal	-93(%rax), %edx
	cmpb	$1, %dl
	ja	.L1293
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movl	%eax, %esi
	movq	200(%r12), %rax
	movq	8(%rax), %rdx
	movzbl	56(%rdx), %edx
	subl	$5, %edx
	cmpb	$35, %dl
	ja	.L1295
	leaq	.L1297(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE,"aG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE,comdat
	.align 4
	.align 4
.L1297:
	.long	.L1302-.L1297
	.long	.L1295-.L1297
	.long	.L1295-.L1297
	.long	.L1295-.L1297
	.long	.L1301-.L1297
	.long	.L1295-.L1297
	.long	.L1295-.L1297
	.long	.L1296-.L1297
	.long	.L1300-.L1297
	.long	.L1295-.L1297
	.long	.L1295-.L1297
	.long	.L1295-.L1297
	.long	.L1299-.L1297
	.long	.L1295-.L1297
	.long	.L1295-.L1297
	.long	.L1295-.L1297
	.long	.L1295-.L1297
	.long	.L1295-.L1297
	.long	.L1295-.L1297
	.long	.L1295-.L1297
	.long	.L1295-.L1297
	.long	.L1295-.L1297
	.long	.L1295-.L1297
	.long	.L1295-.L1297
	.long	.L1295-.L1297
	.long	.L1298-.L1297
	.long	.L1295-.L1297
	.long	.L1295-.L1297
	.long	.L1295-.L1297
	.long	.L1295-.L1297
	.long	.L1295-.L1297
	.long	.L1295-.L1297
	.long	.L1295-.L1297
	.long	.L1295-.L1297
	.long	.L1295-.L1297
	.long	.L1296-.L1297
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE,comdat
	.p2align 4,,10
	.p2align 3
.L1312:
	cmpb	$9, 32(%rbx)
	je	.L1396
.L1315:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	leaq	_ZN2v88internal5Token11token_flagsE(%rip), %rdx
	movzbl	%al, %eax
	testb	$2, (%rdx,%rax)
	je	.L1348
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	cmpb	$9, 56(%rax)
	je	.L1364
.L1386:
	movq	%r12, %rdi
	call	_ZNK2v88internal9PreParser13GetIdentifierEv
.L1349:
	movq	%rax, 8(%rbx)
	movzbl	32(%rbx), %eax
	movb	%dl, 16(%rbx)
	cmpb	$9, %al
	jne	.L1330
.L1390:
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %eax
	subl	$5, %eax
	cmpb	$35, %al
	ja	.L1330
	leaq	.L1361(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE,"aG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE,comdat
	.align 4
	.align 4
.L1361:
	.long	.L1355-.L1361
	.long	.L1330-.L1361
	.long	.L1330-.L1361
	.long	.L1330-.L1361
	.long	.L1350-.L1361
	.long	.L1330-.L1361
	.long	.L1330-.L1361
	.long	.L1356-.L1361
	.long	.L1353-.L1361
	.long	.L1330-.L1361
	.long	.L1330-.L1361
	.long	.L1330-.L1361
	.long	.L1354-.L1361
	.long	.L1330-.L1361
	.long	.L1330-.L1361
	.long	.L1330-.L1361
	.long	.L1330-.L1361
	.long	.L1330-.L1361
	.long	.L1330-.L1361
	.long	.L1330-.L1361
	.long	.L1330-.L1361
	.long	.L1330-.L1361
	.long	.L1330-.L1361
	.long	.L1330-.L1361
	.long	.L1330-.L1361
	.long	.L1352-.L1361
	.long	.L1330-.L1361
	.long	.L1330-.L1361
	.long	.L1330-.L1361
	.long	.L1330-.L1361
	.long	.L1330-.L1361
	.long	.L1330-.L1361
	.long	.L1330-.L1361
	.long	.L1330-.L1361
	.long	.L1330-.L1361
	.long	.L1356-.L1361
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE,comdat
	.p2align 4,,10
	.p2align 3
.L1316:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	cmpb	$9, 56(%rax)
	jne	.L1386
.L1364:
	movl	$1, %edx
	xorl	%eax, %eax
	jmp	.L1349
	.p2align 4,,10
	.p2align 3
.L1295:
	movq	(%rax), %rax
	movl	4(%rax), %edx
	subl	(%rax), %edx
	cmpb	$90, 56(%rax)
	je	.L1397
.L1305:
	cmpb	$0, 28(%rax)
	movl	24(%rax), %ecx
	je	.L1398
.L1306:
	cmpl	%ecx, %edx
	jne	.L1399
.L1307:
	cmpb	$93, %sil
	je	.L1400
	cmpb	$94, %sil
	jne	.L1383
	movb	$1, 32(%rbx)
.L1383:
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %eax
	jmp	.L1293
	.p2align 4,,10
	.p2align 3
.L1310:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$9, 32(%rbx)
	movb	$1, 16(%rbx)
	movq	$0, 8(%rbx)
	jne	.L1330
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %eax
	subl	$5, %eax
	cmpb	$35, %al
	ja	.L1330
	leaq	.L1360(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE,"aG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE,comdat
	.align 4
	.align 4
.L1360:
	.long	.L1355-.L1360
	.long	.L1330-.L1360
	.long	.L1330-.L1360
	.long	.L1330-.L1360
	.long	.L1350-.L1360
	.long	.L1330-.L1360
	.long	.L1330-.L1360
	.long	.L1356-.L1360
	.long	.L1353-.L1360
	.long	.L1330-.L1360
	.long	.L1330-.L1360
	.long	.L1330-.L1360
	.long	.L1354-.L1360
	.long	.L1330-.L1360
	.long	.L1330-.L1360
	.long	.L1330-.L1360
	.long	.L1330-.L1360
	.long	.L1330-.L1360
	.long	.L1330-.L1360
	.long	.L1330-.L1360
	.long	.L1330-.L1360
	.long	.L1330-.L1360
	.long	.L1330-.L1360
	.long	.L1330-.L1360
	.long	.L1330-.L1360
	.long	.L1352-.L1360
	.long	.L1330-.L1360
	.long	.L1330-.L1360
	.long	.L1330-.L1360
	.long	.L1330-.L1360
	.long	.L1330-.L1360
	.long	.L1330-.L1360
	.long	.L1330-.L1360
	.long	.L1330-.L1360
	.long	.L1330-.L1360
	.long	.L1356-.L1360
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE,comdat
	.p2align 4,,10
	.p2align 3
.L1387:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rsi
	movq	8(%rsi), %rcx
	movzbl	56(%rcx), %eax
	cmpb	$40, %al
	jne	.L1401
.L1277:
	cmpb	$0, 76(%rcx)
	jne	.L1285
	movq	(%rsi), %rax
	movl	4(%rax), %edx
	subl	(%rax), %edx
	cmpb	$90, 56(%rax)
	jne	.L1287
	subl	$2, %edx
.L1287:
	cmpb	$0, 28(%rax)
	movl	24(%rax), %eax
	jne	.L1290
	sarl	%eax
.L1290:
	cmpl	%eax, %edx
	jne	.L1402
.L1291:
	movl	$2, 28(%rbx)
	movb	$5, 32(%rbx)
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %eax
	cmpb	$40, %al
	jne	.L1293
	.p2align 4,,10
	.p2align 3
.L1292:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	orl	$1, 28(%rbx)
	movb	$5, 32(%rbx)
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %eax
	jmp	.L1293
.L1283:
	movb	$2, 32(%rbx)
	.p2align 4,,10
	.p2align 3
.L1285:
	movq	%r12, %rdi
	call	_ZNK2v88internal9PreParser13GetIdentifierEv
	movq	%rax, -176(%rbp)
	movq	%rdx, -168(%rbp)
	movq	%rax, 8(%rbx)
	movzbl	-168(%rbp), %eax
	movb	%al, 16(%rbx)
	movl	$2, %eax
	jmp	.L1288
	.p2align 4,,10
	.p2align 3
.L1401:
	subl	$5, %eax
	cmpb	$25, %al
	ja	.L1277
	leaq	.L1279(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE,"aG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE,comdat
	.align 4
	.align 4
.L1279:
	.long	.L1284-.L1279
	.long	.L1277-.L1279
	.long	.L1277-.L1279
	.long	.L1277-.L1279
	.long	.L1283-.L1279
	.long	.L1277-.L1279
	.long	.L1277-.L1279
	.long	.L1282-.L1279
	.long	.L1281-.L1279
	.long	.L1277-.L1279
	.long	.L1277-.L1279
	.long	.L1277-.L1279
	.long	.L1280-.L1279
	.long	.L1277-.L1279
	.long	.L1277-.L1279
	.long	.L1277-.L1279
	.long	.L1277-.L1279
	.long	.L1277-.L1279
	.long	.L1277-.L1279
	.long	.L1277-.L1279
	.long	.L1277-.L1279
	.long	.L1277-.L1279
	.long	.L1277-.L1279
	.long	.L1277-.L1279
	.long	.L1277-.L1279
	.long	.L1278-.L1279
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE,comdat
.L1278:
	movb	$3, 32(%rbx)
	jmp	.L1285
.L1280:
	movb	$4, 32(%rbx)
	jmp	.L1285
.L1281:
	movb	$7, 32(%rbx)
	jmp	.L1285
.L1282:
	movb	$6, 32(%rbx)
	jmp	.L1285
.L1284:
	movb	$5, 32(%rbx)
	jmp	.L1285
	.p2align 4,,10
	.p2align 3
.L1392:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE8ValidateEi.part.0
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1398:
	sarl	%ecx
	jmp	.L1306
	.p2align 4,,10
	.p2align 3
.L1395:
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %eax
	subl	$5, %eax
	cmpb	$35, %al
	ja	.L1317
	leaq	.L1319(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE,"aG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE,comdat
	.align 4
	.align 4
.L1319:
	.long	.L1324-.L1319
	.long	.L1317-.L1319
	.long	.L1317-.L1319
	.long	.L1317-.L1319
	.long	.L1323-.L1319
	.long	.L1317-.L1319
	.long	.L1317-.L1319
	.long	.L1318-.L1319
	.long	.L1322-.L1319
	.long	.L1317-.L1319
	.long	.L1317-.L1319
	.long	.L1317-.L1319
	.long	.L1321-.L1319
	.long	.L1317-.L1319
	.long	.L1317-.L1319
	.long	.L1317-.L1319
	.long	.L1317-.L1319
	.long	.L1317-.L1319
	.long	.L1317-.L1319
	.long	.L1317-.L1319
	.long	.L1317-.L1319
	.long	.L1317-.L1319
	.long	.L1317-.L1319
	.long	.L1317-.L1319
	.long	.L1317-.L1319
	.long	.L1320-.L1319
	.long	.L1317-.L1319
	.long	.L1317-.L1319
	.long	.L1317-.L1319
	.long	.L1317-.L1319
	.long	.L1317-.L1319
	.long	.L1317-.L1319
	.long	.L1317-.L1319
	.long	.L1317-.L1319
	.long	.L1317-.L1319
	.long	.L1318-.L1319
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE,comdat
	.p2align 4,,10
	.p2align 3
.L1396:
	movq	$0, 8(%rbx)
	movb	$0, 16(%rbx)
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rax
	movq	%r12, %rdi
	movzbl	256(%r12), %r14d
	movb	$1, 256(%r12)
	movq	(%rbx), %rsi
	movq	8(%rax), %rax
	movl	(%rax), %r15d
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE36ParsePossibleDestructuringSubPatternEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEE
	movb	$8, 32(%rbx)
	movl	%eax, %r13d
	andl	$7, %eax
	cmpl	$3, %eax
	jne	.L1341
	movq	(%r12), %rax
	testb	$1, 129(%rax)
	je	.L1342
	movl	%r13d, %eax
	shrl	$4, %eax
	subl	$2, %eax
	cmpb	$1, %al
	jbe	.L1343
.L1342:
	movq	200(%r12), %rax
	movq	8(%rax), %rdx
	cmpb	$13, 56(%rdx)
	je	.L1347
	movq	144(%r12), %rdi
	movq	(%rax), %rsi
	movl	$282, %edx
	call	_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE18RecordPatternErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1397:
	subl	$2, %edx
	jmp	.L1305
	.p2align 4,,10
	.p2align 3
.L1400:
	movb	$0, 32(%rbx)
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %eax
	jmp	.L1293
.L1356:
	movb	$6, 32(%rbx)
	jmp	.L1330
.L1399:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L1307
.L1296:
	movb	$6, 32(%rbx)
	.p2align 4,,10
	.p2align 3
.L1303:
	movq	%r12, %rdi
	call	_ZNK2v88internal9PreParser13GetIdentifierEv
	movq	%rax, -192(%rbp)
	movq	%rdx, -184(%rbp)
	movq	%rax, 8(%rbx)
	movzbl	-184(%rbp), %eax
	movb	%al, 16(%rbx)
	movl	$2, %eax
	jmp	.L1288
.L1350:
	movb	$2, 32(%rbx)
	jmp	.L1330
.L1355:
	movb	$5, 32(%rbx)
	jmp	.L1330
.L1352:
	movb	$3, 32(%rbx)
	jmp	.L1330
.L1354:
	movb	$4, 32(%rbx)
	jmp	.L1330
.L1353:
	movb	$7, 32(%rbx)
	jmp	.L1330
.L1300:
	movb	$7, 32(%rbx)
	jmp	.L1303
.L1301:
	movb	$2, 32(%rbx)
	jmp	.L1303
.L1302:
	movb	$5, 32(%rbx)
	jmp	.L1303
.L1298:
	movb	$3, 32(%rbx)
	jmp	.L1303
.L1299:
	movb	$4, 32(%rbx)
	jmp	.L1303
.L1318:
	movb	$6, 32(%rbx)
	jmp	.L1317
.L1322:
	movb	$7, 32(%rbx)
	jmp	.L1317
.L1320:
	movb	$3, 32(%rbx)
	jmp	.L1317
.L1321:
	movb	$4, 32(%rbx)
	jmp	.L1317
.L1323:
	movb	$2, 32(%rbx)
	jmp	.L1317
.L1324:
	movb	$5, 32(%rbx)
	jmp	.L1317
	.p2align 4,,10
	.p2align 3
.L1394:
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %eax
	subl	$5, %eax
	cmpb	$35, %al
	ja	.L1347
	leaq	.L1335(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE,"aG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE,comdat
	.align 4
	.align 4
.L1335:
	.long	.L1340-.L1335
	.long	.L1347-.L1335
	.long	.L1347-.L1335
	.long	.L1347-.L1335
	.long	.L1339-.L1335
	.long	.L1347-.L1335
	.long	.L1347-.L1335
	.long	.L1334-.L1335
	.long	.L1338-.L1335
	.long	.L1347-.L1335
	.long	.L1347-.L1335
	.long	.L1347-.L1335
	.long	.L1337-.L1335
	.long	.L1347-.L1335
	.long	.L1347-.L1335
	.long	.L1347-.L1335
	.long	.L1347-.L1335
	.long	.L1347-.L1335
	.long	.L1347-.L1335
	.long	.L1347-.L1335
	.long	.L1347-.L1335
	.long	.L1347-.L1335
	.long	.L1347-.L1335
	.long	.L1347-.L1335
	.long	.L1347-.L1335
	.long	.L1336-.L1335
	.long	.L1347-.L1335
	.long	.L1347-.L1335
	.long	.L1347-.L1335
	.long	.L1347-.L1335
	.long	.L1347-.L1335
	.long	.L1347-.L1335
	.long	.L1347-.L1335
	.long	.L1347-.L1335
	.long	.L1347-.L1335
	.long	.L1334-.L1335
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE,comdat
.L1334:
	movb	$6, 32(%rbx)
	jmp	.L1347
.L1339:
	movb	$2, 32(%rbx)
	jmp	.L1347
.L1340:
	movb	$5, 32(%rbx)
	jmp	.L1347
.L1336:
	movb	$3, 32(%rbx)
	jmp	.L1347
.L1337:
	movb	$4, 32(%rbx)
	jmp	.L1347
.L1338:
	movb	$7, 32(%rbx)
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1348:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	movq	40(%r12), %rax
	movl	$1, %edx
	movq	56(%rax), %rax
	movq	272(%rax), %rax
	jmp	.L1349
	.p2align 4,,10
	.p2align 3
.L1341:
	cmpl	$2, %eax
	je	.L1403
.L1343:
	movq	200(%r12), %rax
	movq	144(%r12), %rdi
	movq	(%rax), %rax
	movl	4(%rax), %edx
	movzbl	16(%rdi), %eax
	leal	-1(%rax), %ecx
	cmpb	$4, %cl
	ja	.L1344
	subl	$3, %eax
	cmpb	$2, %al
	jbe	.L1404
	movl	%r15d, 76(%rdi)
	movl	%edx, 80(%rdi)
	movl	$245, 84(%rdi)
.L1384:
	movq	200(%r12), %rax
	movq	144(%r12), %rdi
	movq	(%rax), %rax
	movl	4(%rax), %edx
.L1344:
	movl	%edx, -148(%rbp)
	leaq	-152(%rbp), %rsi
	movl	$247, %edx
	movl	%r15d, -152(%rbp)
	call	_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE18RecordPatternErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE
	jmp	.L1342
	.p2align 4,,10
	.p2align 3
.L1393:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L1332
.L1403:
	movl	%r13d, %eax
	shrl	$4, %eax
	andl	$15, %eax
	subl	$1, %eax
	cmpl	$3, %eax
	ja	.L1343
	jmp	.L1342
.L1402:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L1291
.L1404:
	movq	(%rdi), %rbx
	xorl	%r8d, %r8d
	movl	$245, %ecx
	movl	%r15d, %esi
	movq	128(%rbx), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%rbx), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L1384
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L1384
.L1391:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26052:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseForStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseForStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseForStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseForStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseForStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_:
.LFB25538:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -264(%rbp)
	movq	16(%rdi), %rbx
	movq	%rsi, -256(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 24(%rbx)
	movq	200(%rdi), %rax
	movq	136(%rdi), %rdi
	movq	8(%rax), %rax
	movq	24(%rdi), %rdx
	movl	(%rax), %eax
	movl	%eax, -244(%rbp)
	movq	16(%rdi), %rax
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jbe	.L1532
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1407:
	pxor	%xmm0, %xmm0
	movq	200(%r13), %rdi
	movq	%rax, -224(%rbp)
	movabsq	$-4294967296, %rax
	movups	%xmm0, -184(%rbp)
	movdqa	.LC4(%rip), %xmm0
	movq	$1, -216(%rbp)
	movq	%rax, -208(%rbp)
	movq	$0, -168(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r13), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$5, %al
	jne	.L1533
.L1408:
	movq	200(%r13), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %r14d
	cmpb	$104, %r14b
	je	.L1413
	movl	%r14d, %ecx
	cmpb	$98, %r14b
	je	.L1534
.L1410:
	cmpb	$80, %cl
	je	.L1535
	cmpb	$12, %cl
	jne	.L1536
.L1437:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$12, %al
	jne	.L1537
.L1469:
	subq	$8, %rsp
	movl	-244(%rbp), %esi
	movq	%r13, %rdi
	leaq	-228(%rbp), %rax
	pushq	%rax
	movq	-264(%rbp), %rcx
	leaq	-232(%rbp), %r9
	leaq	-236(%rbp), %r8
	movq	-256(%rbp), %rdx
	movl	$0, %r14d
	movl	$0, -236(%rbp)
	movl	$0, -232(%rbp)
	movl	$0, -228(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseStandardForLoopEiPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_PNS0_19PreParserExpressionEPNS0_18PreParserStatementESD_
	movq	200(%r13), %rdx
	popq	%rcx
	popq	%rsi
	movq	24(%rdx), %rdx
	cmpb	$0, 48(%rdx)
	cmove	%eax, %r14d
.L1435:
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1471
	call	_ZdlPv@PLT
.L1471:
	subl	$1, 24(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1538
	leaq	-40(%rbp), %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1536:
	.cfi_restore_state
	movq	(%rax), %rcx
	leaq	-144(%rbp), %r12
	movl	(%rax), %eax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rcx, -280(%rbp)
	movl	%eax, -272(%rbp)
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPS3_NS0_15ExpressionScopeIS4_E9ScopeTypeE.constprop.0
	movzbl	256(%r13), %r15d
	movq	%r13, %rdi
	movb	$0, 256(%r13)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE27ParseExpressionCoverGrammarEv
	movq	200(%r13), %rdi
	movl	%eax, %r9d
	movq	(%rdi), %rax
	movl	4(%rax), %ecx
	movq	8(%rdi), %rax
	movzbl	56(%rax), %esi
	cmpb	$62, %sil
	je	.L1441
	cmpb	$92, %sil
	je	.L1539
.L1442:
	movl	-84(%rbp), %eax
	cmpl	%eax, -88(%rbp)
	jbe	.L1446
.L1531:
	movq	-112(%rbp), %rsi
	movq	-120(%rbp), %rdi
	xorl	%r14d, %r14d
.L1447:
	movb	%r15b, 256(%r13)
	movq	8(%rdi), %rcx
	movq	(%rdi), %rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rsi, %rax
	jb	.L1540
	jbe	.L1460
	salq	$4, %rsi
	addq	%rdx, %rsi
	cmpq	%rsi, %rcx
	je	.L1460
	movq	%rsi, 8(%rdi)
.L1460:
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	%rdx, 144(%rax)
	testb	%r14b, %r14b
	jne	.L1541
	movq	200(%r13), %rdi
	jmp	.L1437
	.p2align 4,,10
	.p2align 3
.L1552:
	cmpb	$3, %al
	jne	.L1529
.L1413:
	movq	136(%r13), %r14
	movq	16(%r14), %r12
	movq	24(%r14), %rax
	subq	%r12, %rax
	cmpq	$135, %rax
	jbe	.L1542
	leaq	136(%r12), %rax
	movq	%rax, 16(%r14)
.L1416:
	movq	0(%r13), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$6, %ecx
	call	_ZN2v88internal5ScopeC1EPNS0_4ZoneEPS1_NS0_9ScopeTypeE@PLT
	movq	0(%r13), %rax
	movq	%r12, 0(%r13)
	movq	%rax, -280(%rbp)
	movq	200(%r13), %rax
	movq	(%rax), %rax
	movl	(%rax), %eax
	movl	%eax, 112(%r12)
	movq	16(%r13), %rax
	movzbl	59(%rax), %r12d
	movb	$0, 59(%rax)
	movq	136(%r13), %r14
	movq	0(%r13), %rdx
	orq	%rax, %r12
	movq	%r12, -272(%rbp)
	movq	24(%r14), %rax
	movq	16(%r14), %r12
	subq	%r12, %rax
	cmpq	$135, %rax
	jbe	.L1543
	leaq	136(%r12), %rax
	movq	%rax, 16(%r14)
.L1418:
	movq	%r14, %rsi
	movl	$6, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal5ScopeC1EPNS0_4ZoneEPS1_NS0_9ScopeTypeE@PLT
	movq	0(%r13), %r14
	movq	%r12, 0(%r13)
	movq	%r13, %rdi
	leaq	-224(%rbp), %r15
	leaq	-200(%rbp), %rdx
	movl	$2, %esi
	movq	%r15, %rcx
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseVariableDeclarationsENS3_26VariableDeclarationContextEPNS3_24DeclarationParsingResultEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	movq	200(%r13), %rdi
	movq	%r14, 0(%r13)
	movq	(%rdi), %rax
	movl	(%rax), %eax
	movl	%eax, -204(%rbp)
	movq	8(%rdi), %rax
	movzbl	56(%rax), %edx
	cmpb	$62, %dl
	je	.L1419
	cmpb	$92, %dl
	je	.L1544
.L1420:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$12, %al
	jne	.L1545
.L1425:
	movq	0(%r13), %rax
	movl	112(%rax), %eax
	movl	%eax, 112(%r12)
	movq	0(%r13), %rax
	movq	136(%r13), %r15
	movq	%r12, 0(%r13)
	movq	%rax, -288(%rbp)
	movq	16(%r15), %r14
	movq	24(%r15), %rax
	subq	%r14, %rax
	cmpq	$135, %rax
	jbe	.L1546
	leaq	136(%r14), %rax
	movq	%rax, 16(%r15)
.L1430:
	movq	%r15, %rsi
	movl	$6, %ecx
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal5ScopeC1EPNS0_4ZoneEPS1_NS0_9ScopeTypeE@PLT
	movq	0(%r13), %r15
	movq	%r14, 0(%r13)
	movq	%r13, %rdi
	movq	200(%r13), %rax
	subq	$8, %rsp
	movl	$0, -236(%rbp)
	leaq	-232(%rbp), %r9
	movl	$0, -232(%rbp)
	movq	-264(%rbp), %rcx
	leaq	-236(%rbp), %r8
	movl	$0, -228(%rbp)
	movq	(%rax), %rax
	movq	-256(%rbp), %rdx
	movl	-244(%rbp), %esi
	movl	(%rax), %eax
	movl	%eax, 112(%r14)
	leaq	-228(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseStandardForLoopEiPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_PNS0_19PreParserExpressionEPNS0_18PreParserStatementESD_
	popq	%r9
	popq	%r10
	movl	$0, -244(%rbp)
	movl	%eax, %ecx
	movq	200(%r13), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L1431
	movq	(%rax), %rax
	movq	0(%r13), %rdx
	movl	%ecx, -244(%rbp)
	movl	4(%rax), %eax
	movl	%eax, 116(%rdx)
	movq	200(%r13), %rax
	movq	%r15, 0(%r13)
	movq	(%rax), %rax
	movl	4(%rax), %eax
	movl	%eax, 116(%r15)
	movslq	-212(%rbp), %rax
	testl	%eax, %eax
	jle	.L1432
	movq	16(%r13), %rdx
	cmpb	$0, 59(%rdx)
	je	.L1432
	movq	0(%r13), %rdx
	movq	-224(%rbp), %r15
	leaq	-237(%rbp), %r8
	movq	%rbx, -256(%rbp)
	movq	%r8, %rbx
	orb	$16, 129(%rdx)
	leaq	(%r15,%rax,8), %r12
	.p2align 4,,10
	.p2align 3
.L1433:
	subq	$8, %rsp
	movq	%rbx, %r8
	movq	%r13, %rdi
	movl	$-1, %r9d
	pushq	$0
	movzbl	-200(%rbp), %edx
	movq	%r14, %rcx
	addq	$8, %r15
	movq	-8(%r15), %rsi
	call	_ZN2v88internal9PreParser19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPNS0_5ScopeEPbiNS0_12VariableKindE
	popq	%rdi
	popq	%r8
	cmpq	%r15, %r12
	jne	.L1433
	movq	-256(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L1431:
	movq	-288(%rbp), %rax
	movq	%rax, 0(%r13)
	movq	%rax, %rdi
	call	_ZN2v88internal5Scope18FinalizeBlockScopeEv@PLT
	movl	-244(%rbp), %r14d
.L1428:
	movq	-272(%rbp), %rcx
	movq	%rcx, %rax
	andq	$-8, %rax
	cmpb	$0, 59(%rax)
	jne	.L1434
	andl	$7, %ecx
	setne	59(%rax)
.L1434:
	movq	-280(%rbp), %rax
	movq	%rax, 0(%r13)
	jmp	.L1435
	.p2align 4,,10
	.p2align 3
.L1544:
	movl	4(%rax), %edx
	subl	(%rax), %edx
	cmpb	$0, 28(%rax)
	movl	24(%rax), %ecx
	je	.L1547
.L1421:
	cmpl	%ecx, %edx
	jne	.L1420
	movq	40(%r13), %rsi
	movq	56(%rsi), %rax
	movq	392(%rax), %r14
	call	_ZNK2v88internal7Scanner10NextSymbolEPNS0_15AstValueFactoryE@PLT
	movq	200(%r13), %rdi
	cmpq	%rax, %r14
	jne	.L1420
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movl	$1, -208(%rbp)
.L1423:
	movq	0(%r13), %rax
	movl	-244(%rbp), %esi
	movq	%r12, %r9
	movq	%r15, %rdx
	movq	-264(%rbp), %r8
	movq	-256(%rbp), %rcx
	movq	%r13, %rdi
	orb	$16, 129(%rax)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseForEachStatementWithDeclarationsEiPNS3_7ForInfoEPNS0_8ZoneListIPKNS0_12AstRawStringEEESB_PNS0_5ScopeE
	movl	%eax, %r14d
	jmp	.L1428
	.p2align 4,,10
	.p2align 3
.L1539:
	movl	4(%rax), %esi
	subl	(%rax), %esi
	cmpb	$0, 28(%rax)
	movl	24(%rax), %r8d
	je	.L1548
.L1443:
	cmpl	%r8d, %esi
	jne	.L1442
	movq	40(%r13), %rsi
	movl	%r9d, -292(%rbp)
	movl	%ecx, -248(%rbp)
	movq	56(%rsi), %rax
	movq	392(%rax), %rax
	movq	%rax, -288(%rbp)
	call	_ZNK2v88internal7Scanner10NextSymbolEPNS0_15AstValueFactoryE@PLT
	cmpq	%rax, -288(%rbp)
	movl	-248(%rbp), %ecx
	movl	-292(%rbp), %r9d
	jne	.L1442
	movq	200(%r13), %rdi
	movl	%r9d, -248(%rbp)
	movl	%ecx, -288(%rbp)
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$98, %r14b
	movl	$1, -208(%rbp)
	movl	-288(%rbp), %ecx
	movl	-248(%rbp), %r9d
	je	.L1549
.L1445:
	movl	%r9d, %eax
	andl	$7, %eax
	leal	-6(%rax), %edx
	cmpl	$1, %edx
	jbe	.L1550
	movq	-144(%rbp), %rdi
	cmpl	$3, %eax
	je	.L1551
	cmpl	$2, %eax
	jne	.L1451
	movl	%r9d, %eax
	shrl	$4, %eax
	andl	$15, %eax
	subl	$1, %eax
	cmpl	$3, %eax
	ja	.L1451
	movl	-84(%rbp), %eax
	cmpl	%eax, -88(%rbp)
	jbe	.L1458
	.p2align 4,,10
	.p2align 3
.L1530:
	movq	-112(%rbp), %rsi
	movq	-120(%rbp), %rdi
.L1452:
	movl	$1, %r14d
	jmp	.L1447
	.p2align 4,,10
	.p2align 3
.L1534:
	call	_ZN2v88internal7Scanner9PeekAheadEv@PLT
	cmpb	$100, %al
	je	.L1411
	ja	.L1529
	cmpb	$8, %al
	je	.L1413
	jbe	.L1552
	subl	$92, %eax
	cmpb	$7, %al
	jbe	.L1413
.L1529:
	movq	200(%r13), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %ecx
	jmp	.L1410
	.p2align 4,,10
	.p2align 3
.L1535:
	leaq	-224(%rbp), %r15
	leaq	-200(%rbp), %rdx
	movq	%r13, %rdi
	movl	$2, %esi
	movq	%r15, %rcx
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseVariableDeclarationsENS3_26VariableDeclarationContextEPNS3_24DeclarationParsingResultEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	movq	200(%r13), %rdi
	movq	(%rdi), %rax
	movl	(%rax), %eax
	movl	%eax, -204(%rbp)
	movq	8(%rdi), %rax
	movzbl	56(%rax), %edx
	cmpb	$62, %dl
	je	.L1436
	cmpb	$92, %dl
	jne	.L1437
	movl	4(%rax), %edx
	subl	(%rax), %edx
	cmpb	$0, 28(%rax)
	movl	24(%rax), %ecx
	jne	.L1438
	sarl	%ecx
.L1438:
	cmpl	%ecx, %edx
	jne	.L1437
	movq	40(%r13), %rsi
	movq	56(%rsi), %rax
	movq	392(%rax), %r12
	call	_ZNK2v88internal7Scanner10NextSymbolEPNS0_15AstValueFactoryE@PLT
	movq	200(%r13), %rdi
	cmpq	%rax, %r12
	jne	.L1437
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movl	$1, -208(%rbp)
.L1440:
	movq	0(%r13), %r9
	movl	-244(%rbp), %esi
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	-264(%rbp), %r8
	movq	-256(%rbp), %rcx
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseForEachStatementWithDeclarationsEiPNS3_7ForInfoEPNS0_8ZoneListIPKNS0_12AstRawStringEEESB_PNS0_5ScopeE
	movl	%eax, %r14d
	jmp	.L1435
	.p2align 4,,10
	.p2align 3
.L1532:
	movl	$8, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1407
	.p2align 4,,10
	.p2align 3
.L1533:
	movq	%r13, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L1408
	.p2align 4,,10
	.p2align 3
.L1541:
	cmpl	$1, -208(%rbp)
	je	.L1553
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPS3_NS0_15ExpressionScopeIS4_E9ScopeTypeE.constprop.0
	movzbl	256(%r13), %r14d
	movq	%r13, %rdi
	movb	$1, 256(%r13)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE27ParseExpressionCoverGrammarEv
	movl	-84(%rbp), %eax
	cmpl	%eax, -88(%rbp)
	jbe	.L1554
.L1465:
	movq	-112(%rbp), %rsi
	movq	-120(%rbp), %rdi
	movb	%r14b, 256(%r13)
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE6resizeEm
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	%rdx, 144(%rax)
.L1464:
	movq	200(%r13), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$6, %al
	jne	.L1555
.L1466:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$1, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE
	movq	200(%r13), %rax
	xorl	%r14d, %r14d
	movq	24(%rax), %rax
	cmpb	$0, 48(%rax)
	sete	%r14b
	addl	%r14d, %r14d
	jmp	.L1435
	.p2align 4,,10
	.p2align 3
.L1540:
	subq	%rax, %rsi
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm
	jmp	.L1460
	.p2align 4,,10
	.p2align 3
.L1537:
	movq	%r13, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1411:
	movq	0(%r13), %rax
	testb	$1, 129(%rax)
	je	.L1413
	jmp	.L1529
	.p2align 4,,10
	.p2align 3
.L1446:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE8ValidateEi.part.0
	jmp	.L1531
	.p2align 4,,10
	.p2align 3
.L1547:
	sarl	%ecx
	jmp	.L1421
	.p2align 4,,10
	.p2align 3
.L1548:
	sarl	%r8d
	jmp	.L1443
	.p2align 4,,10
	.p2align 3
.L1432:
	movq	%r14, %rdi
	movl	%ecx, -244(%rbp)
	call	_ZN2v88internal5Scope18FinalizeBlockScopeEv@PLT
	movq	0(%r13), %rdi
	call	_ZN2v88internal5Scope18FinalizeBlockScopeEv@PLT
	movl	-244(%rbp), %ecx
	testq	%rax, %rax
	movl	$2, %eax
	cmove	%ecx, %eax
	movl	%eax, -244(%rbp)
	jmp	.L1431
	.p2align 4,,10
	.p2align 3
.L1554:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE8ValidateEi.part.0
	jmp	.L1465
	.p2align 4,,10
	.p2align 3
.L1550:
	movl	-272(%rbp), %edx
	movl	%r9d, %esi
	movq	%r12, %rdi
	movl	$1, %r14d
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE15ValidatePatternENS0_19PreParserExpressionEii
	movq	-112(%rbp), %rsi
	movq	-120(%rbp), %rdi
	jmp	.L1447
	.p2align 4,,10
	.p2align 3
.L1551:
	movq	(%rdi), %rax
	testb	$1, 129(%rax)
	je	.L1450
	movl	%r9d, %eax
	shrl	$4, %eax
	subl	$2, %eax
	cmpb	$1, %al
	jbe	.L1451
.L1450:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	-120(%rbp), %rdi
	movl	%edx, %eax
	subl	%esi, %eax
	cmpl	%esi, %edx
	je	.L1452
	subl	$1, %eax
	cltq
	addq	%rax, %rsi
	salq	$4, %rsi
	addq	(%rdi), %rsi
	movq	(%rsi), %rax
	movl	4(%rax), %edx
	movl	%edx, %ecx
	orb	$-128, %cl
	movl	%ecx, 4(%rax)
	andb	$1, %dh
	je	.L1530
	movq	8(%rax), %r14
	movzwl	40(%r14), %eax
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$1, %dl
	je	.L1530
	movq	16(%r14), %r8
	testq	%r8, %r8
	je	.L1455
	testb	$64, %ah
	je	.L1556
.L1455:
	orb	$64, %ah
	movw	%ax, 40(%r14)
	movq	-112(%rbp), %rsi
	movq	-120(%rbp), %rdi
	jmp	.L1452
	.p2align 4,,10
	.p2align 3
.L1419:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movl	$0, -208(%rbp)
	jmp	.L1423
	.p2align 4,,10
	.p2align 3
.L1441:
	movl	%r9d, -288(%rbp)
	movl	%ecx, -280(%rbp)
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movl	-288(%rbp), %r9d
	movl	$0, -208(%rbp)
	movl	-280(%rbp), %ecx
	jmp	.L1445
	.p2align 4,,10
	.p2align 3
.L1553:
	movzbl	256(%r13), %r14d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movb	$1, 256(%r13)
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPS3_NS0_15ExpressionScopeIS4_E9ScopeTypeE.constprop.0
	movq	%r13, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseAssignmentExpressionCoverGrammarEv
	movl	-84(%rbp), %eax
	cmpl	%eax, -88(%rbp)
	jbe	.L1557
.L1463:
	movq	-112(%rbp), %rsi
	movq	-120(%rbp), %rdi
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE6resizeEm
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	%rdx, 144(%rax)
	movb	%r14b, 256(%r13)
	jmp	.L1464
	.p2align 4,,10
	.p2align 3
.L1543:
	movl	$136, %esi
	movq	%r14, %rdi
	movq	%rdx, -288(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-288(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L1418
	.p2align 4,,10
	.p2align 3
.L1542:
	movl	$136, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L1416
	.p2align 4,,10
	.p2align 3
.L1436:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movl	$0, -208(%rbp)
	jmp	.L1440
	.p2align 4,,10
	.p2align 3
.L1546:
	movl	$136, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L1430
	.p2align 4,,10
	.p2align 3
.L1545:
	movq	%r13, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L1425
.L1557:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE8ValidateEi.part.0
	jmp	.L1463
.L1549:
	movq	-280(%rbp), %rax
	movl	-272(%rbp), %esi
	movl	$235, %edx
	movq	%r13, %rdi
	movabsq	$-4294967296, %r14
	andq	%r14, %rax
	xorl	%r14d, %r14d
	orq	%rax, %rsi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	movb	%r15b, 256(%r13)
	movq	-112(%rbp), %rsi
	movq	-120(%rbp), %rdi
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE6resizeEm
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	%rdx, 144(%rax)
	jmp	.L1435
.L1555:
	movq	%r13, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L1466
.L1556:
	movzwl	40(%r8), %edx
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L1455
	movq	16(%r8), %rax
	testq	%rax, %rax
	je	.L1456
	testb	$64, %dh
	je	.L1558
.L1456:
	orb	$64, %dh
	movw	%dx, 40(%r8)
	movzwl	40(%r14), %eax
	jmp	.L1455
.L1458:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE8ValidateEi.part.0
	movq	-112(%rbp), %rsi
	movq	-120(%rbp), %rdi
	jmp	.L1452
.L1451:
	movl	-272(%rbp), %edx
	movl	$253, %r8d
	movl	%r9d, %esi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE33RewriteInvalidReferenceExpressionENS0_19PreParserExpressionEiiNS0_15MessageTemplateE
	jmp	.L1530
.L1558:
	movzwl	40(%rax), %ecx
	movl	%ecx, %esi
	andl	$15, %esi
	subb	$1, %sil
	je	.L1456
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1457
	testb	$64, %ch
	je	.L1559
.L1457:
	orb	$64, %ch
	movw	%cx, 40(%rax)
	movzwl	40(%r8), %edx
	jmp	.L1456
.L1538:
	call	__stack_chk_fail@PLT
.L1559:
	movq	%rax, -280(%rbp)
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movq	-280(%rbp), %rax
	movq	-272(%rbp), %r8
	movzwl	40(%rax), %ecx
	jmp	.L1457
	.cfi_endproc
.LFE25538:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseForStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseForStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE:
.LFB25145:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	200(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%r13), %rdi
	movzbl	56(%rdi), %eax
	subl	$8, %eax
	cmpb	$87, %al
	ja	.L1561
	leaq	.L1563(%rip), %r8
	movzbl	%al, %eax
	movslq	(%r8,%rax,4), %rax
	addq	%r8, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE,"aG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE,comdat
	.align 4
	.align 4
.L1563:
	.long	.L1579-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1578-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1577-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1576-.L1563
	.long	.L1575-.L1563
	.long	.L1561-.L1563
	.long	.L1574-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1573-.L1563
	.long	.L1572-.L1563
	.long	.L1571-.L1563
	.long	.L1561-.L1563
	.long	.L1570-.L1563
	.long	.L1569-.L1563
	.long	.L1568-.L1563
	.long	.L1567-.L1563
	.long	.L1566-.L1563
	.long	.L1565-.L1563
	.long	.L1564-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1561-.L1563
	.long	.L1562-.L1563
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE,comdat
	.p2align 4,,10
	.p2align 3
.L1562:
	movq	%r13, %rdi
	movl	%ecx, -244(%rbp)
	movq	%rdx, -240(%rbp)
	movq	%rsi, -232(%rbp)
	call	_ZN2v88internal7Scanner9PeekAheadEv@PLT
	movq	16(%r13), %rax
	movq	-232(%rbp), %rsi
	movq	-240(%rbp), %rdx
	movl	-244(%rbp), %ecx
	cmpb	$0, 76(%rax)
	je	.L1664
	.p2align 4,,10
	.p2align 3
.L1561:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE34ParseExpressionOrLabelledStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE
	.p2align 4,,10
	.p2align 3
.L1580:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1665
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1579:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE10ParseBlockEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1578:
	movq	%r13, %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movl	$2, %eax
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1577:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseBreakStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1576:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseContinueStatementEv
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1575:
	movq	%r13, %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE15ExpectSemicolonEv
.L1663:
	movl	$2, %eax
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1574:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ParseDoWhileStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1573:
	movq	16(%r12), %rax
	movq	%rdx, -240(%rbp)
	movq	%rsi, -232(%rbp)
	movq	48(%rax), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movq	-232(%rbp), %rsi
	movq	-240(%rbp), %rdx
	movzbl	133(%rax), %eax
	subl	$9, %eax
	cmpb	$4, %al
	jbe	.L1666
.L1582:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseForStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1572:
	movq	(%r12), %rax
	movq	(%rdi), %rsi
	movq	%r12, %rdi
	movzbl	129(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%edx, %edx
	andl	$-4, %edx
	addl	$296, %edx
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	xorl	%eax, %eax
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1571:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE16ParseIfStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1570:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseReturnStatementEv
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1569:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseSwitchStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1568:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseThrowStatementEv
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1567:
	movq	%r13, %rdi
	testq	%rsi, %rsi
	je	.L1667
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE10ParseBlockEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %eax
	cmpb	$65, %al
	je	.L1605
	cmpb	$71, %al
	jne	.L1668
.L1606:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE10ParseBlockEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	jmp	.L1663
	.p2align 4,,10
	.p2align 3
.L1566:
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseVariableStatementENS3_26VariableDeclarationContextEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1565:
	movq	16(%r12), %r13
	leaq	-144(%rbp), %r14
	addl	$1, 24(%r13)
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE6ExpectENS0_5Token5ValueE
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPS3_NS0_15ExpressionScopeIS4_E9ScopeTypeE.constprop.0
	movq	%r12, %rdi
	movzbl	256(%r12), %ebx
	movb	$1, 256(%r12)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE27ParseExpressionCoverGrammarEv
	movl	-84(%rbp), %eax
	cmpl	%eax, -88(%rbp)
	jbe	.L1669
.L1581:
	movq	-112(%rbp), %rsi
	movq	-120(%rbp), %rdi
	movb	%bl, 256(%r12)
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE6resizeEm
	movq	-144(%rbp), %rax
	movl	$6, %esi
	movq	%r12, %rdi
	movq	-136(%rbp), %rdx
	movq	%rdx, 144(%rax)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE6ExpectENS0_5Token5ValueE
	movl	$1, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE
	subl	$1, 24(%r13)
	movl	$2, %eax
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1564:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE18ParseWithStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1664:
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner9PeekAheadEv@PLT
	movq	-232(%rbp), %rsi
	movq	-240(%rbp), %rdx
	cmpb	$73, %al
	movl	-244(%rbp), %ecx
	jne	.L1561
	movq	200(%r12), %rax
	movl	$238, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	xorl	%eax, %eax
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1669:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE8ValidateEi.part.0
	jmp	.L1581
	.p2align 4,,10
	.p2align 3
.L1667:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE10ParseBlockEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %eax
	cmpb	$65, %al
	je	.L1584
	cmpb	$71, %al
	jne	.L1670
.L1585:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE10ParseBlockEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	movq	200(%r12), %rdi
	.p2align 4,,10
	.p2align 3
.L1604:
	movq	24(%rdi), %rax
	cmpb	$0, 48(%rax)
	sete	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1668:
	movl	$278, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	jmp	.L1663
.L1666:
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner9PeekAheadEv@PLT
	movq	-232(%rbp), %rsi
	movq	-240(%rbp), %rdx
	cmpb	$96, %al
	jne	.L1582
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseForAwaitStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_
	jmp	.L1580
.L1670:
	movl	$278, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	xorl	%eax, %eax
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1605:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	cmpb	$5, 56(%rax)
	je	.L1671
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE10ParseBlockEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
.L1611:
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	cmpb	$71, 56(%rax)
	jne	.L1663
	jmp	.L1606
	.p2align 4,,10
	.p2align 3
.L1584:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	cmpb	$5, 56(%rax)
	je	.L1672
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE10ParseBlockEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
.L1590:
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	cmpb	$71, 56(%rax)
	jne	.L1604
	jmp	.L1585
.L1665:
	call	__stack_chk_fail@PLT
.L1671:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	(%r12), %rsi
	movl	$5, %edx
	movq	%r12, %rdi
	call	_ZNK2v88internal10ParserBaseINS0_9PreParserEE18NewScopeWithParentEPNS0_5ScopeENS0_9ScopeTypeE
	movq	%r12, %rdx
	leaq	-224(%rbp), %rdi
	movq	%rax, %rbx
	movq	200(%r12), %rax
	movq	(%rax), %rax
	movl	(%rax), %eax
	movl	%eax, 112(%rbx)
	movq	(%r12), %r14
	movq	%rbx, (%r12)
	movq	136(%r12), %rsi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE10BlockStateC1EPNS0_4ZoneEPPNS0_5ScopeE
	movq	200(%r12), %rdx
	movq	(%r12), %rax
	movq	(%rdx), %rdx
	movl	(%rdx), %edx
	movl	%edx, 112(%rax)
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %eax
	subl	$92, %eax
	cmpb	$9, %al
	ja	.L1673
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseNonRestrictedIdentifierEv
	movq	%rax, %rsi
	movq	200(%r12), %rax
	movq	24(%rax), %rax
	cmpb	$0, 48(%rax)
	jne	.L1613
	movq	%rbx, %rdi
	call	_ZN2v88internal5Scope24DeclareCatchVariableNameEPKNS0_12AstRawStringE@PLT
	movl	$6, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE6ExpectENS0_5Token5ValueE
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE10ParseBlockEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	movq	%rdx, %rax
	testq	%rdx, %rdx
	je	.L1662
.L1614:
	movq	8(%r13), %r13
	leaq	32(%rax), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal11VariableMap6LookupEPKNS0_12AstRawStringE@PLT
	testq	%rax, %rax
	je	.L1662
.L1625:
	testq	%r13, %r13
	je	.L1662
	movq	128(%r12), %rax
	movb	$1, (%rax)
	movb	$1, 2(%rax)
	movq	200(%r12), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L1615
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L1662:
	movq	200(%r12), %rax
.L1615:
	movq	(%rax), %rax
	movq	(%r12), %rdx
	movl	4(%rax), %eax
	movl	%eax, 116(%rdx)
	movq	(%r12), %rdi
	call	_ZN2v88internal5Scope18FinalizeBlockScopeEv@PLT
	movq	-224(%rbp), %rax
	movq	-216(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	200(%r12), %rax
	movq	%r14, (%r12)
	movq	(%rax), %rax
	movl	4(%rax), %eax
	movl	%eax, 116(%rbx)
	jmp	.L1611
.L1673:
	movq	40(%r12), %rax
	movq	%rbx, %rdi
	movq	56(%rax), %rax
	movq	264(%rax), %rsi
	call	_ZN2v88internal5Scope24DeclareCatchVariableNameEPKNS0_12AstRawStringE@PLT
	movq	%rax, %r13
	movq	(%r12), %rax
	movq	96(%rax), %r15
	movq	144(%r12), %rax
	movq	%r12, -176(%rbp)
	movb	$5, -160(%rbp)
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	je	.L1616
	cmpb	$0, 17(%rax)
	setne	-159(%rbp)
	movzbl	18(%rax), %eax
.L1632:
	movb	%al, -158(%rbp)
	movq	%r12, %rdi
	leaq	-176(%rbp), %rax
	movq	%rax, 144(%r12)
	movb	$0, -157(%rbp)
	movq	$0, -152(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseBindingPatternEv
	movl	%eax, -208(%rbp)
	movl	%eax, %ecx
	movq	200(%r12), %rax
	movq	(%rax), %rdx
	movl	4(%rdx), %esi
	movq	(%r12), %rdx
	movq	96(%rdx), %rdx
	cmpq	%rdx, %r15
	je	.L1622
.L1619:
	movq	(%r15), %rax
	movq	8(%rax), %rax
	movl	%esi, 36(%rax)
	movq	(%r15), %r15
	addq	$16, %r15
	cmpq	%r15, %rdx
	jne	.L1619
	movq	200(%r12), %rax
.L1622:
	movq	24(%rax), %rax
	movq	-176(%rbp), %rdx
	movq	-168(%rbp), %rsi
	cmpb	$0, 48(%rax)
	je	.L1674
	movq	%rsi, 144(%rdx)
.L1613:
	movq	-224(%rbp), %rax
	movq	-216(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	%r14, (%r12)
	jmp	.L1663
.L1672:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	(%r12), %rsi
	movl	$5, %edx
	movq	%r12, %rdi
	call	_ZNK2v88internal10ParserBaseINS0_9PreParserEE18NewScopeWithParentEPNS0_5ScopeENS0_9ScopeTypeE
	movq	%r12, %rdx
	leaq	-224(%rbp), %rdi
	movq	%rax, %rbx
	movq	200(%r12), %rax
	movq	(%rax), %rax
	movl	(%rax), %eax
	movl	%eax, 112(%rbx)
	movq	(%r12), %r13
	movq	%rbx, (%r12)
	movq	136(%r12), %rsi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE10BlockStateC1EPNS0_4ZoneEPPNS0_5ScopeE
	movq	200(%r12), %rdx
	movq	(%r12), %rax
	movq	(%rdx), %rdx
	movl	(%rdx), %edx
	movl	%edx, 112(%rax)
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %eax
	subl	$92, %eax
	cmpb	$9, %al
	ja	.L1675
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseNonRestrictedIdentifierEv
	movq	%rax, %rsi
	movq	200(%r12), %rax
	movq	24(%rax), %rax
	cmpb	$0, 48(%rax)
	jne	.L1592
	movq	%rbx, %rdi
	call	_ZN2v88internal5Scope24DeclareCatchVariableNameEPKNS0_12AstRawStringE@PLT
	movl	$6, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE6ExpectENS0_5Token5ValueE
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE10ParseBlockEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	movq	%rdx, %rax
	testq	%rdx, %rdx
	je	.L1661
.L1593:
	movq	8(%r14), %r14
	leaq	32(%rax), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal11VariableMap6LookupEPKNS0_12AstRawStringE@PLT
	testq	%rax, %rax
	je	.L1661
.L1603:
	testq	%r14, %r14
	je	.L1661
	movq	128(%r12), %rax
	movb	$1, (%rax)
	movb	$1, 2(%rax)
	movq	200(%r12), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L1594
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L1661:
	movq	200(%r12), %rax
.L1594:
	movq	(%rax), %rax
	movq	(%r12), %rdx
	movl	4(%rax), %eax
	movl	%eax, 116(%rdx)
	movq	(%r12), %rdi
	call	_ZN2v88internal5Scope18FinalizeBlockScopeEv@PLT
	movq	-224(%rbp), %rax
	movq	-216(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	200(%r12), %rax
	movq	%r13, (%r12)
	movq	(%rax), %rax
	movl	4(%rax), %eax
	movl	%eax, 116(%rbx)
	jmp	.L1590
.L1616:
	movb	$0, -159(%rbp)
	xorl	%eax, %eax
	jmp	.L1632
.L1674:
	movq	%rsi, 144(%rdx)
	movq	%r12, %rdi
	movl	$6, %esi
	movl	%ecx, -232(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE6ExpectENS0_5Token5ValueE
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE10ParseBlockEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	movl	-232(%rbp), %ecx
	testq	%rdx, %rdx
	movq	%rdx, %rdi
	movq	%rdx, %rax
	je	.L1662
	andb	$7, %cl
	je	.L1614
	movq	(%r12), %rsi
	movl	$2, %edx
	call	_ZN2v88internal5Scope22FindVariableDeclaredInEPS1_NS0_12VariableModeE@PLT
	movq	%rax, %r13
	jmp	.L1625
.L1675:
	movq	40(%r12), %rax
	movq	%rbx, %rdi
	movq	56(%rax), %rax
	movq	264(%rax), %rsi
	call	_ZN2v88internal5Scope24DeclareCatchVariableNameEPKNS0_12AstRawStringE@PLT
	movq	%rax, %r14
	movq	(%r12), %rax
	movq	96(%rax), %r15
	movq	144(%r12), %rax
	movq	%r12, -176(%rbp)
	movb	$5, -160(%rbp)
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	je	.L1595
	cmpb	$0, 17(%rax)
	setne	-159(%rbp)
	movzbl	18(%rax), %eax
.L1631:
	movb	%al, -158(%rbp)
	movq	%r12, %rdi
	leaq	-176(%rbp), %rax
	movq	%rax, 144(%r12)
	movb	$0, -157(%rbp)
	movq	$0, -152(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseBindingPatternEv
	movl	%eax, -208(%rbp)
	movl	%eax, %ecx
	movq	200(%r12), %rax
	movq	(%rax), %rax
	movl	4(%rax), %edx
	movq	(%r12), %rax
	movq	96(%rax), %rax
	jmp	.L1599
.L1676:
	movq	(%r15), %rsi
	movq	8(%rsi), %rsi
	movl	%edx, 36(%rsi)
	movq	(%r15), %r15
	addq	$16, %r15
.L1599:
	cmpq	%rax, %r15
	jne	.L1676
	movq	200(%r12), %rsi
	movq	-176(%rbp), %rax
	movq	-168(%rbp), %rdx
	movq	24(%rsi), %rsi
	cmpb	$0, 48(%rsi)
	je	.L1600
	movq	%rdx, 144(%rax)
.L1592:
	movq	-224(%rbp), %rax
	movq	-216(%rbp), %rdx
	movq	%rdx, (%rax)
	xorl	%eax, %eax
	movq	%r13, (%r12)
	jmp	.L1580
.L1595:
	movb	$0, -159(%rbp)
	xorl	%eax, %eax
	jmp	.L1631
.L1600:
	movq	%rdx, 144(%rax)
	movl	$6, %esi
	movq	%r12, %rdi
	movl	%ecx, -232(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE6ExpectENS0_5Token5ValueE
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE10ParseBlockEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	movl	-232(%rbp), %ecx
	testq	%rdx, %rdx
	movq	%rdx, %rdi
	movq	%rdx, %rax
	je	.L1661
	andb	$7, %cl
	je	.L1593
	movq	(%r12), %rsi
	movl	$2, %edx
	call	_ZN2v88internal5Scope22FindVariableDeclaredInEPS1_NS0_12VariableModeE@PLT
	movq	%rax, %r14
	jmp	.L1603
	.cfi_endproc
.LFE25145:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseStatementListItemEv,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseStatementListItemEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseStatementListItemEv
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseStatementListItemEv, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseStatementListItemEv:
.LFB24310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	200(%rdi), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %eax
	subl	$73, %eax
	cmpb	$31, %al
	ja	.L1688
	leaq	.L1680(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseStatementListItemEv,"aG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseStatementListItemEv,comdat
	.align 4
	.align 4
.L1680:
	.long	.L1684-.L1680
	.long	.L1688-.L1680
	.long	.L1688-.L1680
	.long	.L1688-.L1680
	.long	.L1688-.L1680
	.long	.L1688-.L1680
	.long	.L1688-.L1680
	.long	.L1679-.L1680
	.long	.L1688-.L1680
	.long	.L1688-.L1680
	.long	.L1688-.L1680
	.long	.L1688-.L1680
	.long	.L1688-.L1680
	.long	.L1688-.L1680
	.long	.L1688-.L1680
	.long	.L1688-.L1680
	.long	.L1688-.L1680
	.long	.L1688-.L1680
	.long	.L1688-.L1680
	.long	.L1688-.L1680
	.long	.L1688-.L1680
	.long	.L1688-.L1680
	.long	.L1683-.L1680
	.long	.L1688-.L1680
	.long	.L1688-.L1680
	.long	.L1682-.L1680
	.long	.L1688-.L1680
	.long	.L1688-.L1680
	.long	.L1688-.L1680
	.long	.L1688-.L1680
	.long	.L1681-.L1680
	.long	.L1679-.L1680
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseStatementListItemEv,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseStatementListItemEv,comdat
	.p2align 4,,10
	.p2align 3
.L1683:
	call	_ZN2v88internal7Scanner9PeekAheadEv@PLT
	cmpb	$73, %al
	je	.L1705
.L1688:
	addq	$24, %rsp
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	popq	%rbx
	xorl	%esi, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE
	.p2align 4,,10
	.p2align 3
.L1682:
	.cfi_restore_state
	call	_ZN2v88internal7Scanner9PeekAheadEv@PLT
	cmpb	$100, %al
	je	.L1686
	ja	.L1688
	cmpb	$8, %al
	je	.L1679
	jbe	.L1706
	subl	$92, %eax
	cmpb	$7, %al
	ja	.L1688
.L1679:
	addq	$24, %rsp
	movq	%r12, %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseVariableStatementENS3_26VariableDeclarationContextEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	.p2align 4,,10
	.p2align 3
.L1681:
	.cfi_restore_state
	call	_ZN2v88internal7Scanner4NextEv@PLT
	addq	$24, %rsp
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%rbx
	xorl	%esi, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ParseClassDeclarationEPNS0_8ZoneListIPKNS0_12AstRawStringEEEb
	.p2align 4,,10
	.p2align 3
.L1684:
	.cfi_restore_state
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rdi
	xorl	%edx, %edx
	movq	(%rdi), %rax
	movl	(%rax), %esi
	movq	8(%rdi), %rax
	cmpb	$40, 56(%rax)
	je	.L1707
.L1685:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
.L1704:
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseHoistableDeclarationEiNS_4base5FlagsINS0_17ParseFunctionFlagEiEEPNS0_8ZoneListIPKNS0_12AstRawStringEEEb
	.p2align 4,,10
	.p2align 3
.L1706:
	.cfi_restore_state
	cmpb	$3, %al
	je	.L1679
	jmp	.L1688
	.p2align 4,,10
	.p2align 3
.L1686:
	movq	(%r12), %rax
	testb	$1, 129(%rax)
	je	.L1679
	jmp	.L1688
	.p2align 4,,10
	.p2align 3
.L1705:
	movq	200(%r12), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner9PeekAheadEv@PLT
	movq	16(%rbx), %rax
	cmpb	$0, 76(%rax)
	jne	.L1688
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rdi
	movq	(%rdi), %rax
	movl	(%rax), %r13d
	movl	4(%rax), %edx
	subl	%r13d, %edx
	cmpb	$90, 56(%rax)
	jne	.L1690
	subl	$2, %edx
.L1690:
	cmpb	$0, 28(%rax)
	movl	24(%rax), %ecx
	jne	.L1691
	sarl	%ecx
.L1691:
	cmpl	%ecx, %edx
	jne	.L1708
.L1692:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$2, %edx
	movl	%r13d, %esi
	jmp	.L1704
	.p2align 4,,10
	.p2align 3
.L1707:
	movl	%esi, -36(%rbp)
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movl	-36(%rbp), %esi
	movl	$1, %edx
	jmp	.L1685
.L1708:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	movq	200(%r12), %rdi
	movq	(%rdi), %rax
	movl	(%rax), %r13d
	jmp	.L1692
	.cfi_endproc
.LFE24310:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseStatementListItemEv, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseStatementListItemEv
	.section	.rodata._ZN2v88internal9PreParser32ParseStatementListAndLogFunctionEPNS0_25PreParserFormalParametersE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"use strict"
	.section	.text._ZN2v88internal9PreParser32ParseStatementListAndLogFunctionEPNS0_25PreParserFormalParametersE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9PreParser32ParseStatementListAndLogFunctionEPNS0_25PreParserFormalParametersE
	.type	_ZN2v88internal9PreParser32ParseStatementListAndLogFunctionEPNS0_25PreParserFormalParametersE, @function
_ZN2v88internal9PreParser32ParseStatementListAndLogFunctionEPNS0_25PreParserFormalParametersE:
.LFB20775:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	200(%rdi), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %edx
	cmpb	$90, %dl
	je	.L1717
	jmp	.L1710
	.p2align 4,,10
	.p2align 3
.L1714:
	movq	200(%r15), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %edx
	cmpb	$90, %dl
	jne	.L1710
.L1717:
	movzbl	28(%rax), %r14d
	movl	(%rax), %r12d
	movl	4(%rax), %r13d
	testb	%r14b, %r14b
	je	.L1711
	movl	%r13d, %ecx
	xorl	%r14d, %r14d
	subl	%r12d, %ecx
	cmpl	$12, %ecx
	jne	.L1711
	cmpl	$10, 24(%rax)
	jne	.L1711
	movq	8(%rax), %rdi
	movl	$10, %ecx
	leaq	.LC5(%rip), %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	sete	%r14b
	.p2align 4,,10
	.p2align 3
.L1711:
	movq	%r15, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseStatementListItemEv
	testl	%eax, %eax
	je	.L1712
	cmpl	$4, %eax
	jne	.L1713
	testb	%r14b, %r14b
	je	.L1714
	movq	(%r15), %rax
	orb	$1, 129(%rax)
	movq	(%r15), %rdi
	call	_ZN2v88internal5Scope19HasSimpleParametersEv@PLT
	testb	%al, %al
	jne	.L1714
	movq	128(%r15), %rdi
	movl	%r13d, %edx
	movl	$242, %ecx
	movl	%r12d, %esi
	leaq	.LC5(%rip), %r8
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r15), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L1716
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	movq	200(%r15), %rax
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1713:
	movq	200(%r15), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %edx
.L1710:
	cmpb	$13, %dl
	jne	.L1720
	jmp	.L1718
	.p2align 4,,10
	.p2align 3
.L1719:
	cmpb	$13, 56(%rax)
	je	.L1718
.L1720:
	movq	%r15, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseStatementListItemEv
	testl	%eax, %eax
	movq	200(%r15), %rax
	movq	8(%rax), %rax
	jne	.L1719
.L1718:
	movzbl	8(%rbx), %edx
	movl	16(%rbx), %esi
	movd	4(%rax), %xmm2
	movd	12(%rbx), %xmm1
	movd	208(%r15), %xmm3
	subl	%edx, %esi
	movd	%esi, %xmm0
	punpckldq	%xmm0, %xmm2
	punpckldq	%xmm3, %xmm1
	movdqa	%xmm2, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 272(%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1712:
	.cfi_restore_state
	movq	200(%r15), %rax
.L1716:
	movq	8(%rax), %rax
	jmp	.L1718
	.cfi_endproc
.LFE20775:
	.size	_ZN2v88internal9PreParser32ParseStatementListAndLogFunctionEPNS0_25PreParserFormalParametersE, .-_ZN2v88internal9PreParser32ParseStatementListAndLogFunctionEPNS0_25PreParserFormalParametersE
	.section	.text._ZN2v88internal9PreParser15PreParseProgramEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9PreParser15PreParseProgramEv
	.type	_ZN2v88internal9PreParser15PreParseProgramEv, @function
_ZN2v88internal9PreParser15PreParseProgramEv:
.LFB20767:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	136(%rdi), %r13
	movq	40(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%r13), %r12
	movq	24(%r13), %rax
	subq	%r12, %rax
	cmpq	$223, %rax
	jbe	.L1789
	leaq	224(%r12), %rax
	movq	%rax, 16(%r13)
.L1738:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r12, %r14
	call	_ZN2v88internal16DeclarationScopeC1EPNS0_4ZoneEPNS0_15AstValueFactoryE@PLT
	cmpb	$0, 113(%r15)
	jne	.L1790
.L1739:
	movq	(%r15), %rax
	leaq	-128(%rbp), %rdx
	movq	%r15, -128(%rbp)
	movq	%r14, (%r15)
	movq	%rax, -120(%rbp)
	leaq	16(%r15), %rax
	movq	%rax, -96(%rbp)
	movq	16(%r15), %rax
	movq	$0, -112(%rbp)
	movq	%rdx, 16(%r15)
	movq	%r14, %rdx
	movl	$0, -104(%rbp)
	movq	%rax, -88(%rbp)
	movq	%r14, -80(%rbp)
	movl	$0, -72(%rbp)
	testq	%rax, %rax
	je	.L1742
	movzbl	57(%rax), %edx
	movb	$0, 57(%rax)
	movb	%dl, 58(%rax)
	movq	(%r15), %rdx
.L1742:
	movq	200(%r15), %rcx
	movq	%rdx, 8(%r15)
	movq	8(%rcx), %rax
	movl	(%rax), %r12d
	movzbl	56(%rax), %edx
	movl	%r12d, -136(%rbp)
	cmpb	$90, %dl
	je	.L1751
	jmp	.L1743
	.p2align 4,,10
	.p2align 3
.L1747:
	movq	200(%r15), %rcx
	movq	8(%rcx), %rax
	movzbl	56(%rax), %edx
	cmpb	$90, %dl
	jne	.L1743
	movl	(%rax), %r12d
.L1751:
	movzbl	28(%rax), %ebx
	movl	4(%rax), %r13d
	testb	%bl, %bl
	je	.L1744
	movl	%r13d, %ecx
	xorl	%ebx, %ebx
	subl	%r12d, %ecx
	cmpl	$12, %ecx
	jne	.L1744
	cmpl	$10, 24(%rax)
	jne	.L1744
	movq	8(%rax), %rdi
	movl	$10, %ecx
	leaq	.LC5(%rip), %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	sete	%bl
	.p2align 4,,10
	.p2align 3
.L1744:
	movq	%r15, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseStatementListItemEv
	testl	%eax, %eax
	je	.L1745
	cmpl	$4, %eax
	jne	.L1746
	testb	%bl, %bl
	je	.L1747
	movq	(%r15), %rax
	orb	$1, 129(%rax)
	movq	(%r15), %rdi
	call	_ZN2v88internal5Scope19HasSimpleParametersEv@PLT
	testb	%al, %al
	jne	.L1747
	movq	128(%r15), %rdi
	movl	%r13d, %edx
	movl	$242, %ecx
	movl	%r12d, %esi
	leaq	.LC5(%rip), %r8
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r15), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	je	.L1791
	.p2align 4,,10
	.p2align 3
.L1749:
	movq	128(%r15), %rdx
	movq	$0, 8(%r15)
	xorl	%eax, %eax
	cmpb	$0, 1(%rdx)
	jne	.L1757
	movq	(%r15), %rax
	testb	$1, 129(%rax)
	jne	.L1758
.L1788:
	movl	$2, %eax
.L1757:
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rcx
	movq	%rcx, (%rdx)
	movq	-128(%rbp), %rdx
	movq	-120(%rbp), %rcx
	movq	%rcx, (%rdx)
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L1792
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1746:
	.cfi_restore_state
	movq	200(%r15), %rcx
	movq	8(%rcx), %rax
	movzbl	56(%rax), %edx
.L1743:
	cmpb	$14, %dl
	jne	.L1755
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L1754:
	movq	8(%rax), %rdx
	cmpb	$14, 56(%rdx)
	je	.L1787
.L1755:
	movq	%r15, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseStatementListItemEv
	testl	%eax, %eax
	movq	200(%r15), %rax
	jne	.L1754
.L1787:
	movq	24(%rax), %rax
	movzbl	48(%rax), %eax
.L1752:
	testb	%al, %al
	jne	.L1749
	movq	%r14, %rdi
	call	_ZN2v88internal16DeclarationScope31CheckConflictingVarDeclarationsEv@PLT
	testq	%rax, %rax
	je	.L1749
	movl	(%rax), %esi
	movq	8(%rax), %rcx
	movl	$0, %eax
	movq	128(%r15), %rdi
	cmpl	$-1, %esi
	leal	1(%rsi), %edx
	movq	8(%rcx), %r8
	movl	$175, %ecx
	cmove	%eax, %edx
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKNS0_12AstRawStringE@PLT
	movq	200(%r15), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L1749
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L1749
	.p2align 4,,10
	.p2align 3
.L1758:
	movq	200(%r15), %rax
	movl	336(%rax), %edx
	movl	340(%rax), %esi
	cmpl	%edx, -136(%rbp)
	setle	%dil
	cmpl	%esi, %edx
	setbe	%cl
	testb	%cl, %dil
	je	.L1788
	movq	(%rax), %rcx
	cmpl	%esi, 4(%rcx)
	jl	.L1788
	movl	344(%rax), %ebx
	movq	%rsi, %rax
	movl	%edx, %esi
	movq	%r15, %rdi
	salq	$32, %rax
	orq	%rax, %rsi
	movl	%ebx, %edx
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	movq	200(%r15), %rax
	movl	$4294967295, %esi
	movq	%rsi, 336(%rax)
	movl	$0, 344(%rax)
	cmpl	$298, %ebx
	jne	.L1788
	movq	264(%r15), %rax
	testq	%rax, %rax
	je	.L1788
	addl	$1, 128(%rax)
	jmp	.L1788
	.p2align 4,,10
	.p2align 3
.L1790:
	movq	136(%r15), %rdi
	movq	40(%r15), %rdx
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$231, %rax
	jbe	.L1793
	leaq	232(%r14), %rax
	movq	%rax, 16(%rdi)
.L1741:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11ModuleScopeC1EPNS0_16DeclarationScopeEPNS0_15AstValueFactoryE@PLT
	jmp	.L1739
	.p2align 4,,10
	.p2align 3
.L1745:
	movq	200(%r15), %rax
	movq	24(%rax), %rax
	movzbl	48(%rax), %eax
	jmp	.L1752
.L1789:
	movl	$224, %esi
	movq	%r13, %rdi
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-136(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L1738
.L1791:
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	movq	200(%r15), %rax
	movq	24(%rax), %rax
	movzbl	48(%rax), %eax
	jmp	.L1752
.L1793:
	movl	$232, %esi
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-136(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L1741
.L1753:
	movq	24(%rcx), %rax
	movzbl	48(%rax), %eax
	jmp	.L1752
.L1792:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20767:
	.size	_ZN2v88internal9PreParser15PreParseProgramEv, .-_ZN2v88internal9PreParser15PreParseProgramEv
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseAsyncFunctionBodyEPNS0_5ScopeEPNS0_28PreParserScopedStatementListE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseAsyncFunctionBodyEPNS0_5ScopeEPNS0_28PreParserScopedStatementListE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseAsyncFunctionBodyEPNS0_5ScopeEPNS0_28PreParserScopedStatementListE
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseAsyncFunctionBodyEPNS0_5ScopeEPNS0_28PreParserScopedStatementListE, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseAsyncFunctionBodyEPNS0_5ScopeEPNS0_28PreParserScopedStatementListE:
.LFB24351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	200(%rdi), %rdx
	movq	8(%rdx), %rax
	movzbl	56(%rax), %ecx
	cmpb	$90, %cl
	je	.L1802
	jmp	.L1795
	.p2align 4,,10
	.p2align 3
.L1799:
	movq	200(%r15), %rdx
	movq	8(%rdx), %rax
	movzbl	56(%rax), %ecx
	cmpb	$90, %cl
	jne	.L1795
.L1802:
	movzbl	28(%rax), %r14d
	movl	(%rax), %r12d
	movl	4(%rax), %r13d
	testb	%r14b, %r14b
	je	.L1796
	movl	%r13d, %ecx
	xorl	%r14d, %r14d
	subl	%r12d, %ecx
	cmpl	$12, %ecx
	jne	.L1796
	cmpl	$10, 24(%rax)
	jne	.L1796
	movq	8(%rax), %rdi
	movl	$10, %ecx
	leaq	.LC5(%rip), %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	sete	%r14b
	.p2align 4,,10
	.p2align 3
.L1796:
	movq	%r15, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseStatementListItemEv
	testl	%eax, %eax
	je	.L1797
	cmpl	$4, %eax
	jne	.L1798
	testb	%r14b, %r14b
	je	.L1799
	movq	(%r15), %rax
	orb	$1, 129(%rax)
	movq	(%r15), %rdi
	call	_ZN2v88internal5Scope19HasSimpleParametersEv@PLT
	testb	%al, %al
	jne	.L1799
	movq	128(%r15), %rdi
	movl	%r13d, %edx
	movl	$242, %ecx
	movl	%r12d, %esi
	leaq	.LC5(%rip), %r8
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r15), %rdx
	movq	24(%rdx), %rax
	cmpb	$0, 48(%rax)
	jne	.L1803
	movl	$-1, 32(%rdx)
	movq	24(%rax), %rcx
	movb	$1, 48(%rax)
	movq	%rcx, 16(%rax)
	movb	$109, 96(%rdx)
	movb	$109, 176(%rdx)
	movb	$109, 256(%rdx)
	movq	200(%r15), %rdx
.L1803:
	movq	(%rdx), %rax
	movl	4(%rax), %eax
	movl	%eax, 116(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1798:
	.cfi_restore_state
	movq	200(%r15), %rdx
	movq	8(%rdx), %rax
	movzbl	56(%rax), %ecx
.L1795:
	cmpb	$13, %cl
	jne	.L1805
	jmp	.L1803
	.p2align 4,,10
	.p2align 3
.L1804:
	movq	8(%rdx), %rax
	cmpb	$13, 56(%rax)
	je	.L1803
.L1805:
	movq	%r15, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseStatementListItemEv
	movq	200(%r15), %rdx
	testl	%eax, %eax
	jne	.L1804
	jmp	.L1803
	.p2align 4,,10
	.p2align 3
.L1797:
	movq	200(%r15), %rdx
	jmp	.L1803
	.cfi_endproc
.LFE24351:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseAsyncFunctionBodyEPNS0_5ScopeEPNS0_28PreParserScopedStatementListE, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseAsyncFunctionBodyEPNS0_5ScopeEPNS0_28PreParserScopedStatementListE
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseFunctionBodyEPNS0_28PreParserScopedStatementListENS0_19PreParserIdentifierEiRKNS0_25PreParserFormalParametersENS0_12FunctionKindENS0_18FunctionSyntaxKindENS3_16FunctionBodyTypeE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseFunctionBodyEPNS0_28PreParserScopedStatementListENS0_19PreParserIdentifierEiRKNS0_25PreParserFormalParametersENS0_12FunctionKindENS0_18FunctionSyntaxKindENS3_16FunctionBodyTypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseFunctionBodyEPNS0_28PreParserScopedStatementListENS0_19PreParserIdentifierEiRKNS0_25PreParserFormalParametersENS0_12FunctionKindENS0_18FunctionSyntaxKindENS3_16FunctionBodyTypeE
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseFunctionBodyEPNS0_28PreParserScopedStatementListENS0_19PreParserIdentifierEiRKNS0_25PreParserFormalParametersENS0_12FunctionKindENS0_18FunctionSyntaxKindENS3_16FunctionBodyTypeE, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseFunctionBodyEPNS0_28PreParserScopedStatementListENS0_19PreParserIdentifierEiRKNS0_25PreParserFormalParametersENS0_12FunctionKindENS0_18FunctionSyntaxKindENS3_16FunctionBodyTypeE:
.LFB23041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$168, %rsp
	movl	24(%rbp), %eax
	movl	16(%rbp), %r12d
	movq	%rdx, -200(%rbp)
	movl	%eax, -172(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	144(%rdi), %rax
	movq	$0, 144(%rdi)
	movq	(%r9), %r13
	cmpb	$0, 9(%r9)
	movq	%rax, -184(%rbp)
	movq	%r13, -168(%rbp)
	je	.L1988
.L1822:
	movq	(%r14), %rax
	movq	%rax, -192(%rbp)
	movq	-168(%rbp), %rax
	movq	%rax, (%r14)
	movl	32(%rbp), %eax
	testl	%eax, %eax
	je	.L1989
	cmpb	$4, -172(%rbp)
	sete	%al
	addl	$13, %eax
	movb	%al, -176(%rbp)
	leal	-12(%r12), %eax
	cmpb	$1, %al
	jbe	.L1990
	cmpb	$3, %al
	jbe	.L1991
	leal	-9(%r12), %eax
	cmpb	$4, %al
	jbe	.L1992
	movq	200(%r14), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %edx
	cmpb	$90, %dl
	jne	.L1858
	movq	%rbx, -208(%rbp)
	movq	%r14, %rbx
	jmp	.L1863
	.p2align 4,,10
	.p2align 3
.L1862:
	movq	200(%rbx), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %edx
	cmpb	$90, %dl
	jne	.L1993
.L1863:
	movzbl	28(%rax), %r14d
	movq	(%rax), %r15
	testb	%r14b, %r14b
	je	.L1859
	movl	4(%rax), %edx
	subl	(%rax), %edx
	xorl	%r14d, %r14d
	cmpl	$12, %edx
	jne	.L1859
	cmpl	$10, 24(%rax)
	jne	.L1859
	movq	8(%rax), %rdi
	movl	$10, %ecx
	leaq	.LC5(%rip), %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	sete	%r14b
	.p2align 4,,10
	.p2align 3
.L1859:
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseStatementListItemEv
	testl	%eax, %eax
	je	.L1977
	cmpl	$4, %eax
	jne	.L1861
	testb	%r14b, %r14b
	je	.L1862
	movq	(%rbx), %rax
	orb	$1, 129(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internal5Scope19HasSimpleParametersEv@PLT
	testb	%al, %al
	jne	.L1862
	movq	%rbx, %r14
	movq	%r15, %rsi
	leaq	.LC5(%rip), %rcx
	movl	$242, %edx
	movq	%r14, %rdi
	movq	-208(%rbp), %rbx
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc
	.p2align 4,,10
	.p2align 3
.L1864:
	leal	-4(%r12), %eax
	cmpb	$1, %al
	ja	.L1981
	leaq	-144(%rbp), %r8
	movq	%r14, %rsi
	movq	%r8, %rdi
	movq	%r8, -208(%rbp)
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPS3_NS0_15ExpressionScopeIS4_E9ScopeTypeE.constprop.0
	movq	(%r14), %rdi
	call	_ZN2v88internal5Scope15GetClosureScopeEv@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal5Scope16GetReceiverScopeEv@PLT
	movq	-208(%rbp), %r8
	movq	176(%rax), %rdx
	orw	$2048, 40(%rdx)
	cmpq	%rax, %r15
	je	.L1994
	orb	$8, 132(%r15)
	orw	$1024, 40(%rdx)
.L1872:
	movl	-84(%rbp), %eax
	cmpl	%eax, -88(%rbp)
	jbe	.L1995
.L1875:
	movq	-120(%rbp), %rdi
	movq	-112(%rbp), %rsi
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE6resizeEm
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	%rdx, 144(%rax)
	movq	200(%r14), %rdi
.L1869:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	%al, -176(%rbp)
	je	.L1836
	movq	%r14, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L1836
	.p2align 4,,10
	.p2align 3
.L1990:
	movq	200(%r14), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %edx
	cmpb	$90, %dl
	jne	.L1839
	movq	%rbx, -208(%rbp)
	movq	%r14, %rbx
	jmp	.L1844
	.p2align 4,,10
	.p2align 3
.L1843:
	movq	200(%rbx), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %edx
	cmpb	$90, %dl
	jne	.L1996
.L1844:
	movzbl	28(%rax), %r14d
	movq	(%rax), %r15
	testb	%r14b, %r14b
	je	.L1840
	movl	4(%rax), %edx
	subl	(%rax), %edx
	xorl	%r14d, %r14d
	cmpl	$12, %edx
	jne	.L1840
	cmpl	$10, 24(%rax)
	jne	.L1840
	movq	8(%rax), %rdi
	movl	$10, %ecx
	leaq	.LC5(%rip), %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	sete	%r14b
	.p2align 4,,10
	.p2align 3
.L1840:
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseStatementListItemEv
	testl	%eax, %eax
	je	.L1976
	cmpl	$4, %eax
	jne	.L1842
	testb	%r14b, %r14b
	je	.L1843
	movq	(%rbx), %rax
	orb	$1, 129(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internal5Scope19HasSimpleParametersEv@PLT
	testb	%al, %al
	jne	.L1843
.L1984:
	movq	%rbx, %r14
	movq	%r15, %rsi
	leaq	.LC5(%rip), %rcx
	movl	$242, %edx
	movq	%r14, %rdi
	movq	-208(%rbp), %rbx
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc
	jmp	.L1981
	.p2align 4,,10
	.p2align 3
.L1989:
	leaq	-144(%rbp), %r15
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPS3_NS0_15ExpressionScopeIS4_E9ScopeTypeE.constprop.0
	movq	%r14, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseAssignmentExpressionCoverGrammarEv
	movl	%eax, -176(%rbp)
	movl	-84(%rbp), %eax
	cmpl	%eax, -88(%rbp)
	jbe	.L1997
.L1830:
	movq	-120(%rbp), %rdi
	movq	-112(%rbp), %rsi
	movq	8(%rdi), %rcx
	movq	(%rdi), %rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	ja	.L1998
	jb	.L1999
.L1832:
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	%rdx, 144(%rax)
	leal	-9(%r12), %eax
	cmpb	$4, %al
	ja	.L2000
.L1836:
	movq	-192(%rbp), %rcx
	movq	200(%r14), %rax
	movq	%rcx, (%r14)
	movq	(%rax), %rax
	movl	4(%rax), %eax
	movl	%eax, 116(%rcx)
	movq	200(%r14), %rax
	movq	24(%rax), %rax
	cmpb	$0, 48(%rax)
	jne	.L1876
	movq	-168(%rbp), %rdi
	call	_ZN2v88internal16DeclarationScope31CheckConflictingVarDeclarationsEv@PLT
	testq	%rax, %rax
	je	.L1876
	movl	(%rax), %esi
	movq	8(%rax), %rcx
	movl	$0, %eax
	movq	128(%r14), %rdi
	cmpl	$-1, %esi
	leal	1(%rsi), %edx
	movq	8(%rcx), %r8
	movl	$175, %ecx
	cmove	%eax, %edx
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKNS0_12AstRawStringE@PLT
	movq	200(%r14), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	je	.L2001
	.p2align 4,,10
	.p2align 3
.L1876:
	cmpb	$0, 9(%rbx)
	je	.L1878
	testb	$1, 129(%r13)
	jne	.L1983
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal16DeclarationScope25HoistSloppyBlockFunctionsEPNS0_14AstNodeFactoryE@PLT
	testb	$1, 129(%r13)
	jne	.L1983
	movq	(%r14), %rax
	leal	-11(%r12), %edx
	movzbl	129(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %dl
	jbe	.L2002
	leal	-15(%r12), %edx
	testb	%al, %al
	je	.L2003
	cmpb	$2, %dl
	jbe	.L1880
	cmpb	$0, 21(%rbx)
	je	.L1890
	.p2align 4,,10
	.p2align 3
.L1985:
	movq	128(%r14), %rax
	movb	$1, (%rax)
	movb	$1, 2(%rax)
	movq	200(%r14), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L1890
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L1890
	.p2align 4,,10
	.p2align 3
.L1878:
	movq	-168(%rbp), %rdi
	movzbl	129(%rdi), %eax
	movl	%eax, %edx
	movb	%al, -192(%rbp)
	movzbl	129(%r13), %eax
	andl	$1, %edx
	andl	$-2, %eax
	orl	%edx, %eax
	movb	%al, 129(%r13)
	testb	$1, 129(%rdi)
	jne	.L1886
	xorl	%esi, %esi
	call	_ZN2v88internal16DeclarationScope25HoistSloppyBlockFunctionsEPNS0_14AstNodeFactoryE@PLT
.L1886:
	movq	200(%r14), %rax
	movq	-168(%rbp), %r15
	movq	(%rax), %rax
	movq	%r15, %rdi
	movl	4(%rax), %eax
	movl	%eax, 116(%r15)
	call	_ZN2v88internal5Scope18FinalizeBlockScopeEv@PLT
	testq	%rax, %rax
	je	.L1983
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal5Scope22FindVariableDeclaredInEPS1_NS0_12VariableModeE@PLT
	testq	%rax, %rax
	je	.L1983
	movq	128(%r14), %rax
	movb	$1, (%rax)
	movb	$1, 2(%rax)
	movq	200(%r14), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L1983
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	.p2align 4,,10
	.p2align 3
.L1983:
	movq	(%r14), %rax
	testb	$1, 129(%rax)
	je	.L1881
.L1880:
	cmpb	$0, 20(%rbx)
	jne	.L2004
.L1896:
	cmpb	$0, 21(%rbx)
	jne	.L1985
.L1890:
	subl	$8, %r12d
	cmpb	$1, %r12b
	jbe	.L1892
	movq	40(%r14), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal16DeclarationScope16DeclareArgumentsEPNS0_15AstValueFactoryE@PLT
.L1892:
	cmpb	$1, -172(%rbp)
	je	.L2005
.L1894:
	movq	-184(%rbp), %rax
	movq	%rax, 144(%r14)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2006
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2002:
	.cfi_restore_state
	testb	%al, %al
	jne	.L1880
.L1881:
	cmpb	$0, 20(%rbx)
	je	.L1890
	jmp	.L1985
	.p2align 4,,10
	.p2align 3
.L1991:
	movq	200(%r14), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %edx
	cmpb	$90, %dl
	jne	.L1849
	movq	%rbx, -208(%rbp)
	movq	%r14, %rbx
	jmp	.L1854
	.p2align 4,,10
	.p2align 3
.L1853:
	movq	200(%rbx), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %edx
	cmpb	$90, %dl
	jne	.L2007
.L1854:
	movzbl	28(%rax), %r14d
	movq	(%rax), %r15
	testb	%r14b, %r14b
	je	.L1850
	movl	4(%rax), %edx
	subl	(%rax), %edx
	xorl	%r14d, %r14d
	cmpl	$12, %edx
	jne	.L1850
	cmpl	$10, 24(%rax)
	jne	.L1850
	movq	8(%rax), %rdi
	movl	$10, %ecx
	leaq	.LC5(%rip), %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	sete	%r14b
	.p2align 4,,10
	.p2align 3
.L1850:
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseStatementListItemEv
	testl	%eax, %eax
	je	.L1976
	cmpl	$4, %eax
	jne	.L1852
	testb	%r14b, %r14b
	je	.L1853
	movq	(%rbx), %rax
	orb	$1, 129(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internal5Scope19HasSimpleParametersEv@PLT
	testb	%al, %al
	jne	.L1853
	jmp	.L1984
	.p2align 4,,10
	.p2align 3
.L2001:
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L1876
	.p2align 4,,10
	.p2align 3
.L2003:
	cmpb	$2, %dl
	ja	.L1890
	jmp	.L1881
	.p2align 4,,10
	.p2align 3
.L1999:
	salq	$4, %rsi
	addq	%rdx, %rsi
	cmpq	%rsi, %rcx
	je	.L1832
	movq	%rsi, 8(%rdi)
	jmp	.L1832
	.p2align 4,,10
	.p2align 3
.L2005:
	movq	-200(%rbp), %rsi
	leaq	32(%r13), %rdi
	call	_ZN2v88internal11VariableMap6LookupEPKNS0_12AstRawStringE@PLT
	testq	%rax, %rax
	jne	.L1894
	movq	-200(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal16DeclarationScope18DeclareFunctionVarEPKNS0_12AstRawStringEPNS0_5ScopeE@PLT
	jmp	.L1894
	.p2align 4,,10
	.p2align 3
.L1842:
	movq	%rbx, %r14
	movq	-208(%rbp), %rbx
	movq	200(%r14), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %edx
.L1839:
	cmpb	$13, %dl
	jne	.L1847
	jmp	.L1869
	.p2align 4,,10
	.p2align 3
.L1846:
	movq	8(%rdi), %rax
	cmpb	$13, 56(%rax)
	je	.L1869
.L1847:
	movq	%r14, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseStatementListItemEv
	movq	200(%r14), %rdi
	testl	%eax, %eax
	jne	.L1846
	jmp	.L1869
	.p2align 4,,10
	.p2align 3
.L1976:
	movq	%rbx, %r14
	movq	-208(%rbp), %rbx
.L1981:
	movq	200(%r14), %rdi
	jmp	.L1869
	.p2align 4,,10
	.p2align 3
.L2004:
	movq	128(%r14), %rax
	movb	$1, (%rax)
	movb	$1, 2(%rax)
	movq	200(%r14), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L1896
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L1896
	.p2align 4,,10
	.p2align 3
.L1852:
	movq	%rbx, %r14
	movq	-208(%rbp), %rbx
	movq	200(%r14), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %edx
.L1849:
	cmpb	$13, %dl
	jne	.L1856
	jmp	.L1869
	.p2align 4,,10
	.p2align 3
.L1855:
	movq	8(%rdi), %rax
	cmpb	$13, 56(%rax)
	je	.L1869
.L1856:
	movq	%r14, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseStatementListItemEv
	movq	200(%r14), %rdi
	testl	%eax, %eax
	jne	.L1855
	jmp	.L1869
	.p2align 4,,10
	.p2align 3
.L2000:
	movq	16(%r14), %rax
	movq	48(%rax), %rdi
	testb	$7, -176(%rbp)
	jne	.L2008
.L1834:
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	jmp	.L1836
	.p2align 4,,10
	.p2align 3
.L1998:
	subq	%rax, %rsi
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm
	jmp	.L1832
	.p2align 4,,10
	.p2align 3
.L1997:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE8ValidateEi.part.0
	jmp	.L1830
	.p2align 4,,10
	.p2align 3
.L1988:
	movq	200(%rdi), %rax
	movq	24(%rax), %rax
	cmpb	$0, 48(%rax)
	jne	.L1894
	movq	(%rdi), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	testb	$4, 129(%rax)
	je	.L1824
	movq	288(%r14), %rax
	testq	%rax, %rax
	je	.L1824
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1825
	orb	$1, 76(%rdx)
.L1824:
	movq	200(%r14), %rax
	movq	24(%rax), %rax
	cmpb	$0, 48(%rax)
	jne	.L1894
	movq	136(%r14), %r15
	movq	(%r14), %rdx
	movq	16(%r15), %rcx
	movq	24(%r15), %rax
	subq	%rcx, %rax
	cmpq	$223, %rax
	jbe	.L2009
	leaq	224(%rcx), %rax
	movq	%rax, 16(%r15)
.L1827:
	movq	%rcx, %rax
	movq	%rcx, -168(%rbp)
	movq	%r15, %rsi
	xorl	%r8d, %r8d
	movq	%rax, %rdi
	movl	$6, %ecx
	movq	%rax, %r15
	call	_ZN2v88internal16DeclarationScopeC1EPNS0_4ZoneEPNS0_5ScopeENS0_9ScopeTypeENS0_12FunctionKindE@PLT
	movq	200(%r14), %rax
	movq	(%rax), %rax
	movl	(%rax), %eax
	movl	%eax, 112(%r15)
	jmp	.L1822
	.p2align 4,,10
	.p2align 3
.L1861:
	movq	%rbx, %r14
	movq	-208(%rbp), %rbx
	movq	200(%r14), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %edx
.L1858:
	movzbl	-176(%rbp), %r15d
	cmpb	%dl, %r15b
	jne	.L1865
	jmp	.L1864
	.p2align 4,,10
	.p2align 3
.L2010:
	movq	200(%r14), %rax
	movq	8(%rax), %rax
	cmpb	56(%rax), %r15b
	je	.L1864
.L1865:
	movq	%r14, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseStatementListItemEv
	testl	%eax, %eax
	jne	.L2010
	jmp	.L1864
	.p2align 4,,10
	.p2align 3
.L1996:
	movq	%rbx, %r14
	movq	-208(%rbp), %rbx
	jmp	.L1839
	.p2align 4,,10
	.p2align 3
.L1992:
	movq	-168(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-145(%rbp), %rdx
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseAsyncFunctionBodyEPNS0_5ScopeEPNS0_28PreParserScopedStatementListE
	movq	200(%r14), %rdi
	jmp	.L1869
	.p2align 4,,10
	.p2align 3
.L1995:
	xorl	%esi, %esi
	movq	%r8, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE8ValidateEi.part.0
	jmp	.L1875
	.p2align 4,,10
	.p2align 3
.L2007:
	movq	%rbx, %r14
	movq	-208(%rbp), %rbx
	jmp	.L1849
	.p2align 4,,10
	.p2align 3
.L1994:
	movq	144(%r14), %rax
.L1874:
	movzbl	16(%rax), %esi
	leal	-1(%rsi), %edx
	cmpb	$1, %dl
	jbe	.L2011
.L1871:
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1872
	movzbl	16(%rax), %esi
	leal	-1(%rsi), %edx
	cmpb	$1, %dl
	ja	.L1871
.L2011:
	movb	$1, 89(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L1874
	jmp	.L1872
	.p2align 4,,10
	.p2align 3
.L1977:
	movq	%rbx, %r14
	movq	-208(%rbp), %rbx
	jmp	.L1864
	.p2align 4,,10
	.p2align 3
.L1993:
	movq	%rbx, %r14
	movq	-208(%rbp), %rbx
	jmp	.L1858
	.p2align 4,,10
	.p2align 3
.L2008:
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$12, %eax
	cmpb	$1, %al
	movq	16(%r14), %rax
	ja	.L1980
	addl	$1, 20(%rax)
	movq	16(%r14), %rax
.L1980:
	movq	48(%rax), %rdi
	jmp	.L1834
.L1825:
	orb	$1, 76(%rax)
	jmp	.L1824
.L2009:
	movl	$224, %esi
	movq	%r15, %rdi
	movq	%rdx, -168(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-168(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L1827
.L2006:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23041:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseFunctionBodyEPNS0_28PreParserScopedStatementListENS0_19PreParserIdentifierEiRKNS0_25PreParserFormalParametersENS0_12FunctionKindENS0_18FunctionSyntaxKindENS3_16FunctionBodyTypeE, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseFunctionBodyEPNS0_28PreParserScopedStatementListENS0_19PreParserIdentifierEiRKNS0_25PreParserFormalParametersENS0_12FunctionKindENS0_18FunctionSyntaxKindENS3_16FunctionBodyTypeE
	.section	.rodata._ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseArrowFunctionLiteralERKNS0_25PreParserFormalParametersE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"arrow function"
.LC7:
	.string	"parse"
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseArrowFunctionLiteralERKNS0_25PreParserFormalParametersE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseArrowFunctionLiteralERKNS0_25PreParserFormalParametersE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseArrowFunctionLiteralERKNS0_25PreParserFormalParametersE
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseArrowFunctionLiteralERKNS0_25PreParserFormalParametersE, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseArrowFunctionLiteralERKNS0_25PreParserFormalParametersE:
.LFB25562:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -192(%rbp)
	movabsq	$803158884540, %rax
	movq	%rax, -72(%rbp)
	movzbl	112(%rdi), %eax
	movq	96(%rdi), %rdi
	movaps	%xmm0, -176(%rbp)
	movq	$0, -160(%rbp)
	movl	-72(%rbp,%rax,4), %edx
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testq	%rdi, %rdi
	je	.L2013
	testl	%eax, %eax
	jne	.L2044
.L2013:
	xorl	%r14d, %r14d
	cmpb	$0, _ZN2v88internal24FLAG_log_function_eventsE(%rip)
	jne	.L2045
.L2014:
	movq	200(%rbx), %rdi
	movq	8(%rdi), %rax
	cmpb	$0, 76(%rax)
	je	.L2015
	movq	128(%rbx), %rax
	movb	$1, (%rax)
	movb	$1, 2(%rax)
	movq	200(%rbx), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	je	.L2046
.L2016:
	movq	-192(%rbp), %rdi
	movl	$1, %eax
	testq	%rdi, %rdi
	jne	.L2047
.L2027:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2048
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2046:
	.cfi_restore_state
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L2016
	.p2align 4,,10
	.p2align 3
.L2015:
	movq	(%rbx), %rax
	addl	$1, 208(%rbx)
	leaq	-144(%rbp), %r15
	movq	(%r12), %rdx
	movzbl	133(%rdx), %r13d
	movq	%rax, -136(%rbp)
	leaq	16(%rbx), %rax
	movq	%rax, -112(%rbp)
	movq	16(%rbx), %rax
	movq	%rbx, -144(%rbp)
	movq	%rdx, (%rbx)
	movq	$0, -128(%rbp)
	movl	$0, -120(%rbp)
	movq	%rax, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movl	$0, -88(%rbp)
	movq	%r15, 16(%rbx)
	testq	%rax, %rax
	je	.L2019
	movzbl	57(%rax), %edx
	movb	$0, 57(%rax)
	movb	%dl, 58(%rax)
	movq	200(%rbx), %rdi
.L2019:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%rbx), %rdi
	movq	8(%rdi), %rax
	cmpb	$8, 56(%rax)
	je	.L2049
	subq	$8, %rsp
	leaq	-193(%rbp), %rsi
	movq	%r12, %r9
	xorl	%edx, %edx
	pushq	$0
	movl	$-1, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	pushq	$0
	pushq	%r13
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseFunctionBodyEPNS0_28PreParserScopedStatementListENS0_19PreParserIdentifierEiRKNS0_25PreParserFormalParametersENS0_12FunctionKindENS0_18FunctionSyntaxKindENS3_16FunctionBodyTypeE
	addq	$32, %rsp
.L2022:
	movq	200(%rbx), %rdx
	movq	(%r12), %rax
	movq	(%rdx), %rdx
	movl	4(%rdx), %edx
	movl	%edx, 116(%rax)
	movq	(%rbx), %rax
	testb	$1, 129(%rax)
	je	.L2024
	movq	200(%rbx), %rax
	movq	(%rax), %rcx
	movl	340(%rax), %esi
	movl	336(%rax), %edx
	cmpl	%esi, 4(%rcx)
	setge	%dil
	cmpl	%esi, %edx
	setbe	%cl
	testb	%cl, %dil
	je	.L2024
	movq	(%r12), %rcx
	cmpl	%edx, 112(%rcx)
	jle	.L2050
.L2024:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rax
	movq	%rdx, (%rax)
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rax
	movq	%rdx, (%rax)
	cmpb	$0, _ZN2v88internal24FLAG_log_function_eventsE(%rip)
	jne	.L2051
.L2026:
	movq	-192(%rbp), %rdi
	movl	$2, %eax
	testq	%rdi, %rdi
	je	.L2027
.L2047:
	leaq	-184(%rbp), %rsi
	movl	%eax, -212(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movl	-212(%rbp), %eax
	jmp	.L2027
	.p2align 4,,10
	.p2align 3
.L2049:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	subq	$8, %rsp
	movq	%r12, %r9
	xorl	%edx, %edx
	pushq	$1
	movzbl	256(%rbx), %eax
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	pushq	$0
	leaq	-193(%rbp), %rsi
	movl	$-1, %r8d
	pushq	%r13
	movb	$1, 256(%rbx)
	movb	%al, -212(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseFunctionBodyEPNS0_28PreParserScopedStatementListENS0_19PreParserIdentifierEiRKNS0_25PreParserFormalParametersENS0_12FunctionKindENS0_18FunctionSyntaxKindENS3_16FunctionBodyTypeE
	movzbl	-212(%rbp), %eax
	addq	$32, %rsp
	movb	%al, 256(%rbx)
	jmp	.L2022
	.p2align 4,,10
	.p2align 3
.L2045:
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%rax, %r14
	jmp	.L2014
	.p2align 4,,10
	.p2align 3
.L2051:
	movq	(%r12), %r12
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%r15, %rdi
	subq	%r14, %rax
	movq	%rax, -144(%rbp)
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	subq	$8, %rsp
	movl	116(%r12), %r8d
	movl	112(%r12), %ecx
	pushq	$14
	movl	212(%rbx), %edx
	leaq	.LC6(%rip), %r9
	leaq	.LC7(%rip), %rsi
	movq	104(%rbx), %rdi
	call	_ZN2v88internal6Logger13FunctionEventEPKcidiiS3_m@PLT
	popq	%rax
	popq	%rdx
	jmp	.L2026
	.p2align 4,,10
	.p2align 3
.L2050:
	movl	344(%rax), %r13d
	salq	$32, %rsi
	movq	%rbx, %rdi
	orq	%rdx, %rsi
	movl	%r13d, %edx
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	movq	200(%rbx), %rax
	movl	$4294967295, %ecx
	movq	%rcx, 336(%rax)
	movl	$0, 344(%rax)
	cmpl	$298, %r13d
	jne	.L2024
	movq	264(%rbx), %rax
	testq	%rax, %rax
	je	.L2024
	addl	$1, 128(%rax)
	jmp	.L2024
.L2044:
	leaq	-184(%rbp), %rsi
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2013
.L2048:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25562:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseArrowFunctionLiteralERKNS0_25PreParserFormalParametersE, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseArrowFunctionLiteralERKNS0_25PreParserFormalParametersE
	.section	.rodata._ZN2v88internal9PreParser20ParseFunctionLiteralENS0_19PreParserIdentifierENS0_7Scanner8LocationENS0_20FunctionNameValidityENS0_12FunctionKindEiNS0_18FunctionSyntaxKindENS0_12LanguageModeEPNS0_8ZoneListIPKNS0_12AstRawStringEEE.str1.1,"aMS",@progbits,1
.LC8:
	.string	""
.LC9:
	.string	"preparse-resolution"
	.section	.text._ZN2v88internal9PreParser20ParseFunctionLiteralENS0_19PreParserIdentifierENS0_7Scanner8LocationENS0_20FunctionNameValidityENS0_12FunctionKindEiNS0_18FunctionSyntaxKindENS0_12LanguageModeEPNS0_8ZoneListIPKNS0_12AstRawStringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9PreParser20ParseFunctionLiteralENS0_19PreParserIdentifierENS0_7Scanner8LocationENS0_20FunctionNameValidityENS0_12FunctionKindEiNS0_18FunctionSyntaxKindENS0_12LanguageModeEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	.type	_ZN2v88internal9PreParser20ParseFunctionLiteralENS0_19PreParserIdentifierENS0_7Scanner8LocationENS0_20FunctionNameValidityENS0_12FunctionKindEiNS0_18FunctionSyntaxKindENS0_12LanguageModeEPNS0_8ZoneListIPKNS0_12AstRawStringEEE, @function
_ZN2v88internal9PreParser20ParseFunctionLiteralENS0_19PreParserIdentifierENS0_7Scanner8LocationENS0_20FunctionNameValidityENS0_12FunctionKindEiNS0_18FunctionSyntaxKindENS0_12LanguageModeEPNS0_8ZoneListIPKNS0_12AstRawStringEEE:
.LFB20774:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%r9d, %r12d
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -56
	movl	24(%rbp), %eax
	movq	%rdx, -264(%rbp)
	movq	%rsi, -272(%rbp)
	movl	32(%rbp), %r13d
	movq	%rcx, -320(%rbp)
	movl	%r8d, -292(%rbp)
	movl	%eax, -280(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -176(%rbp)
	movabsq	$816043786429, %rax
	movq	%rax, -64(%rbp)
	movzbl	112(%rdi), %eax
	movq	96(%rdi), %rdi
	movaps	%xmm0, -160(%rbp)
	movq	$0, -144(%rbp)
	movl	-64(%rbp,%rax,4), %edx
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testq	%rdi, %rdi
	je	.L2053
	testl	%eax, %eax
	jne	.L2125
.L2053:
	cmpb	$0, _ZN2v88internal24FLAG_log_function_eventsE(%rip)
	movq	$0, -312(%rbp)
	jne	.L2126
.L2054:
	movq	136(%r15), %r14
	movzbl	%r12b, %eax
	movq	(%r15), %rdx
	movl	%eax, -276(%rbp)
	movq	16(%r14), %rbx
	movq	24(%r14), %rax
	subq	%rbx, %rax
	cmpq	$223, %rax
	jbe	.L2127
	leaq	224(%rbx), %rax
	movq	%rax, 16(%r14)
.L2056:
	movzbl	%r12b, %r8d
	movl	$2, %ecx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal16DeclarationScopeC1EPNS0_4ZoneEPNS0_5ScopeENS0_9ScopeTypeENS0_12FunctionKindE@PLT
	movq	16(%r15), %rax
	movb	$1, 59(%rax)
	leal	-8(%r12), %eax
	cmpb	$1, %al
	jbe	.L2057
	movq	40(%r15), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal16DeclarationScope31DeclareDefaultFunctionVariablesEPNS0_15AstValueFactoryE@PLT
.L2057:
	movzbl	129(%rbx), %eax
	andl	$1, %r13d
	movq	%r15, -256(%rbp)
	movq	$0, -248(%rbp)
	andl	$-2, %eax
	orl	%eax, %r13d
	movb	%r13b, 129(%rbx)
	movl	208(%r15), %eax
	addl	$1, %eax
	movl	%eax, 208(%r15)
	movl	%eax, -300(%rbp)
	movq	16(%r15), %rax
	movzbl	57(%rax), %r13d
	testb	%r13b, %r13b
	jne	.L2086
	cmpq	$0, 288(%r15)
	je	.L2058
	leaq	-256(%rbp), %rdi
	movq	%rbx, %rsi
	movl	$1, %r13d
	call	_ZN2v88internal19PreparseDataBuilder18DataGatheringScope5StartEPNS0_16DeclarationScopeE@PLT
	movq	16(%r15), %rax
.L2058:
	movq	(%r15), %rdx
	leaq	-128(%rbp), %rcx
	movq	%r15, -128(%rbp)
	movq	%rbx, (%r15)
	movq	%rdx, -120(%rbp)
	leaq	16(%r15), %rdx
	movq	$0, -112(%rbp)
	movl	$0, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	%rbx, -80(%rbp)
	movl	$0, -72(%rbp)
	movq	%rcx, -328(%rbp)
	movq	%rcx, 16(%r15)
	testq	%rax, %rax
	je	.L2059
	movzbl	57(%rax), %edx
	movb	$0, 57(%rax)
	movb	%dl, 58(%rax)
.L2059:
	movq	200(%r15), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$5, %al
	jne	.L2128
.L2060:
	movq	200(%r15), %rax
	movl	$256, %esi
	xorl	%edi, %edi
	xorl	%edx, %edx
	movl	$259, %r8d
	movq	(%rax), %rax
	movl	(%rax), %eax
	movq	%rbx, -240(%rbp)
	movw	%si, -232(%rbp)
	movl	%eax, 112(%rbx)
	movl	%eax, -296(%rbp)
	movq	144(%r15), %rax
	movq	$0, -228(%rbp)
	movw	%di, -220(%rbp)
	movq	%r15, -208(%rbp)
	movq	%rax, -200(%rbp)
	movw	%r8w, -192(%rbp)
	testq	%rax, %rax
	je	.L2061
	movzbl	18(%rax), %edx
.L2061:
	leaq	-208(%rbp), %rax
	leaq	-240(%rbp), %r14
	movq	%r15, %rdi
	movb	%dl, -190(%rbp)
	movq	%rax, 144(%r15)
	movq	%r14, %rsi
	movq	%rax, -288(%rbp)
	movl	$4294967295, %eax
	movq	%rax, -188(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE24ParseFormalParameterListEPNS0_25PreParserFormalParametersE
	movl	-188(%rbp), %eax
	cmpl	%eax, -184(%rbp)
	jb	.L2062
	movb	$1, -220(%rbp)
.L2062:
	movq	-208(%rbp), %rax
	movq	-200(%rbp), %rdx
	movq	%rdx, 144(%rax)
	movq	200(%r15), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$6, %al
	jne	.L2129
.L2063:
	movq	200(%r15), %rdi
	movl	-224(%rbp), %edx
	movq	(%rdi), %rax
	movl	4(%rax), %eax
	cmpb	$6, %r12b
	je	.L2130
	cmpb	$7, %r12b
	je	.L2131
.L2065:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$8, %al
	jne	.L2132
.L2068:
	cmpl	$-1, 16(%rbp)
	jne	.L2069
	movq	200(%r15), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
	movl	%eax, 16(%rbp)
.L2069:
	subq	$8, %rsp
	movq	%r14, %r9
	movl	16(%rbp), %r8d
	movq	%r15, %rdi
	movzbl	-280(%rbp), %eax
	movq	-264(%rbp), %r14
	pushq	$1
	movzbl	256(%r15), %r12d
	movb	$1, 256(%r15)
	pushq	%rax
	movl	-276(%rbp), %eax
	movq	%r14, %rcx
	movq	-272(%rbp), %rdx
	movq	-288(%rbp), %rsi
	pushq	%rax
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseFunctionBodyEPNS0_28PreParserScopedStatementListENS0_19PreParserIdentifierEiRKNS0_25PreParserFormalParametersENS0_12FunctionKindENS0_18FunctionSyntaxKindENS3_16FunctionBodyTypeE
	addq	$32, %rsp
	testb	$1, 129(%rbx)
	jne	.L2133
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal16DeclarationScope25HoistSloppyBlockFunctionsEPNS0_14AstNodeFactoryE@PLT
.L2073:
	testb	%r13b, %r13b
	jne	.L2134
.L2077:
	movq	-96(%rbp), %rax
	movq	-88(%rbp), %rdx
	movb	%r12b, 256(%r15)
	movq	%rdx, (%rax)
	movq	-128(%rbp), %rax
	movq	-120(%rbp), %rdx
	movq	%rdx, (%rax)
	cmpq	$0, -248(%rbp)
	je	.L2078
	leaq	-256(%rbp), %rdi
	call	_ZN2v88internal19PreparseDataBuilder18DataGatheringScope5CloseEv@PLT
.L2078:
	cmpb	$0, _ZN2v88internal24FLAG_log_function_eventsE(%rip)
	jne	.L2135
.L2079:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2136
.L2081:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2137
	leaq	-40(%rbp), %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2086:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L2058
	.p2align 4,,10
	.p2align 3
.L2133:
	cmpl	$1, -292(%rbp)
	je	.L2071
	testb	%r14b, %r14b
	je	.L2071
	movzbl	-264(%rbp), %edx
	subl	$2, %edx
	cmpb	$1, %dl
	jbe	.L2138
	movl	-292(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L2139
.L2071:
	movq	200(%r15), %rax
	movl	336(%rax), %edx
	movl	340(%rax), %esi
	cmpl	%edx, -296(%rbp)
	setle	%dil
	cmpl	%esi, %edx
	setbe	%cl
	testb	%cl, %dil
	je	.L2073
	movq	(%rax), %rcx
	cmpl	%esi, 4(%rcx)
	jl	.L2073
	movl	344(%rax), %r14d
	movq	%rsi, %rax
	movl	%edx, %esi
	movq	%r15, %rdi
	salq	$32, %rax
	orq	%rax, %rsi
	movl	%r14d, %edx
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	movq	200(%r15), %rax
	movl	$4294967295, %esi
	movq	%rsi, 336(%rax)
	movl	$0, 344(%rax)
	cmpl	$298, %r14d
	jne	.L2073
	movq	264(%r15), %rax
	testq	%rax, %rax
	je	.L2073
	addl	$1, 128(%rax)
	jmp	.L2073
	.p2align 4,,10
	.p2align 3
.L2131:
	movzbl	-232(%rbp), %r12d
	cmpl	$1, %edx
	jne	.L2140
	testb	%r12b, %r12b
	jne	.L2067
.L2124:
	movq	200(%r15), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$8, %al
	je	.L2068
.L2132:
	movq	%r15, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L2068
	.p2align 4,,10
	.p2align 3
.L2130:
	testl	%edx, %edx
	je	.L2065
	movl	-296(%rbp), %esi
	salq	$32, %rax
	movq	%r15, %rdi
	movl	$223, %edx
	orq	%rax, %rsi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	movq	200(%r15), %rdi
	jmp	.L2065
	.p2align 4,,10
	.p2align 3
.L2134:
	movl	-228(%rbp), %edx
	movl	208(%r15), %ecx
	leaq	-256(%rbp), %rdi
	movq	%rbx, %rsi
	subl	-300(%rbp), %ecx
	call	_ZN2v88internal19PreparseDataBuilder18DataGatheringScope20SetSkippableFunctionEPNS0_16DeclarationScopeEii@PLT
	jmp	.L2077
	.p2align 4,,10
	.p2align 3
.L2140:
	movq	%rax, %rdx
	movl	-296(%rbp), %esi
	movq	%r15, %rdi
	movl	%eax, -304(%rbp)
	salq	$32, %rdx
	orq	%rdx, %rsi
	movl	$224, %edx
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	movl	-304(%rbp), %eax
	testb	%r12b, %r12b
	je	.L2124
.L2067:
	movl	-296(%rbp), %esi
	salq	$32, %rax
	movl	$283, %edx
	movq	%r15, %rdi
	orq	%rax, %rsi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	jmp	.L2124
	.p2align 4,,10
	.p2align 3
.L2136:
	leaq	-168(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2081
	.p2align 4,,10
	.p2align 3
.L2135:
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	-328(%rbp), %rdi
	subq	-312(%rbp), %rax
	movq	%rax, -128(%rbp)
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	movq	-272(%rbp), %rsi
	xorl	%eax, %eax
	leaq	.LC8(%rip), %r9
	testq	%rsi, %rsi
	je	.L2080
	movq	8(%rsi), %r9
	movslq	16(%rsi), %rax
.L2080:
	subq	$8, %rsp
	movl	116(%rbx), %r8d
	movl	112(%rbx), %ecx
	leaq	.LC9(%rip), %rsi
	movl	212(%r15), %edx
	movq	104(%r15), %rdi
	pushq	%rax
	call	_ZN2v88internal6Logger13FunctionEventEPKcidiiS3_m@PLT
	popq	%rax
	popq	%rdx
	jmp	.L2079
	.p2align 4,,10
	.p2align 3
.L2128:
	movq	%r15, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L2060
	.p2align 4,,10
	.p2align 3
.L2129:
	movq	%r15, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L2063
	.p2align 4,,10
	.p2align 3
.L2127:
	movl	$224, %esi
	movq	%r14, %rdi
	movq	%rdx, -288(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-288(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L2056
	.p2align 4,,10
	.p2align 3
.L2126:
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%rax, -312(%rbp)
	jmp	.L2054
	.p2align 4,,10
	.p2align 3
.L2139:
	movq	-320(%rbp), %rsi
	movl	$315, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	jmp	.L2071
	.p2align 4,,10
	.p2align 3
.L2138:
	movq	-320(%rbp), %rsi
	movl	$295, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	jmp	.L2071
.L2125:
	leaq	-168(%rbp), %rsi
	movq	%rdi, -176(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2053
.L2137:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20774:
	.size	_ZN2v88internal9PreParser20ParseFunctionLiteralENS0_19PreParserIdentifierENS0_7Scanner8LocationENS0_20FunctionNameValidityENS0_12FunctionKindEiNS0_18FunctionSyntaxKindENS0_12LanguageModeEPNS0_8ZoneListIPKNS0_12AstRawStringEEE, .-_ZN2v88internal9PreParser20ParseFunctionLiteralENS0_19PreParserIdentifierENS0_7Scanner8LocationENS0_20FunctionNameValidityENS0_12FunctionKindEiNS0_18FunctionSyntaxKindENS0_12LanguageModeEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseHoistableDeclarationEiNS_4base5FlagsINS0_17ParseFunctionFlagEiEEPNS0_8ZoneListIPKNS0_12AstRawStringEEEb,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseHoistableDeclarationEiNS_4base5FlagsINS0_17ParseFunctionFlagEiEEPNS0_8ZoneListIPKNS0_12AstRawStringEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseHoistableDeclarationEiNS_4base5FlagsINS0_17ParseFunctionFlagEiEEPNS0_8ZoneListIPKNS0_12AstRawStringEEEb
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseHoistableDeclarationEiNS_4base5FlagsINS0_17ParseFunctionFlagEiEEPNS0_8ZoneListIPKNS0_12AstRawStringEEEb, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseHoistableDeclarationEiNS_4base5FlagsINS0_17ParseFunctionFlagEiEEPNS0_8ZoneListIPKNS0_12AstRawStringEEEb:
.LFB25500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movl	%esi, -68(%rbp)
	movl	%r8d, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	120(%rbx), %rax
	jnb	.L2142
	movq	200(%rbx), %rax
	movq	24(%rax), %rcx
	cmpb	$0, 48(%rcx)
	jne	.L2143
	movl	$-1, 32(%rax)
	movq	24(%rcx), %rsi
	movb	$1, 48(%rcx)
	movq	%rsi, 16(%rcx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L2143:
	movq	128(%rbx), %rax
	movl	$257, %esi
	movw	%si, (%rax)
.L2142:
	movq	200(%rbx), %rdi
	movl	%r14d, %esi
	andl	$2, %esi
	movq	8(%rdi), %rax
	movl	%esi, -80(%rbp)
	movzbl	56(%rax), %eax
	je	.L2144
	cmpb	$40, %al
	je	.L2192
.L2144:
	cmpb	$5, %al
	jne	.L2145
	cmpb	$0, -72(%rbp)
	movl	$1, %edx
	movl	$1, %r8d
	je	.L2193
.L2146:
	movq	(%rbx), %rax
	movl	-80(%rbp), %ecx
	xorl	%esi, %esi
	movl	%r13d, %r9d
	movzbl	129(%rax), %eax
	andl	$1, %eax
	testl	%ecx, %ecx
	leaq	_ZZN2v88internal10ParserBaseINS0_9PreParserEE19FunctionKindForImplEbNS_4base5FlagsINS0_17ParseFunctionFlagEiEEE14kFunctionKinds(%rip), %rcx
	setne	%sil
	andl	$1, %r9d
	xorl	%r11d, %r11d
	leaq	(%rcx,%r9,2), %r9
	movq	(%rdi), %rcx
	pushq	$0
	movb	%dl, %r11b
	pushq	%rax
	movl	-68(%rbp), %eax
	movq	%r11, %rdx
	movq	%rbx, %rdi
	pushq	$2
	movzbl	(%r9,%rsi), %r9d
	movq	%r15, %rsi
	pushq	%rax
	movq	(%rcx), %rcx
	call	_ZN2v88internal9PreParser20ParseFunctionLiteralENS0_19PreParserIdentifierENS0_7Scanner8LocationENS0_20FunctionNameValidityENS0_12FunctionKindEiNS0_18FunctionSyntaxKindENS0_12LanguageModeEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	movq	(%rbx), %r9
	addq	$32, %rsp
	testb	$1, 130(%r9)
	je	.L2156
	xorl	%r8d, %r8d
	cmpb	$3, 128(%r9)
	setne	%dl
	xorl	%r13d, %r13d
	leal	(%rdx,%rdx), %r14d
.L2157:
	movq	200(%rbx), %rax
	movq	%r9, %rdi
	movl	%r14d, %edx
	movq	%r12, %rsi
	leaq	-57(%rbp), %rcx
	movq	%r9, -80(%rbp)
	movq	(%rax), %rax
	movl	4(%rax), %eax
	movl	%eax, -72(%rbp)
	call	_ZN2v88internal5Scope19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPbNS0_12VariableKindE@PLT
	movq	-80(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L2194
	cmpq	(%rax), %r9
	je	.L2161
	movq	48(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L2195
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L2163:
	movl	-68(%rbp), %esi
	movq	$0, 16(%rax)
	movl	$64, 4(%rax)
	movl	%esi, (%rax)
	movq	%r9, 24(%rax)
	movq	%r15, 8(%rax)
	movq	(%r15), %rdx
	movq	96(%rdx), %rcx
	movq	%rax, (%rcx)
	addq	$16, %rax
	movq	%rax, 96(%rdx)
.L2161:
	cmpb	$3, %r13b
	je	.L2196
.L2164:
	movl	$2, %eax
.L2147:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L2197
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2156:
	.cfi_restore_state
	testb	$1, 129(%r9)
	jne	.L2173
	testl	%r13d, %r13d
	jne	.L2173
	movl	$3, %r8d
	xorl	%r14d, %r14d
	movl	$3, %r13d
	jmp	.L2157
	.p2align 4,,10
	.p2align 3
.L2145:
	subl	$97, %eax
	movl	%eax, %r14d
	movq	16(%rbx), %rax
	movq	48(%rax), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movq	200(%rbx), %rdi
	movzbl	133(%rax), %r12d
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$0, 113(%rbx)
	je	.L2148
	leal	-92(%rax), %edx
	cmpb	$3, %dl
	ja	.L2198
.L2149:
	movq	%rbx, %rdi
	call	_ZNK2v88internal9PreParser13GetIdentifierEv
	movq	%rax, %r12
.L2154:
	xorl	%r8d, %r8d
	cmpb	$4, %r14b
	movq	200(%rbx), %rdi
	movq	%r12, %r15
	seta	%r8b
	addl	%r8d, %r8d
	jmp	.L2146
	.p2align 4,,10
	.p2align 3
.L2194:
	movq	128(%rbx), %rax
	movb	$1, (%rax)
	movb	$1, 2(%rax)
	movq	200(%rbx), %rax
	movq	24(%rax), %rcx
	cmpb	$0, 48(%rcx)
	jne	.L2159
	movl	$-1, 32(%rax)
	movq	24(%rcx), %rsi
	movb	$1, 48(%rcx)
	movq	%rsi, 16(%rcx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L2159:
	cmpb	$2, %r14b
	je	.L2199
	leaq	32(%r9), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal11VariableMap6LookupEPKNS0_12AstRawStringE@PLT
	movq	%rax, %r15
.L2202:
	cmpb	$3, %r13b
	jne	.L2164
.L2196:
	movq	16(%rbx), %rax
	movq	48(%rbx), %rdi
	movl	$1099, %r13d
	movq	64(%rbx), %r14
	movl	24(%rax), %eax
	movq	16(%rdi), %r12
	testl	%eax, %eax
	movl	$1035, %eax
	cmovle	%eax, %r13d
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$31, %rax
	jbe	.L2200
	leaq	32(%r12), %rax
	movq	%rax, 16(%rdi)
.L2167:
	movl	-72(%rbp), %eax
	movq	%r14, %xmm1
	movq	%r15, %xmm0
	movq	$0, 24(%r12)
	movl	%r13d, 4(%r12)
	punpcklqdq	%xmm1, %xmm0
	movl	%eax, (%r12)
	movups	%xmm0, 8(%r12)
	movq	(%rbx), %rdi
	call	_ZN2v88internal5Scope19GetDeclarationScopeEv@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal16DeclarationScope26DeclareSloppyBlockFunctionEPNS0_28SloppyBlockFunctionStatementE@PLT
	jmp	.L2164
	.p2align 4,,10
	.p2align 3
.L2148:
	leal	-92(%rax), %edx
	leal	-9(%r12), %ecx
	cmpb	$3, %dl
	jbe	.L2149
	movq	(%rbx), %rdx
	movzbl	129(%rdx), %edx
	andl	$1, %edx
	cmpb	$96, %al
	je	.L2201
.L2151:
	cmpb	$97, %al
	jne	.L2152
	subl	$12, %r12d
	cmpb	$3, %r12b
	jbe	.L2150
.L2191:
	testb	%dl, %dl
	je	.L2149
.L2150:
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	movq	40(%rbx), %rax
	movl	$1, %edx
	movq	56(%rax), %rax
	movq	272(%rax), %r12
	jmp	.L2154
	.p2align 4,,10
	.p2align 3
.L2193:
	movl	$274, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	xorl	%eax, %eax
	jmp	.L2147
	.p2align 4,,10
	.p2align 3
.L2199:
	movq	%r9, %rdi
	call	_ZN2v88internal5Scope19GetDeclarationScopeEv@PLT
	movq	%r12, %rsi
	movq	%rax, %r9
	leaq	32(%r9), %rdi
	call	_ZN2v88internal11VariableMap6LookupEPKNS0_12AstRawStringE@PLT
	movq	%rax, %r15
	jmp	.L2202
	.p2align 4,,10
	.p2align 3
.L2192:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%rbx), %rdi
	orl	$1, %r14d
	movl	%r14d, %r13d
	movq	8(%rdi), %rax
	movzbl	56(%rax), %eax
	jmp	.L2144
	.p2align 4,,10
	.p2align 3
.L2173:
	xorl	%r8d, %r8d
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmp	.L2157
	.p2align 4,,10
	.p2align 3
.L2195:
	movl	$32, %esi
	movq	%r9, -80(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-80(%rbp), %r9
	jmp	.L2163
	.p2align 4,,10
	.p2align 3
.L2198:
	movq	(%rbx), %rdx
	movzbl	129(%rdx), %edx
	andl	$1, %edx
	cmpb	$96, %al
	jne	.L2151
	jmp	.L2150
	.p2align 4,,10
	.p2align 3
.L2152:
	subl	$97, %eax
	cmpb	$4, %al
	jbe	.L2191
	jmp	.L2150
	.p2align 4,,10
	.p2align 3
.L2200:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L2167
.L2201:
	cmpb	$4, %cl
	ja	.L2149
	jmp	.L2150
.L2197:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25500:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseHoistableDeclarationEiNS_4base5FlagsINS0_17ParseFunctionFlagEiEEPNS0_8ZoneListIPKNS0_12AstRawStringEEEb, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseHoistableDeclarationEiNS_4base5FlagsINS0_17ParseFunctionFlagEiEEPNS0_8ZoneListIPKNS0_12AstRawStringEEEb
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE24ParseFunctionDeclarationEv,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE24ParseFunctionDeclarationEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE24ParseFunctionDeclarationEv
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE24ParseFunctionDeclarationEv, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE24ParseFunctionDeclarationEv:
.LFB25899:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	200(%rdi), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rdi
	movq	(%rdi), %rax
	movl	(%rax), %esi
	movq	8(%rdi), %rax
	cmpb	$40, 56(%rax)
	je	.L2204
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	popq	%r12
	xorl	%edx, %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseHoistableDeclarationEiNS_4base5FlagsINS0_17ParseFunctionFlagEiEEPNS0_8ZoneListIPKNS0_12AstRawStringEEEb
	.p2align 4,,10
	.p2align 3
.L2204:
	.cfi_restore_state
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rax
	xorl	%r8d, %r8d
	movq	128(%r12), %rdi
	movl	$237, %ecx
	movq	(%rax), %rax
	movl	4(%rax), %edx
	movl	(%rax), %esi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r12), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L2205
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L2205:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25899:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE24ParseFunctionDeclarationEv, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE24ParseFunctionDeclarationEv
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseScopedStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE.part.0,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE16ParseIfStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE,comdat
	.align 2
	.p2align 4
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseScopedStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE.part.0, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseScopedStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE.part.0:
.LFB26960:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	136(%rdi), %r13
	movq	16(%r13), %r12
	movq	24(%r13), %rax
	subq	%r12, %rax
	cmpq	$135, %rax
	jbe	.L2214
	leaq	136(%r12), %rax
	movq	%rax, 16(%r13)
.L2209:
	movq	(%rbx), %rdx
	movq	%r13, %rsi
	movl	$6, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal5ScopeC1EPNS0_4ZoneEPS1_NS0_9ScopeTypeE@PLT
	movq	200(%rbx), %rax
	movq	(%rbx), %r13
	movq	%r12, (%rbx)
	movq	(%rax), %rax
	movl	(%rax), %eax
	movl	%eax, 112(%r12)
	movq	200(%rbx), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%rbx), %rdi
	movq	(%rdi), %rax
	movl	(%rax), %esi
	movq	8(%rdi), %rax
	cmpb	$40, 56(%rax)
	je	.L2210
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseHoistableDeclarationEiNS_4base5FlagsINS0_17ParseFunctionFlagEiEEPNS0_8ZoneListIPKNS0_12AstRawStringEEEb
	movq	200(%rbx), %rax
.L2211:
	movq	(%rax), %rax
	movq	(%rbx), %rdx
	movl	4(%rax), %eax
	movl	%eax, 116(%rdx)
	movq	(%rbx), %rdi
	call	_ZN2v88internal5Scope18FinalizeBlockScopeEv@PLT
	movq	%r13, (%rbx)
	addq	$8, %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2210:
	.cfi_restore_state
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%rbx), %rax
	xorl	%r8d, %r8d
	movq	128(%rbx), %rdi
	movl	$237, %ecx
	movq	(%rax), %rax
	movl	4(%rax), %edx
	movl	(%rax), %esi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%rbx), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L2211
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	movq	200(%rbx), %rax
	jmp	.L2211
	.p2align 4,,10
	.p2align 3
.L2214:
	movl	$136, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L2209
	.cfi_endproc
.LFE26960:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseScopedStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE.part.0, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseScopedStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE.part.0
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseAsyncFunctionLiteralEv,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseAsyncFunctionLiteralEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseAsyncFunctionLiteralEv
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseAsyncFunctionLiteralEv, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseAsyncFunctionLiteralEv:
.LFB26412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	200(%rdi), %rdi
	movq	(%rdi), %rax
	movl	(%rax), %ebx
	movl	4(%rax), %edx
	subl	%ebx, %edx
	cmpb	$90, 56(%rax)
	jne	.L2216
	subl	$2, %edx
.L2216:
	cmpb	$0, 28(%rax)
	movl	24(%rax), %ecx
	jne	.L2217
	sarl	%ecx
.L2217:
	cmpl	%ecx, %edx
	jne	.L2246
.L2218:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	xorl	%r13d, %r13d
	xorl	%esi, %esi
	movl	$-2, %ecx
	movq	200(%r12), %rdi
	movl	$10, %r14d
	movq	8(%rdi), %rax
	movzbl	56(%rax), %edx
	cmpb	$40, %dl
	je	.L2247
.L2219:
	leal	-97(%rdx), %r15d
	subl	$92, %edx
	xorl	%eax, %eax
	cmpb	$9, %dl
	jbe	.L2248
.L2220:
	movq	(%r12), %rdx
	movq	(%rdi), %rcx
	xorl	%r8d, %r8d
	movl	%r14d, %r9d
	movq	%r12, %rdi
	movzbl	129(%rdx), %edx
	pushq	$0
	andl	$1, %edx
	cmpb	$4, %r15b
	pushq	%rdx
	seta	%r8b
	movq	%r13, %rdx
	pushq	%rax
	addl	%r8d, %r8d
	pushq	%rbx
	movq	(%rcx), %rcx
	call	_ZN2v88internal9PreParser20ParseFunctionLiteralENS0_19PreParserIdentifierENS0_7Scanner8LocationENS0_20FunctionNameValidityENS0_12FunctionKindEiNS0_18FunctionSyntaxKindENS0_12LanguageModeEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	addq	$32, %rsp
	movl	$1, %edx
	testb	$7, %al
	cmove	%edx, %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2248:
	.cfi_restore_state
	movb	%cl, -56(%rbp)
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movzbl	-56(%rbp), %ecx
	leal	-92(%rax), %edx
	cmpb	$3, %dl
	ja	.L2249
.L2222:
	movq	%r12, %rdi
	call	_ZNK2v88internal9PreParser13GetIdentifierEv
	movq	%rax, %rsi
.L2227:
	movq	200(%r12), %rdi
	movb	%dl, %r13b
	movl	$1, %eax
	jmp	.L2220
	.p2align 4,,10
	.p2align 3
.L2247:
	movq	%rsi, -56(%rbp)
	movl	$13, %r14d
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	-56(%rbp), %rsi
	movl	$1, %ecx
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %edx
	jmp	.L2219
	.p2align 4,,10
	.p2align 3
.L2246:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	movq	200(%r12), %rdi
	movq	(%rdi), %rax
	movl	(%rax), %ebx
	jmp	.L2218
	.p2align 4,,10
	.p2align 3
.L2249:
	movq	(%r12), %rdx
	movzbl	129(%rdx), %edx
	andl	$1, %edx
	cmpb	$96, %al
	je	.L2223
	cmpb	$97, %al
	jne	.L2225
	cmpb	$3, %cl
	jbe	.L2223
.L2245:
	testb	%dl, %dl
	je	.L2222
.L2223:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	movq	40(%r12), %rax
	movl	$1, %edx
	movq	56(%rax), %rax
	movq	272(%rax), %rsi
	jmp	.L2227
	.p2align 4,,10
	.p2align 3
.L2225:
	subl	$97, %eax
	cmpb	$4, %al
	jbe	.L2245
	jmp	.L2223
	.cfi_endproc
.LFE26412:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseAsyncFunctionLiteralEv, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseAsyncFunctionLiteralEv
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseFunctionExpressionEv,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseFunctionExpressionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseFunctionExpressionEv
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseFunctionExpressionEv, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseFunctionExpressionEv:
.LFB26415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$24, %rsp
	movq	200(%rdi), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rdi
	movl	$-12, %r8d
	movq	(%rdi), %rax
	movl	(%rax), %r14d
	movq	8(%rdi), %rax
	movzbl	56(%rax), %edx
	cmpb	$40, %dl
	je	.L2278
.L2251:
	leal	-97(%rdx), %r15d
	subl	$92, %edx
	xorl	%esi, %esi
	xorl	%r13d, %r13d
	movl	$4294967295, %ecx
	xorl	%eax, %eax
	cmpb	$9, %dl
	jbe	.L2279
.L2252:
	movq	(%r12), %rdx
	xorl	%r8d, %r8d
	movl	%ebx, %r9d
	movq	%r12, %rdi
	movzbl	129(%rdx), %edx
	pushq	$0
	andl	$1, %edx
	cmpb	$4, %r15b
	pushq	%rdx
	seta	%r8b
	movq	%r13, %rdx
	pushq	%rax
	addl	%r8d, %r8d
	pushq	%r14
	call	_ZN2v88internal9PreParser20ParseFunctionLiteralENS0_19PreParserIdentifierENS0_7Scanner8LocationENS0_20FunctionNameValidityENS0_12FunctionKindEiNS0_18FunctionSyntaxKindENS0_12LanguageModeEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	addq	$32, %rsp
	movl	$1, %edx
	testb	$7, %al
	cmove	%edx, %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2279:
	.cfi_restore_state
	movb	%r8b, -49(%rbp)
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$0, 113(%r12)
	movzbl	-49(%rbp), %r8d
	leal	-92(%rax), %edx
	jne	.L2280
	cmpb	$3, %dl
	ja	.L2281
.L2254:
	movq	%r12, %rdi
	call	_ZNK2v88internal9PreParser13GetIdentifierEv
	movq	%rax, %rsi
.L2259:
	movq	200(%r12), %rax
	movb	%dl, %r13b
	movq	(%rax), %rax
	movq	(%rax), %rcx
	movl	$1, %eax
	jmp	.L2252
	.p2align 4,,10
	.p2align 3
.L2280:
	cmpb	$3, %dl
	jbe	.L2254
	movq	(%r12), %rdx
	movzbl	129(%rdx), %edx
	andl	$1, %edx
	cmpb	$96, %al
	je	.L2255
.L2256:
	cmpb	$97, %al
	jne	.L2257
	cmpb	$3, %r8b
	jbe	.L2255
.L2277:
	testb	%dl, %dl
	je	.L2254
.L2255:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	movq	40(%r12), %rax
	movl	$1, %edx
	movq	56(%rax), %rax
	movq	272(%rax), %rsi
	jmp	.L2259
	.p2align 4,,10
	.p2align 3
.L2278:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movl	$14, %ebx
	movq	200(%r12), %rdi
	movl	$2, %r8d
	movq	8(%rdi), %rax
	movzbl	56(%rax), %edx
	jmp	.L2251
	.p2align 4,,10
	.p2align 3
.L2281:
	movq	(%r12), %rdx
	movzbl	129(%rdx), %edx
	andl	$1, %edx
	cmpb	$96, %al
	jne	.L2256
	jmp	.L2254
	.p2align 4,,10
	.p2align 3
.L2257:
	subl	$97, %eax
	cmpb	$4, %al
	jbe	.L2277
	jmp	.L2255
	.cfi_endproc
.LFE26415:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseFunctionExpressionEv, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseFunctionExpressionEv
	.section	.rodata._ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseObjectPropertyDefinitionEPNS3_17ParsePropertyInfoEPb.str1.1,"aMS",@progbits,1
.LC10:
	.string	"__proto__"
.LC11:
	.string	"unreachable code"
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseObjectPropertyDefinitionEPNS3_17ParsePropertyInfoEPb,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseObjectPropertyDefinitionEPNS3_17ParsePropertyInfoEPb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseObjectPropertyDefinitionEPNS3_17ParsePropertyInfoEPb
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseObjectPropertyDefinitionEPNS3_17ParsePropertyInfoEPb, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseObjectPropertyDefinitionEPNS3_17ParsePropertyInfoEPb:
.LFB25915:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	200(%rdi), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %r14d
	movq	(%rax), %rax
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE
	movzbl	32(%rbx), %r11d
	movq	16(%rbx), %r15
	movq	8(%rbx), %r10
	movzbl	16(%rbx), %r9d
	cmpb	$9, %r11b
	ja	.L2283
	leaq	.L2285(%rip), %rcx
	movzbl	%r11b, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseObjectPropertyDefinitionEPNS3_17ParsePropertyInfoEPb,"aG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseObjectPropertyDefinitionEPNS3_17ParsePropertyInfoEPb,comdat
	.align 4
	.align 4
.L2285:
	.long	.L2290-.L2285
	.long	.L2290-.L2285
	.long	.L2289-.L2285
	.long	.L2287-.L2285
	.long	.L2287-.L2285
	.long	.L2288-.L2285
	.long	.L2284-.L2285
	.long	.L2287-.L2285
	.long	.L2286-.L2285
	.long	.L2284-.L2285
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseObjectPropertyDefinitionEPNS3_17ParsePropertyInfoEPb,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseObjectPropertyDefinitionEPNS3_17ParsePropertyInfoEPb,comdat
	.p2align 4,,10
	.p2align 3
.L2287:
	movq	16(%r12), %rax
	movzbl	113(%r12), %ebx
	movq	48(%rax), %rdi
	testb	%bl, %bl
	je	.L2335
.L2294:
	movb	%r9b, -169(%rbp)
	movq	%r10, -168(%rbp)
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	leal	-92(%r14), %edx
	movq	-168(%rbp), %r10
	movzbl	-169(%rbp), %r9d
	cmpb	$3, %dl
	ja	.L2336
.L2295:
	movq	144(%r12), %rdi
	movl	-152(%rbp), %edx
.L2300:
	movq	%r10, %rsi
	movb	%r9b, -168(%rbp)
	call	_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE11NewVariableEPKNS0_12AstRawStringEi
	movq	(%r12), %rax
	testb	$1, 129(%rax)
	je	.L2307
	movzbl	-168(%rbp), %r9d
	subl	$2, %r9d
	cmpb	$1, %r9b
	jbe	.L2337
.L2307:
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	cmpb	$17, 56(%rax)
	je	.L2338
.L2308:
	movl	$2, %eax
	.p2align 4,,10
	.p2align 3
.L2291:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2339
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2336:
	.cfi_restore_state
	movzbl	133(%rax), %edx
	movq	(%r12), %rax
	movzbl	129(%rax), %eax
	andl	$1, %eax
	cmpb	$96, %r14b
	je	.L2340
	cmpb	$97, %r14b
	jne	.L2299
	subl	$12, %edx
	cmpb	$3, %dl
	jbe	.L2284
.L2333:
	testb	%al, %al
	je	.L2295
	.p2align 4,,10
	.p2align 3
.L2284:
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	xorl	%eax, %eax
	jmp	.L2291
	.p2align 4,,10
	.p2align 3
.L2290:
	movq	144(%r12), %rax
	movq	200(%r12), %rdx
	movl	-152(%rbp), %esi
	movzbl	16(%rax), %ebx
	movq	(%rdx), %rcx
	leal	-3(%rbx), %edx
	movl	4(%rcx), %r13d
	cmpb	$2, %dl
	jbe	.L2341
	movl	68(%rax), %ebx
	cmpl	%ebx, 64(%rax)
	jbe	.L2315
	movl	$252, 52(%rax)
	movl	%esi, 64(%rax)
	movl	%r13d, 68(%rax)
	movq	200(%r12), %rax
	movq	(%rax), %rcx
.L2315:
	movq	(%r12), %rax
	xorl	%edx, %edx
	testb	%r11b, %r11b
	movb	%r9b, %r15b
	setne	%dl
	movzbl	129(%rax), %eax
	pushq	$0
	leal	6(%rdx), %r9d
	andl	$1, %eax
	pushq	%rax
	pushq	$3
	pushq	%rsi
.L2334:
	movq	(%rcx), %rcx
	movl	$1, %r8d
	movq	%r10, %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal9PreParser20ParseFunctionLiteralENS0_19PreParserIdentifierENS0_7Scanner8LocationENS0_20FunctionNameValidityENS0_12FunctionKindEiNS0_18FunctionSyntaxKindENS0_12LanguageModeEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	movl	$2, %eax
	addq	$32, %rsp
	jmp	.L2291
	.p2align 4,,10
	.p2align 3
.L2286:
	movb	$1, 33(%rbx)
	movl	$2, %eax
	movb	$1, 36(%rbx)
	jmp	.L2291
	.p2align 4,,10
	.p2align 3
.L2289:
	cmpb	$0, 33(%rbx)
	movq	200(%r12), %r8
	jne	.L2292
	movq	(%r8), %rax
	cmpb	$0, 28(%rax)
	je	.L2292
	cmpl	$9, 24(%rax)
	je	.L2342
	.p2align 4,,10
	.p2align 3
.L2292:
	movq	%r8, %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	%r12, %rdi
	movzbl	256(%r12), %r13d
	movb	$1, 256(%r12)
	movq	(%rbx), %rsi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE36ParsePossibleDestructuringSubPatternEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEE
	movb	%r13b, 256(%r12)
	movl	$2, %eax
	jmp	.L2291
	.p2align 4,,10
	.p2align 3
.L2288:
	movq	144(%r12), %rax
	movq	200(%r12), %rdx
	movl	28(%rbx), %ebx
	movl	-152(%rbp), %esi
	movzbl	16(%rax), %edi
	movq	(%rdx), %rcx
	leal	-3(%rdi), %edx
	movl	4(%rcx), %r11d
	cmpb	$2, %dl
	jbe	.L2343
	movl	68(%rax), %edi
	cmpl	%edi, 64(%rax)
	jbe	.L2312
	movl	$252, 52(%rax)
	movl	%esi, 64(%rax)
	movl	%r11d, 68(%rax)
	movq	200(%r12), %rax
	movq	(%rax), %rcx
.L2312:
	movq	(%r12), %rax
	leaq	_ZZN2v88internal10ParserBaseINS0_9PreParserEE19FunctionKindForImplEbNS_4base5FlagsINS0_17ParseFunctionFlagEiEEE14kFunctionKinds(%rip), %rdi
	movb	%r9b, %r15b
	movzbl	129(%rax), %edx
	movl	%ebx, %eax
	andl	$1, %ebx
	pushq	$0
	shrl	%eax
	leaq	(%rdi,%rbx,2), %rdi
	andl	$1, %edx
	andl	$1, %eax
	pushq	%rdx
	movzbl	4(%rax,%rdi), %r9d
	pushq	$3
	pushq	%rsi
	jmp	.L2334
	.p2align 4,,10
	.p2align 3
.L2343:
	movq	(%rax), %r13
	xorl	%r8d, %r8d
	movl	%r11d, %edx
	movl	$252, %ecx
	movb	%r9b, -169(%rbp)
	movq	128(%r13), %rdi
	movq	%r10, -168(%rbp)
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r13), %rax
	movq	-168(%rbp), %r10
	movzbl	-169(%rbp), %r9d
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L2311
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L2311:
	movq	200(%r12), %rax
	movl	-152(%rbp), %esi
	movq	(%rax), %rcx
	jmp	.L2312
	.p2align 4,,10
	.p2align 3
.L2341:
	movq	(%rax), %rbx
	xorl	%r8d, %r8d
	movl	%r13d, %edx
	movl	$252, %ecx
	movb	%r9b, -170(%rbp)
	movq	128(%rbx), %rdi
	movb	%r11b, -169(%rbp)
	movq	%r10, -168(%rbp)
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%rbx), %rax
	movq	-168(%rbp), %r10
	movzbl	-169(%rbp), %r11d
	movzbl	-170(%rbp), %r9d
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L2314
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L2314:
	movq	200(%r12), %rax
	movl	-152(%rbp), %esi
	movq	(%rax), %rcx
	jmp	.L2315
	.p2align 4,,10
	.p2align 3
.L2335:
	movb	%r9b, -169(%rbp)
	movq	%r10, -168(%rbp)
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	-169(%rbp), %r9d
	movq	-168(%rbp), %r10
	movzbl	133(%rax), %eax
	subl	$9, %eax
	cmpb	$4, %al
	movq	16(%r12), %rax
	setbe	%bl
	movq	48(%rax), %rdi
	jmp	.L2294
	.p2align 4,,10
	.p2align 3
.L2342:
	movq	8(%rax), %rdi
	movl	$9, %ecx
	leaq	.LC10(%rip), %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L2292
	cmpb	$0, 0(%r13)
	je	.L2293
	movq	144(%r12), %rdx
	cmpb	$2, 16(%rdx)
	ja	.L2293
	movl	60(%rdx), %ecx
	cmpl	%ecx, 56(%rdx)
	jbe	.L2293
	movl	$233, 48(%rdx)
	movq	(%rax), %rax
	movq	%rax, 56(%rdx)
.L2293:
	movb	$1, 0(%r13)
	movq	200(%r12), %r8
	jmp	.L2292
	.p2align 4,,10
	.p2align 3
.L2338:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	leaq	-144(%rbp), %r13
	movq	%r12, %rsi
	movzbl	256(%r12), %ebx
	movb	$1, 256(%r12)
	movq	%r13, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPS3_NS0_15ExpressionScopeIS4_E9ScopeTypeE.constprop.0
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseAssignmentExpressionCoverGrammarEv
	movl	-84(%rbp), %eax
	cmpl	%eax, -88(%rbp)
	jbe	.L2344
.L2309:
	movq	-112(%rbp), %rsi
	movq	-120(%rbp), %rdi
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE6resizeEm
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	%rdx, 144(%rax)
	movq	144(%r12), %rax
	movb	%bl, 256(%r12)
	cmpb	$2, 16(%rax)
	ja	.L2308
	movl	60(%rax), %esi
	cmpl	%esi, 56(%rax)
	jbe	.L2308
	movq	200(%r12), %rdx
	movl	-152(%rbp), %ecx
	movq	(%rdx), %rdx
	movl	4(%rdx), %edx
	movl	$251, 48(%rax)
	movl	%ecx, 56(%rax)
	movl	%edx, 60(%rax)
	jmp	.L2308
	.p2align 4,,10
	.p2align 3
.L2337:
	movq	144(%r12), %rdi
	leaq	-152(%rbp), %rsi
	movl	$295, %edx
	call	_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE18RecordPatternErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE
	jmp	.L2307
	.p2align 4,,10
	.p2align 3
.L2344:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE8ValidateEi.part.0
	jmp	.L2309
.L2299:
	subl	$97, %r14d
	cmpb	$4, %r14b
	jbe	.L2333
	jmp	.L2284
.L2340:
	testb	%bl, %bl
	jne	.L2284
	movq	144(%r12), %rdi
	movl	-152(%rbp), %edx
	cmpb	$2, 16(%rdi)
	ja	.L2300
	movq	-152(%rbp), %rcx
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L2306:
	cmpb	$0, 72(%rdi)
	je	.L2304
	cmpb	$2, 16(%rax)
	je	.L2345
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L2306
.L2304:
	movq	144(%r12), %rdi
	jmp	.L2300
	.p2align 4,,10
	.p2align 3
.L2345:
	movq	%rcx, 76(%rax)
	movl	$303, 84(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L2306
	jmp	.L2304
.L2283:
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2339:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25915:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseObjectPropertyDefinitionEPNS3_17ParsePropertyInfoEPb, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseObjectPropertyDefinitionEPNS3_17ParsePropertyInfoEPb
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE18ParseObjectLiteralEv,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE18ParseObjectLiteralEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE18ParseObjectLiteralEv
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE18ParseObjectLiteralEv, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE18ParseObjectLiteralEv:
.LFB25556:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	200(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movl	(%rax), %eax
	movb	$0, -145(%rbp)
	movl	%eax, -172(%rbp)
	call	_ZN2v88internal7Scanner4NextEv@PLT
	leaq	-96(%rbp), %rax
	movq	144(%r13), %rsi
	movq	%rax, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEEC1EPNS0_15ExpressionScopeIS4_EE
	movq	200(%r13), %rdi
	movq	8(%rdi), %rax
	cmpb	$13, 56(%rax)
	je	.L2347
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	leaq	-145(%rbp), %r15
	leaq	-144(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L2358:
	movq	-168(%rbp), %rax
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	$0, -136(%rbp)
	movq	%rax, -144(%rbp)
	movb	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movl	$9, -112(%rbp)
	movb	$0, -108(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseObjectPropertyDefinitionEPNS3_17ParsePropertyInfoEPb
	testb	$7, %al
	je	.L2371
	movzbl	-108(%rbp), %eax
	movq	200(%r13), %rdi
	testb	%al, %al
	cmovne	%eax, %r12d
	movq	8(%rdi), %rax
	addl	$1, %ebx
	cmpb	$13, 56(%rax)
	je	.L2355
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$30, %al
	jne	.L2356
.L2370:
	movq	200(%r13), %rdi
.L2355:
	movq	8(%rdi), %rax
	cmpb	$13, 56(%rax)
	jne	.L2358
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpl	$65534, %ebx
	jle	.L2360
	testb	%r12b, %r12b
	jne	.L2372
.L2360:
	movl	$6, %r12d
.L2353:
	movq	-168(%rbp), %rdi
	call	_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEED1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2373
	addq	$136, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2371:
	.cfi_restore_state
	movl	$1, %r12d
	jmp	.L2353
	.p2align 4,,10
	.p2align 3
.L2356:
	movq	%r13, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L2370
	.p2align 4,,10
	.p2align 3
.L2372:
	movq	200(%r13), %rdx
	movq	144(%r13), %rax
	movq	(%rdx), %rdx
	movl	(%rdx), %ecx
	movzbl	16(%rax), %edx
	subl	$3, %edx
	cmpb	$2, %dl
	jbe	.L2374
	movl	68(%rax), %edx
	cmpl	%edx, 64(%rax)
	jbe	.L2360
	movl	-172(%rbp), %edx
	movl	$305, 52(%rax)
	movl	%ecx, 68(%rax)
	movl	%edx, 64(%rax)
	jmp	.L2360
	.p2align 4,,10
	.p2align 3
.L2347:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	jmp	.L2360
.L2374:
	movl	-172(%rbp), %esi
	salq	$32, %rcx
	movq	(%rax), %rdi
	movl	$305, %edx
	orq	%rcx, %rsi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	jmp	.L2360
.L2373:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25556:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE18ParseObjectLiteralEv, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE18ParseObjectLiteralEv
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseBindingPatternEv,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseBindingPatternEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseBindingPatternEv
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseBindingPatternEv, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseBindingPatternEv:
.LFB25163:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	200(%rdi), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %ebx
	leal	-92(%rbx), %edx
	cmpb	$9, %dl
	ja	.L2376
	movl	(%rax), %r13d
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movl	%eax, %ebx
	leal	-92(%rbx), %eax
	cmpb	$3, %al
	ja	.L2377
	movq	%r12, %rdi
	call	_ZNK2v88internal9PreParser13GetIdentifierEv
	movq	%rax, %rsi
	movzbl	%dl, %ebx
	cmpb	$3, %dl
	je	.L2421
.L2380:
	movq	(%r12), %rax
	testb	$1, 129(%rax)
	je	.L2393
	leal	-2(%rbx), %eax
	cmpb	$1, %al
	jbe	.L2394
.L2393:
	movl	%ebx, %eax
	sall	$4, %eax
	orl	$3, %eax
	movl	%eax, %ebx
.L2387:
	movq	144(%r12), %rdi
	movl	%r13d, %edx
	call	_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE11NewVariableEPKNS0_12AstRawStringEi
	movl	%ebx, %eax
.L2417:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2376:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	120(%r12), %rax
	jnb	.L2396
	movq	200(%r12), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L2397
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L2397:
	movq	128(%r12), %rax
	movl	$257, %edx
	movw	%dx, (%rax)
.L2396:
	cmpb	$3, %bl
	je	.L2422
	cmpb	$8, %bl
	je	.L2423
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2422:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseArrayLiteralEv
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2377:
	.cfi_restore_state
	movq	16(%r12), %rax
	movzbl	113(%r12), %r14d
	movq	48(%rax), %rdi
	testb	%r14b, %r14b
	je	.L2424
.L2381:
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movq	(%r12), %rdx
	testb	$1, 129(%rdx)
	jne	.L2425
	cmpb	$96, %bl
	je	.L2383
	movzbl	133(%rax), %eax
	cmpb	$97, %bl
	jne	.L2385
	subl	$12, %eax
	cmpb	$3, %al
	jbe	.L2384
.L2386:
	movq	200(%r12), %rax
	movq	144(%r12), %rdi
	movl	$315, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE30RecordStrictModeParameterErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE
.L2420:
	movq	%r12, %rdi
	call	_ZNK2v88internal9PreParser13GetIdentifierEv
	movq	%rax, %rsi
	movzbl	%dl, %ebx
	jmp	.L2380
	.p2align 4,,10
	.p2align 3
.L2423:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE18ParseObjectLiteralEv
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2425:
	.cfi_restore_state
	cmpb	$96, %bl
	je	.L2383
.L2384:
	movq	%r12, %rdi
	movl	$19, %ebx
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	movq	40(%r12), %rax
	movq	56(%rax), %rax
	movq	272(%rax), %rsi
	jmp	.L2387
	.p2align 4,,10
	.p2align 3
.L2424:
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$9, %eax
	cmpb	$4, %al
	movq	16(%r12), %rax
	setbe	%r14b
	movq	48(%rax), %rdi
	jmp	.L2381
	.p2align 4,,10
	.p2align 3
.L2421:
	movq	(%r12), %rdi
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal5Scope18ShouldBanArgumentsEv@PLT
	movq	-40(%rbp), %rsi
	testb	%al, %al
	je	.L2380
	movl	$12, %esi
	movq	%r12, %rdi
	movl	$1, %ebx
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	movq	40(%r12), %rax
	movq	56(%rax), %rax
	movq	272(%rax), %rsi
	jmp	.L2380
	.p2align 4,,10
	.p2align 3
.L2383:
	testb	%r14b, %r14b
	jne	.L2384
	movq	144(%r12), %rdx
	cmpb	$2, 16(%rdx)
	ja	.L2420
	movq	200(%r12), %rax
	movq	(%rax), %rsi
	movq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L2392:
	cmpb	$0, 72(%rdx)
	je	.L2420
	cmpb	$2, 16(%rax)
	je	.L2426
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L2392
	jmp	.L2420
	.p2align 4,,10
	.p2align 3
.L2426:
	movq	(%rsi), %rcx
	movl	$303, 84(%rax)
	movq	%rcx, 76(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L2392
	jmp	.L2420
	.p2align 4,,10
	.p2align 3
.L2385:
	subl	$97, %ebx
	cmpb	$4, %bl
	jbe	.L2386
	jmp	.L2384
	.p2align 4,,10
	.p2align 3
.L2394:
	movq	200(%r12), %rax
	movl	$295, %edx
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	movl	$1, %eax
	jmp	.L2417
	.cfi_endproc
.LFE25163:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseBindingPatternEv, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseBindingPatternEv
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE24ParseFormalParameterListEPNS0_25PreParserFormalParametersE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE24ParseFormalParameterListEPNS0_25PreParserFormalParametersE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE24ParseFormalParameterListEPNS0_25PreParserFormalParametersE
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE24ParseFormalParameterListEPNS0_25PreParserFormalParametersE, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE24ParseFormalParameterListEPNS0_25PreParserFormalParametersE:
.LFB23019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	224(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, 224(%rdi)
	movq	200(%rdi), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %eax
	cmpb	$6, %al
	je	.L2451
.L2428:
	cmpl	$65533, 16(%r15)
	jg	.L2465
	xorl	%edx, %edx
	cmpb	$10, %al
	je	.L2466
.L2433:
	movb	%dl, 8(%r15)
	movq	200(%r12), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rax
	movl	(%rax), %r14d
	movq	(%r12), %rax
	movq	96(%rax), %rbx
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseBindingPatternEv
	movl	%eax, %edx
	andl	$7, %edx
	cmpl	$3, %edx
	je	.L2467
	movb	$0, 9(%r15)
	movq	200(%r12), %rdi
.L2439:
	movq	8(%rdi), %rax
	xorl	%r14d, %r14d
	cmpb	$17, 56(%rax)
	je	.L2468
.L2440:
	movq	(%r12), %rax
	movq	96(%rax), %rdx
	movq	(%rdi), %rax
	movl	4(%rax), %esi
	cmpq	%rdx, %rbx
	je	.L2447
	.p2align 4,,10
	.p2align 3
.L2444:
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	movl	%esi, 36(%rax)
	movq	(%rbx), %rbx
	addq	$16, %rbx
	cmpq	%rbx, %rdx
	jne	.L2444
.L2447:
	movzbl	8(%r15), %esi
	movq	(%r15), %rdi
	movl	%esi, %ebx
	call	_ZN2v88internal16DeclarationScope15RecordParameterEb@PLT
	testb	%bl, %bl
	jne	.L2445
	testb	%r14b, %r14b
	jne	.L2445
	movl	16(%r15), %eax
	leal	1(%rax), %edx
	cmpl	%eax, 12(%r15)
	jne	.L2448
	movl	%edx, 12(%r15)
	.p2align 4,,10
	.p2align 3
.L2448:
	movl	%edx, 16(%r15)
.L2442:
	cmpb	$0, 8(%r15)
	jne	.L2469
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	cmpb	$30, 56(%rax)
	je	.L2470
.L2451:
	cmpb	$0, 9(%r15)
	je	.L2430
.L2432:
	movq	%r13, 224(%r12)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2471
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2467:
	.cfi_restore_state
	shrl	$4, %eax
	movq	200(%r12), %rdi
	subl	$2, %eax
	cmpb	$1, %al
	ja	.L2439
	movq	144(%r12), %rax
	movzbl	16(%rax), %edx
	leal	-1(%rdx), %esi
	cmpb	$2, %sil
	ja	.L2439
	movq	(%rdi), %rsi
	movq	(%rax), %rdi
	movl	4(%rsi), %esi
	cmpb	$3, %dl
	je	.L2472
	movl	%r14d, 232(%rdi)
	movl	%esi, 236(%rdi)
	movq	(%rax), %rax
	movl	$295, 240(%rax)
	movq	200(%r12), %rdi
	jmp	.L2439
	.p2align 4,,10
	.p2align 3
.L2445:
	movl	16(%r15), %eax
	leal	1(%rax), %edx
	jmp	.L2448
	.p2align 4,,10
	.p2align 3
.L2430:
	movq	(%r15), %rax
	andb	$-2, 131(%rax)
	jmp	.L2432
	.p2align 4,,10
	.p2align 3
.L2470:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %eax
	cmpb	$6, %al
	jne	.L2428
	jmp	.L2451
	.p2align 4,,10
	.p2align 3
.L2468:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$0, 8(%r15)
	movb	$0, 9(%r15)
	jne	.L2473
	movzbl	256(%r12), %r14d
	leaq	-144(%rbp), %r8
	movq	%r12, %rsi
	movb	$1, 256(%r12)
	movq	%r8, %rdi
	movq	%r8, -160(%rbp)
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPS3_NS0_15ExpressionScopeIS4_E9ScopeTypeE.constprop.0
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseAssignmentExpressionCoverGrammarEv
	movq	-160(%rbp), %r8
	movl	%eax, -148(%rbp)
	movl	-84(%rbp), %eax
	cmpl	%eax, -88(%rbp)
	jbe	.L2474
.L2443:
	movq	-120(%rbp), %rdi
	movq	-112(%rbp), %rsi
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE6resizeEm
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rdx
	testb	$7, -148(%rbp)
	movq	%rdx, 144(%rax)
	movq	200(%r12), %rdi
	movb	%r14b, 256(%r12)
	setne	%r14b
	jmp	.L2440
	.p2align 4,,10
	.p2align 3
.L2466:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movl	$1, %edx
	jmp	.L2433
.L2474:
	xorl	%esi, %esi
	movq	%r8, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE8ValidateEi.part.0
	jmp	.L2443
.L2473:
	movl	$288, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	jmp	.L2442
.L2465:
	movl	$306, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	jmp	.L2432
.L2469:
	movb	$0, 9(%r15)
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	cmpb	$30, 56(%rax)
	jne	.L2430
	movq	(%rax), %rsi
	movl	$279, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	jmp	.L2432
.L2472:
	movq	(%rdi), %rax
	testb	$1, 129(%rax)
	je	.L2438
	salq	$32, %rsi
	movl	$295, %edx
	orq	%r14, %rsi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	movq	200(%r12), %rdi
	jmp	.L2439
.L2438:
	movq	224(%rdi), %rax
	cmpl	%esi, %r14d
	setbe	21(%rax)
	movq	200(%r12), %rdi
	jmp	.L2439
.L2471:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23019:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE24ParseFormalParameterListEPNS0_25PreParserFormalParametersE, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE24ParseFormalParameterListEPNS0_25PreParserFormalParametersE
	.section	.text._ZN2v88internal9PreParser16PreParseFunctionEPKNS0_12AstRawStringENS0_12FunctionKindENS0_18FunctionSyntaxKindEPNS0_16DeclarationScopeEPiPPNS0_20ProducedPreparseDataEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9PreParser16PreParseFunctionEPKNS0_12AstRawStringENS0_12FunctionKindENS0_18FunctionSyntaxKindEPNS0_16DeclarationScopeEPiPPNS0_20ProducedPreparseDataEi
	.type	_ZN2v88internal9PreParser16PreParseFunctionEPKNS0_12AstRawStringENS0_12FunctionKindENS0_18FunctionSyntaxKindEPNS0_16DeclarationScopeEPiPPNS0_20ProducedPreparseDataEi, @function
_ZN2v88internal9PreParser16PreParseFunctionEPKNS0_12AstRawStringENS0_12FunctionKindENS0_18FunctionSyntaxKindEPNS0_16DeclarationScopeEPiPPNS0_20ProducedPreparseDataEi:
.LFB20770:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	leaq	-128(%rbp), %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$216, %rsp
	movq	16(%rbp), %rax
	movq	%rsi, -240(%rbp)
	xorl	%esi, %esi
	movl	%ecx, -224(%rbp)
	movl	$256, %ecx
	movq	%rax, -232(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	24(%rbp), %eax
	movq	%r9, 264(%rdi)
	movq	%r8, -192(%rbp)
	movl	%eax, 212(%rdi)
	movq	(%rdi), %rax
	movw	%cx, -184(%rbp)
	movq	%rax, -120(%rbp)
	leaq	16(%rdi), %rax
	movq	%rax, -96(%rbp)
	movq	16(%rdi), %rax
	movq	$0, -180(%rbp)
	movw	%si, -172(%rbp)
	movl	$0, 208(%rdi)
	movq	%rdi, -128(%rbp)
	movq	%r8, (%rdi)
	movq	$0, -112(%rbp)
	movl	$0, -104(%rbp)
	movq	%rax, -88(%rbp)
	movq	%r8, -80(%rbp)
	movl	$0, -72(%rbp)
	movq	%rdx, 16(%rdi)
	testq	%rax, %rax
	je	.L2476
	movzbl	57(%rax), %edx
	movb	$0, 57(%rax)
	movb	%dl, 58(%rax)
.L2476:
	leal	-8(%r12), %eax
	movq	%r13, -208(%rbp)
	movq	$0, -200(%rbp)
	movb	%al, -217(%rbp)
	cmpb	$1, %al
	ja	.L2477
	movzbl	131(%rbx), %eax
	movq	200(%r13), %rdi
	leaq	-192(%rbp), %r14
	andl	$1, %eax
	movb	%al, -183(%rbp)
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$8, %al
	jne	.L2580
.L2491:
	cmpb	$0, -183(%rbp)
	movq	0(%r13), %rdx
	movq	%rbx, %r15
	je	.L2581
.L2492:
	movq	%r15, 0(%r13)
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rdx, -216(%rbp)
	call	_ZN2v88internal9PreParser32ParseStatementListAndLogFunctionEPNS0_25PreParserFormalParametersE
	movq	-216(%rbp), %rdx
	movq	200(%r13), %rax
	movq	%rdx, 0(%r13)
	movq	24(%rax), %rax
	cmpb	$0, 48(%rax)
	je	.L2495
.L2499:
	xorl	%eax, %eax
.L2496:
	movq	128(%r13), %rdi
	xorl	%r12d, %r12d
	movq	$0, 264(%r13)
	cmpb	$0, 1(%rdi)
	jne	.L2502
	cmpb	$0, 2(%rdi)
	jne	.L2503
	movq	200(%r13), %rdx
	movq	24(%rdx), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L2579
	cmpb	$1, -217(%rbp)
	jbe	.L2505
	movq	0(%r13), %rdx
	testb	$1, 129(%rdx)
	je	.L2582
	testb	%al, %al
	je	.L2583
	cmpb	$0, -171(%rbp)
	je	.L2509
.L2522:
	movq	%r13, %rdi
	call	_ZNK2v88internal25PreParserFormalParameters18ValidateStrictModeEPNS0_9PreParserE.part.0
.L2510:
	movq	200(%r13), %rax
	movq	24(%rax), %rax
	cmpb	$0, 48(%rax)
	je	.L2509
	movq	128(%r13), %rax
	cmpb	$0, 2(%rax)
	je	.L2579
.L2503:
	movl	$1, %r12d
.L2502:
	cmpq	$0, -200(%rbp)
	je	.L2518
	leaq	-208(%rbp), %rdi
	call	_ZN2v88internal19PreparseDataBuilder18DataGatheringScope5CloseEv@PLT
.L2518:
	movq	-96(%rbp), %rax
	movq	-88(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-128(%rbp), %rax
	movq	-120(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2584
	addq	$216, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2495:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZN2v88internal16DeclarationScope31CheckConflictingVarDeclarationsEv@PLT
	testq	%rax, %rax
	je	.L2497
	movl	(%rax), %esi
	movq	8(%rax), %rcx
	movl	$0, %eax
	movq	128(%r13), %rdi
	cmpl	$-1, %esi
	leal	1(%rsi), %edx
	movq	8(%rcx), %r8
	movl	$175, %ecx
	cmove	%eax, %edx
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKNS0_12AstRawStringE@PLT
	movq	200(%r13), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L2499
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L2497:
	movq	200(%r13), %rax
	movq	24(%rax), %rax
	cmpb	$0, 48(%rax)
	jne	.L2499
	cmpb	$0, -183(%rbp)
	je	.L2500
	testb	$1, 129(%rbx)
	jne	.L2499
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal16DeclarationScope25HoistSloppyBlockFunctionsEPNS0_14AstNodeFactoryE@PLT
	testb	$1, 129(%rbx)
	jne	.L2499
	leal	-11(%r12), %eax
	cmpb	$1, %al
	jbe	.L2499
	subl	$15, %r12d
	cmpb	$2, %r12b
	seta	%al
	jmp	.L2496
	.p2align 4,,10
	.p2align 3
.L2477:
	leaq	-208(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal19PreparseDataBuilder18DataGatheringScope5StartEPNS0_16DeclarationScopeE@PLT
	movq	144(%r13), %rax
	movl	$259, %edx
	movq	%r13, -160(%rbp)
	movw	%dx, -144(%rbp)
	xorl	%edx, %edx
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L2479
	movzbl	18(%rax), %edx
.L2479:
	leaq	-160(%rbp), %rax
	leaq	-192(%rbp), %r14
	movq	%r13, %rdi
	movb	%dl, -142(%rbp)
	movq	%rax, 144(%r13)
	movq	%r14, %rsi
	movl	$4294967295, %eax
	movq	%rax, -140(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE24ParseFormalParameterListEPNS0_25PreParserFormalParametersE
	movl	-136(%rbp), %eax
	cmpl	%eax, -140(%rbp)
	ja	.L2480
	movb	$1, -172(%rbp)
.L2480:
	cmpb	$0, -183(%rbp)
	je	.L2585
.L2482:
	movq	200(%r13), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$6, %al
	jne	.L2586
.L2485:
	movq	200(%r13), %rax
	movl	112(%rbx), %r15d
	movl	-176(%rbp), %edx
	movq	(%rax), %rax
	movl	4(%rax), %ecx
	cmpb	$6, %r12b
	je	.L2587
	cmpb	$7, %r12b
	je	.L2588
.L2488:
	movq	-160(%rbp), %rax
	movq	-152(%rbp), %rdx
	movq	%rdx, 144(%rax)
	movq	200(%r13), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$8, %al
	je	.L2491
.L2580:
	movq	%r13, %rdi
	movq	%rbx, %r15
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	cmpb	$0, -183(%rbp)
	movq	0(%r13), %rdx
	jne	.L2492
	.p2align 4,,10
	.p2align 3
.L2581:
	movq	136(%r13), %r11
	movq	16(%r11), %r9
	movq	24(%r11), %rax
	subq	%r9, %rax
	cmpq	$223, %rax
	jbe	.L2589
	leaq	224(%r9), %rax
	movq	%rax, 16(%r11)
.L2494:
	xorl	%r8d, %r8d
	movl	$6, %ecx
	movq	%r11, %rsi
	movq	%r9, %rdi
	movq	%r9, %r15
	call	_ZN2v88internal16DeclarationScopeC1EPNS0_4ZoneEPNS0_5ScopeENS0_9ScopeTypeENS0_12FunctionKindE@PLT
	movq	200(%r13), %rax
	movq	(%rax), %rax
	movl	(%rax), %eax
	movl	%eax, 112(%r15)
	movq	0(%r13), %rdx
	jmp	.L2492
	.p2align 4,,10
	.p2align 3
.L2582:
	testb	%al, %al
	je	.L2590
.L2509:
	movq	40(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal16DeclarationScope16DeclareArgumentsEPNS0_15AstValueFactoryE@PLT
	cmpb	$1, -224(%rbp)
	jne	.L2514
	movq	-240(%rbp), %rsi
	leaq	32(%rbx), %rdi
	call	_ZN2v88internal11VariableMap6LookupEPKNS0_12AstRawStringE@PLT
	testq	%rax, %rax
	jne	.L2514
	movq	-240(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal16DeclarationScope18DeclareFunctionVarEPKNS0_12AstRawStringEPNS0_5ScopeE@PLT
	.p2align 4,,10
	.p2align 3
.L2514:
	movq	288(%r13), %rdi
	call	_ZNK2v88internal19PreparseDataBuilder7HasDataEv@PLT
	testb	%al, %al
	je	.L2516
	movq	40(%r13), %rax
	movq	288(%r13), %rdi
	movq	1096(%rax), %rsi
	call	_ZN2v88internal20ProducedPreparseData3ForEPNS0_19PreparseDataBuilderEPNS0_4ZoneE@PLT
	movq	-232(%rbp), %rcx
	movq	%rax, (%rcx)
.L2516:
	movq	128(%r13), %rdi
	cmpb	$0, 2(%rdi)
	jne	.L2503
	.p2align 4,,10
	.p2align 3
.L2505:
	testb	$1, 129(%rbx)
	je	.L2579
	movq	200(%r13), %rax
	movq	(%rax), %rcx
	movl	340(%rax), %edx
	movl	336(%rax), %esi
	cmpl	%edx, 4(%rcx)
	setge	%r8b
	cmpl	%edx, %esi
	setbe	%cl
	testb	%cl, %r8b
	je	.L2579
	cmpl	%esi, 112(%rbx)
	jg	.L2579
	movl	344(%rax), %ebx
	xorl	%r8d, %r8d
	movl	%ebx, %ecx
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r13), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L2517
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	movq	200(%r13), %rax
.L2517:
	movl	$0, 344(%rax)
	movl	$4294967295, %ecx
	movq	%rcx, 336(%rax)
	cmpl	$298, %ebx
	jne	.L2579
	movq	264(%r13), %rax
	testq	%rax, %rax
	je	.L2579
	addl	$1, 128(%rax)
	.p2align 4,,10
	.p2align 3
.L2579:
	movl	$2, %r12d
	jmp	.L2502
	.p2align 4,,10
	.p2align 3
.L2500:
	movzbl	129(%r15), %eax
	andl	$1, %eax
	je	.L2591
.L2501:
	movzbl	129(%rbx), %edx
	movq	%r15, %rdi
	andl	$-2, %edx
	orl	%edx, %eax
	movb	%al, 129(%rbx)
	movq	200(%r13), %rax
	movq	8(%rax), %rax
	movl	4(%rax), %eax
	movl	%eax, 116(%r15)
	call	_ZN2v88internal5Scope18FinalizeBlockScopeEv@PLT
	testq	%rax, %rax
	je	.L2499
	movl	$1, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal5Scope22FindVariableDeclaredInEPS1_NS0_12VariableModeE@PLT
	testq	%rax, %rax
	je	.L2499
	movq	128(%r13), %rax
	movb	$1, (%rax)
	movb	$1, 2(%rax)
	movq	200(%r13), %rdx
	movq	24(%rdx), %rcx
	movzbl	48(%rcx), %eax
	testb	%al, %al
	jne	.L2499
	movl	$-1, 32(%rdx)
	movq	24(%rcx), %rsi
	movb	$1, 48(%rcx)
	movq	%rsi, 16(%rcx)
	movb	$109, 96(%rdx)
	movb	$109, 176(%rdx)
	movb	$109, 256(%rdx)
	jmp	.L2496
	.p2align 4,,10
	.p2align 3
.L2591:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal16DeclarationScope25HoistSloppyBlockFunctionsEPNS0_14AstNodeFactoryE@PLT
	movzbl	129(%r15), %eax
	andl	$1, %eax
	jmp	.L2501
	.p2align 4,,10
	.p2align 3
.L2588:
	movzbl	-184(%rbp), %r8d
	cmpl	$1, %edx
	jne	.L2592
.L2490:
	testb	%r8b, %r8b
	je	.L2488
	salq	$32, %rcx
	movl	%r15d, %esi
	movl	$283, %edx
	movq	%r13, %rdi
	orq	%rcx, %rsi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	jmp	.L2488
	.p2align 4,,10
	.p2align 3
.L2587:
	testl	%edx, %edx
	je	.L2488
	salq	$32, %rcx
	movl	%r15d, %esi
	movl	$223, %edx
	movq	%r13, %rdi
	orq	%rcx, %rsi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	jmp	.L2488
	.p2align 4,,10
	.p2align 3
.L2585:
	movq	0(%r13), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	testb	$4, 129(%rax)
	je	.L2482
	movq	288(%r13), %rax
	testq	%rax, %rax
	je	.L2482
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2484
	orb	$1, 76(%rdx)
	jmp	.L2482
	.p2align 4,,10
	.p2align 3
.L2592:
	movq	%rcx, %rdx
	movl	%r15d, %esi
	movq	%r13, %rdi
	movb	%r8b, -248(%rbp)
	salq	$32, %rdx
	movl	%ecx, -216(%rbp)
	orq	%rdx, %rsi
	movl	$224, %edx
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	movzbl	-248(%rbp), %r8d
	movl	-216(%rbp), %ecx
	jmp	.L2490
	.p2align 4,,10
	.p2align 3
.L2583:
	cmpb	$0, -172(%rbp)
	jne	.L2593
.L2508:
	cmpb	$0, -171(%rbp)
	je	.L2510
	jmp	.L2522
	.p2align 4,,10
	.p2align 3
.L2590:
	cmpb	$0, -172(%rbp)
	je	.L2509
	movq	%r13, %rdi
	call	_ZNK2v88internal25PreParserFormalParameters17ValidateDuplicateEPNS0_9PreParserE.part.0
	jmp	.L2510
	.p2align 4,,10
	.p2align 3
.L2586:
	movq	%r13, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L2485
	.p2align 4,,10
	.p2align 3
.L2589:
	movq	%r11, %rdi
	movl	$224, %esi
	movq	%rdx, -248(%rbp)
	movq	%r11, -216(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-216(%rbp), %r11
	movq	-248(%rbp), %rdx
	movq	%rax, %r9
	jmp	.L2494
	.p2align 4,,10
	.p2align 3
.L2484:
	orb	$1, 76(%rax)
	jmp	.L2482
	.p2align 4,,10
	.p2align 3
.L2593:
	movq	%r13, %rdi
	call	_ZNK2v88internal25PreParserFormalParameters17ValidateDuplicateEPNS0_9PreParserE.part.0
	jmp	.L2508
.L2584:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20770:
	.size	_ZN2v88internal9PreParser16PreParseFunctionEPKNS0_12AstRawStringENS0_12FunctionKindENS0_18FunctionSyntaxKindEPNS0_16DeclarationScopeEPiPPNS0_20ProducedPreparseDataEi, .-_ZN2v88internal9PreParser16PreParseFunctionEPKNS0_12AstRawStringENS0_12FunctionKindENS0_18FunctionSyntaxKindEPNS0_16DeclarationScopeEPiPPNS0_20ProducedPreparseDataEi
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseArrowParametersWithRestEPNS0_23PreParserExpressionListEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEEi,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseArrowParametersWithRestEPNS0_23PreParserExpressionListEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseArrowParametersWithRestEPNS0_23PreParserExpressionListEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEEi
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseArrowParametersWithRestEPNS0_23PreParserExpressionListEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEEi, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseArrowParametersWithRestEPNS0_23PreParserExpressionListEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEEi:
.LFB26094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	200(%rdi), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rax
	movl	(%rax), %r15d
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseBindingPatternEv
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ecx
	movl	%r15d, %edx
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ClassifyArrowParameterEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEEiNS0_19PreParserExpressionE
	movq	144(%r12), %rdx
	movzbl	16(%rdx), %eax
	subl	$1, %eax
	cmpb	$1, %al
	ja	.L2595
	movb	$0, 88(%rdx)
.L2595:
	movq	200(%r12), %rdi
	movq	8(%rdi), %r8
	movzbl	56(%r8), %esi
	cmpb	$17, %sil
	je	.L2613
	cmpb	$30, %sil
	je	.L2614
	movq	144(%r12), %rcx
	cmpb	$2, 16(%rcx)
	ja	.L2599
	movq	32(%rcx), %rax
	movl	40(%rcx), %edx
	subl	%eax, %edx
	je	.L2599
	subl	$1, %edx
	cmpl	%edx, %r13d
	jg	.L2599
	movl	(%r8), %edi
	movslq	%edx, %rdx
	jmp	.L2602
	.p2align 4,,10
	.p2align 3
.L2600:
	subq	$1, %rdx
	movl	%edi, 8(%rax)
	cmpl	%edx, %r13d
	jg	.L2612
	movq	32(%rcx), %rax
.L2602:
	movq	24(%rcx), %rsi
	addq	%rdx, %rax
	salq	$4, %rax
	addq	(%rsi), %rax
	cmpl	$-1, 8(%rax)
	je	.L2600
.L2612:
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %esi
.L2599:
	cmpb	$6, %sil
	je	.L2603
.L2606:
	movq	128(%r12), %rax
	movb	$1, (%rax)
	movb	$1, 2(%rax)
	movq	200(%r12), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	je	.L2615
.L2604:
	movl	$1, %eax
.L2597:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2615:
	.cfi_restore_state
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L2604
	.p2align 4,,10
	.p2align 3
.L2603:
	call	_ZN2v88internal7Scanner9PeekAheadEv@PLT
	cmpb	$15, %al
	jne	.L2606
	addl	$1, (%rbx)
	movl	$2, %eax
	jmp	.L2597
	.p2align 4,,10
	.p2align 3
.L2613:
	movq	%r12, %rdi
	movl	$288, %esi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2614:
	.cfi_restore_state
	movl	$279, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	movl	$1, %eax
	jmp	.L2597
	.cfi_endproc
.LFE26094:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseArrowParametersWithRestEPNS0_23PreParserExpressionListEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEEi, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseArrowParametersWithRestEPNS0_23PreParserExpressionListEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEEi
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE27ParseExpressionCoverGrammarEv,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE27ParseExpressionCoverGrammarEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE27ParseExpressionCoverGrammarEv
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE27ParseExpressionCoverGrammarEv, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE27ParseExpressionCoverGrammarEv:
.LFB25884:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	144(%rdi), %rsi
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -100(%rbp)
	call	_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEEC1EPNS0_15ExpressionScopeIS4_EE
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	cmpb	$10, 56(%rax)
	je	.L2617
.L2618:
	movl	(%rax), %r15d
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseAssignmentExpressionCoverGrammarEv
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ecx
	movl	%r15d, %edx
	movl	%eax, %ebx
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ClassifyArrowParameterEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEEiNS0_19PreParserExpressionE
	movl	-100(%rbp), %eax
	movq	200(%r12), %r8
	leal	1(%rax), %ecx
	movq	144(%r12), %rax
	movq	8(%r8), %rsi
	movl	%ecx, -100(%rbp)
	cmpb	$2, 16(%rax)
	ja	.L2620
	movq	32(%rax), %rdx
	movl	40(%rax), %edi
	subl	%edx, %edi
	je	.L2632
	subl	$1, %edi
	cmpl	%r14d, %edi
	jl	.L2633
	movl	(%rsi), %r9d
	movslq	%edi, %rsi
	jmp	.L2623
	.p2align 4,,10
	.p2align 3
.L2621:
	subq	$1, %rsi
	movl	%r9d, 8(%rdx)
	cmpl	%esi, %r14d
	jg	.L2638
	movq	32(%rax), %rdx
.L2623:
	movq	24(%rax), %r8
	addq	%rsi, %rdx
	salq	$4, %rdx
	addq	(%r8), %rdx
	cmpl	$-1, 8(%rdx)
	je	.L2621
.L2638:
	movq	200(%r12), %r8
	movl	%edi, %r14d
	movq	8(%r8), %rsi
.L2620:
	cmpb	$30, 56(%rsi)
	je	.L2639
.L2624:
	cmpl	$1, %ecx
	movl	$2, %r12d
	cmove	%ebx, %r12d
.L2619:
	movq	%r13, %rdi
	call	_ZN2v88internal17AccumulationScopeINS0_11ParserTypesINS0_9PreParserEEEED1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2640
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2632:
	.cfi_restore_state
	xorl	%r14d, %r14d
	cmpb	$30, 56(%rsi)
	jne	.L2624
	.p2align 4,,10
	.p2align 3
.L2639:
	movq	%r8, %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %edx
	cmpb	$6, %dl
	je	.L2641
.L2625:
	cmpb	$73, %dl
	je	.L2642
.L2627:
	cmpb	$10, %dl
	jne	.L2618
	.p2align 4,,10
	.p2align 3
.L2617:
	movq	%r12, %rdi
	leaq	-100(%rbp), %rsi
	movl	%r14d, %ecx
	movq	%r13, %rdx
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseArrowParametersWithRestEPNS0_23PreParserExpressionListEPNS0_17AccumulationScopeINS0_11ParserTypesIS2_EEEEi
	movl	%eax, %r12d
	jmp	.L2619
	.p2align 4,,10
	.p2align 3
.L2642:
	movq	16(%r12), %rdx
	cmpb	$0, 58(%rdx)
	je	.L2618
	movzbl	_ZN2v88internal13FLAG_max_lazyE(%rip), %eax
	xorl	$1, %eax
	movb	%al, 57(%rdx)
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %edx
	jmp	.L2627
	.p2align 4,,10
	.p2align 3
.L2641:
	call	_ZN2v88internal7Scanner9PeekAheadEv@PLT
	cmpb	$15, %al
	je	.L2643
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %edx
	jmp	.L2625
	.p2align 4,,10
	.p2align 3
.L2633:
	movl	%edi, %r14d
	jmp	.L2620
.L2643:
	movl	-100(%rbp), %ecx
	jmp	.L2624
.L2640:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25884:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE27ParseExpressionCoverGrammarEv, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE27ParseExpressionCoverGrammarEv
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseThrowStatementEv,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseThrowStatementEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseThrowStatementEv
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseThrowStatementEv, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseThrowStatementEv:
.LFB25542:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -40
	movq	200(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	movzbl	76(%rax), %ecx
	testb	%cl, %cl
	je	.L2645
	movl	$277, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	xorl	%eax, %eax
.L2646:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2669
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2645:
	.cfi_restore_state
	movq	144(%r12), %rdx
	movq	%r12, -128(%rbp)
	movb	$0, -112(%rbp)
	movq	%rdx, -120(%rbp)
	testq	%rdx, %rdx
	je	.L2647
	cmpb	$0, 17(%rdx)
	movzbl	18(%rdx), %eax
	setne	-111(%rbp)
.L2660:
	movb	%al, -110(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, 144(%r12)
	leaq	176(%r12), %rax
	movq	%rax, -104(%rbp)
	movq	184(%r12), %rax
	subq	176(%r12), %rax
	sarq	$4, %rax
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movups	%xmm0, -72(%rbp)
	testq	%rdx, %rdx
	je	.L2650
	cmpb	$2, 16(%rdx)
	jbe	.L2670
.L2650:
	movzbl	256(%r12), %ebx
	movq	%r12, %rdi
	movb	$1, 256(%r12)
	movb	%cl, -56(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	movl	$-1, -64(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE27ParseExpressionCoverGrammarEv
	movl	-72(%rbp), %esi
	movl	-68(%rbp), %edx
	cmpl	%edx, %esi
	jbe	.L2671
.L2651:
	movq	-104(%rbp), %rdi
	movq	-96(%rbp), %rsi
	movb	%bl, 256(%r12)
	movq	8(%rdi), %rcx
	movq	(%rdi), %rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	ja	.L2672
	jb	.L2673
.L2654:
	movq	-128(%rbp), %rax
	movq	-120(%rbp), %rdx
	movq	%rdx, 144(%rax)
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %edx
	cmpb	$12, %dl
	jne	.L2655
	call	_ZN2v88internal7Scanner4NextEv@PLT
.L2656:
	movl	$3, %eax
	jmp	.L2646
	.p2align 4,,10
	.p2align 3
.L2670:
	movzbl	72(%rdx), %ecx
	jmp	.L2650
	.p2align 4,,10
	.p2align 3
.L2673:
	salq	$4, %rsi
	addq	%rdx, %rsi
	cmpq	%rsi, %rcx
	je	.L2654
	movq	%rsi, 8(%rdi)
	jmp	.L2654
	.p2align 4,,10
	.p2align 3
.L2672:
	subq	%rax, %rsi
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm
	jmp	.L2654
	.p2align 4,,10
	.p2align 3
.L2671:
	movq	-128(%rbp), %r13
	movl	-80(%rbp), %ecx
	xorl	%r8d, %r8d
	movq	128(%r13), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r13), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L2651
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L2651
	.p2align 4,,10
	.p2align 3
.L2647:
	movb	$0, -111(%rbp)
	xorl	%eax, %eax
	jmp	.L2660
	.p2align 4,,10
	.p2align 3
.L2655:
	cmpb	$0, 76(%rax)
	jne	.L2656
	subl	$12, %edx
	cmpb	$2, %dl
	jbe	.L2656
	movq	(%rdi), %rax
	cmpb	$96, 56(%rax)
	je	.L2657
.L2658:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L2656
.L2669:
	call	__stack_chk_fail@PLT
.L2657:
	movq	16(%r12), %rax
	movq	48(%rax), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$9, %eax
	cmpb	$4, %al
	ja	.L2666
	movq	200(%r12), %rdi
	jmp	.L2658
.L2666:
	movq	200(%r12), %rax
	movl	$16, %edx
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateE
	jmp	.L2656
	.cfi_endproc
.LFE25542:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseThrowStatementEv, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseThrowStatementEv
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseTemplateLiteralENS0_19PreParserExpressionEib,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseTemplateLiteralENS0_19PreParserExpressionEib,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseTemplateLiteralENS0_19PreParserExpressionEib
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseTemplateLiteralENS0_19PreParserExpressionEib, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseTemplateLiteralENS0_19PreParserExpressionEib:
.LFB26299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%ecx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	testb	%cl, %cl
	je	.L2675
	movb	$0, 261(%rdi)
.L2675:
	movq	200(%rbx), %rdi
	xorl	$1, %r13d
	movq	8(%rdi), %rax
	cmpb	$1, 56(%rax)
	je	.L2709
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%rbx), %rax
	movq	(%rax), %rdx
	movl	60(%rdx), %ecx
	testl	%ecx, %ecx
	je	.L2681
	testb	%r13b, %r13b
	jne	.L2710
.L2682:
	movl	$0, 60(%rdx)
	movq	200(%rbx), %rax
.L2681:
	movzbl	256(%rbx), %r14d
	jmp	.L2689
	.p2align 4,,10
	.p2align 3
.L2687:
	movl	$0, 60(%rax)
.L2686:
	movb	%r14b, 256(%rbx)
	testb	%r12b, %r12b
	jne	.L2677
	movq	200(%rbx), %rax
.L2689:
	movq	8(%rax), %rax
	movq	%rbx, %rdi
	movl	(%rax), %r12d
	movb	$1, 256(%rbx)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE27ParseExpressionCoverGrammarEv
	movq	200(%rbx), %rdi
	movq	8(%rdi), %rax
	cmpb	$13, 56(%rax)
	jne	.L2711
	call	_ZN2v88internal7Scanner16ScanTemplateSpanEv@PLT
	movq	200(%rbx), %rdi
	movl	%eax, %r12d
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%rbx), %rax
	movq	(%rax), %rax
	movl	60(%rax), %ecx
	testl	%ecx, %ecx
	je	.L2686
	testb	%r13b, %r13b
	je	.L2687
	movl	68(%rax), %edx
	movl	64(%rax), %esi
	xorl	%r8d, %r8d
	movq	128(%rbx), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%rbx), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L2688
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	movq	200(%rbx), %rax
.L2688:
	movq	(%rax), %rax
	jmp	.L2687
	.p2align 4,,10
	.p2align 3
.L2709:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%rbx), %rax
	movq	(%rax), %rax
	movl	60(%rax), %ecx
	testl	%ecx, %ecx
	je	.L2677
	testb	%r13b, %r13b
	je	.L2678
	movl	68(%rax), %edx
	movl	64(%rax), %esi
	xorl	%r8d, %r8d
	movq	128(%rbx), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%rbx), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L2679
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	movq	200(%rbx), %rax
.L2679:
	movq	(%rax), %rax
.L2678:
	movl	$0, 60(%rax)
.L2677:
	popq	%rbx
	movl	$2, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2711:
	.cfi_restore_state
	movl	(%rax), %edx
	movq	128(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	%r12d, %esi
	movl	$331, %ecx
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%rbx), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L2685
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L2685:
	movb	%r14b, 256(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2710:
	.cfi_restore_state
	movl	68(%rdx), %r9d
	movl	64(%rdx), %esi
	xorl	%r8d, %r8d
	movq	128(%rbx), %rdi
	movl	%r9d, %edx
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%rbx), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L2683
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	movq	200(%rbx), %rax
.L2683:
	movq	(%rax), %rdx
	jmp	.L2682
	.cfi_endproc
.LFE26299:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseTemplateLiteralENS0_19PreParserExpressionEib, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseTemplateLiteralENS0_19PreParserExpressionEib
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE35DoParseMemberExpressionContinuationENS0_19PreParserExpressionE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE35DoParseMemberExpressionContinuationENS0_19PreParserExpressionE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE35DoParseMemberExpressionContinuationENS0_19PreParserExpressionE
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE35DoParseMemberExpressionContinuationENS0_19PreParserExpressionE, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE35DoParseMemberExpressionContinuationENS0_19PreParserExpressionE:
.LFB26421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	200(%rdi), %rdi
	movq	8(%rdi), %rdx
	movzbl	56(%rdx), %eax
	jmp	.L2725
	.p2align 4,,10
	.p2align 3
.L2743:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movzbl	256(%rbx), %r13d
	movq	%rbx, %rdi
	movb	$1, 256(%rbx)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE27ParseExpressionCoverGrammarEv
	movl	%r12d, %ecx
	movl	%eax, %edx
	andl	$7, %ecx
	andl	$7, %edx
	cmpl	$3, %edx
	je	.L2739
.L2715:
	cmpl	$2, %ecx
	je	.L2740
	movl	$50, %r12d
.L2717:
	movq	200(%rbx), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$7, %al
	jne	.L2741
.L2718:
	movb	%r13b, 256(%rbx)
.L2719:
	movq	200(%rbx), %rdi
	movq	8(%rdi), %rdx
	movzbl	56(%rdx), %eax
	cmpb	$3, %al
	ja	.L2742
.L2725:
	cmpb	$2, %al
	je	.L2713
	cmpb	$3, %al
	je	.L2743
	movq	(%rdi), %rax
	cmpb	$92, 56(%rax)
	je	.L2744
	movl	(%rdx), %edx
.L2724:
	movl	%r12d, %esi
	movl	$1, %ecx
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseTemplateLiteralENS0_19PreParserExpressionEib
	movl	%eax, %r12d
	jmp	.L2719
	.p2align 4,,10
	.p2align 3
.L2713:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE34ParsePropertyOrPrivatePropertyNameEv
	movl	%r12d, %ecx
	movl	%eax, %edx
	andl	$7, %ecx
	andl	$7, %edx
	cmpl	$3, %edx
	je	.L2745
.L2720:
	cmpl	$2, %ecx
	je	.L2746
	movl	$50, %r12d
	jmp	.L2719
	.p2align 4,,10
	.p2align 3
.L2739:
	shrl	$4, %eax
	cmpb	$8, %al
	jne	.L2715
	cmpl	$2, %ecx
	je	.L2747
	movq	200(%rbx), %rdi
	movl	$66, %r12d
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$7, %al
	je	.L2718
	.p2align 4,,10
	.p2align 3
.L2741:
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L2718
	.p2align 4,,10
	.p2align 3
.L2744:
	movl	(%rax), %edx
	jmp	.L2724
	.p2align 4,,10
	.p2align 3
.L2745:
	shrl	$4, %eax
	cmpb	$8, %al
	jne	.L2720
	cmpl	$2, %ecx
	je	.L2748
	movl	$66, %r12d
	jmp	.L2719
	.p2align 4,,10
	.p2align 3
.L2742:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2746:
	.cfi_restore_state
	shrl	$4, %r12d
	andl	$15, %r12d
	cmpl	$1, %r12d
	sbbl	%r12d, %r12d
	andl	$-32, %r12d
	addl	$50, %r12d
	jmp	.L2719
	.p2align 4,,10
	.p2align 3
.L2740:
	shrl	$4, %r12d
	andl	$15, %r12d
	cmpl	$1, %r12d
	sbbl	%r12d, %r12d
	andl	$-32, %r12d
	addl	$50, %r12d
	jmp	.L2717
	.p2align 4,,10
	.p2align 3
.L2748:
	shrl	$4, %r12d
	andl	$15, %r12d
	cmpl	$1, %r12d
	sbbl	%r12d, %r12d
	andl	$-32, %r12d
	addl	$66, %r12d
	jmp	.L2719
	.p2align 4,,10
	.p2align 3
.L2747:
	shrl	$4, %r12d
	andl	$15, %r12d
	cmpl	$1, %r12d
	sbbl	%r12d, %r12d
	andl	$-32, %r12d
	addl	$66, %r12d
	jmp	.L2717
	.cfi_endproc
.LFE26421:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE35DoParseMemberExpressionContinuationENS0_19PreParserExpressionE, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE35DoParseMemberExpressionContinuationENS0_19PreParserExpressionE
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseReturnStatementEv,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseReturnStatementEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseReturnStatementEv
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseReturnStatementEv, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseReturnStatementEv:
.LFB25541:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	200(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%rbx), %rax
	movq	(%rbx), %rdi
	movq	(%rax), %rax
	movl	(%rax), %r12d
	movl	4(%rax), %r13d
	call	_ZN2v88internal5Scope19GetDeclarationScopeEv@PLT
	movzbl	128(%rax), %eax
	cmpb	$1, %al
	je	.L2750
	subl	$3, %eax
	cmpb	$1, %al
	ja	.L2751
.L2750:
	movq	128(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	%r13d, %edx
	movl	%r12d, %esi
	movl	$243, %ecx
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%rbx), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L2752
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L2752:
	xorl	%eax, %eax
	jmp	.L2753
	.p2align 4,,10
	.p2align 3
.L2751:
	movq	200(%rbx), %rax
	movq	8(%rax), %rax
	cmpb	$0, 76(%rax)
	jne	.L2754
	movzbl	56(%rax), %eax
	subl	$12, %eax
	cmpb	$2, %al
	ja	.L2755
.L2754:
	movq	16(%rbx), %rax
	xorl	%r12d, %r12d
	movq	48(%rax), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$4, %eax
	cmpb	$1, %al
	jbe	.L2794
.L2757:
	movq	200(%rbx), %rdi
	movq	8(%rdi), %rdx
	movzbl	56(%rdx), %ecx
	cmpb	$12, %cl
	jne	.L2770
	call	_ZN2v88internal7Scanner4NextEv@PLT
.L2771:
	movq	16(%rbx), %rdx
	movq	48(%rdx), %rdi
	testl	%r12d, %r12d
	jne	.L2795
.L2774:
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movl	$3, %eax
.L2753:
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L2796
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2795:
	.cfi_restore_state
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$12, %eax
	cmpb	$1, %al
	movq	16(%rbx), %rax
	jbe	.L2775
.L2792:
	movq	48(%rax), %rdi
	jmp	.L2774
	.p2align 4,,10
	.p2align 3
.L2755:
	leaq	-128(%rbp), %r13
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPS3_NS0_15ExpressionScopeIS4_E9ScopeTypeE.constprop.0
	movzbl	256(%rbx), %r14d
	movq	%rbx, %rdi
	movb	$1, 256(%rbx)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE27ParseExpressionCoverGrammarEv
	movl	%eax, %r12d
	movl	-68(%rbp), %eax
	cmpl	%eax, -72(%rbp)
	jbe	.L2797
.L2759:
	movq	-104(%rbp), %rdi
	movq	-96(%rbp), %rsi
	movb	%r14b, 256(%rbx)
	movq	8(%rdi), %rcx
	movq	(%rdi), %rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	ja	.L2798
	jnb	.L2769
	salq	$4, %rsi
	addq	%rdx, %rsi
	cmpq	%rsi, %rcx
	je	.L2769
	movq	%rsi, 8(%rdi)
.L2769:
	movq	-128(%rbp), %rax
	movq	-120(%rbp), %rdx
	andl	$7, %r12d
	movq	%rdx, 144(%rax)
	jmp	.L2757
	.p2align 4,,10
	.p2align 3
.L2794:
	leaq	-128(%rbp), %r13
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPS3_NS0_15ExpressionScopeIS4_E9ScopeTypeE.constprop.0
	movq	(%rbx), %rdi
	call	_ZN2v88internal5Scope15GetClosureScopeEv@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal5Scope16GetReceiverScopeEv@PLT
	movq	176(%rax), %rdx
	orw	$2048, 40(%rdx)
	cmpq	%r12, %rax
	je	.L2799
	orb	$8, 132(%r12)
	orw	$1024, 40(%rdx)
.L2762:
	movl	-68(%rbp), %eax
	cmpl	%eax, -72(%rbp)
	jbe	.L2800
.L2765:
	movq	-104(%rbp), %rdi
	movq	-96(%rbp), %rsi
	movq	8(%rdi), %rcx
	movq	(%rdi), %rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	ja	.L2801
	jnb	.L2767
	salq	$4, %rsi
	addq	%rdx, %rsi
	cmpq	%rsi, %rcx
	je	.L2767
	movq	%rsi, 8(%rdi)
.L2767:
	movq	-128(%rbp), %rax
	movq	-120(%rbp), %rdx
	movl	$2, %r12d
	movq	%rdx, 144(%rax)
	jmp	.L2757
	.p2align 4,,10
	.p2align 3
.L2775:
	addl	$1, 20(%rax)
	movq	16(%rbx), %rax
	jmp	.L2792
	.p2align 4,,10
	.p2align 3
.L2770:
	cmpb	$0, 76(%rdx)
	jne	.L2771
	subl	$12, %ecx
	cmpb	$2, %cl
	jbe	.L2771
	movq	(%rdi), %rax
	cmpb	$96, 56(%rax)
	je	.L2772
.L2773:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L2771
	.p2align 4,,10
	.p2align 3
.L2798:
	subq	%rax, %rsi
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm
	jmp	.L2769
	.p2align 4,,10
	.p2align 3
.L2797:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE8ValidateEi.part.0
	jmp	.L2759
	.p2align 4,,10
	.p2align 3
.L2800:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE8ValidateEi.part.0
	jmp	.L2765
	.p2align 4,,10
	.p2align 3
.L2801:
	subq	%rax, %rsi
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm
	jmp	.L2767
	.p2align 4,,10
	.p2align 3
.L2799:
	movq	144(%rbx), %rax
.L2764:
	movzbl	16(%rax), %ecx
	leal	-1(%rcx), %edx
	cmpb	$1, %dl
	jbe	.L2802
.L2761:
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L2762
	movzbl	16(%rax), %ecx
	leal	-1(%rcx), %edx
	cmpb	$1, %dl
	ja	.L2761
.L2802:
	movb	$1, 89(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L2764
	jmp	.L2762
.L2796:
	call	__stack_chk_fail@PLT
.L2772:
	movq	16(%rbx), %rax
	movq	48(%rax), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$9, %eax
	cmpb	$4, %al
	ja	.L2790
	movq	200(%rbx), %rdi
	jmp	.L2773
.L2790:
	movq	200(%rbx), %rax
	movl	$16, %edx
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateE
	jmp	.L2771
	.cfi_endproc
.LFE25541:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseReturnStatementEv, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseReturnStatementEv
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseLeftHandSideContinuationENS0_19PreParserExpressionE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseLeftHandSideContinuationENS0_19PreParserExpressionE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseLeftHandSideContinuationENS0_19PreParserExpressionE
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseLeftHandSideContinuationENS0_19PreParserExpressionE, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseLeftHandSideContinuationENS0_19PreParserExpressionE:
.LFB26048:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	subq	$176, %rsp
	.cfi_offset 3, -48
	movq	200(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movzbl	56(%rax), %edx
	cmpb	$5, %dl
	je	.L2882
.L2804:
	xorl	%ebx, %ebx
	jmp	.L2810
	.p2align 4,,10
	.p2align 3
.L2887:
	cmpb	$3, %dl
	jne	.L2817
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movzbl	256(%r13), %r14d
	movq	%r13, %rdi
	movb	$1, 256(%r13)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE27ParseExpressionCoverGrammarEv
	movl	%r12d, %ecx
	movl	%eax, %edx
	andl	$7, %ecx
	andl	$7, %edx
	cmpl	$3, %edx
	je	.L2883
.L2821:
	cmpl	$2, %ecx
	je	.L2884
	movl	$50, %r12d
.L2823:
	movq	200(%r13), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$7, %al
	jne	.L2885
.L2824:
	movb	%r14b, 256(%r13)
.L2825:
	movq	200(%r13), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %edx
	cmpb	$5, %dl
	ja	.L2886
.L2810:
	cmpb	$4, %dl
	je	.L2813
	xorl	%ecx, %ecx
.L2820:
	cmpb	$4, %dl
	ja	.L2814
	cmpb	$2, %dl
	jne	.L2887
	testb	%cl, %cl
	jne	.L2888
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE34ParsePropertyOrPrivatePropertyNameEv
	movl	%r12d, %edx
	movl	%eax, %ecx
	andl	$7, %edx
	andl	$7, %ecx
	cmpl	$3, %ecx
	je	.L2889
.L2827:
	cmpl	$2, %edx
	je	.L2880
.L2853:
	movl	$50, %r12d
	jmp	.L2825
	.p2align 4,,10
	.p2align 3
.L2814:
	cmpb	$5, %dl
	jne	.L2817
	leaq	-180(%rbp), %rdx
	leaq	-176(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movl	$0, -176(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseArgumentsEPNS0_23PreParserExpressionListEPbNS0_20ParsingArrowHeadFlagE
	movl	%r12d, %eax
	andl	$7, %eax
	cmpl	$3, %eax
	je	.L2890
	movl	$82, %r12d
	jmp	.L2825
	.p2align 4,,10
	.p2align 3
.L2817:
	testb	%cl, %cl
	jne	.L2891
	testb	%bl, %bl
	jne	.L2892
	movq	(%rdi), %rax
	movl	%r12d, %esi
	movl	$1, %ecx
	movq	%r13, %rdi
	movl	(%rax), %edx
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseTemplateLiteralENS0_19PreParserExpressionEib
	movl	%eax, %r12d
	jmp	.L2825
	.p2align 4,,10
	.p2align 3
.L2891:
	movq	%r13, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE34ParsePropertyOrPrivatePropertyNameEv
	movl	%r12d, %ecx
	movl	%eax, %edx
	andl	$7, %ecx
	andl	$7, %edx
	cmpl	$3, %edx
	je	.L2893
.L2835:
	cmpl	$2, %ecx
	jne	.L2853
.L2880:
	shrl	$4, %r12d
	andl	$15, %r12d
	cmpl	$1, %r12d
	sbbl	%r12d, %r12d
	andl	$-32, %r12d
	addl	$50, %r12d
	jmp	.L2825
	.p2align 4,,10
	.p2align 3
.L2890:
	movl	%r12d, %eax
	movl	$82, %r12d
	shrl	$4, %eax
	cmpb	$2, %al
	jne	.L2825
	movq	0(%r13), %r12
	movq	16(%r13), %rax
	movb	$1, 59(%rax)
	movq	%r12, %rdi
	orb	$2, 129(%r12)
	call	_ZN2v88internal5Scope19GetDeclarationScopeEv@PLT
	movzbl	129(%rax), %ecx
	movl	%ecx, %edx
	orl	$2, %edx
	movb	%dl, 129(%rax)
	andl	$1, %edx
	jne	.L2831
	movzbl	128(%rax), %edx
	cmpb	$1, %dl
	je	.L2831
	cmpb	$4, %dl
	je	.L2831
	orl	$6, %ecx
	movb	%cl, 129(%rax)
.L2831:
	orb	$64, 129(%r12)
	movq	8(%r12), %rax
	testq	%rax, %rax
	jne	.L2833
	jmp	.L2832
	.p2align 4,,10
	.p2align 3
.L2894:
	orl	$64, %edx
	movb	%dl, 129(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L2832
.L2833:
	movzbl	129(%rax), %edx
	testb	$64, %dl
	je	.L2894
.L2832:
	movl	$98, %r12d
	jmp	.L2825
	.p2align 4,,10
	.p2align 3
.L2883:
	shrl	$4, %eax
	cmpb	$8, %al
	jne	.L2821
	cmpl	$2, %ecx
	je	.L2895
	movl	$66, %r12d
	jmp	.L2823
	.p2align 4,,10
	.p2align 3
.L2889:
	shrl	$4, %eax
	cmpb	$8, %al
	jne	.L2827
	cmpl	$2, %edx
	je	.L2879
.L2851:
	movl	$66, %r12d
	jmp	.L2825
	.p2align 4,,10
	.p2align 3
.L2813:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r13), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %edx
	cmpb	$4, %dl
	je	.L2881
	movl	$1, %ecx
	movl	$1, %ebx
	jmp	.L2820
.L2888:
	call	_ZN2v88internal7Scanner4NextEv@PLT
.L2881:
	movq	%r13, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	movl	$1, %eax
.L2812:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2896
	addq	$176, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2884:
	.cfi_restore_state
	shrl	$4, %r12d
	movq	200(%r13), %rdi
	andl	$15, %r12d
	cmpl	$1, %r12d
	sbbl	%r12d, %r12d
	andl	$-32, %r12d
	addl	$50, %r12d
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$7, %al
	je	.L2824
.L2885:
	movq	%r13, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L2824
	.p2align 4,,10
	.p2align 3
.L2882:
	movl	%esi, %ecx
	andl	$7, %ecx
	cmpl	$3, %ecx
	jne	.L2804
	movq	(%rdi), %rcx
	cmpb	$95, 56(%rcx)
	jne	.L2804
	cmpb	$0, 76(%rax)
	jne	.L2804
	movl	4(%rcx), %r8d
	subl	(%rcx), %r8d
	cmpb	$0, 28(%rcx)
	movl	24(%rcx), %esi
	jne	.L2805
	sarl	%esi
.L2805:
	cmpl	%esi, %r8d
	jne	.L2804
	movq	144(%r13), %rax
	movq	0(%r13), %rdi
	leaq	-144(%rbp), %r12
	movq	$0, -64(%rbp)
	movq	%r12, 144(%r13)
	movq	%rax, -136(%rbp)
	leaq	176(%r13), %rax
	movq	%rax, -120(%rbp)
	movq	184(%r13), %rax
	subq	176(%r13), %rax
	movw	$1, -56(%rbp)
	sarq	$4, %rax
	movw	$258, -128(%rbp)
	movq	%rax, %xmm0
	movl	$4294967295, %eax
	movb	$1, -126(%rbp)
	movq	%rax, -80(%rbp)
	movzbl	129(%rdi), %eax
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -112(%rbp)
	movdqa	.LC12(%rip), %xmm0
	shrb	%al
	movb	$1, -72(%rbp)
	andl	$1, %eax
	movaps	%xmm0, -96(%rbp)
	orq	%rdi, %rax
	movl	$-1, -68(%rbp)
	movq	%rax, -176(%rbp)
	movq	16(%rdi), %rax
	movq	%r13, -144(%rbp)
	movq	%rax, -168(%rbp)
	movq	80(%rdi), %rax
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal5Scope15GetClosureScopeEv@PLT
	movq	%r13, %rdi
	movl	$1, %ecx
	leaq	-181(%rbp), %rdx
	movq	64(%rax), %rax
	leaq	-180(%rbp), %rsi
	movq	%rax, -152(%rbp)
	movq	-176(%rbp), %rax
	andq	$-2, %rax
	andb	$-7, 129(%rax)
	movl	$0, -180(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseArgumentsEPNS0_23PreParserExpressionListEPbNS0_20ParsingArrowHeadFlagE
	movq	200(%r13), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rax
	cmpb	$15, 56(%rax)
	jne	.L2806
	call	_ZN2v88internal21ArrowHeadParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE22ValidateAndCreateScopeEv
	leaq	-176(%rbp), %rdi
	movq	%rax, 248(%r13)
	movq	%rax, %rsi
	call	_ZN2v88internal5Scope8Snapshot8ReparentEPNS0_16DeclarationScopeE@PLT
	movl	$10, %eax
.L2807:
	movq	-176(%rbp), %rdx
	movq	%rdx, %rdi
	andq	$-2, %rdi
	je	.L2811
	andb	$1, %dl
	jne	.L2897
.L2811:
	movq	-112(%rbp), %rsi
	movq	-120(%rbp), %rdi
	movl	%eax, -196(%rbp)
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE6resizeEm
	movq	-144(%rbp), %rdx
	movq	-136(%rbp), %rcx
	movl	-196(%rbp), %eax
	movq	%rcx, 144(%rdx)
	jmp	.L2812
	.p2align 4,,10
	.p2align 3
.L2893:
	shrl	$4, %eax
	cmpb	$8, %al
	jne	.L2835
	cmpl	$2, %ecx
	jne	.L2851
.L2879:
	shrl	$4, %r12d
	andl	$15, %r12d
	cmpl	$1, %r12d
	sbbl	%r12d, %r12d
	andl	$-32, %r12d
	addl	$66, %r12d
	jmp	.L2825
	.p2align 4,,10
	.p2align 3
.L2886:
	testb	%bl, %bl
	movl	$2, %eax
	cmove	%r12d, %eax
	jmp	.L2812
.L2895:
	shrl	$4, %r12d
	andl	$15, %r12d
	cmpl	$1, %r12d
	sbbl	%r12d, %r12d
	andl	$-32, %r12d
	addl	$66, %r12d
	jmp	.L2823
.L2892:
	movq	(%rax), %rsi
	movl	$376, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	movl	$1, %eax
	jmp	.L2812
.L2896:
	call	__stack_chk_fail@PLT
.L2806:
	call	_ZN2v88internal21ArrowHeadParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE18ValidateExpressionEv
	movq	200(%r13), %rax
	movq	8(%rax), %rax
	cmpb	$5, 56(%rax)
	jbe	.L2808
	movl	$82, %eax
	jmp	.L2807
.L2897:
	movl	%eax, -196(%rbp)
	call	_ZN2v88internal5Scope14RecordEvalCallEv
	movl	-196(%rbp), %eax
	jmp	.L2811
.L2808:
	movq	-176(%rbp), %rax
	movq	%rax, %rdi
	andq	$-2, %rdi
	je	.L2809
	testb	$1, %al
	je	.L2809
	call	_ZN2v88internal5Scope14RecordEvalCallEv
.L2809:
	movq	-120(%rbp), %rdi
	movq	-112(%rbp), %rsi
	movl	$82, %r12d
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE6resizeEm
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	%rdx, 144(%rax)
	movq	200(%r13), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %edx
	jmp	.L2804
	.cfi_endproc
.LFE26048:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseLeftHandSideContinuationENS0_19PreParserExpressionE, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseLeftHandSideContinuationENS0_19PreParserExpressionE
	.section	.rodata._ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseVariableDeclarationsENS3_26VariableDeclarationContextEPNS3_24DeclarationParsingResultEPNS0_8ZoneListIPKNS0_12AstRawStringEEE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"destructuring"
.LC14:
	.string	"const"
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseVariableDeclarationsENS3_26VariableDeclarationContextEPNS3_24DeclarationParsingResultEPNS0_8ZoneListIPKNS0_12AstRawStringEEE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseVariableDeclarationsENS3_26VariableDeclarationContextEPNS3_24DeclarationParsingResultEPNS0_8ZoneListIPKNS0_12AstRawStringEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseVariableDeclarationsENS3_26VariableDeclarationContextEPNS3_24DeclarationParsingResultEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseVariableDeclarationsENS3_26VariableDeclarationContextEPNS3_24DeclarationParsingResultEPNS0_8ZoneListIPKNS0_12AstRawStringEEE, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseVariableDeclarationsENS3_26VariableDeclarationContextEPNS3_24DeclarationParsingResultEPNS0_8ZoneListIPKNS0_12AstRawStringEEE:
.LFB25525:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$200, %rsp
	movl	%esi, -196(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, 1(%rdx)
	movq	200(%rdi), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
	movl	%eax, 4(%rdx)
	movq	200(%rdi), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
	movl	%eax, 8(%rdx)
	movq	200(%rdi), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %eax
	cmpb	$98, %al
	je	.L2899
	cmpb	$104, %al
	je	.L2900
	cmpb	$80, %al
	je	.L3023
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3023:
	movb	$2, (%rdx)
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movzbl	(%r15), %eax
	cmpb	$2, %al
	setb	%dl
	addl	$4, %edx
.L2902:
	movq	144(%r12), %rcx
	movq	%r12, -176(%rbp)
	movb	%dl, -160(%rbp)
	movq	%rcx, -168(%rbp)
	testq	%rcx, %rcx
	je	.L3024
	cmpb	$0, 17(%rcx)
	movzbl	18(%rcx), %edx
	setne	-159(%rbp)
.L2975:
	movb	%dl, -158(%rbp)
	leaq	-176(%rbp), %rdx
	movq	(%r12), %r13
	movq	%rdx, 144(%r12)
	cmpb	$1, (%r15)
	movb	%al, -157(%rbp)
	movq	%rbx, -152(%rbp)
	jbe	.L2907
	movq	%r13, %rdi
	call	_ZN2v88internal5Scope19GetDeclarationScopeEv@PLT
	movq	%rax, %r13
.L2907:
	movq	200(%r12), %rdi
	movq	96(%r13), %rbx
	movq	%r13, -208(%rbp)
	movq	8(%rdi), %rax
	movl	(%rax), %ecx
	movzbl	56(%rax), %eax
	subl	$92, %eax
	movl	%ecx, -212(%rbp)
	movl	%ecx, -200(%rbp)
	cmpb	$9, %al
	ja	.L2908
.L3038:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movl	%eax, %r14d
	leal	-92(%r14), %eax
	cmpb	$3, %al
	ja	.L2909
	movq	%r12, %rdi
	call	_ZNK2v88internal9PreParser13GetIdentifierEv
	movq	%rax, %r11
	movl	%edx, %r13d
	cmpb	$3, %dl
	je	.L3025
.L2912:
	movq	(%r12), %rax
	testb	$1, 129(%rax)
	je	.L2919
	leal	-2(%r13), %eax
	cmpb	$1, %al
	jbe	.L3026
.L2919:
	movq	200(%r12), %rdi
	movq	8(%rdi), %rdx
	movzbl	56(%rdx), %eax
	cmpb	$17, %al
	je	.L3022
	cmpl	$2, -196(%rbp)
	je	.L3027
.L2931:
	cmpb	$0, (%r15)
	movq	144(%r12), %r14
	jne	.L3028
.L2933:
	movl	-200(%rbp), %edx
	movq	%r14, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE11NewVariableEPKNS0_12AstRawStringEi
	movzbl	%r13b, %edx
	sall	$4, %edx
	orl	$3, %edx
	movl	%edx, %r14d
.L2938:
	movq	200(%r12), %rdi
	movq	8(%rdi), %rcx
	movq	(%rdi), %rsi
	movzbl	56(%rcx), %eax
	cmpb	$17, %al
	je	.L3029
	cmpl	$2, -196(%rbp)
	je	.L3030
.L2954:
	movzbl	(%r15), %eax
	movl	4(%rsi), %esi
	cmpb	$1, %al
	je	.L2961
	testb	%r13b, %r13b
	je	.L2978
	cmpb	$1, %al
	movl	$-1, %r13d
	sbbl	%r8d, %r8d
	andl	$2, %r8d
.L2957:
	movq	-208(%rbp), %rax
	movq	96(%rax), %rdi
	cmpq	%rbx, %rdi
	je	.L2964
	.p2align 4,,10
	.p2align 3
.L2965:
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	movl	%esi, 36(%rax)
	movq	(%rbx), %rbx
	addq	$16, %rbx
	cmpq	%rbx, %rdi
	jne	.L2965
.L2964:
	movl	%r14d, -188(%rbp)
	movq	24(%r15), %rsi
	movl	%r8d, -184(%rbp)
	movl	%r13d, -180(%rbp)
	cmpq	32(%r15), %rsi
	je	.L2966
	movq	-188(%rbp), %rax
	movq	%rax, (%rsi)
	movl	-180(%rbp), %eax
	movl	%eax, 8(%rsi)
	addq	$12, 24(%r15)
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	cmpb	$30, 56(%rax)
	je	.L2967
.L2968:
	movq	(%rdi), %rax
	movl	-212(%rbp), %ecx
	movq	-168(%rbp), %rdx
	movl	4(%rax), %eax
	movl	%ecx, 48(%r15)
	movl	%eax, 52(%r15)
	movq	-176(%rbp), %rax
	movq	%rdx, 144(%rax)
.L2898:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3031
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3027:
	.cfi_restore_state
	cmpb	$62, %al
	jne	.L2932
.L3022:
	movq	144(%r12), %r14
	jmp	.L2933
	.p2align 4,,10
	.p2align 3
.L3030:
	cmpb	$62, %al
	jne	.L2958
	movl	4(%rsi), %esi
	xorl	%r8d, %r8d
	movl	$-1, %r13d
	jmp	.L2957
	.p2align 4,,10
	.p2align 3
.L2900:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movb	$1, (%r15)
	movl	$1, %eax
.L2904:
	movl	$5, %edx
	jmp	.L2902
	.p2align 4,,10
	.p2align 3
.L2899:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movb	$0, (%r15)
	xorl	%eax, %eax
	jmp	.L2904
	.p2align 4,,10
	.p2align 3
.L2958:
	cmpb	$92, %al
	jne	.L2954
	movl	4(%rcx), %eax
	subl	(%rcx), %eax
	cmpb	$0, 28(%rcx)
	movl	24(%rcx), %r8d
	je	.L3032
.L2959:
	cmpl	%r8d, %eax
	jne	.L2954
	movq	40(%r12), %rsi
	movq	56(%rsi), %rax
	movq	392(%rax), %rax
	movq	%rax, -224(%rbp)
	call	_ZNK2v88internal7Scanner10NextSymbolEPNS0_15AstValueFactoryE@PLT
	cmpq	%rax, -224(%rbp)
	movq	200(%r12), %rax
	je	.L3033
	movq	(%rax), %rsi
	jmp	.L2954
	.p2align 4,,10
	.p2align 3
.L3024:
	movb	$0, -159(%rbp)
	xorl	%edx, %edx
	jmp	.L2975
	.p2align 4,,10
	.p2align 3
.L2932:
	cmpb	$92, %al
	jne	.L2931
	movl	4(%rdx), %eax
	subl	(%rdx), %eax
	cmpb	$0, 28(%rdx)
	movl	24(%rdx), %ecx
	jne	.L2934
	sarl	%ecx
.L2934:
	cmpl	%ecx, %eax
	jne	.L2931
	movq	40(%r12), %rsi
	movq	%r11, -224(%rbp)
	movq	56(%rsi), %rax
	movq	392(%rax), %r14
	call	_ZNK2v88internal7Scanner10NextSymbolEPNS0_15AstValueFactoryE@PLT
	movq	-224(%rbp), %r11
	cmpq	%rax, %r14
	jne	.L2931
	jmp	.L3022
	.p2align 4,,10
	.p2align 3
.L3028:
	movq	(%r14), %rdi
	cmpb	$3, 16(%r14)
	movq	(%rdi), %rcx
	je	.L3034
	subq	$8, %rsp
	movzbl	19(%r14), %edx
	movq	%r11, %rsi
	movl	-200(%rbp), %r9d
	pushq	$0
	leaq	-188(%rbp), %r8
	movq	%r11, -224(%rbp)
	call	_ZN2v88internal9PreParser19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPNS0_5ScopeEPbiNS0_12VariableKindE
	cmpb	$0, -188(%rbp)
	popq	%rdx
	movq	(%r14), %rdi
	movq	-224(%rbp), %r11
	movq	%rax, %r8
	popq	%rcx
	je	.L2940
	movq	(%rdi), %rax
	cmpl	$8388607, 44(%rax)
	jg	.L3035
.L2940:
	movq	24(%r14), %rcx
	testq	%rcx, %rcx
	je	.L2941
	movslq	12(%rcx), %rax
	movl	8(%rcx), %edx
	cmpl	%edx, %eax
	jge	.L2942
	movq	(%rcx), %rdx
	leal	1(%rax), %esi
	movl	%esi, 12(%rcx)
	movq	%r11, (%rdx,%rax,8)
	movq	(%r14), %rdi
.L2941:
	cmpb	$5, 16(%r14)
	je	.L3036
	movq	16(%rdi), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jg	.L3037
.L2947:
	xorl	%r14d, %r14d
	jmp	.L2938
	.p2align 4,,10
	.p2align 3
.L2966:
	leaq	16(%r15), %rdi
	leaq	-188(%rbp), %rdx
	call	_ZNSt6vectorIN2v88internal10ParserBaseINS1_9PreParserEE24DeclarationParsingResult11DeclarationESaIS6_EE17_M_realloc_insertIJRKS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	cmpb	$30, 56(%rax)
	jne	.L2968
	.p2align 4,,10
	.p2align 3
.L2967:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	movl	(%rax), %ecx
	movzbl	56(%rax), %eax
	subl	$92, %eax
	movl	%ecx, -200(%rbp)
	cmpb	$9, %al
	jbe	.L3038
	.p2align 4,,10
	.p2align 3
.L2908:
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE19ParseBindingPatternEv
	movl	%eax, %r14d
	jmp	.L2938
	.p2align 4,,10
	.p2align 3
.L3029:
	movl	(%rsi), %eax
	movl	%eax, -232(%rbp)
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rax
	leaq	-144(%rbp), %r9
	movq	%r12, %rsi
	cmpl	$2, -196(%rbp)
	movq	%r9, %rdi
	movq	%r9, -224(%rbp)
	movq	8(%rax), %rax
	movl	(%rax), %r13d
	movzbl	256(%r12), %eax
	setne	256(%r12)
	movb	%al, -200(%rbp)
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPS3_NS0_15ExpressionScopeIS4_E9ScopeTypeE.constprop.0
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseAssignmentExpressionCoverGrammarEv
	movq	-224(%rbp), %r9
	movl	%eax, %r8d
	movl	-84(%rbp), %eax
	cmpl	%eax, -88(%rbp)
	jbe	.L3039
.L2953:
	movq	-112(%rbp), %rsi
	movq	-120(%rbp), %rdi
	movl	%r8d, -224(%rbp)
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE6resizeEm
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rsi
	movl	-224(%rbp), %r8d
	movq	%rsi, 144(%rax)
	movzbl	-200(%rbp), %eax
	movb	%al, 256(%r12)
	movq	200(%r12), %rax
	movq	(%rax), %rax
	movl	4(%rax), %esi
	movl	44(%r15), %eax
	cmpl	%eax, 40(%r15)
	jbe	.L2957
	movl	-232(%rbp), %eax
	movl	%esi, 44(%r15)
	movl	%eax, 40(%r15)
	movq	200(%r12), %rax
	movq	(%rax), %rax
	movl	4(%rax), %esi
	jmp	.L2957
	.p2align 4,,10
	.p2align 3
.L2909:
	movq	16(%r12), %rax
	movzbl	113(%r12), %r13d
	movq	48(%rax), %rdi
	testb	%r13b, %r13b
	je	.L3040
.L2913:
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movq	(%r12), %rcx
	testb	$1, 129(%rcx)
	je	.L2914
	cmpb	$96, %r14b
	je	.L2915
.L2916:
	movq	%r12, %rdi
	movl	$1, %r13d
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	movq	40(%r12), %rax
	movq	56(%rax), %rax
	movq	272(%rax), %r11
	jmp	.L2919
.L3025:
	movq	(%r12), %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal5Scope18ShouldBanArgumentsEv@PLT
	movq	-224(%rbp), %r11
	testb	%al, %al
	je	.L2912
	movl	$12, %esi
	movq	%r12, %rdi
	movl	$1, %r13d
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	movq	40(%r12), %rax
	movq	56(%rax), %rax
	movq	272(%rax), %r11
	jmp	.L2912
.L3034:
	subq	$8, %rsp
	movq	%r11, %rsi
	movl	-200(%rbp), %r9d
	leaq	-188(%rbp), %r8
	pushq	$1
	movl	$2, %edx
	movq	%r11, -224(%rbp)
	call	_ZN2v88internal9PreParser19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPNS0_5ScopeEPbiNS0_12VariableKindE
	popq	%rsi
	movl	24(%r14), %eax
	popq	%rdi
	cmpl	%eax, 20(%r14)
	jbe	.L2947
	cmpb	$0, -188(%rbp)
	jne	.L2947
	movq	-224(%rbp), %r11
	movq	16(%r11), %rdx
	cmpb	$0, 28(%r11)
	movl	%edx, %eax
	jne	.L2937
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
.L2937:
	movl	-200(%rbp), %ecx
	addl	%ecx, %eax
	movl	%ecx, 20(%r14)
	movl	%eax, 24(%r14)
	jmp	.L2947
.L2914:
	cmpb	$96, %r14b
	je	.L2915
	movzbl	133(%rax), %eax
	cmpb	$97, %r14b
	jne	.L2917
	subl	$12, %eax
	cmpb	$3, %al
	jbe	.L2916
.L2918:
	movq	144(%r12), %rax
	movzbl	16(%rax), %edx
	leal	-1(%rdx), %ecx
	cmpb	$2, %cl
	ja	.L2928
	movq	200(%r12), %rcx
	movq	(%rax), %r14
	movq	(%rcx), %rcx
	cmpb	$3, %dl
	je	.L3041
	movq	(%rcx), %rdx
	movq	%rdx, 232(%r14)
	movq	(%rax), %rax
	movl	$315, 240(%rax)
.L2928:
	movq	%r12, %rdi
	call	_ZNK2v88internal9PreParser13GetIdentifierEv
	movq	%rax, %r11
	movl	%edx, %r13d
	jmp	.L2912
.L3037:
	movzwl	40(%r8), %eax
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$1, %dl
	je	.L2947
	movq	16(%r8), %r14
	testq	%r14, %r14
	je	.L2949
	testb	$64, %ah
	je	.L3042
.L2949:
	orb	$64, %ah
	movw	%ax, 40(%r8)
	jmp	.L2947
.L3032:
	sarl	%r8d
	jmp	.L2959
.L2942:
	movq	136(%rdi), %r10
	leal	1(%rdx,%rdx), %r9d
	movslq	%r9d, %rsi
	movq	16(%r10), %rdi
	movq	24(%r10), %rdx
	salq	$3, %rsi
	subq	%rdi, %rdx
	cmpq	%rdx, %rsi
	ja	.L3043
	addq	%rdi, %rsi
	movq	%rsi, 16(%r10)
.L2944:
	movslq	12(%rcx), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L2945
	movq	(%rcx), %rsi
	movl	%r9d, -216(%rbp)
	movq	%r8, -240(%rbp)
	movq	%r11, -232(%rbp)
	movq	%rcx, -224(%rbp)
	call	memcpy@PLT
	movq	-224(%rbp), %rcx
	movl	-216(%rbp), %r9d
	movq	-240(%rbp), %r8
	movq	-232(%rbp), %r11
	movq	%rax, %rdi
	movslq	12(%rcx), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
.L2945:
	addl	$1, %eax
	movq	%rdi, (%rcx)
	movl	%r9d, 8(%rcx)
	movl	%eax, 12(%rcx)
	movq	%r11, (%rdi,%rdx)
	movq	(%r14), %rdi
	jmp	.L2941
.L3035:
	movl	$309, %esi
	movq	%r8, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	movq	(%r14), %rdi
	movq	-232(%rbp), %r8
	movq	-224(%rbp), %r11
	jmp	.L2940
.L2915:
	testb	%r13b, %r13b
	jne	.L2916
	movq	144(%r12), %rdx
	cmpb	$2, 16(%rdx)
	ja	.L2928
	movq	200(%r12), %rax
	movq	(%rax), %rsi
	movq	%rdx, %rax
.L2924:
	cmpb	$0, 72(%rdx)
	je	.L2928
	cmpb	$2, 16(%rax)
	je	.L3044
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L2924
	jmp	.L2928
	.p2align 4,,10
	.p2align 3
.L3036:
	movq	40(%rdi), %rax
	movq	56(%rax), %rax
	cmpq	%r11, 328(%rax)
	jne	.L2947
	movq	16(%r11), %rdx
	cmpb	$0, 28(%r11)
	movl	%edx, %eax
	jne	.L2948
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
.L2948:
	movl	-200(%rbp), %esi
	movl	$207, %edx
	addl	%esi, %eax
	salq	$32, %rax
	orq	%rax, %rsi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	jmp	.L2947
.L3039:
	xorl	%esi, %esi
	movq	%r9, %rdi
	movl	%r8d, -224(%rbp)
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE8ValidateEi.part.0
	movl	-224(%rbp), %r8d
	jmp	.L2953
.L3040:
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$9, %eax
	cmpb	$4, %al
	movq	16(%r12), %rax
	setbe	%r13b
	movq	48(%rax), %rdi
	jmp	.L2913
.L3044:
	movq	(%rsi), %rcx
	movl	$303, 84(%rax)
	movq	%rcx, 76(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L2924
	jmp	.L2928
	.p2align 4,,10
	.p2align 3
.L2961:
	testb	%r13b, %r13b
	leaq	.LC13(%rip), %rcx
	leaq	.LC14(%rip), %rax
	cmovne	%rax, %rcx
.L2962:
	movl	-200(%rbp), %eax
	salq	$32, %rsi
	movl	$48, %edx
	movq	%r12, %rdi
	orq	%rax, %rsi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc
.L2929:
	movq	-176(%rbp), %rax
	movq	-168(%rbp), %rdx
	movq	%rdx, 144(%rax)
	jmp	.L2898
.L2978:
	leaq	.LC13(%rip), %rcx
	jmp	.L2962
.L2917:
	subl	$97, %r14d
	cmpb	$4, %r14b
	jbe	.L2918
	jmp	.L2916
.L3026:
	movq	200(%r12), %rax
	movl	$295, %edx
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	jmp	.L2929
.L3042:
	movzwl	40(%r14), %edx
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L2949
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2950
	testb	$64, %dh
	je	.L3045
.L2950:
	orb	$64, %dh
	movw	%dx, 40(%r14)
	movzwl	40(%r8), %eax
	jmp	.L2949
.L3041:
	movq	(%r14), %rax
	movl	(%rcx), %esi
	movl	4(%rcx), %edx
	testb	$1, 129(%rax)
	je	.L2927
	movq	128(%r14), %rdi
	xorl	%r8d, %r8d
	movl	$315, %ecx
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r14), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L2928
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L2928
.L3033:
	movq	(%rax), %rax
	xorl	%r8d, %r8d
	movl	$-1, %r13d
	movl	4(%rax), %esi
	jmp	.L2957
.L2927:
	movq	224(%r14), %rax
	cmpl	%edx, %esi
	setbe	21(%rax)
	jmp	.L2928
.L3043:
	movq	%r10, %rdi
	movl	%r9d, -216(%rbp)
	movq	%rcx, -240(%rbp)
	movq	%r8, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-224(%rbp), %r11
	movq	-232(%rbp), %r8
	movq	-240(%rbp), %rcx
	movl	-216(%rbp), %r9d
	movq	%rax, %rdi
	jmp	.L2944
.L3045:
	movq	%r8, -224(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movzwl	40(%r14), %edx
	movq	-224(%rbp), %r8
	jmp	.L2950
.L3031:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25525:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseVariableDeclarationsENS3_26VariableDeclarationContextEPNS3_24DeclarationParsingResultEPNS0_8ZoneListIPKNS0_12AstRawStringEEE, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseVariableDeclarationsENS3_26VariableDeclarationContextEPNS3_24DeclarationParsingResultEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseVariableStatementENS3_26VariableDeclarationContextEPNS0_8ZoneListIPKNS0_12AstRawStringEEE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseVariableStatementENS3_26VariableDeclarationContextEPNS0_8ZoneListIPKNS0_12AstRawStringEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseVariableStatementENS3_26VariableDeclarationContextEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseVariableStatementENS3_26VariableDeclarationContextEPNS0_8ZoneListIPKNS0_12AstRawStringEEE, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseVariableStatementENS3_26VariableDeclarationContextEPNS0_8ZoneListIPKNS0_12AstRawStringEEE:
.LFB25138:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rdx, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -64(%rbp)
	movdqa	.LC4(%rip), %xmm0
	movq	$0, -48(%rbp)
	movups	%xmm0, -40(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseVariableDeclarationsENS3_26VariableDeclarationContextEPNS3_24DeclarationParsingResultEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %edx
	cmpb	$12, %dl
	jne	.L3047
	call	_ZN2v88internal7Scanner4NextEv@PLT
.L3048:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3051
	call	_ZdlPv@PLT
.L3051:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3059
	addq	$72, %rsp
	movl	$2, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3047:
	.cfi_restore_state
	cmpb	$0, 76(%rax)
	jne	.L3048
	subl	$12, %edx
	cmpb	$2, %dl
	jbe	.L3048
	movq	(%rdi), %rax
	cmpb	$96, 56(%rax)
	je	.L3049
.L3050:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L3048
.L3059:
	call	__stack_chk_fail@PLT
.L3049:
	movq	16(%r12), %rax
	movq	48(%rax), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$9, %eax
	cmpb	$4, %al
	ja	.L3057
	movq	200(%r12), %rdi
	jmp	.L3050
.L3057:
	movq	200(%r12), %rax
	movl	$16, %edx
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateE
	jmp	.L3048
	.cfi_endproc
.LFE25138:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseVariableStatementENS3_26VariableDeclarationContextEPNS0_8ZoneListIPKNS0_12AstRawStringEEE, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseVariableStatementENS3_26VariableDeclarationContextEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseClassPropertyDefinitionEPNS3_9ClassInfoEPNS3_17ParsePropertyInfoEb,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseClassPropertyDefinitionEPNS3_9ClassInfoEPNS3_17ParsePropertyInfoEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseClassPropertyDefinitionEPNS3_9ClassInfoEPNS3_17ParsePropertyInfoEb
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseClassPropertyDefinitionEPNS3_9ClassInfoEPNS3_17ParsePropertyInfoEb, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseClassPropertyDefinitionEPNS3_9ClassInfoEPNS3_17ParsePropertyInfoEb:
.LFB25820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	200(%rdi), %rdi
	movq	8(%rdi), %rax
	cmpb	$99, 56(%rax)
	movl	(%rax), %r15d
	je	.L3113
	movq	%rdx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE
	movl	%r15d, %ecx
.L3063:
	cmpb	$0, 21(%r13)
	jne	.L3066
	cmpb	$0, 35(%rbx)
	jne	.L3114
.L3066:
	movzbl	32(%rbx), %r8d
	cmpb	$9, %r8b
	ja	.L3067
	leaq	.L3069(%rip), %rsi
	movzbl	%r8b, %edx
	movslq	(%rsi,%rdx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseClassPropertyDefinitionEPNS3_9ClassInfoEPNS3_17ParsePropertyInfoEb,"aG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseClassPropertyDefinitionEPNS3_9ClassInfoEPNS3_17ParsePropertyInfoEb,comdat
	.align 4
	.align 4
.L3069:
	.long	.L3072-.L3069
	.long	.L3072-.L3069
	.long	.L3070-.L3069
	.long	.L3070-.L3069
	.long	.L3068-.L3069
	.long	.L3071-.L3069
	.long	.L3068-.L3069
	.long	.L3068-.L3069
	.long	.L3070-.L3069
	.long	.L3068-.L3069
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseClassPropertyDefinitionEPNS3_9ClassInfoEPNS3_17ParsePropertyInfoEb,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseClassPropertyDefinitionEPNS3_9ClassInfoEPNS3_17ParsePropertyInfoEb,comdat
	.p2align 4,,10
	.p2align 3
.L3068:
	cmpb	$0, 33(%rbx)
	movzbl	35(%rbx), %ecx
	movb	$6, 32(%rbx)
	je	.L3115
.L3073:
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseMemberInitializerEPNS3_9ClassInfoEib
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE15ExpectSemicolonEv
	leaq	-40(%rbp), %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3070:
	.cfi_restore_state
	movq	128(%r12), %rax
	movb	$1, (%rax)
	movb	$1, 2(%rax)
	movq	200(%r12), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L3098
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L3098:
	leaq	-40(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3114:
	.cfi_restore_state
	cmpb	$7, 16(%rbx)
	jne	.L3066
	movb	$1, 21(%r13)
	jmp	.L3066
	.p2align 4,,10
	.p2align 3
.L3072:
	cmpb	$0, 33(%rbx)
	je	.L3116
.L3093:
	movq	200(%r12), %rax
	testb	%r8b, %r8b
	movq	8(%rbx), %rsi
	setne	%r8b
	movq	16(%rbx), %rdx
	movq	(%rax), %rdi
	movq	(%r12), %rax
	pushq	$0
	movzbl	%r8b, %r8d
	leal	6(%r8), %r9d
	movzbl	129(%rax), %eax
	andl	$1, %eax
	pushq	%rax
	pushq	$3
	pushq	%rcx
.L3112:
	movq	(%rdi), %rcx
	movl	$1, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9PreParser20ParseFunctionLiteralENS0_19PreParserIdentifierENS0_7Scanner8LocationENS0_20FunctionNameValidityENS0_12FunctionKindEiNS0_18FunctionSyntaxKindENS0_12LanguageModeEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	addq	$32, %rsp
	leaq	-40(%rbp), %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3071:
	.cfi_restore_state
	cmpb	$0, 33(%rbx)
	movzbl	35(%rbx), %esi
	movl	28(%rbx), %eax
	je	.L3117
.L3078:
	movl	%eax, %edx
	leaq	_ZZN2v88internal10ParserBaseINS0_9PreParserEE19FunctionKindForImplEbNS_4base5FlagsINS0_17ParseFunctionFlagEiEEE14kFunctionKinds(%rip), %rdi
	andl	$1, %eax
	shrl	%edx
	leaq	(%rdi,%rax,2), %rax
	andl	$1, %edx
	movzbl	4(%rdx,%rax), %r9d
	testb	%sil, %sil
	jne	.L3091
.L3085:
	cmpb	$4, 16(%rbx)
	jne	.L3091
	cmpb	$1, %r14b
	movb	$1, 20(%r13)
	sbbl	%r9d, %r9d
	andl	$-3, %r9d
	addl	$5, %r9d
	.p2align 4,,10
	.p2align 3
.L3091:
	movq	200(%r12), %rax
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movq	(%rax), %rdi
	movq	(%r12), %rax
	pushq	$0
	movzbl	129(%rax), %eax
	andl	$1, %eax
	pushq	%rax
	pushq	$3
	pushq	%rcx
	jmp	.L3112
	.p2align 4,,10
	.p2align 3
.L3113:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	movl	(%rax), %ecx
	movzbl	56(%rax), %eax
	cmpb	$5, %al
	je	.L3118
	leal	-12(%rax), %edx
	movl	%ecx, -84(%rbp)
	cmpb	$1, %dl
	jbe	.L3102
	cmpb	$17, %al
	je	.L3102
	movb	$1, 35(%rbx)
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ParsePropertyEPNS3_17ParsePropertyInfoE
	movl	-84(%rbp), %ecx
	jmp	.L3063
	.p2align 4,,10
	.p2align 3
.L3102:
	movq	%r12, %rdi
	call	_ZNK2v88internal9PreParser13GetIdentifierEv
	movl	-84(%rbp), %ecx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rax, 8(%rbx)
	movzbl	-72(%rbp), %eax
	movb	%al, 16(%rbx)
	jmp	.L3063
	.p2align 4,,10
	.p2align 3
.L3117:
	movq	40(%r12), %rdx
	movq	8(%rbx), %rdi
	movq	56(%rdx), %rdx
	cmpq	400(%rdx), %rdi
	je	.L3119
	testb	%sil, %sil
	je	.L3081
	cmpq	416(%rdx), %rdi
	je	.L3082
	movl	%eax, %edx
	leaq	_ZZN2v88internal10ParserBaseINS0_9PreParserEE19FunctionKindForImplEbNS_4base5FlagsINS0_17ParseFunctionFlagEiEEE14kFunctionKinds(%rip), %rsi
	andl	$1, %eax
	shrl	%edx
	leaq	(%rsi,%rax,2), %rax
	andl	$1, %edx
	movzbl	4(%rdx,%rax), %r9d
	jmp	.L3091
	.p2align 4,,10
	.p2align 3
.L3116:
	movq	40(%r12), %rax
	movzbl	35(%rbx), %esi
	movq	8(%rbx), %rdx
	movq	56(%rax), %rax
	cmpq	400(%rax), %rdx
	je	.L3120
	testb	%sil, %sil
	je	.L3096
	cmpq	416(%rax), %rdx
	jne	.L3093
	movl	$162, %esi
	movq	%r12, %rdi
	movl	%ecx, -88(%rbp)
	movb	%r8b, -84(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	movzbl	-84(%rbp), %r8d
	movl	-88(%rbp), %ecx
	jmp	.L3093
	.p2align 4,,10
	.p2align 3
.L3115:
	movq	40(%r12), %rdx
	movq	8(%rbx), %rax
	movq	56(%rdx), %rdx
	testb	%cl, %cl
	je	.L3074
	cmpq	%rax, 416(%rdx)
	jne	.L3074
	movl	$162, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	movzbl	35(%rbx), %ecx
	jmp	.L3073
	.p2align 4,,10
	.p2align 3
.L3074:
	cmpq	%rax, 176(%rdx)
	je	.L3076
	cmpq	%rax, 400(%rdx)
	jne	.L3073
.L3076:
	movl	$38, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	movzbl	35(%rbx), %ecx
	jmp	.L3073
	.p2align 4,,10
	.p2align 3
.L3118:
	movb	$5, 32(%rbx)
	movq	%r12, %rdi
	movl	%ecx, -84(%rbp)
	call	_ZNK2v88internal9PreParser13GetIdentifierEv
	movl	-84(%rbp), %ecx
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)
	movq	%rax, 8(%rbx)
	movzbl	-56(%rbp), %eax
	movb	%al, 16(%rbx)
	jmp	.L3063
	.p2align 4,,10
	.p2align 3
.L3096:
	cmpq	176(%rax), %rdx
	jne	.L3093
	movl	$226, %esi
	movq	%r12, %rdi
	movl	%ecx, -88(%rbp)
	movb	%r8b, -84(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	movl	-88(%rbp), %ecx
	movzbl	-84(%rbp), %r8d
	jmp	.L3093
	.p2align 4,,10
	.p2align 3
.L3081:
	cmpq	176(%rdx), %rdi
	je	.L3084
	movl	%eax, %edx
	leaq	_ZZN2v88internal10ParserBaseINS0_9PreParserEE19FunctionKindForImplEbNS_4base5FlagsINS0_17ParseFunctionFlagEiEEE14kFunctionKinds(%rip), %rsi
	andl	$1, %eax
	shrl	%edx
	leaq	(%rsi,%rax,2), %rax
	andl	$1, %edx
	movzbl	4(%rdx,%rax), %r9d
	jmp	.L3085
	.p2align 4,,10
	.p2align 3
.L3084:
	testl	%eax, %eax
	jne	.L3121
	cmpb	$0, 20(%r13)
	jne	.L3122
	movb	$1, 20(%r13)
	movzbl	35(%rbx), %esi
	movl	28(%rbx), %eax
	jmp	.L3078
.L3120:
	movl	$229, %esi
	movq	%r12, %rdi
	movl	%ecx, -88(%rbp)
	movb	%r8b, -84(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	movzbl	-84(%rbp), %r8d
	movl	-88(%rbp), %ecx
	jmp	.L3093
.L3119:
	movl	%ecx, -84(%rbp)
	movl	$229, %esi
.L3111:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	movzbl	35(%rbx), %esi
	movl	28(%rbx), %eax
	movl	-84(%rbp), %ecx
	jmp	.L3078
.L3121:
	movl	$227, %esi
	testb	$1, %al
	jne	.L3088
	andl	$2, %eax
	cmpl	$1, %eax
	sbbl	%esi, %esi
	andl	$-2, %esi
	addl	$228, %esi
.L3088:
	movl	%ecx, -84(%rbp)
	jmp	.L3111
.L3082:
	movl	%ecx, -84(%rbp)
	movl	$162, %esi
	jmp	.L3111
.L3122:
	movl	%ecx, -84(%rbp)
	movl	$231, %esi
	jmp	.L3111
.L3067:
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25820:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseClassPropertyDefinitionEPNS3_9ClassInfoEPNS3_17ParsePropertyInfoEb, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseClassPropertyDefinitionEPNS3_9ClassInfoEPNS3_17ParsePropertyInfoEb
	.section	.rodata._ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseClassLiteralENS0_19PreParserIdentifierENS0_7Scanner8LocationEbi.str1.1,"aMS",@progbits,1
.LC15:
	.string	".class-field-"
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseClassLiteralENS0_19PreParserIdentifierENS0_7Scanner8LocationEbi,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseClassLiteralENS0_19PreParserIdentifierENS0_7Scanner8LocationEbi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseClassLiteralENS0_19PreParserIdentifierENS0_7Scanner8LocationEbi
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseClassLiteralENS0_19PreParserIdentifierENS0_7Scanner8LocationEbi, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseClassLiteralENS0_19PreParserIdentifierENS0_7Scanner8LocationEbi:
.LFB25509:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$312, %rsp
	movl	%r9d, -324(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	%dl, %dl
	je	.L3124
	movq	%rcx, %rsi
	testb	%r8b, %r8b
	jne	.L3197
	leal	-2(%rdx), %eax
	cmpb	$1, %al
	jbe	.L3198
.L3124:
	movq	136(%r15), %r14
	movq	(%r15), %rdx
	movq	16(%r14), %r12
	movq	24(%r14), %rax
	subq	%r12, %rax
	cmpq	$143, %rax
	jbe	.L3199
	leaq	144(%r12), %rax
	movq	%rax, 16(%r14)
.L3129:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10ClassScopeC1EPNS0_4ZoneEPNS0_5ScopeE@PLT
	movq	(%r15), %r14
	xorl	%edi, %edi
	movq	%r12, (%r15)
	pxor	%xmm0, %xmm0
	orb	$1, 129(%r12)
	testb	%bl, %bl
	movq	$0, -304(%rbp)
	movl	$0, -296(%rbp)
	movq	$0, -288(%rbp)
	movw	%di, -280(%rbp)
	movl	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	sete	-278(%rbp)
	jne	.L3200
.L3130:
	movq	200(%r15), %rdx
	movq	(%r15), %rax
	movq	(%rdx), %rdx
	movl	4(%rdx), %edx
	movl	%edx, 112(%rax)
	movq	200(%r15), %rdi
	movq	8(%rdi), %rax
	cmpb	$106, 56(%rax)
	je	.L3201
.L3131:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$8, %al
	jne	.L3202
.L3135:
	movq	200(%r15), %rdi
	testb	$7, -296(%rbp)
	movabsq	$38654705664, %r13
	setne	%bl
	movq	8(%rdi), %rax
	movzbl	%bl, %ebx
	movzbl	56(%rax), %eax
	cmpb	$13, %al
	jne	.L3136
	jmp	.L3137
	.p2align 4,,10
	.p2align 3
.L3171:
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L3142:
	cmpb	$0, -282(%rbp)
	jne	.L3145
	cmpb	$0, -205(%rbp)
	je	.L3145
	cmpb	$0, -207(%rbp)
	je	.L3145
	movb	$1, -282(%rbp)
	.p2align 4,,10
	.p2align 3
.L3145:
	cmpb	$0, -206(%rbp)
	jne	.L3203
	cmpb	$3, %al
	je	.L3204
.L3152:
	movq	8(%rdi), %rax
	movzbl	56(%rax), %eax
	cmpb	$13, %al
	je	.L3137
.L3136:
	cmpb	$12, %al
	je	.L3205
	pxor	%xmm1, %xmm1
	movq	%r15, %rdi
	leaq	-240(%rbp), %rdx
	movl	%ebx, %ecx
	leaq	-304(%rbp), %rsi
	movb	$0, -224(%rbp)
	movl	$1, -216(%rbp)
	movq	%r13, -212(%rbp)
	movb	$0, -204(%rbp)
	movaps	%xmm1, -240(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseClassPropertyDefinitionEPNS3_9ClassInfoEPNS3_17ParsePropertyInfoEb
	movq	200(%r15), %rdi
	movq	24(%rdi), %rax
	cmpb	$0, 48(%rax)
	jne	.L3156
	movzbl	-208(%rbp), %eax
	cmpb	$5, %al
	je	.L3170
	ja	.L3143
	testb	%al, %al
	je	.L3171
	cmpb	$1, %al
	jne	.L3144
	movl	$2, %eax
	jmp	.L3142
	.p2align 4,,10
	.p2align 3
.L3143:
	cmpb	$6, %al
	jne	.L3144
	movl	$3, %eax
	jmp	.L3142
	.p2align 4,,10
	.p2align 3
.L3170:
	xorl	%eax, %eax
	jmp	.L3142
	.p2align 4,,10
	.p2align 3
.L3205:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r15), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %eax
	cmpb	$13, %al
	jne	.L3136
	.p2align 4,,10
	.p2align 3
.L3137:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$13, %al
	jne	.L3206
.L3153:
	movq	200(%r15), %rax
	movq	%r12, %rdi
	movq	(%rax), %rax
	movl	4(%rax), %eax
	movl	%eax, 116(%r12)
	call	_ZN2v88internal10ClassScope28ResolvePrivateNamesPartiallyEv@PLT
	testq	%rax, %rax
	je	.L3154
	movq	8(%rax), %r8
	testb	$1, 5(%rax)
	je	.L3155
	movq	8(%r8), %r8
.L3155:
	movl	(%rax), %esi
	movq	128(%r15), %rdi
	movl	$258, %ecx
	leal	1(%rsi), %edx
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKNS0_12AstRawStringE@PLT
	movq	200(%r15), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	je	.L3207
.L3156:
	movl	$1, %eax
.L3141:
	movq	%r14, (%r15)
.L3127:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L3208
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3203:
	.cfi_restore_state
	cmpb	$3, %al
	leaq	CSWTCH.1484(%rip), %rsi
	leaq	-305(%rbp), %rcx
	movq	%r12, %rdi
	setne	%dl
	orb	%dl, -279(%rbp)
	movzbl	(%rsi,%rax), %edx
	movq	-232(%rbp), %rsi
	call	_ZN2v88internal10ClassScope18DeclarePrivateNameEPKNS0_12AstRawStringENS0_12VariableModeEPb@PLT
	cmpb	$0, -305(%rbp)
	je	.L3165
	movq	200(%r15), %rdi
	jmp	.L3152
	.p2align 4,,10
	.p2align 3
.L3197:
	movq	%rcx, %rdx
	movq	128(%rdi), %rdi
	xorl	%r8d, %r8d
	movl	$315, %ecx
	sarq	$32, %rdx
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r15), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L3196
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L3196:
	movl	$1, %eax
	jmp	.L3127
	.p2align 4,,10
	.p2align 3
.L3200:
	subq	$8, %rsp
	movq	(%r15), %rcx
	movq	%r13, %rsi
	movl	$-1, %r9d
	pushq	$0
	leaq	-240(%rbp), %r8
	movl	$1, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal9PreParser19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPNS0_5ScopeEPbiNS0_12VariableKindE
	popq	%rcx
	popq	%rsi
	jmp	.L3130
	.p2align 4,,10
	.p2align 3
.L3204:
	cmpb	$0, -207(%rbp)
	je	.L3152
	movl	-256(%rbp), %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-144(%rbp), %rdi
	leaq	.LC0(%rip), %rcx
	movl	$16, %edx
	movq	%rdi, -336(%rbp)
	leal	1(%rax), %r8d
	movq	40(%r15), %rax
	movl	%r8d, -256(%rbp)
	movq	%rax, -344(%rbp)
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movq	-336(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC15(%rip), %rcx
	movl	$13, %r8d
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	leaq	-160(%rbp), %rcx
	movq	%rcx, -176(%rbp)
	movq	(%rax), %rsi
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L3209
	movq	%rsi, -176(%rbp)
	movq	16(%rax), %rsi
	movq	%rsi, -160(%rbp)
.L3149:
	movq	8(%rax), %rsi
	movb	$0, 16(%rax)
	movq	%rsi, -168(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3150
	movq	%rcx, -336(%rbp)
	call	_ZdlPv@PLT
	movq	-336(%rbp), %rcx
.L3150:
	movq	-176(%rbp), %rsi
	movq	%rcx, -352(%rbp)
	movq	%rsi, %rdi
	movq	%rsi, -336(%rbp)
	call	strlen@PLT
	movq	-336(%rbp), %rsi
	movq	-344(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal15AstValueFactory24GetOneByteStringInternalENS0_6VectorIKhEE@PLT
	movq	-176(%rbp), %rdi
	movq	-352(%rbp), %rcx
	movq	%rax, %rsi
	cmpq	%rcx, %rdi
	je	.L3151
	movq	%rax, -336(%rbp)
	call	_ZdlPv@PLT
	movq	-336(%rbp), %rsi
.L3151:
	subq	$8, %rsp
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%r12, %rcx
	pushq	$0
	movl	$-1, %r9d
	leaq	-305(%rbp), %r8
	call	_ZN2v88internal9PreParser19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPNS0_5ScopeEPbiNS0_12VariableKindE
	popq	%rax
	movq	200(%r15), %rdi
	popq	%rdx
	jmp	.L3152
	.p2align 4,,10
	.p2align 3
.L3165:
	movq	128(%r15), %rdi
	movq	-232(%rbp), %r8
	xorl	%edx, %edx
	movl	$175, %ecx
	movl	$-1, %esi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKNS0_12AstRawStringE@PLT
	movq	200(%r15), %rdi
	movq	24(%rdi), %rax
	cmpb	$0, 48(%rax)
	jne	.L3152
	movl	$-1, 32(%rdi)
	movq	24(%rax), %rdx
	movb	$1, 48(%rax)
	movq	%rdx, 16(%rax)
	movb	$109, 96(%rdi)
	movb	$109, 176(%rdi)
	movb	$109, 256(%rdi)
	movq	200(%r15), %rdi
	jmp	.L3152
	.p2align 4,,10
	.p2align 3
.L3207:
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L3156
	.p2align 4,,10
	.p2align 3
.L3198:
	movl	$295, %edx
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	movl	$1, %eax
	jmp	.L3127
	.p2align 4,,10
	.p2align 3
.L3154:
	cmpb	$0, -279(%rbp)
	jne	.L3210
.L3157:
	cmpb	$0, -284(%rbp)
	je	.L3211
.L3158:
	cmpb	$0, -281(%rbp)
	jne	.L3212
.L3163:
	cmpb	$0, -280(%rbp)
	jne	.L3213
.L3164:
	movl	$2, %eax
	jmp	.L3141
	.p2align 4,,10
	.p2align 3
.L3201:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	leaq	-144(%rbp), %r13
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPS3_NS0_15ExpressionScopeIS4_E9ScopeTypeE.constprop.0
	movq	%r15, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParsePrimaryExpressionEv
	movl	%eax, %esi
	movq	200(%r15), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %eax
	cmpb	$3, %al
	jbe	.L3214
.L3132:
	cmpb	$5, %al
	jbe	.L3215
.L3133:
	movl	%esi, -296(%rbp)
	movl	-84(%rbp), %eax
	cmpl	%eax, -88(%rbp)
	jbe	.L3216
.L3134:
	movq	-120(%rbp), %rdi
	movq	-112(%rbp), %rsi
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE6resizeEm
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	%rdx, 144(%rax)
	movq	200(%r15), %rdi
	jmp	.L3131
	.p2align 4,,10
	.p2align 3
.L3199:
	movl	$144, %esi
	movq	%r14, %rdi
	movq	%rdx, -336(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-336(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L3129
	.p2align 4,,10
	.p2align 3
.L3202:
	movq	%r15, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L3135
	.p2align 4,,10
	.p2align 3
.L3209:
	movdqu	16(%rax), %xmm2
	movaps	%xmm2, -160(%rbp)
	jmp	.L3149
.L3206:
	movq	%r15, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L3153
.L3213:
	addl	$1, 208(%r15)
	jmp	.L3164
.L3212:
	addl	$1, 208(%r15)
	jmp	.L3163
.L3211:
	movq	136(%r15), %r12
	xorl	%r8d, %r8d
	movq	(%r15), %rdx
	testb	$7, -296(%rbp)
	movq	16(%r12), %rbx
	movq	24(%r12), %rax
	sete	%r8b
	addl	$3, %r8d
	subq	%rbx, %rax
	cmpq	$223, %rax
	jbe	.L3217
	leaq	224(%rbx), %rax
	movq	%rax, 16(%r12)
.L3161:
	movl	$2, %ecx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal16DeclarationScopeC1EPNS0_4ZoneEPNS0_5ScopeENS0_9ScopeTypeENS0_12FunctionKindE@PLT
	movq	16(%r15), %rax
	movq	%rbx, %rdi
	movb	$1, 59(%rax)
	movq	40(%r15), %rsi
	call	_ZN2v88internal16DeclarationScope31DeclareDefaultFunctionVariablesEPNS0_15AstValueFactoryE@PLT
	movl	-324(%rbp), %eax
	orb	$1, 129(%rbx)
	leaq	-240(%rbp), %rdx
	movl	%eax, 112(%rbx)
	movl	%eax, 116(%rbx)
	movq	(%r15), %rax
	movq	%r15, -240(%rbp)
	movq	%rax, -232(%rbp)
	leaq	16(%r15), %rax
	movq	%rax, -208(%rbp)
	movq	16(%r15), %rax
	movq	%rbx, (%r15)
	movq	$0, -224(%rbp)
	movl	$0, -216(%rbp)
	movq	%rax, -200(%rbp)
	movq	%rbx, -192(%rbp)
	movl	$0, -184(%rbp)
	movq	%rdx, 16(%r15)
	testq	%rax, %rax
	je	.L3162
	movzbl	57(%rax), %edx
	movb	$0, 57(%rax)
	movb	%dl, 58(%rax)
.L3162:
	addl	$1, 208(%r15)
	movq	%rax, 16(%r15)
	jmp	.L3158
.L3210:
	movq	40(%r15), %rsi
	movl	$-1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal10ClassScope20DeclareBrandVariableEPNS0_15AstValueFactoryEi@PLT
	jmp	.L3157
.L3216:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE8ValidateEi.part.0
	jmp	.L3134
.L3215:
	movq	%r15, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseLeftHandSideContinuationENS0_19PreParserExpressionE
	movl	%eax, %esi
	jmp	.L3133
.L3214:
	movq	%r15, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE35DoParseMemberExpressionContinuationENS0_19PreParserExpressionE
	movl	%eax, %esi
	movq	200(%r15), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %eax
	jmp	.L3132
.L3217:
	movl	$224, %esi
	movq	%r12, %rdi
	movq	%rdx, -344(%rbp)
	movl	%r8d, -336(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-336(%rbp), %r8d
	movq	-344(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L3161
.L3144:
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3208:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25509:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseClassLiteralENS0_19PreParserIdentifierENS0_7Scanner8LocationEbi, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseClassLiteralENS0_19PreParserIdentifierENS0_7Scanner8LocationEbi
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE21ParseClassDeclarationEPNS0_8ZoneListIPKNS0_12AstRawStringEEEb,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE21ParseClassDeclarationEPNS0_8ZoneListIPKNS0_12AstRawStringEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ParseClassDeclarationEPNS0_8ZoneListIPKNS0_12AstRawStringEEEb
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ParseClassDeclarationEPNS0_8ZoneListIPKNS0_12AstRawStringEEEb, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE21ParseClassDeclarationEPNS0_8ZoneListIPKNS0_12AstRawStringEEEb:
.LFB25137:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	200(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rcx), %rax
	movl	(%rax), %r15d
	movq	8(%rcx), %rax
	movzbl	56(%rax), %eax
	leal	-97(%rax), %edi
	cmpb	$4, %dil
	setbe	%r13b
	testb	%dl, %dl
	je	.L3219
	xorl	%esi, %esi
	cmpb	$106, %al
	je	.L3244
	cmpb	$8, %al
	je	.L3244
.L3219:
	movq	16(%rbx), %rax
	movq	48(%rax), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movq	200(%rbx), %rdi
	movzbl	133(%rax), %r12d
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$0, 113(%rbx)
	jne	.L3257
	leal	-92(%rax), %edx
	leal	-9(%r12), %ecx
	cmpb	$3, %dl
	ja	.L3258
.L3222:
	movq	%rbx, %rdi
	call	_ZNK2v88internal9PreParser13GetIdentifierEv
	movq	%rax, %r12
.L3227:
	movq	200(%rbx), %rcx
	movq	%r12, %rsi
	movb	%dl, %r14b
.L3220:
	movq	144(%rbx), %rdx
	movq	%rbx, -144(%rbp)
	movb	$0, -128(%rbp)
	movq	%rdx, -136(%rbp)
	testq	%rdx, %rdx
	je	.L3228
	cmpb	$0, 17(%rdx)
	movzbl	18(%rdx), %eax
	setne	-127(%rbp)
.L3242:
	movb	%al, -126(%rbp)
	leaq	-144(%rbp), %rax
	movq	%rax, 144(%rbx)
	leaq	176(%rbx), %rax
	movq	%rax, -120(%rbp)
	movq	184(%rbx), %rax
	subq	176(%rbx), %rax
	sarq	$4, %rax
	movq	%rax, %xmm0
	xorl	%eax, %eax
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movups	%xmm0, -88(%rbp)
	testq	%rdx, %rdx
	je	.L3231
	cmpb	$2, 16(%rdx)
	ja	.L3231
	movzbl	72(%rdx), %eax
.L3231:
	movb	%al, -72(%rbp)
	movl	%r13d, %r8d
	movq	%r14, %rdx
	movl	%r15d, %r9d
	movq	$0, -96(%rbp)
	andl	$1, %r8d
	movq	%rbx, %rdi
	movl	$-1, -88(%rbp)
	movl	$-1, -80(%rbp)
	movq	(%rcx), %rax
	movq	(%rax), %rcx
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseClassLiteralENS0_19PreParserIdentifierENS0_7Scanner8LocationEbi
	movl	-88(%rbp), %esi
	movl	-84(%rbp), %edx
	cmpl	%edx, %esi
	jbe	.L3259
.L3232:
	movq	(%rbx), %r14
	leaq	-145(%rbp), %rcx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal5Scope19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPbNS0_12VariableKindE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3260
	cmpq	(%rax), %r14
	je	.L3236
	movq	48(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L3261
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L3238:
	movabsq	$279172874239, %rsi
	movq	$0, 16(%rax)
	movq	%rsi, (%rax)
	movq	%r14, 24(%rax)
	movq	%r13, 8(%rax)
	movq	0(%r13), %rdx
	movq	96(%rdx), %rcx
	movq	%rax, (%rcx)
	addq	$16, %rax
	movq	%rax, 96(%rdx)
.L3236:
	movq	-120(%rbp), %rdi
	movq	-112(%rbp), %rsi
	movq	8(%rdi), %rcx
	movq	(%rdi), %rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	ja	.L3262
	jnb	.L3240
	salq	$4, %rsi
	addq	%rdx, %rsi
	cmpq	%rsi, %rcx
	je	.L3240
	movq	%rsi, 8(%rdi)
.L3240:
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	%rdx, 144(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3263
	addq	$120, %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3257:
	.cfi_restore_state
	leal	-92(%rax), %edx
	cmpb	$3, %dl
	jbe	.L3222
	movq	(%rbx), %rdx
	movzbl	129(%rdx), %edx
	andl	$1, %edx
	cmpb	$96, %al
	je	.L3223
.L3224:
	cmpb	$97, %al
	jne	.L3225
	subl	$12, %r12d
	cmpb	$3, %r12b
	jbe	.L3223
.L3256:
	testb	%dl, %dl
	je	.L3222
.L3223:
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	movq	40(%rbx), %rax
	movl	$1, %edx
	movq	56(%rax), %rax
	movq	272(%rax), %r12
	jmp	.L3227
	.p2align 4,,10
	.p2align 3
.L3244:
	xorl	%r12d, %r12d
	jmp	.L3220
	.p2align 4,,10
	.p2align 3
.L3262:
	subq	%rax, %rsi
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm
	jmp	.L3240
	.p2align 4,,10
	.p2align 3
.L3259:
	movq	-144(%rbp), %r13
	movl	-96(%rbp), %ecx
	xorl	%r8d, %r8d
	movq	128(%r13), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r13), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L3232
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L3232
	.p2align 4,,10
	.p2align 3
.L3260:
	movq	128(%rbx), %rax
	movb	$1, (%rax)
	movb	$1, 2(%rax)
	movq	200(%rbx), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L3235
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L3235:
	leaq	32(%r14), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal11VariableMap6LookupEPKNS0_12AstRawStringE@PLT
	jmp	.L3236
	.p2align 4,,10
	.p2align 3
.L3228:
	movb	$0, -127(%rbp)
	xorl	%eax, %eax
	jmp	.L3242
	.p2align 4,,10
	.p2align 3
.L3261:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L3238
	.p2align 4,,10
	.p2align 3
.L3258:
	movq	(%rbx), %rdx
	movzbl	129(%rdx), %edx
	andl	$1, %edx
	cmpb	$96, %al
	jne	.L3224
	cmpb	$4, %cl
	ja	.L3222
	jmp	.L3223
	.p2align 4,,10
	.p2align 3
.L3225:
	subl	$97, %eax
	cmpb	$4, %al
	jbe	.L3256
	jmp	.L3223
.L3263:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25137:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ParseClassDeclarationEPNS0_8ZoneListIPKNS0_12AstRawStringEEEb, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE21ParseClassDeclarationEPNS0_8ZoneListIPKNS0_12AstRawStringEEEb
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE22ParsePrimaryExpressionEv,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParsePrimaryExpressionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParsePrimaryExpressionEv
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParsePrimaryExpressionEv, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParsePrimaryExpressionEv:
.LFB26289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	120(%r12), %rax
	jnb	.L3265
	movq	200(%r12), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	je	.L3439
.L3266:
	movq	128(%r12), %rax
	movl	$257, %r8d
	movw	%r8w, (%rax)
.L3265:
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %ebx
	movl	(%rax), %r13d
	leal	-92(%rbx), %r15d
	cmpb	$9, %r15b
	jbe	.L3440
	leal	-84(%rbx), %eax
	cmpb	$6, %al
	jbe	.L3441
	cmpb	$107, %bl
	ja	.L3313
	leaq	.L3315(%rip), %rdx
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal10ParserBaseINS0_9PreParserEE22ParsePrimaryExpressionEv,"aG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParsePrimaryExpressionEv,comdat
	.align 4
	.align 4
.L3315:
	.long	.L3326-.L3315
	.long	.L3326-.L3315
	.long	.L3313-.L3315
	.long	.L3325-.L3315
	.long	.L3313-.L3315
	.long	.L3324-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3323-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3322-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3322-.L3315
	.long	.L3321-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3320-.L3315
	.long	.L3313-.L3315
	.long	.L3319-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3318-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3317-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3316-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3313-.L3315
	.long	.L3314-.L3315
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE22ParsePrimaryExpressionEv,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParsePrimaryExpressionEv,comdat
	.p2align 4,,10
	.p2align 3
.L3439:
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L3266
	.p2align 4,,10
	.p2align 3
.L3441:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$90, %al
	sete	%al
	movzbl	%al, %eax
	leal	2(%rax,%rax), %eax
.L3278:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L3442
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3440:
	.cfi_restore_state
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$95, %bl
	je	.L3268
	movq	200(%r12), %rax
	movl	$1, %edx
	movq	8(%rax), %rax
.L3269:
	cmpb	$15, 56(%rax)
	je	.L3271
	cmpb	$3, %r15b
	ja	.L3300
.L3272:
	movq	%r12, %rdi
	call	_ZNK2v88internal9PreParser13GetIdentifierEv
	movq	%rax, %rsi
	cmpb	$3, %dl
	je	.L3443
.L3301:
	movzbl	%dl, %ebx
	movl	%ebx, %eax
	sall	$4, %eax
	orl	$3, %eax
	movl	%eax, %ebx
.L3303:
	movq	144(%r12), %rdi
	movl	%r13d, %edx
	call	_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE11NewVariableEPKNS0_12AstRawStringEi
	movl	%ebx, %eax
	jmp	.L3278
.L3313:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	movl	$1, %eax
	jmp	.L3278
.L3326:
	xorl	%ecx, %ecx
	movl	%r13d, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseTemplateLiteralENS0_19PreParserExpressionEib
	jmp	.L3278
.L3314:
	cmpb	$0, 258(%r12)
	je	.L3313
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseImportExpressionsEv
	jmp	.L3278
.L3316:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	xorl	%r13d, %r13d
	xorl	%esi, %esi
	movl	$4294967295, %ecx
	movq	200(%r12), %rdi
	xorl	%r8d, %r8d
	movq	(%rdi), %rax
	movl	(%rax), %ebx
	movq	8(%rdi), %rax
	movzbl	56(%rax), %eax
	subl	$92, %eax
	cmpb	$9, %al
	jbe	.L3444
.L3353:
	movl	%ebx, %r9d
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseClassLiteralENS0_19PreParserIdentifierENS0_7Scanner8LocationEbi
	jmp	.L3278
.L3317:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseSuperExpressionEb
	jmp	.L3278
.L3318:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	(%r12), %rdi
	call	_ZN2v88internal5Scope15GetClosureScopeEv@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal5Scope16GetReceiverScopeEv@PLT
	movq	176(%rax), %rdx
	orw	$2048, 40(%rdx)
	cmpq	%rax, %rbx
	je	.L3445
	orb	$8, 132(%rbx)
	orw	$1024, 40(%rdx)
.L3435:
	movl	$2, %eax
	jmp	.L3278
.L3319:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE43ParseMemberWithPresentNewPrefixesExpressionEv
	jmp	.L3278
.L3320:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseFunctionExpressionEv
	jmp	.L3278
.L3321:
	cmpb	$0, 257(%r12)
	jne	.L3365
	cmpq	$0, 24(%r12)
	je	.L3313
.L3365:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE16ParseV8IntrinsicEv
	jmp	.L3278
.L3322:
	call	_ZN2v88internal7Scanner17ScanRegExpPatternEv@PLT
	movq	200(%r12), %rdi
	testb	%al, %al
	jne	.L3332
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movl	$329, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	movl	$1, %eax
	jmp	.L3278
.L3323:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE18ParseObjectLiteralEv
	jmp	.L3278
.L3324:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	cmpb	$6, 56(%rax)
	je	.L3446
	movq	(%r12), %rdi
	leaq	-160(%rbp), %r14
	movzbl	129(%rdi), %eax
	shrb	%al
	andl	$1, %eax
	orq	%rdi, %rax
	movq	%rax, -192(%rbp)
	movq	16(%rdi), %rax
	movq	%rax, -184(%rbp)
	movq	80(%rdi), %rax
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal5Scope15GetClosureScopeEv@PLT
	movl	$257, %ecx
	movq	64(%rax), %rax
	movq	%rax, -168(%rbp)
	movq	-192(%rbp), %rax
	andq	$-2, %rax
	andb	$-7, 129(%rax)
	leaq	176(%r12), %rax
	movq	144(%r12), %rdx
	movq	%rax, -136(%rbp)
	movq	184(%r12), %rax
	subq	176(%r12), %rax
	movq	%r12, -160(%rbp)
	sarq	$4, %rax
	movq	%rdx, -152(%rbp)
	movq	%rax, %xmm0
	movw	%cx, -144(%rbp)
	xorl	%eax, %eax
	punpcklqdq	%xmm0, %xmm0
	movb	$1, -142(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movq	%r14, 144(%r12)
	movups	%xmm0, -104(%rbp)
	testq	%rdx, %rdx
	je	.L3339
	cmpb	$2, 16(%rdx)
	ja	.L3339
	movzbl	72(%rdx), %eax
.L3339:
	movq	200(%r12), %rdi
	movb	%al, -88(%rbp)
	movl	$1, %eax
	movq	$0, -112(%rbp)
	movl	$-1, -104(%rbp)
	movl	$-1, -96(%rbp)
	movl	$-1, -84(%rbp)
	movq	$0, -80(%rbp)
	movw	%ax, -72(%rbp)
	movq	8(%rdi), %rax
	movzbl	56(%rax), %eax
	cmpb	$73, %al
	je	.L3346
	cmpb	$95, %al
	je	.L3447
.L3341:
	movzbl	256(%r12), %r13d
	movq	%r12, %rdi
	movb	$1, 256(%r12)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE27ParseExpressionCoverGrammarEv
	movl	$6, %esi
	movq	%r12, %rdi
	orl	$8, %eax
	movl	%eax, %ebx
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE6ExpectENS0_5Token5ValueE
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	cmpb	$15, 56(%rax)
	je	.L3448
	movq	-160(%rbp), %r14
	movl	$4294967295, %eax
	movq	%rax, 232(%r14)
	movl	$0, 240(%r14)
	movl	-104(%rbp), %esi
	movl	-100(%rbp), %edx
	cmpl	%edx, %esi
	jbe	.L3449
.L3350:
	movq	-136(%rbp), %rdi
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rax
	movq	(%rdi), %r15
	movq	%rsi, %r14
	salq	$4, %r14
	salq	$4, %rax
	addq	%r15, %r14
	addq	%rax, %r15
	cmpq	%r15, %r14
	je	.L3349
	.p2align 4,,10
	.p2align 3
.L3352:
	movq	-160(%rbp), %rax
	movq	(%r14), %rsi
	addq	$16, %r14
	movq	(%rax), %rdi
	call	_ZN2v88internal5Scope13AddUnresolvedEPNS0_13VariableProxyE@PLT
	cmpq	%r14, %r15
	jne	.L3352
	movq	-136(%rbp), %rdi
	movq	-128(%rbp), %rsi
.L3349:
	movb	%r13b, 256(%r12)
	movl	%ebx, -200(%rbp)
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE6resizeEm
	movq	-160(%rbp), %rdx
	movq	-152(%rbp), %rcx
	movl	-200(%rbp), %eax
	movq	%rcx, 144(%rdx)
	movq	-192(%rbp), %rdx
	movq	%rdx, %rdi
	andq	$-2, %rdi
	je	.L3278
	andl	$1, %edx
	je	.L3278
	call	_ZN2v88internal5Scope14RecordEvalCallEv
	movl	-200(%rbp), %eax
	jmp	.L3278
.L3325:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE17ParseArrayLiteralEv
	jmp	.L3278
	.p2align 4,,10
	.p2align 3
.L3268:
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	cmpb	$0, 76(%rax)
	je	.L3270
.L3437:
	cmpb	$15, 56(%rax)
	jne	.L3272
.L3373:
	movl	$3, %r15d
	movl	$1, %edx
	.p2align 4,,10
	.p2align 3
.L3271:
	leaq	176(%r12), %rax
	movq	144(%r12), %rcx
	movl	$257, %edi
	leaq	-160(%rbp), %r14
	movq	%rax, -136(%rbp)
	movq	184(%r12), %rax
	subq	176(%r12), %rax
	movq	%r12, -160(%rbp)
	sarq	$4, %rax
	movq	%rcx, -152(%rbp)
	movq	%rax, %xmm0
	movb	%dl, -144(%rbp)
	movl	$1, %eax
	punpcklqdq	%xmm0, %xmm0
	movw	%di, -143(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movq	%r14, 144(%r12)
	movups	%xmm0, -104(%rbp)
	cmpb	$2, %dl
	je	.L3282
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L3282
	cmpb	$2, 16(%rcx)
	ja	.L3282
	movzbl	72(%rcx), %eax
.L3282:
	movl	$1, %esi
	movb	%al, -88(%rbp)
	movq	$0, -112(%rbp)
	movl	$-1, -104(%rbp)
	movl	$-1, -96(%rbp)
	movl	$-1, -84(%rbp)
	movq	$0, -80(%rbp)
	movw	%si, -72(%rbp)
	cmpb	$3, %r15b
	ja	.L3283
	movq	%r12, %rdi
	call	_ZNK2v88internal9PreParser13GetIdentifierEv
	movq	%rax, %r15
	movl	%edx, %ecx
	cmpb	$3, %dl
	je	.L3450
.L3286:
	movzbl	%cl, %eax
	subl	$2, %ecx
	movq	144(%r12), %rdi
	sall	$4, %eax
	orl	$3, %eax
	movl	%eax, %ebx
	cmpb	$1, %cl
	jbe	.L3451
.L3293:
	movl	%r13d, %edx
	movq	%r15, %rsi
	call	_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE11NewVariableEPKNS0_12AstRawStringEi
	movq	-128(%rbp), %rax
	movl	-120(%rbp), %ecx
	subl	%eax, %ecx
	testl	%ecx, %ecx
	jle	.L3298
	movq	200(%r12), %rdx
	leal	-1(%rcx), %esi
	movslq	%ecx, %rcx
	subq	$2, %rcx
	movq	8(%rdx), %rdx
	movl	(%rdx), %edi
	movslq	%esi, %rdx
	movl	%esi, %esi
	subq	%rsi, %rcx
	jmp	.L3299
	.p2align 4,,10
	.p2align 3
.L3452:
	subq	$1, %rdx
	movl	%edi, 8(%rax)
	cmpq	%rdx, %rcx
	je	.L3298
	movq	-128(%rbp), %rax
.L3299:
	movq	-136(%rbp), %rsi
	addq	%rdx, %rax
	salq	$4, %rax
	addq	(%rsi), %rax
	cmpl	$-1, 8(%rax)
	je	.L3452
.L3298:
	movq	%r14, %rdi
	call	_ZN2v88internal21ArrowHeadParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE22ValidateAndCreateScopeEv
	movq	-128(%rbp), %rsi
	movq	-136(%rbp), %rdi
	movl	%ebx, -200(%rbp)
	movq	%rax, 248(%r12)
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE6resizeEm
	movq	-160(%rbp), %rdx
	movq	-152(%rbp), %rcx
	movl	-200(%rbp), %eax
	movq	%rcx, 144(%rdx)
	jmp	.L3278
	.p2align 4,,10
	.p2align 3
.L3300:
	movq	16(%r12), %rax
	movzbl	113(%r12), %r14d
	movq	48(%rax), %rdi
	testb	%r14b, %r14b
	je	.L3453
.L3304:
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movq	(%r12), %rdx
	testb	$1, 129(%rdx)
	jne	.L3305
	cmpb	$96, %bl
	je	.L3306
	movzbl	133(%rax), %eax
	cmpb	$97, %bl
	jne	.L3307
	subl	$12, %eax
	cmpb	$3, %al
	jbe	.L3308
.L3309:
	movq	200(%r12), %rax
	movq	144(%r12), %rdi
	movl	$315, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE30RecordStrictModeParameterErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE
.L3433:
	movq	%r12, %rdi
	call	_ZNK2v88internal9PreParser13GetIdentifierEv
	movzbl	%dl, %ebx
	movq	%rax, %rsi
	sall	$4, %ebx
	orl	$3, %ebx
	jmp	.L3303
	.p2align 4,,10
	.p2align 3
.L3443:
	movq	(%r12), %rdi
	movq	%rdx, -208(%rbp)
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal5Scope18ShouldBanArgumentsEv@PLT
	movq	-200(%rbp), %rsi
	movq	-208(%rbp), %rdx
	testb	%al, %al
	je	.L3301
	movl	$12, %esi
	movq	%r12, %rdi
	movl	$19, %ebx
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	movq	40(%r12), %rax
	movq	56(%rax), %rax
	movq	272(%rax), %rsi
	jmp	.L3303
.L3307:
	subl	$97, %ebx
	cmpb	$4, %bl
	jbe	.L3309
	.p2align 4,,10
	.p2align 3
.L3308:
	movq	%r12, %rdi
	movl	$19, %ebx
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	movq	40(%r12), %rax
	movq	56(%rax), %rax
	movq	272(%rax), %rsi
	jmp	.L3303
	.p2align 4,,10
	.p2align 3
.L3332:
	call	_ZN2v88internal7Scanner15ScanRegExpFlagsEv@PLT
	movq	200(%r12), %rdi
	testb	%al, %al
	jne	.L3334
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movl	$272, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	movl	$1, %eax
	jmp	.L3278
	.p2align 4,,10
	.p2align 3
.L3305:
	cmpb	$96, %bl
	jne	.L3308
.L3306:
	testb	%r14b, %r14b
	jne	.L3308
	movq	144(%r12), %rdi
	cmpb	$2, 16(%rdi)
	ja	.L3433
	movq	200(%r12), %rax
	movl	$303, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE31RecordAsyncArrowParametersErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE.part.0
	jmp	.L3433
.L3447:
	call	_ZN2v88internal7Scanner9PeekAheadEv@PLT
	cmpb	$73, %al
	jne	.L3341
	.p2align 4,,10
	.p2align 3
.L3346:
	movzbl	_ZN2v88internal13FLAG_max_lazyE(%rip), %eax
	movq	16(%r12), %rdx
	xorl	$1, %eax
	movb	%al, 57(%rdx)
	jmp	.L3341
	.p2align 4,,10
	.p2align 3
.L3444:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movl	%eax, %r14d
	leal	-92(%r14), %eax
	cmpb	$3, %al
	ja	.L3354
	movq	%r12, %rdi
	call	_ZNK2v88internal9PreParser13GetIdentifierEv
	movq	%rax, %rsi
	movl	%edx, %r14d
	cmpb	$3, %dl
	je	.L3454
.L3357:
	movq	200(%r12), %rax
	xorl	%r8d, %r8d
	movb	%r14b, %r13b
	movq	(%rax), %rax
	movq	(%rax), %rcx
	movzbl	56(%rax), %eax
	subl	$97, %eax
	cmpb	$4, %al
	setbe	%r8b
	jmp	.L3353
	.p2align 4,,10
	.p2align 3
.L3445:
	movq	144(%r12), %rax
	.p2align 4,,10
	.p2align 3
.L3331:
	movzbl	16(%rax), %ecx
	leal	-1(%rcx), %edx
	cmpb	$1, %dl
	jbe	.L3455
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L3331
	jmp	.L3435
	.p2align 4,,10
	.p2align 3
.L3455:
	movb	$1, 89(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L3331
	jmp	.L3435
	.p2align 4,,10
	.p2align 3
.L3448:
	movq	%r14, %rdi
	call	_ZN2v88internal21ArrowHeadParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE22ValidateAndCreateScopeEv
	leaq	-192(%rbp), %rdi
	movq	%rax, 248(%r12)
	movq	%rax, %rsi
	call	_ZN2v88internal5Scope8Snapshot8ReparentEPNS0_16DeclarationScopeE@PLT
	movq	-136(%rbp), %rdi
	movq	-128(%rbp), %rsi
	jmp	.L3349
.L3453:
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$9, %eax
	cmpb	$4, %al
	movq	16(%r12), %rax
	setbe	%r14b
	movq	48(%rax), %rdi
	jmp	.L3304
.L3334:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	jmp	.L3435
.L3451:
	movzbl	16(%rdi), %eax
	leal	-1(%rax), %edx
	cmpb	$2, %dl
	ja	.L3293
	movq	200(%r12), %rdx
	movq	(%rdi), %r8
	movq	(%rdx), %rdx
	movl	4(%rdx), %edx
	cmpb	$3, %al
	je	.L3456
	movl	%r13d, 232(%r8)
	movl	%edx, 236(%r8)
	movq	(%rdi), %rax
	movl	$295, 240(%rax)
	movq	144(%r12), %rdi
	jmp	.L3293
.L3449:
	movq	128(%r14), %rdi
	movl	-112(%rbp), %ecx
	xorl	%r8d, %r8d
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r14), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L3350
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L3350
.L3446:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	cmpb	$15, 56(%rax)
	je	.L3338
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
.L3338:
	movq	136(%r12), %r13
	movq	(%r12), %rdx
	movq	16(%r13), %rbx
	movq	24(%r13), %rax
	subq	%rbx, %rax
	cmpq	$223, %rax
	jbe	.L3457
	leaq	224(%rbx), %rax
	movq	%rax, 16(%r13)
.L3343:
	movl	$8, %r8d
	movl	$2, %ecx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal16DeclarationScopeC1EPNS0_4ZoneEPNS0_5ScopeENS0_9ScopeTypeENS0_12FunctionKindE@PLT
	movq	16(%r12), %rax
	movb	$1, 59(%rax)
	movl	$10, %eax
	movq	%rbx, 248(%r12)
	jmp	.L3278
.L3283:
	movq	16(%r12), %rax
	movzbl	113(%r12), %r15d
	movq	48(%rax), %rdi
	testb	%r15b, %r15b
	je	.L3458
.L3287:
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movq	(%r12), %rdx
	testb	$1, 129(%rdx)
	je	.L3288
	cmpb	$96, %bl
	je	.L3289
.L3290:
	movq	%r12, %rdi
	movl	$19, %ebx
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	movq	40(%r12), %rax
	movq	144(%r12), %rdi
	movq	56(%rax), %rax
	movq	272(%rax), %r15
	jmp	.L3293
.L3270:
	movq	(%rdi), %rdx
	movl	4(%rdx), %ecx
	subl	(%rdx), %ecx
	cmpb	$90, 56(%rdx)
	jne	.L3273
	subl	$2, %ecx
.L3273:
	cmpb	$0, 28(%rdx)
	movl	24(%rdx), %edx
	jne	.L3275
	sarl	%edx
.L3275:
	cmpl	%edx, %ecx
	jne	.L3437
	movzbl	56(%rax), %eax
	cmpb	$73, %al
	je	.L3459
	leal	-92(%rax), %edx
	cmpb	$9, %dl
	jbe	.L3279
	cmpb	$15, %al
	jne	.L3272
	jmp	.L3373
	.p2align 4,,10
	.p2align 3
.L3450:
	movq	(%r12), %rdi
	movb	%dl, -200(%rbp)
	call	_ZN2v88internal5Scope18ShouldBanArgumentsEv@PLT
	movzbl	-200(%rbp), %ecx
	testb	%al, %al
	je	.L3286
	movl	$12, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	movq	40(%r12), %rax
	movl	$1, %ecx
	movq	56(%rax), %rax
	movq	272(%rax), %r15
	jmp	.L3286
.L3456:
	movq	(%r8), %rax
	testb	$1, 129(%rax)
	je	.L3297
	salq	$32, %rdx
	movl	%r13d, %esi
	movq	%r8, %rdi
	orq	%rdx, %rsi
	movl	$295, %edx
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	movq	144(%r12), %rdi
	jmp	.L3293
.L3288:
	cmpb	$96, %bl
	je	.L3289
	movzbl	133(%rax), %eax
	cmpb	$97, %bl
	jne	.L3291
	subl	$12, %eax
	cmpb	$3, %al
	jbe	.L3290
.L3292:
	movq	200(%r12), %rax
	movq	144(%r12), %rdi
	movl	$315, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE30RecordStrictModeParameterErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE
.L3432:
	movq	%r12, %rdi
	call	_ZNK2v88internal9PreParser13GetIdentifierEv
	movq	%rax, %r15
	movl	%edx, %ecx
	jmp	.L3286
.L3354:
	movzbl	113(%r12), %r15d
	testb	%r15b, %r15b
	je	.L3460
.L3358:
	movq	16(%r12), %rax
	movq	48(%rax), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movq	(%r12), %rdx
	testb	$1, 129(%rdx)
	jne	.L3359
	cmpb	$96, %r14b
	je	.L3360
	movzbl	133(%rax), %eax
	cmpb	$97, %r14b
	jne	.L3361
	subl	$12, %eax
	cmpb	$3, %al
	jbe	.L3362
.L3363:
	movq	200(%r12), %rax
	movq	144(%r12), %rdi
	movl	$315, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE30RecordStrictModeParameterErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE
.L3434:
	movq	%r12, %rdi
	call	_ZNK2v88internal9PreParser13GetIdentifierEv
	movq	%rax, %rsi
	movl	%edx, %r14d
	jmp	.L3357
.L3458:
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$9, %eax
	cmpb	$4, %al
	movq	16(%r12), %rax
	setbe	%r15b
	movq	48(%rax), %rdi
	jmp	.L3287
.L3454:
	movq	(%r12), %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal5Scope18ShouldBanArgumentsEv@PLT
	movq	-200(%rbp), %rsi
	testb	%al, %al
	je	.L3357
	movl	$12, %esi
	movq	%r12, %rdi
	movl	$1, %r14d
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	movq	40(%r12), %rax
	movq	56(%rax), %rax
	movq	272(%rax), %rsi
	jmp	.L3357
.L3297:
	movq	224(%r8), %rax
	cmpl	%edx, %r13d
	setbe	21(%rax)
	movq	144(%r12), %rdi
	jmp	.L3293
.L3361:
	subl	$97, %r14d
	cmpb	$4, %r14b
	jbe	.L3363
.L3362:
	movq	%r12, %rdi
	movl	$1, %r14d
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	movq	40(%r12), %rax
	movq	56(%rax), %rax
	movq	272(%rax), %rsi
	jmp	.L3357
.L3289:
	testb	%r15b, %r15b
	jne	.L3290
	movq	144(%r12), %rdi
	cmpb	$2, 16(%rdi)
	ja	.L3432
	movq	200(%r12), %rax
	movl	$303, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE31RecordAsyncArrowParametersErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE.part.0
	jmp	.L3432
.L3291:
	subl	$97, %ebx
	cmpb	$4, %bl
	jbe	.L3292
	jmp	.L3290
.L3359:
	cmpb	$96, %r14b
	jne	.L3362
.L3360:
	testb	%r15b, %r15b
	jne	.L3362
	movq	144(%r12), %rdi
	cmpb	$2, 16(%rdi)
	ja	.L3434
	movq	200(%r12), %rax
	movl	$303, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal15ExpressionScopeINS0_11ParserTypesINS0_9PreParserEEEE31RecordAsyncArrowParametersErrorERKNS0_7Scanner8LocationENS0_15MessageTemplateE.part.0
	jmp	.L3434
.L3460:
	movq	16(%r12), %rax
	movq	48(%rax), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$9, %eax
	cmpb	$4, %al
	setbe	%r15b
	jmp	.L3358
.L3442:
	call	__stack_chk_fail@PLT
.L3457:
	movl	$224, %esi
	movq	%r13, %rdi
	movq	%rdx, -200(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-200(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L3343
.L3279:
	call	_ZN2v88internal7Scanner9PeekAheadEv@PLT
	cmpb	$15, %al
	je	.L3280
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	jmp	.L3437
.L3459:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseAsyncFunctionLiteralEv
	jmp	.L3278
.L3280:
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movl	%eax, %ebx
	movq	200(%r12), %rax
	leal	-92(%rbx), %r15d
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	movl	(%rdx), %r13d
	movl	$2, %edx
	jmp	.L3269
	.cfi_endproc
.LFE26289:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParsePrimaryExpressionEv, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParsePrimaryExpressionEv
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseBinaryContinuationENS0_19PreParserExpressionEii,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseBinaryContinuationENS0_19PreParserExpressionEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseBinaryContinuationENS0_19PreParserExpressionEii
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseBinaryContinuationENS0_19PreParserExpressionEii, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseBinaryContinuationENS0_19PreParserExpressionEii:
.LFB26123:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	_ZN2v88internal5Token11precedence_E(%rip), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$24, %rsp
	movq	200(%rdi), %rdi
	movq	8(%rdi), %rdx
	movzbl	56(%rdx), %ecx
	movzbl	256(%r15), %edx
	imulq	$114, %rdx, %rdx
	addq	%r14, %rdx
	movsbl	(%rdx,%rcx), %ecx
	leal	-1(%rbx), %edx
	cmpl	%ecx, %ebx
	je	.L3480
	.p2align 4,,10
	.p2align 3
.L3463:
	cmpl	%edx, %r12d
	jg	.L3481
	movl	%edx, %ebx
.L3483:
	leal	-1(%rbx), %edx
	cmpl	%ecx, %ebx
	jne	.L3463
.L3480:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	xorl	%r13d, %r13d
	cmpb	$43, %al
	movq	200(%r15), %rax
	setne	%r13b
	movq	8(%rax), %rax
	addl	%ebx, %r13d
	movzbl	56(%rax), %edx
	leal	-44(%rdx), %eax
	cmpb	$8, %al
	ja	.L3465
	movq	%r15, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseUnaryOrPrefixExpressionEv
	movq	200(%r15), %rdi
	movl	%eax, %esi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %eax
.L3466:
	movzbl	256(%r15), %edx
	imulq	$114, %rdx, %rdx
	addq	%r14, %rdx
	movsbl	(%rdx,%rax), %ecx
	cmpl	%ecx, %r13d
	jle	.L3482
.L3470:
	movl	%ebx, %edx
	movl	$2, %eax
	movl	%edx, %ebx
	jmp	.L3483
	.p2align 4,,10
	.p2align 3
.L3481:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3465:
	.cfi_restore_state
	movq	16(%r15), %rax
	movb	%dl, -52(%rbp)
	movq	48(%rax), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$9, %eax
	cmpb	$4, %al
	ja	.L3467
	movzbl	-52(%rbp), %edx
	cmpb	$96, %dl
	je	.L3484
.L3467:
	movq	200(%r15), %rax
	movq	%r15, %rdi
	movq	8(%rax), %rax
	movl	(%rax), %eax
	movl	%eax, -52(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParsePrimaryExpressionEv
	movq	200(%r15), %rdi
	movl	%eax, %esi
	movq	8(%rdi), %rdx
	movzbl	56(%rdx), %eax
	cmpb	$3, %al
	jbe	.L3485
.L3468:
	cmpb	$5, %al
	jbe	.L3486
.L3469:
	leal	-51(%rax), %ecx
	cmpb	$1, %cl
	ja	.L3466
	cmpb	$0, 76(%rdx)
	jne	.L3466
	movl	-52(%rbp), %edx
	movq	%r15, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE24ParsePostfixContinuationENS0_19PreParserExpressionEi
	movq	200(%r15), %rdi
	movl	%eax, %esi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %eax
	jmp	.L3466
	.p2align 4,,10
	.p2align 3
.L3482:
	movl	%r13d, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseBinaryContinuationENS0_19PreParserExpressionEii
	movq	200(%r15), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %edx
	movzbl	256(%r15), %eax
	imulq	$114, %rax, %rax
	addq	%r14, %rax
	movsbl	(%rax,%rdx), %ecx
	jmp	.L3470
	.p2align 4,,10
	.p2align 3
.L3486:
	movq	%r15, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseLeftHandSideContinuationENS0_19PreParserExpressionE
	movq	200(%r15), %rdi
	movl	%eax, %esi
	movq	8(%rdi), %rdx
	movzbl	56(%rdx), %eax
	jmp	.L3469
	.p2align 4,,10
	.p2align 3
.L3485:
	movq	%r15, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE35DoParseMemberExpressionContinuationENS0_19PreParserExpressionE
	movq	200(%r15), %rdi
	movl	%eax, %esi
	movq	8(%rdi), %rdx
	movzbl	56(%rdx), %eax
	jmp	.L3468
	.p2align 4,,10
	.p2align 3
.L3484:
	movq	%r15, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseAwaitExpressionEv
	movq	200(%r15), %rdi
	movl	%eax, %esi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %eax
	jmp	.L3466
	.cfi_endproc
.LFE26123:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseBinaryContinuationENS0_19PreParserExpressionEii, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseBinaryContinuationENS0_19PreParserExpressionEii
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseCoalesceExpressionENS0_19PreParserExpressionE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseCoalesceExpressionENS0_19PreParserExpressionE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseCoalesceExpressionENS0_19PreParserExpressionE
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseCoalesceExpressionENS0_19PreParserExpressionE, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseCoalesceExpressionENS0_19PreParserExpressionE:
.LFB26124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	200(%rdi), %rdi
	movq	8(%rdi), %rax
	cmpb	$31, 56(%rax)
	jne	.L3489
	movl	$1, %ebx
	leaq	_ZN2v88internal5Token11precedence_E(%rip), %r14
	jmp	.L3488
	.p2align 4,,10
	.p2align 3
.L3508:
	movq	%r13, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE28ParseUnaryOrPrefixExpressionEv
	movq	200(%r13), %rdi
	movl	%eax, %esi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %ecx
.L3491:
	movzbl	256(%r13), %edx
	movzbl	%cl, %eax
	imulq	$114, %rdx, %rdx
	addq	%r14, %rdx
	movsbl	(%rdx,%rax), %r9d
	cmpl	$5, %r9d
	jg	.L3507
.L3495:
	andl	$-9, %r12d
	movl	$2, %eax
	testb	%bl, %bl
	cmovne	%eax, %r12d
	xorl	%ebx, %ebx
	cmpb	$31, %cl
	jne	.L3489
.L3488:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r13), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %r15d
	leal	-44(%r15), %eax
	cmpb	$8, %al
	jbe	.L3508
	movq	16(%r13), %rax
	movq	48(%rax), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$9, %eax
	cmpb	$4, %al
	ja	.L3492
	cmpb	$96, %r15b
	je	.L3509
.L3492:
	movq	200(%r13), %rax
	movq	%r13, %rdi
	movq	8(%rax), %rax
	movl	(%rax), %r15d
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParsePrimaryExpressionEv
	movq	200(%r13), %rdi
	movl	%eax, %esi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %ecx
	cmpb	$3, %cl
	jbe	.L3510
.L3493:
	cmpb	$5, %cl
	jbe	.L3511
.L3494:
	leal	-51(%rcx), %edx
	cmpb	$1, %dl
	ja	.L3491
	cmpb	$0, 76(%rax)
	jne	.L3491
	movq	%r13, %rdi
	movl	%r15d, %edx
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE24ParsePostfixContinuationENS0_19PreParserExpressionEi
	movq	200(%r13), %rdi
	movl	%eax, %esi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %ecx
	jmp	.L3491
	.p2align 4,,10
	.p2align 3
.L3507:
	movl	%r9d, %ecx
	movq	%r13, %rdi
	movl	$6, %edx
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseBinaryContinuationENS0_19PreParserExpressionEii
	movq	200(%r13), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %ecx
	jmp	.L3495
	.p2align 4,,10
	.p2align 3
.L3511:
	movq	%r13, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseLeftHandSideContinuationENS0_19PreParserExpressionE
	movq	200(%r13), %rdi
	movl	%eax, %esi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %ecx
	jmp	.L3494
	.p2align 4,,10
	.p2align 3
.L3510:
	movq	%r13, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE35DoParseMemberExpressionContinuationENS0_19PreParserExpressionE
	movq	200(%r13), %rdi
	movl	%eax, %esi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %ecx
	jmp	.L3493
	.p2align 4,,10
	.p2align 3
.L3489:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3509:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseAwaitExpressionEv
	movq	200(%r13), %rdi
	movl	%eax, %esi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %ecx
	jmp	.L3491
	.cfi_endproc
.LFE26124:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseCoalesceExpressionENS0_19PreParserExpressionE, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE23ParseCoalesceExpressionENS0_19PreParserExpressionE
	.section	.rodata._ZN2v88internal10ParserBaseINS0_9PreParserEE43ParseMemberWithPresentNewPrefixesExpressionEv.str1.1,"aMS",@progbits,1
.LC16:
	.string	"new.target"
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE43ParseMemberWithPresentNewPrefixesExpressionEv,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE43ParseMemberWithPresentNewPrefixesExpressionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE43ParseMemberWithPresentNewPrefixesExpressionEv
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE43ParseMemberWithPresentNewPrefixesExpressionEv, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE43ParseMemberWithPresentNewPrefixesExpressionEv:
.LFB26413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	200(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Scanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	120(%r12), %rax
	jnb	.L3513
	movq	200(%r12), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L3514
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L3514:
	movq	128(%r12), %rax
	movl	$257, %edx
	movw	%dx, (%rax)
.L3513:
	movq	200(%r12), %rdi
	movq	8(%rdi), %rsi
	movzbl	56(%rsi), %eax
	cmpb	$91, %al
	je	.L3552
	cmpb	$0, 258(%r12)
	je	.L3517
	cmpb	$107, %al
	je	.L3553
.L3517:
	cmpb	$2, %al
	je	.L3554
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParsePrimaryExpressionEv
	movq	200(%r12), %rdx
	movq	8(%rdx), %rdi
	movzbl	56(%rdi), %edx
	cmpb	$3, %dl
	jbe	.L3555
.L3516:
	cmpb	$5, %dl
	je	.L3556
.L3536:
	movl	$2, %eax
	cmpb	$4, %dl
	je	.L3557
.L3522:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L3558
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3554:
	.cfi_restore_state
	movq	(%rdi), %rax
	movl	(%rax), %r13d
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	40(%r12), %rax
	movq	200(%r12), %rdi
	movq	56(%rax), %rax
	movq	464(%rax), %rbx
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$92, %al
	jne	.L3559
.L3524:
	movq	40(%r12), %rsi
	movq	200(%r12), %rdi
	call	_ZNK2v88internal7Scanner13CurrentSymbolEPNS0_15AstValueFactoryE@PLT
	cmpq	%rax, %rbx
	jne	.L3560
.L3525:
	movq	200(%r12), %rax
	movq	(%rax), %rax
	movl	4(%rax), %r9d
	movl	(%rax), %esi
	movl	%r9d, %ecx
	subl	%esi, %ecx
	cmpb	$90, 56(%rax)
	je	.L3561
.L3526:
	cmpb	$0, 28(%rax)
	movl	24(%rax), %edx
	je	.L3562
.L3527:
	cmpl	%edx, %ecx
	jne	.L3563
.L3528:
	movq	(%r12), %rdi
	call	_ZN2v88internal5Scope16GetReceiverScopeEv@PLT
	cmpb	$2, 128(%rax)
	jne	.L3531
	movq	200(%r12), %rdx
	movl	$2, %eax
.L3532:
	movq	8(%rdx), %rdx
	cmpb	$3, 56(%rdx)
	ja	.L3522
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE35DoParseMemberExpressionContinuationENS0_19PreParserExpressionE
	jmp	.L3522
	.p2align 4,,10
	.p2align 3
.L3552:
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseSuperExpressionEb
	movq	200(%r12), %rax
	movq	8(%rax), %rdi
	movzbl	56(%rdi), %edx
	cmpb	$5, %dl
	jne	.L3536
.L3556:
	leaq	-45(%rbp), %rdx
	xorl	%ecx, %ecx
	leaq	-44(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -44(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseArgumentsEPNS0_23PreParserExpressionListEPbNS0_20ParsingArrowHeadFlagE
	movq	200(%r12), %rax
	movq	8(%rax), %rdx
	movl	$2, %eax
	cmpb	$3, 56(%rdx)
	ja	.L3522
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE35DoParseMemberExpressionContinuationENS0_19PreParserExpressionE
	jmp	.L3522
	.p2align 4,,10
	.p2align 3
.L3553:
	cmpb	$0, 259(%r12)
	je	.L3518
	call	_ZN2v88internal7Scanner9PeekAheadEv@PLT
	cmpb	$5, %al
	je	.L3519
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %eax
	jmp	.L3517
	.p2align 4,,10
	.p2align 3
.L3562:
	sarl	%edx
	jmp	.L3527
	.p2align 4,,10
	.p2align 3
.L3531:
	movq	200(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%r8d, %r8d
	movl	$317, %ecx
	movq	(%rax), %rax
	movl	4(%rax), %edx
	movl	(%rax), %esi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r12), %rdx
	movq	24(%rdx), %rax
	cmpb	$0, 48(%rax)
	jne	.L3533
	movl	$-1, 32(%rdx)
	movq	24(%rax), %rcx
	movb	$1, 48(%rax)
	movq	%rcx, 16(%rax)
	movb	$109, 96(%rdx)
	movb	$109, 176(%rdx)
	movb	$109, 256(%rdx)
	movq	200(%r12), %rdx
.L3533:
	movl	$1, %eax
	jmp	.L3532
	.p2align 4,,10
	.p2align 3
.L3561:
	subl	$2, %ecx
	jmp	.L3526
	.p2align 4,,10
	.p2align 3
.L3555:
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE35DoParseMemberExpressionContinuationENS0_19PreParserExpressionE
	movq	200(%r12), %rax
	movq	8(%rax), %rdi
	movzbl	56(%rdi), %edx
	jmp	.L3516
	.p2align 4,,10
	.p2align 3
.L3557:
	movl	4(%rdi), %edx
	movl	(%rdi), %esi
	xorl	%r8d, %r8d
	movl	$374, %ecx
	movq	128(%r12), %r9
	movq	%r9, %rdi
.L3551:
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r12), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L3521
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L3521:
	movl	$1, %eax
	jmp	.L3522
	.p2align 4,,10
	.p2align 3
.L3519:
	movq	200(%r12), %rax
	movq	8(%rax), %rsi
.L3518:
	movl	4(%rsi), %edx
	xorl	%r8d, %r8d
	movl	(%rsi), %esi
	movl	$58, %ecx
	movq	128(%r12), %rdi
	jmp	.L3551
	.p2align 4,,10
	.p2align 3
.L3559:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L3524
	.p2align 4,,10
	.p2align 3
.L3560:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L3525
	.p2align 4,,10
	.p2align 3
.L3563:
	cmpl	$-1, %r13d
	movl	%r9d, %edx
	movq	128(%r12), %rdi
	leaq	.LC16(%rip), %r8
	cmove	%esi, %r13d
	movl	$249, %ecx
	movl	%r13d, %esi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r12), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L3528
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L3528
.L3558:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26413:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE43ParseMemberWithPresentNewPrefixesExpressionEv, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE43ParseMemberWithPresentNewPrefixesExpressionEv
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE10ParseBlockEPNS0_8ZoneListIPKNS0_12AstRawStringEEE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE10ParseBlockEPNS0_8ZoneListIPKNS0_12AstRawStringEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE10ParseBlockEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE10ParseBlockEPNS0_8ZoneListIPKNS0_12AstRawStringEEE, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE10ParseBlockEPNS0_8ZoneListIPKNS0_12AstRawStringEEE:
.LFB25530:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	120(%rbx), %rax
	jnb	.L3565
	movq	200(%rbx), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	je	.L3583
.L3566:
	movq	128(%rbx), %rax
	movl	$257, %edx
	movw	%dx, (%rax)
.L3565:
	movq	136(%rbx), %r12
	movq	16(%r12), %r13
	movq	24(%r12), %rax
	subq	%r13, %rax
	cmpq	$135, %rax
	jbe	.L3584
	leaq	136(%r13), %rax
	movq	%rax, 16(%r12)
.L3568:
	movq	(%rbx), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$6, %ecx
	call	_ZN2v88internal5ScopeC1EPNS0_4ZoneEPS1_NS0_9ScopeTypeE@PLT
	movq	200(%rbx), %rax
	movq	(%rbx), %r12
	movq	%r13, (%rbx)
	movq	8(%rax), %rax
	movl	(%rax), %eax
	movl	%eax, 112(%r13)
	movq	200(%rbx), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$8, %al
	je	.L3569
	jmp	.L3585
	.p2align 4,,10
	.p2align 3
.L3587:
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseStatementListItemEv
	testl	%eax, %eax
	je	.L3586
.L3569:
	movq	200(%rbx), %rdi
	movq	8(%rdi), %rax
	cmpb	$13, 56(%rax)
	jne	.L3587
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$13, %al
	jne	.L3588
.L3574:
	movq	200(%rbx), %rax
	movq	(%rax), %rax
	movl	4(%rax), %edx
	movq	(%rbx), %rax
	movl	%edx, 116(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internal5Scope18FinalizeBlockScopeEv@PLT
	movq	%r12, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	movq	%rax, %rdx
	popq	%r12
	movl	$2, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3583:
	.cfi_restore_state
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L3566
	.p2align 4,,10
	.p2align 3
.L3586:
	movq	%r12, (%rbx)
	addq	$8, %rsp
	movl	$2, %eax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3588:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L3574
	.p2align 4,,10
	.p2align 3
.L3584:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L3568
	.p2align 4,,10
	.p2align 3
.L3585:
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L3569
	.cfi_endproc
.LFE25530:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE10ParseBlockEPNS0_8ZoneListIPKNS0_12AstRawStringEEE, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE10ParseBlockEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseSwitchStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseSwitchStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseSwitchStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseSwitchStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseSwitchStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE:
.LFB25545:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	200(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movl	(%rax), %ebx
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$5, %al
	jne	.L3671
.L3590:
	movq	144(%r12), %rdx
	movq	%r12, -144(%rbp)
	movb	$0, -128(%rbp)
	movq	%rdx, -136(%rbp)
	testq	%rdx, %rdx
	je	.L3591
	cmpb	$0, 17(%rdx)
	setne	-127(%rbp)
	movzbl	18(%rdx), %eax
.L3628:
	movb	%al, -126(%rbp)
	leaq	176(%r12), %rax
	leaq	-144(%rbp), %r13
	movq	%rax, -152(%rbp)
	movq	%rax, -120(%rbp)
	movq	184(%r12), %rax
	subq	176(%r12), %rax
	movq	%r13, 144(%r12)
	sarq	$4, %rax
	movq	%rax, %xmm0
	xorl	%eax, %eax
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movups	%xmm0, -88(%rbp)
	testq	%rdx, %rdx
	je	.L3594
	cmpb	$2, 16(%rdx)
	jbe	.L3672
.L3594:
	movzbl	256(%r12), %r14d
	movq	%r12, %rdi
	movb	$1, 256(%r12)
	movb	%al, -72(%rbp)
	movq	$0, -96(%rbp)
	movl	$-1, -88(%rbp)
	movl	$-1, -80(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE27ParseExpressionCoverGrammarEv
	movl	-88(%rbp), %esi
	movl	-84(%rbp), %edx
	cmpl	%edx, %esi
	jbe	.L3673
.L3595:
	movq	-120(%rbp), %rdi
	movq	-112(%rbp), %rsi
	movb	%r14b, 256(%r12)
	movq	8(%rdi), %rcx
	movq	(%rdi), %rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	ja	.L3674
	jb	.L3675
.L3598:
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	%rdx, 144(%rax)
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$6, %al
	jne	.L3676
.L3599:
	movq	136(%r12), %r14
	movq	16(%r14), %r15
	movq	24(%r14), %rax
	subq	%r15, %rax
	cmpq	$135, %rax
	jbe	.L3677
	leaq	136(%r15), %rax
	movq	%rax, 16(%r14)
.L3601:
	movq	(%r12), %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	$6, %ecx
	call	_ZN2v88internal5ScopeC1EPNS0_4ZoneEPS1_NS0_9ScopeTypeE@PLT
	movq	(%r12), %r14
	movq	%r15, (%r12)
	movl	%ebx, 112(%r15)
	movq	(%r12), %rax
	orb	$8, 129(%rax)
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$8, %al
	jne	.L3678
.L3602:
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %eax
	cmpb	$13, %al
	je	.L3603
	xorl	%ebx, %ebx
.L3620:
	cmpb	$64, %al
	je	.L3604
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$68, %al
	jne	.L3679
	testb	%bl, %bl
	jne	.L3606
.L3687:
	movl	$1, %ebx
.L3607:
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$9, %al
	je	.L3670
	jmp	.L3680
	.p2align 4,,10
	.p2align 3
.L3681:
	cmpb	$13, %al
	je	.L3603
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseStatementListItemEv
	testl	%eax, %eax
	je	.L3616
.L3670:
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %eax
	movl	%eax, %edx
	andl	$-5, %edx
	cmpb	$64, %dl
	jne	.L3681
	cmpb	$13, %al
	jne	.L3620
	.p2align 4,,10
	.p2align 3
.L3603:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$13, %al
	jne	.L3682
.L3624:
	movq	200(%r12), %rax
	movq	(%rax), %rax
	movl	4(%rax), %edx
	movq	(%r12), %rax
	movl	%edx, 116(%rax)
	movq	(%r12), %rdi
	call	_ZN2v88internal5Scope18FinalizeBlockScopeEv@PLT
	movl	$2, %eax
	.p2align 4,,10
	.p2align 3
.L3616:
	movq	%r14, (%r12)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L3683
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3672:
	.cfi_restore_state
	movzbl	72(%rdx), %eax
	jmp	.L3594
	.p2align 4,,10
	.p2align 3
.L3675:
	salq	$4, %rsi
	addq	%rdx, %rsi
	cmpq	%rsi, %rcx
	je	.L3598
	movq	%rsi, 8(%rdi)
	jmp	.L3598
	.p2align 4,,10
	.p2align 3
.L3604:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	144(%r12), %rdx
	movq	%r12, -144(%rbp)
	movb	$0, -128(%rbp)
	movq	%rdx, -136(%rbp)
	testq	%rdx, %rdx
	je	.L3608
	cmpb	$0, 17(%rdx)
	setne	-127(%rbp)
	movzbl	18(%rdx), %eax
.L3627:
	movb	%al, -126(%rbp)
	movq	-152(%rbp), %rax
	pxor	%xmm1, %xmm1
	movq	%r13, 144(%r12)
	movq	%rax, -120(%rbp)
	movq	184(%r12), %rax
	subq	176(%r12), %rax
	movups	%xmm1, -88(%rbp)
	sarq	$4, %rax
	movq	%rax, %xmm0
	xorl	%eax, %eax
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -112(%rbp)
	testq	%rdx, %rdx
	je	.L3611
	cmpb	$2, 16(%rdx)
	jbe	.L3684
.L3611:
	movzbl	256(%r12), %r15d
	movq	%r12, %rdi
	movb	$1, 256(%r12)
	movb	%al, -72(%rbp)
	movq	$0, -96(%rbp)
	movl	$-1, -88(%rbp)
	movl	$-1, -80(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE27ParseExpressionCoverGrammarEv
	movl	-88(%rbp), %esi
	movl	-84(%rbp), %edx
	cmpl	%edx, %esi
	jbe	.L3685
.L3612:
	movq	-120(%rbp), %rdi
	movq	-112(%rbp), %rsi
	movb	%r15b, 256(%r12)
	movq	8(%rdi), %rcx
	movq	(%rdi), %rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	ja	.L3686
	jnb	.L3615
	salq	$4, %rsi
	addq	%rdx, %rsi
	cmpq	%rsi, %rcx
	je	.L3615
	movq	%rsi, 8(%rdi)
.L3615:
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	%rdx, 144(%rax)
	jmp	.L3607
	.p2align 4,,10
	.p2align 3
.L3674:
	subq	%rax, %rsi
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm
	jmp	.L3598
	.p2align 4,,10
	.p2align 3
.L3673:
	movq	-144(%rbp), %r15
	movl	-96(%rbp), %ecx
	xorl	%r8d, %r8d
	movq	128(%r15), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r15), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L3595
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L3595
	.p2align 4,,10
	.p2align 3
.L3680:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L3670
.L3591:
	movb	$0, -127(%rbp)
	xorl	%eax, %eax
	jmp	.L3628
.L3679:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	testb	%bl, %bl
	je	.L3687
.L3606:
	movl	$276, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	xorl	%eax, %eax
	jmp	.L3616
.L3684:
	movzbl	72(%rdx), %eax
	jmp	.L3611
.L3685:
	movq	-144(%rbp), %rax
	movl	-96(%rbp), %ecx
	xorl	%r8d, %r8d
	movq	128(%rax), %rdi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	-160(%rbp), %rax
	movq	200(%rax), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L3612
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L3612
.L3686:
	subq	%rax, %rsi
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm
	jmp	.L3615
.L3608:
	movb	$0, -127(%rbp)
	xorl	%eax, %eax
	jmp	.L3627
.L3671:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L3590
.L3676:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L3599
.L3677:
	movl	$136, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L3601
.L3678:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L3602
.L3682:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L3624
.L3683:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25545:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseSwitchStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseSwitchStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	.section	.rodata._ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseForAwaitStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_.str1.1,"aMS",@progbits,1
.LC17:
	.string	"for-await-of"
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseForAwaitStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseForAwaitStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseForAwaitStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseForAwaitStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseForAwaitStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_:
.LFB25534:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	addl	$1, 24(%rax)
	movq	136(%rdi), %rdi
	movq	%rax, -248(%rbp)
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jbe	.L3857
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L3690:
	pxor	%xmm0, %xmm0
	movq	136(%r12), %r13
	movq	$0, -168(%rbp)
	movups	%xmm0, -184(%rbp)
	movdqa	.LC4(%rip), %xmm0
	movq	%rax, -224(%rbp)
	movaps	%xmm0, -160(%rbp)
	movq	24(%r13), %rax
	movl	$-1, -204(%rbp)
	movl	$1, -208(%rbp)
	movq	16(%r13), %rbx
	movq	$1, -216(%rbp)
	subq	%rbx, %rax
	cmpq	$135, %rax
	jbe	.L3858
	leaq	136(%rbx), %rax
	movq	%rax, 16(%r13)
.L3692:
	movq	(%r12), %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	$6, %ecx
	call	_ZN2v88internal5ScopeC1EPNS0_4ZoneEPS1_NS0_9ScopeTypeE@PLT
	movq	(%r12), %r13
	movq	%rbx, (%r12)
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$72, %al
	jne	.L3859
.L3693:
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$96, %al
	jne	.L3860
.L3694:
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$5, %al
	jne	.L3861
.L3695:
	movq	200(%r12), %rdx
	movq	(%r12), %rax
	movq	(%rdx), %rdx
	movl	(%rdx), %edx
	movl	%edx, 112(%rax)
	movq	(%r12), %rax
	orb	$16, 129(%rax)
	movq	16(%r12), %rax
	addl	$1, 20(%rax)
	movq	16(%r12), %rax
	addl	$1, 20(%rax)
	movq	136(%r12), %r14
	movq	(%r12), %rdx
	movq	16(%r14), %rbx
	movq	24(%r14), %rax
	subq	%rbx, %rax
	cmpq	$135, %rax
	jbe	.L3862
	leaq	136(%rbx), %rax
	movq	%rax, 16(%r14)
.L3697:
	movl	$6, %ecx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal5ScopeC1EPNS0_4ZoneEPS1_NS0_9ScopeTypeE@PLT
	movq	200(%r12), %rdi
	movq	8(%rdi), %rsi
	movzbl	56(%rsi), %eax
	cmpb	$80, %al
	sete	%cl
	cmpb	$104, %al
	sete	%dl
	orb	%dl, %cl
	movb	%cl, -256(%rbp)
	jne	.L3703
	cmpb	$98, %al
	je	.L3863
	movq	(%r12), %rax
	movl	(%rsi), %r15d
	movq	%rbx, (%r12)
	movq	%r12, %rsi
	leaq	-144(%rbp), %r14
	movq	%r14, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPS3_NS0_15ExpressionScopeIS4_E9ScopeTypeE.constprop.0
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParsePrimaryExpressionEv
	movl	%eax, %r10d
	movq	200(%r12), %rax
	movq	8(%rax), %rdx
	movzbl	56(%rdx), %edx
	cmpb	$3, %dl
	jbe	.L3864
.L3710:
	cmpb	$5, %dl
	jbe	.L3865
.L3711:
	movq	(%rax), %rax
	movl	4(%rax), %r11d
	movl	%r10d, %eax
	andl	$7, %eax
	leal	-6(%rax), %edx
	cmpl	$1, %edx
	jbe	.L3866
	movq	-144(%rbp), %rdi
	cmpl	$3, %eax
	je	.L3867
	cmpl	$2, %eax
	jne	.L3728
	movl	%r10d, %eax
	shrl	$4, %eax
	andl	$15, %eax
	subl	$1, %eax
	cmpl	$3, %eax
	ja	.L3728
	movl	-84(%rbp), %eax
	cmpl	%eax, -88(%rbp)
	ja	.L3852
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE8ValidateEi.part.0
	movq	-120(%rbp), %rdi
	movq	-112(%rbp), %rsi
	movq	(%rdi), %rdx
	.p2align 4,,10
	.p2align 3
.L3725:
	movq	8(%rdi), %rcx
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rsi, %rax
	jb	.L3868
	jbe	.L3737
	salq	$4, %rsi
	addq	%rdx, %rsi
	cmpq	%rsi, %rcx
	je	.L3737
	movq	%rsi, 8(%rdi)
.L3737:
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	%rdx, 144(%rax)
	movq	-264(%rbp), %rax
	movq	200(%r12), %rdi
	movq	%rax, (%r12)
.L3708:
	movq	40(%r12), %rax
	movq	56(%rax), %rax
	movq	392(%rax), %r15
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$92, %al
	jne	.L3869
.L3738:
	movq	40(%r12), %rsi
	movq	200(%r12), %rdi
	call	_ZNK2v88internal7Scanner13CurrentSymbolEPNS0_15AstValueFactoryE@PLT
	cmpq	%rax, %r15
	jne	.L3870
.L3739:
	movq	200(%r12), %rax
	movq	(%rax), %rax
	movl	4(%rax), %esi
	movl	(%rax), %edi
	movl	%esi, %ecx
	subl	%edi, %ecx
	cmpb	$90, 56(%rax)
	jne	.L3740
	subl	$2, %ecx
.L3740:
	cmpb	$0, 28(%rax)
	movl	24(%rax), %edx
	jne	.L3741
	sarl	%edx
.L3741:
	cmpl	%edx, %ecx
	jne	.L3871
.L3742:
	movq	144(%r12), %rdx
	movq	%r12, -144(%rbp)
	movzbl	256(%r12), %r15d
	movb	$0, -128(%rbp)
	movb	$1, 256(%r12)
	movq	%rdx, -136(%rbp)
	testq	%rdx, %rdx
	je	.L3743
	cmpb	$0, 17(%rdx)
	setne	-127(%rbp)
	movzbl	18(%rdx), %eax
.L3764:
	movb	%al, -126(%rbp)
	leaq	176(%r12), %rax
	movq	%rax, -120(%rbp)
	movq	184(%r12), %rax
	subq	176(%r12), %rax
	movq	%r14, 144(%r12)
	sarq	$4, %rax
	movq	%rax, %xmm0
	xorl	%eax, %eax
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movups	%xmm0, -88(%rbp)
	testq	%rdx, %rdx
	je	.L3746
	cmpb	$2, 16(%rdx)
	jbe	.L3872
.L3746:
	movq	%r12, %rdi
	movb	%al, -72(%rbp)
	movq	$0, -96(%rbp)
	movl	$-1, -88(%rbp)
	movl	$-1, -80(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseAssignmentExpressionCoverGrammarEv
	movl	-88(%rbp), %esi
	movl	-84(%rbp), %edx
	cmpl	%edx, %esi
	jbe	.L3873
.L3747:
	movq	-120(%rbp), %rdi
	movq	-112(%rbp), %rsi
	movq	8(%rdi), %rcx
	movq	(%rdi), %rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	ja	.L3874
	jb	.L3875
.L3750:
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	%rdx, 144(%rax)
	movq	200(%r12), %rdi
	movb	%r15b, 256(%r12)
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$6, %al
	jne	.L3876
.L3751:
	movq	200(%r12), %rax
	movq	(%r12), %r15
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, (%r12)
	movq	%r12, %rdi
	movl	$1, %ecx
	movq	(%rax), %rax
	movl	(%rax), %eax
	movl	%eax, 112(%rbx)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE
	movq	(%r12), %rax
	movq	200(%r12), %rdx
	cmpb	$0, -256(%rbp)
	movq	(%rdx), %rdx
	movl	4(%rdx), %edx
	movl	%edx, 116(%rax)
	movq	(%r12), %rdi
	jne	.L3877
	call	_ZN2v88internal5Scope18FinalizeBlockScopeEv@PLT
	movq	%r15, (%r12)
	movq	%r15, %rdi
	movl	$2, %r14d
	call	_ZN2v88internal5Scope18FinalizeBlockScopeEv@PLT
	jmp	.L3707
	.p2align 4,,10
	.p2align 3
.L3863:
	call	_ZN2v88internal7Scanner9PeekAheadEv@PLT
	cmpb	$100, %al
	je	.L3701
	ja	.L3702
	cmpb	$8, %al
	je	.L3703
	jbe	.L3878
	subl	$92, %eax
	cmpb	$7, %al
	ja	.L3702
	.p2align 4,,10
	.p2align 3
.L3703:
	movq	(%r12), %r14
.L3699:
	movq	%rbx, (%r12)
	movq	%r12, %rdi
	leaq	-224(%rbp), %rcx
	leaq	-200(%rbp), %rdx
	movl	$2, %esi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE25ParseVariableDeclarationsENS3_26VariableDeclarationContextEPNS3_24DeclarationParsingResultEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	movq	200(%r12), %rdi
	movq	%r14, (%r12)
	movq	(%rdi), %rax
	movl	(%rax), %eax
	movl	%eax, -204(%rbp)
	movq	-176(%rbp), %rax
	subq	-184(%rbp), %rax
	cmpq	$12, %rax
	jne	.L3879
	movl	-160(%rbp), %esi
	movl	-156(%rbp), %edx
	movb	$1, -256(%rbp)
	leaq	-144(%rbp), %r14
	cmpl	%edx, %esi
	ja	.L3708
	movq	128(%r12), %rdi
	leaq	.LC17(%rip), %r8
	movl	$234, %ecx
	jmp	.L3856
	.p2align 4,,10
	.p2align 3
.L3879:
	movq	128(%r12), %rdi
	movl	-148(%rbp), %edx
	leaq	.LC17(%rip), %r8
	movl	$236, %ecx
	movl	-152(%rbp), %esi
.L3856:
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r12), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	je	.L3880
.L3853:
	xorl	%r14d, %r14d
.L3707:
	movq	-184(%rbp), %rdi
	movq	%r13, (%r12)
	testq	%rdi, %rdi
	je	.L3762
	call	_ZdlPv@PLT
.L3762:
	movq	-248(%rbp), %rax
	subl	$1, 24(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3881
	addq	$280, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3877:
	.cfi_restore_state
	call	_ZN2v88internal5Scope18FinalizeBlockScopeEv@PLT
	cmpb	$1, -200(%rbp)
	movq	%r15, (%r12)
	jbe	.L3882
.L3849:
	movq	200(%r12), %rax
	movl	$2, %r14d
	movq	(%rax), %rax
	movl	4(%rax), %eax
	movl	%eax, 116(%r15)
	movq	(%r12), %rdi
	call	_ZN2v88internal5Scope18FinalizeBlockScopeEv@PLT
	jmp	.L3707
	.p2align 4,,10
	.p2align 3
.L3880:
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L3853
	.p2align 4,,10
	.p2align 3
.L3872:
	movzbl	72(%rdx), %eax
	jmp	.L3746
	.p2align 4,,10
	.p2align 3
.L3875:
	salq	$4, %rsi
	addq	%rdx, %rsi
	cmpq	%rsi, %rcx
	je	.L3750
	movq	%rsi, 8(%rdi)
	jmp	.L3750
	.p2align 4,,10
	.p2align 3
.L3866:
	movl	-80(%rbp), %esi
	movl	-76(%rbp), %edx
	cmpl	%edx, %esi
	jbe	.L3883
.L3713:
	andl	$8, %r10d
	jne	.L3884
.L3715:
	movq	-120(%rbp), %rdi
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rax
	movq	(%rdi), %rdx
	movq	%rsi, %r15
	salq	$4, %r15
	salq	$4, %rax
	addq	%rdx, %r15
	addq	%rdx, %rax
	cmpq	%rax, %r15
	jne	.L3724
	jmp	.L3725
	.p2align 4,,10
	.p2align 3
.L3718:
	addq	$16, %r15
	cmpq	%r15, %rax
	je	.L3885
.L3724:
	movq	(%r15), %rdx
	movl	4(%rdx), %ecx
	movl	%ecx, %esi
	orb	$-128, %sil
	movl	%esi, 4(%rdx)
	andb	$1, %ch
	je	.L3718
	movq	8(%rdx), %rcx
	movzwl	40(%rcx), %edx
	movl	%edx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L3718
	movq	16(%rcx), %rsi
	testq	%rsi, %rsi
	je	.L3719
	testb	$64, %dh
	je	.L3886
.L3719:
	orb	$64, %dh
	addq	$16, %r15
	movw	%dx, 40(%rcx)
	cmpq	%r15, %rax
	jne	.L3724
.L3885:
	movq	-120(%rbp), %rdi
	movq	-112(%rbp), %rsi
	movq	(%rdi), %rdx
	jmp	.L3725
	.p2align 4,,10
	.p2align 3
.L3701:
	movq	(%r12), %r14
	testb	$1, 129(%r14)
	je	.L3699
	.p2align 4,,10
	.p2align 3
.L3702:
	movq	200(%r12), %rax
	movl	$235, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc.constprop.0
	jmp	.L3853
	.p2align 4,,10
	.p2align 3
.L3874:
	subq	%rax, %rsi
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm
	jmp	.L3750
	.p2align 4,,10
	.p2align 3
.L3873:
	movq	-144(%rbp), %r14
	movl	-96(%rbp), %ecx
	xorl	%r8d, %r8d
	movq	128(%r14), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r14), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L3747
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L3747
	.p2align 4,,10
	.p2align 3
.L3743:
	movb	$0, -127(%rbp)
	xorl	%eax, %eax
	jmp	.L3764
	.p2align 4,,10
	.p2align 3
.L3882:
	movq	-224(%rbp), %r11
	movslq	-212(%rbp), %rax
	leaq	(%r11,%rax,8), %rax
	movq	%rax, -256(%rbp)
	cmpq	%rax, %r11
	je	.L3849
	leaq	-225(%rbp), %rax
	movq	%r13, -272(%rbp)
	movq	%rax, -264(%rbp)
	movq	%r15, %rax
	movq	%r11, %r15
	movq	%rax, %rbx
	jmp	.L3761
	.p2align 4,,10
	.p2align 3
.L3756:
	cmpq	%rbx, (%rax)
	je	.L3758
	movq	48(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L3887
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L3760:
	movabsq	$279172874239, %rcx
	movq	$0, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rbx, 24(%rax)
	movq	%r14, 8(%rax)
	movq	(%r14), %rdx
	movq	96(%rdx), %rcx
	movq	%rax, (%rcx)
	addq	$16, %rax
	movq	%rax, 96(%rdx)
.L3758:
	movq	(%r12), %rbx
	addq	$8, %r15
	cmpq	%r15, -256(%rbp)
	je	.L3888
.L3761:
	movq	(%r15), %r13
	movq	-264(%rbp), %rcx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal5Scope19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPbNS0_12VariableKindE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L3756
	movq	128(%r12), %rax
	movb	$1, (%rax)
	movb	$1, 2(%rax)
	movq	200(%r12), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L3757
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L3757:
	leaq	32(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal11VariableMap6LookupEPKNS0_12AstRawStringE@PLT
	jmp	.L3758
	.p2align 4,,10
	.p2align 3
.L3878:
	cmpb	$3, %al
	je	.L3703
	jmp	.L3702
	.p2align 4,,10
	.p2align 3
.L3868:
	subq	%rax, %rsi
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm
	jmp	.L3737
	.p2align 4,,10
	.p2align 3
.L3865:
	movl	%r10d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE29ParseLeftHandSideContinuationENS0_19PreParserExpressionE
	movl	%eax, %r10d
	movq	200(%r12), %rax
	jmp	.L3711
	.p2align 4,,10
	.p2align 3
.L3864:
	movl	%r10d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE35DoParseMemberExpressionContinuationENS0_19PreParserExpressionE
	movl	%eax, %r10d
	movq	200(%r12), %rax
	movq	8(%rax), %rdx
	movzbl	56(%rdx), %edx
	jmp	.L3710
	.p2align 4,,10
	.p2align 3
.L3857:
	movl	$8, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L3690
	.p2align 4,,10
	.p2align 3
.L3858:
	movl	$136, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L3692
	.p2align 4,,10
	.p2align 3
.L3859:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L3693
	.p2align 4,,10
	.p2align 3
.L3860:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L3694
	.p2align 4,,10
	.p2align 3
.L3861:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L3695
	.p2align 4,,10
	.p2align 3
.L3862:
	movl	$136, %esi
	movq	%r14, %rdi
	movq	%rdx, -256(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-256(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L3697
	.p2align 4,,10
	.p2align 3
.L3888:
	movq	-272(%rbp), %r13
	movq	%rbx, %r15
	jmp	.L3849
	.p2align 4,,10
	.p2align 3
.L3867:
	movq	(%rdi), %rax
	testb	$1, 129(%rax)
	jne	.L3889
.L3727:
	movq	-104(%rbp), %rcx
	movq	-112(%rbp), %rsi
	movq	-120(%rbp), %rdi
	movl	%ecx, %eax
	movq	(%rdi), %rdx
	subl	%esi, %eax
	cmpl	%esi, %ecx
	je	.L3725
	subl	$1, %eax
	cltq
	addq	%rax, %rsi
	salq	$4, %rsi
	movq	(%rdx,%rsi), %rax
	movl	4(%rax), %edx
	movl	%edx, %ecx
	orb	$-128, %cl
	movl	%ecx, 4(%rax)
	andb	$1, %dh
	jne	.L3730
.L3852:
	movq	-120(%rbp), %rdi
	movq	-112(%rbp), %rsi
	movq	(%rdi), %rdx
	jmp	.L3725
	.p2align 4,,10
	.p2align 3
.L3886:
	movzwl	40(%rsi), %edi
	movl	%edi, %r8d
	andl	$15, %r8d
	cmpb	$1, %r8b
	je	.L3719
	movq	16(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L3720
	testw	$16384, %di
	je	.L3890
.L3720:
	orw	$16384, %di
	movw	%di, 40(%rsi)
	movzwl	40(%rcx), %edx
	jmp	.L3719
	.p2align 4,,10
	.p2align 3
.L3889:
	movl	%r10d, %eax
	shrl	$4, %eax
	subl	$2, %eax
	cmpb	$1, %al
	ja	.L3727
.L3728:
	movl	$253, %r8d
	movl	%r11d, %ecx
	movl	%r15d, %edx
	movl	%r10d, %esi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE33RewriteInvalidReferenceExpressionENS0_19PreParserExpressionEiiNS0_15MessageTemplateE
	jmp	.L3852
	.p2align 4,,10
	.p2align 3
.L3871:
	movq	%rsi, %rax
	movq	8(%r15), %rcx
	movl	%edi, %esi
	movl	$249, %edx
	salq	$32, %rax
	movq	%r12, %rdi
	orq	%rax, %rsi
	call	_ZN2v88internal9PreParser15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateEPKc
	jmp	.L3742
	.p2align 4,,10
	.p2align 3
.L3869:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L3738
	.p2align 4,,10
	.p2align 3
.L3870:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L3739
	.p2align 4,,10
	.p2align 3
.L3876:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L3751
	.p2align 4,,10
	.p2align 3
.L3730:
	movq	8(%rax), %r15
	movzwl	40(%r15), %eax
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$1, %dl
	je	.L3852
	movq	16(%r15), %rdx
	testq	%rdx, %rdx
	je	.L3732
	testb	$64, %ah
	je	.L3891
.L3732:
	orb	$64, %ah
	movw	%ax, 40(%r15)
	movq	-120(%rbp), %rdi
	movq	-112(%rbp), %rsi
	movq	(%rdi), %rdx
	jmp	.L3725
	.p2align 4,,10
	.p2align 3
.L3883:
	movq	-144(%rbp), %rax
	movl	-92(%rbp), %ecx
	xorl	%r8d, %r8d
	movl	%r11d, -288(%rbp)
	movl	%r10d, -280(%rbp)
	movq	128(%rax), %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	-272(%rbp), %rax
	movl	-280(%rbp), %r10d
	movl	-288(%rbp), %r11d
	movq	200(%rax), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L3713
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L3713
	.p2align 4,,10
	.p2align 3
.L3884:
	movq	-144(%rbp), %rax
	xorl	%r8d, %r8d
	movl	%r11d, %edx
	movl	%r15d, %esi
	movl	$252, %ecx
	movq	128(%rax), %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	-272(%rbp), %rax
	movq	200(%rax), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L3715
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L3715
	.p2align 4,,10
	.p2align 3
.L3887:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L3760
	.p2align 4,,10
	.p2align 3
.L3890:
	movzwl	40(%rdx), %r8d
	movl	%r8d, %r9d
	andl	$15, %r9d
	cmpb	$1, %r9b
	je	.L3720
	movq	16(%rdx), %r9
	testq	%r9, %r9
	je	.L3721
	testw	$16384, %r8w
	je	.L3892
.L3721:
	orw	$16384, %r8w
	movw	%r8w, 40(%rdx)
	movzwl	40(%rsi), %edi
	jmp	.L3720
.L3891:
	movzwl	40(%rdx), %ecx
	movl	%ecx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L3732
	movq	16(%rdx), %rax
	testq	%rax, %rax
	je	.L3733
	testb	$64, %ch
	je	.L3893
.L3733:
	orb	$64, %ch
	movw	%cx, 40(%rdx)
	movzwl	40(%r15), %eax
	jmp	.L3732
.L3892:
	movzwl	40(%r9), %edi
	movl	%edi, %r10d
	andl	$15, %r10d
	cmpb	$1, %r10b
	je	.L3721
	movq	16(%r9), %r10
	testq	%r10, %r10
	je	.L3722
	testw	$16384, %di
	je	.L3894
.L3722:
	orw	$16384, %di
	movw	%di, 40(%r9)
	movzwl	40(%rdx), %r8d
	jmp	.L3721
.L3893:
	movzwl	40(%rax), %esi
	movl	%esi, %edi
	andl	$15, %edi
	cmpb	$1, %dil
	je	.L3733
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3734
	testw	$16384, %si
	je	.L3895
.L3734:
	orw	$16384, %si
	movw	%si, 40(%rax)
	movzwl	40(%rdx), %ecx
	jmp	.L3733
.L3894:
	movzwl	40(%r10), %r8d
	movl	%r8d, %r11d
	andl	$15, %r11d
	subb	$1, %r11b
	je	.L3722
	movq	16(%r10), %rdi
	testq	%rdi, %rdi
	je	.L3723
	testw	$16384, %r8w
	je	.L3896
.L3723:
	orw	$16384, %r8w
	movw	%r8w, 40(%r10)
	movzwl	40(%r9), %edi
	jmp	.L3722
.L3881:
	call	__stack_chk_fail@PLT
.L3895:
	movq	%rax, -280(%rbp)
	movq	%rdx, -272(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movq	-280(%rbp), %rax
	movq	-272(%rbp), %rdx
	movzwl	40(%rax), %esi
	jmp	.L3734
.L3896:
	movq	%rax, -312(%rbp)
	movq	%r10, -304(%rbp)
	movq	%r9, -296(%rbp)
	movq	%rdx, -288(%rbp)
	movq	%rsi, -280(%rbp)
	movq	%rcx, -272(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movq	-304(%rbp), %r10
	movq	-312(%rbp), %rax
	movq	-296(%rbp), %r9
	movq	-288(%rbp), %rdx
	movzwl	40(%r10), %r8d
	movq	-280(%rbp), %rsi
	movq	-272(%rbp), %rcx
	jmp	.L3723
	.cfi_endproc
.LFE25534:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseForAwaitStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE22ParseForAwaitStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE21ParseDoWhileStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE21ParseDoWhileStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ParseDoWhileStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ParseDoWhileStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE21ParseDoWhileStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_:
.LFB25532:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	16(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 24(%r12)
	movq	200(%rdi), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	120(%rbx), %rax
	jnb	.L3898
	movq	200(%rbx), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L3899
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L3899:
	movq	128(%rbx), %rax
	movl	$257, %edx
	movw	%dx, (%rax)
.L3898:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE
	movq	200(%rbx), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$81, %al
	jne	.L3929
	movq	200(%rbx), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$5, %al
	jne	.L3930
.L3901:
	movq	144(%rbx), %rdx
	movq	%rbx, -128(%rbp)
	movb	$0, -112(%rbp)
	movq	%rdx, -120(%rbp)
	testq	%rdx, %rdx
	je	.L3902
	cmpb	$0, 17(%rdx)
	movzbl	18(%rdx), %eax
	setne	-111(%rbp)
.L3913:
	movb	%al, -110(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, 144(%rbx)
	leaq	176(%rbx), %rax
	movq	%rax, -104(%rbp)
	movq	184(%rbx), %rax
	subq	176(%rbx), %rax
	sarq	$4, %rax
	movq	%rax, %xmm0
	xorl	%eax, %eax
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movups	%xmm0, -72(%rbp)
	testq	%rdx, %rdx
	je	.L3905
	cmpb	$2, 16(%rdx)
	jbe	.L3931
.L3905:
	movzbl	256(%rbx), %r13d
	movq	%rbx, %rdi
	movb	$1, 256(%rbx)
	movb	%al, -56(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	movl	$-1, -64(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE27ParseExpressionCoverGrammarEv
	movl	-72(%rbp), %esi
	movl	-68(%rbp), %edx
	cmpl	%edx, %esi
	jbe	.L3932
.L3906:
	movq	-104(%rbp), %rdi
	movq	-96(%rbp), %rsi
	movb	%r13b, 256(%rbx)
	movq	8(%rdi), %rcx
	movq	(%rdi), %rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	ja	.L3933
	jb	.L3934
.L3909:
	movq	-128(%rbp), %rax
	movq	-120(%rbp), %rdx
	movq	%rdx, 144(%rax)
	movq	200(%rbx), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$6, %al
	jne	.L3935
.L3910:
	movq	200(%rbx), %rdi
	movq	8(%rdi), %rax
	cmpb	$12, 56(%rax)
	je	.L3936
.L3911:
	subl	$1, 24(%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3937
	addq	$96, %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3931:
	.cfi_restore_state
	movzbl	72(%rdx), %eax
	jmp	.L3905
	.p2align 4,,10
	.p2align 3
.L3934:
	salq	$4, %rsi
	addq	%rdx, %rsi
	cmpq	%rsi, %rcx
	je	.L3909
	movq	%rsi, 8(%rdi)
	jmp	.L3909
	.p2align 4,,10
	.p2align 3
.L3933:
	subq	%rax, %rsi
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm
	jmp	.L3909
	.p2align 4,,10
	.p2align 3
.L3932:
	movq	-128(%rbp), %r14
	movl	-80(%rbp), %ecx
	xorl	%r8d, %r8d
	movq	128(%r14), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r14), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L3906
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L3906
	.p2align 4,,10
	.p2align 3
.L3902:
	movb	$0, -111(%rbp)
	xorl	%eax, %eax
	jmp	.L3913
	.p2align 4,,10
	.p2align 3
.L3936:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	jmp	.L3911
	.p2align 4,,10
	.p2align 3
.L3935:
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L3910
	.p2align 4,,10
	.p2align 3
.L3929:
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	movq	200(%rbx), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$5, %al
	je	.L3901
	.p2align 4,,10
	.p2align 3
.L3930:
	movq	%rbx, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L3901
.L3937:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25532:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ParseDoWhileStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE21ParseDoWhileStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_
	.section	.rodata._ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseForEachStatementWithDeclarationsEiPNS3_7ForInfoEPNS0_8ZoneListIPKNS0_12AstRawStringEEESB_PNS0_5ScopeE.str1.1,"aMS",@progbits,1
.LC18:
	.string	"for-of"
.LC19:
	.string	"for-in"
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseForEachStatementWithDeclarationsEiPNS3_7ForInfoEPNS0_8ZoneListIPKNS0_12AstRawStringEEESB_PNS0_5ScopeE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseForEachStatementWithDeclarationsEiPNS3_7ForInfoEPNS0_8ZoneListIPKNS0_12AstRawStringEEESB_PNS0_5ScopeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseForEachStatementWithDeclarationsEiPNS3_7ForInfoEPNS0_8ZoneListIPKNS0_12AstRawStringEEESB_PNS0_5ScopeE
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseForEachStatementWithDeclarationsEiPNS3_7ForInfoEPNS0_8ZoneListIPKNS0_12AstRawStringEEESB_PNS0_5ScopeE, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseForEachStatementWithDeclarationsEiPNS3_7ForInfoEPNS0_8ZoneListIPKNS0_12AstRawStringEEESB_PNS0_5ScopeE:
.LFB25882:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$152, %rsp
	movq	40(%rdx), %rdx
	movl	16(%rbx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	48(%rbx), %rax
	subq	%rdx, %rax
	cmpq	$12, %rax
	je	.L3939
	cmpl	$1, %ecx
	leaq	.LC18(%rip), %r8
	movl	76(%rbx), %edx
	movl	72(%rbx), %esi
	leaq	.LC19(%rip), %rax
	movq	128(%rdi), %rdi
	movl	$236, %ecx
	cmovne	%rax, %r8
.L3976:
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r12), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	je	.L3977
.L3948:
	xorl	%eax, %eax
.L3942:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L3978
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3977:
	.cfi_restore_state
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L3948
	.p2align 4,,10
	.p2align 3
.L3939:
	movq	%r9, %r13
	movl	64(%rbx), %esi
	movl	68(%rbx), %r9d
	cmpl	%esi, %r9d
	jnb	.L3979
	cmpl	$1, %ecx
	je	.L3980
.L3946:
	leaq	-144(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPS3_NS0_15ExpressionScopeIS4_E9ScopeTypeE.constprop.0
	movq	%r12, %rdi
	movzbl	256(%r12), %r15d
	movb	$1, 256(%r12)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE27ParseExpressionCoverGrammarEv
	movl	-84(%rbp), %eax
	cmpl	%eax, -88(%rbp)
	jbe	.L3981
.L3951:
	movq	-112(%rbp), %rsi
	movq	-120(%rbp), %rdi
	movb	%r15b, 256(%r12)
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE6resizeEm
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	%rdx, 144(%rax)
.L3950:
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$6, %al
	jne	.L3982
.L3952:
	cmpb	$1, 24(%rbx)
	ja	.L3953
	movq	200(%r12), %rax
	movq	(%rax), %rax
	movl	(%rax), %eax
	movl	%eax, 112(%r13)
.L3953:
	movq	(%r12), %r15
	movq	%r13, (%r12)
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$1, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE
	cmpb	$1, 24(%rbx)
	jbe	.L3983
.L3954:
	movq	%r15, (%r12)
	cmpb	$1, 24(%rbx)
	jbe	.L3984
.L3955:
	movl	$2, %eax
	jmp	.L3942
	.p2align 4,,10
	.p2align 3
.L3979:
	movq	(%rdi), %rax
	testb	$1, 129(%rax)
	jne	.L3944
	cmpl	$1, %ecx
	je	.L3966
	cmpb	$1, 24(%rbx)
	jbe	.L3967
	movl	(%rdx), %eax
	leaq	.LC19(%rip), %r8
	andl	$7, %eax
	cmpl	$3, %eax
	jne	.L3945
	jmp	.L3946
	.p2align 4,,10
	.p2align 3
.L3944:
	cmpl	$1, %ecx
	leaq	.LC19(%rip), %r8
	leaq	.LC18(%rip), %rax
	cmove	%rax, %r8
.L3945:
	movq	128(%r12), %rdi
	movl	$234, %ecx
	movl	%r9d, %edx
	jmp	.L3976
	.p2align 4,,10
	.p2align 3
.L3980:
	movzbl	256(%rdi), %r15d
	movb	$1, 256(%rdi)
	leaq	-144(%rbp), %r14
	movq	%rdi, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEEC2EPS3_NS0_15ExpressionScopeIS4_E9ScopeTypeE.constprop.0
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseAssignmentExpressionCoverGrammarEv
	movl	-84(%rbp), %eax
	cmpl	%eax, -88(%rbp)
	jbe	.L3985
.L3949:
	movq	-112(%rbp), %rsi
	movq	-120(%rbp), %rdi
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE6resizeEm
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	%rdx, 144(%rax)
	movb	%r15b, 256(%r12)
	jmp	.L3950
	.p2align 4,,10
	.p2align 3
.L3984:
	movq	(%rbx), %r9
	movslq	12(%rbx), %rax
	leaq	(%r9,%rax,8), %rax
	movq	%rax, -168(%rbp)
	cmpq	%rax, %r9
	je	.L3956
	leaq	-145(%rbp), %rax
	movq	%r9, %r13
	movq	%rax, -176(%rbp)
	jmp	.L3963
	.p2align 4,,10
	.p2align 3
.L3957:
	cmpq	%r15, (%rax)
	je	.L3959
	movq	48(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	$31, %rcx
	jbe	.L3986
	leaq	32(%rax), %rcx
	movq	%rcx, 16(%rdi)
.L3961:
	movabsq	$279172874239, %rsi
	movq	$0, 16(%rax)
	movq	%rsi, (%rax)
	movq	%r15, 24(%rax)
	movq	%rdx, 8(%rax)
	movq	(%rdx), %rdx
	movq	96(%rdx), %rcx
	movq	%rax, (%rcx)
	addq	$16, %rax
	movq	%rax, 96(%rdx)
.L3959:
	addq	$8, %r13
	cmpq	%r13, -168(%rbp)
	je	.L3962
	movq	(%r12), %r15
.L3963:
	movq	0(%r13), %r14
	movq	-176(%rbp), %rcx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal5Scope19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPbNS0_12VariableKindE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L3957
	movq	128(%r12), %rax
	movb	$1, (%rax)
	movb	$1, 2(%rax)
	movq	200(%r12), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L3958
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L3958:
	leaq	32(%r15), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal11VariableMap6LookupEPKNS0_12AstRawStringE@PLT
	jmp	.L3959
	.p2align 4,,10
	.p2align 3
.L3962:
	cmpb	$1, 24(%rbx)
	ja	.L3955
	movq	(%r12), %r15
.L3956:
	movq	200(%r12), %rax
	movq	(%rax), %rax
	movl	4(%rax), %eax
	movl	%eax, 116(%r15)
	movq	(%r12), %rdi
	call	_ZN2v88internal5Scope18FinalizeBlockScopeEv@PLT
	jmp	.L3955
	.p2align 4,,10
	.p2align 3
.L3967:
	leaq	.LC19(%rip), %r8
	jmp	.L3945
	.p2align 4,,10
	.p2align 3
.L3985:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE8ValidateEi.part.0
	jmp	.L3949
	.p2align 4,,10
	.p2align 3
.L3983:
	movq	200(%r12), %rdx
	movq	(%r12), %rax
	movq	(%rdx), %rdx
	movl	4(%rdx), %edx
	movl	%edx, 116(%rax)
	movq	(%r12), %rdi
	call	_ZN2v88internal5Scope18FinalizeBlockScopeEv@PLT
	jmp	.L3954
	.p2align 4,,10
	.p2align 3
.L3981:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal22ExpressionParsingScopeINS0_11ParserTypesINS0_9PreParserEEEE8ValidateEi.part.0
	jmp	.L3951
	.p2align 4,,10
	.p2align 3
.L3966:
	leaq	.LC18(%rip), %r8
	jmp	.L3945
	.p2align 4,,10
	.p2align 3
.L3986:
	movl	$32, %esi
	movq	%rdx, -184(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-184(%rbp), %rdx
	jmp	.L3961
	.p2align 4,,10
	.p2align 3
.L3982:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L3952
.L3978:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25882:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseForEachStatementWithDeclarationsEiPNS3_7ForInfoEPNS0_8ZoneListIPKNS0_12AstRawStringEEESB_PNS0_5ScopeE, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE37ParseForEachStatementWithDeclarationsEiPNS3_7ForInfoEPNS0_8ZoneListIPKNS0_12AstRawStringEEESB_PNS0_5ScopeE
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseStandardForLoopEiPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_PNS0_19PreParserExpressionEPNS0_18PreParserStatementESD_,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseStandardForLoopEiPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_PNS0_19PreParserExpressionEPNS0_18PreParserStatementESD_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseStandardForLoopEiPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_PNS0_19PreParserExpressionEPNS0_18PreParserStatementESD_
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseStandardForLoopEiPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_PNS0_19PreParserExpressionEPNS0_18PreParserStatementESD_, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseStandardForLoopEiPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_PNS0_19PreParserExpressionEPNS0_18PreParserStatementESD_:
.LFB25886:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$120, %rsp
	movq	16(%rbp), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	120(%r12), %rax
	jnb	.L3988
	movq	200(%r12), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L3989
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
.L3989:
	movq	128(%r12), %rax
	movl	$257, %edx
	movw	%dx, (%rax)
.L3988:
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	cmpb	$12, 56(%rax)
	jne	.L4032
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$12, %al
	jne	.L4033
.L3999:
	movq	200(%r12), %rdi
	movq	8(%rdi), %rax
	cmpb	$6, 56(%rax)
	jne	.L4034
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$6, %al
	jne	.L4035
.L4010:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$1, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE
	movl	%eax, 0(%r13)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4036
	addq	$120, %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4034:
	.cfi_restore_state
	movq	144(%r12), %rdx
	movq	%r12, -144(%rbp)
	movb	$0, -128(%rbp)
	movq	%rdx, -136(%rbp)
	testq	%rdx, %rdx
	je	.L4001
	cmpb	$0, 17(%rdx)
	setne	-127(%rbp)
	movzbl	18(%rdx), %eax
.L4013:
	movb	%al, -126(%rbp)
	leaq	-144(%rbp), %rax
	movq	%rax, 144(%r12)
	leaq	176(%r12), %rax
	movq	%rax, -120(%rbp)
	movq	184(%r12), %rax
	subq	176(%r12), %rax
	sarq	$4, %rax
	movq	%rax, %xmm0
	xorl	%eax, %eax
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movups	%xmm0, -88(%rbp)
	testq	%rdx, %rdx
	je	.L4004
	cmpb	$2, 16(%rdx)
	jbe	.L4037
.L4004:
	movzbl	256(%r12), %r15d
	movq	%r12, %rdi
	movb	$1, 256(%r12)
	movb	%al, -72(%rbp)
	movq	$0, -96(%rbp)
	movl	$-1, -88(%rbp)
	movl	$-1, -80(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE27ParseExpressionCoverGrammarEv
	movl	-88(%rbp), %esi
	movl	-84(%rbp), %edx
	movl	%eax, %r14d
	cmpl	%edx, %esi
	jbe	.L4038
.L4005:
	movq	-120(%rbp), %rdi
	movq	-112(%rbp), %rsi
	movb	%r15b, 256(%r12)
	movq	8(%rdi), %rcx
	movq	(%rdi), %rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	ja	.L4039
	jnb	.L4008
	salq	$4, %rsi
	addq	%rdx, %rsi
	cmpq	%rsi, %rcx
	je	.L4008
	movq	%rsi, 8(%rdi)
.L4008:
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	%rdx, 144(%rax)
	movl	%r14d, %eax
	movl	$2, %edx
	andl	$7, %eax
	cmpl	$4, %eax
	cmovne	%edx, %eax
	movl	%eax, (%rbx)
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$6, %al
	je	.L4010
.L4035:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L4010
	.p2align 4,,10
	.p2align 3
.L4032:
	movq	144(%r12), %rdx
	movq	%r12, -144(%rbp)
	movb	$0, -128(%rbp)
	movq	%rdx, -136(%rbp)
	testq	%rdx, %rdx
	je	.L3991
	cmpb	$0, 17(%rdx)
	setne	-127(%rbp)
	movzbl	18(%rdx), %eax
.L4014:
	movb	%al, -126(%rbp)
	leaq	-144(%rbp), %rax
	movq	%rax, 144(%r12)
	leaq	176(%r12), %rax
	movq	%rax, -120(%rbp)
	movq	184(%r12), %rax
	subq	176(%r12), %rax
	sarq	$4, %rax
	movq	%rax, %xmm0
	xorl	%eax, %eax
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movups	%xmm0, -88(%rbp)
	testq	%rdx, %rdx
	je	.L3994
	cmpb	$2, 16(%rdx)
	jbe	.L4040
.L3994:
	movzbl	256(%r12), %r15d
	movq	%r12, %rdi
	movb	$1, 256(%r12)
	movb	%al, -72(%rbp)
	movq	$0, -96(%rbp)
	movl	$-1, -88(%rbp)
	movl	$-1, -80(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE27ParseExpressionCoverGrammarEv
	movl	-88(%rbp), %esi
	movl	-84(%rbp), %edx
	cmpl	%edx, %esi
	jbe	.L4041
.L3995:
	movq	-120(%rbp), %rdi
	movq	-112(%rbp), %rsi
	movb	%r15b, 256(%r12)
	movq	8(%rdi), %r8
	movq	(%rdi), %rcx
	movq	%r8, %rdx
	subq	%rcx, %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	ja	.L4042
	jnb	.L3998
	salq	$4, %rsi
	addq	%rcx, %rsi
	cmpq	%rsi, %r8
	je	.L3998
	movq	%rsi, 8(%rdi)
.L3998:
	movq	-144(%rbp), %rdx
	movq	-136(%rbp), %rcx
	movq	%rcx, 144(%rdx)
	movl	%eax, (%r14)
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$12, %al
	je	.L3999
.L4033:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L3999
	.p2align 4,,10
	.p2align 3
.L4040:
	movzbl	72(%rdx), %eax
	jmp	.L3994
	.p2align 4,,10
	.p2align 3
.L4037:
	movzbl	72(%rdx), %eax
	jmp	.L4004
	.p2align 4,,10
	.p2align 3
.L4042:
	subq	%rdx, %rsi
	movl	%eax, -152(%rbp)
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm
	movl	-152(%rbp), %eax
	jmp	.L3998
	.p2align 4,,10
	.p2align 3
.L4041:
	movq	-144(%rbp), %r9
	movl	-96(%rbp), %ecx
	xorl	%r8d, %r8d
	movl	%eax, -156(%rbp)
	movq	128(%r9), %rdi
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	-152(%rbp), %r9
	movl	-156(%rbp), %eax
	movq	200(%r9), %rdx
	movq	24(%rdx), %rcx
	cmpb	$0, 48(%rcx)
	jne	.L3995
	movl	$-1, 32(%rdx)
	movq	24(%rcx), %rsi
	movb	$1, 48(%rcx)
	movq	%rsi, 16(%rcx)
	movb	$109, 96(%rdx)
	movb	$109, 176(%rdx)
	movb	$109, 256(%rdx)
	jmp	.L3995
	.p2align 4,,10
	.p2align 3
.L4039:
	subq	%rax, %rsi
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm
	jmp	.L4008
	.p2align 4,,10
	.p2align 3
.L4038:
	movq	-144(%rbp), %rax
	movl	-96(%rbp), %ecx
	xorl	%r8d, %r8d
	movq	128(%rax), %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	-152(%rbp), %rax
	movq	200(%rax), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L4005
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L4005
	.p2align 4,,10
	.p2align 3
.L4001:
	movb	$0, -127(%rbp)
	xorl	%eax, %eax
	jmp	.L4013
	.p2align 4,,10
	.p2align 3
.L3991:
	movb	$0, -127(%rbp)
	xorl	%eax, %eax
	jmp	.L4014
.L4036:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25886:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseStandardForLoopEiPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_PNS0_19PreParserExpressionEPNS0_18PreParserStatementESD_, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseStandardForLoopEiPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_PNS0_19PreParserExpressionEPNS0_18PreParserStatementESD_
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE18ParseWithStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE18ParseWithStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE18ParseWithStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE18ParseWithStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE18ParseWithStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE:
.LFB25544:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	200(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	(%r12), %rax
	movzbl	129(%rax), %eax
	andl	$1, %eax
	je	.L4044
	movl	$300, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE13ReportMessageENS0_15MessageTemplateE
	xorl	%eax, %eax
.L4045:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4072
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4044:
	.cfi_restore_state
	movq	200(%r12), %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$5, %al
	jne	.L4073
.L4046:
	movq	144(%r12), %rdx
	movq	%r12, -128(%rbp)
	movb	$0, -112(%rbp)
	movq	%rdx, -120(%rbp)
	testq	%rdx, %rdx
	je	.L4047
	cmpb	$0, 17(%rdx)
	movzbl	18(%rdx), %eax
	setne	-111(%rbp)
.L4059:
	movb	%al, -110(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, 144(%r12)
	leaq	176(%r12), %rax
	movq	%rax, -104(%rbp)
	movq	184(%r12), %rax
	subq	176(%r12), %rax
	sarq	$4, %rax
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movups	%xmm0, -72(%rbp)
	testq	%rdx, %rdx
	je	.L4050
	cmpb	$2, 16(%rdx)
	jbe	.L4074
.L4050:
	movb	%bl, -56(%rbp)
	movq	%r12, %rdi
	movzbl	256(%r12), %ebx
	movb	$1, 256(%r12)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	movl	$-1, -64(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE27ParseExpressionCoverGrammarEv
	movl	-72(%rbp), %esi
	movl	-68(%rbp), %edx
	cmpl	%edx, %esi
	jbe	.L4075
.L4051:
	movq	-104(%rbp), %rdi
	movq	-96(%rbp), %rsi
	movb	%bl, 256(%r12)
	movq	8(%rdi), %rcx
	movq	(%rdi), %rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	ja	.L4076
	jb	.L4077
.L4054:
	movq	-128(%rbp), %rax
	movq	-120(%rbp), %rdx
	movq	%rdx, 144(%rax)
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$6, %al
	jne	.L4078
.L4055:
	movq	136(%r12), %r14
	movq	(%r12), %rdx
	movq	16(%r14), %rbx
	movq	24(%r14), %rax
	subq	%rbx, %rax
	cmpq	$135, %rax
	jbe	.L4079
	leaq	136(%rbx), %rax
	movq	%rax, 16(%r14)
.L4057:
	movq	%r14, %rsi
	movl	$7, %ecx
	movq	%rbx, %rdi
	call	_ZN2v88internal5ScopeC1EPNS0_4ZoneEPS1_NS0_9ScopeTypeE@PLT
	movq	(%r12), %r14
	movq	%rbx, (%r12)
	xorl	%edx, %edx
	movq	200(%r12), %rax
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rax
	movl	(%rax), %eax
	movl	%eax, 112(%rbx)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE
	movq	200(%r12), %rax
	movq	(%rax), %rax
	movl	4(%rax), %eax
	movl	%eax, 116(%rbx)
	movl	$2, %eax
	movq	%r14, (%r12)
	jmp	.L4045
	.p2align 4,,10
	.p2align 3
.L4074:
	movzbl	72(%rdx), %ebx
	jmp	.L4050
	.p2align 4,,10
	.p2align 3
.L4077:
	salq	$4, %rsi
	addq	%rdx, %rsi
	cmpq	%rsi, %rcx
	je	.L4054
	movq	%rsi, 8(%rdi)
	jmp	.L4054
	.p2align 4,,10
	.p2align 3
.L4073:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L4046
	.p2align 4,,10
	.p2align 3
.L4076:
	subq	%rax, %rsi
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm
	jmp	.L4054
	.p2align 4,,10
	.p2align 3
.L4075:
	movq	-128(%rbp), %r14
	movl	-80(%rbp), %ecx
	xorl	%r8d, %r8d
	movq	128(%r14), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r14), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L4051
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L4051
	.p2align 4,,10
	.p2align 3
.L4047:
	movb	$0, -111(%rbp)
	xorl	%eax, %eax
	jmp	.L4059
	.p2align 4,,10
	.p2align 3
.L4078:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L4055
	.p2align 4,,10
	.p2align 3
.L4079:
	movl	$136, %esi
	movq	%r14, %rdi
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-136(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L4057
.L4072:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25544:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE18ParseWithStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE18ParseWithStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE34ParseExpressionOrLabelledStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE34ParseExpressionOrLabelledStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE34ParseExpressionOrLabelledStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE34ParseExpressionOrLabelledStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE34ParseExpressionOrLabelledStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE:
.LFB25547:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -152(%rbp)
	movq	200(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movzbl	56(%rax), %ebx
	cmpb	$98, %bl
	je	.L4081
	jbe	.L4150
	cmpb	$103, %bl
	jne	.L4084
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	xorl	%eax, %eax
.L4086:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4151
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4150:
	.cfi_restore_state
	cmpb	$8, %bl
	je	.L4083
	cmpb	$73, %bl
	je	.L4083
.L4084:
	movq	144(%r13), %rdx
	leal	-92(%rbx), %eax
	movq	%r13, -144(%rbp)
	movb	%al, -153(%rbp)
	movq	%rdx, -136(%rbp)
	movb	$0, -128(%rbp)
	testq	%rdx, %rdx
	je	.L4090
	cmpb	$0, 17(%rdx)
	movzbl	18(%rdx), %eax
	setne	-127(%rbp)
.L4111:
	movb	%al, -126(%rbp)
	leaq	-144(%rbp), %rax
	movq	%rax, 144(%r13)
	leaq	176(%r13), %rax
	movq	%rax, -120(%rbp)
	movq	184(%r13), %rax
	subq	176(%r13), %rax
	sarq	$4, %rax
	movq	%rax, %xmm0
	xorl	%eax, %eax
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movups	%xmm0, -88(%rbp)
	testq	%rdx, %rdx
	je	.L4093
	cmpb	$2, 16(%rdx)
	ja	.L4093
	movzbl	72(%rdx), %eax
.L4093:
	movzbl	256(%r13), %r15d
	movq	%r13, %rdi
	movb	$1, 256(%r13)
	movb	%al, -72(%rbp)
	movq	$0, -96(%rbp)
	movl	$-1, -88(%rbp)
	movl	$-1, -80(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE27ParseExpressionCoverGrammarEv
	movl	-88(%rbp), %esi
	movl	-84(%rbp), %edx
	movl	%eax, %ebx
	cmpl	%edx, %esi
	jbe	.L4152
.L4094:
	movq	200(%r13), %rax
	andl	$7, %ebx
	movq	-120(%rbp), %rdi
	movq	-112(%rbp), %rsi
	movq	8(%rax), %rax
	cmpb	$9, 56(%rax)
	jne	.L4096
	cmpb	$9, -153(%rbp)
	jbe	.L4153
.L4096:
	movb	%r15b, 256(%r13)
	movq	8(%rdi), %r8
	movq	(%rdi), %rdx
	movq	%r8, %rax
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	ja	.L4154
	jnb	.L4099
	salq	$4, %rsi
	addq	%rsi, %rdx
	cmpq	%rdx, %r8
	je	.L4099
	movq	%rdx, 8(%rdi)
.L4099:
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	%rdx, 144(%rax)
	movq	200(%r13), %rdi
	movq	8(%rdi), %rax
	movzbl	56(%rax), %edx
	cmpb	$12, %dl
	jne	.L4100
	call	_ZN2v88internal7Scanner4NextEv@PLT
.L4101:
	cmpl	$1, %ebx
	je	.L4089
	cmpl	$4, %ebx
	movl	$2, %eax
	cmove	%ebx, %eax
	jmp	.L4086
	.p2align 4,,10
	.p2align 3
.L4081:
	call	_ZN2v88internal7Scanner9PeekAheadEv@PLT
	cmpb	$3, %al
	je	.L4087
	movq	200(%r13), %rbx
	cmpb	$8, %al
	je	.L4088
	cmpb	$92, %al
	je	.L4088
	movq	8(%rbx), %rax
	movzbl	56(%rax), %ebx
	jmp	.L4084
	.p2align 4,,10
	.p2align 3
.L4090:
	movb	$0, -127(%rbp)
	xorl	%eax, %eax
	jmp	.L4111
	.p2align 4,,10
	.p2align 3
.L4088:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Scanner9PeekAheadEv@PLT
	movq	16(%rbx), %rax
	cmpb	$0, 76(%rax)
	jne	.L4155
.L4087:
	movq	200(%r13), %rax
	movq	128(%r13), %rdi
	xorl	%r8d, %r8d
	movl	$325, %ecx
	movq	8(%rax), %rax
	movl	4(%rax), %edx
	movl	(%rax), %esi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r13), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L4089
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	.p2align 4,,10
	.p2align 3
.L4089:
	xorl	%eax, %eax
	jmp	.L4086
	.p2align 4,,10
	.p2align 3
.L4152:
	movq	-144(%rbp), %r10
	movl	-96(%rbp), %ecx
	xorl	%r8d, %r8d
	movq	128(%r10), %rdi
	movq	%r10, -168(%rbp)
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	-168(%rbp), %r10
	movq	200(%r10), %rdx
	movq	24(%rdx), %rcx
	cmpb	$0, 48(%rcx)
	jne	.L4094
	movl	$-1, 32(%rdx)
	movq	24(%rcx), %rsi
	movb	$1, 48(%rcx)
	movq	%rsi, 16(%rcx)
	movb	$109, 96(%rdx)
	movb	$109, 176(%rdx)
	movb	$109, 256(%rdx)
	jmp	.L4094
	.p2align 4,,10
	.p2align 3
.L4154:
	subq	%rax, %rsi
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm
	jmp	.L4099
	.p2align 4,,10
	.p2align 3
.L4153:
	cmpl	$3, %ebx
	jne	.L4096
	salq	$4, %rsi
	addq	(%rdi), %rsi
	movq	0(%r13), %rdi
	movq	(%rsi), %rsi
	call	_ZN2v88internal5Scope16DeleteUnresolvedEPNS0_13VariableProxyE@PLT
	movq	200(%r13), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r13), %rax
	movq	8(%rax), %rax
	cmpb	$73, 56(%rax)
	jne	.L4105
	movq	0(%r13), %rax
	testb	$1, 129(%rax)
	jne	.L4105
	testl	%r12d, %r12d
	je	.L4106
.L4105:
	movq	-152(%rbp), %rsi
	movl	%r12d, %ecx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE
.L4108:
	movq	-112(%rbp), %rsi
	movq	-120(%rbp), %rdi
	movb	%r15b, 256(%r13)
	movl	%eax, -152(%rbp)
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE6resizeEm
	movq	-144(%rbp), %rdx
	movq	-136(%rbp), %rcx
	movl	-152(%rbp), %eax
	movq	%rcx, 144(%rdx)
	jmp	.L4086
	.p2align 4,,10
	.p2align 3
.L4100:
	cmpb	$0, 76(%rax)
	jne	.L4101
	subl	$12, %edx
	cmpb	$2, %dl
	jbe	.L4101
	movq	(%rdi), %rax
	cmpb	$96, 56(%rax)
	je	.L4102
.L4103:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L4101
	.p2align 4,,10
	.p2align 3
.L4155:
	movq	200(%r13), %rax
	movq	8(%rax), %rax
	movzbl	56(%rax), %ebx
	jmp	.L4084
.L4106:
	movq	%r13, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE24ParseFunctionDeclarationEv
	jmp	.L4108
.L4083:
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4151:
	call	__stack_chk_fail@PLT
.L4102:
	movq	16(%r13), %rax
	movq	48(%rax), %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$9, %eax
	cmpb	$4, %al
	ja	.L4146
	movq	200(%r13), %rdi
	jmp	.L4103
.L4146:
	movq	200(%r13), %rax
	movl	$16, %edx
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE15ReportMessageAtENS0_7Scanner8LocationENS0_15MessageTemplateE
	jmp	.L4101
	.cfi_endproc
.LFE25547:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE34ParseExpressionOrLabelledStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE34ParseExpressionOrLabelledStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE
	.section	.text._ZN2v88internal10ParserBaseINS0_9PreParserEE16ParseIfStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE,"axG",@progbits,_ZN2v88internal10ParserBaseINS0_9PreParserEE16ParseIfStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10ParserBaseINS0_9PreParserEE16ParseIfStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	.type	_ZN2v88internal10ParserBaseINS0_9PreParserEE16ParseIfStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE, @function
_ZN2v88internal10ParserBaseINS0_9PreParserEE16ParseIfStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE:
.LFB25531:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	200(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$5, %al
	jne	.L4208
.L4157:
	movq	144(%r12), %rdx
	movq	%r12, -144(%rbp)
	movb	$0, -128(%rbp)
	movq	%rdx, -136(%rbp)
	testq	%rdx, %rdx
	je	.L4158
	cmpb	$0, 17(%rdx)
	movzbl	18(%rdx), %eax
	setne	-127(%rbp)
.L4189:
	movb	%al, -126(%rbp)
	leaq	-144(%rbp), %rax
	movq	%rax, 144(%r12)
	leaq	176(%r12), %rax
	movq	%rax, -120(%rbp)
	movq	184(%r12), %rax
	subq	176(%r12), %rax
	sarq	$4, %rax
	movq	%rax, %xmm0
	xorl	%eax, %eax
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movups	%xmm0, -88(%rbp)
	testq	%rdx, %rdx
	je	.L4161
	cmpb	$2, 16(%rdx)
	jbe	.L4209
.L4161:
	movzbl	256(%r12), %ebx
	movq	%r12, %rdi
	movb	$1, 256(%r12)
	movb	%al, -72(%rbp)
	movq	$0, -96(%rbp)
	movl	$-1, -88(%rbp)
	movl	$-1, -80(%rbp)
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE27ParseExpressionCoverGrammarEv
	movl	-88(%rbp), %esi
	movl	-84(%rbp), %edx
	cmpl	%edx, %esi
	jbe	.L4210
.L4162:
	movq	-120(%rbp), %rdi
	movq	-112(%rbp), %rsi
	movb	%bl, 256(%r12)
	movq	8(%rdi), %rcx
	movq	(%rdi), %rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	ja	.L4211
	jb	.L4212
.L4165:
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	%rdx, 144(%rax)
	movq	200(%r12), %rdi
	call	_ZN2v88internal7Scanner4NextEv@PLT
	cmpb	$6, %al
	jne	.L4213
.L4166:
	xorl	%r8d, %r8d
	testq	%r13, %r13
	je	.L4167
	movq	136(%r12), %r14
	movq	16(%r14), %rbx
	movq	24(%r14), %rax
	subq	%rbx, %rax
	cmpq	$15, %rax
	jbe	.L4214
	leaq	16(%rbx), %rax
	movq	%rax, 16(%r14)
.L4169:
	movslq	12(%r13), %rsi
	movq	%rbx, %r8
	movq	%rsi, %rdx
	testl	%esi, %esi
	jg	.L4215
	movq	$0, (%rbx)
	movl	%esi, 8(%rbx)
.L4178:
	movl	%edx, %r15d
.L4180:
	movl	%r15d, 12(%rbx)
	.p2align 4,,10
	.p2align 3
.L4167:
	movq	(%r12), %rax
	testb	$1, 129(%rax)
	jne	.L4183
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	cmpb	$73, 56(%rax)
	je	.L4216
.L4183:
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE
	movl	%eax, %ebx
.L4184:
	movq	200(%r12), %rdi
	movl	$2, %eax
	movq	8(%rdi), %rdx
	cmpb	$70, 56(%rdx)
	je	.L4217
.L4185:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4218
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4209:
	.cfi_restore_state
	movzbl	72(%rdx), %eax
	jmp	.L4161
	.p2align 4,,10
	.p2align 3
.L4212:
	salq	$4, %rsi
	addq	%rdx, %rsi
	cmpq	%rsi, %rcx
	je	.L4165
	movq	%rsi, 8(%rdi)
	jmp	.L4165
	.p2align 4,,10
	.p2align 3
.L4211:
	subq	%rax, %rsi
	call	_ZNSt6vectorISt4pairIPN2v88internal13VariableProxyEiESaIS5_EE17_M_default_appendEm
	jmp	.L4165
	.p2align 4,,10
	.p2align 3
.L4210:
	movq	-144(%rbp), %r14
	movl	-96(%rbp), %ecx
	xorl	%r8d, %r8d
	movq	128(%r14), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc@PLT
	movq	200(%r14), %rax
	movq	24(%rax), %rdx
	cmpb	$0, 48(%rdx)
	jne	.L4162
	movl	$-1, 32(%rax)
	movq	24(%rdx), %rcx
	movb	$1, 48(%rdx)
	movq	%rcx, 16(%rdx)
	movb	$109, 96(%rax)
	movb	$109, 176(%rax)
	movb	$109, 256(%rax)
	jmp	.L4162
	.p2align 4,,10
	.p2align 3
.L4158:
	movb	$0, -127(%rbp)
	xorl	%eax, %eax
	jmp	.L4189
	.p2align 4,,10
	.p2align 3
.L4215:
	movq	16(%r14), %r9
	movq	24(%r14), %rax
	salq	$3, %rsi
	subq	%r9, %rax
	cmpq	%rax, %rsi
	ja	.L4219
	addq	%r9, %rsi
	movq	%rsi, 16(%r14)
.L4172:
	movslq	12(%r13), %rsi
	movq	0(%r13), %rcx
	movq	%r9, (%rbx)
	movl	%edx, 8(%rbx)
	movl	$0, 12(%rbx)
	movq	%rsi, %r15
	cmpl	%edx, %esi
	jle	.L4173
	movq	16(%r14), %rdi
	movq	24(%r14), %rax
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L4220
	addq	%rdi, %rsi
	movq	%rsi, 16(%r14)
.L4175:
	movslq	12(%rbx), %rdx
	movq	%rdi, %r9
	testl	%edx, %edx
	jle	.L4176
	movq	(%rbx), %rsi
	salq	$3, %rdx
	movq	%rdi, -168(%rbp)
	movq	%rcx, -160(%rbp)
	movq	%r8, -152(%rbp)
	call	memcpy@PLT
	movslq	12(%rbx), %rdx
	movq	-168(%rbp), %r9
	movq	-160(%rbp), %rcx
	movq	-152(%rbp), %r8
	movq	%rax, %rdi
.L4176:
	movq	%rdi, (%rbx)
	movl	%r15d, 8(%rbx)
.L4177:
	movq	(%rcx), %rax
	movq	%rax, (%r9,%rdx,8)
	cmpl	$1, %r15d
	je	.L4180
	leal	-2(%r15), %esi
	movl	$1, %eax
	addq	$2, %rsi
	.p2align 4,,10
	.p2align 3
.L4181:
	movl	12(%rbx), %edx
	movq	(%rcx,%rax,8), %r9
	movq	(%rbx), %rdi
	addl	%eax, %edx
	addq	$1, %rax
	movslq	%edx, %rdx
	movq	%r9, (%rdi,%rdx,8)
	cmpq	%rsi, %rax
	jne	.L4181
	movl	%r15d, 12(%rbx)
	jmp	.L4167
	.p2align 4,,10
	.p2align 3
.L4216:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseScopedStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE.part.0
	movl	%eax, %ebx
	jmp	.L4184
	.p2align 4,,10
	.p2align 3
.L4214:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L4169
	.p2align 4,,10
	.p2align 3
.L4173:
	testl	%esi, %esi
	jle	.L4194
	xorl	%edx, %edx
	jmp	.L4177
	.p2align 4,,10
	.p2align 3
.L4217:
	call	_ZN2v88internal7Scanner4NextEv@PLT
	movq	(%r12), %rax
	testb	$1, 129(%rax)
	jne	.L4186
	movq	200(%r12), %rax
	movq	8(%rax), %rax
	cmpb	$73, 56(%rax)
	je	.L4221
.L4186:
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE14ParseStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEES9_NS0_30AllowLabelledFunctionStatementE
.L4187:
	cmpl	$3, %eax
	cmove	%ebx, %eax
	jmp	.L4185
	.p2align 4,,10
	.p2align 3
.L4213:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L4166
	.p2align 4,,10
	.p2align 3
.L4208:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE21ReportUnexpectedTokenENS0_5Token5ValueE.constprop.0
	jmp	.L4157
	.p2align 4,,10
	.p2align 3
.L4219:
	movq	%r14, %rdi
	movl	%edx, -160(%rbp)
	movq	%rbx, -152(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-152(%rbp), %r8
	movl	-160(%rbp), %edx
	movq	%rax, %r9
	jmp	.L4172
	.p2align 4,,10
	.p2align 3
.L4221:
	movq	%r12, %rdi
	call	_ZN2v88internal10ParserBaseINS0_9PreParserEE20ParseScopedStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE.part.0
	jmp	.L4187
	.p2align 4,,10
	.p2align 3
.L4220:
	movq	%r14, %rdi
	movq	%rcx, -160(%rbp)
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-152(%rbp), %r8
	movq	-160(%rbp), %rcx
	movq	%rax, %rdi
	jmp	.L4175
.L4218:
	call	__stack_chk_fail@PLT
.L4194:
	movl	%esi, %edx
	jmp	.L4178
	.cfi_endproc
.LFE25531:
	.size	_ZN2v88internal10ParserBaseINS0_9PreParserEE16ParseIfStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE, .-_ZN2v88internal10ParserBaseINS0_9PreParserEE16ParseIfStatementEPNS0_8ZoneListIPKNS0_12AstRawStringEEE
	.section	.text.startup._GLOBAL__sub_I__ZNK2v88internal9PreParser13GetIdentifierEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZNK2v88internal9PreParser13GetIdentifierEv, @function
_GLOBAL__sub_I__ZNK2v88internal9PreParser13GetIdentifierEv:
.LFB26588:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE26588:
	.size	_GLOBAL__sub_I__ZNK2v88internal9PreParser13GetIdentifierEv, .-_GLOBAL__sub_I__ZNK2v88internal9PreParser13GetIdentifierEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZNK2v88internal9PreParser13GetIdentifierEv
	.section	.rodata.CSWTCH.1484,"a"
	.type	CSWTCH.1484, @object
	.size	CSWTCH.1484, 4
CSWTCH.1484:
	.byte	7
	.byte	9
	.byte	8
	.byte	1
	.weak	_ZZN2v88internal10ParserBaseINS0_9PreParserEE19FunctionKindForImplEbNS_4base5FlagsINS0_17ParseFunctionFlagEiEEE14kFunctionKinds
	.section	.rodata._ZZN2v88internal10ParserBaseINS0_9PreParserEE19FunctionKindForImplEbNS_4base5FlagsINS0_17ParseFunctionFlagEiEEE14kFunctionKinds,"aG",@progbits,_ZZN2v88internal10ParserBaseINS0_9PreParserEE19FunctionKindForImplEbNS_4base5FlagsINS0_17ParseFunctionFlagEiEEE14kFunctionKinds,comdat
	.align 8
	.type	_ZZN2v88internal10ParserBaseINS0_9PreParserEE19FunctionKindForImplEbNS_4base5FlagsINS0_17ParseFunctionFlagEiEEE14kFunctionKinds, @gnu_unique_object
	.size	_ZZN2v88internal10ParserBaseINS0_9PreParserEE19FunctionKindForImplEbNS_4base5FlagsINS0_17ParseFunctionFlagEiEEE14kFunctionKinds, 8
_ZZN2v88internal10ParserBaseINS0_9PreParserEE19FunctionKindForImplEbNS_4base5FlagsINS0_17ParseFunctionFlagEiEEE14kFunctionKinds:
	.byte	0
	.byte	10
	.byte	14
	.byte	13
	.byte	16
	.byte	11
	.byte	15
	.byte	12
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.long	-1
	.long	0
	.long	-1
	.long	0
	.align 16
.LC12:
	.long	0
	.long	0
	.long	-1
	.long	0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
