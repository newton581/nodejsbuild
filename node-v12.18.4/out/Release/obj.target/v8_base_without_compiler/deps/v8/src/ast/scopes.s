	.file	"scopes.cc"
	.text
	.section	.text._ZN2v88internal12_GLOBAL__N_120UpdateNeedsHoleCheckEPNS0_8VariableEPNS0_13VariableProxyEPNS0_5ScopeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_120UpdateNeedsHoleCheckEPNS0_8VariableEPNS0_13VariableProxyEPNS0_5ScopeE, @function
_ZN2v88internal12_GLOBAL__N_120UpdateNeedsHoleCheckEPNS0_8VariableEPNS0_13VariableProxyEPNS0_5ScopeE:
.LFB21294:
	.cfi_startproc
	movzwl	40(%rdi), %eax
	movl	%eax, %ecx
	andl	$15, %ecx
	cmpb	$6, %cl
	jne	.L2
	.p2align 4,,10
	.p2align 3
.L28:
	movq	16(%rdi), %rdi
	movzwl	40(%rdi), %eax
	movl	%eax, %ecx
	andl	$15, %ecx
	cmpb	$6, %cl
	je	.L28
.L2:
	movzwl	%ax, %ecx
	testb	$16, %ah
	jne	.L1
	sarl	$7, %ecx
	andl	$7, %ecx
	cmpb	$5, %cl
	je	.L29
.L5:
	movq	(%rdi), %r8
	movq	%r8, %rax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L6:
	movq	8(%rax), %rax
.L9:
	testb	$1, 130(%rax)
	je	.L6
	movzbl	128(%rax), %ecx
	cmpb	$6, %cl
	je	.L6
	testb	%cl, %cl
	je	.L6
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L10:
	movq	8(%rdx), %rdx
.L7:
	testb	$1, 130(%rdx)
	je	.L10
	movzbl	128(%rdx), %ecx
	cmpb	$6, %cl
	je	.L10
	testb	%cl, %cl
	je	.L10
	cmpq	%rax, %rdx
	je	.L13
.L14:
	andl	$-2049, 4(%rsi)
	orw	$8192, 40(%rdi)
.L1:
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	testb	$8, 129(%r8)
	jne	.L14
	movl	(%rsi), %eax
	cmpl	%eax, 36(%rdi)
	jge	.L14
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movl	32(%rdi), %eax
	testl	%eax, %eax
	jg	.L5
	jmp	.L14
	.cfi_endproc
.LFE21294:
	.size	_ZN2v88internal12_GLOBAL__N_120UpdateNeedsHoleCheckEPNS0_8VariableEPNS0_13VariableProxyEPNS0_5ScopeE, .-_ZN2v88internal12_GLOBAL__N_120UpdateNeedsHoleCheckEPNS0_8VariableEPNS0_13VariableProxyEPNS0_5ScopeE
	.section	.text._ZN2v88internal8Variable16SetMaybeAssignedEv,"axG",@progbits,_ZN2v88internal8Variable16SetMaybeAssignedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8Variable16SetMaybeAssignedEv
	.type	_ZN2v88internal8Variable16SetMaybeAssignedEv, @function
_ZN2v88internal8Variable16SetMaybeAssignedEv:
.LFB9374:
	.cfi_startproc
	endbr64
	movzwl	40(%rdi), %eax
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$1, %dl
	je	.L40
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L32
	testb	$64, %ah
	je	.L43
.L32:
	orb	$64, %ah
	movw	%ax, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movzwl	40(%rbx), %eax
	orb	$64, %ah
	movw	%ax, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9374:
	.size	_ZN2v88internal8Variable16SetMaybeAssignedEv, .-_ZN2v88internal8Variable16SetMaybeAssignedEv
	.section	.rodata._ZN2v88internal11VariableMapC2EPNS0_4ZoneE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Out of memory: HashMap::Initialize"
	.section	.text._ZN2v88internal11VariableMapC2EPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11VariableMapC2EPNS0_4ZoneE
	.type	_ZN2v88internal11VariableMapC2EPNS0_4ZoneE, @function
_ZN2v88internal11VariableMapC2EPNS0_4ZoneE:
.LFB21161:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	16(%rsi), %rax
	movq	24(%rsi), %rdx
	subq	%rax, %rdx
	cmpq	$191, %rdx
	jbe	.L49
	leaq	192(%rax), %rdx
	movq	%rdx, 16(%rsi)
.L46:
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L50
	movq	$0, (%rax)
	movq	$0, 24(%rax)
	movq	$0, 48(%rax)
	movq	$0, 72(%rax)
	movq	$0, 96(%rax)
	movq	$0, 120(%rax)
	movq	$0, 144(%rax)
	movq	$0, 168(%rax)
	movq	$8, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movl	$192, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L46
.L50:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21161:
	.size	_ZN2v88internal11VariableMapC2EPNS0_4ZoneE, .-_ZN2v88internal11VariableMapC2EPNS0_4ZoneE
	.globl	_ZN2v88internal11VariableMapC1EPNS0_4ZoneE
	.set	_ZN2v88internal11VariableMapC1EPNS0_4ZoneE,_ZN2v88internal11VariableMapC2EPNS0_4ZoneE
	.section	.text._ZN2v88internal11VariableMap6LookupEPKNS0_12AstRawStringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11VariableMap6LookupEPKNS0_12AstRawStringE
	.type	_ZN2v88internal11VariableMap6LookupEPKNS0_12AstRawStringE, @function
_ZN2v88internal11VariableMap6LookupEPKNS0_12AstRawStringE:
.LFB21166:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %ecx
	movl	24(%rsi), %eax
	movq	(%rdi), %r9
	subl	$1, %ecx
	shrl	$2, %eax
	andl	%ecx, %eax
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L60:
	cmpq	%r8, %rsi
	je	.L53
	addq	$1, %rax
	andq	%rcx, %rax
.L59:
	leaq	(%rax,%rax,2), %rdx
	leaq	(%r9,%rdx,8), %rdx
	movq	(%rdx), %r8
	testq	%r8, %r8
	jne	.L60
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	movq	8(%rdx), %r8
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE21166:
	.size	_ZN2v88internal11VariableMap6LookupEPKNS0_12AstRawStringE, .-_ZN2v88internal11VariableMap6LookupEPKNS0_12AstRawStringE
	.section	.text._ZN2v88internal5ScopeC2EPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5ScopeC2EPNS0_4ZoneE
	.type	_ZN2v88internal5ScopeC2EPNS0_4ZoneE, @function
_ZN2v88internal5ScopeC2EPNS0_4ZoneE:
.LFB21168:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	16(%rsi), %rax
	movq	24(%rsi), %rdx
	movq	%rsi, (%rbx)
	movq	$0, 8(%rbx)
	subq	%rax, %rdx
	cmpq	$191, %rdx
	jbe	.L69
	leaq	192(%rax), %rdx
	movq	%rdx, 16(%rsi)
.L63:
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	je	.L70
	movl	$8, 40(%rbx)
	movq	$0, (%rax)
	cmpl	$1, 40(%rbx)
	jbe	.L65
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L66:
	movq	32(%rbx), %rcx
	addq	$1, %rax
	movq	$0, (%rcx,%rdx)
	movl	40(%rbx), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L66
.L65:
	leaq	56(%rbx), %rax
	pxor	%xmm0, %xmm0
	andb	$-4, 130(%rbx)
	movq	%rax, 64(%rbx)
	leaq	72(%rbx), %rax
	movq	%rax, 80(%rbx)
	leaq	88(%rbx), %rax
	movups	%xmm0, 16(%rbx)
	movdqa	.LC1(%rip), %xmm0
	movq	%rax, 96(%rbx)
	movl	$4, %eax
	movl	$0, 44(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 72(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 104(%rbx)
	movw	%ax, 128(%rbx)
	movups	%xmm0, 112(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	movl	$192, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L63
.L70:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21168:
	.size	_ZN2v88internal5ScopeC2EPNS0_4ZoneE, .-_ZN2v88internal5ScopeC2EPNS0_4ZoneE
	.globl	_ZN2v88internal5ScopeC1EPNS0_4ZoneE
	.set	_ZN2v88internal5ScopeC1EPNS0_4ZoneE,_ZN2v88internal5ScopeC2EPNS0_4ZoneE
	.section	.text._ZN2v88internal5ScopeC2EPNS0_4ZoneEPS1_NS0_9ScopeTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5ScopeC2EPNS0_4ZoneEPS1_NS0_9ScopeTypeE
	.type	_ZN2v88internal5ScopeC2EPNS0_4ZoneEPS1_NS0_9ScopeTypeE, @function
_ZN2v88internal5ScopeC2EPNS0_4ZoneEPS1_NS0_9ScopeTypeE:
.LFB21171:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$16, %rsp
	movq	16(%rsi), %rax
	movq	24(%rsi), %rdx
	movups	%xmm0, (%rbx)
	subq	%rax, %rdx
	cmpq	$191, %rdx
	jbe	.L79
	leaq	192(%rax), %rdx
	movq	%rdx, 16(%rsi)
.L73:
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	je	.L80
	movl	$8, 40(%rbx)
	movq	$0, (%rax)
	cmpl	$1, 40(%rbx)
	jbe	.L75
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L76:
	movq	32(%rbx), %rsi
	addq	$1, %rax
	movq	$0, (%rsi,%rdx)
	movl	40(%rbx), %esi
	addq	$24, %rdx
	cmpq	%rax, %rsi
	ja	.L76
.L75:
	leaq	56(%rbx), %rax
	movq	$0, 56(%rbx)
	pxor	%xmm0, %xmm0
	movq	%rax, 64(%rbx)
	leaq	72(%rbx), %rax
	movq	%rax, 80(%rbx)
	leaq	88(%rbx), %rax
	movq	$0, 72(%rbx)
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movq	$0, 104(%rbx)
	movzbl	129(%r12), %eax
	movups	%xmm0, 16(%rbx)
	movdqa	.LC1(%rip), %xmm0
	andl	$1, %eax
	movl	$0, 44(%rbx)
	andw	$-1023, 129(%rbx)
	movb	%al, 129(%rbx)
	movq	8(%rbx), %rax
	movb	%cl, 128(%rbx)
	movups	%xmm0, 112(%rbx)
	movq	16(%rax), %rdx
	movq	%rdx, 24(%rbx)
	movq	%rbx, 16(%rax)
	movq	%rax, 8(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movl	$192, %esi
	movl	%ecx, -20(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-20(%rbp), %ecx
	jmp	.L73
.L80:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21171:
	.size	_ZN2v88internal5ScopeC2EPNS0_4ZoneEPS1_NS0_9ScopeTypeE, .-_ZN2v88internal5ScopeC2EPNS0_4ZoneEPS1_NS0_9ScopeTypeE
	.globl	_ZN2v88internal5ScopeC1EPNS0_4ZoneEPS1_NS0_9ScopeTypeE
	.set	_ZN2v88internal5ScopeC1EPNS0_4ZoneEPS1_NS0_9ScopeTypeE,_ZN2v88internal5ScopeC2EPNS0_4ZoneEPS1_NS0_9ScopeTypeE
	.section	.text._ZN2v88internal16DeclarationScopeC2EPNS0_4ZoneEPNS0_5ScopeENS0_9ScopeTypeENS0_12FunctionKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16DeclarationScopeC2EPNS0_4ZoneEPNS0_5ScopeENS0_9ScopeTypeENS0_12FunctionKindE
	.type	_ZN2v88internal16DeclarationScopeC2EPNS0_4ZoneEPNS0_5ScopeENS0_9ScopeTypeENS0_12FunctionKindE, @function
_ZN2v88internal16DeclarationScopeC2EPNS0_4ZoneEPNS0_5ScopeENS0_9ScopeTypeENS0_12FunctionKindE:
.LFB21180:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movups	%xmm0, (%rdi)
	movq	16(%rsi), %rax
	movq	24(%rsi), %rdx
	subq	%rax, %rdx
	cmpq	$191, %rdx
	jbe	.L93
	leaq	192(%rax), %rdx
	movq	%rdx, 16(%rsi)
.L83:
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	je	.L94
	movl	$8, 40(%rbx)
	movq	$0, (%rax)
	cmpl	$1, 40(%rbx)
	jbe	.L85
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L86:
	movq	32(%rbx), %rcx
	addq	$1, %rax
	movq	$0, (%rcx,%rdx)
	movl	40(%rbx), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L86
.L85:
	leaq	56(%rbx), %rax
	movq	$0, 56(%rbx)
	pxor	%xmm0, %xmm0
	movq	%rax, 64(%rbx)
	leaq	72(%rbx), %rax
	movq	%rax, 80(%rbx)
	leaq	88(%rbx), %rax
	movq	$0, 72(%rbx)
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movq	$0, 104(%rbx)
	movl	$0, 44(%rbx)
	movzbl	129(%r13), %eax
	movups	%xmm0, 16(%rbx)
	movdqa	.LC1(%rip), %xmm0
	andl	$1, %eax
	movb	%r15b, 128(%rbx)
	andw	$-1023, 129(%rbx)
	movb	%al, 129(%rbx)
	movq	8(%rbx), %rax
	movups	%xmm0, 112(%rbx)
	movq	16(%rax), %rdx
	movq	%rdx, 24(%rbx)
	movq	%rbx, 16(%rax)
	movq	%rax, 8(%rbx)
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	movb	%r8b, 133(%rbx)
	movl	$0, 136(%rbx)
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L95
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%r12)
.L88:
	movq	%rax, 144(%rbx)
	leaq	160(%rbx), %rax
	movzbl	128(%rbx), %edx
	movq	%rax, 168(%rbx)
	movzwl	130(%rbx), %eax
	andb	$-13, 132(%rbx)
	movq	$4, 152(%rbx)
	andw	$-14082, %ax
	movq	$0, 160(%rbx)
	orw	$257, %ax
	cmpb	$3, %dl
	movw	%ax, 130(%rbx)
	sete	%al
	movq	$0, 216(%rbx)
	cmpb	$2, %dl
	jne	.L90
	movzbl	133(%rbx), %eax
	subl	$8, %eax
	cmpb	$1, %al
	seta	%al
.L90:
	movzwl	131(%rbx), %edx
	movzbl	%al, %eax
	pxor	%xmm0, %xmm0
	movq	$0, 208(%rbx)
	sall	$12, %eax
	movups	%xmm0, 176(%rbx)
	andw	$-4553, %dx
	movups	%xmm0, 192(%rbx)
	orl	%edx, %eax
	movw	%ax, 131(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L93:
	movl	$192, %esi
	movq	%r12, %rdi
	movl	%r8d, -36(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-36(%rbp), %r8d
	jmp	.L83
.L94:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21180:
	.size	_ZN2v88internal16DeclarationScopeC2EPNS0_4ZoneEPNS0_5ScopeENS0_9ScopeTypeENS0_12FunctionKindE, .-_ZN2v88internal16DeclarationScopeC2EPNS0_4ZoneEPNS0_5ScopeENS0_9ScopeTypeENS0_12FunctionKindE
	.globl	_ZN2v88internal16DeclarationScopeC1EPNS0_4ZoneEPNS0_5ScopeENS0_9ScopeTypeENS0_12FunctionKindE
	.set	_ZN2v88internal16DeclarationScopeC1EPNS0_4ZoneEPNS0_5ScopeENS0_9ScopeTypeENS0_12FunctionKindE,_ZN2v88internal16DeclarationScopeC2EPNS0_4ZoneEPNS0_5ScopeENS0_9ScopeTypeENS0_12FunctionKindE
	.section	.text._ZN2v88internal11ModuleScopeC2EPNS0_16DeclarationScopeEPNS0_15AstValueFactoryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11ModuleScopeC2EPNS0_16DeclarationScopeEPNS0_15AstValueFactoryE
	.type	_ZN2v88internal11ModuleScopeC2EPNS0_16DeclarationScopeEPNS0_15AstValueFactoryE, @function
_ZN2v88internal11ModuleScopeC2EPNS0_16DeclarationScopeEPNS0_15AstValueFactoryE:
.LFB21186:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	1096(%rdx), %r12
	movq	%rdi, %rbx
	movq	%rsi, 8(%rdi)
	movq	%r12, (%rdi)
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	subq	%rax, %rdx
	cmpq	$191, %rdx
	jbe	.L114
	leaq	192(%rax), %rdx
	movq	%rdx, 16(%r12)
.L98:
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	je	.L115
	movl	$8, 40(%rbx)
	movq	$0, (%rax)
	cmpl	$1, 40(%rbx)
	jbe	.L100
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L101:
	movq	32(%rbx), %rcx
	addq	$1, %rax
	movq	$0, (%rcx,%rdx)
	movl	40(%rbx), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L101
.L100:
	leaq	56(%rbx), %rax
	movl	$0, 44(%rbx)
	pxor	%xmm0, %xmm0
	movq	%rax, 64(%rbx)
	leaq	72(%rbx), %rax
	movq	%rax, 80(%rbx)
	leaq	88(%rbx), %rax
	movq	%rax, 96(%rbx)
	movzwl	128(%rbx), %eax
	movq	$0, 56(%rbx)
	andw	$256, %ax
	movq	$0, 72(%rbx)
	orl	$3, %eax
	movq	$0, 88(%rbx)
	movw	%ax, 128(%rbx)
	movq	$0, 104(%rbx)
	movzbl	129(%r14), %eax
	movups	%xmm0, 16(%rbx)
	movdqa	.LC1(%rip), %xmm0
	andl	$1, %eax
	andb	$-4, 130(%rbx)
	movb	%al, 129(%rbx)
	movq	8(%rbx), %rax
	movups	%xmm0, 112(%rbx)
	movq	16(%rax), %rdx
	movq	%rdx, 24(%rbx)
	movq	%rbx, 16(%rax)
	movq	%rax, 8(%rbx)
	movb	$1, 133(%rbx)
	movl	$0, 136(%rbx)
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L116
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%r12)
.L103:
	movq	%rax, 144(%rbx)
	leaq	160(%rbx), %rax
	movzbl	128(%rbx), %edx
	movq	%rax, 168(%rbx)
	movzwl	130(%rbx), %eax
	andb	$-13, 132(%rbx)
	movq	$4, 152(%rbx)
	andw	$-14082, %ax
	movq	$0, 160(%rbx)
	orw	$257, %ax
	cmpb	$3, %dl
	movw	%ax, 130(%rbx)
	sete	%al
	movq	$0, 216(%rbx)
	cmpb	$2, %dl
	jne	.L105
	movzbl	133(%rbx), %eax
	subl	$8, %eax
	cmpb	$1, %al
	seta	%al
.L105:
	movzwl	131(%rbx), %edx
	movzbl	%al, %eax
	movq	1096(%r13), %r12
	pxor	%xmm0, %xmm0
	sall	$12, %eax
	movups	%xmm0, 176(%rbx)
	andw	$-4553, %dx
	movups	%xmm0, 192(%rbx)
	movq	$0, 208(%rbx)
	orl	%edx, %eax
	movw	%ax, 131(%rbx)
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	subq	%rax, %rdx
	cmpq	$231, %rdx
	jbe	.L117
	leaq	232(%rax), %rdx
	movq	%rdx, 16(%r12)
.L107:
	leaq	16(%rax), %rdx
	movq	%r12, (%rax)
	movq	%rdx, 32(%rax)
	movq	%rdx, 40(%rax)
	leaq	136(%rax), %rdx
	movq	%rdx, 152(%rax)
	movq	%rdx, 160(%rax)
	leaq	192(%rax), %rdx
	movq	%r12, 56(%rax)
	movq	%r12, 88(%rax)
	movq	%r12, 120(%rax)
	movq	%r12, 176(%rax)
	movq	%rdx, 208(%rax)
	movq	%rdx, 216(%rax)
	movl	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 48(%rax)
	movq	$0, 64(%rax)
	movq	$0, 72(%rax)
	movq	$0, 80(%rax)
	movq	$0, 96(%rax)
	movq	$0, 104(%rax)
	movq	$0, 112(%rax)
	movl	$0, 136(%rax)
	movq	$0, 144(%rax)
	movq	$0, 168(%rax)
	movl	$0, 192(%rax)
	movq	$0, 200(%rax)
	movq	$0, 224(%rax)
	movq	%rax, 224(%rbx)
	movzbl	133(%rbx), %eax
	orb	$1, 129(%rbx)
	leal	-4(%rax), %r14d
	movq	56(%r13), %rax
	movq	472(%rax), %r12
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$47, %rdx
	jbe	.L118
	leaq	48(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L109:
	cmpb	$2, %r14b
	movq	%rbx, %xmm0
	movq	%r12, %xmm1
	movq	$-1, 32(%rax)
	sbbl	%edx, %edx
	punpcklqdq	%xmm1, %xmm0
	andb	$-17, %dh
	movups	%xmm0, (%rax)
	pxor	%xmm0, %xmm0
	addw	$4130, %dx
	movups	%xmm0, 16(%rax)
	movw	%dx, 40(%rax)
	movq	%rax, 176(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L114:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L117:
	movl	$232, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L118:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L109
.L115:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21186:
	.size	_ZN2v88internal11ModuleScopeC2EPNS0_16DeclarationScopeEPNS0_15AstValueFactoryE, .-_ZN2v88internal11ModuleScopeC2EPNS0_16DeclarationScopeEPNS0_15AstValueFactoryE
	.globl	_ZN2v88internal11ModuleScopeC1EPNS0_16DeclarationScopeEPNS0_15AstValueFactoryE
	.set	_ZN2v88internal11ModuleScopeC1EPNS0_16DeclarationScopeEPNS0_15AstValueFactoryE,_ZN2v88internal11ModuleScopeC2EPNS0_16DeclarationScopeEPNS0_15AstValueFactoryE
	.section	.text._ZN2v88internal11ModuleScopeC2EPNS0_7IsolateENS0_6HandleINS0_9ScopeInfoEEEPNS0_15AstValueFactoryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11ModuleScopeC2EPNS0_7IsolateENS0_6HandleINS0_9ScopeInfoEEEPNS0_15AstValueFactoryE
	.type	_ZN2v88internal11ModuleScopeC2EPNS0_7IsolateENS0_6HandleINS0_9ScopeInfoEEEPNS0_15AstValueFactoryE, @function
_ZN2v88internal11ModuleScopeC2EPNS0_7IsolateENS0_6HandleINS0_9ScopeInfoEEEPNS0_15AstValueFactoryE:
.LFB21189:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	1096(%rcx), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, 8(%rbx)
	movq	%rdi, (%rbx)
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$191, %rdx
	jbe	.L134
	leaq	192(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L121:
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	je	.L135
	movl	$8, 40(%rbx)
	movq	$0, (%rax)
	cmpl	$1, 40(%rbx)
	jbe	.L123
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L124:
	movq	32(%rbx), %rcx
	addq	$1, %rax
	movq	$0, (%rcx,%rdx)
	movl	40(%rbx), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L124
.L123:
	leaq	56(%rbx), %rax
	pxor	%xmm0, %xmm0
	andb	$-4, 130(%rbx)
	movq	%rax, 64(%rbx)
	leaq	72(%rbx), %rax
	leaq	-48(%rbp), %r13
	movq	%rax, 80(%rbx)
	leaq	88(%rbx), %rax
	movq	%r13, %rdi
	movq	%rax, 96(%rbx)
	movl	$3, %eax
	movl	$0, 44(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 72(%rbx)
	movq	$0, 88(%rbx)
	movw	%ax, 128(%rbx)
	movq	(%r12), %rax
	movups	%xmm0, 16(%rbx)
	movdqa	.LC1(%rip), %xmm0
	movq	%r12, 104(%rbx)
	movups	%xmm0, 112(%rbx)
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal9ScopeInfo13language_modeEv@PLT
	movq	%r13, %rdi
	andl	$1, %eax
	movl	%eax, %edx
	movzbl	129(%rbx), %eax
	andl	$-2, %eax
	orl	%edx, %eax
	movb	%al, 129(%rbx)
	movq	(%r12), %rax
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal9ScopeInfo13ContextLengthEv@PLT
	orb	$2, 130(%rbx)
	movq	%r13, %rdi
	movl	%eax, 124(%rbx)
	movq	(%r12), %rax
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal9ScopeInfo13function_kindEv@PLT
	leaq	160(%rbx), %rdx
	movzbl	128(%rbx), %ecx
	andb	$-13, 132(%rbx)
	movq	%rdx, 168(%rbx)
	movzwl	130(%rbx), %edx
	movb	%al, 133(%rbx)
	movl	$0, 136(%rbx)
	andw	$-14082, %dx
	movq	$0, 144(%rbx)
	orw	$257, %dx
	cmpb	$3, %cl
	movw	%dx, 130(%rbx)
	sete	%dl
	movq	$0, 152(%rbx)
	movq	$0, 160(%rbx)
	movq	$0, 216(%rbx)
	cmpb	$2, %cl
	jne	.L126
	subl	$8, %eax
	cmpb	$1, %al
	seta	%dl
.L126:
	movzwl	131(%rbx), %eax
	movzbl	%dl, %edx
	pxor	%xmm0, %xmm0
	movq	$0, 208(%rbx)
	sall	$12, %edx
	movups	%xmm0, 176(%rbx)
	movq	%r13, %rdi
	andw	$-4553, %ax
	movups	%xmm0, 192(%rbx)
	orl	%eax, %edx
	movw	%dx, 131(%rbx)
	movq	(%r12), %rax
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal9ScopeInfo23SloppyEvalCanExtendVarsEv@PLT
	testb	%al, %al
	jne	.L136
.L127:
	orb	$1, 129(%rbx)
	movq	$0, 224(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L137
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	orb	$4, 129(%rbx)
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L134:
	movl	$192, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L121
.L135:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L137:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21189:
	.size	_ZN2v88internal11ModuleScopeC2EPNS0_7IsolateENS0_6HandleINS0_9ScopeInfoEEEPNS0_15AstValueFactoryE, .-_ZN2v88internal11ModuleScopeC2EPNS0_7IsolateENS0_6HandleINS0_9ScopeInfoEEEPNS0_15AstValueFactoryE
	.globl	_ZN2v88internal11ModuleScopeC1EPNS0_7IsolateENS0_6HandleINS0_9ScopeInfoEEEPNS0_15AstValueFactoryE
	.set	_ZN2v88internal11ModuleScopeC1EPNS0_7IsolateENS0_6HandleINS0_9ScopeInfoEEEPNS0_15AstValueFactoryE,_ZN2v88internal11ModuleScopeC2EPNS0_7IsolateENS0_6HandleINS0_9ScopeInfoEEEPNS0_15AstValueFactoryE
	.section	.text._ZN2v88internal10ClassScopeC2EPNS0_4ZoneEPNS0_5ScopeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ClassScopeC2EPNS0_4ZoneEPNS0_5ScopeE
	.type	_ZN2v88internal10ClassScopeC2EPNS0_4ZoneEPNS0_5ScopeE, @function
_ZN2v88internal10ClassScopeC2EPNS0_4ZoneEPNS0_5ScopeE:
.LFB21192:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movups	%xmm0, (%rbx)
	movq	16(%rsi), %rax
	movq	24(%rsi), %rdx
	subq	%rax, %rdx
	cmpq	$191, %rdx
	jbe	.L146
	leaq	192(%rax), %rdx
	movq	%rdx, 16(%rsi)
.L140:
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	je	.L147
	movl	$8, 40(%rbx)
	movq	$0, (%rax)
	cmpl	$1, 40(%rbx)
	jbe	.L142
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L143:
	movq	32(%rbx), %rcx
	addq	$1, %rax
	movq	$0, (%rcx,%rdx)
	movl	40(%rbx), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L143
.L142:
	leaq	56(%rbx), %rax
	movl	$0, 44(%rbx)
	pxor	%xmm0, %xmm0
	movq	%rax, 64(%rbx)
	leaq	72(%rbx), %rax
	movq	%rax, 80(%rbx)
	leaq	88(%rbx), %rax
	movq	$0, 56(%rbx)
	movq	$0, 72(%rbx)
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movq	$0, 104(%rbx)
	movzbl	129(%r12), %eax
	andw	$256, 128(%rbx)
	andl	$1, %eax
	movups	%xmm0, 16(%rbx)
	movdqa	.LC1(%rip), %xmm0
	movb	%al, 129(%rbx)
	movq	8(%rbx), %rax
	andb	$-4, 130(%rbx)
	movups	%xmm0, 112(%rbx)
	movq	16(%rax), %rdx
	movq	%rdx, 24(%rbx)
	movq	%rbx, 16(%rax)
	movq	%rax, 8(%rbx)
	orb	$1, 129(%rbx)
	movq	$0, 136(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	movl	$192, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L140
.L147:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21192:
	.size	_ZN2v88internal10ClassScopeC2EPNS0_4ZoneEPNS0_5ScopeE, .-_ZN2v88internal10ClassScopeC2EPNS0_4ZoneEPNS0_5ScopeE
	.globl	_ZN2v88internal10ClassScopeC1EPNS0_4ZoneEPNS0_5ScopeE
	.set	_ZN2v88internal10ClassScopeC1EPNS0_4ZoneEPNS0_5ScopeE,_ZN2v88internal10ClassScopeC2EPNS0_4ZoneEPNS0_5ScopeE
	.section	.text._ZN2v88internal5ScopeC2EPNS0_4ZoneENS0_9ScopeTypeENS0_6HandleINS0_9ScopeInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5ScopeC2EPNS0_4ZoneENS0_9ScopeTypeENS0_6HandleINS0_9ScopeInfoEEE
	.type	_ZN2v88internal5ScopeC2EPNS0_4ZoneENS0_9ScopeTypeENS0_6HandleINS0_9ScopeInfoEEE, @function
_ZN2v88internal5ScopeC2EPNS0_4ZoneENS0_9ScopeTypeENS0_6HandleINS0_9ScopeInfoEEE:
.LFB21198:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$40, %rsp
	movq	24(%rsi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %rax
	movq	%rsi, (%rbx)
	movq	$0, 8(%rbx)
	subq	%rax, %rcx
	cmpq	$191, %rcx
	jbe	.L157
	leaq	192(%rax), %rcx
	movq	%rcx, 16(%rsi)
.L150:
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	je	.L158
	movl	$8, 40(%rbx)
	movq	$0, (%rax)
	cmpl	$1, 40(%rbx)
	jbe	.L152
	movl	$24, %ecx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L153:
	movq	32(%rbx), %rsi
	addq	$1, %rax
	movq	$0, (%rsi,%rcx)
	movl	40(%rbx), %esi
	addq	$24, %rcx
	cmpq	%rax, %rsi
	ja	.L153
.L152:
	leaq	56(%rbx), %rax
	pxor	%xmm0, %xmm0
	andw	$-1024, 129(%rbx)
	movq	%rax, 64(%rbx)
	leaq	72(%rbx), %rax
	leaq	-48(%rbp), %r13
	movq	%rax, 80(%rbx)
	leaq	88(%rbx), %rax
	movq	%r13, %rdi
	movq	$0, 56(%rbx)
	movq	$0, 72(%rbx)
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movq	(%r12), %rax
	movb	%dl, 128(%rbx)
	movups	%xmm0, 16(%rbx)
	movdqa	.LC1(%rip), %xmm0
	movl	$0, 44(%rbx)
	movq	%r12, 104(%rbx)
	movups	%xmm0, 112(%rbx)
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal9ScopeInfo13language_modeEv@PLT
	movq	%r13, %rdi
	andl	$1, %eax
	movl	%eax, %edx
	movzbl	129(%rbx), %eax
	andl	$-2, %eax
	orl	%edx, %eax
	movb	%al, 129(%rbx)
	movq	(%r12), %rax
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal9ScopeInfo13ContextLengthEv@PLT
	orb	$2, 130(%rbx)
	movl	%eax, 124(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L159
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	movl	$192, %esi
	movl	%edx, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %edx
	jmp	.L150
.L158:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L159:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21198:
	.size	_ZN2v88internal5ScopeC2EPNS0_4ZoneENS0_9ScopeTypeENS0_6HandleINS0_9ScopeInfoEEE, .-_ZN2v88internal5ScopeC2EPNS0_4ZoneENS0_9ScopeTypeENS0_6HandleINS0_9ScopeInfoEEE
	.globl	_ZN2v88internal5ScopeC1EPNS0_4ZoneENS0_9ScopeTypeENS0_6HandleINS0_9ScopeInfoEEE
	.set	_ZN2v88internal5ScopeC1EPNS0_4ZoneENS0_9ScopeTypeENS0_6HandleINS0_9ScopeInfoEEE,_ZN2v88internal5ScopeC2EPNS0_4ZoneENS0_9ScopeTypeENS0_6HandleINS0_9ScopeInfoEEE
	.section	.text._ZN2v88internal16DeclarationScopeC2EPNS0_4ZoneENS0_9ScopeTypeENS0_6HandleINS0_9ScopeInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16DeclarationScopeC2EPNS0_4ZoneENS0_9ScopeTypeENS0_6HandleINS0_9ScopeInfoEEE
	.type	_ZN2v88internal16DeclarationScopeC2EPNS0_4ZoneENS0_9ScopeTypeENS0_6HandleINS0_9ScopeInfoEEE, @function
_ZN2v88internal16DeclarationScopeC2EPNS0_4ZoneENS0_9ScopeTypeENS0_6HandleINS0_9ScopeInfoEEE:
.LFB21201:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, (%rbx)
	movq	$0, 8(%rbx)
	movq	16(%rsi), %rax
	movq	24(%rsi), %rcx
	subq	%rax, %rcx
	cmpq	$191, %rcx
	jbe	.L175
	leaq	192(%rax), %rcx
	movq	%rcx, 16(%rsi)
.L162:
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	je	.L176
	movl	$8, 40(%rbx)
	movq	$0, (%rax)
	cmpl	$1, 40(%rbx)
	jbe	.L164
	movl	$24, %ecx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L165:
	movq	32(%rbx), %rsi
	addq	$1, %rax
	movq	$0, (%rsi,%rcx)
	movl	40(%rbx), %esi
	addq	$24, %rcx
	cmpq	%rax, %rsi
	ja	.L165
.L164:
	leaq	56(%rbx), %rax
	pxor	%xmm0, %xmm0
	andw	$-1024, 129(%rbx)
	movq	%rax, 64(%rbx)
	leaq	72(%rbx), %rax
	leaq	-48(%rbp), %r13
	movq	%rax, 80(%rbx)
	leaq	88(%rbx), %rax
	movq	%r13, %rdi
	movl	$0, 44(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 72(%rbx)
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movq	(%r12), %rax
	movb	%dl, 128(%rbx)
	movups	%xmm0, 16(%rbx)
	movdqa	.LC1(%rip), %xmm0
	movq	%r12, 104(%rbx)
	movups	%xmm0, 112(%rbx)
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal9ScopeInfo13language_modeEv@PLT
	movq	%r13, %rdi
	andl	$1, %eax
	movl	%eax, %edx
	movzbl	129(%rbx), %eax
	andl	$-2, %eax
	orl	%edx, %eax
	movb	%al, 129(%rbx)
	movq	(%r12), %rax
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal9ScopeInfo13ContextLengthEv@PLT
	orb	$2, 130(%rbx)
	movq	%r13, %rdi
	movl	%eax, 124(%rbx)
	movq	(%r12), %rax
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal9ScopeInfo13function_kindEv@PLT
	leaq	160(%rbx), %rdx
	movzbl	128(%rbx), %ecx
	andb	$-13, 132(%rbx)
	movq	%rdx, 168(%rbx)
	movzwl	130(%rbx), %edx
	movb	%al, 133(%rbx)
	movl	$0, 136(%rbx)
	andw	$-14082, %dx
	movq	$0, 144(%rbx)
	orw	$257, %dx
	cmpb	$3, %cl
	movw	%dx, 130(%rbx)
	sete	%dl
	movq	$0, 152(%rbx)
	movq	$0, 160(%rbx)
	movq	$0, 216(%rbx)
	cmpb	$2, %cl
	jne	.L167
	subl	$8, %eax
	cmpb	$1, %al
	seta	%dl
.L167:
	movzwl	131(%rbx), %eax
	movzbl	%dl, %edx
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	sall	$12, %edx
	movups	%xmm0, 176(%rbx)
	andw	$-4553, %ax
	movups	%xmm0, 192(%rbx)
	movq	$0, 208(%rbx)
	orl	%eax, %edx
	movq	(%r12), %rax
	movw	%dx, 131(%rbx)
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal9ScopeInfo23SloppyEvalCanExtendVarsEv@PLT
	testb	%al, %al
	jne	.L177
.L160:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L178
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	orb	$4, 129(%rbx)
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L175:
	movl	$192, %esi
	movl	%edx, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %edx
	jmp	.L162
.L176:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L178:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21201:
	.size	_ZN2v88internal16DeclarationScopeC2EPNS0_4ZoneENS0_9ScopeTypeENS0_6HandleINS0_9ScopeInfoEEE, .-_ZN2v88internal16DeclarationScopeC2EPNS0_4ZoneENS0_9ScopeTypeENS0_6HandleINS0_9ScopeInfoEEE
	.globl	_ZN2v88internal16DeclarationScopeC1EPNS0_4ZoneENS0_9ScopeTypeENS0_6HandleINS0_9ScopeInfoEEE
	.set	_ZN2v88internal16DeclarationScopeC1EPNS0_4ZoneENS0_9ScopeTypeENS0_6HandleINS0_9ScopeInfoEEE,_ZN2v88internal16DeclarationScopeC2EPNS0_4ZoneENS0_9ScopeTypeENS0_6HandleINS0_9ScopeInfoEEE
	.section	.text._ZN2v88internal16DeclarationScope11SetDefaultsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16DeclarationScope11SetDefaultsEv
	.type	_ZN2v88internal16DeclarationScope11SetDefaultsEv, @function
_ZN2v88internal16DeclarationScope11SetDefaultsEv:
.LFB21206:
	.cfi_startproc
	endbr64
	movzwl	130(%rdi), %eax
	movzbl	128(%rdi), %edx
	andb	$-13, 132(%rdi)
	andw	$-14082, %ax
	orw	$257, %ax
	cmpb	$3, %dl
	movw	%ax, 130(%rdi)
	sete	%al
	cmpb	$2, %dl
	jne	.L181
	movzbl	133(%rdi), %eax
	subl	$8, %eax
	cmpb	$1, %al
	seta	%al
.L181:
	movzwl	131(%rdi), %edx
	movzbl	%al, %eax
	pxor	%xmm0, %xmm0
	movq	$0, 216(%rdi)
	sall	$12, %eax
	movups	%xmm0, 176(%rdi)
	andw	$-4553, %dx
	movups	%xmm0, 192(%rdi)
	movq	$0, 208(%rdi)
	orl	%edx, %eax
	movw	%ax, 131(%rdi)
	ret
	.cfi_endproc
.LFE21206:
	.size	_ZN2v88internal16DeclarationScope11SetDefaultsEv, .-_ZN2v88internal16DeclarationScope11SetDefaultsEv
	.section	.text._ZN2v88internal5Scope11SetDefaultsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope11SetDefaultsEv
	.type	_ZN2v88internal5Scope11SetDefaultsEv, @function
_ZN2v88internal5Scope11SetDefaultsEv:
.LFB21207:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	leaq	72(%rdi), %rax
	andw	$-1024, 129(%rdi)
	movups	%xmm0, 16(%rdi)
	movdqa	.LC1(%rip), %xmm0
	movq	$0, 72(%rdi)
	movq	%rax, 80(%rdi)
	movups	%xmm0, 112(%rdi)
	ret
	.cfi_endproc
.LFE21207:
	.size	_ZN2v88internal5Scope11SetDefaultsEv, .-_ZN2v88internal5Scope11SetDefaultsEv
	.section	.text._ZN2v88internal5Scope19HasSimpleParametersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope19HasSimpleParametersEv
	.type	_ZN2v88internal5Scope19HasSimpleParametersEv, @function
_ZN2v88internal5Scope19HasSimpleParametersEv:
.LFB21208:
	.cfi_startproc
	endbr64
	.p2align 4,,10
	.p2align 3
.L187:
	movzbl	130(%rdi), %eax
	andl	$1, %eax
	je	.L184
	movzbl	128(%rdi), %edx
	cmpb	$6, %dl
	je	.L184
	testb	%dl, %dl
	jne	.L185
.L184:
	movq	8(%rdi), %rdi
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L185:
	cmpb	$2, %dl
	je	.L192
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	movzbl	131(%rdi), %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE21208:
	.size	_ZN2v88internal5Scope19HasSimpleParametersEv, .-_ZN2v88internal5Scope19HasSimpleParametersEv
	.section	.text._ZN2v88internal16DeclarationScope24set_should_eager_compileEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16DeclarationScope24set_should_eager_compileEv
	.type	_ZN2v88internal16DeclarationScope24set_should_eager_compileEv, @function
_ZN2v88internal16DeclarationScope24set_should_eager_compileEv:
.LFB21209:
	.cfi_startproc
	endbr64
	movzbl	131(%rdi), %eax
	movl	%eax, %edx
	andl	$-65, %eax
	shrb	$7, %dl
	xorl	$1, %edx
	sall	$6, %edx
	orl	%edx, %eax
	movb	%al, 131(%rdi)
	ret
	.cfi_endproc
.LFE21209:
	.size	_ZN2v88internal16DeclarationScope24set_should_eager_compileEv, .-_ZN2v88internal16DeclarationScope24set_should_eager_compileEv
	.section	.text._ZN2v88internal16DeclarationScope17set_is_asm_moduleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16DeclarationScope17set_is_asm_moduleEv
	.type	_ZN2v88internal16DeclarationScope17set_is_asm_moduleEv, @function
_ZN2v88internal16DeclarationScope17set_is_asm_moduleEv:
.LFB21210:
	.cfi_startproc
	endbr64
	orb	$2, 131(%rdi)
	ret
	.cfi_endproc
.LFE21210:
	.size	_ZN2v88internal16DeclarationScope17set_is_asm_moduleEv, .-_ZN2v88internal16DeclarationScope17set_is_asm_moduleEv
	.section	.text._ZNK2v88internal5Scope11IsAsmModuleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal5Scope11IsAsmModuleEv
	.type	_ZNK2v88internal5Scope11IsAsmModuleEv, @function
_ZNK2v88internal5Scope11IsAsmModuleEv:
.LFB21211:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpb	$2, 128(%rdi)
	je	.L198
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	movzbl	131(%rdi), %eax
	shrb	%al
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE21211:
	.size	_ZNK2v88internal5Scope11IsAsmModuleEv, .-_ZNK2v88internal5Scope11IsAsmModuleEv
	.section	.text._ZNK2v88internal5Scope17ContainsAsmModuleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal5Scope17ContainsAsmModuleEv
	.type	_ZNK2v88internal5Scope17ContainsAsmModuleEv, @function
_ZNK2v88internal5Scope17ContainsAsmModuleEv:
.LFB21212:
	.cfi_startproc
	endbr64
	cmpb	$2, 128(%rdi)
	je	.L213
.L200:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L205
	.p2align 4,,10
	.p2align 3
.L204:
	cmpb	$2, 128(%rbx)
	jne	.L202
	movzbl	131(%rbx), %eax
	testb	$4, %al
	jne	.L202
	testb	$64, %al
	je	.L203
	.p2align 4,,10
	.p2align 3
.L202:
	movq	%rbx, %rdi
	call	_ZNK2v88internal5Scope17ContainsAsmModuleEv
	testb	%al, %al
	jne	.L199
.L203:
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L204
.L205:
	xorl	%eax, %eax
.L199:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore 3
	.cfi_restore 6
	movzbl	131(%rdi), %eax
	shrb	%al
	andl	$1, %eax
	je	.L200
	ret
	.cfi_endproc
.LFE21212:
	.size	_ZNK2v88internal5Scope17ContainsAsmModuleEv, .-_ZNK2v88internal5Scope17ContainsAsmModuleEv
	.section	.text._ZN2v88internal5Scope18AsDeclarationScopeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope18AsDeclarationScopeEv
	.type	_ZN2v88internal5Scope18AsDeclarationScopeEv, @function
_ZN2v88internal5Scope18AsDeclarationScopeEv:
.LFB21214:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE21214:
	.size	_ZN2v88internal5Scope18AsDeclarationScopeEv, .-_ZN2v88internal5Scope18AsDeclarationScopeEv
	.section	.text._ZNK2v88internal5Scope18AsDeclarationScopeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal5Scope18AsDeclarationScopeEv
	.type	_ZNK2v88internal5Scope18AsDeclarationScopeEv, @function
_ZNK2v88internal5Scope18AsDeclarationScopeEv:
.LFB27811:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE27811:
	.size	_ZNK2v88internal5Scope18AsDeclarationScopeEv, .-_ZNK2v88internal5Scope18AsDeclarationScopeEv
	.section	.text._ZN2v88internal5Scope13AsModuleScopeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope13AsModuleScopeEv
	.type	_ZN2v88internal5Scope13AsModuleScopeEv, @function
_ZN2v88internal5Scope13AsModuleScopeEv:
.LFB21216:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE21216:
	.size	_ZN2v88internal5Scope13AsModuleScopeEv, .-_ZN2v88internal5Scope13AsModuleScopeEv
	.section	.text._ZNK2v88internal5Scope13AsModuleScopeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal5Scope13AsModuleScopeEv
	.type	_ZNK2v88internal5Scope13AsModuleScopeEv, @function
_ZNK2v88internal5Scope13AsModuleScopeEv:
.LFB27813:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE27813:
	.size	_ZNK2v88internal5Scope13AsModuleScopeEv, .-_ZNK2v88internal5Scope13AsModuleScopeEv
	.section	.text._ZN2v88internal5Scope12AsClassScopeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope12AsClassScopeEv
	.type	_ZN2v88internal5Scope12AsClassScopeEv, @function
_ZN2v88internal5Scope12AsClassScopeEv:
.LFB21218:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE21218:
	.size	_ZN2v88internal5Scope12AsClassScopeEv, .-_ZN2v88internal5Scope12AsClassScopeEv
	.section	.text._ZNK2v88internal5Scope12AsClassScopeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal5Scope12AsClassScopeEv
	.type	_ZNK2v88internal5Scope12AsClassScopeEv, @function
_ZNK2v88internal5Scope12AsClassScopeEv:
.LFB27815:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE27815:
	.size	_ZNK2v88internal5Scope12AsClassScopeEv, .-_ZNK2v88internal5Scope12AsClassScopeEv
	.section	.text._ZN2v88internal16DeclarationScope26DeclareSloppyBlockFunctionEPNS0_28SloppyBlockFunctionStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16DeclarationScope26DeclareSloppyBlockFunctionEPNS0_28SloppyBlockFunctionStatementE
	.type	_ZN2v88internal16DeclarationScope26DeclareSloppyBlockFunctionEPNS0_28SloppyBlockFunctionStatementE, @function
_ZN2v88internal16DeclarationScope26DeclareSloppyBlockFunctionEPNS0_28SloppyBlockFunctionStatementE:
.LFB21220:
	.cfi_startproc
	endbr64
	movq	168(%rdi), %rax
	movq	%rsi, (%rax)
	addq	$24, %rsi
	movq	%rsi, 168(%rdi)
	ret
	.cfi_endproc
.LFE21220:
	.size	_ZN2v88internal16DeclarationScope26DeclareSloppyBlockFunctionEPNS0_28SloppyBlockFunctionStatementE, .-_ZN2v88internal16DeclarationScope26DeclareSloppyBlockFunctionEPNS0_28SloppyBlockFunctionStatementE
	.section	.text._ZN2v88internal16DeclarationScope11DeclareThisEPNS0_15AstValueFactoryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16DeclarationScope11DeclareThisEPNS0_15AstValueFactoryE
	.type	_ZN2v88internal16DeclarationScope11DeclareThisEPNS0_15AstValueFactoryE, @function
_ZN2v88internal16DeclarationScope11DeclareThisEPNS0_15AstValueFactoryE:
.LFB21235:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movzbl	133(%rdi), %eax
	movq	(%rdi), %rdi
	leal	-4(%rax), %r12d
	movq	56(%rsi), %rax
	movq	24(%rdi), %rdx
	movq	472(%rax), %r13
	movq	16(%rdi), %rax
	subq	%rax, %rdx
	cmpq	$47, %rdx
	jbe	.L227
	leaq	48(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L223:
	cmpb	$2, %r12b
	pxor	%xmm0, %xmm0
	movq	%rbx, (%rax)
	sbbl	%edx, %edx
	movq	%r13, 8(%rax)
	andb	$-17, %dh
	movq	$-1, 32(%rax)
	addw	$4130, %dx
	movups	%xmm0, 16(%rax)
	movw	%dx, 40(%rax)
	movq	%rax, 176(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L227:
	.cfi_restore_state
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L223
	.cfi_endproc
.LFE21235:
	.size	_ZN2v88internal16DeclarationScope11DeclareThisEPNS0_15AstValueFactoryE, .-_ZN2v88internal16DeclarationScope11DeclareThisEPNS0_15AstValueFactoryE
	.section	.text._ZN2v88internal16DeclarationScope25DeclareGeneratorObjectVarEPKNS0_12AstRawStringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16DeclarationScope25DeclareGeneratorObjectVarEPKNS0_12AstRawStringE
	.type	_ZN2v88internal16DeclarationScope25DeclareGeneratorObjectVarEPKNS0_12AstRawStringE, @function
_ZN2v88internal16DeclarationScope25DeclareGeneratorObjectVarEPKNS0_12AstRawStringE:
.LFB21239:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	216(%rdi), %r13
	movq	(%rdi), %rdi
	movq	%rsi, -40(%rbp)
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	testq	%r13, %r13
	jne	.L235
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L232:
	movq	8(%rbx), %rbx
.L235:
	testb	$1, 130(%rbx)
	je	.L232
	movzbl	128(%rbx), %ecx
	cmpb	$6, %cl
	je	.L232
	testb	%cl, %cl
	je	.L232
	cmpq	$47, %rdx
	jbe	.L243
	leaq	48(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L237:
	movq	%rbx, %xmm0
	movl	$4099, %edx
	movq	$-1, 32(%rax)
	movhps	-40(%rbp), %xmm0
	movw	%dx, 40(%rax)
	movups	%xmm0, (%rax)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movq	64(%rbx), %rdx
	movq	%rax, (%rdx)
	leaq	24(%rax), %rdx
	movq	%rdx, 64(%rbx)
	movq	%rax, 8(%r13)
	orw	$2048, 40(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	cmpq	$15, %rdx
	jbe	.L244
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L231:
	pxor	%xmm0, %xmm0
	movq	%rax, %r13
	movups	%xmm0, (%rax)
	movq	(%r12), %rdi
	movq	%rax, 216(%r12)
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L243:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L244:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L231
	.cfi_endproc
.LFE21239:
	.size	_ZN2v88internal16DeclarationScope25DeclareGeneratorObjectVarEPKNS0_12AstRawStringE, .-_ZN2v88internal16DeclarationScope25DeclareGeneratorObjectVarEPKNS0_12AstRawStringE
	.section	.text._ZN2v88internal5Scope18FinalizeBlockScopeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope18FinalizeBlockScopeEv
	.type	_ZN2v88internal5Scope18FinalizeBlockScopeEv, @function
_ZN2v88internal5Scope18FinalizeBlockScopeEv:
.LFB21240:
	.cfi_startproc
	endbr64
	movl	44(%rdi), %edx
	movq	%rdi, %rax
	testl	%edx, %edx
	jne	.L246
	testb	$1, 130(%rdi)
	je	.L247
	testb	$4, 129(%rdi)
	jne	.L246
.L247:
	movq	8(%rax), %rcx
	movq	16(%rcx), %rdx
	cmpq	%rdx, %rax
	jne	.L248
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L279:
	movq	24(%rdx), %rcx
	cmpq	%rcx, %rax
	je	.L278
	movq	%rcx, %rdx
.L248:
	testq	%rdx, %rdx
	jne	.L279
.L249:
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L250
	movq	8(%rax), %rdx
	movq	%rdx, 8(%rcx)
	movq	24(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L251
	.p2align 4,,10
	.p2align 3
.L252:
	movq	8(%rax), %rcx
	movq	%rcx, 8(%rdx)
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L252
.L251:
	movq	8(%rax), %rdx
	movq	16(%rdx), %rdx
	movq	%rdx, 24(%rcx)
	movq	16(%rax), %rcx
	movq	8(%rax), %rdx
	movq	%rcx, 16(%rdx)
	movq	$0, 16(%rax)
.L250:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L253
	movq	8(%rax), %rcx
	movq	80(%rax), %rdi
	movq	72(%rcx), %rsi
	movq	%rsi, (%rdi)
	testq	%rsi, %rsi
	je	.L280
.L254:
	movq	%rdx, 72(%rcx)
	leaq	72(%rax), %rdx
	movq	$0, 72(%rax)
	movq	%rdx, 80(%rax)
.L253:
	testb	$64, 129(%rax)
	je	.L255
	movq	8(%rax), %rdx
	orb	$64, 129(%rdx)
.L255:
	movl	$0, 124(%rax)
	xorl	%eax, %eax
.L246:
	ret
	.p2align 4,,10
	.p2align 3
.L280:
	movq	80(%rax), %rsi
	movq	%rsi, 80(%rcx)
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L278:
	movq	24(%rax), %rcx
	movq	%rcx, 24(%rdx)
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L277:
	movq	24(%rax), %rdx
	movq	%rdx, 16(%rcx)
	jmp	.L249
	.cfi_endproc
.LFE21240:
	.size	_ZN2v88internal5Scope18FinalizeBlockScopeEv, .-_ZN2v88internal5Scope18FinalizeBlockScopeEv
	.section	.text._ZN2v88internal16DeclarationScope8AddLocalEPNS0_8VariableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16DeclarationScope8AddLocalEPNS0_8VariableE
	.type	_ZN2v88internal16DeclarationScope8AddLocalEPNS0_8VariableE, @function
_ZN2v88internal16DeclarationScope8AddLocalEPNS0_8VariableE:
.LFB21241:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	movq	%rsi, (%rax)
	addq	$24, %rsi
	movq	%rsi, 64(%rdi)
	ret
	.cfi_endproc
.LFE21241:
	.size	_ZN2v88internal16DeclarationScope8AddLocalEPNS0_8VariableE, .-_ZN2v88internal16DeclarationScope8AddLocalEPNS0_8VariableE
	.section	.text._ZN2v88internal5Scope8Snapshot8ReparentEPNS0_16DeclarationScopeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope8Snapshot8ReparentEPNS0_16DeclarationScopeE
	.type	_ZN2v88internal5Scope8Snapshot8ReparentEPNS0_16DeclarationScopeE, @function
_ZN2v88internal5Scope8Snapshot8ReparentEPNS0_16DeclarationScopeE:
.LFB21242:
	.cfi_startproc
	endbr64
	movq	24(%rsi), %rax
	movq	8(%rdi), %rdx
	cmpq	%rax, %rdx
	je	.L283
	cmpq	24(%rax), %rdx
	je	.L284
	.p2align 4,,10
	.p2align 3
.L288:
	movq	%rsi, 8(%rax)
	testb	$64, 129(%rax)
	je	.L285
	orb	$64, 129(%rsi)
	movq	24(%rax), %rax
	movq	24(%rax), %rcx
	cmpq	%rcx, 8(%rdi)
	jne	.L288
.L284:
	movq	%rsi, 8(%rax)
	testb	$64, 129(%rax)
	jne	.L325
.L289:
	movq	24(%rsi), %rdx
	movq	%rdx, 16(%rsi)
	movq	$0, 24(%rax)
	movq	8(%rdi), %rax
	movq	%rax, 24(%rsi)
.L283:
	movq	(%rdi), %rax
	movq	16(%rdi), %rdx
	andq	$-2, %rax
	cmpq	80(%rax), %rdx
	je	.L294
	movq	80(%rsi), %rcx
	movq	(%rdx), %r8
	movq	%r8, (%rcx)
	movq	80(%rax), %rcx
	movq	%rcx, 80(%rsi)
	movq	%rdx, 80(%rax)
	movq	$0, (%rdx)
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L291:
	movq	8(%rax), %rax
.L294:
	testb	$1, 130(%rax)
	je	.L291
	movzbl	128(%rax), %edx
	cmpb	$6, %dl
	je	.L291
	testb	%dl, %dl
	je	.L291
	movq	24(%rdi), %rdx
	cmpq	64(%rax), %rdx
	je	.L295
	.p2align 4,,10
	.p2align 3
.L296:
	movq	(%rdx), %rcx
	movq	%rsi, (%rcx)
	movq	(%rdx), %rdx
	addq	$24, %rdx
	cmpq	%rdx, 64(%rax)
	jne	.L296
	movq	24(%rdi), %rcx
	cmpq	%rdx, %rcx
	je	.L295
	movq	64(%rsi), %rdx
	movq	(%rcx), %r8
	movq	%r8, (%rdx)
	movq	64(%rax), %rdx
	movq	%rdx, 64(%rsi)
	movq	%rcx, 64(%rax)
	movq	$0, (%rcx)
	movq	24(%rdi), %rdx
.L295:
	movq	%rdx, 64(%rax)
	movq	$0, (%rdx)
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andq	$-2, %rdx
	testb	$2, 129(%rdx)
	je	.L297
	movzbl	129(%rsi), %edx
	movl	%edx, %eax
	orl	$2, %eax
	movb	%al, 129(%rsi)
	testb	$1, %al
	je	.L326
.L298:
	orb	$64, 129(%rsi)
	movq	(%rdi), %rax
.L297:
	testb	$1, %al
	jne	.L327
	andl	$1, %eax
	movq	%rax, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	movq	24(%rax), %rax
	movq	24(%rax), %rcx
	cmpq	%rcx, 8(%rdi)
	jne	.L288
	movq	%rsi, 8(%rax)
	testb	$64, 129(%rax)
	je	.L289
.L325:
	orb	$64, 129(%rsi)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L326:
	movzbl	128(%rsi), %eax
	cmpb	$4, %al
	je	.L298
	cmpb	$1, %al
	je	.L298
	orl	$6, %edx
	movb	%dl, 129(%rsi)
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L327:
	andq	$-2, %rax
	orb	$2, 129(%rax)
	movq	%rax, %rdx
	testb	$1, 130(%rax)
	jne	.L300
	.p2align 4,,10
	.p2align 3
.L301:
	movq	8(%rdx), %rdx
	testb	$1, 130(%rdx)
	je	.L301
.L300:
	movzbl	129(%rdx), %esi
	movl	%esi, %ecx
	orl	$2, %ecx
	movb	%cl, 129(%rdx)
	andl	$1, %ecx
	je	.L328
.L302:
	orb	$64, 129(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L303
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L304:
	orl	$64, %edx
	movb	%dl, 129(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L324
.L303:
	movzbl	129(%rax), %edx
	testb	$64, %dl
	je	.L304
.L324:
	movq	(%rdi), %rax
	andl	$1, %eax
	movq	%rax, (%rdi)
	ret
.L328:
	movzbl	128(%rdx), %ecx
	cmpb	$1, %cl
	je	.L302
	cmpb	$4, %cl
	je	.L302
	orl	$6, %esi
	movb	%sil, 129(%rdx)
	jmp	.L302
	.cfi_endproc
.LFE21242:
	.size	_ZN2v88internal5Scope8Snapshot8ReparentEPNS0_16DeclarationScopeE, .-_ZN2v88internal5Scope8Snapshot8ReparentEPNS0_16DeclarationScopeE
	.section	.text._ZN2v88internal5Scope17ReplaceOuterScopeEPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope17ReplaceOuterScopeEPS1_
	.type	_ZN2v88internal5Scope17ReplaceOuterScopeEPS1_, @function
_ZN2v88internal5Scope17ReplaceOuterScopeEPS1_:
.LFB21243:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	movq	16(%rdx), %rax
	cmpq	%rax, %rdi
	jne	.L330
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L339:
	movq	24(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L338
	movq	%rdx, %rax
.L330:
	testq	%rax, %rax
	jne	.L339
.L331:
	movq	16(%rsi), %rax
	movq	%rax, 24(%rdi)
	movq	%rdi, 16(%rsi)
	movq	%rsi, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	movq	24(%rdi), %rdx
	movq	%rdx, 24(%rax)
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L337:
	movq	24(%rdi), %rax
	movq	%rax, 16(%rdx)
	jmp	.L331
	.cfi_endproc
.LFE21243:
	.size	_ZN2v88internal5Scope17ReplaceOuterScopeEPS1_, .-_ZN2v88internal5Scope17ReplaceOuterScopeEPS1_
	.section	.text._ZN2v88internal16DeclarationScope15RecordParameterEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16DeclarationScope15RecordParameterEb
	.type	_ZN2v88internal16DeclarationScope15RecordParameterEb, @function
_ZN2v88internal16DeclarationScope15RecordParameterEb:
.LFB21246:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	andl	$1, %eax
	leal	0(,%rax,8), %edx
	movzbl	131(%rdi), %eax
	andl	$-9, %eax
	orl	%edx, %eax
	movb	%al, 131(%rdi)
	testb	%sil, %sil
	jne	.L340
	addl	$1, 136(%rdi)
.L340:
	ret
	.cfi_endproc
.LFE21246:
	.size	_ZN2v88internal16DeclarationScope15RecordParameterEb, .-_ZN2v88internal16DeclarationScope15RecordParameterEb
	.section	.text._ZN2v88internal5Scope13AddUnresolvedEPNS0_13VariableProxyE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope13AddUnresolvedEPNS0_13VariableProxyE
	.type	_ZN2v88internal5Scope13AddUnresolvedEPNS0_13VariableProxyE, @function
_ZN2v88internal5Scope13AddUnresolvedEPNS0_13VariableProxyE:
.LFB21251:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	leaq	16(%rsi), %rdx
	movq	%rsi, (%rax)
	movq	16(%rsi), %rax
	testq	%rax, %rax
	jne	.L344
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L352:
	leaq	16(%rax), %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L343
.L344:
	testb	$2, 5(%rax)
	jne	.L352
.L343:
	movq	%rdx, 80(%rdi)
	ret
	.cfi_endproc
.LFE21251:
	.size	_ZN2v88internal5Scope13AddUnresolvedEPNS0_13VariableProxyE, .-_ZN2v88internal5Scope13AddUnresolvedEPNS0_13VariableProxyE
	.section	.text._ZN2v88internal5Scope16RemoveUnresolvedEPNS0_13VariableProxyE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope16RemoveUnresolvedEPNS0_13VariableProxyE
	.type	_ZN2v88internal5Scope16RemoveUnresolvedEPNS0_13VariableProxyE, @function
_ZN2v88internal5Scope16RemoveUnresolvedEPNS0_13VariableProxyE:
.LFB21253:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rcx
	cmpq	%rcx, %rsi
	je	.L426
	.p2align 4,,10
	.p2align 3
.L354:
	testq	%rcx, %rcx
	je	.L381
	movq	16(%rcx), %rax
	leaq	16(%rcx), %r9
	testq	%rax, %rax
	je	.L361
	movq	%rax, %r8
	movq	%rax, %rdx
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L427:
	movq	16(%r8), %rdx
	testq	%rdx, %rdx
	je	.L362
	movq	%rdx, %r8
.L363:
	testb	$2, 5(%rdx)
	jne	.L427
.L362:
	cmpq	%rdx, %rsi
	je	.L365
	movq	%rdx, %rcx
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L361:
	testq	%rsi, %rsi
	je	.L364
.L381:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L365:
	movq	16(%rdx), %r8
	leaq	16(%rdx), %r10
	testq	%r8, %r8
	jne	.L369
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L428:
	movq	16(%r8), %r8
	testq	%r8, %r8
	je	.L368
.L369:
	testb	$2, 5(%r8)
	jne	.L428
.L368:
	testq	%rax, %rax
	je	.L425
.L366:
	movq	%r9, %rsi
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L429:
	leaq	16(%rax), %rsi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L367
.L370:
	testb	$2, 5(%rax)
	jne	.L429
.L367:
	movq	%r8, (%rsi)
	movq	16(%rdx), %rax
	movq	%r10, %rsi
	testq	%rax, %rax
	jne	.L372
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L430:
	leaq	16(%rax), %rsi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L371
.L372:
	testb	$2, 5(%rax)
	jne	.L430
.L371:
	movq	$0, (%rsi)
	movq	16(%rdx), %rax
	testq	%rax, %rax
	jne	.L374
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L431:
	leaq	16(%rax), %r10
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L373
.L374:
	testb	$2, 5(%rax)
	jne	.L431
.L373:
	movl	$1, %eax
	cmpq	%r10, 80(%rdi)
	je	.L432
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	jne	.L376
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L433:
	leaq	16(%rax), %r9
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L375
.L376:
	testb	$2, 5(%rax)
	jne	.L433
.L375:
	movq	%r9, 80(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L426:
	movq	16(%rcx), %rax
	leaq	16(%rcx), %rdx
	testq	%rax, %rax
	jne	.L357
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L434:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L355
.L357:
	testb	$2, 5(%rax)
	jne	.L434
	movq	%rax, 72(%rdi)
.L377:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	jne	.L359
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L435:
	leaq	16(%rax), %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L358
.L359:
	testb	$2, 5(%rax)
	jne	.L435
.L358:
	movq	$0, (%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L364:
	movq	16, %r8
	movl	$16, %r10d
	xorl	%edx, %edx
	testq	%r8, %r8
	jne	.L369
	movq	%rsi, %rdx
	.p2align 4,,10
	.p2align 3
.L425:
	movq	%r9, %rsi
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L355:
	leaq	72(%rdi), %rax
	movq	$0, 72(%rdi)
	movq	%rax, 80(%rdi)
	jmp	.L377
	.cfi_endproc
.LFE21253:
	.size	_ZN2v88internal5Scope16RemoveUnresolvedEPNS0_13VariableProxyE, .-_ZN2v88internal5Scope16RemoveUnresolvedEPNS0_13VariableProxyE
	.section	.text._ZN2v88internal5Scope16DeleteUnresolvedEPNS0_13VariableProxyE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope16DeleteUnresolvedEPNS0_13VariableProxyE
	.type	_ZN2v88internal5Scope16DeleteUnresolvedEPNS0_13VariableProxyE, @function
_ZN2v88internal5Scope16DeleteUnresolvedEPNS0_13VariableProxyE:
.LFB21254:
	.cfi_startproc
	endbr64
	orl	$512, 4(%rsi)
	ret
	.cfi_endproc
.LFE21254:
	.size	_ZN2v88internal5Scope16DeleteUnresolvedEPNS0_13VariableProxyE, .-_ZN2v88internal5Scope16DeleteUnresolvedEPNS0_13VariableProxyE
	.section	.text._ZN2v88internal5Scope12NewTemporaryEPKNS0_12AstRawStringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope12NewTemporaryEPKNS0_12AstRawStringE
	.type	_ZN2v88internal5Scope12NewTemporaryEPKNS0_12AstRawStringE, @function
_ZN2v88internal5Scope12NewTemporaryEPKNS0_12AstRawStringE:
.LFB21255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rsi, -40(%rbp)
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L438:
	movq	8(%rbx), %rbx
.L441:
	testb	$1, 130(%rbx)
	je	.L438
	movzbl	128(%rbx), %eax
	cmpb	$6, %al
	je	.L438
	testb	%al, %al
	je	.L438
	movq	(%rdi), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$47, %rdx
	jbe	.L477
	leaq	48(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L443:
	movq	%rbx, %xmm0
	movl	$4099, %edx
	movq	$-1, 32(%rax)
	movhps	-40(%rbp), %xmm0
	movw	%dx, 40(%rax)
	movups	%xmm0, (%rax)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movq	64(%rbx), %rdx
	movq	%rax, (%rdx)
	leaq	24(%rax), %rdx
	movq	%rdx, 64(%rbx)
	movzwl	40(%rax), %edx
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L437
	movq	16(%rax), %rbx
	testq	%rbx, %rbx
	je	.L445
	testb	$64, %dh
	je	.L478
.L445:
	orb	$64, %dh
	movw	%dx, 40(%rax)
.L437:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L478:
	.cfi_restore_state
	movzwl	40(%rbx), %ecx
	movl	%ecx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L445
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L446
	testb	$64, %ch
	je	.L479
.L446:
	orb	$64, %ch
	movw	%cx, 40(%rbx)
	movzwl	40(%rax), %edx
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L477:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L479:
	movzwl	40(%r12), %edx
	movl	%edx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L446
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L447
	testb	$64, %dh
	je	.L480
.L447:
	orb	$64, %dh
	movw	%dx, 40(%r12)
	movzwl	40(%rbx), %ecx
	jmp	.L446
.L480:
	movzwl	40(%r13), %ecx
	movl	%ecx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L447
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L448
	testb	$64, %ch
	je	.L481
.L448:
	orb	$64, %ch
	movw	%cx, 40(%r13)
	movzwl	40(%r12), %edx
	jmp	.L447
.L481:
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movzwl	40(%r13), %ecx
	movq	-40(%rbp), %rax
	jmp	.L448
	.cfi_endproc
.LFE21255:
	.size	_ZN2v88internal5Scope12NewTemporaryEPKNS0_12AstRawStringE, .-_ZN2v88internal5Scope12NewTemporaryEPKNS0_12AstRawStringE
	.section	.text._ZN2v88internal5Scope12NewTemporaryEPKNS0_12AstRawStringENS0_17MaybeAssignedFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope12NewTemporaryEPKNS0_12AstRawStringENS0_17MaybeAssignedFlagE
	.type	_ZN2v88internal5Scope12NewTemporaryEPKNS0_12AstRawStringENS0_17MaybeAssignedFlagE, @function
_ZN2v88internal5Scope12NewTemporaryEPKNS0_12AstRawStringENS0_17MaybeAssignedFlagE:
.LFB21256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rsi, -40(%rbp)
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L483:
	movq	8(%rbx), %rbx
.L486:
	testb	$1, 130(%rbx)
	je	.L483
	movzbl	128(%rbx), %eax
	cmpb	$6, %al
	je	.L483
	testb	%al, %al
	je	.L483
	movq	(%rdi), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$47, %rdx
	jbe	.L522
	leaq	48(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L488:
	movq	%rbx, %xmm0
	movl	$4099, %edx
	movq	$-1, 32(%rax)
	movhps	-40(%rbp), %xmm0
	movw	%dx, 40(%rax)
	movups	%xmm0, (%rax)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movq	64(%rbx), %rdx
	movq	%rax, (%rdx)
	leaq	24(%rax), %rdx
	movq	%rdx, 64(%rbx)
	cmpb	$1, %r12b
	je	.L523
.L482:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L523:
	.cfi_restore_state
	movzwl	40(%rax), %edx
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L482
	movq	16(%rax), %rbx
	testq	%rbx, %rbx
	je	.L490
	testb	$64, %dh
	je	.L524
.L490:
	orb	$64, %dh
	movw	%dx, 40(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L522:
	.cfi_restore_state
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L488
.L524:
	movzwl	40(%rbx), %ecx
	movl	%ecx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L490
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L491
	testb	$64, %ch
	je	.L525
.L491:
	orb	$64, %ch
	movw	%cx, 40(%rbx)
	movzwl	40(%rax), %edx
	jmp	.L490
.L525:
	movzwl	40(%r12), %edx
	movl	%edx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L491
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L492
	testb	$64, %dh
	je	.L526
.L492:
	orb	$64, %dh
	movw	%dx, 40(%r12)
	movzwl	40(%rbx), %ecx
	jmp	.L491
.L526:
	movzwl	40(%r13), %ecx
	movl	%ecx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L492
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L493
	testb	$64, %ch
	je	.L527
.L493:
	orb	$64, %ch
	movw	%cx, 40(%r13)
	movzwl	40(%r12), %edx
	jmp	.L492
.L527:
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movzwl	40(%r13), %ecx
	movq	-40(%rbp), %rax
	jmp	.L493
	.cfi_endproc
.LFE21256:
	.size	_ZN2v88internal5Scope12NewTemporaryEPKNS0_12AstRawStringENS0_17MaybeAssignedFlagE, .-_ZN2v88internal5Scope12NewTemporaryEPKNS0_12AstRawStringENS0_17MaybeAssignedFlagE
	.section	.text.unlikely._ZN2v88internal16DeclarationScope16DeclareParameterEPKNS0_12AstRawStringENS0_12VariableModeEbbPNS0_15AstValueFactoryEi,"ax",@progbits
	.align 2
.LCOLDB2:
	.section	.text._ZN2v88internal16DeclarationScope16DeclareParameterEPKNS0_12AstRawStringENS0_12VariableModeEbbPNS0_15AstValueFactoryEi,"ax",@progbits
.LHOTB2:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16DeclarationScope16DeclareParameterEPKNS0_12AstRawStringENS0_12VariableModeEbbPNS0_15AstValueFactoryEi
	.type	_ZN2v88internal16DeclarationScope16DeclareParameterEPKNS0_12AstRawStringENS0_12VariableModeEbbPNS0_15AstValueFactoryEi, @function
_ZN2v88internal16DeclarationScope16DeclareParameterEPKNS0_12AstRawStringENS0_12VariableModeEbbPNS0_15AstValueFactoryEi:
.LFB21245:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	cmpb	$3, %dl
	je	.L548
	movl	40(%r12), %eax
	movq	32(%rdi), %rdi
	leal	-1(%rax), %esi
	movl	24(%rbx), %eax
	shrl	$2, %eax
	andl	%esi, %eax
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L549:
	addq	$1, %rax
	andq	%rsi, %rax
.L547:
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L531
	cmpq	%rdx, %rbx
	jne	.L549
	movq	8(%rcx), %r15
.L530:
	movl	%r13d, %eax
	andl	$1, %eax
	leal	0(,%rax,8), %edx
	movzbl	131(%r12), %eax
	andl	$-9, %eax
	orl	%edx, %eax
	movb	%al, 131(%r12)
	movl	16(%rbp), %eax
	movl	%eax, 36(%r15)
	movslq	156(%r12), %rax
	movl	152(%r12), %edx
	cmpl	%edx, %eax
	jge	.L534
	movq	144(%r12), %rdx
	leal	1(%rax), %ecx
	movl	%ecx, 156(%r12)
	movq	%r15, (%rdx,%rax,8)
.L535:
	testb	%r13b, %r13b
	jne	.L539
	addl	$1, 136(%r12)
.L539:
	movq	56(%r14), %rax
	cmpq	112(%rax), %rbx
	je	.L550
.L540:
	orw	$2048, 40(%r15)
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L534:
	.cfi_restore_state
	movq	(%r12), %r8
	leal	1(%rdx,%rdx), %ecx
	movslq	%ecx, %rsi
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L551
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L537:
	movslq	156(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jg	.L552
.L538:
	addl	$1, %eax
	movq	%rdi, 144(%r12)
	movl	%ecx, 152(%r12)
	movl	%eax, 156(%r12)
	movq	%r15, (%rdi,%rdx)
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L550:
	orb	$16, 131(%r12)
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L548:
	movl	$1, %edx
	call	_ZN2v88internal5Scope12NewTemporaryEPKNS0_12AstRawStringENS0_17MaybeAssignedFlagE
	movq	%rax, %r15
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L552:
	movq	144(%r12), %rsi
	movl	%ecx, -52(%rbp)
	call	memcpy@PLT
	movslq	156(%r12), %rdx
	movl	-52(%rbp), %ecx
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$3, %rdx
	jmp	.L538
.L551:
	movq	%r8, %rdi
	movl	%ecx, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %ecx
	movq	%rax, %rdi
	jmp	.L537
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal16DeclarationScope16DeclareParameterEPKNS0_12AstRawStringENS0_12VariableModeEbbPNS0_15AstValueFactoryEi
	.cfi_startproc
	.type	_ZN2v88internal16DeclarationScope16DeclareParameterEPKNS0_12AstRawStringENS0_12VariableModeEbbPNS0_15AstValueFactoryEi.cold, @function
_ZN2v88internal16DeclarationScope16DeclareParameterEPKNS0_12AstRawStringENS0_12VariableModeEbbPNS0_15AstValueFactoryEi.cold:
.LFSB21245:
.L531:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movzbl	131(%r12), %edx
	movl	%r13d, %eax
	andl	$1, %eax
	sall	$3, %eax
	andl	$-9, %edx
	orl	%edx, %eax
	movb	%al, 131(%r12)
	movl	$0, 36
	ud2
	.cfi_endproc
.LFE21245:
	.section	.text._ZN2v88internal16DeclarationScope16DeclareParameterEPKNS0_12AstRawStringENS0_12VariableModeEbbPNS0_15AstValueFactoryEi
	.size	_ZN2v88internal16DeclarationScope16DeclareParameterEPKNS0_12AstRawStringENS0_12VariableModeEbbPNS0_15AstValueFactoryEi, .-_ZN2v88internal16DeclarationScope16DeclareParameterEPKNS0_12AstRawStringENS0_12VariableModeEbbPNS0_15AstValueFactoryEi
	.section	.text.unlikely._ZN2v88internal16DeclarationScope16DeclareParameterEPKNS0_12AstRawStringENS0_12VariableModeEbbPNS0_15AstValueFactoryEi
	.size	_ZN2v88internal16DeclarationScope16DeclareParameterEPKNS0_12AstRawStringENS0_12VariableModeEbbPNS0_15AstValueFactoryEi.cold, .-_ZN2v88internal16DeclarationScope16DeclareParameterEPKNS0_12AstRawStringENS0_12VariableModeEbbPNS0_15AstValueFactoryEi.cold
.LCOLDE2:
	.section	.text._ZN2v88internal16DeclarationScope16DeclareParameterEPKNS0_12AstRawStringENS0_12VariableModeEbbPNS0_15AstValueFactoryEi
.LHOTE2:
	.section	.text._ZN2v88internal5Scope22FindVariableDeclaredInEPS1_NS0_12VariableModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope22FindVariableDeclaredInEPS1_NS0_12VariableModeE
	.type	_ZN2v88internal5Scope22FindVariableDeclaredInEPS1_NS0_12VariableModeE, @function
_ZN2v88internal5Scope22FindVariableDeclaredInEPS1_NS0_12VariableModeE:
.LFB21258:
	.cfi_startproc
	endbr64
	movl	40(%rsi), %eax
	movq	32(%rsi), %rcx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rcx,%rax,8), %r9
	cmpq	%r9, %rcx
	jnb	.L563
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%edx, %ebx
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L576:
	addq	$24, %rcx
	cmpq	%rcx, %r9
	jbe	.L553
.L556:
	movq	(%rcx), %r8
	testq	%r8, %r8
	je	.L576
	movl	40(%rdi), %edx
	movq	32(%rdi), %r10
	leal	-1(%rdx), %r11d
	movq	%r11, %rdx
	.p2align 4,,10
	.p2align 3
.L560:
	movl	24(%r8), %eax
	shrl	$2, %eax
	andl	%edx, %eax
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L577:
	cmpq	%r8, %rsi
	je	.L558
	addq	$1, %rax
	andq	%r11, %rax
.L575:
	leaq	(%rax,%rax,2), %rsi
	leaq	(%r10,%rsi,8), %rdi
	movq	(%rdi), %rsi
	testq	%rsi, %rsi
	jne	.L577
.L557:
	addq	$24, %rcx
	cmpq	%rcx, %r9
	jbe	.L564
	.p2align 4,,10
	.p2align 3
.L561:
	movq	(%rcx), %r8
	testq	%r8, %r8
	jne	.L560
	addq	$24, %rcx
	cmpq	%rcx, %r9
	ja	.L561
.L553:
	movq	%r8, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L558:
	.cfi_restore_state
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L557
	movzwl	40(%rax), %eax
	andl	$15, %eax
	cmpb	%al, %bl
	jnb	.L553
	addq	$24, %rcx
	cmpq	%rcx, %r9
	ja	.L561
.L564:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%r8, %rax
	ret
.L563:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%r8d, %r8d
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE21258:
	.size	_ZN2v88internal5Scope22FindVariableDeclaredInEPS1_NS0_12VariableModeE, .-_ZN2v88internal5Scope22FindVariableDeclaredInEPS1_NS0_12VariableModeE
	.section	.text._ZN2v88internal16DeclarationScope19DeserializeReceiverEPNS0_15AstValueFactoryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16DeclarationScope19DeserializeReceiverEPNS0_15AstValueFactoryE
	.type	_ZN2v88internal16DeclarationScope19DeserializeReceiverEPNS0_15AstValueFactoryE, @function
_ZN2v88internal16DeclarationScope19DeserializeReceiverEPNS0_15AstValueFactoryE:
.LFB21259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$4, 128(%rdi)
	je	.L578
	movzbl	133(%rdi), %eax
	movq	%rdi, %r12
	movq	(%rdi), %rdi
	leal	-4(%rax), %r13d
	movq	56(%rsi), %rax
	movq	16(%rdi), %rbx
	movq	472(%rax), %r14
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$47, %rax
	jbe	.L587
	leaq	48(%rbx), %rax
	movq	%rax, 16(%rdi)
.L581:
	cmpb	$2, %r13b
	pxor	%xmm0, %xmm0
	movq	%r12, (%rbx)
	sbbl	%eax, %eax
	movq	%r14, 8(%rbx)
	andb	$-17, %ah
	movq	$-1, 32(%rbx)
	addw	$4130, %ax
	movups	%xmm0, 16(%rbx)
	movw	%ax, 40(%rbx)
	movq	%rbx, 176(%r12)
	testb	$32, 129(%r12)
	je	.L583
	movzwl	40(%rbx), %eax
	movl	$-1, 32(%rbx)
	andw	$-897, %ax
	orb	$2, %ah
	movw	%ax, 40(%rbx)
.L578:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L588
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L583:
	.cfi_restore_state
	movq	104(%r12), %rax
	leaq	-48(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal9ScopeInfo24ReceiverContextSlotIndexEv@PLT
	movl	%eax, %r8d
	movzwl	40(%rbx), %eax
	movl	%r8d, 32(%rbx)
	andw	$-897, %ax
	orw	$384, %ax
	movw	%ax, 40(%rbx)
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L587:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L581
.L588:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21259:
	.size	_ZN2v88internal16DeclarationScope19DeserializeReceiverEPNS0_15AstValueFactoryE, .-_ZN2v88internal16DeclarationScope19DeserializeReceiverEPNS0_15AstValueFactoryE
	.section	.text._ZNK2v88internal5Scope16HasThisReferenceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal5Scope16HasThisReferenceEv
	.type	_ZNK2v88internal5Scope16HasThisReferenceEv, @function
_ZNK2v88internal5Scope16HasThisReferenceEv:
.LFB21261:
	.cfi_startproc
	endbr64
	testb	$1, 130(%rdi)
	je	.L590
	movzbl	132(%rdi), %eax
	shrb	$3, %al
	andl	$1, %eax
	jne	.L604
.L590:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L595
	.p2align 4,,10
	.p2align 3
.L594:
	testb	$1, 130(%rbx)
	je	.L592
	testb	$16, 132(%rbx)
	jne	.L593
.L592:
	movq	%rbx, %rdi
	call	_ZNK2v88internal5Scope16HasThisReferenceEv
	testb	%al, %al
	jne	.L589
.L593:
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L594
.L595:
	xorl	%eax, %eax
.L589:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L604:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE21261:
	.size	_ZNK2v88internal5Scope16HasThisReferenceEv, .-_ZNK2v88internal5Scope16HasThisReferenceEv
	.section	.text._ZNK2v88internal5Scope43AllowsLazyParsingWithoutUnresolvedVariablesEPKS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal5Scope43AllowsLazyParsingWithoutUnresolvedVariablesEPKS1_
	.type	_ZNK2v88internal5Scope43AllowsLazyParsingWithoutUnresolvedVariablesEPKS1_, @function
_ZNK2v88internal5Scope43AllowsLazyParsingWithoutUnresolvedVariablesEPKS1_:
.LFB21262:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L610
.L609:
	movzbl	128(%rdi), %eax
	cmpb	$1, %al
	je	.L613
	andl	$-3, %eax
	cmpb	$5, %al
	je	.L614
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L614:
	movq	8(%rdi), %rdi
	cmpq	%rdi, %rsi
	jne	.L609
.L610:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L613:
	movzbl	129(%rdi), %eax
	andl	$1, %eax
	xorl	$1, %eax
	ret
	.cfi_endproc
.LFE21262:
	.size	_ZNK2v88internal5Scope43AllowsLazyParsingWithoutUnresolvedVariablesEPKS1_, .-_ZNK2v88internal5Scope43AllowsLazyParsingWithoutUnresolvedVariablesEPKS1_
	.section	.text._ZNK2v88internal16DeclarationScope21AllowsLazyCompilationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16DeclarationScope21AllowsLazyCompilationEv
	.type	_ZNK2v88internal16DeclarationScope21AllowsLazyCompilationEv, @function
_ZNK2v88internal16DeclarationScope21AllowsLazyCompilationEv:
.LFB21263:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testb	$4, 131(%rdi)
	jne	.L615
	cmpb	$17, 133(%rdi)
	setne	%al
.L615:
	ret
	.cfi_endproc
.LFE21263:
	.size	_ZNK2v88internal16DeclarationScope21AllowsLazyCompilationEv, .-_ZNK2v88internal16DeclarationScope21AllowsLazyCompilationEv
	.section	.text._ZNK2v88internal5Scope18ContextChainLengthEPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal5Scope18ContextChainLengthEPS1_
	.type	_ZNK2v88internal5Scope18ContextChainLengthEPS1_, @function
_ZNK2v88internal5Scope18ContextChainLengthEPS1_:
.LFB21264:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	je	.L622
	.p2align 4,,10
	.p2align 3
.L621:
	movl	124(%rdi), %ecx
	xorl	%edx, %edx
	movq	8(%rdi), %rdi
	testl	%ecx, %ecx
	setg	%dl
	addl	%edx, %eax
	cmpq	%rdi, %rsi
	jne	.L621
	ret
	.p2align 4,,10
	.p2align 3
.L622:
	ret
	.cfi_endproc
.LFE21264:
	.size	_ZNK2v88internal5Scope18ContextChainLengthEPS1_, .-_ZNK2v88internal5Scope18ContextChainLengthEPS1_
	.section	.text._ZNK2v88internal5Scope42ContextChainLengthUntilOutermostSloppyEvalEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal5Scope42ContextChainLengthUntilOutermostSloppyEvalEv
	.type	_ZNK2v88internal5Scope42ContextChainLengthUntilOutermostSloppyEvalEv, @function
_ZNK2v88internal5Scope42ContextChainLengthUntilOutermostSloppyEvalEv:
.LFB21265:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L626:
	movl	124(%rdi), %edx
	testl	%edx, %edx
	jle	.L625
	addl	$1, %eax
	testb	$1, 130(%rdi)
	je	.L625
	testb	$4, 129(%rdi)
	cmovne	%eax, %r8d
.L625:
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L626
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE21265:
	.size	_ZNK2v88internal5Scope42ContextChainLengthUntilOutermostSloppyEvalEv, .-_ZNK2v88internal5Scope42ContextChainLengthUntilOutermostSloppyEvalEv
	.section	.text._ZN2v88internal5Scope13GetClassScopeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope13GetClassScopeEv
	.type	_ZN2v88internal5Scope13GetClassScopeEv, @function
_ZN2v88internal5Scope13GetClassScopeEv:
.LFB21266:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L635:
	cmpb	$0, 128(%rax)
	je	.L634
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L635
.L634:
	ret
	.cfi_endproc
.LFE21266:
	.size	_ZN2v88internal5Scope13GetClassScopeEv, .-_ZN2v88internal5Scope13GetClassScopeEv
	.section	.text._ZN2v88internal5Scope19GetDeclarationScopeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope19GetDeclarationScopeEv
	.type	_ZN2v88internal5Scope19GetDeclarationScopeEv, @function
_ZN2v88internal5Scope19GetDeclarationScopeEv:
.LFB21267:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	testb	$1, 130(%rdi)
	jne	.L638
	.p2align 4,,10
	.p2align 3
.L639:
	movq	8(%rax), %rax
	testb	$1, 130(%rax)
	je	.L639
.L638:
	ret
	.cfi_endproc
.LFE21267:
	.size	_ZN2v88internal5Scope19GetDeclarationScopeEv, .-_ZN2v88internal5Scope19GetDeclarationScopeEv
	.section	.text._ZNK2v88internal5Scope15GetClosureScopeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal5Scope15GetClosureScopeEv
	.type	_ZNK2v88internal5Scope15GetClosureScopeEv, @function
_ZNK2v88internal5Scope15GetClosureScopeEv:
.LFB27817:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L643:
	movq	8(%rax), %rax
.L646:
	testb	$1, 130(%rax)
	je	.L643
	movzbl	128(%rax), %edx
	cmpb	$6, %dl
	je	.L643
	testb	%dl, %dl
	je	.L643
	ret
	.cfi_endproc
.LFE27817:
	.size	_ZNK2v88internal5Scope15GetClosureScopeEv, .-_ZNK2v88internal5Scope15GetClosureScopeEv
	.section	.text._ZN2v88internal5Scope15GetClosureScopeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope15GetClosureScopeEv
	.type	_ZN2v88internal5Scope15GetClosureScopeEv, @function
_ZN2v88internal5Scope15GetClosureScopeEv:
.LFB21269:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L651:
	movq	8(%rax), %rax
.L654:
	testb	$1, 130(%rax)
	je	.L651
	movzbl	128(%rax), %edx
	testb	%dl, %dl
	je	.L651
	cmpb	$6, %dl
	je	.L651
	ret
	.cfi_endproc
.LFE21269:
	.size	_ZN2v88internal5Scope15GetClosureScopeEv, .-_ZN2v88internal5Scope15GetClosureScopeEv
	.section	.text._ZNK2v88internal5Scope14NeedsScopeInfoEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal5Scope14NeedsScopeInfoEv
	.type	_ZNK2v88internal5Scope14NeedsScopeInfoEv, @function
_ZNK2v88internal5Scope14NeedsScopeInfoEv:
.LFB21270:
	.cfi_startproc
	endbr64
	cmpb	$2, 128(%rdi)
	movl	$1, %eax
	je	.L658
	movl	124(%rdi), %eax
	testl	%eax, %eax
	setg	%al
.L658:
	ret
	.cfi_endproc
.LFE21270:
	.size	_ZNK2v88internal5Scope14NeedsScopeInfoEv, .-_ZNK2v88internal5Scope14NeedsScopeInfoEv
	.section	.text._ZN2v88internal5Scope18ShouldBanArgumentsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope18ShouldBanArgumentsEv
	.type	_ZN2v88internal5Scope18ShouldBanArgumentsEv, @function
_ZN2v88internal5Scope18ShouldBanArgumentsEv:
.LFB21271:
	.cfi_startproc
	endbr64
	.p2align 4,,10
	.p2align 3
.L664:
	testb	$1, 130(%rdi)
	je	.L662
	cmpb	$4, 128(%rdi)
	je	.L663
	testb	$16, 132(%rdi)
	jne	.L663
.L662:
	movq	8(%rdi), %rdi
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L663:
	cmpb	$17, 133(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE21271:
	.size	_ZN2v88internal5Scope18ShouldBanArgumentsEv, .-_ZN2v88internal5Scope18ShouldBanArgumentsEv
	.section	.text._ZN2v88internal5Scope16GetReceiverScopeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope16GetReceiverScopeEv
	.type	_ZN2v88internal5Scope16GetReceiverScopeEv, @function
_ZN2v88internal5Scope16GetReceiverScopeEv:
.LFB21272:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L675:
	testb	$16, 132(%rax)
	jne	.L670
.L669:
	movq	8(%rax), %rax
.L671:
	testb	$1, 130(%rax)
	je	.L669
	cmpb	$4, 128(%rax)
	jne	.L675
.L670:
	ret
	.cfi_endproc
.LFE21272:
	.size	_ZN2v88internal5Scope16GetReceiverScopeEv, .-_ZN2v88internal5Scope16GetReceiverScopeEv
	.section	.text._ZN2v88internal5Scope24GetOuterScopeWithContextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope24GetOuterScopeWithContextEv
	.type	_ZN2v88internal5Scope24GetOuterScopeWithContextEv, @function
_ZN2v88internal5Scope24GetOuterScopeWithContextEv:
.LFB21273:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	jne	.L678
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L683:
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L676
.L678:
	movl	124(%rax), %edx
	testl	%edx, %edx
	jle	.L683
.L676:
	ret
	.cfi_endproc
.LFE21273:
	.size	_ZN2v88internal5Scope24GetOuterScopeWithContextEv, .-_ZN2v88internal5Scope24GetOuterScopeWithContextEv
	.section	.text._ZN2v88internal5Scope24IsSkippableFunctionScopeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope24IsSkippableFunctionScopeEv
	.type	_ZN2v88internal5Scope24IsSkippableFunctionScopeEv, @function
_ZN2v88internal5Scope24IsSkippableFunctionScopeEv:
.LFB21282:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpb	$2, 128(%rdi)
	jne	.L684
	movzbl	133(%rdi), %ecx
	leal	-8(%rcx), %edx
	cmpb	$1, %dl
	jbe	.L684
	cmpq	$0, 208(%rdi)
	setne	%al
.L684:
	ret
	.cfi_endproc
.LFE21282:
	.size	_ZN2v88internal5Scope24IsSkippableFunctionScopeEv, .-_ZN2v88internal5Scope24IsSkippableFunctionScopeEv
	.section	.text._ZN2v88internal5Scope16SavePreparseDataEPNS0_6ParserE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope16SavePreparseDataEPNS0_6ParserE
	.type	_ZN2v88internal5Scope16SavePreparseDataEPNS0_6ParserE, @function
_ZN2v88internal5Scope16SavePreparseDataEPNS0_6ParserE:
.LFB21283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L691:
	movq	%rax, %rbx
.L689:
	cmpb	$2, 128(%rbx)
	jne	.L690
	movzbl	133(%rbx), %eax
	subl	$8, %eax
	cmpb	$1, %al
	jbe	.L690
	movq	208(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L690
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal19PreparseDataBuilder23SaveScopeAllocationDataEPNS0_16DeclarationScopeEPNS0_6ParserE@PLT
	.p2align 4,,10
	.p2align 3
.L690:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	jne	.L691
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L701
	.p2align 4,,10
	.p2align 3
.L695:
	cmpq	%rbx, %r12
	je	.L688
	movq	8(%rbx), %rbx
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L695
.L701:
	cmpq	%rbx, %r12
	jne	.L691
.L688:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21283:
	.size	_ZN2v88internal5Scope16SavePreparseDataEPNS0_6ParserE, .-_ZN2v88internal5Scope16SavePreparseDataEPNS0_6ParserE
	.section	.text._ZN2v88internal16DeclarationScope35SavePreparseDataForDeclarationScopeEPNS0_6ParserE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16DeclarationScope35SavePreparseDataForDeclarationScopeEPNS0_6ParserE
	.type	_ZN2v88internal16DeclarationScope35SavePreparseDataForDeclarationScopeEPNS0_6ParserE, @function
_ZN2v88internal16DeclarationScope35SavePreparseDataForDeclarationScopeEPNS0_6ParserE:
.LFB21285:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	208(%rdi), %rdi
	movq	%rsi, %rdx
	testq	%rdi, %rdi
	je	.L702
	movq	%r8, %rsi
	jmp	_ZN2v88internal19PreparseDataBuilder23SaveScopeAllocationDataEPNS0_16DeclarationScopeEPNS0_6ParserE@PLT
	.p2align 4,,10
	.p2align 3
.L702:
	ret
	.cfi_endproc
.LFE21285:
	.size	_ZN2v88internal16DeclarationScope35SavePreparseDataForDeclarationScopeEPNS0_6ParserE, .-_ZN2v88internal16DeclarationScope35SavePreparseDataForDeclarationScopeEPNS0_6ParserE
	.section	.text._ZN2v88internal5Scope9ResolveToEPNS0_9ParseInfoEPNS0_13VariableProxyEPNS0_8VariableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope9ResolveToEPNS0_9ParseInfoEPNS0_13VariableProxyEPNS0_8VariableE
	.type	_ZN2v88internal5Scope9ResolveToEPNS0_9ParseInfoEPNS0_13VariableProxyEPNS0_8VariableE, @function
_ZN2v88internal5Scope9ResolveToEPNS0_9ParseInfoEPNS0_13VariableProxyEPNS0_8VariableE:
.LFB21295:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rdi, %rdx
	movq	%rcx, %rdi
	movq	%r9, %rsi
	movq	%rcx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal12_GLOBAL__N_120UpdateNeedsHoleCheckEPNS0_8VariableEPNS0_13VariableProxyEPNS0_5ScopeE
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%r10, %rsi
	movq	%r9, %rdi
	jmp	_ZN2v88internal13VariableProxy6BindToEPNS0_8VariableE@PLT
	.cfi_endproc
.LFE21295:
	.size	_ZN2v88internal5Scope9ResolveToEPNS0_9ParseInfoEPNS0_13VariableProxyEPNS0_8VariableE, .-_ZN2v88internal5Scope9ResolveToEPNS0_9ParseInfoEPNS0_13VariableProxyEPNS0_8VariableE
	.section	.text._ZN2v88internal5Scope24ResolvePreparsedVariableEPNS0_13VariableProxyEPS1_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope24ResolvePreparsedVariableEPNS0_13VariableProxyEPS1_S4_
	.type	_ZN2v88internal5Scope24ResolvePreparsedVariableEPNS0_13VariableProxyEPS1_S4_, @function
_ZN2v88internal5Scope24ResolvePreparsedVariableEPNS0_13VariableProxyEPS1_S4_:
.LFB21296:
	.cfi_startproc
	endbr64
	cmpq	%rdx, %rsi
	je	.L748
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	.p2align 4,,10
	.p2align 3
.L707:
	movq	8(%rdi), %r10
	testb	$1, 5(%rdi)
	je	.L709
	movq	8(%r10), %r10
.L709:
	movl	40(%rsi), %eax
	movq	32(%rsi), %r11
	leal	-1(%rax), %r9d
	movl	24(%r10), %eax
	shrl	$2, %eax
	andl	%r9d, %eax
	jmp	.L750
	.p2align 4,,10
	.p2align 3
.L751:
	cmpq	%rcx, %r10
	je	.L711
	addq	$1, %rax
	andq	%r9, %rax
.L750:
	leaq	(%rax,%rax,2), %rcx
	leaq	(%r11,%rcx,8), %r8
	movq	(%r8), %rcx
	testq	%rcx, %rcx
	jne	.L751
.L710:
	movq	8(%rsi), %rsi
	cmpq	%rsi, %rdx
	jne	.L707
.L706:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L711:
	.cfi_restore_state
	movq	8(%r8), %rbx
	testq	%rbx, %rbx
	je	.L710
	movzwl	40(%rbx), %ecx
	movl	%ecx, %eax
	orb	$8, %ah
	movw	%ax, 40(%rbx)
	andl	$15, %eax
	subl	$4, %eax
	cmpb	$2, %al
	jbe	.L710
	orb	$12, %ch
	movw	%cx, 40(%rbx)
	testb	$-128, 4(%rdi)
	je	.L706
	movl	%ecx, %eax
	andl	$15, %eax
	cmpb	$1, %al
	je	.L706
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L714
	testb	$64, %ch
	je	.L752
.L714:
	orb	$64, %ch
	movw	%cx, 40(%rbx)
	jmp	.L706
.L748:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
.L752:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movzwl	40(%r12), %eax
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$1, %dl
	je	.L714
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L715
	testb	$64, %ah
	je	.L753
.L715:
	orb	$64, %ah
	movw	%ax, 40(%r12)
	movzwl	40(%rbx), %ecx
	jmp	.L714
.L753:
	movzwl	40(%r13), %edx
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L715
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L716
	testb	$64, %dh
	je	.L754
.L716:
	orb	$64, %dh
	movw	%dx, 40(%r13)
	movzwl	40(%r12), %eax
	jmp	.L715
.L754:
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movzwl	40(%r13), %edx
	jmp	.L716
	.cfi_endproc
.LFE21296:
	.size	_ZN2v88internal5Scope24ResolvePreparsedVariableEPNS0_13VariableProxyEPS1_S4_, .-_ZN2v88internal5Scope24ResolvePreparsedVariableEPNS0_13VariableProxyEPS1_S4_
	.section	.text._ZN2v88internal5Scope12MustAllocateEPNS0_8VariableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope12MustAllocateEPNS0_8VariableE
	.type	_ZN2v88internal5Scope12MustAllocateEPNS0_8VariableE, @function
_ZN2v88internal5Scope12MustAllocateEPNS0_8VariableE:
.LFB21298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	8(%rsi), %rax
	movl	16(%rax), %eax
	testl	%eax, %eax
	je	.L757
	testb	$64, 129(%rdi)
	je	.L790
.L758:
	movzwl	40(%rbx), %eax
	orb	$8, %ah
	movw	%ax, 40(%rbx)
	testb	$64, 129(%rdi)
	je	.L757
	movzwl	%ax, %ecx
	movl	%ecx, %edx
	sarl	$4, %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L757
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$1, %dl
	je	.L757
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L760
	andb	$64, %ch
	je	.L791
.L760:
	orb	$64, %ah
	movw	%ax, 40(%rbx)
	.p2align 4,,10
	.p2align 3
.L757:
	movq	%rbx, %rdi
	call	_ZNK2v88internal8Variable22IsGlobalObjectPropertyEv@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	jne	.L755
	movzwl	40(%rbx), %eax
	shrw	$11, %ax
	andl	$1, %eax
.L755:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L790:
	.cfi_restore_state
	movzbl	128(%rdi), %eax
	subl	$4, %eax
	cmpb	$1, %al
	jbe	.L758
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L791:
	movzwl	40(%r12), %edx
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L760
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L761
	testb	$64, %dh
	je	.L792
.L761:
	orb	$64, %dh
	movw	%dx, 40(%r12)
	movzwl	40(%rbx), %eax
	jmp	.L760
.L792:
	movzwl	40(%r13), %eax
	movl	%eax, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L761
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L762
	testb	$64, %ah
	je	.L793
.L762:
	orb	$64, %ah
	movw	%ax, 40(%r13)
	movzwl	40(%r12), %edx
	jmp	.L761
.L793:
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movzwl	40(%r13), %eax
	jmp	.L762
	.cfi_endproc
.LFE21298:
	.size	_ZN2v88internal5Scope12MustAllocateEPNS0_8VariableE, .-_ZN2v88internal5Scope12MustAllocateEPNS0_8VariableE
	.section	.text._ZN2v88internal5Scope21MustAllocateInContextEPNS0_8VariableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope21MustAllocateInContextEPNS0_8VariableE
	.type	_ZN2v88internal5Scope21MustAllocateInContextEPNS0_8VariableE, @function
_ZN2v88internal5Scope21MustAllocateInContextEPNS0_8VariableE:
.LFB21299:
	.cfi_startproc
	endbr64
	movzwl	40(%rsi), %ecx
	xorl	%eax, %eax
	movl	%ecx, %esi
	andl	$15, %esi
	cmpb	$3, %sil
	je	.L794
	movzbl	128(%rdi), %edx
	movl	$1, %eax
	cmpb	$5, %dl
	je	.L794
	cmpb	$4, %dl
	sete	%al
	cmpb	$1, %dl
	sete	%dl
	orb	%dl, %al
	je	.L796
	testb	$14, %cl
	je	.L794
	subl	$7, %esi
	cmpb	$3, %sil
	jbe	.L794
.L796:
	movl	$1, %eax
	andb	$4, %ch
	jne	.L794
	movzbl	129(%rdi), %eax
	shrb	$6, %al
	andl	$1, %eax
.L794:
	ret
	.cfi_endproc
.LFE21299:
	.size	_ZN2v88internal5Scope21MustAllocateInContextEPNS0_8VariableE, .-_ZN2v88internal5Scope21MustAllocateInContextEPNS0_8VariableE
	.section	.text._ZN2v88internal5Scope17AllocateStackSlotEPNS0_8VariableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope17AllocateStackSlotEPNS0_8VariableE
	.type	_ZN2v88internal5Scope17AllocateStackSlotEPNS0_8VariableE, @function
_ZN2v88internal5Scope17AllocateStackSlotEPNS0_8VariableE:
.LFB21300:
	.cfi_startproc
	endbr64
	movzbl	128(%rdi), %eax
	cmpb	$6, %al
	je	.L817
	testb	%al, %al
	jne	.L809
	.p2align 4,,10
	.p2align 3
.L817:
	movq	8(%rdi), %rdi
	testb	$1, 130(%rdi)
	je	.L817
	movzbl	128(%rdi), %eax
	testb	%al, %al
	je	.L817
	cmpb	$6, %al
	je	.L817
.L809:
	movl	120(%rdi), %edx
	leal	1(%rdx), %eax
	movl	%eax, 120(%rdi)
	movzwl	40(%rsi), %eax
	movl	%edx, 32(%rsi)
	andw	$-897, %ax
	orb	$1, %ah
	movw	%ax, 40(%rsi)
	ret
	.cfi_endproc
.LFE21300:
	.size	_ZN2v88internal5Scope17AllocateStackSlotEPNS0_8VariableE, .-_ZN2v88internal5Scope17AllocateStackSlotEPNS0_8VariableE
	.section	.text._ZN2v88internal5Scope25AllocateNonParameterLocalEPNS0_8VariableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope25AllocateNonParameterLocalEPNS0_8VariableE
	.type	_ZN2v88internal5Scope25AllocateNonParameterLocalEPNS0_8VariableE, @function
_ZN2v88internal5Scope25AllocateNonParameterLocalEPNS0_8VariableE:
.LFB21305:
	.cfi_startproc
	endbr64
	movzwl	40(%rsi), %eax
	testw	$896, %ax
	je	.L888
	ret
	.p2align 4,,10
	.p2align 3
.L888:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rsi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	16(%rdx), %edx
	movq	%rdi, %rbx
	testl	%edx, %edx
	je	.L824
	testb	$64, 129(%rdi)
	jne	.L825
	movzbl	128(%rdi), %ecx
	leal	-4(%rcx), %edx
	cmpb	$1, %dl
	ja	.L824
.L825:
	orb	$8, %ah
	movw	%ax, 40(%r12)
	testb	$64, 129(%rbx)
	je	.L824
	movzwl	%ax, %ecx
	movl	%ecx, %edx
	sarl	$4, %edx
	andl	$7, %edx
	cmpb	$2, %dl
	jne	.L889
	.p2align 4,,10
	.p2align 3
.L824:
	movq	%r12, %rdi
	call	_ZNK2v88internal8Variable22IsGlobalObjectPropertyEv@PLT
	testb	%al, %al
	jne	.L820
	movzwl	40(%r12), %eax
	testb	$8, %ah
	je	.L820
	movl	%eax, %ecx
	movzbl	128(%rbx), %edx
	andl	$15, %ecx
	cmpb	$3, %cl
	je	.L837
	cmpb	$5, %dl
	je	.L834
	cmpb	$1, %dl
	je	.L840
	cmpb	$4, %dl
	je	.L840
.L835:
	testb	$4, %ah
	jne	.L834
	testb	$64, 129(%rbx)
	jne	.L834
.L837:
	cmpb	$6, %dl
	je	.L869
	testb	%dl, %dl
	jne	.L833
	.p2align 4,,10
	.p2align 3
.L869:
	movq	8(%rbx), %rdi
	testb	$1, 130(%rdi)
	jne	.L838
	.p2align 4,,10
	.p2align 3
.L839:
	movq	8(%rdi), %rdi
	testb	$1, 130(%rdi)
	je	.L839
.L838:
	movzbl	128(%rdi), %eax
	movq	%rdi, %rbx
	cmpb	$6, %al
	je	.L869
	testb	%al, %al
	je	.L869
.L833:
	movl	120(%rbx), %edx
	leal	1(%rdx), %eax
	movl	%eax, 120(%rbx)
	movzwl	40(%r12), %eax
	movl	%edx, 32(%r12)
	andw	$-897, %ax
	orb	$1, %ah
	movw	%ax, 40(%r12)
.L820:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L840:
	.cfi_restore_state
	testb	$14, %al
	je	.L834
	subl	$7, %ecx
	cmpb	$3, %cl
	ja	.L835
	.p2align 4,,10
	.p2align 3
.L834:
	movl	124(%rbx), %edx
	leal	1(%rdx), %eax
	movl	%eax, 124(%rbx)
	movzwl	40(%r12), %eax
	movl	%edx, 32(%r12)
	andw	$-897, %ax
	orw	$384, %ax
	movw	%ax, 40(%r12)
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L889:
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$1, %dl
	je	.L824
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L827
	andb	$64, %ch
	je	.L890
.L827:
	orb	$64, %ah
	movw	%ax, 40(%r12)
	jmp	.L824
.L890:
	movzwl	40(%r13), %edx
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L827
	movq	16(%r13), %r14
	testq	%r14, %r14
	je	.L828
	testb	$64, %dh
	je	.L891
.L828:
	orb	$64, %dh
	movw	%dx, 40(%r13)
	movzwl	40(%r12), %eax
	jmp	.L827
.L891:
	movzwl	40(%r14), %eax
	movl	%eax, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L828
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.L829
	testb	$64, %ah
	je	.L892
.L829:
	orb	$64, %ah
	movw	%ax, 40(%r14)
	movzwl	40(%r13), %edx
	jmp	.L828
.L892:
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movzwl	40(%r14), %eax
	jmp	.L829
	.cfi_endproc
.LFE21305:
	.size	_ZN2v88internal5Scope25AllocateNonParameterLocalEPNS0_8VariableE, .-_ZN2v88internal5Scope25AllocateNonParameterLocalEPNS0_8VariableE
	.section	.text._ZZN2v88internal5Scope28AllocateVariablesRecursivelyEvENKUlPS1_E_clES2_.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internal5Scope28AllocateVariablesRecursivelyEvENKUlPS1_E_clES2_.isra.0, @function
_ZZN2v88internal5Scope28AllocateVariablesRecursivelyEvENKUlPS1_E_clES2_.isra.0:
.LFB27772:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testb	$1, 130(%rdi)
	je	.L895
	movzbl	131(%rdi), %r14d
	xorl	%eax, %eax
	shrb	$7, %r14b
	jne	.L893
	cmpb	$2, 128(%rdi)
	je	.L897
.L903:
	testb	$16, 132(%r13)
	jne	.L1088
.L895:
	movq	64(%r13), %r14
	leaq	56(%r13), %r12
	cmpq	%r12, %r14
	jne	.L924
	jmp	.L938
	.p2align 4,,10
	.p2align 3
.L927:
	leaq	24(%rbx), %r12
	cmpq	%r12, %r14
	je	.L938
.L924:
	movq	(%r12), %rbx
	testw	$896, 40(%rbx)
	jne	.L927
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal5Scope12MustAllocateEPNS0_8VariableE
	testb	%al, %al
	jne	.L928
.L1084:
	movq	(%r12), %rbx
	leaq	24(%rbx), %r12
	cmpq	%r12, %r14
	jne	.L924
.L938:
	testb	$1, 130(%r13)
	je	.L939
	movq	184(%r13), %rsi
	testq	%rsi, %rsi
	je	.L940
	movq	%r13, %rdi
	call	_ZN2v88internal5Scope12MustAllocateEPNS0_8VariableE
	testb	%al, %al
	jne	.L1089
.L940:
	movq	$0, 184(%r13)
.L941:
	movq	192(%r13), %rsi
	testq	%rsi, %rsi
	je	.L943
	movq	%r13, %rdi
	call	_ZN2v88internal5Scope12MustAllocateEPNS0_8VariableE
	testb	%al, %al
	jne	.L943
	movq	$0, 192(%r13)
.L943:
	movq	216(%r13), %rbx
	testq	%rbx, %rbx
	jne	.L1090
.L939:
	movzbl	128(%r13), %edx
	movl	$1, %eax
	movl	%edx, %ecx
	andl	$-5, %ecx
	cmpb	$3, %cl
	jne	.L1091
.L893:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L928:
	.cfi_restore_state
	movzwl	40(%rbx), %edx
	movzbl	128(%r13), %eax
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$3, %cl
	je	.L929
	cmpb	$5, %al
	je	.L930
	cmpb	$1, %al
	je	.L962
	cmpb	$4, %al
	je	.L962
	andb	$4, %dh
	je	.L1092
	.p2align 4,,10
	.p2align 3
.L930:
	movl	124(%r13), %edx
	leal	1(%rdx), %eax
	movl	%eax, 124(%r13)
	movzwl	40(%rbx), %eax
	movl	%edx, 32(%rbx)
	andw	$-897, %ax
	orw	$384, %ax
	movw	%ax, 40(%rbx)
	movq	(%r12), %rbx
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L1091:
	cmpb	$2, %dl
	je	.L1093
	cmpb	$4, %dl
	je	.L947
	movq	8(%r13), %rcx
	testb	$1, 129(%rcx)
	jne	.L949
	testb	$1, 129(%r13)
	jne	.L893
.L949:
	cmpb	$6, %dl
	je	.L964
	testb	%dl, %dl
	jne	.L947
.L964:
	testb	$1, 130(%r13)
	je	.L947
	movl	$1, %eax
	jmp	.L1086
	.p2align 4,,10
	.p2align 3
.L962:
	testb	$14, %dl
	je	.L930
	subl	$7, %ecx
	cmpb	$3, %cl
	jbe	.L930
	andb	$4, %dh
	jne	.L930
.L1092:
	testb	$64, 129(%r13)
	jne	.L930
.L929:
	testb	%al, %al
	je	.L963
	cmpb	$6, %al
	jne	.L953
.L963:
	movq	%r13, %rax
	.p2align 4,,10
	.p2align 3
.L1042:
	movq	8(%rax), %rax
	testb	$1, 130(%rax)
	je	.L1042
	movzbl	128(%rax), %edx
	cmpb	$6, %dl
	je	.L1042
	testb	%dl, %dl
	je	.L1042
.L933:
	movl	120(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, 120(%rax)
	movzwl	40(%rbx), %eax
	movl	%edx, 32(%rbx)
	andw	$-897, %ax
	orb	$1, %ah
	movw	%ax, 40(%rbx)
	jmp	.L1084
	.p2align 4,,10
	.p2align 3
.L1088:
	movq	176(%r13), %rbx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal5Scope12MustAllocateEPNS0_8VariableE
	testb	%al, %al
	je	.L895
	movzbl	129(%r13), %edx
	movzwl	40(%rbx), %eax
	testb	%dl, %dl
	js	.L918
	movl	%eax, %esi
	andl	$15, %esi
	cmpb	$3, %sil
	je	.L919
	movzbl	128(%r13), %ecx
	cmpb	$5, %cl
	je	.L918
	cmpb	$1, %cl
	je	.L961
	cmpb	$4, %cl
	je	.L961
.L920:
	testb	$4, %ah
	jne	.L918
	andl	$64, %edx
	jne	.L918
.L919:
	testw	$896, %ax
	jne	.L895
	andw	$-897, %ax
	movl	$-1, 32(%rbx)
	orb	$-128, %al
	movw	%ax, 40(%rbx)
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L1089:
	movq	184(%r13), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal5Scope25AllocateNonParameterLocalEPNS0_8VariableE
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L1093:
	testb	$2, 131(%r13)
	jne	.L893
.L1086:
	testb	$4, 129(%r13)
	jne	.L893
.L947:
	cmpl	$4, 124(%r13)
	movl	$1, %eax
	jne	.L893
	movl	$0, 124(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L897:
	.cfi_restore_state
	movq	200(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L900
	call	_ZN2v88internal5Scope12MustAllocateEPNS0_8VariableE
	testb	%al, %al
	jne	.L1094
.L901:
	movq	$0, 200(%r13)
.L900:
	movl	136(%r13), %r12d
	subl	$1, %r12d
	js	.L903
	movq	144(%r13), %rax
	movslq	%r12d, %r15
	salq	$3, %r15
	movq	(%rax,%r15), %rbx
	testb	%r14b, %r14b
	jne	.L1095
	.p2align 4,,10
	.p2align 3
.L904:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal5Scope12MustAllocateEPNS0_8VariableE
	testb	%al, %al
	je	.L910
	movzbl	129(%r13), %edx
	movzwl	40(%rbx), %eax
	testb	%dl, %dl
	js	.L911
	movl	%eax, %ecx
	andl	$15, %ecx
	cmpb	$3, %cl
	je	.L912
	movzbl	128(%r13), %esi
	cmpb	$5, %sil
	je	.L911
	cmpb	$4, %sil
	je	.L960
	cmpb	$1, %sil
	je	.L960
.L913:
	testb	$4, %ah
	jne	.L911
	andl	$64, %edx
	je	.L912
	.p2align 4,,10
	.p2align 3
.L911:
	testw	$896, %ax
	je	.L1096
.L910:
	subl	$1, %r12d
	subq	$8, %r15
	cmpl	$-1, %r12d
	je	.L903
	movq	144(%r13), %rax
	movq	(%rax,%r15), %rbx
	testb	%r14b, %r14b
	je	.L904
.L1095:
	movzwl	40(%rbx), %eax
	orb	$8, %ah
	movl	%eax, %edx
	movw	%ax, 40(%rbx)
	andl	$15, %edx
	cmpb	$1, %dl
	je	.L905
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L906
	testb	$64, %ah
	je	.L1097
.L906:
	orb	$64, %ah
.L905:
	orb	$4, %ah
	movw	%ax, 40(%rbx)
	jmp	.L904
	.p2align 4,,10
	.p2align 3
.L912:
	testw	$896, %ax
	jne	.L910
	andw	$-897, %ax
	movl	%r12d, 32(%rbx)
	orb	$-128, %al
	movw	%ax, 40(%rbx)
	jmp	.L910
.L1097:
	movzwl	40(%rdx), %ecx
	movl	%ecx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L906
	movq	16(%rdx), %rax
	testq	%rax, %rax
	je	.L907
	testb	$64, %ch
	je	.L1098
.L907:
	orb	$64, %ch
	movw	%cx, 40(%rdx)
	movzwl	40(%rbx), %eax
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L1096:
	movl	124(%r13), %edx
	leal	1(%rdx), %eax
	movl	%eax, 124(%r13)
	movzwl	40(%rbx), %eax
	movl	%edx, 32(%rbx)
	andw	$-897, %ax
	orw	$384, %ax
	movw	%ax, 40(%rbx)
	jmp	.L910
	.p2align 4,,10
	.p2align 3
.L960:
	testb	$14, %al
	je	.L911
	subl	$7, %ecx
	cmpb	$3, %cl
	ja	.L913
	jmp	.L911
.L961:
	testb	$14, %al
	je	.L918
	subl	$7, %esi
	cmpb	$3, %sil
	ja	.L920
	.p2align 4,,10
	.p2align 3
.L918:
	testw	$896, %ax
	jne	.L895
	movl	124(%r13), %edx
	leal	1(%rdx), %eax
	movl	%eax, 124(%r13)
	movzwl	40(%rbx), %eax
	movl	%edx, 32(%rbx)
	andw	$-897, %ax
	orw	$384, %ax
	movw	%ax, 40(%rbx)
	jmp	.L895
.L1090:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L939
	movq	%r13, %rdi
	call	_ZN2v88internal5Scope12MustAllocateEPNS0_8VariableE
	testb	%al, %al
	jne	.L939
	movq	$0, (%rbx)
	jmp	.L939
.L1094:
	movzbl	131(%r13), %eax
	testb	$16, %al
	jne	.L901
	andl	$1, %eax
	testb	$1, 129(%r13)
	cmove	%eax, %r14d
	jmp	.L900
.L953:
	movq	%r13, %rax
	jmp	.L933
.L1098:
	movzwl	40(%rax), %esi
	movl	%esi, %edi
	andl	$15, %edi
	cmpb	$1, %dil
	je	.L907
	movq	16(%rax), %r8
	testq	%r8, %r8
	je	.L908
	testw	$16384, %si
	je	.L1099
.L908:
	orw	$16384, %si
	movw	%si, 40(%rax)
	movzwl	40(%rdx), %ecx
	jmp	.L907
.L1099:
	movzwl	40(%r8), %ecx
	movl	%ecx, %edi
	andl	$15, %edi
	cmpb	$1, %dil
	je	.L908
	movq	16(%r8), %rdi
	testq	%rdi, %rdi
	je	.L909
	testb	$64, %ch
	je	.L1100
.L909:
	orb	$64, %ch
	movw	%cx, 40(%r8)
	movzwl	40(%rax), %esi
	jmp	.L908
.L1100:
	movq	%r8, -72(%rbp)
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rdx
	movzwl	40(%r8), %ecx
	jmp	.L909
	.cfi_endproc
.LFE27772:
	.size	_ZZN2v88internal5Scope28AllocateVariablesRecursivelyEvENKUlPS1_E_clES2_.isra.0, .-_ZZN2v88internal5Scope28AllocateVariablesRecursivelyEvENKUlPS1_E_clES2_.isra.0
	.section	.text.unlikely._ZN2v88internal11ModuleScope23AllocateModuleVariablesEv,"ax",@progbits
	.align 2
.LCOLDB3:
	.section	.text._ZN2v88internal11ModuleScope23AllocateModuleVariablesEv,"ax",@progbits
.LHOTB3:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11ModuleScope23AllocateModuleVariablesEv
	.type	_ZN2v88internal11ModuleScope23AllocateModuleVariablesEv, @function
_ZN2v88internal11ModuleScope23AllocateModuleVariablesEv:
.LFB21309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	224(%rdi), %rax
	movq	208(%rax), %r10
	leaq	192(%rax), %rbx
	cmpq	%rbx, %r10
	je	.L1102
	.p2align 4,,10
	.p2align 3
.L1110:
	movq	32(%r10), %r8
	movl	40(%r12), %eax
	movq	32(%r12), %r9
	movl	24(%r8), %edx
	leal	-1(%rax), %edi
	shrl	$2, %edx
	andl	%edi, %edx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%r9,%rax,8), %rsi
	movq	(%rsi), %rcx
	testq	%rcx, %rcx
	jne	.L1105
	jmp	.L1103
	.p2align 4,,10
	.p2align 3
.L1124:
	addq	$1, %rdx
	andq	%rdi, %rdx
	leaq	(%rdx,%rdx,2), %rcx
	leaq	(%r9,%rcx,8), %rsi
	movq	(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L1123
.L1105:
	cmpq	%rcx, %r8
	jne	.L1124
	movq	40(%r10), %rax
	movq	8(%rsi), %rdx
	movq	%r10, %rdi
	movl	36(%rax), %ecx
	movzwl	40(%rdx), %eax
	andw	$-897, %ax
	movl	%ecx, 32(%rdx)
	orw	$640, %ax
	movw	%ax, 40(%rdx)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r10
	cmpq	%rbx, %rax
	jne	.L1110
	movq	224(%r12), %rax
.L1102:
	movq	152(%rax), %r10
	leaq	136(%rax), %rbx
	cmpq	%r10, %rbx
	je	.L1101
	.p2align 4,,10
	.p2align 3
.L1111:
	movq	32(%r10), %r8
	movl	40(%r12), %eax
	movq	32(%r12), %r9
	movl	24(%r8), %edx
	leal	-1(%rax), %edi
	shrl	$2, %edx
	andl	%edi, %edx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%r9,%rax,8), %rsi
	movq	(%rsi), %rcx
	testq	%rcx, %rcx
	jne	.L1109
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1126:
	addq	$1, %rdx
	andq	%rdi, %rdx
	leaq	(%rdx,%rdx,2), %rcx
	leaq	(%r9,%rcx,8), %rsi
	movq	(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L1125
.L1109:
	cmpq	%rcx, %r8
	jne	.L1126
	movq	40(%r10), %rax
	movq	8(%rsi), %rdx
	movq	%r10, %rdi
	movl	36(%rax), %ecx
	movzwl	40(%rdx), %eax
	andw	$-897, %ax
	movl	%ecx, 32(%rdx)
	orw	$640, %ax
	movw	%ax, 40(%rdx)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r10
	cmpq	%rax, %rbx
	jne	.L1111
.L1101:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1125:
	.cfi_restore_state
	jmp	.L1107
.L1123:
	jmp	.L1103
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal11ModuleScope23AllocateModuleVariablesEv
	.cfi_startproc
	.type	_ZN2v88internal11ModuleScope23AllocateModuleVariablesEv.cold, @function
_ZN2v88internal11ModuleScope23AllocateModuleVariablesEv.cold:
.LFSB21309:
.L1103:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movzwl	40, %eax
	ud2
.L1107:
	movzwl	40, %eax
	ud2
	.cfi_endproc
.LFE21309:
	.section	.text._ZN2v88internal11ModuleScope23AllocateModuleVariablesEv
	.size	_ZN2v88internal11ModuleScope23AllocateModuleVariablesEv, .-_ZN2v88internal11ModuleScope23AllocateModuleVariablesEv
	.section	.text.unlikely._ZN2v88internal11ModuleScope23AllocateModuleVariablesEv
	.size	_ZN2v88internal11ModuleScope23AllocateModuleVariablesEv.cold, .-_ZN2v88internal11ModuleScope23AllocateModuleVariablesEv.cold
.LCOLDE3:
	.section	.text._ZN2v88internal11ModuleScope23AllocateModuleVariablesEv
.LHOTE3:
	.section	.text._ZN2v88internal5Scope28AllocateVariablesRecursivelyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope28AllocateVariablesRecursivelyEv
	.type	_ZN2v88internal5Scope28AllocateVariablesRecursivelyEv, @function
_ZN2v88internal5Scope28AllocateVariablesRecursivelyEv:
.LFB21310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rbx, %rdi
	call	_ZZN2v88internal5Scope28AllocateVariablesRecursivelyEvENKUlPS1_E_clES2_.isra.0
	cmpl	$1, %eax
	jne	.L1136
	jmp	.L1137
	.p2align 4,,10
	.p2align 3
.L1134:
	cmpq	%rbx, %r12
	je	.L1127
	movq	8(%rbx), %rbx
.L1136:
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L1134
	cmpq	%rbx, %r12
	je	.L1127
.L1130:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	call	_ZZN2v88internal5Scope28AllocateVariablesRecursivelyEvENKUlPS1_E_clES2_.isra.0
	cmpl	$1, %eax
	jne	.L1136
.L1137:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	jne	.L1130
	jmp	.L1136
	.p2align 4,,10
	.p2align 3
.L1127:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21310:
	.size	_ZN2v88internal5Scope28AllocateVariablesRecursivelyEv, .-_ZN2v88internal5Scope28AllocateVariablesRecursivelyEv
	.section	.text._ZN2v88internal5Scope29AllocateScopeInfosRecursivelyEPNS0_7IsolateENS0_11MaybeHandleINS0_9ScopeInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope29AllocateScopeInfosRecursivelyEPNS0_7IsolateENS0_11MaybeHandleINS0_9ScopeInfoEEE
	.type	_ZN2v88internal5Scope29AllocateScopeInfosRecursivelyEPNS0_7IsolateENS0_11MaybeHandleINS0_9ScopeInfoEEE, @function
_ZN2v88internal5Scope29AllocateScopeInfosRecursivelyEPNS0_7IsolateENS0_11MaybeHandleINS0_9ScopeInfoEEE:
.LFB21314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpb	$2, 128(%rdi)
	movq	%rdi, %rbx
	je	.L1139
	movl	124(%rdi), %ecx
	movq	%rdx, %r12
	testl	%ecx, %ecx
	jg	.L1139
.L1140:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1138
.L1145:
	cmpb	$2, 128(%rbx)
	jne	.L1143
	movzbl	131(%rbx), %eax
	testb	$4, %al
	jne	.L1143
	testb	$64, %al
	je	.L1144
	.p2align 4,,10
	.p2align 3
.L1143:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal5Scope29AllocateScopeInfosRecursivelyEPNS0_7IsolateENS0_11MaybeHandleINS0_9ScopeInfoEEE
.L1144:
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L1145
.L1138:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1139:
	.cfi_restore_state
	movq	(%rbx), %rsi
	movq	%rbx, %rdx
	movq	%r14, %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal9ScopeInfo6CreateEPNS0_7IsolateEPNS0_4ZoneEPNS0_5ScopeENS0_11MaybeHandleIS1_EE@PLT
	movl	124(%rbx), %edx
	movq	%rax, 104(%rbx)
	testl	%edx, %edx
	cmovle	%r14, %rax
	movq	%rax, %r12
	jmp	.L1140
	.cfi_endproc
.LFE21314:
	.size	_ZN2v88internal5Scope29AllocateScopeInfosRecursivelyEPNS0_7IsolateENS0_11MaybeHandleINS0_9ScopeInfoEEE, .-_ZN2v88internal5Scope29AllocateScopeInfosRecursivelyEPNS0_7IsolateENS0_11MaybeHandleINS0_9ScopeInfoEEE
	.section	.text._ZN2v88internal16DeclarationScope18AllocateScopeInfosEPNS0_9ParseInfoEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16DeclarationScope18AllocateScopeInfosEPNS0_9ParseInfoEPNS0_7IsolateE
	.type	_ZN2v88internal16DeclarationScope18AllocateScopeInfosEPNS0_9ParseInfoEPNS0_7IsolateE, @function
_ZN2v88internal16DeclarationScope18AllocateScopeInfosEPNS0_9ParseInfoEPNS0_7IsolateE:
.LFB21315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	168(%rdi), %rax
	movq	40(%rax), %r12
	cmpq	$0, 104(%r12)
	je	.L1169
.L1154:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1169:
	.cfi_restore_state
	movq	8(%r12), %rax
	movq	%rdi, %rbx
	movq	%rsi, %r13
	xorl	%r14d, %r14d
	testq	%rax, %rax
	je	.L1156
	movq	104(%rax), %r14
.L1156:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal5Scope29AllocateScopeInfosRecursivelyEPNS0_7IsolateENS0_11MaybeHandleINS0_9ScopeInfoEEE
	cmpq	$0, 104(%r12)
	je	.L1170
.L1157:
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L1154
	cmpq	$0, 104(%rax)
	jne	.L1154
	movq	%r13, %rdi
	call	_ZN2v88internal9ScopeInfo5EmptyEPNS0_7IsolateE@PLT
	movq	41112(%r13), %rdi
	movq	24(%rbx), %rbx
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L1159
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1160:
	movq	%rax, 104(%rbx)
	jmp	.L1154
	.p2align 4,,10
	.p2align 3
.L1170:
	movq	(%r12), %rsi
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal9ScopeInfo6CreateEPNS0_7IsolateEPNS0_4ZoneEPNS0_5ScopeENS0_11MaybeHandleIS1_EE@PLT
	movq	%rax, 104(%r12)
	jmp	.L1157
	.p2align 4,,10
	.p2align 3
.L1159:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L1171
.L1161:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L1160
.L1171:
	movq	%r13, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L1161
	.cfi_endproc
.LFE21315:
	.size	_ZN2v88internal16DeclarationScope18AllocateScopeInfosEPNS0_9ParseInfoEPNS0_7IsolateE, .-_ZN2v88internal16DeclarationScope18AllocateScopeInfosEPNS0_9ParseInfoEPNS0_7IsolateE
	.section	.text._ZNK2v88internal5Scope17ContextLocalCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal5Scope17ContextLocalCountEv
	.type	_ZNK2v88internal5Scope17ContextLocalCountEv, @function
_ZNK2v88internal5Scope17ContextLocalCountEv:
.LFB21319:
	.cfi_startproc
	endbr64
	movl	124(%rdi), %eax
	testl	%eax, %eax
	je	.L1172
	cmpb	$2, 128(%rdi)
	leal	-4(%rax), %edx
	je	.L1181
.L1174:
	movl	%edx, %eax
.L1172:
	ret
	.p2align 4,,10
	.p2align 3
.L1181:
	movq	184(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L1174
	movzwl	40(%rcx), %ecx
	subl	$5, %eax
	sarl	$7, %ecx
	andl	$7, %ecx
	cmpb	$3, %cl
	cmove	%eax, %edx
	movl	%edx, %eax
	jmp	.L1172
	.cfi_endproc
.LFE21319:
	.size	_ZNK2v88internal5Scope17ContextLocalCountEv, .-_ZNK2v88internal5Scope17ContextLocalCountEv
	.section	.text._ZN2v88internal27IsComplementaryAccessorPairENS0_12VariableModeES1_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27IsComplementaryAccessorPairENS0_12VariableModeES1_
	.type	_ZN2v88internal27IsComplementaryAccessorPairENS0_12VariableModeES1_, @function
_ZN2v88internal27IsComplementaryAccessorPairENS0_12VariableModeES1_:
.LFB21320:
	.cfi_startproc
	endbr64
	cmpb	$8, %dil
	je	.L1183
	xorl	%eax, %eax
	cmpb	$9, %dil
	jne	.L1182
	cmpb	$8, %sil
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L1183:
	cmpb	$9, %sil
	sete	%al
.L1182:
	ret
	.cfi_endproc
.LFE21320:
	.size	_ZN2v88internal27IsComplementaryAccessorPairENS0_12VariableModeES1_, .-_ZN2v88internal27IsComplementaryAccessorPairENS0_12VariableModeES1_
	.section	.text._ZN2v88internal10ClassScope22LookupLocalPrivateNameEPKNS0_12AstRawStringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ClassScope22LookupLocalPrivateNameEPKNS0_12AstRawStringE
	.type	_ZN2v88internal10ClassScope22LookupLocalPrivateNameEPKNS0_12AstRawStringE, @function
_ZN2v88internal10ClassScope22LookupLocalPrivateNameEPKNS0_12AstRawStringE:
.LFB21322:
	.cfi_startproc
	endbr64
	movq	136(%rdi), %r8
	testq	%r8, %r8
	je	.L1186
	movl	24(%r8), %ecx
	movl	24(%rsi), %eax
	movq	16(%r8), %rdi
	subl	$1, %ecx
	shrl	$2, %eax
	andl	%ecx, %eax
	jmp	.L1197
	.p2align 4,,10
	.p2align 3
.L1198:
	cmpq	%r8, %rsi
	je	.L1188
	addq	$1, %rax
	andq	%rcx, %rax
.L1197:
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	movq	(%rdx), %r8
	testq	%r8, %r8
	jne	.L1198
.L1186:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1188:
	movq	8(%rdx), %r8
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE21322:
	.size	_ZN2v88internal10ClassScope22LookupLocalPrivateNameEPKNS0_12AstRawStringE, .-_ZN2v88internal10ClassScope22LookupLocalPrivateNameEPKNS0_12AstRawStringE
	.section	.text._ZN2v88internal10ClassScope28GetUnresolvedPrivateNameTailEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ClassScope28GetUnresolvedPrivateNameTailEv
	.type	_ZN2v88internal10ClassScope28GetUnresolvedPrivateNameTailEv, @function
_ZN2v88internal10ClassScope28GetUnresolvedPrivateNameTailEv:
.LFB21323:
	.cfi_startproc
	endbr64
	movq	136(%rdi), %rax
	testq	%rax, %rax
	je	.L1200
	movq	8(%rax), %rax
.L1200:
	ret
	.cfi_endproc
.LFE21323:
	.size	_ZN2v88internal10ClassScope28GetUnresolvedPrivateNameTailEv, .-_ZN2v88internal10ClassScope28GetUnresolvedPrivateNameTailEv
	.section	.text._ZN2v88internal10ClassScope30ResetUnresolvedPrivateNameTailENS_4base16ThreadedListBaseINS0_13VariableProxyENS2_9EmptyBaseENS4_14UnresolvedNextEE8IteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ClassScope30ResetUnresolvedPrivateNameTailENS_4base16ThreadedListBaseINS0_13VariableProxyENS2_9EmptyBaseENS4_14UnresolvedNextEE8IteratorE
	.type	_ZN2v88internal10ClassScope30ResetUnresolvedPrivateNameTailENS_4base16ThreadedListBaseINS0_13VariableProxyENS2_9EmptyBaseENS4_14UnresolvedNextEE8IteratorE, @function
_ZN2v88internal10ClassScope30ResetUnresolvedPrivateNameTailENS_4base16ThreadedListBaseINS0_13VariableProxyENS2_9EmptyBaseENS4_14UnresolvedNextEE8IteratorE:
.LFB21324:
	.cfi_startproc
	endbr64
	movq	136(%rdi), %rax
	testq	%rax, %rax
	je	.L1204
	cmpq	8(%rax), %rsi
	je	.L1204
	testq	%rsi, %rsi
	je	.L1210
	movq	%rsi, 8(%rax)
	movq	$0, (%rsi)
.L1204:
	ret
	.p2align 4,,10
	.p2align 3
.L1210:
	movq	$0, (%rax)
	movq	%rax, 8(%rax)
	ret
	.cfi_endproc
.LFE21324:
	.size	_ZN2v88internal10ClassScope30ResetUnresolvedPrivateNameTailENS_4base16ThreadedListBaseINS0_13VariableProxyENS2_9EmptyBaseENS4_14UnresolvedNextEE8IteratorE, .-_ZN2v88internal10ClassScope30ResetUnresolvedPrivateNameTailENS_4base16ThreadedListBaseINS0_13VariableProxyENS2_9EmptyBaseENS4_14UnresolvedNextEE8IteratorE
	.section	.text._ZN2v88internal10ClassScope32MigrateUnresolvedPrivateNameTailEPNS0_14AstNodeFactoryENS_4base16ThreadedListBaseINS0_13VariableProxyENS4_9EmptyBaseENS6_14UnresolvedNextEE8IteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ClassScope32MigrateUnresolvedPrivateNameTailEPNS0_14AstNodeFactoryENS_4base16ThreadedListBaseINS0_13VariableProxyENS4_9EmptyBaseENS6_14UnresolvedNextEE8IteratorE
	.type	_ZN2v88internal10ClassScope32MigrateUnresolvedPrivateNameTailEPNS0_14AstNodeFactoryENS_4base16ThreadedListBaseINS0_13VariableProxyENS4_9EmptyBaseENS6_14UnresolvedNextEE8IteratorE, @function
_ZN2v88internal10ClassScope32MigrateUnresolvedPrivateNameTailEPNS0_14AstNodeFactoryENS_4base16ThreadedListBaseINS0_13VariableProxyENS4_9EmptyBaseENS6_14UnresolvedNextEE8IteratorE:
.LFB21325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	136(%rdi), %rax
	testq	%rax, %rax
	je	.L1211
	movq	%rsi, %r13
	movq	%rdx, %rsi
	cmpq	8(%rax), %rdx
	je	.L1211
	leaq	-80(%rbp), %rdx
	movq	$0, -80(%rbp)
	movq	%rdi, %rbx
	movq	%rdx, -72(%rbp)
	testq	%rsi, %rsi
	je	.L1257
	movq	%rsi, %r14
	cmpq	8(%rax), %rsi
	je	.L1221
	movq	(%rsi), %r12
	.p2align 4,,10
	.p2align 3
.L1228:
	movq	0(%r13), %rdi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	$23, %rax
	jbe	.L1258
	leaq	24(%r15), %rax
	movq	%rax, 16(%rdi)
.L1223:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal13VariableProxyC1EPKS1_@PLT
	movq	-72(%rbp), %rax
	leaq	16(%r15), %rcx
	movq	%r15, (%rax)
	movq	16(%r15), %rax
	testq	%rax, %rax
	jne	.L1225
	jmp	.L1224
	.p2align 4,,10
	.p2align 3
.L1259:
	leaq	16(%rax), %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1224
.L1225:
	testb	$2, 5(%rax)
	jne	.L1259
.L1224:
	movq	%rcx, -72(%rbp)
	movq	(%r14), %rax
	movq	16(%rax), %r12
	leaq	16(%rax), %r14
	testq	%r12, %r12
	jne	.L1227
	jmp	.L1226
	.p2align 4,,10
	.p2align 3
.L1260:
	leaq	16(%r12), %r14
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L1226
.L1227:
	testb	$2, 5(%r12)
	jne	.L1260
.L1226:
	movq	136(%rbx), %rax
	cmpq	%r14, 8(%rax)
	jne	.L1228
	movq	-88(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L1220
	movq	%rsi, 8(%rax)
	movq	-80(%rbp), %rdx
	movq	$0, (%rsi)
.L1230:
	testq	%rdx, %rdx
	je	.L1211
	movq	136(%rbx), %rax
	movq	8(%rax), %rcx
	movq	%rdx, (%rcx)
	movq	-72(%rbp), %rdx
	movq	%rdx, 8(%rax)
.L1211:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1261
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1258:
	.cfi_restore_state
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L1223
	.p2align 4,,10
	.p2align 3
.L1257:
	movq	(%rax), %r12
	movq	%rax, %r14
	testq	%r12, %r12
	jne	.L1218
	jmp	.L1217
	.p2align 4,,10
	.p2align 3
.L1262:
	leaq	16(%r12), %r14
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L1217
.L1218:
	testb	$2, 5(%r12)
	jne	.L1262
.L1217:
	cmpq	8(%rax), %r14
	jne	.L1228
	.p2align 4,,10
	.p2align 3
.L1220:
	movq	$0, (%rax)
	movq	-80(%rbp), %rdx
	movq	%rax, 8(%rax)
	jmp	.L1230
.L1221:
	movq	$0, (%rsi)
	jmp	.L1211
.L1261:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21325:
	.size	_ZN2v88internal10ClassScope32MigrateUnresolvedPrivateNameTailEPNS0_14AstNodeFactoryENS_4base16ThreadedListBaseINS0_13VariableProxyENS4_9EmptyBaseENS6_14UnresolvedNextEE8IteratorE, .-_ZN2v88internal10ClassScope32MigrateUnresolvedPrivateNameTailEPNS0_14AstNodeFactoryENS_4base16ThreadedListBaseINS0_13VariableProxyENS4_9EmptyBaseENS6_14UnresolvedNextEE8IteratorE
	.section	.text._ZN2v88internal10ClassScope24AddUnresolvedPrivateNameEPNS0_13VariableProxyE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ClassScope24AddUnresolvedPrivateNameEPNS0_13VariableProxyE
	.type	_ZN2v88internal10ClassScope24AddUnresolvedPrivateNameEPNS0_13VariableProxyE, @function
_ZN2v88internal10ClassScope24AddUnresolvedPrivateNameEPNS0_13VariableProxyE:
.LFB21326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	136(%rdi), %r14
	movq	%rsi, %rbx
	testq	%r14, %r14
	je	.L1283
.L1264:
	movq	8(%r14), %rax
	leaq	16(%rbx), %rdx
	movq	%rbx, (%rax)
	movq	16(%rbx), %rax
	testq	%rax, %rax
	jne	.L1273
	jmp	.L1272
	.p2align 4,,10
	.p2align 3
.L1284:
	leaq	16(%rax), %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1272
.L1273:
	testb	$2, 5(%rax)
	jne	.L1284
.L1272:
	movq	%rdx, 8(%r14)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1283:
	.cfi_restore_state
	movq	%rdi, %r12
	movq	(%rdi), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$47, %rax
	jbe	.L1285
	leaq	48(%r13), %rax
	movq	%rax, 16(%rdi)
.L1266:
	movq	(%r12), %rdi
	movq	$0, 0(%r13)
	movq	%r13, %r14
	movq	%r13, 8(%r13)
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$191, %rdx
	jbe	.L1286
	leaq	192(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1268:
	movq	%rax, 16(%r13)
	testq	%rax, %rax
	je	.L1287
	movl	$8, 24(%r13)
	movq	$0, (%rax)
	cmpl	$1, 24(%r13)
	jbe	.L1270
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L1271:
	movq	16(%r13), %rcx
	addq	$1, %rax
	movq	$0, (%rcx,%rdx)
	movl	24(%r13), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L1271
.L1270:
	movl	$0, 28(%r13)
	movq	$0, 40(%r13)
	movq	%r13, 136(%r12)
	jmp	.L1264
	.p2align 4,,10
	.p2align 3
.L1285:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L1266
	.p2align 4,,10
	.p2align 3
.L1286:
	movl	$192, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1268
.L1287:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21326:
	.size	_ZN2v88internal10ClassScope24AddUnresolvedPrivateNameEPNS0_13VariableProxyE, .-_ZN2v88internal10ClassScope24AddUnresolvedPrivateNameEPNS0_13VariableProxyE
	.section	.text._ZN2v88internal10ClassScope28ResolvePrivateNamesPartiallyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ClassScope28ResolvePrivateNamesPartiallyEv
	.type	_ZN2v88internal10ClassScope28ResolvePrivateNamesPartiallyEv, @function
_ZN2v88internal10ClassScope28ResolvePrivateNamesPartiallyEv:
.LFB21330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	136(%rdi), %r14
	testq	%r14, %r14
	je	.L1289
	movq	(%r14), %r8
	testq	%r8, %r8
	je	.L1289
	movq	8(%rdi), %rbx
	movq	%rdi, %r15
	testq	%rbx, %rbx
	je	.L1290
	.p2align 4,,10
	.p2align 3
.L1292:
	cmpb	$0, 128(%rbx)
	je	.L1291
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L1292
.L1290:
	movl	24(%r14), %edx
	testl	%edx, %edx
	jne	.L1420
.L1288:
	addq	$40, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1291:
	.cfi_restore_state
	movl	24(%r14), %edx
.L1334:
	movq	%r8, %r12
	movq	16(%r12), %r13
	cmpq	%r8, %r12
	je	.L1421
	.p2align 4,,10
	.p2align 3
.L1295:
	testq	%r8, %r8
	je	.L1301
	movq	16(%r8), %rdi
	leaq	16(%r8), %rsi
	testq	%rdi, %rdi
	je	.L1301
	movq	%rdi, %rcx
	movq	%rdi, %rax
	jmp	.L1305
	.p2align 4,,10
	.p2align 3
.L1422:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L1306
	movq	%rax, %rcx
.L1305:
	testb	$2, 5(%rax)
	jne	.L1422
	cmpq	%rax, %r12
	je	.L1307
.L1306:
	movq	%rax, %r8
	jmp	.L1295
	.p2align 4,,10
	.p2align 3
.L1339:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1308:
	movq	%rsi, %rcx
	jmp	.L1311
	.p2align 4,,10
	.p2align 3
.L1423:
	leaq	16(%rdi), %rcx
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1310
.L1311:
	testb	$2, 5(%rdi)
	jne	.L1423
.L1310:
	movq	%rax, (%rcx)
	movq	16(%r12), %rax
	movq	%r9, %rcx
	testq	%rax, %rax
	jne	.L1313
	jmp	.L1312
	.p2align 4,,10
	.p2align 3
.L1424:
	leaq	16(%rax), %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1312
.L1313:
	testb	$2, 5(%rax)
	jne	.L1424
.L1312:
	movq	$0, (%rcx)
	movq	16(%r12), %rax
	testq	%rax, %rax
	jne	.L1315
	jmp	.L1314
	.p2align 4,,10
	.p2align 3
.L1425:
	leaq	16(%rax), %r9
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1314
.L1315:
	testb	$2, 5(%rax)
	jne	.L1425
.L1314:
	cmpq	%r9, 8(%r14)
	jne	.L1301
	movq	16(%r8), %rax
	testq	%rax, %rax
	jne	.L1317
	jmp	.L1316
	.p2align 4,,10
	.p2align 3
.L1426:
	leaq	16(%rax), %rsi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1316
.L1317:
	testb	$2, 5(%rax)
	jne	.L1426
.L1316:
	movq	%rsi, 8(%r14)
	.p2align 4,,10
	.p2align 3
.L1301:
	testl	%edx, %edx
	je	.L1318
	movq	8(%r12), %r8
	testb	$1, 5(%r12)
	je	.L1319
	movq	8(%r8), %r8
.L1319:
	movq	136(%r15), %rax
	testq	%rax, %rax
	je	.L1318
	movl	24(%rax), %edi
	movq	16(%rax), %r9
	movl	24(%r8), %eax
	subl	$1, %edi
	shrl	$2, %eax
	andl	%edi, %eax
	jmp	.L1419
	.p2align 4,,10
	.p2align 3
.L1427:
	cmpq	%rcx, %r8
	je	.L1320
	addq	$1, %rax
	andq	%rdi, %rax
.L1419:
	leaq	(%rax,%rax,2), %rcx
	leaq	(%r9,%rcx,8), %rsi
	movq	(%rsi), %rcx
	testq	%rcx, %rcx
	jne	.L1427
.L1318:
	testq	%rbx, %rbx
	je	.L1346
	movq	136(%rbx), %r8
	testq	%r8, %r8
	je	.L1428
.L1323:
	movq	8(%r8), %rax
	leaq	16(%r12), %rcx
	movq	%r12, (%rax)
	movq	16(%r12), %rax
	testq	%rax, %rax
	jne	.L1332
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1429:
	leaq	16(%rax), %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1331
.L1332:
	testb	$2, 5(%rax)
	jne	.L1429
.L1331:
	movq	%rcx, 8(%r8)
	testq	%r13, %r13
	je	.L1289
.L1433:
	movq	(%r14), %r8
	movq	%r13, %r12
	movq	16(%r12), %r13
	cmpq	%r8, %r12
	jne	.L1295
.L1421:
	leaq	16(%r12), %rsi
	testq	%r13, %r13
	je	.L1296
	movq	%r13, %rcx
	movq	%r13, %rax
	jmp	.L1298
	.p2align 4,,10
	.p2align 3
.L1430:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L1296
	movq	%rax, %rcx
.L1298:
	testb	$2, 5(%rax)
	jne	.L1430
	movq	%rax, (%r14)
.L1335:
	movq	16(%r12), %rax
	testq	%rax, %rax
	jne	.L1300
	jmp	.L1299
	.p2align 4,,10
	.p2align 3
.L1431:
	leaq	16(%rax), %rsi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1299
.L1300:
	testb	$2, 5(%rax)
	jne	.L1431
.L1299:
	movq	$0, (%rsi)
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1307:
	leaq	16(%r12), %r9
	testq	%r13, %r13
	je	.L1339
	movq	%r13, %rcx
	movq	%r13, %rax
	jmp	.L1309
	.p2align 4,,10
	.p2align 3
.L1432:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L1308
	movq	%rax, %rcx
.L1309:
	testb	$2, 5(%rax)
	jne	.L1432
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1320:
	movq	8(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L1318
	orw	$2048, 40(%rsi)
	movq	%r12, %rdi
	movl	%edx, -56(%rbp)
	call	_ZN2v88internal13VariableProxy6BindToEPNS0_8VariableE@PLT
	movl	-56(%rbp), %edx
	testq	%r13, %r13
	jne	.L1433
	.p2align 4,,10
	.p2align 3
.L1289:
	xorl	%r8d, %r8d
	jmp	.L1288
	.p2align 4,,10
	.p2align 3
.L1428:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r9
	movq	24(%rdi), %rax
	subq	%r9, %rax
	cmpq	$47, %rax
	jbe	.L1434
	leaq	48(%r9), %rax
	movq	%rax, 16(%rdi)
.L1325:
	movq	(%rbx), %rdi
	movq	$0, (%r9)
	movq	%r9, %r8
	movq	%r9, 8(%r9)
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	$191, %rcx
	jbe	.L1435
	leaq	192(%rax), %rcx
	movq	%rcx, 16(%rdi)
.L1327:
	movq	%rax, 16(%r9)
	testq	%rax, %rax
	je	.L1436
	movl	$8, 24(%r9)
	movq	$0, (%rax)
	cmpl	$1, 24(%r9)
	jbe	.L1329
	movl	$24, %ecx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L1330:
	movq	16(%r9), %rsi
	addq	$1, %rax
	movq	$0, (%rsi,%rcx)
	movl	24(%r9), %esi
	addq	$24, %rcx
	cmpq	%rax, %rsi
	ja	.L1330
.L1329:
	movl	$0, 28(%r9)
	movq	$0, 40(%r9)
	movq	%r9, 136(%rbx)
	jmp	.L1323
	.p2align 4,,10
	.p2align 3
.L1420:
	xorl	%ebx, %ebx
	jmp	.L1334
	.p2align 4,,10
	.p2align 3
.L1346:
	movq	%r12, %r8
	jmp	.L1288
	.p2align 4,,10
	.p2align 3
.L1296:
	movq	$0, (%r14)
	movq	%r14, 8(%r14)
	jmp	.L1335
.L1435:
	movl	$192, %esi
	movl	%edx, -68(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	movl	-68(%rbp), %edx
	jmp	.L1327
.L1434:
	movl	$48, %esi
	movl	%edx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-56(%rbp), %edx
	movq	%rax, %r9
	jmp	.L1325
.L1436:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21330:
	.size	_ZN2v88internal10ClassScope28ResolvePrivateNamesPartiallyEv, .-_ZN2v88internal10ClassScope28ResolvePrivateNamesPartiallyEv
	.section	.text._ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS_8internal20ZoneAllocationPolicyEE6RemoveERKS2_j,"axG",@progbits,_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS_8internal20ZoneAllocationPolicyEE6RemoveERKS2_j,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS_8internal20ZoneAllocationPolicyEE6RemoveERKS2_j
	.type	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS_8internal20ZoneAllocationPolicyEE6RemoveERKS2_j, @function
_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS_8internal20ZoneAllocationPolicyEE6RemoveERKS2_j:
.LFB23690:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %r10d
	movq	%rsi, %r9
	movq	(%rdi), %rcx
	leal	-1(%r10), %esi
	andl	%esi, %edx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rcx,%rax,8), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	je	.L1437
	movq	(%r9), %r9
	jmp	.L1440
	.p2align 4,,10
	.p2align 3
.L1457:
	addq	$1, %rdx
	andq	%rsi, %rdx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rcx,%rax,8), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	je	.L1437
.L1440:
	cmpq	%r8, %r9
	jne	.L1457
	movq	8(%rax), %r8
	movq	%rax, %rsi
	movl	%r10d, %r9d
	.p2align 4,,10
	.p2align 3
.L1446:
	leaq	(%r9,%r9,2), %rdx
	addq	$24, %rax
	leaq	(%rcx,%rdx,8), %rdx
	cmpq	%rdx, %rax
	cmove	%rcx, %rax
	cmpq	$0, (%rax)
	je	.L1442
	leal	-1(%r10), %edx
	andl	16(%rax), %edx
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rcx,%rdx,8), %rdx
	cmpq	%rax, %rsi
	jnb	.L1443
	cmpq	%rdx, %rsi
	jnb	.L1444
	cmpq	%rdx, %rax
	jnb	.L1446
.L1444:
	movdqu	(%rax), %xmm0
	movups	%xmm0, (%rsi)
	movl	16(%rax), %edx
	movl	%edx, 16(%rsi)
	movl	8(%rdi), %r9d
	movq	%rax, %rsi
	movq	(%rdi), %rcx
	movq	%r9, %r10
	jmp	.L1446
	.p2align 4,,10
	.p2align 3
.L1443:
	jbe	.L1446
	cmpq	%rdx, %rsi
	jb	.L1446
	cmpq	%rdx, %rax
	jb	.L1444
	jmp	.L1446
	.p2align 4,,10
	.p2align 3
.L1442:
	movq	$0, (%rsi)
	subl	$1, 12(%rdi)
.L1437:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE23690:
	.size	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS_8internal20ZoneAllocationPolicyEE6RemoveERKS2_j, .-_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS_8internal20ZoneAllocationPolicyEE6RemoveERKS2_j
	.section	.text._ZN2v88internal11VariableMap6RemoveEPNS0_8VariableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11VariableMap6RemoveEPNS0_8VariableE
	.type	_ZN2v88internal11VariableMap6RemoveEPNS0_8VariableE, @function
_ZN2v88internal11VariableMap6RemoveEPNS0_8VariableE:
.LFB21164:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	leaq	-16(%rbp), %rsi
	movl	24(%rax), %edx
	movq	%rax, -16(%rbp)
	shrl	$2, %edx
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS_8internal20ZoneAllocationPolicyEE6RemoveERKS2_j
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1461
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1461:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21164:
	.size	_ZN2v88internal11VariableMap6RemoveEPNS0_8VariableE, .-_ZN2v88internal11VariableMap6RemoveEPNS0_8VariableE
	.section	.text._ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS_8internal20ZoneAllocationPolicyEE6ResizeES6_,"axG",@progbits,_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS_8internal20ZoneAllocationPolicyEE6ResizeES6_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS_8internal20ZoneAllocationPolicyEE6ResizeES6_
	.type	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS_8internal20ZoneAllocationPolicyEE6ResizeES6_, @function
_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS_8internal20ZoneAllocationPolicyEE6ResizeES6_:
.LFB26494:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	8(%rdi), %eax
	movq	24(%r14), %rdx
	movq	(%rdi), %r12
	movl	12(%rdi), %r13d
	addl	%eax, %eax
	leaq	(%rax,%rax,2), %rsi
	movq	%rax, %r15
	movq	16(%r14), %rax
	salq	$3, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1503
	addq	%rax, %rsi
	movq	%rsi, 16(%r14)
.L1464:
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L1504
	movl	%r15d, 8(%rbx)
	testl	%r15d, %r15d
	je	.L1466
	movq	$0, (%rax)
	cmpl	$1, 8(%rbx)
	movl	$24, %edx
	movl	$1, %eax
	jbe	.L1466
	.p2align 4,,10
	.p2align 3
.L1467:
	movq	(%rbx), %rcx
	addq	$1, %rax
	movq	$0, (%rcx,%rdx)
	movl	8(%rbx), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L1467
.L1466:
	movl	$0, 12(%rbx)
	testl	%r13d, %r13d
	je	.L1462
.L1469:
	movq	(%r12), %rsi
	testq	%rsi, %rsi
	jne	.L1505
.L1470:
	movq	24(%r12), %rsi
	addq	$24, %r12
	testq	%rsi, %rsi
	je	.L1470
.L1505:
	movl	8(%rbx), %eax
	movl	16(%r12), %r15d
	movq	(%rbx), %r8
	leal	-1(%rax), %edi
	movl	%r15d, %eax
	andl	%edi, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%r8,%rdx,8), %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L1471
	cmpq	%rsi, %rdx
	jne	.L1472
	jmp	.L1471
	.p2align 4,,10
	.p2align 3
.L1506:
	cmpq	%rdx, %rsi
	je	.L1471
.L1472:
	addq	$1, %rax
	andq	%rdi, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%r8,%rdx,8), %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.L1506
.L1471:
	movq	8(%r12), %rax
	movq	%rsi, (%rcx)
	movl	%r15d, 16(%rcx)
	movq	%rax, 8(%rcx)
	movl	12(%rbx), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 12(%rbx)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	8(%rbx), %eax
	jnb	.L1474
.L1477:
	addq	$24, %r12
	subl	$1, %r13d
	jne	.L1469
.L1462:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1474:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS_8internal20ZoneAllocationPolicyEE6ResizeES6_
	movl	8(%rbx), %eax
	movq	(%rbx), %rsi
	leal	-1(%rax), %ecx
	movl	%r15d, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	movq	(%rsi,%rdx,8), %rdx
	testq	%rdx, %rdx
	je	.L1477
	movq	(%r12), %rdi
	jmp	.L1478
	.p2align 4,,10
	.p2align 3
.L1507:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	movq	(%rsi,%rdx,8), %rdx
	testq	%rdx, %rdx
	je	.L1477
.L1478:
	cmpq	%rdx, %rdi
	jne	.L1507
	jmp	.L1477
.L1503:
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1464
.L1504:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE26494:
	.size	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS_8internal20ZoneAllocationPolicyEE6ResizeES6_, .-_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS_8internal20ZoneAllocationPolicyEE6ResizeES6_
	.section	.text._ZN2v88internal11VariableMap3AddEPNS0_4ZoneEPNS0_8VariableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11VariableMap3AddEPNS0_4ZoneEPNS0_8VariableE
	.type	_ZN2v88internal11VariableMap3AddEPNS0_4ZoneEPNS0_8VariableE, @function
_ZN2v88internal11VariableMap3AddEPNS0_4ZoneEPNS0_8VariableE:
.LFB21165:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rdx), %r13
	movl	8(%rdi), %eax
	movq	(%rdi), %rdi
	movl	24(%r13), %ebx
	leal	-1(%rax), %r8d
	shrl	$2, %ebx
	movl	%ebx, %eax
	andl	%r8d, %eax
	jmp	.L1532
	.p2align 4,,10
	.p2align 3
.L1533:
	cmpq	%rdx, %r13
	je	.L1510
	addq	$1, %rax
	andq	%r8, %rax
.L1532:
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.L1533
	movq	%r13, (%rcx)
	movq	$0, 8(%rcx)
	movl	%ebx, 16(%rcx)
	movl	12(%r14), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 12(%r14)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	8(%r14), %eax
	jnb	.L1534
.L1510:
	movq	%r12, 8(%rcx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1534:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS_8internal20ZoneAllocationPolicyEE6ResizeES6_
	movl	8(%r14), %eax
	movq	(%r14), %rdi
	leal	-1(%rax), %esi
	andl	%esi, %ebx
	movl	%ebx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rcx
	movq	(%rcx), %rdx
	cmpq	%rdx, %r13
	je	.L1510
	testq	%rdx, %rdx
	jne	.L1513
	jmp	.L1510
	.p2align 4,,10
	.p2align 3
.L1535:
	cmpq	%rdx, %r13
	je	.L1510
.L1513:
	addq	$1, %rax
	andq	%rsi, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.L1535
	jmp	.L1510
	.cfi_endproc
.LFE21165:
	.size	_ZN2v88internal11VariableMap3AddEPNS0_4ZoneEPNS0_8VariableE, .-_ZN2v88internal11VariableMap3AddEPNS0_4ZoneEPNS0_8VariableE
	.section	.text._ZN2v88internal11VariableMap7DeclareEPNS0_4ZoneEPNS0_5ScopeEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagENS0_17MaybeAssignedFlagEPb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11VariableMap7DeclareEPNS0_4ZoneEPNS0_5ScopeEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagENS0_17MaybeAssignedFlagEPb
	.type	_ZN2v88internal11VariableMap7DeclareEPNS0_4ZoneEPNS0_5ScopeEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagENS0_17MaybeAssignedFlagEPb, @function
_ZN2v88internal11VariableMap7DeclareEPNS0_4ZoneEPNS0_5ScopeEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagENS0_17MaybeAssignedFlagEPb:
.LFB21163:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r9d, %r12d
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	24(%r13), %r9d
	movl	8(%rdi), %eax
	movl	16(%rbp), %ecx
	movl	24(%rbp), %r10d
	shrl	$2, %r9d
	leal	-1(%rax), %esi
	movq	(%rdi), %r11
	movl	%r9d, %eax
	andl	%esi, %eax
	jmp	.L1563
	.p2align 4,,10
	.p2align 3
.L1564:
	cmpq	%rdi, %r13
	je	.L1538
	addq	$1, %rax
	andq	%rsi, %rax
.L1563:
	leaq	(%rax,%rax,2), %rdi
	leaq	(%r11,%rdi,8), %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L1564
	movq	%r13, (%rbx)
	movq	$0, 8(%rbx)
	movl	%r9d, 16(%rbx)
	movl	12(%r14), %eax
	addl	$1, %eax
	movl	%eax, %esi
	movl	%eax, 12(%r14)
	shrl	$2, %esi
	addl	%esi, %eax
	cmpl	8(%r14), %eax
	jnb	.L1565
.L1538:
	movq	8(%rbx), %rax
	movq	32(%rbp), %rsi
	testq	%rax, %rax
	sete	(%rsi)
	je	.L1543
	movq	8(%rbx), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1543:
	.cfi_restore_state
	movq	16(%r15), %rax
	movq	24(%r15), %rsi
	subq	%rax, %rsi
	cmpq	$47, %rsi
	jbe	.L1566
	leaq	48(%rax), %rsi
	movq	%rsi, 16(%r15)
.L1546:
	movzbl	%r12b, %r12d
	movzbl	%r8b, %r8d
	movq	%r13, %xmm1
	sall	$12, %ecx
	sall	$4, %r12d
	movq	%rdx, %xmm0
	sall	$14, %r10d
	movq	$-1, 32(%rax)
	orl	%r12d, %r8d
	punpcklqdq	%xmm1, %xmm0
	orl	%r8d, %ecx
	movups	%xmm0, (%rax)
	pxor	%xmm0, %xmm0
	orl	%ecx, %r10d
	movups	%xmm0, 16(%rax)
	movw	%r10w, 40(%rax)
	movq	%rax, 8(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1565:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	%r10d, -76(%rbp)
	movl	%ecx, -72(%rbp)
	movl	%r8d, -68(%rbp)
	movq	%rdx, -64(%rbp)
	movl	%r9d, -56(%rbp)
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS_8internal20ZoneAllocationPolicyEE6ResizeES6_
	movl	8(%r14), %eax
	movl	-56(%rbp), %r9d
	movq	-64(%rbp), %rdx
	movl	-68(%rbp), %r8d
	leal	-1(%rax), %edi
	movl	-72(%rbp), %ecx
	movl	-76(%rbp), %r10d
	andl	%edi, %r9d
	movl	%r9d, %eax
	movq	(%r14), %r9
	leaq	(%rax,%rax,2), %rsi
	leaq	(%r9,%rsi,8), %rbx
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1538
	cmpq	%rsi, %r13
	jne	.L1541
	jmp	.L1538
	.p2align 4,,10
	.p2align 3
.L1567:
	testq	%rsi, %rsi
	je	.L1538
.L1541:
	addq	$1, %rax
	andq	%rdi, %rax
	leaq	(%rax,%rax,2), %rsi
	leaq	(%r9,%rsi,8), %rbx
	movq	(%rbx), %rsi
	cmpq	%rsi, %r13
	jne	.L1567
	jmp	.L1538
	.p2align 4,,10
	.p2align 3
.L1566:
	movl	$48, %esi
	movq	%r15, %rdi
	movl	%r10d, -72(%rbp)
	movl	%ecx, -68(%rbp)
	movl	%r8d, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movl	-64(%rbp), %r8d
	movl	-68(%rbp), %ecx
	movl	-72(%rbp), %r10d
	jmp	.L1546
	.cfi_endproc
.LFE21163:
	.size	_ZN2v88internal11VariableMap7DeclareEPNS0_4ZoneEPNS0_5ScopeEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagENS0_17MaybeAssignedFlagEPb, .-_ZN2v88internal11VariableMap7DeclareEPNS0_4ZoneEPNS0_5ScopeEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagENS0_17MaybeAssignedFlagEPb
	.section	.text._ZN2v88internal5ScopeC2EPNS0_4ZoneEPKNS0_12AstRawStringENS0_17MaybeAssignedFlagENS0_6HandleINS0_9ScopeInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5ScopeC2EPNS0_4ZoneEPKNS0_12AstRawStringENS0_17MaybeAssignedFlagENS0_6HandleINS0_9ScopeInfoEEE
	.type	_ZN2v88internal5ScopeC2EPNS0_4ZoneEPKNS0_12AstRawStringENS0_17MaybeAssignedFlagENS0_6HandleINS0_9ScopeInfoEEE, @function
_ZN2v88internal5ScopeC2EPNS0_4ZoneEPKNS0_12AstRawStringENS0_17MaybeAssignedFlagENS0_6HandleINS0_9ScopeInfoEEE:
.LFB21204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	32(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	24(%rsi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %rax
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	subq	%rax, %rcx
	cmpq	$191, %rcx
	jbe	.L1578
	leaq	192(%rax), %rdx
	movq	%rdx, 16(%rsi)
.L1570:
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	je	.L1579
	movl	$8, 40(%rbx)
	movq	$0, (%rax)
	cmpl	$1, 40(%rbx)
	jbe	.L1572
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L1573:
	movq	32(%rbx), %rcx
	addq	$1, %rax
	movq	$0, (%rcx,%rdx)
	movl	40(%rbx), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L1573
.L1572:
	leaq	56(%rbx), %rax
	subq	$8, %rsp
	pxor	%xmm0, %xmm0
	movzbl	%r13b, %r13d
	movq	%rax, 64(%rbx)
	leaq	72(%rbx), %rax
	xorl	%r9d, %r9d
	movq	%r15, %rcx
	movq	%rax, 80(%rbx)
	leaq	88(%rbx), %rax
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%rax, 96(%rbx)
	movl	$5, %eax
	movq	%r14, %rdi
	andb	$-4, 130(%rbx)
	movw	%ax, 128(%rbx)
	leaq	-57(%rbp), %rax
	pushq	%rax
	movq	%r8, 104(%rbx)
	movl	$2, %r8d
	pushq	%r13
	movups	%xmm0, 16(%rbx)
	movdqa	.LC1(%rip), %xmm0
	pushq	$1
	movl	$0, 44(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 72(%rbx)
	movq	$0, 88(%rbx)
	movups	%xmm0, 112(%rbx)
	call	_ZN2v88internal11VariableMap7DeclareEPNS0_4ZoneEPNS0_5ScopeEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagENS0_17MaybeAssignedFlagEPb
	addq	$32, %rsp
	cmpb	$0, -57(%rbp)
	jne	.L1580
.L1574:
	movl	124(%rbx), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 124(%rbx)
	movzwl	40(%rax), %edx
	movl	%ecx, 32(%rax)
	andw	$-897, %dx
	orw	$384, %dx
	movw	%dx, 40(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1581
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1580:
	.cfi_restore_state
	movq	64(%rbx), %rdx
	movq	%rax, (%rdx)
	leaq	24(%rax), %rdx
	movq	%rdx, 64(%rbx)
	jmp	.L1574
	.p2align 4,,10
	.p2align 3
.L1578:
	movl	$192, %esi
	movq	%r12, %rdi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %r8
	jmp	.L1570
.L1581:
	call	__stack_chk_fail@PLT
.L1579:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21204:
	.size	_ZN2v88internal5ScopeC2EPNS0_4ZoneEPKNS0_12AstRawStringENS0_17MaybeAssignedFlagENS0_6HandleINS0_9ScopeInfoEEE, .-_ZN2v88internal5ScopeC2EPNS0_4ZoneEPKNS0_12AstRawStringENS0_17MaybeAssignedFlagENS0_6HandleINS0_9ScopeInfoEEE
	.globl	_ZN2v88internal5ScopeC1EPNS0_4ZoneEPKNS0_12AstRawStringENS0_17MaybeAssignedFlagENS0_6HandleINS0_9ScopeInfoEEE
	.set	_ZN2v88internal5ScopeC1EPNS0_4ZoneEPKNS0_12AstRawStringENS0_17MaybeAssignedFlagENS0_6HandleINS0_9ScopeInfoEEE,_ZN2v88internal5ScopeC2EPNS0_4ZoneEPKNS0_12AstRawStringENS0_17MaybeAssignedFlagENS0_6HandleINS0_9ScopeInfoEEE
	.section	.text._ZN2v88internal16DeclarationScope31DeclareDefaultFunctionVariablesEPNS0_15AstValueFactoryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16DeclarationScope31DeclareDefaultFunctionVariablesEPNS0_15AstValueFactoryE
	.type	_ZN2v88internal16DeclarationScope31DeclareDefaultFunctionVariablesEPNS0_15AstValueFactoryE, @function
_ZN2v88internal16DeclarationScope31DeclareDefaultFunctionVariablesEPNS0_15AstValueFactoryE:
.LFB21237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	133(%rdi), %eax
	movq	(%rdi), %rdi
	leal	-4(%rax), %r13d
	movq	56(%rsi), %rax
	movq	24(%rdi), %rdx
	movq	472(%rax), %r14
	movq	16(%rdi), %rax
	subq	%rax, %rdx
	cmpq	$47, %rdx
	jbe	.L1595
	leaq	48(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1584:
	cmpb	$2, %r13b
	pxor	%xmm0, %xmm0
	movq	%r14, 8(%rax)
	leaq	-57(%rbp), %r14
	sbbl	%edx, %edx
	movq	%rbx, (%rax)
	subq	$8, %rsp
	leaq	32(%rbx), %r13
	andb	$-17, %dh
	movups	%xmm0, 16(%rax)
	xorl	%r9d, %r9d
	movl	$1, %r8d
	addw	$4130, %dx
	movq	$-1, 32(%rax)
	movq	%r13, %rdi
	movw	%dx, 40(%rax)
	movq	(%rbx), %rsi
	movq	%rbx, %rdx
	movq	%rax, 176(%rbx)
	movq	56(%r12), %rax
	movq	360(%rax), %rcx
	pushq	%r14
	pushq	$0
	pushq	$1
	call	_ZN2v88internal11VariableMap7DeclareEPNS0_4ZoneEPNS0_5ScopeEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagENS0_17MaybeAssignedFlagEPb
	addq	$32, %rsp
	cmpb	$0, -57(%rbp)
	je	.L1586
	movq	64(%rbx), %rdx
	movq	%rax, (%rdx)
	leaq	24(%rax), %rdx
	movq	%rdx, 64(%rbx)
.L1586:
	movq	%rax, 192(%rbx)
	movzbl	133(%rbx), %eax
	cmpb	$17, %al
	ja	.L1582
	movl	$235772, %edx
	btq	%rax, %rdx
	jnc	.L1582
	movq	56(%r12), %rax
	movq	216(%rbx), %r12
	movq	(%rbx), %r15
	movq	480(%rax), %rcx
	testq	%r12, %r12
	je	.L1596
.L1588:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movl	$1, %r8d
	movq	%rbx, %rdx
	pushq	%r14
	movq	%r15, %rsi
	movq	%r13, %rdi
	pushq	$0
	pushq	$1
	call	_ZN2v88internal11VariableMap7DeclareEPNS0_4ZoneEPNS0_5ScopeEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagENS0_17MaybeAssignedFlagEPb
	addq	$32, %rsp
	cmpb	$0, -57(%rbp)
	je	.L1591
	movq	64(%rbx), %rdx
	movq	%rax, (%rdx)
	leaq	24(%rax), %rdx
	movq	%rdx, 64(%rbx)
.L1591:
	movq	%rax, (%r12)
.L1582:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1597
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1596:
	.cfi_restore_state
	movq	16(%r15), %r12
	movq	24(%r15), %rax
	subq	%r12, %rax
	cmpq	$15, %rax
	jbe	.L1598
	leaq	16(%r12), %rax
	movq	%rax, 16(%r15)
.L1590:
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%r12)
	movq	%r12, 216(%rbx)
	jmp	.L1588
	.p2align 4,,10
	.p2align 3
.L1595:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1584
	.p2align 4,,10
	.p2align 3
.L1598:
	movl	$16, %esi
	movq	%r15, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rcx
	movq	%rax, %r12
	jmp	.L1590
.L1597:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21237:
	.size	_ZN2v88internal16DeclarationScope31DeclareDefaultFunctionVariablesEPNS0_15AstValueFactoryE, .-_ZN2v88internal16DeclarationScope31DeclareDefaultFunctionVariablesEPNS0_15AstValueFactoryE
	.section	.text._ZN2v88internal16DeclarationScope20ResetAfterPreparsingEPNS0_15AstValueFactoryEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16DeclarationScope20ResetAfterPreparsingEPNS0_15AstValueFactoryEb
	.type	_ZN2v88internal16DeclarationScope20ResetAfterPreparsingEPNS0_15AstValueFactoryEb, @function
_ZN2v88internal16DeclarationScope20ResetAfterPreparsingEPNS0_15AstValueFactoryEb:
.LFB21281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	88(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, 96(%rdi)
	leaq	56(%rdi), %rax
	movq	%rax, 64(%rdi)
	leaq	72(%rdi), %rax
	andb	$-9, 131(%rdi)
	movq	%rax, 80(%rdi)
	leaq	160(%rdi), %rax
	movq	$0, 88(%rdi)
	movq	$0, 56(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 72(%rdi)
	movq	$0, 144(%rdi)
	movq	$0, 152(%rdi)
	movq	$0, 160(%rdi)
	movq	%rax, 168(%rdi)
	movq	$0, 216(%rdi)
	movq	$0, 184(%rdi)
	movq	(%rdi), %rdi
	call	_ZN2v88internal4Zone13ReleaseMemoryEv@PLT
	testb	%r12b, %r12b
	jne	.L1614
	movq	$0, (%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
.L1607:
	movzbl	131(%rbx), %edx
	xorl	$1, %r12d
	sall	$7, %r12d
	andl	$127, %edx
	orl	%edx, %r12d
	movb	%r12b, 131(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1614:
	.cfi_restore_state
	movq	1096(%r13), %rdi
	movl	40(%rbx), %r14d
	movq	%rdi, (%rbx)
	movq	16(%rdi), %rax
	leaq	(%r14,%r14,2), %rsi
	movq	%r14, %r15
	movq	24(%rdi), %rdx
	salq	$3, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1615
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1602:
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	je	.L1616
	movl	%r15d, 40(%rbx)
	testq	%r14, %r14
	je	.L1604
	movq	$0, (%rax)
	cmpl	$1, 40(%rbx)
	movl	$24, %edx
	movl	$1, %eax
	jbe	.L1604
	.p2align 4,,10
	.p2align 3
.L1605:
	movq	32(%rbx), %rcx
	addq	$1, %rax
	movq	$0, (%rcx,%rdx)
	movl	40(%rbx), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L1605
.L1604:
	movzbl	133(%rbx), %eax
	movl	$0, 44(%rbx)
	subl	$8, %eax
	cmpb	$1, %al
	jbe	.L1607
	orb	$1, 131(%rbx)
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal16DeclarationScope31DeclareDefaultFunctionVariablesEPNS0_15AstValueFactoryE
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1615:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1602
.L1616:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21281:
	.size	_ZN2v88internal16DeclarationScope20ResetAfterPreparsingEPNS0_15AstValueFactoryEb, .-_ZN2v88internal16DeclarationScope20ResetAfterPreparsingEPNS0_15AstValueFactoryEb
	.section	.text._ZN2v88internal5Scope12DeclareLocalEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindEPbNS0_18InitializationFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope12DeclareLocalEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindEPbNS0_18InitializationFlagE
	.type	_ZN2v88internal5Scope12DeclareLocalEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindEPbNS0_18InitializationFlagE, @function
_ZN2v88internal5Scope12DeclareLocalEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindEPbNS0_18InitializationFlagE:
.LFB21247:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%r9b, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$32, %rsp
	pushq	%r8
	movl	%edx, %r8d
	movq	%rbx, %rdx
	pushq	$0
	pushq	%r9
	movzbl	%cl, %r9d
	movq	%rsi, %rcx
	movq	(%rbx), %rsi
	call	_ZN2v88internal11VariableMap7DeclareEPNS0_4ZoneEPNS0_5ScopeEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagENS0_17MaybeAssignedFlagEPb
	addq	$32, %rsp
	cmpb	$0, (%r12)
	je	.L1618
	movq	64(%rbx), %rdx
	movq	%rax, (%rdx)
	leaq	24(%rax), %rdx
	movq	%rdx, 64(%rbx)
.L1618:
	movzbl	128(%rbx), %edx
	subl	$3, %edx
	cmpb	$1, %dl
	ja	.L1617
	movzwl	40(%rax), %edx
	cmpb	$1, %r13b
	jne	.L1649
.L1621:
	orb	$8, %dh
	movw	%dx, 40(%rax)
.L1617:
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1649:
	.cfi_restore_state
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L1621
	movq	16(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1623
	testb	$64, %dh
	jne	.L1623
	movzwl	40(%rbx), %ecx
	movl	%ecx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L1623
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L1624
	testb	$64, %ch
	je	.L1650
.L1624:
	orb	$64, %ch
	movw	%cx, 40(%rbx)
	movzwl	40(%rax), %edx
	.p2align 4,,10
	.p2align 3
.L1623:
	orb	$64, %dh
	jmp	.L1621
.L1650:
	movzwl	40(%r12), %edx
	movl	%edx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L1624
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L1625
	testb	$64, %dh
	je	.L1651
.L1625:
	orb	$64, %dh
	movw	%dx, 40(%r12)
	movzwl	40(%rbx), %ecx
	jmp	.L1624
.L1651:
	movzwl	40(%r13), %ecx
	movl	%ecx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L1625
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1626
	testb	$64, %ch
	je	.L1652
.L1626:
	orb	$64, %ch
	movw	%cx, 40(%r13)
	movzwl	40(%r12), %edx
	jmp	.L1625
.L1652:
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movzwl	40(%r13), %ecx
	movq	-40(%rbp), %rax
	jmp	.L1626
	.cfi_endproc
.LFE21247:
	.size	_ZN2v88internal5Scope12DeclareLocalEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindEPbNS0_18InitializationFlagE, .-_ZN2v88internal5Scope12DeclareLocalEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindEPbNS0_18InitializationFlagE
	.section	.text._ZN2v88internal10ClassScope20DeclareBrandVariableEPNS0_15AstValueFactoryEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ClassScope20DeclareBrandVariableEPNS0_15AstValueFactoryEi
	.type	_ZN2v88internal10ClassScope20DeclareBrandVariableEPNS0_15AstValueFactoryEi, @function
_ZN2v88internal10ClassScope20DeclareBrandVariableEPNS0_15AstValueFactoryEi:
.LFB21331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	movq	%rbx, %rdx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	56(%rsi), %rax
	movq	(%rbx), %rsi
	movq	168(%rax), %rcx
	leaq	-57(%rbp), %rax
	pushq	%rax
	pushq	$1
	pushq	$0
	call	_ZN2v88internal11VariableMap7DeclareEPNS0_4ZoneEPNS0_5ScopeEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagENS0_17MaybeAssignedFlagEPb
	addq	$32, %rsp
	cmpb	$0, -57(%rbp)
	movq	%rax, %r12
	je	.L1654
	movq	64(%rbx), %rax
	movq	%r12, (%rax)
	leaq	24(%r12), %rax
	movq	%rax, 64(%rbx)
.L1654:
	orw	$3072, 40(%r12)
	movq	136(%rbx), %r15
	testq	%r15, %r15
	je	.L1666
.L1655:
	movq	%r12, 40(%r15)
	movl	%r14d, 36(%r12)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1667
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1666:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$47, %rax
	jbe	.L1668
	leaq	48(%r13), %rax
	movq	%rax, 16(%rdi)
.L1657:
	movq	(%rbx), %rdi
	movq	$0, 0(%r13)
	movq	%r13, %r15
	movq	%r13, 8(%r13)
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$191, %rdx
	jbe	.L1669
	leaq	192(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1659:
	movq	%rax, 16(%r13)
	testq	%rax, %rax
	je	.L1670
	movl	$8, 24(%r13)
	movq	$0, (%rax)
	cmpl	$1, 24(%r13)
	jbe	.L1661
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L1662:
	movq	16(%r13), %rcx
	addq	$1, %rax
	movq	$0, (%rcx,%rdx)
	movl	24(%r13), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L1662
.L1661:
	movl	$0, 28(%r13)
	movq	$0, 40(%r13)
	movq	%r13, 136(%rbx)
	jmp	.L1655
	.p2align 4,,10
	.p2align 3
.L1668:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L1657
	.p2align 4,,10
	.p2align 3
.L1669:
	movl	$192, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1659
.L1667:
	call	__stack_chk_fail@PLT
.L1670:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21331:
	.size	_ZN2v88internal10ClassScope20DeclareBrandVariableEPNS0_15AstValueFactoryEi, .-_ZN2v88internal10ClassScope20DeclareBrandVariableEPNS0_15AstValueFactoryEi
	.section	.text._ZN2v88internal16DeclarationScopeC2EPNS0_4ZoneEPNS0_15AstValueFactoryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16DeclarationScopeC2EPNS0_4ZoneEPNS0_15AstValueFactoryE
	.type	_ZN2v88internal16DeclarationScopeC2EPNS0_4ZoneEPNS0_15AstValueFactoryE, @function
_ZN2v88internal16DeclarationScopeC2EPNS0_4ZoneEPNS0_15AstValueFactoryE:
.LFB21177:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	32(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movq	16(%rsi), %rax
	movq	24(%rsi), %rdx
	subq	%rax, %rdx
	cmpq	$191, %rdx
	jbe	.L1684
	leaq	192(%rax), %rdx
	movq	%rdx, 16(%rsi)
.L1673:
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	je	.L1685
	movl	$8, 40(%rbx)
	movq	$0, (%rax)
	cmpl	$1, 40(%rbx)
	jbe	.L1675
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L1676:
	movq	32(%rbx), %rcx
	addq	$1, %rax
	movq	$0, (%rcx,%rdx)
	movl	40(%rbx), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L1676
.L1675:
	leaq	56(%rbx), %rax
	pxor	%xmm0, %xmm0
	andb	$-4, 130(%rbx)
	movq	%rax, 64(%rbx)
	leaq	72(%rbx), %rax
	movq	%rax, 80(%rbx)
	leaq	88(%rbx), %rax
	movups	%xmm0, 16(%rbx)
	movdqa	.LC1(%rip), %xmm0
	movq	%rax, 96(%rbx)
	movl	$4, %eax
	movq	$0, 56(%rbx)
	movq	$0, 72(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 104(%rbx)
	movw	%ax, 128(%rbx)
	movups	%xmm0, 112(%rbx)
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	movl	$0, 44(%rbx)
	subq	%rax, %rdx
	movb	$0, 133(%rbx)
	movl	$0, 136(%rbx)
	cmpq	$31, %rdx
	jbe	.L1686
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%r12)
	movl	$4, %edx
.L1678:
	movq	%rax, 144(%rbx)
	leaq	160(%rbx), %rax
	movq	%rax, 168(%rbx)
	movzwl	130(%rbx), %eax
	andb	$-13, 132(%rbx)
	movq	$4, 152(%rbx)
	andw	$-14082, %ax
	movq	$0, 160(%rbx)
	orw	$257, %ax
	cmpb	$3, %dl
	movw	%ax, 130(%rbx)
	sete	%al
	movq	$0, 216(%rbx)
	cmpb	$2, %dl
	jne	.L1680
	movzbl	133(%rbx), %eax
	subl	$8, %eax
	cmpb	$1, %al
	seta	%al
.L1680:
	movzwl	131(%rbx), %ecx
	movzbl	%al, %eax
	movq	(%rbx), %rsi
	movq	%rbx, %rdx
	sall	$12, %eax
	pxor	%xmm0, %xmm0
	subq	$8, %rsp
	movq	%r13, %rdi
	andw	$-4553, %cx
	movups	%xmm0, 176(%rbx)
	movl	$2, %r9d
	movl	$5, %r8d
	orl	%ecx, %eax
	movups	%xmm0, 192(%rbx)
	movw	%ax, 131(%rbx)
	movq	56(%r14), %rax
	movq	$0, 208(%rbx)
	movq	472(%rax), %rcx
	leaq	-41(%rbp), %rax
	pushq	%rax
	pushq	$0
	pushq	$1
	call	_ZN2v88internal11VariableMap7DeclareEPNS0_4ZoneEPNS0_5ScopeEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagENS0_17MaybeAssignedFlagEPb
	addq	$32, %rsp
	movq	%rax, 176(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1687
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1686:
	.cfi_restore_state
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	128(%rbx), %edx
	jmp	.L1678
	.p2align 4,,10
	.p2align 3
.L1684:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1673
.L1685:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L1687:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21177:
	.size	_ZN2v88internal16DeclarationScopeC2EPNS0_4ZoneEPNS0_15AstValueFactoryE, .-_ZN2v88internal16DeclarationScopeC2EPNS0_4ZoneEPNS0_15AstValueFactoryE
	.globl	_ZN2v88internal16DeclarationScopeC1EPNS0_4ZoneEPNS0_15AstValueFactoryE
	.set	_ZN2v88internal16DeclarationScopeC1EPNS0_4ZoneEPNS0_15AstValueFactoryE,_ZN2v88internal16DeclarationScopeC2EPNS0_4ZoneEPNS0_15AstValueFactoryE
	.section	.text._ZN2v88internal10ClassScope18DeclarePrivateNameEPKNS0_12AstRawStringENS0_12VariableModeEPb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ClassScope18DeclarePrivateNameEPKNS0_12AstRawStringENS0_12VariableModeEPb
	.type	_ZN2v88internal10ClassScope18DeclarePrivateNameEPKNS0_12AstRawStringENS0_12VariableModeEPb, @function
_ZN2v88internal10ClassScope18DeclarePrivateNameEPKNS0_12AstRawStringENS0_12VariableModeEPb:
.LFB21321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	136(%rdi), %r12
	movq	(%rdi), %r11
	testq	%r12, %r12
	je	.L1705
.L1689:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	leaq	16(%r12), %rdi
	movl	%r14d, %r8d
	pushq	%r13
	movq	%r10, %rcx
	movq	%rbx, %rdx
	movq	%r11, %rsi
	pushq	$1
	pushq	$0
	call	_ZN2v88internal11VariableMap7DeclareEPNS0_4ZoneEPNS0_5ScopeEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagENS0_17MaybeAssignedFlagEPb
	addq	$32, %rsp
	cmpb	$0, 0(%r13)
	jne	.L1706
	movzwl	40(%rax), %edx
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$8, %cl
	je	.L1699
	cmpb	$9, %cl
	jne	.L1698
	cmpb	$8, %r14b
	sete	%cl
	testb	%cl, %cl
	je	.L1698
.L1710:
	movb	$1, 0(%r13)
	movzwl	40(%rax), %edx
	andl	$-16, %edx
	orl	$10, %edx
	jmp	.L1698
	.p2align 4,,10
	.p2align 3
.L1706:
	movq	64(%rbx), %rdx
	movq	%rax, (%rdx)
	leaq	24(%rax), %rdx
	movq	%rdx, 64(%rbx)
	movzwl	40(%rax), %edx
.L1698:
	orb	$4, %dh
	movw	%dx, 40(%rax)
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1705:
	.cfi_restore_state
	movq	16(%r11), %r15
	movq	24(%r11), %rax
	subq	%r15, %rax
	cmpq	$47, %rax
	jbe	.L1707
	leaq	48(%r15), %rax
	movq	%rax, 16(%r11)
.L1691:
	movq	(%rbx), %rdi
	movq	$0, (%r15)
	movq	%r15, %r12
	movq	%r15, 8(%r15)
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$191, %rdx
	jbe	.L1708
	leaq	192(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1693:
	movq	%rax, 16(%r15)
	testq	%rax, %rax
	je	.L1709
	movl	$8, 24(%r15)
	movq	$0, (%rax)
	cmpl	$1, 24(%r15)
	jbe	.L1695
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L1696:
	movq	16(%r15), %rcx
	addq	$1, %rax
	movq	$0, (%rcx,%rdx)
	movl	24(%r15), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L1696
.L1695:
	movl	$0, 28(%r15)
	movq	$0, 40(%r15)
	movq	(%rbx), %r11
	movq	%r15, 136(%rbx)
	jmp	.L1689
	.p2align 4,,10
	.p2align 3
.L1699:
	cmpb	$9, %r14b
	sete	%cl
	testb	%cl, %cl
	je	.L1698
	jmp	.L1710
	.p2align 4,,10
	.p2align 3
.L1707:
	movq	%rsi, -56(%rbp)
	movq	%r11, %rdi
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r10
	movq	%rax, %r15
	jmp	.L1691
	.p2align 4,,10
	.p2align 3
.L1708:
	movl	$192, %esi
	movq	%r10, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r10
	jmp	.L1693
.L1709:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21321:
	.size	_ZN2v88internal10ClassScope18DeclarePrivateNameEPKNS0_12AstRawStringENS0_12VariableModeEPb, .-_ZN2v88internal10ClassScope18DeclarePrivateNameEPKNS0_12AstRawStringENS0_12VariableModeEPb
	.section	.text._ZN2v88internal10ClassScope17LookupPrivateNameEPNS0_13VariableProxyE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ClassScope17LookupPrivateNameEPNS0_13VariableProxyE
	.type	_ZN2v88internal10ClassScope17LookupPrivateNameEPNS0_13VariableProxyE, @function
_ZN2v88internal10ClassScope17LookupPrivateNameEPNS0_13VariableProxyE:
.LFB21328:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-58(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	128(%rdi), %eax
	cmpb	$4, %al
	je	.L1721
	.p2align 4,,10
	.p2align 3
.L1712:
	testb	%al, %al
	jne	.L1714
	movq	8(%rbx), %r13
	testb	$1, 5(%rbx)
	je	.L1715
	movq	8(%r13), %r13
.L1715:
	movq	136(%r12), %rax
	testq	%rax, %rax
	je	.L1716
	movl	24(%rax), %edx
	movq	16(%rax), %rdi
	movl	24(%r13), %eax
	subl	$1, %edx
	shrl	$2, %eax
	andl	%edx, %eax
	jmp	.L1744
	.p2align 4,,10
	.p2align 3
.L1745:
	cmpq	%r13, %rcx
	je	.L1718
	addq	$1, %rax
	andq	%rdx, %rax
.L1744:
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rdi,%rcx,8), %rsi
	movq	(%rsi), %rcx
	testq	%rcx, %rcx
	jne	.L1745
.L1716:
	movq	104(%r12), %rax
	testq	%rax, %rax
	je	.L1714
	movq	0(%r13), %rdx
	movq	(%rax), %rdi
	leaq	-59(%rbp), %rcx
	movq	%r14, %r8
	movq	(%rdx), %rsi
	leaq	-60(%rbp), %rdx
	call	_ZN2v88internal9ScopeInfo16ContextSlotIndexES1_NS0_6StringEPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	js	.L1714
	movzbl	-60(%rbp), %edx
	leaq	-57(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10ClassScope18DeclarePrivateNameEPKNS0_12AstRawStringENS0_12VariableModeEPb
	movzwl	40(%rax), %edx
	movl	%r15d, 32(%rax)
	andw	$-897, %dx
	orw	$384, %dx
	movw	%dx, 40(%rax)
	.p2align 4,,10
	.p2align 3
.L1711:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1746
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1714:
	.cfi_restore_state
	movq	8(%r12), %r12
	movzbl	128(%r12), %eax
	cmpb	$4, %al
	jne	.L1712
.L1721:
	xorl	%eax, %eax
	jmp	.L1711
	.p2align 4,,10
	.p2align 3
.L1718:
	movq	8(%rsi), %rax
	testq	%rax, %rax
	jne	.L1711
	jmp	.L1716
.L1746:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21328:
	.size	_ZN2v88internal10ClassScope17LookupPrivateNameEPNS0_13VariableProxyE, .-_ZN2v88internal10ClassScope17LookupPrivateNameEPNS0_13VariableProxyE
	.section	.text._ZN2v88internal10ClassScope19ResolvePrivateNamesEPNS0_9ParseInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ClassScope19ResolvePrivateNamesEPNS0_9ParseInfoE
	.type	_ZN2v88internal10ClassScope19ResolvePrivateNamesEPNS0_9ParseInfoE, @function
_ZN2v88internal10ClassScope19ResolvePrivateNamesEPNS0_9ParseInfoE:
.LFB21329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	136(%rdi), %r14
	movq	%rsi, -56(%rbp)
	testq	%r14, %r14
	je	.L1758
	movq	(%r14), %r12
	movl	$1, %eax
	testq	%r12, %r12
	je	.L1747
	movq	%rdi, %rbx
	movq	%r14, %r15
	.p2align 4,,10
	.p2align 3
.L1750:
	testb	$2, 5(%r12)
	je	.L1749
	leaq	16(%r12), %r15
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L1750
.L1749:
	movq	8(%r14), %r13
	cmpq	%r15, %r13
	je	.L1757
	.p2align 4,,10
	.p2align 3
.L1751:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal10ClassScope17LookupPrivateNameEPNS0_13VariableProxyE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1778
	orw	$2048, 40(%rax)
	movq	%r12, %rdi
	call	_ZN2v88internal13VariableProxy6BindToEPNS0_8VariableE@PLT
	movq	(%r15), %rax
	movq	16(%rax), %r12
	leaq	16(%rax), %r15
	testq	%r12, %r12
	jne	.L1756
	jmp	.L1755
	.p2align 4,,10
	.p2align 3
.L1779:
	leaq	16(%r12), %r15
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L1755
.L1756:
	testb	$2, 5(%r12)
	jne	.L1779
.L1755:
	cmpq	%r13, %r15
	jne	.L1751
.L1757:
	movq	$0, (%r14)
	movl	$1, %eax
	movq	%r14, 8(%r14)
.L1747:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1778:
	.cfi_restore_state
	movl	(%r12), %esi
	movq	8(%r12), %r8
	testb	$1, 5(%r12)
	je	.L1753
	movq	8(%r8), %r8
.L1753:
	movq	16(%r8), %rax
	cmpb	$0, 28(%r8)
	movl	%eax, %edx
	je	.L1780
.L1754:
	movq	-56(%rbp), %rdi
	addl	%esi, %edx
	movl	$258, %ecx
	addq	$176, %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKNS0_12AstRawStringE@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1780:
	.cfi_restore_state
	shrl	$31, %edx
	addl	%eax, %edx
	sarl	%edx
	jmp	.L1754
	.p2align 4,,10
	.p2align 3
.L1758:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21329:
	.size	_ZN2v88internal10ClassScope19ResolvePrivateNamesEPNS0_9ParseInfoE, .-_ZN2v88internal10ClassScope19ResolvePrivateNamesEPNS0_9ParseInfoE
	.section	.text._ZN2v88internal5Scope19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPbNS0_12VariableKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPbNS0_12VariableKindE
	.type	_ZN2v88internal5Scope19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPbNS0_12VariableKindE, @function
_ZN2v88internal5Scope19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPbNS0_12VariableKindE:
.LFB21249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	cmpb	$2, %dl
	jne	.L1782
	testb	$1, 130(%rdi)
	jne	.L1782
	.p2align 4,,10
	.p2align 3
.L1783:
	movq	8(%r12), %r12
	testb	$1, 130(%r12)
	je	.L1783
	leaq	-40(%rbp), %rsp
	movzbl	%r14b, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	popq	%rbx
	movl	$2, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal5Scope19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPbNS0_12VariableKindE
	.p2align 4,,10
	.p2align 3
.L1782:
	.cfi_restore_state
	subq	$8, %rsp
	movq	%rsi, %rcx
	leaq	32(%r12), %rdi
	movzbl	%r14b, %r9d
	pushq	%r13
	movl	%ebx, %r8d
	movq	%r12, %rdx
	pushq	$0
	pushq	$1
	movq	(%r12), %rsi
	call	_ZN2v88internal11VariableMap7DeclareEPNS0_4ZoneEPNS0_5ScopeEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagENS0_17MaybeAssignedFlagEPb
	addq	$32, %rsp
	cmpb	$0, 0(%r13)
	jne	.L1853
	movzbl	128(%r12), %ecx
	movzwl	40(%rax), %edx
	subl	$3, %ecx
	cmpb	$1, %cl
	ja	.L1794
.L1804:
	cmpb	$1, %bl
	je	.L1788
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L1788
	movq	16(%rax), %r12
	testq	%r12, %r12
	je	.L1790
	testb	$64, %dh
	je	.L1854
.L1790:
	orb	$64, %dh
.L1788:
	orb	$8, %dh
	movw	%dx, 40(%rax)
	cmpb	$0, 0(%r13)
	je	.L1794
.L1795:
	orb	$8, %dh
	movw	%dx, 40(%rax)
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1853:
	.cfi_restore_state
	movq	64(%r12), %rdx
	movq	%rax, (%rdx)
	leaq	24(%rax), %rdx
	movq	%rdx, 64(%r12)
	movzbl	128(%r12), %edx
	subl	$3, %edx
	cmpb	$1, %dl
	movzwl	40(%rax), %edx
	jbe	.L1804
	cmpb	$0, 0(%r13)
	jne	.L1795
	.p2align 4,,10
	.p2align 3
.L1794:
	cmpb	$1, %bl
	jbe	.L1796
	movl	%edx, %ecx
	testb	$14, %dl
	jne	.L1797
.L1796:
	movl	%edx, %ecx
	shrb	$4, %cl
	andl	$7, %ecx
	cmpb	$3, %cl
	jne	.L1805
	cmpb	$3, %r14b
	jne	.L1805
	movl	%edx, %ecx
.L1797:
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L1795
	movq	16(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1800
	testb	$64, %dh
	je	.L1855
.L1800:
	orb	$64, %dh
	jmp	.L1795
	.p2align 4,,10
	.p2align 3
.L1805:
	leaq	-40(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1855:
	.cfi_restore_state
	movzwl	40(%rbx), %ecx
	movl	%ecx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L1800
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L1801
	testb	$64, %ch
	je	.L1856
.L1801:
	orb	$64, %ch
	movw	%cx, 40(%rbx)
	movzwl	40(%rax), %edx
	jmp	.L1800
	.p2align 4,,10
	.p2align 3
.L1854:
	movzwl	40(%r12), %ecx
	movl	%ecx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L1790
	movq	16(%r12), %r15
	testq	%r15, %r15
	je	.L1791
	testb	$64, %ch
	je	.L1857
.L1791:
	orb	$64, %ch
	movw	%cx, 40(%r12)
	movzwl	40(%rax), %edx
	jmp	.L1790
.L1856:
	movzwl	40(%r12), %edx
	movl	%edx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L1801
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L1802
	testb	$64, %dh
	je	.L1858
.L1802:
	orb	$64, %dh
	movw	%dx, 40(%r12)
	movzwl	40(%rbx), %ecx
	jmp	.L1801
.L1857:
	movzwl	40(%r15), %edx
	movl	%edx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L1791
	movq	16(%r15), %rcx
	testq	%rcx, %rcx
	je	.L1792
	testb	$64, %dh
	je	.L1859
.L1792:
	orb	$64, %dh
	movw	%dx, 40(%r15)
	movzwl	40(%r12), %ecx
	jmp	.L1791
.L1858:
	movzwl	40(%r13), %ecx
	movl	%ecx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L1802
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1803
	testb	$64, %ch
	je	.L1860
.L1803:
	orb	$64, %ch
	movw	%cx, 40(%r13)
	movzwl	40(%r12), %edx
	jmp	.L1802
.L1859:
	movzwl	40(%rcx), %esi
	movl	%esi, %edi
	andl	$15, %edi
	cmpb	$1, %dil
	je	.L1792
	movq	16(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L1793
	testw	$16384, %si
	je	.L1861
.L1793:
	orw	$16384, %si
	movw	%si, 40(%rcx)
	movzwl	40(%r15), %edx
	jmp	.L1792
.L1860:
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movzwl	40(%r13), %ecx
	movq	-56(%rbp), %rax
	jmp	.L1803
.L1861:
	movq	%rax, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rax
	movzwl	40(%rcx), %esi
	jmp	.L1793
	.cfi_endproc
.LFE21249:
	.size	_ZN2v88internal5Scope19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPbNS0_12VariableKindE, .-_ZN2v88internal5Scope19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPbNS0_12VariableKindE
	.section	.text._ZN2v88internal16DeclarationScope18DeclareFunctionVarEPKNS0_12AstRawStringEPNS0_5ScopeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16DeclarationScope18DeclareFunctionVarEPKNS0_12AstRawStringEPNS0_5ScopeE
	.type	_ZN2v88internal16DeclarationScope18DeclareFunctionVarEPKNS0_12AstRawStringEPNS0_5ScopeE, @function
_ZN2v88internal16DeclarationScope18DeclareFunctionVarEPKNS0_12AstRawStringEPNS0_5ScopeE:
.LFB21238:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%rsi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movzbl	129(%rdi), %eax
	cmove	%rdi, %r12
	movq	(%rdi), %rdi
	andl	$1, %eax
	cmpb	$1, %al
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	sbbl	%r14d, %r14d
	andl	$64, %r14d
	subq	%r13, %rax
	addw	$4097, %r14w
	cmpq	$47, %rax
	jbe	.L1896
	leaq	48(%r13), %rax
	movq	%rax, 16(%rdi)
.L1866:
	movq	%rbx, %xmm0
	movw	%r14w, 40(%r13)
	leaq	32(%r12), %rdi
	movhps	-72(%rbp), %xmm0
	movq	$-1, 32(%r13)
	movups	%xmm0, 0(%r13)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 16(%r13)
	movq	%r13, 184(%rbx)
	testb	$4, 129(%rbx)
	je	.L1867
	subq	$8, %rsp
	leaq	-57(%rbp), %rax
	movq	-72(%rbp), %rcx
	movq	%r12, %rdx
	pushq	%rax
	xorl	%r9d, %r9d
	movl	$4, %r8d
	pushq	$0
	pushq	$1
	movq	(%r12), %rsi
	call	_ZN2v88internal11VariableMap7DeclareEPNS0_4ZoneEPNS0_5ScopeEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagENS0_17MaybeAssignedFlagEPb
	addq	$32, %rsp
	movzwl	40(%rax), %edx
	movl	$-1, 32(%rax)
	andw	$-897, %dx
	orb	$2, %dh
	movw	%dx, 40(%rax)
.L1868:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	movq	184(%rbx), %rax
	jne	.L1897
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1867:
	.cfi_restore_state
	movq	8(%r13), %r15
	movl	40(%r12), %eax
	movq	32(%r12), %r8
	movl	24(%r15), %r14d
	leal	-1(%rax), %esi
	shrl	$2, %r14d
	movl	%r14d, %eax
	andl	%esi, %eax
	jmp	.L1895
	.p2align 4,,10
	.p2align 3
.L1898:
	cmpq	%r15, %rcx
	je	.L1870
	addq	$1, %rax
	andq	%rsi, %rax
.L1895:
	leaq	(%rax,%rax,2), %rdx
	leaq	(%r8,%rdx,8), %rdx
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.L1898
	movq	(%rbx), %rsi
	movq	%r15, (%rdx)
	movq	$0, 8(%rdx)
	movl	%r14d, 16(%rdx)
	movl	44(%r12), %eax
	addl	$1, %eax
	movl	%eax, %ecx
	movl	%eax, 44(%r12)
	shrl	$2, %ecx
	addl	%ecx, %eax
	cmpl	40(%r12), %eax
	jnb	.L1899
.L1870:
	movq	%r13, 8(%rdx)
	jmp	.L1868
	.p2align 4,,10
	.p2align 3
.L1896:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L1866
	.p2align 4,,10
	.p2align 3
.L1899:
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS_8internal20ZoneAllocationPolicyEE6ResizeES6_
	movl	40(%r12), %eax
	movq	32(%r12), %rsi
	leal	-1(%rax), %ecx
	andl	%ecx, %r14d
	leaq	(%r14,%r14,2), %rax
	leaq	(%rsi,%rax,8), %rdx
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L1870
	cmpq	%r15, %rax
	jne	.L1873
	jmp	.L1870
	.p2align 4,,10
	.p2align 3
.L1900:
	testq	%rax, %rax
	je	.L1870
.L1873:
	addq	$1, %r14
	andq	%rcx, %r14
	leaq	(%r14,%r14,2), %rax
	leaq	(%rsi,%rax,8), %rdx
	movq	(%rdx), %rax
	cmpq	%rax, %r15
	jne	.L1900
	jmp	.L1870
.L1897:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21238:
	.size	_ZN2v88internal16DeclarationScope18DeclareFunctionVarEPKNS0_12AstRawStringEPNS0_5ScopeE, .-_ZN2v88internal16DeclarationScope18DeclareFunctionVarEPKNS0_12AstRawStringEPNS0_5ScopeE
	.section	.text._ZN2v88internal5Scope17LookupInScopeInfoEPKNS0_12AstRawStringEPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope17LookupInScopeInfoEPKNS0_12AstRawStringEPS1_
	.type	_ZN2v88internal5Scope17LookupInScopeInfoEPKNS0_12AstRawStringEPS1_, @function
_ZN2v88internal5Scope17LookupInScopeInfoEPKNS0_12AstRawStringEPS1_:
.LFB21244:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-65(%rbp), %r8
	leaq	-66(%rbp), %rcx
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	leaq	-67(%rbp), %rdx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%r8, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movq	(%rax), %r15
	movq	104(%rdi), %rax
	movq	%rdx, -88(%rbp)
	movq	(%rax), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal9ScopeInfo16ContextSlotIndexES1_NS0_6StringEPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE@PLT
	leaq	-64(%rbp), %r9
	movl	$384, %r10d
	movl	%eax, %r14d
	testl	%eax, %eax
	js	.L1920
.L1902:
	movzbl	-65(%rbp), %eax
	subq	$8, %rsp
	movzbl	-67(%rbp), %r8d
	movq	%r13, %rdx
	pushq	%r9
	movq	0(%r13), %rsi
	leaq	32(%rbx), %rdi
	xorl	%r9d, %r9d
	pushq	%rax
	movzbl	-66(%rbp), %eax
	movq	%r12, %rcx
	movl	%r10d, -88(%rbp)
	pushq	%rax
	call	_ZN2v88internal11VariableMap7DeclareEPNS0_4ZoneEPNS0_5ScopeEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagENS0_17MaybeAssignedFlagEPb
	movl	-88(%rbp), %r10d
	addq	$32, %rsp
	movzwl	40(%rax), %edx
	movl	%r14d, 32(%rax)
	andw	$-897, %dx
	orl	%r10d, %edx
	movw	%dx, 40(%rax)
.L1901:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1921
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1920:
	.cfi_restore_state
	movq	104(%r13), %rax
	cmpb	$3, 128(%r13)
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rcx
	movq	(%rax), %rax
	movq	-104(%rbp), %r8
	je	.L1922
.L1903:
	movq	%r15, %rsi
	movq	%r9, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo24FunctionContextSlotIndexENS0_6StringE@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	js	.L1906
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal16DeclarationScope18DeclareFunctionVarEPKNS0_12AstRawStringEPNS0_5ScopeE
	movzwl	40(%rax), %edx
	movl	%r14d, 32(%rax)
	andw	$-897, %dx
	orw	$384, %dx
	movw	%dx, 40(%rax)
	movl	40(%rbx), %esi
	movl	24(%r12), %eax
	movq	32(%rbx), %rdi
	subl	$1, %esi
	shrl	$2, %eax
	andl	%esi, %eax
	jmp	.L1919
	.p2align 4,,10
	.p2align 3
.L1923:
	cmpq	%rdx, %r12
	je	.L1907
	addq	$1, %rax
	andq	%rsi, %rax
.L1919:
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.L1923
.L1906:
	xorl	%eax, %eax
	jmp	.L1901
	.p2align 4,,10
	.p2align 3
.L1907:
	movq	8(%rcx), %rax
	jmp	.L1901
	.p2align 4,,10
	.p2align 3
.L1922:
	movq	%r9, %rdi
	movq	%r15, %rsi
	movq	%r9, -88(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal9ScopeInfo11ModuleIndexENS0_6StringEPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE@PLT
	movq	-88(%rbp), %r9
	testl	%eax, %eax
	movl	%eax, %r14d
	jne	.L1911
	movq	104(%r13), %rax
	movq	(%rax), %rax
	jmp	.L1903
.L1921:
	call	__stack_chk_fail@PLT
.L1911:
	movl	$640, %r10d
	jmp	.L1902
	.cfi_endproc
.LFE21244:
	.size	_ZN2v88internal5Scope17LookupInScopeInfoEPKNS0_12AstRawStringEPS1_, .-_ZN2v88internal5Scope17LookupInScopeInfoEPKNS0_12AstRawStringEPS1_
	.section	.text._ZN2v88internal10ClassScopeC2EPNS0_4ZoneEPNS0_15AstValueFactoryENS0_6HandleINS0_9ScopeInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ClassScopeC2EPNS0_4ZoneEPNS0_15AstValueFactoryENS0_6HandleINS0_9ScopeInfoEEE
	.type	_ZN2v88internal10ClassScopeC2EPNS0_4ZoneEPNS0_15AstValueFactoryENS0_6HandleINS0_9ScopeInfoEEE, @function
_ZN2v88internal10ClassScopeC2EPNS0_4ZoneEPNS0_15AstValueFactoryENS0_6HandleINS0_9ScopeInfoEEE:
.LFB21195:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, (%rbx)
	movq	$0, 8(%rbx)
	movq	16(%rsi), %rax
	movq	24(%rsi), %rdx
	subq	%rax, %rdx
	cmpq	$191, %rdx
	jbe	.L1949
	leaq	192(%rax), %rdx
	movq	%rdx, 16(%rsi)
.L1926:
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	je	.L1936
	movl	$8, 40(%rbx)
	movq	$0, (%rax)
	cmpl	$1, 40(%rbx)
	jbe	.L1928
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L1929:
	movq	32(%rbx), %rcx
	addq	$1, %rax
	movq	$0, (%rcx,%rdx)
	movl	40(%rbx), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L1929
.L1928:
	leaq	56(%rbx), %rax
	pxor	%xmm0, %xmm0
	andb	$-4, 130(%rbx)
	movq	%rax, 64(%rbx)
	leaq	72(%rbx), %rax
	leaq	-48(%rbp), %r14
	movq	%rax, 80(%rbx)
	leaq	88(%rbx), %rax
	movq	%r14, %rdi
	movq	%rax, 96(%rbx)
	xorl	%eax, %eax
	movl	$0, 44(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 72(%rbx)
	movq	$0, 88(%rbx)
	movw	%ax, 128(%rbx)
	movq	(%r12), %rax
	movups	%xmm0, 16(%rbx)
	movdqa	.LC1(%rip), %xmm0
	movq	%r12, 104(%rbx)
	movups	%xmm0, 112(%rbx)
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal9ScopeInfo13language_modeEv@PLT
	movq	%r14, %rdi
	andl	$1, %eax
	movl	%eax, %edx
	movzbl	129(%rbx), %eax
	andl	$-2, %eax
	orl	%edx, %eax
	movb	%al, 129(%rbx)
	movq	(%r12), %rax
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal9ScopeInfo13ContextLengthEv@PLT
	movq	%r14, %rdi
	orw	$513, 129(%rbx)
	movl	%eax, 124(%rbx)
	movq	(%r12), %rax
	movq	$0, 136(%rbx)
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal9ScopeInfo13HasClassBrandEv@PLT
	testb	%al, %al
	jne	.L1950
.L1924:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1951
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1950:
	.cfi_restore_state
	movq	56(%r13), %rax
	movq	%rbx, %rdx
	movq	%rbx, %rdi
	movq	168(%rax), %rsi
	call	_ZN2v88internal5Scope17LookupInScopeInfoEPKNS0_12AstRawStringEPS1_
	movq	136(%rbx), %r14
	movq	%rax, %r13
	testq	%r14, %r14
	je	.L1952
.L1931:
	movq	%r13, 40(%r14)
	jmp	.L1924
	.p2align 4,,10
	.p2align 3
.L1949:
	movl	$192, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1926
	.p2align 4,,10
	.p2align 3
.L1952:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$47, %rax
	jbe	.L1953
	leaq	48(%r12), %rax
	movq	%rax, 16(%rdi)
.L1933:
	movq	(%rbx), %rdi
	movq	$0, (%r12)
	movq	%r12, %r14
	movq	%r12, 8(%r12)
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$191, %rdx
	jbe	.L1954
	leaq	192(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1935:
	movq	%rax, 16(%r12)
	testq	%rax, %rax
	je	.L1936
	movl	$8, 24(%r12)
	movq	$0, (%rax)
	cmpl	$1, 24(%r12)
	jbe	.L1937
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L1938:
	movq	16(%r12), %rcx
	addq	$1, %rax
	movq	$0, (%rcx,%rdx)
	movl	24(%r12), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L1938
.L1937:
	movl	$0, 28(%r12)
	movq	$0, 40(%r12)
	movq	%r12, 136(%rbx)
	jmp	.L1931
.L1953:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L1933
.L1954:
	movl	$192, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1935
.L1936:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1951:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21195:
	.size	_ZN2v88internal10ClassScopeC2EPNS0_4ZoneEPNS0_15AstValueFactoryENS0_6HandleINS0_9ScopeInfoEEE, .-_ZN2v88internal10ClassScopeC2EPNS0_4ZoneEPNS0_15AstValueFactoryENS0_6HandleINS0_9ScopeInfoEEE
	.globl	_ZN2v88internal10ClassScopeC1EPNS0_4ZoneEPNS0_15AstValueFactoryENS0_6HandleINS0_9ScopeInfoEEE
	.set	_ZN2v88internal10ClassScopeC1EPNS0_4ZoneEPNS0_15AstValueFactoryENS0_6HandleINS0_9ScopeInfoEEE,_ZN2v88internal10ClassScopeC2EPNS0_4ZoneEPNS0_15AstValueFactoryENS0_6HandleINS0_9ScopeInfoEEE
	.section	.text._ZN2v88internal5Scope21DeserializeScopeChainEPNS0_7IsolateEPNS0_4ZoneENS0_9ScopeInfoEPNS0_16DeclarationScopeEPNS0_15AstValueFactoryENS1_19DeserializationModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope21DeserializeScopeChainEPNS0_7IsolateEPNS0_4ZoneENS0_9ScopeInfoEPNS0_16DeclarationScopeEPNS0_15AstValueFactoryENS1_19DeserializationModeE
	.type	_ZN2v88internal5Scope21DeserializeScopeChainEPNS0_7IsolateEPNS0_4ZoneENS0_9ScopeInfoEPNS0_16DeclarationScopeEPNS0_15AstValueFactoryENS1_19DeserializationModeE, @function
_ZN2v88internal5Scope21DeserializeScopeChainEPNS0_7IsolateEPNS0_4ZoneENS0_9ScopeInfoEPNS0_16DeclarationScopeEPNS0_15AstValueFactoryENS1_19DeserializationModeE:
.LFB21213:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r8, -88(%rbp)
	movl	%r9d, -72(%rbp)
	testq	%rdx, %rdx
	je	.L1956
	leaq	-56(%rbp), %rbx
	movq	%rsi, %r12
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movq	%rbx, %rdi
	call	_ZNK2v88internal9ScopeInfo10scope_typeEv@PLT
	movq	%rbx, %rdi
	cmpb	$7, %al
	je	.L2038
	.p2align 4,,10
	.p2align 3
.L1957:
	call	_ZNK2v88internal9ScopeInfo10scope_typeEv@PLT
	cmpb	$4, %al
	je	.L2039
	movq	%rbx, %rdi
	call	_ZNK2v88internal9ScopeInfo10scope_typeEv@PLT
	cmpb	$2, %al
	jne	.L1976
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rsi
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1977
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L1978:
	movq	16(%r12), %r9
	movq	24(%r12), %rax
	subq	%r9, %rax
	cmpq	$223, %rax
	jbe	.L2040
	leaq	224(%r9), %rax
	movq	%rax, 16(%r12)
.L1981:
	movl	$2, %edx
	movq	%r12, %rsi
	movq	%r9, %rdi
	movq	%r9, %r15
	call	_ZN2v88internal16DeclarationScopeC1EPNS0_4ZoneENS0_9ScopeTypeENS0_6HandleINS0_9ScopeInfoEEE
	movq	-56(%rbp), %rax
	movl	11(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L1964
	movq	15(%rax), %rax
	btq	$46, %rax
	jnc	.L1964
	orb	$2, 131(%r15)
	.p2align 4,,10
	.p2align 3
.L1964:
	cmpl	$1, -72(%rbp)
	jne	.L2020
	movq	$0, 104(%r15)
.L2020:
	testq	%r13, %r13
	je	.L2021
	movq	16(%r15), %rax
	movq	%rax, 24(%r13)
	movq	%r13, 16(%r15)
	movq	%r15, 8(%r13)
.L2021:
	testq	%r14, %r14
	movq	%rbx, %rdi
	cmove	%r15, %r14
	call	_ZNK2v88internal9ScopeInfo17HasOuterScopeInfoEv@PLT
	testb	%al, %al
	jne	.L2041
.L2025:
	movl	-72(%rbp), %edx
	testl	%edx, %edx
	je	.L2042
.L1971:
	testq	%r14, %r14
	je	.L2028
.L2026:
	movq	-80(%rbp), %rdx
	movq	16(%rdx), %rax
	movq	%rax, 24(%r15)
	movq	%r15, 16(%rdx)
	movq	%rdx, 8(%r15)
.L1955:
	addq	$72, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2041:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZNK2v88internal9ScopeInfo14OuterScopeInfoEv@PLT
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L2025
	movq	%rbx, %rdi
	movq	%r15, %r13
	call	_ZNK2v88internal9ScopeInfo10scope_typeEv@PLT
	movq	%rbx, %rdi
	cmpb	$7, %al
	jne	.L1957
.L2038:
	call	_ZNK2v88internal9ScopeInfo20IsDebugEvaluateScopeEv@PLT
	movq	-56(%rbp), %rsi
	testb	%al, %al
	movq	-64(%rbp), %rax
	movq	41112(%rax), %rdi
	je	.L1958
	testq	%rdi, %rdi
	je	.L1959
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L1960:
	movq	16(%r12), %r9
	movq	24(%r12), %rax
	subq	%r9, %rax
	cmpq	$223, %rax
	jbe	.L2043
	leaq	224(%r9), %rax
	movq	%rax, 16(%r12)
.L1963:
	movq	%r9, %r15
	movl	$2, %edx
	movq	%r12, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal16DeclarationScopeC1EPNS0_4ZoneENS0_9ScopeTypeENS0_6HandleINS0_9ScopeInfoEEE
	orb	$32, 129(%r15)
	jmp	.L1964
	.p2align 4,,10
	.p2align 3
.L1976:
	movq	%rbx, %rdi
	call	_ZNK2v88internal9ScopeInfo10scope_typeEv@PLT
	cmpb	$1, %al
	jne	.L1982
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rsi
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1983
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L1984:
	movq	16(%r12), %r9
	movq	24(%r12), %rax
	subq	%r9, %rax
	cmpq	$223, %rax
	jbe	.L2044
	leaq	224(%r9), %rax
	movq	%rax, 16(%r12)
.L1987:
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r9, %rdi
	movq	%r9, %r15
	call	_ZN2v88internal16DeclarationScopeC1EPNS0_4ZoneENS0_9ScopeTypeENS0_6HandleINS0_9ScopeInfoEEE
	jmp	.L1964
	.p2align 4,,10
	.p2align 3
.L1958:
	testq	%rdi, %rdi
	je	.L1965
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L1966:
	movq	16(%r12), %r9
	movq	24(%r12), %rax
	subq	%r9, %rax
	cmpq	$135, %rax
	jbe	.L2045
	leaq	136(%r9), %rax
	movq	%rax, 16(%r12)
.L1969:
	movl	$7, %edx
	movq	%r12, %rsi
	movq	%r9, %rdi
	movq	%r9, %r15
	call	_ZN2v88internal5ScopeC1EPNS0_4ZoneENS0_9ScopeTypeENS0_6HandleINS0_9ScopeInfoEEE
	jmp	.L1964
	.p2align 4,,10
	.p2align 3
.L1982:
	movq	%rbx, %rdi
	call	_ZNK2v88internal9ScopeInfo10scope_typeEv@PLT
	testb	%al, %al
	jne	.L1988
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rsi
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1989
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L1990:
	movq	16(%r12), %r9
	movq	24(%r12), %rax
	subq	%r9, %rax
	cmpq	$143, %rax
	jbe	.L2046
	leaq	144(%r9), %rax
	movq	%rax, 16(%r12)
.L1993:
	movq	-88(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r9, %rdi
	movq	%r9, %r15
	call	_ZN2v88internal10ClassScopeC1EPNS0_4ZoneEPNS0_15AstValueFactoryENS0_6HandleINS0_9ScopeInfoEEE
	jmp	.L1964
	.p2align 4,,10
	.p2align 3
.L1977:
	movq	41088(%rax), %rcx
	cmpq	41096(%rax), %rcx
	je	.L2047
.L1979:
	movq	-64(%rbp), %rdx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rcx)
	jmp	.L1978
	.p2align 4,,10
	.p2align 3
.L1965:
	movq	41088(%rax), %rcx
	cmpq	41096(%rax), %rcx
	je	.L2048
.L1967:
	movq	-64(%rbp), %rdx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rcx)
	jmp	.L1966
	.p2align 4,,10
	.p2align 3
.L1959:
	movq	41088(%rax), %rcx
	cmpq	41096(%rax), %rcx
	je	.L2049
.L1961:
	movq	-64(%rbp), %rdx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rcx)
	jmp	.L1960
	.p2align 4,,10
	.p2align 3
.L1983:
	movq	41088(%rax), %rcx
	cmpq	41096(%rax), %rcx
	je	.L2050
.L1985:
	movq	-64(%rbp), %rdx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rcx)
	jmp	.L1984
	.p2align 4,,10
	.p2align 3
.L1988:
	movq	%rbx, %rdi
	call	_ZNK2v88internal9ScopeInfo10scope_typeEv@PLT
	movq	%rbx, %rdi
	cmpb	$6, %al
	je	.L2051
	call	_ZNK2v88internal9ScopeInfo10scope_typeEv@PLT
	cmpb	$3, %al
	jne	.L2006
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rsi
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2007
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L2008:
	movq	16(%r12), %r9
	movq	24(%r12), %rax
	subq	%r9, %rax
	cmpq	$231, %rax
	jbe	.L2052
	leaq	232(%r9), %rax
	movq	%rax, 16(%r12)
.L2011:
	movq	-88(%rbp), %rax
	movq	%r9, %r15
	movl	$3, %edx
	movq	%r9, %rdi
	movq	1096(%rax), %rsi
	call	_ZN2v88internal16DeclarationScopeC2EPNS0_4ZoneENS0_9ScopeTypeENS0_6HandleINS0_9ScopeInfoEEE
	orb	$1, 129(%r15)
	movq	$0, 224(%r15)
	jmp	.L1964
	.p2align 4,,10
	.p2align 3
.L2042:
	movq	-80(%rbp), %rax
	movq	104(%rax), %rax
.L1975:
	testq	%rax, %rax
	jne	.L1971
	movq	-64(%rbp), %rax
	movq	-80(%rbp), %rdx
	addq	$1160, %rax
	movq	%rax, 104(%rdx)
	testq	%r14, %r14
	jne	.L2026
	.p2align 4,,10
	.p2align 3
.L2028:
	movq	-80(%rbp), %r14
	jmp	.L1955
	.p2align 4,,10
	.p2align 3
.L1989:
	movq	41088(%rax), %rcx
	cmpq	41096(%rax), %rcx
	je	.L2053
.L1991:
	movq	-64(%rbp), %rdx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rcx)
	jmp	.L1990
	.p2align 4,,10
	.p2align 3
.L2039:
	movl	-72(%rbp), %esi
	movq	%r13, %r15
	testl	%esi, %esi
	jne	.L1971
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rsi
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1972
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1973:
	movq	-80(%rbp), %rdx
	movq	%r13, %r15
	movq	%rax, 104(%rdx)
	jmp	.L1975
	.p2align 4,,10
	.p2align 3
.L2040:
	movl	$224, %esi
	movq	%r12, %rdi
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %rcx
	movq	%rax, %r9
	jmp	.L1981
	.p2align 4,,10
	.p2align 3
.L2047:
	movq	%rax, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L1979
	.p2align 4,,10
	.p2align 3
.L2051:
	call	_ZNK2v88internal9ScopeInfo20is_declaration_scopeEv@PLT
	movq	-56(%rbp), %rsi
	testb	%al, %al
	movq	-64(%rbp), %rax
	movq	41112(%rax), %rdi
	je	.L1995
	testq	%rdi, %rdi
	je	.L1996
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L1997:
	movq	16(%r12), %r9
	movq	24(%r12), %rax
	subq	%r9, %rax
	cmpq	$223, %rax
	jbe	.L2054
	leaq	224(%r9), %rax
	movq	%rax, 16(%r12)
.L2000:
	movl	$6, %edx
	movq	%r12, %rsi
	movq	%r9, %rdi
	movq	%r9, %r15
	call	_ZN2v88internal16DeclarationScopeC1EPNS0_4ZoneENS0_9ScopeTypeENS0_6HandleINS0_9ScopeInfoEEE
	jmp	.L1964
	.p2align 4,,10
	.p2align 3
.L2006:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZNK2v88internal9ScopeInfo16ContextLocalNameEi@PLT
	movq	%rbx, %rdi
	xorl	%esi, %esi
	movq	%rax, %r15
	call	_ZNK2v88internal9ScopeInfo29ContextLocalMaybeAssignedFlagEi@PLT
	movb	%al, -96(%rbp)
	movq	-64(%rbp), %rax
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2012
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2013:
	movq	-88(%rbp), %rdi
	call	_ZN2v88internal15AstValueFactory9GetStringENS0_6HandleINS0_6StringEEE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdx
	movq	-64(%rbp), %rax
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2015
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-104(%rbp), %rdx
	movq	%rax, %r8
.L2016:
	movq	16(%r12), %r9
	movq	24(%r12), %rax
	subq	%r9, %rax
	cmpq	$135, %rax
	jbe	.L2055
	leaq	136(%r9), %rax
	movq	%rax, 16(%r12)
.L2019:
	movzbl	-96(%rbp), %ecx
	movq	%r12, %rsi
	movq	%r9, %rdi
	movq	%r9, %r15
	call	_ZN2v88internal5ScopeC1EPNS0_4ZoneEPKNS0_12AstRawStringENS0_17MaybeAssignedFlagENS0_6HandleINS0_9ScopeInfoEEE
	jmp	.L1964
	.p2align 4,,10
	.p2align 3
.L2043:
	movl	$224, %esi
	movq	%r12, %rdi
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %rcx
	movq	%rax, %r9
	jmp	.L1963
	.p2align 4,,10
	.p2align 3
.L2045:
	movl	$136, %esi
	movq	%r12, %rdi
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %rcx
	movq	%rax, %r9
	jmp	.L1969
	.p2align 4,,10
	.p2align 3
.L2044:
	movl	$224, %esi
	movq	%r12, %rdi
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %rcx
	movq	%rax, %r9
	jmp	.L1987
	.p2align 4,,10
	.p2align 3
.L2049:
	movq	%rax, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L1961
	.p2align 4,,10
	.p2align 3
.L2048:
	movq	%rax, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L1967
	.p2align 4,,10
	.p2align 3
.L2050:
	movq	%rax, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L1985
.L2046:
	movl	$144, %esi
	movq	%r12, %rdi
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %rcx
	movq	%rax, %r9
	jmp	.L1993
.L1972:
	movq	%rax, %rdx
	movq	41088(%rax), %rax
	cmpq	41096(%rdx), %rax
	je	.L2056
.L1974:
	movq	-64(%rbp), %rcx
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rcx)
	movq	%rsi, (%rax)
	jmp	.L1973
.L1995:
	testq	%rdi, %rdi
	je	.L2001
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L2002:
	movq	16(%r12), %r9
	movq	24(%r12), %rax
	subq	%r9, %rax
	cmpq	$135, %rax
	jbe	.L2057
	leaq	136(%r9), %rax
	movq	%rax, 16(%r12)
.L2005:
	movl	$6, %edx
	movq	%r12, %rsi
	movq	%r9, %rdi
	movq	%r9, %r15
	call	_ZN2v88internal5ScopeC1EPNS0_4ZoneENS0_9ScopeTypeENS0_6HandleINS0_9ScopeInfoEEE
	jmp	.L1964
.L2053:
	movq	%rax, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L1991
.L2007:
	movq	41088(%rax), %rcx
	cmpq	41096(%rax), %rcx
	je	.L2058
.L2009:
	movq	-64(%rbp), %rdx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rcx)
	jmp	.L2008
.L2015:
	movq	41088(%rax), %r8
	cmpq	41096(%rax), %r8
	je	.L2059
.L2017:
	movq	-64(%rbp), %rcx
	leaq	8(%r8), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%r8)
	jmp	.L2016
.L2012:
	movq	41088(%rax), %rsi
	cmpq	41096(%rax), %rsi
	je	.L2060
.L2014:
	movq	-64(%rbp), %rdx
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdx)
	movq	%r15, (%rsi)
	jmp	.L2013
.L1956:
	movl	-72(%rbp), %eax
	testl	%eax, %eax
	jne	.L2028
	cmpq	$0, 104(%rcx)
	jne	.L2028
	movq	%rdi, %r15
	addq	$1160, %r15
	movq	%r15, 104(%rcx)
	jmp	.L2028
.L2001:
	movq	41088(%rax), %rcx
	cmpq	41096(%rax), %rcx
	je	.L2061
.L2003:
	movq	-64(%rbp), %rdx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rcx)
	jmp	.L2002
.L1996:
	movq	41088(%rax), %rcx
	cmpq	41096(%rax), %rcx
	je	.L2062
.L1998:
	movq	-64(%rbp), %rdx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rcx)
	jmp	.L1997
.L2052:
	movl	$232, %esi
	movq	%r12, %rdi
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %rcx
	movq	%rax, %r9
	jmp	.L2011
.L2055:
	movl	$136, %esi
	movq	%r12, %rdi
	movq	%r8, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %r8
	movq	%rax, %r9
	jmp	.L2019
.L2056:
	movq	%rdx, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L1974
.L2058:
	movq	%rax, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L2009
.L2060:
	movq	%rax, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2014
.L2059:
	movq	%rax, %rdi
	movq	%rdx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L2017
.L2057:
	movl	$136, %esi
	movq	%r12, %rdi
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %rcx
	movq	%rax, %r9
	jmp	.L2005
.L2054:
	movl	$224, %esi
	movq	%r12, %rdi
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %rcx
	movq	%rax, %r9
	jmp	.L2000
.L2062:
	movq	%rax, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L1998
.L2061:
	movq	%rax, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L2003
	.cfi_endproc
.LFE21213:
	.size	_ZN2v88internal5Scope21DeserializeScopeChainEPNS0_7IsolateEPNS0_4ZoneENS0_9ScopeInfoEPNS0_16DeclarationScopeEPNS0_15AstValueFactoryENS1_19DeserializationModeE, .-_ZN2v88internal5Scope21DeserializeScopeChainEPNS0_7IsolateEPNS0_4ZoneENS0_9ScopeInfoEPNS0_16DeclarationScopeEPNS0_15AstValueFactoryENS1_19DeserializationModeE
	.section	.text._ZN2v88internal16DeclarationScope31CheckConflictingVarDeclarationsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16DeclarationScope31CheckConflictingVarDeclarationsEv
	.type	_ZN2v88internal16DeclarationScope31CheckConflictingVarDeclarationsEv, @function
_ZN2v88internal16DeclarationScope31CheckConflictingVarDeclarationsEv:
.LFB21257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testb	$4, 132(%rdi)
	jne	.L2067
	movq	96(%rdi), %r13
	leaq	88(%rdi), %r14
	movq	%rdi, %rbx
	movq	%r14, %r12
	cmpq	%r13, %r14
	je	.L2076
	.p2align 4,,10
	.p2align 3
.L2066:
	movq	(%r12), %r12
	movl	4(%r12), %eax
	testb	$63, %al
	jne	.L2069
	testb	$64, %al
	je	.L2069
	movq	24(%r12), %r8
	.p2align 4,,10
	.p2align 3
.L2075:
	cmpb	$5, 128(%r8)
	je	.L2072
	movq	8(%r12), %rax
	movq	32(%r8), %rdi
	movq	8(%rax), %rsi
	movl	40(%r8), %eax
	leal	-1(%rax), %r9d
	movl	24(%rsi), %eax
	shrl	$2, %eax
	andl	%r9d, %eax
	jmp	.L2124
	.p2align 4,,10
	.p2align 3
.L2128:
	cmpq	%rdx, %rsi
	je	.L2073
	addq	$1, %rax
	andq	%r9, %rax
.L2124:
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.L2128
.L2072:
	movq	8(%r8), %r8
	cmpq	%r8, %rbx
	jne	.L2075
	.p2align 4,,10
	.p2align 3
.L2069:
	addq	$16, %r12
	cmpq	%r12, %r13
	jne	.L2066
.L2076:
	cmpb	$1, 128(%rbx)
	je	.L2129
.L2067:
	xorl	%r12d, %r12d
.L2063:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2073:
	.cfi_restore_state
	cmpq	$0, 8(%rcx)
	jne	.L2063
	movq	8(%r8), %r8
	cmpq	%r8, %rbx
	jne	.L2075
	jmp	.L2069
.L2129:
	testb	$1, 129(%rbx)
	jne	.L2067
	movq	%rbx, %rax
.L2079:
	movq	8(%rax), %rax
	testb	$1, 130(%rax)
	je	.L2079
	cmpb	$1, 128(%rax)
	je	.L2079
	movq	8(%rax), %r15
	cmpq	%r13, %r14
	je	.L2067
.L2089:
	movq	(%r14), %r12
	movq	8(%r12), %rax
	testb	$14, 40(%rax)
	jne	.L2130
.L2080:
	leaq	16(%r12), %r14
	cmpq	%r14, %r13
	jne	.L2089
	jmp	.L2067
.L2130:
	movq	8(%rbx), %rdi
.L2088:
	movq	8(%rax), %r8
	movl	40(%rdi), %eax
	movq	32(%rdi), %r9
	leal	-1(%rax), %esi
	movl	24(%r8), %eax
	shrl	$2, %eax
	andl	%esi, %eax
	jmp	.L2125
	.p2align 4,,10
	.p2align 3
.L2131:
	cmpq	%rdx, %r8
	je	.L2082
	addq	$1, %rax
	andq	%rsi, %rax
.L2125:
	leaq	(%rax,%rax,2), %rdx
	leaq	(%r9,%rdx,8), %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.L2131
.L2122:
	cmpq	$0, 104(%rdi)
	je	.L2087
	movq	%rdi, %rdx
	movq	%r8, %rsi
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal5Scope17LookupInScopeInfoEPKNS0_12AstRawStringEPS1_
	movq	-56(%rbp), %rdi
	testq	%rax, %rax
	je	.L2087
.L2090:
	testb	$14, 40(%rax)
	je	.L2063
.L2087:
	movq	8(%rdi), %rdi
	cmpq	%rdi, %r15
	je	.L2132
	movq	8(%r12), %rax
	jmp	.L2088
.L2082:
	movq	8(%rcx), %rax
	testq	%rax, %rax
	jne	.L2090
	jmp	.L2122
.L2132:
	movq	(%r14), %r12
	jmp	.L2080
	.cfi_endproc
.LFE21257:
	.size	_ZN2v88internal16DeclarationScope31CheckConflictingVarDeclarationsEv, .-_ZN2v88internal16DeclarationScope31CheckConflictingVarDeclarationsEv
	.section	.text._ZN2v88internal10ClassScope28LookupPrivateNameInScopeInfoEPKNS0_12AstRawStringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ClassScope28LookupPrivateNameInScopeInfoEPKNS0_12AstRawStringE
	.type	_ZN2v88internal10ClassScope28LookupPrivateNameInScopeInfoEPKNS0_12AstRawStringE, @function
_ZN2v88internal10ClassScope28LookupPrivateNameInScopeInfoEPKNS0_12AstRawStringE:
.LFB21327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-59(%rbp), %rcx
	leaq	-60(%rbp), %rdx
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	-58(%rbp), %r8
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	(%rax), %rsi
	movq	104(%rdi), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal9ScopeInfo16ContextSlotIndexES1_NS0_6StringEPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE@PLT
	testl	%eax, %eax
	js	.L2148
	movq	136(%rbx), %r14
	movzbl	-60(%rbp), %r15d
	movl	%eax, %r12d
	movq	(%rbx), %r10
	testq	%r14, %r14
	je	.L2153
.L2135:
	subq	$8, %rsp
	leaq	-57(%rbp), %rax
	xorl	%r9d, %r9d
	movl	%r15d, %r8d
	pushq	%rax
	leaq	16(%r14), %rdi
	movq	%r13, %rcx
	movq	%rbx, %rdx
	pushq	$1
	movq	%r10, %rsi
	pushq	$0
	call	_ZN2v88internal11VariableMap7DeclareEPNS0_4ZoneEPNS0_5ScopeEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagENS0_17MaybeAssignedFlagEPb
	addq	$32, %rsp
	cmpb	$0, -57(%rbp)
	je	.L2143
	movq	64(%rbx), %rdx
	movq	%rax, (%rdx)
	leaq	24(%rax), %rdx
	movq	%rdx, 64(%rbx)
	movzwl	40(%rax), %edx
.L2144:
	andw	$-1921, %dx
	movl	%r12d, 32(%rax)
	orw	$1408, %dx
	movw	%dx, 40(%rax)
.L2133:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2154
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2143:
	.cfi_restore_state
	movzwl	40(%rax), %ecx
	movl	%ecx, %esi
	movl	%ecx, %edx
	andl	$15, %esi
	cmpb	$8, %sil
	je	.L2145
	cmpb	$9, %sil
	jne	.L2144
	cmpb	$8, %r15b
	sete	%sil
.L2146:
	movl	%ecx, %edx
	andl	$-16, %edx
	orl	$10, %edx
	testb	%sil, %sil
	cmove	%ecx, %edx
	jmp	.L2144
	.p2align 4,,10
	.p2align 3
.L2148:
	xorl	%eax, %eax
	jmp	.L2133
	.p2align 4,,10
	.p2align 3
.L2153:
	movq	16(%r10), %rdx
	movq	24(%r10), %rax
	subq	%rdx, %rax
	cmpq	$47, %rax
	jbe	.L2155
	leaq	48(%rdx), %rax
	movq	%rax, 16(%r10)
.L2137:
	movq	(%rbx), %rdi
	movq	$0, (%rdx)
	movq	%rdx, %r14
	movq	%rdx, 8(%rdx)
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	$191, %rcx
	jbe	.L2156
	leaq	192(%rax), %rcx
	movq	%rcx, 16(%rdi)
.L2139:
	movq	%rax, 16(%rdx)
	testq	%rax, %rax
	je	.L2157
	movl	$8, 24(%rdx)
	movq	$0, (%rax)
	cmpl	$1, 24(%rdx)
	jbe	.L2141
	movl	$24, %ecx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L2142:
	movq	16(%rdx), %rsi
	addq	$1, %rax
	movq	$0, (%rsi,%rcx)
	movl	24(%rdx), %esi
	addq	$24, %rcx
	cmpq	%rax, %rsi
	ja	.L2142
.L2141:
	movl	$0, 28(%rdx)
	movq	$0, 40(%rdx)
	movq	(%rbx), %r10
	movq	%rdx, 136(%rbx)
	jmp	.L2135
	.p2align 4,,10
	.p2align 3
.L2145:
	cmpb	$9, %r15b
	sete	%sil
	jmp	.L2146
	.p2align 4,,10
	.p2align 3
.L2155:
	movl	$48, %esi
	movq	%r10, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L2137
	.p2align 4,,10
	.p2align 3
.L2156:
	movl	$192, %esi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	jmp	.L2139
.L2154:
	call	__stack_chk_fail@PLT
.L2157:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21327:
	.size	_ZN2v88internal10ClassScope28LookupPrivateNameInScopeInfoEPKNS0_12AstRawStringE, .-_ZN2v88internal10ClassScope28LookupPrivateNameInScopeInfoEPKNS0_12AstRawStringE
	.section	.text._ZN2v88internal5Scope6LookupILNS1_15ScopeLookupModeE1EEEPNS0_8VariableEPNS0_13VariableProxyEPS1_S8_S8_b,"axG",@progbits,_ZN2v88internal5Scope6LookupILNS1_15ScopeLookupModeE1EEEPNS0_8VariableEPNS0_13VariableProxyEPS1_S8_S8_b,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal5Scope6LookupILNS1_15ScopeLookupModeE1EEEPNS0_8VariableEPNS0_13VariableProxyEPS1_S8_S8_b
	.type	_ZN2v88internal5Scope6LookupILNS1_15ScopeLookupModeE1EEEPNS0_8VariableEPNS0_13VariableProxyEPS1_S8_S8_b, @function
_ZN2v88internal5Scope6LookupILNS1_15ScopeLookupModeE1EEEPNS0_8VariableEPNS0_13VariableProxyEPS1_S8_S8_b:
.LFB23759:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$24, %rsp
	movl	4(%rdi), %r9d
	movq	8(%rdi), %r10
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%r9d, %r11d
	movq	%r10, %rdi
	andl	$256, %r11d
	je	.L2159
	movq	8(%r10), %rdi
.L2159:
	movl	40(%r15), %eax
	movq	32(%r15), %r8
	leal	-1(%rax), %esi
	movl	24(%rdi), %eax
	shrl	$2, %eax
	andl	%esi, %eax
	jmp	.L2198
	.p2align 4,,10
	.p2align 3
.L2199:
	cmpq	%rdi, %rdx
	je	.L2161
	addq	$1, %rax
	andq	%rsi, %rax
.L2198:
	leaq	(%rax,%rax,2), %rdx
	leaq	(%r8,%rdx,8), %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.L2199
.L2160:
	testb	$32, 129(%r13)
	jne	.L2175
	andl	$256, %r9d
	je	.L2165
.L2201:
	movq	8(%r10), %r10
.L2165:
	movq	%r15, %rdx
	movq	%r10, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal5Scope17LookupInScopeInfoEPKNS0_12AstRawStringEPS1_
	testq	%rax, %rax
	je	.L2166
	movzbl	128(%r13), %edx
	cmpb	$1, %dl
	jne	.L2158
	movzbl	40(%rax), %ecx
	andl	$15, %ecx
	cmpb	$4, %cl
	jne	.L2158
	movq	8(%r13), %rax
	cmpq	%r12, %rax
	je	.L2168
.L2169:
	testb	$1, 130(%r13)
	jne	.L2200
.L2170:
	cmpb	$2, %dl
	movl	4(%r14), %r9d
	movq	8(%r14), %r10
	sete	%dl
	orl	%edx, %ebx
	testb	$32, 129(%rax)
	jne	.L2172
	andl	$256, %r9d
	movq	%rax, %r13
	je	.L2165
	jmp	.L2201
.L2168:
	movq	8(%r14), %rcx
	testb	$1, 5(%r14)
	je	.L2174
	movq	8(%rcx), %rcx
.L2174:
	subq	$8, %rsp
	leaq	-57(%rbp), %rax
	leaq	32(%r15), %rdi
	xorl	%r9d, %r9d
	pushq	%rax
	movl	$5, %r8d
	movq	%r13, %rdx
	pushq	$0
	pushq	$1
	movq	0(%r13), %rsi
	call	_ZN2v88internal11VariableMap7DeclareEPNS0_4ZoneEPNS0_5ScopeEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagENS0_17MaybeAssignedFlagEPb
	addq	$32, %rsp
	.p2align 4,,10
	.p2align 3
.L2158:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2202
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2161:
	.cfi_restore_state
	movq	8(%rcx), %rax
	testq	%rax, %rax
	jne	.L2158
	jmp	.L2160
	.p2align 4,,10
	.p2align 3
.L2166:
	movq	8(%r13), %rax
	cmpq	%r12, %rax
	je	.L2168
	movzbl	128(%r13), %edx
	cmpb	$7, %dl
	jne	.L2169
	movzbl	%bl, %r8d
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal5Scope10LookupWithEPNS0_13VariableProxyEPS1_S4_S4_b
	jmp	.L2158
	.p2align 4,,10
	.p2align 3
.L2172:
	movl	%r9d, %r11d
	andl	$256, %r11d
.L2175:
	testl	%r11d, %r11d
	je	.L2163
	movq	8(%r10), %r10
.L2163:
	subq	$8, %rsp
	leaq	-57(%rbp), %rax
	movq	(%r15), %rsi
	movq	%r15, %rdx
	pushq	%rax
	leaq	32(%r15), %rdi
	xorl	%r9d, %r9d
	movl	$4, %r8d
	pushq	$0
	movq	%r10, %rcx
	pushq	$1
	call	_ZN2v88internal11VariableMap7DeclareEPNS0_4ZoneEPNS0_5ScopeEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagENS0_17MaybeAssignedFlagEPb
	addq	$32, %rsp
	movzwl	40(%rax), %edx
	movl	$-1, 32(%rax)
	andw	$-897, %dx
	orb	$2, %dh
	movw	%dx, 40(%rax)
	jmp	.L2158
	.p2align 4,,10
	.p2align 3
.L2200:
	testb	$4, 129(%r13)
	je	.L2170
	movzbl	%bl, %r8d
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal5Scope16LookupSloppyEvalEPNS0_13VariableProxyEPS1_S4_S4_b
	jmp	.L2158
.L2202:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23759:
	.size	_ZN2v88internal5Scope6LookupILNS1_15ScopeLookupModeE1EEEPNS0_8VariableEPNS0_13VariableProxyEPS1_S8_S8_b, .-_ZN2v88internal5Scope6LookupILNS1_15ScopeLookupModeE1EEEPNS0_8VariableEPNS0_13VariableProxyEPS1_S8_S8_b
	.section	.text._ZN2v88internal5Scope16LookupSloppyEvalEPNS0_13VariableProxyEPS1_S4_S4_b,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope16LookupSloppyEvalEPNS0_13VariableProxyEPS1_S4_S4_b
	.type	_ZN2v88internal5Scope16LookupSloppyEvalEPNS0_13VariableProxyEPS1_S4_S4_b, @function
_ZN2v88internal5Scope16LookupSloppyEvalEPNS0_13VariableProxyEPS1_S4_S4_b:
.LFB21291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	movq	8(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	cmove	%rsi, %rcx
	cmpq	$0, 104(%rsi)
	je	.L2228
	xorl	%r8d, %r8d
	call	_ZN2v88internal5Scope6LookupILNS1_15ScopeLookupModeE1EEEPNS0_8VariableEPNS0_13VariableProxyEPS1_S8_S8_b
	movq	%rax, %r12
.L2206:
	testq	%r12, %r12
	je	.L2203
	movq	%r12, %rdi
	call	_ZNK2v88internal8Variable22IsGlobalObjectPropertyEv@PLT
	testb	%al, %al
	jne	.L2208
	movzwl	40(%r12), %eax
.L2209:
	andl	$15, %eax
	subl	$4, %eax
	cmpb	$2, %al
	jbe	.L2203
	testq	%rbx, %rbx
	je	.L2229
	movq	8(%r12), %rax
	leaq	-64(%rbp), %r15
	leaq	32(%rbx), %rdi
	movq	%r15, %rsi
	movq	%rdi, -72(%rbp)
	movl	24(%rax), %edx
	movq	%rax, -64(%rbp)
	shrl	$2, %edx
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS_8internal20ZoneAllocationPolicyEE6RemoveERKS2_j
	movq	-72(%rbp), %rdi
.L2213:
	movq	8(%r13), %rcx
	testb	$1, 5(%r13)
	je	.L2214
	movq	8(%rcx), %rcx
.L2214:
	subq	$8, %rsp
	movq	%rbx, %rdx
	xorl	%r9d, %r9d
	movl	$6, %r8d
	pushq	%r15
	pushq	$0
	pushq	$1
	movq	(%rbx), %rsi
	call	_ZN2v88internal11VariableMap7DeclareEPNS0_4ZoneEPNS0_5ScopeEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagENS0_17MaybeAssignedFlagEPb
	addq	$32, %rsp
	movzwl	40(%rax), %edx
	movl	$-1, 32(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, %r12
	andw	$-897, %dx
	orb	$2, %dh
	movw	%dx, 40(%rax)
.L2203:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2230
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2208:
	.cfi_restore_state
	testq	%rbx, %rbx
	movq	%rbx, %rax
	movq	8(%r13), %rcx
	cmove	%r14, %rax
	testb	$1, 5(%r13)
	je	.L2211
	movq	8(%rcx), %rcx
.L2211:
	subq	$8, %rsp
	leaq	-64(%rbp), %r15
	leaq	32(%rax), %rdi
	xorl	%r9d, %r9d
	pushq	%r15
	movl	$5, %r8d
	movq	%rax, %rdx
	pushq	$0
	pushq	$1
	movq	(%rax), %rsi
	call	_ZN2v88internal11VariableMap7DeclareEPNS0_4ZoneEPNS0_5ScopeEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagENS0_17MaybeAssignedFlagEPb
	addq	$32, %rsp
	movq	%rax, %r12
	movzwl	40(%rax), %eax
	movl	$-1, 32(%r12)
	andw	$-897, %ax
	orb	$2, %ah
	movw	%ax, 40(%r12)
	jmp	.L2209
	.p2align 4,,10
	.p2align 3
.L2228:
	movzbl	%r8b, %r8d
	xorl	%ecx, %ecx
	call	_ZN2v88internal5Scope6LookupILNS1_15ScopeLookupModeE0EEEPNS0_8VariableEPNS0_13VariableProxyEPS1_S8_S8_b
	movq	%rax, %r12
	jmp	.L2206
	.p2align 4,,10
	.p2align 3
.L2229:
	leaq	32(%r14), %rdi
	movq	%r14, %rbx
	leaq	-64(%rbp), %r15
	jmp	.L2213
.L2230:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21291:
	.size	_ZN2v88internal5Scope16LookupSloppyEvalEPNS0_13VariableProxyEPS1_S4_S4_b, .-_ZN2v88internal5Scope16LookupSloppyEvalEPNS0_13VariableProxyEPS1_S4_S4_b
	.section	.text._ZN2v88internal5Scope6LookupILNS1_15ScopeLookupModeE0EEEPNS0_8VariableEPNS0_13VariableProxyEPS1_S8_S8_b,"axG",@progbits,_ZN2v88internal5Scope6LookupILNS1_15ScopeLookupModeE0EEEPNS0_8VariableEPNS0_13VariableProxyEPS1_S8_S8_b,comdat
	.p2align 4
	.weak	_ZN2v88internal5Scope6LookupILNS1_15ScopeLookupModeE0EEEPNS0_8VariableEPNS0_13VariableProxyEPS1_S8_S8_b
	.type	_ZN2v88internal5Scope6LookupILNS1_15ScopeLookupModeE0EEEPNS0_8VariableEPNS0_13VariableProxyEPS1_S8_S8_b, @function
_ZN2v88internal5Scope6LookupILNS1_15ScopeLookupModeE0EEEPNS0_8VariableEPNS0_13VariableProxyEPS1_S8_S8_b:
.LFB23747:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	4(%rdi), %r15d
	movq	8(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	andl	$256, %r15d
	.p2align 4,,10
	.p2align 3
.L2245:
	movq	%rbx, %rcx
	testl	%r15d, %r15d
	je	.L2232
	movq	8(%rbx), %rcx
.L2232:
	movl	40(%rsi), %eax
	movq	32(%rsi), %rdx
	leal	-1(%rax), %r11d
	movl	24(%rcx), %eax
	shrl	$2, %eax
	andl	%r11d, %eax
	leaq	(%rax,%rax,2), %rdi
	leaq	(%rdx,%rdi,8), %r10
	movzbl	128(%rsi), %edi
	movq	(%r10), %r9
	testq	%r9, %r9
	jne	.L2235
	jmp	.L2233
	.p2align 4,,10
	.p2align 3
.L2263:
	addq	$1, %rax
	andq	%r11, %rax
	leaq	(%rax,%rax,2), %r9
	leaq	(%rdx,%r9,8), %r10
	movq	(%r10), %r9
	testq	%r9, %r9
	je	.L2233
.L2235:
	cmpq	%rcx, %r9
	jne	.L2263
	movq	8(%r10), %rax
	testq	%rax, %rax
	je	.L2233
	cmpb	$1, %dil
	jne	.L2236
	movzbl	40(%rax), %edx
	andl	$15, %edx
	cmpb	$4, %dl
	jne	.L2236
.L2233:
	movq	8(%rsi), %r9
	cmpq	%r13, %r9
	je	.L2264
	cmpb	$7, %dil
	je	.L2265
	testb	$1, 130(%rsi)
	jne	.L2266
.L2242:
	cmpb	$2, %dil
	sete	%al
	orl	%eax, %r8d
	cmpq	$0, 104(%r9)
	jne	.L2244
	movq	%r9, %rsi
	jmp	.L2245
	.p2align 4,,10
	.p2align 3
.L2265:
	movzbl	%r8b, %r8d
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal5Scope10LookupWithEPNS0_13VariableProxyEPS1_S4_S4_b
.L2231:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2267
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2266:
	.cfi_restore_state
	testb	$4, 129(%rsi)
	je	.L2242
	movzbl	%r8b, %r8d
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal5Scope16LookupSloppyEvalEPNS0_13VariableProxyEPS1_S4_S4_b
	jmp	.L2231
.L2236:
	testb	%r8b, %r8b
	je	.L2231
	movzwl	40(%rax), %edx
	movl	%edx, %ecx
	andl	$15, %ecx
	subl	$4, %ecx
	cmpb	$2, %cl
	jbe	.L2231
	orb	$4, %dh
	movw	%dx, 40(%rax)
	jmp	.L2231
.L2264:
	xorl	%eax, %eax
	cmpb	$4, %dil
	jne	.L2231
	subq	$8, %rsp
	leaq	-57(%rbp), %rax
	leaq	32(%rsi), %rdi
	movq	%rsi, %rdx
	pushq	%rax
	xorl	%r9d, %r9d
	movl	$5, %r8d
	pushq	$0
	pushq	$1
	movq	(%rsi), %rsi
	call	_ZN2v88internal11VariableMap7DeclareEPNS0_4ZoneEPNS0_5ScopeEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagENS0_17MaybeAssignedFlagEPb
	addq	$32, %rsp
	jmp	.L2231
.L2244:
	xorl	%r8d, %r8d
	movq	%r9, %rcx
	movq	%r13, %rdx
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal5Scope6LookupILNS1_15ScopeLookupModeE1EEEPNS0_8VariableEPNS0_13VariableProxyEPS1_S8_S8_b
	jmp	.L2231
.L2267:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23747:
	.size	_ZN2v88internal5Scope6LookupILNS1_15ScopeLookupModeE0EEEPNS0_8VariableEPNS0_13VariableProxyEPS1_S8_S8_b, .-_ZN2v88internal5Scope6LookupILNS1_15ScopeLookupModeE0EEEPNS0_8VariableEPNS0_13VariableProxyEPS1_S8_S8_b
	.section	.text._ZN2v88internal5Scope10LookupWithEPNS0_13VariableProxyEPS1_S4_S4_b,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope10LookupWithEPNS0_13VariableProxyEPS1_S4_S4_b
	.type	_ZN2v88internal5Scope10LookupWithEPNS0_13VariableProxyEPS1_S4_S4_b, @function
_ZN2v88internal5Scope10LookupWithEPNS0_13VariableProxyEPS1_S4_S4_b:
.LFB21290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	movq	8(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 104(%rsi)
	je	.L2308
	xorl	%r8d, %r8d
	call	_ZN2v88internal5Scope6LookupILNS1_15ScopeLookupModeE1EEEPNS0_8VariableEPNS0_13VariableProxyEPS1_S8_S8_b
	movq	%rax, %r12
.L2270:
	testq	%r12, %r12
	je	.L2282
	movzwl	40(%r12), %edx
	movl	%edx, %eax
	andl	$15, %eax
	subl	$4, %eax
	cmpb	$2, %al
	jbe	.L2273
	testw	$896, %dx
	jne	.L2273
	orb	$12, %dh
	movw	%dx, 40(%r12)
	testb	$-128, 4(%r13)
	jne	.L2309
	.p2align 4,,10
	.p2align 3
.L2273:
	testq	%rbx, %rbx
	je	.L2310
	movq	8(%r12), %rax
	leaq	32(%rbx), %r15
	leaq	-64(%rbp), %r14
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	24(%rax), %edx
	movq	%rax, -64(%rbp)
	shrl	$2, %edx
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS_8internal20ZoneAllocationPolicyEE6RemoveERKS2_j
.L2279:
	movq	8(%r13), %rcx
	testb	$1, 5(%r13)
	je	.L2280
	movq	8(%rcx), %rcx
.L2280:
	subq	$8, %rsp
	movq	%rbx, %rdx
	xorl	%r9d, %r9d
	movl	$4, %r8d
	pushq	%r14
	movq	%r15, %rdi
	pushq	$0
	pushq	$1
	movq	(%rbx), %rsi
	call	_ZN2v88internal11VariableMap7DeclareEPNS0_4ZoneEPNS0_5ScopeEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagENS0_17MaybeAssignedFlagEPb
	addq	$32, %rsp
	movzwl	40(%rax), %edx
	movl	$-1, 32(%rax)
	movq	%r12, 16(%rax)
	andw	$-897, %dx
	orb	$2, %dh
	movw	%dx, 40(%rax)
.L2268:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2311
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2310:
	.cfi_restore_state
	leaq	32(%r14), %r15
	movq	%r14, %rbx
	leaq	-64(%rbp), %r14
	jmp	.L2279
	.p2align 4,,10
	.p2align 3
.L2308:
	movzbl	%r8b, %r8d
	xorl	%ecx, %ecx
	call	_ZN2v88internal5Scope6LookupILNS1_15ScopeLookupModeE0EEEPNS0_8VariableEPNS0_13VariableProxyEPS1_S8_S8_b
	movq	%rax, %r12
	jmp	.L2270
	.p2align 4,,10
	.p2align 3
.L2309:
	movl	%edx, %eax
	andl	$15, %eax
	cmpb	$1, %al
	je	.L2273
	movq	16(%r12), %r15
	testq	%r15, %r15
	je	.L2275
	testb	$64, %dh
	jne	.L2275
	movzwl	40(%r15), %eax
	movl	%eax, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L2275
	movq	16(%r15), %rdx
	testq	%rdx, %rdx
	je	.L2276
	testb	$64, %ah
	je	.L2312
.L2276:
	orb	$64, %ah
	movw	%ax, 40(%r15)
	movzwl	40(%r12), %edx
	.p2align 4,,10
	.p2align 3
.L2275:
	orb	$64, %dh
	movw	%dx, 40(%r12)
	jmp	.L2273
	.p2align 4,,10
	.p2align 3
.L2282:
	xorl	%eax, %eax
	jmp	.L2268
.L2312:
	movzwl	40(%rdx), %ecx
	movl	%ecx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L2276
	movq	16(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2277
	testb	$64, %ch
	je	.L2313
.L2277:
	orb	$64, %ch
	movw	%cx, 40(%rdx)
	movzwl	40(%r15), %eax
	jmp	.L2276
.L2313:
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movq	-72(%rbp), %rdx
	movzwl	40(%rdx), %ecx
	jmp	.L2277
.L2311:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21290:
	.size	_ZN2v88internal5Scope10LookupWithEPNS0_13VariableProxyEPS1_S4_S4_b, .-_ZN2v88internal5Scope10LookupWithEPNS0_13VariableProxyEPS1_S4_S4_b
	.section	.text._ZN2v88internal5Scope16AnalyzePartiallyEPNS0_16DeclarationScopeEPNS0_14AstNodeFactoryEPNS_4base16ThreadedListBaseINS0_13VariableProxyENS6_9EmptyBaseENS8_14UnresolvedNextEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope16AnalyzePartiallyEPNS0_16DeclarationScopeEPNS0_14AstNodeFactoryEPNS_4base16ThreadedListBaseINS0_13VariableProxyENS6_9EmptyBaseENS8_14UnresolvedNextEEEb
	.type	_ZN2v88internal5Scope16AnalyzePartiallyEPNS0_16DeclarationScopeEPNS0_14AstNodeFactoryEPNS_4base16ThreadedListBaseINS0_13VariableProxyENS6_9EmptyBaseENS8_14UnresolvedNextEEEb, @function
_ZN2v88internal5Scope16AnalyzePartiallyEPNS0_16DeclarationScopeEPNS0_14AstNodeFactoryEPNS_4base16ThreadedListBaseINS0_13VariableProxyENS6_9EmptyBaseENS8_14UnresolvedNextEEEb:
.LFB21278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%rdx, -64(%rbp)
	movb	%r8b, -65(%rbp)
.L2315:
	movq	72(%rbx), %r14
	testq	%r14, %r14
	jne	.L2316
	jmp	.L2333
	.p2align 4,,10
	.p2align 3
.L2321:
	movzwl	40(%rax), %edx
	orb	$8, %dh
	movw	%dx, 40(%rax)
	testb	$-128, 4(%r14)
	jne	.L2392
.L2320:
	movq	16(%r14), %r14
	testq	%r14, %r14
	je	.L2333
.L2316:
	testb	$2, 5(%r14)
	jne	.L2320
	movq	8(%r15), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal5Scope6LookupILNS1_15ScopeLookupModeE0EEEPNS0_8VariableEPNS0_13VariableProxyEPS1_S8_S8_b
	testq	%rax, %rax
	jne	.L2321
	movq	8(%r15), %rax
	cmpb	$4, 128(%rax)
	jne	.L2322
	cmpb	$0, -65(%rbp)
	je	.L2320
.L2322:
	movq	-64(%rbp), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L2393
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L2325:
	movq	%rax, %rdi
	movq	%r14, %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal13VariableProxyC1EPKS1_@PLT
	movq	8(%r13), %rdx
	movq	-56(%rbp), %rax
	movq	%rax, (%rdx)
	jmp	.L2390
	.p2align 4,,10
	.p2align 3
.L2394:
	testb	$2, 5(%rax)
	je	.L2326
.L2390:
	leaq	16(%rax), %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L2394
.L2326:
	movq	%rdx, 8(%r13)
	movq	16(%r14), %r14
	testq	%r14, %r14
	jne	.L2316
	.p2align 4,,10
	.p2align 3
.L2333:
	leaq	72(%rbx), %rax
	movq	$0, 72(%rbx)
	movq	%rax, 80(%rbx)
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L2391
.L2317:
	movq	%rax, %rbx
	jmp	.L2315
	.p2align 4,,10
	.p2align 3
.L2337:
	cmpq	%rbx, %r12
	je	.L2314
	movq	8(%rbx), %rbx
.L2391:
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L2337
	cmpq	%rbx, %r12
	jne	.L2317
.L2314:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2392:
	.cfi_restore_state
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L2320
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L2328
	testb	$64, %dh
	je	.L2395
.L2328:
	orb	$64, %dh
	movw	%dx, 40(%rax)
	jmp	.L2320
	.p2align 4,,10
	.p2align 3
.L2395:
	movzwl	40(%rcx), %esi
	movl	%esi, %edi
	andl	$15, %edi
	cmpb	$1, %dil
	je	.L2328
	movq	16(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L2329
	testw	$16384, %si
	je	.L2396
.L2329:
	orw	$16384, %si
	movw	%si, 40(%rcx)
	movzwl	40(%rax), %edx
	jmp	.L2328
	.p2align 4,,10
	.p2align 3
.L2393:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2325
.L2396:
	movzwl	40(%rdx), %edi
	movl	%edi, %r8d
	andl	$15, %r8d
	cmpb	$1, %r8b
	je	.L2329
	movq	16(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L2330
	testw	$16384, %di
	jne	.L2330
	movzwl	40(%rsi), %r8d
	movl	%r8d, %r9d
	andl	$15, %r9d
	cmpb	$1, %r9b
	je	.L2330
	movq	16(%rsi), %r9
	testq	%r9, %r9
	je	.L2331
	testw	$16384, %r8w
	je	.L2397
.L2331:
	orw	$16384, %r8w
	movw	%r8w, 40(%rsi)
	movzwl	40(%rdx), %edi
.L2330:
	orw	$16384, %di
	movw	%di, 40(%rdx)
	movzwl	40(%rcx), %esi
	jmp	.L2329
.L2397:
	movzwl	40(%r9), %edi
	movl	%edi, %r10d
	andl	$15, %r10d
	cmpb	$1, %r10b
	je	.L2331
	movq	16(%r9), %r8
	testq	%r8, %r8
	je	.L2332
	testw	$16384, %di
	je	.L2398
.L2332:
	orw	$16384, %di
	movw	%di, 40(%r9)
	movzwl	40(%rsi), %r8d
	jmp	.L2331
.L2398:
	movq	%r8, %rdi
	movq	%r9, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movq	-104(%rbp), %r9
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movzwl	40(%r9), %edi
	movq	-56(%rbp), %rax
	jmp	.L2332
	.cfi_endproc
.LFE21278:
	.size	_ZN2v88internal5Scope16AnalyzePartiallyEPNS0_16DeclarationScopeEPNS0_14AstNodeFactoryEPNS_4base16ThreadedListBaseINS0_13VariableProxyENS6_9EmptyBaseENS8_14UnresolvedNextEEEb, .-_ZN2v88internal5Scope16AnalyzePartiallyEPNS0_16DeclarationScopeEPNS0_14AstNodeFactoryEPNS_4base16ThreadedListBaseINS0_13VariableProxyENS6_9EmptyBaseENS8_14UnresolvedNextEEEb
	.section	.text._ZN2v88internal16DeclarationScope16AnalyzePartiallyEPNS0_6ParserEPNS0_14AstNodeFactoryEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16DeclarationScope16AnalyzePartiallyEPNS0_6ParserEPNS0_14AstNodeFactoryEb
	.type	_ZN2v88internal16DeclarationScope16AnalyzePartiallyEPNS0_6ParserEPNS0_14AstNodeFactoryEb, @function
_ZN2v88internal16DeclarationScope16AnalyzePartiallyEPNS0_6ParserEPNS0_14AstNodeFactoryEb:
.LFB21286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-80(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	133(%rdi), %eax
	movq	$0, -80(%rbp)
	movq	%r12, -72(%rbp)
	subl	$8, %eax
	cmpb	$1, %al
	jbe	.L2401
	movq	8(%rdi), %rax
	movq	%rsi, %r13
	movq	%rdx, %r14
	cmpb	$4, 128(%rax)
	jne	.L2407
	testb	%cl, %cl
	je	.L2402
.L2407:
	movzbl	%cl, %r8d
	movq	%r14, %rdx
	movq	%r12, %rcx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal5Scope16AnalyzePartiallyEPNS0_16DeclarationScopeEPNS0_14AstNodeFactoryEPNS_4base16ThreadedListBaseINS0_13VariableProxyENS6_9EmptyBaseENS8_14UnresolvedNextEEEb
	movq	184(%rbx), %r15
	testq	%r15, %r15
	je	.L2405
	movq	(%r14), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$47, %rax
	jbe	.L2429
	leaq	48(%r12), %rax
	movq	%rax, 16(%rdi)
.L2409:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8VariableC1EPS1_@PLT
	movq	%r12, 184(%rbx)
.L2405:
	movq	%rbx, %r12
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2412:
	movq	%rax, %r12
.L2410:
	cmpb	$2, 128(%r12)
	jne	.L2411
	movzbl	133(%r12), %eax
	subl	$8, %eax
	cmpb	$1, %al
	jbe	.L2411
	movq	208(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2411
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal19PreparseDataBuilder23SaveScopeAllocationDataEPNS0_16DeclarationScopeEPNS0_6ParserE@PLT
	.p2align 4,,10
	.p2align 3
.L2411:
	movq	16(%r12), %rax
	testq	%rax, %rax
	jne	.L2412
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L2430
	.p2align 4,,10
	.p2align 3
.L2416:
	cmpq	%r12, %rbx
	je	.L2401
	movq	8(%r12), %r12
	movq	24(%r12), %rax
	testq	%rax, %rax
	je	.L2416
.L2430:
	cmpq	%r12, %rbx
	jne	.L2412
	.p2align 4,,10
	.p2align 3
.L2401:
	leaq	88(%rbx), %rax
	leaq	72(%rbx), %r12
	andb	$-9, 131(%rbx)
	movq	(%rbx), %rdi
	movq	%rax, 96(%rbx)
	leaq	56(%rbx), %rax
	movq	%rax, 64(%rbx)
	leaq	160(%rbx), %rax
	movq	%r12, 80(%rbx)
	movq	%rax, 168(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 72(%rbx)
	movq	$0, 144(%rbx)
	movq	$0, 152(%rbx)
	movq	$0, 160(%rbx)
	movq	$0, 216(%rbx)
	movq	$0, 184(%rbx)
	call	_ZN2v88internal4Zone13ReleaseMemoryEv@PLT
	movq	-80(%rbp), %rax
	orb	$-128, 131(%rbx)
	movq	$0, (%rbx)
	testq	%rax, %rax
	cmovne	-72(%rbp), %r12
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movq	%rax, 72(%rbx)
	movq	%r12, 80(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2431
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2402:
	.cfi_restore_state
	movq	208(%rdi), %rdi
	movl	%ecx, -84(%rbp)
	testq	%rdi, %rdi
	je	.L2401
	call	_ZNK2v88internal19PreparseDataBuilder17HasInnerFunctionsEv@PLT
	movl	-84(%rbp), %ecx
	testb	%al, %al
	jne	.L2407
	jmp	.L2401
	.p2align 4,,10
	.p2align 3
.L2429:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L2409
.L2431:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21286:
	.size	_ZN2v88internal16DeclarationScope16AnalyzePartiallyEPNS0_6ParserEPNS0_14AstNodeFactoryEb, .-_ZN2v88internal16DeclarationScope16AnalyzePartiallyEPNS0_6ParserEPNS0_14AstNodeFactoryEb
	.section	.text._ZN2v88internal5Scope15ResolveVariableEPNS0_9ParseInfoEPNS0_13VariableProxyE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope15ResolveVariableEPNS0_9ParseInfoEPNS0_13VariableProxyE
	.type	_ZN2v88internal5Scope15ResolveVariableEPNS0_9ParseInfoEPNS0_13VariableProxyE, @function
_ZN2v88internal5Scope15ResolveVariableEPNS0_9ParseInfoEPNS0_13VariableProxyE:
.LFB21292:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal5Scope6LookupILNS1_15ScopeLookupModeE0EEEPNS0_8VariableEPNS0_13VariableProxyEPS1_S8_S8_b
	movq	%r12, %rsi
	movq	%r13, %rdx
	movq	%rax, %rdi
	movq	%rax, %r9
	call	_ZN2v88internal12_GLOBAL__N_120UpdateNeedsHoleCheckEPNS0_8VariableEPNS0_13VariableProxyEPNS0_5ScopeE
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%r9, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13VariableProxy6BindToEPNS0_8VariableE@PLT
	.cfi_endproc
.LFE21292:
	.size	_ZN2v88internal5Scope15ResolveVariableEPNS0_9ParseInfoEPNS0_13VariableProxyE, .-_ZN2v88internal5Scope15ResolveVariableEPNS0_9ParseInfoEPNS0_13VariableProxyE
	.section	.text._ZN2v88internal5Scope16CollectNonLocalsEPNS0_16DeclarationScopeEPNS0_7IsolateEPNS0_9ParseInfoEPNS0_6HandleINS0_9StringSetEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope16CollectNonLocalsEPNS0_16DeclarationScopeEPNS0_7IsolateEPNS0_9ParseInfoEPNS0_6HandleINS0_9StringSetEEE
	.type	_ZN2v88internal5Scope16CollectNonLocalsEPNS0_16DeclarationScopeEPNS0_7IsolateEPNS0_9ParseInfoEPNS0_6HandleINS0_9StringSetEEE, @function
_ZN2v88internal5Scope16CollectNonLocalsEPNS0_16DeclarationScopeEPNS0_7IsolateEPNS0_9ParseInfoEPNS0_6HandleINS0_9StringSetEEE:
.LFB21276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpb	$3, 128(%r15)
	movq	%rdi, -96(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%r8, -80(%rbp)
	je	.L2486
.L2436:
	movq	%r15, %rbx
	testb	$1, 130(%r15)
	je	.L2437
	cmpb	$0, 131(%r15)
	jns	.L2437
	movq	8(%r15), %rbx
.L2437:
	movq	72(%r15), %r12
	leaq	72(%r15), %rax
	movq	%rax, -88(%rbp)
	movq	%rax, %r13
	testq	%r12, %r12
	jne	.L2439
	jmp	.L2438
	.p2align 4,,10
	.p2align 3
.L2487:
	leaq	16(%r12), %r13
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L2438
.L2439:
	testb	$2, 5(%r12)
	jne	.L2487
.L2438:
	movq	80(%r15), %rax
	movq	%rax, -64(%rbp)
	cmpq	%rax, %r13
	je	.L2448
	movq	%r15, -56(%rbp)
	movq	%r12, %r14
	movq	-104(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L2440:
	movq	8(%r12), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal5Scope6LookupILNS1_15ScopeLookupModeE0EEEPNS0_8VariableEPNS0_13VariableProxyEPS1_S8_S8_b
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2488
	movq	-56(%rbp), %rdx
	movq	%rax, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_120UpdateNeedsHoleCheckEPNS0_8VariableEPNS0_13VariableProxyEPNS0_5ScopeE
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal13VariableProxy6BindToEPNS0_8VariableE@PLT
	movzwl	40(%r15), %ecx
	movl	%ecx, %eax
	andl	$15, %eax
	movl	%eax, %edx
	subl	$4, %edx
	cmpb	$2, %dl
	jbe	.L2445
	cmpq	%rbx, -56(%rbp)
	je	.L2445
	orb	$4, %ch
	movw	%cx, 40(%r15)
	.p2align 4,,10
	.p2align 3
.L2445:
	movq	0(%r13), %rax
	movq	16(%rax), %r14
	leaq	16(%rax), %r13
	testq	%r14, %r14
	jne	.L2447
	jmp	.L2446
	.p2align 4,,10
	.p2align 3
.L2489:
	leaq	16(%r14), %r13
	movq	16(%r14), %r14
	testq	%r14, %r14
	je	.L2446
.L2447:
	testb	$2, 5(%r14)
	jne	.L2489
.L2446:
	cmpq	%r13, -64(%rbp)
	jne	.L2440
	movq	-56(%rbp), %r15
.L2448:
	movq	-88(%rbp), %rax
	movq	$0, 72(%r15)
	movq	%rax, 80(%r15)
	movq	16(%r15), %rax
	testq	%rax, %rax
	je	.L2490
.L2441:
	movq	%rax, %r15
	cmpb	$3, 128(%r15)
	jne	.L2436
.L2486:
	movq	%r15, %rdi
	call	_ZN2v88internal11ModuleScope23AllocateModuleVariablesEv
	jmp	.L2436
	.p2align 4,,10
	.p2align 3
.L2488:
	movq	8(%r14), %rax
	testb	$1, 5(%r14)
	je	.L2444
	movq	8(%rax), %rax
.L2444:
	movq	-80(%rbp), %r15
	movq	(%rax), %rdx
	movq	-72(%rbp), %rdi
	movq	(%r15), %rsi
	call	_ZN2v88internal9StringSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE@PLT
	movq	%rax, (%r15)
	jmp	.L2445
	.p2align 4,,10
	.p2align 3
.L2490:
	movq	24(%r15), %rax
	testq	%rax, %rax
	jne	.L2449
	cmpq	%r15, -96(%rbp)
	je	.L2434
	movq	-96(%rbp), %rdx
	jmp	.L2451
	.p2align 4,,10
	.p2align 3
.L2452:
	cmpq	%r15, %rdx
	je	.L2434
.L2451:
	movq	8(%r15), %r15
	movq	24(%r15), %rax
	testq	%rax, %rax
	je	.L2452
.L2449:
	cmpq	%r15, -96(%rbp)
	jne	.L2441
.L2434:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21276:
	.size	_ZN2v88internal5Scope16CollectNonLocalsEPNS0_16DeclarationScopeEPNS0_7IsolateEPNS0_9ParseInfoEPNS0_6HandleINS0_9StringSetEEE, .-_ZN2v88internal5Scope16CollectNonLocalsEPNS0_16DeclarationScopeEPNS0_7IsolateEPNS0_9ParseInfoEPNS0_6HandleINS0_9StringSetEEE
	.section	.text._ZN2v88internal16DeclarationScope16CollectNonLocalsEPNS0_7IsolateEPNS0_9ParseInfoENS0_6HandleINS0_9StringSetEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16DeclarationScope16CollectNonLocalsEPNS0_7IsolateEPNS0_9ParseInfoENS0_6HandleINS0_9StringSetEEE
	.type	_ZN2v88internal16DeclarationScope16CollectNonLocalsEPNS0_7IsolateEPNS0_9ParseInfoENS0_6HandleINS0_9StringSetEEE, @function
_ZN2v88internal16DeclarationScope16CollectNonLocalsEPNS0_7IsolateEPNS0_9ParseInfoENS0_6HandleINS0_9StringSetEEE:
.LFB21280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	leaq	-8(%rbp), %r8
	movq	%rcx, -8(%rbp)
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	call	_ZN2v88internal5Scope16CollectNonLocalsEPNS0_16DeclarationScopeEPNS0_7IsolateEPNS0_9ParseInfoEPNS0_6HandleINS0_9StringSetEEE
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21280:
	.size	_ZN2v88internal16DeclarationScope16CollectNonLocalsEPNS0_7IsolateEPNS0_9ParseInfoENS0_6HandleINS0_9StringSetEEE, .-_ZN2v88internal16DeclarationScope16CollectNonLocalsEPNS0_7IsolateEPNS0_9ParseInfoENS0_6HandleINS0_9StringSetEEE
	.section	.text._ZN2v88internal5Scope27ResolveVariablesRecursivelyEPNS0_9ParseInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope27ResolveVariablesRecursivelyEPNS0_9ParseInfoE
	.type	_ZN2v88internal5Scope27ResolveVariablesRecursivelyEPNS0_9ParseInfoE, @function
_ZN2v88internal5Scope27ResolveVariablesRecursivelyEPNS0_9ParseInfoE:
.LFB21297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testb	$1, 130(%rdi)
	je	.L2494
	cmpb	$0, 131(%rdi)
	js	.L2562
.L2494:
	movq	72(%r15), %r12
	leaq	72(%r15), %rbx
	testq	%r12, %r12
	jne	.L2505
	jmp	.L2504
	.p2align 4,,10
	.p2align 3
.L2563:
	leaq	16(%r12), %rbx
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L2504
.L2505:
	testb	$2, 5(%r12)
	jne	.L2563
.L2504:
	movq	80(%r15), %r13
	.p2align 4,,10
	.p2align 3
.L2561:
	cmpq	%rbx, %r13
	je	.L2564
.L2506:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZN2v88internal5Scope6LookupILNS1_15ScopeLookupModeE0EEEPNS0_8VariableEPNS0_13VariableProxyEPS1_S8_S8_b
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%rax, %rdi
	movq	%rax, %r9
	call	_ZN2v88internal12_GLOBAL__N_120UpdateNeedsHoleCheckEPNS0_8VariableEPNS0_13VariableProxyEPNS0_5ScopeE
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal13VariableProxy6BindToEPNS0_8VariableE@PLT
	movq	(%rbx), %rax
	movq	16(%rax), %r12
	leaq	16(%rax), %rbx
	testq	%r12, %r12
	je	.L2561
	.p2align 4,,10
	.p2align 3
.L2509:
	testb	$2, 5(%r12)
	je	.L2561
	leaq	16(%r12), %rbx
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L2509
	cmpq	%rbx, %r13
	jne	.L2506
.L2564:
	movq	16(%r15), %rbx
	testq	%rbx, %rbx
	je	.L2499
.L2507:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal5Scope27ResolveVariablesRecursivelyEPNS0_9ParseInfoE
	testb	%al, %al
	je	.L2493
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L2507
.L2499:
	movl	$1, %eax
.L2493:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2562:
	.cfi_restore_state
	movq	%rsi, %rdi
	call	_ZNK2v88internal9ParseInfo5scopeEv@PLT
	cmpb	$4, 128(%rax)
	movq	%rax, %r13
	je	.L2495
	movq	8(%rax), %r13
.L2495:
	movq	72(%r15), %rdi
	leaq	72(%r15), %rbx
	testq	%rdi, %rdi
	jne	.L2497
	jmp	.L2496
	.p2align 4,,10
	.p2align 3
.L2565:
	leaq	16(%rdi), %rbx
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2496
.L2497:
	testb	$2, 5(%rdi)
	jne	.L2565
.L2496:
	movq	80(%r15), %r12
	.p2align 4,,10
	.p2align 3
.L2559:
	cmpq	%rbx, %r12
	je	.L2499
	movq	8(%r15), %rsi
	movq	%r13, %rdx
	call	_ZN2v88internal5Scope24ResolvePreparsedVariableEPNS0_13VariableProxyEPS1_S4_
	movq	(%rbx), %rdx
	movq	16(%rdx), %rdi
	leaq	16(%rdx), %rbx
	testq	%rdi, %rdi
	je	.L2559
	.p2align 4,,10
	.p2align 3
.L2501:
	testb	$2, 5(%rdi)
	je	.L2559
	leaq	16(%rdi), %rbx
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L2501
	jmp	.L2559
	.cfi_endproc
.LFE21297:
	.size	_ZN2v88internal5Scope27ResolveVariablesRecursivelyEPNS0_9ParseInfoE, .-_ZN2v88internal5Scope27ResolveVariablesRecursivelyEPNS0_9ParseInfoE
	.section	.text._ZN2v88internal16DeclarationScope17AllocateVariablesEPNS0_9ParseInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16DeclarationScope17AllocateVariablesEPNS0_9ParseInfoE
	.type	_ZN2v88internal16DeclarationScope17AllocateVariablesEPNS0_9ParseInfoE, @function
_ZN2v88internal16DeclarationScope17AllocateVariablesEPNS0_9ParseInfoE:
.LFB21260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movzbl	128(%rdi), %eax
	cmpb	$3, %al
	je	.L2586
.L2567:
	movq	%r12, %rdi
	jmp	.L2570
	.p2align 4,,10
	.p2align 3
.L2587:
	movzbl	128(%rdi), %eax
.L2570:
	testb	%al, %al
	je	.L2568
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L2587
.L2569:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal5Scope27ResolveVariablesRecursivelyEPNS0_9ParseInfoE
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2580
	movzbl	131(%r12), %eax
	movq	%r12, %rbx
	shrb	$7, %al
	je	.L2573
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2568:
	.cfi_restore_state
	movq	%r13, %rsi
	call	_ZN2v88internal10ClassScope19ResolvePrivateNamesEPNS0_9ParseInfoE
	testb	%al, %al
	jne	.L2569
.L2580:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2589:
	.cfi_restore_state
	cmpq	%r12, %rbx
	je	.L2577
.L2575:
	movq	%rax, %rbx
.L2573:
	movq	%rbx, %rdi
	call	_ZZN2v88internal5Scope28AllocateVariablesRecursivelyEvENKUlPS1_E_clES2_.isra.0
	cmpl	$1, %eax
	jne	.L2585
	jmp	.L2588
	.p2align 4,,10
	.p2align 3
.L2579:
	cmpq	%r12, %rbx
	je	.L2577
	movq	8(%rbx), %rbx
.L2585:
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L2579
	jmp	.L2589
	.p2align 4,,10
	.p2align 3
.L2586:
	call	_ZN2v88internal11ModuleScope23AllocateModuleVariablesEv
	movzbl	128(%r12), %eax
	jmp	.L2567
	.p2align 4,,10
	.p2align 3
.L2577:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2588:
	.cfi_restore_state
	movq	16(%rbx), %rax
	testq	%rax, %rax
	jne	.L2575
	jmp	.L2585
	.cfi_endproc
.LFE21260:
	.size	_ZN2v88internal16DeclarationScope17AllocateVariablesEPNS0_9ParseInfoE, .-_ZN2v88internal16DeclarationScope17AllocateVariablesEPNS0_9ParseInfoE
	.section	.text._ZN2v88internal5Scope24DeclareCatchVariableNameEPKNS0_12AstRawStringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope24DeclareCatchVariableNameEPKNS0_12AstRawStringE
	.type	_ZN2v88internal5Scope24DeclareCatchVariableNameEPKNS0_12AstRawStringE, @function
_ZN2v88internal5Scope24DeclareCatchVariableNameEPKNS0_12AstRawStringE:
.LFB21250:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	24(%rsi), %r15d
	movl	40(%rdi), %eax
	movq	(%rdi), %r14
	movq	32(%rdi), %rsi
	shrl	$2, %r15d
	leal	-1(%rax), %edx
	movl	%r15d, %eax
	andl	%edx, %eax
	jmp	.L2616
	.p2align 4,,10
	.p2align 3
.L2617:
	cmpq	%rcx, %r12
	je	.L2592
	addq	$1, %rax
	andq	%rdx, %rax
.L2616:
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rsi,%rcx,8), %r13
	movq	0(%r13), %rcx
	testq	%rcx, %rcx
	jne	.L2617
	movq	%r12, 0(%r13)
	movq	$0, 8(%r13)
	movl	%r15d, 16(%r13)
	movl	44(%rbx), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 44(%rbx)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	40(%rbx), %eax
	jnb	.L2618
.L2592:
	movq	8(%r13), %rax
	testq	%rax, %rax
	je	.L2619
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2619:
	.cfi_restore_state
	movq	16(%r14), %rax
	movq	24(%r14), %rdx
	subq	%rax, %rdx
	cmpq	$47, %rdx
	jbe	.L2620
	leaq	48(%rax), %rdx
	movq	%rdx, 16(%r14)
.L2599:
	movq	%rbx, %xmm0
	movq	%r12, %xmm1
	movl	$4098, %edx
	movq	$-1, 32(%rax)
	punpcklqdq	%xmm1, %xmm0
	movw	%dx, 40(%rax)
	movups	%xmm0, (%rax)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movq	%rax, 8(%r13)
	movq	64(%rbx), %rdx
	movq	%rax, (%rdx)
	leaq	24(%rax), %rdx
	movq	%rdx, 64(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2618:
	.cfi_restore_state
	movq	%r14, %rsi
	leaq	32(%rbx), %rdi
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS_8internal20ZoneAllocationPolicyEE6ResizeES6_
	movl	40(%rbx), %eax
	movq	32(%rbx), %rsi
	leal	-1(%rax), %ecx
	andl	%ecx, %r15d
	movl	%r15d, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %r13
	movq	0(%r13), %rdx
	testq	%rdx, %rdx
	je	.L2592
	cmpq	%rdx, %r12
	jne	.L2595
	jmp	.L2592
	.p2align 4,,10
	.p2align 3
.L2621:
	testq	%rdx, %rdx
	je	.L2592
.L2595:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %r13
	movq	0(%r13), %rdx
	cmpq	%rdx, %r12
	jne	.L2621
	jmp	.L2592
	.p2align 4,,10
	.p2align 3
.L2620:
	movl	$48, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2599
	.cfi_endproc
.LFE21250:
	.size	_ZN2v88internal5Scope24DeclareCatchVariableNameEPKNS0_12AstRawStringE, .-_ZN2v88internal5Scope24DeclareCatchVariableNameEPKNS0_12AstRawStringE
	.section	.text._ZN2v88internal16DeclarationScope20DeclareDynamicGlobalEPKNS0_12AstRawStringENS0_12VariableKindEPNS0_5ScopeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16DeclarationScope20DeclareDynamicGlobalEPKNS0_12AstRawStringENS0_12VariableKindEPNS0_5ScopeE
	.type	_ZN2v88internal16DeclarationScope20DeclareDynamicGlobalEPKNS0_12AstRawStringENS0_12VariableKindEPNS0_5ScopeE, @function
_ZN2v88internal16DeclarationScope20DeclareDynamicGlobalEPKNS0_12AstRawStringENS0_12VariableKindEPNS0_5ScopeE:
.LFB21252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	24(%rsi), %edx
	movl	40(%rcx), %eax
	movq	(%rdi), %r15
	movq	32(%rcx), %r8
	shrl	$2, %edx
	leal	-1(%rax), %esi
	movl	%edx, %eax
	andl	%esi, %eax
	jmp	.L2648
	.p2align 4,,10
	.p2align 3
.L2649:
	cmpq	%rdi, %r12
	je	.L2624
	addq	$1, %rax
	andq	%rsi, %rax
.L2648:
	leaq	(%rax,%rax,2), %rdi
	leaq	(%r8,%rdi,8), %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L2649
	movq	%r12, (%rbx)
	movq	$0, 8(%rbx)
	movl	%edx, 16(%rbx)
	movl	44(%rcx), %eax
	addl	$1, %eax
	movl	%eax, %esi
	movl	%eax, 44(%rcx)
	shrl	$2, %esi
	addl	%esi, %eax
	cmpl	40(%rcx), %eax
	jnb	.L2650
.L2624:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L2651
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2651:
	.cfi_restore_state
	movq	16(%r15), %rax
	movq	24(%r15), %rdx
	subq	%rax, %rdx
	cmpq	$47, %rdx
	jbe	.L2652
	leaq	48(%rax), %rdx
	movq	%rdx, 16(%r15)
.L2631:
	movzbl	%r14b, %r14d
	pxor	%xmm0, %xmm0
	movq	%r13, (%rax)
	sall	$4, %r14d
	movq	%r12, 8(%rax)
	orw	$4101, %r14w
	movq	$-1, 32(%rax)
	movw	%r14w, 40(%rax)
	movups	%xmm0, 16(%rax)
	movq	%rax, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2650:
	.cfi_restore_state
	leaq	32(%rcx), %rdi
	movq	%r15, %rsi
	movl	%edx, -60(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS_8internal20ZoneAllocationPolicyEE6ResizeES6_
	movq	-56(%rbp), %rcx
	movl	-60(%rbp), %edx
	movl	40(%rcx), %eax
	movq	32(%rcx), %rdi
	leal	-1(%rax), %esi
	andl	%esi, %edx
	movl	%edx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rbx
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L2624
	cmpq	%rdx, %r12
	je	.L2624
	movl	%esi, %ecx
	jmp	.L2627
	.p2align 4,,10
	.p2align 3
.L2653:
	cmpq	%rdx, %r12
	je	.L2624
.L2627:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rbx
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L2653
	jmp	.L2624
	.p2align 4,,10
	.p2align 3
.L2652:
	movl	$48, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2631
	.cfi_endproc
.LFE21252:
	.size	_ZN2v88internal16DeclarationScope20DeclareDynamicGlobalEPKNS0_12AstRawStringENS0_12VariableKindEPNS0_5ScopeE, .-_ZN2v88internal16DeclarationScope20DeclareDynamicGlobalEPKNS0_12AstRawStringENS0_12VariableKindEPNS0_5ScopeE
	.section	.text._ZN2v88internal5Scope8NonLocalEPKNS0_12AstRawStringENS0_12VariableModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope8NonLocalEPKNS0_12AstRawStringENS0_12VariableModeE
	.type	_ZN2v88internal5Scope8NonLocalEPKNS0_12AstRawStringENS0_12VariableModeE, @function
_ZN2v88internal5Scope8NonLocalEPKNS0_12AstRawStringENS0_12VariableModeE:
.LFB21288:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	24(%rsi), %edx
	movl	40(%rdi), %eax
	movq	(%rdi), %r15
	movq	32(%rdi), %rdi
	shrl	$2, %edx
	leal	-1(%rax), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	jmp	.L2681
	.p2align 4,,10
	.p2align 3
.L2684:
	cmpq	%rsi, %r13
	je	.L2656
	addq	$1, %rax
	andq	%rcx, %rax
.L2681:
	leaq	(%rax,%rax,2), %rsi
	leaq	(%rdi,%rsi,8), %r12
	movq	(%r12), %rsi
	testq	%rsi, %rsi
	jne	.L2684
	movq	%r13, (%r12)
	movq	$0, 8(%r12)
	movl	%edx, 16(%r12)
	movl	44(%rbx), %eax
	addl	$1, %eax
	movl	%eax, %ecx
	movl	%eax, 44(%rbx)
	shrl	$2, %ecx
	addl	%ecx, %eax
	cmpl	40(%rbx), %eax
	jnb	.L2685
.L2656:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L2686
.L2661:
	movzwl	40(%rax), %edx
	movl	$-1, 32(%rax)
	andw	$-897, %dx
	orb	$2, %dh
	movw	%dx, 40(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2686:
	.cfi_restore_state
	movq	16(%r15), %rax
	movq	24(%r15), %rdx
	subq	%rax, %rdx
	cmpq	$47, %rdx
	jbe	.L2687
	leaq	48(%rax), %rdx
	movq	%rdx, 16(%r15)
.L2663:
	movq	%rbx, %xmm0
	movq	%r13, %xmm1
	movzbl	%r14b, %r14d
	movq	$-1, 32(%rax)
	punpcklqdq	%xmm1, %xmm0
	orw	$4096, %r14w
	movups	%xmm0, (%rax)
	pxor	%xmm0, %xmm0
	movw	%r14w, 40(%rax)
	movups	%xmm0, 16(%rax)
	movq	%rax, 8(%r12)
	jmp	.L2661
	.p2align 4,,10
	.p2align 3
.L2685:
	movq	%r15, %rsi
	leaq	32(%rbx), %rdi
	movl	%edx, -52(%rbp)
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS_8internal20ZoneAllocationPolicyEE6ResizeES6_
	movl	40(%rbx), %eax
	movl	-52(%rbp), %edx
	movq	32(%rbx), %rsi
	leal	-1(%rax), %ecx
	andl	%ecx, %edx
	movl	%edx, %eax
	jmp	.L2683
	.p2align 4,,10
	.p2align 3
.L2688:
	testq	%rdx, %rdx
	je	.L2656
	addq	$1, %rax
	andq	%rcx, %rax
.L2683:
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %r12
	movq	(%r12), %rdx
	cmpq	%rdx, %r13
	jne	.L2688
	jmp	.L2656
	.p2align 4,,10
	.p2align 3
.L2687:
	movl	$48, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2663
	.cfi_endproc
.LFE21288:
	.size	_ZN2v88internal5Scope8NonLocalEPKNS0_12AstRawStringENS0_12VariableModeE, .-_ZN2v88internal5Scope8NonLocalEPKNS0_12AstRawStringENS0_12VariableModeE
	.section	.text._ZN2v88internal5Scope15DeclareVariableEPNS0_11DeclarationEPKNS0_12AstRawStringEiNS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagEPbSA_SA_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Scope15DeclareVariableEPNS0_11DeclarationEPKNS0_12AstRawStringEiNS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagEPbSA_SA_
	.type	_ZN2v88internal5Scope15DeclareVariableEPNS0_11DeclarationEPKNS0_12AstRawStringEiNS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagEPbSA_SA_, @function
_ZN2v88internal5Scope15DeclareVariableEPNS0_11DeclarationEPKNS0_12AstRawStringEiNS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagEPbSA_SA_:
.LFB21248:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$40, %rsp
	movl	16(%rbp), %r11d
	movq	24(%rbp), %r14
	movq	32(%rbp), %r15
	movq	40(%rbp), %r8
	cmpb	$2, %bl
	je	.L2769
.L2690:
	movl	40(%r12), %eax
	movq	32(%r12), %rdi
	leal	-1(%rax), %esi
	movl	24(%r10), %eax
	shrl	$2, %eax
	andl	%esi, %eax
	jmp	.L2768
	.p2align 4,,10
	.p2align 3
.L2692:
	cmpq	%rdx, %r10
	je	.L2694
	addq	$1, %rax
	andq	%rsi, %rax
.L2768:
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.L2692
	movb	$1, (%r14)
	jmp	.L2712
	.p2align 4,,10
	.p2align 3
.L2694:
	movq	8(%rcx), %rax
	testq	%rax, %rax
	sete	(%r14)
	jne	.L2770
.L2712:
	cmpb	$1, 128(%r12)
	je	.L2771
.L2695:
	subq	$8, %rsp
	movzbl	%r11b, %r11d
	leaq	32(%r12), %rdi
	movl	%ebx, %r8d
	pushq	%r14
	movzbl	%r9b, %r9d
	movq	%r10, %rcx
	movq	%r12, %rdx
	pushq	$0
	pushq	%r11
	movq	(%r12), %rsi
	call	_ZN2v88internal11VariableMap7DeclareEPNS0_4ZoneEPNS0_5ScopeEPKNS0_12AstRawStringENS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagENS0_17MaybeAssignedFlagEPb
	addq	$32, %rsp
	cmpb	$0, (%r14)
	je	.L2698
	movq	64(%r12), %rdx
	movq	%rax, (%rdx)
	leaq	24(%rax), %rdx
	movq	%rdx, 64(%r12)
.L2698:
	movzbl	128(%r12), %edi
	leal	-3(%rdi), %edx
	cmpb	$1, %dl
	ja	.L2699
	movzwl	40(%rax), %edx
	cmpb	$1, %bl
	je	.L2701
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L2701
	movq	16(%rax), %rbx
	testq	%rbx, %rbx
	je	.L2703
	testb	$64, %dh
	je	.L2772
.L2703:
	orb	$64, %dh
.L2701:
	orb	$8, %dh
	movw	%dx, 40(%rax)
.L2699:
	movq	96(%r12), %rdx
	movq	%r13, (%rdx)
	leaq	16(%r13), %rdx
	movq	%rdx, 96(%r12)
	movq	%rax, 8(%r13)
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2769:
	.cfi_restore_state
	testb	$1, 130(%rdi)
	jne	.L2690
	.p2align 4,,10
	.p2align 3
.L2691:
	movq	8(%r12), %r12
	testb	$1, 130(%r12)
	je	.L2691
	movzbl	%r11b, %r11d
	movq	%r8, 40(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r15, 32(%rbp)
	movzbl	%r9b, %r9d
	movl	$2, %r8d
	movq	%r10, %rdx
	movq	%r14, 24(%rbp)
	movl	%r11d, 16(%rbp)
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal5Scope15DeclareVariableEPNS0_11DeclarationEPKNS0_12AstRawStringEiNS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagEPbSA_SA_
	.p2align 4,,10
	.p2align 3
.L2770:
	.cfi_restore_state
	movzwl	40(%rax), %edx
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L2707
	movq	16(%rax), %r14
	testq	%r14, %r14
	je	.L2708
	testb	$64, %dh
	jne	.L2708
	movzwl	40(%r14), %ecx
	movl	%ecx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L2708
	movq	16(%r14), %rsi
	testq	%rsi, %rsi
	je	.L2709
	testb	$64, %ch
	je	.L2773
.L2709:
	orb	$64, %ch
	movw	%cx, 40(%r14)
	movzwl	40(%rax), %edx
.L2708:
	orb	$64, %dh
	movw	%dx, 40(%rax)
.L2707:
	cmpb	$1, %bl
	jbe	.L2711
	testb	$14, %dl
	jne	.L2699
.L2711:
	shrb	$4, %dl
	andl	$7, %edx
	cmpb	$3, %dl
	sete	%dl
	cmpb	$3, %r9b
	sete	%cl
	andl	%ecx, %edx
	movb	%dl, (%r8)
	movb	%dl, (%r15)
	jmp	.L2699
.L2771:
	testb	$1, 129(%r12)
	jne	.L2695
	cmpb	$2, %bl
	jne	.L2695
	movl	$4, %edx
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal5Scope8NonLocalEPKNS0_12AstRawStringENS0_12VariableModeE
	orw	$2048, 40(%rax)
	jmp	.L2699
	.p2align 4,,10
	.p2align 3
.L2772:
	movzwl	40(%rbx), %ecx
	movl	%ecx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L2703
	movq	16(%rbx), %r14
	testq	%r14, %r14
	je	.L2704
	testb	$64, %ch
	je	.L2774
.L2704:
	orb	$64, %ch
	movw	%cx, 40(%rbx)
	movzwl	40(%rax), %edx
	jmp	.L2703
.L2774:
	movzwl	40(%r14), %edx
	movl	%edx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L2704
	movq	16(%r14), %r15
	testq	%r15, %r15
	je	.L2705
	testb	$64, %dh
	je	.L2775
.L2705:
	orb	$64, %dh
	movw	%dx, 40(%r14)
	movzwl	40(%rbx), %ecx
	jmp	.L2704
.L2773:
	movzwl	40(%rsi), %edx
	movl	%edx, %edi
	andl	$15, %edi
	cmpb	$1, %dil
	je	.L2709
	movq	16(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L2710
	testb	$64, %dh
	je	.L2776
.L2710:
	orb	$64, %dh
	movw	%dx, 40(%rsi)
	movzwl	40(%r14), %ecx
	jmp	.L2709
.L2775:
	movzwl	40(%r15), %ecx
	movl	%ecx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L2705
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2706
	testb	$64, %ch
	je	.L2777
.L2706:
	orb	$64, %ch
	movw	%cx, 40(%r15)
	movzwl	40(%r14), %edx
	jmp	.L2705
.L2776:
	movq	%r8, -80(%rbp)
	movl	%r9d, -68(%rbp)
	movq	%rax, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movq	-56(%rbp), %rsi
	movq	-80(%rbp), %r8
	movl	-68(%rbp), %r9d
	movq	-64(%rbp), %rax
	movzwl	40(%rsi), %edx
	jmp	.L2710
.L2777:
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movzwl	40(%r15), %ecx
	movq	-56(%rbp), %rax
	jmp	.L2706
	.cfi_endproc
.LFE21248:
	.size	_ZN2v88internal5Scope15DeclareVariableEPNS0_11DeclarationEPKNS0_12AstRawStringEiNS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagEPbSA_SA_, .-_ZN2v88internal5Scope15DeclareVariableEPNS0_11DeclarationEPKNS0_12AstRawStringEiNS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagEPbSA_SA_
	.section	.text._ZN2v88internal16DeclarationScope25HoistSloppyBlockFunctionsEPNS0_14AstNodeFactoryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16DeclarationScope25HoistSloppyBlockFunctionsEPNS0_14AstNodeFactoryE
	.type	_ZN2v88internal16DeclarationScope25HoistSloppyBlockFunctionsEPNS0_14AstNodeFactoryE, @function
_ZN2v88internal16DeclarationScope25HoistSloppyBlockFunctionsEPNS0_14AstNodeFactoryE:
.LFB21221:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -96(%rbp)
	movq	160(%rdi), %r12
	movq	%rsi, -72(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	testq	%r12, %r12
	je	.L2778
	movq	%rdi, %rax
	jmp	.L2783
	.p2align 4,,10
	.p2align 3
.L2780:
	movq	8(%rax), %rax
.L2783:
	testb	$1, 130(%rax)
	je	.L2780
	movzbl	128(%rax), %edx
	testb	%dl, %dl
	je	.L2780
	cmpb	$6, %dl
	je	.L2780
	movq	-96(%rbp), %r14
	movq	8(%r14), %rbx
	cmpb	$2, %dl
	jne	.L2784
	testb	$1, 131(%rax)
	cmove	%rbx, %r14
.L2784:
	movq	-96(%rbp), %rax
	cmpb	$1, 128(%rax)
	je	.L2911
	jmp	.L2785
	.p2align 4,,10
	.p2align 3
.L2786:
	movq	8(%rbx), %rbx
.L2911:
	testb	$1, 130(%rbx)
	je	.L2786
	movzbl	128(%rbx), %eax
	movq	8(%rbx), %rbx
	cmpb	$1, %al
	je	.L2911
.L2785:
	movq	-96(%rbp), %rsi
	leaq	160(%rsi), %rax
	movq	168(%rsi), %rsi
	movq	%rax, -80(%rbp)
	movq	%rsi, -88(%rbp)
	cmpq	%rax, %rsi
	je	.L2778
	leaq	-57(%rbp), %rax
	movq	%rax, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L2828:
	movq	8(%r12), %r9
	movl	40(%r14), %esi
	movq	32(%r14), %r8
	movq	8(%r9), %r13
	leal	-1(%rsi), %edi
	movl	24(%r13), %eax
	movl	%eax, %edx
	shrl	$2, %edx
	andl	%edi, %edx
	jmp	.L2912
	.p2align 4,,10
	.p2align 3
.L2915:
	cmpq	%rcx, %r13
	je	.L2790
	addq	$1, %rdx
	andq	%rdi, %rdx
.L2912:
	leaq	(%rdx,%rdx,2), %rcx
	leaq	(%r8,%rcx,8), %rsi
	movq	(%rsi), %rcx
	testq	%rcx, %rcx
	jne	.L2915
.L2789:
	movq	(%r9), %rdx
	movq	8(%rdx), %r15
	.p2align 4,,10
	.p2align 3
.L2798:
	movl	40(%r15), %edi
	shrl	$2, %eax
	movq	32(%r15), %rsi
	leal	-1(%rdi), %edx
	andl	%edx, %eax
	jmp	.L2913
	.p2align 4,,10
	.p2align 3
.L2916:
	cmpq	%rcx, %r13
	je	.L2794
	addq	$1, %rax
	andq	%rdx, %rax
.L2913:
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rsi,%rcx,8), %rdi
	movq	(%rdi), %rcx
	testq	%rcx, %rcx
	jne	.L2916
.L2909:
	cmpq	$0, 104(%r15)
	je	.L2796
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal5Scope17LookupInScopeInfoEPKNS0_12AstRawStringEPS1_
	testq	%rax, %rax
	je	.L2796
	testb	$14, 40(%rax)
	je	.L2823
.L2796:
	movq	8(%r15), %r15
	cmpq	%rbx, %r15
	je	.L2797
	movl	24(%r13), %eax
	jmp	.L2798
	.p2align 4,,10
	.p2align 3
.L2794:
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L2909
	testb	$14, 40(%rax)
	jne	.L2796
.L2823:
	movq	-80(%rbp), %rax
	movq	(%rax), %r12
.L2929:
	leaq	24(%r12), %rax
	movq	%rax, -80(%rbp)
	cmpq	%rax, -88(%rbp)
	jne	.L2917
.L2778:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2918
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2790:
	.cfi_restore_state
	movq	8(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L2789
	movzwl	40(%rdx), %edx
	sarl	$4, %edx
	andl	$7, %edx
	cmpb	$1, %dl
	jne	.L2789
.L2792:
	leaq	24(%r12), %rax
	movq	%rax, -80(%rbp)
	cmpq	%rax, -88(%rbp)
	je	.L2778
.L2917:
	movq	24(%r12), %r12
	jmp	.L2828
	.p2align 4,,10
	.p2align 3
.L2797:
	cmpq	$0, -72(%rbp)
	je	.L2919
	movq	-72(%rbp), %rax
	movl	(%r12), %r15d
	movb	$1, -58(%rbp)
	movq	(%rax), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$23, %rax
	jbe	.L2920
	leaq	24(%rsi), %rax
	movq	%rax, 16(%rdi)
.L2800:
	leaq	-58(%rbp), %rax
	movl	%r15d, (%rsi)
	movq	-96(%rbp), %rdi
	movq	%r13, %rdx
	movq	$0, 16(%rsi)
	movl	$2, %r8d
	xorl	%r9d, %r9d
	movl	%r15d, %ecx
	movl	$0, 4(%rsi)
	pushq	%rax
	pushq	$0
	pushq	-120(%rbp)
	pushq	$1
	call	_ZN2v88internal5Scope15DeclareVariableEPNS0_11DeclarationEPKNS0_12AstRawStringEiNS0_12VariableModeENS0_12VariableKindENS0_18InitializationFlagEPbSA_SA_
	movq	8(%r12), %r10
	addq	$32, %rsp
	movq	%rax, %r13
	movq	-72(%rbp), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	subq	%r8, %rax
	cmpq	$23, %rax
	jbe	.L2921
	leaq	24(%r8), %rax
	movq	%rax, 16(%rdi)
.L2802:
	movq	%r8, %rdi
	movl	$-1, %edx
	movq	%r10, %rsi
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal13VariableProxyC1EPNS0_8VariableEi@PLT
	movq	-72(%rbp), %rax
	movq	-104(%rbp), %r8
	movq	(%rax), %rdi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	$23, %rax
	jbe	.L2922
	leaq	24(%rcx), %rax
	movq	%rax, 16(%rdi)
.L2804:
	movl	$-1, %edx
	movq	%r13, %rsi
	movq	%rcx, %rdi
	movq	%r8, -112(%rbp)
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal13VariableProxyC1EPNS0_8VariableEi@PLT
	movl	4(%r12), %r13d
	movq	-104(%rbp), %rcx
	movq	-112(%rbp), %r8
	shrl	$6, %r13d
	cmpb	$16, %r13b
	movzbl	%r13b, %edx
	je	.L2923
	movl	4(%rcx), %esi
	movl	%esi, %edi
	andl	$63, %edi
	cmpb	$54, %dil
	je	.L2924
.L2808:
	movq	-72(%rbp), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r11
	movq	24(%rdi), %rsi
	subq	%r11, %rsi
	cmpb	$17, %r13b
	je	.L2806
	leal	1(%r15), %r9d
	addl	$16, %r13d
	cmpq	$23, %rsi
	jbe	.L2925
	leaq	24(%r11), %rax
	movq	%rax, 16(%rdi)
.L2818:
	movzbl	%r13b, %eax
	movq	%rcx, %xmm0
	movq	%r8, %xmm1
	movl	%r9d, (%r11)
	sall	$7, %eax
	punpcklqdq	%xmm1, %xmm0
	orl	$26, %eax
	movups	%xmm0, 8(%r11)
	movl	%eax, 4(%r11)
	movq	-72(%rbp), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r10
	movq	24(%rdi), %rax
	subq	%r10, %rax
	cmpq	$31, %rax
	jbe	.L2926
	leaq	32(%r10), %rax
	movq	%rax, 16(%rdi)
.L2820:
	movl	%r15d, %r9d
	movl	$33, %esi
	movq	%r10, %rdi
	movq	%r11, -104(%rbp)
	movq	%r10, %r13
	call	_ZN2v88internal10AssignmentC2ENS0_7AstNode8NodeTypeENS0_5Token5ValueEPNS0_10ExpressionES7_i@PLT
	movq	-104(%rbp), %r11
	movq	%r11, 24(%r13)
.L2816:
	movq	-72(%rbp), %rax
	orl	$16384, 4(%r13)
	movq	(%rax), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L2927
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L2822:
	movl	%r15d, (%rax)
	movl	$9, 4(%rax)
	movq	%r13, 8(%rax)
	movq	%rax, 16(%r12)
	movq	-80(%rbp), %rax
	movq	(%rax), %r12
	jmp	.L2792
	.p2align 4,,10
	.p2align 3
.L2919:
	movq	-120(%rbp), %rcx
	movq	-96(%rbp), %rdi
	movl	$2, %edx
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	call	_ZN2v88internal5Scope19DeclareVariableNameEPKNS0_12AstRawStringENS0_12VariableModeEPbNS0_12VariableKindE
	movl	4(%r12), %edx
	shrl	$6, %edx
	cmpb	$17, %dl
	jne	.L2823
	movzwl	40(%rax), %edx
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L2823
	movq	16(%rax), %r12
	testq	%r12, %r12
	je	.L2824
	testb	$64, %dh
	je	.L2928
.L2824:
	orb	$64, %dh
	movw	%dx, 40(%rax)
	movq	-80(%rbp), %rax
	movq	(%rax), %r12
	jmp	.L2929
	.p2align 4,,10
	.p2align 3
.L2923:
	movq	-72(%rbp), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r11
	movq	24(%rdi), %rsi
	subq	%r11, %rsi
.L2806:
	cmpq	$23, %rsi
	jbe	.L2930
	leaq	24(%r11), %rax
	movq	%rax, 16(%rdi)
.L2815:
	movl	%r15d, %r9d
	movl	$24, %esi
	movq	%r11, %rdi
	movq	%r11, %r13
	call	_ZN2v88internal10AssignmentC1ENS0_7AstNode8NodeTypeENS0_5Token5ValueEPNS0_10ExpressionES7_i@PLT
	jmp	.L2816
.L2924:
	movl	%esi, %edi
	orb	$-128, %dil
	andl	$256, %esi
	movl	%edi, 4(%rcx)
	je	.L2808
	movq	8(%rcx), %rsi
	movzwl	40(%rsi), %edi
	movl	%edi, %r9d
	andl	$15, %r9d
	cmpb	$1, %r9b
	je	.L2808
	movq	16(%rsi), %r9
	testq	%r9, %r9
	je	.L2810
	testw	$16384, %di
	je	.L2931
.L2810:
	orw	$16384, %di
	movw	%di, 40(%rsi)
	jmp	.L2808
.L2927:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2822
.L2922:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-104(%rbp), %r8
	movq	%rax, %rcx
	jmp	.L2804
.L2921:
	movl	$24, %esi
	movq	%r10, -104(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-104(%rbp), %r10
	movq	%rax, %r8
	jmp	.L2802
.L2920:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rsi
	jmp	.L2800
.L2926:
	movl	$32, %esi
	movq	%r11, -136(%rbp)
	movq	%r8, -128(%rbp)
	movq	%rcx, -112(%rbp)
	movl	%edx, -104(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-104(%rbp), %edx
	movq	-112(%rbp), %rcx
	movq	-128(%rbp), %r8
	movq	-136(%rbp), %r11
	movq	%rax, %r10
	jmp	.L2820
.L2925:
	movl	$24, %esi
	movq	%r8, -136(%rbp)
	movq	%rcx, -128(%rbp)
	movl	%r9d, -112(%rbp)
	movl	%edx, -104(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-104(%rbp), %edx
	movl	-112(%rbp), %r9d
	movq	-128(%rbp), %rcx
	movq	-136(%rbp), %r8
	movq	%rax, %r11
	jmp	.L2818
.L2930:
	movl	$24, %esi
	movq	%r8, -128(%rbp)
	movq	%rcx, -112(%rbp)
	movl	%edx, -104(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-104(%rbp), %edx
	movq	-112(%rbp), %rcx
	movq	-128(%rbp), %r8
	movq	%rax, %r11
	jmp	.L2815
.L2928:
	movzwl	40(%r12), %ecx
	movl	%ecx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L2824
	movq	16(%r12), %r15
	testq	%r15, %r15
	je	.L2825
	testb	$64, %ch
	je	.L2932
.L2825:
	orb	$64, %ch
	movw	%cx, 40(%r12)
	movzwl	40(%rax), %edx
	jmp	.L2824
.L2931:
	movzwl	40(%r9), %eax
	movl	%eax, %r10d
	andl	$15, %r10d
	cmpb	$1, %r10b
	je	.L2810
	movq	16(%r9), %r10
	testq	%r10, %r10
	je	.L2811
	testb	$64, %ah
	je	.L2933
.L2811:
	orb	$64, %ah
	movw	%ax, 40(%r9)
	movzwl	40(%rsi), %edi
	jmp	.L2810
.L2933:
	movzwl	40(%r10), %edi
	movl	%edi, %r11d
	andl	$15, %r11d
	cmpb	$1, %r11b
	je	.L2811
	movq	16(%r10), %r11
	testq	%r11, %r11
	je	.L2812
	testw	$16384, %di
	je	.L2934
.L2812:
	orw	$16384, %di
	movw	%di, 40(%r10)
	movzwl	40(%r9), %eax
	jmp	.L2811
.L2932:
	movzwl	40(%r15), %edx
	movl	%edx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L2825
	movq	16(%r15), %r13
	testq	%r13, %r13
	je	.L2826
	testb	$64, %dh
	je	.L2935
.L2826:
	orb	$64, %dh
	movw	%dx, 40(%r15)
	movzwl	40(%r12), %ecx
	jmp	.L2825
.L2935:
	movzwl	40(%r13), %ecx
	movl	%ecx, %edi
	andl	$15, %edi
	cmpb	$1, %dil
	je	.L2826
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2827
	testb	$64, %ch
	je	.L2936
.L2827:
	orb	$64, %ch
	movw	%cx, 40(%r13)
	movzwl	40(%r15), %edx
	jmp	.L2826
.L2936:
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movzwl	40(%r13), %ecx
	movq	-104(%rbp), %rax
	jmp	.L2827
.L2934:
	movq	%r11, %rdi
	movq	%r10, -152(%rbp)
	movq	%r9, -144(%rbp)
	movq	%r8, -136(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%rsi, -112(%rbp)
	movl	%edx, -104(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movq	-152(%rbp), %r10
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %r8
	movq	-128(%rbp), %rcx
	movzwl	40(%r10), %edi
	movq	-112(%rbp), %rsi
	movl	-104(%rbp), %edx
	jmp	.L2812
.L2918:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21221:
	.size	_ZN2v88internal16DeclarationScope25HoistSloppyBlockFunctionsEPNS0_14AstNodeFactoryE, .-_ZN2v88internal16DeclarationScope25HoistSloppyBlockFunctionsEPNS0_14AstNodeFactoryE
	.section	.text._ZN2v88internal16DeclarationScope7AnalyzeEPNS0_9ParseInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16DeclarationScope7AnalyzeEPNS0_9ParseInfoE
	.type	_ZN2v88internal16DeclarationScope7AnalyzeEPNS0_9ParseInfoE, @function
_ZN2v88internal16DeclarationScope7AnalyzeEPNS0_9ParseInfoE:
.LFB21222:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	8(%rdi), %eax
	movq	136(%rdi), %rdi
	movaps	%xmm0, -128(%rbp)
	movq	$0, -96(%rbp)
	andl	$8192, %eax
	movaps	%xmm0, -112(%rbp)
	cmpl	$1, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	sbbl	%edx, %edx
	andl	$13, %edx
	addl	$123, %edx
	testq	%rdi, %rdi
	je	.L2939
	testl	%eax, %eax
	jne	.L2958
.L2939:
	movq	168(%r12), %rax
	movq	40(%rax), %r13
	cmpb	$1, 128(%r13)
	je	.L2959
.L2940:
	movzbl	131(%r13), %eax
	movl	%eax, %edx
	andl	$-65, %eax
	shrb	$7, %dl
	xorl	$1, %edx
	sall	$6, %edx
	orl	%edx, %eax
	movb	%al, 131(%r13)
	testb	$2, 130(%r13)
	je	.L2947
	movq	104(%r12), %rdi
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
.L2947:
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal16DeclarationScope17AllocateVariablesEPNS0_9ParseInfoE
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2960
.L2937:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2961
	addq	$120, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2959:
	.cfi_restore_state
	testb	$1, 129(%r13)
	jne	.L2940
	movq	(%r12), %r14
	movq	112(%r12), %rax
	movq	24(%r14), %rdx
	movq	%rax, -72(%rbp)
	movq	16(%r14), %rax
	movq	%r14, -80(%rbp)
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jbe	.L2962
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%r14)
.L2942:
	movabsq	$47244640255, %rcx
	movq	%rcx, (%rax)
	movq	24(%r14), %rdx
	movq	%rax, -64(%rbp)
	movq	16(%r14), %rax
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jbe	.L2963
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%r14)
.L2944:
	movabsq	$223338299391, %rcx
	movq	%rcx, (%rax)
	movq	%rax, -56(%rbp)
	movq	16(%r14), %rax
	movq	24(%r14), %rdx
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jbe	.L2964
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%r14)
.L2946:
	movabsq	$249108103167, %rcx
	leaq	-80(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rcx, (%rax)
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal16DeclarationScope25HoistSloppyBlockFunctionsEPNS0_14AstNodeFactoryE
	jmp	.L2940
	.p2align 4,,10
	.p2align 3
.L2960:
	leaq	-120(%rbp), %rsi
	movb	%al, -129(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movzbl	-129(%rbp), %eax
	jmp	.L2937
.L2958:
	leaq	-120(%rbp), %rsi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2939
	.p2align 4,,10
	.p2align 3
.L2964:
	movl	$8, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2946
	.p2align 4,,10
	.p2align 3
.L2963:
	movl	$8, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2944
	.p2align 4,,10
	.p2align 3
.L2962:
	movl	$8, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2942
.L2961:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21222:
	.size	_ZN2v88internal16DeclarationScope7AnalyzeEPNS0_9ParseInfoE, .-_ZN2v88internal16DeclarationScope7AnalyzeEPNS0_9ParseInfoE
	.section	.text._ZN2v88internal16DeclarationScope16DeclareArgumentsEPNS0_15AstValueFactoryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16DeclarationScope16DeclareArgumentsEPNS0_15AstValueFactoryE
	.type	_ZN2v88internal16DeclarationScope16DeclareArgumentsEPNS0_15AstValueFactoryE, @function
_ZN2v88internal16DeclarationScope16DeclareArgumentsEPNS0_15AstValueFactoryE:
.LFB21236:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	56(%rsi), %rax
	movq	(%rdi), %r14
	movq	32(%rdi), %rsi
	movq	112(%rax), %r13
	movl	40(%rdi), %eax
	movl	24(%r13), %r12d
	leal	-1(%rax), %edx
	shrl	$2, %r12d
	movl	%r12d, %eax
	andl	%edx, %eax
	jmp	.L2993
	.p2align 4,,10
	.p2align 3
.L2994:
	cmpq	%rcx, %r13
	je	.L2967
	addq	$1, %rax
	andq	%rdx, %rax
.L2993:
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rsi,%rcx,8), %r15
	movq	(%r15), %rcx
	testq	%rcx, %rcx
	jne	.L2994
	movq	%r13, (%r15)
	movq	$0, 8(%r15)
	movl	%r12d, 16(%r15)
	movl	44(%rbx), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 44(%rbx)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	40(%rbx), %eax
	jnb	.L2995
.L2967:
	movq	8(%r15), %rax
	testq	%rax, %rax
	je	.L2996
	movq	%rax, 200(%rbx)
	movl	$0, %edx
	testb	$14, 40(%rax)
	cmove	%rdx, %rax
	movq	%rax, 200(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2996:
	.cfi_restore_state
	movq	16(%r14), %rax
	movq	24(%r14), %rdx
	subq	%rax, %rdx
	cmpq	$47, %rdx
	jbe	.L2997
	leaq	48(%rax), %rdx
	movq	%rdx, 16(%r14)
.L2974:
	movl	$4098, %edx
	pxor	%xmm0, %xmm0
	movq	%rbx, (%rax)
	movq	%r13, 8(%rax)
	movq	$-1, 32(%rax)
	movw	%dx, 40(%rax)
	movups	%xmm0, 16(%rax)
	movq	%rax, 8(%r15)
	movq	64(%rbx), %rdx
	movq	%rax, (%rdx)
	leaq	24(%rax), %rdx
	movq	%rdx, 64(%rbx)
	movq	%rax, 200(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2995:
	.cfi_restore_state
	movq	%r14, %rsi
	leaq	32(%rbx), %rdi
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS_8internal20ZoneAllocationPolicyEE6ResizeES6_
	movl	40(%rbx), %eax
	movq	32(%rbx), %rsi
	leal	-1(%rax), %ecx
	andl	%ecx, %r12d
	movl	%r12d, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %r15
	movq	(%r15), %rdx
	cmpq	%rdx, %r13
	je	.L2967
	testq	%rdx, %rdx
	jne	.L2970
	jmp	.L2967
	.p2align 4,,10
	.p2align 3
.L2998:
	cmpq	%rdx, %r13
	je	.L2967
.L2970:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %r15
	movq	(%r15), %rdx
	testq	%rdx, %rdx
	jne	.L2998
	jmp	.L2967
	.p2align 4,,10
	.p2align 3
.L2997:
	movl	$48, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2974
	.cfi_endproc
.LFE21236:
	.size	_ZN2v88internal16DeclarationScope16DeclareArgumentsEPNS0_15AstValueFactoryE, .-_ZN2v88internal16DeclarationScope16DeclareArgumentsEPNS0_15AstValueFactoryE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11VariableMapC2EPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11VariableMapC2EPNS0_4ZoneE, @function
_GLOBAL__sub_I__ZN2v88internal11VariableMapC2EPNS0_4ZoneE:
.LFB27663:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE27663:
	.size	_GLOBAL__sub_I__ZN2v88internal11VariableMapC2EPNS0_4ZoneE, .-_GLOBAL__sub_I__ZN2v88internal11VariableMapC2EPNS0_4ZoneE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11VariableMapC2EPNS0_4ZoneE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	-1
	.long	-1
	.long	0
	.long	4
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
