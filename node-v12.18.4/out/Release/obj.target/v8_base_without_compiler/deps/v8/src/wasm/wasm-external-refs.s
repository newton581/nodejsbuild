	.file	"wasm-external-refs.cc"
	.text
	.section	.text._ZN2v88internal4wasm17f32_trunc_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm17f32_trunc_wrapperEm
	.type	_ZN2v88internal4wasm17f32_trunc_wrapperEm, @function
_ZN2v88internal4wasm17f32_trunc_wrapperEm:
.LFB5014:
	.cfi_startproc
	endbr64
	movl	(%rdi), %edx
	movss	.LC1(%rip), %xmm1
	movss	.LC0(%rip), %xmm3
	movd	%edx, %xmm2
	movd	%edx, %xmm0
	andps	%xmm1, %xmm2
	ucomiss	%xmm2, %xmm3
	jbe	.L2
	cvttss2sil	%xmm0, %eax
	pxor	%xmm0, %xmm0
	movd	%edx, %xmm2
	andnps	%xmm2, %xmm1
	cvtsi2ssl	%eax, %xmm0
	orps	%xmm1, %xmm0
.L2:
	movd	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE5014:
	.size	_ZN2v88internal4wasm17f32_trunc_wrapperEm, .-_ZN2v88internal4wasm17f32_trunc_wrapperEm
	.section	.text._ZN2v88internal4wasm17f32_floor_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm17f32_floor_wrapperEm
	.type	_ZN2v88internal4wasm17f32_floor_wrapperEm, @function
_ZN2v88internal4wasm17f32_floor_wrapperEm:
.LFB5015:
	.cfi_startproc
	endbr64
	movl	(%rdi), %edx
	movss	.LC1(%rip), %xmm2
	movss	.LC0(%rip), %xmm3
	movd	%edx, %xmm1
	movd	%edx, %xmm0
	andps	%xmm2, %xmm1
	ucomiss	%xmm1, %xmm3
	jbe	.L5
	cvttss2sil	%xmm0, %eax
	pxor	%xmm1, %xmm1
	movss	.LC2(%rip), %xmm3
	cvtsi2ssl	%eax, %xmm1
	movaps	%xmm1, %xmm4
	cmpnless	%xmm0, %xmm4
	movaps	%xmm4, %xmm0
	andps	%xmm3, %xmm0
	subss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	movd	%edx, %xmm1
	andnps	%xmm1, %xmm2
	orps	%xmm2, %xmm0
.L5:
	movd	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE5015:
	.size	_ZN2v88internal4wasm17f32_floor_wrapperEm, .-_ZN2v88internal4wasm17f32_floor_wrapperEm
	.section	.text._ZN2v88internal4wasm16f32_ceil_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm16f32_ceil_wrapperEm
	.type	_ZN2v88internal4wasm16f32_ceil_wrapperEm, @function
_ZN2v88internal4wasm16f32_ceil_wrapperEm:
.LFB5016:
	.cfi_startproc
	endbr64
	movl	(%rdi), %edx
	movss	.LC1(%rip), %xmm2
	movss	.LC0(%rip), %xmm3
	movd	%edx, %xmm1
	movd	%edx, %xmm0
	andps	%xmm2, %xmm1
	ucomiss	%xmm1, %xmm3
	jbe	.L7
	cvttss2sil	%xmm0, %eax
	pxor	%xmm1, %xmm1
	movss	.LC2(%rip), %xmm3
	cvtsi2ssl	%eax, %xmm1
	cmpnless	%xmm1, %xmm0
	andps	%xmm3, %xmm0
	addss	%xmm1, %xmm0
	movd	%edx, %xmm1
	andnps	%xmm1, %xmm2
	orps	%xmm2, %xmm0
.L7:
	movd	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE5016:
	.size	_ZN2v88internal4wasm16f32_ceil_wrapperEm, .-_ZN2v88internal4wasm16f32_ceil_wrapperEm
	.section	.text._ZN2v88internal4wasm23f32_nearest_int_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm23f32_nearest_int_wrapperEm
	.type	_ZN2v88internal4wasm23f32_nearest_int_wrapperEm, @function
_ZN2v88internal4wasm23f32_nearest_int_wrapperEm:
.LFB5017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movss	(%rdi), %xmm0
	call	nearbyintf@PLT
	movd	%xmm0, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5017:
	.size	_ZN2v88internal4wasm23f32_nearest_int_wrapperEm, .-_ZN2v88internal4wasm23f32_nearest_int_wrapperEm
	.section	.text._ZN2v88internal4wasm17f64_trunc_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm17f64_trunc_wrapperEm
	.type	_ZN2v88internal4wasm17f64_trunc_wrapperEm, @function
_ZN2v88internal4wasm17f64_trunc_wrapperEm:
.LFB5018:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movsd	.LC4(%rip), %xmm1
	movsd	.LC3(%rip), %xmm3
	movq	%rdx, %xmm2
	movq	%rdx, %xmm0
	andpd	%xmm1, %xmm2
	ucomisd	%xmm2, %xmm3
	jbe	.L11
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm0, %xmm0
	movq	%rdx, %xmm2
	andnpd	%xmm2, %xmm1
	cvtsi2sdq	%rax, %xmm0
	orpd	%xmm1, %xmm0
.L11:
	movq	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE5018:
	.size	_ZN2v88internal4wasm17f64_trunc_wrapperEm, .-_ZN2v88internal4wasm17f64_trunc_wrapperEm
	.section	.text._ZN2v88internal4wasm17f64_floor_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm17f64_floor_wrapperEm
	.type	_ZN2v88internal4wasm17f64_floor_wrapperEm, @function
_ZN2v88internal4wasm17f64_floor_wrapperEm:
.LFB5019:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movsd	.LC4(%rip), %xmm2
	movsd	.LC3(%rip), %xmm3
	movq	%rdx, %xmm1
	movq	%rdx, %xmm0
	andpd	%xmm2, %xmm1
	ucomisd	%xmm1, %xmm3
	jbe	.L13
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm1, %xmm1
	movsd	.LC5(%rip), %xmm3
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm1, %xmm4
	cmpnlesd	%xmm0, %xmm4
	movapd	%xmm4, %xmm0
	andpd	%xmm3, %xmm0
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movq	%rdx, %xmm1
	andnpd	%xmm1, %xmm2
	orpd	%xmm2, %xmm0
.L13:
	movq	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE5019:
	.size	_ZN2v88internal4wasm17f64_floor_wrapperEm, .-_ZN2v88internal4wasm17f64_floor_wrapperEm
	.section	.text._ZN2v88internal4wasm16f64_ceil_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm16f64_ceil_wrapperEm
	.type	_ZN2v88internal4wasm16f64_ceil_wrapperEm, @function
_ZN2v88internal4wasm16f64_ceil_wrapperEm:
.LFB5020:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movsd	.LC4(%rip), %xmm2
	movsd	.LC3(%rip), %xmm3
	movq	%rdx, %xmm1
	movq	%rdx, %xmm0
	andpd	%xmm2, %xmm1
	ucomisd	%xmm1, %xmm3
	jbe	.L15
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm1, %xmm1
	movsd	.LC5(%rip), %xmm3
	cvtsi2sdq	%rax, %xmm1
	cmpnlesd	%xmm1, %xmm0
	andpd	%xmm3, %xmm0
	addsd	%xmm1, %xmm0
	movq	%rdx, %xmm1
	andnpd	%xmm1, %xmm2
	orpd	%xmm2, %xmm0
.L15:
	movq	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE5020:
	.size	_ZN2v88internal4wasm16f64_ceil_wrapperEm, .-_ZN2v88internal4wasm16f64_ceil_wrapperEm
	.section	.text._ZN2v88internal4wasm23f64_nearest_int_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm23f64_nearest_int_wrapperEm
	.type	_ZN2v88internal4wasm23f64_nearest_int_wrapperEm, @function
_ZN2v88internal4wasm23f64_nearest_int_wrapperEm:
.LFB5021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movsd	(%rdi), %xmm0
	call	nearbyint@PLT
	movq	%xmm0, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5021:
	.size	_ZN2v88internal4wasm23f64_nearest_int_wrapperEm, .-_ZN2v88internal4wasm23f64_nearest_int_wrapperEm
	.section	.text._ZN2v88internal4wasm24int64_to_float32_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm24int64_to_float32_wrapperEm
	.type	_ZN2v88internal4wasm24int64_to_float32_wrapperEm, @function
_ZN2v88internal4wasm24int64_to_float32_wrapperEm:
.LFB5022:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	cvtsi2ssq	(%rdi), %xmm0
	movd	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE5022:
	.size	_ZN2v88internal4wasm24int64_to_float32_wrapperEm, .-_ZN2v88internal4wasm24int64_to_float32_wrapperEm
	.section	.text._ZN2v88internal4wasm25uint64_to_float32_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm25uint64_to_float32_wrapperEm
	.type	_ZN2v88internal4wasm25uint64_to_float32_wrapperEm, @function
_ZN2v88internal4wasm25uint64_to_float32_wrapperEm:
.LFB5023:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testq	%rax, %rax
	js	.L20
	pxor	%xmm1, %xmm1
	cvtsi2ssq	%rax, %xmm1
	movd	%xmm1, %eax
	movl	%eax, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2ssq	%rdx, %xmm0
	addss	%xmm0, %xmm0
	movd	%xmm0, %eax
	movl	%eax, (%rdi)
	ret
	.cfi_endproc
.LFE5023:
	.size	_ZN2v88internal4wasm25uint64_to_float32_wrapperEm, .-_ZN2v88internal4wasm25uint64_to_float32_wrapperEm
	.section	.text._ZN2v88internal4wasm24int64_to_float64_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm24int64_to_float64_wrapperEm
	.type	_ZN2v88internal4wasm24int64_to_float64_wrapperEm, @function
_ZN2v88internal4wasm24int64_to_float64_wrapperEm:
.LFB5024:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	cvtsi2sdq	(%rdi), %xmm0
	movq	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE5024:
	.size	_ZN2v88internal4wasm24int64_to_float64_wrapperEm, .-_ZN2v88internal4wasm24int64_to_float64_wrapperEm
	.section	.text._ZN2v88internal4wasm25uint64_to_float64_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm25uint64_to_float64_wrapperEm
	.type	_ZN2v88internal4wasm25uint64_to_float64_wrapperEm, @function
_ZN2v88internal4wasm25uint64_to_float64_wrapperEm:
.LFB5025:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testq	%rax, %rax
	js	.L24
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	movq	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	movq	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE5025:
	.size	_ZN2v88internal4wasm25uint64_to_float64_wrapperEm, .-_ZN2v88internal4wasm25uint64_to_float64_wrapperEm
	.section	.text._ZN2v88internal4wasm24float32_to_int64_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm24float32_to_int64_wrapperEm
	.type	_ZN2v88internal4wasm24float32_to_int64_wrapperEm, @function
_ZN2v88internal4wasm24float32_to_int64_wrapperEm:
.LFB5026:
	.cfi_startproc
	endbr64
	movss	(%rdi), %xmm0
	comiss	.LC6(%rip), %xmm0
	jb	.L35
	movss	.LC7(%rip), %xmm1
	comiss	%xmm0, %xmm1
	ja	.L36
.L35:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	cvttss2siq	%xmm0, %rax
	movq	%rax, (%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE5026:
	.size	_ZN2v88internal4wasm24float32_to_int64_wrapperEm, .-_ZN2v88internal4wasm24float32_to_int64_wrapperEm
	.section	.text._ZN2v88internal4wasm25float32_to_uint64_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm25float32_to_uint64_wrapperEm
	.type	_ZN2v88internal4wasm25float32_to_uint64_wrapperEm, @function
_ZN2v88internal4wasm25float32_to_uint64_wrapperEm:
.LFB5027:
	.cfi_startproc
	endbr64
	movss	(%rdi), %xmm0
	comiss	.LC8(%rip), %xmm0
	jbe	.L48
	movss	.LC9(%rip), %xmm1
	comiss	%xmm0, %xmm1
	ja	.L49
.L48:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	comiss	.LC7(%rip), %xmm0
	jnb	.L41
	cvttss2siq	%xmm0, %rax
	movq	%rax, (%rdi)
.L42:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	subss	.LC7(%rip), %xmm0
	cvttss2siq	%xmm0, %rax
	movq	%rax, (%rdi)
	btcq	$63, (%rdi)
	jmp	.L42
	.cfi_endproc
.LFE5027:
	.size	_ZN2v88internal4wasm25float32_to_uint64_wrapperEm, .-_ZN2v88internal4wasm25float32_to_uint64_wrapperEm
	.section	.text._ZN2v88internal4wasm24float64_to_int64_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm24float64_to_int64_wrapperEm
	.type	_ZN2v88internal4wasm24float64_to_int64_wrapperEm, @function
_ZN2v88internal4wasm24float64_to_int64_wrapperEm:
.LFB5028:
	.cfi_startproc
	endbr64
	movsd	(%rdi), %xmm0
	comisd	.LC10(%rip), %xmm0
	jb	.L59
	movsd	.LC11(%rip), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L60
.L59:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	cvttsd2siq	%xmm0, %rax
	movq	%rax, (%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE5028:
	.size	_ZN2v88internal4wasm24float64_to_int64_wrapperEm, .-_ZN2v88internal4wasm24float64_to_int64_wrapperEm
	.section	.text._ZN2v88internal4wasm25float64_to_uint64_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm25float64_to_uint64_wrapperEm
	.type	_ZN2v88internal4wasm25float64_to_uint64_wrapperEm, @function
_ZN2v88internal4wasm25float64_to_uint64_wrapperEm:
.LFB5029:
	.cfi_startproc
	endbr64
	movsd	(%rdi), %xmm0
	comisd	.LC12(%rip), %xmm0
	jbe	.L72
	movsd	.LC13(%rip), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L73
.L72:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	movsd	.LC11(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L65
	cvttsd2siq	%xmm0, %rax
	movq	%rax, (%rdi)
.L66:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rax
	movq	%rax, (%rdi)
	btcq	$63, (%rdi)
	jmp	.L66
	.cfi_endproc
.LFE5029:
	.size	_ZN2v88internal4wasm25float64_to_uint64_wrapperEm, .-_ZN2v88internal4wasm25float64_to_uint64_wrapperEm
	.section	.text._ZN2v88internal4wasm17int64_div_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm17int64_div_wrapperEm
	.type	_ZN2v88internal4wasm17int64_div_wrapperEm, @function
_ZN2v88internal4wasm17int64_div_wrapperEm:
.LFB5030:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L74
	movq	(%rdi), %rdx
	cmpq	$-1, %rcx
	jne	.L79
	movabsq	$-9223372036854775808, %rsi
	movl	$-1, %eax
	cmpq	%rsi, %rdx
	jne	.L79
.L74:
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	movq	%rdx, %rax
	cqto
	idivq	%rcx
	movq	%rax, (%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE5030:
	.size	_ZN2v88internal4wasm17int64_div_wrapperEm, .-_ZN2v88internal4wasm17int64_div_wrapperEm
	.section	.text._ZN2v88internal4wasm17int64_mod_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm17int64_mod_wrapperEm
	.type	_ZN2v88internal4wasm17int64_mod_wrapperEm, @function
_ZN2v88internal4wasm17int64_mod_wrapperEm:
.LFB5031:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	jne	.L88
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	movq	(%rdi), %rax
	cqto
	idivq	%rcx
	movl	$1, %eax
	movq	%rdx, (%rdi)
	ret
	.cfi_endproc
.LFE5031:
	.size	_ZN2v88internal4wasm17int64_mod_wrapperEm, .-_ZN2v88internal4wasm17int64_mod_wrapperEm
	.section	.text._ZN2v88internal4wasm18uint64_div_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm18uint64_div_wrapperEm
	.type	_ZN2v88internal4wasm18uint64_div_wrapperEm, @function
_ZN2v88internal4wasm18uint64_div_wrapperEm:
.LFB5032:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	jne	.L93
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	movq	(%rdi), %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, (%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE5032:
	.size	_ZN2v88internal4wasm18uint64_div_wrapperEm, .-_ZN2v88internal4wasm18uint64_div_wrapperEm
	.section	.text._ZN2v88internal4wasm18uint64_mod_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm18uint64_mod_wrapperEm
	.type	_ZN2v88internal4wasm18uint64_mod_wrapperEm, @function
_ZN2v88internal4wasm18uint64_mod_wrapperEm:
.LFB5033:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	jne	.L98
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	movq	(%rdi), %rax
	xorl	%edx, %edx
	divq	%rcx
	movl	$1, %eax
	movq	%rdx, (%rdi)
	ret
	.cfi_endproc
.LFE5033:
	.size	_ZN2v88internal4wasm18uint64_mod_wrapperEm, .-_ZN2v88internal4wasm18uint64_mod_wrapperEm
	.section	.text._ZN2v88internal4wasm18word32_ctz_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm18word32_ctz_wrapperEm
	.type	_ZN2v88internal4wasm18word32_ctz_wrapperEm, @function
_ZN2v88internal4wasm18word32_ctz_wrapperEm:
.LFB5034:
	.cfi_startproc
	endbr64
	movl	(%rdi), %edx
	xorl	%eax, %eax
	rep bsfl	%edx, %eax
	testl	%edx, %edx
	movl	$32, %edx
	cmove	%edx, %eax
	ret
	.cfi_endproc
.LFE5034:
	.size	_ZN2v88internal4wasm18word32_ctz_wrapperEm, .-_ZN2v88internal4wasm18word32_ctz_wrapperEm
	.section	.text._ZN2v88internal4wasm18word64_ctz_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm18word64_ctz_wrapperEm
	.type	_ZN2v88internal4wasm18word64_ctz_wrapperEm, @function
_ZN2v88internal4wasm18word64_ctz_wrapperEm:
.LFB5035:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	xorl	%ecx, %ecx
	movl	$64, %eax
	rep bsfq	%rdx, %rcx
	testq	%rdx, %rdx
	cmovne	%ecx, %eax
	ret
	.cfi_endproc
.LFE5035:
	.size	_ZN2v88internal4wasm18word64_ctz_wrapperEm, .-_ZN2v88internal4wasm18word64_ctz_wrapperEm
	.globl	__popcountdi2
	.section	.text._ZN2v88internal4wasm21word32_popcnt_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm21word32_popcnt_wrapperEm
	.type	_ZN2v88internal4wasm21word32_popcnt_wrapperEm, @function
_ZN2v88internal4wasm21word32_popcnt_wrapperEm:
.LFB5036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	(%rdi), %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__popcountdi2@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5036:
	.size	_ZN2v88internal4wasm21word32_popcnt_wrapperEm, .-_ZN2v88internal4wasm21word32_popcnt_wrapperEm
	.section	.text._ZN2v88internal4wasm21word64_popcnt_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm21word64_popcnt_wrapperEm
	.type	_ZN2v88internal4wasm21word64_popcnt_wrapperEm, @function
_ZN2v88internal4wasm21word64_popcnt_wrapperEm:
.LFB5037:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__popcountdi2@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5037:
	.size	_ZN2v88internal4wasm21word64_popcnt_wrapperEm, .-_ZN2v88internal4wasm21word64_popcnt_wrapperEm
	.section	.text._ZN2v88internal4wasm18word32_rol_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm18word32_rol_wrapperEm
	.type	_ZN2v88internal4wasm18word32_rol_wrapperEm, @function
_ZN2v88internal4wasm18word32_rol_wrapperEm:
.LFB5038:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %ecx
	movl	(%rdi), %eax
	roll	%cl, %eax
	ret
	.cfi_endproc
.LFE5038:
	.size	_ZN2v88internal4wasm18word32_rol_wrapperEm, .-_ZN2v88internal4wasm18word32_rol_wrapperEm
	.section	.text._ZN2v88internal4wasm18word32_ror_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm18word32_ror_wrapperEm
	.type	_ZN2v88internal4wasm18word32_ror_wrapperEm, @function
_ZN2v88internal4wasm18word32_ror_wrapperEm:
.LFB5039:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %ecx
	movl	(%rdi), %eax
	rorl	%cl, %eax
	ret
	.cfi_endproc
.LFE5039:
	.size	_ZN2v88internal4wasm18word32_ror_wrapperEm, .-_ZN2v88internal4wasm18word32_ror_wrapperEm
	.section	.text._ZN2v88internal4wasm19float64_pow_wrapperEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm19float64_pow_wrapperEm
	.type	_ZN2v88internal4wasm19float64_pow_wrapperEm, @function
_ZN2v88internal4wasm19float64_pow_wrapperEm:
.LFB5040:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movsd	8(%rdi), %xmm1
	movsd	(%rdi), %xmm0
	call	_ZN2v84base7ieee7543powEdd@PLT
	movq	%xmm0, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5040:
	.size	_ZN2v88internal4wasm19float64_pow_wrapperEm, .-_ZN2v88internal4wasm19float64_pow_wrapperEm
	.section	.text._ZN2v88internal4wasm19memory_copy_wrapperEmmj,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm19memory_copy_wrapperEmmj
	.type	_ZN2v88internal4wasm19memory_copy_wrapperEmmj, @function
_ZN2v88internal4wasm19memory_copy_wrapperEmmj:
.LFB5041:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	cmpq	%rsi, %rdi
	jbe	.L114
	movl	%edx, %ecx
	leaq	(%rcx,%rsi), %r8
	cmpq	%rdi, %r8
	ja	.L115
.L114:
	testl	%edx, %edx
	je	.L180
	leaq	15(%rdi), %rcx
	leal	-1(%rdx), %r8d
	subq	%rsi, %rcx
	cmpq	$30, %rcx
	jbe	.L119
	cmpl	$14, %r8d
	jbe	.L119
	movl	%edx, %eax
	movq	%rdi, %rcx
	shrl	$4, %eax
	subq	%rsi, %rcx
	subl	$1, %eax
	salq	$4, %rax
	leaq	16(%rsi,%rax), %r8
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L120:
	movdqu	(%rax), %xmm0
	movups	%xmm0, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%r8, %rax
	jne	.L120
	movl	%edx, %eax
	movl	%edx, %r8d
	andl	$-16, %eax
	andl	$15, %r8d
	movl	%eax, %ecx
	addq	%rcx, %rdi
	addq	%rcx, %rsi
	cmpl	%eax, %edx
	je	.L113
	movzbl	(%rsi), %eax
	movb	%al, (%rdi)
	cmpl	$1, %r8d
	je	.L113
	movzbl	1(%rsi), %eax
	movb	%al, 1(%rdi)
	cmpl	$2, %r8d
	je	.L113
	movzbl	2(%rsi), %eax
	movb	%al, 2(%rdi)
	cmpl	$3, %r8d
	je	.L113
	movzbl	3(%rsi), %eax
	movb	%al, 3(%rdi)
	cmpl	$4, %r8d
	je	.L113
	movzbl	4(%rsi), %eax
	movb	%al, 4(%rdi)
	cmpl	$5, %r8d
	je	.L113
	movzbl	5(%rsi), %eax
	movb	%al, 5(%rdi)
	cmpl	$6, %r8d
	je	.L113
	movzbl	6(%rsi), %eax
	movb	%al, 6(%rdi)
	cmpl	$7, %r8d
	je	.L113
	movzbl	7(%rsi), %eax
	movb	%al, 7(%rdi)
	cmpl	$8, %r8d
	je	.L113
	movzbl	8(%rsi), %eax
	movb	%al, 8(%rdi)
	cmpl	$9, %r8d
	je	.L113
	movzbl	9(%rsi), %eax
	movb	%al, 9(%rdi)
	cmpl	$10, %r8d
	je	.L113
	movzbl	10(%rsi), %eax
	movb	%al, 10(%rdi)
	cmpl	$11, %r8d
	je	.L113
	movzbl	11(%rsi), %eax
	movb	%al, 11(%rdi)
	cmpl	$12, %r8d
	je	.L113
	movzbl	12(%rsi), %eax
	movb	%al, 12(%rdi)
	cmpl	$13, %r8d
	je	.L113
	movzbl	13(%rsi), %eax
	movb	%al, 13(%rdi)
	cmpl	$14, %r8d
	je	.L113
	movzbl	14(%rsi), %eax
	movb	%al, 14(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	leaq	1(%rsi,%r8), %r8
	subq	%rsi, %rdi
	.p2align 4,,10
	.p2align 3
.L123:
	movq	%rax, %rcx
	leaq	(%rax,%rdi), %rdx
	addq	$1, %rax
	movzbl	(%rcx), %ecx
	movb	%cl, (%rdx)
	cmpq	%r8, %rax
	jne	.L123
.L113:
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	addq	%rdi, %rcx
	cmpq	%rsi, %rcx
	jbe	.L114
	leal	-1(%rdx), %ecx
	addq	%rcx, %rdi
	addq	%rcx, %rsi
	testl	%edx, %edx
	je	.L113
	notq	%rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L118:
	movzbl	(%rsi,%rax), %edx
	movb	%dl, (%rdi,%rax)
	subq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L118
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	ret
	.cfi_endproc
.LFE5041:
	.size	_ZN2v88internal4wasm19memory_copy_wrapperEmmj, .-_ZN2v88internal4wasm19memory_copy_wrapperEmmj
	.section	.text._ZN2v88internal4wasm19memory_fill_wrapperEmjj,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm19memory_fill_wrapperEmjj
	.type	_ZN2v88internal4wasm19memory_fill_wrapperEmjj, @function
_ZN2v88internal4wasm19memory_fill_wrapperEmjj:
.LFB5042:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	je	.L181
	subl	$1, %edx
	movzbl	%sil, %esi
	addq	$1, %rdx
	jmp	memset@PLT
	.p2align 4,,10
	.p2align 3
.L181:
	ret
	.cfi_endproc
.LFE5042:
	.size	_ZN2v88internal4wasm19memory_fill_wrapperEmjj, .-_ZN2v88internal4wasm19memory_fill_wrapperEmjj
	.section	.text._ZN2v88internal4wasm29set_trap_callback_for_testingEPFvvE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm29set_trap_callback_for_testingEPFvvE
	.type	_ZN2v88internal4wasm29set_trap_callback_for_testingEPFvvE, @function
_ZN2v88internal4wasm29set_trap_callback_for_testingEPFvvE:
.LFB5043:
	.cfi_startproc
	endbr64
	movq	%rdi, _ZN2v88internal4wasmL30wasm_trap_callback_for_testingE(%rip)
	ret
	.cfi_endproc
.LFE5043:
	.size	_ZN2v88internal4wasm29set_trap_callback_for_testingEPFvvE, .-_ZN2v88internal4wasm29set_trap_callback_for_testingEPFvvE
	.section	.text._ZN2v88internal4wasm30call_trap_callback_for_testingEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm30call_trap_callback_for_testingEv
	.type	_ZN2v88internal4wasm30call_trap_callback_for_testingEv, @function
_ZN2v88internal4wasm30call_trap_callback_for_testingEv:
.LFB5044:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal4wasmL30wasm_trap_callback_for_testingE(%rip), %rax
	testq	%rax, %rax
	je	.L184
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L184:
	ret
	.cfi_endproc
.LFE5044:
	.size	_ZN2v88internal4wasm30call_trap_callback_for_testingEv, .-_ZN2v88internal4wasm30call_trap_callback_for_testingEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal4wasm17f32_trunc_wrapperEm,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal4wasm17f32_trunc_wrapperEm, @function
_GLOBAL__sub_I__ZN2v88internal4wasm17f32_trunc_wrapperEm:
.LFB5822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE5822:
	.size	_GLOBAL__sub_I__ZN2v88internal4wasm17f32_trunc_wrapperEm, .-_GLOBAL__sub_I__ZN2v88internal4wasm17f32_trunc_wrapperEm
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal4wasm17f32_trunc_wrapperEm
	.section	.bss._ZN2v88internal4wasmL30wasm_trap_callback_for_testingE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal4wasmL30wasm_trap_callback_for_testingE, @object
	.size	_ZN2v88internal4wasmL30wasm_trap_callback_for_testingE, 8
_ZN2v88internal4wasmL30wasm_trap_callback_for_testingE:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1258291200
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC2:
	.long	1065353216
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC3:
	.long	0
	.long	1127219200
	.section	.rodata.cst16
	.align 16
.LC4:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC5:
	.long	0
	.long	1072693248
	.section	.rodata.cst4
	.align 4
.LC6:
	.long	3741319168
	.align 4
.LC7:
	.long	1593835520
	.align 4
.LC8:
	.long	3212836864
	.align 4
.LC9:
	.long	1602224128
	.section	.rodata.cst8
	.align 8
.LC10:
	.long	0
	.long	-1008730112
	.align 8
.LC11:
	.long	0
	.long	1138753536
	.align 8
.LC12:
	.long	0
	.long	-1074790400
	.align 8
.LC13:
	.long	0
	.long	1139802112
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
