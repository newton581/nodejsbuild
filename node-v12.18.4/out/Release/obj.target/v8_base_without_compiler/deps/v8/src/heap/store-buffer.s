	.file	"store-buffer.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5205:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5205:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5206:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5206:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5208:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5208:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.rodata._ZN2v88internal11StoreBuffer29InsertDuringGarbageCollectionEPS1_m.str1.1,"aMS",@progbits,1
.LC0:
	.string	"NewArray"
	.section	.text._ZN2v88internal11StoreBuffer29InsertDuringGarbageCollectionEPS1_m,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11StoreBuffer29InsertDuringGarbageCollectionEPS1_m
	.type	_ZN2v88internal11StoreBuffer29InsertDuringGarbageCollectionEPS1_m, @function
_ZN2v88internal11StoreBuffer29InsertDuringGarbageCollectionEPS1_m:
.LFB20146:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	andq	$-262144, %r12
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	104(%r12), %rax
	testq	%rax, %rax
	je	.L19
.L7:
	movq	%rbx, %rdx
	andl	$262143, %ebx
	subq	%r12, %rdx
	movl	%ebx, %r13d
	movl	%ebx, %r12d
	sarl	$13, %ebx
	shrq	$18, %rdx
	movslq	%ebx, %rbx
	sarl	$8, %r12d
	leaq	(%rdx,%rdx,2), %rdx
	sarl	$3, %r13d
	andl	$31, %r12d
	salq	$7, %rdx
	andl	$31, %r13d
	leaq	(%rdx,%rbx,8), %rbx
	addq	%rax, %rbx
	movq	(%rbx), %r8
	testq	%r8, %r8
	je	.L20
.L9:
	movl	%r13d, %ecx
	movl	$1, %edx
	movslq	%r12d, %r12
	sall	%cl, %edx
	leaq	(%r8,%r12,4), %rcx
	movl	(%rcx), %eax
	testl	%eax, %edx
	je	.L12
.L5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movl	%edx, %edi
	movl	%esi, %eax
	orl	%esi, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %esi
	je	.L5
.L12:
	movl	(%rcx), %esi
	movl	%edx, %eax
	andl	%esi, %eax
	cmpl	%eax, %edx
	jne	.L21
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE0EEEPNS0_7SlotSetEv@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L20:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L22
.L10:
	leaq	8(%r8), %rdi
	movq	%r8, %rcx
	xorl	%eax, %eax
	movq	$0, (%r8)
	andq	$-8, %rdi
	movq	$0, 120(%r8)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r8, (%rbx)
	testq	%rax, %rax
	je	.L9
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	movq	(%rbx), %r8
	jmp	.L9
.L22:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	jne	.L10
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE20146:
	.size	_ZN2v88internal11StoreBuffer29InsertDuringGarbageCollectionEPS1_m, .-_ZN2v88internal11StoreBuffer29InsertDuringGarbageCollectionEPS1_m
	.section	.text._ZN2v88internal11StoreBuffer4TaskD2Ev,"axG",@progbits,_ZN2v88internal11StoreBuffer4TaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11StoreBuffer4TaskD2Ev
	.type	_ZN2v88internal11StoreBuffer4TaskD2Ev, @function
_ZN2v88internal11StoreBuffer4TaskD2Ev:
.LFB23484:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE23484:
	.size	_ZN2v88internal11StoreBuffer4TaskD2Ev, .-_ZN2v88internal11StoreBuffer4TaskD2Ev
	.weak	_ZN2v88internal11StoreBuffer4TaskD1Ev
	.set	_ZN2v88internal11StoreBuffer4TaskD1Ev,_ZN2v88internal11StoreBuffer4TaskD2Ev
	.section	.text._ZN2v88internal11StoreBuffer4TaskD0Ev,"axG",@progbits,_ZN2v88internal11StoreBuffer4TaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11StoreBuffer4TaskD0Ev
	.type	_ZN2v88internal11StoreBuffer4TaskD0Ev, @function
_ZN2v88internal11StoreBuffer4TaskD0Ev:
.LFB23486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23486:
	.size	_ZN2v88internal11StoreBuffer4TaskD0Ev, .-_ZN2v88internal11StoreBuffer4TaskD0Ev
	.section	.text._ZN2v88internal11StoreBuffer4TaskD2Ev,"axG",@progbits,_ZN2v88internal11StoreBuffer4TaskD5Ev,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal11StoreBuffer4TaskD1Ev
	.type	_ZThn32_N2v88internal11StoreBuffer4TaskD1Ev, @function
_ZThn32_N2v88internal11StoreBuffer4TaskD1Ev:
.LFB25198:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	subq	$32, %rdi
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE25198:
	.size	_ZThn32_N2v88internal11StoreBuffer4TaskD1Ev, .-_ZThn32_N2v88internal11StoreBuffer4TaskD1Ev
	.section	.text._ZN2v88internal11StoreBuffer4TaskD0Ev,"axG",@progbits,_ZN2v88internal11StoreBuffer4TaskD5Ev,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal11StoreBuffer4TaskD0Ev
	.type	_ZThn32_N2v88internal11StoreBuffer4TaskD0Ev, @function
_ZThn32_N2v88internal11StoreBuffer4TaskD0Ev:
.LFB25199:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-32(%rdi), %r12
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	movq	%r12, %rdi
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE25199:
	.size	_ZThn32_N2v88internal11StoreBuffer4TaskD0Ev, .-_ZThn32_N2v88internal11StoreBuffer4TaskD0Ev
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8776:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L35
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L38
.L29:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L29
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8776:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.text._ZN2v88internal11StoreBufferC2EPNS0_4HeapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11StoreBufferC2EPNS0_4HeapE
	.type	_ZN2v88internal11StoreBufferC2EPNS0_4HeapE, @function
_ZN2v88internal11StoreBufferC2EPNS0_4HeapE:
.LFB20141:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rsi, -64(%rdi)
	movq	$0, -56(%rdi)
	call	_ZN2v84base5MutexC1Ev@PLT
	pxor	%xmm0, %xmm0
	movb	$0, 104(%rbx)
	movabsq	$4294967296, %rax
	movq	%rax, 108(%rbx)
	leaq	_ZN2v88internal11StoreBuffer19InsertDuringRuntimeEPS1_m(%rip), %rax
	movq	$0, 120(%rbx)
	movq	%rax, 144(%rbx)
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20141:
	.size	_ZN2v88internal11StoreBufferC2EPNS0_4HeapE, .-_ZN2v88internal11StoreBufferC2EPNS0_4HeapE
	.globl	_ZN2v88internal11StoreBufferC1EPNS0_4HeapE
	.set	_ZN2v88internal11StoreBufferC1EPNS0_4HeapE,_ZN2v88internal11StoreBufferC2EPNS0_4HeapE
	.section	.rodata._ZN2v88internal11StoreBuffer5SetUpEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"StoreBuffer::SetUp"
	.section	.text._ZN2v88internal11StoreBuffer5SetUpEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11StoreBuffer5SetUpEv
	.type	_ZN2v88internal11StoreBuffer5SetUpEv, @function
_ZN2v88internal11StoreBuffer5SetUpEv:
.LFB20143:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movl	$16384, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal24GetPlatformPageAllocatorEv@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	%r12, %rdi
	leal	32767(%rax), %r14d
	negl	%eax
	andl	%r14d, %eax
	movslq	%eax, %r14
	movq	(%r12), %rax
	call	*16(%rax)
	cmpq	$16384, %rax
	cmovnb	%rax, %r13
	call	_ZN2v88internal17GetRandomMmapAddrEv@PLT
	movq	%r12, %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%r13, %rcx
	movq	%r13, %r8
	negq	%rcx
	andq	%rax, %rcx
	call	_ZN2v88internal13VirtualMemoryC1EPNS_13PageAllocatorEmPvm@PLT
	movq	-72(%rbp), %r12
	testq	%r12, %r12
	je	.L46
.L42:
	leaq	16384(%r12), %rax
	movq	%r12, 16(%rbx)
	movq	%rax, 32(%rbx)
	movq	%rax, 24(%rbx)
	leaq	32768(%r12), %rax
	movq	%rax, 40(%rbx)
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movl	$2, %ecx
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	-1(%rax,%r14), %rdx
	negq	%rax
	andq	%rax, %rdx
	call	_ZN2v88internal13VirtualMemory14SetPermissionsEmmNS_13PageAllocator10PermissionE@PLT
	testb	%al, %al
	je	.L47
.L43:
	movq	16(%rbx), %rax
	movdqu	-72(%rbp), %xmm0
	movl	$0, 108(%rbx)
	movq	%r15, %rdi
	movq	%rax, 8(%rbx)
	movq	-80(%rbp), %rax
	movups	%xmm0, 128(%rbx)
	movq	%rax, 120(%rbx)
	call	_ZN2v88internal13VirtualMemory5ResetEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal13VirtualMemoryD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L48
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movq	(%rbx), %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	movq	-72(%rbp), %r12
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L47:
	movq	(%rbx), %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	jmp	.L43
.L48:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20143:
	.size	_ZN2v88internal11StoreBuffer5SetUpEv, .-_ZN2v88internal11StoreBuffer5SetUpEv
	.section	.text._ZN2v88internal11StoreBuffer8TearDownEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11StoreBuffer8TearDownEv
	.type	_ZN2v88internal11StoreBuffer8TearDownEv, @function
_ZN2v88internal11StoreBuffer8TearDownEv:
.LFB20144:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, 128(%rdi)
	jne	.L52
.L50:
	pxor	%xmm0, %xmm0
	movq	$0, 56(%rbx)
	movups	%xmm0, 8(%rbx)
	movups	%xmm0, 24(%rbx)
	movups	%xmm0, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	leaq	120(%rdi), %rdi
	call	_ZN2v88internal13VirtualMemory4FreeEv@PLT
	jmp	.L50
	.cfi_endproc
.LFE20144:
	.size	_ZN2v88internal11StoreBuffer8TearDownEv, .-_ZN2v88internal11StoreBuffer8TearDownEv
	.section	.text._ZN2v88internal11StoreBuffer7SetModeENS1_15StoreBufferModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11StoreBuffer7SetModeENS1_15StoreBufferModeE
	.type	_ZN2v88internal11StoreBuffer7SetModeENS1_15StoreBufferModeE, @function
_ZN2v88internal11StoreBuffer7SetModeENS1_15StoreBufferModeE:
.LFB20147:
	.cfi_startproc
	endbr64
	cmpl	$1, %esi
	leaq	_ZN2v88internal11StoreBuffer19InsertDuringRuntimeEPS1_m(%rip), %rax
	leaq	_ZN2v88internal11StoreBuffer29InsertDuringGarbageCollectionEPS1_m(%rip), %rdx
	movl	%esi, 112(%rdi)
	cmovne	%rdx, %rax
	movq	%rax, 144(%rdi)
	ret
	.cfi_endproc
.LFE20147:
	.size	_ZN2v88internal11StoreBuffer7SetModeENS1_15StoreBufferModeE, .-_ZN2v88internal11StoreBuffer7SetModeENS1_15StoreBufferModeE
	.section	.text._ZN2v88internal11StoreBuffer26MoveEntriesToRememberedSetEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11StoreBuffer26MoveEntriesToRememberedSetEi
	.type	_ZN2v88internal11StoreBuffer26MoveEntriesToRememberedSetEi, @function
_ZN2v88internal11StoreBuffer26MoveEntriesToRememberedSetEi:
.LFB20152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	(%rdi,%rsi,8), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	48(%r14), %rax
	testq	%rax, %rax
	je	.L56
	movq	16(%r14), %r12
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	cmpq	%r12, %rax
	ja	.L58
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L60:
	cmpq	%r13, %rbx
	je	.L70
	movq	104(%r15), %rax
	testq	%rax, %rax
	je	.L84
.L66:
	movq	%rbx, %rcx
	movl	%ebx, %esi
	subq	%r15, %rcx
	andl	$262143, %esi
	shrq	$18, %rcx
	movl	%esi, %r13d
	movl	%esi, %r8d
	sarl	$13, %esi
	leaq	(%rcx,%rcx,2), %rcx
	sarl	$8, %r13d
	movslq	%esi, %rsi
	sarl	$3, %r8d
	salq	$7, %rcx
	andl	$31, %r13d
	andl	$31, %r8d
	leaq	(%rcx,%rsi,8), %r10
	addq	%rax, %r10
	movq	(%r10), %r9
	testq	%r9, %r9
	je	.L85
.L68:
	movslq	%r13d, %r13
	movl	$1, %esi
	movl	%r8d, %ecx
	leaq	(%r9,%r13,4), %rdi
	sall	%cl, %esi
	movl	(%rdi), %eax
	testl	%eax, %esi
	je	.L71
.L70:
	addq	$8, %r12
	cmpq	%r12, 48(%r14)
	jbe	.L64
.L58:
	movq	%rbx, %r13
	movq	(%r12), %rbx
	movq	%rbx, %rax
	andq	$-262144, %rax
	testq	%r15, %r15
	je	.L73
	cmpq	%rax, %r15
	je	.L60
.L73:
	movq	%rbx, %r15
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L86:
	leaq	-1(%rdx), %r15
.L62:
	movq	%r15, %rdi
	call	_ZN2v88internal16BasicMemoryChunk17HasHeaderSentinelEm@PLT
	movq	%r15, %rdx
	andq	$-262144, %rdx
	testb	%al, %al
	je	.L86
	movq	%rdx, %r15
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L87:
	movl	%esi, %r8d
	movl	%ecx, %eax
	orl	%ecx, %r8d
	lock cmpxchgl	%r8d, (%rdi)
	cmpl	%eax, %ecx
	je	.L70
.L71:
	movl	(%rdi), %ecx
	movl	%esi, %eax
	andl	%ecx, %eax
	cmpl	%eax, %esi
	jne	.L87
	addq	$8, %r12
	cmpq	%r12, 48(%r14)
	ja	.L58
	.p2align 4,,10
	.p2align 3
.L64:
	movq	$0, 48(%r14)
.L56:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movq	%r10, -64(%rbp)
	movl	%r8d, -56(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-56(%rbp), %r8d
	movq	-64(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L88
.L69:
	leaq	8(%r9), %rdi
	movq	%r9, %rcx
	xorl	%eax, %eax
	movq	$0, (%r9)
	andq	$-8, %rdi
	movq	$0, 120(%r9)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r9, (%r10)
	movq	%r10, -56(%rbp)
	testq	%rax, %rax
	je	.L68
	movq	%r9, %rdi
	movl	%r8d, -64(%rbp)
	call	_ZdaPv@PLT
	movq	-56(%rbp), %r10
	movq	(%r10), %r9
	movl	-64(%rbp), %r8d
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L84:
	movq	%r15, %rdi
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE0EEEPNS0_7SlotSetEv@PLT
	jmp	.L66
.L88:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-56(%rbp), %r8d
	movq	-64(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r9
	jne	.L69
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE20152:
	.size	_ZN2v88internal11StoreBuffer26MoveEntriesToRememberedSetEi, .-_ZN2v88internal11StoreBuffer26MoveEntriesToRememberedSetEi
	.section	.text._ZN2v88internal11StoreBuffer16FlipStoreBuffersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11StoreBuffer16FlipStoreBuffersEv
	.type	_ZN2v88internal11StoreBuffer16FlipStoreBuffersEv, @function
_ZN2v88internal11StoreBuffer16FlipStoreBuffersEv:
.LFB20149:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	64(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	108(%r12), %eax
	movq	%r12, %rdi
	leal	1(%rax), %ebx
	movl	%ebx, %eax
	shrl	$31, %eax
	addl	%eax, %ebx
	andl	$1, %ebx
	subl	%eax, %ebx
	movl	%ebx, %esi
	call	_ZN2v88internal11StoreBuffer26MoveEntriesToRememberedSetEi
	movslq	108(%r12), %rax
	movq	8(%r12), %rdx
	movq	%rdx, 48(%r12,%rax,8)
	cmpb	$0, 104(%r12)
	movl	%ebx, 108(%r12)
	movslq	%ebx, %rbx
	movq	16(%r12,%rbx,8), %rax
	movq	%rax, 8(%r12)
	jne	.L90
	cmpb	$0, _ZN2v88internal28FLAG_concurrent_store_bufferE(%rip)
	jne	.L97
.L90:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L98
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	movb	$1, 104(%r12)
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movl	$56, %edi
	movq	%rax, %r14
	movq	(%rax), %rax
	movq	56(%rax), %rdx
	movq	(%r12), %rax
	movq	%rdx, -72(%rbp)
	leaq	-37592(%rax), %r15
	call	_Znwm@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE@PLT
	addq	$32, %rbx
	movq	%r12, 8(%rbx)
	movq	%r14, %rdi
	leaq	16+_ZTVN2v88internal11StoreBuffer4TaskE(%rip), %rax
	movq	-72(%rbp), %rdx
	leaq	-64(%rbp), %rsi
	movq	%rax, -32(%rbx)
	addq	$48, %rax
	movq	%rax, (%rbx)
	movq	39600(%r15), %rax
	movq	%rax, 16(%rbx)
	movq	%rbx, -64(%rbp)
	call	*%rdx
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L90
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L90
.L98:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20149:
	.size	_ZN2v88internal11StoreBuffer16FlipStoreBuffersEv, .-_ZN2v88internal11StoreBuffer16FlipStoreBuffersEv
	.section	.text._ZN2v88internal11StoreBuffer19InsertDuringRuntimeEPS1_m,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11StoreBuffer19InsertDuringRuntimeEPS1_m
	.type	_ZN2v88internal11StoreBuffer19InsertDuringRuntimeEPS1_m, @function
_ZN2v88internal11StoreBuffer19InsertDuringRuntimeEPS1_m:
.LFB20145:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movslq	108(%rdi), %rcx
	leaq	64(%rax), %rdx
	cmpq	%rdx, 32(%rdi,%rcx,8)
	jb	.L106
.L100:
	movq	%r12, (%rax)
	addq	$8, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	2056(%rax), %rdi
	leaq	-37592(%rax), %r13
	call	_ZN2v88internal11StoreBuffer16FlipStoreBuffersEv
	movq	40960(%r13), %r13
	cmpb	$0, 6688(%r13)
	je	.L101
	movq	6680(%r13), %rax
.L102:
	testq	%rax, %rax
	je	.L105
	addl	$1, (%rax)
.L105:
	movq	8(%rbx), %rax
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L101:
	movb	$1, 6688(%r13)
	leaq	6664(%r13), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 6680(%r13)
	jmp	.L102
	.cfi_endproc
.LFE20145:
	.size	_ZN2v88internal11StoreBuffer19InsertDuringRuntimeEPS1_m, .-_ZN2v88internal11StoreBuffer19InsertDuringRuntimeEPS1_m
	.section	.rodata._ZN2v88internal11StoreBuffer4Task11RunInternalEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	"disabled-by-default-v8.gc"
	.section	.text._ZN2v88internal11StoreBuffer4Task11RunInternalEv,"axG",@progbits,_ZN2v88internal11StoreBuffer4Task11RunInternalEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11StoreBuffer4Task11RunInternalEv
	.type	_ZN2v88internal11StoreBuffer4Task11RunInternalEv, @function
_ZN2v88internal11StoreBuffer4Task11RunInternalEv:
.LFB9979:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-144(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-184(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	48(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8GCTracer32worker_thread_runtime_call_statsEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeC1EPNS0_28WorkerThreadRuntimeCallStatsE@PLT
	movq	48(%rbx), %rsi
	movl	$1, %edx
	movq	%r13, %rdi
	movq	-184(%rbp), %rcx
	call	_ZN2v88internal8GCTracer15BackgroundScopeC1EPS1_NS2_7ScopeIdEPNS0_16RuntimeCallStatsE@PLT
	movq	_ZZN2v88internal11StoreBuffer4Task11RunInternalEvE28trace_event_unique_atomic104(%rip), %rdx
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L128
.L109:
	movq	$0, -176(%rbp)
	movzbl	(%r14), %eax
	testb	$5, %al
	jne	.L129
.L111:
	movq	40(%rbx), %rbx
	leaq	64(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	108(%rbx), %eax
	movq	%rbx, %rdi
	leal	1(%rax), %esi
	movl	%esi, %eax
	shrl	$31, %eax
	addl	%eax, %esi
	andl	$1, %esi
	subl	%eax, %esi
	call	_ZN2v88internal11StoreBuffer26MoveEntriesToRememberedSetEi
	movb	$0, 104(%rbx)
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	leaq	-176(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	%r13, %rdi
	call	_ZN2v88internal8GCTracer15BackgroundScopeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L130
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r14
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L131
.L110:
	movq	%r14, _ZZN2v88internal11StoreBuffer4Task11RunInternalEvE28trace_event_unique_atomic104(%rip)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L129:
	movl	$1, %edi
	xorl	%r15d, %r15d
	call	_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, -200(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L132
.L112:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L113
	movq	(%rdi), %rax
	call	*8(%rax)
.L113:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L114
	movq	(%rdi), %rax
	call	*8(%rax)
.L114:
	movl	$1, %edi
	call	_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE@PLT
	movq	%r14, -168(%rbp)
	movq	%rax, -160(%rbp)
	leaq	-168(%rbp), %rax
	movq	%r15, -152(%rbp)
	movq	%rax, -176(%rbp)
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L131:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %r14
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L132:
	subq	$8, %rsp
	leaq	-80(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r14, %rdx
	movl	$88, %esi
	pushq	%rcx
	movq	-200(%rbp), %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L112
.L130:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9979:
	.size	_ZN2v88internal11StoreBuffer4Task11RunInternalEv, .-_ZN2v88internal11StoreBuffer4Task11RunInternalEv
	.section	.text._ZN2v88internal14CancelableTask3RunEv,"axG",@progbits,_ZN2v88internal14CancelableTask3RunEv,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal14CancelableTask3RunEv
	.type	_ZThn32_N2v88internal14CancelableTask3RunEv, @function
_ZThn32_N2v88internal14CancelableTask3RunEv:
.LFB25200:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	lock cmpxchgl	%edx, -16(%rdi)
	jne	.L133
	movq	-32(%rdi), %rax
	leaq	_ZN2v88internal11StoreBuffer4Task11RunInternalEv(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L158
	subq	$32, %rdi
	call	*%rax
.L133:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L159
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L159:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L158:
	movq	16(%rdi), %rdi
	leaq	-184(%rbp), %r13
	leaq	-144(%rbp), %r14
	call	_ZN2v88internal8GCTracer32worker_thread_runtime_call_statsEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeC1EPNS0_28WorkerThreadRuntimeCallStatsE@PLT
	movq	16(%rbx), %rsi
	movl	$1, %edx
	movq	%r14, %rdi
	movq	-184(%rbp), %rcx
	call	_ZN2v88internal8GCTracer15BackgroundScopeC1EPS1_NS2_7ScopeIdEPNS0_16RuntimeCallStatsE@PLT
	movq	_ZZN2v88internal11StoreBuffer4Task11RunInternalEvE28trace_event_unique_atomic104(%rip), %r12
	testq	%r12, %r12
	je	.L160
.L136:
	movq	$0, -176(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L161
.L138:
	movq	8(%rbx), %rbx
	leaq	64(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	108(%rbx), %eax
	movl	$2, %ecx
	movq	%rbx, %rdi
	addl	$1, %eax
	cltd
	idivl	%ecx
	movl	%edx, %esi
	call	_ZN2v88internal11StoreBuffer26MoveEntriesToRememberedSetEi
	movb	$0, 104(%rbx)
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	leaq	-176(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8GCTracer15BackgroundScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeD1Ev@PLT
	jmp	.L133
.L160:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	je	.L137
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
.L137:
	movq	%r12, _ZZN2v88internal11StoreBuffer4Task11RunInternalEvE28trace_event_unique_atomic104(%rip)
	jmp	.L136
.L161:
	movl	$1, %edi
	xorl	%r15d, %r15d
	call	_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, -200(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L139
	pushq	%rdx
	leaq	-80(%rbp), %rdx
	movq	-200(%rbp), %rcx
	xorl	%r9d, %r9d
	pushq	$0
	xorl	%r8d, %r8d
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
.L139:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L140
	movq	(%rdi), %rax
	call	*8(%rax)
.L140:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L141
	movq	(%rdi), %rax
	call	*8(%rax)
.L141:
	movl	$1, %edi
	call	_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE@PLT
	movq	%r12, -168(%rbp)
	movq	%rax, -160(%rbp)
	leaq	-168(%rbp), %rax
	movq	%r15, -152(%rbp)
	movq	%rax, -176(%rbp)
	jmp	.L138
	.cfi_endproc
.LFE25200:
	.size	_ZThn32_N2v88internal14CancelableTask3RunEv, .-_ZThn32_N2v88internal14CancelableTask3RunEv
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CancelableTask3RunEv
	.type	_ZN2v88internal14CancelableTask3RunEv, @function
_ZN2v88internal14CancelableTask3RunEv:
.LFB9575:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	lock cmpxchgl	%edx, 16(%rdi)
	jne	.L162
	movq	(%rdi), %rax
	leaq	_ZN2v88internal11StoreBuffer4Task11RunInternalEv(%rip), %rdx
	movq	%rdi, %r14
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L164
	movq	48(%rdi), %rdi
	leaq	-184(%rbp), %r12
	leaq	-144(%rbp), %r13
	call	_ZN2v88internal8GCTracer32worker_thread_runtime_call_statsEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeC1EPNS0_28WorkerThreadRuntimeCallStatsE@PLT
	movq	48(%r14), %rsi
	movl	$1, %edx
	movq	%r13, %rdi
	movq	-184(%rbp), %rcx
	call	_ZN2v88internal8GCTracer15BackgroundScopeC1EPS1_NS2_7ScopeIdEPNS0_16RuntimeCallStatsE@PLT
	movq	_ZZN2v88internal11StoreBuffer4Task11RunInternalEvE28trace_event_unique_atomic104(%rip), %rdx
	movq	%rdx, %r15
	testq	%rdx, %rdx
	je	.L188
.L166:
	movq	$0, -176(%rbp)
	movzbl	(%r15), %eax
	testb	$5, %al
	jne	.L189
.L168:
	movq	40(%r14), %rbx
	leaq	64(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	108(%rbx), %eax
	movq	%rbx, %rdi
	leal	1(%rax), %esi
	movl	%esi, %eax
	shrl	$31, %eax
	addl	%eax, %esi
	andl	$1, %esi
	subl	%eax, %esi
	call	_ZN2v88internal11StoreBuffer26MoveEntriesToRememberedSetEi
	movb	$0, 104(%rbx)
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	leaq	-176(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	%r13, %rdi
	call	_ZN2v88internal8GCTracer15BackgroundScopeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeD1Ev@PLT
.L162:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L190
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r15
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L191
.L167:
	movq	%r15, _ZZN2v88internal11StoreBuffer4Task11RunInternalEvE28trace_event_unique_atomic104(%rip)
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L189:
	movl	$1, %edi
	xorl	%ebx, %ebx
	call	_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, -200(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L192
.L169:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L170
	movq	(%rdi), %rax
	call	*8(%rax)
.L170:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L171
	movq	(%rdi), %rax
	call	*8(%rax)
.L171:
	movl	$1, %edi
	call	_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE@PLT
	movq	%r15, -168(%rbp)
	movq	%rax, -160(%rbp)
	leaq	-168(%rbp), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -176(%rbp)
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L164:
	call	*%rax
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L192:
	subq	$8, %rsp
	leaq	-80(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r15, %rdx
	movl	$88, %esi
	pushq	%rcx
	movq	-200(%rbp), %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %rbx
	addq	$64, %rsp
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L191:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %r15
	jmp	.L167
.L190:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9575:
	.size	_ZN2v88internal14CancelableTask3RunEv, .-_ZN2v88internal14CancelableTask3RunEv
	.section	.text._ZN2v88internal11StoreBuffer19StoreBufferOverflowEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11StoreBuffer19StoreBufferOverflowEPNS0_7IsolateE
	.type	_ZN2v88internal11StoreBuffer19StoreBufferOverflowEPNS0_7IsolateE, @function
_ZN2v88internal11StoreBuffer19StoreBufferOverflowEPNS0_7IsolateE:
.LFB20148:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	39648(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	64(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	108(%r13), %eax
	movq	%r13, %rdi
	leal	1(%rax), %ebx
	movl	%ebx, %eax
	shrl	$31, %eax
	addl	%eax, %ebx
	andl	$1, %ebx
	subl	%eax, %ebx
	movl	%ebx, %esi
	call	_ZN2v88internal11StoreBuffer26MoveEntriesToRememberedSetEi
	movslq	108(%r13), %rax
	movq	8(%r13), %rdx
	movq	%rdx, 48(%r13,%rax,8)
	cmpb	$0, 104(%r13)
	movl	%ebx, 108(%r13)
	movslq	%ebx, %rbx
	movq	16(%r13,%rbx,8), %rax
	movq	%rax, 8(%r13)
	jne	.L194
	cmpb	$0, _ZN2v88internal28FLAG_concurrent_store_bufferE(%rip)
	jne	.L207
.L194:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	40960(%r12), %rbx
	cmpb	$0, 6688(%rbx)
	je	.L196
	movq	6680(%rbx), %rax
.L197:
	testq	%rax, %rax
	je	.L198
	addl	$1, (%rax)
.L198:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L208
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	movb	$1, 6688(%rbx)
	leaq	6664(%rbx), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 6680(%rbx)
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L207:
	movb	$1, 104(%r13)
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movl	$56, %edi
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	56(%rax), %rcx
	movq	0(%r13), %rax
	leaq	-37592(%rax), %rdx
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rax, %rbx
	movq	%rdx, %rsi
	addq	$32, %rbx
	call	_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rdx
	movq	%r13, 8(%rbx)
	movq	%r15, %rdi
	leaq	16+_ZTVN2v88internal11StoreBuffer4TaskE(%rip), %rax
	movq	-80(%rbp), %rcx
	leaq	-64(%rbp), %rsi
	movq	%rax, -32(%rbx)
	addq	$48, %rax
	movq	%rax, (%rbx)
	movq	39600(%rdx), %rax
	movq	%rax, 16(%rbx)
	movq	%rbx, -64(%rbp)
	call	*%rcx
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L194
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L194
.L208:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20148:
	.size	_ZN2v88internal11StoreBuffer19StoreBufferOverflowEPNS0_7IsolateE, .-_ZN2v88internal11StoreBuffer19StoreBufferOverflowEPNS0_7IsolateE
	.section	.text._ZN2v88internal11StoreBuffer29MoveAllEntriesToRememberedSetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11StoreBuffer29MoveAllEntriesToRememberedSetEv
	.type	_ZN2v88internal11StoreBuffer29MoveAllEntriesToRememberedSetEv, @function
_ZN2v88internal11StoreBuffer29MoveAllEntriesToRememberedSetEv:
.LFB20153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	64(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	108(%rbx), %eax
	movq	%rbx, %rdi
	leal	1(%rax), %esi
	movl	%esi, %eax
	shrl	$31, %eax
	addl	%eax, %esi
	andl	$1, %esi
	subl	%eax, %esi
	call	_ZN2v88internal11StoreBuffer26MoveEntriesToRememberedSetEi
	movslq	108(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	%rbx, %rdi
	movq	%rdx, 48(%rbx,%rax,8)
	movq	%rax, %rsi
	call	_ZN2v88internal11StoreBuffer26MoveEntriesToRememberedSetEi
	movslq	108(%rbx), %rax
	movq	%r12, %rdi
	movq	16(%rbx,%rax,8), %rax
	movq	%rax, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE20153:
	.size	_ZN2v88internal11StoreBuffer29MoveAllEntriesToRememberedSetEv, .-_ZN2v88internal11StoreBuffer29MoveAllEntriesToRememberedSetEv
	.section	.text._ZN2v88internal11StoreBuffer30ConcurrentlyProcessStoreBufferEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11StoreBuffer30ConcurrentlyProcessStoreBufferEv
	.type	_ZN2v88internal11StoreBuffer30ConcurrentlyProcessStoreBufferEv, @function
_ZN2v88internal11StoreBuffer30ConcurrentlyProcessStoreBufferEv:
.LFB20154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	64(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	108(%rbx), %eax
	movq	%rbx, %rdi
	leal	1(%rax), %esi
	movl	%esi, %eax
	shrl	$31, %eax
	addl	%eax, %esi
	andl	$1, %esi
	subl	%eax, %esi
	call	_ZN2v88internal11StoreBuffer26MoveEntriesToRememberedSetEi
	movb	$0, 104(%rbx)
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE20154:
	.size	_ZN2v88internal11StoreBuffer30ConcurrentlyProcessStoreBufferEv, .-_ZN2v88internal11StoreBuffer30ConcurrentlyProcessStoreBufferEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11StoreBufferC2EPNS0_4HeapE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11StoreBufferC2EPNS0_4HeapE, @function
_GLOBAL__sub_I__ZN2v88internal11StoreBufferC2EPNS0_4HeapE:
.LFB25180:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25180:
	.size	_GLOBAL__sub_I__ZN2v88internal11StoreBufferC2EPNS0_4HeapE, .-_GLOBAL__sub_I__ZN2v88internal11StoreBufferC2EPNS0_4HeapE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11StoreBufferC2EPNS0_4HeapE
	.weak	_ZTVN2v88internal14CancelableTaskE
	.section	.data.rel.ro._ZTVN2v88internal14CancelableTaskE,"awG",@progbits,_ZTVN2v88internal14CancelableTaskE,comdat
	.align 8
	.type	_ZTVN2v88internal14CancelableTaskE, @object
	.size	_ZTVN2v88internal14CancelableTaskE, 88
_ZTVN2v88internal14CancelableTaskE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	__cxa_pure_virtual
	.quad	-32
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.weak	_ZTVN2v88internal11StoreBuffer4TaskE
	.section	.data.rel.ro.local._ZTVN2v88internal11StoreBuffer4TaskE,"awG",@progbits,_ZTVN2v88internal11StoreBuffer4TaskE,comdat
	.align 8
	.type	_ZTVN2v88internal11StoreBuffer4TaskE, @object
	.size	_ZTVN2v88internal11StoreBuffer4TaskE, 88
_ZTVN2v88internal11StoreBuffer4TaskE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11StoreBuffer4TaskD1Ev
	.quad	_ZN2v88internal11StoreBuffer4TaskD0Ev
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	_ZN2v88internal11StoreBuffer4Task11RunInternalEv
	.quad	-32
	.quad	0
	.quad	_ZThn32_N2v88internal11StoreBuffer4TaskD1Ev
	.quad	_ZThn32_N2v88internal11StoreBuffer4TaskD0Ev
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.weak	_ZZN2v88internal11StoreBuffer4Task11RunInternalEvE28trace_event_unique_atomic104
	.section	.bss._ZZN2v88internal11StoreBuffer4Task11RunInternalEvE28trace_event_unique_atomic104,"awG",@nobits,_ZZN2v88internal11StoreBuffer4Task11RunInternalEvE28trace_event_unique_atomic104,comdat
	.align 8
	.type	_ZZN2v88internal11StoreBuffer4Task11RunInternalEvE28trace_event_unique_atomic104, @gnu_unique_object
	.size	_ZZN2v88internal11StoreBuffer4Task11RunInternalEvE28trace_event_unique_atomic104, 8
_ZZN2v88internal11StoreBuffer4Task11RunInternalEvE28trace_event_unique_atomic104:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
