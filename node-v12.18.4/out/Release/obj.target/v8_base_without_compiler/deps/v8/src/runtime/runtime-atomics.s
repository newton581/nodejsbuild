	.file	"runtime-atomics.cc"
	.text
	.section	.text._ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE,"axG",@progbits,_ZN2v88internal21RuntimeCallTimerScopeC5EPNS0_7IsolateENS0_20RuntimeCallCounterIdE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	.type	_ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE, @function
_ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE:
.LFB9148:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	$0, 32(%rdi)
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	40960(%rsi), %r8
	leaq	8(%rdi), %rsi
	addq	$23240, %r8
	movq	%r8, (%rdi)
	movq	%r8, %rdi
	jmp	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	.cfi_endproc
.LFE9148:
	.size	_ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE, .-_ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	.weak	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	.set	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE,_ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	.section	.text._ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev,"axG",@progbits,_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev
	.type	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev, @function
_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev:
.LFB19002:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L5:
	ret
	.cfi_endproc
.LFE19002:
	.size	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev, .-_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev
	.weak	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	.set	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev,_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev
	.section	.rodata._ZN2v88internalL27Stats_Runtime_AtomicsLoad64EiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC1:
	.string	"V8.Runtime_Runtime_AtomicsLoad64"
	.section	.rodata._ZN2v88internalL27Stats_Runtime_AtomicsLoad64EiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC2:
	.string	"unreachable code"
	.section	.text._ZN2v88internalL27Stats_Runtime_AtomicsLoad64EiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL27Stats_Runtime_AtomicsLoad64EiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL27Stats_Runtime_AtomicsLoad64EiPmPNS0_7IsolateE.isra.0:
.LFB22080:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movl	$214, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-96(%rbp), %rdi
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	movq	_ZZN2v88internalL27Stats_Runtime_AtomicsLoad64EiPmPNS0_7IsolateEE28trace_event_unique_atomic536(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L16
.L9:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L17
.L10:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L16:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL27Stats_Runtime_AtomicsLoad64EiPmPNS0_7IsolateEE28trace_event_unique_atomic536(%rip)
	movq	%rax, %r12
	jmp	.L9
.L17:
	pxor	%xmm0, %xmm0
	leaq	-48(%rbp), %r13
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC1(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%r13
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	leaq	-40(%rbp), %rdi
	addq	$64, %rsp
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	jmp	.L10
	.cfi_endproc
.LFE22080:
	.size	_ZN2v88internalL27Stats_Runtime_AtomicsLoad64EiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL27Stats_Runtime_AtomicsLoad64EiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal21Runtime_AtomicsLoad64EiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Runtime_AtomicsLoad64EiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Runtime_AtomicsLoad64EiPmPNS0_7IsolateE, @function
_ZN2v88internal21Runtime_AtomicsLoad64EiPmPNS0_7IsolateE:
.LFB18342:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L21
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L21:
	movq	%rdx, %rdi
	call	_ZN2v88internalL27Stats_Runtime_AtomicsLoad64EiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18342:
	.size	_ZN2v88internal21Runtime_AtomicsLoad64EiPmPNS0_7IsolateE, .-_ZN2v88internal21Runtime_AtomicsLoad64EiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL28Stats_Runtime_AtomicsStore64EiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"V8.Runtime_Runtime_AtomicsStore64"
	.section	.text._ZN2v88internalL28Stats_Runtime_AtomicsStore64EiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Stats_Runtime_AtomicsStore64EiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL28Stats_Runtime_AtomicsStore64EiPmPNS0_7IsolateE.isra.0:
.LFB22081:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movl	$215, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-96(%rbp), %rdi
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	movq	_ZZN2v88internalL28Stats_Runtime_AtomicsStore64EiPmPNS0_7IsolateEE28trace_event_unique_atomic538(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L31
.L24:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L32
.L25:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L31:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL28Stats_Runtime_AtomicsStore64EiPmPNS0_7IsolateEE28trace_event_unique_atomic538(%rip)
	movq	%rax, %r12
	jmp	.L24
.L32:
	pxor	%xmm0, %xmm0
	leaq	-48(%rbp), %r13
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC3(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%r13
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	leaq	-40(%rbp), %rdi
	addq	$64, %rsp
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	jmp	.L25
	.cfi_endproc
.LFE22081:
	.size	_ZN2v88internalL28Stats_Runtime_AtomicsStore64EiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL28Stats_Runtime_AtomicsStore64EiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal22Runtime_AtomicsStore64EiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Runtime_AtomicsStore64EiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Runtime_AtomicsStore64EiPmPNS0_7IsolateE, @function
_ZN2v88internal22Runtime_AtomicsStore64EiPmPNS0_7IsolateE:
.LFB18345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L36
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L36:
	movq	%rdx, %rdi
	call	_ZN2v88internalL28Stats_Runtime_AtomicsStore64EiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18345:
	.size	_ZN2v88internal22Runtime_AtomicsStore64EiPmPNS0_7IsolateE, .-_ZN2v88internal22Runtime_AtomicsStore64EiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL29Stats_Runtime_AtomicsExchangeEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"V8.Runtime_Runtime_AtomicsExchange"
	.section	.text._ZN2v88internalL29Stats_Runtime_AtomicsExchangeEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Stats_Runtime_AtomicsExchangeEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL29Stats_Runtime_AtomicsExchangeEiPmPNS0_7IsolateE.isra.0:
.LFB22082:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movl	$219, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-96(%rbp), %rdi
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	movq	_ZZN2v88internalL29Stats_Runtime_AtomicsExchangeEiPmPNS0_7IsolateEE28trace_event_unique_atomic540(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L46
.L39:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L47
.L40:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L46:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL29Stats_Runtime_AtomicsExchangeEiPmPNS0_7IsolateEE28trace_event_unique_atomic540(%rip)
	movq	%rax, %r12
	jmp	.L39
.L47:
	pxor	%xmm0, %xmm0
	leaq	-48(%rbp), %r13
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC4(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%r13
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	leaq	-40(%rbp), %rdi
	addq	$64, %rsp
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	jmp	.L40
	.cfi_endproc
.LFE22082:
	.size	_ZN2v88internalL29Stats_Runtime_AtomicsExchangeEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL29Stats_Runtime_AtomicsExchangeEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal23Runtime_AtomicsExchangeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Runtime_AtomicsExchangeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Runtime_AtomicsExchangeEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Runtime_AtomicsExchangeEiPmPNS0_7IsolateE:
.LFB18348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L51
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L51:
	movq	%rdx, %rdi
	call	_ZN2v88internalL29Stats_Runtime_AtomicsExchangeEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18348:
	.size	_ZN2v88internal23Runtime_AtomicsExchangeEiPmPNS0_7IsolateE, .-_ZN2v88internal23Runtime_AtomicsExchangeEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL36Stats_Runtime_AtomicsCompareExchangeEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"V8.Runtime_Runtime_AtomicsCompareExchange"
	.section	.text._ZN2v88internalL36Stats_Runtime_AtomicsCompareExchangeEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Stats_Runtime_AtomicsCompareExchangeEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL36Stats_Runtime_AtomicsCompareExchangeEiPmPNS0_7IsolateE.isra.0:
.LFB22083:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movl	$218, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-96(%rbp), %rdi
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	movq	_ZZN2v88internalL36Stats_Runtime_AtomicsCompareExchangeEiPmPNS0_7IsolateEE28trace_event_unique_atomic542(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L61
.L54:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L62
.L55:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L61:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL36Stats_Runtime_AtomicsCompareExchangeEiPmPNS0_7IsolateEE28trace_event_unique_atomic542(%rip)
	movq	%rax, %r12
	jmp	.L54
.L62:
	pxor	%xmm0, %xmm0
	leaq	-48(%rbp), %r13
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC5(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%r13
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	leaq	-40(%rbp), %rdi
	addq	$64, %rsp
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	jmp	.L55
	.cfi_endproc
.LFE22083:
	.size	_ZN2v88internalL36Stats_Runtime_AtomicsCompareExchangeEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL36Stats_Runtime_AtomicsCompareExchangeEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal30Runtime_AtomicsCompareExchangeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Runtime_AtomicsCompareExchangeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Runtime_AtomicsCompareExchangeEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Runtime_AtomicsCompareExchangeEiPmPNS0_7IsolateE:
.LFB18351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L66
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L66:
	movq	%rdx, %rdi
	call	_ZN2v88internalL36Stats_Runtime_AtomicsCompareExchangeEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18351:
	.size	_ZN2v88internal30Runtime_AtomicsCompareExchangeEiPmPNS0_7IsolateE, .-_ZN2v88internal30Runtime_AtomicsCompareExchangeEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL24Stats_Runtime_AtomicsAddEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC6:
	.string	"V8.Runtime_Runtime_AtomicsAdd"
	.section	.text._ZN2v88internalL24Stats_Runtime_AtomicsAddEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL24Stats_Runtime_AtomicsAddEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL24Stats_Runtime_AtomicsAddEiPmPNS0_7IsolateE.isra.0:
.LFB22084:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movl	$216, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-96(%rbp), %rdi
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	movq	_ZZN2v88internalL24Stats_Runtime_AtomicsAddEiPmPNS0_7IsolateEE28trace_event_unique_atomic544(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L76
.L69:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L77
.L70:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L76:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL24Stats_Runtime_AtomicsAddEiPmPNS0_7IsolateEE28trace_event_unique_atomic544(%rip)
	movq	%rax, %r12
	jmp	.L69
.L77:
	pxor	%xmm0, %xmm0
	leaq	-48(%rbp), %r13
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC6(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%r13
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	leaq	-40(%rbp), %rdi
	addq	$64, %rsp
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	jmp	.L70
	.cfi_endproc
.LFE22084:
	.size	_ZN2v88internalL24Stats_Runtime_AtomicsAddEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL24Stats_Runtime_AtomicsAddEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal18Runtime_AtomicsAddEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18Runtime_AtomicsAddEiPmPNS0_7IsolateE
	.type	_ZN2v88internal18Runtime_AtomicsAddEiPmPNS0_7IsolateE, @function
_ZN2v88internal18Runtime_AtomicsAddEiPmPNS0_7IsolateE:
.LFB18354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L81
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L81:
	movq	%rdx, %rdi
	call	_ZN2v88internalL24Stats_Runtime_AtomicsAddEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18354:
	.size	_ZN2v88internal18Runtime_AtomicsAddEiPmPNS0_7IsolateE, .-_ZN2v88internal18Runtime_AtomicsAddEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL24Stats_Runtime_AtomicsSubEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC7:
	.string	"V8.Runtime_Runtime_AtomicsSub"
	.section	.text._ZN2v88internalL24Stats_Runtime_AtomicsSubEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL24Stats_Runtime_AtomicsSubEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL24Stats_Runtime_AtomicsSubEiPmPNS0_7IsolateE.isra.0:
.LFB22085:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movl	$222, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-96(%rbp), %rdi
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	movq	_ZZN2v88internalL24Stats_Runtime_AtomicsSubEiPmPNS0_7IsolateEE28trace_event_unique_atomic546(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L91
.L84:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L92
.L85:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L91:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL24Stats_Runtime_AtomicsSubEiPmPNS0_7IsolateEE28trace_event_unique_atomic546(%rip)
	movq	%rax, %r12
	jmp	.L84
.L92:
	pxor	%xmm0, %xmm0
	leaq	-48(%rbp), %r13
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC7(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%r13
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	leaq	-40(%rbp), %rdi
	addq	$64, %rsp
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	jmp	.L85
	.cfi_endproc
.LFE22085:
	.size	_ZN2v88internalL24Stats_Runtime_AtomicsSubEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL24Stats_Runtime_AtomicsSubEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal18Runtime_AtomicsSubEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18Runtime_AtomicsSubEiPmPNS0_7IsolateE
	.type	_ZN2v88internal18Runtime_AtomicsSubEiPmPNS0_7IsolateE, @function
_ZN2v88internal18Runtime_AtomicsSubEiPmPNS0_7IsolateE:
.LFB18357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L96
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L96:
	movq	%rdx, %rdi
	call	_ZN2v88internalL24Stats_Runtime_AtomicsSubEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18357:
	.size	_ZN2v88internal18Runtime_AtomicsSubEiPmPNS0_7IsolateE, .-_ZN2v88internal18Runtime_AtomicsSubEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL24Stats_Runtime_AtomicsAndEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC8:
	.string	"V8.Runtime_Runtime_AtomicsAnd"
	.section	.text._ZN2v88internalL24Stats_Runtime_AtomicsAndEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL24Stats_Runtime_AtomicsAndEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL24Stats_Runtime_AtomicsAndEiPmPNS0_7IsolateE.isra.0:
.LFB22086:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movl	$217, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-96(%rbp), %rdi
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	movq	_ZZN2v88internalL24Stats_Runtime_AtomicsAndEiPmPNS0_7IsolateEE28trace_event_unique_atomic548(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L106
.L99:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L107
.L100:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L106:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL24Stats_Runtime_AtomicsAndEiPmPNS0_7IsolateEE28trace_event_unique_atomic548(%rip)
	movq	%rax, %r12
	jmp	.L99
.L107:
	pxor	%xmm0, %xmm0
	leaq	-48(%rbp), %r13
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC8(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%r13
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	leaq	-40(%rbp), %rdi
	addq	$64, %rsp
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	jmp	.L100
	.cfi_endproc
.LFE22086:
	.size	_ZN2v88internalL24Stats_Runtime_AtomicsAndEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL24Stats_Runtime_AtomicsAndEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal18Runtime_AtomicsAndEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18Runtime_AtomicsAndEiPmPNS0_7IsolateE
	.type	_ZN2v88internal18Runtime_AtomicsAndEiPmPNS0_7IsolateE, @function
_ZN2v88internal18Runtime_AtomicsAndEiPmPNS0_7IsolateE:
.LFB18360:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L111
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L111:
	movq	%rdx, %rdi
	call	_ZN2v88internalL24Stats_Runtime_AtomicsAndEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18360:
	.size	_ZN2v88internal18Runtime_AtomicsAndEiPmPNS0_7IsolateE, .-_ZN2v88internal18Runtime_AtomicsAndEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL23Stats_Runtime_AtomicsOrEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC9:
	.string	"V8.Runtime_Runtime_AtomicsOr"
	.section	.text._ZN2v88internalL23Stats_Runtime_AtomicsOrEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL23Stats_Runtime_AtomicsOrEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL23Stats_Runtime_AtomicsOrEiPmPNS0_7IsolateE.isra.0:
.LFB22087:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movl	$221, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-96(%rbp), %rdi
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	movq	_ZZN2v88internalL23Stats_Runtime_AtomicsOrEiPmPNS0_7IsolateEE28trace_event_unique_atomic550(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L121
.L114:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L122
.L115:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L121:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL23Stats_Runtime_AtomicsOrEiPmPNS0_7IsolateEE28trace_event_unique_atomic550(%rip)
	movq	%rax, %r12
	jmp	.L114
.L122:
	pxor	%xmm0, %xmm0
	leaq	-48(%rbp), %r13
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC9(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%r13
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	leaq	-40(%rbp), %rdi
	addq	$64, %rsp
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	jmp	.L115
	.cfi_endproc
.LFE22087:
	.size	_ZN2v88internalL23Stats_Runtime_AtomicsOrEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL23Stats_Runtime_AtomicsOrEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal17Runtime_AtomicsOrEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal17Runtime_AtomicsOrEiPmPNS0_7IsolateE
	.type	_ZN2v88internal17Runtime_AtomicsOrEiPmPNS0_7IsolateE, @function
_ZN2v88internal17Runtime_AtomicsOrEiPmPNS0_7IsolateE:
.LFB18363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L126
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L126:
	movq	%rdx, %rdi
	call	_ZN2v88internalL23Stats_Runtime_AtomicsOrEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18363:
	.size	_ZN2v88internal17Runtime_AtomicsOrEiPmPNS0_7IsolateE, .-_ZN2v88internal17Runtime_AtomicsOrEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL24Stats_Runtime_AtomicsXorEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC10:
	.string	"V8.Runtime_Runtime_AtomicsXor"
	.section	.text._ZN2v88internalL24Stats_Runtime_AtomicsXorEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL24Stats_Runtime_AtomicsXorEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL24Stats_Runtime_AtomicsXorEiPmPNS0_7IsolateE.isra.0:
.LFB22088:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movl	$223, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-96(%rbp), %rdi
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	movq	_ZZN2v88internalL24Stats_Runtime_AtomicsXorEiPmPNS0_7IsolateEE28trace_event_unique_atomic552(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L136
.L129:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L137
.L130:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L136:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL24Stats_Runtime_AtomicsXorEiPmPNS0_7IsolateEE28trace_event_unique_atomic552(%rip)
	movq	%rax, %r12
	jmp	.L129
.L137:
	pxor	%xmm0, %xmm0
	leaq	-48(%rbp), %r13
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC10(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%r13
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	leaq	-40(%rbp), %rdi
	addq	$64, %rsp
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	jmp	.L130
	.cfi_endproc
.LFE22088:
	.size	_ZN2v88internalL24Stats_Runtime_AtomicsXorEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL24Stats_Runtime_AtomicsXorEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal18Runtime_AtomicsXorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18Runtime_AtomicsXorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal18Runtime_AtomicsXorEiPmPNS0_7IsolateE, @function
_ZN2v88internal18Runtime_AtomicsXorEiPmPNS0_7IsolateE:
.LFB18366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L141
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L141:
	movq	%rdx, %rdi
	call	_ZN2v88internalL24Stats_Runtime_AtomicsXorEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18366:
	.size	_ZN2v88internal18Runtime_AtomicsXorEiPmPNS0_7IsolateE, .-_ZN2v88internal18Runtime_AtomicsXorEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal21Runtime_AtomicsLoad64EiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal21Runtime_AtomicsLoad64EiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal21Runtime_AtomicsLoad64EiPmPNS0_7IsolateE:
.LFB22076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22076:
	.size	_GLOBAL__sub_I__ZN2v88internal21Runtime_AtomicsLoad64EiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal21Runtime_AtomicsLoad64EiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal21Runtime_AtomicsLoad64EiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL24Stats_Runtime_AtomicsXorEiPmPNS0_7IsolateEE28trace_event_unique_atomic552,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL24Stats_Runtime_AtomicsXorEiPmPNS0_7IsolateEE28trace_event_unique_atomic552, @object
	.size	_ZZN2v88internalL24Stats_Runtime_AtomicsXorEiPmPNS0_7IsolateEE28trace_event_unique_atomic552, 8
_ZZN2v88internalL24Stats_Runtime_AtomicsXorEiPmPNS0_7IsolateEE28trace_event_unique_atomic552:
	.zero	8
	.section	.bss._ZZN2v88internalL23Stats_Runtime_AtomicsOrEiPmPNS0_7IsolateEE28trace_event_unique_atomic550,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL23Stats_Runtime_AtomicsOrEiPmPNS0_7IsolateEE28trace_event_unique_atomic550, @object
	.size	_ZZN2v88internalL23Stats_Runtime_AtomicsOrEiPmPNS0_7IsolateEE28trace_event_unique_atomic550, 8
_ZZN2v88internalL23Stats_Runtime_AtomicsOrEiPmPNS0_7IsolateEE28trace_event_unique_atomic550:
	.zero	8
	.section	.bss._ZZN2v88internalL24Stats_Runtime_AtomicsAndEiPmPNS0_7IsolateEE28trace_event_unique_atomic548,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL24Stats_Runtime_AtomicsAndEiPmPNS0_7IsolateEE28trace_event_unique_atomic548, @object
	.size	_ZZN2v88internalL24Stats_Runtime_AtomicsAndEiPmPNS0_7IsolateEE28trace_event_unique_atomic548, 8
_ZZN2v88internalL24Stats_Runtime_AtomicsAndEiPmPNS0_7IsolateEE28trace_event_unique_atomic548:
	.zero	8
	.section	.bss._ZZN2v88internalL24Stats_Runtime_AtomicsSubEiPmPNS0_7IsolateEE28trace_event_unique_atomic546,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL24Stats_Runtime_AtomicsSubEiPmPNS0_7IsolateEE28trace_event_unique_atomic546, @object
	.size	_ZZN2v88internalL24Stats_Runtime_AtomicsSubEiPmPNS0_7IsolateEE28trace_event_unique_atomic546, 8
_ZZN2v88internalL24Stats_Runtime_AtomicsSubEiPmPNS0_7IsolateEE28trace_event_unique_atomic546:
	.zero	8
	.section	.bss._ZZN2v88internalL24Stats_Runtime_AtomicsAddEiPmPNS0_7IsolateEE28trace_event_unique_atomic544,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL24Stats_Runtime_AtomicsAddEiPmPNS0_7IsolateEE28trace_event_unique_atomic544, @object
	.size	_ZZN2v88internalL24Stats_Runtime_AtomicsAddEiPmPNS0_7IsolateEE28trace_event_unique_atomic544, 8
_ZZN2v88internalL24Stats_Runtime_AtomicsAddEiPmPNS0_7IsolateEE28trace_event_unique_atomic544:
	.zero	8
	.section	.bss._ZZN2v88internalL36Stats_Runtime_AtomicsCompareExchangeEiPmPNS0_7IsolateEE28trace_event_unique_atomic542,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Stats_Runtime_AtomicsCompareExchangeEiPmPNS0_7IsolateEE28trace_event_unique_atomic542, @object
	.size	_ZZN2v88internalL36Stats_Runtime_AtomicsCompareExchangeEiPmPNS0_7IsolateEE28trace_event_unique_atomic542, 8
_ZZN2v88internalL36Stats_Runtime_AtomicsCompareExchangeEiPmPNS0_7IsolateEE28trace_event_unique_atomic542:
	.zero	8
	.section	.bss._ZZN2v88internalL29Stats_Runtime_AtomicsExchangeEiPmPNS0_7IsolateEE28trace_event_unique_atomic540,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Stats_Runtime_AtomicsExchangeEiPmPNS0_7IsolateEE28trace_event_unique_atomic540, @object
	.size	_ZZN2v88internalL29Stats_Runtime_AtomicsExchangeEiPmPNS0_7IsolateEE28trace_event_unique_atomic540, 8
_ZZN2v88internalL29Stats_Runtime_AtomicsExchangeEiPmPNS0_7IsolateEE28trace_event_unique_atomic540:
	.zero	8
	.section	.bss._ZZN2v88internalL28Stats_Runtime_AtomicsStore64EiPmPNS0_7IsolateEE28trace_event_unique_atomic538,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Stats_Runtime_AtomicsStore64EiPmPNS0_7IsolateEE28trace_event_unique_atomic538, @object
	.size	_ZZN2v88internalL28Stats_Runtime_AtomicsStore64EiPmPNS0_7IsolateEE28trace_event_unique_atomic538, 8
_ZZN2v88internalL28Stats_Runtime_AtomicsStore64EiPmPNS0_7IsolateEE28trace_event_unique_atomic538:
	.zero	8
	.section	.bss._ZZN2v88internalL27Stats_Runtime_AtomicsLoad64EiPmPNS0_7IsolateEE28trace_event_unique_atomic536,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL27Stats_Runtime_AtomicsLoad64EiPmPNS0_7IsolateEE28trace_event_unique_atomic536, @object
	.size	_ZZN2v88internalL27Stats_Runtime_AtomicsLoad64EiPmPNS0_7IsolateEE28trace_event_unique_atomic536, 8
_ZZN2v88internalL27Stats_Runtime_AtomicsLoad64EiPmPNS0_7IsolateEE28trace_event_unique_atomic536:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
