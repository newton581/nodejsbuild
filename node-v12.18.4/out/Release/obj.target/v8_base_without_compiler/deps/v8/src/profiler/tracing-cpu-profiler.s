	.file	"tracing-cpu-profiler.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB1935:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE1935:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController21AddTraceStateObserverEPNS0_18TraceStateObserverE,"axG",@progbits,_ZN2v817TracingController21AddTraceStateObserverEPNS0_18TraceStateObserverE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController21AddTraceStateObserverEPNS0_18TraceStateObserverE
	.type	_ZN2v817TracingController21AddTraceStateObserverEPNS0_18TraceStateObserverE, @function
_ZN2v817TracingController21AddTraceStateObserverEPNS0_18TraceStateObserverE:
.LFB1939:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE1939:
	.size	_ZN2v817TracingController21AddTraceStateObserverEPNS0_18TraceStateObserverE, .-_ZN2v817TracingController21AddTraceStateObserverEPNS0_18TraceStateObserverE
	.section	.text._ZN2v817TracingController24RemoveTraceStateObserverEPNS0_18TraceStateObserverE,"axG",@progbits,_ZN2v817TracingController24RemoveTraceStateObserverEPNS0_18TraceStateObserverE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24RemoveTraceStateObserverEPNS0_18TraceStateObserverE
	.type	_ZN2v817TracingController24RemoveTraceStateObserverEPNS0_18TraceStateObserverE, @function
_ZN2v817TracingController24RemoveTraceStateObserverEPNS0_18TraceStateObserverE:
.LFB1940:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE1940:
	.size	_ZN2v817TracingController24RemoveTraceStateObserverEPNS0_18TraceStateObserverE, .-_ZN2v817TracingController24RemoveTraceStateObserverEPNS0_18TraceStateObserverE
	.section	.text._ZN2v88internal22TracingCpuProfilerImpl15OnTraceDisabledEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22TracingCpuProfilerImpl15OnTraceDisabledEv
	.type	_ZN2v88internal22TracingCpuProfilerImpl15OnTraceDisabledEv, @function
_ZN2v88internal22TracingCpuProfilerImpl15OnTraceDisabledEv:
.LFB9052:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	32(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	cmpb	$0, 24(%r12)
	je	.L8
	movb	$0, 24(%r12)
	movq	8(%r12), %rdi
	movq	%r12, %rdx
	leaq	_ZZN2v88internal22TracingCpuProfilerImpl15OnTraceDisabledEvENUlPNS_7IsolateEPvE_4_FUNES3_S4_(%rip), %rsi
	call	_ZN2v88internal7Isolate16RequestInterruptEPFvPNS_7IsolateEPvES4_@PLT
.L8:
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE9052:
	.size	_ZN2v88internal22TracingCpuProfilerImpl15OnTraceDisabledEv, .-_ZN2v88internal22TracingCpuProfilerImpl15OnTraceDisabledEv
	.section	.rodata._ZN2v88internal22TracingCpuProfilerImpl14OnTraceEnabledEv.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.cpu_profiler"
	.section	.text._ZN2v88internal22TracingCpuProfilerImpl14OnTraceEnabledEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22TracingCpuProfilerImpl14OnTraceEnabledEv
	.type	_ZN2v88internal22TracingCpuProfilerImpl14OnTraceEnabledEv, @function
_ZN2v88internal22TracingCpuProfilerImpl14OnTraceEnabledEv:
.LFB9048:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	_ZZN2v88internal22TracingCpuProfilerImpl14OnTraceEnabledEvE27trace_event_unique_atomic27(%rip), %rax
	testq	%rax, %rax
	je	.L16
.L11:
	movzbl	(%rax), %eax
	testb	$5, %al
	jne	.L17
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movb	$1, 24(%r12)
	movq	8(%r12), %rdi
	addq	$8, %rsp
	movq	%r12, %rdx
	leaq	_ZZN2v88internal22TracingCpuProfilerImpl14OnTraceEnabledEvENUlPNS_7IsolateEPvE_4_FUNES3_S4_(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Isolate16RequestInterruptEPFvPNS_7IsolateEPvES4_@PLT
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L18
.L12:
	movq	%rax, _ZZN2v88internal22TracingCpuProfilerImpl14OnTraceEnabledEvE27trace_event_unique_atomic27(%rip)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L18:
	leaq	.LC0(%rip), %rsi
	call	*%rdx
	jmp	.L12
	.cfi_endproc
.LFE9048:
	.size	_ZN2v88internal22TracingCpuProfilerImpl14OnTraceEnabledEv, .-_ZN2v88internal22TracingCpuProfilerImpl14OnTraceEnabledEv
	.section	.rodata._ZZN2v88internal22TracingCpuProfilerImpl15OnTraceDisabledEvENUlPNS_7IsolateEPvE_4_FUNES3_S4_.str1.1,"aMS",@progbits,1
.LC1:
	.string	""
	.section	.text._ZZN2v88internal22TracingCpuProfilerImpl15OnTraceDisabledEvENUlPNS_7IsolateEPvE_4_FUNES3_S4_,"ax",@progbits
	.p2align 4
	.type	_ZZN2v88internal22TracingCpuProfilerImpl15OnTraceDisabledEvENUlPNS_7IsolateEPvE_4_FUNES3_S4_, @function
_ZZN2v88internal22TracingCpuProfilerImpl15OnTraceDisabledEvENUlPNS_7IsolateEPvE_4_FUNES3_S4_:
.LFB9054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	32(%rsi), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L21
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal11CpuProfiler13StopProfilingEPKc@PLT
	movq	16(%rbx), %r13
	movq	$0, 16(%rbx)
	testq	%r13, %r13
	je	.L21
	movq	%r13, %rdi
	call	_ZN2v88internal11CpuProfilerD1Ev@PLT
	movl	$240, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L21:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE9054:
	.size	_ZZN2v88internal22TracingCpuProfilerImpl15OnTraceDisabledEvENUlPNS_7IsolateEPvE_4_FUNES3_S4_, .-_ZZN2v88internal22TracingCpuProfilerImpl15OnTraceDisabledEvENUlPNS_7IsolateEPvE_4_FUNES3_S4_
	.section	.text._ZN2v88internal22TracingCpuProfilerImplD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22TracingCpuProfilerImplD0Ev
	.type	_ZN2v88internal22TracingCpuProfilerImplD0Ev, @function
_ZN2v88internal22TracingCpuProfilerImplD0Ev:
.LFB9047:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal22TracingCpuProfilerImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	32(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L29
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal11CpuProfiler13StopProfilingEPKc@PLT
	movq	16(%r12), %r14
	movq	$0, 16(%r12)
	testq	%r14, %r14
	je	.L29
	movq	%r14, %rdi
	call	_ZN2v88internal11CpuProfilerD1Ev@PLT
	movl	$240, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L29:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*144(%rax)
	leaq	_ZN2v817TracingController24RemoveTraceStateObserverEPNS0_18TraceStateObserverE(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	56(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L39
.L30:
	movq	%r13, %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L31
	movq	%r13, %rdi
	call	_ZN2v88internal11CpuProfilerD1Ev@PLT
	movl	$240, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L31:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movq	%r12, %rsi
	call	*%rax
	jmp	.L30
	.cfi_endproc
.LFE9047:
	.size	_ZN2v88internal22TracingCpuProfilerImplD0Ev, .-_ZN2v88internal22TracingCpuProfilerImplD0Ev
	.section	.text._ZN2v88internal22TracingCpuProfilerImplD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22TracingCpuProfilerImplD2Ev
	.type	_ZN2v88internal22TracingCpuProfilerImplD2Ev, @function
_ZN2v88internal22TracingCpuProfilerImplD2Ev:
.LFB9045:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal22TracingCpuProfilerImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	32(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L43
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal11CpuProfiler13StopProfilingEPKc@PLT
	movq	16(%rbx), %r13
	movq	$0, 16(%rbx)
	testq	%r13, %r13
	je	.L43
	movq	%r13, %rdi
	call	_ZN2v88internal11CpuProfilerD1Ev@PLT
	movl	$240, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L43:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*144(%rax)
	leaq	_ZN2v817TracingController24RemoveTraceStateObserverEPNS0_18TraceStateObserverE(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	56(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L50
.L44:
	movq	%r12, %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L40
	movq	%r12, %rdi
	call	_ZN2v88internal11CpuProfilerD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$240, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movq	%rbx, %rsi
	call	*%rax
	jmp	.L44
	.cfi_endproc
.LFE9045:
	.size	_ZN2v88internal22TracingCpuProfilerImplD2Ev, .-_ZN2v88internal22TracingCpuProfilerImplD2Ev
	.globl	_ZN2v88internal22TracingCpuProfilerImplD1Ev
	.set	_ZN2v88internal22TracingCpuProfilerImplD1Ev,_ZN2v88internal22TracingCpuProfilerImplD2Ev
	.section	.rodata._ZZN2v88internal22TracingCpuProfilerImpl14OnTraceEnabledEvENUlPNS_7IsolateEPvE_4_FUNES3_S4_.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"disabled-by-default-v8.cpu_profiler.hires"
	.section	.text._ZZN2v88internal22TracingCpuProfilerImpl14OnTraceEnabledEvENUlPNS_7IsolateEPvE_4_FUNES3_S4_,"ax",@progbits
	.p2align 4
	.type	_ZZN2v88internal22TracingCpuProfilerImpl14OnTraceEnabledEvENUlPNS_7IsolateEPvE_4_FUNES3_S4_, @function
_ZZN2v88internal22TracingCpuProfilerImpl14OnTraceEnabledEvENUlPNS_7IsolateEPvE_4_FUNES3_S4_:
.LFB9050:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	32(%rsi), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	cmpb	$0, 24(%rbx)
	je	.L65
	cmpq	$0, 16(%rbx)
	je	.L53
.L65:
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	_ZZN2v88internal22TracingCpuProfilerImpl14StartProfilingEvE27trace_event_unique_atomic53(%rip), %rax
	testq	%rax, %rax
	je	.L66
.L55:
	movzbl	(%rax), %eax
	movl	$240, %edi
	andl	$5, %eax
	cmpb	$1, %al
	sbbq	%r14, %r14
	call	_Znwm@PLT
	movq	8(%rbx), %rsi
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%rax, %rdi
	movq	%rax, %r13
	andl	$900, %r14d
	call	_ZN2v88internal11CpuProfilerC1EPNS0_7IsolateENS_22CpuProfilingNamingModeENS_23CpuProfilingLoggingModeE@PLT
	movq	16(%rbx), %r15
	addq	$100, %r14
	movq	%r13, 16(%rbx)
	testq	%r15, %r15
	je	.L58
	movq	%r15, %rdi
	call	_ZN2v88internal11CpuProfilerD1Ev@PLT
	movl	$240, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	movq	16(%rbx), %r13
.L58:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11CpuProfiler21set_sampling_intervalENS_4base9TimeDeltaE@PLT
	movq	16(%rbx), %rdi
	xorl	%ecx, %ecx
	movabsq	$-4294967296, %rdx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal11CpuProfiler14StartProfilingEPKcNS_19CpuProfilingOptionsE@PLT
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L66:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L67
.L56:
	movq	%rax, _ZZN2v88internal22TracingCpuProfilerImpl14StartProfilingEvE27trace_event_unique_atomic53(%rip)
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L67:
	leaq	.LC2(%rip), %rsi
	call	*%rdx
	jmp	.L56
	.cfi_endproc
.LFE9050:
	.size	_ZZN2v88internal22TracingCpuProfilerImpl14OnTraceEnabledEvENUlPNS_7IsolateEPvE_4_FUNES3_S4_, .-_ZZN2v88internal22TracingCpuProfilerImpl14OnTraceEnabledEvENUlPNS_7IsolateEPvE_4_FUNES3_S4_
	.section	.text._ZN2v88internal22TracingCpuProfilerImplC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22TracingCpuProfilerImplC2EPNS0_7IsolateE
	.type	_ZN2v88internal22TracingCpuProfilerImplC2EPNS0_7IsolateE, @function
_ZN2v88internal22TracingCpuProfilerImplC2EPNS0_7IsolateE:
.LFB9042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal22TracingCpuProfilerImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$32, %rdi
	subq	$8, %rsp
	movq	%rsi, -24(%rdi)
	movq	%rax, -32(%rdi)
	movq	$0, -16(%rdi)
	movb	$0, -8(%rdi)
	call	_ZN2v84base5MutexC1Ev@PLT
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*144(%rax)
	leaq	_ZN2v817TracingController21AddTraceStateObserverEPNS0_18TraceStateObserverE(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L71
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE9042:
	.size	_ZN2v88internal22TracingCpuProfilerImplC2EPNS0_7IsolateE, .-_ZN2v88internal22TracingCpuProfilerImplC2EPNS0_7IsolateE
	.globl	_ZN2v88internal22TracingCpuProfilerImplC1EPNS0_7IsolateE
	.set	_ZN2v88internal22TracingCpuProfilerImplC1EPNS0_7IsolateE,_ZN2v88internal22TracingCpuProfilerImplC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal22TracingCpuProfilerImpl14StartProfilingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22TracingCpuProfilerImpl14StartProfilingEv
	.type	_ZN2v88internal22TracingCpuProfilerImpl14StartProfilingEv, @function
_ZN2v88internal22TracingCpuProfilerImpl14StartProfilingEv:
.LFB9056:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	32(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$24, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	cmpb	$0, 24(%rbx)
	je	.L86
	cmpq	$0, 16(%rbx)
	je	.L74
.L86:
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movq	_ZZN2v88internal22TracingCpuProfilerImpl14StartProfilingEvE27trace_event_unique_atomic53(%rip), %rax
	testq	%rax, %rax
	je	.L87
.L76:
	movzbl	(%rax), %eax
	movl	$240, %edi
	andl	$5, %eax
	cmpb	$1, %al
	sbbq	%r14, %r14
	call	_Znwm@PLT
	movq	8(%rbx), %rsi
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%rax, %rdi
	movq	%rax, %r13
	andl	$900, %r14d
	call	_ZN2v88internal11CpuProfilerC1EPNS0_7IsolateENS_22CpuProfilingNamingModeENS_23CpuProfilingLoggingModeE@PLT
	movq	16(%rbx), %r15
	addq	$100, %r14
	movq	%r13, 16(%rbx)
	testq	%r15, %r15
	je	.L79
	movq	%r15, %rdi
	call	_ZN2v88internal11CpuProfilerD1Ev@PLT
	movl	$240, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	movq	16(%rbx), %r13
.L79:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11CpuProfiler21set_sampling_intervalENS_4base9TimeDeltaE@PLT
	movq	16(%rbx), %rdi
	xorl	%ecx, %ecx
	movabsq	$-4294967296, %rdx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal11CpuProfiler14StartProfilingEPKcNS_19CpuProfilingOptionsE@PLT
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L87:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L88
.L77:
	movq	%rax, _ZZN2v88internal22TracingCpuProfilerImpl14StartProfilingEvE27trace_event_unique_atomic53(%rip)
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L88:
	leaq	.LC2(%rip), %rsi
	call	*%rdx
	jmp	.L77
	.cfi_endproc
.LFE9056:
	.size	_ZN2v88internal22TracingCpuProfilerImpl14StartProfilingEv, .-_ZN2v88internal22TracingCpuProfilerImpl14StartProfilingEv
	.section	.text._ZN2v88internal22TracingCpuProfilerImpl13StopProfilingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22TracingCpuProfilerImpl13StopProfilingEv
	.type	_ZN2v88internal22TracingCpuProfilerImpl13StopProfilingEv, @function
_ZN2v88internal22TracingCpuProfilerImpl13StopProfilingEv:
.LFB9057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	32(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L91
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal11CpuProfiler13StopProfilingEPKc@PLT
	movq	16(%rbx), %r13
	movq	$0, 16(%rbx)
	testq	%r13, %r13
	je	.L91
	movq	%r13, %rdi
	call	_ZN2v88internal11CpuProfilerD1Ev@PLT
	movl	$240, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L91:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE9057:
	.size	_ZN2v88internal22TracingCpuProfilerImpl13StopProfilingEv, .-_ZN2v88internal22TracingCpuProfilerImpl13StopProfilingEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal22TracingCpuProfilerImplC2EPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal22TracingCpuProfilerImplC2EPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal22TracingCpuProfilerImplC2EPNS0_7IsolateE:
.LFB10752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE10752:
	.size	_GLOBAL__sub_I__ZN2v88internal22TracingCpuProfilerImplC2EPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal22TracingCpuProfilerImplC2EPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal22TracingCpuProfilerImplC2EPNS0_7IsolateE
	.weak	_ZTVN2v88internal22TracingCpuProfilerImplE
	.section	.data.rel.ro.local._ZTVN2v88internal22TracingCpuProfilerImplE,"awG",@progbits,_ZTVN2v88internal22TracingCpuProfilerImplE,comdat
	.align 8
	.type	_ZTVN2v88internal22TracingCpuProfilerImplE, @object
	.size	_ZTVN2v88internal22TracingCpuProfilerImplE, 48
_ZTVN2v88internal22TracingCpuProfilerImplE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal22TracingCpuProfilerImplD1Ev
	.quad	_ZN2v88internal22TracingCpuProfilerImplD0Ev
	.quad	_ZN2v88internal22TracingCpuProfilerImpl14OnTraceEnabledEv
	.quad	_ZN2v88internal22TracingCpuProfilerImpl15OnTraceDisabledEv
	.section	.bss._ZZN2v88internal22TracingCpuProfilerImpl14StartProfilingEvE27trace_event_unique_atomic53,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal22TracingCpuProfilerImpl14StartProfilingEvE27trace_event_unique_atomic53, @object
	.size	_ZZN2v88internal22TracingCpuProfilerImpl14StartProfilingEvE27trace_event_unique_atomic53, 8
_ZZN2v88internal22TracingCpuProfilerImpl14StartProfilingEvE27trace_event_unique_atomic53:
	.zero	8
	.section	.bss._ZZN2v88internal22TracingCpuProfilerImpl14OnTraceEnabledEvE27trace_event_unique_atomic27,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal22TracingCpuProfilerImpl14OnTraceEnabledEvE27trace_event_unique_atomic27, @object
	.size	_ZZN2v88internal22TracingCpuProfilerImpl14OnTraceEnabledEvE27trace_event_unique_atomic27, 8
_ZZN2v88internal22TracingCpuProfilerImpl14OnTraceEnabledEvE27trace_event_unique_atomic27:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
