	.file	"reloc-info.cc"
	.text
	.section	.text._ZN2v88internal15RelocInfoWriter5WriteEPKNS0_9RelocInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15RelocInfoWriter5WriteEPKNS0_9RelocInfoE
	.type	_ZN2v88internal15RelocInfoWriter5WriteEPKNS0_9RelocInfoE, @function
_ZN2v88internal15RelocInfoWriter5WriteEPKNS0_9RelocInfoE:
.LFB19760:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	subl	8(%rdi), %eax
	movl	%eax, %edx
	movzbl	8(%rsi), %ecx
	sarq	$6, %rdx
	movq	%rdx, %r8
	movq	(%rdi), %rdx
	leaq	-1(%rdx), %r9
	cmpb	$3, %cl
	je	.L50
	testb	%cl, %cl
	je	.L51
	cmpb	$5, %cl
	je	.L52
	movsbl	%cl, %r10d
	testq	%r8, %r8
	je	.L15
	movq	%r9, (%rdi)
	movb	$71, -1(%rdx)
	movl	%eax, %edx
	shrl	$6, %edx
	je	.L17
	.p2align 4,,10
	.p2align 3
.L16:
	movq	(%rdi), %r8
	leaq	-1(%r8), %r9
	movq	%r9, (%rdi)
	leal	(%rdx,%rdx), %r9d
	shrl	$7, %edx
	movb	%r9b, -1(%r8)
	jne	.L16
.L17:
	movq	(%rdi), %rdx
	andl	$63, %eax
	orb	$1, (%rdx)
	movq	(%rdi), %rdx
	leaq	-1(%rdx), %r9
.L15:
	leal	3(,%r10,4), %r8d
	movq	%r9, (%rdi)
	movb	%r8b, -1(%rdx)
	movq	(%rdi), %rdx
	leaq	-1(%rdx), %r8
	movq	%r8, (%rdi)
	movb	%al, -1(%rdx)
	cmpb	$15, %cl
	je	.L53
	leal	-11(%rcx), %eax
	cmpb	$3, %al
	jbe	.L19
	cmpb	$16, %cl
	jne	.L6
.L19:
	movq	(%rdi), %rdx
	movq	16(%rsi), %rax
	leaq	-1(%rdx), %rcx
	movq	%rcx, (%rdi)
	movb	%al, -1(%rdx)
	movq	(%rdi), %rdx
	leaq	-1(%rdx), %rcx
	movq	%rcx, (%rdi)
	movl	%eax, %ecx
	sarl	$8, %ecx
	movb	%cl, -1(%rdx)
	movq	(%rdi), %rdx
	leaq	-1(%rdx), %rcx
	movq	%rcx, (%rdi)
	movl	%eax, %ecx
	sarl	$24, %eax
	sarl	$16, %ecx
	movb	%cl, -1(%rdx)
	movq	(%rdi), %rdx
	leaq	-1(%rdx), %rcx
	movq	%rcx, (%rdi)
	movb	%al, -1(%rdx)
	movq	(%rsi), %rax
	movq	%rax, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	testq	%r8, %r8
	je	.L8
	movq	%r9, (%rdi)
	movb	$71, -1(%rdx)
	movl	%eax, %edx
	shrl	$6, %edx
	je	.L10
	.p2align 4,,10
	.p2align 3
.L9:
	movq	(%rdi), %rcx
	leaq	-1(%rcx), %r8
	movq	%r8, (%rdi)
	leal	(%rdx,%rdx), %r8d
	shrl	$7, %edx
	movb	%r8b, -1(%rcx)
	jne	.L9
.L10:
	movq	(%rdi), %rdx
	andl	$63, %eax
	orb	$1, (%rdx)
	movq	(%rdi), %rdx
	leaq	-1(%rdx), %r9
.L8:
	leal	1(,%rax,4), %eax
	movq	%r9, (%rdi)
	movb	%al, -1(%rdx)
.L6:
	movq	(%rsi), %rax
	movq	%rax, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	testq	%r8, %r8
	je	.L3
	movq	%r9, (%rdi)
	movb	$71, -1(%rdx)
	movl	%eax, %edx
	shrl	$6, %edx
	je	.L5
	.p2align 4,,10
	.p2align 3
.L4:
	movq	(%rdi), %rcx
	leaq	-1(%rcx), %r8
	movq	%r8, (%rdi)
	leal	(%rdx,%rdx), %r8d
	shrl	$7, %edx
	movb	%r8b, -1(%rcx)
	jne	.L4
.L5:
	movq	(%rdi), %rdx
	andl	$63, %eax
	orb	$1, (%rdx)
	movq	(%rdi), %rdx
	leaq	-1(%rdx), %r9
.L3:
	sall	$2, %eax
	movq	%r9, (%rdi)
	movb	%al, -1(%rdx)
	movq	(%rsi), %rax
	movq	%rax, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	testq	%r8, %r8
	je	.L12
	movq	%r9, (%rdi)
	movb	$71, -1(%rdx)
	movl	%eax, %edx
	shrl	$6, %edx
	je	.L14
	.p2align 4,,10
	.p2align 3
.L13:
	movq	(%rdi), %rcx
	leaq	-1(%rcx), %r8
	movq	%r8, (%rdi)
	leal	(%rdx,%rdx), %r8d
	shrl	$7, %edx
	movb	%r8b, -1(%rcx)
	jne	.L13
.L14:
	movq	(%rdi), %rdx
	andl	$63, %eax
	orb	$1, (%rdx)
	movq	(%rdi), %rdx
	leaq	-1(%rdx), %r9
.L12:
	leal	2(,%rax,4), %eax
	movq	%r9, (%rdi)
	movb	%al, -1(%rdx)
	movq	(%rsi), %rax
	movq	%rax, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	movq	(%rdi), %rax
	movq	16(%rsi), %rdx
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdi)
	movb	%dl, -1(%rax)
	movq	(%rsi), %rax
	movq	%rax, 8(%rdi)
	ret
	.cfi_endproc
.LFE19760:
	.size	_ZN2v88internal15RelocInfoWriter5WriteEPKNS0_9RelocInfoE, .-_ZN2v88internal15RelocInfoWriter5WriteEPKNS0_9RelocInfoE
	.section	.text._ZN2v88internal13RelocIterator14AdvanceReadIntEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RelocIterator14AdvanceReadIntEv
	.type	_ZN2v88internal13RelocIterator14AdvanceReadIntEv, @function
_ZN2v88internal13RelocIterator14AdvanceReadIntEv:
.LFB19765:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	-1(%rax), %rdx
	movq	%rdx, (%rdi)
	leaq	-2(%rax), %rdx
	movzbl	-1(%rax), %ecx
	movq	%rdx, (%rdi)
	movzbl	-2(%rax), %edx
	sall	$8, %edx
	orl	%edx, %ecx
	leaq	-3(%rax), %rdx
	movq	%rdx, (%rdi)
	movzbl	-3(%rax), %edx
	sall	$16, %edx
	orl	%ecx, %edx
	leaq	-4(%rax), %rcx
	movq	%rcx, (%rdi)
	movzbl	-4(%rax), %eax
	sall	$24, %eax
	orl	%edx, %eax
	cltq
	movq	%rax, 32(%rdi)
	ret
	.cfi_endproc
.LFE19765:
	.size	_ZN2v88internal13RelocIterator14AdvanceReadIntEv, .-_ZN2v88internal13RelocIterator14AdvanceReadIntEv
	.section	.text._ZN2v88internal13RelocIterator15AdvanceReadDataEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RelocIterator15AdvanceReadDataEv
	.type	_ZN2v88internal13RelocIterator15AdvanceReadDataEv, @function
_ZN2v88internal13RelocIterator15AdvanceReadDataEv:
.LFB19766:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	-1(%rax), %rdx
	movq	%rdx, (%rdi)
	leaq	-2(%rax), %rdx
	movzbl	-1(%rax), %ecx
	movq	%rdx, (%rdi)
	movzbl	-2(%rax), %edx
	salq	$8, %rdx
	orq	%rdx, %rcx
	leaq	-3(%rax), %rdx
	movq	%rdx, (%rdi)
	movzbl	-3(%rax), %edx
	salq	$16, %rdx
	orq	%rcx, %rdx
	leaq	-4(%rax), %rcx
	movq	%rcx, (%rdi)
	movzbl	-4(%rax), %ecx
	salq	$24, %rcx
	orq	%rcx, %rdx
	leaq	-5(%rax), %rcx
	movq	%rcx, (%rdi)
	movzbl	-5(%rax), %ecx
	salq	$32, %rcx
	orq	%rdx, %rcx
	leaq	-6(%rax), %rdx
	movq	%rdx, (%rdi)
	movzbl	-6(%rax), %edx
	salq	$40, %rdx
	orq	%rdx, %rcx
	leaq	-7(%rax), %rdx
	movq	%rdx, (%rdi)
	movzbl	-7(%rax), %edx
	salq	$48, %rdx
	orq	%rcx, %rdx
	leaq	-8(%rax), %rcx
	movq	%rcx, (%rdi)
	movzbl	-8(%rax), %eax
	salq	$56, %rax
	orq	%rdx, %rax
	movq	%rax, 32(%rdi)
	ret
	.cfi_endproc
.LFE19766:
	.size	_ZN2v88internal13RelocIterator15AdvanceReadDataEv, .-_ZN2v88internal13RelocIterator15AdvanceReadDataEv
	.section	.text._ZN2v88internal13RelocIterator21AdvanceReadLongPCJumpEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RelocIterator21AdvanceReadLongPCJumpEv
	.type	_ZN2v88internal13RelocIterator21AdvanceReadLongPCJumpEv, @function
_ZN2v88internal13RelocIterator21AdvanceReadLongPCJumpEv:
.LFB19767:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	leaq	-1(%rdx), %rax
	movq	%rax, (%rdi)
	movzbl	-1(%rdx), %eax
	movl	%eax, %ecx
	sarl	%eax
	andl	$1, %ecx
	jne	.L58
	leaq	-2(%rdx), %rcx
	movq	%rcx, (%rdi)
	movzbl	-2(%rdx), %esi
	movl	%esi, %ecx
	sall	$6, %ecx
	andl	$16256, %ecx
	orl	%ecx, %eax
	andl	$1, %esi
	jne	.L58
	leaq	-3(%rdx), %rcx
	movq	%rcx, (%rdi)
	movzbl	-3(%rdx), %esi
	movl	%esi, %ecx
	sall	$13, %ecx
	andl	$2080768, %ecx
	orl	%ecx, %eax
	andl	$1, %esi
	jne	.L58
	leaq	-4(%rdx), %rcx
	movq	%rcx, (%rdi)
	movzbl	-4(%rdx), %edx
	sarl	%edx
	sall	$21, %edx
	orl	%edx, %eax
.L58:
	sall	$6, %eax
	addq	%rax, 16(%rdi)
	ret
	.cfi_endproc
.LFE19767:
	.size	_ZN2v88internal13RelocIterator21AdvanceReadLongPCJumpEv, .-_ZN2v88internal13RelocIterator21AdvanceReadLongPCJumpEv
	.section	.text._ZN2v88internal13RelocIterator4nextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RelocIterator4nextEv
	.type	_ZN2v88internal13RelocIterator4nextEv, @function
_ZN2v88internal13RelocIterator4nextEv:
.LFB19769:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	8(%rdi), %r8
	cmpq	%r8, %rax
	jbe	.L60
	movq	16(%rdi), %rdx
.L61:
	leaq	-1(%rax), %rsi
	movq	%rsi, (%rdi)
	movzbl	-1(%rax), %ecx
	movl	%ecx, %r9d
	andl	$3, %r9d
	jne	.L62
	shrq	$2, %rcx
	andl	$63, %ecx
	addq	%rcx, %rdx
	movq	%rdx, 16(%rdi)
	testb	$8, 60(%rdi)
	jne	.L80
.L75:
	movq	%rsi, %rax
.L63:
	cmpq	%r8, %rax
	ja	.L61
.L60:
	movb	$1, 56(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	cmpb	$1, %r9b
	je	.L81
	cmpb	$2, %r9b
	je	.L82
	leaq	-2(%rax), %r9
	shrb	$2, %cl
	movq	%r9, (%rdi)
	cmpb	$17, %cl
	je	.L83
	movzbl	-2(%rax), %esi
	addq	%rsi, %rdx
	movl	60(%rdi), %esi
	movq	%rdx, 16(%rdi)
	cmpb	$15, %cl
	je	.L84
	sarl	%cl, %esi
	leal	-11(%rcx), %r10d
	andl	$1, %esi
	cmpb	$3, %r10b
	jbe	.L70
	cmpb	$16, %cl
	jne	.L85
.L70:
	testl	%esi, %esi
	je	.L72
	leaq	-3(%rax), %rdx
	movb	%cl, 24(%rdi)
	movq	%rdx, (%rdi)
	leaq	-4(%rax), %rdx
	movzbl	-3(%rax), %ecx
	movq	%rdx, (%rdi)
	movzbl	-4(%rax), %edx
	sall	$8, %edx
	orl	%edx, %ecx
	leaq	-5(%rax), %rdx
	movq	%rdx, (%rdi)
	movzbl	-5(%rax), %edx
	sall	$16, %edx
	orl	%ecx, %edx
	leaq	-6(%rax), %rcx
	movq	%rcx, (%rdi)
	movzbl	-6(%rax), %eax
	sall	$24, %eax
	orl	%edx, %eax
	cltq
	movq	%rax, 32(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	movb	$3, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	shrq	$2, %rcx
	andl	$63, %ecx
	addq	%rcx, %rdx
	movq	%rdx, 16(%rdi)
	testb	$32, 60(%rdi)
	je	.L75
	movb	$5, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	shrq	$2, %rcx
	andl	$63, %ecx
	addq	%rcx, %rdx
	movq	%rdx, 16(%rdi)
	testb	$1, 60(%rdi)
	je	.L75
	movb	$0, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	leaq	-3(%rax), %rcx
	andl	$32768, %esi
	movq	%rcx, (%rdi)
	je	.L79
	movb	$15, 24(%rdi)
	movzbl	-3(%rax), %eax
	movq	%rax, 32(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	subq	$6, %rax
	movq	%rax, (%rdi)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L83:
	movzbl	-2(%rax), %ecx
	movl	%ecx, %esi
	sarl	%ecx
	andl	$1, %esi
	jne	.L76
	leaq	-3(%rax), %r10
	movq	%r10, (%rdi)
	movzbl	-3(%rax), %r9d
	movl	%r9d, %esi
	sall	$6, %esi
	andl	$16256, %esi
	orl	%esi, %ecx
	andl	$1, %r9d
	jne	.L78
	leaq	-4(%rax), %r10
	movq	%r10, (%rdi)
	movzbl	-4(%rax), %r9d
	movl	%r9d, %esi
	sall	$13, %esi
	andl	$2080768, %esi
	orl	%esi, %ecx
	andl	$1, %r9d
	jne	.L78
	leaq	-5(%rax), %rsi
	movq	%rsi, (%rdi)
	movzbl	-5(%rax), %eax
	sarl	%eax
	sall	$21, %eax
	orl	%eax, %ecx
	movq	%rsi, %rax
.L68:
	sall	$6, %ecx
	addq	%rcx, %rdx
	movq	%rdx, 16(%rdi)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L85:
	testl	%esi, %esi
	je	.L86
	movb	%cl, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	movq	%rcx, %rax
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L86:
	movq	%r9, %rax
	jmp	.L63
.L78:
	movq	%r10, %rax
	jmp	.L68
.L76:
	movq	%r9, %rax
	jmp	.L68
	.cfi_endproc
.LFE19769:
	.size	_ZN2v88internal13RelocIterator4nextEv, .-_ZN2v88internal13RelocIterator4nextEv
	.section	.text._ZN2v88internal13RelocIteratorC2ENS0_4CodeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RelocIteratorC2ENS0_4CodeEi
	.type	_ZN2v88internal13RelocIteratorC2ENS0_4CodeEi, @function
_ZN2v88internal13RelocIteratorC2ENS0_4CodeEi:
.LFB19771:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	7(%rsi), %rax
	movq	%rsi, -64(%rbp)
	movslq	11(%rax), %r14
	leaq	15(%rax), %r13
	call	_ZNK2v88internal4Code17has_constant_poolEv@PLT
	movq	-64(%rbp), %rcx
	addq	%r13, %r14
	testb	%al, %al
	je	.L115
	movl	43(%rcx), %esi
	leaq	63(%rcx), %rax
	movq	%rax, %rdx
	testl	%esi, %esi
	js	.L116
.L91:
	movslq	55(%rcx), %rsi
	addq	%rsi, %rax
	movq	%rax, -72(%rbp)
.L89:
	movq	%r14, %xmm0
	movq	%r13, %xmm1
	movq	$0, 32(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movb	$0, 56(%rbx)
	movups	%xmm0, (%rbx)
	movq	%rcx, %xmm0
	movhps	-72(%rbp), %xmm0
	movl	%r12d, 60(%rbx)
	movq	%rdx, 16(%rbx)
	movups	%xmm0, 40(%rbx)
	testl	%r12d, %r12d
	jne	.L92
	movq	%r13, (%rbx)
.L93:
	movb	$1, 56(%rbx)
	.p2align 4,,10
	.p2align 3
.L87:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L117
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	cmpq	%r14, %r13
	jnb	.L93
	movl	%r12d, %edi
	movl	%r12d, %esi
	movl	%r12d, %eax
	movl	%r12d, %r8d
	sarl	$15, %edi
	sarl	$5, %esi
	andl	$1, %r8d
	sarl	$3, %eax
	andl	$1, %edi
	andl	$1, %esi
	andl	$1, %eax
.L94:
	movzbl	-1(%r14), %ecx
	leaq	-1(%r14), %r9
	movq	%r9, (%rbx)
	movl	%ecx, %r10d
	andl	$3, %r10d
	jne	.L95
	shrq	$2, %rcx
	andl	$63, %ecx
	addq	%rcx, %rdx
	movq	%rdx, 16(%rbx)
	testl	%eax, %eax
	jne	.L118
.L110:
	movq	%r9, %r14
.L96:
	cmpq	%r14, %r13
	jb	.L94
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L95:
	cmpb	$1, %r10b
	je	.L119
	cmpb	$2, %r10b
	je	.L120
	shrb	$2, %cl
	movzbl	-2(%r14), %r9d
	leaq	-2(%r14), %r10
	cmpb	$17, %cl
	je	.L121
	addq	%r9, %rdx
	movq	%r10, (%rbx)
	movq	%rdx, 16(%rbx)
	cmpb	$15, %cl
	je	.L122
	movl	%r12d, %r9d
	leal	-11(%rcx), %r11d
	sarl	%cl, %r9d
	andl	$1, %r9d
	cmpb	$3, %r11b
	jbe	.L104
	cmpb	$16, %cl
	jne	.L123
.L104:
	testl	%r9d, %r9d
	je	.L106
	movzbl	-4(%r14), %edx
	movzbl	-3(%r14), %eax
	subq	$6, %r14
	movb	%cl, 24(%rbx)
	sall	$8, %edx
	orl	%eax, %edx
	movzbl	1(%r14), %eax
	sall	$16, %eax
	orl	%eax, %edx
	movzbl	(%r14), %eax
	movq	%r14, (%rbx)
	sall	$24, %eax
	orl	%edx, %eax
	cltq
	movq	%rax, 32(%rbx)
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L118:
	movb	$3, 24(%rbx)
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L120:
	shrq	$2, %rcx
	andl	$63, %ecx
	addq	%rcx, %rdx
	movq	%rdx, 16(%rbx)
	testl	%esi, %esi
	je	.L110
	movb	$5, 24(%rbx)
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L115:
	movq	$0, -72(%rbp)
	leaq	63(%rcx), %rdx
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L119:
	shrq	$2, %rcx
	andl	$63, %ecx
	addq	%rcx, %rdx
	movq	%rdx, 16(%rbx)
	testl	%r8d, %r8d
	je	.L110
	movb	$0, 24(%rbx)
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L116:
	movq	%r15, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	-64(%rbp), %rcx
	leaq	63(%rcx), %rdx
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L122:
	leaq	-3(%r14), %rcx
	movq	%rcx, (%rbx)
	testl	%edi, %edi
	je	.L113
	movzbl	-3(%r14), %eax
	movb	$15, 24(%rbx)
	movq	%rax, 32(%rbx)
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L106:
	subq	$6, %r14
	movq	%r14, (%rbx)
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L121:
	movzbl	%r9b, %ecx
	sarl	%ecx
	andl	$1, %r9d
	jne	.L124
	movzbl	-3(%r14), %r10d
	leaq	-3(%r14), %r11
	movq	%r11, (%rbx)
	movl	%r10d, %r9d
	sall	$6, %r9d
	andl	$16256, %r9d
	orl	%r9d, %ecx
	andl	$1, %r10d
	jne	.L112
	movzbl	-4(%r14), %r10d
	leaq	-4(%r14), %r11
	movq	%r11, (%rbx)
	movl	%r10d, %r9d
	sall	$13, %r9d
	andl	$2080768, %r9d
	orl	%r9d, %ecx
	andl	$1, %r10d
	jne	.L112
	movzbl	-5(%r14), %r9d
	leaq	-5(%r14), %r10
	movq	%r10, (%rbx)
	movq	%r10, %r14
	sarl	%r9d
	sall	$21, %r9d
	orl	%r9d, %ecx
.L102:
	sall	$6, %ecx
	addq	%rcx, %rdx
	movq	%rdx, 16(%rbx)
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L123:
	testl	%r9d, %r9d
	je	.L125
	movb	%cl, 24(%rbx)
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%rcx, %r14
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L125:
	movq	%r10, %r14
	jmp	.L96
.L112:
	movq	%r11, %r14
	jmp	.L102
.L124:
	movq	%r10, (%rbx)
	movq	%r10, %r14
	jmp	.L102
.L117:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19771:
	.size	_ZN2v88internal13RelocIteratorC2ENS0_4CodeEi, .-_ZN2v88internal13RelocIteratorC2ENS0_4CodeEi
	.globl	_ZN2v88internal13RelocIteratorC1ENS0_4CodeEi
	.set	_ZN2v88internal13RelocIteratorC1ENS0_4CodeEi,_ZN2v88internal13RelocIteratorC2ENS0_4CodeEi
	.section	.text._ZN2v88internal13RelocIteratorC2ENS0_4CodeENS0_9ByteArrayEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RelocIteratorC2ENS0_4CodeENS0_9ByteArrayEi
	.type	_ZN2v88internal13RelocIteratorC2ENS0_4CodeENS0_9ByteArrayEi, @function
_ZN2v88internal13RelocIteratorC2ENS0_4CodeENS0_9ByteArrayEi:
.LFB19774:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-56(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	15(%rdx), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$24, %rsp
	movslq	11(%rdx), %r14
	movq	%rsi, -56(%rbp)
	call	_ZNK2v88internal4Code17has_constant_poolEv@PLT
	movq	-56(%rbp), %rcx
	addq	%r13, %r14
	testb	%al, %al
	je	.L153
	movl	43(%rcx), %esi
	leaq	63(%rcx), %rax
	movq	%rax, %rdx
	testl	%esi, %esi
	js	.L154
.L130:
	movslq	55(%rcx), %rsi
	addq	%rsi, %rax
	movq	%rax, -64(%rbp)
.L128:
	movq	%r14, %xmm0
	movq	%r13, %xmm1
	movq	$0, 32(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movb	$0, 56(%rbx)
	movups	%xmm0, (%rbx)
	movq	%rcx, %xmm0
	movhps	-64(%rbp), %xmm0
	movl	%r12d, 60(%rbx)
	movq	%rdx, 16(%rbx)
	movups	%xmm0, 40(%rbx)
	testl	%r12d, %r12d
	jne	.L131
	movq	%r13, (%rbx)
.L132:
	movb	$1, 56(%rbx)
.L126:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	cmpq	%r13, %r14
	jbe	.L132
	movl	%r12d, %edi
	movl	%r12d, %esi
	movl	%r12d, %eax
	movl	%r12d, %r8d
	sarl	$15, %edi
	sarl	$5, %esi
	andl	$1, %r8d
	sarl	$3, %eax
	andl	$1, %edi
	andl	$1, %esi
	andl	$1, %eax
.L133:
	movzbl	-1(%r14), %ecx
	leaq	-1(%r14), %r9
	movq	%r9, (%rbx)
	movl	%ecx, %r10d
	andl	$3, %r10d
	jne	.L134
	shrq	$2, %rcx
	andl	$63, %ecx
	addq	%rcx, %rdx
	movq	%rdx, 16(%rbx)
	testl	%eax, %eax
	jne	.L155
.L148:
	movq	%r9, %r14
.L135:
	cmpq	%r14, %r13
	jb	.L133
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L134:
	cmpb	$1, %r10b
	je	.L156
	cmpb	$2, %r10b
	je	.L157
	shrb	$2, %cl
	movzbl	-2(%r14), %r9d
	leaq	-2(%r14), %r10
	cmpb	$17, %cl
	je	.L158
	addq	%r9, %rdx
	movq	%r10, (%rbx)
	movq	%rdx, 16(%rbx)
	cmpb	$15, %cl
	je	.L159
	movl	%r12d, %r9d
	leal	-11(%rcx), %r11d
	sarl	%cl, %r9d
	andl	$1, %r9d
	cmpb	$3, %r11b
	jbe	.L143
	cmpb	$16, %cl
	jne	.L160
.L143:
	testl	%r9d, %r9d
	je	.L145
	movzbl	-4(%r14), %edx
	movzbl	-3(%r14), %eax
	subq	$6, %r14
	movb	%cl, 24(%rbx)
	sall	$8, %edx
	orl	%eax, %edx
	movzbl	1(%r14), %eax
	sall	$16, %eax
	orl	%eax, %edx
	movzbl	(%r14), %eax
	movq	%r14, (%rbx)
	sall	$24, %eax
	orl	%edx, %eax
	cltq
	movq	%rax, 32(%rbx)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L155:
	movb	$3, 24(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	shrq	$2, %rcx
	andl	$63, %ecx
	addq	%rcx, %rdx
	movq	%rdx, 16(%rbx)
	testl	%esi, %esi
	je	.L148
	movb	$5, 24(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	movq	$0, -64(%rbp)
	leaq	63(%rcx), %rdx
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L156:
	shrq	$2, %rcx
	andl	$63, %ecx
	addq	%rcx, %rdx
	movq	%rdx, 16(%rbx)
	testl	%r8d, %r8d
	je	.L148
	movb	$0, 24(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	-56(%rbp), %rcx
	leaq	63(%rcx), %rdx
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L159:
	leaq	-3(%r14), %rcx
	movq	%rcx, (%rbx)
	testl	%edi, %edi
	je	.L151
	movzbl	-3(%r14), %eax
	movb	$15, 24(%rbx)
	movq	%rax, 32(%rbx)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L145:
	subq	$6, %r14
	movq	%r14, (%rbx)
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L158:
	movzbl	%r9b, %ecx
	sarl	%ecx
	andl	$1, %r9d
	jne	.L161
	movzbl	-3(%r14), %r10d
	leaq	-3(%r14), %r11
	movq	%r11, (%rbx)
	movl	%r10d, %r9d
	sall	$6, %r9d
	andl	$16256, %r9d
	orl	%r9d, %ecx
	andl	$1, %r10d
	jne	.L150
	movzbl	-4(%r14), %r10d
	leaq	-4(%r14), %r11
	movq	%r11, (%rbx)
	movl	%r10d, %r9d
	sall	$13, %r9d
	andl	$2080768, %r9d
	orl	%r9d, %ecx
	andl	$1, %r10d
	jne	.L150
	movzbl	-5(%r14), %r9d
	leaq	-5(%r14), %r10
	movq	%r10, (%rbx)
	movq	%r10, %r14
	sarl	%r9d
	sall	$21, %r9d
	orl	%r9d, %ecx
.L141:
	sall	$6, %ecx
	addq	%rcx, %rdx
	movq	%rdx, 16(%rbx)
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L160:
	testl	%r9d, %r9d
	je	.L162
	movb	%cl, 24(%rbx)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L151:
	movq	%rcx, %r14
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L162:
	movq	%r10, %r14
	jmp	.L135
.L150:
	movq	%r11, %r14
	jmp	.L141
.L161:
	movq	%r10, (%rbx)
	movq	%r10, %r14
	jmp	.L141
	.cfi_endproc
.LFE19774:
	.size	_ZN2v88internal13RelocIteratorC2ENS0_4CodeENS0_9ByteArrayEi, .-_ZN2v88internal13RelocIteratorC2ENS0_4CodeENS0_9ByteArrayEi
	.globl	_ZN2v88internal13RelocIteratorC1ENS0_4CodeENS0_9ByteArrayEi
	.set	_ZN2v88internal13RelocIteratorC1ENS0_4CodeENS0_9ByteArrayEi,_ZN2v88internal13RelocIteratorC2ENS0_4CodeENS0_9ByteArrayEi
	.section	.text._ZN2v88internal13RelocIteratorC2ENS0_13CodeReferenceEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RelocIteratorC2ENS0_13CodeReferenceEi
	.type	_ZN2v88internal13RelocIteratorC2ENS0_13CodeReferenceEi, @function
_ZN2v88internal13RelocIteratorC2ENS0_13CodeReferenceEi:
.LFB19777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$40, %rsp
	movq	%rdx, -56(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZNK2v88internal13CodeReference16relocation_startEv@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal13CodeReference14relocation_endEv@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal13CodeReference13constant_poolEv@PLT
	movq	%r15, %rdi
	movq	%rax, -72(%rbp)
	call	_ZNK2v88internal13CodeReference17instruction_startEv@PLT
	movq	-72(%rbp), %rdx
	movq	%r14, %xmm0
	movq	%r13, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	$0, 32(%rbx)
	movb	$0, 56(%rbx)
	movl	%r12d, 60(%rbx)
	movq	$0, 40(%rbx)
	movq	%rax, 16(%rbx)
	movq	%rdx, 48(%rbx)
	movups	%xmm0, (%rbx)
	testl	%r12d, %r12d
	jne	.L164
	movq	%r13, (%rbx)
.L165:
	movb	$1, 56(%rbx)
.L163:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	cmpq	%r14, %r13
	jnb	.L165
	movl	%r12d, %edi
	movl	%r12d, %esi
	movl	%r12d, %edx
	movl	%r12d, %r8d
	sarl	$15, %edi
	sarl	$5, %esi
	andl	$1, %r8d
	sarl	$3, %edx
	andl	$1, %edi
	andl	$1, %esi
	andl	$1, %edx
.L166:
	movzbl	-1(%r14), %ecx
	leaq	-1(%r14), %r9
	movq	%r9, (%rbx)
	movl	%ecx, %r10d
	andl	$3, %r10d
	jne	.L167
	shrq	$2, %rcx
	andl	$63, %ecx
	addq	%rcx, %rax
	movq	%rax, 16(%rbx)
	testl	%edx, %edx
	jne	.L186
.L181:
	movq	%r9, %r14
.L168:
	cmpq	%r14, %r13
	jb	.L166
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L167:
	cmpb	$1, %r10b
	je	.L187
	cmpb	$2, %r10b
	je	.L188
	shrb	$2, %cl
	movzbl	-2(%r14), %r9d
	leaq	-2(%r14), %r10
	cmpb	$17, %cl
	je	.L189
	addq	%r9, %rax
	movq	%r10, (%rbx)
	movq	%rax, 16(%rbx)
	cmpb	$15, %cl
	je	.L190
	movl	%r12d, %r9d
	leal	-11(%rcx), %r11d
	sarl	%cl, %r9d
	andl	$1, %r9d
	cmpb	$3, %r11b
	jbe	.L176
	cmpb	$16, %cl
	jne	.L191
.L176:
	testl	%r9d, %r9d
	je	.L178
	movzbl	-4(%r14), %edx
	movzbl	-3(%r14), %eax
	subq	$6, %r14
	movb	%cl, 24(%rbx)
	sall	$8, %edx
	orl	%eax, %edx
	movzbl	1(%r14), %eax
	sall	$16, %eax
	orl	%eax, %edx
	movzbl	(%r14), %eax
	movq	%r14, (%rbx)
	sall	$24, %eax
	orl	%edx, %eax
	cltq
	movq	%rax, 32(%rbx)
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L186:
	movb	$3, 24(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	.cfi_restore_state
	shrq	$2, %rcx
	andl	$63, %ecx
	addq	%rcx, %rax
	movq	%rax, 16(%rbx)
	testl	%esi, %esi
	je	.L181
	movb	$5, 24(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	shrq	$2, %rcx
	andl	$63, %ecx
	addq	%rcx, %rax
	movq	%rax, 16(%rbx)
	testl	%r8d, %r8d
	je	.L181
	movb	$0, 24(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	leaq	-3(%r14), %rcx
	movq	%rcx, (%rbx)
	testl	%edi, %edi
	je	.L184
	movzbl	-3(%r14), %eax
	movb	$15, 24(%rbx)
	movq	%rax, 32(%rbx)
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L178:
	subq	$6, %r14
	movq	%r14, (%rbx)
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L189:
	movzbl	%r9b, %ecx
	sarl	%ecx
	andl	$1, %r9d
	jne	.L192
	movzbl	-3(%r14), %r10d
	leaq	-3(%r14), %r11
	movq	%r11, (%rbx)
	movl	%r10d, %r9d
	sall	$6, %r9d
	andl	$16256, %r9d
	orl	%r9d, %ecx
	andl	$1, %r10d
	jne	.L183
	movzbl	-4(%r14), %r10d
	leaq	-4(%r14), %r11
	movq	%r11, (%rbx)
	movl	%r10d, %r9d
	sall	$13, %r9d
	andl	$2080768, %r9d
	orl	%r9d, %ecx
	andl	$1, %r10d
	jne	.L183
	movzbl	-5(%r14), %r9d
	leaq	-5(%r14), %r10
	movq	%r10, (%rbx)
	movq	%r10, %r14
	sarl	%r9d
	sall	$21, %r9d
	orl	%r9d, %ecx
.L174:
	sall	$6, %ecx
	addq	%rcx, %rax
	movq	%rax, 16(%rbx)
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L191:
	testl	%r9d, %r9d
	je	.L193
	movb	%cl, 24(%rbx)
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L184:
	movq	%rcx, %r14
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L193:
	movq	%r10, %r14
	jmp	.L168
.L183:
	movq	%r11, %r14
	jmp	.L174
.L192:
	movq	%r10, (%rbx)
	movq	%r10, %r14
	jmp	.L174
	.cfi_endproc
.LFE19777:
	.size	_ZN2v88internal13RelocIteratorC2ENS0_13CodeReferenceEi, .-_ZN2v88internal13RelocIteratorC2ENS0_13CodeReferenceEi
	.globl	_ZN2v88internal13RelocIteratorC1ENS0_13CodeReferenceEi
	.set	_ZN2v88internal13RelocIteratorC1ENS0_13CodeReferenceEi,_ZN2v88internal13RelocIteratorC2ENS0_13CodeReferenceEi
	.section	.text._ZN2v88internal13RelocIteratorC2EPNS0_12EmbeddedDataENS0_4CodeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RelocIteratorC2EPNS0_12EmbeddedDataENS0_4CodeEi
	.type	_ZN2v88internal13RelocIteratorC2EPNS0_12EmbeddedDataENS0_4CodeEi, @function
_ZN2v88internal13RelocIteratorC2EPNS0_12EmbeddedDataENS0_4CodeEi:
.LFB19780:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-56(%rbp), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	7(%rdx), %rax
	movq	%rsi, -64(%rbp)
	movslq	11(%rax), %r13
	leaq	15(%rax), %r12
	call	_ZNK2v88internal4Code17has_constant_poolEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	addq	%r12, %r13
	testb	%al, %al
	je	.L221
	movl	43(%rdx), %ecx
	leaq	63(%rdx), %rax
	testl	%ecx, %ecx
	js	.L222
.L198:
	movslq	55(%rdx), %r14
	addq	%rax, %r14
.L196:
	movl	59(%rdx), %esi
	movq	%r8, %rdi
	call	_ZNK2v88internal12EmbeddedData25InstructionStartOfBuiltinEi@PLT
	movq	-56(%rbp), %rdx
	movq	%r13, %xmm0
	movq	%r12, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	$0, 32(%rbx)
	movb	$0, 56(%rbx)
	movl	%r15d, 60(%rbx)
	movq	%rdx, 40(%rbx)
	movq	%rax, 16(%rbx)
	movq	%r14, 48(%rbx)
	movups	%xmm0, (%rbx)
	testl	%r15d, %r15d
	jne	.L199
	movq	%r12, (%rbx)
.L200:
	movb	$1, 56(%rbx)
.L194:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore_state
	cmpq	%r12, %r13
	jbe	.L200
	movl	%r15d, %edi
	movl	%r15d, %esi
	movl	%r15d, %edx
	movl	%r15d, %r8d
	sarl	$15, %edi
	sarl	$5, %esi
	andl	$1, %r8d
	sarl	$3, %edx
	andl	$1, %edi
	andl	$1, %esi
	andl	$1, %edx
.L201:
	movzbl	-1(%r13), %ecx
	leaq	-1(%r13), %r9
	movq	%r9, (%rbx)
	movl	%ecx, %r10d
	andl	$3, %r10d
	jne	.L202
	shrq	$2, %rcx
	andl	$63, %ecx
	addq	%rcx, %rax
	movq	%rax, 16(%rbx)
	testl	%edx, %edx
	jne	.L223
.L216:
	movq	%r9, %r13
.L203:
	cmpq	%r13, %r12
	jb	.L201
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L202:
	cmpb	$1, %r10b
	je	.L224
	cmpb	$2, %r10b
	je	.L225
	shrb	$2, %cl
	movzbl	-2(%r13), %r9d
	leaq	-2(%r13), %r10
	cmpb	$17, %cl
	je	.L226
	addq	%r9, %rax
	movq	%r10, (%rbx)
	movq	%rax, 16(%rbx)
	cmpb	$15, %cl
	je	.L227
	movl	%r15d, %r9d
	leal	-11(%rcx), %r11d
	sarl	%cl, %r9d
	andl	$1, %r9d
	cmpb	$3, %r11b
	jbe	.L211
	cmpb	$16, %cl
	jne	.L228
.L211:
	testl	%r9d, %r9d
	je	.L213
	movzbl	-4(%r13), %edx
	movzbl	-3(%r13), %eax
	subq	$6, %r13
	movb	%cl, 24(%rbx)
	sall	$8, %edx
	orl	%eax, %edx
	movzbl	1(%r13), %eax
	sall	$16, %eax
	orl	%eax, %edx
	movzbl	0(%r13), %eax
	movq	%r13, (%rbx)
	sall	$24, %eax
	orl	%edx, %eax
	cltq
	movq	%rax, 32(%rbx)
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L223:
	movb	$3, 24(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	.cfi_restore_state
	shrq	$2, %rcx
	andl	$63, %ecx
	addq	%rcx, %rax
	movq	%rax, 16(%rbx)
	testl	%esi, %esi
	je	.L216
	movb	$5, 24(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	.cfi_restore_state
	xorl	%r14d, %r14d
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L224:
	shrq	$2, %rcx
	andl	$63, %ecx
	addq	%rcx, %rax
	movq	%rax, 16(%rbx)
	testl	%r8d, %r8d
	je	.L216
	movb	$0, 24(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore_state
	movq	%r14, %rdi
	movq	%r8, -64(%rbp)
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L227:
	leaq	-3(%r13), %rcx
	movq	%rcx, (%rbx)
	testl	%edi, %edi
	je	.L219
	movzbl	-3(%r13), %eax
	movb	$15, 24(%rbx)
	movq	%rax, 32(%rbx)
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L213:
	subq	$6, %r13
	movq	%r13, (%rbx)
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L226:
	movzbl	%r9b, %ecx
	sarl	%ecx
	andl	$1, %r9d
	jne	.L229
	movzbl	-3(%r13), %r10d
	leaq	-3(%r13), %r11
	movq	%r11, (%rbx)
	movl	%r10d, %r9d
	sall	$6, %r9d
	andl	$16256, %r9d
	orl	%r9d, %ecx
	andl	$1, %r10d
	jne	.L218
	movzbl	-4(%r13), %r10d
	leaq	-4(%r13), %r11
	movq	%r11, (%rbx)
	movl	%r10d, %r9d
	sall	$13, %r9d
	andl	$2080768, %r9d
	orl	%r9d, %ecx
	andl	$1, %r10d
	jne	.L218
	movzbl	-5(%r13), %r9d
	leaq	-5(%r13), %r10
	movq	%r10, (%rbx)
	movq	%r10, %r13
	sarl	%r9d
	sall	$21, %r9d
	orl	%r9d, %ecx
.L209:
	sall	$6, %ecx
	addq	%rcx, %rax
	movq	%rax, 16(%rbx)
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L228:
	testl	%r9d, %r9d
	je	.L230
	movb	%cl, 24(%rbx)
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L219:
	movq	%rcx, %r13
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L230:
	movq	%r10, %r13
	jmp	.L203
.L218:
	movq	%r11, %r13
	jmp	.L209
.L229:
	movq	%r10, (%rbx)
	movq	%r10, %r13
	jmp	.L209
	.cfi_endproc
.LFE19780:
	.size	_ZN2v88internal13RelocIteratorC2EPNS0_12EmbeddedDataENS0_4CodeEi, .-_ZN2v88internal13RelocIteratorC2EPNS0_12EmbeddedDataENS0_4CodeEi
	.globl	_ZN2v88internal13RelocIteratorC1EPNS0_12EmbeddedDataENS0_4CodeEi
	.set	_ZN2v88internal13RelocIteratorC1EPNS0_12EmbeddedDataENS0_4CodeEi,_ZN2v88internal13RelocIteratorC2EPNS0_12EmbeddedDataENS0_4CodeEi
	.section	.text._ZN2v88internal13RelocIteratorC2ERKNS0_8CodeDescEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RelocIteratorC2ERKNS0_8CodeDescEi
	.type	_ZN2v88internal13RelocIteratorC2ERKNS0_8CodeDescEi, @function
_ZN2v88internal13RelocIteratorC2ERKNS0_8CodeDescEi:
.LFB19783:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movslq	8(%rsi), %r12
	movslq	52(%rsi), %rcx
	movq	(%rsi), %rax
	movq	$0, 32(%rdi)
	movq	%r12, %rsi
	movb	$0, 56(%rdi)
	subq	%rcx, %rsi
	addq	%rax, %r12
	movl	%edx, 60(%rdi)
	addq	%rax, %rsi
	movq	%r12, %xmm0
	movq	%rax, 16(%rdi)
	movq	%rsi, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rdi)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 40(%rdi)
	testl	%edx, %edx
	jne	.L232
	movq	%rsi, (%rdi)
.L233:
	movb	$1, 56(%rdi)
.L231:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	cmpq	%r12, %rsi
	jnb	.L233
	movl	%edx, %r10d
	movl	%edx, %r9d
	movl	%edx, %r8d
	movl	%edx, %r11d
	sarl	$15, %r10d
	sarl	$5, %r9d
	andl	$1, %r11d
	sarl	$3, %r8d
	andl	$1, %r10d
	andl	$1, %r9d
	andl	$1, %r8d
.L234:
	movzbl	-1(%r12), %ecx
	leaq	-1(%r12), %rbx
	movq	%rbx, (%rdi)
	movl	%ecx, %r13d
	andl	$3, %r13d
	jne	.L235
	shrq	$2, %rcx
	andl	$63, %ecx
	addq	%rcx, %rax
	movq	%rax, 16(%rdi)
	testl	%r8d, %r8d
	jne	.L254
.L249:
	movq	%rbx, %r12
.L236:
	cmpq	%r12, %rsi
	jb	.L234
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L235:
	cmpb	$1, %r13b
	je	.L255
	cmpb	$2, %r13b
	je	.L256
	shrb	$2, %cl
	movzbl	-2(%r12), %ebx
	leaq	-2(%r12), %r13
	cmpb	$17, %cl
	je	.L257
	addq	%rbx, %rax
	movq	%r13, (%rdi)
	movq	%rax, 16(%rdi)
	cmpb	$15, %cl
	je	.L258
	movl	%edx, %ebx
	leal	-11(%rcx), %r14d
	sarl	%cl, %ebx
	andl	$1, %ebx
	cmpb	$3, %r14b
	jbe	.L244
	cmpb	$16, %cl
	jne	.L259
.L244:
	testl	%ebx, %ebx
	je	.L246
	movzbl	-4(%r12), %edx
	movzbl	-3(%r12), %eax
	subq	$6, %r12
	movb	%cl, 24(%rdi)
	sall	$8, %edx
	orl	%eax, %edx
	movzbl	1(%r12), %eax
	sall	$16, %eax
	orl	%eax, %edx
	movzbl	(%r12), %eax
	movq	%r12, (%rdi)
	sall	$24, %eax
	orl	%edx, %eax
	cltq
	movq	%rax, 32(%rdi)
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L254:
	popq	%rbx
	popq	%r12
	movb	$3, 24(%rdi)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	shrq	$2, %rcx
	andl	$63, %ecx
	addq	%rcx, %rax
	movq	%rax, 16(%rdi)
	testl	%r9d, %r9d
	je	.L249
	popq	%rbx
	popq	%r12
	movb	$5, 24(%rdi)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	shrq	$2, %rcx
	andl	$63, %ecx
	addq	%rcx, %rax
	movq	%rax, 16(%rdi)
	testl	%r11d, %r11d
	je	.L249
	popq	%rbx
	popq	%r12
	movb	$0, 24(%rdi)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	leaq	-3(%r12), %rcx
	movq	%rcx, (%rdi)
	testl	%r10d, %r10d
	je	.L252
	movzbl	-3(%r12), %eax
	movb	$15, 24(%rdi)
	movq	%rax, 32(%rdi)
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L246:
	subq	$6, %r12
	movq	%r12, (%rdi)
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L257:
	movzbl	%bl, %ecx
	sarl	%ecx
	andl	$1, %ebx
	jne	.L260
	movzbl	-3(%r12), %r13d
	leaq	-3(%r12), %r14
	movq	%r14, (%rdi)
	movl	%r13d, %ebx
	sall	$6, %ebx
	andl	$16256, %ebx
	orl	%ebx, %ecx
	andl	$1, %r13d
	jne	.L251
	movzbl	-4(%r12), %r13d
	leaq	-4(%r12), %r14
	movq	%r14, (%rdi)
	movl	%r13d, %ebx
	sall	$13, %ebx
	andl	$2080768, %ebx
	orl	%ebx, %ecx
	andl	$1, %r13d
	jne	.L251
	movzbl	-5(%r12), %ebx
	leaq	-5(%r12), %r13
	movq	%r13, (%rdi)
	movq	%r13, %r12
	sarl	%ebx
	sall	$21, %ebx
	orl	%ebx, %ecx
.L242:
	sall	$6, %ecx
	addq	%rcx, %rax
	movq	%rax, 16(%rdi)
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L259:
	testl	%ebx, %ebx
	je	.L261
	movb	%cl, 24(%rdi)
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L252:
	movq	%rcx, %r12
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L261:
	movq	%r13, %r12
	jmp	.L236
.L251:
	movq	%r14, %r12
	jmp	.L242
.L260:
	movq	%r13, (%rdi)
	movq	%r13, %r12
	jmp	.L242
	.cfi_endproc
.LFE19783:
	.size	_ZN2v88internal13RelocIteratorC2ERKNS0_8CodeDescEi, .-_ZN2v88internal13RelocIteratorC2ERKNS0_8CodeDescEi
	.globl	_ZN2v88internal13RelocIteratorC1ERKNS0_8CodeDescEi
	.set	_ZN2v88internal13RelocIteratorC1ERKNS0_8CodeDescEi,_ZN2v88internal13RelocIteratorC2ERKNS0_8CodeDescEi
	.section	.text._ZN2v88internal13RelocIteratorC2ENS0_6VectorIhEENS2_IKhEEmi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RelocIteratorC2ENS0_6VectorIhEENS2_IKhEEmi
	.type	_ZN2v88internal13RelocIteratorC2ENS0_6VectorIhEENS2_IKhEEmi, @function
_ZN2v88internal13RelocIteratorC2ENS0_6VectorIhEENS2_IKhEEmi:
.LFB19788:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	%rcx, %r8
	movq	%rcx, %xmm1
	movq	%rcx, %rax
	movq	%r8, %xmm0
	movq	$0, 32(%rdi)
	punpcklqdq	%xmm1, %xmm0
	movb	$0, 56(%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	$0, 40(%rdi)
	movl	16(%rbp), %edx
	pushq	%r14
	movq	%rsi, 16(%rdi)
	pushq	%r13
	movl	%edx, 60(%rdi)
	pushq	%r12
	movq	%r9, 48(%rdi)
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movups	%xmm0, (%rdi)
	testl	%edx, %edx
	jne	.L263
	movq	%rcx, (%rdi)
.L264:
	movb	$1, 56(%rdi)
.L262:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L263:
	.cfi_restore_state
	cmpq	%rcx, %r8
	jbe	.L264
	movl	%edx, %r11d
	movl	%edx, %r10d
	movl	%edx, %r9d
	movl	%edx, %ebx
	sarl	$15, %r11d
	sarl	$5, %r10d
	andl	$1, %ebx
	sarl	$3, %r9d
	andl	$1, %r11d
	andl	$1, %r10d
	andl	$1, %r9d
.L265:
	movzbl	-1(%r8), %ecx
	leaq	-1(%r8), %r12
	movq	%r12, (%rdi)
	movl	%ecx, %r13d
	andl	$3, %r13d
	jne	.L266
	shrq	$2, %rcx
	andl	$63, %ecx
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
	testl	%r9d, %r9d
	jne	.L285
.L280:
	movq	%r12, %r8
.L267:
	cmpq	%r8, %rax
	jb	.L265
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L266:
	cmpb	$1, %r13b
	je	.L286
	cmpb	$2, %r13b
	je	.L287
	shrb	$2, %cl
	movzbl	-2(%r8), %r12d
	leaq	-2(%r8), %r13
	cmpb	$17, %cl
	je	.L288
	addq	%r12, %rsi
	movq	%r13, (%rdi)
	movq	%rsi, 16(%rdi)
	cmpb	$15, %cl
	je	.L289
	movl	%edx, %r12d
	leal	-11(%rcx), %r14d
	sarl	%cl, %r12d
	andl	$1, %r12d
	cmpb	$3, %r14b
	jbe	.L275
	cmpb	$16, %cl
	jne	.L290
.L275:
	testl	%r12d, %r12d
	je	.L277
	movzbl	-4(%r8), %edx
	movzbl	-3(%r8), %eax
	subq	$6, %r8
	movb	%cl, 24(%rdi)
	sall	$8, %edx
	orl	%eax, %edx
	movzbl	1(%r8), %eax
	sall	$16, %eax
	orl	%eax, %edx
	movzbl	(%r8), %eax
	movq	%r8, (%rdi)
	sall	$24, %eax
	orl	%edx, %eax
	cltq
	movq	%rax, 32(%rdi)
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L285:
	popq	%rbx
	popq	%r12
	movb	$3, 24(%rdi)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L287:
	.cfi_restore_state
	shrq	$2, %rcx
	andl	$63, %ecx
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
	testl	%r10d, %r10d
	je	.L280
	popq	%rbx
	popq	%r12
	movb	$5, 24(%rdi)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L286:
	.cfi_restore_state
	shrq	$2, %rcx
	andl	$63, %ecx
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
	testl	%ebx, %ebx
	je	.L280
	popq	%rbx
	popq	%r12
	movb	$0, 24(%rdi)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	leaq	-3(%r8), %rcx
	movq	%rcx, (%rdi)
	testl	%r11d, %r11d
	je	.L283
	movzbl	-3(%r8), %eax
	movb	$15, 24(%rdi)
	movq	%rax, 32(%rdi)
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L277:
	subq	$6, %r8
	movq	%r8, (%rdi)
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L288:
	movzbl	%r12b, %ecx
	sarl	%ecx
	andl	$1, %r12d
	jne	.L291
	movzbl	-3(%r8), %r13d
	leaq	-3(%r8), %r14
	movq	%r14, (%rdi)
	movl	%r13d, %r12d
	sall	$6, %r12d
	andl	$16256, %r12d
	orl	%r12d, %ecx
	andl	$1, %r13d
	jne	.L282
	movzbl	-4(%r8), %r13d
	leaq	-4(%r8), %r14
	movq	%r14, (%rdi)
	movl	%r13d, %r12d
	sall	$13, %r12d
	andl	$2080768, %r12d
	orl	%r12d, %ecx
	andl	$1, %r13d
	jne	.L282
	leaq	-5(%r8), %r12
	movzbl	-5(%r8), %r8d
	movq	%r12, (%rdi)
	sarl	%r8d
	sall	$21, %r8d
	orl	%r8d, %ecx
	movq	%r12, %r8
.L273:
	sall	$6, %ecx
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L290:
	testl	%r12d, %r12d
	je	.L292
	movb	%cl, 24(%rdi)
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L283:
	movq	%rcx, %r8
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L292:
	movq	%r13, %r8
	jmp	.L267
.L282:
	movq	%r14, %r8
	jmp	.L273
.L291:
	movq	%r13, (%rdi)
	movq	%r13, %r8
	jmp	.L273
	.cfi_endproc
.LFE19788:
	.size	_ZN2v88internal13RelocIteratorC2ENS0_6VectorIhEENS2_IKhEEmi, .-_ZN2v88internal13RelocIteratorC2ENS0_6VectorIhEENS2_IKhEEmi
	.globl	_ZN2v88internal13RelocIteratorC1ENS0_6VectorIhEENS2_IKhEEmi
	.set	_ZN2v88internal13RelocIteratorC1ENS0_6VectorIhEENS2_IKhEEmi,_ZN2v88internal13RelocIteratorC2ENS0_6VectorIhEENS2_IKhEEmi
	.section	.text._ZN2v88internal13RelocIteratorC2ENS0_4CodeEmmPKhS4_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RelocIteratorC2ENS0_4CodeEmmPKhS4_i
	.type	_ZN2v88internal13RelocIteratorC2ENS0_4CodeEmmPKhS4_i, @function
_ZN2v88internal13RelocIteratorC2ENS0_4CodeEmmPKhS4_i:
.LFB19794:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %xmm0
	movq	%r9, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	$0, 32(%rdi)
	movb	$0, 56(%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rsi, 40(%rdi)
	movl	16(%rbp), %eax
	pushq	%r14
	movq	%rdx, 16(%rdi)
	pushq	%r13
	movl	%eax, 60(%rdi)
	pushq	%r12
	movq	%rcx, 48(%rdi)
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movups	%xmm0, (%rdi)
	testl	%eax, %eax
	jne	.L294
	movq	%r9, (%rdi)
.L295:
	movb	$1, 56(%rdi)
.L293:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L294:
	.cfi_restore_state
	cmpq	%r9, %r8
	jbe	.L295
	movl	%eax, %r11d
	movl	%eax, %r10d
	movl	%eax, %esi
	movl	%eax, %ebx
	sarl	$15, %r11d
	sarl	$5, %r10d
	andl	$1, %ebx
	sarl	$3, %esi
	andl	$1, %r11d
	andl	$1, %r10d
	andl	$1, %esi
.L297:
	movzbl	-1(%r8), %ecx
	leaq	-1(%r8), %r12
	movq	%r12, (%rdi)
	movl	%ecx, %r13d
	andl	$3, %r13d
	jne	.L298
	shrq	$2, %rcx
	andl	$63, %ecx
	addq	%rcx, %rdx
	movq	%rdx, 16(%rdi)
	testl	%esi, %esi
	jne	.L317
.L312:
	movq	%r12, %r8
.L299:
	cmpq	%r8, %r9
	jb	.L297
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L298:
	cmpb	$1, %r13b
	je	.L318
	cmpb	$2, %r13b
	je	.L319
	shrb	$2, %cl
	movzbl	-2(%r8), %r12d
	leaq	-2(%r8), %r13
	cmpb	$17, %cl
	je	.L320
	addq	%r12, %rdx
	movq	%r13, (%rdi)
	movq	%rdx, 16(%rdi)
	cmpb	$15, %cl
	je	.L321
	movl	%eax, %r12d
	leal	-11(%rcx), %r14d
	sarl	%cl, %r12d
	andl	$1, %r12d
	cmpb	$3, %r14b
	jbe	.L307
	cmpb	$16, %cl
	jne	.L322
.L307:
	testl	%r12d, %r12d
	je	.L309
	movzbl	-4(%r8), %edx
	movzbl	-3(%r8), %eax
	subq	$6, %r8
	movb	%cl, 24(%rdi)
	sall	$8, %edx
	orl	%eax, %edx
	movzbl	1(%r8), %eax
	sall	$16, %eax
	orl	%eax, %edx
	movzbl	(%r8), %eax
	movq	%r8, (%rdi)
	sall	$24, %eax
	orl	%edx, %eax
	cltq
	movq	%rax, 32(%rdi)
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L317:
	popq	%rbx
	popq	%r12
	movb	$3, 24(%rdi)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	.cfi_restore_state
	shrq	$2, %rcx
	andl	$63, %ecx
	addq	%rcx, %rdx
	movq	%rdx, 16(%rdi)
	testl	%r10d, %r10d
	je	.L312
	popq	%rbx
	popq	%r12
	movb	$5, 24(%rdi)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L318:
	.cfi_restore_state
	shrq	$2, %rcx
	andl	$63, %ecx
	addq	%rcx, %rdx
	movq	%rdx, 16(%rdi)
	testl	%ebx, %ebx
	je	.L312
	popq	%rbx
	popq	%r12
	movb	$0, 24(%rdi)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L321:
	.cfi_restore_state
	leaq	-3(%r8), %rcx
	movq	%rcx, (%rdi)
	testl	%r11d, %r11d
	je	.L315
	movzbl	-3(%r8), %eax
	movb	$15, 24(%rdi)
	movq	%rax, 32(%rdi)
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L309:
	subq	$6, %r8
	movq	%r8, (%rdi)
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L320:
	movzbl	%r12b, %ecx
	sarl	%ecx
	andl	$1, %r12d
	jne	.L323
	movzbl	-3(%r8), %r13d
	leaq	-3(%r8), %r14
	movq	%r14, (%rdi)
	movl	%r13d, %r12d
	sall	$6, %r12d
	andl	$16256, %r12d
	orl	%r12d, %ecx
	andl	$1, %r13d
	jne	.L314
	movzbl	-4(%r8), %r13d
	leaq	-4(%r8), %r14
	movq	%r14, (%rdi)
	movl	%r13d, %r12d
	sall	$13, %r12d
	andl	$2080768, %r12d
	orl	%r12d, %ecx
	andl	$1, %r13d
	jne	.L314
	leaq	-5(%r8), %r12
	movzbl	-5(%r8), %r8d
	movq	%r12, (%rdi)
	sarl	%r8d
	sall	$21, %r8d
	orl	%r8d, %ecx
	movq	%r12, %r8
.L305:
	sall	$6, %ecx
	addq	%rcx, %rdx
	movq	%rdx, 16(%rdi)
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L322:
	testl	%r12d, %r12d
	je	.L324
	movb	%cl, 24(%rdi)
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L315:
	movq	%rcx, %r8
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L324:
	movq	%r13, %r8
	jmp	.L299
.L314:
	movq	%r14, %r8
	jmp	.L305
.L323:
	movq	%r13, (%rdi)
	movq	%r13, %r8
	jmp	.L305
	.cfi_endproc
.LFE19794:
	.size	_ZN2v88internal13RelocIteratorC2ENS0_4CodeEmmPKhS4_i, .-_ZN2v88internal13RelocIteratorC2ENS0_4CodeEmmPKhS4_i
	.globl	_ZN2v88internal13RelocIteratorC1ENS0_4CodeEmmPKhS4_i
	.set	_ZN2v88internal13RelocIteratorC1ENS0_4CodeEmmPKhS4_i,_ZN2v88internal13RelocIteratorC2ENS0_4CodeEmmPKhS4_i
	.section	.text._ZN2v88internal9RelocInfo29OffHeapTargetIsCodedSpeciallyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9RelocInfo29OffHeapTargetIsCodedSpeciallyEv
	.type	_ZN2v88internal9RelocInfo29OffHeapTargetIsCodedSpeciallyEv, @function
_ZN2v88internal9RelocInfo29OffHeapTargetIsCodedSpeciallyEv:
.LFB19796:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE19796:
	.size	_ZN2v88internal9RelocInfo29OffHeapTargetIsCodedSpeciallyEv, .-_ZN2v88internal9RelocInfo29OffHeapTargetIsCodedSpeciallyEv
	.section	.text._ZNK2v88internal9RelocInfo17wasm_call_addressEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9RelocInfo17wasm_call_addressEv
	.type	_ZNK2v88internal9RelocInfo17wasm_call_addressEv, @function
_ZNK2v88internal9RelocInfo17wasm_call_addressEv:
.LFB19797:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movslq	(%rax), %rdx
	leaq	4(%rax,%rdx), %rax
	ret
	.cfi_endproc
.LFE19797:
	.size	_ZNK2v88internal9RelocInfo17wasm_call_addressEv, .-_ZNK2v88internal9RelocInfo17wasm_call_addressEv
	.section	.text._ZN2v88internal9RelocInfo21set_wasm_call_addressEmNS0_15ICacheFlushModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9RelocInfo21set_wasm_call_addressEmNS0_15ICacheFlushModeE
	.type	_ZN2v88internal9RelocInfo21set_wasm_call_addressEmNS0_15ICacheFlushModeE, @function
_ZN2v88internal9RelocInfo21set_wasm_call_addressEmNS0_15ICacheFlushModeE:
.LFB19798:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	subl	$4, %esi
	subl	%edi, %esi
	movl	%esi, (%rdi)
	cmpl	$1, %edx
	jne	.L329
	ret
	.p2align 4,,10
	.p2align 3
.L329:
	movl	$4, %esi
	jmp	_ZN2v88internal21FlushInstructionCacheEPvm@PLT
	.cfi_endproc
.LFE19798:
	.size	_ZN2v88internal9RelocInfo21set_wasm_call_addressEmNS0_15ICacheFlushModeE, .-_ZN2v88internal9RelocInfo21set_wasm_call_addressEmNS0_15ICacheFlushModeE
	.section	.text._ZNK2v88internal9RelocInfo22wasm_stub_call_addressEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9RelocInfo22wasm_stub_call_addressEv
	.type	_ZNK2v88internal9RelocInfo22wasm_stub_call_addressEv, @function
_ZNK2v88internal9RelocInfo22wasm_stub_call_addressEv:
.LFB24195:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movslq	(%rax), %rdx
	leaq	4(%rax,%rdx), %rax
	ret
	.cfi_endproc
.LFE24195:
	.size	_ZNK2v88internal9RelocInfo22wasm_stub_call_addressEv, .-_ZNK2v88internal9RelocInfo22wasm_stub_call_addressEv
	.section	.text._ZN2v88internal9RelocInfo26set_wasm_stub_call_addressEmNS0_15ICacheFlushModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9RelocInfo26set_wasm_stub_call_addressEmNS0_15ICacheFlushModeE
	.type	_ZN2v88internal9RelocInfo26set_wasm_stub_call_addressEmNS0_15ICacheFlushModeE, @function
_ZN2v88internal9RelocInfo26set_wasm_stub_call_addressEmNS0_15ICacheFlushModeE:
.LFB24197:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	subl	$4, %esi
	subl	%edi, %esi
	movl	%esi, (%rdi)
	cmpl	$1, %edx
	jne	.L333
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	movl	$4, %esi
	jmp	_ZN2v88internal21FlushInstructionCacheEPvm@PLT
	.cfi_endproc
.LFE24197:
	.size	_ZN2v88internal9RelocInfo26set_wasm_stub_call_addressEmNS0_15ICacheFlushModeE, .-_ZN2v88internal9RelocInfo26set_wasm_stub_call_addressEmNS0_15ICacheFlushModeE
	.section	.rodata._ZN2v88internal9RelocInfo18set_target_addressEmNS0_16WriteBarrierModeENS0_15ICacheFlushModeE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"address < start || address >= end"
	.section	.rodata._ZN2v88internal9RelocInfo18set_target_addressEmNS0_16WriteBarrierModeENS0_15ICacheFlushModeE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal9RelocInfo18set_target_addressEmNS0_16WriteBarrierModeENS0_15ICacheFlushModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9RelocInfo18set_target_addressEmNS0_16WriteBarrierModeENS0_15ICacheFlushModeE
	.type	_ZN2v88internal9RelocInfo18set_target_addressEmNS0_16WriteBarrierModeENS0_15ICacheFlushModeE, @function
_ZN2v88internal9RelocInfo18set_target_addressEmNS0_16WriteBarrierModeENS0_15ICacheFlushModeE:
.LFB19801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-4(%rsi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	subl	%edi, %eax
	movl	%eax, (%rdi)
	cmpl	$1, %ecx
	je	.L335
	movl	$4, %esi
	call	_ZN2v88internal21FlushInstructionCacheEPvm@PLT
.L335:
	cmpl	$4, %r13d
	je	.L352
.L334:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L352:
	.cfi_restore_state
	cmpq	$0, 24(%r12)
	je	.L334
	cmpb	$1, 8(%r12)
	jg	.L334
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movq	%rax, %r13
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %eax
	addq	%r13, %rax
	cmpq	%rax, %rbx
	jnb	.L337
	cmpq	%rbx, %r13
	jbe	.L353
.L337:
	leaq	-63(%rbx), %rdx
	movq	24(%r12), %rdi
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L334
	addq	$8, %rsp
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal30Heap_MarkingBarrierForCodeSlowENS0_4CodeEPNS0_9RelocInfoENS0_10HeapObjectE@PLT
.L353:
	.cfi_restore_state
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19801:
	.size	_ZN2v88internal9RelocInfo18set_target_addressEmNS0_16WriteBarrierModeENS0_15ICacheFlushModeE, .-_ZN2v88internal9RelocInfo18set_target_addressEmNS0_16WriteBarrierModeENS0_15ICacheFlushModeE
	.section	.text._ZNK2v88internal9RelocInfo23HasTargetAddressAddressEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9RelocInfo23HasTargetAddressAddressEv
	.type	_ZNK2v88internal9RelocInfo23HasTargetAddressAddressEv, @function
_ZNK2v88internal9RelocInfo23HasTargetAddressAddressEv:
.LFB19802:
	.cfi_startproc
	endbr64
	movsbl	8(%rdi), %ecx
	movl	$1, %eax
	sall	%cl, %eax
	testl	$1277, %eax
	setne	%al
	ret
	.cfi_endproc
.LFE19802:
	.size	_ZNK2v88internal9RelocInfo23HasTargetAddressAddressEv, .-_ZNK2v88internal9RelocInfo23HasTargetAddressAddressEv
	.section	.text._ZN2v88internal9RelocInfo30RequiresRelocationAfterCodegenERKNS0_8CodeDescE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9RelocInfo30RequiresRelocationAfterCodegenERKNS0_8CodeDescE
	.type	_ZN2v88internal9RelocInfo30RequiresRelocationAfterCodegenERKNS0_8CodeDescE, @function
_ZN2v88internal9RelocInfo30RequiresRelocationAfterCodegenERKNS0_8CodeDescE:
.LFB19803:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	movslq	52(%rdi), %rcx
	movq	(%rdi), %rdx
	movl	_ZN2v88internal9RelocInfo10kApplyMaskE(%rip), %esi
	movq	%rax, %rdi
	subq	%rcx, %rdi
	orl	$79, %esi
	addq	%rdx, %rdi
	addq	%rax, %rdx
	cmpq	%rdx, %rdi
	jnb	.L403
	movl	%esi, %eax
	sarl	$5, %eax
	andl	$1, %eax
	testl	$32768, %esi
	jne	.L414
	testl	%eax, %eax
	jne	.L366
	movzbl	-1(%rdx), %ecx
	movl	%ecx, %eax
	andl	$3, %eax
	je	.L408
.L418:
	cmpb	$1, %al
	je	.L408
	cmpb	$2, %al
	je	.L415
	shrb	$2, %cl
	leaq	-2(%rdx), %r9
	cmpb	$17, %cl
	je	.L416
	cmpb	$15, %cl
	je	.L411
	leal	-11(%rcx), %eax
	movl	%esi, %r11d
	cmpb	$3, %al
	setbe	%al
	cmpb	$16, %cl
	sete	%r8b
	sarl	%cl, %r11d
	movl	%r11d, %ecx
	andl	$1, %ecx
	orb	%r8b, %al
	je	.L417
	testl	%ecx, %ecx
	jne	.L355
	subq	$6, %rdx
.L368:
	cmpq	%rdx, %rdi
	jnb	.L403
	movzbl	-1(%rdx), %ecx
	movl	%ecx, %eax
	andl	$3, %eax
	jne	.L418
	.p2align 4,,10
	.p2align 3
.L408:
	movl	$1, %eax
.L355:
	ret
	.p2align 4,,10
	.p2align 3
.L414:
	testl	%eax, %eax
	jne	.L358
.L365:
	movzbl	-1(%rdx), %ecx
	movl	%ecx, %eax
	andl	$3, %eax
	je	.L408
	cmpb	$1, %al
	je	.L408
	cmpb	$2, %al
	je	.L359
	shrb	$2, %cl
	leaq	-2(%rdx), %r9
	cmpb	$17, %cl
	je	.L360
	cmpb	$15, %cl
	je	.L408
	leal	-11(%rcx), %eax
	movl	%esi, %r10d
	cmpb	$3, %al
	setbe	%al
	cmpb	$16, %cl
	sete	%r8b
	sarl	%cl, %r10d
	movl	%r10d, %ecx
	andl	$1, %ecx
	orb	%r8b, %al
	je	.L361
	testl	%ecx, %ecx
	jne	.L355
	subq	$6, %rdx
.L362:
	cmpq	%rdx, %rdi
	jb	.L365
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L358:
	movzbl	-1(%rdx), %ecx
	movl	%ecx, %eax
	andl	$3, %eax
	je	.L408
	subl	$1, %eax
	cmpb	$1, %al
	jbe	.L408
	shrb	$2, %cl
	leaq	-2(%rdx), %r9
	cmpb	$17, %cl
	je	.L419
	cmpb	$15, %cl
	je	.L408
	leal	-11(%rcx), %eax
	movl	%esi, %r11d
	cmpb	$3, %al
	setbe	%al
	cmpb	$16, %cl
	sete	%r8b
	sarl	%cl, %r11d
	movl	%r11d, %ecx
	andl	$1, %ecx
	orb	%r8b, %al
	je	.L420
	testl	%ecx, %ecx
	jne	.L355
	subq	$6, %rdx
.L382:
	cmpq	%rdx, %rdi
	jb	.L358
.L403:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L421:
	shrb	$2, %cl
	leaq	-2(%rdx), %r9
	cmpb	$17, %cl
	je	.L375
	cmpb	$15, %cl
	je	.L412
	leal	-11(%rcx), %eax
	movl	%esi, %r10d
	cmpb	$3, %al
	setbe	%al
	cmpb	$16, %cl
	sete	%r8b
	sarl	%cl, %r10d
	movl	%r10d, %ecx
	andl	$1, %ecx
	orb	%r8b, %al
	je	.L377
	testl	%ecx, %ecx
	jne	.L355
	subq	$6, %rdx
.L378:
	cmpq	%rdx, %rdi
	jnb	.L403
.L366:
	movzbl	-1(%rdx), %ecx
	movl	%ecx, %eax
	andl	$3, %eax
	je	.L408
	subl	$1, %eax
	cmpb	$1, %al
	ja	.L421
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L417:
	testl	%ecx, %ecx
	jne	.L408
.L395:
	movq	%r9, %rdx
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L377:
	testl	%ecx, %ecx
	jne	.L408
.L401:
	movq	%r9, %rdx
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L361:
	testl	%ecx, %ecx
	jne	.L408
.L391:
	movq	%r9, %rdx
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L420:
	testl	%ecx, %ecx
	jne	.L408
.L406:
	movq	%r9, %rdx
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L415:
	subq	$1, %rdx
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L359:
	subq	$1, %rdx
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L416:
	testb	$1, -2(%rdx)
	jne	.L395
	testb	$1, -3(%rdx)
	jne	.L411
	testb	$1, -4(%rdx)
	movq	%rdx, %rax
	setne	%dl
	movzbl	%dl, %edx
	leaq	-5(%rdx,%rax), %rdx
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L419:
	testb	$1, -2(%rdx)
	jne	.L406
	testb	$1, -3(%rdx)
	jne	.L422
	testb	$1, -4(%rdx)
	movq	%rdx, %rax
	setne	%dl
	movzbl	%dl, %edx
	leaq	-5(%rdx,%rax), %rdx
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L375:
	testb	$1, -2(%rdx)
	jne	.L401
	testb	$1, -3(%rdx)
	jne	.L412
	testb	$1, -4(%rdx)
	movq	%rdx, %rax
	setne	%dl
	movzbl	%dl, %edx
	leaq	-5(%rdx,%rax), %rdx
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L360:
	testb	$1, -2(%rdx)
	jne	.L391
	testb	$1, -3(%rdx)
	jne	.L423
	testb	$1, -4(%rdx)
	movq	%rdx, %rax
	setne	%dl
	movzbl	%dl, %edx
	leaq	-5(%rdx,%rax), %rdx
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L412:
	subq	$3, %rdx
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L411:
	subq	$3, %rdx
	jmp	.L368
.L423:
	subq	$3, %rdx
	jmp	.L362
.L422:
	subq	$3, %rdx
	jmp	.L382
	.cfi_endproc
.LFE19803:
	.size	_ZN2v88internal9RelocInfo30RequiresRelocationAfterCodegenERKNS0_8CodeDescE, .-_ZN2v88internal9RelocInfo30RequiresRelocationAfterCodegenERKNS0_8CodeDescE
	.section	.text._ZN2v88internal9RelocInfo18RequiresRelocationENS0_4CodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9RelocInfo18RequiresRelocationENS0_4CodeE
	.type	_ZN2v88internal9RelocInfo18RequiresRelocationENS0_4CodeE, @function
_ZN2v88internal9RelocInfo18RequiresRelocationENS0_4CodeE:
.LFB19804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-48(%rbp), %r14
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	_ZN2v88internal9RelocInfo10kApplyMaskE(%rip), %r12d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	7(%rdi), %rax
	movq	%rdi, -48(%rbp)
	movq	%r14, %rdi
	movslq	11(%rax), %rbx
	leaq	15(%rax), %r13
	addq	%r13, %rbx
	call	_ZNK2v88internal4Code17has_constant_poolEv@PLT
	testb	%al, %al
	je	.L425
	movq	-48(%rbp), %rax
	movl	43(%rax), %eax
	testl	%eax, %eax
	js	.L670
.L425:
	xorl	%eax, %eax
	testl	%r12d, %r12d
	je	.L424
	cmpq	%rbx, %r13
	jnb	.L424
	movl	%r12d, %eax
	movl	%r12d, %ecx
	sarl	$15, %eax
	andl	$1, %ecx
	andl	$1, %eax
	movl	%eax, %edx
	movl	%r12d, %eax
	sarl	$5, %eax
	andl	$1, %eax
	testb	$8, %r12b
	jne	.L671
	testl	%eax, %eax
	jne	.L478
	testl	%ecx, %ecx
	jne	.L479
	testl	%edx, %edx
	jne	.L480
.L487:
	movzbl	-1(%rbx), %ecx
	leaq	-1(%rbx), %rdx
	movl	%ecx, %eax
	andl	$3, %eax
	je	.L481
	subl	$1, %eax
	cmpb	$1, %al
	jbe	.L481
	shrb	$2, %cl
	leaq	-2(%rbx), %rdx
	cmpb	$17, %cl
	je	.L672
	cmpb	$15, %cl
	je	.L666
	leal	-11(%rcx), %eax
	movl	%r12d, %edi
	cmpb	$3, %al
	setbe	%sil
	cmpb	$16, %cl
	sete	%al
	sarl	%cl, %edi
	movl	%edi, %ecx
	andl	$1, %ecx
	orb	%sil, %al
	je	.L673
	testl	%ecx, %ecx
	jne	.L424
	leaq	-6(%rbx), %rdx
	.p2align 4,,10
	.p2align 3
.L481:
	cmpq	%rdx, %r13
	jnb	.L613
	movq	%rdx, %rbx
	jmp	.L487
.L673:
	testl	%ecx, %ecx
	je	.L481
	.p2align 4,,10
	.p2align 3
.L618:
	movl	$1, %eax
.L424:
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L674
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L671:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L428
	testl	%ecx, %ecx
	jne	.L675
	testl	%edx, %edx
	jne	.L438
.L445:
	movzbl	-1(%rbx), %ecx
	leaq	-1(%rbx), %rdx
	movl	%ecx, %eax
	andl	$3, %eax
	je	.L618
	cmpb	$1, %al
	je	.L547
	cmpb	$2, %al
	je	.L618
	shrb	$2, %cl
	leaq	-2(%rbx), %rdi
	cmpb	$17, %cl
	je	.L676
	cmpb	$15, %cl
	je	.L663
	leal	-11(%rcx), %eax
	movl	%r12d, %edx
	cmpb	$3, %al
	setbe	%sil
	cmpb	$16, %cl
	sete	%al
	sarl	%cl, %edx
	andl	$1, %edx
	orb	%sil, %al
	je	.L677
	testl	%edx, %edx
	jne	.L424
	subq	$6, %rbx
	.p2align 4,,10
	.p2align 3
.L439:
	cmpq	%rbx, %r13
	jb	.L445
	.p2align 4,,10
	.p2align 3
.L613:
	xorl	%eax, %eax
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L428:
	testl	%ecx, %ecx
	jne	.L451
	testl	%edx, %edx
	jne	.L452
.L459:
	movzbl	-1(%rbx), %ecx
	leaq	-1(%rbx), %rdx
	movl	%ecx, %eax
	andl	$3, %eax
	je	.L618
	subl	$1, %eax
	cmpb	$1, %al
	jbe	.L560
	shrb	$2, %cl
	leaq	-2(%rbx), %rdi
	cmpb	$17, %cl
	je	.L454
	cmpb	$15, %cl
	je	.L664
	leal	-11(%rcx), %eax
	movl	%r12d, %edx
	cmpb	$3, %al
	setbe	%sil
	cmpb	$16, %cl
	sete	%al
	sarl	%cl, %edx
	andl	$1, %edx
	orb	%sil, %al
	je	.L456
	testl	%edx, %edx
	jne	.L424
	subq	$6, %rbx
	.p2align 4,,10
	.p2align 3
.L453:
	cmpq	%rbx, %r13
	jb	.L459
	xorl	%eax, %eax
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L478:
	testl	%ecx, %ecx
	jne	.L506
	testl	%edx, %edx
	jne	.L507
.L514:
	movzbl	-1(%rbx), %ecx
	leaq	-1(%rbx), %rdx
	movl	%ecx, %eax
	andl	$3, %eax
	je	.L508
	cmpb	$1, %al
	je	.L508
	cmpb	$2, %al
	je	.L618
	shrb	$2, %cl
	leaq	-2(%rbx), %rdx
	cmpb	$17, %cl
	je	.L509
	cmpb	$15, %cl
	je	.L668
	leal	-11(%rcx), %eax
	movl	%r12d, %edi
	cmpb	$3, %al
	setbe	%sil
	cmpb	$16, %cl
	sete	%al
	sarl	%cl, %edi
	movl	%edi, %ecx
	andl	$1, %ecx
	orb	%sil, %al
	je	.L511
	testl	%ecx, %ecx
	jne	.L424
	leaq	-6(%rbx), %rdx
	.p2align 4,,10
	.p2align 3
.L508:
	cmpq	%rdx, %r13
	jnb	.L613
	movq	%rdx, %rbx
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L670:
	movq	%r14, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L479:
	testl	%edx, %edx
	jne	.L493
.L500:
	movzbl	-1(%rbx), %ecx
	leaq	-1(%rbx), %rdx
	movl	%ecx, %eax
	andl	$3, %eax
	je	.L494
	cmpb	$1, %al
	je	.L618
	cmpb	$2, %al
	je	.L494
	shrb	$2, %cl
	leaq	-2(%rbx), %rdx
	cmpb	$17, %cl
	je	.L495
	cmpb	$15, %cl
	je	.L667
	leal	-11(%rcx), %eax
	movl	%r12d, %edi
	cmpb	$3, %al
	setbe	%sil
	cmpb	$16, %cl
	sete	%al
	sarl	%cl, %edi
	movl	%edi, %ecx
	andl	$1, %ecx
	orb	%sil, %al
	je	.L497
	testl	%ecx, %ecx
	jne	.L424
	leaq	-6(%rbx), %rdx
	.p2align 4,,10
	.p2align 3
.L494:
	cmpq	%rdx, %r13
	jnb	.L613
	movq	%rdx, %rbx
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L506:
	testl	%edx, %edx
	jne	.L520
.L527:
	movzbl	-1(%rbx), %ecx
	leaq	-1(%rbx), %rdx
	movl	%ecx, %eax
	andl	$3, %eax
	je	.L602
	subl	$1, %eax
	cmpb	$1, %al
	jbe	.L618
	shrb	$2, %cl
	leaq	-2(%rbx), %rdi
	cmpb	$17, %cl
	je	.L678
	cmpb	$15, %cl
	je	.L669
	leal	-11(%rcx), %eax
	movl	%r12d, %edx
	cmpb	$3, %al
	setbe	%sil
	cmpb	$16, %cl
	sete	%al
	sarl	%cl, %edx
	andl	$1, %edx
	orb	%sil, %al
	je	.L679
	testl	%edx, %edx
	jne	.L424
	subq	$6, %rbx
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L675:
	testl	%edx, %edx
	jne	.L430
.L437:
	movzbl	-1(%rbx), %ecx
	movl	%ecx, %eax
	andl	$3, %eax
	je	.L618
	subl	$1, %eax
	cmpb	$1, %al
	jbe	.L618
	shrb	$2, %cl
	leaq	-2(%rbx), %rdi
	cmpb	$17, %cl
	je	.L431
	cmpb	$15, %cl
	je	.L662
	leal	-11(%rcx), %eax
	movl	%r12d, %edx
	cmpb	$3, %al
	setbe	%sil
	cmpb	$16, %cl
	sete	%al
	sarl	%cl, %edx
	andl	$1, %edx
	orb	%sil, %al
	je	.L433
	testl	%edx, %edx
	jne	.L424
	subq	$6, %rbx
.L434:
	cmpq	%rbx, %r13
	jb	.L437
	xorl	%eax, %eax
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L451:
	testl	%edx, %edx
	jne	.L465
.L472:
	movzbl	-1(%rbx), %ecx
	leaq	-1(%rbx), %rdx
	movl	%ecx, %eax
	andl	$3, %eax
	je	.L618
	cmpb	$1, %al
	je	.L618
	cmpb	$2, %al
	je	.L572
	shrb	$2, %cl
	leaq	-2(%rbx), %rdi
	cmpb	$17, %cl
	je	.L680
	cmpb	$15, %cl
	je	.L665
	leal	-11(%rcx), %eax
	movl	%r12d, %edx
	cmpb	$3, %al
	setbe	%sil
	cmpb	$16, %cl
	sete	%al
	sarl	%cl, %edx
	andl	$1, %edx
	orb	%sil, %al
	je	.L681
	testl	%edx, %edx
	jne	.L424
	subq	$6, %rbx
.L466:
	cmpq	%rbx, %r13
	jb	.L472
	xorl	%eax, %eax
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L684:
	shrb	$2, %cl
	leaq	-2(%rbx), %rdi
	cmpb	$17, %cl
	je	.L682
	cmpb	$15, %cl
	je	.L618
	leal	-11(%rcx), %eax
	movl	%r12d, %edx
	cmpb	$3, %al
	setbe	%sil
	cmpb	$16, %cl
	sete	%al
	sarl	%cl, %edx
	andl	$1, %edx
	orb	%sil, %al
	je	.L683
	testl	%edx, %edx
	jne	.L424
	subq	$6, %rbx
.L534:
	cmpq	%rbx, %r13
	jnb	.L613
.L430:
	movzbl	-1(%rbx), %ecx
	movl	%ecx, %eax
	andl	$3, %eax
	je	.L618
	subl	$1, %eax
	cmpb	$1, %al
	ja	.L684
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L687:
	shrb	$2, %cl
	leaq	-2(%rbx), %rdi
	cmpb	$17, %cl
	je	.L685
	cmpb	$15, %cl
	je	.L618
	leal	-11(%rcx), %eax
	movl	%r12d, %edx
	cmpb	$3, %al
	setbe	%sil
	cmpb	$16, %cl
	sete	%al
	sarl	%cl, %edx
	andl	$1, %edx
	orb	%sil, %al
	je	.L686
	testl	%edx, %edx
	jne	.L424
	subq	$6, %rbx
	.p2align 4,,10
	.p2align 3
.L460:
	cmpq	%rbx, %r13
	jnb	.L613
.L452:
	movzbl	-1(%rbx), %ecx
	leaq	-1(%rbx), %rdx
	movl	%ecx, %eax
	andl	$3, %eax
	je	.L618
	subl	$1, %eax
	cmpb	$1, %al
	ja	.L687
	movq	%rdx, %rbx
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L688:
	shrb	$2, %cl
	leaq	-2(%rbx), %rdx
	cmpb	$17, %cl
	je	.L489
	cmpb	$15, %cl
	je	.L618
	leal	-11(%rcx), %eax
	movl	%r12d, %edi
	cmpb	$3, %al
	setbe	%sil
	cmpb	$16, %cl
	sete	%al
	sarl	%cl, %edi
	movl	%edi, %ecx
	andl	$1, %ecx
	orb	%sil, %al
	je	.L490
	testl	%ecx, %ecx
	jne	.L424
	leaq	-6(%rbx), %rdx
	.p2align 4,,10
	.p2align 3
.L488:
	cmpq	%rdx, %r13
	jnb	.L613
	movq	%rdx, %rbx
.L480:
	movzbl	-1(%rbx), %ecx
	leaq	-1(%rbx), %rdx
	movl	%ecx, %eax
	andl	$3, %eax
	je	.L488
	subl	$1, %eax
	cmpb	$1, %al
	ja	.L688
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L691:
	cmpb	$2, %al
	je	.L501
	shrb	$2, %cl
	leaq	-2(%rbx), %rdx
	cmpb	$17, %cl
	je	.L689
	cmpb	$15, %cl
	je	.L618
	leal	-11(%rcx), %eax
	movl	%r12d, %edi
	cmpb	$3, %al
	setbe	%sil
	cmpb	$16, %cl
	sete	%al
	sarl	%cl, %edi
	movl	%edi, %ecx
	andl	$1, %ecx
	orb	%sil, %al
	je	.L690
	testl	%ecx, %ecx
	jne	.L424
	leaq	-6(%rbx), %rdx
	.p2align 4,,10
	.p2align 3
.L501:
	cmpq	%rdx, %r13
	jnb	.L613
	movq	%rdx, %rbx
.L493:
	movzbl	-1(%rbx), %ecx
	leaq	-1(%rbx), %rdx
	movl	%ecx, %eax
	andl	$3, %eax
	je	.L501
	cmpb	$1, %al
	jne	.L691
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L692:
	cmpb	$2, %al
	je	.L618
	shrb	$2, %cl
	leaq	-2(%rbx), %rdi
	cmpb	$17, %cl
	je	.L447
	cmpb	$15, %cl
	je	.L618
	leal	-11(%rcx), %eax
	movl	%r12d, %edx
	cmpb	$3, %al
	setbe	%sil
	cmpb	$16, %cl
	sete	%al
	sarl	%cl, %edx
	andl	$1, %edx
	orb	%sil, %al
	je	.L448
	testl	%edx, %edx
	jne	.L424
	subq	$6, %rbx
	.p2align 4,,10
	.p2align 3
.L446:
	cmpq	%rbx, %r13
	jnb	.L613
.L438:
	movzbl	-1(%rbx), %ecx
	leaq	-1(%rbx), %rdx
	movl	%ecx, %eax
	andl	$3, %eax
	je	.L618
	cmpb	$1, %al
	jne	.L692
	movq	%rdx, %rbx
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L607:
	movq	%rdx, %rbx
.L528:
	cmpq	%rbx, %r13
	jnb	.L613
.L520:
	movzbl	-1(%rbx), %ecx
	leaq	-1(%rbx), %rdx
	movl	%ecx, %eax
	andl	$3, %eax
	je	.L607
	subl	$1, %eax
	cmpb	$1, %al
	jbe	.L618
	shrb	$2, %cl
	leaq	-2(%rbx), %rdi
	cmpb	$17, %cl
	je	.L529
	cmpb	$15, %cl
	je	.L618
	leal	-11(%rcx), %eax
	movl	%r12d, %edx
	cmpb	$3, %al
	setbe	%sil
	cmpb	$16, %cl
	sete	%al
	sarl	%cl, %edx
	andl	$1, %edx
	orb	%sil, %al
	je	.L530
	testl	%edx, %edx
	jne	.L424
	subq	$6, %rbx
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L695:
	cmpb	$2, %al
	je	.L618
	shrb	$2, %cl
	leaq	-2(%rbx), %rdx
	cmpb	$17, %cl
	je	.L693
	cmpb	$15, %cl
	je	.L618
	leal	-11(%rcx), %eax
	movl	%r12d, %edi
	cmpb	$3, %al
	setbe	%sil
	cmpb	$16, %cl
	sete	%al
	sarl	%cl, %edi
	movl	%edi, %ecx
	andl	$1, %ecx
	orb	%sil, %al
	je	.L694
	testl	%ecx, %ecx
	jne	.L424
	leaq	-6(%rbx), %rdx
	.p2align 4,,10
	.p2align 3
.L515:
	cmpq	%rdx, %r13
	jnb	.L613
	movq	%rdx, %rbx
.L507:
	movzbl	-1(%rbx), %ecx
	leaq	-1(%rbx), %rdx
	movl	%ecx, %eax
	andl	$3, %eax
	je	.L515
	cmpb	$1, %al
	jne	.L695
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L696:
	cmpb	$2, %al
	je	.L578
	shrb	$2, %cl
	leaq	-2(%rbx), %rdi
	cmpb	$17, %cl
	je	.L474
	cmpb	$15, %cl
	je	.L618
	leal	-11(%rcx), %eax
	movl	%r12d, %edx
	cmpb	$3, %al
	setbe	%sil
	cmpb	$16, %cl
	sete	%al
	sarl	%cl, %edx
	andl	$1, %edx
	orb	%sil, %al
	je	.L475
	testl	%edx, %edx
	jne	.L424
	subq	$6, %rbx
.L473:
	cmpq	%rbx, %r13
	jnb	.L613
.L465:
	movzbl	-1(%rbx), %ecx
	leaq	-1(%rbx), %rdx
	movl	%ecx, %eax
	andl	$3, %eax
	je	.L618
	cmpb	$1, %al
	jne	.L696
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L602:
	movq	%rdx, %rbx
.L521:
	cmpq	%rbx, %r13
	jb	.L527
	xorl	%eax, %eax
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L547:
	movq	%rdx, %rbx
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L560:
	movq	%rdx, %rbx
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L578:
	movq	%rdx, %rbx
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L572:
	movq	%rdx, %rbx
	jmp	.L466
.L680:
	testb	$1, -2(%rbx)
	jne	.L573
	testb	$1, -3(%rbx)
	jne	.L665
	testb	$1, -4(%rbx)
	movq	%rbx, %rax
	setne	%bl
	movzbl	%bl, %ebx
	leaq	-5(%rbx,%rax), %rbx
	jmp	.L466
.L474:
	testb	$1, -2(%rbx)
	jne	.L581
	testb	$1, -3(%rbx)
	jne	.L697
	testb	$1, -4(%rbx)
	movq	%rbx, %rax
	setne	%bl
	movzbl	%bl, %ebx
	leaq	-5(%rbx,%rax), %rbx
	jmp	.L473
.L431:
	testb	$1, -2(%rbx)
	jne	.L544
	testb	$1, -3(%rbx)
	jne	.L662
	testb	$1, -4(%rbx)
	movq	%rbx, %rax
	setne	%bl
	movzbl	%bl, %ebx
	leaq	-5(%rbx,%rax), %rbx
	jmp	.L434
.L490:
	testl	%ecx, %ecx
	jne	.L618
	jmp	.L488
.L683:
	testl	%edx, %edx
	jne	.L618
.L616:
	movq	%rdi, %rbx
	jmp	.L534
.L682:
	testb	$1, -2(%rbx)
	jne	.L616
	testb	$1, -3(%rbx)
	jne	.L698
	testb	$1, -4(%rbx)
	movq	%rbx, %rax
	setne	%bl
	movzbl	%bl, %ebx
	leaq	-5(%rbx,%rax), %rbx
	jmp	.L534
.L489:
	testb	$1, -2(%rbx)
	jne	.L488
	leaq	-3(%rbx), %rdx
	testb	$1, -3(%rbx)
	jne	.L488
	xorl	%edx, %edx
	testb	$1, -4(%rbx)
	setne	%dl
	leaq	-5(%rdx,%rbx), %rdx
	jmp	.L488
.L686:
	testl	%edx, %edx
	jne	.L618
.L566:
	movq	%rdi, %rbx
	jmp	.L460
.L685:
	testb	$1, -2(%rbx)
	jne	.L566
	testb	$1, -3(%rbx)
	jne	.L462
	testb	$1, -4(%rbx)
	movq	%rbx, %rax
	setne	%bl
	movzbl	%bl, %ebx
	leaq	-5(%rbx,%rax), %rbx
	jmp	.L460
.L681:
	testl	%edx, %edx
	jne	.L618
.L573:
	movq	%rdi, %rbx
	jmp	.L466
.L676:
	testb	$1, -2(%rbx)
	jne	.L549
	testb	$1, -3(%rbx)
	jne	.L663
	testb	$1, -4(%rbx)
	movq	%rbx, %rax
	setne	%bl
	movzbl	%bl, %ebx
	leaq	-5(%rbx,%rax), %rbx
	jmp	.L439
.L690:
	testl	%ecx, %ecx
	jne	.L618
	jmp	.L501
.L689:
	testb	$1, -2(%rbx)
	jne	.L501
	testb	$1, -3(%rbx)
	jne	.L503
	xorl	%edx, %edx
	testb	$1, -4(%rbx)
	setne	%dl
	leaq	-5(%rdx,%rbx), %rdx
	jmp	.L501
.L433:
	testl	%edx, %edx
	jne	.L618
.L544:
	movq	%rdi, %rbx
	jmp	.L434
.L456:
	testl	%edx, %edx
	jne	.L618
.L562:
	movq	%rdi, %rbx
	jmp	.L453
.L454:
	testb	$1, -2(%rbx)
	jne	.L562
	testb	$1, -3(%rbx)
	jne	.L664
	testb	$1, -4(%rbx)
	movq	%rbx, %rax
	setne	%bl
	movzbl	%bl, %ebx
	leaq	-5(%rbx,%rax), %rbx
	jmp	.L453
.L530:
	testl	%edx, %edx
	jne	.L618
.L611:
	movq	%rdi, %rbx
	jmp	.L528
.L529:
	testb	$1, -2(%rbx)
	jne	.L611
	testb	$1, -3(%rbx)
	jne	.L699
	testb	$1, -4(%rbx)
	movq	%rbx, %rax
	setne	%bl
	movzbl	%bl, %ebx
	leaq	-5(%rbx,%rax), %rbx
	jmp	.L528
.L672:
	testb	$1, -2(%rbx)
	jne	.L481
	testb	$1, -3(%rbx)
	jne	.L666
	xorl	%edx, %edx
	testb	$1, -4(%rbx)
	setne	%dl
	leaq	-5(%rdx,%rbx), %rdx
	jmp	.L481
.L677:
	testl	%edx, %edx
	jne	.L618
.L549:
	movq	%rdi, %rbx
	jmp	.L439
.L679:
	testl	%edx, %edx
	jne	.L618
.L604:
	movq	%rdi, %rbx
	jmp	.L521
.L678:
	testb	$1, -2(%rbx)
	jne	.L604
	testb	$1, -3(%rbx)
	jne	.L669
	testb	$1, -4(%rbx)
	movq	%rbx, %rax
	setne	%bl
	movzbl	%bl, %ebx
	leaq	-5(%rbx,%rax), %rbx
	jmp	.L521
.L694:
	testl	%ecx, %ecx
	jne	.L618
	jmp	.L515
.L693:
	testb	$1, -2(%rbx)
	jne	.L515
	testb	$1, -3(%rbx)
	jne	.L517
	xorl	%edx, %edx
	testb	$1, -4(%rbx)
	setne	%dl
	leaq	-5(%rdx,%rbx), %rdx
	jmp	.L515
.L448:
	testl	%edx, %edx
	jne	.L618
.L557:
	movq	%rdi, %rbx
	jmp	.L446
.L509:
	testb	$1, -2(%rbx)
	jne	.L508
	testb	$1, -3(%rbx)
	jne	.L668
	xorl	%edx, %edx
	testb	$1, -4(%rbx)
	setne	%dl
	leaq	-5(%rdx,%rbx), %rdx
	jmp	.L508
.L511:
	testl	%ecx, %ecx
	jne	.L618
	jmp	.L508
.L475:
	testl	%edx, %edx
	jne	.L618
.L581:
	movq	%rdi, %rbx
	jmp	.L473
.L497:
	testl	%ecx, %ecx
	jne	.L618
	jmp	.L494
.L495:
	testb	$1, -2(%rbx)
	jne	.L494
	testb	$1, -3(%rbx)
	jne	.L667
	xorl	%edx, %edx
	testb	$1, -4(%rbx)
	setne	%dl
	leaq	-5(%rdx,%rbx), %rdx
	jmp	.L494
.L447:
	testb	$1, -2(%rbx)
	jne	.L557
	testb	$1, -3(%rbx)
	jne	.L700
	testb	$1, -4(%rbx)
	movq	%rbx, %rax
	setne	%bl
	movzbl	%bl, %ebx
	leaq	-5(%rbx,%rax), %rbx
	jmp	.L446
.L667:
	leaq	-3(%rbx), %rdx
	jmp	.L494
.L664:
	subq	$3, %rbx
	jmp	.L453
.L665:
	subq	$3, %rbx
	jmp	.L466
.L663:
	subq	$3, %rbx
	jmp	.L439
.L669:
	subq	$3, %rbx
	jmp	.L521
.L668:
	leaq	-3(%rbx), %rdx
	jmp	.L508
.L662:
	subq	$3, %rbx
	jmp	.L434
.L666:
	leaq	-3(%rbx), %rdx
	jmp	.L481
.L674:
	call	__stack_chk_fail@PLT
.L699:
	subq	$3, %rbx
	jmp	.L528
.L517:
	leaq	-3(%rbx), %rdx
	jmp	.L515
.L700:
	subq	$3, %rbx
	jmp	.L446
.L462:
	subq	$3, %rbx
	jmp	.L460
.L698:
	subq	$3, %rbx
	jmp	.L534
.L697:
	subq	$3, %rbx
	jmp	.L473
.L503:
	leaq	-3(%rbx), %rdx
	jmp	.L501
	.cfi_endproc
.LFE19804:
	.size	_ZN2v88internal9RelocInfo18RequiresRelocationENS0_4CodeE, .-_ZN2v88internal9RelocInfo18RequiresRelocationENS0_4CodeE
	.section	.rodata._ZN2v88internal9RelocInfo13RelocModeNameENS1_4ModeE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"unknown relocation type"
.LC3:
	.string	"no reloc"
.LC4:
	.string	"full embedded object"
.LC5:
	.string	"code target"
.LC6:
	.string	"relative code target"
.LC7:
	.string	"runtime entry"
.LC8:
	.string	"external reference"
.LC9:
	.string	"internal reference"
.LC10:
	.string	"encoded internal reference"
.LC11:
	.string	"off heap target"
.LC12:
	.string	"deopt script offset"
.LC13:
	.string	"deopt inlining id"
.LC14:
	.string	"deopt reason"
.LC15:
	.string	"deopt index"
.LC16:
	.string	"constant pool"
.LC17:
	.string	"veneer pool"
.LC18:
	.string	"internal wasm call"
.LC19:
	.string	"wasm stub call"
.LC20:
	.string	"compressed embedded object"
.LC21:
	.string	"unreachable code"
	.section	.text._ZN2v88internal9RelocInfo13RelocModeNameENS1_4ModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9RelocInfo13RelocModeNameENS1_4ModeE
	.type	_ZN2v88internal9RelocInfo13RelocModeNameENS1_4ModeE, @function
_ZN2v88internal9RelocInfo13RelocModeNameENS1_4ModeE:
.LFB19805:
	.cfi_startproc
	endbr64
	cmpb	$19, %dil
	ja	.L702
	leaq	.L704(%rip), %rcx
	movzbl	%dil, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal9RelocInfo13RelocModeNameENS1_4ModeE,"a",@progbits
	.align 4
	.align 4
.L704:
	.long	.L722-.L704
	.long	.L721-.L704
	.long	.L720-.L704
	.long	.L719-.L704
	.long	.L718-.L704
	.long	.L717-.L704
	.long	.L716-.L704
	.long	.L715-.L704
	.long	.L714-.L704
	.long	.L713-.L704
	.long	.L712-.L704
	.long	.L711-.L704
	.long	.L710-.L704
	.long	.L709-.L704
	.long	.L708-.L704
	.long	.L707-.L704
	.long	.L706-.L704
	.long	.L705-.L704
	.long	.L705-.L704
	.long	.L723-.L704
	.section	.text._ZN2v88internal9RelocInfo13RelocModeNameENS1_4ModeE
	.p2align 4,,10
	.p2align 3
.L722:
	leaq	.LC5(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L721:
	leaq	.LC6(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L719:
	leaq	.LC4(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L718:
	leaq	.LC18(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L717:
	leaq	.LC19(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L716:
	leaq	.LC7(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L715:
	leaq	.LC8(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L714:
	leaq	.LC9(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L713:
	leaq	.LC10(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L712:
	leaq	.LC11(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L711:
	leaq	.LC16(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L710:
	leaq	.LC17(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L709:
	leaq	.LC12(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L708:
	leaq	.LC13(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L707:
	leaq	.LC14(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L706:
	leaq	.LC15(%rip), %rax
	ret
.L702:
	leaq	.LC2(%rip), %rax
	ret
.L705:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC21(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L720:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	leaq	.LC20(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L723:
	leaq	.LC3(%rip), %rax
	ret
	.cfi_endproc
.LFE19805:
	.size	_ZN2v88internal9RelocInfo13RelocModeNameENS1_4ModeE, .-_ZN2v88internal9RelocInfo13RelocModeNameENS1_4ModeE
	.section	.rodata._ZN2v88internal9RelocInfo5PrintEPNS0_7IsolateERSo.str1.1,"aMS",@progbits,1
.LC22:
	.string	"  "
.LC23:
	.string	"  ("
.LC24:
	.string	")"
.LC25:
	.string	" compressed)"
.LC26:
	.string	" ("
.LC27:
	.string	") "
.LC28:
	.string	" "
.LC29:
	.string	")  ("
.LC30:
	.string	" deoptimization bailout)"
.LC31:
	.string	" (size "
.LC32:
	.string	"\n"
	.section	.text._ZN2v88internal9RelocInfo5PrintEPNS0_7IsolateERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9RelocInfo5PrintEPNS0_7IsolateERSo
	.type	_ZN2v88internal9RelocInfo5PrintEPNS0_7IsolateERSo, @function
_ZN2v88internal9RelocInfo5PrintEPNS0_7IsolateERSo:
.LFB19806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rsi
	movq	%rdx, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	movl	$2, %edx
	leaq	.LC22(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movsbl	8(%rbx), %edi
	call	_ZN2v88internal9RelocInfo13RelocModeNameENS1_4ModeE
	testq	%rax, %rax
	je	.L767
	movq	%rax, %rdi
	movq	%rax, %r15
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	8(%rbx), %eax
	leal	-13(%rax), %edx
	cmpb	$1, %dl
	jbe	.L768
.L729:
	cmpb	$15, %al
	je	.L769
	cmpb	$3, %al
	je	.L770
	cmpb	$2, %al
	je	.L771
	cmpb	$7, %al
	je	.L772
	cmpb	$1, %al
	jle	.L773
	cmpb	$6, %al
	je	.L774
	cmpb	$11, %al
	jne	.L730
	movl	$7, %edx
	movq	%r12, %rdi
	leaq	.LC31(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	16(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC24(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L767:
	movq	0(%r13), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	movzbl	8(%rbx), %eax
	leal	-13(%rax), %edx
	cmpb	$1, %dl
	ja	.L729
.L768:
	movl	$3, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	16(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC24(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L730:
	movl	$1, %edx
	leaq	.LC32(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L775
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L770:
	.cfi_restore_state
	movl	$3, %edx
	movq	%r12, %rdi
	leaq	.LC23(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movl	$1, %edx
	leaq	.LC24(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L773:
	movq	(%rbx), %rax
	movslq	(%rax), %r13
	addq	%rax, %r13
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	leaq	4(%r13), %r15
	movq	%rax, %r14
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %eax
	addq	%r14, %rax
	cmpq	%rax, %r15
	jnb	.L741
	cmpq	%r15, %r14
	ja	.L741
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L741:
	movl	$2, %edx
	leaq	.LC26(%rip), %rsi
	leaq	-59(%r13), %r14
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-16(%r13), %edi
	shrl	%edi
	andl	$31, %edi
	call	_ZN2v88internal4Code11Kind2StringENS1_4KindE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L776
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L743:
	movq	%r14, %rdi
	call	_ZN2v88internal8Builtins9IsBuiltinENS0_4CodeE@PLT
	testb	%al, %al
	jne	.L777
.L744:
	movl	$4, %edx
	leaq	.LC29(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movslq	(%rax), %rdx
	leaq	4(%rax,%rdx), %rsi
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L769:
	movl	$3, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	16(%rbx), %rdi
	call	_ZN2v88internal24DeoptimizeReasonToStringENS0_16DeoptimizeReasonE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L778
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L733:
	movl	$1, %edx
	leaq	.LC24(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L772:
	testq	%r14, %r14
	je	.L737
	leaq	-64(%rbp), %r15
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal24ExternalReferenceEncoderC1EPNS0_7IsolateE@PLT
	movl	$2, %edx
	leaq	.LC26(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	(%rax), %rdx
	call	_ZNK2v88internal24ExternalReferenceEncoder13NameOfAddressEPNS0_7IsolateEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L779
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L739:
	movq	%r12, %rdi
	movl	$2, %edx
	leaq	.LC27(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal24ExternalReferenceEncoderD1Ev@PLT
.L737:
	leaq	.LC26(%rip), %rsi
	movl	$2, %edx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	(%rax), %rsi
.L766:
	movq	%r12, %rdi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC24(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L771:
	movl	$3, %edx
	movq	%r12, %rdi
	leaq	.LC23(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movl	$12, %edx
	leaq	.LC25(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L774:
	cmpq	$0, 41040(%r14)
	je	.L730
	movq	(%rbx), %rax
	leaq	-64(%rbp), %rdx
	movq	%r14, %rdi
	movslq	(%rax), %rcx
	leaq	4(%rax,%rcx), %rsi
	call	_ZN2v88internal11Deoptimizer21IsDeoptimizationEntryEPNS0_7IsolateEmPNS0_14DeoptimizeKindE@PLT
	testb	%al, %al
	je	.L730
	movl	$3, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	-64(%rbp), %edi
	call	_ZN2v88internal11Deoptimizer10MessageForENS0_14DeoptimizeKindE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L780
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L751:
	movl	$24, %edx
	leaq	.LC30(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L779:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L739
	.p2align 4,,10
	.p2align 3
.L776:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L778:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L777:
	movl	$1, %edx
	leaq	.LC28(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	0(%r13), %edi
	call	_ZN2v88internal8Builtins4nameEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L781
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L744
.L781:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L744
.L780:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L751
.L775:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19806:
	.size	_ZN2v88internal9RelocInfo5PrintEPNS0_7IsolateERSo, .-_ZN2v88internal9RelocInfo5PrintEPNS0_7IsolateERSo
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal9RelocInfo20kFillerCommentStringE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal9RelocInfo20kFillerCommentStringE, @function
_GLOBAL__sub_I__ZN2v88internal9RelocInfo20kFillerCommentStringE:
.LFB24165:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24165:
	.size	_GLOBAL__sub_I__ZN2v88internal9RelocInfo20kFillerCommentStringE, .-_GLOBAL__sub_I__ZN2v88internal9RelocInfo20kFillerCommentStringE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal9RelocInfo20kFillerCommentStringE
	.globl	_ZN2v88internal9RelocInfo16kMaxSmallPCDeltaE
	.section	.rodata._ZN2v88internal9RelocInfo16kMaxSmallPCDeltaE,"a"
	.align 4
	.type	_ZN2v88internal9RelocInfo16kMaxSmallPCDeltaE, @object
	.size	_ZN2v88internal9RelocInfo16kMaxSmallPCDeltaE, 4
_ZN2v88internal9RelocInfo16kMaxSmallPCDeltaE:
	.long	63
	.globl	_ZN2v88internal9RelocInfo20kFillerCommentStringE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC33:
	.string	"DEOPTIMIZATION PADDING"
	.section	.data.rel.ro.local._ZN2v88internal9RelocInfo20kFillerCommentStringE,"aw"
	.align 8
	.type	_ZN2v88internal9RelocInfo20kFillerCommentStringE, @object
	.size	_ZN2v88internal9RelocInfo20kFillerCommentStringE, 8
_ZN2v88internal9RelocInfo20kFillerCommentStringE:
	.quad	.LC33
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
