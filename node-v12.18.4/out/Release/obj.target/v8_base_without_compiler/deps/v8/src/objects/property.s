	.file	"property.cc"
	.text
	.section	.rodata._ZN2v88internallsERSoRKNS0_18PropertyAttributesE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"C"
.LC1:
	.string	"_"
.LC2:
	.string	"E"
.LC3:
	.string	"W"
.LC4:
	.string	"["
.LC5:
	.string	"]"
	.section	.text._ZN2v88internallsERSoRKNS0_18PropertyAttributesE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoRKNS0_18PropertyAttributesE
	.type	_ZN2v88internallsERSoRKNS0_18PropertyAttributesE, @function
_ZN2v88internallsERSoRKNS0_18PropertyAttributesE:
.LFB17811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	.LC1(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	leaq	.LC4(%rip), %rsi
	subq	$8, %rsp
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testb	$1, (%rbx)
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	cmovne	%r13, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testb	$2, (%rbx)
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	cmovne	%r13, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testb	$4, (%rbx)
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC0(%rip), %rsi
	cmovne	%r13, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17811:
	.size	_ZN2v88internallsERSoRKNS0_18PropertyAttributesE, .-_ZN2v88internallsERSoRKNS0_18PropertyAttributesE
	.section	.rodata._ZN2v88internallsERSoNS0_17PropertyConstnessE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"mutable"
.LC7:
	.string	"const"
.LC8:
	.string	"unreachable code"
	.section	.text._ZN2v88internallsERSoNS0_17PropertyConstnessE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoNS0_17PropertyConstnessE
	.type	_ZN2v88internallsERSoNS0_17PropertyConstnessE, @function
_ZN2v88internallsERSoNS0_17PropertyConstnessE:
.LFB17812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	testl	%esi, %esi
	je	.L12
	cmpl	$1, %esi
	jne	.L18
	movl	$5, %edx
	leaq	.LC7(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movl	$7, %edx
	leaq	.LC6(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L18:
	.cfi_restore_state
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17812:
	.size	_ZN2v88internallsERSoNS0_17PropertyConstnessE, .-_ZN2v88internallsERSoNS0_17PropertyConstnessE
	.section	.text._ZN2v88internal10DescriptorC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10DescriptorC2Ev
	.type	_ZN2v88internal10DescriptorC2Ev, @function
_ZN2v88internal10DescriptorC2Ev:
.LFB17814:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	movl	$1, 8(%rdi)
	movq	$0, 16(%rdi)
	movl	$0, 24(%rdi)
	ret
	.cfi_endproc
.LFE17814:
	.size	_ZN2v88internal10DescriptorC2Ev, .-_ZN2v88internal10DescriptorC2Ev
	.globl	_ZN2v88internal10DescriptorC1Ev
	.set	_ZN2v88internal10DescriptorC1Ev,_ZN2v88internal10DescriptorC2Ev
	.section	.text._ZN2v88internal10DescriptorC2ENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleENS0_12PropertyKindENS0_18PropertyAttributesENS0_16PropertyLocationENS0_17PropertyConstnessENS0_14RepresentationEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10DescriptorC2ENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleENS0_12PropertyKindENS0_18PropertyAttributesENS0_16PropertyLocationENS0_17PropertyConstnessENS0_14RepresentationEi
	.type	_ZN2v88internal10DescriptorC2ENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleENS0_12PropertyKindENS0_18PropertyAttributesENS0_16PropertyLocationENS0_17PropertyConstnessENS0_14RepresentationEi, @function
_ZN2v88internal10DescriptorC2ENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleENS0_12PropertyKindENS0_18PropertyAttributesENS0_16PropertyLocationENS0_17PropertyConstnessENS0_14RepresentationEi:
.LFB17817:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movdqu	(%rdx), %xmm0
	addl	%r9d, %r9d
	sall	$3, %r8d
	movq	%rsi, (%rdi)
	movups	%xmm0, 8(%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movsbl	24(%rbp), %eax
	movl	32(%rbp), %edx
	movl	16(%rbp), %esi
	popq	%rbp
	.cfi_def_cfa 7, 8
	sall	$6, %eax
	sall	$19, %edx
	andl	$16320, %eax
	orl	%edx, %eax
	orl	%r9d, %eax
	leal	0(,%rsi,4), %r9d
	orl	%r9d, %eax
	orl	%ecx, %eax
	orl	%r8d, %eax
	movl	%eax, 24(%rdi)
	ret
	.cfi_endproc
.LFE17817:
	.size	_ZN2v88internal10DescriptorC2ENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleENS0_12PropertyKindENS0_18PropertyAttributesENS0_16PropertyLocationENS0_17PropertyConstnessENS0_14RepresentationEi, .-_ZN2v88internal10DescriptorC2ENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleENS0_12PropertyKindENS0_18PropertyAttributesENS0_16PropertyLocationENS0_17PropertyConstnessENS0_14RepresentationEi
	.globl	_ZN2v88internal10DescriptorC1ENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleENS0_12PropertyKindENS0_18PropertyAttributesENS0_16PropertyLocationENS0_17PropertyConstnessENS0_14RepresentationEi
	.set	_ZN2v88internal10DescriptorC1ENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleENS0_12PropertyKindENS0_18PropertyAttributesENS0_16PropertyLocationENS0_17PropertyConstnessENS0_14RepresentationEi,_ZN2v88internal10DescriptorC2ENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleENS0_12PropertyKindENS0_18PropertyAttributesENS0_16PropertyLocationENS0_17PropertyConstnessENS0_14RepresentationEi
	.section	.text._ZN2v88internal10DescriptorC2ENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleENS0_15PropertyDetailsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10DescriptorC2ENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleENS0_15PropertyDetailsE
	.type	_ZN2v88internal10DescriptorC2ENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleENS0_15PropertyDetailsE, @function
_ZN2v88internal10DescriptorC2ENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleENS0_15PropertyDetailsE:
.LFB17820:
	.cfi_startproc
	endbr64
	movdqu	(%rdx), %xmm0
	movq	%rsi, (%rdi)
	movl	%ecx, 24(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE17820:
	.size	_ZN2v88internal10DescriptorC2ENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleENS0_15PropertyDetailsE, .-_ZN2v88internal10DescriptorC2ENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleENS0_15PropertyDetailsE
	.globl	_ZN2v88internal10DescriptorC1ENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleENS0_15PropertyDetailsE
	.set	_ZN2v88internal10DescriptorC1ENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleENS0_15PropertyDetailsE,_ZN2v88internal10DescriptorC2ENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleENS0_15PropertyDetailsE
	.section	.text._ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE
	.type	_ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE, @function
_ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE:
.LFB17822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	sall	$3, %r14d
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	sall	$19, %r13d
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	sall	$6, %ebx
	andl	$16320, %ebx
	subq	$8, %rsp
	orl	%r14d, %ebx
	call	_ZN2v88internal9FieldType3AnyEPNS0_7IsolateE@PLT
	orl	%r13d, %ebx
	movq	%r15, (%r12)
	movq	%rax, 16(%r12)
	movq	%r12, %rax
	movl	%ebx, 24(%r12)
	movl	$1, 8(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17822:
	.size	_ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE, .-_ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE
	.section	.text._ZN2v88internal10Descriptor9DataFieldENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationERKNS0_17MaybeObjectHandleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Descriptor9DataFieldENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationERKNS0_17MaybeObjectHandleE
	.type	_ZN2v88internal10Descriptor9DataFieldENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationERKNS0_17MaybeObjectHandleE, @function
_ZN2v88internal10Descriptor9DataFieldENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationERKNS0_17MaybeObjectHandleE:
.LFB17823:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	sall	$6, %r9d
	sall	$19, %edx
	movq	%rdi, %rax
	andl	$16320, %r9d
	sall	$3, %ecx
	movq	%rsi, (%rdi)
	orl	%edx, %r9d
	sall	$2, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	orl	%ecx, %r9d
	movq	16(%rbp), %rdx
	orl	%r8d, %r9d
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%r9d, 24(%rdi)
	movdqu	(%rdx), %xmm0
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE17823:
	.size	_ZN2v88internal10Descriptor9DataFieldENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationERKNS0_17MaybeObjectHandleE, .-_ZN2v88internal10Descriptor9DataFieldENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationERKNS0_17MaybeObjectHandleE
	.section	.text._ZN2v88internal10Descriptor12DataConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Descriptor12DataConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE
	.type	_ZN2v88internal10Descriptor12DataConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE, @function
_ZN2v88internal10Descriptor12DataConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE:
.LFB17824:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal17FLAG_track_fieldsE(%rip)
	movq	%rdi, %rax
	je	.L28
	movq	(%rdx), %r8
	movl	$64, %edi
	testb	$1, %r8b
	jne	.L38
.L29:
	sall	$3, %ecx
	movq	%rsi, (%rax)
	orl	%edi, %ecx
	movl	$1, 8(%rax)
	orl	$6, %ecx
	movq	%rdx, 16(%rax)
	movl	%ecx, 24(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	cmpb	$0, _ZN2v88internal24FLAG_track_double_fieldsE(%rip)
	jne	.L30
.L33:
	cmpb	$0, _ZN2v88internal26FLAG_track_computed_fieldsE(%rip)
	jne	.L39
.L32:
	cmpb	$0, _ZN2v88internal29FLAG_track_heap_object_fieldsE(%rip)
	movl	$192, %edi
	jne	.L29
	.p2align 4,,10
	.p2align 3
.L28:
	movl	$256, %edi
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L30:
	movq	-1(%r8), %r9
	movl	$128, %edi
	cmpw	$65, 11(%r9)
	jne	.L33
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%r8, %rdi
	andq	$-262144, %rdi
	movq	24(%rdi), %r9
	xorl	%edi, %edi
	cmpq	-37512(%r9), %r8
	jne	.L32
	jmp	.L29
	.cfi_endproc
.LFE17824:
	.size	_ZN2v88internal10Descriptor12DataConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE, .-_ZN2v88internal10Descriptor12DataConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE
	.section	.text._ZN2v88internal10Descriptor12DataConstantEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS4_INS0_6ObjectEEENS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Descriptor12DataConstantEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS4_INS0_6ObjectEEENS0_18PropertyAttributesE
	.type	_ZN2v88internal10Descriptor12DataConstantEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS4_INS0_6ObjectEEENS0_18PropertyAttributesE, @function
_ZN2v88internal10Descriptor12DataConstantEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS4_INS0_6ObjectEEENS0_18PropertyAttributesE:
.LFB17825:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$24, %rsp
	call	_ZN2v88internal9FieldType3AnyEv@PLT
	movq	41112(%r14), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L41
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L42:
	sall	$19, %r13d
	sall	$3, %ebx
	movq	%r15, (%r12)
	orl	%r13d, %ebx
	movq	%rax, 16(%r12)
	movq	%r12, %rax
	movl	$1, 8(%r12)
	orl	$260, %ebx
	movl	%ebx, 24(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L45
.L43:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L45:
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L43
	.cfi_endproc
.LFE17825:
	.size	_ZN2v88internal10Descriptor12DataConstantEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS4_INS0_6ObjectEEENS0_18PropertyAttributesE, .-_ZN2v88internal10Descriptor12DataConstantEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS4_INS0_6ObjectEEENS0_18PropertyAttributesE
	.section	.text._ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE
	.type	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE, @function
_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE:
.LFB17826:
	.cfi_startproc
	endbr64
	sall	$3, %ecx
	movq	%rsi, (%rdi)
	movq	%rdi, %rax
	orl	$263, %ecx
	movl	$1, 8(%rdi)
	movq	%rdx, 16(%rdi)
	movl	%ecx, 24(%rdi)
	ret
	.cfi_endproc
.LFE17826:
	.size	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE, .-_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE
	.section	.rodata._ZN2v88internal15PropertyDetails13PrintAsSlowToERSo.str1.1,"aMS",@progbits,1
.LC9:
	.string	"data"
.LC10:
	.string	"accessor"
.LC11:
	.string	"("
.LC12:
	.string	"const "
.LC13:
	.string	", dict_index: "
.LC14:
	.string	", attrs: "
.LC15:
	.string	")"
	.section	.text._ZN2v88internal15PropertyDetails13PrintAsSlowToERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15PropertyDetails13PrintAsSlowToERSo
	.type	_ZN2v88internal15PropertyDetails13PrintAsSlowToERSo, @function
_ZN2v88internal15PropertyDetails13PrintAsSlowToERSo:
.LFB17827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	.LC11(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%rbx), %eax
	testb	$4, %al
	jne	.L56
.L48:
	andl	$1, %eax
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	cmpl	$1, %eax
	sbbq	%rdx, %rdx
	andq	$-4, %rdx
	addq	$8, %rdx
	testl	%eax, %eax
	leaq	.LC10(%rip), %rax
	cmovne	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$14, %edx
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%rbx), %esi
	movq	%r12, %rdi
	shrl	$8, %esi
	andl	$8388607, %esi
	call	_ZNSolsEi@PLT
	movl	$9, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%rbx), %eax
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	shrl	$3, %eax
	andl	$7, %eax
	movl	%eax, -28(%rbp)
	call	_ZN2v88internallsERSoRKNS0_18PropertyAttributesE
	movl	$1, %edx
	leaq	.LC15(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L57
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	movl	$6, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%rbx), %eax
	jmp	.L48
.L57:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17827:
	.size	_ZN2v88internal15PropertyDetails13PrintAsSlowToERSo, .-_ZN2v88internal15PropertyDetails13PrintAsSlowToERSo
	.section	.rodata._ZN2v88internal15PropertyDetails13PrintAsFastToERSoNS1_9PrintModeE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"v"
.LC17:
	.string	"d"
.LC18:
	.string	"h"
.LC19:
	.string	"t"
.LC20:
	.string	"s"
.LC21:
	.string	" field"
.LC22:
	.string	" "
.LC23:
	.string	":"
.LC24:
	.string	" descriptor"
.LC25:
	.string	", p: "
	.section	.text._ZN2v88internal15PropertyDetails13PrintAsFastToERSoNS1_9PrintModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15PropertyDetails13PrintAsFastToERSoNS1_9PrintModeE
	.type	_ZN2v88internal15PropertyDetails13PrintAsFastToERSoNS1_9PrintModeE, @function
_ZN2v88internal15PropertyDetails13PrintAsFastToERSoNS1_9PrintModeE:
.LFB17828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	movl	$1, %edx
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	leaq	.LC11(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%rbx), %eax
	testb	$4, %al
	jne	.L95
.L59:
	andl	$1, %eax
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	cmpl	$1, %eax
	sbbq	%rdx, %rdx
	andq	$-4, %rdx
	addq	$8, %rdx
	testl	%eax, %eax
	leaq	.LC10(%rip), %rax
	cmovne	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testb	$2, (%rbx)
	jne	.L92
	movl	$6, %edx
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testb	$2, %r13b
	jne	.L96
	testb	$4, %r13b
	jne	.L97
.L62:
	testb	$8, %r13b
	jne	.L98
.L70:
	andl	$1, %r13d
	jne	.L99
.L71:
	movl	$1, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L100
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movl	$11, %edx
	leaq	.LC24(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testb	$8, %r13b
	je	.L70
.L98:
	movl	$5, %edx
	leaq	.LC25(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%rbx), %esi
	movq	%r12, %rdi
	shrl	$9, %esi
	andl	$1023, %esi
	call	_ZNSolsEi@PLT
	andl	$1, %r13d
	je	.L71
.L99:
	movq	%r12, %rdi
	movl	$9, %edx
	leaq	.LC14(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%rbx), %eax
	leaq	-44(%rbp), %rsi
	movq	%r12, %rdi
	shrl	$3, %eax
	andl	$7, %eax
	movl	%eax, -44(%rbp)
	call	_ZN2v88internallsERSoRKNS0_18PropertyAttributesE
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L95:
	movl	$6, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%rbx), %eax
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L97:
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%rbx), %eax
	shrl	$6, %eax
	andl	$7, %eax
	cmpb	$4, %al
	ja	.L63
	leaq	.L65(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal15PropertyDetails13PrintAsFastToERSoNS1_9PrintModeE,"a",@progbits
	.align 4
	.align 4
.L65:
	.long	.L69-.L65
	.long	.L68-.L65
	.long	.L76-.L65
	.long	.L66-.L65
	.long	.L64-.L65
	.section	.text._ZN2v88internal15PropertyDetails13PrintAsFastToERSoNS1_9PrintModeE
	.p2align 4,,10
	.p2align 3
.L96:
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%rbx), %esi
	movq	%r12, %rdi
	shrl	$19, %esi
	andl	$1023, %esi
	call	_ZNSolsEi@PLT
	testb	$4, %r13b
	je	.L62
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L66:
	leaq	.LC18(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L67:
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L76:
	leaq	.LC17(%rip), %rsi
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L68:
	leaq	.LC20(%rip), %rsi
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L69:
	leaq	.LC16(%rip), %rsi
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L64:
	leaq	.LC19(%rip), %rsi
	jmp	.L67
.L100:
	call	__stack_chk_fail@PLT
.L63:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17828:
	.size	_ZN2v88internal15PropertyDetails13PrintAsFastToERSoNS1_9PrintModeE, .-_ZN2v88internal15PropertyDetails13PrintAsFastToERSoNS1_9PrintModeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internallsERSoRKNS0_18PropertyAttributesE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internallsERSoRKNS0_18PropertyAttributesE, @function
_GLOBAL__sub_I__ZN2v88internallsERSoRKNS0_18PropertyAttributesE:
.LFB21511:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21511:
	.size	_GLOBAL__sub_I__ZN2v88internallsERSoRKNS0_18PropertyAttributesE, .-_GLOBAL__sub_I__ZN2v88internallsERSoRKNS0_18PropertyAttributesE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internallsERSoRKNS0_18PropertyAttributesE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
