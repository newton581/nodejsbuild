	.file	"unicode.cc"
	.text
	.section	.text._ZN7unibrow4Utf814CalculateValueEPKhmPm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN7unibrow4Utf814CalculateValueEPKhmPm
	.type	_ZN7unibrow4Utf814CalculateValueEPKhmPm, @function
_ZN7unibrow4Utf814CalculateValueEPKhmPm:
.LFB5014:
	.cfi_startproc
	endbr64
	movzbl	(%rdi), %ecx
	testb	%cl, %cl
	js	.L27
	movl	$1, %r9d
	movzbl	%cl, %eax
	addq	%r9, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	1(%rdi), %r8
	leaq	(%rdi,%rsi), %r10
	xorl	%eax, %eax
	movl	$12, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE11transitions(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE6states(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	$127, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L29:
	cmpq	%r8, %r10
	jbe	.L28
	movzbl	(%r8), %ecx
	addq	$1, %r8
.L4:
	movl	%esi, %r11d
.L6:
	movzbl	%cl, %r9d
	movzbl	(%r14,%rcx), %ecx
	movzbl	%r11b, %esi
	movl	%r12d, %r15d
	sall	$6, %eax
	leaq	-1(%r8), %rbx
	addl	%ecx, %esi
	sarl	%ecx
	movslq	%esi, %rsi
	sarl	%cl, %r15d
	movzbl	0(%r13,%rsi), %esi
	andl	%r15d, %r9d
	orl	%r9d, %eax
	testb	%sil, %sil
	je	.L8
	cmpb	$12, %sil
	jne	.L29
	cmpq	%r8, %r10
	jbe	.L25
	cmpl	$-4, %eax
	jne	.L25
	movzbl	(%r8), %ecx
	addq	$1, %r8
	testb	%cl, %cl
	js	.L30
	movq	%r8, %r9
	movzbl	%cl, %eax
	subq	%rdi, %r9
.L7:
	addq	%r9, (%rdx)
.L1:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L8:
	subq	%rdi, %r8
	subq	%rdi, %rbx
	cmpb	$12, %r11b
	movl	$65533, %eax
	cmovne	%rbx, %r8
	movq	%r8, %r9
	jmp	.L7
.L28:
	subq	%rdi, %r8
	movl	$65533, %eax
	addq	%r8, (%rdx)
	jmp	.L1
.L25:
	subq	%rdi, %r8
	movq	%r8, %r9
	jmp	.L7
	.cfi_endproc
.LFE5014:
	.size	_ZN7unibrow4Utf814CalculateValueEPKhmPm, .-_ZN7unibrow4Utf814CalculateValueEPKhmPm
	.section	.text._ZN7unibrow4Utf824ValueOfIncrementalFinishEPN14Utf8DfaDecoder5StateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN7unibrow4Utf824ValueOfIncrementalFinishEPN14Utf8DfaDecoder5StateE
	.type	_ZN7unibrow4Utf824ValueOfIncrementalFinishEPN14Utf8DfaDecoder5StateE, @function
_ZN7unibrow4Utf824ValueOfIncrementalFinishEPN14Utf8DfaDecoder5StateE:
.LFB5015:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpb	$12, (%rdi)
	je	.L31
	movb	$12, (%rdi)
	movl	$65533, %eax
.L31:
	ret
	.cfi_endproc
.LFE5015:
	.size	_ZN7unibrow4Utf824ValueOfIncrementalFinishEPN14Utf8DfaDecoder5StateE, .-_ZN7unibrow4Utf824ValueOfIncrementalFinishEPN14Utf8DfaDecoder5StateE
	.section	.text._ZN7unibrow4Utf816ValidateEncodingEPKhm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN7unibrow4Utf816ValidateEncodingEPKhm
	.type	_ZN7unibrow4Utf816ValidateEncodingEPKhm, @function
_ZN7unibrow4Utf816ValidateEncodingEPKhm:
.LFB5016:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L38
	leaq	-1(%rdi,%rsi), %r8
	movl	$12, %edx
	leaq	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE6states(%rip), %rsi
	leaq	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE11transitions(%rip), %rcx
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L37:
	addq	$1, %rdi
	testb	%dl, %dl
	je	.L39
.L36:
	movzbl	(%rdi), %eax
	movzbl	(%rcx,%rax), %eax
	addl	%edx, %eax
	cltq
	movzbl	(%rsi,%rax), %edx
	cmpq	%rdi, %r8
	jne	.L37
	cmpb	$12, %dl
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	xorl	%eax, %eax
	ret
.L38:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE5016:
	.size	_ZN7unibrow4Utf816ValidateEncodingEPKhm, .-_ZN7unibrow4Utf816ValidateEncodingEPKhm
	.section	.text._ZN7unibrow9Uppercase2IsEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN7unibrow9Uppercase2IsEj
	.type	_ZN7unibrow9Uppercase2IsEj, @function
_ZN7unibrow9Uppercase2IsEj:
.LFB5017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	u_isupper_67@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testb	%al, %al
	setne	%al
	ret
	.cfi_endproc
.LFE5017:
	.size	_ZN7unibrow9Uppercase2IsEj, .-_ZN7unibrow9Uppercase2IsEj
	.section	.text._ZN7unibrow6Letter2IsEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN7unibrow6Letter2IsEj
	.type	_ZN7unibrow6Letter2IsEj, @function
_ZN7unibrow6Letter2IsEj:
.LFB5018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	u_isalpha_67@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testb	%al, %al
	setne	%al
	ret
	.cfi_endproc
.LFE5018:
	.size	_ZN7unibrow6Letter2IsEj, .-_ZN7unibrow6Letter2IsEj
	.section	.text.startup._GLOBAL__sub_I__ZN7unibrow4Utf814CalculateValueEPKhmPm,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN7unibrow4Utf814CalculateValueEPKhmPm, @function
_GLOBAL__sub_I__ZN7unibrow4Utf814CalculateValueEPKhmPm:
.LFB5784:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE5784:
	.size	_GLOBAL__sub_I__ZN7unibrow4Utf814CalculateValueEPKhmPm, .-_GLOBAL__sub_I__ZN7unibrow4Utf814CalculateValueEPKhmPm
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN7unibrow4Utf814CalculateValueEPKhmPm
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata._ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE6states,"a"
	.align 32
	.type	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE6states, @object
	.size	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE6states, 108
_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE6states:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\f"
	.string	""
	.string	""
	.string	"\030$0<H"
	.string	"T`"
	.string	"\f\f\f"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\030\030\030"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\030\030"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"$$$"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"$"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\030"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"$$"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata._ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE11transitions,"a"
	.align 32
	.type	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE11transitions, @object
	.size	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE11transitions, 256
_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE11transitions:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\002\002\002\002\002\002\002\002\002\002\002\002\002\002"
	.ascii	"\002\002\003\003\003\003\003\003\003\003\003\003\003\003\003"
	.ascii	"\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"
	.ascii	"\003\003\003\003\t\t\004\004\004\004\004\004\004\004\004\004"
	.ascii	"\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004"
	.ascii	"\004\004\004\004\004\n\005\005\005\005\005\005\005\005\005\005"
	.ascii	"\005\005\006\005\005\013\007\007\007\b\t\t\t\t\t\t\t\t\t\t\t"
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
