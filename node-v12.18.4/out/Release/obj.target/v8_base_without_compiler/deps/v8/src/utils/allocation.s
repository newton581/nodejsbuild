	.file	"allocation.cc"
	.text
	.section	.text._ZN2v88Platform16GetPageAllocatorEv,"axG",@progbits,_ZN2v88Platform16GetPageAllocatorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88Platform16GetPageAllocatorEv
	.type	_ZN2v88Platform16GetPageAllocatorEv, @function
_ZN2v88Platform16GetPageAllocatorEv:
.LFB1942:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1942:
	.size	_ZN2v88Platform16GetPageAllocatorEv, .-_ZN2v88Platform16GetPageAllocatorEv
	.section	.text._ZN2v88Platform24OnCriticalMemoryPressureEv,"axG",@progbits,_ZN2v88Platform24OnCriticalMemoryPressureEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88Platform24OnCriticalMemoryPressureEv
	.type	_ZN2v88Platform24OnCriticalMemoryPressureEv, @function
_ZN2v88Platform24OnCriticalMemoryPressureEv:
.LFB1943:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE1943:
	.size	_ZN2v88Platform24OnCriticalMemoryPressureEv, .-_ZN2v88Platform24OnCriticalMemoryPressureEv
	.section	.text._ZN2v88Platform24OnCriticalMemoryPressureEm,"axG",@progbits,_ZN2v88Platform24OnCriticalMemoryPressureEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88Platform24OnCriticalMemoryPressureEm
	.type	_ZN2v88Platform24OnCriticalMemoryPressureEm, @function
_ZN2v88Platform24OnCriticalMemoryPressureEm:
.LFB1944:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1944:
	.size	_ZN2v88Platform24OnCriticalMemoryPressureEm, .-_ZN2v88Platform24OnCriticalMemoryPressureEm
	.section	.text._ZN2v88internal24GetPlatformPageAllocatorEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24GetPlatformPageAllocatorEv
	.type	_ZN2v88internal24GetPlatformPageAllocatorEv, @function
_ZN2v88internal24GetPlatformPageAllocatorEv:
.LFB3999:
	.cfi_startproc
	endbr64
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %eax
	testb	%al, %al
	je	.L25
	movq	_ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZGVZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	jne	.L26
	movq	_ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	leaq	_ZN2v88Platform16GetPageAllocatorEv(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L9
	movq	$0, _ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip)
.L10:
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %eax
	testb	%al, %al
	je	.L27
.L13:
	leaq	_ZZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rax
	movq	%rax, _ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip)
.L11:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %rdi
	call	__cxa_guard_release@PLT
	movq	_ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L27:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L13
	leaq	_ZZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rdi
	call	_ZN2v84base13PageAllocatorC1Ev@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rdi
	call	__cxa_guard_release@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L9:
	call	*%rax
	movq	%rax, _ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip)
	testq	%rax, %rax
	jne	.L11
	jmp	.L10
	.cfi_endproc
.LFE3999:
	.size	_ZN2v88internal24GetPlatformPageAllocatorEv, .-_ZN2v88internal24GetPlatformPageAllocatorEv
	.section	.text._ZN2v88internal34SetPlatformPageAllocatorForTestingEPNS_13PageAllocatorE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34SetPlatformPageAllocatorForTestingEPNS_13PageAllocatorE
	.type	_ZN2v88internal34SetPlatformPageAllocatorForTestingEPNS_13PageAllocatorE, @function
_ZN2v88internal34SetPlatformPageAllocatorForTestingEPNS_13PageAllocatorE:
.LFB4000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %eax
	movq	%rdi, %rbx
	testb	%al, %al
	je	.L60
.L30:
	movq	_ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %r12
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %eax
	testb	%al, %al
	je	.L61
.L39:
	movq	%r12, %rax
	movq	%rbx, _ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L30
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	leaq	_ZN2v88Platform16GetPageAllocatorEv(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L32
	movq	$0, _ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip)
.L33:
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %eax
	testb	%al, %al
	je	.L62
.L36:
	leaq	_ZZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rax
	movq	%rax, _ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip)
.L34:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %rdi
	call	__cxa_guard_release@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L61:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L39
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	leaq	_ZN2v88Platform16GetPageAllocatorEv(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L41
	movq	$0, _ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip)
.L42:
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %eax
	testb	%al, %al
	je	.L63
.L45:
	leaq	_ZZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rax
	movq	%rax, _ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip)
.L43:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %rdi
	call	__cxa_guard_release@PLT
	movq	%r12, %rax
	movq	%rbx, _ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L62:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L36
	leaq	_ZZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rdi
	call	_ZN2v84base13PageAllocatorC1Ev@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rdi
	call	__cxa_guard_release@PLT
	jmp	.L36
.L63:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L45
	leaq	_ZZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rdi
	call	_ZN2v84base13PageAllocatorC1Ev@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rdi
	call	__cxa_guard_release@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L32:
	call	*%rax
	movq	%rax, _ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip)
	testq	%rax, %rax
	jne	.L34
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L41:
	call	*%rax
	movq	%rax, _ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip)
	testq	%rax, %rax
	jne	.L43
	jmp	.L42
	.cfi_endproc
.LFE4000:
	.size	_ZN2v88internal34SetPlatformPageAllocatorForTestingEPNS_13PageAllocatorE, .-_ZN2v88internal34SetPlatformPageAllocatorForTestingEPNS_13PageAllocatorE
	.section	.text._ZN2v88internal8MalloceddlEPv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8MalloceddlEPv
	.type	_ZN2v88internal8MalloceddlEPv, @function
_ZN2v88internal8MalloceddlEPv:
.LFB4002:
	.cfi_startproc
	endbr64
	jmp	free@PLT
	.cfi_endproc
.LFE4002:
	.size	_ZN2v88internal8MalloceddlEPv, .-_ZN2v88internal8MalloceddlEPv
	.section	.rodata._ZN2v88internal6StrDupEPKc.str1.1,"aMS",@progbits,1
.LC0:
	.string	"NewArray"
	.section	.text._ZN2v88internal6StrDupEPKc,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal6StrDupEPKc
	.type	_ZN2v88internal6StrDupEPKc, @function
_ZN2v88internal6StrDupEPKc:
.LFB4003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	strlen@PLT
	leaq	_ZSt7nothrow(%rip), %rsi
	leaq	1(%rax), %r13
	movq	%rax, %rbx
	movq	%r13, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L69
	movq	%rax, %r8
.L66:
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	memcpy@PLT
	movb	$0, (%rax,%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L69:
	.cfi_restore_state
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r13, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	jne	.L66
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE4003:
	.size	_ZN2v88internal6StrDupEPKc, .-_ZN2v88internal6StrDupEPKc
	.section	.text._ZN2v88internal7StrNDupEPKcm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal7StrNDupEPKcm
	.type	_ZN2v88internal7StrNDupEPKcm, @function
_ZN2v88internal7StrNDupEPKcm:
.LFB4004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	strlen@PLT
	leaq	_ZSt7nothrow(%rip), %rsi
	cmpq	%rbx, %rax
	cmovbe	%rax, %rbx
	leaq	1(%rbx), %r13
	movq	%r13, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L74
	movq	%rax, %r8
.L71:
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	memcpy@PLT
	movb	$0, (%rax,%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L74:
	.cfi_restore_state
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r13, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	jne	.L71
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE4004:
	.size	_ZN2v88internal7StrNDupEPKcm, .-_ZN2v88internal7StrNDupEPKcm
	.section	.rodata._ZN2v88internal12AlignedAllocEmm.str1.1,"aMS",@progbits,1
.LC1:
	.string	"AlignedAlloc"
	.section	.text._ZN2v88internal12AlignedAllocEmm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal12AlignedAllocEmm
	.type	_ZN2v88internal12AlignedAllocEmm, @function
_ZN2v88internal12AlignedAllocEmm:
.LFB4006:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	_ZN2v88Platform24OnCriticalMemoryPressureEm(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	$2, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	(%rdi,%rsi), %rax
	movq	%rax, -72(%rbp)
.L83:
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	posix_memalign@PLT
	testl	%eax, %eax
	jne	.L76
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	jne	.L75
.L76:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	32(%rax), %rax
	cmpq	%r15, %rax
	jne	.L78
.L81:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	leaq	_ZN2v88Platform24OnCriticalMemoryPressureEv(%rip), %rcx
	movq	(%rax), %rdx
	movq	24(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L91
.L82:
	cmpl	$1, %r13d
	jne	.L85
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal2V823FatalProcessOutOfMemoryEPNS0_7IsolateEPKcb@PLT
	.p2align 4,,10
	.p2align 3
.L75:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L92
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	movl	$1, %r13d
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L78:
	movq	-72(%rbp), %rsi
	call	*%rax
	testb	%al, %al
	jne	.L82
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%rax, %rdi
	call	*%rdx
	jmp	.L82
.L92:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4006:
	.size	_ZN2v88internal12AlignedAllocEmm, .-_ZN2v88internal12AlignedAllocEmm
	.section	.text._ZN2v88internal11AlignedFreeEPv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal11AlignedFreeEPv
	.type	_ZN2v88internal11AlignedFreeEPv, @function
_ZN2v88internal11AlignedFreeEPv:
.LFB4645:
	.cfi_startproc
	endbr64
	jmp	free@PLT
	.cfi_endproc
.LFE4645:
	.size	_ZN2v88internal11AlignedFreeEPv, .-_ZN2v88internal11AlignedFreeEPv
	.section	.text._ZN2v88internal16AllocatePageSizeEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal16AllocatePageSizeEv
	.type	_ZN2v88internal16AllocatePageSizeEv, @function
_ZN2v88internal16AllocatePageSizeEv:
.LFB4008:
	.cfi_startproc
	endbr64
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %eax
	testb	%al, %al
	je	.L114
	movq	_ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %rdi
	movq	(%rdi), %rax
	jmp	*16(%rax)
	.p2align 4,,10
	.p2align 3
.L114:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZGVZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	jne	.L115
.L96:
	movq	_ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	leaq	_ZN2v88Platform16GetPageAllocatorEv(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L98
	movq	$0, _ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip)
.L99:
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %eax
	testb	%al, %al
	je	.L116
.L102:
	leaq	_ZZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rax
	movq	%rax, _ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip)
.L100:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %rdi
	call	__cxa_guard_release@PLT
	jmp	.L96
.L116:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L102
	leaq	_ZZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rdi
	call	_ZN2v84base13PageAllocatorC1Ev@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rdi
	call	__cxa_guard_release@PLT
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L98:
	call	*%rax
	movq	%rax, _ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip)
	testq	%rax, %rax
	jne	.L100
	jmp	.L99
	.cfi_endproc
.LFE4008:
	.size	_ZN2v88internal16AllocatePageSizeEv, .-_ZN2v88internal16AllocatePageSizeEv
	.section	.text._ZN2v88internal14CommitPageSizeEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal14CommitPageSizeEv
	.type	_ZN2v88internal14CommitPageSizeEv, @function
_ZN2v88internal14CommitPageSizeEv:
.LFB4009:
	.cfi_startproc
	endbr64
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %eax
	testb	%al, %al
	je	.L137
	movq	_ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %rdi
	movq	(%rdi), %rax
	jmp	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L137:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZGVZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	jne	.L138
.L119:
	movq	_ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	leaq	_ZN2v88Platform16GetPageAllocatorEv(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L121
	movq	$0, _ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip)
.L122:
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %eax
	testb	%al, %al
	je	.L139
.L125:
	leaq	_ZZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rax
	movq	%rax, _ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip)
.L123:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %rdi
	call	__cxa_guard_release@PLT
	jmp	.L119
.L139:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L125
	leaq	_ZZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rdi
	call	_ZN2v84base13PageAllocatorC1Ev@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rdi
	call	__cxa_guard_release@PLT
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L121:
	call	*%rax
	movq	%rax, _ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip)
	testq	%rax, %rax
	jne	.L123
	jmp	.L122
	.cfi_endproc
.LFE4009:
	.size	_ZN2v88internal14CommitPageSizeEv, .-_ZN2v88internal14CommitPageSizeEv
	.section	.text._ZN2v88internal17SetRandomMmapSeedEl,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal17SetRandomMmapSeedEl
	.type	_ZN2v88internal17SetRandomMmapSeedEl, @function
_ZN2v88internal17SetRandomMmapSeedEl:
.LFB4010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %eax
	testb	%al, %al
	je	.L157
.L142:
	movq	_ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L142
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	leaq	_ZN2v88Platform16GetPageAllocatorEv(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L144
	movq	$0, _ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip)
.L145:
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %eax
	testb	%al, %al
	je	.L158
.L148:
	leaq	_ZZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rax
	movq	%rax, _ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip)
.L146:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %rdi
	call	__cxa_guard_release@PLT
	jmp	.L142
.L158:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L148
	leaq	_ZZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rdi
	call	_ZN2v84base13PageAllocatorC1Ev@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rdi
	call	__cxa_guard_release@PLT
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L144:
	call	*%rax
	movq	%rax, _ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip)
	testq	%rax, %rax
	jne	.L146
	jmp	.L145
	.cfi_endproc
.LFE4010:
	.size	_ZN2v88internal17SetRandomMmapSeedEl, .-_ZN2v88internal17SetRandomMmapSeedEl
	.section	.text._ZN2v88internal17GetRandomMmapAddrEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal17GetRandomMmapAddrEv
	.type	_ZN2v88internal17GetRandomMmapAddrEv, @function
_ZN2v88internal17GetRandomMmapAddrEv:
.LFB4011:
	.cfi_startproc
	endbr64
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %eax
	testb	%al, %al
	je	.L179
	movq	_ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %rdi
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L179:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZGVZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	jne	.L180
.L161:
	movq	_ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	leaq	_ZN2v88Platform16GetPageAllocatorEv(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L163
	movq	$0, _ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip)
.L164:
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %eax
	testb	%al, %al
	je	.L181
.L167:
	leaq	_ZZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rax
	movq	%rax, _ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip)
.L165:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip), %rdi
	call	__cxa_guard_release@PLT
	jmp	.L161
.L181:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L167
	leaq	_ZZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rdi
	call	_ZN2v84base13PageAllocatorC1Ev@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator(%rip), %rdi
	call	__cxa_guard_release@PLT
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L163:
	call	*%rax
	movq	%rax, _ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object(%rip)
	testq	%rax, %rax
	jne	.L165
	jmp	.L164
	.cfi_endproc
.LFE4011:
	.size	_ZN2v88internal17GetRandomMmapAddrEv, .-_ZN2v88internal17GetRandomMmapAddrEv
	.section	.text._ZN2v88internal13AllocatePagesEPNS_13PageAllocatorEPvmmNS1_10PermissionE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal13AllocatePagesEPNS_13PageAllocatorEPvmmNS1_10PermissionE
	.type	_ZN2v88internal13AllocatePagesEPNS_13PageAllocatorEPvmmNS1_10PermissionE, @function
_ZN2v88internal13AllocatePagesEPNS_13PageAllocatorEPvmmNS1_10PermissionE:
.LFB4012:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	(%rdx,%rcx), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movl	%r8d, -56(%rbp)
	movl	$2, -52(%rbp)
	movq	%rax, -72(%rbp)
.L189:
	movq	(%rbx), %rax
	movl	-56(%rbp), %r8d
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	*48(%rax)
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L182
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*16(%rax)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	leaq	_ZN2v88Platform24OnCriticalMemoryPressureEm(%rip), %rcx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L184
.L187:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	leaq	_ZN2v88Platform24OnCriticalMemoryPressureEv(%rip), %rsi
	movq	(%rax), %rdx
	movq	24(%rdx), %rdx
	cmpq	%rsi, %rdx
	jne	.L196
.L188:
	cmpl	$1, -52(%rbp)
	jne	.L190
.L182:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	movl	$1, -52(%rbp)
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L184:
	movq	-72(%rbp), %rsi
	subq	-64(%rbp), %rsi
	call	*%rax
	testb	%al, %al
	jne	.L188
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L196:
	movq	%rax, %rdi
	call	*%rdx
	jmp	.L188
	.cfi_endproc
.LFE4012:
	.size	_ZN2v88internal13AllocatePagesEPNS_13PageAllocatorEPvmmNS1_10PermissionE, .-_ZN2v88internal13AllocatePagesEPNS_13PageAllocatorEPvmmNS1_10PermissionE
	.section	.text._ZN2v88internal9FreePagesEPNS_13PageAllocatorEPvm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal9FreePagesEPNS_13PageAllocatorEPvm
	.type	_ZN2v88internal9FreePagesEPNS_13PageAllocatorEPvm, @function
_ZN2v88internal9FreePagesEPNS_13PageAllocatorEPvm:
.LFB4013:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*56(%rax)
	.cfi_endproc
.LFE4013:
	.size	_ZN2v88internal9FreePagesEPNS_13PageAllocatorEPvm, .-_ZN2v88internal9FreePagesEPNS_13PageAllocatorEPvm
	.section	.text._ZN2v88internal12ReleasePagesEPNS_13PageAllocatorEPvmm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal12ReleasePagesEPNS_13PageAllocatorEPvmm
	.type	_ZN2v88internal12ReleasePagesEPNS_13PageAllocatorEPvmm, @function
_ZN2v88internal12ReleasePagesEPNS_13PageAllocatorEPvmm:
.LFB4014:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*64(%rax)
	.cfi_endproc
.LFE4014:
	.size	_ZN2v88internal12ReleasePagesEPNS_13PageAllocatorEPvmm, .-_ZN2v88internal12ReleasePagesEPNS_13PageAllocatorEPvmm
	.section	.text._ZN2v88internal14SetPermissionsEPNS_13PageAllocatorEPvmNS1_10PermissionE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal14SetPermissionsEPNS_13PageAllocatorEPvmNS1_10PermissionE
	.type	_ZN2v88internal14SetPermissionsEPNS_13PageAllocatorEPvmNS1_10PermissionE, @function
_ZN2v88internal14SetPermissionsEPNS_13PageAllocatorEPvmNS1_10PermissionE:
.LFB4015:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*72(%rax)
	.cfi_endproc
.LFE4015:
	.size	_ZN2v88internal14SetPermissionsEPNS_13PageAllocatorEPvmNS1_10PermissionE, .-_ZN2v88internal14SetPermissionsEPNS_13PageAllocatorEPvmNS1_10PermissionE
	.section	.text._ZN2v88internal24OnCriticalMemoryPressureEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24OnCriticalMemoryPressureEm
	.type	_ZN2v88internal24OnCriticalMemoryPressureEm, @function
_ZN2v88internal24OnCriticalMemoryPressureEm:
.LFB4016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	leaq	_ZN2v88Platform24OnCriticalMemoryPressureEm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L201
.L204:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	leaq	_ZN2v88Platform24OnCriticalMemoryPressureEv(%rip), %rcx
	movq	(%rax), %rdx
	movq	24(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L212
.L205:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	movq	%r12, %rsi
	call	*%rax
	testb	%al, %al
	je	.L204
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
	movq	%rax, %rdi
	call	*%rdx
	jmp	.L205
	.cfi_endproc
.LFE4016:
	.size	_ZN2v88internal24OnCriticalMemoryPressureEm, .-_ZN2v88internal24OnCriticalMemoryPressureEm
	.section	.text._ZN2v88internal14AllocWithRetryEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal14AllocWithRetryEm
	.type	_ZN2v88internal14AllocWithRetryEm, @function
_ZN2v88internal14AllocWithRetryEm:
.LFB4005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	call	malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L219
.L213:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L219:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal24OnCriticalMemoryPressureEm
	testb	%al, %al
	je	.L213
	movq	%r13, %rdi
	call	malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L213
	movq	%r13, %rdi
	call	_ZN2v88internal24OnCriticalMemoryPressureEm
	jmp	.L213
	.cfi_endproc
.LFE4005:
	.size	_ZN2v88internal14AllocWithRetryEm, .-_ZN2v88internal14AllocWithRetryEm
	.section	.rodata._ZN2v88internal8MallocednwEm.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Malloced operator new"
	.section	.text._ZN2v88internal8MallocednwEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8MallocednwEm
	.type	_ZN2v88internal8MallocednwEm, @function
_ZN2v88internal8MallocednwEm:
.LFB4001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	malloc@PLT
	testq	%rax, %rax
	je	.L227
.L220:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L227:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal24OnCriticalMemoryPressureEm
	testb	%al, %al
	je	.L222
	movq	%r12, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	jne	.L220
	movq	%r12, %rdi
	call	_ZN2v88internal24OnCriticalMemoryPressureEm
.L222:
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal2V823FatalProcessOutOfMemoryEPNS0_7IsolateEPKcb@PLT
	.cfi_endproc
.LFE4001:
	.size	_ZN2v88internal8MallocednwEm, .-_ZN2v88internal8MallocednwEm
	.section	.text._ZN2v88internal13VirtualMemoryC2EPNS_13PageAllocatorEmPvm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13VirtualMemoryC2EPNS_13PageAllocatorEmPvm
	.type	_ZN2v88internal13VirtualMemoryC2EPNS_13PageAllocatorEmPvm, @function
_ZN2v88internal13VirtualMemoryC2EPNS_13PageAllocatorEmPvm:
.LFB4018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$40, %rsp
	movups	%xmm0, 8(%rbx)
	movq	(%rsi), %rax
	movq	%rsi, (%rbx)
	movq	%rcx, -64(%rbp)
	call	*16(%rax)
	movq	(%rbx), %r15
	movl	$2, -52(%rbp)
	movq	%rax, %rdx
	leaq	-1(%rax,%r12), %r12
	leaq	-1(%rax,%r14), %r13
	movq	%rbx, -72(%rbp)
	negq	%rdx
	movq	%r15, %rbx
	andq	%rdx, %r12
	andq	%rdx, %r13
	leaq	0(%r13,%r12), %rax
	movq	%r12, %r15
	movq	%rax, -80(%rbp)
.L235:
	movq	(%rbx), %rax
	xorl	%r8d, %r8d
	movq	-64(%rbp), %rsi
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	*48(%rax)
	testq	%rax, %rax
	jne	.L229
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*16(%rax)
	movq	%rax, %r12
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	leaq	_ZN2v88Platform24OnCriticalMemoryPressureEm(%rip), %rcx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L230
.L233:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	leaq	_ZN2v88Platform24OnCriticalMemoryPressureEv(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L243
.L234:
	cmpl	$1, -52(%rbp)
	jne	.L244
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	movl	$1, -52(%rbp)
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L229:
	movq	-72(%rbp), %rbx
	movq	%rax, 8(%rbx)
	movq	%r14, 16(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	movq	-80(%rbp), %rsi
	subq	%r12, %rsi
	call	*%rax
	testb	%al, %al
	jne	.L234
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L243:
	call	*%rax
	jmp	.L234
	.cfi_endproc
.LFE4018:
	.size	_ZN2v88internal13VirtualMemoryC2EPNS_13PageAllocatorEmPvm, .-_ZN2v88internal13VirtualMemoryC2EPNS_13PageAllocatorEmPvm
	.globl	_ZN2v88internal13VirtualMemoryC1EPNS_13PageAllocatorEmPvm
	.set	_ZN2v88internal13VirtualMemoryC1EPNS_13PageAllocatorEmPvm,_ZN2v88internal13VirtualMemoryC2EPNS_13PageAllocatorEmPvm
	.section	.rodata._ZN2v88internal13VirtualMemoryD2Ev.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"FreePages(page_allocator, reinterpret_cast<void*>(region.begin()), RoundUp(region.size(), page_allocator->AllocatePageSize()))"
	.section	.rodata._ZN2v88internal13VirtualMemoryD2Ev.str1.1,"aMS",@progbits,1
.LC4:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal13VirtualMemoryD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13VirtualMemoryD2Ev
	.type	_ZN2v88internal13VirtualMemoryD2Ev, @function
_ZN2v88internal13VirtualMemoryD2Ev:
.LFB4021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %r12
	testq	%r12, %r12
	jne	.L251
.L245:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore_state
	movq	(%rdi), %r13
	pxor	%xmm0, %xmm0
	movq	16(%rdi), %rbx
	movq	$0, (%rdi)
	movups	%xmm0, 8(%rdi)
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	-1(%rbx,%rax), %rdx
	negq	%rax
	andq	%rax, %rdx
	movq	0(%r13), %rax
	call	*56(%rax)
	testb	%al, %al
	jne	.L245
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE4021:
	.size	_ZN2v88internal13VirtualMemoryD2Ev, .-_ZN2v88internal13VirtualMemoryD2Ev
	.globl	_ZN2v88internal13VirtualMemoryD1Ev
	.set	_ZN2v88internal13VirtualMemoryD1Ev,_ZN2v88internal13VirtualMemoryD2Ev
	.section	.text._ZN2v88internal13VirtualMemory5ResetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13VirtualMemory5ResetEv
	.type	_ZN2v88internal13VirtualMemory5ResetEv, @function
_ZN2v88internal13VirtualMemory5ResetEv:
.LFB4023:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	$0, (%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE4023:
	.size	_ZN2v88internal13VirtualMemory5ResetEv, .-_ZN2v88internal13VirtualMemory5ResetEv
	.section	.rodata._ZN2v88internal13VirtualMemory14SetPermissionsEmmNS_13PageAllocator10PermissionE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"InVM(address, size)"
	.section	.text._ZN2v88internal13VirtualMemory14SetPermissionsEmmNS_13PageAllocator10PermissionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13VirtualMemory14SetPermissionsEmmNS_13PageAllocator10PermissionE
	.type	_ZN2v88internal13VirtualMemory14SetPermissionsEmmNS_13PageAllocator10PermissionE, @function
_ZN2v88internal13VirtualMemory14SetPermissionsEmmNS_13PageAllocator10PermissionE:
.LFB4024:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %r8
	movq	%rsi, %rax
	subq	8(%rdi), %rax
	cmpq	%r8, %rax
	jnb	.L254
	addq	%rdx, %rax
	cmpq	%rax, %r8
	jb	.L254
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*72(%rax)
	.p2align 4,,10
	.p2align 3
.L254:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC5(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE4024:
	.size	_ZN2v88internal13VirtualMemory14SetPermissionsEmmNS_13PageAllocator10PermissionE, .-_ZN2v88internal13VirtualMemory14SetPermissionsEmmNS_13PageAllocator10PermissionE
	.section	.rodata._ZN2v88internal13VirtualMemory7ReleaseEm.str1.1,"aMS",@progbits,1
.LC6:
	.string	"InVM(free_start, free_size)"
	.section	.rodata._ZN2v88internal13VirtualMemory7ReleaseEm.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"ReleasePages(page_allocator_, reinterpret_cast<void*>(region_.begin()), old_size, region_.size())"
	.section	.text._ZN2v88internal13VirtualMemory7ReleaseEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13VirtualMemory7ReleaseEm
	.type	_ZN2v88internal13VirtualMemory7ReleaseEm, @function
_ZN2v88internal13VirtualMemory7ReleaseEm:
.LFB4025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %rsi
	movq	16(%rdi), %rdx
	leaq	(%rsi,%rdx), %rax
	subq	%rcx, %rax
	subq	%rsi, %rcx
	cmpq	%rdx, %rcx
	jb	.L263
	leaq	.LC6(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L263:
	movq	%rcx, 16(%rdi)
	movq	(%rdi), %rdi
	movq	%rax, %r12
	movq	(%rdi), %rax
	call	*64(%rax)
	testb	%al, %al
	je	.L264
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L264:
	.cfi_restore_state
	leaq	.LC7(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE4025:
	.size	_ZN2v88internal13VirtualMemory7ReleaseEm, .-_ZN2v88internal13VirtualMemory7ReleaseEm
	.section	.text._ZN2v88internal13VirtualMemory4FreeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13VirtualMemory4FreeEv
	.type	_ZN2v88internal13VirtualMemory4FreeEv, @function
_ZN2v88internal13VirtualMemory4FreeEv:
.LFB4026:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %r12
	movq	8(%rdi), %r13
	movq	$0, (%rdi)
	movq	16(%rdi), %rbx
	movups	%xmm0, 8(%rdi)
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-1(%rbx,%rax), %rdx
	negq	%rax
	andq	%rax, %rdx
	movq	(%r12), %rax
	call	*56(%rax)
	testb	%al, %al
	je	.L268
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE4026:
	.size	_ZN2v88internal13VirtualMemory4FreeEv, .-_ZN2v88internal13VirtualMemory4FreeEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal24GetPlatformPageAllocatorEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal24GetPlatformPageAllocatorEv, @function
_GLOBAL__sub_I__ZN2v88internal24GetPlatformPageAllocatorEv:
.LFB4633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE4633:
	.size	_GLOBAL__sub_I__ZN2v88internal24GetPlatformPageAllocatorEv, .-_GLOBAL__sub_I__ZN2v88internal24GetPlatformPageAllocatorEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal24GetPlatformPageAllocatorEv
	.section	.bss._ZGVZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object,"aw",@nobits
	.align 8
	.type	_ZGVZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object, @object
	.size	_ZGVZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object, 8
_ZGVZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object:
	.zero	8
	.section	.bss._ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object, @object
	.size	_ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object, 8
_ZZN2v88internal12_GLOBAL__N_123GetPageTableInitializerEvE6object:
	.zero	8
	.section	.bss._ZGVZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator,"aw",@nobits
	.align 8
	.type	_ZGVZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator, @object
	.size	_ZGVZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator, 8
_ZGVZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator:
	.zero	8
	.section	.bss._ZZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator,"aw",@nobits
	.align 16
	.type	_ZZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator, @object
	.size	_ZZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator, 24
_ZZN2v88internal12_GLOBAL__N_124PageAllocatorInitializerC4EvE22default_page_allocator:
	.zero	24
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
