	.file	"tagged-impl.cc"
	.text
	.section	.text._ZN2v88internal8OFStreamD1Ev,"axG",@progbits,_ZN2v88internal8OFStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8OFStreamD1Ev
	.type	_ZN2v88internal8OFStreamD1Ev, @function
_ZN2v88internal8OFStreamD1Ev:
.LFB5697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE5697:
	.size	_ZN2v88internal8OFStreamD1Ev, .-_ZN2v88internal8OFStreamD1Ev
	.section	.text._ZN2v88internal8OFStreamD0Ev,"axG",@progbits,_ZN2v88internal8OFStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.type	_ZTv0_n24_N2v88internal8OFStreamD0Ev, @function
_ZTv0_n24_N2v88internal8OFStreamD0Ev:
.LFB6687:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6687:
	.size	_ZTv0_n24_N2v88internal8OFStreamD0Ev, .-_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8OFStreamD0Ev
	.type	_ZN2v88internal8OFStreamD0Ev, @function
_ZN2v88internal8OFStreamD0Ev:
.LFB5698:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5698:
	.size	_ZN2v88internal8OFStreamD0Ev, .-_ZN2v88internal8OFStreamD0Ev
	.section	.text._ZN2v88internal8OFStreamD1Ev,"axG",@progbits,_ZN2v88internal8OFStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.type	_ZTv0_n24_N2v88internal8OFStreamD1Ev, @function
_ZTv0_n24_N2v88internal8OFStreamD1Ev:
.LFB6688:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE6688:
	.size	_ZTv0_n24_N2v88internal8OFStreamD1Ev, .-_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.section	.text._ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC2Em,"axG",@progbits,_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC2Em,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC2Em
	.type	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC2Em, @function
_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC2Em:
.LFB5289:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE5289:
	.size	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC2Em, .-_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC2Em
	.section	.text._ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC1Em,"axG",@progbits,_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC1Em,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC1Em
	.type	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC1Em, @function
_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC1Em:
.LFB5290:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE5290:
	.size	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC1Em, .-_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC1Em
	.section	.text._ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE3ptrEv,"axG",@progbits,_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE3ptrEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE3ptrEv
	.type	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE3ptrEv, @function
_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE3ptrEv:
.LFB5301:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE5301:
	.size	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE3ptrEv, .-_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE3ptrEv
	.section	.text._ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC2Ev,"axG",@progbits,_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC2Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC2Ev
	.type	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC2Ev, @function
_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC2Ev:
.LFB5611:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	ret
	.cfi_endproc
.LFE5611:
	.size	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC2Ev, .-_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC2Ev
	.section	.text._ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC1Ev,"axG",@progbits,_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC1Ev
	.type	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC1Ev, @function
_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC1Ev:
.LFB5612:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	ret
	.cfi_endproc
.LFE5612:
	.size	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC1Ev, .-_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEC1Ev
	.section	.text._ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC2Ev,"axG",@progbits,_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC2Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC2Ev
	.type	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC2Ev, @function
_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC2Ev:
.LFB5620:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	ret
	.cfi_endproc
.LFE5620:
	.size	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC2Ev, .-_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC2Ev
	.section	.text._ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC1Ev,"axG",@progbits,_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC1Ev
	.type	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC1Ev, @function
_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC1Ev:
.LFB5621:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	ret
	.cfi_endproc
.LFE5621:
	.size	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC1Ev, .-_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC1Ev
	.section	.text._ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC2Em,"axG",@progbits,_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC2Em,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC2Em
	.type	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC2Em, @function
_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC2Em:
.LFB5626:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE5626:
	.size	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC2Em, .-_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC2Em
	.section	.text._ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC1Em,"axG",@progbits,_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC1Em,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC1Em
	.type	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC1Em, @function
_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC1Em:
.LFB5627:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE5627:
	.size	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC1Em, .-_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEC1Em
	.section	.text._ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEltES3_,"axG",@progbits,_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEltES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEltES3_
	.type	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEltES3_, @function
_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEltES3_:
.LFB6185:
	.cfi_startproc
	endbr64
	cmpq	%rsi, (%rdi)
	setb	%al
	ret
	.cfi_endproc
.LFE6185:
	.size	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEltES3_, .-_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmEltES3_
	.section	.text._ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE8IsObjectEv,"axG",@progbits,_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE8IsObjectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE8IsObjectEv
	.type	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE8IsObjectEv, @function
_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE8IsObjectEv:
.LFB6186:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE6186:
	.size	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE8IsObjectEv, .-_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE8IsObjectEv
	.section	.text._ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE5IsSmiEv,"axG",@progbits,_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE5IsSmiEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE5IsSmiEv
	.type	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE5IsSmiEv, @function
_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE5IsSmiEv:
.LFB6188:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	notq	%rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE6188:
	.size	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE5IsSmiEv, .-_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE5IsSmiEv
	.section	.text._ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE12IsHeapObjectEv,"axG",@progbits,_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE12IsHeapObjectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE12IsHeapObjectEv
	.type	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE12IsHeapObjectEv, @function
_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE12IsHeapObjectEv:
.LFB6189:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE6189:
	.size	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE12IsHeapObjectEv, .-_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE12IsHeapObjectEv
	.section	.text._ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE9IsClearedEv,"axG",@progbits,_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE9IsClearedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE9IsClearedEv
	.type	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE9IsClearedEv, @function
_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE9IsClearedEv:
.LFB6191:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6191:
	.size	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE9IsClearedEv, .-_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE9IsClearedEv
	.section	.text._ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE14IsStrongOrWeakEv,"axG",@progbits,_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE14IsStrongOrWeakEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE14IsStrongOrWeakEv
	.type	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE14IsStrongOrWeakEv, @function
_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE14IsStrongOrWeakEv:
.LFB6192:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE6192:
	.size	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE14IsStrongOrWeakEv, .-_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE14IsStrongOrWeakEv
	.section	.text._ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE8IsStrongEv,"axG",@progbits,_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE8IsStrongEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE8IsStrongEv
	.type	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE8IsStrongEv, @function
_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE8IsStrongEv:
.LFB6190:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE6190:
	.size	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE8IsStrongEv, .-_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE8IsStrongEv
	.section	.text._ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE6IsWeakEv,"axG",@progbits,_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE6IsWeakEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE6IsWeakEv
	.type	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE6IsWeakEv, @function
_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE6IsWeakEv:
.LFB6193:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6193:
	.size	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE6IsWeakEv, .-_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE6IsWeakEv
	.section	.text._ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE15IsWeakOrClearedEv,"axG",@progbits,_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE15IsWeakOrClearedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE15IsWeakOrClearedEv
	.type	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE15IsWeakOrClearedEv, @function
_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE15IsWeakOrClearedEv:
.LFB6187:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6187:
	.size	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE15IsWeakOrClearedEv, .-_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE15IsWeakOrClearedEv
	.section	.text._ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE10ShortPrintEP8_IO_FILE,"axG",@progbits,_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE10ShortPrintEP8_IO_FILE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE10ShortPrintEP8_IO_FILE
	.type	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE10ShortPrintEP8_IO_FILE, @function
_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE10ShortPrintEP8_IO_FILE:
.LFB6194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-368(%rbp), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$368, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movq	(%rbx), %rax
	leaq	-376(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -376(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rax, -288(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdx, %xmm0
	leaq	-304(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -368(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-288(%rbp), %rdi
	movq	%rax, -368(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -288(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L31
	addq	$368, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L31:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6194:
	.size	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE10ShortPrintEP8_IO_FILE, .-_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE10ShortPrintEP8_IO_FILE
	.section	.text._ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE10ShortPrintEPNS0_12StringStreamE,"axG",@progbits,_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE10ShortPrintEPNS0_12StringStreamE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE10ShortPrintEPNS0_12StringStreamE
	.type	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE10ShortPrintEPNS0_12StringStreamE, @function
_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE10ShortPrintEPNS0_12StringStreamE:
.LFB6195:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-432(%rbp), %r15
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-368(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-320(%rbp), %r12
	pushq	%rbx
	subq	$472, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -496(%rbp)
	movq	.LC0(%rip), %xmm1
	movq	%r12, %rdi
	movq	%rsi, -488(%rbp)
	movhps	.LC1(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -512(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rbx, -432(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movq	-24(%rbx), %rdx
	movq	%rax, -432(%rbp,%rdx)
	movq	-432(%rbp), %rdx
	movq	-24(%rdx), %rdi
	addq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-512(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%r14, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -512(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-496(%rbp), %rax
	movq	%r15, %rdi
	leaq	-472(%rbp), %rsi
	leaq	-448(%rbp), %r15
	movq	(%rax), %rax
	movq	%rax, -472(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movq	-384(%rbp), %rax
	movq	%r15, -464(%rbp)
	leaq	-464(%rbp), %rdi
	movq	$0, -456(%rbp)
	movb	$0, -448(%rbp)
	testq	%rax, %rax
	je	.L33
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L40
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L35:
	movq	-464(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -496(%rbp)
	call	strlen@PLT
	movq	-488(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	-496(%rbp), %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movq	-464(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L36
	call	_ZdlPv@PLT
.L36:
	movq	.LC0(%rip), %xmm0
	movq	%r14, -320(%rbp)
	movq	-352(%rbp), %rdi
	movhps	.LC2(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-512(%rbp), %rdi
	je	.L37
	call	_ZdlPv@PLT
.L37:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%rbx, -432(%rbp)
	movq	-24(%rbx), %rax
	movq	%r12, %rdi
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L41
	addq	$472, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L33:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L35
.L41:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6195:
	.size	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE10ShortPrintEPNS0_12StringStreamE, .-_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE10ShortPrintEPNS0_12StringStreamE
	.section	.text._ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE10ShortPrintERSo,"axG",@progbits,_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE10ShortPrintERSo,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE10ShortPrintERSo
	.type	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE10ShortPrintERSo, @function
_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE10ShortPrintERSo:
.LFB6196:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%r8), %rax
	leaq	-16(%rbp), %rsi
	movq	%rax, -16(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L45
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L45:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6196:
	.size	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE10ShortPrintERSo, .-_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE10ShortPrintERSo
	.section	.text._ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE5PrintEv,"axG",@progbits,_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE5PrintEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE5PrintEv
	.type	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE5PrintEv, @function
_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE5PrintEv:
.LFB6197:
	.cfi_startproc
	endbr64
	movq	stdout(%rip), %rsi
	jmp	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE10ShortPrintEP8_IO_FILE
	.cfi_endproc
.LFE6197:
	.size	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE5PrintEv, .-_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE5PrintEv
	.section	.text._ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE5PrintERSo,"axG",@progbits,_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE5PrintERSo,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE5PrintERSo
	.type	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE5PrintERSo, @function
_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE5PrintERSo:
.LFB6198:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE10ShortPrintERSo
	.cfi_endproc
.LFE6198:
	.size	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE5PrintERSo, .-_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE1EmE5PrintERSo
	.section	.text._ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEltES3_,"axG",@progbits,_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEltES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEltES3_
	.type	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEltES3_, @function
_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEltES3_:
.LFB6199:
	.cfi_startproc
	endbr64
	cmpq	%rsi, (%rdi)
	setb	%al
	ret
	.cfi_endproc
.LFE6199:
	.size	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEltES3_, .-_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmEltES3_
	.section	.text._ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE3ptrEv,"axG",@progbits,_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE3ptrEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE3ptrEv
	.type	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE3ptrEv, @function
_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE3ptrEv:
.LFB6200:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE6200:
	.size	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE3ptrEv, .-_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE3ptrEv
	.section	.text._ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE8IsObjectEv,"axG",@progbits,_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE8IsObjectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE8IsObjectEv
	.type	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE8IsObjectEv, @function
_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE8IsObjectEv:
.LFB6201:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$3, %eax
	cmpq	$3, %rax
	setne	%al
	ret
	.cfi_endproc
.LFE6201:
	.size	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE8IsObjectEv, .-_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE8IsObjectEv
	.section	.text._ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE5IsSmiEv,"axG",@progbits,_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE5IsSmiEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE5IsSmiEv
	.type	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE5IsSmiEv, @function
_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE5IsSmiEv:
.LFB6203:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	notq	%rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE6203:
	.size	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE5IsSmiEv, .-_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE5IsSmiEv
	.section	.text._ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE12IsHeapObjectEv,"axG",@progbits,_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE12IsHeapObjectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE12IsHeapObjectEv
	.type	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE12IsHeapObjectEv, @function
_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE12IsHeapObjectEv:
.LFB6204:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$3, %eax
	cmpq	$1, %rax
	sete	%al
	ret
	.cfi_endproc
.LFE6204:
	.size	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE12IsHeapObjectEv, .-_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE12IsHeapObjectEv
	.section	.text._ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE9IsClearedEv,"axG",@progbits,_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE9IsClearedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE9IsClearedEv
	.type	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE9IsClearedEv, @function
_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE9IsClearedEv:
.LFB6206:
	.cfi_startproc
	endbr64
	cmpl	$3, (%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE6206:
	.size	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE9IsClearedEv, .-_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE9IsClearedEv
	.section	.text._ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE14IsStrongOrWeakEv,"axG",@progbits,_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE14IsStrongOrWeakEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE14IsStrongOrWeakEv
	.type	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE14IsStrongOrWeakEv, @function
_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE14IsStrongOrWeakEv:
.LFB6207:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	xorl	%eax, %eax
	testb	$1, %dl
	je	.L54
	cmpl	$3, %edx
	setne	%al
.L54:
	ret
	.cfi_endproc
.LFE6207:
	.size	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE14IsStrongOrWeakEv, .-_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE14IsStrongOrWeakEv
	.section	.text._ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE8IsStrongEv,"axG",@progbits,_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE8IsStrongEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE8IsStrongEv
	.type	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE8IsStrongEv, @function
_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE8IsStrongEv:
.LFB6205:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$3, %eax
	cmpq	$1, %rax
	sete	%al
	ret
	.cfi_endproc
.LFE6205:
	.size	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE8IsStrongEv, .-_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE8IsStrongEv
	.section	.text._ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE6IsWeakEv,"axG",@progbits,_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE6IsWeakEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE6IsWeakEv
	.type	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE6IsWeakEv, @function
_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE6IsWeakEv:
.LFB6208:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	xorl	%eax, %eax
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$3, %rcx
	je	.L61
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	cmpl	$3, %edx
	setne	%al
	ret
	.cfi_endproc
.LFE6208:
	.size	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE6IsWeakEv, .-_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE6IsWeakEv
	.section	.text._ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE15IsWeakOrClearedEv,"axG",@progbits,_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE15IsWeakOrClearedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE15IsWeakOrClearedEv
	.type	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE15IsWeakOrClearedEv, @function
_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE15IsWeakOrClearedEv:
.LFB6202:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$3, %eax
	cmpq	$3, %rax
	sete	%al
	ret
	.cfi_endproc
.LFE6202:
	.size	_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE15IsWeakOrClearedEv, .-_ZNK2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE15IsWeakOrClearedEv
	.section	.text._ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE10ShortPrintEP8_IO_FILE,"axG",@progbits,_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE10ShortPrintEP8_IO_FILE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE10ShortPrintEP8_IO_FILE
	.type	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE10ShortPrintEP8_IO_FILE, @function
_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE10ShortPrintEP8_IO_FILE:
.LFB6209:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-368(%rbp), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$368, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movq	(%rbx), %rax
	leaq	-376(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -376(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rax, -288(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdx, %xmm0
	leaq	-304(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -368(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-288(%rbp), %rdi
	movq	%rax, -368(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -288(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L66
	addq	$368, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L66:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6209:
	.size	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE10ShortPrintEP8_IO_FILE, .-_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE10ShortPrintEP8_IO_FILE
	.section	.text._ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE10ShortPrintEPNS0_12StringStreamE,"axG",@progbits,_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE10ShortPrintEPNS0_12StringStreamE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE10ShortPrintEPNS0_12StringStreamE
	.type	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE10ShortPrintEPNS0_12StringStreamE, @function
_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE10ShortPrintEPNS0_12StringStreamE:
.LFB6210:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-432(%rbp), %r15
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-368(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-320(%rbp), %r12
	pushq	%rbx
	subq	$472, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -496(%rbp)
	movq	.LC0(%rip), %xmm1
	movq	%r12, %rdi
	movq	%rsi, -488(%rbp)
	movhps	.LC1(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -512(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rbx, -432(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movq	-24(%rbx), %rdx
	movq	%rax, -432(%rbp,%rdx)
	movq	-432(%rbp), %rdx
	movq	-24(%rdx), %rdi
	addq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-512(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%r14, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -512(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-496(%rbp), %rax
	movq	%r15, %rdi
	leaq	-472(%rbp), %rsi
	leaq	-448(%rbp), %r15
	movq	(%rax), %rax
	movq	%rax, -472(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movq	-384(%rbp), %rax
	movq	%r15, -464(%rbp)
	leaq	-464(%rbp), %rdi
	movq	$0, -456(%rbp)
	movb	$0, -448(%rbp)
	testq	%rax, %rax
	je	.L68
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L75
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L70:
	movq	-464(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -496(%rbp)
	call	strlen@PLT
	movq	-488(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	-496(%rbp), %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movq	-464(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L71
	call	_ZdlPv@PLT
.L71:
	movq	.LC0(%rip), %xmm0
	movq	%r14, -320(%rbp)
	movq	-352(%rbp), %rdi
	movhps	.LC2(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-512(%rbp), %rdi
	je	.L72
	call	_ZdlPv@PLT
.L72:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%rbx, -432(%rbp)
	movq	-24(%rbx), %rax
	movq	%r12, %rdi
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L76
	addq	$472, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L68:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L70
.L76:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6210:
	.size	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE10ShortPrintEPNS0_12StringStreamE, .-_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE10ShortPrintEPNS0_12StringStreamE
	.section	.text._ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE10ShortPrintERSo,"axG",@progbits,_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE10ShortPrintERSo,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE10ShortPrintERSo
	.type	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE10ShortPrintERSo, @function
_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE10ShortPrintERSo:
.LFB6211:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%r8), %rax
	leaq	-16(%rbp), %rsi
	movq	%rax, -16(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L80
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L80:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6211:
	.size	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE10ShortPrintERSo, .-_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE10ShortPrintERSo
	.section	.text._ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE5PrintEv,"axG",@progbits,_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE5PrintEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE5PrintEv
	.type	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE5PrintEv, @function
_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE5PrintEv:
.LFB6212:
	.cfi_startproc
	endbr64
	movq	stdout(%rip), %rsi
	jmp	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE10ShortPrintEP8_IO_FILE
	.cfi_endproc
.LFE6212:
	.size	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE5PrintEv, .-_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE5PrintEv
	.section	.text._ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE5PrintERSo,"axG",@progbits,_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE5PrintERSo,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE5PrintERSo
	.type	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE5PrintERSo, @function
_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE5PrintERSo:
.LFB6213:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE10ShortPrintERSo
	.cfi_endproc
.LFE6213:
	.size	_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE5PrintERSo, .-_ZN2v88internal10TaggedImplILNS0_23HeapObjectReferenceTypeE0EmE5PrintERSo
	.section	.text.startup._GLOBAL__sub_I_tagged_impl.cc,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I_tagged_impl.cc, @function
_GLOBAL__sub_I_tagged_impl.cc:
.LFB6669:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE6669:
	.size	_GLOBAL__sub_I_tagged_impl.cc, .-_GLOBAL__sub_I_tagged_impl.cc
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I_tagged_impl.cc
	.hidden	_ZTCN2v88internal8OFStreamE0_So
	.weak	_ZTCN2v88internal8OFStreamE0_So
	.section	.rodata._ZTCN2v88internal8OFStreamE0_So,"aG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal8OFStreamE0_So, @object
	.size	_ZTCN2v88internal8OFStreamE0_So, 80
_ZTCN2v88internal8OFStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal8OFStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal8OFStreamE,"awG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal8OFStreamE, @object
	.size	_ZTTN2v88internal8OFStreamE, 32
_ZTTN2v88internal8OFStreamE:
	.quad	_ZTVN2v88internal8OFStreamE+24
	.quad	_ZTCN2v88internal8OFStreamE0_So+24
	.quad	_ZTCN2v88internal8OFStreamE0_So+64
	.quad	_ZTVN2v88internal8OFStreamE+64
	.weak	_ZTVN2v88internal8OFStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal8OFStreamE,"awG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal8OFStreamE, @object
	.size	_ZTVN2v88internal8OFStreamE, 80
_ZTVN2v88internal8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8OFStreamD1Ev
	.quad	_ZN2v88internal8OFStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.data.rel.ro,"aw"
	.align 8
.LC0:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC1:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC2:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
