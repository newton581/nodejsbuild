	.file	"optimizing-compile-dispatcher.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB3855:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE3855:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB3856:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3856:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB3858:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3858:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal27OptimizingCompileDispatcher11CompileTaskD2Ev,"axG",@progbits,_ZN2v88internal27OptimizingCompileDispatcher11CompileTaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal27OptimizingCompileDispatcher11CompileTaskD2Ev
	.type	_ZN2v88internal27OptimizingCompileDispatcher11CompileTaskD2Ev, @function
_ZN2v88internal27OptimizingCompileDispatcher11CompileTaskD2Ev:
.LFB21793:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE21793:
	.size	_ZN2v88internal27OptimizingCompileDispatcher11CompileTaskD2Ev, .-_ZN2v88internal27OptimizingCompileDispatcher11CompileTaskD2Ev
	.weak	_ZN2v88internal27OptimizingCompileDispatcher11CompileTaskD1Ev
	.set	_ZN2v88internal27OptimizingCompileDispatcher11CompileTaskD1Ev,_ZN2v88internal27OptimizingCompileDispatcher11CompileTaskD2Ev
	.section	.text._ZN2v88internal27OptimizingCompileDispatcher11CompileTaskD0Ev,"axG",@progbits,_ZN2v88internal27OptimizingCompileDispatcher11CompileTaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal27OptimizingCompileDispatcher11CompileTaskD0Ev
	.type	_ZN2v88internal27OptimizingCompileDispatcher11CompileTaskD0Ev, @function
_ZN2v88internal27OptimizingCompileDispatcher11CompileTaskD0Ev:
.LFB21795:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE21795:
	.size	_ZN2v88internal27OptimizingCompileDispatcher11CompileTaskD0Ev, .-_ZN2v88internal27OptimizingCompileDispatcher11CompileTaskD0Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_121DisposeCompilationJobEPNS0_23OptimizedCompilationJobEb,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_121DisposeCompilationJobEPNS0_23OptimizedCompilationJobEb, @function
_ZN2v88internal12_GLOBAL__N_121DisposeCompilationJobEPNS0_23OptimizedCompilationJobEb:
.LFB18755:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testb	%sil, %sil
	jne	.L31
	testq	%rdi, %rdi
	jne	.L21
.L8:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L32
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movq	24(%rdi), %rax
	leaq	-48(%rbp), %r14
	movq	%r14, %rdi
	movq	32(%rax), %rbx
	movq	(%rbx), %r13
	movq	23(%r13), %rax
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo7GetCodeEv@PLT
	leaq	47(%r13), %rsi
	movq	%rax, 47(%r13)
	movq	%rax, %rdx
	testb	$1, %al
	je	.L10
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L10
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L10:
	movabsq	$287762808832, %rcx
	movq	(%rbx), %rdx
	movq	23(%rdx), %rax
	movq	7(%rax), %rax
	cmpq	%rcx, %rax
	je	.L21
	testb	$1, %al
	jne	.L14
.L18:
	movq	39(%rdx), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$155, 11(%rdx)
	jne	.L21
	movq	15(%rax), %rax
	testb	$1, %al
	jne	.L21
	sarq	$32, %rax
	cmpq	$4, %rax
	jne	.L21
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal14FeedbackVector23ClearOptimizationMarkerEv@PLT
	.p2align 4,,10
	.p2align 3
.L21:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L14:
	movq	-1(%rax), %rcx
	cmpw	$165, 11(%rcx)
	je	.L21
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L18
	jmp	.L21
.L32:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18755:
	.size	_ZN2v88internal12_GLOBAL__N_121DisposeCompilationJobEPNS0_23OptimizedCompilationJobEb, .-_ZN2v88internal12_GLOBAL__N_121DisposeCompilationJobEPNS0_23OptimizedCompilationJobEb
	.section	.text._ZN2v88internal27OptimizingCompileDispatcher11CompileTaskD2Ev,"axG",@progbits,_ZN2v88internal27OptimizingCompileDispatcher11CompileTaskD5Ev,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal27OptimizingCompileDispatcher11CompileTaskD1Ev
	.type	_ZThn32_N2v88internal27OptimizingCompileDispatcher11CompileTaskD1Ev, @function
_ZThn32_N2v88internal27OptimizingCompileDispatcher11CompileTaskD1Ev:
.LFB23006:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	subq	$32, %rdi
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE23006:
	.size	_ZThn32_N2v88internal27OptimizingCompileDispatcher11CompileTaskD1Ev, .-_ZThn32_N2v88internal27OptimizingCompileDispatcher11CompileTaskD1Ev
	.section	.text._ZN2v88internal27OptimizingCompileDispatcher11CompileTaskD0Ev,"axG",@progbits,_ZN2v88internal27OptimizingCompileDispatcher11CompileTaskD5Ev,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal27OptimizingCompileDispatcher11CompileTaskD0Ev
	.type	_ZThn32_N2v88internal27OptimizingCompileDispatcher11CompileTaskD0Ev, @function
_ZThn32_N2v88internal27OptimizingCompileDispatcher11CompileTaskD0Ev:
.LFB23007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-32(%rdi), %r12
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	movq	%r12, %rdi
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23007:
	.size	_ZThn32_N2v88internal27OptimizingCompileDispatcher11CompileTaskD0Ev, .-_ZThn32_N2v88internal27OptimizingCompileDispatcher11CompileTaskD0Ev
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB9602:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L42
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L45
.L36:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L36
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE9602:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.text._ZN2v88internal27OptimizingCompileDispatcherD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27OptimizingCompileDispatcherD2Ev
	.type	_ZN2v88internal27OptimizingCompileDispatcherD2Ev, @function
_ZN2v88internal27OptimizingCompileDispatcherD2Ev:
.LFB18769:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L47
	call	_ZdaPv@PLT
.L47:
	leaq	248(%rbx), %rdi
	call	_ZN2v84base17ConditionVariableD1Ev@PLT
	leaq	208(%rbx), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	leaq	152(%rbx), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L48
	movq	144(%rbx), %rax
	movq	112(%rbx), %r12
	leaq	8(%rax), %r13
	cmpq	%r12, %r13
	jbe	.L49
	.p2align 4,,10
	.p2align 3
.L50:
	movq	(%r12), %rdi
	addq	$8, %r12
	call	_ZdlPv@PLT
	cmpq	%r12, %r13
	ja	.L50
	movq	72(%rbx), %rdi
.L49:
	call	_ZdlPv@PLT
.L48:
	addq	$8, %rsp
	leaq	32(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5MutexD1Ev@PLT
	.cfi_endproc
.LFE18769:
	.size	_ZN2v88internal27OptimizingCompileDispatcherD2Ev, .-_ZN2v88internal27OptimizingCompileDispatcherD2Ev
	.globl	_ZN2v88internal27OptimizingCompileDispatcherD1Ev
	.set	_ZN2v88internal27OptimizingCompileDispatcherD1Ev,_ZN2v88internal27OptimizingCompileDispatcherD2Ev
	.section	.text._ZN2v88internal27OptimizingCompileDispatcher9NextInputEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27OptimizingCompileDispatcher9NextInputEb
	.type	_ZN2v88internal27OptimizingCompileDispatcher9NextInputEb, @function
_ZN2v88internal27OptimizingCompileDispatcher9NextInputEb:
.LFB18771:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	32(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	20(%rbx), %esi
	testl	%esi, %esi
	je	.L61
	movl	24(%rbx), %ecx
	movl	16(%rbx), %edi
	subl	$1, %esi
	movl	%ecx, %eax
	cltd
	idivl	%edi
	movq	8(%rbx), %rax
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %r14
	leal	1(%rcx), %eax
	movl	%esi, 20(%rbx)
	cltd
	idivl	%edi
	movl	%edx, 24(%rbx)
	testb	%r12b, %r12b
	jne	.L63
.L60:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movl	192(%rbx), %eax
	cmpl	$1, %eax
	jne	.L60
	movq	%r14, %rdi
	movl	$1, %esi
	xorl	%r14d, %r14d
	call	_ZN2v88internal12_GLOBAL__N_121DisposeCompilationJobEPNS0_23OptimizedCompilationJobEb
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L61:
	xorl	%r14d, %r14d
	jmp	.L60
	.cfi_endproc
.LFE18771:
	.size	_ZN2v88internal27OptimizingCompileDispatcher9NextInputEb, .-_ZN2v88internal27OptimizingCompileDispatcher9NextInputEb
	.section	.rodata._ZN2v88internal27OptimizingCompileDispatcher11CompileNextEPNS0_23OptimizedCompilationJobE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZN2v88internal27OptimizingCompileDispatcher11CompileNextEPNS0_23OptimizedCompilationJobE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27OptimizingCompileDispatcher11CompileNextEPNS0_23OptimizedCompilationJobE
	.type	_ZN2v88internal27OptimizingCompileDispatcher11CompileNextEPNS0_23OptimizedCompilationJobE, @function
_ZN2v88internal27OptimizingCompileDispatcher11CompileNextEPNS0_23OptimizedCompilationJobE:
.LFB18772:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	leaq	152(%rbx), %r13
	subq	$24, %rsp
	call	_ZN2v88internal23OptimizedCompilationJob10ExecuteJobEv@PLT
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	136(%rbx), %rcx
	movq	120(%rbx), %rax
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L66
	movq	%r12, (%rax)
	addq	$8, 120(%rbx)
.L67:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	(%rbx), %rdi
	addq	$24, %rsp
	movl	$4, %esi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	addq	$37512, %rdi
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10StackGuard16RequestInterruptENS1_13InterruptFlagE@PLT
	.p2align 4,,10
	.p2align 3
.L64:
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	144(%rbx), %r14
	movq	112(%rbx), %rsi
	movabsq	$1152921504606846975, %r8
	subq	128(%rbx), %rax
	movq	%r14, %r15
	sarq	$3, %rax
	subq	%rsi, %r15
	movq	%r15, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movq	104(%rbx), %rax
	subq	88(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%r8, %rax
	je	.L78
	movq	72(%rbx), %r9
	movq	80(%rbx), %rdx
	movq	%r14, %rax
	subq	%r9, %rax
	movq	%rdx, %rcx
	sarq	$3, %rax
	subq	%rax, %rcx
	cmpq	$1, %rcx
	jbe	.L79
.L69:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r14)
	movq	120(%rbx), %rax
	movq	%r12, (%rax)
	movq	144(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 144(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 128(%rbx)
	movq	%rdx, 136(%rbx)
	movq	%rax, 120(%rbx)
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L79:
	leaq	2(%rdi), %r10
	leaq	(%r10,%r10), %rax
	cmpq	%rax, %rdx
	jbe	.L70
	subq	%r10, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r9,%rdx,8), %r8
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r8, %rsi
	jbe	.L71
	cmpq	%r14, %rsi
	je	.L72
	movq	%r8, %rdi
	call	memmove@PLT
	movq	%rax, %r8
	.p2align 4,,10
	.p2align 3
.L72:
	movq	%r8, 112(%rbx)
	movq	(%r8), %rax
	leaq	(%r8,%r15), %r14
	movq	(%r8), %xmm0
	movq	%r14, 144(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 96(%rbx)
	movq	(%r14), %rax
	movq	%rax, 128(%rbx)
	addq	$512, %rax
	movq	%rax, 136(%rbx)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L70:
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	cmpq	%r8, %r14
	ja	.L80
	leaq	0(,%r14,8), %rdi
	movq	%r10, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %r10
	movq	112(%rbx), %rsi
	movq	%rax, %rcx
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r10, %rax
	shrq	%rax
	leaq	(%rcx,%rax,8), %r8
	movq	144(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L74
	movq	%r8, %rdi
	subq	%rsi, %rdx
	call	memmove@PLT
	movq	%rax, %r8
.L74:
	movq	72(%rbx), %rdi
	movq	%r8, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 80(%rbx)
	movq	-64(%rbp), %r8
	movq	%rax, 72(%rbx)
	jmp	.L72
.L71:
	cmpq	%r14, %rsi
	je	.L72
	leaq	8(%r15), %rdi
	movq	%r8, -56(%rbp)
	subq	%rdx, %rdi
	addq	%r8, %rdi
	call	memmove@PLT
	movq	-56(%rbp), %r8
	jmp	.L72
.L80:
	call	_ZSt17__throw_bad_allocv@PLT
.L78:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE18772:
	.size	_ZN2v88internal27OptimizingCompileDispatcher11CompileNextEPNS0_23OptimizedCompilationJobE, .-_ZN2v88internal27OptimizingCompileDispatcher11CompileNextEPNS0_23OptimizedCompilationJobE
	.section	.rodata._ZN2v88internal27OptimizingCompileDispatcher11CompileTask11RunInternalEv.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"disabled-by-default-v8.compile"
	.section	.rodata._ZN2v88internal27OptimizingCompileDispatcher11CompileTask11RunInternalEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	"V8.RecompileConcurrent"
	.section	.text._ZN2v88internal27OptimizingCompileDispatcher11CompileTask11RunInternalEv,"axG",@progbits,_ZN2v88internal27OptimizingCompileDispatcher11CompileTask11RunInternalEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal27OptimizingCompileDispatcher11CompileTask11RunInternalEv
	.type	_ZN2v88internal27OptimizingCompileDispatcher11CompileTask11RunInternalEv, @function
_ZN2v88internal27OptimizingCompileDispatcher11CompileTask11RunInternalEv:
.LFB18767:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-176(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	48(%rdi), %rsi
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeC1EPNS0_28WorkerThreadRuntimeCallStatsE@PLT
	movq	-176(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testq	%rdi, %rdi
	je	.L82
	testl	%eax, %eax
	jne	.L114
.L82:
	movq	40(%rbx), %rax
	leaq	-168(%rbp), %r14
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal15TimerEventScopeINS0_29TimerEventRecompileConcurrentEE13LogTimerEventENS0_6Logger8StartEndE@PLT
	movq	_ZZN2v88internal27OptimizingCompileDispatcher11CompileTask11RunInternalEvE27trace_event_unique_atomic72(%rip), %r12
	testq	%r12, %r12
	je	.L115
.L84:
	movq	$0, -160(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L116
.L86:
	movq	56(%rbx), %r12
	movslq	296(%r12), %rdi
	testl	%edi, %edi
	jne	.L117
.L90:
	leaq	32(%r12), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	20(%r12), %esi
	testl	%esi, %esi
	je	.L97
	movl	24(%r12), %ecx
	movl	16(%r12), %edi
	movl	%ecx, %eax
	cltd
	idivl	%edi
	movq	8(%r12), %rax
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %r8
	leal	1(%rcx), %eax
	cltd
	idivl	%edi
	leal	-1(%rsi), %eax
	movl	%eax, 20(%r12)
	movl	%edx, 24(%r12)
	movl	192(%r12), %eax
	cmpl	$1, %eax
	je	.L118
.L91:
	movq	%r15, %rdi
	movq	%r8, -184(%rbp)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-184(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal27OptimizingCompileDispatcher11CompileNextEPNS0_23OptimizedCompilationJobE
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	%r14, %rdi
	movl	$1, %esi
	call	_ZN2v88internal15TimerEventScopeINS0_29TimerEventRecompileConcurrentEE13LogTimerEventENS0_6Logger8StartEndE@PLT
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L119
.L92:
	movq	%r13, %rdi
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeD1Ev@PLT
	movq	56(%rbx), %rax
	leaq	208(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	56(%rbx), %rdx
	subl	$1, 200(%rdx)
	je	.L120
.L93:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L121
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	movq	56(%rbx), %rdi
	addq	$248, %rdi
	call	_ZN2v84base17ConditionVariable9NotifyOneEv@PLT
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L117:
	imulq	$1000, %rdi, %rdi
	call	_ZN2v84base2OS5SleepENS0_9TimeDeltaE@PLT
	movq	56(%rbx), %r12
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L116:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L122
.L87:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L88
	movq	(%rdi), %rax
	call	*8(%rax)
.L88:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L89
	movq	(%rdi), %rax
	call	*8(%rax)
.L89:
	leaq	.LC2(%rip), %rax
	movq	%r12, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L115:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L123
.L85:
	movq	%r12, _ZZN2v88internal27OptimizingCompileDispatcher11CompileTask11RunInternalEvE27trace_event_unique_atomic72(%rip)
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L97:
	xorl	%r8d, %r8d
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L119:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L123:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L122:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC2(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L87
.L114:
	leaq	-120(%rbp), %rsi
	movl	$195, %edx
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L118:
	movq	%r8, %rdi
	movl	$1, %esi
	call	_ZN2v88internal12_GLOBAL__N_121DisposeCompilationJobEPNS0_23OptimizedCompilationJobEb
	xorl	%r8d, %r8d
	jmp	.L91
.L121:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18767:
	.size	_ZN2v88internal27OptimizingCompileDispatcher11CompileTask11RunInternalEv, .-_ZN2v88internal27OptimizingCompileDispatcher11CompileTask11RunInternalEv
	.section	.text._ZN2v88internal14CancelableTask3RunEv,"axG",@progbits,_ZN2v88internal14CancelableTask3RunEv,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal14CancelableTask3RunEv
	.type	_ZThn32_N2v88internal14CancelableTask3RunEv, @function
_ZThn32_N2v88internal14CancelableTask3RunEv:
.LFB23008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	lock cmpxchgl	%edx, -16(%rdi)
	jne	.L124
	movq	-32(%rdi), %rax
	leaq	_ZN2v88internal27OptimizingCompileDispatcher11CompileTask11RunInternalEv(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L161
	subq	$32, %rdi
	call	*%rax
.L124:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L162
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L162:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L161:
	movq	16(%rdi), %rsi
	leaq	-176(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeC1EPNS0_28WorkerThreadRuntimeCallStatsE@PLT
	movq	-176(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testq	%rdi, %rdi
	je	.L127
	testl	%eax, %eax
	je	.L127
	leaq	-120(%rbp), %rsi
	movl	$195, %edx
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
.L127:
	movq	8(%rbx), %rax
	leaq	-168(%rbp), %r14
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal15TimerEventScopeINS0_29TimerEventRecompileConcurrentEE13LogTimerEventENS0_6Logger8StartEndE@PLT
	movq	_ZZN2v88internal27OptimizingCompileDispatcher11CompileTask11RunInternalEvE27trace_event_unique_atomic72(%rip), %r12
	testq	%r12, %r12
	je	.L163
.L128:
	movq	$0, -160(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L164
.L130:
	movq	24(%rbx), %r12
	movslq	296(%r12), %rax
	testl	%eax, %eax
	je	.L134
	imulq	$1000, %rax, %rdi
	call	_ZN2v84base2OS5SleepENS0_9TimeDeltaE@PLT
	movq	24(%rbx), %r12
.L134:
	leaq	32(%r12), %r8
	movq	%r8, %rdi
	movq	%r8, -184(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	20(%r12), %esi
	movq	-184(%rbp), %r8
	testl	%esi, %esi
	je	.L141
	movl	24(%r12), %ecx
	movl	16(%r12), %edi
	subl	$1, %esi
	movl	%ecx, %eax
	cltd
	idivl	%edi
	movq	8(%r12), %rax
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %r15
	leal	1(%rcx), %eax
	movl	%esi, 20(%r12)
	cltd
	idivl	%edi
	movl	%edx, 24(%r12)
	movl	192(%r12), %eax
	subl	$1, %eax
	jne	.L135
	movq	%r15, %rdi
	movl	$1, %esi
	movq	%r8, -184(%rbp)
	xorl	%r15d, %r15d
	call	_ZN2v88internal12_GLOBAL__N_121DisposeCompilationJobEPNS0_23OptimizedCompilationJobEb
	movq	-184(%rbp), %r8
.L135:
	movq	%r8, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal27OptimizingCompileDispatcher11CompileNextEPNS0_23OptimizedCompilationJobE
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	%r14, %rdi
	movl	$1, %esi
	call	_ZN2v88internal15TimerEventScopeINS0_29TimerEventRecompileConcurrentEE13LogTimerEventENS0_6Logger8StartEndE@PLT
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L136
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
.L136:
	movq	%r13, %rdi
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeD1Ev@PLT
	movq	24(%rbx), %rax
	leaq	208(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	24(%rbx), %rdx
	subl	$1, 200(%rdx)
	jne	.L137
	movq	24(%rbx), %rdi
	addq	$248, %rdi
	call	_ZN2v84base17ConditionVariable9NotifyOneEv@PLT
.L137:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L124
.L141:
	xorl	%r15d, %r15d
	jmp	.L135
.L164:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L131
	pushq	%rdx
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC2(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
.L131:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L132
	movq	(%rdi), %rax
	call	*8(%rax)
.L132:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L133
	movq	(%rdi), %rax
	call	*8(%rax)
.L133:
	leaq	.LC2(%rip), %rax
	movq	%r12, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L130
.L163:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	je	.L129
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
.L129:
	movq	%r12, _ZZN2v88internal27OptimizingCompileDispatcher11CompileTask11RunInternalEvE27trace_event_unique_atomic72(%rip)
	jmp	.L128
	.cfi_endproc
.LFE23008:
	.size	_ZThn32_N2v88internal14CancelableTask3RunEv, .-_ZThn32_N2v88internal14CancelableTask3RunEv
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CancelableTask3RunEv
	.type	_ZN2v88internal14CancelableTask3RunEv, @function
_ZN2v88internal14CancelableTask3RunEv:
.LFB15548:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	lock cmpxchgl	%edx, 16(%rdi)
	jne	.L165
	movq	(%rdi), %rax
	leaq	_ZN2v88internal27OptimizingCompileDispatcher11CompileTask11RunInternalEv(%rip), %rdx
	movq	%rdi, %r12
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L167
	movq	48(%rdi), %rsi
	leaq	-176(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeC1EPNS0_28WorkerThreadRuntimeCallStatsE@PLT
	movq	-176(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testq	%rdi, %rdi
	je	.L168
	testl	%eax, %eax
	jne	.L203
.L168:
	movq	40(%r12), %rax
	leaq	-168(%rbp), %r15
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal15TimerEventScopeINS0_29TimerEventRecompileConcurrentEE13LogTimerEventENS0_6Logger8StartEndE@PLT
	movq	_ZZN2v88internal27OptimizingCompileDispatcher11CompileTask11RunInternalEvE27trace_event_unique_atomic72(%rip), %r13
	testq	%r13, %r13
	je	.L204
.L170:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L205
.L172:
	movq	56(%r12), %r13
	movslq	296(%r13), %rdi
	testl	%edi, %edi
	jne	.L206
.L176:
	leaq	32(%r13), %rbx
	movq	%rbx, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	20(%r13), %esi
	testl	%esi, %esi
	je	.L183
	movl	24(%r13), %ecx
	movl	16(%r13), %edi
	movl	%ecx, %eax
	cltd
	idivl	%edi
	movq	8(%r13), %rax
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %r8
	leal	1(%rcx), %eax
	cltd
	idivl	%edi
	leal	-1(%rsi), %eax
	movl	%eax, 20(%r13)
	movl	%edx, 24(%r13)
	movl	192(%r13), %eax
	cmpl	$1, %eax
	je	.L207
.L177:
	movq	%rbx, %rdi
	movq	%r8, -184(%rbp)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-184(%rbp), %r8
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal27OptimizingCompileDispatcher11CompileNextEPNS0_23OptimizedCompilationJobE
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	%r15, %rdi
	movl	$1, %esi
	call	_ZN2v88internal15TimerEventScopeINS0_29TimerEventRecompileConcurrentEE13LogTimerEventENS0_6Logger8StartEndE@PLT
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L208
.L178:
	movq	%r14, %rdi
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeD1Ev@PLT
	movq	56(%r12), %rax
	leaq	208(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	56(%r12), %rdx
	subl	$1, 200(%rdx)
	je	.L209
.L179:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L165:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L210
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	movq	56(%r12), %rdi
	addq	$248, %rdi
	call	_ZN2v84base17ConditionVariable9NotifyOneEv@PLT
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L206:
	imulq	$1000, %rdi, %rdi
	call	_ZN2v84base2OS5SleepENS0_9TimeDeltaE@PLT
	movq	56(%r12), %r13
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L205:
	pxor	%xmm0, %xmm0
	xorl	%ebx, %ebx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L211
.L173:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L174
	movq	(%rdi), %rax
	call	*8(%rax)
.L174:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L175
	movq	(%rdi), %rax
	call	*8(%rax)
.L175:
	leaq	.LC2(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L204:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L212
.L171:
	movq	%r13, _ZZN2v88internal27OptimizingCompileDispatcher11CompileTask11RunInternalEvE27trace_event_unique_atomic72(%rip)
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L167:
	call	*%rax
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L183:
	xorl	%r8d, %r8d
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L208:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L211:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC2(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %rbx
	addq	$64, %rsp
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L212:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L171
.L203:
	leaq	-120(%rbp), %rsi
	movl	$195, %edx
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L207:
	movq	%r8, %rdi
	movl	$1, %esi
	call	_ZN2v88internal12_GLOBAL__N_121DisposeCompilationJobEPNS0_23OptimizedCompilationJobEb
	xorl	%r8d, %r8d
	jmp	.L177
.L210:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15548:
	.size	_ZN2v88internal14CancelableTask3RunEv, .-_ZN2v88internal14CancelableTask3RunEv
	.section	.text._ZN2v88internal27OptimizingCompileDispatcher16FlushOutputQueueEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27OptimizingCompileDispatcher16FlushOutputQueueEb
	.type	_ZN2v88internal27OptimizingCompileDispatcher16FlushOutputQueueEb, @function
_ZN2v88internal27OptimizingCompileDispatcher16FlushOutputQueueEb:
.LFB18773:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movzbl	%sil, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	leaq	152(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L220:
	addq	$8, %rax
	movq	%rax, 88(%rbx)
.L216:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_121DisposeCompilationJobEPNS0_23OptimizedCompilationJobEb
.L217:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	88(%rbx), %rax
	cmpq	120(%rbx), %rax
	je	.L219
	movq	104(%rbx), %rcx
	movq	(%rax), %r14
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	jne	.L220
	movq	96(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	112(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 112(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 96(%rbx)
	movq	%rdx, 104(%rbx)
	movq	%rax, 88(%rbx)
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L219:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE18773:
	.size	_ZN2v88internal27OptimizingCompileDispatcher16FlushOutputQueueEb, .-_ZN2v88internal27OptimizingCompileDispatcher16FlushOutputQueueEb
	.section	.rodata._ZN2v88internal27OptimizingCompileDispatcher25InstallOptimizedFunctionsEv.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"  ** Aborting compilation for "
	.align 8
.LC4:
	.string	" as it has already been optimized.\n"
	.section	.text._ZN2v88internal27OptimizingCompileDispatcher25InstallOptimizedFunctionsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27OptimizingCompileDispatcher25InstallOptimizedFunctionsEv
	.type	_ZN2v88internal27OptimizingCompileDispatcher25InstallOptimizedFunctionsEv, @function
_ZN2v88internal27OptimizingCompileDispatcher25InstallOptimizedFunctionsEv:
.LFB18776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movabsq	$287762808832, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	152(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%r13), %rax
	addl	$1, 41104(%r13)
	movq	%rax, -80(%rbp)
	movq	41096(%r13), %rax
	movq	%rax, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L222:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	88(%rbx), %rax
	cmpq	%rax, 120(%rbx)
	je	.L255
	movq	104(%rbx), %rcx
	movq	(%rax), %r15
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L225
	addq	$8, %rax
	movq	%rax, 88(%rbx)
.L226:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	(%rbx), %rcx
	movq	24(%r15), %rax
	movq	41112(%rcx), %rdi
	movq	32(%rax), %rax
	movq	(%rax), %rsi
	testq	%rdi, %rdi
	je	.L227
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rdx
.L228:
	movq	47(%rsi), %rax
	leaq	47(%rsi), %rcx
	leaq	23(%rsi), %rdi
	cmpl	$67, 59(%rax)
	jne	.L256
.L232:
	movq	(%rdi), %rax
	movq	7(%rax), %rax
	cmpq	%r14, %rax
	je	.L244
	testb	$1, %al
	jne	.L238
.L241:
	movq	39(%rsi), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rcx
	cmpw	$155, 11(%rcx)
	je	.L257
.L244:
	movq	(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8Compiler31FinalizeOptimizedCompilationJobEPNS0_23OptimizedCompilationJobEPNS0_7IsolateE@PLT
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L256:
	movq	23(%rsi), %rax
	movq	7(%rax), %rax
	cmpq	%r14, %rax
	je	.L232
	testb	$1, %al
	jne	.L258
.L233:
	movq	(%rcx), %rax
	testb	$62, 43(%rax)
	jne	.L232
	movq	(%rcx), %rax
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	jne	.L232
.L236:
	cmpb	$0, _ZN2v88internal35FLAG_trace_concurrent_recompilationE(%rip)
	jne	.L259
.L245:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_121DisposeCompilationJobEPNS0_23OptimizedCompilationJobEb
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L227:
	movq	41088(%rcx), %rdx
	cmpq	41096(%rcx), %rdx
	je	.L260
.L229:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%rdx)
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L259:
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rdi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-72(%rbp), %rdx
	movq	stdout(%rip), %rsi
	leaq	-64(%rbp), %rdi
	movq	(%rdx), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L225:
	movq	96(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	112(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 112(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 96(%rbx)
	movq	%rdx, 104(%rbx)
	movq	%rax, 88(%rbx)
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L257:
	movq	15(%rax), %rax
	testb	$1, %al
	je	.L244
	cmpl	$3, %eax
	je	.L244
	andq	$-3, %rax
	je	.L244
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	jne	.L244
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L255:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-80(%rbp), %rax
	subl	$1, 41104(%r13)
	movq	%rax, 41088(%r13)
	movq	-88(%rbp), %rax
	cmpq	41096(%r13), %rax
	je	.L221
	movq	%rax, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L221:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L261
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	movq	-1(%rax), %r9
	cmpw	$165, 11(%r9)
	je	.L232
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L233
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L260:
	movq	%rcx, %rdi
	movq	%rsi, -96(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	-72(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L238:
	movq	-1(%rax), %rcx
	cmpw	$165, 11(%rcx)
	je	.L244
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L241
	jmp	.L244
.L261:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18776:
	.size	_ZN2v88internal27OptimizingCompileDispatcher25InstallOptimizedFunctionsEv, .-_ZN2v88internal27OptimizingCompileDispatcher25InstallOptimizedFunctionsEv
	.section	.text._ZN2v88internal27OptimizingCompileDispatcher20QueueForOptimizationEPNS0_23OptimizedCompilationJobE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27OptimizingCompileDispatcher20QueueForOptimizationEPNS0_23OptimizedCompilationJobE
	.type	_ZN2v88internal27OptimizingCompileDispatcher20QueueForOptimizationEPNS0_23OptimizedCompilationJobE, @function
_ZN2v88internal27OptimizingCompileDispatcher20QueueForOptimizationEPNS0_23OptimizedCompilationJobE:
.LFB18777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	32(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	24(%rbx), %eax
	addl	20(%rbx), %eax
	movq	%r13, %rdi
	cltd
	idivl	16(%rbx)
	movslq	%edx, %rax
	movq	8(%rbx), %rdx
	movq	%r12, (%rdx,%rax,8)
	addl	$1, 20(%rbx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	cmpb	$0, _ZN2v88internal35FLAG_block_concurrent_recompilationE(%rip)
	je	.L263
	addl	$1, 196(%rbx)
.L262:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L271
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L263:
	.cfi_restore_state
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	(%rbx), %r12
	movl	$64, %edi
	movq	%rax, %r14
	movq	(%rax), %rax
	movq	56(%rax), %r15
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE@PLT
	leaq	16+_ZTVN2v88internal27OptimizingCompileDispatcher11CompileTaskE(%rip), %rax
	movq	%r12, 40(%r13)
	addq	$32, %r13
	movq	%rax, -32(%r13)
	addq	$48, %rax
	movq	%rax, 0(%r13)
	movq	40960(%r12), %rax
	leaq	208(%rbx), %r12
	movq	%rbx, 24(%r13)
	movq	%r12, %rdi
	addq	$50888, %rax
	movq	%rax, 16(%r13)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	24(%r13), %rax
	movq	%r12, %rdi
	addl	$1, 200(%rax)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	%r14, %rdi
	movq	%r13, -64(%rbp)
	leaq	-64(%rbp), %rsi
	call	*%r15
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L262
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L262
.L271:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18777:
	.size	_ZN2v88internal27OptimizingCompileDispatcher20QueueForOptimizationEPNS0_23OptimizedCompilationJobE, .-_ZN2v88internal27OptimizingCompileDispatcher20QueueForOptimizationEPNS0_23OptimizedCompilationJobE
	.section	.text._ZN2v88internal27OptimizingCompileDispatcher7UnblockEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27OptimizingCompileDispatcher7UnblockEv
	.type	_ZN2v88internal27OptimizingCompileDispatcher7UnblockEv, @function
_ZN2v88internal27OptimizingCompileDispatcher7UnblockEv:
.LFB18780:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	208(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-64(%rbp), %rax
	movq	%rax, -80(%rbp)
	movl	196(%rdi), %eax
	testl	%eax, %eax
	jle	.L272
	.p2align 4,,10
	.p2align 3
.L277:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	(%rbx), %r14
	movl	$64, %edi
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	56(%rax), %rcx
	movq	%rcx, -72(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE@PLT
	movq	40960(%r14), %rdx
	movq	%r12, %rdi
	movq	%r14, 40(%r13)
	leaq	16+_ZTVN2v88internal27OptimizingCompileDispatcher11CompileTaskE(%rip), %rax
	movq	%rbx, 56(%r13)
	addq	$50888, %rdx
	movq	%rax, 0(%r13)
	addq	$48, %rax
	movq	%rax, 32(%r13)
	movq	%rdx, 48(%r13)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	56(%r13), %rdx
	movq	%r12, %rdi
	addl	$1, 200(%rdx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	leaq	32(%r13), %rax
	movq	%r15, %rdi
	movq	-80(%rbp), %rsi
	movq	%rax, -64(%rbp)
	movq	-72(%rbp), %rcx
	call	*%rcx
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L274
	movq	(%rdi), %rax
	call	*8(%rax)
.L274:
	movl	196(%rbx), %eax
	subl	$1, %eax
	movl	%eax, 196(%rbx)
	testl	%eax, %eax
	jg	.L277
.L272:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L282
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L282:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18780:
	.size	_ZN2v88internal27OptimizingCompileDispatcher7UnblockEv, .-_ZN2v88internal27OptimizingCompileDispatcher7UnblockEv
	.section	.rodata._ZN2v88internal27OptimizingCompileDispatcher5FlushENS0_16BlockingBehaviorE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"  ** Flushed concurrent recompilation queues (not blocking).\n"
	.align 8
.LC6:
	.string	"  ** Flushed concurrent recompilation queues.\n"
	.section	.text._ZN2v88internal27OptimizingCompileDispatcher5FlushENS0_16BlockingBehaviorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27OptimizingCompileDispatcher5FlushENS0_16BlockingBehaviorE
	.type	_ZN2v88internal27OptimizingCompileDispatcher5FlushENS0_16BlockingBehaviorE, @function
_ZN2v88internal27OptimizingCompileDispatcher5FlushENS0_16BlockingBehaviorE:
.LFB18774:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	cmpl	$1, %esi
	je	.L308
	movl	$1, 192(%rdi)
	mfence
	cmpb	$0, _ZN2v88internal35FLAG_block_concurrent_recompilationE(%rip)
	jne	.L309
.L294:
	leaq	208(%rbx), %r12
	leaq	248(%rbx), %r13
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	200(%rbx), %edx
	testl	%edx, %edx
	jle	.L297
	.p2align 4,,10
	.p2align 3
.L298:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v84base17ConditionVariable4WaitEPNS0_5MutexE@PLT
	movl	200(%rbx), %eax
	testl	%eax, %eax
	jg	.L298
.L297:
	movq	%r12, %rdi
	leaq	152(%rbx), %r12
	movl	$0, 192(%rbx)
	mfence
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L311:
	addq	$8, %rax
	movq	%rax, 88(%rbx)
.L303:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal12_GLOBAL__N_121DisposeCompilationJobEPNS0_23OptimizedCompilationJobEb
.L296:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	88(%rbx), %rax
	cmpq	%rax, 120(%rbx)
	je	.L310
	movq	104(%rbx), %rcx
	movq	(%rax), %r13
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	jne	.L311
	movq	96(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	112(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 112(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 96(%rbx)
	movq	%rdx, 104(%rbx)
	movq	%rax, 88(%rbx)
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L310:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	cmpb	$0, _ZN2v88internal35FLAG_trace_concurrent_recompilationE(%rip)
	jne	.L312
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L312:
	.cfi_restore_state
	popq	%rbx
	leaq	.LC6(%rip), %rdi
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6PrintFEPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L309:
	.cfi_restore_state
	call	_ZN2v88internal27OptimizingCompileDispatcher7UnblockEv
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L308:
	cmpb	$0, _ZN2v88internal35FLAG_block_concurrent_recompilationE(%rip)
	jne	.L313
.L285:
	leaq	32(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	20(%rbx), %esi
	testl	%esi, %esi
	jle	.L288
	.p2align 4,,10
	.p2align 3
.L286:
	movl	24(%rbx), %ecx
	movl	16(%rbx), %edi
	subl	$1, %esi
	movl	%ecx, %eax
	cltd
	idivl	%edi
	movq	8(%rbx), %rax
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %r8
	leal	1(%rcx), %eax
	movl	%esi, 20(%rbx)
	movl	$1, %esi
	cltd
	idivl	%edi
	movq	%r8, %rdi
	movl	%edx, 24(%rbx)
	call	_ZN2v88internal12_GLOBAL__N_121DisposeCompilationJobEPNS0_23OptimizedCompilationJobEb
	movl	20(%rbx), %esi
	testl	%esi, %esi
	jg	.L286
.L288:
	leaq	152(%rbx), %r13
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L315:
	addq	$8, %rax
	movq	%rax, 88(%rbx)
.L293:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_121DisposeCompilationJobEPNS0_23OptimizedCompilationJobEb
.L287:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	88(%rbx), %rax
	cmpq	%rax, 120(%rbx)
	je	.L314
	movq	104(%rbx), %rcx
	movq	(%rax), %r14
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	jne	.L315
	movq	96(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	112(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 112(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 96(%rbx)
	movq	%rdx, 104(%rbx)
	movq	%rax, 88(%rbx)
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L314:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	cmpb	$0, _ZN2v88internal35FLAG_trace_concurrent_recompilationE(%rip)
	jne	.L316
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L313:
	.cfi_restore_state
	call	_ZN2v88internal27OptimizingCompileDispatcher7UnblockEv
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L316:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE18774:
	.size	_ZN2v88internal27OptimizingCompileDispatcher5FlushENS0_16BlockingBehaviorE, .-_ZN2v88internal27OptimizingCompileDispatcher5FlushENS0_16BlockingBehaviorE
	.section	.text._ZN2v88internal27OptimizingCompileDispatcher4StopEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27OptimizingCompileDispatcher4StopEv
	.type	_ZN2v88internal27OptimizingCompileDispatcher4StopEv, @function
_ZN2v88internal27OptimizingCompileDispatcher4StopEv:
.LFB18775:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	$1, 192(%rdi)
	mfence
	cmpb	$0, _ZN2v88internal35FLAG_block_concurrent_recompilationE(%rip)
	jne	.L337
.L318:
	leaq	208(%r12), %r13
	leaq	248(%r12), %rbx
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	200(%r12), %r8d
	testl	%r8d, %r8d
	jle	.L322
	.p2align 4,,10
	.p2align 3
.L323:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v84base17ConditionVariable4WaitEPNS0_5MutexE@PLT
	movl	200(%r12), %esi
	testl	%esi, %esi
	jg	.L323
.L322:
	movq	%r13, %rdi
	movl	$0, 192(%r12)
	mfence
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	296(%r12), %edi
	testl	%edi, %edi
	je	.L338
	movl	20(%r12), %ecx
	leaq	32(%r12), %rbx
	testl	%ecx, %ecx
	jle	.L327
	.p2align 4,,10
	.p2align 3
.L329:
	movq	%rbx, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	20(%r12), %esi
	testl	%esi, %esi
	je	.L326
	movl	24(%r12), %ecx
	movl	16(%r12), %edi
	subl	$1, %esi
	movl	%ecx, %eax
	cltd
	idivl	%edi
	movq	8(%r12), %rax
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %r13
	leal	1(%rcx), %eax
	movl	%esi, 20(%r12)
	cltd
	idivl	%edi
	movq	%rbx, %rdi
	movl	%edx, 24(%r12)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal27OptimizingCompileDispatcher11CompileNextEPNS0_23OptimizedCompilationJobE
	movl	20(%r12), %edx
	testl	%edx, %edx
	jg	.L329
.L327:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal27OptimizingCompileDispatcher25InstallOptimizedFunctionsEv
	.p2align 4,,10
	.p2align 3
.L326:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal27OptimizingCompileDispatcher11CompileNextEPNS0_23OptimizedCompilationJobE
	movl	20(%r12), %eax
	testl	%eax, %eax
	jg	.L329
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal27OptimizingCompileDispatcher25InstallOptimizedFunctionsEv
	.p2align 4,,10
	.p2align 3
.L337:
	.cfi_restore_state
	call	_ZN2v88internal27OptimizingCompileDispatcher7UnblockEv
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L338:
	leaq	152(%r12), %r13
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L340:
	addq	$8, %rax
	movq	%rax, 88(%r12)
.L332:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_121DisposeCompilationJobEPNS0_23OptimizedCompilationJobEb
.L324:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	88(%r12), %rax
	cmpq	%rax, 120(%r12)
	je	.L339
	movq	104(%r12), %rcx
	movq	(%rax), %r14
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	jne	.L340
	movq	96(%r12), %rdi
	call	_ZdlPv@PLT
	movq	112(%r12), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 112(%r12)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 96(%r12)
	movq	%rdx, 104(%r12)
	movq	%rax, 88(%r12)
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L339:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE18775:
	.size	_ZN2v88internal27OptimizingCompileDispatcher4StopEv, .-_ZN2v88internal27OptimizingCompileDispatcher4StopEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal27OptimizingCompileDispatcherD2Ev,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal27OptimizingCompileDispatcherD2Ev, @function
_GLOBAL__sub_I__ZN2v88internal27OptimizingCompileDispatcherD2Ev:
.LFB22933:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22933:
	.size	_GLOBAL__sub_I__ZN2v88internal27OptimizingCompileDispatcherD2Ev, .-_GLOBAL__sub_I__ZN2v88internal27OptimizingCompileDispatcherD2Ev
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal27OptimizingCompileDispatcherD2Ev
	.weak	_ZTVN2v88internal14CancelableTaskE
	.section	.data.rel.ro._ZTVN2v88internal14CancelableTaskE,"awG",@progbits,_ZTVN2v88internal14CancelableTaskE,comdat
	.align 8
	.type	_ZTVN2v88internal14CancelableTaskE, @object
	.size	_ZTVN2v88internal14CancelableTaskE, 88
_ZTVN2v88internal14CancelableTaskE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	__cxa_pure_virtual
	.quad	-32
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.weak	_ZTVN2v88internal27OptimizingCompileDispatcher11CompileTaskE
	.section	.data.rel.ro.local._ZTVN2v88internal27OptimizingCompileDispatcher11CompileTaskE,"awG",@progbits,_ZTVN2v88internal27OptimizingCompileDispatcher11CompileTaskE,comdat
	.align 8
	.type	_ZTVN2v88internal27OptimizingCompileDispatcher11CompileTaskE, @object
	.size	_ZTVN2v88internal27OptimizingCompileDispatcher11CompileTaskE, 88
_ZTVN2v88internal27OptimizingCompileDispatcher11CompileTaskE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal27OptimizingCompileDispatcher11CompileTaskD1Ev
	.quad	_ZN2v88internal27OptimizingCompileDispatcher11CompileTaskD0Ev
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	_ZN2v88internal27OptimizingCompileDispatcher11CompileTask11RunInternalEv
	.quad	-32
	.quad	0
	.quad	_ZThn32_N2v88internal27OptimizingCompileDispatcher11CompileTaskD1Ev
	.quad	_ZThn32_N2v88internal27OptimizingCompileDispatcher11CompileTaskD0Ev
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.weak	_ZZN2v88internal27OptimizingCompileDispatcher11CompileTask11RunInternalEvE27trace_event_unique_atomic72
	.section	.bss._ZZN2v88internal27OptimizingCompileDispatcher11CompileTask11RunInternalEvE27trace_event_unique_atomic72,"awG",@nobits,_ZZN2v88internal27OptimizingCompileDispatcher11CompileTask11RunInternalEvE27trace_event_unique_atomic72,comdat
	.align 8
	.type	_ZZN2v88internal27OptimizingCompileDispatcher11CompileTask11RunInternalEvE27trace_event_unique_atomic72, @gnu_unique_object
	.size	_ZZN2v88internal27OptimizingCompileDispatcher11CompileTask11RunInternalEvE27trace_event_unique_atomic72, 8
_ZZN2v88internal27OptimizingCompileDispatcher11CompileTask11RunInternalEvE27trace_event_unique_atomic72:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
