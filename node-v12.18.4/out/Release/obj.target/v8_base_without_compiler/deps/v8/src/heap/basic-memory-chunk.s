	.file	"basic-memory-chunk.cc"
	.text
	.section	.text._ZN2v88internal16BasicMemoryChunkC2Emmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16BasicMemoryChunkC2Emmm
	.type	_ZN2v88internal16BasicMemoryChunkC2Emmm, @function
_ZN2v88internal16BasicMemoryChunkC2Emmm:
.LFB10351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rsi, (%rdi)
	movl	$4096, %esi
	movq	$0, 8(%rdi)
	movl	$1, %edi
	call	calloc@PLT
	movq	%r13, 40(%rbx)
	movq	%rax, 16(%rbx)
	leaq	1(%rbx), %rax
	movq	%r12, 48(%rbx)
	movq	%rax, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10351:
	.size	_ZN2v88internal16BasicMemoryChunkC2Emmm, .-_ZN2v88internal16BasicMemoryChunkC2Emmm
	.globl	_ZN2v88internal16BasicMemoryChunkC1Emmm
	.set	_ZN2v88internal16BasicMemoryChunkC1Emmm,_ZN2v88internal16BasicMemoryChunkC2Emmm
	.section	.text._ZN2v88internal16BasicMemoryChunk17HasHeaderSentinelEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16BasicMemoryChunk17HasHeaderSentinelEm
	.type	_ZN2v88internal16BasicMemoryChunk17HasHeaderSentinelEm, @function
_ZN2v88internal16BasicMemoryChunk17HasHeaderSentinelEm:
.LFB10353:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	xorl	%r8d, %r8d
	andq	$-262144, %rax
	leaq	56(%rax), %rdx
	cmpq	%rdi, %rdx
	ja	.L4
	movq	32(%rax), %rdx
	addq	$1, %rax
	cmpq	%rdx, %rax
	sete	%r8b
.L4:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE10353:
	.size	_ZN2v88internal16BasicMemoryChunk17HasHeaderSentinelEm, .-_ZN2v88internal16BasicMemoryChunk17HasHeaderSentinelEm
	.section	.text._ZN2v88internal16BasicMemoryChunk20ReleaseMarkingBitmapEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16BasicMemoryChunk20ReleaseMarkingBitmapEv
	.type	_ZN2v88internal16BasicMemoryChunk20ReleaseMarkingBitmapEv, @function
_ZN2v88internal16BasicMemoryChunk20ReleaseMarkingBitmapEv:
.LFB10354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	free@PLT
	movq	$0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10354:
	.size	_ZN2v88internal16BasicMemoryChunk20ReleaseMarkingBitmapEv, .-_ZN2v88internal16BasicMemoryChunk20ReleaseMarkingBitmapEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal16BasicMemoryChunkC2Emmm,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal16BasicMemoryChunkC2Emmm, @function
_GLOBAL__sub_I__ZN2v88internal16BasicMemoryChunkC2Emmm:
.LFB11859:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE11859:
	.size	_GLOBAL__sub_I__ZN2v88internal16BasicMemoryChunkC2Emmm, .-_GLOBAL__sub_I__ZN2v88internal16BasicMemoryChunkC2Emmm
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal16BasicMemoryChunkC2Emmm
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
